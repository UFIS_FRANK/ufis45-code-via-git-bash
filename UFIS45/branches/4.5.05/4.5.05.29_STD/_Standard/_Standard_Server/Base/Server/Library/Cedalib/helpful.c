#ifndef _DEF_mks_version_helpful_c
  #define _DEF_mks_version_helpful_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_helpful_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/helpful.c 1.3 2005/09/26 22:02:58SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CCS Program Skeleton                                                       */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/* This program is not a CCS main program */


/* the one and only headerfile... */
#include <helpful.h>

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetNoOfElements -> count number of element, separator is char 
s = String to count elements
c = separator
Format of Sting is: "aaaa-bbbbb-fffff-ggggg-eeeee...."
*/
int GetNoOfElements(char *s, char c)
{
	int	anz;

	if (s == NULL)	
		return -1;
	if (!strlen(s))
		return 0;
	
	anz = 0;
	while (*s)
	{
		if (*s == c)
			anz++;	
		s++;
	}

	return ++anz;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetNoOfElements2->count number of element, separator is char* 
*/
int GetNoOfElements2(char *s1, char *s2)
{
	int	anz;
	int	len;

	if (s1 == NULL)
		return -1;
	if (s2 == NULL)
		return -2;
	if (!strlen(s2))
		return -3;
	if (!strlen(s1))
		return 0;

	anz = 0;
	len = strlen(s2);
	while (*s1)
	{
		if (*s1 == *s2)
		{
			if (!strncmp(s1, s2, len))
				anz++;
		}
		s1++;
	}

	return ++anz;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// StringUPR->converts a string to uppercase 
*/
int StringUPR(UCHAR *s)
{
	if (s == NULL)
		return -1;

	while (*s)
	{
		switch (*s)
		{
			/* � */
			case 252:
				*s = 220;
				break;
			/* � */
			case 228:
				*s = 196;
				break;
			/* � */
			case 246:
				*s = 214;
				break;
			default:
				*s = toupper(*s);
				break;
		}
		s++; 
	}
	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// StringLWR->converts a string to lowercase 
*/
int StringLWR(UCHAR *s)
{
	if (s == NULL)
		return -1;

	while (*s)
	{
		switch (*s)
		{
			/* � */
			case 220:
				*s = 252;
				break;
			/* � */
			case 196:
				*s = 228;
				break;
			/* � */
			case 214:
				*s = 246;
				break;
			default:
				*s = tolower(*s);
				break;
		}
		s++; 
	}
	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////  SeparateIt->
Separates one string 's1' with separator 'c' to two strings 's2' and 's3'
examples: 
s1 = "Murks-Mist"
c = -
s2 = Murks
s3 = Mist
*/
int SeparateIt(char *s1, char *s2, char *s3, char c)
{
	char	*p;

	if (s1 == NULL || s2 == NULL || s3 == NULL)
		return -1;

	p = s2;
	while (*s1)
	{
		if (*s1 == c)
		{
			*p = 0x00;
			p = s3;
			s1++;
			continue;
		}
		*p++ = *s1++;
	}
	*p = 0x00;
	return 0;
}


/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////  GetTimeStamp -> returns GMT-TimeStamp
*/
/* ======================== */
/* Renamed to ...Old by BST */
/* ======================== */
/* Corrected Version see below */
/* ======================== */

char *GetTimeStampOld(void)
{
	time_t		_llCurTime;
	struct tm	*prlCurTime;
	static char pclTime[iTIME_SIZE];
  
	memset((void*)pclTime, 0x00, iTIME_SIZE);
	_llCurTime = time(0L);
	prlCurTime = (struct tm *) gmtime(&_llCurTime);
	strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",prlCurTime);
	return pclTime;	
} 

char *GetTimeStamp(void)
{
	time_t		_llCurTime;
	struct tm	*prlCurTime;
   struct tm   rlTm;
	static char pclTime[iTIME_SIZE];
  
	memset((void*)pclTime, 0x00, iTIME_SIZE);
	_llCurTime = time(0L);
	prlCurTime = (struct tm *) gmtime(&_llCurTime);
   rlTm = *prlCurTime;
	strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
	return pclTime;	
}
char *GetTimeStampLocal(void)
{
	time_t		_llCurTime;
	struct tm	*prlCurTime;
   struct tm   rlTm;
	static char pclTime[iTIME_SIZE];
  
	memset((void*)pclTime, 0x00, iTIME_SIZE);
	_llCurTime = time(0L);
	prlCurTime = (struct tm *) localtime(&_llCurTime);
   rlTm = *prlCurTime;
	strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
	return pclTime;	
} 


/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetDataField ->
returns a field spcified with field-number to the calling routine 
*/
char *GetDataField(char *s1, UINT No, char c)
{
	UINT					i = 0;
	char					*pclS;
	static char			pclTmpBuf[iMAXIMUM];

	if (s1 == NULL)
		return NULL;

	/* clear buffer */
	memset ((void*)pclTmpBuf, 0x00, iMAXIMUM);
	pclS = pclTmpBuf;

	/* search the field */	
	while (*s1)
	{
		if (*s1 == c)
			i++; 
		else if (i == No)
		{
			while ((*s1 != c) && *s1)
				*pclS++ = *s1++;
			break;
		}
		s1++;
	}
	*pclS = 0x00;

	/* delete all blanks at end of string... */
	while (*--pclS == cBLANK)
		*pclS = 0x00;
	
	return pclTmpBuf;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetDataField2, separator is char* 
*/
char *GetDataField2(char *s1, UINT No, char *s2)
{
	UINT				i = 0;
	UINT				ilFound;
	int				len;
	char				*pclS;
	static char		pclTmpBuf[iMAXIMUM];

	if (s1 == NULL || s2 == NULL)
		return NULL;

	/* clear buffer */
	memset ((void*)pclTmpBuf, 0x00, iMAXIMUM);
	pclS = pclTmpBuf;
	len = strlen(s2);

	/* serach field */
	while (*s1)
	{
		ilFound = 0;
		if (!strncmp(s1, s2, len))
		{
			i++;
			ilFound = 1;
		}
		else if (i == No)
		{
			while (strncmp(s1, s2, len) && *s1)
				*pclS++ = *s1++;
			break;
		}
		if (ilFound)
			s1 += len;
		else
			s1++;
	}
	*pclS = 0x00;

	/* delete all blanks at end of string... */
	while (*--pclS == cBLANK)
		*pclS = 0x00;

	return pclTmpBuf;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CopyNextField, separator is char 
*/
char *CopyNextField(char *s1, char c, char *s3)
{
	/* check params */
	if (s1 == NULL || s3 == NULL)
		return NULL;

	/* copy to buffer */
	while (*s1 != c && *s1)
		*s3++ = *s1++;
	*s3 = 0x00;
	s1++;
	
	/* delete all blanks at end of buffer */
	while (*--s3 == cBLANK)
		*s3 = 0x00;

	/* bye bye */
	return s1;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CopyNextField2, separator is char* 
*/
char *CopyNextField2(char *s1, char *s2, char *s3)
{
	int	ilLen;

	/* check params */
	if (s1 == NULL || s2 == NULL || s3 == NULL)
		return NULL;

	/* copy to buffer */
	ilLen = strlen(s2);
	while (strncmp(s1, s2, ilLen) && *s1)
		*s3++ = *s1++;
	*s3 = 0x00;
	s1 += ilLen;
	
	/* delete all blanks at end of string... */
	while (*--s3 == cBLANK)
		*s3 = 0x00;

	/* bye bye */
	return s1;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CopyDataField, separator is char 
*/
int CopyDataField(char *s1, UINT No, char c, char *s2)
{
	UINT		i = 0;

	if (s1 == NULL || s2 == NULL)
		return -1;

	/* search the field */	
	while (*s1)
	{
		if (*s1 == c)
			i++;
		else if (i == No)
		{
			while ((*s1 != c) && *s1)
				*s2++ = *s1++;
			break;
		}
		s1++;
	}
	*s2 = 0x00;
	
	/* delete all blanks at end of string... */
	while (*--s2 == cBLANK)
		*s2 = 0x00;
	
	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CopyDataField2, separator is char*
*/
int CopyDataField2(char *s1, UINT No, char *s2, char *s3)
{
	UINT				i = 0;
	UINT				ilFound;

	if (s1 == NULL || s2 == NULL || s3 == NULL)
		return -1;

	/* serach field */
	while (*s1)
	{
		ilFound = 0;
		if (!strncmp(s1, s2, strlen(s2)))
		{
			i++;
			ilFound = 1;
		}
		else if (i == No)
		{
			while (strncmp(s1, s2, strlen(s2)) && *s1)
				*s3++ = *s1++;
			break;
		}
		if (ilFound)
			s1 += strlen(s2);
		else
			s1++;
	}
	*s3 = 0x00;

	/* delete all blanks at end of string... */
	while (*--s3 == cBLANK)
		*s3 = 0x00;

	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// FormatIt follows
*/
char *FormatIt(UINT ipAlign, UINT ipSize, char *pcpString)
{
	UINT				ilAktLen;
	UINT				ilDiffLen;
	static char		pclTmpBuf[iMAXIMUM];

	if ((ilAktLen = strlen(pcpString)) > ipSize)
		return NULL;
	else
	{
		memset((void*)pclTmpBuf, 0x00, iMAXIMUM);
		ilDiffLen = ipSize - ilAktLen;	
		if (ipAlign == iLEFT)
		{
			memmove((void*)pclTmpBuf,
					  (const void*)pcpString,
					  (size_t)ilAktLen);
			memset((void*)&pclTmpBuf[ilAktLen],
					  iBLANK, (size_t)ilDiffLen);
		}
		else if (ipAlign == iRIGHT)
		{
			memset((void*)pclTmpBuf,
					  iBLANK, (size_t)ilDiffLen);
			memmove((void*)&pclTmpBuf[ilDiffLen],
					  (const void*)pcpString,
					  (size_t)ilAktLen);
		}
		else
		{
			return NULL;
		}
	}
	return pclTmpBuf;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// AnsiToAscii 
*/
void AnsiToAscii(UCHAR *s1)
{
	if (s1 == NULL)
		return;

	while (*s1)
	{
		switch (*s1)
		{
			/* � */
			case 228:
				*s1 = 132;
				break;
			/* � */
			case 196:
				*s1 = 142;
				break;
			/* � */
			case 246:
				*s1 = 148;
				break;
			/* � */
			case 214:
				*s1 = 153;
				break;
			/* � */
			case 252:
				*s1 = 129;
				break;
			/* � */
			case 220:
				*s1 = 154;
				break;
			/* � */
			case 223:
				*s1 = 225;
				break;
			default:
				/* nothing to do here */
				break;
		}
		s1++;
	}
	return;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// AsciiToAnsi 
*/
void AsciiToAnsi(UCHAR *s1)
{
	if (s1 == NULL)
		return;

	while (*s1)
	{
		switch (*s1)
		{
			/* � */
			case 132:
				*s1 = 228;
				break;
			/* � */
			case 142:
				*s1 = 196;
				break;
			/* � */
			case 148:
				*s1 = 246;
				break;
			/* � */
			case 153:
				*s1 = 214;
				break;
			/* � */
			case 129:
				*s1 = 252;
				break;
			/* � */
			case 154:
				*s1 = 220;
				break;
			/* � */
			case 225:
				*s1 = 223;
				break;
			default:
				/* nothing to do here */
				break;
		}
		s1++;
	}
	return;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// BuildFixList follows 
*/
int BuildFixList(char *Out, char **In, UINT *Pos, UINT Anz)
{
	UINT		i;
	UINT		CurrentPos;

	if (Out == NULL || In == NULL || Pos == NULL)
		return -1;

	for (i=0, CurrentPos=0; i<Anz-1; i++)
	{
		CurrentPos = Pos[i];
		memmove((void*)(Out+CurrentPos),
				  (const void*)(In[i]),
				  (size_t)(Pos[i+1]-Pos[i]));
	}
	CurrentPos = Pos[i];
	memmove((void*)(Out+CurrentPos),
			  (const void*)(In[i]),
			  (size_t)(strlen((char*)In[i])));
	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// InsertIntoString 
*/
int InsertIntoString(char *pcpSrc, int ipPos, char *pcpIns)
{
	UINT		i;
	UINT		ilInsLen;
	UINT		ilSrcLen;

	if (pcpSrc == NULL || pcpIns == NULL || ipPos < 0)
		return -1;

	if ((int)strlen((char*)pcpSrc) < ipPos)
	{
		/* simply add Ins at end of Src */
		/* and set all bytes > position strlen to blank */
		for (i=strlen((char*)pcpSrc); i<ipPos; i++)
			pcpSrc[i] = ' ';
		strcpy((char*)&pcpSrc[i], (char*)pcpIns);
	}
	else if (strlen((char*)pcpSrc) == ipPos)
	{
		/* simple case */
		strcat((char*)pcpSrc, (char*)pcpIns);
	}
	else
	{
		ilInsLen = strlen((char*)pcpIns);
		ilSrcLen = strlen((char*)pcpSrc);

		/* difficult case - position < strlen(src) */
		/* move memory to new position */
		memmove((void*)(pcpSrc+ipPos+strlen((char*)pcpIns)),
				  (const void*)(pcpSrc+ipPos),
				  (size_t)strlen((char*)&pcpSrc[ipPos]));

		/* set new bytes at specified position */
		memmove((void*)(pcpSrc+ipPos),
				  (const void*)pcpIns,
				  (size_t)strlen((char*)pcpIns));

		/* set \0 at end */
		*(pcpSrc+ilInsLen+ilSrcLen) = 0x00;
	}

	/* everything looks good */
	return 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// SEncode 
*/
char *SEncode(char *pcpPasswd, int ipLen)
{
	int				ilLen;
	int				ilSeconds;
	char				*pclS;
	char				pclTime[15];
	static char		pclAll[10*iMAX_BUF_SIZE];

	pclTime[0] = 0x00;
	memset(pclAll, 0x00, 10*iMAX_BUF_SIZE);
	strcpy(pclTime, GetTimeStamp());
	memcpy(pclAll, &pclTime[12], 2);
	ilSeconds = atoi(&pclTime[12]);
	memcpy(&pclAll[2], pcpPasswd, ipLen);
	for (ilLen=0, pclS=&pclAll[2]; ilLen<ipLen; ilLen++, pclS++)
		 *pclS += ilSeconds;
	for (ilLen=0, pclS=pclAll; ilLen<ipLen+2; ilLen++, pclS++)
		*pclS = ~(*pclS);
	return pclAll;
}
	
/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// SDecode 
*/
char *SDecode(char *pcpPasswd, int ipLen, int ipFlg)
{
	int				ilLen;
	char				*pclS;
	char				pclSeconds[3];
	static int		ilSeconds;
	static char		pclAll[10*iMAX_BUF_SIZE];

	memset(pclAll, 0x00, 10*iMAX_BUF_SIZE);
	memcpy(pclAll, pcpPasswd, ipLen);
	for (ilLen=0, pclS=pclAll; ilLen<ipLen; ilLen++, pclS++)
		*pclS = ~(*pclS);
	if (!ipFlg)
	{
		memset(pclSeconds, 0x00, 3);
		memcpy(pclSeconds, pclAll, 2);
		ilSeconds = atoi(pclSeconds);
	}
	for (ilLen=0, pclS=pclAll; ilLen<ipLen && ipFlg; ilLen++, pclS++)
		*pclS -= ilSeconds;
	return pclAll;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CEncode 
*/
void CEncode(char **pcpPasswd)
{
	int					i;
	int					ilPos;
	int					year;
	int					month;
	int					day;
	int					hour;
	int					minute;
	int					second;
	char					*pclTime;
	char					*pclS;
	char					pclTmpBuf[iMIN_BUF_SIZE];
	static char			pclAll[iMIN_BUF_SIZE];

	memset((void*)pclAll, 0x00, iMIN_BUF_SIZE);
	strcpy((char*)pclAll, (char*)(*pcpPasswd));
	pclS = pclAll;
	for (i=0; i<(int)strlen((char*)pclAll); i++)
		*(pclS+i) += i;
	pclTime = GetTimeStamp();

	/* copy year */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, pclTime, 4);
	year = atoi((char*)pclTmpBuf);
	
	/* copy month */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, &pclTime[4], 2);
	month = atoi((char*)pclTmpBuf);

	/* copy day */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, &pclTime[6], 2);
	day = atoi((char*)pclTmpBuf);
	
	/* copy hour */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, &pclTime[8], 2);
	hour = atoi((char*)pclTmpBuf);

	/* copy hour */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, &pclTime[10], 2);
	minute = atoi((char*)pclTmpBuf);

	/* copy second */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strncpy((char*)pclTmpBuf, &pclTime[12], 2);
	second = atoi((char*)pclTmpBuf);

	for (i=0; i<6; i++)
	{
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		switch(i)
		{
			case 0:
				year %= 16;
				sprintf((char*)pclTmpBuf,"%4.4d", year);
				ilPos = year;
				break;
			case 1:
				month %= 6;
				sprintf((char*)pclTmpBuf,"%2.2d", month);
				ilPos = month;
				break;
			case 2:
				sprintf((char*)pclTmpBuf,"%2.2d", day);
				ilPos = day;
				break;
			case 3:
				hour %= 6;
				sprintf((char*)pclTmpBuf,"%2.2d", hour);
				ilPos = hour;
				break;
			case 4:
				minute %= 45;
				sprintf((char*)pclTmpBuf,"%2.2d", minute);
				ilPos = minute;
				break;
			case 5:
				second %= 6;
				sprintf((char*)pclTmpBuf,"%2.2d", second);
				ilPos = second;
				break;
		}
		pclS = pclAll;
		while (*pclS)
		{
			*pclS += (!(i%2) ? ilPos : -ilPos);
			pclS++;
		}
		strcat((char*)pclAll, (char*)pclTmpBuf);
	}
	pclS = pclAll;
	while (*pclS)
	{
		*pclS = ~(*pclS);
		pclS++;
	}
	*pcpPasswd = pclAll;
	return;
}
	
/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CDecode 
*/
void CDecode(char **pcpPasswd)
{
	int				i;
	int				ilPos;
	int				ilLen;
	char				*pclS;
	char				pclTmpBuf[iMIN_BUF_SIZE];
	static char		pclAll[iMIN_BUF_SIZE];

	memset((void*)pclAll, 0x00, iMIN_BUF_SIZE);
	strcpy((char*)pclAll, (char*)(*pcpPasswd));
	ilLen = strlen((char*)pclAll);
	pclS = pclAll;
	for (i=0; i<ilLen; i++)
		*(pclS+i) = ~(*(pclS+i));
		
	for (i=5; i>=0; i--)
	{
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		switch (i)
		{
			case 5:
				memmove(pclTmpBuf, &pclAll[ilLen-2], 2);
				ilPos = atoi((char*)pclTmpBuf);
				break;
			case 4:
				memmove(pclTmpBuf, &pclAll[ilLen-4], 2);
				ilPos = atoi((char*)pclTmpBuf);
				break;
			case 3:
				memmove(pclTmpBuf, &pclAll[ilLen-6], 2);
				ilPos = atoi((char*)pclTmpBuf);
				break;
			case 2:
				memmove(pclTmpBuf, &pclAll[ilLen-8], 2);
				ilPos = atoi((char*)pclTmpBuf);
				break;
			case 1:
				memmove(pclTmpBuf, &pclAll[ilLen-10], 2);
				ilPos = atoi((char*)pclTmpBuf);
				break;
			case 0:
				memmove(pclTmpBuf, &pclAll[ilLen-14], 4);
				ilPos = atoi((char*)pclTmpBuf);
				break;
		}
		pclS = pclAll;
		while (*pclS)
		{
			*pclS -= (!(i%2) ? ilPos : -ilPos);
			pclS++;
		}
	}
	pclAll[ilLen-14] = '\0';
	pclS = pclAll;
	for (i=0; i<(int)strlen((char*)pclAll); i++)
		*(pclS+i) -= i;
	*pcpPasswd = pclAll;
	return;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetIndex 
*/
int GetIndex(char *pcpSrc, char *pcpSearch, char cpSeparator)
{
	int		ilPos;
	int		ilIdx;
	char		pclTmpBuf[iMAXIMUM];

	if (pcpSrc == NULL)
		return -1;
	if (pcpSearch == NULL)
		return -2;
	if (cpSeparator == 0x00)
		return -3;

	ilPos = 0;
	ilIdx = 0;
	memset((void*)pclTmpBuf, 0x00, iMAXIMUM);
	while (*pcpSrc)
	{
		if (*pcpSrc == cpSeparator)
		{
			if (!strcmp((char*)pclTmpBuf, (char*)pcpSearch))
				return ilPos;
			else
			{
				ilIdx = 0;
				memset((void*)pclTmpBuf, 0x00, iMAXIMUM);
			}			
			ilPos++; 
		}
		else
			pclTmpBuf[ilIdx++] = *pcpSrc;
		pcpSrc++;
	}
	return !strcmp((char*)pclTmpBuf, (char*)pcpSearch) ? ilPos : -4;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// MatchPattern 
*/
int MatchPattern(char *s1, char *s2)
{
	/* s1: pd0424.txt */
	/* s2: pd????.txt */
	/* s2: pd*.txt    */

	if (s1 == NULL)
		return -1;
	if (s2 == NULL)
		return -2;
	
	while (*s2)
	{
		switch (*s2)
		{
			case '?':
				s1++; s2++;
				break;
			case '*':
				if (strlen((char*)s1) < strlen((char*)s2))
					return 0;
				while (*s2 == '*')
					s2++;
				while (strlen((char*)s1) > strlen((char*)s2))
				{
					s1++;
					if (*s1 == *s2)
						break;
				}
				break;
			default:
				if (*s1 != *s2)
					return 0;
				s1++; s2++;
				break;
		}
	}
	return *s1 != *s2 ? 0 : 1;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// DeleteCharacterInString 
*/
void DeleteCharacterInString(char *pcpS, char cpChar)
{
	if (pcpS == NULL)
		return;

	while (*pcpS)
	{
		if (*pcpS == cpChar) 
		{
			memmove ((void*)pcpS, (const void*)(pcpS+1), 
											(size_t)(strlen((char*)(pcpS+1))));
			pcpS[strlen((char*)pcpS)-1] = 0x00;
		}
		else
			pcpS++;
	}
	return;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetAllBetween 
*/
int GetAllBetween(char *pcpInData, char cpSeparator, int ipNo, char *pcpOutData)
{
	int				ilIdx;
	int				ilStart;
	int				ilInternalNo;

	if (pcpInData == NULL)
		return -1;
	if (pcpOutData == NULL)
		return -2;
	if (ipNo < 0)
		return -3;

	/* init */
	ilStart = ilIdx = 0;	
	ilInternalNo = -1;

	while (*pcpInData)
	{
		if (*pcpInData == cpSeparator) 
		{
			if (*(pcpInData+1) == cpSeparator)
			{
				pcpInData += 2;
				ilInternalNo++;
			}
			else
			{
				ilStart = ilStart ? 0 : 1;
				if (ilStart)
					ilInternalNo++;
				pcpInData++;
			}
		}
		if (ilStart)
		{
			if (ipNo == ilInternalNo)
				pcpOutData[ilIdx++] = *pcpInData;
		}
		pcpInData++;
	}
	pcpOutData[ilIdx] = 0x00;

	/* good bye */
	return ipNo > ilInternalNo ? -4 : 0;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// AddToCurrentTime 
*/
char *AddToCurrentTime(int ipType, int ipTime, int ipFlg)
{
	time_t			tlCurrentTime;
	struct tm		*prlCurrentTime;
	struct tm		tm;
	static char		pclTimeStamp[15];

	/* clear buffer */
	memset ((void*)pclTimeStamp, 0x00, 15);

	/* get current timestamp (result in seconds) */
	tlCurrentTime = time(0L);

	/* add/sub seconds to/from current time */ 
	if (ipType == iMINUTES)
		tlCurrentTime += (ipTime * 60);
	else if (ipType == iDAYS || ipType == iMONTH)
		tlCurrentTime += (ipTime * 24 * 60 * 60);
	else
		return NULL;

	/* convert to structure */
	prlCurrentTime = (struct tm*)localtime(&tlCurrentTime);
	tm = *prlCurrentTime;

	/* format string */
	if (ipType == iMINUTES)
	{
		strftime(pclTimeStamp, 15, 
					"%" "Y%" "m%" "d%" "H%" "M%" "S%", &tm);
	}
	else if (ipType == iDAYS)
	{
		if (ipFlg == iSTART)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "000000", &tm);
		else if (ipFlg == iEND)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "235959", &tm);
		else
			return NULL;
	}
	else if (ipType == iMONTH)
	{
		if (ipFlg == iSTART)
			strftime(pclTimeStamp, 15, "%" "Y%" "m" "01000000", &tm);
		else if (ipFlg == iEND)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "235959", &tm);
		else
			return NULL;
	}
	else
		return NULL;
	
	/* bye bye */
	return pclTimeStamp;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// AddToCurrentUtcTime 
*/
char *AddToCurrentUtcTime(int ipType, int ipTime, int ipFlg)
{
	time_t			tlCurrentTime;
	struct tm		*prlCurrentTime;
	struct tm		tm;
	static char		pclTimeStamp[15];

	/* clear buffer */
	memset ((void*)pclTimeStamp, 0x00, 15);

	/* get current timestamp (result in seconds) */
	tlCurrentTime = time(0L);

	/* add/sub seconds to/from current time */ 
	if (ipType == iMINUTES)
		tlCurrentTime += (ipTime * 60);
	else if (ipType == iDAYS || ipType == iMONTH)
		tlCurrentTime += (ipTime * 24 * 60 * 60);
	else
		return NULL;

	/* convert to structure */
	prlCurrentTime = (struct tm*)gmtime(&tlCurrentTime);
	tm = *prlCurrentTime;

	/* format string */
	if (ipType == iMINUTES)
	{
		strftime(pclTimeStamp, 15, 
					"%" "Y%" "m%" "d%" "H%" "M%" "S%", &tm);
	}
	else if (ipType == iDAYS)
	{
		if (ipFlg == iSTART)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "000000", &tm);
		else if (ipFlg == iEND)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "235959", &tm);
		else
			return NULL;
	}
	else if (ipType == iMONTH)
	{
		if (ipFlg == iSTART)
			strftime(pclTimeStamp, 15, "%" "Y%" "m" "01000000", &tm);
		else if (ipFlg == iEND)
			strftime(pclTimeStamp, 15, "%" "Y%" "m%" "d" "235959", &tm);
		else
			return NULL;
	}
	else
		return NULL;
	
	/* bye bye */
	return pclTimeStamp;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// GetPartOfTimeStamp 
*/
char *GetPartOfTimeStamp(char *pcpTimeStamp, char *pcpPattern)
{
	int			ilCnt;
	int			ilIdx;
	char			*pclS;
	char			*pclPtr;
	static char	pclPartOfTimeStamp[15];

	if (pcpTimeStamp == NULL)
		return NULL;
	if (pcpPattern == NULL)
		return NULL;

	/* format of timestamp must be YYYYMMDDHHMISS */
	if (strlen(pcpTimeStamp) != 14)
		return NULL;

	/* search for not allowed characters */
	pclS = pcpTimeStamp;
	while (*pclS)
	{
		if (*pclS < 0x30 || *pclS > 0x39)
			return NULL;
		pclS++;
	}

	/* convert to uppercase */
	StringUPR((UCHAR*)pcpPattern);

	/* clear internal buffer */
	memset((void*)pclPartOfTimeStamp, 0x00, 15);

	/* pattern can be: YYMMDD, MMSS, everything is possible?!... */
	ilIdx = 0;
	pclS = pcpPattern;
	while (*pclS)
	{
		ilCnt = 0;
		pclPtr = pclS;
		while (*pclPtr == *pclS)
		{
			ilCnt++;
			pclPtr++;
		}

		switch (*pclS)
		{
			case 'S':
				if (ilCnt > 2)
					return NULL;
				memcpy(&pclPartOfTimeStamp[ilIdx], &pcpTimeStamp[14-ilCnt], ilCnt);
				ilIdx += ilCnt;
				break;
			case 'H':
				if (ilCnt > 2)
					return NULL;
				memcpy(&pclPartOfTimeStamp[ilIdx], &pcpTimeStamp[10-ilCnt], ilCnt);
				ilIdx += ilCnt;
				break;
			case 'D':	
				if (ilCnt > 2)
					return NULL;
				memcpy(&pclPartOfTimeStamp[ilIdx], &pcpTimeStamp[8-ilCnt], ilCnt);
				ilIdx += ilCnt;
				break;
			case 'M':
				if (ilCnt > 2)
					return NULL;
				if (*(pclS+1) == 'I')
				{
					/* this is for minute... */
					ilCnt++;
					memcpy(&pclPartOfTimeStamp[ilIdx], 
										&pcpTimeStamp[12-ilCnt], ilCnt);
				}
				else
				{
					memcpy(&pclPartOfTimeStamp[ilIdx], 
										&pcpTimeStamp[6-ilCnt], ilCnt);
				}
				ilIdx += ilCnt;
				break;
			case 'Y':
				if (ilCnt > 4)
					return NULL;
				memcpy(&pclPartOfTimeStamp[ilIdx], &pcpTimeStamp[4-ilCnt], ilCnt);
				ilIdx += ilCnt;
				break;
			default:
				return NULL;
		}
		pclS += ilCnt;
	}
	
	return pclPartOfTimeStamp;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// SearchStringAndReplace 
*/
int SearchStringAndReplace(char *pcpString, char *pcpSearch, char *pcpReplace)
{
	int		ilRC;
	int		ilPos;
	int		ilStringLength;
	char		*pclPos;

	if (pcpString == NULL)
		return -1;
	if (pcpSearch == NULL)
		return -2;
	if (pcpReplace == NULL)
		return -3;

	if ((pclPos = strstr(pcpString, pcpSearch)) == NULL)
		return -4;
	else
	{
		ilPos = pclPos - pcpString;
		ilStringLength = strlen(pcpString);

		memmove((void*)&pcpString[ilPos], 
					&pcpString[ilPos+strlen(pcpSearch)],
					(size_t)strlen(&pcpString[ilPos+strlen(pcpSearch)]));
		pcpString[ilStringLength-strlen(pcpSearch)] = 0x00;

		ilRC = InsertIntoString((char*)pcpString, ilPos, (char*)pcpReplace);
		return ilRC < 0 ? (ilRC - 4) : 0;
	}
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// ConvertToCEDATimeStamp 
*/
char *ConvertToCEDATimeStamp(char *pcpFormat, char *pcpTimeStamp)
{
	int				ilCnt;
	char				*pclS;
	char				*pclT;
	char				*pclPtr;
	char				pclTmpBuf[32];
	static char		pclInternalTimeStamp[15];

	if (pcpFormat == NULL)
		return NULL;
	if (pcpTimeStamp == NULL)
		return NULL;

	memset((void*)pclInternalTimeStamp, 0x30, 14);
	pclInternalTimeStamp[14] = 0x00;
	
	pclS = pcpFormat;
	pclT = pcpTimeStamp;
	while (*pclS)
	{
		ilCnt = 0;
		pclPtr = pclS;
		while (*pclPtr == *pclS)
		{
			ilCnt++;
			pclPtr++;
		}

		switch (*pclS)
		{
			case 'Y':
				if (ilCnt == 2)
				{
					strncpy(pclTmpBuf, pclT, 2);
					if (atoi(pclTmpBuf) < 80)
						memcpy((void*)pclInternalTimeStamp, "20", 2);
					else
						memcpy((void*)pclInternalTimeStamp, "19", 2);
					memcpy((void*)&pclInternalTimeStamp[2], (const void*)pclT, 2);
				}
				else if (ilCnt == 4)
					memcpy((void*)pclInternalTimeStamp, (const void*)pclT, 4);
				else
					return NULL;
				break;
			case 'M':
				if (*(pclS+1) == 'M')
				{
					memcpy((void*)&pclInternalTimeStamp[4], (const void*)pclT, 2);
				}
				else
				{
					memcpy((void*)&pclInternalTimeStamp[10], (const void*)pclT, 2);
				}
				break;
			case 'D':
				memcpy((void*)&pclInternalTimeStamp[6], (const void*)pclT, 2);
				break;
			case 'H':
				memcpy((void*)&pclInternalTimeStamp[8], (const void*)pclT, 2);
				break;
			case 'S':
				memcpy((void*)&pclInternalTimeStamp[12], (const void*)pclT, 2);
				break;
		}
		pclS += ilCnt;
		pclT += ilCnt;
	}

	return pclInternalTimeStamp;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// FileExist 
*/
int FileExist(char *pcpFile)
{
	FILE	*pfh = NULL;

	if (pcpFile == NULL)
		return RC_FAIL;

	if ((pfh = fopen(pcpFile, "r")) != NULL)
	{
		fclose(pfh);
		return RC_SUCCESS;
	}
	else
		return RC_NOT_FOUND;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// DeleteCharacterBetween 
*/
void DeleteCharacterBetween(char *pcpInString, char cpSearchCharacter, 
									 char cpReplaceCharacter, char cpBetweenCharacter)
{
	int	ilFlg = 0;

	while (*pcpInString)
	{
		if (*pcpInString == cpBetweenCharacter)
			ilFlg = !ilFlg;

		if (ilFlg)
		{
			if (*pcpInString == cpSearchCharacter)
				*pcpInString = cpReplaceCharacter;	
		}
		pcpInString++;
	}
	return;
}

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// StringReverse 
*/
void StringReverse(char *pcpS)
{
	char	*pclS;
	char	pclTmpBuf[iMAXIMUM];

	if (strlen(pcpS))
	{
		strcpy(pclTmpBuf, pcpS);
		pclS = &pclTmpBuf[strlen(pclTmpBuf)-1];
		while (pclS >= pclTmpBuf)
			*pcpS++ = *pclS--;
	}
	return;
}

