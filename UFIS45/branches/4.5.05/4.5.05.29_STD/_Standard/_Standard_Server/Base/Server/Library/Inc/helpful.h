#ifndef _DEF_mks_version_helpful_h
  #define _DEF_mks_version_helpful_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_helpful_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/helpful.h 1.3 2005/09/26 22:02:59SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CCS Program Skeleton                                                       */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "tools.h"

#ifndef _HELPFUL_INC
#define _HELPFUL_INC


#define 	UINT					unsigned int
#define	UCHAR					unsigned char
#define	ULONG					unsigned long
#define	USHORT				unsigned short

#define 	iTIME_SIZE			15
#define 	iMIN					32
#define 	iMAX					0xFFFF
#define 	iMIN_BUF_SIZE		128
#define 	iMAX_BUF_SIZE		1024
#define 	iMAXIMUM				8192

#define	iLEFT					0
#define	iRIGHT				1
#define 	iBLANK				32		
#define 	cBLANK				' '

#define	iMINUTES				0
#define	iDAYS					1
#define	iMONTH				2
#define	iSTART				0
#define	iEND					1


/* Prototyps */
int		GetNoOfElements(char *, char);
int		GetNoOfElements2(char *, char *);
int		StringUPR(UCHAR *);
int		StringLWR(UCHAR *);
int		SeparateIt(char *, char *, char *, char);
char		*GetTimeStamp(void);
char		*GetTimeStampLocal(void);
char		*GetDataField(char *, UINT, char);
char		*GetDataField2(char *, UINT, char *);
char		*CopyNextField(char *, char, char *);
char		*CopyNextField2(char *, char *, char *);
int		CopyDataField(char *, UINT, char, char *);
int		CopyDataField2(char *, UINT, char *, char *);
char		*FormatIt(UINT, UINT, char *);
void		AnsiToAscii(UCHAR *);
void		AsciiToAnsi(UCHAR *);
int		BuildFixList(char *, char **, UINT *, UINT);
int		InsertIntoString(char *, int, char *);
char		*SEncode(char *, int);
char		*SDecode(char *, int, int);
void		CDecode(char **);
void		CEncode(char **);
int		GetIndex(char *, char *, char);
int		MatchPattern(char *, char *);
void		DeleteCharacterInString(char *, char);
int		GetAllBetween(char *, char, int, char *);
char		*AddToCurrentTime(int, int, int);
char		*AddToCurrentUtcTime(int, int, int);
char		*GetPartOfTimeStamp(char *, char *);
int		SearchStringAndReplace(char *, char *, char *);
char		*ConvertToCEDATimeStamp(char *, char *);
int		FileExist(char *);
void		DeleteCharacterBetween(char *, char, char, char);
void 		StringReverse(char *);

#endif /* _HELPFUL_H */


