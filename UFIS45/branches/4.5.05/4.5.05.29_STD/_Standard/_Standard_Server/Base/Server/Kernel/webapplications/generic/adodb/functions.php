<?php
session_name("Staff");
session_start();
session_register("Session");

function IsNumeric($n)
{
  $p=trim($n);
  $l=strlen($p);

  for ($t=0; $t<$l; $t++)
  {
    $c=substr($p,$t,1);
    if ($c<'0' || $c>'9')
    { if ($c!='.') return 0; }
  }

  return 1;
}

function len($str) {
		return strlen($str);
}
	
function DateAdd ($interval,  $number, $date) {

$date_time_array  = getdate($date);
    
$hours =  $date_time_array["hours"];
$minutes =  $date_time_array["minutes"];
$seconds =  $date_time_array["seconds"];
$month =  $date_time_array["mon"];
$day =  $date_time_array["mday"];
$year =  $date_time_array["year"];

    switch ($interval) {
    
        case "yyyy":
            $year +=$number;
            break;        
        case "q":
            $year +=($number*3);
            break;        
        case "m":
            $month +=$number;
            break;        
        case "y":
        case "d":
        case "w":
             $day+=$number;
            break;        
        case "ww":
             $day+=($number*7);
            break;        
        case "h":
             $hours+=$number;
            break;        
        case "n":
             $minutes+=$number;
            break;        
        case "s":
             $seconds+=$number;
            break;        

    }    
	$timestamp =  mktime($hours ,$minutes, $seconds,$month ,$day, $year);
    return $timestamp;
}


Function DateDiff ($interval, $date1,$date2) {

    // get the number of seconds between the two dates
	$timedifference =  $date2 - $date1;
    
     switch ($interval) {
         case "w":
             $retval  = bcdiv($timedifference ,604800);
             break;
         case "d":
             $retval  = bcdiv($timedifference,86400);
             break;
         case "h":
              $retval = bcdiv($timedifference,3600);
             break;        
         case "n":
             $retval  = bcdiv($timedifference,60);
             break;        
         case "s":
             $retval  = $timedifference;
             break;        

   }    
     return $retval;
}

Function FindTDI () {
 Global $db;
 $cmdTempCommandText = "SELECT TICH,TDI1,TDI2   from CEDA.APTTAB  where APC3='AYT'";
 $Currenttimediff=&$db->Execute($cmdTempCommandText);
	if (! $Currenttimediff->EOF)  {

/* $tich= ChopString ("n", 0,  $Currenttimediff->fields("TICH")) ;

   if (DateDiff ('s', time(),$tich)>0) {
      $tdi=  $Currenttimediff->fields("TDI2");
   } else {
      $tdi=  $Currenttimediff->fields("TDI1");
   }
*/
	
	    $tich= ChopString ("n", 0,  $Currenttimediff->fields("TICH")) ;
            $hh=gmstrftime("%H",time());
            $mm=gmstrftime("%M",time());
            $ss=gmstrftime("%S",time());

            $Month=gmstrftime("%m",time());
            $Day=gmstrftime("%d",time());
            $Year=gmstrftime("%Y",time());

            $tnow=mktime($hh,$mm,$ss,$Month,$Day,$Year);
 
            if ($tnow>$tich) {
	        $tdi=  $Currenttimediff->fields("TDI2");
            } else {
                $tdi=  $Currenttimediff->fields("TDI1");
            }	
		return $tdi;		
			
	  }// If End
    if ($timediff) $timediff->Close();
}

function ChopString ($interval,  $number, $str) {

	if (len($str)>0) {
	$year = substr ($str, 0, 4); 
	$month = substr ($str, 4,2); 
	$day = substr ($str, 6, 2); 		
	$hour = substr ($str, 8, 2); 
	$min = substr ($str, 10, 2); 
	$sec = substr ($str, 12, 2); 
	$date=mktime ($hour,$min,$sec,$month,$day,$year);			
	} else {$date="";}
		
	
	if ($number>0 && len($str)>0) {
		return DateAdd ($interval,  $number, $date) ;
	} else {
		return $date;	
	}
	
}

//function arrival_hours ($fromhour , $tohour,$flno2='',$flno3='',$org='',$Sfromdate) {
//	Global $db,$tdi;
//	/*
//	-1 , +12
//	start_minute_0 = -60
//	end_minute_0 = +720
//	+12 , +24
//	start_minute_0 = +721
//	end_minute_0 = +1440
//
//	*/
//	$tmp="";
//	/* Start  Getting the information from the user */
////	if (len($flno2)>0 or len($flno3)>0) {
//		if (len($tmp)>0) { $tmp=$tmp." and ";}
//		$tmp= $tmp." (flno like '$flno2%' or flno like '$flno3%')";
//	}
//	
//	if (len($org)>0) {
//		if (len($tmp)>0) { $tmp=$tmp." and ";}
//		$tmp=$tmp." ( org3 ='$org' or via3='$org')";
//	}
//	
//	if (len($tmp)>0) { $tmp="and (". $tmp.")";}
//	
//	/*End  Getting the information from the user */
//
//	/* Create the query */
//	$fields=" stoa,flno,org3,via3,etai,tmoa,land,onbl,act3,regn,rwya,psta,paxt,blt1,remp,ROWNUM ";
//	$table=" CEDA.afttab ";
//
//	/* $wherest="  WHERE ((TIFA BETWEEN '$fromhour' AND '$tohour') AND (ADID = 'A' or ADID='B') AND (FLNO <> ' ')  AND (FTYP in ('J','S','G','B','A','C','O','D','E')) AND (TTYP <> ' ')) $tmp ORDER BY STOA,FLNO  "; */
//	// $wherest="  WHERE ((TIFA BETWEEN '$fromhour' AND '$tohour') AND (ADID = 'A' or ADID='B') AND (FLNO <> ' ')  AND (TTYP <> ' ')) $tmp ORDER BY STOA,FLNO  ";
//echo "where: $wherest<br>";
////	$cmdTempCommandTextCount = "SELECT Count(flno)  recordcount  from ". $table.$wherest;
//	$cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;
//
//	/* Count the returns (Hope we find  a solution for this one ) */	
//	$Count= &$db->Execute($cmdTempCommandTextCount);
//	$RecordCount=htmlspecialchars(trim($Count->fields[0]));
//
//	if ($Count) $Count->Close();
//
//	$rs = &$db->Execute($cmdTempCommandText );
//	/* Use The List.php file in order to return the results */
//	if ($rs) listpages($rs,$RecordCount,"  width=80%  bgcolor=\"#ffffff\"  border=\"1\" cellspacing=\"0\" cellpadding=\"0\"" );
//	else print "<b>Error in Execute of SELECT</b></p>";
//}

//function dep_hours ($fromhour , $tohour,$flno2='',$flno3='',$org='',$Sfromdate) {
//	Global $db;
//echo "JWE: flno2=$flno2;flno3=$flno3;org=$org<br>";
//	/*
//	-1 , +12
//	start_minute_0 = -60
//	end_minute_0 = +720
//	+12 , +24
//	start_minute_0 = +721
//	end_minute_0 = +1440
//
//	*/
//	$tmp="";
//	if (len($flno2)>0 or len($flno3)>0) {
//		if (len($tmp)>0) { $tmp=$tmp." and ";}
//		$tmp=$tmp. " (flno like '$flno2%' or flno like '$flno3%')";
//	}
//
//	
//	if (len($org)>0) {
//		if (len($tmp)>0) { $tmp=$tmp." and ";}
//		$tmp=$tmp." ( des3 ='$org' or via3='$org')";
//	}
//	
//echo "JWE: flno2=$flno2;flno3=$flno3;org=$org<br>";
//	if (len($tmp)>0) { $tmp="and (". $tmp.")";}
//	
//	$fields=" afttab.stod,afttab.flno,afttab.des3,via3,etdi,ofbl,airb,ckif,ckit,gtd1,wro1,pstd,act3,regn,rwyd,remp  ";
//	$table=" CEDA.afttab ";
//	/* $wherest="  WHERE ((TIFD BETWEEN '$fromhour' AND '$tohour') AND (ADID = 'D' or ADID = 'B') AND (FLNO <> ' ')  AND (FTYP in ('J','S','G','B','A','C','O','D','E')) AND (TTYP <> ' ')) ".$tmp." ORDER BY STOD,FLNO  "; */
//	// $wherest="  WHERE ((TIFD BETWEEN '$fromhour' AND '$tohour') AND (ADID = 'D' or ADID = 'B') AND (FLNO <> ' ')  AND (TTYP <> ' ')) ".$tmp." ORDER BY STOD,FLNO  ";
//echo "where: $wherest<br>";
//	$cmdTempCommandTextCount = "SELECT Count(*)  recordcount  from ". $table.$wherest;
//	$cmdTempCommandText = "select * from (SELECT ".$fields.",rownum  from ". $table.$wherest.")where rownum between 1 and 4" ;
//	$cmdTempCommandText = "SELECT ".$fields.",rownum  from ". $table.$wherest."" ;
	
//	$Count= &$db->Execute($cmdTempCommandTextCount);
//	$RecordCount=htmlspecialchars(trim($Count->fields[0]));
//	if ($Count) $Count->Close();
//	$rs = &$db->Execute($cmdTempCommandText );
//
//	if ($rs) listpages($rs,$RecordCount," BORDER=2 width=80%  bgcolor=\"#ffffff\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"" );
//	else print "<b>Error in Execute of SELECT</b></p>";
	
//}
	function IndexMenu() {
	Global $dbS;
		$tmpq="SELECT MENUNAME,TMPL_MENUID";
		$tmpq .=	" FROM  S_TMPL_MENUS";
		$tmpq .=	" ORDER by  ROWNUMBER";
	  	$cmdTempCommandText = $tmpq;
		$rs=$dbS->Execute($cmdTempCommandText);
		if (!$rs->EOF)  {
			while (!$rs->EOF) {
				echo "<tr>\n";
				echo "	<td  class=\"Frontmenu\">&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"inf1.php?TMPL_MENUID=".$rs->Fields("TMPL_MENUID")."\">".$rs->Fields("MENUNAME")."</a></td>\n";
				echo "</tr>\n";
				echo "<tr>\n";
				echo "	<td><img src=\"pics/dot.gif\" width=\"1\" height=\"10\" alt=\"\" border=\"0\"></td>\n";
				echo "</tr>\n";
				$rs->MoveNext();
			}
		  }// If End
    	if ($rs) $rs->Close();

}	
?>
