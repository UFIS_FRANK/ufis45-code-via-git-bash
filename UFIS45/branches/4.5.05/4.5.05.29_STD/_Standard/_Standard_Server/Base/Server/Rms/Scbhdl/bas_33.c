/*********************************************************/
/* GetValue33: Calculating the compensation working time */
/*********************************************************/
static double GetValue33(const char *pcpCheckBsdu,const char *pcpSday,const char *pcpLocalDrrStartTime,const char *pcpLocalDrrEndTime, const char *pcpLocalDrrBreakTime, const char* pcpIsCompensation)
{
	double	dlResultValue = 0;
	double	dlDrrMin;
	double	dlBreakMin;

	char	clVereinbarteWochenstunden[20];

	if (strcmp(pcpIsCompensation,"1") != 0)
	{
		dbg(DEBUG,"GetValue33(): No compensation found! Return 0!");
		return 0;
	}

	/* length of shift */
	dlDrrMin = GetDailyDRRMinutesByStaff(pcpLocalDrrBreakTime,pcpLocalDrrStartTime,pcpLocalDrrEndTime);

	/* break duration */
	dlBreakMin = GetDailyBreakMinutesByStaff(pcpCheckBsdu,pcpLocalDrrBreakTime,pcpSday);

	dbg(DEBUG,"GetValue33(): Shift minutes <dlDrrMin>: %lf",dlDrrMin);
	dbg(DEBUG,"GetValue33(): Break minutes <dlBreakMin>: %lf",dlBreakMin);

	/* check shift length */
	if (dlDrrMin == 0)
	{
		dbg(DEBUG,"GetValue33(): Using agreed weekly working hours.");
		if (ReadTemporaryField("VereinbarteWochenstunden",clVereinbarteWochenstunden) > 0)
		{
			dlDrrMin = atof(clVereinbarteWochenstunden);
		}
	}

	dbg(DEBUG,"GetValue33(): Result minutes: %lf",dlDrrMin + dlBreakMin);

	dlResultValue = (dlDrrMin + dlBreakMin) / 60;

	return dlResultValue;
}

/***********************************************************/
/****** Account 33: compensation hours for temporary staff */
/***********************************************************/
void bas_33()
{
	char	clLastCommand[6];
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clMonthIndex[4];
	char	clYearIndex[6];

	double	dlAccValue[12];
	char	clNewShiftUrno[12];
	char	clDrrBreakTime[16];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clNewDrrStatus[2];
	char	clOldDrrBreakTime[16];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clOldDrrStatus[2];
	char	clOldShiftUrno[12];

	char	clIsCompensation[2];
	char	clCl12Acc32[128];
	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	int	i;

	dbg(DEBUG, "============================= ACCOUNT 33 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 0 && clIsAushilfe[0] == '0')
	{
		dbg(TRACE,"Account 33: Stopped, because no temporary staff");
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 33: Command (%s)",clLastCommand);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 33: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 33: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clNewShiftUrno) == 0)
	{
		dbg(TRACE,"Account 33: Stopped, because <clNewShiftUrno> = undef!");
		return;
	}

	/* Status of the new recoed */
	if (ReadTemporaryField("NewRoss",clNewDrrStatus) == 0)
	{
		dbg(TRACE,"Account 33: <clNewDrrStatus> = undef!");
		clOldShiftUrno[0] = '\0';
	}
	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 33: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 33: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* break duration */
	if (ReadTemporaryField("NewSblu",clDrrBreakTime) <= 0)
	{
		dbg(TRACE, "Account 33: <clDrrBreakTime> = undef!");
		strcpy(clDrrBreakTime,"0");
	}

	if (strcmp(clLastCommand,"URT") == 0) 
	{
		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			strcpy(clOldDrrStartTime,"0");
		}

		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			strcpy(clOldDrrEndTime,"0");
		}

		/* old status */
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) < 0)
		{
			clOldDrrStatus[0] = '\0';
		}

		/* old break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrBreakTime) <= 0)
		{
			strcpy(clOldDrrBreakTime,"0");
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldShiftUrno) == 0)
		{
			strcpy(clOldShiftUrno,"0");
		}
	}

	/* calculate old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* take only active records */
		if (clOldDrrStatus[0] == 'A')
		{
			ReadTemporaryField("OldIsCompensation",clIsCompensation);
			dlOldValue = GetValue33(clOldShiftUrno,clSday,clOldDrrStartTime,clOldDrrEndTime,clOldDrrBreakTime,clIsCompensation);
		}
		else
		{
			dbg(TRACE, "Account 33: Wrong status of old DRR-record: <clOldDrrStatus> = <%s>" ,clOldDrrStatus);
		}
			
		dbg(DEBUG,"Account 33: Old Value <dlOldValue> = <%ld>" , dlOldValue);
	}

	/* calculate new value */
	dlNewValue = 0;
	if (clNewDrrStatus[0] == 'A')
	{
		ReadTemporaryField("NewIsCompensation",clIsCompensation);
		dlNewValue = GetValue33(clNewShiftUrno,clSday,clDrrStartTime,clDrrEndTime,clDrrBreakTime,clIsCompensation);
	}
	else
	{
		dbg(TRACE, "Account 33: Wrong status of new DRR-record: <clNewDrrStatus> = <%s>" ,clNewDrrStatus);
	}
	dbg(DEBUG,"Account 33: New Value <dlNewValue> = <%lf>" ,dlNewValue);

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue;
	}
	dbg(DEBUG,"Account 33: Result Value <dlResultValue> = <%lf>",dlResultValue);

	if (dlResultValue == 0)
	{
		dbg(DEBUG, "Account 33: Value not changed.");
		return;
	}

	/* get ACC record */
	strncpy(clYearIndex,&clSday[0],4);
	clYearIndex[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"33",clYearIndex,dlAccValue);
	strncpy(clMonthIndex,&clSday[4],2);
	clMonthIndex[2] = '\0';

	dbg(DEBUG,"Account 33: Month index <clMonthIndex>: <%s>",clMonthIndex);
	dbg(DEBUG,"Account 33: Old ACC value / month [%s] = <%lf>",clMonthIndex,dlAccValue[atoi(clMonthIndex)-1]);
	i = atoi(clMonthIndex);

	while  (i <= 12)
	{
		dlAccValue[i-1] = dlAccValue[i-1] + dlResultValue;
		dbg(DEBUG,"Account 33: New ACC value / month [%d] = <%lf>",i,dlAccValue[i-1]);
		i = i + 1;
	}

	/* save results */
	UpdateMultiAccountHandle("33",clStaffUrno,clYearIndex,"SCBHDL",dlAccValue);
	if (GetAccValueHandle("32","12",clYearIndex,clStaffUrno,"CL",clCl12Acc32))
	{
		dbg(DEBUG,"Account 33: CL-value account 32 = <%lf>",atof(clCl12Acc32));
		dbg(DEBUG,"Account 33: CL-value account 33 = <%lf>",dlAccValue[11]);
		dbg(DEBUG,"Account 33: Calling UpdateNextYearsValuesHandleAdjust() ...");
		UpdateNextYearsValuesHandleAdjust(clYearIndex,clStaffUrno,atof(clCl12Acc32),dlAccValue[11],"32","33");
	} 
}
