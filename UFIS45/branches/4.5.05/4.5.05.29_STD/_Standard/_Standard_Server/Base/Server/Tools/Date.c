#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Date.c 1.1 2003/03/19 17:03:33SGT fei Exp  $";
#endif /* _DEF_mks_version */
#include <stdio.h>
#include <time.h>

/*main(char* pcpTime,char* pcpSeparator,char* pcpFormat)*/
main(int argc,char *argv[])
{

  struct tm *_tm;
  time_t    now;
  int offset;


 if ( argc > 2 )
exit(0);
if(argc==2)
offset=atoi(argv[1])*86400;
else
offset=0;
  now = time(NULL);
  now = now+ offset;
  _tm = (struct tm *)localtime(&now);
fprintf(stdout,"%04d%02d%02d\n",_tm -> tm_year+1900,_tm -> tm_mon+1,_tm -> tm_mday);
  exit(0); 
}                                                        
