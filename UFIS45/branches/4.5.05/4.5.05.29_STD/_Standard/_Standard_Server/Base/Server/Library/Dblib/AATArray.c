#ifndef _DEF_mks_version_AATArray_c
#define _DEF_mks_version_AATArray_c
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version_AATArray_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/AATArray.c 1.56 2006/08/23 23:41:05SGT hag Exp  $";
#endif /* _DEF_mks_version */
/*******************************************************************************
 *
 * source-code-control-system version string
 */

/* be carefule with strftime or similar functions !!!
 *
 ******************************************************************************/
/*********************************************************************************/
/* 20010112: if no URNO given, check if selection begins with digits of the URNO */
/* 20010112: if URNO in FieldList and URNO of selection is NULL, then get URNO   */
/*           value from DataList (this happens with WHERE URNO=:VURNO)           */
/* 20010112: UnsetRowChanges as function                                         */
/* 20010115: UnsetRowChanges must be done in UpdateRowDB even when no new DB-Data*/
/* 20010115: 'rollback;' replaced by 'rollback();'                               */
/* 20010116: GetNextUrnoFromSelection                                            */
/* 20010129: UpdateEventData, DeleteEventData: pclUrno is filled with strcpy,    */
/*           don't check for NULL but for strlen(pclUrno)                        */
/* 20010131: mod_name und AATArray as parameter in tools_send_info_flag          */
/* 20010327: hag: don't use strcpy(target,GetNextUrnoFromSelection(....))        */
/* 20010329: dbg(TRACE,"UnsetRowChanges ..." ) -> /.* dbg() *./                  */
/* 20010410: FindField: Leerstring/NULL abgefangen                               */
/* 20010410: AATArrayPutField: memset(..,' '..) auf volle Laenge, anschliessend  */
/*           nochmal korrekt terminiert                                          */
/* 20010427: Debug-Ausgaben "Insert/Update/DeleterowDB follows" entfernt(HEB)    */
/* 20010705: EventUpdate: verarbeite jetzt auch letztes Element der Datenliste,  */
/*           wenn dieses leer ist  (MCU)                                         */
/* 20010705: CEDAArrayDeleteRow: loescht jetzt auch Rows, die eingefuegt und     */
/*           gleich wieder geloescht werden (ohne WriteDB zwischendrin) (HEB)    */
/* 20010912: CEDAArrayReadAllHeader: fprintf nur wenn FILEPointer != NULL        */
/* 20010924: sccs_version mit echtem Compile-Zeitstempel                         */
/* 20020708: commented some dbg() after commit_work                              */
/* 20020724: JIM: UpdateRowDB: moved ArraysReleaseActionInfo behind commit_work() */
/*           Note: for multiple updates this will work with ARR_COMMIT_TILL_ERR only*/
/*                 In other cases the ArraysReleaseActionInfo may be before commit!*/
/* 20030709 JIM: prevent alignment problems No 2                                 */
/* 20030825 JIM: ArraysReleaseActionInfo set TwEnd to "Hopo,tab,mod_name" */
/* 20030909 BST: Complete Redesign of IndexHandling (Create, Sort etc.) */
/* 20040310 JIM: 3x dbg(TRACE,"REORG INDEX..." commented                         */
/* 20040504 JIM: more debug if old index not found, PRF 6094                     */
/* 20040629 JIM: moved Filter check before double urno check                     */
/* 20040629 JIM: added double urno check by urno key                             */
/* 20040629 JIM: added CEDAArrayFillHint and CEDAArrayRefillHint to allow use of */
/*               Oracle DML Hints                                                */
/* 20040722 JIM: output of mks_version_AATArray_c instead of sccs_version        */
/*               sccs_version removed                                            */
/* 20040805 JIM: wrong usage of pclUrnoStr in InsertEventData, use pclUrno       */
/* 20040805 JIM: UpdateEventData: updating NEW data also, when already used      */
/* 20040805 JIM: some dbg in UpdateEventData                                     */
/* 20060609 AKL: some dbg set to DEBUG mode                                      */
/*********************************************************************************/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
extern FILE *outp;
extern char *mod_name;
/* 20021203 JIM: as long as MULTI HOPO is incomplete we will release HOPO 
   from systab: */
static char cgAntiMultyHopo[8] = "";
#ifndef CPP
#include "syslib.h"
#include "tools.h"
#include "fditools.h"
#include "libccstr.h"
#include "db_if.h"
#include "cedatime.h"
#endif /*  */
#include "AATArray.h"
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif /*  */
#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif /*  */
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])
#define MAXKEYITEMS      20
#define MAXERRORMESSAGES 30
#define DEFARRAYCOUNT    20     /* defaults */
#define DEFINDEXCOUNT    20
#define MARKINTERNARRAY  "C_"
#define FOR_INSERT        1
#define FOR_UPDATE        2
#define FOR_DELETE        3
#define ARR_CHAR_TYPE     1
#define ARR_VCHAR_TYPE    2
#define ARR_NUM_TYPE      3


typedef struct
{
    char Field[10];               /* Name of indexed Field     */
    long FieldItemNo;             /* FieldItemNo in DataRow    */
    long KeyValSort;              /* Coded Sort Direction      */
    long KeyValLen;               /* Length of Indexed Field   */
    long KeyValStart;             /* Offset in Index KeyValues */
}AA_INDEXKEY_DESCRIPTOR;

typedef struct 
{
    long  DataRowNum;   /* Index in DataArray */
    char *KeyValue;     /* Index Key Value(s) */
}AATINDEXROW;

typedef struct 
{
    AATINDEXROW *IndexRow;
}AA_INDEXWRAPPER;

typedef struct
{
    char Name[20];                /* name of index                               */
    char *TmpKeyNew;              /* Key-String for temporary use (update index) */
    char *TmpKeyOld;              /* Key-String for temporary use (update index) */
    char *TmpKeyCur;              /* Key-String for temporary use (update index) */
    enum enKeyType KeyType;       /* type of keyfield                            */
    long TotalKeyLen;             /* length of created key                       */
    AA_INDEXKEY_DESCRIPTOR IndexDescriptor[MAXKEYITEMS];
    long FirstHitIdxRow;          /* selected key (by FindKey, NextKey etc.)     */
    long LastHitIdxRow;           /* selected key (by FindKey, NextKey etc.)     */
    long CurrIndexRow;            /* selected key (by FindKey, NextKey etc.)     */
    long MaxIndexRows;            /* currently allocated rows                    */
    long UsedIndexRows;           /* currently free rows                         */
    long FreeIndexRows;           /* currently free rows                         */
    BOOL IsActive;                /* TRUE reorganize keyarray on all datachanges */
    AA_INDEXWRAPPER *IndexWrapper;/* array of pointers to index lines            */
}AATARRAYINDEX;

typedef struct
{
    long NextFree;                /* next free row in freelist */
    long SizeOrig;                /* size of data buffer currently allocated */
    long SizeNew;                 /* size of data buffer currently allocated */
    short RowStatus;              /* Used as Deletion Flag */
    enum enChangeFlag ChangeFlag; /* ARR_DATA_EMPTY,ARR_DATA_UNCHANGED,
				     ARR_DATA_CHANGED,ARR_DATA_NEW,
				     ARR_DATA_DELETED,ARR_DATANEW_DELETED */
    long *FieldOffsetsOrig;       /* array containing offsets to all fields in row */
    long *FieldOffsetsNew;        /* array containing offsets to all fields in row */
    char *DataOrig;               /* original data read from database, new inserted
				     rows (no DB) become flag ARR_DATA_NEW  */
    char *DataNew;                /* new or changed data */
}AATARRAYROW;

typedef struct
{
    char Name[20];                /* name of array (for CEDA name of table) MAX 18! */
    char TableName[20];           /* Name of DB Table */
    char TimeStamp[16];           /* Time of last Flush  */
    char RouteIds[32];            /* Destinations to distribute events  */
    AATARRAYINDEX *UrnoIdxHandle; /* Handle (Index) of internal Index on URNO */
    BOOL ReorgPending;            /* Indexes not Up-To-Date */
    BOOL DataLoaded;              /* Records fetched from DB table */
    BOOL ActionUpdates;           /* Flag for EventUpdates */
    BOOL Filter;                  /* is there an internal filtering in array (none ORACLE) */
    BOOL SendToBchdl;             /* Send changes written to DB to broadcast handler? */
    BOOL SendToAction;            /* Send changes written to DB to action handler?    */
    BOOL SendToLoghdl;            /* Send changes written to DB to Loghdl handler?    */
    BOOL LoggingChecked;          /* Send changes written to DB to Loghdl handler?    */
    BOOL SelectDistinct;          /* If TRUE than add distinct in front of fieldlist */
    char FilterStr[ARR_MAXFILTERLEN + 1]; /* internal filter string ( > < = )  */
    long RowLen;                  /* size of row, 0 for variable row size */
    long FieldCount;              /* number of fields in field list (only if Size==0) */
    long *FieldLen;               /* array containing length of each field */
    char *FieldList;              /* kommaseperated list of fields in row */
    long *FieldTyps;              /* Fieldtypes (FITY in SYSTAB)(N,C,VC2) in row */
    long *FieldOffsets;           /* array containing offsets to all fields in row */
    long UrnoIdx;
    long CdatIdx;
    long LstuIdx;
    long UseuIdx;
    long SqlFieldCount;           /* number of table fields in field list */
    char *SqlFieldList;           /* kommaseperated list of table fields in row */
    char *EmptyDataLine;
    char *SqlCmdIrt;
    char *SqlCmdUrt;
    char *SqlCmdDrt;
    short SqlCsrIrt;
    short SqlCsrUrt;
    short SqlCsrDrt;
    long MaxRows;                 /* currently allocated rows */
    long Extent;                  /* rows to allocate if MaxRows reached */
    long RowCount;                /* count of used rows */
    long FirstFree;               /* first free row in freelist */
    long CurrentRow;              /* actual selected row */
    AATARRAYINDEX *KeyArray;      /* array of index-arrays */
    long KeyArrayCount;           /* size of KeyArray */
    AATARRAYROW *Rows;            /* array of pointers to rows */
}AATARRAY;

/* structs for two dimensional filtering */
typedef struct
{
    char rcSubFilterStr[ARR_MAXFILTERLEN + 1];
    int riSubOperand;
    int riResult;
}SUBFILTER;

typedef struct
{
    char rcMainFilterStr[ARR_MAXFILTERLEN + 1];
    int riMainOperand;
    int riNumSubFilter;
    int riResult;
    SUBFILTER rrSubFilter[10];
}MAINFILTER;

/*  GLOBAL VARIABLES  */
AATARRAYINDEX *CurIndexObject = NULL;
AAT_PARAM *prgMyArgDesc = NULL;
REC_DESC rgSqlRecDesc;

char *pcgMultiBuffer = NULL;    /* buffer for store key               */
long *plgMultiLineNum = NULL;   /* buffer for store line number       */
long lgMultiLines = 0;          /* number of lines in buffer          */
long lgOneRowDataLen = 0;
char *pcgOneRowData = NULL;     /* buffer for storing one rows data   */
long lgOneFldDataLen = 0;
char *pcgOneFldData = NULL;     /* buffer for storing one fields data */
long lgCedaFldCnt = 6;          /* for creation of store array        */
char pcgCedaFldList[200] = "ANam,AHdl,TNam,Sele,FldL,AddL";
long plgCedaFldLens[7] = { 20, 10, 6, ARR_MAXSELECTLEN,
			   ARR_MAXFLDLSTLEN,
			   ARR_MAXFLDLSTLEN
};
AATARRAY rgBaseArray = {
    0
};                              /* array for store all arrays         */
long lgDefIndexCount = DEFINDEXCOUNT;
long lgDefArrayCount = DEFARRAYCOUNT;
char pcgSave[2048];             /* temporary buffer for sorting       */

/*   FOR LOGHDL  */
long lgCmdLstLen = 0;         /* commands for all Rows */
long lgUpdSelLstLen = 0;      /* Update Selection list for all Rows */
long lgUpdFldLstLen = 0;      /* Update Field list for all Rows     */
long lgUpdDatLstLen = 0;      /* Update Data List for all Rows      */


char *pcgCmdLst = NULL;         /* commands for all Rows */
long lgCmdLineCnt = 0;
char *pcgUpdSelLst = NULL;      /* Update Selection list for all Rows */
long lgUpdSelLineCnt = 0;
char *pcgUpdFldLst = NULL;      /* Update Field list for all Rows     */
long lgUpdFldLineCnt = 0;
char *pcgUpdDatLst = NULL;      /* Update Data List for all Rows      */
long lgUpdDatLineCnt = 0;
long lgChkDifCnt = 0;
int igActionId = 0;
int igBchdlId = 0;
int igLoghdlId = 7700;
int igWholeRowUpd = FALSE;      /* instead of new parameter for updates   */
long lgRefillRow = -9999;       /* instead of new parameter for fillArray */

char pcgInsertFieldBuf[ARR_MAXSQLFLDLSTLEN + 4]; /* for sql fieldlist format */
char pcgInsertSqlBuf[ARR_MAXSQLFLDLSTLEN + 50]; /* for INSERT, DELETE, UPDATE */
/* just for internal use at refill */
char *pcgNotFound = "RC_NOTFOUND";
char pcgErrorMessages[MAXERRORMESSAGES + 1][24] = { "RC_SUCCESS", "RC_FAILURE",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR", "RC_INVALID",
						    "RC_USED", "RC_DELETED",
						    "RC_DUPLICATE", "RC_NOMEM",
						    "RC_NOTINITIALIZED",
						    "RC_NODATA", "RC_ROWDELETED",
						    "UNKNOWN ERROR",
						    "UNKNOWN ERROR"
};
static AATARRAYROW *GetNewRow (AATARRAY * prpArray, long *plpIndex);
static int AddRow (AATARRAY * prpArray, void *pvpData, HANDLE * plpIndex);
static int DeleteRow (AATARRAY * prpArray, long lpRowNum, BOOL bpReorg);
static int InitializeFreeList (AATARRAY * prpArray);
static int InitializeRows (AATARRAY * prpArray, long lpStart, long lpEnd);
static int CalculateFieldOffsets (AATARRAY * prpArray, long *plpFieldOffsets, long *plpNewSize, void *pvpData);
static int FindArray (HANDLE * pspArray, char *pcpArrayName, AATARRAY ** prpArray);
static HANDLE SearchArray (char *pcpArrayName);
static long FindField (char *pcpFieldList, char *pcpFieldName);
static int GetField (AATARRAY * prpArray, AATARRAYROW * prpRow, long lpFieldNumber, long lpMaxBytes, void *pvpDataBuf);
static int PutField (AATARRAY * prpArray, AATARRAYROW * prpRow, BOOL bpNewData, long lpFieldNumber, void *pvpDataBuf);
static int FindRow (AATARRAY * prpArray, long *plpRowNum, BOOL bpFindAll);
static int GetNewIndex (AATARRAY * prpArray, char *pcpIndexName, AATARRAYINDEX ** prpIndex, HANDLE * plpHandle);
static int FindIndex (AATARRAY * prpArray, HANDLE * pspIndex, char *pcpIndexName, AATARRAYINDEX ** prpIndex);
static int CreateIndexWrapper (AATARRAY * prpArray, AATARRAYINDEX * prpIndex);
static int CreateKey (AATARRAY * prpArray, AATARRAYINDEX * prpIndex, char *pcpData, char *pcpKey);
static int InsertIndexKey (AATARRAYINDEX * prpIndex, char *pcpKey, long lpIndex);
static int DeleteIndexKey (AATARRAYINDEX * prpIndex, char *pcpKey, long lpRowNum);
static int UpdateIndexKey (AATARRAYINDEX * prpIndex, char *pcpOldKey, char *pcpNewKey, long lpRowNum);
static int CompareIndex(const void *_dp1, const void *_dp2);

static int BCompare (AATARRAYINDEX * prpIndex, char *pcpKey1, char *pcpKey2);
static int GetBSearchString (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexNam,
                             char *pvpKey);
static int BFindIndex (AATARRAYINDEX * prpIndex, void *pvpKey, long *plpFirstRow, long *plpLastRow, long lpLookRow,BOOL bpLookOnly);
static void TWETrimRight (char *s);
static long TWEDel2NewDel (char *pcpDelList, char pcpDel, char pcpNewDel);
static long TWECountDelimiter (char *pcpChkStr, char *pcpDel);
static int TWEBuildSqlFieldStr (char *pcpFldLst, char *pcpAddLst, int ipTyp, char *pcpSqlStrg);
static int GetCedaArrFlds (char *pcpArrayName, char *pcpTabNam, char *pcpSelect, char *pcpFldLst, char *pcpAddFldLst);
static int ArrayCreateIntern (HANDLE * pspHandle, char *pcpArrayName, char *pcpTabNam, char *pcpSelect,
                              char *pcpAddFldLst, long *plpAddFldLens, char *pcpFldLst, long *plpFldLens,
                              long *plpFldOffsets, long lpInitialNumRecords);
static int CreateStoreArray (char *pcpArrayName, char *pcpOneRecBuf);

static int TWEExtractField (char *pcpFieldName, char *pcpFields, char *pcpDatablk, char *pcpDel, char *pcpFieldData);
static int AllocInternalBuffers (long lpFldCount, long *plpFieldLens);
static int CEDAArrayWriteArray2DB (HANDLE * pspHandle, char *pcpArrayName, char **pcpCmdLst, char **pcpUpdSelLst,
                                   char **pcpUpdFldLst, char **pcpUpdDatLst, long *plpNumUrnos, long lpCommitFlag);
static int InsertRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, char *pcpTabNam, char *pcpFldLst, short *pspCursor,
                        long lpCommitFlag);
static int DeleteRowDB (HANDLE * pspHandle, AATARRAY * prpArray, long lpRowNum, char *pcpTabNam, short *pspCursor,
                        long lpCommitFlag, char *pcpFldLst);
static int UpdateRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, long lpRowNum, char *pcpTabNam, char *pcpFldLst,short *pspCursor, long lpCommitFlag, long *plpUpdRows, char *pcpOneUpdFldLst,char *pcpOneUpdDatLst);
static int UpdateWholeRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, char *pcpTabNam, char *pcpFldLst,
                             short *pspCursor, long lpCommitFlag);
static int ArrRec2DB (char *pcpData, char *pcpSqlStr, short *pspCursor);

static int ParseOneFilter (char *pcpFilterStr, char *pcpFldLst, char *pcpData);
static int ParseMultiFilter (char *pcpFilterStr, char *pcpFldLst, char *pcpData);
static void SetOperand (char *pcpOperand, int *pipOperand);
static void GetOperand (int ipOperand, char *pcpOperand);
static int ArraysReleaseActionInfo (char *pcpRoute, char *pcpTbl, char *pcpCmd, char *pcpUrnoList, char *pcpSel,
                                    char *pcpFld, char *pcpDat, char *pcpOldDat);

static int StringPut(char *pcpDest,char *pcpSrc1,char *pcpSrc2);
static int ReorgIndexes(AATARRAY *prpArray);
static void TraceIndex(AATARRAY *prpArray, AATARRAYINDEX *prpIndex);

static int CreateSqlProperties(AATARRAY *prpArray);
static void CreateOutRoutes(AATARRAY *prpArray);
static void GetUrnoProperty(AATARRAY *prpArray, AATARRAYROW *prpRow, char *pcpUrnoStr, char *pcpUrnoKey);
static void BuildSqlCmdStrg (char *pcpSql, char *pcpTbl, char *pcpFld, long lpCnt, int ipTyp);
static int AATArrayFlushArray(HANDLE *pspHandle, char *pcpArrayName, AAT_PARAM *prpArgDesc, long lpCommitFlag, int ipCheckAll);
static int HandleRowInsert(AATARRAY *prpArray, AATARRAYROW *prpRow);
static int HandleRowUpdate(AATARRAY *prpArray, AATARRAYROW *prpRow);
static int HandleRowDelete(AATARRAY *prpArray, AATARRAYROW *prpRow);
static int CheckRowDataChanges(AATARRAY *prpArray, AATARRAYROW *prpRow);

static void AppStrgToList(int ipListIdx, char *pcpStrg, long lpCount);
static void AppStrgToListPlus(int ipListIdx, char *pcpStrg, long lpCount, char *pcpSep, char *pcpStrg2);
static void GetOldStyleCfg(int *pipOldStyle, int *pipCheckAll);
static void CheckTableLogging(AATARRAY *prpArray);

/******************************************************************************/
/*  External Functions                                                        */
/******************************************************************************/
extern int StrgPutStrg (char *, int *, char *, int, int, char *);
extern int BuildItemBuffer (char *pcpList, char *pcpFields, long lpNumFields, char *pcpDelimiter);

/******************************************************************************/
/*  Local Functions                                                           */
/******************************************************************************/
/***********************************************************************************/
/****** UnsetRowChanges: Unset the array data changes of records already    ********/
/******                  written to DB                                      ********/
/****** Parameter:   prpRow:  row to unset changes                          ********/
/******              pcpArray: array to unset changes of row                ********/
/******                                                                     ********/
/****** Returnvalue: void                                                   ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
void UnsetRowChanges (AATARRAYROW * prpRow, AATARRAY * pcpArray)
{
    if (prpRow->DataNew != NULL)
    {
	memcpy (prpRow->DataOrig, prpRow->DataNew, pcpArray->RowLen);
	free (prpRow->DataNew);
	prpRow->DataNew = NULL;
    }
	prpRow->ChangeFlag = ARR_DATA_UNCHANGED;
}

/*********************************************************************/
/*                                                                   */
/* GetNextUrnoFromSelection:                                         */
/*         This function gets the next URNO from the WHERE Clause    */
/* IN:     pcpSel: WHERE Clause                                      */
/* IN/OUT: pcpNxt: pointer behind the end of next URNO (for next run)*/
/* RETURN:         pointer to a STATIC copy of the next URNO         */
/*                 The next call to GetNextUrnoFromSelection         */
/*                 uses the same STATIC buffer!                      */
/*                                                                   */
/* !!! pcpNxt must be a pointer initialized to NULL at first call !!!*/
/* !!! and should not be used by user                             !!!*/
/*                                                                   */
/* The Function GetNextUrnoFromSelection scans WHERE clauses like    */
/*  "WHERE URNO = 123"                                               */
/*  "WHERE URNO = '123'"                                             */
/*  "WHERE URNO IN (567,678,789)"                                    */
/*  "WHERE URNO IN ( '567' , '678','789' )"                          */
/*  "123"                                                            */
/*  "'234'"                                                          */
/* The Parameter pcpNxt is set to the end of the URNO just copied.   */
/* In case of "WHERE URNO IN ..." this is the place, where the next  */
/* URNO may start. When in the next call the Variable pcpNxt already */
/* is set, the Function searches from here. In case of "WHERE URNO ="*/
/* the the second search will result to NULL                         */
/*                                                                   */
/* Example:                                                          */
/* void TestWhereClause(char *pcpSel)                                */
/* {                                                                 */
/*   void *pcvNxt = NULL;                                            */
/*   char *pclU = NULL;                                              */
/*                                                                   */
/*   pcvNxt = NULL;    <-- here doubled, but don't forget!           */
/*   printf("TestWhereClause: START with Selection: <%s>\n",pcpSel); */
/*   while ((pclU=GetNextUrnoFromSelection(pcpSel,&pcvNxt))!= NULL)  */
/*   {                                                               */
/*     printf("Selection: <%s>, Urno: <%s>\n",pcpSel, pclUrno);      */
/*   }                                                               */
/* }                                                                 */
/* TestWhereClause("WHERE URNO IN ( '567' , '678','789' )");         */
/*********************************************************************/
void *GetNextUrnoFromSelection (char *pcpSel, void **pcpNxt)
{
    static char pcsRetUrno[32] = ""; /* static! init only done at process startup! */
    char *pclItem = NULL;
    char *pclFieldStart = NULL;
    int ilLen = 0;
    int ilCnt = 0;
    int ilCnt2 = 0;
    char pclUrnoStr[32] = "";
    char pclUrno[32] = "";
    if (*pcpNxt == NULL)
    {                             /* check where clause and data field */
	pclItem = strstr (pcpSel, "URNO=");
	if (pclItem != NULL)
	{
	    pclItem += 5;
	}

	else
	{
	    pclItem = strstr (pcpSel, "URNO =");
	    if (pclItem != NULL)
	    {
		pclItem += 6;
	    }

	    else
	    {
		pclItem = strstr (pcpSel, "URNO IN");
		if (pclItem != NULL)
		{
		    pclItem += 7;
		    ilLen = strcspn (pclItem, "(");
		    pclItem += ilLen + 1;
		}

		else
		{
		    pclItem = pcpSel;
		}
	    }
	}
    }

    else
    {                             /* second,third,... call in case of URNO IN */
	pclItem = *pcpNxt;
    }
    if (pclItem != NULL)
    {                             /* get the (first) number */
	while ((*pclItem == ' ') || (*pclItem == ','))
	{
	    pclItem++;
	}
	if (*pclItem != ')')
	{
	    ilLen = strspn (pclItem, " '1234567890");
	    *pcpNxt = pclItem + ilLen;
	    strncpy (pclUrnoStr, pclItem, ilLen);
	    pclUrnoStr[ilLen] = 0x00;
	    tool_filter_spaces (pclUrnoStr);
	    ilLen = strlen (pclUrnoStr);
	    ilCnt = 0;
	    ilCnt2 = 0;
	    pclFieldStart = pclUrnoStr;
	    while (ilCnt < ilLen)
	    {
		if (*pclFieldStart != '\'')
		{
		    pclUrnoStr[ilCnt2] = *pclFieldStart;
		    ilCnt2++;
		}
		ilCnt++;
		pclFieldStart++;
	    }
	    pclUrnoStr[ilCnt2] = 0x00;
	    strcpy (pclUrno, pclUrnoStr);
	}

	/* not ) reached */
	else
	{

	    /* end */
	}
    }
    if (strlen (pclUrno) > 0)
    {
	strcpy (pcsRetUrno, pclUrno);
	return &pcsRetUrno;
    }

    else
    {
	*pcpNxt = NULL;
	return NULL;
    }
}                               /* end of GetNextUrnoFromSelection */

/*********************************************************************/
int AATArrayInitialize (long lpArrayCount, long lpIndexCount)
{
    int ilRc = RC_SUCCESS;
    long llMaxRows = 0;
    dbg (TRACE, "AATArrayInitialize: %s", mks_version_AATArray_c);
    if (rgBaseArray.Rows != NULL)
    {
	dbg (TRACE, "AATArrayInitialize called twice: old <%ld,%ld> new <%ld,%ld>", lgDefArrayCount, lgDefIndexCount,
	     lpArrayCount, lpIndexCount);
	if (lpArrayCount > rgBaseArray.MaxRows)
	{
	    void *pvlRows;
	    dbg (TRACE, "REALLOC BaseArray (Initialize for %d Arrays)", lpArrayCount);
	    llMaxRows = lpArrayCount;
	    pvlRows = realloc (rgBaseArray.Rows, sizeof (AATARRAYROW) * (llMaxRows));
	    if (pvlRows == NULL)
	    {
		ilRc = RC_NOMEM;
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		rgBaseArray.Rows = (AATARRAYROW *) pvlRows;
		InitializeRows (&rgBaseArray, rgBaseArray.MaxRows, llMaxRows);
		rgBaseArray.MaxRows = llMaxRows;
		InitializeFreeList (&rgBaseArray);
		lgDefArrayCount = lpArrayCount;
	    }
	}

	else
	{
	    dbg (TRACE, "Initialize IGNORED");
	}
    }

    else
    {
	dbg (TRACE, "Initialize IGNORED");
	if (lpArrayCount > 0)
	{
	    lgDefArrayCount = lpArrayCount;
	}
	if (lpIndexCount > 0)
	{
	    lgDefIndexCount = lpIndexCount;
	}
	rgBaseArray.Rows = (AATARRAYROW *) malloc (sizeof (AATARRAYROW) * lgDefArrayCount);
	if (rgBaseArray.Rows == NULL)
	{
	    ilRc = RC_NOMEM;
	}

	else
	{
	    strcpy (rgBaseArray.Name, "AATBASEARR");
	    rgBaseArray.RowLen = sizeof (AATARRAY);
	    rgBaseArray.MaxRows = lgDefArrayCount;
	    rgBaseArray.RowCount = 0;
	    rgBaseArray.CurrentRow = -1;
	    rgBaseArray.KeyArray = NULL;
	    rgBaseArray.KeyArrayCount = 0;
	    InitializeRows (&rgBaseArray, 0, rgBaseArray.MaxRows);
	    InitializeFreeList (&rgBaseArray);
	}
    }
    return ilRc;
}

int AATArrayCreate (HANDLE * pspHandle, char *pcpArrayName, BOOL bpFixLen, long lpInitialCount, long lpFieldCount,
                    long *plpFieldLen, char *pcpFieldList, char *pcpFieldTypes)
{
    int ilRc = RC_SUCCESS;
    *pspHandle = -1;
    if (rgBaseArray.Rows == NULL)
    {
	ilRc = RC_NOTINITIALIZED;
    }
    if (ilRc == RC_SUCCESS)
    {
	if (strlen (pcpArrayName) > 19)
	{
	    ilRc = RC_INVALID;
	}
	if (ilRc == RC_SUCCESS)
	{
	    if (pcpArrayName == NULL)
	    {
		ilRc = RC_INVALID;
	    }
	}
	if (ilRc == RC_SUCCESS)
	{
	    if (SearchArray (pcpArrayName) > -1)
	    {
		ilRc = RC_DUPLICATE;
	    }
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	ilRc = AllocInternalBuffers (lpFieldCount, plpFieldLen);
    }
    if (ilRc == RC_SUCCESS)
    {
	AATARRAY *prlTmpArray;
	prlTmpArray = (AATARRAY *) malloc (sizeof (AATARRAY) + 4);
	if (prlTmpArray == NULL)
	{
	    ilRc = RC_NOMEM;
	}

	else
	{
	    prlTmpArray->TableName[0] = 0x00;
	    prlTmpArray->DataLoaded = FALSE;
	    prlTmpArray->ActionUpdates = FALSE;
	    prlTmpArray->UrnoIdxHandle = NULL;
	    prlTmpArray->CurrentRow = -1;
	    prlTmpArray->RowLen = 0;
	    prlTmpArray->FieldCount = lpFieldCount > 0 ? lpFieldCount : (strlen (pcpFieldList) / 6) + 1;
	    prlTmpArray->MaxRows = lpInitialCount;
	    prlTmpArray->Extent = lpInitialCount;
	    prlTmpArray->RowCount = 0;
	    prlTmpArray->FirstFree = -1;
	    prlTmpArray->KeyArray = NULL;
	    prlTmpArray->Filter = FALSE;
	    prlTmpArray->SendToBchdl = FALSE;
	    prlTmpArray->SendToAction = FALSE;
	    prlTmpArray->SendToLoghdl = FALSE;
	    prlTmpArray->SelectDistinct = FALSE;
	    prlTmpArray->LoggingChecked = FALSE;
	    strcpy (prlTmpArray->FilterStr, "");
	    strcpy (prlTmpArray->Name, pcpArrayName);
	    prlTmpArray->Rows = calloc (sizeof (AATARRAYROW), lpInitialCount + 1);
	    if (prlTmpArray->Rows == NULL)
	    {
		ilRc = RC_NOMEM;
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		prlTmpArray->FieldList = malloc (strlen (pcpFieldList) + 4);
		if (prlTmpArray->FieldList == NULL)
		{
		    ilRc = RC_NOMEM;
		    free (prlTmpArray->Rows);
		    prlTmpArray->Rows = NULL;
		}
		else
		{
		    strcpy (prlTmpArray->FieldList, pcpFieldList);
		}
		prlTmpArray->SqlFieldList = malloc (strlen (pcpFieldList) + 4);
		if (prlTmpArray->SqlFieldList == NULL)
		{
		    ilRc = RC_NOMEM;
		    free (prlTmpArray->Rows);
		    prlTmpArray->Rows = NULL;
		}
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		prlTmpArray->FieldTyps = calloc (sizeof (long), prlTmpArray->FieldCount + 4);
		if (prlTmpArray->FieldTyps == NULL)
		{
		    ilRc = RC_NOMEM;
		    free (prlTmpArray->Rows);
		    prlTmpArray->Rows = NULL;
		}

		else
		{
		    long llFieldNo;
		    char pclType[10];
		    if (pcpFieldTypes != NULL)
		    {
			for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount; llFieldNo++)
			{
			    GetDataItem (pclType, pcpFieldTypes, llFieldNo + 1, ',', "\0", "\0\0");
			    if (pclType[0] == 'N')
			    {
				prlTmpArray->FieldTyps[llFieldNo] = ARR_NUM_TYPE;
			    }

			    else
			    {
				if (pclType[0] == 'C')
				{
				    prlTmpArray->FieldTyps[llFieldNo] = ARR_CHAR_TYPE;
				}

				else
				{
				    prlTmpArray->FieldTyps[llFieldNo] = ARR_VCHAR_TYPE;
				}
			    }
			}                   /* end for */
		    }

		    /* end if != NULL */
		    else
		    {
			for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount; llFieldNo++)
			{
			    prlTmpArray->FieldTyps[llFieldNo] = ARR_CHAR_TYPE;
			}
		    }
		}
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		prlTmpArray->FieldLen = calloc (sizeof (long), prlTmpArray->FieldCount + 4);
		if (prlTmpArray->FieldLen == NULL)
		{
		    ilRc = RC_NOMEM;
		    free (prlTmpArray->Rows);
		    prlTmpArray->Rows = NULL;
		    free (prlTmpArray->FieldList);
		    prlTmpArray->FieldList = NULL;
		    free (prlTmpArray->FieldTyps);
		    prlTmpArray->FieldTyps = NULL;
		}

		else
		{
		    long llFieldNo;
		    for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount; llFieldNo++)
		    {
			prlTmpArray->FieldLen[llFieldNo] = plpFieldLen[llFieldNo];
		    }
		}
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		if (bpFixLen)
		{
		    prlTmpArray->FieldOffsets = calloc (sizeof (long), prlTmpArray->FieldCount + 4);
		    if (prlTmpArray->FieldOffsets == NULL)
		    {
			ilRc = RC_NOMEM;
			free (prlTmpArray->Rows);
			prlTmpArray->Rows = NULL;
			free (prlTmpArray->FieldList);
			prlTmpArray->FieldList = NULL;
			free (prlTmpArray->FieldTyps);
			prlTmpArray->FieldTyps = NULL;
			free (prlTmpArray->FieldLen);
			prlTmpArray->FieldLen = NULL;
		    }

		    else
		    {
			long llFieldNo;
			long llFieldOffset = 0;
			for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount; llFieldNo++)
			{
			    prlTmpArray->FieldOffsets[llFieldNo] = llFieldOffset;
			    llFieldOffset += prlTmpArray->FieldLen[llFieldNo];
			    llFieldOffset++;  /* add space for delimiter ('\0') */
			    prlTmpArray->RowLen += prlTmpArray->FieldLen[llFieldNo] + 1;
			}
			prlTmpArray->RowLen += 1;
		    }
		}

		else
		{
		    prlTmpArray->FieldOffsets = NULL;
		}
	    }
	}
	if (ilRc == RC_SUCCESS)
	{
	    ilRc = AddRow (&rgBaseArray, prlTmpArray, pspHandle);
	    if (ilRc == RC_SUCCESS)
	    {
		InitializeRows (prlTmpArray, 0, prlTmpArray->MaxRows);
		InitializeFreeList (prlTmpArray);
	    }
	}
    }
    return ilRc;
}

int AATArrayDestroy (HANDLE * pspArray, char *pcpArrayName)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    long llRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
   	prlTmpArray->TableName[0] = 0x00;
	prlTmpArray->DataLoaded = FALSE;
	prlTmpArray->ActionUpdates = FALSE;
	prlTmpArray->UrnoIdxHandle = NULL;
	for (llRowNum = 0; llRowNum < prlTmpArray->MaxRows; llRowNum++)
	{
	    prlTmpRow = &prlTmpArray->Rows[llRowNum];
	    if (prlTmpRow->DataNew != NULL)
	    {
		free (prlTmpRow->DataNew);
		prlTmpRow->DataNew = NULL;
	    }
	    if (prlTmpRow->DataOrig != NULL)
	    {
		free (prlTmpRow->DataOrig);
		prlTmpRow->DataOrig = NULL;
	    }
	    if (prlTmpRow->FieldOffsetsOrig != NULL)
	    {
		free (prlTmpRow->FieldOffsetsOrig);
		prlTmpRow->FieldOffsetsOrig = NULL;
	    }
	    if (prlTmpRow->FieldOffsetsNew != NULL)
	    {
		free (prlTmpRow->FieldOffsetsNew);
		prlTmpRow->FieldOffsetsNew = NULL;
	    }
	}
	if (prlTmpArray->KeyArray != NULL)
	{
	    for (llRowNum = 0; llRowNum < prlTmpArray->KeyArrayCount; llRowNum++)
	    {
		AATARRAYINDEX * prlTmpIndex;
		AATINDEXROW *prlTmpIndexRow;
		long llLineNo;
		prlTmpIndex = &prlTmpArray->KeyArray[llRowNum];
		if (prlTmpIndex->IndexWrapper != NULL)
		{
		    llLineNo = 0;
		    while ((prlTmpIndex->IndexWrapper[llLineNo].IndexRow != NULL) && 
			   (llLineNo < prlTmpIndex->MaxIndexRows))
		    {
			prlTmpIndexRow = prlTmpIndex->IndexWrapper[llLineNo].IndexRow;
			free(prlTmpIndexRow->KeyValue);
			prlTmpIndexRow->KeyValue = NULL;
			free(prlTmpIndexRow);
			prlTmpIndex->IndexWrapper[llLineNo].IndexRow = NULL;
			llLineNo++;
		    }
		    free(prlTmpIndex->IndexWrapper);
		    prlTmpIndex->IndexWrapper = NULL;
		}
	    }
	    free (prlTmpArray->KeyArray);
	    prlTmpArray->KeyArray = NULL;
	}
	if (prlTmpArray->FieldLen != NULL)
	{
	    free (prlTmpArray->FieldLen);
	    prlTmpArray->FieldLen = NULL;
	}
	if (prlTmpArray->FieldList != NULL)
	{
	    free (prlTmpArray->FieldList);
	    prlTmpArray->FieldList = NULL;
	}
	if (prlTmpArray->FieldTyps != NULL)
	{
	    free (prlTmpArray->FieldTyps);
	    prlTmpArray->FieldTyps = NULL;
	}
	if (prlTmpArray->FieldOffsets != NULL)
	{
	    free (prlTmpArray->FieldOffsets);
	    prlTmpArray->FieldOffsets = NULL;
	}
	if (prlTmpArray->Rows != NULL)
	{
	    free (prlTmpArray->Rows);
	    prlTmpArray->Rows = NULL;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	llRowNum = (long) (*pspArray);
	ilRc = DeleteRow (&rgBaseArray, llRowNum,TRUE);
	if (ilRc == RC_SUCCESS)
	{
	    rgBaseArray.RowCount--;
	}
    }
    return ilRc;
}

int AATArrayInternCreateIndex (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexName,
                               char *pcpKeyFldList, long *lpDirection, BOOL bpMultiFlag)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    long llItemLen;
    long llKeyStart;
    long llIdxLine = 0;
    /* dbg (TRACE, "AATArrayInternCreateIndex"); */
    dbg(TRACE,"CREATE INDEX <%s> ON <%s> FLD <%s>",pcpIndexName,pcpArrayName,pcpKeyFldList);
    *pspIndexHandle = -1;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYINDEX *prlTmpIndex;
	ilRc = GetNewIndex (prlTmpArray, pcpIndexName, &prlTmpIndex, pspIndexHandle);
	if (ilRc == RC_SUCCESS)
	{
	    char pclFieldName[24];
	    char *pclFieldStart;
	    long llFieldNo = 0;
	    strcpy (prlTmpIndex->Name, pcpIndexName);
	    prlTmpIndex->KeyType = ARR_LKEY_ITEM;
	    prlTmpIndex->TotalKeyLen = 0;
	    for (llFieldNo = 0; llFieldNo < MAXKEYITEMS; llFieldNo++)
	    {
		prlTmpIndex->IndexDescriptor[llFieldNo].FieldItemNo = -1;
		prlTmpIndex->IndexDescriptor[llFieldNo].KeyValSort = ARR_ASC;
	    }
	    llFieldNo = 0;
	    llKeyStart = 0;
	    pclFieldStart = pcpKeyFldList;

	    do
	    {
		llItemLen = GetNextDataItem (pclFieldName, &pclFieldStart, ",", "\0", "\0\0");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "AATArrayInternCreateIndex: Fkt.: GetNextDataItem (%d) failed", llItemLen);
		}
		if (*pclFieldName != '\0')
		{
		    long llFieldNumber;
		    llFieldNumber = FindField (prlTmpArray->FieldList, pclFieldName);
		    if (llFieldNumber == -1)
		    {
			ilRc = RC_INVALID;
		    }
		    else
		    {
			prlTmpIndex->IndexDescriptor[llFieldNo].FieldItemNo = llFieldNumber;
			prlTmpIndex->IndexDescriptor[llFieldNo].KeyValLen = prlTmpArray->FieldLen[llFieldNumber];
			prlTmpIndex->IndexDescriptor[llFieldNo].KeyValStart = llKeyStart;
			llKeyStart += prlTmpIndex->IndexDescriptor[llFieldNo].KeyValLen;
			prlTmpIndex->IndexDescriptor[llFieldNo].KeyValSort = lpDirection[llFieldNo];
			prlTmpIndex->TotalKeyLen += prlTmpArray->FieldLen[llFieldNumber];
		    }
		}
		llFieldNo++;
	    }
	    while (pclFieldStart != NULL && *pclFieldName != '\0' && llFieldNo < MAXKEYITEMS && ilRc == RC_SUCCESS);
	}

	if (ilRc != RC_SUCCESS)
	{

	    /* TODO: free already used memory for this index */
	}

	else
	{
	    prlTmpIndex->TmpKeyNew = calloc (sizeof (char), prlTmpIndex->TotalKeyLen + 4);
	    if (prlTmpIndex->TmpKeyNew == NULL)
	    {
		ilRc = RC_NOMEM;
	    }
	    prlTmpIndex->TmpKeyOld = calloc (sizeof (char), prlTmpIndex->TotalKeyLen + 4);
	    if (prlTmpIndex->TmpKeyOld == NULL)
	    {
	        ilRc = RC_NOMEM;
	    }
	    prlTmpIndex->TmpKeyCur = calloc (sizeof (char), prlTmpIndex->TotalKeyLen + 4);
	    if (prlTmpIndex->TmpKeyCur == NULL)
	    {
	        ilRc = RC_NOMEM;
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		prlTmpIndex->MaxIndexRows = prlTmpArray->MaxRows;
		prlTmpIndex->UsedIndexRows = 0;
		prlTmpIndex->FirstHitIdxRow = 0;
		prlTmpIndex->LastHitIdxRow = 0;
		prlTmpIndex->CurrIndexRow = 0;
		prlTmpIndex->FreeIndexRows = prlTmpIndex->MaxIndexRows;
		prlTmpIndex->IndexWrapper = (AA_INDEXWRAPPER*)malloc(sizeof(AA_INDEXWRAPPER)*prlTmpIndex->MaxIndexRows);
	    }
	    if (prlTmpIndex->IndexWrapper == NULL)
	    {
		free (prlTmpIndex->TmpKeyNew);
		ilRc = RC_NOMEM;
	    }

	}
	if (ilRc == RC_SUCCESS)
	{
	    for (llIdxLine = 0;llIdxLine < prlTmpIndex->MaxIndexRows;llIdxLine++)
	    {
		prlTmpIndex->IndexWrapper[llIdxLine].IndexRow = NULL;
	    }
	    prlTmpIndex->IsActive = TRUE;
	    CreateIndexWrapper (prlTmpArray, prlTmpIndex);
	}
      if (strcmp (pcpKeyFldList, "URNO") == 0 )
      {
        prlTmpArray->UrnoIdxHandle = prlTmpIndex;
      }
    }
    return ilRc;
}

int AATArrayCreateSimpleIndex (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexName,
                               char *pcpKeyFldList, long lpDirection, BOOL bpMultiFlag)
{
    int ilRc = RC_SUCCESS;
    long llOrder[MAXKEYITEMS + 1];
    int llFieldNo;
    for (llFieldNo = 0; llFieldNo < MAXKEYITEMS; llFieldNo++)
    {
	llOrder[llFieldNo] = lpDirection;
    }
    ilRc =
	AATArrayInternCreateIndex (pspArray, pcpArrayName, pspIndexHandle, pcpIndexName, pcpKeyFldList, llOrder,
				   bpMultiFlag); 
    return ilRc;
}

int AATArrayDestroyIndex (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYINDEX *prlTmpIndex;
    ilRC = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "AATArrayDestroyIndex: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	if (prlTmpArray->KeyArray != NULL)
	{
	    ilRC = FindIndex (prlTmpArray, pspIndex, pcpIndexName, &prlTmpIndex);
	    if (ilRC == RC_SUCCESS)
	    {
		if (prlTmpIndex->IndexWrapper != NULL)
		{
		    AATINDEXROW *prlTmpIndexRow;
		    long llLineNo;
		    llLineNo = 0;
		    while ((prlTmpIndex->IndexWrapper[llLineNo].IndexRow != NULL) && 
			   (llLineNo < prlTmpIndex->MaxIndexRows))
		    {
			prlTmpIndexRow = prlTmpIndex->IndexWrapper[llLineNo].IndexRow;
			free(prlTmpIndexRow->KeyValue);
			prlTmpIndexRow->KeyValue = NULL;
			free(prlTmpIndexRow);
			prlTmpIndex->IndexWrapper[llLineNo].IndexRow = NULL;
			llLineNo++;
		    }
		    free(prlTmpIndex->IndexWrapper);
		    prlTmpIndex->IndexWrapper = NULL;
		}
		if (prlTmpIndex->TmpKeyOld != NULL)
		{
		    free (prlTmpIndex->TmpKeyOld);
		    prlTmpIndex->TmpKeyOld = NULL;
		}
		strcpy (prlTmpIndex->Name, "");

		prlTmpIndex->IsActive = FALSE;
	    }
	}

	else
	{
	    dbg (TRACE, "AATArrayDestroyIndex: No Index found for Array <%s>", pcpArrayName);
	}
    }
    return ilRC;
}

int AATArrayActivateIndex (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName, BOOL bpActive)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYINDEX *prlTmpIndex;
  
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindIndex (prlTmpArray, pspIndex, pcpIndexName, &prlTmpIndex);
    }
    if (ilRc == RC_SUCCESS)
    {
	if (prlTmpIndex->IsActive != bpActive)
	{
	    prlTmpIndex->IsActive = bpActive;
	    if (prlTmpIndex->IsActive)
	    {
		CreateIndexWrapper (prlTmpArray, prlTmpIndex);
	    }
	}
    }
    return ilRc;
}

int AATArrayPutRow (HANDLE * pspArray, char *pcpArrayName, long *plpRowNum, void *pvpDataNew)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    long llIndexNo;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, plpRowNum, FALSE);
    }
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYROW *prlTmpRow;
	long llRowLen;
	prlTmpRow = &prlTmpArray->Rows[*plpRowNum];
	if (prlTmpArray->KeyArray != NULL)
	{
            if (prlTmpArray->ReorgPending == TRUE)
            {
	        ReorgIndexes(prlTmpArray);
            }
	    for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
	    {
		if (prlTmpArray->KeyArray[llIndexNo].IsActive)
		{
		    AATARRAYINDEX * prlTmpIndex;
		    char *pclData = NULL;
		    prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
		    pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
		    if (pclData != NULL)
		    {
			CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyOld);
		    }
		}
	    }
	}
	if (prlTmpArray->RowLen == 0)
	{
	    llRowLen = prlTmpRow->SizeOrig;
	}

	else
	{
	    llRowLen = prlTmpArray->RowLen;
	}
	if (prlTmpRow->DataNew == NULL)
	{
	    prlTmpRow->DataNew = (void *) malloc (llRowLen + 4);
	    if (prlTmpRow->DataNew == NULL)
	    {
		ilRc = RC_NOMEM;
	    }

	    else
	    {
		memcpy (prlTmpRow->DataNew, pvpDataNew, llRowLen);
		if (prlTmpArray->KeyArray != NULL)
		{
		    for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
		    {
			if (prlTmpArray->KeyArray[llIndexNo].IsActive)
			{
			    AATARRAYINDEX * prlTmpIndex;
			    char *pclData = NULL;
			    prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
			    pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
			    if (pclData != NULL)
			    {
				CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
				if (strcmp (prlTmpIndex->TmpKeyNew, prlTmpIndex->TmpKeyOld))
				{
				    UpdateIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyOld, prlTmpIndex->TmpKeyNew, *plpRowNum);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    return ilRc;
}

int AATArrayAddRow (HANDLE * pspArray, char *pcpArrayName, long *plpRowNum, void *pvpDataOrig)
{
    int ilRc = RC_SUCCESS;
    long llFieldNo;
    long llIndexNo;
    AATARRAY *prlTmpArray;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYROW *prlTmpRow;
	prlTmpRow = GetNewRow (prlTmpArray, plpRowNum);
	if (prlTmpRow != NULL)
	{
	    long llRowLen;
	    prlTmpRow->SizeOrig = 0;
	    prlTmpRow->SizeNew = 0;
	    prlTmpRow->RowStatus = 0;
			prlTmpRow->ChangeFlag = ARR_DATA_EMPTY;
	    if (prlTmpArray->FieldOffsets == NULL)
	    {
		ilRc = CalculateFieldOffsets (prlTmpArray, prlTmpRow->FieldOffsetsOrig, &llRowLen, pvpDataOrig);
	    }

	    else
	    {
		llRowLen = prlTmpArray->RowLen;
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		char *pclData;
		long llCharNo = 0;
		pclData = pvpDataOrig;
		for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount && ilRc == RC_SUCCESS; llFieldNo++)
		{
		    ilRc = PutField (prlTmpArray, prlTmpRow, FALSE, llFieldNo, pclData);
		    if (ilRc == RC_SUCCESS)
		    {
			while (*pclData != '\0' && llCharNo < llRowLen)
			{
			    pclData++;
			    llCharNo++;
			}
			pclData++;
		    }
		}
		if (ilRc == RC_SUCCESS)
		{
		    prlTmpArray->RowCount++;
		}
		if ((ilRc == RC_SUCCESS) && (prlTmpArray->KeyArray != NULL))
		{
                    if (prlTmpArray->ReorgPending == TRUE)
                    {
		        ReorgIndexes(prlTmpArray);
                    }
		    for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
		    {
			if (prlTmpArray->KeyArray[llIndexNo].IsActive)
			{
			    AATARRAYINDEX * prlTmpIndex;
			    char *pclData = NULL;
			    prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
			    pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
			    if (pclData != NULL)
			    {
				CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
				InsertIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyNew, *plpRowNum);
			    }
			}
		    }
		}
	    }
	}
    }
    return ilRc;
}

int AATArrayAddRowPart (HANDLE * pspArray, char *pcpArrayName, long *plpRowNum, char *pcpFldLst, void *pcpDataOrig)
{
    int ilRc = RC_SUCCESS;
    long llFieldNo;
    long llIndexNo;
    AATARRAY *prlTmpArray;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYROW *prlTmpRow;
	prlTmpRow = GetNewRow (prlTmpArray, plpRowNum);
	if (prlTmpRow != NULL)
	{
	    long llRowLen;
	    prlTmpRow->SizeOrig = 0;
	    prlTmpRow->SizeNew = 0;
	    prlTmpRow->RowStatus = 0;
			prlTmpRow->ChangeFlag = ARR_DATA_EMPTY;
	    if (prlTmpArray->FieldOffsets == NULL)
	    {
		ilRc = CalculateFieldOffsets (prlTmpArray, prlTmpRow->FieldOffsetsOrig, &llRowLen, pcpDataOrig);
	    }

	    else
	    {
		llRowLen = prlTmpArray->RowLen;
	    }
	    if (ilRc == RC_SUCCESS)
	    {
		char *pclArrFld;
		char pclFld[24];
		long llFldCnt = 0;
		long llDatCnt = 0;
		llFldCnt = TWECountDelimiter (pcpFldLst, ",");
		llDatCnt = TWECountDelimiter (pcpDataOrig, ",");
		if ((llFldCnt == llDatCnt) && (llFldCnt > 0))
		{
		    pclArrFld = prlTmpArray->FieldList;
		    for (llFieldNo = 0; llFieldNo < prlTmpArray->FieldCount && ilRc == RC_SUCCESS; llFieldNo++)
		    {
			GetDataItem (pclFld, pclArrFld, llFieldNo + 1, ',', "\0", "\0\0");
			if (strstr (pcpFldLst, pclFld) != NULL)
			{                   /* one of fields given */
			    ilRc = TWEExtractField (pclFld, pcpFldLst, pcpDataOrig, ",", pcgOneFldData);
			}

			else
			{

			    /* Take one Blanc as default for not given fields */
			    strcpy (pcgOneFldData, " ");
			}
			ilRc = PutField (prlTmpArray, prlTmpRow, FALSE, llFieldNo, pcgOneFldData);
		    }                     /* for all fields in array */
		}

		else
		{
		    ilRc = RC_INVALID;
		}
		if (ilRc == RC_SUCCESS)
		{
		    prlTmpArray->RowCount++;
		}
		if ((ilRc == RC_SUCCESS) && (prlTmpArray->KeyArray != NULL))
		{
                    if (prlTmpArray->ReorgPending == TRUE)
                    {
		        ReorgIndexes(prlTmpArray);
                    }
		    for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
		    {
			if (prlTmpArray->KeyArray[llIndexNo].IsActive)
			{
			    AATARRAYINDEX * prlTmpIndex;
			    char *pclData = NULL;
			    prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
			    pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
			    if (pclData != NULL)
			    {
				CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
				InsertIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyNew, *plpRowNum);
			    }
			}
		    }
		}
	    }
	}
    }
    return ilRc;
}

int AATArrayGetRow2(HANDLE * pspArray, char *pcpArrayName, long lpRowNum, 
		    char cpDelimiter, long lpMaxBytes,void *pvpDataBuf,BOOL bpFindAll)
{
    int ilRc = RC_SUCCESS;
    long llLength = 0;
    long llCnt = 0;
    long llRowNum = -1;
    char *pclSrc;
    char *pclDest;
    AATARRAY *prlTmpArray;
    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, bpFindAll);
    }
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYROW *prlTmpRow;
	void *pvlSrc;
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	pvlSrc = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
	if (pvlSrc == NULL)
	{
	    ilRc = RC_INVALID;
	}
	if (ilRc == RC_SUCCESS)
	{
	    long llRowLen;
	    if (prlTmpArray->RowLen == 0)
	    {
		llRowLen = prlTmpRow->SizeOrig;
	    }

	    else
	    {
		llRowLen = prlTmpArray->RowLen;
	    }
	    if (llRowLen > lpMaxBytes)
	    {
		dbg (TRACE, "AATArrayGetRow: Buffersize %ld is too small", lpMaxBytes);
		dbg (TRACE, "AATArrayGetRow: Rowsize is %ld", llRowLen);
		ilRc = RC_INVALID;
	    }
	    if ((cpDelimiter == '\0') && (ilRc == RC_SUCCESS))
	    {
		/*memset (pvpDataBuf, 0, lpMaxBytes);*/
		llLength = min (llRowLen, lpMaxBytes);
		memcpy (pvpDataBuf, pvlSrc, llLength); /* last is NULL  */
		/*TEST*/return RC_SUCCESS;
	    }

	    /*else if (ilRc == RC_SUCCESS)*/
	    if ((cpDelimiter != '\0') && (ilRc == RC_SUCCESS))
	    {
		llLength = 0;
		/*memset (pvpDataBuf, 0, lpMaxBytes);*/
		pclDest = (char *) pvpDataBuf;
		pclSrc = (char *) pvlSrc;
		for (llCnt = 0; (llCnt < prlTmpArray->FieldCount) && (llLength < lpMaxBytes); llCnt++)
		{
		    while ((*pclSrc != '\0') && (llLength < lpMaxBytes - 1))
		    {
			*pclDest = *pclSrc;
			pclDest++;
			pclSrc++;
			llLength++;
		    }
		    if ((llCnt < prlTmpArray->FieldCount - 1) && (cpDelimiter != '\0'))
		    {
			*pclDest = cpDelimiter;
			pclDest++;
			pclSrc++;
			llLength++;
		    }

		    else
		    {
			*pclDest = '\0';
			llLength++;         /* llLength <= lpMaxBytes  */
		    }
		}                       /* for all fields in row */
	    }
	}
    }
    return ilRc;
}

int AATArrayGetRow (HANDLE * pspArray, char *pcpArrayName, long lpRowNum, char cpDelimiter, long lpMaxBytes,
                    void *pvpDataBuf)
{
    return(AATArrayGetRow2(pspArray,pcpArrayName,lpRowNum,cpDelimiter,lpMaxBytes,
			   pvpDataBuf,FALSE));
}


int AATArrayGetRowPointer (HANDLE * pspArray, char *pcpArrayName, long lpRowNum, void **pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    long llRowNum = -1;
    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, FALSE);
    }
    if (ilRc == RC_SUCCESS)
    {
	AATARRAYROW *prlTmpRow;
	void *pvlSrc;
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	pvlSrc = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
	if (pvlSrc == NULL)
	{
	    ilRc = RC_INVALID;
	}
	*pvpDataBuf = pvlSrc;
    }
    return ilRc;
}

int AATArrayDeleteRow (HANDLE * pspArray, char *pcpArrayName, long lpRowNum)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray = NULL;
    long llRowNum = -1;
    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, TRUE);
    }
    if ((ilRc == RC_SUCCESS) || (ilRc == RC_ROWDELETED))
    {                             /*  here: real delete  */
	ilRc = DeleteRow (prlTmpArray, llRowNum,TRUE);
    }
    return ilRc;
}

int AATArrayPutField (HANDLE * pspArray, char *pcpArrayName, long *plpFieldNum, char *pcpFieldName, long lpRowNum,
                      BOOL spNewDat, void *pvpData)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    char *pcpSrc;
    char *pcpDest;
    long llCount;
    long llIndexNo;
    long llFieldNum = -1;
    long llRowNum = -1;

    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, FALSE);
    }
    if (ilRc == RC_SUCCESS)
    {
	if (plpFieldNum != NULL)
	{
	    if (*plpFieldNum < 0 || *plpFieldNum >= prlTmpArray->FieldCount)
	    {
		llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	    }

	    else
	    {
		llFieldNum = *plpFieldNum;
	    }
	}

	else
	{
	    llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	}
	if (llFieldNum < 0 || llFieldNum >= prlTmpArray->FieldCount)
	{
	    ilRc = RC_INVALID;
	}
    }
    if ((ilRc == RC_SUCCESS) && (prlTmpArray->KeyArray != NULL))
    {
        if (prlTmpArray->ReorgPending == TRUE)
        {
	    ReorgIndexes(prlTmpArray);
        }
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
	{
	    if (prlTmpArray->KeyArray[llIndexNo].IsActive)
	    {
		AATARRAYINDEX * prlTmpIndex;
		char *pclData = NULL;
		prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
		pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
		if (pclData != NULL)
		{
		    CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyOld);
		}
	    }
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	long llRowLen;
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	if (prlTmpArray->RowLen == 0)
	{
	    llRowLen = prlTmpRow->SizeOrig;
	}

	else
	{
	    llRowLen = prlTmpArray->RowLen;
	}
	if (prlTmpRow->DataOrig == NULL)
	{
	    prlTmpRow->DataOrig = (void *) malloc (llRowLen + 4);
	    if (prlTmpRow->DataOrig == NULL)
	    {
		ilRc = RC_NOMEM;
	    }

	    else
	    {
		memset (prlTmpRow->DataOrig, 0, llRowLen);
	    }
	}
	if ((prlTmpRow->DataNew == NULL) && (spNewDat == TRUE))
	{
	    prlTmpRow->DataNew = (void *) malloc (llRowLen + 4);
	    if (prlTmpRow->DataNew == NULL)
	    {
		ilRc = RC_NOMEM;
	    }

	    else
	    {
		memcpy (prlTmpRow->DataNew, prlTmpRow->DataOrig, llRowLen);
	    }
	}
	if (spNewDat == TRUE)
	{
	    pcpDest =
		(char *) &prlTmpRow->DataNew[prlTmpArray->FieldOffsets !=
					     NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					     prlTmpRow->FieldOffsetsOrig[llFieldNum]];}

	else
	{
	    pcpDest =
		(char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
					      NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					      prlTmpRow->FieldOffsetsOrig[llFieldNum]];}
	pcpSrc = pvpData;

/* TWE 10022000  NEW */
	/* fill up with blancs  */
	/*memset (pcpDest, ' ', prlTmpArray->FieldLen[llFieldNum] - 1);*/
	memset (pcpDest, ' ', prlTmpArray->FieldLen[llFieldNum]);

/********************/
	for (llCount = 0; *pcpSrc != '\0' && llCount < prlTmpArray->FieldLen[llFieldNum]; llCount++)
	{
	    *pcpDest++ = *pcpSrc++;
	}

	/* fill up with blancs  */
/* TWE 10022000  NEW KOMMENTED */

	for ( ; llCount < prlTmpArray->FieldLen[llFieldNum]; llCount++)
	{
	    *pcpDest++ = ' ';           
	}
	*pcpDest = '\0';

	if (plpFieldNum != NULL)
	{
	    *plpFieldNum = llFieldNum;
	}

			/*** set RecordIsChanged Flag if it is not a logical field **/
			if ((llFieldNum < prlTmpArray->SqlFieldCount) && (prlTmpRow->ChangeFlag == ARR_DATA_EMPTY ||  prlTmpRow->ChangeFlag == ARR_DATA_UNCHANGED))
        {
            /*dbg(TRACE,"CHANGED SQL FIELD"); */
            prlTmpRow->ChangeFlag = ARR_DATA_CHANGED;
        }
    }

    if ((ilRc == RC_SUCCESS) && (prlTmpArray->KeyArray != NULL))
    {
	for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
	{
	    if (prlTmpArray->KeyArray[llIndexNo].IsActive)
	    {
		AATARRAYINDEX * prlTmpIndex;
		char *pclData = NULL;
		prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
		pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
		CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
		if (strcmp (prlTmpIndex->TmpKeyNew, prlTmpIndex->TmpKeyOld))
		{
		    UpdateIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyOld, prlTmpIndex->TmpKeyNew, llRowNum);
		}
	    }
	}
    }
    return ilRc;
}

int AATArrayGetFields (HANDLE * pspArray, char *pcpArrayName, long *plpFieldNumbers, char *pcpFieldList,
                       char cpDelimiter, long lpMaxBytes, long lpRowNum, void *pvpDataBuf, BOOL bpAllFields)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    long *pllFieldNumbers = NULL;
    long llFieldCount;
    long llItemLen;
    long llRow;
    BOOL blGoNext = FALSE;
    if ((lpRowNum == ARR_NEXT) || (lpRowNum == ARR_PREV) || (lpRowNum == ARR_FIRST) || (lpRowNum == ARR_LAST) ||
	(lpRowNum == ARR_CURRENT))
    {
	blGoNext = TRUE;
    }
    llRow = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRow, bpAllFields);
    }
    if (ilRc == RC_SUCCESS)
    {
	llFieldCount = TWECountDelimiter (pcpFieldList, ",");
	if (plpFieldNumbers != NULL)
	{
	    pllFieldNumbers = plpFieldNumbers;
	}
	else
	{
	    pllFieldNumbers = (long *) (malloc (sizeof (long) * (llFieldCount + 4)));
	    if (pllFieldNumbers == NULL)
	    {
		ilRc = RC_NOMEM;
	    }

	    else
	    {
		pllFieldNumbers[0] = -1;
	    }
	}
	if (pllFieldNumbers != NULL)
	{
	    if (pllFieldNumbers[0] == -1)
	    {
		char *pclFieldStart;
		char pclFieldName[24];
		long llFieldNo = 0;
		pclFieldStart = pcpFieldList;

		do
		{
		    llItemLen = GetNextDataItem (pclFieldName, &pclFieldStart, ",", "\0", "\0\0");
		    if (llItemLen < 0)
		    {
			dbg (TRACE, "AATArrayGetFields: Fkt.: GetNextDataItem (%d)", llItemLen);
		    }
		    if (*pclFieldName != '\0')
		    {
			long llFieldNumber;
			llFieldNumber = FindField (prlTmpArray->FieldList, pclFieldName);
			if (llFieldNumber == -1)
			{
			    ilRc = RC_INVALID;
			}

			else
			{
			    pllFieldNumbers[llFieldNo++] = llFieldNumber;
			}
		    }
		}
		while (pclFieldStart != NULL && *pclFieldName != '\0' && ilRc == RC_SUCCESS);
	    }
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	char *pclDest;
	long llCnt = 0;
	long llActualBytes = 0;
	long llFieldNo;
	AATARRAYROW *prlTmpRow;
	prlTmpRow = &prlTmpArray->Rows[llRow];
	if (prlTmpRow->DataNew == NULL && prlTmpRow->DataOrig == NULL)
	{
	    ilRc = RC_NODATA;
	}
	else if ((prlTmpRow->ChangeFlag == ARR_DATA_DELETED) || (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED))
	{
	    if (bpAllFields == TRUE)
	    {
		ilRc = RC_SUCCESS;
	    }
	    else
	    {
		ilRc = RC_ROWDELETED;
	    }
	}
	pclDest = (char *) pvpDataBuf;
	for (llFieldNo = 0; llFieldNo < llFieldCount && ilRc == RC_SUCCESS; llFieldNo++)
	{
	    ilRc = GetField (prlTmpArray, prlTmpRow, pllFieldNumbers[llFieldNo], lgOneFldDataLen, pcgOneFldData);
	    if (ilRc == RC_SUCCESS)
	    {
		char *pclSrc;
		pclSrc = pcgOneFldData;
		for (llCnt = 0; (llActualBytes < lpMaxBytes - 1) && (*pclSrc != '\0'); llCnt++)
		{
		    llActualBytes++;
		    *pclDest++ = *pclSrc++;
		}
		if (*pclSrc != '\0')
		{
		    dbg (TRACE, "AATArrayGetFields: Buffersize %ld is too small!", lpMaxBytes);
		    ilRc = RC_FAIL;
		}
		if ((llActualBytes < lpMaxBytes - 1) && (cpDelimiter != '\0'))
		{
		    *pclDest++ = cpDelimiter;
		}
	    }
	}                           /* end of for all fields */
	if (llActualBytes < lpMaxBytes - 1)
	{                           /* last is delimiter */
	    pclDest--;
	}
	if (lpMaxBytes > 0)
	{
	    *pclDest = '\0';
	}
    }

    /* end of if RC_SUCCESS */
    if ((pllFieldNumbers != NULL) && (pllFieldNumbers != plpFieldNumbers))
    {
	free (pllFieldNumbers);
	pllFieldNumbers = NULL;
    }
    if ((blGoNext == TRUE) && (ilRc == RC_ROWDELETED))
    {
	ilRc = RC_SUCCESS;
    }
    return ilRc;
}

int AATArrayGetField (HANDLE * pspArray, char *pcpArrayName, long *plpFieldNum, char *pcpFieldName, long lpMaxBytes,
                      long lpRowNum, void *pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    long llFieldNum = -1;
    long llRowNum = -1;
    BOOL blGoNext = FALSE;
    if ((lpRowNum == ARR_NEXT) || (lpRowNum == ARR_PREV) || (lpRowNum == ARR_FIRST) || (lpRowNum == ARR_LAST) ||
	(lpRowNum == ARR_CURRENT))
    {
	blGoNext = TRUE;
    }
    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, FALSE);
    }
    if (ilRc == RC_SUCCESS)
    {
	if (plpFieldNum != NULL)
	{
	    if (*plpFieldNum < 0 || *plpFieldNum >= prlTmpArray->FieldCount)
	    {
		llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	    }

	    else
	    {
		llFieldNum = *plpFieldNum;
	    }
	}

	else
	{
	    llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	}
	if (llFieldNum < 0 || llFieldNum >= prlTmpArray->FieldCount)
	{
	    ilRc = RC_INVALID;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	if (llRowNum < 0 || llRowNum >= prlTmpArray->MaxRows)
	{
	    ilRc = RC_INVALID;
	}
	if (ilRc == RC_SUCCESS)
	{
	    prlTmpRow = &prlTmpArray->Rows[llRowNum];
	    if (prlTmpRow->DataNew == NULL && prlTmpRow->DataOrig == NULL)
	    {
		ilRc = RC_NODATA;
	    }

	    else if ((prlTmpRow->ChangeFlag == ARR_DATA_DELETED) || (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED))
	    {
		ilRc = RC_ROWDELETED;
	    }
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	char *pcpSrc;
	char *pcpDest;
	long llCount;
	pcpDest = pvpDataBuf;
	if (prlTmpRow->DataNew != NULL)
	{
	    pcpSrc =
		(char *) &prlTmpRow->DataNew[prlTmpArray->FieldOffsets !=
					     NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					     prlTmpRow->FieldOffsetsNew[llFieldNum]];}

	else if (prlTmpRow->DataOrig != NULL)
	{
	    pcpSrc =
		(char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
					      NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					      prlTmpRow->FieldOffsetsOrig[llFieldNum]];}
	for (llCount = 0; *pcpSrc != '\0' && llCount < lpMaxBytes - 1; llCount++)
	{
	    *pcpDest++ = *pcpSrc++;
	}
	if (*pcpSrc != '\0')
	{
	    dbg (TRACE, "AATArrayGetField: Buffersize %ld is too small!", lpMaxBytes);
	    ilRc = RC_FAIL;
	}
	if (lpMaxBytes > 0)
	{
	    *pcpDest = '\0';
	}
	if (plpFieldNum != NULL)
	{
	    *plpFieldNum = llFieldNum;
	}
    }
    if ((blGoNext == TRUE) && (ilRc == RC_ROWDELETED))
    {
	ilRc = RC_SUCCESS;
    }
    return ilRc;
}

int AATArrayGetFieldPointer (HANDLE * pspArray, char *pcpArrayName, long *plpFieldNum, char *pcpFieldName,
                             long lpRowNum, void **pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    long llFieldNum = -1;
    long llRowNum = -1;
    BOOL blGoNext = FALSE;
    if ((lpRowNum == ARR_NEXT) || (lpRowNum == ARR_PREV) || (lpRowNum == ARR_FIRST) || (lpRowNum == ARR_LAST) ||
	(lpRowNum == ARR_CURRENT))
    {
	blGoNext = TRUE;
    }
    llRowNum = lpRowNum;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindRow (prlTmpArray, &llRowNum, FALSE);
    }
    if (ilRc == RC_SUCCESS)
    {
	if (plpFieldNum != NULL)
	{
	    if (*plpFieldNum < 0 || *plpFieldNum >= prlTmpArray->FieldCount)
	    {
		llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	    }

	    else
	    {
		llFieldNum = *plpFieldNum;
	    }
	}

	else
	{
	    llFieldNum = FindField (prlTmpArray->FieldList, pcpFieldName);
	}
	if (llFieldNum < 0 || llFieldNum >= prlTmpArray->FieldCount)
	{
	    ilRc = RC_INVALID;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	if (llRowNum < 0 || llRowNum >= prlTmpArray->MaxRows)
	{
	    ilRc = RC_INVALID;
	}
	if (ilRc == RC_SUCCESS)
	{
	    prlTmpRow = &prlTmpArray->Rows[llRowNum];
	    if (prlTmpRow->DataNew == NULL && prlTmpRow->DataOrig == NULL)
	    {
		ilRc = RC_NODATA;
	    }

	    else if ((prlTmpRow->ChangeFlag == ARR_DATA_DELETED) || (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED))
	    {
		ilRc = RC_ROWDELETED;
	    }
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	char *pcpSrc;
	if (prlTmpRow->DataNew != NULL)
	{
	    pcpSrc =
		(char *) &prlTmpRow->DataNew[prlTmpArray->FieldOffsets !=
					     NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					     prlTmpRow->FieldOffsetsNew[llFieldNum]];}

	else if (prlTmpRow->DataOrig != NULL)
	{
	    pcpSrc =
		(char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
					      NULL ? prlTmpArray->FieldOffsets[llFieldNum] :
					      prlTmpRow->FieldOffsetsOrig[llFieldNum]];}
	*pvpDataBuf = pcpSrc;
	if (plpFieldNum != NULL)
	{
	    *plpFieldNum = llFieldNum;
	}
    }
    if ((blGoNext == TRUE) && (ilRc == RC_ROWDELETED))
    {
	ilRc = RC_SUCCESS;
    }
    return ilRc;
}

static int GetBSearchString (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexNam,
                             char *pcpKey)
{
    int ilRc = RC_SUCCESS;
    long llFieldNo = 0;
    long llKeyLen = 0;
    long llFieldNumber = 0;
    long llStrLen = 0;
    long llItemLen;
    char *pclFieldStart = NULL;
    AATARRAY *prlTmpArray;
    AATARRAYINDEX *prlTmpIndex;
    static char clGetBDummy[512];
    strcpy (pcgOneRowData, "");
    ilRc = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindIndex (prlTmpArray, pspIndexHandle, pcpIndexNam, &prlTmpIndex);
    }
    if (pcpKey == NULL)
    {
	ilRc = RC_FAIL;
    }

    if (ilRc == RC_SUCCESS)
    {
	llStrLen = 0;
	pclFieldStart = pcpKey;

	do
	{
	    llItemLen = GetNextDataItem (pcgOneFldData, &pclFieldStart, ",", "\0", "  ");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "GetBSearchString: Fkt.: GetNextDataItem (%d)", llItemLen);
	    }
	    llFieldNumber = prlTmpIndex->IndexDescriptor[llFieldNo].FieldItemNo;
	    llKeyLen = prlTmpArray->FieldLen[llFieldNumber];
	    sprintf (clGetBDummy, "%-*s", llKeyLen, pcgOneFldData);
	    llStrLen += llKeyLen + 1;
	    if (llStrLen < lgOneRowDataLen)
	    {
		strcat (pcgOneRowData, clGetBDummy);
		llFieldNo++;
	    }
	    else
	    {
		llFieldNo = MAXKEYITEMS;
	    }
	}
	while ((*pclFieldStart != '\0') && (llFieldNo < MAXKEYITEMS));
    }

/*dbg(TRACE,"GetBSearchString <%s> ",
  pcgOneRowData);*/
    return ilRc;
}

int AATArrayReorgIndexes (HANDLE * pspArray, char *pcpArrayName, BOOL bpReorg)
{
    int ilRc = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
        ReorgIndexes(prlTmpArray);
    }
    return ilRc;
}


int AATArrayDeleteRowsByIndexValue (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName,
                            char *pcpPattern, BOOL bpReorg)
{
    int ilRc = -1;
    int ilGetRc = RC_SUCCESS;
	long llComp;
    long llIdxRow;
    long llRowNum;
    long llFirstRow;
    long llLastRow;
    void *pvlSrc = NULL;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    AATARRAYINDEX *prlTmpIndex;
    AATINDEXROW *prlTmpIndexRow;
    ilGetRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilGetRc == RC_SUCCESS)
    {
	ilGetRc = FindIndex (prlTmpArray, pspIndex, pcpIndexName, &prlTmpIndex);
    }
    if (ilGetRc == RC_SUCCESS)
    {
	if ((pcpPattern != NULL) && (strcmp (pcpPattern, "") != 0))
	{
            ilRc = 0;
	    GetBSearchString (pspArray, pcpArrayName, pspIndex, pcpIndexName, pcpPattern);
	    ilGetRc = BFindIndex (prlTmpIndex, pcgOneRowData, &llFirstRow, &llLastRow,-1,TRUE);
            if (ilGetRc == RC_SUCCESS)
            {
		/* dbg(TRACE,"DELETE FROM %d TO %d",llFirstRow,llLastRow); */
		for (llIdxRow=llFirstRow;llIdxRow<=llLastRow;llIdxRow++)
		{
	    	    prlTmpIndexRow = prlTmpIndex->IndexWrapper[llIdxRow].IndexRow;
		    llRowNum = prlTmpIndexRow->DataRowNum;
		    ilGetRc = DeleteRow (prlTmpArray, llRowNum, FALSE);
	        }
                prlTmpArray->ReorgPending = TRUE;
		if (bpReorg == TRUE)
		{
		    ReorgIndexes(prlTmpArray);
		}
                ilRc = llLastRow - llFirstRow + 1;
            }
        }
    }
    return ilRc  ;
}
int AATArrayFindRowPointer (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName,
                            char *pcpPattern, long *plpIndex, void **pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    long llComp;
    long llRowNum;
    long llFirstRow;
    long llLastRow;
    long llCmd;
    int ilLookAgain = TRUE;
    void *pvlSrc = NULL;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    AATARRAYINDEX *prlTmpIndex;
    AATINDEXROW *prlTmpIndexRow;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindIndex (prlTmpArray, pspIndex, pcpIndexName, &prlTmpIndex);
    }
    if (ilRc == RC_SUCCESS)
    {
	ilLookAgain = TRUE;
	while (ilLookAgain == TRUE)
	{
	ilRc = RC_SUCCESS;
	ilLookAgain = FALSE;
	llCmd = *plpIndex;
	if ((pcpPattern != NULL) && (strcmp (pcpPattern, "") != 0))
	{
	    switch (llCmd)
	    {
		case ARR_FIRST:
		    GetBSearchString (pspArray, pcpArrayName, pspIndex, pcpIndexName, pcpPattern);
		    ilRc = BFindIndex (prlTmpIndex, pcgOneRowData, &llFirstRow, &llLastRow,-1,TRUE);
		    prlTmpIndex->FirstHitIdxRow = llFirstRow;
		    prlTmpIndex->LastHitIdxRow = llLastRow;
		    llRowNum = llFirstRow;
                    strcpy(prlTmpIndex->TmpKeyCur, pcgOneRowData);
		    llComp = 0;
                    /* dbg(TRACE,"IDX ROWS (%d): FIRST=%d / LAST=%d",(llLastRow-llFirstRow+1),llFirstRow,llLastRow); */
		    break;
		case ARR_LAST:
		    GetBSearchString (pspArray, pcpArrayName, pspIndex, pcpIndexName, pcpPattern);
		    ilRc = BFindIndex (prlTmpIndex, pcgOneRowData, &llFirstRow, &llLastRow,-1,TRUE);
		    prlTmpIndex->FirstHitIdxRow = llFirstRow;
		    prlTmpIndex->LastHitIdxRow = llLastRow;
		    llRowNum = llLastRow;
                    strcpy(prlTmpIndex->TmpKeyCur, pcgOneRowData);
		    llComp = 0;
                    /* dbg(TRACE,"IDX ROWS (%d): FIRST=%d / LAST=%d",(llLastRow-llFirstRow+1),llFirstRow,llLastRow); */
		    break;
		case ARR_CURRENT:
		    llRowNum = prlTmpIndex->CurrIndexRow;
			llComp = -1;
		    break;
		case ARR_NEXT:
		    llRowNum = prlTmpIndex->CurrIndexRow + 1;
		    if (llRowNum > prlTmpIndex->LastHitIdxRow)
		    {
			llRowNum = prlTmpIndex->CurrIndexRow;
			ilRc = RC_NOTFOUND;
		    }
			llComp = -1;
		    break;
		case ARR_PREV:
		    llRowNum = prlTmpIndex->CurrIndexRow - 1;
		    if (llRowNum < prlTmpIndex->FirstHitIdxRow)
		    {
			llRowNum = prlTmpIndex->CurrIndexRow;
			ilRc = RC_NOTFOUND;
		    }
			llComp = -1;
		    break;
		default:
		    llRowNum = 0;
		    ilRc = RC_INVALID;
		    break;
	    }
	}
	else
	{
		llComp = 0;
	    switch (llCmd)
	    {
		case ARR_FIRST:
		    prlTmpIndex->FirstHitIdxRow = 0;
		    prlTmpIndex->LastHitIdxRow = prlTmpIndex->UsedIndexRows - 1;
		    llRowNum = prlTmpIndex->FirstHitIdxRow;
		    break;
		case ARR_LAST:
		    prlTmpIndex->FirstHitIdxRow = 0;
		    prlTmpIndex->LastHitIdxRow = prlTmpIndex->UsedIndexRows - 1;
		    llRowNum = prlTmpIndex->LastHitIdxRow;
		    break;
		case ARR_CURRENT:
		    llRowNum = prlTmpIndex->CurrIndexRow;
		    break;
		case ARR_NEXT:
		    llRowNum = prlTmpIndex->CurrIndexRow + 1;
		    if (llRowNum > prlTmpIndex->LastHitIdxRow)
		    {
			llRowNum = prlTmpIndex->CurrIndexRow;
			ilRc = RC_NOTFOUND;
		    }
		    break;
		case ARR_PREV:
		    llRowNum = prlTmpIndex->CurrIndexRow - 1;
		    if (llRowNum < prlTmpIndex->FirstHitIdxRow)
		    {
			llRowNum = prlTmpIndex->CurrIndexRow;
			ilRc = RC_NOTFOUND;
		    }
		    break;
		default:
		    llRowNum = 0;
		    ilRc = RC_INVALID;
		    break;
	    }
	    if((prlTmpIndex->UsedIndexRows <= 0) || (llRowNum < 0))
	    {
		ilRc = RC_NOTFOUND;
	    }
	}
	if (ilRc == RC_SUCCESS)
	{
	    ilRc = RC_NOTFOUND;
	    prlTmpIndexRow = prlTmpIndex->IndexWrapper[llRowNum].IndexRow;
	    if (prlTmpIndexRow != NULL)
	    {
			*plpIndex = prlTmpIndexRow->DataRowNum;
			if (*plpIndex >= 0)
			{
				pvlSrc = NULL;
				prlTmpRow = &prlTmpArray->Rows[*plpIndex];
				if (prlTmpRow != NULL)
				{
					pvlSrc = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
					*pvpDataBuf = pvlSrc;
				}
				ilRc = RC_SUCCESS;
				if (pvlSrc == NULL)
				{
					dbg(TRACE,"INDEX <%s> CORRUPT! (ROW %ld OF %ld INDEX LINES)",
							pcpIndexName,llRowNum,prlTmpIndex->UsedIndexRows);
					ilLookAgain = TRUE;
	    				switch (llCmd)
	    				{
						case ARR_FIRST:
							*plpIndex = ARR_NEXT;
		    				break;
						case ARR_LAST:
							*plpIndex = ARR_PREV;
		    				break;
						case ARR_CURRENT:
							ilRc = RC_NOTFOUND;
							ilLookAgain = FALSE;
		    				break;
						case ARR_NEXT:
							*plpIndex = ARR_NEXT;
		    				break;
						case ARR_PREV:
							*plpIndex = ARR_PREV;
		    				break;
						default:
							ilRc = RC_INVALID;
							ilLookAgain = FALSE;
		    				break;
					}
				}
			}
	    }
	}
	prlTmpIndex->CurrIndexRow = llRowNum;
        /* dbg(TRACE,"CURR IDX ROW = %d",llRowNum); */
	} /* end while */

	if ((ilRc == RC_SUCCESS) && (llComp < 0))
	{
		/*
		GetBSearchString (pspArray, pcpArrayName, pspIndex, pcpIndexName, pcpPattern);
		llComp = BCompare (prlTmpIndex, pcgOneRowData, prlTmpIndexRow->KeyValue);
		*/
		llComp = BCompare (prlTmpIndex, prlTmpIndex->TmpKeyCur, prlTmpIndexRow->KeyValue);
		if (llComp != 0)
		{
			ilRc = RC_NOTFOUND;
		}
	}
	if (ilRc != RC_SUCCESS)
	{
	    *plpIndex = -1;
	}
    }
    return ilRc;
}

int AATArrayFindKey (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName, long *plpIndex,
                     long lpMaxBytes, void *pvpPattern)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = -1;
    long llFirstRow = -1;
    long llLastRow = -1;
    long llLength = 0;
    AATARRAY *prlTmpArray;
    AATARRAYINDEX *prlTmpIndex;
    AATINDEXROW *prlTmpIndexRow;
    ilRc = FindArray (pspArray, pcpArrayName, &prlTmpArray);
    if (ilRc == RC_SUCCESS)
    {
	ilRc = FindIndex (prlTmpArray, pspIndex, pcpIndexName, &prlTmpIndex);
    }
    if ((ilRc == RC_SUCCESS) && (prlTmpIndex->IndexWrapper == NULL))
    {
	ilRc = RC_NOTFOUND;
    }
    if (ilRc == RC_SUCCESS)
    {
	switch (*plpIndex)
	{
	    case ARR_FIRST:
		prlTmpIndex->FirstHitIdxRow = 0;
		prlTmpIndex->LastHitIdxRow = prlTmpIndex->UsedIndexRows - 1;
		llRowNum = prlTmpIndex->FirstHitIdxRow;
		break;
	    case ARR_LAST:
		prlTmpIndex->FirstHitIdxRow = 0;
		prlTmpIndex->LastHitIdxRow = prlTmpIndex->UsedIndexRows - 1;
		llRowNum = prlTmpIndex->LastHitIdxRow;
		break;
	    case ARR_CURRENT:
		llRowNum = prlTmpIndex->CurrIndexRow;
		break;
	    case ARR_NEXT:
		llRowNum = prlTmpIndex->CurrIndexRow + 1;
		if (llRowNum > prlTmpIndex->LastHitIdxRow)
		{
		    llRowNum = prlTmpIndex->CurrIndexRow;
		    ilRc = RC_NOTFOUND;
		}
		break;
	    case ARR_PREV:
		llRowNum = prlTmpIndex->CurrIndexRow - 1;
		if (llRowNum < prlTmpIndex->FirstHitIdxRow)
		{
		    llRowNum = prlTmpIndex->CurrIndexRow;
		    ilRc = RC_NOTFOUND;
		}
		break;
	    case ARR_FIND:
		GetBSearchString (pspArray, pcpArrayName, pspIndex, pcpIndexName, pvpPattern);
		ilRc = BFindIndex (prlTmpIndex, pcgOneRowData, &llFirstRow, &llLastRow,-1,TRUE);
		prlTmpIndex->FirstHitIdxRow = llFirstRow;
		prlTmpIndex->LastHitIdxRow = llLastRow;
		llRowNum = llFirstRow;
		break;
	    default:
		llRowNum = 0;
		ilRc = RC_INVALID;
	}
    }
    if ((ilRc == RC_SUCCESS) && ((llRowNum < 0) || (llRowNum >= prlTmpIndex->UsedIndexRows)))
    {
	ilRc = RC_NOTFOUND;
    }
    if (ilRc == RC_SUCCESS)
    {
	ilRc = RC_NOTFOUND;
	prlTmpIndexRow = prlTmpIndex->IndexWrapper[llRowNum].IndexRow;
	if (prlTmpIndexRow != NULL)
	{
	    if (prlTmpIndexRow->KeyValue != NULL)
	    {
	      *plpIndex = prlTmpIndexRow->DataRowNum;
	      llLength = min (prlTmpIndex->TotalKeyLen, lpMaxBytes);
	      memcpy (pvpPattern, prlTmpIndexRow->KeyValue, llLength); /* last is NULL */
    	      prlTmpIndex->CurrIndexRow = llRowNum;
	      ilRc = RC_SUCCESS;
	      if (prlTmpIndex->TotalKeyLen > lpMaxBytes)
	      {
	        dbg (TRACE, "AATArrayFindKey: Buffersize %ld is too small", lpMaxBytes);
	        ilRc = RC_FAIL;
	      }
	    }
	}
    }
    if (ilRc != RC_SUCCESS)
    {
	*plpIndex = -1;
    }
  
    return ilRc;
}

static int BFindIndex(AATARRAYINDEX * prpIndex, void *pvpKey, long *plpFirst, long *plpLast, long lpLookRow,BOOL bpLookOnly)
{
    int ilRc = RC_NOTFOUND;
    long llLeft = -1;
    long llRight = -1;
    long llMiddle = -1;
    long llHitRow = -1;
    long llComp;
    AATINDEXROW *prlTmpIndexRow = NULL;
    AA_INDEXWRAPPER *prlTmpWrapper = NULL;

    *plpFirst = -1;
    *plpLast = -1;
    prlTmpWrapper = prpIndex->IndexWrapper;
    if ((prlTmpWrapper != NULL) && (prpIndex->UsedIndexRows > 0))
    {
	llLeft = 0;
	llRight = prpIndex->UsedIndexRows - 1;
	while (ilRc == RC_NOTFOUND && llLeft <= llRight)
	{
	    llMiddle = llLeft + ((long) (llRight - llLeft) / 2);
	    prlTmpIndexRow = prlTmpWrapper[llMiddle].IndexRow;
	    llComp = BCompare (prpIndex, pvpKey, prlTmpIndexRow->KeyValue);
	    if (llComp < 0)
	    {
		llRight = llMiddle - 1;
	    }

	    else if (llComp > 0)
	    {
		llLeft = llMiddle + 1;
	    }

	    else
	    {
		ilRc = RC_SUCCESS;
	    }
	}
	if (ilRc == RC_SUCCESS)
	{
	    llHitRow = llMiddle;
	    if (lpLookRow >= 0)
	    {
		if(prlTmpIndexRow->DataRowNum == lpLookRow)
		{
		    *plpFirst = llMiddle;
		    *plpLast = llMiddle;
		}
	    }
	    if (*plpFirst < 0)
	    {
	    /** run backward through index until key doesn't match anymore */
	    while (llComp == 0 && llMiddle > 0)
	    {
		prlTmpIndexRow = prlTmpWrapper[llMiddle-1].IndexRow;
		
		llComp = BCompare (prpIndex, pvpKey, prlTmpIndexRow->KeyValue);
		if (llComp == 0)
		{
		    llMiddle--;
		    if (lpLookRow >= 0)
		    {
			if(prlTmpIndexRow->DataRowNum == lpLookRow)
			{
			    *plpFirst = llMiddle;
			    *plpLast = llMiddle;
			    break;
			}
		    }
		}
		else
		{
		    break;
		}
	    }
	    }
	    if (*plpLast < 0)
	    {
	    llMiddle = max (llMiddle, 0); /* avoid underrun */
	    *plpFirst = llMiddle;
	    /** run forward through index until key doesn't match anymore */
	    llMiddle = llHitRow;
	    if (llMiddle < prpIndex->UsedIndexRows-1)
	    {
		do
		{
		    prlTmpIndexRow = prlTmpWrapper[llMiddle + 1].IndexRow;
		    llComp = BCompare (prpIndex, pvpKey, prlTmpIndexRow->KeyValue);
		    if (llComp == 0)
		    {
			llMiddle++;
		    if (lpLookRow >= 0)
		    {
			if(prlTmpIndexRow->DataRowNum == lpLookRow)
			{
			    *plpFirst = llMiddle;
			    *plpLast = llMiddle;
			    break;
			}
		    }
		    }
		    else
		    {
			break;
		    }
		}
		while (llComp == 0 && llMiddle < prpIndex->UsedIndexRows - 1);
	    }
	    llMiddle = min (llMiddle, prpIndex->UsedIndexRows-1); /* avoid overrun */
	    *plpLast = llMiddle;
	    }
	}
	else
	{

	    /* we found no matching key */
	    llMiddle = max (llMiddle - 1, -1); /* avoid underrun */
	    if (bpLookOnly != TRUE)
	    {
		/** run forward through index until key is greater */
		if (llMiddle < prpIndex->UsedIndexRows - 1)
		{
		    do
		    {
			prlTmpIndexRow = prlTmpWrapper[llMiddle + 1].IndexRow;
			llComp = BCompare (prpIndex, pvpKey, prlTmpIndexRow->KeyValue);
			if (llComp > 0)
			{
			    llMiddle++;
			}
			else
			{
			    break;
			}
		    }
		    while (llMiddle < prpIndex->UsedIndexRows - 1);
		}
	    }
	    llMiddle = min (llMiddle, prpIndex->UsedIndexRows - 1); /* avoid overrun */
	    *plpFirst = llMiddle;
	    *plpLast = llMiddle;
	    /*dbg(TRACE,"KEY <%s> NEW POS AT %d - %d",pvpKey,*plpFirst,*plpLast); */
	}
    }
    return ilRc;
}


char *AATArrayGetErrorMsg (short spErrno)
{
    int slErrno;
    if (spErrno == RC_NOTFOUND)
    {
	return pcgNotFound;
    }
    slErrno = abs (spErrno);
    if (slErrno > MAXERRORMESSAGES)
    {
	slErrno = UNKNOWN_ERROR;
    }
    return pcgErrorMessages[slErrno];
}

/***********************************************************************************/
/******                                                                     ********/
/******                                                                     ********/
/****** functions for internal use                                          ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
/***********************************************************************************/
/******                                                                     ********/
/****** Search for free row in array, if there is no free row, extend array ********/
/****** the found row is extracted from freelist                            ********/
/****** Parameter:   prpArray                                               ********/
/******              pipIndex  filled with index of new created row         ********/
/******                                                                     ********/
/****** Returnvalue: RC_SUCCESS ,RC_NOMEM                                   ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static AATARRAYROW *GetNewRow (AATARRAY * prpArray, long *plpIndex)
{
    int ilRc = RC_SUCCESS;
    if (prpArray->FirstFree == -1)
    {

	/* no more free rows */
	long llMaxRows;
	void *pvlRows;
	llMaxRows = prpArray->MaxRows + prpArray->Extent;
	pvlRows = realloc (prpArray->Rows, sizeof (AATARRAYROW) * (llMaxRows + 4));
	if (pvlRows == NULL)
	{
	    ilRc = RC_NOMEM;
	}
	if (ilRc == RC_SUCCESS)
	{
	    long llIndexNo;
	    prpArray->Rows = (AATARRAYROW *) pvlRows;
	    InitializeRows (prpArray, prpArray->MaxRows, llMaxRows);
	    prpArray->MaxRows = llMaxRows;
	    InitializeFreeList (prpArray);
	    if (prpArray->KeyArray != NULL)
	    {
		for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount; llIndexNo++)
		{
		    long llIdxLine;
		    long llFirstNewIdx;
		    AATARRAYINDEX * prlTmpIndex;
		    prlTmpIndex = &prpArray->KeyArray[llIndexNo];
		    if (*prlTmpIndex->Name != '\0')
		    {
			llFirstNewIdx = prlTmpIndex->MaxIndexRows;
			prlTmpIndex->MaxIndexRows = prpArray->MaxRows;
			prlTmpIndex->IndexWrapper = (AA_INDEXWRAPPER *)realloc(prlTmpIndex->IndexWrapper,sizeof(AA_INDEXWRAPPER)*prlTmpIndex->MaxIndexRows);
			if (prlTmpIndex->IndexWrapper != NULL)
			{
			    prlTmpIndex->FreeIndexRows = prlTmpIndex->MaxIndexRows - prlTmpIndex->UsedIndexRows;
			    for (llIdxLine = llFirstNewIdx;llIdxLine < prlTmpIndex->MaxIndexRows;llIdxLine++)
			    {
				prlTmpIndex->IndexWrapper[llIdxLine].IndexRow = NULL;
			    }
			}
			else
			{
			    ilRc = RC_NOMEM;
			}
		    }
		}
	    }
	}
    }
    if (prpArray->FirstFree > -1)
    {
	long llFirstFree;
	llFirstFree = prpArray->Rows[prpArray->FirstFree].NextFree;
	*plpIndex = prpArray->FirstFree;
	prpArray->FirstFree = llFirstFree;
	return (&prpArray->Rows[*plpIndex]);
    }
    return NULL;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Add new row to array prpArray, filled with pcpData                  ********/
/****** Parameter:   prpArray                                               ********/
/******              pcpData   data for new row                             ********/
/******              pipIndex  filled with index of new created row         ********/
/******                                                                     ********/
/****** Returnvalue: RC_SUCCESS ,RC_NOMEM                                   ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
int AddRow (AATARRAY * prpArray, void *pvpData, HANDLE * plpIndex)
{
    int ilRc = RC_SUCCESS;
    long llMaxRows;
    if (rgBaseArray.FirstFree == -1)
    {

	/* no more free rows */
	void *pvlRows;
	dbg (TRACE, "REALLOC BaseArray (Initialize for %d Arrays was too small)", rgBaseArray.MaxRows);
	llMaxRows = rgBaseArray.MaxRows + lgDefArrayCount;
	pvlRows = realloc (rgBaseArray.Rows, sizeof (AATARRAYROW) * (llMaxRows));
	if (pvlRows == NULL)
	{
	    ilRc = RC_NOMEM;
	}
	if (ilRc == RC_SUCCESS)
	{
	    rgBaseArray.Rows = (AATARRAYROW *) pvlRows;
	    InitializeRows (&rgBaseArray, rgBaseArray.MaxRows, llMaxRows);

/* 
   WRONG TWE (before InitializeFreeList) 
*/
	    rgBaseArray.MaxRows = llMaxRows;
	    InitializeFreeList (&rgBaseArray);
	}
    }
    if (prpArray->FirstFree > -1)
    {
	long llFirstFree;
	llMaxRows = prpArray->MaxRows + prpArray->Extent;
	InitializeRows (prpArray, prpArray->MaxRows, llMaxRows);
	InitializeFreeList (prpArray);
	llFirstFree = prpArray->Rows[prpArray->FirstFree].NextFree;
	prpArray->Rows[prpArray->FirstFree].DataOrig = pvpData;
	*plpIndex = prpArray->FirstFree;
	prpArray->FirstFree = llFirstFree;
	prpArray->RowCount++;
    }
    return ilRc;
}

static int ReorgIndexes(AATARRAY *prpArray)
{
  int ilRc = RC_SUCCESS;
  AATARRAYROW *prlTmpRow;
  AATARRAYINDEX *prlTmpIndex;
  AATINDEXROW *prlTmpIndexRow = NULL;
  AA_INDEXWRAPPER *prlTmpWrapper = NULL;
  long llIndexNo;
  long llIdxRow = 0;
  long llDatRow = 0;
  long llValid = 0;
  for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount; llIndexNo++)
  {
    prlTmpIndex = &prpArray->KeyArray[llIndexNo];
    if (prlTmpIndex->IsActive == TRUE)
    {
      prlTmpWrapper = prlTmpIndex->IndexWrapper;
      if (prlTmpWrapper != NULL)
      {
        llValid = 0;
        for (llIdxRow=0;llIdxRow<prlTmpIndex->UsedIndexRows;llIdxRow++)
        {
          prlTmpIndexRow = prlTmpWrapper[llIdxRow].IndexRow;
          llDatRow = prlTmpIndexRow->DataRowNum;
          prlTmpRow = &prpArray->Rows[llDatRow];
          if (prlTmpRow->RowStatus == 0)
          {
            prlTmpWrapper[llValid].IndexRow = prlTmpWrapper[llIdxRow].IndexRow;
            llValid++;
          }
        }
        prlTmpIndex->UsedIndexRows = llValid;
        prlTmpIndex->FreeIndexRows = prlTmpIndex->MaxIndexRows - llValid;
        TraceIndex(prpArray,prlTmpIndex);
      }
    }
  }
  prpArray->ReorgPending = FALSE;
  return ilRc;
}

static int DeleteRow (AATARRAY * prpArray, long lpRowNum,BOOL bpReorg)
{
    int ilRc = RC_SUCCESS;
    AATARRAYROW *prlTmpRow;
    long llIndexNo;
    char *pclData;
    prlTmpRow = &prpArray->Rows[lpRowNum];
    prlTmpRow->ChangeFlag = ARR_DATA_EMPTY;
    pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
    if (pclData != NULL && prpArray->KeyArray != NULL)
    {
	if (bpReorg == TRUE)
	{
	for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount; llIndexNo++)
	{
	    AATARRAYINDEX *prlTmpIndex;
	    prlTmpIndex = &prpArray->KeyArray[llIndexNo];
	    if (prlTmpIndex->IsActive == TRUE)
	    {
		if (prlTmpIndex->IndexWrapper != NULL)
		{
		    CreateKey (prpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
		    DeleteIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyNew, lpRowNum);
		}
	    }
	}
	}
    }

    if (prlTmpRow->DataNew != NULL)
    {
	free (prlTmpRow->DataNew);
	prlTmpRow->DataNew = NULL;
    }
    if (prlTmpRow->DataOrig != NULL)
    {
	free (prlTmpRow->DataOrig);
	prlTmpRow->DataOrig = NULL;
    }
    if (prlTmpRow->FieldOffsetsOrig != NULL)
    {
	free (prlTmpRow->FieldOffsetsOrig);
	prlTmpRow->FieldOffsetsOrig = NULL;
    }
    if (prlTmpRow->FieldOffsetsNew != NULL)
    {
	free (prlTmpRow->FieldOffsetsNew);
	prlTmpRow->FieldOffsetsNew = NULL;
    }

    if (prlTmpRow->RowStatus == 0)
    {
    if (prpArray->FirstFree != -1)
    {
	prpArray->Rows[lpRowNum].NextFree = prpArray->Rows[prpArray->FirstFree].NextFree;
	prpArray->Rows[prpArray->FirstFree].NextFree = lpRowNum;
    }

    else
    {
	prpArray->FirstFree = lpRowNum;
	prpArray->Rows[prpArray->FirstFree].NextFree = -1;
    }
    }

    prlTmpRow->RowStatus = 1;

    return ilRc;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Intitialize a range of rows of an array to default values (eg. 0,NULL)******/
/****** Parameter:   prpArray                                               ********/
/******              ipStart    first row to initialize                     ********/
/******              ipEnd      last row + 1 to initialize                  ********/
/******                                                                     ********/
/****** Returnvalue: RC_SUCCESS ,RC_INVALID,RC_NOMEM                        ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static int InitializeRows (AATARRAY * prpArray, long lpStart, long lpEnd)
{
    int ilRc = RC_SUCCESS;
    if ((lpStart > lpEnd) || lpStart < 0)
    {
	ilRc = RC_INVALID;
    }
    if (ilRc == RC_SUCCESS)
    {
	long llRowNum;
	for (llRowNum = lpStart; llRowNum < lpEnd && ilRc == RC_SUCCESS; llRowNum++)
	{
	    AATARRAYROW *prlTmpRow;
	    prlTmpRow = &prpArray->Rows[llRowNum];
	    prlTmpRow->NextFree = -1;
	    prlTmpRow->SizeOrig = 0;
	    prlTmpRow->SizeNew = 0;
	    prlTmpRow->ChangeFlag = ARR_DATA_EMPTY;
	    prlTmpRow->DataOrig = NULL;
	    prlTmpRow->DataNew = NULL;
	    if (prpArray->FieldOffsets == NULL)
	    {
		prlTmpRow->FieldOffsetsOrig = calloc (sizeof (long), prpArray->FieldCount + 4);
		if (prlTmpRow->FieldOffsetsOrig == NULL)
		{
		    ilRc = RC_NOMEM;
		}
		if (ilRc == RC_SUCCESS)
		{
		    prlTmpRow->FieldOffsetsNew = calloc (sizeof (long), prpArray->FieldCount + 4);
		    if (prlTmpRow->FieldOffsetsNew == NULL)
		    {
			free (prlTmpRow->FieldOffsetsOrig);
			prlTmpRow->FieldOffsetsOrig = NULL;
			ilRc = RC_NOMEM;
		    }
		}
	    }

	    else
	    {
		prlTmpRow->FieldOffsetsOrig = NULL;
		prlTmpRow->FieldOffsetsNew = NULL;
	    }
	}
    }
    return ilRc;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Recalculates the free list for the whole array                      ********/
/****** Parameter:   prpArray                                               ********/
/******                                                                     ********/
/****** Returnvalue: RC_SUCCESS                                             ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static int InitializeFreeList (AATARRAY * prpArray)
{
    int ilRc = RC_SUCCESS;
    long llRowNum;
    long llPrevFreeRow = -1;

    /* Begin with last because if overflow FirstFree will be -1 => realloc */
    for (llRowNum = 0; llRowNum < prpArray->MaxRows && ilRc == RC_SUCCESS; llRowNum++)
    {
	AATARRAYROW *prlTmpRow;
	prlTmpRow = &prpArray->Rows[llRowNum];
	if (prlTmpRow->DataOrig == NULL && prlTmpRow->DataNew == NULL)
	{

	    /* free row found */
	    if (llPrevFreeRow != -1)
	    {
		prlTmpRow->NextFree = llPrevFreeRow;
	    }
	    llPrevFreeRow = llRowNum;
	}
    }
    if (llPrevFreeRow != -1)
    {
	prpArray->FirstFree = llPrevFreeRow;
    }
    return ilRc;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Search in for given pcpArrayName in rgBaseArray                     ********/
/****** Parameter:   pcpArrayName                                           ********/
/******                                                                     ********/
/****** Returnvalue: HANDLE slIndex   handle of found array, -1 if not found********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static HANDLE SearchArray (char *pcpArrayName)
{
    HANDLE slIndex;
    BOOL blFound = FALSE;
    for (slIndex = rgBaseArray.MaxRows - 1; slIndex >= 0 && blFound == FALSE; slIndex--)
    {
	AATARRAY *prlTmpArray;
	prlTmpArray = (AATARRAY *) rgBaseArray.Rows[slIndex].DataOrig;
	if (prlTmpArray->Name != NULL)
	{
	    if (!strcmp (prlTmpArray->Name, pcpArrayName))
	    {
		blFound = TRUE;
	    }
	}
    }
    slIndex++;
    if (!blFound)
    {
	slIndex = -1;
    }
    return slIndex;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Calculates the field offsets in FieldOffsets of prpData             ********/
/****** Parameter:   prpArray,prpData                                       ********/
/******                                                                     ********/
/****** Returnvalue: RC_SUCCESS,RC_INVALID                                  ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static int CalculateFieldOffsets (AATARRAY * prpArray, long *plpFieldOffsets, long *plpNewSize, void *pvpData)
{
    int ilRc = RC_SUCCESS;
    long llFieldNo;
    long llCharNo = 0;
    char *pcpSrc;
    pcpSrc = (char *) pvpData;
    plpFieldOffsets[0] = 0;
    for (llFieldNo = 1; llFieldNo < prpArray->FieldCount; llFieldNo++)
    {
	while (pcpSrc[llCharNo] != '\0')
	{
	    llCharNo++;
	}
	plpFieldOffsets[llFieldNo] = ++llCharNo;
    }
    *plpNewSize = llCharNo;
    return ilRc;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Search in for given pcpArrayName in rgBaseArray                     ********/
/****** Parameter:   pcpArrayName                                           ********/
/******                                                                     ********/
/****** Returnvalue: pointer to found array in *prpArray                    ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static int FindArray (HANDLE * pspArray, char *pcpArrayName, AATARRAY ** prpArray)
{
    int ilRc = RC_SUCCESS;
    *prpArray = NULL;
    if (rgBaseArray.Rows == NULL)
    {
	ilRc = RC_NOTINITIALIZED;
    }
    if (ilRc == RC_SUCCESS)
    {
	if (*pspArray < 0 && pcpArrayName == NULL)
	{
	    ilRc = RC_INVALID;
	}
    }
    if ((ilRc == RC_SUCCESS) && (*pspArray < 0))
    {
	if ((*pspArray = SearchArray (pcpArrayName)) < 0)
	{
	    ilRc = RC_INVALID;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	ilRc = RC_INVALID;
	if (*pspArray >= 0 && *pspArray < rgBaseArray.MaxRows)
	{
	    if (rgBaseArray.Rows[*pspArray].DataOrig != NULL)
	    {
		*prpArray = (AATARRAY *) rgBaseArray.Rows[*pspArray].DataOrig;
		ilRc = RC_SUCCESS;
                /*
		if (!strcmp ((*prpArray)->Name, pcpArrayName))
		{
		    ilRc = RC_SUCCESS;
		}
                */
	    }
	}
    }
    return ilRc;
}

/***********************************************************************************/
/******                                                                     ********/
/****** Search in for given pcpFieldName in prpArray->Fieldlist             ********/
/****** Parameter:   pcpArrayName                                           ********/
/******                                                                     ********/
/****** Returnvalue: sort slFieldIndex, index of found field                ********/
/******                                                                     ********/
/******                                                                     ********/
/***********************************************************************************/
static long FindField (char *pcpFieldList, char *pcpFieldName)
{
    long llFieldNo = -1;
    char *pclField;
    char clHelp;
  
    if ((*pcpFieldName) && (strcmp(pcpFieldName,"") !=0 ))
    {
	pclField = strstr (pcpFieldList, pcpFieldName);
	if (pclField != NULL)
	{
	    long llOffset;
	    llOffset = pclField - pcpFieldList;
	    clHelp = pcpFieldList[llOffset];
	    memset (pcpFieldList + llOffset, '\0', 1);
	    llFieldNo = TWECountDelimiter (pcpFieldList, ",");
	    memset (pcpFieldList + llOffset, clHelp, 1);
	    if (llFieldNo > 0)
	    {
		llFieldNo--;		  /* start with 0  */
	    }
	}
    }
    return llFieldNo;
}
static int GetField (AATARRAY * prpArray, AATARRAYROW * prpRow, long lpFieldNumber, long lpMaxBytes, void *pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    char *pcpSrc;
    char *pcpDest;
    long llCount;
    pcpDest = pvpDataBuf;
    if (prpRow->DataNew != NULL)
    {
	pcpSrc =
	    (char *) &prpRow->DataNew[prpArray->FieldOffsets !=
				      NULL ? prpArray->FieldOffsets[lpFieldNumber] : prpRow->FieldOffsetsNew[lpFieldNumber]];}
    else if (prpRow->DataOrig != NULL)
    {
	pcpSrc =
	    (char *) &prpRow->DataOrig[prpArray->FieldOffsets !=
				       NULL ? prpArray->FieldOffsets[lpFieldNumber] :
				       prpRow->FieldOffsetsOrig[lpFieldNumber]];}
    for (llCount = 0; *pcpSrc != '\0' && llCount < lpMaxBytes - 1; llCount++)
    {
	*pcpDest++ = *pcpSrc++;
    }
    if (*pcpSrc != '\0')
    {
	dbg (TRACE, "GetField: Buffersize %ld is too small", lpMaxBytes);
	ilRc = RC_FAIL;
    }
    if (lpMaxBytes > 0)
    {
	*pcpDest = '\0';
    }
    return ilRc;
}
static int PutField (AATARRAY * prpArray, AATARRAYROW * prpRow, BOOL bpNewData, long lpFieldNumber, void *pvpDataBuf)
{
    int ilRc = RC_SUCCESS;
    char *pclSrc;
    char *pclDest;
    char *pclData;
    long llCount;
    long llRowLen;
    if (prpArray->RowLen == 0)
    {
	llRowLen = prpRow->SizeOrig;
    }

    else
    {
	llRowLen = prpArray->RowLen;
    }
    if (prpRow->DataOrig == NULL)
    {
	prpRow->DataOrig = (void *) calloc (sizeof (char), llRowLen + 4);
	if (prpRow->DataOrig == NULL)
	{
	    ilRc = RC_NOMEM;
	}
    }
    if (bpNewData && prpRow->DataNew == NULL)
    {
	prpRow->DataNew = (void *) calloc (sizeof (char), llRowLen + 4);
	if (prpRow->DataNew == NULL)
	{
	    ilRc = RC_NOMEM;
	}
    }
  if (ilRc == RC_SUCCESS)
  {
		if (bpNewData)
		{
			pclData = prpRow->DataNew;
			/*** set ChangeFlag if it is not a logical field **/
			if ((lpFieldNumber < prpArray->SqlFieldCount) && (prpRow->ChangeFlag == ARR_DATA_EMPTY ||  prpRow->ChangeFlag == ARR_DATA_UNCHANGED))
			{
				/*dbg(TRACE,"CHANGED SQL FIELD"); */
				prpRow->ChangeFlag = ARR_DATA_CHANGED;
			}
		}
		else
		{
				pclData = prpRow->DataOrig;
		}
  }
    if (ilRc == RC_SUCCESS)
    {
	pclDest =
	    (char *) &pclData[prpArray->FieldOffsets !=
			      NULL ? prpArray->FieldOffsets[lpFieldNumber] : prpRow->FieldOffsetsOrig[lpFieldNumber]];
	pclSrc = pvpDataBuf;
	for (llCount = 0; *pclSrc != '\0' && llCount < prpArray->FieldLen[lpFieldNumber]; llCount++)
	{
	    *pclDest++ = *pclSrc++;
	}
	for (; llCount < prpArray->FieldLen[lpFieldNumber]; llCount++)
	{
	    *pclDest++ = ' ';
	}
	*pclDest = '\0';
    }
    return ilRc;
}
static int FindRow (AATARRAY * prpArray, long *plpRowNum, BOOL bpFindAll)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = -1;
    AATARRAYROW *prlTmpRow;
    switch (*plpRowNum)
    {
	case ARR_FIRST:
	    *plpRowNum = -1;
	    for (llRowNum = 0; llRowNum < prpArray->MaxRows && *plpRowNum == -1; llRowNum++)
	    {
		prlTmpRow = &prpArray->Rows[llRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
			ilRc = RC_SUCCESS;
		    }

		    else
		    {
			ilRc = RC_ROWDELETED;
		    }
		}
	    }
	    break;
	case ARR_LAST:
	    *plpRowNum = -1;
	    for (llRowNum = prpArray->MaxRows - 1; llRowNum >= 0 && *plpRowNum == -1; llRowNum--)
	    {
		prlTmpRow = &prpArray->Rows[llRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
			ilRc = RC_SUCCESS;
		    }

		    else
		    {
			ilRc = RC_ROWDELETED;
		    }
		}
	    }
	    break;
	case ARR_CURRENT:
	    *plpRowNum = -1;
	    for (llRowNum = prpArray->CurrentRow; llRowNum < prpArray->MaxRows && *plpRowNum == -1; llRowNum++)
	    {
		prlTmpRow = &prpArray->Rows[llRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
			ilRc = RC_SUCCESS;
		    }

		    else
		    {
			ilRc = RC_ROWDELETED;
		    }
		}
	    }
	    break;
	case ARR_NEXT:
	    *plpRowNum = -1;
	    for (llRowNum = prpArray->CurrentRow + 1; llRowNum < prpArray->MaxRows && *plpRowNum == -1; llRowNum++)
	    {
		prlTmpRow = &prpArray->Rows[llRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
			ilRc = RC_SUCCESS;
		    }

		    else
		    {
			ilRc = RC_ROWDELETED;
		    }
		}
	    }
	    break;
	case ARR_PREV:
	    *plpRowNum = -1;
	    for (llRowNum = prpArray->CurrentRow - 1; llRowNum >= 0 && *plpRowNum == -1; llRowNum--)
	    {
		prlTmpRow = &prpArray->Rows[llRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			*plpRowNum = llRowNum;
			prpArray->CurrentRow = llRowNum;
			ilRc = RC_SUCCESS;
		    }

		    else
		    {
			ilRc = RC_ROWDELETED;
		    }
		}
	    }
	    break;
	default:
	    ilRc = RC_INVALID;
	    if ((*plpRowNum < prpArray->MaxRows) && (*plpRowNum >= 0))
	    {
		prlTmpRow = &prpArray->Rows[*plpRowNum];
		if ((prlTmpRow->DataNew != NULL) || (prlTmpRow->DataOrig != NULL))
		{
		    if (bpFindAll == TRUE)
		    {
			ilRc = RC_SUCCESS;
		    }

		    else if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
		    {
			ilRc = RC_SUCCESS;
		    }
		}
		/*	hag 06.03.2006:  Wrong condition: "if (ilRc = RC_SUCCESS)" may have led to unpredictable results   */
		/*
		if (ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"INVALID LINE %ld ",*plpRowNum);
		}*/
	    }
    }
    if ((*plpRowNum == -1) && (ilRc == RC_SUCCESS))
    {
	ilRc = RC_NOTFOUND;
    }
    return ilRc;
}

int GetNewIndex (AATARRAY * prpArray, char *pcpIndexName, AATARRAYINDEX ** prpIndex, HANDLE * plpHandle)
{
    int ilRc = RC_SUCCESS;
    long llIndexNo;
    long llFirstFreeIndex = -1;
    *prpIndex = NULL;
    if (prpArray->KeyArray == NULL)
    {
	prpArray->KeyArray = calloc (sizeof (AATARRAYINDEX), lgDefIndexCount);
	if (prpArray->KeyArray == NULL)
	{
	    ilRc = RC_NOMEM;
	}

	else
	{
	    prpArray->KeyArrayCount = lgDefIndexCount;
	    for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount; llIndexNo++)
	    {
		prpArray->KeyArray[llIndexNo].Name[0] = 0x00;
	    }
	}
    }

    /* check for duplicate IndexName and search free entry in index array */
    for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount && ilRc == RC_SUCCESS; llIndexNo++)
    {
	if (!(*prpArray->KeyArray[llIndexNo].Name))
	{
	    if (llFirstFreeIndex == -1)
	    {
		llFirstFreeIndex = llIndexNo;
	    }
	}

	else if (!strcmp (prpArray->KeyArray[llIndexNo].Name, pcpIndexName))
	{
	    ilRc = RC_DUPLICATE;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	if (llFirstFreeIndex == -1)
	{

	    /* no free index found, allocate some more rows in indexarray */
	    AATARRAYINDEX *prlIndexRows;
	    prlIndexRows = realloc (prpArray->KeyArray, sizeof (AATARRAYINDEX) * (prpArray->KeyArrayCount + lgDefIndexCount));
	    if (prlIndexRows == NULL)
	    {
		ilRc = RC_NOMEM;
	    }

	    else
	    {
		long llNewIndexCount;
		long llNewKeyNo;
		prpArray->KeyArray = prlIndexRows;
		llFirstFreeIndex = prpArray->KeyArrayCount;
		llNewIndexCount = prpArray->KeyArrayCount + lgDefIndexCount;
		for (llNewKeyNo = llFirstFreeIndex; llNewKeyNo < llNewIndexCount; llNewKeyNo++)
		{
		    memset (&prpArray->KeyArray[llNewKeyNo], 0, sizeof (AATARRAYINDEX));
		}
		prpArray->KeyArrayCount += lgDefIndexCount;
	    }
	}
	*prpIndex = &prpArray->KeyArray[llFirstFreeIndex];
	*plpHandle = llFirstFreeIndex;
    }
    return ilRc;
}
static int FindIndex (AATARRAY * prpArray, HANDLE * pspIndex, char *pcpIndexName, AATARRAYINDEX ** prpIndex)
{
    int ilRc = RC_NOTFOUND;
    long llFoundIndex = -1;
    long llIndexNo;
    if (*pspIndex >= 0)
    {
	*prpIndex = &prpArray->KeyArray[*pspIndex];
	ilRc = RC_SUCCESS;
    }
    else
    {
    for (llIndexNo = 0; llIndexNo < prpArray->KeyArrayCount && llFoundIndex == -1; llIndexNo++)
    {
	if (*prpArray->KeyArray[llIndexNo].Name && (!strcmp (pcpIndexName, prpArray->KeyArray[llIndexNo].Name)))
	{
	    llFoundIndex = llIndexNo;
	    ilRc = RC_SUCCESS;
	}
    }
    if (ilRc == RC_SUCCESS)
    {
	*prpIndex = &prpArray->KeyArray[llFoundIndex];
	*pspIndex = llFoundIndex;
    }
    }
    return ilRc;
}

int CreateIndexWrapper (AATARRAY * prpArray, AATARRAYINDEX * prpIndex)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = 0;
    long llIdxRow = 0;
    char *pclData = NULL;
    AATARRAYROW *prlTmpRow;
    AATINDEXROW *prlTmpIndexRow;
    AA_INDEXWRAPPER *prlTmpWrapper;

    /* dbg (TRACE, "CreateIndexWrapper (Create Keys)"); */

    prlTmpWrapper = prpIndex->IndexWrapper;

    prpIndex->FreeIndexRows = prpIndex->MaxIndexRows;
    prpIndex->UsedIndexRows = 0;

    for (llRowNum = 0; llRowNum < prpArray->MaxRows; llRowNum++)
    {
	prlTmpRow = &prpArray->Rows[llRowNum];
	if (prlTmpRow->DataNew != NULL || prlTmpRow->DataOrig != NULL)
	{
	    if ((prlTmpRow->ChangeFlag != ARR_DATA_DELETED) && (prlTmpRow->ChangeFlag != ARR_DATANEW_DELETED))
	    {
		prlTmpIndexRow = prlTmpWrapper[llIdxRow].IndexRow;
		if (prlTmpIndexRow == NULL)
		{
		    prlTmpIndexRow = (AATINDEXROW *)malloc(sizeof(AATINDEXROW));
		    prlTmpIndexRow->KeyValue = (char *)malloc(prpIndex->TotalKeyLen + 1);
		    prlTmpWrapper[llIdxRow].IndexRow = prlTmpIndexRow;
		}
		prlTmpIndexRow->DataRowNum = llRowNum;
		pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
		ilRc = CreateKey (prpArray, prpIndex, pclData, prlTmpIndexRow->KeyValue);
		llIdxRow++;
	    }
	}
    }
    prpIndex->UsedIndexRows = llIdxRow;
    prpIndex->FreeIndexRows = prpIndex->MaxIndexRows - llIdxRow;
    
    if (llIdxRow > 0)
    {
	/* dbg (TRACE, "Sorting Index follows"); */
	CurIndexObject = prpIndex;
	qsort((AA_INDEXWRAPPER *)prpIndex->IndexWrapper,prpIndex->UsedIndexRows,sizeof(AA_INDEXWRAPPER),CompareIndex);
	/* dbg (TRACE, "after Sorting Index"); */
    }
    else
    {
	dbg(DEBUG,"Index is empty, no sorting required");
    }
    TraceIndex(prpArray, prpIndex);
    /* dbg (TRACE, "CreateIndexWrapper (Create Keys) Finished"); */
    return ilRc;
}

static void TraceIndex(AATARRAY *prpArray, AATARRAYINDEX *prpIndex)
{
    dbg(DEBUG,"INDEX <%s><%s> ROWS: MAX=%d USED=%d FREE=%d",prpArray->Name,prpIndex->Name,
        prpIndex->MaxIndexRows,prpIndex->UsedIndexRows,prpIndex->FreeIndexRows);
  return;
}

static int CompareIndex(const void *_dp1, const void *_dp2)
{
    const AA_INDEXWRAPPER *dp1 = (const AA_INDEXWRAPPER *) _dp1;
    const AA_INDEXWRAPPER *dp2 = (const AA_INDEXWRAPPER *) _dp2;
    AATINDEXROW *dl1 = dp1->IndexRow;
    AATINDEXROW *dl2 = dp2->IndexRow;
    return (BCompare (CurIndexObject, dl1->KeyValue, dl2->KeyValue));
} /* End */

static int CreateKey (AATARRAY * prpArray, AATARRAYINDEX * prpIndex, char *pcpData, char *pcpKey)
{
    int ilRc = RC_SUCCESS;
    if (pcpData == NULL)
    {
	ilRc = RC_INVALID;
    }
    if (ilRc == RC_SUCCESS)
    {
	switch (prpIndex->KeyType)
	{
	    case ARR_LKEY_ITEM:

	    {
		long llItemNo;
		long llCount;
		char *pclDest;
		char *pclSrc;
		pclDest = pcpKey;
		for (llItemNo = 0; llItemNo < MAXKEYITEMS && prpIndex->IndexDescriptor[llItemNo].FieldItemNo != -1; llItemNo++)
		{
		    pclSrc = &pcpData[prpArray->FieldOffsets[prpIndex->IndexDescriptor[llItemNo].FieldItemNo]];
		    for (llCount = 0; *pclSrc != '\0' && llCount < prpArray->FieldLen[prpIndex->IndexDescriptor[llItemNo].FieldItemNo];
			 llCount++)
		    {
			*pclDest++ = *pclSrc++;
		    }
		    *pclDest = '\0';
		}
		*pclDest = '\0';
	    }
	    break;
	    default:
		ilRc = RC_INVALID;
	}
    }
    return ilRc;
}
static int InsertIndexKey (AATARRAYINDEX * prpIndex, char *pcpKey, long lpRowNum)
{
    int ilRc = RC_SUCCESS;
    long llLineNo = -1;
    long llFirstLine = -1;
    long llLastLine = -1;
    long llFirstRow = -1;
    long llLastRow = -1;
    AATINDEXROW *prlTmpIndexRow = NULL;
    AA_INDEXWRAPPER HelpLine;

    ilRc = BFindIndex (prpIndex, pcpKey, &llFirstRow, &llLastRow, -1,FALSE);
    /* dbg(TRACE,"REORG INDEX <%s> FOR INSERT",prpIndex->Name); */
    /* dbg(TRACE,"NEW INDEX <%s> FIRST=%d LAST=%d",pcpKey,llFirstRow,llLastRow); */

    llFirstLine = llLastRow + 1;
    llLastLine = prpIndex->UsedIndexRows;

    HelpLine.IndexRow = prpIndex->IndexWrapper[llLastLine].IndexRow;
    for (llLineNo=llLastLine;llLineNo>llFirstLine;llLineNo--)
    {
	prpIndex->IndexWrapper[llLineNo].IndexRow = prpIndex->IndexWrapper[llLineNo-1].IndexRow;
    }
    prpIndex->IndexWrapper[llFirstLine].IndexRow = HelpLine.IndexRow;

    prlTmpIndexRow = prpIndex->IndexWrapper[llFirstLine].IndexRow;
    if (prlTmpIndexRow == NULL)
    {
	prlTmpIndexRow = (AATINDEXROW *)malloc(sizeof(AATINDEXROW));
	prlTmpIndexRow->KeyValue = (char *)malloc(prpIndex->TotalKeyLen + 1);
	prpIndex->IndexWrapper[llFirstLine].IndexRow = prlTmpIndexRow;
    }
    prlTmpIndexRow->DataRowNum = lpRowNum;
    memcpy(prlTmpIndexRow->KeyValue,pcpKey,prpIndex->TotalKeyLen + 1);

    prpIndex->FreeIndexRows--;
    prpIndex->UsedIndexRows++;

    ilRc = RC_SUCCESS;
    return ilRc;
}
static int DeleteIndexKey (AATARRAYINDEX * prpIndex, char *pcpKey, long lpRowNum)
{
    int ilRc = RC_SUCCESS;
    long llLineNo = -1;
    long llFirstLine = -1;
    long llLastLine = -1;
    long llFirstRow = -1;
    long llLastRow = -1;
    AATINDEXROW *prlTmpIndexRow = NULL;
    AA_INDEXWRAPPER HelpLine;
    llLineNo = prpIndex->CurrIndexRow;
    if (llLineNo >= 0)
    {
      prlTmpIndexRow = prpIndex->IndexWrapper[llLineNo].IndexRow;
      if (prlTmpIndexRow->DataRowNum == lpRowNum)
      {
        llFirstRow = llLineNo;
        llLastRow = llLineNo;
        /* dbg(TRACE,"HIT CURRENT ROW"); */
      }
    }
    if (llFirstRow < 0)
    {
      ilRc = BFindIndex (prpIndex, pcpKey, &llFirstRow, &llLastRow,lpRowNum,TRUE);
    }
    if (ilRc == RC_SUCCESS)
    {
    	/* dbg(TRACE,"REORG INDEX <%s> FOR DELETE",prpIndex->Name); */
    	/* dbg(TRACE,"DEL FROM <%s> INDEX <%s> FIRST=%d LAST=%d",prpIndex->Name,pcpKey,llFirstRow,llLastRow); */
	/* Search RowNum  */
	for (llFirstRow=llFirstRow;llFirstRow<=llLastRow;llFirstRow++)
	{
	    prlTmpIndexRow = prpIndex->IndexWrapper[llFirstRow].IndexRow;
	    if (prlTmpIndexRow->DataRowNum == lpRowNum)
	    {
		llLineNo = llFirstRow;
		/* dbg(TRACE,"HIT INDEX-NAME <%s> INDEX <%s> FIRST=%d LAST=%d",prpIndex->Name,pcpKey,llFirstRow,llLastRow); */
		break;
	    }
	}
	if (prlTmpIndexRow->DataRowNum == lpRowNum)
	{
	    prlTmpIndexRow->DataRowNum = -1;

	    llFirstLine = llLineNo;
	    llLastLine = prpIndex->UsedIndexRows - 1;

	    HelpLine.IndexRow = prpIndex->IndexWrapper[llFirstLine].IndexRow;
	    for (llLineNo=llFirstLine;llLineNo<llLastLine;llLineNo++)
	    {
		prpIndex->IndexWrapper[llLineNo].IndexRow = prpIndex->IndexWrapper[llLineNo+1].IndexRow;
	    }
	    prpIndex->IndexWrapper[llLastLine].IndexRow = HelpLine.IndexRow;
	    prpIndex->FreeIndexRows++;
	    prpIndex->UsedIndexRows--;
	}
    }
    return ilRc;
}
static int UpdateIndexKey (AATARRAYINDEX * prpIndex, char *pcpOldKey, char *pcpNewKey, long lpRowNum)
{
    int ilRc = RC_SUCCESS;
    long llDeleteLine = -1;
    long llInsertLine = -1;
    long llLineNo = -1;
    long llFirstLine = -1;
    long llLastLine = -1;
    long llFirstRow = -1;
    long llLastRow = -1;
    AATINDEXROW *prlTmpIndexRow = NULL;
    AA_INDEXWRAPPER HelpLine;
    /* Find Old Key IdxLineNo to be removed */
    ilRc = BFindIndex (prpIndex, pcpOldKey, &llFirstRow, &llLastRow,-1,TRUE);
    if (ilRc == RC_SUCCESS)
    {
    	/* dbg(TRACE,"REORG INDEX <%s> FOR UPDATE",prpIndex->Name); */
    	/* dbg(TRACE,"DEL FROM <%s> INDEX <%s> FIRST=%d LAST=%d",prpIndex->Name,pcpOldKey,llFirstRow,llLastRow); */
	/* Search RowNum  */
	for (llFirstRow=llFirstRow;llFirstRow<=llLastRow;llFirstRow++)
	{
	    prlTmpIndexRow = prpIndex->IndexWrapper[llFirstRow].IndexRow;
	    if (prlTmpIndexRow->DataRowNum == lpRowNum)
	    {
		llLineNo = llFirstRow;
		/* dbg(TRACE,"HIT INDEX <%s> LINE=%d DATA=%d",pcpOldKey,llFirstRow,lpRowNum); */
		break;
	    }
	}
	if (prlTmpIndexRow->DataRowNum == lpRowNum)
	{
	    llDeleteLine = llLineNo;
	}
    }
    if (llDeleteLine < 0)
    {
	/* For any reason the OldKey of DataRowNum doesn't exist */
	/* THIS IS AN ERROR AND SHOULD BE SOLVED */
	dbg(TRACE,"WARNING: OLD KEY <%s> LINE %d NOT FOUND, rc: <%d>",pcpOldKey,lpRowNum,ilRc);
    if (ilRc == RC_SUCCESS)
    { /* 20040504 JIM: more debug if old index not found */
       ilRc = BFindIndex (prpIndex, pcpOldKey, &llFirstRow, &llLastRow,-1,TRUE);
 	     if (ilRc == RC_SUCCESS)
 	     {
	        dbg(TRACE," llFirstRow <%d>, llLastRow <%d> ", llFirstRow, llLastRow);
 	    	  /* debug RowNum  */
  	    	for (llFirstRow=llFirstRow;llFirstRow<=llLastRow;llFirstRow++)
  	    	{
 	    	    prlTmpIndexRow = prpIndex->IndexWrapper[llFirstRow].IndexRow;
	          dbg(TRACE,"%d: <%d> =? <%d>", llFirstRow, prlTmpIndexRow->DataRowNum,lpRowNum);
 	     	 }
 	     }
    }
	llDeleteLine = prpIndex->UsedIndexRows;
	prpIndex->FreeIndexRows--;
	prpIndex->UsedIndexRows++;
    }

    /* Find the New Key Position to be inserted */
    ilRc = BFindIndex (prpIndex, pcpNewKey, &llFirstRow, &llLastRow, -1,FALSE);
    /* dbg(TRACE,"NEW INDEX <%s> FIRST=%d LAST=%d",pcpNewKey,llFirstRow,llLastRow); */
    llInsertLine = llLastRow + 1;

    if (llInsertLine < llDeleteLine)
    {
	/* Shift all Pointers to upper lines */
	llFirstLine = llInsertLine + 1;
	llLastLine = llDeleteLine;

	HelpLine.IndexRow = prpIndex->IndexWrapper[llDeleteLine].IndexRow;
	for (llLineNo=llLastLine;llLineNo>=llFirstLine;llLineNo--)
	{
	    prpIndex->IndexWrapper[llLineNo].IndexRow = prpIndex->IndexWrapper[llLineNo-1].IndexRow;
	}
	prpIndex->IndexWrapper[llInsertLine].IndexRow = HelpLine.IndexRow;
    }
    if (llDeleteLine < llInsertLine)
    {
	/* Shift all Pointers to lower lines */
	llInsertLine--;
	llFirstLine = llDeleteLine;
	llLastLine = llInsertLine -1;

	HelpLine.IndexRow = prpIndex->IndexWrapper[llDeleteLine].IndexRow;
	for (llLineNo=llFirstLine;llLineNo<=llLastLine;llLineNo++)
	{
	    prpIndex->IndexWrapper[llLineNo].IndexRow = prpIndex->IndexWrapper[llLineNo+1].IndexRow;
	}
	prpIndex->IndexWrapper[llInsertLine].IndexRow = HelpLine.IndexRow;
    }

    prlTmpIndexRow = prpIndex->IndexWrapper[llInsertLine].IndexRow;
    if (prlTmpIndexRow == NULL)
    {
	prlTmpIndexRow = (AATINDEXROW *)malloc(sizeof(AATINDEXROW));
	prlTmpIndexRow->KeyValue = (char *)malloc(prpIndex->TotalKeyLen);
	prpIndex->IndexWrapper[llInsertLine].IndexRow = prlTmpIndexRow;
    }
    prlTmpIndexRow->DataRowNum = lpRowNum;
    memcpy(prlTmpIndexRow->KeyValue,pcpNewKey,prpIndex->TotalKeyLen);

    return ilRc;
}

static int BCompare (AATARRAYINDEX * prpIndex, char *pcpKey1, char *pcpKey2)
{
    int ilRc = RC_SUCCESS;
    int ilKeyNo;
    if (*pcpKey1 == '\0' && *pcpKey2 == '\0')
	return 0;

    for (ilKeyNo = 0; ilKeyNo < MAXKEYITEMS && prpIndex->IndexDescriptor[ilKeyNo].FieldItemNo > -1; ilKeyNo++)
    {
	if (pcpKey1[prpIndex->IndexDescriptor[ilKeyNo].KeyValStart] == 0)
	{
	    ilRc = 0;
	    break;
	}

	else
	{
	    ilRc =
		strncmp (&pcpKey1[prpIndex->IndexDescriptor[ilKeyNo].KeyValStart], 
		         &pcpKey2[prpIndex->IndexDescriptor[ilKeyNo].KeyValStart],
			 prpIndex->IndexDescriptor[ilKeyNo].KeyValLen);
	}
	if (ilRc == 0)
	{
	    continue;
	}
	if (prpIndex->IndexDescriptor[ilKeyNo].KeyValSort == ARR_DESC)
	{
	    ilRc = (ilRc < 0) ? 1 : -1;
	}
	break;
    }
    return ilRc;
}


#ifndef CPP
/*******************************************************************
                BEGIN OF CEDA FUNKTIONS EXTERNAL  (TWE)
********************************************************************/
/*****************************************************************************
 * Function: TWETrimRight
 * Parameter: String to trim
 * Description: String routines used in data conversion
 *              clears all blanks on the right side of the string
 ******************************************************************************/
static void TWETrimRight (char *s)
{
    int i = 0;

    /* search for last non-space character */
    for (i = strlen (s) - 1; i >= 0 && isspace (s[i]); i--);

    /* trim off right spaces */
    s[++i] = '\0';
}

/* ****************************************************************
 * Function:    TWEDel2NewDel
 * Parameter:   IN/OUT : pcpDelList     String to prepare
 *              IN     : pcpDel         Delimiter to change
 *                       pcpNewDel      New delimiter
 * Return:      Length of prepared string, RC_FAIL (-1)
 * Description: This function turns delimiters to new delimiter.
 *
 *              WARNING: OLD = '\0' to new isn't possible!!!!
 *
 *              If there is one delimiter directly behind 
 *              another a blanc will be added. Also if first or
 *              last sign are delimiters.
 ***************************************************************** */
static long TWEDel2NewDel (char *pcpDelList, char pcpDel, char pcpNewDel)
{
    int ilRC = RC_SUCCESS;
    long llLen = 0;
    long llBufCount = 0;
    long llCount = 0;
    short slFlag = FALSE;
    char pclDel[2] = "";
    static char *pclBuf = NULL;
    static long llBufSize = 0;
    if ((pcpDelList == NULL) || (*pcpDelList == '\0'))
    {
	return 0;
    }

    /* get size to alloc for local buffer */
    llLen = strlen (pcpDelList);
    pclDel[0] = pcpDel;
    pclDel[1] = 0x00;
    llCount = TWECountDelimiter (pcpDelList, pclDel);

    /* alloc local buffer */
    if ((pclBuf == NULL) || (llBufSize == 0))
    {
	pclBuf = (char *) (malloc (llLen + llCount + 1));
	if (pclBuf == NULL)
	{
	    dbg (TRACE, "TWEDel2NewDel: Malloc for pclBuf (%ld) failed", llLen + llCount + 1);
	    ilRC = RC_NOMEM;
	    return ilRC;
	}

	else
	{
	    llBufSize = llLen + llCount + 1;
	}
    }

    else if (llBufSize <= llLen + llCount)
    {

	/* realloc */
	pclBuf = (char *) (realloc (pclBuf, llLen + llCount + 1));
	if (pclBuf == NULL)
	{
	    ilRC = RC_NOMEM;
	    dbg (TRACE, "TWEDel2NewDel: Realloc for pclBuf (%ld) failed", llLen + llCount + 1);
	    return ilRC;
	}

	/* end if realloc fail */
	else
	{
	    llBufSize = llLen + llCount + 1;
	}
    }
    if (*(pcpDelList) == pcpDel)
    {                             /* insert blanc if first is delimiter */
	pclBuf[llBufCount] = ' ';
	llBufCount++;
	pclBuf[llBufCount] = pcpNewDel;
	llBufCount++;
    }

    /* end if */
    else
    {
	pclBuf[llBufCount] = *(pcpDelList);
	llBufCount++;
    }                             /* end else */
    if (*(pcpDelList + llLen - 1) == pcpDel)
    {                             /* get last sign */
	slFlag = TRUE;              /* last is komma */
    }

    /* end if */
    for (llCount = 1; llCount < llLen; llCount++)
    {                             /* from second to last */
	if (*(pcpDelList + llCount) == pcpDel)
	{
	    if (*(pcpDelList + llCount - 1) == pcpDel)
	    {                         /* insert blanc */
		pclBuf[llBufCount] = ' ';
		llBufCount++;
	    }

	    /* end if */
	    pclBuf[llBufCount] = pcpNewDel;
	    llBufCount++;
	}

	/* end if */
	else
	{
	    pclBuf[llBufCount] = *(pcpDelList + llCount);
	    llBufCount++;
	}                           /* end else */
    }                             /* end for */

    /* check if last is komma */
    if (slFlag == TRUE)
    {                             /* add blanc if last = delimiter */
	pclBuf[llBufCount] = ' ';
	llBufCount++;
    }

    /* end if */
    memcpy (pcpDelList, pclBuf, llBufCount);
    pcpDelList[llBufCount] = 0x00; /* terminate */
    return llBufCount;
}                               /* end of TWEDel2NewDel */

/* ****************************************************************
 * Function:    TWECountDelimiter
 * Parameter:   IN/OUT : pcpChkStr      check string
 *                       pcpDel         Delimiter string (1 or more)
 * Return:      Number of items
 * Description: Returns the Number of items in check string
 *              that are seperated by the delimiter.
 *              Examples: "abc"          => 1
 *                        "abc,def"      => 2
 *                        "abc,"         => 2
 *                        ",abc"         => 2
 *                        ",abc,"        => 3  
 *                        "abc,def,ghi"  => 3
 ***************************************************************** */
static long TWECountDelimiter (char *pcpChkStr, char *pcpDel)
{
    long llCount = 0;
    long llDelLen = 0;
    char *pclStrPtr = NULL;
    pclStrPtr = pcpChkStr;
    if (*pclStrPtr != '\0')
    {
	llDelLen = strlen (pcpDel);
	if (llDelLen > 1)
	{                           /*  words  */
	    while ((pclStrPtr = strstr (pclStrPtr, pcpDel)) != NULL)
	    {                         /* Del found */
		pclStrPtr += llDelLen;
		llCount++;
	    }
	}

	else
	{                           /* one sign  */
	    while (*pclStrPtr != '\0')
	    {
		if (*pclStrPtr == pcpDel[0])
		{                       /* Del found */
		    llCount++;
		}
		pclStrPtr++;
	    }
	}
	llCount++;
    }
    return llCount;
}                               /* end of TWECountDelimiter */

/* ***************************************************
 * Function:    TWEBuildSqlFieldStr
 * Parameter:   IN : pcpFldLst       field list
 *                   pcpAddLst       items to add to field list
 *                   ipTyp           kind of SQL string
 *              OUT: pcpSqlStrg      resulting SQL string
 * Return:      RC_SUCCESS, RC_FAIL
 * Description: Builds up the field list parts of insert
 *              and update SQL statements.  
 **************************************************** */
static int TWEBuildSqlFieldStr (char *pcpFldLst, char *pcpAddLst, int ipTyp, char *pcpSqlStrg)
{
    int ilRC = RC_SUCCESS;
    int ilStrLen = 0;             /*  index counter in sql string    */
    long llItemCnt = 0;           /*  num item in item strings       */
    long llItem = 0;              /*  index counter for item strings */
    long llItemLen = 0;           /*  length of one fieldstr item   */
    long llAddLen = 0;            /*  length of add string           */
    char pclTmpBuf[ARR_MAXSQLFLDLSTLEN + 4] = ""; /*  help buffer for sqlstr  */
    char pclItemNam[8] = "";      /*  name of actual item            */
    char pclItemVal[16] = "";     /*  sql string for this item       */
    char pclDel[4] = ",";
    char *pclItem = NULL;
    llAddLen = strlen (pcpAddLst);
    llItemCnt = TWECountDelimiter (pcpFldLst, ",");
    switch (ipTyp)
    {
	case FOR_UPDATE:
	    ilStrLen = 0;
	    pcpSqlStrg[ilStrLen] = 0x00;

	    /*  for all items found in field list  */
	    pclItem = pcpFldLst;
	    for (llItem = 1; llItem <= llItemCnt; llItem++)
	    {
		llItemLen = GetNextDataItem (pclItemNam, &pclItem, pclDel, "\0", "  ");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "TWEBuildSqlFieldStr: Fkt.: GetNextDataItem (%d) failed", llItemLen);
		}
		if (igWholeRowUpd == TRUE)
		{
		    if (strcmp (pclItemNam, "URNO") == 0)
		    {
		    }

		    else
		    {
			sprintf (pclItemVal, "%s=:V%s", pclItemNam, pclItemNam);
			StrgPutStrg (pcpSqlStrg, &ilStrLen, pclItemVal, 0, 2 * llItemLen + 2, ",");
		    }
		}

		else
		{
		    sprintf (pclItemVal, "%s=:V%s", pclItemNam, pclItemNam);
		    StrgPutStrg (pcpSqlStrg, &ilStrLen, pclItemVal, 0, 2 * llItemLen + 2, ",");
		}
	    }                         /* end for */

	    /*  terminate pcpSqlStrg */
	    if (ilStrLen > 0)
	    {                         /*  item found  */
		ilStrLen--;
		pcpSqlStrg[ilStrLen] = 0x00;
	    }

	    /* end if */
	    /*
	      RESULT:
	      >Field1=:VField1,Field2=:VField2,...,Fieldn=:VFieldn<
	    */
	    break;
	case FOR_INSERT:
	    ilStrLen = 0;
	    pclTmpBuf[ilStrLen] = 0x00;

	    /*  for all items found in field list  */
	    pclItem = pcpFldLst;
	    for (llItem = 1; llItem <= llItemCnt; llItem++)
	    {
		llItemLen = GetNextDataItem (pclItemNam, &pclItem, pclDel, "0", "  ");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "TWEBuildSqlFieldStr: Fkt.: GetNextDataItem (%d) failed", llItemLen);
		}
		sprintf (pclItemVal, ":V%s", pclItemNam);
		StrgPutStrg (pclTmpBuf, &ilStrLen, pclItemVal, 0, llItemLen + 1, ",");
	    }                         /* end for */

	    /*  for all items found in add list  */
	    if (llAddLen > 0)
	    {
		pcpAddLst[llAddLen - 1] = 0x00; /*  rem last komma  */
		llAddLen--;
		llItemCnt = TWECountDelimiter (pcpAddLst, ",");
		pclItem = pcpAddLst;
		for (llItem = 1; llItem <= llItemCnt; llItem++)
		{
		    llItemLen = GetNextDataItem (pclItemNam, &pclItem, pclDel, "0", "  ");
		    if (llItemLen < 0)
		    {
			dbg (TRACE, "TWEBuildSqlFieldStr: Fkt.: GetNextDataItem (%d) failed", llItemLen);
		    }
		    sprintf (pclItemVal, ":V%s", pclItemNam);
		    StrgPutStrg (pclTmpBuf, &ilStrLen, pclItemVal, 0, llItemLen + 1, ",");
		}                       /* end for */
	    }

	    /* end if */
	    /*  terminate pclTmpBuf */
	    if (ilStrLen > 0)
	    {                         /*  item found  */
		ilStrLen--;
		pclTmpBuf[ilStrLen] = 0x00;
	    }

	    /* end if */
	    if (llAddLen > 0)
	    {
		sprintf (pcpSqlStrg, "(%s,%s) VALUES (%s)", pcpFldLst, pcpAddLst, pclTmpBuf);
	    }

	    /* end if */
	    else
	    {
		sprintf (pcpSqlStrg, "(%s) VALUES (%s)", pcpFldLst, pclTmpBuf);
	    }                         /* end else */

	    /*                                                                                  RESULT:
												>(Field1,Field2,...,Fieldn) VALUES (:VField1,:VField2,...,:VFieldn)<
	    */
	    break;
	default:
	    dbg (TRACE, "TWEBuildSqlFieldStr: No defined SQL type");
	    break;
    }                             /* end switch */
    return ilRC;
}                               /* end of TWEBuildSqlFieldStr */

/* ****************************************************************
 * Function:    GetCedaArrFlds        
 * Parameter:   
 * Return:      RC_SUCCESS, RC_FAIL
 * Description: This function gets TabNam, Selection,
 *              FieldList and User defined FieldList from CEDAARRAY.
 *              ITS ONLY FOR INTERNAL USE!  
 ***************************************************************** */
static int GetCedaArrFlds (char *pcpArrayName, char *pcpTabNam, char *pcpSelect, char *pcpFldLst, char *pcpAddFldLst)
{
    int ilRC = RC_SUCCESS;
    long llRowNo = ARR_FIRST;
    char pclCedaArray[20] = "";
    HANDLE slCedaHandle = -1;
    AATARRAY *prlTmpArray = NULL;

    /* get handle for cedaarray */
    sprintf (pclCedaArray, "%s%s", MARKINTERNARRAY, pcpArrayName);
    ilRC = FindArray (&slCedaHandle, pclCedaArray, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "GetCedaArrFlds: Array %s not found", pclCedaArray);
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNo = ARR_FIRST;
	ilRC = CEDAArrayGetField (&slCedaHandle, pclCedaArray, NULL, "TNam", 7, llRowNo, pcpTabNam);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "GetCedaArrFlds:Fkt. CEDAArrayGetField failed %d (TNam)", ilRC);
	    strcpy (pcpTabNam, "");
	}

	else
	{
	    TWETrimRight (pcpTabNam);
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNo = ARR_FIRST;
	ilRC = CEDAArrayGetField (&slCedaHandle, pclCedaArray, NULL, "Sele", ARR_MAXSELECTLEN + 1, llRowNo, pcpSelect);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "GetCedaArrFlds:Fkt. CEDAArrayGetField failed %d (Sele)", ilRC);
	    strcpy (pcpSelect, "");
	}

	else
	{
	    TWETrimRight (pcpSelect);
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNo = ARR_FIRST;
	ilRC = CEDAArrayGetField (&slCedaHandle, pclCedaArray, NULL, "FldL", ARR_MAXFLDLSTLEN + 1, llRowNo, pcpFldLst);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "GetCedaArrFlds:Fkt. CEDAArrayGetField failed %d (FldL)", ilRC);
	    strcpy (pcpFldLst, "");
	}

	else
	{
	    TWETrimRight (pcpFldLst);
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNo = ARR_FIRST;
	ilRC = CEDAArrayGetField (&slCedaHandle, pclCedaArray, NULL, "AddL", ARR_MAXFLDLSTLEN + 1, llRowNo, pcpAddFldLst);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "GetCedaArrFlds:Fkt. CEDAArrayGetField failed %d (AddL)", ilRC);
	    strcpy (pcpAddFldLst, "");
	}

	else
	{
	    TWETrimRight (pcpAddFldLst);
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	TWEDel2NewDel (pcpSelect, ';', ',');
	TWEDel2NewDel (pcpFldLst, ';', ',');
	TWEDel2NewDel (pcpAddFldLst, ';', ',');
    }
    return ilRC;
}                               /* end of GetCedaArrFlds  */

/* ****************************************************************
 * Function:    CreateStoreArray
 * Parameter:   IN    : pcpArrayName  Name of CedaArray
 *                      pcpOneRecBuf  kommagetrennte Datenliste 
 * Description: This function creates an array for storing the
 *              informations send by user like Table, fields, Selection.
 *              ITS ONLY FOR INTERNAL USE!
 ***************************************************************** */
static int CreateStoreArray (char *pcpArrayName, char *pcpOneRecBuf)
{
    int ilRC = RC_SUCCESS;
    long llRowNo = ARR_FIRST;
    char pclCedaArray[20] = "";
    char pclCedaFldTypes[50];
    HANDLE slCedaHandle = -1;
    sprintf (pclCedaArray, "%s%s", MARKINTERNARRAY, pcpArrayName);
    pclCedaFldTypes[0] = 0x00;
    for (llRowNo = 0; llRowNo < lgCedaFldCnt - 1; llRowNo++)
    {
	strcat (pclCedaFldTypes, "C,");
    }
    strcat (pclCedaFldTypes, "C");
    llRowNo = ARR_FIRST;
    ilRC =
	AATArrayCreate (&slCedaHandle, pclCedaArray, TRUE, 2, lgCedaFldCnt, plgCedaFldLens, pcgCedaFldList,
			pclCedaFldTypes);
    if (ilRC != RC_SUCCESS)
    {
	if (ilRC != RC_DUPLICATE)
	{
	    dbg (TRACE, "CreateStoreArray: Fkt. AATArrayCreate failed  (%d)", ilRC);
	}

	else
	{
	    ilRC = RC_SUCCESS;
	    llRowNo = ARR_NEXT;
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	TWEDel2NewDel (pcpOneRecBuf, ',', '\0');
	ilRC = AATArrayAddRow (&slCedaHandle, pclCedaArray, &llRowNo, pcpOneRecBuf);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CreateStoreArray: Fkt. AATArrayAddRow failed (%d)", ilRC);
	}
    }
    return ilRC;
}                               /* end of CreateStoreArray */
int InsertEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpFldLst, char *pcpDataLst, char *pcpUrno)
{
    int ilRC = RC_SUCCESS;
    long llFieldNumber = 0;
    long llRow = 0;
    long llLen = 0;
    long llFldCnt = 0;
    long llCnt = 0;
    long llFound = 0;
    char *pclFldPtr = NULL;
    char *pclRowData = NULL;
    char pclUrno[12];
    char pclUrnoStr[20];
    char pclFldNam[24];
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    HANDLE       lIndex= -1;

    *pspHandle = -1;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "InsertEventData: ERROR: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	char *pclTmp = NULL;

	/* MCU 20010530: delete '\n' from datalist */
	pclTmp = strchr(pcpDataLst,'\n');
	if (pclTmp != NULL)
	{
	    *pclTmp = '\0';
	}
	ilRC = TWEExtractField ("URNO", pcpFldLst, pcpDataLst, ",", pclUrno);
	if (ilRC == RC_FAIL)
	{
	    dbg (TRACE, "InsertEventData: ERROR: No URNO in Field- and DataList");
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	llFieldNumber = FindField (prlTmpArray->FieldList, "URNO");
	if (llFieldNumber == -1)
	{
	    dbg (TRACE, "InsertEventData: ERROR: No URNO in Array <%s> fieldlist", pcpArrayName);
	    ilRC = RC_INVALID;
	}
    }
    /* 20040629 JIM: BEGIN moved Filter check before double urno check: */
    if (ilRC == RC_SUCCESS)
    {
	if (prlTmpArray->Filter == TRUE)
	{
	    ilRC = ParseFilterStr (pspHandle, pcpArrayName, pcpFldLst, pcpDataLst);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg (TRACE, "InsertEventData: Record doesn't match filter");
		dbg (TRACE, "InsertEventData: Record <%s>", pcpDataLst);
		dbg (TRACE, "InsertEventData: Filter <%s>", prlTmpArray->FilterStr);
	    }
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	tool_filter_spaces (pclUrno);
	strcpy (pcpUrno, pclUrno);
	llFound = -1;
  if ((prlTmpArray->UrnoIdxHandle != NULL) && (prlTmpArray->UrnoIdxHandle->IsActive == TRUE))
  {  /* 20040629 JIM: Use URNO Index, if present, to detect double URNO's: */
      dbg (DEBUG, "InsertEventData: check for double URNO by AATArrayFindKey(%s=%s) ",
                                       prlTmpArray->UrnoIdxHandle->Name,pclUrno);
      llRow = ARR_FIND;   
      /* 20040805 JIM: pclUrnoStr is wrong here, use pclUrno: */
      ilRC = AATArrayFindKey (pspHandle, pcpArrayName, &lIndex, prlTmpArray->UrnoIdxHandle->Name,
                              &llRow, 20, pclUrno);
      if (ilRC == RC_SUCCESS)
      {
        llFound= llRow;
      }
  } /* 20040629 JIM: END Use URNO Index, if present, to detect double URNO's */
  else
  {
   dbg (DEBUG, "InsertEventData: check for double URNO by Loop");
   for (llRow = 0; (llRow < prlTmpArray->MaxRows) && (llFound == -1); llRow++)
    {
	    prlTmpRow = &prlTmpArray->Rows[llRow];
	    if (prlTmpRow->DataOrig != NULL)
	    {
       pclRowData =
		    (char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
						  NULL ? prlTmpArray->FieldOffsets[llFieldNumber] :
						  prlTmpRow->FieldOffsetsOrig[llFieldNumber]];
      strcpy (pclUrnoStr, pclRowData);
      tool_filter_spaces (pclUrnoStr);
      if (strcmp (pclUrno, pclUrnoStr) == 0)
      {
		      llFound = llRow;
        }
      }
    }
  }
	if (llFound == -1)
	{
	    ilRC = RC_SUCCESS;
	}
	else
	{
	    dbg (TRACE, "InsertEventData: Record with Urno <%s> exists already", pclUrno);
	    ilRC = RC_FAIL;
	}
    }
    /* 20040629 JIM: END moved Filter check before double urno check: */
    if (ilRC == RC_SUCCESS)
    {
	llRow = llFound;
	strcpy (pcgOneRowData, "");
	llFldCnt = prlTmpArray->FieldCount;
	pclFldPtr = prlTmpArray->FieldList;
	for (llCnt = 0; llCnt < llFldCnt; llCnt++)
	{
	    GetNextDataItem (pclFldNam, &pclFldPtr, ",", "\0", "\0\0");
	    if (*pclFldNam != '\0')
	    {
		ilRC = TWEExtractField (pclFldNam, pcpFldLst, pcpDataLst, ",", pcgOneFldData);
		if (ilRC == RC_FAIL)
		{
		    dbg (DEBUG, "InsertEventData: Field <%s> not in FieldList <%s> => take default", pclFldNam, pcpArrayName);
		    sprintf (pcgOneFldData, "%*s", prlTmpArray->FieldLen[llCnt], " ");
		}

		else if (strcmp (pcgOneFldData, "") == 0)
		{
		    dbg (DEBUG, "InsertEventData: Field <%s> no data => take default", pclFldNam);
		    sprintf (pcgOneFldData, "%*s", prlTmpArray->FieldLen[llCnt], " ");
		}
		strcat (pcgOneFldData, ",");
		strcat (pcgOneRowData, pcgOneFldData);
	    }
	}
	llLen = strlen (pcgOneRowData);
	if (llLen > 0)
	{
	    pcgOneRowData[llLen - 1] = 0x00; /*  rem last komma  */
	}
	TWEDel2NewDel (pcgOneRowData, ',', '\0');
	llRow = ARR_FIRST;
	ilRC = AATArrayAddRow (pspHandle, pcpArrayName, &llRow, pcgOneRowData);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "InsertEventData: ERROR: AATArrayAddRow <%s> failed", pcpArrayName);
	}
    }
    return ilRC;
}                               /* end of InsertEventData */

int UpdateEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, char *pcpFldLst, char *pcpDataLst,
                            char *pcpUrnoLst)
{
    int ilRC = RC_SUCCESS;
    int ilErrFlag = FALSE;
    long llFieldNumber = 0;
    long llFieldNumberSve = 0;
    long llRow = 0;
    long llFound = 0;
    long llRecCnt = 0;
    char *pclFieldStart = NULL;
    char *pclDataStart = NULL;
    char *pclRowData = NULL;
    char pclUrno[12];
    char pclUrnoStr[20];
    char pclFieldNam[24];
    char pclFieldNamSve[24];
    void *pclNxt = NULL;
    void *pclThis = NULL;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    HANDLE       lIndex= -1;
    /* 20040811 JIM:    1/5 debug orig and new before change */
    int llNumOfFlds = 0;

    pcpUrnoLst[0] = 0x00;
    *pspHandle = -1;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "UpdateEventData: ERROR: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	char *pclTmp = NULL;

    /* 20040811 JIM:    2/5 debug orig and new before change */
    llNumOfFlds = TWECountDelimiter (prlTmpArray->FieldList, ",");

	/* MCU 20010530: delete '\n' from datalist */
	pclTmp = strchr(pcpDataLst,'\n');
	if (pclTmp != NULL)
	{
	    *pclTmp = '\0';
	}
	llFieldNumber = FindField (prlTmpArray->FieldList, "URNO");
	if (llFieldNumber == -1)
	{
	    dbg (TRACE, "UpdateEventData: ERROR: No URNO in Array <%s> fieldlist", pcpArrayName);
	    ilRC = RC_INVALID;
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	pclNxt = NULL;

	/*  hag 20010327 */
	/* strcpy (pclUrno, GetNextUrnoFromSelection (pcpSelect, &pclNxt)); */
	pclThis = GetNextUrnoFromSelection (pcpSelect, &pclNxt);
	if (pclThis)
	    strcpy (pclUrno, pclThis);

	else
	    pclUrno[0] = '\0';

	/* +++ jim +++ 20010112: new: if URNO in FieldList and URNO of selection is NULL */
	/*                            then get URNO value from DataList                  */
	/*                            (this happens with WHERE URNO=:VURNO)              */
	if (strlen (pclUrno) == 0)
	{
	    dbg (TRACE, "UpdateEventData: Warning: Select <%s> contains no URNO select", pcpSelect);
	    dbg (TRACE, "UpdateEventData: Evaluating URNO from data list");
	    ilRC = TWEExtractField ("URNO", pcpFldLst, pcpDataLst, ",", pclUrno);
	}
	if (strlen (pclUrno) == 0)
	{
	    dbg (TRACE, "UpdateEventData: Evaluating URNO from data list failed!");
	    dbg (TRACE, "UpdateEventData: field list: <%s> ", pcpFldLst);
	    dbg (TRACE, "UpdateEventData: data list:  <%s> ", pcpDataLst);
	    ilRC = RC_FAIL;
	}
    }
    dbg (DEBUG, "UpdateEventData Urno=<%s>", pclUrno);
    if (ilRC == RC_SUCCESS)
    {

	/* +++ jim +++ 20010129: pclUrno is filled with strcpy, don't check for NULL */
	while (strlen (pclUrno) > 0)
	{
	    strcat (pcpUrnoLst, pclUrno);
	    llFound = -1;
      if ((prlTmpArray->UrnoIdxHandle != NULL) && (prlTmpArray->UrnoIdxHandle->IsActive == TRUE))
      {  /* 20040629 JIM: Use URNO Index, if present, to find row: */
          dbg (DEBUG, "UpdateEventData: check for double URNO by AATArrayFindKey(%s=%s) ",
                       prlTmpArray->UrnoIdxHandle->Name,pclUrno);
          llRow = ARR_FIND;   
          ilRC = AATArrayFindKey (pspHandle, pcpArrayName, &lIndex, prlTmpArray->UrnoIdxHandle->Name, 
                                  &llRow, 20, pclUrno);
          if (ilRC == RC_SUCCESS)
          {
            dbg(TRACE,"HIT LINE %d OF %d",llRow,prlTmpArray->MaxRows);
            llFound= llRow;
            /* 20040811 JIM: BEGIN 3/5 debug orig and new before change: */
            prlTmpRow = &prlTmpArray->Rows[llRow];
            if (prlTmpRow->DataNew != NULL)
            {
              BuildItemBuffer (prlTmpRow->DataNew, "", llNumOfFlds, ",");
              dbg(TRACE,"New  before update: <%s> ",prlTmpRow->DataNew);
		  	      delton(prlTmpRow->DataNew);
            }
            if (prlTmpRow->DataOrig != NULL)
            {
  			      BuildItemBuffer (prlTmpRow->DataOrig, "", llNumOfFlds, ",");
              dbg(DEBUG,"Orig before update: <%s> ",prlTmpRow->DataOrig);
		  	      delton(prlTmpRow->DataOrig);
            }
            /* 20040811 JIM: END   3/5 debug orig and new before change */
          }
      } /* 20040629 JIM: END Use URNO Index, if present, to find row  */
      else
      {
        dbg (DEBUG, "UpdateEventData: check for double URNO by Loop");
        for (llRow = 0; (llRow < prlTmpArray->MaxRows) && (llFound == -1); llRow++)
        {
          llRecCnt++;
          prlTmpRow = &prlTmpArray->Rows[llRow];
          if (prlTmpRow->DataOrig != NULL)
          {
             pclRowData =
                 (char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
						      NULL ? prlTmpArray->FieldOffsets[llFieldNumber] :
						      prlTmpRow->FieldOffsetsOrig[llFieldNumber]];
            strcpy (pclUrnoStr, pclRowData);
            tool_filter_spaces (pclUrnoStr);
            tool_filter_spaces (pclUrno);
            if (strcmp (pclUrno, pclUrnoStr) == 0)
            {
              dbg(TRACE,"HIT LINE %d OF %d",llRecCnt,prlTmpArray->RowCount);
              llFound = llRow;
              /* 20040811 JIM: BEGIN 4/5 debug orig and new before change: */
            if (prlTmpRow->DataNew != NULL)
            {
              BuildItemBuffer (prlTmpRow->DataNew, "", llNumOfFlds, ",");
              dbg(TRACE,"New  before update: <%s> ",prlTmpRow->DataNew);
		  	      delton(prlTmpRow->DataNew);
            }
            if (prlTmpRow->DataOrig != NULL)
            {
  			      BuildItemBuffer (prlTmpRow->DataOrig, "", llNumOfFlds, ",");
              dbg(DEBUG,"Orig before update: <%s> ",prlTmpRow->DataOrig);
		  	      delton(prlTmpRow->DataOrig);
            }
              /* 20040811 JIM: END   4/5 debug orig and new before change */
              if (prlTmpRow->DataNew != NULL)
              {
                dbg (TRACE, "UpdateEventData: Update on Row with NEWDATA!(%ld)", llRow);
                ilRC = RC_WRONGDATA;
              }
		        }
		      }
        }
      }
	    if (llFound == -1)
	    {
		ilRC = RC_NOTFOUND;
		dbg (TRACE, "UpdateEventData: Urno <%s> not found", pclUrno);
	    }

/* +++ if (ilRC == RC_SUCCESS) jim +++ 20010129: in error case also get next urno */
	    {

		/*  hag 20010327 */
		/* strcpy (pclUrno, GetNextUrnoFromSelection (pcpSelect, &pclNxt)); */
		pclThis = GetNextUrnoFromSelection (pcpSelect, &pclNxt);
		if (pclThis)
		    strcpy (pclUrno, pclThis);

		else
		    pclUrno[0] = '\0';

		/* +++ jim +++ 20010129: pclUrno is filled with strcpy, don't check for NULL */
		if (strlen (pclUrno) > 0)
		{
		    strcat (pcpUrnoLst, ",");
		}

		/* not ) reached */
	    }                         /* case URNO IN */
	}                           /* for all run */
    }
    if (ilRC == RC_SUCCESS)
    {
	llRow = llFound;
	pclFieldStart = pcpFldLst;
	pclDataStart = pcpDataLst;
  prlTmpRow = &prlTmpArray->Rows[llRow];

	do
	{
	    GetNextDataItem (pclFieldNam, &pclFieldStart, ",", "\0", "\0\0");
	    GetNextDataItem (pcgOneFldData, &pclDataStart, ",", "\0", "\0\0");
	    llFieldNumber = FindField (prlTmpArray->FieldList, pclFieldNam);
	    /*dbg(DEBUG,"%05d UpdateEventData: pclFieldNam: <%s>",__LINE__,pclFieldNam);*/
	    if (llFieldNumber >= 0)
	    {
                llFieldNumberSve = llFieldNumber;
		ilRC = AATArrayPutField (pspHandle, pcpArrayName, &llFieldNumber, pclFieldNam, llRow, FALSE, pcgOneFldData);
	        if (ilRC != RC_SUCCESS)
	        {
                    if (ilErrFlag != TRUE)
                    {
	              dbg(TRACE, "UpdateEventData: ===================== ERROR MESSAGE ==============");
	              dbg(TRACE, "UpdateEventData: ERROR: CEDAArrayPutFields %ld in array <%s> failed", llRow, pcpArrayName);
                      dbg(TRACE,"ARRAY FIELDS <%s> COUNT=%d",prlTmpArray->FieldList,prlTmpArray->FieldCount);
                      ilErrFlag = TRUE;
                    }
                    dbg(TRACE,"RC=%d FIELD <%s> FLDNO=%d (%d)",ilRC,pclFieldNam,llFieldNumberSve,llFieldNumber);
                    ilRC = RC_SUCCESS;
	        }
          else if (prlTmpRow->DataNew != NULL)
          {  /* 20040810 JIM: already new data, repeat update on DataNew */
             llFieldNumber= llFieldNumberSve;
             dbg(TRACE, "UpdateEventData: updating NEW data!");
             ilRC = AATArrayPutField (pspHandle, pcpArrayName, &llFieldNumber, pclFieldNam, llRow, TRUE, pcgOneFldData);
             if (ilRC != RC_SUCCESS)
             {
                if (ilErrFlag != TRUE)
                {
                 dbg(TRACE, "UpdateEventData: ===================== ERROR MESSAGE ==============");
                 dbg(TRACE, "UpdateEventData: ERROR: CEDAArrayPutFields %ld in array <%s> failed", llRow, pcpArrayName);
                 dbg(TRACE,"ARRAY FIELDS <%s> COUNT=%d",prlTmpArray->FieldList,prlTmpArray->FieldCount);
                 ilErrFlag = TRUE;
                }
                dbg(TRACE,"RC=%d FIELD <%s> FLDNO=%d (%d)",ilRC,pclFieldNam,llFieldNumberSve,llFieldNumber);
                ilRC = RC_SUCCESS;
            }
          }
	    }
	}
	while ((*pclFieldNam != '\0') && (*pclFieldStart != '\0') && ilRC == RC_SUCCESS);
	/*while ((*pclFieldNam != '\0') && (*pclDataStart != '\0') && ilRC == RC_SUCCESS);*/
        if (ilErrFlag == TRUE)
        {
	  dbg(TRACE, "UpdateEventData: ==================================================");
        }
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "UpdateEventData: ERROR: CEDAArrayPutFields %ld in array <%s> failed", llRow, pcpArrayName);
	}
            /* 20040811 JIM: BEGIN 5/5 debug orig and new before change: */
            if (prlTmpRow->DataNew != NULL)
            {
              BuildItemBuffer (prlTmpRow->DataNew, "", llNumOfFlds, ",");
              dbg(DEBUG,"New  after update: <%s> ",prlTmpRow->DataNew);
		  	      delton(prlTmpRow->DataNew);
            }
            if (prlTmpRow->DataOrig != NULL)
            {
  			      BuildItemBuffer (prlTmpRow->DataOrig, "", llNumOfFlds, ",");
              dbg(DEBUG,"Orig after update: <%s> ",prlTmpRow->DataOrig);
		  	      delton(prlTmpRow->DataOrig);
            }
            /* 20040811 JIM: END   5/5 debug orig and new before change */
    }
    return ilRC;
}                               /* end of UpdateEventData */
int DeleteEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, char *pcpUrnoLst)
{
    int ilRC = RC_SUCCESS;
    long llFieldNumber = 0;
    long llRow = 0;
    long llFound = 0;
    char *pclRowData = NULL;
    void *pclNxt = NULL;
    char pclUrno[12];
    char pclUrnoStr[20];
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    HANDLE       lIndex= -1;
    char *pclThis = NULL;

    *pspHandle = -1;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "DeleteEventData: ERROR: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	llFieldNumber = FindField (prlTmpArray->FieldList, "URNO");
	if (llFieldNumber == -1)
	{
	    dbg (TRACE, "DeleteEventData: ERROR: No URNO in Array <%s> fieldlist", pcpArrayName);
	    ilRC = RC_INVALID;
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	pclNxt = NULL;

	/*  hag 20010327 */
	/* strcpy (pclUrno, GetNextUrnoFromSelection (pcpSelect, &pclNxt)); */
	pclThis = GetNextUrnoFromSelection (pcpSelect, &pclNxt);
	if (pclThis)
	    strcpy (pclUrno, pclThis);

	else
	    pclUrno[0] = '\0';

	/* +++ jim +++ 20010112: new: if URNO in FieldList and URNO of selection is NULL */
	/*                            then get URNO value from DataList                  */
	/*                            (this happens with WHERE URNO=:VURNO)              */
	/* but sorry, there is no fieldlst - - code remains to compare fkts * *    
	   else if (strlen(pclUrno) == 0)
	   {
	   ilRC = TWEExtractField ("URNO", pcpFldLst, pcpDataLst, ",", pclUrno);
	   }
	*/
    }
    if (ilRC == RC_SUCCESS)
    {

	/* +++ jim +++ 20010129: pclUrno is filled with strcpy, don't check for NULL */
	while (strlen (pclUrno) > 0)
	{
	    strcat (pcpUrnoLst, pclUrno);
	    llFound = -1;
      if ((prlTmpArray->UrnoIdxHandle != NULL) && (prlTmpArray->UrnoIdxHandle->IsActive == TRUE))
      {  /* 20040629 JIM: Use URNO Index, if present, to find row: */
          dbg (DEBUG, "DeleteEventData: check for double URNO by AATArrayFindKey(%s=%s) ",
                       prlTmpArray->UrnoIdxHandle->Name,pclUrno);
          llRow = ARR_FIND;   
          ilRC = AATArrayFindKey (pspHandle, pcpArrayName, &lIndex, prlTmpArray->UrnoIdxHandle->Name,
                                  &llRow, 20, pclUrno);
          if (ilRC == RC_SUCCESS)
          {
            llFound= llRow;
          }
      } /* 20040629 JIM: END Use URNO Index, if present, to find row  */
      else
      {
        dbg (DEBUG, "DeleteEventData: check for double URNO by Loop");
        for (llRow = 0; (llRow < prlTmpArray->MaxRows) && (llFound == -1); llRow++)
        {
          prlTmpRow = &prlTmpArray->Rows[llRow];
          if (prlTmpRow->DataOrig != NULL)
          {
              pclRowData =
                 (char *) &prlTmpRow->DataOrig[prlTmpArray->FieldOffsets !=
          		             NULL ? prlTmpArray->FieldOffsets[llFieldNumber] :
          		             prlTmpRow->FieldOffsetsOrig[llFieldNumber]];
              strcpy (pclUrnoStr, pclRowData);
              tool_filter_spaces (pclUrnoStr);
              tool_filter_spaces (pclUrno);
              if (strcmp (pclUrno, pclUrnoStr) == 0)
              {
                llFound = llRow;
              }
          }
          else if (prlTmpRow->DataNew != NULL)
          {
              pclRowData = (char *) &prlTmpRow->DataNew[prlTmpArray->FieldOffsets[llFieldNumber]];
              sprintf (pclUrnoStr, "%*s", prlTmpArray->FieldLen[llFieldNumber], pclUrno);
              if (strcmp (pclRowData, pclUrnoStr) == 0)
              {
                llFound = llRow;
              }
          }
      }
    }                         /* end of for all rows in array */
	  if (llFound == -1)
	  {
		  dbg (TRACE, "DeleteEventData: Record with Urno <%s> not found", pclUrno);
	    }

	    else
	    {

		/* delete row if URNO found */
		llRow = llFound;
		ilRC = AATArrayDeleteRow (pspHandle, pcpArrayName, llRow);
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "DeleteEventData: ERROR: AATArrayDeleteRow %ld in array <%s> failed", llRow, pcpArrayName);
		}
	    }

	    /* get next in list of URNO IN (...)   */
	    /*if (ilRC == RC_SUCCESS)
	    {		hag20040719: PRF6338, GetNextUrnoFromSelection must be called in any case, 
							 otherwise endless loop, see also UpdateEventData */

		/*  hag 20010327 */
		/* strcpy (pclUrno, GetNextUrnoFromSelection (pcpSelect, &pclNxt)); */
		pclThis = GetNextUrnoFromSelection (pcpSelect, &pclNxt);
		if (pclThis)
		    strcpy (pclUrno, pclThis);

		else
		    pclUrno[0] = '\0';

		/* +++ jim +++ 20010129: pclUrno is filled with strcpy, don't check for NULL */
		if (strlen (pclUrno) > 0)
		{
		    strcat (pcpUrnoLst, ",");
		}

	    /* } 	hag20040719: PRF6338, 2nd part (corresponding bracket) */

	    /* case URNO IN */
	}                           /* end for llRun */
    }

    /* if success */
    return ilRC;
}                               /* end of DeleteEventData */

/*****************************************************************************/
/* Function:    TWEExtractField()                                            */
/*****************************************************************************/
/* Parameter:   IN : pcpFieldName   name of field                            */
/*                   pcpFields      field list                               */
/*                   pcpDatablk     insert data list                         */
/*                   pcpDel         delimiter in lists                       */
/*              OUT  pcpFieldData   content of data list                     */
/* Return:      RC_SUCCESS, RC_FAIL                                          */
/*                                                                           */
/* Description: Searches in field list for pFieldName                        */
/*              and takes its position to get value from                     */
/*              Data list. RC_FAIL => not found                              */
/*                                                                           */
/* History:     10.05.99 TWE Written                                         */
/*                                                                           */
/*****************************************************************************/
static int TWEExtractField (char *pcpFieldName, char *pcpFields, char *pcpDatablk, char *pcpDel, char *pcpFieldData)
{

/*  char *pclFct = "ExtractField"; */
    int ilRC = RC_SUCCESS;
    int ilItem = 0;
    int ilItemCnt = 0;
    int ilend = 0;
    char pclItemNam[50] = "";
    char *pclItem = NULL;

    /*  get FieldName from fieldstr  */
    ilItemCnt = TWECountDelimiter (pcpFields, pcpDel);
    pclItem = pcpFields;
    for (ilItem = 1; (ilItem <= ilItemCnt) && (ilend == 0); ilItem++)
    {
	(void) GetNextDataItem (pclItemNam, &pclItem, pcpDel, "\0", "\0\0");
	if (strcmp (pclItemNam, pcpFieldName) == 0)
	{
	    ilend = ilItem;
	}

	/* end if */
    }                             /* end for */
    if (ilend != 0)
    {
	(void) GetDataItem (pcpFieldData, pcpDatablk, ilend, ',', "\0", "\0\0");
    }

    /* end if */
    else
    {
	ilRC = RC_FAIL;
    }                             /* end else */
    return (ilRC);
}                               /* end of TWEExtractField */
static int AllocInternalBuffers (long lpFldCount, long *plpFieldLens)
{
    int ilRC = RC_SUCCESS;
    long llOneRowDataLen;
    long llOneFldDataLen;
    long llCnt;

    /* alloc or realloc pcgOneRowData for storing row information */
    llOneRowDataLen = 0;
    llOneFldDataLen = 0;
    for (llCnt = 0; llCnt < lpFldCount; llCnt++)
    {
	llOneRowDataLen += plpFieldLens[llCnt] + 1; /* plus delimiter */
	if (plpFieldLens[llCnt] > llOneFldDataLen)
	{
	    llOneFldDataLen = plpFieldLens[llCnt];
	}
    }
    if (llOneRowDataLen < (ARR_MAXFLDLSTLEN * 2 + 20 + 10 + 3 + 6 + ARR_MAXSELECTLEN + 100))
    {
	llOneRowDataLen = ARR_MAXFLDLSTLEN * 2 + 20 + 10 + 3 + 6 + ARR_MAXSELECTLEN + 100;
    }
    if (llOneRowDataLen >= lgOneRowDataLen)
    {
	lgOneRowDataLen = llOneRowDataLen + 10;
	if (pcgOneRowData == NULL)
	{
	    pcgOneRowData = (char *) (malloc (lgOneRowDataLen));
	}

	else
	{
	    pcgOneRowData = (char *) (realloc (pcgOneRowData, lgOneRowDataLen));
	}
	if (pcgOneRowData == NULL)
	{
	    dbg (TRACE, "ArrayCreateIntern: Alloc of pcgOneRowData (%ld) failed", lgOneRowDataLen);
	    lgOneRowDataLen = 0;
	    ilRC = RC_NOMEM;
	}

	else
	{
	    if (pcgMultiBuffer != NULL)
	    {
		pcgMultiBuffer = (char *) (realloc (pcgMultiBuffer, lgMultiLines * lgOneRowDataLen));
		if (pcgMultiBuffer == NULL)
		{
		    dbg (TRACE, "ArrayCreateIntern: Realloc of pcgMultiBuffer (%ld) failed", lgMultiLines * lgOneRowDataLen);
		    ilRC = RC_NOMEM;
		}
	    }
	}
    }

    /* alloc or realloc pcgOneFldData for storing one fields information */
    if (llOneFldDataLen >= lgOneFldDataLen)
    {
	lgOneFldDataLen = llOneFldDataLen + 10;
	if (pcgOneFldData == NULL)
	{
	    pcgOneFldData = (char *) (malloc (lgOneFldDataLen));
	}

	else
	{
	    pcgOneFldData = (char *) (realloc (pcgOneFldData, lgOneFldDataLen));
	}
	if (pcgOneFldData == NULL)
	{
	    dbg (TRACE, "ArrayCreateIntern: Alloc of pcgOneFldData (%ld) failed", lgOneFldDataLen);
	    lgOneFldDataLen = 0;
	    ilRC = RC_NOMEM;
	}
    }
    return ilRC;
}                               /* end if AllocInternalBuffers */

/* ****************************************************************
 * Function:    CEDAArrayInitialize
 * Parameter:   IN    : lpArrayCount    Number of arrays to store
 *                      lpIndexCount    Number of index for one array
 * Description: This function allocates memory for lpArrayCount Rows.
 *              Function just calls AATArrayInitialize.
 ***************************************************************** */
int CEDAArrayInitialize (long lpArrayCount, long lpIndexCount)
{
    int ilRC = RC_SUCCESS;

    /* for internal CEDAArrays */
    lpArrayCount *= 2;
    ilRC = AATArrayInitialize (lpArrayCount, lpIndexCount);
    return ilRC;
}                               /* end of CEDAArrayInitialize */

/* ****************************************************************
 * Function:    CEDAArrayCreate
 * Parameter:   OUT   : pspHandle       Handle for array   
 *              IN    : pcpArrayName    name for the array
 *                      pcpTabNam       name of table
 *                      pcpSelect       selection string
 *                      pcpAddFldLst    user defined field list
 *                      plpAddFldLens   length of user defined fields
 *              IN/OUT: pcpFldLst       field list sep by komma
 *                      plpFldLens      length of fields in list
 *                      plpFldOffsets   offsets to each field
 * Description: This function creates an array for a list of fields.
 *              The funtion returns an arraystructure and a handle
 *              for handling the array.
 *              If fieldlist is NULL, the function looks into SYSTAB
 *              and gets all fields and length for defined table.
 *              The length of field is always get from SYSTAB.
 *              For User defined fields use AddFieldslist for fields
 *              and AddFlieldsLen for field length.
 ***************************************************************** */
int CEDAArrayCreate (HANDLE * pspHandle, char *pcpArrayName, char *pcpTabNam, char *pcpSelect, char *pcpAddFldLst,
                     long *plpAddFldLens, char *pcpFldLst, long *plpFldLens, long *plpFldOffsets)
{
    int ilRC = RC_SUCCESS;
    ilRC =
	ArrayCreateIntern (pspHandle, pcpArrayName, pcpTabNam, pcpSelect, pcpAddFldLst, plpAddFldLens, pcpFldLst,
			   plpFldLens, plpFldOffsets, 1000);
    return ilRC;
}                               /* end of CEDAArrayCreate */

/* ****************************************************************
 * Function:    CEDAArrayCreateInitCount
 * Parameter:   OUT   : pspHandle       Handle for array
 *              IN    : pcpArrayName    name for the array
 *                      pcpTabNam       name of table
 *                      pcpSelect       selection string
 *                      pcpAddFldLst    user defined field list
 *                      plpAddFldLens   length of user defined fields
 *                      lpInitialNumRecords   first alloc of records
 *                                            (if more realloc is done)
 *              IN/OUT: pcpFldLst       field list sep by komma
 *                      plpFldLens      length of fields in list
 *                      plpFldOffsets   offsets to each field
 * Description: This function creates an array for a list of fields.
 *              The funtion returns an arraystructure and a handle
 *              for handling the array.
 *              If fieldlist is NULL, the function looks into SYSTAB
 *              and gets all fields and length for defined table.
 *              The length of field is always get from SYSTAB.
 *              For User defined fields use AddFieldslist for fields
 *              and AddFlieldsLen for field length.
 ***************************************************************** */
int CEDAArrayCreateInitCount (HANDLE * pspHandle, char *pcpArrayName, char *pcpTabNam, char *pcpSelect,
                              char *pcpAddFldLst, long *plpAddFldLens, char *pcpFldLst, long *plpFldLens,
                              long *plpFldOffsets, long lpInitialNumRecords)
{
    int ilRC = RC_SUCCESS;
    ilRC =
	ArrayCreateIntern (pspHandle, pcpArrayName, pcpTabNam, pcpSelect, pcpAddFldLst, plpAddFldLens, pcpFldLst,
			   plpFldLens, plpFldOffsets, lpInitialNumRecords);
    return ilRC;
}                               /* CEDAArrayCreateInitCount */

/* ****************************************************************
 * Function:    ArrayCreateIntern
 * Parameter:   OUT   : pspHandle       Handle for array   
 *              IN    : pcpArrayName    name for the array
 *                      pcpTabNam       name of table
 *                      pcpSelect       selection string
 *                      pcpAddFldLst    user defined field list
 *                      plpAddFldLens   length of user defined fields
 *                      lpInitialNumRecords   first alloc of records
 *                                            (if more realloc is done)
 *              IN/OUT: pcpFldLst       field list sep by komma
 *                      plpFldLens      length of fields in list
 *                      plpFldOffsets   offsets to each field
 * Description: This function creates an array for a list of fields.
 *              The funtion returns an arraystructure and a handle
 *              for handling the array.
 *              If fieldlist is NULL, the function looks into SYSTAB
 *              and gets all fields and length for defined table.
 *              The length of field is always get from SYSTAB.
 *              For User defined fields use AddFieldslist for fields
 *              and AddFlieldsLen for field length.
 ***************************************************************** */
static int ArrayCreateIntern (HANDLE * pspHandle, char *pcpArrayName, char *pcpTabNam, char *pcpSelect,
                              char *pcpAddFldLst, long *plpAddFldLens, char *pcpFldLst, long *plpFldLens,
                              long *plpFldOffsets, long lpInitialNumRecords)
{
    int ilRC = RC_SUCCESS;
    int ilNumber = 0;             /* int because of lib fkt. */
    long llLength = 0;
    long llStrCnt = 0;
    long llCount = 0;
    long llCnt = 0;
    long llFldCount = 0;
    long llSqlFldCount = 0;
    long pllFieldLens[300];       /* max 300 fields per table */
    long llItemLen;
    char *pclStrPtr = NULL;
    char pclDummy[30] = "";
    char pclTabExt[iMIN] = "";    /* for syslib call */
    char pclTabShort[7] = "";
    char pclFina[24] = "";
    char pclFity[10] = "";
    char pclFele[10] = "";
    char pclInFldLst[30] = "";
    char pclInFldVal[30] = "";
    char pclOutFldLst[30] = "";
    char pclFldLen[10] = "";
    char pclSelect[ARR_MAXSELECTLEN + 4] = ""; /* Selection for one Array row */
    char pclHelpSelect[ARR_MAXSELECTLEN + 4] = ""; /* Selection for CEDAArray */
    char pclFieldList[ARR_MAXFLDLSTLEN + 4] = ""; /* FieldList for array */
    char pclHelpFldLst[ARR_MAXFLDLSTLEN + 4] = ""; /* Fields from Table (DB) */
    char pclHelpAddFldLst[ARR_MAXFLDLSTLEN + 4] = ""; /* Fields to add (USER) */
    char pclOneRecBuf[4096] = ""; /* (4+4+2) * 250 => 4k  */
    char pclFieldTypeLst[ARR_MAXFLDLSTLEN + 4] = ""; /* field types               */

    /*  (max 3 signs per field)  */
    AATARRAY *prlTmpArray;
    pclFieldTypeLst[0] = 0x00;
    if (pcpSelect == NULL)
    {
	strcpy (pclSelect, "");
    }

    else if (strcmp (pcpSelect, "") != 0)
    {
	if ((long) (strlen (pcpSelect)) > ARR_MAXSELECTLEN)
	{
	    dbg (TRACE, "ArrayCreateIntern: ERROR:Selection <%s> is too long!!!", pcpSelect);
	    dbg (TRACE, "ArrayCreateIntern: Change #define ARR_MAXSELECTLEN to new size in AATArray.h");
	    ilRC = RC_INVALID;
	    return ilRC;
	}

	else
	{
	    strcpy (pclSelect, pcpSelect);
	}
    }

    else
    {
	strcpy (pclSelect, "");
    }
    strcpy (pclTabExt, "TAB");

    /* get table extension for syslib calls */
    if ((ilRC = tool_search_exco_data ("ALL", "TABEND", pclTabExt)) != RC_SUCCESS)
    {
	dbg (TRACE, "ArrayCreateIntern: cannot find entry <TABEND> in sgs.tab (%d)", ilRC);
	dbg (TRACE, "ArrayCreateIntern: Set TABEND to <TAB>");
	strcpy (pclTabExt, "TAB");
	ilRC = RC_SUCCESS;
    }
    memset (pllFieldLens, 0, sizeof (pllFieldLens));
    strcpy (pclTabShort, pcpTabNam);
    pclTabShort[3] = 0x00;
    if ((pcpFldLst == NULL) || (!strcmp (pcpFldLst, "")))
    {

	/*  no Fieldlist exists  */
	/* get default from systab */
	strcpy (pclInFldLst, "TANA");
	sprintf (pclInFldVal, "%s", pclTabShort); /* tablename 3 signs */

	/* get field name , type and len from systab */
	strcpy (pclOutFldLst, "FINA,FELE,FITY");
	ilNumber = 0;               /*  all that are found  */
	ilRC = syslibSearchSystabData (pclTabExt, pclInFldLst, pclInFldVal, pclOutFldLst, pclOneRecBuf, &ilNumber, "");
	if (ilRC == RC_SUCCESS)
	{
	    llCount = 0;
	    pclStrPtr = pclOneRecBuf; /* result list is delimited by NULL  */
	    while (llCount < (long) ilNumber)
	    {

		/* fieldname */
		llItemLen = GetDataItem (pclFina, pclStrPtr, 1, ',', "\0", "\0\0");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llItemLen);
		}
		sprintf (pclDummy, "%s,", pclFina);
		strcat (pclFieldList, pclDummy);

		/* fieldlen */
		llItemLen = GetDataItem (pclFele, pclStrPtr, 2, ',', "\0", "\0\0");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llItemLen);
		}
		pllFieldLens[llCount] = strtol (pclFele, NULL, 10);
		if (plpFldLens != NULL)
		{
		    plpFldLens[llCount] = pllFieldLens[llCount];
		}
		llCount++;

		/* fieldtype */
		llItemLen = GetDataItem (pclFity, pclStrPtr, 3, ',', "\0", "\0\0");
		if (llItemLen < 0)
		{
		    dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llItemLen);
		}
		sprintf (pclDummy, "%s,", pclFity);
		strcat (pclFieldTypeLst, pclDummy);
		llLength = strlen (pclStrPtr);
		pclStrPtr += llLength + 1; /*  jump to next delimiter NULL  */
	    }                         /* end while */
	    pclFieldList[strlen (pclFieldList) - 1] = 0x00; /*  rem last komma  */
	    pclFieldTypeLst[strlen (pclFieldTypeLst) - 1] = 0x00; /*  rem last komma  */
	    llFldCount = llCount;     /* number of fields found for table */
	}

	/* end if */
	else
	{
	    dbg (TRACE, "ArrayCreateIntern: Fkt. syslibSearchSystabData failed (%d)", ilRC);
	    strcpy (pclFieldList, "");
	    strcpy (pclFieldTypeLst, "");
	    ilRC = RC_FAIL;
	}
	if (pcpFldLst != NULL)
	{
	    strcpy (pcpFldLst, pclFieldList);
	}
    }

    /* end if */
    else
    {                             /*  Fieldlist exists just get length  */
	strcpy (pclFieldList, pcpFldLst);
	llFldCount = TWECountDelimiter (pclFieldList, ",");
        llSqlFldCount = llFldCount;
	for (llCount = 0; (llCount < llFldCount) && (ilRC == RC_SUCCESS); llCount++)
	{

	    /* get fieldname from list and make toupper  */
	    llLength = GetDataItem (pclFina, pclFieldList, llCount + 1, ',', "\0", "\0\0");
	    if (llLength < 0)
	    {
		dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llLength);
		strcpy (pclFieldList, "");
		ilRC = RC_FAIL;
	    }

	    else if (llLength == 0)
	    {
		dbg (TRACE, "ArrayCreateIntern: No fieldname at position %ld of fieldlist", llCount + 1);
		strcpy (pclFieldList, "");
		ilRC = RC_FAIL;
	    }
	    if (ilRC == RC_SUCCESS)
	    {
		for (llStrCnt = 0; llStrCnt < llLength; llStrCnt++)
		{
		    pclFina[llStrCnt] = toupper (pclFina[llStrCnt]);
		}
		if (llCount == 0)
		{                       /* get distinct selection */
		    if ((pclStrPtr = strstr (pclFina, "DISTINCT")) != NULL)
		    {
			pclStrPtr += 8;
			strcpy (pclFina, pclStrPtr);
			tool_filter_spaces (pclFina);
		    }
		}

		/* get length of field from systab */
		strcpy (pclInFldLst, "TANA,FINA");
		sprintf (pclInFldVal, "%s,%s", pclTabShort, pclFina); /*table 3 signs */
		strcpy (pclOutFldLst, "FELE,FITY"); /* get len and type from systab */
		ilNumber = 1;           /* first that is found  */
		ilRC = syslibSearchSystabData (pclTabExt, pclInFldLst, pclInFldVal, pclOutFldLst, pclOneRecBuf, &ilNumber, "");
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "ArrayCreateIntern:Fkt. syslibSearchSystabData failed(%d)", ilRC);
		    dbg (TRACE, "ArrayCreateIntern: Error with Field <%s> in Table <%s><%s>", pclFina, pcpTabNam, pclTabShort);
		    strcpy (pclFieldList, "");
		    ilRC = RC_FAIL;
		}

		else
		{
		    pclStrPtr = pclOneRecBuf;
		    llItemLen = GetDataItem (pclFldLen, pclStrPtr, 1, ',', "\0", "\0\0");
		    pllFieldLens[llCount] = strtol (pclFldLen, NULL, 10);
		    if (plpFldLens != NULL)
		    {
			plpFldLens[llCount] = pllFieldLens[llCount];
		    }

		    /* fieldtype */
		    pclStrPtr = pclOneRecBuf;
		    llItemLen = GetDataItem (pclFity, pclStrPtr, 2, ',', "\0", "\0\0");
		    if (llItemLen < 0)
		    {
			dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llItemLen);
		    }
		    sprintf (pclDummy, "%s,", pclFity);
		    strcat (pclFieldTypeLst, pclDummy);
		}
	    }
	}                           /* end for */
	pclFieldTypeLst[strlen (pclFieldTypeLst) - 1] = 0x00; /*  rem last komma  */
    }                             /* end else */
    strcpy (pclHelpSelect, pclSelect); /* for CEADARRAY  */
    TWEDel2NewDel (pclHelpSelect, ',', ';');
    strcpy (pclHelpFldLst, pclFieldList); /* for CEADARRAY  */
    TWEDel2NewDel (pclHelpFldLst, ',', ';');

    /*  Add all internal (logical) fields to pclFieldList and pllFieldLens  */
    if ((pcpAddFldLst != NULL) && (strcmp (pcpAddFldLst, "") != 0) && (ilRC == RC_SUCCESS))
    {
	strcpy (pclHelpAddFldLst, pcpAddFldLst); /* for CEADARRAY  */
	TWEDel2NewDel (pclHelpAddFldLst, ',', ';');
	llCount = TWECountDelimiter (pcpAddFldLst, ",");
	for (llCnt = 0; (llCnt < llCount) && (ilRC == RC_SUCCESS); llCnt++)
	{
	    llItemLen = GetDataItem (pclFina, pcpAddFldLst, llCnt + 1, ',', "\0", "  ");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "ArrayCreateIntern: Fkt.: GetDataItem (%d)", llItemLen);
	    }

	    else if (llItemLen == 0)
	    {
		dbg (TRACE, "No Name in AddFldLst at pos %ld => ignore", llCnt);
	    }

	    else
	    {
		sprintf (pclDummy, ",%s", pclFina);
		if ((plpAddFldLens == NULL) || (plpAddFldLens[llCnt] <= 0))
		{
		    dbg (TRACE, "No Length for AddField %s! => ignore", pclFina);
		}

		else
		{
		    pllFieldLens[llFldCount] = plpAddFldLens[llCnt];
		    strcat (pclFieldList, pclDummy);
		}
	    }
	    llFldCount++;             /* count for all fields found */
	}                           /* end for */
    }

    /* end if */
    /*
      IS DONE IN AATArrayCreate 
      if (ilRC == RC_SUCCESS)
      {
      ilRC = AllocInternalBuffers(llFldCount, pllFieldLens);
      }
    */
    /* CREATE ARRAY */
    if (ilRC == RC_SUCCESS)
    {
	ilRC =
	    AATArrayCreate (pspHandle, pcpArrayName, TRUE, lpInitialNumRecords, llFldCount, pllFieldLens, pclFieldList,
			    pclFieldTypeLst);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "ArrayCreateIntern: Fkt. AATArrayCreate failed (%d)", ilRC);
	    ilRC = RC_FAIL;
	}

	else
	{
	    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
	    if (ilRC == RC_SUCCESS)
	    {
    		strcpy (prlTmpArray->TableName, pcpTabNam);
		prlTmpArray->DataLoaded = FALSE;
		prlTmpArray->ReorgPending = FALSE;
		prlTmpArray->ActionUpdates = FALSE;
		prlTmpArray->UrnoIdxHandle = NULL;
	        if (plpFldOffsets != NULL)
	        {
		    for (llCnt = 0; llCnt < llFldCount; llCnt++)
		    {
			*plpFldOffsets = prlTmpArray->FieldOffsets[llCnt];
			plpFldOffsets++;
		    }
		}
		prlTmpArray->SqlFieldCount = llSqlFldCount;
	        strcpy (prlTmpArray->SqlFieldList, pcpFldLst);
                ilRC = CreateSqlProperties(prlTmpArray);
	    }
	}
    }

    /* end if */
    /* CREATE CEDA ARRAY  */
    if (ilRC == RC_SUCCESS)
    {
	sprintf (pcgOneRowData, "%-20s,%-10d,%-6s,%-*s,%-*s,%-*s", pcpArrayName, *pspHandle, pcpTabNam, ARR_MAXSELECTLEN,
		 pclHelpSelect, ARR_MAXFLDLSTLEN, pclHelpFldLst, ARR_MAXFLDLSTLEN, pclHelpAddFldLst);
	CreateStoreArray (pcpArrayName, pcgOneRowData);
    }
    return ilRC;
}                               /* end of ArrayCreateIntern */

/* ****************************************************************
 * Function:    CEDAArrayFillHint
 * Parameter:   IN    : pspHandle       Handle for array 
 *                      pcpArrayName    Name for the array
 *                      pcpAddFldData   user defined data list sep by komma
 *                      pcpHint         Oracle HINT, i.e 
 *                                      /#+ NOINDEX(JOBTAB_UJTY) #/   <== *)
 * Description: This function fills an array with data of one
 *              database table. Selection is possible.
 *              Fields and Selection are read from CEDAARRAY created
 *              by CEDAArrayCreate Call. The Example 
 *                /#+ NOINDEX(oracle index name) #/                   <== *)
 *              tells Oracle, not to use index <oracle index name>
 *              To use a special index, use 
 *                /#+ INDEX(oracle index name) #/                     <== *)
 *   <== *) replace all '#' by '*', can not write '/<star>' in c-code
 ***************************************************************** */
int CEDAArrayFillHint (HANDLE * pspHandle, char *pcpArrayName, 
                       char *pcpAddFldData, char *pcpHint)
{
    int ilRC = RC_SUCCESS;
    int ilDBRC = DB_SUCCESS;
	int ilUseDbArray = TRUE;
    int ilFkt = START;
    short slLocalCursor = 0;
    long llRowCnt = 0;
    long llRowNo = ARR_FIRST;
    long llStrLen = 0;
    long llCnt = 0;
    long llFldCnt1 = 0;
    long llFldCnt2 = 0;
    long llFldCnt3 = 0;
    char pclErrDescr[1000];
    char *pclSqlBuf = NULL;
    char pclTabNam[7] = "";
    char pclSelect[ARR_MAXSELECTLEN + 4] = "";
    char pclFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char pclAddFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    AATARRAY *prlTmpArray;
	REC_DESC rlRecDesc;
	short ilCurLine = 0;
	short slMaxLineNo = 500;
	char *pclLinePtr = NULL;

    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayFillHint: Array not found");
	ilRC = RC_FAIL;
    }
    if (ilRC == RC_SUCCESS)
    {
	ilRC = GetCedaArrFlds (pcpArrayName, pclTabNam, pclSelect, pclFldLst, pclAddFldLst);
    }
    if (ilRC == RC_SUCCESS)
    {
	llFldCnt1 = TWECountDelimiter (pclFldLst, ",");
	llFldCnt2 = TWECountDelimiter (pclAddFldLst, ",");
	llFldCnt3 = 0;
	if (pcpAddFldData != NULL)
	{
	    llFldCnt3 = TWECountDelimiter (pcpAddFldData, ",");
	}
	if (llFldCnt3 == 0)
	{
	}

	else if (llFldCnt2 != llFldCnt3)
	{
	    dbg (TRACE, "CEDAArrayFillHint: Different itemnumber in field- and datalist (%ld != %ld)", llFldCnt2, llFldCnt3);
	    ilRC = RC_FAIL;
	}
    }
    if (ilRC == RC_SUCCESS)
    {

	/*  allocate memory for sql statement  */
	llStrLen = 20 + strlen (pclFldLst) + strlen (pclTabNam) + strlen (pclSelect) + strlen (pcpHint);
	if ((pclSqlBuf = (char *) (malloc (llStrLen + 1))) == NULL)
	{
	    ilRC = RC_NOMEM;
	    dbg (TRACE, "CEDAArrayFillHint: Allocation for pclSqlBuf (%ld) failed", llStrLen + 1);
	    return ilRC;
	}
	if (prlTmpArray->SelectDistinct == TRUE)
	{
	    sprintf (pclSqlBuf, "SELECT %s DISTINCT %s FROM %s %s", pcpHint, pclFldLst, pclTabNam, pclSelect);
	}

	else
	{
	    sprintf (pclSqlBuf, "SELECT %s %s FROM %s %s", pcpHint, pclFldLst, pclTabNam, pclSelect);
	}
	dbg (TRACE, "CEDAArrayFillHint: SQL-Statement <%s>", pclSqlBuf);

	/*  
	    The memory for each data record is not dynamical.
	    The size is about 10KByte
	*/
	slLocalCursor = 0;
	ilFkt = START;
	if (lgRefillRow != -9999)
	{
	    llRowNo = lgRefillRow;
	}

	else
	{
	    llRowNo = ARR_FIRST;
	}

	if (ilUseDbArray == TRUE)
	{
		/* ============================= */
		/* READING SQL ARRAY RECORDS NOW */
		/* ============================= */

		dbg(DEBUG,"READING DB ARRAY %d LINES BUFFER",slMaxLineNo);

		InitRecordDescriptor(&rlRecDesc,(1024*1024),0,FALSE);
		rlRecDesc.FldSep = ',';
		rlRecDesc.RecSep = '\0';

		ilFkt = START;
		slLocalCursor = 0;
		llRowCnt = 0;

		ilDBRC = DB_SUCCESS;
		while (ilDBRC == DB_SUCCESS)
		{
			pcgOneRowData[0] = 0x00;

			ilDBRC = SqlIfArray(ilFkt,&slLocalCursor,pclSqlBuf,pcgOneRowData,slMaxLineNo,&rlRecDesc);

			if (ilDBRC != DB_ERROR)
			{
				pclLinePtr = rlRecDesc.Value;
				for (ilCurLine = 1; ilCurLine <= rlRecDesc.LineCount; ilCurLine++)
				{
					strcpy(pcgOneRowData, pclLinePtr);

					/* initialize AddFldData if no list is send  */
					if ((llFldCnt3 == 0) && (llFldCnt2 > 0))
					{
						for (llCnt = 0; llCnt < llFldCnt2; llCnt++)
						{
						strcat (pcgOneRowData, ",");
						}
					}
					else if (llFldCnt2 > 0)
					{                       /* AddFldData are same as AddFldLst items  */
						strcat (pcgOneRowData, ",");
						strcat (pcgOneRowData, pcpAddFldData);

					}
					/* dbg(TRACE,"<%s>",pcgOneRowData); */
					/* AATArrayAddRow needs Null terminated list of data  */
					TWEDel2NewDel (pcgOneRowData, ',', '\0');
					ilRC = AATArrayAddRow (pspHandle, pcpArrayName, &llRowNo, pcgOneRowData);
					if (ilRC != RC_SUCCESS)
					{
						dbg (TRACE, "CEDAArrayFillHint: Fkt. AATArrayAddRow failed  (%d)", ilRC);
						ilRC = RC_FAIL;
						ilDBRC = RC_FAIL;
					}

					pclLinePtr += strlen(pclLinePtr) + 1;
				} /* end for next record */

				llRowCnt += rlRecDesc.LineCount;
				ilFkt = NEXT;
			} /* end if */
			else
			{
			dbg (TRACE, "CEDAArrayFillHint: SQL-ERROR %d", ilDBRC);
			get_ora_err (ilDBRC, pclErrDescr);
			ilRC = RC_FAIL;
			ilDBRC = RC_FAIL;
			}                         /* end else */

			llRowNo = ARR_NEXT;

		} /* end while */

		if (ilDBRC == SQL_NOTFOUND)
		{
		dbg (TRACE, "CEDAArrayFillHint: Added %ld rows to array", llRowCnt);
		}

		dbg(DEBUG,"DB_IF ARRAY ALLOC SIZE= %d",rlRecDesc.CurrSize);

		FreeRecordDescriptor(&rlRecDesc);

	} /* end if UseDbArray ========================= */
	else
	{
		/* ============================= */
		/* READING RECORD BY RECORD NOW  */
		/* ============================= */
	do
	{
	    strcpy (pcgOneRowData, "");
	    ilDBRC = sql_if (ilFkt, &slLocalCursor, pclSqlBuf, pcgOneRowData);
	    if (ilDBRC == DB_SUCCESS)
	    {

		/* initialize AddFldData if no list is send  */
		if ((llFldCnt3 == 0) && (llFldCnt2 > 0))
		{
		    BuildItemBuffer (pcgOneRowData, "", llFldCnt1, ",");
		    for (llCnt = 0; llCnt < llFldCnt2; llCnt++)
		    {
			strcat (pcgOneRowData, ",");
		    }

		    /* AATArrayAddRow needs Null terminated list of data  */
		    TWEDel2NewDel (pcgOneRowData, ',', '\0');
		}
		else if (llFldCnt2 > 0)
		{                       /* AddFldData are same as AddFldLst items  */
		    BuildItemBuffer (pcgOneRowData, "", llFldCnt1, ",");
		    strcat (pcgOneRowData, ",");
		    strcat (pcgOneRowData, pcpAddFldData);

		    /* AATArrayAddRow needs Null terminated list of data  */
		    TWEDel2NewDel (pcgOneRowData, ',', '\0');
		}
		ilRC = AATArrayAddRow (pspHandle, pcpArrayName, &llRowNo, pcgOneRowData);
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "CEDAArrayFillHint: Fkt. AATArrayAddRow failed  (%d)", ilRC);
		    ilRC = RC_FAIL;
		}
		llRowCnt++;
		ilFkt = NEXT;
	    }

	    /* end if */
	    else if (ilDBRC == SQL_NOTFOUND)
	    {
		dbg (DEBUG, "CEDAArrayFillHint: Added %ld rows to array", llRowCnt);
	    }

	    else
	    {
		dbg (TRACE, "CEDAArrayFillHint: SQL-ERROR %d", ilDBRC);
		get_ora_err (ilDBRC, pclErrDescr);
		ilRC = RC_FAIL;
	    }                         /* end else */
	    llRowNo = ARR_NEXT;
	} while ((ilDBRC == DB_SUCCESS) && (ilRC == RC_SUCCESS));

	}


	close_my_cursor (&slLocalCursor);
    }

    prlTmpArray->DataLoaded = TRUE;
    prlTmpArray->ActionUpdates = TRUE;

    if (pclSqlBuf != NULL)
    {
	free (pclSqlBuf);
	pclSqlBuf = NULL;
    }
    return ilRC;
}                               /* end of CEDAArrayFillHint */

/* ****************************************************************
 * Function:    CEDAArrayFill
 * Parameter:   IN    : pspHandle       Handle for array 
 *                      pcpArrayName    Name for the array
 *                      pcpAddFldData   user defined data list sep by komma
 * Description: This function fills an array with data of one
 *              database table. Selection is possible.
 *              Fields and Selection are read from CEDAARRAY created
 *              by CEDAArrayCreate Call. 
 * !!! This function is used for compatibility. Use CEDAArrayRefillHint !!!
 ***************************************************************** */
int CEDAArrayFill (HANDLE * pspHandle, char *pcpArrayName, char *pcpAddFldData)
{
   return CEDAArrayFillHint (pspHandle, pcpArrayName, pcpAddFldData, "");
}

/* ****************************************************************
 * Function:    CEDAArrayRefillHint
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpSelect       New selection string
 *                      pcpAddFldData   user defined data list sep by komma, NOT USED!
 *                      lpRow           ARR_FIRST/ARR_NEXT => Delete/NoDelete
 *                      pcpHint         Oracle HINT, i.e 
 *                                      /#+ NOINDEX(JOBTAB_UJTY) #/   <== *)
 * Description: This function refills an array with data of one
 *              database table. Only a new Selection is possible.
 *              Fields and Table must be the same as in CEDAArrayCreate Call.
 *              The fieldlist and the length of fields didn't change.        
 *              Two possibilities: Function first deletes array and then
 *                                 fills array. (ARR_FIRST)
 *                                 Function adds new rows to array(ARR_NEXT)
 *              The Example 
 *                /#+ NOINDEX(oracle index name) #/                   <== *)
 *              tells Oracle, not to use index <oracle index name>
 *              To use a special index, use 
 *                /#+ INDEX(oracle index name) #/                     <== *)
 *   <== *) replace all '#' by '*', can not write '/<star>' in c-code
 ***************************************************************** */
int CEDAArrayRefillHint (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, 
                         char *pcpAddFldData, long lpRow, char *pcpHint)
{
    int ilRC = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    char pclCedaArray[20] = "";
    HANDLE slCedaHandle = -1;
    HANDLE slIdxHdl = -1;
    AATARRAY *prlTmpArray;
    AATARRAYINDEX *prlTmpIndex;

    dbg(TRACE,"%05d: CEDAArrayRefillHint Arrayname: <%s>, Selection: <%s>",__LINE__,pcpArrayName,pcpSelect);
    lgRefillRow = lpRow;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayRefillHint: Array <%s> not found", pcpArrayName);

	/* +++ jim +++ 20010115: just to avoid warning 'pcpAddFldData NOT REFERENCED' */
	dbg (TRACE, "CEDAArrayRefillHint: called with pcpAddFldData: <%s>", pcpAddFldData);
    }
    if (ilRC == RC_SUCCESS)
    {
	sprintf (pclCedaArray, "%s%s", MARKINTERNARRAY, pcpArrayName);
	ilRC = FindArray (&slCedaHandle, pclCedaArray, &prlTmpArray);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CEDAArrayRefillHint: Array <%s> not found", pclCedaArray);
	}
    }
    if ((ilRC == RC_SUCCESS) && (lpRow == ARR_FIRST))
    {
	ilRC = CEDAArrayDelete (pspHandle, pcpArrayName);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CEDAArrayRefillHint: Fkt. CEDAArrayDelete <%s> failed (%d)", pcpArrayName, ilRC);
	}

	else
	{
	    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg (TRACE, "CEDAArrayRefillHint: Array <%s> not found", pcpArrayName);
	    }
	}
    }

    /* DISACTIVATE INDEX FOR REFILL */
    if ((prlTmpArray->KeyArray != NULL) && (ilRC == RC_SUCCESS))
    {
	for (llRowNum = 0; llRowNum < prlTmpArray->KeyArrayCount; llRowNum++)
	{
	    prlTmpIndex = &prlTmpArray->KeyArray[llRowNum];
	    slIdxHdl = -1;
	    ilRC = CEDAArrayDisactivateIndex (pspHandle, pcpArrayName, &slIdxHdl, prlTmpIndex->Name);

	    if (ilRC != RC_SUCCESS)
	    {

/*
  unbenutzte kein Fehler!!!
  dbg(TRACE,"CEDAArrayRefillHint: CEDAArrayDisactivateIndex %s failed (%d)",
  ilRC, prlTmpIndex->Name);
*/
	    }
	    ilRC = RC_SUCCESS;
	}
    }

    /* REFILL */
    if (ilRC == RC_SUCCESS)
    {
	if (strlen (pcpSelect) <= ARR_MAXSELECTLEN)
	{
	    llRowNum = ARR_FIRST;
	    ilRC = AATArrayPutField (&slCedaHandle, pclCedaArray, NULL, "Sele", llRowNum, TRUE, pcpSelect);
	    if (ilRC == RC_SUCCESS)
	    {
		ilRC = CEDAArrayFillHint (pspHandle, pcpArrayName, NULL, pcpHint);
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "CEDAArrayRefillHint: Fkt. CEDAArrayFill <%s> failed (%d)", pcpArrayName, ilRC);
		}
	    }

	    else
	    {
		dbg (TRACE, "CEDAArrayRefillHint: Fkt.:AATArrayPutField (Select)failed %d", ilRC);
	    }
	}

	else
	{
	    dbg (TRACE, "CEDAArrayRefillHint: Selection is too long");
	}
    }

    /* ACTIVATE INDEX FOR REFILL */
    if ((prlTmpArray->KeyArray != NULL) && (ilRC == RC_SUCCESS))
    {
	for (llRowNum = 0; llRowNum < prlTmpArray->KeyArrayCount; llRowNum++)
	{
	    prlTmpIndex = &prlTmpArray->KeyArray[llRowNum];
	    slIdxHdl = -1;
	    ilRC = CEDAArrayActivateIndex (pspHandle, pcpArrayName, &slIdxHdl, prlTmpIndex->Name);
	    if (ilRC != RC_SUCCESS)
	    {

/*
  unbenutzte kein Fehler
  dbg(TRACE,"CEDAArrayRefillHint: Fkt.CEDAArrayActivateIndex %s failed (%d)",
  ilRC, prlTmpIndex->Name);
*/
	    }
	    ilRC = RC_SUCCESS;
	}
    }
    prlTmpArray->ReorgPending = FALSE;
    lgRefillRow = -9999;
    dbg(DEBUG,"%05d: CEDAArrayRefillHint end",__LINE__);
    return ilRC;
}                               /* end of CEDAArrayRefillHint */

/* ****************************************************************
 * Function:    CEDAArrayRefill
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpSelect       New selection string
 *                      pcpAddFldData   user defined data list sep by komma, NOT USED!
 *                      lpRow           ARR_FIRST/ARR_NEXT => Delete/NoDelete
 * Description: This function refills an array with data of one
 *              database table. Only a new Selection is possible.
 *              Fields and Table must be the same as in CEDAArrayCreate Call.
 *              The fieldlist and the length of fields didn't change.        
 *              Two possibilities: Function first deletes array and then
 *                                 fills array. (ARR_FIRST)
 *                                 Function adds new rows to array(ARR_NEXT)
 * !!! This function is used for compatibility. Use CEDAArrayRefillHint !!!
  ***************************************************************** */
int CEDAArrayRefill (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, char *pcpAddFldData, long lpRow)
{
   return CEDAArrayRefillHint (pspHandle, pcpArrayName, pcpSelect, pcpAddFldData, lpRow, "");
}

/* ****************************************************************
 * Function:    CEDAArraySetFilter
 * Parameter:   IN    : pspHandle     Handle of Array
 *                      pcpArrayName  Name of Array
 *                      pcpFilterStr  Filter string
 * Description: This function is to set a filter to the defined array.
 *              The filter is like the sql-statement in FillArray,
 *              but in a more primitive way for parsing. 
 *
 *              The pcpFilterStr can contain operands like:
 *              "==", "!=", "&&", "||", "<", ">"
 *
 *              Each statement should be written in paranthesis.
 *              The whole pcpFilterStr should be also written in
 *              paranthesis. 
 *              Examples: "(TIFA > 19991214000000)"
 *                        "((TIFA > 19991214000000) && (TIFA < 19991214235900))"
 *                       "(((TIFA > 19991214000000) && (TIFA < 19991214235900))
 *                        ||
 *                        ((TIFD > 19991214000000) && (TIFD < 19991214235900)))"
 *
 *              Till now only first example is realized!!!!!!!!!!!!!
 *
 ***************************************************************** */
int CEDAArraySetFilter (HANDLE * pspHandle, char *pcpArrayName, char *pcpFilterStr)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArraySetFilter: Array <%s> not found", pcpArrayName);
    }
    if ((ilRC == RC_SUCCESS) && (pcpFilterStr[0] != '\0'))
    {
	prlTmpArray->Filter = TRUE;
	strcpy (prlTmpArray->FilterStr, pcpFilterStr);
    }
    return ilRC;
}                               /* end of CEDAArraySetFilter */

/* ****************************************************************
 * Function:    CEDAArraySendChanges2BCHDL
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 *                      bpFlag       Flag (TRUE/FALSE)
 * Description: This function is for setting Arrays sendstatus to 
 *              process bchdl in CEDA. If bpFlag == TRUE all changes
 *              written to Database will be sended to bchdl. 
 *              (FALSE means no send)
 ***************************************************************** */
int CEDAArraySendChanges2BCHDL (HANDLE * pspHandle, char *pcpArrayName, BOOL bpFlag)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArraySendChanges2BCHDL: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	igBchdlId = tool_get_q_id ("bchdl");
	if (igBchdlId <= 0)
	{
	    dbg (TRACE, "CEDAArraySendChanges2BCHDL: bchdl mod_id not found");
	}

	else
	{
	    prlTmpArray->SendToBchdl = bpFlag;
	}
        strcpy(prlTmpArray->RouteIds,"");
    }
    return ilRC;
}                               /* end of CEDAArraySendChanges2BCHDL */

/* ****************************************************************
 * Function:    CEDAArraySendChanges2ACTION
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 *                      bpFlag       Flag (TRUE/FALSE)
 * Description: This function is for setting Arrays sendstatus to 
 *              process action in CEDA. If bpFlag == TRUE all changes
 *              written to Database will be sended to action. 
 *              (FALSE means no send)
 ***************************************************************** */
int CEDAArraySendChanges2ACTION (HANDLE * pspHandle, char *pcpArrayName, BOOL bpFlag)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArraySendChanges2ACTION: Array <%s> not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	igActionId = tool_get_q_id ("action");
	if (igActionId <= 0)
	{
	    dbg (TRACE, "CEDAArraySendChanges2ACTION: action mod_id not found");
	}

	else
	{
	    prlTmpArray->SendToAction = bpFlag;
	    dbg(DEBUG,"Send Changes to ACTION set to %s",bpFlag ? "TRUE" : "FALSE");
	}
        strcpy(prlTmpArray->RouteIds,"");
    }

    return ilRC;
}                               /* end of CEDAArraySendChanges2ACTION */

/* ****************************************************************
 * Function:    CEDAArrayDelete
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 * Description: This function deletes one array without destroy.
 ***************************************************************** */
int CEDAArrayDelete (HANDLE * pspHandle, char *pcpArrayName)
{
    int ilRC = RC_SUCCESS;
    HANDLE slIdxHdl = -1;
    long llRowCnt = 0;
    long llRowNum = 0;
    long llRow = ARR_FIRST;
    AATARRAYINDEX *prlTmpIndex;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayDelete: Array <%s> not found", pcpArrayName);
    }

    /* DISACTIVATE INDEX FOR DELETE */
    if ((prlTmpArray->KeyArray != NULL) && (ilRC == RC_SUCCESS))
    {
	for (llRowNum = 0; llRowNum < prlTmpArray->KeyArrayCount; llRowNum++)
	{
	    prlTmpIndex = &prlTmpArray->KeyArray[llRowNum];
	    slIdxHdl = -1;
	    ilRC = CEDAArrayDisactivateIndex (pspHandle, pcpArrayName, &slIdxHdl, prlTmpIndex->Name);
	    if (ilRC != RC_SUCCESS)
	    {
		if (ilRC != RC_NOTFOUND)
		{
		    dbg (TRACE, "CEDAArrayDelete: CEDAArrayDisactivateIndex %s failed (%d)", prlTmpIndex->Name, ilRC);
		}
	    }
	    ilRC = RC_SUCCESS;
	}
    }
    while (ilRC == RC_SUCCESS)
    {
	ilRC = AATArrayDeleteRow (pspHandle, pcpArrayName, llRow);
	if (ilRC == RC_SUCCESS)
	{
	    llRowCnt++;
	}
	llRow = ARR_NEXT;
    }
    if (ilRC == RC_NOTFOUND)
    {
	ilRC = RC_SUCCESS;
	ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
	if (ilRC == RC_SUCCESS)
	{
	    prlTmpArray->RowCount = 0;
	}
    }

    /* ACTIVATE INDEX AFTER DELETE */
    if ((prlTmpArray->KeyArray != NULL) && (ilRC == RC_SUCCESS))
    {
	for (llRowNum = 0; llRowNum < prlTmpArray->KeyArrayCount; llRowNum++)
	{
	    prlTmpIndex = &prlTmpArray->KeyArray[llRowNum];
	    slIdxHdl = -1;
	    ilRC = CEDAArrayActivateIndex (pspHandle, pcpArrayName, &slIdxHdl, prlTmpIndex->Name);
	    if (ilRC != RC_SUCCESS)
	    {
		if (ilRC != RC_NOTFOUND)
		{
		    dbg (TRACE, "CEDAArrayDelete: Fkt.CEDAArrayActivateIndex %s failed (%d)", ilRC, prlTmpIndex->Name);
		}
	    }
	    ilRC = RC_SUCCESS;
	}
    }
    prlTmpArray->ReorgPending = FALSE;
    return ilRC;
}                               /* end of CEDAArrayDelete */

/* ****************************************************************
 * Function:    CEDAArrayDestroy     
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 * Description: This function frees memory for one array.
 *              Function just calls AATArrayDestroy.
 ***************************************************************** */
int CEDAArrayDestroy (HANDLE * pspHandle, char *pcpArrayName)
{
    int ilRC = RC_SUCCESS;
    HANDLE slCedaHandle = -1;
    char pclCedaArray[20] = "";
    ilRC = AATArrayDestroy (pspHandle, pcpArrayName);
    if (ilRC == RC_SUCCESS)
    {
	sprintf (pclCedaArray, "%s%s", MARKINTERNARRAY, pcpArrayName);
	ilRC = AATArrayDestroy (&slCedaHandle, pclCedaArray);
    }
    return ilRC;
}                               /* end of CEDAArrayDestroy */

/* ****************************************************************
 * Function:    CEDAArrayCreateSimpleIndexUp
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHandle  Handle for Index
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys 
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array
 *              sorted from small (first) to large in all items
 *              of keyfields.
 ***************************************************************** */
int CEDAArrayCreateSimpleIndexUp (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexNam,
                                  char *pcpKeyFldList)
{
    int ilRC = RC_SUCCESS;
    long llDirFlag = ARR_ASC;
    BOOL blMultiFlag = FALSE;
    ilRC =
	AATArrayCreateSimpleIndex (pspHandle, pcpArrayName, pspIndexHandle, pcpIndexNam, pcpKeyFldList, llDirFlag,
				   blMultiFlag); 
    return ilRC;
}                               /* end of CEDAArrayCreateSimpleIndexUp */

/* ****************************************************************
 * Function:    CEDAArrayCreateSimpleIndexDown
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHandle  Handle for Index
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys 
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array
 *              sorted from large (first) to small in all items
 *              of keyfields.
 ***************************************************************** */
int CEDAArrayCreateSimpleIndexDown (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexNam,
                                    char *pcpKeyFldList)
{
    int ilRC = RC_SUCCESS;
    long llDirFlag = ARR_DESC;
    BOOL blMultiFlag = FALSE;
    ilRC =
	AATArrayCreateSimpleIndex (pspHandle, pcpArrayName, pspIndexHandle, pcpIndexNam, pcpKeyFldList, llDirFlag,
				   blMultiFlag); return ilRC;
}                               /* end of CEDAArrayCreateSimpleIndexDown */

/* ****************************************************************
 * Function:    CEDAArrayCreateMultiIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHandle  Handle for Index
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys
 *                      plpDirection    Array of directions (1 Up, -1 Down)
 *                                      (Must be one direction for each field)
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array.
 *              For each item of keyfields the sort can be selected
 *              in plpDirection (ARR_ASC, ARR_DESC).
 ***************************************************************** */
int CEDAArrayCreateMultiIndex (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHandle, char *pcpIndexNam,
                               char *pcpKeyFldList, long *plpDirection)
{
    int ilRc = RC_SUCCESS;
    BOOL blMultiFlag = TRUE;
    /* dbg (TRACE, "%05d:CreateMultiIndex",__LINE__); */
    ilRc = AATArrayInternCreateIndex (pspHandle, pcpArrayName, 
				      pspIndexHandle, pcpIndexNam, pcpKeyFldList, 
				      plpDirection, blMultiFlag); 
    /* dbg (TRACE, "%05d:CreateMultiIndex finished",__LINE__); */
    return ilRc;
} /* end of CEDAArrayCreateMultiIndex */

/* ****************************************************************
 * Function:    CEDAArrayActivateIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHdl     Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function activates an index for
 *              defined an array. So on every delete, update, or add row
 *              the index will be refreshed. For multiple actions 
 *              first disactivate and than activate index.
 ***************************************************************** */
int CEDAArrayActivateIndex (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHdl, char *pcpIndexNam)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayActivateIndex (pspHandle, pcpArrayName, pspIndexHdl, pcpIndexNam, TRUE);
    return ilRC;
}                               /* end of CEDAArrayActivateIndex */

/* ****************************************************************
 * Function:    CEDAArrayDisactivateIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHdl     Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function disactivates an index for
 *              defined an array. So no refresh on index on every delete,
 *              update, or add row till activate is called.
 *              For multiple actions first disactivate and
 *              and than activate index.
 ***************************************************************** */
int CEDAArrayDisactivateIndex (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndexHdl, char *pcpIndexNam)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayActivateIndex (pspHandle, pcpArrayName, pspIndexHdl, pcpIndexNam, FALSE);
    return ilRC;
}                               /* end of CEDAArrayDisactivateIndex */

/* ****************************************************************
 * Function:    CEDAArrayDestroyIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function destroys an index for defined array and
 *              frees its memory.
 ***************************************************************** */
int CEDAArrayDestroyIndex (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayDestroyIndex (pspArray, pcpArrayName, pspIndex, pcpIndexName);
    return ilRC;
}                               /* end of CEDAArrayDestroyIndex */

/* ****************************************************************
 * Function:    CEDAArrayGetRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      lpMaxBytes      maximum size of result data 
 *                                      (incl. Terminator)
 *              OUT   : pvpData         Result Data
 * Description: This function gets data of one row in defined
 *              array (delimited with \0).
 ***************************************************************** */
int CEDAArrayGetRow (HANDLE * pspHandle, char *pcpArrayName, long lpRowNum, char cpDelimiter, long lpMaxBytes,
                     void *pvpData)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayGetRow (pspHandle, pcpArrayName, lpRowNum, cpDelimiter, lpMaxBytes, pvpData);
    return ilRC;
}                               /* end of CEDAArrayGetRow */

/* ****************************************************************
 * Function:    CEDAArrayGetRowPointer
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets pointer to data of one row in defined
 *              array.
 ***************************************************************** */
int CEDAArrayGetRowPointer (HANDLE * pspHandle, char *pcpArrayName, long lpRowNum, void **pvpData)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayGetRowPointer (pspHandle, pcpArrayName, lpRowNum, pvpData);
    return ilRC;
}                               /* end of CEDAArrayGetRowPointer */

/* ****************************************************************
 * Function:    CEDAArrayPutRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pvpDataNew      Input Data (seperated by komma)
 * Description: This function changes data of one row in defined
 *              array.
 ***************************************************************** */
int CEDAArrayPutRow (HANDLE * pspHandle, char *pcpArrayName, long *plpRowNum, void *pvpDataNew)
{
    int ilRC = RC_SUCCESS;
    long llFldCnt = 0;
    long llNumOfFlds1 = 0;
    long llNumOfFlds2 = 0;
    long llFieldNum = -1;
    long llRowNum = -1;
    long llItemLen = -1;
    char pclTabNam[7] = "";
    char pclFina[24] = "";
    char pclSelect[ARR_MAXSELECTLEN + 4] = "";
    char pclFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char pclAddFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char *pclFldStart = NULL;
    char *pclDatStart = NULL;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayPutRow: Array not found");
	return ilRC;
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNum = *plpRowNum;
	ilRC = FindRow (prlTmpArray, &llRowNum, FALSE);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CEDAArrayPutRow: Row %ld not found", llRowNum);
	}

	else
	{
	    *plpRowNum = llRowNum;
	}
    }

    /* get field list from CEDAARRAY  */
    if (ilRC == RC_SUCCESS)
    {
	ilRC = GetCedaArrFlds (pcpArrayName, pclTabNam, pclSelect, pclFldLst, pclAddFldLst);
    }
    if (ilRC == RC_SUCCESS)
    {
	if ((strcmp (pclFldLst, "") != 0) && (strcmp (pclAddFldLst, "") != 0))
	{
	    strcat (pclFldLst, ",");
	}
	strcat (pclFldLst, pclAddFldLst);
	llNumOfFlds1 = TWECountDelimiter (pclFldLst, ",");
	llNumOfFlds2 = TWECountDelimiter (pvpDataNew, ",");
	if (llNumOfFlds1 != llNumOfFlds2)
	{
	    dbg (TRACE, "CEDAArrayPutRow: Number of fields in datalist is not correct");
	    dbg (TRACE, "CEDAArrayPutRow: Array %ld fields, data %ld fields", llNumOfFlds1, llNumOfFlds2);
	    ilRC = RC_FAIL;
	}
	pclFldStart = pclFldLst;
	pclDatStart = pvpDataNew;
	for (llFldCnt = 0; (llFldCnt < llNumOfFlds1) && (ilRC == RC_SUCCESS); llFldCnt++)
	{

	    /* get field name */
	    llItemLen = GetNextDataItem (pclFina, &pclFldStart, ",", "\0", "\0\0");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "CEDAArrayPutRow: Fkt.: GetNextDataItem (%d) (Fields)", llItemLen);
	    }

	    /* get data content */
	    llItemLen = GetNextDataItem (pcgOneFldData, &pclDatStart, ",", "\0", "\0\0");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "CEDAArrayPutRow: Fkt.: GetNextDataItem (%d) (Data)", llItemLen);
	    }
	    llFieldNum = -1;
	    ilRC = AATArrayPutField (pspHandle, pcpArrayName, &llFieldNum, pclFina, llRowNum, TRUE, pcgOneFldData);
	}                           /* end of for all fields */
    }
    return ilRC;
}                               /* end of CEDAArrayPutRow */

/* ****************************************************************
 * Function:    CEDAArrayAddRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pvpDataOrig     Input Data
 * Description: This function adds one new row with data to array.
 ***************************************************************** */
int CEDAArrayAddRow (HANDLE * pspHandle, char *pcpArrayName, long *plpRowNum, void *pvpData)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    ilRC = AATArrayAddRow (pspHandle, pcpArrayName, plpRowNum, pvpData);
    if (ilRC == RC_SUCCESS)
    {
	ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    }
    if (ilRC == RC_SUCCESS)
    {
	ilRC = FindRow (prlTmpArray, plpRowNum, FALSE);
    }
    if (ilRC == RC_SUCCESS)
    {
	prlTmpRow = &prlTmpArray->Rows[*plpRowNum];
	prlTmpRow->ChangeFlag = ARR_DATA_NEW;
    }
    return ilRC;
}                               /* end of CEDAArrayAddRow */

/* ****************************************************************
 * Function:    CEDAArrayAddRowPart
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pcpFldLst       Fieldnamelist that are added (,)
 *                      pcpDataOrig     Input Data (komma seperated!)
 * Description: This function adds one new row with data to array.
 *              The difference to CEDAArrayAddRow is that the data
 *              mustn't contain all fields of array. Not send fields
 *              will be filled with one blanc as default (Later type dependent).
 ***************************************************************** */
int CEDAArrayAddRowPart (HANDLE * pspHandle, char *pcpArrayName, long *plpRowNum, char *pcpFldLst, char *pcpDataOrig)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    ilRC = AATArrayAddRowPart (pspHandle, pcpArrayName, plpRowNum, pcpFldLst, pcpDataOrig);
    if (ilRC == RC_SUCCESS)
    {
	ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    }
    if (ilRC == RC_SUCCESS)
    {
	ilRC = FindRow (prlTmpArray, plpRowNum, FALSE);
    }
    if (ilRC == RC_SUCCESS)
    {
	prlTmpRow = &prlTmpArray->Rows[*plpRowNum];
	prlTmpRow->ChangeFlag = ARR_DATA_NEW;
    }
    return ilRC;
}                               /* end of CEDAArrayAddRowPart */

/* ****************************************************************
 * Function:    CEDAArrayDeleteRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 * Description: This function deletes one row from array. 
 ***************************************************************** */
int CEDAArrayDeleteRow (HANDLE * pspHandle, char *pcpArrayName, long lpRowNum)
{
    int ilRC = RC_SUCCESS;
    long llRowNum = ARR_CURRENT;
    long llIndexNo = ARR_CURRENT;
    char *pclData = NULL;
    AATARRAY *prlTmpArray;
    AATARRAYROW *prlTmpRow;
    AATARRAYINDEX *prlTmpIndex;
    llRowNum = lpRowNum;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC == RC_SUCCESS)
    {
	ilRC = FindRow (prlTmpArray, &llRowNum, FALSE);
    }
    lpRowNum = llRowNum;
    if (ilRC == RC_SUCCESS)
    {
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	if (prlTmpRow->ChangeFlag == ARR_DATA_NEW)
	{
	    prlTmpRow->ChangeFlag = ARR_DATANEW_DELETED;
	}

	else
	{
	    prlTmpRow->ChangeFlag = ARR_DATA_DELETED;
	}
	prlTmpArray->RowCount--;
	pclData = prlTmpRow->DataNew != NULL ? prlTmpRow->DataNew : prlTmpRow->DataOrig;
	if ((prlTmpArray->KeyArray != NULL) && (pclData != NULL))
	{
	    for (llIndexNo = 0; llIndexNo < prlTmpArray->KeyArrayCount; llIndexNo++)
	    {
		prlTmpIndex = &prlTmpArray->KeyArray[llIndexNo];
		if (prlTmpIndex->IsActive == TRUE)
		{
		    if (prlTmpIndex->IndexWrapper != NULL)
		    {
			CreateKey (prlTmpArray, prlTmpIndex, pclData, prlTmpIndex->TmpKeyNew);
			DeleteIndexKey (prlTmpIndex, prlTmpIndex->TmpKeyNew, lpRowNum);
		    }
		}
	    }
	}
    }
    return ilRC;
}                               /* end of CEDAArrayDeleteRow */

/* ****************************************************************
 * Function:    CEDAArrayGetRowCount
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT   : plpRowCount     Number of valid rows in Array 
 * Description: This function returns in last Parameter the number
 *              of valid rows in array described by Handle or name.
 ***************************************************************** */
int CEDAArrayGetRowCount (HANDLE * pspHandle, char *pcpArrayName, long *plpRowCount)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC == RC_SUCCESS)
    {
	*plpRowCount = prlTmpArray->RowCount;
    }

    else
    {
	*plpRowCount = -1;
    }
    return ilRC;
}                               /* end of CEDAArrayGetRowCount */

/* ****************************************************************
 * Function:    CEDAArrayGetFields
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFields       List of fieldlens
 *                      pcpFldLst       List of fields
 *                      cpDelimiter     Delimiter for result data (NULL = none)
 *                      lpMaxBytes      maximum size of result data
 *                                      (incl. Terminator and delimiters)  
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets a list of field data for defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetFields (HANDLE * pspHandle, char *pcpArrayName, long *plpFields, char *pcpFldLst, char cpDelimiter,
                        long lpMaxBytes, long lpRowNum, void *pvpData)
{
    int ilRC = RC_SUCCESS;
    ilRC =
	AATArrayGetFields (pspHandle, pcpArrayName, plpFields, pcpFldLst, cpDelimiter, lpMaxBytes, lpRowNum, pvpData,
			   FALSE);
    return ilRC;
}                               /* end of CEDAArrayGetFields */

/* ****************************************************************
 * Function:    CEDAArrayGetField
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFields       Field length
 *                      pcpFieldNam     Name of field
 *                      lpMaxBytes      maximum size of result data
 *                                      (incl. Terminator)  
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets data of one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetField (HANDLE * pspHandle, char *pcpArrayName, long *plpFields, char *pcpFieldNam, long lpMaxBytes,
                       long lpRowNum, void *pvpDataBuf)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayGetField (pspHandle, pcpArrayName, plpFields, pcpFieldNam, lpMaxBytes, lpRowNum, pvpDataBuf);
    return ilRC;
}                               /* end of CEDAArrayGetField */

/* ****************************************************************
 * Function:    CEDAArrayGetFieldPointer
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Position of field in list
 *                      pcpFieldNam     Name of field
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data Pointer
 * Description: This function gets pointer to data of one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetFieldPointer (HANDLE * pspHandle, char *pcpArrayName, long *plpFieldNum, char *pcpFieldNam,
                              long lpRowNum, void **pvpDataBuf)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayGetFieldPointer (pspHandle, pcpArrayName, plpFieldNum, pcpFieldNam, lpRowNum, pvpDataBuf);
    return ilRC;
}                               /* end of CEDAArrayGetFieldPointer */

/* ****************************************************************
 * Function:    CEDAArrayPutField
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Number of field in array
 *                      pcpFieldNam     Name of field
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pvpDataNew      Input Data
 * Description: This function puts data to one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayPutField (HANDLE * pspHandle, char *pcpArrayName, long *plpFieldNum, char *pcpFieldNam, long lpRowNum,
                       void *pvpDataNew)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayPutField (pspHandle, pcpArrayName, plpFieldNum, pcpFieldNam, lpRowNum, TRUE, pvpDataNew);
    return ilRC;
}                               /* end of CEDAArrayPutField */

/* ****************************************************************
 * Function:    CEDAArrayPutFields
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Array of FieldNumbers in array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pcpFieldLst     List of Fields (komma seperated)
 *                      pvpDataNewLst   List of new Data (komma seperated)
 * Description: This function puts data to all fields in one row
 *              seperated by komma in pcpFieldLst.
 *              Data is from pvpDataNewLst.
 ***************************************************************** */
int CEDAArrayPutFields (HANDLE * pspHandle, char *pcpArrayName, long *plpFieldNum, long *plpRowNum, char *pcpFieldLst,
                        void *pvpDataNewLst)
{
    int ilRC = RC_SUCCESS;
    short slFlag = TRUE;
    long llFieldNum = -1;
    long llRowNum = -1;
    long llItemLen;
    char *pclFieldStart = NULL;
    char *pclDataStart = NULL;
    char pclFieldNam[24] = "";
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayPutFields: Array not found");
	return ilRC;
    }
    if (ilRC == RC_SUCCESS)
    {
	llRowNum = *plpRowNum;
	ilRC = FindRow (prlTmpArray, &llRowNum, FALSE);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CEDAArrayPutFields: Row %ld not found", llRowNum);
	}

	else
	{
	    *plpRowNum = llRowNum;
	}
    }
    if (ilRC == RC_SUCCESS)
    {
	pclFieldStart = pcpFieldLst;
	pclDataStart = (char *) pvpDataNewLst;

	do
	{
	    llItemLen = GetNextDataItem (pclFieldNam, &pclFieldStart, ",", "\0", "\0\0");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "CEDAArrayPutFields: Fkt.: GetNextDataItem (%d) (Fields)", llItemLen);
	    }
	    llItemLen = GetNextDataItem (pcgOneFldData, &pclDataStart, ",", "\0", "\0\0");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "CEDAArrayPutFields: Fkt.: GetNextDataItem (%d) (Data)", llItemLen);
	    }
	    if ((plpFieldNum != NULL) && (*plpFieldNum > 0) && (slFlag == TRUE))
	    {
		llFieldNum = *plpFieldNum;
	    }

	    else
	    {
		llFieldNum = -1;
		slFlag = FALSE;
	    }
	    if (strcmp (pclFieldNam, "") != 0)
	    {
		ilRC = AATArrayPutField (pspHandle, pcpArrayName, &llFieldNum, pclFieldNam, llRowNum, TRUE, pcgOneFldData);
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "CEADArrayPutFields: AATArrayPutField failed for field <%s> and data <%s>", pclFieldNam,
			 pcgOneFldData);}
		if ((plpFieldNum != NULL) && (ilRC == RC_SUCCESS))
		{
		    *plpFieldNum = llFieldNum;
		    if (*pclFieldNam != '\0')
		    {
			plpFieldNum++;
		    }
		}
	    }
	}
	while ((*pclFieldNam != '\0') && (ilRC == RC_SUCCESS));
    }
    return ilRC;
}                               /* end of CEDAArrayPutField */

/* ****************************************************************
 * Function:    CEDAArrayFindKey
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle of Index
 *                      pcpIndexName    Name of index
 *              IN/OUT: plpIndex        RowNum Index (ARR_FIRST, ARR_LAST ...)
 *                                      or ARR_FIND for search
 *                                      Back: RowNum of array
 *                      lpMaxBytes      Max size of result data
 *                                      (incl. Terminator)  
 *              IN/OUT: pvpKey          Result Data (Key)
 * Description: This function returns data in Key of row.
 *              Also it returns Number of array Row to get record.
 *              With function ARR_FIND Key content could be searched.
 *              This content should be komma seperated data in key
 *              (Always from first till n keyitems!)
 ***************************************************************** */
int CEDAArrayFindKey (HANDLE * pspHandle, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName, long *plpIndex,
                      long lpMaxBytes, void *pvpKey)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayFindKey (pspHandle, pcpArrayName, pspIndex, pcpIndexName, plpIndex, lpMaxBytes, pvpKey);
    return ilRC;
}                               /* end of CEDAArrayFindKey  */

/* ****************************************************************
 * Function:   CEDAArrayFindRowPointer
 * Parameter:  IN     : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle of Index
 *                      pcpIndexName    Name of index
 *                      pcpPattern      Search Pattern in Keys  
 *             IN & OUT:plpIndex        (ARR_FIRST,..) back: Row in Array
 *             OUT     :pvpKey          Result Data (Key)
 * Description: This function returns data stored in array in one
 *              row. The row is found with index. The Row could be
 *              defined by an Pattern and ARR_FIRST or ARR_LAST,
 *              or just by ARR_FIRST, ARR_LAST, ARR_NEXT, ARR_PREV ...
 *              With pattern and FIRST/NEXT LAST/PREV all found records 
 *              become returned. FIRST and Pattern is like FindKey
 *              with an Key content. 
 ***************************************************************** */
int CEDAArrayFindRowPointer (HANDLE * pspArray, char *pcpArrayName, HANDLE * pspIndex, char *pcpIndexName,
                             char *pcpPattern, long *plpIndex, void **pvpDataBuf)
{
    int ilRC = RC_SUCCESS;
    ilRC = AATArrayFindRowPointer (pspArray, pcpArrayName, pspIndex, pcpIndexName, pcpPattern, plpIndex, pvpDataBuf);
    return ilRC;
}                               /* end of CEDAArrayFindRowPointer */

/* ****************************************************************
 * Function:    CEDAArrayReadAllHeader
 * Parameter:   IN :   outp       Pointer to output (file, stdout) 
 * Description: This function prints out header information 
 *              from all existing arrays to stdout.
 *              (Name, Handle, NumberRows)
 ***************************************************************** */
int CEDAArrayReadAllHeader (FILE * outp)
{
    int ilRC = RC_SUCCESS;
    long llCnt = 1;
    HANDLE slIndex = -1;
    AATARRAY *prlTmpArray = NULL;
    if (outp == NULL)
    {
	dbg (TRACE, "CEDAArrayReadAllHeader: No output exists");
	ilRC = RC_FAIL;
    }
    if (rgBaseArray.Rows == NULL)
    {
	ilRC = RC_NOTINITIALIZED;
    }
    if (ilRC == RC_SUCCESS)
    {
	fprintf (outp, "CEDAArrayReadAllHeader: HEADERINFORMATIONS\n");
	fprintf (outp, "CEDAArray: %d Arrays created\n", rgBaseArray.RowCount / 2);
	for (slIndex = rgBaseArray.MaxRows - 1; slIndex >= 0; slIndex--)
	{
	    if ((prlTmpArray = (AATARRAY *) rgBaseArray.Rows[slIndex].DataOrig) != NULL)
	    {
		if (slIndex % 2)
		{
		    fprintf (outp, "%ld. Array: Name <%s>, Handle %d, Rows %ld\n", llCnt, prlTmpArray->Name, slIndex,
			     prlTmpArray->RowCount);
		    llCnt++;
		}

		else
		{

		    /*
		      fprintf(outp, "Intern created Array <%s>, Handle %d, Rows %ld\n\n",
		      prlTmpArray->Name, slIndex, prlTmpArray->RowCount);
		    */
		}
	    }
	}
	fprintf (outp, "\n\n");
    }
    return ilRC;
}                               /* end of CEDAArrayReadAllHeader  */

/* ****************************************************************
 * Function:    CEDAArrayInfoOneArray
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array 
 *                      outp            Pointer to output (file, stdout) 
 * Description: This function prints out header information 
 *              from one existing arrays to stdout.
 *              (Name, Handle, NumberRows, Table, Selection, Fieldlist)
 ***************************************************************** */
int CEDAArrayInfoOneArray (HANDLE * pspHandle, char *pcpArrayName, FILE * outp)
{
    int ilRC = RC_SUCCESS;
    char pclTabNam[7] = "";
    char pclSelect[ARR_MAXSELECTLEN + 4] = "";
    char pclFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char pclAddFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    AATARRAY *prlTmpArray = NULL;
    if (outp == NULL)
    {
	dbg (TRACE, "CEDAArrayInfoOneArray: No output exists");
	ilRC = RC_FAIL;
    }
    if (rgBaseArray.Rows == NULL)
    {
	ilRC = RC_NOTINITIALIZED;
    }
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayInfoOneArray: Array %s not found", pcpArrayName);
    }
    if (ilRC == RC_SUCCESS)
    {
	fprintf (outp, "CEDAArrayInfoOneArray: HEADERINFORMATION for %s\n", pcpArrayName);
	fprintf (outp, "Name <%s>, Handle %d, Rows %ld\n", prlTmpArray->Name, *pspHandle, prlTmpArray->RowCount);

	/* get fieldlist and selection  */
	ilRC = GetCedaArrFlds (pcpArrayName, pclTabNam, pclSelect, pclFldLst, pclAddFldLst);
	fprintf (outp, "Tablename <%s>, Selection <%s>,\n", pclTabNam, pclSelect);
	fprintf (outp, "Database fields: <%s>\n", pclFldLst);
	fprintf (outp, "User fields: <%s>\n", pclAddFldLst);
    }
    fprintf (outp, "\n\n");
    return ilRC;
}                               /* end of CEDAArrayInfoOneArray  */

/* ****************************************************************
 * Function:    CEDAArrayEventUpdate
 * Parameter:   IN    : prpEvent     Eventstructure  (pcgEvent from skeleton)
 * Return:      RC_SUCCESS if all ok 
 * Description: This function updates all arrays with events data send
 *              from ACTION for example. The commands "IRT,URT,DRT"
 *              are valid and will update all arrays that are filled
 *              with table of event.
 *              Selections must content "URNO =" or URNO IN (...)
 *              to get right record in array.
 *              Multiple (newline) events are not possible yet.
 ***************************************************************** */
int CEDAArrayEventUpdate (EVENT * prpEvent)
{
    int ilRC = RC_SUCCESS;
    char pclUrnoLst[100];
    char pclTable[30];
    ilRC = CEDAArrayEventUpdate2 (prpEvent, pclUrnoLst, pclTable);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayEventUpdate: CEDAArrayEventUpdate2 failed (%d)", ilRC);
    }
    return ilRC;
}                               /* end of CEDAArrayEventUpdate */

/* ****************************************************************
 * Function:    CEDAArrayEventUpdate2
 * Parameter:   IN    : prpEvent     Eventstructure  (pcgEvent from skeleton)
 *              OUT   : pcpUrnoLst   The Urnos that are in Eventselection
 *                      pclTable     The Tablename from Event
 * Return:      RC_SUCCESS if all ok 
 * Description: This function updates all arrays with events data send
 *              from ACTION for example. The commands "IRT,URT,DRT"
 *              are valid and will update all arrays that are filled
 *              with table of event.
 *              Selections must content "URNO =" or URNO IN (...)
 *              to get right record in array.
 *              Multiple (newline) events are not possible yet.
 ***************************************************************** */
int CEDAArrayEventUpdate2 (EVENT * prpEvent, char *pcpUrnoLst, char *pcpTable)
{
    int ilRC = RC_SUCCESS;
    int ilGetRc = RC_SUCCESS;
    int ilNewVersion = 1;
    short slIndex = 0;
    HANDLE slArrHandle = -1;
    BC_HEAD *prlBchd = NULL;
    CMDBLK *prlCmdblk = NULL;
    char *pclSelKey = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    AATARRAY *prlTmpArray = NULL;
    char pclTable[16];
    char pclActCmd[16];
    char pclRecvName[100];
    char pclDestName[100];
    char pclArrTabNam[32];
    char pclArrSelect[ARR_MAXSELECTLEN + 4];
    char pclArrFldLst[ARR_MAXFLDLSTLEN + 4];
    char pclArrAddFldLst[ARR_MAXFLDLSTLEN + 4];
    prlBchd = (BC_HEAD *) ((char *) prpEvent + sizeof (EVENT)); /* broadcast header */

    /* Save Global Elements of BC_HEAD */
    strcpy (pclRecvName, prlBchd->recv_name); /* Workstation */
    strcpy (pclDestName, prlBchd->dest_name); /* UserLoginName */

    /* get command block  */
    /* Save Global Elements of CMDBLK */
    prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);

/*
  strcpy(pcgTwStart,prlCmdblk->tw_start);
  strcpy(pcgTwEnd,prlCmdblk->tw_end);
*/
    strcpy (pclTable, prlCmdblk->obj_name); /*  table name  */
    strcpy (pclActCmd, prlCmdblk->command); /*  actual command  */

    /*  3 Strings in CMDBLK -> data  */
    pclSelKey = (char *) prlCmdblk->data;
    pclFields = (char *) pclSelKey + strlen (pclSelKey) + 1;
    pclData = (char *) pclFields + strlen (pclFields) + 1;
    dbg (DEBUG, "CMD    <%s>  TBL <%s>", pclActCmd, pclTable);
    dbg (DEBUG, "SELECT <%s>", pclSelKey);
    dbg (DEBUG, "FIELDS <%s>", pclFields);
    dbg (DEBUG, "DATA   <%s>", pclData);
    strcpy (pcpTable, pclTable);

    /* check all arrays for tablename  */
    for (slIndex = rgBaseArray.MaxRows - 1; (slIndex >= 0); slIndex--)
    {
	ilRC = RC_SUCCESS;
	prlTmpArray = (AATARRAY *) rgBaseArray.Rows[slIndex].DataOrig;
	if ((prlTmpArray->Name != NULL) && (strcmp (prlTmpArray->Name, "") != 0) &&
	    (strncmp (prlTmpArray->Name, MARKINTERNARRAY, 2) != 0))
	{
	    ilGetRc = RC_SUCCESS;
	    if (ilNewVersion == 1)
	    {
		if (strcmp(prlTmpArray->TableName, pclTable) != 0)
		{
			ilGetRc = RC_FAIL;
	    	}
	    }
	    else
	    {
	        ilRC = GetCedaArrFlds (prlTmpArray->Name, pclArrTabNam, pclArrSelect, pclArrFldLst, pclArrAddFldLst);
		if ((ilRC == RC_SUCCESS) && (strcmp(pclTable, pclArrTabNam) != 0))
		{
			ilGetRc = RC_FAIL;
	    	}
	    }
	    if (ilGetRc == RC_SUCCESS)
	    {
		dbg(TRACE,"DATA TABLE <%s> ARRAY <%s> (ROWS=%d)",prlTmpArray->TableName,prlTmpArray->Name,prlTmpArray->RowCount);
		slArrHandle = slIndex;

		/*  GO TO FUNCTION REPRESENTS COMMAND  */
		if (strcmp ("IRT", pclActCmd) == 0)
		{
			dbg(TRACE,"PERFORMING INSERT");
		    ilRC = InsertEventData (&slArrHandle, prlTmpArray->Name, pclFields, pclData, pcpUrnoLst);
		    if (ilRC != RC_SUCCESS)
		    {
			dbg (DEBUG, "CEDAArrayEventUpdate2: InsertEventData in array <%s> failed", prlTmpArray->Name);
			ilRC = RC_SUCCESS;
		    }
			dbg(DEBUG,"DONE");
		}

		/* end if */
		else if (strcmp ("URT", pclActCmd) == 0)
		{
			dbg(TRACE,"PERFORMING UPDATE");
		    ilRC = UpdateEventData (&slArrHandle, prlTmpArray->Name, pclSelKey, pclFields, pclData, pcpUrnoLst);
		    if (ilRC != RC_SUCCESS)
		    {
			dbg (DEBUG, "CEDAArrayEventUpdate2: UpdateEventData in array <%s> failed", prlTmpArray->Name);
			if (ilRC != RC_WRONGDATA)
			{
			    ilRC = RC_SUCCESS;
			}

			else
			{
			    slIndex = -1;
			}
		    }
			dbg(DEBUG,"DONE");
		}

		/* end if */
		else if (strcmp ("DRT", pclActCmd) == 0)
		{
			dbg(TRACE,"PERFORMING DELETE");
		    ilRC = DeleteEventData (&slArrHandle, prlTmpArray->Name, pclSelKey, pcpUrnoLst);
		    if (ilRC != RC_SUCCESS)
		    {
			dbg (DEBUG, "CEDAArrayEventUpdate2: DeleteEventData in array <%s> failed", prlTmpArray->Name);
			ilRC = RC_SUCCESS;
		    }
			dbg(DEBUG,"DONE");
		}

		/* end if */
		else
		{
		    dbg (TRACE, "CEDAArrayEventUpdate2: COMMAND <%s> UNKNOWN", pclActCmd);
		    slIndex = -1;
		}                       /* end else */
	    } /* if tablename ok  */
	}

	/* if array valid  */
    }                             /*  for all arrays  */
    return ilRC;
}                               /* end of CEDAArrayEventUpdate2 */

/* ****************************************************************
 * Function:    CEDAArrayDBUpdateArray
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array 
 * Return:      RC_SUCCESS, RC_FAIL
 * Description: This function must be called after CEDAArrayWriteDB
 *              if the commit_flag was ARR_COMMIT_USER. Otherwise
 *              the array is already updated to current DB contents.
 *              If return != RC_SUCCESS destroy and fill or refill array.
 ***************************************************************** */
int CEDAArrayDBUpdateArray (HANDLE * pspHandle, char *pcpArrayName)
{
    int ilRC = RC_SUCCESS;
    AATARRAY *prlTmpArray = NULL;
    AATARRAYROW *prlTmpRow = NULL;
    long llRowNum = 0;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayDBUpdateArray: Array %s not found", pcpArrayName);
	return ilRC;
    }

    /* refresh for all rows in array  */
    for (llRowNum = 0; (llRowNum < prlTmpArray->MaxRows) && (ilRC == RC_SUCCESS); llRowNum++)
    {
	prlTmpRow = &prlTmpArray->Rows[llRowNum];
	if ((prlTmpRow->ChangeFlag == ARR_DATA_DELETED) || (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED))
	{
	    ilRC = AATArrayDeleteRow (pspHandle, pcpArrayName, llRowNum);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg (TRACE, "CEDAArrayDBUpdateArray: Refresh Array with AATArrayDeleteRow (%ld) failed (%d)", llRowNum, ilRC);
	    }
	}

	else
	{
	    UnsetRowChanges (prlTmpRow, prlTmpArray);
	    /* dbg (TRACE, "Line %ld: UnsetRowChanges for %ld ...", __LINE__,llRowNum); 
	     */
	}
    }
    return ilRC;
}

/*******************************************************************
                WRITING CEDA FUNKTIONS EXTERNAL
********************************************************************/
/* ****************************************************************
 * Function:    CEDAArrayWriteDB
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT:    pcpUpdSelLst    Komma and newline seperated list
 *                                      of selections for records
 *                      pcpUpdFldLst    Komma and newline seperated list
 *                                      of updated fields of records
 *                      pcpUpdDatLst    Komma and newline seperated list
 *                                      of fields update data
 *                      plpNumUrnos     Number of changed Urnos (no insert)
 * Description: This function calls the function that fills database 
 *              with data of defined array.
 *              For LOGHDL:
 *              When an update or delete is done, the URNO of  
 *              the record is written in one line
 *              of pcpUpdSelLst, the updated fields are written to
 *              the same line of pcpUpdFldLst and the new or deleted 
 *              data for these fields is written to same line in
 *              pcpUpdDatLst. 
 *              This is for LOGHDL messages and history of DB operations! 
 ***************************************************************** */
int CEDAArrayWriteDB (HANDLE * pspHandle, char *pcpArrayName, char **pcpUpdSelLst, char **pcpUpdFldLst,
                      char **pcpUpdDatLst, long *plpNumUrnos, long lpCommitFlag)
{
    int ilRC = RC_SUCCESS;
    ilRC =
	CEDAArrayWriteArray2DB (pspHandle, pcpArrayName, NULL, pcpUpdSelLst, pcpUpdFldLst, pcpUpdDatLst, plpNumUrnos,
				lpCommitFlag); return ilRC;
}                               /* end of CEDAArrayWriteDB */

/* ****************************************************************
 * Function:    CEDAArrayWriteDBGetCmds
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT:    pcpCmdLst       newline seperated list 
 *                                      of commands done to DBwritten records
 *                                      (IRT,URT OR DRT)
 *                      pcpUpdSelLst    Komma and newline seperated list
 *                                      of selections for records
 *                      pcpUpdFldLst    Komma and newline seperated list
 *                                      of updated fields of records
 *                      pcpUpdDatLst    Komma and newline seperated list
 *                                      of fields update data
 *                      plpNumUrnos     Number of changed Urnos (no insert)
 * Description: This function calls the function that fills database 
 *              with data of defined array.
 *              For BROADCASTS:
 *              The function has one more parameter than CEDAArrayWriteDB.
 *              For broadcasts the command must be send also.
 *              When an insert, update or delete is done, the command
 *              IRT, DRT or URT is written to one line in pcpCmdLst.
 *              The URNO of the new or deleted or changed record
 *              is written to the same line of pcpUpdSelLst,
 *              the updated fields are written to the same line of 
 *              pcpUpdFldLst and the new or deleted data for these fields
 *              is written to same line in pcpUpdDatLst.
 *              This is for BCHDL messages and history of DB operations! 
 ***************************************************************** */
int CEDAArrayWriteDBGetChanges (HANDLE * pspHandle, char *pcpArrayName, char **pcpCmdLst, char **pcpUpdSelLst,
                                char **pcpUpdFldLst, char **pcpUpdDatLst, long *plpNumUrnos, long lpCommitFlag)
{
    int ilRC = RC_SUCCESS;
    ilRC =
	CEDAArrayWriteArray2DB (pspHandle, pcpArrayName, pcpCmdLst, pcpUpdSelLst, pcpUpdFldLst, pcpUpdDatLst, plpNumUrnos,
				lpCommitFlag);
    return ilRC;
}                               /* end of CEDAArrayWriteDBGetChanges */

/* ****************************************************************
 * Function:    CEDAArrayWriteArray2DB
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT:    pcpCmdLst       newline seperated list 
 *                                      of commands done to DBwritten records
 *                                      (IRT,URT OR DRT)
 *                      pcpUpdSelLst    Komma and newline seperated list
 *                                      of selections for records
 *                      pcpUpdFldLst    Komma and newline seperated list
 *                                      of updated fields of records
 *                      pcpUpdDatLst    Komma and newline seperated list
 *                                      of fields update data
 *                      plpNumUrnos     Number of changed Urnos (no insert)
 * Description: This function fills database with data of defined array.
 *              Fields in array with unchanged data become ignored.
 *              Changed fields become updated. If an row from array was
 *              deleted the record in database will be deleted too.
 *              Added rows become inserted in database.
 *              User defined fields in AddFieldList from ArrayCreate
 *              are ignored.
 *              For LOGHDL:
 *              When an update or delete is done, the selection for
 *              the record (WHERE URNO = ...) is written in one line
 *              of pcpUpdSelLst, the updated fields are written to
 *              the same line of pcpUpdFldLst and the new or deleted 
 *              data for these fields is written to same line in
 *              pcpUpdDatLst. 
 *              This is for LOGHDL messages and history of DB operations! 
 ***************************************************************** */
static int CEDAArrayWriteArray2DB (HANDLE * pspHandle, char *pcpArrayName, char **pcpCmdLst, char **pcpUpdSelLst,
                                   char **pcpUpdFldLst, char **pcpUpdDatLst, long *plpNumUrnos, long lpCommitFlag)
{
    int ilRC = RC_SUCCESS;
    int ilDBRC = DB_SUCCESS;
    long llItemLen = 0;
    long llUpdRecNum = 0;
    long llUpdCnt = 0;
    long llRowCnt = 0;
    long llRow = ARR_FIRST;
    long llRowNum = ARR_CURRENT;
    long llFldNum = -1;
    long llFields[300];
    long llInsRows = 0;
    long llDelRows = 0;
    long llUpdRows = 0;
    short slICursor = 0;
    short slUCursor = 0;
    short slDCursor = 0;
    char pclTabNam[7] = "";
    char pclUrno[30] = "";
    char pclRoute[50] = "";
    char pclRouteIds[100] = "";
    char pclSelect[ARR_MAXSELECTLEN + 4] = "";
    char pclFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char pclAddFldLst[ARR_MAXFLDLSTLEN + 4] = "";
    char pclOneUpdFldLst[ARR_MAXFLDLSTLEN + 4] = ""; /* Updatefields for one Row */
    static char *pclOneUpdDatLst = NULL; /* Data for Update of one Row */
    static long llOneUpdDatLstLen = 0;
    AATARRAY *prlTmpArray = NULL;
    AATARRAYROW *prlTmpRow = NULL;
    if (pcgUpdDatLst != NULL)
    {
	pcgUpdDatLst[0] = 0x00;
	lgUpdDatLstLen = 0;
    }
    if (pcgUpdFldLst != NULL)
    {
	pcgUpdFldLst[0] = 0x00;
	lgUpdFldLstLen = 0;
    }
    if (pcgUpdSelLst != NULL)
    {
	pcgUpdSelLst[0] = 0x00;
	lgUpdSelLstLen = 0;
    }
    if (pcgCmdLst != NULL)
    {
	pcgCmdLst[0] = 0x00;
    }
    lgCmdLstLen = 0;
    lgUpdSelLstLen = 0;
    lgUpdFldLstLen = 0;
    lgUpdDatLstLen = 0;
    slICursor = 0;
    slUCursor = 0;
    slDCursor = 0;
    llInsRows = 0;
    llUpdRows = 0;
    llDelRows = 0;
    llRowCnt = 0;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (TRACE, "CEDAArrayWriteDB: Array %s not found", pcpArrayName);
	return ilRC;
    }

    else
    {

	/* get field list of Array  */
	ilRC = GetCedaArrFlds (pcpArrayName, pclTabNam, pclSelect, pclFldLst, pclAddFldLst);
	if (ilRC != RC_SUCCESS)
	{
	    dbg (TRACE, "CEDAArrayWriteDB: Fkt. GetCedaArrFlds failed");
	}
    }
    /*
    dbg(TRACE,"IN CEDAArrayWriteArray2DB");
    dbg(TRACE,"ARRAY NAME   <%s>",pcpArrayName);
    dbg(TRACE,"ARRAY TABLE  <%s>",pclTabNam);
    dbg(TRACE,"ARRAY SELKEY <%s>",pclSelect);
    dbg(TRACE,"ARRAY FIELDS <%s>",pclFldLst);
    */

    *pcgInsertFieldBuf = '\0';
    *pcgInsertSqlBuf = '\0';

    /* get Data of each array row (one record) and update Database  */
    lgChkDifCnt = 0;
    llRow = ARR_FIRST;
    *llFields = -1;
    while ((ilRC == RC_SUCCESS) && (ilDBRC == DB_SUCCESS))
    {
  	AATARRAY *prlTmpArray;
  	ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
  	if (ilRC == RC_SUCCESS)
  	{
	    ilRC = FindRow (prlTmpArray, &llRow, TRUE);
	    if (ilRC == RC_SUCCESS)
	    {
    		AATARRAYROW *prlTmpRow;
    		prlTmpRow = &prlTmpArray->Rows[llRow];
    		if ((prlTmpRow->DataNew == NULL)  && (prlTmpRow->ChangeFlag != ARR_DATA_NEW && prlTmpRow->ChangeFlag != ARR_DATA_DELETED))
		{
		    /* no changes 
		       switch(prlTmpRow->ChangeFlag)
		       {
		       case ARR_DATA_EMPTY:
		       dbg(TRACE,"ARR_DATA_EMPTY");
		       break;
		       case ARR_DATA_UNCHANGED:
		       dbg(TRACE,"ARR_DATA_UNCHANGED");
		       break;
		       case ARR_DATA_CHANGED:
		       dbg(TRACE,"ARR_DATA_CHANGED");
		       break;
		       case ARR_DATA_NEW:
		       dbg(TRACE,"ARR_DATA_NEW");
		       break;
		       case ARR_DATA_DELETED:
		       dbg(TRACE,"ARR_DATA_DELETED");
		       break;
		       case ARR_DATANEW_DELETED:
		       dbg(TRACE,"ARR_DATANEW_DELETED");
		       break;
		       default:
		       dbg(TRACE,"invalid ChangeFlag");

		       }
		    */
		    llRow = ARR_NEXT;
		    continue;
		}
		else
		{

		    llRow = ARR_CURRENT;
		    strcpy (pcgOneRowData, "");
/***
    ilRC =
    AATArrayGetFields(pspHandle, pcpArrayName, llFields, pclFldLst, 
    ',', lgOneRowDataLen, llRow, pcgOneRowData,TRUE);
***/
		    ilRC = AATArrayGetRow2(pspHandle, pcpArrayName, 
					   llRow, ',',lgOneRowDataLen-1,pcgOneRowData,TRUE);
		    llRow = ARR_NEXT;
		    if (ilRC != RC_SUCCESS)
		    {
			if (ilRC == RC_NOTFOUND)
			{
			    if (llRowCnt == 0)
			    {
				dbg (TRACE, "CEDAArrayWriteDB: Array is empty => nothing to write");
				return ilRC;
			    }

			    else
			    {
				dbg (TRACE, "CEDAArrayWriteDB: All %ld rows of array <%s> done", llRowCnt, pcpArrayName);
			    }
			}

			else
			{
			    dbg (TRACE, "CEDAArrayWriteDB: Fkt. AATArrayGetFields failed (%d)", ilRC);
			    if (llRowCnt == 0)
			    {
				return ilRC;
			    }
			}
		    }
		    else
		    {
			llRowCnt++;

			/* allocate memory for one rows update data */
			if (pclOneUpdDatLst == NULL)
			{
			    llOneUpdDatLstLen = prlTmpArray->RowLen + 4;
			    pclOneUpdDatLst = (char *) (malloc (llOneUpdDatLstLen));
			}
			else if (prlTmpArray->RowLen >= llOneUpdDatLstLen - 1)
			{
			    llOneUpdDatLstLen = prlTmpArray->RowLen + 4;
			    pclOneUpdDatLst = (char *) (realloc (pclOneUpdDatLst, llOneUpdDatLstLen));
			}
			if (pclOneUpdDatLst == NULL)
			{
			    dbg (TRACE, "CEDAArrayWriteDB: Malloc of OneUpdDatLst (%ld) failed", llOneUpdDatLstLen);
			    ilRC = RC_NOMEM;
			    return ilRC;
			}
			/****** FOR LOGHDL BEGIN ***********/
			/*
			  allocate memory for n rows of actions (commands (INS,UPD,DEL))
			*/
			if (pcgCmdLst == NULL)
			{
			    lgCmdLineCnt = 100;     /* 10 signs for command */
			    pcgCmdLst = (char *) (malloc (lgCmdLineCnt * 10));
			    if (pcgCmdLst != NULL)
			    {
				pcgCmdLst[0] = 0x00;
				lgCmdLstLen = 0;
			    }
			}
			else if (llUpdRecNum >= lgCmdLineCnt - 1)
			{
			    lgCmdLineCnt += 100;
			    pcgCmdLst = (char *) (realloc (pcgCmdLst, lgCmdLineCnt * 10));
			}
			if (pcgCmdLst == NULL)
			{
			    dbg (TRACE,"CEDAArrayWriteDB: Malloc of pcgCmdLst (%ld) failed", lgCmdLineCnt * 10);
			    lgCmdLineCnt = 0;
			    ilRC = RC_NOMEM;
			    return ilRC;
			}

			/* allocate memory for n rows of update data 
			   (allocate for whole record (all fields data)) */
			if (pcgUpdDatLst == NULL)
			{
			    lgUpdDatLstLen = prlTmpArray->RowLen + 4;
			    lgUpdDatLineCnt = 100;
			    pcgUpdDatLst = (char *) (malloc (lgUpdDatLstLen * lgUpdDatLineCnt));
			    if (pcgUpdDatLst != NULL)
			    {
				pcgUpdDatLst[0] = 0x00;
			    }
			    lgUpdDatLstLen = 0;
			}
			else if (prlTmpArray->RowLen >= lgUpdDatLstLen - 1)
			{
			    lgUpdDatLstLen = prlTmpArray->RowLen + 4;
			    pcgUpdDatLst = (char *) (realloc (pcgUpdDatLst, lgUpdDatLstLen * lgUpdDatLineCnt));
			    if (pcgUpdDatLst != NULL)
			    {
				pcgUpdDatLst[0] = 0x00;
				lgUpdDatLstLen = 0;
			    }
			}
			else if (llUpdRecNum >= lgUpdDatLineCnt - 1)
			{
			    lgUpdDatLineCnt += 100;
			    pcgUpdDatLst = (char *) (realloc (pcgUpdDatLst, lgUpdDatLstLen * lgUpdDatLineCnt));
			}
			if (pcgUpdDatLst == NULL)
			{
			    dbg (TRACE, "CEDAArrayWriteDB: Malloc of pcgUpdDatLst (%ld) failed", lgUpdDatLstLen * lgUpdDatLineCnt);
			    lgUpdDatLstLen = 0;
			    lgUpdDatLineCnt = 0;
			    ilRC = RC_NOMEM;
			    return ilRC;
			}

			/* allocate memory for n rows of field lists
			   length of each list is MAXFLDLSTLEN.  */
			if (pcgUpdFldLst == NULL)
			{
			    lgUpdFldLineCnt = 100;
			    pcgUpdFldLst = (char *) (malloc ((ARR_MAXFLDLSTLEN + 4) * lgUpdFldLineCnt));
			    if (pcgUpdFldLst != NULL)
			    {
				pcgUpdFldLst[0] = 0x00;
			    }
			    lgUpdFldLstLen = 0;
			}
			else if (llUpdRecNum >= lgUpdFldLineCnt - 1)
			{
			    lgUpdFldLineCnt += 100;
			    pcgUpdFldLst = (char *) (realloc (pcgUpdFldLst, lgUpdFldLineCnt * (ARR_MAXFLDLSTLEN + 4)));
			}
			if (pcgUpdFldLst == NULL)
			{
			    dbg (TRACE, "CEDAArrayWriteDB: Malloc of pcgUpdFldLst (%ld) failed", lgUpdFldLineCnt * (ARR_MAXFLDLSTLEN + 4));
			    lgUpdFldLineCnt = 0;
			    ilRC = RC_NOMEM;
			    return ilRC;
			}

			/* allocate memory for n rows of Urno 
			   Len = 12 */
			if (pcgUpdSelLst == NULL)
			{
			    lgUpdSelLineCnt = 100;
			    pcgUpdSelLst = (char *) (malloc (12 * lgUpdSelLineCnt));
			    if (pcgUpdSelLst != NULL)
			    {
				pcgUpdSelLst[0] = 0x00;
			    }
			    lgUpdSelLstLen = 0;
			}
			else if (llUpdRecNum >= lgUpdSelLineCnt - 1)
			{
			    lgUpdSelLineCnt += 100;
			    pcgUpdSelLst = (char *) (realloc (pcgUpdSelLst, 12 * lgUpdSelLineCnt));
			}
			if (pcgUpdSelLst == NULL)
			{
			    dbg (TRACE, "CEDAArrayWriteDB: Malloc of pcgUpdSelLst (%ld) failed", lgUpdSelLineCnt * 12);
			    ilRC = RC_NOMEM;
			    return ilRC;
			}

			/****** FOR LOGHDL END ***********/
			/*
			  for row in array check change flag
			  ARR_DATA_NEW => INSERT ROW in DB
			  ARR_DATA_DELETED => DELETE ROW in DB
			  ARR_DATANEW_DELETED => insert and delete => no action
			  else compare in each field DataOrig and DataNew for Update Field.
			*/
			llRowNum = ARR_CURRENT;
			ilRC = FindRow (prlTmpArray, &llRowNum, TRUE);
			if (ilRC == RC_SUCCESS)
			{
			    prlTmpRow = &prlTmpArray->Rows[llRowNum];
			}
			/*********************
										INSERT
			*********************/
			if (prlTmpRow->ChangeFlag == ARR_DATA_NEW)
			{                         /*  INSERT  */
			    /* dbg(TRACE, "InsertRowDB follows"); */
			    ilDBRC = InsertRowDB (prlTmpArray, prlTmpRow, pclTabNam, pclFldLst, &slICursor, lpCommitFlag);
			    ilDBRC = DB_SUCCESS;

			    /*dbg(TRACE,"InsertRowDB");*/
			    if (ilDBRC == DB_SUCCESS)
			    {
				/****** FOR LOGHDL BEGIN ***********/
				llFldNum = -1;
				llFldNum = FindField (prlTmpArray->FieldList, "URNO");
				if ((llFldNum < 0) || (llFldNum >= prlTmpArray->FieldCount))
				{
				    dbg (TRACE, "CEDAArrayWriteDB:INSERT: No URNO in Fieldlist <%s>", prlTmpArray->FieldList);
				}

				else
				{
				    llItemLen = GetDataItem (pclUrno, pcgOneRowData, llFldNum + 1, ',', "\0", "\0\0");
				    if (llItemLen < 0)
				    {
					dbg (TRACE, "CEDAArrayWriteDB: Fkt.: GetDataItem(%d)(Data)", llItemLen);
				    }
				}
				/************   INFORM ACTION AND BCHDL  ************/
				pclRouteIds[0] = 0x00;
                                CheckTableLogging(prlTmpArray);
				if (prlTmpArray->SendToAction == TRUE)
				{
				    sprintf (pclRoute, "%d,", igActionId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (prlTmpArray->SendToBchdl == TRUE)
				{
				    sprintf (pclRoute, "%d,", igBchdlId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (prlTmpArray->SendToLoghdl == TRUE)
				{
				    sprintf (pclRoute, "%d,", igLoghdlId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (pclRouteIds[0] != '\0')
				{
				    pclRouteIds[strlen (pclRouteIds) - 1] = 0x00;
				    dbg (DEBUG, "ArraysReleaseActionInfo follows <%s>", pclRouteIds);
				    ilRC =
					ArraysReleaseActionInfo (pclRouteIds, pclTabNam, "IRT", pclUrno, pclUrno, pclFldLst, pcgOneRowData, " ");
				    /*dbg (DEBUG, "After ArraysReleaseActionInfo");*/
				}

				/************   INFORM ACTION AND BCHDL END  ***********/
				strcpy (pclOneUpdFldLst, pclFldLst);
				lgUpdFldLstLen += 
				    StringPut(&pcgUpdFldLst[lgUpdFldLstLen],pclOneUpdFldLst,"\n");
				strcpy (pclOneUpdDatLst, pcgOneRowData);
				lgUpdDatLstLen += 
				    StringPut(&pcgUpdDatLst[lgUpdDatLstLen],pclOneUpdDatLst,"\n");
				lgCmdLstLen += StringPut(&pcgCmdLst[lgCmdLstLen], "IRT","\n");
				lgUpdSelLstLen += 
				    StringPut(&pcgUpdSelLst[lgUpdSelLstLen],pclUrno,"\n");
				llUpdRecNum++;

				/****** FOR LOGHDL END ***********/
				llInsRows++;
			    }

			    /* sql insert ok */
			    if (lpCommitFlag == ARR_COMMIT_ALL_OK)
			    {
				ilDBRC = DB_SUCCESS;
			    }
			}

			/* insert */
			/*********************
										DELETE
			*********************/
			else if (prlTmpRow->ChangeFlag == ARR_DATA_DELETED)
			{                         /*  DELETE  */
			    /* dbg (TRACE, "DeleteRowDB follows"); */
			    ilDBRC = DeleteRowDB (pspHandle, prlTmpArray, llRowNum, pclTabNam, &slDCursor, lpCommitFlag, pclFldLst);
			    if (ilDBRC == DB_SUCCESS)
			    {

				/****** FOR LOGHDL BEGIN ***********/
				llFldNum = -1;
				llFldNum = FindField (prlTmpArray->FieldList, "URNO");
				if ((llFldNum < 0) || (llFldNum >= prlTmpArray->FieldCount))
				{
				    dbg (TRACE, "CEDAArrayWriteDB:DELETE: No URNO in Fieldlist <%s>", prlTmpArray->FieldList);
				}

				else
				{
				    llItemLen = GetDataItem (pclUrno, pcgOneRowData, llFldNum + 1, ',', "\0", "\0\0");
				    if (llItemLen < 0)
				    {
					dbg (TRACE, "CEDAArrayWriteDB: Fkt.: GetDataItem(%d)(Data)", llItemLen);
				    }
				}

				/************   INFORM ACTION AND BCHDL  ************/
				pclRouteIds[0] = 0x00;
                                CheckTableLogging(prlTmpArray);
				if (prlTmpArray->SendToAction == TRUE)
				{
				    sprintf (pclRoute, "%d,", igActionId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (prlTmpArray->SendToBchdl == TRUE)
				{
				    sprintf (pclRoute, "%d,", igBchdlId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (prlTmpArray->SendToLoghdl == TRUE)
				{
				    sprintf (pclRoute, "%d,", igLoghdlId);
				    strcat (pclRouteIds, pclRoute);
				}
				if (pclRouteIds[0] != '\0')
				{
				    pclRouteIds[strlen (pclRouteIds) - 1] = 0x00;
				    ilRC =
					ArraysReleaseActionInfo (pclRouteIds, pclTabNam, "DRT", pclUrno, pclUrno, pclFldLst, " ", pcgOneRowData);}

				/************   INFORM ACTION AND BCHDL END  ***********/
				strcpy (pclOneUpdFldLst, pclFldLst);
				lgUpdFldLstLen += 
				    StringPut(&pcgUpdFldLst[lgUpdFldLstLen],pclOneUpdFldLst,"\n");
				strcpy (pclOneUpdDatLst, pcgOneRowData);
				lgUpdDatLstLen += 
				    StringPut(&pcgUpdDatLst[lgUpdDatLstLen],pclOneUpdDatLst,"\n");
				lgCmdLstLen += StringPut(&pcgCmdLst[lgCmdLstLen], "DRT","\n");
				lgUpdSelLstLen += 
				    StringPut(&pcgUpdSelLst[lgUpdSelLstLen],pclUrno,"\n");
				llUpdRecNum++;

				/****** FOR LOGHDL END ***********/
				llDelRows++;
			    }

			    /* sql delete ok */
			    if (lpCommitFlag == ARR_COMMIT_ALL_OK)
			    {
				ilDBRC = DB_SUCCESS;
			    }
			}
			/* delete */
			/*********************
										NOOP
			*********************/
			else if (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED)
			{
			    dbg (DEBUG, "ARR_DATANEW_DELETED: nothing done here...");
			}
			/*********************
										UPDATE
			*********************/
			else
			{                         /*  UPDATE  */
			    if (igWholeRowUpd == FALSE)
			    {
				if (slUCursor != 0)
				{
				    close_my_cursor (&slUCursor);
				}
				slUCursor = 0;
				/* dbg (TRACE, "UpdateRowDB follows"); */
				/*************
								ilDBRC =
									UpdateRowDB (prlTmpArray, prlTmpRow, llRowNum, pclTabNam, 
											pclFldLst, &slUCursor, lpCommitFlag, &llUpdCnt,
													pclOneUpdFldLst, pclOneUpdDatLst, pcgCmdLst, pcgUpdFldLst, 
															pcgUpdDatLst, pcgUpdSelLst);
				***************/
				ilDBRC =
				    UpdateRowDB (prlTmpArray, prlTmpRow, llRowNum, pclTabNam, 
						 pclFldLst, &slUCursor, lpCommitFlag, &llUpdCnt,
						 pclOneUpdFldLst, pclOneUpdDatLst);
				llUpdRows += llUpdCnt;
				llUpdRecNum += llUpdCnt;
				if (lpCommitFlag == ARR_COMMIT_ALL_OK)
				{
				    ilDBRC = DB_SUCCESS;
				}
			    }
			    /* if igWholeRowUpd == FALSE */
			    else
			    {
				dbg (TRACE, "UpdateWholeRowDB follows");
				ilDBRC = UpdateWholeRowDB (prlTmpArray, prlTmpRow, pclTabNam, pclFldLst, &slUCursor, lpCommitFlag);
				if (ilDBRC == DB_SUCCESS)
				{
				    llUpdRows++;
				}
			    }
			}                         /* update */
		    }                           /* get fields failed (last Row) */
		}
	    }
  	}	 
	llRow = ARR_NEXT;
    }
    /* end while rows in array 
       (ilRC == RC_SUCCESS) && (ilDBRC == DB_SUCCESS) */
    if (lpCommitFlag == ARR_COMMIT_ALL_OK)
    {
	dbg (DEBUG, "Line %ld: commit_work follows", __LINE__);
	commit_work ();
	dbg (DEBUG, "Line %ld: commit_work (many 1) ...", __LINE__);
    }
    dbg (TRACE, "%ld Rows in Array checked. Written to DB:", llRowCnt);
    dbg (TRACE, "%ld InsertRows, %ld UpdateRows, %ld DeleteRows (Checked %d)", 
                 llInsRows, llUpdRows, llDelRows,lgChkDifCnt);
    if (slICursor != 0)
    {
	close_my_cursor (&slICursor);
    }
    if (slUCursor != 0)
    {
	close_my_cursor (&slUCursor);
    }
    if (slDCursor != 0)
    {
	close_my_cursor (&slDCursor);
    }
    if ((ilDBRC == DB_SUCCESS) && (llRowCnt > 0))
    {
	if (lpCommitFlag == ARR_COMMIT_NONE)
	{
	    rollback ();
	    strcpy (pcgCmdLst, "");
	    lgCmdLstLen = 0;
	    strcpy (pcgUpdSelLst, "");
	    lgUpdSelLstLen = 0;
	    strcpy (pcgUpdFldLst, "");
	    lgUpdFldLstLen = 0;
	    strcpy (pcgUpdDatLst, "");
	    lgUpdDatLstLen = 0;
	    llUpdRecNum = 0;
	}

	else if (lpCommitFlag == ARR_COMMIT_NONE_IF_ERR)
	{
	    dbg (DEBUG, "Line %ld: commit_work follows", __LINE__);
	    commit_work ();
	    dbg (DEBUG, "Line %ld: commit_work (many 2) ...", __LINE__);

	    /* refresh for all rows in array  */
	    dbg (DEBUG, "Line %ld: UnsetRowChanges follows", __LINE__);
	    dbg(TRACE,"start of second loop");
	    for (llRowNum = 0; llRowNum < prlTmpArray->MaxRows; llRowNum++)
	    {
		prlTmpRow = &prlTmpArray->Rows[llRowNum];
		if ((prlTmpRow->ChangeFlag == ARR_DATA_DELETED) || (prlTmpRow->ChangeFlag == ARR_DATANEW_DELETED))
		{
		    ilRC = AATArrayDeleteRow (pspHandle, pcpArrayName, llRowNum);
		    if (ilRC != RC_SUCCESS)
		    {
			dbg (TRACE, "CEDAArrayWriteDB: Refresh Array failed", llRowNum, ilRC);
			ilRC = RC_SUCCESS;
		    }
		}

		else
		{
		    UnsetRowChanges (prlTmpRow, prlTmpArray);
		    /*dbg (DEBUG, "Line %ld: UnsetRowChanges for %ld ...", __LINE__, llRowNum);*/
		}
	    }
	    dbg(TRACE,"second loop finished");
	    dbg (TRACE, "Line %ld: UnsetRowChanges finished", __LINE__);
	}
    }

    else if (llRowCnt > 0)
    {                             /*  DB ERROR APPEARED  */
	if ((lpCommitFlag == ARR_COMMIT_NONE_IF_ERR) || (lpCommitFlag == ARR_COMMIT_NONE))
	{
	    rollback ();
	}
	if ((lpCommitFlag == ARR_COMMIT_NONE) || (lpCommitFlag == ARR_COMMIT_NONE_IF_ERR) ||
	    (lpCommitFlag == ARR_COMMIT_USER))
	{
	    strcpy (pcgCmdLst, "");
	    lgCmdLstLen = 0;
	    strcpy (pcgUpdSelLst, "");
	    lgUpdSelLstLen = 0;
	    strcpy (pcgUpdFldLst, "");
	    lgUpdFldLstLen = 0;
	    strcpy (pcgUpdDatLst, "");
	    lgUpdDatLstLen = 0;
	    llUpdRecNum = 0;
	}
	ilRC = RC_FAIL;
    }

    /* Abbruch by GetFields */
    if ((ilRC == RC_NOTFOUND) && (llRowCnt > 0))
    {
	ilRC = RC_SUCCESS;
    }
    if (ilRC == RC_SUCCESS)
    {

	/* remove last newline  */
	if ((pcgCmdLst != NULL) && ((llItemLen = strlen (pcgCmdLst)) > 0))
	{
	    pcgCmdLst[llItemLen - 1] = 0x00;
	}
	if ((pcgUpdSelLst != NULL) && ((llItemLen = strlen (pcgUpdSelLst)) > 0))
	{
	    pcgUpdSelLst[llItemLen - 1] = 0x00;
	}
	if ((pcgUpdFldLst != NULL) && ((llItemLen = strlen (pcgUpdFldLst)) > 0))
	{
	    pcgUpdFldLst[llItemLen - 1] = 0x00;
	}
	if ((pcgUpdDatLst != NULL) && ((llItemLen = strlen (pcgUpdDatLst)) > 0))
	{
	    pcgUpdDatLst[llItemLen - 1] = 0x00;
	}
        /* content could be NULL but not adress */
	if (pcpCmdLst != NULL)
	{    
	    *pcpCmdLst = pcgCmdLst;
	}
	if (pcpUpdSelLst != NULL)
	{
	    *pcpUpdSelLst = pcgUpdSelLst;
	}
	if (pcpUpdFldLst != NULL)
	{
	    *pcpUpdFldLst = pcgUpdFldLst;
	}
	if (pcpUpdDatLst != NULL)
	{
	    *pcpUpdDatLst = pcgUpdDatLst;
	}
	if (plpNumUrnos != NULL)
	{
	    *plpNumUrnos = llUpdRecNum;
	}
    }
    dbg(DEBUG,"WriteDB finished");
    return ilRC;
}                               /* end of CEDAArrayWriteArray2DB  */

/* ****************************************************************
 * Function:    InsertRowDB
 * Parameter:   IN    : 
 * Description: This function fills database with data of defined row.
 ***************************************************************** */
static int InsertRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, char *pcpTabNam, char *pcpFldLst, short *pspCursor,
                        long lpCommitFlag)
{
    int ilRC = DB_SUCCESS;
    int ilTyp = FOR_INSERT;
    long llItemLen = 0;
    long llNumOfFlds = 0;
    long llNumOfData = 0;
    long llFldNum = -1;
    char pclUrno[15];
    char pclDummy[100];
    /*char pclFldBuf[ARR_MAXSQLFLDLSTLEN + 4];*/  /* for sql fieldlist format */
    char pclSqlBuf[ARR_MAXSQLFLDLSTLEN + 50]; /* for INSERT, DELETE, UPDATE */

    llNumOfFlds = TWECountDelimiter (pcpFldLst, ",");

    llFldNum = FindField (prpArray->FieldList, "URNO");
    if (llFldNum < 0)
    {
	dbg (TRACE, "InsertRowDB: Insert not possible (no URNO in %s)", pcgOneRowData);
	ilRC = RC_FAIL;
    }
    else
    {
	llItemLen = GetDataItem (pclUrno, pcgOneRowData, llFldNum + 1, ',', "\0", "\0\0");
	if (llItemLen <= 0)
	{
	    dbg (TRACE, "InsertRowDB: Fkt.: GetDataItem (URNO) failed (%ld)", llItemLen);
	    ilRC = RC_FAIL;
	}

	else
	{

	    /*  build up field string part of sqlstatement  */
	    strcpy (pclDummy, "");
	    ilTyp = FOR_INSERT;
	    /*TWEBuildSqlFieldStr (pcpFldLst, pclDummy, ilTyp, pclFldBuf);*/
	    if (*pcgInsertFieldBuf == '\0')
	    {
		TWEBuildSqlFieldStr (pcpFldLst, pclDummy, ilTyp, pcgInsertFieldBuf);
	    }

	    if (*pcgInsertSqlBuf == '\0')
	    {
		sprintf (pcgInsertSqlBuf, "INSERT INTO %s %s", pcpTabNam, pcgInsertFieldBuf);
		/*  build the whole sql statement  */
	    }

	    /*sprintf (pclSqlBuf, "INSERT INTO %s %s", pcpTabNam, pcgInsertFieldBuf);*/
	    /*ilRC = ArrRec2DB (pcgOneRowData, pclSqlBuf, pspCursor);*/
	    ilRC = ArrRec2DB (pcgOneRowData, pcgInsertSqlBuf, pspCursor);
	    if (ilRC != DB_SUCCESS)
	    {
		if (lpCommitFlag != ARR_COMMIT_ALL_OK)
		{
		    rollback ();
		}
	    }

	    /* end if */
	    else
	    {
		/* Create Attachment Entry */
		llNumOfData = TWECountDelimiter (pcgOneRowData, ",");
		delton(pcgOneRowData);
                AppStrgToList(4, pcgOneRowData, llNumOfFlds);
		BuildItemBuffer (pcgOneRowData, "", llNumOfData, ",");
                AppStrgToList(7, pclUrno, 1);

		if (lpCommitFlag == ARR_COMMIT_TILL_ERR)
		{
		    commit_work ();
		    /* 20020708: commented 
		       dbg (TRACE, "Line %ld: commit_work ...", __LINE__);
		    */
		}
		if ((lpCommitFlag == ARR_COMMIT_TILL_ERR) || (lpCommitFlag == ARR_COMMIT_ALL_OK))
		{
		    UnsetRowChanges (prpRow, prpArray);
		    /* dbg (DEBUG, "Line %ld: UnsetRowChanges ...", __LINE__);
		     */
		}
	    }
	}
    }
    return ilRC;
}                               /* end of InsertRowDB  */

/* ****************************************************************
 * Function:    DeleteRowDB
 * Parameter:   IN    : 
 * Description: This function deletes record from database by URNO select.
 ***************************************************************** */
static int DeleteRowDB (HANDLE * pspHandle, AATARRAY * prpArray, long lpRowNum, char *pcpTabNam, short *pspCursor,
                        long lpCommitFlag, char *pcpFldLst)
{
    int ilRC = RC_SUCCESS;
    long llItemLen = 0;
    long llNumOfFlds = 0;
    long llNumOfData = 0;
    long llFldNum = -1;
    char pclUrno[15];
    char pclSqlBuf[100];

    /* Delete Row with URNO  */
    llNumOfFlds = TWECountDelimiter (pcpFldLst, ",");
    llFldNum = -1;
    llFldNum = FindField (prpArray->FieldList, "URNO");
    if ((llFldNum < 0) || (llFldNum >= prpArray->FieldCount))
    {
	dbg (TRACE, "DeleteRowDB: Delete row %ld with <%s> not possible (no URNO)", lpRowNum, pcgOneRowData);
	ilRC = RC_FAIL;
    }

    else
    {
	llItemLen = GetDataItem (pclUrno, pcgOneRowData, llFldNum + 1, ',', "\0", "\0\0");
	if (llItemLen < 0)
	{
	    dbg (TRACE, "DeleteRowDB: Fkt.: GetDataItem(%d)(Data)", llItemLen);
	}
	sprintf (pclSqlBuf, "DELETE FROM %s WHERE URNO=:VURNO", pcpTabNam);
	ilRC = ArrRec2DB (pclUrno, pclSqlBuf, pspCursor);
	if (ilRC != DB_SUCCESS)
	{
		if (ilRC == RC_NOTFOUND)
		{
			/*** not found is not a real error, just ignore it ***/
			ilRC = DB_SUCCESS;
		}
		else
		{
	    if (lpCommitFlag != ARR_COMMIT_ALL_OK)
	    {
				rollback ();
	    }
		}
	}
	
	if (ilRC == DB_SUCCESS)
	{
		/* Create Attachment Entry */
		llNumOfData = TWECountDelimiter (pcgOneRowData, ",");
		delton(pcgOneRowData);
                AppStrgToList(9, pcgOneRowData, llNumOfFlds);
		BuildItemBuffer (pcgOneRowData, "", llNumOfData, ",");
                AppStrgToList(6, pclUrno, 1);

	    if (lpCommitFlag == ARR_COMMIT_TILL_ERR)
	    {
		commit_work ();
		/* 20020708: commented 
		   dbg (TRACE, "Line %ld: commit_work (DeleteRowDB)...", __LINE__);
		*/
	    }
	    if ((lpCommitFlag == ARR_COMMIT_TILL_ERR) || (lpCommitFlag == ARR_COMMIT_ALL_OK))
	    {
		ilRC = AATArrayDeleteRow (pspHandle, prpArray->Name, lpRowNum);
		if (ilRC != RC_SUCCESS)
		{
		    dbg (TRACE, "DeleteRowDB: AATArrayDeleteRow (%ld) failed (%d)", lpRowNum, ilRC);
		    dbg (TRACE, "DeleteRowDB: No Refresh of Array");
		}
	    }
	    ilRC = RC_SUCCESS;
	}                           /* ilRC == RC_SUCCESS */
    }                             /* end if URNO exists */
    return ilRC;
}                               /* end of DeleteRowDB */


/* ****************************************************************
 * Function:    UpdateRowDB
 * Parameter:   IN    : 
 * Description: This function fills database with data of defined row.
 *              It updates only the changed fields in record.
 ***************************************************************** */
static int UpdateRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, long lpRowNum, char *pcpTabNam, char *pcpFldLst,short *pspCursor, long lpCommitFlag, long *plpUpdRows, char *pcpOneUpdFldLst,char *pcpOneUpdDatLst)
{
    int ilRC = RC_SUCCESS;
    int ilTyp = 0;
    short slFkt = START;
    short slEnd = FALSE;
    long llFldCnt;
    long llFldNum;
    long llNumOfFlds;
    long llOffset;
    long llItemLen;
    long llUpdCnt;
    long llUrnoTyp;
    char *pclFldStart;
    char *pclDataNew;
    char *pclDataOrig;
    char pclFina[10];
    char pclUrno[15];
    char pclUrnoSel[100];
    char pclDummy[10];
    char pclErrDescr[500];
    char pclRouteIds[100];
    char pclRoute[50];
    char pclTimeStamp[20];
    char pclFldBuf[ARR_MAXSQLFLDLSTLEN + 4]; /* for sql fieldlist format */
    char pclSqlBuf[ARR_MAXSQLFLDLSTLEN + 50]; /* for INSERT, DELETE, UPDATE */

    /* check each field for new data and update with URNO  */
    pcgOneRowData[0] = 0x00;
    *plpUpdRows = 0;
    llUpdCnt = 0;
    if (prpRow->DataNew == NULL && prpRow->DataOrig == NULL)
    {
	dbg (TRACE, "UpdateRowDB: No Data in Row %ld => No update", lpRowNum);
    }

    else if ((prpRow->DataNew == NULL) && (prpRow->DataOrig != NULL))
    {

	/* Just OrigData found => No Update  */
        /*
           dbg(DEBUG, "UpdateRowDB: Just OrigData found");
           dbg(TRACE, "UpdateRowDB: UPDATE: NOTHING TO DO");
        */
    }
    else if ((prpRow->DataNew != NULL) && (prpRow->DataOrig != NULL))
    {

        pclFldStart = pcpFldLst;
        llNumOfFlds = TWECountDelimiter (pcpFldLst, ",");
        lgChkDifCnt++;

	/* check for each field if data is really new */
	/* copy new fields and data to pcpOneUpdFldLst and pcpOneUpdDatLst */
	strcpy (pcpOneUpdFldLst, "");
	strcpy (pcpOneUpdDatLst, "");
	slEnd = FALSE;
	for (llFldCnt = 0; (llFldCnt < llNumOfFlds) && (slEnd == FALSE); llFldCnt++)
	{

	    /* get field name */
	    llItemLen = GetNextDataItem (pclFina, &pclFldStart, ",", "\0", "\0\0");
	    if (llItemLen < 0)
	    {
		dbg (TRACE, "UpdateRowDB: Fkt.: GetNextDataItem(%d)(Fields)", llItemLen);
	    }

	    /* no update on URNO */
	    if ((strcmp (pclFina, "URNO") == 0) || (strcmp (pclFina, "LSTU") == 0))
	    {
		if (llFldCnt < llNumOfFlds - 1)
		{
		    llFldCnt++;
		    llItemLen = GetNextDataItem (pclFina, &pclFldStart, ",", "\0", "\0\0");
		    if (llItemLen < 0)
		    {
			dbg (TRACE, "UpdateRowDB: Fkt.: GetNextDataItem(%d)(Fields)", llItemLen);
		    }
		}

		else
		{
		    slEnd = TRUE;
		}                       /* if URNO,LSTU first time */
		if ((strcmp (pclFina, "URNO") == 0) || (strcmp (pclFina, "LSTU") == 0))
		{
		    if (llFldCnt < llNumOfFlds - 1)
		    {
			llFldCnt++;
			llItemLen = GetNextDataItem (pclFina, &pclFldStart, ",", "\0", "\0\0");
			if (llItemLen < 0)
			{
			    dbg (TRACE, "UpdateRowDB: Fkt.: GetNextDataItem(%d)(Fields)", llItemLen);
			}
		    }

		    else
		    {
			slEnd = TRUE;
		    }
		}

		/* if URNO,LSTU second time */
	    }
	    if (slEnd == FALSE)
	    {

		/* check if really new data? */
		llOffset = prpArray->FieldOffsets[llFldCnt];
		pclDataNew = (char *) &prpRow->DataNew[llOffset];
		llOffset = prpArray->FieldOffsets[llFldCnt];
		pclDataOrig = (char *) &prpRow->DataOrig[llOffset];
		if (strcmp (pclDataOrig, pclDataNew) == 0)
		{

		    /* no update */
		}

		else
		{

		    /* 
		       DB Update !!!
		       Copy field to update field list, 
		       data to update data list and
		       selection to selection list.  
		    */
		    strcat (pcgOneRowData, pclDataOrig);
		    strcat (pcgOneRowData, ",");
		    sprintf (pclDummy, "%s,", pclFina);
		    strcat (pcpOneUpdFldLst, pclDummy);
		    sprintf (pcgOneFldData, "%s,", pclDataNew);
		    strcat (pcpOneUpdDatLst, pcgOneFldData);
		}
	    }
	}                           /* end for all fields  */

	/* sql_if if update field list exists  */
	if (strcmp (pcpOneUpdFldLst, "") != 0)
	{

	    /* Add LSTU if nescessary */
	    llFldNum = FindField (prpArray->FieldList, "LSTU");
	    if (llFldNum >= 0)
	    {
		strcat (pcpOneUpdFldLst, "LSTU,");
		llOffset = prpArray->FieldOffsets[llFldCnt];
		pclDataNew = (char *) &prpRow->DataNew[llOffset];
		strcpy (pclTimeStamp, pclDataNew);
		tool_filter_spaces (pclTimeStamp);
		if ((long) (strlen (pclTimeStamp)) < 14)
		{
		    GetServerTimeStamp ("UTC", 1, 0, pclTimeStamp);
		}
		strcat (pcpOneUpdDatLst, pclTimeStamp);
		strcat (pcpOneUpdDatLst, ",");
		strcat (pcgOneRowData, (char *) &prpRow->DataOrig[llFldCnt]);
		strcat (pcgOneRowData, ",");
	    }

	    /* remove last komma in lists */
	    pcpOneUpdFldLst[strlen (pcpOneUpdFldLst) - 1] = 0x00;
	    pcpOneUpdDatLst[strlen (pcpOneUpdDatLst) - 1] = 0x00;
	    pcgOneRowData[strlen (pcgOneRowData) - 1] = 0x00;
	    llFldNum = -1;
	    llFldNum = FindField (prpArray->FieldList, "URNO");
	    if (llFldNum < 0)
	    {
		dbg (TRACE, "UpdateRowDB: Update row %ld with <%s> not possible", lpRowNum, pcpOneUpdDatLst);
		dbg (TRACE, "UpdateRowDB: No URNO found");
	    }

	    else
	    {
		llUrnoTyp = prpArray->FieldTyps[llFldNum];
		llOffset = prpArray->FieldOffsets[llFldNum];
		pclDataOrig = (char *) &prpRow->DataOrig[llOffset];
		strcpy (pclUrno, pclDataOrig);
		if (llUrnoTyp == ARR_NUM_TYPE)
		{
		    sprintf (pclUrnoSel, "WHERE URNO = %s", pclUrno);
		}

		else
		{
		    sprintf (pclUrnoSel, "WHERE URNO = '%s'", pclUrno);
		}

		/*  build up field string part of sqlstatement  */
		strcpy (pclDummy, "");
		ilTyp = FOR_UPDATE;
		ilRC = TWEBuildSqlFieldStr (pcpOneUpdFldLst, pclDummy, ilTyp, pclFldBuf);

		/*  build the whole sql statement  */
		sprintf (pclSqlBuf, "UPDATE %s SET %s %s", pcpTabNam, pclFldBuf, pclUrnoSel);
		/*dbg (DEBUG, "UpdateRowDB: pclSqlBuf <%s>", pclSqlBuf);*/

		/*  change kommas to NULLs  */
		/*  ACHTUNG: With Blancs up to lgOneRowDataLen possible */
/************   INFORM ACTION AND BCHDL  ************/
		pclRouteIds[0] = 0x00;
                CheckTableLogging(prpArray);
		if (prpArray->SendToAction == TRUE)
		{
		    sprintf (pclRoute, "%d,", igActionId);
		    strcat (pclRouteIds, pclRoute);
		}
		/*dbg (DEBUG, "SendToBchdl = %d", prpArray->SendToBchdl);*/
		if (prpArray->SendToBchdl == TRUE)
		{
		    sprintf (pclRoute, "%d,", igBchdlId);
		    strcat (pclRouteIds, pclRoute);
		}
		if (prpArray->SendToLoghdl == TRUE)
		{
		    sprintf (pclRoute, "%d,", igLoghdlId);
		    strcat (pclRouteIds, pclRoute);
		}
		dbg (DEBUG, "pclRouteIds <%s>", pclRouteIds);
		if (pclRouteIds[0] != '\0')
		{
		    pclRouteIds[strlen (pclRouteIds) - 1] = 0x00;

/* no update on URNO but must be send */
		    if (strstr (pcpOneUpdFldLst, "URNO") == NULL)
		    {
			strcat (pcpOneUpdFldLst, ",");
			strcat (pcpOneUpdFldLst, "URNO");
			strcat (pcpOneUpdDatLst, ",");
			strcat (pcpOneUpdDatLst, pclUrno);
			strcat (pcgOneRowData, ",");
			strcat (pcgOneRowData, pclUrno);
		    }
		    /* 20020724: JIM: moved ArraysReleaseActionInfo behind commit_work() */
		}

/************   INFORM ACTION AND BCHDL END  ***********/
		strcpy (pcgOneRowData, pcpOneUpdDatLst);
		TWEDel2NewDel (pcgOneRowData, ',', '\0');

		/* sql_if  */
		slFkt = START;
		ilRC = sql_if (slFkt, pspCursor, pclSqlBuf, pcgOneRowData);
		if (ilRC != DB_SUCCESS)
		{
		    dbg (TRACE, "UpdateRowDB: sql_if failed (%d)", ilRC);
		    dbg (TRACE, "UpdateRowDB: pclSqlBuf <%s>", pclSqlBuf);
		    get_ora_err (ilRC, pclErrDescr);
		    dbg (TRACE, "UpdateRowDB: ORA-ERR:<%s>", pclErrDescr);
		    if (lpCommitFlag != ARR_COMMIT_ALL_OK)
		    {
			rollback ();
		    }
		}

		/* end if */
		else
		{

		    /* Create Attachment Entry */
                    AppStrgToList(5, prpRow->DataNew, llNumOfFlds);
                    AppStrgToList(8, pclUrno, 1);

		    if ((lpCommitFlag == ARR_COMMIT_TILL_ERR) || (lpCommitFlag == ARR_COMMIT_ALL_OK))
		    {
			commit_work ();

			/* BST 20.07.2004: Sending all fields and data */

			BuildItemBuffer (prpRow->DataNew, "", llNumOfFlds, ",");
			BuildItemBuffer (prpRow->DataOrig, "", llNumOfFlds, ",");

			(void) ArraysReleaseActionInfo (pclRouteIds, pcpTabNam, "URT", pclUrno, pclUrno, 
							pcpFldLst, prpRow->DataNew, prpRow->DataOrig);
			delton(prpRow->DataNew);
			delton(prpRow->DataOrig);

			/*
			(void) ArraysReleaseActionInfo (pclRouteIds, pcpTabNam, "URT", pclUrno, pclUrno, 
							pcpOneUpdFldLst, pcpOneUpdDatLst, pcgOneRowData);
			*/
			/* dbg (DEBUG, "Line %ld: commit_work for %ld ...", __LINE__, lpRowNum);
			 */
			UnsetRowChanges (prpRow, prpArray);
			/* dbg (DEBUG, "Line %ld: UnsetRowChanges for %ld ...", __LINE__, lpRowNum);
			 */
		    }

		    /****** FOR LOGHDL/BCHDL/ACTION BEGIN ***********/

		    lgUpdFldLstLen += 
			StringPut(&pcgUpdFldLst[lgUpdFldLstLen],pcpOneUpdFldLst,"\n");
		    lgUpdDatLstLen += 
			StringPut(&pcgUpdDatLst[lgUpdDatLstLen],pcpOneUpdDatLst,"\n");
		    lgCmdLstLen += StringPut(&pcgCmdLst[lgCmdLstLen], "URT","\n");
		    lgUpdSelLstLen += 
			StringPut(&pcgUpdSelLst[lgUpdSelLstLen],pclUrno,"\n");
		    llUpdCnt++;

		    /****** FOR LOGHDL END ***********/
		}                       /* end if DB_SUCCESS */
	    }                         /* end if Urno exists */
	}

	/* end if UpdFldLst exists */
	else
	{
	    if ((lpCommitFlag == ARR_COMMIT_TILL_ERR) || (lpCommitFlag == ARR_COMMIT_ALL_OK))
	    {                         /* +++ jim +++ 20010115: unset must be done even if update was 
					 done with same data
				      */
		/* dbg (DEBUG, "UpdateRowDB: Nothing to write to DB");
		 */
		UnsetRowChanges (prpRow, prpArray);
		/* dbg (DEBUG, "Line %ld: UnsetRowChanges for %ld ...", __LINE__, lpRowNum);
		 */
	    }
	}
    }

    /* end if DataNew and DataOrig exists */
    *plpUpdRows = llUpdCnt;
    return ilRC;
}                               /* end of UpdateRowDB */

/* ****************************************************************
 * Function:    UpdateWholeRowDB
 * Parameter:   IN    : 
 * Description: This function fills database with data of defined row.
 *              It updates the whole record of table (without URNO)
 *              and leaves the Cursor opened.
 ***************************************************************** */
static int UpdateWholeRowDB (AATARRAY * prpArray, AATARRAYROW * prpRow, char *pcpTabNam, char *pcpFldLst,
                             short *pspCursor, long lpCommitFlag)
{
    int ilRC = DB_SUCCESS;
    int ilTyp = FOR_UPDATE;
    long llItemLen = 0;
    long llFldNum = -1;
    long llUrno;
    long llLen;
    long llUrnoTyp;
    char pclUrno[15];
    char pclDummy[100];
    char pclUrnoSel[100];
    char pclFldBuf[ARR_MAXSQLFLDLSTLEN + 4]; /* for sql fieldlist format */
    char pclSqlBuf[ARR_MAXSQLFLDLSTLEN + 50]; /* for INSERT, DELETE, UPDATE */
    char *pclPtr;
    char *pclPtr2;
    llFldNum = FindField (prpArray->FieldList, "URNO");
    if (llFldNum < 0)
    {
	dbg (TRACE, "UpdateWholeRowDB: Update not possible (no URNO in %s)", pcgOneRowData);
	ilRC = RC_FAIL;
    }

    else
    {
	llUrnoTyp = prpArray->FieldTyps[llFldNum];
	llItemLen = GetDataItem (pclUrno, pcgOneRowData, llFldNum + 1, ',', "\0", "\0\0");
	if (llItemLen <= 0)
	{
	    dbg (TRACE, "UpdateWholeRowDB: Fkt.: GetDataItem (URNO) failed (%ld)", llItemLen);
	    ilRC = RC_FAIL;
	}

	else
	{

	    /* Delete Urno from Data */
	    pclPtr = strstr (pcgOneRowData, pclUrno);
	    llLen = strcspn (pclPtr, ",");
	    pclPtr2 = pclPtr;
	    pclPtr2 += llLen + 1;
	    llLen = strcspn (pclPtr2, "\0");
	    memmove (pclPtr, pclPtr2, llLen + 1);
	    llUrno = atol (pclUrno);

	    /*  build up field string part of sqlstatement  */
	    strcpy (pclDummy, "");
	    ilTyp = FOR_UPDATE;

	    /* URNO not in UpdList */
	    TWEBuildSqlFieldStr (pcpFldLst, pclDummy, ilTyp, pclFldBuf);

	    /*  build the whole sql statement  */
	    if (llUrnoTyp == ARR_NUM_TYPE)
	    {
		sprintf (pclUrnoSel, "WHERE URNO = %ld", llUrno);
	    }

	    else
	    {
		sprintf (pclUrnoSel, "WHERE URNO = '%ld'", llUrno);
	    }
	    sprintf (pclSqlBuf, "UPDATE %s SET %s %s", pcpTabNam, pclFldBuf, pclUrnoSel);
	    ilRC = ArrRec2DB (pcgOneRowData, pclSqlBuf, pspCursor);
	    if (ilRC != DB_SUCCESS)
	    {
		if (lpCommitFlag != ARR_COMMIT_ALL_OK)
		{
		    rollback ();
		}
	    }

	    /* end if */
	    else
	    {
		if (lpCommitFlag == ARR_COMMIT_TILL_ERR)
		{
		    commit_work ();
		    /* dbg (DEBUG, "Line %ld: commit_work (UpdateWholeRowDB) ...", __LINE__);
		     */
		}
		if ((lpCommitFlag == ARR_COMMIT_TILL_ERR) || (lpCommitFlag == ARR_COMMIT_ALL_OK))
		{
		    UnsetRowChanges (prpRow, prpArray);
		    /* dbg (DEBUG, "Line %ld: UnsetRowChanges (UpdateWholeRowDB) ...", __LINE__);
		     */
		}
	    }
	}
    }
    return ilRC;
}                               /* end of UpdateWholeRowDB  */
static int ArrRec2DB (char *pcpData, char *pcpSqlStr, short *pspCursor)
{
    int ilDBRC = DB_SUCCESS;
    short slFkt = START;
    char pclErrDescr[500] = "";
    static long llDataLen = 0;
    static char *pclData = NULL;
    if (llDataLen <= lgOneRowDataLen)
    {
	llDataLen = lgOneRowDataLen + 10;
	if (pclData == NULL)
	{
	    pclData = (char *) (malloc (llDataLen));
	}

	else
	{
	    pclData = (char *) (realloc (pclData, llDataLen));
	}
    }
    strcpy (pclData, pcpData);

    /*  change kommas to NULLs  */
    TWEDel2NewDel (pclData, ',', '\0');
    ilDBRC = sql_if (slFkt, pspCursor, pcpSqlStr, pclData);
    if (ilDBRC != DB_SUCCESS)
    {
	dbg (TRACE, "ArrRec2DB: sql_if failed (%d)", ilDBRC);
	dbg (TRACE, "ArrRec2DB: pcpSqlStr <%s>", pcpSqlStr);
	get_ora_err (ilDBRC, pclErrDescr);
	dbg (TRACE, "ArrRec2DB: ORA-ERR:<%s>", pclErrDescr);
        dbg(TRACE,"PASSED DATA <%s>",pcpData);
        dbg(TRACE,"GLOBAL DATA <%s>",pcgOneRowData);
    }
    return ilDBRC;
}

/* ****************************************************************
 * Function:    SwitchToWholeRowUpdate
 * Parameter:   IN    : bpSwitch    TRUE => Update on all fields called
 *                      bpSwitch    FALSE=> Update on changed fields called
 * Description: This function fills database with data of defined row.
 *              In some cases the update on all fields in table is faster 
 *              than checking wether field has changed and updating only
 *              a part of table fields. The Updatecursor could be left open
 *              when update is always on whole row.
 ***************************************************************** */
void SwitchToWholeRowUpdate (int bpSwitch)
{
    if (bpSwitch)
    {
	igWholeRowUpd = TRUE;
    }

    else
    {
	igWholeRowUpd = FALSE;
    }
}                               /* SwitchToWholeRowUpdate */
int ParseFilterStr (HANDLE * pspHandle, char *pcpArrayName, char *pcpFldLst, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    int ilKlamAuf;
    char *pclPtr1;
    char pclFilterStr[ARR_MAXFILTERLEN + 1];
    AATARRAY *prlTmpArray;
    ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
    if (ilRC != RC_SUCCESS)
    {
	dbg (DEBUG, "ParseFilterStr: Array <%s> not found", pcpArrayName);
    }
    if ((ilRC == RC_SUCCESS) && (prlTmpArray->Filter == TRUE))
    {
	ilKlamAuf = 0;
	dbg (DEBUG, "Filter for array <%s> <%s>", pcpArrayName, prlTmpArray->FilterStr);
	strcpy (pclFilterStr, prlTmpArray->FilterStr);
	pclPtr1 = pclFilterStr;
	while (*pclPtr1 != '\0')
	{
	    if (*pclPtr1 == '(')
	    {
		ilKlamAuf++;
	    }
	    pclPtr1++;
	}
	dbg (DEBUG, "ParseFilter: KlammAuf %d", ilKlamAuf);
	if (ilKlamAuf <= 1)
	{                           /* special case */
	    ilRC = ParseOneFilter (pclFilterStr, pcpFldLst, pcpData);
	}

	/* end of ilKlamAuf <= 1 */
	else
	{
	    ilRC = ParseMultiFilter (pclFilterStr, pcpFldLst, pcpData);
	}
    }

    /* end of RC_SUCCESS */
    return ilRC;
}                               /* end of ParseFilterStr */

int ParseOneFilter (char *pcpFilterStr, char *pcpFldLst, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    int ilOp;
    int ilLen;
    char pclFkt[] = "ParseOneFilter";
    char pclFilterStr[ARR_MAXFILTERLEN + 1];
    char pclFina[8];
    char pclVal[100];
    char pclOp[8];
    char *pclPtr1;

/* FOR TEST ONLY */
    if (pcgOneFldData == NULL)
    {
	pcgOneFldData = (char *) (malloc (10000));
    }
    strcpy (pclFilterStr, pcpFilterStr);
    dbg (DEBUG, "ParseOne: Filter <%s>", pclFilterStr);
    str_trm_all (pclFilterStr, "(", TRUE);
    str_trm_all (pclFilterStr, ")", TRUE);
    str_trm_all (pclFilterStr, " ", TRUE);
    dbg (DEBUG, "ParseOne: Filter after trim <%s>", pclFilterStr);
    pclPtr1 = pclFilterStr;
    ilLen = strcspn (pclPtr1, "=><&|!");
    memcpy (pclFina, pclPtr1, ilLen);
    pclFina[ilLen] = 0x00;
    tool_filter_spaces (pclFina);
    pclPtr1 += ilLen;
    ilLen = strspn (pclPtr1, "=><&|!");
    memcpy (pclOp, pclPtr1, ilLen);
    pclOp[ilLen] = 0x00;
    pclPtr1 += ilLen;
    ilLen = 0;

    do
    {
	pclPtr1++;
	pclVal[ilLen] = *pclPtr1;
	ilLen++;
    }
    while (*pclPtr1 != '\0');
    tool_filter_spaces (pclVal);
    if (pclVal[0] == '\0')
    {
	strcpy (pclVal, " ");
    }
    ilRC = TWEExtractField (pclFina, pcpFldLst, pcpData, ",", pcgOneFldData);
    if (ilRC == RC_SUCCESS)
    {
	tool_filter_spaces (pcgOneFldData);
    }

    else
    {
	strcpy (pcgOneFldData, pclFina);
	tool_filter_spaces (pcgOneFldData);
	ilRC = RC_SUCCESS;
    }
    if (pcgOneFldData[0] == '\0')
    {
	strcpy (pcgOneFldData, " ");
    }
    dbg (DEBUG, "pclOp <%s>", pclOp);
    SetOperand (pclOp, &ilOp);
    dbg (DEBUG, "ilOp %d Values <%s> <%s>", ilOp, pcgOneFldData, pclVal);
    switch (ilOp)
    {
	case ARR_EQUAL:
	    if (!strcmp (pcgOneFldData, pclVal))
	    {
	    }

	    else
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_NOTEQUAL:
	    if (strcmp (pcgOneFldData, pclVal) != 0)
	    {
	    }

	    else
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_SMALLER:
	    if (strcmp (pcgOneFldData, pclVal) >= 0)
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_SMALLEREQUAL:
	    if (strcmp (pcgOneFldData, pclVal) > 0)
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_LARGER:
	    if (strcmp (pcgOneFldData, pclVal) <= 0)
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_LARGEREQUAL:
	    if (strcmp (pcgOneFldData, pclVal) < 0)
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_AND:
	    if ((strcmp (pcgOneFldData, "0") == 0) && (strcmp (pclVal, "0") == 0))
	    {
	    }

	    else
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	case ARR_OR:
	    if ((strcmp (pcgOneFldData, "0") == 0) || (strcmp (pclVal, "0") == 0))
	    {
	    }

	    else
	    {
		ilRC = RC_OP_FALSE;
	    }
	    break;
	default:
	    dbg (TRACE, "%s: Filter <%s> is not valid", pclFkt, pcpFilterStr);
	    ilRC = RC_FAIL;
    }                             /* end switch */
    return ilRC;
}                               /* end of ParseOneFilter */

int ParseMultiFilter (char *pcpFilterStr, char *pcpFldLst, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    int ilKlamAuf;
    int ilKlamZu;
    int ilCnt;
    int ilCnt2;
    int ilLen;
    int ilPart;
    int ilMainFilterCnt;
    int ilSub;
    int ilOp;
    short slEnd;
    char pclFkt[] = "ParseMultiFilter";
    char pclOp[10];
    char pclSubFilter[ARR_MAXFILTERLEN + 1];
    char pclMainFilter[ARR_MAXFILTERLEN + 1];
    char pclDummy[ARR_MAXFILTERLEN + 1];
    char *pclPtr1;
    char *pclPtr2;
    MAINFILTER rlMainFilter[10];

    /* First get all main parts in FilterStr */
    ilKlamZu = -10;
    ilKlamAuf = 0;
    ilCnt = 0;
    ilMainFilterCnt = 0;
    slEnd = FALSE;
    pclPtr1 = pcpFilterStr;
    dbg (DEBUG, "%s: Filter <%s>", pclFkt, pcpFilterStr);
    while ((*pclPtr1 != '\0') && (slEnd == FALSE))
    {
	if (*pclPtr1 == '(')
	{
	    ilKlamAuf++;
	}

	else if (*pclPtr1 == ')')
	{
	    if (ilKlamZu == -10)
	    {
		ilKlamZu = 0;
	    }
	    ilKlamZu++;
	}
	rlMainFilter[ilMainFilterCnt].rcMainFilterStr[ilCnt] = *pclPtr1;
	ilCnt++;
	if (ilKlamZu == ilKlamAuf - 1)
	{
	    rlMainFilter[ilMainFilterCnt].rcMainFilterStr[ilCnt] = 0x00;
	    dbg (DEBUG, "%s: %d.MainFilter <%s>", pclFkt, ilMainFilterCnt, rlMainFilter[ilMainFilterCnt].rcMainFilterStr);

	    /* get operand between main Parts */
	    if (ilMainFilterCnt > 0)
	    {
		pclPtr2 = rlMainFilter[ilMainFilterCnt].rcMainFilterStr;
		ilLen = strspn (pclPtr2, " =><&|");
		memcpy (pclOp, pclPtr2, ilLen);
		pclOp[ilLen] = 0x00;
		tool_filter_spaces (pclOp);
		SetOperand (pclOp, &ilOp);
		rlMainFilter[ilMainFilterCnt - 1].riMainOperand = ilOp;
		pclPtr2 = rlMainFilter[ilMainFilterCnt].rcMainFilterStr;
		for (ilCnt = 0; ilCnt < ilLen; ilCnt++)
		{
		    *pclPtr2 = ' ';
		    pclPtr2++;
		}
		str_trm_lft (rlMainFilter[ilMainFilterCnt].rcMainFilterStr, " ");
	    }
	    ilMainFilterCnt++;
	    ilCnt = 0;
	    ilKlamAuf = 1;
	    ilKlamZu = -10;
	}

	else if (ilKlamZu == ilKlamAuf)
	{
	    slEnd = TRUE;
	}
	pclPtr1++;
    }                             /* end while */
    dbg (DEBUG, "%s: Number of Main Filters %d", pclFkt, ilMainFilterCnt);

    /* remove first DoppelKlammer im ersten Mainfilter */
    str_trm_lft (rlMainFilter[0].rcMainFilterStr, " ");
    rlMainFilter[0].rcMainFilterStr[0] = ' ';
    str_trm_lft (rlMainFilter[0].rcMainFilterStr, " ");
    for (ilPart = 0; ilPart < ilMainFilterCnt; ilPart++)
    {
		ilKlamZu = -10;
		ilKlamAuf = 0;
		ilCnt = 0;
		ilSub = 0;
		slEnd = FALSE;
		pclPtr1 = rlMainFilter[ilPart].rcMainFilterStr;
		dbg (DEBUG, "%s: %d. MainFilter <%s>", pclFkt, ilPart, pclPtr1);
		while ((*pclPtr1 != '\0') && (slEnd == FALSE))
		{
			if (*pclPtr1 == '(')
			{
				ilKlamAuf++;
			}
			else if (*pclPtr1 == ')')
			{
				if (ilKlamZu == -10)
				{
					ilKlamZu = 0;
				}
				ilKlamZu++;
			}
			else
			{
				rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr[ilCnt] = *pclPtr1;
				ilCnt++;
			}
			if (ilKlamZu == ilKlamAuf - 1)
			{
				rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr[ilCnt] = 0x00;
				dbg (DEBUG, "%s: Subfilterstr <%s>", pclFkt, rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr);

				/* get operand between main Parts */
				if (ilSub > 0)
				{
					pclPtr2 = rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr;
					ilLen = strspn (pclPtr2, " =><&|");
					memcpy (pclOp, pclPtr2, ilLen);
					pclOp[ilLen] = 0x00;
					tool_filter_spaces (pclOp);
					SetOperand (pclOp, &ilOp);
					rlMainFilter[ilPart].rrSubFilter[ilSub - 1].riSubOperand = ilOp;
					pclPtr2 = rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr;
					for (ilCnt = 0; ilCnt < ilLen; ilCnt++)
					{
					*pclPtr2 = ' ';
					pclPtr2++;
					}
					str_trm_lft (rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr, " ");
				}
				ilCnt = 0;
				ilSub++;
				ilKlamAuf = 1;
				ilKlamZu = -10;
			}

			else if (ilKlamZu == ilKlamAuf)
			{
				slEnd = TRUE;
				if (ilSub == 0)
				{
					rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr[ilCnt] = 0x00;
					dbg (DEBUG, "%s: Subfilterstr <%s>", pclFkt, rlMainFilter[ilPart].rrSubFilter[ilSub].rcSubFilterStr);
					ilSub = 1;
				}
			}
			pclPtr1++;
		}                           /* end while */
		rlMainFilter[ilPart].riNumSubFilter = ilSub;
    }                             /* end of for */
    strcpy (pclMainFilter, "");
    for (ilCnt = 0; ilCnt < ilMainFilterCnt; ilCnt++)
    {
	strcpy (pclSubFilter, "");
	dbg (DEBUG, "%s: TAKE %d. MAINFILTER", pclFkt, ilCnt);
	dbg (DEBUG, "%s: MAIN HAS %d SUBFILTERS", pclFkt, rlMainFilter[ilCnt].riNumSubFilter);
	for (ilCnt2 = 0; ilCnt2 < rlMainFilter[ilCnt].riNumSubFilter; ilCnt2++)
	{
	    ilRC = ParseOneFilter (rlMainFilter[ilCnt].rrSubFilter[ilCnt2].rcSubFilterStr, pcpFldLst, pcpData);
	    dbg (DEBUG, "%s: SUBFILTER RESULT FROM <%s> IS %d", pclFkt,
		 rlMainFilter[ilCnt].rrSubFilter[ilCnt2].rcSubFilterStr, ilRC);
	    rlMainFilter[ilCnt].rrSubFilter[ilCnt2].riResult = ilRC;
	    GetOperand (rlMainFilter[ilCnt].rrSubFilter[ilCnt2].riSubOperand, pclOp);
	    sprintf (pclDummy, "%d %s ", rlMainFilter[ilCnt].rrSubFilter[ilCnt2].riResult, pclOp);
	    strcat (pclSubFilter, pclDummy);
	    dbg (DEBUG, "%s: pclSubFilter <%s>", pclFkt, pclSubFilter);
	}

/* REALISIERT BIS TIEFE 2 */
	if (rlMainFilter[ilCnt].riNumSubFilter > 1)
	{                           /* == 2 */
	    ilRC = ParseOneFilter (pclSubFilter, pcpFldLst, pcpData);
	}
	dbg (DEBUG, "%s: MAINFILTER RESULT IS %d", pclFkt, ilRC);
	rlMainFilter[ilCnt].riResult = ilRC;
	GetOperand (rlMainFilter[ilCnt].riMainOperand, pclOp);
	if ((ilCnt + 1 >= 2) && (ilCnt + 1 < ilMainFilterCnt))
	{
	    sprintf (pclDummy, "%d", rlMainFilter[ilCnt].riResult);
	    strcat (pclMainFilter, pclDummy);
	    ilRC = ParseOneFilter (pclMainFilter, pcpFldLst, pcpData);
	    dbg (DEBUG, "%s: PART RESULT OF <%s> IS %d", pclFkt, pclMainFilter, ilRC);
	    sprintf (pclDummy, "%d %s ", ilRC, pclOp);
	    strcpy (pclMainFilter, pclDummy);
	    dbg (DEBUG, "%s: pclMainFilter <%s>", pclFkt, pclMainFilter);
	}

	else if (ilCnt + 1 == 1)
	{
	    sprintf (pclDummy, "%d %s ", rlMainFilter[ilCnt].riResult, pclOp);
	    strcpy (pclMainFilter, pclDummy);
	    dbg (DEBUG, "%s: pclMainFilter <%s>", pclFkt, pclMainFilter);
	}

	else
	{
	    sprintf (pclDummy, "%d", rlMainFilter[ilCnt].riResult);
	    strcat (pclMainFilter, pclDummy);
	    dbg (DEBUG, "%s: pclMainFilter <%s>", pclFkt, pclMainFilter);
	    ilRC = ParseOneFilter (pclMainFilter, pcpFldLst, pcpData);
	    dbg (DEBUG, "%s: ABSOLUT RESULT OF MAINFILTER IS %d", pclFkt, ilRC);
	}
    }

/*
  ilRC = ParseOneFilter(pclMainFilter, pcpFldLst, pcpData);
  dbg(DEBUG,"ABSOLUT RESULT FROM <%s> IS %d", pclMainFilter, ilRC);
*/
    return ilRC;
}                               /* end of ParseMultiFilter */

void SetOperand (char *pcpOperand, int *pipOperand)
{
    if (!strcmp (pcpOperand, "=="))
    {
	*pipOperand = ARR_EQUAL;
    }

    else if (!strcmp (pcpOperand, "!="))
    {
	*pipOperand = ARR_NOTEQUAL;
    }

    else if (!strcmp (pcpOperand, "<"))
    {
	*pipOperand = ARR_SMALLER;
    }

    else if (!strcmp (pcpOperand, "<="))
    {
	*pipOperand = ARR_SMALLEREQUAL;
    }

    else if (!strcmp (pcpOperand, ">"))
    {
	*pipOperand = ARR_LARGER;
    }

    else if (!strcmp (pcpOperand, ">="))
    {
	*pipOperand = ARR_LARGEREQUAL;
    }

    else if (!strcmp (pcpOperand, "||"))
    {
	*pipOperand = ARR_OR;
    }

    else if (!strcmp (pcpOperand, "&&"))
    {
	*pipOperand = ARR_AND;
    }

    else
    {
	*pipOperand = ARR_NOOP;
    }
}                               /* end of SetOperand */
void GetOperand (int ipOperand, char *pcpOperand)
{
    if (ipOperand == ARR_EQUAL)
    {
	strcpy (pcpOperand, "==");
    }

    else if (ipOperand == ARR_NOTEQUAL)
    {
	strcpy (pcpOperand, "!=");
    }

    else if (ipOperand == ARR_SMALLER)
    {
	strcpy (pcpOperand, "<");
    }

    else if (ipOperand == ARR_SMALLEREQUAL)
    {
	strcpy (pcpOperand, "<=");
    }

    else if (ipOperand == ARR_LARGER)
    {
	strcpy (pcpOperand, ">");
    }

    else if (ipOperand == ARR_LARGEREQUAL)
    {
	strcpy (pcpOperand, ">=");
    }

    else if (ipOperand == ARR_OR)
    {
	strcpy (pcpOperand, "||");
    }

    else if (ipOperand == ARR_AND)
    {
	strcpy (pcpOperand, "&&");
    }

    else
    {
	strcpy (pcpOperand, "");
    }
}                               /* end of SetOperand */

/********************************************************/
static int ArraysReleaseActionInfo (char *pcpRoute, char *pcpTbl, char *pcpCmd, char *pcpUrnoList, char *pcpSel,
                                    char *pcpFld, char *pcpDat, char *pcpOldDat)
{
    int ilRC = RC_SUCCESS;
    int ilSelLen = 0;
    int ilSelPos = 0;
    int ilDatLen = 0;
    int ilDatPos = 0;
    int ilRouteItm = 0;
    int ilItmCnt = 0;
    int ilActRoute = 0;
    char pclRouteNbr[8];
    char *pclOutSel = NULL;
    char *pclOutDat = NULL;
    char clTmpTwEnd[124]; /* 20021203 JIM: set TwEnd to "Hopo,tab,mod_name"*/


/* 20021203 JIM: as long as MULTI HOPO is incomplete we will release HOPO 
   from systab: */
    if (cgAntiMultyHopo[0] == 0)
    {
	ilRC = tool_search_exco_data ("SYS", "HOMEAP", cgAntiMultyHopo);
    }
    sprintf(clTmpTwEnd,"%s,%s,%s",cgAntiMultyHopo,&pcpTbl[3],mod_name);

  dbg(DEBUG,"ACTION TBL <%s> ROUTE <%s>",pcpTbl, pcpRoute);
    /**********************
  dbg(TRACE,"ACTION CMD <%s>",pcpCmd);
  dbg(TRACE,"ACTION SEL <%s>",pcpSel);
  dbg(TRACE,"ACTION URN <%s>",pcpUrnoList);
  dbg(TRACE,"ACTION FLD/DAT/OLD\n<%s>\n<%s>\n<%s>",pcpFld,pcpDat,pcpOldDat);
    **************************/
    ilSelLen = strlen (pcpUrnoList) + strlen (pcpSel) + 32;
    pclOutSel = (char *) malloc (ilSelLen);
    ilDatLen = strlen (pcpDat) + strlen (pcpOldDat) + 32;
    pclOutDat = (char *) malloc (ilDatLen);
    if ((pclOutSel != NULL) && (pclOutDat != NULL))
    {

	/* StrgPutStrg(pclOutSel,&ilSelPos,pcpSel,0,-1," \n");
	   avoid duplicate UrnoList MCU 29.10.2000 **** */
	StrgPutStrg (pclOutSel, &ilSelPos, pcpUrnoList, 0, -1, "\n");
	if (ilSelPos > 0)
	{
	    ilSelPos--;
	}

	/* end if */
	pclOutSel[ilSelPos] = 0x00;
	StrgPutStrg (pclOutDat, &ilDatPos, pcpDat, 0, -1, " \n");
	StrgPutStrg (pclOutDat, &ilDatPos, pcpOldDat, 0, -1, "\n");
	if (ilDatPos > 0)
	{
	    ilDatPos--;
	}

	/* end if */
	pclOutDat[ilDatPos] = 0x00;
	ilItmCnt = field_count (pcpRoute);
	for (ilRouteItm = 1; ilRouteItm <= ilItmCnt; ilRouteItm++)
	{
	    (void) get_real_item (pclRouteNbr, pcpRoute, ilRouteItm);
	    ilActRoute = atoi (pclRouteNbr);
	    if (ilActRoute > 0)
	    { /* 20030825 JIM: added TWEND: */
		(void) tools_send_info_flag (ilActRoute, 0, "", mod_name, "", "AATArray", "", "", clTmpTwEnd, pcpCmd, pcpTbl, pclOutSel,
					     pcpFld, pclOutDat, 0);
	    }

	    /* end if */
	}                           /* end for */
	free (pclOutSel);
	free (pclOutDat);
    }

    /* end if */
    else
    {
	dbg (TRACE, "ERROR: ALLOC %d/%d BYTES FOR ACTION INFO", ilSelLen, ilDatLen);
    }                             /* end else */
    pclOutSel = NULL;
    pclOutDat = NULL;
    return ilRC;
}                               /* end ReleaseActionInfo() */

static int StringPut(char *pcpDest,char *pcpSrc1,char *pcpSrc2)
{
    int ilLen = 0;

    while (*pcpSrc1 != '\0')
    {
	*pcpDest = *pcpSrc1;
	pcpDest++;
	pcpSrc1++;
	ilLen++;
    }
    while (*pcpSrc2 != '\0')
    {
	*pcpDest = *pcpSrc2;
	pcpDest++;
	pcpSrc2++;
	ilLen++;
    }
    *pcpDest = '\0';
    return(ilLen);
}


/*******************************************************************
                END OF CEDA FUNKTIONS EXTERNAL
********************************************************************/
#endif /*  */

/* *************************************************************** */
/* *************************************************************** */
int AATArraySaveChanges(HANDLE *pspHandle, char *pcpArrayName, AAT_PARAM **prpArgDesc, long lpCommitFlag)
{
  static int ilCfgChecked = FALSE;
  static int ilUseOldStyle = FALSE;
  static int ilCheckAllDiff = FALSE;
  int ilRC = RC_SUCCESS;
  int i = 0;
  long llCurLen = 0;
  long llSepLen = 0;
  long llCount = 0;
  LIST_DESC *prlListArray = NULL;
  LIST_DESC *prlList = NULL;
  prgMyArgDesc = NULL;
  if (prpArgDesc != NULL)
  {
    AATArrayCreateArgDesc(pcpArrayName, prpArgDesc);
    prgMyArgDesc = *prpArgDesc;
  }
  if (prgMyArgDesc != NULL)
  {
    prlListArray = prgMyArgDesc->DataList;
    if (prgMyArgDesc->AppendData == FALSE)
    {
      for (i=0; i<prgMyArgDesc->ListCount; i++)
      {
        prlList = &(prlListArray[i]);
        prlList->ValueCount = 0;
        prlList->UsedSize = 0;
        if (prlList->AllocSize > 0)
        {
          prlList->ValueList[0] = 0x00;
        }
      }
    }
    else
    {
      for (i=0; i<=3; i++)
      {
        prlList = &(prlListArray[i]);
        prlList->ValueCount = 0;
        prlList->UsedSize = 0;
        if (prlList->AllocSize > 0)
        {
          prlList->ValueList[0] = 0x00;
        }
      }
      /* Must restore last ValueSeparator */
      /* and UsedSize                     */
      for (i=4; i<prgMyArgDesc->ListCount; i++)
      {
        prlList = &(prlListArray[i]);
        if (prlList->UsedSize > 0)
        {
          ToolsListAddValue(prlList, "", -1);
          prlList->ValueCount--;
        }
      }
    }
  }

  if (ilCfgChecked != TRUE)
  {
    GetOldStyleCfg(&ilUseOldStyle, &ilCheckAllDiff);
    ilCfgChecked = TRUE;
  }


  /* ilUseOldStyle = TRUE; */

  if (ilUseOldStyle == FALSE)
  {
    dbg(TRACE,"NOW CALL: FLUSH ARRAY");
    ilRC = AATArrayFlushArray(pspHandle, pcpArrayName, prgMyArgDesc, lpCommitFlag, ilCheckAllDiff);
    dbg(TRACE,"BACK FROM FLUSH ARRAY");
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,
				"RC from AATArrayFlushArray is %d, USING OLDSTYLE for this action",ilRC);
    	dbg(TRACE,"NOW CALL WRITE2DB");
    	ilRC = CEDAArrayWriteArray2DB(pspHandle, pcpArrayName, 
           NULL, NULL, NULL, NULL, &llCount, lpCommitFlag);
    	dbg(TRACE,"BACK FROM WRITE2DB");
    }
  }

  if (ilUseOldStyle == TRUE)
  {
    dbg(TRACE,"NOW CALL WRITE2DB");
    ilRC = CEDAArrayWriteArray2DB(pspHandle, pcpArrayName, 
           NULL, NULL, NULL, NULL, &llCount, lpCommitFlag);
    dbg(TRACE,"BACK FROM WRITE2DB");
  }


  if (prgMyArgDesc != NULL)
  {
    /* For DownWard Compatibility Reasons     */
    /* Copy Default Lists into Return Buffers */
    prlListArray = prgMyArgDesc->DataList;

    if ((pcgCmdLst != NULL) && (pcgCmdLst[0] != '\0'))
    {
      prlList = &(prlListArray[10]);
      ToolsListAddValue(prlList, pcgCmdLst, -1);
      prlList->ValueCount += llCount - 1;
    }

    if ((pcgUpdSelLst != NULL) && (pcgUpdSelLst[0] != '\0'))
    {
      prlList = &(prlListArray[11]);
      ToolsListAddValue(prlList, pcgUpdSelLst, -1);
      prlList->ValueCount += llCount - 1;
    }

    if ((pcgUpdFldLst != NULL) && (pcgUpdFldLst[0] != '\0'))
    {
      prlList = &(prlListArray[12]);
      ToolsListAddValue(prlList, pcgUpdFldLst, -1);
      prlList->ValueCount += llCount - 1;
    }

    if ((pcgUpdDatLst != NULL) && (pcgUpdDatLst[0] != '\0'))
    {
      prlList = &(prlListArray[13]);
      ToolsListAddValue(prlList, pcgUpdDatLst, -1);
      /* prlList->ValueCount = llCount; */
      /* Seems that we always get only one line */
      /* prlList->ValueCount += 1; */
    }

    /* Cut off last ValueSeparator */
    for (i=0; i<prgMyArgDesc->ListCount; i++)
    {
      prlList = &(prlListArray[i]);
      if (prlList->UsedSize > 0)
      {
        llCurLen = prlList->UsedSize;
        llSepLen = strlen(prlList->ValueSepa);
        if (llCurLen >= llSepLen)
        {
          llCurLen -= llSepLen;
          prlList->ValueList[llCurLen] = 0x00;
          prlList->UsedSize = llCurLen;
        }
      }
    }

  }
  prgMyArgDesc = NULL;
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
int AATArrayCreateArgDesc(char *pcpArrayName, AAT_PARAM **prpArgDesc)
{
  int ilRC = RC_SUCCESS;
  if (prpArgDesc != NULL)
  {
    if (*prpArgDesc == NULL)
    {
      dbg(TRACE,"AAT_PARAM: CREATE DESCRIPTOR");
      *prpArgDesc = (AAT_PARAM *)malloc(sizeof(AAT_PARAM));
      AATArrayInitArgDesc(pcpArrayName, *prpArgDesc, -1);
    }
  }
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
int AATArrayDestroyArgDesc(char *pcpArrayName, AAT_PARAM **prpArgDesc)
{
  int ilRC = RC_SUCCESS;
  int i = 0;
  LIST_DESC *prlListArray = NULL;
  LIST_DESC *prlList = NULL;
  if (prpArgDesc != NULL)
  {
    if (*prpArgDesc != NULL)
    {
      dbg(TRACE,"AAT_PARAM: DESTROY DESCRIPTOR");
      prgMyArgDesc = *prpArgDesc;
      prlListArray = prgMyArgDesc->DataList;
      for (i=0; i<prgMyArgDesc->ListAlloc; i++)
      {
        prlList = &(prlListArray[i]);
        ToolsListFree(prlList);
      }
      free(prlListArray);
      prlListArray = NULL;
      free(prgMyArgDesc);
      *prpArgDesc = NULL;
      prgMyArgDesc = NULL;
    }
  }
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
int AATArrayInitArgDesc(char *pcpArrayName, AAT_PARAM *prpArgDesc, int ipListCount)
{
  int ilRC = RC_SUCCESS;
  int ilNbr = -1;
  int i = 0;
  char pclTabNam[7] = "";
  char pclSelect[ARR_MAXSELECTLEN + 4] = "";
  char pclFldLst[ARR_MAXFLDLSTLEN + 4] = "";
  char pclAddFldLst[ARR_MAXFLDLSTLEN + 4] = "";

  if (prpArgDesc != NULL)
  {
    ilRC = GetCedaArrFlds (pcpArrayName, pclTabNam, pclSelect, pclFldLst, pclAddFldLst);
    if (ilRC != RC_SUCCESS)
    {
      dbg (TRACE, "InitArgDesc: Fkt. GetCedaArrFlds failed");
    }


    ToolsEventInfoInit(&(prpArgDesc->OutEvent));

    prpArgDesc->AppendData = FALSE;
    prpArgDesc->ListAlloc = 0;
    prpArgDesc->ListCount = 0;

    /* ATTENTION: ipListCount = -1   */
    /* Avoid call of future features */
    ipListCount = -1;
    if (ipListCount < 0)
    {
      ilNbr = 14;
      dbg(TRACE,"AAT_PARAM: CREATE %d DEFAULT LISTS", ilNbr);
      prpArgDesc->DataList = (LIST_DESC *)malloc(sizeof(LIST_DESC) * ilNbr);

      dbg(TRACE,"AAT_PARAM: SET INITIAL VALUES");
      for (i=0; i<ilNbr; i++)
      {
        ToolsListInit(&(prpArgDesc->DataList[i]));
      }

      ToolsListSetInfo(&(prpArgDesc->DataList[0]),"ATTACH",pclTabNam,"TABLE,FIELDS",pclFldLst,"{=","=}","\n",4096);

      ToolsListSetInfo(&(prpArgDesc->DataList[1]),"DBIF_IRT",pclTabNam,"IRT",pclFldLst,"","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[2]),"DBIF_URT",pclTabNam,"URT",pclFldLst,"","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[3]),"DBIF_DRT",pclTabNam,"DRT","URNO","","","\n",4096);

      ToolsListSetInfo(&(prpArgDesc->DataList[4]),"INSERT",pclTabNam,"DATA",pclFldLst,"","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[5]),"UPDATE",pclTabNam,"DATA",pclFldLst,"","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[6]),"DELETE",pclTabNam,"DATA","URNO","","",",",4096);

      ToolsListSetInfo(&(prpArgDesc->DataList[7]),"IRT_URNO",pclTabNam,"IRT","URNO","","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[8]),"URT_URNO",pclTabNam,"URT","URNO","","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[9]),"DRT_DATA",pclTabNam,"DRT",pclFldLst,"","","\n",4096);

      ToolsListSetInfo(&(prpArgDesc->DataList[10]),"CMD_LIST","","","","","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[11]),"SEL_LIST","","","","","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[12]),"FLD_LIST","","","","","","\n",4096);
      ToolsListSetInfo(&(prpArgDesc->DataList[13]),"DAT_LIST","","","","","","\n",4096);
    }
    if (ipListCount > 0)
    {
      ilNbr = ipListCount;
      prpArgDesc->DataList = (LIST_DESC *)malloc(sizeof(LIST_DESC) * ilNbr);
      for (i=0; i<ilNbr; i++)
      {
        ToolsListInit(&prpArgDesc->DataList[i]);
        ToolsListSetInfo(&(prpArgDesc->DataList[i]),"","","",pclFldLst,"","","\n",4096);
      }
    }
    prpArgDesc->ListAlloc = ilNbr;
    prpArgDesc->ListCount = ilNbr;
  }
  return ilRC;
}



/* *************************************************************** */
/* *************************************************************** */
int AATArraySetSbcInfo(char *pcpArrayName, AAT_PARAM *prpArgDesc,
                       char *pcpQue, char *pcpPri, char *pcpCmd, char *pcpTbl,
                       char *pcpWks, char *pcpUsr, char *pcpTws, char *pcpTwe,
                       char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRC = RC_SUCCESS;
  EVT_INFO *prlEvtInfo;
  prlEvtInfo = &(prpArgDesc->OutEvent);
  ToolsEventSetInfo(prlEvtInfo, pcpQue, pcpPri, pcpCmd, pcpTbl, pcpWks, pcpUsr, 
                                pcpTws, pcpTwe, pcpSel, pcpFld, pcpDat);
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
int AATArrayCreateSbc(char *pcpArrayName, AAT_PARAM *prpArgDesc,
                      int ipFormat, char *pcpLists, int ipSpoolEvt)
{
  int ilRC = RC_SUCCESS;
  int ilItmCnt = 0;
  int ilItmNbr = 0;
  int ilIdx = 0;
  long llCurLen = 0;
  long llSepLen = 0;
  char pclPrefix[4];
  char pclSuffix[4];
  char pclTmp[32];
  char pclQuePrio[64];
  LIST_DESC *prlListArray = NULL;
  LIST_DESC *prlAttach = NULL;
  LIST_DESC *prlList = NULL;
  EVT_INFO *prlEvtInfo = NULL;

  if (prpArgDesc != NULL)
  {
    prlEvtInfo = &(prpArgDesc->OutEvent);
    prlListArray = prpArgDesc->DataList;
    prlAttach = &(prlListArray[0]);
    prlAttach->UsedSize = 0;
    prlAttach->ValueCount = 0;
    prlAttach->ValueList[0] = 0x00;
    strcpy(pclPrefix,prlAttach->ValuePref);
    strcpy(pclSuffix,prlAttach->ValueSuff);
    if ((ipFormat >= 0) && (pcpLists[0] != '\0'))
    {
      ToolsListAddKeyItem(prlAttach, pclPrefix, prlAttach->ListName, pclSuffix, "", "", -1);
      (void) get_real_item (pclTmp, prlAttach->ListType, 1);
      ToolsListAddKeyItem(prlAttach, pclPrefix, pclTmp, pclSuffix, prlAttach->ListCode, "\\", -1);
      (void) get_real_item (pclTmp, prlAttach->ListType, 2);
      ToolsListAddKeyItem(prlAttach, pclPrefix, pclTmp, pclSuffix, prlAttach->ListHead, "\\", -1);
      ToolsListAddKeyItem(prlAttach, pclPrefix, "INFO", pclSuffix, "NONE", "\\", -1);
      ilItmCnt = field_count (pcpLists);
      for (ilItmNbr = 1; ilItmNbr <= ilItmCnt; ilItmNbr++)
      {
        (void) get_real_item (pclTmp, pcpLists, ilItmNbr);
        ilIdx = atoi(pclTmp);
        prlList = &(prlListArray[ilIdx]);
        ToolsListAddKeyItem(prlAttach, pclPrefix, prlList->ListName, pclSuffix, "", "", -1);
        prlAttach->UsedSize--;
        sprintf(pclTmp,"%d",prlList->ValueCount);
        ToolsListAddKeyItem(prlAttach, pclPrefix, "COUNT", pclSuffix, pclTmp, "\\", -1);
        prlAttach->UsedSize--;
        ToolsListAddKeyItem(prlAttach, pclPrefix, prlList->ListType, pclSuffix, prlList->ValueList, "\\", -1);
        prlAttach->UsedSize--;
        sprintf(pclTmp,"\\%s",prlList->ListName);
        ToolsListAddKeyItem(prlAttach, pclPrefix, pclTmp, pclSuffix, "", "", -1);
      } /* end for */
      sprintf(pclTmp,"\\%s",prlAttach->ListName);
      ToolsListAddKeyItem(prlAttach, pclPrefix, pclTmp, pclSuffix, "", "", -1);
      llCurLen = prlAttach->UsedSize;
      llSepLen = strlen(prlAttach->ValueSepa);
      llCurLen -= llSepLen;
      prlAttach->ValueList[llCurLen] = 0x00;
      prlAttach->UsedSize = llCurLen;
    }

    ToolsSetSpoolerInfo(prlEvtInfo->EvtUsr, prlEvtInfo->EvtWks, prlEvtInfo->EvtTws, prlEvtInfo->EvtTwe);

    sprintf(pclQuePrio,"%s|%s",prlEvtInfo->OutQue,prlEvtInfo->OutPri);
    ToolsSpoolEventData(pclQuePrio, prlEvtInfo->EvtTbl, prlEvtInfo->EvtCmd, "", 
                   prlEvtInfo->EvtSel.Value, prlEvtInfo->EvtFld.Value, prlEvtInfo->EvtDat.Value, 
                   prlAttach->ValueList);
    if (ipSpoolEvt == FALSE)
    {
      ToolsReleaseEventSpooler(TRUE, 3);
    }
  }
  
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
static void AppStrgToList(int ipListIdx, char *pcpStrg, long lpCount)
{
  LIST_DESC *prlListArray = NULL;
  LIST_DESC *prlList = NULL;
  if (prgMyArgDesc != NULL)
  {
    prlListArray = prgMyArgDesc->DataList;
    prlList = &(prlListArray[ipListIdx]);
    if (lpCount > 1)
    {
      BuildItemBuffer (pcpStrg, "", lpCount, ",");
    }
    ToolsListAddValue(prlList, pcpStrg, -1);
    if (lpCount > 1)
    {
      delton(pcpStrg);
    }
  }
  return;
}

/* *************************************************************** */
/* *************************************************************** */
static void AppStrgToListPlus(int ipListIdx, char *pcpStrg, long lpCount, char *pcpSep, char *pcpStrg2)
{
  LIST_DESC *prlListArray = NULL;
  LIST_DESC *prlList = NULL;
  if (prgMyArgDesc != NULL)
  {
    prlListArray = prgMyArgDesc->DataList;
    prlList = &(prlListArray[ipListIdx]);
    if (lpCount > 1)
    {
      BuildItemBuffer (pcpStrg, "", lpCount, ",");
    }
    ToolsListAddValuePlus(prlList, pcpStrg, -1, pcpSep, pcpStrg2, -1);
    if (lpCount > 1)
    {
      delton(pcpStrg);
    }
  }
  return;
}

/* *************************************************************** */
/* *************************************************************** */
static int AATArrayFlushArray(HANDLE *pspHandle, char *pcpArrayName, AAT_PARAM *prpArgDesc, long lpCommitFlag, int ipCheckAll)
{
  static ilRecDescInit = FALSE;
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilList = 0;
  int ilDifCnt = 0;
  int ilUpdCnt = 0;
  int ilChgCnt = 0;
  int ilEquCnt = 0;
	int ilInsertCount = 0;
	int ilUpdateCount = 0;
	int ilDeleteCount = 0;
  long llRowNum = 0;
  long llCnt = 0;
  short *pslCursor = NULL;
  char *pclSqlCmd = NULL;
  char *pclSqlDat = NULL;
  AATARRAY *prlTmpArray = NULL;
  AATARRAYROW *prlTmpRow = NULL;

  if (ilRecDescInit == FALSE)
  {
    InitRecordDescriptor(&rgSqlRecDesc,32,0,TRUE);
    ilRecDescInit = TRUE;
  }

  ilRC = FindArray (pspHandle, pcpArrayName, &prlTmpArray);
  if (ilRC == RC_SUCCESS)
  {
    GetServerTimeStamp("UTC", 1, 0, prlTmpArray->TimeStamp);
    CreateOutRoutes(prlTmpArray);
    dbg(TRACE,"%s: OUT ROUTES <%s>",prlTmpArray->TableName,prlTmpArray->RouteIds);

    /* ---------------------------------------------- */
    /* Step 0:                                        */
    /* Run through the lines and calculate list values*/
    /* ---------------------------------------------- */
		dbg(DEBUG,"%05d: calculate list values follows",__LINE__);
    for (llRowNum = 0; llRowNum < prlTmpArray->MaxRows; llRowNum++)
    {
      prlTmpRow = &(prlTmpArray->Rows[llRowNum]);
      if (prlTmpRow != NULL)
      {
        switch (prlTmpRow->ChangeFlag)
        {
          case ARR_DATA_NEW:
               ilInsertCount++;
               break;
          case ARR_DATA_CHANGED:
  							if ((prlTmpRow->DataNew != NULL) && 
											(prlTmpRow->DataOrig != NULL))
									{
								 			ilUpdateCount++;
								 	}
								break;
          case ARR_DATA_DELETED:
         						ilDeleteCount++; 
               break;
        }
      }
    }


		if (ilInsertCount > 0)
		{
			long llStepSize;

			llStepSize = ((ilInsertCount+1)*prlTmpArray->RowLen);

      ToolsListSetStepSize(&(prpArgDesc->DataList[1]),llStepSize);
      ToolsListSetStepSize(&(prpArgDesc->DataList[4]),llStepSize);
			dbg(DEBUG,"%05d: Calculated Inserts: %d Bytes %ld",__LINE__,ilInsertCount,llStepSize);
     }
	
		if (ilUpdateCount > 0)
		{
			long llStepSize;

			llStepSize = ((ilUpdateCount+1)*prlTmpArray->RowLen);

			ToolsListSetStepSize(&(prpArgDesc->DataList[2]),llStepSize);
      ToolsListSetStepSize(&(prpArgDesc->DataList[5]),llStepSize);
			dbg(DEBUG,"%05d: Calculated Updates: %d Bytes %ld",__LINE__,ilUpdateCount,llStepSize);
			}

		if (ilDeleteCount > 0)
		{
			long llStepSize;

			llStepSize = ((ilDeleteCount+1)*prlTmpArray->RowLen);
      ToolsListSetStepSize(&(prpArgDesc->DataList[9]),llStepSize);
			dbg(DEBUG,"%05d: Calculated Deletions: %d Bytes %ld",__LINE__,ilDeleteCount,llStepSize);
		}


		
    /* ---------------------------------------------- */
    /* Step 1:                                        */
    /* Run through the lines and store changed values */
    /* ---------------------------------------------- */
    for (llRowNum = 0; llRowNum < prlTmpArray->MaxRows; llRowNum++)
    {
      prlTmpRow = &(prlTmpArray->Rows[llRowNum]);
      if (prlTmpRow != NULL)
      {
        switch (prlTmpRow->ChangeFlag)
        {
          case ARR_DATA_NEW:
               ilGetRc = HandleRowInsert(prlTmpArray, prlTmpRow);
               break;
          case ARR_DATA_CHANGED:
               ilUpdCnt++;
               ilGetRc = HandleRowUpdate(prlTmpArray, prlTmpRow);
               if (ilGetRc == TRUE)
               {
                 ilChgCnt++;
               }
               else
               {
                 ilEquCnt++;
               }
               break;
          case ARR_DATA_DELETED:
               ilGetRc = HandleRowDelete(prlTmpArray, prlTmpRow);
               break;
          case ARR_DATANEW_DELETED:
               break;
          default:
							 /* now ChangeFlag should be set correct MCU 15.10.2004 */
               if (ipCheckAll == TRUE)
               {
                 if ((prlTmpRow->DataNew != NULL) && (prlTmpRow->DataOrig != NULL))
                 {
                   /* Might be that we still forgot to set */
                   /* ARR_DATA_CHANGED in some functions.  */
                   ilDifCnt++;
                   ilGetRc = HandleRowUpdate(prlTmpArray, prlTmpRow);
                   if (ilGetRc == TRUE)
                   {
                     ilChgCnt++;
                   }
                   else
                   {
                     ilEquCnt++;
                   }
                 }
               }
          break;
        }
      }
    }
    dbg(TRACE,"%s: UPD=%d DIF=%d CHG=%d EQU=%d", prlTmpArray->TableName, ilUpdCnt, ilDifCnt, ilChgCnt, ilEquCnt);

    /* ---------------------------------------------- */
    /* Step 2:                                        */
    /* Run through the lists and call DBIF            */
    /* ---------------------------------------------- */
    ilRC = DB_SUCCESS;
    ilList = 0;
    llCnt = 0;
    while ((ilList < 3) && (ilRC == DB_SUCCESS))
    {
      ilList++;
      if (prpArgDesc->DataList[ilList].ValueCount > 0)
      {
        dbg(TRACE,"%s: <%s> COUNT=%d", prlTmpArray->TableName,
                                       prpArgDesc->DataList[ilList].ListName,
                                       prpArgDesc->DataList[ilList].ValueCount);
        llCnt += prpArgDesc->DataList[ilList].ValueCount;
        switch (ilList)
        {
          case 1:          /* ALL INSERTS */
               pslCursor = &(prlTmpArray->SqlCsrIrt);
               pclSqlCmd = prlTmpArray->SqlCmdIrt;
               pclSqlDat = prpArgDesc->DataList[ilList].ValueList;
               break;
          case 2:          /* ALL UPDATES */
               pslCursor = &(prlTmpArray->SqlCsrUrt);
               pclSqlCmd = prlTmpArray->SqlCmdUrt;
               pclSqlDat = prpArgDesc->DataList[ilList].ValueList;
               break;
          case 3:          /* ALL DELETES */
               pslCursor = &(prlTmpArray->SqlCsrDrt);
               pclSqlCmd = prlTmpArray->SqlCmdDrt;
               pclSqlDat = prpArgDesc->DataList[ilList].ValueList;
               break;
          default:
          break;
        }
        ilRC = SqlIfArray(0,pslCursor,pclSqlCmd,pclSqlDat,0,&rgSqlRecDesc);
        if (ilRC == ORA_NOT_FOUND)
        {
          ilRC = DB_SUCCESS;
        }
      }
    }
    if ((ilRC == DB_SUCCESS) && (llCnt > 0))
    {
      ilRC = RC_SUCCESS;
      if ((lpCommitFlag == ARR_COMMIT_ALL_OK) || (lpCommitFlag == ARR_COMMIT_TILL_ERR))
      {
        commit_work();
        dbg(TRACE,"COMMITTED: %s = %d RECORDS", prlTmpArray->TableName, llCnt);
        ToolsReleaseEventSpooler(TRUE, 3);

        /* ---------------------------------------------- */
        /* Step 3:                                        */
        /* Run again through the lines and reset values.  */
        /* Note:                                          */
        /* We can't reset the lines in step 1 because     */
        /* we'll need them as they are for the "OldStyle" */
        /* Write2DB in any case of DB errors.             */
        /* ---------------------------------------------- */
        for (llRowNum = 0; llRowNum < prlTmpArray->MaxRows; llRowNum++)
        {
          prlTmpRow = &(prlTmpArray->Rows[llRowNum]);
          if (prlTmpRow != NULL)
          {
            switch (prlTmpRow->ChangeFlag)
            {
              case ARR_DATA_NEW:
	           UnsetRowChanges(prlTmpRow, prlTmpArray);
                   break;
              case ARR_DATA_CHANGED:
	           UnsetRowChanges(prlTmpRow, prlTmpArray);
                   break;
              case ARR_DATA_DELETED:
                   ilGetRc = AATArrayDeleteRow(pspHandle, pcpArrayName, llRowNum);
                   break;
              case ARR_DATANEW_DELETED:
                   ilGetRc = AATArrayDeleteRow(pspHandle, pcpArrayName, llRowNum);
                   break;
              default:
                   if ((prlTmpRow->DataNew != NULL) && (prlTmpRow->DataOrig != NULL))
                   {
	             UnsetRowChanges(prlTmpRow, prlTmpArray);
                   }
              break;
            }
          }
        }
      }
    }
    else
    {
      if (llCnt > 0)
      {
        dbg(TRACE,"MUST ROLL BACK: %s = %d RECORDS", prlTmpArray->TableName, llCnt);
        rollback();
        ToolsReleaseEventSpooler(FALSE, 3);
        ilRC = RC_FAIL;
      }
      else
      {
        dbg(TRACE,"NO CHANGES: %s = %d RECORDS", prlTmpArray->TableName, llCnt);
        ilRC = RC_SUCCESS;
      }
    }
  }
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
static int HandleRowInsert(AATARRAY *prpArray, AATARRAYROW *prpRow)
{
  int ilRC = RC_SUCCESS;
  long llFldCnt = 0;
  char pclUrnoStr[16];
  char pclUrnoKey[32];
  char *pclNewData = NULL;
  char *pclOldData = NULL;
  llFldCnt = prpArray->SqlFieldCount;
  pclNewData = prpRow->DataNew;
  if (pclNewData == NULL)
  {
    pclNewData = prpRow->DataOrig;
  }
  pclOldData = prpArray->EmptyDataLine;
  if (pclNewData != NULL)
  {
    GetUrnoProperty(prpArray, prpRow, pclUrnoStr, pclUrnoKey);
    BuildItemBuffer (pclNewData, "", llFldCnt, ",");
    AppStrgToList(1, pclNewData, 1);
    AppStrgToList(4, pclNewData, 1);
    AppStrgToList(7, pclUrnoStr, 1);
    if (prpArray->RouteIds[0] != '\0')
    {
      BuildItemBuffer (pclOldData, "", llFldCnt, ",");
      ToolsSpoolEventData(prpArray->RouteIds, prpArray->TableName, "IRT", pclUrnoStr,
                   pclUrnoKey, prpArray->SqlFieldList, pclNewData, pclOldData);
      delton(pclOldData);
    }
    delton(pclNewData);
  }
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
static int HandleRowUpdate(AATARRAY *prpArray, AATARRAYROW *prpRow)
{
  int ilChanged = FALSE;
  long llFldCnt = 0;
  long llFldNum = 0;
  long llOffset = 0;
  char pclUrnoStr[16];
  char pclUrnoKey[32];
  char *pclNewData = NULL;
  char *pclOldData = NULL;
  llFldCnt = prpArray->SqlFieldCount;
  pclNewData = prpRow->DataNew;
  pclOldData = prpRow->DataOrig;
  if ((pclNewData != NULL) && (pclOldData != NULL))
  {
    /* Might be that the Main Process updated fields */
    /* with unchanged values. But we won't save such */
    /* unchanged records. Thus we must look for DIFF */
    ilChanged = CheckRowDataChanges(prpArray, prpRow);
    if (ilChanged == TRUE)
    {
      llFldNum = prpArray->LstuIdx;
      if (llFldNum >= 0)
      {
        llOffset = prpArray->FieldOffsets[llFldNum];
        strcpy(&pclNewData[llOffset], prpArray->TimeStamp);
      }
      GetUrnoProperty(prpArray, prpRow, pclUrnoStr, pclUrnoKey);
      BuildItemBuffer (pclNewData, "", llFldCnt, ",");
      AppStrgToListPlus(2, pclNewData, 1, ",", pclUrnoStr);
      AppStrgToList(5, pclNewData, 1);
      AppStrgToList(8, pclUrnoStr, 1);
      if (prpArray->RouteIds[0] != '\0')
      {
        BuildItemBuffer (pclOldData, "", llFldCnt, ",");
        ToolsSpoolEventData(prpArray->RouteIds, prpArray->TableName, "URT", pclUrnoStr,
                     pclUrnoKey, prpArray->SqlFieldList, pclNewData, pclOldData);
        delton(pclOldData);
      }
      delton(pclNewData);
    }
  }
  return ilChanged;
}

/* *************************************************************** */
/* *************************************************************** */
static int HandleRowDelete(AATARRAY *prpArray, AATARRAYROW *prpRow)
{
  int ilRC = RC_SUCCESS;
  long llFldCnt = 0;
  char pclUrnoStr[16];
  char pclUrnoKey[32];
  char *pclNewData = NULL;
  char *pclOldData = NULL;
  llFldCnt = prpArray->SqlFieldCount;
  pclNewData = prpRow->DataOrig;
  if (pclNewData != NULL)
  {
    GetUrnoProperty(prpArray, prpRow, pclUrnoStr, pclUrnoKey);
    AppStrgToList(3, pclUrnoStr, 1);
    AppStrgToList(6, pclUrnoStr, 1);
    BuildItemBuffer (pclNewData, "", llFldCnt, ",");
    AppStrgToList(9, pclNewData, 1);
    if (prpArray->RouteIds[0] != '\0')
    {
      ToolsSpoolEventData(prpArray->RouteIds, prpArray->TableName, "DRT", pclUrnoStr,
                   pclUrnoKey, prpArray->SqlFieldList, pclNewData, pclNewData);
    }
    delton(pclNewData);
  }
  return ilRC;
}

/* *************************************************************** */
/* *************************************************************** */
static int CreateSqlProperties(AATARRAY *prpArray)
{
  int ilRC = RC_SUCCESS;
  long llFldCnt = 0;
  long llAllCnt = 0;
  long llLstLen = 0;
  char *pclFldLst = NULL;
  pclFldLst = prpArray->SqlFieldList;
  prpArray->UrnoIdx = FindField(pclFldLst, "URNO");
  prpArray->CdatIdx = FindField(pclFldLst, "CDAT");
  prpArray->LstuIdx = FindField(pclFldLst, "LSTU");
  prpArray->UseuIdx = FindField(pclFldLst, "USEU");
  llFldCnt = prpArray->FieldCount;
  prpArray->EmptyDataLine = malloc(llFldCnt);
  memset(prpArray->EmptyDataLine, ',', llFldCnt-1);
  prpArray->EmptyDataLine[llFldCnt-1] = '\0';
  delton(prpArray->EmptyDataLine);
  llFldCnt = prpArray->SqlFieldCount;
  llLstLen = strlen(pclFldLst);
  llAllCnt = (llLstLen*2) + (llFldCnt*2) + (llFldCnt*2) + 64;
  prpArray->SqlCsrIrt = 0;
  prpArray->SqlCmdIrt = malloc(llAllCnt);
  BuildSqlCmdStrg (prpArray->SqlCmdIrt, prpArray->TableName, pclFldLst, llFldCnt, FOR_INSERT);
  prpArray->SqlCsrUrt = 0;
  prpArray->SqlCmdUrt = malloc(llAllCnt);
  BuildSqlCmdStrg (prpArray->SqlCmdUrt, prpArray->TableName, pclFldLst, llFldCnt, FOR_UPDATE);
  prpArray->SqlCsrDrt = 0;
  prpArray->SqlCmdDrt = malloc(llAllCnt);
  BuildSqlCmdStrg (prpArray->SqlCmdDrt, prpArray->TableName, pclFldLst, llFldCnt, FOR_DELETE);
  strcpy(prpArray->RouteIds,"");
  return ilRC;
}

/********************************************************/
/********************************************************/
static void BuildSqlCmdStrg (char *pcpSql, char *pcpTbl, char *pcpFld, long lpCnt, int ipTyp)
{
  int ilPos = 0;
  long llFld = 0;
  char *pclBgn = NULL;
  char *pclEnd = NULL;
  switch (ipTyp)
  {
    case FOR_INSERT:
      ilPos = 0;
      StrgPutStrg (pcpSql, &ilPos, "INSERT INTO ", 0, -1, "\0");
      StrgPutStrg (pcpSql, &ilPos, pcpTbl, 0, -1, " (");
      pclBgn = pcpFld;
      for (llFld = 0; llFld < lpCnt; llFld++)
      {
        pclEnd = strstr(pclBgn, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = '\0';
        }
        StrgPutStrg (pcpSql, &ilPos, pclBgn, 0, -1, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = ',';
        }
        pclBgn = pclEnd + 1;
      }
      ilPos--;
      StrgPutStrg (pcpSql, &ilPos, ") VALUES (", 0, -1, "\0");
      pclBgn = pcpFld;
      for (llFld = 0; llFld < lpCnt; llFld++)
      {
        pclEnd = strstr(pclBgn, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = '\0';
        }
        StrgPutStrg (pcpSql, &ilPos, ":V", 0, -1, "\0");
        StrgPutStrg (pcpSql, &ilPos, pclBgn, 0, -1, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = ',';
        }
        pclBgn = pclEnd + 1;
      }
      ilPos--;
      StrgPutStrg (pcpSql, &ilPos, ")", 0, -1, "\0");
      pcpSql[ilPos] = 0x00;
    break;
    case FOR_UPDATE:
      ilPos = 0;
      StrgPutStrg (pcpSql, &ilPos, "UPDATE ", 0, -1, "\0");
      StrgPutStrg (pcpSql, &ilPos, pcpTbl, 0, -1, " SET ");
      pclBgn = pcpFld;
      for (llFld = 0; llFld < lpCnt; llFld++)
      {
        pclEnd = strstr(pclBgn, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = '\0';
        }
        StrgPutStrg (pcpSql, &ilPos, pclBgn, 0, -1, "=:V");
        StrgPutStrg (pcpSql, &ilPos, pclBgn, 0, -1, ",");
        if (pclEnd != NULL)
        {
          *pclEnd = ',';
        }
        pclBgn = pclEnd + 1;
      }
      ilPos--;
      StrgPutStrg (pcpSql, &ilPos, " WHERE URNO=:VURNO", 0, -1, "\0");
      pcpSql[ilPos] = 0x00;
    break;
    case FOR_DELETE:
      sprintf(pcpSql,"DELETE FROM %s WHERE URNO=:VURNO", pcpTbl);
    break;
  }
  return;
}

/********************************************************/
/********************************************************/
static void GetUrnoProperty(AATARRAY *prpArray, AATARRAYROW *prpRow, char *pcpUrnoStr, char *pcpUrnoKey)
{
  int ilChr = 0;
  long llUrnoTyp = 0;
  long llFldNum = 0;
  long llOffset = 0;
  char *pclDataOrig = NULL;
  llFldNum = prpArray->UrnoIdx;
  llOffset = prpArray->FieldOffsets[llFldNum];
  if (prpRow->DataNew != NULL)
  {
    pclDataOrig = (char *) &prpRow->DataNew[llOffset];
  }
  else
  {
    pclDataOrig = (char *) &prpRow->DataOrig[llOffset];
  }
  strcpy (pcpUrnoStr, pclDataOrig);
  ilChr = strlen(pcpUrnoStr) - 1;
  while ((ilChr >= 0) && (pcpUrnoStr[ilChr] == ' '))
  {
    pcpUrnoStr[ilChr] = '\0';
    ilChr--;
  }
  llUrnoTyp = prpArray->FieldTyps[llFldNum];
  if (llUrnoTyp == ARR_NUM_TYPE)
  {
    sprintf (pcpUrnoKey, "WHERE URNO=%s", pcpUrnoStr);
  }
  else
  {
    sprintf (pcpUrnoKey, "WHERE URNO='%s'", pcpUrnoStr);
  }
  return;
}

/********************************************************/
/********************************************************/
static void CreateOutRoutes(AATARRAY *prpArray)
{
  int ilSendOut = FALSE;
  int ilLen = 0;
  char pclQue[32] = "";
  char pclPri[32] = "";
  char pclTmpQue[16];
  char pclTmpPri[16];
  if (prpArray->RouteIds[0] == '\0')
  {
    CheckTableLogging(prpArray);

    if (prpArray->SendToAction == TRUE)
    {
      sprintf(pclTmpQue, "%d,", igActionId);
      strcat(pclQue,pclTmpQue);
      sprintf(pclTmpPri, "%d,", 4);
      strcat(pclPri,pclTmpPri);
      ilSendOut = TRUE;
    }
    if (prpArray->SendToBchdl == TRUE)
    {
      sprintf(pclTmpQue, "%d,", igBchdlId);
      strcat(pclQue,pclTmpQue);
      sprintf(pclTmpPri, "%d,", 3);
      strcat(pclPri,pclTmpPri);
      ilSendOut = TRUE;
    }

    if (prpArray->SendToLoghdl == TRUE)
    {
      sprintf(pclTmpQue, "%d,", igLoghdlId);
      strcat(pclQue,pclTmpQue);
      sprintf(pclTmpPri, "%d,", 3);
      strcat(pclPri,pclTmpPri);
      ilSendOut = TRUE;
    }

    if (ilSendOut == TRUE)
    {
      ilLen = strlen(pclQue) - 1;
      pclQue[ilLen] = 0x00;
      ilLen = strlen(pclPri) - 1;
      pclPri[ilLen] = 0x00;
      sprintf(prpArray->RouteIds,"%s|%s",pclQue,pclPri);
    }
  }
  return;
}

/********************************************************/
/********************************************************/
static int CheckRowDataChanges(AATARRAY *prpArray, AATARRAYROW *prpRow)
{
  int ilChanged = FALSE;
  long llFldCnt = 0;
  long llCurFld = 0;
  long llOffset = 0;
  char *pclRowNew = NULL;
  char *pclRowOld = NULL;
  char *pclFldNew = NULL;
  char *pclFldOld = NULL;
  pclRowNew = prpRow->DataNew;
  pclRowOld = prpRow->DataOrig;
  if ((pclRowNew != NULL) && (pclRowOld != NULL))
  {
    llFldCnt = prpArray->SqlFieldCount;
    while ((llCurFld < llFldCnt) && (ilChanged != TRUE))
    {
      if ((llCurFld != prpArray->LstuIdx) && (llCurFld != prpArray->UseuIdx))
      {
        llOffset = prpArray->FieldOffsets[llCurFld];
        pclFldNew = &pclRowNew[llOffset];
        pclFldOld = &pclRowOld[llOffset];
        if (strcmp(pclFldNew,pclFldOld) != 0)
        {
          ilChanged = TRUE;
        }
      }
      llCurFld++;
    }
  }
  return ilChanged;
}

/********************************************************/
/********************************************************/
static void GetOldStyleCfg(int *pipOldStyle, int *pipCheckAll)
{
  int ilUseOldStyle = FALSE;
  int ilCheckDiff = TRUE;
  int ilGetRc = RC_SUCCESS;
  char *Ptmp = NULL;
  char pclCfgFile[128];
  char pclCfgBuf[64];
  char pclMyName[64];
  if ((Ptmp = getenv("CFG_PATH")) == NULL)
  {
    dbg (TRACE,"Error Reading CFG_PATH");
  }
  else
  {
    sprintf(pclCfgFile, "%s/aatarray.cfg", Ptmp);
    strcpy(pclMyName, mod_name);
    strcpy(pclCfgBuf,"");
    dbg(TRACE,"READ <%s>",pclCfgFile);
    dbg(TRACE,"LOOK IN [USE_OLD_STYLE] <%s>",pclMyName);
    ilGetRc = iGetConfigRow(pclCfgFile,"USE_OLD_STYLE",pclMyName,CFG_STRING,pclCfgBuf);
    if ((ilGetRc == RC_SUCCESS) && (pclCfgBuf[0] == 'Y'))
    {
      ilUseOldStyle = TRUE;
    }
  }
  if (ilUseOldStyle == FALSE)
  {
    dbg(TRACE,"<%s> USING NEW STYLE (FLUSH ARRAY)",pclMyName);
    dbg(TRACE,"LOOK IN [CHECK_ALL_DIFF] <%s>",pclMyName);
    ilGetRc = iGetConfigRow(pclCfgFile,"CHECK_ALL_DIFF",pclMyName,CFG_STRING,pclCfgBuf);
    if ((ilGetRc == RC_SUCCESS) && (pclCfgBuf[0] == 'N'))
    {
      ilCheckDiff = FALSE;
    }
    if (ilCheckDiff == FALSE)
    {
      dbg(TRACE,"<%s> CHECK UPD FOR <ARR_DATA_CHANGED> ONLY",pclMyName);
    }
    else
    {
      dbg(TRACE,"<%s> CHECK UPD FOR ALL MODIFIED ROWS",pclMyName);
    }
  }
  else
  {
    dbg(TRACE,"<%s> USING OLD STYLE (WRITE2DB)",pclMyName);
  }

  *pipOldStyle = ilUseOldStyle;
  *pipCheckAll = ilCheckDiff;
  return;
}

static void CheckTableLogging(AATARRAY *prpArray)
{
  char pclSqlBuf[64];
  char pclSqlDat[64];
  char pclSqlTab[64];
  int ilDBRC = DB_SUCCESS;
  short slFkt = START;
  short slLocalCursor = 0;
  if (prpArray->LoggingChecked == FALSE)
  {
    strcpy(pclSqlTab,prpArray->TableName);
    pclSqlTab[3] = 0x00;
    sprintf(pclSqlBuf,"SELECT LOGD FROM SYSTAB WHERE TANA='%s' AND LOGD<>' '", pclSqlTab);
    dbg(TRACE,"CHECK LOGGING <%s>", pclSqlBuf);
    ilDBRC = sql_if (slFkt, &slLocalCursor, pclSqlBuf, pclSqlDat);
    if (ilDBRC == DB_SUCCESS)
    {
      dbg(TRACE,"LOGGING OF <%s> ACTIVATED", prpArray->TableName);
      prpArray->SendToLoghdl = TRUE;
    }
    else
    {
      dbg(TRACE,"LOGGING OF <%s> NOT CONFIGURED", prpArray->TableName);
      prpArray->SendToLoghdl = FALSE;
    }

    close_my_cursor (&slLocalCursor);
    prpArray->LoggingChecked = TRUE;
  }
  return;
}






