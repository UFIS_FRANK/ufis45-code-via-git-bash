#ifndef _DEF_mks_version_loghdl_h
  #define _DEF_mks_version_loghdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_loghdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/loghdl.h 1.2 2004/07/27 16:48:02SGT jim Exp  $";
#endif /* _DEF_mks_version */

/*	**********************************************************************	*/
/*	The include file loghdl.h										*/
/*																*/
/*	Program		:	loghdl.c										*/
/*	Date			:	23.09.1997									*/
/*	Author		:	JM�											*/
/*																*/
/*	Revision date	:												*/
/*	NOTE			:	This file contains the Communication Structure		*/
/*					neccessary to serve loghdl						*/
/*																*/
/*	**********************************************************************	*/

#ifndef	LOGHDL_H
#define	LOGHDL_H

static   char  sccs_version_loghdl_hrd[] = "@(#) UFIS 4.3 (c) ABB ACE/FE loghdl.h  1.3 / 98/06/18 12:07:42 / JMU";
#define	MAX_FUNCTION_SIZE	10
#define	MAX_TABNAME_SIZE	TABLE_NAME_LEN + 2
#define	MAX_URNO_SIZE		10
#define	MAX_SSMDATE_SIZE	10000
#define	MAX_USERNAME_SIZE	20
#define	MAX_FIELD_SIZE		1000
#define	MAX_FIELD_NO		100
#define	MAX_FIELDNAME_SIZE	FIELD_NAME_LEN
#define	MAX_VALUE_SIZE		1000
#define	MAX_CEDA_ROUTE		20000
#define	TIMESTAMP_SIZE		18
#define	MAX_DATABUF_SIZE	MAX_EVENT_SIZE
#define	MAX_SQLBUF_SIZE	MAX_EVENT_SIZE
#define	MAX_ROUTE_SIZE		6
#define	MAX_USER_SIZE		20


/*	The Command Codes for Transactions, WriteLogRecord() in struct LOGCOM		*/

#define	NO_TRANS				0			/*	not a transaction		*/
#define	TRANS_BEGIN			1			/*	start of a transaction	*/
#define	TRANS_MEMBER			2			/*	sub action			*/
#define	TRANS_COMMIT			3			/*	commit of transaction	*/
#define	TRANS_ROLLBACK			4			/*	rollback transaction	*/
#define	INIT_TRANSACTION_HEAD	5		/*	initialize transaction buffer	*/
/*	loghdl communication structure					*/

/*	The WriteLogRecord Error Messages									*/

#define	LOG_PROTOCOL			1				/*	Protokoll Fehler	*/
#define	LOG_PARAM				2				/*	Parameter Fehler	*/
#define	LOG_MEMORY			3	/*	Fehler beim Speicher Allokieren	*/
#define	LOG_SEND				4			/*	Fehler beim Versenden	*/
#define	LOG_DELETE			5		/*	Fehler beim L�schen der Liste	*/



typedef	struct	{
	int		IntCom;							/*	Transaction Command	*/
	int		UserRoute;
	char		Function[MAX_FUNCTION_SIZE];
	char		Table[MAX_TABNAME_SIZE];
	char		LeaderUrno[MAX_URNO_SIZE];
	int		UserName;							/*	Offset to UserName	*/
	int		OrnoList;							/*	Offset to OrnoList	*/
	int		FieldList;						/*	Offset to FieldList	*/
	int		ValueList;						/*	Offset to ValueList	*/
	int		UserLen;
	int		OrnoLen;
	int		FieldLen;
	int		ValueLen;
} LOGCOM;
#define	LOGCOM_SIZE	sizeof(LOGCOM)

/*	Best way to prevent errors is to separate the Blocks by NULL-Character.	*/
/*	This helps, but istn't really neccessary, as for reading only offsets and	*/
/*	Length are neccessary	*/

#endif
