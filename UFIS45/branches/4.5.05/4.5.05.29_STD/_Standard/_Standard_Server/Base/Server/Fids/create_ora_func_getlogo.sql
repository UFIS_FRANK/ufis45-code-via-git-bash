CREATE OR REPLACE  FUNCTION GETLOGO 
(v_urno in fldtab.urno%TYPE, v_num in number)
return char
is
rc_txt char(128);
CURSOR flgtab_curs (p_urno in fldtab.urno%TYPE) IS
	SELECT lgsn FROM flgtab WHERE
		flgtab.urno IN
		(SELECT flgu FROM flztab WHERE fldu = p_urno);
BEGIN
	open flgtab_curs (v_urno);
	for i in 1..v_num loop
	    fetch flgtab_curs into rc_txt;
	    IF flgtab_curs%NOTFOUND THEN
		rc_txt := ' ';
		END IF;
		EXIT WHEN flgtab_curs%NOTFOUND;
	END LOOP;
	close flgtab_curs;
	RETURN (rtrim(rc_txt,' '));
END;
/
