# 
#   ##    #####   #####
#  #  #   #    #  #    #
# #    #  #####   #####
# ######  #    #  #    #
# #    #  #    #  #    #
# #    #  #####   #####  Airport Technologies GmbH
#
# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/Makefile 1.3 2004/10/04 20:54:14SGT jim Exp  $
# Makefile for DBLIB
#
# 20020624: JIM: Regel fuer *.pc verallgemeinert
# 20020625 JIM:  avoid 'rm *' in clean
# 20021008 JIM: improved 'clean' of non existing targets
# 20021008 JIM: do not include .depends for target 'clean' targets
# 20021107 JIM: call $(MKDEP) instead of mkdep
# 20021125 JIM: improved 'clean' of non existing targets
# 20021125 JIM: binary files are stored in /ceda/SCM_Bins/<user>/<prj>/Obj
#               to avoid trouble with HP/NFS and improve performance
# 20031001 JIM: allow analyze of used .c-file created by Oracle precompiler
#               by renaming to .c.used
# 20041004 JIM: clean: remove temporary files "*.used"
#

# globale Definitionen fuer alle Makefiles
include $(PB)/Make/GlobalDefines

# Definition aller .c Dateien als Quellen fuer Lib-Objects:
STD_SOURCES=$(wildcard *.c)

# Definition aller .pc Dateien als Quellen fuer Oracle-Lib-Objects:
SPEC_SOURCES=$(wildcard *.pc)


# hier sollen die Binaries der Lib-Objects abgelegt werden:
# hier liegt mkdep:
ifeq "$(O)" ""
OBJDIR=tmp_$(MY_OS)
MKDEP= $(TS)/Bin_$(MY_OS)/mkdep
SPEC_DIR=./$(OBJDIR)
else
OBJDIR=$(O)/Dblib
MKDEP= $(L)/mkdep
SPEC_DIR=/Dblib
endif
VPATH= $(OBJDIR)

# Die Namen der Lib-Objects leiten sich aus den Namen der .c-Dateien ab.
# Hier wird eine Liste der Lib-Objects aufgebaut, indem von allen .c-Dateien
# das .c abgeschnitten wird und der Pfad '$(OBJDIR)/' hinzugefuegt wird:
DBLIB_OBJS= $(patsubst %,$(OBJDIR)/%,$(STD_SOURCES:.c=.o)) 
SPEC_OBJS= $(patsubst %,$(OBJDIR)/%,$(SPEC_SOURCES:.pc=.o))

ifdef OBJDIR
OBJS=$(wildcard $(OBJDIR)/*)
endif
DEPENDS=$(wildcard .depends $(L)/DBLIB.a)
TMP_USED=$(wildcard *.used)

# 1. Ziel und seine Abhaengigkeit
$L/DBLIB.a: $(OBJDIR) $(L) $(DBLIB_OBJS) $(SPEC_OBJS)

#include .depends

# Standard-Regel zum Make der DB Library-Objects
$(DBLIB_OBJS): $(OBJDIR)/%.o: %.c
	@echo "\n\n     $* ... \n"
	$(RCC) $(SYMBOLS) $(COMPILE) $(CFLAGS) -c $< -o $@ 2>&1
	@echo "\n     ... $* \n\n"
	@ar rv $L/DBLIB.a $@


	
# Standard-Regel zum Make der DB Library-Objects von Oracle
$(SPEC_OBJS): $(O)$(SPEC_DIR)/%.o: %.pc
	@echo "\n\n     $* ... \n"
	$(PROC) $(PCCFLAGS) iname=$<  2>&1
	$(RCC)  $(CFLAGS) $(COMPILE) $(SYMBOLS) -I$(PCCINC) -o $@ -c $*.c 2>&1
	@echo "\n     ... $* \n\n"
	@ar rv $L/DBLIB.a $@ 2>&1
	@mv $*.c $*.c.used 2>&1  # 20031001 JIM: allow analyze of used .c-file

# hier soll die Library erzeugt werden:
$(L):
	mkdir -p $(L)

clean:
# 20020625 JIM:  avoid 'rm *' in clean
ifneq "$(OBJS)" ""
	  @rm $(OBJDIR)/* 2>&1 >/dev/null 
endif   
ifneq "$(DEPENDS)" ""
	@rm $(DEPENDS) 2>&1 >/dev/null 
endif
ifneq "$(TMP_USED)" ""
	@rm $(TMP_USED) 2>&1 >/dev/null 
endif


dep:
	cd $(TS)/Mkdep ; $(MAKE) 
	$(MKDEP) *.c >.depends

.depends:
	$(MKDEP) *.c >.depends
          

ifneq ($(MAKECMDGOALS),clean)
  include .depends
endif

$(OBJDIR): /ceda/SCM_Bins
	@-mkdir -p $(OBJDIR) 2>&1

/ceda/SCM_Bins:
ifneq "$(LOGNAME)" "ceda"
	@echo == Das Verzeichnis /ceda/SCM_Bins existiert nicht. Diese Verzeichnis muss ==
	@echo == der User CEDA anlegen und fuer die Gruppe CEDA beschreibbar machen     ==
	@echo == Bitte Passwort fuer den User CEDA angeben:                             ==
	su - ceda -c "mkdir /ceda/SCM_Bins && chmod g+rwx /ceda/SCM_Bins"
else
	mkdir /ceda/SCM_Bins && chmod g+rwx /ceda/SCM_Bins
endif
