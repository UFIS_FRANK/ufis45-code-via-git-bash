CREATE OR REPLACE PACKAGE "PCK_URNODISPOSER" AS
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Package              - PACKAGE PCK_URNODISPOSER
--
--      Version               - 1.2.0
--
--      Written By            - Jurgen Hammerschmid
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Change Log
--
--      Name           Date               Version    Change
--      ----           ----------         -------    ------
--      Hammerschmid   20.06.2008         1.0.0      new
--      Hammerschmid   23.06.2008		  1.1.0      additional field records
--												     in urno_log included
--		Hammerschmid   05.02.2009         1.2.0      new parameter for writing in numtab
--		Hammerschmid   15.05.2009         1.3.0      table VALIDPLSQL to control usage
--                                                   of prc.. fun... etc.
--                                                   New function FUN_CODE_ISVALID
--                                                   Changes to initial an cyclic run. No
--                                                   change to fratab when error during
--                                                   collecting used urnos.
--	    Hammerschmid   16.08.2009         1.4.0		 PRC_USEURNOFROMFRATAB included for
--                                                   online switch on or off urnodisposer
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

	-- Public type declarations
	--type <TypeName> is <Datatype>;

	-- Public constant declarations
	--<ConstantName> constant <Datatype> := <Value>;

	-- Public variable declarations
	--<VariableName> <Datatype>;

	-- Public function and procedure declarations
	--function <FunctionName>(<Parameter> <Datatype>) return <Datatype>;
    gv_TableSpace				CONSTANT VARCHAR2(20) := 'CEDAURNO';
    gv_Security					CONSTANT NUMBER(10) := 10;
	gv_MaxUrno					CONSTANT NUMBER(10) := 2147483647;

	FUNCTION FUN_COLLECT_ALL_USED_URNOS
    RETURN INTEGER;

    PROCEDURE PRC_RUN_INITIAL		(p_WriteNumtab IN VARCHAR2 DEFAULT 'Y');
    PROCEDURE PRC_RUN_INITIALJOB	(p_WriteNumtab IN VARCHAR2 DEFAULT 'Y');

    PROCEDURE PRC_RUN_CYCLIC 	(p_DaysBehind	IN NUMBER);

	PROCEDURE PRC_CREATE_FREE_URNO_RANGE (p_Start	   IN NUMBER DEFAULT 0,
                                          p_End		   IN NUMBER DEFAULT 0);

	PROCEDURE PRC_USED_URNOS_FOR_TABLE( p_Table IN VARCHAR2);

	PROCEDURE PRC_SCANURNOJOB 	(p_HOUROFDAY 	IN VARCHAR2,
    						     p_EveryDays    IN NUMBER,
    							 p_DaysBehind	IN NUMBER);

	PROCEDURE PRC_SCANURNOJOBIMMEDIATE 	( p_DaysBehind	IN NUMBER);

    PROCEDURE PRC_RUNAFTEROTHERJOB	( p_Process IN VARCHAR2 );

    PROCEDURE PRC_USEURNOFROMFRATAB( p_OnOff IN VARCHAR2 DEFAULT 'N');

    FUNCTION FUN_GETURNO( p_Range 	IN 	NUMBER)
	RETURN VARCHAR2;

    FUNCTION FUN_RKEYWITHOUTURNO( p_Adjust IN CHAR DEFAULT 'N' )
    RETURN INTEGER;

    FUNCTION FUN_TRUNCATE_TABLE	(p_Table	IN VARCHAR2)
	RETURN INTEGER;

	FUNCTION FUN_JOBSRUNNING
	RETURN INTEGER;

	FUNCTION FUN_CREATEINDEXFORUSEDURNOS (p_Index 		IN VARCHAR2,
    			 					      p_Table 		IN VARCHAR2,
                                          p_Column		IN VARCHAR2,
                                          p_TableSpace	IN VARCHAR2)
    RETURN INTEGER;

	FUNCTION FUN_DROPINDEX (p_Index 	IN VARCHAR2)
    RETURN INTEGER;

    FUNCTION FUN_READURNOVALUEFROMNUMTAB
    RETURN NUMTAB.ACNU%TYPE;

    FUNCTION FUN_CODE_ISVALID (p_Function IN VALIDPLSQL.PCKPRCFUN%TYPE)
    RETURN INTEGER;


end PCK_URNODISPOSER;
/
CREATE OR REPLACE PACKAGE BODY "PCK_URNODISPOSER" AS
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Package Body      - PCK_URNOSCRABBER
--
--  Version           - 1.0.0
--
--  Written By        - Juergen Hammerschmid
--
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Change Log
--
--  Name           Date               Version    Change
--  ----           ----------         -------    ------
--  Hammerschmid   20.06.2008         1.1        new
--	Hammerschmid   05.02.2009         1.2.0      new parameter for writing in numtab
--	Hammerschmid   15.05.2009         1.3.0      table VALIDPLSQL to control usage
--                                               of prc.. fun... etc.
--                                               New function FUN_CODE_ISVALID
--                                               Changes to initial an cyclic run. No
--                                               change to fratab when error during
--                                               collecting used urnos
--  Hammerschmid   16.08.2009         1.4.0		 PRC_USEURNOFROMFRATAB included for
--                                               online stich on or off urnodisposer
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--      PROCEDURE              	   - PRC_RUN_INITIAL
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_RUN_INITIAL	(p_WriteNumtab IN VARCHAR2 DEFAULT 'Y')
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_RUN_INITIAL
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   05.09.2008         1.0.0      new
    --      Hammerschmid   06.12.2008         1.1.0      check used urnos during scan
    --		Hammerschmid   05.02.2009         1.2.0      new parameter for writing in numtab
    --      Hammerschmid   15.05.2009         1.3.0      Complete only if collecting URNOS
    --                                                   terminates successful
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Success	NUMBER := -1;
    v_SeqId		NUMBER := 0;
    v_Frst		NUMBER := 0;
    v_Rend		NUMBER := 0;
    v_Free		NUMBER := 0;
    v_Seid		Number := 0;
    v_Error     NUMBER := 0;

    v_UrnoStart NUMTAB.ACNU%TYPE := 0;
    v_UrnoEnd   NUMTAB.ACNU%TYPE := 0;

    v_UrnoRangeStart	NUMBER := 0;

    BEGIN
    	-- delete used_urnos

        v_Error := 1;
        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, 'DROP INDEX', SYSDATE, NULL, 'N');
		COMMIT;

		v_Success := FUN_DROPINDEX('URNO_USED');

        v_Error := 2;
        UPDATE TR_LOG SET P_END = SYSDATE
        WHERE SEQ_ID = v_SeqId;
        COMMIT;

        v_Error := 3;
        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, 'TRUNCATE FRATAB', SYSDATE, NULL, 'N');
		COMMIT;

        v_Success := FUN_TRUNCATE_TABLE('FRATAB');

        v_Error := 4;
        UPDATE TR_LOG SET P_END = SYSDATE
        WHERE SEQ_ID = v_SeqId;
        COMMIT;

        v_Error := 5;
        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, 'TRUNCATE USED_URNOS', SYSDATE, NULL, 'N');
		COMMIT;

        v_Success := FUN_TRUNCATE_TABLE('USED_URNOS');

        v_Error := 6;
        UPDATE TR_LOG SET P_END = SYSDATE
        WHERE SEQ_ID = v_SeqId;
        COMMIT;

        IF v_Success = -1 THEN

	    	-- because all two parameter are missing (default = 0)
	        -- the procedure will collect all urnos
	        -- from all tables instead to look for
	        -- used urnos in a range
	        v_Error := 7;

	        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
	        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
	                    VALUES (v_SeqId, 'COLLECT USED URNOS', SYSDATE, NULL, 'N');
			COMMIT;

			-- read actual free urno in numtab
			v_UrnoStart := FUN_READURNOVALUEFROMNUMTAB;

	    	v_Success := FUN_COLLECT_ALL_USED_URNOS;

	        v_Error := 8;
	        UPDATE TR_LOG SET P_END = SYSDATE
	        WHERE SEQ_ID = v_SeqId;
	        COMMIT;

            -- Do only the rest when colleting URNOS terminates successful
            IF v_Success = -1 THEN

                v_Error := 9;
                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                            VALUES (v_SeqId, 'CREATE INDEX', SYSDATE, NULL, 'N');
                COMMIT;

                v_Success := FUN_CREATEINDEXFORUSEDURNOS('URNO_USED', 'USED_URNOS', 'USEDURNO', gv_TableSpace);

                v_Error := 10;
                UPDATE TR_LOG SET P_END = SYSDATE
                WHERE SEQ_ID = v_SeqId;
                COMMIT;

                v_Error := 11;
                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                            VALUES (v_SeqId, 'CREATE FREE URNOS', SYSDATE, NULL, 'N');
                COMMIT;

                PRC_CREATE_FREE_URNO_RANGE();

                v_Error := 12;
                UPDATE TR_LOG SET P_END = SYSDATE
                WHERE SEQ_ID = v_SeqId;
                COMMIT;

                -- read actual free urno in numtab and add an
                -- offset for the time between reading and
                -- writing numtab
                v_UrnoEnd := FUN_READURNOVALUEFROMNUMTAB + 1000;

                v_Error := 13;
                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
                            VALUES (v_SeqId, 'URNOS USED DURING SCAN Start=' || TO_CHAR(v_UrnoStart) ||
                                    ' End=' || TO_CHAR(v_UrnoEnd),
                                    SYSDATE, SYSDATE,
                                    'REC=' || TO_CHAR(v_UrnoEnd - v_UrnoStart), 'N');
                COMMIT;

                v_Error := 14;
                SELECT SEID, FRST, REND INTO v_Seid, v_Frst, v_Rend
                FROM FRATAB
                WHERE FREE = (SELECT MAX(FREE) FROM FRATAB);

                -- reserve biggest free range for ceda compatibility
                IF v_UrnoEnd BETWEEN v_Frst AND v_Rend THEN
                    v_Frst := v_UrnoEnd;
                    v_Free := v_Rend - v_UrnoEnd - 1;
                END IF;

                v_Error := 15;
                UPDATE FRATAB SET FRST = v_Frst,
                                  REND = v_Rend,
                                  FREE = v_Free,
                                  LSTU = '20990101000000'
                WHERE SEID = v_Seid;
                COMMIT;

                -- and set NUMTAB
                v_Error := 16;
                IF p_WriteNumtab = 'Y' THEN
                    UPDATE NUMTAB
                    SET ACNU = v_Frst, MAXN = v_Rend
                    WHERE TRIM(Keys) = 'SNOTAB';
                    COMMIT;
                END IF;

                -- block Urnorange during scan
                v_Error := 17;

                v_UrnoRangeStart := v_UrnoEnd - 3000000;
                IF v_UrnoRangeStart < 0 THEN
                    v_UrnoRangeStart := 0;
                END IF;

                UPDATE FRATAB SET FREE = 0
                WHERE FRST BETWEEN v_UrnoRangeStart AND v_UrnoEnd;
                COMMIT;
            END IF;

		END IF;

	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_RUN INITIAL ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150) || ' Error = ' || TO_CHAR(v_Error), 1);
    END PRC_RUN_INITIAL;

-------------------------------------------------------------------------------------------
--      PROCEDURE              	   - PRC_RUN_INITIALJOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_RUN_INITIALJOB	(p_WriteNumtab IN VARCHAR2 DEFAULT 'Y')
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_RUN_INITIALJOB
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   10.09.2008         1.0.0      new
    --		Hammerschmid   05.02.2009         1.1.0      new parameter for writing in numtab
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	jobno		NUMBER;

	BEGIN
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        --RUN_INITIALJOB
	    v_SEQ_ID	TR_LOG.SEQ_ID%TYPE;
        v_OtherJobsRunning 	INTEGER := -1;
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
        VALUES (v_Seq_ID,''PRC_SCANURNOINITIALJOB'',sysdate,null,''JOB'',''R'');
        COMMIT;
        v_OtherJobsRunning := PCK_URNODISPOSER.FUN_JOBSRUNNING;
        IF v_OtherJobsRunning = -1 THEN
	    PCK_URNODISPOSER.PRC_RUN_INITIAL(''' || p_WriteNumtab || ''');
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''N'' WHERE seq_id = v_Seq_ID;
        ELSE
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''D'' WHERE seq_id = v_Seq_ID;
		PCK_URNODISPOSER.PRC_RUNAFTEROTHERJOB( ''PRC_RUN_INITIAL'' );
        END IF;
        COMMIT;
		END;', SYSDATE, NULL, FALSE);
        COMMIT;
	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_RUN INITIALJOB ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150) || ' WHEN OTHERS', 1);
    END PRC_RUN_INITIALJOB;


-------------------------------------------------------------------------------------------
--      PROCEDURE              	   - PRC_RUN_CYCLIC
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_RUN_CYCLIC	(p_DaysBehind IN NUMBER)
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_RUN_CYCLIC
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   05.09.2008         1.0.0      new
    --      Hammerschmid   15.05.2009         1.1.0      Complete only if collecting URNOS
    --                                                   terminates successful
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE FRATabType         IS REF CURSOR;
    v_FRATab_cur            FRATABType;
    r_FRATab                FRATAB%ROWTYPE;

	v_Success				NUMBER := -1;
    v_SeqId					NUMBER := 0;
    v_SQLString				VARCHAR2(512);
    v_Error     			NUMBER := 0;

    BEGIN

    	-- delete used_urnos

        v_Error := 1;

        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, 'DROP INDEX', SYSDATE, NULL, 'N');
		COMMIT;

		v_Error := FUN_DROPINDEX('URNO_USED');

        UPDATE TR_LOG SET P_END = SYSDATE
        WHERE SEQ_ID = v_SeqId;
        COMMIT;

        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, 'TRUNCATE USED_URNOS', SYSDATE, NULL, 'N');
		COMMIT;

        v_Error := FUN_TRUNCATE_TABLE('USED_URNOS');

        UPDATE TR_LOG SET P_END = SYSDATE
        WHERE SEQ_ID = v_SeqId;
        COMMIT;

        IF v_Error = -1 THEN
	        v_Error := 2;

			-- scan all used urnos
            SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
            INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                        VALUES (v_SeqId, 'COLLECT USED URNOS', SYSDATE, NULL, 'N');
			COMMIT;

            v_Success := FUN_COLLECT_ALL_USED_URNOS;

	        UPDATE TR_LOG SET P_END = SYSDATE
	        WHERE SEQ_ID = v_SeqId;
	        COMMIT;

            -- Do only the rest when colleting URNOS terminates successful
            IF v_Success = -1 THEN

                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                            VALUES (v_SeqId, 'CREATE INDEX', SYSDATE, NULL, 'N');
                COMMIT;

                v_Success := FUN_CREATEINDEXFORUSEDURNOS('URNO_USED', 'USED_URNOS', 'USEDURNO', gv_TableSpace);

                UPDATE TR_LOG SET P_END = SYSDATE
                WHERE SEQ_ID = v_SeqId;
                COMMIT;

                -- only check records where range (free = 0) may be used
                v_SQLString := 'SELECT * FROM FRATAB ' ||
                               'WHERE FREE = 0 ' ||
                               'AND TO_DATE(SUBSTR(LSTU,1,8),''YYYYMMDD'') <= TRUNC(SYSDATE - ' || p_DaysBehind || ')';

                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                            VALUES (v_SeqId, 'START CHECK FREE = 0', SYSDATE, NULL, 'N');
                COMMIT;

                OPEN v_FRATab_cur FOR v_SQLString;
                LOOP
                    -- Process data
                    FETCH v_FRATab_cur INTO r_FRATab;
                    EXIT WHEN v_FRATab_cur%NOTFOUND OR
                              v_FRATab_cur%NOTFOUND IS NULL;

                    BEGIN
                        PRC_CREATE_FREE_URNO_RANGE(r_FRATAB.FRST, r_FRATAB.ACTL);
                    EXCEPTION
                        WHEN OTHERS THEN
                            ROLLBACK;
                                PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_RUN CYCLIC ', 'URNODISPOSER-80000',
                                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                    SUBSTR(SQLERRM, 1, 150) || ' Error = ' || TO_CHAR(v_Error), 1);
                    END;
                END LOOP;

                UPDATE TR_LOG SET P_END = SYSDATE
                WHERE SEQ_ID = v_SeqId;
                COMMIT;

                BEGIN
                    v_Error := 4;
                    -- clean up FRATAB
                    SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;
                    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                                VALUES (v_SeqId, 'CLEAN UP FRATAB', SYSDATE, NULL, 'N');
                    COMMIT;

                    DELETE FRATAB WHERE FREE = 0;
                    COMMIT;

                    UPDATE TR_LOG SET P_END = SYSDATE
                    WHERE SEQ_ID = v_SeqId;
                    COMMIT;

                EXCEPTION
                    WHEN OTHERS THEN
                        ROLLBACK;
                            PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_RUN CYCLIC ', 'URNODISPOSER-80000',
                                'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                SUBSTR(SQLERRM, 1, 150) || ' Error = ' || TO_CHAR(v_Error), 1);
                END;
            END IF;
		END IF;

	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			IF v_FRATab_cur%ISOPEN = TRUE THEN
				CLOSE v_FRATab_cur;
			END IF;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_RUN CYCLIC ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150) || ' Error = ' || TO_CHAR(v_Error), 1);
    END PRC_RUN_CYCLIC;

-------------------------------------------------------------------------------------------
--      FUNCTION              - FUN_COLLECT_ALL_USED_URNOS
-------------------------------------------------------------------------------------------
	FUNCTION FUN_COLLECT_ALL_USED_URNOS
    RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_COLLECT_ALL_USED_URNOS
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   20.06.2008         1.0.0      new
    --      Hammerschmid   23.06.2008		  1.1.0      additional field records
    --												     in urno_log included
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE TableType        	IS REF CURSOR;
    v_Table_cur           	TableType;
	v_SeqId					NUMBER;
    v_Records				NUMBER;
    v_Table					USER_TAB_COLUMNS.TABLE_NAME%TYPE;
    v_SQLString				VARCHAR2(512);

    v_TimeStart				DATE := SYSDATE;

	v_Success				INTEGER := -1;

    BEGIN
        v_SQLString := 'SELECT TABLE_NAME ' ||
        			   'FROM USER_TAB_COLUMNS ' ||
                       'WHERE (TABLE_NAME LIKE ' || '''___TAB''' || ' ' ||
                       'OR TABLE_NAME LIKE ' || '''ARCH____TAB''' || ') ' ||
                       'AND COLUMN_NAME = ' || '''URNO''' || ' ' ||
                       'AND TABLE_NAME NOT IN ( ' ||
                       'SELECT TRIM(KEYS) FROM NUMTAB ' ||
                       'WHERE (TRIM(KEYS) IS NOT NULL ' ||
                       'AND (TRIM(KEYS) LIKE ' || '''___TAB''' || ' ' ||
					   'OR TRIM(KEYS) LIKE ' || '''ARCH____TAB''' || '))) ' ||
                       'AND TABLE_NAME NOT IN ( ' ||
                       'SELECT VIEW_NAME FROM USER_VIEWS )' ||
                       'ORDER BY TABLE_NAME';

		-- mark maxurno
       	INSERT INTO USED_URNOS (USEDURNO)
           	VALUES (gv_MaxUrno);
        COMMIT;

    	OPEN v_Table_cur FOR v_SQLString;
        LOOP
        	FETCH v_Table_cur INTO v_Table;
            EXIT WHEN v_Table_cur%NOTFOUND OR
            		  v_Table_cur%NOTFOUND IS NULL;
            BEGIN

                -- start to log every table
                SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;

                INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                            VALUES (v_SeqId, v_Table, SYSDATE, NULL, 'N');
				COMMIT;

                EXECUTE IMMEDIATE
                'INSERT /*+ APPEND */ INTO USED_URNOS ' ||
                'SELECT TO_NUMBER(URNO) FROM ' || v_Table;
                v_Records := SQL%ROWCOUNT;

--                EXECUTE IMMEDIATE
--                'INSERT /*+ APPEND */ INTO USED_URNOS ' ||
--                'SELECT TO_NUMBER(URNO) ,''' || v_Table || ''' FROM ' || v_Table;
--                v_Records := SQL%ROWCOUNT;

                UPDATE TR_LOG SET P_END = SYSDATE, P_STATUS = 'REC=' || TO_CHAR(v_Records)
                WHERE SEQ_ID = v_SeqId;
                COMMIT;

            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    v_Success := 0;
					PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_COLLECT_ALL_USED_URNOS ', 'URNODISPOSER-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' problem to collect urnos from ' || v_table, 1);
            END;
        END LOOP;
        CLOSE v_Table_cur;
        RETURN v_Success;
	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
            v_Success := 0;
			IF v_Table_cur%ISOPEN = TRUE THEN
				CLOSE v_Table_cur;
			END IF;
            RETURN v_Success;
    END FUN_COLLECT_ALL_USED_URNOS;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_USED_URNOS_FOR_TABLE
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_USED_URNOS_FOR_TABLE( p_Table IN VARCHAR2)
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_USED_URNOS_FOR_TABLE
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   30.07.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_SeqId					NUMBER;
    v_Records				NUMBER;

    BEGIN
        SELECT TR_LOG_SEQ.Nextval INTO v_SeqId FROM DUAL;

        INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, ERROR_FLAG)
                    VALUES (v_SeqId, p_Table, SYSDATE, NULL, 'N');
		COMMIT;

        EXECUTE IMMEDIATE
        'INSERT INTO USED_URNOS ' ||
        'SELECT TO_NUMBER(URNO) FROM ' || p_Table;
        v_Records := SQL%ROWCOUNT;

        UPDATE TR_LOG SET P_END = SYSDATE, P_STATUS = 'REC=' || TO_CHAR(v_Records)
        WHERE SEQ_ID = v_SeqId;
        COMMIT;
	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_USED_URNOS_FOR_TABLE ', 'URNODISPOSER-80000',
                'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' problem to collect urnos from ' || p_table, 1);
    END PRC_USED_URNOS_FOR_TABLE;

------------------------------------------------------------------------------------------
--      Procedure              - PRC_CREATE_FREE_URNO_RANGE
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_CREATE_FREE_URNO_RANGE (p_Start	   IN NUMBER DEFAULT 0,
                                          p_End		   IN NUMBER DEFAULT 0)
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_CREATE_FREE_URNO_RANGE
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   20.06.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE UrnoType        	IS REF CURSOR;
    v_Urno_cur           	UrnoType;

	v_SQLString				VARCHAR2(512);
    v_OldUrno				NUMBER := 0;
    v_NewUrno				USED_URNOS.USEDURNO%TYPE := 0;
    v_DiffUrno				USED_URNOS.USEDURNO%TYPE := 0;
	v_Seqid					NUMBER := 0;
    v_Start					NUMBER := p_Start -1;
    v_End					NUMBER := p_End +1;

    BEGIN
    	IF p_Start = 0 AND p_End = 0 THEN
        	-- run initial
            v_SQLString := 'SELECT DISTINCT USEDURNO ' ||
                           'FROM USED_URNOS ' ||
                           'WHERE USEDURNO > 0 ' ||
                           'ORDER BY USEDURNO';
		ELSE
        	-- run cyclic jobs
            -- p_Start - 1 and p_End + 1 because
            -- p_Start is the first free Urno and
            -- p_End is the last free Urno
            v_SQLString := 'SELECT DISTINCT USEDURNO ' ||
                           'FROM USED_URNOS ' ||
                           'WHERE USEDURNO BETWEEN (' || v_Start || ') ' ||
                           'AND (' || v_End || ') ' ||
                           'ORDER BY USEDURNO';
            v_OldUrno := v_Start;
        END IF;

    	OPEN v_Urno_cur FOR v_SQLString;
        LOOP
        	FETCH v_Urno_cur INTO v_NewUrno;
            EXIT WHEN v_Urno_cur%NOTFOUND OR
            		  v_Urno_cur%NOTFOUND IS NULL;
        	BEGIN
            	IF v_NewUrno - v_OldUrno > 1 THEN
                	v_DiffUrno := v_NewUrno - v_OldUrno;
                    SELECT URNO_SEQ.Nextval INTO v_SeqId FROM DUAL;
              		INSERT INTO FRATAB
                    	FIELDS (SEID,
                        		FRST,
                                ACTL,
                                REND,
                                FREE,
                                LSTU)
                    	VALUES (v_SeqId,
                        		v_OldUrno + 1,
                                v_OldUrno + 1,
                                v_NewUrno - 1,
                                v_DiffUrno - 1,
                                TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'));
                  	COMMIT;
                END IF;
                v_OldUrno := v_NewUrno;
            EXCEPTION
            	WHEN OTHERS THEN
                	ROLLBACK;
					PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_CREATE_FREE_URNO_RANGE ', 'URNODISPOSER-80000',
                		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						SUBSTR(SQLERRM, 1, 150) || ' Insert into free urno range', 1);
            END;
        END LOOP;
        CLOSE v_Urno_cur;
    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_CREATE_FREE_URNO_RANGE ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
			IF v_Urno_cur%ISOPEN = TRUE THEN
				CLOSE v_Urno_cur;
			END IF;
    END PRC_CREATE_FREE_URNO_RANGE;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_SCANURNOJOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_SCANURNOJOB (p_HOUROFDAY 	IN VARCHAR2,
    						   p_EveryDays  IN NUMBER,
    						   p_DaysBehind	IN NUMBER)
	IS
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_SCANURNOJOB
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   09.09.2008         1.0.0      new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	v_NewHourOfDay	NUMBER(20);
	jobno			NUMBER;
   	v_Job_check		NUMBER := 0;

	BEGIN
		BEGIN
			-- check if job is already running
			SELECT job
    		INTO v_Job_check
			FROM SYS.user_Jobs
			WHERE what LIKE '%--SCAN_URNOJOB%';
    	EXCEPTION
    		WHEN NO_DATA_FOUND THEN
        		v_Job_check := 0;
	    	WHEN OTHERS THEN
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_SCANURNOJOB ', 'URNODISPOSER-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 9);
		END;
        -- Check if hour of day is in the past
        IF to_Number(to_char(sysdate , 'HH24')) > to_Number(p_HourOfDay) THEN
			v_NewHourOfDay := 1;
        ELSE
			v_NewHourOfDay := 0;
        END IF;
		IF v_Job_check <> 0 THEN
			--remove old job and create new
			dbms_job.remove(v_Job_check);
	    END IF;
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        --SCAN_URNOJOB
	    v_SEQ_ID	NUMBER(12);
        v_OtherJobsRunning 	INTEGER := -1;
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
        VALUES (v_Seq_ID,''PRC_SCANURNOJOB'',sysdate,null,''JOB'',''R'');
        COMMIT;
        v_OtherJobsRunning := PCK_URNODISPOSER.FUN_JOBSRUNNING;
        IF v_OtherJobsRunning = -1 THEN
	    PCK_URNODISPOSER.PRC_RUN_CYCLIC(' || p_DaysBehind || ');
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''N'' WHERE seq_id = v_Seq_ID;
        ELSE
		PCK_URNODISPOSER.PRC_RUNAFTEROTHERJOB(''PRC_RUN_CYCLIC(' || p_DaysBehind || ')'' );
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''D'' WHERE seq_id = v_Seq_ID;
        END IF;
        COMMIT;
		END;',
	   	TRUNC(SYSDATE) + p_EveryDays + v_NewHourOfDay + (p_HOUROFDAY/24),
		'TRUNC(SYSDATE) + ' || p_EveryDays || ' + (' || p_HOUROFDAY || '/24)',
		FALSE);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
    		ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_SCANURNOJOB ', 'URNODISPOSER-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 9);

	END PRC_SCANURNOJOB;

-------------------------------------------------------------------------------------------
--      PROCEDURE              	   - PRC_SCANURNOJOBIMMEDIATE
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_SCANURNOJOBIMMEDIATE 	( p_DaysBehind	IN NUMBER)
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_SCANURNOJOBIMMEDIAT
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   01.10.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	jobno		NUMBER;

	BEGIN

		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        --SCAN_URNO_JOBIMMEDIATE
	    v_SEQ_ID	NUMBER(12);
        v_OtherJobsRunning 	INTEGER := -1;
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
        VALUES (v_Seq_ID,''PRC_SCANURNOJOBIMMEDIATE'',sysdate,null,null,''R'');
        COMMIT;
        v_OtherJobsRunning := PCK_URNODISPOSER.FUN_JOBSRUNNING;
        IF v_OtherJobsRunning = -1 THEN
	    PCK_URNODISPOSER.PRC_RUN_CYCLIC(' || p_DaysBehind || ');
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''N'' WHERE seq_id = v_Seq_ID;
        ELSE
		PCK_URNODISPOSER.PRC_RUNAFTEROTHERJOB(''PRC_RUN_CYCLIC(' || p_DaysBehind || ')'' );
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''D'' WHERE seq_id = v_Seq_ID;
        END IF;
		END;', SYSDATE,	NULL, FALSE);
		COMMIT;

	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_SCANURNOJOBIMMEDIATE ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150) || ' WHEN OTHERS', 1);
    END PRC_SCANURNOJOBIMMEDIATE;

-------------------------------------------------------------------------------------------
--      PROCEDURE              	   - PRC_RUNAFTEROTHERJOBE
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_RUNAFTEROTHERJOB( p_Process IN VARCHAR2 )
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_RUNAFTEROTHERJOB
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   11.01.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	jobno		NUMBER;

	BEGIN
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        --RUN_AFTEROTHERJOB
	    v_SEQ_ID	NUMBER(12);
        v_Self		NUMBER;
        v_OtherJobsRunning 	INTEGER := -1;
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
        VALUES (v_Seq_ID,''PRC_RUNAFTEROTHERJOB'',sysdate,null,''JOB'',''R'');
        COMMIT;
		SELECT job
    	INTO v_Self
		FROM SYS.user_Jobs
		WHERE UPPER(WHAT) LIKE ''%--RUN_AFTEROTHERJOB%'';
        v_OtherJobsRunning := PCK_URNODISPOSER.FUN_JOBSRUNNING;
        IF v_OtherJobsRunning = -1 THEN
	    PCK_URNODISPOSER.' || p_Process || ';
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''N'' WHERE seq_id = v_Seq_ID;
        DBMS_JOB.REMOVE( v_Self );
        ELSE
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''D'' WHERE seq_id = v_Seq_ID;
        END IF;
        COMMIT;
		END;', SYSDATE, 'SYSDATE + 60/86400', FALSE);
		COMMIT;

	EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_SCANURNOJOBIMMEDIATE ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150) || ' WHEN OTHERS', 1);
    END PRC_RUNAFTEROTHERJOB;

-------------------------------------------------------------------------------------------
--      Function              - FUN_JOBSRUNNING
-------------------------------------------------------------------------------------------
	FUNCTION FUN_JOBSRUNNING
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_JOBSRUNNING
    --
    --      Version               - 1.0.0
    --
    --      Return                - 1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   10.01.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_RunningJobs
    IS
    	SELECT *
        FROM TR_LOG
        WHERE ERROR_FLAG = 'R'
        AND TRIM(PROCESS) NOT IN ('PRC_SCANURNOJOB',
                                  'PRC_SCANURNOJOBIMMEDIATE',
                                  'PRC_SCANURNOINITIALJOB')
        AND P_END IS NULL;

	r_RunningJobs			c_RunningJobs%ROWTYPE;
    v_RunningJobs			NUMBER(22) := -1;
    v_End					BOOLEAN := FALSE;

	BEGIN
		FOR r_RunningJobs IN c_RunningJobs
		LOOP
        	BEGIN
            	-- Is this running job a job which
                -- should run till end without interrupts?
            	IF TRIM(r_RunningJobs.Process) = 'HOUSEKEEPING_JOB' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
/*
                ELSIF TRIM(r_RunningJobs.Process) = 'PRC_SCANURNOINITIALJOB' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
*/
                END IF;

                IF v_End = TRUE THEN
					pck_ceda_errors.prc_write_error('FUN_JOBSRUNNING TR_LOG SEQ_ID = ' ||
                    								TO_CHAR(r_RunningJobs.Seq_Id), 'URNODISPOSER-80000',
                   									'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   									SUBSTR(SQLERRM, 1, 150), 1);
                    EXIT;
                END IF;
            EXCEPTION
            	WHEN OTHERS THEN
            		v_RunningJobs := 0;
            END;
    	END LOOP;

		RETURN v_RunningJobs;

    EXCEPTION
    	WHEN OTHERS THEN
       		v_RunningJobs := 0;
			RETURN v_RunningJobs;

    END FUN_JOBSRUNNING;

-------------------------------------------------------------------------------------------
--      Function              - FUN_RKEYWITHOUTURNO
-------------------------------------------------------------------------------------------
    FUNCTION FUN_RKEYWITHOUTURNO( p_Adjust IN CHAR DEFAULT 'N' )
    RETURN INTEGER
    IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_RKEYWITHOUTURNO
    --
    --      Version               - 1.0.0
    --
    --      Return                - RecordsFound
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   11.05.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_RKEYWithoutUrno
    IS
    	SELECT URNO, AURN, RKEY,
        	   ADID, ORG3, DES3,
               HOPO, RTYP, FLNO,
               CDAT, USEC, CSGN, REGN
		FROM AFTTAB a
		WHERE RKEY NOT IN ( SELECT URNO
        					FROM AFTTAB
                            WHERE URNO = a.RKEY )
        ORDER BY RKEY;

    r_RKEYWithoutUrno	c_RKEYWithoutUrno%ROWTYPE;

	v_RecordsFound		INTEGER := 0;
    v_RKeysFound		INTEGER := 0;
    v_OldUrno			r_RKEYWithoutUrno.Urno%TYPE;

    v_Test				INTEGER := 0;

	BEGIN
    	OPEN c_RKEYWithoutUrno;
        LOOP
        	FETCH c_RKEYWithoutUrno INTO r_RKEYWithoutUrno;
                  EXIT WHEN c_RKEYWithoutUrno%NOTFOUND OR
                            c_RKEYWithoutUrno%NOTFOUND IS NULL;
           	v_RecordsFound := v_RecordsFound + 1;

            IF v_RecordsFound = 1 THEN
            	v_OldUrno := r_RKEYWithoutUrno.Urno;
                v_RKeysFound := 1;
            ELSE
            	IF v_OldUrno <> r_RKEYWithoutUrno.Urno THEN
                    IF (UPPER(p_Adjust) = 'Y' OR UPPER(p_Adjust) = 'J')
                        AND v_RKeysFound = 1 THEN
                        BEGIN
                            UPDATE AFTTAB SET USEU = 'JHA' WHERE URNO = v_OldUrno;
                            COMMIT;

/*
                            UPDATE AFTTAB SET RKEY = v_OldUrno,
                                              AURN = ' ',
                                              RTYP = 'S',
                                              LSTU = TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'),
                                              USEU = 'URNO_DISP'
                            WHERE URNO = v_OldUrno;
                            COMMIT;
*/
                        EXCEPTION
                            WHEN OTHERS THEN
                                ROLLBACK;
                                PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_RKEYWITHOUTURNO ', 'URNODISPOSER-80001',
                                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                    SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
                        END;
                    END IF;

                    v_OldUrno := r_RKEYWithoutUrno.Urno;
                    v_RKeysFound := 1;
                ELSE
                	v_RKeysFound := v_RKeysFound + 1;
                END IF;
            END IF;
        END LOOP;

        -- Last record to correct
        IF (UPPER(p_Adjust) = 'Y' OR UPPER(p_Adjust) = 'J')
            AND v_RKeysFound = 1 THEN
			BEGIN
            UPDATE AFTTAB SET USEU = 'JHALAST' WHERE URNO = v_OldUrno;
            COMMIT;
/*
                UPDATE AFTTAB SET RKEY = v_OldUrno,
                                  AURN = ' ',
                                  RTYP = 'S',
                                  LSTU = TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'),
                                  USEU = 'URNO_DISP'
                WHERE URNO = v_OldUrno;
                COMMIT;
*/
                v_Test := v_Test + 1;
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_RKEYWITHOUTURNO ', 'URNODISPOSER-80001',
                        'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                        SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
            END;
        END IF;

        RETURN v_RecordsFound;

    EXCEPTION
    	WHEN OTHERS THEN
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_RKEYWITHOUTURNO ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
            RETURN TO_CHAR(v_RecordsFound);

	END FUN_RKEYWITHOUTURNO;
------------------------------------------------------------------------------------------
--      Function              - FUN_GETURNO
-------------------------------------------------------------------------------------------
	FUNCTION FUN_GETURNO ( p_Range	IN 	NUMBER)
    RETURN VARCHAR2
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_GETURNO
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   02.09.2008         1.0.0      new
    --      Hammerschmid   08.05.2009         1.1.0      switch Unrodisposer on/off
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    CURSOR c_GetUrno
    IS
    	SELECT *
        FROM FRATAB
        WHERE FREE >= p_Range
        AND LSTU <> '20990101000000'
        AND ROWNUM = 1
        FOR UPDATE;

    r_FRATAB            FRATAB%ROWTYPE;

    v_StartRange		NUMBER := 0;
	v_End				NUMBER := 0;
   	v_Job_check			NUMBER := 0;

    -- this option is mandatory if you will
    -- use something like this:
    -- select pck_urnodisposer.fun_getUrno(5)
    -- to get the first of 5 URNOS
	PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
        v_StartRange := 0;
		IF FUN_CODE_ISVALID('PCK_URNODISPOSER.FUN_GETURNO') = -1 THEN
            BEGIN
                -- check if initial job is running
                SELECT job
                INTO v_Job_check
                FROM SYS.user_Jobs
                WHERE what LIKE '%--RUN_INITIALJOB%';
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_Job_check := 0;
                WHEN OTHERS THEN
                    PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_GETURNO ', 'URNODISPOSER-80000',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                               SUBSTR(SQLERRM, 1, 150), 9);
            END;

            IF v_Job_check = 0 THEN

                OPEN c_GetUrno;
                LOOP
                    FETCH c_GetUrno INTO r_FRATAB;
                        EXIT WHEN 	c_GetUrno%NOTFOUND OR
                                    c_GetUrno%NOTFOUND IS NULL;
                    v_StartRange := r_FRATAB.ACTL;
                    v_End := r_FRATAB.ACTL + p_Range;
                    -- just for cosmetic reasons otherwise
                    -- r_FRATAB.actl can be > than r_FRATAB.Rend
                    -- this will happen because r_FRATAB.Actl is the
                    -- first valid URNO and r_FRATAB.Rend the last
                    -- available URNO in  the range
                    IF v_End > r_FRATAB.REND THEN
                        v_End := r_FRATAB.REND;
                    END IF;
                    UPDATE FRATAB SET ACTL = v_End,
                                      FREE = r_FRATAB.FREE - p_Range,
                                      LSTU = TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')
                    WHERE CURRENT OF c_GetUrno;
                END LOOP;

                COMMIT;
                CLOSE c_GetUrno;

            END IF;
        END IF;

        RETURN TO_CHAR(v_StartRange);

    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
            v_StartRange := 0;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_GETURNO ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
            RETURN TO_CHAR(v_StartRange);
    END FUN_GETURNO;

-------------------------------------------------------------------------------------------
--      FUNCTION              - FUN_READURNOVALUEFROMNUMTABS
-------------------------------------------------------------------------------------------
	FUNCTION FUN_READURNOVALUEFROMNUMTAB
    RETURN NUMTAB.ACNU%TYPE
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_READURNOVALUEFROMNUMTAB
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                -
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   05.12.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_ActualUrnoValue		NUMTAB.ACNU%TYPE := 0;

    BEGIN
    	SELECT ACNU INTO v_ActualUrnoValue FROM NUMTAB WHERE TRIM(KEYS) = 'SNOTAB';
        RETURN v_ActualUrnoValue;
	EXCEPTION
    	WHEN OTHERS THEN
            v_ActualUrnoValue := 0;
            RETURN v_ActualUrnoValue;
    END FUN_READURNOVALUEFROMNUMTAB;

------------------------------------------------------------------------------------------
--      Function                 - FUN_TRUNCATE_TABLE (p_Tabble IN VARCHAR2)
-------------------------------------------------------------------------------------------
	FUNCTION FUN_TRUNCATE_TABLE (p_Table IN VARCHAR2)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_TRUNCATE_USED_URNOS
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   17.09.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;

	PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
    	EXECUTE IMMEDIATE
        'TRUNCATE TABLE ' || p_Table;
        COMMIT;
    	RETURN v_Success;
    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
    		v_Success := 0;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_TRUNCATE_TABLE ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
			RETURN v_Success;
    END FUN_TRUNCATE_TABLE;

------------------------------------------------------------------------------------------
--      Function                 - FUN_CREATEINDEXFORUSEDURNOS
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CREATEINDEXFORUSEDURNOS (p_Index 		IN VARCHAR2,
    			 					      p_Table 		IN VARCHAR2,
                                          p_Column		IN VARCHAR2,
                                          p_TableSpace	IN VARCHAR2)
    RETURN INTEGER
    IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CREATEINDEXFORUSEDURNOS
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   23.09.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;
    v_SQLString 		VARCHAR(512);

	PRAGMA AUTONOMOUS_TRANSACTION;
	--tablespace URNO

    BEGIN
		v_SQLString :=  'create index ' || p_Index || ' on ' || p_Table || ' ( ' || p_Column || ' ) ' ||
						'tablespace ' || p_TableSpace || ' ' ||
  						'pctfree 10 ' ||
  						'initrans 2 ' ||
  						'maxtrans 255 ' ||
  						'storage ' ||
				  		'( ' ||
    						'initial 1M ' ||
    						'next 1M ' ||
    						'minextents 1 ' ||
    						'maxextents unlimited ' ||
    						'pctincrease 0 ' ||
  						')';

    	EXECUTE IMMEDIATE v_SQLString;
        RETURN v_Success;

    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
    		v_Success := 0;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_CREATEINDEXFORUSEDURNOS ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
			RETURN v_Success;
    END FUN_CREATEINDEXFORUSEDURNOS;

------------------------------------------------------------------------------------------
--      Function                 - FUN_DROPINDEX (p_Index IN VARCHAR2)
-------------------------------------------------------------------------------------------
	FUNCTION FUN_DROPINDEX (p_Index 		IN VARCHAR2)
    RETURN INTEGER
    IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_DROPINDEX
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   23.09.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;
    v_SQLString 		VARCHAR(512);

	PRAGMA AUTONOMOUS_TRANSACTION;
	--tablespace URNO

    BEGIN
		v_SQLString :=  'DROP INDEX ' || p_Index;

    	EXECUTE IMMEDIATE v_SQLString;
        RETURN v_Success;

    EXCEPTION
    	WHEN OTHERS THEN
        	IF SUBSTR(SQLERRM,1,9) = 'ORA-01418' THEN
            	-- index already dropped;
            	RETURN v_Success;
            ELSE
                ROLLBACK;
                v_Success := 0;
                PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_DROPINDEX ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                    SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
                RETURN v_Success;
            END IF;
    END FUN_DROPINDEX;

------------------------------------------------------------------------------------------
--      Procedure                 - PRC_USEURNOFROMFRATAB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_USEURNOFROMFRATAB( p_OnOff IN VARCHAR2 DEFAULT 'N')
    IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - PROCEDURE PRC_SETFRATAB
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   16.09.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	BEGIN
    	IF p_OnOff = 'Y' OR
           p_OnOff = 'J' THEN
        	UPDATE VALIDPLSQL SET VALID = 'Y' WHERE PCKPRCFUN = 'PCK_URNODISPOSER.FUN_GETURNO';
        ELSE
        	UPDATE VALIDPLSQL SET VALID = 'N' WHERE PCKPRCFUN = 'PCK_URNODISPOSER.FUN_GETURNO';
        END IF;
        COMMIT;
    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
            PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_USEURNOFROMFRATAB ', 'URNODISPOSER-80000',
                    'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                    SUBSTR(SQLERRM, 1, 150) || ' when others', 1);

    END PRC_USEURNOFROMFRATAB;

------------------------------------------------------------------------------------------
--      Function                 - FUN_CODE_ISVALID
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CODE_ISVALID(p_Function IN VALIDPLSQL.PCKPRCFUN%TYPE)
    RETURN INTEGER
    IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CODE_ISVALID
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   30.04.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;
    v_Valid				VALIDPLSQL.VALID%TYPE := 'Y';

    BEGIN
    	SELECT VALID
        INTO v_Valid
        FROM VALIDPLSQL
        WHERE UPPER(PCKPRCFUN) = UPPER(p_Function);

		IF v_Valid <> 'Y' THEN
        	v_Success := 0;
        ELSE
        	v_Success := -1;
        END IF;

        RETURN v_Success;

    EXCEPTION
    	WHEN OTHERS THEN
        	v_Success := 0;
           	RETURN v_Success;
    END FUN_CODE_ISVALID;

END PCK_URNODISPOSER;
/
