#ifndef _DEF_mks_version_infofkt_h
  #define _DEF_mks_version_infofkt_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_infofkt_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/infofkt.h 1.2 2004/07/27 16:47:53SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*******************************************************************/
/*******************************************************************/
/*******************************************************************/
/*	<infoffkt.h>	Definitions and structures for OINFORMIXfunc  */

#ifndef INFOFKT_INC
#define INFOFKT_INC

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "glbdef.h"
#include "flddef.h"


#ifdef CCSDB_INFORMIX

/* informix does not support arrays */
#ifdef ARRAY
#undef ARRAY
#define ARRAY 0
#endif

/* following the configuration defines 			*/
/* Max field size in INFORMIX */
#define		MAX_FLD_SIZE		32000	 /* usaed for data_area[] */
#define		SQL_BUFSIZE		(8*1024) /* used for sql_buf[]    */

#ifndef DEBUG
#define DEBUG 1
#endif

/* following the functional defines for db_if		*/
#define		IGNORE			NOACTION

/* following the internal data type defines		*/
#define		BYTE			1
#define		CHAR			2
#define		SHORT			3
#define		INT			4
#define		LONG			5
#define		RAW			6
#define		STR                     7
#define		LONGRAW			8

#define		BYTE_SIZE		1
#define		CHAR_SIZE		sizeof(char)
#define		SHORT_SIZE		sizeof(short)
#define		INT_SIZE		sizeof(int)
#define		LONG_SIZE		sizeof(long)
#define		RAW_SIZE		BYTE_SIZE
#define		STRING_SIZE		BYTE_SIZE

/* following the return + error code defines			*/
#define		DB_SUCCESS		RC_SUCCESS
#define		DB_ERROR		RC_FAIL
#define		NOTFOUND		1
#define		SQL_NOTFOUND		NOTFOUND

#ifndef END
#define		END			-9999
#endif
#define		INVALID_RC		-9998

int init_db(void);
int commit_work(void);
int create(int);
int exec_sql(short ,char *, char *,short *);
void get_fld(char *,short ,short ,int ,char *);
char *calc_adr(char *,short );
void form_hex_buf();
void form_char_buf();
char hex_to_byte();
int sql_if(short ,short* ,char* ,char* );
extern mtox();

#endif
#endif
