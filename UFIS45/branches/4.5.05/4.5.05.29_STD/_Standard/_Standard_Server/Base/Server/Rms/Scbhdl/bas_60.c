/***********************************/
/* Account 60: night working hours */
/***********************************/
static void bas_60()
{
	char 	clLastCommand[4];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clDrrSbfr[16];
	char	clDrrSblu[16];
	char	clOldDrrSbfr[16];
	char	clOldDrrSblu[16];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clYear[6];
	char	clMonth[4];
	char	clIsBsd[2];
	char	clIsLikeWork[2];
	char	clCl12Acc61[12];

	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	double	dlNewAccValue[12];
	int	i;

	dbg(DEBUG, "============================= ACCOUNT 60 START ===========================");

	/* checking command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 60: Command (%s)!",clLastCommand);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 60: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 60: Stopped, because <clSday> = undef!");
		return;
	}

	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 60: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 60: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* break start */
	if (ReadTemporaryField("NewSbfr",clDrrSbfr) <= 0)
	{
		dbg(TRACE, "Account 60: <clDrrSbfr> = undef!");
		strcpy(clDrrSbfr,"");
	}

	/* break duration */
	if (ReadTemporaryField("NewSblu",clDrrSblu) <= 0)
	{
		dbg(TRACE, "Account 60: <clDrrSblu> = undef!");
		strcpy(clDrrSblu,"0");
	}

	/* get old data */
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			dbg(TRACE, "Account 60: <clOldDrrStartTime> = undef!");
			strcpy(clOldDrrStartTime,"0");
		}
	
		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			dbg(TRACE, "Account 60: <clOldDrrEndTime> = undef!");
			strcpy(clOldDrrEndTime,"0");
		}
	
		/* old break start */
		if (ReadTemporaryField("OldSbfr",clOldDrrSbfr) <= 0)
		{
			strcpy(clOldDrrSbfr,"");
		}

		/* old break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrSblu) <= 0)
		{
			strcpy(clOldDrrSblu,"");
		}
	}
		
	/* calculate old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		ReadTemporaryField("OldIsBsd",clIsBsd);
		ReadTemporaryField("IsOldLikeWork",clIsLikeWork);
		dlOldValue = CalcNightWorkTime(clOldDrrStartTime,clOldDrrEndTime,clIsBsd,clIsLikeWork);
		if (dlOldValue > 0)
		{
			dlOldValue = dlOldValue - CalcBreakNightWorkTime(clOldDrrSbfr,clOldDrrSblu,clSday);
		}
		dbg(DEBUG,"Account 60: Old Value <dlOldValue> = <%lf>" , dlOldValue);
	}

	/* calculate new value */
	dlNewValue = 0;
	ReadTemporaryField("NewIsBsd",clIsBsd);
	ReadTemporaryField("IsNewLikeWork",clIsLikeWork);
	dlNewValue = CalcNightWorkTime(clDrrStartTime,clDrrEndTime,clIsBsd,clIsLikeWork);
	if (dlNewValue > 0)
	{
		dlNewValue = dlNewValue - CalcBreakNightWorkTime(clDrrSbfr,clDrrSblu,clSday);
	}
	dbg(DEBUG,"Account 60: New Value <dlNewValue> = <%lf>" , dlNewValue);

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue;
	}

	/* get ACC record */
	strncpy(clYear,clSday,4);
	clYear[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"60",clYear,dlNewAccValue);

	strncpy(clMonth,&clSday[4],2);
	clMonth[2] = '\0';

	dbg(DEBUG,"Account 60: Month Index <clMonth> = <%s>",clMonth);
	dbg(DEBUG,"Account 60: Old ACC  value: <%lf>",dlNewAccValue[atoi(clMonth)-1]);
	dbg(DEBUG,"Account 60: Result Value <dlResultValue> = <%lf>",dlResultValue);

	if  (dlResultValue == 0)
	{
		dbg(DEBUG,"Account 60: Value not changed.");
		return;
	}

	i = atoi(clMonth);

	while (i <= 12)
	{
		dlNewAccValue[i-1] = dlNewAccValue[i-1] + dlResultValue;
		dbg(DEBUG,"Account 60: New ACC value / month [%d] = <%lf>",i,dlNewAccValue[i-1]);
		i = i + 1;
	}

	/* save results */
	UpdateMultiAccountHandle("60",clStaffUrno,clYear,"SCBHDL",dlNewAccValue);
	if (GetAccValueHandle("61","12",clYear,clStaffUrno,"CL",clCl12Acc61))
	{
		dbg(DEBUG,"Account 60: CL-value account 60 = <%lf>",dlNewAccValue[11]);
		dbg(DEBUG,"Account 60: CL-value account 61 = <%lf>",atof(clCl12Acc61));
		dbg(DEBUG,"Account 60: Calling UpdateNextYearsValuesHandleAdjust() ...");
		UpdateNextYearsValuesHandleAdjust(clYear,clStaffUrno,dlNewAccValue[11],atof(clCl12Acc61),"60","61");
	} 
}
