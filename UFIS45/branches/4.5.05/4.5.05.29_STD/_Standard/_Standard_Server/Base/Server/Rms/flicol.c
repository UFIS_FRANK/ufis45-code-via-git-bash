#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/flicol.c 1.8 2008/10/01 17:02:18SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                 MCU 2001/02/26 Command CMD_CFM implemented,sends flights   */
/*                 to rulfnd at DCS updates                                   */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS 4.5 (c) ABB AAT/I flicol.c 4.5.1.2 / 2003/07/07 / HAG";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>

#define FLIGHTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgFlightArray.plrArrayFieldOfs[ipFieldNo-1]])

extern int get_no_of_items(char *s);

#define RUL_FLD_LEN	(4)
#define MAXRUES (120)

#define EVT_TURNAROUND	(0)
#define EVT_INBOUND			(1)
#define EVT_OUTBOUND		(2)
#define EVT_UNKNOWN     (3)

#define CMD_CFL     (1)
#define CMD_CND     (2)
#define CMD_CFM     (3)
#define CMD_GFR     (4)
#define CMD_SFC     (5)

#define IDX_INBOUND (0)
#define IDX_OUTBOUND (1)

static char *prgCmdTxt[] = {    "CFL",	"CND", "CFM", "SFC", NULL };
static int    rgCmdDef[] = {  CMD_CFL, CMD_CND,CMD_CFM,CMD_SFC, 0 };

#define CDT_SIMPLE  (0)
#define CDT_DYNAMIC (1)
#define CDT_STATIC  (2)

#define CMP_FALSE   (1)
#define CMP_SMALLER (2)
#define CMP_EQUAL   (3)
#define CMP_GREATER (4)

#define TPL_ACTIVE  (1)

#define START_OF_DAY_HOUR_DEFAULT	0
#define START_OF_DAY_MIN_DEFAULT	0
#define DURATION_OF_DAY_DEFAULT		24
#define DAY_OFFSET_DEFAULT			0

#define DATELEN		14

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

char pcgFlightFields[8192];


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static long  lgTimeOffset  = 259200;              /* 60 * 60 * 24 * 3 */
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static char  cgProcessName[80];                  /* name of executable */
static char  cgWksName[80];                     /* name of workstation */
static int   igRouter = 1200;

static char pcgTmpBuf[18192];
static char pcgInbound[8192];
static char pcgOutbound[8192];
static char pcgFlight[18192];
static char cgGlobalSelection[8192];

static int	igStartOfDayHour;
static int	igStartOfDayMin;
static int	igDurationOfDay;
static int	igDayOffset;
static char cgCNDTemplates[256];

#define INBOUNDTIMES "TIFA,STOA,ETOA,TMOA,LAND,ONBL"
#define OUTBOUNDTIMES "TIFD,STOD,ETOD,OFBL,AIRB"

static char   cgNewline[2];

	
/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgFlightArray;

static int igFlightRKEY;
static int igFlightADID;
static int igFlightTIFA;
static int igFlightTIFD;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  InitFliCol();
static int InitFieldIndex(char *pcpFieldList);
static int GetFieldList();

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,ARRAYINFO *prpArrayInfo);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int);*/                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *pipLen);




static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int HandleDataTest(EVENT *prpEvent);       /* Handles event data     */
static int TimeToStr(char *pcpTime,time_t lpTime); 

static int CreateAndFillFlightArray(char *, char *pclFields,char *pclData);
static int SendToFlight(char *pcpSelection);
static int SendToRulfnd(char *, char *pcpFields, char *pcpInbound,
						char *pcpOutbound);
static int SendCFLToRulfnd();
static int SendToQue(int ipModIDDest,int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
							CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData);
static int SendAnswer(EVENT *prpEvent,int ilRc);       
static int IniCNDPeriod ( char *pcpCmd );
static int	HandleCND(char *pcpCmd);
static int	HandleSFC(char *pcpCmd,char *pcpSelection);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel;

	/************************
	FILE *prlErrFile = NULL;
	char clErrFile[512];
	************************/

	INITIALIZE;			/* General initialization	*/

	/****************************************************************************
	sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
	prlErrFile = freopen(&clErrFile[0],"w",stderr);
	fprintf(stderr,"HoHoHo\n");fflush(stderr);
	dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
	****************************************************************************/

	ilOldDebugLevel = debug_level;
	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/rulfnd.cfg",getenv("CFG_PATH"));
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitFliCol();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitFliCol: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

/***
	dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
	sleep(60);
***/

	debug_level = ilOldDebugLevel;

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	300	: /* TEST ONLY */
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleDataTest(prgEvent);
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					


			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitFieldIndex(char *pcpFieldList)
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;

	FindItemInList(pcpFieldList,"RKEY",',',&igFlightRKEY,&ilCol,&ilPos);
	FindItemInList(pcpFieldList,"ADID",',',&igFlightADID,&ilCol,&ilPos);
	FindItemInList(pcpFieldList,"TIFA",',',&igFlightTIFA,&ilCol,&ilPos);
	FindItemInList(pcpFieldList,"TIFD",',',&igFlightTIFD,&ilCol,&ilPos);
	dbg(TRACE,"RKEY %d, ADID %d, TIFA %d, TIFD %d",
		igFlightRKEY,igFlightADID,igFlightTIFA,igFlightTIFD);
	return ilRc;
}


static int InitFliCol()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  int  ilDummy = 0;
  char clSection[64];
  char clKeyword[64];
	int ilOldDebugLevel;
	char	clTmp[13];

	/************************
	void *pvlHandleSignalDummyPointer = HandleSignal;
	void *pvlSetArrayInfoVBDummyPointer = SetArrayInfoVB;
	void *pvlDebugPrintArrayInfoDummyPointer = DebugPrintArrayInfo;
	void *pvlDummyPointer = ;*/

	ilOldDebugLevel = debug_level;
	debug_level = TRACE;

	/* BST und SMI wollen das so */

	cgNewline[0] = '\n';
	cgNewline[1] = 0x00;

	/* now reading from configfile or from database */

	rgFlightArray.rrArrayHandle = -1;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitDemhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitFliCol> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitFliCol> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitFliCol> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"time_offset");

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char*)&ilDummy);
		if(ilRc != RC_SUCCESS)
		{
			lgTimeOffset = (60*60*24*3);
			dbg(TRACE,"InitFliCol: <%s> <%s> <%d>",&clSection[0],&clKeyword[0],lgTimeOffset);
			ilRc = RC_SUCCESS;
		}else{
			lgTimeOffset = ilDummy;
			dbg(TRACE,"InitFliCol: <%s> <%s> <%d>",&clSection[0],&clKeyword[0],lgTimeOffset);
		}/* end of if */
	}/* end of if */

	sprintf(cgConfigFile,"%s/flicol.cfg",getenv("CFG_PATH"));

	if (ilRc == RC_SUCCESS)
	{
		ilRc = iGetConfigRow(cgConfigFile,"CND","START_OF_DAY_HOUR",CFG_STRING,
								clTmp);
		if (ilRc == RC_SUCCESS)
			igStartOfDayHour = atoi(clTmp);
		else
		{
			igStartOfDayHour = START_OF_DAY_HOUR_DEFAULT;
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitFliCol: <%s> <%s> <%d>","CND","START_OF_DAY_HOUR",
			igStartOfDayHour);
	}

	if (ilRc == RC_SUCCESS)
	{
		ilRc = iGetConfigRow(cgConfigFile,"CND","START_OF_DAY_MIN",CFG_STRING,
								clTmp);
		if (ilRc == RC_SUCCESS)
			igStartOfDayMin = atoi(clTmp);
		else
		{
			igStartOfDayMin = START_OF_DAY_MIN_DEFAULT;
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitFliCol: <%s> <%s> <%d>","CND","START_OF_DAY_MIN",
			igStartOfDayMin);
	}

	if (ilRc == RC_SUCCESS)
	{
		ilRc = iGetConfigRow(cgConfigFile,"CND","DURATION_OF_DAY",CFG_STRING,
								clTmp);
		if (ilRc == RC_SUCCESS)
			igDurationOfDay = atoi(clTmp);
		else
		{
			igDurationOfDay = DURATION_OF_DAY_DEFAULT;
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitFliCol: <%s> <%s> <%d>","CND","DURATION_OF_DAY",
			igDurationOfDay);
	}

	if (ilRc == RC_SUCCESS)
	{
		ilRc = iGetConfigRow(cgConfigFile,"CND","DAY_OFFSET",CFG_STRING,
								clTmp);
		if (ilRc == RC_SUCCESS)
			igDayOffset = atoi(clTmp);
		else
		{
			igDayOffset = DAY_OFFSET_DEFAULT;
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitFliCol: <%s> <%s> <%d>","CND","DAY_OFFSET",
			igDayOffset);
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(10,10);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitDemhdl: SetArrayInfo flights array");

/**************
		ilRc = SetArrayInfo("NEWDEMANDS","DEMTAB",DEM_FIELDS,&rgNewDemArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo NEWDEMANDS failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitDemhdl: SetArrayInfo NEWDEMANDS OK");
	dbg(TRACE,"<%s> %d",rgNewDemArray.crArrayName,
	rgNewDemArray.rrArrayHandle);
		}
   ********************** */
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitFliCol: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitFliCol: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		/*ilRc = InitFieldIndex();*/
	}
	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
	}/* end of if */

	debug_level = ilOldDebugLevel;

	return(ilRc);
	
} /* end of initialize */

static int GetFieldList()
{
	int			ilRc = RC_SUCCESS;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;
	char pclData[124];
	char clQueueName[124];
	int ilAnswerQueue;

		sprintf(&clQueueName[0],"%s2",mod_name);

		ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"GetFieldList: GetDynamicQueue failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"GetFieldList:  got <%d> for <%s>",
					ilAnswerQueue,&clQueueName[0]);
		}
		if( ilRc == RC_SUCCESS )
		{
			*pclData = '\0';
			/* set BC-Head members */
			memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
			rlBCHead.rc = RC_SUCCESS;
			strncpy(rlBCHead.dest_name, "FLICOL", 10);
			strncpy(rlBCHead.recv_name, "FLICOL", 10);

			/* set CMDBLK members */
			memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
			strncpy(rlCmdblk.command, "SFL", 6);
			strncpy(rlCmdblk.obj_name, "AFT", 10);
			sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

			/* write request to QUE */
			if ((ilRc=SendToQue(7560,ilAnswerQueue, PRIORITY_3, &rlBCHead, &rlCmdblk,
					pclData,pclData,pclData)) != RC_SUCCESS)
			{
				dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
				dbg(DEBUG," ----- END -----");
			}
			else
			{
				dbg(TRACE,"%05d SendToQue (%d) returns <%d>", __LINE__,
							ilAnswerQueue,ilRc);
			}

			if( ilRc == RC_SUCCESS )
			{
				ilRc=que(QUE_GETBIG,0,ilAnswerQueue,PRIORITY_3,
						igItemLen,(char *)&prgItem);
				prgEvent = (EVENT *) prgItem->text;	
			}

			if( ilRc == RC_SUCCESS )
			{
				BC_HEAD *prlBchead       = NULL;
				CMDBLK  *prlCmdblk       = NULL;
				char    *pclSelection    = NULL;
				char    *pclFields       = NULL;
				char    *pclData         = NULL;

				prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
				prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
				pclSelection = prlCmdblk->data;
				pclFields    = pclSelection + strlen(pclSelection) + 1;
				pclData      = pclFields + strlen(pclFields) + 1;
				DebugPrintBchead(TRACE,prlBchead);
				DebugPrintCmdblk(TRACE,prlCmdblk);
				dbg(TRACE,"selection <%s>",pclSelection);
				dbg(TRACE,"fields    <%s>",pclFields);
				dbg(TRACE,"data      <%s>",pclData);

				strcpy(pcgFlightFields,pclFields);
			}

			ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue,ilRc);
			}
			else
			{
				dbg(DEBUG,"GetFieldList:  queue <%d> <%s> deleted",
					ilAnswerQueue,&clQueueName[0]);
			}
		}

	return ilRc;
}
/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,ARRAYINFO *prpArrayInfo)
{
	int	ilRc = RC_SUCCESS;				/* Return code */

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=10;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,pcpTableName,NULL,NULL,(long *)NULL,pcpArrayFieldList,&(prpArrayInfo->plrArrayFieldLen[0]),&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */

/*****************************
if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}
	}
*******************/

	return(ilRc);
}


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);

	sleep(ipSleep);

	dbg(TRACE,"Terminate: now leaving ...");
	
	sleep(2);

	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */
#endif
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitFliCol();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitFliCol: init failed!");
			} /* end of if */
	}/* end of if */

} /* end of HandleQueues */
	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

	/*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(TRACE,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		/* dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]); */
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */
	int       ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	strcpy(cgWksName,prlBchead->recv_name);
	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
		case CMD_CFL :
			dbg(TRACE,"%05d:fields    <%s>",__LINE__,pclFields);
			SendAnswer(prpEvent,RC_SUCCESS);
			HandleDataTest(prpEvent);
			SendCFLToRulfnd();
			break; /* CMD_CFL */
		case CMD_CND :
			IniCNDPeriod ( "CND" );	/* don't care about RC, use defaults (same behaviour as before) */
			HandleCND( "CND" );
			SendCFLToRulfnd();
			break;
		case CMD_SFC :
			 SendAnswer(prpEvent,RC_SUCCESS);
			 HandleSFC("SFC",pclSelection);
			break; /* CMD_GFR */
		case CMD_CFM :
			dbg(TRACE,"%05d:fields    <%s>",__LINE__,pclFields);
			SendAnswer(prpEvent,RC_SUCCESS);
			HandleDataTest(prpEvent);
			break; /* CMD_CFL */
		default :
			dbg(TRACE,"HandleData: unknown command <%s>",
				&prlCmdblk->command[0]);
			DebugPrintBchead(TRACE,prlBchead);
			DebugPrintCmdblk(TRACE,prlCmdblk);
			dbg(TRACE,"selection <%s>",pclSelection);
			dbg(TRACE,"fields    <%s>",pclFields);
			dbg(TRACE,"data      <%s>",pclData);
			break; /* default */

		}/* end of switch */
	}
	else
	{
		if ( IniCNDPeriod ( prlCmdblk->command ) == RC_SUCCESS ) 
		{
			HandleCND( prlCmdblk->command );
			SendCFLToRulfnd();
		}
		else
		{
			dbg(TRACE,"HandleData: GetCommand failed");
			DebugPrintBchead(TRACE,prlBchead);
			DebugPrintCmdblk(TRACE,prlCmdblk);
			dbg(TRACE,"selection <%s>",pclSelection);
			dbg(TRACE,"fields    <%s>",pclFields);
			dbg(TRACE,"data      <%s>",pclData);
		}
	}/* end of if */

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */


static int HandleDataTest(EVENT *prpEvent)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char	clSelection[1540];
	char    pclFrom[124];
	char    pclTo[124];

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	strcpy(cgGlobalSelection,pclSelection);
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	*pclFrom = '\0';
	*pclTo = '\0';
	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	if (*pclSelection == '\0')
	{
		sprintf(cgConfigFile,"%s/flicol.cfg",getenv("CFG_PATH"));
		dbg(TRACE,"ConfigFile: <%s>",cgConfigFile);
		ilRc = TransferFile(cgConfigFile);

		ilRc = iGetConfigEntry(cgConfigFile,
				"MAIN","FROM",CFG_STRING,(char*)pclFrom);
		dbg(TRACE,"RC=%d FROM: <%s>",ilRc,pclFrom);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = iGetConfigEntry(cgConfigFile,"MAIN","TO",
							CFG_STRING,(char*)pclTo);
		}
		dbg(TRACE,"RC=%d TO  : <%s>",ilRc,pclTo);

		/***************
		iGetConfigEntry(cgConfigFile,"MAIN","RULFND",
					CFG_INT,(char*)&pcgRulfndModId);
					**********/

		/*if(ilRc == RC_SUCCESS)*/
		{
			sprintf(clSelection,
					"WHERE (TIFD >= '%s' AND TIFA <= '%s') [ROTATIONS]",
					pclFrom,pclTo);
		}
	}
	else
	{
		strcpy(clSelection,pclSelection);
	}
	if(ilRc == RC_SUCCESS)
	{
		GetFieldList();
		dbg(TRACE,"FieldList: <%s>",pcgFlightFields);
		dbg(TRACE,"SendToFlight follows Sel: <%s>",clSelection);
		ilRc = SendToFlight(clSelection);
		dbg(TRACE,"SendToFlight returned: %d",ilRc);
	}
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
}

static int TimeToStr(char *pcpTime,time_t lpTime)                               
{                                                                               
	struct tm *_tm;
	
	_tm = (struct tm *)localtime(&lpTime);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
			_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
			_tm->tm_min,_tm->tm_sec);
	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               

/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




static int CreateAndFillFlightArray(char *pcpSelection, char *pcpFields,
									char *pcpData)
{
	int	ilRc = RC_SUCCESS;
	int ilDataLineLen;
	long llRowNum = ARR_LAST;

	if(rgFlightArray.rrArrayHandle != -1)
	{
		CEDAArrayDestroy(&(rgFlightArray.rrArrayHandle),
									&(rgFlightArray.crArrayName[0]));
	}


	dbg(TRACE,"CreateAndFillFlightArray: SetArrayInfo flights array");
	InitFieldIndex(pcpFields);
	ilRc = SetArrayInfo("FLIGHTS","AFTTAB",pcpFields,&rgFlightArray);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"CreateAndFillFlightArray: SetArrayInfo FLIGHTS failed <%d>",
			ilRc);
	}
	else
	{
		dbg(TRACE,"CreateAndFillFlightArray: SetArrayInfo FLIGHTS OK");
		dbg(TRACE,"<%s> %d",rgFlightArray.crArrayName,
			rgFlightArray.rrArrayHandle);
		dbg(TRACE,"Offsets: Rkey %d Tifa %d Tifd %d Adid %d",
			rgFlightArray.plrArrayFieldOfs[igFlightRKEY],
			rgFlightArray.plrArrayFieldOfs[igFlightTIFA],
			rgFlightArray.plrArrayFieldOfs[igFlightTIFD],
			rgFlightArray.plrArrayFieldOfs[igFlightADID]);
	}

	if(ilRc == RC_SUCCESS)
	{
		do
		{
			ilDataLineLen = GetNextDataItem(pcgTmpBuf,
							&pcpData,&cgNewline[0],"","\0\0");
			dbg(TRACE,"ilDataLineLen = %d,Flight: <%s>",
				ilDataLineLen,pcgTmpBuf);
			if (ilDataLineLen  > 0)
			{
				ilRc = CEDAArrayAddRowPart(&rgFlightArray.rrArrayHandle,
											rgFlightArray.crArrayName,&llRowNum,											pcpFields,pcgTmpBuf);
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Error in CEDAArrayAddRowPart:%d <%s> ",
						ilRc,AATArrayGetErrorMsg(ilRc));
				}
			}
		} while (ilDataLineLen > 0);

		strcpy(rgFlightArray.crIdx01Name,"RKEY");
		ilRc = CEDAArrayCreateSimpleIndexUp(&rgFlightArray.rrArrayHandle,
				rgFlightArray.crArrayName,&rgFlightArray.rrIdx01Handle,
				rgFlightArray.crIdx01Name,"RKEY,ADID");
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Error in CEDAArrayCreateSimpleIndex:%d <%s> ",
				ilRc,AATArrayGetErrorMsg(ilRc));
		}
		memset(pcgInbound,0,sizeof(pcgInbound));
		memset(pcgOutbound,0,sizeof(pcgOutbound));
    	if(ilRc == RC_SUCCESS)
    	{
			long llFunc  = ARR_FIRST;
			char *pclResult = NULL;
			char pclRkey[124];

			*pclRkey = '\0';
				
			while(CEDAArrayFindRowPointer(&(rgFlightArray.rrArrayHandle),
				&(rgFlightArray.crArrayName[0]),&(rgFlightArray.rrIdx01Handle),
				&(rgFlightArray.crIdx01Name[0]),NULL,
							&llFunc,(void *) &pclResult) == RC_SUCCESS)
	        {
				dbg(TRACE,"RKEY <%s> <%s> <%s> <%s>",
					FLIGHTFIELD(pclResult,igFlightRKEY),
					FLIGHTFIELD(pclResult,igFlightADID),
					FLIGHTFIELD(pclResult,igFlightTIFA),
					FLIGHTFIELD(pclResult,igFlightTIFD));
				if (*pclRkey &&
					strcmp(pclRkey,FLIGHTFIELD(pclResult,igFlightRKEY)))
				{
					dbg(TRACE,
						"SendToRulfnd follows RKEY: %s\nInbound:<%s>\nOutbound:<%s>",
						pclRkey, pcgInbound,pcgOutbound);
					SendToRulfnd(pcpSelection,pcpFields,pcgInbound,pcgOutbound);
					memset(pcgInbound,0,sizeof(pcgInbound));
					memset(pcgOutbound,0,sizeof(pcgOutbound));
				}
				strcpy(pclRkey,FLIGHTFIELD(pclResult,igFlightRKEY));
				if(!strcmp(FLIGHTFIELD(pclResult,igFlightADID),"A"))
				{
					CEDAArrayGetRow(&(rgFlightArray.rrArrayHandle),
									&(rgFlightArray.crArrayName[0]),llFunc,',',
									sizeof(pcgInbound),pcgInbound);
				}
				if(!strcmp(FLIGHTFIELD(pclResult,igFlightADID),"D"))
				{
					CEDAArrayGetRow(&(rgFlightArray.rrArrayHandle),
									&(rgFlightArray.crArrayName[0]),llFunc,',',
									sizeof(pcgOutbound),pcgOutbound);
				}
				llFunc  = ARR_NEXT;
			}
			if(*pcgInbound | *pcgOutbound)
			{
				dbg(TRACE,
					"SendToRulfnd follows RKEY:%s\nInbound:<%s>\nOutbound:<%s>",
					pclRkey, pcgInbound,pcgOutbound);
				SendToRulfnd(pcpSelection,pcpFields,pcgInbound,pcgOutbound);
				memset(pcgInbound,0,sizeof(pcgInbound));
				memset(pcgOutbound,0,sizeof(pcgOutbound));
			}
		}
	}
	return ilRc;
}

static int SendAnswer(EVENT *prpEvent,int ipRc)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char	*pclData         = NULL;
	char	clDel = '|';
	char	*pclPipe = NULL;
	char	clSelection[1024];

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	prlBchead->rc = ipRc;

	strcpy(clSelection,pclSelection);
/* we don't want to send the data after the pipe */
	pclPipe = strchr(clSelection, clDel);
	if (pclPipe != NULL)
	{
		*pclPipe = 0x00;
	}

	dbg(TRACE,"SendAnswer to: %d",prpEvent->originator);
	if ((ilRc=SendToQue(prpEvent->originator,mod_id,
				PRIORITY_4, prlBchead, prlCmdblk,
						clSelection,pclFields,pclData)) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
		dbg(DEBUG," ----- END -----");
	}
	else
	{
		dbg(TRACE,"%05d SendToQue (%d) returns <%d>", __LINE__,
			prpEvent->originator,ilRc);
	}

	return ilRc;
}


static int SendCFLToRulfnd()
{
	int			ilRc;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;

			/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, cgProcessName, 10);
	strncpy(rlBCHead.dest_name, "FLICOL", 10);
	strncpy(rlBCHead.recv_name,cgWksName, 10);

			/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "CFL", 6);
	sprintf(rlCmdblk.obj_name,"AFT%s",cgTabEnd);
	sprintf(rlCmdblk.tw_start, "%s", cgProcessName);
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

			/* write request to QUE */
	if ((ilRc=SendToQue(7560,mod_id, PRIORITY_4, &rlBCHead, &rlCmdblk,
						cgGlobalSelection," "," ")) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
		dbg(DEBUG," ----- END -----");
	}
	else
	{
		dbg(TRACE,"%05d SendToQue CFL (%d) returns <%d>", __LINE__,
			7560,ilRc);
	}
	return ilRc;
}

static int SendToRulfnd(char *pcpSelection, char *pcpFields,char *pcpInbound,
						char *pcpOutbound)
{
	int		ilRc;
	BC_HEAD	rlBCHead;
	CMDBLK	rlCmdblk;
	char	clFields[2048];
	char	clInbound[4096];
	char	clOutbound[4096];
	char	clDdlTmp[40];
	int		ilDdlR,ilDdlA,ilDdlD,ilDdlf;

	strcpy(clFields,pcpFields);
	strcpy(clInbound,pcpInbound);
	strcpy(clOutbound,pcpOutbound);

	ilDdlf = get_item_no(clFields, "DDLF", 5) + 1;
	if (ilDdlf > 0)
	{
		/*** found DDLF add DDLR ***/
	 	get_real_item(clDdlTmp, clInbound, ilDdlf);
		ilDdlA = atoi(clDdlTmp);
	 	get_real_item(clDdlTmp, clOutbound, ilDdlf);
		ilDdlD = atoi(clDdlTmp);
		ilDdlR = ilDdlA + ilDdlD;
		dbg(TRACE,
			"DDL A set to %d DDL D to %d DDL R to %d",ilDdlA,ilDdlD,ilDdlR);
		sprintf(clDdlTmp,",DDLR");
		strcat(clFields,clDdlTmp);
		sprintf(clDdlTmp,",%d",ilDdlR);
		if ( *pcpInbound )
			strcat(clInbound,clDdlTmp);
		if ( *pcpOutbound )
			strcat(clOutbound,clDdlTmp);
	}

	if (! *pcpInbound && !*pcpOutbound)
	{
		return RC_SUCCESS;  /* nothing to do */
	}
			/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, cgProcessName, 10);
	strncpy(rlBCHead.dest_name, "FLICOL", 10);
	strncpy(rlBCHead.recv_name, "FLICOL", 10);

			/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "UFR", 6);
	strncpy(rlCmdblk.obj_name, "AFT", 10);
	sprintf(rlCmdblk.tw_start, "%s", cgProcessName);
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

	sprintf(pcgFlight,"%s\n%s",clInbound,clOutbound);
			/* write request to QUE */
	if ((ilRc=SendToQue(7560,mod_id, PRIORITY_4, &rlBCHead, &rlCmdblk,
						pcpSelection, clFields, pcgFlight)) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
		dbg(DEBUG," ----- END -----");
	}
	else
	{
		dbg(TRACE, "%05d SendToQue (%d) returns <%d>", __LINE__, 7560, ilRc);
	}

	return ilRc;
}
static int SendToFlight(char *pcpSelection)
{
	int		ilRc;
	BC_HEAD	rlBCHead;
	CMDBLK	rlCmdblk;
	char	clSelectFlight[2048];
	char	*pclSelectRulfnd = NULL;
	char	pclData[124];
	char	clQueueName[124];
	int		ilAnswerQueue;
	char    clDel = '|' ;
	char	clEmpty[2]=" ";

	strcpy(clSelectFlight,pcpSelection);
	pclSelectRulfnd = strchr(clSelectFlight, clDel);
	if (pclSelectRulfnd != NULL)
	{
		*pclSelectRulfnd = 0x00;
		pclSelectRulfnd++;
	}
	else
		pclSelectRulfnd = clEmpty;  /* hag 04.07.2003 */


	strcpy(pclData," ");

	sprintf(&clQueueName[0],"%s2",mod_name);

	ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"SendToFlight: GetDynamicQueue failed <%d>",ilRc);
	}
	else
	{
		dbg(DEBUG,"SendToFlight:  got <%d> for <%s>",
					ilAnswerQueue,&clQueueName[0]);
	}
	if( ilRc == RC_SUCCESS )
	{
			/* set BC-Head members */
		memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
		rlBCHead.rc = RC_SUCCESS;
		strncpy(rlBCHead.dest_name, cgProcessName, 10);
		strncpy(rlBCHead.dest_name, "FLICOL", 10);
		strncpy(rlBCHead.recv_name, "FLICOL", 10);

			/* set CMDBLK members */
		memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
		strncpy(rlCmdblk.command, "GFR", 6);
		strncpy(rlCmdblk.obj_name, "AFTTAB", 10);
		sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

			/* write request to QUE */
		if ((ilRc=SendToQue(igRouter, ilAnswerQueue, PRIORITY_3, &rlBCHead,
							 &rlCmdblk, clSelectFlight, pcgFlightFields,
														pclData)) != RC_SUCCESS)
		{
			dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
			dbg(DEBUG," ----- END -----");
		}
		else
		{
			dbg(TRACE,"%05d SendToQue (%d) returns <%d>", __LINE__,
				ilAnswerQueue,ilRc);
		}

		if( ilRc == RC_SUCCESS )
		{
			ilRc=que(QUE_GETBIG,0,ilAnswerQueue,PRIORITY_3,
					igItemLen,(char *)&prgItem);
			prgEvent = (EVENT *) prgItem->text;	
		}

		{ /* delete temparary queue */
			int ilRC;

			ilRC = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0);
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue,ilRC);
			}
			else
			{
				dbg(DEBUG,"SendToFlight:  queue <%d> <%s> deleted",
					ilAnswerQueue,&clQueueName[0]);
			}
		}

		if( ilRc == RC_SUCCESS )
		{
			BC_HEAD *prlBchead       = NULL;
			CMDBLK  *prlCmdblk       = NULL;
			char    *pclSelection    = NULL;
			char    *pclFields       = NULL;
			char    *pclData         = NULL;
			prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
			prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
			pclSelection = prlCmdblk->data;
			pclFields    = pclSelection + strlen(pclSelection) + 1;
			pclData      = pclFields + strlen(pclFields) + 1;
			DebugPrintBchead(TRACE,prlBchead);
			DebugPrintCmdblk(TRACE,prlCmdblk);
			dbg(TRACE,"selection <%s>",pclSelection);
			dbg(TRACE,"fields    <%s>",pclFields);
				/*dbg(TRACE,"data      <%s>",pclData);*/

			strcpy(pcgFlightFields,pclFields);
			ilRc = CreateAndFillFlightArray(pclSelectRulfnd,pclFields,pclData);
		}
	}
	/* bye bye */
	return ilRc;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModIDDest, int ipModID, int ipPrio, 
							BC_HEAD *prpBCHead, CMDBLK *prpCmdblk,
							char *pcpSelection, char *pcpFields, char *pcpData)
{
	int		ilRc;
	int		ilLen;
	EVENT	*prlOutEvent 	= NULL;
	BC_HEAD	*prlOutBCHead 	= NULL;
	CMDBLK	*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			 strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = ipModID;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);

	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRc = que(QUE_PUT,ipModIDDest, ipModID, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc);
		return RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

static int HandleSFC(char *pcpCmd,char *pcpSelection)
{
	int			ilRc = RC_SUCCESS;
	struct tm	*prlTimeStruct;
	time_t		tlStart, tlEnd;
	char		clStart[DATELEN+1];
	char		clEnd[DATELEN+1];
	char        clDel[2] = "|" ;
	
	dbg(TRACE, "Command %s arrived", pcpCmd );


	sprintf(cgGlobalSelection," %s AND HOPO = '%s' [ROTATIONS]",pcpSelection,cgHopo);
	GetFieldList();
	dbg(TRACE,"SendToFlight follows Sel: <%s>",cgGlobalSelection);
	ilRc = SendToFlight(cgGlobalSelection);
	dbg(TRACE,"after SendToFlight");
	return ilRc;
}

static int HandleCND(char *pcpCmd)
{
	int			ilRc = RC_SUCCESS;
	struct tm	*prlTimeStruct;
	time_t		tlStart, tlEnd;
	char		clStart[DATELEN+1];
	char		clEnd[DATELEN+1];
	char        clDel[2] = "|" ;
	
	dbg(TRACE, "Command %s arrived", pcpCmd );

	tlStart = time(0L);
	prlTimeStruct = (struct tm *)localtime(&tlStart);
	prlTimeStruct->tm_hour = igStartOfDayHour;
	prlTimeStruct->tm_min = igStartOfDayMin;
	prlTimeStruct->tm_sec = 0;
	prlTimeStruct->tm_isdst = -1;
	tlStart = mktime(prlTimeStruct);
	tlStart = tlStart + 60*60*24*igDayOffset;
	tlEnd = tlStart + 60*60*igDurationOfDay;

	TimeToStr(clStart, tlStart);
	TimeToStr(clEnd, tlEnd);

	dbg(TRACE, "Start of Calculation %s", clStart);
	dbg(TRACE, "End of Calculation %s", clEnd);

	GetFieldList();
	sprintf(cgGlobalSelection,
			"WHERE (((TIFA >= '%s' AND TIFA <= '%s' AND DES3 = '%s') OR (TIFD >= '%s' AND TIFD <= '%s' AND ORG3 = '%s')) AND (FTYP IN ('O','R','D','S'))) [ROTATIONS]",
					clStart,clEnd,cgHopo,clStart,clEnd,cgHopo);
	if ( cgCNDTemplates[0] )
	{
		strcat ( cgGlobalSelection, clDel );
		strcat ( cgGlobalSelection, cgCNDTemplates );
	}
	ilRc = SendToFlight(cgGlobalSelection);
	return ilRc;
}

static int IniCNDPeriod ( char *pcpCmd )
{
	int		ilRc1, ilRc = RC_SUCCESS;
	char	clTmp[13];

	if ( !pcpCmd )
		return RC_INVALID;
	
	ilRc1 = iGetConfigRow(cgConfigFile,pcpCmd,"START_OF_DAY_HOUR",CFG_STRING, clTmp);
	if (ilRc1 == RC_SUCCESS)
		igStartOfDayHour = atoi(clTmp);
	else
	{
		igStartOfDayHour = START_OF_DAY_HOUR_DEFAULT;
		ilRc = RC_NODATA;
	}
	
	ilRc1 = iGetConfigRow(cgConfigFile,pcpCmd,"START_OF_DAY_MIN",CFG_STRING, clTmp);
	if (ilRc1 == RC_SUCCESS)
		igStartOfDayMin = atoi(clTmp);
	else
	{
		igStartOfDayMin = START_OF_DAY_MIN_DEFAULT;
		ilRc = RC_NODATA;
	}

	ilRc1 = iGetConfigRow(cgConfigFile,pcpCmd,"DURATION_OF_DAY",CFG_STRING, clTmp);
	if (ilRc1 == RC_SUCCESS)
		igDurationOfDay = atoi(clTmp);
	else
	{
		igDurationOfDay = DURATION_OF_DAY_DEFAULT;
		ilRc = RC_NODATA;
	}

	ilRc1 = iGetConfigRow(cgConfigFile,pcpCmd,"DAY_OFFSET",CFG_STRING, clTmp);
	if (ilRc1 == RC_SUCCESS)
		igDayOffset = atoi(clTmp);
	else
	{
		igDayOffset = DAY_OFFSET_DEFAULT;
		ilRc = RC_NODATA;
	}
	cgCNDTemplates[0] = '\0';
	ilRc1 = iGetConfigRow(cgConfigFile,pcpCmd,"TEMPLATES",CFG_STRING, cgCNDTemplates);
	if (ilRc1 != RC_SUCCESS)
		cgCNDTemplates[0] = '\0';

	dbg(TRACE,"IniCNDPeriod: <%s> Templ. <%s> ilRc <%d>", pcpCmd, cgCNDTemplates, ilRc );
	dbg(TRACE,"IniCNDPeriod: START HOUR <%d> MIN    <%d>",igStartOfDayHour, igStartOfDayMin ) ;
	dbg(TRACE,"IniCNDPeriod: DURATION   <%d> OFFSET <%d>", igDurationOfDay, igDayOffset );
	return ilRc;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */









