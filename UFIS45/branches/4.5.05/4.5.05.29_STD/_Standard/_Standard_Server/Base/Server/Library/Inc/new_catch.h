#ifndef _DEF_mks_version_new_catch_h
  #define _DEF_mks_version_new_catch_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_new_catch_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/new_catch.h 1.2 2004/07/27 16:48:10SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*********************************************************************
  New Signal-Handler Funktions
  **********************************************************************/

#ifndef __NEWCATCH_INC
#define __NEWCATCH_INC


int	SetSignals(void (*HandleSignal)(int));
int	UnsetSignals(void);

#endif

