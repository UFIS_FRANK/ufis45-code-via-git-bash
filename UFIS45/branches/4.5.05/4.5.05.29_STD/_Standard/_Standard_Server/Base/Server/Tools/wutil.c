#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/wutil.c 1.3 2003/07/04 21:44:45SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#define		CEDA_PRG
/* ******************************************************************** */
/*									*/
/* This file contains the implementation of the UGCCS utility program	*/
/* The queue utility program allows the user to :			*/
/*									*/
/*	1. Display queue overview.					*/
/*	2. Display route overview.					*/
/*	3. Display queue details.					*/
/*	4. Manipulate queues. ( ex. Add/Delete/Edit )			*/
/*	5. Manipulate routes. ( ex. Add/Delete/Edit )			*/
/*	6. Send commands to routines.					*/
/*									*/
/* All manipulations are limited to be performed by a super user.	*/
/*									*/
/*	changes by jhi:							*/
/*	added NW for QUE_GET in delete_queue() so it comes back		*/
/*	if a proccess is in PNTAB mod_name or mod_id is usable		*/
/*	mod_names longer than 6 characters are allowed for dynamic queue*/
/*	ESC always steps back to prev. menue or quit 			*/
/*	loop for queue overview is usable as manual step or automatic 2s.*/
/*	SPACE steps to next page					*/
/*	now a string in send command is send as command for EVENT_DATA	*/
/*	not longer as command in SYS_EVENT				*/
/*                                            */
/*  20020709: JIM: debug_level = 0 ( koennt zuwenig sein, weil eine   */
/*                 Lib-Function zwecks schwerem Fehler debug_level hoch */
/*                 setzt oder outp direct benutzt)                      */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* 20030617 JIM: send_commands may be called from <1/6> also, so display */
/*               last menue on exit (may be <> main menu)                */
/* 20030704 JIM: QUE_GETNW for answer of status change                   */
/* ******************************************************************** */

static char sccs_mycutil[] ="@(#) UFIS 4.4 (c) ABB AAT/I mycutil.c 44.2 / 00/01/07 09:57:20 / VBL / MIKE";


#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <memory.h>
#include <signal.h>

#define U_MAIN					/* A main program	*/
#define QUE_INTERNAL				/* Use internal defines */
/*#define DESTROY*/					/* test for que_rebuild */


#define M_TERMINFO				/* For using curses	*/
#define NOMACROS
#include <curses.h>

#include "glbdef.h"
#include "ugccsma.h"
#include "initdef.h"
#include "quedef.h"
#include "queutil.h"
#include "uevent.h"
#include "cli.h"
#include "tools.h"
#include "new_catch.h"
/* The global variables */

static int		selection;		/* input choice		 */
static int		super_user	= 0;	/* The super user flag	 */
static int		last_menu;		/* The last menu flag	 */
static int		cr_flag		= 0;	/* create flag		 */
static Q_ENTRY		*next_item	= NULL;	/* ptr to last+1 send item
						   in queue_display	 */
static int		last_prio	= NUL;	/* last priority	 */
static int		last_ID		= NUL;	/* last queue ID	 */
static int              sigflag=0;
static int              refreshscreen=0;
int				sth_INIT = 0;
int	debug_level=0;
char	*mod_name;
char	__CedaLogFile[128];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
WINDOW			*winu, *winl;		/* Upper and lower window */
static int gettimeout=1;

/* The subroutine prototypes */

/* The general routines and the main menu */

static void		display_last_menu(void);
static void		display_main(void);	/* Display main screen	*/
static void		hand_main(void);	/* Handle main menu	*/
static void		display_qmain(void);	/* Display que main screen */
static void		hand_queue(void);	/* Handle queue menu	*/
static void		show_error(char *);	/* Display error msg	*/
static void		selection_prompt(void);	/* Display selection prompt */
static void		prompt(char *);		/* Display a prompt	*/
static void		terminate(int);		/* Termination routine	*/
static void		getse(char *, int);	/* Read a string	*/
static void		display_send(void);	/* Display send menu	*/
static void		send_commands(void);	/* Sends commands (event) */
static void		send_cmd(char*);		/* Send specific command */

/* The queue display and manipulation routines */

static void		queue_overview(void);	/* Queue overview	*/
static void		queue_overview_loop(void);	/* Queue overview	*/
static void		queue_overview_loop_refresh(void); 	/* Queue overview	*/
static void		queue_overview_loop_automatic(void);	/* Queue overview	*/
static void		queue_display(void);	/* Queue display	*/
static void		man_queues(void);	/* Manipulate queues	*/
static void		display_manq(void);	/* Queue manipulation menu */
static void		add_queue(void);	/* Add a queue		*/
static void		delete_queue(void);	/* Delete a queue	*/
static void		clear_queue(void);	/* Clear a queue	*/
static void		set_status(int);	/* Set queue status	*/

/* The route display and manipulation routines */

static void		route_overview(void);	/* Route overview	*/
static void		man_routes(void);	/* Manipulate routes	*/
static void		display_manr(void);	/* Route manipulation menu */
static void		add_route(void);	/* Add a route		*/
static void		delete_route(void);	/* Delete a route	*/
static void		handle_dest(int);	/* Handles destinations */
static int		display_route(int,int);	/* Display route menu	*/
static void	        HandleSignal(int);
#ifdef DESTROY
static void		display_dest(void);	/* Display destroy queue 
							menu		*/
static void		display_dest2(void);	/* Display destroy queue 
							menu		*/
static void		dest_queue(void);	/* Destroy a queue	*/
static void		dest_queue2(void);	/* Destroy a queue	*/
static void		destroy(int);
#endif

/* ******************************************************************** */
/*									*/
/* The following routine is the main program.				*/
/* The program communicates with the QCP via the QCP input queue and	*/
/* the attach queue.							*/
/*									*/
/* ******************************************************************** */

MAIN

{
  ITEM	item;
  int	tag;
	
  mod_name = argv[0];
  /* if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
	sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());

  if(argc==2)
    {
      gettimeout=atoi(argv[1]);
    }else{
      gettimeout=0;
    }

  init();			/* Init access to ST */	
 	
  /* Catch all signals for orderly termination */
  SetSignals(HandleSignal);
/*    catch_all(terminate); */
/*    signal(SIGALRM,SIG_DFL); */
	
  /* Attach to the QCP queues ( if they are there !! ) */
	
  if ( init_que() != NUL ) {
    terminate(-1);
  } /* end if */
	
  tag = (getpid()%1000) + 30000;
  if ( que(QUE_CREATE,0,tag,0,5,"CUTIL") != RC_SUCCESS ) {
    terminate(-2);
  } /* end if */

  alarm(10);
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item) != RC_SUCCESS ) {
    terminate(-3);
  } /* end if */
  alarm(0);
	
  mod_id = item.originator;
  if ( mod_id == NUL ) {
    terminate(-4);
  } /* end if */
	
  super_user = 1;		/* He is a super user */
	
  /* Intialize the standard screen and the upper and lower windows */

  if ( initscr() == (WINDOW *) ERR ) {
    printf(CR_LF"Failed to initialize stdscr, terminating");
    printf("..."CR_LF);
    exit(-5);
  } /* end if */
  winu = newwin(12,80,0,0);
  winl = newwin(12,80,12,0);
	
  if (has_colors()) {
    start_color();
  }
  /*	wattron(stdscr,A_ALTCHARSET); */
  wclear(stdscr);
	
  hand_main();
	
  return 0;
}

/* ******************************************************************** */
/*									*/
/* The following routine handles the main display selections.		*/
/*									*/
/* ******************************************************************** */

static void	hand_main()

{
  display_main();
		
  /* Loop here until termination is requested */
	
  while (1) {
    last_menu = 1;
    echo();
    selection = wgetch(stdscr);
    if(selection == 0x1b)
      selection=0x39;
    noecho();

    switch ( selection - (int) '0' ) {
    case 1:
      hand_queue();
      break;
    case 2:
      if ( super_user != 1 ) {
	show_error(M_NOTSU);
	selection_prompt();
      } /* endif */
      else {
	send_commands();
      } /* end else */
      break;
    case 3:
      if(gettimeout==0)
	queue_overview_loop();
      else
	{
	wtimeout(stdscr,gettimeout*1000);
	queue_overview_loop_refresh();
	wtimeout(stdscr,-1);
	}
      break;
    case 9:
      terminate(-6);
      break;
    default:
      show_error(M_INVALID);
      selection_prompt();
      break;
    } /* end switch */
  } /* end while */
} /* end of hand_main */

/* ******************************************************************** */
/*									*/
/* The following routine displays the main screen.			*/
/*									*/
/* ******************************************************************** */

static void	display_main()

{
	wclear(stdscr);		/* Erase the virtual screen */
	mvaddstr(1,22,"      CEDA new utility program.");
	mvaddstr(2,22,"-----------------------------------");
	mvaddstr(5,26,"1. Queueing utilities.");
	mvaddstr(7,26,"2. Send commands.");
	if(gettimeout>0)
	  mvaddstr(12,10,"!!! THIS AUTOMATC REFRESH MODE GENERATES SYSTEM LOAD !!!");
	mvaddstr(21,26,"9. Exit program.");
	selection_prompt();
} /* end of display_main */

/* ******************************************************************** */
/*									*/
/* The following three routines display error messages, the selection	*/
/* prompt and a general prompt.						*/
/*									*/
/* ******************************************************************** */

static void	show_error(char *strng)

{
	wmove(stdscr,23,0);
	wclrtoeol(stdscr);
	mvaddstr(23,0,strng);
	wrefresh(stdscr);
	sleep(2);
	return;
} /* end of show_error */


static void selection_prompt()

{
	wmove(stdscr,23,0);
	wclrtoeol(stdscr);
	mvaddstr(23,0,M_SELECTION);
	wmove(stdscr,23,strlen(M_SELECTION));
	wrefresh(stdscr);
	return;
} /* end of selection_prompt */


static void prompt(char *prompt)

{
	int len;

	wmove(stdscr,23,0);
	wclrtoeol(stdscr);
	mvaddstr(23,0,prompt);
	len = strlen(prompt);
	wmove(stdscr,23,len);
	wrefresh(stdscr);
	return;
} /* end of prompt */

/* ******************************************************************** */
/*									*/
/* The following routine gets characters until n characters have been	*/
/* entered or a [RETURN] has been entered. Characters are echoed.	*/
/*									*/
/* ******************************************************************** */

static void	getse(char *buffer,int n)

{
	char	dummy;
	int	i;
	int	ret;
	
	ret = 0;
	echo();
	for ( i = 0 ; i <= n ; i++ ) {
		*(buffer + i) = wgetch(stdscr);
		if ( (*(buffer + i) == 0x08) && (i > 0) ) {
			i -= 2;
		} /* end if */
		if ( (*(buffer + i) == '\n') || (*(buffer + i) == ' ') ) {
			*(buffer + i) = 0x00;
			ret = 1;
			break;
		} /* end if */
	} /* end for */
	
	*(buffer + n) =0x00;
	noecho();
	return;
} /* end getse */

/* ******************************************************************** */
/*									*/
/* The following routine displays the last menu. The last menu indica-	*/
/* tor is held in the global variable last_menu.			*/
/*									*/
/* ******************************************************************** */

static void	display_last_menu()

{
	switch (last_menu) {
		
		case 1	:
			display_main();
			break;
		case 2	:
			display_manq();
			break;
		case 3	:
			display_manr();
			break;
		case 4	:
			display_qmain();
		default:
			break;
	} /* end switch */
	return;
} /* end of display_last_menu */

/* ******************************************************************** */
/*									*/
/* The following routine performs an orderly termination of the program	*/
/*									*/
/* ******************************************************************** */

static void	terminate(int sig)

{
	if ( sig < 0 ) {
		switch(sig) {
			case -1	:
				printf(CR_LF"QCP queues do not exist");
				printf(" ... terminating"CR_LF);
				break;
			case -2 :
				printf(CR_LF"Send QUE CREATE request failed");
				printf(" ... terminating"CR_LF);
				break;
			case -3 :
				printf(CR_LF"QUE CREATE ack not received");
				printf(" ... terminating"CR_LF);
				que(QUE_DELETE,mod_id,mod_id,0,0,0);
				break;
			case -4	:
				printf(CR_LF"QUE CREATE rejected by QCP");
				printf(" ... terminating"CR_LF);
				break;
			case -5 :
				printf(CR_LF"Screen initializing failed");
				printf(" ... terminating"CR_LF);
				que(QUE_DELETE,mod_id,mod_id,0,0,0);
				break;
			case -6	:
				que(QUE_DELETE,mod_id,mod_id,0,0,0);
				echo();
				wclear(stdscr);
				wrefresh(stdscr);
				endwin();
				break;
			default:
				break;
			} /* end switch */
		} /* end if */
		else {
			echo();
			wclear(stdscr);
			wrefresh(stdscr);
			endwin();
			signal(SIGTERM,SIG_IGN);
			que(QUE_DELETE,mod_id,mod_id,0,0,0);
			printf("Abort due to signal : %u ...."CR_LF,sig);
		} /* end else */

	exit(0);
} /* end of terminate */

/* ******************************************************************** */
/*									*/
/* The following routine handles inputs from the queue main menu.	*/
/*									*/
/* ******************************************************************** */

static void	hand_queue()

{
	display_qmain();
		
	/* Loop here until termination is requested */
	
	while (1) {
		last_menu = 4;
		echo();
		selection = wgetch(stdscr);
		if(selection == 0x1b)
		  selection=0x39;
		noecho();

		switch (selection - (int) '0') {
			case 1:
				queue_overview();
				break;
			case 2:
				queue_display();
				break;
			case 3:
				route_overview();
				break;
			case 4:
				if ( super_user != 1 ) {
					show_error(M_NOTSU);
					selection_prompt();
				} /* endif */
				else {
					man_queues();
				} /* end else */
				break;
			case 5:
				if ( super_user != 1 ) {
					show_error(M_NOTSU);
					selection_prompt();
				} /* endif */
				else {
					man_routes();
				} /* end else */
				break;
			case 6:
				if ( super_user != 1 ) {
					show_error(M_NOTSU);
					selection_prompt();
				} /* endif */
				else {
					send_commands();
				} /* end else */
				break;
	             	case 7:
			  if(gettimeout==0)
			    queue_overview_loop();
			  else
			   {
			    wtimeout(stdscr,gettimeout*1000);
			    queue_overview_loop_refresh();
			    wtimeout(stdscr,-1);
			   }
				break;
		        case 8:
			        queue_overview_loop_automatic();
				break;
			case 9:
				display_main();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
} /* end of hand_queue */

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue main screen.		*/
/*									*/
/* ******************************************************************** */

static void	display_qmain()

{
	wclear(stdscr);		/* Erase the virtual screen */
	mvaddstr(1,22,"QCP display/manipulating program.");
	mvaddstr(2,22,"---------------------------------");
	mvaddstr(5,26,"1. Display queue overview.");
	mvaddstr(7,26,"2. Display queue details.");
	mvaddstr(9,26,"3. Display route overview.");
	mvaddstr(11,26,"4. Manipulate queues.");
	mvaddstr(13,26,"5. Manipulate routes.");
	mvaddstr(15,26,"6. Send commands.");
	mvaddstr(17,26,"7. Loop queue overview.");
	mvaddstr(19,26,"8. Loop queue overview 2 sec.");
	mvaddstr(21,26,"9. Exit program.");
	selection_prompt();
} /* end of display_qmain */

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue overview.			*/
/*									*/
/* ******************************************************************** */

static void	queue_overview()

{
	ITEM		*item;
	QUE_INFO	*que_info;
	int		line_count;
	int		info_count;
	int		len;
	int		return_value;
	int             move;
	/* Request the queue overview */
	errno = 10;
	return_value = que(QUE_QOVER,0,mod_id,5,0,0);
	if ( return_value != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */
#if 0
	len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
	item = (ITEM *) malloc(len);
	if ( item == (ITEM *) NUL ) {
		show_error(M_MEMORY);
		selection_prompt();
		return;
	} /* end if */
#endif
len=0;
item=NULL;
	if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */
	
	if ( item->msg_length == FATAL ) {
		free((char *) item);
		show_error(M_MEMORY);
		selection_prompt();
		return;
	} /* end if */
	
	que_info = (QUE_INFO *) &(item->text[0]);
	wclear(stdscr);
	mvaddstr(0,0,QO_HEADER);
	mvaddstr(1,0,QO_HEAD2);
	info_count = 0;
	line_count = 3;
	while ( info_count <= que_info->entries) {
		if ( line_count > 21 ) {
			mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
			wrefresh(stdscr);
			move=wgetch(stdscr);
			while ( move != '\n'&& move != 0x20 ) 
			{
			if(move==0x1b)
			  {
			    free((char *) item);
			    display_last_menu();
			    return;
			  }
			move=wgetch(stdscr);
			}
			wmove(stdscr,2,0);
			wclrtobot(stdscr);
			line_count = 2;
			wrefresh(stdscr);
		} /* end if */
		wprintw(stdscr,QO_INFO(que_info->data[info_count]));
		info_count++;
		line_count++;
	} /* end while */
	free((char *) item);
	mvaddstr(23,0,"End of display, type [RETURN] or [SPACE] to return to main menu");
	wrefresh(stdscr);
	move=wgetch(stdscr);
	while (move != '\n'&& move != 0x20 && move !=0x1b ) 
	  {
	    move=wgetch(stdscr); 
	  }
	display_last_menu();
	return;
} /* end of queue_overview */

/* ******************************************************************** */
/*									*/
/* The following routine gives a detailed display of a specific queue.	*/
/*									*/
/* ******************************************************************** */

static void	queue_display()

{
	MINFO		*info; 
	ITEM		*item;
	ITEM		*buff;
	EVENT		*evn;
	HEAD		*head;
	QUE_SEND	send;
	char		wrkbuff[80];
	char		*data;
	int		count;
	int		line;
	int		ret;
	int		len;
	int		j;
	int move;
	send.next = NULL;
	
	/* Prompt for queue ID */
	prompt(M_ENTERQID2);
	/*  getse(wrkbuff,5); */
	getse(wrkbuff,20);
	if((send.que_ID = atoi(wrkbuff))==0)
	  {
	    send.que_ID= tool_get_q_id(wrkbuff);
	  }
	/*  send.que_ID = atoi(wrkbuff); */

	if ( ( send.que_ID <= 0 ) || ( send.que_ID >= 30000 ) ) {	
		show_error(M_INVQUEID2);
		selection_prompt();
		return;
	} /* end if */

	/* Prompt for priority */
	prompt(M_ENTERPRIO);
	getse(wrkbuff,1);
	send.prio = atoi(wrkbuff);
	     
	if ( ( send.prio < 0 ) || ( send.prio >= 6 ) ) {
		show_error(M_INVPRIO);
		selection_prompt();
		return;
	} /* end if */
	
	if ( send.prio == NUL ) {
		if ( (last_prio != NUL) && (next_item != NUL) &&
			(send.que_ID == last_ID) ) {
			   send.prio = last_prio;
			   send.next = next_item;   
		} /* end if */
		else {
			show_error(M_INVPRIO);
			selection_prompt();
			return;
		} /* end else */
	} /* end if */
		   
	/* Prompt for number of items */
	prompt(M_ENTERNOIT);
	getse(wrkbuff,3);
	send.no_item = atoi(wrkbuff);

	if ( ( send.no_item < 0 ) || ( send.no_item > 188 ) ) {
		show_error(M_INVNOIT);
		selection_prompt();
		return;
	} /* end if */
	else {
		if ( send.no_item == NUL ) {
			send.no_item = 15;
		} /* end if */ 
	} /* end else */

        ret = que(QUE_QDISP,0,mod_id,5,sizeof(QUE_SEND),(char *)&send);
	if ( ret != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */

	len = sizeof(ITEM) + sizeof(HEAD) + send.no_item * (Q_ITEM_DATA +
		2 * sizeof(int));
	item = (ITEM *) malloc(len);
	if ( item == (ITEM *) NUL ) {
	       show_error(M_MEMORY);
	       selection_prompt();
	       return;
	} /* end if */

	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
	       free((char *) item);
	       show_error(M_QUEUINGR);
	       selection_prompt();
	       return;
	} /* end if */

	switch(item->msg_length) {
	case NOT_FOUND :
	       free((char *) item);
	       show_error(M_NOQUEUE);
	       selection_prompt();
	       return;
	       break;
	case NONFATAL :
	       free((char *) item);
	       show_error(M_NOITEMS);
	       selection_prompt();
	       return;
	       break;
	case FATAL :
	       free((char *) item);
	       show_error(M_MEMORY);
	       selection_prompt();
	       return;
	       break;
	default :
	       break;
	} /* end switch */
		
	head = (HEAD *) item->text;
	info = (MINFO *) (&(item->text[0]) + sizeof(HEAD));
	if ( head->qcb.total <= NUL ) {
	       free((char *) item);
	       show_error(M_NOITEMS);
	       selection_prompt();
	       return;
	} /* end if */
	
	count = line = 0;
	wclear(stdscr);
	while ( count < head->qcb.total ) {
		evn = (EVENT *) info->item.text;
		mvaddstr(line,0,QD_HEADERI);
		wprintw(stdscr,QD_ITEM(info->item));
		for ( j = 0 ; j < SYSID_SIZE ; j++ ) {
			if ( (evn->sys_ID[j] < 0x20) ||
				(evn->sys_ID[j] > 0x7e) ) {
				evn->sys_ID[j] = 0x58;
			} /* end if */
		} /* end for */
		mvaddstr(line+3,0,QD_HEADERE);
		wprintw(stdscr,QD_EVENT(evn));
		data = (char *) evn + sizeof(EVENT); 
		/*  wrefresh(stdscr); */
		/*  snap(data,(evn->data_length>48)?48:evn->data_length,outp); */
		wrefresh(stdscr);
		count++;
		if ( (line == 7) && (count < head->qcb.total) ) {
			mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
			wrefresh(stdscr);
			move=wgetch(stdscr);
			while ( move != '\n'&& move != 0x20 ) 
			  {
			    if(move==0x1b)
			      {
				free((char *) item);
				display_last_menu();
				return;
			      }
			    move=wgetch(stdscr);
			  }
			wmove(stdscr,0,0);
			wclear(stdscr);
			line = 0;
		} /* end if */
		else { 
			line = 7;
		} /* end else */
		last_prio = info->prio;
		info = (MINFO *) ((char *) info + info->len + 2*sizeof(int));
	} /* end while */
	next_item = head->q_ent;
	last_ID = head->qcb.que_ID;
	free((char *) item);
	mvaddstr(23,0,"End of display, type [RETURN] to return to main menu");
	wrefresh(stdscr);
	move=wgetch(stdscr);
	while ( move != '\n'&& move != 0x20&&move!=0x1b ) 
	  {
	    move=wgetch(stdscr);
	  }

	display_last_menu();
	return;
} /* end of queue_display */

/* ******************************************************************** */
/*									*/
/* The following function manipulates queues.				*/
/*									*/
/* ******************************************************************** */

static void	man_queues()

{
	display_manq();

	/* Loop here until termination is requested */

	while (1) {
		last_menu = 2;
		echo();
		selection = wgetch(stdscr);
		if(selection == 0x1b)
		  selection=0x39;
		noecho();

		switch (selection - (int) '0') {
			case 1:
				queue_overview();
				break;
			case 2:
				queue_display();
				break;
			case 3:
				add_queue();
				break;
			case 4:
				delete_queue();
				break;
			case 5:
				clear_queue();
				break;
			case 6:
				set_status(QUE_HOLD);
				break;
			case 7:
				set_status(QUE_GO);
				break;
		        case 8:
			  if(gettimeout==0)
			    queue_overview_loop();
			  else
			   {
			    wtimeout(stdscr,gettimeout*1000);
			    queue_overview_loop_refresh();
			    wtimeout(stdscr,-1);
			   }
				break;
			case 9:
				display_qmain();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
	return;
} /* end of man_queues */

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue manipulation screen.	*/
/*									*/
/* ******************************************************************** */

static void	display_manq()

{
	wclear(stdscr);	/* Erase the virtual screen */
	mvaddstr(1,22,"      Queue manipulating menu.    ");
	mvaddstr(2,22,"-----------------------------------");
	mvaddstr(5,26,"1. Display queue overview.");
	mvaddstr(7,26,"2. Display queue details.");
	mvaddstr(9,26,"3. Add queue.");
	mvaddstr(11,26,"4. Delete queue.");
	mvaddstr(13,26,"5. Clear queue.");
	mvaddstr(15,26,"6. Set queue on HOLD.");
	mvaddstr(17,26,"7. Set queue to GO.");
	mvaddstr(19,26,"8. Loop queue overview .");
	mvaddstr(21,26,"9. Exit to main menu.");
	selection_prompt();
	return;
} /* end of display_manq */

/* ******************************************************************** */
/*									*/
/* The following routine requests allocation of a new queue.		*/
/* The queue ID must be in the range of 1 -19999.			*/
/*									*/
/* ******************************************************************** */

static void	add_queue()

{
	ITEM	buff;
	int	len,i;
	int	que_ID;
	char	name[21];
	char	wrkbuff[80];
	
	/* Prompt for queue ID and name */
	
	prompt(M_ENTERQID1);
	getse(wrkbuff,5);
	que_ID = atoi(wrkbuff);

	if ( ( que_ID > 0 ) && ( que_ID < 20000 ) ) {

		memset(name,0x00,sizeof(name));
		prompt(M_ENTERNAME);
		getse(name,20);

		if ( que(QUE_CREATE,que_ID,mod_id,5,7,name) != RC_SUCCESS ) {
			show_error(M_QUEUINGS);
			selection_prompt();
			return;
		} /* end if */
		else {
			len = sizeof(ITEM);
			if ( que(QUE_RESP,0,mod_id,0,len,(char *)&buff) != RC_SUCCESS ) {
				show_error(M_QUEUINGR);
				selection_prompt();
				return;
			} /* end if */
			else {
				if ( buff.route == NUL ) {
					show_error(M_QUEADDFAIL);
				} /* end if */
				else {
					show_error(M_QUEADDOK);
				} /* end else */
				selection_prompt();
				return;
			} /* end else */
		} /* end else */
	} /* end if */
	else {
		show_error(M_INVQUEID1);
		selection_prompt();
	} /* end else */
	return;
} /* end of add_queue */

/* ******************************************************************** */
/*									*/
/* The following routine deletes a queue.				*/
/*									*/
/* ******************************************************************** */

static void	delete_queue()

{
	ITEM	*item;
	int	que_ID;
	int	len;
	char	wrkbuff[80];
	
	/* Prompt for queue ID */
	
	prompt(M_ENTERQID2);
/*  	getse(wrkbuff,5); */
/*  	que_ID = atoi(wrkbuff); */
	getse(wrkbuff,20);
	if((que_ID = atoi(wrkbuff))==0)
	  {
	    que_ID= tool_get_q_id(wrkbuff);
	  }
	if ( ( que_ID <= 0 ) || ( que_ID >= 30000 ) ) {
		show_error(M_INVQUEID2);
		selection_prompt();
		return;
	} /* end if */
	if ( que(QUE_DELETE,que_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */
	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GETNW,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error("Queue is successfully deleted");
	
		selection_prompt();
		return;
	} /* end if */
	switch(item->msg_length) {
		case FOUND:
			free((char *) item);
			show_error(M_QUEDELOK);
			selection_prompt();
			return;
			break;
		case NOT_FOUND:
			free((char *) item);
			show_error(M_NOQUEUE);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			selection_prompt();
			return;
			break;
	} /* end switch */

} /* end of delete_queue */

/* ******************************************************************** */
/*									*/
/* The following routine requests a clear of a queue.			*/
/*									*/
/* ******************************************************************** */

static void	clear_queue()

{
	ITEM	*item;
	int	que_ID;
	int	len;
	char	wrkbuff[80];
	
	/* Prompt for queue ID */
	
	prompt(M_ENTERQID2);
/*  	getse(wrkbuff,5); */
/*  	que_ID = atoi(wrkbuff); */
	getse(wrkbuff,20);
	if((que_ID = atoi(wrkbuff))==0)
	  {
	    que_ID= tool_get_q_id(wrkbuff);
	  }
	if ( ( que_ID <= 0 ) || ( que_ID >= 30000 ) ) {
		show_error(M_INVQUEID2);
		selection_prompt();
		return;
	} /* end if */
	
	if ( que(QUE_EMPTY,que_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */
	
	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */
		
	switch(item->msg_length) {
		case FOUND:
			free((char *) item);
			show_error(M_QUECLROK);
			selection_prompt();
			return;
			break;
		case NOT_FOUND:
			free((char *) item);
			show_error(M_NOQUEUE);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			selection_prompt();
			return;
			break;
	} /* end switch */

} /* end of clear_queue */

/* ******************************************************************** */
/*									*/
/* The following routine changes a queue status ( HOLD or GO ).		*/
/*									*/
/* ******************************************************************** */

static void	set_status(int new_status)

{
	ITEM	*item;
	int	que_ID;
	char	wrkbuff[80];
	int	func;
	int	len;
	
	/* Prompt for queue ID */
	
	prompt(M_ENTERQID2);
	/*  getse(wrkbuff,5); */
	/*  que_ID = atoi(wrkbuff); */
	getse(wrkbuff,20);
	if((que_ID = atoi(wrkbuff))==0)
	  {
	    que_ID= tool_get_q_id(wrkbuff);
	  }
	if(( que_ID <= 0 ) || ( que_ID >= 30000 )) {
		show_error(M_INVQUEID2);
		selection_prompt();
		return;
	} /* end if */
	
	if ( que(new_status,que_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */

	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GETNW,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		/* show_error(M_QUEUINGR); */
		selection_prompt();	
		return;
	} /* end if */	
	
	switch(item->msg_length) {
		case FOUND:
			free((char *) item);
			show_error(M_QUESTAOK);
			selection_prompt();
			return;
			break;
		case NOT_FOUND:
			free((char *) item);
			show_error(M_NOQUEUE);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			show_error(M_NOSTATUS);
			selection_prompt();
			return;
			break;
	} /* end switch */

} /* end of set_status */

/* ******************************************************************** */
/*									*/
/* The following routine displays the route overview.			*/
/*									*/
/* ******************************************************************** */

static void	route_overview()

{
	ITEM		*item;
	ROU_INFO	*rou_info;
	int		line_count;
	int		info_count;
	int		len;
	int		return_value;
	int		i,cnt;
	int move;
	/* Request the route oveview */
	
	return_value = que(QUE_ROVER,0,mod_id,5,0,0);
	if ( return_value != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
	} /* end if */
	
	len = sizeof(ITEM) + sizeof(ROU_INFO) + (sizeof(RINFO) * 100);
	item = (ITEM *) malloc(len);
	if ( item == (ITEM *) NUL ) {
		show_error(M_MEMORY);
		selection_prompt();
		return;
	} /* end if */
	
	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */

	if ( item->msg_length == FATAL ) {
		free((char *) item);
		show_error(M_MEMORY);
		selection_prompt();
		return;
	} /* end if */
	
	rou_info = (ROU_INFO *) &(item->text[0]);
	wclear(stdscr);
	mvaddstr(0,0,RO_HEADER);
	info_count = 0;
	line_count = 2;
	while ( info_count < rou_info->entries ) {
		if ( line_count > 21 ) {
			mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
			wrefresh(stdscr);
			move=wgetch(stdscr);
			while ( move != '\n'&& move != 0x20 ) 
			  {
			    if(move==0x1b)
			      {
				free((char *) item);
				display_last_menu();
				return;
			      }
			    move=wgetch(stdscr);
			}
			wmove(stdscr,2,0);
			wclrtobot(stdscr);
			line_count = 2;
			wrefresh(stdscr);
		} /* end if */
		wprintw(stdscr,RO_INFO(rou_info->data[info_count]));
		
		/* Display the queues */
		
		cnt = 0;
		wprintw(stdscr,"\t\t");
		for ( i = 0 ; i < rou_info->data[info_count].entries ; i++) {
			if ( cnt > 6 ) {
				line_count++;
				wprintw(stdscr,CR_LF"\t\t");
				cnt = 0;
			} /* end if */
			wprintw(stdscr,RO_QUE(rou_info->data[info_count],i));
			cnt++;
		} /* end for */
		
		info_count++;
		line_count++;
	} /* end while */
	mvaddstr(23,0,"End of display, type [RETURN] to return to main menu");
	wrefresh(stdscr);
	move=wgetch(stdscr);
	while (move != '\n'&& move != 0x20 && move !=0x1b ) 
	  {
	    move=wgetch(stdscr); 
	  }
	display_last_menu();
	free((char *) item);
	return;
} /* end of route_overview */

/* ******************************************************************** */
/*									*/
/* The following routine manipulates routes.				*/
/*									*/
/* ******************************************************************** */

static void	man_routes()

{
	display_manr();

	/* Loop here until termination is requested */

	while (1) {
		last_menu = 3;
		echo();
		selection = wgetch(stdscr);
		noecho();

		switch (selection - (int) '0') {
			case 1:
				route_overview();
				break;
			case 2:
				add_route();
				break;
			case 3:
				delete_route();
				break;
			case 4:
				handle_dest(ADD);
				break;
			case 5:
				handle_dest(DELETE);
				break;
			case 9:
				display_qmain();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
	return;
} /* end of man_routes */

/* ******************************************************************** */
/*									*/
/* The following routine displays route manipulating screen.		*/
/*									*/
/* ******************************************************************** */

static void	display_manr()

{
	wclear(stdscr);	/* Erase the virtual screen */
	mvaddstr(1,22,"      Route manipulating menu.    ");
	mvaddstr(2,22,"-----------------------------------");
	mvaddstr(5,26,"1. Display route overview.");
	mvaddstr(7,26,"2. Add route.");
	mvaddstr(9,26,"3. Delete route.");
	mvaddstr(11,26,"4. Add destination.");
	mvaddstr(13,26,"5. Delete destination.");
	mvaddstr(21,26,"9. Exit to main menu.");
	selection_prompt();
} /* end of display_manr */

/* ******************************************************************** */
/*									*/
/* The following routine adds a route entry.				*/
/*									*/
/* ******************************************************************** */

static void	add_route()

{
	ITEM	*item;
	int	rou_ID;
	int	len;
	char	wrkbuff[80];
	
	/* Prompt for route ID */
	
	prompt(M_ENTERRID1);
	getse(wrkbuff,5);
	rou_ID = atoi(wrkbuff);

	if ( ( rou_ID <= 0 ) || ( rou_ID >= 20000 ) ) {
		show_error(M_INVROUID1);
		selection_prompt();
		return;
	} /* end if */
	
	if ( que(QUE_AROUTE,rou_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */
	
	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */
		
	switch(item->msg_length) {
		case RC_SUCCESS:
			free((char *) item);
			show_error(M_ROUADDOK);
			selection_prompt();
			return;
			break;
		case FOUND:
			free((char *) item);
			show_error(M_ROUEXISTS);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			selection_prompt();
			return;
			break;
	} /* end switch */
	
} /* end of add_route */

/* ******************************************************************** */
/*									*/
/* The following routine deletes a route entry.				*/
/*									*/
/* ******************************************************************** */

static void	delete_route()

{
	ITEM	*item;
	int	rou_ID;
	int	len;
	char	wrkbuff[80];
	
	/* Prompt for route ID */
	
	prompt(M_ENTERRID2);
	getse(wrkbuff,5);
	rou_ID = atoi(wrkbuff);

	if ( ( rou_ID <= 0 ) || ( rou_ID >= 30000 ) ) {
		show_error(M_INVROUID2);
		selection_prompt();
		return;
	} /* end if */
	
	if ( que(QUE_DROUTE,rou_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */
	
	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */
		
	switch(item->msg_length) {
		case FOUND:
			free((char *) item);
			show_error(M_ROUDELOK);
			selection_prompt();
			return;
			break;
		case NOT_FOUND:
			free((char *) item);
			show_error(M_NOROUTE);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			selection_prompt();
			return;
			break;
	} /* end switch */

} /* end of delete_route */

/* ******************************************************************** */
/*									*/
/* The following routine adds or deletes a destination.			*/
/*									*/
/* ******************************************************************** */

static void	handle_dest(int func)

{
	ITEM	*item;
	int	len;
	int	rou_ID;
	int	que_ID;
	char	wrkbuff[80];
	int	qcp_fun;
	
	/* Check if ADD or DELETE */

	if ( func == ADD ) {
		qcp_fun = QUE_ARENTRY;
	} /* end if */
	else {	
		qcp_fun = QUE_DRENTRY;
	} /* end else */
	
	/* Prompt for route ID */
	
	prompt(M_ENTERRID2);
	/*  getse(wrkbuff,6); */
/*  	rou_ID = atoi(wrkbuff); */
	getse(wrkbuff,20);
	if((rou_ID = atoi(wrkbuff))==0)
	  {
	    rou_ID= tool_get_q_id(wrkbuff);
	  }
	if ( ( rou_ID <= 0 ) || ( rou_ID >= 30000 ) ) {
		show_error(M_INVROUID2);
		selection_prompt();
		return;
	} /* end if */
	
	if ( display_route(rou_ID,func) == -1 ) {
		show_error(M_NOROUTE);
		selection_prompt();
		return;
	} /* end if */
		
	/* Prompt for destination */
		
	prompt(M_DESTINATION);

/*  	getse(wrkbuff,5); */
/*  	que_ID = atoi(wrkbuff); */
	getse(wrkbuff,20);
	if((que_ID = atoi(wrkbuff))==0)
	  {
	    que_ID= tool_get_q_id(wrkbuff);
	  }
	if ( ( que_ID <= 0) || ( que_ID >= 30000 ) ) {
	     show_error(M_INVQUEID2);
	     display_last_menu();
	     selection_prompt();
	     return;
	} /* end if */
	
	if ( que(qcp_fun,rou_ID,mod_id,5,sizeof(int),(char *)&que_ID) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		selection_prompt();
		return;
	} /* end if */

	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		selection_prompt();
		return;
	} /* end if */

	switch(item->msg_length) {
		case OK2:
			free((char *) item);
			show_error(M_RCHANGE);
			display_last_menu();
			selection_prompt();
			return;
			break;
		case FOUND:
			free((char *) item);
			show_error(M_DESTEXISTS);
			display_last_menu();
			selection_prompt();
			return;
			break;
		case NOT_FOUND:
			free((char *) item);
			show_error(M_NOROUTE);
			display_last_menu();
			selection_prompt();
			return;
			break;
		case FATAL:
			free((char *) item);
			show_error(M_NOSPACE);
			display_last_menu();
			selection_prompt();
			return;
			break;
		case NOT_FOUND_D:
			free((char *) item);
			show_error(M_NODEST);
			selection_prompt();
			return;
			break;
		default:
			free((char *) item);
			display_last_menu();
			selection_prompt();
			return;
			break;
	} /* end switch */

} /* end of handle_dest */

/* ******************************************************************** */
/*									*/
/* The following function displays a single route for change processing	*/
/*									*/
/* ******************************************************************** */

static int	display_route(int rou,int func)

{
	int	len;
	int	i;
	int	cnt;
	ROUTE	*route;
	ITEM	*buff;
		

	len = sizeof(ITEM) + sizeof(ROUTE);
	buff = (ITEM *) malloc(len);
	if ( buff == (ITEM *) NUL ) {
		show_error(M_MEMORY);
		return -1;
	} /* end if */

	if ( que(QUE_RDISP,rou,mod_id,5,0,0) != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		free((char *) buff);
		return -1;
	} /* end if */
	
	if ( que(QUE_GET,0,mod_id,0,len,(char *)buff) != RC_SUCCESS ) {
		show_error(M_QUEUINGR);
		free((char *) buff);
		return -1;
	} /* end if */

	if ( (buff->msg_length == FATAL) ||
		(buff->msg_length == NOT_FOUND) ) {
		free((char *) buff);
		return -1;
	} /* end if */
	
	route = (ROUTE *) buff->text;
	
	wclear(stdscr);
	if ( func == DELETE ) {
		mvaddstr(0,0,M_RCDELETE);
	} /* end if */
	else {
		mvaddstr(0,0,M_RCADD);
	} /* end else */
	
	mvaddstr(2,0,RO_HEADER);
	wmove(stdscr,4,0);
	wprintw(stdscr,RD_INFO(route));
		
	/* Display the queues */

	cnt = 0;
	wprintw(stdscr,"\t\t");
	for ( i = 0 ; i < route->nbr_entries ; i++ ) {
		if ( cnt > 6 ) {
			wprintw(stdscr,CR_LF"\t\t");
			cnt = 0;
		} /* end if */
		wprintw(stdscr,RD_QUE(route,i));
		cnt++;
	} /* end for */
		
	free((char *) buff);
	wrefresh(stdscr);
	return RC_SUCCESS;
} /* end of display_route */

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue destroy command screen.	*/
/*									*/
/* ******************************************************************** */
#ifdef DESTROY
static void	display_dest()

{
	wclear(stdscr);	/* Erase the virtual screen */
	mvaddstr(1,22,"        Destroy queue menu.    ");
	mvaddstr(2,22,"   -----------------------------");
	mvaddstr(5,26,"1. first = last = 0");
	mvaddstr(6,26,"2. first = 0 + last != 0");
	mvaddstr(7,26,"3. first = 0 + last != 0 + time error");
	mvaddstr(8,26,"4. first = 0 + last time error");
	mvaddstr(9,26,"5. 5 items (no error)");
	mvaddstr(10,26,"6. 3 items + time error");
	mvaddstr(11,26,"7. 5 items + 0 ptr ");
	mvaddstr(12,26,"8. 4 items + time error");
	mvaddstr(21,26,"9. Exit to send menu.");
	selection_prompt();
} /* end of display_dest */

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue destroy command screen.	*/
/*									*/
/* ******************************************************************** */

static void	display_dest2()

{
	wclear(stdscr);	/* Erase the virtual screen */
	mvaddstr(1,22,"        Destroy queue menu 2.    ");
	mvaddstr(2,22,"   -----------------------------");
	mvaddstr(5,26,"1. 0-ptr + time error(9)");
	mvaddstr(6,26,"2. 0-ptr + mind = prev(11)");
	mvaddstr(7,26,"3. time error + time error(12)");
	mvaddstr(8,26,"4. 0-ptr + 0-ptr(13)");
	mvaddstr(9,26,"5. first time error + 0-ptr (14)");
	mvaddstr(10,26,"6. first->next time error (15)");

	mvaddstr(21,26,"9. Exit to send menu.");
	selection_prompt();
} /* end of display_dest2 */

/* ******************************************************************** */
/*									*/
/* The following routine destroys the queue control block.		*/
/*									*/
/* ******************************************************************** */

static void destroy(int cas)


{
	ITEM	buff;
	int	len;
	int	i;
	int	mod_id;
	int	que_ID;
	char	wrkbuff[80];
	
	if ( cr_flag == 0 ) {
		if ( que(QUE_CREATE,9999,9999,5,7,"CHAOS") != RC_SUCCESS ) {
			show_error(M_QUEUINGS);
			sleep(5);
			selection_prompt();
			return;
		} /* end if */
		else {
			len = sizeof(ITEM);
			if ( que(QUE_RESP,9999,9999,0,len,&buff) != RC_SUCCESS ){
				show_error(M_QUEUINGR);
				sleep(5);
				selection_prompt();
				return;
			} /* end if */
			else {
				if ( buff.route == NUL ) {
					show_error(M_QUEADDFAIL);
					sleep(5);
					selection_prompt();
					return;
				} /* end if */
				cr_flag = 1;
			} /* end else */
		} /* end else */
	} /* end if */
	mod_id = buff.originator;
	que_ID = 9999;
	if ( que(QUE_DESTROY,que_ID,mod_id,1,sizeof(cas),&cas)
		!= RC_SUCCESS ) {
		show_error("queue destroy err"); 
		sleep(5);
		selection_prompt();
	} /* end if */
	selection_prompt();
	return;
	
} /* end destroy */

/* ******************************************************************** */
/*									*/
/* The following routine destroys the queue.				*/
/*									*/
/* ******************************************************************** */

static void	dest_queue()

{
	display_dest();

	/* Loop here until termination is requested */

	while (1) {
		echo();
		selection = wgetch(stdscr);
		if(selection == 0x1b)
		  selection=0x39;
		noecho();

		switch ( selection - (int) '0' ) {

			case 1:
				destroy(1);
				break;
			case 2:
				destroy(2);
				break;
			case 3:
				destroy(3);
				break;
			case 4:
				destroy(4);
				break;
			case 5:
				destroy(5);
				break;
			case 6:
				destroy(6);
				break;
			case 7:
				destroy(7);
				break;
			case 8:
				destroy(8);
				break;
			case 9:
				display_send();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
	return;
} /* end of dest_queue */

/* ******************************************************************** */
/*									*/
/* The following routine destroys the queue.				*/
/*									*/
/* ******************************************************************** */

static void	dest_queue2()

{
	display_dest2();

	/* Loop here until termination is requested */

	while (1) {
		echo();
		selection = wgetch(stdscr);
		if(selection == 0x1b)
		  selection=0x39;
		noecho();

		switch ( selection - (int) '0' ) {

			case 1:
				destroy(9);
				break;
			case 2:
				destroy(10);
				break;
			case 3:
				destroy(11);
				break;
			case 4:
				destroy(12);
				break;
			case 5:
				destroy(13);
				break;
			case 6:
				destroy(14);
				break;

			case 9:
				display_send();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
	return;
} /* end of dest_queue */
#endif

/* ******************************************************************** */
/*									*/
/* The following routine sends commands.				*/
/*									*/
/* ******************************************************************** */

static void	send_commands()

{
	int	comd;
	char	wrkbuff[80];
	int move;
	display_send();

	/* Loop here until termination is requested */

	while (1) {
		/* 20030617 JIM: this is wrong: last_menu = 4; */
		echo();
		selection = wgetch(stdscr);
		if(selection == 0x1b)
		  selection=0x39;
		noecho();

		switch ( selection - (int) '0' ) {
			case 1:
				send_cmd("SHUTDOWN");
				break;
			case 2:
				send_cmd("RESET");
				break;
			case 3:
				prompt(M_COMMAND);
				getse(wrkbuff,79);
				send_cmd(wrkbuff);
				break;
#ifdef DESTROY
			case 4:
				dest_queue();
				break;
			case 5:
				dest_queue2();
				break;
#endif
			case 9:
		     /* 20030617 JIM: this is wrong: 				display_main();*/
			  display_last_menu();
				return;
				break;
			default:
				show_error(M_INVALID);
				selection_prompt();
				break;
			} /* end switch */
	} /* end while */
	return;
} /* end of send_commands */

/* ******************************************************************** */
/*									*/
/* The following routine displays the send command screen.		*/
/*									*/
/* ******************************************************************** */

static void	display_send()

{
	wclear(stdscr);		/* Erase the virtual screen */
	mvaddstr(1,22,"        Send command menu.    ");
	mvaddstr(2,22,"-----------------------------------");
	mvaddstr(5,26,"1. Send SHUTDOWN.");
	mvaddstr(7,26,"2. Send RESET.");
	mvaddstr(9,26,"3. Send free format command.");
#ifdef DESTROY
	mvaddstr(11,26,"4. Queue destroy 1.");
	mvaddstr(13,26,"5. Queue destroy 2.");
#endif
	mvaddstr(21,26,"9. Exit to main menu.");
	selection_prompt();
} /* end of display_send */

/* ******************************************************************** */
/*									*/
/* The following routine builds an event with the requested command.	*/
/*									*/
/* ******************************************************************** */

static void	send_cmd(char* cmd)

{
  EVENT	event;
  int	rou_ID;
  char	wrkbuff[80];
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;
  EVENT   *prlOutEvent  = NULL;
  int   iLen     = 0;
  /* Prompt for destination */
		
  prompt(M_DESTINATION);
  getse(wrkbuff,20);
  if((rou_ID = atoi(wrkbuff))==0)
    {
      rou_ID= tool_get_q_id(wrkbuff);
    }
  if(atoi(cmd)==0&&strstr(cmd,"SHUTDOWN")== NULL&&strstr(cmd,"RESET")==NULL)
    {
      iLen= sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK);
      prlOutEvent = (EVENT*)malloc((size_t)iLen);
      memset((void*)prlOutEvent, 0x00, iLen);
      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = 0;
      prlOutEvent->data_length  = 0; 
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,cmd);
     
      if(que(QUE_PUT,rou_ID,mod_id,1,iLen,(char *)prlOutEvent)!=RC_SUCCESS)
	{
	  show_error(M_QUEUINGS);
	  selection_prompt();
	  return;
	  
	} /* end if */
      
    }else{
      event.command=0;
      event.type = SYS_EVENT;
      if(!strcmp(cmd,"SHUTDOWN"))
	event.command=SHUTDOWN;
      if(!strcmp(cmd,"RESET"))
	event.command=RESET;
      if(event.command==0)
      event.command = atoi(cmd);

      event.originator = mod_id;
      event.retry_count = 0;
      event.data_offset = 0;
      event.data_length = 0;
	
      if ( que(QUE_PUT,rou_ID,mod_id,1,sizeof(EVENT),(char *)&event)
	   != RC_SUCCESS ) {
	show_error(M_QUEUINGS);
	selection_prompt();
	return;
      } /* end if */
      else {
	show_error(M_COMMANDS);
      } /* end else */
    }
  selection_prompt();
  return;
} /* end of send_cmd */

static void	queue_overview_loop()

{
	ITEM		*item;
	QUE_INFO	*que_info;
	int		line_count;
	int		info_count;
	int		len;
	int		return_value;
	int             move=0;
	/* Request the queue overview */
	sigflag=1;
	while(move !=0x1b)
	  {
	errno = 10;
	return_value = que(QUE_QOVER,0,mod_id,5,0,0);
	if ( return_value != RC_SUCCESS ) {
		show_error(M_QUEUINGS);
		sigflag=0;
		selection_prompt();
		return;
	} /* end if */
#if 0
	len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
	item = (ITEM *) malloc(len);
	if ( item == (ITEM *) NUL ) {
		show_error(M_MEMORY);
		selection_prompt();
		return;
	} /* end if */
#endif
len=0;
item=NULL;
	if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		free((char *) item);
		show_error(M_QUEUINGR);
		sigflag=0;
		selection_prompt();
		return;
	} /* end if */
	
	if ( item->msg_length == FATAL ) {
		free((char *) item);
		show_error(M_MEMORY);
		sigflag=0;
		selection_prompt();
		return;
	} /* end if */
	
	que_info = (QUE_INFO *) &(item->text[0]);
	wclear(stdscr);
	mvaddstr(0,0,QO_HEADER);
	mvaddstr(1,0,QO_HEAD2);
	info_count = 0;
	line_count = 3;
	while ( info_count <= que_info->entries) {
		if ( line_count > 21 ) {
			mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
			wrefresh(stdscr);
			move=wgetch(stdscr);
			while ( move != '\n'&& move != 0x20 ) 
			{
			if(move==0x1b)
			  {
			    free((char *) item);
			    sigflag=0;
			    display_last_menu();
			    return;
			  }
			move=wgetch(stdscr);
			}
			wmove(stdscr,2,0);
			wclrtobot(stdscr);
			line_count = 2;
			wrefresh(stdscr);
		} /* end if */
		wprintw(stdscr,QO_INFO(que_info->data[info_count]));
		info_count++;
		line_count++;
	} /* end while */
	free((char *) item);
	mvaddstr(23,0,"End of display, type [ESC] to return to main menu or [SPACE] for loop");
	wrefresh(stdscr);
	move=wgetch(stdscr);

	while (move != '\n'&& move != 0x20 && move !=0x1b ) 
	  {
	    move=wgetch(stdscr); 
	  }
	}
	sigflag=0;
	display_last_menu();
	return;
} /* end of queue_overview */

static void	queue_overview_loop_automatic()

{
  ITEM		*item;
  QUE_INFO	*que_info;
  int		line_count;
  int		info_count;
  int		len;
  int		return_value;
  int             move=0;
  /* Request the queue overview */
  sigflag=1;
  while(move !=0x1b)
    {
      errno = 10;
      return_value = que(QUE_QOVER,0,mod_id,5,0,0);
      if ( return_value != RC_SUCCESS ) {
	show_error(M_QUEUINGS);
	selection_prompt();
	sigflag=0;
	return;
      } /* end if */
#if 0
      len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
      item = (ITEM *) malloc(len);
      if ( item == (ITEM *) NUL ) {
	show_error(M_MEMORY);
	selection_prompt();
	sigflag=0;
	return;
      } /* end if */
#endif
len=0;
item=NULL;
      if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
	free((char *) item);
	show_error(M_QUEUINGR);
	selection_prompt();
	sigflag=0;
	return;
      } /* end if */
	
      if ( item->msg_length == FATAL ) {
	free((char *) item);
	show_error(M_MEMORY);
	selection_prompt();
	return;
      } /* end if */
	
      que_info = (QUE_INFO *) &(item->text[0]);
      wclear(stdscr);
      mvaddstr(0,0,QO_HEADER);
      mvaddstr(1,0,QO_HEAD2);
      info_count = 0;
      line_count = 3;
      while ( info_count <= que_info->entries) {
	if ( line_count > 21 ) {
	  mvaddstr(23,0,"Press [ESC] to return to main menu");
	  wrefresh(stdscr);
	  alarm(5);
	  move=wgetch(stdscr);
	  alarm(0);
	  if ( move == 0x1b ) 
	    {
	      free((char *) item);
	      display_last_menu();
	      sigflag=0;
	      return;
	    }
	  wmove(stdscr,2,0);
	  wclrtobot(stdscr);
	  line_count = 2;
	  wrefresh(stdscr);
	} /* end if */
	wprintw(stdscr,QO_INFO(que_info->data[info_count]));
	info_count++;
	line_count++;
      } /* end while */
      free((char *) item);
      mvaddstr(23,0,"Press [ESC] to return to main menu");
      wrefresh(stdscr);
      alarm(5);
      move=wgetch(stdscr);
      alarm(0);
     
    }
  display_last_menu();
  sigflag=0;
  return;
} /* end of queue_overview */
static void queue_overview_loop_refresh()

{
  ITEM		*item;
  QUE_INFO	*que_info;
  int		line_count;
  int		info_count;
  int           info_count_save;
  int		len;
  int		return_value;
  int             move=0;
  int           firstflag=0;
int i=0;
  /* Request the queue overview */
  sigflag=1;
  while(move !=0x1b)
    {
      errno = 10;
      return_value = que(QUE_QOVER,0,mod_id,5,0,0);
      if ( return_value != RC_SUCCESS ) {
	show_error(M_QUEUINGS);
	sigflag=0;
	selection_prompt();
	return;
      } /* end if */
#if 0
      len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
      item = (ITEM *) malloc(len);
      if ( item == (ITEM *) NUL ) {
	show_error(M_MEMORY);
	selection_prompt();
	return;
      } /* end if */
#endif
len=0;
item=NULL;
      if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
	free((char *) item);
	show_error(M_QUEUINGR);
	sigflag=0;
	selection_prompt();
	return;
      } /* end if */
	
      if ( item->msg_length == FATAL ) {
	free((char *) item);
	show_error(M_MEMORY);
	sigflag=0;
	selection_prompt();
	return;
      } /* end if */
	
      que_info = (QUE_INFO *) &(item->text[0]);
      wclear(stdscr);
      mvaddstr(0,0,QO_HEADER);
      mvaddstr(1,0,QO_HEAD2);
      info_count = 0;
      line_count = 3;
      while ( info_count <= que_info->entries) {
	info_count_save=info_count-line_count+3;	
	if ( line_count > 21 ) 
	  {
	    mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
	    wrefresh(stdscr);
	    move=wgetch(stdscr);
	    wmove(stdscr,22,0);
/*	wprintw(stdscr,"%d - %d",__LINE__,i++);*/
	    /***********************/
		free((char *) item);
		errno = 10;
		return_value = que(QUE_QOVER,0,mod_id,5,0,0);
		if ( return_value != RC_SUCCESS ) {
		  show_error(M_QUEUINGS);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
#if 0
		len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
		item = (ITEM *) malloc(len);
		if ( item == (ITEM *) NUL ) {
		  show_error(M_MEMORY);
		  selection_prompt();
		  return;
		} /* end if */
#endif
len=0;
item=NULL;	
		if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		  free((char *) item);
		  show_error(M_QUEUINGR);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
	
		if ( item->msg_length == FATAL ) {
		  free((char *) item);
		  show_error(M_MEMORY);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
		que_info = (QUE_INFO *) &(item->text[0]);
		wmove(stdscr,2,0);
		wclrtobot(stdscr);
		line_count = 3;
		info_count=info_count_save;
		wrefresh(stdscr); 
		while(line_count<=21&&info_count<= que_info->entries)
		  {
		    wprintw(stdscr,QO_INFO(que_info->data[info_count]));
		    line_count++;
		    info_count++;
		  }
		mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
		wrefresh(stdscr);
		line_count = 3;
	    /*******************************************************/
	    while ( move != '\n'&& move != 0x20 ) 
	      {
		if(move==0x1b)
		  {
		    free((char *) item);
		    sigflag=0;
		    display_last_menu();
		    return;
		  }
		move=wgetch(stdscr);
		/***********************/
		    free((char *) item);
		    errno = 10;
		    return_value = que(QUE_QOVER,0,mod_id,5,0,0);
		    if ( return_value != RC_SUCCESS ) {
		      show_error(M_QUEUINGS);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
#if 0
		    len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
		    item = (ITEM *) malloc(len);
		    if ( item == (ITEM *) NUL ) {
		      show_error(M_MEMORY);
		      selection_prompt();
		      return;
		    } /* end if */
#endif
len=0;
item=NULL;	
		    if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		      free((char *) item);
		      show_error(M_QUEUINGR);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
	
		    if ( item->msg_length == FATAL ) {
		      free((char *) item);
		      show_error(M_MEMORY);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
		    que_info = (QUE_INFO *) &(item->text[0]);
		    wmove(stdscr,2,0);
		    wclrtobot(stdscr);
		    line_count = 3;
		    info_count=info_count_save;
		    wrefresh(stdscr); 
		    while(line_count<=21&&info_count <= que_info->entries)
		      {
			wprintw(stdscr,QO_INFO(que_info->data[info_count]));
			wrefresh(stdscr); 
			line_count++;
			info_count++;
		      }
		    line_count = 3;
		    mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
		    wrefresh(stdscr);
		/*******************************************************/
	      }
	    wmove(stdscr,2,0);
	    wclrtobot(stdscr);
	    wrefresh(stdscr);
	  }
	wprintw(stdscr,QO_INFO(que_info->data[info_count]));
	
	info_count++;
	line_count++;
      } /* end while */
      free((char *) item);
      mvaddstr(23,0,"End of display, type [ESC] to return to main menu or [SPACE] for first page");
      wrefresh(stdscr);
      /******************************************************************************/

	    move=wgetch(stdscr);
	    /***********************/
		errno = 10;
		return_value = que(QUE_QOVER,0,mod_id,5,0,0);
		if ( return_value != RC_SUCCESS ) {
		  show_error(M_QUEUINGS);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
#if 0
		len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
		item = (ITEM *) malloc(len);
		if ( item == (ITEM *) NUL ) {
		  show_error(M_MEMORY);
		  selection_prompt();
		  return;
		} /* end if */
#endif
len=0;
item=NULL;	
		if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		  free((char *) item);
		  show_error(M_QUEUINGR);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
	
		if ( item->msg_length == FATAL ) {
		  free((char *) item);
		  show_error(M_MEMORY);
		  sigflag=0;
		  selection_prompt();
		  return;
		} /* end if */
		que_info = (QUE_INFO *) &(item->text[0]);
		wmove(stdscr,2,0);
		wclrtobot(stdscr);
		line_count = 3;
		info_count=info_count_save;
		wrefresh(stdscr); 
		while(line_count<=21&&info_count<= que_info->entries)
		  {
		    wprintw(stdscr,QO_INFO(que_info->data[info_count]));
		    line_count++;
		    info_count++;
		  }
		mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
		wrefresh(stdscr);
		line_count = 3;
	    /*******************************************************/
	    while ( move != '\n'&& move != 0x20 ) 
	      {
		if(move==0x1b)
		  {
		    free((char *) item);
		    sigflag=0;
		    display_last_menu();
		    return;
		  }
		move=wgetch(stdscr);
		/***********************/
		if(item!=NULL)
		    free((char *) item);
		    errno = 10;
		    return_value = que(QUE_QOVER,0,mod_id,5,0,0);
		    if ( return_value != RC_SUCCESS ) {
		      show_error(M_QUEUINGS);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
#if 0
		    len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
		    item = (ITEM *) malloc(len);
		    if ( item == (ITEM *) NUL ) {
		      show_error(M_MEMORY);
		      selection_prompt();
		      return;
		    } /* end if */
#endif
len=0;
item=NULL;	
		    if ( que(QUE_GETBIG,0,mod_id,5,len,(char *)&item) != RC_SUCCESS ) {
		      free((char *) item);
		      show_error(M_QUEUINGR);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
	
		    if ( item->msg_length == FATAL ) {
		      free((char *) item);
		      show_error(M_MEMORY);
		      sigflag=0;
		      selection_prompt();
		      return;
		    } /* end if */
		    que_info = (QUE_INFO *) &(item->text[0]);
		    wmove(stdscr,2,0);
		    wclrtobot(stdscr);
		    line_count = 3;
		    info_count=info_count_save;
		    wrefresh(stdscr); 
		    while(line_count<=21&&info_count <= que_info->entries)
		      {
			wprintw(stdscr,QO_INFO(que_info->data[info_count]));
			wrefresh(stdscr); 
			line_count++;
			info_count++;
		      }
		    line_count = 3;
		    mvaddstr(23,0,"Press [RETURN] or [SPACE] for next page..");
		    wrefresh(stdscr);
		/*******************************************************/
	      }
#if 0
      move=wgetch(stdscr);

      while (move != '\n'&& move != 0x20 && move !=0x1b ) 
	{
	  move=wgetch(stdscr); 
	}
      /****************************************************************************************************/
#endif   
    }
  sigflag=0;
  display_last_menu();
  return;
} /* end of queue_overview */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      if(sigflag==0)
	{
	  terminate(-1);
	}else{
	  refreshscreen=1;
	}
      break;
    case SIGPIPE:
      terminate(-1);
      break;
    case SIGCHLD:
      terminate(-1);
      break;
    case SIGTERM:
      terminate(-6);
      break;
    default    :
      terminate(-1);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/




















