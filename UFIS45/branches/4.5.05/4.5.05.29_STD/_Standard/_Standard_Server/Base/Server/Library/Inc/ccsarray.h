#ifndef _DEF_mks_version_ccsarray_h
  #define _DEF_mks_version_ccsarray_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ccsarray_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ccsarray.h 1.2 2004/07/27 16:46:52SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* File : ccsarray.h */
#ifndef __CCSARRAY_H__
#define __CCSARRAY_H__   /* perform statements in this include file only once */


#ifndef CCS_BOOL
#define CCS_BOOL int
#define CCS_TRUE    (1)
#define CCS_FALSE   (0)
#endif /* CCS_BOOL */
#ifndef CCS_SUCCESS
#define CCS_SUCCESS (0)
#endif
#ifndef CCS_FAIL
#define CCS_FAIL    (-1)
#endif
#ifndef CCS_NOT_FOUND
#define CCS_NOT_FOUND   (-7)
#endif
#ifndef CCS_ERR_NULL_POINTER
#define CCS_ERR_NULL_POINTER   (-13)
#endif
#ifndef CCS_ERR_NO_MEM
#define CCS_ERR_NO_MEM   (-14)
#endif
#ifndef CCS_ERR_DOUBLE
#define CCS_ERR_DOUBLE   (-15)
#endif
#ifndef CCS_ERR_ORDER
#define CCS_ERR_ORDER   (-16)
#endif
#ifndef CCS_ERR_NUMBER
#define CCS_ERR_NUMBER   (-17)
#endif
#define CCS_ERR_NO_KEYS     (-21)   /* CCSCedaInitKey: no key symbols defined */
#define CCS_ERR_SYM_NAME    (-22)   /* CCSCedaInitKey: len of key symbol exceeded */
#define CCS_ERR_KEY_NAME    (-23)   /* CCSCedaInitKey: len of key name exceeded */
#define CCS_ERR_KEY_BEG     (-24)   /* CCSCedaInitKey: illegel value for key begin */
#define CCS_ERR_KEY_LEN     (-25)   /* CCSCedaInitKey: illegel value for key len */
#define CCS_ERR_OP_UNKNOWN  (-26)   /* CCSCedaSubIndexList: unknown operator in CCSCedaGet */
#define CCS_ERR_KEY_SYNTAX  (-27)   /* CCSCedaInitKey: key syntax error */
#define CCS_ERR_FIELD_DESCR (-28)   /* CCSCedaCreateFDesc: Error in field description */
#define CCS_ERR_TAB_EXISTS  (-29)
#define CCS_ERR_NAM_LEN     (-30)
#define CCS_ERR_KEY_UNKNOWN (-31)
#define CCS_ERR_TAB_NOT_FOUND (-32)

#define CEDA_SYMBOL_LEN (6)


/* Begin * Structures for CCSArray routines */
/* type of compare function: */
typedef int CCS_COMPARE (const void *u, const void *a, const void *b); 
/* type of key generation function */
typedef void* CCS_KEY_PROC (const void *UserInfo, const void *DataPtr); 

typedef struct T_KEY_RECORD   /* == Element of the array == */
{
  long ArrayIndex;                    /* Index of array to data */
  void *Key;                          /* key for this item */
} KEY_RECORD;

typedef struct T_DATA_RECORD  /* == Element of the array == */
{
  long DataLen;                       /* length of the data */
  void *DataPtr;                      /* points to the data */
} DATA_RECORD;


typedef struct T_CCS_KEY      /* == the key structure == */
{
  struct T_CCS_ARRAY  *ArrBase;       /* pointer to the array strukture */
  void                *UserInfo;      /* pointer to a structure of unknown user info */
  KEY_RECORD          *Keys;          /* Start of key array */
  int                  KeyLen;        /* Length of Key */
  CCS_COMPARE         *Compare;       /* pointer to routine for compare */
  CCS_KEY_PROC        *KeyOfData;     /* how to generate a key from the data */
  long                 SearchRange1;  /* Index in Array of first Element to process */
  long                 SearchRange2;  /* Index in Array of last  Element to process */
  long                 RangeBeg1;
  long                 RangeEnd1;
  long                 RangeBeg2;
  long                 RangeEnd2;
  void                *RangeKey1;     /* First key of subrange to be processed */
  void                *RangeKey2;     /* Last key of subrange to be processed */
  void                *TmpKey;        /* i.e. for inserts */
  long                 NextIndex;     /* Index for Key Array for step by step operations */
  struct T_CCS_KEY    *NextKey;       /* pointer to the next key */
} CCS_KEY;

typedef struct T_CCS_ARRAY    /* == the array base structure == */
{
  long                 Count;         /* Number of elements in array */
  DATA_RECORD         *CcsBase;       /* Start of data array */
  long                 MaxCount;      /* #no of elements to allocate space for */
  long                 IncCount;      /* #increment of elements to allocate when full */
  int                  MustSortKey;   /* Flag: ==1: Key must be resorted */
  long                 NextIndexA;    /* Index for Array for step by step operations */
  struct T_CCS_KEY    *FirstKey;      /* pointer to the first key */
} CCS_ARR;
/* End * Structures for CCSArray routines */

/* Begin * Structures for CEDA routines */
typedef struct T_CEDA_FDESC   /* == the CEDA field description == */
{
  char                FINA[5];       /* field name of this field */
  int                 FITY;          /* 0=?, 1=C, 2=VC2, 3=N  */
  int                 FELE;          /* Length of field */
  int                 SYST;          /* 1= field in shared memory */
  int                 TYPE;          /* 0=?, 1=TRIM, 2=DATE, 3=LONG  */
} CEDA_FDESC;

typedef struct T_CEDA_KDESC   /* == the CEDA key description == */
{
  char                KeyName[5];    /* field name of this key */
  int                 Begin;         /* Offset of field in list  */
  unsigned int        Len;           /* Length of field */
  unsigned int        FieldNum;      /* Number of field in data record */
  int                 FieldType;     /* 0=?, 1=TRIM, 2=DATE, 3=LONG  */
} CEDA_KDESC;

typedef struct T_CEDA_COMBI_KEY    /* == the CEDA key base structure == */
{
  char                 KeySym[7];     /* symbol Key */
  CEDA_KDESC           KeyDescr;      /* Pointer to Key attributes */
  CCS_KEY             *Key;           /* the CCS_ARR key */
  struct T_CEDA_COMBI_KEY *Next;   
} CEDA_COMBI_KEY;

typedef struct T_CEDA_ARRAY   /* == the CEDA array base structure == */
{
  CCS_ARR             *CedaBase;      /* Base of data array */
  char                *FieldNames;    /* list of field names loaded into the array */
  int                  NumOfFields;   /* Number of Fields for this array */
  CEDA_FDESC          *FieldDescr;    /* Pointer list of Fields with CCSCeda attributes */
  CEDA_COMBI_KEY      *FirstCombiKey; /* Pointer list of Keys with CCSCeda attributes */
} CEDA_ARRAY;

typedef struct T_USR_ARRAY    /* == the CEDA data base structure == */
{
  void               **UsrBase;       /* Base of data pointer */
  long                 Count;         /* Number of data pointer */
  int                  Type;          /* Type of data pointer */
} USR_ARRAY;
/* End   * Structures for CEDA routines */

/* Begin * CCSArray routines */
extern int        CCSArrayAddUnsort( CCS_KEY      *prpKeyBase, 
                                     const void   *pvpData, 
                                     const long    lpDataLen);
extern void       CCSArrayArrDestroy(CCS_KEY     **prpKeyBase);
extern void      *CCSArrayArrFirst(  CCS_KEY      *prpKeyBase);
extern void      *CCSArrayArrNext(   CCS_KEY      *prpKeyBase);
extern int        CCSArrayDelete(    CCS_KEY      *prpKeyBase,
                                     const void   *prpDelkey);
extern long       CCSArrayGetCount(  CCS_KEY      *prpKeyBase);
extern void      *CCSArrayGetData(   CCS_KEY      *prpKeyBase,
                                     const void   *pvpSearchKey);
extern void      *CCSArrayGetFirst(  CCS_KEY      *prpKeyBase);
extern void      *CCSArrayGetFirstIn(CCS_KEY      *prpKeyBase,
                                     const void   *pvpKey1,
                                     const void   *pvpKey2);
extern void      *CCSArrayGetNext(   CCS_KEY      *prpKeyBase);
extern void      *CCSArrayGetNextIn( CCS_KEY      *prpKeyBase,
                                     const void   *pvpKey1,
                                     const void   *pvpKey2);
extern void       CCSArrayKeyDestroy(CCS_KEY     **prpKeyBase);
extern void       CCSArrayKeySort(   CCS_KEY      *prpKeyBase);
extern CCS_KEY   *CCSArrayInit(      CCS_KEY      *prpKeyBase, 
                                     void         *pvpUserInfo,
                                     const int     ipKeyLen,
                                     CCS_COMPARE  *pppCompareFkt,  
                                     CCS_KEY_PROC *pppKeyOfData,
                                     const long    lpMaxCount,    
                                     const long    lpIncCount,
                                     int          *pipRC);
extern int        CCSArrayInsert(    CCS_KEY      *prpKeyBase, 
                                     const void   *pvpData, 
                                     const long    lpDataLen);
extern int        CCSArrayTestKeys(  CCS_KEY      *prpKeyBase);
extern int        CCSArrayTestOrder( CCS_KEY      *prpKeyBase);
/* End   * CCSArray routines */

/* Begin * CEDA routines */
extern int        CCSCedaAddUnsort(  char         *pcpTabName,
                                     const void   *pvpData);
extern int        CCSCedaDelete(     char         *pcpTabName, 
                                     char         *pcpCombiKey);
extern void       CCSCedaDelUsr(     USR_ARRAY    *prpDataPtr);
extern int        CCSCedaDestroy(    char         *pcpTabName);
extern long       CCSCedaGetCount(   char         *pcpTabName);
extern int        CCSCedaGetData(    char         *pcpTabName, 
                                     char         *pcpCombiKey,
                                     USR_ARRAY    *prpDataPtr);
extern int        CCSCedaInit(       char         *pcpTabName,
                                     char         *pcpFields, 
                                     char         *pclFiTyS, 
                                     char         *pclFeLeS, 
                                     char         *pclSystS, 
                                     char         *pclTypes, 
                                     long          lpStartCount, 
                                     long          lpIncCount);
extern int        CCSCedaInitKey(    char         *pcpTabName,
                                     char         *pcpSymbols,
                                     char         *pcpKeyList, 
                                     char         *pcpKeyStart,
                                     char         *pcpKeyLen);
extern int        CCSCedaInsert(     char         *pcpTabName,
                                     const void   *pvpData);
extern int        CCSCedaKeySort(    char         *pcpTabName);
extern int        CCSCedaTestKeys(   char         *pcpTabName);
extern int        CCSCedaTestOrder(  char         *pcpTabName);
extern char *CopyNextFieldSkipBlank(char *s1, char c, char *s3);
/* End   * CEDA routines */

#endif /* __CCSARRAY_H__ */
