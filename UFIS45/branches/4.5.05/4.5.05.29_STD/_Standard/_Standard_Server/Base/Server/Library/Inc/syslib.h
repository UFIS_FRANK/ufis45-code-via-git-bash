#ifndef _DEF_mks_version_syslib_h
  #define _DEF_mks_version_syslib_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_syslib_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/syslib.h 1.4 2004/12/21 17:13:58SGT akl Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/*                                                                          */
/*	S Y S T E M   M E M O R Y   Defines                                     */
/*                                                                          */
/****************************************************************************/
#ifndef _SYSLIB_H_
#define _SYSLIB_H_


/*	Global Data */
typedef struct
{
	char	cProject[4];		/* Project name */
	long	lVersion;			/* SYSLIB version */
	time_t	lStarted;			/* CEDA start time */
	time_t	lUpdated;			/* last update time */
	long	lTotLenSSM;			/* Total size of SSM */
	long	lUsedLenSSM;		/* Used size of SSM */
	long	lOffSystab;			/* Offset SYSTAB in SSM */
	long	lOffDbStruct;		/* Offset DB structure array */
	long	lOffFieldStruct;	/* Offset field structure array */
	long	lOffDbData;			/* Offset DB data */
	long	lCntSystab;			/* No of records in SYSTAB */
	long	lCntTables;			/* No of tables in DB area */
} SYSH_GLOBAL;
#define SYSH_GLOBAL_SIZE ((sizeof(SYSH_GLOBAL) + 3) / 4) * 4



/*	Systab Entry */
typedef struct
{
	char	urno[11];	/* unique reord number */
	char	tana[33];	/* table name */
	char	fina[17];	/* field name */
	char	fity[4];	/* field type */
	char	fele[9];	/* field length */
	char	msgt[81];	/* message text, number */
	char	addi[1025];	/* additional information */
	char	type[5];	/* field contents type */
	char	refe[49];	/* basic data */
	char	datr[49];	/* source of field */
	char	indx[2];	/* index type */
	char	defa[65];	/* default value */
	char	taty[11];	/* table type */
	char	syst[2];	/* shared memory flag */
	char	proj[129];	/* project list */
	char	stat[33];	/* mandatory, optional */
	char	depe[65];	/* reference to other fields */
	char	guid[21];	/* GUI information */
	char	sort[5];	/* sort number */
	char	cont[33];	/* container */
	char	labl[65];	/* GUI label */
	char	fmts[21];	/* format string */
	char	gtyp[3];	/* GUI control type */
	char	gsty[21];	/* GUI window style */
	char	glfl[65];	/* GUI filter list */
	char	gdsr[33];	/* GUI data source */
	char	gcfl[33];	/* GUI control constraint fields */
	char	orie[2];	/* orientation */
	char	reqf[2];	/* required field flag */
	char	kont[129];	/* kontext sensitive help */
	char	logd[33];	/* table name for logging */
}SYSH_SYSTAB;
#define SYSH_SYSTAB_SIZE ((sizeof(SYSH_SYSTAB) + 3) / 4) * 4



/*	DB structs */
typedef struct
{
	char	pcTabName[33];		/* Table name */
	long	lCntTotLines;		/* Number of total lines */
	long	lCntUsedLines;		/* Number of used lines */
	long	lCntFields;			/* Number of fields */
	long	lOffStart;			/* Offset table data in SSM */
	long	lOffFieldStruct;	/* Offset field structure array */
	long	lLenLine;			/* Size of one line */
} SYSH_DBSTRUCT;
#define SYSH_DBSTRUCT_SIZE ((sizeof(SYSH_DBSTRUCT) + 3) / 4) * 4



/*	Field structs */
typedef struct
{
	char	pcFieldName[33];	/* Field name */
	long	lLenField;			/* Size of field */
	long	lOffStart;			/* Offset field data in line */
	long	lIndexSystab;		/* Index of field defines in SYSTAB */
} SYSH_FIELDSTRUCT;
#define SYSH_FIELDSTRUCT_SIZE ((sizeof(SYSH_FIELDSTRUCT) + 3) / 4) * 4



/*	Control Shared Memory */
typedef struct
{
	char	cProject[4];		/* Project name */
	long	lVersion;			/* SYSLIB version */
	time_t	lStarted;			/* CEDA start time */
	long	lSSMId;				/* Shared memory id */
	long	lCurrentUser;		/* Current number of users */
	long	lReorg;				/* Reorg flag */
	long	lSEMId;				/* semaphore id */
} SYSH_CONTROL;
#define SYSH_CONTROL_SIZE ((sizeof(SYSH_CONTROL) + 3) / 4) * 4



/*	Field List */
typedef struct
{
	char	pclFieldName[33];	/* Field name */
	int	ilLenField;		/* Field size */
	int	ilOffStart;		/* Field offset */
	int	ilLenValue;		/* Value size */
	char	*pclFieldValue;		/* Field value */
} SYSH_FIELDLIST;



/*	Field List 2 */
#define MAXFIELDSIZE 1024
typedef struct
{
	char	pclFieldName[33];	/* Field name */
	int	ilLenField;		/* Field size */
	int	ilOffStart;		/* Field offset */
	int	ilLenValue;		/* Value size */
	char	pclFieldValue[MAXFIELDSIZE];	/* Field value */
} SYSH_FIELDLIST2;



/*	Semaphore Operation Buffer */
typedef struct
{
	short	sem_num;			/* Semaphore number */
	short	sem_op;				/* Semaphore op-code */
	short	sem_flg;			/* Semaphore flag */
} SYSH_SEMBUF;

#ifndef _WINNT
union semun
{
	int val;
	struct semid_ds *buf;
	unsigned short *array;
};
#endif

#define SYSH_NO_SYSTAB_FIELDS 31

/* Prototypes */
int syslibCreateSMS(char *pclTable);
int syslibDestroySMS(char *pclTable);
int syslibSearchValidDbData(char *pcpTableName,
                            char *pcpInFieldList,char *pcpInFieldValues,
                            char *pcpOutFieldList,char *pcpOutFieldValues,
                            int *pipCount, char *pcpLineSeperator,
                            char *pcpDateFields, char *pcpTime);
int syslibSearchDbData(char *pcpTableName,
                       char *pcpInFieldList,char *pcpInFieldValues,
                       char *pcpOutFieldList,char *pcpOutFieldValues,
                       int *pipCount, char *pcpLineSeperator);
int syslibGetFieldList(SYSH_DBSTRUCT *psysDbStruct, 
		       SYSH_FIELDSTRUCT *psysFieldStruct,
		       char *pclFieldList,char *pclFieldValues,
		       SYSH_FIELDLIST2 *pFieldDesc);
int syslibReloadDbData(char *pclTableName);
int syslibSearchValidSystabData(char *pcpProject,
                                char *pcpInFieldList, char *pcpInFieldValues,
                                char *pcpOutFieldList, char *pcpOutFieldValues,
                                int *pipCount, char *pcpLineSeperator,
                                char *pcpDateFields, char *pcpTime);
int syslibSearchSystabData(char *pclProject,
			   char *pclInFieldList,char *pclInFieldValues,
			   char *pclOutFieldList,char *pclOutFieldValues,
			   int *pilCount,char *pclLineSeperator);
int syslibGetSystabFieldList(char *pclFieldList,char *pclFieldValues,
			     SYSH_FIELDLIST *pFieldDesc);

int syslibUpdateDbData(char clFkt,char *pclTableName,char *pclKey,
					   char *pclKeyValue);


#endif
