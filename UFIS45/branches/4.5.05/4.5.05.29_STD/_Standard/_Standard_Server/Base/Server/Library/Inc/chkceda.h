#ifndef _DEF_mks_version_chkceda_h
  #define _DEF_mks_version_chkceda_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkceda_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkceda.h 1.2 2004/07/27 16:46:57SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*	The Master include file.											*/
/*																*/
/*	Program		:	chkceda										*/
/*	Revision date	:												*/
/*	Author		:	jmu											*/
/*																*/
/*	NOTE			:	This should be the only include file for your program	*/
/*	**********************************************************************	*/

#include	"chkdef.h"

#ifndef __CHKCEDA_INC
#define __CHKCEDA_INC


mode_t	plGModeBufer[] = {	S_IRWXU,S_IRUSR,S_IWUSR,S_IXUSR,S_IRWXG,S_IRGRP,S_IWGRP,S_IXGRP,
						S_IRWXO,S_IROTH,S_IWOTH,S_IXOTH	};

char	*pcGModeString[] = {
	"ALL Rights for OWNER ",
	"READ for OWNER ",
	"WRITE for OWNER  ",
	"EXECUTE for OWNER ",
	"ALL Rights for GROUP ",
	"READ for GROUP ",
	"WRITE for GROUP  ",
	"EXECUTE for GROUP ",
	"ALL Rights for ALL ",
	"READ for ALL ",
	"WRITE for ALL  ",
	"EXECUTE for ALL "
	};
	
#endif

