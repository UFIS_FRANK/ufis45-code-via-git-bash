#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/demfnd.c 1.7 2005/03/11 00:42:22SGT hag Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Frank Scheibel                                            */
/* Date           :                                                           */
/* Description    :           
                                                */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/*static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I demfnd.c 4.5.1.2 / 03/09/17 / HEB, HAG";*/
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
 
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "fditools.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>


#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#define CMD_SND     (1)
#define CMD_SSD     (2)
#define CMD_AFL     (3)
#define CMD_IRT     (4)
#define CMD_URT     (5)
#define CMD_DRT     (6)
#define CMD_ANSWER  (7)

#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define JODFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJodArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRueArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PARFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgParArray.plrArrayFieldOfs[ipFieldNo-1]])
#define TPLFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgTplArray.plrArrayFieldOfs[ipFieldNo-1]])

extern int get_no_of_items(char *s);

#define ARR_NAME_LEN           (8)
#define ARR_FLDLST_LEN         (256)


static char *prgCmdTxt[] = {     "SND",    "SSD",    "AFL",    "IRT",    "URT",    "DRT",  "ANSWER",  NULL };
static int    rgCmdDef[] = {  CMD_SND , CMD_SSD , CMD_AFL , CMD_IRT , CMD_URT , CMD_DRT ,  CMD_ANSWER, 0 };


/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

#define DEM_FIELDS "URNO,DEBE,DEEN,RETY,URUD"

#define JOB_SORTFIELDS "URNO"
#define JOB_SORTFIELDS2 "UDEM"
#define JOB_SORTDIR "D"
#define JOB_FIELDS "URNO"

#define JOD_SORTFIELDS "UDEM"
#define JOD_SORTDIR "D"
#define JOD_FIELDS "UDEM,UJOB"

#define RUD_SORTFIELDS "URNO"
#define RUD_SORTDIR "D"
#define RUD_FIELDS "URNO,URUE,HOPO"

#define RUE_SORTFIELDS "URNO"
#define RUE_SORTDIR "D"
#define RUE_FIELDS "URNO,UTPL,HOPO"

#define PAR_SORTFIELDS "NAME"
#define PAR_SORTDIR "D"
#define PAR_FIELDS "APPL,NAME,URNO,HOPO,PTYP"

#define TPL_SORTFIELDS "URNO"
#define TPL_SORTDIR "D"
#define TPL_FIELDS "TNAM,URNO,HOPO"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static	int		igStartUpMode = TRACE;
static	int		igRuntimeMode = 0;
static EVENT  *prgOutEvent      = NULL;
struct _arrayinfo
{
    HANDLE   rrArrayHandle;
    char     crArrayName[ARR_NAME_LEN+1];
    char     crArrayFieldList[ARR_FLDLST_LEN+1];
    long     lrArrayFieldCnt;
    char    *pcrArrayRowBuf;
    long     lrArrayRowLen;
    long    *plrArrayFieldOfs;
    long    *plrArrayFieldLen;
    HANDLE   rrIdx01Handle;
    char     crIdx01Name[ARR_NAME_LEN+1];
    char     crIdx01FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx01FieldCnt;
    char    *pcrIdx01RowBuf;
    long     lrIdx01RowLen;
    long    *plrIdx01FieldPos;
    long    *plrIdx01FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgJobArray;
static ARRAYINFO rgJodArray;
static ARRAYINFO rgDemArray;
static ARRAYINFO rgRudArray;
static ARRAYINFO rgRueArray;
static ARRAYINFO rgParArray;
static ARRAYINFO rgTplArray;

static long  lgEvtCnt      = 0;

static int igDemUrno;
static int igDemUtpl;
static int igDemRety;
static int igDemUrud;
static int igDemStat;

static int igJobUrno;
static int igJobUdem;

static int igJodUdem;
static int igJodUjob;

static int igRueUrno;
static int igRueUtpl;

static int igRudUrno;
static int igRudUrue;

static int igParAppl;
static int igParUrno;
static int igParName;
static int igParHopo;
static int igParPtyp;

static int igTplHopo;
static int igTplUrno;
static int igTplTnam;


static int   igRouter = 1200;

static char * pcgFieldList = NULL;
static char  cgRety[40];
static char  cgTabEnd[8];                       /* default table extension */

static char cgJobTab[14];
static char cgJodTab[14];
static char cgDemTab[14];
static char cgRudTab[14];
static char cgRueTab[14];
static char cgParTab[14];
static char cgTplTab[14];

static long    lgUrnoListLen   = 0 ;
static char   *pcgUrnoListBuf  = NULL ;  

static char cgHopo[8]; /* default home airport    */
static char cgTdi1[24]; /* UTC-Local Difference */
static char cgTdi2[24]; /* UTC-Local Difference */
static char cgTich[24]; /* UTC-Local Difference */
static long lgLoadOffSetStart;
static long lgLoadOffSetEnd;
static time_t tgCurrTime;
static time_t tgOldCurrTime;
static char cgWithoutALID[5] = "\0";
static char cgDemFields[51];
static char cgJobFields[21];

static BOOL bgTestCurrTime;	
static BOOL bgUseJODTAB=TRUE;
static BOOL bgUseRUDTAB=TRUE;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int InitDemfnd();
static int  SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, ARRAYINFO *prpArrayInfo,BOOL bpFill,BOOL bpUseTrigger);

static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
		     CMDBLK *prpCmdblk, char *pcpSelection, 
		     char *pcpFields, char *pcpData);
 
static int SendToJobHdl(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData);

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo);

static int ProcessSND();

static int  GetFieldLength(char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpTana, char *pcpFieldList, long *pipLen);
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);

static int TimeToStr(char *pcpTime,time_t lpTime);
static int StrToTime(char *pcpTime,time_t *plpTime);

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo);

static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest);
static void TrimRight(char *pcpBuffer);
static void ToUpper(char *pcpText);
static int LocalToUtc(char *pcpTime);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer);

static int  SaveIndexInfo(ARRAYINFO *prpArrayInfo);


static void	Terminate(int ipSleep);            /* Terminate program      */

static int	Reset(void);                       /* Reset program          */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int  GetCommand(char *pcpCommand, int *pipCmd);
static int	GetDebugLevel(char *, int *);

static int FillParTab();
static BOOL FindJob ( char *pcpUdem );
static int GetUtpl ( char *pcpDemRow, char *pcpUtpl ) ;


/**********Externe Funktionen*********/
extern void GetServerTimeStamp(char *pcpType, int ipFormat,long lpTimeDiff, char *pcpTimeStamp);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRc = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilOldDebugLevel;

    INITIALIZE;			/* General initialization	*/

    debug_level = TRACE;

    dbg(TRACE,"MAIN: version <%s>",mks_version);

    /* Attach to the MIKE queues */
    do
    {
	ilRc = init_que();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
	ilRc = init_db();
	if (ilRc != RC_SUCCESS)
	{
	    check_ret(ilRc);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
	sleep(60);
	exit(2);
    }else{
	dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/demfnd",getenv("BIN_PATH"));
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    /* 20000619 bch start */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    }
    /* 20000619 bch end */


    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
	dbg(DEBUG,"MAIN: waiting for status switch ...");
	HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
	dbg(TRACE,"MAIN: initializing ...");
	if(igInitOK == FALSE)
	{
	    ilRc = InitDemfnd();
	    if(ilRc != RC_SUCCESS)
	    {
		dbg(TRACE,"InitDemfnd: init failed!");
	    } /* end of if */
	}/* end of if */
    } else {
	Terminate(0);
    }/* end of if */


    dbg(TRACE,"MAIN: initializing OK");

    debug_level = igRuntimeMode;

    for(;;)
    {
	ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);

	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */

	prgEvent = (EVENT *) prgItem->text;
				
	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
				/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
			
	    lgEvtCnt++;

	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    break;	
		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
		    HandleQueues();
		    break;	
		case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(0);
		    break;	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    break;	
		case	REMOTE_DB :
				/* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
		    Terminate(0);
		    break;
					
		case	RESET		:
		    ilRc = Reset();
		    break;
					
		case	EVENT_DATA	:
		    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
		    {
			ilRc = HandleData();
			if(ilRc != RC_SUCCESS)
			{
			    HandleErr(ilRc);
			}/* end of if */
		    }else{
			dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
			DebugPrintItem(TRACE,prgItem);
			DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case    222:
		    ProcessSND();
		    break;
		case    100:
			SaveIndexInfo (&rgJobArray);
			break;
		case    101:
			if ( bgUseJODTAB )
				SaveIndexInfo (&rgJodArray);
			break;
		case    102:
			SaveIndexInfo (&rgDemArray);
			break;
		case    103:
			if ( bgUseRUDTAB )
				SaveIndexInfo (&rgRudArray);
			break;
		case    104:
			if ( bgUseRUDTAB )
				SaveIndexInfo (&rgRueArray);
			break;
		case    105:
			SaveIndexInfo (&rgParArray);
			break;
		case    106:
			SaveIndexInfo (&rgTplArray);
			break;
		case    333:
		    FillParTab();
		    break;
		default			:
		    dbg(TRACE,"MAIN: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;

	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
		
	} /* end else */
		
    } /* end for */
	
    /*exit(0);*/
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitFieldIndex()
{
    int ilRc = RC_SUCCESS;
    int ilLine,ilCol,ilPos;

    FindItemInList(cgDemFields,"URNO",',',&igDemUrno,&ilCol,&ilPos);
    FindItemInList(cgDemFields,"UTPL",',',&igDemUtpl,&ilCol,&ilPos);
    FindItemInList(cgDemFields,"RETY",',',&igDemRety,&ilCol,&ilPos);
    FindItemInList(cgDemFields,"URUD",',',&igDemUrud,&ilCol,&ilPos);
    FindItemInList(cgDemFields,"STAT",',',&igDemStat,&ilCol,&ilPos);

    FindItemInList(cgJobFields,"URNO",',',&igJobUrno,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UDEM",',',&igJobUdem,&ilCol,&ilPos);

    FindItemInList(JOD_FIELDS,"UDEM",',',&igJodUdem,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"UJOB",',',&igJodUjob,&ilCol,&ilPos);

    FindItemInList(RUD_FIELDS,"URNO",',',&igRudUrno,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"URUE",',',&igRudUrue,&ilCol,&ilPos);

    FindItemInList(RUE_FIELDS,"URNO",',',&igRueUrno,&ilCol,&ilPos);
    FindItemInList(RUE_FIELDS,"UTPL",',',&igRueUtpl,&ilCol,&ilPos);

    FindItemInList(PAR_FIELDS,"APPL",',',&igParAppl,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"URNO",',',&igParUrno,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"NAME",',',&igParName,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"HOPO",',',&igParHopo,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"PTYP",',',&igParPtyp,&ilCol,&ilPos);

    FindItemInList(TPL_FIELDS,"HOPO",',',&igTplHopo,&ilCol,&ilPos);
    FindItemInList(TPL_FIELDS,"URNO",',',&igTplUrno,&ilCol,&ilPos);
    FindItemInList(TPL_FIELDS,"TNAM",',',&igTplTnam,&ilCol,&ilPos);


    return ilRc;
}

static int InitDemfnd()
{
    int	ilRc = RC_SUCCESS, ilItem;
    char pclSection[64];
    char pclKeyword[64];
    char pclSelection[1024];
    char pclSqlBuf[2560];
    char pclDataArea[2560];
    char pclErr[2560];
    short slCursor;
    short slSqlFunc;
    char pclJtytab[10];
    char pclApttab[10];
    long pllAddFieldLens[2], llFldLen;
    char pclAddFields[256];
    char clCurrTime[20], *pclJobSort;

    /* read TableExtension from SGS.TAB */
    if(ilRc == RC_SUCCESS)
    {
	sprintf(pclSection,"ALL");
	sprintf(pclKeyword,"TABEND");

	ilRc = tool_search_exco_data(pclSection, pclKeyword, cgTabEnd);
	if (ilRc != RC_SUCCESS || strlen(cgTabEnd) <= 0)
	{
	    dbg(TRACE,"InitDemfnd() Error reading TableExtension: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
	    ilRc = RC_FAIL;
	    Terminate(30);
	}
	else
	{
	    sprintf(pclJtytab,"JTY%s",cgTabEnd);
	    sprintf(pclApttab,"APT%s",cgTabEnd);
	    dbg(TRACE,"InitDemfnd() TableExtension = <%s>",cgTabEnd);
	}
    }
	
    /* read HomeAirport from SGS.TAB */
    if(ilRc == RC_SUCCESS)
    {
	memset((void*)&cgHopo[0], 0x00, 8);

	sprintf(pclSection,"SYS");
	sprintf(pclKeyword,"HOMEAP");

	ilRc = tool_search_exco_data(pclSection, pclKeyword, cgHopo);
	if (ilRc != RC_SUCCESS || strlen(cgHopo) <= 0)
	{
	    dbg(TRACE,"InitDemfnd() Error reading HomeAirport: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
	    ilRc = RC_FAIL;
	    Terminate(30);
	}
	else
	{
	    dbg(TRACE,"InitDemfnd() HomeAirport = <%s>",cgHopo);
	}
    }

    if(ilRc == RC_SUCCESS)
    {
	GetDebugLevel("STARTUP_MODE", &igStartUpMode);
	GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;
	
    if(ilRc == RC_SUCCESS)
    {
	slSqlFunc = START;
	slCursor = 0;
	sprintf(pclSqlBuf,"SELECT TICH,TDI1,TDI2 FROM %s WHERE APC3='%s'",pclApttab,cgHopo);
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != DB_SUCCESS)
	{
	    get_ora_err(ilRc,pclErr);
	    dbg(TRACE,"InitDemfnd() Error reading UTC-Local difference in APTTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,pclSqlBuf);
	    ilRc = RC_FAIL;
	}
	else
	{
	    strcpy(cgTdi1,"60");
	    strcpy(cgTdi2,"60");
	    get_fld(pclDataArea,0,STR,14,cgTich);
	    get_fld(pclDataArea,1,STR,14,cgTdi1);
	    get_fld(pclDataArea,2,STR,14,cgTdi2);
	    TrimRight(cgTich);
	    if (*cgTich != '\0')
	    {
		TrimRight(cgTdi1);
		TrimRight(cgTdi2);
		sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
		sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
	    }
	    dbg(TRACE,"InitDemfnd() Read %s -> TICH: <%s> TDI1: <%s> TDI2 <%s>",pclApttab,cgTich,cgTdi1,cgTdi2);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;
    }
	
    if(ilRc == RC_SUCCESS)
    {
	char pclTmpText[100];
	char pclTmpTime[64];
	int  ilOffSet = 0;
	  
	tgCurrTime = time(NULL);

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);

	bgTestCurrTime = FALSE;

	sprintf(pclSection,"MAIN");
	sprintf(pclKeyword,"CURRENT_TIME");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    StrToTime(pclTmpText,&tgCurrTime);
	    dbg(TRACE,"InitDispo() Reading Curren Time <%s>" ,pclTmpText);
	    bgTestCurrTime = TRUE;
	}
	
	tgOldCurrTime = tgCurrTime;

	sprintf(pclKeyword,"LOAD_OFFSET_START");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    lgLoadOffSetStart = ilOffSet*60;
	}
	else
	{
	    dbg(TRACE,"InitDemfnd() Error reading [%s] %s in demfnd.cfg -" ,pclSection,pclKeyword);
	    lgLoadOffSetStart = 60*60;
	}
	sprintf(pclKeyword,"LOAD_OFFSET_END");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    lgLoadOffSetEnd = ilOffSet*60;
	}
	else
	{
	    dbg(TRACE,"InitDemfnd() Error reading [%s] %s in demfnd.cfg -" ,pclSection,pclKeyword);
	    lgLoadOffSetEnd = 120*60;
	}
	cgRety[0] = '\0';
	sprintf(pclSection,"MAIN");
	sprintf(pclKeyword,"RESOURCETYPE");
	if(ReadConfigEntry(pclSection,pclKeyword,pclSqlBuf) != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd() Error reading [%s] %s in demfnd.cfg -" ,pclSection,pclKeyword);
	    strcpy(cgRety,"'100'");
	}
	else
	{
		ilItem = 1;
		while ( get_real_item(pclDataArea, pclSqlBuf, ilItem ) > 0 ) 
		{
			pclDataArea[3] = '\0';
			if ( ilItem>1 )
				sprintf ( &(cgRety[strlen(cgRety)]), ",'%s'",pclDataArea  );
			else
				sprintf ( &(cgRety[strlen(cgRety)]), "'%s'", pclDataArea );
			ilItem++;
		}
		dbg(TRACE,"InitDemfnd; RESOURCETYPE <%s>  cgRety<%s>" ,pclSqlBuf, cgRety);
	}

	/*ASSIGN_WITHOUT_ALID*/
	sprintf(pclSection,"MAIN");
	sprintf(pclKeyword,"ASSIGN_WITHOUT_ALID");
	if(ReadConfigEntry(pclSection,pclKeyword,cgWithoutALID) != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd() Error reading [%s] %s in demfnd.cfg -" ,pclSection,pclKeyword);
	    strcpy(cgWithoutALID,"YES");
	}
	dbg(TRACE,"ASSIGN_WITHOUT_ALID: <%s>",cgWithoutALID);

    }
    
	
    if(ilRc == RC_SUCCESS)
    {
	sprintf(cgJobTab,"JOB%s",cgTabEnd);
	sprintf(cgDemTab,"DEM%s",cgTabEnd);
	sprintf(cgJodTab,"JOD%s",cgTabEnd);
	sprintf(cgRueTab,"RUE%s",cgTabEnd);
	sprintf(cgRudTab,"RUD%s",cgTabEnd);
	sprintf(cgParTab,"PAR%s",cgTabEnd);
	sprintf(cgTplTab,"TPL%s",cgTabEnd);
    }
	

	if ( CheckDBTable ( cgJodTab, cgConfigFile ) != RC_SUCCESS )
		bgUseJODTAB = FALSE;
    dbg(TRACE,"InitDemfnd: UseJODTAB <%d>",bgUseJODTAB);

    if(ilRc == RC_SUCCESS)
    {
	ilRc = CEDAArrayInitialize(20,20);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
    if(ilRc == RC_SUCCESS)
    {
	dbg(TRACE,"InitDemfnd: SetArrayInfo JOBTAB start");
	strcpy ( cgJobFields, JOB_FIELDS );
	if ( GetFieldLength(cgJobTab,"UDEM",&llFldLen) == RC_SUCCESS )
	{
		strcat ( cgJobFields, "," );
		strcat ( cgJobFields, "UDEM" );
	}
	else
	{
		dbg ( TRACE,"InitDemfnd: field JOBTAB.UDEM not in DB configuration" );
	}
	pclJobSort = bgUseJODTAB ? JOB_SORTFIELDS : JOB_SORTFIELDS2;
	ilRc = SetArrayInfo(cgJobTab,cgJobTab,cgJobFields,NULL,NULL,"JOBIDX1",pclJobSort,JOB_SORTDIR,&rgJobArray,FALSE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: SetArrayInfo JOBTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgJobArray);                

	    dbg(TRACE,"InitDemfnd: SetArrayInfo JOBTAB OK");
	}/* end of if */

	if ( bgUseJODTAB )
	{
		dbg(TRACE,"InitDemfnd: SetArrayInfo JODTAB start");
		ilRc = SetArrayInfo(cgJodTab,cgJodTab,JOD_FIELDS,NULL,NULL,"JODIDX1",JOD_SORTFIELDS,JOD_SORTDIR,&rgJodArray,FALSE,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemfnd: SetArrayInfo JODTAB failed <%d>",ilRc);
		}
		else
		{
			DebugPrintArrayInfo(DEBUG,&rgJodArray);
			dbg(TRACE,"Init_demfnd: SetArrayInfo JODTAB OK");
		}/* end of if */
	}
	/* Initialising DEMTAB Array */
	dbg(TRACE,"InitDemfnd: SetArrayInfo DEMTAB start");
	strcpy ( cgDemFields, DEM_FIELDS );
	if ( GetFieldLength(cgDemTab,"UTPL",&llFldLen) == RC_SUCCESS )
	{
		strcat ( cgDemFields, "," );
		strcat ( cgDemFields, "UTPL" );
	}
	else
	{
		dbg ( TRACE,"InitDemfnd: field DEMTAB.UTPL not in DB configuration" );
	}
	strcpy(pclAddFields,"STAT");
	pllAddFieldLens[0] = 4;
	pllAddFieldLens[1] = 0;

	ilRc = SetArrayInfo(cgDemTab,cgDemTab,cgDemFields,pclAddFields,pllAddFieldLens,"DEMIDX1",pclAddFields,"D",&rgDemArray,FALSE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: SetArrayInfo DEMTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDemArray);
	    dbg(TRACE,"Init_demfnd: SetArrayInfo DEMTAB OK");
	}
	strcat ( cgDemFields, "," );
	strcat ( cgDemFields, pclAddFields );
	
	/*  if UTPL in DEMTAB and filled for all records 
		(LoadFormat = DEM in demhdl.cfg), we will not create RUD- and RUE-array */
    sprintf ( pclDataArea, "%s/demhdl.cfg", getenv("CFG_PATH") );
	if ( strstr ( cgDemFields, "UTPL") &&
		( iGetConfigEntry ( pclDataArea,"MAIN","LoadFormat", CFG_STRING,pclKeyword ) == RC_SUCCESS ) &&
		  ( strcmp(pclKeyword,"DEM") == 0 )
		)
	{
		bgUseRUDTAB = FALSE;
		dbg ( TRACE, "InitDemfnd: rgRueArray and rgRudArray will not be created" );
	}
	if ( bgUseRUDTAB )
	{
		/* Initialising RUETAB Array */
		ilRc = SetArrayInfo(cgRueTab,cgRueTab,RUE_FIELDS,NULL,NULL,"RUEIDX1",RUE_SORTFIELDS,RUE_SORTDIR,&rgRueArray,TRUE,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemfnd: SetArrayInfo RUETAB failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgRueArray);
			dbg(TRACE,"Init_demfnd: SetArrayInfo RUETAB OK");
		}/* end of if */

		ilRc = SetArrayInfo(cgRudTab,cgRudTab,RUD_FIELDS,NULL,NULL,"RUDIDX1",RUD_SORTFIELDS,RUD_SORTDIR,&rgRudArray,TRUE,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemfnd: SetArrayInfo RUDTAB failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgRudArray);
			dbg(TRACE,"Init_demfnd: SetArrayInfo RUDTAB OK");
		}/* end of if */
	}

	ilRc = SetArrayInfo(cgTplTab,cgTplTab,TPL_FIELDS,NULL,NULL,"TPLIDX1",TPL_SORTFIELDS,TPL_SORTDIR,&rgTplArray,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: SetArrayInfo TPLTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgTplArray);
	    dbg(TRACE,"Init_demfnd: SetArrayInfo TPLTAB OK");
	}/* end of if */


				
	sprintf(pclSelection,"WHERE (APPL = 'DEMFND' AND HOPO = '%s')",cgHopo);

	dbg(TRACE,"InitJobhdl: PARTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgParTab,cgParTab,PAR_FIELDS,NULL,NULL,"PARIDX1",PAR_SORTFIELDS,PAR_SORTDIR,&rgParArray,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PARTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgParArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PARTAB OK");
	}/* end of if */

	CEDAArrayRefill(&rgParArray.rrArrayHandle,rgParArray.crArrayName,pclSelection,NULL,ARR_FIRST);
	sprintf(pclSelection,
		"((APPL == DEMFND) && (HOPO == %s)) ",cgHopo);
	ilRc = CEDAArraySetFilter(&rgParArray.rrArrayHandle,
				  rgParArray.crArrayName,pclSelection);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter PARTAB failed <%d>",ilRc);
	}

    InitFieldIndex();
    }

    if(ilRc == RC_SUCCESS)
    {
	igInitOK = TRUE;
    }
    else
    {
	igInitOK = FALSE;
    }

    return ilRc;
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRc = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRc;
	
} /* end of Reset */

 
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
	default	:
	    Terminate(0);
	    break;
    } /* end of switch */
    exit(0);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int	ilRc = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int	ilRc = RC_SUCCESS;
	
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    dbg(TRACE,"<%d> : no messages on queue",pipErr);
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int	ilBreakOut = FALSE;
	
    do
    {
	ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;	
	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
				/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
		
	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    ilBreakOut = TRUE;
		    break;	
		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(0);
		    break;	
	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    ilBreakOut = TRUE;
		    break;	
		case	REMOTE_DB :
				/* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    Terminate(0);
		    break;
						
		case	RESET		:
		    ilRc = Reset();
		    break;
						
		case	EVENT_DATA	:
		    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		default			:
		    dbg(TRACE,"HandleQueues: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
	} /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
	ilRc = InitDemfnd();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: init failed!");
	} /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
	
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int      ilCmd          = 0;
    int      ilRoutingFlag  = 0; 
    int      ilOldSaveEvent = FALSE;
    int      ilWorkAgain    = RC_SUCCESS ; 
    BC_HEAD *prlBchead      = NULL;
    CMDBLK  *prlCmdblk      = NULL;
    char    *pclSelection   = NULL;
    char    *pclFields      = NULL;
    char    *pclData        = NULL;
    char    *pclTmp         = NULL;
    char     clActualDate[16];
    char     clUrno[20];
    char     clDeen[20];
    char     clDebe[20];
    char     *pclNewline;
    int      ilItemNoUrno = 0;
    int      ilItemNoDebe = 0;
    int      ilItemNoDeen = 0;
    BOOL     blDebeFound = 0;
    BOOL     blDeenFound = 0;
    char 		clUrnoList[2400];
    char 		clTable[24];

    prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
   
    pclData      = pclFields + strlen(pclFields) + 1;

 
    pclNewline = strchr(pclData,'\n');
    if (pclNewline != NULL)
    {
	*pclNewline = '\0';
    }
    pcgFieldList  = pclFields;
    ilRoutingFlag = 0 ; 
    ilWorkAgain   = RC_SUCCESS ; 

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    DebugPrintBchead(DEBUG,prlBchead);
    DebugPrintCmdblk(DEBUG,prlCmdblk);
    dbg(DEBUG,"selection <%s>",pclSelection);
    dbg(DEBUG,"fields    <%s>",pclFields);


    ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
    if(ilRc == RC_SUCCESS)
    {
	switch (ilCmd)
	{
	    case CMD_IRT :
	    case CMD_URT :
	    case CMD_DRT :
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		dbg(TRACE,"CEDAArrayEventUpdate2 ilRc  <%ld>",ilRc);
		break;
	    case CMD_SND : /* Select New Demands  */
		ProcessSND();
		break;
		break;
	    default:
		break;	
				 

	}
	
    }

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
    return ilRc;
	
} /* end of HandleData */

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/

/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
    int	ilRc   = RC_SUCCESS;			/* Return code */
    int ilLoop = 0;
    char clCommand[8];
	
    memset(&clCommand[0],0x00,8);

    ilRc = get_real_item(&clCommand[0],pcpCommand,1);
    if(ilRc > 0)
    {
	ilRc = RC_FAIL;
    }/* end of if */

    while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
    {
	ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
	ilLoop++;
    }/* end of while */

    if(ilRc == 0)
    {
	ilLoop--;
	dbg(TRACE,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
	*pipCmd = rgCmdDef[ilLoop];
    }else{
	dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
	ilRc = RC_FAIL;
    }/* end of if */

    return(ilRc);
	
} /* end of GetCommand */


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, ARRAYINFO *prpArrayInfo,BOOL bpFill,BOOL bpUseTrigger)
{
    int	ilRc   = RC_SUCCESS;				/* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];
    long llTmpRowLength1 = 0;
    long llTmpRowLength2 = 0;

    if(prpArrayInfo == NULL)
    {
	dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
	ilRc = RC_FAIL;
    }else{
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	prpArrayInfo->rrIdx01Handle = -1;

	if(pcpArrayName != NULL)
	{
	    if(strlen(pcpArrayName) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	    }/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
	    if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
	    }/* end of if */
	}/* end of if */

	if(pcpAddFields != NULL)
	{
	    if(strlen(pcpAddFields) + 
	       strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
	    {
		strcat(prpArrayInfo->crArrayFieldList,",");
		strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
		dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
	    }/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
	    prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
	    if (pcpAddFields != NULL)
	    {
		prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
	    }

	    prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    if(prpArrayInfo->plrArrayFieldOfs == NULL)
	    {
		dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		ilRc = RC_FAIL;
	    }/* end of if */
	}/* end of if */


	if(pcpIdx01Name != NULL)
	{
	    if(strlen(pcpIdx01Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx01FieldList != NULL)
	{
	    if(strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);
	    }/* end of if */
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	if(prpArrayInfo->plrArrayFieldLen == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrArrayFieldLen) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
       	ilRc = GetRowLength(pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->lrArrayRowLen++;
	prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
	if(prpArrayInfo->pcrArrayRowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
/*       	ilRc = GetRowLength(pcpTableName,pcpIdx01FieldList,&(prpArrayInfo->lrIdx01RowLen));*/
       	ilRc = GetRowLength(pcpTableName,pcpIdx01FieldList,&llTmpRowLength1);
	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */
	ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx01FieldList,plpAddFieldLens,&llTmpRowLength2);
	prpArrayInfo->lrIdx01RowLen = llTmpRowLength1 + llTmpRowLength2;
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->lrIdx01RowLen++;
	prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
	if(prpArrayInfo->pcrIdx01RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);

	prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx01FieldPos) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx01Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,pcpTableName,NULL,pcpAddFields,plpAddFieldLens,pcpArrayFieldList,&(prpArrayInfo->plrArrayFieldLen[0]),&(prpArrayInfo->plrArrayFieldOfs[0]));
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && bpFill)
    {
	ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
    if(ilRc == RC_SUCCESS && bpUseTrigger) 
    { 
	/*ilRc = TriggerAction(pcpTableName); */
		ilRc = ConfigureAction(pcpTableName, pcpArrayFieldList, NULL);
	 
	if(ilRc != RC_SUCCESS) 
	{ 
	    dbg(TRACE,"SetArrayInfo: ConfigureAction failed <%d>",ilRc); 
	}/* end of if */ 
    }/* end of if */ 
 
    return(ilRc);
	
} /* end of SetArrayInfo */

/*************************************************************************************/
/*************************************************************************************/

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo)
{
    int ilLoop = 0;
	
    if(prpArrayInfo == NULL)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: nothing to print");
	return;
    }/* end of if */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: rrArrayHandle         <%d>",prpArrayInfo->rrArrayHandle);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayName           <%s>",&(prpArrayInfo->crArrayName[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayFieldList      <%s>",&(prpArrayInfo->crArrayFieldList[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayFieldCnt       <%d>",prpArrayInfo->lrArrayFieldCnt);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayRowLen         <%d>",prpArrayInfo->lrArrayRowLen);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrArrayRowBuf        <%8.8x>",prpArrayInfo->pcrArrayRowBuf);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen      <%8.8x>",prpArrayInfo->plrArrayFieldLen);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldLen[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs      <%8.8x>",prpArrayInfo->plrArrayFieldOfs);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldOfs[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: rrIdx01Handle         <%d>",prpArrayInfo->rrIdx01Handle);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01Name           <%s>",&(prpArrayInfo->crIdx01Name[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01FieldList      <%s>",&(prpArrayInfo->crIdx01FieldList[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01FieldCnt       <%d>",prpArrayInfo->lrIdx01FieldCnt);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01RowLen         <%ld>",prpArrayInfo->lrIdx01RowLen);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrIdx01RowBuf        <%8.8x>",prpArrayInfo->pcrIdx01RowBuf);

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos      <%8.8x>",prpArrayInfo->plrIdx01FieldPos);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldPos[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd      <%8.8x>",prpArrayInfo->plrIdx01FieldOrd);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldOrd[ilLoop]);
    }/* end of for */

} /* end of DebugPrintArrayInfo */


	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop      = 0;
    long llFldLen    = 0;
    long llRowLen    = 0;
    char clFina[8];
	
    ilNoOfItems = get_no_of_items(pcpFieldList);

    dbg(TRACE,"GetRowLength: Tana <%s> FieldList <%s> NoOfItems <%ld>",pcpTana,pcpFieldList,ilNoOfItems);
    ilLoop = 1;
    do
    {
	ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
	
	if(ilRc > 0)
	{
	    ilRc = RC_SUCCESS;
	    if(GetFieldLength(pcpTana,&clFina[0],&llFldLen) == RC_SUCCESS)
	    {
		llRowLen++;
		llRowLen += llFldLen;
	    }/* end of if */

	}/* end of if */
	ilLoop++;
    }while(ilLoop <= ilNoOfItems);


    *plpLen = llRowLen;

    dbg(TRACE,"GetRowLength: llRowLen <%ld>",llRowLen);

    return(ilRc);
	
} /* end of GetRowLength */

static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llFldLen    = 0;
    long llRowLen    = 0;
    char clFina[8];
	
    if (pcpTotalFieldList != NULL)
    {
	ilNoOfItems = get_no_of_items(pcpFieldList);

		
	ilLoop = 1;
	do
	{
	    get_real_item(clFina,pcpFieldList,ilLoop);

	    ilRc = GetItemNo(clFina,pcpTotalFieldList,&ilItemNo);
	    if(ilRc == RC_SUCCESS) 
	    {
		llRowLen++;
		llRowLen += plpFieldSizes[ilItemNo];
	    }

	    ilLoop++;
	}while(ilLoop <= ilNoOfItems);
    }
    if(ilRc == RC_SUCCESS)
    {
	*plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
	
} /* end of GetLogicalRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc  = RC_SUCCESS;			/* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt - konstante nicht m�glich */

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
	case RC_SUCCESS :
	    *plpLen = atoi(&clFele[0]);
	    /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
	    break;

	case RC_NOT_FOUND :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
	    break;

	case RC_FAIL :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
	    break;

	default :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
	    break;
    }/* end of switch */

    return(ilRc);
	
} /* end of GetFieldLength */





/******************************************************************************/
/******************************************************************************/
static int ProcessSND()
{
    int	ilRc        = RC_SUCCESS;             /* Return code */
    char pclSelection[456];
    char pclAddData[256];
    char pclStart[20];
    char pclEnd[20];
    char pclTmpTime[64];
    char pclCurrTime[30] = "\0";
    char  *pclUrnoList = NULL ;
    char clKey[5];
    char clJodKey[15];
    char clJobKey[15];
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llRowNum4 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;
    long llJodRowNum = ARR_FIRST;
    long llRowCount = 0L;
    char *pclDemRow = NULL;
    char *pclJodRow = NULL;
    char *pclJobRow = NULL;
    char *pclTplRow = NULL;
    char *pclParRow = NULL;

    char clCurrTime[20];
    long  llOffSet = 0;
    long  llOpenDemCount = 0;
    long	llUrnoLen = 0;
    long  llCount = 0;
    long  llMaxCount = 0;

    long  llDemCount = 0;
    time_t tlJobLoadStart;
    time_t tlJobLoadEnd;
    time_t tlLoadStart;
    time_t tlLoadEnd;
 
    time_t tlJodLoadStart;
    int old_debug_level;
    char clTemp[300] = "\0";
	char clUtpl[21];


    BOOL blDemIsValid = TRUE;
  
    tgCurrTime = time(NULL);

    GetServerTimeStamp("UTC", 1, 0, clCurrTime);

    StrToTime(clCurrTime,&tgCurrTime);

    if(bgTestCurrTime)
    {
	tgCurrTime = tgOldCurrTime + 5*60;
    }

    tlLoadStart = tgCurrTime + lgLoadOffSetStart;
    tlLoadEnd = tgCurrTime + lgLoadOffSetEnd;

    tlJobLoadStart = tlLoadStart - 2*60*60; /*LadeStartoffset f�r alle Jobs -2h*/
    tlJobLoadEnd = tlLoadEnd + 2*60*60; /*LadeEndoffset f�r alle Jobs +2h*/

    tlJodLoadStart = tlLoadStart - 30*24*60*60;

    TimeToStr(pclStart,tlLoadStart);
    TimeToStr(pclEnd,tlLoadEnd);
    
    sprintf(clKey,"");

    if (strcmp(cgWithoutALID,"YES") == 0)
    {
	sprintf(pclSelection,"WHERE DEBE >= '%s' AND DEBE <= '%s' AND RETY IN (%s)",pclStart,pclEnd,cgRety);
    }
    else
    {
	sprintf(pclSelection,"WHERE DEBE >= '%s' AND DEBE <= '%s' AND RETY IN (%s) AND ALID != ' '",pclStart,pclEnd,cgRety);
    }

    dbg(TRACE,"ProcessSND Dem Selection: %s",pclSelection);
    dbg(TRACE,"ProcessSND Currenttime: <%s>",clCurrTime);

    CEDAArrayRefill(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,pclSelection,pclAddData,llAction);


    TimeToStr(pclStart,tlJobLoadStart);
    TimeToStr(pclEnd,tlJobLoadEnd);


    sprintf(pclSelection,"WHERE ACTO >= '%s' AND ACFR <= '%s'",pclStart,pclEnd);
    dbg(TRACE,"ProcessSND Job Selection: %s",pclSelection);

    llAction = ARR_FIRST;
    CEDAArrayRefill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,pclSelection,pclAddData,llAction);

    TimeToStr(pclStart,tlJodLoadStart);

    sprintf(pclSelection,"WHERE CDAT >= '%s' ",pclStart);
    dbg(TRACE,"ProcessSND Jod Selection: %s",pclSelection);


    if ( bgUseJODTAB )
	{
		llAction = ARR_FIRST;
		CEDAArrayRefill(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,pclSelection,pclAddData,llAction);	
	}

    ilRc = CEDAArrayGetRowCount(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,&llDemCount);
	
    if(ilRc == RC_SUCCESS && llDemCount > 0)
    {
		while((ilRc = CEDAArrayFindRowPointer(&(rgDemArray.rrArrayHandle),
					      &(rgDemArray.crArrayName[0]),
					      &(rgDemArray.rrIdx01Handle),
					      &(rgDemArray.crIdx01Name[0]),
					      clKey,&llRowNum,
					      (void *) &pclDemRow )) == RC_SUCCESS)
	{
			
	    blDemIsValid = FALSE;

	    CEDAArrayPutField(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,NULL,"STAT",llRowNum,"0") ;
		if ( GetUtpl ( pclDemRow, clUtpl ) == RC_SUCCESS )
		{
			TrimRight(clUtpl);
		    llRowNum4 = ARR_FIRST;
		    ilRc = CEDAArrayFindRowPointer( &(rgTplArray.rrArrayHandle),
											rgTplArray.crArrayName,
											&(rgTplArray.rrIdx01Handle),
											rgTplArray.crIdx01Name,
											clUtpl, &llRowNum4, (void*)&pclTplRow );
			if ( ilRc == RC_SUCCESS)
		    {
				strcpy(clTemp,TPLFIELD(pclTplRow,igTplTnam));
				TrimRight(clTemp);
				dbg(DEBUG,"Template Name: <%s> ",clTemp);
				llRowNum5 = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgParArray.rrArrayHandle),
							   &(rgParArray.crArrayName[0]),
							   &(rgParArray.rrIdx01Handle),
							   &(rgParArray.crIdx01Name[0]),
							   clTemp,&llRowNum5,
							   (void *) &pclParRow ) == RC_SUCCESS)
				{
					dbg(DEBUG,"Template in ParTab gefunden");
					CEDAArrayPutField(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,NULL,"STAT",llRowNum,"1") ;
					llOpenDemCount++;
					blDemIsValid = TRUE;
				}
				if (blDemIsValid != TRUE)
				{
					dbg(DEBUG,"Template in ParTab NICHT gefunden");
				}
		    }
			else
				dbg (TRACE, "ProcessSND: Template <%s> not found, RC <%d> !!!", clUtpl, ilRc );
	    }
	    if(blDemIsValid)
	    {
			if ( FindJob ( DEMFIELD(pclDemRow,igDemUrno) ) )
			{
				CEDAArrayPutField(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,NULL,"STAT",llRowNum,"0") ;
				llOpenDemCount--;
			}
	    }		

	    llRowNum = ARR_NEXT;
	}

	dbg(TRACE,"ProcessSND llOpenDemCount: %ld",llOpenDemCount);

	if(llOpenDemCount > 0)
	{
	    ilRc = GetFieldLength(&rgDemArray.crArrayName[0],"URNO",&llUrnoLen);
	    if (ilRc == RC_SUCCESS)
	    {
		llUrnoLen++;
	    } /* end of if */
	    else
	    {
		llUrnoLen = 14;
	    } /* end of else */

	    lgUrnoListLen = (llOpenDemCount * (llUrnoLen + 2) + 1);
				  
	    pcgUrnoListBuf = (char *) realloc(pcgUrnoListBuf, (lgUrnoListLen + 1));
	    if (pcgUrnoListBuf == NULL)
	    {
		ilRc = RC_FAIL ;
		dbg (TRACE, "ProcessSND: realloc failed - pcgUrnoListBuf");
	    }
	    else
	    {		   
		memset(pcgUrnoListBuf,0x00,lgUrnoListLen);
	    } /* end of if */
			 
	    sprintf(clKey,"1");
			
	    llRowNum = ARR_FIRST;
	    pclUrnoList = pcgUrnoListBuf ;
	    llCount = 0;
	    while((ilRc = CEDAArrayFindRowPointer(&(rgDemArray.rrArrayHandle),
						  &(rgDemArray.crArrayName[0]),
						  &(rgDemArray.rrIdx01Handle),
						  &(rgDemArray.crIdx01Name[0]),
						  clKey,&llRowNum,
						  (void *) &pclDemRow )) == RC_SUCCESS)
	    {
		llRowNum = ARR_NEXT;
		dbg (DEBUG, "ProcessSND: pclDemRow <%s> ",DEMFIELD(pclDemRow,igDemStat));
		if(llCount == 0)
		{
		    sprintf(pclUrnoList, "'%s'", DEMFIELD(pclDemRow,igDemUrno)) ;
		}
		else
		{
		    if(llMaxCount > 50)
		    {
			sprintf(pclUrnoList, "|'%s'", DEMFIELD(pclDemRow,igDemUrno)) ;
			llMaxCount = 0;
		    }
		    else
		    {
			sprintf(pclUrnoList, ",'%s'", DEMFIELD(pclDemRow,igDemUrno)) ;
		    }

		}
		llCount++;
		llMaxCount++;

		dbg (DEBUG, "ProcessSND: DemUrno <%s> llCount <%ld>",DEMFIELD(pclDemRow,igDemUrno),llCount);
		pclUrnoList += strlen (pclUrnoList) ;
				
	    }
	    /*		dbg(TRACE,"ProcessSND SND pcpUrnoBuf: %s",pcgUrnoListBuf);*/
	    SendToJobHdl("SND","","",pcgUrnoListBuf);
	}
    }
    return(ilRc) ;

} /* ProcessSND */



/* 20000619 bch start */
static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    ilItemCount = get_no_of_items(pcpFields);

    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
	get_real_item(pclFieldName,pcpFields,ilLc);
	if(!strcmp(pclFieldName,pcpFieldName))
	{
	    get_real_item(pcpDest,pcpData,ilLc);
	    dbg(TRACE,"%05d: %s <%s>",ilLc,pclFieldName,pcpDest);
	    ilRc = RC_SUCCESS;
	}
    }

    return ilRc;
}
/* 20000619 bch end */

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    *pipItemNo = 0;

    ilItemCount = get_no_of_items(pcpFields);

    dbg(TRACE,"GetItemData itemCount = %d",ilItemCount);	
    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
	get_real_item(pclFieldName,pcpFields,ilLc);
	if(!strcmp(pclFieldName,pcpFieldName))
	{
	    *pipItemNo = ilLc;
	    dbg(TRACE,"%05d: %s",ilLc,pclFieldName);
	    ilRc = RC_SUCCESS;
	}
    }

    return ilRc;
}


static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char pclSection[124];
    char pclKeyword[124];

    strcpy(pclSection,pcpSection);
    strcpy(pclKeyword,pcpKeyword);

    return iGetConfigEntry(cgConfigFile,pclSection,pclKeyword,CFG_STRING,pcpCfgBuffer);
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t now;
    char   _tmpc[6];

/*	dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
    if (strlen(pcpTime) < 12 )
    {
	*plpTime = time(0L);
	return RC_FAIL;
    } /* end if */

    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
/*
  strncpy(_tmpc,pcpTime+12,2);
  _tm -> tm_sec = atoi(_tmpc);
*/
    _tm -> tm_sec = 0;
	
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm -> tm_year = atoi(_tmpc)-1900;
    _tm -> tm_wday = 0;
    _tm -> tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
	_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    if (now != (time_t) -1)
    {
	/*now +=  + 3600;*/
	*plpTime = now;
	return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
    struct tm *_tm;
    time_t now;
    char   _tmpc[6];
	
    _tm = (struct tm *)localtime(&lpTime);
    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	    _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
	    _tm->tm_min,_tm->tm_sec);
    return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               



/******************************************************************************/
/******************************************************************************/

/******************************************************************************/ 
/******************************************************************************/ 
 

static int SendToJobHdl(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData)
{
    int			ilRc;
    BC_HEAD		rlBCHead;
    CMDBLK		rlCmdblk;

    dbg(TRACE,"<SCAFC> ----- START -----");

    /* set BC-Head members */
    memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
    rlBCHead.rc = RC_SUCCESS;
    strncpy(rlBCHead.dest_name, "DEMFND", 10);
    strncpy(rlBCHead.recv_name, "DEMFND", 10);

    /* set CMDBLK members */
    memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
    strncpy(rlCmdblk.command, "SSD", 6);
    strncpy(rlCmdblk.obj_name, pcpObject, 10);
    sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, "DEMFND");

    /* write request to QUE */
    if ((ilRc = SendToQue(9100, PRIORITY_3, &rlBCHead, &rlCmdblk,
			  pcpSelection,pcpFields,pcpData)) != RC_SUCCESS)
    {
	dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
	dbg(DEBUG," ----- END -----");
	return ilRc;
    }
		
    dbg(TRACE,"<SCAFC> ----- END -----");

    /* bye bye */
    return RC_SUCCESS;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
		     CMDBLK *prpCmdblk, char *pcpSelection, 
		     char *pcpFields, char *pcpData)
{
    int			ilRc;
    int			ilLen;
    EVENT			*prlOutEvent 	= NULL;
    BC_HEAD		*prlOutBCHead 	= NULL;
    CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"<SendToQue> ----- START -----");

    /* calculate size of memory we need */
    ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
	strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

    dbg(DEBUG,"<SendToQue> ilLen <%ld>",ilLen);
    /* get memory for out event */
    if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
	dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
	dbg(DEBUG,"<SendToQue> ----- END -----");
	return RC_FAIL;
    }

    /* clear buffer */
    memset((void*)prlOutEvent, 0x00, ilLen);

    /* set structure members */
    prlOutEvent->type 		 = SYS_EVENT;
    prlOutEvent->command 	 = EVENT_DATA;
    prlOutEvent->originator  = mod_id;
    prlOutEvent->retry_count = 0;
    prlOutEvent->data_offset = sizeof(EVENT);
    prlOutEvent->data_length = ilLen - sizeof(EVENT);

    /* BCHead members */
    prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
    memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

    /* CMDBLK members */
    prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
    memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

    /* Selection */
    strcpy(prlOutCmdblk->data, pcpSelection);
    /* fields */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);


    /* data */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);


    /* send this message */
    dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
    if ((ilRc = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
    {
	dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc);
	return RC_FAIL;
    }

    /* delete memory */
    free((void*)prlOutEvent);

    dbg(DEBUG,"<SendToQue> ----- END -----");

    /* bye bye */
    return RC_SUCCESS;
}



/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(min(ipSleep,1));
	
    exit(0);
	
} /* end of Terminate */
 

/******************************************************************************/
/******************************************************************************/

static void TrimRight(char *pcpBuffer)
{
    char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
    while(isspace(*pclBlank) && pclBlank != pcpBuffer)
    {
	*pclBlank = '\0';
	pclBlank--;
    }
}



/******************************************************************************/
/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
    int    ilRC       = RC_SUCCESS ;
    long   llRow      = 0 ;
    FILE  *prlFile    = NULL ;
    char   clDel      = ',' ;
    char   clFile[512] ;
    char  *pclTrimBuf = NULL ;
    long llRowCount;
    int ilCount = 0;
    char  *pclTestBuf = NULL ;


    dbg (DEBUG,"SaveIndexInfo: SetArrayInfo <%s>",prpArrayInfo->crArrayName) ;

    if (ilRC == RC_SUCCESS)
    {
	sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

	errno = 0 ;
	prlFile = fopen (&clFile[0], "w") ;
	if (prlFile == NULL)
	{
	    dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
	    ilRC = RC_FAIL ;
	} /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
	{
	    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
	    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
	    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
	}

	fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
	fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
	fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

	{
	    dbg(DEBUG,"IndexData") ; 
	}
	fprintf(prlFile,"IndexData\n"); fflush(prlFile);

	dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
	*prpArrayInfo->pcrIdx01RowBuf = '\0';
	llRow = ARR_FIRST ;
	do
	{
	    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
	    if (ilRC == RC_SUCCESS)
	    {
    
		dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

		if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
		{
		    ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
		    if(ilRC == RC_SUCCESS)
		    {
			dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
		    } /* end of if */
		} /* end of if */

		pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
		if (pclTrimBuf != NULL)
		{
		    ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
		    if (ilRC > 0)
		    {
			ilRC = RC_SUCCESS ;
			fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
		    } /* end of if */

		    free (pclTrimBuf) ;
		    pclTrimBuf = NULL ;
		} /* end of if */

		llRow = ARR_NEXT;
	    }
	    else
	    {
		dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
	    } /* end of if */
	} while (ilRC == RC_SUCCESS) ;

	{
	    dbg (DEBUG,"ArrayData") ;
	}
	fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

	llRow = ARR_FIRST ;

	CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

	llRow = ARR_FIRST;
	dbg(DEBUG,"Array  <%s> data follows Rows %ld",
	    prpArrayInfo->crArrayName,llRowCount);
	do
	{
	    ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
					  &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
	    if (ilRC == RC_SUCCESS)
	    {
		for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
		{
				
		    fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
		    fflush(prlFile) ;
		    dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
		}
		fprintf (prlFile,"\n") ; 
		dbg (DEBUG, "\n") ;
		fflush(prlFile) ;
		llRow = ARR_NEXT;
	    }
	    else
	    {
		dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
	    }
	} while (ilRC == RC_SUCCESS);

	if (ilRC == RC_NOTFOUND)
	{
	    ilRC = RC_SUCCESS ;
	} /* end of if */
    } /* end of if */

    if (prlFile != NULL)
    {
	fclose(prlFile) ;
	prlFile = NULL ;
  
	dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
    } /* end of if */

    return (ilRC) ;

}/* end of SaveIndexInfo */


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int		ilRC = RC_SUCCESS;
    char	clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
			 clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
	dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
	if (!strcmp(clCfgValue, "DEBUG"))
	    *pipMode = DEBUG;
	else if (!strcmp(clCfgValue, "TRACE"))
	    *pipMode = TRACE;
	else
	    *pipMode = 0;
    }

    return ilRC;
}


static int FillParTab()
{
    int ilRC = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llParRowNum = ARR_FIRST;
    char *pclTplRow = NULL;
    char *pclParRow = NULL;
    char pclData[1024] = "\0";
    char pclUrno[32] = "\0";
    char pclTplName[1024] = "\0";
    char pclSqlBuf[200] = "\0";
    short slCursor;
    short slSqlFunc;
    char pclDataArea[1000] = "\0";
    char pclErr[2560] ="\0";


    slSqlFunc = START;
    slCursor = 0;
    sprintf(pclSqlBuf,"DELETE FROM PARTAB WHERE APPL = 'DEMFND'");
    ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
    if (ilRC != DB_SUCCESS)
    {
	get_ora_err(ilRC,pclErr);
	dbg(TRACE,"FillParTab Error deleting partab-entries");
	ilRC = RC_FAIL;
    }
    close_my_cursor(&slCursor);
    slCursor = 0;


    ilRC = CEDAArrayDelete(&rgParArray.rrArrayHandle,rgParArray.crArrayName);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"FillParTab Error deleting PARTAB ");
    }
    else
    {
	dbg(TRACE,"FillParTab deleting PARTAB ");
    }

    ilRC = CEDAArrayWriteDB(&rgParArray.rrArrayHandle,
			    rgParArray.crArrayName,
			    NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"FillParTab Error writing to PARTAB ");
    }
	    

    llRowNum = ARR_FIRST;
    while((ilRC = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
					  &(rgTplArray.crArrayName[0]),
					  &(rgTplArray.rrIdx01Handle),
					  &(rgTplArray.crIdx01Name[0]),
					  "",&llRowNum,
					  (void *) &pclTplRow )) == RC_SUCCESS)
    {
	dbg(DEBUG,"%05d: Check Template: <%s>",__LINE__,(TPLFIELD(pclTplRow,igTplTnam)));
	llRowNum2 = ARR_FIRST;
	strcpy(pclTplName, (TPLFIELD(pclTplRow,igTplTnam)));
	TrimRight(pclTplName);
	dbg(DEBUG,"%05d: Check Template: <%s>",__LINE__,pclTplName);
	if ((ilRC = CEDAArrayFindRowPointer(&(rgParArray.rrArrayHandle),
					    &(rgParArray.crArrayName[0]),
					    &(rgParArray.rrIdx01Handle),
					    &(rgParArray.crIdx01Name[0]),
					    pclTplName,&llRowNum2,
					    (void *) &pclParRow )) != RC_SUCCESS)
	{
	    dbg(DEBUG,"%05d: Template not Found! Insert it: <%s>",__LINE__,pclTplName);
	    
	    GetNextValues(pclUrno,1);
	    sprintf(pclData,"DEMFND,cont_assign,%s,%s,%s",pclTplName,cgHopo,pclUrno);
	    ilRC = CEDAArrayAddRowPart(&rgParArray.rrArrayHandle,
				       rgParArray.crArrayName,
				       &llParRowNum,
				       "APPL,PTYP,NAME,HOPO,URNO",
				       pclData);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg(DEBUG,"%05d: CEDAArrayAddRowPart ParArray failed",__LINE__);
	    }
	}
	llRowNum = ARR_NEXT;
    }

    ilRC = CEDAArrayWriteDB(&rgParArray.rrArrayHandle,
			    rgParArray.crArrayName,
			    NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"FillParTab Error writing to PARTAB ");
    }

    return ilRC;
}


static int GetUtpl ( char *pcpDemRow, char *pcpUtpl ) 
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST, llRowNum2 = ARR_FIRST;
	char *pclRudRow, *pclRueRow;

	pcpUtpl[0] = '\0';
	if ( bgUseRUDTAB )
	{
	    if(CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle),
									rgRudArray.crArrayName,
									&(rgRudArray.rrIdx01Handle),
									rgRudArray.crIdx01Name,
									DEMFIELD(pcpDemRow,igDemUrud),&llRowNum,
									(void *) &pclRudRow ) == RC_SUCCESS )
	    {
			if(CEDAArrayFindRowPointer( &(rgRueArray.rrArrayHandle),
										rgRueArray.crArrayName,
										&(rgRueArray.rrIdx01Handle),
									    rgRueArray.crIdx01Name,
									    RUDFIELD(pclRudRow,igRudUrue), &llRowNum2,
									    (void *) &pclRueRow ) == RC_SUCCESS)
				strcpy ( pcpUtpl, RUEFIELD(pclRueRow,igRueUtpl) );
		}
	}
	else
		strcpy ( pcpUtpl, DEMFIELD(pcpDemRow,igDemUtpl) );

	if ( *pcpUtpl && (*pcpUtpl!=' ') )	
		ilRc = RC_SUCCESS;
	else
		ilRc = RC_FAIL;
	dbg ( DEBUG, "GetUtpl: URNO <%s>  UTPL <%s> RC <%d>", 
		  DEMFIELD(pcpDemRow,igDemUrud), pcpUtpl, ilRc );
	return ilRc;

}

static BOOL FindJob ( char *pcpUdem )
{
	long llRowNum1 = ARR_FIRST, llRowNum2;
	char *pclJodRow, *pclJobRow=0;
	BOOL blFound = FALSE;
	
	if ( bgUseJODTAB )
	{
		while( !blFound &&
			    ( CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
										  rgJodArray.crArrayName,
										  &(rgJodArray.rrIdx01Handle),
										  rgJodArray.crIdx01Name,
										  pcpUdem, &llRowNum1, (void *) &pclJodRow ) == RC_SUCCESS) )
		{
			llRowNum2 = ARR_FIRST;
			if ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
										 rgJobArray.crArrayName,
										 &(rgJobArray.rrIdx01Handle),
										 rgJobArray.crIdx01Name,
										 JODFIELD(pclJodRow,igJodUjob),&llRowNum2,
										 (void *) &pclJobRow ) == RC_SUCCESS)
			{
				blFound = TRUE;
			}
		}
	}
	else
	{
		llRowNum2 = ARR_FIRST;
		if ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
									 rgJobArray.crArrayName,
									 &(rgJobArray.rrIdx01Handle),
									 rgJobArray.crIdx01Name, pcpUdem,
									 &llRowNum2, (void*) &pclJobRow ) == RC_SUCCESS)
		{
			blFound = TRUE;
		}
	}
	if ( pclJobRow )
		dbg(DEBUG,"FindJob: bgUseJod <%d> Job Found <%d> UDEM <%s> UJOB <%s>", 
				   bgUseJODTAB, blFound, pcpUdem,JOBFIELD(pclJobRow,igJobUrno));
	else
		dbg(DEBUG,"FindJob: bgUseJod <%d> Job Found <%d> UDEM <%s> UJOB <null>", 
					bgUseJODTAB, blFound, pcpUdem );
	return blFound;
}	

/******************************************************************************/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */











