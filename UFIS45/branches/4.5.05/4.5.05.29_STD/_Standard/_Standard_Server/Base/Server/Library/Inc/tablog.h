#ifndef _DEF_mks_version_tablog_h
  #define _DEF_mks_version_tablog_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tablog_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tablog.h 1.2 2004/07/27 17:04:35SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */


#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>

#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"

    



