#ifndef _DEF_mks_version_dbg_c
  #define _DEF_mks_version_dbg_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dbg_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/dbg.c 1.8 2006/02/21 21:38:31SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/* 20010220: SetDbgLimits und SwitchDebugFileAuto: automatisches Umschalten
             der Debug-Datei nach 30000 Zeilen und Uebershreiben alter Debug
	     -Dateien nach 10 Dateien. Verhindert unmaessiges Anwachsen der
	     Debug-Dateien, da pro Prozess defaultmaessig nur 11 x 30000 
	     Zeilen gleichzeitig vorhanden sein koennen. Siehe SetDbgLimits 
	     zum Einstellen der Anzahl der Zeilen und der Anzahl der Dateien
   20010831: Reihenfolge von close und rename in SwitchDebugFileAuto
*/
/* update 1. set Filepointer outp to NULL after close GRM 13.11.2001 */ 
/* update 2. reopen to avoid original and .bak mismatch GRM 15.11.2001 */ 
/* 20060221 JIM: included SwitchDebugFile and SwitchDebugFileExt from        */
/*               debugrec.c, paremters to them are no evaluated. This avoids */
/*               log file mess by own counters of file numbers in ie. SQLHDL */
/* 20060221 JIM: removed sccs_dbg_lib                                        */
/*****************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include "ugccsma.h"
#include "glbdef.h"
#include "uevent.h"

extern FILE *outp;
FILE *outp1;
extern char *mod_name;
extern int debug_level;
extern char *mod_version;
static long lgMaxDbgLines  = 30000; /* Default number of lines per debug file */
static long lgActDbgLines  = 0;     /* first line in file */
static int  igMaxDbgFileNo = 10;    /* backup files are Numbered from 0..igMaxDbgFileNo */
static int  igActDbgFileNo = 0;     /* start file numbering with 0 */

void	SwitchDebugFileAuto(int ipLevel,int *pipCounter,int ipMaxCnt);
void SwitchMyLogFile(void);

static void get_time(char *s);

void dbg(int level,char *fmt, ...)
{
	va_list args;
	char *s; 
	char Errbuf[1000];
	int i;

	if (debug_level > 0 ) 
	{

		if ((level == TRACE) && debug_level>0 ) 
		{
      fseek(outp,0,SEEK_END);
      if (lgActDbgLines  == 0)
      { /* 20040109 JIM: print version string in first lin of debug file */
	   		fprintf(outp,mod_version); 
	   		fprintf(outp,"\n"); fflush(outp); 
        lgActDbgLines++;
        if (igActDbgFileNo > 0)
        {
	     		fprintf(outp,"=== Logfile continued ===\n"); fflush(outp);
          lgActDbgLines++;
        }
      }
			/* sieht zwar doof aus ist aber schneller !!! vbl */
			va_start(args, fmt);
			(void) sprintf(Errbuf, "%s ",mod_name);
			s = Errbuf + strlen(Errbuf);
			get_time(s);
			s = Errbuf + strlen(Errbuf);
			strcat (s, fmt);
			strcat (s, "\n");
			vfprintf(outp,Errbuf, args); fflush(outp);
      lgActDbgLines++;  /* Note: we don't count '\n' in fmt */
			va_end(args);                        
		} /* end if */

		if (level == DEBUG && debug_level>TRACE ) 
		{
      fseek(outp,0,SEEK_END);
      if (lgActDbgLines  == 0)
      { /* 20040109 JIM: print version string in first lin of debug file */
	   		fprintf(outp,mod_version); 
	   		fprintf(outp,"\n"); fflush(outp); 
        lgActDbgLines++;
        if (igActDbgFileNo > 0)
        {
	     		fprintf(outp,"=== Logfile continued ===\n"); fflush(outp);
          lgActDbgLines++;
        }
      }
			/* sieht zwar doof aus ist aber schneller !!! vbl */
			va_start(args, fmt);
			(void) sprintf(Errbuf, "%s ",mod_name);
			s = Errbuf + strlen(Errbuf);
			get_time(s);
			s = Errbuf + strlen(Errbuf);
			strcat (s, fmt);
			strcat (s, "\n");
			vfprintf(outp,Errbuf, args); fflush(outp);
      lgActDbgLines++;  /* Note: we don't count '\n' in fmt */
			va_end(args);                        
		} /* end if */


    if ( (lgMaxDbgLines > 0) && (lgActDbgLines > lgMaxDbgLines) )
    {
      fseek(outp,0,SEEK_END);
			 fprintf(outp,"(dbg) lgActDbgLines: %ld, lgMaxDbgLines: %d\n",
                   lgActDbgLines,lgMaxDbgLines); 
       fflush(outp);
       i = debug_level;
       SwitchDebugFileAuto(debug_level, &igActDbgFileNo, igMaxDbgFileNo);
       debug_level= i;
       lgActDbgLines= 0;
    }


	} /* end if */
}

static void get_time(char *s)
{
   struct timeb tp;
   time_t	_CurTime;
   struct tm	*CurTime;
   struct tm	rlTm;
   long    	secVal,minVal,hourVal;
   char		tmp[10];
  
   if(s != NULL)
     {      
       _CurTime = time(0L);
       CurTime = (struct tm *)localtime(&_CurTime);
       rlTm = *CurTime;
#if defined(_WINNT) 
       strftime(s,20,"%" "H:%" "M:%" "S",&rlTm);
#else
       strftime(s,20,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
       /*   strftime(s,20,"%" "b%" "d %" "T",&rlTm);*/
#endif

       ftime(&tp);
       minVal = tp.time / 60;
       secVal = tp.time % 60;
       hourVal= minVal / 60;
       minVal = minVal % 60;
       sprintf(tmp,":%3.3d:",tp.millitm);
       strcat(s,tmp);
     }

}
   

/* ******************************************************************** */
/* The dbg_handle_debug routine						*/
/* ******************************************************************** */

void dbg_handle_debug(int dbg_mod)
{
  switch(dbg_mod)
  {

  case TRACE_ON:
    if (debug_level == TRACE){
      debug_level  = DEBUG;
      dbg(TRACE,"(dbg) LOGFILE set to FULL mode");
    }/* end if */
    else{
      debug_level  = TRACE;
      dbg(TRACE,"(dbg) LOGFILE set to TRACE mode");
    }/* end else */
    break;

  case TRACE_OFF:
    dbg(TRACE,"(dbg) LOGFILE set to OFF mode");
    debug_level  = 0;
    break;      

  default:
    break;

  }/* end switch */                
        
  return;
}/* end of handle_debug */

/* ******************************************************************** */
/* SetDbgLimits: Initialize the number of lines per Debug-File and the  */
/*               number of files to use.                                */
/*	Parameters:							*/
/*	long lpMaxDbgLines: maximum number of lines per debug file	*/
/*      int ipMaxDbgFileNo: maximum number of files (additional to the	*/
/*                          first file)					*/
/*	Returns:							*/
/*	No Return value							*/
/* In default, all 30000 lines the current debug files is renamed with  */
/* a counter appended to the file name and a new debug file with the    */
/* regular name <module><mod_id>.log is opened. To avoid an unlimited   */
/* number of debug files, after 10 new files the file name counter is   */
/* reset to 1 so that only 11 files (beginning from zero) with 30000    */
/* lines are written. This mechanism avoids the old problem of full     */
/* harddisks from debug files. With the the function SetDbgLimits you   */
/* can modify the maximum number of lines and the maximum nunber of     */
/* files. If the maximum number of lines is less 1, no file switching   */
/* is done, else if the maximum number of files is less 1 the original  */
/* debug file is overwritten.                                           */
/* The old debug file with the number 0 appended is not overwritten to  */
/* behold the initialization debug messages                             */
/* Example:                                                             */
/*      SetDbgLimits (20000,50);                                        */
/* This will write at maximum 51 debug files with 20000 lines before    */
/* overwriting old files                                                */
/* ******************************************************************** */
void SetDbgLimits (long lpMaxDbgLines, int ipMaxDbgFileNo)
{
   lgMaxDbgLines  = lpMaxDbgLines; /* Default number of lines per debug file */
   igMaxDbgFileNo = ipMaxDbgFileNo;  /* backup files are Numbered from 0..igMaxDbgFileNo */
   dbg(DEBUG,"(SetDbgLimits) max. No. of Lines for Debug files set to %ld",lgMaxDbgLines);
   dbg(DEBUG,"(SetDbgLimits) max. No. of Debug filesset to %d",igMaxDbgFileNo);
}

/* ******************************************************************** */
/*	SwitchDebugFileAuto(...)  					*/
/*	Extended Version of SwitchDebugFile(). The third parameter is	*/
/*	the maximum number of debug files to be hold. If number is	*/
/*	reached, the counter *pipCounter is reset to 1. 1 is supposed	*/
/*	to be the initial value of *pipCounter, when program starts	*/
/*	Parameters:							*/
/*	int ipLevel	actual value of 'debug_level' variable		*/
/*	int *pipCounter	actual Counter value for debug file backup	*/
/*	int ipMaxCnt	maximum number of debug files and backups hold	*/
/*	Returns:							*/
/*	No Return value							*/
/* ******************************************************************** */
void	SwitchDebugFileAuto(int ipLevel,int *pipCounter,int ipMaxCnt)
{
	int	ilDebug, ili;
	char pclCommand[200];
  char pclLastDbgName[200]; /* Last debug file name for 'continued... ' remark */  
  char pclNewDbgName[200]; /* Last debug file name for 'continued... ' remark */  

/* !!! don't use dbg(...) in this routine                            !!! 
   !!! it recurses to this routine (and calls another dbg(...) ..... !!!
*/
	outp1 = outp;
        fseek(outp1,0,SEEK_END);
	sprintf(pclLastDbgName,"%s.bak.%d",__CedaLogFile,*pipCounter);
	fprintf(outp1,"Renaming Old Debug File to '%s'\n",pclLastDbgName);
	fflush (outp1);	/* necessary, because if close is not done rename will result in a file with no contence */
	fclose (outp1);
	sprintf(pclNewDbgName,"%s.new",__CedaLogFile);
	outp = NULL;
#ifdef _LINUX
	outp = fopen(pclNewDbgName,"w");
#else
	outp = freopen(pclNewDbgName,"w",outp1); /* reopen to avoid original and .bak mismatch 15.11.2001 2. */
#endif
	/* therefore following sleep and fopen loop no longer necessary (only one open) GRM 13.11.2001 1. */
	rename(__CedaLogFile,pclLastDbgName);
	rename(pclNewDbgName,__CedaLogFile);
	if ((ipMaxCnt > 0) &&(*pipCounter > ipMaxCnt))
  {
	   sprintf(pclLastDbgName,"%s.bak.%d",__CedaLogFile,*pipCounter - ipMaxCnt);
     remove(pclLastDbgName);
  }
	*pipCounter = *pipCounter + 1;
  if ( *pipCounter < 0 ) /* turn around integer definition range */
		*pipCounter = 1;

	return;
}

/* ******************************************************************** */
/* ******************************************************************** */
void SwitchMyLogFile(void)
{
  int i;
  fseek(outp,0,SEEK_END);
  fprintf(outp,"(dbg) lgActDbgLines: %ld, lgMaxDbgLines: %d\n",
  lgActDbgLines,lgMaxDbgLines); 
  fflush(outp);
  i = debug_level;
  SwitchDebugFileAuto(debug_level, &igActDbgFileNo, igMaxDbgFileNo);
  debug_level= i;
  lgActDbgLines= 0;
  return;
}

/* ******************************************************************** */
/*	SwitchDebugFile()                                                    */
/*	Dummy "Extended Version" of SwitchDebugFile() from debugfrec.c       */
/* !!! All parameters are ignored	                                 !!! */
/* This routine only exists for compatibility porpuse.                  */
/*	20060221 JIM: The old version with its own numbering messed up the   */
/*	               log file history, so it is deactivated                */
/* ******************************************************************** */
void	SwitchDebugFile(int ipLevel,int *pipCounter)
{
   fprintf(outp,"SwitchDebugFile: switch requested..\n");
   SwitchMyLogFile();
}

/* ******************************************************************** */
/*	SwitchDebugFileExt()                                                 */
/*	Dummy "Extended Version" of SwitchDebugFile() from debugfrec.c       */
/* !!! All parameters are ignored	                                 !!! */
/* This routine only exists for compatibility porpuse.                  */
/*	20060221 JIM: The old version with its own numbering messed up the   */
/*	               log file history, so it is deactivated                */
/* ******************************************************************** */
void	SwitchDebugFileExt(int ipLevel,int *pipCounter,int ipMaxCnt)
{
   fprintf(outp,"SwitchDebugFileExt: switch requested..\n");
   SwitchMyLogFile();
}
