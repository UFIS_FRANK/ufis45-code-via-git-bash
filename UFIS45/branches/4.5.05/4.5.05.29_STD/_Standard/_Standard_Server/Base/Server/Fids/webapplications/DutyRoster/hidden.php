<?php 
	include("include/manager.php");
	include("$ADODB_RootPath/list.php"); 
//	include("include/class.FastTemplate.php3");			 
	include ("include/template.php"); 
//	include ("class.overlib/class.overlib.php3"); 
//	include ("include/class.LoginInit.php"); 
	include ("include/class.PHP_Timer.php"); 
	
	
	// Check The TMPL_MENUID default must be 18?
	
//	if (!$TMPL_MENUID && $TMPL_MENUID<=0 )  {$TMPL_MENUID=1;}
	
	/* Login Check */
	/* Set Login Instance*/
	//$login = new  LoginInit;
	
	//echo $tmpl_path;
	
	$tpl = new Template($tmpl_path);

	
	if ($ExportXls=="True") {
		header("Content-Type: application/vnd.ms-excel; name='excel'");
		header("Content-disposition:  attachment; filename=DutyRoster" .date("Y-m-d").".xls");

		$tpl->load_file("Xls.tmpl","Index");
	} else {
		$tpl->load_file("inf.tmpl","Index");
	}
	
	
	
				
	
/* Start Debug  if requested */
if ($debug=="true") {
	$tmpdebug="?debug=true";
	$db->debug=$debug;
}else {
	$debug="false";
}			 
			 

//$debug=true;
/* Debug */

/* Set Timer Instance*/
//$timer = new PHP_timer;
//$timer->start();
/* Set Timer Instance*/


/* Global  Variables*/
//$alc2="%".$flno;
//$alc3="%".$flno;

$tdi=FindTDI();
$tdiminus=$tdi-($tdi*2);

$StaffTblArray=Array();
$DRRTblArray=Array();
$BSDTblArray=Array();
$ODATblArray=Array();
$DailyStatsArray=Array();
$DailyScodArray=Array();

$startdate = time();
//$startdate=DateAdd("n" ,$tdi,mktime (20,0,0,11,30,2000));
//$startdate=mktime (20,0,0,11,30,2000);

// Refresh the page 
$Refresh=300;

// Expires the Page
$Cexpires=DateAdd("s" ,$Refresh,$startdate);
$Cexpires=date("D, d M Y H:i:s ",DateAdd("n" ,$tdiminus,$Cexpires))." GMT";


	// Start Header
	
//	$start = $tpl->utime();

	$tpl->set_var("PageTitle", $PageTitle );
	$tpl->set_var( "Refresh",$Refresh);
	$tpl->set_var( "Cexpires",$Cexpires );
	
	$CurrentDate = time();
	$tpl->set_var("CurrentDate",date("Y-m-d",$CurrentDate));


	$tpl->set_var("btnValue","Select");
	// Call OrgUnitTbl Function
	OrgUnitTbl($OrgUnitCode);
	$tpl->set_var("DspbtnValueOrg","");
	
	if ($btnValueOrg=="Change Org. Unit") {
		$FunctionCode="";
	}
	
	if (strlen($OrgUnitCode)>0) {
		//Cal FunctionTbl Function
		FunctionTbl($FunctionCode,$OrgUnitCode);
		$tpl->set_var("btnValue","Display Duty Roster");
		$tpl->set_var("btnValueOrg","Change Org. Unit");

		DisplayMonthNames($Month);
		$tpl->parse("MonthFunction",false);
		
		$tpl->parse("DspbtnValueOrg",false);
		$tpl->parse("OrgFunction",false);
	} else {
		$tpl->set_var("MonthFunction","");
		$tpl->set_var("Export","");
		$tpl->set_var("Legend","");
		$tpl->set_var("DspDateFromTo","");
		$tpl->set_var("OrgFunction","");
		$tpl->set_var("NoResults","");
	}


	if (strlen($FunctionCode)>0) {
	
	

	$tpl->set_var("FunctionCode",$FunctionCode);
	$tpl->set_var("OrgUnitCode",$OrgUnitCode);
	
	//Cal BSDTab Function 
	BSDTab();

	//Cal ODATab Function 
	ODATab();

	$tpl->parse("Legend",false);
	$tpl->parse("Export",false);

	/* We Must reverse the values of the array in order to pass  the results  
	to the parse functions in the correct order */

	// We must Call the DateFromDateTo ($datefrom,$dateto)
	
		
	// Test Variables
	//$DateFrom = "20040601000000";
	//$DateTo   = "20040610000000";
	

	//$mktimeDateFrom=DateAdd("n" ,0,time());
	
	$mktimeDateFrom=mktime(00 ,00, 00,$Month ,01, date("Y",time()));
	
	//$mktimeDateTo=DateAdd("m" ,+1,$mktimeDateFrom);
	$mktimeDateTo=mktime(00 ,00, 00,$Month ,date("t",$mktimeDateFrom), date("Y",$mktimeDateFrom));
	
	$DateFrom = date("Ymd000000",$mktimeDateFrom);
	$DateTo   = date("Ymd000000",$mktimeDateTo);
	
	$tpl->set_var("CurrentMonth", $Month );
	
	//if (len($DateFrom)>0 and len($DateTo)>0 ) {

	//$tpl->set_var("DateFrom", $DateFrom );
	//$tpl->set_var("DateTo",$DateTo);

	//$tpl->set_var("DspDateFrom",date("M Y",$mktimeDateFrom ));
	//$tpl->set_var("DspDateTo",date("M Y",$mktimeDateTo));
	//$tpl->parse("DspDateFromTo",false);
	$tpl->set_var("DspDateFromTo","");
	
	//$DateFrom=str_replace("-", "", $DateFrom)."000000";
	//$DateTo=str_replace("-", "", $DateTo)."000000";


		
		DateFromDateTo ($DateFrom,$DateTo);

		//echo $DateFrom ."--".$DateTo."<br>".$DateFrom1 ."--".$DateTo1;
		$tpl->set_var("NoResults","<br><br>No Staff in DataBase");	
		// Display The Staff Names
		if (len($OrgUnitCode)>0 &&  len($FunctionCode)>0) {
			// Select The Staff Names from the STFTAB
			$SurnoSql="";
			$SurnoSql=StaffNames($OrgUnitCode,$FunctionCode,$DateFrom);
			if (len($SurnoSql)>0) {
				// Select The Daily Roster from the DRRTAB
				if ( len($SurnoSql)>0 ) {
					DRRTab($SurnoSql,$DateFrom,$DateTo);
				}
				//Display the Data to the Screen 
				DisplayStaff($DateFrom,$DateTo);
				DisplayDDr($DateFrom,$DateTo);
				//echo count($DailyStatsArray);
				if (count($DailyScodArray)>0) {
					DisplayDailyStats ($DateFrom,$DateTo);
					DisplayDailyScod();
				} else {
					$tpl->set_var("DailyStatsScod","");
					$tpl->set_var("DrrStats0","");
				}
				//print_r($DailyScodArray);

				$tpl->parse("NoResults",false);	
			}
		}

	
	} else {
		$tpl->set_var("Export","");
		$tpl->set_var("Legend","");
		$tpl->set_var("DspDateFromTo","");		
		$tpl->set_var("NoResults","");
	}
	


	
	//$tpl->set_var("dateerror",$dateerror );			

	$tpl->set_var( "type",$type );					


	$tpl->set_var( "PHP_SELF",$PHP_SELF );		
	$tpl->set_var( "debug",$debug);		
	
	
	
	// End Header


	
	if ($debug=="true") {
	 $tpl->set_var("PerfOutput",$perf->UI($pollsecs=5));
	 $tpl->parse("Perf",false);	
	}  else {
	 $tpl->set_var("Perf","");
	}

	
	
	

	$tpl->pparse("Index",false);			
//	Header("Content-type: text/plain");
//	$tpl->FastPrint();
//	$end = $tpl->utime();
//	$run = $end - $start;
//	echo "Runtime [$run] seconds<BR>\n";
	exit;
	// End Footer


?>