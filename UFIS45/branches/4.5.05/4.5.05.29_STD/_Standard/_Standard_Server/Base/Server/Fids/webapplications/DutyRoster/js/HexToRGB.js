function GiveDec(Hex)
{
   if(Hex == "A")
      Value = 10;
   else
   if(Hex == "B")
      Value = 11;
   else
   if(Hex == "C")
      Value = 12;
   else
   if(Hex == "D")
      Value = 13;
   else
   if(Hex == "E")
      Value = 14;
   else
   if(Hex == "F")
      Value = 15;
   else
      Value = eval(Hex);

   return Value;
}

function GiveHex(Dec)
{
   if(Dec == 10)
      Value = "A";
   else
   if(Dec == 11)
      Value = "B";
   else
   if(Dec == 12)
      Value = "C";
   else
   if(Dec == 13)
      Value = "D";
   else
   if(Dec == 14)
      Value = "E";
   else
   if(Dec == 15)
      Value = "F";
   else
      Value = "" + Dec;

   return Value;
}

function HexToDec(InputHex)
{
   Input = InputHex;

   Input = Input.toUpperCase();

   FirstStr = Input.substring(0, 1);
   
   if (FirstStr == "#") { 
	   a = GiveDec(Input.substring(1,  2));
	   b = GiveDec(Input.substring(2, 3));
	   c = GiveDec(Input.substring(3, 4));
	   d = GiveDec(Input.substring(4, 5));
	   e = GiveDec(Input.substring(5, 6));
	   f = GiveDec(Input.substring(6, 7));
   } else {
	   a = GiveDec(Input.substring(0, 1));
	   b = GiveDec(Input.substring(1, 2));
	   c = GiveDec(Input.substring(2, 3));
	   d = GiveDec(Input.substring(3, 4));
	   e = GiveDec(Input.substring(4, 5));
	   f = GiveDec(Input.substring(5, 6));
   }
   

   x = (a * 16) + b;
   y = (c * 16) + d;
   z = (e * 16) + f;

  return x+","+y+","+z;

}

function DecToHex(InputRed,InputGreen,InputBlue)
{
   Red = InputRed;
   Green = InputGreen;
   Blue = InputBlue;

   a = GiveHex(Math.floor(Red / 16));
   b = GiveHex(Red % 16);
   c = GiveHex(Math.floor(Green / 16));
   d = GiveHex(Green % 16);
   e = GiveHex(Math.floor(Blue / 16));
   f = GiveHex(Blue % 16);

   z = a + b + c + d + e + f;

   return  z;
}
