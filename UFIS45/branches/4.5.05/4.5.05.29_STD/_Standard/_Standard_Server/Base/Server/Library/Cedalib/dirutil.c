#ifndef _DEF_mks_version_dirutil_c
  #define _DEF_mks_version_dirutil_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dirutil_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/dirutil.c 1.2 2012/01/16 01:49:34SGT ble Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/* Author: Mei, Lee Bee Suan                                            */
/* Date of Creation: 24-Nov-2011                                        */
/* Initiate Project: AUH - 2011 AODB delivery                           */
/* Purpose: 1. Get filename list from a specific directory              */
/* ******************************************************************** */

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>
#include "glbdef.h"
#include "dirutil.h"



int getfiles_in_dir( char *pcpDirName, char *pcpExpression, char *pcpFilenames, int ipMaxNumFiles, int ipSortType )
{
    struct dirent   *prlDirEntry;
    DIR *prlDirPtr;
    int ilNumFiles = 0;
    int ilBlkSize = 5120;
    int ilNewBlkSize;
    char pclOneFile[5120] = "\0";
    char *pclFilenames = "\0";
    char *pclFunc = "getfiles_in_dir";

    if( (prlDirPtr = opendir(pcpDirName)) == NULL)
    {
        dbg( TRACE, "Error in opening directory name <%s> error <%d><%s>", pcpDirName, errno, strerror(errno) );
        return -1;
    }
    ilNewBlkSize = ilBlkSize;
    pclFilenames = (char *)malloc( ilNewBlkSize );
    memset( pclFilenames, 0, ilNewBlkSize );
    while( (prlDirEntry = readdir(prlDirPtr)) != NULL )
    {
        strcpy( pclOneFile, prlDirEntry->d_name );
        if( MatchPattern( pclOneFile, pcpExpression ) == 1 )
        {
            ilNumFiles++;       
            dbg( DEBUG, "File %d <%s>", ilNumFiles, pclOneFile );

            if( (strlen(pclFilenames)+strlen(pclOneFile)+10) >= ilNewBlkSize )
            {
                ilNewBlkSize += ilBlkSize;
                pclFilenames = (char *)realloc( pclFilenames, ilNewBlkSize );
                if( pclFilenames == NULL )
                {
                    dbg( TRACE, "%s: ERROR! Not able to allocate enough memory for all the files"
                                "(%d) in the directory", pclFunc, ilNumFiles );
                    free( pclFilenames );
                    return -1;
                }
            }
            strcat( pclFilenames, pclOneFile );
            strcat( pclFilenames, "," );    
       }
    }
    if( strlen(pclFilenames) > 0 )
        pclFilenames[strlen(pclFilenames)-1] = '\0';
 
    if (closedir(prlDirPtr) == -1)
    {
        dbg( TRACE, "Error in closing directory name <%s> error <%d><%s>", pcpDirName, errno, strerror(errno) );
        free( pclFilenames );
        return -1;
    }
    if( ipSortType == SORT_BY_FILENAME && ilNumFiles > 1 )
        SortFilenames( ilNumFiles, ipMaxNumFiles, pclFilenames );
    strcpy( pcpFilenames, pclFilenames );
    free( pclFilenames );
    return ilNumFiles;
}

int SortFilenames( int ipNumFiles, int ipMaxNumFiles, char *pcpFilenames )
{
    int ilFileCount;
    int ili, ilj;
    char **pclFileArray;
    char pclOneFile[5120] = "\0";
    char *pclFunc = "SortFilenames";

    dbg( TRACE, "%s: Before Sorting (show 100 char only) <%100s....>", pclFunc, pcpFilenames );

    pclFileArray = (char **)malloc( ipNumFiles*sizeof(char *) );
    for( ili = 0; ili < ipNumFiles; ili++ )
        pclFileArray[ili] = (char *)malloc(256);

    /* set into array of file names */
    for( ilFileCount = 1; ilFileCount <= ipNumFiles; ilFileCount++ )
    {
        get_real_item( pclOneFile, pcpFilenames, ilFileCount );
        strcpy( pclFileArray[ilFileCount-1], pclOneFile );
    }
    /* sort */
    for( ili = 0; ili < ipNumFiles; ili++ )
    {
        for( ilj = 0; ilj < (ipNumFiles-1); ilj++ )
        {
            if( strcmp( pclFileArray[ilj], pclFileArray[ilj+1] ) > 0 ) 
            {
                strcpy( pclOneFile, pclFileArray[ilj+1] );
                strcpy( pclFileArray[ilj+1], pclFileArray[ilj] );
                strcpy( pclFileArray[ilj], pclOneFile );
            }
        }
    }

    /* set back to comma separated file names */
    pcpFilenames[0] = '\0';
    for( ilFileCount = 0; ilFileCount < ipNumFiles && ilFileCount < ipMaxNumFiles; ilFileCount++ )
    {
        strcat( pcpFilenames, pclFileArray[ilFileCount] );
        if( ilFileCount < (ipNumFiles-1) && ilFileCount < (ipMaxNumFiles-1) )
            strcat( pcpFilenames, "," );
    }
    dbg( TRACE, "%s: Sorted <%s>", pclFunc, pcpFilenames );

    for( ili = 0; ili < ipNumFiles; ili++ )
        free( (char *)pclFileArray[ili] );
    free( (char **)pclFileArray );
}

