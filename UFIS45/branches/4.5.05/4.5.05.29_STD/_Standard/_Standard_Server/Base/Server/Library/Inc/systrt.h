#ifndef _DEF_mks_version_systrt_h
  #define _DEF_mks_version_systrt_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_systrt_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/systrt.h 1.3 2008/04/03 22:08:21SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*                         S y s t r t. h                                   */
/*                                                                          */
/****************************************************************************/ 
#ifndef __SYSTRT_INC
#define __SYSTRT_INC


#ifndef STTABLE
#include <sthdef.h>     /* Def. f. System Table Handler */               
#endif

#define FAILURE		(char *) -1
#define NOLUCK		-1
#define NO_OF_KIDS	150
#define PATHNAME	40
#define NO_OF_COMMANDS	4  /* orginal 3 (151093/JEL) */
#define TRESHOLD	5
#define OK		0
#define KEEP		1	

#define REMOVE		2       /* this comes only from uutil */
				/* could also be placed in uevent */

#define SYSTRT		SYS_PRIMARY       /* this is my queue ID */
				/* could also be placed in queuedef */

struct _mykids
  {
  char path_name[PATHNAME];	/* pathname for 'execv' */
  char task_name[NO_OF_KIDS][20]; /* taskname for restart */
  char proc_no[NO_OF_KIDS][10];	/* Process no for restart */
  char *cmd[NO_OF_COMMANDS];    /* parameters for 'execv' */
  long status;			/* status from 'wait' */
  long child_id[NO_OF_KIDS];    /* child process id's */
  long child_deaths[NO_OF_KIDS]; /* no. of deaths per kid */
  } ;

typedef struct _mykids MYKIDS;

struct _shr_memory
  {
  char *shm_base;		/* base address */
  char *shm_end;		/* end address */
  unsigned long size;		/* size */
  unsigned long id;		/* id */
  struct shmid_ds shdms;        /* statistic structure */
  } ;
  
typedef struct _shr_memory SHRD_MEM;  

struct _syswork
  {
  MYKIDS *pmykids;		/* child processes */
  SHRD_MEM *pshrd_mem;		/* shared memory */
  SPTREC *psptrec;		/* SPTAB */
  GBLADR *pgbladr;		/* global addresses */
  STHBCB *pbcb;			/* STH bcb */
  SRCDES *psrcdes;		/* search descr. for STH */
  char *anyptr;			/* working pointer */
  } ;
  
typedef struct _syswork SYSWORK; 

#endif

/****************************************************************************/
/****************************************************************************/
