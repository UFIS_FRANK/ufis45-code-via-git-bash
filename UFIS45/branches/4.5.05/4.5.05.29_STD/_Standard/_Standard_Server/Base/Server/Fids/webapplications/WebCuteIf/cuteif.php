<?php 
/*
Copyright ��� 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
	include("include/manager.php");

	include("include/class.LoginInit.php");
	
	
	/* Login Check */
	/* Set Login Instance*/
	$login = new  LoginInit;
	$logintype="Login_"	.$AuthType;
	if (isset($Session["userid"])) { 
		// The user has already been Logged
		if (len($username)>0 && len($password)>0) {
			
			$Query=	$login->$logintype($username,$password);
		}
	} else {
		$Query=	$login->$logintype($username,$password);

	}
	
	$gate_frame = "";
	if ($Session["Type"]=="GATE") {
		$main_file="mainGate.php";
		$remarks_file = "remarksGate.php";
		$remark_frame = ",0";
		$logo_frame="logo.php";
		$mainFramerow=350;
	} else {
		$main_file="main.php";
		$logo_file=
		$remarks_file = "remarks.php";
		$remark_frame = ",250";
		$logo_frame="blank.html";
		$mainFramerow=180;
		if ($DisplayBoth_Logo_Remark==false) {
		if ($Session["CheckinPredifendSelection"] == true || $Session["CheckinPredifendSelectionUser"] == true) {
			$logo_frame="blank.html";
			$remarks_file = "remarks.php";
		} elseif ($Session["logoSelection"]== true || $Session["logoSelectionUser"] == true) {
			$logo_frame="logo.php";
			$remarks_file="blank.html";
		} else {
			$remarks_file="blank.html";
			$logo_frame="blank.html";
		}
	} else {
			$remarks_file = "remarks.php";
			$logo_frame="logo.php";
}

	
	}
	/* Start Debug  if requested */
	$cputWidth=1; // Must be at keast 1 for IE 7
	if ($debug=="true") {
		$tmpdebug="?debug=true";
		$db->debug=$debug;
		$dbMysql->debug=$debug;
		$cputWidth=50;
	}else {
		$debug="false";
		$tmpdebug="";
	}	
	//$cputWidth=1;
	
?>

<html>
<head><title><%=$PageTitle%></title>
  <META NAME="description" CONTENT="">
  <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
  <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">  
  <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">  
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">  
  <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
  <META HTTP-EQUIV="Expires" CONTENT="<%echo $Cexpires%>">	
  <link rel="STYLESHEET" type="text/css" href="style/style.css">
</head>

<script type="text/javascript" src="include/noclick.js"></script>

<frameset rows="83,*" framespacing="0" frameborder="0">

    <frameset  cols="300,*,300" framespacing="0" frameborder="0">
        <frame name="UFIS" src="ufis.html" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" noresize>
        <frame name="Status" src="status.php<%=$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" noresize>
        <frame name="DateTime" src="datetime.php<%=$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" noresize>
    </frameset>
    <frameset  cols="300,*" framespacing="0" frameborder="1">
        <frame name="Logo" src="<%=$logo_frame%><%=$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
        <frameset  rows="<%=$mainFramerow%><%=$remark_frame%>,100,<%=$cputWidth%>" framespacing="0" frameborder="0">
            <frame name="Worksheet" src="<%=$main_file.$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
            <frame name="Remarks" src="<%=$remarks_file.$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
			<frame name="bottom" src="bottom.php" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" noresize>
            <frame name="Cput" src="cput.php<%=$tmpdebug%>" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
        </frameset>
    </frameset>
	
</frameset>

<body></body>

</html>
