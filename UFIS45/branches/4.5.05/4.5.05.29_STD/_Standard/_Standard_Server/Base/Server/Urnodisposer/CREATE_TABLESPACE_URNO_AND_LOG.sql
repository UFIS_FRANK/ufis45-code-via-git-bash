
prompt prerequisite: a link in /ora/urno must point to free space
prompt create link: ln -s /u04 /ora/urno

prompt prerequisite: a link in /ora/logging must point to free space
prompt create link: ln -s /u04 /ora/logging
 
CREATE TABLESPACE URNO DATAFILE
        '/ora/urno/oradata/UFIS/urno.dbf' SIZE 2000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );

CREATE TABLESPACE LOGGING DATAFILE
        '/ora/urno/oradata/UFIS/logging.dbf' SIZE 1000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );
