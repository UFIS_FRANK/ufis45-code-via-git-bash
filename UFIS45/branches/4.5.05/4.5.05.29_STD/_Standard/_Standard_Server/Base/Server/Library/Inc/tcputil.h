#ifndef _DEF_mks_version_tcputil_h
  #define _DEF_mks_version_tcputil_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tcputil_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tcputil.h 1.2 2004/07/27 17:04:39SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :		RBI					  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _TCPUTIL_H
#define _TCPUTIL_H


/* the ccs_nto.. makros renamed due to functions ccs_... */

#define ccs_ntohs(s) ( (((s) & 0x00ff) << 8) | (((s) & 0xff00) >> 8) )

#define ccs_htons(s) ( (((s) & 0x00ff) << 8) | (((s) & 0xff00) >> 8) )

#define ccs_ntohl(s) ( (((s) & 0x000000ff) << 24) | (((s) & 0x0000ff00) << 8) | (((s) & 0x00ff0000) >> 8) | (((s) & 0xff000000) >> 24) )

#define ccs_htonl(s) ( (((s) & 0x000000ff) << 24) | (((s) & 0x0000ff00) << 8) | (((s) & 0x00ff0000) >> 8) | (((s) & 0xff000000) >> 24) )


/*Exported  Prototypes */

int tcp_create_socket (int type, char *Pservice);
int tcp_open_connection (int socket, char *Pservice, char *Phostname);
int tcp_send_data (char *Pdata, int len, int socket);
int tcp_forward_data (COMMIF *data, int que_out);
int tcp_send_shutdown (int sock);
int tcp_wait_timeout (int sock, int t);
short aat_ntohs(unsigned short spNet);
short aat_htons(unsigned short spHost);
long aat_ntohl(unsigned long lpNet);
long aat_htonl(unsigned long lpHost);
#ifndef _HPUX_SOURCE
int tcp_send_datagram (int sock, char *Phostadr, char *Phostname, 
		       char *Pservice, char *Pdata, int len);
#endif
int TcpGetHostname (char *pcpMyName, int ipLen);

#endif /* _TCPUTIL_H */

