#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/loahdl.c 1.00 2011/01/27 15:00:00SGT dka Exp dka(2011/04/19 08:50:03SGT) $";
#endif /* _DEF_mks_version */
/********************************************************************************
 *          
 * LOAD HANDLER
 *                      
 * Author         : dka 
 * Date           : 20120127                                                    
 * Description    : UFIS-1387 
 *                                                                     
 * Update history :                                                   
 *  20120127 DKA 1.00   First release.
 *
 *
 *
 ********************************************************************************/

/**********************************************************************************/
/*                                                                                */
/* be carefule with strftime or similar functions !!!                             */
/*                                                                                */
/**********************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "ct.h"

/******************************************************************************/
/* Macros */
/******************************************************************************/

#define     N_SECTIONS      64   /* no. of sections from cfg */
#define	    N_FORMULAE    32   /* no. of formulae per config section */
#define	    N_PARAMETERS    128   /* no. of parameter lines per config section */
#define     RC_DATA_ERR 2222 
#define	    UTC_TIME  5601
#define	    LOCAL_TIME  5602
#define	    ENTRY_NAME_SIZE	64
#define	    ENTRY_VALUE_SIZE  256
#define     AFT_MSG_SIZE  256

#define	    MAX_CTFLT_VALUE 4

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/

int  debug_level = DEBUG;

/******************************************************************************/
/* Declarations                                                               */
/******************************************************************************/

typedef struct t_struct_config_formula_s {
  char name [ENTRY_NAME_SIZE];
  char defn [ENTRY_VALUE_SIZE]; 
} t_struct_config_formula;

typedef struct t_struct_config_variable_s {
  char name [ENTRY_NAME_SIZE];
  char dssn [ENTRY_NAME_SIZE]; 
  /* char where [ENTRY_VALUE_SIZE]; */
  char type [32];
  char styp [32];
  char sstp [32];
  char ssst [32];
} t_struct_config_variable;

typedef struct t_struct_config_section_s {
  char name [ENTRY_NAME_SIZE];
  char aft_fields [ENTRY_VALUE_SIZE];
  char aft_arr_prio [ENTRY_VALUE_SIZE];
  char aft_dep_prio [ENTRY_VALUE_SIZE];
  t_struct_config_formula formula [N_FORMULAE];
  t_struct_config_variable variable [N_PARAMETERS]; 
} t_struct_config_section;

static t_struct_config_section sgConfig [N_SECTIONS];


/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igGlNoAck     = 0;		  /* flag for acknowledgement */
static int   igQueOut      = 0;
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgCedaSend[10] = "\0";
static char  cgCedaSendPrio[10] = "\0";
static char  cgCedaSendCommand[10] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static int   igModIdSqlhdl = 506;       /* target ID for SQLHDL */
static char  pcgUserName [64];		/* last sender's username if any */
static char  pcgCedaCmd [64];		/* last command */

static char pcgAftSections [128];	/* all the defined sections in config */

static int  igModIdFlight = -1;	        /* the FLIGHT process  & it's message */
static char pcgAftCmd [32];
static char pcgAftFld [AFT_MSG_SIZE];
static char pcgAftSel [AFT_MSG_SIZE];
static char pcgAftVal [AFT_MSG_SIZE];
static char pcgAftDestName [64];
static char pcgAftRecvName [64];
static char pcgAftTwStart [64];
static char pcgAftTwEnd [64];
static char pcgAftTable [64];

/******************************************************************************/
/* For ct library */
/******************************************************************************/

static char pcgLoaTabName[] = "LOATAB";
static char pcgLoaTabGrid[] = "LoaTab";
static char pcgLoaTabFldList[] = "APC3,DSSN,FLNO,FLNU,HOPO,IDNT,RURN,SSST,SSTP,STYP,TIME,TYPE,URNO,VALU";
static char pcgLoaTabFldSize[] = "3   ,3   ,9   ,10  ,3   ,22  ,10  ,3   ,10  ,3   ,14  ,3   ,10  ,6   ";
static char pcgLoaTabNulStrg[] = ",,,0,,,0,,,,,,0,";

static STR_DESC argCtFlValue[MAX_CTFLT_VALUE+1];

static STR_DESC rgCtTabLine;
static STR_DESC rgCtTabData;
static STR_DESC rgCtTabList;
static STR_DESC rgCtTabUrno;

static STR_DESC rgCtFlLine;
static STR_DESC rgCtFlData;
static STR_DESC rgCtHtLine;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int  InitPgm();
static int  Reset(void); 
static void Terminate(void);
static void HandleSignal(int);
static void HandleErr(int);         /* General errors */
static void HandleQueErr(int);
static int  HandleData(void);       /* CEDA  event data     */
static void HandleQueues(void);     /* Waiting for Sts.-switch*/

static int  TimeToStr (char *pcpTime,time_t lpTime, int ipFlag); /* handy */
static int  SendToCeda (char *pcpBuffer,char *pcpMyTime,long plpMyReason,
          char *pcpUrno);
static void HandleLOA_Cmd (char *pcpDssn, char *pcpFlnu);
static void LoadAftConfig (void);
static void ReplaceChar(char *pcpBuffer, char clOrgChar, char clDestChar);
/*static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, char *pcpWhereClause); */
static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, 
              char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst); 
static void PrintLoaConfig (void);
static int  DoSelect (char *pcpFieldList, char *pcpWhereClause, char *pcpTable,
              char *pcpDataBuf);
static int  GetFlightDetails (char *pcpUrno, char *pcpAdid, char *pcpFlno,
              char *pcpSchedule);
static int  ScanConfig (char *pcpAdid, char *pcpDssn, char *pcpFlnu);
static int  InitConfigStructure (void);
static int  GetLoatabValue (char *pcpDssn, char *pcpType, char *pcpStyp,
              char *pcpSstp, char *pcpSsst, char *pcpValu); 
static int  GetVariableValue (int ipSectionIdx, char *pcpVarName,
  char *pcpInputDssn, int *ipVal);

/******************************************************************************/
/* For CT library                                                             */ 
/******************************************************************************/

static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize);
static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey);
int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep);

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/

extern int get_real_item (char *, char *, int);
extern int get_item_no (char *, char *, int);

/******************************************************************************/
/* Internal variables                                                         */
/******************************************************************************/

#define DATABLK_SIZE (1024)

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[DATABLK_SIZE];

int  igDbgSave = DEBUG;
int  igSendToModid = 9560;
int  igSendPrio = 3;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/

MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* handles signal       */
    (void)SetSignals(HandleSignal);
    (void)UnsetSignals();

    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    /* uncomment if necessary */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"MAIN: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"==================== Entering main pgm loop  =================");
    for(;;)
    {
        dbg(TRACE,"==================== START/END =================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */ 
        }
   
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************
 * ReadConfigEntry
 ******************************************************************************
 */

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
     char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[256] = "\0";
    char clKeyword[256] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE,"ReadConfigEntry: Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
    dbg(TRACE,"ReadConfigEntry: use default-value: <%s>",pcpDefault);
    strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
    dbg(TRACE,"ReadConfigEntry: Config Entry [%s],<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitPgm()
{
 int    ilRC = RC_SUCCESS;            /* Return code */
 char     pclDbgLevel [iMIN_BUF_SIZE];
 char     pclDummy [iMIN_BUF_SIZE];
 char     pclDummy2 [iMIN_BUF_SIZE],pclTmpBuf [64];
 int ilCnt = 0 ;
 int ilItem = 0 ;
 int ilLoop = 0 ;
 int jj, kk;

  /* now reading from configfile or from database */
  SetSignals(HandleSignal);

  do
  {
    ilRC = init_db();
    if (ilRC != RC_SUCCESS)
    {
      check_ret(ilRC);
      dbg(TRACE,"InitPgm: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"InitPgm: init_db() failed! waiting 60 sec ...");
    sleep(60);
    exit(2);
  }else{
    dbg(TRACE,"InitPgm: init_db() OK!");
  } /* end of if */

  (void) ReadConfigEntry("MAIN","DEBUG_LEVEL",pclDbgLevel,"TRACE");
  if (strcmp(pclDbgLevel,"DEBUG") == 0)
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <DEBUG>");
    igDbgSave = DEBUG;
  } 
  else if (strcmp(pclDbgLevel,"OFF") == 0) 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <OFF>");
    igDbgSave = 0;
  } 
  else 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <TRACE>");
    igDbgSave = TRACE;
  }

  igModIdFlight = tool_get_q_id ("flight");
  if ((igModIdFlight == RC_FAIL) || (igModIdFlight == RC_NOT_FOUND))
  {
    dbg(TRACE,"InitPgm: ====== ERROR ====== Flight Mod Id not found, default to 7800");
    igModIdFlight = 7800; 
  }
  else
    dbg(TRACE,"InitPgm: Flight Mod Id <%d>", igModIdFlight);

  (void) ReadConfigEntry("MAIN","AFT_SECTIONS", pcgAftSections, "NONE");
   
  (void) InitConfigStructure ();

  /*
    Fill up the global configuration structure
  */

  LoadAftConfig ();
  PrintLoaConfig ();
  if(ilRC == RC_SUCCESS)
  {
    /* read HomeAirPort from SGS.TAB */
    ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,SYS,HOMEAP not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: home airport <%s>",cgHopo);
    }
  }

  if(ilRC == RC_SUCCESS)
  { 
    ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,ALL,TABEND not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: table extension <%s>",cgTabEnd);
    }
  }

  InitMyTab(pcgLoaTabGrid,pcgLoaTabFldList,pcgLoaTabFldSize);
  strcpy (pcgAftCmd,"UFR");   

  igInitOK = TRUE;
  debug_level = igDbgSave;
  return(ilRC);

} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{ 
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
        case SIGTERM:
            dbg(TRACE,"HandleSignal: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
            break;
        case SIGALRM:
            dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
            return;
            break;
        default:
            dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
            return;
            break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"HandleQueErr: <%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"HandleQueErr: <%d> malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"HandleQueErr: <%d> msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"HandleQueErr: <%d> msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"HandleQueErr: <%d> route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"HandleQueErr: <%d> route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"HandleQueErr: <%d> unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"HandleQueErr: <%d>  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"HandleQueErr: <%d> queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"HandleQueErr: <%d> missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"HandleQueErr: <%d> queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"HandleQueErr: <%d> no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"HandleQueErr: <%d> too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"HandleQueErr: <%d> no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"HandleQueErr: <%d> invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"HandleQueErr: <%d> queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"HandleQueErr: <%d> requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"HandleQueErr: <%d> receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"HandleQueErr: <%d> unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }
        
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"HandleQueues: : init failed!");
            } /* end of if */
    }/* end of if */
} /* end of HandleQueues */
    

/******************************************************************************
 * HandleData
 ******************************************************************************
 */

static int HandleData()
{

  int ilRC = RC_SUCCESS; 
  int que_out;          /* Sender que id */
  int ilItemNo,jj;
  BC_HEAD *prlBchd = NULL;  /* Broadcast heade */
  CMDBLK  *prlCmdblk = NULL;    /* Command Block */
  char *pclSel = '\0';
  char *pclFld = '\0';
  char *pclData = '\0';
  char pclFields [4096], pclValues [4096];
  char *pclOldData = '\0';  
  char pclTable [32];
  char pclDssn [32], pclFlnu [32];
  
  /* dbg (TRACE, " --------------START/END------------------"); */
  /*  Very detailed logging of the CEDA message.
  DebugPrintItem(DEBUG,prgItem);
  DebugPrintEvent(DEBUG,prgEvent);
  */

  igQueOut = prgEvent->originator;   /* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);

  pclSel = prlCmdblk->data;
  pclFld = pclSel + strlen(pclSel)+1;
  pclData = pclFld + strlen(pclFld)+1;
  strncpy (pclFields, pclFld, 4096);
  strncpy (pclValues, pclData, 4096);

  pclOldData= strtok(pclData,"\n");
  pclOldData= strtok(NULL,"\n");
  dbg(TRACE,"FROM <%d> TBL <%s>",igQueOut,prlCmdblk->obj_name);
  strcpy (pclTable,prlCmdblk->obj_name);
  dbg(TRACE,"Cmd <%s> Que (%d) WKS <%s> Usr <%s>",
    prlCmdblk->command, prgEvent->originator, prlBchd->recv_name, prlBchd->dest_name);
  dbg(TRACE,"Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
  strcpy (pcgAftTwStart,prlCmdblk->tw_start);
  strcpy (pcgAftTwEnd,prlCmdblk->tw_end);
  memset(pcgAftDestName, '\0', (sizeof(prlBchd->dest_name) + 1));
  strncpy (pcgAftDestName, prlBchd->dest_name, sizeof(prlBchd->dest_name));
  memset(pcgAftRecvName, '\0', (sizeof(prlBchd->recv_name) + 1));
  strncpy(pcgAftRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
  dbg(TRACE,"Sel <%s> ", pclSel);
  dbg(TRACE,"Fld <%s> ", pclFld);
  dbg(TRACE,"Dat <%s> ", pclData);
  if (pclOldData != '\0')
    dbg(TRACE,"Old <%s> ", pclOldData);

  if (prlBchd->rc == NETOUT_NO_ACK)
  {
    igGlNoAck = TRUE;
     dbg(TRACE,"No Answer Expected (NO_ACK is true)"); 
  }
  else
  {
    igGlNoAck = FALSE;
     dbg(TRACE,"Sender Expects Answer (NO_ACK is false)");
  }
 
  strcpy (pcgCedaCmd,prlCmdblk->command);

  if (!(strcmp (prlCmdblk->command, "LOA"))) 
  {
    ilItemNo = get_item_no (pclFld, "DSSN", 5) + 1;
    if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
    {
       (void) get_real_item (pclDssn, pclData, ilItemNo);
    }
    else
    {
      dbg(TRACE,"========= No DSSN in LOA message ========");
      strcpy (pclDssn,""); 
    }

    ilItemNo = get_item_no (pclFld, "FLNU", 5) + 1;
    if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
    {
       (void) get_real_item (pclFlnu, pclData, ilItemNo);
    }
    else
    { 
      dbg(TRACE,"========= No FLNU in LOA message ========");
      strcpy (pclFlnu,""); 
    }

    HandleLOA_Cmd (pclDssn, pclFlnu);
  }

  if ((igGlNoAck == FALSE) && ((strcmp (prlCmdblk->command, "LOA")) == 0)) 
  {
    (void) tools_send_info_flag(igQueOut,
      0,		/* will be set to mod_id */
      "loahdl",		/* user name */
      "",		/* hostname by bchdl */
      "CEDA",		/* workstation name */
      "",		/* unused? */
      "",		/* unused? */
      pcgAftTwStart,pcgAftTwEnd,
      "LOA",
      "",		/* table name */
      "",		/* selection */
      "",		/* field list */
      "",		/* data list */
      NETOUT_NO_ACK); 	/* should this be zero ? */
    dbg (TRACE,"Sent acknowledgement to <%d>", igQueOut); 
  }
  
  return ilRC;
    
} /* end of HandleData */

/************************************************************************
 *      
 *  HandleLOA_Cmd 
 *
 *  
 ************************************************************************
 */

void HandleLOA_Cmd (char *pcpDssn, char *pcpFlnu) 
{ 
  char pclFunc [] = "HandleLOA_Cmd:";
  char clNow [32] = "\0";
  int jj,kk,ilRC;
  char pclTmpBuf [64], pclAdid [32], pclFlno [32], pclSchedule [32];
 
  /* (void) TimeToStr (clNow, time(NULL), UTC_TIME); */

  if ((pcpDssn [0] == '\0') || (pcpFlnu [0] == '\0'))
  {
    dbg (TRACE, "%s DSSN and/or FLNU empty - doing nothing", pclFunc);
    return;
  }

  if (GetFlightDetails (pcpFlnu, pclAdid, pclFlno, pclSchedule) != RC_SUCCESS)
  {
    dbg(TRACE,"%s Flight not found urno <%s> or SQL error",pclFunc,pcpFlnu);
    return;
  }
  dbg(TRACE,"%s Flight found <%s><%s><%s>",pclFunc,pclFlno,pclAdid,pclSchedule);

  memset (pcgAftFld,'\0',sizeof(pcgAftFld));
  memset (pcgAftSel,'\0',sizeof(pcgAftSel));
  memset (pcgAftVal,'\0',sizeof(pcgAftVal));

  ScanConfig (pclAdid,pcpDssn,pcpFlnu); 

  sprintf (pcgAftSel,"WHERE URNO = '%s'",pcpFlnu);

  if (strlen (pcgAftVal) > 0)
  {
    ilRC = SendCedaEvent(igModIdFlight,0,pcgAftDestName,pcgAftRecvName,
              pcgAftTwStart,pcgAftTwEnd,pcgAftCmd,
              pcgAftTable,pcgAftSel,pcgAftFld,pcgAftVal,
              "",4,NETOUT_NO_ACK);

    dbg(DEBUG,"%s Selection <%s>",pclFunc,pcgAftSel);
    dbg(DEBUG,"%s FieldList <%s>",pclFunc,pcgAftFld);
    dbg(DEBUG,"%s ValueList <%s>",pclFunc,pcgAftVal);
    dbg(DEBUG,"%s Sent <%s>. Returned <%d>",pclFunc,pcgAftCmd,ilRC); 
  }
  else
  {
    dbg(TRACE,"%s Nothing to send to FLIGHT",pclFunc); 
  }

  return;
}

/********************************************************************************/

/******************************************************************************
 * Handy 
 ******************************************************************************
 */

static int TimeToStr(char *pcpTime,time_t lpTime, int ipFlag)
{
    struct tm *_tm;
    char   _tmpc[6];

    if (ipFlag == UTC_TIME)
      _tm = (struct tm *)gmtime(&lpTime);
    else if (ipFlag == LOCAL_TIME)
      _tm = (struct tm *)localtime(&lpTime);
    else
    {
      dbg(TRACE,"TimeToStr: ERROR: called with unknown flag - defaulting to UTC");
      _tm = (struct tm *)gmtime(&lpTime);
    }
    
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */

/******************************************************************************
 *
 *  LoadAftConfig - read and parse the configuration into the global 
 *	configuration structure. The structure is proposed so as to reduce
 *	repeated parsing code and re-reading the config file at each CEDA
 *	message.
 ******************************************************************************
 */

static void LoadAftConfig (void)
{

  char pclFunc[] = "LoadAftConfig: ";
  
  int ilRC = RC_SUCCESS;

  char pclSection [64], pclTemp [ENTRY_VALUE_SIZE], pclTemp2 [ENTRY_VALUE_SIZE];
  char pclTemp3[ENTRY_VALUE_SIZE],pclArgRaw [ENTRY_VALUE_SIZE];
  char pclAftArrPrio [ENTRY_VALUE_SIZE],pclAftDepPrio [ENTRY_VALUE_SIZE];
  char pclFormula [ENTRY_VALUE_SIZE];
  char pclArgName [ENTRY_VALUE_SIZE],pclDssnList [ENTRY_VALUE_SIZE];
  char pclLoaWhereClause [ENTRY_VALUE_SIZE];
  int  jj,kk,mm,nn,ilSection,ilFrmlIdx,ilVarIdx,ilNoPrio,ilNoArgs;
  int  repeated,ilNumSections;
  char pclType [32],pclStyp [32],pclSstp [32],pclSsst [32]; 

  ilNumSections = get_no_of_items(pcgAftSections);
  for(jj = 1; jj <= ilNumSections; jj++ )
  {
    ilSection = jj - 1;
    (void) get_real_item (pclSection, pcgAftSections, jj); 
    strcpy (sgConfig [ilSection].name,pclSection);

    ReadConfigEntry(pclSection,"AFT_FIELDS", pclTemp2, "");
    strcpy (sgConfig [ilSection].aft_fields, pclTemp2);
    
    ReadConfigEntry(pclSection,"AFT_ARR_PRIO", pclAftArrPrio, "");
    strcpy (sgConfig [ilSection].aft_arr_prio, pclAftArrPrio);

    ReadConfigEntry(pclSection,"AFT_DEP_PRIO", pclAftDepPrio, "");
    strcpy (sgConfig [ilSection].aft_dep_prio, pclAftDepPrio);

    ilFrmlIdx = 0;
    ilVarIdx = 0;

    /*
      Extract the formula names, then get each formula definition. Convert the
      spaces in the formula to commas for get_real_item later on.
    */
    ilNoPrio = get_no_of_items(pclAftArrPrio);
 
    for (kk = 1; kk <=  ilNoPrio; kk++)
    {
      (void) get_real_item (pclFormula, pclAftArrPrio, kk); 
      ReadConfigEntry(pclSection,pclFormula,pclTemp2, "");
      ReplaceChar(pclTemp2,' ',',');
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].name, pclFormula);
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].defn, pclTemp2);
      ilFrmlIdx++; 

      /* 
        Get each argument from the formula, then lookup the arg's definition, 
        and parse it into a dssn list and loatab where clause.
      */
      ilNoArgs = get_no_of_items (pclTemp2);
      for (mm = 1; mm <=  ilNoArgs; mm++)     
      {
        (void) get_real_item (pclArgName, pclTemp2, mm); 
        if ((strcmp (pclArgName,"+") == 0) || (strcmp (pclArgName,"-") == 0))
          continue;

        /*
          If arg already parsed, skip it. 
        */
        for (nn = 0,repeated=0; nn < N_PARAMETERS; nn++)
        {
          if ((sgConfig [ilSection].variable [nn].name [0]) == '\0')
            break;   /* reached end of already assigned variables */

          if (strcmp(sgConfig [ilSection].variable [nn].name,pclArgName) == 0)
          {
            dbg(DEBUG,"%s Argument <%s> repeated, skipped",pclFunc,pclTemp3);
            repeated = 1;
            break;
          } 
        }
        if (repeated == 1)
          continue; 

        ReadConfigEntry(pclSection,pclArgName,pclArgRaw,"");
        if (strlen (pclArgRaw) == 0) /* in case missed out in config */
          continue;
        ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclType,
          pclStyp, pclSstp, pclSsst);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].name, pclArgName);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].dssn, pclDssnList);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].type, pclType);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].styp, pclStyp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].sstp, pclSstp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].ssst, pclSsst);
        ilVarIdx++; 
      }
    }

    /*
       Next repeat for the departure formula. Skip repeated formula names.
    */

    ilNoPrio = get_no_of_items(pclAftDepPrio);
    for (kk = 1; kk <=  ilNoPrio; kk++)
    {
      (void) get_real_item (pclFormula, pclAftDepPrio, kk); 

      /*
        If formula was found in AFT_ARR_PRIO, skip it. Note that formula
        start only at entry index 3
      */
      for (nn = 0,repeated=0; nn < N_FORMULAE; nn++)
      {
        if ((sgConfig [ilSection].formula [nn].name [0]) == '\0')
            break;   /* reached end of already assigned formula */

        if (strcmp(sgConfig [ilSection].formula [nn].name,pclFormula) == 0)
        {
          dbg(DEBUG,"%s Formula <%s> repeated, skipped",pclFunc,pclFormula);
          repeated = 1;
          break;
        } 
      }
      if (repeated == 1)
        continue; 


      ReadConfigEntry(pclSection,pclFormula,pclTemp2, "");
      ReplaceChar(pclTemp2,' ',',');
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].name, pclFormula);
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].defn, pclTemp2);
      ilFrmlIdx++; 

      /* 
        Get each argument from the formula, then lookup the arg's definition, 
        and parse it into a dssn list and loatab where clause.
      */
      ilNoArgs = get_no_of_items (pclTemp2);
      for (mm = 1; mm <=  ilNoArgs; mm++)     
      {
        (void) get_real_item (pclArgName, pclTemp2, mm); 
        if ((strcmp (pclArgName,"+") == 0) || (strcmp (pclArgName,"-") == 0))
          continue;

        /*
          If arg already parsed, skip it. 
        */
        for (nn = 0,repeated=0; nn < N_PARAMETERS; nn++)
        {
          if ((sgConfig [ilSection].variable [nn].name [0]) == '\0')
            break;   /* reached end of already assigned variables */

          if (strcmp(sgConfig [ilSection].variable [nn].name,pclArgName) == 0)
          {
            dbg(DEBUG,"%s Argument <%s> repeated, skipped",pclFunc,pclArgName);
            repeated = 1;
            break;
          } 
        }
        if (repeated == 1)
          continue; 

        ReadConfigEntry(pclSection,pclArgName,pclArgRaw,"");
        if (strlen (pclArgRaw) == 0) /* in case missed out in config */
          continue;
        /*ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclLoaWhereClause); */

        ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclType,
          pclStyp, pclSstp, pclSsst);

        strcpy (sgConfig [ilSection].variable [ilVarIdx].name, pclArgName); 
        strcpy (sgConfig [ilSection].variable [ilVarIdx].dssn, pclDssnList);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].type, pclType);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].styp, pclStyp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].sstp, pclSstp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].ssst, pclSsst);
        ilVarIdx++; 

      }
    }
  } 
}

/******************************************************************************
 *
 *  ReplaceChar - lifted from fdihxx.src
 *
 ******************************************************************************
 */

static void ReplaceChar(char *pcpBuffer, char clOrgChar, char clDestChar)
{
   int ilI;

   if (strlen(pcpBuffer) > 0)
   {
      for (ilI = 0; ilI < strlen(pcpBuffer); ilI++)
      {
         if (pcpBuffer[ilI] == clOrgChar)
         {
            pcpBuffer[ilI] = clDestChar;
         }
      }
   }
}

/******************************************************************************
 *
 *  ExtractLoaWhereClauseValues - given a formula value argument raw definition,
 *	extract the DSSN list and convert the LOA fields into a where clause.
 *
 *	pcpArgDefn sample is [DSSN|,AIM,LDM,] [PAX,,,B]
 * 	or
 *	pcpArgDefn sample is [DSSN|,AIM,LDM,] [PAX,#,#,B]
 *
 ******************************************************************************
 */

static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, 
              char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst) 
{
  char	pclFunc[] = "ExtractLoaWhereClauseValues: ";
  char	pclTemp [ENTRY_VALUE_SIZE], *pclPtr1, *pclPtr2, pclTemp2 [ENTRY_VALUE_SIZE];
  int	jj;
  char  pclType [32],pclStyp [32], pclSstp [32], pclSsst [32];

  pclPtr1 = strstr (pcpArgDefn,"[DSSN|");
  if (pclPtr1 == '\0')
    return;

  while ((*pclPtr1) != '|')
    pclPtr1++;

  pclPtr1++;

  jj = 0;
  while ((*pclPtr1) != ']')
  {
    pcpDssnList [jj] = (*pclPtr1);
    pclPtr1++; jj++;
  }
  pcpDssnList [jj] = '\0';

  pclPtr1--;
  while ((*pclPtr1) != '[')
    pclPtr1++;

  pclPtr1++;

  pclPtr2 = pclPtr1;
  while ((*pclPtr2) != ']')
    pclPtr2++;

  memset (pclTemp2,'\0',sizeof(pclTemp2));
  strncpy (pclTemp2,pclPtr1,(pclPtr2-pclPtr1));

  (void) get_real_item (pclType, pclTemp2, 1);
  (void) get_real_item (pclStyp, pclTemp2, 2); 
  if ((strlen (pclStyp) == 0) || (pclStyp[0] == '#'))
    strcpy (pclStyp, " ");

  (void) get_real_item (pclSstp, pclTemp2, 3);
  if ((strlen (pclSstp) == 0) || (pclSstp[0] == '#'))
    strcpy (pclSstp, " ");

  (void) get_real_item (pclSsst, pclTemp2, 4);
  if ((strlen (pclSsst) == 0) || (pclSsst[0] == '#'))
    strcpy (pclSsst, " ");

  /* sprintf (pcpWhereClause," TYPE='%s' AND STYP='%s' AND SSTP='%s' AND SSST='%s' ",
    pclType,pclStyp,pclSstp,pclSsst); */
  strcpy (pcpType,pclType);
  strcpy (pcpStyp,pclStyp);
  strcpy (pcpSstp,pclSstp);
  strcpy (pcpSsst,pclSsst);

  return;

}

/******************************************************************************
 *
 *  	PrintLoaConfig
 *
 ******************************************************************************
 */

static void PrintLoaConfig (void)
{
  int	nn, ilSectionIdx;
  
  for (ilSectionIdx = 0; ilSectionIdx < N_SECTIONS; ilSectionIdx++)
  {
    if (sgConfig [ilSectionIdx].name [0] == '\0')
      break; /* reached end of sections in configuration */

    dbg(TRACE,"===============[%s]===========================================",
      sgConfig [ilSectionIdx].name);
    dbg(TRACE,"AFT_FIELDS <%s>",sgConfig [ilSectionIdx].aft_fields);
    dbg(TRACE,"AFT_ARR_PRIO <%s>",sgConfig [ilSectionIdx].aft_arr_prio);
    dbg(TRACE,"AFT_DEP_PRIO <%s>",sgConfig [ilSectionIdx].aft_dep_prio);

    dbg(TRACE,"------------ Formulae ------------------");

    for (nn = 0; nn < N_FORMULAE; nn++)
    {
      if (sgConfig [ilSectionIdx].formula [nn].name [0] == '\0')
        break;
      dbg(TRACE,"<%s> = <%s>",
        sgConfig [ilSectionIdx].formula [nn].name,
        sgConfig [ilSectionIdx].formula [nn].defn);
    }

    dbg(TRACE,"------------ Variables ------------------");

    for (nn = 0; nn < N_PARAMETERS; nn++)
    {
      if (sgConfig [ilSectionIdx].variable [nn].name [0] == '\0')
        break;
      dbg(TRACE,"<%s> : DSSN <%s> TYPE <%s> STYP <%s> SSTP <%s> SSST <%s>",
        sgConfig [ilSectionIdx].variable [nn].name,
        sgConfig [ilSectionIdx].variable [nn].dssn,
        sgConfig [ilSectionIdx].variable [nn].type,
        sgConfig [ilSectionIdx].variable [nn].styp,
        sgConfig [ilSectionIdx].variable [nn].sstp,
        sgConfig [ilSectionIdx].variable [nn].ssst);
    }

  dbg(TRACE,"===============[%s_END]===========================================",
    sgConfig [ilSectionIdx].name);
  }
  return;
}

/******************************************************************************
 *
 *  	DoSelect - G.P. handy, I hope..
 *
 ******************************************************************************
 */

static int DoSelect (char *pcpFieldList, char *pcpWhereClause, char *pcpTable,
    char *pcpDataBuf)
{
  char pclFunc[] = "DoSelect: ";
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  int ilNoOfRec = 0, ilNoOfItems;
  int ilGetRc = DB_SUCCESS;

  sprintf (clSqlBuf,"SELECT %s FROM %s WHERE %s",
    pcpFieldList,pcpTable,pcpWhereClause);
  dbg(TRACE,"%s <%s>",pclFunc,clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, pcpDataBuf);
  if (ilGetRc == RC_SUCCESS)
  {
    ilNoOfItems = get_no_of_items (pcpFieldList);
    BuildItemBuffer (pcpDataBuf, pcpFieldList, ilNoOfItems, ","); 
  }
  else if (ilGetRc == SQL_NOTFOUND)
  {
    dbg(TRACE,"%s SQL_IF not found <%d>",pclFunc,ilGetRc);
  }
  else
  {
    dbg(TRACE,"%s SQL_IF error <%d>",pclFunc,ilGetRc);
  }

  commit_work();
  close_my_cursor (&slCursor);
  return ilGetRc; 
}

/******************************************************************************
 *
 *  GetFlightDetails 
 *
 ******************************************************************************
 */

static int GetFlightDetails (char *pcpUrno, char *pcpAdid, char *pcpFlno,
              char *pcpSchedule)
{
  char	pclFieldList [1024];
  char  pclWhereClause [1024];
  char  pclDataBuf [1024],pclStoa [64],pclStod [64];
  int	ilRcDb;
  
  strcpy (pclFieldList,"ADID,FLNO,STOA,STOD"); 
  sprintf(pclWhereClause,"URNO = '%s'",pcpUrno);
  ilRcDb = DoSelect (pclFieldList, pclWhereClause, "AFTTAB", pclDataBuf);
  if (ilRcDb == RC_SUCCESS)
  {
    (void) get_real_item (pcpAdid, pclDataBuf, 1);
    (void) get_real_item (pcpFlno, pclDataBuf, 2);
    (void) get_real_item (pclStoa, pclDataBuf, 3);
    (void) get_real_item (pclStod, pclDataBuf, 4);
    if (strcmp (pcpAdid,"A") == 0)
      strcpy (pcpSchedule,pclStoa);
    else if (strcmp (pcpAdid,"D") == 0)
      strcpy (pcpSchedule,pclStod);
    else
      strcpy (pcpSchedule,"");
  }
  return ilRcDb;
}


/******************************************************************************
 *
 *  ScanConfig 
 *
 ******************************************************************************
 */

static int  ScanConfig (char *pcpAdid, char *pcpDssn, char *pcpFlnu)
{
  char pclFunc [] = "ScanConfig: ";
  char pclLoaCond [128];
  int  llCurLine,llMaxLine;
  char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
  char pclLoaSsst [32], pclLoaValu [32], pclLoaDssn [32];
  int  ilNumSections, ilSection, ilFormula, ilEntry, ilPrio; 
  int  nn,jj,kk,iFlagSumOk,iFlagStopFormulaScan,ilVarRes; 
  int  ilFormulaMembers,fm, ilRC = RC_SUCCESS;
  char pclCurPrio [ENTRY_VALUE_SIZE],pclFormula [ENTRY_NAME_SIZE];
  char pclFormulaMember [64],pclTemp [AFT_MSG_SIZE];
  int  ilSum, ilCurVal;
  char pclLastOp [32];

  /*
    Retrieve all LOATAB rows for the FLNO in one select
  */
  sprintf (pclLoaCond,"WHERE FLNU='%s'",pcpFlnu);
  LoadGridData(pcgLoaTabGrid, pcgLoaTabName, pclLoaCond);

  llMaxLine = CT_GetLineCount(pcgLoaTabGrid) - 1;

  dbg(DEBUG,"DSSN\t|TYPE\t|STYP\t|SSTP\t|SSST\t|VALU\t|"); 
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "DSSN", pclLoaDssn);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "TYPE", pclLoaType);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "STYP", pclLoaStyp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSTP", pclLoaSstp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSST", pclLoaSsst);
    /* no tab for VALU so can see if it is a space */
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "VALU", pclLoaValu);
    dbg(DEBUG,"%s\t|%s\t|%s\t|%s\t|%s\t|%s|",
      pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pclLoaValu);
  }

  /* Loop through all sections from config file */ 
  for (ilSection = 0; ilSection < N_SECTIONS; ilSection++)
  {
    if (sgConfig [ilSection].name [0] == '\0')
      break; /* reached end of sections in configuration */

    dbg(TRACE,"%s ========= Working on section [%s]=================",
      pclFunc,sgConfig [ilSection].name);

    strcpy(pclCurPrio,"");
    if (pcpAdid[0] == 'A')
    {
      strcpy (pclCurPrio,sgConfig [ilSection].aft_arr_prio);
    }
    else if (pcpAdid[0] == 'D')
    {
      strcpy (pclCurPrio,sgConfig [ilSection].aft_dep_prio);
    }

    ilPrio = get_no_of_items(pclCurPrio);
    iFlagStopFormulaScan = 0; /* set when a formula is completely successful */
    for (kk = 1; kk <=  ilPrio; kk++)
    {
      if (iFlagStopFormulaScan == 1)
      {
        dbg(TRACE,"%s Stop scanning formulae for <%s>",pclFunc,sgConfig [ilSection].name);
        break;
      }
      iFlagSumOk = 0;
      (void) get_real_item (pclFormula, pclCurPrio, kk); 
      dbg(TRACE,"%s ---------- Working on Formula <%s> ----------",pclFunc,pclFormula);
      
      for (ilFormula = 0; ilFormula < N_FORMULAE,iFlagStopFormulaScan == 0; ilFormula++)
      {
        ilSum = 0;
        if (sgConfig [ilSection].formula [ilFormula].name [0] == '\0')
          break; /* no more formula defined in this section */
        if (strcmp ((sgConfig [ilSection].formula [ilFormula].name),pclFormula) != 0)
          continue;

        /* Loop through all constituents of the formula line */
        ilFormulaMembers = get_no_of_items(sgConfig [ilSection].formula [ilFormula].defn);
        memset (pclLastOp, '\0', sizeof (pclLastOp));
        for (fm = 1; fm <=  ilFormulaMembers; fm++)
        {
          (void) get_real_item (pclFormulaMember,
            sgConfig [ilSection].formula [ilFormula].defn,fm); 

          if ((pclFormulaMember [0] != '+') && (pclFormulaMember [0] != '-'))
          {
            dbg(TRACE,"%s Working on variable <%s>",pclFunc,pclFormulaMember);
            ilVarRes = GetVariableValue (ilSection, pclFormulaMember, pcpDssn, &ilCurVal);
            if (ilVarRes == RC_FAIL)
            {
              dbg(TRACE,"%s No value for <%s> - abandon formula",
                pclFunc,pclFormulaMember);
              break;

            }
            else
            { 
              if (fm == ilFormulaMembers)
              {
                /*
                  We have now successfully found all members of the formula
                */
                iFlagStopFormulaScan = 1;

              }
              if (pclLastOp [0] != '\0')
              {
                if (pclLastOp [0] == '+')
                  ilSum += ilCurVal;

                if (pclLastOp [0] == '-')
                  ilSum -= ilCurVal; 

                dbg(DEBUG,"%s Operator <%c> on <%d>, new Sum = <%d>",
                  pclFunc,pclLastOp [0],ilCurVal,ilSum);
              }
              else
              {
                /* Must be the first variable in formula member list */
                ilSum = ilCurVal; 
                dbg(DEBUG,"%s Assign first variable <%d> to Sum",pclFunc,ilCurVal);
              }
            }
          }
          else
          { 
            pclLastOp [0] = pclFormulaMember [0];
            pclLastOp [1] = '\0';
            dbg(TRACE,"%s Found operator <%s>",pclFunc,pclLastOp);
          }
        }
      }
      dbg(TRACE,"%s ----- End work for Formula <%s> -------",pclFunc,pclFormula);
    }

    dbg(TRACE,"%s =======Section End [%s_END]================",
        pclFunc,sgConfig [ilSection].name);

    if (iFlagStopFormulaScan == 1)
    {
      if (strlen (pcgAftFld) == 0)
        strcpy (pcgAftFld,sgConfig [ilSection].aft_fields);
      else
      {
        strcat (pcgAftFld,",");
        strcat (pcgAftFld,sgConfig [ilSection].aft_fields);
      }

      if (strlen (pcgAftVal) == 0)
        sprintf (pcgAftVal,"%d",ilSum); 
      else
      {
        sprintf (pclTemp,",%d",ilSum);
        strcat (pcgAftVal,pclTemp); 
      }
    }
  }
  return ilRC;
}

/******************************************************************************
 *
 *  InitConfigStructure
 *
 ******************************************************************************
 */
static int  InitConfigStructure (void)
{
  int ilRC = RC_SUCCESS,jj,kk;

  /* Init the global configuration structure */
  /* Not the most elegant, but short of time. C++ map class preferred */

  dbg(TRACE,"InitConfigStructure starting");
  for (jj = 0; jj < N_SECTIONS; jj++)
  {
    memset (sgConfig [jj].name,'\0', ENTRY_NAME_SIZE);
    memset (sgConfig [jj].aft_fields,'\0', ENTRY_VALUE_SIZE);   
    memset (sgConfig [jj].aft_arr_prio,'\0', ENTRY_VALUE_SIZE);   
    memset (sgConfig [jj].aft_dep_prio,'\0', ENTRY_VALUE_SIZE);    
 
    for (kk = 0; kk < N_FORMULAE; kk++)   
    {
      memset (sgConfig [jj].formula[kk].name,'\0', ENTRY_NAME_SIZE);    
      memset (sgConfig [jj].formula[kk].defn,'\0', ENTRY_VALUE_SIZE);
    }

    for (kk = 0; kk < N_PARAMETERS; kk++)   
    {
      memset (sgConfig [jj].variable[kk].name,'\0', ENTRY_NAME_SIZE);    
      memset (sgConfig [jj].variable[kk].dssn,'\0', ENTRY_NAME_SIZE);    
      /*memset (sgConfig [jj].variable[kk].where,'\0', ENTRY_VALUE_SIZE); */
      memset (sgConfig [jj].variable[kk].type,'\0', 32);
      memset (sgConfig [jj].variable[kk].styp,'\0', 32);
      memset (sgConfig [jj].variable[kk].sstp,'\0', 32); 
      memset (sgConfig [jj].variable[kk].ssst,'\0', 32); 
    }
  }
  dbg(TRACE,"InitConfigStructure completed");

  return ilRC;
}

/******************************************************************************
 *
 *  InitMyTab - for ct use. Copied from cophdl
 *
 ******************************************************************************
 */

static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize)
{
  int ilRC = RC_SUCCESS;
  int i = 0;

  if (strcmp(pcpCtTabName,"STR_DESC") != 0)
  {
    CT_CreateArray(pcpCtTabName);
    CT_SetFieldList(pcpCtTabName,pcpCtFldList);
    CT_SetLengthList(pcpCtTabName, pcpCtFldSize);
    CT_SetDataPoolAllocSize(pcpCtTabName, 1000);
    dbg(TRACE,"CT DATA GRID <%s> INITIALIZED",pcpCtTabName);
  }
  else
  {

    for (i=0;i<=MAX_CTFLT_VALUE;i++)
	{
      CT_InitStringDescriptor(&argCtFlValue[i]);
      argCtFlValue[i].Value = malloc(4096);
      argCtFlValue[i].AllocatedSize = 4096;
      argCtFlValue[i].Value[0] = 0x00;
	}

	/* static STR_DESC rgCtTabLine; */
    CT_InitStringDescriptor(&rgCtTabLine);
    rgCtTabLine.Value = malloc(4096);
    rgCtTabLine.AllocatedSize = 4096;
    rgCtTabLine.Value[0] = 0x00;

	/* static STR_DESC rgCtTabData; */
    CT_InitStringDescriptor(&rgCtTabData);
    rgCtTabData.Value = malloc(4096);
    rgCtTabData.AllocatedSize = 4096;
    rgCtTabData.Value[0] = 0x00;



	/* static STR_DESC rgCtFlLine; */
    CT_InitStringDescriptor(&rgCtFlLine);
    rgCtFlLine.Value = malloc(4096);
    rgCtFlLine.AllocatedSize = 4096;
    rgCtFlLine.Value[0] = 0x00;


	/* static STR_DESC rgCtFlData; */
    CT_InitStringDescriptor(&rgCtFlData);
    rgCtFlData.Value = malloc(4096);
    rgCtFlData.AllocatedSize = 4096;
    rgCtFlData.Value[0] = 0x00;

	/* static STR_DESC rgCtHtLine; */
    CT_InitStringDescriptor(&rgCtHtLine);
    rgCtHtLine.Value = malloc(4096);
    rgCtHtLine.AllocatedSize = 4096;
    rgCtHtLine.Value[0] = 0x00;


	/* static STR_DESC rgCtTabList; */
	/* static STR_DESC rgCtTabUrno; */

/*
static STR_DESC rgCtAftLine;
static STR_DESC rgCtAftData;
static STR_DESC rgCtAftList;
static STR_DESC rgCtAftUrno;


static STR_DESC rgCtCfdLine;
static STR_DESC rgCtCfdData;
static STR_DESC rgCtCfgList;
static STR_DESC rgCtCfgUrno;

static STR_DESC rgCtFltLine;
static STR_DESC rgCtFltData;
static STR_DESC rgCtFltList;
static STR_DESC rgCtFltUrno;
*/

    dbg(TRACE,"STRING BUFFERS <%s> INITIALIZED",pcpCtTabName);
  }
  return ilRC;
} /* end of InitMyTab */

/******************************************************************************
 *
 *  LoadGridData - copied from cophdl
 *
 ******************************************************************************
 */

static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[1024] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  long llCurTabLine = 0;
  long llMaxTabLine = 0;
  int ilFldCnt = 0;
  int ilLegCnt = 0, ilCount;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"---------------------------------");
  dbg(TRACE,"LOADING <%s> RECORDS",pcpGridName);
  dbg(TRACE,"---------------------------------");
  CT_ResetContent(pcpGridName);
  CT_GetFieldList(pcpGridName, pclFldList);

  strcpy(pclTblName,pcpTblName);
  strcpy(pclSqlCond,pcpSqlKey);
  sprintf(pclSqlBuff,"SELECT %s FROM %s %s ",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);
  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  ilCount = 0;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      TAB_TrimRecordData(pclSqlData, ",");
      /*dbg(DEBUG,"<%s>",pclSqlData); */
      CT_InsertTextLine(pcpGridName,pclSqlData);
      ilCount++;
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"----Loaded %d records -------------------------------",ilCount);

  return ilRc;
}

/******************************************************************************
 *
 *  TAB_TrimRecordData - copied from cophdl
 *
 ******************************************************************************
 */

int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep)
{
  int ilCnt = 0;
  char *pclDest = NULL;
  char *pclSrc = NULL;
  char *pclLast = NULL; 
  /* last non blank byte position */
  /* but keep at least one blank */
  pclDest = pclTextBuff;
  pclSrc  = pclTextBuff;
  pclLast  = pclDest - 1;
  if (*pclSrc == ' ')
  {
    pclLast = pclTextBuff;
    pclSrc++;
    pclDest++;
  }
  while (*pclSrc != '\0')
  {
    if (*pclSrc == pcpFldSep[0])
    {
      pclDest = pclLast + 1;
      *pclDest = *pclSrc;
      pclSrc++;
      pclDest++;
      if (*pclSrc == ' ')
      {
        pclLast = pclDest;
      }
    }
    *pclDest = *pclSrc;
    if (*pclDest != ' ')
    {
      pclLast = pclDest;
    }
    pclDest++;
    pclSrc++;
  }
  pclDest = pclLast + 1;
  *pclDest = '\0';
  return ilCnt;
} /* end TAB_TrimRecordData */

/******************************************************************************
 *
 *  GetLoatabValue - find the matching line in the CT array of current LOA values.
 *	Return RC_SUCCESS is a valid (i.e. non space) value found.
 *	Return RC_FAIL if no value or a space is found.
 *
 ******************************************************************************
 */

static int  GetLoatabValue (char *pcpDssn, char *pcpType, char *pcpStyp,
              char *pcpSstp, char *pcpSsst, char *pcpValu)
{
  int  ilRC = RC_FAIL;
  char pclFunc [] = "GetLoatabValue: ";
  int  llCurLine,llMaxLine;
  char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
  char pclLoaSsst [32], pclLoaDssn [32];

  /*
    If dssn list begins with a comma the first call will be with
    an empty dssn.
  */
  if (strcmp (pcpDssn,"") == 0)
    return ilRC;

  llMaxLine = CT_GetLineCount(pcgLoaTabGrid) - 1;

  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "DSSN", pclLoaDssn);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "TYPE", pclLoaType);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "STYP", pclLoaStyp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSTP", pclLoaSstp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSST", pclLoaSsst);

    if ((strcmp (pcpDssn,pclLoaDssn) == 0) &&
      (strcmp (pcpType,pclLoaType) == 0) &&
      (strcmp (pcpStyp,pclLoaStyp) == 0) &&
      (strcmp (pcpSstp, pclLoaSstp) == 0) &&
      (strcmp (pcpSsst,pclLoaSsst) == 0))
    { 
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "VALU", pcpValu);
      dbg(TRACE,"%s Found match <%s><%s><%s><%s><%s><%s>",
        pclFunc,pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pcpValu);
      ilRC = RC_SUCCESS;

      if (strcmp (pcpValu," ") == 0)
      {
        dbg(TRACE,"%s But value is blank",pclFunc);
        ilRC = RC_FAIL;
      }
      break;
    }
  } 

  if (ilRC == RC_FAIL)
    dbg(TRACE,"%s No match for <%s><%s><%s><%s><%s>",
        pclFunc,pcpDssn,pcpType,pcpStyp,pcpSstp,pcpSsst);

  return ilRC;
}

/******************************************************************************
 *
 *  GetVariableValue - find variable name within a section, and try to get its
 *    value from Loatab grid. If received dssn is not in priority, RC_FAIL.
 *    If no value available, RC_FAIL.
 *
 ******************************************************************************
 */
static int  GetVariableValue (int ipSectionIdx, char *pcpVarName,
  char *pcpInputDssn, int *ipVal)
{
  char pclFunc [] = "GetVariableValue: ";
  int ilRC = RC_SUCCESS, ilRC2,ilNoDssn,jj;
  int idxVar;
  char pclCurVarDssn [32],pclValue [32];

  for (idxVar = 0; idxVar < N_PARAMETERS; idxVar++)
  {
    if (sgConfig [ipSectionIdx].variable[idxVar].name [0] == '\0')
    {
      dbg(TRACE,"%s Could not find variable <%s> in section <%s>",
        pclFunc,pcpVarName,sgConfig [ipSectionIdx].name);
      ilRC = RC_FAIL; 
      break;
    }

    if (strcmp (sgConfig [ipSectionIdx].variable[idxVar].name,pcpVarName) == 0)
    {
      if (strstr (sgConfig [ipSectionIdx].variable[idxVar].dssn,pcpInputDssn) == '\0')
      {
        dbg(TRACE,"%s Input DSSN <%s> not in variable's <%s> DSSN list <%s>",
          pclFunc,pcpInputDssn,sgConfig [ipSectionIdx].variable[idxVar].name,
          sgConfig [ipSectionIdx].variable[idxVar].dssn);
        ilRC = RC_FAIL; 
        break;
      }

      ilNoDssn = get_no_of_items (sgConfig [ipSectionIdx].variable[idxVar].dssn);
      ilRC2 = RC_FAIL;
      for (jj = 1; jj <= ilNoDssn; jj++ )
      {
        (void) get_real_item (pclCurVarDssn, sgConfig [ipSectionIdx].variable[idxVar].dssn, jj); 
        ilRC2 = GetLoatabValue (pclCurVarDssn,
          sgConfig [ipSectionIdx].variable[idxVar].type,
          sgConfig [ipSectionIdx].variable[idxVar].styp,
          sgConfig [ipSectionIdx].variable[idxVar].sstp,
          sgConfig [ipSectionIdx].variable[idxVar].ssst,
          pclValue);

        if (ilRC2 == RC_SUCCESS)
        {
          *ipVal = atoi (pclValue);
          dbg(TRACE,"%s Found match <%s><%s><%d>",pclFunc,
            sgConfig [ipSectionIdx].variable[idxVar].name,pclCurVarDssn,*ipVal);

          /* Stop scaning the variable's dssn list */
          break;
        } 
      }
      ilRC = ilRC2;
      /* No more scanning to find the matching variable name */
      break;
    }
  }
  
  return ilRC;
}
/******************************************************************************
 *
 *  
 *
 ******************************************************************************
 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
