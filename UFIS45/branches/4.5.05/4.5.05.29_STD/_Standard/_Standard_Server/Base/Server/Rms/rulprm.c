#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/rulprm.c 1.7 2011/03/29 12:17:51GMT ble Exp  $";
  static char svn_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/rulprm.c 1.7 2011/03/29 12:17:51GMT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/* 20110327 MEI: For pte jet/adhoc flight, ALC2 is blank. And it match all static group members for */
/*               ALTTAB if airlines only contain 3-letters airline code */ 
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/* static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I rulfnd.c 4.5.1.8 / 03/09/05 HAG"; */

/* static char sccs_version_rulfnd[] ="%Z% UFIS44 (c) ABB AAT/I %M% %I% / %E% %U% / VBL"; */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/


/*----------------------------------------------------------------*/ 
/* short names for the DEBUG and TRACE output                     */
/* ------------------------------------------                     */
/*                                                                */
/* SendFieldListToOne   : SFLTO                                   */ 
/* SendFieldListToAll   : SFLTA                                   */ 
/* ReallocOutBuf        : ROB                                     */
/* CheckCondition       : CC                                      */
/* ProcessConditions    : PC                                      */
/* CheckCDT0            : CCDT0                                   */
/* CheckCDT1            : CCDT1                                   */
/* CheckCDT2            : CCDT2                                   */
/* GetFieldData         : GFDAT                                   */
/* CheckRuleCalendar    : CRCAL                                   */
/* ProcessRules         : PR                                      */
/* ProcessIPRules       : PR_IP                                   */
/* ProcessTemplates     : PT                                      */
/* SendFlightIndepRules : SFIRUL                                  */
/* FieldCompare         : FCMP                                    */
/*                                                                */
/*                                                                */
/*                                                                */
/*                                                                */
/*                                                                */
/*----------------------------------------------------------------*/ 



/*----------------------------------------------------*/ 
/* YOU NEED !!!                                       */ 
/*----------------------------------------------------*/ 
/* !!! parts,         what you used !!!!              */ 
/*  chkval.c          libsource find in $LIBSRC/DB    */ 
/*  rmsch.lexx/yacc   find in $LIBSRC                 */ 
/*----------------------------------------------------*/ 



/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "chkval.h"       
#include "db_if.h"
#include "ruldiag.h"

extern int get_no_of_items(char *s);
extern int PrintRuleCheck(char *);
extern int  AddSecondsToCEDATime(char *, time_t, int);
extern int  GetServerTimeStamp(char *, int, long, char *);

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#define RUL_FLD_LEN             (4)

#define EVT_TURNAROUND          (0)
#define EVT_INBOUND         (1)
#define EVT_OUTBOUND            (2)
#define EVT_FLIGHT_INDEPENDENT  (3)
#define EVT_UNKNOWN             (4)

#define USE_AZATAB              (1) 
#define USE_NOT_AZATAB          (0) 

/* ---------------------------------- COMMAND DEFINES ------------------- */ 
#define CMD_SFL     (1)
#define CMD_IFR     (2)
#define CMD_UFR     (3)
#define CMD_DFR     (4)
#define CMD_IRT     (5)
#define CMD_URT     (6)
#define CMD_DRT     (7)
#define CMD_FIR     (8)
#define CMD_IFI     (9)
#define CMD_UFI     (10)
#define CMD_DFI     (11)
#define CMD_IAZ     (12)
#define CMD_UAZ     (13)
#define CMD_DAZ     (14)
#define CMD_CFL     (15)
#define CMD_CKR     (16)  
#define CMD_PFL     (17)
#define CMD_PRM     (18)
#define CMD_ANSWER  (19)

static char *prgCmdTxt[] = {     "SFL",    "IFR",    "UFR",    "DFR",    "IRT",    "URT",    "DRT",    "FIR",    "IFI",    "UFI",    "DFI",    "CFL",    "IAZ",    "UAZ",    "DAZ",    "PFL", "PRM", "ANSWER", NULL };
static int    rgCmdDef[] = {  CMD_SFL , CMD_IFR , CMD_UFR , CMD_DFR , CMD_IRT , CMD_URT , CMD_DRT , CMD_FIR , CMD_IFI , CMD_UFI , CMD_DFI , CMD_CFL , CMD_IAZ , CMD_UAZ , CMD_DAZ , CMD_PFL , CMD_PRM,  CMD_ANSWER, 0 };
/*----------------------------------------------------------------------- */ 

#define CDT_NULL    (7)
#define CDT_SIMPLE  (0)
#define CDT_DYNAMIC (1)   /* 1 */ 
#define CDT_STATIC  (2)   /* 2 */ 


#define CDT_ASC_NULL    ((char) (CDT_NULL    + 48)) 
#define CDT_ASC_SIMPLE  ((char) (CDT_SIMPLE  + 48))
#define CDT_ASC_DYNAMIC ((char) (CDT_DYNAMIC + 48))
#define CDT_ASC_STATIC  ((char) (CDT_STATIC  + 48))


#define CMP_FALSE   (1)
#define CMP_SMALLER (2)
#define CMP_EQUAL   (3)
#define CMP_GREATER (4)

#define TPL_ACTIVE  (1)

#define RUE_INACTIVE  (0)
#define RUE_ACTIVE    (1)
#define RUE_ARCHIVED  (2)

#define RTY_SINGLE       (0)
#define RTY_COLLECTIV    (1)
#define RTY_FIR          (3)
#define RTY_INDEPENDENT  (2)

#define IS_O_VERSION     (1)
#define IS_P_VERSION     (0)

/* static array informations */
#define O_ORGININATOR_ID       (7550)
#define P_ORGININATOR_ID       (7560)

#define ARR_NAME_LEN           (8)
#define ARR_FLDLST_LEN         (256)
#define FORWARD_PL_DEFAULT     (0)
#define FORWARD_OP_DEFAULT     (0)
#define ACT_PMODE              (0)

#define C_MAX_STRING_SIZE      (8192) 

#define DIAGINT     0
#define DIAGEXT     1

#define DATELEN         14
#define URNOLEN         10
#define DIAGTEXTLEN     128
#define NUMBLEN         3

/*-------------------------------------------------------*/
/* process prio is defined as:   normal (DEFAULT) = 20 ; */ 
/*                               lowest prio.     = 39 ; */
/*                               highest prio.    =  0 ; */ 
/*-------------------------------------------------------*/
/* #define LOW_PROC_PRIO    (32)  */    /* rulfndp */ 
/* #define HIGH_PROC_PRIO   (10)  */    /* rulfndo */ 

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp              = NULL ;*/
int  debug_level        = DEBUG;
static int igTestOutput = 3 ;                     /* 0 ........ 9 (most info) */ 
extern char  cgSelStr[];
#define DPV_FIELDS  "PRID,NAME,NATL,LANG,GEND,MAIL,OWHC,PRMT,FLNU,TFLU,TLXU,SEAT,ABDT,ABSR,ALOC,SATI,AATI,HOPO,CDAT,LSTU,URNO,USEC,USEU,FDAT,WCTP,ACCP,DOGP,ACT5,ADID,ORG3,DES3,ALC3,PRMC,FLTI,STOA,STOD,ETOA,ETOD,TIFA,TIFD,WRO1,PSTA,PSTD,ACT3,SOUR,AFTU,FTYP,ETDA,ETDI,ETAI"
#define DPV_FIELDCOUNT 50
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static int     igUsedAzaTab    = USE_NOT_AZATAB;   /* USE_AZATAB or USE_NOT_AZATAB */     
static ITEM   *prgItem         = NULL;             /* The queue item pointer  */  
static EVENT  *prgEvent        = NULL;             /* The event pointer       */
static int     igItemLen       = 0;                /* length of incoming item */  
static EVENT  *prgOutEvent      = NULL ;
static long    lgOutEventSize  = 0 ;
static int     igInitOK        = FALSE ; 
static long    lgEvtCnt        = 0 ;
static long    lgTimeOffset    = 259200 ;         /* 60 * 60 * 24 * 3 */
static char    cgConfigFile[512] ;
static char    cgHopo[8] ;                        /* default home airport    */
static char    cgAddFields[256] ;
static char    cgTabEnd[64] ; 
static char    cgFieldListDest[256] ;
static char     cgFirTemplates[512];
static char     cgIgnoreFtypIn[21]="";
static char     cgIgnoreFtypOut[21]="";
static int     igSaveEvent      = FALSE ;
static int     igEventType      = 0 ;
static char   *pcgFieldList     = NULL ;
static char   *pcgInbound       = NULL ;
static char   *pcgOutbound      = NULL ;
static long   plgInbBuffLen = 0;
static long   plgOutbBuffLen = 0;
static char   *pcgInboundUrno   = NULL ;
static char   *pcgOutboundUrno  = NULL ;
static char    pcgUtpl[32] ;
static int     igRecOrgQueLen ; 
static char    pcgSearchTable[64] ;   /* AFT or AZA table name */ 
static int     igSearchTableAction = 0 ; 
static  int     igStartUpMode = TRACE;
static  int     igRuntimeMode = 0;
static int     igRulprmModid = -1;
static int     igDemprmModid = -1;
static int     bgIsPrm = FALSE;
static char    cgPrmTemplates[1024] = "";
static char    cgErrMsg[2048];
static char  *pcgPrmBuf = NULL;
static int      igPrmBufLen = 0;
 
static char  *pcgPrmEventBuf = NULL;
static int      igPrmEventBufLen = 0;
static char   cgPrmUrnoList[4096];

static long    lgUrnoListLen   = 0 ;
static char   *pcgUrnoListBuf  = NULL ;  
static char   *pcgPrio         = NULL ;
static int     igDest          = 9000 ;
static int     igFIRDest       = 9000 ;
static int     igFlightDataDest = -1;
static int     igFlightDelDest = -1;
static int     igOpTimeFrame = -1;

static char    cgSoh[2] ;
static char    cgStx[2] ;
static char    cgEtx[2] ;
static char    cgDc1[2] ;

static int     lgOPFlag ; 
static long    lgforwardRouting=0L ;    

static long    igMyOriginator  = 0 ;
static long    igRecOriginator = 0 ;
static long    igParOriginator = 0 ;

static BOOL     bgDiagInt = FALSE;
static BOOL     bgDiagExt = FALSE;
static BOOL     bgDiagOn = FALSE;

static char cgDiagBuffer[134];              
static char cgEvalBuffer[201];              

static char    cTmpInBuffer [C_MAX_STRING_SIZE] ; 
static char    *pcgTotalFieldList=0 ; 
/*  next two variables to undo character mapping performed by classlib when */
/*  writing to database                                                     */
/*                        "   '  , [LF] [CR] :  {   }   [   ]   |   ^   ?  $  (  )  ;  @    */
static char cgClientChars[] = "\042\047\54\012\015\072\173\175\133\135\174\136\77\44\50\51\73\100"; 
static char cgServerChars[] = "\260\261\262\264\263\277\223\224\225\226\227\230\231\232\233\234\235\236";
static  char    cgTdi1[24]="";
static  char    cgTdi2[24]="";
static  char    cgTich[24]="";
static char cgExclude[]="exclude";

FILE    *pfgDiagIn;
FILE    *pfgDiagIntOut;
FILE    *pfgDiagExtOut;
static char     cgTplFields[101];
static int igTplFTYA = -1;
static int igTplFTYD = -1;

static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgQueDiff = 0;
static time_t tgMaxStampDiff = 0;
static BOOL bgShowEvent = FALSE;
static int  igFIRDuration=24;
static int  igFIRDayOffset=0;

static int igMaxEvtHistNo = 50;             /* max. count of events to keep in history */
static int igMaxEvtHistLenght = 10 * 60;    /* max. length of time to keep events in history */

static char   *pcgOuri = NULL;
static char   *pcgOuro = NULL;
static long   lgOuriLen = 0;
static long   lgOuroLen=0;   
static bgOuriFound = FALSE;
static bgOuroFound = FALSE;

struct _arrayinfo
{
  HANDLE   rrArrayHandle;
  char     crArrayName[ARR_NAME_LEN+1];
  char     crArrayFieldList[ARR_FLDLST_LEN+1];
  long     lrArrayFieldCnt;
  char    *pcrArrayRowBuf;
  long     lrArrayRowLen;
  long    *plrArrayFieldOfs;
  long    *plrArrayFieldLen;
  HANDLE   rrIdx01Handle;
  char     crIdx01Name[ARR_NAME_LEN+1] ;
  char     crIdx01FieldList[ARR_FLDLST_LEN+1] ;
  long     lrIdx01FieldCnt ;
  char    *pcrIdx01RowBuf ;
  long     lrIdx01RowLen ;
  long    *plrIdx01FieldPos ;
  long    *plrIdx01FieldOrd ;
  HANDLE   rrIdx02Handle;
  char     crIdx02Name[ARR_NAME_LEN+1] ;
  char     crIdx02FieldList[ARR_FLDLST_LEN+1] ;
} ;
typedef struct _arrayinfo ARRAYINFO ;


struct _sDateInfo 
{
 char    cDateStampStr [32] ; 
 int     iYear ;
 int     iMonth ; 
 int     iDate ;
 int     iHour ;
 int     iMins ;
 int     iSecs ; 
 int     iStDateInterpret ; 
 int     iStTimeInterpret ; 
} ; 
typedef struct _sDateInfo DATEINFO ;  



static ARRAYINFO rgTplArray ;
static ARRAYINFO rgRueArray ;
static ARRAYINFO rgDgrArray ;
static ARRAYINFO rgSgrArray ;
/* static ARRAYINFO rgSgmArray ; currently not used */
static ARRAYINFO rgSgrEvalArr;
static ARRAYINFO rgFeleEvalArr; /* saves field lengths */
static ARRAYINFO rgEvtHistArr;

static ARRAYINFO rgDiagInArray;     /*contains text for diagnostic file*/

struct _condinfo 
{
 char   crTable[8] ;
 char   crField[8] ;
 char   crIotf[8] ;
 long   lrIotf ;
 char   crType[8] ;
 long   lrType ;
 char   crOp[8] ;
 char   crValue[2048] ;
 char   crRefTab[8] ;
 char   crRefFld[8] ;
 long   lrChkInbound ; 
 long   lrChkOutbound ;
} ;
typedef struct _condinfo CONDINFO ;

static CONDINFO rgCondition ;


/*------------------ add. structs GH -------------------- */ 
struct _condblockctrl 
{
 CONDINFO          spCond ;
 void             *CI_PREV ;
 void             *CI_NEXT ;
} ;



typedef struct _condblockctrl COND_BLOCK_INFO ; 

/* define for max condition size */ 
#define CONDITION_SIZE     2100  
#define C_MAX_CONDITIONS   20 

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_rulfnd();
static int SetArrayInfo (char *pcpArrayName, char *pcpArrayFieldList, char *pcpIdx01Name, 
                         char *pcpIdx01FieldList, char *pcpIdx01Order, 
                         char *pcpSelection, ARRAYINFO *prpArrayInfo);
static int  ReallocOutBuf(void);

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo) ;

static int  Reset(void) ;                       /* Reset program          */
static void Terminate(int ipSleep) ;            /* Terminate program      */
static void HandleSignal(int) ;                 /* Handles signals        */
static void HandleQueErr(int) ;                 /* Handles queuing errors */
static void HandleQueues(void) ;                /* Waiting for Sts.-switch*/

static int  GetFieldLength(char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpTana, char *pcpFieldList, long *pipLen);

static int  CheckCondition(char *pcpCondition); /* interface to LexYacc */

static int  ProcessConditions(char *pcpHopo, char *pcpRuleCDs, long *plpIsTrue);

static int  GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, char *pcpData, char **pppFieldData);
static int  GetFieldData2(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, 
                         char *pcpData, char **pppFieldData, long *lpBufLen );
static int  CheckRuleCalendar(char *pcpUrno, char *pcpActualDate, long *plpIsValid) ;
static int CheckLocalDate(char *pcpUrno, char *pcpActualDate, long *plpIsValid);
static int  ProcessRules(char *pcpHopo, char *pcpTplUrno, char *pcpActualDate) ;
static int  ProcessIPRules(char *pcpHopo, char *pcpTplUrno, char *pcpActualDate) ; 

static int  ProcessTemplates(char *pcpHopo, char *pcpActualDate, char *);

static int  FieldCompare(char *pcpTableName, char *pcpFldNam1, char *pcpFldVal1, char *pcpFieldList, char *pcpData, int ipFunc);

static int  MyCompare(const void*, const void*);
static int  SendFieldListToOne(int ipDest);
static int  SendFieldListToAll(char *pcpFieldList);
static int  SendFlightIndepRules(char *pcpHopo, int ipDest, char *dateFrom, char *dateTo) ;

static int GetEventType(int *plpEventType, char **pcpInbound, char **pcpOutbound, 
                        long *plpInbBuffLen, long *plpOutbBuffLen, char *pcpData);
static int  GetCommand(char *pcpCommand, int *pipCmd);
static int  GetCedaTimestamp(char *pcpResultBuf,int ipResultLen, long lpTimeOffset);

static int  HandleData(EVENT *prpEvent);       /* Handles event data     */

static int ForwardEvent(int ipDest, int ipSendDate, char *cpSendDate, char *pcpData );

static int  SaveEvent(void);
static int  SaveIndexInfo(ARRAYINFO *prpArrayInfo);

static int  CheckCDT2 (char *pcpHopo, CONDINFO psCondition, long *plpIsTrue) ;
static int  CheckCDT1 (char *pcpUtpl, char *pcpHopo, CONDINFO psCondition, long *plpIsTrue) ;
static int  CheckCDT0 (char *pcpHopo, CONDINFO psCondition, char *pcpSearchStr, long *plpIsTrue) ;

static int  AnalyseStr (char *cpSrcStr, char *cpTabname, long *lpFldCnt, 
                        char *cpFieldStr, long *lpUsedChars) ; 
static int  SetCmpStringCond (char *slCondition, char *cpcmpStr, char *cpDataArea ) ;

void ReplaceChar ( char *pcpStr, char cpOld, char cpNew ) ;
void UndoCharMapping ( char* pcpString );

static int  SendAnswer(EVENT *prpEvent, int ipRc) ; 
static int  SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, CMDBLK *prpCmdblk, 
                      char *pcpSelection, char *pcpFields, char *pcpData) ; 
static int  CmpStrgs (char *cmpPar1, char *cmpPar2, int ipCmpLen) ;
static int  ChkDateTimeRng (char *pcpFrom, char *pcpTo, char *pcpAct) ;
static int  getDateTimefromString (char *cpFrom, char *cpTo, char *cpAct, char CDIV) ; 

static int  interpretDateInfo (char *charDate, DATEINFO *st_Date ) ;  
static int  FindSgrEvaluation ( char *pcpHopo, char *pcpTable, 
                               char *pcpField, char *pcpGroup,
                               char *pcpValue, long *plpIsTrue ) ;
static int SaveSgrEvaluation ( char *pcpHopo, char *pcpTable, 
                               char *pcpField, char *pcpGroup,
                               char *pcpValue, long plpIsTrue ) ;
static int GetItemNo(char *pcpFieldName,char *pcpFields, int *pipItemNo);
static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int DeleteFromSgrEval ( char *pcpHopo, char *pcpSgrUrno, char *pcpSgrGrpn );
static int SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
                           char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
                           ARRAYINFO *prpArrayInfo);
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);
static int UpdateAddFields ( char *pcpFields, int *ipChanged );
static int BuildTotalFieldList ();
void TrimRight(char *s);
static int GetDataFromBuffer(char *, char *, char *, char *, char *, int);
static int CompareTplUrno(char *, char *);
static int OpenDiagFile(char *);
static int CheckDiagFlag(char *);
static int WriteDiagFile(int, int, char *, char *, char *);
static int  ReadDiagIn( void );
static int  GetDebugLevel(char *, int *);
static int UtcToLocal(char *pcpTime);
static int LocalToUtc(char *pcpTime);
static int InitTichTdi();
static int GetBeginOfDay(char *pcpTime, char *pcpBegin);
static int  CheckFirTemplates(char *pcpHopo, char *pclPipe);
static int CheckFtypForTpl ( int ipEvtType, char *pclTplRow, int *pipTypeToEval );
static int IsFlightOutsideOpRange ( char *pcpFieldList );
static void CheckPerformance (int ipStart, char *pcpRtTime);
static int GetHopo ( char *pcpTwEnd, char *pcpHopo );
static int GetLastModid4Flight ();
static int AddToHistory ( int ipModid );
static int ForwardPrmEvent(int ipDest, int ipSendDate, char *cpSendDate, char *pcpData );
static int check_ret1(int ipRc,int ipLine);

/* functions, only for test */ 
/* static int TestAnalyseStr(void) ;  */



/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int   ilRC  = RC_SUCCESS ;            /* Return code  */  
  int   ilCnt = 0;

  INITIALIZE ;                          /* General initialization */

  dbg(TRACE,"MAIN: version <%s>", mks_version) ;

  /* Attach to the MIKE queues */
  do
  {
   ilRC = init_que() ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(TRACE, "MAIN: init_que() failed! waiting 6 sec...") ;
    sleep (6) ;
    ilCnt++ ;
   } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;

  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...") ;
   sleep(60) ;
   if (pfgDiagIntOut)
        fclose(pfgDiagIntOut);
   if (pfgDiagExtOut)
        fclose(pfgDiagExtOut);
    exit(1) ;
  }
  else
  {
   dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>", mod_id) ;
  } /* end of if */

  do
  {
   ilRC = init_db() ;
   if (ilRC != RC_SUCCESS)
   {
    check_ret(ilRC) ;
    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...") ;
    sleep(6) ;
    ilCnt++ ;
   } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;

  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...") ;
   sleep(60) ;
   if (pfgDiagIntOut)
        fclose(pfgDiagIntOut);
   if (pfgDiagExtOut)
        fclose(pfgDiagExtOut);
   exit(2) ;
  }
  else
  {
   dbg(TRACE, "MAIN: init_db() OK!") ;
  } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRC = TransferFile(cgConfigFile);
  if(ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile) ;
  } /* end of if */

  sprintf(cgConfigFile,"%s/rulfnd.cfg",getenv("CFG_PATH"));
    if (strncmp(mod_name,"rulprm",6) == 0)
    {
    sprintf(cgConfigFile,"%s/rulprm.cfg",getenv("CFG_PATH"));
    }
    dbg(TRACE,"using <%s> as config file",cgConfigFile);
  ilRC = TransferFile(cgConfigFile);
  if (ilRC != RC_SUCCESS)
  { 
   dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
  }
  else
  {
   dbg(TRACE,"MAIN: using <%s> as config file",cgConfigFile);
  } /* end of if */

  ilRC = SendRemoteShutdown(mod_id);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
  } /* end of if */

  if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
  {
   dbg(TRACE,"MAIN: waiting for status switch ...");
   HandleQueues();
  } /* end of if */

  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
  {
   dbg(TRACE,"MAIN: initializing ...") ;
   if (igInitOK == FALSE)
   {
    ilRC = Init_rulfnd() ;
    dbg (DEBUG,"---------------  MAIN: initializing OK ---------------- "); 

    if (ilRC != RC_SUCCESS)
    {
     dbg (TRACE,"MAIN: init failed!") ;
    } /* end of if */
   } /* end of if */
  } 
  else 
  {
   Terminate(30);
  } /* end of if */

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");

  strcpy (pcgSearchTable, "AFTTAB") ;    /* set default search table */ 
  igSearchTableAction = 0 ;              /* -.-                  AFT */

  /* set paralellProcId. p/O proc. */
  if (O_ORGININATOR_ID == mod_id)
  {  
   igParOriginator = P_ORGININATOR_ID ; 
  }
  else 
  {
   if (P_ORGININATOR_ID == mod_id)
   {
    igParOriginator = O_ORGININATOR_ID ; 
   }
   else 
   {
    dbg (DEBUG,"MAIN: parallel Orginator NOT set!");
   }
  }

  for (;;)
  {
   ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_4, igItemLen, (char *)&prgItem) ;

   /* depending on the size of the received item  */
   /* a realloc could be made by the que function */
   /* so do never forget to set event pointer !!! */

   prgEvent = (EVENT *) prgItem->text ;

   igRecOriginator = prgItem->originator ; 
   igRecOrgQueLen  = prgItem->msg_length ; 

   if ((prgItem->function != QUE_PUT) && (prgItem->function != QUE_PUT_FRONT))
   {
    if ( igTestOutput > 2 ) 
    {
     dbg (DEBUG, "=====================") ; 
     DebugPrintItem  (DEBUG, prgItem) ;    
     DebugPrintEvent (DEBUG, prgEvent) ;     
     dbg (DEBUG, "=====================") ;  
    }
   } /* end of if */

   if ( ilRC == RC_SUCCESS )
   {                                        /* Acknowledge the item */
    ilRC = que(QUE_ACK,0,mod_id,0,0,NULL) ;
    if( ilRC != RC_SUCCESS ) 
    {                                       /* handle que_ack error */
     HandleQueErr (ilRC) ;
    } /* fi */

    lgEvtCnt++ ;
    switch( prgEvent->command )
    {
      case   HSB_STANDBY        :
                                  dbg (TRACE, "MAIN: HSB_STANDBY") ;  
                                  ctrl_sta = prgEvent->command ;
                                  HandleQueues() ;
                                 break ;

      case   HSB_COMING_UP  :
                                  dbg (TRACE, "MAIN: HSB_COMING_UP") ;  
                              ctrl_sta = prgEvent->command ;
                                  HandleQueues() ;
                             break ;

       case   HSB_ACTIVE    :
                                  dbg (TRACE, "MAIN: HSB_ACTIVE") ;  
                              ctrl_sta = prgEvent->command ;
                                 break ;

       case   HSB_ACT_TO_SBY    :
                                  dbg (TRACE, "MAIN: HSB_ACT_TO_SBY") ;    
                                  ctrl_sta = prgEvent->command ;
                                  /* CloseConnection() ; */
                                  HandleQueues() ;
                                 break ;

       case   HSB_DOWN   :
                                  dbg (TRACE, "MAIN: HSB_DOWN") ;         
                               /* whole system shutdown - do not further use */ 
                               /* que(), send_message() or timsch() !        */
                                  ctrl_sta = prgEvent->command ;
                                  Terminate (1) ;
                                 break ;

       case   HSB_STANDALONE    :
                                  dbg (TRACE, "MAIN: HSB_STANDALONE");   
                                  ctrl_sta = prgEvent->command ;
                                  ResetDBCounter() ;
                                 break ;

       case   REMOTE_DB :         /* ctrl_sta is checked inside */
                                  HandleRemoteDB (prgEvent) ;
                                 break ;

       case   SHUTDOWN  :
                                  dbg(TRACE,"MAIN: SHUTDOWN") ; 
                                  /* process shutdown - maybe from uutil */
                                  Terminate(1) ;
                                 break ;
                    
       case   RESET     :
                                  ilRC = Reset() ;
                                 break ;
                    
       case   EVENT_DATA  :
                        if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                        {
                         ilRC = HandleData (prgEvent) ;
                         if (ilRC != RC_SUCCESS)        
                         {
                          dbg(TRACE, "MAIN: HandleData failed <%d>",ilRC) ;
                         } /* end of if */
            }
                        else
                        {
             dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta) ;
             DebugPrintItem(TRACE,prgItem) ;
             DebugPrintEvent(TRACE,prgEvent) ;
            } /* end of if */
            break ;

       case   665 :
                                ilRC = CEDAArrayReadAllHeader(outp) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg(TRACE,"MAIN: CEDAArrayReadAllHeader failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: CEDAArrayReadAllHeader OK") ;
                        } /* end of if */
                      break ;

       case   666 :
                                ilRC = SaveIndexInfo(&rgTplArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg(TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg(TRACE, "MAIN: SaveIndexInfo OK") ;
                                } /* end of if */
                               break ;

       case   667 :
                                ilRC = SaveIndexInfo(&rgRueArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                                } /* end of if */
                               break ;

       case   668 :
                                ilRC = SaveIndexInfo(&rgDgrArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>",ilRC) ;
                                }
                                else
                                {
                                 dbg(TRACE,"MAIN: SaveIndexInfo OK") ;
                                } /* end of if */
                               break ;

       case   669 :
                                ilRC = SaveIndexInfo(&rgSgrArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE,"MAIN: SaveIndexInfo OK") ;
                                } /* end of if */
                               break ;

/*       case   670 :
                                ilRC = SaveIndexInfo(&rgSgmArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                                }  
                               break ;*/
        case   671 :
                                ilRC = SaveIndexInfo(&rgSgrEvalArr) ;
                                if (ilRC != RC_SUCCESS)
                                  dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                else
                                  dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                               break ;
        case   672 :
                                ilRC = SaveIndexInfo(&rgFeleEvalArr) ;
                                if (ilRC != RC_SUCCESS)
                                  dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                else
                                  dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                               break ;
        case   675 :           ilRC = SaveIndexInfo(&rgEvtHistArr) ;
                               if (ilRC != RC_SUCCESS)
                                  dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                               break ;
        case 678 :
            ReadDiagIn();
            break;

       case   777 :             ilRC = SaveIndexInfo (&rgTplArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ;
                                } /* end of if */
                                ilRC = SaveIndexInfo (&rgRueArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>",ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ;  
                                } /* end of if */
                                ilRC = SaveIndexInfo(&rgDgrArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>", ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                                } /* end of if */
                                ilRC = SaveIndexInfo (&rgSgrArray) ;
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg(TRACE, "MAIN: SaveIndexInfo failed <%d>",ilRC) ;
                                }
                                else
                                {
                                 dbg(TRACE, "MAIN: SaveIndexInfo OK") ; 
                                } /* end of if */
                                /*
                                ilRC = SaveIndexInfo(&rgSgmArray) ; 
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo failed <%d>",ilRC) ;
                                }
                                else
                                {
                                 dbg (TRACE, "MAIN: SaveIndexInfo OK") ; 
                                }  */
                               break ;

       case   999 :             dbg(DEBUG, "MAIN: FIR") ;
                                strcpy(cgFirTemplates, "\0");
                                ilRC = SendFlightIndepRules(&cgHopo[0], igFIRDest, "", "") ;  
                                if (ilRC != RC_SUCCESS)
                                {
                                 dbg (TRACE, "MAIN: SendFlightIndepRules failed") ;
                                } /* end of if */
                               break ;

       case   TRACE_ON :        dbg_handle_debug (prgEvent->command) ;
                               break ;

       case   TRACE_OFF :       dbg_handle_debug (prgEvent->command) ;
                               break ;

       default          :       dbg(TRACE, "MAIN: unknown event") ;
                                DebugPrintItem (TRACE, prgItem) ;
                                DebugPrintEvent (TRACE, prgEvent) ;
                               break ;
    } /* end switch */
   } 
   else 
   {                           /* Handle queuing errors */
    HandleQueErr (ilRC) ;
   } /* end else */
  } /* end for */
                         /* exit(0) ;   <-- statement not reached */
} /* end of MAIN */



/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_rulfnd()
{
 int   ilRC       = RC_SUCCESS ;    /* Return code */
 int   ilDummy    = 0 ;
 long  llRowLen   = 0 ;
 long  ilHLen     = 0 ; 
 char  clSection   [64] ;
 char  clKeyword   [64] ;
 char  clQueueName [16] ;
 long   pllAddFieldLens[7], llFieldLength;
    char    clYesOrNo[1+1];
    int ilCol, ilPos;
 char  clSelection[101];

 debug_level = DEBUG;
 rgSgrEvalArr.rrArrayHandle = -1;
 rgFeleEvalArr.rrArrayHandle = -1;
 rgDiagInArray.rrArrayHandle = -1;
 rgEvtHistArr.rrArrayHandle = -1;
     
 cgSoh[0] = 0x01;                /* BST und SMI wollen das so */
 cgSoh[1] = 0x00;

 cgStx[0] = 0x02;
 cgStx[1] = 0x00;

 cgEtx[0] = 0x03 ;
 cgEtx[1] = 0x00 ;

 cgDc1[0] = 0x11 ;
 cgDc1[1] = 0x00 ;

 dbg (DEBUG, "Init_rulfnd : Name of the modul = <%s>", mod_name) ;

 /* set lgOPFlag [IS_O_VERSION | IS_P_VERSION */ 

 lgOPFlag = IS_O_VERSION ;                        /* default */ 
 igMyOriginator = O_ORGININATOR_ID ; 
 ilHLen = strlen (mod_name) ; 
 if ((mod_name[ilHLen-1] == 'P') || (mod_name[ilHLen-1] == 'p'))
 {
  lgOPFlag = IS_P_VERSION ;        /* P Version -> set low prio */ 
  igMyOriginator = P_ORGININATOR_ID ; 
  /* nice (LOW_PROC_PRIO) ;  */         /* set lower prio */
  dbg (DEBUG, "Init_rulfnd : ident for Process is rulfndp") ;
 }
 else
 {
  /*nice (HIGH_PROC_PRIO) ; */             /* set higher prio */
  dbg (DEBUG, "Init_rulfnd : ident for Process is rulfndo") ;  
  dbg (DEBUG, "Init_rulfnd : --> set higher PRIO = <%d>") ;  
 }

 /* now reading from configfile or from database */
 if (ilRC == RC_SUCCESS)
 {
  ilRC = SetSignals(HandleSignal) ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE, "Init_rulfnd: SetSignals failed <%d>", ilRC) ;
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  sprintf (&clQueueName[0], "%s2", mod_name) ;
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  memset((void*)&cgHopo[0], 0x00, 8);       /* read HomeAirPort from SGS.TAB */
  strcpy(clSection,"SYS");
  strcpy(clKeyword,"HOMEAP");

  ilRC = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
   Terminate(30);
  }
  else
  {
   dbg (TRACE,"Init_rulfnd: home airport <%s>",&cgHopo[0]) ; 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)             /* read table extension from ALL.TABEND */
 {
  strcpy (clSection, "ALL") ;
  strcpy (clKeyword, "TABEND") ;

  ilRC = tool_search_exco_data (&clSection[0], &clKeyword[0], &cgTabEnd[0]) ;

  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE, "Init_rulfnd: EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
   Terminate (30) ;
  }
  else 
  {
   dbg (TRACE,"Init_rulfnd: table extension <%s>",&cgTabEnd[0]) ;
  } /* end of if */
 } /* end of if */

    if(ilRC == RC_SUCCESS)
    {
         GetDebugLevel("STARTUP_MODE", &igStartUpMode);
         GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;

    ilRC = CEDAArrayInitialize(10,2);

    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        iGetConfigEntry( cgConfigFile, "DIAGNOSTICS",
                         "DIAGINTERN", CFG_STRING, clYesOrNo );

        dbg(TRACE,"Internal diagnostics : <%s>",clYesOrNo);

        if (strcmp(clYesOrNo, "Y") == 0)
        {
            bgDiagInt = TRUE;
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        iGetConfigEntry( cgConfigFile, "DIAGNOSTICS",
                         "DIAGEXTERN", CFG_STRING, clYesOrNo );

        dbg(TRACE,"External diagnostics : <%s>",clYesOrNo);

        if (strcmp(clYesOrNo, "Y") == 0)
        {
            bgDiagExt = TRUE;
        }
    }
    
    if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, "MAIN") ;
  strcpy (clKeyword, "add_fields") ;

  ilRC = iGetConfigEntry (cgConfigFile,clSection,clKeyword,CFG_STRING,&cgAddFields[0]) ;
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: invalid configuration : no <%s> in <%s> of <%s>",&clKeyword[0],&clSection[0],&cgConfigFile[0]);
   igInitOK = FALSE ;
  }
  else
  {
   dbg(TRACE, "Init_rulfnd: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],&cgAddFields[0]);   
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, "MAIN") ;
  strcpy (clKeyword, "send_fieldlist_to") ;

  ilRC = iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_STRING, (char *)&cgFieldListDest[0]) ;
  if (ilRC != RC_SUCCESS)
  {
   strcpy (&cgFieldListDest[0], "7800,9000") ;
   dbg(TRACE,"Init_rulfnd: not in configuration - using default : <%s> <%s> <%s>",&clSection[0],&clKeyword[0],&cgFieldListDest[0]);
   ilRC = RC_SUCCESS ;
  }
  else
  {
   dbg (TRACE, "Init_rulfnd: <%s> <%s> <%s>", &clSection[0], &clKeyword[0], &cgFieldListDest[0]) ; 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
    strcpy (clSection, "MAIN") ;
    strcpy (clKeyword, "ignore_FTYP_inbound") ;

    if ( iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_STRING, cgIgnoreFtypIn ) != RC_SUCCESS )
    {
        strcpy (cgIgnoreFtypIn, "DRXN") ;
        dbg(TRACE,"Init_rulfnd: not in configuration - using default : <%s> <%s> <%s>",
            clSection,clKeyword,cgIgnoreFtypIn);
    }
    else
    {
        dbg (TRACE, "Init_rulfnd: <%s> <%s> <%s>", clSection, clKeyword, cgIgnoreFtypIn ) ; 
    } /* end of if */
 } /* end of if */
 if (ilRC == RC_SUCCESS)
 {
    strcpy (clSection, "MAIN") ;
    strcpy (clKeyword, "ignore_FTYP_outbound") ;

    if ( iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_STRING, cgIgnoreFtypOut ) != RC_SUCCESS )
    {
        strcpy (cgIgnoreFtypOut, "DRXN") ;
        dbg(TRACE,"Init_rulfnd: not in configuration - using default : <%s> <%s> <%s>",
            clSection,clKeyword,cgIgnoreFtypOut);
    }
    else
    {
        dbg (TRACE, "Init_rulfnd: <%s> <%s> <%s>", clSection, clKeyword, cgIgnoreFtypOut ) ; 
    } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, mod_name) ;
  strcpy (clKeyword, "save_events") ;

  ilRC = iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_INT, (char*) &ilDummy) ;
  if (ilRC != RC_SUCCESS)
  {
   igSaveEvent = FALSE ;
   dbg(TRACE, "Init_rulfnd: not in configuration - using default : <%s> <%s> <%d>",&clSection[0],&clKeyword[0],igSaveEvent);
   ilRC = RC_SUCCESS ;
  }
  else
  {
   igSaveEvent = ilDummy ;
   dbg(TRACE, "Init_rulfnd: <%s> <%s> <%d>", &clSection[0], &clKeyword[0], igSaveEvent) ; 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, mod_name) ;
  strcpy (clKeyword, "time_filter") ;

  ilRC = iGetConfigEntry (cgConfigFile, clSection, clKeyword, CFG_INT, (char*)&ilDummy) ;
  if (ilRC != RC_SUCCESS)
  {
   lgTimeOffset = (60*60*24*3) ;
   dbg(TRACE, "Init_rulfnd: not in configuration - using default : <%s> <%s> <%d>",&clSection[0],&clKeyword[0],lgTimeOffset);
   ilRC = RC_SUCCESS ;
  }
  else
  {
   lgTimeOffset = ilDummy ;
   dbg (TRACE,"Init_rulfnd: <%s> <%s> <%d>",&clSection[0],&clKeyword[0],lgTimeOffset); 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, mod_name) ;
  strcpy (clKeyword,"send_result_to") ;

  ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
  if (ilRC != RC_SUCCESS)
  {
   igDest = 9000 ;
   dbg(TRACE,"Init_rulfnd: not in configuration - using default : <%s> <%s> <%d>",&clSection[0],&clKeyword[0],igDest);
   ilRC = RC_SUCCESS ;
  }
  else
  {
   igDest = ilDummy ;
   dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",&clSection[0],&clKeyword[0],igDest); 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy (clSection, mod_name) ;
  strcpy (clKeyword,"send_FIR_to") ;

  ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: not in configuration - using default : <%s> <%s> <%d>",&clSection[0],&clKeyword[0],igFIRDest);
   ilRC = RC_SUCCESS ;
  }
  else
  {
   igFIRDest = ilDummy ;
   dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",&clSection[0],&clKeyword[0],igFIRDest); 
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  /* read forward router condition for OP & PL  */
  /* default is do not route to other processes */  
  strcpy (clSection, mod_name) ;
  if (lgOPFlag == IS_O_VERSION)      /* ACTIVE OP MODE */  
  {
   strcpy (clKeyword, "forward_none_op_flight") ;
   ilRC = iGetConfigEntry (cgConfigFile, clSection, clKeyword, CFG_INT, (char *)&ilDummy) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(TRACE, "Init_rulfnd: , [%s] - forward_none_op_flight not found in %s ", mod_name, cgConfigFile) ;
    lgforwardRouting = FORWARD_OP_DEFAULT ;           /* use default */ 
    ilRC = RC_SUCCESS ; 
   } 
   else 
   {
    lgforwardRouting = ilDummy ; 
    dbg(TRACE,"Init_rulfnd: forward_none_op_flight=<%ld>",lgforwardRouting); 
   } /* end of if */
  }
  else                             /* ACTIVE PL MODE */  
  {
   strcpy (clKeyword, "forward_none_pl_flight") ;
   ilRC = iGetConfigEntry (cgConfigFile, clSection, clKeyword, CFG_INT, (char *)&ilDummy) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"Init_rulfnd: , [%s] - forward_none_pl_flight not found in %s ", mod_name, cgConfigFile) ;
    lgforwardRouting = FORWARD_PL_DEFAULT ;           /* use default */ 
    ilRC = RC_SUCCESS ; 
   } 
   else 
   {
    lgforwardRouting = ilDummy ;
    dbg (TRACE,"Init_rulfnd: forward_none_pl_flight=<%ld>",lgforwardRouting) ; 
   } /* end of if */  
  }
 } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        strcpy ( clSection, "PRM") ;
        strcpy ( clKeyword,"RULPRM_MODID");

        ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_rulfnd: <%s> <%s> not in configuration, no send to RULPRM",
                clSection, clKeyword );
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igRulprmModid = ilDummy ;
            dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",clSection, clKeyword, igRulprmModid); 
        } /* end of if */
    }
    
    if (ilRC == RC_SUCCESS)
    {
        strcpy ( clSection, "PRM") ;
        strcpy ( clKeyword,"DEMPRM_MODID");

        ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_rulfnd: <%s> <%s> not in configuration, no send to DEMPRM",
                clSection, clKeyword );
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igDemprmModid = ilDummy ;
            dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",clSection, clKeyword, igDemprmModid); 
        } /* end of if */
    }

 if (ilRC == RC_SUCCESS)
{
    strcpy (clSection, "PRM") ;
    strcpy (clKeyword, "PRMTEMPLATES") ;

    if ( iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_STRING, cgPrmTemplates ) != RC_SUCCESS )
    {
        dbg(TRACE,"Init_rulfnd: No PRM Templates found. Is not a PRM rule finder");
    }
    else
    {
        dbg (TRACE, "Init_rulfnd: <%s> <%s> <%s>", clSection, clKeyword, cgPrmTemplates ) ; 
    } /* end of if */
 }
 {
    char clTmp[24];
    strcpy (clSection, "PRM") ;
    strcpy (clKeyword, "PRMRULEFINDER") ;

    if ( iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_STRING, clTmp ) != RC_SUCCESS )
    {
        dbg(TRACE,"Init_rulfnd: PRMRULEFINDER not found. Is not a PRM rule finder");
    }
    else
    {
        dbg (TRACE, "Init_rulfnd: <%s> <%s> <%s>", clSection, clKeyword, cgPrmTemplates ) ; 
        bgIsPrm = TRUE;
    } /* end of if */
 }

    if (ilRC == RC_SUCCESS)
    {
        strcpy ( clSection, "MAIN") ;
        strcpy ( clKeyword,"op_range") ;

        ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_rulfnd: <%s> <%s> not in configuration, no time frame for rulfndo applied",
                clSection, clKeyword );
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igOpTimeFrame = ilDummy ;
            dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",clSection, clKeyword, igOpTimeFrame); 
        } /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        if ( iGetConfigEntry(cgConfigFile,"MAIN","history_events",CFG_INT,(char *)&ilDummy) == RC_SUCCESS )
        {
            igMaxEvtHistNo = ilDummy;
        }
        if ( iGetConfigEntry(cgConfigFile,"MAIN","history_seconds",CFG_INT,(char *)&ilDummy) == RC_SUCCESS )
        {
            igMaxEvtHistLenght = ilDummy;
        }
        dbg(TRACE,"Init_rulfnd: History max <%d> events of last <%d> seconds",
            igMaxEvtHistNo, igMaxEvtHistLenght );
    } /* end of if */

/**********hag001115*********/
    if (ilRC == RC_SUCCESS)
    {
        strcpy ( clSection, mod_name) ;
        strcpy ( clKeyword,"send_flightchanges_to") ;

        ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            /*igFlightDataDest = 8850 ;*/
            dbg(TRACE,"Init_rulfnd: <%s> <%s> not in configuration, don't forward flight changes",
                clSection, clKeyword );
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igFlightDataDest = ilDummy ;
            dbg (TRACE, "Init_rulfnd: <%s> <%s> <%d>",clSection, clKeyword, igFlightDataDest); 
        } /* end of if */
    } /* end of if */
/**********hag001115*********/

    if (ilRC == RC_SUCCESS)
    {
        strcpy(clSection, mod_name);
        strcpy(clKeyword, "send_flightdeletes_to");

        ilRC = iGetConfigEntry(cgConfigFile, clSection, clKeyword, CFG_INT,
                                (char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE, "Init_rulfnd: <%s> <%s> not in configuration, don't forward flight deletes",
                clSection, clKeyword);
            ilRC = RC_SUCCESS;
        }
        else
        {
            igFlightDelDest = ilDummy;
            dbg(TRACE, "Init_rulfnd: <%s> <%s> <%d>", clSection, clKeyword,
                igFlightDelDest); 
        }
    }

    ilRC = iGetConfigRow ( cgConfigFile, "SYSTEM", "TIME_TRACING", CFG_INT, (char*) &ilDummy) ;
    if ( ilRC == RC_SUCCESS )
    {
        tgMaxStampDiff = ilDummy;
        dbg(TRACE,"Init_rulfnd: Warning if processing of event lasts longer than %d seconds (since flight)",
            tgMaxStampDiff );
    }
    else
    {
        dbg(TRACE,"Init_rulfnd: not found: <SYSTEM>, <TIME_TRACING> in <%s>", cgConfigFile);
        ilRC = RC_SUCCESS;
    }

    /* new parameters for automatic calculation of FID (controlled by ntisch) */
    if ( iGetConfigRow ( cgConfigFile, "FIR", "DURATION", CFG_INT, (char*) &ilDummy) == RC_SUCCESS )
    {
        igFIRDuration = ilDummy;
    }

    if ( iGetConfigRow ( cgConfigFile, "FIR", "DAY_OFFSET", CFG_INT, (char*) &ilDummy) == RC_SUCCESS )
    {
        igFIRDayOffset = ilDummy;
    }
    dbg(TRACE,"Init_rulfnd: FIR calculation with DURATION <%d>, DAY_OFFSET <%d>", igFIRDuration, igFIRDayOffset );

    if ( InitTichTdi() != RC_SUCCESS )
    {
        dbg (TRACE,"Init_rulfnd: InitTichTdi failed, No Timeconversion will be done" ); 
    }

    if (ilRC == RC_SUCCESS)  
    {   /* create a table where all evaluations of sgrtab are stored */                                    
        if ( GetFieldLength("SYS","TANA",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[0] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SYS.TANA failed <%d>",ilRC);
            pllAddFieldLens[0] = 32;
        }

        if ( GetFieldLength("SYS","FINA",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[1] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SYS.FINA failed <%d>",ilRC);
            pllAddFieldLens[1] = 16;
        }

        if ( GetFieldLength("SYS","FELE",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[2] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SYS.FELE failed <%d>",ilRC);
            pllAddFieldLens[2] = 8;
        }
        pllAddFieldLens[3] = 0;

        ilRC = SetAATArrayInfo( "LENEVAL", "TANA,FINA,FELE", pllAddFieldLens, 
                                "IDXLEN", "TANA,FINA", "A,A", &rgFeleEvalArr );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_rulfnd: SetAATArrayInfo for LENEVAL failed <%d>",ilRC);
    }

    if (ilRC == RC_SUCCESS)  
    {   /* create a table where all evaluations of sgrtab are stored */                                    
        if ( GetFieldLength("SGR","HOPO",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[0] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SGR.HOPO failed <%d>",ilRC);
            pllAddFieldLens[0] = 3;
        }

        if ( GetFieldLength("SGR","GRPN",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[1] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SGR.GRPN failed <%d>",ilRC);
            pllAddFieldLens[1] = 32;
        }

        if ( GetFieldLength("SGR","TABN",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[2] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for SGR.TABN failed <%d>",ilRC);
            pllAddFieldLens[2] = 6;
        }

        if ( GetFieldLength("TSR","RFLD",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[3] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_rulfnd: GetFieldLength for TSR.RFLD failed <%d>",ilRC);
            pllAddFieldLens[3] = 10;
        }
        pllAddFieldLens[4] = 20;
        pllAddFieldLens[5] = 1;
        pllAddFieldLens[6] = 0;

        ilRC = SetAATArrayInfo( "SGREVAL", "HOPO,GRPN,TABN,RFLD,VALU,EVAL", pllAddFieldLens, 
                                "IDXEVAL", "HOPO,GRPN,TABN,RFLD,VALU", "A,A,A,A,A,A",
                                &rgSgrEvalArr );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_rulfnd: SetAATArrayInfo for SGREVAL failed <%d>",ilRC);
    }

    if (ilRC == RC_SUCCESS && (bgDiagInt || bgDiagExt))  
    {
        pllAddFieldLens[0] = NUMBLEN;
        pllAddFieldLens[1] = DIAGTEXTLEN;
        pllAddFieldLens[2] = 0;

        ilRC = SetAATArrayInfo( "DIAGTEXT", "NUMB,TEXT", pllAddFieldLens, 
                                "IDXDIA", "NUMB", "A", &rgDiagInArray );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE, "Init_rulfnd: SetAATArrayInfo for DIAGTEXT failed <%d>",
                ilRC);
        else
            ReadDiagIn();
    }


    if (ilRC == RC_SUCCESS)
    {
        dbg(TRACE,"Init_rulfnd: SetArrayInfo TPLTAB start"); 

        strcpy ( cgTplFields, "TPST,HOPO,URNO,TNAM,RELX,FLDA,FLDD,FLDT,USEU,USEC,LSTU,FISU,EVOR,DALO,CDAT,APPL" );
        if ( ( GetFieldLength( "TPL", "FTYA", &llFieldLength ) == RC_SUCCESS ) &&
             ( GetFieldLength( "TPL", "FTYD", &llFieldLength ) == RC_SUCCESS ) )
        {
            strcat ( cgTplFields, ",FTYA,FTYD" );
        }
        else
        {
            dbg ( TRACE,"Init_rulfnd: fields FTYA and/or FTYD of TPLTAB not in DB configuration" );
        }
        FindItemInList(cgTplFields,"FTYA",',',&igTplFTYA,&ilCol,&ilPos);
        FindItemInList(cgTplFields,"FTYD",',',&igTplFTYD,&ilCol,&ilPos);

        ilRC = SetArrayInfo("TPLTAB", cgTplFields, "TPLIDX1", "TPST,HOPO,URNO",
                            "D,D,D", NULL, &rgTplArray) ;

        if (ilRC != RC_SUCCESS)
        {
            dbg (TRACE,"Init_rulfnd: SetArrayInfo TPLTAB failed <%d>", ilRC) ;
        }
        else
        {
            if ( igTestOutput > 4 )
                DebugPrintArrayInfo(TRACE,&rgTplArray) ;          
            dbg(TRACE,"Init_rulfnd: SetArrayInfo TPLTAB OK") ;
        } /* end of if */

  dbg (TRACE,"Init_rulfnd: SetArrayInfo RUETAB start") ;

  ilRC = SetArrayInfo("RUETAB",

/*----------------------------------------------------------------------------*/
/*the next line gets additional fields, while a change exist at date 11.04.00,*/
/* An update of fields, between the executable time, doesnt work correctly,   */
/* without the additional fields     GHe                                      */
/*->RUST,HOPO,UTPL,RUTY,EVTY,EXCL,MAXT,PRIO,FSPL,MIST,EVRM,URNO,RUNA,APPL,FISU*/
/*----------------------------------------------------------------------------*/

                      "RUST,HOPO,UTPL,RUTY,EVTY,EXCL,MAXT,PRIO,FSPL,MIST,EVRM,URNO,RUNA,APPL,FISU,USEU,USEC,UARC,RUSN",
                      "RUEIDX1",
                      "RUST,HOPO,UTPL,RUTY,EVTY,EXCL,PRIO,MAXT",    /*hag20020108 PRF:2556 */
                      "D,D,D,D,A,D,D,A",                            /*hag20020108 PRF:2556 */
                      "WHERE RUST!='2'", &rgRueArray) ;
    if (ilRC != RC_SUCCESS)
    {
     dbg(TRACE,"Init_rulfnd: SetArrayInfo RUETAB failed <%d>", ilRC) ;
    }
    else
    {
        rgRueArray.rrIdx02Handle = -1;
        strcpy ( rgRueArray.crIdx02FieldList,  "RUST,RUTY,URNO") ;
        strcpy( rgRueArray.crIdx02Name, "RUEFIR") ;

        ilRC = CEDAArrayCreateSimpleIndexDown(&(rgRueArray.rrArrayHandle),
                                               rgRueArray.crArrayName,
                                               &(rgRueArray.rrIdx02Handle),
                                               rgRueArray.crIdx02Name,
                                               rgRueArray.crIdx02FieldList);
        if (ilRC != RC_SUCCESS)
        {
            dbg (TRACE, "Init_rulfnd: CEDAArrayCreateSimpleIndex failed <%d>",ilRC);
        }
        else
        {
            dbg (DEBUG, "Init_rulfnd: <%s> created with handle <%d>", 
                 rgRueArray.crIdx02Name, rgRueArray.rrIdx02Handle ) ;
        } /* end of if */

        if ( igTestOutput > 4 )
            DebugPrintArrayInfo(TRACE,&rgRueArray) ;             
        dbg(TRACE,"Init_rulfnd: SetArrayInfo RUETAB OK") ;
    } /* end of if */

  dbg (TRACE,"Init_rulfnd: SetArrayInfo DGRTAB start"); 

  ilRC = SetArrayInfo("DGRTAB","GNAM,HOPO,URNO,UTPL,REFT,EXPR","DGRIDX1",
                               "HOPO,UTPL,GNAM,URNO","D,D,D,D",NULL, &rgDgrArray) ;

  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: SetArrayInfo DGRTAB failed <%d>", ilRC) ; 
  }
  else
  {
   if ( igTestOutput > 4 )
      DebugPrintArrayInfo(TRACE,&rgDgrArray) ; 
   dbg(TRACE,"Init_rulfnd: SetArrayInfo DGRTAB OK") ;
  } /* end of if */

  dbg(TRACE,"Init_rulfnd: SetArrayInfo SGRTAB start") ;
  sprintf ( clSelection, "WHERE APPL='RULE_AFT'" );

  ilRC = SetArrayInfo("SGRTAB","HOPO,GRPN,URNO,TABN,FLDN,APPL","SGRIDX1","HOPO,GRPN,TABN,URNO,FLDN",
                      "D,D,D,D,D",clSelection, &rgSgrArray);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE, "Init_rulfnd: SetArrayInfo SGRTAB failed <%d>", ilRC) ;
  }
  else
  {
    sprintf(clSelection, "(APPL == RULE_AFT)" );
    ilRC = CEDAArraySetFilter(&rgSgrArray.rrArrayHandle,
                              rgSgrArray.crArrayName,clSelection);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Init_rulfnd: CEDAArraySetFilter SGRTAB failed <%d>",ilRC);
    }

   if ( igTestOutput > 4 )
      DebugPrintArrayInfo(TRACE, &rgSgrArray) ;            
   dbg (TRACE,"Init_rulfnd: SetArrayInfo SGRTAB OK") ;  
  } /* end of if */
  /*
  dbg (TRACE,"Init_rulfnd: SetArrayInfo SGMTAB start");

  ilRC = SetArrayInfo("SGMTAB","HOPO,USGR,UVAL,URNO","SGMIDX1","HOPO,USGR,UVAL,URNO","D,D,D,D",NULL, &rgSgmArray);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: SetArrayInfo SGMTAB failed <%d>",ilRC);
  }
  else
  {
   if ( igTestOutput > 4 )
      DebugPrintArrayInfo(TRACE,&rgSgmArray) ;            
   dbg(TRACE,"Init_rulfnd: SetArrayInfo SGMTAB OK") ; 
  }  */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
     strcpy ( cgSelStr, "WHERE appl like 'RULE_%' and uval in (select urno from ruetab where rust!='2')" );
  ilRC = InitValidityModule (CHKVAL_ARRAY, "VALTAB") ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE,"Init_rulfnd: InitValidityModule failed <%d>",ilRC) ;
   ilRC = RC_SUCCESS ;
  }
  else
  {
   dbg (TRACE,"Init_rulfnd: InitValidityModule OK") ;  

   if (ilRC == RC_SUCCESS)
   {
    ilRC = ConfigureAction ("VALTAB",NULL,NULL) ;
    if (ilRC != RC_SUCCESS)
    {
     dbg(TRACE,"Init_rulfnd: ConfigureAction failed <%d>", ilRC) ;
    } /* end of if */
   } /* end of if */
  } /* end of if */
 } /* end of if */

    if ( (ilRC == RC_SUCCESS)  && (lgforwardRouting==1) )
    {   /* create an array where the for the last n calculations the responsible modid is kept */
        pllAddFieldLens[0] = DATELEN;
        pllAddFieldLens[1] = URNOLEN;
        pllAddFieldLens[2] = URNOLEN;
        pllAddFieldLens[3] = URNOLEN;
        pllAddFieldLens[4] = URNOLEN;
        pllAddFieldLens[5] = 0;

        ilRC = SetAATArrayInfo( "EVTHIST", "TIME,OURI,OURO,TOID,NUMB", pllAddFieldLens, 
                                "LAST", "NUMB", "D", &rgEvtHistArr );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_rulfnd: SetAATArrayInfo for SENTTO failed <%d>",ilRC);
    }
    else
        dbg(TRACE,"Init_rulfnd: SetAATArrayInfo for SENTTO not necessary");

    
 if (ilRC == RC_SUCCESS)
 {
  ilRC = CEDAArrayReadAllHeader(outp);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"Init_rulfnd: CEDAArrayReadAllHeader failed <%d>",ilRC);
  }
  else
  {
   dbg(TRACE,"Init_rulfnd: CEDAArrayReadAllHeader OK"); 
  } /* end of if */
 }/* end of if */

 if (ilRC == RC_SUCCESS)
 {
  if (ilRC == RC_SUCCESS)
  {
   ilRC = GetRowLength("RUETAB", "PRIO", &llRowLen) ;
   if (ilRC == RC_SUCCESS)
   {
    llRowLen++ ;
   }
   else
   {
    dbg(TRACE,"Init_rulfnd: GetRowLength failed - ruetab prio <%d>",ilRC) ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   pcgPrio = (char *) calloc(1,llRowLen) ;
   if (pcgPrio == NULL)
   {
    ilRC = RC_FAIL ;
    dbg(TRACE, "Init_rulfnd: calloc failed - pcgPrio") ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   ilRC = ReallocOutBuf() ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "!!!!!!!!!! Init_rulfnd: ReallocOutBuf failed <%d>",ilRC) ;
   } /* end of if */
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  ilRC = SendFieldListToAll(&cgFieldListDest[0]) ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE, "Init_rulfnd: SFLTA failed") ;
  }
  else
  {
   dbg(TRACE, "Init_rulfnd: SFLTA  OK") ;
  } /* end of if */
 } /* end of if */
 if ( strlen (cgClientChars) != strlen(cgServerChars) )
     dbg ( TRACE, "Init_rulfnd: Lengths of cgClientChars and cgServerChars are different !!" );

 if (ilRC == RC_SUCCESS)
 {
  igInitOK = TRUE ;
 } 


    {
        long llMaxDbgLines = 30000;
        int ilMaxDbgFiles = 10;
        char    clCfgValue[264];

        ilRC = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGLINES", 
        CFG_STRING, clCfgValue );
        if ( ilRC == RC_SUCCESS )
            llMaxDbgLines = atol ( clCfgValue );
        else
        {
            dbg(TRACE,"InitDemhdl: not found: <SYSTEM>, <MAXDBGLINES> in <%s>",
            cgConfigFile);
            ilRC = RC_SUCCESS;
        }
        ilRC = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGFILES", CFG_STRING, clCfgValue );
        if ( ilRC == RC_SUCCESS )
            ilMaxDbgFiles = atoi ( clCfgValue );
        else
        {
            dbg(TRACE,"InitDemhdl: not found: <SYSTEM>, <MAXDBGFILES> in <%s>", cgConfigFile);
            ilRC = RC_SUCCESS;
        }
        SetDbgLimits(llMaxDbgLines,ilMaxDbgFiles);
    }
 debug_level = igRuntimeMode; 
 return (ilRC) ;
    
} /* end of initialize */






/******************************************************************************/
static int ReallocOutBuf(void)
{
    int  ilRC     = RC_SUCCESS;
    long llRowLen = 0;
    long llRowCnt = 0;
    long llBuffLen = 0;
                              /* get rowlen from ruetab flds.: URNO,FISU and realloc inb.Urno */
    
    dbg (DEBUG, "ReallocOutBuf: Start") ;
    ilRC = GetRowLength ("RUETAB","URNO,FISU",&llRowLen) ;
    if (ilRC == RC_SUCCESS)
    {
        llRowLen++;
        if ( !pcgInboundUrno )
            pcgInboundUrno = (char *) realloc (pcgInboundUrno,llRowLen) ;
        if (pcgInboundUrno == NULL)
        {
            ilRC = RC_FAIL ;
            dbg (TRACE, "ReallocOutBuf: realloc failed - pcgInboundUrno") ;
        } /* end of if */
        if ( !pcgOutboundUrno )
            pcgOutboundUrno = (char *) realloc (pcgOutboundUrno, llRowLen) ;
        if (pcgOutboundUrno == NULL)
        {
            ilRC = RC_FAIL ;
            dbg (TRACE, "ReallocOutBuf: realloc failed - pcgOutboundUrno") ;
        } /* end of if */
    
        if (ilRC == RC_SUCCESS)
        {
            ilRC = CEDAArrayGetRowCount(&rgTplArray.rrArrayHandle,&rgTplArray.crArrayName[0],&llRowCnt);
            if (ilRC != RC_SUCCESS)
            {
                dbg (TRACE,"ReallocOutBuf: CEDAArrayGetRowCount failed - tpltab <%d>", ilRC) ;
                ilRC = RC_SUCCESS;
            } /* end of if */
            if ( llRowCnt < 1 )
            {
                llRowCnt = 1;
                dbg (TRACE,"ReallocOutBuf: No Template found, create buffer for one template" ) ;
            }
            llBuffLen = (llRowCnt * 2 * llRowLen) ;
        } /* end of if */
    
        if ( (ilRC == RC_SUCCESS) && (llBuffLen > lgUrnoListLen) )
        {
            pcgUrnoListBuf = (char *) realloc(pcgUrnoListBuf, llBuffLen) ;
            if (pcgUrnoListBuf == NULL)
            {
                lgUrnoListLen = 0;
                ilRC = RC_FAIL ;
                dbg (TRACE,"ReallocOutBuf: realloc failed - pcgUrnoListBuf") ;
            }
            else
            {
                lgUrnoListLen = llBuffLen;
                dbg (DEBUG,"ReallocOutBuf: realloc <%d> bytes for - pcgUrnoListBuf", lgUrnoListLen) ;
            } /* end of if */
        } /* end of if */
        else
            dbg ( TRACE,"ReallocOutBuf: nothing reallocated length necessary <%ld> existing <%d>", 
                  llBuffLen, lgUrnoListLen );
    }
    else
    {
        dbg (TRACE, "ReallocOutBuf: GetRowLength failed - ruetab urno, fisu <%d>", ilRC) ;
    } /* end of if */
    
    return (ilRC) ;

} /* end of ReallocOutBuf */



/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo)
{
 int ilLoop = 0;

 if (debug_level == 0) return ;   
    
 if(prpArrayInfo == NULL)
 {
  dbg (ipDebugLevel,"DebugPrintArrayInfo: nothing to print");
  return ;
 } /* end of if */

 dbg (ipDebugLevel,"DebugPrintArrayInfo: rrArrayHandle         <%d>",prpArrayInfo->rrArrayHandle);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: crArrayName           <%s>",&(prpArrayInfo->crArrayName[0]));
 dbg (ipDebugLevel,"DebugPrintArrayInfo: crArrayFieldList      <%s>",&(prpArrayInfo->crArrayFieldList[0]));
 dbg (ipDebugLevel,"DebugPrintArrayInfo: lrArrayFieldCnt       <%d>",prpArrayInfo->lrArrayFieldCnt);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: lrArrayRowLen         <%d>",prpArrayInfo->lrArrayRowLen);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: pcrArrayRowBuf        <%8.8x>",prpArrayInfo->pcrArrayRowBuf);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen      <%8.8x>",prpArrayInfo->plrArrayFieldLen);

 for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
 {
  dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldLen[ilLoop]);
 } /* end of for */

 dbg (ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs      <%8.8x>",prpArrayInfo->plrArrayFieldOfs);

 for (ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
 {
  dbg (ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldOfs[ilLoop]);
 } /* end of for */

 dbg (ipDebugLevel,"DebugPrintArrayInfo: rrIdx01Handle         <%d>",prpArrayInfo->rrIdx01Handle);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: crIdx01Name           <%s>",&(prpArrayInfo->crIdx01Name[0]));
 dbg (ipDebugLevel,"DebugPrintArrayInfo: crIdx01FieldList      <%s>",&(prpArrayInfo->crIdx01FieldList[0]));
 dbg (ipDebugLevel,"DebugPrintArrayInfo: lrIdx01FieldCnt       <%d>",prpArrayInfo->lrIdx01FieldCnt);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: lrIdx01RowLen         <%ld>",prpArrayInfo->lrIdx01RowLen);
 dbg (ipDebugLevel,"DebugPrintArrayInfo: pcrIdx01RowBuf        <%8.8x>",prpArrayInfo->pcrIdx01RowBuf);

 dbg (ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos      <%8.8x>",prpArrayInfo->plrIdx01FieldPos);

 for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
 {
  dbg (ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldPos[ilLoop]);
 } /* end of for */

 dbg (ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd      <%8.8x>",prpArrayInfo->plrIdx01FieldOrd);

 for (ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
 {
  dbg (ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldOrd[ilLoop]);
 } /* end of for */

} /* end of DebugPrintArrayInfo */



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
 int   ilRC = RC_SUCCESS ;    /* Return code */

 dbg (TRACE, "Reset: now reset process") ;
 return  ilRC ;

} /* end of Reset */



/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
 int ilRC = RC_SUCCESS ;

 /* unset SIGCHLD ! DB-Child will terminate ! */

 ilRC = UnsetSignals() ;
 if(ilRC != RC_SUCCESS)
 {
  dbg(TRACE, "Terminate: UnsetSignals failed <%d>", ilRC) ;
 } /* end of if */

 dbg(TRACE, "Terminate: now DB logoff ...") ;
 signal (SIGCHLD, SIG_IGN) ;
 logoff() ;

 dbg(TRACE, "Terminate: now sleep(%d) ...", ipSleep) ;
 sleep(ipSleep) ;
 dbg(TRACE, "Terminate: now leaving ...") ;
    
   if (pfgDiagIntOut)
        fclose(pfgDiagIntOut);
   if (pfgDiagExtOut)
        fclose(pfgDiagExtOut);
 exit(0) ;
    
} /* end of Terminate */



/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
 dbg (TRACE, "HandleSignal: signal <%d> received", pipSig) ;

 switch (pipSig)
 {
  default   : Terminate (1) ;
          break ;
 } /* end of switch */

   if (pfgDiagExtOut)
        fclose(pfgDiagExtOut);
 exit(1) ;
    
} /* end of HandleSignal */



/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        Terminate(1);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */





/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int   ilRC       = RC_SUCCESS;        /* Return code */
  int   ilBreakOut = FALSE;

  if ( debug_level != 0 ) 
  { 
   dbg (TRACE, "HandleQueues: waiting for status switch") ;
   DebugPrintItem (TRACE,prgItem) ;
   DebugPrintEvent (TRACE,prgEvent) ;
  }

  do
  {
   ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_4, igItemLen, (char *)&prgItem) ;

   /* depending on the size of the received item  */
   /* a realloc could be made by the que function */
   /* so do never forget to set event pointer !!! */

   prgEvent = (EVENT *) prgItem->text ;

   if( ilRC == RC_SUCCESS )
   {                                           /* Acknowledge the item */
    ilRC = que(QUE_ACK,0,mod_id,0,0,NULL) ;
    if ( ilRC != RC_SUCCESS ) 
    {                                          /* handle que_ack error */
     HandleQueErr (ilRC) ;
    } /* fi */

    switch( prgEvent->command )
    {
     case HSB_STANDBY     :
                            dbg(TRACE,"HandleQueues: HSB_STANDBY") ;  
                            ctrl_sta = prgEvent->command ;
                          break ;   

     case HSB_COMING_UP   :
                            dbg(TRACE,"HandleQueues: HSB_COMING_UP") ; 
                            ctrl_sta = prgEvent->command ;
                          break ;   
    
     case HSB_ACTIVE      :
                            dbg(TRACE, "HandleQueues: HSB_ACTIVE") ; 
                            ctrl_sta = prgEvent->command ;
                            ilBreakOut = TRUE;
                          break;    

      case HSB_ACT_TO_SBY :
                            dbg(TRACE, "HandleQueues: HSB_ACT_TO_SBY") ; 
                            ctrl_sta = prgEvent->command;
                          break ;   
    
      case HSB_DOWN       :
                            dbg(TRACE, "HandleQueues: HSB_DOWN") ; 
                            /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                            ctrl_sta = prgEvent->command ;
                            Terminate(1) ;
                          break;    

      case HSB_STANDALONE :
                            dbg(TRACE, "HandleQueues: HSB_STANDALONE") ; 
                            ctrl_sta = prgEvent->command ;
                            ResetDBCounter() ;
                            ilBreakOut = TRUE ;
                          break;    

      case REMOTE_DB      : /* ctrl_sta is checked inside */
                            HandleRemoteDB(prgEvent) ;
                          break ;

      case SHUTDOWN       :
                            dbg(TRACE, "HandleQueues: SHUTDOWN") ;
                            Terminate(1) ;
                          break;
                        
      case RESET          :
                            ilRC = Reset() ;
                          break;
                        
      case EVENT_DATA     :
                            dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                            DebugPrintItem(TRACE,prgItem);
                            DebugPrintEvent(TRACE,prgEvent);
                          break;

      case TRACE_ON       :
                            dbg_handle_debug(prgEvent->command);
                          break;

      case TRACE_OFF      :
                            dbg_handle_debug(prgEvent->command);
                          break;

      default             :
                            dbg(TRACE,"HandleQueues: unknown event");
                            DebugPrintItem(TRACE,prgItem);
                            DebugPrintEvent(TRACE,prgEvent);
                          break;
    } /* end switch */
   } 
   else 
   {         /* Handle queuing errors */
    HandleQueErr(ilRC) ;
   } /* end else */
  } while (ilBreakOut == FALSE) ;

  if(igInitOK == FALSE)
  {
   ilRC = Init_rulfnd() ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"HandleQueues: init failed!") ;
   } /* end of if */
  } /* end of if */

} /* end of HandleQueues */
    




/******************************************************************************/
/* using Lex and Yacc                                                         */
/******************************************************************************/
static int CheckCondition(char *pcpCondition)
{
    int ilRC;            /* Return code */
    int ilRes;
    char *pclCondCopy=0, *pclOutput;

    ilRes = RC_SUCCESS;

    if ( !pcpCondition )
    {
        dbg(TRACE, "CheckCondition: Invalid parameter <null>");
        return RC_INVALID;

    }

    pclOutput = pcpCondition;
    if ( pclCondCopy = (char*)malloc ( strlen(pcpCondition)+1 ) )
    {
        strcpy ( pclCondCopy, pcpCondition );
        ReplaceChar ( pclCondCopy, '\n', ' ');
        ReplaceChar ( pclCondCopy, '\r', ' ');
        TrimRight ( pclCondCopy );
        pclOutput = pclCondCopy;
    }
    if ( debug_level != 0 ) 
    {
        dbg(DEBUG, "CC: <%s>", pclOutput);
        dbg(DEBUG, "CC <-> RmsCheckRule");
    }
    ilRC = RmsCheckRule(pcpCondition, &ilRes);

    if (ilRC == RC_SUCCESS)
    {
        if(ilRes == RC_SUCCESS)
        {
            if ( debug_level != 0 ) 
            {
                dbg(DEBUG, "CC: <%s> is true", pclOutput); 
            }
        } 
        else
        {
            dbg(DEBUG, "CC: <%s> is false", pclOutput);
            ilRC = RC_FAIL; 
            if (bgDiagOn)
            {
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGCONDFALSE, pclOutput, NULL,
                                    NULL);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGCONDFALSE, pclOutput, NULL,
                                    NULL);
            }
        } /* end of if */
    } 
    else 
    {
        dbg(DEBUG, "CC: Syntax-Error in check rule <%s>", pclOutput);
        ilRC = RC_FAIL;
        if (bgDiagOn)
        {
            if (bgDiagInt)
                WriteDiagFile(DIAGINT, DIAGCONDINCORR, pclOutput, NULL,
                                NULL);
            if (bgDiagExt)
                WriteDiagFile(DIAGEXT, DIAGCONDINCORR, pclOutput, NULL,
                                NULL);
        }
    } /* end of if */   
    if ( pclCondCopy )
        free ( pclCondCopy );
    return ilRC;
    
} /* end of CheckCondition */





/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
 int  ilRC        = RC_SUCCESS;    /* Return code */
 int  ilNoOfItems = 0;
 int  ilLoop      = 0;
 long llFldLen    = 0;
 long llRowLen    = 0;
 char clFina[8];
    
 ilNoOfItems = get_no_of_items(pcpFieldList);

 ilLoop = 1 ;
 do
 {
  ilRC = get_real_item (&clFina[0], pcpFieldList, ilLoop) ;
  if(ilRC > 0)
  {
   ilRC = GetFieldLength (pcpTana, &clFina[0], &llFldLen) ;
   if (ilRC == RC_SUCCESS)
   {
    llRowLen++ ;
    llRowLen += llFldLen ;
   } /* end of if */
  } /* end of if */
  ilLoop++;
 } while ((ilRC == RC_SUCCESS) && (ilLoop <= ilNoOfItems)) ;

 if (ilRC == RC_SUCCESS)
 {
  *plpLen = llRowLen ;
 } /* end of if */

 return (ilRC) ;
    
} /* end of GetRowLength */



/*******************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
 int  ilRC  = RC_FAIL;     /* Return code */
 int  ilCnt = 0 ;
 char clTaFi[64] ;
 char clFele[32] ;
 char clFldLst[32] ;

 ilRC = FindFeleEvaluation ( pcpTana, pcpFina, clFele );
 if ( ( ilRC == RC_SUCCESS ) && ( sscanf(clFele, "%ld", plpLen ) >= 1 ) )
 {
    dbg (DEBUG, "GetFieldLength: from Array <%s,%s> <%ld>", pcpTana, pcpFina, *plpLen) ;
    return ilRC;
 }
 ilCnt = 1 ;
 sprintf (&clTaFi[0], "%s,%s", pcpTana, pcpFina) ;
 strcpy ( clFldLst, "TANA,FINA") ; /* wird von syslib zerstoert - konstante nicht moeglich */

 ilRC = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"") ;
 switch (ilRC)
 {
  case RC_SUCCESS :
        SaveFeleEvaluation ( pcpTana, pcpFina, clFele );
        *plpLen = atoi (&clFele[0]) ;
        if ( igTestOutput > 3 ) 
            dbg (DEBUG, "GetFieldLength: <%s,%s> <%d>", pcpTana, pcpFina, *plpLen) ;
      break ;

  case RC_NOT_FOUND :

        dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>", ilRC, &clTaFi[0]) ;
      break ;

  case RC_FAIL :

        dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_FAIL", ilRC) ;
      break ;

  default :

        dbg(TRACE, "GetFieldLength: syslibSSD returned <%d> unexpected", ilRC) ;
      break ;

 } /* end of switch */

 return (ilRC) ;
    
} /* end of GetFieldLength */





/*************************************************************************/
/* additional version from GH                                            */  
/*************************************************************************/ 
static int ProcessConditions (char *pcpHopo, char *pcpRuleCDs, long *plpIsTrue )
{
    int    ilRC  = RC_SUCCESS;                   /* Return code */
    int    ilMoreRecords; 
    int    ilErrCondition; 
    int    ilFoundRules; 
    long   llCondCnt  = 0;
    long   ilZli      = 0; 
    char  *pclCond    = NULL;
    char  *pclValue   = NULL;
    char   clCondition [CONDITION_SIZE];             /* sizeof(evrm) */
    char   info [5000]; 

    ilFoundRules  = 0; 
    pclCond = pcpRuleCDs;
    
    UndoCharMapping ( pclCond );    /* hag 010809 */

    llCondCnt = 0;
    *plpIsTrue = TRUE;

    if (debug_level != 0) 
    {
        strcpy(info, pcpRuleCDs);  
        TrimRight(info);
        dbg(DEBUG, "PC: ev <%s>", info ); 
    }

    ilMoreRecords        = 0; 
    ilErrCondition       = 0;  

    do 
    {               /* read the complete condition -> one STX condition */ 
        ilRC = GetNextDataItem(&clCondition[0], &pclCond, &cgStx[0], "", "  ");
        ilFoundRules = 0; 
        if(ilRC > 0)             /* > 0 --> char read counter */ 
        {
            dbg(DEBUG, "PC: len=<%d> item=<%s>", ilRC, &clCondition[0]);
            ilZli++;
            /*ilRC = RC_SUCCESS;*/
            llCondCnt++;

            pclValue = &clCondition[0];
            ilRC = GetNextDataItem(&rgCondition.crTable[0], &pclValue,
                                     &cgSoh[0], "", "\0\0");
            if(ilRC > 0)
            {
                ilRC = RC_SUCCESS;
            }
            else
            {
                dbg (DEBUG, "PC: ERR -> table");
                ilRC = RC_FAIL;
            } /* end of if */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */ 
            if (ilRC == RC_SUCCESS)   /* READ FIELD NAME OF ACT. CONDITION */
            {
                ilRC = GetNextDataItem(&rgCondition.crField[0], &pclValue,
                                        &cgSoh[0], "", "\0\0");
                if(ilRC > 0)
                {
                    ilRC = RC_SUCCESS;
                    dbg (DEBUG, "PC: table <%s> field <%s>", rgCondition.crTable, rgCondition.crField);
                }
                else
                {
                    dbg (TRACE, "PC: ERR -> field");
                    ilRC = RC_FAIL;
                } /* end of if */
            } /* end of if */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */ 
            if (ilRC == RC_SUCCESS) /* get CF cond. In-Outbound, turnaround */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crIotf[0], &pclValue,
                                        &cgSoh[0], "", "\0\0"); 
                if (ilRC > 0) 
                {
                    ilRC = RC_SUCCESS;

                    switch (rgCondition.crIotf[0])
                    {
                        case '0' :
                            rgCondition.lrIotf = EVT_TURNAROUND;
                            dbg(DEBUG, "PC: CF = <0> EVT_TURNAROUND");  
                            break;

                        case '1' :
                            rgCondition.lrIotf = EVT_INBOUND ;
                            dbg(DEBUG, "PC: CF = <1> EVT_INBOUND");     
                            break;

                        case '2' :
                            rgCondition.lrIotf = EVT_OUTBOUND;
                            dbg(DEBUG, "PC: CF = <2> EVT_OUTBOUND");    
                            break;

                        case '3' :
                            rgCondition.lrIotf = EVT_FLIGHT_INDEPENDENT;
                            igEventType = EVT_FLIGHT_INDEPENDENT; 
                            dbg(DEBUG, "PC: CF = <2> EVT_FLIGHT_INDEPENDENT");
                            break;

                        default  :
                            dbg(TRACE, "PC: unknown condition type <%c>",
                                rgCondition.crIotf[0]);
                            ilRC = RC_FAIL;
                    } /* end of switch */
                }
                else
                {
                    dbg(TRACE, "PC: ERR -> CF");
                    ilRC = RC_FAIL;
                } /* end of if */
            } /* end of RC_SUCCESS */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */ 
            if(ilRC == RC_SUCCESS)      /* get CT cond. simple, dyn. stat. */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crType[0], &pclValue,
                                        &cgSoh[0], "", "\0\0"); 
                if(ilRC > 0) 
                {
                    ilRC = RC_SUCCESS;

                    switch (rgCondition.crType[0])
                    {
                        case CDT_ASC_NULL   :  /* no rule */
                            rgCondition.lrType = CDT_NULL;
                            dbg(DEBUG, "PC: CT = <%c> NO RULE Cond.",
                                rgCondition.crType[0]);
                            break; 

                        case CDT_ASC_SIMPLE :
                            rgCondition.lrType = CDT_SIMPLE;  
                            dbg(DEBUG, "PC: CT = <%c> CDT_SIMPLE",
                                rgCondition.crType[0]);
                            break;
 
                        case CDT_ASC_DYNAMIC :
                            rgCondition.lrType = CDT_DYNAMIC;
                            dbg(DEBUG, "PC: CT = <%c> CDT_DYNAMIC",
                                rgCondition.crType[0]);
                            break;

                        case CDT_ASC_STATIC  :
                            rgCondition.lrType = CDT_STATIC;  
                            dbg(DEBUG, "PC: CT = <%c> CDT_STATIC",
                                rgCondition.crType[0]);
                            break;

                        default  :
                            dbg(TRACE, "PC: unknown condition type <%c>",
                                rgCondition.crType[0]);
                            ilRC = RC_FAIL;
                    } /* end of switch */

                }
                else
                {
                    dbg(TRACE, "PC: ERR -> CT" );
                    ilRC = RC_FAIL;
                } /* end of if */
            } /* end of RC_SUCCESS */ 

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
            if (ilRC == RC_SUCCESS)     /* get OP type = , >=, =<, < , ... */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crOp[0], &pclValue,
                                        &cgSoh[0], "", "\0\0"); 
                if(ilRC > 0)
                {
                    ilRC = RC_SUCCESS;
                }
                else
                {
                    ilRC = RC_FAIL;
                    dbg(TRACE, "PC: ERR -> OP type");
                } /* end of if */
            } /* end of if */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */ 
            if (ilRC == RC_SUCCESS)       /* get value from act. condition */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crValue[0], &pclValue,
                                        &cgSoh[0], "", "\0\0");
                if(ilRC > 0)
                {
                    ilRC = RC_SUCCESS;
                    dbg(DEBUG, "PC: OP <%s>  value <%s>", rgCondition.crOp, 
                                                          rgCondition.crValue);
                }
                else
                {
                    ilRC = RC_FAIL;
                    dbg(TRACE, "PC: ERR -> value");
                } /* end of if */
            } /* end of if */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
            if (ilRC == RC_SUCCESS)            /* conditon reference table */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crRefTab[0], &pclValue,
                                        &cgSoh[0], "", "\0\0");
                if (ilRC > 0)
                {
                    ilRC = RC_SUCCESS;
                }
                else
                {
                    dbg(TRACE, "PC: ERR -> Ref Tab");
                    ilRC = RC_FAIL;
                } /* end of if */
            } /* end of if */


   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
            if (ilRC == RC_SUCCESS)           /* condition refenence field */ 
            {
                ilRC = GetNextDataItem(&rgCondition.crRefFld[0], &pclValue,
                                        &cgSoh[0], "", "\0\0");
                if (ilRC > 0)
                {
                    ilRC = RC_SUCCESS;
                    dbg(DEBUG, "PC: RefTab <%s> RefFld <%s>", rgCondition.crRefTab, 
                        rgCondition.crRefFld);
                }
                else
                {
                    dbg(TRACE, "PC: ERR -> Ref Fld");
                    ilRC = RC_FAIL;
                } /* end of if */
            } /* end of if */
            if (ilRC !=RC_SUCCESS)
            {
                if (bgDiagOn)
                {  
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGCONDINVALID, NULL, NULL,
                                        NULL);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGCONDINVALID, NULL, NULL,
                                        NULL);
                }
            }

   /* -------------------------------------------------------- */ 
   /* interpret the rgCondition.lrIotf and set the bool values */ 
   /* lrChkInbound and lrChkOutbound to TRUE OR FALSE          */ 
   /* -------------------------------------------------------- */ 
            if (ilRC == RC_SUCCESS)
            {
                dbg(DEBUG, "PC: evt type=<%d> | iotf=<%d>", igEventType,
                    rgCondition.lrIotf);
                rgCondition.lrChkInbound = FALSE;      /* set as default */ 
                rgCondition.lrChkOutbound = FALSE;              
                switch (igEventType)
                {
                    case EVT_TURNAROUND :
                        switch(rgCondition.lrIotf)
                        {
                            case EVT_TURNAROUND :
                                rgCondition.lrChkInbound  = TRUE;
                                rgCondition.lrChkOutbound = TRUE;
                                break;

                            case EVT_INBOUND    :
                                rgCondition.lrChkInbound  = TRUE;
                                break;

                            case EVT_OUTBOUND   :
                                rgCondition.lrChkOutbound = TRUE;
                                break;

                            default :
                                dbg(TRACE, "PC: unknown condition type <%d>",
                                    rgCondition.lrIotf); 
                                ilRC = RC_FAIL;
                                break; 

                        } /* end of switch */           
                        break;

                    case EVT_INBOUND :
                        switch(rgCondition.lrIotf)
                        {
                            case EVT_TURNAROUND :
                                rgCondition.lrChkInbound  = TRUE;
                                break;

                            case EVT_INBOUND    :
                                rgCondition.lrChkInbound  = TRUE;
                                break;

                            case EVT_OUTBOUND   :
                                dbg(TRACE,
                                    "PC: invalid combination evt_type <%d> <%d> cdt_iotf", igEventType,rgCondition.lrIotf);
                                ilRC = RC_FAIL; 
                                if (bgDiagOn)
                                {
                                    if (bgDiagInt)
                                        WriteDiagFile(DIAGINT, DIAGTYPEFALSE,
                                                        (char *)igEventType,
                                                        (char *)
                                                            rgCondition.lrIotf,
                                                        NULL);
                                    if (bgDiagExt)
                                        WriteDiagFile(DIAGEXT, DIAGTYPEFALSE,
                                                        (char *)igEventType,
                                                        (char *)
                                                            rgCondition.lrIotf,
                                                        NULL);
                                }
                                break;

                            default :
                                dbg(TRACE, "PC: unknown condition type <%d>",
                                    rgCondition.lrIotf);
                                ilRC = RC_FAIL;
                        } /* end of switch */
                        break;

                    case EVT_OUTBOUND :
                        switch(rgCondition.lrIotf)
                        {
                            case EVT_TURNAROUND :
                                rgCondition.lrChkOutbound = TRUE;
                                break ;

                            case EVT_INBOUND :
                                dbg(TRACE,
                                    "PC: invalid combination evt_type=<%d> | iotf=<%d>", igEventType, rgCondition.lrIotf);
                                ilRC = RC_FAIL;
                                if (bgDiagOn)
                                {
                                    if (bgDiagInt)
                                        WriteDiagFile(DIAGINT, DIAGTYPEFALSE,
                                                        (char *)igEventType,
                                                        (char *)
                                                            rgCondition.lrIotf,
                                                        NULL);
                                    if (bgDiagExt)
                                        WriteDiagFile(DIAGEXT, DIAGTYPEFALSE,
                                                        (char *)igEventType,
                                                        (char *)
                                                            rgCondition.lrIotf,
                                                        NULL);
                                }
                                break;

                            case EVT_OUTBOUND :
                                rgCondition.lrChkOutbound = TRUE;
                                break;
                
                            default :
                                dbg(TRACE, "PC: unknown condition type <%d>",
                                    rgCondition.lrIotf);
                                ilRC = RC_FAIL;
                        } /* end of switch */

                        break;

                    case EVT_FLIGHT_INDEPENDENT :
                        rgCondition.lrChkOutbound = FALSE;
                        rgCondition.lrChkInbound  = TRUE; 
                                             /* use inbound for FI_TYPE=3 */ 
                        break; 

                    default :
                        dbg(TRACE, "PC: unknown event type <%d>", igEventType);
                        ilRC = RC_FAIL;
                } /* end of switch */
            }   /* ilRC == RC_SUCCESS */
            if ( ilRC != RC_SUCCESS )
            {   /* syntax error etc. detected */
                *plpIsTrue = FALSE;
            }
        } /* end of if */
        else /* ilRC == 0  ! NO MORE CONDITION FOUND  */  
        {
            ilMoreRecords = 1;
            ilRC = RC_NOTFOUND;

   /* leave the PC Conditions ----------- !!!!! */ 
            if (llCondCnt == 0)   /*---------------------*/ 
            {
                dbg(DEBUG, 
                    "PC: rule without condition ??? -> plpIsTrue = TRUE");
                *plpIsTrue = TRUE;
                ilRC = RC_SUCCESS; 
            } /* end of if */
        }

        if ( (ilRC == RC_SUCCESS) && llCondCnt )
        {
            if (debug_level != 0) 
            {
                dbg(DEBUG, "PC: lrChkInbound <%ld>  lrChkOutbound <%d>", 
                    rgCondition.lrChkInbound, rgCondition.lrChkOutbound );
            }
        } /* if (RC_SUCCESS) */ 

  /* -------------------------------------------------------- */
  /* call the function, that works with the actally condition */
  /* -------------------------------------------------------- */
        if ( (ilRC == RC_SUCCESS) && llCondCnt )
        {
            switch(rgCondition.lrType)
            {
                case CDT_NULL    :
                    *plpIsTrue = FALSE; 
                    ilRC = RC_SUCCESS; 
                    break;

                case CDT_SIMPLE  : 
                    ilRC = CheckCDT0(pcpHopo, rgCondition, pcgSearchTable,
                                     plpIsTrue);
                    if (*plpIsTrue == TRUE) 
                    {
                        if ( ilFoundRules > 0) 
                        {
                            dbg(TRACE, "PC: CONFLICT : FOUND MORE RULES !");
                         /* CALL CONFLICT HANDLER !!!! */ 
                        }
                        ilFoundRules++;
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg(TRACE, "PC: CheckCDT0 failed <%d>", ilRC);
                    } /* end of if */
                    break;

                case CDT_DYNAMIC : 
                    ilRC = CheckCDT1(pcgUtpl, pcpHopo, rgCondition, plpIsTrue);
                    if (*plpIsTrue == TRUE) 
                    {
                        if ( ilFoundRules > 0) 
                        {
                            dbg(TRACE, "PC: CONFLICT : FOUND MORE RULES !");
                         /* CALL CONFLICT HANDLER !!!! */ 
                        }
                        ilFoundRules++;
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS;
                        dbg(DEBUG, "PC: CheckCDT1 failed <%d>", ilRC);
                    } /* end of if */ 
                    break;

                case CDT_STATIC  : 
                    ilRC = CheckCDT2(pcpHopo, rgCondition, plpIsTrue);
                    if (*plpIsTrue == TRUE) 
                    {
                        if ( ilFoundRules > 0) 
                        {
                            dbg(TRACE,"PC: CONFLICT : FOUND MORE RULES !");
                   /* CALL CONFLICT HANDLER !!!! */ 
                        }
                        ilFoundRules++; 
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg(TRACE, "PC: CheckCDT2 failed <%d>", ilRC);
                    } /* end of if */
                    break;

                default :
                    dbg(TRACE, "PC: unknown condition type <%s>",
                        &rgCondition.crType[0]);
                    ilRC = RC_FAIL;
            } /* end of switch */
        } /* if RC_SUCCESS */ 
    } while ((ilErrCondition == 0) && (ilMoreRecords == 0)
                                    && (*plpIsTrue == TRUE)); 

 /* ---- end of the loop ----  READ STX RECORDS ---- */ 

    if ( ilErrCondition != 0 )
    { 
  /* err detected  -> dump file, etc... */ 
        dbg(TRACE, "PC: read CF FROM FIRST = ilErrCond=%d", ilErrCondition);
    }

    if (llCondCnt == 0)
    {
        dbg(DEBUG, "PC: rule without condition ??? -> plpIsTrue = TRUE");
        *plpIsTrue = TRUE;
        ilRC = RC_SUCCESS;
    } /* end of if */

    if (ilRC == RC_FAIL)
    {
        dbg(DEBUG, "PC: rc_fail ??? -> plpIsTrue = FALSE");
        *plpIsTrue = FALSE;
    } /* end of if */

    if (ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS;
    }

    return (ilRC);
    
} /* end of ProcessConditions () */



/**************************************************************************/
/* check simple rule conditions                                           */
/**************************************************************************/
static int CheckCDT0(char *pcpHopo, CONDINFO psCondition, char *pcpTable,
                    long *plpIsTrue)
{
    int   ilRC;        /* Return code */
    static char  *pclFieldData = NULL;
    static long  llDataLen= 0;
    char    clTabField[32];

    *plpIsTrue = TRUE;
    ilRC = RC_SUCCESS; 

    sprintf(clTabField, "%s.%s", psCondition.crTable, psCondition.crField);

    if (psCondition.lrIotf == EVT_FLIGHT_INDEPENDENT) 
    {
        if (psCondition.lrChkInbound == TRUE)
        {
            ilRC = GetFieldData2(pcpTable, psCondition.crField, pcgFieldList,
                                pcgInbound, &pclFieldData, &llDataLen );

            if (ilRC == RC_SUCCESS)
            {
                 TrimRight ( pclFieldData ) ;  /*hag010418: fix bug reported by IPR */
                if (( pclFieldData[0] != ' ' ) &&
                    (strcmp(pclFieldData, psCondition.crValue) == 0)) 
                {
                    *plpIsTrue = TRUE;
                }
                else 
                {
                    *plpIsTrue = FALSE;
                }
                dbg(DEBUG, "CDT0: FLI_Indep. evt=<%s> cdt=<%s> eval <%ld>", pclFieldData,
                    psCondition.crValue, *plpIsTrue );
            } /* end of if (ilRC == RC_SUCCESS)              */
        } /* end of if (rgCondition.lrChkInbound == TRUE) */ 
    }
    else    /* not a flight independence condition */  
    {
        if (psCondition.lrChkInbound == TRUE)
        {
            ilRC = GetFieldData2(pcpTable, psCondition.crField, pcgFieldList,
                                pcgInbound, &pclFieldData, &llDataLen );

            if (ilRC == RC_SUCCESS)
            {
                TrimRight ( pclFieldData ) ;  /*hag010418: fix bug reported by IPR */
                if ((pclFieldData[0] != ' ') &&
                    (strcmp(pclFieldData, psCondition.crValue) == 0))
                {
                    *plpIsTrue = TRUE;
                }
                else 
                {
                    *plpIsTrue = FALSE;
                    if (bgDiagOn)
                    {
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGCONDUNEQUAL, clTabField,
                                            pclFieldData, psCondition.crValue);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGCONDUNEQUAL, clTabField,
                                            pclFieldData, psCondition.crValue);
                    }
                }
                dbg(DEBUG, "CDT0: inbound evt=<%s> cdt=<%s> eval <%ld>", pclFieldData,
                    psCondition.crValue, *plpIsTrue );

            }/* end of if (ilRC == RC_SUCCESS)              */
        }/* end of if (rgCondition.lrChkInbound == TRUE) */

        if (ilRC == RC_SUCCESS)
        {
            if (*plpIsTrue == TRUE)
            {
                if (rgCondition.lrChkOutbound == TRUE)
                {
                    ilRC = GetFieldData2(pcpTable, psCondition.crField,
                                        pcgFieldList, pcgOutbound,
                                        &pclFieldData, &llDataLen );
                    if (ilRC == RC_SUCCESS)
                    {
                        TrimRight ( pclFieldData ) ;  /*hag010418: fix bug reported by IPR */   
                        if ((pclFieldData[0] != ' ') &&
                            (strcmp(pclFieldData, psCondition.crValue) == 0)) 
                        {
                            *plpIsTrue = TRUE;
                        }
                        else 
                        {
                            *plpIsTrue = FALSE;
                            if (bgDiagOn)
                            {
                                if (bgDiagInt)
                                    WriteDiagFile(DIAGINT, DIAGCONDUNEQUAL,
                                                    clTabField, pclFieldData,
                                                    psCondition.crValue);
                                if (bgDiagExt)
                                    WriteDiagFile(DIAGEXT, DIAGCONDUNEQUAL,
                                                    clTabField, pclFieldData,
                                                    psCondition.crValue);
                            }
                        }
                    } /* end of if GetFieldData2 () == RC_SUCCESS */
                    dbg(DEBUG, "CDT0: outbound evt=<%s> cdt=<%s> eval <%ld>",
                            pclFieldData, psCondition.crValue, *plpIsTrue );

                } /* end of if psCondition.lrChkOutbound */
            } /* end of if (*plpIsTrue == TRUE) */
        } /* end of if RC_SUCCESS */ 
    } /* if lg_event type not FLIGHT INDEPENDENT */ 
    /*
    if (pclFieldData != NULL)
    {
        free (pclFieldData);
        pclFieldData = NULL;
    }  */

    return (ilRC);

} /* end of CheckCDT0 */







/******************************************************************************/
/* Additional Check version for dynamic datarecords  GH                       */
/* at the end of this source a add. function from CheckCDT1(a) exist          */ 
/******************************************************************************/
static int CheckCDT1(char *pcpUtpl, char *pcpHopo, CONDINFO  psCondition, long* plpIsTrue) 
{
    int     ilRC       = RC_SUCCESS ;
    long    llFunc     = ARR_FIRST ;
    char   *pclResult  = NULL ;  /* READ ONLY !!! */
    char   *pclExpr    = NULL ;  /* READ ONLY */  
    static char *pclInbValu = NULL ;
    static char   *pclOutValu = NULL ;
    static long   llInValLen = 0;
    static long   llOutValLen = 0;

    short   slCursor ;
    char    clSqlbuf[1024] ;
    char    clDataArea[2048] ;
    short   slSqlFkt = START | REL_CURSOR ;
    char    cpcmpStr[2048] ; 
    char    pclTabFldVal[2048] ="" ;  
    char    pclTableName[64] ; 
    char    pclFieldsName[1024] ; 
    long    ilFldCnt, lcondLen ; 
    static char   *slCondition = 0 ;
    static long     llLastCondLen = 0;
    int     ilLocalIotf = EVT_UNKNOWN;  /* hag04.11.2003, PRF5389 */

    ilRC        = RC_SUCCESS ;
    *plpIsTrue  = FALSE ; 
    slCursor    = 0 ;

    dbg(DEBUG,"CDT1: table <%s> field <%s>", psCondition.crTable,
        psCondition.crField);

    if (((igEventType == EVT_TURNAROUND) || (igEventType == EVT_INBOUND)) &&
        ((psCondition.lrIotf == EVT_INBOUND) ||
            (psCondition.lrIotf == EVT_TURNAROUND)))
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                            pcgFieldList, pcgInbound, &pclInbValu, &llInValLen );  
        if (ilRC == RC_SUCCESS)
        {
            ilLocalIotf = EVT_INBOUND;  /* hag04.11.2003, PRF5389 */
            strcpy(pclTabFldVal, pclInbValu);
            if (igTestOutput > 2)
            {
                dbg(DEBUG, "CDT1: inbound field <%s> <%s> found",
                    psCondition.crField, pclInbValu);
            }
        }
        else
        { 
            if (igTestOutput > 2)
            {
                dbg(DEBUG, "CDT1: inbound field <%s> not found - inbound",
                    psCondition.crField);
            }
        } /* end of if GetFieldData2 () == RC_SUCCESS */
    } /* end of if igEventType == EVT_....Turnaround / Inbound  */


    if (((igEventType == EVT_TURNAROUND) || (igEventType == EVT_OUTBOUND)) &&
        ((psCondition.lrIotf == EVT_OUTBOUND) ||
            (psCondition.lrIotf == EVT_TURNAROUND)))
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                            pcgFieldList, pcgOutbound, &pclOutValu, &llOutValLen);      
        if (ilRC == RC_SUCCESS)
        {
            ilLocalIotf = EVT_OUTBOUND; /* hag04.11.2003, PRF5389 */
            strcpy(pclTabFldVal, pclOutValu);
            dbg(DEBUG, "CDT1: outbound field <%s> <%s> found",
                psCondition.crField, pclOutValu);
        }
        else
        {
            dbg(DEBUG, "CDT1: outbound field <%s> not found - outbound",
                psCondition.crField);
        } /* end of else GetFieldData2 () == RC_SUCCESS */
    } /* end of if igEventType == EVT_....Outbound */
  
    if (igEventType == EVT_FLIGHT_INDEPENDENT)     /* added 06.09.2000 GHe */ 
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                            pcgFieldList, pcgInbound, &pclInbValu, &llInValLen );  
        if (ilRC == RC_SUCCESS)
        { 
            ilLocalIotf = EVT_INBOUND;  /* hag04.11.2003, PRF5389 */
            strcpy(pclTabFldVal, pclInbValu);
            psCondition.lrIotf = EVT_FLIGHT_INDEPENDENT;
            dbg(DEBUG,"CDT1: FIN - FI_bound field <%s> <%s> found",
                psCondition.crField, pclInbValu);
        }
        else
        {
            dbg(DEBUG, "CDT1: FIN - FI_bound field <%s> not found - FIbound",
                psCondition.crField);
        } /* end of else GetFieldData2 () == RC_SUCCESS */
    } /* end of if igEventType == EVT_FLIGHT_INDEPENDENT  */
 

    if (pclTabFldVal[0] == '\0')
        /* event type and condition type do not fit */
    {
         dbg(DEBUG,
            "CDT1: event type <%d> and condition type <%ld> do not fit",
            igEventType, psCondition.lrIotf);
        ilRC = RC_FAIL;
        if (bgDiagOn)
        {
            if (bgDiagInt)
                WriteDiagFile(DIAGINT, DIAGTYPEFALSE, (char *)igEventType,
                                (char *)psCondition.lrIotf, NULL);
            if (bgDiagExt)
                WriteDiagFile(DIAGEXT, DIAGTYPEFALSE, (char *)igEventType,
                                (char *)psCondition.lrIotf, NULL);
        }
    }

    sprintf(rgDgrArray.pcrIdx01RowBuf, "%s,%s,%s", pcpHopo, pcpUtpl,
            psCondition.crValue);
    dbg(DEBUG, "CDT1: pattern <%s>", rgDgrArray.pcrIdx01RowBuf); 
 
    if(ilRC == RC_SUCCESS)
    {
        llFunc = ARR_FIRST;
        ilRC = CEDAArrayFindRowPointer( &(rgDgrArray.rrArrayHandle),
                                        &(rgDgrArray.crArrayName[0]),
                                        &(rgDgrArray.rrIdx01Handle),
                                        &(rgDgrArray.crIdx01Name[0]),
                                        rgDgrArray.pcrIdx01RowBuf, &llFunc,
                                        (void *) &pclResult);

        if(ilRC != RC_SUCCESS)               /*------------------------*/
        {                                    /* ERR -> no RULE found   */
                                             /* -----------------------*/
            ilRC = RC_NOTFOUND;
            dbg(DEBUG, "CDT1: DGRTAB > NO RECORDS FOUND");
            if (bgDiagOn)
            {
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGNODYNRULE, psCondition.crValue, NULL, NULL);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGNODYNRULE, psCondition.crValue, NULL, NULL);
            }
        }

        if(ilRC == RC_SUCCESS)
        {
            pclExpr = pclResult + rgDgrArray.plrArrayFieldOfs[5];
    
            UndoCharMapping( pclExpr );
 
            dbg(DEBUG, "CDT1: DRG EXPR= <%s>", pclExpr); 
            strcpy(cpcmpStr, pclExpr);

    /* check, if one more data record exist */
            llFunc = ARR_NEXT;
            ilRC = CEDAArrayFindRowPointer( &(rgDgrArray.rrArrayHandle),
                                            &(rgDgrArray.crArrayName[0]),
                                            &(rgDgrArray.rrIdx01Handle),
                                            &(rgDgrArray.crIdx01Name[0]),
                                            rgDgrArray.pcrIdx01RowBuf, &llFunc,
                                            (void *) &pclResult);
            if(ilRC == RC_SUCCESS)
            {
     /* ------------------------------ */ 
     /* ERR -> only one RULE is useful */
     /* ------------------------------ */ 
                ilRC = RC_FAIL;
                dbg(DEBUG, "CDT1: URNO2=%s",
                    pclResult + rgDgrArray.plrArrayFieldOfs[2]);
                if (bgDiagOn)
                {
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGTOOMANYDYNR, psCondition.crValue, NULL,
                                        NULL);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGTOOMANYDYNR, psCondition.crValue, NULL,
                                        NULL);
                }
            }
            else 
            {
                ilRC = RC_SUCCESS;  /* for perfect situation */     
            } 
        } /* ilRC = RC_SUCCESS */ 
    } /* if RC_SUCCESS > array handling */ 

    if(ilRC == RC_SUCCESS)     /* create SQL string */ 
    {
        ilRC = AnalyseStr(cpcmpStr, pclTableName, &ilFldCnt, pclFieldsName,
                        &lcondLen);
        if (ilRC == RC_SUCCESS)      /* create SQL string */  
        {
            if ( strcmp(psCondition.crTable, psCondition.crRefTab) == 0 )
            {/*inbound OR outbound filled is the question*/
                /* if ( pclInbValu != '\0' )  hag04.11.2003, PRF5389 */
                if ( ilLocalIotf == EVT_INBOUND )   
                    ilRC = GetDataFromBuffer(pcgInbound, pcgFieldList,
                                                pclTableName, &pclFieldsName[0],
                                                &clDataArea[0],
                                                ilFldCnt);
                else 
                /*  if ( pclOutValu != '\0' )  hag04.11.2003, PRF5389 */
                if ( ilLocalIotf == EVT_OUTBOUND )  
                    ilRC = GetDataFromBuffer(pcgOutbound, pcgFieldList,
                                                pclTableName, &pclFieldsName[0],
                                                &clDataArea[0],
                                                ilFldCnt);
                else
                    ilRC = RC_FAIL;
            }
            else
            {
                sprintf (clSqlbuf, "select %s from %s where %s = :Val1",
                         pclFieldsName, pclTableName, psCondition.crRefFld);
                                                              /*hag000811*/

                dbg (DEBUG,"CDT1: sqlbuf=<%s>", clSqlbuf) ;  
                strcpy(clDataArea, pclTabFldVal);  /* set reference value new */
                dbg(DEBUG,"CDT1: VAL1 for SQL is = <%s>", clDataArea );
                ilRC = sql_if (slSqlFkt, &slCursor, clSqlbuf, clDataArea);
                if (ilRC == DB_SUCCESS)
                {
                    dbg(DEBUG,"CheckCDT1: sqlif OK  <%s>", clDataArea );   
                    ilRC = RC_SUCCESS;
                }
                else
                {
                    dbg (TRACE, "CheckCDT1: sqlif NOT FOUND" ) ;
                    ilRC = RC_NOTFOUND ; 
                    if (bgDiagOn)
                    {
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGNODATA, psCondition.crValue, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGNODATA, psCondition.crValue, NULL,
                                            NULL);
                    }
                } /* end of if */
            }
        }
    } /* if RC_SUCCESS */ 

    if (ilRC == RC_SUCCESS)
    {
        lcondLen++;
        if ( llLastCondLen < lcondLen )
            slCondition = realloc (slCondition, lcondLen ) ;

        if (slCondition == NULL) 
        {
            dbg(DEBUG,"CDT1: ALLOC ERR for Condition String" ) ;
            ilRC = RC_FAIL ;
        }
        else
            llLastCondLen = lcondLen;
    }

    if (ilRC == RC_SUCCESS)
    {
        ilRC = SetCmpStringCond (slCondition, cpcmpStr, clDataArea  ) ;
        if (ilRC == RC_SUCCESS)
        {
            if (igTestOutput > 2 ) 
            {
                dbg (DEBUG, "CONDITION --> %s", slCondition) ;
            }
            /* test for lex yacc function */
            ilRC = CheckCondition (slCondition) ;
            if (ilRC == RC_SUCCESS) 
            {
                *plpIsTrue = TRUE;
            }
            dbg(DEBUG, "CheckCDT1: CheckCondition <%s> evaluates to <%d>",
                    slCondition, *plpIsTrue );
        }
        else
        {
            if (bgDiagOn)
            {
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGCONDINCORR, slCondition, NULL,
                                    NULL);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGCONDINCORR, slCondition, NULL,
                                    NULL);
            }
        }
    }

  dbg (DEBUG, "CDT1: closing cursor <%d>", slCursor) ;
  close_my_cursor (&slCursor) ;
/*
  if (pclInbValu != NULL)
  {
   free(pclInbValu) ;
   pclInbValu = NULL ;
  }  */
/*
  if (pclOutValu != NULL)
  {
   free (pclOutValu) ;
   pclOutValu = NULL ;
  }  */

  /* if (slCondition != NULL)
  {
   free (slCondition) ;
   slCondition = NULL ;
  }  */

  if(ilRC == RC_NOTFOUND)
  {
   ilRC = RC_SUCCESS ;
  } /* if (ilRC == RC_NOTFOUND) */

  return (ilRC) ;
        
} /* end of CheckCDT1 */






/******************************************************************************/
static int GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, char *pcpData, char **pppFieldData)
{
 int   ilRC     = RC_SUCCESS ;       /* Return code */
 int   ilPos    = 0 ;
 int   ilCol    = 0 ;
 int   ilItemNo = 0 ;
 long  llLen    = 0 ;
 char  clDel    = ',' ;

 ilRC = FindItemInList (pcpFldLst, pcpFldNam, clDel, &ilItemNo, &ilCol, &ilPos) ;
 if (ilRC == RC_SUCCESS)
 {
  ilRC = GetFieldLength (pcpTabNam, pcpFldNam, &llLen) ;
  if ( llLen<=0 )
  {
      ilRC = RC_FAIL;
      if ( pcpTabNam && pcpFldNam )
        dbg (TRACE, "GFDAT: GetFielLen for <%s.%s> returned Len <%ld>", pcpTabNam, pcpFldNam, llLen ) ;      
      else
        dbg (TRACE, "GFDAT: GetFielLen returned Len <%ld>", llLen ) ;      
  }
   if (ilRC == RC_SUCCESS)
  {
   llLen++ ;
   *pppFieldData = (char*)calloc ( 1, llLen ) ;
   if (*pppFieldData == NULL)
   {
    dbg (TRACE, "GFDAT: malloc(%ld) failed", llLen) ;  
    ilRC = RC_FAIL ;
   } /* end of if */
   else      /* alloc ok */ 
   {
    ilRC = GetDataItem (*pppFieldData, pcpData, ilItemNo, clDel, "", "\0\0") ;
    if (ilRC > 0)
    {
     ilRC = RC_SUCCESS ;
    } /* end of if */
   } /* else alloc ok */ 
  } /* end of if */
 } /* end of if */

 return (ilRC) ;

} /* end of GetFieldData */



/******************************************************************************/
static int CheckRuleCalendar(char *pcpUrno, char *pcpActualDate,
                             long *plpIsValid)
{
    int     ilRC = RC_SUCCESS;          /* Return code */
    char    pclRulename[64], clDate[24]; 

    *plpIsValid = FALSE;

    sprintf(pclRulename, "RULE_%s", "AFT");
    strcpy ( clDate, pcpActualDate );
    UtcToLocal( clDate );

    ilRC = CheckValidity(pclRulename, pcpUrno, clDate, (int *)plpIsValid);
    if (ilRC != RC_SUCCESS)
    {
        dbg (DEBUG, "CRCAL: CheckValidity failed <%d>", ilRC);
    }
    else
    {
        dbg(DEBUG, "CRCAL: <%s> is valid at <%s>", pcpUrno, pcpActualDate);
        *plpIsValid = TRUE;
    } /* end of if */

    return ilRC;
    
} /* end of CheckRuleCalendar */


/******************************************************************************/
static int CheckLocalDate(char *pcpUrno, char *pcpActualDate, long *plpIsValid)
{
    int     ilRC = RC_SUCCESS;          /* Return code */
    char    pclRulename[64]; 

    *plpIsValid = FALSE;

    sprintf(pclRulename, "RULE_%s", "AFT");

    ilRC = CheckValidity(pclRulename, pcpUrno, pcpActualDate, (int *)plpIsValid);
    if (ilRC != RC_SUCCESS)
    {
        dbg (DEBUG, "CheckLocalDate: CheckValidity failed <%d>", ilRC);
    }
    else
    {
        dbg(DEBUG, "CheckLocalDate: <%s> is valid at <%s>", pcpUrno, pcpActualDate);
        *plpIsValid = TRUE;
    } /* end of if */

    return ilRC;
    
} /* end of CheckLocalDate */



/******************************************************************************/
static int ProcessPrmRules(char *pcpHopo, char *pcpTplUrno, char *pcpActualDate)
{
    int     ilRC            = RC_SUCCESS ;  /* Return code */
    long    llIsTrue        = FALSE;
    long    llRuleFound     = FALSE;
    long    llBreakout      = FALSE;
    long    llCheckRule     = TRUE;
    long    llRowCnt        = 0;
    long    llFunc          = ARR_FIRST;
    char   *pclResult       = NULL; /* READ ONLY !!! */
    char   *pclRust         = NULL; /* READ ONLY !!! */
    char   *pclHopo         = NULL; /* READ ONLY !!! */
    char   *pclUtpl         = NULL; /* READ ONLY !!! */
    char   *pclEvty         = NULL; /* READ ONLY !!! */
    char   *pclExcl         = NULL; /* READ ONLY !!! */
    char   *pclMaxt         = NULL; /* READ ONLY !!! */
    char   *pclPrio         = NULL; /* READ ONLY !!! */
    char   *pclFspl         = NULL; /* READ ONLY !!! */
    char   *pclMist         = NULL; /* READ ONLY !!! */
    char   *pclEvrm         = NULL; /* READ ONLY !!! */
    char   *pclRuna         = NULL; /* READ ONLY !!! */
    char   *pclRusn         = NULL; /* READ ONLY !!! */
    char   *pclUrno         = NULL; /* READ ONLY !!! */
    char   *pclFisu         = NULL; /* READ ONLY !!! */
    long    llMaxt          = 0;
    long    llTifa          = 0;
    long    llTifd          = 0;
    long    llMist          = 0;
    static char   *pclTifa         = NULL;
    static char   *pclTifd         = NULL;
    static long   llTifaLen = 0;
    static long   llTifdLen=0;   
    long    llDiff          = 0;
    long    llIsValid       = 0;
    struct  tm rlTime ;
    char    clTifaLocal[16];
    char    clTifdLocal[16];

    clTifaLocal[0] = '\0';
    clTifdLocal[0] = '\0';
    
    switch (igEventType)
    {
        case EVT_TURNAROUND :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno);
            break;

        case EVT_INBOUND    :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0,%d", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno, igEventType);
            break;

        case EVT_OUTBOUND  :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0,%d", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno, igEventType);
            break;

        /*case EVT_FLIGHT_INDEPENDENT :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,3", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno);
            break;*/

        default :
            dbg(TRACE, "PR: invalid event type <%d>", igEventType);
            ilRC = RC_FAIL;
    } /* end of switch */

    if (igEventType == EVT_INBOUND)
    {
        ilRC = GetFieldData2("DPVTAB", "TIFA", pcgFieldList, pcgInbound,
                            &pclTifa, &llTifaLen);
        if ( ilRC != RC_SUCCESS )
            dbg(TRACE, "PR: GetFieldData2 <TIFA> failed <%d>", ilRC);
        else
        {
            strcpy ( clTifaLocal, pclTifa );
            UtcToLocal ( clTifaLocal );
        }
    }
    if (igEventType == EVT_OUTBOUND)
    {
        ilRC = GetFieldData2("DPVTAB", "TIFD", pcgFieldList, pcgOutbound,
                            &pclTifd, &llTifdLen );
        if ( ilRC != RC_SUCCESS )
            dbg(TRACE, "%05d:PR: GetFieldData2 <TIFD> failed <%d> \nFields:<%s>\nData:<%s>", __LINE__,
                    ilRC,pcgFieldList,pcgOutbound);
        else
        {
            strcpy ( clTifdLocal, pclTifd );
            UtcToLocal ( clTifdLocal );
        }
    }

    if(ilRC == RC_SUCCESS)
    {
        pclResult = NULL;
        llRowCnt = 0;
        llFunc = ARR_FIRST;
        llRuleFound = FALSE;
        llBreakout = FALSE;
        strcpy(pcgInboundUrno, "empty");
        strcpy(pcgOutboundUrno, "empty");
        strcpy(pcgPrio,"empty");

        dbg(DEBUG, "PR: pattern <%s>", rgRueArray.pcrIdx01RowBuf);

        do
        {
            ilRC = CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),
                                            &(rgRueArray.crArrayName[0]),
                                            &(rgRueArray.rrIdx01Handle),
                                            &(rgRueArray.crIdx01Name[0]),
                                            rgRueArray.pcrIdx01RowBuf, &llFunc,
                                            (void *) &pclResult);

            if (ilRC != RC_SUCCESS)
            {
                dbg(DEBUG, "PR: RUETAB no >> ROW! ilRC = <%ld>", ilRC);
            }
            else          /* if (ilRC == RC_SUCCESS)    */ 
            {
                llRowCnt++;
                llCheckRule = TRUE;
                llFunc = ARR_NEXT;
     
/* PRIO  7 -> ilItemNo - ilRC = FindItemInList(pcpFieldList,"MAXT",clDel,&ilItemNo,&ilCol,&ilPos); */
     
                pclRust = pclResult + rgRueArray.plrArrayFieldOfs[0];
                pclHopo = pclResult + rgRueArray.plrArrayFieldOfs[1];
                pclUtpl = pclResult + rgRueArray.plrArrayFieldOfs[2];
                pclPrio = pclResult + rgRueArray.plrArrayFieldOfs[7];
                pclUrno = pclResult + rgRueArray.plrArrayFieldOfs[11];
                pclRuna = pclResult + rgRueArray.plrArrayFieldOfs[12];
                pclRusn = pclResult + rgRueArray.plrArrayFieldOfs[18];
                pclEvty = pclResult + rgRueArray.plrArrayFieldOfs[4];   
                strcpy(pcgUtpl, pclUtpl);
     
     /****************************************************************/
                dbg(DEBUG, "######## Checking URNO <%s> RUSN <%s> Utpl <%s> ########",
                        pclUrno, pclRusn, pclUtpl );
                if ((llRuleFound == TRUE) && (strcmp (pcgPrio, pclPrio) != 0))
                {
                    llBreakout = TRUE;
                    dbg(DEBUG, "PR: breako/ut prio changed, prio <%s>",
                        pclPrio);
                }
                else
                {
                    if (bgDiagOn)
                    {
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGRUE, pclRusn, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGRUE, pclRusn, NULL,
                                            NULL);
                    }

      /* if Arrival or Turnaround check arival date */
      
                    if (clTifaLocal[0] && ((pclEvty[0] == '0') || (pclEvty[0] == '1')))
                    {/*Check Inbound Time, if ruletyp is turnaround or inbound*/
                        /*  ilRC = CheckRuleCalendar(pclUrno, pclTifa, &llIsValid);  */
                        ilRC = CheckLocalDate(pclUrno, clTifaLocal,  &llIsValid);
                        dbg(DEBUG,
                            "CheckLocalDate: Rule <%s> Date <%s> IsValid <%ld>",
                            pclRuna, pclTifa, llIsValid);
                        if (bgDiagOn && (ilRC != RC_SUCCESS))
                        {
                            if (bgDiagInt)
                                WriteDiagFile(DIAGINT, DIAGRUENOTVALA, pclTifa,
                                                NULL, NULL);
                            if (bgDiagExt)
                                WriteDiagFile(DIAGEXT, DIAGRUENOTVALA, pclTifa,
                                                NULL, NULL);
                        }
                    }
      /* if Departure or Turnaround check departure date */
      
                    if (clTifdLocal[0] && ((pclEvty[0] == '0') || (pclEvty[0] == '2')))
                    {/*Check outbound Time, if ruletyp is turnaround or outbound*/
                        /* ilRC |= CheckRuleCalendar(pclUrno, pclTifd, &llIsValid); */
                        ilRC = CheckLocalDate(pclUrno, clTifdLocal,  &llIsValid);
                        dbg(DEBUG,
                            "CheckLocalDate: Rule <%s> Date <%s> IsValid <%ld>",
                            pclRuna, pclTifd, llIsValid);
                        if (bgDiagOn && (ilRC != RC_SUCCESS))
                        {
                            if (bgDiagInt)
                                WriteDiagFile(DIAGINT, DIAGRUENOTVALD, pclTifd,
                                                NULL, NULL);
                            if (bgDiagExt)
                                WriteDiagFile(DIAGEXT, DIAGRUENOTVALD, pclTifd,
                                                NULL, NULL);
                        }
                    }

      /* ################## */

                    if (ilRC == RC_SUCCESS)
                    {

                        if (llCheckRule == TRUE)
                        {
                            pclEvrm = pclResult +
                                        rgRueArray.plrArrayFieldOfs[10];
                            llIsTrue = FALSE;

                            ilRC = ProcessConditions(pcpHopo, pclEvrm,
                                                        &llIsTrue);
                            if (ilRC == RC_SUCCESS)
                            {
                                if (llIsTrue == TRUE)
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT, DIAGCONDTRUE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT, DIAGCONDTRUE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG,
                                        "PR: <%s> <%s> all conditions are true",
                                        pclRusn, pclUrno);
                                    pclExcl = pclResult +
                                                rgRueArray.plrArrayFieldOfs[5];
    
                                    if (strcmp(pclExcl, "1") == 0)
                                    {
                                        dbg(DEBUG, "PR: excl set");    
                                        pclUrno = cgExclude;
                                    }

                                    if (llRuleFound == TRUE)
                                    {
                                        dbg(TRACE, "PR: second rule found");
                                    }
                                    else
                                    {
                                        pclFisu = pclResult +
                                            rgRueArray.plrArrayFieldOfs[14];
                                        dbg(DEBUG, "PR: urno <%s> fisu <%s>",
                                            pclUrno, pclFisu); 

                                        if (*pclEvty == '3')
                                            /* EVT_FLIGHT_INDEPENDENT */
                                        { 
                                            sprintf(pcgInboundUrno,
                                                    "%s%s%s", pclUrno,
                                                    &cgSoh[0], pclFisu);
                                            sprintf(pcgOutboundUrno, "0%s0", &cgSoh[0]);
                                            llRuleFound = TRUE;
                                            strcpy(pcgPrio,pclPrio);    
                                            dbg(DEBUG,
                                                "PR: FOUND INDEP.RULE TYPE 3 - prio <%s>",
                                                pcgPrio);
                                        }
                                        else 
                                        {
                                            switch (*pclEvty)
                                            {

                                                case '1' :    /* INBOUND */
                                                    switch (igEventType)
                                                    {

                                                        case EVT_INBOUND :
                                                            sprintf(pcgInboundUrno,
                                                                    "%s%s%s",
                                                                    pclUrno,
                                                                    &cgSoh[0],
                                                                    pclFisu);
                                                            sprintf(pcgOutboundUrno,
                                                                    "0%s0",
                                                                    &cgSoh[0]);
                                                            llRuleFound = TRUE; 
                                                            strcpy(pcgPrio,
                                                                    pclPrio);
                                                            dbg(DEBUG,
                                                                "PR: prio <%s>",
                                                                pcgPrio);    
                                                            break;

                                                        case EVT_OUTBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType,pclEvty);   
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        default :
                                                            dbg(TRACE,
                                                                "PR: unknown event type <%d>",
                                                                igEventType);
                                                            ilRC = RC_FAIL;
                                                            break; 
                                                    } /* end of switch */               
                                                    break;

                                                case '2' :  /* OUTBOUND */
                                                    switch (igEventType)
                                                    {
                                                        case EVT_INBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType, pclEvty);
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        case EVT_OUTBOUND :
                                                            sprintf(pcgInboundUrno,
                                                                    "0%s0",
                                                                    &cgSoh[0]);
                                                            sprintf(pcgOutboundUrno, "%s%s%s", pclUrno, &cgSoh[0], pclFisu);
                                                            llRuleFound = TRUE;
                                                            strcpy(pcgPrio,pclPrio);
                                                            dbg(DEBUG,
                                                                "PR: prio <%s>", pcgPrio);
                                                            break;

                                                        default :
                                                            dbg(TRACE,
                                                                "PR: unknown event type <%d>",
                                                                igEventType);
                                                            ilRC = RC_FAIL;
                                                            break; 
                                                    } /* end of switch */
                                                    break;

                                                default :
                                                    dbg(TRACE,
                                                        "PR: unknown evty <%c>",
                                                        pclEvty);
                                                    ilRC = RC_FAIL;
                                                    break;

                                            } /* end of switch */
                                        } /*else - not FLIGHT INDEPENDENT*/
                                    } /* end of if */
                                }
                                else
                                {
                                    dbg(DEBUG,
                                        "PR: <%s> <%s> any condition is false",
                                        pclRusn, pclUrno);  
                                } /* end of if */
                            }
                            else
                            {
                                dbg(TRACE, "PR: ProcessConditions failed <%d>",
                                    ilRC);
                            } /* end of if */
                        } /* end of if */
                    } 
                    else
                    {
                        ilRC = RC_SUCCESS;
                        dbg(DEBUG, "PR: rule <%s> <%s> is NOT valid ", pclRusn, pclUrno ); 
                    } /* end of else */
                } /* end of if */
            } /* end of if  (if found a record in the array ) */
        } while((ilRC == RC_SUCCESS) && (llBreakout == FALSE));

        dbg(DEBUG, "PR: ilRC <%d> llBreakout <%ld>", ilRC , llBreakout);

        if (ilRC == RC_NOTFOUND)
        {
            if (llRowCnt == 0)
            {
                dbg(DEBUG, "PR: pattern <%s> NOT FOUND, tmpl without rule ?",
                    rgRueArray.pcrIdx01RowBuf); 
                ilRC = RC_FAIL;

                if (bgDiagOn)
                {
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGNORUE, NULL, NULL, NULL);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGNORUE, NULL, NULL, NULL);
                }
            }
            else
            {
                ilRC = RC_SUCCESS;
            } /* end of if */
        }
        else
        {
            if (llRuleFound == FALSE)
            {
                dbg(TRACE, "PR: CEDAArrayFindRowPointer failed <%d>", ilRC);  
            } /* end of if */
        } /* end of if */
    } /* end of if */

    /*
    if (pclTifa != NULL)
    {
        free(pclTifa);
        pclTifa = NULL;
    } 

    if (pclTifd != NULL)
    {
        free(pclTifd);
        pclTifd = NULL;
    }   */

    return (ilRC);
    
}

/******************************************************************************/
static int ProcessRules(char *pcpHopo, char *pcpTplUrno, char *pcpActualDate)
{
    int     ilRC            = RC_SUCCESS ;  /* Return code */
    long    llIsTrue        = FALSE;
    long    llRuleFound     = FALSE;
    long    llBreakout      = FALSE;
    long    llCheckRule     = TRUE;
    long    llRowCnt        = 0;
    long    llFunc          = ARR_FIRST;
    char   *pclResult       = NULL; /* READ ONLY !!! */
    char   *pclRust         = NULL; /* READ ONLY !!! */
    char   *pclHopo         = NULL; /* READ ONLY !!! */
    char   *pclUtpl         = NULL; /* READ ONLY !!! */
    char   *pclEvty         = NULL; /* READ ONLY !!! */
    char   *pclExcl         = NULL; /* READ ONLY !!! */
    char   *pclMaxt         = NULL; /* READ ONLY !!! */
    char   *pclPrio         = NULL; /* READ ONLY !!! */
    char   *pclFspl         = NULL; /* READ ONLY !!! */
    char   *pclMist         = NULL; /* READ ONLY !!! */
    char   *pclEvrm         = NULL; /* READ ONLY !!! */
    char   *pclRuna         = NULL; /* READ ONLY !!! */
    char   *pclRusn         = NULL; /* READ ONLY !!! */
    char   *pclUrno         = NULL; /* READ ONLY !!! */
    char   *pclFisu         = NULL; /* READ ONLY !!! */
    long    llMaxt          = 0;
    long    llTifa          = 0;
    long    llTifd          = 0;
    long    llMist          = 0;
    static char   *pclTifa         = NULL;
    static char   *pclTifd         = NULL;
    static long   llTifaLen = 0;
    static long   llTifdLen=0;   
    long    llDiff          = 0;
    long    llIsValid       = 0;
    struct  tm rlTime ;
    char    clTifaLocal[16];
    char    clTifdLocal[16];

    clTifaLocal[0] = '\0';
    clTifdLocal[0] = '\0';
    
    switch (igEventType)
    {
        case EVT_TURNAROUND :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno);
            break;

        case EVT_INBOUND    :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0,%d", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno, igEventType);
            break;

        case EVT_OUTBOUND  :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,0,%d", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno, igEventType);
            break;

        /*case EVT_FLIGHT_INDEPENDENT :
            sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,3", TPL_ACTIVE,
                    pcpHopo, pcpTplUrno);
            break;*/

        default :
            dbg(TRACE, "PR: invalid event type <%d>", igEventType);
            ilRC = RC_FAIL;
    } /* end of switch */

    if (igEventType == EVT_TURNAROUND)
    {
        ilRC = GetFieldData2("AFTTAB", "TIFA", pcgFieldList, pcgInbound,
                            &pclTifa, &llTifaLen);
        if (ilRC == RC_SUCCESS)
        {
            strcpy ( clTifaLocal, pclTifa );
            UtcToLocal ( clTifaLocal );

            ilRC = CedaDateToTimeStruct(&rlTime, pclTifa);
            if (ilRC == RC_SUCCESS)
            {
                ilRC = TimeStructToTimeType(&llTifa, &rlTime, NULL);
                if (ilRC == RC_SUCCESS)
                {
                    dbg(DEBUG, "PR: inbound  tifa <%s><%d>", pclTifa, llTifa);
                }
                else
                {
                    dbg(TRACE, "PR: TimeStructToTimeType failed <%d>", ilRC);
                } /* end of if */
            }
            else
            {
                dbg(TRACE, "PR: CedaDateToTimeStruct failed <%d>", ilRC);
            } /* end of if */
        }
        else
        {
            dbg(TRACE, "PR: GetFieldData2 failed <%d>", ilRC);
        } /* end of if */

        if (ilRC == RC_SUCCESS)
        {
            ilRC = GetFieldData2("AFTTAB", "TIFD", pcgFieldList, pcgOutbound,
                                &pclTifd, &llTifdLen );
            strcpy ( clTifdLocal, pclTifd );
            UtcToLocal ( clTifdLocal );

            if (ilRC == RC_SUCCESS)
            {
                ilRC = CedaDateToTimeStruct(&rlTime,pclTifd);
                if (ilRC == RC_SUCCESS)
                {
                    ilRC = TimeStructToTimeType(&llTifd,&rlTime,NULL);
                    if (ilRC == RC_SUCCESS)
                    {
                        dbg(DEBUG, "PR: outbound  tifd <%s><%d>", pclTifd,
                            llTifd);
                    }
                    else
                    {
                        dbg(TRACE, "PR: TimeStructToTimeType failed <%d>",
                            ilRC);
                    } /* end of if */
                }
                else
                {
                    dbg(TRACE, "PR: CedaDateToTimeStruct failed <%d>", ilRC);
                } /* end of if */
            }
            else
            {
                dbg(TRACE, "PR: GetFieldData2 failed <%d>", ilRC);
            } /* end of if */
        } /* end of if */
    } /* end of if EVT_TURNAROUND */
    if (igEventType == EVT_INBOUND)
    {
        ilRC = GetFieldData2("AFTTAB", "TIFA", pcgFieldList, pcgInbound,
                            &pclTifa, &llTifaLen);
        if ( ilRC != RC_SUCCESS )
            dbg(TRACE, "PR: GetFieldData2 <TIFA> failed <%d>", ilRC);
        else
        {
            strcpy ( clTifaLocal, pclTifa );
            UtcToLocal ( clTifaLocal );
        }
    }
    if (igEventType == EVT_OUTBOUND)
    {
        ilRC = GetFieldData2("AFTTAB", "TIFD", pcgFieldList, pcgOutbound,
                            &pclTifd, &llTifdLen );
        if ( ilRC != RC_SUCCESS )
            dbg(TRACE, "PR: GetFieldData2 <TIFD> failed <%d>", ilRC);
        else
        {
            strcpy ( clTifdLocal, pclTifd );
            UtcToLocal ( clTifdLocal );
        }
    }

    if(ilRC == RC_SUCCESS)
    {
        pclResult = NULL;
        llRowCnt = 0;
        llFunc = ARR_FIRST;
        llRuleFound = FALSE;
        llBreakout = FALSE;
        strcpy(pcgInboundUrno, "empty");
        strcpy(pcgOutboundUrno, "empty");
        strcpy(pcgPrio,"empty");

        dbg(DEBUG, "PR: pattern <%s>", rgRueArray.pcrIdx01RowBuf);

        do
        {
            ilRC = CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),
                                            &(rgRueArray.crArrayName[0]),
                                            &(rgRueArray.rrIdx01Handle),
                                            &(rgRueArray.crIdx01Name[0]),
                                            rgRueArray.pcrIdx01RowBuf, &llFunc,
                                            (void *) &pclResult);

            if (ilRC != RC_SUCCESS)
            {
                dbg(DEBUG, "PR: RUETAB no >> ROW! ilRC = <%ld>", ilRC);
            }
            else          /* if (ilRC == RC_SUCCESS)    */ 
            {
                llRowCnt++;
                llCheckRule = TRUE;
                llFunc = ARR_NEXT;
     
/* PRIO  7 -> ilItemNo - ilRC = FindItemInList(pcpFieldList,"MAXT",clDel,&ilItemNo,&ilCol,&ilPos); */
     
                pclRust = pclResult + rgRueArray.plrArrayFieldOfs[0];
                pclHopo = pclResult + rgRueArray.plrArrayFieldOfs[1];
                pclUtpl = pclResult + rgRueArray.plrArrayFieldOfs[2];
                pclPrio = pclResult + rgRueArray.plrArrayFieldOfs[7];
                pclUrno = pclResult + rgRueArray.plrArrayFieldOfs[11];
                pclRuna = pclResult + rgRueArray.plrArrayFieldOfs[12];
                pclRusn = pclResult + rgRueArray.plrArrayFieldOfs[18];
                pclEvty = pclResult + rgRueArray.plrArrayFieldOfs[4];   
                strcpy(pcgUtpl, pclUtpl);
     
     /****************************************************************/
                dbg(DEBUG, "######## Checking URNO <%s> RUSN <%s> Utpl <%s> ########",
                        pclUrno, pclRusn, pclUtpl );
                if ((llRuleFound == TRUE) && (strcmp (pcgPrio, pclPrio) != 0))
                {
                    llBreakout = TRUE;
                    dbg(DEBUG, "PR: breako/ut prio changed, prio <%s>",
                        pclPrio);
                }
                else
                {
                    if (bgDiagOn)
                    {
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGRUE, pclRusn, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGRUE, pclRusn, NULL,
                                            NULL);
                    }

      /* if Arrival or Turnaround check arival date */
      
                    if (clTifaLocal[0] && ((pclEvty[0] == '0') || (pclEvty[0] == '1')))
                    {/*Check Inbound Time, if ruletyp is turnaround or inbound*/
                        /*  ilRC = CheckRuleCalendar(pclUrno, pclTifa, &llIsValid);  */
                        ilRC = CheckLocalDate(pclUrno, clTifaLocal,  &llIsValid);
                        dbg(DEBUG,
                            "CheckLocalDate: Rule <%s> Date <%s> IsValid <%ld>",
                            pclRuna, pclTifa, llIsValid);
                        if (bgDiagOn && (ilRC != RC_SUCCESS))
                        {
                            if (bgDiagInt)
                                WriteDiagFile(DIAGINT, DIAGRUENOTVALA, pclTifa,
                                                NULL, NULL);
                            if (bgDiagExt)
                                WriteDiagFile(DIAGEXT, DIAGRUENOTVALA, pclTifa,
                                                NULL, NULL);
                        }
                    }
      /* if Arrival or Turnaround check arival date */
      
                    if (clTifdLocal[0] && ((pclEvty[0] == '0') || (pclEvty[0] == '2')))
                    {/*Check outbound Time, if ruletyp is turnaround or outbound*/
                        /* ilRC |= CheckRuleCalendar(pclUrno, pclTifd, &llIsValid); */
                        ilRC = CheckLocalDate(pclUrno, clTifdLocal,  &llIsValid);
                        dbg(DEBUG,
                            "CheckLocalDate: Rule <%s> Date <%s> IsValid <%ld>",
                            pclRuna, pclTifd, llIsValid);
                        if (bgDiagOn && (ilRC != RC_SUCCESS))
                        {
                            if (bgDiagInt)
                                WriteDiagFile(DIAGINT, DIAGRUENOTVALD, pclTifd,
                                                NULL, NULL);
                            if (bgDiagExt)
                                WriteDiagFile(DIAGEXT, DIAGRUENOTVALD, pclTifd,
                                                NULL, NULL);
                        }
                    }

      /* ################## */

                    if (ilRC == RC_SUCCESS)
                    {
                        if (igEventType == EVT_TURNAROUND)          
                        {
                            pclMaxt = pclResult +
                                        rgRueArray.plrArrayFieldOfs[6];
                            dbg(DEBUG, "PR: evty <%s> maxt <%s>", pclEvty,
                                pclMaxt);
                            llMaxt = atol(pclMaxt) * 60;

                            llDiff = (llTifd - llTifa);
                            if (strcmp (pclEvty,"0") == 0)
                            {
                                if (llMaxt >= llDiff) /*hag 24.06.2003, PRF4528 */
                                {
                                    pclFspl = pclResult +
                                                rgRueArray.plrArrayFieldOfs[8];
                                    pclMist = pclResult +
                                                rgRueArray.plrArrayFieldOfs[9];
                                    llMist = atol(pclMist) * 60;
                                    dbg(DEBUG, "PR: fspl <%s> mist <%s><%d>",
                                        pclFspl, pclMist, llMist);
                                    if (llMist < llDiff)
                                    {
                                        if (strcmp(pclFspl,"1") == 0)
                                        {
                                            if (strncmp(pclTifa,pclTifd,8) != 0)
                                            {
                                                dbg(DEBUG,
                                                    "PR: splitt at day change");
                                                llCheckRule = FALSE;
                                                if (bgDiagOn)
                                                {
                                                    if (bgDiagInt)
                                                        WriteDiagFile(DIAGINT,
                                                                DIAGMISTTOSHORT,
                                                                (char *)llMist,
                                                                NULL, NULL);
                                                    if (bgDiagExt)
                                                        WriteDiagFile(DIAGEXT,
                                                                DIAGMISTTOSHORT,
                                                                (char *)llMist,
                                                                NULL, NULL);
                                                }
                                            } /* end of if */
                                        } /* end of if */
                                    } /* end of if */
                                }
                                else
                                {
                                    dbg(DEBUG, "PR: maxt exceeded (%d) < (%d)",
                                        llMaxt, llDiff);
                                    llCheckRule = FALSE;                        
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT,
                                                            DIAGMAXTTOLONG,
                                                            (char *)llMaxt,
                                                            NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT,
                                                            DIAGMAXTTOLONG,
                                                            (char *)llMaxt,
                                                            NULL, NULL);
                                    }
                                } /* end of if */                   
                            } /* end of if */
                        } /* end of if */

                        if (llCheckRule == TRUE)
                        {
                            pclEvrm = pclResult +
                                        rgRueArray.plrArrayFieldOfs[10];
                            llIsTrue = FALSE;

                            ilRC = ProcessConditions(pcpHopo, pclEvrm,
                                                        &llIsTrue);
                            if (ilRC == RC_SUCCESS)
                            {
                                if (llIsTrue == TRUE)
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT, DIAGCONDTRUE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT, DIAGCONDTRUE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG,
                                        "PR: <%s> <%s> all conditions are true",
                                        pclRusn, pclUrno);
                                    pclExcl = pclResult +
                                                rgRueArray.plrArrayFieldOfs[5];
    
                                    if (strcmp(pclExcl, "1") == 0)
                                    {
                                        dbg(DEBUG, "PR: excl set");    
                                        pclUrno = cgExclude;
                                    }

                                    if (llRuleFound == TRUE)
                                    {
                                        dbg(TRACE, "PR: second rule found");
                                    }
                                    else
                                    {
                                        pclFisu = pclResult +
                                            rgRueArray.plrArrayFieldOfs[14];
                                        dbg(DEBUG, "PR: urno <%s> fisu <%s>",
                                            pclUrno, pclFisu); 

                                        if (*pclEvty == '3')
                                            /* EVT_FLIGHT_INDEPENDENT */
                                        { 
                                            sprintf(pcgInboundUrno,
                                                    "%s%s%s", pclUrno,
                                                    &cgSoh[0], pclFisu);
                                            sprintf(pcgOutboundUrno, "0%s0", &cgSoh[0]);
                                            llRuleFound = TRUE;
                                            strcpy(pcgPrio,pclPrio);    
                                            dbg(DEBUG,
                                                "PR: FOUND INDEP.RULE TYPE 3 - prio <%s>",
                                                pcgPrio);
                                        }
                                        else 
                                        {
                                            switch (*pclEvty)
                                            {
                                                case '0':   /* TURNAROUND */
                                                    switch (igEventType)
                                                    {
                                                        case EVT_TURNAROUND:
                                                            sprintf(pcgInboundUrno,
                                                                    "%s%s%s",
                                                                    pclUrno,
                                                                    &cgSoh[0],
                                                                    pclFisu);
                                                            sprintf(pcgOutboundUrno,
                                                                    "%s%s%s",
                                                                    pclUrno, &cgSoh[0],
                                                                    pclFisu);
                                                            strcpy(pcgPrio,
                                                                    pclPrio);
                                                            dbg(DEBUG,
                                                                "PR: prio <%s>",
                                                                pcgPrio);
                                                            llRuleFound = TRUE;
                                                            break; 

                                                        case EVT_INBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType,
                                                                pclEvty);
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        case EVT_OUTBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType,                                                                    pclEvty);
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        default :
                                                            dbg(TRACE,
                                                                "PR: unknown event type <%d>",
                                                                igEventType);
                                                            ilRC = RC_FAIL;
                                                            break;
                                                    } /* end of switch */
                                                    break;

                                                case '1' :    /* INBOUND */
                                                    switch (igEventType)
                                                    {
                                                        case EVT_TURNAROUND:
                                                            if (strncmp(pcgInboundUrno, "empty", 5) == 0)
                                                            {
                                                                sprintf(pcgInboundUrno,
                                                                        "%s%s%s",
                                                                        pclUrno,
                                                                        &cgSoh[0],
                                                                        pclFisu);
                                                            }
                                                            if (strncmp(pcgOutboundUrno, "empty", 5) != 0)
                                                            {
                                                                llRuleFound = TRUE;
                                                                strcpy(pcgPrio,
                                                                       pclPrio);
                                                                dbg(DEBUG, "PR: prio <%s>", pcgPrio);     
                                                            }/* end of if */
                                                            break;

                                                        case EVT_INBOUND :
                                                            sprintf(pcgInboundUrno,
                                                                    "%s%s%s",
                                                                    pclUrno,
                                                                    &cgSoh[0],
                                                                    pclFisu);
                                                            sprintf(pcgOutboundUrno,
                                                                    "0%s0",
                                                                    &cgSoh[0]);
                                                            llRuleFound = TRUE; 
                                                            strcpy(pcgPrio,
                                                                    pclPrio);
                                                            dbg(DEBUG,
                                                                "PR: prio <%s>",
                                                                pcgPrio);    
                                                            break;

                                                        case EVT_OUTBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType,pclEvty);   
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        default :
                                                            dbg(TRACE,
                                                                "PR: unknown event type <%d>",
                                                                igEventType);
                                                            ilRC = RC_FAIL;
                                                            break; 
                                                    } /* end of switch */               
                                                    break;

                                                case '2' :  /* OUTBOUND */
                                                    switch (igEventType)
                                                    {
                                                        case EVT_TURNAROUND:
                                                            if (strncmp(pcgOutboundUrno, "empty", 5) == 0)
                                                            {
                                                                sprintf(pcgOutboundUrno,
                                                                        "%s%s%s",
                                                                        pclUrno,
                                                                        &cgSoh[0],
                                                                        pclFisu);
                                                            }
                                                            if (strncmp(pcgInboundUrno,"empty",5) != 0)
                                                            {
                                                                llRuleFound
                                                                     = TRUE;
                                                                strcpy(pcgPrio,
                                                                       pclPrio);
                                                                dbg(DEBUG,
                                                                    "PR: prio <%s>",
                                                                    pcgPrio);
                                                            }/* end of if */
                                                            break;
                     
                                                        case EVT_INBOUND :
                                                            dbg(TRACE,
                                                                "PR: illegal event type <%d> - <%c> rule combination",
                                                                igEventType, pclEvty);
                                                            ilRC = RC_FAIL;
                                                            break;

                                                        case EVT_OUTBOUND :
                                                            sprintf(pcgInboundUrno,
                                                                    "0%s0",
                                                                    &cgSoh[0]);
                                                            sprintf(pcgOutboundUrno, "%s%s%s", pclUrno, &cgSoh[0], pclFisu);
                                                            llRuleFound = TRUE;
                                                            strcpy(pcgPrio,pclPrio);
                                                            dbg(DEBUG,
                                                                "PR: prio <%s>", pcgPrio);
                                                            break;

                                                        default :
                                                            dbg(TRACE,
                                                                "PR: unknown event type <%d>",
                                                                igEventType);
                                                            ilRC = RC_FAIL;
                                                            break; 
                                                    } /* end of switch */
                                                    break;

                                                default :
                                                    dbg(TRACE,
                                                        "PR: unknown evty <%c>",
                                                        pclEvty);
                                                    ilRC = RC_FAIL;
                                                    break;

                                            } /* end of switch */
                                        } /*else - not FLIGHT INDEPENDENT*/
                                    } /* end of if */
                                }
                                else
                                {
                                    dbg(DEBUG,
                                        "PR: <%s> <%s> any condition is false",
                                        pclRusn, pclUrno);  
                                } /* end of if */
                            }
                            else
                            {
                                dbg(TRACE, "PR: ProcessConditions failed <%d>",
                                    ilRC);
                            } /* end of if */
                        } /* end of if */
                    } 
                    else
                    {
                        ilRC = RC_SUCCESS;
                        dbg(DEBUG, "PR: rule <%s> <%s> is NOT valid ", pclRusn, pclUrno ); 
                    } /* end of else */
                } /* end of if */
            } /* end of if  (if found a record in the array ) */
        } while((ilRC == RC_SUCCESS) && (llBreakout == FALSE));

        dbg(DEBUG, "PR: ilRC <%d> llBreakout <%ld>", ilRC , llBreakout);

        if (ilRC == RC_NOTFOUND)
        {
            if (llRowCnt == 0)
            {
                dbg(DEBUG, "PR: pattern <%s> NOT FOUND, tmpl without rule ?",
                    rgRueArray.pcrIdx01RowBuf); 
                ilRC = RC_FAIL;

                if (bgDiagOn)
                {
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGNORUE, NULL, NULL, NULL);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGNORUE, NULL, NULL, NULL);
                }
            }
            else
            {
                ilRC = RC_SUCCESS;
            } /* end of if */
        }
        else
        {
            if (llRuleFound == FALSE)
            {
                dbg(TRACE, "PR: CEDAArrayFindRowPointer failed <%d>", ilRC);  
            } /* end of if */
        } /* end of if */
    } /* end of if */

    /*
    if (pclTifa != NULL)
    {
        free(pclTifa);
        pclTifa = NULL;
    } 

    if (pclTifd != NULL)
    {
        free(pclTifd);
        pclTifd = NULL;
    }   */

    return (ilRC);
    
} /* end of ProcessRules */



/******************************************************************************/
static int ProcessIPRules(char *pcpHopo, char *pcpTplUrno, char *pcpActualDate)
{
    int     ilRC            = RC_SUCCESS ;  /* Return code */
    long    llIsTrue        = FALSE ;       /* process cond. answer */ 
    long    llRuleFound     = FALSE ;       /* flag for found more rules */ 
    long    llBreakout      = FALSE ;
    long    llCheckRule     = TRUE ;
    long    llRowCnt        = 0 ;
    long    llFunc          = ARR_FIRST ; 
    char   *pclResult       = NULL;         /* RO    result ceda arr */
    char   *pclRust         = NULL; /* READ ONLY !!! */
    char   *pclHopo         = NULL; /* READ ONLY !!! */
    char   *pclUtpl         = NULL; /* READ ONLY !!! */
    char   *pclExcl         = NULL; /* READ ONLY !!! */
    char   *pclPrio         = NULL; /* READ ONLY !!! */
    char   *pclEvrm         = NULL; /* READ ONLY !!! */
    char   *pclRuna         = NULL; /* READ ONLY !!! */
    char   *pclUrno         = NULL; /* READ ONLY !!! */
    char   *pclFisu         = NULL; /* READ ONLY !!! */
    char   *pclTifa         = NULL ;
    char   *pclTifd         = NULL ;
    long    llIsValid       = 0 ;    

    dbg(DEBUG, "PR_IP: >>");
    sprintf(rgRueArray.pcrIdx01RowBuf, "%d,%s,%s,3", TPL_ACTIVE, pcpHopo,
            pcpTplUrno);
    pclResult = NULL;
    llRowCnt = 0;
    llFunc = ARR_FIRST;
    llRuleFound = FALSE;
    llBreakout = FALSE;
    strcpy(pcgInboundUrno, "empty");
    strcpy(pcgOutboundUrno, "empty");
    strcpy(pcgPrio,"empty");

    do
    {
        dbg(DEBUG, "PR_IP: pattern <%s>", rgRueArray.pcrIdx01RowBuf);
        ilRC = CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),
                                        &(rgRueArray.crArrayName[0]),
                                        &(rgRueArray.rrIdx01Handle),
                                        &(rgRueArray.crIdx01Name[0]),
                                        rgRueArray.pcrIdx01RowBuf, &llFunc,
                                        (void *) &pclResult);

        if (ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"PR_IP: RUETAB no >> ROW! ilRC = <%ld>", ilRC); 
        }
        else 
        {  
            llRowCnt++;
            llCheckRule = TRUE;
            llFunc = ARR_NEXT;

            pclRust = pclResult + rgRueArray.plrArrayFieldOfs[0];
            pclHopo = pclResult + rgRueArray.plrArrayFieldOfs[1];
            pclUtpl = pclResult + rgRueArray.plrArrayFieldOfs[2];
            pclPrio = pclResult + rgRueArray.plrArrayFieldOfs[7];
            pclUrno = pclResult + rgRueArray.plrArrayFieldOfs[11];
            pclRuna = pclResult + rgRueArray.plrArrayFieldOfs[12];
            strcpy (pcgUtpl, pclUtpl); 
     
    /****************************************************************/
            dbg(DEBUG, "PR_IP: (Rust,Hopo,Utpl,Runa) found  <%s,%s,%s,%s>",
                pclRust, pclHopo, pclUtpl, pclRuna);     
            if((llRuleFound == TRUE) && (strcmp (pcgPrio, pclPrio) != 0))
            {
                llBreakout = TRUE;
                dbg(DEBUG, "PR_IP: breakout prio changed, prio <%s>", pclPrio);
            }
            else
            {
                dbg(DEBUG, "PR_IP: runa <%s>, prio <%s>", pclRuna, pclPrio);
                ilRC = CheckRuleCalendar(pclUrno, pcpActualDate, &llIsValid);
                dbg(DEBUG,"%05d: ActualDate <%s>",__LINE__,pcpActualDate);
                if (ilRC == RC_SUCCESS)
                {
                    if (llCheckRule == TRUE)
                    {
                        pclEvrm = pclResult + rgRueArray.plrArrayFieldOfs[10];
                        llIsTrue = FALSE;
 
                        ilRC = ProcessConditions(pcpHopo, pclEvrm, &llIsTrue);
                        if (ilRC == RC_SUCCESS)
                        {
                            if (llIsTrue == TRUE)
                            {
                                dbg(DEBUG,"PR_IP: <%s> all conditions are true",
                                    pclUrno) ;
                                pclExcl = pclResult +
                                                 rgRueArray.plrArrayFieldOfs[5];
     
                                if (strcmp(pclExcl, "1") == 0)
                                {
                                    dbg(DEBUG, "PR_IP: excl set");
                                    strcpy(pcgInboundUrno, "exclude");
                                    strcpy(pcgOutboundUrno,"exclude");
                                    llRuleFound = TRUE;
                                }
                                else
                                {
                                    if (llRuleFound == TRUE)
                                    {
                                        dbg(TRACE, "PR_IP: second rule found");
                                    }
                                    else
                                    {
                                        pclFisu = pclResult +
                                             rgRueArray.plrArrayFieldOfs[14];
                                        dbg(DEBUG, "PR_IP: urno <%s> fisu <%s>",
                                            pclUrno, pclFisu);
                                        sprintf(pcgInboundUrno, "%s%s%s",
                                                pclUrno, &cgSoh[0], pclFisu);
                                        sprintf(pcgOutboundUrno, "0%s0",
                                                &cgSoh[0]);
                                        llRuleFound = TRUE;
                                        strcpy(pcgPrio,pclPrio);    
                                        dbg(DEBUG,
                                            "PR_IP: FOUND INDEP.RULE TYPE 3 - prio <%s>",
                                            pcgPrio);
                                    } /* end of if */
                                } /* end of if */
                            }
                            else
                            {
                                dbg(DEBUG,"PR_IP: <%s> any condition is false",
                                    pclUrno);  
                            } /* end of if */
                        }
                        else
                        {
                            dbg(TRACE,"PR_IP: ProcessConditions failed <%d>",
                                ilRC);
                        } /* end of if */
                    } /* end of if */
                } 
                else
                {
                    ilRC = RC_SUCCESS;
                    dbg(DEBUG, "PR_IP: rule <%s> is NOT valid at <%s>", pclUrno,                        pcpActualDate);  
                } /* end of else */
            } /* end of if */
        } /* end of if  (if found a record in the array ) */
    } while ((ilRC == RC_SUCCESS) && (llBreakout == FALSE));

    dbg(DEBUG, "PR_IP: ilRC <%d> llBreakout <%ld>", ilRC , llBreakout);

    if (ilRC == RC_NOTFOUND)
    {
        if (llRowCnt == 0)
        {
            dbg (DEBUG, "PR_IP: pattern <%s> NOT FOUND, tmpl without rule ?",
                        rgRueArray.pcrIdx01RowBuf);
            ilRC = RC_FAIL;
        }
        else
        {
            ilRC = RC_SUCCESS;
        } /* end of if */
    }
    else
    {
        if (llRuleFound == FALSE)
        {
            dbg(TRACE, "PR_IP: CEDAArrayFindRowPointer failed <%d>", ilRC);  
        } /* end of if */
    } /* end of if */

    if (pclTifa != NULL)
    {
        free(pclTifa);
        pclTifa = NULL;
    } /* end of if */

    if (pclTifd != NULL)
    {
        free(pclTifd);
        pclTifd = NULL;
    } /* end of if */

    return (ilRC);
    
} /* end of ProcessIPRules */

static int AddToPrmQueue(char *pcpFirstLine,char *pcpSecondLine)
{
     static char *pclUrno = NULL;
     static long llUrnoLen = 12;
        static char clTmpUrno[24];
     int ilRC = RC_SUCCESS;
     int ilNewPrmBufLen;

    if (pclUrno == NULL)
    {
        pclUrno = realloc(pclUrno,llUrnoLen+4);
        memset(pclUrno,'\0',llUrnoLen);
    }
     ilRC = GetFieldData2( "DPV", "URNO",DPV_FIELDS ,pcpFirstLine,&pclUrno,&llUrnoLen);
     if (ilRC != RC_SUCCESS)
     {
            ilRC = GetFieldData2( "DPV", "URNO",DPV_FIELDS ,pcpSecondLine,&pclUrno,&llUrnoLen);
         }
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"No PRM Urno found <%s> <%s>",pcpFirstLine,pcpSecondLine);
        return ilRC;
    }
     sprintf(clTmpUrno,"%s,",pclUrno);
     strcat(cgPrmUrnoList,clTmpUrno);
     dbg(DEBUG,"New added URNO <%s> UrnoList <%s>",clTmpUrno,cgPrmUrnoList);

   dbg(TRACE,"AddToPrmQueue");
     ilNewPrmBufLen =  igPrmBufLen + strlen(pcpFirstLine) + strlen(pcpSecondLine) + 20;

     if (pcgPrmBuf == NULL)
     {
          pcgPrmBuf = calloc(sizeof(char),ilNewPrmBufLen);
     }
    else
    {
   pcgPrmBuf = realloc(pcgPrmBuf,ilNewPrmBufLen);
     }
     if (pcgPrmBuf == NULL)
     {
            dbg(TRACE,"Unable to  allocate %d Bytesfor PRM buffer",ilNewPrmBufLen);
            Terminate(-1);
     }
     igPrmBufLen = ilNewPrmBufLen;
     strcat(pcgPrmBuf,pcpFirstLine);
     strcat(pcgPrmBuf,cgEtx);
     strcat(pcgPrmBuf,pcpSecondLine);
     strcat(pcgPrmBuf,"\n");
     dbg(DEBUG,"Add pcpLine <%s><%s> PrmBuf is now: \n<%s>",pcpFirstLine,pcpSecondLine,pcgPrmBuf);
}

static int ReadOtherPRM(char *pcpFlight,char *pcpUrno,char *pcpSour)
{
    int ilRC = RC_SUCCESS;
    char clSelection[512];
    char clSqlbuf[2048];
    int sqlFunc = START;
    short slCursor = 0;
    char clUrno[24];

  sprintf(clSelection,"WHERE URNO = %s and SOUR = '%s'",pcpUrno,pcpSour);
    sprintf(clSqlbuf,"SELECT %s FROM DPVTAB %s ",DPV_FIELDS,clSelection);
    dbg(DEBUG,"%05d:%s",__LINE__,clSqlbuf);
    sqlFunc = START;

    ilRC = sql_if(sqlFunc, &slCursor, clSqlbuf, pcpFlight);
    if (ilRC == DB_SUCCESS)
    {
        BuildItemBuffer (pcpFlight, "", DPV_FIELDCOUNT, ",");
    }
    else if (ilRC == SQL_NOTFOUND)
    {
        dbg(DEBUG,"No other flight found");
    }
    else
    {
        dbg(TRACE,"Error reading other flight (%d)",ilRC);
        check_ret(ilRC);
    }
    close_my_cursor(&slCursor);
    return ilRC;

}




static int PutOnRulprmQueue(char *pcpAftUrno)
{
    int ilRC = 0;
    char clSelection[512];
    int sqlFunc = START;
    short slCursor = 0;
    char clSqlbuf[1024];
    char clUrno[24];
    static char clFirstFlight[4096];
    static char clSecondFlight[4096];
    char *pclFirstFlight = NULL;
    char *pclSecondFlight = NULL;
    static char *pclAdid = NULL;
    static char *pclTflu = NULL;
    static char *pclUrno = NULL;
    long llAdidLen = 0;
    long llTfluLen = 0;
    long llUrnoLen = 0;
    static char *pclThisSour = NULL;
    long llThisSourLen = 0;
    static char *pclOtherSour = NULL;
    long llOtherSourLen = 0;
    BC_HEAD  rlOutBCHead;
    BC_HEAD  *prlBCHead;
    CMDBLK rlOutCmdblk;
    CMDBLK *prlCmdblk;

    memset(clFirstFlight,0,sizeof(clFirstFlight));
    memset(clSecondFlight,0,sizeof(clSecondFlight));

    if (pclOtherSour == NULL)
    {
        pclOtherSour = malloc(22);
        llOtherSourLen = 10;
    }
    prlBCHead     = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT));
    prlCmdblk     = (CMDBLK *)  ((char *) prlBCHead->data);
    strcpy(clSelection,prlCmdblk->data);
    if (strcmp(prlBCHead->dest_name,"FLICOL") != 0)
    {
      if  (strcmp(prlCmdblk->tw_end,"OPSS-PM") == 0)
        {
            dbg(DEBUG,"Event not from FLICOL <%s>, but created by OPSS-PM. Will not processed",prlBCHead->dest_name);
            return RC_SUCCESS;
        }
    }
    sprintf(clSelection,"WHERE AFTU = %s",pcpAftUrno);
    sprintf(clSqlbuf,"SELECT %s FROM DPVTAB %s ",DPV_FIELDS,clSelection);
    dbg(DEBUG,"%05d:%s",__LINE__,clSqlbuf);
    sqlFunc = START;

    while ( ( ilRC = sql_if(sqlFunc, &slCursor, clSqlbuf, clFirstFlight))== DB_SUCCESS)
    {

    dbg(TRACE,"%05d",__LINE__);

        sqlFunc = NEXT;
        BuildItemBuffer (clFirstFlight, "", DPV_FIELDCOUNT, ",");
    

        ilRC = GetFieldData2( "DPV", "URNO",DPV_FIELDS ,clFirstFlight,&pclUrno,&llUrnoLen);
        ilRC = GetFieldData2( "DPV", "ADID",DPV_FIELDS ,clFirstFlight,&pclAdid,&llAdidLen);
        ilRC = GetFieldData2( "DPV", "TFLU",DPV_FIELDS ,clFirstFlight,&pclTflu,&llTfluLen);
        ilRC = GetFieldData2( "DPV", "SOUR",DPV_FIELDS ,clFirstFlight,&pclThisSour,&llThisSourLen);
        dbg(TRACE,"%05d RC=%d Urno <%s>",__LINE__,ilRC,pclUrno);
        if (ilRC != RC_SUCCESS)
        {
                dbg(TRACE,"URNO not found in DPV record. Fields: <%s> Data: <%s>",DPV_FIELDS,clFirstFlight);
        }
        else
        {
            if (!strcmp(pclThisSour,"FLNU"))
            {
                strcpy(pclOtherSour,"TFLU");
            }
            else
            {
                strcpy(pclOtherSour,"FLNU");
            }
            strcpy(clSecondFlight,"");
            ilRC = ReadOtherPRM(clSecondFlight,pclUrno,pclOtherSour);
            /*if (ilRC == RC_SUCCESS)*/
            {
            /* check ADID for first flight */
                ilRC = GetFieldData2( "DPV", "ADID",DPV_FIELDS ,clFirstFlight,&pclUrno,&llUrnoLen);
                if (*pclAdid == 'A')
                {
                    pclFirstFlight = clFirstFlight;
                    pclSecondFlight = clSecondFlight;
                }
                else
                {
                    if (*pclAdid == 'D')
                    {
                        pclFirstFlight = clSecondFlight;
                        pclSecondFlight = clFirstFlight;
                    }
                }
                AddToPrmQueue(pclFirstFlight,pclSecondFlight);
            }   
        }
    }
    memcpy(&rlOutCmdblk,prlCmdblk,sizeof(rlOutCmdblk));
    memcpy(&rlOutBCHead,prlBCHead,sizeof(rlOutBCHead));
    strcpy(rlOutCmdblk.command,"PRM");
    if (igPrmBufLen > 0  && *pcgPrmBuf != '\0')
    {
    dbg(DEBUG,"SendToRulprm %d\nFields: <%s>\nData:<%s>",igRulprmModid,
         DPV_FIELDS, pcgPrmBuf);
         SendToQue(igRulprmModid, PRIORITY_3, &rlOutBCHead, 
                     &rlOutCmdblk, clSelection, DPV_FIELDS, pcgPrmBuf);
        *pcgPrmBuf = '\0';
        }
        else
        {
                char clData[124];
            dbg(DEBUG,"SendToRulprm for AFT-URNO %s, no PRM found, send PRD command to  demprm",pcpAftUrno);    
                strcpy(clData,pcpAftUrno);
                strcpy(rlOutCmdblk.command,"PRD");
                SendToQue(igDemprmModid, PRIORITY_3, &rlOutBCHead, 
                    &rlOutCmdblk, clSelection, "URNO" , clData);
} if (ilRC ==  RC_NOTFOUND)
    {
        dbg(DEBUG,"RC=NOTFOUND");
    }
    close_my_cursor(&slCursor);
}


static int  SendToRulprm()
{
  int ilRC = FALSE;
    BC_HEAD *prlBchead         = NULL;
    CMDBLK  *prlCmdblk         = NULL;
    char    *pclSelection      = NULL;
    char    *pclFields         = NULL;
    char    *pclData           = NULL;
    char *pclNewline = NULL;
    static char  clArrivalFlight[2048];
    static char  clDepartureFlight[2048];

    dbg(DEBUG,"SendToRulprm start");

    memset(cgPrmUrnoList,0,sizeof(cgPrmUrnoList));
    strcpy(cgPrmUrnoList,",");

    prlBchead     = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT));
    prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data);
    pclSelection  = prlCmdblk->data;
    dbg(DEBUG,"%05d Selection:<%s>",__LINE__,pclSelection);
    pclFields     = pclSelection + strlen(pclSelection) + 1;
    dbg(DEBUG,"%05d Fields:<%s>",__LINE__,pclFields);
    pclData       = pclFields + strlen(pclFields) + 1;
    dbg(DEBUG,"%05d Data:<%s>",__LINE__,pclData);
    pclNewline = strchr(pclData,'\n');
    if (pclNewline != NULL)
    {
        *pclNewline = '\0';
        strcpy(clArrivalFlight,pclData);
        strcpy(clDepartureFlight,(pclNewline+1));
        *pclNewline = '\n';
    }
    else
    {
        strcpy(clArrivalFlight,pclData);
    }

            /* if  not the  PRM rulefinder forward the  event(s)  to the PRM rulefinder if fligth has PRM*/
    dbg(DEBUG,"%05dPRM Modid %d",__LINE__,igRulprmModid);
    if (igRulprmModid > -1)
    {
        /* send flight also to RULPRM*/
        static char *pclAftPrmc = NULL;
        static char *pclAftUrno = NULL;
        static char *pclAftAdid = NULL;
        static long llPrmcLen =  0;
        static long llUrnoLen =  0;
        static long llAdidLen = 0;

        ilRC = GetFieldData2("AFT", "ADID", pclFields,clArrivalFlight, &pclAftAdid, &llAdidLen );
        ilRC = GetFieldData2("AFT", "PRMC", pclFields,clArrivalFlight, &pclAftPrmc, &llPrmcLen );
        dbg(DEBUG,"%05d:RC=%d, <%s> %ld, ADID <%s>",__LINE__,ilRC,pclAftPrmc,llPrmcLen,pclAftAdid);
        if (ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"Fields: <%s> Data:<%s>",pclFields,pclData);
        }
        else if (*pclAftPrmc == 'Y')
        {
            ilRC = GetFieldData2("AFT", "URNO", pclFields,clArrivalFlight, &pclAftUrno, &llUrnoLen );
            dbg(DEBUG,"%05d ilRC=%d, AftUrno<%s>",__LINE__,ilRC,pclAftUrno);
            if (ilRC == RC_SUCCESS)
            {
                    PutOnRulprmQueue(pclAftUrno);
            }
        }
/* now check departure flight*/
        ilRC = GetFieldData2("AFT", "ADID", pclFields,clArrivalFlight, &pclAftAdid, &llAdidLen );
        ilRC = GetFieldData2("AFT", "PRMC", pclFields,clDepartureFlight, &pclAftPrmc, &llPrmcLen );
        dbg(DEBUG,"%05d:RC=%d PRMC=<%s>, ADID=<%s>",__LINE__,ilRC,pclAftPrmc,pclAftAdid);
        if (ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"%05d:Fields: <%s> \nData:<%s>",__LINE__,pclFields,clDepartureFlight);
        }
        else if (*pclAftPrmc == 'Y')
        {
            ilRC = GetFieldData2("AFT", "URNO", pclFields,clDepartureFlight, &pclAftUrno, &llUrnoLen );
            dbg(DEBUG,"%05d ilRC=%d, AftUrno<%s>",__LINE__,ilRC,pclAftUrno);
            if (ilRC == RC_SUCCESS)
            {
                    PutOnRulprmQueue(pclAftUrno);
            }
        }
    }
    return ilRC;
}

/******************************************************************************/
static int ProcessPrmTemplates(char *pcpHopo, char *pcpActualDate,
                            char *pcpSelection)
{
    int     ilRC        = RC_SUCCESS ;             /* Return code */
    long    llFunc      = ARR_FIRST ;
    char    *pclUrnoList = NULL ;
    char    *pclResult   = NULL ; /* READ ONLY !!! */
    char    *pclTpst     = NULL ; /* READ ONLY !!! */
    char    *pclHopo     = NULL ; /* READ ONLY !!! */
    char    *pclUrno     = NULL ; /* READ ONLY !!! */
    char    *pclTnam     = NULL ; /* READ ONLY !!! */
    char    *pclAppl = NULL;
    char    cpInfo [2000] ; 
    char    clApplCompare[9];

    sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
    dbg (DEBUG, "%05d:ProcessPRMTemplates : pattern <%s>", __LINE__,rgTplArray.pcrIdx01RowBuf) ;

    llFunc = ARR_FIRST ;
    pclUrnoList = pcgUrnoListBuf ;

    if (igEventType == EVT_FLIGHT_INDEPENDENT)
        strcpy(clApplCompare,"RULE_FIR");
    else
        strcpy(clApplCompare,"RULE_AFT");

    do
    {
        pclResult = NULL;
        ilRC = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult);
        llFunc  = ARR_NEXT;
        if (ilRC == RC_SUCCESS)
        {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
          if (bgIsPrm)
            {
                TrimRight(pclTnam);
                if (strstr(cgPrmTemplates,pclTnam) == NULL)
                {
                    /* not a PRM template*/
                    dbg(DEBUG,"%05d:not a PRM template <%s> <%s>",__LINE__,pclTnam,cgPrmTemplates);
                    llFunc = ARR_NEXT ;
                    continue;
                }
            }

            pclTpst = pclResult + rgTplArray.plrArrayFieldOfs[0];
            pclHopo = pclResult + rgTplArray.plrArrayFieldOfs[1];
            pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];

            if (strcmp(pclAppl,clApplCompare) == 0)
            {
                    strcpy(cpInfo, pclTnam);  
                    TrimRight(cpInfo);

                    dbg(DEBUG,"-----------PT: template URNO <%s> TNAM <%s> TPST <%s> -> call PR ()-----------", 
                        pclUrno, cpInfo, pclTpst);
 
                    if (bgDiagOn)
                    {
                        TrimRight(cpInfo);
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                    }

                    if (igEventType == EVT_FLIGHT_INDEPENDENT)
                    {
                        /*ilRC = ProcessIPRules(pcpHopo, pclUrno, pcpActualDate);*/
                    }
                    else 
                    {
                        int ilEvtTypeNew, ilEvtTypeSav;
                        
                        strcpy(pcgInboundUrno, "empty");
                        strcpy(pcgOutboundUrno, "empty");

                            ilRC = ProcessPrmRules(pcpHopo, pclUrno, pcpActualDate);
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS;
                    }
                    else
                    {
                        dbg(DEBUG, "PT: InboundUrno <%s> OutboundUrno <%s>",
                            pcgInboundUrno, pcgOutboundUrno);
                        if ((strncmp(pcgInboundUrno, "empty", 5) != 0) &&
                            (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                        {
                            sprintf(pclUrnoList, "%s%s%s%s", pcgInboundUrno,
                                    &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                            pclUrnoList += strlen (pclUrnoList);
                        }
                        else
                        {
                            if ((strncmp(pcgInboundUrno, "empty", 5) == 0) &&
                                (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                            {
                                sprintf(pclUrnoList, "0%s0%s%s%s", &cgSoh[0],
                                        &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                                pclUrnoList += strlen (pclUrnoList);
                            }
                            else
                            { 
                                if ((strncmp(pcgInboundUrno, "empty", 5) != 0)
                                && (strncmp(pcgOutboundUrno, "empty", 5) == 0))
                                {
                                    sprintf(pclUrnoList, "%s%s0%s0%s",
                                            pcgInboundUrno, &cgSoh[0],
                                            &cgSoh[0], &cgStx[0]);
                                    pclUrnoList += strlen (pclUrnoList);
                                }
                                else 
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG, "PT: not enough urnos");
                                }
                            }        
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
            }
        else
        {
            *pclUrnoList = 0x00 ;
            if (ilRC != RC_NOTFOUND)
            {
                dbg (TRACE, "PT: CEDAArrayGetFields failed <%d>",ilRC);
            } /* end of if */
        } /* end of if */

        llFunc = ARR_NEXT ;
    } while (ilRC == RC_SUCCESS) ;

    if (ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS ;
        dbg (DEBUG, "PT: all templates processed") ;
    } /* end of if */

    return(ilRC) ;

}


/******************************************************************************/
static int ProcessTemplates(char *pcpHopo, char *pcpActualDate,
                            char *pcpSelection)
{
    int     ilRC        = RC_SUCCESS ;             /* Return code */
    long    llFunc      = ARR_FIRST ;
    char    *pclUrnoList = NULL ;
    char    *pclResult   = NULL ; /* READ ONLY !!! */
    char    *pclTpst     = NULL ; /* READ ONLY !!! */
    char    *pclHopo     = NULL ; /* READ ONLY !!! */
    char    *pclUrno     = NULL ; /* READ ONLY !!! */
    char    *pclTnam     = NULL ; /* READ ONLY !!! */
    char    *pclAppl = NULL;
    char    cpInfo [2000] ; 
    char    clApplCompare[9];
    BOOL  blIsPrmRule = FALSE;

    sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
    dbg (DEBUG, "ProcessTemplates : pattern <%s>", rgTplArray.pcrIdx01RowBuf) ;

    llFunc = ARR_FIRST ;
    pclUrnoList = pcgUrnoListBuf ;

    if (igEventType == EVT_FLIGHT_INDEPENDENT)
        strcpy(clApplCompare,"RULE_FIR");
    else
        strcpy(clApplCompare,"RULE_AFT");

    dbg(DEBUG,"%05d:ProcessTemplates: event type %s, ruleprm modid %d",__LINE__,
       igEventType != EVT_FLIGHT_INDEPENDENT ? "EVT_FLIGHT_INDEPENDENT"  : "FLIGHT DEPENDENT" ,
         igRulprmModid);
    if  (igRulprmModid > -1 && igEventType != EVT_FLIGHT_INDEPENDENT)
    {
            SendToRulprm();  /* send flight to RULPRM also to handle PRM rules*/
    }
    do
    {
        pclResult = NULL;
        ilRC = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult);
        if (ilRC == RC_SUCCESS)
        {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
            strcpy(cpInfo, pclTnam);  
            TrimRight(cpInfo);
            blIsPrmRule = (strstr(cgPrmTemplates,cpInfo) != NULL);
          if (bgIsPrm == FALSE)
            {
                if (blIsPrmRule)
                {
                    /* not a PRM template*/
                    llFunc = ARR_NEXT ;
                    dbg(TRACE,"PRM rule for template <%s>, will not be processed",cpInfo);
                    continue;
                }
                else
                {
                    dbg(TRACE,"Not a PRM rule (template <%s>), will be processed by this program",cpInfo);
                    llFunc = ARR_NEXT ;
                }
            }
            else
            {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
            strcpy(cpInfo, pclTnam);  
            TrimRight(cpInfo);
            blIsPrmRule = (strstr(cgPrmTemplates,cpInfo) != NULL);
          if (bgIsPrm == TRUE)
            {
                if (blIsPrmRule)
                {
                    /* not a PRM template*/
                    llFunc = ARR_NEXT ;
                    dbg(TRACE,"PRM rule for template <%s>, will be processed",cpInfo);
                }
                else
                {
                    dbg(TRACE,"Not a PRM rule (template <%s>), will not be processed by this program",cpInfo);
                    llFunc = ARR_NEXT ;
                    continue;
                }
            }
            }


            pclTpst = pclResult + rgTplArray.plrArrayFieldOfs[0];
            pclHopo = pclResult + rgTplArray.plrArrayFieldOfs[1];
            pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];

            if (strcmp(pclAppl,clApplCompare) == 0)
            {
                if ((atoi(pcpSelection) == 0) ||
                    (CompareTplUrno(pclUrno,pcpSelection) == RC_SUCCESS))
                {
                    strcpy(cpInfo, pclTnam);  
                    TrimRight(cpInfo);

                    dbg(DEBUG,"-----------PT: template URNO <%s> TNAM <%s> TPST <%s> -> call PR ()-----------", 
                        pclUrno, cpInfo, pclTpst);
 
                    if (bgDiagOn)
                    {
                        TrimRight(cpInfo);
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                    }

                    if (igEventType == EVT_FLIGHT_INDEPENDENT)
                    {
                        ilRC = ProcessIPRules(pcpHopo, pclUrno, pcpActualDate);
                    }
                    else 
                    {
                        int ilEvtTypeNew, ilEvtTypeSav;
                        
                        strcpy(pcgInboundUrno, "empty");
                        strcpy(pcgOutboundUrno, "empty");

                        if (!bgIsPrm)
                        {
                            ilRC = CheckFtypForTpl ( igEventType, pclResult, &ilEvtTypeNew );
                        }
                        else
                        {
                            dbg(DEBUG,"PRM: EventType %d",igEventType);
                            ilEvtTypeNew = igEventType;
                        }
                        if ( ( ilRC == RC_SUCCESS ) && (ilEvtTypeNew!=EVT_UNKNOWN) )
                        {
                            ilEvtTypeSav = igEventType; /* Save global eventtype */
                            igEventType = ilEvtTypeNew; /* change global eventtype */
                            ilRC = ProcessRules(pcpHopo, pclUrno, pcpActualDate);
                            igEventType = ilEvtTypeSav; /* restore global eventtype */
                        }
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS;
                    }
                    else
                    {
                        dbg(DEBUG, "PT: InboundUrno <%s> OutboundUrno <%s>",
                            pcgInboundUrno, pcgOutboundUrno);
                        if ((strncmp(pcgInboundUrno, "empty", 5) != 0) &&
                            (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                        {
                            sprintf(pclUrnoList, "%s%s%s%s", pcgInboundUrno,
                                    &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                            pclUrnoList += strlen (pclUrnoList);
                        }
                        else
                        {
                            if ((strncmp(pcgInboundUrno, "empty", 5) == 0) &&
                                (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                            {
                                sprintf(pclUrnoList, "0%s0%s%s%s", &cgSoh[0],
                                        &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                                pclUrnoList += strlen (pclUrnoList);
                            }
                            else
                            { 
                                if ((strncmp(pcgInboundUrno, "empty", 5) != 0)
                                && (strncmp(pcgOutboundUrno, "empty", 5) == 0))
                                {
                                    sprintf(pclUrnoList, "%s%s0%s0%s",
                                            pcgInboundUrno, &cgSoh[0],
                                            &cgSoh[0], &cgStx[0]);
                                    pclUrnoList += strlen (pclUrnoList);
                                }
                                else 
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG, "PT: not enough urnos");
                                }
                            }        
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
            }
        }
        else
        {
            *pclUrnoList = 0x00 ;
            if (ilRC != RC_NOTFOUND)
            {
                dbg (TRACE, "PT: CEDAArrayGetFields failed <%d>",ilRC);
            } /* end of if */
        } /* end of if */

        llFunc = ARR_NEXT ;
    } while (ilRC == RC_SUCCESS) ;

    if (ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS ;
        dbg (DEBUG, "PT: all templates processed") ;
    } /* end of if */

    return(ilRC) ;

} /* end of ProcessTemplates */





/******************************************************************************/
static int MyCompare(const void* ppP1, const void* ppP2)
{
 return (strcmp((char *)ppP1,(char *)ppP2));
}/* end of MyCompare */


/******************************************************************************/
/******************************************************************************/
static int  SendFieldListToAll(char *pcpFieldList)
{
 int   ilRC        = RC_SUCCESS ;
 int   ilLoop      = 0 ;
 int   ilNoOfItems = 0 ;
 int   ilDest      = 0 ;
 char  clTmpBuf[256] ;

 ilNoOfItems = get_no_of_items(pcpFieldList);

 for(ilLoop = 1 ; ilLoop <= ilNoOfItems ; ilLoop++)
 {
  ilRC = get_real_item(&clTmpBuf[0],pcpFieldList,ilLoop);

  if (ilRC > 0)
  {
   ilDest = atoi(&clTmpBuf[0]);   
   ilRC = SendFieldListToOne(ilDest);

   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"SendFieldListToAll: SendFieldListToOne failed <%d>",ilRC);
   } /* end of if */
  }
  else
  {
   dbg (TRACE,"SendFieldListToAll: get_real_item failed <%d>",ilRC);
   ilRC = RC_FAIL;
  } /* end of if */
 } /* end of for */

 return(ilRC) ;
    
} /* end of SendFieldListToAll */


/******************************************************************************/
static int  SendFlightIndepRules(char *pcpHopo, int ipDest, char *pcpDateFrom,
                                    char *pcpDateTo)
{
    int         ilRC = RC_SUCCESS;
    long        ilCounter;  
    long        llFunc = ARR_FIRST;
    long        llRowCnt = 0;
    long        llUrnoLen = 0 ;
    long        llMaxLen = 0 ;
    char        *pclUrnoList    = NULL ;
    char        *pclResult      = NULL ; /* READ ONLY !!! */
    char        *pclRust        = NULL ; /* READ ONLY !!! */
    char        *pclRuty        = NULL ; /* READ ONLY !!! */
    char        *pclUrno        = NULL ; /* READ ONLY !!! */
    char        *pclUtpl = NULL; 
    char        *pclAppl = NULL; 
    char        *pclFisu        = NULL ; /* READ ONLY !!! */
    char        clField[8] ;
    char        cpAct[100] ; 
    char        pcpRulename [64] ; 
    int         iSendRule ; 
    char        cpActDate[32] ; 
    char        cpActDateLocal[32] ; 
    char        clBeginOfDay[DATELEN+1]; 
    char        clDateAndTemplates[512]; 
    DATEINFO    spActDate ; 
    DATEINFO    spLastDate ;
    long        llIsValid ; 
    int         iStatus=0; 
    char        cpDateFrom[32]; 
    char        cpDateTo[32]; 
    char        clKey[24]; 
    BOOL        blAllTemplates = FALSE;

    if ( !cgFirTemplates[0] )
    {   /* no templates in event, collect all ACTIVE FIR-templates */
        blAllTemplates = TRUE;
        sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
        llFunc = ARR_FIRST ;
                        
        while ( CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult) == RC_SUCCESS )
        {
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];
            if (strstr(pclAppl,"RULE_FIR") )
            {
                pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
                if ( cgFirTemplates[0] )
                    strcat ( cgFirTemplates, "," );
                strcat ( cgFirTemplates, pclUrno );
                TrimRight ( cgFirTemplates );
            }
            llFunc = ARR_NEXT ; 
        }
    }

    strcpy(cpDateFrom,pcpDateFrom);
    strcpy(cpDateTo,pcpDateTo);

    iSendRule = 0 ; 
    dbg (DEBUG,"SFIRUL: HOPO <%s>  FROM <%s>  TO  <%s>", pcpHopo, cpDateFrom, cpDateTo ) ;

    ilRC = GetFieldLength(rgRueArray.crArrayName, "URNO", &llUrnoLen) ;
    if (ilRC == RC_SUCCESS)
    {
        llUrnoLen++ ;
        dbg (DEBUG, "SFIRUL: Urno Length is <%ld>", llUrnoLen) ; 
    } /* end of if */

    if (ilRC == RC_SUCCESS)     /* create place for max. urno field list */ 
    {
        ilRC = CEDAArrayGetRowCount(&rgRueArray.rrArrayHandle,&rgRueArray.crArrayName[0],&llRowCnt);
        if (ilRC == RC_SUCCESS)
        {
            llMaxLen = llRowCnt * (llUrnoLen+3) + 1;
            dbg (DEBUG,"SFIRUL: RUETAB counter is = <%ld>", llRowCnt) ; 
        }
        else
        {
            dbg (TRACE, "SFIRUL: CEDAArrayGetRowCount failed - ruetab <%d>", ilRC) ;
        } /* end of if */
    } /* end of if */

    if ( (ilRC == RC_SUCCESS) && (lgUrnoListLen <llMaxLen) )
    {
        pcgUrnoListBuf = (char *) realloc(pcgUrnoListBuf, llMaxLen );
        if (pcgUrnoListBuf == NULL)
        {
            lgUrnoListLen = 0;
            ilRC = RC_FAIL ;
            dbg (TRACE, "SFIRUL: realloc failed - pcgUrnoListBuf");
        }
        else
        {
            lgUrnoListLen = llMaxLen ;
            dbg (DEBUG, "SFIRUL: realloc <%d> bytes for - pcgUrnoListBuf",lgUrnoListLen);
            memset(pcgUrnoListBuf,0x00,lgUrnoListLen);
        } /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        if ((cpDateFrom[0] == '\0') || (cpDateTo[0] == '\0'))
        {
            /* no date & time check */
            if ((cpDateFrom[0] == '\0') && (cpDateTo[0] == '\0'))
            {
                /* check only for this date */ 
                GetServerTimeStamp("UTC", 1, 0, cpAct);
                AddSecondsToCEDATime(cpAct, 86400*igFIRDayOffset, 1);
                strcpy (cpDateFrom, cpAct) ; 
                AddSecondsToCEDATime(cpAct, 3600*igFIRDuration, 1);
                strcpy (cpDateTo, cpAct) ;
                dbg (DEBUG, "SFIRUL: without no dates in event, calculated FROM <%s> TO <%s>",
                     cpDateFrom, cpDateTo ) ;
            }
            else 
            {
                if (cpDateTo[0] == '\0')
                {
                    dbg (DEBUG, "SFIRUL: copy ToDate from FromDate!") ;
                    strcpy (cpDateTo, cpDateFrom) ;
                }
                else 
                {
                    dbg (DEBUG, "SFIRUL: copy FromDate from ToDate!") ;
                    strcpy (cpDateFrom, cpDateTo) ;
                }
            } /* else */ 
        } /* ((cpDateFrom[0] == '\0') || (cpDateTo[0] == '\0')) */ 
        else 
        { 
            GetServerTimeStamp("UTC", 1, 0, cpAct);
            ilRC = ChkDateTimeRng (cpDateFrom, cpDateTo, cpAct) ;    
            dbg (DEBUG, "SFIRUL: cpDateFrom = <%s>", cpDateFrom) ;
            dbg (DEBUG, "SFIRUL: cpDateTo = <%s>", cpDateTo) ;
        }

        strcpy (cpActDate, cpDateFrom) ;
        ilRC = interpretDateInfo(cpActDate, &spActDate);  /* into date struct */
        if (ilRC == RC_FAIL)
        {
            dbg (DEBUG, "SFIRUL: cpActDate ERR") ; 
        }
        ilRC = interpretDateInfo (cpDateTo, &spLastDate ) ;  /* into date struct */
        if (ilRC == RC_FAIL)
        {
            dbg (DEBUG, "SFIRUL: cpLastDate ERR") ; 
        }
        /* the next sequenz is useful for the realloc from above                     */ 

        ilRC = SendAnswer(prgEvent, RC_SUCCESS)  ;    /* ack for client desktop */  
        if (ilRC != RC_SUCCESS)  
            dbg ( TRACE, "SFIRUL: SendAnswer failed RC <%d>", ilRC );

        /* if (ilRC == RC_SUCCESS)  doesn't matter, nevertheless we send the rules */
        {
            ilCounter = 0 ;
            do
            {
                strcpy ( cpActDateLocal, cpActDate );
                UtcToLocal ( cpActDateLocal );

                pcpRulename [0] = '\0' ;  /* init for CheckVality -> not important */ 
                sprintf (clKey, "%d,%d", RUE_ACTIVE, RTY_INDEPENDENT) ;
                llFunc = ARR_FIRST ;
                pclUrnoList = pcgUrnoListBuf ;
                *pclUrnoList = 0x00;
                dbg (DEBUG, "SFIRUL: ----now calculation for UTC <%s> Local <%s> --------", cpActDate, cpActDateLocal ) ;
        
                do        /* read all records for this date */ 
                {
                    pclResult = NULL ;
                    ilRC = CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),&(rgRueArray.crArrayName[0]),
                                                    &(rgRueArray.rrIdx02Handle),rgRueArray.crIdx02Name,
                                                    clKey,&llFunc,(void *) &pclResult);
                    if (ilRC == RC_SUCCESS)
                    {
                        pclRust = pclResult + rgRueArray.plrArrayFieldOfs[0] ;
                        pclUtpl = pclResult + rgRueArray.plrArrayFieldOfs[2] ;
                        pclRuty = pclResult + rgRueArray.plrArrayFieldOfs[3] ;
                        pclUrno = pclResult + rgRueArray.plrArrayFieldOfs[11] ;
                        pclFisu = pclResult + rgRueArray.plrArrayFieldOfs[14] ;   /* is used flag 0=F, 1=T */  
                        if ( CompareTplUrno(pclUtpl, cgFirTemplates) != RC_SUCCESS ) 
                        {
                            llFunc = ARR_NEXT ; 
                            continue;
                        } 

                        strcpy ( pcpRulename, pclResult + rgRueArray.plrArrayFieldOfs[18]);
        
                        /* CheckRuleCalendar(pclUrno, cpActDate, &llIsValid); */
                        CheckLocalDate(pclUrno, cpActDateLocal,  &llIsValid);
                        if (llIsValid == 1)  
                        {
                            ilCounter++ ;
                            dbg(DEBUG, "SFIRUL: rule <%s> URNO <%s> is valid at <%s>", pcpRulename, pclUrno, cpActDateLocal ); 
                            iSendRule = 1 ;      /* have read a rule */
                            sprintf (pclUrnoList, "%s%s%s%s",pclUrno,&cgSoh[0],pclFisu,&cgStx[0]) ;
                            pclUrnoList += strlen (pclUrnoList) ;
                        } /* llIsValid */
                        else 
                        { 
                            dbg(DEBUG, "SFIRUL: rule <%s> is NOT valid date <%s>", pcpRulename, cpActDateLocal ) ; 
                        } 
                    } /* end of if */ 
                    llFunc = ARR_NEXT ; 
                } while (ilRC == RC_SUCCESS) ;
        
                if (ilCounter != 0) 
                { 
                    pclUrnoList-- ;
                    *pclUrnoList = 0x00 ;
                    ilCounter = 0 ; 
                    iSendRule = 1 ;
                }
                GetBeginOfDay(cpActDate, clBeginOfDay);
                sprintf(clDateAndTemplates, "%s|", clBeginOfDay );
                if ( !blAllTemplates )
                    strcat ( clDateAndTemplates, cgFirTemplates );
                ilRC = ForwardEvent(ipDest, TRUE, clDateAndTemplates, 0);
                /* look for the next date */ 
                AddSecondsToCEDATime(cpActDate, 86400, 1);
                if (strcmp(cpActDate,cpDateTo) >= 0)
                    iStatus = 1;
                dbg (DEBUG, "SFIRUL: next date is <%s>", cpActDate) ; 
            } while (iStatus == 0) ;         /* while not all date scanned */
                
        }  /* if RC_SUCCESS */ 
                
        if (iSendRule == 0)     /* no rules found */ 
        {
            dbg (DEBUG, "SFIRUL: found no Indep rules !!!") ;
        } /* end of if */
        else         
        {    
            dbg (DEBUG, "SFIRUL: all flight independent rules processed") ;
        }

        pcgUrnoListBuf[0] = '\0' ;
        dbg (DEBUG, "SFIRUL: sending last (empty) FIR to <%d>",ipDest) ;

/*  PRF5866: moved to top of function to prevent use of inactive templates 

        if ( !cgFirTemplates[0] )
        {   /* no selection of templates -> collect all FIR-templates 

            sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
            llFunc = ARR_FIRST ;
                        
            while ( CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                            &(rgTplArray.crArrayName[0]),
                                            &(rgTplArray.rrIdx01Handle),
                                            &(rgTplArray.crIdx01Name[0]),
                                            rgTplArray.pcrIdx01RowBuf,&llFunc,
                                            (void *) &pclResult) == RC_SUCCESS )
            {
                pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];
                if (strstr(pclAppl,"RULE_FIR") )
                {
                    pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
                    if ( cgFirTemplates[0] )
                        strcat ( cgFirTemplates, "," );
                    strcat ( cgFirTemplates, pclUrno );
                    TrimRight ( cgFirTemplates );
                }
                llFunc = ARR_NEXT ; 
            }
        }
*/
        sprintf(clDateAndTemplates, "%s-%s|%s", cpDateFrom, cpActDate, cgFirTemplates);
        ilRC = ForwardEvent(ipDest, TRUE, "\0", clDateAndTemplates ) ;
                
    } /* if RC_SUCCESS */ 

    return (ilRC) ;
    
} /* end of SendFlightIndepRules */



/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
                     CMDBLK *prpCmdblk, char *pcpSelection, 
                     char *pcpFields, char *pcpData)
{
 int          ilRc ;
 int          ilLen ;
 EVENT        *prlOutEvent  = NULL ;
 BC_HEAD      *prlOutBCHead = NULL ;
 CMDBLK       *prlOutCmdblk = NULL ;

 dbg (DEBUG, "<SendToQue> ----- START -----") ;

 /* calculate size of memory we need */
 ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
         strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20 ; 

 /* get memory for out event */
 if ((prlOutEvent = (EVENT*) malloc((size_t)ilLen)) == NULL)
 {
  dbg (TRACE, "<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
  dbg (DEBUG, "<SendToQue> ----- END -----") ;
  return RC_FAIL ;
 }

 /* clear buffer */
 memset ((void*) prlOutEvent, 0x00, ilLen) ;

 /* set structure members */
 prlOutEvent->type        = SYS_EVENT ;
 prlOutEvent->command     = EVENT_DATA ;
 prlOutEvent->originator  = mod_id ;
 prlOutEvent->retry_count = 0 ;
 prlOutEvent->data_offset = sizeof(EVENT) ;
 prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

 /* BCHead members */
 prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
 memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

 /* CMDBLK members */
 prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
 memcpy (prlOutCmdblk, prpCmdblk, sizeof(CMDBLK)) ;

 /* Selection */
 strcpy (prlOutCmdblk->data, pcpSelection) ;

 /* fields */
 strcpy (prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields) ;

 /* data */
 strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData) ;

 /* send this message */
 dbg (DEBUG, "<SendToQue> sending to Mod-ID: %d", ipModID) ;
 if ((ilRc = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
 {
  dbg(TRACE, "<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc) ;
  return RC_FAIL ;
 }

 /* delete memory */
 free((void*) prlOutEvent) ;

 dbg(DEBUG, "<SendToQue> ----- END -----") ;

 /* bye bye */
 return (RC_SUCCESS) ;
} /* end of function SendToQue() */ 





/******************************************************************************/
/* used to send a acknowledge to the desktop                                  */
static int SendAnswer(EVENT *prpEvent, int ipRc)
{
  int      ilRc   = RC_SUCCESS ;         /* Return code */
    
  BC_HEAD  *prlBchead       = NULL ;
  CMDBLK   *prlCmdblk       = NULL ;
  char     *pclSelection    = NULL ;
  char     *pclFields       = NULL ;
  char     *pclData         = NULL ;

  prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT)) ;
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
  pclSelection = prlCmdblk->data ;
  pclFields    = pclSelection + strlen(pclSelection) + 1 ;
  pclData      = pclFields + strlen(pclFields) + 1 ;

  prlBchead->rc = ipRc ;

  dbg (DEBUG, "SendAnswer: %d",prpEvent->originator);
  if ((ilRc=SendToQue(prpEvent->originator, PRIORITY_4, prlBchead, prlCmdblk,
                      pclSelection, pclFields, pclData)) != RC_SUCCESS)
  {
   dbg (TRACE, "SendAnswer: %05d SendToQue returns <%d>", __LINE__, ilRc) ;
   dbg (DEBUG, "SendAnswer: ----- END -----") ;
  }
  else
  {
   dbg (DEBUG, "%05d SendToQue (%d) returns <%d>", __LINE__,
               prpEvent->originator, ilRc) ;
  }
  return ilRc ;
} /* end of function sendAnswer () */ 

#if 0
static int PutOnRulprmQueue(char *pcpAftUrno)
{
    int ilRC = 0;
    char clSelection[512];
    int sqlFunc = START;
    short slCursor = 0;
    char clSqlbuf[1024];
    char clUrno[24];
    static char clFirstFlight[4096];
    static char clSecondFlight[4096];
    BC_HEAD  rlOutBCHead;
    BC_HEAD  *prlBCHead;
    CMDBLK rlOutCmdblk;
    CMDBLK *prlCmdblk;

    memset(clFirstFlight,0,sizeof(clFirstFlight));
    memset(clSecondFlight,0,sizeof(clSecondFlight));
    memset(cgPrmUrnoList,0,sizeof(clPrmUrnoList));
    strcpy(cgPrmUrnoList,",");

    prlBCHead     = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT));
    prlCmdblk     = (CMDBLK *)  ((char *) prlBCHead->data);
    strcpy(clSelection,prlCmdblk->data);
    dbg(DEBUG,"%05d ilRC=%d, AftUrno<%s>",__LINE__,ilRC,pcpAftUrno);
    if (strcmp(prlBCHead->dest_name,"FLICOL") != 0)
    {
      if  (strcmp(prlCmdblk->tw_end,"OPSS-PM") == 0)
        {
            dbg(DEBUG,"Event not from FLICOL <%s>, but created by OPSS-PM. Will not processed",prlBCHead->dest_name);
            return RC_SUCCESS;
        }
    }
    sprintf(clSelection,"WHERE FLNU = %s AND SOUR = 'FLNO'",pcpAftUrno);
    sprintf(clSqlbuf,"SELECT %s FROM DPVTAB %s ",DPV_FIELDS,clSelection);
    dbg(DEBUG,"%05d:%s",__LINE__,clSqlbuf);
    sqlFunc = START;
    memcpy(&rlOutCmdblk,prlCmdblk,sizeof(rlOutCmdblk));
    memcpy(&rlOutBCHead,prlBCHead,sizeof(rlOutBCHead));

  dbg(TRACE,"%05d",__LINE__);
    strcpy(rlOutCmdblk.command,"PRM");
  dbg(TRACE,"%05d",__LINE__);
    while ( ( ilRC = sql_if(sqlFunc, &slCursor, clSqlbuf, clFirstFlight))== DB_SUCCESS)
    {
        static char *pclAdid = NULL;
        long llAdidLen = 12;

    dbg(TRACE,"%05d",__LINE__);

      if (pclAdid == NULL)
        {
            pclAdid = realloc(pclAdid,llAdidLen+4);
            memset(pclAdid,'\0',llAdidLen);
        }
        sqlFunc = NEXT;
        BuildItemBuffer (clFirstFlight, "", DPV_FIELDCOUNT, ",");
    dbg(TRACE,"%05d",__LINE__);
        ilRC = GetFieldData2( "DPV", "ADID",DPV_FIELDS ,clFirstFlight,&pclAdid,&llAdidLen);
                                dbg(TRACE,"%05d:",__LINE__);
    dbg(TRACE,"%05d",__LINE__);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"ADID not found in DPV record. Fields: <%s> Data: <%s>",DPV_FIELDS,clFirstFlight);
        }
        else
        {
    dbg(TRACE,"%05d",__LINE__);
            if (*pclAdid == 'A')
            {
            
                static char *pclTflu  = NULL;
                long llTfluLen = 14;
                static char *pclUrno  = NULL;
                long llUrnoLen = 14;

    dbg(TRACE,"%05d",__LINE__);
            if (pclUrno == NULL)
                {
                    pclUrno = realloc(pclUrno,llUrnoLen+4);
                    memset(pclUrno,'\0',llUrnoLen);
                }
                /* arrival flight, check for transit departure */
              ilRC = GetFieldData2( "DPV", "URNO",DPV_FIELDS ,clFirstFlight,&pclUrno,&llUrnoLen);
    dbg(TRACE,"%05d RC=%d Urno <%s>",__LINE__,ilRC,pclUrno);
                if (ilRC != RC_SUCCESS)
                {
                        dbg(TRACE,"URNO not found in DPV record. Fields: <%s> Data: <%s>",DPV_FIELDS,clFirstFlight);
                }
                else
                {
                    TrimRight(pclUrno);
                    if (atol(pclUrno) != 0L)
                    {
                        short sqlFunc = START;
                        short slCursor = 0;
                        char clSqlbuf[2048];

                        /* there is a transit flight */
                        sprintf(clSelection,"WHERE URNO = %s AND SOUR = 'TFLU'",pclTflu);
                        sprintf(clSqlbuf,"SELECT %s FROM DPVTAB %s ",DPV_FIELDS,clSelection);
    dbg(TRACE,"%05d <%s>",__LINE__,clSqlbuf);
                        ilRC = sql_if(sqlFunc, &slCursor, clSqlbuf, clSecondFlight);
    dbg(TRACE,"%05d RC from sql_if %d",__LINE__,ilRC);
                        if (ilRC == RC_NOTFOUND)
                        {
                            dbg(TRACE,"Transit Flight for URNO <%s> not found",pclUrno);
                        close_my_cursor(&slCursor);
                            continue;
                        } 
                        else if (ilRC != DB_SUCCESS )
                        {
                                /* there must be a database error */
                                get_ora_err(ilRC, cgErrMsg);
                                dbg(TRACE,"Select transit flight failed RC=%d <%s>",ilRC,cgErrMsg);
                                dbg(TRACE,"SQL: <%s>",clSqlbuf);
                        close_my_cursor(&slCursor);
                                continue;
                        }
                        else
                        {
                                dbg(TRACE,"%05d:",__LINE__);
                                BuildItemBuffer (clSecondFlight, "", DPV_FIELDCOUNT, ",");
                                dbg(TRACE,"%05d:",__LINE__);
                              close_my_cursor(&slCursor);
                            AddToPrmQueue(clFirstFlight,clSecondFlight);
                        }
                    }
                    else
                    {
                                dbg(TRACE,"%05d:",__LINE__);
                            AddToPrmQueue(clFirstFlight,"");
                    }
                }
            }
            else
            {
                static char *pclTflu;
                long llTfluLen;

            if (pclTflu == NULL)
                {
                    pclTflu = realloc(pclTflu,llTfluLen+4);
                    memset(pclTflu,'\0',llTfluLen);
                }
                /* departure flight, check for transit arrival */
              ilRC = GetFieldData2( "DPV", "TFLU",DPV_FIELDS ,clFirstFlight,&pclTflu,&llTfluLen);
    dbg(TRACE,"%05d RC from sql_if %d",__LINE__,ilRC);
                if (ilRC != RC_SUCCESS)
                {
                        dbg(TRACE,"TFLU not found in DPV record. Fields: <%s> Data: <%s>",DPV_FIELDS,clFirstFlight);
                }
                else
                {
                    if (atol(pclTflu) != 0L)
                    {
                        dbg(DEBUG,"Departure for transit arrival <%s>, already processed",pclTflu);
                        continue;
                    }
                    else
                    {
                                dbg(TRACE,"%05d:",__LINE__);
                        AddToPrmQueue("",clFirstFlight);
                    }
                }
            }
            }
        sqlFunc = NEXT;
        }
    if (igPrmBufLen > 0  && *pcgPrmBuf != '\0')
    {
    dbg(DEBUG,"SendToRulprm %d\nFields: <%s>\nData:<%s>",igRulprmModid,
         DPV_FIELDS, pcgPrmBuf);
         SendToQue(igRulprmModid, PRIORITY_3, &rlOutBCHead, 
                     &rlOutCmdblk, clSelection, DPV_FIELDS, pcgPrmBuf);
        *pcgPrmBuf = '\0';
        }
        else
        {
                char clData[124];
            dbg(DEBUG,"SendToRulprm for AFT-URNO %s, no PRM found, send PRD command to  demprm",pcpAftUrno);    
                strcpy(clData,pcpAftUrno);
                strcpy(rlOutCmdblk.command,"PRD");
                SendToQue(igDemprmModid, PRIORITY_3, &rlOutBCHead, 
                    &rlOutCmdblk, clSelection, "URNO" , clData);

        }
    if (ilRC ==  RC_NOTFOUND)
    {
        dbg(DEBUG,"RC=NOTFOUND");
    }
    close_my_cursor(&slCursor);
}
#endif

#if 0
static int  SendToRulprm()
{
  int ilRC = FALSE;
    BC_HEAD *prlBchead         = NULL;
    CMDBLK  *prlCmdblk         = NULL;
    char    *pclSelection      = NULL;
    char    *pclFields         = NULL;
    char    *pclData           = NULL;
    char *pclNewline = NULL;
    static char  clArrivalFlight[2048];
    static char  clDepartureFlight[2048];

    dbg(DEBUG,"SendToRulprm start");

    memset(cgPrmUrnoList,0,sizeof(clPrmUrnoList));
    strcpy(cgPrmUrnoList,",");

    prlBchead     = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT));
    prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data);
    pclSelection  = prlCmdblk->data;
    dbg(DEBUG,"%05d Selection:<%s>",__LINE__,pclSelection);
    pclFields     = pclSelection + strlen(pclSelection) + 1;
    dbg(DEBUG,"%05d Fields:<%s>",__LINE__,pclFields);
    pclData       = pclFields + strlen(pclFields) + 1;
    dbg(DEBUG,"%05d Data:<%s>",__LINE__,pclData);
    pclNewline = strchr(pclData,'\n');
    if (pclNewline != NULL)
    {
        *pclNewline = '\0';
        strcpy(clArrivalFlight,pclData);
        strcpy(clDepartureFlight,(pclNewline+1));
        *pclNewline = '\n';
    }
    else
    {
        strcpy(clArrivalFlight,pclData);
    }

            /* if  not the  PRM rulefinder forward the  event(s)  to the PRM rulefinder if fligth has PRM*/
    dbg(DEBUG,"%05dPRM Modid %d",__LINE__,igRulprmModid);
    if (igRulprmModid > -1)
    {
        /* send flight also to RULPRM*/
        static char *pclAftPrmc = NULL;
        static char *pclAftUrno = NULL;
        static char *pclAftAdid = NULL;
        static long llPrmcLen =  0;
        static long llUrnoLen =  0;
        static long llAdidLen = 0;

        ilRC = GetFieldData2("AFT", "ADID", pclFields,clArrivalFlight, &pclAftAdid, &llAdidLen );
        ilRC = GetFieldData2("AFT", "PRMC", pclFields,clArrivalFlight, &pclAftPrmc, &llPrmcLen );
        dbg(DEBUG,"%05d:RC=%d, <%s> %ld, ADID <%s>",__LINE__,ilRC,pclAftPrmc,llPrmcLen,pclAftAdid);
        if (ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"Fields: <%s> Data:<%s>",pclFields,pclData);
        }
        else if (*pclAftPrmc == 'Y')
        {
            ilRC = GetFieldData2("AFT", "URNO", pclFields,clArrivalFlight, &pclAftUrno, &llUrnoLen );
            dbg(DEBUG,"%05d ilRC=%d, AftUrno<%s>",__LINE__,ilRC,pclAftUrno);
            if (ilRC == RC_SUCCESS)
            {
                    PutOnRulprmQueue(pclAftUrno);
            }
        }
/* now check departure flight*/
        ilRC = GetFieldData2("AFT", "ADID", pclFields,clArrivalFlight, &pclAftAdid, &llAdidLen );
        ilRC = GetFieldData2("AFT", "PRMC", pclFields,clDepartureFlight, &pclAftPrmc, &llPrmcLen );
        dbg(DEBUG,"%05d:RC=%d PRMC=<%s>, ADID=<%s>",__LINE__,ilRC,pclAftPrmc,pclAftAdid);
        if (ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"%05d:Fields: <%s> \nData:<%s>",__LINE__,pclFields,clDepartureFlight);
        }
        else if (*pclAftPrmc == 'Y')
        {
            ilRC = GetFieldData2("AFT", "URNO", pclFields,clDepartureFlight, &pclAftUrno, &llUrnoLen );
            dbg(DEBUG,"%05d ilRC=%d, AftUrno<%s>",__LINE__,ilRC,pclAftUrno);
            if (ilRC == RC_SUCCESS)
            {
                    PutOnRulprmQueue(pclAftUrno);
            }
        }
    }
    return ilRC;
}

/******************************************************************************/
static int ProcessPrmTemplates(char *pcpHopo, char *pcpActualDate,
                            char *pcpSelection)
{
    int     ilRC        = RC_SUCCESS ;             /* Return code */
    long    llFunc      = ARR_FIRST ;
    char    *pclUrnoList = NULL ;
    char    *pclResult   = NULL ; /* READ ONLY !!! */
    char    *pclTpst     = NULL ; /* READ ONLY !!! */
    char    *pclHopo     = NULL ; /* READ ONLY !!! */
    char    *pclUrno     = NULL ; /* READ ONLY !!! */
    char    *pclTnam     = NULL ; /* READ ONLY !!! */
    char    *pclAppl = NULL;
    char    cpInfo [2000] ; 
    char    clApplCompare[9];

    sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
    dbg (DEBUG, "%05d:ProcessPRMTemplates : pattern <%s>", __LINE__,rgTplArray.pcrIdx01RowBuf) ;

    llFunc = ARR_FIRST ;
    pclUrnoList = pcgUrnoListBuf ;

    if (igEventType == EVT_FLIGHT_INDEPENDENT)
        strcpy(clApplCompare,"RULE_FIR");
    else
        strcpy(clApplCompare,"RULE_AFT");

    do
    {
        pclResult = NULL;
        ilRC = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult);
        llFunc  = ARR_NEXT;
        if (ilRC == RC_SUCCESS)
        {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
          if (bgIsPrm)
            {
                TrimRight(pclTnam);
                if (strstr(cgPrmTemplates,pclTnam) == NULL)
                {
                    /* not a PRM template*/
                    dbg(DEBUG,"%05d:not a PRM template <%s> <%s>",__LINE__,pclTnam,cgPrmTemplates);
                    llFunc = ARR_NEXT ;
                    continue;
                }
            }

            pclTpst = pclResult + rgTplArray.plrArrayFieldOfs[0];
            pclHopo = pclResult + rgTplArray.plrArrayFieldOfs[1];
            pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];

            if (strcmp(pclAppl,clApplCompare) == 0)
            {
                    strcpy(cpInfo, pclTnam);  
                    TrimRight(cpInfo);

                    dbg(DEBUG,"-----------PT: template URNO <%s> TNAM <%s> TPST <%s> -> call PR ()-----------", 
                        pclUrno, cpInfo, pclTpst);
 
                    if (bgDiagOn)
                    {
                        TrimRight(cpInfo);
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                    }

                    if (igEventType == EVT_FLIGHT_INDEPENDENT)
                    {
                        /*ilRC = ProcessIPRules(pcpHopo, pclUrno, pcpActualDate);*/
                    }
                    else 
                    {
                        int ilEvtTypeNew, ilEvtTypeSav;
                        
                        strcpy(pcgInboundUrno, "empty");
                        strcpy(pcgOutboundUrno, "empty");

                            ilRC = ProcessPrmRules(pcpHopo, pclUrno, pcpActualDate);
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS;
                    }
                    else
                    {
                        dbg(DEBUG, "PT: InboundUrno <%s> OutboundUrno <%s>",
                            pcgInboundUrno, pcgOutboundUrno);
                        if ((strncmp(pcgInboundUrno, "empty", 5) != 0) &&
                            (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                        {
                            sprintf(pclUrnoList, "%s%s%s%s", pcgInboundUrno,
                                    &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                            pclUrnoList += strlen (pclUrnoList);
                        }
                        else
                        {
                            if ((strncmp(pcgInboundUrno, "empty", 5) == 0) &&
                                (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                            {
                                sprintf(pclUrnoList, "0%s0%s%s%s", &cgSoh[0],
                                        &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                                pclUrnoList += strlen (pclUrnoList);
                            }
                            else
                            { 
                                if ((strncmp(pcgInboundUrno, "empty", 5) != 0)
                                && (strncmp(pcgOutboundUrno, "empty", 5) == 0))
                                {
                                    sprintf(pclUrnoList, "%s%s0%s0%s",
                                            pcgInboundUrno, &cgSoh[0],
                                            &cgSoh[0], &cgStx[0]);
                                    pclUrnoList += strlen (pclUrnoList);
                                }
                                else 
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG, "PT: not enough urnos");
                                }
                            }        
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
            }
        else
        {
            *pclUrnoList = 0x00 ;
            if (ilRC != RC_NOTFOUND)
            {
                dbg (TRACE, "PT: CEDAArrayGetFields failed <%d>",ilRC);
            } /* end of if */
        } /* end of if */

        llFunc = ARR_NEXT ;
    } while (ilRC == RC_SUCCESS) ;

    if (ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS ;
        dbg (DEBUG, "PT: all templates processed") ;
    } /* end of if */

    return(ilRC) ;

}


/******************************************************************************/
static int ProcessTemplates(char *pcpHopo, char *pcpActualDate,
                            char *pcpSelection)
{
    int     ilRC        = RC_SUCCESS ;             /* Return code */
    long    llFunc      = ARR_FIRST ;
    char    *pclUrnoList = NULL ;
    char    *pclResult   = NULL ; /* READ ONLY !!! */
    char    *pclTpst     = NULL ; /* READ ONLY !!! */
    char    *pclHopo     = NULL ; /* READ ONLY !!! */
    char    *pclUrno     = NULL ; /* READ ONLY !!! */
    char    *pclTnam     = NULL ; /* READ ONLY !!! */
    char    *pclAppl = NULL;
    char    cpInfo [2000] ; 
    char    clApplCompare[9];
    BOOL  blIsPrmRule = FALSE;

    sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
    dbg (DEBUG, "ProcessTemplates : pattern <%s>", rgTplArray.pcrIdx01RowBuf) ;

    llFunc = ARR_FIRST ;
    pclUrnoList = pcgUrnoListBuf ;

    if (igEventType == EVT_FLIGHT_INDEPENDENT)
        strcpy(clApplCompare,"RULE_FIR");
    else
        strcpy(clApplCompare,"RULE_AFT");

    dbg(DEBUG,"%05d:ProcessTemplates: event type %s, ruleprm modid %d",__LINE__,
       igEventType != EVT_FLIGHT_INDEPENDENT ? "EVT_FLIGHT_INDEPENDENT"  : "FLIGHT DEPENDENT" ,
         igRulprmModid);
    if  (igRulprmModid > -1 && igEventType != EVT_FLIGHT_INDEPENDENT)
    {
            SendToRulprm();  /* send flight to RULPRM also to handle PRM rules*/
    }
    do
    {
        pclResult = NULL;
        ilRC = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult);
        if (ilRC == RC_SUCCESS)
        {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
            strcpy(cpInfo, pclTnam);  
            TrimRight(cpInfo);
            blIsPrmRule = (strstr(cgPrmTemplates,cpInfo) != NULL);
          if (bgIsPrm == FALSE)
            {
                if (blIsPrmRule)
                {
                    /* not a PRM template*/
                    llFunc = ARR_NEXT ;
                    dbg(TRACE,"PRM rule for template <%s>, will not be processed",cpInfo);
                    continue;
                }
                else
                {
                    dbg(TRACE,"Not a PRM rule (template <%s>), will be processed by this program",cpInfo);
                    llFunc = ARR_NEXT ;
                }
            }
            else
            {
            pclTnam = pclResult + rgTplArray.plrArrayFieldOfs[3];
            strcpy(cpInfo, pclTnam);  
            TrimRight(cpInfo);
            blIsPrmRule = (strstr(cgPrmTemplates,cpInfo) != NULL);
          if (bgIsPrm == TRUE)
            {
                if (blIsPrmRule)
                {
                    /* not a PRM template*/
                    llFunc = ARR_NEXT ;
                    dbg(TRACE,"PRM rule for template <%s>, will be processed",cpInfo);
                }
                else
                {
                    dbg(TRACE,"Not a PRM rule (template <%s>), will not be processed by this program",cpInfo);
                    llFunc = ARR_NEXT ;
                    continue;
                }
            }
            }


            pclTpst = pclResult + rgTplArray.plrArrayFieldOfs[0];
            pclHopo = pclResult + rgTplArray.plrArrayFieldOfs[1];
            pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];

            if (strcmp(pclAppl,clApplCompare) == 0)
            {
                if ((atoi(pcpSelection) == 0) ||
                    (CompareTplUrno(pclUrno,pcpSelection) == RC_SUCCESS))
                {
                    strcpy(cpInfo, pclTnam);  
                    TrimRight(cpInfo);

                    dbg(DEBUG,"-----------PT: template URNO <%s> TNAM <%s> TPST <%s> -> call PR ()-----------", 
                        pclUrno, cpInfo, pclTpst);
 
                    if (bgDiagOn)
                    {
                        TrimRight(cpInfo);
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGTEMPLATE, cpInfo, NULL,
                                            NULL);
                    }

                    if (igEventType == EVT_FLIGHT_INDEPENDENT)
                    {
                        ilRC = ProcessIPRules(pcpHopo, pclUrno, pcpActualDate);
                    }
                    else 
                    {
                        int ilEvtTypeNew, ilEvtTypeSav;
                        
                        strcpy(pcgInboundUrno, "empty");
                        strcpy(pcgOutboundUrno, "empty");

                        if (!bgIsPrm)
                        {
                            ilRC = CheckFtypForTpl ( igEventType, pclResult, &ilEvtTypeNew );
                        }
                        else
                        {
                            dbg(DEBUG,"PRM: EventType %d",igEventType);
                            ilEvtTypeNew = igEventType;
                        }
                        if ( ( ilRC == RC_SUCCESS ) && (ilEvtTypeNew!=EVT_UNKNOWN) )
                        {
                            ilEvtTypeSav = igEventType; /* Save global eventtype */
                            igEventType = ilEvtTypeNew; /* change global eventtype */
                            ilRC = ProcessRules(pcpHopo, pclUrno, pcpActualDate);
                            igEventType = ilEvtTypeSav; /* restore global eventtype */
                        }
                    }
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS;
                    }
                    else
                    {
                        dbg(DEBUG, "PT: InboundUrno <%s> OutboundUrno <%s>",
                            pcgInboundUrno, pcgOutboundUrno);
                        if ((strncmp(pcgInboundUrno, "empty", 5) != 0) &&
                            (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                        {
                            sprintf(pclUrnoList, "%s%s%s%s", pcgInboundUrno,
                                    &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                            pclUrnoList += strlen (pclUrnoList);
                        }
                        else
                        {
                            if ((strncmp(pcgInboundUrno, "empty", 5) == 0) &&
                                (strncmp(pcgOutboundUrno, "empty", 5) != 0))
                            {
                                sprintf(pclUrnoList, "0%s0%s%s%s", &cgSoh[0],
                                        &cgSoh[0], pcgOutboundUrno, &cgStx[0]);
                                pclUrnoList += strlen (pclUrnoList);
                            }
                            else
                            { 
                                if ((strncmp(pcgInboundUrno, "empty", 5) != 0)
                                && (strncmp(pcgOutboundUrno, "empty", 5) == 0))
                                {
                                    sprintf(pclUrnoList, "%s%s0%s0%s",
                                            pcgInboundUrno, &cgSoh[0],
                                            &cgSoh[0], &cgStx[0]);
                                    pclUrnoList += strlen (pclUrnoList);
                                }
                                else 
                                {
                                    if (bgDiagOn)
                                    {
                                        if (bgDiagInt)
                                            WriteDiagFile(DIAGINT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                        if (bgDiagExt)
                                            WriteDiagFile(DIAGEXT,
                                                            DIAGNOVALIDRULE,
                                                            NULL, NULL, NULL);
                                    }
                                    dbg(DEBUG, "PT: not enough urnos");
                                }
                            }        
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
            }
        }
        else
        {
            *pclUrnoList = 0x00 ;
            if (ilRC != RC_NOTFOUND)
            {
                dbg (TRACE, "PT: CEDAArrayGetFields failed <%d>",ilRC);
            } /* end of if */
        } /* end of if */

        llFunc = ARR_NEXT ;
    } while (ilRC == RC_SUCCESS) ;

    if (ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS ;
        dbg (DEBUG, "PT: all templates processed") ;
    } /* end of if */

    return(ilRC) ;

} /* end of ProcessTemplates */





/******************************************************************************/
static int MyCompare(const void* ppP1, const void* ppP2)
{
 return (strcmp((char *)ppP1,(char *)ppP2));
}/* end of MyCompare */


/******************************************************************************/
/******************************************************************************/
static int  SendFieldListToAll(char *pcpFieldList)
{
 int   ilRC        = RC_SUCCESS ;
 int   ilLoop      = 0 ;
 int   ilNoOfItems = 0 ;
 int   ilDest      = 0 ;
 char  clTmpBuf[256] ;

 ilNoOfItems = get_no_of_items(pcpFieldList);

 for(ilLoop = 1 ; ilLoop <= ilNoOfItems ; ilLoop++)
 {
  ilRC = get_real_item(&clTmpBuf[0],pcpFieldList,ilLoop);

  if (ilRC > 0)
  {
   ilDest = atoi(&clTmpBuf[0]);   
   ilRC = SendFieldListToOne(ilDest);

   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"SendFieldListToAll: SendFieldListToOne failed <%d>",ilRC);
   } /* end of if */
  }
  else
  {
   dbg (TRACE,"SendFieldListToAll: get_real_item failed <%d>",ilRC);
   ilRC = RC_FAIL;
  } /* end of if */
 } /* end of for */

 return(ilRC) ;
    
} /* end of SendFieldListToAll */


/******************************************************************************/
static int  SendFlightIndepRules(char *pcpHopo, int ipDest, char *pcpDateFrom,
                                    char *pcpDateTo)
{
    int         ilRC = RC_SUCCESS;
    long        ilCounter;  
    long        llFunc = ARR_FIRST;
    long        llRowCnt = 0;
    long        llUrnoLen = 0 ;
    long        llMaxLen = 0 ;
    char        *pclUrnoList    = NULL ;
    char        *pclResult      = NULL ; /* READ ONLY !!! */
    char        *pclRust        = NULL ; /* READ ONLY !!! */
    char        *pclRuty        = NULL ; /* READ ONLY !!! */
    char        *pclUrno        = NULL ; /* READ ONLY !!! */
    char        *pclUtpl = NULL; 
    char        *pclAppl = NULL; 
    char        *pclFisu        = NULL ; /* READ ONLY !!! */
    char        clField[8] ;
    char        cpAct[100] ; 
    char        pcpRulename [64] ; 
    int         iSendRule ; 
    char        cpActDate[32] ; 
    char        cpActDateLocal[32] ; 
    char        clBeginOfDay[DATELEN+1]; 
    char        clDateAndTemplates[512]; 
    DATEINFO    spActDate ; 
    DATEINFO    spLastDate ;
    long        llIsValid ; 
    int         iStatus=0; 
    char        cpDateFrom[32]; 
    char        cpDateTo[32]; 
    char        clKey[24]; 
    BOOL        blAllTemplates = FALSE;

    if ( !cgFirTemplates[0] )
    {   /* no templates in event, collect all ACTIVE FIR-templates */
        blAllTemplates = TRUE;
        sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
        llFunc = ARR_FIRST ;
                        
        while ( CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llFunc,
                                        (void *) &pclResult) == RC_SUCCESS )
        {
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];
            if (strstr(pclAppl,"RULE_FIR") )
            {
                pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
                if ( cgFirTemplates[0] )
                    strcat ( cgFirTemplates, "," );
                strcat ( cgFirTemplates, pclUrno );
                TrimRight ( cgFirTemplates );
            }
            llFunc = ARR_NEXT ; 
        }
    }

    strcpy(cpDateFrom,pcpDateFrom);
    strcpy(cpDateTo,pcpDateTo);

    iSendRule = 0 ; 
    dbg (DEBUG,"SFIRUL: HOPO <%s>  FROM <%s>  TO  <%s>", pcpHopo, cpDateFrom, cpDateTo ) ;

    ilRC = GetFieldLength(rgRueArray.crArrayName, "URNO", &llUrnoLen) ;
    if (ilRC == RC_SUCCESS)
    {
        llUrnoLen++ ;
        dbg (DEBUG, "SFIRUL: Urno Length is <%ld>", llUrnoLen) ; 
    } /* end of if */

    if (ilRC == RC_SUCCESS)     /* create place for max. urno field list */ 
    {
        ilRC = CEDAArrayGetRowCount(&rgRueArray.rrArrayHandle,&rgRueArray.crArrayName[0],&llRowCnt);
        if (ilRC == RC_SUCCESS)
        {
            llMaxLen = llRowCnt * (llUrnoLen+3) + 1;
            dbg (DEBUG,"SFIRUL: RUETAB counter is = <%ld>", llRowCnt) ; 
        }
        else
        {
            dbg (TRACE, "SFIRUL: CEDAArrayGetRowCount failed - ruetab <%d>", ilRC) ;
        } /* end of if */
    } /* end of if */

    if ( (ilRC == RC_SUCCESS) && (lgUrnoListLen <llMaxLen) )
    {
        pcgUrnoListBuf = (char *) realloc(pcgUrnoListBuf, llMaxLen );
        if (pcgUrnoListBuf == NULL)
        {
            lgUrnoListLen = 0;
            ilRC = RC_FAIL ;
            dbg (TRACE, "SFIRUL: realloc failed - pcgUrnoListBuf");
        }
        else
        {
            lgUrnoListLen = llMaxLen ;
            dbg (DEBUG, "SFIRUL: realloc <%d> bytes for - pcgUrnoListBuf",lgUrnoListLen);
            memset(pcgUrnoListBuf,0x00,lgUrnoListLen);
        } /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        if ((cpDateFrom[0] == '\0') || (cpDateTo[0] == '\0'))
        {
            /* no date & time check */
            if ((cpDateFrom[0] == '\0') && (cpDateTo[0] == '\0'))
            {
                /* check only for this date */ 
                GetServerTimeStamp("UTC", 1, 0, cpAct);
                AddSecondsToCEDATime(cpAct, 86400*igFIRDayOffset, 1);
                strcpy (cpDateFrom, cpAct) ; 
                AddSecondsToCEDATime(cpAct, 3600*igFIRDuration, 1);
                strcpy (cpDateTo, cpAct) ;
                dbg (DEBUG, "SFIRUL: without no dates in event, calculated FROM <%s> TO <%s>",
                     cpDateFrom, cpDateTo ) ;
            }
            else 
            {
                if (cpDateTo[0] == '\0')
                {
                    dbg (DEBUG, "SFIRUL: copy ToDate from FromDate!") ;
                    strcpy (cpDateTo, cpDateFrom) ;
                }
                else 
                {
                    dbg (DEBUG, "SFIRUL: copy FromDate from ToDate!") ;
                    strcpy (cpDateFrom, cpDateTo) ;
                }
            } /* else */ 
        } /* ((cpDateFrom[0] == '\0') || (cpDateTo[0] == '\0')) */ 
        else 
        { 
            GetServerTimeStamp("UTC", 1, 0, cpAct);
            ilRC = ChkDateTimeRng (cpDateFrom, cpDateTo, cpAct) ;    
            dbg (DEBUG, "SFIRUL: cpDateFrom = <%s>", cpDateFrom) ;
            dbg (DEBUG, "SFIRUL: cpDateTo = <%s>", cpDateTo) ;
        }

        strcpy (cpActDate, cpDateFrom) ;
        ilRC = interpretDateInfo(cpActDate, &spActDate);  /* into date struct */
        if (ilRC == RC_FAIL)
        {
            dbg (DEBUG, "SFIRUL: cpActDate ERR") ; 
        }
        ilRC = interpretDateInfo (cpDateTo, &spLastDate ) ;  /* into date struct */
        if (ilRC == RC_FAIL)
        {
            dbg (DEBUG, "SFIRUL: cpLastDate ERR") ; 
        }
        /* the next sequenz is useful for the realloc from above                     */ 

        ilRC = SendAnswer(prgEvent, RC_SUCCESS)  ;    /* ack for client desktop */  
        if (ilRC != RC_SUCCESS)  
            dbg ( TRACE, "SFIRUL: SendAnswer failed RC <%d>", ilRC );

        /* if (ilRC == RC_SUCCESS)  doesn't matter, nevertheless we send the rules */
        {
            ilCounter = 0 ;
            do
            {
                strcpy ( cpActDateLocal, cpActDate );
                UtcToLocal ( cpActDateLocal );

                pcpRulename [0] = '\0' ;  /* init for CheckVality -> not important */ 
                sprintf (clKey, "%d,%d", RUE_ACTIVE, RTY_INDEPENDENT) ;
                llFunc = ARR_FIRST ;
                pclUrnoList = pcgUrnoListBuf ;
                *pclUrnoList = 0x00;
                dbg (DEBUG, "SFIRUL: ----now calculation for UTC <%s> Local <%s> --------", cpActDate, cpActDateLocal ) ;
        
                do        /* read all records for this date */ 
                {
                    pclResult = NULL ;
                    ilRC = CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),&(rgRueArray.crArrayName[0]),
                                                    &(rgRueArray.rrIdx02Handle),rgRueArray.crIdx02Name,
                                                    clKey,&llFunc,(void *) &pclResult);
                    if (ilRC == RC_SUCCESS)
                    {
                        pclRust = pclResult + rgRueArray.plrArrayFieldOfs[0] ;
                        pclUtpl = pclResult + rgRueArray.plrArrayFieldOfs[2] ;
                        pclRuty = pclResult + rgRueArray.plrArrayFieldOfs[3] ;
                        pclUrno = pclResult + rgRueArray.plrArrayFieldOfs[11] ;
                        pclFisu = pclResult + rgRueArray.plrArrayFieldOfs[14] ;   /* is used flag 0=F, 1=T */  
                        if ( CompareTplUrno(pclUtpl, cgFirTemplates) != RC_SUCCESS ) 
                        {
                            llFunc = ARR_NEXT ; 
                            continue;
                        } 

                        strcpy ( pcpRulename, pclResult + rgRueArray.plrArrayFieldOfs[18]);
        
                        /* CheckRuleCalendar(pclUrno, cpActDate, &llIsValid); */
                        CheckLocalDate(pclUrno, cpActDateLocal,  &llIsValid);
                        if (llIsValid == 1)  
                        {
                            ilCounter++ ;
                            dbg(DEBUG, "SFIRUL: rule <%s> URNO <%s> is valid at <%s>", pcpRulename, pclUrno, cpActDateLocal ); 
                            iSendRule = 1 ;      /* have read a rule */
                            sprintf (pclUrnoList, "%s%s%s%s",pclUrno,&cgSoh[0],pclFisu,&cgStx[0]) ;
                            pclUrnoList += strlen (pclUrnoList) ;
                        } /* llIsValid */
                        else 
                        { 
                            dbg(DEBUG, "SFIRUL: rule <%s> is NOT valid date <%s>", pcpRulename, cpActDateLocal ) ; 
                        } 
                    } /* end of if */ 
                    llFunc = ARR_NEXT ; 
                } while (ilRC == RC_SUCCESS) ;
        
                if (ilCounter != 0) 
                { 
                    pclUrnoList-- ;
                    *pclUrnoList = 0x00 ;
                    ilCounter = 0 ; 
                    iSendRule = 1 ;
                }
                GetBeginOfDay(cpActDate, clBeginOfDay);
                sprintf(clDateAndTemplates, "%s|", clBeginOfDay );
                if ( !blAllTemplates )
                    strcat ( clDateAndTemplates, cgFirTemplates );
                ilRC = ForwardEvent(ipDest, TRUE, clDateAndTemplates, 0);
                /* look for the next date */ 
                AddSecondsToCEDATime(cpActDate, 86400, 1);
                if (strcmp(cpActDate,cpDateTo) >= 0)
                    iStatus = 1;
                dbg (DEBUG, "SFIRUL: next date is <%s>", cpActDate) ; 
            } while (iStatus == 0) ;         /* while not all date scanned */
                
        }  /* if RC_SUCCESS */ 
                
        if (iSendRule == 0)     /* no rules found */ 
        {
            dbg (DEBUG, "SFIRUL: found no Indep rules !!!") ;
        } /* end of if */
        else         
        {    
            dbg (DEBUG, "SFIRUL: all flight independent rules processed") ;
        }

        pcgUrnoListBuf[0] = '\0' ;
        dbg (DEBUG, "SFIRUL: sending last (empty) FIR to <%d>",ipDest) ;

/*  PRF5866: moved to top of function to prevent use of inactive templates 

        if ( !cgFirTemplates[0] )
        {   /* no selection of templates -> collect all FIR-templates 

            sprintf (rgTplArray.pcrIdx01RowBuf, "%d,%s", TPL_ACTIVE, pcpHopo) ;
            llFunc = ARR_FIRST ;
                        
            while ( CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                            &(rgTplArray.crArrayName[0]),
                                            &(rgTplArray.rrIdx01Handle),
                                            &(rgTplArray.crIdx01Name[0]),
                                            rgTplArray.pcrIdx01RowBuf,&llFunc,
                                            (void *) &pclResult) == RC_SUCCESS )
            {
                pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];
                if (strstr(pclAppl,"RULE_FIR") )
                {
                    pclUrno = pclResult + rgTplArray.plrArrayFieldOfs[2];
                    if ( cgFirTemplates[0] )
                        strcat ( cgFirTemplates, "," );
                    strcat ( cgFirTemplates, pclUrno );
                    TrimRight ( cgFirTemplates );
                }
                llFunc = ARR_NEXT ; 
            }
        }
*/
        sprintf(clDateAndTemplates, "%s-%s|%s", cpDateFrom, cpActDate, cgFirTemplates);
        ilRC = ForwardEvent(ipDest, TRUE, "\0", clDateAndTemplates ) ;
                
    } /* if RC_SUCCESS */ 

    return (ilRC) ;
    
} /* end of SendFlightIndepRules */



/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
                     CMDBLK *prpCmdblk, char *pcpSelection, 
                     char *pcpFields, char *pcpData)
{
 int          ilRc ;
 int          ilLen ;
 EVENT        *prlOutEvent  = NULL ;
 BC_HEAD      *prlOutBCHead = NULL ;
 CMDBLK       *prlOutCmdblk = NULL ;

 dbg (DEBUG, "<SendToQue> ----- START -----") ;

 /* calculate size of memory we need */
 ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
         strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20 ; 

 /* get memory for out event */
 if ((prlOutEvent = (EVENT*) malloc((size_t)ilLen)) == NULL)
 {
  dbg (TRACE, "<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
  dbg (DEBUG, "<SendToQue> ----- END -----") ;
  return RC_FAIL ;
 }

 /* clear buffer */
 memset ((void*) prlOutEvent, 0x00, ilLen) ;

 /* set structure members */
 prlOutEvent->type        = SYS_EVENT ;
 prlOutEvent->command     = EVENT_DATA ;
 prlOutEvent->originator  = mod_id ;
 prlOutEvent->retry_count = 0 ;
 prlOutEvent->data_offset = sizeof(EVENT) ;
 prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

 /* BCHead members */
 prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
 memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

 /* CMDBLK members */
 prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
 memcpy (prlOutCmdblk, prpCmdblk, sizeof(CMDBLK)) ;

 /* Selection */
 strcpy (prlOutCmdblk->data, pcpSelection) ;

 /* fields */
 strcpy (prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields) ;

 /* data */
 strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData) ;

 /* send this message */
 dbg (DEBUG, "<SendToQue> sending to Mod-ID: %d", ipModID) ;
 if ((ilRc = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
 {
  dbg(TRACE, "<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc) ;
  return RC_FAIL ;
 }

 /* delete memory */
 free((void*) prlOutEvent) ;

 dbg(DEBUG, "<SendToQue> ----- END -----") ;

 /* bye bye */
 return (RC_SUCCESS) ;
} /* end of function SendToQue() */ 





/******************************************************************************/
/* used to send a acknowledge to the desktop                                  */
static int SendAnswer(EVENT *prpEvent, int ipRc)
{
  int      ilRc   = RC_SUCCESS ;         /* Return code */
    
  BC_HEAD  *prlBchead       = NULL ;
  CMDBLK   *prlCmdblk       = NULL ;
  char     *pclSelection    = NULL ;
  char     *pclFields       = NULL ;
  char     *pclData         = NULL ;

  prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT)) ;
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
  pclSelection = prlCmdblk->data ;
  pclFields    = pclSelection + strlen(pclSelection) + 1 ;
  pclData      = pclFields + strlen(pclFields) + 1 ;

  prlBchead->rc = ipRc ;

  dbg (DEBUG, "SendAnswer: %d",prpEvent->originator);
  if ((ilRc=SendToQue(prpEvent->originator, PRIORITY_4, prlBchead, prlCmdblk,
                      pclSelection, pclFields, pclData)) != RC_SUCCESS)
  {
   dbg (TRACE, "SendAnswer: %05d SendToQue returns <%d>", __LINE__, ilRc) ;
   dbg (DEBUG, "SendAnswer: ----- END -----") ;
  }
  else
  {
   dbg (DEBUG, "%05d SendToQue (%d) returns <%d>", __LINE__,
               prpEvent->originator, ilRc) ;
  }
  return ilRc ;
} /* end of function sendAnswer () */ 



#endif
/******************************************************************************/
static int FieldCompare(char *pcpTableName, char *pcpFldNam1, char *pcpFldVal1, char *pcpFieldList, char *pcpData, int ipFunc)
{
 int    ilRC        = RC_SUCCESS ;     /* Return code */
 static char  *pclFldVal2  = NULL ;
 static long llBufLen=0;

 ilRC = GetFieldData2 (pcpTableName, pcpFldNam1, pcpFieldList, pcpData, &pclFldVal2, &llBufLen) ;
 if (ilRC == RC_SUCCESS)
 {
  ilRC = strcmp (pcpFldVal1,pclFldVal2) ;
  dbg(DEBUG, "FCMP: <%s> <%s> <%d>",pcpFldVal1,pclFldVal2,ilRC) ;

  switch (ipFunc)
  {
   case CMP_SMALLER :   if (ilRC < 0)
                        {
                         ilRC = ipFunc ;
            }
                        else
                        {
                         ilRC = RC_IGNORE ;
                        } /* end of if */
                     break ;

   case CMP_EQUAL :     if(ilRC == 0)
                        {
                         ilRC = ipFunc ;
            }
                        else
                        {
                         ilRC = RC_IGNORE ;
            }/* end of if */
                     break;

   case CMP_GREATER :   if (ilRC > 0)
                        {
                         ilRC = ipFunc ;
                        }
                        else
                        {
                         ilRC = RC_IGNORE ;
                        } /* end of if */
                       break ;

   default :            dbg(TRACE, "FCMP: unknown function <%d>", ipFunc) ;
                        ilRC = RC_FAIL ;
                       break ;
   } /* end of switch */
  }
  else
  {
   dbg (TRACE, "FCMP: GetFieldData2 failed" ) ;
  } /* end of if */

  /*
  if (pclFldVal2 != NULL)
  {
   free (pclFldVal2) ;
   pclFldVal2 = NULL ;
  } */

  return (ilRC) ;
    
} /* end of FieldCompare */


static int GetPrmEventTypeOld(int *plpEventType,  char **pcpInbound, char **pcpOutbound,
        long *plpInbBuffLen, long *plpOutbBuffLen,char *pcpFields,char *pcpData)
{
 int     ilRC    = RC_SUCCESS ;         /* Return code */
 char    clDel   = ',' ;
 char   *pclTmp  = NULL ;
 long    llLen;
 static char *pclAdid = NULL;
 static long llAdidLen =  0;


    ilRC = GetFieldData2( "DPV", "ADID", pcpFields,pcpData,&pclAdid,&llAdidLen);
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"%05d \nFields:<%s>\nData:<%s>:ADID not found",__LINE__,pcpFields,pcpData);
        return (ilRC);
    }
    if (*pclAdid == 'A')
    {
    llLen = strlen (pcpData) + 1;
        if ( *plpInbBuffLen < llLen )
        *pcpInbound = (char*)realloc ( *pcpInbound, llLen );
        if (*pcpInbound == NULL)
        {
         dbg (TRACE, "GetEventType: realloc failed") ; 
         ilRC = RC_FAIL ;
        } /* end of if */
        else 
        {
        *plpInbBuffLen = llLen;
        strcpy (*pcpInbound, pcpData) ;  /* copy str. for 1st part (prev.\n) of string */ 
    *plpEventType = EVT_INBOUND ;
        }
    }
    else
    {
   llLen = strlen (pcpData) + 1;
                                    dbg(DEBUG,"%05d:llLen %d,%d",__LINE__,llLen,*plpOutbBuffLen);
   if ( *plpOutbBuffLen < llLen )
    *pcpOutbound = (char*)realloc ( *pcpOutbound, llLen );
                                    dbg(DEBUG,"%05d %x Len %d:",__LINE__,*pcpOutbound,llLen);

   if (*pcpOutbound == NULL)
   {
    dbg (TRACE, "GetEventType: realloc failed") ;
    ilRC = RC_FAIL ;
   } /* end of if*/
   else
   {
    *plpOutbBuffLen = llLen;
    strcpy ( *pcpOutbound,pcpData) ;  /* copy str. for 1st part (prev.\n) of string */ 
   *plpEventType = EVT_OUTBOUND ;
  }
  } /* end of if*/

 dbg (DEBUG,"%05d:GetEventType:  inbound <%x><%s>",__LINE__, pcpInbound, *pcpInbound) ; 
 dbg (DEBUG,"%05d:GetEventType: outbound <%x><%s>",__LINE__, pcpOutbound, *pcpOutbound) ;

 return (ilRC) ;
}


static int GetPrmEventType(int *plpEventType,  char **pcpInbound, char **pcpOutbound,
        long *plpInbBuffLen, long *plpOutbBuffLen,char *pcpFields,char *pcpData)
{
 int     ilRC    = RC_SUCCESS ;         /* Return code */
 char    clDel   = ',' ;
 char   *pclTmp  = NULL ;
 long    llLen;
 static char *pclAdid = NULL;
 static long llAdidLen =  0;
    static char *pclInbound;
    static char *pclOutbound;

    pclOutbound = strchr(pcpData,*cgEtx);
    if (pclOutbound == NULL)
    {
        dbg(TRACE,"Invalid DPV-Data, no outbound <%s>",pcpData);
        return RC_FAIL;
    }
    *pclOutbound = '\0';
    pclOutbound++;
    pclInbound = pcpData;

#if 0
    ilRC = GetFieldData2( "DPV", "ADID", pcpFields,pcpInbound,&pclAdid,&llAdidLen);
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"%05d \nFields:<%s>\nData:<%s>:ADID not found",__LINE__,pcpFields,pcpInbound);
        return (ilRC);
    }
    if (*pclAdid == 'A')
    {
    llLen = strlen (pcpData) + 1;
        if ( *plpInbBuffLen < llLen )
        *pcpInbound = (char*)realloc ( *pcpInbound, llLen );
        if (*pcpInbound == NULL)
        {
         dbg (TRACE, "GetEventType: realloc failed") ; 
         ilRC = RC_FAIL ;
        } /* end of if */
        else 
        {
        *plpInbBuffLen = llLen;
        strcpy (*pcpInbound, pcpData) ;  /* copy str. for 1st part (prev.\n) of string */ 
    *plpEventType = EVT_INBOUND ;
        }
    }
    else
    {
   llLen = strlen (pcpData) + 1;
                                    dbg(DEBUG,"%05d:llLen %d,%d",__LINE__,llLen,*plpOutbBuffLen);
   if ( *plpOutbBuffLen < llLen )
    *pcpOutbound = (char*)realloc ( *pcpOutbound, llLen );
                                    dbg(DEBUG,"%05d %x Len %d:",__LINE__,*pcpOutbound,llLen);

   if (*pcpOutbound == NULL)
   {
    dbg (TRACE, "GetEventType: realloc failed") ;
    ilRC = RC_FAIL ;
   } /* end of if*/
   else
   {
    *plpOutbBuffLen = llLen;
    strcpy ( *pcpOutbound,pcpData) ;  /* copy str. for 1st part (prev.\n) of string */ 
   *plpEventType = EVT_OUTBOUND ;
  }
  } /* end of if*/

 dbg (DEBUG,"%05d:GetEventType:  inbound <%x><%s>",__LINE__, pcpInbound, *pcpInbound) ; 
 dbg (DEBUG,"%05d:GetEventType: outbound <%x><%s>",__LINE__, pcpOutbound, *pcpOutbound) ;

#endif
 return (ilRC) ;
}

/******************************************************************************/
static int GetEventType(int *plpEventType, char **pcpInbound, char **pcpOutbound, 
                        long *plpInbBuffLen, long *plpOutbBuffLen, char *pcpData)
{
 int     ilRC    = RC_SUCCESS ;         /* Return code */
 char    clDel   = '\n' ;
 char   *pclTmp  = NULL ;
 long    llLen;

 if (bgIsPrm)
 {
     clDel   = *cgEtx;
 }
 pclTmp = strchr (pcpData, clDel) ;
 if (pclTmp != NULL)        /* found clDel = \n in the string */ 
 { 
  *pclTmp = 0x00 ;
  llLen = strlen (pcpData) + 1;
  if ( *plpInbBuffLen < llLen )
    *pcpInbound = (char*)realloc ( *pcpInbound, llLen );
  if (*pcpInbound == NULL)
  {
   dbg (TRACE, "GetEventType: realloc failed") ; 
   ilRC = RC_FAIL ;
  } /* end of if */
  else 
  {
    *plpInbBuffLen = llLen;
    strcpy (*pcpInbound, pcpData) ;  /* copy str. for 1st part (prev.\n) of string */ 
  }
  *pclTmp = clDel ;
 }
 else
 {
  dbg (TRACE, "GetEventType: strchr <0x%x> not found in <%s>", clDel, pcpData) ;
  ilRC = RC_FAIL ;
 } /* end of if*/

 if (ilRC == RC_SUCCESS)
 {
  pclTmp = strrchr (pcpData, clDel) ;
  if (pclTmp != NULL)
  {
   pclTmp++;
   llLen = strlen (pclTmp) + 1;
   if ( *plpOutbBuffLen < llLen )
    *pcpOutbound = (char*)realloc ( *pcpOutbound, llLen );

   if (*pcpOutbound == NULL)
   {
    dbg (TRACE, "GetEventType: realloc failed") ;
    ilRC = RC_FAIL ;
   } /* end of if*/
   else
   {
    *plpOutbBuffLen = llLen;
    strcpy ( *pcpOutbound,pclTmp) ;  /* copy str. for 1st part (prev.\n) of string */ 
  }

  } /* end of if*/
 } /* end of if*/

 dbg (DEBUG,"GetEventType:  inbound <%x><%s>", pcpInbound, *pcpInbound) ; 
 dbg (DEBUG,"GetEventType: outbound <%x><%s>", pcpOutbound, *pcpOutbound) ;
 *plpEventType = EVT_UNKNOWN ;

 if (strlen(*pcpInbound) == 0)
 {
  if (strlen(*pcpOutbound) == 0)
  {
   dbg (TRACE, "GetEventType: inbound and outbound empty") ;    
   *plpEventType = EVT_UNKNOWN ;
   ilRC = RC_FAIL ;
  }
  else 
  {
   *plpEventType = EVT_OUTBOUND ;
   ilRC = RC_SUCCESS ;
  } /* end of if */
 }
 else
 {
  if (strlen(*pcpOutbound) == 0)
  {
   *plpEventType = EVT_INBOUND ;
   ilRC = RC_SUCCESS ;
  }
  else
  {
   *plpEventType = EVT_TURNAROUND ;
   ilRC = RC_SUCCESS ;
  } /* end of if */
 } /* end of if */

 return (ilRC) ;
    
} /* end of GetEventType */





/******************************************************************************/
static int GetCedaTimestamp(char *pcpResultBuf,int ipResultLen, long lpTimeOffset)
{
 int         ilRC      = RC_SUCCESS ;      /* Return code */
 int         ilTimeLen = 0 ;
 time_t      llCurTime = 0 ;
 struct tm  *prlLocalTimeResult ;
 struct tm   rlTimeStruct ;
 char        clTimeStamp[16];

 if (ilRC == RC_SUCCESS)
 {
  if (pcpResultBuf == NULL)
  {
   dbg (TRACE, "GetCedaTimestamp: Result <%s>",clTimeStamp) ; 
   dbg (TRACE, "GetCedaTimestamp: invalid 1.st argument <0x%8.8x>",pcpResultBuf) ; 
   ilRC = RC_FAIL ;
  }  
 } 

 if (ilRC == RC_SUCCESS)
 {
  if (ipResultLen < 15)
  {
   dbg( TRACE, "GetCedaTimestamp: invalid 2.st argument <%d>",ipResultLen);
   dbg( TRACE,"GetCedaTimestamp: minimum of 15 characters required!");
   ilRC = RC_FAIL ;
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  memset ((void*) &clTimeStamp[0], 0x00, 16) ;
  llCurTime = time(0L) ;
  llCurTime += lpTimeOffset ;
  prlLocalTimeResult = (struct tm *) localtime (&llCurTime) ;
  memcpy ((char *) &rlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm)) ;
  ilTimeLen = strftime(clTimeStamp,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTimeStruct) ;
  if (ilTimeLen <= ipResultLen)
  {
    LocalToUtc ( clTimeStamp );
    strcpy (pcpResultBuf, clTimeStamp) ;
  } /* end of if */
 } /* end of if */

 return ilRC ;

} /* end of GetCedaTimestamp */





/********************************************************************************/
/* returns the number of a command like input => 'XYZ', output = n (n | 0..n+1) */ 
/********************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
 int  ilRC   = RC_SUCCESS ;   /* Return code */
 int  ilLoop = 0 ;
 char clCommand [8] ;
    
 memset (&clCommand[0], 0x00, 8) ;

 ilRC = get_real_item (&clCommand[0], pcpCommand, 1) ;
 if (ilRC > 0)
 {
  ilRC = RC_FAIL ;
 } /* end of if */

 while ((ilRC != 0) && (prgCmdTxt[ilLoop] != NULL))
 {
  ilRC = strcmp(&clCommand[0], prgCmdTxt[ilLoop]) ;
  ilLoop++ ;
 } /* end of while */

 if (ilRC == 0)
 {
  ilLoop-- ;
  if (igTestOutput > 1) 
  {
   dbg (DEBUG, "GetCommand: <%s> <%d>", prgCmdTxt[ilLoop], rgCmdDef[ilLoop]) ;
  }
  *pipCmd = rgCmdDef[ilLoop] ;
 }
 else
 {
  dbg (TRACE, "GetCommand: <%s> is not valid", &clCommand[0]) ;
  ilRC = RC_FAIL ;
 } /* end of if */

 return (ilRC) ;
    
} /* end of GetCommand */



static int AddToPrmEventBuf(char *pcpLine,char *pcpRules)
{
     int ilNewPrmEventBufLen =  igPrmEventBufLen + strlen(pcpLine) + strlen(pcpRules) + 20;

     if (pcgPrmEventBuf == NULL)
     {
          pcgPrmEventBuf = calloc(sizeof(char),ilNewPrmEventBufLen);
     }
    else
    {
   pcgPrmEventBuf = realloc(pcgPrmEventBuf,ilNewPrmEventBufLen);
     }
     if (pcgPrmEventBuf == NULL)
     {
            dbg(TRACE,"Unable to  allocate %d Bytesfor PRM Event buffer",ilNewPrmEventBufLen);
            Terminate(-1);
     }
     igPrmEventBufLen = ilNewPrmEventBufLen;
     strcat(pcgPrmEventBuf,pcpLine);
     strcat(pcgPrmEventBuf,cgDc1);
     strcat(pcgPrmEventBuf,pcpRules);
     strcat(pcgPrmEventBuf,"\n");
     dbg(DEBUG,"AddToPrmEvent: Line <%s> Rules <%s> \nPrmEventBuf is now: \n<%s>",pcpLine,pcpRules,pcgPrmEventBuf);
}


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData (EVENT *prpEvent)
{
    int       ilRC              = RC_SUCCESS;   /* Return code */   
    int       ilInboundFtyp     = RC_SUCCESS;   /* Return code */   
    int       ilOutboundFtyp    = RC_SUCCESS;   /* Return code */
    int       ilCmd             = 0;
    int       ilRoutingFlag     = 0; 
    int       ilOldSaveEvent    = FALSE;
    int     ilWorkAgain       = RC_SUCCESS; 
    char    clDateFrom[20];
    char    clDateTo[20];
    BC_HEAD *prlBchead         = NULL;
    CMDBLK  *prlCmdblk         = NULL;
    char    *pclSelection      = NULL;
    char    *pclFields         = NULL;
    char    *pclData           = NULL;
    char    clHopo[8];
    char    clActualDate[16];
    long    llActSize;
    char    *pclSgrUrno=0, *pclSgrGrpn=0;
    int     ilChanged, ilOldDebugLevel;
    char    clSelection[1024];
    char    *pclPipe = NULL;
    char    clFlightName[32];
    char   *pclLine;
    static char *pclAftAlc=0;
    static long llAlcLen = 0;
    static long llFltnLen = 0;
    static char *pclAftFltn=0;
    static char *pclFtypIn=0;
    static char *pclFtypOut=0;
    static long llFtypInLen = 0;
    static long llFtypOutLen = 0;
    int         ilLastID = -1;

debug_level =  DEBUG;

    dbg (TRACE,"============= START <%10.10d> of HandleData =============", lgEvtCnt );         
    if ( prgItem )
        dbg (TRACE, "item->priority <%d>", prgItem->priority ) ;
    DebugPrintEvent (DEBUG, prgEvent);
    if (igRecOrgQueLen != 0) 
    {
        llActSize = igRecOrgQueLen;
    }
    else 
    {
        llActSize = prgEvent->data_length + sizeof(EVENT) + 2048;
    }
    prlBchead     = (BC_HEAD *) ((char *) prpEvent + sizeof(EVENT));
    prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data);
    pclSelection  = prlCmdblk->data;
    pclFields     = pclSelection + strlen(pclSelection) + 1;
    pclData       = pclFields + strlen(pclFields) + 1;

    pcgFieldList  = pclFields;
    ilRoutingFlag = 0;
    ilWorkAgain   = RC_SUCCESS;

    if ( pcgUrnoListBuf )
        *pcgUrnoListBuf = 0x00;

    if (igTestOutput > 2) 
    {
        if ( debug_level < DEBUG )
            dbg (TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
        DebugPrintBchead (DEBUG, prlBchead);
        DebugPrintCmdblk (TRACE, prlCmdblk);
        dbg (TRACE, "%05d:selection <%s>", __LINE__,pclSelection);
        dbg (TRACE, "%05d:fields    <%s>", __LINE__,pclFields);
        dbg (TRACE, "%05d:data      <%s>", __LINE__,pclData);
    }
    bgShowEvent = FALSE;
    CheckPerformance (TRUE, prlBchead->orig_name);
    GetHopo ( prlCmdblk->tw_end, clHopo );
    dbg ( DEBUG, "HandleData: using HOPO <%s>", clHopo );

    ilRC = GetCommand (&prlCmdblk->command[0], &ilCmd);
    if (ilRC == RC_SUCCESS)
    {
        switch (ilCmd)
        {

            case CMD_IAZ : /* flow into case OK vbl */
            case CMD_UAZ :
            case CMD_DAZ : 
                ilOldSaveEvent = igSaveEvent;
                igSaveEvent    = TRUE;

                ilRC = SaveEvent();
                if (ilRC != RC_SUCCESS)
                {
                    ilRC = RC_SUCCESS;
                } /* end of if */

                igSaveEvent = ilOldSaveEvent;
                break;


            case CMD_IFI :                           /* flow into case OK vbl */
            case CMD_UFI : 
            case CMD_DFI : 
                dbg (DEBUG, "HandleData: cmd = IFI,UFI,DFI") ;
                strcpy (pcgSearchTable, "AZATAB") ;     /* set table */ 
                igSearchTableAction = 1 ;               /* set 2 AZA */
                if (igUsedAzaTab == USE_NOT_AZATAB)
                {
                    strcpy (pcgSearchTable, "AFTTAB") ;    /* set table */
                    igSearchTableAction = 0 ;              /* set 2 AZA */
                } 

                dbg(DEBUG, "HandleData CMD=xFI : used table is <%s>",
                    pcgSearchTable);
                igEventType = EVT_FLIGHT_INDEPENDENT; /* flight independent */

                ilInboundFtyp  = RC_SUCCESS ;
                ilOutboundFtyp = RC_FAIL ; 

                if (ilRC == RC_SUCCESS)
                {
                    ilRC = GetCedaTimestamp(&clActualDate[0], 16, lgTimeOffset);
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg (TRACE, "HandleData: GetCedaTimestamp failed") ;
                    } /* end of if */
                } /* end of if */

                if (ilRC == RC_SUCCESS)
                {
                    ilRC = ProcessTemplates(&clHopo[0], &clActualDate[0], '\0');
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg(TRACE, "HandleData: ProcessTemplates failed <%d>",
                            ilRC);
                    } /* end of if */

                    if (ilRC == RC_SUCCESS)
                    {
                        ilRC = ForwardEvent (igDest, FALSE, "\0", 0) ;
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData: ForwardEvent failed <%d>",
                                ilRC);
                        }
                        else
                        {
                            ilRC = SaveEvent();
                            if (ilRC != RC_SUCCESS)
                            {
                                ilRC = RC_SUCCESS;
                            } /* end of if */
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
                break;

            case CMD_CKR :                                    /* test command */
                break; 

            case CMD_PRM :                                    /* PRM command */
                dbg(DEBUG,"PRM Command received");
                strcpy(pcgSearchTable, "DPVTAB");     /* set table */ 
                igSearchTableAction = 0;               /* set 2 AFT */
                pclLine =  strtok(pclData,"\n");
                if (pclLine != NULL)
                {
                    do
                    {
                        dbg(DEBUG,"PRM-Line: <%s>",pclLine);
                        ilRC = GetEventType(&igEventType, &pcgInbound, &pcgOutbound,
                                    &plgInbBuffLen, &plgOutbBuffLen, pclLine);      
                        ilRC = ProcessTemplates(&clHopo[0], &clActualDate[0],
                                    pclSelection);
                        dbg(DEBUG,"AddToPrmEventBuf follows, ilRC=%d",ilRC);
                        if (ilRC == RC_SUCCESS)
                        {
                            AddToPrmEventBuf(pclLine,pcgUrnoListBuf);
                        }
                        pclLine = strtok(NULL,"\n");
                    } while(pclLine != NULL);


                    if (ilRC == RC_SUCCESS  && pcgPrmEventBuf != NULL)
                    {
                        ilRC = ForwardPrmEvent (igDest, FALSE, "\0", pcgPrmEventBuf) ;
                        *pcgPrmEventBuf = '\0';
                        igPrmEventBufLen = 0;
                if (ilRC != RC_SUCCESS)
            {
              dbg(TRACE, "HandleData: ForwardPrmEvent failed <%d>",
                ilRC);
            }
          }
                }
        break;

      case CMD_IFR : /* flow into case OK vbl */
      case CMD_UFR :


                /* ++++++++++++++++++++++ */
                ilWorkAgain = RC_SUCCESS;
                strcpy(pcgSearchTable, "AFTTAB");     /* set table */ 
                igSearchTableAction = 0;               /* set 2 AFT */
                ilRC = GetEventType(&igEventType, &pcgInbound, &pcgOutbound,
                                    &plgInbBuffLen, &plgOutbBuffLen, pclData);      
                if(ilRC == RC_SUCCESS)
                {
                    ilInboundFtyp  = RC_FAIL;

                    if ((igEventType==EVT_INBOUND) || (igEventType==EVT_TURNAROUND))
                    {
                        ilRC = GetFieldData2( "AFT", "FTYP", pclFields,
                                             pcgInbound, &pclFtypIn, &llFtypInLen );
                        if ( (ilRC==RC_SUCCESS) && pclFtypIn )
                        {
                            ilInboundFtyp = RC_SUCCESS;
                            /*
                            if ( !strchr ( cgIgnoreFtypIn, pclFtypIn[0] ) )
                            {       
                                dbg(DEBUG, "HandleData: Use Inbound FTYP <%s>", pclFtypIn );
                                ilInboundFtyp = RC_SUCCESS;
                            }
                            else
                                dbg(TRACE, "HandleData: Ignore Inbound FTYP <%s>", pclFtypIn );*/
                        }
                        else
                        {
                            ilRC = RC_SUCCESS;
                            dbg(TRACE, "%05d:HandleData: Unable to read Inbound FTYP RC <%d>",__LINE__, ilRC );
                        }
                    }
                }
                if(ilRC == RC_SUCCESS)
                {
                    ilOutboundFtyp  = RC_FAIL;
                    if ((igEventType==EVT_OUTBOUND) || (igEventType==EVT_TURNAROUND))
                    {
                        ilRC = GetFieldData2( "AFT", "FTYP", pclFields,
                                             pcgOutbound, &pclFtypOut, &llFtypOutLen );
                        if ( (ilRC==RC_SUCCESS) && pclFtypOut )
                        {
                            ilOutboundFtyp = RC_SUCCESS;
                            /*
                            if ( !strchr ( cgIgnoreFtypOut, pclFtypOut[0] ) )
                            {       
                                dbg(DEBUG, "HandleData: Use Outbound FTYP <%s>", pclFtypOut );
                                ilOutboundFtyp = RC_SUCCESS;
                            }
                            else
                                dbg(TRACE, "HandleData: Ignore Outbound FTYP <%s>", pclFtypOut );
                            */
                        }
                        else
                        {
                            ilRC = RC_SUCCESS;
                            dbg(TRACE, "%05d:HandleData: Unable to read Outbound FTYP RC <%d>",__LINE__, ilRC );
                        }
                    }
                }

                if (ilRC == RC_SUCCESS)
                {
                    if ( lgforwardRouting==1 )
                    {
                        ilLastID = GetLastModid4Flight ();
                        dbg ( DEBUG, "GetLastModid4Flight returned <%d>", ilLastID );
                    }
                    if ( ilLastID < 0 )
                    {
                        if (ilInboundFtyp == RC_SUCCESS)
                        {
                            if (ilOutboundFtyp == RC_SUCCESS)
                            {
                                if (igTestOutput > 2) 
                                    dbg(DEBUG, "HandleData: inbound FTYP and outbound FTYP ok -> TURNAROUND");
                                
                                igEventType = EVT_TURNAROUND ;
                                /* PRF6290: Don't forward cancellations -> process at once */ 
                                if ( (lgforwardRouting == 1) && (pclFtypIn[0]!='X') && (pclFtypOut[0]!='X') )
                                {   /* in the config file is forward routing active  */
                                    ilRoutingFlag = ( (lgOPFlag==IS_P_VERSION) && 
                                                      (pclFtypIn[0]!='S') && (pclFtypOut[0]!='S') );
                                    ilRoutingFlag |= ( (lgOPFlag==IS_O_VERSION) && 
                                                       (pclFtypIn[0]!='O') && (pclFtypOut[0]!='O') );
                                    if ( (lgOPFlag==IS_O_VERSION) && !ilRoutingFlag )
                                        ilRoutingFlag = IsFlightOutsideOpRange (pclFields); 
                                }
                            }
                            else
                            {
                                if (igTestOutput > 2) 
                                    dbg(DEBUG, "HandleData: Only inbound FTYP ok -> INBOUND");
                                
                                igEventType = EVT_INBOUND ;
                                /* PRF6290: Don't forward cancellations -> process at once */ 
                                if ( (lgforwardRouting == 1) && (pclFtypIn[0]!='X') )
                                {   /* in the config file is forward routing active  */
                                    ilRoutingFlag = ( (lgOPFlag==IS_P_VERSION) && (pclFtypIn[0]!='S') );
                                    ilRoutingFlag |= ( (lgOPFlag==IS_O_VERSION) && (pclFtypIn[0]!='O') );
                                    if ( (lgOPFlag==IS_O_VERSION) && !ilRoutingFlag )
                                        ilRoutingFlag = IsFlightOutsideOpRange (pclFields); 
                                }

                            }
                        }
                        else 
                            if (ilOutboundFtyp == RC_SUCCESS)
                            { 
                                if (igTestOutput > 2) 
                                    dbg(DEBUG, "HandleData: Only outbound FTYP ok -> OUTBOUND");
                                
                                igEventType = EVT_OUTBOUND ;
                                /* PRF6290: Don't forward cancellations -> process at once */ 
                                if ( (lgforwardRouting == 1) && (pclFtypOut[0]!='X') )
                                {   /* in the config file is forward routing active  */
                                    ilRoutingFlag = ( (lgOPFlag==IS_P_VERSION) && (pclFtypOut[0]!='S') );
                                    ilRoutingFlag |= ( (lgOPFlag==IS_O_VERSION) && (pclFtypOut[0]!='O') );
                                    if ( (lgOPFlag==IS_O_VERSION) && !ilRoutingFlag )
                                        ilRoutingFlag = IsFlightOutsideOpRange (pclFields); 
                                }
                            }
                            else
                            {
                                dbg(TRACE, "HandleData: Flight(s) won't be handled");
                                igEventType = EVT_UNKNOWN;
                            }
                    }
                    else
                    {
                        if ( ilLastID != mod_id )
                        {
                            dbg ( TRACE,"Flight was last time handled by different modid <%d> != <%d>", 
                                  ilLastID, mod_id );
                            ilRoutingFlag = 1;
                        }
                    }
                    if ( (ilRC == RC_SUCCESS) && ilRoutingFlag )
                    {
                        dbg ( DEBUG, "HandleData: Routing necessary RecOrig <%d> ParOrig <%d> MyOrig <%d>",
                              igRecOriginator, igParOriginator, igMyOriginator );
                        if ( (igRecOriginator != igMyOriginator)  &&
                             (igRecOriginator != igParOriginator) )
                        {
                            prgItem->originator = igMyOriginator;
                            /* set prio to 4 for flights routed to rulfndp */
                            if ( (lgOPFlag==IS_O_VERSION) && 
                                 (prgItem->priority < PRIORITY_4) )
                                prgItem->priority = PRIORITY_4 ;    
                            ilRC = que( QUE_PUT, igParOriginator, mod_id, prgItem->priority,
                                        llActSize, (char *) prgEvent);     
                            dbg ( TRACE, "HandleData: Routing to <%d> returned <%d>", 
                                  igParOriginator, ilRC );
                            if  ( ilRC == RC_SUCCESS )  
                            {
                                AddToHistory ( igParOriginator );
                                ilWorkAgain = RC_FAIL;
                            }
                            ilRC = RC_FAIL ;
                        }   
                    }   /* routing finished */  

                    if ( (ilRC == RC_SUCCESS) &&    (ilWorkAgain==RC_SUCCESS) )
                    {
                        ilRC = GetCedaTimestamp(&clActualDate[0], 16,
                                                lgTimeOffset);
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData: GetCedaTimestamp failed");
                        } /* end of if */
                    } /* end of if */

                    if ((ilRC == RC_SUCCESS) && ( ilWorkAgain == RC_SUCCESS ))
                    {   
                        /* set prio to 3 for really operative flights */
                        if ( (lgOPFlag==IS_O_VERSION) && 
                             (prgItem->priority > PRIORITY_3) )
                            prgItem->priority = PRIORITY_3 ;    
                        switch (igEventType)
                        {
                            case EVT_INBOUND:
                                ilRC = FieldCompare("AFTTAB", "STOA",
                                                    &clActualDate[0], pclFields,
                                                    pcgInbound,CMP_GREATER); 
    
                                if (ilRC == CMP_GREATER)
                                {
                                    ilRC = RC_SUCCESS;
                                }
                                else
                                {
                                    if (ilRC == RC_FAIL)
                                    {
                                        dbg(TRACE,
                                            "HandleData: FieldCompare failed inbound STOA");
                                    }
                                    else
                                    {
                                        dbg(TRACE,
                                            "HandleData: ignore inbound event STOA");
                                        ilRC = RC_FAIL;
                                    } /* end of if */
                                } /* end of if */
                                break;

                            case EVT_OUTBOUND:
                                ilRC = FieldCompare("AFTTAB", "STOD",
                                                    &clActualDate[0], pclFields,
                                                    pcgOutbound, CMP_GREATER);
                                if (ilRC == CMP_GREATER)
                                {
                                    ilRC = RC_SUCCESS;
                                }
                                else
                                {
                                    if(ilRC == RC_FAIL)
                                    {
                                        dbg(TRACE,
                                            "HandleData: FieldCompare failed oubound stod");
                                    }
                                    else
                                    {
                                        dbg(TRACE,
                                            "HandleData: ignore outbound event STOD");
                                        ilRC = RC_FAIL;
                                    } /* end of if */
                                } /* end of if */
                                break;

                            case EVT_TURNAROUND :
                                ilRC = FieldCompare("AFTTAB", "STOA",
                                                    &clActualDate[0], pclFields,
                                                     pcgInbound, CMP_GREATER);
                                if (ilRC == CMP_GREATER)
                                {
                                    ilRC = RC_SUCCESS;
                                }
                                else
                                {
                                    if (ilRC == RC_FAIL)
                                    {
                                        dbg(TRACE,
                                            "HandleData: FieldCompare failed turnaround STOA");
                                    }
                                    else
                                    {
                                        dbg(TRACE,
                                            "HandleData: ignore turnaround event STOA");
                                        ilRC = RC_FAIL;
                                    } /* end of if */
                                } /* end of if */
                                break;

                            default :
                                dbg(TRACE,
                                    "HandleData: eventtype UNKNOWN - ignore event! %d",
                                    igEventType);
                                break;

                        } /* end of switch igEventType */
                    } /* end of if */
                }
                else
                {
                    if (ilRoutingFlag == 0) 
                    {
                        dbg(TRACE, "HandleData: GetEventType failed <%d>",
                            ilRC);
                    }
                } /* end of if */

                if ((ilRC == RC_SUCCESS) && ( ilWorkAgain == RC_SUCCESS ))   
                {
                    dbg(DEBUG, "HandleData: Hopo = <%s> ", clHopo);             
                    if (igEventType != EVT_UNKNOWN)
                    {
                        dbg(DEBUG, "HandleData: call process template");
                        if (bgDiagInt || bgDiagExt)
                            CheckDiagFlag(pclSelection);
                        if (bgDiagOn)
                        {
                            ilRC = GetFieldData2("AFT", "ALC2", pclFields,
                                                pclData, &pclAftAlc, &llAlcLen );
                            if ( strncmp(pclAftAlc, " ",1) == 0 )
                                ilRC = GetFieldData2("AFT", "ALC3", pclFields,
                                                    pclData, &pclAftAlc, &llAlcLen );
                            ilRC = GetFieldData2("AFT", "FLTN", pclFields,
                                                pclData, &pclAftFltn, &llFltnLen);
                            sprintf(clFlightName, "%s%s", pclAftAlc,
                                    pclAftFltn);
                            TrimRight(clFlightName);
                            ilRC = OpenDiagFile(clFlightName);
                        }
                        ilRC = ProcessTemplates(&clHopo[0], &clActualDate[0],
                                                pclSelection);
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE,"HandleData:ProcessTemplates failed <%d>",
                                ilRC);
                        } /* end of if */
                    
                        if (bgDiagOn)
                        {
                            if (pfgDiagIntOut)
                                fclose(pfgDiagIntOut);
                            if (pfgDiagExtOut)
                                fclose(pfgDiagExtOut);
                            bgDiagOn = FALSE;
                        }
                    } /* end of if */
    
                    if ((ilRC == RC_SUCCESS) && ( ilWorkAgain == RC_SUCCESS )) 
                    {
                        dbg(DEBUG, "HandleData: call forward event"); 
                        /* ilRC = ForwardEvent(mod_id+1, FALSE, "\0", 0); */
                        ilRC = ForwardEvent(igDest, FALSE, "\0",0);
                        if ( igFlightDataDest > 0 ) 
                            ilRC |= ForwardEvent(igFlightDataDest, FALSE, "\0",0);
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData: ForwardEvent failed <%d>",
                                ilRC);
                        }
                        else
                        {
                            if ( lgforwardRouting==1)
                                AddToHistory ( mod_id );

                            if ( ilWorkAgain == RC_SUCCESS )
                            {
                                ilRC = SaveEvent();
                                if (ilRC != RC_SUCCESS)
                                {
                                    ilRC = RC_SUCCESS;
                                } /* end of if */
                            }
                        } /* end of if */
                    } /* end of if */
                } /* end of if */
                /*
                if ( pclFtypIn )
                    free ( pclFtypIn );
                if ( pclFtypOut )
                    free ( pclFtypOut );
                pclFtypIn = pclFtypOut = 0;*/
                break; /* CMD_UFR */

            case CMD_DFR :
                dbg(DEBUG,"HandleData: DFR"); 
                /* ilRC = ForwardEvent(mod_id+1, FALSE, "\0", 0); */
                ilRC = ForwardEvent(igDest, FALSE, "\0", 0);
                if ( igFlightDelDest > 0 )      
                    ilRC |= ForwardEvent(igFlightDelDest, FALSE, "\0", 0);

                if (ilRC != RC_SUCCESS)
                {
                    dbg(TRACE,"HandleData: ForwardEvent failed <%d>",ilRC);
                } /* end of if */
                break;                 /* CMD_UFR */

            case CMD_FIR :                         /* Flight independece */
                dbg(DEBUG,"HandleData: FIR");  
                strcpy(clSelection, pclSelection);
                pclPipe = strchr(clSelection, '|');
                strcpy(cgFirTemplates, "\0");
                if ( pclPipe != NULL )
                {
                    CheckFirTemplates(cgHopo, pclPipe);
                    *pclPipe = '\0';
                }

                ilRC = getDateTimefromString(clDateFrom, clDateTo, clSelection,
                                                ','); 
                if (ilRC != RC_SUCCESS) 
                {
                    dbg(DEBUG, "HandleData: received FIR without date, taking actual day !!!");
                    clDateFrom[0] = clDateTo[0] = '\0'; 
                }
                ilRC = SendFlightIndepRules(&cgHopo[0], igFIRDest, clDateFrom,
                                            clDateTo);
                if (ilRC != RC_SUCCESS)
                {
                    dbg(TRACE, "HandleData: SendFlightIndepRules failed");
                }
                else
                {
                    ilRC = SaveEvent();
                    if (ilRC != RC_SUCCESS)
                    {
                        ilRC = RC_SUCCESS ;
                    } /* end of if */
                } /* end of if */
                break ; /* CMD_FIR */

            case CMD_CFL :
                dbg(DEBUG,"HandleData: CFL"); 

                if ( igFlightDataDest > 0 )                             /*rna*/
                {                                                       /*rna*/
                    ilRC = ForwardEvent(igFlightDataDest, FALSE, "\0", 0);  /*rna*/
                    if (ilRC != RC_SUCCESS)                             /*rna*/
                    {                                                   /*rna*/
                        dbg(TRACE,                                      /*rna*/
                            "HandleData: ForwardEvent to profnd failed <%d>",
                            ilRC);                                      /*rna*/
                    }                                                   /*rna*/
                }
                else
                {
                    ilRC = ForwardEvent(igDest, FALSE, "\0", 0);
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg(TRACE, "HandleData: ForwardEvent failed <%d>",
                            ilRC);
                    }
                }
                break; /* CMD_UFR */

            case CMD_SFL :
                dbg(DEBUG, "HandleData: SFL");
                if ( igFlightDataDest > 0 )     /**********hag001115*********/
                {
                    ilRC = ForwardEvent(igFlightDataDest, FALSE, "\0", 0);
                    if (ilRC != RC_SUCCESS)
                        dbg(TRACE,"HandleData: ForwardEvent <SFL> failed");
                }
                ilRC = SendFieldListToOne(prpEvent->originator);
                if (ilRC != RC_SUCCESS)
                {
                    dbg(TRACE,"HandleData: SendFieldListToOne failed") ;
                } /* end of if */
                break; /* CMD_SFL */

            case CMD_IRT :
            case CMD_DRT :
            case CMD_URT :
                dbg(DEBUG,"HandleData: <%s> Table <%s>", prlCmdblk->command,
                    prlCmdblk->obj_name);  
                ilRC = CEDAArrayEventUpdate(prgEvent);
                if (ilRC != RC_SUCCESS)
                {
                    dbg(TRACE,"HandleData: CEDAArrayEventUpdate failed <%d>",
                        ilRC);
                } /* end of if */

                if ( (ilRC == RC_SUCCESS) && (debug_level > TRACE) )
                {
                    ilRC = CEDAArrayReadAllHeader(outp);
                    if (ilRC != RC_SUCCESS)
                    {
                        dbg(TRACE,
                            "HandleData: CEDAArrayReadAllHeader failed <%d>",
                            ilRC);
                    }
                    else
                    {
                        dbg (DEBUG, "HandleData: CEDAArrayReadAllHeader OK") ;
                    } /* end of if */
                } /* end of if */

                if ( strncmp ( prlCmdblk->obj_name, "SGM", 3 ) == 0 )
                {   
                    ilRC = GetFieldData( "SGM", "USGR", pclFields, pclData,
                                         &pclSgrUrno ); 
                    if (ilRC == RC_SUCCESS)
                    {
                        dbg(DEBUG,
                            "HandleData: extracted USGR <%s> successfully",
                            pclSgrUrno); 
                        DeleteFromSgrEval( clHopo, pclSgrUrno, 0 );
                    }
                    else
                        dbg(TRACE,
                            "HandleData: Failed to Get USGR from SGM-record");
                }
                else
                    if ( /*( strcmp ( prlCmdblk->command, "DRT" ) == 0 ) && */
                         ( strncmp ( prlCmdblk->obj_name, "SGR", 3 ) == 0 ))
                    {
                        ilRC = GetFieldData( "SGR", "GRPN", pclFields, pclData,
                                             &pclSgrGrpn ); 
                        if (ilRC == RC_SUCCESS)
                        {
                            dbg(DEBUG,
                                "HandleData: extracted GRPN <%s> successfully",
                                pclSgrGrpn); 
                            DeleteFromSgrEval( clHopo, 0, pclSgrGrpn );
                        }
                        else
                            dbg(TRACE,
                                "HandleData: Failed to Get GRPN from SGR-record");
                    }
                
                if ( pclSgrUrno )
                    free(pclSgrUrno);
                if ( pclSgrGrpn )
                    free(pclSgrGrpn);
                pclSgrUrno = pclSgrGrpn = NULL;

                if ( strncmp(prlCmdblk->obj_name, "TPL", 3) == 0 )
                {   /*  Rest has only to be done if TPL-Record caused event */
                    if (ilRC == RC_SUCCESS)
                    {
                        ilRC = ReallocOutBuf();
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData: ReallocOutBuf failed <%d>",
                                ilRC);
                            Terminate(10);
                        } 
                    } 
                    if (ilRC == RC_SUCCESS)
                    {
                        ilRC = BuildTotalFieldList();
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData:BuildTotalFieldList failed");
                        } /* end of if */
                        else
                        {
                            ilRC = SendFieldListToAll(&cgFieldListDest[0]);
                            if (ilRC != RC_SUCCESS)
                            {
                                dbg(TRACE,
                                    "HandleData: SendFieldListToAll failed");
                            }
                        } /* end of if */
                    } /* end of if */
                }
                break; /* CMD_IRT, CMD_URT, CMD_DRT */
            case CMD_PFL:
                dbg(DEBUG,
                    "HandleData: Received Command <PFL> with fieldlist <%s>",
                    pclFields);
                ilChanged = 0;
                ilRC = UpdateAddFields( pclFields, &ilChanged );
                if ( ilRC != RC_SUCCESS )
                    dbg(TRACE,
                        "HandleData: UpdateAddFields failed RC <%d>", ilRC);
                else
                    if ( ilChanged > 0 )
                    {
                        ilRC = SendFieldListToAll( cgFieldListDest );
                        if (ilRC != RC_SUCCESS)
                        {
                            dbg(TRACE, "HandleData: SendFieldListToAll failed");
                        }
                    }
                break;
            case CMD_ANSWER:
                break;
            default :
                dbg(TRACE, "HandleData: unknown command <%s>",
                    &prlCmdblk->command[0]);        
                if (debug_level == TRACE)
                    DebugPrintBchead(TRACE, prlBchead);
                /* DebugPrintCmdblk(TRACE, prlCmdblk); */
                dbg(TRACE,"selection <%s>", pclSelection);
                /* dbg(TRACE,"fields    <%s>", pclFields); */
                /* dbg(TRACE,"data      <%s>", pclData); */
                break ; /* default */
        } /* end of switch */
    }
    else
    {
        dbg (TRACE, "HandleData: GetCommand failed");
        if (debug_level == TRACE)
            DebugPrintBchead (TRACE, prlBchead);
        /* DebugPrintCmdblk (TRACE, prlCmdblk); */
        dbg (TRACE, "selection <%s>", pclSelection);
        /* dbg (TRACE, "fields    <%s>", pclFields); */
        /* dbg (TRACE, "data      <%s>", pclData);  */
    } /* end of if */
    /*
    if (pcgInbound != NULL)
    {
        free(pcgInbound);
        pcgInbound = NULL;
    } 

    if (pcgOutbound != NULL)
    {
        free(pcgOutbound);
        pcgOutbound = NULL;
    }  */
/*      
    if ( pclAftAlc )
        free ( pclAftAlc );
    if ( pclAftFltn )
        free ( pclAftFltn );
*/
    CheckPerformance (FALSE, "\0");
    ilOldDebugLevel = debug_level;
    if (bgShowEvent == TRUE)
    {
        debug_level = TRACE;
        dbg (TRACE, "item->priority <%d>", prgItem->priority ) ;
        dbg (TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
        DebugPrintCmdblk(TRACE,prlCmdblk);
        dbg(TRACE,"selection <%s>",pclSelection);
        dbg(TRACE,"fields    <%s>",pclFields);
        dbg(TRACE,"data      <%s>",pclData);
    }                

    dbg (TRACE, "=> END   <%10.10d> -", lgEvtCnt); 
    debug_level = ilOldDebugLevel ;

    return(ilRC);
    
} /* end of HandleData */




/******************************************************************************/
static int ForwardPrmEvent(int ipDest, int ipSendDate, char *cpSendDate, char *pcpData )
{
    int       ilRC        = RC_SUCCESS;  /* Return code */
    BC_HEAD  *prlBchead    = NULL;
    CMDBLK   *prlCmdblk    = NULL;
    char     *pclSelection = NULL;
    char     *pclFields    = NULL;
    char     *pclData      = NULL;
    char     *pclUrnoList  = NULL;
    long      llActSize   = 0;
    int      ilPrio = PRIORITY_4;
        
    if (igTestOutput > 2) 
    {
        dbg(TRACE,"ForwardPrmEvent: ###############  dest <%d> ###############", ipDest);
        dbg(DEBUG,"Data: \n<%s>",pcpData);
    }
    /* llActSize = prgEvent->data_length + sizeof(EVENT) + lgUrnoListLen ; */
    
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + prgEvent->data_length + 20 ;  
    if ( pcgUrnoListBuf )
        llActSize += strlen(pcgUrnoListBuf);    /*  hag 20030710 */

    if ( ipSendDate && cpSendDate )
        llActSize += strlen ( cpSendDate );
    if ( pcpData )
        llActSize += strlen ( pcpData );

    if (llActSize > lgOutEventSize)
    {

        lgOutEventSize = llActSize ;

        prgOutEvent = realloc (prgOutEvent, lgOutEventSize) ;
        if (prgOutEvent == NULL)
        {
            dbg (TRACE, "ForwardEvent: realloc out event <%ld> bytes failed",lgOutEventSize);
            ilRC = RC_FAIL ;
        } /* end of if */
        else
            dbg (TRACE, "ForwardEvent: realloc out event <%ld> bytes ok", lgOutEventSize );
    } /* end of if */
    
    if (ilRC == RC_SUCCESS)
    {
        memcpy (prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT)) ;
        
        prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        pclSelection = prlCmdblk->data ;
        prgOutEvent->originator = mod_id;   /* hag001116 */
        
        if (ipSendDate == TRUE)
        {
            if (cpSendDate[0] == '\0')
            {
                dbg (DEBUG, "ForwardEvent: ipSendDate <%d> Date <>", ipSendDate ) ;         
            }
            strcpy (pclSelection, cpSendDate) ;     
            
            pclFields    = pclSelection + strlen(pclSelection) + 1 ;
            strcpy (pclFields, " ") ;
            pclData      = pclFields + strlen(pclFields) + 1 ;
            if ( pcpData )
                strcpy ( pclData, pcpData );
            else
                strcpy (pclData," ") ;
            /**
            pclUrnoList  = pclData + strlen(pclData) ;
            *pclUrnoList = 0x00 ;
            pclUrnoList++ ;
            if ( pcgUrnoListBuf )
                strcpy (pclUrnoList,pcgUrnoListBuf) ;
                ***/
        }
        else
        {
            pclFields    = pclSelection + strlen(pclSelection) + 1 ;
            pclData      = pclFields + strlen(pclFields) + 1 ;
            if ( pcpData )
                strcpy ( pclData, pcpData );
            /***
            pclUrnoList  = pclData + strlen(pclData) ;
            *pclUrnoList = 0x00 ;
            pclUrnoList++ ;
            if ( pcgUrnoListBuf )
                strcpy (pclUrnoList, pcgUrnoListBuf) ;
        ***/
        } /* end of if */
    
        prgOutEvent->data_length = llActSize - sizeof(EVENT) ;


        if ( prgItem && strcmp (prlCmdblk->command, "FIR" ) )
        {   /*  hag 16.04.2003, PRF4089: use PRIORITY_4 for FIR command. 
                Otherwise use priority of incoming event                    */
            ilPrio = prgItem->priority;
        }
        if (debug_level != 0 ) 
        {
            dbg (DEBUG, "item->priority <%d>", ilPrio) ;
            DebugPrintBchead (DEBUG,prlBchead) ;
            DebugPrintCmdblk (DEBUG,prlCmdblk) ;
            dbg (DEBUG, "ForwardEvent: selection <%s>", pclSelection) ;         
            dbg (DEBUG, "ForwardEvent: fields    <%s>", pclFields) ;            
            dbg (DEBUG, "ForwardEvent: data      <%s>", pclData) ;              
        }
        /*dbg (TRACE, "ForwardEvent: urnolist  <%s>", pclUrnoList) ;     */    
        dbg (TRACE, "ForwardEvent: event->data_length  <%ld> llActSize <%ld>", 
             prgOutEvent->data_length, llActSize) ;              
        /*************************************************************************/
        ilRC = que (QUE_PUT, ipDest, mod_id, ilPrio, llActSize, (char *)prgOutEvent) ;
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
            /*dbg (TRACE, "urnolist  <%s>", pclUrnoList) ;*/
        } /* end of if */
    } /* end of if */
 
    if (ilRC != RC_SUCCESS) 
    {
        dbg (TRACE, "ForwardEvent: ERR detected !!!") ;         
    }   
    return (ilRC) ;
    
}
/******************************************************************************/
static int ForwardEvent(int ipDest, int ipSendDate, char *cpSendDate, char *pcpData )
{
    int       ilRC        = RC_SUCCESS;  /* Return code */
    BC_HEAD  *prlBchead    = NULL;
    CMDBLK   *prlCmdblk    = NULL;
    char     *pclSelection = NULL;
    char     *pclFields    = NULL;
    char     *pclData      = NULL;
    char     *pclUrnoList  = NULL;
    long      llActSize   = 0;
    int      ilPrio = PRIORITY_4;
        
    if (igTestOutput > 2) 
    {
        dbg(TRACE,"ForwardEvent: ###############  dest <%d> ###############", ipDest);
    }
    /* llActSize = prgEvent->data_length + sizeof(EVENT) + lgUrnoListLen ; */
    
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + prgEvent->data_length + 20 ;  
    if ( pcgUrnoListBuf )
        llActSize += strlen(pcgUrnoListBuf);    /*  hag 20030710 */

    if ( ipSendDate && cpSendDate )
        llActSize += strlen ( cpSendDate );
    if ( pcpData )
        llActSize += strlen ( pcpData );

    if (llActSize > lgOutEventSize)
    {

        lgOutEventSize = llActSize ;

        prgOutEvent = realloc (prgOutEvent, lgOutEventSize) ;
        if (prgOutEvent == NULL)
        {
            dbg (TRACE, "ForwardEvent: realloc out event <%ld> bytes failed",lgOutEventSize);
            ilRC = RC_FAIL ;
        } /* end of if */
        else
            dbg (TRACE, "ForwardEvent: realloc out event <%ld> bytes ok", lgOutEventSize );
    } /* end of if */
    
    if (ilRC == RC_SUCCESS)
    {
        memcpy (prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT)) ;
        
        prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        pclSelection = prlCmdblk->data ;
        prgOutEvent->originator = mod_id;   /* hag001116 */
        
        if (ipSendDate == TRUE)
        {
            if (cpSendDate[0] == '\0')
            {
                dbg (DEBUG, "ForwardEvent: ipSendDate <%d> Date <>", ipSendDate ) ;         
            }
            strcpy (pclSelection, cpSendDate) ;     
            
            pclFields    = pclSelection + strlen(pclSelection) + 1 ;
            strcpy (pclFields, " ") ;
            pclData      = pclFields + strlen(pclFields) + 1 ;
            if ( pcpData )
                strcpy ( pclData, pcpData );
            else
                strcpy (pclData," ") ;
            pclUrnoList  = pclData + strlen(pclData) ;
            *pclUrnoList = 0x00 ;
            pclUrnoList++ ;
            if ( pcgUrnoListBuf )
                strcpy (pclUrnoList,pcgUrnoListBuf) ;
        }
        else
        {
            pclFields    = pclSelection + strlen(pclSelection) + 1 ;
            pclData      = pclFields + strlen(pclFields) + 1 ;
            if ( pcpData )
                strcpy ( pclData, pcpData );
            pclUrnoList  = pclData + strlen(pclData) ;
            *pclUrnoList = 0x00 ;
            pclUrnoList++ ;
            if ( pcgUrnoListBuf )
                strcpy (pclUrnoList, pcgUrnoListBuf) ;
        } /* end of if */
    
        prgOutEvent->data_length = llActSize - sizeof(EVENT) ;


        if ( prgItem && strcmp (prlCmdblk->command, "FIR" ) )
        {   /*  hag 16.04.2003, PRF4089: use PRIORITY_4 for FIR command. 
                Otherwise use priority of incoming event                    */
            ilPrio = prgItem->priority;
        }
        if (debug_level != 0 ) 
        {
            dbg (DEBUG, "item->priority <%d>", ilPrio) ;
            DebugPrintBchead (DEBUG,prlBchead) ;
            DebugPrintCmdblk (DEBUG,prlCmdblk) ;
            dbg (DEBUG, "ForwardEvent: selection <%s>", pclSelection) ;         
            dbg (DEBUG, "ForwardEvent: fields    <%s>", pclFields) ;            
            dbg (DEBUG, "ForwardEvent: data      <%s>", pclData) ;              
        }
        dbg (TRACE, "ForwardEvent: urnolist  <%s>", pclUrnoList) ;         
        dbg (TRACE, "ForwardEvent: event->data_length  <%ld> llActSize <%ld>", 
             prgOutEvent->data_length, llActSize) ;              
        /*************************************************************************/
        ilRC = que (QUE_PUT, ipDest, mod_id, ilPrio, llActSize, (char *)prgOutEvent) ;
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
            dbg (TRACE, "urnolist  <%s>", pclUrnoList) ;
        } /* end of if */
    } /* end of if */
 
    if (ilRC != RC_SUCCESS) 
    {
        dbg (TRACE, "ForwardEvent: ERR detected !!!") ;         
    }   
    return (ilRC) ;
    
} /* end of ForwardEvent */





/******************************************************************************/
/******************************************************************************/
static int SaveEvent(void)
{
 int    ilRC      = RC_SUCCESS ;
 long   llLen     = 0 ;
 FILE  *prlFile = NULL ;
 char   clFile[512] ;

 typedef struct {
                 char crKeyWord[16];
                 long lrLength;
                } EVT_FILEINFO ;

 EVT_FILEINFO rlFileInfo ;

 if (igSaveEvent != TRUE)
 {
  return(RC_SUCCESS);
 } /* end of if */

 memset (&rlFileInfo,0x00,sizeof(EVT_FILEINFO));

 if (prgEvent->originator == 7555)
 {
  ilRC = RC_FAIL ;
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  sprintf(&clFile[0],"%s/%s%10.10d.evt",getenv("DBG_PATH"),mod_name,lgEvtCnt);
  errno = 0 ;
  prlFile = fopen (&clFile[0], "w") ;
  if (prlFile == NULL)
  {
   dbg (TRACE,"SaveEvent: fopen <%s> failed <%d-%s>",&clFile[0],errno,strerror(errno));
   ilRC = RC_FAIL ;
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  strcpy(rlFileInfo.crKeyWord,mod_name) ;
  rlFileInfo.lrLength = (sizeof(EVENT)+prgEvent->data_length) ;
 } /* end of if */
    
 if (ilRC == RC_SUCCESS)
 {
  llLen = sizeof(EVT_FILEINFO);
  errno = 0;

  ilRC = fwrite ((void *)&rlFileInfo,1,llLen,prlFile);
  if (ilRC != llLen)
  {
   dbg(TRACE, "SaveEvent: fopen <%s> failed <%d-%s>",&clFile[0],errno,strerror(errno));
   ilRC = RC_FAIL ;
  }
  else
  {
   ilRC = RC_SUCCESS ;
  } /* end of if */
 } /* end of if */

 if (ilRC == RC_SUCCESS)
 {
  llLen = rlFileInfo.lrLength ;
  errno = 0 ;

  ilRC = fwrite ((void *)prgEvent,1,llLen,prlFile);
  if (ilRC != llLen)
  {
   dbg(TRACE,"SaveEvent: fopen <%s> failed <%d-%s>",&clFile[0], errno, strerror(errno)) ;
   ilRC = RC_FAIL ;
  }
  else
  {
   ilRC = RC_SUCCESS ;
  } /* end of if */
 } /* end of if */
        
 if (prlFile != NULL)
 {
  fclose (prlFile) ;
  prlFile = NULL ;
  dbg (TRACE, "SaveEvent: event saved - file <%s>", &clFile[0]) ;
 } /* end of if */

 return (ilRC) ;

} /* SaveEvent */


/******************************************************************************/
/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (DEBUG,"SaveIndexInfo: SaveIndexInfo <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRC == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRC > 0)
      {
       ilRC = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
     ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRC == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRC) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   if (ilRC == RC_NOTFOUND)
   {
    ilRC = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRC) ;

}/* end of SaveIndexInfo */



/*******************************************************************/
/* Analyse Tablename and the fieldcount from the expression record */
/*  input string (cpSrcStr) = "ACT3, (TAB<ETX>FLDN <= XY) && (..)."*/
/*  was parsed for the following OUTPUT information:               */ 
/*     << cpTabname  : DB table (prev for delimiter <ETX>)         */
/*     << lpFldCnt   : count of all detected fields                */
/*     << cp1FieldStr: detect.flds like "aaaa,bbbb,cccc"           */
/*     << lpusedChars: the count of max. chars for the scan string */    
/* getestet 13.12.99 GHe                                           */
/* sep. tested with function: TestAnalyseStr(void) [use ilTest = 1]*/
/*******************************************************************/
static int AnalyseStr(char *cpSrcStr, char *cpTabname, long *lpFldCnt, 
                      char *cp1FieldStr, long *lpUsedChars) 
{ 
    long  ilLength;
    long  ilFldCnt;
    long  ilSearchPos;
    long  llusedChars;
    long  llHelp;
    long  ilTest;
    int   ilRC;
    char  cDelimiter;
    char  cpFieldStr[1024];
    char  clpTabname[64];

    ilLength = strlen( cpSrcStr );  /* len from ACT3,(ACT.ACWS >= 41)... str */
    llusedChars = ilLength; 
    cDelimiter = cgEtx[0];        /* between tablename and fieldACT<ETX>ACWS */
    ilFldCnt = 0;                    /* counter for found fields*/
    ilSearchPos = 0;                 /* act search string position */  
    clpTabname[0] = '\0';            /* found tablename without end */ 
    ilRC = RC_SUCCESS; 

    ilTest = 0;    /* 1 --> test function */ 
    if (ilTest == 1) 
    {
        dbg(DEBUG, "AnalyseStr : len cmp Str = %ld", ilLength);
    }

    do 
    {
        if ( cpSrcStr[ilSearchPos] == cDelimiter ) 
        {
            if (ilTest == 1) 
            {
                dbg(DEBUG, "AnalyseStr : delimiter found on pos=%ld",
                    ilSearchPos);
            }
            if (clpTabname[0] == '\0')
            {
                if (ilSearchPos < 3)
                {
                    dbg(TRACE, "AnalyseStr : ERROR <EXT> ON START POSITION");
                    ilRC = RC_FAIL;
                    if (bgDiagOn)
                    {
                        if (bgDiagInt)
                            WriteDiagFile(DIAGINT, DIAGNOTABNAME, NULL, NULL,
                                            NULL);
                        if (bgDiagExt)
                            WriteDiagFile(DIAGEXT, DIAGNOTABNAME, NULL, NULL,
                                            NULL);
                    }
                } 
                else 
                {
                    strncpy(clpTabname, &cpSrcStr[ilSearchPos-3], 3);
                    clpTabname[3] = '\0';
                    sprintf(cpTabname, "%sTAB", clpTabname );
                    if (ilTest == 1)
                    {
                        dbg(DEBUG, "AnalyseStr : copy tablename=<%s>",
                            clpTabname);
                        dbg(DEBUG, "AnalyseStr : tablename(END)=<%s>",
                            cpTabname);
                    } 
                }
            } /* if (clpTabname[0] == '\0')  */ 

            if (ilTest == 1) 
            {
                dbg(DEBUG, "AnalyseStr : search following field from <%s>",
                    &cpSrcStr[ilSearchPos]);
            }
            if (ilSearchPos+4 > ilLength)
            {
                dbg(TRACE, "AnalyseStr : ERROR <EXT> FIELD_LENGTH?");
                ilRC = RC_FAIL;
                if (bgDiagOn)
                {
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGNOFIELDNAME, NULL, NULL,
                                        NULL);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGNOFIELDNAME, NULL, NULL,
                                        NULL);
                }
            }
            else  /* CP TAB STRING already exist */
            {
                if (ilFldCnt == 0)
                {
                    strncpy(cpFieldStr, &cpSrcStr[ilSearchPos+1], 4);
                    cpFieldStr[4] = '\0';
                    strcpy(cp1FieldStr, cpFieldStr);
                    if (ilTest == 1)
                    {
                        llHelp = 20;
                    }
                    else
                    {
                        clpTabname[3] = '\0';
                        cpFieldStr[4] = '\0';
                        ilRC = GetFieldLength(clpTabname, cpFieldStr, &llHelp);
                        if (ilTest == 1) 
                        {
                            dbg(DEBUG, "AnalyseStr : Tablename = <%s>",
                                clpTabname); 
                            dbg(DEBUG, "AnalyseStr : Fieldname = <%s>",
                                cpFieldStr);
                            dbg(DEBUG, "AnalyseStr : Found Fieldlength = <%ld>",                                llHelp);
                        }
                    }
                    llusedChars += llHelp;
                }
                else  /* ilFldCnt != 0 */
                {
                    strncpy(cpFieldStr, &cpSrcStr[ilSearchPos+1], 4);
                    cpFieldStr[4] = '\0';
                    if (ilTest == 1)
                    {
                        dbg(DEBUG, "AnalyseStr : copy Fieldname=<%s>",
                            cpFieldStr);
                    }
                    strcat(cp1FieldStr, ", ");
                    strcat(cp1FieldStr, cpFieldStr);
                    if (ilTest == 1)
                    {
                        llHelp = 10;
                    }
                    else
                    {
                        ilRC = GetFieldLength(clpTabname, cpFieldStr, &llHelp);
                    }
                    llusedChars += llHelp;
                }
            }
            ilFldCnt++;
        }
        ilSearchPos++;
    } while (( ilSearchPos < ilLength ) && (ilRC == RC_SUCCESS));

    *lpFldCnt = ilFldCnt;
    *lpUsedChars = llusedChars;
    dbg ( DEBUG, "AnalyseStr: RC <%d> Table <%s> FieldCount <%ld> FieldStr <%s> UsedChars <%ld>", 
                            ilRC, cpTabname, *lpFldCnt, cp1FieldStr, *lpUsedChars) ;
    return ilRC; 

} /* end of function AnalyseStr() */ 





/*******************************************************************/
/* CREATE the compare string       CHANGED VERSION                 */
/*  input string (cpSrcStr) = "(TAB<ETX>FLDN <= XY) && (..)."      */
/*  was parsed and changed for the following OUTPUT information:   */ 
/*     << cpCmpStr   : "(12 <= XY) && (..)"                        */
/*  the TAB<ETX>FLDN information must be in correct sequenz in the */
/*  input dataString in the following form:                        */
/*     >> cpDataArea : "12\\0112\\0..."                            */
/* getestet 14.12.99 GHe                                           */ 
/* sep. tested with function: TestAnalyseStr(void) [use ilTest = 1]*/
/* Additional we can work with different Tables                    */ 
/*                                                                 */  
/*******************************************************************/
static int SetCmpStringCond (char *slCondition,   /* cond. for lex/Yacc */
                             char *cpCmpStr,      /* given expression   */
                             char *cpDataArea  )  /* Fieldvals from DB  */
{
    int     ilRC  = RC_SUCCESS;
    long    ilConditionPos;
    long    ilDataAeraPos;
    long    ilCmpStrPos;
    char    clTabName[32];
    char    ilNameFieldlength;
    long    llFieldLen;
    long    ilLength;
    char    cDelimiter;
    long    ilScanAll;
    char    clActField[64];
    long    ilTest;
    char    clHelpStr[2048];

  /* init values */
    ilLength = strlen( cpCmpStr );
    ilCmpStrPos = 0;
    ilConditionPos = 0;
    clTabName[0] = '\0';
    ilNameFieldlength = 4;
    ilDataAeraPos = 0;
    cDelimiter = cgEtx[0];

    ilTest = 0;        /* 1 --> test function */ 
    ilScanAll = 0; 
    while ((ilScanAll == 0) && (ilRC == RC_SUCCESS))
    {
        if (ilRC == RC_SUCCESS)     /* get tablename */ 
        {
            while (( ilCmpStrPos < ilLength ) &&
                     ( cpCmpStr[ilCmpStrPos] != cDelimiter ))
            {
                slCondition[ilConditionPos] = cpCmpStr[ilCmpStrPos];
                ilCmpStrPos++;
                ilConditionPos++;
                slCondition[ilConditionPos] = '\0';

                if (slCondition [ilConditionPos-1] == ' ')
                {           /* filter blanks after cond.   */      
                    while (slCondition[ilConditionPos-1] == ' ')
                    {
                        ilConditionPos--;
                        slCondition[ilConditionPos] = '\0';
                    }
                    slCondition[ilConditionPos] = ' ';
                    ilConditionPos++;
                    slCondition[ilConditionPos] = '\0';
                }

                if (ilTest == 1)
                {
                    dbg(DEBUG, "SetCmpStringCond : slConditionStr = <%s>",
                        slCondition);
                }
            }
            ilCmpStrPos++;/* pos behind delimiter -> ABC1,(TAB<ETX>FLD1 >=... */
                          /*                                       ^          */
            if (ilCmpStrPos >= ilLength)
                ilScanAll = 1;
            if (ilTest == 1)
            {
                dbg(DEBUG, "SetCmpStringCond : slConditionStr POS = <%ld>",
                    ilCmpStrPos);
            }
            slCondition[ilConditionPos] = '\0';
        }

        if (ilScanAll != 1)
        {
            if ( ilConditionPos >= 3 )     /* get Tablename */ 
            {
                if (clTabName[0] == '\0')
                {
                    strncpy(clTabName, &cpCmpStr[ilCmpStrPos-4], 3);
                    clTabName[3] = '\0';
                    sprintf(clTabName, "%sTAB", clTabName);
                    if (ilTest == 1)
                    {
                        dbg(DEBUG, "SetCmpStringCond : TABNAME = <%s>",
                            clTabName);
                    }
                }
                if (( cpCmpStr[ilCmpStrPos-1] != cDelimiter ) &&
                    ( ilScanAll != 1 ))
                {
                    if (ilTest == 1)
                    {
                        dbg(DEBUG, "SetCmpStringCond : TABNAME for Condition?");
                    }
                    ilRC = RC_FAIL;
                }
                ilConditionPos -= 3;
                slCondition[ilConditionPos] = '\0';
                if (ilTest == 1)
                {
                    dbg(DEBUG, "SetCmpStringCond : xConditionStr= <%s>",
                        slCondition);
                }
            }
            else
            {
                if (ilTest == 1)
                {
                    dbg(DEBUG, "SetCmpStringCond : condition pos < 3"); 
                }
                ilRC = RC_FAIL;
            }
        }

        if (( ilRC == RC_SUCCESS ) && ( ilScanAll == 0 ))   
        {
            if ( ilCmpStrPos+ilNameFieldlength >= ilLength)
            {                                    /* error in scan fieldlength */
                if (ilTest == 1)
                {
                    dbg(DEBUG, "SetCmpStringCond : Fieldlength not correct");
                }
                ilRC = RC_FAIL;
            }
            else
            {
                strncpy(clActField, &cpCmpStr[ilCmpStrPos], ilNameFieldlength);
                clActField[ilNameFieldlength] = '\0';
                if (ilTest == 1)
                {
                    dbg(DEBUG, "SetCmpStringCond : act field = <%s>",
                        clActField); 
                }

                ilCmpStrPos += ilNameFieldlength;
                
                ilRC = GetFieldLength(clTabName, clActField, &llFieldLen);
                if ((ilRC != RC_SUCCESS) || (llFieldLen == 0))
                {
                    if (ilTest == 1)
                    {
                        dbg(DEBUG,
                            "SetCmpStringCond : GetFieldlength not correct");
                    }
                    ilRC = RC_FAIL;
                }
                else
                {
                    strncpy(clHelpStr, &cpDataArea[ilDataAeraPos], llFieldLen);
                    clHelpStr[llFieldLen] = ' ';
                    clHelpStr[llFieldLen+1] = '\0';
                    strcat(&slCondition[ilConditionPos], " " );
                    strcat(&slCondition[ilConditionPos], clHelpStr);
                    ilDataAeraPos += llFieldLen + 1;
                    ilConditionPos += llFieldLen + 2;  /* for blanks */
                }
            }
        } /* if (RC_SUCCESS && ilScan == 0) */
    } /* while */

  /* set a newline at the end of the condition str */
    strcat(slCondition, "\n");
    if (ilTest == 1) 
    {
        dbg(DEBUG, "SetCmpStringCond : slConditionStr= <%s>", slCondition);
    }
    return (RC_SUCCESS);

} /* function SetCmpStringCond () */ 








static int SetArrayInfo (char *pcpArrayName, char *pcpArrayFieldList, char *pcpIdx01Name, 
                         char *pcpIdx01FieldList, char *pcpIdx01Order, 
                         char *pcpSelection, ARRAYINFO *prpArrayInfo)
{
  int   ilRC   = RC_SUCCESS ;   /* Return code */
  int   ilLoop = 0 ;
  char  clOrderTxt[4] ;

  ilRC = RC_SUCCESS ; 
  if (prpArrayInfo == NULL)
  {
   dbg (TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid") ;
   ilRC = RC_FAIL ;
  }
  else
  {
   memset (prpArrayInfo, 0x00, sizeof(prpArrayInfo) ) ;     /* init prpArrayInfo */ 
   prpArrayInfo->rrArrayHandle = -1 ;
   prpArrayInfo->rrIdx01Handle = -1 ;

   if(pcpArrayName != NULL)
   {
    if(strlen(pcpArrayName) <= ARR_NAME_LEN)
    {
     strcpy(prpArrayInfo->crArrayName,pcpArrayName);       /* set ArrayName */ 
    } /* end of if */
   } /* end of if */

   if (pcpArrayFieldList != NULL)
   {
    if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)        /* set field list */ 
    {                                    
     strcpy(prpArrayInfo->crArrayFieldList, pcpArrayFieldList) ;
    } /* end of if */
   } /* end of if */

   if (pcpIdx01Name != NULL)
   {
    if (strlen(pcpIdx01Name) <= ARR_NAME_LEN)
    {
     strcpy (prpArrayInfo->crIdx01Name, pcpIdx01Name) ;   /* set index name */ 
    } /* end of if */
   } /* end of if */

   if (pcpIdx01FieldList != NULL)
   {
    if (strlen (pcpIdx01FieldList) <= ARR_FLDLST_LEN)    /* set index field list */  
    {
     strcpy (prpArrayInfo->crIdx01FieldList, pcpIdx01FieldList) ;
    } /* end of if */
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {                       /* get items for selected array fields and get space, for every item (long) */ 
   prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList) ;
   prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt, sizeof(long)) ;
   if (prpArrayInfo->plrArrayFieldOfs == NULL)
   {
    dbg (TRACE, "SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {                        /* create for every dield space for the field length */  
   prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
   if (prpArrayInfo->plrArrayFieldLen == NULL)
   {
    dbg (TRACE, "SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
    ilRC = RC_FAIL ;
   }
   else
   {
    *(prpArrayInfo->plrArrayFieldLen) = -1 ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)                            /* write in the complet row length */ 
  {
   ilRC = GetRowLength (pcpArrayName, pcpArrayFieldList, &(prpArrayInfo->lrArrayRowLen)) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: GetRowLength failed <%s> <%s>", pcpArrayName, pcpArrayFieldList) ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)                         /* get space for the act field value */  
  {
   prpArrayInfo->lrArrayRowLen++ ;
   prpArrayInfo->pcrArrayRowBuf = (char *) calloc (1, (prpArrayInfo->lrArrayRowLen+1)) ;
   if (prpArrayInfo->pcrArrayRowBuf == NULL)
   {
    ilRC = RC_FAIL ;
    dbg (TRACE, "SetArrayInfo: calloc failed") ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)        /* get space for the Index list for row length */ 
  {
   ilRC = GetRowLength (pcpArrayName, pcpIdx01FieldList, &(prpArrayInfo->lrIdx01RowLen)) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: GetRowLength failed <%s> <%s>", pcpIdx01Name, pcpIdx01FieldList) ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)                      /* get space for the act index field value */
  {
   prpArrayInfo->lrIdx01RowLen++ ;
   prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1)) ;
   if (prpArrayInfo->pcrIdx01RowBuf == NULL)
   {
    ilRC = RC_FAIL ;
    dbg (TRACE,"SetArrayInfo: calloc failed") ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)                      /* get space(s) (long) for all index fields */  
  {
   prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
   prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
   if (prpArrayInfo->plrIdx01FieldPos == NULL)
   {
    dbg (TRACE, "SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
    ilRC = RC_FAIL;
   }
   else
   {
    *(prpArrayInfo->plrIdx01FieldPos) = -1 ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
   if (prpArrayInfo->plrIdx01FieldOrd == NULL)
   {
    dbg (TRACE, "SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
    ilRC = RC_FAIL ;
   }
   else
   {
    for (ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
    {
     ilRC = get_real_item (&clOrderTxt[0], pcpIdx01Order, ilLoop+1) ;
     if (ilRC > 0)
     {
      switch (clOrderTxt[0])
      {
       case 'A' :
                 prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC ;
               break ;
       case 'D' :
                 prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC ;
               break ;
       default :
                 prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC ;
               break ;
      } /* end of switch */

      ilRC = RC_SUCCESS ;
     }
     else
     {
      prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1 ;
     } /* end of if */
    } /* end of for */
   } /* end of if */
  } /* end of if */

  /* CREATE ARRAYS, FILL ARRAYS AND CREATE INDEXs */ 
  if (ilRC == RC_SUCCESS)
  {
   ilRC = CEDAArrayCreate (&(prpArrayInfo->rrArrayHandle),pcpArrayName,pcpArrayName,
                            pcpSelection,NULL,NULL,pcpArrayFieldList,
                            &(prpArrayInfo->plrArrayFieldLen[0]),
                            &(prpArrayInfo->plrArrayFieldOfs[0]));
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: CEDAArrayCreate failed <%d>", ilRC) ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   ilRC = CEDAArrayFill (&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: CEDAArrayFill failed <%d>",ilRC);
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   ilRC = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: CEDAArrayCreateSimpleIndex failed <%d>",ilRC) ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {                                          /* send update msg. for active changes */ 
   ilRC = ConfigureAction(pcpArrayName,NULL,NULL) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "SetArrayInfo: ConfigureAction failed <%d>", ilRC) ;
   } /* end of if */
  }/* end of if */

  return (ilRC) ;
    
} /* end of SetArrayInfo */


/*===========================================================*/
/* TEST FUNCTIONS !!!                                        */ 
/*===========================================================*/

/*************************************************************/
/* test for the following functions :                        */
/* ----------------------------------  AnalyseStr (....)     */
/*                                     SetCmpStringCond (..) */ 
/*************************************************************/
/* 
static int TestAnalyseStr(void)
{
 char cpSrcStr[200], cpTabname[64], cp1FieldStr[1024] ;  
 char conditionStr[2048], clDataArea[2048] ; 
 long lpFldCnt ;                         
 long lpUsedChars ;  
 long ilI ; 
 int  ilRC ; 

 lpFldCnt = 0 ;
 lpUsedChars = 0 ;
 ilI = 0 ; 
 strcpy (cpSrcStr, 
  "ACT3,(ACT.ACWS >= 41) && (ACT.ACWS <= 49) && (ACT.SEAT < 100)") ; 

 while (cpSrcStr[ilI] != '\0')    /          * set Delimiter *        / 
 {
  if (cpSrcStr[ilI] == '.') cpSrcStr[ilI] = (char) 0x03 ; 
  ilI++ ; 
 }  

 ilRC = AnalyseStr(cpSrcStr, cpTabname, &lpFldCnt, cp1FieldStr, &lpUsedChars) ;
 dbg (DEBUG, "TestAnalyseStr : cmpStr = <%s>", cpSrcStr ) ; 
 dbg (DEBUG, "TestAnalyseStr : Tablename = <%s>", cpTabname ) ; 
 dbg (DEBUG, "TestAnalyseStr : FieldCounter = <%ld>", lpFldCnt ) ; 
 dbg (DEBUG, "TestAnalyseStr : FieldStr = <%s>", cp1FieldStr ) ; 
 dbg (DEBUG, "TestAnalyseStr : UsedChars for cmp str = <%ld>", lpUsedChars ) ; 
 if (ilRC == RC_SUCCESS) 
 {
  dbg (DEBUG, "TestAnalyseStr : AnalyseStr returns RC_SUCCESS" ) ; 
 }
 else 
 {
  dbg (DEBUG, "TestAnalyseStr : AnalyseStr returns FALSE" ) ; 
 }

 if (ilRC == RC_SUCCESS)
 {
  /          * test from SetCmpStringCond() *      /
  strcpy (clDataArea, "173,142,222,333,666,777") ;
  ilI = 0 ;    
  while (clDataArea[ilI] != '\0')    /       *   set Delimiter *       / 
  {
   if (clDataArea[ilI] == ',') clDataArea[ilI] = '\0' ; 
   ilI++ ; 
  }  
  ilRC = SetCmpStringCond (conditionStr, cpSrcStr, clDataArea) ; 
 }

 / * return 0 ;   * /   

} / * end of function TestAnalyseStr() * / 

*/ 





void ReplaceChar ( char *pcpStr, char cpOld, char cpNew )
{
 char *pclPos, *pclAct;
 pclAct = pcpStr;
 while ( pclAct )
 {
  if ( pclPos = strchr ( pclAct, cpOld ) ) *pclPos = cpNew;
  pclAct = pclPos;
 }
}








/*--tested GHe ------------------------------------------------*/
/* Test act. Time/Date (Par.3) for FROM (Par.1) and TO (Par.2) */
/* Time and Date format like "yyyymmddhhmmss"                  */
/* returns :  0 : act date in range                            */ 
/*            1 : < day  [from]                                */ 
/*            2 : < time [from]                                */  
/*            3 : > day  [to]                                  */ 
/*            4 : > time [to]                                  */  
/*-------------------------------------------------------------*/
static int  ChkDateTimeRng (char *pcpFrom, char *pcpTo, char *pcpAct) 
{
 int ilCmpI ; 

 ilCmpI = CmpStrgs (pcpAct, pcpFrom, 8) ;     
 if  (ilCmpI == -1 ) return 1 ;   /* less actDate */     
 if  (ilCmpI == 0 ) 
 {        /* special case !! date are identical -> look for times */ 
  ilCmpI = CmpStrgs (&pcpAct[8], &pcpFrom[8], 6) ; /* time check */      
  if  (ilCmpI == -1 ) return 2 ;   /* less actTime */   
 }

 /* check the <To> date and time */ 
 ilCmpI = CmpStrgs (pcpAct, pcpTo, 8) ;     
 if  (ilCmpI == 1 ) return 3 ;   /* actDate is greater */ 
 if  (ilCmpI == 0) 
 {            /* special case !! date are identical -> look for times */ 
  ilCmpI = CmpStrgs (&pcpAct[8], &pcpTo[8], 6) ; /* time check */      
  if  (ilCmpI == 1 ) return 4 ;   /* greater actTime */      
 }

 return 0 ;   

}  /* ChkDateTimeRng ( )  */ 



/*------------------------------------------------------------------*/
/* Compare 2 strings with the SAME length !!                        */ 
/* returns are :   -1 parString_1 is less then parString_2          */ 
/* -------------    0 both strings are equal                        */ 
/*                  1 parString_1 is bigger as parString_2          */ 
/*------------------------------------------------------------------*/ 
static int CmpStrgs (char *cmpPar1, char *cmpPar2, int ipCmpLen)    
{     
  int     ilI ;
  
  ilI = 0 ; 
  while (ilI < ipCmpLen) 
  {
   if ( (int) cmpPar1[ilI] != (int) cmpPar2[ilI] )
   {
    if ( (int) cmpPar1[ilI] < (int) cmpPar2[ilI] ) return -1 ; 
    return 1 ; 
   }    
   ilI++ ; 
  }
  return 0 ; 
 
} /* end of function CmpStrgs () */ 



/*-------------------------------------------------------------*/
/* Interprets from cpAct the first 2 fields for date           */
/* Time and Date format like "yyyymmddhhmmss", no date allowed */
/* returns :  RC_SUCCESS                                       */ 
/*            RC_FAIL                                          */               
/*-------------------------------------------------------------*/
static int  getDateTimefromString (char *cpFrom, char *cpTo, char *cpAct, char CDIV) 
{
 int i_li, i_li1 ; 
 int ilRC = RC_SUCCESS ; 
 char   cHelpChr ; 

 /* init */ 
 cpFrom[0] = '\0' ; 
 cpTo[0] = '\0' ; 
 ilRC = RC_SUCCESS ;
 i_li = 0 ; 
 i_li1 = 0 ; 

 /* get cpForm */ 
 while ((cpAct[i_li] != '\0') && (cpAct[i_li] != CDIV)) i_li++ ; 
 if (cpAct[i_li] == CDIV)
 {
  cHelpChr = cpAct[i_li] ; 
  cpAct[i_li] = '\0' ; 
  if (strlen (cpAct) > 14) 
  {
   /* ERR - 2 much chars for date   */ 
   ilRC = RC_FAIL ;  
  }
  else 
  {
   strcpy (cpFrom, cpAct) ; 
  }
  if (cHelpChr != '\0') 
  {
   cpAct[i_li] = CDIV ;
  }
  else 
  {
   cpAct[i_li] = '\0' ; 
  }
 }
 else 
 {
  dbg(DEBUG,"<getDateTimefromString> no second date -> use <>") ; 
  ilRC = RC_FAIL;
 }

 if (ilRC == RC_SUCCESS)    /* interpret to time (must be the second string) */  
 {
  i_li++ ; 
  i_li1 = i_li ;                                         /* next par */
  while ((cpAct[i_li] != '\0') && (cpAct[i_li] != CDIV)) i_li++ ; 
  if ((cpAct[i_li] == CDIV) || (cpAct[i_li] == '\0'))    /* found additional pars */ 
  {
   cHelpChr = cpAct[i_li] ; 
   cpAct[i_li] = '\0' ; 
   if (strlen (&cpAct[i_li1]) > 14) 
   {
    /* ERR - 2 much chars for date TO   */ 
    ilRC = RC_FAIL ;  
   }
   else 
   {
    strcpy (cpTo, &cpAct[i_li1]) ; 
   }
   cpAct[i_li] = cHelpChr ;
  }
 }

 return (ilRC) ; 

} /* end of function getDateTimefromString ()   */ 

/*-----------------------------------------------------------------------*/
/* function :  interpretDateInfo ()                                      */ 
/*             interprets a date/Time-string and writes the information  */
/*             to the DATEINFO struct                                    */
/*             On success the return is RC_SUCCESS else RC_FAIL          */ 
/*  pars.:                                                               */ 
/*          charDate       : the interpret date/time as string           */ 
/*          st_Date        : act. date in DATEINFO struct format         */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int interpretDateInfo (char *charDate, DATEINFO *st_Date ) 
{
 char cHelpDate[64] ; 
 int  ilRC = RC_SUCCESS ; 
 int  iLen = 0 ; 
 
 dbg (DEBUG, "interpretDateInfo : START") ; 

 iLen = strlen (charDate) ; 
 if (iLen > 32) 
 {
  memcpy (st_Date->cDateStampStr, charDate, 14) ;
  st_Date->cDateStampStr[14] = '\0' ; 
 }
 else 
 {
  strcpy (st_Date->cDateStampStr, charDate) ;
 }

 dbg (DEBUG, "interpretDateInfo : input str = <%s>", charDate) ; 

 st_Date->iStDateInterpret = 0 ;     /* status for date */  
 st_Date->iStTimeInterpret = 0 ;     /* status for time */ 
 st_Date->iYear  = 0 ;               /* reset all dates */ 
 st_Date->iMonth = 0 ;  
 st_Date->iDate  = 0 ;    
 st_Date->iHour  = 0 ;
 st_Date->iMins  = 0 ;
 st_Date->iSecs  = 0 ; 

 if (iLen >= 8)
 {                                       /* get Date info */ 
  dbg (DEBUG, "interpretDateInfo : get date info") ; 

  strcpy (cHelpDate, charDate) ; 
  cHelpDate[4] = '\0' ; 
  st_Date->iYear = atoi (cHelpDate) ;    /* year */ 

  strcpy (cHelpDate, &charDate[4]) ; 
  cHelpDate[2] = '\0' ; 
  st_Date->iMonth = atoi (cHelpDate) ;   /* month */ 

  strcpy (cHelpDate, &charDate[6]) ; 
  cHelpDate[2] = '\0' ; 
  st_Date->iDate = atoi (cHelpDate) ;    /* day */ 
  st_Date->iStDateInterpret = 1 ;        /* status : 1 -> read a date seq. */  

 }
 else
 {                                       /* cant interpret date */
  dbg (DEBUG,"<interpretDateInfo> cant interpret date (len) => <%s>", charDate);
  ilRC = RC_FAIL ; 
 }

 if (ilRC == RC_SUCCESS)                 /* interpret time string */
 {          
  dbg (DEBUG, "interpretDateInfo : get time info") ; 

  strcpy (cHelpDate, &charDate[8]) ;     /* start of time */  
  if (strlen(cHelpDate) < 6)
  {
   dbg (DEBUG,"<interpretDateInfo> time not interpreted (len) !") ;
   ilRC = RC_FAIL ; 
  }
  else
  {      
   strcpy (cHelpDate, charDate) ; 
   cHelpDate[2] = '\0' ; 
   st_Date->iHour = atoi (cHelpDate) ;    /* hours  */ 

   strcpy (cHelpDate, &charDate[10]) ; 
   cHelpDate[2] = '\0' ; 
   st_Date->iMins = atoi (cHelpDate) ;    /* minutes */ 

   strcpy (cHelpDate, &charDate[12]) ; 
   cHelpDate[2] = '\0' ; 
   st_Date->iSecs = atoi (cHelpDate) ;    /* seconds */
   st_Date->iStTimeInterpret = 1 ;        /* status : 1 -> read a time seq. */  
  }
 }

 dbg (DEBUG, "interpretDateInfo : check sequenz") ; 

 if ((ilRC == RC_SUCCESS) && (st_Date->iStDateInterpret == 1) )    /* SYN CHECK FOR DATE */  
 {
  if ((st_Date->iMonth > 12) || (st_Date->iMonth < 1)) 
  {
   st_Date->iStDateInterpret = 11 ;       /* status : 11 -> wrong month in date seq. */  
   return (RC_FAIL) ;
  } 

  if ((st_Date->iDate < 1) || (st_Date->iDate > 31)) 
  {
   st_Date->iStDateInterpret = 12 ;       /* status : 12 -> wrong day in date seq. */
   return (RC_FAIL) ;
  } 
  
  switch (st_Date->iMonth) 
  { 
   case 2 : if ((((st_Date->iYear - 1900) % 4 ) != 0) && (st_Date->iDate > 28) ) 
            {
             st_Date->iStDateInterpret = 12 ;       /* status : 12 -> wrong day in date seq. */
             return (RC_FAIL) ;
            }
            else   
            {
             if (( ((st_Date->iYear - 1900) % 4 ) == 0) && (st_Date->iDate > 29) ) 
             {
              st_Date->iStDateInterpret = 12 ;       /* status : 12 -> wrong day in date seq. */
              return (RC_FAIL) ;
             }
            }
           break ;

   case  4 : /* months with 30 days */ 
   case  6 :
   case  9 :
   case 11 : if (st_Date->iDate > 30) 
             {
              st_Date->iStDateInterpret = 12 ;       /* status : 12 -> wrong day in date seq. */
              return (RC_FAIL) ;
             } 
           break ; 

   default : /* months with 31 days ! check is done above */

           break ;

  }  /* switch */ 

  st_Date->iStTimeInterpret = 2 ;        /* status : 2 -> date seq. ok */  
 }    /* if rc == RC_SUCCESS */  


 if ((ilRC == RC_SUCCESS)  && (st_Date->iStTimeInterpret == 1) )    /* SYN CHECK FOR TIME */  
 {
  if ((st_Date->iHour > 23) || (st_Date->iHour <= 0))
  {
   st_Date->iStTimeInterpret = 12 ;       /* status : 11 -> wrong hour range in date seq. */
   return (RC_FAIL) ;
  } 

  if ((st_Date->iMins > 60) || (st_Date->iMins <= 0))
  {
   st_Date->iStTimeInterpret = 13 ;       /* status : 11 -> wrong minute range in date seq. */
   return (RC_FAIL) ;
  } 

  if ((st_Date->iSecs > 60) || (st_Date->iSecs <= 0))
  {
   st_Date->iStTimeInterpret = 14 ;       /* status : 11 -> wrong secs range in date seq. */
   return (RC_FAIL) ;
  } 

  st_Date->iStTimeInterpret = 1 ;        /* status : 2 -> correct time seq. */  

 }

 return ( ilRC ) ; 

} /* end of interpretDateInfo () */ 
 
static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
                            char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
                            ARRAYINFO *prpArrayInfo)
{
    int ilRc   = RC_SUCCESS;                /* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];
    long    *pllOrdering=0;

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetAATArrayInfo: invalid last parameter : null pointer not valid");
        return  RC_FAIL;
    }
    
    memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

    prpArrayInfo->rrArrayHandle = -1;
    prpArrayInfo->rrIdx01Handle = -1;
    
    if( pcpArrayName && (strlen(pcpArrayName) <= ARR_NAME_LEN) )
        strcpy(prpArrayInfo->crArrayName,pcpArrayName);
    
    if(pcpArrayFieldList && (strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN) )
        strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

        prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if( prpArrayInfo->plrArrayFieldOfs == NULL )
        {
            dbg(TRACE,"SetAATArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
        else
        {
            prpArrayInfo->plrArrayFieldOfs[0] = 0;
            dbg ( DEBUG, "SetAATArrayInfo: Field Nr <%d> Length <%ld> Offset <%ld>", 0, plpFieldLens[0], 
                   prpArrayInfo->plrArrayFieldOfs[0] );
            for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
            {
                prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
                dbg ( DEBUG, "SetAATArrayInfo: Field Nr <%d> Length <%ld> Offset <%ld>", ilLoop, plpFieldLens[ilLoop], 
                   prpArrayInfo->plrArrayFieldOfs[ilLoop] );
            }/* end of for */
        }
    }/* end of if */


    if(pcpIdx01Name && (strlen(pcpIdx01Name) <= ARR_NAME_LEN) )
        strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);

    if( pcpIdx01FieldList && (strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN) )
        strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetAATArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
            *(prpArrayInfo->plrArrayFieldLen) = -1;
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpArrayFieldList,plpFieldLens,&(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen++;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
            ilRc = RC_FAIL;
            dbg(TRACE,"SetAATArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx01FieldList,plpFieldLens,&(prpArrayInfo->lrIdx01RowLen));
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01RowLen++;
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
        if(prpArrayInfo->pcrIdx01RowBuf == NULL)
        {
            ilRc = RC_FAIL;
            dbg(TRACE,"SetAATArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
        pllOrdering = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
        if(!pllOrdering )
        {
            dbg(TRACE,"SetAATArrayInfo: pllOrdering calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
        {
            for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
            {
                if ( !pcpIdx01Order ||
                     ( get_real_item(clOrderTxt,pcpIdx01Order,ilLoop+1)!=RC_SUCCESS )
                    )                
                    clOrderTxt[0] = 'D';
                if ( clOrderTxt[0] == 'A' )
                    pllOrdering[ilLoop] = ARR_ASC;
                else
                    pllOrdering[ilLoop] = ARR_DESC;
            }/* end of for */
        }/* end of else */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        
        ilRc = AATArrayCreate( &(prpArrayInfo->rrArrayHandle), pcpArrayName,
                               TRUE, 1000, prpArrayInfo->lrArrayFieldCnt,
                               plpFieldLens, pcpArrayFieldList, NULL );
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAAtArrayInfo: AATArrayCreate failed <%d>",ilRc);
    }/* end of if */



    if( (ilRc == RC_SUCCESS) && pcpIdx01Name )
    {
        ilRc = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle),
                                          pcpArrayName,
                                          &(prpArrayInfo->rrIdx01Handle),
                                          pcpIdx01Name,pcpIdx01FieldList,
                                          pllOrdering  );

        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
    }/* end of if */
 
    if ( pllOrdering )
        free (pllOrdering);
    pllOrdering = 0;

    return(ilRc);
    
} /* end of SetAAtArrayInfo */



static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
    int  ilRc        = RC_SUCCESS;          /* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    char clFina[8];
    
    if (pcpTotalFieldList != NULL && pcpFieldList != NULL)
    {
        ilNoOfItems = get_no_of_items(pcpFieldList);

        ilLoop = 1;
        do
        {
            get_real_item(clFina,pcpFieldList,ilLoop);
            if( ( GetItemNo(clFina,pcpTotalFieldList,&ilItemNo) == RC_SUCCESS)  
                && ( ilItemNo > 0 ) )
            {
                llRowLen++;
                llRowLen += plpFieldSizes[ilItemNo-1];
                dbg(DEBUG,"GetLogicalRowLength:  clFina <%s> plpFieldSizes[ilItemNo] <%ld>",clFina,plpFieldSizes[ilItemNo-1]);
            }
            ilLoop++;
        }while(ilLoop <= ilNoOfItems+1);
    }
    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    dbg(DEBUG,"GetLogicalRowLength:  FieldList <%s> plpLen <%ld>",pcpFieldList,*plpLen);
    return(ilRc);
    
} /* end of GetLogicalRowLength */


static int CheckCDT2(char *pcpHopo, CONDINFO psCondition, long *plpIsTrue)
{
    long    llFunc     = ARR_FIRST;
    static long llInBufLen=0;
    static long llOutBufLen=0;
    BOOL    blValidValue;
    BOOL    blEmptyALC3;
    BOOL    blAdhocFlt;
    int     ilRC  = RC_SUCCESS;       /* Return code VBL */
    int     ilRC2 = RC_SUCCESS;       /* Return code VBL */
    int   ilPos    = 0 ;
    int   ilCol    = 0 ;
    int   ilItemNo = 0 ;
    int     ilLocalIotf;
    short   sgSqlFkt;
    short   slCursor   = 0;
    char    clSqlbuf[2048];
    char    clDataArea[2048];
    char    clRefTabField[64];
    char    cpcmpStr[2048];
    char pclALC2[4];
    char pclALC3[4];
    static char   *pclInbValu = NULL;
    static char   *pclOutValu = NULL;
    char   *pclResult  = NULL;             /* READ ONLY !!! */
    char   *pclUrno    = NULL;             /* READ ONLY !!! */
    char   *pclCompValu = NULL;/* value that has to be compared with group members */

    *plpIsTrue  = FALSE;

    dbg(DEBUG, "CDT2: table <%s> field <%s>", psCondition.crTable,
        psCondition.crField);
    if((igEventType == EVT_TURNAROUND) || (igEventType == EVT_INBOUND))
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                              pcgFieldList, pcgInbound, &pclInbValu, &llInBufLen);
        if (ilRC == RC_SUCCESS)
        {
            dbg(DEBUG, "CDT2: inbound field <%s> <%s> found",
                psCondition.crField, pclInbValu);
        }
        else
        {
            dbg(TRACE, "CheckCDT2: inbound field <%s> not found - inbound",
                psCondition.crField);
        } /* end of if GetFieldData2 () == RC_SUCCESS */
    } /* end of if igEventType == EVT_....Turnaround / Inbound  */


    if ((igEventType == EVT_OUTBOUND) || (igEventType == EVT_TURNAROUND)) 
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                            pcgFieldList, pcgOutbound, &pclOutValu, &llOutBufLen);
        if (ilRC == RC_SUCCESS)
        {    
            dbg(DEBUG, "CDT2: outbound field <%s> <%s> found",
                psCondition.crField, pclOutValu);
        }
        else
        {
            dbg(TRACE, "CheckCDT2: outbound field <%s> not found - outbound",
                psCondition.crField);
        } /* end of else GetFieldData2 () == RC_SUCCESS */ 
    } /* end of if igEventType == EVT_....Turnaround / Outbound */

    /*  hag 000912 */
    if (igEventType == EVT_FLIGHT_INDEPENDENT)     
    {
        ilRC = GetFieldData2(psCondition.crTable, psCondition.crField,
                            pcgFieldList, pcgInbound, &pclInbValu, &llInBufLen);
        if (ilRC == RC_SUCCESS)
        {
            psCondition.lrIotf = EVT_FLIGHT_INDEPENDENT;
            dbg(DEBUG, "CDT2: FIN - FI_bound field <%s> <%s> found",
                psCondition.crField, pclInbValu);
        }
        else
        {
            dbg(TRACE, "CDT2: FIN - FI_bound field <%s> not found - FIbound",
                psCondition.crField);
        } /* end of else GetFieldData2 () == RC_SUCCESS */
    } /* end of if igEventType == EVT_FLIGHT_INDEPENDENT  */
    /*  ende hag 000912 */
 
    if(ilRC == RC_SUCCESS)
    {
        switch(igEventType)
        {
            case EVT_TURNAROUND:
                ilLocalIotf = EVT_TURNAROUND;
                if ((psCondition.lrChkInbound == TRUE) &&
                    (psCondition.lrChkOutbound == FALSE))
                {
                    ilLocalIotf = EVT_INBOUND;
                }
                else
                { 
                    if ((psCondition.lrChkInbound == FALSE) &&
                        (psCondition.lrChkOutbound == TRUE))
                    {
                        ilLocalIotf = EVT_OUTBOUND;
                    }
                }
                break;

            case EVT_INBOUND:
                ilLocalIotf = EVT_INBOUND;
                break;
            
            case EVT_OUTBOUND:
                ilLocalIotf = EVT_OUTBOUND;
                break;
            
            case EVT_FLIGHT_INDEPENDENT:
                ilLocalIotf = EVT_FLIGHT_INDEPENDENT;
                break;
            default:
                dbg(TRACE, "CDT2: line=%05d: Invalid Eventtype <%d>",__LINE__,
                    igEventType);
                ilRC = RC_FAIL;
                break;
        }

        if (ilRC == RC_SUCCESS)
        {     /* determine which value has to be compared with group members  */
            switch(ilLocalIotf)
            { 
                case EVT_TURNAROUND : 
                    if ( (pclOutValu[0] != '\0') && (pclInbValu[0] != '\0') &&
                         !strcmp ( pclInbValu, pclOutValu ) )
                    {   /* inbound == outbound != empty */
                        pclCompValu = pclInbValu;
                        dbg(DEBUG,
                            "CDT2: turnaround value <%s> will be compared !",
                            pclCompValu);
                    }
                    break;
                case  EVT_INBOUND : 
                    if ( pclInbValu[0] != '\0' ) 
                    {
                        pclCompValu = pclInbValu;
                        dbg(DEBUG,"CDT2: inbound value <%s> will be compared !",
                            pclCompValu);   
                    }
                    break;
                case  EVT_OUTBOUND: 
                    if ( pclOutValu[0] != '\0') 
                    {
                        pclCompValu = pclOutValu;
                        dbg(DEBUG,
                            "CDT2: outbound value <%s> will be compared !",
                            pclCompValu);   
                    }
                    break;
        
                case  EVT_FLIGHT_INDEPENDENT: 
                    if ( pclInbValu[0] != '\0') 
                    {
                        pclCompValu = pclInbValu;
                        dbg(DEBUG,
                            "CDT2: flight indep. value <%s> will be compared !",                            pclCompValu);   
                    }
                    break;
                default:
                    dbg(TRACE, "Event invalid (%d)", psCondition.lrIotf);
                    ilRC = RC_FAIL;
                    break;
            } /* end of switch */
        }   /* end of if (ilRC == RC_SUCCESS)  ABOVE */

        *plpIsTrue = FALSE;

        if (pclCompValu == NULL)
        {
            dbg(DEBUG, "In- and outbound value are different for turnaround or in(out)bound value is empty for in(out)bound");
            if (bgDiagOn)
            {
                sprintf(clRefTabField, "%s.%s", psCondition.crRefTab, psCondition.crRefFld);
                strcpy ( clSqlbuf, pclInbValu ? pclInbValu : "null" );
                strcpy ( clDataArea, pclOutValu ? pclOutValu : "null" );
                if (bgDiagInt)
                    WriteDiagFile ( DIAGINT, DIAGINBUNEQALAOUTB, clRefTabField, 
                                    clSqlbuf, clDataArea );
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGINBUNEQALAOUTB, clRefTabField, 
                                  clSqlbuf, clDataArea );
            }
        }
        else
        {
            blValidValue = TRUE;
            blEmptyALC3 = TRUE;
            if( !strcmp(psCondition.crRefFld, "ALC2" ) )
            {
                strcpy( pclALC2, pclCompValu );
                TrimRight(pclALC2);
                if( strlen(pclALC2) <= 0 )
                {
                    blValidValue = FALSE;
                    dbg( TRACE, "No ALC2: Get ALC3 from fields collected" );
                    ilRC = FindItemInList( pcgFieldList, "ALC3", ',', &ilItemNo, &ilCol, &ilPos);
                    if( ilRC != RC_SUCCESS || ilItemNo < 0 )
                        dbg( TRACE, "No ALC2: Cannot find ALC3 from the field list recv. fail" );
                    else
                    {
                        if( ilItemNo >= 0 )
                        {
                            memset( pclALC3, 0, sizeof(pclALC3) );
                            if( ilLocalIotf == EVT_OUTBOUND )
                                GetDataItem (pclALC3, pcgOutbound, ilItemNo, ',', "", "\0\0") ;
                            else
                                GetDataItem (pclALC3, pcgInbound, ilItemNo, ',', "", "\0\0") ;
                            dbg( TRACE, "No ALC2: Found ALC3 = <%s>", pclALC3 );
                            TrimRight(pclALC3);
                            if( strlen(pclALC3) > 0 )
                                blEmptyALC3 = FALSE;
                        }
                    }
                }                  
            }
            blAdhocFlt = FALSE;
            if( blValidValue == FALSE && blEmptyALC3 == FALSE ) /* Exist ALC3 */
                ilRC = FindSgrEvaluation(pcpHopo, psCondition.crRefTab, 
                                         "ALC3", psCondition.crValue,
                                         pclALC3, plpIsTrue);
            else if( blValidValue == TRUE )
                ilRC = FindSgrEvaluation(pcpHopo, psCondition.crRefTab, 
                                         psCondition.crRefFld, psCondition.crValue,
                                         pclCompValu, plpIsTrue);
            else 
            {
                ilRC = RC_NOTFOUND;
                if( blValidValue == FALSE && blEmptyALC3 == TRUE )
                {
                    dbg( TRACE, "NoALC2: This is an adhoc flight" );
                    blAdhocFlt = TRUE;
                }
            }
            if( ilRC == RC_SUCCESS )
                dbg( TRACE, "NoALC2: Found evaluation status in memory IsTrue <%ld>", *plpIsTrue );
            
            if ( ilRC!=RC_SUCCESS && blAdhocFlt == FALSE )
            {
                /*  the actual value/Group combination has not been evaluated, yet */
                sprintf(rgSgrArray.pcrIdx01RowBuf, "%s,%s,%s", pcpHopo,
                        psCondition.crValue,psCondition.crRefTab);  /*hag20020605: PRF3021 */
                dbg(DEBUG, "CDT2: pattern <%s>", rgSgrArray.pcrIdx01RowBuf);
                sprintf(cpcmpStr, "('%s' %s ('", psCondition.crValue, psCondition.crOp);
                llFunc = ARR_FIRST;
                pclUrno = NULL;
            
                ilRC = CEDAArrayFindRowPointer( &(rgSgrArray.rrArrayHandle),
                                                &(rgSgrArray.crArrayName[0]),
                                                &(rgSgrArray.rrIdx01Handle),
                                                &(rgSgrArray.crIdx01Name[0]),
                                                rgSgrArray.pcrIdx01RowBuf,
                                                &llFunc,
                                                (void *) &pclResult);
                /* Found the group name in SGRTAB */
                if(ilRC == RC_SUCCESS)  
                {
                    pclUrno = pclResult + rgSgrArray.plrArrayFieldOfs[2];
                    /* create sql statement */
                    clDataArea[0] = '\0';

                    if ((ilRC == RC_SUCCESS) && (pclCompValu != NULL))
                    {
                        if( blValidValue == FALSE && blEmptyALC3 == FALSE )
                            sprintf(clSqlbuf, "SELECT ALC3 from %sTAB where URNO in "
                                              "(SELECT UVAL from SGMTAB where USGR='%s') and ALC3='%s'",
                                              psCondition.crRefTab, pclUrno, pclALC3);
                        else                                        
                            sprintf(clSqlbuf, "select %s from %sTAB where URNO in "
                                    "(select UVAL from SGMTAB where USGR='%s') and %s='%s'",
                                    psCondition.crRefFld, psCondition.crRefTab,
                                    pclUrno, psCondition.crRefFld, pclCompValu);

                        if( ilRC == RC_SUCCESS )
                        {
                            slCursor = 0;
                            sgSqlFkt = START;  
                
                            dbg(DEBUG, "CDT2: sql-Statement <%s>", clSqlbuf);
                            ilRC = sql_if(sgSqlFkt, &slCursor, clSqlbuf, clDataArea);
                    
                            dbg(DEBUG, "CheckCDT2: sqlif return <%d> Dataarea <%s>", ilRC, clDataArea);
                
                            if ((ilRC == DB_SUCCESS) && 
                                ( strstr(clDataArea, pclCompValu) || (blEmptyALC3 == FALSE && strstr(clDataArea, pclALC3)) ))
                            {
                                dbg(DEBUG, "CheckCDT2: sqlif OK  <%s>", &clDataArea[0]);
                                ilRC = RC_SUCCESS;
                                *plpIsTrue = TRUE;  
                            }
                            else
                            {
                                if (bgDiagOn)
                                {
                                    sprintf(clRefTabField, "%s.%s",
                                            psCondition.crRefTab,
                                            psCondition.crRefFld);
                                    if (bgDiagInt)
                                        WriteDiagFile(DIAGINT, DIAGNOTINSGR,
                                                      clRefTabField, pclCompValu,
                                                      psCondition.crValue);
                                    if (bgDiagExt)
                                        WriteDiagFile(DIAGEXT, DIAGNOTINSGR,
                                                      clRefTabField, pclCompValu,
                                                      psCondition.crValue);
                                }
                                dbg(DEBUG, "CheckCDT2: sql cond. not found!");
                            } /* end of if */
                        }
                    } /* value exist */
                       
                    if (ilRC == RC_NOTFOUND)
                    {
                       ilRC = RC_SUCCESS; 
                    }   
                    
                    dbg(DEBUG, "CDT2: closing cursor <%d> pcpIsTrue = %d",
                        slCursor, *plpIsTrue);
                    close_my_cursor(&slCursor);
                }
                if( blAdhocFlt == FALSE )
                {
                    if( blValidValue == FALSE && blEmptyALC3 == FALSE )
                        ilRC2 = SaveSgrEvaluation(pcpHopo, psCondition.crRefTab, 
                                                  "ALC3", psCondition.crValue,
                                                  pclALC3, *plpIsTrue);
                    else 
                        ilRC2 = SaveSgrEvaluation(pcpHopo, psCondition.crRefTab, 
                                                  psCondition.crRefFld, psCondition.crValue,
                                                  pclCompValu, *plpIsTrue);
                    if( ilRC2 != RC_SUCCESS )
                        dbg(TRACE, "CheckCDT2: SaveSgrEvaluation failed GRPN<%s> Value <%s>",
                                   psCondition.crValue, blEmptyALC3 == FALSE ? pclCompValu : pclALC3);
                    if (igTestOutput > 2) 
                        dbg(DEBUG,
                            "CDT2: Test with SQL, <%s> in group <%s> Result <%ld> ilRC <%d>",
                            pclCompValu, psCondition.crValue, *plpIsTrue, ilRC);
                }
            }  
            else /* after/skip memory search */
            {
                if (igTestOutput > 2) 
                    dbg(DEBUG,
                        "CDT2: Test in Array, <%s> in group <%s> Result <%ld>",
                        pclCompValu, psCondition.crValue, *plpIsTrue);
                sprintf(clRefTabField, "%s.%s", psCondition.crRefTab, psCondition.crRefFld);
                if (bgDiagOn && (*plpIsTrue == 0) )
                {
                    if (bgDiagInt)
                        WriteDiagFile(DIAGINT, DIAGNOTINSGR,
                                        clRefTabField, pclCompValu,
                                        psCondition.crValue);
                    if (bgDiagExt)
                        WriteDiagFile(DIAGEXT, DIAGNOTINSGR,
                                        clRefTabField, pclCompValu,
                                        psCondition.crValue);
                }
            }
        } /* exist value to be compared */
    } /* end of if */
/*
    if (pclInbValu != NULL)
    {
        free(pclInbValu);
        pclInbValu = NULL;
    } 

    if (pclOutValu != NULL)
    {
        free (pclOutValu);
        pclOutValu = NULL;
    } */

    if(ilRC == RC_NOTFOUND)
    {
        ilRC = RC_SUCCESS;
        if (igTestOutput > 2) 
        {
            dbg(DEBUG, "CheckCDT2: all static group members processed");
        }
    } /* end of if (ilRC == RC_NOTFOUND) */
    
    return ilRC;
} /* end of CheckCDT2 */


static int FindSgrEvaluation ( char *pcpHopo, char *pcpTable, 
                               char *pcpField, char *pcpGroup,
                               char *pcpValue, long *plpIsTrue ) 
{
    long llRow= ARR_FIRST;  
    int ilRC = RC_FAIL; 
    char *pclResult=0;
    char clKey[201];    /*"HOPO,GRPN,TABN,RFLD,VALU" */
    char *pclEval;
    char clRefTabField[32];

    sprintf(clKey, "%s,%s,%s,%s,%s", pcpHopo, pcpGroup, pcpTable, pcpField,
            pcpValue);
    
    if ( rgSgrEvalArr.rrArrayHandle >= 0 ) 
        ilRC = CEDAArrayFindRowPointer( &(rgSgrEvalArr.rrArrayHandle),  
                                        rgSgrEvalArr.crArrayName,
                                        &(rgSgrEvalArr.rrIdx01Handle), 
                                        rgSgrEvalArr.crIdx01Name, clKey, 
                                        &llRow, (void**)&pclResult ); 
    if ( ilRC ==RC_SUCCESS ) 
    {
        pclEval = pclResult + rgSgrEvalArr.plrArrayFieldOfs[5];
        if ( sscanf ( pclEval, "%ld", plpIsTrue ) < 0 )
        {
            if (bgDiagOn)
            {
                sprintf(clRefTabField, "%s.%s", pcpTable, pcpField);
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGNOTINSGR, clRefTabField, pclEval,
                                    pcpValue);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGNOTINSGR, clRefTabField, pclEval,
                                    pcpValue);
            }
            ilRC = RC_FAIL;
            dbg(TRACE,
                "FindSgrEvaluation:Failed to scan from EVAL <%s>, pattern <%s>",
                pclEval, clKey);
        }
    }                   
    else
    {
        if (igTestOutput > 4) 
            dbg ( DEBUG, "FindSgrEvaluation: Didn't find pattern <%s>", clKey );
    }
    return ilRC;
}

static int SaveSgrEvaluation ( char *pcpHopo, char *pcpTable, 
                               char *pcpField, char *pcpGroup,
                               char *pcpValue, long plpIsTrue ) 
{
    long llRow= ARR_FIRST;  
    int ilRC = RC_FAIL;
    
    memset(cgEvalBuffer, ' ', sizeof(cgEvalBuffer));
    /* "HOPO,GRPN,TABN,RFLD,VALU,EVAL" */
    sprintf(cgEvalBuffer, "%s,%s,%s,%s,%s,%ld", pcpHopo, pcpGroup, pcpTable,
            pcpField, pcpValue, plpIsTrue); 
    delton ( cgEvalBuffer );

    if ( rgSgrEvalArr.rrArrayHandle >= 0 ) 
        ilRC = AATArrayAddRow(&(rgSgrEvalArr.rrArrayHandle), 
                                rgSgrEvalArr.crArrayName, &llRow,
                                (void*)cgEvalBuffer);
    if ( ilRC != RC_SUCCESS )
    {
        dbg(TRACE,
            "SaveSgrEvaluation: AATArrayAddRow failed Group <%s> Value <%s> ilRC <%d>", 
            pcpGroup, pcpValue, ilRC );
    }
    else
    {
        if (igTestOutput > 4) 
        {
            if ( CEDAArrayGetRowCount(&rgSgrEvalArr.rrArrayHandle,
                                       rgSgrEvalArr.crArrayName, &llRow ) !=RC_SUCCESS )
                dbg(TRACE, "SaveSgrEvaluation:CEDAArrayGetRowCount failed");
            else
                dbg(DEBUG, "SaveSgrEvaluation: Added Row <%s> successfully, now %ld rows", 
                      cgEvalBuffer, llRow);
        }
    }
    return ilRC;
}

static int GetItemNo(char *pcpFieldName,char *pcpFields, int *pipItemNo)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    *pipItemNo = 0;

    ilItemCount = get_no_of_items(pcpFields);

    dbg(DEBUG,"GetItemNo itemCount = %d",ilItemCount);  
    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
        get_real_item(pclFieldName,pcpFields,ilLc);
        if(!strcmp(pclFieldName,pcpFieldName))
        {
                *pipItemNo = ilLc;
                dbg(DEBUG,"GetItemNo:%05d: %s",ilLc,pclFieldName);
                ilRc = RC_SUCCESS;
        }
    }

    return ilRc;
}

static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
    long llRow= ARR_FIRST;  
    int ilRC = RC_FAIL ;
    char *pclResult=0;
    char clKey[51]; /*"TANA,FINA" */
    char *pclFele;
    sprintf ( clKey, "%s,%s", pcpTable, pcpField );
    
    if ( rgFeleEvalArr.rrArrayHandle >= 0 ) 
        ilRC = CEDAArrayFindRowPointer( &(rgFeleEvalArr.rrArrayHandle),  
                                        rgFeleEvalArr.crArrayName,
                                        &(rgFeleEvalArr.rrIdx01Handle), 
                                        rgFeleEvalArr.crIdx01Name, clKey, 
                                        &llRow, (void**)&pclResult ); 
    if ( ilRC ==RC_SUCCESS ) 
    {
        pclFele = pclResult + rgFeleEvalArr.plrArrayFieldOfs[2];
        if ( ( pclFele[0] == ' ' ) || ( pclFele[0] == '\0' ) )
        {
            ilRC = RC_FAIL;
            dbg ( TRACE, "FindFeleEvaluation: Found empty FELE, pattern <%s>", 
                  clKey );
        }
        else
        {
            strncpy ( pcpFele, pclFele, 8 );
            if (igTestOutput > 4) 
                dbg ( DEBUG, "FindFeleEvaluation: Found fele <%s>", pclFele );
            pcpFele[8]= '\0';
        }
    }                   
    else
    {
        if (igTestOutput > 4) 
            dbg( DEBUG, "FindFeleEvaluation: Didn't find pattern <%s>", clKey );
    }
    return ilRC;
}

static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
    long llRow= ARR_FIRST;  
    int ilRC =RC_FAIL ;
    
    memset ( cgEvalBuffer, ' ', sizeof(cgEvalBuffer) );
    sprintf ( cgEvalBuffer, "%s,%s,%s", pcpTable, pcpField, pcpFele ); 
    delton ( cgEvalBuffer );
    if ( rgFeleEvalArr.rrArrayHandle >= 0 ) 
        ilRC = AATArrayAddRow( &(rgFeleEvalArr.rrArrayHandle), 
                               rgFeleEvalArr.crArrayName, &llRow,
                               (void*)cgEvalBuffer );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "SaveFeleEvaluation: AATArrayAddRow failed Tana <%s> Fina <%s> ilRC <%d>", 
                      pcpTable, pcpField, ilRC );
    }
    else
    {
        if (igTestOutput > 4) 
        {
            if ( CEDAArrayGetRowCount(&rgFeleEvalArr.rrArrayHandle,
                                       rgFeleEvalArr.crArrayName, &llRow ) !=RC_SUCCESS )
                dbg ( TRACE, "SaveFeleEvaluation: CEDAArrayGetRowCount failed" );
            else
                dbg ( DEBUG, "SaveFeleEvaluation: Added Row <%s> successfully, now %ld rows", 
                      cgEvalBuffer, llRow );
        }
    }
    return ilRC;
}

/*  DeleteFromSgrEval                                                       */
/*  In:  pcpSgrUrno:        Urno of static group                            */
/*  In:  pcpHopo:           concerned hopo                                  */
/*  Delete all calculated evaluations ( from CheckCDT2 ) for Group          */
/*  pcpSgrUrno resp. pcpSgrGrpn from rgSgrEvalArr                           */ 
static int DeleteFromSgrEval ( char *pcpHopo, char *pcpSgrUrno, char *pcpSgrGrpn )
{
    short   slCursor =0;
    char    clSqlbuf[1024] ;
    char    clDataArea[1024]="", *pclResult=0 ;
    short   sgSqlFkt ; 
    int     ilRC=RC_SUCCESS, ilRC1, ilDeleted=0;
    long    llLen;
    long    llFunc = ARR_FIRST ;
    
    /*  One of these Parameters must not be NULL */
    if ( !pcpSgrUrno && !pcpSgrGrpn )
    {
        dbg ( TRACE, "DeleteFromSgrEval: Parameters pcpSgrUrno and pcpSgrGrpn NULL !" );
        return RC_FAIL;
    }
    if ( pcpSgrGrpn )   /* if we already have GRPN, we don't need to use SQL */
        strcpy ( clDataArea, pcpSgrGrpn );
    else
    {
        sprintf ( clSqlbuf, "select GRPN from SGR%s where URNO = '%s'", cgTabEnd, pcpSgrUrno );
        dbg (DEBUG,"DeleteFromSgrEval: sqlbuf=<%s>", clSqlbuf) ;  

        sgSqlFkt = START | REL_CURSOR ;
        ilRC = sql_if (sgSqlFkt, &slCursor, clSqlbuf, clDataArea) ;
        close_my_cursor (&slCursor) ;
        
        if (ilRC == DB_SUCCESS)
        {
            dbg(DEBUG,"DeleteFromSgrEval: sqlif OK  <%s>", clDataArea ) ;   
            ilRC = RC_SUCCESS ;
        }
        else
        {
            dbg (TRACE, "DeleteFromSgrEval: sqlif NOT FOUND" ) ;
            ilRC = RC_NOTFOUND ; 
        } /* end of if */
    }

    if (ilRC == RC_SUCCESS)
    {   /*  "IDXEVAL", "HOPO,GRPN,TABN,RFLD,VALU",  */
        if ( GetFieldLength("SGR","GRPN",&llLen) != RC_SUCCESS )
            llLen = 32;
        clDataArea[llLen] = '\0';
        sprintf ( clSqlbuf, "%s,%s", pcpHopo, clDataArea );
        dbg (DEBUG,"DeleteFromSgrEval: pattern for deletion <%s>", clSqlbuf) ;  
        while ( CEDAArrayFindRowPointer( &(rgSgrEvalArr.rrArrayHandle),
                                         rgSgrEvalArr.crArrayName,
                                         &(rgSgrEvalArr.rrIdx01Handle),
                                         rgSgrEvalArr.crIdx01Name,
                                         clSqlbuf,&llFunc, 
                                         (void *)&pclResult ) == RC_SUCCESS )
        {
            dbg(DEBUG,"DeleteFromSgrEval: Going to delete row <%ld>", llFunc );
            ilRC1 = AATArrayDeleteRow( &(rgSgrEvalArr.rrArrayHandle),
                                       rgSgrEvalArr.crArrayName, llFunc );
            if ( ilRC1 != RC_SUCCESS )
            {
                dbg ( TRACE, "DeleteFromSgrEval: AATArrayDeleteRow Row<%ld> failed, RC<%d>",
                      llFunc, ilRC1 );
                ilRC = ilRC1;
                llFunc = ARR_NEXT;
            }
            else
            {
                ilDeleted ++;
                llFunc = ARR_FIRST;
            }
            dbg(DEBUG,"DeleteFromSgrEval: pattern for deletion <%s>", clSqlbuf);
        }
    } /* if RC_SUCCESS */ 
    if ( ilRC == RC_SUCCESS )
        dbg ( DEBUG, "DeleteFromSgrEval: Deleted %d rows from rgSgrEvalArr", ilDeleted );
    return ilRC;

}


static int BuildTotalFieldList ()
{
    int       ilRC           = RC_SUCCESS ;            /* Return code */
    char      clTabName[8] ;
    char      clKey[8] ;
    char      clFldLst[21] ;
    char      clLastItem[8] ;
    long      llRowLen       = 0 ;
    char     *pclResult      = NULL ;
    char     *pclTmpDst      = NULL ;
    char      *pclNext;
    char      clDel          = ',' ;
    long      llFunc         = ARR_FIRST ;
    long      llFldCnt       = 0 ;
    long     *pllFldPos      = NULL ;
    int      ilPrefLen=5;
    int      ilNoOfItems, ilFound;
    
    dbg (DEBUG,"Start BuildTotalFieldList" ) ;
    
    sprintf ( clTabName,"TPL%s", cgTabEnd );
    strcpy ( clFldLst, "FLDA,FLDD,FLDT");

    if (ilRC == RC_SUCCESS)
    {
        ilRC = GetRowLength ( clTabName, clFldLst, &llRowLen) ;
        if (ilRC != RC_SUCCESS)
        {
            dbg (TRACE, "BuildTotalFieldList: GetRowLength failed <%s> <%s>",
                     clTabName,clFldLst);
        } /* end of if */
    } /* end of if */

  
    if (ilRC == RC_SUCCESS)
    {
        llFldCnt = get_no_of_items (clFldLst) ;
        pllFldPos = (long *) calloc (llFldCnt, sizeof(long)) ;
        if (pllFldPos == NULL)
        {
            ilRC = RC_NOMEM ;
            dbg(TRACE, "BuildTotalFieldList: pllFldPos calloc(%d,%d) failed", llFldCnt, sizeof(long)) ;
        }
        else
            *pllFldPos = -1 ;
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        llFunc = ARR_FIRST ;
    
        sprintf ( clKey, "%d", TPL_ACTIVE ) ;
        llFunc = ARR_FIRST ;
        ilPrefLen = strlen ( ".AFT." );     
        cTmpInBuffer[0] = '\0';
        pclTmpDst = cTmpInBuffer;
        strcpy( pclTmpDst, cgAddFields );
        pclTmpDst += strlen ( pclTmpDst );

        do
        {
            pclResult = NULL ;
            ilFound = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                           rgTplArray.crArrayName, 
                                           &(rgTplArray.rrIdx01Handle),
                                           rgTplArray.crIdx01Name, clKey,
                                           &llFunc,(void *) &pclResult);
            if (ilFound == RC_SUCCESS)
            {
                ilRC = CEDAArrayGetFields( &(rgTplArray.rrArrayHandle),rgTplArray.crArrayName,
                                           pllFldPos, clFldLst, clDel, llRowLen, llFunc, 
                                           rgTplArray.pcrArrayRowBuf );
                if (ilRC == RC_SUCCESS)
                {
                    pclNext = rgTplArray.pcrArrayRowBuf;
                    while ( pclNext =  strstr ( pclNext, ".AFT.") )
                    {
                        pclNext += ilPrefLen;
                        if ( strlen(pclNext)>= RUL_FLD_LEN )
                        {
                            strncpy ( clLastItem, pclNext, RUL_FLD_LEN);
                            clLastItem[RUL_FLD_LEN] = '\0';
                            TrimRight ( clLastItem );
                /*  workaround due to problems in flight-process:           */
                /*  don't insert field DDLR into field list, flight will    */
                /*  send it anyhow, if DDLF is required                     */
                            if ( strcmp ( clLastItem, "DDLR" ) == 0 )
                                continue;   /* end of workaround hag20010202 */

                            if ( !strstr ( cTmpInBuffer, clLastItem ) )
                            {   /* add field, if not yet in Fieldlist */
                                if ( cTmpInBuffer[0] == '\0' )
                                    strcpy ( pclTmpDst, clLastItem );   /* only if list is empty */
                                else
                                    sprintf ( pclTmpDst, ",%s", clLastItem );
                                pclTmpDst += strlen ( pclTmpDst );
                            }
                        }
                    }
                }
                else
                {
                     dbg (TRACE, "BuildTotalFieldList: CEDAArrayGetFields failed <%d>",ilRC);
                } /* end of if */
            } /* end of if */
            llFunc = ARR_NEXT;
        } while ( (ilRC == RC_SUCCESS) && (ilFound==RC_SUCCESS) );
    }
    if (ilRC == RC_SUCCESS)
    {
        pcgTotalFieldList = (char *) realloc( pcgTotalFieldList, 
                                              strlen(cTmpInBuffer) + 1 );
        if ( !pcgTotalFieldList )
        {
            ilRC = RC_NOMEM;
            dbg(TRACE, "BuildTotalFieldList: realloc for TotalFieldList failed");
        }
        else
        {
            memset(pclTmpDst,0x00,RUL_FLD_LEN+1);
    
            ilNoOfItems = get_no_of_items(cTmpInBuffer);
            qsort((void *)cTmpInBuffer,ilNoOfItems-1,RUL_FLD_LEN+1,MyCompare);
    
            dbg(DEBUG,"BuildTotalFieldList:   sorted fields <%d> <%s>",ilNoOfItems,cTmpInBuffer);

            strcpy ( pcgTotalFieldList, cTmpInBuffer );
        }
    }

    if (pllFldPos != NULL)
    {
        free (pllFldPos) ;
        pllFldPos = NULL ;
    } /* end of if */
    if ( pcgTotalFieldList && (ilRC == RC_SUCCESS) )
        dbg(DEBUG, "BuildTotalFieldList ok");
    else
        dbg (DEBUG, "BuildTotalFieldList RC <%d>", ilRC ) ;
    return(ilRC) ;
}

void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
    s[++i] = '\0';
}

static int SendFieldListToOne (int ipDest)
{
    int       ilRC           = RC_SUCCESS ;            /* Return code */
    int       ilEventLen     = 0 ;
    EVENT    *prlEvent ;
    BC_HEAD  *prlBchead      = NULL ;
    CMDBLK   *prlCmdblk      = NULL ;
    char     *pclSelection   = NULL ;
    char     *pclFields      = NULL ;
    char     *pclData        = NULL ;

    if (igTestOutput > 2) 
    {
        dbg (DEBUG,"SendFieldListToOne: dest <%d>",ipDest) ;
    }

    if ( !pcgTotalFieldList )
        ilRC = BuildTotalFieldList ();
    if ( pcgTotalFieldList && (ilRC==RC_SUCCESS) )
    {
        ilEventLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 1 + 
                     strlen(pcgTotalFieldList) + 1;
        prlEvent = (EVENT *) calloc (1, ilEventLen) ;
        if (prlEvent == NULL)
        {
            ilRC = RC_FAIL;
            dbg (TRACE, "SendFieldListToOne: calloc failed") ;
        }
    }
    
    if ( pcgTotalFieldList && (ilRC==RC_SUCCESS) )
    {
        prlBchead    = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        pclSelection = prlCmdblk->data ;
        pclFields    = pclSelection + strlen (pclSelection) + 1 ;
        strcpy ( pclFields, pcgTotalFieldList );

        pclData      = pclFields + strlen(pclFields) + 1 ;
        *pclData     = 0x00 ;

        prlEvent->type = SYS_EVENT ;
        prlEvent->command = EVENT_DATA ;
        prlEvent->originator = mod_id ;
        prlEvent->retry_count = 0 ;
        prlEvent->data_offset = sizeof(EVENT) ;
        prlEvent->data_length = ilEventLen - sizeof(EVENT) ;
        strcpy (prlEvent->sys_ID, mod_name) ;

        strcpy(prlBchead->dest_name, mod_name) ;
        strcpy(prlBchead->recv_name, "flight") ;

        strcpy(prlCmdblk->command,  "SFL") ;
        if (igSearchTableAction == 1)             /* load AZA vals */ 
        { 
            strcpy (prlCmdblk->obj_name, "AZA") ;   /* "AZA" */ 
        }
        else 
        {
            strcpy (prlCmdblk->obj_name, "AFT") ;   /* "AFT" */ 
        }
        strcpy (prlCmdblk->obj_name, "AFT") ;/* "AFT" *//* for a little time */

        strcpy (prlCmdblk->tw_start, "RMS") ;
        sprintf (prlCmdblk->tw_end, "%s, TAB, INIT", &cgHopo[0]) ;
        if (igTestOutput > 2 ) 
        {
            DebugPrintEvent (DEBUG, prlEvent) ;          
            DebugPrintBchead (DEBUG, prlBchead) ;          
            DebugPrintCmdblk (DEBUG, prlCmdblk) ;         
            dbg (DEBUG,"selection <%s>", pclSelection) ;  
            dbg (DEBUG,"fields    <%s>", pclFields) ;     
            dbg (DEBUG,"data      <%s>", pclData) ;      
        }

        ilRC = que (QUE_PUT, ipDest, mod_id, PRIORITY_4, ilEventLen, (char *)prlEvent) ;
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent ( TRACE, prlEvent) ; 
            DebugPrintBchead ( TRACE, prlBchead) ;   
            DebugPrintCmdblk ( TRACE, prlCmdblk) ;  
            dbg (TRACE,"selection <%s>",pclSelection) ;
            dbg (TRACE,"fields    <%s>",pclFields) ;
            dbg (TRACE,"data      <%s>",pclData) ;
        } /* end of if */
    } /* end of if */

    if (prlEvent != NULL)
    {
        free (prlEvent) ;
        prlEvent = NULL ;
    } /* end of if */

    if (igTestOutput > 3)
    {
        dbg (DEBUG, "SendFieldList2One END ") ;
    }
    return(ilRC) ;

} /* end of SendFieldListToOne */

/*  UpdateAddFields: Add fields of pcpFields to cgAddFields, if not yet     */
/*                   part of cgAddFields or pcgTotalFieldList               */
/*  IN:              pcpFields, comma-separated list of AFT-fields          */
/*  OUT:             *ipChanged, no. of fields, that have been added to     */
/*                   cgAddFields                                            */
static int UpdateAddFields ( char *pcpFields, int *ipChanged )
{
    char    clFina[11], *pclTmpDst;
    int     ilItemCount, ilLoop=1;
    int     ilRC=RC_SUCCESS;

    if ( !pcpFields || !ipChanged )
    {
        dbg ( TRACE, "UpdateAddFields: At least one parameter NULL" );
        return RC_FAIL;
    }
    else
    {
        dbg ( DEBUG,"UpdateAddFields: pcpFields <%s> cgAddFields <%s>", 
              pcpFields, cgAddFields );
        if( pcgTotalFieldList )
            dbg ( DEBUG,"UpdateAddFields: pcgTotalFieldList <%s>", 
                 pcgTotalFieldList );
        else
            dbg ( DEBUG,"UpdateAddFields: pcgTotalFieldList is NULL" );
    }
    *ipChanged = 0;
    pclTmpDst = cgAddFields + strlen(cgAddFields);
    ilItemCount = get_no_of_items(pcpFields);

    while ((ilRC == RC_SUCCESS) && (ilLoop <= ilItemCount)) 
    {
        if ( get_real_item(clFina,pcpFields,ilLoop) <0 )
        {
            ilRC = RC_FAIL;
            ilLoop++;
        }
        else
        {
            ilLoop++;
            clFina[RUL_FLD_LEN] = '\0';
            TrimRight ( clFina );

            /*  workaround due to problems in flight-process:           */
            /*  don't insert field DDLR into field list, flight will    */
            /*  send it anyhow, if DDLF is required                     */
            if ( strcmp ( clFina, "DDLR" ) == 0 )
                continue;   /* end of workaround hag20010202 */

            if ( strstr ( cgAddFields, clFina ) )
                continue;   /*  field already in cgAddFields */
            if ( pcgTotalFieldList && strstr ( pcgTotalFieldList, clFina ) )
                continue;   /*  field already in pcgTotalFieldList */
            /* add field, if not yet in Fieldlist */
            if ( cgAddFields[0] == '\0' )
                strcpy( pclTmpDst, clFina );    /* only if list is empty */
            else
                sprintf ( pclTmpDst, ",%s", clFina );
            pclTmpDst += strlen ( pclTmpDst );
            (*ipChanged) ++;
        }
    } 
    dbg ( DEBUG,"UpdateAddFields: ilRC <%d> changed %d Fields,  cgAddFields now <%s>", 
          ilRC, *ipChanged, cgAddFields );
    return RC_SUCCESS;
}

static int GetDataFromBuffer(char *pcpBuffer, char *pcpFieldList,
                             char *pcpTable, char *pcpFields, char *pcpData,
                             int ipFldCnt)
{
    int     ilRC = RC_SUCCESS;
    int     i,j;
    int     ilDataPos = 0, ilFieldPos = 0;
    long    llLen;
    char    clFieldNam[16];
    static char *pclFieldData=0;
    static long llBufLen=0;

    for ( i = 0; (i < ipFldCnt) && (ilRC == RC_SUCCESS); i++)
    {
        get_real_item(clFieldNam,pcpFields,i+1);
        
        j = 0;
        ilFieldPos = 0;
        while(clFieldNam[j] == ' ')
        {
            j++;
        }
        ilFieldPos += j;

        ilRC = GetFieldData2( pcpTable, &clFieldNam[ilFieldPos], pcpFieldList,
                             pcpBuffer, &pclFieldData, &llBufLen);
        if ( ilRC == RC_SUCCESS)
        {
            ilRC = GetFieldLength (pcpTable, clFieldNam, &llLen) ;
            sprintf(&pcpData[ilDataPos],"%-*.*s",llLen,llLen,pclFieldData);
            ilDataPos += llLen+1;
        }
    }

    return ilRC;
}

/*  Convert characters (mapped by CCSClasslib) in pcpString back to original characters */
void UndoCharMapping ( char* pcpString )
{
    unsigned int ilLen, i;

    ilLen = min ( strlen(cgClientChars), strlen(cgServerChars) );
    for ( i=0; i<ilLen; i++ )
        ReplaceChar ( pcpString, cgServerChars[i], cgClientChars[i] ) ;
}

static int CompareTplUrno(char *pcpUrno, char *pcpSelection)
{
    int     ilRC = RC_SUCCESS;
    int     ilPos    = 0;
    int     ilCol    = 0;
    int     ilItemNo = 0;
    char    clDel    = ',';
    char    clUrno[URNOLEN+1];

    strcpy(clUrno, pcpUrno);
    TrimRight(clUrno);

    ilRC = FindItemInList(pcpSelection, clUrno, clDel, &ilItemNo, &ilCol,
                            &ilPos);

    return ilRC;
}

static int OpenDiagFile( char *pcpFlight )
{
    int     ilRC = RC_SUCCESS;
    char    clFileNameWithPath[128];

    if (bgDiagInt)
    {
        sprintf(&clFileNameWithPath[0], "%s/%s%s", getenv("DBG_PATH"),
                 pcpFlight, "rul.diag");

        pfgDiagIntOut = fopen(clFileNameWithPath,"w");
        if ( pfgDiagIntOut == NULL )
            ilRC = RC_FAIL;
    }

    if (bgDiagExt)
    {
        sprintf(&clFileNameWithPath[0], "%s/ruldiag/%s%s", getenv("DBG_PATH"),
                 pcpFlight, "rul.diag");
        pfgDiagExtOut = fopen(clFileNameWithPath,"w");
        if ( pfgDiagExtOut == NULL )
        {
            dbg ( TRACE, "OpenDiagFile: Unable to open file %s, turn diagnosis off", 
                  clFileNameWithPath );
            ilRC = RC_FAIL;
            bgDiagExt = FALSE;
        }
    }
    return ilRC;
}

static int WriteDiagFile(int ipExt, int ipNumb, char *pcpPar1, char *pcpPar2,
                            char *pcpPar3)
{
    int     ilRC = RC_SUCCESS;
    char    clDiagText[1024];
    char    clNumb[NUMBLEN+1];
    char    *pclResult = NULL;
    long    llFunc = ARR_FIRST;
    char    *pclMaske = NULL;

    sprintf(clNumb, "%*.*d", NUMBLEN, NUMBLEN, ipNumb);
    ilRC = CEDAArrayFindRowPointer(&(rgDiagInArray.rrArrayHandle),
                                     rgDiagInArray.crArrayName,
                                     &(rgDiagInArray.rrIdx01Handle),
                                     rgDiagInArray.crIdx01Name, clNumb,
                                     &llFunc,(void *) &pclResult);
    if ( ilRC == RC_SUCCESS)
    {
        pclMaske = pclResult + rgDiagInArray.plrArrayFieldOfs[1];
        sprintf(clDiagText, pclMaske, pcpPar1, pcpPar2, pcpPar3);
        TrimRight(clDiagText);
        strcat(clDiagText, "\n");

        if (ipExt)
            fwrite(clDiagText, strlen(clDiagText), 1, pfgDiagExtOut);
        else
            fwrite(clDiagText, strlen(clDiagText), 1, pfgDiagIntOut);
    }
    else
        dbg ( DEBUG, "WriteDiagFile: No entry found key <%s> Number <%d>", 
              clNumb, ipNumb );

    return ilRC;
}

static int CheckDiagFlag(char *pcpSelection)
{
    int     ilRC = RC_SUCCESS;
    char    clDel = '|';
    char    *pclTmp = NULL;

    pclTmp = strchr(pcpSelection, clDel);
    if (pclTmp != NULL)
    {
        if (strcmp(&pclTmp[1], "1") == 0)
        {
            bgDiagOn = TRUE;
        }
        /* *pclTmp = 0x00; */
    }

    return ilRC;
}

static int ReadDiagIn( void )
{
    int     ilRC = RC_SUCCESS;
    char    clFileNameWithPath[128];
    char    clReadBuffer[134];
    char    clDiagNumb[NUMBLEN+1];
    char    clDiagText[DIAGTEXTLEN+1];
    long    llRow = ARR_NEXT;

    sprintf(&clFileNameWithPath[0], "%s/rul.txt", getenv("CFG_PATH"));

    pfgDiagIn = fopen(clFileNameWithPath,"r");
    if ( pfgDiagIn == NULL )
        return RC_FAIL;

    while ( fgets(clReadBuffer, 134, pfgDiagIn) != NULL )
    {
        strncpy(clDiagNumb,clReadBuffer,NUMBLEN);
        clDiagNumb[NUMBLEN] = '\0';
        strcpy(clDiagText,(clReadBuffer + NUMBLEN+1));
        memset(cgDiagBuffer, 0, sizeof(cgDiagBuffer));
        if (rgDiagInArray.rrArrayHandle >= 0) 
            ilRC = AATArrayAddRow( &(rgDiagInArray.rrArrayHandle), 
                                   rgDiagInArray.crArrayName, &llRow,
                                       (void*)cgDiagBuffer );
        if ( ilRC != RC_SUCCESS )
        {
            dbg(TRACE, "ReadDiagIn: AATArrayAddRow failed ilRC <%d>", 
                ilRC);
        }
        else
        {
            sprintf(cgDiagBuffer, "%s,%s", clDiagNumb, clDiagText); 
            ilRC = CEDAArrayPutFields(&rgDiagInArray.rrArrayHandle,
                                       rgDiagInArray.crArrayName, 0, &llRow,
                                       "NUMB,TEXT", cgDiagBuffer);
        }
    }

    if (debug_level > 0)
        SaveIndexInfo(&rgDiagInArray);

    fclose(pfgDiagIn);

    return ilRC;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int     ilRC = RC_SUCCESS;
    char    clCfgValue[264];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
                            clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
        dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
        if (!strcmp(clCfgValue, "DEBUG"))
            *pipMode = DEBUG;
        else if (!strcmp(clCfgValue, "TRACE"))
            *pipMode = TRACE;
        else
            *pipMode = 0;
    }

    return ilRC;
}

static int LocalToUtc(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clLocal[32];

    strcpy(clLocal,pcpTime);
    if ( cgTich[0] )
    {
        pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
        AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
        dbg(DEBUG,"LocalToUtc: <%s> -> <%s> ", clLocal, pcpTime);
    }
    return ilRc;
}

static int UtcToLocal(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clUtc[32];

    strcpy(clUtc,pcpTime);
    if ( cgTich[0] )
    {
        pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
        AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
        dbg(DEBUG,"UtcToLocal: <%s> -> <%s> ", clUtc, pcpTime);
    }
    return ilRc;
}

static int InitTichTdi()
{
    /** read TICH,TDI1,TDI2 ***********/
    char pclDataArea[2256];
    char pclSqlBuf[256];
    short slCursor = 0;
    int   ilRc=RC_SUCCESS;

    sprintf(pclSqlBuf,
        "SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
    ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
    if (ilRc != DB_SUCCESS)
    {
        dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
    } /* end while */
    else
    {
        strcpy(cgTdi1,"60");
        strcpy(cgTdi2,"60");
        get_fld(pclDataArea,0,STR,14,cgTich);
        get_fld(pclDataArea,1,STR,14,cgTdi1);
        get_fld(pclDataArea,2,STR,14,cgTdi2);
        TrimRight(cgTich);
        if (*cgTich != '\0')
        {
            TrimRight(cgTdi1);
            TrimRight(cgTdi2);
            sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
            sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
        }
        dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
            cgTich,cgTdi1,cgTdi2);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;
    return ilRc;
}

static int GetBeginOfDay(char *pcpTime, char *pcpBegin)
{
    int     ilRc = RC_SUCCESS;
    char    clDate[DATELEN+1];

    strcpy(clDate, pcpTime);
    UtcToLocal(clDate);
    clDate[8] = '\0';
    strcat(clDate, "000000");
    LocalToUtc(clDate);
    strcpy(pcpBegin, clDate);

    return ilRc;
}
 
static int  CheckFirTemplates(char *pcpHopo, char *pcpPipe)
{
    int     ilRc = RC_SUCCESS;
    int     ilItemNo = 1;
    long    llRow;
    char    clDel = ',';
    char    clTplUrno[URNOLEN+1];
    char    *pclAppl = NULL;
    char    *pclResult;
    BOOL    blNotFirst = FALSE;

    while(GetDataItem(clTplUrno, pcpPipe+1, ilItemNo, clDel, "", "\0\0") > 0)
    {
        llRow = ARR_FIRST;
        pclResult = NULL;
        sprintf(rgTplArray.pcrIdx01RowBuf, "%d,%s,%s", TPL_ACTIVE, pcpHopo,
                clTplUrno);
        dbg(DEBUG, "Key for Tpl <%s>", rgTplArray.pcrIdx01RowBuf);
        ilRc = CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
                                        &(rgTplArray.crArrayName[0]),
                                        &(rgTplArray.rrIdx01Handle),
                                        &(rgTplArray.crIdx01Name[0]),
                                        rgTplArray.pcrIdx01RowBuf,&llRow,
                                        (void *) &pclResult);
        if (ilRc == RC_SUCCESS)
        {
            pclAppl = pclResult + rgTplArray.plrArrayFieldOfs[15];
            dbg(DEBUG, "TPL.URNO <%s>\tTPL.APPL <%s>", clTplUrno, pclAppl);
            if (!(strcmp(pclAppl, "RULE_FIR")))
            {
                if (blNotFirst)
                    strcat(cgFirTemplates, ",");
                strcat(cgFirTemplates, clTplUrno);
                blNotFirst = TRUE;
            }
        }
        ilItemNo++;
    }
    dbg(DEBUG, "FIR-Templates <%s>", cgFirTemplates);

    return ilRc;
}


static int CheckFtypForTpl ( int ipEvtType, char *pclTplRow, int *pipTypeToEval )
{
    char    *pclFtya=0, *pclFtyd=0, *pclTnam=0;
    static char *pclFtypIn=0, *pclFtypOut=0;
    static long llFtypInLen=0, llFtypOutLen=0;
    int     ilInboundFtyp=RC_FAIL, ilOutboundFtyp=RC_FAIL;
    int     ilRC = RC_SUCCESS;
    BOOL    blFtypeInTpl = FALSE;
    char    clTnam[65] = "TPL";
    BOOL    blUseInbound = FALSE;
    BOOL    blUseOutbound = FALSE;
    
    if ( pcgFieldList && pipTypeToEval )
    {
        *pipTypeToEval = ipEvtType;
        if ((ipEvtType==EVT_INBOUND) || (ipEvtType==EVT_TURNAROUND))
        {
            ilRC = GetFieldData2( "AFT", "FTYP", pcgFieldList, pcgInbound, 
                                 &pclFtypIn, &llFtypInLen );
            blUseInbound = TRUE;
        }
        if ((ipEvtType==EVT_OUTBOUND) || (ipEvtType==EVT_TURNAROUND))
        {
            ilRC |= GetFieldData2( "AFT", "FTYP", pcgFieldList, pcgOutbound, 
                                  &pclFtypOut, &llFtypOutLen );
            blUseOutbound = TRUE;
        }
    }
    else
        ilRC = RC_FAIL;
    if ( ilRC != RC_SUCCESS )
        dbg ( TRACE, "CheckFtypForTpl: Unable to read FTYP RC <%d>", ilRC );
    else
    {
        if ( blUseInbound && pclFtypIn && (pclFtypIn[0]==' ' ) )
            pclFtypIn[0]='_';
        if ( blUseOutbound && pclFtypOut && (pclFtypOut[0]==' ' ) )
            pclFtypOut[0]='_';
        if ( ( igTplFTYA > 0 ) && ( igTplFTYD > 0 ) )
        {
            pclFtya = pclTplRow + rgTplArray.plrArrayFieldOfs[igTplFTYA-1];
            pclFtyd = pclTplRow + rgTplArray.plrArrayFieldOfs[igTplFTYD-1];
            pclTnam = pclTplRow + rgTplArray.plrArrayFieldOfs[3];
            if ( pclTnam && *pclTnam )
            {
                strncpy ( clTnam, pclTnam, 64 );
                clTnam[64] = '\0';
                TrimRight (clTnam );
            }
        }

        /*  TPLTAB.FTYA and TPLTAB.FTYD existing and filled */
        if ( pclFtya && pclFtya[0] && (pclFtya[0]!=' ') &&
             pclFtyd && pclFtyd[0] && (pclFtyd[0]!=' ') )
        {
            blFtypeInTpl = TRUE;            
        }

        if  ( blUseInbound && pclFtypIn )
        {
            if ( blFtypeInTpl )
            {
                if ( strchr ( pclFtya, pclFtypIn[0] ) )
                {       
                    dbg(DEBUG, "CheckFtypForTpl: Use Inbound FTYP <%s> (configured in TPL <%s)", pclFtypIn, pclFtya );
                    ilInboundFtyp = RC_SUCCESS;
                }
                else
                    dbg(TRACE, "CheckFtypForTpl: Ignore Inbound FTYP <%s> (not configured in <%s> <%s>)", pclFtypIn, clTnam, pclFtya );
            }
            else
            {
                if ( !strchr ( cgIgnoreFtypIn, pclFtypIn[0] ) )
                {       
                    dbg(DEBUG, "CheckFtypForTpl: Use Inbound FTYP <%s> (see rulfnd.cfg)", pclFtypIn );
                    ilInboundFtyp = RC_SUCCESS;
                }
                else
                    dbg(TRACE, "CheckFtypForTpl: Ignore Inbound FTYP <%s> (see rulfnd.cfg)", pclFtypIn );
            }
            if ( ( ilInboundFtyp != RC_SUCCESS ) && bgDiagOn )
            {
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGINBFTYP, pclFtypIn, NULL, NULL);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGINBFTYP, pclFtypIn, NULL, NULL);
            }
        }

        if  ( blUseOutbound && pclFtypOut )
        {
            if ( blFtypeInTpl )
            {
                if ( strchr ( pclFtyd, pclFtypOut[0] ) )
                {       
                    dbg(DEBUG, "CheckFtypForTpl: Use Outbound FTYP <%s> (configured in TPL <%s>)", pclFtypOut, pclFtyd );
                    ilOutboundFtyp = RC_SUCCESS;
                }
                else
                    dbg(TRACE, "CheckFtypForTpl: Ignore Outbound FTYP <%s> (not configured in <%s> <%s>)", pclFtypOut, clTnam, pclFtyd );
            }
            else
            {
                if ( !strchr ( cgIgnoreFtypOut, pclFtypOut[0] ) )
                {       
                    dbg(DEBUG, "CheckFtypForTpl: Use Outbound FTYP <%s> (see rulfnd.cfg)", pclFtypOut );
                    ilOutboundFtyp = RC_SUCCESS;
                }
                else
                    dbg(TRACE, "CheckFtypForTpl: Ignore Outbound FTYP <%s> (see rulfnd.cfg)", pclFtypOut );
            }
            if ( ( ilOutboundFtyp != RC_SUCCESS ) && bgDiagOn )
            {
                if (bgDiagInt)
                    WriteDiagFile(DIAGINT, DIAGOUTBFTYP, pclFtypOut, NULL, NULL);
                if (bgDiagExt)
                    WriteDiagFile(DIAGEXT, DIAGOUTBFTYP, pclFtypOut, NULL, NULL);
            }
        }
        if ( (ilInboundFtyp==RC_SUCCESS) && (ilOutboundFtyp==RC_SUCCESS) )
        {
            *pipTypeToEval = EVT_TURNAROUND;
            dbg(DEBUG, "CheckFtypForTpl: Identified TURNAROUND event");
        }
        else if ( ilInboundFtyp==RC_SUCCESS )
        {
            *pipTypeToEval = EVT_INBOUND;
            dbg(DEBUG, "CheckFtypForTpl: Identified INBOUND event");
        }
        else if ( ilOutboundFtyp==RC_SUCCESS )
        {
            *pipTypeToEval = EVT_OUTBOUND ;
            dbg(DEBUG, "CheckFtypForTpl: Identified OUTBOUND event");
        }
        else
        {
            dbg(DEBUG, "CheckFtypForTpl: Flight(s) won't be handled");
            *pipTypeToEval = EVT_UNKNOWN;
        }   
    }
    /*
    if ( pclFtypIn )
        free ( pclFtypIn );
    if ( pclFtypOut )
        free ( pclFtypOut );*/
    return ilRC;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/******************************************************************************/

static int GetFieldData2(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, char *pcpData, char **pppFieldData,  long *lpBufLen )
{
    int   ilRC     = RC_SUCCESS ;       /* Return code */
    int   ilPos    = 0 ;
    int   ilCol    = 0 ;
    int   ilItemNo = 0 ;
    long  llLen    = 0 ;
    char  clDel    = ',' ;

    ilRC = FindItemInList (pcpFldLst, pcpFldNam, clDel, &ilItemNo, &ilCol, &ilPos) ;
    if (ilRC == RC_SUCCESS)
    {
        ilRC = GetFieldLength (pcpTabNam, pcpFldNam, &llLen) ;
        if ( llLen<=0 )
        {
            ilRC = RC_FAIL;
            if ( pcpTabNam && pcpFldNam )
                dbg (TRACE, "GetFieldData2: GetFielLen for <%s.%s> returned Len <%ld>", pcpTabNam, pcpFldNam, llLen ) ;      
            else
                dbg (TRACE, "GetFieldData2: GetFielLen returned Len <%ld>", llLen ) ;      
        }
        if (ilRC == RC_SUCCESS)
        {
            llLen++ ;
            if ( llLen > *lpBufLen )
            {
                *pppFieldData = (char*)realloc ( *pppFieldData, llLen ) ;
                if (*pppFieldData == NULL)
                {
                    dbg (TRACE, "GetFieldData2: realloc(%ld) failed", llLen) ;  
                    *lpBufLen = 0;
                    ilRC = RC_FAIL ;
                } /* end of if */
                else
                    *lpBufLen = llLen;
            }
            if (ilRC == RC_SUCCESS)
            {
                ilRC = GetDataItem (*pppFieldData, pcpData, ilItemNo, clDel, "", "\0\0");
                dbg(TRACE,"GetFieldData2:  RC=%d, Data <%s> <%s>, Item %d, Del %c",
                    ilRC,*pppFieldData,pcpData,ilItemNo,clDel);

                if (ilRC > 0)
                {
                    ilRC = RC_SUCCESS ;
                } /* end of if */
            } /* else alloc ok */ 
        } /* end of if */
    } /* end of if */
    
 return (ilRC) ;

} /* end of GetFieldData */

static int IsFlightOutsideOpRange ( char *pcpFieldList )
{
    char clEndOfRange[16];
    int ilOutside = 0, ilRC;

    if ( igOpTimeFrame <= 0 )
        return 0;
    ilRC = GetCedaTimestamp( clEndOfRange, 16, igOpTimeFrame );
    if ( ilRC == RC_SUCCESS )
    {
        if ( (igEventType == EVT_TURNAROUND) || (igEventType == EVT_INBOUND) )
        {   /* check STOA for turnaround and inbound*/
            ilRC = FieldCompare("AFTTAB", "STOA", clEndOfRange, pcpFieldList,
                                 pcgInbound, CMP_SMALLER); 
            if (ilRC == CMP_SMALLER)    
            {   
                ilOutside = 1;
                dbg ( DEBUG, "IsFlightOutsideOpRange: STOA outside range <%s>", clEndOfRange );
            }
        }
        if ( !ilOutside && (igEventType == EVT_OUTBOUND) )
        {   /* check STOD for outbound */
            ilRC = FieldCompare("AFTTAB", "STOD", clEndOfRange, pcpFieldList,
                                 pcgOutbound, CMP_SMALLER); 
            if (ilRC == CMP_SMALLER)            
            {
                ilOutside = 1;
                dbg ( DEBUG, "IsFlightOutsideOpRange: STOD outside range <%s>", clEndOfRange );
            }
        }
    }
    return ilOutside;
}

/* *******************************************************/
static void CheckPerformance (int ipStart, char *pcpRtTime)
{
    time_t tlQueTime, tlStampDiff, tlNowTime;
    int ilOldDbgLevel = debug_level;

    if (tgMaxStampDiff > 0)
    {
        if (ipStart == TRUE)
        {
            tgBeginStamp = time (0L);
            tgQueDiff = 0;
            if (pcpRtTime && (pcpRtTime[0] >= '0') && (pcpRtTime[0] <= '9'))
            {
                tlNowTime = tgBeginStamp % 10000;
                tlQueTime = atol (pcpRtTime);
                tgQueDiff = tlNowTime - tlQueTime;
                if (tgQueDiff > tgMaxStampDiff)
                {
                    if ( debug_level < TRACE )
                    {
                        debug_level = TRACE;
                        bgShowEvent = TRUE;
                    }
                    dbg (TRACE, "===> PERF_CHECK: EVENT QUEUE_TIME (%d SEC) <===", tgQueDiff);
                }                       
            }                         
        }  
        else
        {
            tgEndStamp = time (0L);
            tlStampDiff = tgEndStamp - tgBeginStamp;
            if ( tlStampDiff > tgMaxStampDiff )
            {       
                if ( debug_level < TRACE )
                {
                    debug_level = TRACE;
                    bgShowEvent = TRUE;
                }
                dbg (TRACE, "===> PERF_CHECK: RULFND  EVENT PROCESSING (%d SEC) <===", tlStampDiff);
            }
            else
                dbg (TRACE, "----- RULFND  EVENT PROCESSING (%d SEC)", tlStampDiff);
            if ((tgQueDiff > 0) && (tlStampDiff > 0))
            {
                tlStampDiff += tgQueDiff;
                if (tlStampDiff > tgMaxStampDiff)
                {
                    if ( debug_level < TRACE )
                    {
                        debug_level = TRACE;
                        bgShowEvent = TRUE;
                    }
                    dbg (TRACE, "===> PERF_CHECK: TOTAL QUEUE_TIME (%d SEC) <===", tlStampDiff);
                }
                else
                    dbg (TRACE, "----- TOTAL QUEUE_TIME (%d SEC)", tlStampDiff);
            }    
        }      
        debug_level = ilOldDbgLevel;
    }        
    return;
}                               /* end CheckPerformance */


static int GetHopo ( char *pcpTwEnd, char *pcpHopo )
{
    char clDel = ',';
    char *pclTmp;
    int ilRc = RC_NOTFOUND;

    pclTmp = strchr (pcpTwEnd, clDel) ;
    if (pclTmp != NULL)
    {
        *pclTmp = 0x00 ;
        if (strlen(pcpTwEnd) == 3)
        {
            strcpy ( pcpHopo, pcpTwEnd) ;
            ilRc = RC_SUCCESS;
        }
        *pclTmp = clDel ;
    }
    if ( ilRc != RC_SUCCESS )
    {
        strcpy ( pcpHopo, cgHopo );
        dbg ( TRACE, "GetHopo: TW_END not filled correctly -> using default HOPO <%s>", pcpHopo );
    }
    return ilRc;
}

static int GetLastModid4Flight ()
{
    int           ilRet=-1, ilDummy=-1, ilRc;
    long          llRow = ARR_FIRST;
    time_t        tlNow, tlTime;
    char          *pclUrnoIn, *pclUrnoOut, *pclTime, *pclModid, *pclRow;
    BOOL          blEnd = FALSE;

    dbg ( DEBUG, "GetLastModid4Flight entered");
    bgOuriFound = FALSE;
    bgOuroFound = FALSE;

    if ( rgEvtHistArr.rrArrayHandle < 0 )
    {
        dbg ( DEBUG, "GetLastModid4Flight: rgEvtHistArr not initialised");
        return ilRet;
    }
    if ( (igEventType == EVT_TURNAROUND) || (igEventType == EVT_INBOUND) )
    {
        ilRc = GetFieldData2("AFTTAB", "URNO", pcgFieldList, pcgInbound,
                            &pcgOuri, &lgOuriLen);
        if (ilRc == RC_SUCCESS)
        {
            bgOuriFound = TRUE;     
            dbg ( DEBUG, "GetLastModid4Flight: Found OURI <%s>", pcgOuri );
        }
        else
            dbg ( TRACE, "GetLastModid4Flight: ERROR: URNO of Inbound not found" );
    }
    if ( (igEventType == EVT_TURNAROUND) || (igEventType == EVT_OUTBOUND) )
    {
        ilRc = GetFieldData2("AFTTAB", "URNO", pcgFieldList, pcgOutbound,
                            &pcgOuro, &lgOuroLen);
        if (ilRc == RC_SUCCESS)
        {
            bgOuroFound = TRUE;     
            dbg ( DEBUG, "GetLastModid4Flight: Found OURO <%s>", pcgOuro );
        }
        else
            dbg ( TRACE, "GetLastModid4Flight: ERROR: URNO of Outbound not found" );
    }
    
    if ( bgOuriFound || bgOuroFound )
    {
        tlNow = time (0L);
        while ( (ilRet<0) && !blEnd &&
                CEDAArrayFindRowPointer(&(rgEvtHistArr.rrArrayHandle),
                                        rgEvtHistArr.crArrayName,
                                        &(rgEvtHistArr.rrIdx01Handle),
                                        rgEvtHistArr.crIdx01Name,
                                        "",&llRow, (void *) &pclRow) == RC_SUCCESS )
        {   /* field list of Array: "TIME,OURI,OURO,TOID" */
            pclTime = pclRow + rgEvtHistArr.plrArrayFieldOfs[0];
            pclUrnoIn = pclRow + rgEvtHistArr.plrArrayFieldOfs[1];
            pclUrnoOut = pclRow + rgEvtHistArr.plrArrayFieldOfs[2];
            pclModid = pclRow + rgEvtHistArr.plrArrayFieldOfs[3];

            dbg (DEBUG, "GetLastModid4Flight: Found Row <%s,%s,%s,%s>", 
                 pclTime, pclUrnoIn, pclUrnoOut, pclModid );
    
            if ( ( sscanf ( pclTime, "%ld", &tlTime ) > 0 ) && 
                 ( tlNow > tlTime + igMaxEvtHistLenght ) )
            {
                dbg ( DEBUG, "GetLastModid4Flight: Ran out of time frame Now %ld > %ld + %d",
                      tlNow, tlTime, igMaxEvtHistLenght );
                blEnd = TRUE;
            }
            else
            {
                if ( bgOuriFound )
                {
                    /* dbg (DEBUG, "GetLastModid4Flight: Compare Inbounds <%s> <%s>", pclUrnoIn,pcgOuri ); */
                    if ( strstr(pclUrnoIn,pcgOuri) && 
                         ( sscanf(pclModid, "%d", &ilDummy)>0)  && (ilDummy>0) )
                    {   
                        ilRet = ilDummy;
                        dbg ( TRACE, "Found modid <%d> for URNOIN <%s> TIME <%s> Now <%ld>",
                              ilRet, pclUrnoIn, pclTime, tlNow );
                    }
                }
                if( ( ilRet < 0 ) && bgOuroFound )
                {
                    /* dbg (DEBUG, "GetLastModid4Flight: Compare Outbounds <%s> <%s>", pclUrnoOut,pcgOuro ); */
                    if ( strstr(pclUrnoOut,pcgOuro) && 
                         ( sscanf(pclModid, "%d", &ilDummy)>0)  && (ilDummy>0) )
                    {   
                        ilRet = ilDummy;
                        dbg ( TRACE, "Found modid <%d> for URNOOUT <%s> TIME <%s> Now <%ld>",
                              ilRet, pclUrnoOut, pclTime, tlNow );
                    }
                }
            }
            llRow = ARR_NEXT;
        }
    }
    return ilRet;
}   
    

static int AddToHistory ( int ipModid )
{
    long        llRowCnt;
    time_t      tlNow = time (0L);
    int         ilRc = RC_FAIL ;
    long        llRow=ARR_LAST;
    char        clBuf[81], clEmpty[2]="", *pclU1, *pclU2;
    static long llCurrNumb = 0L;
    char        *pclRow=0;
    

    if ( rgEvtHistArr.rrArrayHandle < 0 )
    {
        dbg ( DEBUG, "AddToHistory: rgEvtHistArr not initialised");
        return ilRc;
    }
    if ( bgOuriFound || bgOuroFound )
    {
        CEDAArrayGetRowCount( &rgEvtHistArr.rrArrayHandle,
                              rgEvtHistArr.crArrayName, &llRowCnt ) ;
        if ( llRowCnt >= igMaxEvtHistNo )
        {
            llRow = ARR_LAST;
            ilRc = CEDAArrayFindRowPointer ( &(rgEvtHistArr.rrArrayHandle), 
                                             rgEvtHistArr.crArrayName, 
                                             &(rgEvtHistArr.rrIdx01Handle),
                                             rgEvtHistArr.crIdx01Name,
                                             "", &llRow, (void *) &pclRow);
            if ( ilRc == RC_SUCCESS )
                ilRc = CEDAArrayDeleteRow( &(rgEvtHistArr.rrArrayHandle),
                                          rgEvtHistArr.crArrayName, llRow );
            if ( ilRc != RC_SUCCESS )
                dbg ( TRACE, "AddToHistory: CEDAArrayDeleteRow failed RC <%d>", ilRc );
        }
        /* field list of Array: "TIME,OURI,OURO,TOID" */
        pclU1 = bgOuriFound ? pcgOuri : clEmpty;
        pclU2 = bgOuroFound ? pcgOuro : clEmpty;
        llCurrNumb++;

        sprintf ( clBuf, "%ld,%s,%s,%d,%010ld", tlNow, pclU1, pclU2, ipModid, llCurrNumb );
        dbg ( TRACE, "AddToHistory: Row to Add <%s>", clBuf );
        delton ( clBuf );

        llRow = ARR_FIRST;
        ilRc = AATArrayAddRow( &(rgEvtHistArr.rrArrayHandle), 
                               rgEvtHistArr.crArrayName, &llRow, (void*)clBuf );
        if ( ilRc != RC_SUCCESS )
            dbg ( TRACE, "AddToHistory: CEDAArrayDeleteRow failed RC <%d>", ilRc );
        else
        {
            CEDAArrayGetRowCount( &rgEvtHistArr.rrArrayHandle,
                                  rgEvtHistArr.crArrayName, &llRow ) ;
            dbg ( DEBUG, "AddToHistory: Row added Rowcount <%ld> -> <%ld> Max <%d>", llRowCnt, llRow, igMaxEvtHistNo );
        }
    }
    else
        dbg ( TRACE, "AddToHistory: Nothing to Add no URNO set" );
    return ilRc;
}

static int check_ret1(int ipRc,int ipLine)
{
    int ilOldDebugLevel;

    ilOldDebugLevel = debug_level;

    debug_level = TRACE;

    dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
    debug_level  = ilOldDebugLevel;
    return check_ret(ipRc);
}

