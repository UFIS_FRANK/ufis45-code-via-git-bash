#ifndef _DEF_mks_version_dhsif_h
  #define _DEF_mks_version_dhsif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dhsif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dysif.h 1.3 2004/07/27 17:25:30SGT jim Exp  $";
#endif /* _DEF_mks_version */
/**************************************************************/
/* DYNAMIC-SIGNAGE  Interface (DYSIF) header file        */
/* for ATHENS International Airport                      */
/**************************************************************/
#ifndef BOOL
#define BOOL int
#endif

/****************/
/* ALL INCLUDES */
/****************/
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <dirent.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "db_if.h"
#include "sthdef.h"
#include "helpful.h"
#include "hsbsub.h"
#include "debugrec.h"
#include "netin.h"
#include "tcputil.h"
#include "list.h"
#include "ftptools.h"
#include "ftphdl.h"

/*********************/
/* common structures */
/*********************/
/* telegram header */
typedef struct
{
  char start[1];				/* start of message = '/' hex(0x02) */
  char cycle_no[1];			/* cycle nr. !NOT USED! JWE 18.11.1998 will be hex(0x30) */ 
  char t_type[2];				/* message identifier */
  char sender[2];				/* sending system ID. We're always 'FO' */
  char receiver[2];			/* receiving system ID */
  char order_no[2];			
}HEAD;

/* telegram end */
typedef struct
{
	char	time[9];				/* time of telegram creation and send */
	char	tail[5];				/* end of message sign='  (CR)(ETX)*/  
							/* CR=hex(0xD) */
							/* ETX=hex(0x03) */  
}TAIL;
/**********************/
/* different messages */
/**********************/
/* flight information's */
typedef struct
{
	char	tifd[15];
	char	adid[2];
	char	ftyp[2];
	char	flno[10];
	char	stod[15];
}FI_INT;

/* struct for AFI (Arrival-Flight-Information) - messages */
typedef struct
{
	char	flno[8];
	char	belt[2];
	char	org3[3];
	char	psta[3];
	char	show[2];
}AFI_OUT;

/* struct for CFI (chute+flight-Information) - messages */
typedef struct
{
	char	indi[1];
	char	lnam[3];
	char	fcla[1];
	char	flno[8];
	char	pstd[4];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char 	o_tim[12];
	char	c_tim[13];
}CFI_OUT;

/* struct for SACI actual chute info - messages */
/* insert,update of records in chatab */
typedef struct
{
	char	indi[1];
	char	lnam[3];
	char	fcla[1];
	char	flno[8];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char 	o_tim[12];
	char	c_tim[13];
}CSI_OUT;

/* flight cancelled message */
typedef struct
{
	char	day[1];
	char	flno[8];
	char	stod[13];
}CXX_EXT;

/* telegram acknowledge */
typedef struct
{
	HEAD	head;						/* acknowledge header */  
	TAIL	tail;						/* acknowledge end */  						
}ACK_EXT;

typedef struct
{
	HEAD	head;						/* acknowledge header */  
	TAIL	tail;						/* acknowledge end */  						
}ALIVE;
/***************/
/***************/
/* ALL DEFINES */
/***************/
#define XXS_BUFF  32
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define DATABLK_SIZE  524288 /* Maximum space for sql_if data_area&sql-buffer*/
#define SOM  (char)0x02 /* start of message */
#define EOT  (char)0x03 /* end of message */
#define MAX_TELEGRAM_LEN 400
#define CONNECT_TIMEOUT 5
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define RIGHT 1100
#define LEFT 1101

#define SYS_ID_LEN 2 /* means 2 bytes for the system-ID */

#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204
#define CMD_LEN 2 			/* means 2 bytes for the length of a command */
#define ALIVE_LEN 4 		/* means 4 bytes for the length of alive-cmd */
#define ALIVE_ID "Uxxx"	/* string for alive telegram */

#define MSG_CXX 51 		/* ID for cancelled flight info message */
#define MSG_FTF 53 		/* ID for flight table file information message */
#define MSG_SACI 55 		/* ID for Chute Status (open/close) message */
#define MSG_SSCI 60 	/* ID for scheduled chute information message*/
#define MSG_SAI 61 		/* ID for arrival information message*/
#define MSG_ALV 98 		/* ID for Alive Msg.*/
#define MSG_ACK 99 		/* ID for Acknowledge message */

#define SAI	101		/* ID for sending the arrival flight information to the DYS system */
#define SSCI	102		/* ID for sending the chute information to the DYS system */
#define SACI	103		/* ID for sending a flight update to the BHS system */
#define CXX	104		/* ID for sending a flight cancel to the BHS system */
#define BLT	105		/* ID for sending info for belt (re)allocation */
#define FTU	106		/* ID for sending info about flight table updates related fields to dynamic signage */

/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *debug_level;		/* startup debug-level */ 
	char *mode;						/* type of running mode (REAL or TEST)*/
	char *my_sys_id;			/* my own system ID 2 chars */
	char *dys_host1;			/* IP-address or hostname of the 1. DYS-server */ 
	char *dys_host2;			/* IP-address or hostname of the 2. DYS-server */ 
	char *service_port; 	/* service name for receiving data */
	char *wait_for_ack; 	/* wait-time in sec. for acknowledge */
	char *recv_timeout; 	/* wait-time in sec. for receiving a valid telegram */
	char *max_sends;			/* max. number of sends(resends) for a telegram */
	char *recv_log; 			/* log-file for receiving telegrams */
	char *send_log; 			/* log-file for sended telegrams */
	char *try_reconnect; 	/* try to connect every xy sec. */
	char *remote_sys_id; 	/* ID of the remote system */
	char *db_arr_filter; 	/* filter condition for SQL*/
	char *db_dep_filter; 	/* filter condition for SQL*/
	char *fs_offset; 			/* number of days the flight-schedule is send in advance*/
	char *fs_local_path; 	/* local path for flight-schedule files */
	char *fs_remote_path; /* remote path for flight-schedule files */
	char *ftp_client_os; 	/* operating system of the client */
	char *ftp_user; 			/* username for FTP-connection */
	char *ftp_pass; 			/* password for FTP-connection */
	char *after_ftp; 			/* what to do after FTP-transmission */
	char *arr_from; 			/* timeframe from to show arrival data*/
	char *arr_to; 				/* timeframe to for arrival data */
	char *check_interval; /* interval for checking if a Nr. 61 telegram has to be sent */
	char *time_beltopen; 	/* time after which a Nr. 61 telegram must be sent according to beltopen-time */
	char *time_beltclose; /* time after which a Nr. 61 telegram must be sent according to beltclose-time */
	char *auto_delete; 		/* time after which a Nr. 61 is triggered automatically */
}CFG;

typedef struct
{
	int ilMsgNr;
	char *msg;
}MSG_RECV;
