#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/bchdl.c 1.18 2008/09/19 15:20:43SGT bst Exp  $";
#endif /* _DEF_mks_version */

/* ******************************************************************** */
/*									*/
/* CEDA Program Skeleton						*/
/*									*/
/* Author		: Jochen Beese					*/
/* Date			: 26/04/94					*/
/* Description		: Network Broadcast Interface: 	       		*/
/*			  Receives packets from the qcp and broadcasts	*/
/*			  them on the network. If the 			*/
/*			  cmdblk contains an RBC command, the requested	*/
/*			  "old" broadcast packet is sent to the 	*/
/*			  originator.					*/
/*									*/
/* Update history	:						*/
/*			 15.8.94  JB  Shared memory is now used for	*/
/*				      the bc buffer			*/
/*			Jul99 Redesigned by BST				*/
/*  MCU 2002/08/07      : Now send BC to all clients in other nets      */
/*                         don't check config file anymore              */
/* 20041208 JIM: modifying PRF 6604: check for configuration also       */
/* 20041208 JIM: avoid 6 warnings by replacing 'static fct' by 'static int fct' */
/* 20050405 BST: new BC filter IgnoreTableTws                           */
/* 20080812 BST: new BC filter IgnoreTableUdp                           */
/*									*/
/* ******************************************************************** */

/* This program is a CEDA main program (ach was) */

static char sccs_version[]="@(#)UFIS 4.3 (c) ABB ACE/FC bchdl.c 2.3 " __DATE__ "  " __TIME__ " / BST/MCU"; 

#define U_MAIN
#define CEDA_PRG
#define STH_USE

/* Values: for MAXINT */
#include <values.h> 
#include <stdio.h>
#include <unistd.h>
/* For StackFile */
#include <sys/types.h>
#include <sys/stat.h>

/* For MAXHOSTNAMELEN on HP */
#include <sys/param.h> 

#include  "netin.h"
#include  "bchdl.h"
#include  "tcputil.h"
#include  "cli.h"
#include  "send_tools.h"
#include  "libccstr.h"
#include  "send.h"
#include  "cedatime.h"
#include  "ct.h"

/* New netin with sgs.tab-EXTAB entry*/
#define USE_EXTAB_HOSTNAME


#ifdef EOS
#undef EOS
#define EOS '\0'
#endif

/* Command for packet requests */
#define BROADCAST_REQUEST	"RBC"
#define BROADCAST_TO_SEND	"SBC"
#define BROADCAST_TO_RESEND	"RBS"

#define BC_NOT_FOUND_MSG	"Broadcast packet not found"

/* Remote hosts array size */
#define NO_OF_REMOTE_HOSTS	4096
#define NO_OF_REMOTE_NETWORKS	1024

/* Code for empty buffer in sgs.tab */
#define EMPTY_BUFFER_CODE	"EmPtY BuFfEr"

#ifdef MAX_EVENT_SIZE
#undef MAX_EVENT_SIZE
#endif
#define MAX_EVENT_SIZE (sizeof(EVENT) + PACKET_LEN * 32)

/* Length for event struct */
#define	PACKETDATALEN  (PACKET_LEN-sizeof(EVENT))

#define MAX_BC_NUM 30000

/* Table BcNum array size */
#define MAX_NO_OF_TABLES 600
#define MAX_NO_OF_ORIGIN 200

/* For BCQ File Mode */
#define GRP_REC_SIZE 1024


/* ******************************************************************** */
/* Internal used structures                                             */
/* ******************************************************************** */
typedef struct
{
#if defined(_LINUX)
  unsigned short ActBcNum;
  unsigned short MaxBcNum;
#else
  short ActBcNum;
  short MaxBcNum;
#endif
} STACK_HEAD;

typedef struct
{
  long Min[61];
  long Hour[25];
  long Day[32];
  long Month[13];
} BC_REPORT;

typedef struct
{
	char  TableName[64];
	char  FieldName1[64];
	char  FieldValu1[64];
	char  FieldName2[64];
	char  FieldValu2[64];
	char  FieldName3[64];
	char  FieldValu3[64];
  short CurBcNum1;
  short CurBcNum2;
  short CurBcNum3;
  long TotalSent;
  long PeakSent;
  BC_REPORT BcSent;
  BC_REPORT BcIgno;
} TABLE_NUM;

typedef struct
{
  char TimeStamp[16];
  int InternalClient;
  int PendingRbs;
  long SpoolCount;
  long SpoolIdNbr;
  long TotalRbsCmds;
  long PeakRbsCmds;
  long TotalResent;
  long PeakResent;
  long TotalSpool;
  long PeakSpool;
  BC_REPORT BcSent;
} BC_SPOOL;

typedef struct
{
  int Orig;
  BC_REPORT EvtRcv;
} BC_ORIGIN;

typedef struct
{
  BC_REPORT WksCount;
  BC_REPORT DataGram;
} BC_CLIENT;

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE	*outp;*/
extern int  errno;
int	debug_level=TRACE;
extern long nap(long);


/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */

static ITEM  	*item = NULL;			/* The queue item pointer	*/
static EVENT	*event;			/* The event pointer		*/
static EVENT	*MyEvent = NULL;
static int	igMyEventLen = 0;
static int	igItemLen = 0;		/* length of incoming item 	*/
static int	parent_mod_id;
static int 	sock=0;		/* bc socket */
static int	que_out;	/* requesters modul id */
static char 	req_buf[MAX_EVENT_SIZE]; /* buffer for outgoing packets */
static char	cfgfile[128];	/* Full path name of config file */
static char	cfgRptPath[128];	/* Full path name of report files */
static char	pcgUdpAdrFile[128];	/* Full path name of DataGram Addresses file */
static char	pcgBcqPath[128];	/* Path of BCQ files */
static char	pcgBcqFile[128];	/* Name of BCQ files */
static char pcgBcqFullPath[128];
static FILE *pfgBcqPtr = NULL;
static char pcgGrpCtl[GRP_REC_SIZE+1];
static int	igMaxBcqSize = (1024 * 8);
static int	igBcqFileNo = 0;
static int      igCeiOutQue = 0;
static int      igTcpOutQue = 0;
static int      igTcpOutPri = 0;
static char	pcgUdpCutOff[32];
static char	pcgUdpNotOut[32];
static char	pcgTcpNotOut[32];

static STR_DESC rgBcqFileData;

static char	remote_hosts[NO_OF_REMOTE_HOSTS+1][32];
static char	rhosts_cpid[NO_OF_REMOTE_HOSTS+1][32];
static int	rhosts_cnt[NO_OF_REMOTE_HOSTS+1];
static int	rhosts_keep[NO_OF_REMOTE_HOSTS+1];
static int	rhosts_que[NO_OF_REMOTE_HOSTS+1];
static STR_DESC rhosts_key[NO_OF_REMOTE_HOSTS+1];
static BC_SPOOL rhosts_spool[NO_OF_REMOTE_HOSTS+1];

static char	remote_network[NO_OF_REMOTE_NETWORKS+1][32];
static int	remote_net_cnt[NO_OF_REMOTE_NETWORKS+1];

static char 	pcgPort2[64]=""; /* Second BC-Port for 32-Bit DLL */
static unsigned long ulgBroadcastAddr = 0xffffffff;

static char pcgHostName[16];
static unsigned long		ulgHostIp=0;	/* my IP addr  */
static char	pcgNetmask[10];
static  unsigned int igNetmask=0xffffff00;
static char pcgSgsPrj[16];
static char pcgIgnoreTables[1024];
static char pcgIgnoreCeiTables[1024];
static char pcgIgnoreTableTws[1024];
static char pcgIgnoreTableUdp[1024];
static char pcgIgnoreTableTcp[1024];
static STACK_HEAD  *prgStHd = NULL;
static COMMIF  *prgOutPacket = NULL;
static COMMIF  *prgCloPacket = NULL;
static COMMIF  *prgReadBcPkt = NULL;
static FILE	*pfgBcStack = NULL;
static int igMyPid = 0;
static int igSwitchLog = FALSE;
static int igTestLoose = FALSE;
static int igStartupMode = TRACE;
static int igRuntimeMode = 0;    
static int igSupport16Bit = TRUE;
static int igSendModuleName = FALSE;
static int igSpoolRbsRequest = -1;
static int igSpoolRbsGroup = 10;
static int igSend32BitPort = TRUE;
static int igSend32BitWksDgr = TRUE;
static int igSend32BitNetDgr = FALSE;


static int igMoveRbsToP4 = FALSE;
static int igTestP4TimeSpan = FALSE;
static int igTestP4TimeSet = FALSE;
static int lgTestP4TimeMax = 0;
static int lgTestP4TimeSum = 0;
static int lgTestP4TimePeak = 0;
static int lgTestP3EvtCount = 0;
static int lgTestP4EvtCount = 0;
static int igSetStartEnd = TRUE;
static long lgResentCount = 0;
static long lgSpooledCount = 0;
static char pcgTimeStamp[16];
static char pcgDailyStamp[16];
static int  igRptMin = -1;
static int  igRptHour = -1;
static int  igRptDay = -1;
static int  igRptMonth = -1;
static int  igRptLastMin = -1;
static int  igRptLastHour = -1;
static int  igRptLastDay = -1;
static int  igRptLastMonth = -1;
static long lgLastTimeStamp = 0;
static long lgCurrTimeStamp = 0;
static long lgRefreshStatistic = 0;
static long igTotalCliCnt = 0;
static long lgTotalWksCon = 0;
static long lgTotalHrgCnt = 0;
static long lgTotalHurCnt = 0;
static long lgTotalP2Sent = 0;
static long lgTotalD2Sent = 0;
static long lgTotalResent = 0;
static long lgTotalRbsCmd = 0;
static long lgTotalSpooled = 0;
static long igPeakCliCnt = 0;
static long lgPeakHrgCnt = 0;
static long lgPeakHurCnt = 0;
static long lgPeakP2Sent = 0;
static long lgPeakD2Sent = 0;
static long lgPeakResent = 0;
static long lgPeakRbsCmd = 0;
static long lgPeakSpooled = 0;
static TABLE_NUM rgTblBcNum[MAX_NO_OF_TABLES+1];
static BC_ORIGIN rgBcOrig[MAX_NO_OF_ORIGIN+1];
static BC_CLIENT rgClient;
static int igTblNumIdx = 0;
static int igTblNumCnt = 0;
static long lgNapTime = 0L;
static long lgNapTime2 = 0L;
static int igBcSent = 0;

/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */

extern	int	init_que();		/* Attach to CEDA queues	*/
extern	int	que(int, int, int, int, int, char*);
					/* CEDA queuing routine	*/
extern	void	catch_all();		/* Catches all signals		*/
extern  int	send_message(int,int,int,int,char*);
					/* CEDA message handler i/f    */
extern int StrgPutStrg (char *, int *, char *, int, int, char *);
extern int get_item_no(char *, char *, int);
    
/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */
static int init_bchdl();
static int ResetBcHdl();

static void  terminate(int errcode);	/* Terminate program		*/
static void  handle_sig(int sig);	/* Handles signals		*/
static void HandleQueErr(int err);
int create_bc_socket (void);
int SendBroadCast (int, char *, int, int);
int SendTo16BitPort(int, char *);
int SendTo32BitPort(int, char *);
static int handle_data();
static int BcBufAdd (short, char *);
static int handle_bc_req (CMDBLK *cmdblk);
static int HandleRbsSpoolOrSend (int, char *, char *, char *);
static int HandleSpooledRbs (char *, char *, char *);
static int HandleShiftRbsToP4(char *pcpSelKey);
static int bc_hsb_logoff(void);
/*static handle_reg_cmd (char *pcpHexAddr, char *pcpQueId, char *pcpData, int ipCountIt);*/
/*static handle_unreg_cmd (char *pcpHexAddr, char *pcpQueId);*/
static int handle_reg_cmd (char *pcpHexAddr, char *pcpQueId, char *pcpData, int ipCountIt);
static int handle_unreg_cmd (char *pcpHexAddr, char *pcpQueId);

static short GetNextBcNumber(void);
static void SetShortValue(short *pspDest, short spValue);
static void SetLongValue(long *, long);

static int TestBcForLoose(short spBcNum, short spPart);

static void TestByteOrder(void);
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault);
static int  ReadCfg(char *, char *, char *, char *, char *);
static void SaveUdpAdrFile(int);
static void ReadUdpAdrFile(void);
static void ReadSendAlways(void);
/* static HandleKeepAlive (char *pcpHexAddr, int); */
static int HandleKeepAlive (char *pcpHexAddr, int);
static void CreateTblBcNum(char *pcpTwEnd, char *pcpTbl, char *pcpCmd,
	 char *pcpFields, char *pcpData, int ipValid);
static int SendBcOutEvents(char *pcpTbl, char *pcpCmd, int ipPrio);
static int StoreBcOutFilter (char *pcpHexAddr, char *pcpQueId, char *pcpFilter);
static int ResendBcEvents (char *pcpHexAddr, char *pcpQueId, char *pcpFromBcNum);
static int SendInternalCommand(int ipHostIdx, char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat);
static void PrintStatisticToFile(long lpTimeDiff,int ipPrintAll);
static void ResetStatistic(void);
static int GetClientIdx(char *pcpHexAddr);
static void InitDailyReport(int ipCheckPrint);
static void PrintHourStatistic(int ipReset);
static void PrintDailyStatistic(int ipReset);
static void PrintMonthStatistic(int ipReset);
static void PrintYearStatistic(int ipReset);
static void SetMaxWksCount(void);
static void CountBcEvent(int ipOrigin);
static int RebuildBcOutQueues(void);

static int CreateBcqFileData(char *pcpFileName, STR_DESC* prpString, int ipTrimData, char *pcpSize);
static void TrimAndFilterCr(char *pclTextBuff,char *pcpFldSep, char *pcpRecSep);
static void CheckBcqFile(int ipBcqDataSize, int ipSentCnt);
static void SendCeiOutEvents(void);
static int ExtendEvent(char *pcpSel, char *pcpFld, char *pcpDat);
static int InitNetworkRegister(void);
static int RegisterNetwork(int ipRegFlag, char *pcpHexTcpIp);
static int RuntimeConfig(void);

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */

MAIN
{
  int rc=RC_SUCCESS;
  int ilDbgFileCount = 0;
  int ili = 0;
  char pclTestBuff[64];
  char *pclTestData = NULL;
  debug_level = igStartupMode;

  INITIALIZE;			/* General initialization	*/
  STH_INIT();			/* system table handler init */

  dbg(TRACE,"%s",sccs_version);

  rc = init_bchdl();
  if (rc != RC_SUCCESS)
    terminate (rc);

  catch_all(handle_sig);       	/* handles signal		*/
  sock = create_bc_socket();	/* create the broadcast socket	*/
  if (sock < 0)
  {
    dbg(TRACE,"BCHDL: Create socket failed: %s ", strerror( errno)); 
    
    terminate (RC_FAIL);
  }

  debug_level = igRuntimeMode;
  lgLastTimeStamp = time(0L);

  while(TRUE) 
  {
    if (igSetStartEnd == TRUE)
    {
      dbg(TRACE,"============== START/END ================");
    }
    rc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&item);
    event = (EVENT *) item->text;
		
    if (rc == RC_SUCCESS) 
    {
      rc = que(QUE_ACK,0,mod_id,0,0,NULL);
      if ( rc != RC_SUCCESS ) 
      {
	/* handle que_ack error */
	HandleQueErr(rc);
      } /* fi */
			
	switch (event->command) 
	{
	case	RESET		:
		rc = ResetBcHdl();
		break;
	case	EVENT_DATA	:
		if(ctrl_sta==HSB_ACTIVE || ctrl_sta==HSB_STANDALONE ){
			rc = handle_data();
		} /* end if */
		break;

	case TRACE_ON:  /* 7 */
	case TRACE_OFF: /* 8 */
	        dbg_handle_debug(event->command);
		rc = RC_SUCCESS;
		break;
	case LOG_SWITCH: /* 2 */
		SwitchDebugFile(debug_level,&ilDbgFileCount);
		rc = RC_SUCCESS;
		break;
		
	case	SHUTDOWN	:
	case	HSB_DOWN	:
		rc=bc_hsb_logoff();
		terminate(RC_SHUTDOWN);
		break;

	case	HSB_ACT_TO_SBY	:
		ctrl_sta = event->command;
		send_message(0,HSB_REQUEST,0,0,NULL);
		bc_hsb_logoff();
		break;
	case	HSB_STANDBY	:
	case	HSB_ACTIVE	:
	case	HSB_COMMING_UP	:
	case	HSB_STANDALONE	:
		ctrl_sta = event->command;
		send_message(0,HSB_REQUEST,0,0,NULL);
		break;

case 100:
  rc = RebuildBcOutQueues();
  rc = RC_SUCCESS;                                                
  break;
case 200:
  rc = RuntimeConfig();
  rc = RC_SUCCESS;                                                
  break;
case 300: /* TEST FOR BROADCASTS */
  dbg(TRACE,"---- START SEND TO BCHDL QUE_ID 1900 ----");

  pclTestData = "'O','S','X','N','T','G','D','R'" ;


  for (ili=1;ili<=10000;ili++)
  {
    sprintf(pclTestBuff,"%d",ili);
    (void) tools_send_info_flag(1900,0, "BERNI", "", "WKS157", "", "",
           "0",
           "SIN,TAB,TEST",              /* Specials  */            
           pclTestBuff,			/* Command   */
           "BIGBRC",			/* Table     */
           "BIG SELECTION",				/* Selection */
           "BIG FIELDS",	/* Fields    */
           pclTestData,
           4);             
  } /* end for */

  dbg(TRACE,"---- END ----");                                
  rc = RC_SUCCESS;                                                
  break;

case 310:
  prgStHd->ActBcNum = 29973;
  (void)GetNextBcNumber();
	dbg(TRACE,"LAST BC NUM SET TO %d",prgStHd->ActBcNum);
  rc = RC_SUCCESS;                                                
  break;

case 320:
   dbg(TRACE,"Function TestBcForLoose() activated !!");
   igTestLoose = TRUE;
  break;

case 321:
   dbg(TRACE,"Function TestBcForLoose() disabled !!");
   igTestLoose = FALSE;
  break;

case 500:
  SaveUdpAdrFile(500);
  break;

case 777:
  TestByteOrder();
  break;

case 999:
  /* PrintStatisticToFile(-99,FALSE); */
  PrintHourStatistic(FALSE);
  PrintDailyStatistic(FALSE);
  PrintMonthStatistic(FALSE);
  PrintYearStatistic(FALSE);
  break;

	default			:
		break;
	} /* end switch */
      
      /* Handle error conditions */
      
      if (rc != RC_SUCCESS) {
	dbg(TRACE,"BCHDL: Handle_data returns %d ", rc);
	terminate(rc);
      } /* end if */
      
    } /* end if */
    else 
    {
      /* Handle queuing errors */
      HandleQueErr(rc);
      sleep (5);
    } /* end else */

    if (igSwitchLog == TRUE)
    {
			/* Done by dbg() function automatically */
      /* SwitchDebugFile(debug_level,&ilDbgFileCount); */
      igSwitchLog = FALSE;
    } /* end if */
    
  } /* end while */

  return rc;
} /* end main */
  
/* ******************************************************************** */
/*									*/
/* The Init_bchdl function						*/
/*									*/
/* ******************************************************************** */
static int init_bchdl()
{
  int	rc = RC_SUCCESS;	/* Return code */
  int ilGetRc = RC_SUCCESS;
  int i = 0 ;
  int n = 0;
  long llRecPos;
  char *Ptmp;
  char buf[64];
  char buf2[MAXHOSTNAMELEN];
  char pclStackFile[128];
  char pclTmp[1024];
  struct stat rlStat;
  unsigned int ilTmp=0;
  struct hostent *hp;

  igMyPid = getpid();

  if (rc == RC_SUCCESS)
  {
    if ( (Ptmp = getenv("CFG_PATH")) == NULL) 
    {
      dbg (TRACE,"init_bchdl: Error Reading CFG_PATH");
      rc = RC_FAIL;
    }
    else
    {
      sprintf (cfgfile, "%s/bchdl.cfg", Ptmp);
      sprintf(pclStackFile,"%s/BcHdlStack.Dat",Ptmp);
      sprintf(pcgUdpAdrFile,"%s/BcHdlUdpAdr.Dat",Ptmp);
    } /* fi */
  } /* fi */
  ReadCfg(cfgfile,pclTmp,"SYSTEM","REPORT_DIRECTORY","-");  
	if (pclTmp[0] == '-')
	{
    		if ( (Ptmp = getenv("DBG_PATH")) != NULL) 
    		{
			strcpy(pclTmp,Ptmp);
    		}
		else
		{
      			dbg (TRACE,"init_bchdl: Error Reading DBG_PATH");
			strcpy(pclTmp,"/ceda/debug");
		}
	} /* end if */
	strcpy(cfgRptPath,pclTmp);

  ReadCfg(cfgfile,pclTmp,"SYSTEM","BCQ_FILE_PATH","-");  
	if (pclTmp[0] == '-')
	{
    		if ( (Ptmp = getenv("DBG_PATH")) != NULL) 
    		{
			strcpy(pclTmp,Ptmp);
    		}
		else
		{
      			dbg (TRACE,"init_bchdl: Error Reading DBG_PATH");
			strcpy(pclTmp,"/ceda/debug");
		}
	} /* end if */
	strcpy(pcgBcqPath,pclTmp);

  GetLogFileMode(&igStartupMode,"STARTUP_MODE","OFF");
  GetLogFileMode(&igRuntimeMode,"RUNTIME_MODE","OFF");
  debug_level = igStartupMode;

rc=RC_SUCCESS;

    if (rc == RC_SUCCESS)
    {
	
      /* Attach to the CEDA queues */
      while (init_que()) {
	sleep(5);		/* Wait for QCP to create queues */
      } /* end while */

    } /* fi */
  for (i=0; i<NO_OF_REMOTE_HOSTS ; i++)
  {
    remote_hosts[i][0] = EOS;
    rhosts_cpid[i][0] = EOS;
    rhosts_cnt[i] = 0;
    rhosts_keep[i] = 0;
    rhosts_que[i] = 0;
    rhosts_key[i].Value = (char *)malloc(64);
    rhosts_key[i].Value[0] = 0x00;
    rhosts_key[i].AllocatedSize = 64;
    rhosts_key[i].UsedLen = 0;
    rhosts_spool[i].TimeStamp[0] = 0x00;
    rhosts_spool[i].InternalClient = 0;
    rhosts_spool[i].PendingRbs = 0;
    rhosts_spool[i].SpoolIdNbr = 0;
    rhosts_spool[i].SpoolCount = 0;
    rhosts_spool[i].TotalRbsCmds = 0;
    rhosts_spool[i].PeakRbsCmds = 0;
    rhosts_spool[i].TotalResent = 0;
    rhosts_spool[i].PeakResent = 0;
    rhosts_spool[i].TotalSpool = 0;
    rhosts_spool[i].PeakSpool = 0;
		for (n=0;n<60;n++)
		{
		  rhosts_spool[i].BcSent.Min[n] = 0;
		}
		for (n=0;n<25;n++)
		{
		  rhosts_spool[i].BcSent.Hour[n] = 0;
		}
		for (n=0;n<32;n++)
		{
		  rhosts_spool[i].BcSent.Day[n] = 0;
		}
		for (n=0;n<13;n++)
		{
		  rhosts_spool[i].BcSent.Month[n] = 0;
		}
  } /* fi */

		for (n=0;n<60;n++)
		{
		  rgClient.WksCount.Min[n] = 0;
		  rgClient.DataGram.Min[n] = 0;
		}
		for (n=0;n<25;n++)
		{
		  rgClient.WksCount.Hour[n] = 0;
		  rgClient.DataGram.Hour[n] = 0;
		}
		for (n=0;n<32;n++)
		{
		  rgClient.WksCount.Day[n] = 0;
		  rgClient.DataGram.Day[n] = 0;
		}
		for (n=0;n<13;n++)
		{
		  rgClient.WksCount.Month[n] = 0;
		  rgClient.DataGram.Month[n] = 0;
		}

  for (i=0; i<=MAX_NO_OF_ORIGIN ; i++)
  {
    rgBcOrig[i].Orig = -1;
		for (n=0;n<60;n++)
		{
		  rgBcOrig[i].EvtRcv.Min[n] = 0;
		}
		for (n=0;n<25;n++)
		{
		  rgBcOrig[i].EvtRcv.Hour[n] = 0;
		}
		for (n=0;n<32;n++)
		{
		  rgBcOrig[i].EvtRcv.Day[n] = 0;
		}
		for (n=0;n<13;n++)
		{
		  rgBcOrig[i].EvtRcv.Month[n] = 0;
		}
  }
  rgBcOrig[0].Orig = 0;

  for (i=0; i<NO_OF_REMOTE_NETWORKS; i++)
  {
    remote_network[i][0] = EOS;
    remote_net_cnt[i] = 0;
  }

  if (rc == RC_SUCCESS)
  {
    rc = tool_search_exco_data ("BC","PORT2",pcgPort2);
    /* Fallback */
    if (rc == RC_SUCCESS)
    {
      dbg(TRACE,"init_bchdl: Got PORT2 entry from sgs.tab: %s", pcgPort2);
    }
    else
    {
      pcgPort2[0] = EOS;
      rc = RC_SUCCESS;
    } /* fi */
  } /* fi */

  rc = tool_search_exco_data ("NET","BCADDR",buf);
  if (rc != RC_SUCCESS)
  {
    ulgBroadcastAddr = 0xffffffff;
    dbg(TRACE,"init_bchdl: No entry in sgs.tab: EXTAB,BCADDR : using default 0xffffffff");
    rc = RC_SUCCESS;
  }else{
    dbg(TRACE,"init_bchdl: Got BCADDR <%s>",buf);
    sscanf(buf,"%lx",&ulgBroadcastAddr);
    rc = RC_SUCCESS;
  } /* fi */
#ifndef _SOLARIS
  dbg(TRACE,"init_bchdl: Got ulgBROADCASTADDR <%s> normal <%x> ntohl <%x> htonl <%x>",inet_ntoa(htonl(ulgBroadcastAddr)),ulgBroadcastAddr,ntohl(ulgBroadcastAddr),htonl(ulgBroadcastAddr));
#endif

  /********************** Get hostname ***********/
  memset(pcgHostName, 0x00, 10);
  if (rc == RC_SUCCESS)
  {
		#ifdef USE_EXTAB_HOSTNAME
			rc = tool_search_exco_data ("NET","HOST",buf2);
			/* Fallback */
			if (rc != RC_SUCCESS)
			{
				dbg(TRACE,"init_bchdl: No HOST entry in sgs.tab: EXTAB! Please add");
				rc = gethostname (buf2, (size_t) MAXHOSTNAMELEN);
			} /* fi */
		#else
			#if defined(_HPUX_SOURCE) || defined(_SNI)
				rc = gethostname (buf2, (size_t) MAXHOSTNAMELEN);
			#else
				rc = gethostname (buf2, MAXHOSTNAMELEN);
			#endif
				dbg(DEBUG,"init_bchdl: Gethostname returned %d errno=%d",rc, errno);
		#endif
    if (rc == 0)
    {	/* Only the pure host name */
      for (i=0;buf2[i] != EOS &&  buf2[i] != '.' && i < MAXHOSTNAMELEN; i++)
	;
      buf2[i] = EOS;
      strncpy (pcgHostName, buf2, 10);
    } /* fi */

		/********************** JWE: 20041022 - START PRF 6604 ** Get host IP ***********/
    hp = gethostbyname(pcgHostName);
		if (hp == NULL)
		{
			dbg(TRACE,"init_bchdl: Gethostbyname(%s): ERROR=<%s>",pcgHostName,strerror(errno));
			rc = RC_FAIL;
		}
		else
		{
			dbg (TRACE,"init_bchdl: Got HOSTINFO: name=%s len=%d adr1=%x" ,hp->h_name, hp->h_length, ntohl (*((int *) (hp->h_addr))) );
			ulgHostIp = ntohl (*((int *) (hp->h_addr)));
			rc = RC_SUCCESS;
		} /* end if */
	} /* fi */

  /********************** JWE: 20041022 - Get netmask *********/
  memset(pcgNetmask, 0x00, 10);
  if ((rc = tool_search_exco_data("NET","MASK",
  				pcgNetmask)) == RC_SUCCESS)
  {
    sscanf (pcgNetmask, "%x",&ilTmp);
    /* Simple plausi */
    if (ilTmp > 0xff000000)
    {
      igNetmask = ilTmp;
    } /* fi */
  	dbg(TRACE,"init_bchdl: Got NETMASK <%x>", igNetmask);
		rc = RC_SUCCESS;
  } /* fi */
  /********************** JWE: 20041022 - END PRF 6604 **************************/

  if (rc == RC_SUCCESS)
  {
    prgStHd = (STACK_HEAD *) malloc (PACKET_LEN);
    if (prgStHd == NULL)
    {
      dbg (TRACE, "init_bchdl: MALLOC STACKHEAD FAILED");
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    prgOutPacket = (COMMIF *) malloc (PACKET_LEN);
    if (prgOutPacket == NULL)
    {
      dbg (TRACE, "init_bchdl: MALLOC OUT PACKET FAILED");
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    prgCloPacket = (COMMIF *) malloc (PACKET_LEN);
    if (prgCloPacket == NULL)
    {
      dbg (TRACE, "init_bchdl: MALLOC CLO PACKET FAILED");
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    prgReadBcPkt = (COMMIF *) malloc (PACKET_LEN);
    if (prgReadBcPkt == NULL)
    {
      dbg (TRACE, "init_bchdl: MALLOC READ PACKET FAILED");
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    pfgBcStack = fopen(pclStackFile,"r+");
    if (pfgBcStack == NULL)
    {
      pfgBcStack = fopen(pclStackFile,"w+");
      if (pfgBcStack != NULL)
      {
        fclose(pfgBcStack);
      } /* end if */
      pfgBcStack = fopen(pclStackFile,"r+");
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    if (pfgBcStack == NULL)
    {
      dbg(TRACE,"init_bchdl: UNABLE TO OPEN STACK FILE <%s>",pclStackFile);
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    dbg(TRACE,"init_bchdl: OPENED STACK FILE <%s>",pclStackFile);
    rc = fstat(fileno(pfgBcStack), &rlStat);
    dbg(TRACE,"init_bchdl: SIZE: %d",rlStat.st_size);
    if (rlStat.st_size >= PACKET_LEN)
    {
      fread((void *)prgStHd,PACKET_LEN,1,pfgBcStack);
      dbg(TRACE,"init_bchdl: BC NUM: ACTUAL=%d MAX=%d",
                 prgStHd->ActBcNum, prgStHd->MaxBcNum);
    } /* end if */
    else
    {
      memset(prgStHd,0x00,PACKET_LEN);
      prgStHd->MaxBcNum = MAX_BC_NUM;
      fseek(pfgBcStack, 0, SEEK_SET);
      fwrite((void *)prgStHd,PACKET_LEN,1,pfgBcStack);
      fflush(pfgBcStack);
      llRecPos = (MAX_BC_NUM+1) * PACKET_LEN;
      dbg(TRACE,"init_bchdl: FIRST BYTE OF HOST ARRAY = %d",llRecPos);
      memset(prgOutPacket,0x00,PACKET_LEN);
      fseek(pfgBcStack, llRecPos, SEEK_SET);
      fwrite((void *)prgOutPacket,PACKET_LEN,1,pfgBcStack);
      fflush(pfgBcStack);
    } /* end else */
  } /* end if */

  dbg(TRACE,"init_bchdl: DO FILTER: HOSTNAME <%s>", pcgHostName);

  if (rc == RC_SUCCESS)
  {
    ReadSendAlways();
    ReadUdpAdrFile();
    InitNetworkRegister();

    dbg(TRACE,"init_bchdl: ------------------------");

    ReadCfg(cfgfile,pclTmp,"SYSTEM","SUPPORT_16BIT","NO");  
    if (strcmp(pclTmp,"YES") == 0)
    {
      igSupport16Bit = TRUE;
      dbg(TRACE,"init_bchdl: PORT 16BIT IS SUPPORTED");
    } /* end if */
    else
    {
      igSupport16Bit = FALSE;
      dbg(TRACE,"init_bchdl: PORT 16BIT IS NOT SUPPORTED");
    } /* end else */

    dbg(TRACE,"init_bchdl: ------------------------");

    ReadCfg(cfgfile,pclTmp,"SYSTEM","SEND_MODULE_NAME","NO");  
		if (strcmp(pclTmp,"YES") == 0)
		{
			igSendModuleName = TRUE;
			dbg(TRACE,"init_bchdl: SEND MODULE NAME IS SUPPORTED");
		} /* end if */
		else
		{
			igSendModuleName = FALSE;
			dbg(TRACE,"init_bchdl: SEND MODULE NAME NOT SUPPORTED");
		} /* end else */
    ReadCfg(cfgfile,pclTmp,"SYSTEM","TEST_P4_TIMESPAN","NO");  
		if (strcmp(pclTmp,"YES") == 0)
		{
			igTestP4TimeSpan = TRUE;
			dbg(TRACE,"init_bchdl: TEST_P4_TIMESPAN IS ACTIVATED");
		} /* end if */
		else
		{
			igTestP4TimeSpan = FALSE;
			dbg(TRACE,"init_bchdl: TEST_P4_TIMESPAN DISABLED");
		} /* end else */
    ReadCfg(cfgfile,pclTmp,"SYSTEM","MOVE_RBS_TO_P4","NO");  
		if (strcmp(pclTmp,"YES") == 0)
		{
			igMoveRbsToP4 = TRUE;
			dbg(TRACE,"init_bchdl: MOVE_RBS_TO_P4 IS ACTIVATED");
		} /* end if */
		else
		{
			igMoveRbsToP4 = FALSE;
			dbg(TRACE,"init_bchdl: MOVE_RBS_TO_P4 DISABLED");
		} /* end else */
    ReadCfg(cfgfile,pclTmp,"SYSTEM","SPOOL_RBS_REQUEST","-1");  
		igSpoolRbsRequest = atoi(pclTmp);
		if (igSpoolRbsRequest > 0)
		{
			dbg(TRACE,"init_bchdl: SPOOLING RBS REQUEST SET TO <%d>",igSpoolRbsRequest);
		} /* end if */
		else
		{
			dbg(TRACE,"init_bchdl: SPOOLING RBS REQUEST SET TO <NO>");
		} /* end else */

    dbg(TRACE,"init_bchdl: ------------------------");

    ReadCfg(cfgfile,pcgUdpCutOff,"SYSTEM","UDP_CUT_OFF","{=ATTACH=}");  
    dbg(TRACE,"init_bchdl: UDP_CUT_OFF: <%s>",pcgUdpCutOff);

    ReadCfg(cfgfile,pcgUdpNotOut,"SYSTEM","UDP_NOT_OUT","{=NOUDP=}");  
    dbg(TRACE,"init_bchdl: UDP_NOT_OUT: <%s>",pcgUdpNotOut);

    ReadCfg(cfgfile,pcgTcpNotOut,"SYSTEM","TCP_NOT_OUT","{=NODATA=}");  
    dbg(TRACE,"init_bchdl: TCP_NOT_OUT: <%s>",pcgTcpNotOut);

    ReadCfg(cfgfile,pclTmp,"SYSTEM","TCP_OUT_QUE","0");  
		igTcpOutQue = atoi(pclTmp);
		if (igTcpOutQue > 0)
		{
			dbg(TRACE,"init_bchdl: TCP_OUT_QUE <%d> ACTIVATED",igTcpOutQue);
		} /* end if */
		else
		{
			dbg(TRACE,"init_bchdl: TCP_OUT_QUE NOT ACTIVATED");
		} /* end else */

    dbg(TRACE,"init_bchdl: ------------------------");
    ReadCfg(cfgfile,pclTmp,"SYSTEM","CEI_OUT_QUE","0");  
		igCeiOutQue = atoi(pclTmp);
		if (igCeiOutQue > 0)
		{
			dbg(TRACE,"init_bchdl: CEI_OUT_QUE <%d> ACTIVATED",igCeiOutQue);
		} /* end if */
		else
		{
			dbg(TRACE,"init_bchdl: CEI_OUT_QUE NOT ACTIVATED");
		} /* end else */
 
    pcgIgnoreCeiTables[0] = 0x00;
    ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES",
                    "CEISND_IGNORE",CFG_STRING,pcgIgnoreCeiTables);
    if (ilGetRc == RC_SUCCESS)
    {
        dbg(TRACE,"init_bchdl: IGNORED CEI TABLES <%s>",pcgIgnoreCeiTables);
    } /* end if */
    else
    {
        dbg(TRACE,"init_bchdl: NO CEI TABLES IGNORED");
    }


	  dbg(TRACE,"init_bchdl: ------------------------");
    dbg(TRACE,"init_bchdl: BCQ FILE DIRECTORY <%s>",pcgBcqPath);
    ReadCfg(cfgfile,pclTmp,"SYSTEM","MAX_BCQ_SIZE","8192");  
    igMaxBcqSize = atoi(pclTmp);
    dbg(TRACE,"init_bchdl: MAX_BCQ_SIZE = %d",igMaxBcqSize);
	  dbg(TRACE,"init_bchdl: ------------------------");

    GetServerTimeStamp ("UTC", 1, 0, pcgTimeStamp);
    dbg(TRACE,"init_bchdl: INITIAL TIMESTAMP <%s>",pcgTimeStamp);
    dbg(TRACE,"init_bchdl: REPORT DIRECTORY <%s>",cfgRptPath);
    InitDailyReport(FALSE);
    ReadCfg(cfgfile,pclTmp,"SYSTEM","REFRESH_STATISTIC","0");  
		lgRefreshStatistic = atoi(pclTmp);
		if (lgRefreshStatistic > 0)
		{
			dbg(TRACE,"init_bchdl: REFRESH STATISTIC SET TO <%d> MINUTES",lgRefreshStatistic);
		} /* end if */
		else
		{
			dbg(TRACE,"init_bchdl: REFRESH STATISTIC SWITCHED OFF");
		} /* end else */
	  dbg(TRACE,"init_bchdl: ------------------------");
    ilGetRc = tool_search_exco_data ("SYS", "HOMEAP", pcgSgsPrj);
    if (ilGetRc == RC_SUCCESS)
    {
      dbg(TRACE,"init_bchdl: SGS.TAB HOMEAP <%s>",pcgSgsPrj);
    } /* end if */
    else
    {
      strcpy(pcgSgsPrj,"???");
      dbg(TRACE,"init_bchdl: UNKNOWN HOMEAP <%s>",pcgSgsPrj);
    }

    CT_InitStringDescriptor(&rgBcqFileData);

    dbg(TRACE,"init_bchdl: ------------------------");
    dbg(TRACE,"init_bchdl: SUPPORTING %d WKS CONNECTIONS (NO_OF_REMOTE_HOSTS)",NO_OF_REMOTE_HOSTS); 
    dbg(TRACE,"init_bchdl: SUPPORTING %d NET CONNECTIONS (NO_OF_REMOTE_NETWORKS)",NO_OF_REMOTE_NETWORKS); 
    RuntimeConfig();
    dbg(TRACE,"init_bchdl: BCHDL init OK !"); 
  } /* fi */
  else
  {
    dbg(TRACE,"init_bchdl: BCHDL INIT WITH ERRORS !"); 
  } /* end else */

  return rc;
} /* end of initialize */

/* ******************************************************************** */
/* ******************************************************************** */
static int RuntimeConfig(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclTmp[1024];
  dbg(TRACE,"CMD200 BGN: ====== RUNTIME CONFIGURATION START ======");

  pcgIgnoreTables[0] = 0x00;
  ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES", "IGNORE_TABLES",CFG_STRING,pcgIgnoreTables);
  if (ilGetRc == RC_SUCCESS)
  {
    dbg(TRACE,"init_bchdl: MSG INPUT FILTER 1 (IGNORE_TABLES   ) <%s>",pcgIgnoreTables);
  } /* end if */
  else
  {
    pcgIgnoreTables[0] = 0x00;
    dbg(TRACE,"init_bchdl: MSG INPUT FILTER 1 (IGNORE_TABLES   ) <NOTHING DEFINED>");
  }

  pcgIgnoreTableTws[0] = 0x00;
  ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES", "IGNORE_TABLE_TWS",CFG_STRING,pcgIgnoreTableTws);
  if (ilGetRc == RC_SUCCESS)
  {
    dbg(TRACE,"init_bchdl: MSG INPUT FILTER 2 (IGNORE_TABLE_TWS) <%s>",pcgIgnoreTableTws);
  } /* end if */
  else
  {
    pcgIgnoreTableTws[0] = 0x00;
    dbg(TRACE,"init_bchdl: MSG INPUT FILTER 2 (IGNORE_TABLE_TWS) <NOTHING DEFINED>");
  }

  pcgIgnoreTableUdp[0] = 0x00;
  ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES", "IGNORE_TABLE_UDP",CFG_STRING,pcgIgnoreTableUdp);
  if (ilGetRc == RC_SUCCESS)
  {
    dbg(TRACE,"init_bchdl: UDP OUTPUT FILTER: (IGNORE_TABLE_UDP) <%s>",pcgIgnoreTableUdp);
  } /* end if */
  else
  {
    pcgIgnoreTableUdp[0] = 0x00;
    dbg(TRACE,"init_bchdl: UDP OUTPUT FILTER: (IGNORE_TABLE_UDP) <NOTHING DEFINED>");
  }

  pcgIgnoreTableTcp[0] = 0x00;
  ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES", "IGNORE_TABLE_TCP",CFG_STRING,pcgIgnoreTableTcp);
  if (ilGetRc == RC_SUCCESS)
  {
    dbg(TRACE,"init_bchdl: TCP OUTPUT FILTER: (IGNORE_TABLE_TCP) <%s>",pcgIgnoreTableTcp);
  } /* end if */
  else
  {
    pcgIgnoreTableTcp[0] = 0x00;
    dbg(TRACE,"init_bchdl: TCP OUTPUT FILTER: (IGNORE_TABLE_TCP) <NOTHING DEFINED>");
  }

  dbg(TRACE,"init_bchdl: ------------------------");

  ReadCfg(cfgfile,pclTmp,"SYSTEM","SEND_BC_PORT","YES");  
  if (strcmp(pclTmp,"YES") == 0)
  {
    igSend32BitPort = TRUE;
    dbg(TRACE,"init_bchdl: SINGLE UDP TO MY OWN NET: SWITCHED ON");
  } /* end if */
  else
  {
    igSend32BitPort = FALSE;
    dbg(TRACE,"init_bchdl: SINGLE UDP TO MY OWN NET: SWITCHED OFF");
  } /* end else */

  ReadCfg(cfgfile,pclTmp,"SYSTEM","SEND_UDP_WKS","YES"); 
  if (strcmp(pclTmp,"YES") == 0)
  {
    if (igSend32BitNetDgr == TRUE)
    {
      dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL NET: SWITCHED OFF");
    }
    igSend32BitWksDgr = TRUE;
    igSend32BitNetDgr = FALSE;
    dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL WKS: SWITCHED ON");
  } /* end if */
  else
  {
    igSend32BitWksDgr = FALSE;
    dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL WKS: SWITCHED OFF");
  } /* end else */

  ReadCfg(cfgfile,pclTmp,"SYSTEM","SEND_UDP_NET","NO");
  if (strcmp(pclTmp,"YES") == 0)
  {
    if (igSend32BitWksDgr == TRUE)
    {
      dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL WKS: SWITCHED OFF");
    }
    igSend32BitNetDgr = TRUE;
    igSend32BitWksDgr = FALSE;
    dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL NET: SWITCHED ON");
  } /* end if */
  else
  {
    igSend32BitNetDgr = FALSE;
    dbg(TRACE,"init_bchdl: DATAGRAM TO EXTERNAL NET: SWITCHED OFF");
  } /* end else */

  dbg(TRACE,"init_bchdl: ------------------------");

  ReadCfg(cfgfile,pclTmp,"SYSTEM","OUTEVENT_PRIO","0");  
  if (strcmp(pclTmp,"AUTO") == 0)
  {
    igTcpOutPri = -1;
    dbg(TRACE,"init_bchdl: MSG OUT PRIORITY TO BCCOM (OUTEVENT_PRIO) = <AUTO>");
    dbg(TRACE,"init_bchdl: (USING RECEIVED PRIO FOR EVENTS TO BCCOM)");
  }
  else
  {
    igTcpOutPri = atoi(pclTmp);
    if (igTcpOutPri == 0)
    {
      igTcpOutPri = 3;
    }
    if (igTcpOutPri < 1)
    {
      igTcpOutPri = 1;
    }
    if (igTcpOutPri > 5)
    {
      igTcpOutPri = 5;
    }
    dbg(TRACE,"init_bchdl: MSG OUT PRIORITY TO BCCOM (OUTEVENT_PRIO) = <%d>",igTcpOutPri);
  }

  dbg(TRACE,"init_bchdl: ------------------------");

  ReadCfg(cfgfile,pclTmp,"SYSTEM","BRC_DELAY_TIME","0");  
  lgNapTime = atol(pclTmp);
  if (lgNapTime < 0)
  {
    lgNapTime = 0;
  }
  dbg(TRACE,"init_bchdl: BRC_DELAY_TIME = NAP(%d)",lgNapTime);
  ReadCfg(cfgfile,pclTmp,"SYSTEM","UDP_DELAY_TIME","0");  
  lgNapTime2 = atol(pclTmp);
  if (lgNapTime2 < 0)
  {
    lgNapTime2 = 0;
  }
  dbg(TRACE,"init_bchdl: UDP_DELAY_TIME = NAP(%d)",lgNapTime2);

  dbg(TRACE,"CMD200 END: ====== RUNTIME CONFIGURATION READY ======");

  return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int ResetBcHdl(void)
{
  int rc = 0;
  dbg(TRACE,"TERMINATE FOR RESET");
  rc = SendInternalCommand(-1, "RST", "CLORST", "", "", "");
  terminate(500);
  return RC_SUCCESS;
} /* end ResetBcHdl */

/* ******************************************************************** */
/* Following the create_bc_socket function				*/
/* ******************************************************************** */
int create_bc_socket (void)
{
  int	rc = RC_SUCCESS;	/* Return code */
  int	sock;			/* The socket */

#if defined(_HPUX_SOURCE) || defined(_SNI) || defined(_SOLARIS) || defined(_UNIXWARE) || defined(_LINUX)
  int broadcast_bool=1;
#else
#if defined (_WINNT)
  int bool_value = 1;
  int *broadcast_bool=&bool_value;
#else
  short broadcast_bool=1;	/* Arg for setsockopt */
#endif
#endif

  sock = socket(AF_INET,SOCK_DGRAM,0); /* create socket */

  if (sock < 0 ) {
    dbg(TRACE,"BCHDL: Error open socket: %s", strerror( errno));
    
    rc = RC_FAIL;
  } /* end if */
  else
  {
    dbg(DEBUG,"BCHDL:Socket opened"); 
  
    rc = setsockopt (sock, SOL_SOCKET, SO_BROADCAST, (char *) &broadcast_bool, 
		   sizeof (broadcast_bool));
    if (rc == -1 )
    {	
      dbg(TRACE,"BCHDL: Error Setsockopt; %s", strerror(errno));
      
      rc = RC_FAIL;
    } 	/* end if */
  }

  if (rc == RC_SUCCESS)
    return sock;
  else
    return RC_FAIL;
} /* end create_bc_socket */


/* ******************************************************************** */
/* Following the SendBroadCast function					*/
/* Sends PACKET_LEN bytes out of the buffer on the given socket		*/
/* ******************************************************************** */
int SendBroadCast(int sock, char *pcpOutBuf, int ipPktNbr, int ipPktCnt)
{
  int	rc = RC_SUCCESS;
	int ilGetRc = RC_SUCCESS;
  short slActCmd = 0;
  short slActBcNum = 0;
  COMMIF *prlComm = NULL;
  BC_HEAD *prlBcHd = NULL;

  prlComm = (COMMIF *) pcpOutBuf;
  prlBcHd = (BC_HEAD *)prlComm->data;

  prlComm->length = 0;

  if (ipPktCnt == 1)
  {
    slActCmd = PACKET_SINGLE;
  } /* end if */
  else if(ipPktNbr == 1)
  {
    slActCmd = PACKET_START;
  } /* end else if */
  else if(ipPktNbr == ipPktCnt)
  {
    slActCmd = PACKET_END;
  } /* end else if */
  else
  {
    slActCmd = PACKET_DATA;
  } /* end else if */

  SetShortValue(&prlComm->command, slActCmd);

  slActBcNum = GetNextBcNumber();
  SetShortValue(&prlBcHd->bc_num, slActBcNum);
  SetShortValue(&prlBcHd->act_buf, ipPktNbr);
  SetShortValue(&prlBcHd->tot_buf, ipPktCnt);

  rc = BcBufAdd(slActBcNum, pcpOutBuf);

/* ============================================ */
/* TEST ONLY !!!!! */
  if (igTestLoose == TRUE)
  {
  	ilGetRc = TestBcForLoose(slActBcNum, ipPktNbr);
	if (ilGetRc != RC_SUCCESS)
	{
		return RC_SUCCESS;
	} /* end if */
  } /* end if */
/* ============================================ */


  rc = SendTo32BitPort(sock, pcpOutBuf);

  /* The old Ufis16Bit.Dll always looks for PACKET_END */
  /* All Other Commands cause servere Errors !! */

  SetShortValue(&prlComm->command, PACKET_END);
  rc = SendTo16BitPort(sock, pcpOutBuf);

  if ((igBcSent > 0) || (igSend32BitPort == TRUE))
  {
    dbg(TRACE,"BC SENT: NUM %d (%d/%d)",slActBcNum,ipPktNbr,ipPktCnt);
  }
  else
  {
    dbg(TRACE,"NOBODY IS CONNECTED");
    dbg(TRACE,"SPOOLED: NUM %d (%d/%d)",slActBcNum,ipPktNbr,ipPktCnt);
  }

  return RC_SUCCESS;
} /* end of SendBroadCast */

/* ******************************************************************** */
/* ******************************************************************** */
static int RebuildBcOutQueues(void)
{
  int ilRC = RC_SUCCESS;
  int i = 0;
  int ilQueCnt = 0;
  char *pclPtr = NULL;
  for (i=0; i<NO_OF_REMOTE_HOSTS; i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      if (rhosts_que[i] <= 0)
      {
        pclPtr = strstr(remote_hosts[i],".");
        if (pclPtr != NULL)
        {
          pclPtr++;
          rhosts_que[i] = atoi(pclPtr);
          ilQueCnt++;
        }
      } /* end if */
    } /* fi */
  } /* for */
  dbg(TRACE,"BCQ_CONNECTIONS: %d RESTORED",ilQueCnt);
  return ilRC;
} /* end RebuildBcOutQueues */

static void SendCeiOutEvents(void)
{
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  if (igCeiOutQue > 0)
  {
    ilLen = sizeof(EVENT) + event->data_length;
    event->originator = mod_id;
    ilGetRc = que(QUE_PUT,igCeiOutQue,mod_id,3,ilLen,(char *)event);
    if (ilGetRc != RC_SUCCESS)
    {
      dbg(TRACE,"INVALID CEI_OUT_QUE <%d> CLOSED",igCeiOutQue);
      igCeiOutQue = 0;
    }
    else
    {
      dbg(TRACE,"BC EVENT SENT TO CEI_OUT_QUE <%d>", igCeiOutQue);
    }
  }
  return;
}


/* ******************************************************************** */
/* ******************************************************************** */
static int SendBcOutEvents(char *pcpTbl, char *pcpCmd, int ipPrio)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilSendIt = FALSE;
  int i = 0;
  int len = 0;
  int ilSntCnt = 0;
  int ilQueCnt = 0;
  int ilBcqGrp = 0;
  int ilGrpPos = 0;
  int ilFileMode = FALSE;
  char pclSize[16];
  char pclBcqCnt[16];

  pfgBcqPtr = NULL;

  len = sizeof(EVENT) + event->data_length;
  /* DISABLE BCQ AND FILE MODE FEATURES */
  len = 0;
  ilFileMode = FALSE;

  if (len > igMaxBcqSize)
  {
    dbg(TRACE,"USING BCQ FILE MODE (SIZE = %d)", len);
    rgBcqFileData.UsedLen = 0;
    ilFileMode = TRUE;
    /* ACTIVATE ONLY FOR TEST PURPOSES */
    /* ELSE COMMENT IT OUT */
    /* ilGetRc = CreateBcqFileData(pcgBcqFile, &rgBcqFileData, FALSE, pclSize); */
  }

  if (igTcpOutQue > 0)
  {
    if ((ilFileMode == TRUE) && (rgBcqFileData.UsedLen == 0))
    {
      ilGetRc = CreateBcqFileData(pcgBcqFile, &rgBcqFileData, FALSE, pclSize);
    }
    len = sizeof(EVENT) + event->data_length;
    ilRC = que(QUE_PUT,igTcpOutQue,mod_id,ipPrio,len,(char *)event);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"QUEUE ERROR: INVALID TCP_OUT_QUE <%d> CLOSED",igTcpOutQue);
      igTcpOutQue = 0;
    }
    else
    {
      dbg(TRACE,"BC EVENT FORWARDED TO TCP_OUT_QUE (BCCOM) <%d> ON PRIO <%d>", igTcpOutQue, ipPrio);
    }
  }

/* DISABLE BCQ AND FILE MODE FEATURES */
len = 0;
if (len > 0)
{
  for (i=0; i<NO_OF_REMOTE_HOSTS; i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      if (rhosts_que[i] > 0)
      {
        ilQueCnt++;
        ilSendIt = TRUE;
        if (rhosts_key[i].UsedLen > 0)
        {
          dbg(DEBUG,"%s FILTER: <%s>",remote_hosts[i],rhosts_key[i].Value);
          if (strstr(rhosts_key[i].Value,pcpTbl) == NULL)
          {
            ilSendIt = FALSE;
          }
        }
        if (ilSendIt != TRUE)
        {
          if (strstr("TIMERS,SYSTEM",pcpTbl) != NULL)
          {
            ilSendIt = TRUE;
          }
          /*
          if (strstr("SBC,...",pcpCmd) != NULL)
          {
            ilSendIt = TRUE;
          }
          */
        }
        if (ilSendIt == TRUE)
        {
          if ((ilFileMode == TRUE) && (rgBcqFileData.UsedLen == 0))
          {
            ilGetRc = CreateBcqFileData(pcgBcqFile, &rgBcqFileData, FALSE, pclSize);
          }
          if ((ilGetRc != RC_SUCCESS) || ((ilFileMode == TRUE) && (pfgBcqPtr == NULL)))
          {
            dbg(TRACE,"UNABLE TO USE FILE MODE. USING SHM EVENTS NOW !");
            ilFileMode = FALSE;
          }
          dbg(DEBUG,"SENDING TO BC_OUT_QUE <%s> (%s)",remote_hosts[i],rhosts_cpid[i]);
          if (ilFileMode == TRUE)
          {
            ilGrpPos = rgBcqFileData.UsedLen + ilSntCnt + 1;
            fseek(pfgBcqPtr, ilGrpPos, SEEK_SET);
            fwrite((void *)"S",1,1,pfgBcqPtr);
            fflush(pfgBcqPtr);
            sprintf(pclBcqCnt,"%d",ilSntCnt);
            ilRC = SendCedaEvent (rhosts_que[i], /* adress to send the answer   */
                           0,                    /* Set to mod_id if < 1        */
                           "",                   /* BC_HEAD.dest_name           */
                           "",                   /* BC_HEAD.recv_name           */
                           "",                   /* CMDBLK.tw_start             */
                           "",                   /* CMDBLK.tw_end               */
                           "BCQF",               /* CMDBLK.command              */
                           "BCQFILE",            /* CMDBLK.obj_name             */
                           pcgBcqFile,           /* Selection Block             */
                           pclBcqCnt,            /* Field Block                 */
                           pclSize,               /* Data Block                  */
                           "",                   /* Error description           */
                           3,                    /* 0 or Priority               */
                           RC_SUCCESS);          /* BC_HEAD.rc: RC_SUCCESS      */
          }
          else
          {
            ilRC = que(QUE_PUT,rhosts_que[i],mod_id,3,len,(char *)event);
          }
          if (ilRC != RC_SUCCESS)
          {
            dbg(TRACE,"INVALID BC_OUT_QUE <%s> (%s) CLOSED",remote_hosts[i],rhosts_cpid[i]);
            rhosts_cnt[i]--;
            if (rhosts_cnt[i] <= 0)
            {
              rhosts_que[i] = -1;
              if (rhosts_key[i].AllocatedSize > 0)
              {
                rhosts_key[i].Value[0] = 0x00;
              }
              rhosts_key[i].UsedLen = 0;
              remote_hosts[i][0] = EOS;
              rhosts_cnt[i] = 0;
              rhosts_cpid[i][0] = 0x00;
            }
          }
          else
          {
            dbg(DEBUG,"SENT BCQ_EVENT TO <%s> (%s)",remote_hosts[i],rhosts_cpid[i]);
            ilSntCnt++;
          }
        }
      } /* end if */
    } /* fi */
  } /* for */
  dbg(TRACE,"SENT BCQ_EVENT TO <%d/%d> TCP CLIENTS",ilSntCnt,ilQueCnt);

  if ((ilSntCnt > 0) && (pfgBcqPtr != NULL))
  {
    ilGrpPos = rgBcqFileData.UsedLen + ilSntCnt + 1;
    fseek(pfgBcqPtr, ilGrpPos, SEEK_SET);
    fwrite((void *)"*",1,1,pfgBcqPtr);
    fflush(pfgBcqPtr);
    ilGrpPos = rgBcqFileData.UsedLen;
    fseek(pfgBcqPtr, ilGrpPos, SEEK_SET);
    fwrite((void *)"!",1,1,pfgBcqPtr);
    fflush(pfgBcqPtr);
    fclose(pfgBcqPtr);
    pfgBcqPtr = NULL;
    CheckBcqFile(rgBcqFileData.UsedLen, ilSntCnt);
  }
}

  return ilRC;
} /* end SendBcOutEvents */

/* ******************************************************************** */
/* Sends to PORT1 on the given socket		*/
/* ******************************************************************** */
int SendTo16BitPort(int sock, char *pcpOutBuf)
{
  int	rc = RC_SUCCESS;
  int i;
  struct servent *sp;
  struct servent *sp2;
  struct sockaddr_in dummy;

	if (igSupport16Bit != TRUE)
	{
    /* dbg(TRACE,"16BIT SERVICE NOT SUPPORTED "); */
    return RC_SUCCESS;
	} /* end if */

  if ( ( sp = getservbyname("UFIS_BC",NULL) ) == NULL )
  {
    dbg(TRACE,"UNKNOWN SERVICE UFIS_BC (16Bit)");
    return RC_SUCCESS;
  } /* end if */

  dummy.sin_addr.s_addr = htonl(ulgBroadcastAddr);
  dummy.sin_family = AF_INET;
  dummy.sin_port   = (u_short) sp->s_port;

  /* Sending to 16Bit Port */
  /* BUT: Check If it Is Configured */

    rc = sendto(sock, pcpOutBuf, PACKET_LEN,0, (struct sockaddr *) &dummy, 
		sizeof (struct sockaddr_in));
    if ( rc < 0 ) 
    {
      dbg(TRACE,"SEND_BC: PORT1 SENDTO SOCKET = 0x%x: ", sock);
      dbg(TRACE,"ERROR  : RC = %d : STR=%s ", rc, strerror( errno));
    } 
    else 
    {
      dbg(DEBUG,"SEND_BC PORT1: %d BYTES SEND ",rc);
    }	/* end else */

  if (rc < 0)
    return RC_FAIL;
  else
    return RC_SUCCESS;
} /* end of SendTo16BitPort */

/* ******************************************************************** */
/* Sends to PORT2 on the given socket		*/
/* ******************************************************************** */
int SendTo32BitPort(int sock, char *pcpOutBuf)
{
  int	rc = RC_SUCCESS;
  int i;
  int ilBreakOut = FALSE;
  int iTest = 1;
  int ilCnt = 0;
  char pclSpoolInfo[64];
  struct servent *sp;
  struct servent *sp2;
  struct sockaddr_in dummy;

  if (pcgPort2[0] != EOS)
  {
    if ((sp2 = getservbyname(pcgPort2,NULL)) == NULL)
    {
      dbg(TRACE,"UNKNOWN SERVICE %s (32bit)", pcgPort2);
      return RC_SUCCESS;
    } /* end if */
  } /* end if */

  if (pcgPort2[0] != EOS)
  {
    if (igSend32BitPort == TRUE)
    {
      dummy.sin_addr.s_addr = htonl(ulgBroadcastAddr);
      dummy.sin_family = AF_INET;
      dummy.sin_port   = (u_short) sp2->s_port;

      rc = sendto(sock, pcpOutBuf, PACKET_LEN,0, (struct sockaddr *) &dummy, sizeof (struct sockaddr_in));
      if ( rc < 0 ) 
      {
        dbg(TRACE,"SEND_BC: PORT2 SENDTO SOCKET = 0x%x: ", sock);
        dbg(TRACE,"ERROR  : RC=%d STR=%s ", rc, strerror( errno));
      }   
      else 
      {
        lgTotalP2Sent++;
        /* dbg(DEBUG,"SEND_BC PORT2: %d BYTES SEND ",rc); */
      } /* fi */
    }
    if (igSend32BitWksDgr == TRUE)
    {
      for (i=0; i<NO_OF_REMOTE_HOSTS; i++)
      {
        if ((remote_hosts[i][0] != EOS) && 
            ((rhosts_que[i] < 1) || ((rhosts_que[i] > 0) && (rhosts_cnt[i] > 1))))
        {
          if ((strncmp(remote_hosts[i],"BCQ",3) != 0) && (rhosts_cnt[i] > 0))
          {
            if (pcgPort2[0] != EOS)
            {
              rc = tcp_send_datagram (sock, remote_hosts[i], NULL, pcgPort2, pcpOutBuf, PACKET_LEN);
              dbg(DEBUG,"DATAGRAM SENT TO <%s> PORT2",remote_hosts[i]);
              ilCnt++;
              lgTotalD2Sent++;
              if (lgNapTime2 > 0L)
              {
                nap(lgNapTime2);
              } /* end if */
            } /* fi */
          } /* fi */
        } /* fi */
      } /* for */
      if (ilCnt > 0)
      {
        dbg(TRACE,"DATAGRAM SENT TO <%d> EXTERNAL CLIENTS ON PORT2",ilCnt);
      }
    }
    if (igSend32BitNetDgr == TRUE)
    {
      for (i=0; ((ilBreakOut!=TRUE) && (i<NO_OF_REMOTE_NETWORKS)); i++)
      {
        if (remote_net_cnt[i] > 0)
        {
          if (pcgPort2[0] != EOS)
          {
            rc = tcp_send_datagram (sock, remote_network[i], NULL, pcgPort2, pcpOutBuf, PACKET_LEN);
            dbg(DEBUG,"DATAGRAM SENT TO NETWORK <%s> PORT2",remote_network[i]);
            ilCnt++;
            lgTotalD2Sent++;
            if (lgNapTime2 > 0L)
            {
              nap(lgNapTime2);
            } /* end if */
          }
        }
        else if (remote_network[i][0] == EOS)
        {
          ilBreakOut = TRUE;
        }
      }
      if (ilCnt > 0)
      {
        dbg(TRACE,"DATAGRAM SENT TO <%d> EXTERNAL NETWORKS ON PORT2",ilCnt);
      }
    }
    if (ilCnt > 0)
    {
      if (ilCnt > igTotalCliCnt) igTotalCliCnt = ilCnt;
      if (ilCnt > lgTotalWksCon) lgTotalWksCon = ilCnt;
      rgClient.DataGram.Min[igRptMin] += ilCnt;
      rgClient.DataGram.Hour[igRptHour] += ilCnt;
      rgClient.DataGram.Day[igRptDay] += ilCnt;
      rgClient.DataGram.Month[igRptMonth] += ilCnt;
    }
  } /* fi */

  if (lgNapTime > 0L)
  {
    nap(lgNapTime);
  } /* end if */

  /*
  if (rc < 0)
    return RC_FAIL;
  else
  */

    igBcSent = ilCnt;
    return RC_SUCCESS;

} /* end of SendTo32BitPort */

/* ******************************************************************** */
/* The handle data routine						*/
/* Checks the incoming packet if it contains a request for an old bc or */
/* a new packet to broadcast and store and does the right things	*/
/* ******************************************************************** */

static int handle_data (void)
{
  int	rc = RC_SUCCESS;	/* Return code 			*/
  int ilSendUdp = TRUE;
  int ilSendTcp = TRUE;
  int ilSendCei = TRUE;
  int ilUdpCutOff = FALSE;
  int ilOldCutOff = FALSE;
  int ilEventIsValid = TRUE;
  int ilUdpEventIsValid = TRUE;
  int ilDatLenRcv = 0;
  int ilDatLenNow = 0;
  int ilFldLen = 0;
  int ilDatLen = 0;
  int ilSelLen = 0;
  int ilTotLen = 0;
  int ilStructLen = 0;
  int ilOffsetLen = 0;
  int ilTotalSize = 0;
  int ilPacketSize = 0;
  int ilPktCnt = 0;
  int ilPktNbr = 0;
  int ilPrio = 3;
  long llTimeDiff = 0;
  char pclChkTbl[64];
  char pclUtcTime[32];
  char pclLocTime[32];
  char pclTmpData[32];

  /* Incoming Event */
  BC_HEAD *prlRcvBcHd;		/* Broadcast header		*/
  CMDBLK *prlRcvCmdBlk;		/* Command block */
  char *pclRcvData = NULL;

  /* Outgoing Packet */
  BC_HEAD *prlOutBcHd;		/* Broadcast header		*/
  CMDBLK *prlOutCmdBlk;		/* Command block */
  char *pclOutData = NULL;

  char *pclSelKey = NULL; 
  char *pclFields = NULL; 
  char *pclData = NULL;   
  char *pclOldData = NULL;   
  char *pclUdpCutPos = NULL;   
  char *pclUdpNotPos = NULL;   
  char *pclTcpNotPos = NULL;   
  char *pclOldDumm = "\0";


  char	*pclCmdBuf = NULL;
  int ilSbcAnsw = FALSE;
  int i=0;

  que_out = event->originator;
  CountBcEvent(que_out);

  prlRcvBcHd  = (BC_HEAD *) ((char *)event + sizeof(EVENT));
  prlRcvCmdBlk = (CMDBLK *) prlRcvBcHd->data;
  pclRcvData = (char *)prlRcvCmdBlk->data;                  

  pclSelKey = (char *) pclRcvData;
  pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;  
  if (igTcpOutPri < 0)
  {
    ilPrio = item->priority;
  }
  else if (igTcpOutPri == 0)
  {
    ilPrio = 3;
  }
  else
  {
    ilPrio = igTcpOutPri;
  }

InitDailyReport(TRUE);

lgCurrTimeStamp = time(0L);
llTimeDiff = (lgCurrTimeStamp - lgLastTimeStamp) / 60;
if ((llTimeDiff >= lgRefreshStatistic) && (lgRefreshStatistic > 0))
{
  /* PrintStatisticToFile(llTimeDiff,FALSE); */
  ResetStatistic();
  dbg(TRACE,"============== START/END ================");
  lgLastTimeStamp = lgCurrTimeStamp;
}
else
{
  if ((llTimeDiff >= 60) && (lgRefreshStatistic <= 0))
  {
    ResetStatistic();
  }
}

if ((strcmp (prlRcvCmdBlk->command, "{RBS}") != 0) &&
    (strcmp (prlRcvCmdBlk->command, "P4TST") != 0))
{
  if (igSetStartEnd != TRUE)
  {
    if ((lgSpooledCount > 0) || (lgResentCount > 0))
    {
      if (lgSpooledCount > 0)
      {
        dbg(TRACE,"IN THE MEANTIME SPOOLED %d RBS",lgSpooledCount);
        lgSpooledCount = 0;
      }
      if (lgResentCount > 0)
      {
        dbg(TRACE,"IN THE MEANTIME RESENT: %d BCs",lgResentCount);
        lgResentCount = 0;
      }
      dbg(TRACE,"============== START/END ================");
    }
  }

  dbg(TRACE,"CMD <%s> TBL <%s> LAST BC: NUM=%d",
             prlRcvCmdBlk->command,prlRcvCmdBlk->obj_name,prgStHd->ActBcNum);
  dbg(TRACE,"FROM (%d) WKS <%s> USR <%s>",
             event->originator, prlRcvBcHd->recv_name, prlRcvBcHd->dest_name); 
  dbg(TRACE,"SPECIAL INFO PRIO (%d) TWS <%s> TWE <%s>",
             item->priority,prlRcvCmdBlk->tw_start,prlRcvCmdBlk->tw_end);
  if ((strcmp (prlRcvCmdBlk->command, "TIME") != 0) &&
      (strcmp (prlRcvCmdBlk->command, "TICO") != 0))
  {
    dbg(TRACE,"SEL <%s>",pclSelKey);
    dbg(TRACE,"FLD <%s>",pclFields);
    if (strcmp (prlRcvCmdBlk->command, "RBS") != 0)
    {
      dbg(TRACE,"DAT <%s>",pclData);  
    }
    else
    {
      dbg(TRACE,"DAT <...> (NOT PUBLISHED: AVOID LOGFILE JAM)");  
    }
  }
  igSetStartEnd = TRUE;
}
else
{
  igSetStartEnd = FALSE;
}

  if (igTestP4TimeSpan == TRUE)
  {
    if (item->priority < 4)
    {
      if (igTestP4TimeSet != TRUE)
      {
        lgCurrTimeStamp = time(0L);
        sprintf(pclTmpData,"%d",lgCurrTimeStamp);
        (void) tools_send_info_flag(mod_id,0, "", "", "", "", "",
         "","","P4TST","P4TEST",pclTmpData," "," ",4);
        igTestP4TimeSet = TRUE;
      }
      lgTestP3EvtCount++;
      lgTestP4EvtCount = 0;
    }
    else if(item->priority == 4)
    {
      lgTestP4EvtCount++;
    }
  }


  pclCmdBuf = strstr(prlRcvCmdBlk->obj_name,"/");
  if (pclCmdBuf != NULL)
  {
    *pclCmdBuf = 0x00;
    pclCmdBuf++;
    strcpy(prlRcvCmdBlk->command,pclCmdBuf);
    dbg(TRACE,"SBC CHANGED: CMD <%s> TBL <%s>",
               prlRcvCmdBlk->command,prlRcvCmdBlk->obj_name);
    ilSbcAnsw = TRUE;
  } /* end if */


  /*
  ilSelLen = strlen(pclSelKey);
  ilFldLen = strlen(pclFields);
  ilDatLen = strlen(pclData);
  ilTotLen = ilFldLen + ilDatLen + ilSelLen + 3;
  dbg(TRACE,"LENGTH: SEL=%d FLD=%d DAT=%d TOT=%d",
             ilSelLen, ilFldLen, ilDatLen, ilTotLen);
  */

  if (strcmp (prlRcvCmdBlk->command, BROADCAST_REQUEST) == 0)
  {
    /* Request for old broadcast packet */
    rc = handle_bc_req(prlRcvCmdBlk);
  }
  else if (strcmp (prlRcvCmdBlk->command, BROADCAST_TO_RESEND) == 0)
  {	
    /* Resend (again) Requested Broadcast to Originator */
    if ((item->priority < 4) && (igMoveRbsToP4 == TRUE))
    {
      lgTotalRbsCmd++;
      rc = HandleShiftRbsToP4(pclSelKey);
    }
    else
    {
      if (igMoveRbsToP4 != TRUE)
      {
        lgTotalRbsCmd++;
      }
      rc = HandleRbsSpoolOrSend(igSpoolRbsRequest, pclSelKey, pclFields, pclData);
    }
  }
  else if (strcmp (prlRcvCmdBlk->command, "{RBS}") == 0)
  {	
    /* Resend (again) spooled Broadcast to Originator */
    rc = HandleSpooledRbs(pclSelKey, pclFields, pclData);
  }
  else if (strcmp (prlRcvCmdBlk->command, "P4TST") == 0)
  {	
    /* Test availability of QuePrio 4 */
    lgCurrTimeStamp = time(0L);
    llTimeDiff = atol(pclSelKey);
    llTimeDiff = lgCurrTimeStamp - llTimeDiff;
    if (llTimeDiff > 1)
    {
      if (llTimeDiff > lgTestP4TimeMax)
      {
        lgTestP4TimeMax = llTimeDiff;
      }
      lgTestP4TimeSum += llTimeDiff;
      if ((lgTestP3EvtCount <= 0) && (lgTestP4EvtCount <= 1))
      {
        dbg(TRACE,"QUE_HOLD OR CPU_SLEEP %d SEC ON PRIO4",llTimeDiff);
      }
      dbg(TRACE,"TIMESPAN: WAITED %d SEC ON PRIO4 (MAX=%d) (SUM=%d) (EVT=%d)",
          llTimeDiff,lgTestP4TimeMax,lgTestP4TimeSum,lgTestP3EvtCount);
      dbg(TRACE,"============== START/END ================");
    }
    lgTestP3EvtCount = 0;
    lgTestP4EvtCount = 0;
    igTestP4TimeSet = FALSE;
  }
  else if (strcmp (prlRcvCmdBlk->command, "KEEP") == 0)
  {	
    /* keep registered client alive */
    rc = HandleKeepAlive (pclSelKey, 1);
    ilSbcAnsw = TRUE;
  }
  else if (strcmp (prlRcvCmdBlk->command, "BCOUT") == 0)
  {	
    /* Establish TCP/IP Messages  */
    dbg(TRACE,"CMD ATTACH <BC_OUT_CONNECTOR>");
    rc = handle_reg_cmd(pclSelKey, pclFields, pclData, TRUE);
  }
  else if (strcmp (prlRcvCmdBlk->command, "BCKEY") == 0)
  {	
    rc = StoreBcOutFilter(pclSelKey, pclFields, pclData);
  }
  else if (strcmp (prlRcvCmdBlk->command, "BCGET") == 0)
  {	
    dbg(TRACE,"CMD RESEND TO <BC_OUT_CONNECTOR>");
    rc = ResendBcEvents(pclSelKey, pclFields, pclData);
  }
  else if (strcmp (prlRcvCmdBlk->command, "BCOFF") == 0)
  {	
    /* Detach TCP/IP Messages  */
    dbg(TRACE,"CMD DETACH <BC_OUT_CONNECTOR>");
    rc = handle_unreg_cmd(pclSelKey,pclFields);
  }
  else if (strcmp (prlRcvCmdBlk->command, "CLOSE") == 0)
  {	
    /* close registered client alive connection */
    rc = HandleKeepAlive (pclSelKey, 0);
    ilSbcAnsw = TRUE;
  }
  else if (strcmp (prlRcvCmdBlk->command, BC_REGISTER_CMD) == 0)
  {	
    /* Network register message */
    rc = handle_reg_cmd(pclSelKey, pclData, "UDP", TRUE);
  }
  else if (strcmp (prlRcvCmdBlk->command, BC_UNREGISTER_CMD) == 0)
  {	
    /* Network unregister message */
    rc = handle_unreg_cmd(pclSelKey,pclData);
  }
  else
  {	
    /* ------------------------ */
    /* Distribute the Broadcast */
    /* ------------------------ */
    ilEventIsValid = TRUE;
    ilUdpEventIsValid = TRUE;
    sprintf(pclChkTbl,",%s,",prlRcvCmdBlk->obj_name);
    if (strstr(pcgIgnoreTables,pclChkTbl) != NULL)
    {
      ilEventIsValid = FALSE;
    }
    if (strstr(pcgIgnoreTableUdp,pclChkTbl) != NULL)
    {
      if (igTcpOutQue > 0)
      {
        dbg(TRACE,"BCCOM IS CONNECTED (OMIT UDP MESSAGE)");
        ilUdpEventIsValid = FALSE;
      }
      else
      {
        dbg(TRACE,"BCCOM IS NOT CONNECTED (MUST SEND UDP MESSAGE)");
        ilUdpEventIsValid = TRUE;
      }
    }
    sprintf(pclChkTbl,",%s.%s,",prlRcvCmdBlk->obj_name,
                                prlRcvCmdBlk->tw_start);
    if (strstr(pcgIgnoreTableTws,pclChkTbl) != NULL)
    {
      ilEventIsValid = FALSE;
    }
    if (ilEventIsValid == TRUE)
    {
      if (igCeiOutQue > 0)
      {
        ilSendCei = TRUE;
      }

      if ((strcmp (prlRcvCmdBlk->command, "TIME") == 0) ||
          (strcmp (prlRcvCmdBlk->command, "TICO") == 0))
      {
        strcpy(prlRcvCmdBlk->obj_name,"TIMERS");
        /* strcpy(prlRcvCmdBlk->tw_end,",,NTISCH"); */
        GetServerTimeStamp ("UTC", 1, 0, pclUtcTime);
        GetServerTimeStamp ("LOC", 1, 0, pclLocTime);
        dbg(TRACE,"TIMER TABLE NAME: <%s>",prlRcvCmdBlk->obj_name);
        dbg(TRACE,"TIMESTAMPS (TWS): UTC=<%s> LOC=<%s>",pclUtcTime,pclLocTime);
        sprintf(prlRcvCmdBlk->tw_start,"%s,%s",pclUtcTime,pclLocTime);
	ilSendCei = FALSE;

        /* Must extend the received event */
        ExtendEvent("SELECTION", "FIELDS", "DATA");

        /* and recreated the pointers     */
        prlRcvBcHd  = (BC_HEAD *) ((char *)event + sizeof(EVENT));
        prlRcvCmdBlk = (CMDBLK *) prlRcvBcHd->data;
        pclRcvData = (char *)prlRcvCmdBlk->data;                  

        pclSelKey = (char *) pclRcvData;
        pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
        pclData = (char *)pclFields + strlen(pclFields) + 1;  

        dbg(TRACE,"==== GENERATED INTERNAL BROADCAST EVENT ====");
        dbg(TRACE,"     CMD <%s> TBL <%s> LAST BC: NUM=%d",
             prlRcvCmdBlk->command,prlRcvCmdBlk->obj_name,prgStHd->ActBcNum);
        dbg(TRACE,"     FROM (%d) WKS <%s> USR <%s>",
             event->originator, prlRcvBcHd->recv_name, prlRcvBcHd->dest_name); 
        dbg(TRACE,"     SPECIAL INFO PRIO (%d) TWS <%s> TWE <%s>",
             item->priority,prlRcvCmdBlk->tw_start,prlRcvCmdBlk->tw_end);
        dbg(TRACE,"     SEL <%s>",pclSelKey);
        dbg(TRACE,"     FLD <%s>",pclFields);
        dbg(TRACE,"     DAT <%s>",pclData);  
      }
      if (strstr(pcgIgnoreCeiTables,pclChkTbl) != NULL)
      {
        ilSendCei = FALSE;
      }
      if (ilSendCei == TRUE)
      {
        SendCeiOutEvents();
      }

      ilSendUdp = TRUE;
      ilSendTcp = TRUE;
      ilDatLenRcv = strlen(pclData);

      pclTcpNotPos = strstr(pclData,pcgTcpNotOut);
      if (pclTcpNotPos != NULL)
      {
        dbg(TRACE,"WON'T SEND TCP: <%s>",pcgTcpNotOut);
        ilSendTcp = FALSE;
      }
      pclUdpNotPos = strstr(pclData,pcgUdpNotOut);
      if (pclUdpNotPos != NULL)
      {
        dbg(TRACE,"WON'T SEND UDP: <%s>",pcgUdpNotOut);
        ilSendUdp = FALSE;
        ilUdpEventIsValid = FALSE;
      }

      /* ------------------------- */
      /* Necessary for all clients */
      /* SendModuleName etc.       */
      /* ------------------------- */
      strncpy (prlRcvBcHd->orig_name, pcgHostName, 10);
      CreateTblBcNum(prlRcvCmdBlk->tw_end,
                       prlRcvCmdBlk->obj_name, prlRcvCmdBlk->command,
                       pclFields, pclData, ilUdpEventIsValid);

      pclUdpCutPos = strstr(pclData,pcgUdpCutOff);
      if (pclUdpCutPos == NULL)
      {
        dbg(TRACE,"BROADCAST TYPE: <STANDARD>");
      }
      else
      {
        dbg(TRACE,"BROADCAST TYPE: <%s>",pcgUdpCutOff);
      }

      if (ilSendTcp == TRUE)
      {
        if (pclUdpCutPos == NULL)
        {
          pclOldData = strstr(pclData,"\n");
          if (pclOldData != NULL)              
          {                                        
            *pclOldData = 0x00;                
            dbg(TRACE,"TCP: OLD DATA CUT OFF");
            ilOldCutOff = TRUE;
          } /* end if */                       
        }

        /* -------------------------- */
        /* TCP clients always get the */
        /* last valid UDP BcNum       */
        /* -------------------------- */
        prlRcvBcHd->bc_num = prgStHd->ActBcNum;
        rc = SendBcOutEvents(prlRcvCmdBlk->obj_name, prlRcvCmdBlk->command, ilPrio);
      }

      ilSendUdp = ilUdpEventIsValid;
      if (ilSendUdp == TRUE)
      {
        if (pclUdpCutPos != NULL)
        {
          dbg(TRACE,"UDP: DATA <%s> CUT OFF",pcgUdpCutOff);
          *pclUdpCutPos = 0x00;    
        } /* end if */
        if (ilOldCutOff == FALSE)
        {
          pclOldData = strstr(pclData,"\n");
          if (pclOldData != NULL)              
          {                                        
            *pclOldData = 0x00;                
            dbg(TRACE,"UDP: OLD DATA CUT OFF");
          } /* end if */                       
        }

        /* --------------------------------- */
        /* UDP clients need sequential BcNum */
        /* --------------------------------- */
        prlRcvBcHd->bc_num = prgStHd->ActBcNum + 1;
        if (prlRcvBcHd->bc_num > 30000)
        {
          prlRcvBcHd->bc_num = 1;
        }
    
        errno=0;
        rc = RC_SUCCESS;
        dbg(TRACE,"NOW SENDING UDP BROADCAST");

        ilDatLenNow = strlen(pclData);

        dbg(DEBUG,"DATA LENGTH: RCV=%d NOW=%d",ilDatLenRcv,ilDatLenNow);
        ilTotalSize = event->data_length - sizeof(BC_HEAD) - sizeof(CMDBLK);
        ilTotalSize -= (ilDatLenRcv - ilDatLenNow);
        /* Yes I know that that's not correct */
        /* But because data_length is not correct */
        /* this is the best way to forward the mist(ake) */

        dbg(DEBUG,"PACK LENGTH: TOTAL=%d USED=%d (+ EOS)",
                 event->data_length,ilTotalSize);

        prgOutPacket->command = PACKET_END;
        prgOutPacket->length = event->data_length;

        prlOutBcHd  = (BC_HEAD *) prgOutPacket->data;
        memcpy((char *)prlOutBcHd, (char *)prlRcvBcHd, sizeof(BC_HEAD));
        prlOutBcHd->rc = RC_SUCCESS;
        strncpy (prlOutBcHd->orig_name, pcgHostName, 10);

        prlOutCmdBlk = (CMDBLK *) prlOutBcHd->data;
        memcpy(prlOutCmdBlk, prlRcvCmdBlk, sizeof(CMDBLK));

        pclOutData = (char *)prlOutCmdBlk->data;

        ilOffsetLen = pclOutData - (char *) prgOutPacket;
        /* dbg(TRACE,"POINTER DISTANCE DATA-COMMIF=%d",ilOffsetLen); */

        ilPacketSize = PACKET_LEN - ilOffsetLen;
        memset(pclOutData, 0x00, ilPacketSize);

        /* Last 3 Bytes always zero */
        /* So the Client gets valid Pointers */
        /* to Selection, Fields and Data */
        ilPacketSize -= 3;
        /* dbg(TRACE,"DATA SIZE OF PACKETS = %d",ilPacketSize); */

        ilPktCnt = (ilTotalSize - 1) / ilPacketSize + 1;
        /* dbg(TRACE,"PACKET COUNT = %d",ilPktCnt); */

        SetShortValue(&prlOutBcHd->tot_size, (short)ilTotalSize);

        ilPktNbr = 0;
        while (ilTotalSize > 0)
        {
          ilPktNbr++;
          /*
          dbg(TRACE," ");
          dbg(TRACE,"BC %d: PACKET %d/%d",(prgStHd->ActBcNum+1),
                  ilPktNbr,ilPktCnt);
          */
          if (ilPktNbr > 1)
          {
            /* Both old Versions of the Ufis.Dll */
            /* will not understand these packets */
            /* so the Clients can ignore them. */
            strcpy(prlOutCmdBlk->command,"???");
            strcpy(prlOutCmdBlk->obj_name,"IGNORE");
          } /* end if */
          memset(pclOutData, 0x00, ilPacketSize);
          if (ilTotalSize < ilPacketSize)
          {
            ilPacketSize = ilTotalSize;
          } /* end if */
          SetShortValue(&prlOutBcHd->cmd_size, (short)ilPacketSize);
          memcpy(pclOutData, pclRcvData, ilPacketSize);


          rc = SendBroadCast(sock, (char *)prgOutPacket, ilPktNbr, ilPktCnt);

          pclRcvData += ilPacketSize;
          ilTotalSize -= ilPacketSize;
        } /* end while */


      } /* end if send udp */
      else
      {
        if (ilUdpEventIsValid == FALSE)
        {
          dbg(TRACE,"UDP MESSAGE NOT SENT DUE TO TABLE FILTER <%s>",prlRcvCmdBlk->obj_name);
        }
        else
        {
          dbg(TRACE,"BROADCAST NOT SENT PER UDP.");
        }
      } /* end else */

    } /* end if */
    else
    {
      dbg(TRACE,"BROADCAST NOT SENT DUE TO TABLE FILTER <%s>",pclChkTbl);
    } /* end else */

  } /* end else */

  if ((strcmp (prlRcvCmdBlk->command, BROADCAST_TO_SEND) == 0) ||
      (ilSbcAnsw == TRUE))
  {
    if (que_out != mod_id)
    {
      (void) tools_send_sql(que_out,prlRcvCmdBlk->command," "," "," ","OK");
    }
  } /* end if */
  
  if (rc == RC_DATA_CUT)
  {
    /* Dont terminate the bchdl because a packet was too big */
    rc = RC_SUCCESS;
  } /* fi */

  return rc;
}  /* end handle_data */

/* ******************************************************************** */
/* The handle_reg_cmd routine						*/
/* ******************************************************************** */
static int handle_reg_cmd (char *pcpHexAddr, char *pcpQueId, char *pcpCdrPid, int ipCountIt)
{
  int rc = RC_SUCCESS;
  int ilUseDataGramms = RC_SUCCESS;
  char table_list[256];
  char buf[32];
  int ilQueId = 0;
  int i = 0;
  int j = -1;
  int k = -1;
  int n = 0;
  int ilFound = FALSE;
	unsigned long ulClientIp = 0l;

  sscanf (pcpHexAddr, "%x",&ulClientIp);
  strcpy (buf, pcpHexAddr);
  (void) iStrup(buf);

  dbg(TRACE,"---- GOT REGISTER CMD FOR TCP/IP ADR = <%s>",buf);

  ilQueId = atoi(pcpQueId);
  if (ilQueId > 0)
  {
    sprintf(buf,"%s.%s",pcpHexAddr,pcpQueId);
    dbg(TRACE,"---- USING NAMED QUEUE = <%s>",buf);
  }


  /*** 20041208 JIM: modifying PRF 6604: check for configuration also ***/
  memset(table_list,0x00,256);
  ilUseDataGramms=iGetConfigEntry(cfgfile,"MAIN",pcpHexAddr,CFG_STRING,table_list);
  if(ilUseDataGramms != RC_SUCCESS)
  {	/* Second try with upper case hex address */
    ilUseDataGramms=iGetConfigEntry(cfgfile,"MAIN",buf,CFG_STRING,table_list);
  } /* fi */

#if 0
  if(rc == RC_SUCCESS)
  {
#endif 
    /********************** JWE: 20041022 - START PRF 6604 **************************/
    /* Register only those clients for datagrams which are NOT in the same network  */
    /* Otherwise we would send out BC's to specific clients twice. One plain UDP-BC */
    /* and one datagram */
    /* 20041208 JIM: modifying PRF 6604: check for configuration also */

		if ( ((ntohl(ulClientIp) & igNetmask) == (ulgHostIp & igNetmask)) &&
         (ilUseDataGramms != RC_SUCCESS))
    {
      dbg(TRACE,"CLIENT IN SAME NET => COMPARED CLI.-NET:<%x> SRV.-NET:<%x>! DO --NOT-- REGISTER!", ntohl(ulClientIp) & igNetmask, ulgHostIp & igNetmask);
    }    
    else
    {
      if (ilUseDataGramms == RC_SUCCESS)
      {
        dbg(TRACE,"CLIENT CONFIGURED TO GET ADRESSED DATGRAMMS!");
      }
      else
      {
        dbg(TRACE,"CLIENT NOT IN SAME NET => COMPARED CLI.-NET:<%x> SRV.-NET:<%x>! DO REGISTER!", ntohl(ulClientIp) & igNetmask, ulgHostIp & igNetmask);
      }
			/********************** JWE: 20041022 - END *************************************/

			for (i=0; ((ilFound != TRUE) && (i < NO_OF_REMOTE_HOSTS)); i++)
			{
				if (remote_hosts[i][0] != EOS)
				{
					/* Already used client index */
					if (strcmp (remote_hosts[i], buf) == 0)
					{
						ilFound = TRUE;
						j = i;
					} /* end if */
					if (ilFound != TRUE) 
					{
						if ((rhosts_cnt[i] < 1) && (rhosts_spool[i].SpoolCount < 1))
						{
							/* keep the last */
							/* not busy index */
							/* for re-use */
							k = i;
						}
					}
					
				} /* end if */
				else
				{
					if (j < 0)
					{
						/* keep the first unused index */
						j = i;
					} /* end if */
				} /* end else */
			}/* end for */

			if (j < 0)
			{
				j = k;
			}
			if (j >= 0)
			{
				strcpy (remote_hosts[j], buf);
				if ((ilFound != TRUE) || (ipCountIt == TRUE))
				{
					rhosts_cnt[j]++;
					if (rhosts_cnt[j] < 1)
					{
						rhosts_cnt[j] = 1;
					} /* end if */

					if (rhosts_cnt[j] == 1)
					{
						GetServerTimeStamp ("UTC", 1, 0, rhosts_spool[j].TimeStamp);
						rhosts_keep[j] = 0;
						rhosts_que[j] = 0;
						rhosts_spool[j].InternalClient = 0;
						RegisterNetwork(TRUE, remote_hosts[j]);
					} /* end if */
					strcpy(rhosts_cpid[j],pcpCdrPid);
					if (ilFound != TRUE)
					{
						rhosts_spool[j].TotalRbsCmds = 0;
						rhosts_spool[j].SpoolCount = 0;
						rhosts_spool[j].SpoolIdNbr = 0;
						rhosts_spool[j].TotalRbsCmds = 0;
						rhosts_spool[j].PeakRbsCmds = 0;
						rhosts_spool[j].TotalResent = 0;
						rhosts_spool[j].PeakResent = 0;
						rhosts_spool[j].TotalSpool = 0;
						rhosts_spool[j].PeakSpool = 0;
			for (n=0;n<60;n++)
			{
				rhosts_spool[j].BcSent.Min[n] = 0;
			}
			for (n=0;n<25;n++)
			{
				rhosts_spool[j].BcSent.Hour[n] = 0;
			}
			for (n=0;n<32;n++)
			{
				rhosts_spool[j].BcSent.Day[n] = 0;
			}
			for (n=0;n<13;n++)
			{
				rhosts_spool[j].BcSent.Month[n] = 0;
			}
					}
					dbg(TRACE,"WKS REGISTERED AS: IDX=%d IP=<%s> APPL CONNECTIONS=%d", j, remote_hosts[j], rhosts_cnt[j]);
					lgTotalHrgCnt++;
					if (ilQueId > 0)
					{
						rhosts_que[j] = ilQueId;
						dbg(TRACE,"ENTRY FLAGGED AS BC_OUT CONNECTOR");
					}
				} /* end if */
				else
				{
					dbg(TRACE,"MULTIPLE REG ENTRY IGNORED");
					dbg(TRACE,"IN REGLIST AS: IDX=%d IP=<%s> CONNECTIONS=%d", j, remote_hosts[j], rhosts_cnt[j]);
				} /* end else */
			}
			else
			{
				dbg (TRACE, "CAN'T REGISTER: REMOTE HOSTS LIST EXCEEDED");
				rc = RC_FAIL;
			} /* fi */
    }    
#if 0
  }
  else
  {
    dbg (TRACE, "CAN'T REGISTER: NOT IN THE CONFIGURED LIST");
    rc = RC_SUCCESS;
  } /* fi */
#endif 

  return rc;
} /* handle_reg_cmd */

/* ******************************************************************** */
/* ******************************************************************** */
static int GetClientIdx(char *pcpHexAddr)
{
  char buf[32];
  int i = 0;
  int j = -1;
  int ilFound = FALSE;
  strcpy (buf, pcpHexAddr);
  (void) iStrup(buf);
  for (i=0; ((ilFound != TRUE) && (i < NO_OF_REMOTE_HOSTS)); i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      if (strcmp (remote_hosts[i], buf) == 0)
      {
        ilFound = TRUE;
        j = i;
      } /* end if */
    } /* end if */
    else
    {
      if (j < 0)
      {
        /* keep the first free index */
        j = i;
      } /* end if */
    } /* end else */
  }/* end for */
  if (j >= 0)
  {
    strcpy (remote_hosts[j], buf);
    if ((rhosts_cnt[j] < 1) || (ilFound != TRUE))
    {
      dbg(TRACE,"INTERNAL CLIENT IDX=%d",j);
      rhosts_spool[j].InternalClient = 1;
      GetServerTimeStamp ("UTC", 1, 0, rhosts_spool[j].TimeStamp);
    }
  }
  else
  {
    dbg(TRACE,"TOO MANY CLIENTS");
  }
  return j;
} /* GetClientIdx */

/* ******************************************************************** */
/* The handle_unreg_cmd routine						*/
/* ******************************************************************** */
static int handle_unreg_cmd (char *pcpHexAddr, char *pcpQueId)
{
  int rc = RC_SUCCESS;
  int i = 0;
  int j = -1;
  int ilFound = FALSE;
  int ilQueId = 0;
  char buf[32];

  strcpy (buf, pcpHexAddr);
  (void) iStrup(buf);
  dbg(TRACE,"GOT UNREGIST CMD FOR TCP/IP ADR = <%s>",buf);

  ilQueId = atoi(pcpQueId);
  if (ilQueId > 0)
  {
    sprintf(buf,"%s.%s",pcpHexAddr,pcpQueId);
    dbg(TRACE,"---- USING NAMED QUEUE = <%s>",buf);
  }

  for (i=0; ((ilFound != TRUE) && (i < NO_OF_REMOTE_HOSTS)); i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      if (strcmp(remote_hosts[i], buf) == 0)
      {
        ilFound = TRUE;
        j = i;
      } /* end if */
    } /* end if */
  } /* end for */
  if (j >= 0)
  {
    dbg(TRACE,"WKS IN REGLIST AS: IDX=%d IP=<%s> APPL CONNECTIONS=%d", j, remote_hosts[j], rhosts_cnt[j]);
    lgTotalHurCnt++;
    if ((rhosts_cnt[j] == 1) && (rhosts_keep[j] == 1))
    {
      dbg(TRACE,"WON'T CLOSE PERMANENT CONNECTION");
    } /* end if */
    else
    {
      if (rhosts_cnt[j] > 0)
      {
        rhosts_cnt[j]--;
      }
      if (ilQueId >0)
      {
        rhosts_que[j] = 0;
        if (rhosts_key[j].AllocatedSize > 0)
        {
          rhosts_key[j].Value[0] = 0x00;
        }
        rhosts_key[j].UsedLen = 0;
        dbg(TRACE,"BC_OUT CONNECTOR: ENTRY DISABLED");
      }
      if (rhosts_cnt[j] <= 0)
      {
        rhosts_cnt[j] = 0;
        /* Keep Client Address !! */
        /* BCs might be spooled */
        /* remote_hosts[j][0] = EOS; */
        rhosts_que[j] = 0;
        if (rhosts_key[j].AllocatedSize > 0)
        {
          rhosts_key[j].Value[0] = 0x00;
        }
        rhosts_key[j].UsedLen = 0;
        GetServerTimeStamp ("UTC", 1, 0, rhosts_spool[j].TimeStamp);
        rhosts_spool[j].SpoolIdNbr++;
        dbg(TRACE,"ALL APPL CONNECTIONS OF WKS CLOSED");
        RegisterNetwork(FALSE, remote_hosts[j]);
      } /* fi */
      else
      {
        dbg(TRACE,"APPL CONNECTION CLOSED");
      } /* end else */
    } /* end else */
  }
  else
  {
    dbg (TRACE, "REMOTE HOST NOT FOUND");
  } /* fi */

  return rc;
} /* handle_unreg_cmd */

/* ******************************************************************** */
/* The HandleKeepAlive routine						*/
/* ******************************************************************** */
static int HandleKeepAlive (char *pcpHexAddr, int ipKeepVal)
{
  int rc = RC_SUCCESS;
  int i = 0;
	int j = -1;
	int ilFound = FALSE;
  char buf[16];
  strcpy (buf, pcpHexAddr);
  (void) iStrup(buf);

	if (ipKeepVal == 0)
	{
    dbg(TRACE,"GOT CLOSE CMD FOR TCP/IP ADR = <%s>",buf);
	} /* end if */
	else
	{
    dbg(TRACE,"GOT KEEP CMD FOR TCP/IP ADR = <%s>",buf);
	} /* end else */

  for (i=0; ((ilFound != TRUE) && (i < NO_OF_REMOTE_HOSTS)); i++)
	{
	  if (remote_hosts[i][0] != EOS)
		{
	    if (strcmp(remote_hosts[i], buf) == 0)
			{
				ilFound = TRUE;
				j = i;
			} /* end if */
		} /* end if */
  } /* end for */
  if (j >= 0)
  {
    dbg(TRACE,"WKS IN REGLIST AS: IDX=%d IP=<%s> CONNECTIONS=%d", 
               j, remote_hosts[j], rhosts_cnt[j]);
    rhosts_keep[j] = ipKeepVal;
	  if (ipKeepVal == 0)
	  {
      rhosts_cnt[j]--;
		  dbg(TRACE,"WKS PERMANENT CONNECTION CLOSED");
	  } /* end if */
	  else
	  {
		  dbg(TRACE,"WKS PERMANENT CONNECTION ESTABLISHED");
	  } /* end else */
  }
  else
  {
    dbg (TRACE, "NOTHING TO DO: CLIENT IS IN MY NETWORK");
  } /* fi */

  return rc;
} /* end HandleKeepAlive */

/* ******************************************************************** */
/* The handle_bc_req routine						*/
/* ******************************************************************** */
static int handle_bc_req (CMDBLK *cmdblk)
{
  int rc = RC_SUCCESS;
  int bc_num=0;			/* No of requested bc */
  int bc_num_first=0;		/* bc_num of oldes packet in buffer */
  int sgs_i=0;			/* Where is the packet in the shared mem ? */
  long llRecPos = 0;
  EVENT	*out_event = NULL;	/* Pointer to new event struct */
  char *Pact=NULL;		/* Actual Pointer */
  char buf[PACKET_LEN];
  
  BC_HEAD *Pbc_head = NULL;	/* Bc_head of found packet */

  Pact = cmdblk->data + strlen(cmdblk->data) + 1; /* selection */
  Pact = Pact + strlen(Pact) + 1; /* fields */

  bc_num = atoi(Pact);
  dbg(DEBUG,"handle_bc_req: bc_num is %d", bc_num); 

  /* Inits */
  out_event = (EVENT *) req_buf;	/* Static buffer */
  out_event->data_length =  (short) PACKETDATALEN;
  out_event->command = EVENT_DATA;
  out_event->data_offset = sizeof(EVENT);
  out_event->originator = mod_id;

  rc = RC_NOT_FOUND;

  if ((bc_num > 0) && (bc_num <= MAX_BC_NUM))
  {
    llRecPos = (bc_num * PACKET_LEN);
    fseek(pfgBcStack, llRecPos, SEEK_SET);
    fread((void *)prgReadBcPkt,PACKET_LEN,1,pfgBcStack);
    if (debug_level == DEBUG)
    {
      snap ((char *)prgReadBcPkt,PACKET_LEN,outp);
    } /* end if */
    Pbc_head = (BC_HEAD *)prgReadBcPkt->data;
    SetShortValue(&Pbc_head->bc_num, Pbc_head->bc_num);
    SetShortValue(&Pbc_head->tot_buf, Pbc_head->tot_buf);
    SetShortValue(&Pbc_head->act_buf, Pbc_head->act_buf);
    SetShortValue(&Pbc_head->rc, Pbc_head->rc);
    SetShortValue(&Pbc_head->tot_size, Pbc_head->tot_size);
    SetShortValue(&Pbc_head->cmd_size, Pbc_head->cmd_size);
    SetShortValue(&Pbc_head->data_size, Pbc_head->data_size);
    memcpy(((char *)out_event) + sizeof(EVENT), (char *)Pbc_head,
           PACKETDATALEN);
    rc = RC_SUCCESS;
  } /* end if */
  

  if (rc != RC_SUCCESS)
  {	/* Create error message */
    memset (((char *)out_event) + sizeof(EVENT), 0, PACKETDATALEN);
    Pbc_head = (BC_HEAD *) (((char *)out_event) + sizeof(EVENT));
    Pbc_head->rc = RC_FAIL;
    strcpy (Pbc_head->data, BC_NOT_FOUND_MSG);
    rc = RC_SUCCESS;
  }
  
  if (rc == RC_SUCCESS)
  {
    rc = que(QUE_PUT,que_out,mod_id,PRIORITY_3,sizeof(EVENT)+PACKETDATALEN,
	     (char *)out_event);
  }

  dbg(DEBUG,"handle_bc_req: end rc=%d", rc); 
  return rc;
} /* end handle_bc_req */

/* ******************************************************************** */
/* The HandleRbsSpoolOrSend routine						*/
/* ******************************************************************** */
static int HandleRbsSpoolOrSend (int ipSpoolCnt, char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int rc = RC_SUCCESS;
  int ilIdx = -1;
  int ilLen = 0;
  int ilReqCnt = 0;
  int ilReqNbr = 0;
  int ilDoIt = TRUE;
  int ilSpoolIt = FALSE;
  int ilSendIt = FALSE;
  int ilSpoolRbs = FALSE;
  int ilRbsGroup = 0;
  long llSent = 0;
  long llSpooled = 0;
  short slReqNum = 0;
  long llRecPos = 0;
  long llIdNbr = 0;
  long llSecNow = 0;
  char pclIdxVal[8];
  char pclNbrVal[8];
  char pclSpoolInfo[64];
  char pclSpoolTime[16];
  char pclSpoolType[8];
  char *pclBgnPtr = NULL;
  char *pclEndPtr = NULL;

  ilDoIt = TRUE;
  ilSpoolRbs = FALSE;
  ilSpoolIt = FALSE;
  ilSendIt = TRUE;
  strcpy(pclIdxVal,"-1");
  strcpy(pclNbrVal,"-1");
  
  ilReqCnt = field_count(pcpData);
  ilIdx = GetClientIdx(pcpSelKey);
  llSecNow = time(0L);
  if (ilIdx >= 0)
  {
    rhosts_spool[ilIdx].TotalRbsCmds++;
    if ((rhosts_spool[ilIdx].SpoolCount > 0) || ((ipSpoolCnt > 0) && (ilReqCnt > ipSpoolCnt)))
    {
      ilSpoolRbs = TRUE;
      ilSendIt = FALSE;
      ilSpoolIt = TRUE;
      sprintf(pclIdxVal,"%d",ilIdx);
      rhosts_spool[ilIdx].SpoolIdNbr++;
      sprintf(pclNbrVal,"%d",rhosts_spool[ilIdx].SpoolIdNbr);
      strcpy(pclSpoolTime,rhosts_spool[ilIdx].TimeStamp);
      sprintf(pclSpoolInfo,"%s,%s,%s,R,%d,%d",
              pclIdxVal,pclNbrVal,pclSpoolTime,llSecNow,ilReqCnt);
      dbg(TRACE,"SPOOLING RBS OF %d BC TO <%s> ",ilReqCnt,pcpSelKey);
    }
    else
    {
      dbg(TRACE,"RBS RECEIVED FOR %d BCs TO <%s> ",ilReqCnt,pcpSelKey);
    }
    if (rhosts_spool[ilIdx].PendingRbs > 0)
    {
      rhosts_spool[ilIdx].PendingRbs--;
    }
    if (rhosts_spool[ilIdx].PendingRbs > 0)
    {
      ilDoIt = FALSE;
      dbg(TRACE,"MULTIPLE RBS IGNORED (STILL %d ON QUE)",rhosts_spool[ilIdx].PendingRbs);
    }
  }
  else
  {
    dbg(TRACE,"RBS RECEIVED FOR %d BCs TO <%s> ",ilReqCnt,pcpSelKey);
  }

  if (ilDoIt == TRUE)
  {  
    pclBgnPtr = pcpData;
    do
    {
      pclEndPtr = strstr(pclBgnPtr,",");
      if ((ilSpoolIt == TRUE) && (ilSendIt == FALSE) && (igSpoolRbsGroup > 1))
      {
        ilRbsGroup = igSpoolRbsGroup - 1;
        while ((pclEndPtr != NULL) && (ilRbsGroup > 0))
        {
          ilRbsGroup--;
          pclEndPtr++;
          pclEndPtr = strstr(pclEndPtr,",");
        }
      }
      if (pclEndPtr != NULL)
      {
        *pclEndPtr = 0x00;
      }
      if (*pclBgnPtr != '\0')
      {
        if (ilSpoolIt == TRUE)
        {
          (void) tools_send_info_flag(mod_id,0, "", "", "", "", "",
           "","","{RBS}","SPOOLER",pcpSelKey,pclSpoolInfo,pclBgnPtr,5);
          llSpooled++;
        } /* end if */
        if (ilSendIt == TRUE)
        {
          slReqNum = atoi(pclBgnPtr);
          if ((slReqNum > 0) && (slReqNum <= MAX_BC_NUM))
          {
            llRecPos = (slReqNum * PACKET_LEN);
            fseek(pfgBcStack, llRecPos, SEEK_SET);
            fread((void *)prgReadBcPkt,PACKET_LEN,1,pfgBcStack);
            SetShortValue(&prgReadBcPkt->command,PACKET_RESEND);
            rc = tcp_send_datagram (sock, pcpSelKey, NULL, pcgPort2,
                                 (char *)prgReadBcPkt, PACKET_LEN);
            llSent++;
          } /* end if */
        } /* end if */
      } /* end if */
      if (pclEndPtr != NULL)
      {
        pclBgnPtr = pclEndPtr+1;
      }
    } while (pclEndPtr != NULL); /* end do */
    lgTotalResent += llSent;
    lgTotalSpooled += llSpooled;
    if (ilSpoolRbs == TRUE)
    {
      dbg(TRACE,"RBS SPOOLED %d BCs TO <%s> ",ilReqCnt,pcpSelKey);
    }
    else
    {
      dbg(TRACE,"RBS RESENT %d BCs TO <%s> ",ilReqCnt,pcpSelKey);
    }
    if (ilIdx >= 0)
    {
      rhosts_spool[ilIdx].SpoolCount += llSpooled;
      rhosts_spool[ilIdx].TotalSpool += llSpooled;
      rhosts_spool[ilIdx].TotalResent += llSent;
      rhosts_spool[ilIdx].BcSent.Min[igRptMin] += llSent;
      rhosts_spool[ilIdx].BcSent.Hour[igRptHour] += llSent;
      rhosts_spool[ilIdx].BcSent.Day[igRptDay] += llSent;
      rhosts_spool[ilIdx].BcSent.Month[igRptMonth] += llSent;
      if (llSpooled > 0)
      {
        sprintf(pclSpoolInfo,"%s,%s,%s,L,%d,%d",
                pclIdxVal,pclNbrVal,pclSpoolTime,llSecNow,ilReqCnt);
        (void) tools_send_info_flag(mod_id,0, "", "", "", "", "",
         "","","{RBS}","SPOOLER",pcpSelKey,pclSpoolInfo,"0",5);
      }
    }
  }
  if (igMoveRbsToP4 != TRUE)
  {
    rc = tools_send_sql(que_out,"RBS"," "," "," ","OK");
  }
  return RC_SUCCESS;
} /* end HandleRbsSpoolOrSend */


/* ******************************************************************** */
/* The HandleSpooledRbs routine						*/
/* ******************************************************************** */
static int HandleSpooledRbs (char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int rc = RC_SUCCESS;
  int ilIdx = -1;
  int ilLen = 0;
  int ilReqCnt = 0;
  int ilReqNbr = 0;
  int ilDoIt = TRUE;
  int ilSpoolIt = FALSE;
  int ilSendIt = FALSE;
  long llSent = 0;
  long llSpooled = 0;
  short slReqNum = 0;
  long llRecPos = 0;
  long llIdNbr = 0;
  char pclIdxVal[8];
  char pclNbrVal[8];
  char pclSpoolInfo[64];
  char pclSpoolTime[16];
  char pclSpoolType[8];
  char *pclBgnPtr = NULL;
  char *pclEndPtr = NULL;


  get_real_item(pclIdxVal,pcpFields,1);
  get_real_item(pclNbrVal,pcpFields,2);
  get_real_item(pclSpoolTime,pcpFields,3);
  get_real_item(pclSpoolType,pcpFields,4);
  ilIdx = atoi(pclIdxVal);
  llIdNbr = atol(pclNbrVal);

  ilDoIt = TRUE;
  ilSendIt = TRUE;
  ilSpoolIt = FALSE;
  if (ilIdx >= 0)
  {
    if (llIdNbr != rhosts_spool[ilIdx].SpoolIdNbr)
    {
      ilSendIt = FALSE;
      ilDoIt = FALSE;
      if ((pclSpoolType[0] == 'B') && (rhosts_cnt[ilIdx] > 0))
      {
        ilSpoolIt = TRUE;
        ilDoIt = TRUE;
        sprintf(pclSpoolInfo,"%d,%d,%s,B",ilIdx,
                rhosts_spool[ilIdx].SpoolIdNbr,
                rhosts_spool[ilIdx].TimeStamp);
      }
    }
    else
    {
      if (pclSpoolType[0] == 'L')
      {
        ilDoIt = FALSE;
        rhosts_spool[ilIdx].SpoolCount = 0;
        lgSpooledCount--;
      }
    }
  }

  if (ilDoIt == TRUE)
  {  
    ilReqCnt = 0;
    pclBgnPtr = pcpData;
    do
    {
      pclEndPtr = strstr(pclBgnPtr,",");
      if (pclEndPtr != NULL)
      {
        *pclEndPtr = 0x00;
      }
      if (*pclBgnPtr != '\0')
      {
        slReqNum = atoi(pclBgnPtr);
        if ((slReqNum > 0) && (slReqNum <= MAX_BC_NUM))
        {
          if (ilSpoolIt == TRUE)
          {
            (void) tools_send_info_flag(mod_id,0, "", "", "", "", "",
             "","","{RBS}","SPOOLER",pcpSelKey,pclSpoolInfo,pclBgnPtr,5);
            llSpooled++;
          } /* end if */
          if (ilSendIt == TRUE)
          {
            llRecPos = (slReqNum * PACKET_LEN);
            fseek(pfgBcStack, llRecPos, SEEK_SET);
            fread((void *)prgReadBcPkt,PACKET_LEN,1,pfgBcStack);
            SetShortValue(&prgReadBcPkt->command,PACKET_RESEND);
            rc = tcp_send_datagram (sock, pcpSelKey, NULL, pcgPort2,
                                 (char *)prgReadBcPkt, PACKET_LEN);
            llSent++;
          } /* end if */
          ilReqCnt++;
        } /* end if */
      } /* end if */
      if (pclEndPtr != NULL)
      {
        pclBgnPtr = pclEndPtr+1;
      }
    } while (pclEndPtr != NULL); /* end do */

    lgTotalResent += llSent;
    lgTotalSpooled += llSpooled;
  }
  if (ilIdx >= 0)
  {
    rhosts_spool[ilIdx].SpoolCount += llSpooled;
    rhosts_spool[ilIdx].TotalSpool += llSpooled;
    rhosts_spool[ilIdx].TotalResent += llSent;
    rhosts_spool[ilIdx].SpoolCount--;
    if (rhosts_spool[ilIdx].SpoolCount < 0)
    {
      rhosts_spool[ilIdx].SpoolCount = 0;
    }
    rhosts_spool[ilIdx].BcSent.Min[igRptMin] += llSent;
    rhosts_spool[ilIdx].BcSent.Hour[igRptHour] += llSent;
    rhosts_spool[ilIdx].BcSent.Day[igRptDay] += llSent;
    rhosts_spool[ilIdx].BcSent.Month[igRptMonth] += llSent;
  }
  lgResentCount += llSent;
  lgSpooledCount++;
  return RC_SUCCESS;
} /* end HandleSpooledRbs */


/* -----------------------------------------------------------------
//
// Function: BcBufAdd
//
// Purpose : Adds an entry to the broadcast buffer
//
// Params  : entry
//
// Returns : RC_SUCCESS
//
// Comments: Got from the ufis.dll, but changed a lot ...
//           
// -----------------------------------------------------------------
*/

static int BcBufAdd(short spBcNum, char *pcpOutBuf)
{
  int ilRC=RC_SUCCESS;
  long llRecPos = 0;
  llRecPos = (long) (spBcNum * PACKET_LEN);
  /* dbg(TRACE,"STORE BC %d AT %d",spBcNum,llRecPos); */
  fseek(pfgBcStack, llRecPos, SEEK_SET);
  fwrite((void *)pcpOutBuf,PACKET_LEN,1,pfgBcStack);
  fflush(pfgBcStack);
  if (debug_level == DEBUG)
  {
    snap (pcpOutBuf,PACKET_LEN,outp);
  } /* end if */
  return ilRC;
} /* end BcBufAdd */                        



/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */


/* ******************************************************************** */
/* The handle signals routine						*/
/* ******************************************************************** */

static void handle_sig(int sig)
{
	int	rc = 0;			/* Return code */
	if (sig == SIGCHLD) {
		dbg(TRACE,"BCHDL: received signal %d. Call WAITPID.",sig);
		waitpid(-1,(int *)NULL,WNOHANG);
	} /* end if */

	signal(sig,handle_sig);
	dbg(TRACE,"BCHDL: received signal %d. Reset signal. ",sig);
	exit(0);
		
	return ;
	
} /* end of handle_sig */


/* ******************************************************************** */
/* The termination routine						*/
/* ******************************************************************** */

static void terminate(int errcode)
{
  SaveUdpAdrFile(errcode);
  if(prgOutPacket != NULL)
  {
    free(prgOutPacket);
  } /* end if */
  if(pfgBcStack != NULL)
  {
    fclose(pfgBcStack);
  } /* end if */
  if (errcode == 10)
  {
    errcode = 0;
  } /* end if */
  /* PrintStatisticToFile(-1,TRUE); */
  PrintHourStatistic(TRUE);
  PrintDailyStatistic(TRUE);
  PrintMonthStatistic(TRUE);
  PrintYearStatistic(TRUE);
  dbg(TRACE,"AND NOW: EXIT");
  exit(errcode);
} /* end of terminate */


/* ******************************************************************** */
/* broadcast a logoff command on SHUTDOWN, HSB_DOWN and HSB_ACT_TO_SBY	*/
/* ******************************************************************** */
static int bc_hsb_logoff(void)
{
	int	rc	= RC_SUCCESS;
	BC_HEAD	*bchd	= NULL;
	CMDBLK	*cmdblk	= NULL;

	dbg(TRACE,"bc_hsb_logoff: NOW BROADCASTING CLO COMMAND");
	
	memset(prgCloPacket,0x00,PACKET_LEN);

	prgCloPacket->command = PACKET_END;
	prgCloPacket->length = 0;

	bchd	= (BC_HEAD *) prgCloPacket->data;
	cmdblk	= (CMDBLK *) bchd->data;

        strncpy (bchd->orig_name, pcgHostName, 10);
	bchd->rc = RC_SUCCESS;
	SetShortValue(&bchd->tot_size,sizeof(BC_HEAD)+sizeof(CMDBLK));
	SetShortValue(&bchd->cmd_size,sizeof(CMDBLK));
	bchd->data_size=0;

	sprintf(cmdblk->command,"CLO");
	sprintf(cmdblk->obj_name,"CLOTAB");

        rc = SendInternalCommand(-1, "CLO", "CLOTAB", "", "", "");
	rc = SendBroadCast (sock, (char *) prgCloPacket,1,1);
  
	return(rc);

}/* end of bc_hsb_logoff */

/* ******************************************************************** */
/* ******************************************************************** */
static void SetShortValue(short *pspDest, short spValue)
{
#if defined(_HPUX_SOURCE) || defined(_SNI) || defined(_SOLARIS)
  *pspDest = ccs_htons(spValue);
#else
  *pspDest = spValue;
#endif
  return;
} /* end SetShortValue */

/* ******************************************************************** */
/* ******************************************************************** */
static void SetLongValue(long *plpDest, long lpValue)
{
#if defined(_HPUX_SOURCE) || defined(_SNI) || defined(_SOLARIS)
  *plpDest = ccs_htonl(lpValue);
#else
  *plpDest = lpValue;
#endif
  return;
} /* end SetLongValue */


/* ******************************************************************** */
/* ******************************************************************** */
static short GetNextBcNumber(void)
{
  int ilGetRc = RC_SUCCESS;
  char	pclTmpBuf[128];
  prgStHd->ActBcNum++;
  if (prgStHd->ActBcNum > MAX_BC_NUM)	/* MAXNUM reached */
  {				/* one time arround 	*/
    prgStHd->ActBcNum = 1;		/* start with 1 again 	*/
  } /* fi */

  fseek(pfgBcStack, 0, SEEK_SET);
  fwrite((void *)prgStHd,PACKET_LEN,1,pfgBcStack);
  fflush(pfgBcStack);

  if ((prgStHd->ActBcNum % 10000) == 0)
  {
    igSwitchLog = TRUE;
#ifndef _SNI
    /***********************
    rc = send_error_msg (BCHDL_STAT, 0, prgStHd->ActBcNum, NULL);
    29.09.97 changed for new message handler...
    ***********************/
    memset((void*)pclTmpBuf, 0x00, 128);
    sprintf(pclTmpBuf, "%d", prgStHd->ActBcNum);	
    ilGetRc = send_message(IPRIO_DB, BCHDL_STAT, 0, 0, pclTmpBuf);
    dbg(DEBUG,"Broadcast Handler: sending msg rc = %d ",ilGetRc);
#endif
  } /* fi */

  return prgStHd->ActBcNum;
} /* end GetNextBcNumber */ 

/* ******************************************************************** */
/* ******************************************************************** */
static int TestBcForLoose(short spBcNum, short spPart)
{
  int ilRC = RC_SUCCESS;
	int ilGetRc = RC_SUCCESS;
	char pclCmpStrg[1024];
	char pclChkStrg[64];

if (ilRC == RC_SUCCESS)
{
  ilGetRc = iGetConfigRow(cfgfile,"TEST_SECTION",
                         "LOOSE_PART",CFG_STRING,pclCmpStrg);
  if (ilGetRc == RC_SUCCESS)
  {
		sprintf(pclChkStrg,",%d,",spPart);
		if (strstr(pclCmpStrg,pclChkStrg) != NULL)
		{
			dbg(TRACE,"BROADCAST NOT SENT DUE TO CONFIGURATION:");
      dbg(TRACE,"LOOSE_PART <%s> IN <%s>",pclChkStrg,pclCmpStrg);
			ilRC = RC_FAIL;
		} /* end if */
  } /* end if */
} /* end if */

if (ilRC == RC_SUCCESS)
{
  ilGetRc = iGetConfigRow(cfgfile,"TEST_SECTION",
													"LOOSE_NMBR",CFG_STRING,pclCmpStrg);
  if (ilGetRc == RC_SUCCESS)
  {
		sprintf(pclChkStrg,",%d,",spBcNum);
		if (strstr(pclCmpStrg,pclChkStrg) != NULL)
		{
			dbg(TRACE,"BROADCAST NOT SENT DUE TO CONFIGURATION:");
      dbg(TRACE,"LOOSE_NMBR <%s> IN <%s>",pclChkStrg,pclCmpStrg);
			ilRC = RC_FAIL;
		} /* end if */
  } /* end if */
} /* end if */

if (ilRC == RC_SUCCESS)
{
  ilGetRc = iGetConfigRow(cfgfile,"TEST_SECTION",
													"LOOSE_EACH",CFG_STRING,pclCmpStrg);
  if (ilGetRc == RC_SUCCESS)
  {
		spBcNum = spBcNum % 100;
		sprintf(pclChkStrg,",%d,",spBcNum);
		if (strstr(pclCmpStrg,pclChkStrg) != NULL)
		{
			dbg(TRACE,"BROADCAST NOT SENT DUE TO CONFIGURATION:");
      dbg(TRACE,"LOOSE_EACH <%s> IN <%s>",pclChkStrg,pclCmpStrg);
			ilRC = RC_FAIL;
		} /* end if */
  } /* end if */
} /* end if */

	return ilRC;
} /* end TestBcForLoose */

/* ******************************************************************** */
/* ******************************************************************** */
static void TestByteOrder(void)
{
  short slTest = 65;
  char pclStrg[16];
  memcpy(pclStrg,&slTest,sizeof(short));
  snap (pclStrg,sizeof(short),outp);
  if (pclStrg[0] == 65)
  {
    dbg(TRACE,"SYSTEM USES INTEL BYTE ORDER");
  } /* end if */
  else
  {
    dbg(TRACE,"SYSTEM DOESN'T USE INTEL BYTE ORDER");
  } /* end else */

  
  return;
}

/*******************************************************/
/*******************************************************/
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  char pclTmp[32];
  ReadCfg(cfgfile,pclTmp,"SYSTEM",pcpLine,pcpDefault);  
  if(strcmp(pclTmp,"TRACE")==0)
  {
    *ipModeLevel = TRACE;
  } else if(strcmp(pclTmp,"DEBUG")==0) {
    *ipModeLevel = DEBUG;
  } /* end else if */
  else
  {
    *ipModeLevel = 0;
  } /* end else */
  return;
} /* end GetLogFileMode */

/* ******************************************************************** */
/* ******************************************************************** */
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection,
        char *pcpEntry, char *pcpDefVar)
{
        int ilRc = RC_SUCCESS;                                               
  pcpVar[0] = 0x00;
        ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpEntry,1,pcpVar);        
        if( ilRc != RC_SUCCESS || strlen(pcpVar) <= 0 ) {                    
                strcpy(pcpVar,pcpDefVar); /* copy the default value */       
                ilRc = RC_SUCCESS;                                              
        }                                                                    
                                                                             
        return ilRc;                                                         
                                                                             
} /* end ReadCfg() */

/* ******************************************************************** */
/* ******************************************************************** */
static void ReadSendAlways(void)
{
  int ilGetRc = RC_SUCCESS;
  int ilAdrCnt = 0;
  int ilCurAdr = 0;
  int ilAdrLen = 0;
  char pclSndUdpAdr[1024];
  char pclCurUdpAdr[16];

  dbg(TRACE,"init_bchdl: ------------------------");
  dbg(TRACE,"init_bchdl: READ SEND ALWAYS UDP ADR");
  pclSndUdpAdr[0] = 0x00;
  ilGetRc = iGetConfigRow(cfgfile,"GLOBAL_DEFINES", "AUTOSEND_UDP",CFG_STRING,pclSndUdpAdr);
  if (ilGetRc == RC_SUCCESS)
  {
    dbg(TRACE,"GOT ADDRESS <%s>",pclSndUdpAdr);
    ilAdrCnt = field_count(pclSndUdpAdr);
    for (ilCurAdr=1; ilCurAdr<=ilAdrCnt; ilCurAdr++)
    {
      ilAdrLen = get_real_item(pclCurUdpAdr,pclSndUdpAdr,ilCurAdr);
      if (ilAdrLen == 8)
      {
        ilGetRc = handle_reg_cmd(pclCurUdpAdr, "0", "UDP", FALSE);
      } /* end if */
    } /* end for */
  } /* end if */
  else
  {
    dbg(TRACE,"init_bchdl: NO AUTOSEND ADDRESSES DEFINED");
  } /* end else */

return;
} /* end ReadSendAlways */

/* ******************************************************************** */
/* ******************************************************************** */
static void ReadUdpAdrFile(void)
{
	FILE *pflSave = NULL;
	char *pclBuff = NULL;
	char pclLine[2002];
	char pclTmpVal[2002];
	char pclTcpAdr[64];
	int i = 0;
	int ilLen = 0;
	dbg(TRACE,"init_bchdl: ------------------------");
	dbg(TRACE,"init_bchdl: READ <%s>",pcgUdpAdrFile);
	pflSave = fopen(pcgUdpAdrFile,"r");
	if (pflSave != NULL)
	{
		i = -1;
		while ((pclBuff = fgets(pclLine,2000,pflSave)) != NULL)
		{
			ilLen = strlen(pclLine) - 1;
			if (ilLen > 0)
			{
			  if (pclLine[ilLen] == '\n')
			  {
				  pclLine[ilLen] = 0x00;
			  } /* end if */
			  ilLen = get_real_item(pclTcpAdr, pclLine, 1);
			  if (ilLen >= 8)
			  {
			     ilLen = get_real_item(pclTmpVal, pclLine, 6);
			     if (ilLen > 0)
			     {
				/* New File Version */
				  i = atoi(pclTmpVal);
				  dbg(TRACE,"init_bchdl: RESTORE (IDX=%d) <%s> ", i,pclLine);
				  strcpy(remote_hosts[i],pclTcpAdr);
			    (void) get_real_item(pclTmpVal, pclLine, 2);
				  rhosts_cnt[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 3);
				  rhosts_keep[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 4);
				  rhosts_que[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 5);
                            ilLen = strlen(pclTmpVal) + 1;
                            if (ilLen > rhosts_key[i].AllocatedSize)
                            {
                              rhosts_key[i].Value = (char*)realloc((char*)rhosts_key[i].Value, ilLen);
                              rhosts_key[i].AllocatedSize = ilLen;
                            }
                            strcpy(rhosts_key[i].Value,pclTmpVal);
                            rhosts_key[i].UsedLen = ilLen-1;
			    (void) get_real_item(rhosts_spool[i].TimeStamp, pclLine, 7);
			    (void) get_real_item(pclTmpVal, pclLine, 8);
                                   rhosts_spool[i].SpoolIdNbr = atol(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 9);
                                   rhosts_spool[i].SpoolCount = atol(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 10);
                                   rhosts_spool[i].PendingRbs = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 11);
                                   rhosts_spool[i].InternalClient = atoi(pclTmpVal);
				} /* end if */
				else
				{
				/* Old File Version */
				  i++;
				  dbg(TRACE,"RESTORE (I=%d) <%s> ", i,pclLine);
				  strcpy(remote_hosts[i],pclTcpAdr);
			    (void) get_real_item(pclTmpVal, pclLine, 2);
				  rhosts_cnt[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 3);
				  rhosts_keep[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 4);
				  rhosts_que[i] = atoi(pclTmpVal);
			    (void) get_real_item(pclTmpVal, pclLine, 5);
                            ilLen = strlen(pclTmpVal) + 1;
                            if (ilLen > rhosts_key[i].AllocatedSize)
                            {
                              rhosts_key[i].Value = (char*)realloc((char*)rhosts_key[i].Value, ilLen);
                              rhosts_key[i].AllocatedSize = ilLen;
                            }
                            strcpy(rhosts_key[i].Value,pclTmpVal);
                            rhosts_key[i].UsedLen = ilLen-1;
				} /* end else */
			
                            if (rhosts_spool[i].TimeStamp[0] == 0x00)
                            {
          		      GetServerTimeStamp ("UTC", 1, 0, rhosts_spool[i].TimeStamp);
                            }
			  } /* end if */
			} /* end if */
		} /* end while */
		fclose(pflSave);
		pflSave = NULL;
	} /* end if */
	else
	{
		dbg(TRACE,"CAN'T OPEN UDP_ADR_FILE");
	} /* end else */
	return;
} /* end ReadUdpAdrFile */

/* ******************************************************************** */
/* ******************************************************************** */
static void SaveUdpAdrFile(int ipErrCod)
{
  FILE *pflSave = NULL;
  int i = 0;
  int iCnt = 0;
  dbg(TRACE,"OPEN <%s>",pcgUdpAdrFile);
  pflSave = fopen(pcgUdpAdrFile,"w");
  if (pflSave != NULL)
  {
    if (ipErrCod == 500) /* Reset Command */
    {
      dbg(TRACE,"SAVE <%s> (RESET)",pcgUdpAdrFile);
      for (i=0; i < NO_OF_REMOTE_HOSTS; i++)
      {
        if (remote_hosts[i][0] != EOS)
        {
          /* New Version */
          fprintf(pflSave,"%s,%d,%d,%d,%s,%d,%s,%d,%d,%d,%d\n",
          remote_hosts[i],
          rhosts_cnt[i],
          rhosts_keep[i],
          rhosts_que[i],
          rhosts_key[i].Value,
          i,
          rhosts_spool[i].TimeStamp,
          rhosts_spool[i].SpoolIdNbr,
          rhosts_spool[i].SpoolCount,
          rhosts_spool[i].PendingRbs,
          rhosts_spool[i].InternalClient);

          /* Old Version */
/*
          fprintf(pflSave,"%s,%d,%d,%d,%s\n", 
          remote_hosts[i],rhosts_cnt[i],rhosts_keep[i],rhosts_que[i],rhosts_key[i].Value);
*/
          iCnt++;
        } /* end if */
      } /* end for */
      dbg(TRACE,"SAVED %d CONNECTIONS",iCnt);
    } /* end if */
    else
    {
      dbg(TRACE,"CLEAR <%s> (SHUTDOWN)",pcgUdpAdrFile);
    }
    fclose(pflSave);
    pflSave = NULL;
  } /* end if */
  else
  {
    dbg(TRACE,"CAN'T OPEN FILE <BcHdlUdpAdr.Dat> FOR WRITE!");
  } /* end else */
  return;
} /* end SaveUdpAdrFile */

/* ******************************************************************** */
/* ******************************************************************** */
static void CreateTblBcNum(char *pcpTwEnd, char *pcpTbl, char *pcpCmd,
			 char *pcpFields, char *pcpData, int ipValid)
{
  static char pclPrvTblNam[64];
  static int ilPrvFound = -1;
  char pclHopo[16];
  char pclTblExt[16];
  char pclModule[32];
  char pclUseTblNam[64];
	char pclResult[32];
	char pclCfgCode[64];
	char pclTmp[128];
	int ilCnt2 = 0;
	int ilCnt3 = 0;
	int ilItmNbr = 0;
	int ilFound = -1;
	int i = 0;

	(void) get_real_item(pclHopo, pcpTwEnd, 1);
	(void) get_real_item(pclTblExt, pcpTwEnd, 2);
        (void) get_real_item(pclModule, pcpTwEnd, 3);
	if (pclHopo[0] == '\0')
        {
	  strcpy(pclHopo,pcgSgsPrj);
	}

	sprintf(pclUseTblNam,"%s/%s",pclHopo,pcpTbl);

	if ((ilPrvFound >= 0) && (strcmp(pclPrvTblNam,pclUseTblNam) == 0))
        {
          ilFound = ilPrvFound;
        }
        else
        {
	  ilFound = -1;
	  for (igTblNumIdx=1; ((igTblNumIdx<=igTblNumCnt) && (ilFound < 1));
			 igTblNumIdx++)
	  {
		if (strcmp(rgTblBcNum[igTblNumIdx].TableName,pclUseTblNam) == 0)
		{
			ilFound = igTblNumIdx;
		} /* end if */
	  } /* end for */
        }
	if (ilFound < 1)
	{
	  if (igTblNumCnt <= MAX_NO_OF_TABLES)
	  {
	    dbg(TRACE,"READ CONFIG OF TABLE <%s>",pclUseTblNam);
	    igTblNumCnt++;
		  ilFound = igTblNumCnt;
      strcpy(rgTblBcNum[ilFound].TableName,pclUseTblNam);
			sprintf(pclCfgCode,"%s_2",pclUseTblNam);
      ReadCfg(cfgfile,pclTmp,"TABLE_BC_NUM",pclCfgCode,"");
			dbg(TRACE,"BC NUM 2 <%s> <%s>",pclCfgCode,pclTmp);
			(void) get_real_item(rgTblBcNum[ilFound].FieldName2,pclTmp,1);
			(void) get_real_item(rgTblBcNum[ilFound].FieldValu2,pclTmp,2);
			sprintf(pclCfgCode,"%s_3",pclUseTblNam);
      ReadCfg(cfgfile,pclTmp,"TABLE_BC_NUM",pclCfgCode,"");
			dbg(TRACE,"BC NUM 3 <%s> <%s>",pclCfgCode,pclTmp);
			(void) get_real_item(rgTblBcNum[ilFound].FieldName3,pclTmp,1);
			(void) get_real_item(rgTblBcNum[ilFound].FieldValu3,pclTmp,2);
		  rgTblBcNum[ilFound].CurBcNum1 = -1;
		  rgTblBcNum[ilFound].CurBcNum2 = 0;
		  rgTblBcNum[ilFound].CurBcNum3 = 0;
		  rgTblBcNum[ilFound].TotalSent = 0;
		  rgTblBcNum[ilFound].PeakSent = 0;
		for (i=0;i<60;i++)
		{
		  rgTblBcNum[ilFound].BcSent.Min[i] = 0;
		  rgTblBcNum[ilFound].BcIgno.Min[i] = 0;
		}
		for (i=0;i<25;i++)
		{
		  rgTblBcNum[ilFound].BcSent.Hour[i] = 0;
		  rgTblBcNum[ilFound].BcIgno.Hour[i] = 0;
		}
		for (i=0;i<32;i++)
		{
		  rgTblBcNum[ilFound].BcSent.Day[i] = 0;
		  rgTblBcNum[ilFound].BcIgno.Day[i] = 0;
		}
		for (i=0;i<13;i++)
		{
		  rgTblBcNum[ilFound].BcSent.Month[i] = 0;
		  rgTblBcNum[ilFound].BcIgno.Month[i] = 0;
		}
	  } /* end if */
	} /* end if */

	if (ilFound > 0)
	{
		/* dbg(TRACE,"FOUND <%s> as (%d)",pclUseTblNam,ilFound); */
		/* ADJUST MAIN TBL_BC_NUM */
		rgTblBcNum[ilFound].CurBcNum1++;
		if (rgTblBcNum[ilFound].CurBcNum1 > 30000)
		{
		  rgTblBcNum[ilFound].CurBcNum1 = 1;
		} /* end if */

		/* ADJUST 2. TBL_BC_NUM */
		ilCnt2 = -rgTblBcNum[ilFound].CurBcNum2;
		if (rgTblBcNum[ilFound].FieldName2[0] != '\0')
		{
		  ilItmNbr = get_item_no(pcpFields,rgTblBcNum[ilFound].FieldName2,5) + 1;
		  if (ilItmNbr > 0)
		  {
			  (void) get_real_item(pclTmp,pcpData,ilItmNbr);
			  if (strcmp(pclTmp,rgTblBcNum[ilFound].FieldValu2) == 0)
			  {
		      rgTblBcNum[ilFound].CurBcNum2++;
		      if (rgTblBcNum[ilFound].CurBcNum2 > 30000)
		      {
		        rgTblBcNum[ilFound].CurBcNum2 = 1;
		      } /* end if */
		      ilCnt2 = rgTblBcNum[ilFound].CurBcNum2;
			  } /* end if */
		  } /* end if */
		} /* end if */
		/* ADJUST 3. TBL_BC_NUM */
		ilCnt3 = -rgTblBcNum[ilFound].CurBcNum3;
		if (rgTblBcNum[ilFound].FieldName3[0] != '\0')
		{
		  ilItmNbr = get_item_no(pcpFields,rgTblBcNum[ilFound].FieldName3,5) + 1;
		  if (ilItmNbr > 0)
		  {
			  (void) get_real_item(pclTmp,pcpData,ilItmNbr);
			  if (strcmp(pclTmp,rgTblBcNum[ilFound].FieldValu3) == 0)
			  {
		      rgTblBcNum[ilFound].CurBcNum3++;
		      if (rgTblBcNum[ilFound].CurBcNum3 > 30000)
		      {
		        rgTblBcNum[ilFound].CurBcNum3 = 1;
		      } /* end if */
		      ilCnt3 = rgTblBcNum[ilFound].CurBcNum3;
			  } /* end if */
		  } /* end if */
		} /* end if */
		sprintf(pclResult,"%d,%d,%d",
			rgTblBcNum[ilFound].CurBcNum1, ilCnt2, ilCnt3);
		if (ipValid == TRUE)
		{
			rgTblBcNum[ilFound].TotalSent++;
			rgTblBcNum[ilFound].BcSent.Min[igRptMin]++;
			rgTblBcNum[ilFound].BcSent.Hour[igRptHour]++;
			rgTblBcNum[ilFound].BcSent.Day[igRptDay]++;
			rgTblBcNum[ilFound].BcSent.Month[igRptMonth]++;
		}
		else
		{
			rgTblBcNum[ilFound].BcIgno.Min[igRptMin]++;
			rgTblBcNum[ilFound].BcIgno.Hour[igRptHour]++;
			rgTblBcNum[ilFound].BcIgno.Day[igRptDay]++;
			rgTblBcNum[ilFound].BcIgno.Month[igRptMonth]++;
		}
		ilPrvFound = ilFound;
                strcpy(pclPrvTblNam,pclUseTblNam);
	} /* end if */
	else
	{
		strcpy(pclResult,"-1,0,0");
	} /* end else */
        if (igSendModuleName == TRUE)
        {
          sprintf(pcpTwEnd,"%s,,%s",pclModule,pclResult);
        }
        else
        {
	  sprintf(pcpTwEnd,"%s,%s,%s",pclHopo,pclTblExt,pclResult);
        }
	dbg(DEBUG,"TBL_BC_NUM: <%s> TWE <%s>",pclUseTblNam,pcpTwEnd);
	return;
} /* end CreateTblBcNum */

/* ******************************************************************** */
/* ******************************************************************** */
static int StoreBcOutFilter (char *pcpHexAddr, char *pcpQueId, char *pcpFilter)
{
  int ilRC = RC_SUCCESS;
  int ilQueId = 0;
  int ilLen = 0;
  int i = 0;
  int j = -1;
  char buf[32];

  sprintf(buf,"%s.%s",pcpHexAddr,pcpQueId);
  (void) iStrup(buf);
  dbg(TRACE,"STORING BC FILTER FOR BC_OUT_CONNECTOR");
  dbg(TRACE,"IDENT <%s> QUEUE <%s>",buf,pcpQueId);
  dbg(TRACE,"FILTER <%s>",pcpFilter);
  ilQueId = atoi(pcpQueId);
  if (ilQueId > 0)
  {
    dbg(TRACE,"---- BC_OUT QUEUE_ID = <%s> (%d)", pcpQueId, ilQueId);
    if (strlen(buf) == 0)
    {
      sprintf(buf,"BCQ%s",pcpQueId);
      dbg(TRACE,"---- USING NAMED QUEUE = <%s>",buf);
    }
    for (i=0; ((j < 0) && (i < NO_OF_REMOTE_HOSTS)); i++)
    {
      if (remote_hosts[i][0] != EOS)
      {
        if (strcmp(remote_hosts[i], buf) == 0)
        {
          j = i;
          dbg(TRACE,"WKS IN REGLIST AS: IDX=%d IP=<%s> APPL CONNECTIONS=%d", j, remote_hosts[j], rhosts_cnt[j]);
          if (rhosts_que[j] != ilQueId)
          {
            dbg(TRACE,"QUEUE ID MISMATCH: %d <--> %d",ilQueId,rhosts_que[j]);
            dbg(TRACE,"FILTER IGNORED");
            j = -1;
          }
        } /* end if */
      } /* end if */
    } /* end for */
    if (j >= 0)
    {
      ilLen = strlen(pcpFilter) + 1;

      if (ilLen > rhosts_key[j].AllocatedSize)
      {
        if (rhosts_key[j].AllocatedSize <= 0)
        {
          rhosts_key[j].Value = (char *)malloc(ilLen);
          rhosts_key[j].AllocatedSize = ilLen;
          if (rhosts_key[j].Value == NULL)
          {
            dbg(TRACE,"CAN'T ALLOC %d BYTES!!!",ilLen);
            rhosts_key[j].AllocatedSize = 0;
          } /* end if */
        } /* end if */
        else
        {
          rhosts_key[j].Value = (char*)realloc((char*)rhosts_key[j].Value, ilLen);
          rhosts_key[j].AllocatedSize = ilLen;
          if (rhosts_key[j].Value == NULL)
          {
            dbg(TRACE,"CAN'T RE-ALLOC %d BYTES!!!",ilLen);
            rhosts_key[j].AllocatedSize = 0;
          } /* end if */
        } /* end else */
      } /* end if */
      if (ilLen <= rhosts_key[j].AllocatedSize)
      {
        strcpy(rhosts_key[j].Value, pcpFilter);
        rhosts_key[j].UsedLen = ilLen-1;
      }
    }
    else
    {
      dbg(TRACE, "REMOTE HOST NOT FOUND");
    }
  }
  return ilRC;
} /* end StoreBcOutFilter */

/* ******************************************************************** */
/* ******************************************************************** */
static int ResendBcEvents (char *pcpHexAddr, char *pcpQueId, char *pcpFromBcNum)
{
  int ilRC = RC_SUCCESS;
  dbg(TRACE,"------ UNDER CONSTRUCTION ------");
  dbg(TRACE,"SENDING BC EVENTS TO BC_OUT_CONNECTOR");
  dbg(TRACE,"IDENT <%s> QUEUE <%s>",pcpHexAddr,pcpQueId);
  dbg(TRACE,"BCNUM <%s>",pcpFromBcNum);
  dbg(TRACE,"--------------------------------");
  return ilRC;
} /* end ResendBcEvents */

/* ******************************************************************** */
/* ******************************************************************** */
static int SendInternalCommand(int ipHostIdx, char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRC = RC_SUCCESS;
  int n = 0;
  int i = 0;
  int j = 0;
  dbg(TRACE,"SENDING INTERNAL COMMAND TO BC_OUT_CONNECTOR");
  if (ipHostIdx >= 0)
  {
    n = ipHostIdx;
    j = ipHostIdx;
  }
  else
  {
    n = 0;
    j = NO_OF_REMOTE_HOSTS;
  }
  for (i=n; i<j; i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      if (rhosts_que[i] > 0)
      {

        ilRC = SendCedaEvent (rhosts_que[i],       /* adress to send the answer   */
                           0, /* Set to mod_id if < 1        */
                           "",     /* BC_HEAD.dest_name           */
                           "",     /* BC_HEAD.recv_name           */
                           "",     /* CMDBLK.tw_start             */
                           "",      /* CMDBLK.tw_end               */
                           pcpCmd,     /* CMDBLK.command              */
                           pcpTbl,     /* CMDBLK.obj_name             */
                           pcpSel,        /* Selection Block  (HexAddress) */
                           pcpFld,     /* Field Block                 */
                           pcpDat,    /* Data Block                  */
                           "",            /* Error description           */
                           1,             /* 0 or Priority               */
                           RC_SUCCESS);   /* BC_HEAD.rc: RC_SUCCESS or   */
                                          /*             RC_FAIL or      */
                                          /*             NETOUT_NO_ACK   */

        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"RESET BC_OUT_QUE <%s> (Q:%d) CLOSED",remote_hosts[i],rhosts_que[i]);
          rhosts_cnt[i]--;
          if (rhosts_cnt[i] <= 0)
          {
            rhosts_que[i] = -1;
            if (rhosts_key[i].AllocatedSize > 0)
            {
              rhosts_key[i].Value[0] = 0x00;
            }
            rhosts_key[i].UsedLen = 0;
            remote_hosts[i][0] = EOS;
            rhosts_cnt[i] = 0;
          }
        }
        else
        {
          dbg(TRACE,"SENT BC_EVENT TO <%s> (Q:%d)",remote_hosts[i],rhosts_que[i]);
        }
      }
    } /* fi */
  } /* for */
  return ilRC;
} /* end SendInternalCommand */

/* ******************************************************************** */
/* ******************************************************************** */
static void PrintStatisticToFile(long lpTimeDiff,int ipPrintAll)
{
  FILE *pflSave = NULL;
  char pclFileName[128];
  char pclTimeStamp[16];
  long llTimeDiff = 0;
  int ilTblNumIdx = 0;
  int i = 0;
if ((lgRefreshStatistic > 0)||(lpTimeDiff==-99))
{
  GetServerTimeStamp ("UTC", 1, 0, pclTimeStamp);
  sprintf (pclFileName, "%s/REP_BRC%d_%05d_%s.txt", cfgRptPath,mod_id,igMyPid,pclTimeStamp);
  pflSave = fopen(pclFileName,"w");
  if (pflSave != NULL)
  {
    llTimeDiff = lpTimeDiff;
    if (llTimeDiff < 0)
    {
      lgCurrTimeStamp = time(0L);
      llTimeDiff = (lgCurrTimeStamp - lgLastTimeStamp) / 60;
    }
    fprintf(pflSave,"= STATISTIC AT %s ===========\n",pclTimeStamp);
    fprintf(pflSave,"= TOTALS DURING THE LAST %d MINUTES \n",llTimeDiff);
    fprintf(pflSave,"= PEAKS SINCE STARTUP AT %s \n",pcgTimeStamp);
    fprintf(pflSave,"============ BC IN GENERAL ==============\n");
    fprintf(pflSave,"TOTAL BC SENT PORT2 = %d / PEAK = %d\n",lgTotalP2Sent,lgPeakP2Sent);
    fprintf(pflSave,"TOTAL DATAGRAM SENT = %d / PEAK = %d\n",lgTotalD2Sent,lgPeakD2Sent);
    fprintf(pflSave,"TOTAL RBS REQUESTED = %d / PEAK = %d\n",lgTotalRbsCmd,lgPeakRbsCmd);
    fprintf(pflSave,"TOTAL BC SPOOLED P5 = %d / PEAK = %d\n",lgTotalSpooled,lgPeakSpooled);
    fprintf(pflSave,"TOTAL BC SENT AGAIN = %d / PEAK = %d\n",lgTotalResent,lgPeakResent);
    fprintf(pflSave,"TOTAL REGISTER CLNT = %d / PEAK = %d\n",lgTotalHrgCnt,lgPeakHrgCnt);
    fprintf(pflSave,"TOTAL UNREGISTER CL = %d / PEAK = %d\n",lgTotalHurCnt,lgPeakHurCnt);
    fprintf(pflSave,"TOTAL CONNECTED WKS = %d / PEAK = %d\n",igTotalCliCnt,igPeakCliCnt);
    if (igTestP4TimeSpan == TRUE)
    {
    fprintf(pflSave,"TOTAL PRIO 4 WAITED = %d / PEAK = %d\n",lgTestP4TimeSum,lgTestP4TimePeak);
    }
    fprintf(pflSave,"============ BC PER TABLE ===============\n");
    for (ilTblNumIdx=1; ilTblNumIdx<=igTblNumCnt; ilTblNumIdx++)
    {
      fprintf(pflSave,"TABLE <%s> TOTAL = %d / PEAK = %d\n", 
                rgTblBcNum[ilTblNumIdx].TableName,
                rgTblBcNum[ilTblNumIdx].TotalSent,
                rgTblBcNum[ilTblNumIdx].PeakSent);
    } /* end for */
    fprintf(pflSave,"============ RBS REQUESTED ==============\n");
    for (i=0; i < NO_OF_REMOTE_HOSTS; i++)
    {
      if (remote_hosts[i][0] != EOS)
      {
        if ((rhosts_spool[i].PeakRbsCmds > 0) || (ipPrintAll == TRUE))
        {
          fprintf(pflSave,"%04d %d %s %d %s R=%d/%d Q=%d/%d S=%d/%d P=%d\n",
                     i,
                     rhosts_cnt[i],
                     remote_hosts[i],
                     rhosts_spool[i].InternalClient,
                     rhosts_spool[i].TimeStamp,
                     rhosts_spool[i].TotalRbsCmds,
                     rhosts_spool[i].PeakRbsCmds,
                     rhosts_spool[i].TotalSpool,
                     rhosts_spool[i].PeakSpool,
                     rhosts_spool[i].TotalResent,
                     rhosts_spool[i].PeakResent,
                     rhosts_spool[i].SpoolCount);
                      
        }
      }
    }
    fprintf(pflSave,"=========================================\n");
    fclose(pflSave);
    dbg(TRACE,"REPORT ISSUED: <%s>",pclFileName);
    dbg(TRACE,"============== START/END ================");
  }
}
  return;
}
  
/* ******************************************************************** */
/* ******************************************************************** */
static void ResetStatistic(void)
{
  int ilTblNumIdx = 0;
  int i = 0;
  lgTotalRbsCmd = 0;
  lgTotalResent = 0;
  lgTotalP2Sent = 0;
  lgTotalD2Sent = 0;
  lgTotalSpooled = 0;
  lgTotalHrgCnt = 0;
  lgTotalHurCnt = 0;
  igTotalCliCnt = 0;
  for (ilTblNumIdx=1; ilTblNumIdx<=igTblNumCnt; ilTblNumIdx++)
  {
    rgTblBcNum[ilTblNumIdx].TotalSent = 0;
  } /* end for */
  for (i=0; i < NO_OF_REMOTE_HOSTS; i++)
  {
    if (remote_hosts[i][0] != EOS)
    {
      rhosts_spool[i].TotalRbsCmds = 0;
      rhosts_spool[i].TotalResent = 0;
      rhosts_spool[i].TotalSpool = 0;
    }
  }
  lgTestP4TimeSum = 0;
  return;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int HandleShiftRbsToP4(char *pcpSelKey)
{
  int ilRc = RC_SUCCESS;
  int len = 0;
  int ilIdx = -1;
  len = sizeof(EVENT) + event->data_length;
  ilRc = que(QUE_PUT,mod_id,mod_id,4,len,(char *)event);
  if (ilRc != RC_SUCCESS)
  {
    dbg(TRACE,"SYSQCP ERROR (%d): UNABLE TO PUT RBS ONTO PRIO4",ilRc);
  }
  else
  {
    ilIdx = GetClientIdx(pcpSelKey);
    if (ilIdx >= 0)
    {
      rhosts_spool[ilIdx].PendingRbs++;
      if (rhosts_spool[ilIdx].PendingRbs > 1)
      {
        dbg(TRACE,"MULTIPLE RBS (%d) MOVED TO QUE PRIO4",rhosts_spool[ilIdx].PendingRbs);
      }
      else
      {
        dbg(TRACE,"NEW RBS LIST MOVED TO QUE PRIO4");
      }
    }
  }
  ilRc = tools_send_sql(que_out,"RBS"," "," "," ","OK");
  ilRc = RC_SUCCESS;
  return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static void InitDailyReport(int ipCheckPrint)
{
  int ilPrinted = FALSE;
  char pclTimeStamp[16];
  char pclTmp[4];
  GetServerTimeStamp ("UTC", 1, 0, pclTimeStamp);
  pclTmp[2] = 0x00;
  pclTmp[0] = pclTimeStamp[10];
  pclTmp[1] = pclTimeStamp[11];
  igRptMin = atoi(pclTmp);
  pclTmp[0] = pclTimeStamp[8];
  pclTmp[1] = pclTimeStamp[9];
  igRptHour = atoi(pclTmp);
  pclTmp[0] = pclTimeStamp[6];
  pclTmp[1] = pclTimeStamp[7];
  igRptDay = atoi(pclTmp);
  pclTmp[0] = pclTimeStamp[4];
  pclTmp[1] = pclTimeStamp[5];
  igRptMonth = atoi(pclTmp);
  pclTimeStamp[8] = 0x00;
  if (ipCheckPrint == TRUE)
  {
    if (igRptLastMin != igRptMin)
    {
      SetMaxWksCount();
      igRptLastMin = igRptMin;
    }
    if (igRptLastHour != igRptHour)
    {
      PrintHourStatistic(TRUE);
      ilPrinted = TRUE;
      igRptLastHour = igRptHour;
    }
    if (igRptLastDay != igRptDay)
    {
      PrintDailyStatistic(TRUE);
      ilPrinted = TRUE;
      igRptLastDay = igRptDay;
    }
    if (igRptLastMonth != igRptMonth)
    {
      PrintMonthStatistic(TRUE);
      ilPrinted = TRUE;
      if (igRptLastMonth == 12)
      {
        PrintYearStatistic(TRUE);
      }
      igRptLastMonth = igRptMonth;
    }
    if (ilPrinted == TRUE)
    {
      strcpy(pcgDailyStamp,pclTimeStamp);
      dbg(TRACE,"NEW INITIAL REPORT TIMESTAMP <%s> MT=%d,DY=%d,HR=%d,MI=%d",
        pcgDailyStamp,igRptMonth,igRptDay,igRptHour,igRptMin);
      dbg(TRACE,"============== START/END ================");
    }
  }
  else
  {
    strcpy(pcgDailyStamp,pclTimeStamp);
    dbg(TRACE,"init_bchdl: CFG INITIAL REPORT TIMESTAMP <%s> MT=%d,DY=%d,HR=%d,MI=%d",
        pcgDailyStamp,igRptMonth,igRptDay,igRptHour,igRptMin);
    igRptLastMonth = igRptMonth;
    igRptLastDay = igRptDay;
    igRptLastHour = igRptHour;
    igRptLastMin = igRptMin;
  }
  return;
}

/* ******************************************************************** */
/* ******************************************************************** */
static void PrintDailyStatistic(int ipReset)
{
  FILE *pflSave = NULL;
  char pclFileName[128];
  char pclOut[4098];
  char pclTmp[16];
  int ilDoIt = FALSE;
  int ilPrintIt = FALSE;
  int ilPos = 0;
  int ilTbl = 0;
  int i = 0;
  if ((lgRefreshStatistic > 0) || (ipReset == FALSE))
  {
    sprintf (pclFileName, "%s/REP_BRC%d_%05d_%s_DAILY.csv", cfgRptPath,mod_id,igMyPid,pcgDailyStamp);
    pflSave = fopen(pclFileName,"w");
  }
  if ((pflSave != NULL) || (ipReset == TRUE))
  {
    if (pflSave != NULL)
    {
      fprintf(pflSave,"DAILY(%d);TYPE;MEAN;OBJ.HOURS;"
                      "00;01;02;03;04;05;06;07;08;09;"
                      "10;11;12;13;14;15;16;17;18;19;"
                      "20;21;22;23;\n",igRptLastDay);
      ilPrintIt = TRUE;
    }
    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<24;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcSent.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcSent.Hour[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcSent.Hour[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;SENT;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<24;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcIgno.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcIgno.Hour[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcIgno.Hour[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;IGNO;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=0; ilTbl < NO_OF_REMOTE_HOSTS; ilTbl++)
    {
      if (remote_hosts[ilTbl][0] != EOS)
      {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<24;i++)
	{
          pclTmp[0] = 0x00;
          if (rhosts_spool[ilTbl].BcSent.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rhosts_spool[ilTbl].BcSent.Hour[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rhosts_spool[ilTbl].BcSent.Hour[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;CLNT;RSNT;%s/%d;%s\n",
                  pcgDailyStamp,remote_hosts[ilTbl],
                  rhosts_spool[ilTbl].InternalClient,
                  pclOut);
        }
      }
    }
    for (ilTbl=1; ilTbl <= rgBcOrig[0].Orig; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
        for (i=0;i<24;i++)
	{
          pclTmp[0] = 0x00;
          if (rgBcOrig[ilTbl].EvtRcv.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgBcOrig[ilTbl].EvtRcv.Hour[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgBcOrig[ilTbl].EvtRcv.Hour[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;ORIG;RCVD;QUE %d;%s\n",
                  pcgDailyStamp,rgBcOrig[ilTbl].Orig,pclOut);
        }
    }
    ilPos = 0;
    for (i=0;i<24;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.WksCount.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.WksCount.Hour[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.WksCount.Hour[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;CONN;MAXIMUM;%s\n",
                      pcgDailyStamp, pclOut);
    }
    ilPos = 0;
    for (i=0;i<24;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.DataGram.Hour[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.DataGram.Hour[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.DataGram.Hour[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;SENT;DATAGRAM;%s\n",
                      pcgDailyStamp, pclOut);
      fclose(pflSave);
      dbg(TRACE,"REPORT ISSUED: <%s>",pclFileName);
      dbg(TRACE,"============== START/END ================");
    }
  }
  return;
}
  
/* ******************************************************************** */
/* ******************************************************************** */
static void PrintHourStatistic(int ipReset)
{
  FILE *pflSave = NULL;
  char pclFileName[128];
  char pclOut[4098];
  char pclTmp[16];
  int ilDoIt = FALSE;
  int ilPrintIt = FALSE;
  int ilPos = 0;
  int ilTbl = 0;
  int i = 0;
  if ((lgRefreshStatistic > 0) || (ipReset == FALSE))
  {
    /* Must Print When Command 999 Has Been Received */
    sprintf (pclFileName, "%s/REP_BRC%d_%05d_%s_HH_%02d.csv",
                          cfgRptPath,mod_id,igMyPid,pcgDailyStamp,igRptLastHour);
    pflSave = fopen(pclFileName,"w");
  }
  if ((pflSave != NULL) || (ipReset == TRUE))
  {
    /* Must Run into Loops for Reset */
    /* Even if the Statistic is switched off */
    if (pflSave != NULL)
    {
      fprintf(pflSave,"HOUR(%d);TYPE;MEAN;OBJ.MINUTE;"
                      "00;01;02;03;04;05;06;07;08;09;"
                      "10;11;12;13;14;15;16;17;18;19;"
                      "20;21;22;23;24;25;26;27;28;29;"
                      "30;31;32;33;34;35;36;37;38;39;"
                      "40;41;42;43;44;45;46;47;48;49;"
                      "50;51;52;53;54;55;56;57;58;59;"
                      "\n",igRptLastHour);
      ilPrintIt = TRUE;
    }
    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<60;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcSent.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcSent.Min[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcSent.Min[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;SENT;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<60;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcIgno.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcIgno.Min[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcIgno.Min[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;IGNO;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=0; ilTbl < NO_OF_REMOTE_HOSTS; ilTbl++)
    {
      if (remote_hosts[ilTbl][0] != EOS)
      {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=0;i<60;i++)
	{
          pclTmp[0] = 0x00;
          if (rhosts_spool[ilTbl].BcSent.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rhosts_spool[ilTbl].BcSent.Min[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rhosts_spool[ilTbl].BcSent.Min[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;CLNT;RSNT;%s/%d;%s\n",
                  pcgDailyStamp,remote_hosts[ilTbl],
                  rhosts_spool[ilTbl].InternalClient,
                  pclOut);
        }
      }
    }
    for (ilTbl=1; ilTbl <= rgBcOrig[0].Orig; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
        for (i=0;i<60;i++)
	{
          pclTmp[0] = 0x00;
          if (rgBcOrig[ilTbl].EvtRcv.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgBcOrig[ilTbl].EvtRcv.Min[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgBcOrig[ilTbl].EvtRcv.Min[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;ORIG;RCVD;QUE %d;%s\n",
                  pcgDailyStamp,rgBcOrig[ilTbl].Orig,pclOut);
        }
    }
    ilPos = 0;
    for (i=0;i<60;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.WksCount.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.WksCount.Min[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.WksCount.Min[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;CONN;MAXIMUM;%s\n",
                    pcgDailyStamp, pclOut);
    }
    ilPos = 0;
    for (i=0;i<60;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.DataGram.Min[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.DataGram.Min[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.DataGram.Min[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;SENT;DATAGRAM;%s\n",
                    pcgDailyStamp, pclOut);
      fclose(pflSave);
      dbg(TRACE,"REPORT ISSUED: <%s>",pclFileName);
      dbg(TRACE,"============== START/END ================");
    }
  }
  return;
}
  
/* ******************************************************************** */
/* ******************************************************************** */
static void PrintMonthStatistic(int ipReset)
{
  FILE *pflSave = NULL;
  char pclFileName[128];
  char pclOut[4098];
  char pclTmp[16];
  int ilDoIt = FALSE;
  int ilPrintIt = FALSE;
  int ilPos = 0;
  int ilTbl = 0;
  int i = 0;
  if ((lgRefreshStatistic > 0) || (ipReset == FALSE))
  {
    sprintf (pclFileName, "%s/REP_BRC%d_%05d_%s_MONTH.csv", cfgRptPath,mod_id,igMyPid,pcgDailyStamp);
    pflSave = fopen(pclFileName,"w");
  }
  if ((pflSave != NULL) || (ipReset == TRUE))
  {
    if (pflSave != NULL)
    {
      fprintf(pflSave,"MONTH(%d);TYPE;MEAN;OBJ.DAYS;"
                      "01;02;03;04;05;06;07;08;09;"
                      "10;11;12;13;14;15;16;17;18;19;"
                      "20;21;22;23;24;25;26;27;28;29;"
                      "30;31;\n",igRptLastMonth);
      ilPrintIt = TRUE;
    }
    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=31;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcSent.Day[i] > 0)
          {
            ilDoIt = ilPrintIt;
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcSent.Day[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcSent.Day[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;SENT;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=31;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcIgno.Day[i] > 0)
          {
            ilDoIt = ilPrintIt;
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcIgno.Day[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcIgno.Day[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;IGNO;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=0; ilTbl < NO_OF_REMOTE_HOSTS; ilTbl++)
    {
      if (remote_hosts[ilTbl][0] != EOS)
      {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=31;i++)
	{
          pclTmp[0] = 0x00;
          if (rhosts_spool[ilTbl].BcSent.Day[i] > 0)
          {
	    sprintf(pclTmp,"%d", rhosts_spool[ilTbl].BcSent.Day[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rhosts_spool[ilTbl].BcSent.Day[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;CLNT;RSNT;%s/%d;%s\n",
                  pcgDailyStamp,remote_hosts[ilTbl],
                  rhosts_spool[ilTbl].InternalClient,
                  pclOut);
        }
      }
    }
    for (ilTbl=1; ilTbl <= rgBcOrig[0].Orig; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
        for (i=1;i<=31;i++)
	{
          pclTmp[0] = 0x00;
          if (rgBcOrig[ilTbl].EvtRcv.Day[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgBcOrig[ilTbl].EvtRcv.Day[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgBcOrig[ilTbl].EvtRcv.Day[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;ORIG;RCVD;QUE %d;%s\n",
                  pcgDailyStamp,rgBcOrig[ilTbl].Orig,pclOut);
        }
    }
    ilPos = 0;
    for (i=1;i<=31;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.WksCount.Day[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.WksCount.Day[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.WksCount.Day[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;CONN;MAXIMUM;%s\n",
                    pcgDailyStamp, pclOut);
    }
    ilPos = 0;
    for (i=1;i<=31;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.DataGram.Day[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.DataGram.Day[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.DataGram.Day[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;SENT;DATAGRAM;%s\n",
                    pcgDailyStamp, pclOut);
      fclose(pflSave);
      dbg(TRACE,"REPORT ISSUED: <%s>",pclFileName);
      dbg(TRACE,"============== START/END ================");
    }
  }
  return;
}
  
/* ******************************************************************** */
/* ******************************************************************** */
static void PrintYearStatistic(int ipReset)
{
  FILE *pflSave = NULL;
  char pclFileName[128];
  char pclOut[4098];
  char pclTmp[16];
  int ilDoIt = FALSE;
  int ilPrintIt = FALSE;
  int ilPos = 0;
  int ilTbl = 0;
  int i = 0;
  if ((lgRefreshStatistic > 0) || (ipReset == FALSE))
  {
    sprintf (pclFileName, "%s/REP_BRC%d_%05d_%s_RYEAR.csv", cfgRptPath,mod_id,igMyPid,pcgDailyStamp);
    pflSave = fopen(pclFileName,"w");
  }
  if ((pflSave != NULL) || (ipReset == TRUE))
  {
    strcpy(pclTmp,pcgDailyStamp);
    pclTmp[4] = 0x00;
    if (pflSave != NULL)
    {
      fprintf(pflSave,"YEAR(%s);TYPE;MEAN;OBJ.MONTH;"
                      "01;02;03;04;05;06;07;08;09;10;11;12;\n",pclTmp);
      ilPrintIt = TRUE;
    }
    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=12;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcSent.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcSent.Month[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcSent.Month[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;SENT;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=1; ilTbl<=igTblNumCnt; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=12;i++)
	{
          pclTmp[0] = 0x00;
          if (rgTblBcNum[ilTbl].BcIgno.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgTblBcNum[ilTbl].BcIgno.Month[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgTblBcNum[ilTbl].BcIgno.Month[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;TABS;IGNO;%s;%s\n",pcgDailyStamp,rgTblBcNum[ilTbl].TableName,pclOut);
        }
    } /* end for */

    for (ilTbl=0; ilTbl < NO_OF_REMOTE_HOSTS; ilTbl++)
    {
      if (remote_hosts[ilTbl][0] != EOS)
      {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=12;i++)
	{
          pclTmp[0] = 0x00;
          if (rhosts_spool[ilTbl].BcSent.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rhosts_spool[ilTbl].BcSent.Month[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rhosts_spool[ilTbl].BcSent.Month[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;CLNT;RSNT;%s/%d;%s\n",
                  pcgDailyStamp,remote_hosts[ilTbl],
                  rhosts_spool[ilTbl].InternalClient,
                  pclOut);
        }
      }
    }
    for (ilTbl=1; ilTbl <= rgBcOrig[0].Orig; ilTbl++)
    {
        ilDoIt = FALSE;
	ilPos = 0;
	for (i=1;i<=12;i++)
	{
          pclTmp[0] = 0x00;
          if (rgBcOrig[ilTbl].EvtRcv.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgBcOrig[ilTbl].EvtRcv.Month[i]);
            ilDoIt = ilPrintIt;
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgBcOrig[ilTbl].EvtRcv.Month[i] = 0;
          }
	}
	pclOut[ilPos] = 0x00;
        if (ilDoIt == TRUE)
        {
	  fprintf(pflSave,"%s;ORIG;RCVD;QUE %d;%s\n",
                  pcgDailyStamp,rgBcOrig[ilTbl].Orig,pclOut);
        }
    }
    ilPos = 0;
    for (i=1;i<=12;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.WksCount.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.WksCount.Month[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.WksCount.Month[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;CONN;MAXIMUM;%s\n",
                  pcgDailyStamp, pclOut);
    }
    ilPos = 0;
    for (i=1;i<=12;i++)
    {
          pclTmp[0] = 0x00;
          if (rgClient.DataGram.Month[i] > 0)
          {
	    sprintf(pclTmp,"%d", rgClient.DataGram.Month[i]);
          }
	  StrgPutStrg (pclOut, &ilPos, pclTmp, 0, -1, ";");
          if (ipReset == TRUE)
          {
            rgClient.DataGram.Month[i] = 0;
          }
    }
    pclOut[ilPos] = 0x00;
    if (ilPrintIt == TRUE)
    {
      fprintf(pflSave,"%s;CLNT;SENT;DATAGRAM;%s\n",
                    pcgDailyStamp, pclOut);
      fclose(pflSave);
      dbg(TRACE,"REPORT ISSUED: <%s>",pclFileName);
      dbg(TRACE,"============== START/END ================");
    }
  }
  return;
}

/* ******************************************************************** */
/* ******************************************************************** */
static void SetMaxWksCount(void)
{
  if (lgTotalWksCon > rgClient.WksCount.Min[igRptMin])
      rgClient.WksCount.Min[igRptMin] = lgTotalWksCon;
  if (lgTotalWksCon > rgClient.WksCount.Hour[igRptHour])
      rgClient.WksCount.Hour[igRptHour] = lgTotalWksCon;
  if (lgTotalWksCon > rgClient.WksCount.Day[igRptDay])
      rgClient.WksCount.Day[igRptDay] = lgTotalWksCon;
  if (lgTotalWksCon > rgClient.WksCount.Month[igRptMonth])
      rgClient.WksCount.Month[igRptMonth] = lgTotalWksCon;
  lgTotalWksCon = 0;
  return;
}

/* ******************************************************************** */
/* ******************************************************************** */
static void CountBcEvent(int ipQueId)
{
  static int ilPrvOrigin = -1;
  static int ilPrvIdx = -1;
  int ilOrigin = 0;
  int ilIdx = 0;
  int i = 0;
  ilOrigin = ipQueId;
  if (ilOrigin >= 20000)
  {
    ilOrigin = 1800;
  }
  if ((ilPrvOrigin >= 0) && (ilPrvOrigin == ilOrigin))
  {
    ilIdx = ilPrvIdx;
  }
  else
  {
    while ((i<rgBcOrig[0].Orig) && (ilIdx < 1))
    {
      i++;
      if (rgBcOrig[i].Orig == ilOrigin)
      {
        ilIdx = i;
      }
    }
  }
  if (ilIdx < 1)
  {
    if (rgBcOrig[0].Orig < MAX_NO_OF_ORIGIN)
    {
      rgBcOrig[0].Orig++;
      ilIdx = rgBcOrig[0].Orig;
      rgBcOrig[ilIdx].Orig = ilOrigin;
    }
  }
  if (ilIdx > 0)
  {
    rgBcOrig[ilIdx].EvtRcv.Min[igRptMin]++;
    rgBcOrig[ilIdx].EvtRcv.Hour[igRptHour]++;
    rgBcOrig[ilIdx].EvtRcv.Day[igRptDay]++;
    rgBcOrig[ilIdx].EvtRcv.Month[igRptMonth]++;
    ilPrvOrigin = ilOrigin;
    ilPrvIdx = ilIdx;
  }

  return;
}

/* =============================================================== */
/* =============================================================== */
static int CreateBcqFileData(char *pcpFileName, STR_DESC* prpString, int ipTrimData, char *pcpSize)
{
  int ilRC = RC_SUCCESS;
  int ilOutDatLen = 0;
  int ilOutDatPos = 0;
  char pclKeyTot[64];
  char pclKeyCmd[64];
  char pclKeyTbl[64];
  char pclKeyTws[64];
  char pclKeyTwe[64];
  char pclKeyUsr[64];
  char pclKeyWks[64];
  char pclKeyApp[64];
  char pclKeyQue[64];
  char pclKeyHex[64];
  char pclKeyBcn[64];
  char pclTmpBuf[64];
  char pclDummy[32];
  EVENT        *prlEvent      = NULL;
  BC_HEAD      *prlBchead     = NULL;
  CMDBLK       *prlCmdblk     = NULL;
  char         *pclSelection  = NULL;
  char         *pclFields     = NULL;
  char         *pclData       = NULL;
  char         *pclError      = NULL;
  char         *pclNoError    = "\0";
  char *pclOutData = NULL;

  prpString->UsedLen = 0;

  prlEvent = (EVENT *)event;
  if (prlEvent->command == EVENT_DATA)
  {
    prlBchead    = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
    if (prlBchead->rc != RC_FAIL)
    {
      pclError = pclNoError;
      prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
      TrimAndFilterCr(pclData, ",", "\n");
    } /* end if */
    else
    {
      pclError   = prlBchead->data;
      prlCmdblk    = (CMDBLK *)((char *) pclError + strlen(pclError) + 1) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
      pclData = pclNoError;
    } /* end else */

    pclOutData = prpString->Value;
    ilOutDatLen = prlEvent->data_length;
    if (ilOutDatLen > prpString->AllocatedSize)
    {
      if (prpString->AllocatedSize <= 0)
      {
        pclOutData = (char *)malloc(ilOutDatLen);
        if (pclOutData == NULL)
        {
          dbg(TRACE,"CAN'T ALLOC %d BYTES!!!",ilOutDatLen);
          /* Terminate(); */
        } /* end if */
        prpString->AllocatedSize = ilOutDatLen;
        prpString->Value = pclOutData;
      } /* end if */
      else
      {
        pclOutData = (char*)realloc((char*)pclOutData, ilOutDatLen);
        if (pclOutData == NULL)
        {
          dbg(TRACE,"CAN'T RE-ALLOC %d BYTES!!!",ilOutDatLen);
          /* Terminate(); */
        } /* end if */
        prpString->AllocatedSize = ilOutDatLen;
        prpString->Value = pclOutData;
      } /* end else */
    } /* end if */

    prpString->UsedLen = 0;
    memset (pclKeyUsr, '\0', sizeof(prlBchead->dest_name) + 1) ;
    strncpy (pclKeyUsr, prlBchead->dest_name, sizeof(prlBchead->dest_name)) ;
    memset (pclKeyWks, '\0', sizeof(prlBchead->recv_name) + 1) ;
    strncpy (pclKeyWks, prlBchead->recv_name, sizeof(prlBchead->recv_name)) ;
    memset (pclKeyCmd, '\0', sizeof(prlCmdblk->command) + 1) ;
    strncpy (pclKeyCmd, prlCmdblk->command, sizeof(prlCmdblk->command)) ;
    memset (pclKeyTws, '\0', sizeof(prlCmdblk->tw_start) + 1) ;
    strncpy (pclKeyTws, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start)) ;
    memset (pclKeyTwe, '\0', sizeof(prlCmdblk->tw_end) + 1) ;
    strncpy (pclKeyTwe, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end)) ;
    memset (pclKeyTbl, '\0', sizeof(prlCmdblk->obj_name) + 1) ;
    strncpy (pclKeyTbl, prlCmdblk->obj_name, sizeof(prlCmdblk->obj_name)) ;
    sprintf(pclKeyBcn,"%d",prlBchead->bc_num);

    igBcqFileNo++;
    if (igBcqFileNo > 99999)
    {
      igBcqFileNo = 1;
    }
    sprintf(pcpFileName,"%05d_%05d.BCQ",prlBchead->bc_num,igBcqFileNo);

    ilOutDatPos = 16;

    StrgPutStrg(pclOutData,&ilOutDatPos,"{=BCNUM=}",0,-1,pclKeyBcn);
    if (pclKeyCmd[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=CMD=}",0,-1,pclKeyCmd);
    if (pclKeyTbl[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=TBL=}",0,-1,pclKeyTbl);
    if (pclKeyUsr[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=USR=}",0,-1,pclKeyUsr);
    if (pclKeyWks[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=WKS=}",0,-1,pclKeyWks);
    if (pclKeyTws[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=TWS=}",0,-1,pclKeyTws);
    if (pclKeyTwe[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=TWE=}",0,-1,pclKeyTwe);
    if (prlBchead->rc == RC_FAIL)
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=ERR=}",0,-1,pclError);
    if (pclSelection[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=WHE=}",0,-1,pclSelection);
    if (pclFields[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=FLD=}",0,-1,pclFields);
    if (pclData[0] != '\0')
        StrgPutStrg(pclOutData,&ilOutDatPos,"{=DAT=}",0,-1,pclData);

    pclOutData[ilOutDatPos] = 0x00;
    prpString->UsedLen = ilOutDatPos;
    sprintf(pclKeyTot,"%09d",ilOutDatPos);
    ilOutDatPos = 0;
    StrgPutStrg(pclOutData,&ilOutDatPos,"{=TOT=}",0,-1,pclKeyTot);

    sprintf(pcpSize,"%d",prpString->UsedLen);
    dbg(TRACE,"BCQ FILE NAME <%s> (SIZE = %s)", pcpFileName, pcpSize);

    ilRC = RC_FAIL;
    sprintf(pcgBcqFullPath,"%s/%s",pcgBcqPath, pcgBcqFile);
    pfgBcqPtr = fopen(pcgBcqFullPath,"w");
    if (pfgBcqPtr != NULL)
    {
      fwrite((void *)pclOutData,prpString->UsedLen,1,pfgBcqPtr);
      memset(pcgGrpCtl, '.', GRP_REC_SIZE);
      pcgGrpCtl[0] = '*';
      pcgGrpCtl[GRP_REC_SIZE-1] = '*';
      fwrite((void *)pcgGrpCtl,GRP_REC_SIZE,1,pfgBcqPtr);
      fflush(pfgBcqPtr);
      fclose(pfgBcqPtr);
      ilRC = RC_SUCCESS;
    }
    if (ilRC == RC_SUCCESS)
    {
      pfgBcqPtr = fopen(pcgBcqFullPath,"r+");
      if (pfgBcqPtr == NULL)
      {
        ilRC = RC_FAIL;
      }
    }
  } /* end if */
  else
  {
    dbg(TRACE,"TRANSLATE: EVENT WITHOUT DATA (TYPE=%d)",prlEvent->command);
    ilRC = RC_FAIL;
  } /* end else */
  return ilRC; 
} /* end CreateBcqFileData */

static void TrimAndFilterCr(char *pclTextBuff,char *pcpFldSep, char *pcpRecSep)
{
  char *pclDest = NULL;
        char *pclSrc = NULL;
        char *pclLast = NULL; /* last non blank byte position */
        pclDest = pclTextBuff;
        pclSrc  = pclTextBuff;
        pclLast  = pclTextBuff - 1;
        while (*pclSrc != '\0')
        {
                if (*pclSrc != '\r')
                {
                        if ((*pclSrc == pcpFldSep[0]) || (*pclSrc == pcpRecSep[0]))
                        {
                                pclDest = pclLast + 1;
                        } /* end if */
                        *pclDest = *pclSrc;
                        if (*pclDest != ' ')
                        {
                                pclLast = pclDest;
                        } /* end if */
                        pclDest++;
                } /* end if */
                pclSrc++;
        } /* end while */
        pclDest = pclLast + 1;
        *pclDest = '\0';
  return;
}

/* =============================================================== */
/* =============================================================== */
static void CheckBcqFile(int ipBcqDataSize, int ipSentCnt)
{
  struct stat rlLfBuff;
  int ilCheckFile = TRUE;
  int ilClearFile = FALSE;
  int ilReadNow = 0;
  int ilMaxPos = 0;
  int ilChrPos = 0;
  char pclChr[4] = " ";
  dbg(TRACE,"BCQF <%s> CHECK ACKNOWLEDGE",pcgBcqFullPath);
  pfgBcqPtr = fopen(pcgBcqFullPath,"r+");
  if (pfgBcqPtr != NULL)
  {
    if (ilCheckFile == TRUE)
    {
      if (fstat(fileno(pfgBcqPtr),&rlLfBuff) !=  RC_FAIL)
      {
        /* ilMaxPos = rlLfBuff.st_size - ipBcqDataSize; */
        ilMaxPos = ipSentCnt + 2;
        memset(pcgGrpCtl,0x00,ilMaxPos+1);
        ilChrPos = ipBcqDataSize;
        fseek(pfgBcqPtr, ilChrPos, SEEK_SET);
        ilReadNow = fread((void *)pcgGrpCtl,1,ilMaxPos,pfgBcqPtr);
        dbg(TRACE,"BCQ FILE STATUS <%s>",pcgGrpCtl);
        if (ilReadNow == ilMaxPos)
        {
          ilMaxPos -= 2;
          while ((ilMaxPos > 0) && (pcgGrpCtl[ilMaxPos] == '.'))
          {
            ilMaxPos--;
          }
          if (ilMaxPos <= 0)
          {
            dbg(TRACE,"ALL MEMBERS GOT THE FILE");
            ilClearFile = TRUE;
          }
        }
      }
    }
    fclose(pfgBcqPtr);
    if (ilClearFile == TRUE)
    {
      remove(pcgBcqFullPath);
      dbg(TRACE,"FILE <%s> REMOVED", pcgBcqFullPath);
    }
  }
  else
  {
    dbg(TRACE,"FILE ALREADY REMOVED BY BCQ GROUP");
  }
  return;
}

static int ExtendEvent(char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  BC_HEAD *prlRcvBcHd = NULL;
  CMDBLK *prlRcvCmdBlk = NULL;
  BC_HEAD *prlMyBcHd = NULL;
  CMDBLK *prlMyCmdBlk = NULL;
  char *pclPtr = NULL;
  ilLen = strlen(pcpSel) + strlen(pcpFld) +
          strlen(pcpDat) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
          sizeof(EVENT) + 128;
  MyEvent = (EVENT *) realloc(MyEvent, ilLen);
  if (MyEvent == NULL)
  {
    ilRc= RC_FAIL;
    dbg(TRACE,"Realloc %d for MY EVENT FAILED <%s>", ilLen, strerror(errno));
  }
  ilRc = RC_SUCCESS;
  if (ilRc == RC_SUCCESS)
  {
    /* Get the received structure */
    prlRcvBcHd  = (BC_HEAD *) ((char *)event + sizeof(EVENT));
    prlRcvCmdBlk = (CMDBLK *) prlRcvBcHd->data;
    /* and copy everything to the new buffer */
    memcpy(MyEvent,event,sizeof(EVENT));

    prlMyBcHd = (BC_HEAD *) (((char *)MyEvent) + sizeof(EVENT));
    memcpy(prlMyBcHd,prlRcvBcHd,sizeof(BC_HEAD));
    prlMyCmdBlk = (CMDBLK *) prlMyBcHd->data;
    memcpy(prlMyCmdBlk,prlRcvCmdBlk,sizeof(CMDBLK));

    /* Just using the same faulty (data-)length as all others do */
    dbg(TRACE,"NEW EVENT LENGTH = %d",ilLen);
    igMyEventLen = ilLen;
    MyEvent->data_length = ilLen; 
    pclPtr = (char *)prlMyCmdBlk->data;
    strcpy(pclPtr,pcpSel);
    pclPtr += strlen(pcpSel) + 1;
    strcpy(pclPtr,pcpFld);
    pclPtr += strlen(pcpFld) + 1;
    strcpy(pclPtr,pcpDat);
    event = MyEvent;
  }


  return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int InitNetworkRegister(void)
{
  int ilRc = RC_SUCCESS;
  int i = 0;
  dbg(TRACE,"init_bchdl: INIT NETWORK REGISTER");
  for (i=0; i<NO_OF_REMOTE_HOSTS ; i++)
  {
    if (rhosts_cnt[i] > 0)
    {
      RegisterNetwork(TRUE, remote_hosts[i]);
    }
  }
  return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int RegisterNetwork(int ipRegFlag, char *pcpHexTcpIp)
{
  int ilFound = FALSE;
  int ilIdx = 0;
  int i = 0;
  int k = 0;
  char pclNetId[16];
  strcpy(pclNetId,pcpHexTcpIp);
  if (strncmp(pclNetId,"BCQ",3) != 0)
  {
    pclNetId[6] = '\0';
    strcat(pclNetId,"FF");
    k = -1;
    ilIdx = -1;
    ilFound = FALSE;
    for (i=0; ((ilIdx<0) && (i<NO_OF_REMOTE_NETWORKS)); i++)
    {
      if (remote_network[i][0] != EOS)
      {
        /* Already used entry */
        if (strcmp(remote_network[i],pclNetId) == 0)
        {
          /* Network ID found */
          ilIdx = i;
          ilFound = TRUE;
        }
        else if ((k < 0) && (remote_net_cnt[i] < 1))
        {
          /* Remember the first not connected network */
          /* if we run out of available entries in array */
          k = i;
        }
      }
      else
      {
        /* First free entry */
        ilIdx = i;
      }
    }
    if (ilIdx < 0)
    {
      ilIdx = k;
    }
    if (ilIdx >= 0)
    {
      if (ilFound != TRUE)
      {
        strcpy(remote_network[ilIdx],pclNetId);
      }
      if (ipRegFlag == TRUE)
      {
        remote_net_cnt[ilIdx]++;
      }
      else
      {
        remote_net_cnt[ilIdx]--;
      }
      if (remote_net_cnt[ilIdx] <= 0)
      {
        dbg(TRACE,"NETWORK <%s> WKS: ALL CONNECTIONS CLOSED",remote_network[ilIdx]);
        remote_net_cnt[ilIdx] = 0;
      }
      else
      {
        dbg(TRACE,"NETWORK <%s> NOW: %d ACTIVE WORKSTATIONS",remote_network[ilIdx],remote_net_cnt[ilIdx]);
      }
    }
    else
    {
      dbg(TRACE,"====>> ============================= <<====");
      dbg(TRACE,"====>> REMOTE NETWORK ARRAY OVERFLOW <<====");
      dbg(TRACE,"====>> ============================= <<====");
    }
  }
  return ilIdx;
}
  
/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
