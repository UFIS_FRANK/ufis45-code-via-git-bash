#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/cophdl.c 1.6 2012/02/10 00:06:45SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20021127 JIM: The cophdl should not write argv[0] to debug file            */
/*                                                                            */
/* 201111   BST  v.1.3 Special version for EK POC, Selects from EK views.     */
/* 20111216 DKA: v.1.4 General version that will read PTM from LOATAB after a */
/*               trigger from FDIHDL.                                         */
/*               Removed LOTC and LCCC from CFDTAB references. Rather than    */
/*               alter all the tables in various machines. Not sure if these  */
/*               fields are EK POC specific.                                  */
/*               After first release must also be able to handle action msg   */
/*               if LoadTabViewer edits LOATAB PTM data.                      */
/* 20111227 DKA: v.1.5 Refine LOATAB selection for departures for PTMF cmnd,  */
/*               such that only rows for the arrival's telex are selected.    */
/*               This avoids getting rows for that same departure from another*/
/*               PTM telex.                                                   */
/* 20120210 DKA: v.1.6 Cursor leak due to returning from functions on DB "not */
/*               found" bypassing call to close_my_cursor().                  */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="@(#) UFIS (c) UFIS-AS ACDM cophdl.c 1.3 / "__DATE__" "__TIME__" / BST";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
/*
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
*/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include "ct.h"

#define FOR_NORMAL 0
#define FOR_INSERT 1
#define FOR_UPDATE 2
#define FOR_DELETE 3
#define FOR_SEARCH 4
#define FOR_TABLES 5

#define SORT_ASC        0
#define SORT_DESC       1


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];



static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "DXB";
static char  cgTabEnd[8] ="TAB";

static char pcgCurTime[32] = "";
static char pcgRecvName[64] = ""; /* BC-Head used as WKS-Name */
static char pcgDestName[64] = ""; /* BC-Head used as USR-Name */
static char pcgTwStart[64] = "";  /* BC-Head used as Stamp */
static char pcgTwEnd[64] = "";    /* BC-Head used as Stamp */

static long lgEvtCnt = 0;

/* BST                              */
/* Handling of Connex Info (CFITAB) */
static igFltCnt = 0;
static igConCnt = 0;

static int igFltWinVpfrDays = -1;
static int igFltWinVptoDays = 1;
static char pcgFltWinVpfrStrg[32] = "-1";
static char pcgFltWinVptoStrg[32] = "+1";

static int igMinShortConnexTime = 0;
static int igMaxShortConnexTime = 60;
static int igCheckTifaTifdTimes = TRUE;
static char pcgDataArea[1024*12];
static char pcgSqlBuf[1024*12];
static char pcgTmpBuf1[1024*12];
static char pcgTmpBuf2[1024*12];
static char pcgTmpBuf3[1024*12];

static char pcgAftTabName[] = "AFTTAB";
static char pcgAftTabGrid[] = "AftTab";
static char pcgAftTabFldList[] = "";
static char pcgAftTabFldSize[] = "";

static char pcgCurAftArrFields[1024] = "";
static char pcgCurAftArrData[1024] = "";

static char pcgRawTabRead[] = "VW_EK_MACS_ONWARD";
static char pcgRawTabSave[] = "RAWTAB";
static char pcgRawTabGrid[] = "RawTab";
static char pcgRawTabGetName[] = "ALN,FLIGHTNUMBER,TO_CHAR(SCHEDULEDDATE,'YYYYMMDD'),ONW_AIRLINE_DESIGNATOR,ONW_FLIGHT_NUMBER,TO_CHAR(ONW_OPERATION_DATE,'YYYYMMDD'),ONW_OPERATION_TIME,ONW_BOARD_POINT,REFERENCE_NUMBER,BOOKED_CLASS";
static char pcgRawTabFldList[] = "ALCD,FLTN,FDAY,OALC,OFLT,ODAY,OSKD,OORG,OREF,OCLS,SORT";
static char pcgRawTabFldSize[] = "3   ,4   ,8   ,5   ,4   ,8   ,8   ,6   ,10  ,1   ,20  ";
static char pcgRawTabNulStrg[] = ",,,,,,,,,,";

static char pcgCfdTabName[] = "CFDTAB";
static char pcgCfdTabGrid[] = "CfdTab";
static char pcgCfdTabFldList[] = "URNO,PRFL,HOPO,CDAT,LSTU,USEC,USEU,RURN,FLNO,ADID,STDT,MKEY,LKEY,CFAD,CFLN,CFCS,CFDT,CFDA,PAXF,PAXC,PAXY,PAXU,ADDI"; 
static char pcgCfdTabFldSize[] = "10  ,1   ,3   ,14  ,14  ,32  ,32  ,10  ,9   ,1   ,14  ,18  ,18  ,1   ,9   ,9   ,14  ,8   ,4   ,4   ,4   ,4   ,64  ";
static char pcgCfdTabNulStrg[] = "0,,,,,,,0,,,,,,,,,,,,,,,";

static char pcgLoaTabName[] = "LOATAB";
static char pcgLoaTabGrid[] = "LoaTab";
static char pcgLoaTabFldList[] = "APC3,DSSN,FLNO,FLNU,HOPO,IDNT,RURN,SSST,SSTP,STYP,TIME,TYPE,URNO,VALU"; 
static char pcgLoaTabFldSize[] = "3   ,3   ,9   ,10  ,3   ,22  ,10  ,3   ,10  ,3   ,14  ,3   ,10  ,6   ";
static char pcgLoaTabNulStrg[] = ",,,0,,,0,,,,,,0,";


/* static char pcgLoaTabFldList[] = "URNO,PRFL,HOPO,CDAT,LSTU,USEC,USEU,RURN,FLNO,ADID,STDT,MKEY,LKEY,CFAD,CFLN,CFCS,CFDT,CFDA,PAXF,PAXC,PAXY,PAXU,ADDI,LCCC,LOTC"; 
static char pcgCfdTabFldSize[] = "10  ,1   ,3   ,14  ,14  ,32  ,32  ,10  ,9   ,1   ,14  ,18  ,18  ,1   ,9   ,9   ,14  ,8   ,4   ,4   ,4   ,4   ,64  ,10  ,10  ";

static char pcgCfdTabNulStrg[] = "0,,,,,,,0,,,,,,,,,,,,,,,,,";
*/

static char pcgCfdFldLst[1024] = "";
static char pcgCfdDatLst[4096] = "";

#define MAX_CTFLT_VALUE 4
static STR_DESC argCtFlValue[MAX_CTFLT_VALUE+1];

static STR_DESC rgCtTabLine;
static STR_DESC rgCtTabData;
static STR_DESC rgCtTabList;
static STR_DESC rgCtTabUrno;

static STR_DESC rgCtFlLine;
static STR_DESC rgCtFlData;
static STR_DESC rgCtHtLine;

typedef struct
{
  int  ClCnt;
  int  AlCnt;
  int  ClIdx;
  char Class[2];
  char Alias[2];
} STR_CNT;
static STR_CNT rgClassTotals[30];

static char pcgMainClass[8] = "FCYU";
static char pcgCompClass[1024] = "F:ABD|C:EGH|Y:JIKLM";

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int InitCophdl();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(void);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/



/******************************************************************************/
/* Connection Information Function Prototypes : BST                           */
/******************************************************************************/
static int InitConnexInfoProcessing(void);
static int SendFlightConnexTriggers(char *pcpAftUrno, int ipForWhat);
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst);
static int SearchFlight (char *pcpTable, char *pcpFields, char *pcpWhere, int ipActionType, int ipFirstCall);
static int  get_any_item(char *dest, char *src, int ItemNbr, char *sep);
static void TrimRight (char *pcpBuffer);
static int BuildFlightNbr (char *fnr3, char *fnr2, char *alc3, char *alc2, char *flnr, char *flsf, int mth);
static int GetBasicData(char *pcpTable,char *pcpFields,char *pcpData,char *pcpResultFields,
                        char *pcpResultData,int ipMaxLines, int ipDefRc);
static int HandlePtmMessages(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData);
static int HandlePtmUpdates(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData);

/* simulation test */ 
static int ProcessLoaData(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData,
    int piLoadGrid);
static int ReadLoaTab (char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData);

static int      ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);
static int      BuildSqlFldStrg (char *pcpSqlStrg, char *pcpFldLst, int ipTyp);


static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize);

static int ReadEkViewFltList(char *pcpFltKey, char *pcpFltDay);
static int ReadFromEkView(char *pcpFltKey, char *pcpFltDay);

int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep);
static void ReplaceChars(char *pcpItems, char *pcpGet, char *pcpSet);;
static int InitClassTotals(int ipInit, int ipReset);
static int CountClassTotals(char *pcpClass);
static int ShowClassTotals(int ipShow);
static int GetMainTotals(int ipMNbr, char *pcpValue);
static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey);
static int CheckDbAction(char *pcpGridName, char *pcpTableName, int ipForWhat);

static int TestFunction(int ipForWhat);


static int GOR_SetMyTabUrnoPool(char *pcpCtTabName, char *pcpTableName);
static int GOR_GetValueFromUrnoPool(char *pcpGridName, char *pcpNxtUrno);
static int GOR_CheckGridUrnoPool(char *pcpGridName);
static int GOR_FillGridUrnoPool(char *pcpGridName, int ipCount);
static int GOR_InsertGridRecord2DB(char *pcpGridName, long lpLineNo);
static int GOR_UpdateGridRecord2DB(char *pcpGridName, long lpLineNo);
static int GOR_DeleteGridRecord2DB(char *pcpGridName, long lpLineNo);
static void GOR_BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst);
static void GOR_BuildSqlUpdValStrg(char *pcpResult, char *pcpFldLst);

static int MaintainCfdTabLine(char *pcpArrFlt, char *pcpDepFlt);
static long InsertCfdTabLine(char *pcpArrFlt, char *pcpDepFlt);
static long UpdateCfdTabLine(long lpCfdLine, char *pcpArrFlt, char *pcpDepFlt);

static int CreateCfdDataList(char *pcpArrFlt, char *pcpDepFlt, char *pcpFldLst, char *pcpDatLst);
static int CreatePtmTrigger(char *pcpMkey, char *pcpFltDay);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
char pcgMyRealFileName[]= "cophdl:"; /* 20021127 JIM: The cophdl should not 
                                       write argv[0] to debug file
                                     */
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    /* 20021127 JIM: The cophdl should not write argv[0] to debug file: */
    mod_name=pcgMyRealFileName;

    /* 20111216 DKA : get hopo from syslib */
    if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo)) != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: Error cannot find entry <HOMEAP> in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"MAIN: Found HOMEAP <%s> in SGS.TAB",cgHopo);
    }


    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* sprintf(cgConfigFile,"%s/cophdl",getenv("BIN_PATH"));         */
    /* ilRC = TransferFile(cgConfigFile);                            */
    /* if(ilRC != RC_SUCCESS)                                        */
    /* {                                                             */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* }                                                             */

    /* uncomment if necessary */
    sprintf(cgConfigFile,"%s/cophdl.cfg",getenv("CFG_PATH"));
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitCophdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitCophdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
      dbg (TRACE, "================= START/END =========================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
			case 777:
				TestFunction(1);
				break;
			case 778:
				TestFunction(0);
				break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitCophdl()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    char pclTmp[512];
    /* now reading from configfile or from database */
	debug_level = TRACE;
    SetSignals(HandleSignal);
    dbg(TRACE,"============================");

    (void) ReadConfigEntry("TIME_WINDOW", "FLT_WIN_VPFR_DAYS", pclTmp, "-1");
    igFltWinVpfrDays = atoi(pclTmp);

    (void) ReadConfigEntry("TIME_WINDOW", "FLT_WIN_VPTO_DAYS", pclTmp, "1");
    igFltWinVptoDays = atoi(pclTmp);

	InitConnexInfoProcessing();
        /* Following initializes CT grids for CFDTAB and LOATAB */
	InitEkViewEnvironment(); 


    igInitOK = TRUE;
	debug_level = TRACE;
    return(ilRC);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate();
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitCophdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitCophdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/* 20111216 Added CFIX for PTM trigger test from FDIHDL	                      */
/* 20111216 Added SIM1 for simulated LOATAB data                              */
/*     Cmd : SIM1, SEL: arrival MKEY per "EK-0349-20111216"                   */
/*     and FIELDS: departure LKEY per "EK-0045-20111216"                      */
/*     and DATA: totals for class F C E U per "1,2,3,4"                       */
/* 20111217 Added PTMF for PTM from FDIHDL.				      */
/*     SEL: arrival urno                                                      */
/*     DATA: comma separated list of departure urno's.                        */
/******************************************************************************/
static int HandleData()
{
  int    ilRC = RC_SUCCESS;
  char pclTable[16];
  char pclActCmd[16];
  BC_HEAD *prlBchd;
  CMDBLK *prlCmdblk;
  char *pclSelKey;
  char *pclFields;
  char *pclData;

  prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof (EVENT));
  prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);
  pclSelKey = (char *) prlCmdblk->data;
  pclFields = (char *) pclSelKey + strlen (pclSelKey) + 1;
  pclData = (char *) pclFields + strlen (pclFields) + 1;

  strcpy (pclTable, prlCmdblk->obj_name);
  strcpy (pclActCmd, prlCmdblk->command);
  str_trm_rgt (pclActCmd, " ", TRUE);

  if (strcmp(pclActCmd,"CFDC") == 0)
  {
	TestFunction(0);
  }
  else if (strcmp(pclActCmd,"CFIU") == 0)
  {
    HandlePtmUpdates(pclActCmd, "TABLE", pclSelKey, pclFields, pclData);
  }
  else if (strcmp(pclActCmd,"CFIX") == 0)
  {
    HandlePtmMessages(pclActCmd, "TABLE", pclSelKey, pclFields, pclData);
  } 
  else if (strcmp(pclActCmd,"SIM1") == 0)
  {
    ProcessLoaData(pclActCmd, "TABLE", pclSelKey, pclFields, pclData, 0);
  }
  else if (strcmp(pclActCmd,"PTMF") == 0)
  {
    /* PTM from FDIHDL */
    ReadLoaTab (pclActCmd, "TABLE", pclSelKey, pclFields, pclData);
  }
  else
    dbg(TRACE,"Unknown command <%s> - doing nothing", pclActCmd);


  return ilRC;
    
} /* end of HandleData */


/* =============================================================================== */
/*                        CONNECTION INFO FUNCTIONS BY BST                         */
/* =============================================================================== */
static int InitConnexInfoProcessing(void)
{
    int ilRc = RC_SUCCESS;
    char pclTmp[512];

    /* Processing of Connex Info (CFITAB) */
    /* ====================================== */
    dbg(TRACE,"==== CONNEX HANDLING ===");

    (void) ReadConfigEntry("CFITAB_PROCESSING", "CHECK_TIFA_TIFD_TIMES", pclTmp, "YES");
    if (strcmp (pclTmp, "YES") == 0)
    {
        igCheckTifaTifdTimes = TRUE;
    }
    else
    {
        igCheckTifaTifdTimes = FALSE;
    }

    (void) ReadConfigEntry("CFITAB_PROCESSING", "MIN_SHORT_CONNEX_TIME", pclTmp, "0");
    igMinShortConnexTime = atoi(pclTmp);

    (void) ReadConfigEntry("CFITAB_PROCESSING", "MAX_SHORT_CONNEX_TIME", pclTmp, "60");
    igMaxShortConnexTime = atoi(pclTmp);

    dbg(TRACE,"============================");
    return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int HandlePtmMessages(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    int ilGetRc = RC_SUCCESS;
    int ilCfdGetRc = RC_SUCCESS;
    int ilIsCodeShare = FALSE;
    int ilLen = 0;
    int ilTmpValue = 0;
    int ilArrStoaMin = 0;
    int ilArrTifaMin = 0;
    int ilDepStodMin = 0;
    int ilDepTifdMin = 0;
    int ilHubCtis = 0;
    int ilHubCtit = 0;
    int i = 0;
    long llTmpLen = 0;
    short slCfdCursor = 0;
    short slCfdFkt = START;
    short slInsCursor = 0;
    short slUpdCursor = 0;
    char pclFieldItems[32];
    char pclFldNam[16];
    char pclFldVal[16];
    char pclArrUrno[16];
    char pclArrAdid[16];
    char pclArrFlno[16];
    char pclArrFlda[16];
    char pclDepUrno[16];
    char pclDepFlno[16];
    char pclDepFlda[16];
    char pclDepPaxt[16];
    char pclDepPaxf[16];
    char pclDepPaxc[16];
    char pclDepPaxu[16]; //MSI: 20080118: To fix CFITAB problem
    char pclDepPaxy[16];
    char pclCfdSqlBuf[1024];

    //char pclCfdFields[] = "CFAD,CFLN,CFCS,CFDT,CFDA,PAXF,PAXC,PAXY"; // MSI: 20071128: commented out; see next line
    char pclCfdFields[] = "CFAD,CFLN,CFCS,CFDT,CFDA,PAXF,PAXC,PAXU,PAXY"; //MSI: 20071128: To accomodate "U" PAX class
    char pclCfdData[1024];
    char pclArrFields[] = "FTYP,URNO,FLNO,STOA,TIFA,PSTA,GTA1,REGN,ORG3,VIA3,TTYP,STYP";
    char pclArrData[4096];
    char pclDepFields[] = "FTYP,URNO,FLNO,STOD,TIFD,PSTD,GTD1,DES3,VIA3,REGN,TTYP,STYP";
    char pclDepData[4096];
    char pclHubArrFields[] = "AFSA,AURN,FLNA,STOA,TIFA,PSTA,GATA,RGNA,ORG3,VSA3,TTPA,STPA";
    char pclHubDepFields[] = "AFSD,DURN,FLND,STOD,TIFD,PSTD,GATD,DES3,VSD3,RGND,TTPD,STPD";
    char pclHubChkFields[] = "URNO,TALL,CTMS,CTMT";
    char pclHubChkData[128];
    char pclSysFields[] = "URNO,PRFL,CDAT,USEC,LSTU,USEU,HOPO,TYPE";
    char pclSysData[4096];
    //char pclPaxFields[] = "TALL,TCLF,TCLC,TCLY,TOTH"; // MSI: 20071128: commented out; see next line
    char pclPaxFields[] = "TALL,TCLF,TCLC,TCLU,TCLY,TOTH"; //MSI: 20071128: To accomodate "U" PAX class
    char pclPaxData[4096];
    char pclConFields[] = "SCID,CTMS,CTMT";
    char pclConData[4096];
    char pclHubAddi[128];
    char pclHubUrno[16];
    char pclTmpData[16];
    char pclAftDepFlno[16];
    char pclAftDepJfno[16];
    char pclFnr3[16];
    char pclFnr2[16];
    char pclAlc3[16];
    char pclAlc2[16];
    char pclFnbr[16];
    char pclFlsf[16];
    char pclSqlSelKey[256];
    char pclSqlTable[16];
    char pclHubAllFields[256];
    char pclHubAllData[512];
    char *pclBgnPtr = NULL;
    char *pclEndPtr = NULL;

    dbg(TRACE,"===== DATA ELEMENTS FROM QUEUE ==========");
    dbg(TRACE,"%s TABLE  <%s>", pcpCmd, pcpTable);
    dbg(TRACE,"%s FIELDS <%s>", pcpCmd, pcpFields);
    dbg(TRACE,"%s DATA   <%s>", pcpCmd, pcpData);
    dbg(TRACE,"%s SELECT <%s>", pcpCmd, pcpSelKey);
    dbg(TRACE,"=========================================");

    strcpy(pclArrUrno,"");
    strcpy(pclArrAdid,"");
    strcpy(pclArrFlno,"");
    strcpy(pclArrFlda,"");

    dbg(TRACE,"GET KEY VALUES FROM HEADER SECTION IN SELECTION STRING");
    (void) CedaGetKeyItem(pclArrFlno, &llTmpLen, pcpSelKey, "FLNO=", ",", TRUE);
    (void) CedaGetKeyItem(pclArrAdid, &llTmpLen, pcpSelKey, "ADID=", ",", TRUE);
    (void) CedaGetKeyItem(pclArrUrno, &llTmpLen, pcpSelKey, "URNO=", ",", TRUE);
    (void) CedaGetKeyItem(pclArrFlda, &llTmpLen, pcpSelKey, "FLDA=", ",", TRUE);

    if (pclArrAdid[0] == 'A')
    {
        strcpy (pclSqlTable, "AFTTAB");
        sprintf(pclSqlSelKey,"WHERE URNO=%s", pclArrUrno);
        dbg (TRACE, "ARR FLIGHT SELECTION <%s>", pclSqlSelKey);

        ilRc = SearchFlight (pclSqlTable, pclArrFields, pclSqlSelKey, FOR_SEARCH, TRUE);
        if (ilRc == RC_SUCCESS)
        {
            dbg (TRACE, "FOUND ARR CONNEX FLIGHT:\nFIELDS <%s>\nDATA <%s>", pclArrFields,pcgDataArea);

            strcpy(pclArrData,pcgDataArea);
            (void) get_real_item (pclTmpData, pcgDataArea, 4); /* STOA */
            (void) GetFullDay (pclTmpData, &ilTmpValue, &ilArrStoaMin, &ilTmpValue);
            (void) get_real_item (pclTmpData, pcgDataArea, 5); /* TIFA */
            (void) GetFullDay (pclTmpData, &ilTmpValue, &ilArrTifaMin, &ilTmpValue);

            sprintf (pcgSqlBuf, "UPDATE CFITAB SET PRFL='D',SCID=' ' WHERE AURN=%s", pclArrUrno);
            strcpy (pcgDataArea, "");
            dbg(TRACE,"\n<%s>",pcgSqlBuf);
            slUpdCursor = 0;

            ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
            if (ilRc == DB_SUCCESS)
            {
                commit_work ();
            }
            close_my_cursor (&slUpdCursor);

            sprintf(pclCfdSqlBuf,"SELECT %s FROM CFDTAB WHERE RURN=%s",pclCfdFields,pclArrUrno);
            slCfdCursor = 0;
            slCfdFkt = START;
            ilCfdGetRc = RC_SUCCESS;

            while (ilCfdGetRc == RC_SUCCESS)
            {
                ilCfdGetRc = sql_if(slCfdFkt, &slCfdCursor, pclCfdSqlBuf, pclCfdData);
                slCfdFkt = NEXT;
                if (ilCfdGetRc == RC_SUCCESS)
                {
                    BuildItemBuffer (pclCfdData, pclCfdFields, 0, ",");
                    /* "CFAD,CFLN,CFCS,CFDT,CFDA,PAXF,PAXC,PAXU,PAXY" */
                    get_real_item(pclDepFlno,pclCfdData,2);
                    get_real_item(pclDepFlda,pclCfdData,5);
                    get_real_item(pclDepPaxf,pclCfdData,6);
                    get_real_item(pclDepPaxc,pclCfdData,7);
                    get_real_item(pclDepPaxu,pclCfdData,8); //MSI: 20080118: To fix CFITAB problem
                    get_real_item(pclDepPaxy,pclCfdData,9);

                    dbg(TRACE,"GOT CONNEX DATA <%s> FLNO=<%s> FLDA=<%s>",pclCfdData,pclDepFlno,pclDepFlda);
                    strcpy (pclFnr3, pclDepFlno);
                    ilGetRc = BuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFnbr, pclFlsf, 0);
                    dbg (TRACE, "--- FLNO ITEMS OF <%s> ----", pclDepFlno);
                    dbg (TRACE, "FNR3 <%s>", pclFnr3);
                    dbg (TRACE, "FNR2 <%s>", pclFnr2);
                    dbg (TRACE, "ALC3 <%s>", pclAlc3);
                    dbg (TRACE, "ALC2 <%s>", pclAlc2);
                    dbg (TRACE, "FLTN <%s>", pclFnbr);
                    dbg (TRACE, "FLNS <%s>", pclFlsf);
                    dbg (TRACE, "---------------------------------");
                    strcpy(pclHubAddi," ");

                    ilTmpValue = 0;
                    ilTmpValue += atoi(pclDepPaxf);
                    ilTmpValue += atoi(pclDepPaxc);
                    ilTmpValue += atoi(pclDepPaxu); //MSI: 20080118: To fix CFITAB problem
                    ilTmpValue += atoi(pclDepPaxy);
                    sprintf(pclTmpData,"%d",ilTmpValue);

                    /* PaxFields = "TALL,TCLF,TCLC,TCLU,TCLY,TOTH" */ //MSI: to fix CFITAB problem
                    sprintf(pclPaxData,"%s,%s,%s,%s,%s,0",pclTmpData,pclDepPaxf,pclDepPaxc,pclDepPaxu,pclDepPaxy); //MSI:20080118:Added pclDepPaxu:To fix CFITAB problem

                    ilIsCodeShare = FALSE;
                    strcpy (pclSqlTable, "AFTTAB");
                    sprintf(pclSqlSelKey,"WHERE FLDA='%s' AND (FLNO='%s' OR FLNO='%s') AND ORG3='%s' AND FTYP NOT IN ('T','G','N')",
                            pclDepFlda,pclFnr2,pclFnr3,cgHopo);
                    dbg (TRACE, "DEP FLIGHT SELECTION <%s>", pclSqlSelKey);
 			/* 13Jun08: IPR added filter for NOOP */
                    ilRc = SearchFlight (pclSqlTable, pclDepFields, pclSqlSelKey, FOR_SEARCH, TRUE);
                    if (ilRc != RC_SUCCESS)
                    {
                        dbg (TRACE, "AFT CONNEX DEPARTURE NOT FOUND. CHECKING CODE SHARES");
                        strcpy (pclSqlTable, "AFTTAB");
                        sprintf(pclSqlSelKey,"WHERE FLDA='%s' AND (JFNO LIKE '%%%s%%' OR JFNO LIKE '%%%s%%') "
                                "AND ORG3='%s' AND FTYP NOT IN ('T','G','N')",
                                pclDepFlda,pclFnr2,pclFnr3,cgHopo);
                        dbg (TRACE, "DEP CODE SHARE SELECTION <%s>", pclSqlSelKey);
                        ilRc = SearchFlight (pclSqlTable, pclDepFields, pclSqlSelKey, FOR_SEARCH, TRUE);
                        strcpy(pclHubAddi,pclFnr2);
                        ilIsCodeShare = TRUE;
                    }

                    if (ilRc == RC_SUCCESS)
                    {
                        dbg (TRACE, "FOUND DEP CONNEX FLIGHT:\nFIELDS <%s>\nDATA <%s>", pclDepFields,pcgDataArea);
                        strcpy(pclDepData,pcgDataArea);
                        (void) get_real_item (pclDepUrno, pcgDataArea, 2);
                        (void) get_real_item (pclTmpData, pcgDataArea, 4); /* STOD */
                        (void) GetFullDay (pclTmpData, &ilTmpValue, &ilDepStodMin, &ilTmpValue);
                        (void) get_real_item (pclTmpData, pcgDataArea, 5); /* TIFD */
                        (void) GetFullDay (pclTmpData, &ilTmpValue, &ilDepTifdMin, &ilTmpValue);

                        ilHubCtis = ilDepStodMin - ilArrStoaMin;
                        if (ilHubCtis > 86400)
                        {
                            ilHubCtis = 86401;
                        }

                        ilHubCtit = ilDepTifdMin - ilArrTifaMin;
                        if (ilHubCtit > 86400)
                        {
                            ilHubCtit = 86401;
                        }
                        sprintf(pclConData," ,%d,%d",ilHubCtis,ilHubCtit);

                        strcpy (pclSqlTable, "CFITAB");
                        if (ilIsCodeShare == FALSE)
                        {
                            sprintf(pclSqlSelKey,"WHERE AURN=%s AND DURN=%s AND ADDI=' '", pclArrUrno,pclDepUrno);
                        }
                        else
                        {
                            sprintf(pclSqlSelKey,"WHERE AURN=%s AND DURN=%s AND ADDI='%s'", pclArrUrno,pclDepUrno,pclFnr2);
                        }
                        dbg (TRACE, "HUB RECORD SELECTION <%s>", pclSqlSelKey);

                        ilRc = SearchFlight (pclSqlTable, pclHubChkFields, pclSqlSelKey, FOR_SEARCH, TRUE);
                        if (ilRc == RC_SUCCESS)
                        {
                            dbg (TRACE, "FOUND HUB CONNEX RECORD:\nFIELDS <%s>\nDATA <%s>", pclHubChkFields,pcgDataArea);
                            strcpy(pclHubChkData,pcgDataArea);
                            (void) get_real_item (pclHubUrno, pcgDataArea, 1);
                            sprintf(pclSysData,"%s, , , , , ,%s,PAX",pclHubUrno,cgHopo);
                            sprintf(pclHubAllFields,"%s,%s,%s,%s,%s,ADDI",
                                    pclSysFields,pclHubArrFields,pclPaxFields,pclConFields,pclHubDepFields);
                            sprintf(pclHubAllData,"%s,%s,%s,%s,%s,%s",
                                    pclSysData,pclArrData,pclPaxData,pclConData,pclDepData,pclHubAddi);
                            dbg(TRACE,"UPDATE HUB CONNEX RECORD:\nFLD: <%s>\nDAT: <%s>",pclHubAllFields,pclHubAllData);

                            ilRc = BuildSqlFldStrg (pcgTmpBuf1, pclHubAllFields, FOR_UPDATE);
                            if (ilIsCodeShare == FALSE)
                            {
                                sprintf (pcgSqlBuf, "UPDATE CFITAB SET %s WHERE AURN=%s AND DURN=%s AND ADDI=' '",
                                         pcgTmpBuf1, pclArrUrno,pclDepUrno);
                            }
                            else
                            {
                                sprintf (pcgSqlBuf, "UPDATE CFITAB SET %s WHERE AURN=%s AND DURN=%s AND ADDI='%s'",
                                         pcgTmpBuf1, pclArrUrno,pclDepUrno,pclFnr2);
                            }
                            strcpy (pcgDataArea, pclHubAllData);
                            dbg(TRACE,"\n<%s>",pcgSqlBuf);
                            dbg(TRACE,"\n<%s>",pcgDataArea);
                            delton (pcgDataArea);
                            slUpdCursor = 0;

                            ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
                            if (ilRc == DB_SUCCESS)
                            {
                                commit_work ();
                            }
                            else
                            {
                                dbg (TRACE, "ORA UPDATE FAILED. ROLLBACK!");
                                rollback ();
                            }
                            close_my_cursor (&slUpdCursor);
                        }

                        else
                        {
                            dbg (TRACE, "HUB CONNEX RECORD NOT FOUND. INSERT A NEW ONE.");
                            /* We must insert a new record */
                            ilRc = GetNextValues (pclHubUrno, 1);
                            dbg(TRACE,"NEW HUB URNO = <%s>",pclHubUrno);
                            /* pclSysFields[] = "URNO,PRFL,CDAT,USEC,LSTU,USEU,HOPO,TYPE" */
                            sprintf(pclSysData,"%s, , , , , ,%s,PAX",pclHubUrno,cgHopo);
                            sprintf(pclHubAllFields,"%s,%s,%s,%s,%s,ADDI",
                                    pclSysFields,pclHubArrFields,pclPaxFields,pclConFields,pclHubDepFields);
                            sprintf(pclHubAllData,"%s,%s,%s,%s,%s,%s",
                                    pclSysData,pclArrData,pclPaxData,pclConData,pclDepData,pclHubAddi);
                            dbg(TRACE,"NEW HUB CONNEX RECORD:\nFLD: <%s>\nDAT: <%s>",pclHubAllFields,pclHubAllData);

                            ilRc = BuildSqlFldStrg (pcgTmpBuf1, pclHubAllFields, FOR_INSERT);
                            sprintf (pcgSqlBuf, "INSERT INTO CFITAB %s", pcgTmpBuf1);
                            strcpy (pcgDataArea, pclHubAllData);
                            dbg(TRACE,"\n<%s>",pcgSqlBuf);
                            dbg(TRACE,"\n<%s>",pcgDataArea);
                            delton (pcgDataArea);
                            slInsCursor = 0;

                            ilRc = sql_if (IGNORE, &slInsCursor, pcgSqlBuf, pcgDataArea);
                            if (ilRc == DB_SUCCESS)
                            {
                                commit_work ();
                            }
                            else
                            {
                                dbg (TRACE, "ORA INSERT FAILED. ROLLBACK!");
                                rollback ();
                            }
                            close_my_cursor (&slInsCursor);
                        }
                    }

                    else
                    {
                        dbg (TRACE, "DEP CONNEX FLIGHT NOT FOUND. INSERT DUMMY RECORD.");
                        /* We'll insert a dummy record to save the message */
                        strcpy(pclDepUrno,"0");
                        /* pclDepFields[] = "FTYP,URNO,FLNO,STOD,TIFD,PSTD,GTD1,DES3,VIA3,REGN"; */
                        ilRc = GetNextValues (pclHubUrno, 1);
                        dbg(TRACE,"NEW HUB URNO = <%s>",pclHubUrno);
                        sprintf(pclSysData,"%s, , , , , ,%s,PAX",pclHubUrno,cgHopo);
                        sprintf(pclDepData," ,0,%s, , , , , , , ",pclDepFlno);
                        strcpy(pclHubAddi,"Not Found");
                        strcpy(pclConData," ,0,0");
                        sprintf(pclHubAllFields,"%s,%s,%s,%s,%s,ADDI",
                                pclSysFields,pclHubArrFields,pclPaxFields,pclConFields,pclHubDepFields);
                        sprintf(pclHubAllData,"%s,%s,%s,%s,%s,%s",
                                pclSysData,pclArrData,pclPaxData,pclConData,pclDepData,pclHubAddi);
                        dbg(TRACE,"NEW HUB CONNEX RECORD:\nFLD: <%s>\nDAT: <%s>",pclHubAllFields,pclHubAllData);

                        ilRc = BuildSqlFldStrg (pcgTmpBuf1, pclHubAllFields, FOR_INSERT);
                        sprintf (pcgSqlBuf, "INSERT INTO CFITAB %s", pcgTmpBuf1);
                        strcpy (pcgDataArea, pclHubAllData);
                        dbg(TRACE,"\n<%s>",pcgSqlBuf);
                        dbg(TRACE,"\n<%s>",pcgDataArea);
                        delton (pcgDataArea);
                        slInsCursor = 0;

                        ilRc = sql_if (IGNORE, &slInsCursor, pcgSqlBuf, pcgDataArea);
                        if (ilRc == DB_SUCCESS)
                        {
                            commit_work ();
                        }
                        else
                        {
                            dbg (TRACE, "ORA INSERT FAILED. ROLLBACK!");
                            rollback ();
                        }
                        close_my_cursor (&slInsCursor);
                    }

                } /* end if */
            } /* end while */
            close_my_cursor (&slCfdCursor);

            if (igCheckTifaTifdTimes == TRUE)
            {
                sprintf (pcgSqlBuf, "UPDATE CFITAB SET SCID='X' WHERE AURN=%s AND PRFL<>'D'"
                         " AND ((CTMS>%d AND CTMS<%d) OR (CTMT>%d AND CTMT<%d))",
                         pclArrUrno,igMinShortConnexTime,igMaxShortConnexTime,
                         igMinShortConnexTime,igMaxShortConnexTime);
            }
            else
            {
                sprintf (pcgSqlBuf, "UPDATE CFITAB SET SCID='X' WHERE AURN=%s AND PRFL<>'D'"
                         " AND (CTMS>%d AND CTMS<%d)",
                         pclArrUrno,igMinShortConnexTime,igMaxShortConnexTime);
            }
            strcpy (pcgDataArea, "");
            dbg(TRACE,"\n<%s>",pcgSqlBuf);
            slUpdCursor = 0;

            ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
            if (ilRc == DB_SUCCESS)
            {
                commit_work ();
            }
            close_my_cursor (&slUpdCursor);

            if (igCheckTifaTifdTimes == TRUE)
            {
                sprintf (pcgSqlBuf, "UPDATE CFITAB SET SCID='C' WHERE AURN=%s AND PRFL<>'D'"
                         " AND (CTMS<=%d OR CTMT<=%d)",
                         pclArrUrno,igMinShortConnexTime,igMinShortConnexTime);
            }
            else
            {
                sprintf (pcgSqlBuf, "UPDATE CFITAB SET SCID='C' WHERE AURN=%s AND PRFL<>'D' AND CTMS<=%d",
                         pclArrUrno,igMinShortConnexTime);
            }
            strcpy (pcgDataArea, "");
            dbg(TRACE,"\n<%s>",pcgSqlBuf);
            slUpdCursor = 0;

            ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
            if (ilRc == DB_SUCCESS)
            {
                commit_work ();
            }
            close_my_cursor (&slUpdCursor);

            (void) SendFlightConnexTriggers(pclArrUrno, 1);

            sprintf (pcgSqlBuf, "DELETE FROM CFITAB WHERE AURN=%s AND PRFL='D'", pclArrUrno);
            strcpy (pcgDataArea, "");
            dbg(TRACE,"\n<%s>",pcgSqlBuf);
            slUpdCursor = 0;

            ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
            if (ilRc == DB_SUCCESS)
            {
                commit_work ();
            }
            close_my_cursor (&slUpdCursor);
        }
        else
        {
            dbg (TRACE, "OOPS. ARR CONNEX FLIGHT NOT FOUND!");
        }
    }
    else
    {
        dbg(TRACE,"---> CONNEX INFO OF DEPARTURE FLIGHTS NOT HANDLED.");
    }
    return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int HandlePtmUpdates(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    int ilLen = 0;
    int ilSkedMinNew = 0;
    int ilSkedMinOld = 0;
    int ilSkedMinDif = 0;
    int ilTifrMinNew = 0;
    int ilTifrMinOld = 0;
    int ilTifrMinDif = 0;
    int ilDummy = 0;
    int ilDoUpdate = FALSE;
    int ilCheckScid = FALSE;
    short slUpdCursor = 0;
    char pclAftUrno[16];
    char pclAftAdid[16];
    char pclAftFtypNew[16];
    char pclAftFtypOld[16];
    char pclAftSkedNew[16];
    char pclAftSkedOld[16];
    char pclAftTifrNew[16];
    char pclAftTifrOld[16];
    char pclAftFldVal[32];
    char pclHubSelKey[128];
    char pclHubFldUpdates[256];
    char pclTmpData[128];
    char *pclOldData = NULL;
    char pclDummy[] = "#";

    pclOldData = strstr(pcpData,"\n");
    if (pclOldData != NULL)
    {
        *pclOldData = '\0';
        pclOldData++;
    }
    else
    {
        pclOldData = pclDummy;
    }

    dbg(TRACE,"===== DATA ELEMENTS FROM QUEUE ==========");
    dbg(TRACE,"%s TABLE  <%s>", pcpCmd, pcpTable);
    dbg(TRACE,"%s FIELDS <%s>", pcpCmd, pcpFields);
    dbg(TRACE,"%s NEW DATA <%s>", pcpCmd, pcpData);
    dbg(TRACE,"%s OLD DATA <%s>", pcpCmd, pclOldData);
    dbg(TRACE,"%s SELECT <%s>", pcpCmd, pcpSelKey);
    dbg(TRACE,"=========================================");

    ilLen = GetFieldValue(pclAftFtypOld,pcpFields,"FTYP",pclOldData);
    ilLen = GetFieldValue(pclAftFtypNew,pcpFields,"FTYP",pcpData);
    if (ilLen == 1 && pclAftFtypNew[0] != 'T' && pclAftFtypNew[0] != 'G')
    {
        ilLen = GetFieldValue(pclAftUrno,pcpFields,"URNO",pcpData);
        ilLen = GetFieldValue(pclAftAdid,pcpFields,"ADID",pcpData);
        if (ilLen == 1)
        {
            ilDoUpdate = FALSE;
            pclHubFldUpdates[0] = '\0';
            if (pclAftAdid[0] == 'A')
            {
                /* This works because STOA and TIFA are always filled! */
                sprintf(pclHubSelKey,"AURN=%s",pclAftUrno);
                ilLen = GetFieldValue(pclAftSkedNew,pcpFields,"STOA",pcpData);
                ilLen += GetFieldValue(pclAftSkedOld,pcpFields,"STOA",pclOldData);
                if (ilLen > 0)
                {
                    (void) GetFullDay (pclAftSkedNew, &ilDummy, &ilSkedMinNew, &ilDummy);
                    (void) GetFullDay (pclAftSkedOld, &ilDummy, &ilSkedMinOld, &ilDummy);
                    ilSkedMinDif = ilSkedMinOld - ilSkedMinNew;
                    if (ilSkedMinDif != 0)
                    {
                        if (ilDoUpdate == TRUE)
                        {
                            strcat(pclHubFldUpdates,",");
                        }
                        sprintf(pclTmpData,"STOA='%s',CTMS=CTMS+(%d)",pclAftSkedNew,ilSkedMinDif);
                        strcat(pclHubFldUpdates,pclTmpData);
                        ilDoUpdate = TRUE;
                    }
                }
                ilLen = GetFieldValue(pclAftTifrNew,pcpFields,"TIFA",pcpData);
                ilLen += GetFieldValue(pclAftTifrOld,pcpFields,"TIFA",pclOldData);
                if (ilLen > 0)
                {
                    (void) GetFullDay (pclAftTifrNew, &ilDummy, &ilTifrMinNew, &ilDummy);
                    (void) GetFullDay (pclAftTifrOld, &ilDummy, &ilTifrMinOld, &ilDummy);
                    ilTifrMinDif = ilTifrMinOld - ilTifrMinNew;
                    if (ilTifrMinDif != 0)
                    {
                        if (ilDoUpdate == TRUE)
                        {
                            strcat(pclHubFldUpdates,",");
                        }
                        sprintf(pclTmpData,"TIFA='%s',CTMT=CTMT+(%d)",pclAftTifrNew,ilTifrMinDif);
                        strcat(pclHubFldUpdates,pclTmpData);
                        ilDoUpdate = TRUE;
                    }
                }
                ilCheckScid = FALSE;
                if (ilDoUpdate == TRUE)
                {
                    strcat(pclHubFldUpdates,",");
                    strcat(pclHubFldUpdates,"SCID=' '");
                    ilCheckScid = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"PSTA",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"PSTA='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"GTA1",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"GTA1='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                if (pclAftFtypNew[0] != pclAftFtypOld[0])
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"TYPA='%s'",pclAftFtypNew);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
            }
            else if (pclAftAdid[0] == 'D')
            {
                /* This works because STOD and TIFD are always filled! */
                sprintf(pclHubSelKey,"DURN=%s",pclAftUrno);
                ilLen = GetFieldValue(pclAftSkedNew,pcpFields,"STOD",pcpData);
                ilLen += GetFieldValue(pclAftSkedOld,pcpFields,"STOD",pclOldData);
                if (ilLen > 0)
                {
                    (void) GetFullDay (pclAftSkedNew, &ilDummy, &ilSkedMinNew, &ilDummy);
                    (void) GetFullDay (pclAftSkedOld, &ilDummy, &ilSkedMinOld, &ilDummy);
                    ilSkedMinDif = ilSkedMinNew - ilSkedMinOld;
                    if (ilSkedMinDif != 0)
                    {
                        if (ilDoUpdate == TRUE)
                        {
                            strcat(pclHubFldUpdates,",");
                        }
                        sprintf(pclTmpData,"STOD='%s',CTMS=CTMS+(%d)",pclAftSkedNew,ilSkedMinDif);
                        strcat(pclHubFldUpdates,pclTmpData);
                        ilDoUpdate = TRUE;
                    }
                }
                ilLen = GetFieldValue(pclAftTifrNew,pcpFields,"TIFD",pcpData);
                ilLen += GetFieldValue(pclAftTifrOld,pcpFields,"TIFD",pclOldData);
                if (ilLen > 0)
                {
                    (void) GetFullDay (pclAftTifrNew, &ilDummy, &ilTifrMinNew, &ilDummy);
                    (void) GetFullDay (pclAftTifrOld, &ilDummy, &ilTifrMinOld, &ilDummy);
                    ilTifrMinDif = ilTifrMinNew - ilTifrMinOld;
                    if (ilTifrMinDif != 0)
                    {
                        if (ilDoUpdate == TRUE)
                        {
                            strcat(pclHubFldUpdates,",");
                        }
                        sprintf(pclTmpData,"TIFD='%s',CTMT=CTMT+(%d)",pclAftTifrNew,ilTifrMinDif);
                        strcat(pclHubFldUpdates,pclTmpData);
                        ilDoUpdate = TRUE;
                    }
                }
                ilCheckScid = FALSE;
                if (ilDoUpdate == TRUE)
                {
                    strcat(pclHubFldUpdates,",");
                    strcat(pclHubFldUpdates,"SCID=' '");
                    ilCheckScid = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"PSTD",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"PSTD='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"GTD1",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"GTD1='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"DES3",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"DES3='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"VIA3",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"VIA3='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                ilLen = GetFieldValue(pclAftFldVal,pcpFields,"REGN",pcpData);
                if (ilLen >= 0)
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"REGN='%s'",pclAftFldVal);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
                if (pclAftFtypNew[0] != pclAftFtypOld[0])
                {
                    if (ilDoUpdate == TRUE)
                    {
                        strcat(pclHubFldUpdates,",");
                    }
                    sprintf(pclTmpData,"TYPD='%s'",pclAftFtypNew);
                    strcat(pclHubFldUpdates,pclTmpData);
                    ilDoUpdate = TRUE;
                }
            }
            if (ilDoUpdate == TRUE)
            {
                sprintf(pcgSqlBuf, "UPDATE CFITAB SET %s WHERE %s AND STOD<>' '", pclHubFldUpdates,pclHubSelKey);
                dbg(TRACE,"<%s>",pcgSqlBuf);
                pcgDataArea[0] = '\0';
                slUpdCursor = 0;
                ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
                if (ilRc == DB_SUCCESS)
                {
                    commit_work ();
                }
                else
                {
                    dbg (TRACE, "ORA UPDATE FAILED. ROLLBACK!");
                    rollback ();
                }
                close_my_cursor (&slUpdCursor);

                if (ilCheckScid == TRUE)
                {
                    if (igCheckTifaTifdTimes == TRUE)
                    {
                        sprintf (pcgSqlBuf,
                                 "UPDATE CFITAB SET SCID='X' WHERE %s AND ((CTMS>%d AND CTMS<%d) OR (CTMT>%d AND CTMT<%d)) AND STOD<>' '",
                                 pclHubSelKey,igMinShortConnexTime,igMaxShortConnexTime,igMinShortConnexTime,igMaxShortConnexTime);
                    }
                    else
                    {
                        sprintf (pcgSqlBuf,
                                 "UPDATE CFITAB SET SCID='X' WHERE %s AND (CTMS>%d AND CTMS<%d) AND STOD<>' '",
                                 pclHubSelKey,igMinShortConnexTime,igMaxShortConnexTime);
                    }
                    dbg(TRACE,"<%s>",pcgSqlBuf);
                    pcgDataArea[0] = '\0';
                    slUpdCursor = 0;
                    ilRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
                    if (ilRc == DB_SUCCESS)
                    {
                        commit_work ();
                    }
                    close_my_cursor (&slUpdCursor);
                    if (pclAftAdid[0] == 'A')
                    {
                        (void) SendFlightConnexTriggers(pclAftUrno, 1);
                    }
                    else if (pclAftAdid[0] == 'D')
                    {
                        (void) SendFlightConnexTriggers(pclAftUrno, 2);
                    }
                }
            }
        }
    }

    ilRc = RC_SUCCESS;

    return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int SendFlightConnexTriggers(char *pcpAftUrno, int ipForWhat)
{
    int ilRc = RC_SUCCESS;
    int ilGetRc = DB_SUCCESS;
    short slCursor1 = 0;
    short slCursor2 = 0;
    short slFkt1 = START;
    short slFkt2 = START;
    char pclBAA1[8];
    char pclBAA2[8];
    char pclBAA3[8];
    char pclOutFields[] = "BAA2,BAA3";
    char pclOutSqlKey[128];
    char pclOutData[128];
    char pclSqlBuf[128];
    char pclSqlDat[128];
    int X = 0;

    /* We don't need this feature */
    /* So return from function */
    if (X == 1)
    {
        return ilRc;
    }

    if (ipForWhat == 1)
    {
        /* Driving Flight is Arrival */
        sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE AURN=%s AND SCID=' ' AND PRFL<>'D'",pcpAftUrno);
        slFkt1 = START;
        slCursor1 = 0;
        pcgDataArea[0] = '\0';
        ilGetRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
        if (ilGetRc != DB_SUCCESS)
        {
            strcpy(pcgDataArea,"0");
        }
        close_my_cursor (&slCursor1);
        if (strcmp(pcgDataArea,"0") == 0)
        {
            strcpy(pcgDataArea," ");
        }
        strcpy(pclBAA2,pcgDataArea);
        sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE AURN=%s AND SCID='X'",pcpAftUrno);
        slFkt1 = START;
        slCursor1 = 0;
        pcgDataArea[0] = '\0';
        ilGetRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
        if (ilGetRc != DB_SUCCESS)
        {
            strcpy(pcgDataArea,"0");
        }
        close_my_cursor (&slCursor1);
        if (strcmp(pcgDataArea,"0") == 0)
        {
            strcpy(pcgDataArea," ");
        }
        strcpy(pclBAA3,pcgDataArea);
        sprintf(pclOutData,"%s,%s",pclBAA2,pclBAA3);
        sprintf(pclOutSqlKey,"WHERE URNO=%s",pcpAftUrno);
        (void) tools_send_info_flag (7800, 0, pcgDestName, "", pcgRecvName, "", "",
                                     pcgTwStart, pcgTwEnd, "UFR",
                                     "AFTTAB", pclOutSqlKey, pclOutFields, pclOutData, NETOUT_NO_ACK);

        sprintf (pclSqlBuf, "SELECT DURN FROM CFITAB WHERE AURN=%s AND DURN>0",pcpAftUrno);
        slFkt2 = START;
        slCursor2 = 0;
        ilGetRc = DB_SUCCESS;
        while (ilGetRc == DB_SUCCESS)
        {
            pclSqlDat[0] = '\0';
            ilGetRc = sql_if (slFkt2, &slCursor2, pclSqlBuf, pclSqlDat);
            if (ilGetRc == DB_SUCCESS)
            {
                sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE DURN=%s AND SCID=' ' AND PRFL<>'D'",pclSqlDat);
                slFkt1 = START;
                slCursor1 = 0;
                pcgDataArea[0] = '\0';
                ilRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
                if (ilRc != DB_SUCCESS)
                {
                    strcpy(pcgDataArea,"0");
                }
                close_my_cursor (&slCursor1);
                if (strcmp(pcgDataArea,"0") == 0)
                {
                    strcpy(pcgDataArea," ");
                }
                strcpy(pclBAA2,pcgDataArea);
                sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE DURN=%s AND SCID='X'",pclSqlDat);
                slFkt1 = START;
                slCursor1 = 0;
                pcgDataArea[0] = '\0';
                ilRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
                if (ilRc != DB_SUCCESS)
                {
                    strcpy(pcgDataArea,"0");
                }
                close_my_cursor (&slCursor1);
                if (strcmp(pcgDataArea,"0") == 0)
                {
                    strcpy(pcgDataArea," ");
                }
                strcpy(pclBAA3,pcgDataArea);
                sprintf(pclOutData,"%s,%s",pclBAA2,pclBAA3);
                sprintf(pclOutSqlKey,"WHERE URNO=%s",pclSqlDat);
                (void) tools_send_info_flag (7800, 0, pcgDestName, "", pcgRecvName, "", "",
                                             pcgTwStart, pcgTwEnd, "UFR",
                                             "AFTTAB", pclOutSqlKey, pclOutFields, pclOutData, NETOUT_NO_ACK);
            }
            slFkt2 = NEXT;
        } /* end while */
        close_my_cursor (&slCursor2);
    }
    else
    {
        /* Driving Flight is Departure */
        sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE DURN=%s AND SCID=' ' AND PRFL<>'D'",pcpAftUrno);
        slFkt1 = START;
        slCursor1 = 0;
        pcgDataArea[0] = '\0';
        ilGetRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
        if (ilGetRc != DB_SUCCESS)
        {
            strcpy(pcgDataArea,"0");
        }
        close_my_cursor (&slCursor1);
        if (strcmp(pcgDataArea,"0") == 0)
        {
            strcpy(pcgDataArea," ");
        }
        strcpy(pclBAA2,pcgDataArea);
        sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE DURN=%s AND SCID='X'",pcpAftUrno);
        slFkt1 = START;
        slCursor1 = 0;
        pcgDataArea[0] = '\0';
        ilGetRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
        if (ilGetRc != DB_SUCCESS)
        {
            strcpy(pcgDataArea,"0");
        }
        close_my_cursor (&slCursor1);
        if (strcmp(pcgDataArea,"0") == 0)
        {
            strcpy(pcgDataArea," ");
        }
        strcpy(pclBAA3,pcgDataArea);
        sprintf(pclOutData,"%s,%s",pclBAA2,pclBAA3);
        sprintf(pclOutSqlKey,"WHERE URNO=%s",pcpAftUrno);
        (void) tools_send_info_flag (7800, 0, pcgDestName, "", pcgRecvName, "", "",
                                     pcgTwStart, pcgTwEnd, "UFR",
                                     "AFTTAB", pclOutSqlKey, pclOutFields, pclOutData, NETOUT_NO_ACK);

        sprintf (pclSqlBuf, "SELECT AURN FROM CFITAB WHERE DURN=%s",pcpAftUrno);
        slFkt2 = START;
        slCursor2 = 0;
        ilGetRc = DB_SUCCESS;
        while (ilGetRc == DB_SUCCESS)
        {
            pclSqlDat[0] = '\0';
            ilGetRc = sql_if (slFkt2, &slCursor2, pclSqlBuf, pclSqlDat);
            if (ilGetRc == DB_SUCCESS)
            {
                sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE AURN=%s AND SCID=' ' AND PRFL<>'D'",pclSqlDat);
                slFkt1 = START;
                slCursor1 = 0;
                pcgDataArea[0] = '\0';
                ilRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
                if (ilRc != DB_SUCCESS)
                {
                    strcpy(pcgDataArea,"0");
                }
                close_my_cursor (&slCursor1);
                if (strcmp(pcgDataArea,"0") == 0)
                {
                    strcpy(pcgDataArea," ");
                }
                strcpy(pclBAA2,pcgDataArea);
                sprintf (pcgSqlBuf, "SELECT COUNT(*) FROM CFITAB WHERE AURN=%s AND SCID='X'",pclSqlDat);
                slFkt1 = START;
                slCursor1 = 0;
                pcgDataArea[0] = '\0';
                ilRc = sql_if (slFkt1, &slCursor1, pcgSqlBuf, pcgDataArea);
                if (ilRc != DB_SUCCESS)
                {
                    strcpy(pcgDataArea,"0");
                }
                close_my_cursor (&slCursor1);
                if (strcmp(pcgDataArea,"0") == 0)
                {
                    strcpy(pcgDataArea," ");
                }
                strcpy(pclBAA3,pcgDataArea);
                sprintf(pclOutData,"%s,%s",pclBAA2,pclBAA3);
                sprintf(pclOutSqlKey,"WHERE URNO=%s",pclSqlDat);
                (void) tools_send_info_flag (7800, 0, pcgDestName, "", pcgRecvName, "", "",
                                             pcgTwStart, pcgTwEnd, "UFR",
                                             "AFTTAB", pclOutSqlKey, pclOutFields, pclOutData, NETOUT_NO_ACK);
            }
            slFkt2 = NEXT;
        } /* end while */
        close_my_cursor (&slCursor2);
    }
    ilRc = RC_SUCCESS;
    return ilRc;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst)
{
    int ilLen = -1;
    int ilFldItm = 0;
    ilFldItm = get_item_no (pcpFldLst, pcpFldNam, 5) + 1;
    if (ilFldItm > 0)
    {
        ilLen = get_real_item (pcpFldVal, pcpDatLst, ilFldItm);
        if (ilLen <= 0)
        {
            strcpy (pcpFldVal, " ");
        }
    }
    return ilLen;
}

/********************************************************/
/********************************************************/
static int SearchFlight (char *pcpTable, char *pcpFields, char *pcpWhere, int ipActionType, int ipFirstCall)
{
    int ilRC = RC_SUCCESS;
    int ilGetRc = RC_SUCCESS;
    int ilTbl = 0;
    short slFkt = 0;
    short slLocalCursor = 0;

    sprintf (pcgSqlBuf, "SELECT %s FROM %s %s", pcpFields, pcpTable, pcpWhere);
    dbg(TRACE,"<%s>",pcgSqlBuf);
    slFkt = START;
    slLocalCursor = 0;
    strcpy (pcgDataArea, "");
    ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea);
    close_my_cursor (&slLocalCursor);
    if (ilGetRc == DB_SUCCESS)
    {
        BuildItemBuffer (pcgDataArea, pcpFields, 0, ",");
        ilRC = RC_SUCCESS;
    }                         /* end if */
    else
    {
        strcpy (pcgDataArea, "");
        ilRC = RC_FAIL;
    }                           /* end else */

    return ilRC;
}                               /* end SearchFlight */

static int  get_any_item(char *dest, char *src, int ItemNbr, char *sep)
{
    int rc = -1;
    int gt_rc;
    int b_len = 0;
    int trim = TRUE;
    if (ItemNbr < 0){
        trim = FALSE;
        ItemNbr = - ItemNbr;
    } /* end if */
    gt_rc = get_item(ItemNbr, src, dest, b_len, sep, "\0", "\0");
    if (gt_rc == TRUE){
        if (trim == TRUE){
            str_trm_rgt(dest, " ", TRUE);
        } /* end if */
        rc = strlen(dest);
    }/* end if */
    return rc;
}/* end of get_any_item */

/******************************************************************************/
/* The TrimRight routine                                                      */
/******************************************************************************/
static void TrimRight (char *pcpBuffer)
{
    char *pclBlank = &pcpBuffer[strlen (pcpBuffer) - 1];

    if (strlen (pcpBuffer) == 0)
    {
        strcpy (pcpBuffer, " ");
    }
    else
    {
        while (isspace (*pclBlank) && pclBlank != pcpBuffer)
        {
            *pclBlank = '\0';
            pclBlank--;
        }
    }
}                               /* end of TrimRight */

/***********************************************************************************************************/   
/***********************************************************************************************************/   
static int BuildFlightNbr (char *fnr3, char *fnr2, char *alc3, char *alc2, char *flnr, char *flsf, int mth)
{
    int rc = RC_SUCCESS;
    int fnrLen;
    int i;
    int cp;
    int dp;
    char zeros[] = "0000";
    char tmpFlnr[16];
    char pclTmpTable[16];

    strcpy(pclTmpTable, "ALTTAB");
    fnrLen = strlen (fnr3);
    if (fnrLen == 0)
    {
        sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
    }                             /* end if */
    /* dbg(DEBUG,"BUILD FLNO <%s>",fnr3); */
    fnrLen = strlen (fnr3);
    fnr2[0] = 0x00;
    alc3[0] = 0x00;
    alc2[0] = 0x00;
    flnr[0] = 0x00;
    flsf[0] = 0x00;

    if (fnrLen > 2)
    {
        cp = 2;
        if ((fnr3[cp] >= 'A') && (fnr3[cp] <= 'Z'))
        {
            cp = 3;
        }                           /* end if */
        for (i = 0; i < cp; i++)
        {
            alc3[i] = fnr3[i];
        }                           /* end for */
        alc3[cp] = 0x00;

        while ((cp < fnrLen) && ((fnr3[cp] == ' ') || (fnr3[cp] == '0')))
        {
            cp++;
        }                           /* end while */
        dp = 0;
        while ((cp < fnrLen) && ((fnr3[cp] >= '0') && (fnr3[cp] <= '9')))
        {
            flnr[dp] = fnr3[cp];
            dp++;
            cp++;
            if (dp > 4)
            {
                dp = 4;
                rc = RC_FAIL;
            }                         /* end if */
        }                           /* end while */
        flnr[dp] = 0x00;

        while ((cp < fnrLen) && (fnr3[cp] == ' '))
        {
            cp++;
        }
        if (cp < fnrLen)
        {
            flsf[0] = fnr3[cp];
            flsf[1] = 0x00;
        }                           /* end if */

        if (strlen (flnr) == 0)
        {
            rc = RC_FAIL;
        }                           /* end if */
        /*
           dbg(DEBUG,"1. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
         */
        if (rc == RC_SUCCESS)
        {
            if (strlen (alc3) == 2)
            {
                if (mth != 1)
                {
                    strcpy (alc2, alc3);
                    if (strcmp (alc2, "YY") != 0)
                    {
                        rc = GetBasicData (pclTmpTable, "ALC2", alc2, "ALC3", alc3, 1, RC_FAIL);
                    }                     /* end if */
                }                       /* end if */
                else
                {
                    alc3[2] = 0x20;
                    alc3[3] = 0x00;
                }                       /* end else */
            }                         /* end if */
            else
            {
                if (mth != 1)
                {
                    rc = GetBasicData (pclTmpTable, "ALC3", alc3, "ALC2", alc2, 1, RC_FAIL);
                    if (rc != RC_SUCCESS)
                    {
                        strcpy (alc2, alc3);
                        rc = RC_SUCCESS;
                    }                     /* end if */
                }                       /* end if */
            }                         /* end else */
        }                           /* end if */
        /*
           dbg(DEBUG,"2. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
         */
        if (rc == RC_SUCCESS)
        {
            if (strlen (alc2) == 2)
            {
                alc2[2] = 0x20;
                alc2[3] = 0x00;
            }                         /* end if */
            if (strlen (alc3) == 2)
            {
                alc3[2] = 0x20;
                alc3[3] = 0x00;
            }                         /* end if */
            cp = 3 - strlen (flnr);
            if (cp > 0)
            {
                zeros[cp] = 0x00;
                sprintf (tmpFlnr, "%s%s", zeros, flnr);
                strcpy (flnr, tmpFlnr);
            }                         /* end if */
            /*
               dbg(DEBUG,"3. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
             */
            if (strlen (flnr) > 3)
            {
                sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
                if (mth != 1)
                {
                    sprintf (fnr2, "%s%s%s", alc2, flnr, flsf);
                }                       /* end if */
            }                         /* end if */
            else
            {
                if (strlen (flsf) > 0)
                {
                    sprintf (fnr3, "%s%s %s", alc3, flnr, flsf);
                    if (mth != 1)
                    {
                        sprintf (fnr2, "%s%s %s", alc2, flnr, flsf);
                    }                     /* end if */
                }                       /* end if */
                else
                {
                    sprintf (fnr3, "%s%s", alc3, flnr);
                    if (mth != 1)
                    {
                        sprintf (fnr2, "%s%s", alc2, flnr);
                    }                     /* end if */
                }                       /* end else */
            }                         /* end else */

            alc2[2] = 0x00;

        }                           /* end if */
    }                             /* end if */
    else
    {
        rc = RC_FAIL;
    }                             /* end else */
    return rc;
}                               /* end BuildFlightNbr */

/********************************************************/
/********************************************************/
static int GetBasicData(char *pcpTable,char *pcpFields,char *pcpData,
                        char *pcpResultFields,char *pcpResultData,
                        int ipMaxLines, int ipDefRc)
{
    int ilRC = RC_SUCCESS;
    int ilGetRc = RC_SUCCESS;
    int ilCount = 0;
    int ilUseOra = FALSE;
    int ilItm = 0;
    int ilCnt = 0;
    short slCursor = 0;
    short slFkt = START;
    char *pclNlPos;
    char pclSqlBuf[512];
    char pclSqlKey[512];
    char pclFldNam[8];
    char pclFldDat[32];
    char pclTmp[64];
    pcpResultData[0] = 0x00;
    pcgTmpBuf3[0] = 0x00;

    if (ilRC == RC_SUCCESS)
    {
        ilCount = ipMaxLines;
        if (strcmp(cgTabEnd,"TAB") == 0)
        {
            sprintf(pclSqlBuf,"%s,HOPO",pcpFields);
            sprintf(pclSqlKey,"%s,%s",pcpData,cgHopo);
        } /* end if */
        else
        {
            strcpy(pclSqlBuf,pcpFields);
            strcpy(pclSqlKey,pcpData);
        } /* end else */
        dbg(DEBUG,"SYSLIB: TBL <%s> FLD <%s> DAT <%s>",
            pcpTable,pclSqlBuf,pclSqlKey);
        dbg(DEBUG,"RESULT FIELDS <%s>",pcpResultFields);
        ilRC = syslibSearchDbData(pcpTable,pclSqlBuf,pclSqlKey,
                                  pcpResultFields,pcgTmpBuf3,&ilCount,"\n");
        if (ilRC == RC_SUCCESS)
        {
            dbg(DEBUG,"FOUND = %d ENTRIES", ilCount);
            dbg(DEBUG,":\n%s>",pcgTmpBuf3);
            if ((ipMaxLines < 1) || (ipMaxLines > ilCount))
            {
                ipMaxLines = ilCount;
            } /* end if */
            pclNlPos = pcgTmpBuf3;
            pclNlPos++;
            while (ipMaxLines > 0)
            {
                pclNlPos = strstr(pclNlPos,"\n");
                if (pclNlPos != NULL)
                {
                    ipMaxLines--;
                    pclNlPos++;
                } /* end if */
                else
                {
                    ipMaxLines = 0;
                } /* end else */
            } /* end while */
            if (pclNlPos != NULL)
            {
                pclNlPos--;
                if (*pclNlPos == '\n')
                {
                    *pclNlPos = 0x00;
                    dbg(TRACE,"DATA CUT! \n<%s>",pcgTmpBuf3);
                } /* end if */
            } /* end if */
            strcpy(pcpResultData,pcgTmpBuf3);
        }   /* end if */
        else
        {
            if (ilRC == RC_FAIL)
            {
                ilUseOra = TRUE;
                dbg(TRACE,"=====================================================");
                dbg(TRACE,"ERROR: SHARED MEMORY FAILURE !!");
                dbg(TRACE,"USE TBL <%s>",pcpTable);
                dbg(TRACE,"GET FLD <%s>",pcpResultFields);
                dbg(TRACE,"KEY FLD <%s>",pcpFields);
                dbg(TRACE,"KEY DAT <%s>",pcpData);
                dbg(TRACE,"GET BASIC DATA SWITCHED TO USE OF ORACLE");
                dbg(TRACE,"=====================================================");
            } /* end if */
            if (ilRC == RC_NOT_FOUND)
            {
                ilRC = ipDefRc;
            } /* end if */
        } /* end else */
    }     /* end if */
    else
    {
        ilUseOra = TRUE;
    } /* end else */
    if (ilRC != RC_SUCCESS)
    {
        ilUseOra = TRUE;
    }     /* end if */

    if (ilUseOra == TRUE)
    {
        dbg(TRACE,"GET_BASIC USING ORACLE");
        /* sprintf(pclSqlKey,"WHERE %s='%s'", pcpFields,pcpData); */
        strcpy(pclSqlKey,"WHERE ");
        ilCnt = field_count(pcpFields);
        for (ilItm = 1; ilItm <= ilCnt; ilItm++)
        {
            get_real_item(pclFldNam, pcpFields, ilItm);
            get_real_item(pclFldDat, pcpData, ilItm);
            if (pclFldDat[0] == '\0')
            {
                strcpy(pclFldDat, " ");
            }
            sprintf(pclTmp,"%s='%s'",pclFldNam,pclFldDat);
            if (ilItm > 1)
            {
                strcat(pclSqlKey," AND ");
            }
            strcat(pclSqlKey,pclTmp);
        }
        sprintf(pclSqlBuf,"SELECT %s FROM %s %s",
                pcpResultFields,pcpTable,pclSqlKey);
        dbg(TRACE,"<%s>",pclSqlBuf);
        slFkt = START;
        slCursor = 0;
        ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf,pcpResultData);
        if (ilGetRc == DB_SUCCESS)
        {
            BuildItemBuffer(pcpResultData, pcpResultFields, 0, ",");
            ilRC = RC_SUCCESS;
        } /* end if */
        else
        {
            ilRC = ipDefRc;
        } /* end else */
        close_my_cursor(&slCursor);
        dbg(DEBUG,"RESULT <%s> <%s>",pcpResultFields,pcpResultData);
    } /* end else */
    return ilRC;
} /* end GetBasicData */



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);
	/* dbg(TRACE,"READ CFG <%s>",cgConfigFile); */
    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword, CFG_STRING,pcpCfgBuffer);
    if (ilRC != RC_SUCCESS)
    {
        strcpy(pcpCfgBuffer,pcpDefault);
        dbg(TRACE,"DEFAULT VALUE [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    }
    else
    {
        dbg(TRACE,"CONFIGURATION [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    }/* end of if */
    return ilRC;
}


/********************************************************/
/********************************************************/
static int BuildSqlFldStrg (char *pcpSqlStrg, char *pcpFldLst, int ipTyp)
{
    int ilRC = RC_SUCCESS;
    int ilFldCnt = 0;
    int ilFld = 0;
    int ilStrLen = 0;
    char pclFldNam[8];
    char pclFldVal[16];
    char pclTmpBuf[4096];

    pcpSqlStrg[0] = 0x00;
    pclTmpBuf[0] = 0x00;
    ilFldCnt = field_count (pcpFldLst);
    switch (ipTyp)
    {
    case FOR_UPDATE:
        ilStrLen = 0;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
            (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
            sprintf (pclFldVal, "%s=:V%s", pclFldNam, pclFldNam);
            StrgPutStrg (pcpSqlStrg, &ilStrLen, pclFldVal, 0, -1, ",");
        }
        if (ilStrLen > 0)
        {
            ilStrLen--;
            pcpSqlStrg[ilStrLen] = 0x00;
        }
        break;

    case FOR_INSERT:
        ilStrLen = 0;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
            (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
            sprintf (pclFldVal, ":V%s", pclFldNam);
            StrgPutStrg (pclTmpBuf, &ilStrLen, pclFldVal, 0, -1, ",");
        }
        if (ilStrLen > 0)
        {
            ilStrLen--;
            pclTmpBuf[ilStrLen] = 0x00;
            sprintf (pcpSqlStrg, "(%s) VALUES (%s)", pcpFldLst, pclTmpBuf);
        }
        break;

    default:
        break;
    }
    return ilRC;
}

/* ************************************************************************* */
/* ************************************************************************* */
static int ReadEkViewFltList(char *pcpFltKey, char *pcpFltDay)
{
  int ilRc = RC_SUCCESS;
  short slRawCursor = 0;
  short slRawFkt = START;
  int ilRawGetRc = RC_SUCCESS;
  char pclSqlTab[64] = "";
  char pclSqlBuf[4096] = "";
  char pclSqlKey[4096] = "";
  char pclSqlFld[4096] = "";
  char pclSqlDat[4096] = "";
  char pclRawFltn[64] = "";
  char pclRawDate[64] = "";

  /*
  sprintf(pclSqlBuf,"SELECT DISTINCT(FLIGHTNUMBER||'/'||TO_CHAR(SCHEDULEDDATE,'YYYYMMDD')) FROM %s where SCHEDULEDDATE between sysdate%s and sysdate%s",
                    pcgRawTabRead,pcgFltWinVpfrStrg,pcgFltWinVptoStrg);
  */

  strcpy(pcgFltWinVpfrStrg,"");
  if (igFltWinVpfrDays > 0)
  {
    sprintf(pcgFltWinVpfrStrg,"+%d",igFltWinVpfrDays);
  }
  if (igFltWinVpfrDays < 0)
  {
    sprintf(pcgFltWinVpfrStrg,"%d",igFltWinVpfrDays);
  }
  strcpy(pcgFltWinVptoStrg,"");
  if (igFltWinVptoDays > 0)
  {
    sprintf(pcgFltWinVptoStrg,"+%d",igFltWinVptoDays);
  }
  if (igFltWinVptoDays < 0)
  {
    sprintf(pcgFltWinVptoStrg,"%d",igFltWinVptoDays);
  }
  sprintf(pclSqlBuf,"SELECT DISTINCT(FLIGHTNUMBER||'/'||TO_CHAR(SCHEDULEDDATE,'YYYYMMDD')) FROM %s WHERE SCHEDULEDDATE BETWEEN SYSDATE%s AND SYSDATE%s AND ARRIVALDEPARTUREFLAG='A'",
                     pcgRawTabRead,pcgFltWinVpfrStrg,pcgFltWinVptoStrg);

  dbg(TRACE,"<%s>",pclSqlBuf);
  igFltCnt = 0;
  igConCnt = 0;

	slRawCursor = 0;
	slRawFkt = START;
	ilRawGetRc = RC_SUCCESS;

	dbg(TRACE,"============== READ FLIGHTS START ===================");
	while (ilRawGetRc == RC_SUCCESS)
	{
		ilRawGetRc = sql_if(slRawFkt, &slRawCursor, pclSqlBuf, pclSqlDat);
		slRawFkt = NEXT;
		if (ilRawGetRc == RC_SUCCESS)
		{
			igFltCnt++;
			BuildItemBuffer (pclSqlDat, "", 1, ",");
			TAB_TrimRecordData(pclSqlDat, ",");
			dbg(TRACE,"<%s>",pclSqlDat);
			get_any_item(pclRawFltn, pclSqlDat, 1, "/");
			get_any_item(pclRawDate, pclSqlDat, 2, "/");
			dbg(TRACE,"============== READ DETAILS START ===================");
			ilRc = ReadFromEkView(pclRawFltn, pclRawDate);
			dbg(TRACE,"============== READ DETAILS READY ===================");
		}
	} /* end while */
	close_my_cursor (&slRawCursor);
	dbg(TRACE,"HANDLED %d FLIGHTS WITH %d CONNECTIONS",igFltCnt,igConCnt);
	dbg(TRACE,"============== READ FLIGHTS READY ===================");

  return ilRc;
}


/* ************************************************************************* */
/* ************************************************************************* */
static int ReadFromEkView(char *pcpRawFltn, char *pcpFltDay)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  short slRawCursor = 0;
  short slRawFkt = START;
  int ilRawGetRc = RC_SUCCESS;
  int ilDbChgFlag = FALSE;
  long llRawLine = 0;
  long llCurLine = 0;
  long llMaxLine = 0;
  char pclClass[4] = "";
  char pclKeyDat[64] = "";
  char pclKeyLst[64] = "";
  char pclSqlTab[64] = "";
  char pclSqlBuf[4096] = "";
  char pclSqlKey[4096] = "";
  char pclSqlFld[4096] = "";
  char pclSqlDat[4096] = "";
  char pclCfdCond[128] = "";
  char pclCfdMKey[68] = "";
  char pclCurKey[68] = "";
  char pclPrvKey[68] = "";
  char pclAftTable[16] = "AFTTAB";
  char pclAftFltn[16] = "";
  char pclAftFields[128] = "";
  char pclAftSelKey[128] = "";
  char pclAftArrData[1024] = "";
  char pclTmpData[128] = "";

  char pclAftArrFlno[16] = "";
  char pclAftArrJfno[16] = "";
  char pclFnr3[16] = "";
  char pclFnr2[16] = "";
  char pclAlc3[16] = "";
  char pclAlc2[16] = "";
  char pclFnbr[16] = "";
  char pclFlsf[16] = "";


  strcpy(pclAftFields,"URNO,FLNO,STOA");
  sprintf(pclAftArrFlno,"EK%s",pcpRawFltn);
  strcpy (pclFnr3, pclAftArrFlno);
  ilGetRc = BuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFnbr, pclFlsf, 0);
  dbg (TRACE, "--- FLNO ITEMS OF <%s> ----", pclAftArrFlno);
  dbg (TRACE, "FNR3 <%s>", pclFnr3);
  dbg (TRACE, "FNR2 <%s>", pclFnr2);
  dbg (TRACE, "ALC3 <%s>", pclAlc3);
  dbg (TRACE, "ALC2 <%s>", pclAlc2);
  dbg (TRACE, "FLTN <%s>", pclFnbr);
  dbg (TRACE, "FLNS <%s>", pclFlsf);
  dbg (TRACE, "---------------------------------");
  
  sprintf(pclAftSelKey,"WHERE FLTN='%s' AND FLDA='%s' AND ALC2='EK'",pclFnbr,pcpFltDay);
  ilRc = SearchFlight (pclAftTable, pclAftFields, pclAftSelKey, FOR_SEARCH, TRUE);
  if (ilRc == RC_SUCCESS)
  {
    dbg (TRACE, "\nFOUND ARR CONNEX FLIGHT:\nFIELDS <%s>\nVALUES <%s>", pclAftFields,pcgDataArea);
    strcpy(pcgCurAftArrFields,pclAftFields);
    strcpy(pcgCurAftArrData,pcgDataArea);
  }
  else
  {
    dbg(TRACE,"FLIGHT NOT FOUND");
    return ilRc;
  }

  dbg(TRACE,"----------------------------------------------------------");

  strcpy(pclSqlTab,pcgRawTabRead);
  strcpy(pclSqlFld,pcgRawTabGetName);
  sprintf(pclSqlKey,"WHERE FLIGHTNUMBER='%s' AND ARRIVALDEPARTUREFLAG='A' AND TO_CHAR(SCHEDULEDDATE,'YYYYMMDD')='%s' AND ALN='EK'",pcpRawFltn,pcpFltDay);
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclSqlFld,pclSqlTab,pclSqlKey);
  dbg(TRACE,"<%s>",pclSqlBuf);

	CT_ResetContent(pcgRawTabGrid);

	slRawCursor = 0;
	slRawFkt = START;
	ilRawGetRc = RC_SUCCESS;

	while (ilRawGetRc == RC_SUCCESS)
	{
		ilRawGetRc = sql_if(slRawFkt, &slRawCursor, pclSqlBuf, pclSqlDat);
		slRawFkt = NEXT;
		if (ilRawGetRc == RC_SUCCESS)
		{
			igConCnt++;
			BuildItemBuffer (pclSqlDat, pclSqlFld, 0, ",");
			/*TAB_TrimRecordData(pclSqlDat, ",");*/
			CT_InsertTextLine(pcgRawTabGrid,pcgRawTabNulStrg);
			llRawLine = CT_GetLineCount(pcgRawTabGrid) - 1;
			CT_SetFieldValues(pcgRawTabGrid, llRawLine, pcgRawTabFldList, pclSqlDat);
			/*dbg(TRACE,"<%s>",pclSqlDat);*/
		}
	} /* end while */
	close_my_cursor (&slRawCursor);

    dbg(TRACE,"----------------------------------------------------------");
    llMaxLine = CT_GetLineCount(pcgRawTabGrid);
    dbg(TRACE,"==> <%s/%s> %d RECORDS FETCHED",pcpRawFltn,pcpFltDay,llMaxLine);

	/*static char pcgRawTabFldList[] = "ALCD,FLTN,FDAY,OALC,OFLT,ODAY,OSKD,OORG,OREF,OCLS,SORT";*/
	/*static char pcgRawTabFldSize[] = "3   ,4   ,8   ,5   ,4   ,8   ,8   ,6   ,10  ,2   ,20  ";*/

    CT_GetMultiValues(pcgRawTabGrid, 0, "ALCD,FLTN,FDAY", pclCfdMKey);
    ReplaceChars(pclCfdMKey, ",", "-");
    sprintf(pclCfdCond,"WHERE MKEY='%s'",pclCfdMKey);
    LoadGridData(pcgCfdTabGrid, pcgCfdTabName, pclCfdCond);

	strcpy(pclKeyLst,"OALC,OFLT,ODAY");

    llMaxLine = CT_GetLineCount(pcgRawTabGrid) - 1;
    for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
    {
      CT_GetMultiValues(pcgRawTabGrid, llCurLine, pclKeyLst, pclKeyDat);
      ReplaceChars(pclKeyDat, ",", "-");
	  CT_SetFieldValues(pcgRawTabGrid, llCurLine, "SORT", pclKeyDat);
	}

    CT_SortByColumnNames(pcgRawTabGrid, "OCLS", SORT_ASC);
    CT_SortByColumnNames(pcgRawTabGrid, "SORT", SORT_ASC);

    dbg(TRACE,"----------------------------------------------------------");
    CT_GetFieldList(pcgRawTabGrid, pclSqlFld);
    dbg(TRACE,"FLIGHT EVENTS OF FETCHED EK VIEW SORTED BY CLASS AND FLIGHT");
    dbg(TRACE,"(FIELDS: %s)",pclSqlFld);
    dbg(TRACE,"----------------------------------------------------------");
    llMaxLine = CT_GetLineCount(pcgRawTabGrid) - 1;
    for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
    {
      CT_GetLineValues(pcgRawTabGrid, llCurLine, &rgCtTabLine);
      dbg(TRACE,"<%s>",rgCtTabLine.Value);
    }
    dbg(TRACE,"----------------------------------------------------------");

  InitClassTotals(FALSE, TRUE);


  strcpy(pclPrvKey,"START");
  llMaxLine = CT_GetLineCount(pcgRawTabGrid) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcgRawTabGrid, llCurLine, "SORT", pclCurKey);
    if (strcmp(pclCurKey,pclPrvKey) != 0)
    {
      if (llCurLine > 0)
      {
        MaintainCfdTabLine(pclCfdMKey, pclPrvKey);
        dbg(TRACE,"----------------------------------------------------------");
      }
      strcpy(pclPrvKey,pclCurKey);
    }
    CT_GetMultiValues(pcgRawTabGrid, llCurLine, "OCLS", pclClass);
    CountClassTotals(pclClass);
  }

  if (strcmp(pclPrvKey,"START") != 0)
  {
    MaintainCfdTabLine(pclCfdMKey, pclCurKey);
  }

  ilDbChgFlag = CheckDbAction(pcgCfdTabGrid, pcgCfdTabName, 0);

  if (ilDbChgFlag == TRUE)
  {
    CreatePtmTrigger(pclCfdMKey, pcpFltDay);
  }
  else
  {
    dbg(TRACE,"NOTHING CHANGED. NOTHING ELSE TO DO.");
  }
  
  dbg(TRACE,"----------------------------------------------------------");

  return ilRc;
}

static int CreatePtmTrigger(char *pcpMkey, char *pcpFltDay)
{
  int ilRc = RC_SUCCESS;
  char pclSelKey[1024] = "";
  char pclTbLNam[32] = "";
  char pclMsgFld[128] = "";
  char pclMsgDat[1024] = "";
  char pclArrFlno[32] = "";
  char pclArrAdid[32] = "";
  char pclArrUrno[32] = "";
  char pclArrFlda[32] = "";

  /*
  dbg(TRACE,"GET KEY VALUES FROM HEADER SECTION IN SELECTION STRING");
  (void) CedaGetKeyItem(pclArrFlno, &llTmpLen, pcpSelKey, "FLNO=", ",", TRUE);
  (void) CedaGetKeyItem(pclArrAdid, &llTmpLen, pcpSelKey, "ADID=", ",", TRUE);
  (void) CedaGetKeyItem(pclArrUrno, &llTmpLen, pcpSelKey, "URNO=", ",", TRUE);
  (void) CedaGetKeyItem(pclArrFlda, &llTmpLen, pcpSelKey, "FLDA=", ",", TRUE);
  */
  /*
  strcpy(pclFldName,"RURN,FLNO,STDT");
  strcpy(pclFldData,pcgCurAftArrData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");
  */
  get_any_item(pclArrUrno, pcgCurAftArrData, 1, ",");
  get_any_item(pclArrFlno, pcgCurAftArrData, 2, ",");
  strcpy(pclArrFlda, pcpFltDay);

  sprintf(pclSelKey,"FLNO=%s,ADID=A,URNO=%s,FLDA=%s",pclArrFlno,pclArrUrno,pclArrFlda);

  HandlePtmMessages("CFI", "CFDTAB", pclSelKey, "", "");


  return ilRc;
}

/* ************************************************************************* */
/* ************************************************************************* */
static int MaintainCfdTabLine(char *pcpArrFlt, char *pcpDepFlt)
{
  int ilRc = RC_SUCCESS;
  long llHitLines = 0;
  long llCfdLine = 0;
  dbg(TRACE,"MAINTAIN CFDTAB LINE:");
  dbg(TRACE,"ARR FLIGHT <%s> DEP FLIGHT <%s>",pcpArrFlt,pcpDepFlt);

  llHitLines = CT_GetLinesByColumnValue(pcgCfdTabGrid, "LKEY", pcpDepFlt, &rgCtHtLine, 0);
  if (llHitLines > 0)
  {
    dbg(TRACE,"FOUND %d CFD LINES FOR LKEY <%s> HIT <%s>",llHitLines,pcpDepFlt,rgCtHtLine.Value);
    dbg(TRACE,"UPDATING CFD LINE");
    llCfdLine = atol(rgCtHtLine.Value);
    UpdateCfdTabLine(llCfdLine, pcpArrFlt, pcpDepFlt);
  }
  else
  {
    dbg(TRACE,"FOUND %d CFD LINES FOR LKEY <%s>",llHitLines,pcpDepFlt);
    dbg(TRACE,"INSERTING CFD LINE");
    llCfdLine = InsertCfdTabLine(pcpArrFlt, pcpDepFlt);
  }
  InitClassTotals(FALSE, TRUE);
  return ilRc;
}

/* ************************************************************************* */
/* ************************************************************************* */
static long InsertCfdTabLine(char *pcpArrFlt, char *pcpDepFlt)
{
  int ilRc = RC_SUCCESS;
  long llCfdLine = 0;

  CT_InsertTextLine(pcgCfdTabGrid,pcgCfdTabNulStrg);
  llCfdLine = CT_GetLineCount(pcgCfdTabGrid) - 1;
  dbg(TRACE,"NEW CFD LINE = %d",llCfdLine);

  CT_SetOldData(pcgCfdTabGrid, llCfdLine, pcgCfdTabNulStrg);
  CT_SetLineDbInsFlag(pcgCfdTabGrid, llCfdLine, 1);

  CT_SetMultiValues(pcgCfdTabGrid, llCfdLine, "MKEY", pcpArrFlt);
  CT_SetMultiValues(pcgCfdTabGrid, llCfdLine, "LKEY", pcpDepFlt);

  CreateCfdDataList(pcpArrFlt, pcpDepFlt, pcgCfdFldLst, pcgCfdDatLst);
  CT_SetMultiValues(pcgCfdTabGrid, llCfdLine, pcgCfdFldLst, pcgCfdDatLst);

  CT_SetLineCheckedFlag(pcgCfdTabGrid, llCfdLine, 1);

  return llCfdLine;
}


/* ************************************************************************* */
/* ************************************************************************* */
static long UpdateCfdTabLine(long lpCfdLine, char *pcpArrFlt, char *pcpDepFlt)
{
  int ilRc = RC_SUCCESS; 
  dbg(TRACE,"UPDATING CFD LINE %d",lpCfdLine);

  CreateCfdDataList(pcpArrFlt, pcpDepFlt, pcgCfdFldLst, pcgCfdDatLst);
  CT_SetMultiValues(pcgCfdTabGrid, lpCfdLine, pcgCfdFldLst, pcgCfdDatLst);

  CT_SetLineCheckedFlag(pcgCfdTabGrid, lpCfdLine, 1);

  return lpCfdLine;
}



/* ************************************************************************* */
/* ************************************************************************* */
static int CreateCfdDataList(char *pcpArrFlt, char *pcpDepFlt, char *pcpFldLst, char *pcpDatLst)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  int ilFldPos = 0;
  int ilDatPos = 0;
  char pclFldName[16] = "";
  char pclFldData[4096] = "";


  char pclDepFlno[16] = "";
  char pclDepFlda[16] = "";
  char pclDepAlcd[16] = "";
  char pclDepFltn[16] = "";
  char pclFnr3[16] = "";
  char pclFnr2[16] = "";
  char pclAlc3[16] = "";
  char pclAlc2[16] = "";
  char pclFnbr[16] = "";
  char pclFlsf[16] = "";

  /*EK-0622-20111103*/
  get_any_item(pclDepAlcd, pcpDepFlt, 1, "-");
  get_any_item(pclDepFltn, pcpDepFlt, 2, "-");
  get_any_item(pclDepFlda, pcpDepFlt, 3, "-");

  sprintf(pclDepFlno,"%s%s",pclDepAlcd,pclDepFltn);
  strcpy (pclFnr3, pclDepFlno);
  ilGetRc = BuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFnbr, pclFlsf, 0);
  dbg (TRACE, "--- FLNO ITEMS OF <%s> ----", pclDepFlno);
  dbg (TRACE, "FNR3 <%s>", pclFnr3);
  dbg (TRACE, "FNR2 <%s>", pclFnr2);
  dbg (TRACE, "ALC3 <%s>", pclAlc3);
  dbg (TRACE, "ALC2 <%s>", pclAlc2);
  dbg (TRACE, "FLTN <%s>", pclFnbr);
  dbg (TRACE, "FLNS <%s>", pclFlsf);
  dbg (TRACE, "---------------------------------");
  

  pcpFldLst[0] = 0x00;
  pcpDatLst[0] = 0x00;

  strcpy(pclFldName,"PRFL");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"HOPO");
  strcpy(pclFldData,cgHopo);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  /*strcpy(pclAftFields,"URNO,FLNO,STOA");*/
  /*strcpy(pcgCurAftArrData,pcgDataArea);*/

  strcpy(pclFldName,"RURN,FLNO,STDT");
  strcpy(pclFldData,pcgCurAftArrData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"ADID");
  strcpy(pclFldData,"A");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"CFAD");
  strcpy(pclFldData,"D");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"CFLN");
  strcpy(pclFldData,pclFnr2);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"CFCS");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"CFDT");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"CFDA");
  strcpy(pclFldData,pclDepFlda);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"PAXF");
  GetMainTotals(1,pclFldData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"PAXC");
  GetMainTotals(2,pclFldData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"PAXY");
  GetMainTotals(3,pclFldData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"PAXU");
  GetMainTotals(4,pclFldData);
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"ADDI");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

/*  strcpy(pclFldName,"LCCC");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");

  strcpy(pclFldName,"LOTC");
  strcpy(pclFldData," ");
  StrgPutStrg (pcpFldLst, &ilFldPos, pclFldName, 0, -1, ",");
  StrgPutStrg (pcpDatLst, &ilDatPos, pclFldData, 0, -1, ",");
*/

  if (ilFldPos > 0)
  {
    ilFldPos--;
    pcpFldLst[ilFldPos] = 0x00;
  }
  if (ilDatPos > 0)
  {
    ilDatPos--;
    pcpDatLst[ilDatPos] = 0x00;
  }
  return ilRc;
}

/* ************************************************************************* */
/* ************************************************************************* */
static int TestFunction(int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  char pclTest1[32] = "";
  char pclTest2[32] = "";
  int ilTest1 = 0;
  if (ipForWhat > 0)
  {
    dbg(TRACE,"TEST FUNCTION TYPE %d",ipForWhat);
    /*
    strcpy(pclTest1,"10:25:00");
    dbg(TRACE,"UAE <%s> CEDA <%s> MIN=%d",pclTest1,pclTest2,ilTest1);
    ilTest1 = ChgUaeTimeToMin(pclTest1,pclTest2);
    dbg(TRACE,"UAE <%s> CEDA <%s> MIN=%d",pclTest1,pclTest2,ilTest1);
    */
    /*
    strcpy(pclTest1,"A");  
    ilTest1 = pclTest1[0];
    dbg(TRACE,"<%s> IS INT %d",pclTest1,ilTest1);
    */
  }
  else
  {
    ilRc = ReadEkViewFltList("", "");
  }

  return ilRc;
}













/*
static char pcgRawTabRead[] = "VW_EK_MACS_ONWARD";
static char pcgRawTabSave[] = "RAWTAB";
static char pcgRawTabGrid[] = "RawTab";
static char pcgRawTabGetName[] = "FLIGHTNUMBER,TO_CHAR(SCHEDULEDDATE,'YYYYMMDD'),ONW_AIRLINE_DESIGNATOR,ONW_FLIGHT_NUMBER,TO_CHAR(ONW_OPERATION_DATE,'YYYYMMDD'),ONW_OPERATION_TIME,ONW_BOARD_POINT,REFERENCE_NUMBER,BOOKED_CLASS";
static char pcgRawTabFldList[] = "FLTN,FDAY,OALC,OFLT,ODAY,OSKD,OORG,OREF,OCLS";
static char pcgRawTabFldSize[] = ""
*/

/* *************************************************** */
/* *************************************************** */
int InitEkViewEnvironment(void)
{
  int ilRc = RC_SUCCESS;
  dbg(TRACE,"PREPARING THE FLTLIB GRID ENVIRONMENT");
  InitMyTab("STR_DESC","","");
  dbg(TRACE,"-------------------------------------");
  InitMyTab(pcgRawTabGrid,pcgRawTabFldList,pcgRawTabFldSize);
  dbg(TRACE,"-------------------------------------");
  InitMyTab(pcgCfdTabGrid,pcgCfdTabFldList,pcgCfdTabFldSize);
  CT_SetTrimMethod(pcgCfdTabGrid, "R");
  CT_SetArrayDataTracking(pcgCfdTabGrid, pcgCfdTabName, 1, 1);
  GOR_SetMyTabUrnoPool(pcgCfdTabGrid, pcgCfdTabName);

  InitMyTab(pcgLoaTabGrid,pcgLoaTabFldList,pcgLoaTabFldSize);

  dbg(TRACE,"-------------------------------------");

  InitClassTotals(TRUE, FALSE);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize)
{
  int ilRC = RC_SUCCESS;
  int i = 0;

  if (strcmp(pcpCtTabName,"STR_DESC") != 0)
  {
    CT_CreateArray(pcpCtTabName);
    CT_SetFieldList(pcpCtTabName,pcpCtFldList);
    CT_SetLengthList(pcpCtTabName, pcpCtFldSize);
    CT_SetDataPoolAllocSize(pcpCtTabName, 1000);
    dbg(TRACE,"CT DATA GRID <%s> INITIALIZED",pcpCtTabName);
  }
  else
  {

    for (i=0;i<=MAX_CTFLT_VALUE;i++)
	{
      CT_InitStringDescriptor(&argCtFlValue[i]);
      argCtFlValue[i].Value = malloc(4096);
      argCtFlValue[i].AllocatedSize = 4096;
      argCtFlValue[i].Value[0] = 0x00;
	}

	/* static STR_DESC rgCtTabLine; */
    CT_InitStringDescriptor(&rgCtTabLine);
    rgCtTabLine.Value = malloc(4096);
    rgCtTabLine.AllocatedSize = 4096;
    rgCtTabLine.Value[0] = 0x00;

	/* static STR_DESC rgCtTabData; */
    CT_InitStringDescriptor(&rgCtTabData);
    rgCtTabData.Value = malloc(4096);
    rgCtTabData.AllocatedSize = 4096;
    rgCtTabData.Value[0] = 0x00;



	/* static STR_DESC rgCtFlLine; */
    CT_InitStringDescriptor(&rgCtFlLine);
    rgCtFlLine.Value = malloc(4096);
    rgCtFlLine.AllocatedSize = 4096;
    rgCtFlLine.Value[0] = 0x00;


	/* static STR_DESC rgCtFlData; */
    CT_InitStringDescriptor(&rgCtFlData);
    rgCtFlData.Value = malloc(4096);
    rgCtFlData.AllocatedSize = 4096;
    rgCtFlData.Value[0] = 0x00;

	/* static STR_DESC rgCtHtLine; */
    CT_InitStringDescriptor(&rgCtHtLine);
    rgCtHtLine.Value = malloc(4096);
    rgCtHtLine.AllocatedSize = 4096;
    rgCtHtLine.Value[0] = 0x00;


	/* static STR_DESC rgCtTabList; */
	/* static STR_DESC rgCtTabUrno; */

/*
static STR_DESC rgCtAftLine;
static STR_DESC rgCtAftData;
static STR_DESC rgCtAftList;
static STR_DESC rgCtAftUrno;


static STR_DESC rgCtCfdLine;
static STR_DESC rgCtCfdData;
static STR_DESC rgCtCfgList;
static STR_DESC rgCtCfgUrno;

static STR_DESC rgCtFltLine;
static STR_DESC rgCtFltData;
static STR_DESC rgCtFltList;
static STR_DESC rgCtFltUrno;
*/

    dbg(TRACE,"STRING BUFFERS <%s> INITIALIZED",pcpCtTabName);
  }
  return ilRC;
} /* end of InitMyTab */




/* *************************************************** */
/* *************************************************** */
int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep)
{
  int ilCnt = 0;
  char *pclDest = NULL;
  char *pclSrc = NULL;
  char *pclLast = NULL; 
  /* last non blank byte position */
  /* but keep at least one blank */
  pclDest = pclTextBuff;
  pclSrc  = pclTextBuff;
  pclLast  = pclDest - 1;
  if (*pclSrc == ' ')
  {
    pclLast = pclTextBuff;
    pclSrc++;
    pclDest++;
  }
  while (*pclSrc != '\0')
  {
    if (*pclSrc == pcpFldSep[0])
    {
      pclDest = pclLast + 1;
      *pclDest = *pclSrc;
      pclSrc++;
      pclDest++;
      if (*pclSrc == ' ')
      {
        pclLast = pclDest;
      }
    }
    *pclDest = *pclSrc;
    if (*pclDest != ' ')
    {
      pclLast = pclDest;
    }
    pclDest++;
    pclSrc++;
  }
  pclDest = pclLast + 1;
  *pclDest = '\0';
  return ilCnt;
} /* end TAB_TrimRecordData */

/* *************************************************** */
/* *************************************************** */
static void ReplaceChars(char *pcpItems, char *pcpGet, char *pcpSet)
{
  char *pclPtr = NULL;
  pclPtr = pcpItems;
  while (*pclPtr != '\0')
  {
    if (*pclPtr == *pcpGet) *pclPtr = *pcpSet;
    pclPtr++;
  }
  return;
}


/* *************************************************** */
static int InitClassTotals(int ipInit, int ipReset)
{
  int ilRc = RC_SUCCESS;
  int ilIdx = 0;
  int ilItm = 0;
  int ilPos = 0;
  int ilLen = 0;
  char pclLink[16] = "";
  char pclMain[1024] = "";
  char pclComp[1024] = "";
  char pclList[1024] = "";

  if (ipInit == TRUE)
  {
    for (ilIdx=0;ilIdx<30;ilIdx++)
    {
      rgClassTotals[ilIdx].ClCnt = 0;
      rgClassTotals[ilIdx].AlCnt = 0;
      rgClassTotals[ilIdx].ClIdx = 0;
      rgClassTotals[ilIdx].Class[0] = ilIdx + 65;
      rgClassTotals[ilIdx].Class[1] = 0;
      rgClassTotals[ilIdx].Alias[0] = ilIdx + 65;
      rgClassTotals[ilIdx].Alias[1] = 0;
    }

    /* static char pcgMainClass[8] = "FCYU"; */
    dbg(TRACE,"MAIN CLASSES <%s>",pcgMainClass);
    dbg(TRACE,"COMP CLASSES <%s>",pcgCompClass);
    ilLen = strlen(pcgMainClass);
    for (ilPos=0;ilPos<ilLen;ilPos++)
    {
      ilIdx = pcgMainClass[ilPos] - 65;
      rgClassTotals[ilIdx].ClIdx = ilPos + 1;
      dbg(TRACE,"SETTING POS %d OF CLASS MAIN (index %d)",ilPos,ilIdx);
      dbg(TRACE,"MAIN %d: CLASS <%s> TOTAL = %d / ALIAS <%s> TOTAL = %d",
                 rgClassTotals[ilIdx].ClIdx,
                 rgClassTotals[ilIdx].Class,rgClassTotals[ilIdx].ClCnt,
                 rgClassTotals[ilIdx].Alias,rgClassTotals[ilIdx].AlCnt);
    }

    ilItm = 0;
    strcpy(pclComp,"START");
    ilLen = strlen(pclComp);
    while (ilLen > 0)
    {
      ilItm++;
      ilLen = get_any_item(pclComp, pcgCompClass, ilItm, "|");
      if (ilLen > 0)
      {
        /* static char pcgCompClass[1024] = "F:ABCD|C:EFGH|Y:JIKLM" */
        ilLen = get_any_item(pclLink, pclComp, 1, ":");
        ilLen = get_any_item(pclList, pclComp, 2, ":");
        if (ilLen > 0)
        {
          for (ilPos=0;ilPos<ilLen;ilPos++)
          {
            ilIdx = pclList[ilPos] - 65;
            rgClassTotals[ilIdx].Alias[0] = pclLink[0];
            dbg(DEBUG,"SETTING IDX %d OF COMP CLASS (POS %d)",ilIdx,ilPos);
            dbg(DEBUG,"MAIN %d: CLASS <%s> TOTAL = %d / ALIAS <%s> TOTAL = %d",
                 rgClassTotals[ilIdx].ClIdx,
                 rgClassTotals[ilIdx].Class,rgClassTotals[ilIdx].ClCnt,
                 rgClassTotals[ilIdx].Alias,rgClassTotals[ilIdx].AlCnt);
          }
        }
      }
    }
  }

  if (ipReset == TRUE)
  {
    for (ilIdx=0;ilIdx<30;ilIdx++)
    {
      rgClassTotals[ilIdx].ClCnt = 0;
      rgClassTotals[ilIdx].AlCnt = 0;
    }
  }
  return ilRc;
}

/* *************************************************** */
static int CountClassTotals(char *pcpClass)
{
  int ilRc = RC_SUCCESS;
  int ilIdx = 0;
  if (pcpClass[0] != 0x00)
  {
    ilIdx = pcpClass[0] - 65;
    rgClassTotals[ilIdx].ClCnt++;
    if (rgClassTotals[ilIdx].Alias[0] != rgClassTotals[ilIdx].Class[0])
    {
      ilIdx = rgClassTotals[ilIdx].Alias[0] - 65;
      rgClassTotals[ilIdx].AlCnt++;
    }
  }
  return ilRc;
}


/* *************************************************** */
static int ShowClassTotals(int ipShow)
{
  int ilRc = RC_SUCCESS;
  int ilIdx = 0;
  if (ipShow == TRUE)
  {
    for (ilIdx=0;ilIdx<30;ilIdx++)
    {
      if ((rgClassTotals[ilIdx].ClCnt > 0) || (rgClassTotals[ilIdx].AlCnt > 0))
      {
        dbg(TRACE,"MAIN %d: CLASS <%s> TOTAL = %d / ALIAS <%s> TOTAL = %d",
                   rgClassTotals[ilIdx].ClIdx,
                   rgClassTotals[ilIdx].Class,rgClassTotals[ilIdx].ClCnt,
                   rgClassTotals[ilIdx].Alias,rgClassTotals[ilIdx].AlCnt);
      }
    }
  }
  return ilRc;
}

/* *************************************************** */
static int GetMainTotals(int ipMNbr, char *pcpValue)
{
  int ilRc = RC_SUCCESS;
  int ilIdx = 0;
  int ilTotal = 0;

  for (ilIdx=0;((ilIdx<30)&&(ilTotal==0));ilIdx++)
  {
    if (rgClassTotals[ilIdx].ClIdx == ipMNbr)
    {
      dbg(TRACE,"MAIN %d: CLASS <%s> TOTAL = %d / ALIAS <%s> TOTAL = %d",
                 rgClassTotals[ilIdx].ClIdx,
                 rgClassTotals[ilIdx].Class,rgClassTotals[ilIdx].ClCnt,
                 rgClassTotals[ilIdx].Alias,rgClassTotals[ilIdx].AlCnt);
      ilTotal = rgClassTotals[ilIdx].ClCnt + rgClassTotals[ilIdx].AlCnt;
    }
  }
  sprintf(pcpValue,"%d",ilTotal);
  return ilRc;
}



/* *************************************************** */
/* *************************************************** */
static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[1024] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  long llCurTabLine = 0;
  long llMaxTabLine = 0;
  int ilFldCnt = 0;
  int ilLegCnt = 0, ilCount;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"---------------------------------");
  dbg(TRACE,"LOADING <%s> RECORDS",pcpGridName);
  dbg(TRACE,"---------------------------------");
  CT_ResetContent(pcpGridName);
  CT_GetFieldList(pcpGridName, pclFldList);

  strcpy(pclTblName,pcpTblName);
  strcpy(pclSqlCond,pcpSqlKey);
  sprintf(pclSqlBuff,"SELECT %s FROM %s %s ",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);
  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  ilCount = 0;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      TAB_TrimRecordData(pclSqlData, ",");
      dbg(TRACE,"<%s>",pclSqlData);
      CT_InsertTextLine(pcpGridName,pclSqlData);
      ilCount++;
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"----Loaded %d records -------------------------------",ilCount);

  return ilRc;
}


/* *************************************************** */
/* *************************************************** */
/* *************************************************** */
/* *************************************************** */
static int CheckDbAction(char *pcpGridName, char *pcpTableName, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilDbChgFlag = FALSE;
  long ilLineChecked = 0;
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclTableName[16] = "";
  char pclRangeName[16] = "";
  char pclCurUrno[16] = "";
  char pclNxtUrno[16] = "";
  long llMaxLine = 0;
  long llCurLine = 0;
  long llDbUpdFlag = 0;
  long llDbInsFlag = 0;
  long llDbDelFlag = 0;
  long llChgCount = 0;
  char pclLKEY [32], pclMKEY [32];
  dbg(TRACE,"CHECK DB ACTIONS (INS/UPD/DEL) OF <%s> (%s)",pcpGridName,pcpTableName);

  ilDbChgFlag = FALSE;
  llMaxLine = CT_GetLineCount(pcpGridName) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcpGridName, llCurLine, "URNO", pclCurUrno);
    CT_GetMultiValues(pcpGridName, llCurLine, "MKEY", pclMKEY);
    CT_GetMultiValues(pcpGridName, llCurLine, "LKEY", pclLKEY);

    llDbInsFlag = CT_GetLineDbInsFlag(pcpGridName,llCurLine);
    llDbUpdFlag = CT_GetLineDbUpdFlag(pcpGridName,llCurLine);
    llDbDelFlag = CT_GetLineDbDelFlag(pcpGridName,llCurLine);
    ilLineChecked = CT_GetLineCheckedFlag(pcpGridName,llCurLine);
    dbg(TRACE,"LINE %d: INS=%d UPD=%d DEL=%d (CHECKED=%d)",llCurLine,llDbInsFlag,llDbUpdFlag,llDbDelFlag,ilLineChecked);
    if ((llDbDelFlag > 0) || (ilLineChecked == 0))
    {
      if (ilLineChecked == 0)
      {
        dbg(TRACE,
          "DELETION OF AN OBSOLETE GORTAB RECORD (URNO <%s>,MKEY <%s>,LKEY <%s>)",
          pclCurUrno,pclMKEY,pclLKEY);
      }
      if (llDbInsFlag == 0)
      {
        dbg(TRACE,"-------------------------------");
		dbg(TRACE,"DELETE GOR RECORD (URNO <%s>)",pclCurUrno);
        GOR_DeleteGridRecord2DB(pcpGridName, llCurLine);
        ilDbChgFlag = TRUE;
      }
      else
      {
		dbg(TRACE,"WON'T DELETE OR INSERT A RECORD WITH 'INS/DEL' FLAGS COMBINATION");
      }
    }
    else if (llDbInsFlag > 0)
    {
      dbg(TRACE,"-------------------------------");
      dbg(TRACE,"INSERT GOR RECORD (URNO <%s>)",pclCurUrno);
      if (strcmp(pclCurUrno,"0") == 0)
      {
        dbg(TRACE,"GET ONE NEW URNO");
        GOR_GetValueFromUrnoPool(pcpGridName, pclNxtUrno);
        dbg(TRACE,"GOT NEW URNO <%s>",pclNxtUrno);
        CT_SetFieldValues(pcpGridName, llCurLine, "URNO", pclNxtUrno);  
        GOR_InsertGridRecord2DB(pcpGridName, llCurLine);
        ilDbChgFlag = TRUE;
      }
    }
    else if (llDbUpdFlag > 0)
    {
      dbg(TRACE,"-------------------------------");
      dbg(TRACE,"UPDATE GOR RECORD (URNO <%s>)",pclCurUrno);
      GOR_UpdateGridRecord2DB(pcpGridName, llCurLine);
      ilDbChgFlag = TRUE;
    }
  }
  dbg(TRACE,"-------------------------------");
  return ilDbChgFlag;
}


  /*GOR_CheckGorTabRecs(FOR_INIT);*/


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */







/* *************************************************** */
/* *************************************************** */
static int GOR_GetValueFromUrnoPool(char *pcpGridName, char *pcpNxtUrno)
{
  int ilGetRc = 0;
  ilGetRc = GOR_CheckGridUrnoPool(pcpGridName);
  ilGetRc = CT_GetNextGridUrno(pcpGridName, pcpNxtUrno, TRUE);
  return ilGetRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_CheckGridUrnoPool(char *pcpGridName)
{
  int ilCnt = 0;
  ilCnt = CT_GetGridUrnoCount(pcpGridName);
  dbg(TRACE,"URNO COUNT IS %d",ilCnt);
  if (ilCnt < 1)
  {
    dbg(TRACE,"MUST GET NEW URNO FROM NUMTAB");
    ilCnt = GOR_FillGridUrnoPool(pcpGridName, 1);
  }
  return ilCnt;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_FillGridUrnoPool(char *pcpGridName, int ipCount)
{
  int ilRc = RC_SUCCESS;
  int ilHandleRanges = FALSE;
  int ilPoolIdx = 0;
  char pclTableName[16] = "";
  char pclNumTabKeys[16] = "";
  char pclNumTabAcnu[32] = "";
  ilHandleRanges = CT_GetUrnoPoolMethod(pcpGridName, pclTableName, pclNumTabKeys);
  dbg(TRACE,"FILL URNO POOL <%s> <%s> <%s> (NEED %d)",pcpGridName, pclTableName, pclNumTabKeys, ipCount);
  if (ilHandleRanges != TRUE)
  {
    dbg(TRACE,"GET %d %s URNO VALUES BY <GetNextValues>",ipCount,pclTableName);
    ilRc = GetNextValues (pclNumTabAcnu, ipCount);
    dbg(TRACE,"NEXT URNO (FIRST VALUE) FROM NUMTAB = <%s>", pclNumTabAcnu);
    ilPoolIdx = CT_SetUrnoPool(pclTableName,pclNumTabKeys, pclNumTabAcnu, ipCount);
    dbg(TRACE,"URNO POOL INDEX <%s><%s> = %d",pclTableName,pclNumTabKeys,ilPoolIdx);
  }
  else
  {
    /* Still to be implemented */
    /* See Flthdl.c */
  }
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_InsertGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetFieldList(pcpGridName, pclSqlFld);
  CT_GetLineValues(pcpGridName, lpLineNo, &rgCtFlLine);
  dbg(TRACE,"NEW GRID RECORD\n<%s>",rgCtFlLine.Value);
  CT_CheckNullValues(rgCtFlLine.Value,rgCtFlData.Value);
  dbg(TRACE,"VALUES CHECKED\n<%s>",rgCtFlData.Value);

  GOR_BuildSqlInsValStrg(pclSqlVal,pclSqlFld);
  sprintf(pclSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",pclSqlTab,pclSqlFld,pclSqlVal);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(rgCtFlData.Value);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, rgCtFlData.Value);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD INSERTED");
    dbg(TRACE,"===============");
  }
  else
  {
    dbg (TRACE, "ORA INSERT FAILED. ROLLBACK!");
    dbg (TRACE, "============================");
    rollback ();
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_UpdateGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  long llChgCount = 0;
  char pclUrno[16] = "";
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  llChgCount = CT_GetChangedValues(pcpGridName, "", lpLineNo, pclFld, pclNew, pclOld);
  dbg(TRACE,"UPDATE 2DB LINE %d:",lpLineNo);
  dbg(TRACE,"FLD LIST: <%s>",pclFld);
  dbg(TRACE,"NEW DATA: <%s>",pclNew);
  dbg(TRACE,"OLD DATA: <%s>",pclOld);
  CT_CheckNullValues(pclNew,rgCtFlData.Value);
  dbg(TRACE,"VALUES CHECKED:\n<%s>",rgCtFlData.Value);

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetMultiValues(pcpGridName, lpLineNo, "URNO", pclUrno);

  GOR_BuildSqlUpdValStrg(pclSqlVal,pclFld);
  sprintf(pclSqlBuf,"UPDATE %s SET %s WHERE URNO=%s",pclSqlTab,pclSqlVal,pclUrno);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(rgCtFlData.Value);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, rgCtFlData.Value);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD UPDATED");
    dbg(TRACE,"==============");
  }
  else
  {
    dbg (TRACE, "ORA UPDATE FAILED. ROLLBACK!");
    dbg (TRACE, "============================");
    rollback ();
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_DeleteGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  long llChgCount = 0;
  char pclUrno[16] = "";
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"DELETE 2DB LINE %d:",lpLineNo);

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetMultiValues(pcpGridName, lpLineNo, "URNO", pclUrno);

  sprintf(pclSqlBuf,"DELETE FROM %s WHERE URNO=%s",pclSqlTab,pclUrno);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclNew);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD DELETED");
    dbg(TRACE,"==============");
  }
  else
  {
    dbg (TRACE, "ORA DELETION FAILED. (NO ROLLBACK!)");
    dbg (TRACE, "===================================");
    /*rollback ();*/
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static void GOR_BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst)
{
  char pclFldNam[16] = "";
  char pclFldVal[32] = "";
  long llFldCnt = 0;
  long i = 0;
  pcpResult[0] = 0x00;
  llFldCnt = CT_CountPattern(pcpFldLst, ",");
  for (i=0;i<=llFldCnt;i++)
  {
    CT_GetItemsFromTo(pclFldNam, pcpFldLst, i, i, ",");    
    sprintf(pclFldVal,":V%s,",pclFldNam);
    strcat(pcpResult,pclFldVal);
  }
  i = strlen(pcpResult) - 1;
  pcpResult[i] = 0x00;
  return;
}

/* *************************************************** */
/* *************************************************** */
static void GOR_BuildSqlUpdValStrg(char *pcpResult, char *pcpFldLst)
{
  char pclFldNam[16] = "";
  char pclFldVal[32] = "";
  long llFldCnt = 0;
  long i = 0;
  pcpResult[0] = 0x00;
  llFldCnt = CT_CountPattern(pcpFldLst, ",");
  for (i=0;i<=llFldCnt;i++)
  {
    CT_GetItemsFromTo(pclFldNam, pcpFldLst, i, i, ",");    
    sprintf(pclFldVal,"%s=:V%s,",pclFldNam,pclFldNam);
    strcat(pcpResult,pclFldVal);
  }
  i = strlen(pcpResult) - 1;
  pcpResult[i] = 0x00;
  return;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_SetMyTabUrnoPool(char *pcpCtTabName, char *pcpTableName)
{
  int ilRc = RC_SUCCESS;
  int ilHandleUrnoRanges = 0;
  char pclNumTabKey[32] = "";
  ilHandleUrnoRanges = DB_GetUrnoKeyCode(pcpTableName,pclNumTabKey);
  dbg(TRACE,"URNO POOL: <%s> <%s> RANGE KEY CONFIGURED AS <%s>",pcpCtTabName,pcpTableName,pclNumTabKey);
  if (ilHandleUrnoRanges != TRUE)
  {
    dbg(TRACE,"URNO POOL: <%s> <%s> VALUES BY <GetNextValues> SNOTAB.ACNU",pcpCtTabName,pcpTableName);
  }
  else
  {
    dbg(TRACE,"URNO POOL: <%s> <%s> NUMBERS BY <GetNextNumbers> NUMTAB.KEYS",pcpCtTabName,pcpTableName);
  }
  CT_SetUrnoPoolMethod(pcpCtTabName, pcpTableName, ilHandleUrnoRanges, pclNumTabKey);
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */

/********************************************************************
 *
 * ProcessLoaData - force LOATAB data into a form that is processed
 * by existing COPHDL code. Each LOATAB PTM message would require the
 * deletion of existing depatures being fed from the arrival of the PTM
 * as such we need to call LoadGridData so that existing entries for
 * MKEY are deleted before the new ones are inserted.
 * Arg piLoadGrid used to flag the start of the handling of an arrival,
 * whereby all existing records with its MKEY are therefore flagged for
 * deletion, to be replaced with the new PTM data.
 ********************************************************************
 */

static int ProcessLoaData(char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData, int piLoadGrid)
{ 
    int ilRc = RC_SUCCESS,ilDbChgFlag;
    char pclMKEY [64], pclFltDay [32], pclFltn [32], pclAlc2 [32];
    char pclLKEY [64];
    char pclCfdCond [64]; 
    int  ilPaxF,ilPaxC,ilPaxE,ilPaxU,jj;
    char pclTmpData [64];
    char pclFunc[] = "ProcessLoaData: ";
    char pclAftFields[128] = "";
    char pclAftSelKey[128] = "";
    char pclAftTable[16] = "AFTTAB";
    static int siGridLoaded = 0;
    int  llCurLine,llMaxLine;

    int ilRCdb = DB_SUCCESS;
    short slFkt;
    short slCursor;
    char pclSqlBuf[1024];
    char pclDataBuf[1024];

    strcpy (pclMKEY,pcpSelKey);
    strcpy (pclLKEY,pcpFields);
    /* Extract FLDA, etc from arrival */
    strcpy (pclTmpData,pclMKEY);
    ReplaceChars(pclTmpData, "-", ",");
    (void) get_real_item (pclAlc2, pclTmpData, 1);
    (void) get_real_item (pclFltn, pclTmpData, 2);
    (void) get_real_item (pclFltDay, pclTmpData, 3);
    dbg(TRACE,"%s <%s><%s><%s>",pclFunc,pclAlc2,pclFltn,pclFltDay);

    (void) get_real_item (pclTmpData, pcpData, 1);
    ilPaxF = atoi (pclTmpData);
    (void) get_real_item (pclTmpData, pcpData, 2);
    ilPaxC = atoi (pclTmpData);
    (void) get_real_item (pclTmpData, pcpData, 3);
    ilPaxE = atoi (pclTmpData);
    (void) get_real_item (pclTmpData, pcpData, 4);
    ilPaxU = atoi (pclTmpData);
   

    dbg(TRACE,"===== DATA ELEMENTS FOR SIMULATION =========="); 
    dbg(TRACE,"%s SELECT (MKEY of arrival) <%s>", pcpCmd, pclMKEY);
    dbg(TRACE,"%s FIELDS (LKEY of departure) <%s>", pcpCmd, pclLKEY); 
    dbg(TRACE,"PAXF %d PAXC %d PAXE %d PAXU %d",ilPaxF,ilPaxC,ilPaxE,ilPaxU);
    dbg(TRACE,"=========================================");

    InitClassTotals(FALSE, TRUE);
    rgClassTotals[5].ClCnt = ilPaxF;
    rgClassTotals[2].ClCnt = ilPaxC;
    rgClassTotals[24].ClCnt = ilPaxE;
    rgClassTotals[20].ClCnt = ilPaxU;
    strcpy(pclAftFields,"URNO,FLNO,STOA"); 
    sprintf(pclAftSelKey,
      "WHERE FLTN='%s' AND FLDA='%s' AND ALC2='%s'",
      pclFltn,pclFltDay,pclAlc2);
    ilRc = SearchFlight (pclAftTable, pclAftFields, pclAftSelKey, FOR_SEARCH, TRUE);
    if (ilRc == RC_SUCCESS)
    {
      dbg (TRACE, "%s FOUND ARR CONNEX FLIGHT:\nFIELDS <%s>\nVALUES <%s>",
        pclFunc,pclAftFields,pcgDataArea);
      strcpy(pcgCurAftArrFields,pclAftFields);
      strcpy(pcgCurAftArrData,pcgDataArea);
    }
    else
    {
      dbg(TRACE,"%s FLIGHT NOT FOUND",pclFunc);
      return ilRc;
    }


    sprintf(pclCfdCond,"WHERE MKEY='%s'",pclMKEY);
    /* if (siGridLoaded == 0) */
    if (piLoadGrid == 1) 
    {
      /*
        LoadGridData calls CT_ResetContent but that does not delete DB content.
        When existing CFDTAB rows for this arrival are loaded, the Check flag
        is unset. 
        To avoid mis-intepretation of records to be deleted in CheckDbAction
        have to pre-delete all CFDTAB rows for this MKEY first.
        This can't be the correct way to use the CT library but short of time.
        It will work only because when FDIHDL gets a PTM, it too deletes all 
        existing LOATAB rows for that flight, i.e. PTMs do not add to previous
        PTM's.
      */
      
      sprintf (pclSqlBuf,
        "DELETE FROM CFDTAB WHERE MKEY = '%s'",pclMKEY);
      slCursor = 0;
      slFkt = IGNORE;
      dbg(TRACE,"%s <%s>",pclFunc,pclSqlBuf);
      ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
      if (ilRCdb == DB_SUCCESS)
      {
        commit_work ();
        dbg(TRACE,"RECORD(s) DELETED");
      }
      else
      {
        dbg (TRACE, "ORA DELETION FAILED. (NO ROLLBACK!)");
        /*rollback ();*/
      }
      close_my_cursor (&slCursor);

      LoadGridData(pcgCfdTabGrid, pcgCfdTabName, pclCfdCond);
      dbg(TRACE,"%s LOADING GRID <%s>",pclFunc,pclCfdCond);
      siGridLoaded = 1;
    }
    else
    {
      dbg(TRACE,"%s GRID ALREADY LOADED <%s>",pclFunc,pclCfdCond);
    }

    MaintainCfdTabLine(pclMKEY, pclLKEY);

    ilDbChgFlag = CheckDbAction(pcgCfdTabGrid, pcgCfdTabName, 0);

    if (ilDbChgFlag == TRUE)
    {
      CreatePtmTrigger(pclMKEY, pclFltDay);
    }
    else
    {
      dbg(TRACE,"NOTHING CHANGED. NOTHING ELSE TO DO.");
    }

    dbg(TRACE,"----------------------------------------------------------");

    return ilRc;
}

/********************************************************************
 *
 * ReadLoaTab - FDIHDL sends PTMF message with a list of URNOs.
 * 
 * Retrive the arrival and departure info from AFTTAB to create MKEY
 * and series of LKEY's, then for each LKEY, read the PTM data.
 *
 * pcpSelKey is the arrivalURNO
 * pcpData is the list of dep urno's comma separated.
 * v.1.05 - refine selection of departure info from LOATAB with RURN
 ********************************************************************
 */

static int ReadLoaTab (char *pcpCmd, char *pcpTable, char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int ilRC=RC_SUCCESS;
  char pclFunc[]="ReadLoaTab:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  char pclTmp1 [64],pclTmp2[64];
  char pclArrFlno [64], pclArrFlda [64], pclArrFltn [32], pclArrAlc2 [32];
  char pclDepFlno [64], pclDepFlda [64], pclDepFltn [32], pclDepAlc2 [32];
  char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32], pclLoaSsst [32], pclLoaValu [32];
  char pclLoaCond [64], pclPaxInfo [64], pclLoaRurn [32];
  char pclMKEY [32], pclLKEY [32];
  int  ilLen,jj,kk,llMaxLine,llCurLine;
  int  ilPaxF,ilPaxC,ilPaxE,ilPaxU,ilLoadCfd; 
  char pclPaxValues [64];
  

  dbg(TRACE,"===== DATA ELEMENTS FOR PTMF ==========");
  dbg(TRACE,"%s SELECT (URNO of arrival) <%s>", pcpCmd, pcpSelKey);
  dbg(TRACE,"%s DATA (URNO's departure) <%s>", pcpCmd, pcpData); 
  dbg(TRACE,"=========================================");

  /* get the arrival */
  sprintf (pclSqlBuf,
    "SELECT FLNO,FLDA,FLTN,ALC2 FROM AFTTAB WHERE URNO = %s",pcpSelKey);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
    BuildItemBuffer(pclDataBuf,"FLNO,FLDA,FLTN,ALC2",4,",");
    ilLen = get_real_item(pclArrFlno,pclDataBuf,1);
    ilLen = get_real_item(pclArrFlda,pclDataBuf,2);
    ilLen = get_real_item(pclArrFltn,pclDataBuf,3);
    ilLen = get_real_item(pclArrAlc2,pclDataBuf,4);
    sprintf (pclMKEY,"%s-%s-%s",pclArrAlc2,pclArrFltn,pclArrFlda);
    dbg(TRACE,"%s Found Arrival Flight <%s><%s><%s><%s><%s>",
      pclFunc,pclArrFlno,pclArrFlda,pclArrFltn,pclArrAlc2,pclMKEY); 
    /* dbg(TRACE,"========================================="); */
  }
  else
  {
     dbg(TRACE,"%s === ERROR === PTM Arrival Not Found ===",pclFunc);
     /*
       v.1.5 missed this closing of cursor - but why should we ever get here?  
     */
     close_my_cursor(&slCursor);
     return ilRCdb;
  }
  close_my_cursor(&slCursor);

  /*
    Get the TLXTAB urno referring to the above arrival's PTM. There is only one PTM 
    LOATAB group of rows for a particular arrival, because any subsequent PTM for
    that arrival does not add to the previous PTM, but completely replaces it.
  */

  sprintf (pclSqlBuf,
    "SELECT DISTINCT(RURN) FROM LOATAB WHERE DSSN = 'PTM' AND FLNU = %s",pcpSelKey);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclLoaRurn);
  if (ilRCdb == DB_SUCCESS)
  {
    dbg(TRACE,"%s LOATAB RURN for arrival <%s> is <%s>",
      pclFunc,pclArrFlno,pclLoaRurn); 
    dbg(TRACE,"=========================================");
  }
  else
  {
     dbg(TRACE,"%s === ERROR === LOATAB.RURN for Arrival Not Found ===",pclFunc);
     close_my_cursor(&slCursor);
     return ilRCdb;
  }
  close_my_cursor(&slCursor);

  /* get the departures */
  ilLen = 1;
  jj = 0;
  while (ilLen >= 0)
  {
    jj++;
    ilLen = get_real_item(pclTmp1,pcpData,jj);
    if (ilLen < 0)
    {
      dbg(TRACE,"%s No more departures to process",pclFunc);
      break;
    }

    sprintf (pclSqlBuf,
      "SELECT FLNO,FLDA,FLTN,ALC2 FROM AFTTAB WHERE URNO = %s",pclTmp1);
    slCursor = 0;
    slFkt = START;
    dbg(TRACE,"%s <%s>",pclFunc,pclSqlBuf);
    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
    if (ilRCdb == DB_SUCCESS)
    {
      BuildItemBuffer(pclDataBuf,"FLNO,FLDA,FLTN,ALC2",4,",");
      ilLen = get_real_item(pclDepFlno,pclDataBuf,1);
      ilLen = get_real_item(pclDepFlda,pclDataBuf,2);
      ilLen = get_real_item(pclDepFltn,pclDataBuf,3);
      ilLen = get_real_item(pclDepAlc2,pclDataBuf,4);
      sprintf (pclLKEY,"%s-%s-%s",pclDepAlc2,pclDepFltn,pclDepFlda);
      dbg(TRACE,"%s Found Departure Flight <%s><%s><%s><%s><%s>",
        pclFunc,pclDepFlno,pclDepFlda,pclDepFltn,pclDepAlc2,pclLKEY); 
    }
    else 
    {
      dbg(TRACE,"%s === ERROR === PTM Departure Not Found - Urno <%s>",
        pclFunc,pclTmp1);
      close_my_cursor(&slCursor);
      continue;
    }
    close_my_cursor(&slCursor);
    commit_work();
  
    /* for each departure, get the summed class pax values from LOATAB */
    /* try use the CT library without a written manual **/
    /* v.1.05 - add RURN to restrict to current PTM telex */
    sprintf(pclLoaCond,"WHERE DSSN='PTM' AND FLNU='%s' AND RURN='%s'",
      pclTmp1,pclLoaRurn);

    LoadGridData(pcgLoaTabGrid, pcgLoaTabName, pclLoaCond);
    
    llMaxLine = CT_GetLineCount(pcgLoaTabGrid) - 1;
    

    ilPaxF = ilPaxC = ilPaxE = ilPaxU = 0;
    for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
    {
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "TYPE", pclLoaType);
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "STYP", pclLoaStyp);
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSTP", pclLoaSstp);
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSST", pclLoaSsst);
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "VALU", pclLoaValu);

      if ((strcmp (pclLoaType,"PAX") == 0)                
        && (strcmp (pclLoaStyp,"F") == 0)                
        && (strcmp (pclLoaSstp,"TD") == 0)                
        && (strcmp (pclLoaSsst," ") == 0))    
      {
        ilPaxF = atoi (pclLoaValu);
        dbg(TRACE,"%s Total F <%d>",pclFunc,ilPaxF);
        continue;
      }
  
      if ((strcmp (pclLoaType,"PAX") == 0)                
        && (strcmp (pclLoaStyp,"B") == 0)                
        && (strcmp (pclLoaSstp,"TD") == 0)                
        && (strcmp (pclLoaSsst," ") == 0))    
      {
        ilPaxC = atoi (pclLoaValu);
        dbg(TRACE,"%s Total B <%d>",pclFunc,ilPaxC);
        continue;
      }

      if ((strcmp (pclLoaType,"PAX") == 0)                
        && (strcmp (pclLoaStyp,"E") == 0)                
        && (strcmp (pclLoaSstp,"TD") == 0)                
        && (strcmp (pclLoaSsst," ") == 0))    
      {
        ilPaxE = atoi (pclLoaValu);
        dbg(TRACE,"%s Total E <%d>",pclFunc,ilPaxE);
        continue;
      }

      if ((strcmp (pclLoaType,"PAX") == 0)                
        && (strcmp (pclLoaStyp,"U") == 0)                
        && (strcmp (pclLoaSstp,"TD") == 0)                
        && (strcmp (pclLoaSsst," ") == 0))    
      {
        ilPaxU = atoi (pclLoaValu);
        dbg(TRACE,"%s Total U <%d>",pclFunc,ilPaxU);
        continue;
      }
    }

    sprintf (pclPaxInfo,"%d,%d,%d,%d",ilPaxF,ilPaxC,ilPaxE,ilPaxU);
    dbg(TRACE,"%s MKEY <%s> LKEY <%s> PAX <%s>",
      pclFunc,pclMKEY,pclLKEY,pclPaxInfo);

    /* then call ProcessLoaData with all the values */
    ilLoadCfd = (jj == 1 ? 1:0);
    ProcessLoaData("SIM1", "TABLE", pclMKEY, pclLKEY, pclPaxInfo, ilLoadCfd);
    dbg(TRACE,"=========================================");
  }
  close_my_cursor(&slCursor);
  commit_work ();
  return ilRC;
}
