<%
switch ($listElement) {
    case "arrival_hours":%>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Flight No" ); ?>>Flight</a></TD>
        <TD VALIGN=TOP  class="TDTitleArrival"><a href="#"  class="TDTitleArrival" <? echo $ol->over ($masktitle,"Code Share Flights" ); ?>>Share Flights</a></TD>
        <TD VALIGN=TOP  class="TDTitleArrival"><a href="#"  class="TDTitleArrival" <? echo $ol->over ($masktitle,"Scheduled Time of Arrival" ); ?>>STA</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#"  class="TDTitleArrival" <? echo $ol->over ($masktitle,"Origin" ); ?>>Org</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Via" ); ?>>Via</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Estimated Time  of Arrival" ); ?>>ETA</a></TD>			
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Actual Time  of Arrival" ); ?>>ATA</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"On block (when the plane is at Parking position)" ); ?>>OBL</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Aircraft type" ); ?>>A/C</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Which belt will be used" ); ?>>BELT</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"General Remark" ); ?>>REM</a></TD>
        <TD VALIGN=TOP class="TDTitleArrival"><a href="#" class="TDTitleArrival" <? echo $ol->over ($masktitle,"Next Information<br>will be given at:" ); ?>>NXTI</a></TD>

<%        break;
    case "dep_hours":%>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Flight No" ); ?>>Flight</a></TD>
        <TD VALIGN=TOP  class="TDTitleArrival"><a href="#"  class="TDTitleArrival" <? echo $ol->over ($masktitle,"Code Share Flights" ); ?>>Share Flights</a></TD>
        <TD VALIGN=TOP  class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Scheduled Time of Departure" ); ?>>STD</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Destination" ); ?>>DES</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Via" ); ?>>Via</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Estimated Time  of Departure" ); ?>>ETD</a></TD>					
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Off Block (when the plane leave the Parking position)" ); ?>>OFB</a></TD>					
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Actual Time  of Departure" ); ?>>ATD</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Checkin Counters to be used" ); ?>>CHECKIN</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Gate to be used" ); ?>>GATE</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Exit or Waiting room or Lounge No" ); ?>>EXIT</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Aircraft type" ); ?>>A/C</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"General Remark" ); ?>>REM</a></TD>
        <TD VALIGN=TOP class="TDTitleDeparture"><a href="#" class="TDTitleDeparture" <? echo $ol->over ($masktitle,"Next Information<br>will be given at:" ); ?>>NXTI</a></TD>

<%        break;
    case "flno":%>
        <TD VALIGN=TOP class="TDTitleAirline">2 Letter Code</TD>
        <TD VALIGN=TOP class="TDTitleAirline">3 Letter Code</TD>
        <TD VALIGN=TOP class="TDTitleAirline">Airline Name</TD>
<%        break;
    case "city":%>
        <TD VALIGN=TOP class="TDTitlecity">3 Letter Code</TD>
        <TD VALIGN=TOP class="TDTitlecity">4 Letter Code</TD>
        <TD VALIGN=TOP class="TDTitlecity">City Name</TD>
<%        break;
	 default:%>
	<TH class="tdtitle" align="center"> #Χί   ψ# </TH>
<%
 }
%>
