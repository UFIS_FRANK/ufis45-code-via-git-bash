001	Verification of template <%s>
002		No valid rules for this template found
003		No rules for this template found
004		Verification of rule <%s>
005			Rule not valid for arrival at <%s>
006			Rule not valid for departure at <%s>
007			Maximal ground time <%ld min> shorter than real ground time 
008			Minimal split time <%ld min> shorter than real ground time
009			
010			ALL CONDITIONS ARE TRUE !!!
011			Syntax error in condition
012			Condition <%s> not fulfilled
013			Single value <%s> <%s> unequal <%s>
014			Event type <%s> and condition type <%s> don't fit
015			Dynamic group <%s> not found
016			Dynamic group <%s> exists more than once
017			No data for <%s> found
018			No valid tablename in condition
019			No valid fieldname in condition
020			Condition <%s> is not correct
021			%s: <%s> is not in static group <%s>
022			%s: Values for inbound <%s> and outbound <%s> different or empty"
023		Status of arrival <%s> not configured for demand creation
024		Status of departure <%s> not configured for demand creation