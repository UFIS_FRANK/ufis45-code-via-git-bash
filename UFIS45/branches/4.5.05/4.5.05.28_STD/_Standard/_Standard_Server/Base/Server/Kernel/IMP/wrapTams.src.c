#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/IMP/wrapTams.src.c 1.2 2004/12/13 18:45:43SGT usc Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Uwe Schmeling                                             */
/* Date           : 13.12.2004                                                */
/* Description    : This is a dummy module for projects that don't implement  */
/*                  a TAMS interface                                          */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string 

/* Function Prototypes */


int wrapTamsData(char *pclData, char *pclCmd,
		 char *pclSelOut,char *pclFldOut,char *pclDatOut,
		 int ipPrioToTams, int ipQueueToTams);

int wrapTamsData(char *pclData, char *pclCmd,
		 char *pclSelOut,char *pclFldOut,char *pclDatOut,
		 int ipPrioToTams, int ipQueueToTams)
/* pcpData: The original Data that has to be transformed*/
/* Command, Selection,Fields,Data: the return-values of the function.... input for impman*/
{
  dbg(TRACE,"<wrapTamsData> This is a dummy wrapper module -- TAMS functionality is in project specific implementation");
  return RC_FAIL;
}



