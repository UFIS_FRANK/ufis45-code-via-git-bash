#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Wmq/briwmq.c 1.3 2005/08/19 21:27:36SGT heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : HEB: added Alerter-Funktionality                          */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/* static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I wmqcdi.c 45.3 / 04.06.2002 HEB";*/


/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
/* includes for WebsphereMQ  */
#include <cmqc.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;

static char cgMQServer[100] = "\0";
static char cgQMgrReceive[50] = "\0";
static char cgQMgrSend[50] = "\0";
static char cgMqReceive[50] = "\0";
static char cgMqSend[50] = "\0";
static char cgWaitSend[100] = "0";
static char cgReconnectMq[10] = "\0";


pid_t gChildPid;
/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecBrowse;              /* connection handle             */
MQHCONN  HconRecGet;                 /* connection handle             */
MQHOBJ   HobjRecBrowse;              /* object handle                 */
MQHOBJ   HobjRecGet;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetCommand(char *pcpCommand, int *pipCmd);

static int ReceiveFromMQ();
static int SendToCeda(char *pcpBuffer,char *pcpMyTime,long plpMyReason, char *pcpUrno);
static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int OpenSendMQ();
static int CreateLogFile();
void myinitialize(int argc,char *argv[]);
static int SendToMq(char *pcpData);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRC = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilOldDebugLevel = 0;

    gChildPid = fork();

	
	    

/*    INITIALIZE;	*/
    myinitialize(argc,argv);

    (void)SetSignals(HandleSignal);
    debug_level = DEBUG;

    if(gChildPid == -1)
    {
	dbg(TRACE,"%05d: Fork failed!",__LINE__);
    }

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    while (gChildPid == -1)
    {
	dbg(TRACE,"********* fork failed, waiting 60 Seconds **********");
    }
		
    if (gChildPid == 0)
    {
	dbg(TRACE,"******* I am the child *******");
    }
    else
    {
	dbg(TRACE,"******* I am the parent *******");
    }

    /* Attach to the MIKE queues */
    do
    {
	ilRC = init_que();
	if(ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */


    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"Binary_File <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */


    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
	

    ilRC = Init_Process();

    if (gChildPid == 0)
    {
	ReceiveFromMQ();
    }
    for(;;)
    {
	/* the child should not listen to this CEDA-queue*/
			dbg(TRACE,"Now receive from CEDA-queue");
	ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;
				
	if( ilRC == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRC != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRC);
	    } /* fi */

	    lgEvtCnt++;

	    switch( prgEvent->command )
	    {

		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    /* process shutdown - maybe from uutil */
		    Terminate(1);
		    break;
					
		case	RESET		:
		    ilRC = Reset();
		    break;
					
		case	EVENT_DATA	:

		    ilRC = HandleData(prgEvent);
		    if(ilRC != RC_SUCCESS)
		    {
			HandleErr(ilRC);
		    }/* end of if */
		    break;
				

		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		default			:
		    dbg(TRACE,"MAIN: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
			dbg(TRACE,"after DebugPrintEvent");
		    break;
	    } /* end switch */




	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRC);
	} /* end else */
		
    } /* end for */
	
      
} /* end of MAIN */



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry [%s],<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_Process()
{
    int  ilRC = RC_SUCCESS;			/* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
/*    char pclSqlBuf[2560] = "\0";*/
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clQueueName[30] = "\0";

    int ilPid = 0;

    debug_level = DEBUG;

    if(ilRC == RC_SUCCESS)
    {
	/* read HomeAirPort from SGS.TAB */
	ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
	}
    }

	
    if(ilRC == RC_SUCCESS)
    {

	ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
	}
    }

    if (ilRC == RC_SUCCESS)
    {
	ilRC = ReadConfigEntry("QUEUE","MQSERVER",cgMQServer);
	ilRC = ReadConfigEntry("QUEUE","MQ_MGR_RECEIVE",cgQMgrReceive);
	ilRC = ReadConfigEntry("QUEUE","MQ_MGR_SEND",cgQMgrSend);
	ilRC = ReadConfigEntry("QUEUE","MQ_RECEIVE",cgMqReceive);
	ilRC = ReadConfigEntry("QUEUE","MQ_SEND",cgMqSend);
	ilRC = ReadConfigEntry("QUEUE","WAIT_SEND_TO_CEDA",cgWaitSend);
	ilRC = ReadConfigEntry("RECONNECT","RECONNECT_MQ",cgReconnectMq);
    }

    if( gChildPid != 0)
    {
	dbg(TRACE,"%05d: connect for sending to MQ",__LINE__);
	if(strlen(cgQMgrSend) > 0)
	{
	    ilRC = ConnectMQ(cgQMgrSend, &HconSend);
	    ilRC = OpenSendMQ();
	}
	else
	{
	    dbg(TRACE,"%05d: Send-Qmanager not configured",__LINE__);
	}
    }

    dbg(TRACE," PID: <%d>",getpid());

    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"Init_Process failed");
    }
    else
    {
	dbg(DEBUG,"Init_Process successful");
    }

    debug_level = TRACE;
	
    return(ilRC);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRC = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    int ilRC = RC_SUCCESS;
    MQLONG   C_options;              /* MQCLOSE options                 */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */

    C_options = MQCO_NONE;           /* no close options             */

    if(gChildPid == 0)   
    {
	MQCLOSE (HconRecBrowse, &HobjRecBrowse, C_options, &CompCode, &Reason);
	MQCLOSE (HconRecGet, &HobjRecGet, C_options, &CompCode, &Reason);
	MQDISC(&HconRecBrowse,&CompCode,&Reason);
	MQDISC(&HconRecGet,&CompCode,&Reason);

    }
    else
    {
	MQCLOSE (HconSend, &HobjSend, C_options, &CompCode, &Reason);
	MQDISC(&HconSend,&CompCode,&Reason);
	dbg(TRACE,"Due to SIGTERM Kill Child now");

	kill(gChildPid,SIGTERM);
    }

    if(gChildPid == 0)
    {
    }
    else
    {
	kill(gChildPid,SIGTERM);
    }




    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");
	
    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
	
    exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
	case SIGCHLD:
	    dbg(TRACE,"HandleSignal:SIGHCLD received, Terminate now");
	    Terminate(1);
	    break;
	case SIGTERM:
	    dbg(TRACE,"HandleSignal:SIGTERM received !");
	    if(gChildPid != 0)   /* then its a parent */
	    {
		dbg(TRACE,"Due to SIGTERM Kill Child now");
		kill(gChildPid,SIGTERM);
	    }
	    dbg(TRACE,"Due to SIGTERM Terminate now");
	    Terminate(1);
	    break;


	default	:
	    Terminate(1);
	    break;
    } /* end of switch */

    exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"%05d: <%d> : msgrcv failed",__LINE__,pipErr);
	    Terminate(1);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    dbg(TRACE,"<%d> : no messages on queue",pipErr);
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int	   ilRC           = RC_SUCCESS;			/* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData1         = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char 		clUrnoList[2400];
    char 		clTable[34];
    int			ilUpdPoolJob = TRUE;

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData1      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    dbg(TRACE,"Command:   <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start:  <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end:    <%s>",prlCmdblk->tw_end);
    dbg(TRACE,"originator <%d>",prpEvent->originator);
    dbg(TRACE,"selection  <%s>",pclSelection);
    dbg(TRACE,"fields     <%s>",pclFields);
    dbg(TRACE,"data       <%s>",pclData1);
    /****************************************/
    dbg(TRACE,"now do CedaToMq:");
    CedaToMq(prpEvent,&pclData,NULL,NULL);
    dbg(TRACE,"CedaToMq done");
    ilRC = SendToMq(pclData);


    if (strcmp(prlCmdblk->command,"28674") == 0)
    {
	Terminate(1);
    }

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
	
} /* end of HandleData */



static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon)
{
    static int ilRC = RC_SUCCESS;
 
    MQLONG   CompCode;               /* completion code               */
    MQLONG   CReason;                /* reason code for MQCONN        */

    static char clMyMQServer[100] = "\0";
    static char clNowTime[20] = "\0";
    static char clAlert[256] = "\0";

    /* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
    sprintf(clMyMQServer,"MQSERVER=%s",cgMQServer);
    ilRC=putenv(clMyMQServer);
   
    /* CONNECT TO GIVEN QMANAGER */
    CompCode = MQCC_FAILED;
    
    while (CompCode == MQCC_FAILED)
    {
	MQCONN(pcpQManager,                  /* queue manager                  */
	       Hcon,                         /* connection handle              */
	       &CompCode,                    /* completion code                */
	       &CReason);                    /* reason code                    */

	/* report reason and stop if it failed     */
	if (CompCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: MQCONN ended with reason code <%d>, now waiting <%s> seconds and try to reconnect",__LINE__, CReason, cgReconnectMq);
	      sleep(atoi(cgReconnectMq));
	}
	else
	{
	    dbg(TRACE,"%05d: MQCONN connected with QManager <%s>",__LINE__, pcpQManager);
	    dbg(TRACE,"%05d: on Server: <%s>",__LINE__,cgMQServer); 
	}
    }
    

    return ilRC;
}


static int ReceiveFromMQ()
{
    int ilRC = RC_SUCCESS;
    MQLONG   OpenBrowseOptions;              /* MQOPEN options                */
    MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
    MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseReason;                 /* reason code                   */
    MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQLONG   OpenOptions;              /* MQOPEN options                */
    MQHOBJ   QueueHobj;                   /* object handle                 */
    MQOD     QueueOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueReason;                 /* reason code                   */
    MQGMO    QueueGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQBYTE   RecBuffer[4000];            /* message buffer                */
    MQLONG   RecBuffLen;                 /* buffer length                 */
    MQLONG   MessLen;                /* message length received       */

    char clMsgId[30] = "\0";
    char clMyBuffer[4000] = "\0";
    char clMyTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    static char clAlert[256] = "\0";


/* CONNECT TO QMANAGER */
    if(strlen(cgQMgrReceive) == 0)
    {
	dbg(TRACE,"%05d: Receive-QManager not configured",__LINE__);
    }
    else
    {
	ilRC = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
	ilRC = ConnectMQ(cgQMgrReceive, &HconRecGet);
    
    
/* OPEN QUEUE FOR BROWSING */
	OpenBrowseOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueBrowseOd.ObjectName,cgMqReceive); 	     
    
	QueueBrowseReason = 1111; /* != MQRC_NONE */
	QueueBrowseOpenCode = MQCC_FAILED;
    
	while ((QueueBrowseReason != MQRC_NONE) || (QueueBrowseOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecBrowse,                      /* connection handle            */
		   &QueueBrowseOd,                /* object descriptor for queue  */
		   OpenBrowseOptions,                 /* open options                 */
		   &QueueBrowseHobj,                     /* object handle                */
		   &QueueBrowseOpenCode,                 /* completion code              */
		   &QueueBrowseReason);                  /* reason code                  */

	    if (QueueBrowseReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueBrowseReason);

		ilRC = RC_FAIL;
	    }

	    if (QueueBrowseOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for browsing",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueBrowseReason.......*/

/* OPEN QUEUE FOR Receiving with GET (and delete item on queue) */
	OpenOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    +  MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueOd.ObjectName,cgMqReceive); 	     
    
	QueueReason = 1111; /* != MQRC_NONE */
	QueueOpenCode = MQCC_FAILED;
    
	while ((QueueReason != MQRC_NONE) || (QueueOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecGet,                      /* connection handle            */
		   &QueueOd,                /* object descriptor for queue  */
		   OpenOptions,                 /* open options                 */
		   &QueueHobj,                     /* object handle                */
		   &QueueOpenCode,                 /* completion code              */
		   &QueueReason);                  /* reason code                  */

	    if (QueueReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);

		ilRC = RC_FAIL;
	    }

	    if (QueueOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for receive and delete item",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueReason.......*/


/* NOW LOOP FOR RECEIVING AND SENDING THE DATA FROM THE MQ-QUEUE */


	for(;;)
	{
/* NOW GET FIRST ITEM ON QUEUE WITH BROWSE, SO THE ITEM WILL NOT BE DELETED FROM THE QUEUE */

	    dbg(TRACE,"%05d: =================Wait for next message on Queue <%s>==============",__LINE__,cgMqReceive);
	    QueueBrowseGmo.Options = MQGMO_WAIT 
			+ MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_BROWSE_FIRST
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
		QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;

	    RecBuffLen = sizeof(RecBuffer) -1;
    
	    memcpy(ReceiveBrowseMd.MsgId, MQMI_NONE, sizeof(ReceiveBrowseMd.MsgId));
	    memcpy(ReceiveBrowseMd.CorrelId, MQCI_NONE, sizeof(ReceiveBrowseMd.CorrelId));
    
	    
		do
		{
		MQGET(HconRecBrowse,                /* connection handle                 */
		      QueueBrowseHobj,                /* object handle                     */
		      &ReceiveBrowseMd,                 /* message descriptor                */
		      &QueueBrowseGmo,                /* get message options               */
		      RecBuffLen,              /* buffer length                     */
		      RecBuffer,              /* message buffer                    */
		      &MessLen,            /* message length                    */
		      &QueueBrowseCompCode,           /* completion code                   */
		      &QueueBrowseReason);            /* reason code                       */


	    if (QueueBrowseReason == MQRC_NONE)
	    {
		strcpy(clMyTime,ReceiveBrowseMd.PutDate);
		clMyTime[16] = '\0';
		RecBuffer[MessLen] = '\0';
		dbg(TRACE,"%05d: MQGET (browse) received Message: <%s> Time: <%s>, Reason: <%d>",__LINE__,RecBuffer,clMyTime,QueueBrowseReason);

	    }
	    else
	    {
		dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueBrowseReason);
		dbg(TRACE,"%05d: MQGET received Message: <%s> Time: <%s>",__LINE__,RecBuffer,clMyTime);
		if(QueueBrowseReason != 2079)
		{
		    if(QueueBrowseReason == 2016)
		    {
			dbg(TRACE,"%05d: (Browse) GetInhibited, waiting 120 seconds for retry",__LINE__);
			sleep(120);
		    }
		    else
		    {
			Terminate(1);
		    }
		}

	    }
		}
		while (QueueBrowseReason != MQRC_NONE);

	    sprintf(clMyBuffer,"%s",RecBuffer);


/* SEND DATA TO CEDA */
	    if(QueueBrowseReason == MQRC_NONE)
	    {
		ilRC = SendToCeda(clMyBuffer,clMyTime,QueueBrowseReason,clUrno);
	    }

/* NOW GET FIRST ITEM ON QUEUE AND DELETE IT FROM THE QUEUE */
    
	    QueueGmo.Options = MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
    
	    RecBuffLen = sizeof(RecBuffer) -1;
    
	    memcpy(ReceiveMd.MsgId, MQMI_NONE, sizeof(ReceiveMd.MsgId));
	    memcpy(ReceiveMd.CorrelId, MQCI_NONE, sizeof(ReceiveMd.CorrelId));
		do
		{
	    MQGET(HconRecGet,                /* connection handle                 */
		  QueueHobj,                /* object handle                     */
		  &ReceiveMd,                 /* message descriptor                */
		  &QueueGmo,                /* get message options               */
		  RecBuffLen,              /* buffer length                     */
		  RecBuffer,              /* message buffer                    */
		  &MessLen,            /* message length                    */
		  &QueueCompCode,           /* completion code                   */
		  &QueueReason);            /* reason code                       */

	    if (QueueReason == MQRC_NONE)
	    {
		RecBuffer[MessLen] = '\0';
		dbg(DEBUG,"%05d: MQGET received and deleted Message: <%s>",__LINE__,RecBuffer);
		dbg(TRACE,"%05d: MQGET received and deleted Message from Queue",__LINE__);
	    }
	    else
	    {

		dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueReason);
		dbg(TRACE,"%05d: MQGET received and deleted Message from MQ: <%s>",__LINE__,RecBuffer);
		if(QueueBrowseReason != 2079)
		{
		    if(QueueBrowseReason == 2016)
		    {
			dbg(TRACE,"%05d: GetInhibited, waiting 120 seconds for retry",__LINE__);
			sleep(120);
		    }
		    else
		    {
			Terminate(1);
		    }
		}
	    }
		}
		while (QueueBrowseReason != MQRC_NONE);

	} /* end of for(;;) */

    } /* end of if(strlen(Receive-Qmanager.....)) */
	 return ilRC;


}


static int SendToCeda(char *pcpBuffer,char *pcpMyTime,long plpMyReason, char *pcpUrno) 
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    int ilCount = 0;
    int ilAckRC = RC_FAIL;
		int ilLen = 0;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    char clTwStart[30] = "\0";
    char clTwEnd[30] = "\0";
    int i = 0;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
		static ITEM *prlItem;
		EVENT *prlEvent;

    strcpy(clUrno,pcpUrno);
    TimeToStr(clNowTime,time(NULL));


    dbg(TRACE,"%05d: -----SendToCeda start -----",__LINE__);
    dbg(TRACE,"%05d: pcpBuffer: <%s>, pcpMyTime: <%s>, plpMyReason: <%d>, pcpUrno: <%s>",__LINE__,pcpBuffer,pcpMyTime,plpMyReason,pcpUrno);
    dbg(TRACE,"%05d: Now do MqToCeda",__LINE__);
    MqToCeda(&prlItem,pcpBuffer,NULL,NULL);
    dbg(TRACE,"%05d: MqToCeda done",__LINE__);
    prlEvent = (EVENT *) prlItem->text;

    prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;
    
    
    /****************************************/
    dbg(TRACE,"Command:   <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start:  <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end:    <%s>",prlCmdblk->tw_end);
    dbg(TRACE,"originator <%d>",prlEvent->originator);
    dbg(TRACE,"selection  <%s>",pclSelection);
    dbg(TRACE,"fields     <%s>",pclFields);
    dbg(TRACE,"data       <%s>",pclData);
    dbg(TRACE,"route      <%d>",prlItem->route);
    /****************************************/

		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) +sizeof(CMDBLK) +strlen(pclSelection) + strlen(pclFields)+strlen(pclData);
    ilRC = que(QUE_PUT,prlItem->route,mod_id,PRIORITY_3,ilLen,(char *)prlEvent);
	dbg(TRACE,"%05d: Now wait <%s> milliseconds",__LINE__,cgWaitSend);
	nap(atoi(cgWaitSend));
    dbg(TRACE,"%05d: -----SendToCeda end -----",__LINE__);

    return ilRC;
}



static int TimeToStr(char *pcpTime,time_t lpTime)
{
    struct tm *_tm;
    char   _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */






static int OpenSendMQ()
{
    int ilRC = RC_SUCCESS;
    MQOD od = {MQOD_DEFAULT};
    MQLONG   O_options;              
    MQLONG   QueueOpenCode;               /* completion code                 */
    MQLONG   QueueReason;                 /* reason code                     */
    static char clAlert[256] = "\0";

    strcpy(od.ObjectName, cgMqSend);
    
    O_options = MQOO_OUTPUT         /* open queue for output          */
	+ MQOO_FAIL_IF_QUIESCING; /* but not if MQM stopping    */
    QueueReason = 1111; /* != MQRC_NONE */
    QueueOpenCode = MQCC_FAILED;
    
    while ((QueueReason != MQRC_NONE) || (QueueOpenCode == MQCC_FAILED))
    {
	MQOPEN (HconSend, &od, O_options, &HobjSend, &QueueOpenCode, &QueueReason);

	if (QueueReason != MQRC_NONE)
	{
	    dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);

	    ilRC = RC_FAIL;
	}

	if (QueueOpenCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: unable to open send-queue <%s>",__LINE__,cgMqSend);
	    ilRC = RC_FAIL;
	}

	if (ilRC == RC_FAIL)
	{
	    dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
	    sleep(atoi(cgReconnectMq));
	}
	else
	{
	    dbg(TRACE,"%05d: Queue <%s> opened for sending to MQ",__LINE__,cgMqSend);
	}
    } /* end of while (QueueReason.......*/


    return ilRC;
}





void myinitialize(int argc,char *argv[])
{
    mod_name = argv[0];
    /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name,'/')!= NULL)
    {
	mod_name= strrchr(mod_name,'/');
	mod_name++;
    }
    if(gChildPid == 0)
    {
        sprintf(__CedaLogFile,"%s/%sc%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    else
    {
        sprintf(__CedaLogFile,"%s/%sp%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    outp = fopen(__CedaLogFile,"w");
    if (argc > 1)
    {
	mod_id = atoi(argv[1]);
	ctrl_sta = atoi(argv[2]);
    } else {
	mod_id = 0;
	ctrl_sta = 1;
    }
    glbrc = init();
    if(glbrc)
    {
	fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
	exit(1);
    }
}



static int SendToMq(char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char clMyTime[30] = "\0";
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clUrno[20] = "\0";
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    char clAlert[256] = "\0";

    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};   /* put message options             */

    dbg(TRACE,"-----SendToMq start -----");





/* PUT MESSAGE TO MQ QUEUE */

	memcpy(md.Format,           
	       MQFMT_STRING, MQ_FORMAT_LENGTH);
	memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));

	pmo.Options = MQPMO_NEW_MSG_ID
	    + MQPMO_FAIL_IF_QUIESCING;

	dbg(TRACE,"%05d: Send to Queue: <%s> pcpData <%s>, length <%d>",__LINE__,cgMqSend,pcpData,strlen(pcpData));

	
	do
	{
	MQPUT(HconSend,                /* connection handle               */
	      HobjSend,          /* object handle                   */
	      &md,                 /* message descriptor              */
	      &pmo,                /* default options                 */
	      strlen(pcpData),              /* buffer length                   */
	      pcpData,              /* message buffer                  */
	      &CompCode,           /* completion code                 */
	      &Reason);            /* reason code                     */

	strcpy(clMyTime,md.PutDate);
	clMyTime[16] = '\0';
	/* report reason, if any */
	dbg(TRACE,"MQPUT ended with reason code <%d>, Time: <%s>", Reason, clMyTime);
	if (Reason != MQRC_NONE)
	{
		sleep(1);
	}
	}
	while(Reason != MQRC_NONE);

	if (Reason == MQRC_NONE)
	{
	}
	else
	{


	    if(Reason == 2009)
	    {
		dbg(TRACE,"%05d: Due to ReasonCode 2009 reconnect to QMANAGER for sending to MQ",__LINE__);
		if(strlen(cgQMgrSend) > 0)
		{
		    ilRC = ConnectMQ(cgQMgrSend, &HconSend);
		    ilRC = OpenSendMQ();
		}
		else
		{
		    dbg(TRACE,"%05d: Send-Qmanager not configured",__LINE__);
		}

	    }
	}



    dbg(TRACE,"-----SendToMq end -----");

    return ilRC;
}
