#ifndef _DEF_mks_version_nmghdl_h
  #define _DEF_mks_version_nmghdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_nmghdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/nmghdl.h 1.2 2004/07/27 16:48:12SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						                                  */
/*									                                                      */
/*  Program	  :     						                                          */
/*  Revision date :							                                          */
/*  Author    	  :							                                          */
/*  								                                                  	  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef 	_NMGHDL_H
#define 	_NMGHDL_H


/* allgemeine Defines */
#define 	IUSER_MSG					1 /* Typ User-Message	*/		
#define 	ICONFLICT_MSG				2 /* Typ Conflict-Message */
#define 	IERROR_MSG					3 /* Typ Error-Message */
#define 	IFIRST_MSG					IUSER_MSG  /* Erste moegliche Message */
#define	ILAST_MSG					IERROR_MSG /* Letzte moegl. Message */
#define 	IMSG_COUNTER				4    /* Anzahl der moeglicher Message */
#define	IUSE_SECONDARY_DEVICE	0  	 /* secondary device wird benutzt */
#define	IPERMS						0666   /* fuer open */
#define	SLOG_EXTENSION				".old" /* fuer rename */
#define	ILOG_COUNTER				4	 /* Anzahl moeg. Logfiles */
#define	ITEXT_AUS_FILE				0    /* Messagetext aus Textfile */
#define	ITEXT_AUS_DB				1    /* Messagetext aus DB */
#define	IREALLOC_COUNTER			50	 /* alle 50 Zeilen neu allokieren */
#define	SHEADER_TEXT          "STARTUP MESSAGE HANDLER >> %s << %s %s\n"
#define	IINDI_COUNTER				3	 /* currently 3 dif.message types */
#define	SFILE_TEXT					"%s %s %-6s %5d : %s\n"
#define 	SMESSAGE_INTERN			"SMI" /* send message intern */
#define	SMESSAGE_EXTERN			"SME" /* send message extern */
#define	ILACO_SIZE					11	  /* 11 Byte fuer language code */
#define	IMESSAGE_TYP_SIZE			3	  /* 3 Bytes fuer Error/user/con */ 
#define	CDATA_SEPARATOR			';'	  /* Separator for Data */
#define	SDEFAULT_LACO				"DE"  /* default language code */

/* Returnwerte bei Fehlern */
#define	IPRIO_FAIL					-1	 /* prio-failure */
#define	ILOG_FAIL					-2   /* falscher Logtyp */
#define	IFIELD1_FAIL				-3   /* kein Language Code */
#define	IFIELD2_FAIL				-4	 /* kein msg_val */
#define	IFIELD3_FAIL				-5   /* kein prog-name */
#define	IFIELD4_FAIL				-6	 /* keine message-number */
#define 	IFIELD5_FAIL				-7	 /* keine prio */
#define	IFIELD6_FAIL				-8   /* keine flight number */
#define	IFIELD7_FAIL				-9   /* keine rot_no */
#define	IFIELD8_FAIL				-10  /* kein ref_fld */
#define	IFIELD9_FAIL				-11	 /* keine Message-Data */
#define	IMSGNO_FAIL					-12  /* MsgNo in keinem Nummernkreis */
#define	ICOMMAND_FAIL				-13  /* weder interne noch externe Me.*/
#define	IORIGIN_FAIL				-14  /* fehlerhafter Message Ursprung */

/* Buffergroessen */
#define 	ISQL_BUFFER_SIZE			256  /* max.Groesse des SQLStatements */
#define 	IDATA_AREA_SIZE			256  /* max. Groesse der Daten */
#define	IFILE_TEXT_SIZE			256  /* max. Groesse Textfile-Eintrag */
#define	IMIN_BUF						128  /* kleine geht nimmer */
#define	IMAX_BUF						512  /* groesser geht nimmer */

/* defines fuer Prioritaeten */
#define 	IPRIO_ADMIN					4	/* Systemadministrator */		
#define	IPRIO_AP						3   /* Alarmdrucker */
#define	IPRIO_DB						2   /* Datenbank */
#define	IPRIO_ASCII					1   /* Ascii-File */
#define	IPRIO_NOTHING				0   /* nothing to do with data */
#define	IMAX_PRIO					IPRIO_ADMIN /* hoechste Prioritaet */

/* defines fuer Nummernkreise */
#define	IMIN_ERROR_NUM				0
#define	IMAX_ERROR_NUM				1000
#define	IMIN_USER_NUM				2000
#define	IMAX_USER_NUM				3000
#define 	IMIN_CONF_NUM				4000
#define	IMAX_CONF_NUM				5000

/* Defines fuer Status-Word */
#define	IINITIALIZE					0x0000 /* fuer erste Initialisierung */
#define 	IDB_CONNECT					0x0001 /* DB-Connect Flag setzen */
#define 	IRESET_DB_CONNECT			~IDB_CONNECT	
#define 	IDATA_TO_ASCII				0x0002 /* Data in Ascii-File Flag */
#define 	IRESET_DATA_TO_ASCII		~IDATA_TO_ASCII	
#define 	IDATA_TO_DB					0x0004 /* Data in DB Flag setzen */
#define	IRESET_DATA_TO_DB			~IDATA_TO_DB	
#define	IDATA_TO_AP					0x0008 /* Data zum Alarm-Printer Flag */
#define	IRESET_DATA_TO_AP			~IDATA_TO_AP	
#define	IDATA_TO_ADMIN				0x0010 /* Data zum Administrator */
#define 	IRESET_DATA_TO_ADMIN		~IDATA_TO_ADMIN
/*
hier die Defines fuer Bit 2^5 - 2^6
#define 								0x0020
#define								0x0040					
*/
#define 	IUSER_LFOPEN				0x0080 /* Usr-Logfile ist geoeffnet */
#define 	IRESET_USER_LFOPEN		~IUSER_LFOPEN
#define	ICONF_LFOPEN				0x0100 /* Conflict-Logfile ist offen */
#define	IRESET_CONF_LFOPEN		~ICONF_LFOPEN
#define	IPRIM_LFOPEN				0x0200 /* Primary Device ist offen */
#define 	IRESET_PRIM_LFOPEN		~IPRIM_LFOPEN
#define 	ISECO_LFOPEN				0x0400 /* Secondary Device ist offen */
#define	IRESET_SECO_LFOPEN		~ISECO_LFOPEN
#define	IUSER_DB_MSGTXT			0x0800 /* User-Msgtext aus DB */
#define 	IRESET_USER_DB_MSGTXT	~IUSER_DB_MSGTXT
#define	ICONF_DB_MSGTXT			0x1000 /* Conflict-Msgtext aus DB */
#define	IRESET_CONF_DB_MSGTXT	~ICONF_DB_MSGTXT
#define	IERROR_DB_MSGTXT			0x2000 /* Error-Msgtext aus DB */
#define	IRESET_ERROR_DB_MSGTXT	~IERROR_DB_MSGTXT
#define	IMESSAGE_EXTERN			0x4000 /* Message v. Application */
#define	IRESET_MESSAGE_EXTERN	~IMESSAGE_EXTERN
#define 	IINIT_OK						0x8000 /* Initialisieung OK-Flag */
#define	IRESET_INIT_OK				~IINIT_OK	

/* Defines fuer USER-Message */
#define 	SDEFAULT_USR_LOGFILE		"./usrmsg.log" /* def.usr-msg-file */
#define	SDEFAULT_USR_TXTFILE		"./usrtxt.msg" /* def usr txr-file */
#define	SUSR_MESSAGE_TYP			"UM"
#define 	IUSER_LOG_SIZE				256 /* Buffergroesse fuer UsrLogFile */

/* Defines fuer CONFLICT-Message */
#define 	SDEFAULT_CONF_LOGFILE		"./cflmsg.log" /* def cfl-msg-file */
#define	SDEFAULT_CONF_TXTFILE		"./cfltxt.msg" /* def cfl txr-file */
#define	SCONF_MESSAGE_TYP			"CM"
#define 	ICONF_LOG_SIZE				256 /* Buffergroesse fuer ConfLogFile */

/* Defines fuer ERROR-Message */
#define	SDEFAULT_ERROR_TXTFILE	"./ceda.msg" /* def error txr-file */
#define	SERROR_MESSAGE_TYP		"EM"
#define 	IERROR_LOG_SIZE			256 /* Buffergroesse fur ErrorLogFile */

/* Macros ... */
#define	IsUserMsg(a)  ((a)>=IMIN_USER_NUM && (a)<=IMAX_USER_NUM)
#define	IsConfMsg(a)  ((a)>=IMIN_CONF_NUM && (a)<=IMAX_CONF_NUM)
#define	IsErrorMsg(a) ((a)>=IMIN_ERROR_NUM && (a)<=IMAX_ERROR_NUM)

#define	CTRL_STA(a) ((a) == HSB_STANDBY ? "STANDBY" : ((a) == HSB_COMING_UP ? "COMING UP" : ((a) == HSB_ACTIVE ? "ACTIVE" : ((a) == HSB_STANDALONE ? "STANDALONE" : ((a) == HSB_ACT_TO_SBY ? "ACTIVE TO STANDBY" : "OTHER STATUS")))))

/* Strukturen/Unions etc. */
typedef struct _msgtext
{
	int		iMsgNO;
	char		pcLaCo[ILACO_SIZE];
	char		*pcText;
} MsgText;

typedef struct _msg
{
	int			iAnzahl;
	MsgText		*prMsgText;	
} Msg;

/* Diverses */
typedef int (*MsgHandler)(void);

/* aus kompatibilitaesgruenden */
#define LANG_CODE_LEN	4	/* Language Code "0049" Germany */
#define FLIGHT_NO_LEN	8	/* "DLH4711f" 	*/
#define ROT_NO_LEN	10      /* "98667"    	*/
#define REF_FLD_LEN	20	/* referenzfeldname max. 6 Zeichen */
#define MSG_DATA_LEN	512	/* 512 messagetext */
#define MSG_ERR_TEXT_LEN 128	/* len. error text */
#define PROG_NAME_LEN	10	/* len. des Programmnamens	*/

typedef struct 
{
  int msg_no;
  int prio;
  char data[1];
}RTSBLK;

typedef struct 
{
  int msg_no;
  int prio;
  char flight_no[FLIGHT_NO_LEN + 1];
  char rot_no[ROT_NO_LEN + 1];
  char ref_fld[REF_FLD_LEN + 1];
  char msg_data[MSG_DATA_LEN +1];
}MSGBLK;

typedef struct
{
  MSGBLK	msgblk;
  char 		lang_code[LANG_CODE_LEN + 1];
  int 		msg_origin; 
  int 		msg_val;	
  char 		prog_name[PROG_NAME_LEN + 1];
}MSG_INFO_BLK;

#endif /*_NMGHDL_H */

