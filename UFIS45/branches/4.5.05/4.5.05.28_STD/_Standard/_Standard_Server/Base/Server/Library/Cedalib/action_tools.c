#ifndef _DEF_mks_version_action_tools_c
  #define _DEF_mks_version_action_tools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_action_tools_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/action_tools.c 1.2 2004/08/10 20:10:30SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/


#include <action_tools.h>
#include <tools.h>

/*X////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////X*/
struct _Actions *AddListItem(struct _Actions *prpNewAction,
			     					  struct _Actions *prpPrevAction,
			     					  struct _ActionData *rpActionData)
{
	/* check pointer */
	if (rpActionData == NULL)
	{
		dbg(TRACE,"<AddListItem> parameter rpActionData is NULL");
		return NULL;
	}

	/* check parameter */
	if (rpActionData->iRouteID[0] <= 0 || rpActionData->iRouteID[0] > 127)
	{
		dbg(TRACE,"<AddListItem> number of route-ID's must be > 0 and < 128");
		return NULL;
	}

	/* this look good... */
	if (prpNewAction == NULL)
	{
		if ((prpNewAction = 
			 (struct _Actions*)malloc(sizeof(struct _Actions))) == NULL)
		{
			dbg(TRACE,"<AddListItem>: not enough memory to run (1)");
			return NULL;
		}
		else
		{
			/* only a message */
			dbg(TRACE,"<AddListItem> add new item");

			/* the mod-id's (origin, route) */
			/* save origin ID's */	
			prpNewAction->rActionData.iOriginID = rpActionData->iOriginID;

			/* save route ID's */
			memcpy((void*)prpNewAction->rActionData.iRouteID,
					 (const void*)rpActionData->iRouteID,
					 (size_t)((rpActionData->iRouteID[0]+1)*sizeof(short)));

			/* the command */
			strcpy(prpNewAction->rActionData.pcCommand, rpActionData->pcCommand);

			/* the object name */
			strcpy(prpNewAction->rActionData.pcObjName, rpActionData->pcObjName);

			/* the selection */
			if (rpActionData->pcSelection == NULL)
				prpNewAction->rActionData.pcSelection = NULL;
			else
				if ((prpNewAction->rActionData.pcSelection = 
							(char*)strdup(rpActionData->pcSelection)) == NULL)
				{
					dbg(TRACE,"<AddListItem>: not enough memory to run (2)");
					return NULL;
				}

			/* the Fields */
			if (rpActionData->pcFields == NULL)
				prpNewAction->rActionData.pcFields = NULL;
			else
				if ((prpNewAction->rActionData.pcFields = 
							(char*)strdup(rpActionData->pcFields)) == NULL)
				{
					dbg(TRACE,"<AddListItem>: not enough memory to run (3)");
					return NULL;
				}

			/* the data */
			if (rpActionData->pcData == NULL)
				prpNewAction->rActionData.pcData = NULL;
			else
				if ((prpNewAction->rActionData.pcData = 
							(char*)strdup(rpActionData->pcData)) == NULL)
				{
					dbg(TRACE,"<AddListItem>: not enough memory to run (4)");
					return NULL;
				}

			/* chain it */
			prpNewAction->prev = prpPrevAction;
			prpNewAction->next = NULL;
		}
	}
	else
		prpNewAction->next = 
				AddListItem(prpNewAction->next, prpNewAction, rpActionData);
	return prpNewAction;
}

/*X////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////X*/
struct _Actions *DeleteListItem(struct _Actions *prpAction, int ipNumber)
{
	int					ilCurrentNumber;
	struct _Actions	*prlSave;

	if (prpAction == NULL)
		return prpAction;
	if (ipNumber < 0)
		return prpAction;

	ilCurrentNumber = 0;
	prlSave = prpAction;
	while (prpAction)
	{
		if (ilCurrentNumber == ipNumber)
		{
			/* the next address */
			prlSave = prpAction->next;

			/* the first element */
			if (prpAction->prev != NULL)
				((struct _Actions*)(prpAction->prev))->next = prpAction->next;

			/* the last element */
			if (prpAction->next != NULL)
				((struct _Actions*)(prpAction->next))->prev = prpAction->prev;

			/* delete entries for this element */
			if (prpAction->rActionData.pcSelection != NULL)
			{
				free ((void*)prpAction->rActionData.pcSelection);
				prpAction->rActionData.pcSelection = NULL;
			}
			if (prpAction->rActionData.pcFields != NULL)
			{
				free ((void*)prpAction->rActionData.pcFields);
				prpAction->rActionData.pcFields = NULL;
			}
			if (prpAction->rActionData.pcData != NULL)
			{
				free ((void*)prpAction->rActionData.pcData);
				prpAction->rActionData.pcData = NULL;
			}
			if (prpAction != NULL)
			{
				free ((void*)prpAction);
				prpAction = NULL;
			}
			break;
		}
		ilCurrentNumber++;
		prpAction = prpAction->next;
	}
	return prlSave; 	
}

/*X////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////X*/
struct _Actions *DeleteList(struct _Actions *prpAction)
{
	struct _Actions	*prlHlp;

	if (prpAction == NULL)
		return prpAction;

	while (prpAction)
	{
		prlHlp = prpAction->next;
		if (prpAction->rActionData.pcSelection != NULL)
		{
			free ((void*)prpAction->rActionData.pcSelection);
			prpAction->rActionData.pcSelection = NULL;
		}
		if (prpAction->rActionData.pcFields != NULL)
		{
			free ((void*)prpAction->rActionData.pcFields);
			prpAction->rActionData.pcFields = NULL;
		}
		if (prpAction->rActionData.pcData != NULL)
		{
			free ((void*)prpAction->rActionData.pcData);
			prpAction->rActionData.pcData = NULL;
		}
		if (prpAction != NULL)
		{
			free ((void*)prpAction);
			prpAction = NULL;
		}
		prpAction = prlHlp;
	}
	return prpAction;
}

/*X////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////X*/
struct _Actions *CommitActions(struct _Actions *prpAction)
{
	/* here we should handle all actions */
	int					ilRC;
	int					ilFirst;
	int					ilCurRoute;
	struct _Actions	*prlRoot;

	/* valid? */
	if (prpAction == NULL)
		return prpAction;
	
	/* initialize */
	ilFirst = 1;
	prlRoot = prpAction;

	/* for all elements */
	while (prpAction)
	{
		/* set flag */
		ilRC = RC_SUCCESS;

		/* schedule actions now */
		for (ilCurRoute=1; 
			  ilCurRoute<=prpAction->rActionData.iRouteID[0] && ilRC == RC_SUCCESS;
			  ilCurRoute++)
		{		
			dbg(TRACE,"<CommitAction> calling ScheduleAction for route %d",
											prpAction->rActionData.iRouteID[ilCurRoute]);
			if ((ilRC = ScheduleAction(prpAction->rActionData.iOriginID,
											prpAction->rActionData.iRouteID[ilCurRoute],
									 		prpAction->rActionData.pcCommand,
									 		prpAction->rActionData.pcObjName,
									 		prpAction->rActionData.pcSelection,
									 		prpAction->rActionData.pcFields,
									 		prpAction->rActionData.pcData)) != RC_SUCCESS)
			{
				dbg(TRACE,"<CommitAction> ScheduleAction for route %d returns %d",
									prpAction->rActionData.iRouteID[ilCurRoute], ilRC);
			}
		}

		/* check returncode */
		if (ilRC != RC_SUCCESS)
		{
			if (ilFirst)
			{
				ilFirst = 0;
				prlRoot = prpAction;
			}
			prpAction = prpAction->next;
		}
		else
			prpAction = DeleteListItem(prpAction, 0);
	}
	
	/* list should be deleted */
	return ilFirst ? prpAction : prlRoot;
}

/*X////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////X*/
struct _Actions *RollbackActions(struct _Actions *prpAction)
{
	/* valid? */
	if (prpAction == NULL)
		return prpAction;
	
	/* very simple */
	return DeleteList(prpAction);
}




