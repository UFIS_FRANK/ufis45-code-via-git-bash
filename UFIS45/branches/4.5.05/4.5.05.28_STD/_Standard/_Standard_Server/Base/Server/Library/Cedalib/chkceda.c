#ifndef _DEF_mks_version_chkceda_c
  #define _DEF_mks_version_chkceda_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkceda_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/chkceda.c 1.4 2006/01/21 00:26:38SGT jim Exp  $";
#endif /* _DEF_mks_version */

/*
20060117 JIM: Checking for AIX
*/
/*	#define	CHKCEDA	*/
#include	"chkceda.h"


/*	extern	CONFPAR	clCfgEnvPath;	*/
/*	extern	CONFPAR	pSGKernelArray[];
extern	CONFPAR	pSGParArray[];
extern	CONFPAR	pSGPathArray[];
extern	CONFPAR	pSGFileArray[];                            
extern	CONFPAR	pSGServArray[];

extern	int		iGKernelCount;
extern	int		iGEnvCount;
extern	int		iGPathCount;
extern	int		iGFileCount;
extern	int		iGServCount;
*/
extern	debug_level;

int	CheckEnvVariables(char *,int,char *,FILE *);
int	CheckPath(int,char *,FILE *);
int	CheckPathComplete(int,char *,FILE *);
int	CheckEnvVar(int,int,FILE *,CONFPAR *,int);
int	GetUserIdAndGroup(char *,int *,int *);
int	CheckUserRights(char *,char *,mode_t,FILE *);
void	PrintRights(mode_t,char *,int);
void PrintPassWordInfo(char *,FILE *);
void	PrintFileStatInfo(char *,FILE *);
int	GetFileStatInfo(char *,struct stat *);

/*	***************************************************************************	*/
/*	Funktion CheckEnvVariables()											*/
/*	CheckEnvVariables() pr�ft ob die �bergebene Environment-Variable existiert		*/
/*	und schreibt sie in den vorgesehenen Puffer. Existiert die Variable noch nicht, */
/*	gibt die Fkt. einen Fehlerwert zur�ck. �ber den Parameter iPMode kann eine Aus-	*/
/*	gabe in eine Datei erwirkt werden. Der Datei-Zeiger wird ebenfalls als Para-	*/
/*	meter �bergeben.													*/
/*	Input:	char	*cPEnvVar,	Characterstring mit dem Namen der Variablen		*/
/*			int	iPMode,		Betriebsmodus der Funktion, mit:				*/
/*							TRUE	= Ausgabe in Datei						*/
/*							FALSE= keine Ausgabe in Datei					*/
/*			char	*cPPathBuf,	Puffer f�r Pfadnamen						*/
/*			FILE	*FPDbgFile,	Zeiger auf Datei f�r Ausgaben					*/
/*	Output	int	SUCCESS	=	Environment-Variable existiert				*/
/*				FAILURE	=	Environment-Variable existiert nicht			*/
/*	***************************************************************************	*/
int	CheckEnvVariables(char *cPEnvVar,int iPMode,char *cPPathBuf,FILE *FPDbgFile)
{
	int	ilRC = SUCCESS;
	
	if (getenv(cPEnvVar) != (char *) NULL) {
		if (cPPathBuf != NULL) {
			sprintf(cPPathBuf,"%s",getenv(cPEnvVar));
			if (iPMode == TRUE) {
				if (FPDbgFile != (char *) NULL) {
					fprintf(FPDbgFile,"Environment Variable '%s' set to '%s'\n",cPEnvVar,cPPathBuf);
				}
				else {
					printf("No Debug-File\n");
					printf("Environment Variable '%s' set to '%s'\n",cPEnvVar,cPPathBuf);
					fflush(stdout);
				}
			}
		}
		else  {
			if (iPMode == TRUE) {
				if (FPDbgFile != (char *) NULL) {
					fprintf(FPDbgFile,"No Buffer for Path-String!\n");
					fprintf(FPDbgFile,"Environment Variable '%s' set to '%s'\n",cPEnvVar,cPPathBuf);
				}
				else {
					printf("No Debug-File\n");
					printf("No Buffer for Path-String!\n");
					printf("Environment Variable '%s' set to '%s'\n",cPEnvVar,cPPathBuf);
					fflush(stdout);
				}
			}
		}
	}
	else {
		ilRC = FAILURE;
		if (iPMode == TRUE) {
			if (FPDbgFile != (char *) NULL) {
				fprintf(FPDbgFile,"Environment Variable '%s' not set\n",cPEnvVar);
			}
			else {
				printf("No Debug-File\n");
				printf("Environment Variable '%s' not set\n",cPEnvVar);
				fflush(stdout);
			}
		}
	}
	return ilRC;
}	/*	end of CheckEnvVariables()	*/

/*	***************************************************************************	*/
/*	Funktion CheckPath()												*/
/*	CheckPath() pr�ft ob der �bergebene Pfad existiert. Falls gew�nscht schreibt 	*/
/*	CheckPath() die Ergebnisse in eine Datei. Der Datei Zeiger wird ebenfalls als	*/
/*	Parameter �bergeben.												*/
/*	Input:	int	iPMode,		Betriebsmodus der Funktion, mit:				*/
/*							TRUE	= Ausgabe in Datei						*/
/*							FALSE= keine Ausgabe in Datei					*/
/*			char	*cPPathName,	Puffer f�r Pfadnamen						*/
/*			FILE	*FPDbgFile,	Zeiger auf Datei f�r Ausgaben					*/
/*	Output	int	SUCCESS	=	Environment-Variable existiert				*/
/*				FAILURE	=	Environment-Variable existiert nicht			*/
/*	***************************************************************************	*/
int	CheckPath(int iPMode,char *cPPathName,FILE *FPDbgFile)
{
	int	ilRC = SUCCESS;

	if (cPPathName == (char *) NULL) {
		ilRC = FAILURE;
		if (iPMode == TRUE) {
			if (FPDbgFile != (char *) NULL) {
				fprintf(FPDbgFile,"Pointer to Path is NULL\n");
			}
			else {
				printf("No Debug-File\n"); 
				printf("Pointer to Path is NULL\n");
				fflush(stdout);
			}
		}
	}
	else {
		ilRC = access(cPPathName,F_OK);
		if (ilRC != SUCCESS) {
			ilRC = FAILURE;
			if (iPMode == TRUE) {
				if (FPDbgFile != (char *) NULL) {
					fprintf(FPDbgFile,"Path '%s' doesn't exist (errno = %d)\n",cPPathName,errno); 
				}
				else {
					printf("No Debug-File\n"); 
					printf("Path '%s' doesn't exist (errno = %d)\n",cPPathName,errno); 
					fflush(stdout);
				}
			}
		}
		else {
			if (iPMode == TRUE) {
				if (FPDbgFile != (char *) NULL) {
					fprintf(FPDbgFile,"Path '%s' exists\n",cPPathName);
				}
				else {
					printf("No Debug-File\n"); 
					printf("Path '%s' exists\n",cPPathName); 
					fflush(stdout);
				}
			}
		}
	}
	return ilRC;
}	/*	end of CheckPath()	*/
				

/*	***************************************************************************	*/
/*	Funktion CheckPathComplete()												*/
/*	CheckPath() pr�ft ob der �bergebene Pfad existiert und pr�ft mit Funktion		*/
/*	CheckPrivileges() die Rechte (Schreib-, Lese- und Ausf�hrrechte).			*/
/*	Falls gew�nscht schreibt CheckPath() die Ergebnisse in eine Datei. Der Datei-	*/
/*	Zeiger wird ebenfalls als Parameter �bergeben.							*/
/*	Input:	int	iPMode,		Betriebsmodus der Funktion, mit:				*/
/*							TRUE	= Ausgabe in Datei						*/
/*							FALSE= keine Ausgabe in Datei					*/
/*			char	*cPPathName,	Puffer f�r Pfadnamen						*/
/*			FILE	*FPDbgFile,	Zeiger auf Datei f�r Ausgaben					*/
/*	Output	int	SUCCESS	=	Environment-Variable existiert				*/
/*				FAILURE	=	Environment-Variable existiert nicht			*/
/*	***************************************************************************	*/
int	CheckPathComplete(int iPMode,char *cPPathName,FILE *FPDbgFile)
{
	int	ilRC = SUCCESS;

	ilRC = CheckPath(iPMode,cPPathName,FPDbgFile);
	if (ilRC != SUCCESS) {
		ilRC = FAILURE;
	}
	else {
		ilRC = CheckPrevileges(iPMode,cPPathName,FPDbgFile);
	}
	return ilRC;
}	/*	end of CheckPath()	*/
				


/*	***************************************************************************	*/
/*	Funktion CheckEnvVar()												*/
/*	CheckPath() pr�ft ob der �bergebene Pfad existiert und pr�ft mit Funktions		*/
/*	die Rechte (Schreib-, Lese- und Ausf�hrrechte).							*/
/*	Falls gew�nscht schreibt CheckPath() die Ergebnisse in eine Datei. Der Datei-	*/
/*	Zeiger wird ebenfalls als Parameter �bergeben.							*/
/*	Input:	int		iPMode,		Ausgabemodus der Funktion, mit:			*/
/*								TRUE	= Ausgabe in Datei					*/
/*								FALSE= keine Ausgabe in Datei				*/
/*			int		iPChkMode,	Pr�fmodus der Funktion, Bitmuster, mit:		*/
/*								CHECK_PATH = Pr�ft ob der Pfad existiert	*/
/*								CHECK_PRIVILEG = Pr�ft Privilegien des Pfades*/
/*			FILE		*FPDbgFile,	Zeiger auf Datei f�r Ausgaben				*/
/*			CONFPAR	*pSpCfgEnvPath,Puffre mit zu pr�fenden Env-Vars			*/
/*			int		ipCount,	Anzahl zu pr�fender Env-Vars	 			*/
/*	Output	int		SUCCESS	=	Environment-Variable existiert			*/
/*					FAILURE	=	Environment-Variable existiert nicht		*/
/*	***************************************************************************	*/

int	CheckEnvVar(int iPMode,int iPChkMode,FILE *FPDbgFile,CONFPAR *pSpCfgEnvPath,int ipCount)
{
	int	ili,
		ilRC;
	char	clCfgPath[MAX_PATH_LEN];

	for (ili = 0; (ili < ipCount) && (ili < MAX_ENV_VAR_NO); ili++) {
		memset(clCfgPath,0x00,MAX_PATH_LEN);
		ilRC = CheckEnvVariables(pSpCfgEnvPath[ili].name,iPMode,clCfgPath,FPDbgFile);
		if ( (ilRC == SUCCESS) &&
			(pSpCfgEnvPath[ili].path == 1) &&
			((iPChkMode & CHECK_PATH) == CHECK_PATH) ) {
			if ( (iPChkMode & CHECK_PRIVILEG) == CHECK_PRIVILEG )
				ilRC = CheckPathComplete(0,clCfgPath,FPDbgFile);
			else
				ilRC = CheckPath(FALSE,clCfgPath,FPDbgFile);
		}
		
	}
	return ilRC;
}	/*	end of CheckEnvVar()	*/

/*int	CheckPrevileges(mode_t iPMode,char *pcpPathName,FILE *pFpDbgFile)*/
int	CheckPrevileges(int iPMode,char *pcpPathName,FILE *pFpDbgFile)
{
	int	ilRC = SUCCESS;
	char	pclUser[20];

	ilRC = CheckEnvVariables("CCSUSER",FALSE,pclUser,pFpDbgFile);
	if (ilRC == FAILURE) {
		if (pFpDbgFile != NULL) {
			fprintf(pFpDbgFile,"Env-Var CCSUSER not set\nCan not check Access-Mode without!\n");
			fprintf(pFpDbgFile,"************************************************************\n");
			fflush(pFpDbgFile);
		}
		else {
			printf("Env-Var CCSUSER not set\nCan not check Access-Mode without!\n");
			printf("************************************************************\n");
			fflush(stdout);
		}
	}
	else {
		ilRC = CheckUserRights(pcpPathName,pclUser,iPMode,pFpDbgFile);
	}
	return ilRC;
}	/*	end of CheckPrevileges()	*/

int	GetUserIdAndGroup(char *pcpUser,int *pipUid,int *pipGid)
{
	int	ilRC = SUCCESS;
	struct passwd	/* PlPassWord,	*/
				*PlpPassPoint;

	if ( (PlpPassPoint = getpwnam(pcpUser)) == (struct passwd *) NULL)
		ilRC = FAILURE;
	else {
		*pipUid = (int) PlpPassPoint->pw_uid;
		*pipGid = (int) PlpPassPoint->pw_gid;
	}
	return ilRC;
}	/*	end of GetUserIdAndGroup()	*/

int	CheckUserRights(char *pcpFileName,char *pcpUser,mode_t ipRights,FILE *pFpDbgFile)
{
	int			ilRC = SUCCESS;
	struct stat	SlFileStat,
				*pSlFileStatPointer;
	char			pclErrorText[200];
	int			ilUid,
				ilGid;
	char			pclModeout[1000];


	pSlFileStatPointer = &SlFileStat;
	if ( (pcpFileName == NULL) || (pcpUser == NULL)/* || (pFpDbgFile == NULL) */) {
		sprintf(pclErrorText,"CheckUserRights() returns error as a parameter is invalid\n");
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		ilRC = FAILURE;
	}	/*	parameter error	*/
	else if ( (ilRC = stat(pcpFileName ,pSlFileStatPointer)) == -1 ) {
		sprintf(pclErrorText,"Routinte stat() returns error %d for File '%s'\n",errno,pcpFileName);
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		ilRC = FAILURE;
	}	/*	Stat() returns error	*/
	else {
		ilRC = GetUserIdAndGroup(pcpUser,&ilUid,&ilGid);
		if (ilRC != SUCCESS) {
			sprintf(pclErrorText,"CheckUserRights():\nGetUserIdAndGroup returns error for user '%s', no check possible\n",pcpUser);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = FAILURE;
		}
		else {
/*			PrintPassWordInfo(pcpUser,pFpDbgFile);
			PrintFileStatInfo(pcpFileName,pFpDbgFile);
*/
			sprintf(pclErrorText,"Checking User Rights on File '%s' now\n",pcpFileName);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			if ((int) pSlFileStatPointer->st_uid != ilUid) {
				sprintf(pclErrorText,"User is not owner of File\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
			else {
				sprintf(pclErrorText,"User is owner of File\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
			if ((int) pSlFileStatPointer->st_gid != ilGid) {
				sprintf(pclErrorText,"User-Group is not Group of File\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
			else {
				sprintf(pclErrorText,"User-Group and Group of File are identical\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
/*			if ( (ilRC = pSlFileStatPointer->st_mode & ipRights) == ipRights ) {
				sprintf(pclErrorText,"All Rights on File are set as expected, mode = '%x', expected '%x', return = %d\n",pSlFileStatPointer->st_mode,ipRights,ilRC);
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
			else {
				sprintf(pclErrorText,"NOT all Rights on File are set as expected, see beyond, mode = '%x', expected '%x', return = %d\n",pSlFileStatPointer->st_mode,ipRights,ilRC);
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}
*/
			sprintf(pclErrorText,"File/Directory-mode defines the following Rights\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			PrintRights(pSlFileStatPointer->st_mode,pclModeout,1000);
			PrintToSpezifiedFile(pclModeout,pFpDbgFile);
			sprintf(pclErrorText,"************************************************************\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
/*			sprintf(pclErrorText,"The following Rights are expected\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			PrintRights(ipRights,pclModeout,1000);
			PrintToSpezifiedFile(pclModeout,pFpDbgFile);
*/
		}
	}
	return ilRC;
}	/*	end of CheckUserRights()	*/

void	PrintRights(mode_t ipMode,char *pcpOutput,int ipBufLen)
{
	int	ili;
	int	ilRC;
	int	ilActLen;
	char	pclErrorString[100];

/*	sprintf(pcpOutput,"mode is %d (%x)\n",ipMode,ipMode);	*/
	if (pcpOutput != NULL) {
		memset(pcpOutput,0x00,ipBufLen);
/*		sprintf(pcpOutput,"mode is %d (%x)\n",ipMode,ipMode);	*/
		for (ilActLen = 0, ili = 0; ili < MAX_RIGHT_NO; ili++) {
			memset(pclErrorString,0x00,100);
			if ( (ilRC = ipMode & plGModeBufer[ili]) != plGModeBufer[ili] )
				/*sprintf(pclErrorString,"%s not set (act = %x, buf = %x, with rc = %d)\n",pcGModeString[ili],ipMode,plGModeBufer[ili],ilRC)*/;
			else
				sprintf(pclErrorString,"%s set\n",pcGModeString[ili]);
			if ( (ilActLen += strlen(pclErrorString)) < ipBufLen )
				strcat(pcpOutput,pclErrorString);
			else
				break;
		}
	}
	return;
}	/*	end of PrintRights()	*/
			

void PrintPassWordInfo(char *UserName,FILE *TheFile)
{
	int	ilRC = SUCCESS;
	struct passwd	/* PlPassWord,	*/
				*PlpPassPoint;

	if ( (TheFile != NULL) && (UserName != NULL) ) {
		if ( (PlpPassPoint = getpwnam(UserName)) == (struct passwd *) NULL)
			fprintf(TheFile,"getpwnam() failed for User '%s'\n",UserName);
		else {
			fprintf(TheFile,"getpwnam() Info for User '%s'\n",UserName);
			fprintf(TheFile,"PW-Name = '%s'\n",PlpPassPoint->pw_name);
			fprintf(TheFile,"PW-PW = '%s'\n",PlpPassPoint->pw_passwd);
			fprintf(TheFile,"PW-Uid = '%d'\n",(int) PlpPassPoint->pw_uid);
			fprintf(TheFile,"PW-Gid = '%d'\n",(int) PlpPassPoint->pw_gid);
#if !defined(_LINUX) && !defined(_AIX)
			fprintf(TheFile,"PW-Age = '%s'\n",PlpPassPoint->pw_age);
			fprintf(TheFile,"PW-Com = '%s'\n",PlpPassPoint->pw_comment);
#endif
			fprintf(TheFile,"PW-Gecos = '%s'\n",PlpPassPoint->pw_gecos);
			fprintf(TheFile,"PW-Dir = '%s'\n",PlpPassPoint->pw_dir);
			fprintf(TheFile,"PW-Shell = '%s'\n",PlpPassPoint->pw_shell);
		}
		fflush(TheFile);
	}
	else if (UserName != NULL) {
		printf("PrintPassWordInfo failed for User '%s', No File-Parameter\n",UserName);
		fflush(stdout);
	}
	else {
		if (TheFile != NULL) {
			fprintf(TheFile,"PrintPassWordInfo failed, No User-Name\n");
			fflush(TheFile);
		}
		else {
			printf("PrintPassWordInfo failed, No File-Parameter and no User-Name\n");
			fflush(stdout);
		}
	}
	return;
}

void PrintFileStatInfo(char *file,FILE *TheFile)
{
	int	ilRC = SUCCESS;
	struct stat	statstruct;

	if ( (TheFile != NULL) && (file != NULL) ) {
		if ( (ilRC = stat(file,&statstruct)) != SUCCESS)
			fprintf(TheFile,"stat() failed for file '%s' with error %d\n",file,errno);
		else {
			fprintf(TheFile,"stat() Info on file '%s'\n",file);
			fprintf(TheFile,"Stat-Mode = '%ld'\n",(long) statstruct.st_mode);
			fprintf(TheFile,"Stat-ino_t = '%ld'\n",(long) statstruct.st_ino);
			fprintf(TheFile,"Stat-dev = '%ld'\n",(long) statstruct.st_dev);
			fprintf(TheFile,"Stat-rdev = '%ld'\n",(long) statstruct.st_rdev);
			fprintf(TheFile,"Stat-nlink = '%ld'\n",(long) statstruct.st_nlink);
			fprintf(TheFile,"Stat-uid = '%ld'\n",(long) statstruct.st_uid);
			fprintf(TheFile,"Stat-gid = '%ld'\n",(long) statstruct.st_gid);
			fprintf(TheFile,"Stat-size = '%ld'\n",(long) statstruct.st_size);
			fprintf(TheFile,"Stat-atime = '%ld'\n",(long) statstruct.st_atime);
			fprintf(TheFile,"Stat-mtime = '%ld'\n",(long) statstruct.st_mtime);
			fprintf(TheFile,"Stat-ctime = '%ld'\n",(long) statstruct.st_ctime);
		}
		fflush(TheFile);
	}
	else if (file != NULL) {
		printf("PrintFilestatInfo failed for File '%s', No File-Parameter for debug-file\n",file);
		fflush(stdout);
	}
	else {
		if (TheFile != NULL) {
			fprintf(TheFile,"PrintPassWordInfo failed, No User-Name\n");
			fflush(TheFile);
		}
		else {
			printf("PrintPassWordInfo failed, No File-Parameter and no User-Name\n");
			fflush(stdout);
		}
	}
	return;
}


int	GetFileStatInfo(char *file,struct stat *statstruct)
{
	int	ilRC = FAILURE;

	if ( (statstruct != NULL) && (file != NULL) ) {
		if ( (ilRC = stat(file,statstruct)) != SUCCESS) {
			if (debug_level == DEBUG) {
				printf("In GetFileStatInfo: stat() failed for file '%s' with error %d\n",file,errno);
				fflush(stdout);
			}
			ilRC = FAILURE;
		}
	}
	else if (file != NULL) {
		if (debug_level == DEBUG) {
			printf("PrintFilestatInfo failed, No File-Parameter\n");
			fflush(stdout);
		}
	}
	else if (statstruct != NULL) {
		if (debug_level == DEBUG) {
			printf("PrintFilestatInfo failed for File '%s', No STAT-Parameter\n",file);
			fflush(stdout);
		}
	}

	return ilRC;
}

