#ifndef _DEF_mks_version_libccstr_h
  #define _DEF_mks_version_libccstr_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_libccstr_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/libccstr.h 1.3 2005/03/08 17:18:51SGT heb Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef _LIBCCSTR_INC
#define _LIBCCSTR_INC


/* STRING HANDLERS                          */
/* -------------------------------------------------------------------- */

char *str_new_chr (int, char *);
void str_cpy_chg (char *, char *, int);
int  str_cpy_mid ( char *, int, int, char *, int, int, char *);
void str_chg_upc (char *);
int  str_fnd_str (int, int, char *, char *);
int  str_fnd_fst(int, int, char *, char *, int *, int);
int  str_fnd_lst(int, int, char *, char *, char *, int);
void str_trm_lft(char *, char *);
void str_trm_rgt(char *, char *, int);
void str_trm_all(char *, char *, int);
void str_mov_lft(int, int, char *);
int  get_item(int, char *, char *, int, char *, char *, char *);
int  get_real_item (char *, char *, int);
int  GetWords (int,int,char *, char *);
int str_fnd_buf(int, int, char *, char *, int *, int *, char *);
char *mysGet_data(char *, char *, char *);
char *CedaGetKeyItem(char *, long *, char *, char *, char *, int);

/* STRINGS AND NUMERICAL VALUES                     */
/* -------------------------------------------------------------------- */
int  chk_num_val(char *);
/* int  chk_tim_val(char *); */
int  str_cut_int(char *);
int  str_get_int(int *, char *, int);
int  chck_range (int, int, int, int);
int GetItemBounds(char *, int, int *, int *, int *);

#endif
