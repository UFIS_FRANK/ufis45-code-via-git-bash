CREATE OR REPLACE VIEW WEBFIDS_VIEW as
SELECT b.dpid DISPLAYID, 
	   b.dadr IP_ADDRESS, 
       b.devn DEVICENAME, 
       b.dffd FILTERFIELD, 
       b.dfco FILTERCONTENT
FROM dsptab a, devtab b
WHERE a.dpid = b.dpid
AND trim(upper(b.grpn)) = 'WEBFIDS'
ORDER BY DADR   
