#ifndef _DEF_mks_version_catchal_h
  #define _DEF_mks_version_catchal_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_catchal_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/catchal.h 1.2 2004/07/27 16:46:51SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************
 T H E   M A S T E R   I N C L U D E   F I L E 
 Program        : catch_all     
 Creation date  : Thu Nov 07 13:18:18 GMT 1991 
 Revision date  :        
 -----------------------------------------------------------------------------
 (c) Computer Communication GmbH 
*****************************************************************************/
#include "glbdef.h"
#include "signal.h"

#ifndef __CATCHAL_INC
#define __CATCHAL_INC


void catch_all(void(*)(int));

#endif

