#ifndef _DEF_mks_version_fdimvt
  #define _DEF_mks_version_fdimvt
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdimvt[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdimvt.src 1.13 2008/04/01 15:59:32SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  SMI                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDIMVT handle functions                                                    */
/* -----------------------                                                    */
/* static int HandleMVT (char *pcpData)                                       */
/* static int SendMVTLOARes()                                                 */

static int HandleMVT(char *pcpData)
{
  int i,j,k,ilRC,ilHit;
  int ilRC2;
  int ilYear=0;
  int ilMonth=0;
  int ilLine,ilCol,ilPos,ilCount;
  char pclFunc[]="HandleMVT:";
  char *pclData;
  char *pclSaveSIPos=NULL;
  char pclResult[100],pclLine[100];
  T_TLXINFOCMD *prlInfoCmd;
  T_TLX_ARRAY_ELEMENT *prlTlxArrayElement;
  T_TLXRESULT *prlResult;
  T_TLXRESULT prlTlxMonth;
  T_TLXRESULT prlTlxYear;

  memset(&prlTlxMonth,0x00,TLXRESULT);
  memset(&prlTlxYear,0x00,TLXRESULT);

  prlInfoCmd = &prgInfoCmd[0][0];

  pclData = pcpData;

  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 

  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC < 2 && ilLine < 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"MVT") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     ilRC = PruneFlight(pclResult,prgMasterTlx,0);
     prlResult = CCSArrayGetData(prgMasterTlx,"FDate");
     if (ilRC == RC_SUCCESS &&
         prlResult != NULL &&
         rgTlxInfo.TlxDay[0] != 0x00 &&
         rgTlxInfo.TlxMonth[0] != 0x00)
     {
        if (atoi(prlResult->DValue) > atoi(rgTlxInfo.TlxDay))
        {
           dbg(DEBUG,"%s Change Day of flight ",pclFunc);
           ilMonth = atoi(rgTlxInfo.TlxMonth);
           dbg(DEBUG,"%s Month <%d>",pclFunc,ilMonth);
           switch (ilMonth)
           {
              case 1: /* in JAN switch back to DEC*/
                 sprintf(prlTlxMonth.DValue,"12");
                 /* decrement year*/
                 if (rgTlxInfo.TlxYear[0] != 0x00 )
                 {
                    ilYear = atoi(rgTlxInfo.TlxYear);
                    switch (ilYear)
                    {
                       case 0: /* if year 00 goto 99*/
                          sprintf(prlTlxYear.DValue,"99");
                          break;
                       default: /* else Year--*/
                          sprintf(prlTlxYear.DValue,"%2.2d",ilYear-1);
                          break;
                    } /*end switch year*/
                    strcpy(prlTlxYear.DName,"FYear");
                    CCSArrayAddUnsort(prgMasterTlx,&prlTlxYear,TLXRESULT);
                 }/* end if Year*/
                 break;
              default:
                 sprintf(prlTlxMonth.DValue,"%2.2d",ilMonth-1);
                 dbg(DEBUG,"%s Case Default TlxMonth-1 <%s> ",pclFunc,prlTlxMonth.DValue);
                 break;
           } /* end switch month */
           strcpy(prlTlxMonth.DName,"FMonth");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxMonth,TLXRESULT);
        }/* end if FDate > TlxDay */
     }/* end if all values available*/
  }

  if (ilRC == RC_SUCCESS)
  {  /* Step behind Flight line and point to \n  */
     ilRC = CountToDelim(&(pclData[rgTlxInfo.TxtStart]),ilLine-1,'\n');
     /* Manipulate the TxtStart to start next search without 
        flightline */
     rgTlxInfo.TxtStart += ilRC-1;
     ilRC=RC_SUCCESS;
  }
  if (ilRC == RC_SUCCESS)
  {
     ilRC = SearchInfoCmds(pcpData);
  }
  /* Restore 'S' at SI position  */
  if (pclSaveSIPos != NULL)
     *pclSaveSIPos = 'S';

  /* if (ilRC == RC_SUCCESS) */
     ilRC = SendTlxRes(FDI_MVT);

  return ilRC;
} /* end of HandleMVT  */



static int SendMVTLOARes()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="SendMVTLOARes:";
  int i,j;
  char pclIdent[32];
  char pclFieldList[1000];
  char pclValueList[2000];
  char pclSelection[100];
  T_TLXRESULT *prlTlxPtr = NULL;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem = NULL;
  char pclSqlBuf[1024];
  char pclTmpValue[32];
	  
  memset(pclFieldList,0x00,sizeof(pclFieldList));
  memset(pclValueList,0x00,sizeof(pclValueList));
  memset(pclSelection,0x00,sizeof(pclSelection));

  /*    First delete "old" entries from table */
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FltKey");
  if (prlTlxPtr  != NULL )
  {
     if (strlen(pcgAftUrno) > 0)
     {
        sprintf(pclSelection,"WHERE FLNU = %s AND DSSN = 'MVT'",pcgAftUrno);
     }
     else
     {
        sprintf(pclIdent,"%sMVT",prlTlxPtr->FValue);		
        sprintf(pclSelection,"WHERE IDNT = '%s'",pclIdent);
     }
     strcpy(pclSqlBuf,"DELETE FROM LOATAB ");
     strcat(pclSqlBuf,pclSelection);

     prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"MVT");
     if (prlCfgArrElem == NULL)
     {
	dbg(TRACE,"%s No Fields defined",pclFunc); 
        return RC_FAIL;
     }
     strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,SSST,APC3,VALU,DSSN,FLNU,RURN");
     if (igNewLoatab == TRUE)
     {
        strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
        if (igNewLoatabUkey == TRUE)
           strcat(pclFieldList,",UKEY");
     }
     sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
     ilRC = GetLOAData(prlCfgArrElem,"PXT",&prlTlxPtr);
     strcat(pclValueList,"PAX,T,,,,");
     if (prlTlxPtr != NULL)
     {
        if (strlen(prlTlxPtr->FValue) > 0)
        {
	   if (strlen(prlTlxPtr->FValue) > 6)
           {
              strncpy(pclTmpValue,prlTlxPtr->FValue,6);
              pclTmpValue[6] = '\0';
              strcat(pclValueList,pclTmpValue);
           }
           else
           {
              strcat(pclValueList,prlTlxPtr->FValue);
           }
           strcat(pclValueList,",MVT");
           if (strlen(pcgAftUrno) > 0)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgAftUrno);
           }
           else
           {
              strcat(pclValueList,",0");
           }
           if (strlen(pcgNextTlxUrno) > 0)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgNextTlxUrno);
           }
           else
           {
              strcat(pclValueList,",0");
           }
           if (igNewLoatab == TRUE)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgMvtTotPax);
           }
           ilRC = FdiSendSql(pclSqlBuf);
           ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                               pcgTwStart,pcgTwEndNew,FALSE,FALSE);
	   if (ilRC != RC_SUCCESS)
           {
	      return RC_FAIL;
	   }
        }
     } /* if value n/a*/
  }
  else
  {
     dbg(DEBUG,"%s No FltKey avail.",pclFunc); 
  }    
  return RC_SUCCESS;
} /* End of SendMVTLoaRes */

