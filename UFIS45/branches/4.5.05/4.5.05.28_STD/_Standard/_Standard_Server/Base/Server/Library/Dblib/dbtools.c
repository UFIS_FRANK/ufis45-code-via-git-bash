#ifndef _DEF_mks_version_dbtools_c
  #define _DEF_mks_version_dbtools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dbtools_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/dbtools.c 1.20 2012/02/14 15:11:43SGT gfo Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* DBTOOLS.C		                                                              */
/*                                                                            */
/* Author         :    BST                                                    */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :     MOS 	30 Nov 1999					                            */
/*			replaced 'SUCCESS' with 'RC_SUCCESS'                                  */
/*			in HandleArray (2x)				                                            */
/*                                                                            */
/*              MOS 3.July 2000                                         */
/*                      If flag NO_AUTO_INCREASE is specified                 */
/*                      then acnu won't be increased                    */
/*                      ->required for IDS interface Athen              */
/*                                                                      */
/*                                                                            */
/* 20050517 JIM: dbg of GetNextNumbers with Count and Key                     */
/* 20050520 JIM: commented out "dbg of GetNextNumbers with Count and Key"     */
/* 20120214 GFO: Added DB_LoadUrnoList UFIS-1484                              */
/******************************************************************************/

#define STH_USE
#define STH_IS_USED_IN_LIB

#if defined(_LINUX)
#define LONG_MAX     2147483647L
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <values.h>

/* Define OWN_NAP, if the nap function is not supported by the unix */
#ifdef OWN_NAP
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/param.h>
#endif

#include "glbdef.h"
#include "db_if.h"
#include "tools.h"
#include "cli.h"
#include "AATArray.h"

extern int debug_level;

static short igSnoTabCursor=0;
static short igUrnoCursor;
extern FILE	*outp;

static char cgInterface[34] = "";
static char cgType[34] = "";
static char cgLastConnection[34] = "";
static char cgConnectionState[34] = "";

static char pcgUfisCedaCfg[256] = "";
static char pcgDbTmpBuf1[1024] = "";
static char pcgDbTmpBuf2[1024] = "";
static char pcgDbTmpBuf3[1024] = "";

static blNewUrnoMethod = TRUE;

static void DB_GetUrnoRangeInit(char *pcpKeyWord, int *pipSetFlag);

/* UFIS-1485 */
int DB_LoadUrnoList(int ipNumUrnos ,char *pcpUrno ,char* pcgTableKey);

/* ****************************************************************
 * Function: 	 	GetNextVal_01
 * Parameter:		OUT: char *pcpDataArea 	Data Storage
 * Return: 			Unique Record Number
 * Description: 	Determines next urno.
 * 			        Editted by GSA  13-08-97
 * *****************************************************************/
int GetNextValues(char *pcpDataArea,int ipCnt)
{
/* JWE mapped to new function GetNextNumbers (see below) */
int ilRc;
char pclNewDataArea[3000];
char pclFlag[5];
*pclNewDataArea = '\0';
*pclFlag = '\0';

strcpy(pclFlag,"GNV");

if ((ilRc=GetNextNumbers("SNOTAB",pclNewDataArea,ipCnt,pclFlag)) ==
		RC_SUCCESS)
{
	sprintf(pcpDataArea,"%s",pclNewDataArea);
}
return ilRc;
/*JWE END*/

#if 0
int	ilRC=DB_SUCCESS;	/* Len of buffer	*/
int i=0,ilLen=0;
static short ilSelCursor=0,ilUpdateCursor=0;
static char pclUrno[32]; /* Temp. storage for URNO count  */
char pclSqlBuf[128]="SELECT ACTUAL_SEQUENCE FROM SNOTAB FOR UPDATE";

	dbg(DEBUG,"GetNextValues: Request for <%d> Urno",ipCnt);
	if(ipCnt<=0){
		ilRC=RC_FAIL;
		dbg(TRACE,"GetNextValues: Error. Request to generate -ve/zero URNOs");
		return RC_FAIL;
	}
	memset(pcpDataArea,'\0',10);
	dbg(DEBUG,"GetNextValues: ilSelCursor<%hd>",ilSelCursor);
	snap(pcpDataArea,64,outp);
	fflush(outp);
	ilRC=sql_if(START,&ilSelCursor,pclSqlBuf,pcpDataArea);
	dbg(DEBUG,"GetNextValues: SqlBuf<%s> pcpDataArea<%s> ilSelCursor<%hd>. Returned ilRC<%d>",pclSqlBuf,pcpDataArea,ilSelCursor,ilRC);
	/* snap(pcpDataArea,64,outp);
	fflush(outp); */
	if(ilRC==DB_SUCCESS){
		sprintf(pclSqlBuf,"UPDATE SNOTAB SET ACTUAL_SEQUENCE=:Val0");
		sprintf(pclUrno,"%d",atoi(pcpDataArea)+ipCnt);
		dbg(DEBUG,"GetNextValues: Update SnoTab to <%s> i.e. %d URNO(s). SqlBuf<%s> OldValue<%s>",pclUrno,ipCnt,pclSqlBuf,pcpDataArea);
		ilRC=sql_if(NOACTION,&ilUpdateCursor,pclSqlBuf,pclUrno);
		dbg(DEBUG,"GetNextValues: Update to URNO<%s> ilUpdateCursor<%hd>",pclUrno,ilUpdateCursor);
		/* snap(pclUrno,64,outp);
		fflush(outp); */
		if (ilRC==DB_SUCCESS) {
			commit_work();
			dbg(DEBUG,"GetNextValues: Committing with pcpDataArea<%s>",pcpDataArea);
		} /* end if */
		else {
			dbg(TRACE,"GetNextValues: Update on SNOTAB failed!! Return code<%d>",ilRC);
			check_ret(ilRC);
			ilRC=RC_FAIL;
			rollback();
		} /* end else */
	} else {
		/* lock failed */
		dbg(TRACE,"GetNextValues: ilRC<%d>. Lock on SNOTAB failed!!",ilRC);
		ilRC=RC_FAIL;
	}
	dbg(DEBUG,"GetNextValues: Returning %d. URNO<%s> pclUrno<%s> Count<%d>",ilRC,pcpDataArea,pclUrno,ipCnt);

	return ilRC;
#endif
} /* end of GetNextValue */


/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////
*/
int HandleArray(int ipFunction, short spNoOfRows, char *pcpTableName,
					 UINT ipNoOfFields, char **pcpFields, short *pspFieldLength,
				    char ***pcpData)
{
	short				slActCursor;
	int				ilRC;
	UINT				ilCurRow;
	UINT				ilCurField;
	short				slUrnoLength;
	ULONG				lNextUrno;
	ULONG				lLen;
	char				pclSqlBuffer[iMAXIMUM];
	char				pclTmpBuf[iMIN_BUF_SIZE];
	char				pclUrnoArea[iMIN_BUF_SIZE];
	char				*pclS;
	char				*pclDataArea;

	/* check parameter */
	if (ipFunction < iINSERT_ARRAY || ipFunction > iUPDATE_ARRAY)
		return iFUNCTION_FAIL;
	if (pcpTableName == NULL)
		return iTABLE_FAIL;
	if (pcpFields == NULL)
		return iFIELD_FAIL;
	if (pspFieldLength == NULL)
		return iFIELDLENGTH_FAIL;
	if (pcpData == NULL)
		return iDATA_FAIL;

	/* init */
	slActCursor  = 0;
	slUrnoLength = 10;
	pclS			 = NULL;
	pclDataArea  = NULL;
	memset((void*)pclSqlBuffer, 0x00, iMAXIMUM);
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);

	/* calculate length of memory we need */
	/* this is for number of rows */
	lLen = 2;
	dbg(DEBUG,"<HandleArray> Len: %ld bytes", lLen);

	/* this is for URNO, we need it for insert */
	dbg(DEBUG,"<HandleArray> NoOfRow: %d UrnoLength: %d",
				spNoOfRows, slUrnoLength);
	if (ipFunction == iINSERT_ARRAY)
		lLen += (ULONG)((spNoOfRows*slUrnoLength)+2);
	dbg(DEBUG,"<HandleArray> Len: %ld bytes", lLen);

	/* this is for fields... */
	for (ilCurField=0; ilCurField<ipNoOfFields; ilCurField++)
	{
		dbg(DEBUG,"<HandleArray> FieldLength %d for field: %d",
					pspFieldLength[ilCurField], ilCurField);
		lLen += (ULONG)((spNoOfRows*(pspFieldLength[ilCurField]))+2);
	}

	/* get memory */
	dbg(DEBUG,"<HandleArray> try to allocate %ld bytes", lLen);
	if ((pclDataArea = (char*)malloc((size_t)lLen)) == NULL)
	{
		dbg(DEBUG,"<HandleArray> malloc failure...");
		return iMALLOC_FAIL;
	}

	/* clear it */
	memset((void*)pclDataArea, 0x00, lLen);

	/* set pointer */
	pclS = pclDataArea;

	/* copy number of rows */
	memcpy((void*)pclS, (short*)&spNoOfRows, sizeof(short));
	dbg(DEBUG,"<HandleArray> NoOfRows: %d", spNoOfRows);
	pclS += sizeof(short);

	/* for all fields, copy all to data area */
	for (ilCurField=0; ilCurField<ipNoOfFields; ilCurField++)
	{
		/* copy length of this field */
		memcpy((void*)pclS, (short*)&pspFieldLength[ilCurField], sizeof(short));
		dbg(DEBUG,"<HandleArray> LengthOfField(%d): %d",
				ilCurField, pspFieldLength[ilCurField]);

		/* next pointer position */
		pclS += sizeof(short);

		/* copy all data for this field */
		for (ilCurRow=0; ilCurRow<spNoOfRows; ilCurRow++)
		{
			sprintf(pclS, "%.*s",
					  pspFieldLength[ilCurField],
					  pcpData[ilCurField][ilCurRow]);
			pclS += pspFieldLength[ilCurField];
		}
	}

	/* where to go */
	switch (ipFunction)
	{
		case iINSERT_ARRAY:
			/* get (first) new urno... */
			memset((void*)pclUrnoArea, 0x00, iMIN_BUF_SIZE);
			if ((ilRC = GetNextValues(pclUrnoArea, (int)spNoOfRows)) != RC_SUCCESS)
			{
				if (pclDataArea != NULL)
					free((void*)pclDataArea);
				return iURNO_FAIL;
			}

			/* convert to long */
			lNextUrno = atol(pclUrnoArea);

			/* set length of urno in db */
			memcpy((void*)pclS, (short*)&slUrnoLength, sizeof(short));
			dbg(DEBUG,"<HandleArray> LengthOfUrno: %d", slUrnoLength);

			/* next pointer position */
			pclS += sizeof(short);

			/* copy all data for this field */
			for (ilCurRow=0; ilCurRow<spNoOfRows; ilCurRow++, lNextUrno++)
			{
				/* clear buffer */
				memset((void*)pclUrnoArea, 0x00, iMIN_BUF_SIZE);

				/* set buffer */
				sprintf(pclUrnoArea, "%ld", lNextUrno);

				/* copy to data-area */
				sprintf(pclS, "%.*s", 10, pclUrnoArea);

				/* set new pointer position... */
				pclS += slUrnoLength;
			}

			/* build SQL-Statememt */
			sprintf(pclSqlBuffer, "insert into %s (", pcpTableName);
			for (ilCurField=0; ilCurField<ipNoOfFields; ilCurField++)
			{
				strcat(pclSqlBuffer, pcpFields[ilCurField]);
				strcat(pclSqlBuffer, ",");
			}
			strcat(pclSqlBuffer, "urno) values (");
			for (ilCurField=0; ilCurField<ipNoOfFields+1; ilCurField++)
			{
				memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
				sprintf(pclTmpBuf,":V%d", ilCurField+1);
				strcat(pclSqlBuffer, pclTmpBuf);
				if (ilCurField < ipNoOfFields)
					strcat(pclSqlBuffer, ",");
			}
			strcat(pclSqlBuffer, ")");
			break;

		case iUPDATE_ARRAY:
			/* build SQL-Statememt */
			sprintf(pclSqlBuffer, "update %s set ", pcpTableName);

			for (ilCurField=0; ilCurField<ipNoOfFields-1; ilCurField++)
			{
				strcat(pclSqlBuffer, pcpFields[ilCurField]);
				strcat(pclSqlBuffer, "=");
				memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
				sprintf(pclTmpBuf,":V%d", ilCurField+1);
				strcat(pclSqlBuffer, pclTmpBuf);
				if (ilCurField < ipNoOfFields-2)
					strcat(pclSqlBuffer, ",");
			}
			strcat(pclSqlBuffer, " where urno=");
			memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
			sprintf(pclTmpBuf,":V%d", ilCurField+1);
			strcat(pclSqlBuffer, pclTmpBuf);
			break;
	}

	/* send this to database */
	dbg(DEBUG,"<HandleArray> SQL-Statement: <%s>", pclSqlBuffer);
	snap(pclDataArea, lLen, outp);
	if ((ilRC = sql_if(COMMIT|ARRAY|REL_CURSOR, &slActCursor,
							 pclSqlBuffer, pclDataArea)) != RC_SUCCESS)
	{
		/* rollback */
		rollback();
	}
	dbg(DEBUG,"<HandleArray> sql_if returns: %d", ilRC);

	/* free memory */
	if (pclDataArea != NULL)
		free((void*)pclDataArea);

	/* return to calling routine */
	return ilRC;
}
/* ****************************************************************/
/* Function: 	 	int GetNextNumbers (from NUMTAB for number-circle) */
/* Parameter:		IN: char *pcpKey (name of number circle)           */
/*             	IN: int ipCnt (acount of numbers to be returned)   */
/*             	IN: char *pcpFlag (for future use)                 */
/*             	OUT: char *pcpDataArea (first of the requested nr.)*/
/* Return: 			Unique Record Number(s)                            */
/* Description: Gets next number(s) from NUMTAB for a special      */
/*							circle which is identified by "key"								 */
/* Author:      JWE 16/11/1997                                     */
/* Update:                                                         */
/* *****************************************************************/
int GetNextNumbers(char *pcpKey, char *pcpDataArea, int ipCnt,
													char *pcpFlag)
{
	int	ilRc = DB_SUCCESS;			/* Return code */
	int ilGetRc = RC_SUCCESS;
	char pclFunc[] = "GetNextNumbers:";
	int ilMaxnOk = RC_SUCCESS;
	static short ilSCursor = 0;	/* Select-Cursor */
	static short ilUCursor = 0;	/* Update-Cursor*/
	static short ilMAXNCursor= 0;/* MAXN-Cursor*/
	static short ilMINNCursor= 0;/* MINN-Cursor*/
	static char pclUrno[16];  	/* Temp. storage for URNO count*/
	static char pclWhere[24]; 	/* storage for update */
	static char pclHostVars[44]; /* storage for new urno AND where */
	char pclSqlBuf[128];				/* storage for SQL-statement */
	char pclMAXN[14];						/* storage for returning MAXN-value */
	char pclMINN[14];						/* storage for returning MINN-value */
	char clOraErr[300];					/* storage for DB-Error string */
	char pclValue[32]; 					/* Storage for urno value, flag="SNU" */
	double ldMAXN = 0;
	double ldMINN = 0;
	double ldACNU = 0;
	unsigned long ullMax_long = MAXLONG;
	unsigned long ullNewNumber = 0;
	unsigned long ullMINN = 0;
	unsigned long ullMAXN = 0;
	unsigned long ullACNU = 0;
	unsigned long ullValue = 0;
	unsigned long ullRange;
	unsigned long ullNrLeft;
	int saved_debug_level =  TRACE;
        /*
	static blNewUrnoMethod = TRUE;
        */
	static int ilConfigChecked = FALSE;
        char pclTmpData[128] = "";

	saved_debug_level = debug_level;
	debug_level  =  TRACE;
	*clOraErr 	= '\0';
	*pclSqlBuf 	= '\0';
	*pclHostVars = '\0';
	*pclMAXN = '\0';
	*pclMINN = '\0';

  if (ilConfigChecked == FALSE)
  {
    ilGetRc = DB_GetUfisCedaCfg("URNO_RANGE_KEYS","USE_URNO_DISPOSER",pclTmpData,"YES");
    if (strcmp(pclTmpData,"YES") == 0)
    {
      dbg(TRACE,"<UFIS_CEDA.CFG><URNO_RANGE_KEYS> USE_URNO_DISPOSER = YES");
      blNewUrnoMethod = TRUE;
    }
    else
    {
      dbg(TRACE,"<UFIS_CEDA.CFG><URNO_RANGE_KEYS> USE_URNO_DISPOSER = NO");
      blNewUrnoMethod = FALSE;
    }
    ilConfigChecked = TRUE;
  }

    /*** new URNO method    ***/
    /* ONLY FOR KEYS='SNOTAB' */
    /* AND WHEN CONFIGURED !  */
    /**************************/
	if (blNewUrnoMethod && !strcmp(pcpKey,"SNOTAB"))
	{
		static char clSqlBuf[1024];

		/*sprintf(clSqlBuf,"SELECT PCK_URNODISPOSER.FUN_GETURNO(%d) FROM DUAL",ipCnt);*/
		sprintf(clSqlBuf,"SELECT PCK_URNODISPOSER.FUN_GETURNO(:VPARA) FROM DUAL");
		sprintf(pcpDataArea,"%d",ipCnt);
		if ((ilRc = sql_if(START,&ilMINNCursor,clSqlBuf,pcpDataArea)) == DB_SUCCESS)
		{
			if (atol(pcpDataArea) != 0)
			{
				dbg(TRACE,"%s Got %d URNO(S) from FRATAB starting with <%s>",pclFunc,ipCnt,pcpDataArea);
				return ilRc;
			}
			else
			{
				/*** if pcpDataArea == 0 fall through using old version **/
				dbg(TRACE,"%s New URNO gives 0, no Urnos available, using NUMTAB",pclFunc);
 				close_my_cursor(&ilMINNCursor);
				ilMINNCursor = 0;
			}
		}
		else
		{
			debug_level = DEBUG;
			dbg(TRACE,"%s DB_ERROR RC=%d. Set NewUrnoMethod to FALSE.",pclFunc,ilRc);
			check_ret(ilRc);
			blNewUrnoMethod = FALSE; /* using old version from now on */
			debug_level = saved_debug_level;
 			close_my_cursor(&ilMINNCursor);
			ilMINNCursor = 0;
		}
	}
	else
	{
		if (!blNewUrnoMethod)
		{
			dbg(TRACE,"%s New URNO function not available.",pclFunc);
		}
		else
		{
			dbg(TRACE,"%s KEYS not SNOTAB but (%s)",pclFunc,pcpKey);
		}
	}
	dbg(TRACE,"GetNextNumbers: Key: <%s>, Count: <%d>, Flag: <%s>",pcpKey,ipCnt,pcpFlag);

	if(ipCnt <= 0)
	{
		ilRc = RC_FAIL;
		dbg(TRACE,"GetNextNumbers: Error.Can't generate negative or zero numbers");
		return RC_FAIL;
	}

	if(strcmp(pcpFlag,"SNU")==0){
		strcpy(pclValue,pcpDataArea);		/* store set value */
		ullValue = atof(pclValue);
	}
	/* reading MINN from NUMTAB */
	memset(pcpDataArea,0x00,1);
	sprintf(pcpDataArea,"%s",pcpKey);
	sprintf(pclSqlBuf,"SELECT MINN FROM NUMTAB WHERE KEYS =:Val0");
	if ((ilRc = sql_if(START,&ilMINNCursor,pclSqlBuf,pcpDataArea)) ==
			DB_SUCCESS)
	{
		ldMINN = atof(pcpDataArea);
		if (ldMINN >= (double)(ullMax_long * 2))
		{
			ullMINN = (ullMax_long * 2);
			dbg(DEBUG,"GNN: Value too high for datatype <unsigned long>."
								"Setting MINN to (%u).",ullMINN);
		}
		else
		{
			ullMINN = (unsigned long)ldMINN;
		}
	}

	/* reading MAXN from NUMTAB */
	memset(pcpDataArea,0x00,1);
	sprintf(pcpDataArea,"%s",pcpKey);
	sprintf(pclSqlBuf,"SELECT MAXN FROM NUMTAB WHERE KEYS =:Val0");
	if ((ilRc = sql_if(START,&ilMAXNCursor,pclSqlBuf,pcpDataArea)) ==
			DB_SUCCESS)
	{
			ldMAXN = atof(pcpDataArea);
			if (ldMAXN >= (double)(ullMax_long * 2))
			{
				ullMAXN = (ullMax_long * 2);
/*
				dbg(DEBUG,"GNN: Value to high for datatype <unsigned long>."
									"Setting MAXN to (%u).",ullMAXN);
*/
			}
			else
			{
				ullMAXN = (unsigned long)ldMAXN;
			}
	}
	/* Following code is courtesy of GSA */
	if(strcmp(pcpFlag,"SNU")==0){
		if(ullValue>ullMAXN || ullValue<ullMINN){
			dbg(TRACE,"SetNextUrno: Hey! Value is out of bounds!!");
			ilRc=RC_FAIL;
		} else {
			sprintf(pclSqlBuf,"UPDATE NUMTAB SET ACNU=:Val0 WHERE KEYS=:Val1");

			/* updating of DB-field ACNU in NUMTAB with number pclUrno*/
			sprintf(pclHostVars,"%u,%s",ullValue,pcpKey);
			dbg(TRACE,"GNN: SNU SqlBuf=(%s), HostVars: <%s>",pclSqlBuf,pclHostVars);
			delton(pclHostVars);
			if ((ilRc=sql_if(NOACTION,&ilUCursor,pclSqlBuf,pclHostVars)) ==
								DB_SUCCESS)
			{
				commit_work();
				/* rollback(); */
				dbg(TRACE,"GNN: SNU updating ACNU of (%s)-numbercircle to (%s)"
									,pcpKey,pclHostVars);
			}
			else
			{
				get_ora_err(ilRc,clOraErr);
				dbg(TRACE,"GNN: SNU ORA-ERR:(%s)",clOraErr);
				check_ret(ilRc);
				ilRc = RC_FAIL;
			} /* end if sql_if()... */
		} /* end if ullValue out of range */
	} else {
		/* locking SELECT on DB-field ACNU in NUMTAB where KEYS = pcpKey */
		memset(pcpDataArea,0x00,1);
		sprintf(pcpDataArea,"%s",pcpKey);

     /* Changes by MOS 3. July 2000 for the IDS Interface in Athen */
		 /* If the Flag NO_AUTO_INCREASE is specified, then the record won't be locked */
		 if (strcmp(pcpFlag,"NO_AUTO_INCREASE") != 0) {
						sprintf(pclSqlBuf,"SELECT ACNU FROM NUMTAB WHERE KEYS =:Val0 FOR UPDATE");
		} else {
            sprintf(pclSqlBuf,"SELECT ACNU FROM NUMTAB WHERE KEYS =:Val0");
		}/* LOCKING?? */


		if ((ilRc = sql_if(START,&ilSCursor,pclSqlBuf,pcpDataArea)) ==
				DB_SUCCESS)
		{
			ldACNU = atof(pcpDataArea);
			if (ldACNU > (double)(ullMax_long * 2))
			{
				ullACNU = (ullMax_long * 2);
				dbg(TRACE,"GNN: Value too high for datatype <unsigned long>."
									" Can't continue to generate numbers.");
				ilMaxnOk = RC_FAIL;
			}
			else
			{
				ullACNU = (unsigned long)ldACNU;
			}
			/***********************************/
			/* the MAXN-value is NOT  exceeded */
			/***********************************/
			if ((ullACNU+(unsigned long)(ipCnt-1)) < ullMAXN)
			{
				ullNewNumber = (ullACNU + (unsigned long)(ipCnt));
			}
			/******************************/
			/* the MAXN-value IS exceeded */
			/******************************/
			else
			{
				/* the MAXN-value should NOT be overwritten */
				if (strcmp(pcpFlag,"GNV") == 0 || pcpFlag == NULL)
				{
					dbg(TRACE,"GNN: Error. MAXN-value of %s-numbercircle"
										"reached !",pcpKey);
					rollback();
					ilMaxnOk = RC_FAIL;
				}
				/************************/
				/* all other used Flags */
				/************************/
				else
				{
					if (strcmp(pcpFlag,"MAXOW") == 0)
					{
						ullRange = (ullMAXN - ullMINN);
						ullNrLeft=	(unsigned long)(ipCnt) % ullRange;
						ullNewNumber = ullNrLeft+ullACNU;
						if (ullNewNumber > ullMAXN)
						{
							ullNewNumber = (ullNewNumber % ullRange)-(unsigned long)1;
						}
					}
					else
					{
						dbg(TRACE,"GNN: unknown Flag (%s) received.",pcpFlag);
						rollback();
						ilMaxnOk = RC_FAIL;
					}
				}
			}
		if (ilMaxnOk == RC_SUCCESS)
		{
			/* if GetNextNumbers is NOT called from GetNextValues, the MAXN-value */
			/* is returned inside pcpDataArea. Otherwise only the first URNO is   */
			/* returned.(for compatibility with GetNextValues function)           */
			if (strcmp(pcpFlag,"GNV") != 0)
			{
				sprintf(pclMAXN,",%u",ullMAXN);
				strcat(pcpDataArea,pclMAXN);
				sprintf(pclMINN,",%u",ullMINN);
				strcat(pcpDataArea,pclMINN);
			}
                        /* Changes by MOS 3. July 2000 for the IDS Interface in Athen */
                        /* If the Flag NO_AUTO_INCREASE is specified, then the acnu won't be increased */
                        if (strcmp(pcpFlag,"NO_AUTO_INCREASE") != 0)
                        {

				sprintf(pclSqlBuf,"UPDATE NUMTAB SET ACNU=:Val0 WHERE KEYS=:Val1");
				sprintf(pclUrno,"%u",ullNewNumber);
				sprintf(pclWhere,"%s",pcpKey);
				/*
				dbg(DEBUG,"GNN: SqlBuf=(%s)",pclSqlBuf);
      				*/
				/* updating of DB-field ACNU in NUMTAB with number pclUrno*/
				sprintf(pclHostVars,"%s,%s,",pclUrno,pclWhere);
				delton(pclHostVars);
				if ((ilRc=sql_if(NOACTION,&ilUCursor,pclSqlBuf,pclHostVars)) ==
								DB_SUCCESS)
				{
					commit_work();
					dbg(TRACE,"%s Updating ACNU of (%s)-numbercircle to (%s)"
									,pclFunc,pcpKey,pclHostVars);
				}
				else
				{
					dbg(TRACE,"%s <%s> HostVars <%s>",pclFunc,pclSqlBuf,pclHostVars);
					get_ora_err(ilRc,clOraErr);
					dbg(TRACE,"GNN: ORA-ERR:(%s)",clOraErr);
					check_ret(ilRc);
					ilRc = RC_FAIL;
					rollback();
				}
			}/*flag =NO_AUTO_INCREASE*/
		}
		else
		{
			ilRc = RC_FAIL;
		}
	}
	else /* lock failed */
	{
		get_ora_err(ilRc,clOraErr);
		dbg(TRACE,"GetNextNumbers: ORA-ERR:(%s)",clOraErr);
		ilRc=RC_FAIL;
	}
	}
	if (ilRc == RC_FAIL)
	{
		memset(pcpDataArea,0x00,1);
	}
	debug_level = saved_debug_level;
	return ilRc;
}



static char *ItemPut(char *pcpDest,char *pcpSource,char cpDelimiter)
{
	if (*pcpSource != '\0')
	{
		strcpy(pcpDest,pcpSource);
		pcpDest += strlen(pcpDest);
	}
	else
	{
		strcpy(pcpDest," ");
		pcpDest++;
	}
	if (cpDelimiter != '\0')
	{
		*pcpDest = cpDelimiter;
		pcpDest++;
	}
	return (pcpDest);
}



static int InsertAlert(char *pcpDataArea,int bpDoCommit,int bpSendToSqlhdl,
	char *pcpDestName, char *pcpWksName, char *pcpTwStart,char *pcpTwEnd)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[1024];
	static char clDataArea[1024];
	static char clData[1024];
	char clTimeStamp[24];
	char clSelection[124];
	char clErrMsg[1240];
	char clUrno[24];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;

	dbg(TRACE,"InsertAlert start");

	strcpy(clDataArea,pcpDataArea);
	dbg(DEBUG,"<%s>",clDataArea);
	dbg(DEBUG,"SendToSqlhdl is %d",bpSendToSqlhdl);
	if (!bpSendToSqlhdl)
	{
		strcpy(clSqlBuf,"INSERT INTO ICSTAB (INTR,CTYP,CONT,CONS,ERRM,DATA,STAT,CDAT,URNO) "
			"VALUES(:VINTR,:VCTYP,:VCONT,:VCONS,:VERRM,:VDATA,:VSTAT,:VCDAT,:VURNO)");
		if (clDataArea[strlen(clDataArea)-1] != ',')
		{
			strcat(clDataArea,",");
		}
		pclDataArea = clDataArea + strlen(clDataArea);
		GetServerTimeStamp("UTC", 1,0,clTimeStamp);
		pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');


		/***
		if (debug_level == DEBUG)
		{
			dbg(DEBUG,"InsertAlert: <%s>",clSqlBuf);
			dbg(DEBUG,"InsertAlert: <%s>",clDataArea);
		}
		***/

		GetNextValues(clUrno,1);
		pclDataArea = ItemPut(pclDataArea,clUrno,'\0');
		sprintf(clSelection,"WHERE URNO = '%s'",clUrno);

		strcpy(clData,clDataArea);

		delton(clDataArea);
		ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
		if(ilRc != RC_SUCCESS)
		{
			/* there must be a database error */
			get_ora_err(ilRc, clErrMsg);
			dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,clErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
		}
		if (bpDoCommit)
		{
			commit_work();
		}
		close_my_cursor(&slCursor);
		slCursor = 0;
		if (ilRc == RC_SUCCESS)
		{
    	SendCedaEvent(1900,0, pcpDestName,pcpWksName,pcpTwStart, pcpTwEnd,
          	"IRT","ICSTAB",clSelection,"INTR,CTYP,CONT,CONS,ERRM,DATA,STAT,CDAT,URNO",
								clData,"",3,0);
		}
	}
	else
	{
		static int ilSqlhdl = 0;

		strcpy(clData,clDataArea);

	  if(ilSqlhdl == 0)
		{
			int ilTmpModId = 0;
			if ((ilTmpModId = tool_get_q_id("router")) > RC_SUCCESS)
			{
				ilSqlhdl = ilTmpModId;
				dbg(TRACE,"InsertAlert: Mod-Id for <sqlhdl/router> set to %d",ilSqlhdl);
			}
			else
			{
				ilSqlhdl =506;
				dbg(TRACE,"InsertAlert: Mod-Id for <sqlhdl/router> not found, set to default %d",
						ilSqlhdl);
			}
		}

    /*SendCedaEvent(ilSqlhdl,0, pcpDestName,pcpWksName,pcpTwStart, pcpTwEnd,
          "IRT","ICSTAB",clSelection,"INTR,CTYP,CONT,CONS,ERRM,DATA,STAT,CDAT,URNO",
								clData,"",3,0);          */

    SendCedaEvent(ilSqlhdl,0, pcpDestName,pcpWksName,pcpTwStart, pcpTwEnd,
          "IBT","ICSTAB",clSelection,"INTR,CTYP,CONT,CONS,ERRM,DATA,STAT",
								clData,"",3,NETOUT_NO_ACK);
	}

	strcpy(pcpDataArea,clUrno);
	return(ilRc);
}


int AddAlert(char *pcpInterface,char *pcpType,char *pcpLastConnection,
	char *pcpConnectionState,char *pcpMessage,int bpDoCommit,int bpSendToSqlhdl,
		char *pcpDestName, char *pcpWksName, char *pcpTwStart,char *pcpTwEnd)
{
	return AddAlert2(pcpInterface,pcpType,pcpLastConnection,
		pcpConnectionState,pcpMessage," ",bpDoCommit,bpSendToSqlhdl,
			pcpDestName, pcpWksName, pcpTwStart,pcpTwEnd);
}

int AddAlert2(char *pcpInterface,char *pcpType,char *pcpLastConnection,
	char *pcpConnectionState,char *pcpMessage,char *pcpData,int bpDoCommit,
		int bpSendToSqlhdl,char *pcpDestName, char *pcpWksName,
		char *pcpTwStart,char *pcpTwEnd)
{
		int ilRc = RC_SUCCESS;
		char clDataArea[5024];
		char clData[4024];
		char clMessage[300];
		char *pclDataArea;

		dbg(TRACE,"AddAlert: bpSendToSqlhdl is %d",bpSendToSqlhdl);

		memset(clDataArea,0,sizeof(clDataArea));
		memset(clMessage,0,sizeof(clMessage));

		strncpy(clMessage,pcpMessage,255);
		/*strncpy(clMessage,pcpMessage,15);*/
		ConvertClientStringToDb(clMessage);
		strncpy(clData,pcpData,4000);
		ConvertClientStringToDb(clData);
		pclDataArea = clDataArea;

		pclDataArea = ItemPut(pclDataArea,pcpInterface,',');
		pclDataArea = ItemPut(pclDataArea,pcpType,',');
		pclDataArea = ItemPut(pclDataArea,pcpLastConnection,',');
		pclDataArea = ItemPut(pclDataArea,pcpConnectionState,',');
		pclDataArea = ItemPut(pclDataArea,clMessage,',');
		pclDataArea = ItemPut(pclDataArea,clData,',');
		pclDataArea = ItemPut(pclDataArea,"O",',');

		ilRc = InsertAlert(clDataArea,bpDoCommit,bpSendToSqlhdl,
				pcpDestName,pcpWksName,pcpTwStart,pcpTwEnd);

		return ilRc;
}


/* **************************************************************************/
/* Function: 	CheckDBTable (from NUMTAB for number-circle)				*/
/* Description:	Checks whether a table is defined in TABTAB. if defined,	*/
/*				MAIN section of pcpConfigFile is checked for an entry		*/
/*				UseXXXTAB = NO, where  XXXTAB is the content of pcpTana		*/
/* Parameter:	IN: pcpTana (name of DB table to be checked)				*/
/*             	IN: int ipCnt (acount of numbers to be returned)			*/
/*             	IN: pcpConfigFile (path of config-file to look for			*/
/*													UseXXXTAB entry)		*/
/* Return: 		RC_SUCCEES, if table in configuration on usage not excluded */
/*				RC_NOTFOUND, if table is not in configuration				*/
/*				RC_IGNORE, if usage of table is excluded in config file		*/
/* Author:      HAG 17.12.2004												*/
/* Update:																	*/
/* **************************************************************************/
int CheckDBTable ( char *pcpTana, char *pcpConfigFile )
{
	int ilRc;
	short slCursor=0;
	char clTable[21], clKeyWord[41], clRes[21], clSqlBuf[101];

	strncpy ( clTable, pcpTana, 3 );
	clTable[3] = '\0';

	clRes[0] = '\0';
	sprintf(clSqlBuf, "SELECT URNO FROM TABTAB WHERE LTNA like '%s%%'", clTable );
	ilRc = sql_if(START,&slCursor,clSqlBuf, clRes);
	if (ilRc != DB_SUCCESS)
	{
		dbg (  TRACE, "GetStartEnd: SQL-statement <%s> failed, RC <%d>", clSqlBuf, ilRc );
	}
	else
	{
		dbg ( DEBUG, "GetStartEnd: SQL-statement <%s> Data <%s> OK", clSqlBuf, clRes );
		close_my_cursor(&slCursor);
	}
	if ( ilRc == RC_SUCCESS )
	{
		sprintf ( clKeyWord, "Use%s", pcpTana );
		ilRc = iGetConfigEntry(pcpConfigFile,"MAIN",clKeyWord,CFG_STRING,clRes );
		if ( (ilRc==RC_SUCCESS) && strstr(clRes, "NO") )
			ilRc = RC_IGNORE;
		else
			ilRc = RC_SUCCESS ;
	}
	else
		ilRc = RC_NOTFOUND;
	dbg ( TRACE, "CheckDBTable: RC <%d>", ilRc );
	return ilRc ;
}


/***********************************************/
/* AREA OF URNO POOL AND URNO RANGES IN NUMTAB */
/*******************************************************/


/* ******************************************************************** */
/* ******************************************************************** */
int DB_GetUrnoKeyCode(char *pcpTableName, char *pcpTableKeyCode)
{
  int ilCnt = 0;
  static int isIsInitUrnoRanges = FALSE;
  static int isHandleUrnoRanges = FALSE;

  char pclSqlTable[16] = "";
  char pclSqlData[128] = "";
  char pclSqlCond[128] = "";
  char pclSqlFields[128] = "";
  char pclNumTabKeys[16] = "";
  strcpy(pclSqlTable,"NUMTAB");
  strcpy(pclSqlFields,"ACNU,MINN,MAXN");
  strcpy(pcpTableKeyCode,"SNOTAB");
  strcpy(pclNumTabKeys,pcpTableName);

  if (isIsInitUrnoRanges == FALSE)
  {
    dbg(TRACE,"CHECK CONFIG FOR URNO POOL RANGE HANDLING");
    DB_GetUrnoRangeInit("USE_NUMTAB_KEYS", &isHandleUrnoRanges);
    isIsInitUrnoRanges = TRUE;
  }
  if (isHandleUrnoRanges == TRUE)
  {
    dbg(TRACE,"HANDLING OF URNO KEYS USING FUNCTION: GetNextNumbers");
    dbg(TRACE,"---> CHECK AUTOMATED CONFIGURATION");
    strcpy(pclNumTabKeys,pcpTableName);
    sprintf(pclSqlCond,"WHERE KEYS='%s'",pclNumTabKeys);
    ilCnt = DB_HandleSqlSelectRows(pclSqlTable, pclSqlCond, pclSqlFields, pclSqlData, 1);
    if (ilCnt > 0)
    {
      dbg(TRACE,"TABLE <%s> FOUND URNO RANGE KEYS <%s>",pcpTableName,pclNumTabKeys);
    }
    else
    {
      strcpy(pclNumTabKeys,"SNOTAB");
      dbg(TRACE,"TABLE <%s> KEYS NOT FOUND. USING DEFAULT <%s>",pcpTableName,pclNumTabKeys);
    }
    dbg(TRACE,"---> CHECK MANUALLY CONFIGURED KEYS");
    strcpy(pclSqlData,pclNumTabKeys);
    sprintf(pclSqlCond,"%s_KEYS",pcpTableName);
    (void) DB_GetUfisCedaCfg("URNO_RANGE_KEYS", pclSqlCond, pclNumTabKeys, pclSqlData);
    dbg(TRACE,"TABLE <%s> KEYS CONFIG: <%s> RESULT: <%s>",pcpTableName,pclSqlCond,pclNumTabKeys);
    dbg(TRACE,"---> CROSS_CHECK FINAL CONFIGURATION");
    sprintf(pclSqlCond,"WHERE KEYS='%s'",pclNumTabKeys);
    ilCnt = DB_HandleSqlSelectRows(pclSqlTable, pclSqlCond, pclSqlFields, pclSqlData, 1);
    if (ilCnt > 0)
    {
      strcpy(pcpTableKeyCode,pclNumTabKeys);
      dbg(TRACE,"TABLE <%s> USING URNO RANGE KEYS <%s>",pcpTableName,pcpTableKeyCode);
    }
    else
    {
      strcpy(pcpTableKeyCode,"SNOTAB");
      dbg(TRACE,"TABLE <%s> USING DEFAULT URNO RANGE KEYS <%s>",pcpTableName,pcpTableKeyCode);
    }
  }
  else
  {
    strcpy(pcpTableKeyCode,"SNOTAB");
    dbg(TRACE,"HANDLING OF URNO KEYS USING FUNCTION: GetNextValues");
    dbg(TRACE,"TABLE <%s> URNO RANGE DEFAULT KEY <%s>",pcpTableName,pcpTableKeyCode);
  }
  strcpy(pclNumTabKeys,pcpTableKeyCode);
  sprintf(pclSqlCond,"WHERE KEYS='%s'",pclNumTabKeys);
  ilCnt = DB_HandleSqlSelectRows(pclSqlTable, pclSqlCond, pclSqlFields, pclSqlData, 1);
  dbg(TRACE,"NUMTAB <%s> ACTUAL VALUES <%s>",pcpTableKeyCode,pclSqlData);
  return isHandleUrnoRanges;
}


/* ******************************************************************** */
/* ******************************************************************** */
static void DB_GetUrnoRangeInit(char *pcpKeyWord, int *pipSetFlag)
{
  char pclDefValue[8] = "";
  /* GET CONFIGURATION */
  if (*pipSetFlag == TRUE)
  {
    strcpy(pclDefValue,"YES");
  }
  else
  {
    strcpy(pclDefValue,"NO");
  }
  (void) DB_GetUfisCedaCfg("URNO_RANGE_KEYS", pcpKeyWord, pcgDbTmpBuf1, pclDefValue);
  if (strcmp(pcgDbTmpBuf1,"YES") == 0)
  {
    *pipSetFlag = TRUE;
    dbg(TRACE,"===> %s = YES",pcpKeyWord);
  }
  else
  {
    *pipSetFlag = FALSE;
    dbg(TRACE,"===> %s = NO",pcpKeyWord);
  }
  return;
}

/*******************************************************/
/*******************************************************/
int DB_GetUfisCedaCfg (char *pcpGroup, char *pcpLine, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  char pclActPath[128] = "";
  char *pclPtrPath = NULL;
  if (strlen(pcgUfisCedaCfg) == 0)
  {
    /* Attach Config File */
    if ((pclPtrPath = getenv ("CFG_PATH")) == NULL)
    {
      strcpy (pclActPath, "/ceda/conf");
    }                           /* end if */
    else
    {
      strcpy (pclActPath, pclPtrPath);
    }                           /* end else */
    sprintf (pcgUfisCedaCfg, "%s/ufis_ceda.cfg", pclActPath);
  }

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgUfisCedaCfg, pcpGroup, pcpLine, CFG_STRING, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
} /* end DB_GetUfisCedaCfg */



/* ******************************************************************** */
/* Funny: SQLHDL must read a lot of records from different tables but   */
/* there is still no generic function to perform this job in Sqlhdl.    */
/* I copied the function from MVTGEN. It originally derives from GOCHDL */
/* ******************************************************************** */
int DB_HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlBuf[4096*2];
  char pclDatBuf[4096*2];
  int ilPos = 0;
  int ilCnt = 0;
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s", pcpSqlFld, pcpTable, pcpSqlKey);
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilPos = 0;
  ilCnt = 0;
  short slCursor = 0;
  short slFkt = START;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    pclDatBuf[0] = 0x00;
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclDatBuf);
    if (ilGetRc == DB_SUCCESS)
    {
      ilCnt++;
      /* dbg(TRACE,"DATA <%s>",pclDatBuf); */
      BuildItemBuffer(pclDatBuf,pcpSqlFld,-1,",");
      StrgPutStrg(pcpSqlDat, &ilPos, pclDatBuf, 0, -1, "\n");
      slFkt = NEXT;
    }
  }
  close_my_cursor(&slCursor);
  if (ilPos > 0)
  {
    ilPos--;
  }
  pcpSqlDat[ilPos] = 0x00;
  if (ilCnt > 0)
  {
    dbg(TRACE,"SELECT ROWS: GOT %d RECORDS",ilCnt);
  }
  ilRc = ilGetRc;
  if ((ilGetRc == ORA_NOT_FOUND) && (ilPos > 0))
  {
    ilRc = DB_SUCCESS;
  }
  else
  {
    dbg(TRACE,"SELECT ROWS: NO DATA FOUND");
  }
  return ilCnt;
}

/* UFIS-1485 */
int DB_LoadUrnoList(int ipNumUrnos ,char *pcpUrno ,char* pcgTableKey) {
    int         ilRc = RC_SUCCESS;
    if (blNewUrnoMethod != TRUE) {
        if( GetNextValues(pcpUrno,ipNumUrnos) != RC_SUCCESS )
        {
            dbg(TRACE,"LoadUrnoList() Error GetNextVal()");
            ilRc = RC_FAIL;
        }
    } else {
        dbg(TRACE,"GET %d URNO VALUES BY <GetNextValues>",ipNumUrnos);
        if( GetNextNumbers(pcgTableKey,pcpUrno,ipNumUrnos,"GNV") != RC_SUCCESS )
        {
            dbg(TRACE,"LoadUrnoList() Error GetNextNumbers()");
            ilRc = RC_FAIL;
        }
    }
    return ilRc;
}
