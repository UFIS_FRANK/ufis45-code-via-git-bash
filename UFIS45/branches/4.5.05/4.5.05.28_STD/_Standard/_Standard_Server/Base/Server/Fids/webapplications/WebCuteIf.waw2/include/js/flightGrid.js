// We use a document ready jquery function.
jQuery(document).ready(function(){
    jQuery("#FlightData").jqGrid({
        width:'600',
        height: 'auto',
        // the url parameter tells from where to get the data from server
        // adding ?nd='+new Date().getTime() prevent IE caching
        url:'controller.php?task=Allocation&Display=1',
        // datatype parameter defines the format of data returned from the server
        // in this case we use a JSON data
        datatype: "json",
        // colNames parameter is a array in which we describe the names
        // in the columns. This is the text that apper in the head of the grid.
        colNames:['Urno','Open','Flight Number', 'Airline' ,'Remark','STOD','ETOD'],
        // colModel array describes the model of the column.
        // name is the name of the column,
        // index is the name passed to the server to sort data
        // note that we can pass here nubers too.
        // width is the width of the column
        // align is the align of the column (default is left)
        // sortable defines if this column can be sorted (default true)
        colModel:[
        {
            name:'urno',
            index:'urno',
            width:1,
            hidden:true
        },

        {
            name:'open',
            index:'open',
            width:1,
            hidden:true
        },

        {
            name:'flno',
            index:'flno',
            width:90,
            sortable:false
        },

        {
            name:'alc2',
            index:'alc2',
            width:90,
            sortable:false
        },

        {
            name:'rem',
            index:'rem',
            width:90,
            sortable:false
        },

        {
            name:'stod',
            index:'stod',
            width:90,
            sortable:false
        },

        {
            name:'etod',
            index:'etod',
            width:90,
            sortable:false
        }
        ],
        // pager parameter define that we want to use a pager bar
        // in this case this must be a valid html element.
        // note that the pager can have a position where you want
        //pager: jQuery('#pager'),
        // rowNum parameter describes how many records we want to
        // view in the grid. We use this in example.php to return
        // the needed data.
        rowNum:10,
        // rowList parameter construct a select box element in the pager
        //in wich we can change the number of the visible rows
        //rowList:[10,20,30],
        // path to mage location needed for the grid
        imgpath: 'include/jquery/themes/sand/images',
        // sortname sets the initial sorting column. Can be a name or number.
        // this parameter is added to the url
        //sortname: 'flno',
        //viewrecords defines the view the total records from the query in the pager
        //bar. The related tag is: records in xml or json definitions.
        viewrecords: false,
        //sets the sorting order. Default is asc. This parameter is added to the url
        //sortorder: "flno",
        multiselect: false,
        hidegrid: false,
        gridComplete: function(){
            var ids=[];
            ids = jQuery('#FlightData').getDataIDs();
            if (ids.length<=0)  {
                alert("The Allocation for the Counter has been expired\n you will be transferd to the login page \n ")
                document.location.href="index.php";
              
                return false
            }
            //alert(ids.length);
            for(var i=0;i<ids.length;i++) {
                var ret = jQuery("#FlightData").getRowData(ids[i]);
                if (ret.open=="true") {
                    jQuery("#FlightData").setSelection(ids[i]);
                   
                }
               
            }
        },
        onSelectRow: function(){
            var urno = jQuery("#FlightData").getGridParam('selrow');
            // alert(urno)
            if (urno) {
                var ret = jQuery("#FlightData").getRowData(urno);
                // alert(ret.urno);
                reload(ret.alc2,ret.urno);

            }
        },
        caption: "Flight Data"
    });
//.navGrid('#pager',{add:true,del:false,edit:false,search:true,position:"right"});
});

jQuery("#FlightData").click( function(){
    var urno = jQuery("#FlightData").getGridParam('selrow');
    //alert(urno)
    if (urno) {
        var ret = jQuery("#FlightData").getRowData(urno);
        //alert(ret.urno);
        reload(ret.alc2,ret.urno);
            
    }
    else {
        alert("Please select Flight");
    }

})

function reload(alc2,urno) {
    //alert(alc2+'-'+urno)
   // alert($("#PreviewLogo").html("test"));
    
    $.ajax({url: "controller.php?task=logopreview&FlightURNO="+ urno, data: $(this.elements).serialize(), success: function(response){$("#PreviewLogo").html(response);}, dataType: "html"});
    if ($("select#LogoSelect")) {
        $.getJSON("controller.php?task=logos&ALC2="+alc2+"&FlightURNO="+ urno,{
            ajax: 'true'
        }, function(j){
            var options = '';
            for (var i = 0; i < j.length; i++) {
                Selected = " ";
                if (j[i].optionSelected == j[i].optionValue) {
                    Selected = " Selected";
                }
                        
                options += '<option value="' + j[i].optionValue + '" '+Selected+'>' + j[i].optionDisplay + '</option>';
            }
            $("select#LogoSelect").html(options);
        });
        //Change the Status
        $.getJSON("controller.php?task=status&FlightURNO="+ urno,{
            ajax: 'true'
        }, function(j){
            $("#Status").html(j);
        });
    }
    /*
    //We must check if the Entity Exists
    //The FreeText1
    if ($("#Text1")) {
        $.ajax({url: "controller.php?task=Freetext&Number=1&FlightURNO="+ urno, data: $(this.elements).serialize(), success: function(response){$("#Text1").html(response);}, dataType: "html"});
    }
    if ($("#Text2")) {
        //The FreeText2
        $.ajax({url: "controller.php?task=Freetext&Number=2&FlightURNO="+ urno, data: $(this.elements).serialize(), success: function(response){$("#Text2").html(response);}, dataType: "html"});
    }
    if ($("#PRemarkCode")) {
        //PRemark
        $.getJSON("controller.php?task=Ppremarks&FlightURNO="+ urno,{
            ajax: 'true'
        }, function(j){
           var options = '';
            for (var i = 0; i < j.length; i++) {
                Selected = " ";
                if (j[i].optionSelected == j[i].optionValue) {
                    Selected = " Selected";
                }

                options += '<option value="' + j[i].optionValue + '" '+Selected+'>' + j[i].optionDisplay + '</option>';
            }
            $("select#PRemarkCode").html(options);
        });
    }
*/
}
function gridReload(){
    var nm_mask = jQuery("#item_nm").val();
    var cd_mask = jQuery("#search_cd").val();
    jQuery("#FlightData").trigger("reloadGrid");
}
/*
 * gridComplete: function(){
            //var ids=[];
            //ids = jQuery('#FlightData').getDataIDs();

            //for(var i=0;i<=ids.length;i++) {
                alert("loaded");
               // jQuery("#FlightData").getRowData(ids[i]);
        //}
        }
 */