#ifndef _DEF_mks_version_lexdsp_h
  #define _DEF_mks_version_lexdsp_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_lexdsp_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/lexdsp.h 1.2 2004/07/27 16:47:59SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*
 *  Display handler lex function declaration
 */

#ifndef dsplex_included
#define dsplex_included

static char *dsplex_h_sccsid="%Z% %P% %R%%S% %G% %U%";

#include <stdio.h>
#include "dsplib.h"

void dsplex(FILE *, UDBXPage *);
#endif /* dsplex_included */
