#ifndef _DEF_mks_version_ftptools_c
  #define _DEF_mks_version_ftptools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ftptools_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/ftptools.c 1.3 2006/01/21 00:31:09SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CEDA Program Skeleton                                                      */
/*                                                                            */
/* Author         : RBI                                                       */
/* Date           : 98/11/12                                                  */
/* Description    : FTP-Library-Functions                                     */
/*                                                                            */
/* Attention: This library-functions uses signals !!!                         */
/*                                                                            */
/* Update history : 98/11/12 Start                                            */
/*                                                                            */
/*                  23.10.2001 eth tide up                                    */
/*                                                                            */
/*                 2002.04.27 MCU checking reply code in 2nd line             */
/* 20060117 JIM: checking includes for AIX                                    */
/******************************************************************************/
/* This program is not a CEDA main program */


/* headerfiles... */
#include <sys/types.h>
#include <sys/socket.h>
#include <dirent.h>

#ifndef _SNI
#ifndef _HPUX_SOURCE
#ifndef _SOLARIS
#ifndef _WINNT
#ifndef _UNIXWARE
#ifndef _LINUX
#ifndef _AIX
#include <sys/select.h>
#include <sys/itimer.h>
#endif
#endif
#endif
#endif
#endif
#endif
#endif

#if defined(_UNIXWARE) || defined(_LINUX)
#include <sys/time.h>
#endif

#include "helpful.h"
#include "netin.h"
#include "tcputil.h"
#include "helpful.h"
#include "ftptools.h" 

/* internal function prototypes */
static int ReceiveTcpIp(CEDA_FTPInfo *);
static int HandleReply(CEDA_FTPInfo *, int, int *);
static int WriteAndRead(CEDA_FTPInfo *, int);
static int TcpOpenConnection (CEDA_FTPInfo *);

/* external variables and prototypes */
extern int debug_level;
extern FILE *outp;
extern int snap(char*, long, FILE*);

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPInit(CEDA_FTPInfo *prpInfo,
                         char *pcpHostName,
                         char *pcpUser, 
                         char *pcpPasswd,
                         char cpTransferType,
                         int  ipTimeout,
                         long lpGetFileTimer,
                         long lpReceiveTimer,
                         int  ipDebugLevel,
                         int  ipServerOS,
                         int  ipClientOS)
{
	int	ilLen;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPInit> missing prpInfo");
		return RC_FAIL;
	}

	if (pcpHostName == NULL)
	{
		dbg(TRACE,"<CEDA_FTPInit> missing pcpHostName");
		return RC_FAIL;
	}

	if (((ilLen = strlen(pcpHostName)) > 127) || ilLen == 0)
	{
		dbg(TRACE,"<CEDA_FTPInit> pcpHostName-len error");
		return RC_FAIL;
	}

	if (pcpUser == NULL)
	{
		dbg(TRACE,"<CEDA_FTPInit> missing pcpUser");
		return RC_FAIL;
	}

	if (((ilLen = strlen(pcpUser)) > 127) || ilLen == 0)
	{
		dbg(TRACE,"<CEDA_FTPInit> pcpUser-len error");
		return RC_FAIL;
	}

	if (pcpPasswd == NULL)
	{
		dbg(TRACE,"<CEDA_FTPInit> missing pcpPasswd");
		return RC_FAIL;
	}

	if (((ilLen = strlen(pcpPasswd)) > 127) || ilLen == 0)
	{
		dbg(TRACE,"<CEDA_FTPInit> pcpPasswd-len error");
		return RC_FAIL;
	}

	/* check transfer type */
	if (cpTransferType != CEDA_ASCII && cpTransferType != CEDA_BINARY &&
		 cpTransferType != CEDA_EBCDIC && cpTransferType != CEDA_BYTE)
	{
		dbg(TRACE,"<CEDA_FTPInit> not a valid transfer type");
		return RC_FAIL;
	}

	/* check debug level */
	if (ipDebugLevel != DEBUG && ipDebugLevel != TRACE && ipDebugLevel != 0)
		prpInfo->iDebugLevel	= TRACE;
	else
		prpInfo->iDebugLevel	= ipDebugLevel;

	/* check OS */
	if (ipServerOS != OS_UNIX && ipServerOS != OS_WIN)
		prpInfo->iServerOS = OS_UNIX;
	else
		prpInfo->iServerOS = ipServerOS;

	if (ipClientOS != OS_UNIX && ipClientOS != OS_WIN)
		prpInfo->iClientOS = OS_UNIX;
	else
		prpInfo->iClientOS = ipClientOS;
    

	/* set structure members */
	memset((void*)prpInfo->pcCommand, 0x00, iMAXIMUM);
	memset((void*)prpInfo->pcReply, 0x00, iMAXIMUM);

	strcpy(prpInfo->pcHostName, pcpHostName);
	strcpy(prpInfo->pcUser, pcpUser);
	strcpy(prpInfo->pcPasswd, pcpPasswd);

	prpInfo->iConnected 	= FALSE;
	prpInfo->iTimeout 	= ipTimeout;
	prpInfo->lGetFileTimer 	= lpGetFileTimer;
	prpInfo->lReceiveTimer 	= lpReceiveTimer;

	switch (cpTransferType)
	{
		case CEDA_ASCII:
		case CEDA_BINARY:
		case CEDA_EBCDIC:
		case CEDA_BYTE:
			prpInfo->cTransferType = cpTransferType;
			break;
		default:
			prpInfo->cTransferType = CEDA_ASCII;
			break;
	}
	prpInfo->iDataSocket 	= -1;
	prpInfo->iCtrlSocket	= -1;
	prpInfo->iInitOK	= TRUE;

	/* bye bye */
	return RC_SUCCESS;

} /* end of CEDA_FTPInit */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPConnect(CEDA_FTPInfo *prpInfo)
{
	int	ilRC;
	int	ilReceived;
	int	il2ndRead;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPConnect> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPConnect> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	/* is there a existing connection */
	if (prpInfo->iConnected == TRUE)
	{
		dbg(TRACE,"<CEDA_FTPConnect> connection is already established, use FTPClose first");
		return iFTP_FOREVER_INVALID;
	}

	/* create a valid control-socket */
	if ((prpInfo->iCtrlSocket = tcp_create_socket(SOCK_STREAM, NULL)) == RC_FAIL)
	{
		dbg(TRACE,"<CEDA_FTPConnect> error creating socket");
		return iFTP_TRY_AGAIN;
	}

	/* try to open connection to remote host */
	if ((ilRC = tcp_open_connection(prpInfo->iCtrlSocket, "ftp", prpInfo->pcHostName)) == RC_FAIL)
	{
		dbg(TRACE,"<CEDA_FTPConnect> error opening connection");
		return iFTP_TRY_AGAIN;	
	}

	/* read the reply */
	ilReceived = 0;
	do
	{
		while ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
		{
			ilReceived = 1;
			if ((ilRC = HandleReply(prpInfo, CEDA_CONNECT, &il2ndRead)) != RC_SUCCESS)
			{
			   dbg(DEBUG,"<CEDA_FTPConnect>: HandleReply returns %d", ilRC);
				return ilRC;
			}
			if (il2ndRead == TRUE) ilReceived = 0;
		}
	} while (!ilReceived);

  	/* if there is an 2nd reply -> read the reply */
	if ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
	{
      if ((ilRC = HandleReply(prpInfo, CEDA_NLST, &il2ndRead)) != RC_SUCCESS)
	  {
		dbg(DEBUG,"<CEDA_FTPConnect>: HandleReply %d Line returns: %d", 
		          __LINE__,ilRC);
	  }
    }


	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPConnect */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPLogin(CEDA_FTPInfo *prpInfo)
{
	int		i;
	int		ilRC;
	int		ilGoOn;
	int		ilFunction;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPLogin> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPLogin> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	for (i=0, ilGoOn=TRUE; i<2 && ilGoOn == TRUE; i++)
	{
		if (i == 0)
		{
			ilFunction = CEDA_LOGIN_USER;
			/* build user-command */
			sprintf(prpInfo->pcCommand, "USER %s%c%c", prpInfo->pcUser, 0x0D, 0x0A);
		}
		else
		{
			ilFunction = CEDA_LOGIN_PASS;
			/* build user-command */
			sprintf(prpInfo->pcCommand, "PASS %s%c%c", prpInfo->pcPasswd, 0x0D, 0x0A);
		}

		/* thats all */
		if ((ilRC = WriteAndRead(prpInfo, ilFunction)) != RC_SUCCESS)
		{
                  dbg(DEBUG,"<CEDA_FTPLogin> WriteAndRead failed ... returns %d",ilRC);
			return ilRC;
		}
	}

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPLogin */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPSetType(CEDA_FTPInfo *prpInfo, char cpTransferType)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPSetType> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPSetType> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (cpTransferType != CEDA_ASCII && cpTransferType != CEDA_BINARY &&
		 cpTransferType != CEDA_EBCDIC && cpTransferType != CEDA_BYTE)
	{
		dbg(TRACE,"<CEDA_FTPSetType> not a valid transfer type");
		return iFTP_FOREVER_INVALID;
	}

	switch (cpTransferType)
	{
		case CEDA_ASCII:
		case CEDA_BINARY:
		case CEDA_EBCDIC:
		case CEDA_BYTE:
			prpInfo->cTransferType = cpTransferType;
			break;
		default:
			prpInfo->cTransferType = CEDA_ASCII;
			break;
	}
	
	/* build command */
	sprintf(prpInfo->pcCommand, "TYPE %c%c%c", prpInfo->cTransferType, 0x0D, 0x0A);

	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_SETTYPE)) != RC_SUCCESS)
	{
          dbg(DEBUG,"<CEDA_FTPSetType> WriteAndRead failed ... returns %d",ilRC);
		return ilRC;
	}

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPSetType */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPSetFileStructure(CEDA_FTPInfo *prpInfo, char cpStructureType)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPSetFileStructure> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPSetFileStructure> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (cpStructureType != CEDA_FILE && cpStructureType != CEDA_RECORD &&
		 cpStructureType != CEDA_PAGE)
	{
		dbg(TRACE,"<CEDA_FTPSetFileStructure> not a valid structure type");
		return iFTP_FOREVER_INVALID;
	}

	/* build command */
	sprintf(prpInfo->pcCommand, "STRU %c%c%c", cpStructureType, 0x0D, 0x0A);

	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_SET_FILE_STRUCTURE)) != RC_SUCCESS)
	{
          dbg(DEBUG,"<CEDA_FTPSetFileStructure> WriteAndRead failed ... returns %d",ilRC);
		return ilRC;
	}

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPSetFileStructure */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPSetTransferMode(CEDA_FTPInfo *prpInfo, char cpTransferMode)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPSetTransferMode> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPSetTransferMode> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (cpTransferMode != CEDA_STREAM && cpTransferMode != CEDA_BLOCK &&
		 cpTransferMode != CEDA_COMPRESSED)
	{
		dbg(TRACE,"<CEDA_FTPSetTransferMode> not a valid transfer mode");
		return iFTP_FOREVER_INVALID;
	}

	/* build command */
	sprintf(prpInfo->pcCommand, "MODE %c%c%c", cpTransferMode, 0x0D, 0x0A);

	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_SET_TRANSFER_MODE)) != RC_SUCCESS)
        {
          dbg(DEBUG,"<CEDA_FTPSetTransferMode> WriteAndRead failed ... returns %d",ilRC);
	  return ilRC;
        }

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPSetTransferMode */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPClose(CEDA_FTPInfo *prpInfo, int ipFlg)
{
  /* so ein schwachsinn ... eth */

  dbg(DEBUG,"<CEDA_FTPClose> calling CEDA_FTPBye");
  return CEDA_FTPBye(prpInfo, ipFlg);
}


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPBye(CEDA_FTPInfo *prpInfo, int ipFlg)
{
  int  ilRC;
  int  il2ndRead;


  if (prpInfo == NULL)
  {
    dbg(TRACE,"<CEDA_FTPBye> missing prpInfo");
    return iFTP_FOREVER_INVALID;
  }

  /* check init */
  if (prpInfo->iInitOK != TRUE)
  {
    dbg(TRACE,"<CEDA_FTPBye> init failure, use CEDA_FTPInit first");
    return iFTP_FOREVER_INVALID;
  }

  dbg(DEBUG,"<CEDA_FTPBye> ipFlg = %d", ipFlg);

  /* only if last action returns with success */
  if (ipFlg == RC_SUCCESS)
  {
    /* build command */
    sprintf(prpInfo->pcCommand, "QUIT%c%c", 0x0D, 0x0A);
    dbg(DEBUG,"<CEDA_FTPBye> write QUIT to CtrlSocket");

    /* write it to socket */
    if ((ilRC = write(prpInfo->iCtrlSocket,
                      prpInfo->pcCommand,
                      strlen(prpInfo->pcCommand)))
              != (ssize_t)strlen(prpInfo->pcCommand))
    {
      dbg(TRACE,"<CEDA_FTPBye> can't write to CtrlSocket");
    }
    else
    {
      /* read the reply */
      if ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
        if ((ilRC = HandleReply(prpInfo, CEDA_BYE, &il2ndRead)) != RC_SUCCESS)
        {
          dbg(DEBUG,"<CEDA_FTPBye> HandleReply returns: %d", ilRC);
        }
    }
  } /* end if */

  /* shutdown & close all sockets */
  /* dbg(DEBUG,"<CEDA_FTPBye> shutdown & close DataSocket"); */
  /* ilRC = shutdown(prpInfo->iDataSocket, 2); */
  /* if (ilRC != 0) dbg(DEBUG,"<CEDA_FTPBye> problem with shutdown DataSocket <%d>",ilRC); */
  ilRC = close(prpInfo->iDataSocket);
  if (ilRC != 0) dbg(DEBUG,"<CEDA_FTPBye> problem with close DataSocket <%d>", ilRC);

  /* dbg(DEBUG,"<CEDA_FTPBye> shutdown & close CtrlSocket"); */
  /* ilRC = shutdown(prpInfo->iCtrlSocket, 2); */
  /* if (ilRC != 0) dbg(DEBUG,"<CEDA_FTPBye> problem with shutdown CrtlSocket <%d>", ilRC); */
  ilRC = close(prpInfo->iCtrlSocket);
  if (ilRC != 0) dbg(DEBUG,"<CEDA_FTPBye> problem with close CrtlSocket <%d>", ilRC);

  /* reset flag */
  prpInfo->iConnected = FALSE;

  /* everthing looks fine */
  return RC_SUCCESS;

} /* end of CEDA_FTPBye */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPGetFile(CEDA_FTPInfo *prpInfo,
                            char *pcpSourceFile, 
                            char *pcpTargetPath,
                            char *pcpTargetFile,
                            int   ipFlg)
{
  int	i;
  int	j;
  int	ilRC = 0;
  int	ilNoOfBytes = 0;
  int	ilBreakOut;
  char *pclS = NULL;
  char *pclD = NULL;
  char *pclCPtr = NULL;
  char	pclTmpBuf[iMIN];
  char	pclReceiveBuf[iMAX];
  char	pclReceiveTmpBuf[iMAX];
  char	pclTargetPathAndFile[iMAX_BUF_SIZE];
  time_t  tlStartTime;
  time_t  tlEndTime;
  time_t  tlDifTime;
  FILE   *pfh = NULL;
  struct itimerval rlTimeVal;


	if (prpInfo == NULL)
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> missing prpInfo");
	  return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> init failure, use CEDA_FTPInit first");
	  return iFTP_FOREVER_INVALID;
	}

	if (!pcpSourceFile || (pcpSourceFile && !strlen(pcpSourceFile)))
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> missing pcpSourceFile");
	  return iFTP_FOREVER_INVALID;
	}

	if (!pcpTargetPath || (pcpTargetPath && !strlen(pcpTargetPath)))
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> missing pcpTargetPath");
	  return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> init failure, use CEDA_FTPInit first");
	  return iFTP_FOREVER_INVALID;
	}

    /* create a valid control-socket */
	if ((prpInfo->iDataSocket = tcp_create_socket(SOCK_STREAM, NULL)) == RC_FAIL)
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> error creating socket");
	  return iFTP_TRY_AGAIN;
	}
    dbg(DEBUG,"<CEDA_FTPGetFile> socket created <%d>",prpInfo->iDataSocket);

    /* build path & filename */
    strcpy(pclTargetPathAndFile, pcpTargetPath);
    if (pcpTargetPath[strlen(pcpTargetPath)-1] != '/')
	    strcat(pclTargetPathAndFile, "/");
    if (!pcpTargetFile || (pcpTargetFile && !strlen(pcpTargetFile)))
	    strcat(pclTargetPathAndFile, pcpSourceFile);
    else
	 	strcat(pclTargetPathAndFile, pcpTargetFile);

    sprintf(prpInfo->pcCommand, "PASV%c%c", 0x0D, 0x0A);
    if ((ilRC = WriteAndRead(prpInfo, CEDA_GETFILE)) != RC_SUCCESS)
    {
      dbg(DEBUG,"<CEDA_FTPGetFile> WriteAndRead failed ... returns %d",ilRC);
      close(prpInfo->iDataSocket);
      return ilRC;
    }

    dbg(DEBUG,"<CEDA_FTPGetFile> Send PASV - get reply <%s>",prpInfo->pcReply);

    if((pclCPtr = strstr(prpInfo->pcReply,"(")) != NULL)
    {
      strcpy(pclTmpBuf, pclCPtr);
    }
    
    (void) DeleteCharacterInString(pclTmpBuf, '(');
    (void) DeleteCharacterInString(pclTmpBuf, ')');

    prpInfo->rRemoteAddress.lByte4 = atol(GetDataField(pclTmpBuf, 0, ','));
    prpInfo->rRemoteAddress.lByte3 = atol(GetDataField(pclTmpBuf, 1, ','));
    prpInfo->rRemoteAddress.lByte2 = atol(GetDataField(pclTmpBuf, 2, ','));
    prpInfo->rRemoteAddress.lByte1 = atol(GetDataField(pclTmpBuf, 3, ','));
    prpInfo->rRemotePort.sHigh = (short)atoi(GetDataField(pclTmpBuf, 4, ','));
    prpInfo->rRemotePort.sLow  = (short)atoi(GetDataField(pclTmpBuf, 5, ','));

    dbg(DEBUG,"<CEDA_FTPGetFile> RemoteAddress: %d.%d.%d.%d  Port:%d.%d",
               prpInfo->rRemoteAddress.lByte4,
               prpInfo->rRemoteAddress.lByte3,
               prpInfo->rRemoteAddress.lByte2,
               prpInfo->rRemoteAddress.lByte1,
               prpInfo->rRemotePort.sHigh,
               prpInfo->rRemotePort.sLow);


    if ((ilRC = TcpOpenConnection(prpInfo)) != RC_SUCCESS)
	{
	  dbg(TRACE,"<CEDA_FTPGetFile> error opening connection...");
          close(prpInfo->iDataSocket);
	  return iFTP_TRY_AGAIN;
    }

	sprintf(prpInfo->pcCommand, "RETR %s%c%c", pcpSourceFile, 0x0D, 0x0A);
	if ((ilRC = WriteAndRead(prpInfo, CEDA_GETFILE)) != RC_SUCCESS)
        {
          dbg(DEBUG,"<CEDA_FTPGetFile> WriteAndRead failed ... returns %d",ilRC);
          close(prpInfo->iDataSocket);
	  return ilRC;
        }

	/* check port */
	if (prpInfo->iDataSocket >= 0)
	{
		/* write filebuffer to local file */
		if (ipFlg == CEDA_APPEND)
		{
			if (prpInfo->cTransferType == CEDA_ASCII)
				pfh = fopen(pclTargetPathAndFile, "a+");
			else
				pfh = fopen(pclTargetPathAndFile, "a+b");
		}
		else
		{
			if (prpInfo->cTransferType == CEDA_ASCII)
				pfh = fopen(pclTargetPathAndFile, "w");
			else
				pfh = fopen(pclTargetPathAndFile, "w+b");
		}

		/* is this a valid pointer */
		if (pfh == NULL)
		{
			dbg(TRACE,"<CEDA_FTPGetFile> can't open file: <%s>", pclTargetPathAndFile); 
          		close(prpInfo->iDataSocket);
			return iFTP_TRY_AGAIN;
		}

		ilBreakOut = FALSE;
		ilNoOfBytes = 0;

		/* get start time */
		tlStartTime = time(NULL);
		do
		{
			do
			{
				/* clear buffer */
				memset(pclReceiveBuf, 0x00, iMAX);

				/* try to read blocks of 64 kbyte */
				/* set interrupt (SIGALRM) to xx sec */
				rlTimeVal.it_interval.tv_sec = 0;
				rlTimeVal.it_interval.tv_usec = 0;
				rlTimeVal.it_value.tv_sec = prpInfo->lGetFileTimer;
				rlTimeVal.it_value.tv_usec = 0;
				ilRC=setitimer(ITIMER_REAL, &rlTimeVal, NULL);
				ilRC = read(prpInfo->iDataSocket, pclReceiveBuf, iMAX);
				alarm(0);

				if (ilRC <= 0)
					ilBreakOut = TRUE;
				ilNoOfBytes += ilRC;
			} while (ilRC <= 0 && ilBreakOut == FALSE);

			/* write to file */
			if (ilRC > 0)
			{
				/* if type is set to ASCII, change EOL-character if necessary */
				if (prpInfo->cTransferType == CEDA_ASCII && 
					 prpInfo->iClientOS == OS_UNIX)
				{
					/* in this case we have to alter from CR,LF to LF... */
					pclS = pclReceiveBuf;
					pclD = pclReceiveTmpBuf;
					for (i=0, j=ilRC; i<ilRC; i++)
					{
						if (i < ilRC-1)
							if (*pclS == 0x0D && *(pclS+1) == 0x0A)
							{
								pclS++;
								--j;
							}
						*pclD++ = *pclS++;
					}
					memcpy(pclReceiveBuf, pclReceiveTmpBuf, j);
				}
				else
					j = ilRC;
				if (j > 0)
					fwrite(pclReceiveBuf, j, 1, pfh);
			}
		} while (ilBreakOut == FALSE);

		/* get end time */
		tlEndTime = time(NULL);

		/* close file */
		if (pfh != NULL)
			fclose(pfh);

	  /* close all sockets */
	  shutdown(prpInfo->iDataSocket, 2);
	  close(prpInfo->iDataSocket);
        
	  dbg(DEBUG,"<CEDA_FTPGetFile> FileLen is: %d bytes", ilNoOfBytes);
	  tlDifTime = tlEndTime - tlStartTime;
	  dbg(DEBUG,"<CEDA_FTPGetFile> need   : %ld seconds", (long)tlDifTime);
	  if ((long)tlDifTime > 0)
		dbg(DEBUG,"<CEDA_FTPGetFile> average: %ld bytes/s", ((long)ilNoOfBytes/(long)tlDifTime));
	}
	else
		return iFTP_TRY_AGAIN;
	return RC_SUCCESS;

} /* end of CEDA_FTPGetFile */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPMGetFile(CEDA_FTPInfo *prpInfo,
                             char *pcpSourceFile, 
                             char *pcpTargetPath,
                              int ipRenameFlag,
		             char *pcpRenameExtension,
                              int ipDeleteRemoteFile,
                              int ipFlg)
{
  char *pclFct = "CEDA_FTPMGetFile";
  int  	i;
  int	j;
  int	ilRC;
  int	ilNoOfBytes;
  int	ilBreakOut;
  int   il2ndRead = FALSE;
  int   ilAdresse_OK = FALSE;
  char	*pclS = NULL;
  char	*pclD = NULL;
  char  *pclPtr = NULL;
  char  *pclCPtr = NULL;
  char	pclTmpBuf[iMIN];
  char	pclDirBuffer[iMAX];
  char	pclReceiveBuf[iMAX];
  char	pclReceiveTmpBuf[iMAX];
  char  pclRemoteFileName[iMAX_BUF_SIZE];
  char  pclRemoteFileNameExt[iMAX_BUF_SIZE];
  time_t	tlStartTime;
  time_t	tlEndTime;
  time_t	tlDifTime;
  struct itimerval rlTimeVal;

  if (prpInfo == NULL)
  {
    dbg(TRACE,"<%s> missing prpInfo",pclFct);
    return iFTP_FOREVER_INVALID;
  }

  /* check init */
  if (prpInfo->iInitOK != TRUE)
  {
    dbg(TRACE,"<%s> init failure, use CEDA_FTPInit first",pclFct);
    return iFTP_FOREVER_INVALID;
  }

  if (!pcpSourceFile || (pcpSourceFile && !strlen(pcpSourceFile)))
  {
    dbg(TRACE,"<%s> missing pcpSourceFile",pclFct);
    return iFTP_FOREVER_INVALID;
  }

  if (!pcpTargetPath || (pcpTargetPath && !strlen(pcpTargetPath)))
  {
    dbg(TRACE,"<%s> missing pcpTargetPath",pclFct);
    return iFTP_FOREVER_INVALID;
  }

  /* check init */
  if (prpInfo->iInitOK != TRUE)
  {
    dbg(TRACE,"<%s> init failure, use CEDA_FTPInit first",pclFct);
    return iFTP_FOREVER_INVALID;
  }

  /* create a valid control-socket */
  if ((prpInfo->iDataSocket = tcp_create_socket(SOCK_STREAM, NULL)) == RC_FAIL)
  {
    dbg(TRACE,"<%s> error creating socket",pclFct);
    return iFTP_TRY_AGAIN;
  }

  sprintf(prpInfo->pcCommand, "PASV%c%c", 0x0D, 0x0A);
  if ((ilRC = WriteAndRead(prpInfo, CEDA_GETFILE)) != RC_SUCCESS)
     return ilRC;

  if((pclCPtr = strstr(prpInfo->pcReply,"(")) != NULL)
  {
     strcpy(pclTmpBuf, pclCPtr);
  }
  

  (void) DeleteCharacterInString(pclTmpBuf, '(');
  (void) DeleteCharacterInString(pclTmpBuf, ')');
  prpInfo->rRemoteAddress.lByte4 = atol(GetDataField(pclTmpBuf, 0, ','));
  prpInfo->rRemoteAddress.lByte3 = atol(GetDataField(pclTmpBuf, 1, ','));
  prpInfo->rRemoteAddress.lByte2 = atol(GetDataField(pclTmpBuf, 2, ','));
  prpInfo->rRemoteAddress.lByte1 = atol(GetDataField(pclTmpBuf, 3, ','));
  prpInfo->rRemotePort.sHigh = (short)atoi(GetDataField(pclTmpBuf, 4, ','));
  prpInfo->rRemotePort.sLow  = (short)atoi(GetDataField(pclTmpBuf, 5, ','));
  if ((ilRC = TcpOpenConnection(prpInfo)) != RC_SUCCESS)
  {
    dbg(TRACE,"<%s> error opening connection...",pclFct);
    return iFTP_TRY_AGAIN;
  }

  /* ls Befehl fuer directory senden */
  sprintf(prpInfo->pcCommand, "NLST %s%c%c", pcpSourceFile, 0x0D, 0x0A);
  dbg(TRACE,"<%s>: Sending Command <%s>",pclFct,prpInfo->pcCommand);

  if ((ilRC = WriteAndRead(prpInfo, CEDA_NLST)) != RC_SUCCESS)
    return ilRC;

  /* check port */
  if (prpInfo->iDataSocket >= 0)
  {
    ilBreakOut = FALSE;
    ilNoOfBytes = 0;
    pclDirBuffer[0] = 0x00;
    /* get start time */
    tlStartTime = time(NULL);
    do
    {
      do
      {
    	/* clear buffer */
	    memset(pclReceiveBuf, 0x00, iMAX);

    	/* try to read blocks of 64 kbyte */
    	/* set interrupt (SIGALRM) to xx sec */
	    rlTimeVal.it_interval.tv_sec = 0;
	    rlTimeVal.it_interval.tv_usec = 0;
	    rlTimeVal.it_value.tv_sec = prpInfo->lGetFileTimer;
	    rlTimeVal.it_value.tv_usec = 0;
	    ilRC=setitimer(ITIMER_REAL, &rlTimeVal, NULL);
	    ilRC = read(prpInfo->iDataSocket, pclReceiveBuf, iMAX);
	    alarm(0);

	    if (ilRC <= 0)
          ilBreakOut = TRUE;
	    ilNoOfBytes += ilRC;
      } while (ilRC <= 0 && ilBreakOut == FALSE);

      if(ilRC > 0)
	  {
	    /* we have to alter from CR,LF to LF... */
		pclS = pclReceiveBuf;
		pclD = pclReceiveTmpBuf;
		for (i=0, j=ilRC; i<ilRC; i++)
		{
  		  if (i < ilRC-1)
  		  {
    	    if (*pclS == 0x0D && *(pclS+1) == 0x0A)
    		{
      		  pclS++;
      		  --j;
    		}
  		  }
  		  *pclD++ = *pclS++;
		} /* end for */
		*pclD = 0x00; /* string end */
    	strcpy(pclReceiveBuf, pclReceiveTmpBuf);
		/* dbg(TRACE,"<%s> Received Buffer:<%s>", pclFct, pclReceiveBuf);*/
        strcat(pclDirBuffer,pclReceiveBuf);
	  } /* if (ilRC > 0) */
	  
    } while (ilBreakOut == FALSE);

    /* close all sockets */
    shutdown(prpInfo->iDataSocket, 2);
    close(prpInfo->iDataSocket);

  } /* end if (prpInfo->iDataSocket >= 0) */
  else
    return iFTP_TRY_AGAIN;


  /* Pruefen ob bisher nur eine vorlaeufige Bestaetigung gekommen ist */
  if(prpInfo->iReplyCode < 200)  
  {
  	/* read the reply */
	if ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
	{
      if ((ilRC = HandleReply(prpInfo, CEDA_NLST, &il2ndRead)) != RC_SUCCESS)
	  {
		dbg(DEBUG,"<%s>: HandleReply %d Line returns: %d", pclFct,__LINE__,ilRC);
	  }
    }
  }

  
  dbg(TRACE,"<%s>: Received Filenames:<%s> Bufferlength = <%d>",pclFct, pclDirBuffer,
            strlen(pclDirBuffer)); 

  if(strlen(pclDirBuffer) > 0)
  {
    dbg(TRACE,"<%s> Get multiple Files",pclFct);
    pclPtr = pclDirBuffer;
    i=0;

    while(*pclPtr != 0x00)
    {
      /* build remote filename */
      if(*pclPtr != '\n')
      {
        pclRemoteFileName[i] = *pclPtr;
        i++;
      }
      else
      {
        pclRemoteFileName[i] = '\0';
        i=0;
        /* get file (remotefilename=localfilename) */
        dbg(DEBUG,"<%s> Try to get File with Filename <%s>", pclFct,pclRemoteFileName);

        ilRC = CEDA_FTPGetFile(prpInfo,
                               pclRemoteFileName, 
                               pcpTargetPath, 
                               pclRemoteFileName, 
                               ipFlg);
        if(ilRC == RC_SUCCESS)
        {
          dbg(TRACE,"<%s> got file <%s>",pclFct,pclRemoteFileName);

          /* Pruefen ob bisher nur Startmeldung gekommen ist */
          /* dbg(TRACE,"<%s> last Reply = <%d> ",pclFct,prpInfo->iReplyCode);*/
          if(prpInfo->iReplyCode < 200)  
          {
            dbg(DEBUG,"<%s> read next Reply",pclFct);
  	        /* read the reply */
	        if ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
	        {
              dbg(DEBUG,"<%s> got Replycode <%d>",pclFct,prpInfo->iReplyCode);

              if ((ilRC = HandleReply(prpInfo, CEDA_GETFILE, &il2ndRead)) != RC_SUCCESS)
	          {
		        dbg(DEBUG,"<%s>: HandleReply %d Line returns: %d", pclFct,__LINE__,ilRC);
	          }
            }  
          }
        
          if (ipDeleteRemoteFile)
          {
            /* rename remote file - append Extension */
            ilRC = CEDA_FTPrm(prpInfo,pclRemoteFileName);
            if(ilRC == RC_SUCCESS)
            {
              dbg(DEBUG,"<%s> delete remote file <%s>",pclFct,
                        pclRemoteFileName);
		
		    } /*end if */
	      } 
		  else if (ipRenameFlag)
          {
            /* rename remote file - append Extension */
            strcpy(pclRemoteFileNameExt,pclRemoteFileName);
            strcat(pclRemoteFileNameExt,pcpRenameExtension);
            ilRC = CEDA_FTPRemoteRename(prpInfo,pclRemoteFileName,pclRemoteFileNameExt);
            if(ilRC == RC_SUCCESS)
            {
              dbg(DEBUG,"<%s> rename remote file from <%s> to <%s>",pclFct,
                        pclRemoteFileName,pclRemoteFileNameExt);
		
		    } /*end if */
	      } /* end if */

        } /* end if */	
	  } /* end else */
	  pclPtr++;
    } /* end while */
      	  
    /* get end time */
    tlEndTime = time(NULL);


    dbg(DEBUG,"<%s> Received BufLen is: %d bytes", pclFct, ilNoOfBytes);
    tlDifTime = tlEndTime - tlStartTime;
    dbg(DEBUG,"<%s> need   : %ld seconds", pclFct, (long)tlDifTime);
    if ((long)tlDifTime > 0)
      dbg(DEBUG,"<%s> average: %ld bytes/s", pclFct, ((long)ilNoOfBytes/(long)tlDifTime));

  }
  return RC_SUCCESS;
  
} /* end of CEDA_FTPMGetFile */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPPutFile(CEDA_FTPInfo *prpInfo,
                            char *pcpSourcePath, 
                            char *pcpSourceFile,
                            char *pcpTargetFile,
                            int   ipFlg,
                            char *pcpOptSiteCmd)
{
  char   *pclFct = "CEDA_FTPPutFile";
  int    i;
  int    j; 
  int    k;
  int    ilRC;
  int    ilWait;
  int    ilMatch = FALSE;
  long   llFileLen;
  /* float  flWait; */
  char   *pclFileBuffer = NULL;
  char   *pclS = NULL;
  char   *pclD = NULL;
  char    pclTmpBuf[iMIN];
  char    pclFileName[iMAX_BUF_SIZE];
  char    pclSourcePathAndFile[iMAX_BUF_SIZE];
  char    pclTargetFileName[iMAX_BUF_SIZE];
  char    pclTmpFileBuffer[0xFFFF];
  DIR    *prlAktDir = NULL;
  struct dirent *prlDirent = NULL;
  FILE   *pfh = NULL;


	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPPutFile> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPPutFile> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpSourcePath || (pcpSourcePath && !strlen(pcpSourcePath)))
	{
		dbg(TRACE,"<CEDA_FTPPutFile> missing pcpSourcePath");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpSourceFile || (pcpSourceFile && !strlen(pcpSourceFile)))
	{
		dbg(TRACE,"<CEDA_FTPPutFile> missing pcpSourceFile");
		return iFTP_FOREVER_INVALID;
	}
	else
	{
		strcpy(pclFileName,pcpSourceFile);
		ilMatch = TRUE;
	}
    
	if(strstr(pcpSourceFile,"*") != NULL)   /* Wildcard in Filename */
	{
		ilMatch = FALSE;
		/* open this directory... */
		dbg(DEBUG,"<%s> open directory <%s>",pclFct,pcpSourcePath);
		if ((prlAktDir = opendir(pcpSourcePath)) == NULL)
		{
			dbg(TRACE,"<%s> opendir <%s> returns NULL", pclFct,pcpSourcePath);
			return RC_FAIL;
		}

		/* search file */
		while ((prlDirent = readdir(prlAktDir)) != NULL)
		{
            
			/* don't use .xxxx files (.profile, .exrc, .cshrc, ...) */
#ifdef _SNI
			if ((prlDirent->d_name-2)[0] == '.')
#else
			if ((prlDirent->d_name)[0] == '.')
#endif
			{
#ifdef _SNI
				dbg(DEBUG,"<%s> continue because file is <%s>", pclFct,prlDirent->d_name-2);
#else
				dbg(DEBUG,"<%s> continue because file is <%s>", pclFct,prlDirent->d_name);
#endif
				continue;
			}

			/* check file -> match it pattern? */
#ifdef _SNI
			dbg(DEBUG,"<%s> compare <%s> and <%s>", pclFct, prlDirent->d_name-2, pcpSourceFile);
#else
			dbg(DEBUG,"<%s> compare <%s> and <%s>", pclFct, prlDirent->d_name, pcpSourceFile);
#endif

#ifdef _SNI
			if ((ilRC = MatchPattern(prlDirent->d_name-2, pcpSourceFile)) == 1)
#else
			if ((ilRC = MatchPattern(prlDirent->d_name, pcpSourceFile)) == 1)
#endif
			{
				/* found file... */
				dbg(DEBUG,"<%s> ...MATCH...",pclFct);
#ifdef _SNI
				dbg(DEBUG,"<%s> found file <%s>", pclFct,prlDirent->d_name-2);
#else
				dbg(DEBUG,"<%s> found file <%s>", pclFct,prlDirent->d_name);
#endif
				ilMatch = TRUE;
	
				/* copy Filename */
#ifdef _SNI
				strcpy(pclFileName,prlDirent->d_name-2);
#else
				strcpy(pclFileName,prlDirent->d_name);
#endif
				break;
			}

		} /* end of while */

		/* close the directory */
		if (prlAktDir != NULL)
		{
			dbg(DEBUG,"<%s> close directory...",pclFct);
			closedir(prlAktDir);
			prlAktDir = NULL;
		}
    }

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPPutFile> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

    if(ilMatch == TRUE)
	{
		/* build path & filename */
		strcpy(pclSourcePathAndFile, pcpSourcePath);
		if (pcpSourcePath[strlen(pcpSourcePath)-1] != '/')
			strcat(pclSourcePathAndFile, "/");
		strcat(pclSourcePathAndFile, pclFileName);
    }
	else
	{
		dbg(TRACE,"<%s> no file found",pclFct);
		return iFTP_FOREVER_INVALID;
	}
	/* build target-file name */
	if (!pcpTargetFile || (pcpTargetFile && !strlen(pcpTargetFile)))
		strcpy(pclTargetFileName, pclFileName);
	else
		strcpy(pclTargetFileName, pcpTargetFile);

	/* read the file */
	if ((ilRC = tool_fn_to_buf(pclSourcePathAndFile, &llFileLen, &pclFileBuffer)) != RC_SUCCESS)
	{
		dbg(DEBUG,"<CEDA_FTPPutFile> tool_fn_to_buf returns: %d", ilRC);
		return RC_SUCCESS;
	}
	dbg(DEBUG,"<CEDA_FTPPutFile> file-len is %ld bytes", llFileLen);

	/* create a valid control-socket */
	if ((prpInfo->iDataSocket = tcp_create_socket(SOCK_STREAM, NULL)) == RC_FAIL)
	{
		dbg(TRACE,"<CEDA_FTPPutFile> error creating socket");
		free(pclFileBuffer);
		return iFTP_TRY_AGAIN;
	}

	sprintf(prpInfo->pcCommand, "PASV%c%c", 0x0D, 0x0A);
	if ((ilRC = WriteAndRead(prpInfo, CEDA_PUTFILE)) != RC_SUCCESS)
		return ilRC;
	strcpy(pclTmpBuf, GetDataField(prpInfo->pcReply, 4, ' '));

	(void) DeleteCharacterInString(pclTmpBuf, '(');
	(void) DeleteCharacterInString(pclTmpBuf, ')');

	prpInfo->rRemoteAddress.lByte4 = atol(GetDataField(pclTmpBuf, 0, ','));
	prpInfo->rRemoteAddress.lByte3 = atol(GetDataField(pclTmpBuf, 1, ','));
	prpInfo->rRemoteAddress.lByte2 = atol(GetDataField(pclTmpBuf, 2, ','));
	prpInfo->rRemoteAddress.lByte1 = atol(GetDataField(pclTmpBuf, 3, ','));
	prpInfo->rRemotePort.sHigh = (short)atoi(GetDataField(pclTmpBuf, 4, ','));
	prpInfo->rRemotePort.sLow  = (short)atoi(GetDataField(pclTmpBuf, 5, ','));

	if ((ilRC = TcpOpenConnection(prpInfo)) != RC_SUCCESS)
	{
		dbg(TRACE,"<CEDA_FTPPutFile> error opening connection...");
		return iFTP_TRY_AGAIN;
	}
	/* optional Site Command to send before Sending File */
	if (strcmp(pcpOptSiteCmd,"") != 0)
	{
		sprintf(prpInfo->pcCommand, "SITE %s%c%c", pcpOptSiteCmd, 0x0D, 0x0A);
		dbg(DEBUG,"<CEDA_FTPPutFile> Sending SiteCmd <%s>",prpInfo->pcCommand);
    	if ((ilRC = WriteAndRead(prpInfo, CEDA_SITECMD)) != RC_SUCCESS)
			return ilRC;

    }
	/* create or append/create the file */
	if (ipFlg == CEDA_APPEND)
		sprintf(prpInfo->pcCommand, "APPE %s%c%c", pclTargetFileName, 0x0D, 0x0A);
	else
		sprintf(prpInfo->pcCommand, "STOR %s%c%c", pclTargetFileName, 0x0D, 0x0A);
	if ((ilRC = WriteAndRead(prpInfo, CEDA_PUTFILE)) != RC_SUCCESS)
	{
		free(pclFileBuffer);
		return ilRC;
	}

	/* check port */
	if (prpInfo->iDataSocket >= 0)
	{
		/* if server runs at windows an client at unix */
		/* if type is set to ASCII, change EOL-character if necessary */
		if (prpInfo->cTransferType == CEDA_ASCII && prpInfo->iClientOS == OS_UNIX && prpInfo->iServerOS == OS_WIN)
		{
			dbg(DEBUG,"<CEDA_FTPPutFile> change file now, add 0x0D...");
			memset((void*)pclTmpFileBuffer, 0x00, 0xFFFF);
			pclD = pclTmpFileBuffer;
			pclS = pclFileBuffer;
			for (i=0, j=0, ilRC=0; i<llFileLen; i++)
			{
				if (*pclS == 0x0A)
					if (i == 0)
					{
						*pclD++ = 0x0D; j++;
					}
					else if (*(pclS-1) != 0x0D)
					{
						*pclD++ = 0x0D; j++;
					}

				*pclD++ = *pclS++; j++;
				if (j == 64000)
				{
					/* now write file to port... */
					if ((k = write(prpInfo->iDataSocket, pclTmpFileBuffer, j)) != j)
					{
						dbg(TRACE,"<CEDA_FTPPutFile> tried to write %d, wrote %d bytes", j, k);
						free(pclFileBuffer);
						return iFTP_TRY_AGAIN;
					}
					memset((void*)pclTmpFileBuffer, 0x00, 0xFFFF);
					pclD = pclTmpFileBuffer;
					j = 0;
					ilRC += k;
				}
			}	
			if (j > 0)
			{
				/* now write file to port... */
				if ((k = write(prpInfo->iDataSocket, pclTmpFileBuffer, j)) != j)
				{
					dbg(TRACE,"<CEDA_FTPPutFile> tried to write %d, wrote %d bytes", j, k);
					free(pclFileBuffer);
					return iFTP_TRY_AGAIN;
				}
				ilRC += k;
			}
		}
		else
		{
			dbg(DEBUG,"<CEDA_FTPPutFile> sending file unchanged (%ld bytes)...", llFileLen);
			/* now write file to port... */
			if ((ilRC = write(prpInfo->iDataSocket, pclFileBuffer, llFileLen)) != llFileLen)
			{
				dbg(TRACE,"<CEDA_FTPPutFile> tried to write %d, wrote %d bytes", llFileLen, ilRC);
				free(pclFileBuffer);
				return iFTP_TRY_AGAIN;
			}
		}

		/* close all sockets */
		shutdown(prpInfo->iDataSocket, 2);
		close(prpInfo->iDataSocket);

		dbg(DEBUG,"<CEDA_FTPPutFile> wrote %ld bytes", ilRC);

		/* flWait = ((float)prpInfo->lGetFileTimer/1000000)+0.5; */
		/* ilWait = (int)flWait; */
		ilWait = prpInfo->lGetFileTimer; /* jetzt sind's sekunden ... eth */
		if (ilWait < 1) ilWait = 1;
		dbg(DEBUG,"<CEDA_FTPPutFile> wait %u seconds now", (unsigned int)ilWait);
		sleep((unsigned int)ilWait);
	}

	/* delete buffer */
	free(pclFileBuffer);

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPPutFile */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPmkdir(CEDA_FTPInfo *prpInfo, char *pcpDirectory)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPmkdir> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPmkdir> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpDirectory || (pcpDirectory && !strlen(pcpDirectory)))
	{
		dbg(TRACE,"<CEDA_FTPmkdir> missing pcpDirectory");
		return iFTP_FOREVER_INVALID;
	}

	/* build command */
	sprintf(prpInfo->pcCommand, "MKD %s%c%c", pcpDirectory, 0x0D, 0x0A);
	
	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_MKDIR)) != RC_SUCCESS)
		return ilRC;

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPmkdir */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPrmdir(CEDA_FTPInfo *prpInfo, char *pcpDirectory)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPrmdir> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPrmdir> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpDirectory || (pcpDirectory && !strlen(pcpDirectory)))
	{
		dbg(TRACE,"<CEDA_FTPrmdir> missing pcpDirectory");
		return iFTP_FOREVER_INVALID;
	}

	/* build command */
	sprintf(prpInfo->pcCommand, "RMD %s%c%c", pcpDirectory, 0x0D, 0x0A);

	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_RMDIR)) != RC_SUCCESS)
		return ilRC;

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPrmdir */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPrm(CEDA_FTPInfo *prpInfo, char *pcpFilename)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPrm> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}
	
	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPrm> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpFilename || (pcpFilename && !strlen(pcpFilename)))
	{
		dbg(TRACE,"<CEDA_FTPrm> missing pcpFilename");
		return iFTP_FOREVER_INVALID;
	}

	/* build command */
	sprintf(prpInfo->pcCommand, "DELE %s%c%c", pcpFilename, 0x0D, 0x0A);

	/* thats all */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_RM)) != RC_SUCCESS)
		return ilRC;

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPrm */


/* ------------------------------------------------------------------------- */
/* rkl 23.11.99 ------------------------------------------------------------ */
/*  change directory in single steps                                                */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPcd(CEDA_FTPInfo *prpInfo, char *pcpPath)
{
  int    i;
  int    ili;
  int    ilLength;
  int    ilRC=RC_SUCCESS;
  char   pclDirName[512];  /* 512 is big enough for directory name */
  char  *pclPathPtr;


  if (prpInfo == NULL)
  {
    dbg(TRACE,"<CEDA_FTPcd> missing prpInfo");
    return iFTP_FOREVER_INVALID;
  }

  /* check init */
  if (prpInfo->iInitOK != TRUE)
  {
    dbg(TRACE,"<CEDA_FTPcd> init failure, use CEDA_FTPInit first");
    return iFTP_FOREVER_INVALID;
  }

  if (!pcpPath || (pcpPath && !strlen(pcpPath)))
  {
    dbg(TRACE,"<CEDA_FTPcd> missing pcpPath");
    return iFTP_FOREVER_INVALID;
  }
    
  pclPathPtr = pcpPath;
    
  if(*pclPathPtr == '/')  /* Path is absolute */
  {
    strcpy(pclDirName,"/");
    /* build command */
    dbg(DEBUG,"<CEDA_FTPcd>: change to toplevel directory <%s>",pclDirName);
    sprintf(prpInfo->pcCommand, "CWD %s%c%c", pclDirName, 0x0D, 0x0A);

    /* change to toplevel directory */
    ilRC = WriteAndRead(prpInfo, CEDA_CD);
    dbg(DEBUG,"<CEDA_FTPcd> WriteAndRead returns %d",ilRC);
      
    pclPathPtr++;
  }

  ili = 0;
  ilLength=strlen(pclPathPtr);   
  for(i=0;((i<=ilLength)&&(ilRC==RC_SUCCESS));i++)   
  {
    if((*pclPathPtr != '/')&&(i<ilLength))
    {
      pclDirName[ili++] = *pclPathPtr;
      pclDirName[ili] = '\0';
    }  
    else  
    {
      if(((pclDirName[0] != '/') && (strlen(pclDirName)>0)) || (i==ilLength))
      {
        /* build command */
        dbg(DEBUG,"<CEDA_FTPcd>: change to directory %s",pclDirName);
        sprintf(prpInfo->pcCommand, "CWD %s%c%c", pclDirName, 0x0D, 0x0A);
        ilRC = WriteAndRead(prpInfo, CEDA_CD);
        dbg(DEBUG,"<CEDA_FTPcd> WriteAndRead returns %d",ilRC);
      }
      ili = 0;
    }
    pclPathPtr++;
  } /* end of for */
    
  dbg(DEBUG,"<CEDA_FTPcd>: ready");
  return ilRC;

} /* end of CEDA_FTPcd */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
int CEDA_FTPRemoteRename(CEDA_FTPInfo *prpInfo, char *pcpFrom, char *pcpTo)
{
	int		ilRC;

	if (prpInfo == NULL)
	{
		dbg(TRACE,"<CEDA_FTPRemoteRename> missing prpInfo");
		return iFTP_FOREVER_INVALID;
	}

	/* check init */
	if (prpInfo->iInitOK != TRUE)
	{
		dbg(TRACE,"<CEDA_FTPRemoteRename> init failure, use CEDA_FTPInit first");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpFrom || (pcpFrom && !strlen(pcpFrom)))
	{
		dbg(TRACE,"<CEDA_FTPRemoteRename> missing pcpFrom");
		return iFTP_FOREVER_INVALID;
	}

	if (!pcpTo || (pcpTo && !strlen(pcpTo)))
	{
		dbg(TRACE,"<CEDA_FTPRemoteRename> missing pcpTo");
		return iFTP_FOREVER_INVALID;
	}

	dbg(DEBUG,"<CEDA_FTPRemoteRename> rename from <%s> to <%s>", pcpFrom, pcpTo);

	/* build command */
	sprintf(prpInfo->pcCommand, "RNFR %s%c%c", pcpFrom, 0x0D, 0x0A);
	dbg(DEBUG,"<CEDA_FTPRemoteRename> sending <%s>", prpInfo->pcCommand);

	/* send it, wait for reply */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_RENAME)) != RC_SUCCESS)
        {
          dbg(DEBUG,"<CEDA_FTPRemoteRename> WriteAndRead returns %d",ilRC);
	  return ilRC;
        }

	/* build command */
	sprintf(prpInfo->pcCommand, "RNTO %s%c%c", pcpTo, 0x0D, 0x0A);
	dbg(DEBUG,"<CEDA_FTPRemoteRename> sending <%s>", prpInfo->pcCommand);

	/* send it, wait for reply */
	if ((ilRC = WriteAndRead(prpInfo, CEDA_RENAME)) != RC_SUCCESS)
        {
          dbg(DEBUG,"<CEDA_FTPRemoteRename> WriteAndRead returns %d",ilRC);
	  return ilRC;
        }

	/* everthing looks fine */
	return RC_SUCCESS;

} /* end of CEDA_FTPRemoteRename */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* FOLLOWING ALL LIBRARY INTERNAL FUNCTIONS                                  */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
static int ReceiveTcpIp(CEDA_FTPInfo *prpInfo)
{
  int	ilRC;
  int	ilBreakOut;
  char	pclReceiveBuf[iMAX_BUF_SIZE];
  char	pclTmpBuffer[iMAX_BUF_SIZE];
  char    *pclCPtr = NULL;
  struct  itimerval rlTimeVal;


  memset(pclReceiveBuf, 0x00, iMAX_BUF_SIZE);
 
  /* get answer from ftp-server, wait x seconds */
  ilRC = tcp_wait_timeout(prpInfo->iCtrlSocket, prpInfo->iTimeout);
  if (ilRC == RC_FAIL)
  {
    /* this is an error */
    dbg(TRACE,"ReceiveTcpIp: %05d tcp_wait_timeout error: %d", __LINE__, ilRC);
    return RC_FAIL;
  }
  else if (ilRC == RC_NOT_FOUND) 
  {
    /* timeout */	
    return RC_TIMEOUT;
  }
  else /* RC_SUCCESS */
  {
    ilBreakOut = FALSE;
    prpInfo->pcReply[0] = 0x00;

    do
    {
      /* clear buffer */
      memset(pclTmpBuffer, 0x00, iMAX_BUF_SIZE);

      do
      {
        /* read always blocks of 128 byte */
        /* set interrupt (SIGALRM) to xx sec */
        rlTimeVal.it_interval.tv_sec = 0;
        rlTimeVal.it_interval.tv_usec = 0;
        rlTimeVal.it_value.tv_sec = prpInfo->lReceiveTimer;
        rlTimeVal.it_value.tv_usec = 0;
        ilRC=setitimer(ITIMER_REAL, &rlTimeVal, NULL);
        ilRC = read(prpInfo->iCtrlSocket, pclTmpBuffer, 128);
        alarm(0);

        if (ilRC <= 0) 
          ilBreakOut = TRUE;

      } while (ilRC <= 0 && ilBreakOut == FALSE);

      if (ilRC > 0)
      {
        /* copy to Buffer */
        if (!strlen(pclReceiveBuf))
          strcpy(pclReceiveBuf, pclTmpBuffer);
        else
          strcat(pclReceiveBuf, pclTmpBuffer);
      }
    } while (ilBreakOut == FALSE);

    dbg(DEBUG,"ReceiveTcpIp: received Buffer: <%s>", pclReceiveBuf);

    if (pclReceiveBuf[strlen(pclReceiveBuf)-1] == 0x0A)
      pclReceiveBuf[strlen(pclReceiveBuf)-1] = 0x00;
    if (pclReceiveBuf[strlen(pclReceiveBuf)-1] == 0x0D)
      pclReceiveBuf[strlen(pclReceiveBuf)-1] = 0x00;

    /* if two Reply in buffer - use the 2nd */	
    if((pclCPtr = strstr(pclReceiveBuf,"\r\n")) != NULL)
		{
			char clReplyCode[24];
	
			/*** MCU 20020427: check if there is a reply code in second line **/	
			strncpy(clReplyCode,pclCPtr+2,4); 
			clReplyCode[3] = '\0';
			if (atoi(clReplyCode) > 0)
			{
				/*** could be a valid reply code, we can use this line **/
      	strcpy(prpInfo->pcReply ,pclCPtr+2);
			}
			else
			{
				/*** no reply code, we use the first line **/
      	strcpy(prpInfo->pcReply, pclReceiveBuf);
			}
		}
    else
		{
      strcpy(prpInfo->pcReply, pclReceiveBuf);
		}

    dbg(DEBUG,"ReceiveTcpIP: use Reply=<%s>",prpInfo->pcReply);	
    strncpy(prpInfo->pcReplyCode, prpInfo->pcReply, 3);
    prpInfo->pcReplyCode[3] = 0x00;
    prpInfo->iReplyCode = atoi(prpInfo->pcReplyCode);
    /* dbg(DEBUG,"FTP: reply code: <%d>", prpInfo->iReplyCode); */

  } /* end of RC_SUCCESS */

  /* bye bye */
  return RC_SUCCESS;

} /* end of ReceiveTcpIp */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
static int HandleReply(CEDA_FTPInfo *prpInfo, int ipFunction, int *pip2ndRead)
{
	int 		ilRC;

	*pip2ndRead = FALSE;
	switch (ipFunction)
	{
		case CEDA_CONNECT:
			/* this is for the connect function */
			switch (prpInfo->iReplyCode)
			{
				case 230:
					*pip2ndRead = TRUE;
				case 220:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_CONNECT> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_LOGIN_USER:
			/* this is for the login function, with USER */
			switch (prpInfo->iReplyCode)
			{
				case 331:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_LOGIN_USER> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_LOGIN_PASS:
			/* this is for the login function, with PASS */
			switch (prpInfo->iReplyCode)
			{
				case 230:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_LOGIN_PASS> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_SETTYPE:
		case CEDA_SET_FILE_STRUCTURE:
		case CEDA_SET_TRANSFER_MODE:
			/* this is for the login function, with PASS */
			switch (prpInfo->iReplyCode)
			{
				case 200:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_SET_xxx> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_BYE:
			/* this is for the login function, with PASS */
			switch (prpInfo->iReplyCode)
			{
				case 221:
				case 226:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_BYE> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_CD:
		case CEDA_MKDIR:
		case CEDA_RMDIR:
			/* this is for the login function, with PASS */
			switch (prpInfo->iReplyCode)
			{
				case 250:
				case 257:
					ilRC = RC_SUCCESS;
					break;
				default:
					dbg(TRACE,"<CEDA_CD/MKDIR/RMDIR> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_NLST:
			switch (prpInfo->iReplyCode)
			{
				case 125:
				case 150:
				case 226:
				case 250:
					ilRC = RC_SUCCESS;
					break;

				default:
					dbg(TRACE,"<CEDA_GETFILE> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;


		case CEDA_GETFILE:
			switch (prpInfo->iReplyCode)
			{
				case 125:
				case 150:
				case 200:
				case 221:
				case 225:
				case 226:
				case 227:
				case 250:
/*				case 550:*/
					ilRC = RC_SUCCESS;
					break;

				default:
					dbg(TRACE,"<CEDA_GETFILE> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_PUTFILE:
			switch (prpInfo->iReplyCode)
			{
				case 125:
				case 150:
				case 200:
				case 221:
				case 225:
				case 226:
				case 227:
					ilRC = RC_SUCCESS;
					break;		

				default:
					dbg(TRACE,"<CEDA_PUTFILE> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_SITECMD:
			switch (prpInfo->iReplyCode)
			{
				case 200:
				case 202:
					ilRC = RC_SUCCESS;
					break;

				default:
					dbg(TRACE,"<CEDA_SITECMD> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;
			
		case CEDA_RM:
			switch (prpInfo->iReplyCode)
			{
				case 226:
				case 250:
					ilRC = RC_SUCCESS;
					break;

				default:
					dbg(TRACE,"<CEDA_RM> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		case CEDA_RENAME:
			switch (prpInfo->iReplyCode)
			{
				case 226:
				case 250:
				case 350:
					ilRC = RC_SUCCESS;
					break;

				default:
					dbg(TRACE,"<CEDA_RENAME> unexpected reply code %d", prpInfo->iReplyCode);
					ilRC = iFTP_TRY_AGAIN;
					break;
			}
			break;

		default:
			dbg(TRACE,"<CEDA_FUNCTION> unknown function received");
			ilRC = iFTP_TRY_AGAIN;
			break;
	}

	/* bye bye */
	return ilRC;

} /* end of HandleReply */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
static int WriteAndRead(CEDA_FTPInfo *prpInfo, int ipFunction)
{
  int	ilRC;
  int	ilReceived;
  int	il2ndRead;

  /* dbg(TRACE,"WriteAndRead: prpInfo->pcCommand=<%s>",prpInfo->pcCommand);*/

/* write it to socket */
  if ((ilRC = write(prpInfo->iCtrlSocket,
                    prpInfo->pcCommand,
                    strlen(prpInfo->pcCommand)))
            != (ssize_t)strlen(prpInfo->pcCommand))
  {
    dbg(TRACE,"FTP: %05d can't write to CtrlSocket", __LINE__);
    return iFTP_TRY_AGAIN;
  }

  /* read the reply */
  ilReceived = 0;

  do
  {
    while ((ilRC = ReceiveTcpIp(prpInfo)) == RC_SUCCESS)
    {
      ilReceived = 1;

      if ((ilRC = HandleReply(prpInfo, ipFunction, &il2ndRead)) != RC_SUCCESS)
        return ilRC;

      if(il2ndRead == TRUE)
        ilReceived = 0;

    } /* end while */

    /* hier abbrechen */
    if (ilRC == RC_FAIL)
      return iFTP_FOREVER_INVALID;

  } while (!ilReceived);

  /* everthing looks fine */
  return RC_SUCCESS;

} /* end of WriteAndRead */


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
static int TcpOpenConnection(CEDA_FTPInfo *prpInfo)
{
  int	rc;
  struct sockaddr_in	dummy;

  dummy.sin_family = AF_INET;
  dummy.sin_port   = htons((prpInfo->rRemotePort.sHigh << 8) | prpInfo->rRemotePort.sLow);
  dummy.sin_addr.s_addr = htonl((prpInfo->rRemoteAddress.lByte4 << 24) |
                                (prpInfo->rRemoteAddress.lByte3 << 16) | 
                                (prpInfo->rRemoteAddress.lByte2 << 8)  | 
                                (prpInfo->rRemoteAddress.lByte1));

   dbg(DEBUG,"TcpOpenConnection: RemoteAddress: %d.%d.%d.%d  Port:%d.%d",
             prpInfo->rRemoteAddress.lByte4,
             prpInfo->rRemoteAddress.lByte3,
             prpInfo->rRemoteAddress.lByte2,
             prpInfo->rRemoteAddress.lByte1,
             prpInfo->rRemotePort.sHigh,
             prpInfo->rRemotePort.sLow);


  if ((rc = connect(prpInfo->iDataSocket, (struct sockaddr*)&dummy, sizeof(dummy))) < 0)
  {
    dbg(TRACE,"%05d TcpOpenConnection: Connect: %s", __LINE__, strerror(errno) );
    return RC_FAIL;
  } 

  dbg(DEBUG,"<TcpOpenConnection> got connection %05d", __LINE__); 
  return RC_SUCCESS;                                         

} /* end of TcpOpenConnection */

