package jcedalib;

import java.util.*;
//import java.sql.*;
import java.text.*;

public class HandleSpecial 
{
      //static String sgFields = "URNO,ADID,FLNO,STOD,OFBL,PSTA,FTYP,LAND,ONBL";
      //static String sgData = "141064780,D,TG 031,20041222101510,20041222101510,,O,,";

	public void HandleSpecial() 
      {
	}

      
      public static String HandleVDGS(String slFields, String slData)
      {
            Hashtable htCedaData = new Hashtable();
            StringBuffer sbData = new StringBuffer(slData);
            StringTokenizer stFields = new StringTokenizer(slFields,",");
            StringTokenizer stData = new StringTokenizer(slData,","); 
            String slResult = "<?xml version=\"1.0\"?>\n<MSG>\n<MSGSTREAM_OUT>\n<INFOBJ_GENERIC>\n<MESSAGEORIGIN>UFIS</MESSAGEORIGIN>\n<TIMEID>UTC</TIMEID>\n<TIMESTAMP>";
            String slCedaTime ="20050118160000";
            String slAdid = "";
            String slTemp = "";
            int start = 0;
            int end = 0;
            int sbDataLength = sbData.length();
            end = sbData.indexOf(",", start);
            
            while (stFields.hasMoreElements()) 
            {
                  
                  htCedaData.put(stFields.nextToken(), sbData.substring(start,end));
                  start = end + 1;
                  end = sbData.indexOf(",", start);
                  if ( end == -1 ) end = start;
                  
            }
     	   // Timestamp ActualTime = new Timestamp(System.currentTimeMillis());
	  //  slCedaTime = ActualTime.toLocaleString();
 // 	    slCedaTime = ActualTime.toGMTString();
 //	    slCedaTime = ActualTime.toGMTString();
            Date myDate = new Date();
            myDate.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss"); 
            FieldPosition pos = new FieldPosition(0);
            StringBuffer empty = new StringBuffer();
            StringBuffer date = sdf.format(myDate, empty, pos);
            slCedaTime = date.toString();

        


            slResult = slResult + slCedaTime + "</TIMESTAMP>\n<MESSAGETYPE>UFISVGDUD</MESSAGETYPE>\n";
            if (htCedaData.containsKey("URNO") == true)
            {
                  slTemp = (String)htCedaData.get("URNO");
                  if (slTemp.length() > 0)
                  {
                        slResult = slResult + "<URNO>" + htCedaData.get("URNO") + "</URNO>\n";
                  }
            }
            if (htCedaData.containsKey("ADID") == true)
            {
                  slAdid = (String)htCedaData.get("ADID");
                  slResult = slResult + "<ADID>" + slAdid + "</ADID>\n";
            }
            else
            {
                  slResult = slResult + "<ADID></ADID>\n";
            }
            if (slAdid.equals("A"))
            {
                  if (htCedaData.containsKey("STOA") == true)
                  {
                        slResult = slResult + "<STDT>" + htCedaData.get("STOA") + "</STDT>\n";
                  }
            }
            else
            {
                  if (slAdid.equals("D"))
                  {
                        if (htCedaData.containsKey("STOD") == true)
                        {
                              slResult = slResult + "<STDT>" + htCedaData.get("STOD") + "</STDT>\n";
                        }
                  }
                  else
                  {
                        slResult = slResult + "<STDT></STDT>\n";
                  }
            }
            if (htCedaData.containsKey("FLNO") == true)
            {
                  slTemp = (String)htCedaData.get("FLNO");
                  if (slTemp.length() > 0)
                  {
                      slResult = slResult + "<FLNO>" + htCedaData.get("FLNO") + "</FLNO>\n";
                  }
            }
            if (htCedaData.containsKey("CSGN") == true)
            {
                  slTemp = (String)htCedaData.get("CSGN");
                  if (slTemp.length() > 0)
                  {
                        slResult = slResult + "<CSGN>" + htCedaData.get("CSGN") + "</CSGN>\n";
                  }
            }
            if (htCedaData.containsKey("RKEY") == true)
            {
                  slTemp = (String)htCedaData.get("RKEY");
                  if (slTemp.length() > 0)
                  {
                   slResult = slResult + "<RKEY>" + htCedaData.get("RKEY") + "</RKEY>\n";
                  }
            }
            if (htCedaData.containsKey("RTYP") == true)
            {
                  slTemp = (String)htCedaData.get("RTYP");
                  if (slTemp.length() > 0)
                  {
                   slResult = slResult + "<RTYP>" + htCedaData.get("RTYP") + "</RTYP>\n";
                  }
            }
            slResult = slResult + "</INFOBJ_GENERIC>\n<INFOBJ_VDGS>\n";
            
            if (slAdid.equals("A"))
            {
                  slResult = slResult + "<VDGSARR>\n";
                  if (htCedaData.containsKey("PSTA") == true)
                  {
                        slResult = slResult + "<PSTA>" + htCedaData.get("PSTA") + "</PSTA>\n";
                  }
                  else
                  {
                        slResult = slResult + "<PSTA></PSTA>\n";
                  }
                  if (htCedaData.containsKey("ACT5") == true)
                  {
                        slResult = slResult + "<ACT5>" + htCedaData.get("ACT5") + "</ACT5>\n";
                  }
                  else
                  {
                        slResult = slResult + "<ACT5></ACT5>\n";
                  }
                  if (htCedaData.containsKey("STOA") == true)
                  {
                        slResult = slResult + "<STOA>" + htCedaData.get("STOA") + "</STOA>\n";
                  }
                  else
                  {
                        slResult = slResult + "<STOA></STOA>\n";
                  }
                  if (htCedaData.containsKey("FTYP") == true)
                  {
                        slTemp = (String)htCedaData.get("FTYP");
                        if (slTemp.length() > 0)
                        {
                               slResult = slResult + "<FTYP>" + htCedaData.get("FTYP") + "</FTYP>\n";
                        }
                  }
                  if (htCedaData.containsKey("LAND") == true)
                  {
                        slTemp = (String)htCedaData.get("LAND");
                        if (slTemp.length() > 0)
                        {
                              slResult = slResult + "<LAND>" + htCedaData.get("LAND") + "</LAND>\n";
                        }
                  }
                  if (htCedaData.containsKey("ONBL") == true)
                  {
                        slTemp = (String)htCedaData.get("ONBL");
                        if (slTemp.length() > 0)
                        {
                               slResult = slResult + "<ONBL>" + htCedaData.get("ONBL") + "</ONBL>\n";
                        }
                  }
                  slResult = slResult + "</VDGSARR>\n";
            } // end of : if (slAdid.equals("A"))

            if (slAdid.equals("D"))
            {
                  slResult = slResult + "<VDGSDEP>\n";
                  if (htCedaData.containsKey("PSTD") == true)
                  {
                        slResult = slResult + "<PSTD>" + htCedaData.get("PSTD") + "</PSTD>\n";
                  }
                  else
                  {
                        slResult = slResult + "<PSTD></PSTD>\n";
                  }
                  
                  if (htCedaData.containsKey("ACT5") == true)
                  {
                        slResult = slResult + "<ACT5>" + htCedaData.get("ACT5") + "</ACT5>\n";
                  }
                  else
                  {
                        slResult = slResult + "<ACT5></ACT5>\n";
                  }
                  if (htCedaData.containsKey("STOD") == true)
                  {
                        slResult = slResult + "<STOD>" + htCedaData.get("STOD") + "</STOD>\n";
                  }
                  else
                  {
                        slResult = slResult + "<STOD></STOD>\n";
                  }
                  if (htCedaData.containsKey("FTYP") == true)
                  {
                        slTemp = (String)htCedaData.get("FTYP");
                        if (slTemp.length() > 0)
                        {
                               slResult = slResult + "<FTYP>" + htCedaData.get("FTYP") + "</FTYP>\n";
                        }
                  }
                  if (htCedaData.containsKey("TIFD") == true)
                  {
                        slTemp = (String)htCedaData.get("TIFD");
                        if (slTemp.length() > 0)
                        {
                               slResult = slResult + "<TIFD>" + htCedaData.get("TIFD") + "</TIFD>\n";
                        }
                  }
                  if (htCedaData.containsKey("OFBL") == true)
                  {
                        slTemp = (String)htCedaData.get("OFBL");
                        if (slTemp.length() > 0)
                        {
                               slResult = slResult + "<OFBL>" + htCedaData.get("OFBL") + "</OFBL>\n";
                        }
                  }
                  slResult = slResult + "</VDGSDEP>\n";
                  
            } // end of : if (slAdid.equals("A"))
            slResult = slResult + "</INFOBJ_VDGS>\n</MSGSTREAM_OUT>\n</MSG>\n";
            
            return slResult;
      }
}