<?php
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
include("include/manager.php");
include("include/maininclude.php");

if ($Button=='Transmit') {

    //   echo "<script language=\"JavaScript1.2\">parent.Cput.location=\"cput.php?Button=LogoTransmit&FlightURNO=".$Session["FlightURNO"]."\"</script>";
}


//echo $Button;

//echo $Session["PicName"]."--1".$Session["LogoNameALC"];
//echo $LogoSelect.'--'.$Session["logoSelection"].'----';
?>



<html>
<title><?php echo $PageTitle ?></title>
<META NAME="description" CONTENT="">
<META NAME="author" CONTENT="UFIS AS">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
<META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">

<link rel="STYLESHEET" type="text/css" href="style/style.css">
<!-- In head section we should include the style sheet for the grid -->
<link rel="stylesheet" type="text/css" media="screen" href="include/jquery/themes/basic/grid.css" />

<!-- Of course we should load the jquery library -->
<script src="include/jquery/js/jquery.js" type="text/javascript"></script>

<!-- and at end the jqGrid Java Script file -->
<script src="include/jquery/js/jquery.jqGrid.js" type="text/javascript"></script>

<!-- and at end the Flight Grid Java Script file -->
<script src="include/js/flightGrid.js" type="text/javascript"></script>


</head>


<body style="font-family:Arial" bgcolor="#ffffff">
<div id="#container">
<div id="top" >

    <table border="0" width="100%" cellpadding="0" cellspacing="0" >
        <tr>
            <td width="117"><img src="pics/UFIS-Logo300dpii.jpg" alt="UFIS AS GmbH " width="117" hspace="34" border="0" title="UFIS AS GmbH "></td>
            <td width="100%" class="bluebg" align="center">
                <table>
                    <tr>
                        <td><font class="StatusTitle"><?php echo $Session["Type"] ?>&nbsp;<?php echo $Session["Number"] ?>&nbsp;</font></td>
                        <td><font class="StatusTitle"><div id ="Status"><?php echo Status(); ?></div></font></td><td><font class="StatusTitle">-</font></td>

                    </tr>
                </table>
            </td>
            <td width="250"  class="bluebg"><div id="idtime" class="Clock"><?php echo CurrentTime(); ?></div>
            </td>
        </tr>
    </table>

</div>


<div id="left"  class="left">
<div id ="LeftLogoSelection">
<?php  if ($logo_frame == "logo.php") { ?>
<TABLE  align=top border=0>
    <TR >
        <TD colspan="2"><p align = left>&nbsp;&nbsp;<strong>Logo:</strong></p>
            <div id="PreviewLogo"><img  src="<?php echo LogoPreview($_GET['LogoSelect'],$_GET['Request']) ?>" border=1 width="270" height="76"></div>
        </TD>
    </TR>
    <?php if (($Session["logoSelection"] == true && $FLZTABuse==true) || ($Session["logoSelection"] == true && $Session["Type"]!="GATE" && $Session["logoSelectionUser"] != false)) {
        $logofunction="logos".$logoSelectionType;
        ?>
<!--    <form action="logo.php" method="get" title="Logo">-->
        <?php echo $pquery->form_remote_tag(array('url'=>'controller.php?task=logopreview','update'=>'#PreviewLogo'));?>
        <input name="Request" type="hidden" value="Logo">
        <TR align=center>
            <TD colspan="2">
                <p align = left>&nbsp;&nbsp;<strong>Selection:</strong></p>
                <select name="LogoSelect" size="10">
                    <?php  $login->$logofunction();?>
                </select>
            </TD>
        </TR>

        <TR >
        <TD align=right>
            <input type="submit" name="Button" value="Preview">
            <p>


            </form>
        </TD>
        <!-- </form>
    <form action="logo.php" method="post" >-->
    <?php if ($Session["STATUS"]==1) {
        $tmp=" disabled";
        //if ($Session["STATUS"]==1) {
        echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"".$Session["FlightURNO"]."\" >";
        //} else {
        //	echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"\" >";
        //}
    }
    ?>
        <td align="left"><input type="submit" name="Button" value="Transmit" ></td>
    </form>
    </TR>
    <?php } ?>

</TABLE>
<?php } ?>
</div>
</div>
<div id="right"  >

    <div id ="MainSelection">
        <br/><br/><br/><br/>

        <!-- the grid definition in html is a table tag with class 'scroll' -->
        <center>
            <table border=1>
                <tr>
                    <td>
                        <table id="FlightData" class="scroll" cellpadding="0" cellspacing="0"></table>
                    </td>
                    <td valign="top">


                        <?php if ( ($Session["FldRows"]>1 && $Session["STATUS"]==1) || ($Session["OldCounterOpen"]==True && $Session["STATUS"]==1)  ) {?>

                        <table width="550">
                            <tr>
                                <td valign="top"><img src="pics/yeah.gif" alt="" border="0" align="top"><strong>Note:</strong></td>
                                <td>In case that the previous flight has not been closed the flight<br>
                                    has to be closed first before you can assign another one.<br><br>

                                    Click the '<strong>Close</strong>' button <br>
                                    or contact the <strong>ASOC</strong> Coordinator for more information<BR>
                                    <table>
                                        <tr>
                                            <td><strong>CHECK-IN </strong></td><td><strong> Tel.:</strong></td>
                                            <td>&nbsp;<strong>40003</strong> or </td>
                                        </tr>
                                        <tr>
                                            <td><strong>GATES </strong></td><td><strong> Tel.:</strong></td>
                                            <td>&nbsp;<strong>40002</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                            <?php } ?>
                    </td>

                </tr>
            </table>
              <table border=1>

              <tr>

                        <td>
                        <!--PRemarks-->
                        <p><strong>Predefined Remark:</strong><br>
		<!-- <p><%=$CPRemark%></p> -->
		<select name="PRemarkCode" size="3">
		<?php if ($Session["Type"]!="GATE" ) {
			echo PRemarks();

		 } ?>

            </select>
        </td>
            </tr>
            <?php if ($FreeTextSelection==true) { ?>
                <tr>
            
			<td colspan="2">
			<table>
			<tr>
			<td>
		<p></p>
		<p><strong>Free Text 1:</strong><br>
		<input type="text" size="50" maxlength=35 name="Text1" value="<?php echo FreeRemarks(1) ?>">
		<p></p>
			</td>
		</tr>


		<tr>
			<td colspan="2">
		<p></p>
		<p><strong>Free Text 2: </strong><br>
		<input type="text" size="50" maxlength=35 name="Text2" value="<?php echo FreeRemarks(2) ?>">
		<p></p>
		</td>
		</tr>
		</table>
			</td>
		</tr>
<?php } ?>
                        </table>

            <?php
			 if ($Session["Type"]=="GATE" ) {

			 {$gtoStyle = "";}
			 {$boaStyle = "";}
			 {$fncStyle = "";}
			 {$gclStyle = "";}

			if ($cpr=="") {$clrStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateOpen) {$gtoStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateBoarding) {$boaStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateFinalCall) {$fncStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateClose) {$gclStyle = "style=\"background-color: red;\"";}
		?>
		<table BORDER=0 width=100%>
			<tr>
				<td valign=top width=10% ><input type="submit" name="Button" value="Gate Open" <?php echo $gtoStyle?>></td>
				<td valign=top width=10% ><input type="submit" name="Button" value="Boarding" <?php echo $boaStyle?>></td>
				<td valign=top width=10% ><input type="submit" name="Button" value="Final Call" <?php echo $fncStyle?>></td>
				<?php if ($Session["STATUS"]==1) {?>
				<td valign=top width=10% ><input type="submit" name="Button" value="Gate Closed" <?php echo $gclStyle?>></td>
				<?php } ?>

			</tr>
			<tr>
				<td valign=top width=10% ><input type="submit" name="Button" value="Clear Gate" <?php echo $clrStyle?>></td>

			</tr>
		</table>
        <?php } else { ?>

            <table border=1 >
                <tr>
                    <td><input type="submit" name="Button" value="Open"></td>
                </tr>
                <?php

                $bValue = "Close";
                if ($RButton == "Close") {
                    $bValue = "Clear";
                }
                if ($Session["STATUS"]==1 || $RButton == "Close") {
                    ?>
                <tr>
                    <td>
                        <input type="submit" name="Button" value="<%=$bValue%>">
                    </td>
                </tr>
                <?php } ?>


            </table>
<?php } ?>
            <br>
            <a href="#" id="a1">Get data from selected row</a> <br />
        </center>

    </div>
</div>







<div id ="bottom" class="bottom">
    Bottom Footer
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
<?php echo $pquery->periodically_call_remote(array('url'=>'controller.php?task=status','update'=>'#Status','frequency'=>20));?>
    });
    $(document).ready(function() {
<?php echo $pquery->periodically_call_remote(array('url'=>'controller.php?task=ctime','update'=>'#idtime','frequency'=>30));?>
    });
</script>
</body>

</html>

<?php
//Safely close all appenders with...

LoggerManager::shutdown();
?>

