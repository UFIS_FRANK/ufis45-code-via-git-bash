#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
  static char mks_version[]   MARK_UNUSED = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fdihdl.h 1.42 2011/12/08 04:21:53GMT dka Exp  $";
/*  static char mks_version[]   MARK_UNUSED = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fdihdl.h 1.40 2011/07/12 04:21:53GMT dka Exp  $";
  static char mks_version[]   MARK_UNUSED = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fdihdl.h 1.39 2011/04/21 04:21:53GMT ble Exp  $"; */
  static char svn_version[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fdihdl.h 1.41 2011/12/08 04:21:53GMT dka Exp  $";
#endif /* _DEF_mks_version */

/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : FDIHDL                                                */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author        :  SMI                                                  */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef __FDIHDL_INC
#define __FDIHDL_INC


/* ********************************************************************** */
/* ********************************************************************** */

char pcgMVTVars[]="Recv,Command,Table,Synonyms,Regis,Alc,Suffix,Fltn,FDay,TDay,Orig,Dest,AATouch,AAOnBlock,ADOffTime,ADAirTime,DLReason1,DLTime1,DLReason2,DLTime2,EATime,EAAirport,EBTime,EDTime,EOTime,FRTouch,FROnBlock,NITime,PX1,PX2,PX3,PX4,PX5,PXT,RCTime,RRTime,RRClADAirTime,RRClEATime,SIComment1,SIComment2,Subtype";

char pcgLDMVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,FDay,TDay,Regis,Orig,Dest,Via,PXchil,TrPXchil,PXad,TrPXad,PXinf,TrPXinf,PXfirst,PXbus,PXeco,PADfirst,PADbus,PADeco,TrPXfirst,TrPXbus,TrPXeco,TrPADfirst,TrPADbus,TrPADeco,PXT,TrPXT,PXmale,PXfemale,StripBAGW,StripBAGN,StripCGOT,StripMAIL,StripDesBAGW,StripDesBAGN,StripDesCGOT,StripDesMAIL,DeadLoad";

char pcgCPMVars[]="Recv,Command,Alc,Fltn,Suffix,Regis,Orig,Dest,FDay,TDay,Tweight";

char pcgSRMVars[]="Recv,Command,Alc,Fltn,Suffix,Airb,Offbt,Orig,Dest,TDay,FDay";

char pcgSAMVars[]="Recv,Command,Alc,Fltn,Suffix,Airb,Offbt,Orig,Dest,TDay,FDay";

char pcgSLCVars[]="Recv,Command,Alc,Fltn,Suffix,Airb,Offbt,Orig,Dest,TDay,FDay";

char pcgPTMVars[]="Synonyms,Recv,Command,Fltn,FDay,TDay,Alc,Suffix,PXT,FltKey,Trans,Board,SavLoc,First,Bus,Eco";
char pcgPTMTVars[]="TRecv,TCommand,TTable,Ident,TFlt,Unit,Value,Typ,STyp,SSTyp,SSSTyp";

char pcgRQMVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgDIVVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgTPMVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgPSMVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgETMVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgFWDVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgSCRVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgBTMVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgUCMVars[]="Recv,Command,Alc,Fltn,Suffix,Regis,Orig,Dest,FDay,TDay";

char pcgDLSVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgAFTNVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgKRISCOMVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgALTEAVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgDFSVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,A-EDTime,D-EDTime,A-EATime,D-EATime";

char pcgADFVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay";

char pcgASDIVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay";

char pcgMANVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgSCOREVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgFFMVars[]="Recv,Command,Alc,Fltn,Suffix,Regis,Orig,Dest,FDay,TDay,Tweight";

char pcgUBMVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgCCMVars[]="Recv,Command,Synonyms,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgBAYVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgPALVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgCALVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

char pcgPSCVars[]="Recv,Command,Alc,Fltn,Suffix,Orig,Dest,FDay,TDay";

/*
,TDPC1Ad,TDPC1Ch,TDPC1In,TDPC1,TDPC2Ad,TDPC2Ch,TDPC2In,TDPC2,TDPC3Ad,TDPC3Ch,TDPC3In,TDPC3,TDPAd,TDPCh,TDPIn,TDP,TDBC1,TDBC2,TDBC3,TDB,TPAd,TPCh,TPInf,TC1,TC2,TC3,TP";
*/


char pcgOthersVars[]="Synonyms";

char pcgCmpTlx[]="Recv,Command,Table,Alc,CreateDate,Suffix,Fltn,FltKey,FltUrno,MsgText,Status,SendRec,RcvTime,TlxTyp,TlxTxt1,TlxTxt2,PartNo,TlxKey,Unique,Contrib,Seque,saveflist,SubType";

char pcgTrash[]="\t\r\001\002\003";

char pcgClientChars[100];  /*="\042\047\054\012\015";  chars on Client  " ' , \n \r  */
char pcgServerChars[100];  /*="\260\261\262\263\263";  chars on Server */

#define MAX_TLX_ARR    16 /* inital number of array elements*/
#define ADD_TLX_ARR    3  /* add number of array elements to array*/
#define FDI_BORDER_YEAR 90 /* year 2000 problem year > Border=1900, year < border=2000*/
#define COMPLETE_TELEX_LEN 32000
#define FDI_MAX_TLX_TEXT 2000 /* max Number of characters in Database field  */
#define MAX_DL_REASON  20  /* max Number of Delay Reasons*/
#define MAX_PX_ENTRIES 3   /* max Number of PX entries in telex*/
#define MAX_REGIST 10
#define FDI_GET_PTM  'R'     /* Flag for retrieval  */
#define FDI_DEF_TIME_SPAN 10 /*  default time span for deletion of arr entries*/
#define FDI_CEDA_DATELEN 14
#define FDI_END_WITH_REGIS 1 /* end prune flight after registration*/
#define FDI_SUBST_COMMA 178
#define FDI_DATE     100   /*  Flag for Date */    
#define FDI_ALC      101   /*  Flag for Airline code */
#define FDI_AIRP     102   /*  Flag for Airport code */
#define END_TELEX_STRING      -42 /* End of Telex*/

/* Status used by FDIHDL */
#define FDI_STAT_ASSIGNED             'A' /* Flight assigned */
#define FDI_STAT_ERROR                'E' /* Erreonous text */
#define FDI_STAT_FLT_UNCHANGED        'F' /* Flight unchanged */
#define FDI_STAT_INTERPRETED          'I' /* Interpreted only */
#define FDI_STAT_FLT_NOT_FOUND        'N' /* Flight not found */
#define FDI_STAT_OTHER_AIRPORT        'O' /* Other airport */
#define FDI_STAT_PTM_PART             'P' /* Telex part stored (PTM) */
#define FDI_STAT_TYPE_RECOGNIZED      'T' /* Type recognized */
#define FDI_STAT_FLT_UPDATED          'U' /* Flight updated */
#define FDI_STAT_WRITTEN              'W' /* Telex initially written */
#define FDI_STAT_TYPE_UNKNOWN         'Z' /* Unknown type */
#define FDI_STAT_CURRENT              '1' /* Currently processed telex */
#define FDI_STAT_TOO_MANY_FLTS        '?' /* Too many flights found */
#define FDI_STAT_TLX_TOO_OLD          'H' /* Telex too old */
#define FDI_STAT_NEW_FLT              'Y' /* Flight inserted (AFTN/ADF) */
#define FDI_STAT_FLT_DELETED          'D' /* Flight deleted (ADF) */
#define FDI_STAT_INVALID_LEG          'G' /* Invalid Leg */
#define FDI_STAT_FLIGHT_DATE_CONFLICT 'B' /* Flight Date Conflict */
#define FDI_STAT_FLIGHT_TOO_OLD       'J' /* Flight Too Old */
#define FDI_STAT_FLIGHT_CLOSED        'K' /* Flight Already Closed */

/* Status not used by FDIHDL */
#define FDI_STAT_CREATED         'C' /* Telex created (tlxgen)*/
#define FDI_STAT_SENT            'S' /* Telex was sent (tlxgen)*/
#define FDI_STAT_VIEWED          'V' /* Already viewed (TelexPool)*/
#define FDI_STAT_EXCLUDED        'X' /* Excluded (TelexPool) */
#define FDI_STAT_LOCAL_FLIGHT    'L' /* Local flight (ATC) */
#define FDI_STAT_BAD_ROUTING     'R' /* Bad routing (ATC) */
#define FDI_STAT_MSG_TOO_EARLY   '.' /* Message too early (ATC) */

#define FDI_MVT           1
#define FDI_LDM           2
#define FDI_PTM           3
#define FDI_SAM           4
#define FDI_SRM           5
#define FDI_SLC           6
#define FDI_CPM           7
#define FDI_RQM           8
#define FDI_DIV           9
#define FDI_SCR           10
#define FDI_BTM           11
#define FDI_TPM           12
#define FDI_PSM           13
#define FDI_ETM           14
#define FDI_FWD           15
#define FDI_UCM           16
#define FDI_DLS           17
#define FDI_AFTN          18
#define FDI_KRISCOM       19
#define FDI_OTHERS        20
#define FDI_DFS           21
#define FDI_ADF           22
#define FDI_ASDI          23
#define FDI_MAN           24
#define FDI_SCORE         25
#define FDI_FFM           26
#define FDI_UBM           27
#define FDI_CCM           28
#define FDI_BAY           29
#define FDI_PAL           30
#define FDI_CAL           31
#define FDI_PSC           32
#define FDI_ALTEA         33

#define FDI_AA  1
#define FDI_AD  2
#define FDI_DL  3
#define FDI_EA  4
#define FDI_EB  5
#define FDI_ED  6
#define FDI_EO  7
#define FDI_FR  8
#define FDI_PX  9
#define FDI_RC  10
#define FDI_RR  11
#define FDI_SI  12

#define BUF_SIZE_ALTEA  2400000 

static int HandleAA(char *pcpLine); 
static int HandleAD(char *pcpLine);
static int HandleDL(char *pcpLine);
static int HandleEA(char *pcpLine);
static int HandleEB(char *pcpLine);
static int HandleED(char *pcpData);
static int HandleEO(char *pcpLine);
static int HandleFR(char *pcpLine);
static int HandleNI(char *pcpData);
static int HandlePX(char *pcpLine);
static int HandleRC(char *pcpLine);
static int HandleRR(char *pcpLine);
static int HandleSI(char *pcpLine);
/*
typedef struct {
char Field[20];
int Count;
} T_PTMARRAY;
#define PTMARRAY  sizeof(T_PTMARRAY)

T_PTMARRAY pcgPTMArray[]={{" ",1},{" ",2},{"TDPC1Ad",3},{"TDPC1Ch",4},{"TDPC1In",5},{"TDPC2Ad",6},{"TDPC2Ch",7},{"TDPC2In",8},{"TDPC2",9},{"TDPC3Ad",10},{"TDPC3Ch",11},{"TDPC3In",12},{"TDPC3",13},{"TDPAd",14},{"TDPCh",15},{"TDPIn",16},{"TDP",17},{"TDBC1",18},{"TDBC2",19},{"TDBC3",20},{"TDB",21},{"TPAd",22},{"TPCh",23},{"TPInf",24},{"TC1",25},{"TC2",26},{"TC3",27},{"TP",28},{"TDPC1",29},{"",-1}};
*/

typedef struct {
int Adult;
int Chd;
int Inf;
int Sum;
} T_PAXLIST;
#define PAXLIST  sizeof(T_PAXLIST)

typedef struct {
T_PAXLIST Class1;
T_PAXLIST Class2;
T_PAXLIST Class3;
T_PAXLIST Class4;
T_PAXLIST Sum;
} T_PCLASSLIST;
#define PCLASSLIST  sizeof(T_PCLASSLIST)

typedef struct {
int Class1;
int Class2;
int Class3;
int Class4;
int Sum;
} T_BAGLIST;
#define BAGLIST  sizeof(T_BAGLIST)

typedef struct {
int Class1;
int Class2;
int Class3;
int Class4;
int Sum;
} T_BAGWEIGHTLIST;
#define BAGWEIGHTLIST  sizeof(T_BAGWEIGHTLIST)

typedef struct {
T_PCLASSLIST Pax;
T_BAGLIST Bag;
T_BAGWEIGHTLIST BagWeight;
char Flight[15];
char FlightDate[32];
char Dest[10];
char FlightUrno[15];
} T_DSTITEM;
#define DSTITEM  sizeof(T_DSTITEM)

typedef struct {
int Tadult;
int TChd;
int TInf;
int TClass1;
int TClass2;
int TClass3;
int TClass4;
int TPax;
} T_TOTPAX;
#define TOTPAX sizeof(T_TOTPAX)

typedef struct {
T_TOTPAX  TotalPax;
T_BAGLIST TotalBag;
T_BAGWEIGHTLIST TotalBagWeight;
} T_SUMLIST;
#define SUMLIST sizeof(T_SUMLIST)

typedef struct {
T_SUMLIST  Sum;  
T_DSTITEM *DstArray;
int AlloNum;
int PosNum;
} T_RESLIST;
#define RESLIST  sizeof(T_RESLIST)


typedef struct {
char DName[64];
char FName[64];
} T_FIELDLIST;
#define FIELDLIST  sizeof(T_FIELDLIST)

typedef struct {
  char key[32];
}T_FIELDLIST_KEY;
#define FIELDLIST_KEY sizeof(T_FIELDLIST_KEY)

typedef struct {
  int Flag;
  char DName[64];
  char DValue[4001];
  char FName[64];
  char FValue[4001];
} T_TLXRESULT ;
#define TLXRESULT sizeof(T_TLXRESULT)

typedef struct {
  int Flag;
  char DName[64];
  char DValue[4001];
  char FName[64];
  char FValue[4001];
} T_TLXHEAD ;
#define TLXHEAD sizeof(T_TLXHEAD)


typedef struct {
  char TlxType[32];
  char Unique[32];
  char Flag[32];
  int Count;
  CCS_KEY *MasterKey;
} T_TLX_ARRAY_ELEMENT;
#define TLX_ARRAY_ELEMENT sizeof (T_TLX_ARRAY_ELEMENT)

typedef struct {
  char key[32];
}T_TLX_ARRAY_KEY;
#define TLX_ARRAY_KEY sizeof(T_TLX_ARRAY_KEY)



typedef struct {
  char key[32];
} T_TLX_ELEMENT_KEY;
#define TLX_ELEMENT_KEY sizeof(T_TLX_ELEMENT_KEY)


typedef struct {
  int Flag;
  char CfgType[32];
  CCS_KEY *MasterKey;
} T_CFG_ARRAY_ELEMENT;
#define CFG_ARRAY_ELEMENT sizeof (T_CFG_ARRAY_ELEMENT)

typedef struct {
  char key[32];
}T_CFG_ARRAY_KEY;
#define CFG_ARRAY_KEY sizeof(T_CFG_ARRAY_KEY)

typedef struct  {
  int TlxStart;
  int TlxEnd  ;
  char TlxDay[10];
  char TlxMonth[10];
  char TlxYear[10];
  char TlxNumber[10];
  char TlxTime[10];
  int TxtStart;
  int TxtEnd ;
  int TlxCmd;
} T_TLXINFO ;
#define TLXINFO sizeof(T_TLXINFO)

typedef int T_HANDLE_FUNC (char *); 

typedef struct {
  char SynTlx[100];
  int function;
  int Flag;
} T_SYNTLX;

typedef struct {
  char TlxInfo[100];
  T_HANDLE_FUNC *Function;
} T_TLXINFOCMD;

typedef struct {
  int  Used;
  char TlxKey[32];
  char ApxRurn[16];
  char ApxType[16];
  char ApxValue[32];
} APX_VALUES;
#define MAX_APX_RECS 1000

/* char pcgAllCommands[]="LDM,MVT,PDM";  */

/* T_SYNTLX prgTlxCmds[] ={ */
/*   {"\nPDM",FDI_OTHERS}, */
/*   {"\nMVT,\nMVA",FDI_MVT}, */
/*   {"\nLDM",FDI_LDM}, */
/*   {"",-1} */
/* }; */

T_SYNTLX prgTlxCmds[100] ={
  {"",-1}
};

T_TLXINFOCMD prgInfoCmd[7][20]=
{
  {
    {"\nAD",&HandleAD},{" EA",&HandleEA},{"EO",&HandleEO},{"\nDL",&HandleDL},{"\nEDL",&HandleDL},{"\nDLA",&HandleDL},
    {"\nPX,\nPAX",&HandlePX}, {"RR",&HandleRR},{"\nRC",&HandleRC},{"",NULL}
  },
  {
    {"\nAA",&HandleAA},{"EB",&HandleEB},{"",NULL}
  },
  {
    {"\nNI",&HandleNI},{"\nDL",&HandleDL},{"\nEDL",&HandleDL},{"\nDLA",&HandleDL},{"",NULL}
  },
  {
    {"\nED",&HandleED},{"\nDL",&HandleDL},{"\nEDL",&HandleDL},{"\nDLA",&HandleDL},{"",NULL}
  },
  {
    {"\nFR",&HandleFR},{"",NULL}
  },
  {
    {"\nEA",&HandleEA},{"",NULL}
  },
  {
    {"",NULL}
  }
};

typedef struct
{
   char Field1[16];
} PRE_AIRPORT;

PRE_AIRPORT pcgPreAirport[] =
   { {"\n"},
     {""},
     {"."},
     {" "} };
#define PRE_AIRPORT_NO 4

typedef struct
{
   char Field1[16];
   char Field2[16];
} MAIL_KEYS;

MAIL_KEYS pcgMailKeys[] =
   { {" POS "," "},
     {".POS/","."},
     {".POS/"," "},
     {" POS/","."},
     {"\nPOS/","."},
     {" M "," "},
     {".M","."},
     {" M/"," "},
     {"M"," "},
     {"\nM/"," "} };
#define MAIL_KEYS_NO 10

typedef struct
{
   char Field1[16];
   char Field2[16];
} CARGO_KEYS;

CARGO_KEYS pcgCargoKeys[] =
   { {" FRE "," "},
     {".FRE/","."},
     {".FRE/"," "},
     {" FRE/","."},
     {"\nFRE/","."},
     {" C "," "},
     {".C","."},
     {" C/"," "},
     {"C"," "},
     {"\nC/"," "} };
#define CARGO_KEYS_NO 10

typedef struct
{
   char Field1[16];
   char Field2[16];
} BAGGAGE_KEYS;

BAGGAGE_KEYS pcgBaggageKeys[] =
   { {" BAG "," "},
     {".BAG/","."},
     {".BAG/"," "},
     {" BAG/","."},
     {"\nBAG/","."},
     {" B "," "},
     {".B","."},
     {" B/"," "},
     {"B"," "},
     {" B"," "},
     {"\nB/"," "} };
#define BAGGAGE_KEYS_NO 11

typedef struct
{
   char Field1[16];
   char Field2[16];
} OTHER_KEYS;

OTHER_KEYS pcgOtherKeys[] =
   { {" O "," "},
     {".O","."},
     {" O/"," "},
     {"\nO/"," "} };
#define OTHER_KEYS_NO 4

typedef struct
{
   char Field1[16];
   char Field2[16];
} TRANSIT_KEYS;

TRANSIT_KEYS pcgTransitKeys[] =
   { {" TRA "," "},
     {".TRA/","."},
     {".TRA/"," "},
     {" TRA/","."},
     {"\nTRA/","."},
     {" TR "," "},
     {".TR/","."},
     {".TR/"," "},
     {" TR/","."},
     {"\nTR/","."},
     {" T "," "},
     {".T","."},
     {" T/"," "},
     {"\nT/"," "} };
#define TRANSIT_KEYS_NO 14

typedef struct
{
   char Text[128];
} KEY_WORDS;

KEY_WORDS pcgStartOfLine[] =
   { {"PAX WEIGHTS USED"} };
#define KEY_WORDS_NO 1

typedef struct
{
   char pclUldNumber[16];
   char pclUldDestination[8];
   char pclUldType[8];
   char pclUldAddi[128];
} UCM_VALUES;

typedef struct
{
   char pclUldNumber[16];
   char pclUldDestination[8];
   char pclUldWeight[8];
   char pclUldAddi[128];
} DLS_VALUES;

typedef struct
{
   char pclUldNumber[16];
   char pclUldDestination[8];
   char pclUldType[8];
   char pclUldPosition[8];
   char pclUldAddi[128];
   char pclUldWeight[8];
} CPM_VALUES;

typedef struct
{
   char pclDest[32];
   char pclPaxFirst[32];
   char pclPaxBusiness[32];
   char pclPaxEconomy[32];
   char pclPaxEconomy2[32];
   char pclPaxMale[32];
   char pclPaxFemale[32];
   char pclPaxAdults[32];
   char pclPaxChild[32];
   char pclPaxInfants[32];
   char pclPadFirst[32];
   char pclPadBusiness[32];
   char pclPadEconomy[32];
   char pclBaggage[32];
   char pclBaggageQty[32];
   char pclMail[32];
   char pclCargo[32];
   char pclOther[32];
   char pclTransit[32];
   char pclTotal[32];
   char pclDeadLoad[32];
} LDM_VALUES;

typedef struct
{
   char pclUldDest[8];
   char pclUldType[16];
   char pclUldAddi[132];
   char pclUldWeight[8];
} FFM_VALUES;

typedef struct
{
   char pclPsmName[32];
   char pclPsmType[8];
   char pclPsmSeat[8];
   char pclPsmConUrno[16];
   char pclPsmConFlight[32];
   char pclPsmConFlightDate[32];
   char pclPsmConStod[16];
   char pclPsmClass[8];
   char pclPsmConHdlAgt[8];
} PSM_VALUES;

typedef struct
{
   char pclPalName[32];
   char pclPalType[8];
   char pclPalSeat[8];
   char pclPalClass[8];
   char pclPalConUrno[16];
   char pclPalConFlight[32];
   char pclPalConFlightDate[32];
   char pclPalConFlightDest[32];
   char pclPalConFlightClass[32];
   char pclPalConFlightAdid[4];
   char pclPalConFlightStof[16];
   char pclPalFkt[8];
   char pclPalTlxTtyp[8];
   char pclPalConHdlAgt[8];
} PAL_VALUES;

typedef struct
{
   char pclGeneralKey[64];
   char pclFlightKey[64];
   int ilNoCharAlc;
   char pclDateKey[64];
   char pclDateFormat[16];
   char pclAAKey[64];
   char pclBBKey[64];
   char pclCCKey[64];
   char pclDDKey[64];
   char pclEEKey[64];
   char pclFFKey[64];
   int ilFirstValue;
   int ilBusValue;
   int ilEcoValue;
   char pclINFKey[64];
   int ilInfValue;
} PSC_CONFIG_DATA;

typedef struct
{
   char pclATISections[100][32];
   int ilNoATISections;
   char pclDHSections[100][32];
   int ilNoDHSections;
   char pclATPISections[100][32];
   int ilNoATPISections;
   char pclATPINSSections[100][32];
   int ilNoATPINSSections;
   char pclATPINSections[100][32];
   int ilNoATPINSections;
   char pclATGSections[100][32];
   int ilNoATGSections;
   char pclLPSections[100][32];
   int ilNoLPSections;
   char pclDESections[100][32];
   int ilNoDESections;
   char pclPWSections[100][32];
   int ilNoPWSections;
   char pclTelexSections[100][32];
   int ilNoTelexSections;
} KRISCOM_MAIN;

typedef struct
{
   char pclClassCode[4];
   char pclAirlines[256];
} CLASS_CODES;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclStatus[7][4];
   char pclOrigin[1][4];
   char pclNewClassCodes[5][4];
   char pclConfig[6][32];
   char pclDestPax[4][32];
   char pclBookedPax[6][32];
   char pclCheckedInPax[6][32];
   char pclTransit[6][32];
   char pclDestLoad[3][4];
   char pclLoadDef[3][4];
   char pclUnderload[4][32];
   char pclOtherLeg[80];
   char pclDestPax2[3][4];
   char pclBookedPax2[6][4];
   char pclCheckedInPax2[6][4];
   char pclAddCkdToBkd[8];
} KRISCOM_ATI;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclDestLoad[4][32];
   char pclCargoDef[4][32];
   char pclMailDef[4][32];
   char pclBagDef[4][32];
} KRISCOM_DH;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclInfants[6][32];
   char pclClasses[3][4];
   char pclStatus[3][4];
   char pclClassCodes[3][8];
   char pclNewClassCodes[5][4];
} KRISCOM_ATPI;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclTotals[3][4];
   char pclFlight[3][4];
   char pclTrPaxClass[3][4];
   char pclTrPaxTot[3][4];
   char pclTerm[3][4];
   char pclTermText[128];
} KRISCOM_ATPINS;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclTotals[6][32];
   char pclFlight[4][8];
   char pclFltDate[4][8];
   char pclDest[4][8];
   char pclClass[4][8];
   char pclTerm[3][4];
   char pclTermText[128];
   char pclClassCodes[3][8];
   char pclNewClassCodes[5][4];
   char pclCheckedInFlag[3][4];
   char pclNoOfBags[3][4];
   char pclBagWeight[3][4];
   char pclPaxDest[4][8];
} KRISCOM_ATPIN;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclCheckedIn[4][4];
   char pclCheckedInText[128];
   char pclBoard[4][4];
   char pclBoardText[128];
   char pclYetToBoard[4][4];
   char pclYetToBoardText[128];
} KRISCOM_ATG;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
} KRISCOM_LP;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclDest[3][4];
   char pclType[3][4];
   char pclWeight[3][4];
   char pclCode[3][4];
   char pclPos[3][4];
   char pclTerm[3][4];
   char pclTermText[128];
   char pclNoValueKeywords[256];
} KRISCOM_DE;

typedef struct
{
   char pclCommands[10][16];
   int ilNoCommands;
   char pclSubType[8];
   char pclAirlines[50][4];
   int ilNoAirlines;
   char pclOrigin[1][4];
   char pclDestPax[4][32];
   char pclFclPax[6][32];
   char pclBclPax[6][32];
   char pclEclPax[6][32];
   char pclKeyWord[80];
   char pclKeyWordPax[10];
   char pclKeyWordBulk[10];
   char pclNewClassCodes[5][4];
} KRISCOM_PW;

typedef struct
{
   char pclStatus[4];
   char pclCfgFirst[4];
   char pclCfgBusiness[4];
   char pclCfgEconomy[4];
   char pclDest[4];
   char pclBkdFirst[4];
   char pclBkdBusiness[4];
   char pclBkdEconomy[4];
   char pclBkdTotal[4];
   char pclCkdFirst[4];
   char pclCkdBusiness[4];
   char pclCkdEconomy[4];
   char pclCkdTotal[4];
   char pclTraFirst[4];
   char pclTraBusiness[4];
   char pclTraEconomy[4];
   char pclTraTotal[4];
   char pclBaggage[8];
   char pclBaggageQty[8];
   char pclMail[8];
   char pclCargo[8];
   char pclTotal[8];
   char pclUnderload[8];
} ATI_VALUES;

typedef struct
{
   char pclDest[4];
   char pclBaggage[8];
   char pclMail[8];
   char pclCargo[8];
   char pclTotal[8];
} DH_VALUES;

typedef struct
{
   char pclInfFirst[4];
   char pclInfBusiness[4];
   char pclInfEconomy[4];
   char pclInfTotal[4];
   char pclInfFirstCkd[4];
   char pclInfBusinessCkd[4];
   char pclInfEconomyCkd[4];
   char pclInfTotalCkd[4];
   char pclInfFirstBkd[4];
   char pclInfBusinessBkd[4];
   char pclInfEconomyBkd[4];
   char pclInfTotalBkd[4];
   char pclInfFirstOff[4];
   char pclInfBusinessOff[4];
   char pclInfEconomyOff[4];
   char pclInfTotalOff[4];
   char pclDest[16];
} ATPI_VALUES;

typedef struct
{
   char pclTotFirst[4];
   char pclTotBusiness[4];
   char pclTotEconomy[4];
   char pclTotTotal[4];
   char pclFlight[16];
   char pclTrpFirst[4];
   char pclTrpBusiness[4];
   char pclTrpEconomy[4];
   char pclTrpTotal[4];
} ATPINS_VALUES;

typedef struct
{
   char pclTotFirst[6];
   char pclTotBusiness[6];
   char pclTotEconomy[6];
   char pclTotTotal[6];
   char pclTotFirstBag[6];
   char pclTotBusinessBag[6];
   char pclTotEconomyBag[6];
   char pclTotBag[6];
   char pclTotFirstBagWeight[6];
   char pclTotBusinessBagWeight[6];
   char pclTotEconomyBagWeight[6];
   char pclTotBagWeight[6];
   char pclFlight[16];
   char pclFltDate[16];
   char pclDest[6];
   char pclPaxDest[6];
   char pclTrpFirst[6];
   char pclTrpBusiness[6];
   char pclTrpEconomy[6];
   char pclTrpTotal[6];
   char pclTrpFirstBag[6];
   char pclTrpBusinessBag[6];
   char pclTrpEconomyBag[6];
   char pclTrpTotalBag[6];
   char pclTrpFirstBagWeight[6];
   char pclTrpBusinessBagWeight[6];
   char pclTrpEconomyBagWeight[6];
   char pclTrpTotalBagWeight[6];
} ATPIN_VALUES;

typedef struct
{
   char pclFlnuRurn[32];
   char pclFlnoApc3[32];
   char pclTotFirst[6];
   char pclTotBusiness[6];
   char pclTotEconomy[6];
   char pclTotTotal[6];
   char pclTotFirstBag[6];
   char pclTotBusinessBag[6];
   char pclTotEconomyBag[6];
   char pclTotBag[6];
   char pclTotFirstBagWeight[6];
   char pclTotBusinessBagWeight[6];
   char pclTotEconomyBagWeight[6];
   char pclTotBagWeight[6];
} ATPIN_VALUES_ARR;

typedef struct
{
   char pclFlnuRurn[32];
   char pclFlnoApc3[32];
   char pclTotFirst[6];
   char pclTotBusiness[6];
   char pclTotEconomy[6];
   char pclTotTotal[6];
   char pclTotFirstBag[6];
   char pclTotBusinessBag[6];
   char pclTotEconomyBag[6];
   char pclTotBag[6];
   char pclTotFirstBagWeight[6];
   char pclTotBusinessBagWeight[6];
   char pclTotEconomyBagWeight[6];
   char pclTotBagWeight[6];
} ATPIN_VALUES_DEP;

typedef struct
{
   char pclCheckedIn[4];
   char pclBoard[4];
   char pclYetToBoard[4];
} ATG_VALUES;

typedef struct
{
   char pclDest[4];
   char pclType[4];
   char pclWeight[8];
   char pclCode[16];
   char pclPos[4];
} DE_VALUES;

typedef struct
{
   char pclDest[4];
   char pclMaleFirst[4];
   char pclFemaleFirst[4];
   char pclChildrenFirst[4];
   char pclInfantsFirst[4];
   char pclMaleBus[4];
   char pclFemaleBus[4];
   char pclChildrenBus[4];
   char pclInfantsBus[4];
   char pclMaleEco[4];
   char pclFemaleEco[4];
   char pclChildrenEco[4];
   char pclInfantsEco[4];
   char pclPaxFirst[4];
   char pclPaxBus[4];
   char pclPaxEco[4];
   char pclTotal[4];
   char pclAdult[4];
   char pclChildren[4];
   char pclMale[4];
   char pclFemale[4];
   char pclInfants[4];
   char pclBlkFirst[8];
   char pclBlkBus[8];
   char pclBlkEco[8];
   char pclBlkTot[8];
} PW_VALUES;

typedef struct
{
   char pclText[128];
   char pclCommand[8];
   char pclFlightNo[3][4];
} TELEX_INPUT;

typedef struct
{
   char *pclAddr;
   char pclKey[32];
   char pclText[128];
} LOATAB_KEYS;

/* MEI 07-SEP-2010 */
typedef struct 
{
    int ilDseq;
    char pclDelCode[5];
    char pclDelAlpha[5];
    char pclDelNumeric[5];
    char pclDelValue[5];
    char pclDelReason[76];
    char pclDenUrno[11];
    char pclSubCode[3];
    char pclTelexCode[5];
    char spare;
} DELAY_REAS_STRUCT;

static char pcgMvtTotPax[128];

static char pcgCpmTot[128];
static char pcgCpmApt[128];
static char pcgCpmTyp[128];
static char pcgCpmTypApt[128];
static char pcgCpmDtl[128];

static char pcgFfmDtl[128];

static char pcgUcmDtl[128];

static char pcgDlsTot[128];
static char pcgDlsTotApt[128];
static char pcgDlsDtl[128];

static char pcgLdmTotPax[128];
static char pcgLdmTotPad[128];
static char pcgLdmTotAdu[128];
static char pcgLdmTotChi[128];
static char pcgLdmTotMal[128];
static char pcgLdmTotFem[128];
static char pcgLdmTotInf[128];
static char pcgLdmTotFclPax[128];
static char pcgLdmTotFclPad[128];
static char pcgLdmTotBclPax[128];
static char pcgLdmTotBclPad[128];
static char pcgLdmTotEclPax[128];
static char pcgLdmTotEclPad[128];
static char pcgLdmTotPaxApt[128];
static char pcgLdmTotPaxAptAdd[128];
static char pcgLdmTotPadApt[128];
static char pcgLdmTotPadAptAdd[128];
static char pcgLdmTotAduApt[128];
static char pcgLdmTotChiApt[128];
static char pcgLdmTotMalApt[128];
static char pcgLdmTotFemApt[128];
static char pcgLdmTotInfApt[128];
static char pcgLdmTotInfAptAdd[128];
static char pcgLdmTotFclPaxApt[128];
static char pcgLdmTotFclPaxAptAdd[128];
static char pcgLdmTotFclPadApt[128];
static char pcgLdmTotBclPaxApt[128];
static char pcgLdmTotBclPaxAptAdd[128];
static char pcgLdmTotBclPadApt[128];
static char pcgLdmTotEclPaxApt[128];
static char pcgLdmTotEclPaxAptAdd[128];
static char pcgLdmTotEclPadApt[128];
static char pcgLdmTrsPax[128];
static char pcgLdmTrsPad[128];
static char pcgLdmTrsAdu[128];
static char pcgLdmTrsChi[128];
static char pcgLdmTrsInf[128];
static char pcgLdmTrsFclPax[128];
static char pcgLdmTrsFclPad[128];
static char pcgLdmTrsBclPax[128];
static char pcgLdmTrsBclPad[128];
static char pcgLdmTrsEclPax[128];
static char pcgLdmTrsEclPad[128];
static char pcgLdmTrsPaxApt[128];
static char pcgLdmTrsPadApt[128];
static char pcgLdmTrsAduApt[128];
static char pcgLdmTrsChiApt[128];
static char pcgLdmTrsInfApt[128];
static char pcgLdmTrsFclPaxApt[128];
static char pcgLdmTrsFclPadApt[128];
static char pcgLdmTrsBclPaxApt[128];
static char pcgLdmTrsBclPadApt[128];
static char pcgLdmTrsEclPaxApt[128];
static char pcgLdmTrsEclPadApt[128];
static char pcgLdmLoaTot[128];
static char pcgLdmLoaDld[128];
static char pcgLdmLoaBag[128];
static char pcgLdmLoaCgo[128];
static char pcgLdmLoaMal[128];
static char pcgLdmLoaOth[128];
static char pcgLdmLoaTra[128];
static char pcgLdmLoaTotApt[128];
static char pcgLdmLoaTotAptAdd[128];
static char pcgLdmLoaDldApt[128];
static char pcgLdmLoaBagApt[128];
static char pcgLdmLoaBagAptAdd[128];
static char pcgLdmLoaCgoApt[128];
static char pcgLdmLoaCgoAptAdd[128];
static char pcgLdmLoaMalApt[128];
static char pcgLdmLoaMalAptAdd[128];
static char pcgLdmLoaOthApt[128];
static char pcgLdmLoaTraApt[128];

static char pcgPtmTrfPax[128];
static char pcgPtmTrfAdu[128];
static char pcgPtmTrfChi[128];
static char pcgPtmTrfInf[128];
static char pcgPtmTrfBag[128];
static char pcgPtmTrfBwt[128];
static char pcgPtmTrfFclPax[128];
static char pcgPtmTrfFclAdu[128];
static char pcgPtmTrfFclChi[128];
static char pcgPtmTrfFclInf[128];
static char pcgPtmTrfFclBag[128];
static char pcgPtmTrfFclBwt[128];
static char pcgPtmTrfBclPax[128];
static char pcgPtmTrfBclAdu[128];
static char pcgPtmTrfBclChi[128];
static char pcgPtmTrfBclInf[128];
static char pcgPtmTrfBclBag[128];
static char pcgPtmTrfBclBwt[128];
static char pcgPtmTrfEclPax[128];
static char pcgPtmTrfEclAdu[128];
static char pcgPtmTrfEclChi[128];
static char pcgPtmTrfEclInf[128];
static char pcgPtmTrfEclBag[128];
static char pcgPtmTrfEclBwt[128];
static char pcgPtmTrfPclPax[128];
static char pcgPtmTrfPclAdu[128];
static char pcgPtmTrfPclChi[128];
static char pcgPtmTrfPclInf[128];
static char pcgPtmTrfPclBag[128];
static char pcgPtmTrfPclBwt[128];
static char pcgPtmTrfPaxDtl[128];
static char pcgPtmTrfAduDtl[128];
static char pcgPtmTrfChiDtl[128];
static char pcgPtmTrfInfDtl[128];
static char pcgPtmTrfBagDtl[128];
static char pcgPtmTrfBwtDtl[128];
static char pcgPtmTrfFclPaxDtl[128];
static char pcgPtmTrfFclAduDtl[128];
static char pcgPtmTrfFclChiDtl[128];
static char pcgPtmTrfFclInfDtl[128];
static char pcgPtmTrfFclBagDtl[128];
static char pcgPtmTrfFclBwtDtl[128];
static char pcgPtmTrfBclPaxDtl[128];
static char pcgPtmTrfBclAduDtl[128];
static char pcgPtmTrfBclChiDtl[128];
static char pcgPtmTrfBclInfDtl[128];
static char pcgPtmTrfBclBagDtl[128];
static char pcgPtmTrfBclBwtDtl[128];
static char pcgPtmTrfEclPaxDtl[128];
static char pcgPtmTrfEclAduDtl[128];
static char pcgPtmTrfEclChiDtl[128];
static char pcgPtmTrfEclInfDtl[128];
static char pcgPtmTrfEclBagDtl[128];
static char pcgPtmTrfEclBwtDtl[128];
static char pcgPtmTrfPclPaxDtl[128];
static char pcgPtmTrfPclAduDtl[128];
static char pcgPtmTrfPclChiDtl[128];
static char pcgPtmTrfPclInfDtl[128];
static char pcgPtmTrfPclBagDtl[128];
static char pcgPtmTrfPclBwtDtl[128];

static char pcgPscJoiPax[128];
static char pcgPscTotInf[128];
static char pcgPscFclJoiPax[128];
static char pcgPscBclJoiPax[128];
static char pcgPscEclJoiPax[128];
static char pcgPscJoiTt1[128];
static char pcgPscFclJoiTt1[128];
static char pcgPscBclJoiTt1[128];
static char pcgPscEclJoiTt1[128];
static char pcgPscJoiTt2[128];
static char pcgPscFclJoiTt2[128];
static char pcgPscBclJoiTt2[128];
static char pcgPscEclJoiTt2[128];

static char pcgKriPaxFst[128];
static char pcgKriTotPax[128];
static char pcgKriTotCki[128];
static char pcgKriTotCkg[128];
static char pcgKriTotBoa[128];
static char pcgKriTotNyb[128];
static char pcgKriPxjPax[128];
static char pcgKriTotInf[128];
static char pcgKriTotInfCki[128];
static char pcgKriTotInfBoa[128];
static char pcgKriTotInfOff[128];
static char pcgKriPxjInf[128];
static char pcgKriCfgFclPax[128];
static char pcgKriBkdFclPax[128];
static char pcgKriCkiFclPax[128];
static char pcgKriPxjPaxFcl[128];
static char pcgKriTotFclInf[128];
static char pcgKriCkiFclInf[128];
static char pcgKriBoaFclInf[128];
static char pcgKriOffFclInf[128];
static char pcgKriPxjInfFcl[128];
static char pcgKriCfgBclPax[128];
static char pcgKriBkdBclPax[128];
static char pcgKriCkiBclPax[128];
static char pcgKriPxjPaxBcl[128];
static char pcgKriTotBclInf[128];
static char pcgKriCkiBclInf[128];
static char pcgKriBoaBclInf[128];
static char pcgKriOffBclInf[128];
static char pcgKriPxjInfBcl[128];
static char pcgKriCfgEclPax[128];
static char pcgKriBkdEclPax[128];
static char pcgKriCkiEclPax[128];
static char pcgKriPxjPaxEcl[128];
static char pcgKriTotEclInf[128];
static char pcgKriCkiEclInf[128];
static char pcgKriBoaEclInf[128];
static char pcgKriOffEclInf[128];
static char pcgKriPxjInfEcl[128];
static char pcgKriTotPaxApt[128];
static char pcgKriTotCkiApt[128];
static char pcgKriPxjTotApt[128];
static char pcgKriPxjAduApt[128];
static char pcgKriPxjChiApt[128];
static char pcgKriPxjMalApt[128];
static char pcgKriPxjFemApt[128];
static char pcgKriPxjInfApt[128];
static char pcgKriBkdFclPaxApt[128];
static char pcgKriCkiFclPaxApt[128];
static char pcgKriPxjPaxFclApt[128];
static char pcgKriPxjMalFclApt[128];
static char pcgKriPxjFemFclApt[128];
static char pcgKriPxjChiFclApt[128];
static char pcgKriPxjInfFclApt[128];
static char pcgKriBkdBclPaxApt[128];
static char pcgKriCkiBclPaxApt[128];
static char pcgKriPxjPaxBclApt[128];
static char pcgKriPxjMalBclApt[128];
static char pcgKriPxjFemBclApt[128];
static char pcgKriPxjChiBclApt[128];
static char pcgKriPxjInfBclApt[128];
static char pcgKriBkdEclPaxApt[128];
static char pcgKriCkiEclPaxApt[128];
static char pcgKriPxjPaxEclApt[128];
static char pcgKriPxjMalEclApt[128];
static char pcgKriPxjFemEclApt[128];
static char pcgKriPxjChiEclApt[128];
static char pcgKriPxjInfEclApt[128];
static char pcgKriTrfPax[128];
static char pcgKriTrfFclPax[128];
static char pcgKriTrfBclPax[128];
static char pcgKriTrfEclPax[128];
static char pcgKriTrfBag[128];
static char pcgKriTrfFclBag[128];
static char pcgKriTrfBclBag[128];
static char pcgKriTrfEclBag[128];
static char pcgKriTrfBwt[128];
static char pcgKriTrfFclBwt[128];
static char pcgKriTrfBclBwt[128];
static char pcgKriTrfEclBwt[128];
static char pcgKriTrfPaxDtl[128];
static char pcgKriTrfFclPaxDtl[128];
static char pcgKriTrfBclPaxDtl[128];
static char pcgKriTrfEclPaxDtl[128];
static char pcgKriTrfBagDtl[128];
static char pcgKriTrfFclBagDtl[128];
static char pcgKriTrfBclBagDtl[128];
static char pcgKriTrfEclBagDtl[128];
static char pcgKriTrfBwtDtl[128];
static char pcgKriTrfFclBwtDtl[128];
static char pcgKriTrfBclBwtDtl[128];
static char pcgKriTrfEclBwtDtl[128];
static char pcgKriTrsPax[128];
static char pcgKriTrsFclPax[128];
static char pcgKriTrsBclPax[128];
static char pcgKriTrsEclPax[128];
static char pcgKriLoaTot[128];
static char pcgKriLoaUdl[128];
static char pcgKriLojTot[128];
static char pcgKriLoaBag[128];
static char pcgKriLoaCgo[128];
static char pcgKriLoaMal[128];
static char pcgKriLojFcl[128];
static char pcgKriLojBcl[128];
static char pcgKriLojEcl[128];
static char pcgKriLoaTotApt[128];
static char pcgKriLojTotApt[128];
static char pcgKriLoaBagApt[128];
static char pcgKriLoaCgoApt[128];
static char pcgKriLoaMalApt[128];
static char pcgKriLojFclApt[128];
static char pcgKriLojBclApt[128];
static char pcgKriLojEclApt[128];
static char pcgKriUldTot[128];
static char pcgKriUldTotApt[128];
static char pcgKriUldTotTyp[128];
static char pcgKriUldTotDtl[128];

LOATAB_KEYS pcgLoatabKeys[] =
{
  {pcgMvtTotPax,      "MVT_TOT_PAX",        "PAX,1,010,Total Passengers"},

  {pcgCpmTot,         "CPM_TOT",            "ULD,1,000,Total Weight of ULDs"},
  {pcgCpmApt,         "CPM_APT",            "ULD,3,000,Weight of ULDs to Airport"},
  {pcgCpmTyp,         "CPM_TYP",            "ULD,3,010,Weight of ULDs with Contents"},
  {pcgCpmTypApt,      "CPM_TYP_APT",        "ULD,3,020,Weight of ULDs with Contents to Airport"},
  {pcgCpmDtl,         "CPM_DTL",            "ULD,4,000,ULD Details"},

  {pcgFfmDtl,         "FFM_DTL",            "ULD,4,000,ULD Details"},

  {pcgUcmDtl,         "UCM_DTL",            "ULD,4,000,ULD Details"},

  {pcgDlsTot,         "DLS_TOT",            "ULD,1,000,Total Weight of ULDs"},
  {pcgDlsTotApt,      "DLS_TOT_APT",        "ULD,3,000,Weight of ULDs to Airport"},
  {pcgDlsDtl,         "DLS_DTL",            "ULD,4,000,ULD Details"},

  {pcgLdmTotPax,      "LDM_TOT_PAX",        "PAX,1,010,Total Passengers"},
  {pcgLdmTotPad,      "LDM_TOT_PAD",        "PAX,1,060,Total PADs"},
  {pcgLdmTotAdu,      "LDM_TOT_ADU",        "PAX,1,070,Total Adult Passengers"},
  {pcgLdmTotChi,      "LDM_TOT_CHI",        "PAX,1,080,Total Children"},
  {pcgLdmTotMal,      "LDM_TOT_MAL",        "PAX,1,090,Total Male Passengers"},
  {pcgLdmTotFem,      "LDM_TOT_FEM",        "PAX,1,100,Total Female Passengers"},
  {pcgLdmTotInf,      "LDM_TOT_INF",        "PAX,1,110,Total Infants"},
  {pcgLdmTotFclPax,   "LDM_TOT_FCL_PAX",    "PAX,2,010,First Class Passengers"},
  {pcgLdmTotFclPad,   "LDM_TOT_FCL_PAD",    "PAX,2,040,First Class PADs"},
  {pcgLdmTotBclPax,   "LDM_TOT_BCL_PAX",    "PAX,2,110,Business Class Passengers"},
  {pcgLdmTotBclPad,   "LDM_TOT_BCL_PAD",    "PAX,2,140,Business Class PADs"},
  {pcgLdmTotEclPax,   "LDM_TOT_ECL_PAX",    "PAX,2,210,Economy Class Passengers"},
  {pcgLdmTotEclPad,   "LDM_TOT_ECL_PAD",    "PAX,2,240,Economy Class PADs"},
  {pcgLdmTotPaxApt,   "LDM_TOT_PAX_APT",    "PAX,3,000,Total Passengers to Airport"},
  {pcgLdmTotPaxAptAdd,"LDM_TOT_PAX_APT_ADD","PAX,3,010,Add Passengers to HOPO from Airport"},
  {pcgLdmTotPadApt,   "LDM_TOT_PAD_APT",    "PAX,3,030,Total PADs to Airport"},
  {pcgLdmTotPadAptAdd,"LDM_TOT_PAD_APT_ADD","PAX,3,035,Add PADs to HOPO from Airport"},
  {pcgLdmTotAduApt,   "LDM_TOT_ADU_APT",    "PAX,3,040,Adult Passengers to Airport"},
  {pcgLdmTotChiApt,   "LDM_TOT_CHI_APT",    "PAX,3,050,Children to Airport"},
  {pcgLdmTotMalApt,   "LDM_TOT_MAL_APT",    "PAX,3,060,Male Passengers to Airport"},
  {pcgLdmTotFemApt,   "LDM_TOT_FEM_APT",    "PAX,3,070,Female Passengers to Airport"},
  {pcgLdmTotInfApt,   "LDM_TOT_INF_APT",    "PAX,3,080,Infants to Airport"},
  {pcgLdmTotInfAptAdd,"LDM_TOT_INF_APT_ADD","PAX,3,085,Add Infants to HOPO from Airport"},
  {pcgLdmTotFclPaxApt,"LDM_TOT_FCL_PAX_APT","PAX,4,000,First Class Passengers to Airport"},
  {pcgLdmTotFclPaxAptAdd,"LDM_TOT_FCL_PAX_APT_ADD","PAX,4,010,Add First Class Passengers to HOPO from Airport"},
  {pcgLdmTotFclPadApt,"LDM_TOT_FCL_PAD_APT","PAX,4,030,First Class PADs to Airport"},
  {pcgLdmTotBclPaxApt,"LDM_TOT_BCL_PAX_APT","PAX,4,040,Business Class Passengers to Airport"},
  {pcgLdmTotBclPaxAptAdd,"LDM_TOT_BCL_PAX_APT_ADD","PAX,4,050,Add Business Class Passengers to HOPO from Airport"},
  {pcgLdmTotBclPadApt,"LDM_TOT_BCL_PAD_APT","PAX,4,070,Business Class PADs to Airport"},
  {pcgLdmTotEclPaxApt,"LDM_TOT_ECL_PAX_APT","PAX,4,080,Economy Class Passengers to Airport"},
  {pcgLdmTotEclPaxAptAdd,"LDM_TOT_ECL_PAX_APT_ADD","PAX,4,090,Add Economy Class Passengers to HOPO from Airport"},
  {pcgLdmTotEclPadApt,"LDM_TOT_ECL_PAD_APT","PAX,4,110,Economy Class PADs to Airport"},
  {pcgLdmTrsPax,      "LDM_TRS_PAX",        "TRS,1,000,Total Transit Passengers"},
  {pcgLdmTrsPad,      "LDM_TRS_PAD",        "TRS,1,010,Total Transit PADs"},
  {pcgLdmTrsAdu,      "LDM_TRS_ADU",        "TRS,1,020,Total Transit Adults"},
  {pcgLdmTrsChi,      "LDM_TRS_CHI",        "TRS,1,030,Total Transit Children"},
  {pcgLdmTrsInf,      "LDM_TRS_INF",        "TRS,1,040,Total Transit Infants"},
  {pcgLdmTrsFclPax,   "LDM_TRS_FCL_PAX",    "TRS,2,000,First Class Transit Passengers"},
  {pcgLdmTrsFclPad,   "LDM_TRS_FCL_PAD",    "TRS,2,010,First Class Transit PADs"},
  {pcgLdmTrsBclPax,   "LDM_TRS_BCL_PAX",    "TRS,2,100,Business Class Transit Passengers"},
  {pcgLdmTrsBclPad,   "LDM_TRS_BCL_PAD",    "TRS,2,110,Business Class Transit PADs"},
  {pcgLdmTrsEclPax,   "LDM_TRS_ECL_PAX",    "TRS,2,200,Economy Class Transit Passengers"},
  {pcgLdmTrsEclPad,   "LDM_TRS_ECL_PAD",    "TRS,2,210,Economy Class Transit PADs"},
  {pcgLdmTrsPaxApt,   "LDM_TRS_PAX_APT",    "TRS,3,000,Total Transit Passengers to Airport"},
  {pcgLdmTrsPadApt,   "LDM_TRS_PAD_APT",    "TRS,3,010,Total Transit PADs to Airport"},
  {pcgLdmTrsAduApt,   "LDM_TRS_ADU_APT",    "TRS,3,020,Total Transit Adults to Airport"},
  {pcgLdmTrsChiApt,   "LDM_TRS_CHI_APT",    "TRS,3,030,Total Transit Children to Airport"},
  {pcgLdmTrsInfApt,   "LDM_TRS_INF_APT",    "TRS,3,040,Total Transit Infants to Airport"},
  {pcgLdmTrsFclPaxApt,"LDM_TRS_FCL_PAX_APT","TRS,4,000,First Class Transit Passengers to Airport"},
  {pcgLdmTrsFclPadApt,"LDM_TRS_FCL_PAD_APT","TRS,4,010,First Class Transit PADs to Airport"},
  {pcgLdmTrsBclPaxApt,"LDM_TRS_BCL_PAX_APT","TRS,4,100,Business Class Transit Passengers to Airport"},
  {pcgLdmTrsBclPadApt,"LDM_TRS_BCL_PAD_APT","TRS,4,110,Business Class Transit PADs to Airport"},
  {pcgLdmTrsEclPaxApt,"LDM_TRS_ECL_PAX_APT","TRS,4,200,Economy Class Transit Passengers to Airport"},
  {pcgLdmTrsEclPadApt,"LDM_TRS_ECL_PAD_APT","TRS,4,210,Economy Class Transit PADs to Airport"},
  {pcgLdmLoaTot,      "LDM_LOA_TOT",        "LOA,1,000,Total Weight"},
  {pcgLdmLoaDld,      "LDM_LOA_DLD",        "LOA,1,010,Total Deadload"},
  {pcgLdmLoaBag,      "LDM_LOA_BAG",        "LOA,2,000,Total Weight of Baggage"},
  {pcgLdmLoaCgo,      "LDM_LOA_CGO",        "LOA,2,010,Total Weight of Cargo"},
  {pcgLdmLoaMal,      "LDM_LOA_MAL",        "LOA,2,020,Total Weight of Mail"},
  {pcgLdmLoaOth,      "LDM_LOA_OTH",        "LOA,2,030,Total Weight of Other"},
  {pcgLdmLoaTra,      "LDM_LOA_TRA",        "LOA,2,040,Total Weight of Transit"},
  {pcgLdmLoaTotApt,   "LDM_LOA_TOT_APT",    "LOA,3,000,Total Weight to Airport"},
  {pcgLdmLoaTotAptAdd,"LDM_LOA_TOT_APT_ADD","LOA,3,005,Add Weight to HOPO from Airport"},
  {pcgLdmLoaDldApt,   "LDM_LOA_DLD_APT",    "LOA,3,010,Total Deadload to Airport"},
  {pcgLdmLoaBagApt,   "LDM_LOA_BAG_APT",    "LOA,4,000,Total Weight of Baggage to Airport"},
  {pcgLdmLoaBagAptAdd,"LDM_LOA_BAG_APT_ADD","LOA,4,005,Add Weight of Baggage to HOPO from Airport"},
  {pcgLdmLoaCgoApt,   "LDM_LOA_CGO_APT",    "LOA,4,010,Total Weight of Cargo to Airport"},
  {pcgLdmLoaCgoAptAdd,"LDM_LOA_CGO_APT_ADD","LOA,4,015,Add Weight of Cargo to HOPO from Airport"},
  {pcgLdmLoaMalApt,   "LDM_LOA_MAL_APT",    "LOA,4,020,Total Weight of Mail to Airport"},
  {pcgLdmLoaMalAptAdd,"LDM_LOA_MAL_APT_ADD","LOA,4,025,Add Weight of Mail to HOPO from Airport"},
  {pcgLdmLoaOthApt,   "LDM_LOA_OTH_APT",    "LOA,4,030,Total Weight of Other to Airport"},
  {pcgLdmLoaTraApt,   "LDM_LOA_TRA_APT",    "LOA,4,040,Total Weight of Transit to Airport"},

  {pcgPtmTrfPax,      "PTM_TRF_PAX",        "TRF,1,000,Total Transfer Passengers"},
  {pcgPtmTrfAdu,      "PTM_TRF_ADU",        "TRF,1,010,Total Transfer Adults"},
  {pcgPtmTrfChi,      "PTM_TRF_CHI",        "TRF,1,020,Total Transfer Children"},
  {pcgPtmTrfInf,      "PTM_TRF_INF",        "TRF,1,030,Total Transfer Infants"},
  {pcgPtmTrfBag,      "PTM_TRF_BAG",        "TRF,1,040,Total Transfer Baggage"},
  {pcgPtmTrfBwt,      "PTM_TRF_BWT",        "TRF,1,050,Total Transfer Baggage Weight"},
  {pcgPtmTrfFclPax,   "PTM_TRF_FCL_PAX",    "TRF,2,000,First Class Transfer Passengers"},
  {pcgPtmTrfFclAdu,   "PTM_TRF_FCL_ADU",    "TRF,2,010,First Class Transfer Adults"},
  {pcgPtmTrfFclChi,   "PTM_TRF_FCL_CHI",    "TRF,2,020,First Class Transfer Children"},
  {pcgPtmTrfFclInf,   "PTM_TRF_FCL_INF",    "TRF,2,030,First Class Transfer Infants"},
  {pcgPtmTrfFclBag,   "PTM_TRF_FCL_BAG",    "TRF,2,040,First Class Transfer Baggage"},
  {pcgPtmTrfFclBwt,   "PTM_TRF_FCL_BWT",    "TRF,2,050,First Class Transfer Baggage Weight"},
  {pcgPtmTrfBclPax,   "PTM_TRF_BCL_PAX",    "TRF,2,100,Business Class Transfer Passengers"},
  {pcgPtmTrfBclAdu,   "PTM_TRF_BCL_ADU",    "TRF,2,110,Business Class Transfer Adults"},
  {pcgPtmTrfBclChi,   "PTM_TRF_BCL_CHI",    "TRF,2,120,Business Class Transfer Children"},
  {pcgPtmTrfBclInf,   "PTM_TRF_BCL_INF",    "TRF,2,130,Business Class Transfer Infants"},
  {pcgPtmTrfBclBag,   "PTM_TRF_BCL_BAG",    "TRF,2,140,Business Class Transfer Baggage"},
  {pcgPtmTrfBclBwt,   "PTM_TRF_BCL_BWT",    "TRF,2,150,Business Class Transfer Baggage Weight"},
  {pcgPtmTrfEclPax,   "PTM_TRF_ECL_PAX",    "TRF,2,300,Economy Class Transfer Passengers"},
  {pcgPtmTrfEclAdu,   "PTM_TRF_ECL_ADU",    "TRF,2,310,Economy Class Transfer Adults"},
  {pcgPtmTrfEclChi,   "PTM_TRF_ECL_CHI",    "TRF,2,320,Economy Class Transfer Children"},
  {pcgPtmTrfEclInf,   "PTM_TRF_ECL_INF",    "TRF,2,330,Economy Class Transfer Infants"},
  {pcgPtmTrfEclBag,   "PTM_TRF_ECL_BAG",    "TRF,2,340,Economy Class Transfer Baggage"},
  {pcgPtmTrfEclBwt,   "PTM_TRF_ECL_BWT",    "TRF,2,350,Economy Class Transfer Baggage Weight"},
  {pcgPtmTrfPclPax,   "PTM_TRF_PCL_PAX",    "TRF,2,200,Premium Class Transfer Passengers"},
  {pcgPtmTrfPclAdu,   "PTM_TRF_PCL_ADU",    "TRF,2,210,Premium Class Transfer Adults"},
  {pcgPtmTrfPclChi,   "PTM_TRF_PCL_CHI",    "TRF,2,220,Premium Class Transfer Children"},
  {pcgPtmTrfPclInf,   "PTM_TRF_PCL_INF",    "TRF,2,230,Premium Class Transfer Infants"},
  {pcgPtmTrfPclBag,   "PTM_TRF_PCL_BAG",    "TRF,2,240,Premium Class Transfer Baggage"},
  {pcgPtmTrfPclBwt,   "PTM_TRF_PCL_BWT",    "TRF,2,250,Premium Class Transfer Baggage Weight"},
  {pcgPtmTrfPaxDtl,   "PTM_TRF_PAX_DTL",    "TRF,3,000,Total Transfer Passenger Details"},
  {pcgPtmTrfAduDtl,   "PTM_TRF_ADU_DTL",    "TRF,3,010,Total Transfer Adult Details"},
  {pcgPtmTrfChiDtl,   "PTM_TRF_CHI_DTL",    "TRF,3,020,Total Transfer Children Details"},
  {pcgPtmTrfInfDtl,   "PTM_TRF_INF_DTL",    "TRF,3,030,Total Transfer Infant Details"},
  {pcgPtmTrfBagDtl,   "PTM_TRF_BAG_DTL",    "TRF,3,040,Total Transfer Baggage Details"},
  {pcgPtmTrfBwtDtl,   "PTM_TRF_BWT_DTL",    "TRF,3,050,Total Transfer Baggage Weight Details"},
  {pcgPtmTrfFclPaxDtl,"PTM_TRF_FCL_PAX_DTL","TRF,4,000,First Class Transfer Passenger Details"},
  {pcgPtmTrfFclAduDtl,"PTM_TRF_FCL_ADU_DTL","TRF,4,010,First Class Transfer Adult Details"},
  {pcgPtmTrfFclChiDtl,"PTM_TRF_FCL_CHI_DTL","TRF,4,020,First Class Transfer Children Details"},
  {pcgPtmTrfFclInfDtl,"PTM_TRF_FCL_INF_DTL","TRF,4,030,First Class Transfer Infant Details"},
  {pcgPtmTrfFclBagDtl,"PTM_TRF_FCL_BAG_DTL","TRF,4,040,First Class Transfer Baggage Details"},
  {pcgPtmTrfFclBwtDtl,"PTM_TRF_FCL_BWT_DTL","TRF,4,050,First Class Transfer Baggage Weight Details"},
  {pcgPtmTrfBclPaxDtl,"PTM_TRF_BCL_PAX_DTL","TRF,4,100,Business Class Transfer Passenger Details"},
  {pcgPtmTrfBclAduDtl,"PTM_TRF_BCL_ADU_DTL","TRF,4,110,Business Class Transfer Adult Details"},
  {pcgPtmTrfBclChiDtl,"PTM_TRF_BCL_CHI_DTL","TRF,4,120,Business Class Transfer Children Details"},
  {pcgPtmTrfBclInfDtl,"PTM_TRF_BCL_INF_DTL","TRF,4,130,Business Class Transfer Infant Details"},
  {pcgPtmTrfBclBagDtl,"PTM_TRF_BCL_BAG_DTL","TRF,4,140,Business Class Transfer Baggage Details"},
  {pcgPtmTrfBclBwtDtl,"PTM_TRF_BCL_BWT_DTL","TRF,4,150,Business Class Transfer Baggage Weight Details"},
  {pcgPtmTrfEclPaxDtl,"PTM_TRF_ECL_PAX_DTL","TRF,4,300,Economy Class Transfer Passenger Details"},
  {pcgPtmTrfEclAduDtl,"PTM_TRF_ECL_ADU_DTL","TRF,4,310,Economy Class Transfer Adult Details"},
  {pcgPtmTrfEclChiDtl,"PTM_TRF_ECL_CHI_DTL","TRF,4,320,Economy Class Transfer Children Details"},
  {pcgPtmTrfEclInfDtl,"PTM_TRF_ECL_INF_DTL","TRF,4,330,Economy Class Transfer Infant Details"},
  {pcgPtmTrfEclBagDtl,"PTM_TRF_ECL_BAG_DTL","TRF,4,340,Economy Class Transfer Baggage Details"},
  {pcgPtmTrfEclBwtDtl,"PTM_TRF_ECL_BWT_DTL","TRF,4,350,Economy Class Transfer Baggage Weight Details"},
  {pcgPtmTrfPclPaxDtl,"PTM_TRF_PCL_PAX_DTL","TRF,4,200,Premium Class Transfer Passenger Details"},
  {pcgPtmTrfPclAduDtl,"PTM_TRF_PCL_ADU_DTL","TRF,4,210,Premium Class Transfer Adult Details"},
  {pcgPtmTrfPclChiDtl,"PTM_TRF_PCL_CHI_DTL","TRF,4,220,Premium Class Transfer Children Details"},
  {pcgPtmTrfPclInfDtl,"PTM_TRF_PCL_INF_DTL","TRF,4,230,Premium Class Transfer Infant Details"},
  {pcgPtmTrfPclBagDtl,"PTM_TRF_PCL_BAG_DTL","TRF,4,240,Premium Class Transfer Baggage Details"},
  {pcgPtmTrfPclBwtDtl,"PTM_TRF_PCL_BWT_DTL","TRF,4,250,Premium Class Transfer Baggage Weight Details"},

  {pcgPscJoiPax,      "PSC_JOI_PAX",        "PSC,1,000,Total Joining Passengers"},
  {pcgPscTotInf,      "PSC_TOT_INF",        "PSC,1,010,Total Joining Infants"},
  {pcgPscFclJoiPax,   "PSC_FCL_JOI_PAX",    "PSC,2,000,Total Joining First Class Passengers"},
  {pcgPscBclJoiPax,   "PSC_BCL_JOI_PAX",    "PSC,2,010,Total Joining Business Class Passengers"},
  {pcgPscEclJoiPax,   "PSC_ECL_JOI_PAX",    "PSC,2,020,Total Joining Economy Class Passengers"},
  {pcgPscJoiTt1,      "PSC_JOI_TT1",        "PSC,1,000,Total Joining TRF/TRS Passengers (> 24h)"},
  {pcgPscFclJoiTt1,   "PSC_FCL_JOI_TT1",    "PSC,2,000,Total Joining First Class TRF/TRS Passengers (> 24h)"},
  {pcgPscBclJoiTt1,   "PSC_BCL_JOI_TT1",    "PSC,2,010,Total Joining Business Class TRF/TRS Passengers (> 24h)"},
  {pcgPscEclJoiTt1,   "PSC_ECL_JOI_TT1",    "PSC,2,020,Total Joining Economy Class TRF/TRS Passengers (> 24h)"},
  {pcgPscJoiTt2,      "PSC_JOI_TT2",        "PSC,1,000,Total Joining TRF/TRS Passengers (< 24h)"},
  {pcgPscFclJoiTt2,   "PSC_FCL_JOI_TT2",    "PSC,2,000,Total Joining First Class TRF/TRS Passengers (< 24h)"},
  {pcgPscBclJoiTt2,   "PSC_BCL_JOI_TT2",    "PSC,2,010,Total Joining Business Class TRF/TRS Passengers (< 24h)"},
  {pcgPscEclJoiTt2,   "PSC_ECL_JOI_TT2",    "PSC,2,020,Total Joining Economy Class TRF/TRS Passengers (< 24h)"},

  {pcgKriPaxFst,      "KRI_PAX_FST",        "PAX,1,000,Flight Status"},
  {pcgKriTotPax,      "KRI_TOT_PAX",        "PAX,1,020,Total Booked Passengers"},
  {pcgKriTotCki,      "KRI_TOT_CKI",        "PAX,1,030,Total Checked-In Passengers (@I-Request)"},
  {pcgKriTotCkg,      "KRI_TOT_CKG",        "PAX,1,031,Total Checked-In Passengers (@G-Request)"},
  {pcgKriTotBoa,      "KRI_TOT_BOA",        "PAX,1,040,Total Passengers already boarded"},
  {pcgKriTotNyb,      "KRI_TOT_NYB",        "PAX,1,050,Total Passengers not yet boarded"},
  {pcgKriPxjPax,      "KRI_PXJ_PAX",        "PAX,1,055,Total Joined Passengers"},
  {pcgKriTotInf,      "KRI_TOT_INF",        "PAX,1,110,Total Infants"},
  {pcgKriTotInfCki,   "KRI_TOT_INF_CKI",    "PAX,1,120,Total Checked-In Infants"},
  {pcgKriTotInfBoa,   "KRI_TOT_INF_BOA",    "PAX,1,130,Total Boarded Infants"},
  {pcgKriTotInfOff,   "KRI_TOT_INF_OFF",    "PAX,1,140,Total Offloaded Infants"},
  {pcgKriPxjInf,      "KRI_PXJ_INF",        "PAX,1,145,Total Joined Infants"},
  {pcgKriCfgFclPax,   "KRI_CFG_FCL_PAX",    "PAX,2,000,Configuration First Class Passengers"},
  {pcgKriBkdFclPax,   "KRI_BKD_FCL_PAX",    "PAX,2,020,Total Booked First Class Passengers"},
  {pcgKriCkiFclPax,   "KRI_CKI_FCL_PAX",    "PAX,2,030,Total Checked-In First Class Passengers"},
  {pcgKriPxjPaxFcl,   "KRI_PXJ_PAX_FCL",    "PAX,2,035,Total Joined First Class Passengers"},
  {pcgKriTotFclInf,   "KRI_TOT_FCL_INF",    "PAX,2,050,Total Infants First Class"},
  {pcgKriCkiFclInf,   "KRI_CKI_FCL_INF",    "PAX,2,060,Total Checked-In Infants First Class"},
  {pcgKriBoaFclInf,   "KRI_BOA_FCL_INF",    "PAX,2,070,Total Boarded Infants First Class"},
  {pcgKriOffFclInf,   "KRI_OFF_FCL_INF",    "PAX,2,080,Total Offloaded Infants First Class"},
  {pcgKriPxjInfFcl,   "KRI_PXJ_INF_FCL",    "PAX,2,085,Total Joined Infants First Class"},
  {pcgKriCfgBclPax,   "KRI_CFG_BCL_PAX",    "PAX,2,100,Configuration Business Class Passengers"},
  {pcgKriBkdBclPax,   "KRI_BKD_BCL_PAX",    "PAX,2,120,Total Booked Business Class Passengers"},
  {pcgKriCkiBclPax,   "KRI_CKI_BCL_PAX",    "PAX,2,130,Total Checked-In Business Class Passengers"},
  {pcgKriPxjPaxBcl,   "KRI_PXJ_PAX_BCL",    "PAX,2,135,Total Joined Business Class Passengers"},
  {pcgKriTotBclInf,   "KRI_TOT_BCL_INF",    "PAX,2,150,Total Infants Business Class"},
  {pcgKriCkiBclInf,   "KRI_CKI_BCL_INF",    "PAX,2,160,Total Checked-In Infants Business Class"},
  {pcgKriBoaBclInf,   "KRI_BOA_BCL_INF",    "PAX,2,170,Total Boarded Infants Business Class"},
  {pcgKriOffBclInf,   "KRI_OFF_BCL_INF",    "PAX,2,180,Total Offloaded Infants Business Class"},
  {pcgKriPxjInfBcl,   "KRI_PXJ_INF_BCL",    "PAX,2,185,Total Joined Infants Business Class"},
  {pcgKriCfgEclPax,   "KRI_CFG_ECL_PAX",    "PAX,2,200,Configuration Economy Class Passengers"},
  {pcgKriBkdEclPax,   "KRI_BKD_ECL_PAX",    "PAX,2,220,Total Booked Economy Class Passengers"},
  {pcgKriCkiEclPax,   "KRI_CKI_ECL_PAX",    "PAX,2,230,Total Checked-In Economy Class Passengers"},
  {pcgKriPxjPaxEcl,   "KRI_PXJ_PAX_ECL",    "PAX,2,235,Total Joined Economy Class Passengers"},
  {pcgKriTotEclInf,   "KRI_TOT_ECL_INF",    "PAX,2,250,Total Infants Economy Class"},
  {pcgKriCkiEclInf,   "KRI_CKI_ECL_INF",    "PAX,2,260,Total Checked-In Infants Economy Class"},
  {pcgKriBoaEclInf,   "KRI_BOA_ECL_INF",    "PAX,2,270,Total Boarded Infants Economy Class"},
  {pcgKriOffEclInf,   "KRI_OFF_ECL_INF",    "PAX,2,280,Total Offloaded Infants Economy Class"},
  {pcgKriPxjInfEcl,   "KRI_PXJ_INF_ECL",    "PAX,2,285,Total Joined Infants Economy Class"},
  {pcgKriTotPaxApt,   "KRI_TOT_PAX_APT",    "PAX,3,010,Total Booked Passengers to Airport"},
  {pcgKriTotCkiApt,   "KRI_TOT_CKI_APT",    "PAX,3,020,Total Checked-In Passengers to Airport"},
  {pcgKriPxjTotApt,   "KRI_PXJ_TOT_APT",    "PAX,3,025,Total Joined Passengers to Airport"},
  {pcgKriPxjAduApt,   "KRI_PXJ_ADU_APT",    "PAX,3,041,Joined Adults to Airport"},
  {pcgKriPxjChiApt,   "KRI_PXJ_CHI_APT",    "PAX,3,051,Joined Children to Airport"},
  {pcgKriPxjMalApt,   "KRI_PXJ_MAL_APT",    "PAX,3,061,Joined Males to Airport"},
  {pcgKriPxjFemApt,   "KRI_PXJ_FEM_APT",    "PAX,3,071,Joined Females to Airport"},
  {pcgKriPxjInfApt,   "KRI_PXJ_INF_APT",    "PAX,3,081,Joined Infants to Airport"},
  {pcgKriBkdFclPaxApt,"KRI_BKD_FCL_PAX_APT","PAX,4,010,Booked First Class Passengers to Airport"},
  {pcgKriCkiFclPaxApt,"KRI_CKI_FCL_PAX_APT","PAX,4,020,Checked-In First Class Passengers to Airport"},
  {pcgKriPxjPaxFclApt,"KRI_PXJ_PAX_FCL_APT","PAX,4,022,Joined First Class Passengers to Airport"},
  {pcgKriPxjMalFclApt,"KRI_PXJ_MAL_FCL_APT","PAX,4,023,Joined First Class Males to Airport"},
  {pcgKriPxjFemFclApt,"KRI_PXJ_FEM_FCL_APT","PAX,4,024,Joined First Class Females to Airport"},
  {pcgKriPxjChiFclApt,"KRI_PXJ_CHI_FCL_APT","PAX,4,025,Joined First Class Children to Airport"},
  {pcgKriPxjInfFclApt,"KRI_PXJ_INF_FCL_APT","PAX,4,026,Joined First Class Infants to Airport"},
  {pcgKriBkdBclPaxApt,"KRI_BKD_BCL_PAX_APT","PAX,4,050,Booked Business Class Passengers to Airport"},
  {pcgKriCkiBclPaxApt,"KRI_CKI_BCL_PAX_APT","PAX,4,060,Checked-In Business Class Passengers to Airport"},
  {pcgKriPxjPaxEclApt,"KRI_PXJ_PAX_ECL_APT","PAX,4,062,Joined Business Class Passengers to Airport"},
  {pcgKriPxjMalEclApt,"KRI_PXJ_MAL_ECL_APT","PAX,4,063,Joined Business Class Males to Airport"},
  {pcgKriPxjFemEclApt,"KRI_PXJ_FEM_ECL_APT","PAX,4,064,Joined Business Class Females to Airport"},
  {pcgKriPxjChiEclApt,"KRI_PXJ_CHI_ECL_APT","PAX,4,065,Joined Business Class Children to Airport"},
  {pcgKriPxjInfEclApt,"KRI_PXJ_INF_ECL_APT","PAX,4,066,Joined Business Class Infants to Airport"},
  {pcgKriBkdEclPaxApt,"KRI_BKD_ECL_PAX_APT","PAX,4,090,Booked Economy Class Passengers to Airport"},
  {pcgKriCkiEclPaxApt,"KRI_CKI_ECL_PAX_APT","PAX,4,100,Checked-In Economy Class Passengers to Airport"},
  {pcgKriPxjPaxBclApt,"KRI_PXJ_PAX_BCL_APT","PAX,4,102,Joined Economy Class Passengers to Airport"},
  {pcgKriPxjMalBclApt,"KRI_PXJ_MAL_BCL_APT","PAX,4,103,Joined Economy Class Males to Airport"},
  {pcgKriPxjFemBclApt,"KRI_PXJ_FEM_BCL_APT","PAX,4,104,Joined Economy Class Females to Airport"},
  {pcgKriPxjChiBclApt,"KRI_PXJ_CHI_BCL_APT","PAX,4,105,Joined Economy Class Children to Airport"},
  {pcgKriPxjInfBclApt,"KRI_PXJ_INF_BCL_APT","PAX,4,106,Joined Economy Class Infants to Airport"},
  {pcgKriTrfPax,      "KRI_TRF_PAX",        "TRF,1,000,Total Transfer Passengers"},
  {pcgKriTrfFclPax,   "KRI_TRF_FCL_PAX",    "TRF,2,000,First Class Transfer Passengers"},
  {pcgKriTrfBclPax,   "KRI_TRF_BCL_PAX",    "TRF,2,100,Business Class Transfer Passengers"},
  {pcgKriTrfEclPax,   "KRI_TRF_ECL_PAX",    "TRF,2,200,Economy Class Transfer Passengers"},
  {pcgKriTrfBag,      "KRI_TRF_BAG",        "TRF,1,000,Total Transfer Bags"},
  {pcgKriTrfFclBag,   "KRI_TRF_FCL_BAG",    "TRF,2,000,First Class Transfer Bags"},
  {pcgKriTrfBclBag,   "KRI_TRF_BCL_BAG",    "TRF,2,100,Business Class Transfer Bags"},
  {pcgKriTrfEclBag,   "KRI_TRF_ECL_BAG",    "TRF,2,200,Economy Class Transfer Bags"},
  {pcgKriTrfBwt,      "KRI_TRF_BWT",        "TRF,1,000,Total Transfer Bags Weight"},
  {pcgKriTrfFclBwt,   "KRI_TRF_FCL_BWT",    "TRF,2,000,First Class Transfer Bags Weight"},
  {pcgKriTrfBclBwt,   "KRI_TRF_BCL_BWT",    "TRF,2,100,Business Class Transfer Bags Weight"},
  {pcgKriTrfEclBwt,   "KRI_TRF_ECL_BWT",    "TRF,2,200,Economy Class Transfer Bags Weight"},
  {pcgKriTrfPaxDtl,   "KRI_TRF_PAX_DTL",    "TRF,3,000,Total Transfer Passenger Details"},
  {pcgKriTrfFclPaxDtl,"KRI_TRF_FCL_PAX_DTL","TRF,4,000,First Class Transfer Passenger Details"},
  {pcgKriTrfBclPaxDtl,"KRI_TRF_BCL_PAX_DTL","TRF,4,100,Business Class Transfer Passenger Details"},
  {pcgKriTrfEclPaxDtl,"KRI_TRF_ECL_PAX_DTL","TRF,4,200,Economy Class Transfer Passenger Details"},
  {pcgKriTrfBagDtl,   "KRI_TRF_BAG_DTL",    "TRF,3,000,Total Transfer Bags Details"},
  {pcgKriTrfFclBagDtl,"KRI_TRF_FCL_BAG_DTL","TRF,4,000,First Class Transfer Bags Details"},
  {pcgKriTrfBclBagDtl,"KRI_TRF_BCL_BAG_DTL","TRF,4,100,Business Class Transfer Bags Details"},
  {pcgKriTrfEclBagDtl,"KRI_TRF_ECL_BAG_DTL","TRF,4,200,Economy Class Transfer Bags Details"},
  {pcgKriTrfBwtDtl,   "KRI_TRF_BWT_DTL",    "TRF,3,000,Total Transfer Bags Weight Details"},
  {pcgKriTrfFclBwtDtl,"KRI_TRF_FCL_BWT_DTL","TRF,4,000,First Class Transfer Bags Weight Details"},
  {pcgKriTrfBclBwtDtl,"KRI_TRF_BCL_BWT_DTL","TRF,4,100,Business Class Transfer Bags Weight Details"},
  {pcgKriTrfEclBwtDtl,"KRI_TRF_ECL_BWT_DTL","TRF,4,200,Economy Class Transfer Bags Weight Details"},
  {pcgKriTrsPax,      "KRI_TRS_PAX",        "TRS,1,000,Total Transit Passengers"},
  {pcgKriTrsFclPax,   "KRI_TRS_FCL_PAX",    "TRS,2,000,First Class Transit Passengers"},
  {pcgKriTrsBclPax,   "KRI_TRS_BCL_PAX",    "TRS,2,100,Business Class Transit Passengers"},
  {pcgKriTrsEclPax,   "KRI_TRS_ECL_PAX",    "TRS,2,200,Economy Class Transit Passengers"},
  {pcgKriLoaTot,      "KRI_LOA_TOT",        "LOA,1,000,Total Weight"},
  {pcgKriLoaUdl,      "KRI_LOA_UDL",        "LOA,1,020,Total Underload"},
  {pcgKriLojTot,      "KRI_LOJ_TOT",        "LOA,1,030,Total Joined Bulk"},
  {pcgKriLoaBag,      "KRI_LOA_BAG",        "LOA,2,000,Total Weight of Baggage"},
  {pcgKriLoaCgo,      "KRI_LOA_CGO",        "LOA,2,010,Total Weight of Cargo"},
  {pcgKriLoaMal,      "KRI_LOA_MAL",        "LOA,2,020,Total Weight of Mail"},
  {pcgKriLojFcl,      "KRI_LOJ_FCL",        "LOA,2,030,Total Joined Bulk First Class"},
  {pcgKriLojBcl,      "KRI_LOJ_BCL",        "LOA,2,040,Total Joined Bulk Business Class"},
  {pcgKriLojEcl,      "KRI_LOJ_ECL",        "LOA,2,050,Total Joined Bulk Economy Class"},
  {pcgKriLoaTotApt,   "KRI_LOA_TOT_APT",    "LOA,3,000,Total Weight to Airport"},
  {pcgKriLojTotApt,   "KRI_LOJ_TOT_APT",    "LOA,3,020,Total Joined Bulk to Airport"},
  {pcgKriLoaBagApt,   "KRI_LOA_BAG_APT",    "LOA,4,000,Total Weight of Baggage to Airport"},
  {pcgKriLoaCgoApt,   "KRI_LOA_CGO_APT",    "LOA,4,010,Total Weight of Cargo to Airport"},
  {pcgKriLoaMalApt,   "KRI_LOA_MAL_APT",    "LOA,4,020,Total Weight of Mail to Airport"},
  {pcgKriLojFclApt,   "KRI_LOJ_FCL_APT",    "LOA,4,030,Joined Bulk First Class to Airport"},
  {pcgKriLojBclApt,   "KRI_LOJ_BCL_APT",    "LOA,4,040,Joined Bulk Business Class to Airport"},
  {pcgKriLojEclApt,   "KRI_LOJ_ECL_APT",    "LOA,4,050,Joined Bulk Economy Class to Airport"},
  {pcgKriUldTot,      "KRI_ULD_TOT",        "ULD,1,000,Total Weight of ULDs"},
  {pcgKriUldTotApt,   "KRI_ULD_TOT_APT",    "ULD,3,000,Weight of ULDs to Airport"},
  {pcgKriUldTotTyp,   "KRI_ULD_TOT_TYP",    "ULD,3,010,Weight of ULDs with Contents"},
  {pcgKriUldTotDtl,   "KRI_ULD_TOT_DTL",    "ULD,4,000,ULD Details"},

  {NULL,"",""}
};

#endif

