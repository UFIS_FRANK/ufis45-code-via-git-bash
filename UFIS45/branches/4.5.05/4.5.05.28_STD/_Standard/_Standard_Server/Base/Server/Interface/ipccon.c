#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/ipccon.c 1.3 2009/11/30 14:40:12SGT bst Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netinet/in.h>

#include <malloc.h>
#include <errno.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include "tcpudp.h"

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

#define TCP_SHUTDOWN    0x14046701
#define TCP_DATA        0x14046702
#define TCP_KEEPALIVE   0x14046703
#define TCP_ACKNOWLEDGE 0x14046704

typedef struct {
        long command;
        long length;
}TCP_INFOHEADER;
#define TCP_INFOHEADER_SIZE sizeof(TCP_INFOHEADER)

static TCP_INFOHEADER InfoHeader;

static int igAcceptSocket = 0;


int debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int igStartUpMode = TRACE;
static int igRuntimeMode = 0;
static int igSendQueue = 0;
static char cgSendCmd[16] = "CMD";

static int igRoleIsSender = FALSE;
static int igRoleIsReader = FALSE;

/* Variables of the TCP/IP bridge */
static char cgHostName[64] = "green";
static char cgServiceName[64] = "UFIS_WMQ";
static char cgClientName[64] = "UNDEF_CLIENT";

static char cgDefMsgiCmd[8] = "MSGI";
static char cgDefMsgiSel[8] = "";
static char cgDefMsgiFld[8] = "";
static char cgDefMsgiDat[8] = "";
static char cgDefMsgiApp[8] = "";

static struct servent *prgServiceEntry = NULL;
static struct hostent *prgHostEntry = NULL;
static struct in_addr *prgInAddr = NULL;

static int igTcpFd = -1;
static int igListenFd = -1;
static int igClientFd = -1;
static int igAttempts = 0;
static int igMaxAttempt = 5;
static struct sockaddr_in rgSin;
static struct sockaddr_in rgCSin;
static struct linger rgLinger;
static char pcgClientIP[64];

static int igSocket = -1;
static char *pcgTcpMsgBuf = NULL;
static int igTcpBufLen = 0;
static int igTcpMsgLen = 0;
static int igTcpMsgHdrLen = 0;
static int igTcpReadLenType = 0;
static int igTcpSendLenType = 0;

static int igTcpWaitLogin = TRUE;
static int igSendStatusTo = 0;

static int igTcpReadMsgAck = TRUE;
static int igTcpReadChkMsg = TRUE; /* still unused */
static int igTcpReadUseTgr = TRUE;

static int igTcpSendAllCmd = TRUE;
static int igTcpSendMsgAck = TRUE;
static int igTcpSendChkMsg = TRUE;
static int igTcpSendUseTgs = FALSE; /* still unused */

static pid_t ptgChildPid = 0;
static int igRemoteClient = FALSE;
static int igClientIsActive = FALSE;
static int igClientWasActive = FALSE;
static int igWaitingForClient = FALSE;
static int igListenIsActive = FALSE;
static int igIpcQueueCheck = FALSE;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static char pcgTwEnd[64] = "";    /* BC-Head used as Stamp */

static int igGotAlarm = FALSE;
static int igMainWaitLoop = 30;
static int igConnectTimeout = 10;
static int igReadTimeout = 10;
static int igCheckTime = 20;
static time_t tgLastCheck = 0;

static long lgEvtCnt = 0;

static igExpectedSignal = 0;

static int igCurMsgSeqNo = -1;
static int igNxtMsgSeqNo = -1;
static int igMaxMsgSeqNo = 999999;
static int igCurTistCnt = 0;
static char cgCurTgrSeqn[16] = "";
static char cgPrvTimeStamp[16] = "0";
static char cgCurTimeStamp[16] = "";
static char cgCurMsgMkey[32] = "";
static char cgMaxMsgMkey[32] = "";


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);

static int ReadConfigRow(char *pcpSection,char *pcpKeyword,
			 char *pcpCfgBuffer);
static int GetDebugLevel(char *pcpMode, int *pipMode);
static int ConnectToClient(void);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int	HandleData(EVENT *prpEvent);       /* Handles event data     */

static int HandleMsgFromChild(void);

static int GetTcpMsg(char **pcpCmd, char **pcpSel, char **pcpFld, char **pcpDat, char **pcpApp);
static int PutTcpMsg(char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpData, char *pcpApp);

static int MyTcpSend(int ipSocket, char *pcpBuf, int ipBufLen);
static int MyTcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen);
static int MyTcpCloseSocket(int ipSocket);
static int MyTcpRecv(int ipSocket, char **pppBuf, int *pipBufLen);
static int MyTcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen);

static void KillRemoteChild(char *pcpChildName);

static int GetCurrentMsgSequence(char *pcpTable);
static int CheckMessageRecovery(char *pcpTable);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRc = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int i = 0;
    int ilGoWaitLoop = FALSE;
    int ilGoCheckQue = FALSE;
    int ilSendAnswer = FALSE;
    int ilOldDebugLevel = 0;
    char clAlert[200] = "\0";
    char clTmp[128] = "TEST";

    INITIALIZE;			/* General initialization	*/

  /* all Signals */
  (void)SetSignals(HandleSignal);
  errno = 0;
  i = sigignore(SIGCHLD);
  dbg(TRACE,"MAIN: sigingore(SIGCHLD) returns <%d>; ERRNO=<%s>",i,strerror(errno));


    debug_level = TRACE;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
	ilRc = init_que();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
	ilRc = init_db();
	if (ilRc != RC_SUCCESS)
	{
	    check_ret(ilRc);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
	sleep(60);
	exit(2);
    }else{
	dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"BinaryFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
	dbg(DEBUG,"MAIN: waiting for status switch ...");
	HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
	dbg(TRACE,"MAIN: initializing ...");
	ilRc = Init_Process();
    } 
    else 
    {
	Terminate(30);
    }/* end of if */

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");

    igClientIsActive = FALSE;
    ilGoWaitLoop = FALSE;

    for(;;)
    {
        ilRc = RC_SUCCESS;
        if ((igClientIsActive == FALSE) || (igWaitingForClient == TRUE))
        {
          ilRc = ConnectToClient();
          if (ilRc == RC_SUCCESS)
          {
            igClientIsActive = TRUE;
            igClientWasActive = TRUE;
            ilGoWaitLoop = FALSE;
          }
          else if (ilRc == RC_TIMEOUT)
          {
            igClientIsActive = FALSE;
            ilGoWaitLoop = FALSE;
          }
          else
          {
            igClientIsActive = FALSE;
            ilGoWaitLoop = TRUE;
          }
        }
        if ((ilGoWaitLoop == TRUE) || (ilRc == RC_TIMEOUT))
        {
          if (ilGoWaitLoop == TRUE)
          {
            /* Problem could not be solved.  */
            /* Waiting for Shutdown by Admin */
            dbg(TRACE,"PLEASE CHECK MY CONFIGURATION");
            dbg(TRACE,"-----------------------------");
          }
          /* Running in a loop trying to restart a new child */
          dbg(TRACE,"------> READING MY CEDA IPC QUEUE (%d) ---",mod_id);
	  ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        }
        else if (igRoleIsReader == TRUE)
        {
          dbg(DEBUG,"===>> MY ROLE IS READER");
          if (ilGoCheckQue != TRUE)
          {
            /* Send pending ACK to wmqcon child */
            if (ilSendAnswer == TRUE)
            {
              if (igTcpSendMsgAck == TRUE)
              {
                PutTcpMsg("CHKC","MSG_ACK", "", "", "");
              }
              ilSendAnswer = FALSE;
            }
            /* Wait here until we come back from the socket */
            ilRc = HandleMsgFromChild();
            if (ilRc == RC_SUCCESS)
            {
              igClientIsActive = TRUE;
              igClientWasActive = TRUE;
              ilGoWaitLoop = FALSE;
              ilSendAnswer = TRUE;
            }
            else if (ilRc == RC_TIMEOUT)
            {
              igClientIsActive = TRUE;
              igClientWasActive = TRUE;
              ilGoWaitLoop = FALSE;
              ilSendAnswer = FALSE;
            }
            else if (ilRc == RC_RESET)
            {
              igClientIsActive = FALSE;
              ilGoWaitLoop = FALSE;
              ilSendAnswer = FALSE;
            }
            else
            {
              igClientIsActive = FALSE;
              ilGoWaitLoop = TRUE;
              ilSendAnswer = FALSE;
            }
          }
          dbg(TRACE,"--- CHECKING MY CEDA IPC QUEUE (%d) ---",mod_id);
          if (igClientWasActive == TRUE)
          {
            ilGoCheckQue = FALSE;
	    ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
          }
          else
          {
            ilGoCheckQue = TRUE;
	    ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
          }
        }
        else if (igRoleIsSender == TRUE)
        {
          dbg(DEBUG,"===>> MY ROLE IS SENDER");
          /* We are waiting on IPC queue for the next message */
          /* The transmission to Wmq is performed in HandleData */
          
          dbg(TRACE,"--- WAITING ON CEDA IPC QUEUE (%d) ---",mod_id);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        }

	prgEvent = (EVENT *) prgItem->text;
			
	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
			
	    lgEvtCnt++;
            ilGoCheckQue = TRUE;

	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    break;	
		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    /* CloseConnection(); */
		    HandleQueues();
		    break;	
		case	HSB_DOWN	:
		    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(1);
		    break;	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    break;	
		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    /* process shutdown - maybe from uutil */
		    Terminate(1);
		    break;
					
		case	RESET		:
		    ilRc = Reset();
		    break;
					
		case	EVENT_DATA	:
		    if((ctrl_sta == HSB_STANDALONE) || 
		       (ctrl_sta == HSB_ACTIVE) || 
		       (ctrl_sta == HSB_ACT_TO_SBY))
		    {
      			if (igRoleIsSender == TRUE)
      			{
			  ilRc = HandleData(prgEvent);
                          if (ilRc == RC_SUCCESS)
                          {
                            igClientIsActive = TRUE;
                            igClientWasActive = TRUE;
                            ilGoWaitLoop = FALSE;
                          }
                          else
                          {
                            igClientIsActive = FALSE;
                            ilGoWaitLoop = TRUE;
                          }
			}
			else
			{
			  dbg(TRACE,"UNEXPECTED DATA EVENT RECEIVED");
			  DebugPrintItem(TRACE,prgItem);
			  DebugPrintEvent(TRACE,prgEvent);
			}
		    }else{
			dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
			DebugPrintItem(TRACE,prgItem);
			DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		    break;
				
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	777 :
		    break;
		case	778 :
		    break;
		case	779 :
		    break;
		case	780 :
		    break;
		case	781 :
		    break;
		default			:
		    dbg(TRACE,"MAIN: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */
	} 
	else if (ilRc == QUE_E_NOMSG)
	{
	    if(time(NULL) > (tgLastCheck + igCheckTime))
	    {
		char clCmd[1024];
		char *pclData = NULL;
		/*
		PutTcpMsg("CHK","xx", " ", " ", "");
		GetCommandFromSocket(clCmd,&pclData);
		ilRc = snap(pclData, 20, outp);
		if (strncmp(clCmd,"COC",3) != 0)
		{
		    dbg(TRACE,"invalid reply to CHK command <%s> %p",clCmd,pclData);

		    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
		    ilRc = AddAlert2("KRISCOM"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		    Terminate(30);
		}
		*/
	    }
	    else
	    {
		nap(500);
	    }
	}
	else 
        {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
	} /* end else */

        if (ilGoWaitLoop == TRUE)
        {
          dbg(TRACE,"SOMETHING WENT WRONG: SLEEP %d",igMainWaitLoop);
          sleep(igMainWaitLoop);
        }

    } /* end of for ever */

  dbg(TRACE,"OOPS. SHOULD NEVER COME HERE ...");
  exit(0);
	
} /* end of MAIN */


/* ******************************************************************* */
static int PutTcpMsg(char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpData, char *pcpApp)
{
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  char pclTmp[32] = "";
  /* NOTE: PARAMETER pcpApp NOT YET USED */


  switch (igTcpSendLenType)
  {
    case 0:
        /* Default method: Integer variable as header */
    break;

    case 1:
        /* Bridge method: #LEN# keyword header */
        sprintf(pcgTcpMsgBuf,"{#LEN#}0000000000{/#LEN#}\n"
                         "{#CMD#}%s{/#CMD#}\n{#SEL#}%s{/#SEL#}\n"
                         "{#FLD#}%s{/#FLD#}\n{#DAT#}%s{/#DAT#}",
                         pcpCommand,pcpSelection,pcpFields,pcpData);
        igTcpMsgLen = strlen(pcgTcpMsgBuf) + 1;
        sprintf(pclTmp,"{#LEN#}%0.10d{/#LEN#}",igTcpMsgLen);
        ilLen = strlen(pclTmp);
        strncpy(pcgTcpMsgBuf,pclTmp,ilLen);
    break;

    case 3:
        /* xx (10) byte header */
        igTcpMsgLen = strlen(pcpData) + 1;
        sprintf(pcgTcpMsgBuf,"%0.*d%s", igTcpMsgHdrLen, igTcpMsgLen, pcpData);
    break;

    default :
        dbg(TRACE,"NO LENGTH METHOD DEFINED FOR TCP/IP BRIDGE");
        ilRc = RC_FAIL;
    break;

  } /* end of switch */

  if(ilRc == RC_SUCCESS)
  {
    
    igTcpMsgLen = strlen(pcgTcpMsgBuf) + 1;
    dbg(TRACE,"SENDING MESSAGE (LEN=%d):\n<%s>",igTcpMsgLen,pcgTcpMsgBuf);
    ilRc = MyTcpSend(igSocket,pcgTcpMsgBuf,igTcpMsgLen);
    if (ilRc == igTcpMsgLen)
    {
      dbg(TRACE,"MSG <%s> SENT TO TCPCON LEN=%d",pcpCommand,ilRc);
      ilRc = RC_SUCCESS;
    }
    else
    {
      dbg(TRACE,"MSG TO TCPCON FAILED (RC=%d)",ilRc);
      ilRc = RC_FAIL;
    }
  }

  return ilRc;
}

/* ===================================================================== */
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword, CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
        strcpy(pcpCfgBuffer,pcpDefault);
        dbg(TRACE,"DEFAULT VALUE [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    }
    else
    {
        dbg(TRACE,"CONFIGURATION [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    }/* end of if */
    return ilRC;
}



static int ReadConfigRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,
			 CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int  ilRc = RC_SUCCESS;			/* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    char clTmpString[164] = "\0";
    int ilOldDebugLevel = 0;
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSqlBuf[2560] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clAlert[200] = "\0";
    char clTemp[100] = "\0";
    char clHeaderType[100] = "\0";
    char clUseTcpAck[100] = "\0";

    ilRc = RC_SUCCESS;
    igRoleIsSender = FALSE;
    igRoleIsReader = FALSE;

    GetDebugLevel("STARTUP_MODE", &igStartUpMode);
    GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    debug_level = igStartUpMode;

    if(ilRc == RC_SUCCESS)
    {
	/* read HomeAirPort from SGS.TAB */
	ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
            /*
	    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
	    ilRc = AddAlert2("KRISCOM"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            */
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
	}
    }
	
    if(ilRc == RC_SUCCESS)
    {

	ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
            /*
	    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
	    ilRc = AddAlert2("KRISCOM"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            */
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
	}
    }

    if(ilRc == RC_SUCCESS)
    {
      sprintf(pcgTwEnd,"%s,%s,IPCCON",cgHopo,cgTabEnd);
    }

    if (ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"===> CONFIG OF IPC BRIDGE CONNECTOR");
        ilRc = ReadConfigEntry("MAIN","ROLE",clTemp,"UNDEF");
        if ((strcmp(clTemp,"SEND") == 0) || (strcmp(clTemp,"SENDER") == 0))
        {
            igRoleIsSender = TRUE;
        }
        else if ((strcmp(clTemp,"READ") == 0) || (strcmp(clTemp,"READER") == 0) ||
                 (strcmp(clTemp,"RECV") == 0) || (strcmp(clTemp,"RECEIVER") == 0))
        {
            igRoleIsReader = TRUE;
        }
        else
        {
          dbg(TRACE,"ROLE UNDEFINED");
          dbg(TRACE,"--------------");
        }
        (void) ReadConfigEntry("MAIN","TCP_HEADER_TYPE",clHeaderType,"TCP_INFOHEADER");
        if (strcmp(clHeaderType,"TCP_INFOHEADER") == 0)
        {
          igTcpReadLenType = 0;
          igTcpSendLenType = 0;
        }
        else if (strcmp(clHeaderType,"UFIS_BRIDGE") == 0)
        {
          igTcpReadLenType = 1;
          igTcpSendLenType = 1;
        }
        else if (strcmp(clHeaderType,"GOCC_BRIDGE") == 0)
        {
          igTcpReadLenType = 2;
          igTcpSendLenType = 2;
        }
        else if (strstr(clHeaderType,"BYTE_HEAD") != NULL)
        {
          igTcpReadLenType = 3;
          igTcpSendLenType = 3;
          igTcpMsgHdrLen = atoi(clHeaderType);
          dbg(TRACE,"TCP HEADER LENGTH = %d",igTcpMsgHdrLen);
        }
        else
        {
          dbg(TRACE,"UNKNOWN TCP_HEADER_TYPE. USING DEFAULT (TCP_INFOHEADER)");
        }
        (void) ReadConfigEntry("MAIN","CLIENT_LOGIN",clTemp,"YES");
        if (strcmp(clTemp,"YES") == 0)
        {
          igTcpWaitLogin = TRUE;
        }
        else
        {
          igTcpWaitLogin = FALSE;
        }
        (void) ReadConfigEntry("MAIN","TCP_MSG_ACK",clUseTcpAck,"YES");
        if (strcmp(clUseTcpAck,"YES") == 0)
        {
          igTcpReadMsgAck = TRUE;
          igTcpSendMsgAck = TRUE;
        }
        else
        {
          igTcpReadMsgAck = FALSE;
          igTcpSendMsgAck = FALSE;
        }
        (void) ReadConfigEntry("MAIN","MAIN_WAITLOOP",clTemp,"30");
        igMainWaitLoop = atoi(clTemp);
        (void) ReadConfigEntry("MAIN","SEND_STATUS",clTemp,"0");
        igSendStatusTo = atoi(clTemp);
    }

    if(ilRc == RC_SUCCESS)
    {
        if (igRoleIsReader == TRUE)
        {
          dbg(TRACE,"===> CONFIG OF READER");
	  (void) ReadConfigEntry("MAIN","CHILD_NAME",cgClientName,"wmqconr");
	  (void) ReadConfigEntry("MAIN","SEND_CMD",cgSendCmd,"MSGI");
	  (void) ReadConfigEntry("MAIN","SENDTOQUEUE",clTmpString,"-1");
	  igSendQueue = atoi(clTmpString);
          (void) ReadConfigEntry("MAIN","MSG_SPOOLER",clTemp,"YES");
          if (strcmp(clTemp,"YES") == 0)
          {
            igTcpReadUseTgr = TRUE;
          }
          else
          {
            igTcpReadUseTgr = FALSE;
          }
        }

        if (igRoleIsSender == TRUE)
        {
          dbg(TRACE,"===> CONFIG OF SENDER");
	  (void) ReadConfigEntry("MAIN","CHILD_NAME",cgClientName,"wmqcons");
	  (void) ReadConfigEntry("MAIN","SEND_CMD",cgSendCmd,"MSGO");
          (void) ReadConfigEntry("MAIN","SEND_ALL",clTemp,"YES");
          if (strcmp(clTemp,"YES") == 0)
          {
            igTcpSendAllCmd = TRUE;
          }
          else
          {
            igTcpSendAllCmd = FALSE;
          }
          (void) ReadConfigEntry("MAIN","SEND_CHK_MSG",clTemp,"YES");
          if (strcmp(clTemp,"YES") == 0)
          {
            igTcpSendChkMsg = TRUE;
          }
          else
          {
            igTcpSendChkMsg = FALSE;
          }
        }

	dbg(TRACE, "===> CONFIG OF TCP/IP NETWORK");
	(void) ReadConfigEntry("NETWORK","HOSTNAME",cgHostName,"UNDEFINED");
	(void) ReadConfigEntry("NETWORK","SERVICE",cgServiceName,"UNDEFINED");
	(void) ReadConfigEntry("NETWORK","CONNECT_TIMEOUT",clTmpString,"10");
	igConnectTimeout = atoi(clTmpString);
	(void) ReadConfigEntry("NETWORK","READ_TIMEOUT",clTmpString,"10");
	igReadTimeout = atoi(clTmpString);
	
    }

    if (strstr(cgClientName,"MANUALLY") != NULL)
    {
      igRemoteClient = TRUE;
    }
    else if (strstr(cgClientName,"REMOTE") != NULL)
    {
      igRemoteClient = TRUE;
    }

    igTcpBufLen = 1024 * 1024 * 4;
    pcgTcpMsgBuf = (char *)malloc(igTcpBufLen);

    debug_level = TRACE;
    ilRc = RC_SUCCESS;
    return(ilRc);
	
} /* end of Init_Process */


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int		ilRc = RC_SUCCESS;
    char	clCfgValue[64];

    ilRc = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
			 clCfgValue);

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
	dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
	if (!strcmp(clCfgValue, "DEBUG"))
	    *pipMode = DEBUG;
	else if (!strcmp(clCfgValue, "TRACE"))
	    *pipMode = TRACE;
	else
	    *pipMode = 0;
    }
    return ilRc;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRc = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    if (igSocket >= 0)
    {
	PutTcpMsg("EXIT","", "", "", "");
    }
    /* signal(SIGCHLD,SIG_IGN); */

    logoff();

    dbg(TRACE,"Terminate: now sleep 2 seconds ...");
    sleep(2);
	
    dbg(TRACE,"Terminate: now leaving ... ");
    fclose(outp);

    exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{

    switch(pipSig)
    {
        case SIGTERM:
            dbg(TRACE,"GOT SIGNAL <SIGTERM> (%d). NOW TERMINATING ...",pipSig);
	    Terminate(1);
            break;
        case SIGCLD:
            if (igExpectedSignal == SIGCLD)
            {
              dbg(DEBUG,"GOT EXPECTED SIGNAL <SIGCLD> (%d)",pipSig);
            }
            else
            {
              dbg(TRACE,"GOT UNEXPECTED SIGNAL <SIGCLD> (%d)",pipSig);
            }
	    break;
        case SIGPIPE:
            dbg(TRACE,"GOT SIGNAL <SIGPIPE> (%d) (BROKEN LINK)",pipSig);
	    break;
        case SIGALRM:
            igGotAlarm = TRUE;
            if (igExpectedSignal == SIGALRM)
            {
              dbg(DEBUG,"GOT EXPECTED SIGNAL <SIGALRM> (%d)",pipSig);
            }
            else
            {
              dbg(TRACE,"GOT UNEXPECTED SIGNAL <SIGALRM> (%d)",pipSig);
            }
	    break;
	default	:
            dbg(TRACE,"GOT UNEXPECTED SIGNAL (%d). NOW TERMINATING ...",pipSig);
	    Terminate(1);
	    break;
    } /* end of switch */

    return;
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    dbg(TRACE,"<%d> : no messages on queue",pipErr);
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int	ilBreakOut = FALSE;
	
    do
    {
	ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;	

	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
		
	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    ilBreakOut = TRUE;
		    break;	

		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_DOWN	:
		    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(1);
		    break;	
	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    ilBreakOut = TRUE;
		    break;	

		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;

		case	SHUTDOWN	:
		    Terminate(1);
		    break;
						
		case	RESET		:
		    ilRc = Reset();
		    break;
						
		case	EVENT_DATA	:
		    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;

		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;

		default			:
		    dbg(TRACE,"HandleQueues: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
	} /* end else */
    } while (ilBreakOut == FALSE);

    ilRc = Init_Process();
     
    return;

} /* end of HandleQueues */

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int	ilRc = RC_SUCCESS;
    int ilSendIt = TRUE;
    int que_out = 0;
    int ilSendAnswer = FALSE;
    int ilSendFields = FALSE;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char *pclCmd = NULL;
    char *pclSel = NULL;
    char *pclFld = NULL;
    char *pclDat = NULL;
    char *pclApp = NULL;
    char 	clCurCmd[32];
    char 	clTable[32];

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;

    que_out = prgEvent->originator;

    strcpy(clTable,prlCmdblk->obj_name);
    strcpy(clCurCmd,prlCmdblk->command);

    dbg(DEBUG,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    dbg(TRACE,"Originator<%d>",prpEvent->originator);
    dbg(TRACE,"Command   <%s>",prlCmdblk->command);
    if (*prlCmdblk->obj_name != '\0')
        dbg(TRACE,"Object    <%s>",prlCmdblk->obj_name);
    if (*prlCmdblk->tw_start != '\0')
        dbg(TRACE,"Tw Start  <%s>",prlCmdblk->tw_start);
    if (*prlCmdblk->tw_end != '\0')
        dbg(TRACE,"Tw End    <%s>",prlCmdblk->tw_end);
    if (*pclSelection != '\0')
        dbg(TRACE,"Selection <%s>",pclSelection);
    if (*pclFields != '\0')
        dbg(TRACE,"Fields    <%s>",pclFields);
    if (*pclData != '\0')
        dbg(TRACE,"Data      :\n%s",pclData);
    /****************************************/

  ilSendIt = igTcpSendAllCmd;
  ilSendFields = FALSE;
  if (ilSendIt != TRUE)
  {
    if (strcmp(clCurCmd,"XMLO") == 0)
    {
      ilSendIt = TRUE;
      ilSendFields = FALSE;
    }
    else if (strcmp(clCurCmd,"MSGO") == 0)
    {
      ilSendIt = TRUE;
      ilSendFields = FALSE;
    }
    else if (strcmp(clCurCmd,"TLX") == 0)
    {
      ilSendIt = TRUE;
      ilSendFields = FALSE;
    }
    else if (strcmp(clCurCmd,cgSendCmd) == 0)
    {
      ilSendIt = TRUE;
      ilSendFields = FALSE;
    }
    else if (strcmp(clCurCmd,"CHKC") == 0)
    {
      if (igTcpSendChkMsg == TRUE)
      {
        ilSendIt = TRUE;
        ilSendFields = FALSE;
      }
    }
    else
    {
      dbg(TRACE,"CEDA COMMAND NOT FORWARDED TO TCP/IP BRIDGE");
    }
  }

  if (strcmp(clCurCmd,"REFR") == 0)
  {
    /* Refresh Command For External System */
    ilSendIt = TRUE;
    ilSendAnswer = TRUE;
    ilSendFields = TRUE;
  }
  else if (strcmp(clCurCmd,"RCMD") == 0)
  {
    /* Future Use: Interface Control Functions */
    ilSendAnswer = TRUE;
    ilSendFields = TRUE;
  }
  else if (strcmp(clCurCmd,"USR") == 0)
  {
    /* Future Use: Status Requests */
    ilSendAnswer = TRUE;
    ilSendFields = TRUE;
  }

  if ((igClientIsActive == FALSE) || (igWaitingForClient == TRUE))
  {
    dbg(TRACE,"THERE IS NO CLIENT CONNECTED TO THE BRIDGE");
    ilSendIt = FALSE;
  }

  if (ilSendIt == TRUE) 
  {
    if (ilSendFields == TRUE)
    {
      ilRc = PutTcpMsg(prlCmdblk->command,prlCmdblk->tw_start,pclFields,pclData, ""); 
    }
    else
    {
      ilRc = PutTcpMsg(prlCmdblk->command,prlCmdblk->tw_start,prlCmdblk->tw_end,pclData, ""); 
    }
    dbg(TRACE,"MESSAGE PUT ONTO TCP/IP BRIDGE");
    if ((ilRc == RC_SUCCESS) && (igTcpReadMsgAck == TRUE))
    {
      dbg(TRACE,"WAITING FOR ACK FROM TCP/IP BRIDGE");
      ilRc = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat, &pclApp);
    }
  }
  else
  {
    dbg(TRACE,"COMMAND NOT FORWARDED TO TCP/IP BRIDGE");
  }

  if (ilSendAnswer == TRUE)
  {
    if ((igClientIsActive == FALSE) || (igWaitingForClient == TRUE))
    {
      ilRc = tools_send_info_flag (que_out, 0, "DEST", "", "RECV", "", "", 
             "TWS", "TWE", "ERR", "TBL", "SEL", "FLD", "ERR: THE INTERFACE IS DOWN.", 0);
    }
    else
    {
      ilRc = tools_send_info_flag (que_out, 0, "DEST", "", "RECV", "", "", 
             "TWS", "TWE", "OK", "TBL", "SEL", "FLD", "OK: REQUEST SUCCESSFULLY TRANSMITTED", 0);
    }
    dbg(TRACE,"TRIGGER SENT BACK TO CLIENT");
  }

  dbg(DEBUG,"==========  END  <%10.10d> ==========",lgEvtCnt);

  return ilRc;
	
} /* end of HandleData */


/* *********************************************************************** */
/* *********************************************************************** */
static int ConnectToClient(void)
{
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  int ilOn = 1;
  int ilPort = 0;
  int ilLoop = TRUE;
  int ilBufLen = 0;
  long llTmpLen = 0;
  char pclCmdStrg[256];
  char pclTmp[128];
  char *pclCmd = NULL;
  char *pclSel = NULL;
  char *pclFld = NULL;
  char *pclDat = NULL;
  char *pclApp = NULL;

  if(ilRc == RC_SUCCESS)
  {
    if (igRemoteClient != TRUE)
    {
      dbg(TRACE,"SENDING SIGTERM TO PERHAPS STILL RUNNING CHILD");
      KillRemoteChild(cgClientName);
    }

    if (igWaitingForClient != TRUE)
    {
      dbg(TRACE,"STEP 1: ESTABLISH LISTEN SOCKET");
      if (igClientFd >= 0)
      {
        dbg(TRACE,"CLOSING SOCKET %d (CLIENT)",igClientFd);
        close(igClientFd);
        igClientFd = -1;
      }
      if (igListenFd >= 0)
      {
        dbg(TRACE,"CLOSING SOCKET %d (LISTEN)",igListenFd);
        close(igListenFd);
        igListenFd = -1;
      }
      if (igTcpFd >= 0)
      {
        dbg(TRACE,"CLOSING SOCKET %d (TCP)",igTcpFd);
        close(igTcpFd);
        igTcpFd = -1;
      }
      igSocket = -1;
      igClientIsActive = FALSE;
      igWaitingForClient = FALSE;
      igListenIsActive = FALSE;

      if(ilRc == RC_SUCCESS)
      {
        dbg(TRACE,"===>> LOOK UP SERVICE <%s>", cgServiceName);
        errno = 0;
        if (!(prgServiceEntry = getservbyname(cgServiceName,NULL)))
        {
          dbg(TRACE,"ERROR: getservbyname(%s) <%s>",cgServiceName,strerror(errno));
          ilRc = RC_FAIL;
        }     
        else
        {
          dbg(TRACE,"GOT SERVICE: NAME <%s>",prgServiceEntry->s_name);
          dbg(TRACE,"GOT SERVICE: PORT <%d>",ntohs(prgServiceEntry->s_port));
          dbg(TRACE,"GOT SERVICE: TYPE <%s>",prgServiceEntry->s_proto);
        }
      }

      if(ilRc == RC_SUCCESS)
      {
        dbg(TRACE,"===>> LOOK UP HOST <%s>", cgHostName);
        errno = 0;
        if (!(prgHostEntry = gethostbyname(cgHostName)))
        {
          dbg(TRACE,"ERROR: gethostbyname(%s) errno %d <%s>",cgHostName,errno,strerror(errno));
          ilRc = RC_FAIL;
        }   
        else 
        {
          dbg(TRACE,"GOT HOST: NAME <%s>",prgHostEntry->h_name);
          prgInAddr = (struct in_addr *) &(*prgHostEntry->h_addr);
          dbg(TRACE,"GOT HOST: ADDR <%s>",inet_ntoa(*prgInAddr));
        }
      }

      if(ilRc == RC_SUCCESS)
      {
        igTcpFd = socket( AF_INET, SOCK_STREAM, PF_UNSPEC );
        if( igTcpFd < 0 )
        {
          dbg(TRACE,"Unable to create socket! ERROR!");
          ilRc = RC_FAIL;
        }
      }

      if (ilRc == RC_SUCCESS)
      {
        ilPort = ntohs(prgServiceEntry->s_port);
        rgSin.sin_family = AF_INET;
        rgSin.sin_port = htons(ilPort);
        rgSin.sin_addr.s_addr = htonl(INADDR_ANY);
        rgLinger.l_onoff = 0;

        igAttempts = 0;

        setsockopt( igTcpFd, SOL_SOCKET, SO_LINGER, (char *)&rgLinger, sizeof(rgLinger) );
        setsockopt( igTcpFd, SOL_SOCKET, SO_REUSEADDR, (char *)&ilOn, sizeof(int) );
        setsockopt( igTcpFd, SOL_SOCKET, SO_KEEPALIVE, (char *)&ilOn, sizeof(int) );

        while( bind( igTcpFd, (struct sockaddr *)&rgSin, sizeof(rgSin) ) < 0 &&
               igAttempts < igMaxAttempt )
        {
          igAttempts++;
          dbg(TRACE,"Error (%s)in binding!! Number of attempt = %d", strerror(errno), igAttempts );
          sleep( 1 );
        }

        if( igAttempts >= igMaxAttempt )
        {
          dbg(TRACE,"Still cannot bind! EXIT!!" );
          ilRc = RC_FAIL;
        }
        else
        {
          dbg(TRACE,"Port %d is binded successfully", ilPort);
        }
      }

      if(ilRc == RC_SUCCESS)
      {
        igAttempts = 0;
        while(((igListenFd = listen(igTcpFd, 5)) == -1) && (igAttempts < igMaxAttempt))
        {
          igAttempts++;
          dbg(TRACE,"Error (%s) in listening!! Number of attempt = %d", strerror(errno), igAttempts );
          sleep( 1 );
        }

        if( igAttempts >= igMaxAttempt )
        {
          dbg(TRACE,"Still cannot listen! EXIT!!");
          ilRc = RC_FAIL;
        }
        else
        {
          dbg(TRACE,"listen successfully");
          igListenIsActive = TRUE;
        }
      }
    }
  }

  if(ilRc == RC_SUCCESS)
  {
    if (igWaitingForClient != TRUE)
    {
      dbg(TRACE,"STEP 2: STARTING OUTSIDE CONNECTOR");
      if (strstr(cgClientName,"MANUALLY") != NULL)
      {
        dbg(TRACE,"------> TO BE STARTED MANUALLY");
        igRemoteClient = TRUE;
        ilRc = RC_SUCCESS;
      }
      else if (strstr(cgClientName,"REMOTE") != NULL)
      {
        dbg(TRACE,"------> CONNECTING FROM REMOTE");
        igRemoteClient = TRUE;
        ilRc = RC_SUCCESS;
      }
      else
      {
        ilRc = RC_FAIL;
        igRemoteClient = FALSE;
        if (igRoleIsReader == TRUE)
        {
          sprintf(pclCmdStrg,"cp %s/%s %s/%s",getenv("BIN_PATH"),cgClientName,getenv("RUN_PATH"),cgClientName);
          dbg(TRACE,"SYSTEM CALL <%s>",pclCmdStrg);
          igExpectedSignal = SIGCLD;
          system(pclCmdStrg);
          igExpectedSignal = -1;
          sprintf(pclCmdStrg,"nohup %s/%s port &",getenv("RUN_PATH"),cgClientName);
          dbg(TRACE,"SYSTEM CALL <%s>",pclCmdStrg);
          igExpectedSignal = SIGCLD;
          system(pclCmdStrg);
          igExpectedSignal = -1;
          ilRc = RC_SUCCESS;
        }
        if (igRoleIsSender == TRUE)
        {
          sprintf(pclCmdStrg,"cp %s/%s %s/%s",getenv("BIN_PATH"),cgClientName,getenv("RUN_PATH"),cgClientName);
          dbg(TRACE,"SYSTEM CALL <%s>",pclCmdStrg);
          igExpectedSignal = SIGCLD;
          system(pclCmdStrg);
          igExpectedSignal = -1;
          sprintf(pclCmdStrg,"nohup %s/%s port &",getenv("RUN_PATH"),cgClientName);
          dbg(TRACE,"SYSTEM CALL <%s>",pclCmdStrg);
          igExpectedSignal = SIGCLD;
          system(pclCmdStrg);
          igExpectedSignal = -1;
          ilRc = RC_SUCCESS;
        }
      }
    }
  }

  if(ilRc == RC_SUCCESS)
  {
    if (igWaitingForClient != TRUE)
    {
      dbg(TRACE,"STEP 3: WAITING FOR CLIENT TO CONNECT");
    }
    else
    {
      dbg(TRACE,"------> WAITING FOR CLIENT TO CONNECT");
    }
    igWaitingForClient = TRUE;
    igAttempts = 0;
    if (igRemoteClient == TRUE)
    {
      igMaxAttempt = 1;
    }
    memset( &rgCSin, 0, sizeof(rgCSin) );
    ilLen = sizeof(struct sockaddr_in);
    igGotAlarm = FALSE;
    igExpectedSignal = SIGALRM;
    alarm(igConnectTimeout);
    while((igAttempts < igMaxAttempt) && ((igClientFd = accept(igTcpFd, (struct sockaddr *)&rgCSin, &ilLen)) < 0))
    {
      alarm(0);
      igExpectedSignal = -1;
      igAttempts++;
      if (igGotAlarm == TRUE)
      {
        if (igAttempts < igMaxAttempt)
        {
          dbg(TRACE,"------> WAITED FOR CLIENT TO CONNECT (LOOP %d)",igAttempts);
        }
      }
      else
      {
        dbg(TRACE,"Error (%s) in accepting connection!! Number of attempt = %d", strerror(errno), igAttempts );
      }
      igGotAlarm = FALSE;
      if (igAttempts < igMaxAttempt)
      {
        igExpectedSignal = SIGALRM;
        alarm(igConnectTimeout);
      }
    }
    alarm(0);
    igExpectedSignal = -1;
  
    if (igClientFd < 0)
    {
      if (igRemoteClient != TRUE)
      {
        dbg(TRACE,"STILL NO CONNECTION REQUEST! EXIT!!" );
        igWaitingForClient = FALSE;
        ilRc = RC_FAIL;
      }
      else
      {
        dbg(DEBUG,"------> GOING TO CHECK MY IPC QUEUE NOW" );
        igWaitingForClient = TRUE;
        ilRc = RC_TIMEOUT;
      }
    }
    else
    {
      igWaitingForClient = FALSE;
      igSocket = igClientFd;
      dbg(TRACE,"Connection is accepted successfully");
      if( (char *)inet_ntop( AF_INET, &rgCSin.sin_addr.s_addr, pcgClientIP, sizeof(pcgClientIP) ) == NULL )
      {
        dbg(TRACE,"Cant get client IP");
      }
      else
      {
        dbg(TRACE,"Client IP <%s>", pcgClientIP);
      }
    }
  }

  if(ilRc == RC_SUCCESS)
  {
    igSocket = igClientFd;
    dbg(TRACE,"CONNECTION ESTABLISHED ON SOCKET %d",igSocket);
    if (igTcpWaitLogin == TRUE)
    {
      dbg(TRACE,"WAITING FOR READY MESSAGE FROM CLIENT");
      while ((ilRc == RC_SUCCESS) && (ilLoop == TRUE))
      {
        ilRc = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat, &pclApp);
        if (ilRc == RC_SUCCESS)
        {
          if (strcmp(pclCmd,"INIT") == 0)
          {
            if (strstr(pclDat,"PID=") != NULL)
            {
              (void) CedaGetKeyItem(pclTmp, &llTmpLen, pclDat, "PID=", ",", TRUE);
              ptgChildPid = atoi(pclTmp);
              dbg(TRACE,"REMOTE CHILD PID = %d",ptgChildPid);
            }
            if (strstr(pclDat,"READY") != NULL)
            {
              dbg(TRACE,"BRIDGE CONNECTION READY FOR USE");
              ilLoop = FALSE;
              ilRc = RC_SUCCESS;
            }
            if (strstr(pclDat,"TERM") != NULL)
            {
              dbg(TRACE,"BRIDGE CONNECTION CLOSED BY CLIENT");
              ilLoop = FALSE;
              ilRc = RC_FAIL;
            }
          }
        }
        else if (ilRc == RC_TIMEOUT)
        {
          dbg(TRACE,"STILL NO READY MESSAGE FROM CLIENT");
          ilRc = RC_SUCCESS;
        }
        else
        {
          dbg(TRACE,"CONNECTION FAILED (SOCKET CLOSED BY CLIENT)");
          ilRc = RC_FAIL;
        }
      }
    }
  }

  if ((ilRc != RC_SUCCESS) && (ilRc != RC_TIMEOUT))
  {
    dbg(TRACE,"COULD NOT ESTABLISH SERVICE <%s> WITH WMQ CONNECTOR",cgServiceName);
    dbg(TRACE,"===============================================================");
    /*
    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
    ilRc = AddAlert2("KRISCOM"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
    Terminate(30);
    */
    if ((ptgChildPid > 0) && (igRemoteClient != TRUE))
    {
      dbg(TRACE,"SENDING SIGTERM TO CHILD PID %d",ptgChildPid);
      kill(ptgChildPid,SIGTERM);
      ptgChildPid = 0;
    }

    if (igClientFd >= 0)
    {
      dbg(TRACE,"CLOSING SOCKET %d (CLIENT)",igClientFd);
      close(igClientFd);
      igClientFd = -1;
    }
    if (igListenFd >= 0)
    {
      dbg(TRACE,"CLOSING SOCKET %d (LISTEN)",igListenFd);
      close(igListenFd);
      igListenFd = -1;
    }
    if (igTcpFd >= 0)
    {
      dbg(TRACE,"CLOSING SOCKET %d (TCP)",igTcpFd);
      close(igTcpFd);
      igTcpFd = -1;
    }
    igWaitingForClient = FALSE;
    igSocket = -1;
    ilRc = RC_FAIL;
  }

  igCurMsgSeqNo = -1;

  return(ilRc);
}

/* ********************************************************************* */
/* ********************************************************************* */
static int HandleMsgFromChild(void)
{
  int ilRc = RC_SUCCESS;
  int ilLoop = TRUE;
  int ilMinTistCnt = 0;
  int ilCurTistCnt = 0;
  int ilMaxTistCnt = 0;
  int ilMinPackCnt = 0;
  int ilCurPackCnt = 0;
  int ilMaxPackCnt = 0;
  short slCursor = 0;
  short slFkt = 0;
  char clMyTime[16] = "";
  char clSqlBuf[256] = "";
  char clData[32] = "";
  char clTmpData[32] = "";
  char pclTmpMsgKey[32] = "MSG_KEY";
  char *pclCmd = NULL;
  char *pclSel = NULL;
  char *pclFld = NULL;
  char *pclDat = NULL;
  char *pclApp = NULL;

  if ((igTcpReadUseTgr == TRUE) && (igCurMsgSeqNo < 0))
  {
    ilRc = CheckMessageRecovery("TGRTAB");
  }

  while ((ilRc == RC_SUCCESS) && (ilLoop == TRUE))
  {
    ilRc = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat, &pclApp);
    if (ilRc == RC_SUCCESS)
    {
      if (strcmp(pclCmd,"MSGI") == 0)
      {
        if (igTcpReadUseTgr == TRUE)
        {
          igCurMsgSeqNo = atoi(pclFld);
        }
        else
        {
          igCurMsgSeqNo = igNxtMsgSeqNo;
        }
        if (igCurMsgSeqNo != igNxtMsgSeqNo)
        {
          dbg(TRACE,"=== MESSAGE SYNCHRONIZATION ERROR ===");
          dbg(TRACE,"UNEXPECTED SEQUENCE %d RECEIVED (EXPECTED = %d)",igCurMsgSeqNo,igNxtMsgSeqNo);
          ilRc = CheckMessageRecovery("TGRTAB");
          if (igTcpSendMsgAck == TRUE)
          {
            PutTcpMsg("MSG_ACK","SEND_OK", "", "", "");
          }
        }
        if (igCurMsgSeqNo == igNxtMsgSeqNo)
        {
          if (pclSel[0] == '\0')
          {
            /* We didn't get a message key from the external connector */
            /* So we create one (without using TGRTAB) and forward it  */
            GetServerTimeStamp("UTC",1,0,cgCurTimeStamp);
            if (strcmp(cgCurTimeStamp,cgPrvTimeStamp) != 0)
            {
              igCurTistCnt = 0;
            }
            else
            {
              igCurTistCnt++;
            }
            strcpy(cgPrvTimeStamp,cgCurTimeStamp);
            sprintf(pclTmpMsgKey,"%s%0.3d",cgCurTimeStamp,igCurTistCnt);
            pclSel = pclTmpMsgKey;
          }
          ilRc = tools_send_info_flag (igSendQueue, 0, "DEST", "", "RECV", "", "", pclSel, pcgTwEnd, 
                                     cgSendCmd, "TBL", "SEL", "FLD", pclDat, 0);
          dbg(TRACE,"CMD <%s> SENT TO %d (RC=%d)",cgSendCmd,igSendQueue,ilRc);
          if (igTcpReadUseTgr == TRUE)
          {
            (void)get_real_item (pclTmpMsgKey, pclSel, 1);
            (void)get_real_item (clTmpData, pclSel, 2);
            ilMinTistCnt = atoi(clTmpData);
            (void)get_real_item (clTmpData, pclSel, 3);
            ilMaxTistCnt = atoi(clTmpData);
            if (ilRc == RC_SUCCESS)
            {
              GetServerTimeStamp("LOC",1,0,clMyTime);
              ilCurPackCnt = 1;
              ilMaxPackCnt = ilMaxTistCnt - ilMinTistCnt + 1;
              for (ilCurTistCnt = ilMinTistCnt; ilCurTistCnt <= ilMaxTistCnt; ilCurTistCnt++)
              {
                sprintf(cgCurMsgMkey,"%s%0.3d",pclTmpMsgKey,ilCurTistCnt);
                if (ilMaxPackCnt == 1)
                {
                  sprintf(clSqlBuf,"UPDATE TGRTAB SET STAT='SEND_OK',OUTT='%s' WHERE MKEY='%s'",clMyTime,cgCurMsgMkey);
                }
                else
                {
                  sprintf(clSqlBuf,"UPDATE TGRTAB SET STAT='OK_%d/%d',OUTT='%s' WHERE MKEY='%s'",
                                      ilCurPackCnt,ilMaxPackCnt,clMyTime,cgCurMsgMkey);
                  ilCurPackCnt++;
                }
                dbg(TRACE,"<%s>",clSqlBuf);
                slFkt = START;
                slCursor = 0;
                clData[0] = 0x00;
                ilRc = sql_if (slFkt, &slCursor, clSqlBuf, clData);
                if (ilRc != RC_SUCCESS)
                {
                  dbg(TRACE,"sql_if failed###################");
                }
                commit_work();
                close_my_cursor (&slCursor);
              }
              if (igTcpSendMsgAck == TRUE)
              {
                PutTcpMsg("MSG_ACK","SEND_OK", "", "", "");
              }
            }
            else
            {
              /*
              sprintf(clSqlBuf,"UPDATE TGRTAB SET STAT='SEND_FAIL' WHERE MKEY='%s'",pclSel);
              dbg(TRACE,"<%s>",clSqlBuf);
              slFkt = START;
              slCursor = 0;
              ilRc = sql_if (slFkt, &slCursor, clSqlBuf, clData);
              if (ilRc != RC_SUCCESS)
              {
                dbg(TRACE,"sql_if failed###################");
              }
              commit_work();
              close_my_cursor (&slCursor);
              */
              dbg(TRACE,"PUT IPC MESSAGE FAILED. SO TERMINATE ...");
              if (igTcpSendMsgAck == TRUE)
              {
                PutTcpMsg("MSG_ACK","SEND_FAIL", "", "", "");
              }
              Terminate(2);
            }
          }
          else
          {
            if (igTcpSendMsgAck == TRUE)
            {
              PutTcpMsg("MSG_ACK","SEND_OK", "", "", "");
            }
          }
          igNxtMsgSeqNo++;
          if (igNxtMsgSeqNo > igMaxMsgSeqNo)
          {
            igNxtMsgSeqNo = 1;
          }
        }
      }
      else if (strcmp(pclCmd,"CHKC") == 0)
      {
        dbg(TRACE,"GOT <CHECK CONNECTION> FROM CLIENT");
        ilLoop = FALSE;
        ilRc = RC_SUCCESS;
      }
      else if (strcmp(pclCmd,"EXIT") == 0)
      {
        dbg(TRACE,"GOT <EXIT> FROM CLIENT");
        ilLoop = FALSE;
        ilRc = RC_FAIL;
        if (strcmp(pclSel,"RESTART") == 0)
        {
          dbg(TRACE,"CLIENT REQUESTED RESTART OF THE BRIDGE");
          PutTcpMsg("EXIT","", "", "", "");
          sleep(2);
          ilRc = RC_RESET;
        }
      }
      else
      {
        dbg(TRACE,"GOT UNKNOWN CMD <%s>",pclCmd);
      }
    }
    else if (ilRc == RC_TIMEOUT)
    {
      ilLoop = FALSE;
    }
  }
  return ilRc;
}


/* ===================================================================== */
static int GetTcpMsg(char **pcpCmd, char **pcpSel, char **pcpFld, char **pcpDat, char **pcpApp)
{
  int ilRc = RC_SUCCESS;
  int ilCnt = 0;
  long llCmdSize = 0;
  long llSelSize = 0;
  long llFldSize = 0;
  long llDatSize = 0;
  long llAppSize = 0;
  char pclDummy[4] = "";
  dbg(TRACE,"--- WAITING ON TCP/IP BRIDGE <%s> (SOCK=%d) ---",cgServiceName,igSocket);
  if (pcgTcpMsgBuf != NULL)
  {
    pcgTcpMsgBuf[0] = 0x00;
  }
  ilRc = MyTcpRecv(igSocket,&pcgTcpMsgBuf,&igTcpBufLen);
  dbg(DEBUG,"GOT MSG LEN(RC)=%d FROM TCP",ilRc);

  ilCnt = 0;
  if (ilRc > 0)
  {
    ilRc = RC_SUCCESS;
    dbg(DEBUG,"GOT MSG <%s>",pcgTcpMsgBuf);
    if (igTcpReadLenType < 2)
    {
      /* Default format of UFIS Bridge Message */
      *pcpCmd = CedaGetKeyItem(pclDummy, &llCmdSize, pcgTcpMsgBuf, "{#CMD#}", "{/#CMD#}", FALSE);
      *pcpSel = CedaGetKeyItem(pclDummy, &llSelSize, pcgTcpMsgBuf, "{#SEL#}", "{/#SEL#}", FALSE);
      *pcpFld = CedaGetKeyItem(pclDummy, &llFldSize, pcgTcpMsgBuf, "{#FLD#}", "{/#FLD#}", FALSE);
      *pcpDat = CedaGetKeyItem(pclDummy, &llDatSize, pcgTcpMsgBuf, "{#DAT#}", "{/#DAT#}", FALSE);
      *pcpApp = CedaGetKeyItem(pclDummy, &llAppSize, pcgTcpMsgBuf, "{#APP#}", "{/#APP#}", FALSE);
      if (*pcpCmd != NULL)
      {
        (*pcpCmd)[llCmdSize] = 0x00;
        dbg(TRACE,"GOT CMD <%s>",*pcpCmd);
        ilCnt++;
      }
      if (*pcpSel != NULL)
      {
        (*pcpSel)[llSelSize] = 0x00;
        if (llSelSize > 0)
        {
          dbg(TRACE,"GOT SEL <%s>",*pcpSel);
        }
        ilCnt++;
      }
      if (*pcpFld != NULL)
      {
        (*pcpFld)[llFldSize] = 0x00;
        if (llFldSize > 0)
        {
          dbg(TRACE,"GOT FLD <%s>",*pcpFld);
        }
        ilCnt++;
      }
      if (*pcpDat != NULL)
      {
        (*pcpDat)[llDatSize] = 0x00;
        if (llDatSize > 0)
        {
          dbg(TRACE,"GOT DAT\n%s\n(LEN=%d)",*pcpDat,llDatSize);
        }
        ilCnt++;
      }
      if (*pcpApp != NULL)
      {
        (*pcpApp)[llAppSize] = 0x00;
        if (llAppSize > 0)
        {
          dbg(TRACE,"GOT APP\n%s\n(LEN=%d)",*pcpApp,llAppSize);
        }
        ilCnt++;
      }

      if (ilCnt < 4)
      {
        dbg(TRACE,"GOT INCOMPLETE MESSAGE");
        ilRc = RC_FAIL;
      }
    }
    else
    {
      /* Format currently used at GOCC project */
      /* Only a XML Message Buffer, no parameters */
      /* We use defaults for the other parameters */
      *pcpCmd = cgDefMsgiCmd;
      *pcpSel = cgDefMsgiSel;
      *pcpFld = cgDefMsgiFld;
      *pcpDat = pcgTcpMsgBuf;
      *pcpApp = cgDefMsgiApp;
    }
  }
  else
  {
    if (ilRc != RC_TIMEOUT)
    {
      ilRc = RC_FAIL;
    }
  }

  return ilRc;
}

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpSend(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilSendLen = 0;


        /***** Parameter check *****/

        if(ipSocket < 1)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 1st Parameter");
        }/* end of if */

        if(pcpBuf == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 2nd Parameter");
        }/* end of if */

        if(ipBufLen < 1)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 3rd Parameter");
        }/* end of if */

        /***** functionality *****/

  switch (igTcpSendLenType)
  {
    case 0:
        /* Default method: Integer variable as header */
        if(ilRC == RC_SUCCESS)
        {
                InfoHeader.command = htonl(TCP_DATA);
                InfoHeader.length  = htonl(ipBufLen);

                ilSendLen = MyTcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));
                if(ilSendLen != sizeof(TCP_INFOHEADER))
                {
                        ilRC = RC_FAIL;
                }
        }/* end of if */

        if(ilRC == RC_SUCCESS)
        {
                ilRC = MyTcpSendBuf(ipSocket,pcpBuf,ipBufLen);
        }/* end of if */
    break;

    case 1:
        /* Bridge method: #LEN# keyword header */
        if(ilRC == RC_SUCCESS)
        {
          ilRC = MyTcpSendBuf(ipSocket,pcpBuf,ipBufLen);
        }/* end of if */
    break;

    case 3:
        /* 10 byte header */
        if(ilRC == RC_SUCCESS)
        {
          ilRC = MyTcpSendBuf(ipSocket,pcpBuf,ipBufLen);
        }/* end of if */
    break;

    default :
        dbg(TRACE,"NO LENGTH METHOD DEFINED FOR TCP/IP BRIDGE");
        ilRC = RC_FAIL;
    break;

  } /* end of switch */

  return(ilRC);

}/* end of MyTcpSend */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilSendLen = 0;
        int ilRemainder = 0;
        int ilFlag = 0;
        char *pclBufOfs = NULL;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        pclBufOfs   = pcpBuf;
        ilRemainder = ipBufLen;

        while(ilRemainder != 0)
        {
                errno = 0;
                ilSendLen = write(ipSocket,pclBufOfs,ilRemainder);
                /* ilSendLen = send(ipSocket,pclBufOfs,ilRemainder,ilFlag); */
                if(ilSendLen > 0)
                {
                        if(ilSendLen == ilRemainder)
                        {
                                ilRC         = ipBufLen;
                                ilRemainder  = 0;
                        }else{
                                ilRemainder -= ilSendLen;
                                pclBufOfs   += ilSendLen;
                        }/* end of if */
                }else{
                        switch(errno)
                        {
                        case EINTR :
                                dbg(DEBUG,"MyTcpSendBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilSendLen);
                                break;

                        case 0 :
                                dbg(DEBUG,"MyTcpSendBuf: errno <%d> ilRecvLen <%d>",errno,ilSendLen);
                                MyTcpCloseSocket(ipSocket);
                                ilRC = RC_FAIL;
                                ilRemainder = 0;
                                break;

                        default :
                                dbg(TRACE,"MyTcpSendBuf: <%d> bytes <%s>",ilSendLen,strerror(errno));
                                MyTcpCloseSocket(ipSocket);
                                ilRC = RC_FAIL;
                                ilRemainder = 0;
                                break;
                        }/* end of switch */
                }/* end of if */
        }/* end of while */

        return(ilRC);

}/* end of MyTcpSendBuf */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpCloseSocket(int ipSocket)
{
        int ilRC = RC_SUCCESS;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                errno = 0;
                close(ipSocket);
                switch(errno)
                {
                case 0 :
                        break;
                default :
                        /*dbg(TRACE,"MyTcpCloseSocket: close <%s>",strerror(errno));*/
                        break;
                }/* end of switch */

                errno = 0;
                shutdown(ipSocket,2);
                switch(errno)
                {
                case 0 :
                        break;
                default :
                        /*dbg(TRACE,"MyTcpCloseSocket: shutdown <%s>",strerror(errno));*/
                        break;
                }/* end of switch */

        }/* end of if */

        return(ilRC);

}/* end of MyTcpCloseSocket */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpRecv(int ipSocket, char **pppBuf, int *pipBufLen)
{
  int ilRC = RC_SUCCESS;
  int ilLoop = FALSE;
  int ilRecvLen = 0;
  int ilKeyLen = 0;
  int ilMsgLen = 0;
  long llTmpLen = 0;
  char pclTmpBuf[1024] = "";
  char pclTmpKey[1024] = "";
  char *pclTmpPtr = NULL;


        /***** Parameter check *****/

        if(ipSocket < 0)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 1st Parameter");
        }/* end of if */

        if(pppBuf == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 2nd Parameter");
        }/* end of if */

        if(pipBufLen == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 3rd Parameter");
        }/* end of if */


        /***** functionality *****/

  switch (igTcpReadLenType)
  {
    case 0:
        /* Default method: Integer variable as header */
        if(ilRC == RC_SUCCESS)
        {
                do
                {
                        ilLoop = FALSE;
                        ilRecvLen = MyTcpRecvBuf(ipSocket,(char *) &InfoHeader, TCP_INFOHEADER_SIZE);
                        if(ilRecvLen == RC_TIMEOUT)
                        {
                          ilRC = RC_TIMEOUT;
                        }
                        else
                        {
                          if(ilRecvLen != TCP_INFOHEADER_SIZE)
                          {
                                if(ilRecvLen != RC_RESET)
                                {
                                        dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,TCP_INFOHEADER_SIZE);
                                        ilRC = RC_FAIL;
                                }
                                else
                                {
                                        ilRC = ilRecvLen;
                                        ilLoop = FALSE;
                                }/* end of if */
                          }
                          else
                          {
                                if(ntohl(InfoHeader.command) == TCP_KEEPALIVE)
                                {
                                        dbg(DEBUG,"MyTcpRecv: TCP_KEEPALIVE received");
                                        ilLoop = TRUE;
                                }/* end of if */
                          }/* end of if */
                        }
                }while((ilLoop == TRUE) && (ilRC == RC_SUCCESS));
        }/* end of if */
        if(ilRC == RC_SUCCESS)
        {
                switch(ntohl(InfoHeader.command))
                {
                case TCP_DATA :
                        ilRecvLen = ntohl(InfoHeader.length);
			ilRecvLen++;
                        if(ilRecvLen > *pipBufLen)
                        {
                                errno = 0;
                                *pppBuf = realloc(*pppBuf,ilRecvLen);
                                if(*pppBuf == NULL)
                                {
                                        ilRC = RC_FAIL;
                                        dbg(TRACE,"MyTcpRecv: realloc(%d) failed <%s>",ilRecvLen,strerror(errno));
                                }else{
                                        ilRC = RC_SUCCESS;
                                        dbg(DEBUG,"MyTcpRecv: new buffer size <%d>",ilRecvLen);
                                }/* end of if */
                        }/* end of if */
                        if(ilRC == RC_SUCCESS)
                        {
                                memset(*pppBuf,0x00,ilRecvLen);
				ilRecvLen--;
                                *pipBufLen = ilRecvLen;
                                ilRC = MyTcpRecvBuf(ipSocket,*pppBuf,ilRecvLen);
				if (ilRC >= 0)
				{
                			*pppBuf[ilRC] = '\0';
				}
                        }/* end of if */
                        break;
                case TCP_SHUTDOWN :
                        MyTcpCloseSocket(ipSocket);
                        ilRC = RC_SHUTDOWN;
                        dbg(DEBUG,"MyTcpRecv: TCP_SHUTDOWN received");
                        break;
                default :
                        ilRC = RC_FAIL;
                        dbg(TRACE,"MyTcpRecv: unknown command");
                        break;
                }/* end of switch */
        }/* end of if */
    break;

    case 1:
        /* Bridge method: #LEN# keyword header */
        ilKeyLen = 25;
        if(ilRC == RC_SUCCESS)
        {
          ilRecvLen = MyTcpRecvBuf(ipSocket, pclTmpBuf, ilKeyLen);
          if(ilRecvLen == RC_TIMEOUT)
          {
            ilRC = RC_TIMEOUT;
          }
          else
          {
            if(ilRecvLen != ilKeyLen)
            {
              if(ilRecvLen != RC_RESET)
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_FAIL;
              }
              else
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_RESET;
              }
            }
          }
        }
        if(ilRC == RC_SUCCESS)
        {
          dbg(TRACE,"TCP HEADER: <%s>",pclTmpBuf);
          pclTmpPtr = CedaGetKeyItem(pclTmpKey, &llTmpLen, pclTmpBuf, "{#LEN#}", "{/#LEN#}", TRUE);
          if (llTmpLen > 0)
          {
            ilMsgLen = atoi(pclTmpKey);
            ilMsgLen++;
            if(ilMsgLen > *pipBufLen)
            {
              errno = 0;
              *pppBuf = realloc(*pppBuf,ilMsgLen);
              if(*pppBuf == NULL)
              {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: realloc(%d) failed <%s>",ilMsgLen,strerror(errno));
              }
              else
              {
                ilRC = RC_SUCCESS;
                dbg(DEBUG,"MyTcpRecv: new buffer size <%d>",ilMsgLen);
              }
            }
            if(ilRC == RC_SUCCESS)
            {
              memset(*pppBuf,0x00,ilMsgLen);
              ilMsgLen--;
              *pipBufLen = ilMsgLen;
              strcpy(*pppBuf,pclTmpBuf);
              pclTmpPtr = *pppBuf;
              pclTmpPtr += ilRecvLen;
              ilRecvLen = ilMsgLen - ilRecvLen;
              ilRC = MyTcpRecvBuf(ipSocket,pclTmpPtr,ilRecvLen);
              if (ilRC >= 0)
              {
                pclTmpPtr[ilRC] = '\0';
                ilRC += 25; /* UFIS #LEN# Header */
              }
            }
          }
        }
    break;

    case 2:
        /* XML tag method: <LEN> keyword header */
        ilKeyLen = 32;
        if(ilRC == RC_SUCCESS)
        {
          ilRecvLen = MyTcpRecvBuf(ipSocket, pclTmpBuf, ilKeyLen);
          if(ilRecvLen == RC_TIMEOUT)
          {
            ilRC = RC_TIMEOUT;
          }
          else
          {
            if(ilRecvLen != ilKeyLen)
            {
              if(ilRecvLen != RC_RESET)
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_FAIL;
              }
              else
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_RESET;
              }
            }
          }
        }
        if(ilRC == RC_SUCCESS)
        {
          dbg(TRACE,"TCP HEADER AREA: <%s>",pclTmpBuf);
          pclTmpPtr = CedaGetKeyItem(pclTmpKey, &llTmpLen, pclTmpBuf, "<LEN>", "</LEN>", TRUE);
          if (llTmpLen > 0)
          {
            ilMsgLen = atoi(pclTmpKey);
            ilMsgLen++;
            if(ilMsgLen > *pipBufLen)
            {
              errno = 0;
              *pppBuf = realloc(*pppBuf,ilMsgLen);
              if(*pppBuf == NULL)
              {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: realloc(%d) failed <%s>",ilMsgLen,strerror(errno));
              }
              else
              {
                ilRC = RC_SUCCESS;
                dbg(DEBUG,"MyTcpRecv: new buffer size <%d>",ilMsgLen);
              }
            }
            if(ilRC == RC_SUCCESS)
            {
              memset(*pppBuf,0x00,ilMsgLen);
              ilMsgLen--;
              *pipBufLen = ilMsgLen;
              strcpy(*pppBuf,pclTmpBuf);
              pclTmpPtr = *pppBuf;
              pclTmpPtr += ilRecvLen;
              ilRecvLen = ilMsgLen - ilRecvLen;
              ilRC = MyTcpRecvBuf(ipSocket,pclTmpPtr,ilRecvLen);
              if (ilRC >= 0)
              {
                pclTmpPtr[ilRC] = '\0';
                ilRC += 32; /* XML <LEN> Header */
              }
            }
          }
        }
    break;

    case 3:
        /*  xx (ten) bytes ascii header */
        ilKeyLen = igTcpMsgHdrLen;
        if(ilRC == RC_SUCCESS)
        {
          ilRecvLen = MyTcpRecvBuf(ipSocket, pclTmpBuf, ilKeyLen);
          if(ilRecvLen == RC_TIMEOUT)
          {
            ilRC = RC_TIMEOUT;
          }
          else
          {
            if(ilRecvLen != ilKeyLen)
            {
              if(ilRecvLen != RC_RESET)
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_FAIL;
              }
              else
              {
                dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,ilKeyLen);
                ilRC = RC_RESET;
              }
            }
          }
        }
        if(ilRC == RC_SUCCESS)
        {
          dbg(TRACE,"TCP HEADER AREA: <%s>",pclTmpBuf);
          llTmpLen = strlen(pclTmpBuf);
          if (llTmpLen > 0)
          {
            ilMsgLen = atoi(pclTmpBuf);
            ilMsgLen++;
            if(ilMsgLen > *pipBufLen)
            {
              errno = 0;
              *pppBuf = realloc(*pppBuf,ilMsgLen);
              if(*pppBuf == NULL)
              {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: realloc(%d) failed <%s>",ilMsgLen,strerror(errno));
              }
              else
              {
                ilRC = RC_SUCCESS;
                dbg(DEBUG,"MyTcpRecv: new buffer size <%d>",ilMsgLen);
              }
            }
            if(ilRC == RC_SUCCESS)
            {
              memset(*pppBuf,0x00,ilMsgLen);
              ilMsgLen--;
              *pipBufLen = ilMsgLen;
              /* strcpy(*pppBuf,pclTmpBuf); */
              pclTmpPtr = *pppBuf;
              /* pclTmpPtr += ilRecvLen; */
              ilRecvLen = ilMsgLen;
              ilRC = MyTcpRecvBuf(ipSocket,pclTmpPtr,ilRecvLen);
              if (ilRC >= 0)
              {
                pclTmpPtr[ilRC] = '\0';
                /* ilRC += 32; */  /* XML <LEN> Header */
              }
            }
          }
        }
    break;

    default :
        dbg(TRACE,"NO LENGTH METHOD DEFINED FOR TCP/IP BRIDGE");
        ilRC = RC_FAIL;
    break;

  } /* end of switch */

  return(ilRC);

}/* end of MyTcpRecv*/


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilRecvLen = 0;
        int ilRemainder = 0;
        int ilFlag = 0;
        char *pclBufOfs = NULL;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                pclBufOfs   = pcpBuf;
                ilRemainder = ipBufLen;

                while(ilRemainder != 0)
                {
                        errno = 0;
                        igGotAlarm = FALSE;
                        igExpectedSignal = SIGALRM;
                        alarm(igReadTimeout);
                        ilRecvLen = read(ipSocket,pclBufOfs,ilRemainder);
                        alarm(0);
                        igExpectedSignal = -1;
                        if(ilRecvLen > 0)
                        {
                                if(ilRecvLen == ilRemainder)
                                {
                                        ilRC         = ipBufLen;
                                        ilRemainder  = 0;
                                }else{
                                        ilRemainder -= ilRecvLen;
                                        pclBufOfs   += ilRecvLen;
                                }/* end of if */
                        }
                        else
                        {
                                if (igGotAlarm == FALSE)
                                {
                                  switch(errno)
                                  {
                                    case EINTR :
                                        dbg(DEBUG,"MyTcpRecvBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilRecvLen);
                                        if (ilRemainder == TCP_INFOHEADER_SIZE)
                                        {
                                                ilRC = RC_RESET;
                                                ilRemainder = 0;
                                        }/* end of if */
                                        break;
                                    case 0 :
                                        dbg(DEBUG,"MyTcpRecvBuf: errno <%d> ilRecvLen <%d>",errno,ilRecvLen);
                                        MyTcpCloseSocket(ipSocket);
                                        ilRC = RC_FAIL;
                                        ilRemainder = 0;
                                        break;
                                    default :
                                        dbg(TRACE,"MyTcpRecvBuf: <%d> bytes <%s>",ilRecvLen,strerror(errno));
                                        MyTcpCloseSocket(ipSocket);
                                        ilRC = RC_FAIL;
                                        ilRemainder = 0;
                                        break;
                                  }/* end of switch */
                                }
                                else
                                {
                                  dbg(TRACE,"--- WAITED %d SECONDS WITH NO MESSAGES ON THE BRIDGE",igReadTimeout);
                                  ilRC = RC_TIMEOUT;
                                  ilRemainder = 0;
                                }
                        }/* end of if */
                }/* end of while */
        }/* end of if */
        return(ilRC);
}/* end of MyTcpRecvBuf */

/* ************************************************************* */
static void KillRemoteChild(char *pcpChildName)
{
  int ilItm = 0;
  int ilLen = 0;
  pid_t ilPid = 0;
  char pclPidList[256] = " ,";
  char pclPid[16] = "";
  char pclShellCmd[256] = "";
  char pclTmpFile[256] = "";
  FILE *fp = NULL;


/* return; */

  dbg(TRACE,"===> LOOKING FOR CHILD <%s> TO TERMINATE",pcpChildName);

      sprintf(pclTmpFile,"%s/IpcCon%s.tmp",getenv("DBG_PATH"),pcpChildName);
      dbg(DEBUG,"USING TEMP FILE <%s>",pclTmpFile);
      sprintf (pclShellCmd, "ps -e | grep -w %s | awk ' { printf $1\",\" > \"%s\"}'", pcpChildName,pclTmpFile);
      igExpectedSignal = SIGCLD;
      dbg(DEBUG,"<%s>",pclShellCmd);
      /* signal(SIGCHLD,SIG_IGN); */
      fp = popen (pclShellCmd, "w");
      pclose (fp);
      igExpectedSignal = -1;
      fp = fopen (pclTmpFile, "r");
      if (fp != NULL)
      {
        fscanf (fp, "%s", pclPidList);
        fclose (fp);
        fflush (fp);
      }
      else
      {
        dbg(TRACE,"NO CHILD TO BE TERMINATED ...");
        return;
      }
      pclPidList[strlen (pclPidList) - 1] = '\0';
      dbg(TRACE,"FOUND PID OF CHILD <%s> = <%s>",pcpChildName,pclPidList);
      if (strchr (pclPidList, ',') == NULL)
      {
        ilPid = atoi(pclPidList);
        if (ilPid > 0)
        {
          kill (ilPid, SIGTERM);
          sleep (2);
        }
      }
      else
      {
        dbg(TRACE,"FOUND MORE THAN ONE PID <%s>. TERMINATING ALL ...", pclPidList);
        ilItm = 1;
        ilLen = 1;
        while (ilLen > 0)
        {
          ilLen = get_real_item (pclPid, pclPidList, ilItm);
          if (ilLen > 0)
          {
            dbg(TRACE,"TERMINATING PID <%s>",pclPid);
            ilPid = atoi(pclPid);
            if (ilPid > 0)
            {
              kill (ilPid, SIGTERM);
            }
            ilItm++;
          }
        }
        sleep (2);
      }
      remove(pclTmpFile);

  dbg(TRACE,"===> CHILD <%s> TERMINATED",pcpChildName);

  return;
}

/* ************************************************************************* */
static int CheckMessageRecovery(char *pcpTable)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilQueRc = RC_SUCCESS;
  int ilUpdRc = DB_SUCCESS;
  int ilQueueNumber = 0;
  int ilMaxSeqn = 0;
  int ilDatLen = 0;
  int ilMinTistCnt = 0;
  int ilCurTistCnt = 0;
  int ilMaxTistCnt = 0;
  int ilMinPackCnt = 0;
  int ilCurPackCnt = 0;
  int ilMaxPackCnt = 0;
  long llTmpLen = 0;
  short slCursor = 0;
  short slFkt = 0;
  short slUpdCursor = 0;
  short slUpdFkt = 0;
  char pclUpdSqlBuf[128] = "";
  char pclUpdSqlDat[128] = "";
  char pclMyTime[16] = "";
  char pclSqlBuf[128] = "";
  char pclSqlKey[128] = "";
  char pclSqlDat[4096*2] = "";
  char pclMsgMkey[32] = "";
  char pclMsgSeqn[8] = "";
  char pclMsgStat[16] = "";
  char pclMsgData[4096] = "";
  char pclOutData[4096*2] = "";
  char pclTmpData[128] = "";
  char pclTmpMsgKey[32] = "";
  dbg(TRACE,"===> CHECK MESSAGE RECOVERY");
  igCurMsgSeqNo = -1;
  GetCurrentMsgSequence(pcpTable);
  dbg(TRACE,"CURR MAX SEQUENCE = %d",igCurMsgSeqNo);
  ilMaxSeqn = igCurMsgSeqNo;

  sprintf(pclSqlBuf,"SELECT MKEY,STAT,SEQN,DATA FROM %s WHERE OUTT=' ' ORDER BY MKEY",pcpTable);
  slCursor = 0;
  slFkt = START;
  pclSqlDat[0] = 0x00;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc == DB_SUCCESS)
    {
      ilRc = RC_SUCCESS;
      BuildItemBuffer(pclSqlDat,"",4,",");
      (void) get_real_item(cgCurMsgMkey, pclSqlDat, 1);
      (void) get_real_item(pclMsgStat, pclSqlDat, 2);
      (void) get_real_item(pclMsgSeqn, pclSqlDat, 3);
      (void) get_real_item(pclMsgData, pclSqlDat, 4);
      dbg(TRACE,"---------------- RECOVERY -----------------");
      dbg(TRACE,"MKEY <%s> STAT <%s> SEQN <%s>",cgCurMsgMkey,pclMsgStat,pclMsgSeqn);
      dbg(DEBUG,"DATA:\n%s",pclMsgData);
      igCurMsgSeqNo = atoi(pclMsgSeqn);
      if (strstr(pclMsgStat,"SP_") == NULL)
      {
        ilMinTistCnt = atoi(&cgCurMsgMkey[14]);
        ilMaxTistCnt = atoi(&cgCurMsgMkey[14]);
        ConvertDbStringToClient(pclMsgData);
        ilQueRc = tools_send_info_flag (igSendQueue, 0, "DEST", "", "RECV", "", "", cgCurMsgMkey, pcgTwEnd, 
                                     cgSendCmd, "TBL", "SEL", "FLD", pclMsgData, 0);
        dbg(TRACE,"CMD <%s> SENT TO %d (RC=%d)",cgSendCmd,igSendQueue,ilQueRc);
      }
      else
      {
        (void) CedaGetKeyItem(pclTmpData, &llTmpLen, pclMsgStat, "_", "/", TRUE);
        ilMinPackCnt = atoi(pclTmpData);
        ilCurPackCnt = atoi(pclTmpData);
        (void) CedaGetKeyItem(pclTmpData, &llTmpLen, pclMsgStat, "/", "/", TRUE);
        ilMaxPackCnt = atoi(pclTmpData);
        dbg(TRACE,"MUST COLLECT MESSAGE PACKETS FROM %d TO %d",ilCurPackCnt,ilMaxPackCnt);
        dbg(TRACE,"---------------- MESSAGE PACKAGES -----------------");
        dbg(TRACE,"MKEY <%s> STAT <%s> SEQN <%s>",cgCurMsgMkey,pclMsgStat,pclMsgSeqn);
        ilMinTistCnt = atoi(&cgCurMsgMkey[14]);
        llTmpLen = strlen(pclMsgData) - 1;
        pclMsgData[llTmpLen] = '\0';
        strcpy(pclOutData,pclMsgData);
        slFkt = NEXT;
        while (ilCurPackCnt < ilMaxPackCnt)
        {
          ilCurPackCnt++;
          ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
          if (ilGetRc == DB_SUCCESS)
          {
            ilRc = RC_SUCCESS;
            BuildItemBuffer(pclSqlDat,"",4,",");
            (void) get_real_item(cgCurMsgMkey, pclSqlDat, 1);
            (void) get_real_item(pclMsgStat, pclSqlDat, 2);
            (void) get_real_item(pclMsgSeqn, pclSqlDat, 3);
            (void) get_real_item(pclMsgData, pclSqlDat, 4);
            dbg(TRACE,"MKEY <%s> STAT <%s> SEQN <%s>",cgCurMsgMkey,pclMsgStat,pclMsgSeqn);
            dbg(DEBUG,"DATA:\n%s",pclMsgData);
            igCurMsgSeqNo = atoi(pclMsgSeqn);
            llTmpLen = strlen(pclMsgData) - 1;
            pclMsgData[llTmpLen] = '\0';
            strcat(pclOutData,pclMsgData);
          }
        }
        ilMaxTistCnt = atoi(&cgCurMsgMkey[14]);
        ConvertDbStringToClient(pclOutData);
        dbg(TRACE,"COLLECTED MESSAGE RESULT:\n%s",pclOutData);
        ilQueRc = tools_send_info_flag (igSendQueue, 0, "DEST", "", "RECV", "", "", cgCurMsgMkey, pcgTwEnd, 
                                     cgSendCmd, "TBL", "SEL", "FLD", pclOutData, 0);
        dbg(TRACE,"CMD <%s> SENT TO %d (RC=%d)",cgSendCmd,igSendQueue,ilQueRc);
      }
      if (ilQueRc == RC_SUCCESS)
      {
        GetServerTimeStamp("LOC",1,0,pclMyTime);
        strcpy(pclTmpMsgKey,cgCurMsgMkey);
        pclTmpMsgKey[14] = '\0';
        ilCurPackCnt = 1;
        for (ilCurTistCnt = ilMinTistCnt; ilCurTistCnt <= ilMaxTistCnt; ilCurTistCnt++)
        {
          sprintf(cgCurMsgMkey,"%s%0.3d",pclTmpMsgKey,ilCurTistCnt);
          if (ilMaxPackCnt == 1)
          {
            sprintf(pclUpdSqlBuf,"UPDATE %s SET STAT='SEND_OK',OUTT='%s' WHERE MKEY='%s'",pcpTable,pclMyTime,cgCurMsgMkey);
          }
          else
          {
            sprintf(pclUpdSqlBuf,"UPDATE %s SET STAT='OK_%d/%d',OUTT='%s' WHERE MKEY='%s'",
                                  pcpTable,ilCurPackCnt,ilMaxPackCnt,pclMyTime,cgCurMsgMkey);
            ilCurPackCnt++;
          }
          dbg(TRACE,"<%s>",pclUpdSqlBuf);
          slUpdFkt = START;
          slUpdCursor = 0;
          pclUpdSqlDat[0] = 0x00;
          ilUpdRc = sql_if (slUpdFkt, &slUpdCursor, pclUpdSqlBuf, pclUpdSqlDat);
          if (ilUpdRc != RC_SUCCESS)
          {
            dbg(TRACE,"sql_if failed###################");
          }
          commit_work();
          close_my_cursor (&slUpdCursor);
        }
      }
      else
      {
        dbg(TRACE,"QUE_PUT (IPC) FAILED");
        /*
        sprintf(pclUpdSqlBuf,"UPDATE %s SET STAT='SEND_FAIL' WHERE MKEY='%s'",pcpTable,cgCurMsgMkey);
        */
      }
      dbg(TRACE,"-------------------------------------------");
    }
    else
    {
      strcpy(pclSqlDat,"0");
      ilRc = RC_SUCCESS;
      if (ilGetRc != NOTFOUND)
      {
        dbg (TRACE, "=====================================================");
        dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
        dbg (TRACE, "=====================================================");
        ilRc = RC_FAIL;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);

  dbg(TRACE,"CURR MSG SEQUENCE = %d",igCurMsgSeqNo);
  igNxtMsgSeqNo = igCurMsgSeqNo + 1;
  if (igNxtMsgSeqNo > igMaxMsgSeqNo)
  {
    igNxtMsgSeqNo = 1;
  }
  dbg(TRACE,"NEXT MSG SEQUENCE = %d",igNxtMsgSeqNo);

  dbg(TRACE,"===> MESSAGE RECOVERY CHECKED");
  return ilRc;
}

/* ************************************************************************* */
static int GetCurrentMsgSequence(char *pcpTable)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[128] = "";
  char pclSqlKey[128] = "";
  char pclSqlDat[4096*2] = "";
  char pclMsgMkey[32] = "";
  char pclMsgSeqn[8] = "";
  char pclMsgStat[16] = "";
  char pclMsgData[4096] = "";
  dbg(TRACE,"---> FETCH LAST MESSAGE SEQUENCE");

  sprintf(pclSqlBuf,"SELECT MAX(MKEY) FROM %s",pcpTable);
  dbg(TRACE,"%s",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  cgMaxMsgMkey[0] = 0x00;

  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, cgMaxMsgMkey);
  if (ilGetRc == DB_SUCCESS)
  {
    ilRc = RC_SUCCESS;
  }
  else
  {
    strcpy(cgMaxMsgMkey,"0");
    ilRc = RC_SUCCESS;
    if (ilGetRc != NOTFOUND)
    {
      dbg (TRACE, "=====================================================");
      dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
      dbg (TRACE, "=====================================================");
      ilRc = RC_FAIL;
    }
  }
  close_my_cursor (&slCursor);

  sprintf(pclSqlBuf,"SELECT SEQN FROM %s WHERE MKEY='%s'",pcpTable,cgMaxMsgMkey);
  dbg(TRACE,"%s",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  pclSqlDat[0] = 0x00;

  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
  if (ilGetRc == DB_SUCCESS)
  {
    ilRc = RC_SUCCESS;
  }
  else
  {
    strcpy(pclSqlDat,"-1");
    ilRc = RC_SUCCESS;
    if (ilGetRc != NOTFOUND)
    {
      dbg (TRACE, "=====================================================");
      dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
      dbg (TRACE, "=====================================================");
      ilRc = RC_FAIL;
    }
  }
  close_my_cursor (&slCursor);

  igCurMsgSeqNo = atoi(pclSqlDat);

  dbg(TRACE,"MAX MKEY <%s>",cgMaxMsgMkey);
  dbg(TRACE,"CUR SEQN (%d)",igCurMsgSeqNo);
  dbg(TRACE,"-------------");
  return ilRc;
}

