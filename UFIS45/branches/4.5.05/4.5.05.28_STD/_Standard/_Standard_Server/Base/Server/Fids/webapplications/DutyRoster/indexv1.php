<?php include("include/manager.php");
if ($Session["cano"]) {
		session_unregister("Session");
		session_destroy();
}
?>
<html>
<head><title>UFIS&reg-Duty Roster login</title>
<META NAME="description" CONTENT="UFIS 4.4 - staff terminal">
  <META NAME="author" CONTENT="Infomap/Greece - Team@Work">
  <META NAME="dbauthor" CONTENT="Infomap/Greece & ABB Airport Technologies GmbH/Germany  - Team@Work">
  <META NAME="Art designer" CONTENT="Infomap/Greece & ABB Airport Technologies GmbH/Germany  - Team@Work">
  <META NAME="co-author" CONTENT="ABB Airport Technologies GmbH/Germany  - Team@Work">
  <META NAME="Art design" CONTENT="Infomap/Greece & ABB Airport Technologies GmbH/Germany  - Team@Work">
  <META HTTP-EQUIV="Reply-to" CONTENT="Infomap/Greece & ABB Airport Technologies GmbH/Germany  - Team@Work">
  <META HTTP-EQUIV="pragma" content="no-cache">
  <META HTTP-EQUIV="expires" content="0"> 
  <META HTTP-EQUIV="cache-control" content="no-cache">
  <META HTTP-EQUIV="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="STYLESHEET" type="text/css" href="style/style.css">
</head>
<body bgcolor="White" text="Black" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 link="black" vlink="black" alink="black" >
<form action="Roster.php" method="post">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td   height="300"  valign="top">
					<table>
					<tr>
						<td>&nbsp;</td>
						<td><img src="pics/AIA_Logo_UK.png" width="110" height="161" alt="" border="0"></td>
						<td>&nbsp;</td>
						<td valign="bottom">
							<table>
								<tr>
									<td>&nbsp;<font class="AIAFrontPage"><strong>Human Resources Department</strong></font></td>
								</tr>
								<tr>
									<td>&nbsp;<font class="AIAFrontPage">Labour Relations and Corporate Health & Safety</font></td>
								</tr>
							</table>						
						
						</td>
					</tr>
					</table>
					<!--<img src="pics/ufis.gif" width="161" height="142" alt="" border="0">--></td>
					<!--td  valign="top"><img src="../graphics/ana-logo-large.jpg" width="192" height="100" alt="" border="0"></td-->
				</tr>
				<tr>
					<td  class="Frontmenu">&nbsp;&nbsp;&nbsp;User Name</td>
					<td class="Frontmenu"><input type="text" name="UserName" autocomplete="off"></td>
				</tr>
				<tr>
					<td colspan="2"><img src="pics/dot.gif" width="1" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td class="Frontmenu">&nbsp;&nbsp;&nbsp;Password</td>
					<td class="Frontmenu"><input type="password" name="Password" autocomplete="off"></td>
				</tr>
				<tr>
					<td colspan="2"><img src="pics/dot.gif" width="1" height="10" alt="" border="0"></td>
				</tr>				
				<tr>
					<td colspan="2" align="center" valign="middle" class="Frontmenu">&nbsp;<input type="submit" name="Submit" value="Login"></td>
				</tr>
				
			</table>
		</td>
		<td align="right"><img src="pics/fpagerms.jpg"  alt="" border="0"></td>		
	</tr>
</table>
</form>
<table>
<tr height=110>
	<td align=left><img src="pics/UfisLogo.png" alt="UFIS Airport Solutions GmbH" title="UFIS Airport Solutions GmbH">
	&nbsp;&nbsp;
	<FONT FACE="Arial" SIZE=2 COLOR=#000000> &copy; 1995-2005 UFIS Airport Solutions GmbH </FONT></td>
</tr>
</table>
</body>
</html>
