#ifndef _DEF_mks_version_action_tools_h
  #define _DEF_mks_version_action_tools_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_action_tools_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/action_tools.h 1.2 2004/07/27 16:46:41SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _ACTION_TOOLS_H
#define _ACTION_TOOLS_H


#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "glbdef.h"
#include "helpful.h"
#include "send_tools.h"

/* structures */
struct _ActionData
{
	short iOriginID;	/* only 1 origin ID */
	short iRouteID[iMIN_BUF_SIZE]; 	/* max. 128 destination ID's */	
	char	pcCommand[6];	/* look at CMDBLK in glbdef.h... */
	char	pcObjName[33]	/* look at CMDBLK in glbdef.h... */;
	char	*pcSelection; /* the selection... */
	char	*pcFields; /* field list */
	char	*pcData; /* data list for all fields */
};

struct _Actions
{
	struct _ActionData	rActionData;
	struct _Actions		*prev;
	struct _Actions		*next;
};

/* prototypes */
struct _Actions *AddListItem(struct _Actions *,
					  				  struct _Actions	*, 
									  struct _ActionData *);
struct _Actions *DeleteListItem(struct _Actions *, int);
struct _Actions *DeleteList(struct _Actions *);
struct _Actions *CommitActions(struct _Actions *);
struct _Actions *RollbackActions(struct _Actions *);


#endif /* _ACTION_TOOLS_H */


