#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/putque.c 1.1 2003/03/19 17:03:44SGT fei Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/* UGCCS Program Skeleton						*/
/*									*/
/* Author		: Claudia Jeckel				*/
/* Date			: 24.09.1993					*/
/* Description		:						*/
/*									*/
/* Update history	:						*/
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* 20021126 JIM: debug_level init to zero to avoid segm. fault in dbg   */
/* 20021127 JIM: initialize_no_dbg: avoid (empty) putque log files      */
/* ******************************************************************** */
/* This program is a UGCCS main program */

static char sccs_putque[] ="@(#) UFIS 4.4 (c) ABB AAT/I putque.c 44.1 / 00/01/07 09:55:53 / VBL / MIKE";

#ident  "putque.c Version 3.2.7.0"

#define U_MAIN
#define CEDA_PRG	
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"

/*
  20021125 JIM: 
  if you want debug lines, outp, modname and log file file 
  name __CedaLogFile also hafe to be set to avoid null pointers
extern int debug_level=DEBUG;
*/
extern int debug_level=0;

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
char	*mod_name;
char	__CedaLogFile[128];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
static	EVENT	event;			/* The event        		*/

/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */
extern	int	init_que();		/* Attach to UGCCS queues	*/
extern	int	que(int,int,int,int,int,char*); /* UGCCS queuing routine*/
extern	void	catch_all();		/* Catches all signals		*/
extern  int	send_message(int,int,int,int,char*);
extern	int	atoi(const char *);
    
/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */
static int 	init_putque();
static void	handle_sig();		/* Handles signals		*/
/* static void	handle_err(int);*/	/* Handles general errors	*/
static void	handle_qerr(int);	/* Handles queuing errors	*/


/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
MAIN
{
	int	rc=0;			/* Return code			*/
	int	dest;			/* destination 			*/
	short	cmd;			/* command			*/
  
	initialize_no_dbg(argc,argv); /* General initialization	without dbg file */

	if ( argc != 3 ) {
		printf("Wrong number of parameters (2) !!!\n");fflush(stdout);
		printf("putque destination command\n");fflush(stdout);
		printf("RESET:%d SHUTDOWN:%d\n",RESET,SHUTDOWN);fflush(stdout);
		exit(0);
	} /* end if */

	rc=init_putque();
	if(rc!=RC_SUCCESS){
		exit(1);
	}/* end of if */

	catch_all(handle_sig);		/* handles signal		*/

	dest = atoi(argv[1]);
	cmd  = atoi(argv[2]);
	
	event.type = SYS_EVENT;
	event.command = cmd;
	event.originator = 9;
	event.retry_count = 0;
	event.data_offset = 0;
	event.data_length = 0;
	
	if((rc=que(QUE_PUT,dest,9,1,sizeof(EVENT),(char *)&event))!=RC_SUCCESS){
		handle_qerr(rc);
		exit(2);
	}else{
		printf("OK");fflush(stdout);
	} /* end if */
			
	exit(0);
	
} /* end of MAIN */

/* ******************************************************************** */
/* The initialization routine						*/
/* ******************************************************************** */
static int init_putque()
{
	int	cnt = 0;
	
	/* Attach to the SYMAP queues */
	while( (init_que() !=0) && (cnt<10) ){
		cnt++;
		sleep(6);		/* Wait for QCP to create queues */
	} /* end while */
	
	if(cnt>=10){
		printf("PUTQUE: ERROR: unable to init que!\n");
		return(FATAL);
	}/* end of if */

	return(RC_SUCCESS);
	
} /* end of initialize */

/* ******************************************************************** */
/* The handle signals routine						*/
/* ******************************************************************** */
static void handle_sig()
{
	
	return;
	
} /* end of handle_sig */

/* ******************************************************************** */
/* The handle general error routine					*/
/* ******************************************************************** */
/* static void handle_err(int err)
{
	return;

}*/ /* end of handle_err */

/* ******************************************************************** */
/* The handle queuing error routine					*/
/* ******************************************************************** */
static void handle_qerr(int err)
{
	switch(err) {
	case	  QUE_E_FUNC	:		/* Unknown function */
	case	  QUE_E_MEMORY	:		/* Malloc reports no memory */
	case	  QUE_E_SEND	:		/* Error using msgsnd */
	case	  QUE_E_GET	:		/* Error using msgrcv */
	case	  QUE_E_EXISTS	:		/* Route/Queue exists */
	case	  QUE_E_NOFIND	:		/* Not found ( ex. route ) */
	case	  QUE_E_ACKUNEX	:		/* Unexpected ACK received */
	case	  QUE_E_STATUS	:		/* Unknown queue status */
	case	  QUE_E_INACTIVE:      		/* Queue is inactive */
	case	  QUE_E_MISACK	:		/* Missing ACK */
	case	  QUE_E_NOQUEUES:		/* The queues don't exist */
	case	  QUE_E_RESP	:		/* No response on CREATE */
	case	  QUE_E_FULL	:		/* Table full */
	case	  QUE_E_NOMSG	:		/* No message on queue */
	case	  QUE_E_INVORG	:		/* Mod id by que call is 0 */
	case	  QUE_E_NOINIT	:		/* Queues is not initialized*/
	default			:		/* Unknown queue error */
			break;
	} /* end switch */
         
        printf("PUTQUE ERROR:%d\n",err);fflush(stdout); 
	return;

} /* end of handle_qerr */

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
