/******************************************************************************/
/*                                                                            */
/*  Author      : rsc                                                         */
/*  Date        : 28.02.2000                                                  */
/*  Description : sends events to ceda via SendCedaEvent()                    */
/*                much better than sendque                                    */
/*                                                                            */
/******************************************************************************/

#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/sendquemsg.c 1.1 2005/04/18 16:18:04SGT hag Exp  $";
#endif /* _DEF_mks_version */

#define U_MAIN     
#define UGCCS_PRG  
#define STH_USE    

#include <stdio.h>      
#include <malloc.h>     
#include <errno.h>      
#include <signal.h>     
#include "ugccsma.h"    
#include "msgno.h"      
#include "glbdef.h"     
#include "quedef.h"     
#include "uevent.h"     
#include "sthdef.h"     
#include "debugrec.h"   
#include "hsbsub.h"     

#define MAXBUFSIZE 2048

/* static char sccs_version[] ="@(#) MIKE (c) ABB AAT/I sendquemsg.c 1.2 / 00/02/29 15:26:22 / VBL"; */

int debug_level = 0;
int mod_id = 9998;

MAIN
{
  FILE *pflScript;
  int ilOutId = 0;
  char pclWKS[11] = "";
  char pclTwStart[34] = "";
  char pclTwEnd[34] = "";
  char pclCmd[7] = "";
  char pclTable[34] = "";
  char pclSelection[MAXBUFSIZE] = "";
  char pclFields[MAXBUFSIZE] = "";
  char pclData[MAXBUFSIZE] = "";
  char pclScriptFile[512] = "";
  char pclTemp[MAXBUFSIZE] = "";
  char *pclGSRet = NULL;
  int ilTemp = 0;
  int ilRC = RC_SUCCESS;

  INITIALIZE;			/* General initialization	*/

  if (argc >= 2)
  {
    ilRC = RC_FAIL;
    pflScript = fopen(argv[1], "r");
    if (pflScript != NULL)
    {
      pclGSRet = fgets(pclTemp, MAXBUFSIZE - 1, pflScript);
      if (pclGSRet != NULL)
      {
        ilOutId = atoi(pclTemp);
        if ((ilOutId <= 1) || (ilOutId >= 32767))
        {
          pclGSRet = NULL;
        } /* end of if */
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclGSRet = fgets(pclWKS, 11, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclWKS[strlen(pclWKS) - 1] = 0;
        pclGSRet = fgets(pclTwStart, 34, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclTwStart[strlen(pclTwStart) - 1] = 0;
        pclGSRet = fgets(pclTwEnd, 34, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclTwEnd[strlen(pclTwEnd) - 1] = 0;
        pclGSRet = fgets(pclCmd, 7, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclCmd[strlen(pclCmd) - 1] = 0;
        pclGSRet = fgets(pclTable, 34, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclTable[strlen(pclTable) - 1] = 0;
        pclGSRet = fgets(pclSelection, MAXBUFSIZE - 1, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclSelection[strlen(pclSelection) - 1] = 0;
        pclGSRet = fgets(pclFields, MAXBUFSIZE - 1, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclFields[strlen(pclFields) - 1] = 0;
        pclGSRet = fgets(pclData, MAXBUFSIZE - 1, pflScript);
      } /* end of if */
      if (pclGSRet != NULL)
      {
        pclData[strlen(pclData) - 1] = 0;
        ilRC = RC_SUCCESS;
      } /* end of if */
      fclose(pflScript);
    } /* end of if */
    else
    {
      printf("Unable to open scriptfile %s !\n", pclScriptFile);
    } /* end of if-else */
  } /* end of if */
  else
  {
    ilRC = RC_SUCCESS;

    do
    {
      printf("MOD-ID: ");
      ilTemp = scanf("%d", &ilOutId);
      fflush(stdin);
      if (ilTemp != EOF)
      {
        if ((ilOutId < 1) || (ilOutId > 32767))
        {
          ilTemp = EOF;
        } /* end of if */
      } /* end of if */
      if (ilTemp == EOF)
      {
        printf("Please enter a valid ");
      } /* end of if */
    } while (ilTemp == EOF);

    printf("Workstation: ");
    fflush(stdin);
    gets(pclTemp);
    memset(pclWKS, '\0', 11);
    strncpy(pclWKS, pclTemp, 9);

    printf("TW_START: ");
    fflush(stdin);
    gets(pclTemp);
    memset(pclTwStart, '\0', 34);
    strncpy(pclTwStart, pclTemp, 32);

    printf("TW_END: ");
    fflush(stdin);
    gets(pclTemp);
    memset(pclTwEnd, '\0', 34);
    strncpy(pclTwEnd, pclTemp, 32);

    printf("COMMAND: ");
    fflush(stdin);
    gets(pclTemp);
    memset(pclCmd, '\0', 7);
    strncpy(pclCmd, pclTemp, 5);

    printf("TABLE: ");
    fflush(stdin);
    gets(pclTemp);
    memset(pclTable, '\0', 34);
    strncpy(pclTable, pclTemp, 32);

    printf("SELECTION: ");
    fflush(stdin);
    gets(pclSelection);

    printf("FIELDS: ");
    fflush(stdin);
    gets(pclFields);
 
    printf("DATA: ");
    fflush(stdin);
    gets(pclData);

    printf("Scriptfile to write (enter means no scriptfile will be created)\n.tsc will be added.\n");
    fflush(stdin);
    gets(pclTemp);
    memset(pclScriptFile, '\0', 512);
    strncpy(pclScriptFile, pclTemp, 500); /* Not 512 because .tsc will be added
                                             later */

    fflush(stdin);
    if (strlen(pclScriptFile) > 0)
    {
      strcat(pclScriptFile, ".tsc");
      pflScript = fopen(pclScriptFile, "w+");
      if (pflScript == NULL)
      {
        printf("Could not create script file\n");
      } /* end of if */
      else
      {
        fprintf(pflScript, "%d\n", ilOutId);
        fprintf(pflScript, "%s\n", pclWKS);
        fprintf(pflScript, "%s\n", pclTwStart);
        fprintf(pflScript, "%s\n", pclTwEnd);
        fprintf(pflScript, "%s\n", pclCmd);
        fprintf(pflScript, "%s\n", pclTable);
        fprintf(pflScript, "%s\n", pclSelection);
        fprintf(pflScript, "%s\n", pclFields);
        fprintf(pflScript, "%s\n", pclData);
        fclose(pflScript);
        printf("Script file written to %s .\n", pclScriptFile);
      } /* end of if-else */
    } /* end of if */
  } /* end of if-else */
  if (ilRC == RC_SUCCESS)
  {
    printf("\n");
    printf("Mod ID    : %d\n", ilOutId);
    printf("WKS       : %s\n", pclWKS);
    printf("TwStart   : %s\n", pclTwStart);
    printf("TwEnd     : %s\n", pclTwEnd);
    printf("Cmd       : %s\n", pclCmd);
    printf("Table     : %s\n", pclTable);
    printf("Selection : %s\n", pclSelection);
    printf("Fields    : %s\n", pclFields);
    printf("Data      : %s\n", pclData);

    ilTemp = 0;

    do
    {

      ilRC = init_que();

      if (ilRC != RC_SUCCESS)
      {
        printf("Failed to initialize que ! Waiting 6 sec ...");
        sleep(6);
        ilTemp++;
        if (ilTemp < 10)
        {
          printf("retry\n");
        } /* end of if */
      } /* end of if */
    } while ((ilRC != RC_SUCCESS) && (ilTemp < 10));

    if (ilRC == RC_SUCCESS)
    {
      ilRC = tools_send_info_flag (ilOutId,
                                   mod_id,  
                                   "sendqm",
                                   "",
                                   pclWKS,
                                   "",
                                   "",
                                   pclTwStart,
                                   pclTwEnd,    
                                   pclCmd,
                                   pclTable,
                                   pclSelection,
                                   pclFields,
                                   pclData,
                                   RC_SUCCESS);

/*
      ilRC = SendCedaEvent(ilOutId,
                           mod_id,
                           "sendqm",
                           pclWKS,
                           pclTwStart,
                           pclTwEnd,
                           pclCmd,
                           pclTable,
                           pclSelection,
                           pclFields,
                           pclData,
                           "",
                           0,
                           RC_SUCCESS);
*/
      if (ilRC == RC_SUCCESS)
      {
        printf("Event sended correctly\n");
      } /* end of if */
      else
      {
        printf("Failed while sending event !\n");
      } /* end of if-else */
    } /* end of if */
    else
    {
      printf("\nUnable to initialize que -> Event not sended !\n");
    } /* end of if-else */
  } /* end of if */
  exit(0);
} /* end */

