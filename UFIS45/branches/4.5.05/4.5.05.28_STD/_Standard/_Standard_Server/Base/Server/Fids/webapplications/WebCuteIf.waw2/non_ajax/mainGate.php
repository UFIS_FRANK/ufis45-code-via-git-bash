<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
	include("include/manager.php");

	include("include/class.LoginInit.php");
	include ("include/class.PHP_Timer.php"); 
	
	
	/* Set Timer Instance*/
	$timer = new PHP_timer;
	$timer->start();
	/* Set Timer Instance*/	


	
	/* Login Check */
	/* Set Login Instance*/
	$login = new  LoginInit;
	$logintype="Login_"	.$AuthType;				
	if (isset($Session["userid"])) { 
		// The user has already been Logged
		
	} else {
		$Query=	$login->$logintype($username,$password);

	}
	
	/* Start Debug  if requested */
	if ($debug=="true") {
		$tmpdebug="?debug=true";
		$db->debug=$debug;
		$dbMysql->debug=$debug;
	}else {
		$debug="false";
	}			 

//include('include/ErrorHandler.php');
//$error =& new ErrorHandler();
//$error->set_altdlog(__FILE__, FILE_LOG, 'logs/main.log');


?>

<html>
<title><?php echo $PageTitle ?></title>
 <META NAME="description" CONTENT="">
  <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
  <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">  
  <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">  
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">  
  <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1253">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
  <META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">
  <link rel="STYLESHEET" type="text/css" href="style/style.css">


</head>

<body background="pics/workback.jpg" style="font-family:Arial">
<center>
	<form action="cput.php" method="post" target="Cput">
	<table>
		<tr>
			<td colspan="2"><strong>Flight Nr.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Airl.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Rem</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Sche. Time </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Est. Time</strong></td>
		</tr>

		<?php //echo $Session["FlightURNO"].'--'.$cpr;
			$tmp="";
			$cpr ="";
			
			 {$cpr = CurrentPRemarks();}
			 
			//if ($Session["STATUS"]==1  ) {
			if (isset($Session["FlightURNO"]) && $cpr!="") {
				$tmp=" disabled";
				//$tmp= "";
				//if ($Session["STATUS"]==1) {
					echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"".$Session["FlightURNO"]."\" >";
				//} else {
				//	echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"\" >";
				//}
			} 
			
		?>
		<tr>
			<td valign="top">
		<p><select name="FlightURNO" size=8 style="font-style:bold" style="font-family:Courier New" style="font-size:10pt" style="background-color:linen;" <%echo $tmp%>>
			<?php
			CheckAlloc(1);
			?>
		</select>
			</td>
		
			<td>
				
			</td>
		</tr>
		<?php if ($FreeTextSelection==true) { ?>
		<tr>
			<td colspan="2">
		<p></p>
		<p><strong>Free Text 1:</strong><br>
		<input type="text" size="50" maxlength=35 name="Text1" value="<%=FreeRemarks(1)%>">
		<p></p>
			</td>
		</tr>
		
		
		<tr>
			<td colspan="2">
		<p></p>
		<p><strong>Free Text 2: </strong><br>
		<input type="text" size="50" maxlength=35 name="Text2" value="<%=FreeRemarks(2)%>">
		<p></p>
			</td>
		</tr>
        <?php } ?>
		</table>

		<?php
			
			 
			 {$gtoStyle = "";}
			 {$boaStyle = "";}
			 {$fncStyle = "";}
			 {$gclStyle = "";}

			if ($cpr=="") {$clrStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateOpen) {$gtoStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateBoarding) {$boaStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateFinalCall) {$fncStyle = "style=\"background-color: red;\"";}
			if ($cpr==$RemarkGateClose) {$gclStyle = "style=\"background-color: red;\"";}
		?>
		<table BORDER=0 width=100%>
			<tr>
				<td valign=top width=10% ><input type="submit" name="Button" value="Gate Open" <?php echo $gtoStyle?>></td>
				<td valign=top width=10% ><input type="submit" name="Button" value="Boarding" <?php echo $boaStyle?>></td>
				<td valign=top width=10% ><input type="submit" name="Button" value="Final Call" <?php echo $fncStyle?>></td>
				<?php if ($Session["STATUS"]==1) {?>
				<td valign=top width=10% ><input type="submit" name="Button" value="Gate Closed" <?php echo $gclStyle?>></td>
				<?php } ?>
			
			</tr>
			<tr>
				<td valign=top width=10% ><input type="submit" name="Button" value="Clear Gate" <?php echo $clrStyle?>></td>
		</form>		
			</tr>
		</table>

</center>
<?php

	$timer->stop();
	//$timer->debug();
	//$timer->showtime();

?>


</body>
</html>
 
