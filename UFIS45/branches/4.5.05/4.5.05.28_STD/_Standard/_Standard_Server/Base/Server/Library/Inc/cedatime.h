#ifndef _DEF_mks_version_cedatime_h
  #define _DEF_mks_version_cedatime_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_cedatime_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/cedatime.h 1.2 2004/07/27 16:46:56SGT jim Exp  $";
#endif /* _DEF_mks_version */
#include <time.h>
#include "glbdef.h"
#include "calutl.h"

#ifndef __CEDATIME_INC
#define __CEDATIME_INC


/* Defines For CheckTimeOut() */
#define TIMO_INIT    1
#define TIMO_CHECK   2
#define TIMO_VALUE   3
#define TIMO_QUEWAIT 4

typedef long Date;  


/*
** 	The format string work with the same input than strftime function!
**  
**  Format	Contains format specifiers for the strptime subroutine.
**  The Format parameter contains zero or more specifiers.  Each
**  specifier is composed of one of the following:
**  
**  *	One or more white-space characters
**  
**  *	An ordinary character (neither % nor a white-space character)
**  
**  *	A format specifier.
**  
**  The  LC_TIME category defines the locale values for the format
**  specifiers.  The following format specifiers are supported:
**  
**  %a	Represents the abbreviated weekday name (for example, Sun) de-
**  fined by the abday statement.
**  
**  %A	Represents the full weekday name (for example, Sunday) defined
**  by the day statement.
**  
**  %b	Represents the abbreviated month name (for example, Jan) de-
**  fined by the abmon statement.
**  
**  %B	Represents the full month name (for example, January) defined
**  by the month statement.
**  
**  %c	Represents the date and time format defined by the d_t_fmt
**  statement.
**  
**  %d	Represents the day of the month as a decimal number (01 to
**  31).
**  
**  %D	Represents the date in %m/%d/%y format (for example,
**  01/31/91).
**  
**  %e	Represents the day of the month as a decimal number (01 to
**  31).  The %e field descriptor uses a two-digit field.  If the day
**  of  the  month is not a two-digit number, the  leading  digit  is
**  filled with a space character.
**  
**  %E	Represents the combined alternate era year and name, respec-
**  tively, in %o %N format.
**  
**  
**  SPECIAL FOR CEDA
**  %g  Represents the abbreviated month name in GERMAN (for example, Dez)
**      Only the first is a large Letter.
**  %G  Represents the abbreviated month name in GERMAN (for example, DEZ)
**      All Letters are large Letters.
**  END SPECIAL FOR CEDA  
**  
**  
**  %h	Represents the abbreviated month name (for example, Jan) de-
**  fined by the abmon statement.  This field descriptor is a synonym
**  for the %b field descriptor.
**  
**  %H	Represents the 24-hour-clock hour as a decimal number (00 to
**  23).
**  
**  %I	Represents the 12-hour-clock hour as a decimal number (01 to
**  12).
**  
**  %j	Represents the day of the year as a decimal number (001 to
**  366).
**  
**  %m	Represents the month of the year as a decimal number (01 to
**  12).
**  
**  %M	Represents the minutes of the hour as a decimal number (00 to
**  59).
**  
**  %n	Specifies a new-line character.
**  
**  %N	Represents the alternate era name.
**  
**  %o	Represents the alternate era year.
**  
**  %p	Represents the AM or PM string defined by the am_pm statement.
**  
**  %r	Represents 12-hour-clock time with AM/PM notation as defined
**  by the t_fmt_ampm statement.
**  
**  %S	Represents the seconds of the minute as a decimal number (00
**  to 59).
**  
**  %t	Specifies a tab character.
**  
**  %T	Represents 24-hour-clock time in the format %H:%M:%S (for ex-
**  ample, 16:55:15).
**  
**  %U	Represents the week of the year as a decimal number (00 to
**  53).  Sunday, or its equivalent as defined by the day statement,
**  is considered the first day of the week for calculating the value
**  of this field descriptor.
**  
**  %w	Represents the day of the week as a decimal number (0 to 6).
**  Sunday, or its equivalent as defined by the day statement, is
**  considered as 0  for calculating the value of this field descrip-
**  tor.
**  
**  %W	Represents the week of the year as a decimal number (00 to
**  53).  Monday, or its equivalent as defined by the day statement,
**  is considered the first day of the week for calculating the value
**  of this field descriptor.
**  
**  %x	Represents the date format defined by the d_fmt statement.
**  
**  %X	Represents the time format defined by the t_fmt statement.
**  
**  %y	Represents the year of the century (00 to 99).
**  
**  %Y	Represents the year as a decimal number (for example, 1989).
**  
**  %Z	Represents the time-zone name, if one can be determined.
**      No characters are displayed if a time zone cannot be determined.
**  
**  %%	Specifies a % (percent sign) character.
**  
**  A format specification consisting of white-space characters is
**  performed by reading input until the first non-white-space  char-
**  acter  (which  is  not  read)  or up to no more characters can be
**  read.
**  
**  A format specification consisting of an ordinary character is
**  performed by reading the next character from the Buf parameter.
**  If  this  character  differs  from  the  character comprising the
**  directive,  the  directive  fails and the differing character and
**  any characters following  it remain unread.  Case is ignored when
**  matching Buf items, such as month or weekday names.
**  
**
*/

extern void GetServerTimeStamp(char* pcpType,int ipFormat, 
                               long tpTimeDiff,char *pcpTimeStamp);

extern int CheckTimeOut(int, char *);

extern int CheckServerTime(void);

extern int AddSecondsToCEDATime (char *pcpCEDATime, time_t tpSeconds,
                                 int ipFormat);    

extern int AddSecondsToCEDATime2 (char *pcpCEDATime, time_t tpSeconds,
                                  char *pcpTimeFormat, int ipFormat);    

extern int AddSecondsToTime (char *pcpInTime, char *pcpFormat, time_t tpSeconds,
                             short spMaxSize, char *pcpOutTime);

extern int DateToString(char * buffer, const short size,
                        const char * format, const Date * date);

extern int StringToDate(const char *buffer, const char *format, Date *date);

extern int DateStringToDateString(char *target_string,
                           const char * target_format,
                           short target_size,
                           const char *source_format,
                           char *source_string);

extern short DayOfWeek(const Date *date);

extern short DayOfMonth(const Date * date);

extern short DayOfYear(const Date * date);

extern short Year(const Date * date);

extern short MonthOfYear(const Date * date);

extern Date  GetCurrentDate(void);
#endif

