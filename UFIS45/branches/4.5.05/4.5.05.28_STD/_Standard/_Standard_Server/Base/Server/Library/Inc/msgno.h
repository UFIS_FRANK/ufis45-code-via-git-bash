#ifndef _DEF_mks_version_msgno_h
  #define _DEF_mks_version_msgno_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_msgno_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/msgno.h 1.2 2004/07/27 16:48:06SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* msgno.h	28.07.93 											 */
/* ********************************************************************     */
/*													     	 */
/* This file contains the defines for the message handler message nos.	 */
/*															 */
/* ********************************************************************     */
/* The global defines for all programs */


#ifndef _MSGNO
#define _MSGNO


#define	SHUTD_REQUEST	6
#define	RESET_REQUEST	7
#define HSB_REQUEST 8
#define HSB_MAINMSG 9
#define DEBUG_MSG	10
#define QUE_ERR		11

/* the define for systrt */
#define SYSTRT_OFFSET	50
#define SYSTEXECC	SYSTRT_OFFSET +1 /* error from execv */
#define SYSTSPAWNE      SYSTRT_OFFSET +2 /* spwan error */
#define SYSTCHILDT      SYSTRT_OFFSET +3 /* child terminated */
#define SYSTCHILDR      SYSTRT_OFFSET +4 /* child restarted */
#define SYSTCHILDKD     SYSTRT_OFFSET +5 /* child keeps dying */

/* The defines for the QCP */
#define QCP_OFFSET	100
#define QCP_SYSERR	QCP_OFFSET+1		/* System error */
#define QCP_INTERR	QCP_OFFSET+2		/* Internal QCP error */
#define QCP_MSGSTAT	QCP_OFFSET+3		/* Message statistics */
#define QCP_INVFUNC	QCP_OFFSET+4		/* Invalid function */
#define QCP_ADDQUE	QCP_OFFSET+5		/* Queue added */
#define QCP_DELQUE	QCP_OFFSET+6		/* Queue deleted */
#define QCP_ROUNOF	QCP_OFFSET+7		/* Route not found */
#define QCP_ADDROU	QCP_OFFSET+8		/* Route added */
#define QCP_DELROU	QCP_OFFSET+9		/* Route deleted */
#define QCP_INVORG	QCP_OFFSET+10		/* Invalid originator */
#define QCP_NOACCS	QCP_OFFSET+11		/* Error accessing rec */
#define QCP_INACTV	QCP_OFFSET+12		/* Queue inactive */
#define QCP_MISACK	QCP_OFFSET+13		/* Missing ACK */
#define QCP_NOACK	QCP_OFFSET+14		/* No ACK */
#define QCP_DESTRO	QCP_OFFSET+15		/* Queue destroyed */
#define QCP_CONST	QCP_OFFSET+19		/* Consisting queue error */
#define QCP_QUE_GET	QCP_OFFSET+20		/* QUE_GET error */
#define QCP_CORRUP	QCP_OFFSET+21		/* 16 Queue corrupt,Rebuilding*/
#define QCP_STSSHM	QCP_OFFSET+22		/* write status to shared memory 
														failure */
#define QCP_INITQCP	QCP_OFFSET+23		/* init_QCP failure... */ 

/* the define for bchdl */
#define BCHDL_OFFSET	150
#define BCHDL_STAT	BCHDL_OFFSET +1 /* Statistic dates */

/* the define for bdhdl */
#define BDHDL_OFFSET		200
#define BDHDL_SH_MEM_ERR	BDHDL_OFFSET +1 /* error writing to shared mem */

/* the defines for SYSLIB */
#define SYSLIB_OFFSET		250
#define SYSLIB_SSM_CREATED			SYSLIB_OFFSET+1 /* SSM created */
#define SYSLIB_SSM_CREATE_FAILED	SYSLIB_OFFSET+2 /* SSM creation failed */
#define SYSLIB_SSM_DELETED			SYSLIB_OFFSET+3 /* SSM deleted */
#define SYSLIB_SSM_REORG_PERM		SYSLIB_OFFSET+4 /* SSM locked */
#define SYSLIB_SSM_USED_PERM		SYSLIB_OFFSET+5 /* SSM locked */
#define SYSLIB_SSM_REORG_OK			SYSLIB_OFFSET+6 /* SSM reorganized */
#define SYSLIB_SSM_REORG_FAILED		SYSLIB_OFFSET+7 /* SSM reorg failed */

/* the defines for netin */
#define NETIN_OFFSET		300
#define NETIN_USER_LOGIN_OK			NETIN_OFFSET+1
#define NETIN_USER_LOGIN_ERR		NETIN_OFFSET+2
#define NETIN_USER_LOGOUT			NETIN_OFFSET+3

#endif
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
