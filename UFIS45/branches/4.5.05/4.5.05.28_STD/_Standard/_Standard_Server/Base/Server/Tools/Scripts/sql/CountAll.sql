
set termout off echo off feed off trimspool on head off pages 0

spool countall.tmp
select 'SELECT count(*), '''||table_name||''' from '||table_name||';'
from   user_tables
/
spool off

set termout on
spool CountAll.txt
@@countall.tmp
spool off

set head on feed on

