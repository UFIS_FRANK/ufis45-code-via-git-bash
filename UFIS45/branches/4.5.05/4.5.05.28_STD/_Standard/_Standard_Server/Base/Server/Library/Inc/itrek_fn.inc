#ifndef _DEF_mks_version_itrek_fn_c
  #define _DEF_mks_version_itrek_fn_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_itrek_lib_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Library/Inc/itrek_fn.inc 1.2 2008/07/11 12:19:20SGT ble Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************
 AUTHOR : BLE
 CREATED DATE : 17 APR 2007
 CHANGES:
 28-JUL-2008 BLE: To pull some basic function from itkhdl.c to here
****************************************************************************/

#define UGCCS_FKT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "glbdef.h" 

extern int xml_untag( char *, char *, char *, int );
extern void to_upper( char *);
extern int strip_special_char( char *);
extern int GetTagName( char *pcpData, char *pcpTagName, char *pcpTagValue );
extern time_t FormatTime( time_t ipTime, char *pcpTimeStr, char *pcpTimeType );
extern int TrimSpace( char *pcpString );
extern void printhex( char *s );

/***********************************************************/
/* This function extract the string embedded by the XML tag*/
/* It will extract the X tag required                      */
/* Eg. xml_untag( result, str, "AAA", 1) returns "Tag1"    */
/* Eg. xml_untag( result, str, "AAA", 2) returns "Tag2"    */
/* str = <AAA>Tag1</AAA><AAA>Tag2</AAA>                    */
/***********************************************************/
int xml_untag( char *pcpResult, char *pcpXml, char *pcpKey, int ipUntagCnt )
{
    int ilLen = 0;
    int ilUntagCnt = 0;
    char *pclFunc = "xml_untag";
    char *pclStr = NULL;
    char *pclStr1 = NULL;
    char *pclKey = NULL;
    char *pclXml = NULL;

    if( ipUntagCnt <= 0 )
    {
        dbg( TRACE, "<%s> Invalid Tag Cnt = <%d>", pclFunc, ipUntagCnt );
        return FALSE;
    }
    /* dbg( DEBUG, "<%s> Tag Cnt = <%d>", pclFunc, ipUntagCnt ); */

    ilLen = strlen( pcpXml );
    /*dbg( DEBUG, "<%s> XML Len = <%d>", pclFunc, ilLen );*/

    if( strlen( pcpKey ) >= ilLen )
    {
        dbg( TRACE, "<%s> Invalid key = (%s)", pclFunc, pcpKey );
        return FALSE;
    }
    /*dbg( DEBUG, "<%s> Key Len = <%d>", pclFunc, strlen(pcpKey) );*/

    pclKey = (char *)malloc( ilLen + 10 ); /* Reserve some buffer */
    if( pclKey == NULL )
    {
        dbg( TRACE, "%s: <%d> ERROR!!! in getting memory!!!", pclFunc, __LINE__ );
        exit( 1 );
    }
    pclXml = (char *)malloc( ilLen );
    if( pclXml == NULL )
    {
        dbg( TRACE, "%s: <%d> ERROR!!! in getting memory!!!", pclFunc, __LINE__ );
        exit( 1 );
    }
    memcpy( pclXml, pcpXml, ilLen ); /* To preserve the org str */

    while( ilUntagCnt < ipUntagCnt )
    {
        ilUntagCnt++;

        sprintf( pclKey, "<%s>", pcpKey );
        pclStr = (char *)strstr( pclXml, pclKey );
        if( pclStr == NULL )
        {
            free(pclKey);
            free(pclXml);
            return FALSE;
        }
        pclStr += strlen( pclKey );
        sprintf( pclKey, "</%s>", pcpKey );
        pclStr1 = (char *)strstr( pclStr, pclKey );
        if( pclStr1 == NULL )
        {
            free(pclKey);
            free(pclXml);
            return FALSE;
        }
        if( ilUntagCnt >= ipUntagCnt )
            break;
        strcpy( pclXml, pclStr1 );
    }

    ilLen = 0;
    ilLen = strlen( pclStr ) - strlen( pclStr1 );
    if( ilLen <= 0 )
    {
        free(pclKey);
        free(pclXml);
        return FALSE;
    }
    memcpy( pcpResult, pclStr, ilLen );
    /* dbg( DEBUG, "<%s> Result Str <%s>", pclFunc, pcpResult ); */
    pcpResult[ilLen] = '\0';
    free(pclKey);
    free(pclXml);
    return TRUE;
}

void to_upper( char *pcpRawStr )
{
    int ili;
    int ilLen;

    ilLen = strlen(pcpRawStr);
    for( ili = 0; ili < ilLen; ili++ )
        pcpRawStr[ili] = toupper( pcpRawStr[ili] );
    return;
}

int strip_special_char( char *pcpProcessStr )
{
    int blStripped = FALSE;
    int ilLen;
    int ili, ilj;
    unsigned char clChar;
    char *pclStr;
    char *pclFunc = "strip_special_char";

    ilLen = strlen(pcpProcessStr);
    /*dbg( DEBUG, "%s: Orig len <%d>", pclFunc, ilLen );*/
    if( ilLen <= 0 )
        return blStripped;
    pclStr = (char *)malloc(ilLen+10);

    ilj = 0;
    for( ili = 0; ili < ilLen; ili++ )
    {
        clChar = pcpProcessStr[ili] & 0xFF;
        if( (clChar >= 0x20 && clChar <= 0x21) ||
            (clChar >= 0x23 && clChar <= 0x25) ||
            (clChar >= 0x28 && clChar <= 0x3B) ||
             clChar == 0x3D ||
            (clChar >= 0x3F && clChar <= 0x5F) ||
            (clChar >= 0x61 && clChar <= 0x7E) )
        {
            pclStr[ilj++] = pcpProcessStr[ili];
            continue;
        }
        blStripped = TRUE;
        pclStr[ilj++] = ' ';
    }

    pclStr[ilj] = '\0';
    strcpy( pcpProcessStr, pclStr );
    /*dbg( DEBUG, "%s: New len <%d>", pclFunc, strlen(pcpProcessStr) );*/
    if( blStripped == TRUE )
        dbg( TRACE, "%s: str <%s>", pclFunc, pcpProcessStr );
    if( pclStr != NULL )
        free(pclStr);
    return blStripped;
}

int GetTagName( char *pcpData, char *pcpTagName, char *pcpTagValue )
{
    char pclTmpStr[2148]; 
    char pclKey[10] = "</URNO>";
    char *pclToken;
    char *pclFunc = "GetTagName";

    memcpy( pclTmpStr, pcpData, strlen( pcpData ) );
    pclToken = (char *)strstr( pclTmpStr, pclKey );
    if( pclToken == NULL )
        return RC_FAIL;
    pclToken += strlen( pclKey ) + 1;  /* Due to anchor bracket */

    while( *pclToken != '>' )
    {
        *pcpTagName++ = *pclToken++;
    }
    *pcpTagName = '\0';
    *pclToken++;  /* Due to anchor bracket */
 
    while( *pclToken != '<' )
    {
        *pcpTagValue++ = *pclToken++;
    }
    *pcpTagValue = '\0';
    /*dbg (TRACE, "<%s> TagName = (%s) TagValue = (%s)", pclFunc, pcpTagName, pcpTagValue );*/
    return RC_SUCCESS;
} 

int TrimSpace( char *pcpInStr )
{
    int ili = 0;
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10);
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else
       {
           while( *pclP == ' ' )
               pclP++;
       }
    }
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}

/* Get current local time and format to YYYYMMDDHHMISS */
time_t FormatTime( time_t ipTime, char *pcpTimeStr, char *pcpTimeType )
{
    struct tm rlTmptr;
    time_t ilNow;
    char pclTimeStr[20];

    ilNow = ipTime;
    if( !strncmp(pcpTimeType, "UTC", 3 ) )
        gmtime_r( (time_t *)&ilNow, &rlTmptr );
    else
        localtime_r( (time_t *)&ilNow, &rlTmptr );
    sprintf( pcpTimeStr, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d", rlTmptr.tm_year+1900, rlTmptr.tm_mon+1,
                                                           rlTmptr.tm_mday, rlTmptr.tm_hour,
                                                           rlTmptr.tm_min,rlTmptr.tm_sec );
    return( ilNow );
}


void printhex( char *s )
{
    int i;
    int len;

    if( !s )
        return;

    len = strlen(s);
    for( i = 0; i < len; i++ )
    {
        dbg( TRACE, "0x%x ", s[i] );
        if( (i%8) == 0 )
            dbg( TRACE, "" );
    }
    dbg( TRACE, "" );
}

/****************************************************************************/

