/*******************************************/
/* GetValue30: Calculation of absence time */
/*******************************************/
static double GetValue30(const char *pcpIsHoliday,const char *pcpSday,const char *pcpLocalDrrStartTime,const char *pcpLocalDrrEndTime, const char *pcpSblu)
{
	double dlDrrMin;
	double dlDrrSblu;

	if (pcpIsHoliday[0] != '1')
	{
		dbg(DEBUG,"GetValue30(): No absence!");
		return 0;
	}

	/* length of absence */
	dlDrrMin = GetDailyDRRMinutesByStaff(pcpSblu,pcpLocalDrrStartTime,pcpLocalDrrEndTime);
	dbg(DEBUG,"GetValue30(): Minutes of absence <dlDrrMin> = <%lf>",dlDrrMin);

	if (dlDrrMin == 0)
	{
		char clDrrMin[10];
		char clHolType[10];

		if (ReadTemporaryField("VereinbarteWochenstunden",clDrrMin) > 0)
		{
			dlDrrMin = atof(clDrrMin);
		}
		dbg(DEBUG,"GetValue30(): Use agreed working hours per week: <%lf>",dlDrrMin);

		/* check holiday type */
		if (GetHolType(pcpSday,clHolType))
		{
			dbg(DEBUG,"GetValue30(): Holiday type: <%s>",clHolType);

			if (strcmp(clHolType,"3") == 0)
			{
				dbg(DEBUG,"GetValue30(): Take only half of the time because of holiday type!");
				dlDrrMin = dlDrrMin / 2;
			}
		}
	}

	dbg(DEBUG, "GetValue30(): Result = <%lf>",dlDrrMin);

	/* return hours */
	return dlDrrMin / 60;
}

/* ***********************************************/
/* ****** Account 30: holiday hours (reversible) */
/* ***********************************************/
static void bas_30()
{
	char 	clLastCommand[4];
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clNewShiftCode[10];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clNewDrrStatus[2];
	char	clDrrSblu[6];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clOldShiftCode[10];
	char	clOldDrrStatus[2];
	char	clOldDrrSblu[6];
	char	clYear[6];
	char	clMonth[4];
	char	clOldAccValue[128];
	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	double	dlNewAccValue[12];
	int	i;
	char	clIsHoliday[2];

	dbg(DEBUG, "============================= ACCOUNT 30 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 30: Stopped, because of temporary staff");
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 30: Command (%s)",clLastCommand);

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 30: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 30: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 30: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 30: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 30: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 30: Stopped, because <clSday> = undef!");
		return;
	}

	/* Status of new record	*/
	if (ReadTemporaryField("NewRoss",clNewDrrStatus) == 0)
	{
		dbg(TRACE,"Account 30: Stopped, because <clNewDrrStatus> = undef!");
		return;
	}

	/* Shift code of new DRR-record */
	if (ReadTemporaryField("NewScod",clNewShiftCode) == 0)
	{
		dbg(TRACE, "Account 30: <clNewShiftCode> = undef!");
		strcpy(clNewShiftCode,"");
	}

	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 30: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 30: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* Break duration */
	if (ReadTemporaryField("NewSblu",clDrrSblu) <= 0)
	{
		dbg(TRACE, "Account 30: <clDrrSblu> = undef!");
		strcpy(clDrrSblu,"0");
	}

	/* calculate values with old DRR */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* check old data */
		if (strlen(pcgOldData) == NULL)
		{
			dbg(TRACE, "Account 30: <lastOldData> = undef!!");
		}

		/* Shift code of old DRR-record */
		if (ReadTemporaryField("OldScod",clOldShiftCode) == 0)
		{
			dbg(TRACE, "Account 30: <clOldShiftCode> = undef!");
			strcpy(clOldShiftCode,"");
		}

		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			dbg(TRACE, "Account 30: <clOldDrrStartTime> = undef!");
			strcpy(clOldDrrStartTime,"0");
		}
	
		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			dbg(TRACE, "Account 30: <clOldDrrEndTime> = undef!");
			strcpy(clOldDrrEndTime,"0");
		}

		/* Status of old record	*/
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) == 0)
		{
			dbg(TRACE,"Account 30: <clOldDrrStatus> = undef!");
			clOldDrrStatus[0] = '\0';
		}

		/* Break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrSblu) <= 0)
		{
			dbg(TRACE, "Account 30: <clOldDrrSblu> = undef!");
			strcpy(clOldDrrSblu,"0");
		}
	}

	/* old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		dbg(DEBUG,"Account 30: (clOldShiftCode) <%s>" , clOldShiftCode);
		if (strcmp(clOldDrrStatus,"A") == 0)
		{
			ReadTemporaryField("OldIsHoliday",clIsHoliday);
			dlOldValue = GetValue30(clIsHoliday,clSday,clOldDrrStartTime,clOldDrrEndTime,clOldDrrSblu);
		}
		else
		{
			dbg(TRACE,"Account 30: Wrong status of old DRR-record: <%s>" ,clOldDrrStatus);
		}

		dbg(DEBUG,"Account 30: (oldValue) <%lf>" ,dlOldValue);
	}

	/* new value */
	dlNewValue = 0;
	dbg(DEBUG,"Account 30: (clNewShiftCode) <%s>",clNewShiftCode);
	if (strcmp(clNewDrrStatus,"A") == 0)
	{
		ReadTemporaryField("NewIsHoliday",clIsHoliday);
		dlNewValue = GetValue30(clIsHoliday,clSday,clDrrStartTime,clDrrEndTime,clDrrSblu);
	}
	else
	{
		dbg(DEBUG,"Account 30: Wrong status of new DRR-record: <%s>" ,clNewDrrStatus);
	}

	dbg(DEBUG,"Account 30: New value <%lf>" ,dlNewValue);

	/* calculate result */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue; 
	}

	dbg(DEBUG,"Account 30: Result value <%lf>",dlResultValue);

	if  (dlResultValue == 0)
	{
		dbg(DEBUG,"Account 30: Value not changed.");
		return;
	}

	/* get ACC record */
	strncpy(clYear,clSday,4);
	clYear[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"30",clYear,dlNewAccValue);

	strncpy(clMonth,&clSday[4],2);
	clMonth[2] = '\0';

	dbg(DEBUG,"Account 30: Old ACC value / month [%s] = <%lf>",clMonth,dlNewAccValue[atoi(clMonth)-1]);

	i = atoi(clMonth);

	dbg(DEBUG,"Account 30: Month index: <%s>",clMonth);

	while(i <= 12)
	{
		dlNewAccValue[i-1] = (dlNewAccValue[i-1] - dlResultValue);
		dbg(DEBUG,"Account 30: New ACC value / month [%d] = <%lf>",i,dlNewAccValue[i-1]);
		i = i + 1;
	}

	/* save results for this and for next year */
	UpdateMultiAccountHandle("30",clStaffUrno,clYear,"SCBHDL",dlNewAccValue);
	UpdateNextYearsValuesHandle("30",clYear,clStaffUrno,dlNewAccValue[11]);
}
