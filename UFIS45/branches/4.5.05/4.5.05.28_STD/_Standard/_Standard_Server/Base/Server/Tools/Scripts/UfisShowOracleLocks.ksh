# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisShowOracleLocks.ksh 1.3 2006/07/25 23:34:36SGT jim Exp  $
#
# 20031208 JIM: using $CEDADBUSER/$CEDADBPW instead of fix settings
# 20060725 JIM: correct ordering of SQLTEXT
#
function doc
{
cat <<EOFDOC1

====== 
Title: 
====== 
 
Report Sessions Waiting for Locks 
 
 
=========== 
Disclaimer: 
=========== 
 
This script is provided for educational purposes only. It is NOT supported by 
Oracle World Wide Technical Support.  The script has been tested and appears  
to work as intended.  However, you should always test any script before  
relying on it. 
 
PROOFREAD THIS SCRIPT PRIOR TO USING IT!!!  Due to differences in the way text  
editors, email packages and operating systems handle text formatting (spaces,  
tabs and carriage returns), this script may not be in an executable state when  
you first receive it.  Check over the script to ensure that errors of this  
type are corrected. 
 
 
========= 
Abstract: 
========= 
 
Generates a report of users waiting for locks. 
 
 
============= 
Requirements: 
============= 
 
SELECT on V\$SESSION, V\$LOCK 
 
========= 
Examples: 
========= 
 
 
USERNAME          SID TYPE HELD        REQ           ID1        ID2 
--------------- ----- ---- ----------- ----------- -------- -------- 
SYSTEM             12 TX   Exclusive   None          131087     2328 
SCOTT               7 TX   None        Exclusive     131087     2328 
SCOTT   	    8 TX   Exclusive   None          131099     2332 
SYSTEM             10 TX   None        Exclusive     131099     2332 
SYSTEM             12 TX   None        Exclusive     131099     2332

EOFDOC1
}

# ==============================================================

function Script1
{
   sqlplus -s $CEDADBUSER/$CEDADBPW @<<EOFSQL

SET ECHO off 
REM NAME:   TFSLCKWT.SQL 
REM USAGE:"@path/tfslckwt" 
REM ------------------------------------------------------------------------ 
REM REQUIREMENTS: 
REM    SELECT on V\$SESSION, V\$LOCK 
REM ------------------------------------------------------------------------ 
REM AUTHOR:  
REM    Joe Sparks      
REM ------------------------------------------------------------------------ 
REM PURPOSE: 
REM    Reports users waiting for locks. 
REM ------------------------------------------------------------------------ 
REM EXAMPLE: 
REM    USERNAME          SID TYPE HELD        REQ           ID1        ID2 
REM    --------------- ----- ---- ----------- ----------- -------- -------- 
REM    SYSTEM             12 TX   Exclusive   None          131087     2328 
REM    SCOTT               7 TX   None        Exclusive     131087     2328  
REM    SCOTT               8 TX   Exclusive   None          131099     2332 
REM    SYSTEM             10 TX   None        Exclusive     131099     2332 
REM    SYSTEM             12 TX   None        Exclusive     131099     2332 
REM  
REM ------------------------------------------------------------------------ 
REM DISCLAIMER: 
REM    This script is provided for educational purposes only. It is NOT  
REM    supported by Oracle World Wide Technical Support. 
REM    The script has been tested and appears to work as intended. 
REM    You should always run new scripts on a test instance initially. 
REM ------------------------------------------------------------------------ 
REM Main text of script follows: 
 
column sid      format  9990    heading SID 
column type     format  A4 
column program  format  A25     heading 'Program'
column process  format  99999990 heading 'PID'
column lmode    format  990     heading 'HELD' 
column request  format  990     heading 'REQ' 
column id1      format  9999990 
column id2 format  9999990 
column username format A8
column object format A18 
column SQL format A32

set pagesize 60 
set linesize 120
break on id1 skip 1 dup 
prompt
prompt List of held and requested Locks:
prompt =================================
prompt
SELECT sn.username, substr(sn.program,1,20)||'...' Program, sn.process, m.sid, m.type, 
        DECODE(m.lmode, 0, 'None', 
                        1, 'Null', 
                        2, 'Row Share', 
                        3, 'Row Excl.', 
                        4, 'Share', 
                        5, 'S/Row Excl.', 
                        6, 'Exclusive', 
                        lmode, ltrim(to_char(lmode,'990'))) lmode, 
        DECODE(m.request,0, 'None', 
                        1, 'Null', 
                        2, 'Row Share', 
                        3, 'Row Excl.', 
                        4, 'Share', 
                        5, 'S/Row Excl.', 
                        6, 'Exclusive', 
                        request, ltrim(to_char(m.request, '990'))) request,
	m.kaddr
FROM v\$session sn, v\$lock m 
WHERE (sn.sid = m.sid AND m.request != 0) 
   OR (sn.sid = m.sid AND m.request = 0 AND lmode != 4 
       AND (id1, id2) IN (SELECT s.id1, s.id2 
                          FROM v\$lock s 
                          WHERE request != 0 
                            AND s.id1 = m.id1 
                            AND s.id2 = m.id2) 
      ) 
ORDER BY id1, id2, m.request; 
clear breaks 


prompt
prompt List of waiting SQL statements:
prompt ===============================
prompt

select s.username username,  
       substr(s.program,1,20)||'...' Program, s.process PID, a.sid sid,  
       a.owner||'.'||a.object object,  
       s.lockwait,  
       t.sql_text SQL 
from   v\$sqltext t,  
       v\$session s,  
       v\$access a 
where  t.address=s.sql_address  
and    t.hash_value=s.sql_hash_value  
and    s.sid = a.sid  
and    a.owner != 'SYS' 
and    upper(substr(a.object,1,2)) != 'V\$' 
and    s.lockwait <> ' '
order  by t.address,t.piece
/ 
prompt I do not no a way to find the sql-Statement of the blocking process
prompt but "select sql_text from v\$sqltext where username ='....';" may help you
prompt 

prompt
prompt List of last SQL-Statements for processes and lock mode <> share:
prompt (If this list is not empty, it could include the blocking SQL-Statement. 
prompt  But the blocking SQL-Statement may be to old to be in the list.) 
prompt ===================================================================
prompt
SELECT sq.username, substr(sq.program,1,20)||'...' Program, 
       sq.process PID, sq.sid, q.sql_text
FROM v\$session sq, v\$sqltext q
where q.address=sq.sql_address  
and   q.hash_value=sq.sql_hash_value  
and (q.address, q.hash_value) in ( select sn.sql_address, sn.sql_hash_value  
   FROM v\$session sn, v\$lock m 
   WHERE (sn.sid = m.sid AND m.request = 0 AND lmode != 4 
       AND (id1, id2) IN (SELECT s.id1, s.id2 
                          FROM v\$lock s 
                          WHERE request != 0 
                            AND s.id1 = m.id1 
                            AND s.id2 = m.id2) 
      ) 
   )
order  by q.address,q.piece
;
-- select * from v$sqltext where sql_text like 'update ceda.systab set ttyp%';
-- select * from v$lock where addr = (
--    select address from v$sqltext where sql_text like 'update ceda.systab set ttyp%');
-- select * from v$lock where kaddr = (
--    select address from v$sqltext where sql_text like 'update ceda.systab set ttyp%');
-- select * from v$session where saddr = (select ADDRESS from v$sqltext where sql_text like 'update ceda.systab set ttyp%');
-- select * from v$session where paddr = (select ADDRESS from v$sqltext where sql_text like 'update ceda.systab set ttyp%');
-- select * from v$session where SQL_ADDRESS = (select ADDRESS from v$sqltext where sql_text like 'update ceda.systab set ttyp%');

EOFSQL
}


# ==============================================================


case "$1" in
   "-?"|"-h"|"-help"|"--h"|"--help")
       doc
       echo  ==============================================================
       echo  ==============================================================
       exit
       ;;
   *)  Script1 | grep -v "SP2-0310: unable to open file"
       ;;
esac
