<?php
include("ApplicationSettings.php");

// generates a drop down list 
function GenSelectList($del,$data,$OptTextNr,$OptValNr,$SelectEntry,$SelectExit) {
    echo "[$del,$data,$OptTextNr,$OptValNr,$SelectEntry,$SelectExit]";
    //$rs=explode($del,$data);
    //echo "[$SelectEntry]";
    $x=0;
    $xmax=count($data);
    while ($x < $xmax) {
        echo "$data[0])";
        echo "<option value=\"".$rs[$OptValNr]."\">".$rs[$OptTextNr]."</option>\n";
        $x++;
    }
    //echo "$SelectExit";
}
function CallQput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$Twstart,$Twend,$SubCmd) {
    Global $HOPO, $QputShellCommand;
    Global $RSHIP,$CputPath;
    Global $isCput,$wks,$cputuser,$TW_START,$TW_END;

    $Func="CallQput";
    //$Def_Twstart=$_SERVER['REMOTE_ADDR'];
    //$TW_START="\'".$_SERVER['REMOTE_ADDR']."\'";
    //$TW_START="\'0\'";
    //$Def_Twstart="XXX";
    //$Def_Twend=$HOPO.",TAB,QPUT";

    //////////////////////////////////////////////////////////////////////
    // if the len is <=0 we must not have some "" around the those values.
    // this would mix up the cput-programm
    //////////////////////////////////////////////////////////////////////
    if (strlen($Fields) >= 0) {
        str_pad($Fields,strlen($Fields)+2,"\"",STR_PAD_BOTH);
    }
    if (strlen($Data) >= 0) {
        str_pad($Data,strlen($Data)+2,"\"",STR_PAD_BOTH);
    }
//    if (strlen($Selection) >= 0) {
//        str_pad($Selection,strlen($Selection)+2,"\"",STR_PAD_BOTH);
//        $Selection = stripslashes($Selection);
//    }

    ///////////////////////////////////////////////////////
    // now we sho some info about the event if DEBUG is set
    ///////////////////////////////////////////////////////
    if ($Dbg_level==DEBUG) {
        //print $Func."--que:".$Dest." "."prio:".$Prio." "."cmd:".$Command." "."tab:".$Table." "."fld:".$Fields." "."dat:".$Data." "."sel.:".$Selection." "."wait:".$Timeout;

        print "<br>-----------------------------------------";
        print "<br>".$Func."que :".$Dest;
        print "<br>".$Func."pri :".$Prio;
        print "<br>".$Func."cmd :".$Command;
        print "<br>".$Func."tab :".$Table;
        print "<br>".$Func."fld :".$Fields;
        print "<br>".$Func."dat :".$Data;
        print "<br>".$Func."sel.:".$Selection;
        print "<br>".$Func."wait:".$Timeout;
        print "<br>".$Func."tws :".$TW_START;
        print "<br>".$Func."twe :".$TW_END;
        print "<br>-----------------------------------------<br>";
    }
    if (empty($Prio)) {
        $Prio=3;
    }
//    if (empty($Twstart)) {
//        $Twstart=$Def_Twstart;
//    }
//    if (empty($Twend)) {
//        $Twend=$Def_Twend;
//    }

    //////////////////////////////////////////////
    // now we call qput with the passed parameters
    //////////////////////////////////////////////
    if (!empty($Prio) && !empty($Command) && !empty($Table) && !empty($Fields) && !empty($Data)) {
        //$cmd='/ceda/etc/qput que='
        //$cmd='remsh tonga -l ceda -n ". /ceda/etc/UfisEnv; /ceda/etc/qput que='
//		$cmd= $QputShellCommand.' que='
//			.$Dest.' prio='
//			.$Prio.' cmd='
//			.$Command.' tab='
//			.$Table.' fld='
//			.$Fields.' dat="'
//			.$Data.'" sel=\"'
//			.$Selection.'\" wait='
//			.$Timeout.' tws='
//			.$Twstart.' twe='
//			.$Twend.' "';

        
        if ($isCput) {
        $cmd=  " 2 \'$Dest\' "
                ."\'$Prio\' "
                ."\'$Command\' "
                ."\'$Table\' "
                ."\'$Fields\' "
                ."\'$Data\' "
                ."\'$Selection\' "
                //."\'$Timeout\' "
                ."\'0\' "
                .$wks ." "
                .$cputuser ." "
                .$TW_START ." "
                .$TW_END." ";
        } else {
            $cmd=  "\'$Dest\' "
                ."\'$Prio\' "
                ."\'$Command\' "
                ."\'$Table\' "
                ."\'$Fields\' "
                ."\'$Data\' "
                ."\'$Selection\' "
                ."\'$Timeout\' "
                .$TW_START." "
                .$TW_END." ";
        }
        //////////////////////////////////////////////
        // now we call cput with the passed parameters
        //////////////////////////////////////////////
        $command=$CputPath." ".$cmd;
        
        if (!empty($SubCmd)) {
            $command=$command ."|$SubCmd";
        }
        
        //$command=$CputPath." 2 $Dest $Prio $Command $Table $Fields $Data $Selection $Timeout ";
        if (strlen($RSHIP)>0) {
            $first_result= cmd($RSHIP,$command );
        } else {
            $command=str_replace ("\'", "'",$command);
            $first_result = `$command`;
        }

        if ($Dbg_level==DEBUG) {
            echo "<pre>CMD:$command</pre>";
        }

//        if (!empty($SubCmd))
//            $result=`$cmd|$SubCmd`;
//        else
//            $result=`$cmd`;
    }
    else {
        $first_result="Invalid qput-call issued. Can not process!";
    }
    return $first_result;
}

function cmd ($ip,$cmd) {
    Global $error,$RSCMD;
    $strreturn="";
    //You may need to provide the full path
    //If your web-server user has no environment 'path'
    //echo $RSCMD ." " . $ip. "  ". $cmd ." ";
    $line = exec($RSCMD ." " . $ip. "  ". $cmd ." " , $output, $rsherror);

    while (list(,$line) = each($output)) {
        $strreturn= $strreturn."".$line;
    }

    if ($rsherror) {
        $strreturn="Unix error code: $rsherror<BR>\n";
    }
    return 	$strreturn;
}
?>
