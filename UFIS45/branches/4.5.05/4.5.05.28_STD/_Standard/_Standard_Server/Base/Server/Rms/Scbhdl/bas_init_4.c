/* calculate the month-value for initializing account 4 */
static BOOL	CalculateInit4(const char *pcpStaffUrno,const char *pcpSday,double *dpFreitageMonPlan)
{
	char		clMfmField[5];
	char		clMfmValue[10];
	char		clYear[6];
	char		clMonth[4];
	char		clKeyValue_ResultBuffer[100];
	int			ilMfmHandle;
	SCODATA 	olScoData;
	long		llRowNum = ARR_FIRST;

	/* get the handle of the MFMTAB (Minimum Free of Month) */
	ilMfmHandle = GetArrayIndex("MFMTAB");
	if (ilMfmHandle < 0)
	{
		dbg(TRACE,"CalculateInit4(): ERROR - Array for MFMTAB not found!");
		return FALSE;
	}

	/* get the contract data of the employee */
	if (GetScoData(pcpStaffUrno,pcpSday,&olScoData) == FALSE)
	{
		dbg(TRACE,"CalculateInit4(): ERROR - Couldn't get contract data of employee <%s>!",pcpStaffUrno);
		return FALSE;
	}

	/* Initialize variables */
	strncpy(clYear,pcpSday,4);
	clYear[4] = '\0';
	strncpy(clMonth,&pcpSday[4],2);
	clMonth[2]= '\0';

	/* building field name */
	strcpy(clMfmField,"MF");
	strcat(clMfmField,clMonth);

	/* building key value to search */
	strcpy(clKeyValue_ResultBuffer,olScoData.cmScoCode);
	strcat(clKeyValue_ResultBuffer,",");
	strcat(clKeyValue_ResultBuffer,clYear);

	if (FindFirstRecordByIndex(ilMfmHandle, clKeyValue_ResultBuffer) > 0)
	{
		if (GetFieldValueFromMonoBlockedDataString(clMfmField,sgRecordsets[ilMfmHandle].cFieldList,clKeyValue_ResultBuffer,clMfmValue) > 0)
		{
			*dpFreitageMonPlan = atof(clMfmValue);
			dbg(DEBUG,"CalculateInit4(): Value of <%s> = <%lf>",clMfmField,*dpFreitageMonPlan);
			return TRUE;
		}
		else
		{
			dbg(TRACE,"CalculateInit4(): MFM-field value of field <%s> not found!",clMfmField);
			return FALSE;
		}
	}
	else
	{
		dbg(TRACE,"CalculateInit4(): MFM-record for contract <%s> and year <%s> not found!",olScoData.cmScoCode,clYear);
		return FALSE;
	}
}

/***********************************************/
/* Initializing account 4: free days per month */
/***********************************************/
static void bas_init_4()
{
	char	clFrom[16];
	char	clYearFrom[6];
	char	clMonFrom[4];
	char	clDayFrom[4];

	char	clTo[16];
	char	clYearTo[6];
	char	clMonTo[4];
	char	clDayTo[4];

	int		ilCountEmpl;
	char	clCurrentEmpl[12];

	STAFFDATA	olStfData;

	double	dlFreitageMonPlan = 0;
	char	clFreitageMonPlan[20];

	int		ilCounter;
	int		ilYearCount;
	int		ilMonthCount;
	char	clCalcDate[16];
	int		ilAccHandle = -1;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_4 START =============================");

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account Init 4: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account Init 4: <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account Init 4: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 4: <pcgFields> = <%s>",pcgFields);

	/* checking the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"Account Init 4: Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 4: <pcgSelKey> = <%s>",pcgSelKey);

	/* get start date */
	if (GetFieldValueFromMonoBlockedDataString("FROM",pcgFields,pcgData,clFrom) <= 0)
	{
		dbg(TRACE,"Account Init 4: Stopped, because <clFrom> = undef!");
		return;
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strncpy(clMonFrom,&clFrom[4],2);
	clMonFrom[2] = '\0';
	strncpy(clDayFrom,&clFrom[6],2);
	clDayFrom[2] = '\0';

	/* get end date */
	if (GetFieldValueFromMonoBlockedDataString("TO",pcgFields,pcgData,clTo) <= 0)
	{
		dbg(TRACE,"Account Init 4: Stopped, because <clTo> = undef!");
		return;
	}
	strncpy(clYearTo,&clTo[0],4);
	clYearTo[4] = '\0';
	strncpy(clMonTo,&clTo[4],2);
	clMonTo[2] = '\0';
	strncpy(clDayTo,&clTo[6],2);
	clDayTo[2] = '\0';

	dbg(DEBUG, "Account Init 4: Initializing from %s to %s.",clFrom,clTo);

	/* count number of employees */
	ilCountEmpl = CountElements(pcgSelKey);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"Account Init 4: Stopped, because no employees to initialize!");
		return;
	}
	dbg(DEBUG,"Account Init 4: Number of employees = <%d>", ilCountEmpl);

	/* loop per employee */
	for (ilCounter = 0; ilCounter < ilCountEmpl; ilCounter++)
	{
		if (GetElement(pcgSelKey,ilCounter,clCurrentEmpl) <= 0)
		{
			dbg(TRACE,"Account Init 4: Stopped, because <clCurrentEmpl> = undef!");
			return;
		}

		/* get staff data containing e.g. PENO */
		GetStaffData(clCurrentEmpl,&olStfData);

		/* Used in UpdateAccountXY! */
		WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

		dbg(DEBUG,"Account Init 4: Start calculation for employee <%s>",clCurrentEmpl);

		/* loop through the years */
		for (ilYearCount = atoi(clYearFrom); ilYearCount <= atoi(clYearTo); ilYearCount++)
		{
			/* loop through the months */
			for (ilMonthCount = atoi(clMonFrom); ilMonthCount <= atoi(clMonTo); ilMonthCount++)
			{
				/* reset values */
				dlFreitageMonPlan = 0.0e0;

				/* build time-variable for calculation */
				sprintf(clCalcDate,"%4d%02d%02d",ilYearCount,ilMonthCount,1);
				dbg(DEBUG,"Account Init 4: Start calculation for date <%s>",clCalcDate);

				/* calculate values */
				if (!CalculateInit4(clCurrentEmpl,clCalcDate,&dlFreitageMonPlan))
				{
					dbg(TRACE,"Account Init 4: ERROR in 'CalculateInit4'! Calculating next employee!");
					continue;
				}
				else
				{
					dbg(DEBUG,"Account Init 4: <dlFreitageMonPlan> = <%lf>",dlFreitageMonPlan);
				}

				if (dlFreitageMonPlan > -1)
				{
					sprintf(clFreitageMonPlan,"%-8.2lf",dlFreitageMonPlan);
					dlFreitageMonPlan = atof(clFreitageMonPlan);
					UpdateAccountXYHandle(clCurrentEmpl,dlFreitageMonPlan,"4",clCalcDate,"XBSHDL","CO");
				}
			}
		}
	}
}

