#ifndef _DEF_mks_version_stbsubr_c
  #define _DEF_mks_version_stbsubr_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_stbsubr_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/stbsubr.c 1.3 2004/08/10 20:29:42SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*$:
----------------------------------------------------------------------------

	S  T  B   -   S  U  B  R  O  U  T  I  N  E  S

----------------------------------------------------------------------------

	Version		:  0.0	May 1989
	Programmer	:  KJK 
	Name		:  STBSUBR

	This module is used by 'STB'.
	It has the following entry points:

	- char *sysalloc(bytes)      - this entry point allocates 'bytes'
                                       (long) bytes of dynamic memory and
                                       zeroes it if allocation successful
                                     - it returns either a character pointer
                                       to a start of the dynamic memory block
                                       or a character pointer NULL
                                     - it calls 'malloc'-routine to allocate 
                                       the memory

	- int isint(p)               - this entry point checks if the ASCII-
                                       string pointed to by 'p' (character
                                       pointer) is numeric
                                     - it returns a one if true and a zero if
                                       not
                                     - it uses the macro 'isdigit' 

	- int ishex(p)               - this entry point checks if the ASCII-
                                       string pointed to by 'p' (character
                                       pointer) is a hex-digit
                                     - it returns a one if true and a zero if
                                       not
                                     - it uses the macro 'isxdigit' 

	- int sb_move(dest,src,n)    - this entry point moves 'n' characters
                                       from 'src' to 'dest' (both character
                                       pointers) and pads 'dest' with blanks
                                       if 'src' terminated before 'n' charac-
                                       ters are moved


         - int getyesno(s)           - this entry point processes the 'Y'/'N'-
                                       parameter (either an additional check
                                       for the system resource or a continuat-
                                       ion of a definition) 
                                     - it returns a minus one if the parameter
                                       pointed by 's' (character pointer) is
                                       neither 'Y' nor 'N'
                                     - otherwise it returns a one if 'Y' and
                                       a zero if 'N'

         - char *strsave(s)          - this entry point saves the ASCII-string
                                       pointed by 's' (character pointer) into
                                       a dynamic memory block
                                     - it calls 'sysalloc'-entry to allo-
                                       cate a dynamic memory block the size of
                                       which is the length of the ASCII-string
                                       plus one and, if successful it calls
                                       'strcpy'-routine to copy the string.
                                       Because 'sysalloc' always zeroes the
                                       the allocated memory block, the copied
                                       string will automatically be zero ter-
                                       minated
                                     - it either returns the start address of
                                       the memory block (character pointer) or
                                       a character pointer NULL (allocation
                                       failed)

         - int blkmove(src,des,x)    - this entry point moves a memory block
                                       from 'src' (character pointer) into
                                       'des' (character pointer). The size
                                       of the block is 'x' (int)

         - int asctohex(dest,src,lim) 
                                     - this entry point converts the ASCII-
                                       string 'src' (character pointer) to
                                       hexadecimal format ('dest' (character
                                       pointer)). The length of the ASCII- 
                                       string is 'lim' (int). All characters
                                       of the ASCII-string must be 'hex-
                                       digits' 
                                     - the following macros are called:
                                       - toupper
                                       - isxdigit
                                     - the return value is either a zero
                                       (failed, ie. a non-hexdigit within the
                                        ASCII-string) or a one (successful)
                                                                         $:*/
 
/*2+*/

#define UGCCS_FKT
#include <stdio.h>
#include <ctype.h>
#include "glbdef.h" /* For __FILESTAT__ !! */

#define BLANK ' '

/**************************  Function prototype header **********************/
extern  char *sysalloc(long bytes);
extern  int isint(char *p);
extern  int ishex(char *p);
extern  int sb_move(char *dest,char *src,int n);
extern  int getyesno(char *s);
extern  char *strsave(char *s);
extern  int blkmove(char *src,char *des,int x);
extern  int asctohex(char *dest,char *src,int lim);
/****************************************************************************/
 

/*1:
	S Y S A L L O C  -  subfunction

	External routines:

	- char *malloc();       allocates system dynamic memory 
                                                                          1:*/

/*2+*/

char *sysalloc(long bytes)        /* allocates dynamic memory for 'STB'     */
/* bytes : size of the memory block                  */ 

{
        register char *p;    /* working pointer                           */ 
        register int n = 0;  /* working variable                          */ 
	char *base;          /* pointer returned                          */
  /* 20040107 JIM: allready defined via glbdef.h/malloc.h: 
	char *malloc();      * memory allocation routine                 */

        if ((p = base = malloc(bytes)) != NULL)  /* successful            */
             while (n < bytes)
                    p[n++] = 0;   /* clear memory                         */

        return(base);        /* return the pointer                        */ 
}

/*1:
	I  S  I  N  T  -  subfunction

	Macros used:

	- isdigit
                                                                         1:*/

/*2+*/


isint(register char *p)           /* check for int string             */

/* p : pointer to the ASCII-string      */ 

{
        register c;               /* working variable                 */ 

        while ((c = *p++) !=0)     /* not a terminator                 */ 
               if (!isdigit(c))   /* not a digit                      */ 
                   return(0);     /* return error                     */ 
        return(1);                /* OK                               */ 
}

/*1:
	I  S  H  E  X  -  subfunction

 	Macros used:

	- isxdigit
                                                                         1:*/

/*2+*/

ishex(char *p)                    /* check for a hex integer string       */

/* p : pointer to the ASCII-string          */ 

{
        register c;               /* working variable                     */ 

        while ((c = *p++) !=0)    /* until NUL terminator                 */ 
               if (!isxdigit(c))  /* not a hex digit                      */ 
                   return(0);     /* return error                         */ 
        return(1);                /* OK                                   */ 
}

/*1:
	S  B  _  M  O  V  E  -  subfunction
                                                                         1:*/

/*2+*/ 


sb_move(register char *dest,char *src,int n)
/* move & pad with blank            */

/* *dest : destination pointer              */ 
/* *src  : source pointer                   */
/* n     : size in characters               */ 

{
        register c;               /* working variable                     */ 

        while (((c = *src++) !=0) && n-- > 0)
			/* until terminator or until n    */ 
                *dest++ = c;      /* exhausted                            */ 

        while (n-- > 0)           /* source was shorter                   */ 
                *dest++ = BLANK;  /* pad with space character             */ 
}
/*1:
	G  E  T  Y  E  S  N  O  -  subfunction
                                                                         1:*/

/*2+*/


getyesno(register char *s)         /* process 'Y'/'N'                      */ 

/* *s : pointer to the parameter             */ 

{
        register c;               /* working variable                     */ 

        if ((c = *s) != 'Y' && c != 'N') /* invalid parameter             */ 
             return(-1);          /* return error                         */ 
        return((c == 'Y') ? 1 : 0); /* if 'Y' return 1                    */ 
}

/*1:
	S  T  R  S  A  V  E  -  subfunction

	Internal subroutines:

	- sysalloc

	External routines:

	- strlen
                                                                         1:*/

/*2+*/


char *strsave(register char *s)          /* save string 's' somewhere      */

/* *s : address of ASCII-string        */ 

{
        register char *p;               /* working pointer                 */ 

        if ((p = sysalloc(strlen(s)+1)) != NULL) /* allocated              */ 
             strcpy(p,s);               /* copy the string                 */ 
        return(p);                      /* return the address or NULL      */ 
}

/*1:
	B  L  K  M  O  V  E  -  subfunction
                                                                         1:*/

/*2+*/


blkmove(register char *src,register char *des,register int x)
/* move a block of memory          */ 

/*  *src : source address                  */
/*  *des : destination address             */ 
/*  x    : size of the block in characters */ 

{
        while (x--)                     /* the whole block                 */ 
               *des++ = *src++;
}

/*1:
	A  S  C  T  O  H  E  X  -  subfunction

	Macros used:

	- toupper
	- isxdigit
                                                                          1:*/
/*2+*/


asctohex(char *dest,register char *src,register int lim)
/* converts an ASCII hex-digit to  */ 
                                        /* binary hex-digit                */
/* *dest : address of binary hex-digit     */
/* *src  : address of ASCII hex-digit      */ 
/* lim   : size of ASCII hex-digit        */  
{
        register int c;                 /* working variable               */
        char *help;                     /* working pointer                */ 
	int size = 0;                   /* working variable               */

        help = dest;

        for (size = 0; lim > 0; lim--, src++) /* the whole string         */ 
            {
            c = toupper(*src);          /* to upper case                  */ 
            if (!isxdigit(c))           /* not a hex digit                */ 
                return(0);              /* return error                   */ 
            c = (c > '9') ? c - 'A' + 10 : c - '0'; /* convert to binary   */ 
            if (++size & 1)             /* 1st digit of a pair             */ 
                *help = 0 | c << 4;     /* yes                             */ 
            else
                *help++ |= c;           /* 2nd digit of a pair             */ 
            }
        return(1);                      /* OK                              */ 
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
