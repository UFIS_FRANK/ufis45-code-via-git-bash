%{
#include <stdio.h>
#include <string.h>
#include "glbdef.h"

void yyerror(char *s);
int  rc(int a, char c, int b);
int  fc(float a, char c, float b);
int  sc(char *s1, char c, char *s2);

#ifndef _DEF_mks_version_rmschkrul_y
  #define _DEF_mks_version_rmschkrul_y
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_rmschkrul_y[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/rmschkrul.y 1.5 2006/01/21 00:38:58SGT jim Exp  $";
#endif /* _DEF_mks_version_rmschkrul_y */

static int igYaccRC = 0;  /* 0 if false, 1 if rule is true , -1 is syntax error */

int RmsCheckRule(char *pcpRule, int *pipRuleResult) ;
extern char *rmschkrulstring; /* 20060117 JIM: why was this missing? AIX failed! */
%}

%union {
        int     no ;
        float   real ;
        char    string[2048] ;
}

%token UNGLEICH ODER UND
%token <real> REAL
%token <string> STRING

%type <no> alphaausdruck regel 
%type <no> teilregel realausdruck gesamtregel
%type <string> alphaterm 
%type <real> realterm

%left '+' '-'
%left '*' '/'
%left VORZEICHEN

%%

gesamtregel   : regel {$$ = $1; igYaccRC = $$;}
              | error { igYaccRC = -1; *rmschkrulstring = 0x00;}
              ;

regel         : regel UND teilregel  {$$ = rc($1,'&',$3);}
              | regel ODER teilregel {$$ = rc($1,'|',$3);}
              | teilregel  {$$ = $1;}
              ;

teilregel     : alphaausdruck {$$ = $1;}
              | realausdruck  {$$ = $1;}
              | '(' regel ')' {$$ = $2;}
              ;

alphaausdruck : alphaterm '>' alphaterm {$$ = sc($1,'>',$3);}
              | alphaterm '<' alphaterm {$$ = sc($1,'<',$3);}
              | alphaterm '=' alphaterm {$$ = sc($1,'=',$3);}
              | alphaterm UNGLEICH alphaterm {$$ = sc($1,'!',$3);}
              ;

alphaterm     : '(' alphaterm ')' {strcpy($$,$2);}
              | STRING            {strcpy($$,$1);}
              ;

realausdruck  : realterm '>' realterm {$$ = fc($1,'>',$3);}
              | realterm '<' realterm {$$ = fc($1,'<',$3);}
              | realterm '=' realterm {$$ = fc($1,'=',$3);}
              | realterm UNGLEICH realterm {$$ = fc($1,'!',$3);}
              ;

realterm      :  '(' realterm ')' {$$ = $2;}
              | realterm '+' realterm {$$ = $1+$3;}
              | realterm '-' realterm {$$ = $1-$3;}
              | realterm '*' realterm	{$$ = $1*$3;}
              | realterm '/' realterm {$$ = $1/$3;}
              | '-' realterm %prec VORZEICHEN {$$ = -$2;}
              | REAL {$$ = $1;}
              ;

%%



/******************************************************************************/
/* unary compare                                                              */
/******************************************************************************/
int rc(int a, char c, int b)
{
  switch (c)
  {
   case '&': return a&&b?1:0 ; 
   case '|': return a||b?1:0 ;
   default : return 1 ;  
  }
} /* function rc(...) */



/******************************************************************************/
/* float compare                                                              */
/******************************************************************************/
int fc(float a, char c, float b)
{ 
  switch (c)
  {
   case '>': return a>b?1:0 ;
   case '<': return a<b?1:0 ;
   case '=': return a==b?1:0 ;
   case '!': return a!=b?1:0 ; 
   default : return 1 ;  
  }
} /* function fc(...) */


/******************************************************************************/
/* string compare                                                             */
/******************************************************************************/
int sc(char *s1, char c, char *s2)
{ 
  int	ilRC ;

  ilRC = strcmp (s1,s2) ; 
  switch (c)
  {
   case '>': return ilRC>0?1:0 ;
   case '<': return ilRC<0?1:0 ;
   case '=': return ilRC==0?1:0 ;
   case '!': return ilRC!=0?1:0 ;
   default : return 1 ;  
  }
} /* function sc(...) */



#include "lex.rmschkrul.h"
#include "glbdef.h"

/***********************************************************************/
/* call parser for string                                              */
/***********************************************************************/
int RmsCheckRule(char *pcpRule, int *pipRuleResult)
{
  int  ilRC = 0 ;

  rmschkrulstring = pcpRule ;
  igYaccRC = 0 ;

  ilRC = yyparse() ;
  if (ilRC == 0)
  {
   switch (igYaccRC)
   {
    case -1 : /* syntax error in expression */
              *pipRuleResult = -1 ;
               ilRC = -1 ;
            break ;

    case 0 :  /* expression is true */
              *pipRuleResult = TRUE;
            break ;

    case 1 :  /* expression is false */
              *pipRuleResult = FALSE ;
            break ;

    default : /* error in source */
              dbg(TRACE,"RmsCheckRule: unexpected RC <%d> from yyparse()", igYaccRC) ;
              *pipRuleResult = -1 ;
               ilRC = -1 ;
            break ;

   } /* end of switch */
  } 
  else 
  {
   *pipRuleResult = -1 ;
  } /* end of if */

  return ilRC ;

} /* end of RmsCheckRule */



/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void yyerror(char *s)
{
 dbg (DEBUG,"yyerror: <%s>",s) ;
 return ;
}


