# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/LibDependencies.mak 1.2 2005/03/25 02:18:20SGT jim Exp  $
# 20050324: added $(wildcard xy.c) to allow classification of sources,
#           in _Standard which are not included in project
# 20050324: classified action.c bchdl.c dummy.c ntisch.c router.c sysqcp.c
#
# following processes require multi-thread library support
MTH_DB_SRCS= $(wildcard bccom.c)

# following processes do not require Oracle library support and DBlib:
NDB_SRCS= $(wildcard action.c bchdl.c dummy.c netin.c ntisch.c router.c sysqcp.c)

# build some lists of targets depending on above lists of sources:
# (used in Makefile.sub)
TMP_DB_OBJS= $(patsubst %,$(OBJDIR)/%,$(STD_SOURCES:.c=)) 
MTH_DB_OBJS= $(patsubst %,$(OBJDIR)/%,$(MTH_DB_SRCS:.c=)) 
NDB_OBJS=    $(patsubst %,$(OBJDIR)/%,$(NDB_SRCS:.c=))

# exclude the lists of above targets from normal DB build:
DB_OBJS= $(filter-out $(MTH_DB_OBJS) $(NDB_OBJS),$(TMP_DB_OBJS))
