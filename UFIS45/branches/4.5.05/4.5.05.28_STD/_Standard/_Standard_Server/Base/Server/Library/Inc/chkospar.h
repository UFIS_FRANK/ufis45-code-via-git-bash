#ifndef _DEF_mks_version_chkospar_h
  #define _DEF_mks_version_chkospar_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkospar_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkospar.h 1.2 2004/07/27 16:47:01SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*	The Master include file.											*/
/*																*/
/*	Program		:	chkospar										*/
/*	Revision date	:												*/
/*	Author		:	jmu											*/
/*																*/
/*	NOTE			:	This should be the only include file for your program	*/
/*	**********************************************************************	*/

#ifndef __CHKOSPAR_INC
#define __CHKOSPAR_INC


#include	"chkdef.h"

#define	STATUS_INIT	0
#define	STATUS_NUMBER	1
#define	STATUS_LETTER	2
#define	STATUS_CALC	3
#define	OPEN_BRACKET	4
#define	CLOSE_BRACKET	5
#define	STATUS_EXIT	6

char	*pcGStatString[] = {
	"STATUS_INIT",
	"STATUS_NUMBER",
	"STATUS_LETTER",
	"STATUS_CALC",
	"OPEN_BRACKET",
	"CLOSE_BRACKET",
	"STATUS_EXIT"
};

#endif

