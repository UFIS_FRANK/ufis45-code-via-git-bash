CREATE OR REPLACE  FUNCTION GETCCAFLDVAL
(v_urno in ccatab.urno%TYPE,v_fld in systab.fina%TYPE,v_tab in fldtab.rtab%TYPE)
return char
is
rc_txt char(128);
CURSOR ccatab_curs (p_urno in ccatab.urno%TYPE) IS
	SELECT * FROM ccatab WHERE urno = p_urno;
BEGIN
  if v_tab <> 'CCA' and v_tab <> 'CCATAB' then
		rc_txt := ' ';
  else
	for cca_rec in ccatab_curs(v_urno) loop
		IF ccatab_curs%NOTFOUND THEN
			rc_txt := ' ';
		ELSE
			if upper(v_fld) = 'CTYP' THEN
				rc_txt := cca_rec.ctyp;
			elsif upper(v_fld) = 'CKBA' THEN
				rc_txt := cca_rec.ckba;
			elsif upper(v_fld) = 'CKBS' THEN
				rc_txt := cca_rec.ckbs;
			elsif upper(v_fld) = 'CKEA' THEN
				rc_txt := cca_rec.ckea;
			elsif upper(v_fld) = 'CKES' THEN
				rc_txt := cca_rec.ckes;
			elsif upper(v_fld) = 'DISP' THEN
				rc_txt := cca_rec.disp;
			elsif upper(v_fld) = 'REMA' THEN
				rc_txt := cca_rec.rema;
			else
				rc_txt := ' ';
			end if;
		END IF;
		EXIT WHEN ccatab_curs%NOTFOUND OR ccatab_curs%FOUND;
	END LOOP;
	end if;
    RETURN (rtrim(rc_txt,' '));
END;
/

