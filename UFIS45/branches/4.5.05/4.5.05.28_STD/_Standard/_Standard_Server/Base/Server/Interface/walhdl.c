#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/walhdl.c 1.0 2011/07/25 18:11:45CEST jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/*                                                 */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
//static char sccs_version[] ="@(#) UFIS (c) ABB AAT/I dummy.c 44.2 / "__DATE__" "__TIME__" / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
typedef struct {
  char *data;
  uint status;      /* various status information (bit-mask)    */ 
  uint len;         /* size of currently allocated data element */
  uint timeType;    /* time is UTC or timezone */
  char timeZone[3]; /* the 2 letter code of the timezone, if time is local */
} DataElementType;

#define MAX_NUM_LEGS 32

struct List {
  char *field;
  struct List *next;
  struct List *prev;
  DataElementType dobj[MAX_NUM_LEGS];    /* points to a DataElement   */
};

struct myList{
	char Airline[3];
	char FlightNo[4];
	char DepartureTime[15];
	char Urno[10];
	struct myList *next;
};
typedef struct myList myListElem;
typedef struct mylistElem myListPtr;
//static myListPtr AFDUList=NULL;


int  debug_level = DEBUG;
typedef struct List ListElem;
typedef ListElem *ListPtr;
#define MAX_INT_PARAMETER 32000
#define IS_VALID   0x01
#define DATABLK_SIZE (256*1024)
#define RC_NODATA -129
#define RC_NOTFOUND -130

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int ConvertDbStringToClient(char *pcpString);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";  
enum TimerType {NONE,TIM,EVT};
static uint igDayTab []	= { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
static char  cgErrMsg[128];
char *cgValidEvents [] = { "FC", "ATD", NULL };

static char pcgFdihdlName[16] = "fdihdl";
static int   pcgGrp1Modid;
//static int   pcgGrp2Modid;
//static int   pcgGrp3Modid;
static int   igFdiId = 0;
static int   igDelTimeIntrvl;
static int   TimeRangeSelectFlights = 72;
static int   igTimeWindow = 10;         /* time window to fetch flights from -- should be read from config */
//static ListPtr pcgGrp2Airlines=NULL;
static ListPtr myRequestCmdList=NULL;
static ListPtr pcgItemList=NULL;
static char  pcgDataArea[DATABLK_SIZE];
static char  pcgSqlBuf[12 * 1024];

static char cgErrorsToFdi[10] = "\0";
static char cgAlerts[100][512];
static char cgErrors[100][512];
static char cgAlertSections[1024] = "\0";
static int igAlertCount = 0;

static char  pcgRecvName[64] ;          /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;          /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;           /* BC-Head used as Stamp */
static char  pcgTwEnd[64] ;             /* BC-Head used as Stamp */
static char  pcgChkWks[128] ; 
static char  pcgChkUsr[128] ; 
static char  pcgActCmd[16];
static long lgEvtCnt = 0;

static int myTIME=30;
static time_t lasttime=0;
/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);
static int get_int(char *Section, char *field, int *data);
static int setup_config();
static int HandleLongTimerRequest(int ipInit);
static void alert(char *pcpMessage);
static void ReadAlertConfig();
static int ReadConfigEntryRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int deleteItemList(ListPtr hook);
static int createItemList(char *list, ListPtr *hook);
static char *getNextItem(char *cpSource, char *cpDelim, uint *ipLen, uint *ipLast);
static ListPtr getFirstFree(ListPtr hook); 
static int removeItemFromList (ListPtr hook);
static int addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus);
int addTimerEntry(long lpFLURNO, char *cpALC2, char *cpStartTime, char *cpFLNO);
static char *getFirstActTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry, int ipHour, int ipMinAct, int ipMinStart);
static char *getFirstTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry);
static char *getItemDataByName(ListPtr hook, char *name, uint legLine);
static void PutSequence(char *pcpKey,long lpSequence);
static long GetSequence(char *pcpKey);
int getOrPutDBData(uint ipMode);
static int checkCedaTime(char *pcpData);
static int addItemToList(char *element, ListPtr *hook);
int performTimerOrEventAction(char *pcpSelection, int ipIsEvent);
int sendAlteaCommand( long myWAID,char *pcpActCmd,  char *pcpFLNO, char *pcpACTTOD, char *pcpURNO, char *pclActTimer, char *pclALC2);
int updateTimerEntry(char *pcpHRList, char *pcpMinList, char *pcpACTTOD, long myWAID, char *pcpEvent, int ipIsEvent, int myTIMR);
int isValidALC2(char *cpALC2);

static int  HandleData_User(BC_HEAD *prlBchead,
			    CMDBLK *prlCmdblk,
			    char *pclSelection,
			    char *pclFields,
			    char *pclData);

static void  HandleShortTimerRequest();
static void HandleAlteaResponse(char *pcpData, char *pcpCommand, char *pcpIdent);
static void HandleUpdateFlightTimer(char *pcpFields, char *pcpData);
static void HandleDeleteFlightTimer(char *pcpFields, char *pcpData);
static void  HandleCleanUp();
static void HandleInsertFlightTimer(char *pcpFields, char *pcpData);			  
int testSend();  
void delete_aevtab();
static int readTime();
static int send_defaultflights();
static int send_tmp(char* alc2,char* flno,char* dtime,char* urno,char* viades);

static int    Reset(void);                       /* Reset program          */
static void    Terminate(int ipSleep);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(EVENT *prpEvent);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
 
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
char pcgMyRealFileName[]= "WALHDL:"; 
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    debug_level = TRACE|DEBUG;
    
    INITIALIZE;            /* General initialization    */

    //argv[0] name and path of this process
	  strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(DEBUG,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(DEBUG,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(DEBUG,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

		do
	  {
			ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			check_ret(ilRC);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(DEBUG,"MAIN: init_db() OK!");
	} /* end of if */
	
		//ctrl_sta=HSB_ACTIVE;
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    dbg(DEBUG,"ConfigFile <%s>",cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(DEBUG,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    /* uncomment if necessary */
    /* sprintf(cgConfigFile,"%s/dummy.cfg",getenv("CFG_PATH")); */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        //if(igInitOK == FALSE)
        //{
           //delete_aevtab();
            ilRC = Init_Process();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
        //}/* end of if */
    } else {
        Terminate(30);
    }/* end of if */
    
    
		dbg(TRACE,"=====================");
		dbg(TRACE,"MAIN: initializing OK");
		dbg(TRACE,"=====================");
		
    //testSend();
    readTime();
    lasttime=time(NULL);
#if 1
   for(;;)
	{   
		ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData(prgEvent);
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */




		}
		else if (ilRC == QUE_E_NOMSG)
{
	//dbg(TRACE,"MAIN: QUE_E_NOMSG");
	if(time(NULL) > (lasttime + myTIME))
		{
		   send_defaultflights();
		   lasttime=time(NULL);
		}
	nap(500);
} 
		else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
		
	} /* end for */
#endif  
} /* end of MAIN */

void delete_aevtab()
{
  sprintf(pcgSqlBuf,"DELETE FROM AEVTAB WHERE ALC2<>' '");
  getOrPutDBData(START|COMMIT);
 }

 static int readTime()
{
	char mytime[10]="\0";
	//char *mytime;
	char myConfigFile[200]="\0";
	int myResult=RC_SUCCESS;
	char *mySection = "MAIN";
	char *myKeyword = "TIME";
	
		dbg(TRACE,"<readTime> 1");
	sprintf(myConfigFile,"%s/walhdl_tmp.cfg",getenv("CFG_PATH"));
	
		dbg(TRACE,"<readTime> 2:w");
	myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword,CFG_STRING,mytime);

		dbg(TRACE,"<readTime> 3:w");
	if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<readTime> failed when read TIME");
		return myResult;
	}
	myTIME=atoi(mytime);
	dbg(TRACE,"<readTime> read TIME <%d>",myTIME);
        return 1;
}
static int send_defaultflights()
{
	int myResult=RC_SUCCESS;
	char myConfigFile[200]="\0";
	char *mySection = "MAIN";
	char myAIRLINE_NAME[1000]="\0";
	char myFLIGHT_NO[1000]="\0";
	char myDEPARTURE_TIME[2000]="\0";
	char myURNODATA[2000]="\0";
	char myVIADESDATA[1000]="\0";
	char *myALC2;
	char *myFLNO;
	char *myTIFD;
	char *myURNO;
	char *myVIADES;
	char *myALC2_s;
	char *myFLNO_s;
	char *myTIFD_s;
	char *myURNO_s;
	char *myVIADES_s;
  char *myKeyword1 = "AIRLINE_NAME";
  char *myKeyword2 = "FLIGHT_NO";
  char *myKeyword3 = "DEPARTURE_TIME";
  char *myKeyword4 = "URNO";
  char *myKeyword5 = "VIADES";
  
	sprintf(myConfigFile,"%s/walhdl_tmp.cfg",getenv("CFG_PATH"));
	
	
	myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword1,CFG_STRING,myAIRLINE_NAME);
	if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<send_defaultflights> failed when read AIRLINE_NAME");
		return myResult;
	}
	dbg(TRACE,"<send_defaultflights> AIRLINE_NAME <%s>",myAIRLINE_NAME);
  myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword2,CFG_STRING,myFLIGHT_NO);
	if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<send_defaultflights> failed when read FLIGHT_NO");
		return myResult;
	}
	dbg(TRACE,"<send_defaultflights> FLIGHT_NO <%s>",myFLIGHT_NO);
	
	myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword3,CFG_STRING,myDEPARTURE_TIME);
  if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<send_defaultflights> failed when read DEPARTURE_TIME");
		return myResult;
	}
	dbg(TRACE,"<send_defaultflights> DEPARTURE_TIME <%s>",myDEPARTURE_TIME);
	
  myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword4,CFG_STRING,myURNODATA);
   if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<send_defaultflights> failed when read URNO");
		return myResult;
	}
	dbg(TRACE,"<send_defaultflights> URNODATA <%s>",myURNODATA);
	
	myResult = iGetConfigEntry(myConfigFile,mySection,myKeyword5,CFG_STRING,myVIADESDATA);
   if(myResult != RC_SUCCESS)
		{
		dbg(TRACE,"<send_defaultflights> failed when read VIADES");
		return myResult;
	}
	dbg(TRACE,"<send_defaultflights> VIADES <%s>",myVIADESDATA);
	

	myALC2 = strtok_r(myAIRLINE_NAME, ",",&myALC2_s);
	myFLNO = strtok_r(myFLIGHT_NO,",",&myFLNO_s);
	myTIFD = strtok_r(myDEPARTURE_TIME, ",",&myTIFD_s);
	myURNO = strtok_r(myURNODATA, ",",&myURNO_s);
	myVIADES = strtok_r(myVIADESDATA, ",",&myVIADES_s);
	
	while(myALC2!=NULL&&myFLNO!=NULL&&myTIFD!=NULL&&myURNO!=NULL&&myVIADES!=NULL)
	{	     
	    send_tmp(myALC2,myFLNO,myTIFD,myURNO,myVIADES);
			 myALC2 = strtok_r(NULL, ",",&myALC2_s);
	     myFLNO = strtok_r(NULL,",",&myFLNO_s);
	     myTIFD = strtok_r(NULL, ",",&myTIFD_s);
	     myURNO = strtok_r(NULL, ",",&myURNO_s);
	     myVIADES = strtok_r(NULL, ",",&myVIADES_s);
  }
  return;
}
static int send_tmp(char* alc2,char* flno,char* dtime,char* urno,char* viades)
{
	short slFkt;
	short slCursor = 0;
//	char myCOMD[30]="\0";
	int ilGetRc;
	char  pclSqlBuf[1024]="\0";
	char pclDataArea[1024]="\0";
	
	dbg(TRACE,"<send_tmp> viades <%s>",viades);
       // char mydtime[50]="\0";
        //strncpy(mydtime,dtime,strlen(dtime));	
sprintf(pclSqlBuf,"SELECT COMD FROM HCFTAB WHERE ALC2='%s' AND STAT='ACTIVE'", alc2);
	slFkt = START;

  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
    if (ilGetRc != RC_SUCCESS) {
      dbg(TRACE, "<send_tmp> Airline not configured <%s>", alc2);
      close_my_cursor (&slCursor);
      return RC_FAIL;
    }
    else {
    	//BuildItemBuffer(pclDataArea, NULL, 1, ";");
			//myCOMD = strtok(pclDataArea, ";");
                        char myCOMD[20]="\0";
			strncpy(myCOMD,pclDataArea,strlen(pclDataArea));
			//myCOMD[strlen(pclDataArea)]="\0";
			dbg(TRACE, "<send_tmp> try to send walhdl_tmp data ALC2 <%s> COMD<%s> FLNO<%s> TIFD<%s> URNO<%s>", alc2,myCOMD,flno,dtime,urno);
char tmp[20]="\0";
                     sprintf(tmp,"%s %s",alc2,flno);			
char mydtime[50]="\0";
    sprintf(mydtime,"%s",dtime); 
 // strncpy(mydtime,dtime,strlen(dtime));	
sendAlteaCommand_tmp("55555",myCOMD,tmp,mydtime,urno,"2h",alc2,viades);
		}
		slFkt = NEXT;
		}
		close_my_cursor (&slCursor);
  
}

static int Init_Process()
{
  int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
  long pclAddFieldLens[12];
  char pclAddFields[256] = "\0";
  char pclSqlBuf[2560] = "\0";
  char pclSelection[1024] = "\0";
  short slCursor = 0;
  short slSqlFunc = 0;
  char  clBreak[24] = "\0";
  short slWait = 1;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if(ilRc == RC_SUCCESS) {
          ilRc = ReadConfigEntry("MAIN","FDIHDL_NAME",pcgFdihdlName);
	  //igCEDAbridge = tool_get_q_id("bridge");
	  /*igFdiId = tool_get_q_id("fdihdl");*/
	  igFdiId = tool_get_q_id(pcgFdihdlName);
	  dbg(TRACE,"Mod-Id of %s = %d",pcgFdihdlName,igFdiId);
	}

	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "Grp_Modid", &pcgGrp1Modid);
	}

	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "EXPIRED_TIMER_INTERVAL", &igDelTimeIntrvl);
	}
	if (ilRc != RC_SUCCESS){
	  igDelTimeIntrvl = 7;
	  ilRc = RC_SUCCESS;
	}
	if(ilRc == RC_SUCCESS) {
	  ilRc = setup_config();
	}
	//TIME RANGE ,WHEN CHECK AFTTAB TO SELECT FLIGHTS
	ilRc = get_int("MAIN","TIME_RANGE_SELECT_FLIGHTS",&TimeRangeSelectFlights);
	if(ilRc != RC_SUCCESS)
	{
		TimeRangeSelectFlights=72;
		dbg(TRACE,"<Init_Process>,TIME_RANGE_SELECT_FLIGHTS not found in cfg ,use default value <%d>",TimeRangeSelectFlights);
		ilRc=RC_SUCCESS;
	}
	else
		dbg(TRACE,"<Init_Process>,TIME_RANGE_SELECT_FLIGHTS <%d>",TimeRangeSelectFlights);
	
	/* while (slWait); */

	if(ilRc == RC_SUCCESS) {
	  HandleLongTimerRequest(TRUE); 
	}

	ReadAlertConfig();

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}

	return(ilRc);
	
} /* end of initialize */
/******************************************************************************/
/* HandleLongTimerRequest                                                     */
/*                                                                            */
/* Triggered by ntisch every 10 minutes. Scan database for records with       */
/* ORG3 = HOPO and TIFD within a 10 minute time window 72 hours before TIFD.  */
/* If the ALC2 is configured, this function will make an entry in the timer   */
/* table for scheduling commands related to this record.                      */
/*                                                                            */
/******************************************************************************/
static int HandleLongTimerRequest(int ipInit) 
{
	char  clUtcTimeStart[15];
  char  clUtcTimeEnd[15];
  short slFkt = 0;
  short slCursor = 0;
  int   ilGetRc;
  int   ilRC = RC_SUCCESS;
  long  llURNO;
  char  *pclURNO;
  char  *pclALC2;
  char  *pclTIFD;
  char  *pclFLNO;
  char  clALC2[3];
  char  clTIFD[15];
  char  clFLNO[10];
  int   ilTimeOffset = 0;
  int   ilTimeOffsetEnd = 0;
  char  clErrBuff[128];
  char  pclSqlBuf[1024];

  if (ipInit == TRUE) {
    ilTimeOffset = TimeRangeSelectFlights * 3600;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeEnd);
    GetServerTimeStamp("UTC", 1, 0, clUtcTimeStart);
    dbg(TRACE,"Current UTC <%s>",clUtcTimeStart);
    char utc2localtime[15];
    strcpy(utc2localtime,clUtcTimeStart);
    UtcToLocalTimeFixTZ(utc2localtime); // convert to local 
    dbg(TRACE,"Current Local Time <%s>",utc2localtime);
  }
  else {
    ilTimeOffset = TimeRangeSelectFlights * 3600 + 120;  /* give me 2 minutes of preparation please */ 
    ilTimeOffsetEnd = 60 * igTimeWindow;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeStart);
    ilTimeOffset = ilTimeOffset + ilTimeOffsetEnd;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeEnd);
  }

  sprintf (pclSqlBuf,"SELECT URNO,ALC2,TIFD,FLNO FROM AFTTAB WHERE TIFD BETWEEN '%s' AND '%s' AND ORG3='%s' AND ADID='D'" 
	   , clUtcTimeStart, clUtcTimeEnd, cgHopo);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pcgDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(pcgDataArea, NULL, 4, ",");
      pclURNO = strtok(pcgDataArea, ",");
      if (pclURNO == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No URNO -- data skipped");
	continue;
      }
      llURNO = atol(pclURNO);
      /* ALC2 */
      pclALC2 = strtok(NULL, ",");
      if (pclALC2 == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No ALC2 -- data skipped");
	continue;
      }
      strcpy(clALC2, pclALC2);
      /* TIFD */
      pclTIFD = strtok(NULL, ",");
      if (pclTIFD == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No TIFD -- data skipped");
	continue;
      }
      strcpy(clTIFD, pclTIFD);
      /* FLNO */
      pclFLNO = strtok(NULL, ",");
      if (pclFLNO == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No FLNO -- data skipped");
	continue;
      }
      strcpy(clFLNO, pclFLNO);
      addTimerEntry(llURNO, clALC2, clTIFD, clFLNO);
    }
    else {
      if (ilGetRc != NOTFOUND) {
	get_ora_err(ilGetRc, &clErrBuff[0]);
	dbg (TRACE, "<HandleLongTimerRequest> Error getting Oracle data error <%s>", &clErrBuff[0]);
	ilRC RC_FAIL;
	break;
      }
      else {
	ilRC = RC_NODATA;
	break;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);
  return ilRC;
}
static int ReadConfigEntryRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}
static void ReadAlertConfig()
{
    int ilRC = RC_SUCCESS;
    int i = 0;
    char clSectionName[20] = "\0";

    ilRC = ReadConfigEntryRow("MAIN","ALERT_SECTIONS",cgAlertSections);
    igAlertCount = GetNoOfElements(cgAlertSections,',');
    dbg(TRACE,"%05d: igAlertCount: <%d>",__LINE__,igAlertCount);
    for (i=1 ; i<= igAlertCount; i++)
    {
	GetDataItem(clSectionName,cgAlertSections,i,',',"","");
	dbg(TRACE,"clSectionName: <%s>",clSectionName);

	ilRC = ReadConfigEntryRow(clSectionName,"ERROR",cgErrors[i-1]);
	ilRC = ReadConfigEntryRow(clSectionName,"ALERT",cgAlerts[i-1]);
	dbg(DEBUG,"ERROR <%s>",cgErrors[i-1]);
	dbg(DEBUG,"ALERT <%s>",cgAlerts[i-1]);

    }
    ilRC = ReadConfigEntry("MAIN","SEND_ERRORS_TO_FDI",cgErrorsToFdi);
}

/******************************************************************************/
/* NOW read available entries and update config                            */
/******************************************************************************/

static int setup_config()
{
  int ilRc;
  char clValue[1024];
  char clSection[32];
  char clField[128];
  char clFieldList[1024];
  char clDataList[1024];

  if (myRequestCmdList != NULL) {
    deleteItemList(myRequestCmdList);
    myRequestCmdList = NULL;
  }

  sprintf(clSection, "%s", "MAIN");
  sprintf(clField, "%s", "REQUEST_CMD");

  ilRc =  ReadConfigEntry(clSection,  clField, clValue);
  if (ilRc<0) {
    /*
    ** no entry or missing section
    */
    if (ilRc == E_PARAM) {
      dbg(TRACE,"<setup_config> Unknown Parameter <%s>", clField);
      return RC_FAIL;
    }
    else if (ilRc == E_SECTION) {
      dbg(TRACE,"<setup_config> Unknown Section <%s>", clSection);
      return RC_FAIL;
    }
  }
  strcpy(clFieldList,clValue);
  strcpy(clDataList,clValue);

  createItemList(clFieldList, &myRequestCmdList);
  addDataItemToList(clDataList, &myRequestCmdList, 0, ',', IS_VALID);
}
/******************************************************************************/
/* addDataItemToList  	                                                      */
/*                                                                            */
/* Add new items to the list, allocate required memory, the field list needs  */
/* already exist. The list of data has to correspond to the field list        */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the data items        */
/*        hook       -- pointer to list where the element is to add           */
/*        entry      -- the number of the related leg                         */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus)
{
  char *pclElement;      /* pointer to start of element */
  char *pclActElement;   /* pointer to actual element   */
  uint ilLenOfStr;
  uint ilMaxLen;
  uint ilLastElem = FALSE;
  uint ilNumItems = 0;
  ListPtr pclPtr;
  ListPtr pclRemPtr;
//createItemList(pcpFields, &pcgItemList);
 // addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);
  pclElement = list;
  pclPtr = *hook;
  //dbg(DEBUG,"addDataItemToList: list <%s>",list);
  while (*pclElement != NULL && pclPtr != NULL) {
    pclActElement = getNextItem(pclElement, &delimiter, &ilLenOfStr, &ilLastElem);
    //dbg(DEBUG,"addDataItemToList: pclActElement <%s>",pclActElement);
    if (ilLastElem == FALSE) {
      pclElement = pclElement + ilLenOfStr + 1;
    }
    else {
      pclElement = pclElement + ilLenOfStr;
    }
    if (ilLenOfStr != 0) {
      ilMaxLen = ilLenOfStr + 1;
      pclPtr->dobj[entry].data = (char *)malloc(ilMaxLen*sizeof(char));
      pclPtr->dobj[entry].len = ilMaxLen;
      pclPtr->dobj[entry].status = defaultStatus;
      strncpy(pclPtr->dobj[entry].data,pclActElement,ilMaxLen-1);
      pclPtr->dobj[entry].data[ilLenOfStr] = '\0';
      ilNumItems++;
      pclPtr = pclPtr->next;
    }
    else {
      pclRemPtr = pclPtr;
      pclPtr = pclPtr->next;
      removeItemFromList(pclRemPtr);
    }
    
  }
  dbg(DEBUG,"addDataItemToList: Added <%d> items to list", ilNumItems);
}  
/******************************************************************************/
/* removeItemFromList	                                                      */
/*                                                                            */
/* Remove an element from the list and free the allocated storage             */
/*                                                                            */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int removeItemFromList (ListPtr hook)
{
  int i;

  if (hook->next != NULL) {
    hook->prev->next = hook->next;
  }
  else {
    hook->prev->next = NULL;
  }
  if (hook->next != NULL) {
    hook->next->prev = hook->prev;
  }
  dbg(TRACE,"removeItemFromList: removing %s",hook->field);
  free(hook->field);
  for (i=0; i<MAX_NUM_LEGS; i++) {
    if (hook->dobj[i].data != NULL) {
      free(hook->dobj[i].data);
    }
  }
  free(hook);
}
/******************************************************************************/
/* getNextItem                                                                */
/*                                                                            */
/* Helper function to fetch the next item from a ',' separated list. If the   */
/* last item was fetched, ipLast will be set to TRUE.                         */
/*                                                                            */
/* Input:                                                                     */
/*      cpSource -- pointer to the item list                                  */
/*      cpDelim  -- separator character (something like ',')                  */
/*                                                                            */
/* Return:                                                                    */
/*      ipLen    -- len of the found item                                     */
/*      ipLast   -- TRUE, if the last item was fetched                        */
/*      returns a pointer to the item (isn't '\0' terminated, so use ipLen)   */
/*                                                                            */
/******************************************************************************/
static char *getNextItem(char *cpSource, char *cpDelim, uint *ipLen, uint *ipLast)
{
  char *clStart;

  if (cpSource == NULL) {
    *ipLen = 0;
    return NULL;
  }
  clStart = cpSource;
  while (*cpSource != NULL && *cpSource != *cpDelim) {
    cpSource++;
  }
  *ipLen = cpSource-clStart;
  if (*cpSource == NULL) *ipLast = TRUE;
  else *ipLast = FALSE;
  return clStart;
}

/******************************************************************************/
/* createItemList  	                                                      */
/*                                                                            */
/* Add new field items to the list, scan the item list and create a new entry */
/* for each item.                                                             */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the field items       */
/*        hook       -- pointer to list where the element is to add           */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int createItemList(char *list, ListPtr *hook)
{
  char *pclElement;
  
  if (strlen(list) == 0) return RC_FAIL;
  if ((pclElement=strtok(list, ","))==NULL) {
    addItemToList(pclElement, hook);
    return ;
  }
  addItemToList(pclElement, hook);
  while ((pclElement = strtok(NULL, ",")) != NULL) {
    addItemToList(pclElement, hook);
  }
}
/******************************************************************************/
/* deleteItemList	                                                      */
/*                                                                            */
/* Delete the entire item list and free all storage.                          */
/* This function is called recursively!                                       */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                              */
/*                                                                            */
/******************************************************************************/
int deleteItemList(ListPtr hook)
{
  int i;
  if (hook != NULL)
    {
      deleteItemList(hook->next);
      free(hook->field);
      for (i=0; i<MAX_NUM_LEGS; i++) {
	if (hook->dobj[i].data != NULL) {
	  free(hook->dobj[i].data);
	}
      }
      free(hook);
    }
}
/*
** Fetch an integer value from cfg
**
** Input:	Section -- name of the related section 
**		field	-- name of the entry
**		entry	-- entry to a specific interface table
**
** Output:	None
**
** Returns:	value contains the fetched value if rtc == RC_SUCCESS
**		rtc contains the message provided from the ReadConfigEntry
**		function, i.e E_SECTION or E_PARAM in case of error
**
** Description: 
**	Reads a value from impman.cfg. 
** 	If the requested section was not found an error message is printed. 
** 	If the requested field was not found, this is silently ignored.
**	If a value between 0 and 32000 is returned, an appropriate value is 
**	written to the config (the value is no longer treated as default,
**	even if we have read a value that equals the default value!).
**	If the value is not in range, an error message is printed
*/
static int get_int(char *Section, char *field, int *data)
{
  int rtc = RC_SUCCESS;
  char value[1024];
  int intval;

  rtc = ReadConfigEntry(Section, field, value);
  if (rtc<0) {
    /*
    ** no entry or missing section
    */
    if (rtc == E_PARAM) return rtc;	/* this parameter wasn't found in cfg -- keep default */
    else if (rtc == E_SECTION) {
      dbg(TRACE,"<get_int> Section %s not found in impman.cfg !", Section);
      return rtc;
    }
  }
  intval = atoi(value); /* Note: using atoi without prechecking the string maybe dangeroes! */
  if (intval > 0 && intval <MAX_INT_PARAMETER) { 
    *data = intval;
    return RC_SUCCESS;
  }
  else {
    *data = -1;
    dbg(TRACE,"<get_int> Illegal Value %s for field %s in Section %s !", value, field, Section);
    return RC_FAIL;
  }
}

 static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */
 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{

    dbg(TRACE,"Terminate: now DB logoff ...");

		signal(SIGCHLD,SIG_IGN);

		logoff();

		dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


		dbg(TRACE,"Terminate: now leaving ...");

		fclose(outp);

		sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */
 
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate(1);
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */
 
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */
 
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
 
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_Process();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_dummy: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    
 
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	if (strcmp(prlCmdblk->command,"TIM")) { /* supress info for timer message */}
	  dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	  
	  /****************************************/
	  DebugPrintBchead(DEBUG,prlBchead);
	  DebugPrintCmdblk(DEBUG,prlCmdblk);
	  dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	  dbg(TRACE,"originator follows event = %p ",prpEvent);
	  
	  dbg(TRACE,"originator<%d>",prpEvent->originator);
	  dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	  dbg(TRACE,"selection <%s>",pclSelection);
	  dbg(TRACE,"fields    <%s>",pclFields);
	  dbg(TRACE,"data(first 1000chars)      <%.1000s>",pclData);
	  /****************************************/
	
	
	HandleData_User(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);


	if (strcmp(prlCmdblk->command,"TIM")) { /* supress info for timer message */}
	  dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	

	return(RC_SUCCESS);
    
} /* end of HandleData */
/******************************************************************************/
/* HandleData_User	                                                      */
/*									      */
/* Handle process specific data						      */
/*									      */
/******************************************************************************/
static int HandleData_User(BC_HEAD *prlBchead,
			   CMDBLK *prlCmdblk,
			   char *pclSelection,
			   char *pclFields,
			   char *pclData)
{
	uint ilDataID = 0;
  int  ilRC;

  strcpy  (pcgActCmd,prlCmdblk->command);
  memset  (pcgRecvName, '\0', (sizeof(prlBchead->recv_name) + 1)); 
  strncpy (pcgRecvName, prlBchead->recv_name, sizeof(prlBchead->recv_name));
    
  /* User */
  memset  (pcgDestName, '\0', sizeof(prlBchead->recv_name)) ; 
  strcpy  (pcgDestName, prlBchead->dest_name) ;   /* UserLoginName */
  sprintf (pcgChkWks, ",%s,", pcgRecvName) ; 
  sprintf (pcgChkUsr, ",%s,", pcgDestName) ; 
    
  /* SaveGlobal Info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
  strcpy (pcgTwEnd, prlCmdblk->tw_end) ; 
    
  strcpy (pcgActCmd,prlCmdblk->command);
  
  if (!strcmp(pcgActCmd, "TIM")) { /* 1 min timer, perform required actions */
    HandleShortTimerRequest();
  }
  else if (!strcmp(pcgActCmd, "TIML")) { /* 10 min timer */
    HandleLongTimerRequest(FALSE);
  }
  else if (!strcmp(pcgActCmd, "CLUP")) { /* daily cleanup */
    HandleCleanUp();
  }
  else if (!strcmp(pcgActCmd, "DFR")) { /* record deletion */
    HandleDeleteFlightTimer(pclFields, pclData);
  }
  else if (!strcmp(pcgActCmd, "UFR")) { /* update and possibly event */
    HandleUpdateFlightTimer(pclFields, pclData);
  }
  else if (!strcmp(pcgActCmd, "IFR")) { /* new flight */
    HandleInsertFlightTimer(pclFields, pclData);
  }
  //else if (!strcmp(pcgActCmd, "KRI")) { /* kriscom response */
    //HandleKriscomResponse(pclData, pclSelection, pcgTwStart);
  //}
  else if (!strcmp(pcgActCmd, "ALT")) { /* Altea response */
    HandleAlteaResponse(pclData, pclSelection, pcgTwStart);
  }
  else if (!strcmp(pcgActCmd, "KER")) { /* Altea response */
    //HandleKriscomError(pclData, pclSelection, pcgTwStart);
  }
  else if (!strcmp(pcgActCmd, "CAN")) { /* cancel timer */
    //setTimerState(pclData, pclSelection,TRUE);
  }
  else if (!strcmp(pcgActCmd, "RES")) { /* resume timer */
    //setTimerState(pclData, pclSelection,FALSE);
  }
  else if (!strcmp(pcgActCmd, "EXRQ")) { /* external request */
    HandleExternalRequest(pclData, pclSelection);
  }
  else {
    dbg(TRACE,"<HandleData_User> Unknown Command <%s>", pcgActCmd);
    return RC_FAIL;
  }
}
/******************************************************************************/
/* HandleExternalRequest                                                      */
/*                                                                            */
/* Handle external request command.                                           */ 
/*                                                                            */
/* Input:                                                                     */
/*       pcpCmd      -- Command to be sent                                    */
/*       pcpUrno -- URNO of requested flight                                  */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
HandleExternalRequest(char *pcpCmd, char *pcpUrno)
{
  long llHASIDENT;
  long llUrno;
  long llCFUrno;
  int  ilRC;
  char *pclGrpTyp;
  char *pclCFUrno;
  char *pclALC2;
  char *pclFLNO;
  char *pclACTTOD;
  char clALC2[3];
  char clFLNO[10];
  char clACTTOD[15];

  dbg(DEBUG, "<HandleExternalRequest> Received command: <%s> Urno: <%s>", pcpCmd, pcpUrno);

  llUrno = atol(pcpUrno);

  sprintf(pcgSqlBuf, "SELECT ALC2,FLNO,TIFD FROM AFTTAB WHERE URNO=%ld", llUrno);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_FAIL) {
    dbg(TRACE, "<HandleExternalRequest> SQL-statement failed <%s>", pcgSqlBuf);
  }
  else {
    BuildItemBuffer(pcgDataArea, NULL, 3, ";");
    pclALC2 = strtok(pcgDataArea, ";");
    pclFLNO = strtok(NULL, ";");
    pclACTTOD = strtok(NULL, ";");
  }

  if (pclALC2 == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no ALC2 available");
    return RC_FAIL;
  }
  else if (pclFLNO == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no FLNO available");
    return RC_FAIL;
  }
  else if (pclACTTOD == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no TIFD available");
    return RC_FAIL;
  }

  strcpy(clALC2, pclALC2);
  strcpy(clFLNO, pclFLNO);
  strcpy(clACTTOD, pclACTTOD);

  
 
  //sendKriscomCommand(0, pcpCmd, pclGrpTyp, clFLNO, clACTTOD, pcpUrno, "HASCONF", llCFUrno);
  sendAlteaCommand( 0, pcpCmd, clFLNO, clACTTOD, pcpUrno, "HASCONF",clALC2);
}
/******************************************************************************/
/* HandleAlteaResponse                                                      */
/*                                                                            */
/* Receives data from the Altea system and prepares sending to FDIHDL.      */
/* Timer status is set to 'ACTIVE' if there are remaining request. If there    */
/* is not a remaining request, this record will be removed.                   */ 
/*                                                                            */
/* Input:                                                                     */
/*       pcpData  -- data from Altea                                        */
/*       pcpIdent -- ID of the timer record                                   */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
void HandleAlteaResponse(char *pcpData, char *pcpSelection, char *pcpIdent)
{
	dbg(TRACE, "HandleAlteaResponse");
	dbg(TRACE, "<HandleAlteaResponse> Received data: <%.1000s> Ident: <%s>",pcpData, pcpIdent);
	dbg(TRACE, "<HandleAlteaResponse> pcpSelection:  <%s>",pcpSelection);
	
	char *clId;
  char *clUrno;
  char *clCommand;
  char *clPtr,*clPtr1;
  char *enId;
  char *pclTmp;
  char *pclRealAlertFlight;

  long ilWalId=0;
  char  pclFields[1024];
  char  pclValues[64000]; /* This is the buffer to contain the screen data */
  char  pclSelection[100];
  char  pclActCmd[16];
  char  pclTwStart[64];
  int   ilRC = RC_SUCCESS;
  int   ilPrio=4;
  long  ilEnId=-1;
  char  clOSRQ[129];
  char  clSearch[15];
  int   ilFound = FALSE;
  int   ilRightComma = FALSE;
  int   ilLeftComma = FALSE;
  int   ilEmbedComma = FALSE;
  int   i = 0;
  char clAlertFlight[100] = "\0";
  char clMyAlert[256] = "\0";

  //dbg(DEBUG, "<HandleAlteaResponse> Received data: <%s> Ident: <%s>",pcpData, pcpIdent);
  
  //1.check pacData,whether is error msg
  /*
  dbg(TRACE,"Now get FLNO, DATE, COMMAND for alert-message");
  GetDataItem(clAlertFlight,pcpSelection,10,';',"","");
  pclRealAlertFlight = strstr(pcpSelection,clAlertFlight);
  dbg(TRACE,"FLNO, DATE, COMMAND for alert-message found <%s>",pclRealAlertFlight);

  for (i=1 ; i<= igAlertCount; i++)
  {
      if (strstr(pcpData,cgErrors[i-1]) != NULL)
      {
	  sprintf(clMyAlert,"%s : %s",pclRealAlertFlight,cgAlerts[i-1]);
	  alert(clMyAlert);
	  dbg(TRACE,"<HandleKriscomResponse> Fatal -- Error received: <%s>, sended alert: <%s>",cgErrors[i-1],cgAlerts[i-1]);
	  if ((strcmp(cgErrorsToFdi,"NO") == 0) || (strcmp(cgErrorsToFdi,"no") == 0))
	  {
	      return;
	  }
      }
  }
  */
  
  //2.get walid from TwStart
  clId = strstr(pcpIdent, ",");
  if (clId != NULL) {
    clId++;
    ilWalId = atol(clId);
  }

  enId = strstr(clId, ",");
  if (enId != NULL) {
    enId++;
    ilEnId = atol(enId);
    dbg(DEBUG, "<HandleAlteaResponse> Timer request <%s>", enId);
  }
  dbg(DEBUG, "<HandleAlteaResponse> WalIdent: <%ld>", ilWalId);

  //clUrno = strtok(pcpSelection, ";");
  //if (clUrno == NULL) {
    //dbg(TRACE, "<HandleAlteaResponse> Fatal -- no URNO, skipped response");
    //return;
 // }
  
  clCommand = strtok(pcpSelection, ";");
  if (clCommand == NULL) {
    dbg(TRACE, "<HandleAlteaResponse> Fatal -- no Command, skipped response");
    return;
  }
  
  /*
  if (ilEnId != 0 && ilEnId != -1) {
    sprintf(pcgSqlBuf,"UPDATE SEVTAB SET STAT='ACTIVE' WHERE UHAT=%ld", clUrno);
    ilRC = getOrPutDBData(START|COMMIT);
  }
  */
  
  //3.Send received data to FDIHDL
  strcpy(pclActCmd, "FDI");
  strcpy(pclSelection, "TELEX,7");
  sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);

  sprintf(pclFields, "");
  sprintf(pclValues, "<=SOURCE=>ALTEA<=/SOURCE=>\n<=COMMAND=>%s<=/COMMAND=>\n<=URNO=>%s<=/URNO=>\n<=SCREENCONTENT=>\n%s\n<=/SCREENCONTENT=>",clCommand, clUrno, pcpData);
  ilRC = SendCedaEvent(igFdiId, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
		       pclActCmd, "", pclSelection,
		       pclFields, pclValues, "", ilPrio, RC_SUCCESS) ;
  
  if(ilRC==RC_SUCCESS)
		dbg(DEBUG, "<HandleAlteaResponse> \n ****** Send command report ****** \nSending command: <%s> to <%d>\n ****** End send command report ****** ", pclSelection, igFdiId); 
	else
		dbg(DEBUG,"<HandleAlteaResponse> unable send command:<%s> to <%d> ,AEVTAB record:<%d>",pclSelection,igFdiId,ilWalId);
	
}
/******************************************************************************/
/* HandleShortTimerRequest                                                    */
/*                                                                            */
/* Called every minute. Checks for some work: if a timer has expired, i.e = 0 */
/* then send the required command. If there are more commands to perform,     */
/* setup the new timer value, if not, remove that entry. Furthermore:         */
/* decrement all timers from the table that are <> 0                          */
/*                                                                            */
/******************************************************************************/
static void  HandleShortTimerRequest() 
{
  int ilRC;
	//TIMR=0,means need send request at this moment
  ilRC =  performTimerOrEventAction("TIMR<=0 AND ATIM<>' ' ORDER BY PRIO", FALSE);
  sprintf(pcgSqlBuf,"UPDATE AEVTAB SET TIMR=TIMR-1 WHERE TIMR<>0");
  ilRC = getOrPutDBData(START|COMMIT);
}

/******************************************************************************/
/* performTimerOrEventAction                                                  */
/*                                                                            */
/* This function will figure out which commands are to be performed due to    */
/* expired timers or occured events.                                          */
/*                                                                            */
/* Input:                                                                     */
/*       pcpSelection -- search selection from caller                         */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
int performTimerOrEventAction(char *pcpSelection, int ipIsEvent)
{
	
  int ilRC;
  int ilGetRc;
  short slFkt = 0;
  short slCursor = 0;
  char *pclURNO;
  //char *pclCFURNO;
  long llCFURNO;
  long llURNO;
  long myWAID;
  //char *pclHasId;
  //long llHasId;
  char *pclActCmd;
  //char *pclGrpTyp;
  char *pclFLNO;
  char *pclACTTOD;
  char *pclHRList;
  char *pclMinList;
  char *pclActTimer;
  char *pclALC2;
  char *pclWAID;
  char *pclEVNT;
  char *pclTIMR;
  //char *pclEvent;
  char clErrBuff[128];
  char clDate[15];
  char pclSqlBuf[1024];
  char pclDataArea[1024];
  char myTIMR[10]="\0";
  
  char  myACTTOD[20];

  sprintf (pclSqlBuf,"SELECT UAFT,ACMD,FLNO,ATOD,TIHR,TIMI,ATIM,ALC2,WAID,EVNT,TIMR FROM AEVTAB WHERE %s", pcpSelection);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
    	dbg(TRACE,"<performTimerOrEventAction> DB Data <%s>",pclDataArea);
      BuildItemBuffer(pclDataArea, NULL, 11, ";");
      pclURNO = strtok(pclDataArea, ";");
      if (pclURNO == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No URNO -- data skipped");
				slFkt = NEXT;
				continue;
      }
      else llURNO = atol(pclURNO);

      pclActCmd = strtok(NULL, ";");
      if (pclActCmd == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No Command -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      pclFLNO = strtok(NULL, ";");
      if (pclFLNO  == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No FLNO -- data skipped");
				slFkt = NEXT;	
				continue;
      }
      
      pclACTTOD = strtok(NULL, ";");
      if (pclACTTOD == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No ACTTOD -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      pclHRList = strtok(NULL, ";");
      if (pclHRList == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No Hour List -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      pclMinList = strtok(NULL, ";");
      if (pclMinList == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No Minute List -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      pclActTimer = strtok(NULL, ";");
      if (pclActTimer == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No ActTimer -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      
      pclALC2 = strtok(NULL, ";");
      if (pclALC2 == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No ALC2 -- data skipped");
				slFkt = NEXT;
				continue;
      }
      
      pclWAID = strtok(NULL, ";");
      if (pclWAID == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No WAID -- data skipped");
				slFkt = NEXT;
				continue;
			}
			else
      	myWAID = atol(pclWAID);
				
			pclEVNT = strtok(NULL, ";");
      if (pclEVNT == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No EVNT -- data skipped");
				slFkt = NEXT;
				continue;
			}
			
			pclTIMR = strtok(NULL, ";");
      if (pclEVNT == NULL) {
				dbg(TRACE, "<HandleShortTimerRequest> No TIMR -- data skipped");
				slFkt = NEXT;
				continue;
			}
      //strcpy(myTIMR,pclActTimer);
      int mytimr=atoi(pclTIMR);
      
      
      //sendKriscomCommand(llHasId, pclActCmd, pclGrpTyp, pclFLNO, pclACTTOD, pclURNO, pclActTimer, llCFURNO);
      dbg(TRACE,"<performTimerOrEventAction> Try to sendAlteaCommand");
      
      //sendAlteaCommand need change TIFD to local time,so pass a copy to it
  		strcpy(myACTTOD,pclACTTOD);
  		myACTTOD[strlen(pclACTTOD)]='\0';
      sendAlteaCommand( myWAID, pclActCmd, pclFLNO, myACTTOD, pclURNO, pclActTimer,pclALC2);

      ilRC = updateTimerEntry(pclHRList, pclMinList, pclACTTOD ,myWAID ,pclEVNT ,ipIsEvent ,mytimr);
  	}
    else {
      if (ilGetRc != NOTFOUND) {
				get_ora_err(ilGetRc, &clErrBuff[0]);
				dbg (TRACE, "<HandleShortTimerRequest> Error getting Oracle data error <%s>", &clErrBuff[0]);
				ilRC = RC_FAIL;
				break;
      }
      else {
				ilRC = RC_NODATA;
				break;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);

      
}
/******************************************************************************/
/*  updateTimerEntry                                                          */
/*                                                                            */
/* The timer record is updated to the next valid entry. If the record is      */
/* expired, it will be removed.                                               */
/*                                                                            */
/* Input:                                                                     */
/*       pcpHRList  -- list of valid timers (in hours)                        */
/*       pcpMinList -- list of valid timers (in minutes)                      */
/*       pcpACTTOD  -- actual time of day                                     */
/*       myWAID     -- ID of the record we are working on                     */
/*       ipIsEvent  -- event like AIR or OFBL,means aircraft depatured                                                                    */
/*                                                                            */
/******************************************************************************/
int updateTimerEntry(char *pcpHRList, char *pcpMinList, char *pcpACTTOD, long myWAID, char *pcpEvent, int ipIsEvent, int myTIMR)
{
	int  ilType;
	int  ilTime;
  int  ilTimeFact;
  int  ilPrio;
  char *clFirstTimer;
	char *pclNextTimLst;
	char  clTimerHR[200]="\0";
  char  clTimerMin[200]="\0";
  char clUtcTimeStart[15]="\0";
  int  ilDiffMin;
  int  ilTimeOffset;
  char clTimerData[16]="\0";
  int  ilRC;
  int  ilHaveEvent = FALSE;
  char clEvent[11]="\0";
  int  ilActDay,ilActMin,ilActWkDy;
	
	strcpy(clTimerHR, " ");
  strcpy(clTimerMin, " ");
  
	//if (strcmp(pcpEvent, " ")) ilHaveEvent = TRUE;
	//0. if event ,delete record
	#if 1
	if(ipIsEvent==TRUE) {
		//sprintf(pcgSqlBuf,"DELETE FROM AEVTAB WHERE WAID='%ld'", myWAID);
		sprintf(pcgSqlBuf, "UPDATE AEVTAB SET TIMR='0',TIHR=' ',TIMI=' ',PRIO='5',STAT='DELETE',EVNT='%s' WHERE WAID='%d'",
	    clEvent, myWAID);
    ilRC = getOrPutDBData(START|COMMIT);
    if(ilRC==RC_SUCCESS) {
    	dbg(DEBUG, "<updateTimerEntry> Receive event ,Delete record in AEVTAB <%d> ", myWAID);
    	return TRUE;
    }
    else
    	dbg(DEBUG, "<updateTimerEntry> Receive event ,unable delete record in AEVTAB <%d> ilRC <%d>", myWAID,ilRC);
    return FALSE;
  }
  #endif
	//1. check hours list
	dbg(TRACE, "<updateTimerEntry> pcpHRList <%s> pcpMinList <%s> pcpACTTOD <%s> WAID <%d> TIMR<%d>", pcpHRList,pcpMinList,pcpACTTOD,myWAID,myTIMR);
  clFirstTimer = getFirstTimerEntry(pcpHRList, &ilType, &pclNextTimLst);
  if (ilType == TIM) {
  	if (pclNextTimLst != NULL) 
  		strcpy(clTimerHR,pclNextTimLst);
  	strcpy(clTimerMin,pcpMinList);
    ilTime = atoi(clFirstTimer);
    strcat(clFirstTimer,"H");
    ilPrio = 1;
    ilTimeFact = 60;
  }
  //2. no entry in hours list, then check minutes list
  if (ilType == NONE) {
    clFirstTimer = getFirstTimerEntry(pcpMinList, &ilType, &pclNextTimLst);
    if (ilType == TIM) {
    	if (pclNextTimLst != NULL) 
    		strcpy(clTimerMin,pclNextTimLst);
      ilTime = atoi(clFirstTimer);
      strcat(clFirstTimer,"M");
      ilTimeFact = 1;
      ilPrio = 5;
    }
    //3. no entry in hours and minutes list, then delete this record of AEVTAB
    if (ilType == NONE  && myTIMR==0) {
      
      // we didn't find further entries 
      // so we assume that this record has completed
      // remove the acttimer  
      
      sprintf(pcgSqlBuf,"UPDATE AEVTAB SET STAT='DELETE' WHERE WAID='%ld'", myWAID);
      dbg(DEBUG, "<updateTimerEntry> Set state DELETE record <%ld> ", myWAID);
      ilRC = getOrPutDBData(START|COMMIT);
      return TRUE;
    }
  }
  
  dbg(TRACE, "<updateTimerEntry> Timer Entry <%s> ", clFirstTimer);

  //4. update AEVTAB
  if (ilType == TIM) {
    ilTimeOffset = ilTime * ilTimeFact;
    dbg(TRACE, "<updateTimerEntry> ilTimeOffset <%ld> ", ilTimeOffset);
    GetServerTimeStamp("UTC", 1, 0, clUtcTimeStart);
    DateDiffToMin(pcpACTTOD, clUtcTimeStart, &ilDiffMin);
    dbg(TRACE, "<updateTimerEntry> ilDiffMin <%ld> ", ilDiffMin);
    ilTime = ilDiffMin-ilTimeOffset;
    if (ilTime < 0) ilTime = 0;
    if (clFirstTimer == NULL) {
      strcpy(clTimerData, " ");
      dbg(TRACE, "<updateTimerEntry> Warning setting ATIM to ' ' <%ld>, shouldn't happen", myWAID);
    }
    else strcpy(clTimerData, clFirstTimer);

    sprintf(pcgSqlBuf, "UPDATE AEVTAB SET TIMR='%d',TIHR='%s',TIMI='%s',PRIO='%d',ATIM='%s' WHERE WAID='%d'",
	    ilTime, clTimerHR, clTimerMin, ilPrio, clTimerData, myWAID);
    dbg(DEBUG, "<updateTimerEntry> Sql statement: <%s>", pcgSqlBuf);
    ilRC = getOrPutDBData(START|COMMIT);
    	if (ilRC != RC_SUCCESS) {
    	dbg(TRACE, "<updateTimerEntry> Unable to perform <%s>", pcgSqlBuf);  
    	return FALSE;
  	}
  }

  return TRUE;
}
/******************************************************************************/
/* sendAlteaCommand                                                         */
/*                                                                            */
/* Send a request to the Altea system. Mark the record as 'RQPENDING'. The  */
/* sending context depends on the type of the group the request is dedicated  */
/* to.                                                                        */
/*                                                                            */
/* Input:                                                                     */
/*       myWAID     -- the id of the record																				                              */
/*       pcpActCmd   -- the command to be send                                */
/*       pcpFLNO     -- the flight number, is used for selection              */
/*       pcpACTTOD   -- actual time of depature, used to build the selection       */
/*       pclActTimer -- action time from hours or minutes list                                                                     */
/* Return:                                                                    */
/*       status of the send command                                           */
/*                                                                            */
/******************************************************************************/
int sendAlteaCommand( long myWAID,char *pcpActCmd,  char *pcpFLNO, char *pcpACTTOD, char *pcpURNO, char *pclActTimer, char *pclALC2)
{
	
	//char  clUser[32];
	char  pclActCmd[16];
	char  clDate[6];
	char  clDay[3];
  char  clMonth[3];
	long  llURNO=0;
	int   ilMonth;
	int   ilRC = RC_SUCCESS;
	
	int   ilReceiver=0;
	char  pclSelection[200];
	char  pclFields[1024];
  char  pclValues[1024];
  char  pclTwStart[64];
  
  char myVIAN[10]="\0";
	char myVIAL[1024] = "\0";
	char myVIA_DESN_AIRPORT[50]="\0";
	char myDES3[5]="\0";
	
	char *myVIANptr;
	char *myVIALptr;
	char *myDES3ptr;
  //char  *myALC2;
  char  *myFLNO;
  
	llURNO = atol(pcpURNO);
  strcpy(pclActCmd, "ALT");
  sprintf(pcgTwEnd, "%s;%s;%s", cgHopo, cgTabEnd, cgProcessName);
  //strcpy(clUser, "SQ");
  
  UtcToLocalTimeFixTZ(pcpACTTOD); // convert to local 
  dbg(TRACE,"<sendAlteaCommand> pcpACTTOD <%s>",pcpACTTOD);
  //sprintf(pclSelection, "%ld;%s;%s;%s;%s;%s;%s;%s;%s;%s %s %s",
	  //llURNO, pcpActCmd, pclNPCmd, pclNPKey, pclNPCol, pclNPLine, pclLPKey, pclLPCol, pclLPLine, pcpFLNO, pcpACTTOD, pcpActCmd);
	
	//<943710619;SQ;490;20110716015000;SIN>
	if(pcpFLNO != NULL)
	{
		strtok(pcpFLNO, " ");
		myFLNO = strtok(NULL," ");
	}
	sprintf(pclSelection, "%s",pcpActCmd);
	//1.ilReceiver: from GRPT, it is queue id of walbridge or LS-wal
	ilReceiver = pcgGrp1Modid;
 
  //2,3 pcgDestName,pcgRecvName already defined in handle_usrdata()
  
  //4.pclTwStart: WAID+ATIM
  sprintf(pclTwStart,"walhdl,%d,%s", myWAID,pclActTimer);
  
  //5.pcgTwEnd
  //sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);
  
  //6.pclActCmd
  
  //7.pclSelection
  
  //8.pclFields: something like password or login logout id, no idea now
  sprintf(pclFields, " ");
  
  //9.pclValues: something like password or login logout id, no idea now
  //get destination and via airport
  int myVIAN_i=0;
  sprintf(pcgSqlBuf, "SELECT VIAN,VIAL,DES3 FROM AFTTAB WHERE URNO=%ld", llURNO);
    ilRC = getOrPutDBData(START);
    if (ilRC == RC_FAIL) {
			dbg(TRACE, "<sendAlteaCommand> SQL-statement failed,SELECT VIAN,VIAL,DES3 FROM AFTTAB WHERE URNO=%ld <%s> sqlbuf <>",llURNO, pcgSqlBuf);
    }
    else {
			BuildItemBuffer(pcgDataArea, NULL, 3, ";");
			myVIANptr = strtok(pcgDataArea, ";");
			strncpy(myVIAN,myVIANptr,1);
			myVIALptr = strtok(NULL, ";");
			strncpy(myVIAL,myVIALptr,strlen(myVIALptr));
			myDES3ptr = strtok(NULL, ";");
			strncpy(myDES3,myDES3ptr,3);
			if (myVIAN == NULL && myVIAL == NULL) 
			  dbg(TRACE, "<sendAlteaCommand> Unable to fetch VIAN or VIAL");
			else
			{
				dbg(TRACE, "<sendAlteaCommand> VIAN<%s>  VIAL<%s> DES3 <%s>",myVIAN,myVIAL,myDES3);
				myVIAN_i=atoi(myVIAN);
				int i;
				
				for(i=1;i<=myVIAN_i;i++)
				{

					switch(i)
					{
						case 1:
							myVIA_DESN_AIRPORT[0]=myVIAL[1];//1 via airport
							myVIA_DESN_AIRPORT[1]=myVIAL[2];
							myVIA_DESN_AIRPORT[2]=myVIAL[3];
						break;
						case 2:
							myVIA_DESN_AIRPORT[3]='-';
							myVIA_DESN_AIRPORT[4]=myVIAL[120+1];//2 via airport
							myVIA_DESN_AIRPORT[5]=myVIAL[120+2];
							myVIA_DESN_AIRPORT[6]=myVIAL[120+3];
						break;
						case 3:
							myVIA_DESN_AIRPORT[7]='-';
							myVIA_DESN_AIRPORT[8]=myVIAL[120*2+1];//3 via airport
							myVIA_DESN_AIRPORT[9]=myVIAL[120*2+2];
							myVIA_DESN_AIRPORT[10]=myVIAL[120*2+3];
						break;						
						case 4:
							myVIA_DESN_AIRPORT[11]='-';
							myVIA_DESN_AIRPORT[12]=myVIAL[120*3+1];//4 via airport
							myVIA_DESN_AIRPORT[13]=myVIAL[120*3+2];
							myVIA_DESN_AIRPORT[14]=myVIAL[120*3+3];
						break;						
						case 5:
							myVIA_DESN_AIRPORT[15]='-';
							myVIA_DESN_AIRPORT[16]=myVIAL[120*4+1];//5 via airport
							myVIA_DESN_AIRPORT[17]=myVIAL[120*4+2];
							myVIA_DESN_AIRPORT[18]=myVIAL[120*4+3];
						break;						
						case 6:
							myVIA_DESN_AIRPORT[19]='-';
							myVIA_DESN_AIRPORT[20]=myVIAL[120*5+1];//6 via airport
							myVIA_DESN_AIRPORT[21]=myVIAL[120*5+2];
							myVIA_DESN_AIRPORT[22]=myVIAL[120*5+3];
						break;						
						case 7:
							myVIA_DESN_AIRPORT[23]='-';
							myVIA_DESN_AIRPORT[24]=myVIAL[120*6+1];//7 via airport
							myVIA_DESN_AIRPORT[25]=myVIAL[120*6+2];
							myVIA_DESN_AIRPORT[26]=myVIAL[120*6+3];
						break;			
						case 8:
							myVIA_DESN_AIRPORT[27]='-';
							myVIA_DESN_AIRPORT[28]=myVIAL[120*7+1];//8 via airport
							myVIA_DESN_AIRPORT[29]=myVIAL[120*7+2];
							myVIA_DESN_AIRPORT[30]=myVIAL[120*7+3];
						break;
						case 9:
							myVIA_DESN_AIRPORT[31]='-';
							myVIA_DESN_AIRPORT[32]=myVIAL[120*8+1];//9 via airport
							myVIA_DESN_AIRPORT[33]=myVIAL[120*8+2];
							myVIA_DESN_AIRPORT[34]=myVIAL[120*8+3];
						break;			
					}			
				}
			}
    }
  if(myVIAN_i==0)
  	sprintf(pclValues,"%ld;%s;%s;%s;%s-%s",
	    llURNO, pclALC2, myFLNO, pcpACTTOD, cgHopo,myDES3);
  else
    sprintf(pclValues,"%ld;%s;%s;%s;%s-%s-%s",
	    llURNO, pclALC2, myFLNO, pcpACTTOD, cgHopo,myVIA_DESN_AIRPORT,myDES3);
  
  
  //10.ilPrio:
  int ilPrio = 5;
  
  dbg(TRACE,"<sendAlteaCommand> pclValues <%s>",pclValues);
  dbg(TRACE,"<sendAlteaCommand> pcgTwEnd <%s>",pcgTwEnd);
  dbg(TRACE,"<sendAlteaCommand> pclActCmd <%s>",pclActCmd);
  ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			 pclActCmd, "", pclSelection,
			 pclFields, pclValues, "", ilPrio, RC_SUCCESS) ;
  
	if(ilRC==RC_SUCCESS)
		dbg(DEBUG, "<sendAlteaCommand> \n ****** Send command report ****** \nSending command: <%s> to <%d>\n ****** End send command report ****** ", pclSelection, ilReceiver); 
	else
		dbg(DEBUG,"<sendAlteaCommand> unable send command:<%s> to <%d> ,AEVTAB record:<%d>",pclSelection,ilReceiver,myWAID);
	
}
int sendAlteaCommand_tmp( long myWAID,char *pcpActCmd,  char *pcpFLNO, char *pcpACTTOD, char *pcpURNO, char *pclActTimer, char *pclALC2,char *viades)
{
	
	//char  clUser[32];
	char  pclActCmd[16];
	char  clDate[6];
	char  clDay[3];
  char  clMonth[3];
	long  llURNO=0;
	int   ilMonth;
	int   ilRC = RC_SUCCESS;
	
	int   ilReceiver=0;
	char  pclSelection[200];
	char  pclFields[1024];
  char  pclValues[1024];
  char  pclTwStart[64];
  
  //char  *myALC2;
  char  *myFLNO;
  
	llURNO = atol(pcpURNO);
  strcpy(pclActCmd, "ALT");
  sprintf(pcgTwEnd, "%s;%s;%s", cgHopo, cgTabEnd, cgProcessName);
  //strcpy(clUser, "SQ");
  
  UtcToLocalTimeFixTZ(pcpACTTOD); // convert to local 
  dbg(TRACE,"<sendAlteaCommand_tmp> pcpACTTOD <%s>",pcpACTTOD);
  //sprintf(pclSelection, "%ld;%s;%s;%s;%s;%s;%s;%s;%s;%s %s %s",
	  //llURNO, pcpActCmd, pclNPCmd, pclNPKey, pclNPCol, pclNPLine, pclLPKey, pclLPCol, pclLPLine, pcpFLNO, pcpACTTOD, pcpActCmd);
	
	//<943710619;SQ;490;20110716015000;SIN>
	if(pcpFLNO != NULL)
	{
		strtok(pcpFLNO, " ");
		myFLNO = strtok(NULL," ");
	}
	sprintf(pclSelection, "%s",pcpActCmd);
	//1.ilReceiver: from GRPT, it is queue id of walbridge or LS-wal
	ilReceiver = pcgGrp1Modid;
 
  //2,3 pcgDestName,pcgRecvName already defined in handle_usrdata()
  
  //4.pclTwStart: WAID+ATIM
  sprintf(pclTwStart,"walhdl,%d,%s", myWAID,pclActTimer);
  
  //5.pcgTwEnd
  //sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);
  
  //6.pclActCmd
  
  //7.pclSelection
  
  //8.pclFields: something like password or login logout id, no idea now
  sprintf(pclFields, " ");
  
  //9.pclValues: something like password or login logout id, no idea now
  
  	sprintf(pclValues,"%ld;%s;%s;%s;%s",
	    llURNO, pclALC2, myFLNO, pcpACTTOD, viades);
  
  //10.ilPrio:
  int ilPrio = 5;
  
  dbg(TRACE,"<sendAlteaCommand_tmp> pclValues <%s>",pclValues);
  dbg(TRACE,"<sendAlteaCommand_tmp> pcgTwEnd <%s>",pcgTwEnd);
  dbg(TRACE,"<sendAlteaCommand_tmp> pclActCmd <%s>",pclActCmd);
  ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			 pclActCmd, "", pclSelection,
			 pclFields, pclValues, "", ilPrio, RC_SUCCESS) ;
  
	if(ilRC==RC_SUCCESS)
		dbg(DEBUG, "<sendAlteaCommand_tmp> \n ****** Send command report ****** \nSending command: <%s> to <%d>\n ****** End send command report ****** ", pclSelection, ilReceiver); 
	else
		dbg(DEBUG,"<sendAlteaCommand_tmp> unable send command:<%s> to <%d> ,AEVTAB record:<%d>",pclSelection,ilReceiver,myWAID);
	
}
/******************************************************************************/
/* HandleCleanUp                                                              */
/*                                                                            */
/* Remove unused records from SEVTAB (stat='DELETE')                          */
/*                                                                            */
/******************************************************************************/
static void HandleCleanUp()
{
  int  ilRC;
  int  ilActDay,ilActMin,ilActWkDy;
  int  ilDelTime;
  char clActTim[15];

  GetServerTimeStamp("UTC", 1, 0, clActTim);
  ilRC = GetFullDay(clActTim, (int *)&ilActDay, (int *)&ilActMin, (int *)&ilActWkDy);
  ilDelTime = ilActDay - igDelTimeIntrvl;
  sprintf(pcgSqlBuf, "DELETE FROM AEVTAB WHERE STAT='DELETE'");
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC  != RC_SUCCESS && ilRC != RC_NODATA) {
    dbg(TRACE, "<HandleCleanUp> Unable to delete AEVTAB entries with STAT='DELETE'");
  }
}
/******************************************************************************/
/* HandleInsertFlightTimer                                                    */
/*                                                                            */
/* Receiving an IFR, we have to check if this flight needs to be inserted     */
/* into the timer tab.                                                        */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleInsertFlightTimer(char *pcpFields, char *pcpData)
{
  char  *pclADID;
  char  *pclData;
  char  *pclALC2;
  char  *pclFLNO;
  int   ilTimer;
  int   ilRC = RC_SUCCESS;
  long  llURNO;
  int   ilDiffMin;
  char  clActTim[15];
  char  clNewTifd[15];
  char  clALC2[3];
  char  clFLNO[15];

  dbg(DEBUG, "<HandleInsertFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);
  pclADID = getItemDataByName(pcgItemList, "ADID", 0);
  if (pclADID != NULL) {
    if (strcmp(pclADID,"D")) {
      dbg(DEBUG, "<HandleInsertFlightTimer> Not a departure -- skipped");
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  }
  else
  {
  	dbg(DEBUG, "<HandleInsertFlightTimer> NO ADID -- skipped");
  	return;
  }
  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  else
  {
  	dbg(DEBUG, "<HandleInsertFlightTimer> NO URNO -- skipped");
  	return;
  }
  
  pclData = getItemDataByName(pcgItemList, "TIFD", 0);
  if (pclData != 0) {
    sprintf(clNewTifd, "%s", pclData);
  }
  else
  {
  	dbg(DEBUG, "<HandleInsertFlightTimer> NO TIFD -- skipped");
  	return;
  }
  /*
  ** Check, if we need to insert a new timer
  */
  GetServerTimeStamp("UTC", 1, 0, clActTim);
  DateDiffToMin(clNewTifd, clActTim, &ilDiffMin);
  //if (ilDiffMin <= 72*60) {
  if(ilDiffMin <= TimeRangeSelectFlights*60) {
  	sprintf(pcgSqlBuf, "SELECT ALC2,FLNO FROM AFTTAB WHERE URNO=%ld", llURNO);
    ilRC = getOrPutDBData(START);
    if (ilRC == RC_FAIL) {
			dbg(TRACE, "<HandleInsertFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
    }
    else {
			BuildItemBuffer(pcgDataArea, NULL, 2, ";");
			pclALC2 = strtok(pcgDataArea, ";");
			pclFLNO = strtok(NULL, ";");
			if (pclALC2 != NULL && pclFLNO != NULL) {
	  		strcpy(clALC2, pclALC2);
	  		strcpy(clFLNO, pclFLNO);
	  		addTimerEntry(llURNO, clALC2, clNewTifd, clFLNO);
			}
			else dbg(TRACE, "<HandleInsertFlightTimer> Unable to fetch ALC2 or FLNO");
    }
  
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* getItemDataByName	                                                      */
/*                                                                            */
/* Retrieve data from an item.                                                */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*        legLine  -- the data entry to use                                   */
/*                                                                            */
/* Return:                                                                    */
/*        returns a pointer to the data string or NULL if not found           */
/*        or invalid                                                          */
/*                                                                            */
/******************************************************************************/
static char *getItemDataByName(ListPtr hook, char *name, uint legLine)
{
  char *data;

  if (!strcmp(name,hook->field)) {
    if (hook->dobj[legLine].status & IS_VALID) return hook->dobj[legLine].data;
    else return NULL;
  }
  if (hook->next == NULL) return NULL;
  data = getItemDataByName(hook->next, name, legLine);
  return data;
}
/******************************************************************************/
/* HandleDeleteFlightTimer                                                    */
/*                                                                            */
/* Receiving a DFR results in deleting the timer request from AEVTAB,         */
/* using the UAFT field.                                                      */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              *
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleDeleteFlightTimer(char *pcpFields, char *pcpData)
{
  char *pclData;
  long llURNO;
  int  ilRC;

  dbg(DEBUG, "<HandleDeleteFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);

  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  else {
    dbg(TRACE, "<HandleDeleteFlightTimer> Failed to perform DELETE on record with missing URNO");
  }
  sprintf(pcgSqlBuf, "DELETE FROM AEVTAB WHERE UAFT=%ld", llURNO);
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC  != RC_SUCCESS && ilRC != RC_NODATA) {
    dbg(TRACE, "<HandleFlightData> Unable to delete AEVTAB entry with URNO=%ld", llURNO);
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* HandleUpdateFlightTimer                                                    */
/*                                                                            */
/* Receiving an UFR, we have to check if this flight needs to be inserted     */
/* into the timer tab. (If we receive an AIRB or OFFBL, this will create an   */
/* 'flight closed' (FC) event.) 
/* The timerlist is then scanned for commands     														*/
/* waiting for an FC event.                                                   */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleUpdateFlightTimer(char *pcpFields, char *pcpData)
{
	char  *pclData;
  char  *pclTimer;
  char  *pclACTOD;
  char  *pclADID;
  char  *pclALC2;
  char  *pclFLNO;
  int   ilTimer;
  long  llURNO;
  int   ilRC = RC_SUCCESS;
  int   ilGetRc;
  int   ilHaveActTod = FALSE;
  int   ilFlightClosed = FALSE;
  int   ilCntDayActTod, ilCntMinActTod, ilWkDy;
  int   ilCntDayNewTifd, ilCntMinNewTifd;
  int   ilTimerDiff;
  int   ilDiffMin;
  short slFkt,slCursor;
  char  clNewTifd[15];
  char  clACTOD[15];
  char  clActTim[15];
  char  clALC2[3];
  char  clFLNO[15];
  char  clWhereClause[256];
  char  pclSqlBuf[1024];
  char  pclDataArea[1024];

  dbg(DEBUG, "<HandleUpdateFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  
  //1.build Key/Item list
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);
	//2.check ADID, whether Departure
  pclADID = getItemDataByName(pcgItemList, "ADID", 0);
  if (pclADID != NULL) {
    if (strcmp(pclADID,"D")) {
      dbg(DEBUG, "<HandleUpdateFlightTimer> Not a departure -- skipped");
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  }
  //3.check URNO
  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  
  //4.check if this is a FC (Flight Close) event
  pclData = getItemDataByName(pcgItemList, "AIRB", 0);
  if (pclData != 0) {
    if (checkCedaTime(pclData) == RC_SUCCESS) ilFlightClosed = TRUE;
  }
  pclData = getItemDataByName(pcgItemList, "OFBL", 0);
  if (pclData != 0) {
    if (checkCedaTime(pclData) == RC_SUCCESS) ilFlightClosed = TRUE;
  }
  if (ilFlightClosed) {
    deleteItemList(pcgItemList);
    pcgItemList = NULL;
    //event only FC ,ATD
    sprintf(clWhereClause, "(EVNT='FC' OR EVNT='ATD') AND UAFT=%ld ORDER BY PRIO", llURNO);
    ilRC =  performTimerOrEventAction(clWhereClause, TRUE);
    return;
  }
  //5.Check, if we need to update an existant timer
  //if this URNO exist in AEVTAB, so need update; if not , need add timer entry
  pclData = getItemDataByName(pcgItemList, "TIFD", 0);
  if (pclData != 0) {
    sprintf(clNewTifd, "%s", pclData);
    sprintf(pclSqlBuf, "SELECT ATOD FROM AEVTAB WHERE UAFT='%ld'", llURNO);

    slFkt = START;
    slCursor = 0;
    while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS && ilHaveActTod == FALSE) {
      if (ilGetRc == DB_SUCCESS) {
				if (pclDataArea !=  NULL) {
	  			sprintf(clACTOD, "%s", pclDataArea);
	  			if (checkCedaTime(clACTOD) == RC_SUCCESS) {
	  				//true ,then out
	   				ilHaveActTod = TRUE;
	    			ilRC = GetFullDay(clACTOD, &ilCntDayActTod, &ilCntMinActTod, &ilWkDy);
	    			ilRC = GetFullDay(clNewTifd, &ilCntDayNewTifd, &ilCntMinNewTifd, &ilWkDy);
	    			ilTimerDiff = ilCntMinNewTifd - ilCntMinActTod;
	    			dbg(DEBUG, "<HandleUpdateFlightTimer> Timer offset will be added <%d>", ilTimerDiff); 
	  			}
				}
      }
      slFkt = NEXT;
    }
    //6.update TIMR and ATOD of AEVTAB
    close_my_cursor (&slCursor);
    if (ilHaveActTod && ilTimerDiff !=0) {
    	//very bright, shift TIMR; but ilTimerDiff always >0 , means never advance TIFD? answer is yes.
      sprintf(pcgSqlBuf, "UPDATE AEVTAB SET TIMR=TIMR+%d,ATOD='%s' WHERE UAFT='%ld'", ilTimerDiff, clNewTifd, llURNO);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_FAIL) {
				dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
      // negativ Timer values -> 0 
      sprintf(pcgSqlBuf, "UPDATE AEVTAB SET TIMR=0 WHERE TIMR<0");
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_FAIL) {
				dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
    }
    //7,ture means already exist and updated
    if (ilHaveActTod) {
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  
    //8.Check, if we need to insert a new timer
    GetServerTimeStamp("UTC", 1, 0, clActTim);
    DateDiffToMin(clNewTifd, clActTim, &ilDiffMin);

    if (ilDiffMin <= TimeRangeSelectFlights*60) {
      sprintf(pcgSqlBuf, "SELECT ALC2,FLNO FROM AFTTAB WHERE URNO=%ld", llURNO);
      ilRC = getOrPutDBData(START);
      if (ilRC == RC_FAIL) {
				dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
      else {
				BuildItemBuffer(pcgDataArea, NULL, 2, ";");
				pclALC2 = strtok(pcgDataArea, ";");
				pclFLNO = strtok(NULL, ";");
				if (pclALC2 != NULL && pclFLNO != NULL) {
	  			strcpy(clALC2, pclALC2);
	  			strcpy(clFLNO, pclFLNO);
	  			addTimerEntry(llURNO, clALC2, clNewTifd, clFLNO);
				}
				else dbg(TRACE, "<HandleUpdateFlightTimer> Unable to fetch ALC2 or FLNO");
      }
    }
    //  ilRC = GetFullDay(clNewTifd, &ilCntDayNewTifd, &ilCntMinNewTifd, &ilWkDy); 
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* addTimerEntry                                                              */
/*                                                                            */
/* Add an entry to the timer table.                                           */
/*                                                                            */
/*  cpStartTime : depature time                                                                          */
/*                                                                            */
/******************************************************************************/
int addTimerEntry(long lpFLURNO, char *cpALC2, char *cpStartTime, char *cpFLNO)
{
  int  ilMinutes = 0;
  int  ilTime = 0;
  int  ilRC;
  int  ilGetRc;
  int  ilType;
  int  ilTimer;
  long llHASIDENT;
  int  ilCntDayTIFD, ilCntMinTIFD;
  int  ilCntDayAct, ilCntMinAct;
  int  ilWkDy; 
  int  ilHaveEvent = FALSE;
  char clActTime[15];
  char clValues [2048];
  char clFields [2048];
  char clACTTIMER[11];
  char *clFirstTimer;
  char clEvent[11]="\0";
  char *pclGRPTYP;
  char *pclCFURNO;
  char *pclACTCMD;
  char *pclHRTIMLST;
  char *pclMNTIMLST;
  char *pclNextTimLst;
  char *pclCFSERIAL;
  char *pclEvent;
  char clHRTIMLST[65];
  char clMNTIMLST[65];
  int   ilPrio=0;
  char  pclSqlBuf[1024];
  char  pclSqlBuf1[1024];
  short slFkt;
  short slCursor = 0;
  char  clNum[15];
  char  clTimerHR[200];
  char  clTimerMin[200];
  char  clActTimer[16];
  char  pclDataArea[DATABLK_SIZE];
  long  llWALIDENT=0;
  char  clCOMD[20]="\0";

  
  //1. first check, if there is an entry for this flight already
  // check UAFT not enough, as need check COMD too
  //sprintf(pcgSqlBuf,"SELECT UAFT FROM AEVTAB WHERE UAFT='%ld'", lpFLURNO);
  //ilRC = getOrPutDBData(START);
  //if (ilRC == RC_SUCCESS) {
    //dbg(TRACE,"<addTimerEntry> This flight <%ld> already has an timer entry -- skipping", lpFLURNO);
    //return RC_SUCCESS;
  //}
  //2. check the ALC2 is in DB
  if (isValidALC2(cpALC2) != TRUE) {
    dbg(DEBUG,"<addTimerEntry> Airline <%s> not supported -- skipping", cpALC2);
    return RC_FAIL;
  }

  dbg(DEBUG,"<addTimerEntry> Adding timer for airline <%s>", cpALC2);
  
  //3.begin add timer entry,initialize variables 
  strcpy(clTimerHR, " ");
  strcpy(clTimerMin, " ");
  clEvent[0] = '\0';
  
  //4.calculate minutes until first start
  ilRC = GetFullDay(cpStartTime, &ilCntDayTIFD, &ilCntMinTIFD, &ilWkDy);
  GetServerTimeStamp("UTC", 1, 0, clActTime);
  ilRC = GetFullDay(clActTime, &ilCntDayAct, &ilCntMinAct, &ilWkDy); 

  //5.get data from DB
  sprintf(pclSqlBuf,"SELECT UHAT,COMD,TIHR,TIMI,EVNT FROM HCFTAB WHERE ALC2='%s' AND STAT='ACTIVE'", cpALC2);
  //sprintf(pclSqlBuf,"SELECT UHAT,COMD,TIHR,TIMI FROM HCFTAB WHERE ALC2='%s' AND STAT='ACTIVE' AND URNO=132864265", cpALC2);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
  	memset(clTimerHR,'\0',sizeof(clTimerHR));
  	memset(clTimerMin,'\0',sizeof(clTimerMin));
  	strcpy(clTimerHR, " ");
    strcpy(clTimerMin, " ");
    clEvent[0] = '\0';
  	memset(clCOMD,'\0',sizeof(clCOMD));
    if (ilGetRc != RC_SUCCESS) {
      dbg(TRACE, "<addTimerEntry> Airline not configured <%s>", cpALC2);
      close_my_cursor (&slCursor);
      return RC_FAIL;
    }
    else {
      BuildItemBuffer(pclDataArea, NULL, 5, ";");
      pclCFURNO = strtok(pclDataArea, ";");
      pclACTCMD = strtok(NULL, ";");
      pclHRTIMLST = strtok(NULL, ";");
      pclMNTIMLST = strtok(NULL, ";");
      pclEvent = strtok(NULL, ";");

      strcpy(clHRTIMLST, pclHRTIMLST);
      strcpy(clMNTIMLST, pclMNTIMLST);
      strcpy(clEvent, pclEvent);
      
      memset(clCOMD,'\0',sizeof(clCOMD));
      strcpy(clCOMD,pclACTCMD);
      
      char *res=NULL;
      res = getItemDataByName(myRequestCmdList, clCOMD, 0);
	    if (res == NULL)
	    	dbg(TRACE, "<addTimerEntry> Command <%s> not supported in walhdl.cfg", clCOMD);
	    else
	   {
      //check if uaft and acmd is already exit
      sprintf(pcgSqlBuf,"SELECT UAFT FROM AEVTAB WHERE UAFT='%ld' AND ACMD='%s'", lpFLURNO,clCOMD);
      ilRC = getOrPutDBData(START);
      if (ilRC == RC_SUCCESS) 
        dbg(TRACE,"<addTimerEntry> This flight <%ld> and Comand <%s> already has an timer entry -- skipping", lpFLURNO,clCOMD);
      else
   	  {
      
      if (strcmp(clEvent, " ")) ilHaveEvent = TRUE;

      dbg(DEBUG, "<addTimerEntry>\nCommand: <%s>\nHR-Timer: <%s>\nMin-Timer: <%s>\nDeparture-Time: <%s>",
	  	pclACTCMD, pclHRTIMLST, pclMNTIMLST, cpStartTime);
	  	
	  	//6.get Hours entry
      clFirstTimer = getFirstActTimerEntry(pclHRTIMLST, &ilType, &pclNextTimLst, TRUE, ilCntMinAct, ilCntMinTIFD);
      if (ilType == TIM) {
      	if(pclNextTimLst!=NULL)
					strcpy(clTimerHR,pclNextTimLst);
				strcpy(clTimerMin,pclMNTIMLST);
				ilTime = atoi(clFirstTimer) * 60;
				strcat(clFirstTimer,"H");
				ilPrio = 1;
				dbg(DEBUG,"<addTimerEntry> Hour Entry <%s>,pclHRTIMLST: <%s> ",clFirstTimer,pclHRTIMLST);
      }
      //7.if no Hours entry, get Minutes entry  
      if (ilType == NONE) {
				clFirstTimer = getFirstActTimerEntry(pclMNTIMLST, &ilType, &pclNextTimLst, FALSE, ilCntMinAct, ilCntMinTIFD);
				if (ilType == TIM) {
				if(pclNextTimLst!=NULL)
					strcpy(clTimerMin,pclNextTimLst);
	  		ilTime = atoi(clFirstTimer);
	  		strcat(clFirstTimer,"M");
	  		ilPrio = 5;
				}
				//8.both entry empty, then next
				if (ilType == NONE && ilHaveEvent == FALSE) {
	  			dbg(TRACE, "<addTimerEntry> Fatal no valid timer specified");
	  			slFkt = NEXT;
	  			continue;
				}
				
				dbg(DEBUG,"<addTimerEntry> Minute Entry <%s>,pclHRTIMLST: <%s> ",clFirstTimer,pclMNTIMLST);
      }
      
      if (ilType == EVT) strcpy(clEvent, clFirstTimer);
      if (ilType == EVT || ilHaveEvent) {
	strcpy(clActTimer, " ");
	ilPrio = 5;
      }

			//caculate TIMR
      if (ilType == TIM) {
				if (clFirstTimer != NULL) strcpy(clActTimer, clFirstTimer);
				else strcpy(clActTimer, " ");

				ilMinutes = (ilCntMinTIFD - ilTime) - ilCntMinAct;
				if (ilMinutes<0) ilMinutes = 0; 
				dbg(DEBUG, "<addTimerEntry> <%s> Timer for request <%s> will start in <%d> minutes", clFirstTimer, cpStartTime, ilMinutes);
      }

      //llHASIDENT = GetSequence("KRISCOM");
      //PutSequence("KRISCOM", llHASIDENT+1);
      
      llWALIDENT = GetSequence("ALTEA");
      PutSequence("ALTEA", llWALIDENT+1);
			
			//9.Insert the flight to AEVTAB
      sprintf(clFields, "ALC2,ACMD,ATIM,TIMR,ATOD,TIHR,TIMI,PRIO,FLNO,UAFT,WAID,EVNT,STAT");
      
      sprintf(clValues,"'%s','%s','%s','%d','%s','%s','%s','%d','%s','%d','%d','%s','ACTIVE'",
	      							cpALC2, pclACTCMD, clActTimer, ilMinutes,  cpStartTime, clTimerHR, clTimerMin, ilPrio, cpFLNO,lpFLURNO,llWALIDENT,clEvent);
      sprintf(pcgSqlBuf,"INSERT INTO AEVTAB (%s) VALUES(%s)", clFields, clValues);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
				dbg(TRACE, "<addTimerEntry> Unable to activate timer SQL statement:\n<%s>", pcgSqlBuf);
				slFkt = NEXT;
				continue;
      }
    }
  }
    slFkt = NEXT;
  }
  }
  close_my_cursor (&slCursor);
}
/******************************************************************************/
/* checkCedaTime   	                                                      */
/*                                                                            */
/* Check if the provided time pattern is valid. It has to look like:          */
/* YYYYMMDDHHMM, DDHH.                                                        */
/*                                                                            */
/* Input:                                                                     */
/*        pcpData -- pointer to data we have to check                         */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if pattern is valid                                      */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkCedaTime(char *pcpData) 
{
  uint ilTimStrLen;
  uint ilHour, ilMin, ilSec;
  uint ilYear, ilMonth, ilDay;
  uint ilTime = FALSE;
  uint ilDate = FALSE;
  char clHour[3], clMin[3], clSec[3];
  char clYear[5], clMonth[3], clDay[3];
  char *pclData;

  ilTimStrLen = strlen(pcpData);

  if (ilTimStrLen == 4 || ilTimStrLen ==6) ilTime = TRUE;
  else if (ilTimStrLen == 8) ilDate = TRUE;
  else if (ilTimStrLen == 12 || ilTimStrLen == 14) {
    ilTime = TRUE;
    ilDate = TRUE;
  }
  else {
    dbg(TRACE,"<checkCedaTime> Illegal time format <%s> valid is YYYYMMDD DDHH",pcpData); 
    return RC_FAIL;
  }

  pclData = pcpData;

  if (ilDate == TRUE) {
    strncpy(&clYear[0],pclData,4);
    clYear[4] = '\0';
    ilYear    = atoi(clYear);
    clMonth[0]= *(pclData+4);
    clMonth[1]= *(pclData+5);
    clMonth[2]= '\0';
    ilMonth   = atoi(clMonth);
    clDay[0]  = *(pclData+6);
    clDay[1]  = *(pclData+7);
    clDay[2]  = '\0';
    ilDay     = atoi(clDay);
    if (ilYear < 1970 || ilYear > 2100) {
      dbg(TRACE,"<checkCedaTime> Illegal year <%d>", ilYear);
      return RC_FAIL;
    }
    if (ilMonth < 1 || ilMonth > 12) {
      dbg(TRACE,"<checkCedaTime> Illegal month <%d>", ilMonth);
      return RC_FAIL;
    }
    /* check special case of 29. FEB in leap years! */
    if (LeapYear(ilYear) == 1 && ilMonth == 2) {
      if (ilDay < 1 || ilDay > 29) {
	dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
	return RC_FAIL;
      }
    }
    if (ilDay < 1 || ilDay > igDayTab[ilMonth]) {
      dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
      return RC_FAIL;
    } 
    /* proceed to date entry */
    pclData = pcpData+8;
  }
  if (ilTime == TRUE) {
    clHour[0] = *pclData;
    clHour[1] = *(pclData+1);
    clHour[2] = '\0';
    ilHour    = atoi(clHour);
    clMin[0]  = *(pclData+2);
    clMin[1]  = *(pclData+3);
    clMin[2]  = '\0';
    ilMin     = atoi(clMin);
    if (ilTimStrLen == 6 || ilTimStrLen== 14) {
      clSec[0] = *(pclData+4);
      clSec[1] = *(pclData+5);
      clSec[2] = '\0';
      ilSec    = atoi(clSec);
      if (ilSec > 59) {
	dbg(TRACE,"<checkCedaTime> Illegal Second <%d>", ilMin);
      return RC_FAIL;
      }
    }
    if(ilHour > 23) {
      dbg(TRACE,"<checkCedaTime> Illegal Hour <%d>", ilHour);
      return RC_FAIL;
    }
    if (ilMin > 59) {
      dbg(TRACE,"<checkCedaTime> Illegal Minute <%d>", ilMin);
      return RC_FAIL;
    }
  }
  return RC_SUCCESS;
}

/******************************************************************************/
/* addItemToList  	                                                      */
/*                                                                            */
/* add a new item to the list, allocate required memory, initialize pointers  */
/*                                                                            */
/* Input:                                                                     */
/*        element    -- pointer to field name                                   */
/*        hook       -- pointer to list where the element is to add             */
/*        numOfLegs  -- number of legs to initialize                                                                  */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
static int addItemToList(char *element, ListPtr *hook)
{
  ListPtr pclFreeEl;
  ListPtr pclInitEl;
  uint i;
  uint ilSize;

  ilSize = sizeof(ListElem);
  ilSize = sizeof(struct List);
  if (*hook == NULL) {
    *hook = malloc(sizeof(ListElem));
    (*hook)->next = NULL;
    (*hook)->prev = NULL;
    for (i=0; i<MAX_NUM_LEGS; i++) {
      (*hook)->dobj[i].data = NULL;
      (*hook)->dobj[i].status = 0;
      (*hook)->dobj[i].len = 0;
      (*hook)->dobj[i].timeType = 0;
      (*hook)->dobj[i].timeZone[0] = '\0';
      (*hook)->dobj[i].timeZone[1] = '\0';
      (*hook)->dobj[i].timeZone[2] = '\0';
    }
    (*hook)->field = malloc(strlen(element));
    strcpy((*hook)->field, element);
    return 1;
  }
  else {
    pclFreeEl = *hook;
    pclFreeEl = getFirstFree (pclFreeEl);
    pclFreeEl->next = malloc(sizeof(ListElem));
    pclFreeEl->next->next = NULL;
    pclInitEl = pclFreeEl->next;
    for (i=0; i<MAX_NUM_LEGS; i++) {
      pclInitEl->dobj[i].data = NULL;
      pclInitEl->dobj[i].status = 0;
      pclInitEl->dobj[i].len = 0;
      pclInitEl->dobj[i].timeType = 0;
      pclInitEl->dobj[i].timeZone[0] = '\0';
      pclInitEl->dobj[i].timeZone[1] = '\0';
      pclInitEl->dobj[i].timeZone[2] = '\0';
    }
    pclFreeEl->next->prev = pclFreeEl;
    pclFreeEl->next->field = malloc(strlen(element));
    strcpy(pclFreeEl->next->field, element);
  }
}
/******************************************************************************/
/* getFirstFree  	                                                      */
/*                                                                            */
/* return a pointer to a 'field' object with name 'name'                      */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field list                                      */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
ListPtr getFirstFree(ListPtr hook)
{
  ListPtr pclLis;

  if (hook->next == NULL) {
    pclLis = hook;
    return pclLis;
  }
  pclLis = getFirstFree(hook->next);
  return pclLis;
}
/******************************************************************************/
/* getFirstActTimerEntry                                                      */
/*                                                                            */
/* Fetch the first entry from the list of timer entries. Try to figure out,   */
/* what kind of entry was found and advance to the next one if an entry is    */
/* already expired.                                                           */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static char *getFirstActTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry, int ipHour, int ipMinAct, int ipMinStart)
{
  char *pclEntry = (char *)malloc(20);
  memset(pclEntry,0,20);
  int  ilTime;
  int  ilFound=FALSE;
  char clEntryList[300];
  int  test = TRUE;

  /* while (test == TRUE); */
  if(0){
  	strncpy(*pcpNextEntry,"6,4,2",5);
  	*ipType=TIM;
  	char *re="8";
  	return re;
	}

  pclEntry = getFirstTimerEntry(pcpEntries, ipType, pcpNextEntry);

  while (ilFound == FALSE) {
    if (!strcmp(pclEntry, "")) return "";
    else if (*ipType == EVT || *ipType == NONE) return pclEntry;
    
    /*
    ** check if timer entry is expired 
    */
    ilTime = atoi(pclEntry);
    if (ipHour) ilTime = ilTime * 60;
    if ((ipMinStart - ilTime) - ipMinAct > 0) {
      ilFound = TRUE;
      return pclEntry;
    }
    if (*pcpNextEntry == NULL) {
      if (ipHour == FALSE) return pclEntry;
      else {
				*ipType = NONE; /* force minute list scanning */
				//return "";
				return pclEntry;
      }
    }
    //strcpy(clEntryList, *pcpNextEntry);
    strncpy(clEntryList,*pcpNextEntry,strlen(*pcpNextEntry));
    clEntryList[strlen(*pcpNextEntry)]='\0';
    pclEntry = getFirstTimerEntry(clEntryList, ipType, pcpNextEntry);
    if (*pcpNextEntry != NULL) {
      dbg(DEBUG, "<getFirstActTimerEntry> Next: <%s> Act: <%s> Remaining: <%s>", pclEntry, clEntryList, *pcpNextEntry); 
    }
    else {
      dbg(DEBUG, "<getFirstActTimerEntry> Next: <%s> Act: <%s> Remaining: <>", pclEntry, clEntryList);
    }
  }
}
/******************************************************************************/
/* getFirstTimerEntry                                                         */
/*                                                                            */
/* Fetch the first entry from the list of timer entries. Try to figure out,   */
/* what kind of entry was found.                                              */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static char *getFirstTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry)
{
  char *pclData;
  char *pclEnd;
  char *pclEndSav;
  static  char clSave[300];
  int  ilEventReq = FALSE;
  int  ilFirst = FALSE;
  int  entry = 0;

  *ipType = NONE;
  ConvertDbStringToClient(pcpEntries);

  if (!strcmp(pcpEntries, "") || !strcmp(pcpEntries, " ")) {
    *pcpNextEntry = NULL;
    return "";
  }

  pclData = pcpEntries;
  pclEnd = strchr (pcpEntries, ',');

  if (pclEnd != NULL) pclEnd++;

  if (pclData != NULL) {
    //strcpy(clSave, pclData);
    strncpy(clSave,pclData,strlen(pclData));
    pclEndSav = strchr (clSave, ',');
    if (pclEndSav != NULL) *pclEndSav = '\0';
  }

  *pcpNextEntry = pclEnd; /* point to next entry if any */
  
  entry = 0;
  while(clSave[entry] != '\0') {
    if (isalpha(clSave[entry])) {
      *ipType = NONE;
      break;
    }
    else *ipType = TIM;
    entry++;
  }
  return clSave;
}
/******************************************************************************/
/* Get a unique identifier (MCU)                                              */
/******************************************************************************/
static void PutSequence(char *pcpKey,long lpSequence)
{
	int ilRc;

	short slCursor = 0;
	char clSqlBuf[200];
	char clDataArea[256];

	sprintf(clSqlBuf,"UPDATE NUMTAB SET ACNU='%10d' WHERE KEYS='%s' AND HOPO='%s'"
		,lpSequence,pcpKey,cgHopo);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if (ilRc != RC_SUCCESS) { 
		/* sequence not yet written, create new one */
	  dbg(TRACE,"sequence not yet written, create new one");
	  sprintf(clSqlBuf,"INSERT INTO NUMTAB (ACNU,FLAG,HOPO,KEYS,MAXN,MINN) VALUES(%d,' ','%s','%s',9999999999,1)",
		  lpSequence,cgHopo,pcpKey);
	  close_my_cursor(&slCursor);
	  slCursor = 0;
	  ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	  if (ilRc != RC_SUCCESS) {
	    /* there must be a database error */
	    get_ora_err (ilRc, cgErrMsg);
	    dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
	  }
	}
	commit_work();
	close_my_cursor(&slCursor);
	slCursor = 0;
}


static long GetSequence(char *pcpKey)
{
	int ilRc;
	long llSequence = 0;
	short slCursor = 0;
	char clSqlBuf[200];
	char clDataArea[256];

	sprintf(clSqlBuf,"SELECT ACNU FROM NUMTAB WHERE KEYS='%s' AND HOPO='%s'",
			pcpKey,cgHopo);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc == RC_SUCCESS) {
	  llSequence = atol(clDataArea);
	}
	else if(ilRc == RC_NOTFOUND) {
	  //PutSequence(pcpKey,0);
	  llSequence =0;
	  dbg(TRACE,"sequence not yet written, create new one");
	  sprintf(clSqlBuf,"INSERT INTO NUMTAB (ACNU,FLAG,HOPO,KEYS,MAXN,MINN) VALUES(%d,' ','%s','%s',9999999999,1)",
		  llSequence,cgHopo,pcpKey);
	  close_my_cursor(&slCursor);
	  slCursor = 0;
	  ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	  if (ilRc != RC_SUCCESS) {
	    /* there must be a database error */
	    get_ora_err (ilRc, cgErrMsg);
	    dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
	  }
	} 
	else { 
	  /* there must be a database error */
	  get_ora_err (ilRc, cgErrMsg);
	  dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

	return llSequence;
}

/******************************************************************************/
/* getOrPutDBData                                                             */
/*                                                                            */
/* Fetch data as described in the global pcgSqlBuf and return the result in   */
/* global buffer pcgDataArea.                                                 */
/*                                                                            */
/* Input:                                                                     */
/*        ipMode -- function mode mask like START|COMMIT                      */
/*                                                                            */
/******************************************************************************/
int getOrPutDBData(uint ipMode)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}
/******************************************************************************/
/* isValidALC2                                                                */
/*                                                                            */
/* Check if the airline is contained in the list of configured airlines.      */
/*                                                                            */
/* Input:                                                                     */
/*       cpALC2 -- points to the ALC2 to check                                */
/*                                                                            */
/* Returns:                                                                   */
/*       TRUE/FALSE                                                           */
/*                                                                            */
/******************************************************************************/
int isValidALC2(char *cpALC2)
{
  int ilRC;

  sprintf(pcgSqlBuf,"SELECT ALC2 FROM HCFTAB WHERE ALC2='%s'", cpALC2);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) return TRUE;
  else return FALSE;
}
static void alert(char *pcpMessage)
{
    char clMyAlert[2000] = "\0";
    int ilRC = RC_SUCCESS;
    dbg(TRACE, "ALERT: *****%s*****", pcpMessage);
    ilRC = AddAlert2("ALTEA"," "," ","E",pcpMessage," ",TRUE,FALSE,"ALERT",mod_name," "," ");
}
/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
