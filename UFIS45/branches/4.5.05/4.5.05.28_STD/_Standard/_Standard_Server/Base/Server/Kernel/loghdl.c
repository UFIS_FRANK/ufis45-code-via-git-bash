#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/loghdl.c 1.16 2009/07/30 14:34:39SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "urno_fn.h"
#include "cedatime.h"
#include "tools.h"
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
#define MAX_KEYF 5
#define KEYF_LEN 256

/******************************************************************************/
/* Internal Structures                                                        */
/******************************************************************************/
typedef struct
{
  int  Count;
  char TblName[8];
  char LogDest[8];
  char LogList[4004];
  char LogItem[4004];
  char LogOldItem[4004];
  /* members for logging int R0xTAB (key fields) */
  int  KeyfCnt;
  char KeyfSep[2];
  char KeyfName[MAX_KEYF][8];
  char KeyfField[MAX_KEYF][64];
  char *KeyfData[MAX_KEYF];
} LOG_TABLE;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
enum LogType {LOG_SINGLE,LOG_BOTH};
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static int   igQueOut = 0;

static int igRuntimeMode = TRACE;
static int igStartUpMode = TRACE;
static int igLogType;
static int igUseLocalTimeStamp = FALSE;

static char pcgRecvName[64];
static char pcgDestName[64];
static char pcgTwStart[64];
static char pcgTwEnd[64];

static char pcgDefTblExt[32];
static char pcgTblExt[32];
static char pcgDefH3LC[32];
static char pcgH3LC[32];
static char pcgCmdList[512];
static char pcgUfisTables[2048];
static char pcgLoggTables[2048];
static char pcgIgnoreHopo[512];
static char pcgSqlBuf[12*1024];
static char pcgDataArea[12*1024];
static char pcgDataAreaOld[12*1024];
static char pcgSeparateUrnoTables[1024];      /* Mei */
static char cgHopo[8] = "\0";                 /* Mei, default home airport    */

static char *pcgDelList = ",DRT,DFR,";
static char *pcgKeyfNames[] = {"SDAY","KEY1","KEY2","KEY3","KEY4"};
static char pcgItemSep[4];
extern char *getItem(char *,char *,char *);

#define FOR_INIT 1
#define FOR_WORK 2
#define FOR_SHOW 3
#define MAX_LOG_TBL 150
static LOG_TABLE argLogTable[MAX_LOG_TBL+1];
#define URNO_POOL_SIZE 500        /* number of URNOS fetched with each refill of pool */
#define URNO_SIZE 12              /* maximum size of URNO in string representation    */ 
/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int InitLogHdl();
static int  Reset(void);                       /* Reset program          */
static void Terminate(void);                   /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static void CollectUfisTables(void);
static void CollectLoggingTables(void);
static void ResetLogTab(int);
static void AppendField(char *,char *,char *);
static int HandleLogging(char *,char *,char *,char *,char *,char *);
static void CheckFields(char *, char *, char *);
static void WriteToTable(int, char *, char *, char *, char *, char *, char *);
static void CheckHomePort(void);
static int ReadCfg(char *, char *, char *, char *, char *);
static void InitLogFileModes(void);
static void GetLogFileMode(int *, char *, char *);
static void WriteToHxxTable(char *,char *,char *,char *,char *,char *,char *,char *,char *);
static int getFreeUrno(char *,char *);
static int FillKeyField ( int ipTblNbr, int ipKeyIdx, char *pcpFld, char *pcpDat );
static void WriteToRxxTable( int ipTblNbr, char *pcpCmd, char *pcpSel, char *pcpOutFld, char *pcpOutDat);
static int GetRxxUrno(char *cpUrno);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRC = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    
    INITIALIZE;         /* General initialization   */
    dbg(TRACE,"MAIN:\nVERSION <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */
    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
        ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */
    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/loghdl",getenv("BIN_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    sprintf(cgConfigFile,"%s/loghdl.cfg",getenv("CFG_PATH"));
    ilRC = TransferFile(cgConfigFile);
    InitLogFileModes();
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*  dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitLogHdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitLogHdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        dbg(TRACE,"=================== START/END ==================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET       :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
    /* exit(0);*/
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitLogHdl()
{
  char pclTmp[128];
  int   ilRC = RC_SUCCESS;          /* Return code */
  int ilValue = 0;
  char pclCfgCode[32];
  char pclCfgValu[32];
  igInitOK = TRUE;
  strcpy(pclCfgCode,"COMMANDS");
  ReadCfg(cgConfigFile,pcgCmdList,"GLOBAL_DEFINES",pclCfgCode,
      ",IRT,URT,DRT,IFR,UFR,DFR,");
  dbg(TRACE,"COMMANDS: <%s>",pcgCmdList);
  strcpy(pclCfgCode,"ITEM_SEPARATOR");
  ReadCfg(cgConfigFile,pclCfgValu,"GLOBAL_DEFINES",pclCfgCode,"30");
  dbg(TRACE,"ITEM SEPARATOR VALUE = %s (DEC)",pclCfgValu);
  ilValue = atoi(pclCfgValu);
  if (ilValue <= 0)
  {
    ilValue = 30;
  } /* end if */
  sprintf(pcgItemSep,"%c",ilValue);
  dbg(TRACE,"ITEM SEPARATOR CHAR  = %s ",pcgItemSep);
  /* Configurable logging table */
  strcpy(pclCfgCode,"LOG_TYPE");
  ReadCfg(cgConfigFile,pclCfgValu,"GLOBAL_DEFINES",pclCfgCode,"LOG_SINGLE");
  dbg(TRACE,"LOG_TYPE = %s",pclCfgValu);
  if (!strcmp(pclCfgValu,"LOG_BOTH")) 
      igLogType = LOG_BOTH;
  else
      igLogType = LOG_SINGLE;

  /*** Mei ***/
  ReadCfg(cgConfigFile,pcgSeparateUrnoTables,"GLOBAL_DEFINES","SEPARATE_URNO","");
  dbg(TRACE,"SEPARTE_URNO tables: <%s>",pcgSeparateUrnoTables);
  /***********/

  pcgDefTblExt[0] = 0x00;
  pcgDefH3LC[0] = 0x00;
  ilRC = tool_search_exco_data("ALL","TABEND",pcgDefTblExt);
  strcpy(pcgTblExt,pcgDefTblExt);
  ilRC = tool_search_exco_data("SYS","HOMEAP",pcgDefH3LC);
  strcpy(pcgH3LC,pcgDefH3LC);
  dbg(TRACE,"DEFAULTS: HOME <%s> EXT <%s>",pcgDefH3LC,pcgDefTblExt);

  ReadCfg(cgConfigFile,pclTmp,"SYSTEM","TIMESTAMPS","UTC");
  if (strcmp(pclTmp,"LOCAL")==0)
  {
    igUseLocalTimeStamp = TRUE;
     dbg(TRACE,"TIMESTAMPS ARE GENERATED IN <LOCAL>!");
  } else {
    igUseLocalTimeStamp = FALSE;
     dbg(TRACE,"TIMESTAMPS ARE GENERATED IN <UTC>!");
  } /* end else if */
  dbg(TRACE,"=================================================");

  ResetLogTab(FOR_INIT);
  CollectUfisTables();
  CollectLoggingTables();
  debug_level = igRuntimeMode;

  return(ilRC);
    
} /* end of initialize */

/* *******************************************************/
/* *******************************************************/
static void CollectUfisTables(void)
{
  int ilRC = DB_SUCCESS;
  int ilChrPos = 0;
  short slCursor = 0;
  short slFkt = 0;
  strcpy(pcgIgnoreHopo,"SYSTAB,NUMTAB,MSGTAB");
  sprintf(pcgSqlBuf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE COLUMN_NAME='HOPO' "
                    "AND TABLE_NAME IN "
                    "(SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '___%s' "
                    "AND COLUMN_NAME='URNO') "
                    "ORDER BY TABLE_NAME",pcgDefTblExt);
  dbg(DEBUG,"<%s>",pcgSqlBuf);
  pcgUfisTables[0] = 0x00;
  ilChrPos = 0;
  slCursor = 0;
  slFkt=START;
  while((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == DB_SUCCESS)
  {
    str_trm_all(pcgDataArea," ",TRUE);
    if (strstr(pcgIgnoreHopo,pcgDataArea) == NULL)
    {
      StrgPutStrg(pcgUfisTables, &ilChrPos, pcgDataArea, 0, -1, ",");
    } /* end if */
    slFkt = NEXT;
  } /* end while */
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
    ilChrPos--;
  } /* end if */
  pcgUfisTables[ilChrPos] = 0x00;
  dbg(DEBUG,"UFIS TABLES CONTAINING 'URNO' AND 'HOPO'\n<%s>",pcgUfisTables);
  dbg(DEBUG,"=================================================");
  return;
} /* end CollectUfisTables */ 

/* *******************************************************/
/* *******************************************************/
static void CollectLoggingTables(void)
{
  int ilRC = DB_SUCCESS;
  int ilFldCnt = 0;
  int ilFldCntTot = 0;
  int ilNotCnt = 0;
  int ilNotCntTot = 0;
  int ilTblCnt = 0;
  int ilTblNbr = 0;
  int ilChrPos = 0;
  int ilLen = 0;
  short slCursor = 0;
  short slFkt = 0;
  char pclTable[64];
  char pclFina[64];
  char pclLogd[64];
  char pclOldLogd[64];
  strcpy(pcgIgnoreHopo,"SYSTAB,NUMTAB,MSGTAB");
  sprintf(pcgSqlBuf,"SELECT DISTINCT(TANA) FROM SYS%s "
                    "WHERE LOGD<>' ' ORDER BY TANA",pcgDefTblExt);
  dbg(DEBUG,"<%s>",pcgSqlBuf);
  pcgLoggTables[0] = 0x00;
  ilChrPos = 0;
  slCursor = 0;
  slFkt=START;
  while((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == DB_SUCCESS)
  {
    str_trm_all(pcgDataArea," ",TRUE);
    sprintf(pclTable,"%s%s",pcgDataArea,pcgDefTblExt);
    StrgPutStrg(pcgLoggTables, &ilChrPos, pclTable, 0, -1, ",");
    slFkt = NEXT;
  } /* end while */
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
    ilChrPos--;
  } /* end if */
  pcgLoggTables[ilChrPos] = 0x00;
  dbg(TRACE,"LOGGING TABLES:\n<%s>",pcgLoggTables);
  dbg(TRACE,"=================================================");

  ilTblCnt = field_count(pcgLoggTables);
  dbg(TRACE,"INITIALIZING %d TABLES FOR LOGGING",ilTblCnt);
  dbg(TRACE,"-------------------------------------------------");
  for (ilTblNbr=1;ilTblNbr<=ilTblCnt;ilTblNbr++)
  {
    get_real_item(pclTable,pcgLoggTables,ilTblNbr);
    dbg(TRACE,"INITIALIZING TABLE <%s>",pclTable);
    sprintf(pcgSqlBuf,"SELECT FINA,LOGD FROM SYS%s "
                      "WHERE TANA='%3.3s' ORDER BY FINA",
              pcgDefTblExt,pclTable,pcgDefTblExt);
    dbg(DEBUG,"<%s>",pcgSqlBuf);
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    pclOldLogd[0] = 0x00;
    ilFldCnt = 0;
    ilNotCnt = 0;
    while((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == DB_SUCCESS)
    {
      BuildItemBuffer(pcgDataArea,"",2,",");
      ilLen = get_real_item(pclFina,pcgDataArea,1);
      ilLen = get_real_item(pclLogd,pcgDataArea,2);
      if (ilLen > 0)
      {
    ilFldCnt++;
        if (pclOldLogd[0] == 0x00)
        {
      strcpy(pclOldLogd,pclLogd);
        } /* end if */
        if (strcmp(pclOldLogd,pclLogd) != 0)
        {
      dbg(TRACE,"=== <%s> DIFFERENT TABLES <%s> <%s> ===",
            pclFina,pclOldLogd,pclLogd);
        } /* end if */
        AppendField(pclTable,pclFina,pclLogd);
        dbg(DEBUG,"<%s> :  <%s> <%s>",pclTable,pclFina,pclLogd);
        strcpy(pclOldLogd,pclLogd);
      } /* end if */
      else
      {
        dbg(DEBUG,"<%s> :  <%s> <NOT LOGGED> ---",pclTable,pclFina);
    ilNotCnt++;
      } /* end else */
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    ilFldCntTot += ilFldCnt;
    ilNotCntTot += ilNotCnt;
    dbg(TRACE,"RESULT: LOGGING %d FIELDS (%d NOT)",ilFldCnt,ilNotCnt);
    dbg(TRACE,"-------------------------------------------------");
  } /* end for */
  dbg(TRACE,"TOTAL: LOGGING %d FIELDS (%d NOT)",ilFldCntTot,ilNotCntTot);
  dbg(TRACE,"CONFIGURED %d TABLES FOR %d LOGGIN DESTINATIONS",
        ilTblCnt,argLogTable[0].Count);
  dbg(TRACE,"=================================================");

  /* ResetLogTab(FOR_SHOW); */

  return;
} /* end CollectLoggingTables */ 

/******************************************************************************/
/******************************************************************************/
static void AppendField(char *pcpTable,char *pcpFina,char *pcpLogd)
{
  int ilTblNbr = 0;
  int ilTblCnt = 0;
  int ilFound = FALSE;
  ilTblCnt = argLogTable[0].Count;
  ilTblNbr = 1;
  while ((ilTblNbr <= ilTblCnt) && (ilFound == FALSE))
  {
    if ((strcmp(argLogTable[ilTblNbr].TblName,pcpTable) == 0) &&
        (strcmp(argLogTable[ilTblNbr].LogDest,pcpLogd) == 0))
    {
      ilFound = TRUE;
      strcat(argLogTable[ilTblNbr].LogList,",");
      strcat(argLogTable[ilTblNbr].LogList,pcpFina);
      ilTblNbr = ilTblCnt;
    } /* end if */
    ilTblNbr++;
  } /* end while */
  if (ilFound == FALSE)
  {
    argLogTable[0].Count++;
    ilTblNbr = argLogTable[0].Count;
    if (ilTblNbr > MAX_LOG_TBL)
    {
      dbg(TRACE,"CONFIG ERROR: TABLE OVERFLOW %d/%d",ilTblNbr,MAX_LOG_TBL);
      ilTblNbr = MAX_LOG_TBL;
    } /* end if */
    argLogTable[ilTblNbr].Count = 0;
    strcpy(argLogTable[ilTblNbr].TblName,pcpTable);
    strcpy(argLogTable[ilTblNbr].LogDest,pcpLogd);
    strcpy(argLogTable[ilTblNbr].LogList,pcpFina);
    argLogTable[ilTblNbr].LogItem[0] = 0x00;
    /*  initialise members for R0xTAB logging method */
    if ( pcpLogd[0] == 'R' )
    {
        char pclTmp[128];
        int ilVal, ilIdx;
        if ( ReadCfg(cgConfigFile,pclTmp,pcpTable,"SEPARATOR", "30") == RC_SUCCESS )
        {
            ilVal = atoi(pclTmp);
            if (ilVal > 0)
            {
                sprintf(pclTmp,"%c",ilVal);
                strcpy ( argLogTable[ilTblNbr].KeyfSep, pclTmp);
            }
        }
        for ( ilIdx=0; ilIdx<MAX_KEYF; ilIdx++ )
        {
            if ( ReadCfg(cgConfigFile,pclTmp,pcpTable,pcgKeyfNames[ilIdx], "") == RC_SUCCESS )
            {
                if ( strlen ( pclTmp ) > 0 )
                {
                    ilVal = argLogTable[ilTblNbr].KeyfCnt;
                    argLogTable[ilTblNbr].KeyfCnt++;
                    strcpy ( argLogTable[ilTblNbr].KeyfName[ilVal], pcgKeyfNames[ilIdx] );
                    strcpy ( argLogTable[ilTblNbr].KeyfField[ilVal], pclTmp );
                    argLogTable[ilTblNbr].KeyfData[ilVal] = malloc ( KEYF_LEN+1 );
                }
            }
        }
        dbg(DEBUG,"<%s> : <%d> KEYFIELDS, SEPARATOR <%s>", pcpTable,
            argLogTable[ilTblNbr].KeyfCnt, argLogTable[ilTblNbr].KeyfSep );
        for ( ilIdx=0; ilIdx<argLogTable[ilTblNbr].KeyfCnt; ilIdx++ )
        {
            dbg(DEBUG,"<%s> : KEYFIELD <%s> <%s>", pcpTable,
                argLogTable[ilTblNbr].KeyfName[ilIdx], argLogTable[ilTblNbr].KeyfField[ilIdx] );
        }
    }
  } /* end if */
  return;
} /* end AppendField */

/******************************************************************************/
/******************************************************************************/
static void ResetLogTab(int ipWhat)
{
  int ilTblNbr = 0;
  int ilTblCnt = 0;
  if (ipWhat == FOR_INIT)
  {
    ilTblCnt = MAX_LOG_TBL;
    argLogTable[0].Count = 0;
  } /* end if */
  else
  {
    ilTblCnt = argLogTable[0].Count;
  } /* end else */
  for (ilTblNbr=1;ilTblNbr<=ilTblCnt;ilTblNbr++)
  {
    if (ipWhat != FOR_SHOW)
    {
      argLogTable[ilTblNbr].Count = 0;
    } /* end if */
    if (ipWhat == FOR_INIT)
    {
      argLogTable[ilTblNbr].TblName[0] = 0x00;
      argLogTable[ilTblNbr].LogDest[0] = 0x00;
      argLogTable[ilTblNbr].LogList[0] = 0x00;
      argLogTable[ilTblNbr].LogItem[0] = 0x00;
      /*  initialise members for R0xTAB logging method */
      argLogTable[ilTblNbr].KeyfCnt = 0;
      strcpy (argLogTable[ilTblNbr].KeyfSep, pcgItemSep);
      memset ( argLogTable[ilTblNbr].KeyfName, 0, sizeof(argLogTable[ilTblNbr].KeyfName) );
      memset ( argLogTable[ilTblNbr].KeyfField, 0, sizeof(argLogTable[ilTblNbr].KeyfField) );
      memset ( argLogTable[ilTblNbr].KeyfData, 0, sizeof(argLogTable[ilTblNbr].KeyfData) );
    } /* end if */
    if (ipWhat == FOR_SHOW)
    {
      dbg(TRACE,"LOGGING TABLE <%s> TO DEST <%s>",
         argLogTable[ilTblNbr].TblName,argLogTable[ilTblNbr].LogDest);
      dbg(TRACE,"FIELDS: <%s>",argLogTable[ilTblNbr].LogList);
      dbg(TRACE,"ITEMS : <%s>",argLogTable[ilTblNbr].LogItem);
      dbg(TRACE,"-------------------------------------------------");
    } /* end if */
  } /* end for */
  return;
} /* end ResetLogTab */



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRC = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
    /* unset SIGCHLD ! DB-Child will terminate ! */
    logoff();
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int ilRC = RC_SUCCESS;          /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default :
        Terminate();
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET       :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitLogHdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitLogHdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int   ilRC = RC_SUCCESS;
  char pclActCmd[16];
  char pclChkCmd[16];
  BC_HEAD *prlBchd = NULL;
  CMDBLK  *prlCmdblk = NULL;
  char *pclSel=NULL,*pclFld=NULL,*pclData=NULL;
  char *pclOldData = NULL;
  char *pclOldDumm = "\0";
  igQueOut         = prgEvent->originator;
  prlBchd         = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk       = (CMDBLK  *) ((char *)prlBchd->data);
  pclSel          = prlCmdblk->data;
  pclFld          = pclSel + strlen(pclSel) + 1;
  pclData         = pclFld + strlen(pclFld) + 1;

  pclOldData = strstr(pclData,"\n");
  if (pclOldData != NULL)
  {
    *pclOldData = 0x00;
    pclOldData++;
  } /* end if */
  else
  {
    pclOldData = pclOldDumm;
  } /* end else */

  strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
  pcgRecvName[sizeof(prlBchd->recv_name)] = 0x00;

  strncpy(pcgDestName, prlBchd->dest_name, sizeof(prlBchd->dest_name));
  pcgDestName[sizeof(prlBchd->dest_name)] = 0x00;

  strncpy(pcgTwStart, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start));
  pcgTwStart[sizeof(prlCmdblk->tw_start)] = 0x00;

  strncpy(pcgTwEnd, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end));
  pcgTwEnd[sizeof(prlCmdblk->tw_end)] = 0x00;

  dbg(TRACE,"CMD <%s> TBL <%s>", prlCmdblk->command,prlCmdblk->obj_name);
  dbg(TRACE,"FROM <%d> WKS <%s> USR <%s>",
      igQueOut,pcgRecvName,pcgDestName);
  dbg(TRACE,"SPECIAL INFO TWS <%s> TWE <%s>",pcgTwStart,pcgTwEnd);
  if (pclOldData != NULL)
    dbg(TRACE,"\nFLD <%s>\nDAT <%s>\nOLDDAT <%s>\nSEL <%s>",pclFld,pclData,pclOldData,pclSel);
  else
    dbg(TRACE,"\nFLD <%s>\nDAT <%s>\nSEL <%s>",pclFld,pclData,pclSel);
  CheckHomePort();

  strcpy(pclActCmd,prlCmdblk->command);
  sprintf(pclChkCmd,",%s,",pclActCmd);
  if (strstr(pcgCmdList,pclChkCmd) != NULL)
  {
    ilRC = HandleLogging(pclActCmd,prlCmdblk->obj_name,pclFld,pclData,pclOldData,pclSel);
  }
    return ilRC;
    
} /* end of HandleData */

/******************************************************************************/
/******************************************************************************/
static int HandleLogging(char *pcpCmd,char *pcpTbl,
             char *pcpFld,char *pcpDat,char *pcpOldDat,char *pcpSel)
{
    int ilRC = RC_SUCCESS, ilRc1;
    int ilTblNbr = 0;
    int ilTblCnt = 0;
    int ilFldCnt = 0;
    int ilFldNbr = 0;
    int ilItmNbr = 0;
    int ilFldLen = 0;
    int ilDatLen = 0;
    int ilOlDatLen = 0;
    int ilFldPos = 0;
    int ilDatPos = 0;
    int ilRecCnt = 0;
    int ilBoth = FALSE;
    int i, ilIdx;
    char ilInsert = FALSE;
    char ilWriteField = FALSE;
    char pclFldItm[128];
    char pclFldNam[128];
    char pclFldDat[4004];
    char pclFldOlDat[4004];
    char pclOutFld[4004];
    char pclOutDat[4004];
    char pclOutOldDat[4004];
    char *pclItem;
    char pclFkey[32];
    char pclAdid[2];
    char pclStox[16];
    char pclStoFldNam[8];
    char clLogD[8];
    char *pclDat2Use = pcpDat;

    ResetLogTab(FOR_WORK);
    CheckFields(pcpCmd, pcpTbl, pcpFld);
    ilTblCnt = argLogTable[0].Count;
    for (ilTblNbr = 1; ilTblNbr <= ilTblCnt; ilTblNbr++) 
    {
        if(argLogTable[ilTblNbr].Count > 0) 
        {
            ilFldCnt = argLogTable[ilTblNbr].Count;
            ilFldPos = 0;
            ilDatPos = 0;
            ilRecCnt = 0;

            if ( argLogTable[ilTblNbr].LogDest[0] ==  'R' )
            {   /* PRF7400: RMS Logging */
                if ( strstr ( pcpCmd, "DRT" ) )
                {
                    pclDat2Use = pcpOldDat;
                    dbg ( DEBUG, "HandleLogging: Detected delete event -> use old Data <%s>", pclDat2Use );
                }
                ilRc1 = RC_SUCCESS;
                for ( ilIdx=0; ilIdx<argLogTable[ilTblNbr].KeyfCnt; ilIdx++ )
                {
                    if ( argLogTable[ilTblNbr].KeyfData[ilIdx] )
                        *(argLogTable[ilTblNbr].KeyfData[ilIdx]) = '\0';
                    if ( strlen(argLogTable[ilTblNbr].KeyfName[ilIdx]) > 0 )
                        ilRc1 |= FillKeyField ( ilTblNbr, ilIdx, pcpFld, pclDat2Use );
                }
                dbg ( DEBUG, "HandleLogging: Key %d key fields for table <%s> filled, RC <%d>", 
                      argLogTable[ilTblNbr].KeyfCnt, argLogTable[ilTblNbr].TblName, ilRc1 );
                for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++ )
                {
                    get_real_item(pclFldItm,argLogTable[ilTblNbr].LogItem,ilFldNbr);
                    ilItmNbr = atoi(pclFldItm);
                    ilFldLen = get_real_item(pclFldNam,pcpFld,ilItmNbr);
                    if (ilFldLen < 1)
                    {
                        strcpy(pclFldNam," ");
                        ilFldLen = 1;
                    } 
                    ilDatLen = get_real_item(pclFldDat,pclDat2Use,ilItmNbr);
                    if (ilDatLen < 1)
                    {
                        strcpy(pclFldDat," ");
                        ilDatLen = 1;
                    } 
                    StrgPutStrg(pclOutFld,&ilFldPos,pclFldNam,0,(ilFldLen-1),pcgItemSep);
                    StrgPutStrg(pclOutDat,&ilDatPos,pclFldDat,0,(ilDatLen-1),pcgItemSep);
                } /* end for */
                if (ilFldPos > 0)
                {
                    ilFldPos--;
                    ilDatPos--;
                    pclOutFld[ilFldPos] = 0x00;
                    pclOutDat[ilDatPos] = 0x00;
                    /* NOW STORE REST OF DATA */
                    dbg(DEBUG,"STORE FLD <%s>",pclOutFld);
                    dbg(DEBUG,"STORE DAT <%s>",pclOutDat);
                    WriteToRxxTable( ilTblNbr,pcpCmd,pcpSel,pclOutFld,pclOutDat);
                } /* end if */
                
            }
            else 
            {
                if (igLogType == LOG_BOTH || argLogTable[ilTblNbr].LogDest[0] != 'H') 
                {   /*  PRF7640: conventional logging to be used if logd != RxxTAB and logd!=HxxTAB */
                    /*           also to be used if igLogType == LOG_BOTH and  logd ==HxxTAB */

                    strcpy ( clLogD, argLogTable[ilTblNbr].LogDest );
                    if ( argLogTable[ilTblNbr].LogDest[0] == 'H')
                        clLogD[0] = 'L';
                    for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
                    {
                        get_real_item(pclFldItm,argLogTable[ilTblNbr].LogItem,ilFldNbr);
                        ilItmNbr = atoi(pclFldItm);
                        ilFldLen = get_real_item(pclFldNam,pcpFld,ilItmNbr);
                        if (ilFldLen < 1)
                        {
                            strcpy(pclFldNam," ");
                            ilFldLen = 1;
                        } /* end if */
                        ilDatLen = get_real_item(pclFldDat,pcpDat,ilItmNbr);
                        if (ilDatLen < 1)
                        {
                            strcpy(pclFldDat," ");
                            ilDatLen = 1;
                        } /* end if */
                        if (((ilFldPos + ilFldLen + 1) > 2000) ||
                            ((ilDatPos + ilDatLen + 1) > 2000))
                        {
                            if (ilFldPos>0) ilFldPos--;
                            if (ilDatPos>0) ilDatPos--;
                            pclOutFld[ilFldPos] = 0x00;
                            pclOutDat[ilDatPos] = 0x00;
                            if (ilRecCnt < 1)
                            {
                                ilRecCnt = 1;
                            } /* end if */
                    
                            /* NOW STORE DATA */
                            dbg(DEBUG,"STORE REC %d",ilRecCnt);
                            dbg(DEBUG,"STORE FLD <%s>",pclOutFld);
                            dbg(DEBUG,"STORE DAT <%s>",pclOutDat);
                            WriteToTable(ilRecCnt,clLogD, pcpCmd,pcpTbl,pcpSel,pclOutFld,pclOutDat);
                            ilRecCnt++;
                            ilFldPos = 0;
                            ilDatPos = 0;
                      } /* end if */
                      StrgPutStrg(pclOutFld,&ilFldPos,pclFldNam,0,(ilFldLen-1),pcgItemSep);
                      StrgPutStrg(pclOutDat,&ilDatPos,pclFldDat,0,(ilDatLen-1),pcgItemSep);
                    } /* end for */
                    if (ilFldPos > 0)
                    {
                        ilFldPos--;
                        ilDatPos--;
                        pclOutFld[ilFldPos] = 0x00;
                        pclOutDat[ilDatPos] = 0x00;
                        /* NOW STORE REST OF DATA */
                        dbg(DEBUG,"STORE REC %d",ilRecCnt);
                        dbg(DEBUG,"STORE FLD <%s>",pclOutFld);
                        dbg(DEBUG,"STORE DAT <%s>",pclOutDat);
                        WriteToTable(ilRecCnt,clLogD, pcpCmd,pcpTbl,pcpSel,pclOutFld,pclOutDat);
                    } /* end if */
                }

                if ( ( igLogType == LOG_BOTH && (argLogTable[ilTblNbr].LogDest[0] ==  'L') )
                     || ( argLogTable[ilTblNbr].LogDest[0] ==  'H')  )
                {   /*  PRF7640: logging into HxxTAB if logd != RxxTAB and logd==HxxTAB */
                    /*           also to be used if igLogType == LOG_BOTH and  logd ==LxxTAB        */

                    strcpy ( clLogD, argLogTable[ilTblNbr].LogDest );
                    if ( argLogTable[ilTblNbr].LogDest[0] == 'L')
                        clLogD[0] = 'H'; 
                        /*
                        ** Handle new log format i.E. write to HXXTAB
                        ** insert: write each field to HXXTAB
                        ** update: write only changed fields
                        ** delete:
                        */
                    if (pcpOldDat == NULL) 
                        ilInsert = TRUE;
                    else 
                        ilInsert = FALSE;
                    for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++) 
                    {
                        get_real_item(pclFldItm,argLogTable[ilTblNbr].LogItem,ilFldNbr);
                        ilItmNbr = atoi(pclFldItm);
                        ilFldLen = get_real_item(pclFldNam,pcpFld,ilItmNbr);
                        if (ilFldLen < 1) 
                        {
                            strcpy(pclFldNam," ");
                            ilFldLen = 1;
                        } /* end if */
                        ilDatLen = get_real_item(pclFldDat,pcpDat,ilItmNbr);
                        if (!ilInsert) 
                        { /* update check new<->old */
                            ilOlDatLen = get_real_item(pclFldOlDat,pcpOldDat,ilItmNbr);
                            if (ilOlDatLen != ilDatLen || strncmp(pclFldDat,pclFldOlDat,ilDatLen)) 
                                ilWriteField = TRUE;
                            else 
                                ilWriteField = FALSE;
                        }
                        else ilWriteField = TRUE;
                        if (ilDatLen < 1) 
                        {
                            strcpy(pclFldDat," ");
                            ilDatLen = 1;
                        } /* end if */
                        if (ilWriteField) 
                        {
                            dbg(TRACE,"Writing Field <%s> to <%s> Old Data: <%s> New Data: <%s>",pclFldNam,clLogD,pclFldOlDat,pclFldDat);
                            if (!strcmp(pcpTbl, "AFTTAB") && (strstr(pcgDelList,pcpCmd) != NULL))
                            {
                                /* fetch FKEY from data */
                                pclItem = getItem(pcpFld,pcpDat,"FKEY");
                                if( pclItem != NULL ) 
                                {
                                    sprintf(pclFkey,"%s",pclItem);
                                }
                                else 
                                {
                                    sprintf(pclFkey," ");
                                    dbg(TRACE,"ERROR: Field FKEY not delivered by flight, add to DEL_FLD_LIST in flight.cfg");
                                }
                                /* fetch ADID from data */
                                pclItem = getItem(pcpFld,pcpDat,"ADID");
                                if( pclItem != NULL ) 
                                {
                                    sprintf(pclAdid,"%s",pclItem);
                                }
                                else 
                                {
                                    sprintf(pclAdid," ");
                                    dbg(TRACE,"ERROR: Field ADID not delivered by flight, add to DEL_FLD_LIST in flight.cfg");
                                }
                                if (!strcmp(pclAdid,"A")) 
                                {
                                    strcpy(pclStoFldNam, "STOA");
                                }
                                else if (!strcmp(pclAdid,"D") || !strcmp(pclAdid,"B")) 
                                { 
                                    strcpy(pclStoFldNam, "STOD"); 
                                }
                                pclItem = getItem(pcpFld,pcpDat,pclStoFldNam);
                                if( pclItem != NULL ) 
                                {
                                    sprintf(pclStox,"%14.14s",pclItem);
                                }
                                else 
                                {
                                    sprintf(pclStox," ");
                                    dbg(TRACE,"ERROR: Field %s not delivered by flight, add to DEL_FLD_LIST in flight.cfg",pclStox);
                                }
                            }
                            WriteToHxxTable(clLogD,pcpCmd,pcpTbl,pcpSel,pclFldNam,pclFldDat,pclFldOlDat,pclFkey,pclStox);
                        }
                    } /* end for */
                    if (ilFldPos > 0) 
                    {
                        dbg(TRACE,"Writing Field <%s> to <%s> Old Data: <%s> New Data: <%s>",pclFldNam,clLogD,pclFldOlDat,pclFldDat);
                    } /* end if */
                } 
            }
        }
    }
    return ilRC;
} /* end HandleLogging */

/******************************************************************************/
/******************************************************************************/
static void WriteToHxxTable(char *pcpTable, char *pcpCmd,char *pcpTbl, char *pcpSel,
                char *pcpField, char * pcpFieldData, char * pcpFieldOldData, char *pcpFkey, char *pcpStox)
{
  int ilGetRc = RC_SUCCESS;
  long llUrno = 0;
  int ilOrnoCnt = 0;
  int ilDatPos = 0;
  short slCursor = 0;
  short slFkt = 0;
  char pclTmpBuf[2048];
  char pclFldLst[2048];
  char pclTmpTim[64];
  char pclFldVal[4096];
  char pclSqlBuf[1024];
  char pclDataArea[1024];
  char *pclOrnoList = NULL;
  char *pclDefOrno = "\n0";
  char *pclLogFields = "UREF,FINA,KEYF,SEQN,TANA,TIME,TRAN,USEC,VALU,OVAL,URNO,STOX";
  char *pclLogValues = ":VUREF,:VFINA,:VKEYF,:VSEQN,:VTANA,:VTIME,:VTRAN,:VUSEC,:VVALU,:VOVAL,:VURNO,:VSTOX"; 
  char *pclItem;
  char errBuff[128];
  char pclAdid[2];
  char pclStoa[16];
  char pclStod[16];
  char pclStox[16];
  char pclFlno[16];
  char pclFldLstN[128];
  int  ilLen;

  dbg(TRACE,"Write Request: <%s> <%s> <%s> <%s> <%s> <%s>",pcpTable,pcpCmd,pcpTbl,pcpSel,pcpField,pcpFieldData);

    if (igUseLocalTimeStamp == FALSE)
    {
      GetServerTimeStamp("UTC",1,0,pclTmpTim);
    }
    else
    {
      GetServerTimeStamp("LOC",1,0,pclTmpTim);
    }

  pclOrnoList = strstr(pcpSel,"\n");
  if (pclOrnoList == NULL)
  {
    pclOrnoList = pclDefOrno;
  } /* end if */
  pclOrnoList++;
  /* ilOrnoCnt = field_count(pclOrnoList); */
  ilGetRc = getFreeUrno(pcpTable,pcgDataArea);
  if (ilGetRc == RC_SUCCESS)
  {
    llUrno = atol(pcgDataArea);
  } /* end if */
  else
  {
    debug_level = TRACE;
    dbg(TRACE,"ERROR: (WRITE TO TABLE) GET URNO FAILED !");
  } /* end else */
  
  strcpy(pclFldLst,pclLogFields);
  strcpy(pclFldVal,pclLogValues);
  sprintf(pcgSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",
             pcpTable,pclFldLst,pclFldVal);
  ilDatPos = 0;

  /* UREF */
  sprintf(pclTmpBuf,"%s",pclOrnoList);
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* FINA */
  StrgPutStrg(pcgDataArea,&ilDatPos,pcpField,0,-1,",");
  /* KEYF for AFT only */
  if (!strcmp(pcpTbl, "AFTTAB")) {
    if (strstr(pcgDelList,pcpCmd) != NULL) {
      sprintf(pclTmpBuf,"%s",pcpFkey);
      sprintf(pclStox,pcpStox);
    }
    else {
      /* fetch FKEY for that flight */
      sprintf(pclFldLstN,"FKEY,ADID,STOD,STOA,FLNO");
      sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB WHERE URNO='%s'",pclFldLstN,pclOrnoList);
      slCursor = 0;
      slFkt = START;
      ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
      if (ilGetRc == DB_SUCCESS){
    commit_work();
    BuildItemBuffer(pclDataArea,NULL,5,",");
    pclItem = getItem(pclFldLstN,pclDataArea,"FKEY");
    if (pclItem != 0) {
      /*sprintf(pclTmpBuf,"%s",pclItem);*/
       memset(pclTmpBuf,0x00,128);
       get_real_item(pclFlno,pclDataArea,5);
       strcpy(pclTmpBuf,pclItem);
       pclTmpBuf[5] = '\0';
       strcat(pclTmpBuf,pclFlno);
       pclTmpBuf[8] = '\0';
       strcat(pclTmpBuf,&pclItem[8]);
    }
    pclItem = getItem(pclFldLstN,pclDataArea,"ADID");
    if (pclItem != 0) {
      sprintf(pclAdid,"%s",pclItem);
    }
    pclItem = getItem(pclFldLstN,pclDataArea,"STOD");   if (pclItem != 0) {
      sprintf(pclStod,"%s",pclItem);
    }
    pclItem = getItem(pclFldLstN,pclDataArea,"STOA");
    if (pclItem != 0) {
      sprintf(pclStoa,"%s",pclItem);
    }
    if (!strcmp(pclAdid,"A")) {
      sprintf(pclStox, pclStoa);
    }
    else sprintf(pclStox, pclStod);
      }
      else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "%05d:ERROR: (WRITE TO TABLE) Error getting Oracle data error <%s>",__LINE__, &errBuff[0]);
    }
    rollback();
    dbg(TRACE,"ERROR: (WRITE TO TABLE) Fetching of FKEY failed");
    sprintf(pclTmpBuf," ");
      }
      close_my_cursor(&slCursor);
    }
  }
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* SEQN */
  sprintf(pclTmpBuf,"0");
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* TANA */
  sprintf(pclTmpBuf,"%3.3s",pcpTbl);
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* TIME */
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpTim,0,-1,",");
  /* TRAN */
  sprintf(pclTmpBuf,"%1.1s",pcpCmd);
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* USEC */
  StrgPutStrg(pcgDataArea,&ilDatPos,pcgDestName,0,-1,",");
  /* VALU */
  StrgPutStrg(pcgDataArea,&ilDatPos,pcpFieldData,0,-1,",");
  /* VALO */
  StrgPutStrg(pcgDataArea,&ilDatPos,pcpFieldOldData,0,-1,",");
  /* URNO */
  sprintf(pclTmpBuf,"%d",llUrno);
  StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
  /* STOX */
  StrgPutStrg(pcgDataArea,&ilDatPos,pclStox,0,-1,",");
  ilDatPos--;
  pcgDataArea[ilDatPos] = 0x00;

	strcpy(pcgDataAreaOld,pcgDataArea);
  delton(pcgDataArea);
  slCursor = 0;
  ilGetRc = sql_if(0,&slCursor,pcgSqlBuf,pcgDataArea);
  if (ilGetRc != DB_SUCCESS && ilGetRc != NOTFOUND) {
    get_ora_err(ilGetRc, &errBuff[0]);
    dbg (TRACE, "%05d:<getOrPutDBData> Error getting Oracle data error <%s>",__LINE__, &errBuff[0]);
		dbg(TRACE,"DATA: <%s>",pcgDataAreaOld);
    rollback();
  }
  if (ilGetRc == DB_SUCCESS) commit_work();
  close_my_cursor(&slCursor);
}
/******************************************************************************/
/******************************************************************************/
static void WriteToTable(int ipRecCnt, char *pcpTable,
    char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpOutFld, char *pcpOutDat)
{
  int ilGetRc = RC_SUCCESS;
  long llUrno = 0;
  int ilOrnoCnt = 0;
  int ilRecNbr = 0;
  int ilLen = 0;
  int ilDatPos = 0;
  int ilUseHopo = TRUE;
  short slCursor = 0;
  char *pclOrnoList = NULL;
  char *pclDefOrno = "\n0";
  char *pclLogFields = "URNO,ORNO,MORE,STAB,SFKT,FLST,FVAL,USEC,TIME"; 
  char *pclLogValues = ":VURNO,:VORNO,:VMORE,:VSTAB,:VSFKT,:VFLST,:VFVAL,"
               ":VUSEC,:VTIME"; 
  char pclOrno[16];
  char pclTmpBuf[2048];
  char pclTmpTim[64];
  char pclFldLst[2048];
  char pclFldVal[2048];
  char pclSeparateUrnoTables[1024];  /* Mei */

  if (strstr(pcgUfisTables,pcpTable) != NULL)
  {
    ilUseHopo = TRUE;
    sprintf(pclFldLst,"%s,HOPO",pclLogFields);
    sprintf(pclFldVal,"%s,:VHOPO",pclLogValues);
  } /* end if */
  else
  {
    ilUseHopo = FALSE;
    strcpy(pclFldLst,pclLogFields);
    strcpy(pclFldVal,pclLogValues);
  } /* end else */

    if (igUseLocalTimeStamp == FALSE)
    {
      GetServerTimeStamp("UTC",1,0,pclTmpTim);
    }
    else
    {
      GetServerTimeStamp("LOC",1,0,pclTmpTim);
    }

  pclOrnoList = strstr(pcpSel,"\n");
  if (pclOrnoList == NULL)
  {
    pclOrnoList = pclDefOrno;
  } /* end if */
  pclOrnoList++;
  ilOrnoCnt = field_count(pclOrnoList);

  /*** Mei ***/
  llUrno = -1;
  strcpy( pclSeparateUrnoTables, pcgSeparateUrnoTables );   /* In case string is flush off */
	dbg(DEBUG,"TAble: <%s>, Separate Urno Tables <%s>",pcpTable,pclSeparateUrnoTables);
  if( strstr( pclSeparateUrnoTables, pcpTable ) != NULL )
  {
      llUrno = NewUrnos( pcpTable, ilOrnoCnt );
      sprintf( pcgDataArea, "%d", llUrno );
      ilGetRc = RC_SUCCESS;
  }
  if( llUrno <= 0 )
  {
      ilGetRc = GetNextValues(pcgDataArea, ilOrnoCnt);
      if (ilGetRc == RC_SUCCESS)
      {
          llUrno = atol(pcgDataArea);
      } /* end if */
      else
      {
           debug_level = TRACE;
           dbg(TRACE,"ERROR: (WRITE TO TABLE) GET URNO FAILED !");
           return;  /* Mei */
      } /* end else */
  }

  for (ilRecNbr = 1; ilRecNbr <= ilOrnoCnt; ilRecNbr++)
  {
    ilLen = get_real_item(pclOrno,pclOrnoList,ilRecNbr);
    if (ilLen > 0)
    {
      sprintf(pcgSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",
             pcpTable,pclFldLst,pclFldVal);
             ilDatPos = 0;

                 /* "URNO" */
      sprintf(pclTmpBuf,"%d",llUrno);
      StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
      llUrno++;
                 /* "ORNO" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pclOrno,0,-1,",");
                 /* "MORE" */
      sprintf(pclTmpBuf,"%d",ipRecCnt);
      StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
                 /* "STAB" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pcpTbl,0,-1,",");
                 /* "SFKT" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pcpCmd,0,-1,",");
                 /* "FLST" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pcpOutFld,0,-1,",");
                 /* "FVAL" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pcpOutDat,0,-1,",");
                 /* "USEC" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pcgDestName,0,-1,",");
                 /* "TIME" */
      StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpTim,0,-1,"000,");
      if (ilUseHopo == TRUE)
      {
        StrgPutStrg(pcgDataArea,&ilDatPos,pcgH3LC,0,-1,","); 
        StrgPutStrg(pcgDataArea,&ilDatPos,"ATH",0,-1,",");
      } /* end if */
      ilDatPos--;
      pcgDataArea[ilDatPos] = 0x00;
      dbg(DEBUG,"<%s>",pcgSqlBuf);
      dbg(DEBUG,"<%s>",pcgDataArea);
      delton(pcgDataArea);
      slCursor = 0;
      ilGetRc = sql_if(0,&slCursor,pcgSqlBuf,pcgDataArea);
      commit_work();
      close_my_cursor(&slCursor);
    } /* end if */
  } /* end for */
  return;
} /* end WriteToTable */

/******************************************************************************/
/******************************************************************************/
static void CheckFields(char *pcpCmd, char *pcpTbl, char *pcpFld)
{
  int ilTblNbr = 0;
  int ilTblCnt = 0;
  int ilFirstTable = 0;
  int ilLastTable = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  char pclFldNam[8];
  char pclFldItm[8];

  ilTblNbr = 1;
  ilTblCnt = argLogTable[0].Count;
  while ((ilTblNbr <= ilTblCnt) && (ilLastTable < 1))
  {
    if (ilFirstTable < 1)
    {
      if (strcmp(argLogTable[ilTblNbr].TblName,pcpTbl) == 0)
      {
    ilFirstTable = ilTblNbr;
      } /* end if */
    } /* end if */
    else
    {
      if (strcmp(argLogTable[ilTblNbr].TblName,pcpTbl) != 0)
      {
    ilLastTable = ilTblNbr - 1;
      } /* end if */
    } /* end else */
    ilTblNbr++;
  } /* end for */

  if (ilFirstTable > 0)
  {
    /* HAG20050826: fields of deleted RMS data shall be logged */
    if (( argLogTable[ilFirstTable].LogDest[0] != 'R') &&
        (strstr(pcgDelList,pcpCmd) != NULL) || (strlen(pcpFld) == 0) )
    {
      argLogTable[ilFirstTable].Count = 1;
      strcpy(argLogTable[ilFirstTable].LogItem,"0");
    } /* end if */
    else
    {
      if (ilLastTable < 1)
      {
        ilLastTable = ilFirstTable;
      } /* end if */
      ilFldCnt = field_count(pcpFld);
      for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
      {
        get_real_item(pclFldNam,pcpFld,ilFldNbr);
        ilTblNbr = ilFirstTable;
        while (ilTblNbr <= ilLastTable)
        {
      if (strstr(argLogTable[ilTblNbr].LogList,pclFldNam) != NULL)
      {
        sprintf(pclFldItm,"%d",ilFldNbr);
        argLogTable[ilTblNbr].Count++;
        if (argLogTable[ilTblNbr].Count == 1)
        {
          strcpy(argLogTable[ilTblNbr].LogItem,pclFldItm);
        } /* end if */
        else
        {
          strcat(argLogTable[ilTblNbr].LogItem,",");
          strcat(argLogTable[ilTblNbr].LogItem,pclFldItm);
        } /* end else */
        ilTblNbr = ilLastTable;
      } /* end if */
      ilTblNbr++;
        } /* end while */
      } /* end for */
    } /* end else */
  } /* end if */

  return;
} /* end CheckFields */

/********************************************************/
/********************************************************/
static void CheckHomePort(void)
{
  int ilGetRc = RC_SUCCESS;
  int ilTblNbr = -1;
  int ilHomLen = 0;
  int ilExtLen = 0;
  char pclHome[8];
  char pclExts[8];
  char pclTmp[8];

  if (strcmp(pcgDefTblExt,"TAB") == 0)
  {
    /* First Check Table Extension */
    /* Because We Need It Later */
    ilExtLen = get_real_item(pclExts,pcgTwEnd,2);
    if (ilExtLen != 3)
    {
      strcpy(pclExts,pcgDefTblExt);
    } /* end if */

/* QuickHack */
strcpy(pclExts,pcgDefTblExt);

    if (strcmp(pclExts,pcgTblExt) != 0)
    {
      strcpy(pcgTblExt,pclExts);
      dbg(TRACE,"EXTENSION SET TO <%s>",pcgTblExt);
    } /* end if */

    /* Now Check HomePort */
    ilHomLen = get_real_item(pclHome,pcgTwEnd,1);
    if (ilHomLen != 3)
    {
      strcpy(pclHome,pcgDefH3LC);
    } /* end if */
    if (strcmp(pclHome,pcgH3LC) != 0)
    {
      strcpy(pcgH3LC,pclHome);
      dbg(TRACE,"HOMEPORT SET TO <%s>",pcgH3LC);
    } /* end if */
  } /* end if */

  return;
} /* end CheckHomePort */

/******************************************************************************/
/******************************************************************************/
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection, 
    char *pcpEntry, char *pcpDefVar)
{
    int ilRc = RC_SUCCESS;
    ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpEntry,1,pcpVar);
    if( ilRc != RC_SUCCESS || strlen(pcpVar) <= 0 )
    {
        strcpy(pcpVar,pcpDefVar); /* copy the default value */
        ilRc = RC_FAIL;
    }
    return ilRc;
} /* end ReadCfg() */

/******************************************************************************/
/******************************************************************************/
static void InitLogFileModes(void)
{
  GetLogFileMode(&igStartUpMode,"STARTUP_MODE","OFF");
  GetLogFileMode(&igRuntimeMode,"RUNTIME_MODE","OFF");
  debug_level = igStartUpMode;
  return;
} /* end InitLogFileModes */


/******************************************************************************/
/******************************************************************************/
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  char pclTmp[128];
  ReadCfg(cgConfigFile,pclTmp,"SYSTEM",pcpLine,pcpDefault);
  if (strcmp(pclTmp,"TRACE")==0)
  {
    *ipModeLevel = TRACE;
  } else if (strcmp(pclTmp,"DEBUG")==0) {
    *ipModeLevel = DEBUG;
  } /* end else if */
  else
  {
    *ipModeLevel = 0;
  } /* end else */
  return;
} /* end GetLogFileMode */


/******************************************************************************/
/* getFreeUrno                                                                */
/*                                                                            */
/* Maintains a pool of URNOS. If no more URNOS are available, the pool is     */
/* refilled with a set of URNO_POOL_SIZE.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       Nothing                                                              */
/*                                                                            */
/* Return:                                                                    */
/*       cpUrno -- the buffer pointed to is filled with a new URNO            */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
/*
** URNO pool
*/
static int getFreeUrno(char *pcpTable,char *cpUrno)
{
  static unsigned int ilNumUrnos = 0;
  static unsigned int ilFirstUrno = 0;
  static unsigned int ilNextUrno = 0;
  char clNewUrno[URNO_SIZE];
  int ilRc;
  char pclSeparateUrnoTables[1024];  /* Mei */
  long llUrno = 0;


  if (ilNumUrnos == 0) {


  /*** Mei/Mcu ***/
  llUrno = -1;
  strcpy( pclSeparateUrnoTables, pcgSeparateUrnoTables );   /* In case string is flush off */
	dbg(DEBUG,"TAble: <%s>, Separate Urno Tables <%s>",pcpTable,pclSeparateUrnoTables);
  if( strstr( pclSeparateUrnoTables, pcpTable ) != NULL )
  {
      llUrno = NewUrnos( pcpTable, URNO_POOL_SIZE );
      sprintf( pcgDataArea, "%d", llUrno );
      ilRc = RC_SUCCESS;
  }
  if( llUrno <= 0 )
  {
      ilRc = GetNextValues(pcgDataArea, URNO_POOL_SIZE);
      if (ilRc == RC_SUCCESS)
      {
          llUrno = atol(pcgDataArea);
      		ilNumUrnos = URNO_POOL_SIZE;
      } /* end if */
      else
      {
           debug_level = TRACE;
           dbg(TRACE,"ERROR: (WRITE TO TABLE) GET URNO FAILED !");
           return RC_FAIL;  /* Mei */
      } /* end else */
  }
	else
	{
      		ilNumUrnos = URNO_POOL_SIZE;
	}


#if 0
   /*** old get Urnos from SNOTAB **/
    ilRc = GetNextValues (clNewUrno, URNO_POOL_SIZE);
    if (ilRc == RC_SUCCESS) {
      ilFirstUrno = atoi(clNewUrno);
      ilNextUrno = ilFirstUrno;

      /* MEI: amend code due to bug here. 02-Oct-07 */
      /*ilNumUrnos = 100; */
      ilNumUrnos = URNO_POOL_SIZE;

      dbg( TRACE, "getFreeUrno: ilNumUrnos = %d ilFirstUrno = %d\n", ilNumUrnos, ilFirstUrno );
    }
    else return RC_FAIL;
	#endif
  }
  sprintf(cpUrno, "%d", ilNextUrno);
  ilNextUrno++;
  ilNumUrnos--;
  dbg(DEBUG,"<getFreeUrno> URNO  from <%s> First:<%d> Next:<%d> Num:<%d>", pcpTable,
      ilFirstUrno, ilNextUrno, ilNumUrnos);
  return RC_SUCCESS;
}

static int FillKeyField ( int ipTblNbr, int ipKeyIdx, char *pcpFld, char *pcpDat )
{
    int ilRc = RC_SUCCESS;
    int  ilFldCnt, i, ilLen, ilPos;
    char clFina[11];
    char clBuffer[KEYF_LEN+1], clTmp[KEYF_LEN+1];
    int ilDatPos=0;
    
    if ( (ipKeyIdx < 0) ||  (ipKeyIdx >= argLogTable[ipTblNbr].KeyfCnt) )
    {
        dbg ( TRACE, "FillKeyField: invalid KeyIdx <%d> Table has <%d> key fields",  
                      ipKeyIdx, argLogTable[ipTblNbr].KeyfCnt ) ;
        return  RC_FAIL;
    }
    ilFldCnt = get_no_of_items(argLogTable[ipTblNbr].KeyfField[ipKeyIdx] );
    if ( ilFldCnt < 1 )
    {
        dbg ( TRACE, "FillKeyField: no fields defined for Keyf <%d> <%s> List <%s>",  
                      ipKeyIdx, argLogTable[ipTblNbr].KeyfName[ipKeyIdx], 
                      argLogTable[ipTblNbr].KeyfField[ipKeyIdx] ) ;
        return  RC_FAIL;
    }
    clBuffer [0]; '\0';
    for ( i=0; (i<ilFldCnt)&& (ilRc==RC_SUCCESS); i++ )
    {
        ilLen = get_real_item(clFina, argLogTable[ipTblNbr].KeyfField[ipKeyIdx], i+1 );
        if ( ilLen<= 0 )
        {
            dbg ( TRACE, "FillKeyField: Failed to read %d. field name from <%s>",
                 i, argLogTable[ipTblNbr].KeyfField[ipKeyIdx] );
            ilRc = RC_FAIL;
        }
        else
        {
            ilPos = get_item_no(pcpFld, clFina, 5);
            if (ilPos < 0)
            {
                dbg ( TRACE, "FillKeyField: ipKeyIdx <%d> field <%s> not in field list <%s> ", 
                      ipKeyIdx, clFina, pcpFld );
                ilRc = RC_FAIL;
            }
            else
            {               
                ilLen = get_real_item(clTmp,pcpDat,ilPos+1);
                if (ilLen > 1)
                {
                    ilLen = CT_TrimRight( clTmp, ' ', ilLen );
                }
                if ( ilLen < 1 )
                {
                    strcpy(clTmp," ");
                    ilLen = 1;
                }
                StrgPutStrg(clBuffer,&ilDatPos,clTmp,0,(ilLen-1),argLogTable[ipTblNbr].KeyfSep);
            }               
        }
    }
    if (ilDatPos > 0)
    {
        ilDatPos--;
        clBuffer[ilDatPos] = '\0';
    } /* end if */

    if ( ilRc == RC_SUCCESS )
    {
        if ( !argLogTable[ipTblNbr].KeyfData[ipKeyIdx] )
            argLogTable[ipTblNbr].KeyfData[ipKeyIdx] = malloc ( KEYF_LEN+1 );
        if ( !argLogTable[ipTblNbr].KeyfData[ipKeyIdx] )
        {
            ilRc = RC_FAIL;
            dbg ( TRACE, "FillKeyField: Unable to allcoate memory for key field <%s>",
                  argLogTable[ipTblNbr].KeyfName[ipKeyIdx] );
        }
        else
        {
            strcpy ( argLogTable[ipTblNbr].KeyfData[ipKeyIdx], clBuffer );
        }
    }
    if ( ilRc == RC_SUCCESS )
    {
        dbg ( DEBUG, "FillKeyField: Idx <%d> Name <%s> Fields <%s> Data <%s> OK",
              ipKeyIdx, argLogTable[ipTblNbr].KeyfName[ipKeyIdx], 
              argLogTable[ipTblNbr].KeyfField[ipKeyIdx], 
              argLogTable[ipTblNbr].KeyfData[ipKeyIdx] );
    }
    else
        dbg ( TRACE, "FillKeyField: Idx <%d> failed, RC <%d>", ipKeyIdx, ilRc );

    return ilRc;

}

static void WriteToRxxTable( int ipTblNbr, char *pcpCmd, char *pcpSel, char *pcpOutFld, char *pcpOutDat)
{
    int ilGetRc = RC_SUCCESS;
    int i, ilOrnoCnt = 0;
    int ilRecNbr = 0;
    int ilLen = 0;
    int ilDatPos = 0;
    int ilUseHopo = TRUE;
    short slCursor = 0;
    char *pclOrnoList = NULL;
    char *pclDefOrno = "\n0";
    char *pclLogFields = "URNO,ORNO,STAB,SFKT,FLST,FVAL,USEC,TIME"; 
    char *pclLogValues = ":VURNO,:VORNO,:VSTAB,:VSFKT,:VFLST,:VFVAL,:VUSEC,:VTIME"; 
    char pclOrno[16];
    char pclTmpBuf[2048];
    char pclTmpTim[64];
    char pclFldLst[2048];
    char pclFldVal[2048];
    char pclKeysVal[2048];
    char pclTmp[128];

    if (strstr(pcgUfisTables,argLogTable[ipTblNbr].TblName) != NULL)
    {
        ilUseHopo = TRUE;
        sprintf(pclFldLst,"%s,HOPO",pclLogFields);
        sprintf(pclFldVal,"%s,:VHOPO",pclLogValues);
    } /* end if */
    else
    {
        ilUseHopo = FALSE;
        strcpy(pclFldLst,pclLogFields);
        strcpy(pclFldVal,pclLogValues);
    } 
    /*  Handle Key fields */
    pclKeysVal[0]='\0';
    for ( i=0; i<argLogTable[ipTblNbr].KeyfCnt; i++ )
    {
        if ( strlen(argLogTable[ipTblNbr].KeyfName[i]) > 0 )
        {
            strcat ( pclFldLst, "," );
            strcat ( pclFldLst, argLogTable[ipTblNbr].KeyfName[i] );
            sprintf ( pclTmp, ",:V%s", argLogTable[ipTblNbr].KeyfName[i] );
            strcat ( pclFldVal, pclTmp );
            if ( strlen(argLogTable[ipTblNbr].KeyfData[i]) > 0 )
                sprintf ( pclTmp, "%s,", argLogTable[ipTblNbr].KeyfData[i] );
            else
                strcpy ( pclTmp, " ," );
            strcat ( pclKeysVal, pclTmp );
        }
    }

    if (igUseLocalTimeStamp == FALSE)
    {
      GetServerTimeStamp("UTC",1,0,pclTmpTim);
    }
    else
    {
      GetServerTimeStamp("LOC",1,0,pclTmpTim);
    }

    pclOrnoList = strstr(pcpSel,"\n");
    if (pclOrnoList == NULL)
    {
      pclOrnoList = pclDefOrno;
    } /* end if */
    pclOrnoList++;
    ilOrnoCnt = field_count(pclOrnoList);
    
    for (ilRecNbr = 1; (ilRecNbr <= ilOrnoCnt)&&(ilGetRc==RC_SUCCESS); ilRecNbr++)
    {
        ilLen = get_real_item(pclOrno,pclOrnoList,ilRecNbr);
        if (ilLen > 0)
        {
            sprintf(pcgSqlBuf,"INSERT INTO %s (%s) VALUES (%s)", 
                    argLogTable[ipTblNbr].LogDest,pclFldLst,pclFldVal);
            ilDatPos = 0;
        
                        /* "URNO" */
            ilGetRc  = GetRxxUrno(pclTmpBuf);
            if ( ilGetRc != RC_SUCCESS )
            {
                debug_level = TRACE;
                dbg(TRACE,"ERROR: (WRITE TO TABLE) GET URNO FAILED !");
            } /* end else */
        
            StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpBuf,0,-1,",");
                       /* "ORNO" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pclOrno,0,-1,",");
                       /* "STAB" */
            StrgPutStrg(pcgDataArea,&ilDatPos,argLogTable[ipTblNbr].TblName,0,-1,",");
                       /* "SFKT" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pcpCmd,0,-1,",");
                       /* "FLST" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pcpOutFld,0,-1,",");
                       /* "FVAL" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pcpOutDat,0,-1,",");
                       /* "USEC" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pcgDestName,0,-1,",");
                       /* "TIME" */
            StrgPutStrg(pcgDataArea,&ilDatPos,pclTmpTim,0,-1,"000,");
            if (ilUseHopo == TRUE)
            {
                StrgPutStrg(pcgDataArea,&ilDatPos,pcgH3LC,0,-1,","); 
            } /* end if */
            StrgPutStrg(pcgDataArea,&ilDatPos,pclKeysVal,0,-1,","); 
            ilDatPos--;
            pcgDataArea[ilDatPos] = 0x00;
            dbg(DEBUG,"<%s>",pcgSqlBuf);
            dbg(DEBUG,"<%s>",pcgDataArea);
            delton(pcgDataArea);
            slCursor = 0;
            ilGetRc = sql_if(0,&slCursor,pcgSqlBuf,pcgDataArea);
            commit_work();
            close_my_cursor(&slCursor);
        } /* end if */
    } /* end for */
    return;
} /* end WriteToRxxTable */


static int GetRxxUrno(char *cpUrno)
{
    static unsigned int ilNumRxxUrnos = 0;
    static unsigned int ilFirstRxxUrno = 0;
    static unsigned int ilNextRxxUrno = 0;
    char clNewUrno[URNO_SIZE];
    int ilRc;

    if (ilNumRxxUrnos == 0) 
    {
        ilRc = GetNextNumbers("R0XTAB",clNewUrno,URNO_POOL_SIZE,"GNV");
        if (ilRc == RC_SUCCESS) 
        {
            ilFirstRxxUrno = atoi(clNewUrno);
            ilNextRxxUrno = ilFirstRxxUrno;
            ilNumRxxUrnos = URNO_POOL_SIZE;
        }
        else 
            return RC_FAIL;
    }
    sprintf(cpUrno, "%d", ilNextRxxUrno);
    ilNextRxxUrno++;
    ilNumRxxUrnos--;
    dbg(DEBUG,"GetRxxUrno: URNO First:<%d> Next:<%d> Num:<%d>", 
              ilFirstRxxUrno, ilNextRxxUrno, ilNumRxxUrnos);
  return RC_SUCCESS;
}


/******************************************************************************/

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
