# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/count_null_values.ksh 1.3 2004/09/01 22:47:08SGT jim Exp  $
#
# 20031208 JIM: using $CEDADBUSER/$CEDADBPW instead of fix settings
#
function doc
{
cat <<EOFDOC1

====== 
Title: 
====== 
 
Count NULL values in CEDA datamodel via

   select count(AATO) from ACCTAB where AATO is NULL ;

 
 
============= 
Requirements: 
============= 
 
SELECT on 'user_tab_columns' 

============= 
Author: 
============= 
Jibbo Mueller

 
EOFDOC1
}

if [ ! -z "$1" ] ; then
   WHERE="WHERE TABLE_NAME=upper('$1')"
fi

# ==============================================================
function sql_grep
{
   $* | grep -v "SP2-0310: unable to open file" | grep -v "^$" | grep -v selected 
}


function sub_get_field_and_table
{

sqlplus -s $CEDADBUSER/$CEDADBPW @<<EOSQL
SET PAGES 0
SET LINESIZE 1024
SELECT 'select '''||TABLE_NAME||'/'||COLUMN_NAME||': ''||count('||COLUMN_NAME||') from '||TABLE_NAME||' where '||COLUMN_NAME||' is NULL;'
FROM user_tab_columns  
-- where column_type <> long
${WHERE}
ORDER BY TABLE_NAME,COLUMN_NAME;
quit;
EOSQL
}

function get_field_and_table
{
   sql_grep sub_get_field_and_table
}

# ==============================================================
function for_all
{
sqlplus -s $CEDADBUSER/$CEDADBPW @<<EOSQL
SET PAGES 0
SET LINESIZE 1024
`get_field_and_table`
quit
EOSQL
}

case "$1" in
   "-?"|"-h"|"-help"|"--h"|"--help")
       doc
       echo  ==============================================================
       echo  ==============================================================
       exit
       ;;
   *)  sql_grep for_all | tee /ceda/tmp/`basename $0`.log
       echo "========================================================"
       echo "ANFANG: grep in der Log-Datei nach Anzahl>0 NULL values:"
       echo "========================================================"
       grep -v ": 0" /ceda/tmp/`basename $0`.log
       echo "========================================================"
       echo "ENDE:    grep in der Log-Datei nach Anzahl>0 NULL values"
       echo "========================================================"
       ;;
esac
