CREATE OR REPLACE PACKAGE PCK_CEDA_HOUSEKEEPING
AS
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Package              - PACKAGE PCK_CEDA_HOUSEKEEPING
--
--      Version               - 1.0.0
--
--      Written By            - Jurgen Hammerschmid
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Change Log
--
--      Name           Date               Version    Change
--      ----           ----------         -------    ------
--      Hammerschmid   15.11.2007         1.0.0      new
--      Hammerschmid   19.08.2008		  1.0.4		 Calculate records to be archived
--													 deleted once
--      Hammerschmid   04.08.2009         1.1        Option archive in flatfile implemented
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

  -- Author  : JHA
  -- Created : 15.11.2007 13:05:12
  -- Purpose : Archiving of tables

  -- Public type declarations
  --TYPE <TypeName> is <Datatype>;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

	v_archive_records               NUMBER;
	v_deleted_records               NUMBER;
    v_Inclause_records				CONSTANT INTEGER := 50;

  -- Public procedure declarations
  	PROCEDURE PRC_START_HOUSEKEEPING;

	PROCEDURE PRC_HOUSEKEEPING_JOB (p_HOUROFDAY IN VARCHAR2);

  -- Public function declarations
  	FUNCTION FUN_ARCHIVE_AT_ONCE 	(p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
									 p_where_clause IN CEDA_Housekeeping.Table_Name%TYPE,
									 p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
									 p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
									 p_MaxRecords IN CEDA_Housekeeping.Max_Records_For_Select%TYPE,
                                     p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                     p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_ARCHIVE_RECORD_BY_RECORD  (p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
											p_where_clause IN CEDA_Housekeeping.Table_Name%TYPE,
									 		p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
											p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
											p_MaxRecords IN CEDA_Housekeeping.Max_Records_For_Select%TYPE,
                                            p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                            p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_CREATE_URNO_TEMP 	(p_FromTable 	IN VARCHAR2,
    								 p_WhereClause 	IN VARCHAR2,
                                     i_Records		OUT NUMBER)
	RETURN INTEGER;

	FUNCTION FUN_ARCHIVE_WITH_TEMP_TABLE	(p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
									 		 p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
									 		 p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
                                             p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                             p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
    RETURN INTEGER;

	FUNCTION FUN_ARCHIVE_WITH_INCLAUSE	(p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
						 		 		 p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
						 		 		 p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
						 		 		 p_InClause IN VARCHAR2,
                                         p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                         p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
    RETURN INTEGER;

	FUNCTION  FUN_SQLINFLATFILE	( p_table     IN VARCHAR2,
    						  	  p_query     IN VARCHAR2,
                          	  	  p_separator IN VARCHAR2 DEFAULT ',',
                          	  	  p_dir       IN VARCHAR2,
                          	  	  p_filename  IN VARCHAR2 )
	RETURN NUMBER;

	FUNCTION FUN_JOBSRUNNING
	RETURN INTEGER;

	FUNCTION FUN_PARSE_CEDA_HOUSEKEEPING
	RETURN INTEGER;

	FUNCTION FUN_GET_NEXT_SEQUENCE_ID
	RETURN NUMBER;

	FUNCTION FUN_TRUNCATE_TABLE (p_Table IN VARCHAR2)
	RETURN INTEGER;

END PCK_CEDA_HOUSEKEEPING;
/
CREATE OR REPLACE PACKAGE BODY PCK_CEDA_HOUSEKEEPING
AS

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Package Body      - PCK_CEDA_HOUSKEEPING
--
--  Version           - 1.0.3
--
--  Written By        - Jurgen Hammerschmid
--
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Change Log
--
--  Name           Date               Version    Change
--  ----           ----------         -------    ------
--  Hammerschmid   15.11.2007         1.0        new
--  Hammerschmid   04.08.2009         1.1        Option archive in flatfile implemented
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--      Procedure                 - PRC_START_HOUSEKEEPING
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_START_HOUSEKEEPING
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_START_HOUSEKEEPING
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   15.11.2007         1.0.0      new
    --		Hammerschmid   19.12.2007		  1.0.1		 changes in logfile to minimize
    --													 entries
    --		Hammerschmid   28.12.2007         1.0.2      Insert error messages to be locked
    --      Hammerschmid   10.01.2008         1.0.3      Insert error in arch log are locked
    --      Hammerschmid   19.08.2008		  1.0.4		 Calculate records to be archived
    --													 deleted once
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_tables
	IS
		SELECT --PRC_START_HOUSEKEEPING__active_tables
        type, table_name, where_clause,
        archive_table, max_records_for_select, records_at_once,
        urno_temp, flat_file, flat_file_dir
		FROM   ceda_housekeeping
		WHERE  active = 'Y'
		ORDER  BY run_sequence;

	r_Table					c_tables%ROWTYPE;

	v_SQLString				VARCHAR2(512);
	v_Records				NUMBER(9);
	v_Error					INTEGER;
	v_TempUrno				BOOLEAN := FALSE;
    v_Seq_id				NUMBER(12);

	BEGIN
		FOR r_table IN c_tables LOOP

        	-- archive only if r_Table.Max_Records_For_Select > 0
            -- otherwise you will get a deadlock
            IF r_Table.Max_Records_For_Select > 0 THEN

                -- If we are working with temp tables than we can get the
                -- number of records out of the function FUN_CREATE_URNO_TEMP
                IF upper(trim(r_table.urno_temp)) = 'J' OR
                   upper(trim(r_table.urno_temp)) = 'Y' THEN
                   v_TempUrno := TRUE;
                   v_Error := FUN_CREATE_URNO_TEMP(r_table.table_name,
                                                   r_table.where_clause,
                                                   v_Records);
                    IF v_Error != -1 THEN
                        v_Records := 0;
                    END IF;
                ELSE
                    -- otherwise we have to calculate as following
                    -- count(0) seems to be a little bit faster then count(*)
                    -- but even better to appand max_records_for_select
                    v_SQLString := ' SELECT /* PRC_START_HOUSEKEEPING__rows_table */ '
                                    || ' COUNT(0) '
                                    || ' FROM ' || r_table.table_name
                                    || ' WHERE ' || r_table.where_clause;

                    EXECUTE IMMEDIATE v_SQLString INTO v_Records;
                END IF;

                WHILE v_Records > 0 LOOP
                    -- next package to archive or delete?
                    -- e.g. it is possible to run 10 sequences of 10 records to
                    -- archive or delete 100 records. Depends on max_records_for select
                    -- get next sequence
                    v_Seq_id := FUN_GET_NEXT_SEQUENCE_ID;

                    BEGIN
                        INSERT INTO CEDA_arch_log
                        VALUES (v_seq_id, r_table.type, r_table.table_name,
                                r_table.where_clause, r_table.archive_table,
                                sysdate, null, 0);
                        COMMIT;
                    EXCEPTION
                        WHEN OTHERS THEN
                             ROLLBACK;
                             pck_ceda_errors.prc_write_error('PRC_START_HOUSEKEEPING ', 'ARCHIVE-80000',
                                                            'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                                            SUBSTR(SQLERRM, 1, 150) || ' Error in Insert CEDA_arch_log', 1);
                    END;

                    -- decide which function should be used
                    IF v_TempUrno = TRUE THEN
                        -- Archive / delete with temp table
                        v_Error := FUN_ARCHIVE_WITH_TEMP_TABLE	(r_table.table_name,
                                                                 r_table.archive_table,
                                                                 r_table.type,
                                                                 r_table.flat_file,
                                                                 r_table.flat_file_dir);
                    ELSE
                        IF v_Records > r_table.records_at_once THEN
                            v_Error := FUN_ARCHIVE_RECORD_BY_RECORD	(r_table.table_name,
                                                                     r_table.where_clause,
                                                                     r_table.archive_table,
                                                                     r_table.type,
                                                                     r_table.max_records_for_select,
                                                                 	 r_table.flat_file,
                                                                 	 r_table.flat_file_dir);
                        ELSE
                            v_Error := FUN_ARCHIVE_AT_ONCE 	(r_table.table_name,
                                                             r_table.where_clause,
                                                             r_table.archive_table,
                                                             r_table.type,
                                                             r_table.max_records_for_select,
                                                             r_table.flat_file,
                                                             r_table.flat_file_dir);
                                                             
                        END IF;
                    END IF;

                    -- calculate new v_Records
                    -- Value might get negative, will be checked for > 0 in while
                    v_Records := v_Records - r_table.max_records_for_select;

                    IF v_Error = -1 THEN	-- All OK
                        IF upper (ltrim (rtrim (r_table.type))) = 'ARCHIVE' THEN
                            BEGIN
                            	IF UPPER(r_table.flat_file) = 'J' OR
                                   UPPER(r_table.flat_file) = 'Y' THEN
                                    UPDATE CEDA_arch_log
                                    SET DATE_TIME_END = sysdate, 
                                        RECORDS = v_archive_records, 
                                        ARCHIVE_TABLE = r_table.table_name || '_' || 
                                        TO_CHAR(TRUNC(DATE_TIME_START),'YYYYMMDD') || 
                                        '.sql'
                                    WHERE SEQ_ID = v_seq_id;
                                ELSE   
                                    UPDATE CEDA_arch_log
                                    SET DATE_TIME_END = sysdate, RECORDS = v_archive_records
                                    WHERE SEQ_ID = v_seq_id;
                                END IF;
                                COMMIT;
                            EXCEPTION
                                WHEN OTHERS THEN
                                    ROLLBACK;
                                    pck_ceda_errors.prc_write_error('PRC_START_HOUSEKEEPING ', 'ARCHIVE-80000',
                                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                               SUBSTR(SQLERRM, 1, 150) || ' Error in Update CEDA_arch_log', 1);
                            END;
                        ELSIF upper (ltrim (rtrim (r_table.type))) = 'DELETE' THEN
                            BEGIN
                                UPDATE CEDA_arch_log
                                SET DATE_TIME_END = sysdate, RECORDS = v_deleted_records
                                WHERE SEQ_ID = v_seq_id;
                                COMMIT;
                            EXCEPTION
                                WHEN OTHERS THEN
                                    ROLLBACK;
                                    pck_ceda_errors.prc_write_error('PRC_START_HOUSEKEEPING ', 'ARCHIVE-80000',
                                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                               SUBSTR(SQLERRM, 1, 150) || ' Error in Update CEDA_arch_log', 1);
                            END;
                        END IF;
                    ELSE
                        -- end while loop
                        v_Records := 0;
                        pck_ceda_errors.prc_write_error('PRC_START_HOUSEKEEPING ', 'ARCHIVE-80000',
                                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                   SUBSTR(SQLERRM, 1, 150) || ' Error in Archiving', 1);

                    END IF;

                END LOOP;  -- v_Records

            END IF;

		END LOOP;
	EXCEPTION
		WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('PRC_START_HOUSEKEEPING ', 'ARCHIVE-80000',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
							   SUBSTR(SQLERRM, 1, 150) || ' Error in HOUSEKEEPING when others', 1);

	END PRC_START_HOUSEKEEPING;

-------------------------------------------------------------------------------------------
--      Function              - FUN_ARCHIVE_AT_ONCE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_ARCHIVE_AT_ONCE  (p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
								   p_where_clause IN CEDA_Housekeeping.Table_Name%TYPE,
								   p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
								   p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
								   p_MaxRecords IN CEDA_Housekeeping.Max_Records_For_Select%TYPE,
                                   p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                   p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_ARCHIVE_AT_ONCE
    --
    --      Version               - 1.0.1
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   15.11.2007         1.0.0      new
    --      Hammerschmid   28.12.2007         1.0.1      Insert Error locking
    --      Hammerschmid   05.08.2009         1.1        extend archiving with flat files
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_archive_cursor                INTEGER;
	v_delete_cursor                 INTEGER;
	v_sql_statement                 VARCHAR2(512);
	v_Error							INTEGER := -1;
	v_MaxRecords					NUMBER(9);
    
    v_ReturnValue					NUMBER;

	BEGIN
		v_MaxRecords := p_MaxRecords + 1;

		IF upper (trim (p_Archivetype)) = 'ARCHIVE' THEN

			-- Decide whether archive in table or flatfile
            IF upper(p_ArchiveFlat_file) = 'J' OR
               upper(p_ArchiveFlat_file) = 'Y' THEN
               -- Archive in flatfile
               v_ReturnValue := FUN_SQLINFLATFILE(p_Table,
               									  'SELECT * ' ||
				                                  'FROM ' || P_Table || ' ' ||
                                    			  'WHERE ' || p_where_clause || ' ' ||
                                                  'AND ROWNUM < ' || v_MaxRecords, 
                                                  ',',
                                                  p_ArchiveFlat_file_dir,
                                                  p_Table || '_' ||
                                                  TO_CHAR(TRUNC(sysdate),'YYYYMMDD') || 
                                                  '.sql');
                                                  
                IF v_ReturnValue > 0 THEN                                  
                    v_archive_cursor := dbms_sql.open_cursor;
                    v_sql_statement := 'DELETE FROM ' || p_Table
                                        || ' WHERE ' || p_where_clause
                                        || ' AND ROWNUM < ' || v_MaxRecords;
                    dbms_sql.parse (v_archive_cursor, v_sql_statement, dbms_sql.v7);
                    v_archive_records := dbms_sql.execute (v_archive_cursor);
                    dbms_sql.close_cursor (v_archive_cursor);
                END IF;
			ELSE
                v_Archive_cursor := dbms_sql.open_cursor;
                v_sql_statement := 'INSERT INTO ' || p_ArchiveTable
                                    || ' SELECT * '
                                    || ' FROM ' || P_Table
                                    || ' WHERE ' || p_where_clause
                                    || ' AND ROWNUM < ' || v_MaxRecords;

                dbms_sql.parse (v_archive_cursor, v_sql_statement, dbms_sql.v7);
                v_archive_records := dbms_sql.execute (v_archive_cursor);
                dbms_sql.close_cursor (v_archive_cursor);
                
                v_archive_cursor := dbms_sql.open_cursor;
                v_sql_statement := 'DELETE FROM ' || p_Table
                                    || ' WHERE ' || p_where_clause
                                    || ' AND ROWNUM < ' || v_MaxRecords;
                dbms_sql.parse (v_archive_cursor, v_sql_statement, dbms_sql.v7);
                v_archive_records := dbms_sql.execute (v_archive_cursor);
                dbms_sql.close_cursor (v_archive_cursor);
			END IF;

		ELSIF upper (trim (p_Archivetype)) = 'DELETE' THEN

			v_delete_cursor := dbms_sql.open_cursor;
			v_sql_statement := 'DELETE FROM ' || p_Table
								|| ' WHERE ' || p_where_clause
								|| ' AND ROWNUM < ' || v_MaxRecords;
			dbms_sql.parse (v_delete_cursor, v_sql_statement, dbms_sql.v7);
			v_deleted_records := dbms_sql.execute (v_delete_cursor);
			dbms_sql.close_cursor (v_delete_cursor);

		END IF;
		COMMIT;

		RETURN v_Error;

	EXCEPTION
		WHEN others THEN  -- cursor error
			ROLLBACK;
			IF dbms_sql.is_open (v_archive_cursor) THEN
				dbms_sql.close_cursor (v_archive_cursor);
			END IF;
			IF dbms_sql.is_open (v_delete_cursor) THEN
				dbms_sql.close_cursor (v_delete_cursor);
			END IF;
			v_Error := 0;
			pck_ceda_errors.prc_write_error('FUN_ARCHIVE_AT_ONCE ', 'ARCHIVE-80000',
                       'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
					   SUBSTR(SQLERRM, 1, 150) || ' Error in Archive / Delete when others', 1);
			RETURN v_Error;

	END FUN_ARCHIVE_AT_ONCE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_ARCHIVE_RECORD_BY_RECORD
-------------------------------------------------------------------------------------------
	FUNCTION FUN_ARCHIVE_RECORD_BY_RECORD  (p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
											p_where_clause IN CEDA_Housekeeping.Table_Name%TYPE,
											p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
											p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
											p_MaxRecords IN CEDA_Housekeeping.Max_Records_For_Select%TYPE,
                                            p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                            p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_ARCHIVE_RECORD_BY_RECORD
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   15.11.2007         1.0.0      new
    --	    Hammerschmid   28.12.2007         1.0.1      insert error locking
    --      Hammerschmid   05.08.2009         1.1        extend archiving with flat files
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE ArchiveType        IS REF CURSOR;
    v_Archive_cur           ArchiveType;
	v_RowId					ROWID;
	v_SQLString				VARCHAR2(512);
	v_Error					INTEGER := -1;
	v_MaxRecords			NUMBER(9);

	v_ReturnValue 			NUMBER;
    
	BEGIN

		v_MaxRecords := p_MaxRecords + 1;
		v_SQLString := ' SELECT ROWID '
						|| ' FROM ' || p_Table
						|| ' WHERE ' || p_where_clause
						|| ' AND ROWNUM < ' || v_MaxRecords;
		v_Error := -1;
		v_Archive_Records := 0;
		v_Deleted_Records := 0;

		OPEN v_Archive_cur FOR v_SQLString;
		LOOP
			-- Process data
			FETCH v_Archive_cur INTO v_RowId;
			EXIT WHEN v_Archive_cur%NOTFOUND OR
					  v_Archive_cur%NOTFOUND IS NULL;

			-- Decide whether archive in table or flatfile
			IF upper (trim (p_ArchiveType)) = 'ARCHIVE' THEN
                IF upper(p_ArchiveFlat_file) = 'J' OR
                   upper(p_ArchiveFlat_file) = 'Y' THEN
                   -- Archive in flatfile
                   v_ReturnValue := FUN_SQLINFLATFILE(p_Table,
                                                      'SELECT * ' ||
                                                      'FROM ' || P_Table || ' ' ||
                                                      'WHERE ' || p_where_clause || ' ' ||
                                                      'AND ROWID = ' || v_RowId, 
                                                      ',',
                                                      p_ArchiveFlat_file_dir,
                                                  	  p_Table || '_' ||
                                                  	  TO_CHAR(TRUNC(sysdate),'YYYYMMDD') || 
                                                  	  '.sql');
                                                      
                    IF v_ReturnValue > 0 THEN                                  
                        -- be careful in deleting only with rowid as statement
                        EXECUTE IMMEDIATE
                        'DELETE FROM ' || p_Table
                                    || ' WHERE ' || p_where_clause
                                    || ' AND ROWID = :rid'
                        USING v_RowId;

                        COMMIT;
                        v_Archive_Records := v_Archive_Records + 1;
                    END IF;
            	ELSE
                    BEGIN
                        EXECUTE IMMEDIATE
                        'INSERT INTO ' || p_Archivetable
                                    || ' SELECT * '
                                    || ' FROM ' || p_Table
                                    || ' WHERE ' || p_where_clause
                                    || ' AND ROWID = :rid'
                        USING v_RowId;

                        -- be careful in deleting only with rowid as statement
                        EXECUTE IMMEDIATE
                        'DELETE FROM ' || p_Table
                                    || ' WHERE ' || p_where_clause
                                    || ' AND ROWID = :rid'
                        USING v_RowId;

                        COMMIT;
                        v_Archive_Records := v_Archive_Records + 1;
                    EXCEPTION
                        WHEN OTHERS THEN
                            ROLLBACK;
                            v_Error := 0;
                            pck_ceda_errors.prc_write_error('FUN_ARCHIVE_RECORD_BY_RECORD ', 'ARCHIVE-80000',
                                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                   SUBSTR(SQLERRM, 1, 150) || ' Error in Archive when others', 1);
                    END;
				END IF;
			ELSIF upper (trim (p_ArchiveType)) = 'DELETE' THEN
				BEGIN
					EXECUTE IMMEDIATE
					'DELETE FROM ' || p_Table
								|| ' WHERE ' || p_where_clause
						 		|| ' AND ROWID = :rid'
					USING v_RowId;

					COMMIT;
					v_Deleted_Records := v_Deleted_Records + 1;
				EXCEPTION
					WHEN OTHERS THEN
						ROLLBACK;
						v_Error := 0;
						pck_ceda_errors.prc_write_error('FUN_ARCHIVE_RECORD_BY_RECORD ', 'ARCHIVE-80000',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
							   SUBSTR(SQLERRM, 1, 150) || ' Error in Delete when others', 1);
				END;
			END IF;

		END LOOP;

		CLOSE v_Archive_cur;

		RETURN v_Error;

	EXCEPTION
		WHEN OTHERS THEN
			IF v_Archive_cur%ISOPEN THEN
				CLOSE v_Archive_cur;
			END IF;
			pck_ceda_errors.prc_write_error('FUN_ARCHIVE_RECORD_BY_RECORD ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150) || ' Error in Cursor when others ' || v_SQLString, 1);
            v_Error := 0;
			RETURN v_Error;

	END FUN_ARCHIVE_RECORD_BY_RECORD;

-------------------------------------------------------------------------------------------
--      Function              - FUN_ARCHIVE_WITH_TEMP_TABLE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_ARCHIVE_WITH_TEMP_TABLE	(p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
						 		 			 p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
						 		 			 p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
                                             p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                             p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
    RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function             - FUN_ARCHIVE_WITH_TEMP_TABLE
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   19.08.2008         1.0.0      new
    --      Hammerschmid   05.08.2009         1.1        extend archiving with flat files
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_Records
	IS
		SELECT URNO, STATUS
		FROM   CEDA_URNO_TEMP
		WHERE  TRIM(STATUS) IS NULL;

	r_Records		c_Records%ROWTYPE;

	v_Error			INTEGER := -1;
    v_InClause		VARCHAR2(1000);
    v_ActualRecord	INTEGER := 0;

	BEGIN
		FOR r_Records IN c_Records LOOP
        	BEGIN
            	EXECUTE IMMEDIATE
                'UPDATE CEDA_URNO_TEMP SET STATUS = ' || '''W''' ||
                ' WHERE URNO = ' || r_Records.Urno;
				COMMIT;

                v_ActualRecord := v_ActualRecord + 1;

                IF v_ActualRecord = 1 THEN
                	v_Inclause := '(';
				END IF;

                IF v_ActualRecord < v_InClause_records THEN
                	v_Inclause := v_Inclause || r_Records.Urno || ',';
                ELSE
                	-- Process Data
                	v_InClause := SUBSTR(v_InClause, 1, LENGTH(v_InClause)-1 ) || ')';
                	v_ActualRecord := 0;

                    -- Archive / delete data
                    v_Error := FUN_ARCHIVE_WITH_INCLAUSE(p_Table,
                    									 p_ArchiveTable,
                                                         p_ArchiveType,
                                                         v_InClause,
                                                         p_ArchiveFlat_file,
                                                         p_ArchiveFlat_file_dir);
					-- Errorhandling ?
                	v_ActualRecord := 0;
                    v_Inclause := '';

                END IF;
        	EXCEPTION
        		WHEN OTHERS THEN
            		ROLLBACK;
					pck_ceda_errors.prc_write_error('FUN_ARCHIVE_WITH_TEMP_TABLE ', 'ARCHIVE-80000',
        		           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: Problem in Archiving' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
                	v_ActualRecord := 0;
                    v_InClause := '';
                    v_Error := 0;
        	END;

        END LOOP;

        -- Archive / delete data (the rest)
        IF v_ActualRecord > 0 THEN
            v_Error := FUN_ARCHIVE_WITH_INCLAUSE(p_Table,
                                                 p_ArchiveTable,
                                                 p_ArchiveType,
                                                 v_InClause,
                                                 p_ArchiveFlat_file,
                                                 p_ArchiveFlat_file_dir);
			-- Errorhandling ?
            v_ActualRecord := 0;
            v_InClause := '';
        END IF;

        RETURN v_Error;

    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			pck_ceda_errors.prc_write_error('FUN_ARCHIVE_WITH_TEMP_TABLE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: Problem in cursor' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
            v_Error := 0;
			RETURN v_Error;
    END FUN_ARCHIVE_WITH_TEMP_TABLE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_ARCHIVE_WITH_INCLAUSE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_ARCHIVE_WITH_INCLAUSE	(p_Table IN CEDA_Housekeeping.Table_Name%TYPE,
						 		 		 p_ArchiveTable IN CEDA_Housekeeping.Archive_Table%TYPE,
						 		 		 p_ArchiveType IN CEDA_Housekeeping.Type%TYPE,
						 		 		 p_InClause IN VARCHAR2,
                                         p_ArchiveFlat_file IN CEDA_HOUSEKEEPING.FLAT_FILE%TYPE,
                                         p_ArchiveFlat_file_dir IN CEDA_HOUSEKEEPING.FLAT_FILE_DIR%TYPE)
    RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function             - FUN_ARCHIVE_WITH_INCLAUSE
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   20.08.2008         1.0.0      new
    --      Hammerschmid   05.08.2009         1.1        extend archiving with flat files
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------


	v_Error			INTEGER := -1;
    v_ReturnValue   NUMBER := 0;

	BEGIN

		IF upper (trim (p_ArchiveType)) = 'ARCHIVE' THEN
        	IF upper(p_ArchiveFlat_file) = 'Y' OR
               upper(p_ArchiveFlat_file) = 'J' THEN
            	-- Archive in flatfile
                v_ReturnValue := FUN_SQLINFLATFILE(p_Table,
                                                   'SELECT * ' ||
                                                   'FROM ' || P_Table || ' ' ||
                                                   'WHERE URNO IN ' || p_Inclause,
                                                   ',',
                                                   p_ArchiveFlat_file_dir,
                                              	   p_Table || '_' ||
                                                   TO_CHAR(TRUNC(sysdate),'YYYYMMDD') || 
                                                   '.sql');
                                                      
                IF v_ReturnValue > 0 THEN                                  
                    EXECUTE IMMEDIATE
                    ' DELETE FROM ' || p_Table ||
                    ' WHERE URNO IN ' || p_Inclause;

                    EXECUTE IMMEDIATE
                    'UPDATE CEDA_URNO_TEMP SET STATUS = ' || '''A''' ||
                    ' WHERE URNO IN ' || p_Inclause;
            	END IF;
            
            ELSE
                EXECUTE IMMEDIATE
                'INSERT INTO ' || p_ArchiveTable ||
                ' SELECT * FROM ' || p_Table ||
                ' WHERE URNO IN ' || p_Inclause;
                EXECUTE IMMEDIATE
                ' DELETE FROM ' || p_Table ||
                ' WHERE URNO IN ' || p_Inclause;

                EXECUTE IMMEDIATE
                'UPDATE CEDA_URNO_TEMP SET STATUS = ' || '''A''' ||
                ' WHERE URNO IN ' || p_Inclause;
			END IF;
            
		ELSIF upper (trim (p_ArchiveType)) = 'DELETE' THEN
            EXECUTE IMMEDIATE
            ' DELETE FROM ' || p_Table ||
            ' WHERE URNO IN ' || p_Inclause;

            EXECUTE IMMEDIATE
            'UPDATE CEDA_URNO_TEMP SET STATUS = ' || '''D''' ||
            ' WHERE URNO IN ' || p_Inclause;
        END IF;

        COMMIT;

        RETURN v_Error;

    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			pck_ceda_errors.prc_write_error('FUN_ARCHIVE_WITH_INCLAUSE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: Problem in cursor' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
            v_Error := 0;
			RETURN v_Error;
    END FUN_ARCHIVE_WITH_INCLAUSE;

-------------------------------------------------------------------------------------------
--      FUNCTION              	   - FUN_SQLINFLATFILE
-------------------------------------------------------------------------------------------
	FUNCTION  FUN_SQLINFLATFILE	( p_table     IN VARCHAR2,
    						  	  p_query     IN VARCHAR2,
                          	  	  p_separator IN VARCHAR2 DEFAULT ',',
                          	  	  p_dir       IN VARCHAR2,
                          	  	  p_filename  IN VARCHAR2 )
	RETURN NUMBER
	IS
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Function	        -	FUN_SQLINFLATFILE
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
    --  Return              - Records written
    --
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   04.08.2009         1.0.0      new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

    v_output        	UTL_FILE.FILE_TYPE;
    v_theCursor     	INTEGER DEFAULT DBMS_SQL.OPEN_CURSOR;
    v_status        	INTEGER;
    v_colCnt        	NUMBER := 0;
    v_separator     	VARCHAR2(10) := '';
    v_cnt           	NUMBER := 0;
    v_descTbl       	DBMS_SQL.DESC_TAB;
    v_HeaderWritten		BOOLEAN := FALSE;

	v_InsertColumns		VARCHAR2(4000);

   	Type t_ntype IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
   	v_col_n 			t_ntype;
   	Type t_ctype IS TABLE OF CHAR(4000) INDEX BY BINARY_INTEGER;
   	v_col_c 			t_ctype;
   	Type t_vtype IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
   	v_col_v 			t_vtype;
   	Type t_dtype IS TABLE OF DATE INDEX BY BINARY_INTEGER;
   	v_col_d 			t_dtype;

	BEGIN
    	-- Do not forget the filedirectory in parameter utl_file_dir = [filedirectory]
        -- or more than one entry in INITUFIS.ORA
        -- exp. utl_file_dir=/tmp or utl_file_dir=/home/ceda/tmp

        v_output := UTL_FILE.fopen( p_dir, p_filename, 'a', 20000 );

        DBMS_SQL.PARSE(  v_theCursor,  p_query, dbms_sql.native );
        -- get column information
        DBMS_SQL.describe_columns(c			=> v_theCursor,
               					  col_cnt 	=> v_colCnt,
                                  desc_t	=> v_descTbl);

        FOR i in 1..v_colCnt LOOP
        	BEGIN
                v_col_n(i) := 0;
                v_col_c(i) := '';
                v_col_v(i) := NULL;
                v_col_d(i) := NULL;
                CASE v_descTbl(i).col_type
                    WHEN DBMS_TYPES.TYPECODE_CHAR THEN
                    	DBMS_SQL.DEFINE_COLUMN(v_theCursor, i, v_col_c(i), 2000);
                    WHEN DBMS_TYPES.TYPECODE_VARCHAR THEN
                    	DBMS_SQL.DEFINE_COLUMN(v_theCursor, i, v_col_v(i), 2000);
                    WHEN DBMS_TYPES.TYPECODE_NUMBER THEN
                    	DBMS_SQL.DEFINE_COLUMN(v_theCursor, i, v_col_n(i));
                    WHEN DBMS_TYPES.TYPECODE_DATE THEN
                    	DBMS_SQL.DEFINE_COLUMN(v_theCursor, i, v_col_d(i));
                END CASE;
            EXCEPTION
                WHEN OTHERS THEN
                    IF ( SQLCODE = -1007 ) THEN
                        EXIT;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;

        v_status := DBMS_SQL.execute(v_theCursor);

        LOOP
            EXIT WHEN ( DBMS_SQL.fetch_rows(v_theCursor) <= 0 );
            v_separator := '';

            IF v_HeaderWritten = FALSE THEN
                -- writer header information
                v_InsertColumns := '';
                FOR i IN 1 .. v_colCnt LOOP
                	IF i = 1 THEN
                		v_InsertColumns := 'INSERT INTO ' || p_table || ' (' || v_descTbl(i).col_name;
                    ELSE
                    	IF MOD(i,10) = 0 THEN
                        	-- insert LF
                        	v_InsertColumns := v_InsertColumns || CHR(10) || CHR(9);
                        END IF;
                		v_InsertColumns := v_InsertColumns || ',' || v_descTbl(i).col_name;
                    END IF;
                END LOOP;
                v_InsertColumns := v_InsertColumns || ')';
                v_HeaderWritten := TRUE;
            END IF;

			UTL_FILE.put( v_output, v_InsertColumns );
            UTL_FILE.new_line( v_output );
            UTL_FILE.put( v_output, 'VALUES (' );

            v_separator := '';
            FOR i IN 1 .. v_colCnt LOOP
              	IF MOD(i,10) = 0 THEN
                   	-- insert LF and TAB
            		UTL_FILE.new_line( v_output );
                    UTL_FILE.put( v_output, CHR(9));
                END IF;
                -- NULL values are not allowed in CEDA !!!!!
				CASE v_descTbl(i).col_type
            		WHEN DBMS_TYPES.TYPECODE_CHAR THEN
                		DBMS_SQL.column_value( v_theCursor, i, v_col_c(i) );
                        IF TRIM(v_col_c(i)) IS NULL THEN 
                			UTL_FILE.put( v_output, v_separator || ''' ''');
                        ELSE
                			UTL_FILE.put( v_output, v_separator || '''' || TRIM(v_col_c(i)) || '''' );
                        END IF;
                    WHEN DBMS_TYPES.TYPECODE_VARCHAR THEN
                		DBMS_SQL.column_value( v_theCursor, i, v_col_v(i) );
                        IF TRIM(v_col_v(i)) IS NULL THEN 
                			UTL_FILE.put( v_output, v_separator || ''' ''');
                        ELSE
                			UTL_FILE.put( v_output, v_separator || '''' || TRIM(v_col_v(i)) || '''' );
                        END IF;
                    WHEN DBMS_TYPES.TYPECODE_VARCHAR2 THEN
                		DBMS_SQL.column_value( v_theCursor, i, v_col_v(i) );
                        IF TRIM(v_col_v(i)) IS NULL THEN 
                			UTL_FILE.put( v_output, v_separator || ''' ''');
                        ELSE
                			UTL_FILE.put( v_output, v_separator || '''' || TRIM(v_col_v(i)) || '''' );
                        END IF;
                    WHEN DBMS_TYPES.TYPECODE_NUMBER THEN
                		DBMS_SQL.column_value( v_theCursor, i, v_col_n(i) );
                		UTL_FILE.put( v_output, v_separator || (v_col_n(i)) );
                    WHEN DBMS_TYPES.TYPECODE_DATE THEN
                		DBMS_SQL.column_value( v_theCursor, i, v_col_d(i) );
                		UTL_FILE.put( v_output, v_separator || 'TO_DATE(''' ||
                        	TRIM(TO_CHAR(v_col_d(i),'DD.MM.YYYY HH24:MI:SS')) ||
                            ''',' || '''DD.MM.YYYY HH24:MI:SS' || '''' || ')' );
                END CASE;
                v_separator := ',';
            END LOOP;
            UTL_FILE.put( v_Output, ');');
            UTL_FILE.new_line( v_output );
            UTL_FILE.new_line( v_output );
            v_cnt := v_cnt+1;

        END LOOP;
        DBMS_SQL.close_cursor(v_theCursor);
        UTL_FILE.fclose( v_output );
        RETURN v_cnt;

    EXCEPTION
    	WHEN UTL_FILE.invalid_path THEN
--        	dbms_output.put_line('invalid_path');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.invalid_path ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
        WHEN UTL_FILE.invalid_mode THEN
--        	dbms_output.put_line('invalid_mode');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.invalid_mode ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
        WHEN UTL_FILE.invalid_filehandle THEN
--        	dbms_output.put_line('invalid_filehandle');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.invalid_filehandle ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
        WHEN UTL_FILE.invalid_operation THEN
--        	dbms_output.put_line('invalid_operation');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.invalid_operation ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
        WHEN UTL_FILE.read_error THEN
--        	dbms_output.put_line('read_error');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.read_error ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
        WHEN UTL_FILE.write_error THEN
--        	dbms_output.put_line('write_error');
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: UTL_FILE.write_error ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
    	WHEN OTHERS THEN
--        	dbms_output.put_line(SQLERRM);
        	DBMS_SQL.CLOSE_CURSOR(v_theCursor);
        	UTL_FILE.FCLOSE( v_output );
			pck_ceda_errors.prc_write_error('FUN_SQLINFLATFILE ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: When Others ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
	        RETURN v_cnt;
    END FUN_SQLINFLATFILE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_CREATE_URNO_TEMP
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CREATE_URNO_TEMP 	(p_FromTable 	IN VARCHAR2,
    								 p_WhereClause 	IN VARCHAR2,
                                     i_Records		OUT NUMBER)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CREATE_URNO_TEMP
    --
    --      Version               - 1.0.0
    --
    --      Return                - 1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   19.08.2008         1.0.0      new
    --		Hammerschmid   20.09.2008		  1.1        New function FUN_TRUNCATE_TABLE
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Error				INTEGER := -1;

	BEGIN
    	i_Records := 0;
        -- clean up old urno_temp
        v_Error := FUN_TRUNCATE_TABLE('URNO_TEMP');

		IF v_Error = -1 THEN
            EXECUTE IMMEDIATE
            'INSERT INTO URNO_TEMP ' ||
            'SELECT TO_NUMBER(URNO) FROM ' || p_FromTable || ' ' ||
            'WHERE ' || p_WhereClause;
            i_Records := SQL%ROWCOUNT;
            COMMIT;
        END IF;

        RETURN v_Error;

    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
			pck_ceda_errors.prc_write_error('FUN_CREATE_URNO_TEMP ', 'ARCHIVE-80000',
                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);
            v_Error := 0;
            i_Records := 0;
			RETURN v_Error;

	END FUN_CREATE_URNO_TEMP;

-------------------------------------------------------------------------------------------
--      Function              - FUN_JOBSRUNNING
-------------------------------------------------------------------------------------------
	FUNCTION FUN_JOBSRUNNING
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_JOBSRUNNING
    --
    --      Version               - 1.0.0
    --
    --      Return                - 1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   10.01.2009         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_RunningJobs
    IS
    	SELECT *
        FROM TR_LOG
        WHERE ERROR_FLAG = 'R'
        AND TRIM(PROCESS) NOT IN ('HOUSEKEEPING_JOB')
        AND P_END IS NULL;

	r_RunningJobs			c_RunningJobs%ROWTYPE;
    v_RunningJobs			NUMBER(22) := -1;
    v_End					BOOLEAN := FALSE;

	BEGIN
		FOR r_RunningJobs IN c_RunningJobs
		LOOP
        	BEGIN
            	-- Is this running job a job which
                -- should run till end without interrupts?
            	IF TRIM(r_RunningJobs.Process) = 'PRC_SCANURNOJOB' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
                ELSIF TRIM(r_RunningJobs.Process) = 'PRC_SCANURNOJOBIMMEDIATE' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
                ELSIF TRIM(r_RunningJobs.Process) = 'PRC_SCANURNOINITIALJOB' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
                ELSIF TRIM(r_RunningJobs.Process) = 'PRC_RUNAFTEROTHERJOB' THEN
                	v_RunningJobs := r_RunningJobs.Seq_Id;
                    v_End := TRUE;
                END IF;

                IF v_End = TRUE THEN
					pck_ceda_errors.prc_write_error('FUN_JOBSRUNNING TR_LOG SEQ_ID = ' ||
                    								TO_CHAR(r_RunningJobs.Seq_Id), 'ARCHIVE-80000',
                   									'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   									SUBSTR(SQLERRM, 1, 150), 1);
                    EXIT;
                END IF;
            EXCEPTION
            	WHEN OTHERS THEN
            		v_RunningJobs := 0;
            END;
    	END LOOP;

		RETURN v_RunningJobs;

    EXCEPTION
    	WHEN OTHERS THEN
       		v_RunningJobs := 0;
			RETURN v_RunningJobs;

    END FUN_JOBSRUNNING;

-------------------------------------------------------------------------------------------
--      Function              - FUN_PARSE_CEDA_HOUSEKEEPING
-------------------------------------------------------------------------------------------
	FUNCTION FUN_PARSE_CEDA_HOUSEKEEPING
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_ARCHIVE_RECORD_BY_RECORD
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   15.11.2007         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	CURSOR c_Ceda
	IS
		SELECT *
		FROM CEDA_HOUSEKEEPING;

	r_Ceda				c_Ceda%ROWTYPE;

	v_SQLString			VARCHAR2(512);
	v_Error				INTEGER := -1;

	BEGIN
		FOR r_Ceda IN c_Ceda
		LOOP
			BEGIN
				-- Check if source table exists
				v_SQLString := 'SELECT * FROM ' || r_Ceda.table_name || ' WHERE ROWNUM = 1';
				EXECUTE IMMEDIATE v_SQLString;

				BEGIN
					-- Check if where clause is OK
					v_SQLString := 'SELECT * FROM ' || r_Ceda.table_name ||
									' WHERE ' || r_Ceda.where_clause ||
									' AND ROWNUM = 1';
					EXECUTE IMMEDIATE v_SQLString;
				EXCEPTION
					WHEN OTHERS THEN
						pck_ceda_errors.prc_write_error('FUN_CEDA_HOUSEKEEPING','ARCHIVE-80000',
														'WHERE CLAUSE in SQLString: ' || v_SQLString || ' not correct',1);
				END;

			EXCEPTION
				WHEN OTHERS THEN
					pck_ceda_errors.prc_write_error('FUN_CEDA_HOUSEKEEPING','ARCHIVE-80000',
													'SOURCE TABLE ' || r_Ceda.table_name || ' not found',1);
			END;

			BEGIN
				-- Check if Archive table exists
				v_SQLString := 'SELECT * FROM ' || r_Ceda.archive_table || ' WHERE ROWNUM = 1';
				EXECUTE IMMEDIATE v_SQLString;

			EXCEPTION
				WHEN OTHERS THEN
					pck_ceda_errors.prc_write_error('FUN_CEDA_HOUSEKEEPING','ARCHIVE-80000',
													'ARCHIVE TABLE ' || r_Ceda.archive_table || ' not found',1);
					v_SQLString := 'CREATE TABLE ' || r_Ceda.archive_table || ' AS ' ||
								   ' SELECT * FROM ' || r_Ceda.table_name ||
								   ' WHERE ROWNUM = 1';

					pck_ceda_errors.prc_write_error('FUN_CEDA_HOUSEKEEPING','ARCHIVE-80000',
													'USE: ' || v_SQLString, 1);
					EXECUTE IMMEDIATE v_SQLString;
			END;


		END LOOP;

		RETURN v_Error;

	END FUN_PARSE_CEDA_HOUSEKEEPING;


-------------------------------------------------------------------------------------------
--      Procedure              - PRC_HOUSEKEEPING_JOB
-------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_HOUSEKEEPING_JOB
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   08.03.2008         1.0.0      new
    --  Hammerschmid   10.01.2009		  1.1.0      Check if other jobs are running
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	PROCEDURE PRC_HOUSEKEEPING_JOB (p_HOUROFDAY IN VARCHAR2)
	AS

		v_NewHourOfDay		NUMBER(20);
		jobno				NUMBER;
    	v_Job_check			NUMBER := 0;

	BEGIN
		BEGIN
			-- check if job is already running
			SELECT job
    		INTO v_Job_check
			FROM SYS.user_Jobs
			WHERE what LIKE '%--HOUSEKEEPING_JOB%';
    	EXCEPTION
    		WHEN NO_DATA_FOUND THEN
        		v_Job_check := 0;
	    	WHEN OTHERS THEN
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING_JOB ', 'ARCHIVE-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 9);
		END;
        -- Check if hour of day is in the past
        IF to_Number(to_char(sysdate , 'HH24')) > to_Number(p_HourOfDay) THEN
			v_NewHourOfDay := 1;
        ELSE
			v_NewHourOfDay := 0;
        END IF;
		IF v_Job_check <> 0 THEN
			--remove old job and create new
			dbms_job.remove(v_Job_check);
	    END IF;
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        --HOUSEKEEPING_JOB
	    v_SEQ_ID	TR_LOG.SEQ_ID%TYPE;
        v_OtherJobsRunning 	INTEGER := -1;
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG (SEQ_ID, PROCESS, P_START, P_END, P_STATUS, ERROR_FLAG)
        VALUES (v_Seq_ID,''--HOUSEKEEPING_JOB'',sysdate,null,''JOB'',''R'');
        COMMIT;
        v_OtherJobsRunning := PCK_CEDA_HOUSEKEEPING.FUN_JOBSRUNNING;
        IF v_OtherJobsRunning = -1 THEN
	    PCK_CEDA_HOUSEKEEPING.PRC_START_HOUSEKEEPING;
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''N'' WHERE seq_id = v_Seq_ID;
        ELSE
	    UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = ''D'' WHERE seq_id = v_Seq_ID;
        END IF;
        COMMIT;
		END;',
	   	TRUNC(SYSDATE) + v_NewHourOfDay + (p_HOUROFDAY/24),
		'TRUNC(SYSDATE) + 1 + (' || p_HOUROFDAY || '/24)',
		FALSE);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
    		ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING_JOB ', 'ARCHIVE-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 9);

	END PRC_HOUSEKEEPING_JOB;

-------------------------------------------------------------------------------------------
--      Function              - FUN_GET_NEXT_SEQUENCE_ID
-------------------------------------------------------------------------------------------

	FUNCTION FUN_GET_NEXT_SEQUENCE_ID
	RETURN NUMBER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_GET_NEXT_SEQUENCE_ID
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - New Sequence iD
	--
	--		Error				  - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   19.12.2007         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Seq_ID			NUMBER(22);

	BEGIN
		-- get next Sequence ID for dispays
		SELECT HOUSEKEEPING_SEQ.Nextval
			INTO v_Seq_ID
		FROM dual;
		RETURN v_Seq_ID;
	END FUN_GET_NEXT_SEQUENCE_ID;

------------------------------------------------------------------------------------------
--      Function                 - FUN_TRUNCATE_TABLE (p_Tabble IN VARCHAR2)
-------------------------------------------------------------------------------------------
	FUNCTION FUN_TRUNCATE_TABLE (p_Table IN VARCHAR2)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_TRUNCATE_USED_URNOS
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   20.09.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;

	PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
    	EXECUTE IMMEDIATE
        'TRUNCATE TABLE ' || p_Table;
        COMMIT;
    	RETURN v_Success;
    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
    		v_Success := 0;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_TRUNCATE_TABLE ', 'ARCHIVE-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
			RETURN v_Success;
    END FUN_TRUNCATE_TABLE;

END PCK_CEDA_HOUSEKEEPING;
/
