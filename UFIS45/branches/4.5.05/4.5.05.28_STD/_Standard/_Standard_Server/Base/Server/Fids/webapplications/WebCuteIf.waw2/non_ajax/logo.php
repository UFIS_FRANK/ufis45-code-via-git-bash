<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
    include("include/manager.php");

    include("include/class.LoginInit.php");
        /* Login Check */
        /* Set Login Instance*/
    $login = new  LoginInit;
    $logintype="Login_"	.$AuthType;
    if (isset($Session["userid"])) {
        // The user has already been Logged

    } else {
        $Query=	$login->$logintype($username,$password);

    }

        /* Start Debug  if requested */
    if ($debug=="true") {
        $tmpdebug="?debug=true";
        $db->debug=$debug;
        $dbMysql->debug=$debug;
    }else {
        $debug="false";
    }
?>

<html>
<title><?php echo $PageTitle ?></title>
    <META NAME="description" CONTENT="">
    <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
    <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">
    <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
    <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">
    <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">
    <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1253">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
  <META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">
    <link rel="STYLESHEET" type="text/css" href="style/style.css">

</head>
<body bgcolor="White" text="Black" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 link="black" vlink="black" alink="black" style="font-family:Arial">
<?php



    $AirlineURNO=0;

    $PicName="pics/dot.gif";
    //Added Display LogoName
    //Different if Gate and Dedicated Checkin from Common CheckIN
    //WAW Project

    $tmplogo = "";

    if ($Request=="DisableLogo") {
        $Session["logoSelection"] = !$Session["logoSelection"];
    }

    if (strlen($LogoSelect)>1 )  {
        if ($LogoSelect=="None") {
            $Session["PicName"] = "None";
        } else {
            $PicName=$logoPath.$LogoSelect.$logoExt;
            //if ($Session["LogoNameALC"]==false) {$Session["PicName"] = $LogoSelect;}
            $Session["PicName"] = $LogoSelect;
        }
    } else {

        if ($FLZTABuse==true) {
            if (strlen($LogoSelect)==0 )  {
                $LogoSelect=Currentlogos($FLZTABuse);
            }
            $tok = strtok($LogoSelect,"|");
            $i=0;

            while ($tok) {

                if ($i==0) {
                    $AirlineURNO=$tok;
                    $Session["AirlineURNO"]=$tok;
                }

                if ($i==1) {
                    $PicName=$logoPath.$tok.".GIF";
                    $Session["PicName"]=$tok;
                }

                $i++;
                $tok = strtok("|");

            }
        } else {
            if ($Session["CommonF"]==0) {

                $LogoSelect=$Session["LogoName"];
            } else {
                $tmplogo = Currentlogos($FLZTABuse);
                if (strlen(trim($tmplogo))>1) {
                    $LogoSelect = $tmplogo;
                } else {
                    //echo $Session["LogoName"];
                    $LogoSelect=$Session["LogoName"];
                }
            }
            if (strlen($LogoSelect)>1 )  {
                $PicName=$logoPath.$LogoSelect.$logoExt;
                if ($Session["LogoNameALC"]==false) {$Session["PicName"] = $LogoSelect;}

            }
        }


    }


    if ($Button=='Transmit') {

        echo "<script language=\"JavaScript1.2\">parent.Cput.location=\"cput.php?Button=LogoTransmit&FlightURNO=".$Session["FlightURNO"]."\"</script>";
    }


    //echo $Button;

    //echo $Session["PicName"]."--1".$Session["LogoNameALC"];
    //echo $LogoSelect.'--'.$Session["logoSelection"].'----';
?>

<!--	<form action="logo.php" method="get" title="Logo">		-->

<TABLE  align=top border=0>
    <TR >
        <TD colspan="2"><p align = left>&nbsp;&nbsp;<strong>Logo:</strong></p>
            <img  src="<?php echo $PicName ?>" border=1 width="270" height="76">
        </TD>
    </TR>
    <?php if (($Session["logoSelection"] == true && $FLZTABuse==true) || ($Session["logoSelection"] == true && $Session["Type"]!="GATE" && $Session["logoSelectionUser"] != false)) {
    $logofunction="logos".$logoSelectionType;
    ?>
    <form action="logo.php" method="get" title="Logo">

        <input name="Request" type="hidden" value="Logo">
        <TR align=center>
            <TD colspan="2">
                <p align = left>&nbsp;&nbsp;<strong>Selection:</strong></p>
                <select name="LogoSelect" size="10">
                    <?php  $login->$logofunction();?>
                </select>
            </TD>
        </TR>

        <TR >
        <TD align=right>
            <input type="submit" name="Button" value="Preview">
        </TD>
        <!-- </form>
        <form action="logo.php" method="post" >-->
        <?php if ($Session["STATUS"]==1) {
        $tmp=" disabled";
        //if ($Session["STATUS"]==1) {
        echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"".$Session["FlightURNO"]."\" >";
        //} else {
        //	echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"\" >";
        //}
        }
        ?>
        <td align="left"><input type="submit" name="Button" value="Transmit" ></td>
    </form>
    </TR>
    <?php } ?>




    <!--
<TR >
<TD align=center>
<BR>
<a href="logo.php">Refresh the Logo</a>
<BR><BR><BR>
</TD>
    </TR>-->
</TABLE>

<!--	<input name="Request" type="hidden" value="Logo">
</form>-->
</body>
</html>
