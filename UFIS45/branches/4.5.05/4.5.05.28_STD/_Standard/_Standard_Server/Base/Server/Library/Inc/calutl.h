#ifndef _DEF_mks_version_calutl_h
  #define _DEF_mks_version_calutl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_calutl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/calutl.h 1.2 2004/07/27 16:46:50SGT jim Exp  $";
#endif /* _DEF_mks_version */
#ifndef __CALUTL_INC
#define __CALUTL_INC


/* Header File for Calendar Utilities */

struct _TblYrMo
{
  char cNamFul[10];/* Full Name of element        */
  char cNam4Lc[5]; /* 4 letter name of element    */
  char cNam3Lc[4]; /* 3 letter name of element    */
  char cNam2Lc[3]; /* 2 letter name of element    */
  int  iNbr4Lc;    /* full  number (4 digits)     */
  int  iNbr2Lc;    /* short number (2 digits)     */
  int  iCntDay;    /* Sum of Days before entry    */
  int  iCntMin;    /* Sum of Minutes before entry */
  int  iWekDay;    /* WeekDay of first Day        */
  int  iMaxDay;    /* Amount of Days in element   */
  int  iNoWeek;    /* Weeknumber of first Day     */
  int  iLpYear;    /* LeapYears = 1 else 0        */
};/* end of struct _TblYears */
typedef struct _TblYrMo YRMO;
#define YRMO_SIZE sizeof(YRMO)

extern YRMO *YrMo;
extern int  InitYearTable(char *, int);
extern int	ShowCalendar(void);

extern int  LeapYear(int);

extern int  CheckDate(char *, int *, int *, int *, int *, int *);
extern int  GetFullDay(char *, int *, int *, int *);
extern int  DateDiffToMin(char *, char *, int *);

extern int  DayDate(char *, int);
extern int  TimDate(char *, int);
extern int  FullDayDate(char *, int);
extern int  FullTimDate(char *, int);
extern int  FullTimeString(char *, int, char *);

extern int  DateOffsToDate(char *, char *, char *);
extern int	ChgDateToDay(char *, int *);
extern int	ChgDateToMin(char *);
extern int	ChgDayToDate(char *, int);
extern int	ChgMinToDate(char *, int);

extern int ChgOraDateToDay(char *pcpOraDate, char *pcpCedaDate, int *ipWkDay);
extern int ChgUaeTimeToMin(char *pcpUaeTime, char *pcpCedaTime);



#endif

