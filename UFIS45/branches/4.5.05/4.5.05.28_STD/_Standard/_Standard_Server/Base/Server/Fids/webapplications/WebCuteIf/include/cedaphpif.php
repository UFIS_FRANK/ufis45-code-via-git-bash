<?php
/*
Copyright ��� 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
// defining level for debug purpose
define("DEBUG",100);
define("TRACE",101);

function CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime='0')
{
	Global $RSHIP,$error,$CputPath,$wks,$cputuser,$TW_START,$TW_END;
	Global $Session,$ClientIP,$NoIp;
	
	$Func="CallCput";
	
	if ($currenttime=='0') {
		$Logdate = date("YmdHis",time()) ." U";
	} Else {
		$Logdate=$currenttime."C";
	}
	
	
	//Change the $TW_START var
	if ($NoIp!="true") {
		$TW_START="\'".$Session["username"]."_".$Logdate."\'";
	} else {
		$TW_START="\'".$ClientIP."_".$Logdate."\'";
	}
	
	//////////////////////////////////////////////////////////////////////
	// if the len is <=0 we must not have some "" around the those values.
	// this would mix up the cput-programm
	//////////////////////////////////////////////////////////////////////
	if (strlen($Fields) >= 0)
	{
		str_pad($Fields,strlen($Fields)+2,"\"",STR_PAD_BOTH);
	}
	if (strlen($Data) >= 0)
	{
		str_pad($Data,strlen($Data)+2,"\"",STR_PAD_BOTH);
	}

	str_pad($wks,strlen($wks)+2,"\"",STR_PAD_BOTH);
	str_pad($cputuser,strlen($cputuser)+2,"\"",STR_PAD_BOTH);
	str_pad($TW_START,strlen($TW_START)+2,"\"",STR_PAD_BOTH);
	str_pad($TW_END,strlen($TW_END)+2,"\"",STR_PAD_BOTH);
	   

	if (strlen($Selection) >= 0)
	{
		//str_pad($Selection,strlen($Selection)+2,"\"",STR_PAD_BOTH);
		//$Selection = stripslashes($Selection);
	}

	///////////////////////////////////////////////////////
	// now we sho some info about the event if DEBUG is set
	///////////////////////////////////////////////////////
	$strdebug="";
	if ($Dbg_level==DEBUG)
	{
		// print $Func."--DEST:".$Dest." "."PRIO:".$Prio." "."COMMAND:".$Command." "."TABLE:".$Table." "."FIELDS:".$Fields." "."DATA:".$Data." "."SELECT.:".$Selection." "."TIMEOUT:".$Timeout;

		$strdebug=$strdebug. "<br>-----------------------------------------";
		$strdebug=$strdebug."<br>".$Func."-Dest   :".$Dest;
		$strdebug=$strdebug."<br>".$Func."-Prio   :".$Prio;
		$strdebug=$strdebug."<br>".$Func."-Command:".$Command;
		$strdebug=$strdebug."<br>".$Func."-Table  :".$Table;
		$strdebug=$strdebug."<br>".$Func."-Fields :".$Fields;
		$strdebug=$strdebug."<br>".$Func."-Data   :".$Data;
		$strdebug=$strdebug."<br>".$Func."-Select.:".$Selection;
		$strdebug=$strdebug."<br>".$Func."-Timeout:".$Timeout;
		$strdebug=$strdebug."<br>-----------------------------------------<br>";
		$error->debug($strdebug, 'strdebug', __FILE__, __LINE__);
	}
//	echo $strdebug;
	////////////////////////////////////////////// 
	// now we call cput with the passed parameters 
	////////////////////////////////////////////// 
	$command=$CputPath." 2 $Dest $Prio $Command $Table $Fields $Data $Selection $Timeout $wks $cputuser $TW_START $TW_END ";
	//$command=$CputPath." 2 $Dest $Prio $Command $Table $Fields $Data $Selection $Timeout ";
	if (strlen($RSHIP)>0) {
		$first_result= cmd($RSHIP,$command );
	} else {
		$command=str_replace ("\'", "'",$command);
		$first_result = `$command`;	
	}
	
	$error->debug($command, 'command', __FILE__, __LINE__); 
	$error->debug($first_result, 'first_result', __FILE__, __LINE__);
	
	 
	#$final_result = str_replace ("\n", "<br />",$first_result);
	$second_result = str_replace (" ", "&nbsp ",$first_result);
	#$final_result = str_replace ("\n", "<br />",$second_result);
	/*if ($Prio < -99)
	{
	 $final_result = str_replace ("\n", "<br /><font face=\"Courier New\">",$second_result);
	}
	else
	{
	 $final_result = str_replace ("\n", "<br />",$second_result);
	}*/
	return $first_result;
}
?>
