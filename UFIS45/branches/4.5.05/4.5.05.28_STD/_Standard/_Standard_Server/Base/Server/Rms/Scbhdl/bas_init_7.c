/**************************************************************************************************/
/* KorrigiereMonat: Start- und Endmonat ggbf. korrigieren (auf Quartalsanfang, bzw. -ende setzen) */
/**************************************************************************************************/
BOOL KorrigiereMonat(int ipMonat,BOOL bpAnfangOderEnde)
{
	int ilMonatVal;
	int ilQuartal;

	/* calculate quarter */
	ilMonatVal = ipMonat - 1;
	ilQuartal = ilMonatVal / 3;
	ilQuartal += 1;

	if (!bpAnfangOderEnde)
	{
		/* calculate start of quarter */
		ilQuartal = ilQuartal - 1;
		ilMonatVal = 3 * ilQuartal;
		ilMonatVal = ilMonatVal + 1;
	}
	else
	{
		/* calculate end of quarter */
		ilMonatVal = 3 * ilQuartal;
	}

	return ilMonatVal;
}

/***********************************************************************************/
/* init_7: Initialize account 7 - sum values of account 4 of corresponding quarter */
/***********************************************************************************/
static void bas_init_7()
{
	char	clFrom[16];
	char	clYearFrom[6];
	char	clMonFrom[4];
	char	clDayFrom[4];

	char	clTo[16];
	char	clYearTo[6];
	char	clMonTo[4];
	char	clDayTo[4];

	char	clCurrentEmpl[12];
	char	clQuartalCODates[4][16];

	STAFFDATA	olStfData;

	double	dlFreitageQuartalPlan = 0;
	char	clFreitageQuartalPlan[128];
	char	clCalcDate[16];

	int	ilCountEmpl;
	int	ilCounter;
	int	ilYearCount;
	int	ilMonthCount;
	int	ilQuartalCount;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_7 START =============================");

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account Init 7: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account Init 7: <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account Init 7: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 7: <pcgFields> = <%s>",pcgFields);

	/* checking the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"Account Init 7: Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 7: <pcgSelKey> = <%s>",pcgSelKey);

	/* get start date */
	if (GetFieldValueFromMonoBlockedDataString("FROM",pcgFields,pcgData,clFrom) <= 0)
	{
		dbg(TRACE,"Account Init 7: Stopped, because <clFrom> = undef!");
		return;
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strncpy(clMonFrom,&clFrom[4],2);
	clMonFrom[2] = '\0';
	strncpy(clDayFrom,&clFrom[6],2);
	clDayFrom[2] = '\0';

	/* get end date */
	if (GetFieldValueFromMonoBlockedDataString("TO",pcgFields,pcgData,clTo) <= 0)
	{
		dbg(TRACE,"Account Init 7: Stopped, because <clTo> = undef!");
		return;
	}
	strncpy(clYearTo,&clTo[0],4);
	clYearTo[4] = '\0';
	strncpy(clMonTo,&clTo[4],2);
	clMonTo[2] = '\0';
	strncpy(clDayTo,&clTo[6],2);
	clDayTo[2] = '\0';

	dbg(DEBUG, "Account Init 7: Initializing from %s to %s.",clFrom,clTo);

	/* count number of employees */
	ilCountEmpl = CountElements(pcgSelKey);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"Account Init 7: Stopped, because no employees to initialize!");
		return;
	}
	dbg(DEBUG,"Account Init 7: Number of employees = <%d>", ilCountEmpl);

	/* loop per employee */
	for (ilCounter = 0; ilCounter < ilCountEmpl; ilCounter++)
	{
		if (GetElement(pcgSelKey,ilCounter,clCurrentEmpl) <= 0)
		{
			dbg(TRACE,"Account Init 7: Stopped, because <clCurrentEmpl> = undef!");
			return;
		}

		GetStaffData(clCurrentEmpl,&olStfData);

		/* Used in UpdateAccountXY! */
		WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

		dbg(DEBUG,"Account Init 7: Start calculation for employee <%s>",clCurrentEmpl);

		/* loop through the years */
		for (ilYearCount = atoi(clYearFrom); ilYearCount <= atoi(clYearTo); ilYearCount++)
		{
			ilQuartalCount = 0;

			dlFreitageQuartalPlan = 0;

			/* loop through the months */
			for (ilMonthCount = atoi(clMonFrom); ilMonthCount <= atoi(clMonTo); ilMonthCount++)
			{
				/* reset values */
				double dlFreitageMonPlan = 0.0e0;

				/* build time-variable for calculation */
				sprintf(clCalcDate,"%4d%02d%02d",ilYearCount,ilMonthCount,1);
				dbg(TRACE,"Account Init 7: Start calculation for date <%s>",clCalcDate);

				strcpy(clQuartalCODates[ilQuartalCount],clCalcDate);

				/* we take the same function of initialize account 4 - so we are independant if acc 4 is used or not */
				if (!CalculateInit4(clCurrentEmpl,clCalcDate,&dlFreitageMonPlan))
				{
					dbg(TRACE,"Account Init 7: ERROR in 'CalculateInit4'! Try to calculate next month!");
					continue;
				}
				else
				{
					dlFreitageQuartalPlan += dlFreitageMonPlan;
					dbg(DEBUG,"Account Init 7: <dlFreitageQuartalPlan> = <%lf>",dlFreitageQuartalPlan);
				}

				/* is the quarter complete? */
				if ((KorrigiereMonat(ilMonthCount,1)) == ilMonthCount)
				{
					/* round and cut */
					sprintf(clFreitageQuartalPlan,"%-8.2lf",dlFreitageQuartalPlan);
					dlFreitageQuartalPlan = atof(clFreitageQuartalPlan);

					/* update array-values to write in DB later */
					UpdateAccountXYHandle(clCurrentEmpl,dlFreitageQuartalPlan,"7",clQuartalCODates[0],"XBSHDL","CO");
					UpdateAccountXYHandle(clCurrentEmpl,dlFreitageQuartalPlan,"7",clQuartalCODates[1],"XBSHDL","CO");
					UpdateAccountXYHandle(clCurrentEmpl,dlFreitageQuartalPlan,"7",clQuartalCODates[2],"XBSHDL","CO");

					/* start new quarter, reset values */
					dlFreitageQuartalPlan = 0;
					ilQuartalCount = 0;
				}
				else
				{
					/* no - increment counter */
					++ilQuartalCount;
				}
			}
		}
	}
}

