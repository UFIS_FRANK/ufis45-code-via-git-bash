#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/bhsif.c 1.15b 2011/10/20  WAW gfo Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I Program bhsif.c    	                                      */
/*                                                                            */
/* Author         : Joern Weerts                                              */
/* Date           : 18.11.1998                                                */
/* Description    : Interface to the baggage handling system of Mannesmann    */
/*                  Dematic / Offenbach for New Athens International Airport /*/
/*                  Greece                                                    */
/* Update history : mos  30 nov 1999                                          */
/*                  replaced 'SUCCESS' with 'RC_SUCCESS'                      */
/*                  in HandleQueues (2x) and poll_q_and_sock (1x)             */
/*                  jwe 07.02.2000                                            */
/*                  completed interface                                       */
/*                  jwe 03.05.2000                                            */
/*                  made changes according to new Mannesmann Hostcom - Spec.  */
/*                  jwe 15.06.2000                                            */
/*                  made changes according to Mannesmann Hostcom - Spec.      */
/*                  date/who                                                  */
/*                  action                                                    */
/* 20060803 jim: remote side may request flight schedule table by tel-type 54 */
/* 20060803 jim: transfer of flight schedule table may be configured to FTP or TCP */
/*                 avoid NULL pointer in dbg() and other printf()             */
/* 20060803 jim: delete chutes with indicator 'D'                             */
/* 20060803 jim: brackets removed, in any case do a malloc to                 */
/*                 avoid NULL pointer in dbg() and other printf()             */
/* 20060810 jim: delete chutes with "empty chute" also (configurable)         */
/* 20060810 jim: Check for "Chute Update" length 74 (class miss., configurable)*/
/* 20060810 jim: Don't add " " to 2 letter ALC (configurable)                 */
/* 20070117 jim: get the Baggage Handling agent according ALC3, max 3 Chars!  */ 
/* 20070117 JIM: corrected if igCAI_ShortLength <> 0, use it, not only ==74   */
/* 20070117 JIM: use igCAI_ShortLength also for CST telegrams                 */
/* 20070117 JIM: indicator F/N/L: 'F' may be followed by 'F', only one chute  */
/* 20070117 JIM: corrected ALC2 in FLNO                                       */
/* 20070117 JIM: free() new config entries                                    */
/* 20110929 GFO: UFIS-881 , UFIS-882                                          */  
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include "bhsif.h"
/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */      
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;

extern int snap(char*,long,FILE*);
extern int get_real_item(char *,char *,int);
extern void BuildItemBuffer(char *, char *, int, char *);
extern int GetDataItem(char*,char*,int,char,char*,char*);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static int igCnt = 0;
static ITEM  *prgItem      = NULL;    	/* The queue item pointer  */
static int igItemLen     	 = 0;  	/* length of incoming item */
static EVENT *prgEvent     = NULL;    	/* The event pointer       */
static CFG  prgCfg;			/* pointer to cfg-file structure */
static FTPConfig prgFtp;		/* struct for hyper-dynamic programming of FTPHDL */
static char pcgProcessName[20];      	/* buffer for my own process name */ 
static char pcgBHS_Host[64];    	/* buffer for the host that is connected */ 
/* 20060802 JIM: Prf 8352: FTP or TCP */
static char *pcgUseFtpFile;    		/* YES: send flight schedule table via FTP, NO: send via TCP */ 
static char pcgHomeAp[20];      	/* buffer for name of the home-airport */
static char pcgTabEnd[20];      	/* buffer for name of the table extension */
static char pcgAftTable[20];		/* buffer for AFT-table to use */
static int igHeadLen 		   = 0;	/* length of telegram header */
static int igTailLen 		   = 0;	/* length of telegram header */
static int igAckLen 		   = 0;	/* length of telegram header */
static int igInitOK   		 = FALSE;	/* init flag */
static int igSock 		 = 0;	/* global tcp/ip socket for receive */
static int igMsgCounter 	 = 0;	/* counter for message numbers */
static int igWaitForAck 	 = 0;	/* seconds for waiting for acknowledge */
static int igMaxSends 	 	 = 0;	/* maximum number of sends for telegram */
static int igModID_Flight  = 0;		/* MOD-ID of Flight */
static int igModID_Router  = 0;		/* MOD-ID of Router  */
static int igModID_Dysif  = 0;		/* MOD-ID of Dysif  */
static int igModID_Ftphdl  = 0;		/* MOD-ID of Ftphdl  */
static int igSeqWriteQueue  = 0;	/* dedicated queue for inserts, updates, IRT, URT */
static int igRecvTimeout	 = 0;	/* number of sec. to wait for a valid telegram */
static int igFtpTimeout	 = 0;		
static time_t tgRecvTimeOut	 = 0;	/* time until to bhsif waits for a valid telegram */
static time_t tgNextConnect	 = 0;	/* time for trying to establish a con. to bhs*/
static char pcgConfFile[S_BUFF];	/* buffer for filename */
static char pcg_HSB_ConfFile[S_BUFF];	/* buffer for filename */
static char	*pcgSecondQueue = "bhs_2";
static int 	igSecondQueue = 0;
static char	*pcgValid_SFS_Fields = "flno,stod,etdi,alc3,act3,nose,vian,vial,des3";
static char	*pcgValid_SFX_Fields = "tifd,adid,ftyp,flno,stod,etdi,alc3,act3,nose,vian,vial,des3";
static char	*pcgFtypClause = "AND FTYP IN ('O','R') AND ADID in ('B','D')";
static char	*pcgChaInsertFields = "CDAT,DES3,DES4,FCLA,FTYP,HOPO,INDI,LNAM,LSTU,LTYP,RTAB,RURN,STOB,STOE,TIFB,TIFE,TISB,TISE,URNO,USEC,USEU";
static char cgNextAllowedIndi1 = 'F'; /* initial value for the indicator inside a CAI telegram */
static char cgNextAllowedIndi2 = 'S'; /* initial value for the indicator inside a CAI telegram */
static int igFirstRc = RC_SUCCESS;
static LPLISTHEADER prgRecvMsgList = NULL;/* pointer to internal message list */
static int igCAI_ShortLength = 0; /* Flag to check for telexes with wrong lenth */

static char prgSendBuffer[MAX_TELEGRAM_LEN]; /* global used buffer for socket-write */ 
static char prgRecvBuffer[MAX_TELEGRAM_LEN]; /* global used buffer for socket-read */ 
static TAIL prgEnd;													/* telegram end struct */

static BOOL bgSocketOpen = FALSE;	/* global flag for connection-state */
static BOOL bgAlarm 	 = FALSE;	/* global flag for timed-out socket-read */
static BOOL bgWaitForAck = FALSE;	/* global flag for ack-waiting */
static BOOL bgUseHopo	 = FALSE;	/* global flag for use of HOPO database field */

static FILE *pgReceiveLogFile = NULL;	/* filepointer for all received telegrams*/
static FILE *pgSendLogFile = NULL;	/* filepointer for all send telegrams*/

static int igTerminalChange;
static int igTerminalEquals2 = FALSE;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
/*----------------*/
/* MAIN functions */
/*----------------*/
static int Init_bhsif(void);		/* main initializing */	
static int Reset(void);         /* Reset program          */
static int HandleInternalData(void);        /* Handles data from CEDA */
static int HandleExternalData(char *pcpData);/* handles data from BHS */
static void	Terminate(BOOL bpSleep);        /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/
static int ConnectTCP(char *pcpHost,int ipTry);
static void CloseTCP();
static int ReadCfg(void);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
                       char **pcpDest,int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int CheckCfg(void);
static void FreeCfgMem(void);
static int poll_q_and_sock ();
static int Receive_data(int ipSock,int ipAlarm,char *pcpCalledBy);
static int Send_data(int ipSock,char *pcpData,int ipLen);
static int WaitForAck(int ipSock,char *pcpMsg,int ipLen);
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddLen,int ipModIdSend,int ipModIdRecv,
                     int ipPriority,char *pcpTable,char *pcpType,char *pcpFile);
static int ProcessFlights(char *pcpFlights,int ipType,char *pcpSendBuffer,char *pcpFile);
static int Convert_SFX(char *pcpData);
static int Convert_CXX(char *pcpData);
static char *GetViaByNumber(char *pcpVial,int ipViaNo, int ipVia34);
static int CreateFlightTables();
static int SendFileViaFtp(char *pcpHost,char *pcpFile);
static int CheckFlight(char *pcpData,char *pcpFieldList,int ipActionType,int *ipType);
/*---------------------*/
/* formating functions */
/*---------------------*/
static void GetMsgTimeStamp(char *pcpTimeStr);
static void TrimRight(char *pcpBuffer);
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType);
static void CleanMessage(char *pcpMsg,int ipMsgLen);
static void TrimNewLine(char *pcpString);
static int MakeIntFlno(char *pcpFlno);
static int MakeExtFlno(char *pcpFlno);
/*------------------------*/
/* functions to get infos */
/*------------------------*/

/*-------------------*/
/* logging functions */
/*-------------------*/
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile);
static void StoreRecvMessage(char *pcpMsg); /* stores incoming messages */
static void HandleStoredData(int ipTimesToDo);/* Handles stored, received messages */

/*-----------------------------------------*/
/* functions for internal created messages */
/*-----------------------------------------*/
static int PrepareData(int ipCmd, char *pcpSelection,time_t tpDayToSend);
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,
                            char *pcpDest);
static void GetMsgNumber(char *pcpMsgNumber);
static void MakeEnd(void);
static int MakeSendBuffer(int ipGetNr,int ipCmd, char *pcpData,char *pcpSendBuffer);
static void GetHandlingAgent(char *pcpAlc3, char *pcpFlno, char * pcpStod, char *pcpHandlingAgent);

/*---------------------------------*/
/* functions for external messages */
/*---------------------------------*/
static int CheckHeader(char *pcpData,int *ipRecvCmd);
static int ProcessData(char *pcpData,int ipCmd,BOOL bpAck);
static int Process_CAI_Telegramm(char *pcpData);
static int Build_CAI_Data(char *pcpData,CAI_DB **prpCaiInt,int ipDoSelect);
static int DeleteOld_CAI_Data(CAI_DB *prpCaiInt);
static int DeleteOldRecord(char *pcpUrno);
static int SetNew_CAI_Data(CAI_DB *prpCaiInt);
static int Process_CST_Telegramm(char *pcpData);
static int Build_CST_Data(char *pcpData,CST_DB **prpCstInt);
static int SetNew_CST_Data(CST_DB *prpCstInt);
static int Send_FTF_Msg();
static void Send_Ack_Msg();
static void Send_New_Alive();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int ilRc = RC_SUCCESS;	/* Return code */
  int ilCnt = 0;

  INITIALIZE;	/* General initialization	*/

  /* handles signal		*/
  (void)SetSignals(HandleSignal);

  /* copy process-name to global buffer */
  memset(pcgProcessName,0x00,sizeof(pcgProcessName));
  strcpy(pcgProcessName, argv[0]);                            

  dbg(TRACE,"MAIN: version <%s>",mks_version);

  /* Attach to the MIKE queues */
  do
  {
     ilRc = init_que();
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
        sleep(6);
        ilCnt++;
     }
  } while ((ilCnt < 10) && (ilRc != RC_SUCCESS));
	
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
     sleep(60);
     exit(1);
  }
  else
  {
     dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
  }
  /* Attach to the DB */
  do
  {
     ilRc = init_db();
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
        sleep(6);
        ilCnt++;
     } /* end of if */
  } while ((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
     sleep(60);
     exit(2);
  }
  else
  {
     dbg(TRACE,"MAIN: init_db()  OK!");
  } /* end of if */
  /**********************************************************/
  /* following lines for identical binaries on HSB-machines */
  /**********************************************************/
  *pcgConfFile = '\0'; 
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),pcgProcessName);
  ilRc = TransferFile(pcgConfFile); 
  if (ilRc != RC_SUCCESS) 
  {
     dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
  }
	
  sprintf(pcg_HSB_ConfFile,"%s/hsb.dat",getenv("CFG_PATH"));
  /*sprintf(pcg_HSB_ConfFile,"%s/hsb.jwe",getenv("CFG_PATH"));*/
  /********************************************************************/
  /*For identical cfg-files on HSB-machines uncomment following lines */
  /********************************************************************/
  *pcgConfFile = '\0';
  sprintf(pcgConfFile,"%s/%s.cfg",getenv("CFG_PATH"),pcgProcessName);
  ilRc = TransferFile(pcgConfFile); 
  if (ilRc != RC_SUCCESS) 
  { 
     dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
  } 
  ilRc = SendRemoteShutdown(mod_id);
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
  }
	
  /* now waiting if I'm STANDBY */
  if ((ctrl_sta != HSB_STANDALONE) &&
      (ctrl_sta != HSB_ACTIVE) &&
      (ctrl_sta != HSB_ACT_TO_SBY))
  {
     HandleQueues();
  }

  dbg(TRACE,"--------------------------------");
  if ((ctrl_sta == HSB_STANDALONE) ||
      (ctrl_sta == HSB_ACTIVE) ||
      (ctrl_sta == HSB_ACT_TO_SBY))
  {
     dbg(TRACE,"MAIN: initializing ...");
     dbg(TRACE,"--------------------------------");
     if (igInitOK == FALSE)
     {
        ilRc = Init_bhsif();
        if (ilRc != RC_SUCCESS)
        {
           dbg(TRACE,"--------------------------------");
           dbg(TRACE,"MAIN: Init_bhsif() failed! Terminating!");
           Terminate(TRUE);
        }
     }
  } 
  else
  {
     dbg(TRACE,"MAIN: wrong HSB-state! Terminating!");
     Terminate(FALSE);
  }
	
  if (ilRc == RC_SUCCESS)
  {
     dbg(TRACE,"MAIN: initializing OK");
     dbg(TRACE,"--------------------------------");
		
     if (strcmp(prgCfg.mode,"TEST") == 0)
     {
        ilRc = CreateFlightTables();
     }

     while (TRUE)
     {
        if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
        {
           if ((ilRc = HandleExternalData(prgRecvBuffer)) != RC_SUCCESS)
           {
              dbg(TRACE,"MAIN: HandleExternalData() failed!");
           }
        }
     }
  }
  else
  {
     dbg(TRACE,"MAIN: Init_bhsif() invalid! Terminating!");
  }
  Terminate(TRUE);
#ifdef _WINNT
  return(0);
#endif
} /* end of MAIN */
/*********************************************************************
Function	 :Init_bhsif
Paramter	 :IN: ipInitPhase = which parameters should be initalized 
Returnvalue:RC_SUCCES,RC_FAIL
Description:initializes the interface with all necessary values
*********************************************************************/
static int Init_bhsif()
{
  int ilRc = RC_FAIL; /* Return code */
	
  igHeadLen = sizeof(HEAD);	
  igTailLen = sizeof(TAIL);	
  igAckLen = sizeof(ACK_EXT);	
  /* get mod-id of flight */
  if ((igModID_Flight = tool_get_q_id("flight")) == RC_NOT_FOUND ||
     igModID_Flight == RC_FAIL)
  {
     dbg(TRACE,"Init_bhsif: tool_get_q_id(flight) returns: <%d>",igModID_Flight);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"Init_bhsif : flight mod_id <%d>",igModID_Flight);
  }

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL)
  {
     dbg(TRACE,"Init_bhsif: tool_get_q_id(router) returns: <%d>",igModID_Router);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"Init_bhsif : router mod_id <%d>",igModID_Router);
  }

  /* get mod-id of dysif (dynamic signage interface athen's) */
  if ((igModID_Dysif = tool_get_q_id("dysif")) == RC_NOT_FOUND ||
      igModID_Dysif == RC_FAIL)
  {
     dbg(TRACE,"Init_bhsif: tool_get_q_id(dysif) returns: <%d>",igModID_Dysif);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"Init_bhsif : dysif  mod_id <%d>",igModID_Dysif);
  }

  /* get mod-id of ftphdl */
  if ((igModID_Ftphdl = tool_get_q_id("ftphdl")) == RC_NOT_FOUND ||
      igModID_Ftphdl == RC_FAIL)
  {
     dbg(TRACE,"Init_bhsif: tool_get_q_id(ftphdl) returns: <%d>",igModID_Ftphdl);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"Init_bhsif : ftphdl mod_id <%d>",igModID_Ftphdl);
  }

  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"Init_bhsif : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"Init_bhsif : HOMEAP = <%s>",pcgHomeAp);
  }
	
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"Init_bhsif : No TABEND entry in sgs.tab: EXTAB! Please add!");
     return RC_FAIL;
  }
  else
  {
     *pcgAftTable = 0x00;
     strcpy(pcgAftTable,"AFT");
     dbg(TRACE,"Init_bhsif : TABEND = <%s>",pcgTabEnd);
     strcat(pcgAftTable,pcgTabEnd);
     dbg(TRACE,"Init_bhsif : AFT-table = <%s>",pcgAftTable);
     if (strcmp(pcgTabEnd,"TAB") == 0)
     {
        bgUseHopo = TRUE;
        dbg(TRACE,"Init_bhsif : use HOPO-field!");
     }
  }

  dbg(TRACE,"Init_bhsif: Delete old, possible left queues !");
  DeleteOldQueues(pcgSecondQueue);

  /* Get a second queue for answers on deletes */
  /* (for automatic balance) */
  if (ilRc == RC_SUCCESS)
  {
     if ((ilRc = GetDynamicQueue(&igSecondQueue,pcgSecondQueue)) != RC_SUCCESS)
     {
        dbg(TRACE,"Init_bhsif: GetDynamicQueue() failed. ilRc=<%d>!",ilRc);
        return ilRc;
     }
     else
     {
        dbg(TRACE,"Init_bhsif: my second queue is <%s=%d>.",pcgSecondQueue,igSecondQueue);
     }
  }

  memset(prgSendBuffer,0x00,MAX_TELEGRAM_LEN);
  memset(prgRecvBuffer,0x00,MAX_TELEGRAM_LEN);
  MakeEnd();	

  if ((prgRecvMsgList=ListInit(prgRecvMsgList,sizeof(MSG_RECV))) == NULL)
  {
     dbg(TRACE,"Init_bhsif : couldn't create internal Recv.-Msg-List!");
     return RC_FAIL;
  }

  /* now reading from configfile */
  if (*pcgConfFile != 0x00)
  {
     if ((ilRc = ReadCfg()) == RC_SUCCESS)
     {
        if ((ilRc = CheckCfg()) == RC_SUCCESS)
        {
           igInitOK = TRUE;
           if (strlen(prgCfg.recv_log) > 0)
           {
              pgReceiveLogFile = fopen(prgCfg.recv_log,"w");	
              if (!pgReceiveLogFile)
              {
                 dbg(TRACE,"Init_bhsif : ReceiveLog: <%> not opened! fopen() returns <%s>.",
                     prgCfg.recv_log,strerror(errno));
              }
           }
				
           if (strlen(prgCfg.send_log) > 0)
           {
              pgSendLogFile = fopen(prgCfg.send_log,"w");	
              if (!pgSendLogFile)
              {
                 dbg(TRACE,"Init_bhsif : SendLog: <%> not opened! fopen() returns <%s>.",
                     prgCfg.send_log,strerror(errno));
              }
           }
           tgRecvTimeOut = time(0) + (time_t)(igRecvTimeout - READ_TIMEOUT);
        }
        else
        {
           dbg(TRACE,"Init_bhsif : CheckCfg() failed!");
        }
     }
     else
     {
        dbg(TRACE,"Init_bhsif : ReadCfg() failed!");
     }
  }

  return(ilRc);
} /* end of initialize */

/*********************************************************************
Function	 :Reset()
Paramter	 :IN:                                                   
Returnvalue:RC_SUCCESS,RC_FAIL
Description:rereads the config-file,reinitializes the recv. and send
            log-files and clears the receive and the send buffers
*********************************************************************/
static int Reset(void)
{
  int ilRc = RC_SUCCESS;
	
  dbg(TRACE,"Reset: now resetting ...");
  dbg(TRACE,"Reset: closing TCP-IP connection.",ilRc);
  CloseTCP();
	
  tgNextConnect = 0;

  memset(prgSendBuffer,0x00,MAX_TELEGRAM_LEN);
  memset(prgRecvBuffer,0x00,MAX_TELEGRAM_LEN);

  FreeCfgMem();

  ListDestroy(prgRecvMsgList);

  dbg(TRACE,"Reset: closing Log-files.");
  if (pgReceiveLogFile != NULL)
     fclose(pgReceiveLogFile);
  if (pgSendLogFile != NULL)
     fclose(pgSendLogFile);

  igMsgCounter = 0;
	
  if ((prgRecvMsgList=ListInit(prgRecvMsgList,sizeof(MSG_RECV))) == NULL)
  {
     dbg(TRACE,"Init_bhsif : couldn't create internal Recv.-Msg-List!");
     return RC_FAIL;
  }

  if (*pcgConfFile != '\0')
  {
     if ((ilRc = ReadCfg()) != RC_SUCCESS)
     {
        dbg(TRACE,"Reset: ReadCfg() failed!");
        ilRc = RC_FAIL;
     }
     else
     {
        if ((ilRc = CheckCfg()) != RC_SUCCESS)
        {
           dbg(TRACE,"Reset: CheckCfg() failed!");
           ilRc = RC_FAIL;
        }
        else
        {
           if (strlen(prgCfg.recv_log) > 0)
           {
              pgReceiveLogFile = fopen(prgCfg.recv_log,"w");	
              if (!pgReceiveLogFile )
              {
                 dbg(TRACE,"Reset: ReceiveLog <%> not opened",prgCfg.recv_log);
              }
           }
				
           if (strlen(prgCfg.send_log) > 0)
           {
              pgSendLogFile = fopen(prgCfg.send_log,"w");	
              if (!pgSendLogFile)
              {
                 dbg(TRACE,"Reset: Send_log <%> not opened",prgCfg.send_log);
              }
           }
        }
     }
  }
  dbg(TRACE,"Reset: ... finished!");
  return ilRc;
} 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(BOOL bpSleep)
{
int ilRc = 0;

  if (bpSleep == TRUE)
  {
     dbg(TRACE,"Terminate: sleeping 60 sec. before terminating.");
     sleep(60);
  }

  if (igInitOK == TRUE)
  {
     CloseTCP();
     dbg(TRACE,"Terminate: TCP/IP-connections closed.");

     if (pgReceiveLogFile != NULL)
        fclose(pgReceiveLogFile);
     if (pgSendLogFile != NULL)
        fclose(pgSendLogFile);
     dbg(TRACE,"Terminate: Log-files closed.");

     FreeCfgMem();
     dbg(TRACE,"Terminate: Config-memory freed.");

     ListDestroy(prgRecvMsgList);
     dbg(TRACE,"Terminate: Unacked messages deleted.");

     dbg(TRACE,"Terminate: removing second queue <%d>!",igSecondQueue);
     if ((ilRc = que(QUE_DELETE,igSecondQueue,igSecondQueue,0,0,0)) != RC_SUCCESS)
     {
        dbg(TRACE,"Terminate: error removing second queue <%d>",igSecondQueue);
     }
  }
  dbg(TRACE,"Terminate: now leaving ......");
  exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
  switch (ipSig)
  {
     case SIGPIPE:
        dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGPIPE).",ipSig);
        CloseTCP();
        break;
     case SIGTERM:
        dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
        Terminate(FALSE);
        break;
     case SIGALRM:
        /*dbg(DEBUG,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);*/
        bgAlarm = TRUE;
        break;
     case SIGCHLD:
        dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGCHLD)",ipSig);
        break;
     default:
        dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
        Terminate(FALSE);
        break;
  } 
} 

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
  switch(pipErr)
  {
     case QUE_E_FUNC:     /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
     case QUE_E_MEMORY:   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
     case QUE_E_SEND:     /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
     case QUE_E_GET:      /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
     case QUE_E_EXISTS:
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
     case QUE_E_NOFIND:
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
     case QUE_E_ACKUNEX:
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
     case QUE_E_STATUS:
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
     case QUE_E_INACTIVE:
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
     case QUE_E_MISACK:
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
     case QUE_E_NOQUEUES:
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
     case QUE_E_RESP:	  /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
     case QUE_E_FULL:
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
     case QUE_E_NOMSG:    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
     case QUE_E_INVORG:   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
     case QUE_E_NOINIT:   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
     case QUE_E_ITOBIG:
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
     case QUE_E_BUFSIZ:
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
     case QUE_E_PRIORITY:
        dbg(TRACE,"<%d> : invalid priority was send ",pipErr);
        break;
     default:             /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
  } /* end switch */
  return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int ilRc = RC_SUCCESS; /* Return code */
  int ilBreakOut = FALSE;
	
  dbg(TRACE,"HandleQueues: now entering ...");
  do
  {
     /*memset(prgItem,0x00,igItemLen);*/
     ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
     /* depending on the size of the received item  */
     /* a realloc could be made by the que function */
     /* so do never forget to set event pointer !!! */
     prgEvent = (EVENT *) prgItem->text;	
     if (ilRc == RC_SUCCESS)
     {
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if (ilRc != RC_SUCCESS) 
        {
           /* handle que_ack error */
           HandleQueErr(ilRc);
        } /* fi */
		
        switch(prgEvent->command)
        {
           case HSB_STANDBY:
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_STANDBY event!");
              break;	
           case HSB_COMING_UP:
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_COMING_UP event!");
              break;	
           case HSB_ACTIVE:
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_ACTIVE event!");
              dbg(TRACE,"HandleQueues: Terminating for new init ...");
              Terminate(FALSE);
              ilBreakOut = TRUE;
              break;	
           case HSB_ACT_TO_SBY:
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_ACT_TO_SBY event!");
              break;	
           case HSB_DOWN:
              /* 	whole system shutdown - do not further use que(), */
              /*	send_message() or timsch() ! */
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_DOWN event!");
              Terminate(FALSE);
              break;	
           case HSB_STANDALONE:
              ctrl_sta = prgEvent->command;
              dbg(TRACE,"HandleQueues: received HSB_STANDALONE event!");
              dbg(TRACE,"HandleQueues: Terminating for new init ...");
              Terminate(FALSE);
              ilBreakOut = TRUE;
              break;	
           case REMOTE_DB:
              /* ctrl_sta is checked inside */
              /*HandleRemoteDB(prgEvent);*/
              break;
           case SHUTDOWN:
              dbg(TRACE,"HandleQueues: received SHUTDOWN event!");
              Terminate(FALSE);
              break;
           case RESET:
              dbg(TRACE,"HandleQueues: received RESET event!");
              ilRc = Reset();
              if (ilRc == RC_FAIL)
              {
                 Terminate(FALSE);
              }
              break;
           case EVENT_DATA:
              dbg(TRACE,"HandleQueues: wrong HSB-status <%d>",ctrl_sta);
              DebugPrintItem(TRACE,prgItem);
              DebugPrintEvent(TRACE,prgEvent);
              break;
           case TRACE_ON:
              dbg_handle_debug(prgEvent->command);
              break;
           case TRACE_OFF:
              dbg_handle_debug(prgEvent->command);
              break;
           default:
              dbg(TRACE,"HandleQueues: unknown event");
              DebugPrintItem(TRACE,prgItem);
              DebugPrintEvent(TRACE,prgEvent);
              break;
        } /* end switch */
     }
     else
     {
        /* Handle queuing errors */
        HandleQueErr(ilRc);
     } /* end else */
  } while (ilBreakOut == FALSE);

  if (igInitOK == FALSE)
  {
     ilRc = Init_bhsif();
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"Init_bhsif: init phase 2 failed!");
     } /* end of if */
  }/* end of if */
  dbg(TRACE,"HandleQueues: ... now leaving");
} /* end of HandleQueues */
	
/****************************************************************************** 
 *  GetHandlingAgent: get the Baggage Handling agent according ALC3 
 *                   from basic data
 * in:  pcpAlc3:          3 Letter Airline code
 * in:  pcpFlno:          Flight Number
 * in:  pcpStod:          Scheduled departure time
 * out: pcpHandlingAgent: Baggage Handling Agent, max 3 Chars according ICD!
*******************************************************************************/
static void GetHandlingAgent(char *pcpAlc3, char *pcpFlno, char * pcpStod, char *pcpHandlingAgent)
{
  char pclSyslibResultUrno[64];
  char pclSyslibHsnaKey[64];
  char pclSyslibResultHsna[32];
  int ilRC = RC_SUCCESS;
  int ilCnt = 0;
  int ilRCdb = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char pclSqlBuf[1024];
  char pclUrno[1024];

   dbg(TRACE,"GetHandlingAgent: ilUseHTAForBagAgent = <%d>",prgCfg.ilUseHTAForBagAgent);
   
  if (prgCfg.ilUseHTAForBagAgent == FALSE)
  {
     ilRC = syslibSearchDbData("ALTTAB","ALC3",pcpAlc3,"URNO",pclSyslibResultUrno,&ilCnt,"\n");
     if (ilCnt > 0)
     {
        ilCnt = 0;
        TrimRight(pclSyslibResultUrno);
        TrimRight(prgCfg.BagTaskName);
        sprintf(pclSyslibHsnaKey,"%s,%s",pclSyslibResultUrno,prgCfg.BagTaskName);
        ilRC = syslibSearchDbData("HAITAB","ALTU,TASK",pclSyslibHsnaKey,"HSNA",pclSyslibResultHsna,&ilCnt,"");
        if (ilCnt > 0)
        {
           TrimRight(pclSyslibResultHsna);
           dbg(TRACE,"GetHandlingAgent: Handling Agent for ALC3 <%s> for <%s> is <%s>",
               pcpAlc3,prgCfg.BagTaskName,pclSyslibResultHsna);
        }
        else
        {
           dbg(TRACE,"GetHandlingAgent: no Handling Agent for Airline with ALC3 <%s>",pcpAlc3);
        }
     }
     else
     {
        dbg(TRACE,"GetHandlingAgent: Airline with ALC3 <%s> unknown",pcpAlc3);
     }
  }
  else
  {
     ilCnt = 0;
     strcpy(pclSyslibResultHsna,"");
     sprintf(pclSqlBuf,"SELECT URNO FROM AFT%s WHERE FLNO = '%s' AND STOD = '%s' AND ADID = 'D'",
             pcgTabEnd,pcpFlno,pcpStod);
     slFkt = START; 
     slCursor = 0;
     dbg(TRACE,"GetHandlingAgent: SQL = <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclUrno);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        ilCnt++;
        sprintf(pclSqlBuf,"SELECT HSNA FROM HTA%s WHERE FLNU = %s AND HTYP = '%s' AND ADID = 'D'",
             pcgTabEnd,pclUrno,prgCfg.BagTaskCode);
        slFkt = START; 
        slCursor = 0;
        dbg(DEBUG,"GetHandlingAgent: SQL = <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclSyslibResultHsna);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           TrimRight(pclSyslibResultHsna);
           dbg(DEBUG,"GetHandlingAgent: Got Handling Agent = <%s>",pclSyslibResultHsna);
        }
        else
        {
           ilCnt = 0;
           strcpy(pclSyslibResultHsna,"");
        }
     }
  }
  if (ilCnt < 1 || strlen(pclSyslibResultHsna) == 0)
  {
     dbg(TRACE,"GetHandlingAgent: Airline with ALC3 <%s> unknown, Handling Agent defaults to <%s>",
         pcpAlc3,prgCfg.BagAgentDefault);
     if (strcmp(prgCfg.BagAgentDefault,"ALC3")==0)
        strcpy(pclSyslibResultHsna, pcpAlc3);
     else if (strcmp(prgCfg.BagAgentDefault,"BLANK")==0)
        strcpy(pclSyslibResultHsna, "   ");
     else
        strcpy(pclSyslibResultHsna, prgCfg.BagAgentDefault);
  }
  sprintf(pcpHandlingAgent,"%03.3s",pclSyslibResultHsna);
}

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int ilRc = RC_SUCCESS;		/* Return code */
  int ilDataLen = 0;			/* Return code */
  int ilType = 0;			/* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  FTPConfig *pclFtp = NULL;
  BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  CMDBLK  *cmdblk = NULL;	/* Command Block 		*/
  char pclLocalFile[S_BUFF];
  char pclTwStart[S_BUFF];
  char pclSendBuffer[XL_BUFF];
  int ilItemNo;
  char pclTerminal[16]  = "\0";
  
  char *pclOldData = NULL;
  char *pclOldDumm = "\0";
  char pclOldTerminal[16]  = "\0";
  
  char pcltmpFields[100] = "\0";
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  
  int igSendData = FALSE;
  
  char pclSqlBuf[L_BUFF];
  char pclTmpSqlAnswer[XL_BUFF];
  char pclDbData[DATABLK_SIZE];
  short slFkt = 0;
  short slCursor = 0;
  
  

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  
  dbg(TRACE,"");
  dbg(TRACE,"--- END/START HandleInternalData ---");

  dbg(TRACE,"HID: From<%d> Cmd<%s> Table<%s> Twstart<%s>",
      prgEvent->originator,cmdblk->command,cmdblk->obj_name,cmdblk->tw_start);
	
  /*DebugPrintItem(DEBUG,prgItem);*/
  /*DebugPrintEvent(DEBUG,prgEvent);*/

  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char*)pclFields + strlen(pclFields) + 1;
  
  strcpy(pcltmpFields,pclFields);
  /*TrimNewLine(pclSelection);*/
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }
  
  /* Get Old */
  pclOldData = strstr(pclData,"\n");
  if (pclOldData != NULL)
  {
    *pclOldData = 0x00;
    pclOldData++;
  } /* end if */
  else
  {
    pclOldData = pclOldDumm;
  } /* end else */


  dbg(TRACE,"HID: 2 Selection,URNO: <%s>,<%s>",pclSelection,pclUrno); 
  dbg(TRACE,"HID: Fields   : <%s>",pclFields); 
  dbg(TRACE,"HID: Data     : <%s>",pclData);
  dbg(TRACE,"HID: OldData     : <%s>",pclOldData);

    /*  UFIS-881 / UFIS-882
    * Special for WAW 
    * we need to send Cancellation Messages if the STEV field has been changed from 2 to another value
    * we need to send Cancellation Messages if the Flights has been deleted and the STEV was equal to 2 
    * we need to send any other Updates / Inserts only if STEV is 2
    * There for in the action we need to include the STEV field in all sections 
    * We need to send the old data 
    * snd_old_data = yes
    * Only mdblk->obj_name=="AFTTAB" OR "HTATAB" We receive a command 
    * From<7150> Cmd<IRT> Table<CHATAB> Twstart<BHSIF-INSERT,>
    */
  if (strcmp(prgCfg.term_list," ") != 0 && (strcmp(cmdblk->obj_name,"AFTTAB") ==0 || strcmp(cmdblk->obj_name,"HTATAB") ==0 )  ) {
    

    /******************************************************************************/
    /* receiving command from ACTION for change in the Handling Agents             */
    /******************************************************************************/
    if (strcmp(cmdblk->command, "SFUH") == 0 && strlen(pcgBHS_Host) > 0) {
        dbg(TRACE, "HID: <SFUH> command received.");

        ilItemNo = get_item_no(pclFields, "FLNU", 5) + 1;
        if (ilItemNo > 0) {
            
            get_real_item(pclUrno, pclData, ilItemNo);            
            sprintf(pclSelection, " WHERE URNO = %s", pclUrno);
            strcpy(cmdblk->command, "SFU");
            strcpy(cmdblk->obj_name, pcgAftTable);
            dbg(TRACE, "HID: !!!!!!!!!!! FLNU from a HTATAB change = %s", pclSelection);

        }
    }
  
    

    ilItemNo = get_item_no(pclFields, "STEV", 5) + 1;    
    strncpy(pclOldTerminal, "-1",1);
    strncpy(pclTerminal, "-1",1);   
    
    if (pclOldData != NULL && ilItemNo > 0)  {
       get_real_item(pclOldTerminal, pclOldData, ilItemNo);

    }         
    if (strcmp(cmdblk->command, "DFR") == 0) {       
        dbg(TRACE, "HID: command = %s", cmdblk->command);        
        sprintf(pclSelection, " WHERE URNO = %s", pclUrno);
    }
    
    
    if (ilItemNo > 0) {
        get_real_item(pclTerminal, pclData, ilItemNo);
    } else if (strlen(pclUrno) > 2 ){
        sprintf(pclSqlBuf, "SELECT %s FROM AFT%s  WHERE URNO = %s ", "URNO,STEV", pcgTabEnd, pclUrno);
        slFkt = START;
        slCursor = 0;
        dbg(TRACE, "HID: !!!!!!!!!!! pclTerminal NOT FOUND from Action get it from the DB sql = %s", pclSqlBuf);
        memset(pclTmpSqlAnswer, 0x00, XL_BUFF);
        memset(pclDbData, 0x00, DATABLK_SIZE);
        if ((ilRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclTmpSqlAnswer)) == RC_SUCCESS) {
            tool_filter_spaces(pclTmpSqlAnswer);
            GetDataItem(pclTerminal, pclTmpSqlAnswer, 2, '\0', "", "");
        } else {
            dbg(TRACE, "HID: !!!!!!!!!!! DB selection FAILED !!!!  sql = %s", pclSqlBuf);
        }
        close_my_cursor(&slCursor);

        dbg(TRACE, "HID: !!!!!!!!!!! pclTerminal NOT FOUND from Action get it from the DB pclTerminal = %s", pclTerminal);
    }

    dbg(TRACE, "HID: !!!!!!!!!!! pclOldTerminal %s", pclOldTerminal);
    dbg(TRACE, "HID: !!!!!!!!!!! pclTerminal %s cfg Term List %s", pclTerminal , prgCfg.term_list);

    igTerminalChange = FALSE;
    
    char *tmpoldTerm;
    char *tmpTerm;
    
    tmpoldTerm = strstr( prgCfg.term_list,pclOldTerminal);
    tmpTerm = strstr( prgCfg.term_list,pclTerminal);
    
    if (strcmp(cmdblk->command, "DFR") == 0 && ((tmpTerm != NULL && *pclTerminal != '\0') ||(tmpoldTerm != NULL && *pclOldTerminal != '\0'))   ) {
        dbg(TRACE, "HID: !!!!!!!!!!! IF 1");        
        igSendData = TRUE;
    } else if ((tmpoldTerm != NULL && *pclOldTerminal != '\0') &&  tmpTerm == NULL   )  {
        dbg(TRACE, "HID: !!!!!!!!!!! IF 2");
        strcpy(cmdblk->command, "CXX");
        igTerminalChange = TRUE;
        igSendData = TRUE;
    } else if (tmpTerm != NULL && *pclTerminal != '\0' ) {
        dbg(TRACE, "HID: !!!!!!!!!!! IF 3");
        igSendData = TRUE;
    }

    if (igSendData == FALSE) {
        dbg(TRACE, "HID: !!!!!!!!!!!!! DATA WILL NOT BE SEND .");
        return TRUE;
    }
    
  }
  
  /******************************************************************************/
  /* receiving command from NTISCH for sending the daily-flight-schedule to BHS */
  /******************************************************************************/
  if (strcmp(cmdblk->command,"SFS") == 0 && strlen(pcgBHS_Host) > 0)
  {
     dbg(TRACE,"HID: <SFS> command received.");
     if ((ilRc = PrepareData(SFS,"",0)) != RC_SUCCESS)
     {
        dbg(TRACE,"HID: PrepareData() failed!");
     }
  }

  /******************************************************************************/
  /* receiving answer on insert, update, delete */
  /******************************************************************************/
  if (strcmp(cmdblk->command,"DRT") == 0)
  {
     dbg(TRACE,"HID: <DRT> command received.");
     dbg(TRACE,"HID: RC from BHSIF-DELETE = <%d>",bchd->rc);
     if (strncmp(cmdblk->tw_start,"BHSIF-DELETE",12) != 0)
     {
        dbg(TRACE,"HID: received invalid BHSIF-DELETE!");
     }
     ilRc = bchd->rc;
  }

  /***************************************************************/
  /* receiving update from ACTION for sending a new flight to BHS */
  /***************************************************************/
  /* (taking it only if I wasn't the originator */
  if ((strcmp(cmdblk->obj_name,pcgAftTable) == 0 &&
      prgEvent->originator!=mod_id))
  {
      
        if (strcmp(cmdblk->command,"SFU") == 0)
        {
        dbg(TRACE,"HID: <SFU>-info from ACTION SEL=<%s>.",pclSelection); 
        if (igTerminalChange == FALSE)
           ilRc = PrepareData(SFU,pclSelection,0);
        else
           ilRc = PrepareData(CXX,pclSelection,0);
        }
        
        
/*
        if (strcmp(cmdblk->command,"SFU") == 0)
        {
         ilRc = PrepareData(SFU,pclSelection,0);
        }
*/
        
/*
     igTerminalChange = FALSE;
     if (strcmp(cmdblk->command,"SFU") == 0)
     {
        strcpy(pclTerminal,"");
        ilItemNo = get_item_no(pclFields,"STEV",5) + 1;
        if (ilItemNo > 0)
        {
           get_real_item(pclTerminal,pclData,ilItemNo);
           if (prgCfg.term_list[0] != ' ')
           {
              if (*pclTerminal == '\0' || 
                  strstr(prgCfg.term_list,pclTerminal) == NULL)
                 igTerminalChange = TRUE;
           }
        }
        TrimNewLine(pclSelection);
        dbg(DEBUG,"HID: <SFU>-info from ACTION SEL=<%s>.",pclSelection); 
        if (igTerminalChange == FALSE)
           ilRc = PrepareData(SFU,pclSelection,0);
        else
           ilRc = PrepareData(CXX,pclSelection,0);
     }
*/

     if (strcmp(cmdblk->command,"SFI") == 0)
     {
        TrimNewLine(pclData);
        dbg(TRACE,"HID: <SFI>-info from ACTION DATA=<%s>.",pclData); 
        ilRc = PrepareData(SFI,pclData,0);
     }

     if (strcmp(cmdblk->command,"CXX") == 0)
     {
        TrimNewLine(pclSelection);
        dbg(TRACE,"HID: <CXX>-info from ACTION SEL=<%s>.",pclSelection); 
        ilRc = PrepareData(CXX,pclSelection,0);
     }

     if (strcmp(cmdblk->command,"DFR") == 0)
     {
        TrimNewLine(pclSelection);
        dbg(TRACE,"HID: <DFR>-info from ACTION SEL=<%s>.",pclSelection); 
        ilRc = PrepareData(DFR,pclData,0);
     }
  }

  /**********************************/
  /* receiving answer from a FTPHDL */
  /**********************************/
  if (strcmp(cmdblk->command,"FTP") == 0)
  {
     dbg(DEBUG,"HID: answer from FTPHDL."); 
     pclFtp = (FTPConfig*) ((char*)pclData + strlen(pclData) + 1);
     if (pclFtp->iFtpRC==FTP_SUCCESS) 
     {
        dbg(TRACE,"HID: FTP to <%s> OK!",pclFtp->pcHostName);
        if (strcmp(pclFtp->pcHostName,pcgBHS_Host)==0)
        {
           dbg(DEBUG,"HID: Send_FTF_Msg()!"); 
           if ((ilRc = Send_FTF_Msg(pclFtp->pcRemoteFileName)) != RC_SUCCESS)
           {
              dbg(TRACE,"HID: Send_FTF_Msg() returns <%d>",ilRc);
           }
        }
     }
     else
     {
        dbg(TRACE,"HID: FTPHDL error <%d>!",pclFtp->iFtpRC);
        dbg(TRACE,"HID: check configuration and connection!");
     }
  }
  return ilRc;
} 

/* ********************************************************************/
/* The TRimNewLine() routine																					 */
/* ********************************************************************/
static void TrimNewLine(char *pcpString)
{
  char *pclPointer = NULL;

  if ((pclPointer=strchr(pcpString,'\n')) != NULL)
  {
     *pclPointer = '\0';
  }
}
/* ********************************************************************/
/* The ReadCfg() routine																					 */
/* ********************************************************************/
static int ReadCfg(void)
{
  int ilRc = RC_SUCCESS;
  int ilCnt = 0;
  char *pclTmpBuf;

  memset(&prgCfg,0x00,sizeof(CFG));
	
  dbg(TRACE,"--------------------------------");
  dbg(TRACE,"ReadCfg: file <%s>",pcgConfFile);
  dbg(TRACE,"--------------------------------");
  /* reading [MAIN] section from cfg-file */
  ilRc = GetCfgEntry(pcgConfFile,"MAIN","MODE",CFG_STRING,&prgCfg.mode,CFG_ALPHA,"REAL");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc = GetCfgEntry(pcgConfFile,"MAIN","DB_WRITE",CFG_STRING,&prgCfg.db_write,CFG_ALPHA,"N");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;

  (void) GetCfgEntry(pcgConfFile,"MAIN","SEQ_WRITE_QUEUE",CFG_STRING,&prgCfg.seq_write_queue,CFG_NUM,"7150");
  igSeqWriteQueue=atoi(prgCfg.seq_write_queue);

  ilRc=GetCfgEntry(pcgConfFile,"MAIN","DB_FILTER",CFG_STRING,&prgCfg.db_filter,CFG_PRINT," ");
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","CXX_FILTER",CFG_STRING,&prgCfg.cxx_filter,CFG_PRINT," ");
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","TERM_FILTER",CFG_STRING,&prgCfg.term_filter,CFG_PRINT," ");
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","TERM_LIST",CFG_STRING,&prgCfg.term_list,CFG_PRINT," ");
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","MY_SYS_ID",CFG_STRING,&prgCfg.my_sys_id,CFG_ALPHANUM,"F0");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","BHS_HOST1",CFG_STRING,&prgCfg.bhs_host1,CFG_PRINT,"");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","BHS_HOST2",CFG_STRING,&prgCfg.bhs_host2,CFG_PRINT,"");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","SERVICE_PORT",CFG_STRING,&prgCfg.service_port,CFG_PRINT,"EXCO_BHS");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","WAIT_FOR_ACK",CFG_STRING,&prgCfg.wait_for_ack,CFG_NUM,"2");
  if (ilRc != RC_SUCCESS)
  {
     return RC_FAIL;
  }
  else
  {
     igWaitForAck = atoi(prgCfg.wait_for_ack);
  }
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","RECV_TIMEOUT",CFG_STRING,&prgCfg.recv_timeout,CFG_NUM,"60");
  if (ilRc != RC_SUCCESS)
  {
     return RC_FAIL;
  }
  else
  {
     igRecvTimeout = atoi(prgCfg.recv_timeout);
  }
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","MAX_SENDS",CFG_STRING,&prgCfg.max_sends,CFG_NUM,"2");
  if (ilRc != RC_SUCCESS)
  {
     return RC_FAIL;
  }
  else
  {
     igMaxSends = atoi(prgCfg.max_sends);
  }
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","RECV_LOG",CFG_STRING,&prgCfg.recv_log,CFG_PRINT,
                   "/ceda/debug/bhsif_recv.log");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","SEND_LOG",CFG_STRING,&prgCfg.send_log,CFG_PRINT,
                   "/ceda/debug/bhsif_send.log");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","TRY_RECONNECT",CFG_STRING,&prgCfg.try_reconnect,CFG_NUM,"60");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","REMOTE_SYS_ID",CFG_STRING,&prgCfg.remote_sys_id,CFG_ALPHA,"BD");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_OFFSET",CFG_STRING,&prgCfg.fs_offset,CFG_NUM,"7");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;

  /* 20060803 JIM: Prf 8352: FTP or TCP */
  GetCfgEntry(pcgConfFile,"MAIN","TABLE_BY_FTP",CFG_STRING,&pcgUseFtpFile,CFG_ALPHA,"YES");

  GetCfgEntry(pcgConfFile,"MAIN","CAI_SHORT_LENGTH",CFG_STRING,&prgCfg.CAI_ShortLength,CFG_NUM,"0");
  igCAI_ShortLength= atoi(prgCfg.CAI_ShortLength);

  GetCfgEntry(pcgConfFile,"MAIN","EXT_ALC2_ADD_BLANK",CFG_STRING,&prgCfg.ExtAlc2AddBlank,CFG_ALPHA,"Y");
  GetCfgEntry(pcgConfFile,"MAIN","EMPTY_CHUTE_DELETES",CFG_STRING,&prgCfg.EmptyChuteDeletes,CFG_ALPHA,"Y");

  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_REMOTE_PATH",CFG_STRING,&prgCfg.fs_remote_path,CFG_PRINT,
                   "/ceda/tmp");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_LOCAL_PATH",CFG_STRING,&prgCfg.fs_local_path,CFG_PRINT,"/");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_CLIENT_OS",CFG_STRING,&prgCfg.ftp_client_os,CFG_ALPHA,"UNIX");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_USER",CFG_STRING,&prgCfg.ftp_user,CFG_ALPHANUM,"none");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_PASS",CFG_STRING,&prgCfg.ftp_pass,CFG_IGNORE,"none");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_TIMEOUT",CFG_STRING,&prgCfg.ftp_timeout,CFG_NUM,"1");
  if (ilRc != RC_SUCCESS)
     igFtpTimeout=1;
  else
     igFtpTimeout=atoi(prgCfg.ftp_timeout);
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","AFTER_FTP",CFG_STRING,&prgCfg.after_ftp,CFG_ALPHA,"D");
  if (ilRc != RC_SUCCESS)
     return RC_FAIL;

  prgCfg.ilReceiveTransitInfo = FALSE;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","RECEIVE_TRANSIT_INFO",CFG_STRING,&pclTmpBuf,CFG_ALPHA,"NO");
  if (ilRc == RC_SUCCESS && strcmp(pclTmpBuf,"YES") == 0)
  {
     prgCfg.ilReceiveTransitInfo = TRUE;
     strcpy(pcgChaInsertFields,"CDAT,DES3,DES4,FCLA,LAFX,FTYP,HOPO,INDI,LNAM,LSTU,LTYP,RTAB,RURN,STOB,STOE,TIFB,TIFE,TISB,TISE,URNO,USEC,USEU");
  }

  prgCfg.ilUseHTAForBagAgent = FALSE;
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","USE_HTATAB_FOR_BAG_AGENT",CFG_STRING,&pclTmpBuf,CFG_ALPHA,"NO");
  if (ilRc == RC_SUCCESS && strcmp(pclTmpBuf,"YES") == 0)
     prgCfg.ilUseHTAForBagAgent = TRUE;

  /* 20070117 jim: get the Baggage Handling agent according ALC3, max 3 Chars!  */ 
  /* Code and name of Baggage Task are not fix in DB, so */
  /* they have to be configured for retrieving Handling Agent */
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","BAG_TASK_CODE",CFG_STRING,&prgCfg.BagTaskCode,CFG_ALPHA,"BG");
  if (ilRc == RC_SUCCESS)
  {
     dbg(TRACE,"BAG_TASK_CODE: <%s> (RC_SUCCESS)",prgCfg.BagTaskCode);
     ilRc = syslibSearchDbData("HTYTAB", "HTYP", prgCfg.BagTaskCode, "HNAM", prgCfg.BagTaskName, &ilCnt, "");
     if (ilCnt > 0)
     {
        dbg(TRACE,"BAG_TASK_NAME: using <%s> ",prgCfg.BagTaskName);
     }
     else
     {
        strcpy(prgCfg.BagTaskName,"Baggage");
        dbg(TRACE,"BAG_TASK_NAME: set to default <%s> ",prgCfg.BagTaskName);
     }
  }
  else
  {
     strcpy(prgCfg.BagTaskName,"Baggage");
     dbg(TRACE,"BAG_TASK_NAME: set to default <%s> ",prgCfg.BagTaskName);
  }
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","BAG_AGENT_DEF",CFG_STRING,&prgCfg.BagAgentDefault,CFG_ALPHANUM,"XXX");
  if (ilRc == RC_SUCCESS)
  {
     dbg(TRACE,"BAG_AGENT_DEF: read <%s>",prgCfg.BagAgentDefault);
/*
     if (strcmp(prgCfg.BagAgentDefault,"XXX")!=0 && strcmp(prgCfg.BagAgentDefault,"ALC3")!=0)
     {
        free(prgCfg.BagAgentDefault);
        prgCfg.BagAgentDefault=strdup("XXX");
        dbg(TRACE,"BAG_AGENT_DEF: set to default <%s> ",prgCfg.BagAgentDefault);
     }
*/
  }
  else
  {
     prgCfg.BagAgentDefault=strdup("XXX");
     dbg(TRACE,"BAG_AGENT_DEF: set to default <%s>",prgCfg.BagAgentDefault);
  }
  dbg(TRACE,"--------------------------------");
  ilRc=GetCfgEntry(pcgConfFile,"MAIN","DEBUG_LEVEL",CFG_STRING,&prgCfg.debug_level,CFG_NUM,"0");
  if (ilRc == RC_SUCCESS)
  {
     if ((debug_level=atoi(prgCfg.debug_level))==0)
     {
        debug_level = 0;
     }
     else
     {
        if ((debug_level=atoi(prgCfg.debug_level))==1)
        {
           debug_level = TRACE;
        }
        else
        {
           if ((debug_level=atoi(prgCfg.debug_level))==2)
           {
              debug_level = DEBUG;
           }
           else
           {
              debug_level = DEBUG;
           }
        }
     }
  }
  return ilRc;
}
/* *********************************************************************/
/* The GetCfgEntry() routine                                           */
/* *********************************************************************/
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
                       char **pcpDest,int ipValueType,char *pcpDefVal)
{
  int ilRc = RC_SUCCESS;
  char pclCfgLineBuffer[L_BUFF];
	
  memset(pclCfgLineBuffer,0x00,L_BUFF);
  ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,pclCfgLineBuffer);
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
     dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
     strcpy(pclCfgLineBuffer,pcpDefVal);
  }
  else
  {
     if (strlen(pclCfgLineBuffer) < 1)
     {
        dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
        strcpy(pclCfgLineBuffer,pcpDefVal);
     }
  }
/* 20060803 jim: Prf 8352: brackets removed, in any case do a malloc 
                           to avoid NULL pointer in dbg() and other printf() 
*/
  *pcpDest = malloc(strlen(pclCfgLineBuffer)+1);
  strcpy(*pcpDest,pclCfgLineBuffer);
  dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
  if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
  {
     dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry); 	
  }
  return ilRc;
}
/* ******************************************************************** */
/* The CheckValue() routine                                             */
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
  int ilRc = RC_SUCCESS;

  switch (ipType)
  {
     case CFG_NUM:
        while (*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isdigit(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!",pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     case CFG_ALPHA:
        while (*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isalpha(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        } 
        break;
     case CFG_ALPHANUM:
        while (*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isalnum(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     case CFG_PRINT:
        while (*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isprint(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     default:
        break;
  }
  return ilRc;
}
/* ******************************************************************** */
/* The CheckCfg() routine                                               */
/* ******************************************************************** */
static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  if (*prgCfg.bhs_host1 == 0x00 || *prgCfg.bhs_host2 == 0x00)
  {
     dbg(TRACE,"CheckCfg: missing BHS_HOST-name in <%s.cfg>!",pcgProcessName);
     return RC_FAIL;
  }
  /* MODE */
  if (strcmp(prgCfg.mode,"REAL") != 0 &&
      strcmp(prgCfg.mode,"TEST") != 0)
  {
     dbg(TRACE,"CheckCfg: MODE <%s> unknown!",prgCfg.mode);
     dbg(TRACE,"CheckCfg: Now using default \"REAL\"!");
     *prgCfg.mode = 0x00;
     strcpy(prgCfg.mode,"REAL");
  }
  /* DB_WRITE */
  if ((*prgCfg.db_write != 'N' && *prgCfg.db_write != 'n') &&
      (*prgCfg.db_write != 'Y' && *prgCfg.db_write != 'y'))
  {
     dbg(TRACE,"CheckCfg: DB_WRITE <%c> unknown!",*prgCfg.db_write);
     dbg(TRACE,"CheckCfg: Now using default \"N\"!");
     *prgCfg.db_write = 0x00;
     strcpy(prgCfg.mode,"N");
  }
  /* service to use */
  /* client operating system */
  if (strcmp(prgCfg.ftp_client_os,"WIN") != 0 &&
      strcmp(prgCfg.ftp_client_os,"UNIX")!= 0)
  {
     dbg(TRACE,"CheckCfg: FTP_CLIENT_OS <%s> unknown!",prgCfg.ftp_client_os);
     dbg(TRACE,"CheckCfg: Now using default \"UNIX\"!");
     *prgCfg.ftp_client_os = 0x00;
     strcpy(prgCfg.ftp_client_os,"UNIX");
  }

  if (strcmp(prgCfg.after_ftp,"D") != 0 &&
      strcmp(prgCfg.after_ftp,"R") != 0 &&
      strcmp(prgCfg.after_ftp,"K") != 0) 
  {
     dbg(TRACE,"CheckCfg: AFTER_FTP <%s> invalid!",prgCfg.after_ftp);
     dbg(TRACE,"CheckCfg: Now using default \"D\" (for delete)!");
     *prgCfg.after_ftp = 0x00;
     strcpy(prgCfg.after_ftp,"D");
  }
  return ilRc;
}
/* ******************************************************************** */
/* The FreeCfgMem() routine	  	                                */
/* ******************************************************************** */
static void FreeCfgMem(void)
{

  /* entries from bhsif.cfg file*/
  free(prgCfg.debug_level);
  free(prgCfg.mode);
  free(prgCfg.db_write);
  free(prgCfg.seq_write_queue);
  free(prgCfg.db_filter);
  free(prgCfg.cxx_filter);
  free(prgCfg.term_filter);
  free(prgCfg.term_list);
  free(prgCfg.my_sys_id);
  free(prgCfg.bhs_host1);
  free(prgCfg.bhs_host2);
  free(prgCfg.service_port);
  free(prgCfg.wait_for_ack);
  free(prgCfg.recv_timeout);
  free(prgCfg.max_sends);
  free(prgCfg.recv_log);
  free(prgCfg.send_log);
  free(prgCfg.try_reconnect);
  free(prgCfg.remote_sys_id);
  free(prgCfg.fs_offset);
  free(prgCfg.fs_local_path);
  free(prgCfg.fs_remote_path);
  free(prgCfg.ftp_client_os);
  free(prgCfg.ftp_user);
  free(prgCfg.ftp_pass);
  free(prgCfg.ftp_timeout);
  free(prgCfg.after_ftp);
  free(prgCfg.CAI_ShortLength);
  free(prgCfg.ExtAlc2AddBlank);
  free(prgCfg.EmptyChuteDeletes);
  free(prgCfg.BagAgentDefault);
  free(prgCfg.BagTaskCode);

}
/* ****************************************************************** */
/* The ConnectTCP routine                                             */
/* ****************************************************************** */
static int ConnectTCP (char *pcpHost,int ipTry)
{
  int ilRc = RC_FAIL;	/*Return code*/
  time_t tlNow = 0;

  tlNow = time(NULL);
  if (*pcpHost != 0x00)
  {
     /* try to establish the connection */
     if ((bgSocketOpen==FALSE) && (tlNow > tgNextConnect))
     {
        dbg(DEBUG,"ConnectTCP: Try Nr.%d!",ipTry);
        igSock = tcp_create_socket(SOCK_STREAM, NULL); /* create the socket*/
        if (igSock < 0)
        {
           dbg(TRACE,"ConnectTCP: Create socket failed: %s.",strerror(errno));
           ilRc = RC_FAIL;
        }
        else
        {
           dbg(DEBUG,"ConnectTCP: Create socket <ID=%d> successfull.",igSock);
           alarm(CONNECT_TIMEOUT);
           ilRc = tcp_open_connection(igSock,prgCfg.service_port,pcpHost);
           alarm(0);
           if (ilRc != RC_SUCCESS)
           {
              dbg(TRACE,"ConnectTCP: connect to <%s> failed: <%s> ",pcpHost,strerror(errno));
              shutdown(igSock,2);
              close(igSock);
              bgSocketOpen = FALSE;
              ilRc = RC_FAIL;
              if (ipTry == 2)
              {
                 tlNow = time(NULL);
                 tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
                 dbg(DEBUG,"ConnectTCP: next try in %ld sec.!",(tgNextConnect-tlNow));
              }
           }
           else
           {
              bgSocketOpen = TRUE;
              igMsgCounter = 0;
              ilRc = RC_SUCCESS;
              dbg(TRACE,"ConnectTCP: open connection to <%s> successfull.",pcpHost);
              tlNow = time(NULL);
              tgRecvTimeOut = tlNow + (time_t)(igRecvTimeout - READ_TIMEOUT);
              strcpy(pcgBHS_Host,pcpHost);
           }
        }
     }
  }
  else
  {
     dbg(TRACE,"ConnectTCP: wrong BHS_HOST <%s> entry!",pcpHost);
  }
  return ilRc;
} 
/* ******************************************************************** */
/* Following the poll_q_and_sock function				*/
/* Waits for input on the socket and polls the QCP for messages         */
/* ******************************************************************** */
static int poll_q_and_sock () 
{
  int ilRc;
  int ilRc_Connect = RC_FAIL;
  char pclTmpBuf[1024];
  char pclCurrentTime[32];

  do
  {
     nap(100); /* waiting because of NOT-waiting QUE_GETBIGNW */
     /*---------------------------*/
     /* now looking on ceda-queue */
     /*---------------------------*/
     ilRc = RC_NOT_FOUND;
     if (ilRc == RC_NOT_FOUND)
     {
        while ((ilRc=que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *) &prgItem)) == RC_SUCCESS)
        {
           /* depending on the size of the received item  */
           /* a realloc could be made by the que function */
           /* so do never forget to set event pointer !!! */
           prgEvent = (EVENT *) prgItem->text;

           /* Acknowledge the item */
           ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
           if (ilRc != RC_SUCCESS) 
           {
              /* handle que_ack error */
              HandleQueErr(ilRc);
           }

           switch (prgEvent->command)
           {
              case HSB_STANDBY:
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_STANDBY event!");
                 HandleQueues();
                 break;	
              case HSB_COMING_UP:
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_COMING_UP event!");
                 HandleQueues();
                 break;	
              case HSB_ACTIVE:
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_ACTIVE event!");
                 break;	
              case HSB_ACT_TO_SBY:
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
                 HandleQueues();
                 break;	
              case HSB_DOWN:
                 /* whole system shutdown - do not further use que(), */
                 /* send_message() or timsch() ! */
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_DOWN event!");
                 Terminate(FALSE);
                 break;	
              case HSB_STANDALONE:
                 ctrl_sta = prgEvent->command;
                 dbg(TRACE,"PQS: received HSB_STANDALONE event!");
                 /*ResetDBCounter();*/
                 break;	
              case REMOTE_DB:
                 /* ctrl_sta is checked inside */
                 /*HandleRemoteDB(prgEvent);*/
                 break;
              case SHUTDOWN:
                 /* process shutdown - maybe from uutil */
                 dbg(TRACE,"PQS: received SHUTDOWN event!");
                 CloseTCP();
                 Terminate(FALSE);
                 break;
              case RESET:
                 dbg(TRACE,"PQS: received RESET event!");
                 ilRc = Reset();
                 if (ilRc == RC_FAIL)
                 {
                    Terminate(FALSE);
                 }
                 break;
              case EVENT_DATA:
                 if ((ctrl_sta == HSB_STANDALONE) ||
                     (ctrl_sta == HSB_ACTIVE) ||
                     (ctrl_sta == HSB_ACT_TO_SBY))
                 {
                    if (bgSocketOpen == TRUE ||
                        strcmp(prgCfg.mode,"TEST") == 0)
                    {
                       ilRc = HandleInternalData();
                       if (ilRc!=RC_SUCCESS)
                       {
                          dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
                          HandleErr(ilRc);
                       }
                     }
                     else
                     {
                        dbg(TRACE,"PQS: No BHS-connection! Ignoring event!");
                     }
                  }
                  else
                  {
                     dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
                     DebugPrintItem(TRACE,prgItem);
                     DebugPrintEvent(TRACE,prgEvent);
                  }/* end of if */
                  break;
               case TRACE_ON:
                  dbg_handle_debug(prgEvent->command);
                  break;
               case TRACE_OFF:
                  dbg_handle_debug(prgEvent->command);
                  break;
               case 1000:
                  if (prgCfg.ilReceiveTransitInfo == FALSE)
                     strcpy(pclTmpBuf,"FB01@LH901   @@@200809161500            200809160600200809161500");
                  else
                     strcpy(pclTmpBuf,"FA01@ALH3315  @@@200812051550            200812051250200812051550");
                  TimeToStr(pclCurrentTime,time(NULL),1);
                  strcat(pclTmpBuf,pclCurrentTime);
                  ilRc = Process_CAI_Telegramm(pclTmpBuf);
                  break;
               case 1001:
                  if (prgCfg.ilReceiveTransitInfo == FALSE)
                     strcpy(pclTmpBuf,"FB01@LH901   @@@200809161500            200809160600200809161500");
                  else
                     strcpy(pclTmpBuf,"NA02@ALH3315  @@@200812051550            200812051250200812051550");
                  TimeToStr(pclCurrentTime,time(NULL),1);
                  strcat(pclTmpBuf,pclCurrentTime);
                  ilRc = Process_CAI_Telegramm(pclTmpBuf);
                  break;
               case 1002:
                  if (prgCfg.ilReceiveTransitInfo == FALSE)
                     strcpy(pclTmpBuf,"FB01@LH901   @@@200809161500            200809160600200809161500");
                  else
                     strcpy(pclTmpBuf,"LA01@ALH3315  MUC200812051550            200812051250200812051550");
                  TimeToStr(pclCurrentTime,time(NULL),1);
                  strcat(pclTmpBuf,pclCurrentTime);
                  ilRc = Process_CAI_Telegramm(pclTmpBuf);
                  break;
               case 1005:
                  if (prgCfg.ilReceiveTransitInfo == FALSE)
                     strcpy(pclTmpBuf,"FB01@LH901   @@@200809161500            200809160630200809161530");
                  else
                     strcpy(pclTmpBuf,"FB01@ALH901   @@@200809161500            200809160630200809161530");
                  TimeToStr(pclCurrentTime,time(NULL),1);
                  strcat(pclTmpBuf,pclCurrentTime);
                  ilRc = Process_CST_Telegramm(pclTmpBuf);
                  break;
               default:
                  dbg(TRACE,"MAIN: unknown event");
                  DebugPrintItem(TRACE,prgItem);
                  DebugPrintEvent(TRACE,prgEvent);
                  break;
           } /* end switch */
        }/* end while */ 
        ilRc = RC_FAIL; /* proceed */
     }

     if (bgSocketOpen==TRUE)
     {
        /*----------------------------------*/
        /* now looking for stored messages  */
        /*----------------------------------*/
        if (prgRecvMsgList->Count > 0)
        {
           dbg(DEBUG,"PQS: Stored messages (%d)",prgRecvMsgList->Count);
           HandleStoredData(prgRecvMsgList->Count);	
        }
        /*-----------------------*/
        /* now looking on socket */
        /*-----------------------*/
        /* We will only look on the socket if there are no more stored */
        /* messages. If there are messages on the socket, these will be stored */
        /* during the "WaitForAck"-function.*/
        if (prgRecvMsgList->Count < 1)
        {
           ilRc = Receive_data(igSock,READ_TIMEOUT,"MAIN");
        }
     }
     else
     {
        ilRc = RC_FAIL;
     }

     /* now trying to reconnect if there was a failure */
     if (ilRc == RC_FAIL)
     {
        CloseTCP(); /* for cleaning up the connections */

        /* toggeling between the two BHS-Servers to find the Master-Server */
        if ((ilRc_Connect = ConnectTCP(prgCfg.bhs_host1,1)) != RC_SUCCESS)
        {
           if ((ilRc_Connect = ConnectTCP(prgCfg.bhs_host2,2)) == RC_SUCCESS)
           {
              strcpy(pcgBHS_Host,prgCfg.bhs_host2);
           }
           else
           {
              strcpy(pcgBHS_Host,prgCfg.bhs_host1);
           }
        }
        else
        {
           strcpy(pcgBHS_Host,prgCfg.bhs_host1);
        }

        if ((ilRc_Connect==RC_SUCCESS))
        {
           ilRc_Connect = CreateFlightTables();
        }
     }
  } while (ilRc!=RC_SUCCESS);
  return ilRc;
} 
/* ***************************************************************** */
/* The Receive_data routine                                          */
/* Rec data from BHS into global buffer (and into a file )           */
/* ***************************************************************** */
static int Receive_data(int ipSock,int ipTimeOut,char *pcpCalledBy)
{
  int ilRc;
  int ilAlive = FALSE;
  int ilEomByte = 0;
  int ilNrOfBytes = 0;
  int ilBytesToRead = 0;
  int ilBytesAlreadyRead=0;
  char pclTmpBuffer[10];
  time_t tlNow = 0;

  memset(prgRecvBuffer,0x00,sizeof(MAX_TELEGRAM_LEN));
  do
  {
     ilNrOfBytes = 0;
     bgAlarm = FALSE;
     memset(pclTmpBuffer,0x00,sizeof(pclTmpBuffer));

     errno = 0;
     alarm(ipTimeOut);
     ilNrOfBytes=read(ipSock,&pclTmpBuffer[0],1);
     alarm(0);

     /* <0 means failure at read if no alarm appeared, =0 means connection lost*/
     if (ilNrOfBytes <= 0 && bgAlarm == FALSE)
     {
        ilRc = RC_FAIL;
        dbg(TRACE,"Receive_data: OS-ERROR read(%d) on socket <%d>",ilNrOfBytes,ipSock);
        dbg(TRACE,"Receive_data: ERRNO (%d)=(%s)",errno,strerror(errno));
     }
     /* <0 means no data read inside READ_TIMEOUT time */
     if (ilNrOfBytes < 0 && bgAlarm == TRUE)
        ilRc = RC_NOT_FOUND;
     /* if the read byte is a start of message sign */
     if (ilNrOfBytes==1 && pclTmpBuffer[0]==SOM)
        ilRc = RC_SUCCESS;
     /* if the read byte is NOT a start of message sign */
     /* dummy-ilRc for not leaving the loop */
     if (ilNrOfBytes==1 && pclTmpBuffer[0]!=SOM)
        ilRc = 1618;
  } while(ilRc!=RC_SUCCESS && ilRc!=RC_NOT_FOUND && ilRc!=RC_FAIL);
	
  tlNow = time(0);
  if (ilRc == RC_SUCCESS)
  {
     tgRecvTimeOut = tlNow + (time_t)(igRecvTimeout - READ_TIMEOUT);
  }	
  if (tlNow > tgRecvTimeOut)
  {
     ilRc = RC_FAIL;
     dbg(TRACE,"Receive_data: RECV_TIMEOUT <%d> sec. reached! Disconnecting!",igRecvTimeout);
  }
	
  /*----------------------------------------*/
  /* reading the header of BHS-telegram */
  /*----------------------------------------*/
  if (ilRc == RC_SUCCESS)
  {
     dbg(TRACE,"");
     dbg(TRACE,"--- START HandleExternalData ---");
     /* now copy SOM sign to global receive buffer */
     strncpy(prgRecvBuffer,pclTmpBuffer,1);
     ilNrOfBytes = 0;
     ilBytesAlreadyRead = 1;
     ilBytesToRead = igHeadLen-ilBytesAlreadyRead;
     do
     {
        alarm(ipTimeOut);
        ilNrOfBytes=read(ipSock,(prgRecvBuffer+ilBytesAlreadyRead),ilBytesToRead);
        alarm(0);
        if (ilNrOfBytes <= 0)
        {
           if (ilNrOfBytes == 0)
              ilRc = RC_NOT_FOUND;
           else
           {
              if (bgAlarm == FALSE)
              {
                 ilRc = RC_FAIL;
                 dbg(TRACE,"Receive_data: error (%d) (%s)!",
                 errno,strerror(errno));
              }
              else
              {
                 ilRc = RC_NOT_FOUND;
                 bgAlarm = FALSE;
              }
           }
        }
        else
        { 
           if (strncmp((char*)&prgRecvBuffer[1],ALIVE_ID,ALIVE_LEN) == 0)
           {
              ilAlive = TRUE;
              ilRc = RC_SUCCESS;
           }
           if (ilAlive == FALSE)
           {
              if (ilNrOfBytes != igHeadLen-1)
                 dbg(DEBUG,"Receive_data: read <%d> more header-bytes.",ilBytesToRead);
              ilBytesToRead -= ilNrOfBytes;
              ilBytesAlreadyRead += ilNrOfBytes;
              ilRc = RC_SUCCESS;
           }
        }
     } while (ilRc==RC_SUCCESS && ilBytesToRead>0 && ilAlive==FALSE);
		
     /*--------------------------------------*/
     /* reading data of the BHS-telegram */
     /*--------------------------------------*/
     if (ilRc == RC_SUCCESS && ilAlive == FALSE)
     {
        ilNrOfBytes = 0;
        ilBytesToRead = 1;
        ilBytesAlreadyRead = 0;
        do
        {
           alarm(ipTimeOut);
           ilNrOfBytes = read(ipSock,(&prgRecvBuffer[igHeadLen]+ilBytesAlreadyRead),ilBytesToRead);
           alarm(0);
           if (ilNrOfBytes <= 0)
           {
              if (ilNrOfBytes == 0)
              {
                 ilRc = RC_NOT_FOUND;
              }
              else
              {
                 if (bgAlarm == FALSE) 
                 {
                    ilRc = RC_FAIL;
                    dbg(TRACE,"read: error (%d) (%s)!",
                    errno,strerror(errno));
                 }
                 else
                 {
                    ilRc = RC_NOT_FOUND;
                    bgAlarm = FALSE;
                 }
              }
           }
           else
           {
              ilBytesAlreadyRead += ilNrOfBytes;
              ilRc = RC_SUCCESS;
           }
        } while((prgRecvBuffer[igHeadLen+ilBytesAlreadyRead-1] != EOT)  &&
                (ilBytesAlreadyRead < MAX_TELEGRAM_LEN-1));

        /* Setting end-mark of the telegram */ 
        ilEomByte = igHeadLen+ilBytesAlreadyRead-1;
        prgRecvBuffer[ilEomByte] = 0x00;
        snapit((char*)prgRecvBuffer,ilEomByte+1,outp);

        /* Now cleaning message from unallowed bytes (0x00 and '_' - bytes) */
        /* until one byte before the end-NULL(0x00)-byte */
        CleanMessage(prgRecvBuffer,ilEomByte);
     }
  }
 	
	 
  if (ilRc==RC_SUCCESS && ilAlive == FALSE)
  {
     dbg(DEBUG,"Receive_data: <%s>. Bytes <%d+1>. Socket <%d>.",pcpCalledBy,ilEomByte,ipSock);
     if (pgReceiveLogFile)
     {
        fwrite(prgRecvBuffer,sizeof(char),ilEomByte,pgReceiveLogFile);
        fwrite("\n",sizeof(char),1,pgReceiveLogFile);
        fflush(pgReceiveLogFile);
     }
     ilRc = RC_SUCCESS;
  }
  return ilRc;
}
/* ***************************************************************** */
/* The CloseTCP routine                                              */
/* ***************************************************************** */
static void CloseTCP()
{
  time_t tlNow = 0;

  *pcgBHS_Host = 0x00;

  if (igSock != 0)
  {
     dbg(DEBUG,"CloseTCP: closing TCP/IP connection!");
     shutdown(igSock,2);
     close(igSock);
     tlNow = time(NULL);
     tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
     dbg(TRACE,"CloseTCP: connection closed! Wait (%s) sec. before reconnect",prgCfg.try_reconnect);
  }
  igSock = 0;
  bgSocketOpen = FALSE;

  /* reseting transmission info for CAI packages */
  if (strcmp(prgCfg.mode,"REAL") == 0)
  {
     cgNextAllowedIndi1 = 'F'; /* initial value for the indicator inside a CAI telegram */
     cgNextAllowedIndi2 = 'S'; /* initial value for the indicator inside a CAI telegram */
  }
  igFirstRc = RC_SUCCESS;
}
/* ***************************************************************** */
/* The MakeEnd routine                                               */
/* ***************************************************************** */
static void MakeEnd()
{	
  memset((char*)&prgEnd,0x00,igTailLen);
  prgEnd.tail[0] = 0x55; /* 'U' like aggreed with Mannesmann */
  prgEnd.tail[1] = 0x55; /* 'U' like aggreed with Mannesmann */
  prgEnd.tail[2] = 0xD; 
  prgEnd.tail[3] = EOT; /* EOT = End Of Telegram define (char)0x03 */
  prgEnd.tail[4] = 0x00;
}
/* ******************************************************************** */
/* The HandleStoredData routine                                         */
/* ******************************************************************** */
static void HandleStoredData(int ipTimesToDo)
{
  int ilRc;
  int ilRecvCmd=0;
  int ilCnt=0;
  LPLISTELEMENT prlEle = NULL;
  MSG_RECV* prlMsg = NULL;
			
  while ((prlEle=ListFindFirst(prgRecvMsgList)) && (ilCnt<ipTimesToDo))
  {
     prlMsg = (MSG_RECV*)prlEle->Data;
     dbg(TRACE,"HandleStoredData: (%d) Nr.<%s> <%x>",prlMsg->ilMsgNr,prlMsg->msg,prlMsg->msg);
     if (ilRc=CheckHeader(prlMsg->msg,&ilRecvCmd) == RC_SUCCESS)
     {
        if (ilRc = ProcessData(prlMsg->msg,ilRecvCmd,FALSE)!=RC_SUCCESS)
        {
           dbg(TRACE,"HandleStoredData: ProcessData() failed!");
        }
     }
     free(prlMsg->msg);
     ListDeleteFirst(prgRecvMsgList);		
     ilCnt++;
  }
}
/* **************************************************************** */
/* The snapit routine                                               */
/* snaps data if the debug-level is set to DEBUG                    */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
  if (debug_level==DEBUG)
  {
     snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
  }
}
/*********************************************************************
Function :  CleanMessage(char *pcpMsg,int ipMsgLen)
Paramter :  IN:pcpMsg = Message received directly after the socket read.
            IN:ipMsgLen=length of the message
Returnvalue:none
Description:Replaces unallowed bytes from the original telegram
            (especially '\0' and '_') with "allowed" blanks
*********************************************************************/
static void CleanMessage(char *pcpMsg,int ipMsgLen)
{
  int ilCnt = 0;

  while(ilCnt < ipMsgLen)
  {
     if (pcpMsg[ilCnt]==0x00)
     {
        pcpMsg[ilCnt]=' ';
     }
     ilCnt++;
  }
}
/* **************************************************************** */
/* The HandleExternalData routine                                   */
/* Checks the data wich have been received from the ADS-system      */
/* **************************************************************** */
static int HandleExternalData(char *pcpData)
{	
  int ilRc = RC_SUCCESS;
  int ilRecvCmd = 0;

  if (bgSocketOpen == TRUE)
  {
     if ((ilRc = CheckHeader(pcpData,&ilRecvCmd)) == RC_SUCCESS)
     {
        if (ilRecvCmd == MSG_ALV)
        {
           dbg(DEBUG,"HandleExternalData: ALIVE-telegram (%d) received!",MSG_ALV);
           Send_New_Alive();
           ilRc = RC_SUCCESS;
        }
        else
        {
           ilRc = ProcessData(pcpData,ilRecvCmd,TRUE);
        }
     }
  }
  dbg(TRACE,"--- END   HandleExternalData ---");
  return ilRc;
}
/* **************************************************************** */
/* The CheckHeader routine                                          */
/* Checks the structure and contents of the received telegram-header*/
/* and sets the received command to an integer value                */
/* **************************************************************** */
static int CheckHeader(char *pcpData,int *ipRecvCmd)
{
  int ilRc = RC_SUCCESS;
  char pclCmd[5];
  char pclSender[5];
  char pclReceiver[5];
  HEAD pclRecvHead;

  if (pcpData[0] == SOM)
  {
     memset((char*)&pclRecvHead,0x00,igHeadLen);
     memcpy((char*)&pclRecvHead,pcpData,igHeadLen);
     memset(pclCmd,0x00,sizeof(pclCmd));
     strncpy(pclCmd,pclRecvHead.t_type,CMD_LEN);
     if ((ilRc = CheckValue("Telegram type",pclCmd,CFG_NUM)) == RC_SUCCESS)
     {
        *ipRecvCmd = atoi(pclCmd);
        dbg(TRACE,"CheckHeader: Command <%d> received!",*ipRecvCmd);
        memset(pclSender,0x00,5);
        strncpy(pclSender,pclRecvHead.sender,2);
        if (strncmp(pclSender,prgCfg.remote_sys_id,2) == 0)
        {
           memset(pclReceiver,0x00,sizeof(pclReceiver));
           strncpy(pclReceiver,pclRecvHead.receiver,2);
           if (strncmp(pclReceiver,prgCfg.my_sys_id,2) == 0)
           {
              if (*ipRecvCmd != MSG_ACK &&
                  *ipRecvCmd != MSG_ALV &&
                  *ipRecvCmd != MSG_CAI &&
                  *ipRecvCmd != MSG_STX &&
                  *ipRecvCmd != MSG_ETX &&
                  *ipRecvCmd != MSG_FTT && /* 20060803 JIM: PRF 8352: request for flight schedule table */
                  *ipRecvCmd != MSG_CST)
              {
                 dbg(TRACE,"CheckHeader: unknown command <%d> received!",*ipRecvCmd);
                 *ipRecvCmd = 0;
                 ilRc = RC_FAIL;
              }
           }
           else
           {
              dbg(TRACE,"CheckHeader: wrong MY_SYS_ID <%s> received!",pclReceiver);
              ilRc = RC_FAIL;
           }
        }
        else
        {
           dbg(TRACE,"CheckHeader: wrong REMOTE_SYS_ID <%s> received!",pclSender);
           ilRc = RC_FAIL;
        }
     }
     else
     {
        dbg(TRACE,"CheckHeader: invalid telegram type <%s> received!",pclCmd);
        ilRc = RC_FAIL;
     }
  }
  else
  {
     dbg(TRACE,"CheckHeader: wrong SOM-sign received!");
     ilRc = RC_FAIL;
  }
  return ilRc;
}
/* *****************************************************************/
/* The ProcessData routine                                         */
/* Checks the data of the received telgram                         */
/* *****************************************************************/
static int ProcessData(char *pcpData,int ipCmd,BOOL bpAck)
{
  int ilRc = RC_SUCCESS;
  int ilDataLen = 0;
  char pclRecvData[MAX_TELEGRAM_LEN];

  memset(pclRecvData,0x00,MAX_TELEGRAM_LEN);
  ilDataLen=(int)strlen((char*)&pcpData[igHeadLen])+1;
  strncpy(pclRecvData,&pcpData[igHeadLen],ilDataLen);
  switch (ipCmd)
  {
     case MSG_STX:
        if (bpAck == TRUE)
        {
           Send_Ack_Msg();	
           dbg(TRACE,"ProcessData: ****** STX daily chute info ******");
        }
        break;
     case MSG_ETX:
        if (bpAck == TRUE)
        {
           Send_Ack_Msg();	
           dbg(TRACE,"ProcessData: ****** ETX daily chute info ******");
        }
        if (igModID_Dysif > 0)
        {
           ilRc=SendEvent("SSCI","","","",NULL,0,igModID_Dysif,igModID_Dysif,PRIORITY_3,"","","");
           if (ilRc != RC_SUCCESS)
           { 
              dbg(TRACE,"ProcessData: sending of SSCI-message to DYSIF failed!");
           }
        }
        break;
     case MSG_CAI:
        if (bpAck == TRUE)
        {
           Send_Ack_Msg();	
        }
        if ((ilRc=Process_CAI_Telegramm(pclRecvData)) != RC_SUCCESS)
        { 
           dbg(TRACE,"ProcessData: processing CAI-telegram failed!");
        }
        break;
     case MSG_CST:
        if (bpAck == TRUE)
        {
           Send_Ack_Msg();	
        }
        if ((ilRc=Process_CST_Telegramm(pclRecvData)) != RC_SUCCESS)
        { 
           dbg(TRACE,"ProcessData: processing CST-telegram failed!");
        }
        break;
     case MSG_FTT: /* 20060803 JIM: PRF 8352: request for flight schedule table */
        if (bpAck == TRUE)
        {
           Send_Ack_Msg();	
        }
        dbg(TRACE,"ProcessData: flight table via TCP requested....");
        if ((ilRc=CreateFlightTables()) != RC_SUCCESS)
        { 
           dbg(TRACE,"ProcessData: sending requested flight table via TCP failed!");
        }
        break;
     case MSG_ACK:
        dbg(TRACE,"ProcessData: ACK received!");
        break;
     default:
        dbg(TRACE,"ProcessData: unknown command <%d> received!",ipCmd);
        break;
  }
  return ilRc;
}
/* ******************************************************************** */
/* The Send_New_Alive()                                                 */
/* ******************************************************************** */
static void Send_New_Alive()
{
  int ilDataLen = 0;
  int ilRc = RC_SUCCESS;
  ALIVE rlAliveMsg;
	
  memset(&rlAliveMsg,0x00,sizeof(ALIVE));
  /* HEADER */
  *(rlAliveMsg.head.start) = SOM;
  *(rlAliveMsg.head.cycle_no) = '0';
  sprintf(rlAliveMsg.head.t_type,"%02d",MSG_ALV);
  strncpy(rlAliveMsg.head.sender,prgCfg.my_sys_id,2);
  strncpy(rlAliveMsg.head.receiver,prgCfg.remote_sys_id,2);
  strncat((char*)&rlAliveMsg.head.order_no,"  ",2);
  /* TAIL */
  GetMsgTimeStamp(rlAliveMsg.tail.time);
  strncpy(rlAliveMsg.tail.tail,prgEnd.tail,4);
  ilDataLen = strlen((char*)&rlAliveMsg);

  if ((ilRc = Send_data(igSock,(char*)&rlAliveMsg,ilDataLen))==RC_FAIL)
  {
     CloseTCP();
  }
}
/* ******************************************************************** */
/* The Send_Ack_Msg()                                                   */
/* ******************************************************************** */
static void Send_Ack_Msg()
{
  int ilDataLen = 0;
  int ilRc = RC_SUCCESS;
  HEAD *pclRecvHead;
  ACK_EXT rlAckMsg;
  memset((char*)&rlAckMsg,0x00,igAckLen);

  /* HEADER */
  *(rlAckMsg.head.start) = SOM;
  *(rlAckMsg.head.cycle_no) = '0';
  sprintf(rlAckMsg.head.t_type,"%02d",MSG_ACK);
  strncpy(rlAckMsg.head.sender,prgCfg.my_sys_id,2);
  strncpy(rlAckMsg.head.receiver,prgCfg.remote_sys_id,2);
  pclRecvHead = (HEAD*)prgRecvBuffer;
  strncpy(rlAckMsg.head.order_no,pclRecvHead->order_no,2);
  /* TAIL */
  GetMsgTimeStamp(rlAckMsg.tail.time);
  strncpy(rlAckMsg.tail.tail,prgEnd.tail,4);
  ilDataLen = strlen((char*)&rlAckMsg);

  if ((ilRc = Send_data(igSock,(char*)&rlAckMsg,ilDataLen))==RC_FAIL)
  {
     CloseTCP();
  }
}
/* ******************************************************************** */
/* The Send_FTF_Msg()                                                   */
/* ******************************************************************** */
static int Send_FTF_Msg(char *pcpFileName)
{
  int ilDataLen = 0;
  int ilLen = 0;
  int ilRc = RC_SUCCESS;
  char pclFileName[16];
  char pclSendBuffer[XL_BUFF];

  memset(pclFileName,0x00,16);
  memset(pclSendBuffer,0x00,XL_BUFF);
  ilLen = strlen(pcpFileName);
  pcpFileName[ilLen-4] = 0x00;
  strcat(pcpFileName,".TBL");
  strncpy(pclFileName,pcpFileName,12);

  if ((ilRc = MakeSendBuffer(TRUE,MSG_FTF,pclFileName,pclSendBuffer)) == RC_SUCCESS)
  {
     ilDataLen = (int)strlen(pclSendBuffer);
     dbg(DEBUG,"Send_FTF_Msg: <%s>",pclSendBuffer);
     if ((ilRc=Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
     {
        CloseTCP();
     }
     else
     {
        ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
     }
  }
  return ilRc;
}
/* ***************************************************************** */
/* The GetMsgNumber routine                                          */
/* ***************************************************************** */
static void GetMsgNumber(char *pcpMsgNumber) 
{
  *pcpMsgNumber  = '\0';
  if (igMsgCounter > 99)
  {
     /*dbg(DEBUG,"GetMsgNumber: reseting MSG-telegram number to zero.");*/
     igMsgCounter = 1;
  }
  sprintf(pcpMsgNumber ,"%02d",igMsgCounter);
  /*dbg(DEBUG,"GetMsgNumber: <%d> => (%s)",igMsgCounter,pcpMsgNumber);*/
  igMsgCounter++;	
}
/* ***************************************************************** */
/* The GetMsgTimeStamp routine                                       */
/* ***************************************************************** */
static void GetMsgTimeStamp(char *pcpTimeStr) 
{
  char pclNow[20];

  *pcpTimeStr = 0x00;
  TimeToStr(pclNow,0,4);
  strncpy(pcpTimeStr,pclNow,9);
}
/* ******************************************************************** */
/* The FormatDate() routine                                             */
/* ******************************************************************** */
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate)
{
  int ilRc = RC_SUCCESS;
  int ilCfgChar = 0;
  int ilRightChar = 0;
  int ilDateChar = 0;
  char pclOldDate[20];
  char pclValidLetters[] = "YMDHIS";
  char pclRightFormat[20];
  char *pclTmpDate = NULL;
	
  TrimRight(pcpOldDate);
  memset(pclOldDate,0x00,sizeof(pclOldDate));
  memset(pclRightFormat,0x00,sizeof(pclRightFormat));
  *pcpNewDate = 0x00;
	
  if (strlen(pcpOldDate) > 14)
  {
     return RC_FAIL;	
  }
  else
  {
     strcpy(pclOldDate,pcpOldDate);
     TrimRight(pclOldDate);
  }

  /* adding '0' until the length of the date is 14byte */ 
  if (strlen(pclOldDate) < 14)
  {
     while(strlen(pclOldDate) < 14)
     {
        pclOldDate[strlen(pclOldDate)] = '0';
        pclOldDate[strlen(pclOldDate)+1] = 0x00;
     }
  }
	
  /* removing all unallowed letters from pcpFormat-string */
  ilCfgChar=0;
  while (ilCfgChar < (int)strlen(pcpFormat))
  {
     if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
     {
        pclRightFormat[ilRightChar] = pcpFormat[ilCfgChar];
        ilRightChar++;
     }
     ilCfgChar++;
  }

  /* now formatting CEDA-time format from pclOldDate to right format */	
  if ((pclTmpDate = GetPartOfTimeStamp(pclOldDate,pclRightFormat)) != NULL)
  {
     /* now changing the layout like it is in the cfg-file */	
     ilCfgChar = 0;
     ilRightChar = 0;
     ilDateChar = 0;
     while (ilCfgChar < (int)strlen(pcpFormat))
     {
        if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
        {
           (pcpNewDate)[ilDateChar] = pclTmpDate[ilRightChar];
           ilRightChar++;
           ilDateChar++;
        }
        else
        {
           (pcpNewDate)[ilDateChar] = pcpFormat[ilCfgChar];
           ilDateChar++;
        }
        ilCfgChar++;
     }
     (pcpNewDate)[strlen(pcpFormat)] = 0x00;
  }
  else
  {
     ilRc = RC_FAIL;
  }
  /*dbg(DEBUG,"FormatDate: Old=<%s> New=<%s>",pclOldDate,pcpNewDate);*/
  return ilRc;	
}
/* ******************************************************************** */
/* The TrimRight() routine                                              */
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
  int i = 0;

  for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
  pcpBuffer[i] = '\0';    
}
/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends (header & data & tail) to the BHS-system                   */
/* **************************************************************** */
static int Send_data(int ipSock,char *pcpData,int ipLen)
{
  int ilRc=RC_SUCCESS;
  int ilBytes = 0;
		
  /* ipSock = opened socket */
  /* ilBytes = number of bytes that have to be transmitted. */
  ilBytes = ipLen;
  if (ipSock != 0)
  {
     errno = 0;
     alarm(WRITE_TIMEOUT);
     ilRc = write(ipSock,pcpData,ilBytes);/*unix*/
     alarm(0);

     if (ilRc == -1)
     {
        if (bgAlarm == FALSE)
        {
           dbg(TRACE,"Send_data: Write failed: Socket <%d>! <%d>=<%s>",ipSock,errno,strerror(errno));
           ilRc = RC_FAIL;
        }
        else
        {
           bgAlarm = FALSE;
           ilRc = RC_FAIL;
        }
     }
     else
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to socket Nr.<%d>.",ilRc,ipSock); 
        snapit((char *)pcpData,ilBytes+1,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcpData,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
     }
  }
  else
  {
     if (strcmp(prgCfg.mode,"TEST") == 0)
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to dummy-socket.",ilBytes); 
        snapit((char *)pcpData,ilBytes+1,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcpData,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
     }
     else
     {
        dbg(TRACE,"Send_data: No connection to <%s>! Can't send!",prgCfg.remote_sys_id);
        ilRc = RC_FAIL;
     }
  }
  return ilRc;
} 
/* ******************************************************************** */
/*                                                                      */
/* ******************************************************************** */
static int WaitForAck(int ipSock,char *pcpMsg,int ipLen)
{
  int ilRc;
  int ilCnt = 1; /* send-counter set to 1 because telegram has already been send once */
  int ilRecvCmd = 0;

  dbg(DEBUG,"WaitForAck: TRUE! Socket <%d>",ipSock);
  do
  {
     if ((ilRc=Receive_data(ipSock,igWaitForAck,"WFA")) == RC_SUCCESS)
     {
        if ((ilRc=CheckHeader(prgRecvBuffer,&ilRecvCmd)) == RC_SUCCESS)
        {
           if (ilRecvCmd!=MSG_ACK && ilRecvCmd!=MSG_ALV)
           {
              StoreRecvMessage(prgRecvBuffer);
           }
           else
           {
              if (ilRecvCmd==MSG_ALV)
              {
                 dbg(DEBUG,"WaitForAck: received ALIVE-telegram!");
                 Send_New_Alive();
              }
              else /* received message was an ACK */
              {
                 dbg(DEBUG,"WaitForAck: FALSE! Socket <%d>",ipSock);
                 bgWaitForAck = FALSE;
                 ilRc = RC_SUCCESS;
              }
           }
        }
     }
     else
     {
        if (ilRc == RC_NOT_FOUND)
        {
           dbg(TRACE,"WaitForAck: last message wasn't acked!");
           bgWaitForAck = TRUE;
        }
        else
        {
           dbg(TRACE,"WaitForAck: Receive_data() returns <%d>",ilRc);
           bgWaitForAck = FALSE;
        }
     }

     /* resending telegram if no MSG_ACK was received */
     if (bgWaitForAck==TRUE && ilCnt<igMaxSends)
     {
        if ((ilRc=Send_data(igSock,pcpMsg,ipLen))==RC_FAIL)
        {
           dbg(TRACE,"WaitForAck: resending with Send_data() returns <%d>",ilRc);
           ilRc = RC_FAIL;
           bgWaitForAck = FALSE;
           CloseTCP();
        }
        else /* increasing send-counter */
        {
           ilCnt++;
           ilRc = RC_FAIL;
           dbg(DEBUG,"WaitForAck: <%d.> send of max.<%d> sends done!",ilCnt,igMaxSends);
        }
     }
  } while(bgWaitForAck==TRUE && ilCnt<igMaxSends);
  /* setting to FALSE, so that one unacked messages doesn't disturb operation */
  bgWaitForAck = FALSE;
  if (ilRc == RC_FAIL)
  {
     if (strcmp(prgCfg.mode,"TEST") == 0)
        return RC_SUCCESS;
     else
        return RC_FAIL;
  }
  else
  {
     return RC_SUCCESS;
  }
}
/* ****************************************************************** */
/*                                                                    */
/*                                                                    */
/* ****************************************************************** */
static void StoreRecvMessage(char *pcpMsg)
{
  int ilLen = 0;
  int ilDataLen = 0;
  MSG_RECV rlMsg;
  char pclMsgNo[16];

  Send_Ack_Msg();

  memset(pclMsgNo,0x00,sizeof(pclMsgNo));
  strncpy(pclMsgNo,&pcpMsg[72],8);

  memset(&rlMsg,0x00,sizeof(MSG_RECV));
  ilDataLen = strlen(pcpMsg);
  ilLen = ilDataLen+5; /* +5 just for saftey */
  if ((rlMsg.msg=(char*)malloc(ilLen)) != NULL)
  {
     igCnt++;
     rlMsg.ilMsgNr = igCnt;
     memset(rlMsg.msg,0x00,ilLen);
     memcpy(rlMsg.msg,pcpMsg,ilDataLen);

     /* appending message to message list*/
     if (ListAppend(prgRecvMsgList,&rlMsg) == NULL)
     {
        dbg(TRACE,"StoreRecvMessage: ListAppend failed!");
        Terminate(FALSE);
     }
     else
     {
        dbg(TRACE,"StoreRecvMessage: (%d) Nr.<%s> <%x>",rlMsg.ilMsgNr,pclMsgNo,rlMsg.msg);
     }
  }
  else
  {
     dbg(TRACE,"StoreRecvMessage: No memory for storeing availiable!");
  }
}
/* ****************************************************************** */
/* The PrepareData routine                                            */
/* prepares the data after the command for sending                    */
/* ****************************************************************** */
static int PrepareData(int ipCmd,char *pcpSelection,time_t tpDayToSend)
{
  int ilCnt = 0;
  int ilType = 0;		
  int ilDataLen = 0;	
  int ilRc = RC_SUCCESS;
  char pclDBSelection[L_BUFF];
  char *pclDBFields = NULL;
  time_t tlCurrentDay = time(NULL);
  char pclLocalStamp[20];
  char pclLocalFile[S_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclSendBuffer[XL_BUFF];
  char pclTmpSqlAnswer[XL_BUFF];
  char pclData[DATABLK_SIZE];
  short slFkt = 0;
  short slCursor = 0;
	
  switch(ipCmd)
  {
     case SFS:
        /*-----------------------*/
        /* setting the selection */
        /*-----------------------*/
        if (tpDayToSend == 0)
        {
           tpDayToSend = tlCurrentDay + (time_t)(SECONDS_PER_DAY * atoi(prgCfg.fs_offset));
        }
        TimeToStr(pclLocalStamp,tpDayToSend,1);
        pclLocalStamp[8] = 0x00;
        /*dbg(DEBUG,"PrepareData: Date <%s>",pclLocalStamp);*/
        memset(pclDBSelection,0x00,L_BUFF);
        /* should HomePort(HOPO) database field be used or not ? */
        if (bgUseHopo == TRUE)
        {
           sprintf(pclDBSelection,"WHERE HOPO = '%s' AND STOD LIKE '%s",
                   pcgHomeAp,pclLocalStamp);
           /* looks funny, but  has to be that way for CEDA-NT and CEDA-SOLARIS*/
           strcat(pclDBSelection,"%' ");
           strcat(pclDBSelection,prgCfg.db_filter);
        }
        else
        {
           sprintf(pclDBSelection,"WHERE STOD LIKE '%s",pclLocalStamp);
           /* looks funny, but  has to be that way for CEDA-NT and CEDA-SOLARIS*/
           strcat(pclDBSelection,"%' ");
           strcat(pclDBSelection,prgCfg.db_filter);
        }
        dbg(DEBUG,"PrepareData: (SFS) <%s>",pclDBSelection);
        memset(pclLocalFile,0x00,S_BUFF);
        sprintf(pclLocalFile,"%s.TBL",pclLocalStamp);

        sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pcgValid_SFS_Fields,pcgTabEnd,pclDBSelection);
        slFkt = START; 
        slCursor = 0;
        memset(pclTmpSqlAnswer,0x00,XL_BUFF);
        memset(pclData,0x00,DATABLK_SIZE);
        while ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
        {
           ilCnt++;
           BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SFS_Fields,0,",");
           strcat(pclTmpSqlAnswer,"\n");
           strcat(pclData,pclTmpSqlAnswer);
           slFkt = NEXT;
        }
        close_my_cursor(&slCursor);
        /* delete the last "\n" for not confusing the GetNoOfElements()-function */
        pclData[strlen(pclData)-1] = 0x00;
        if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
        {
           memset(pclSendBuffer,0x00,XL_BUFF);
           if ((ilRc = ProcessFlights(pclData,SFS,pclSendBuffer,pclLocalFile)) == RC_SUCCESS)
           {
              if (pcgUseFtpFile[0]=='Y')
              {/* 20060803 JIM: Prf 8352: FTP or TCP */
                 /* transfering flight table to the BHS-system using FTPHDL */
                 if ((ilRc = SendFileViaFtp(pcgBHS_Host,pclLocalFile)) != RC_SUCCESS)
                 {
                    dbg(TRACE,"PrepareData: SendFileViaFtp() to <%s> failed!",pcgBHS_Host); 
                 }
              }
           }
        }
        else
        {
           dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
           dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
        }
        break;

     case CXX:
     case SFU:
        if (ipCmd==CXX)
        {
           dbg(DEBUG,"PrepareData: (CXX) <%s>",pcpSelection);
            sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s %s",
            pcgValid_SFX_Fields,pcgTabEnd,pcpSelection,prgCfg.db_filter);
           /*if (igTerminalChange == FALSE)
            HEB fix issue UFIS-882*/
             sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s %s",
                      pcgValid_SFX_Fields,pcgTabEnd,pcpSelection,prgCfg.cxx_filter);
            /* 
            if (igTerminalChange == TRUE)
              sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s %s",
                      pcgValid_SFX_Fields,pcgTabEnd,pcpSelection,prgCfg.cxx_filter);
           else
              sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s %s",
                      pcgValid_SFX_Fields,pcgTabEnd,pcpSelection,prgCfg.term_filter);
             * */
        }
        else
        {
           dbg(DEBUG,"PrepareData: (SFU) <%s>",pcpSelection);
           sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s %s",
                   pcgValid_SFX_Fields,pcgTabEnd,pcpSelection,prgCfg.db_filter);
        }
        slFkt = START; 
        slCursor = 0;
        memset(pclTmpSqlAnswer,0x00,XL_BUFF);
        memset(pclData,0x00,DATABLK_SIZE);
        if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
        {
           ilCnt++;
           BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SFX_Fields,0,",");
           strcat(pclData,pclTmpSqlAnswer);
        }
        close_my_cursor(&slCursor);
        if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
        {
           if ((ilRc = CheckFlight(pclData,pcgValid_SFX_Fields,ipCmd,&ilType)) == RC_SUCCESS)
           {
              memset(pclSendBuffer,0x00,XL_BUFF);
              if ((ilRc = ProcessFlights(pclData,ilType,pclSendBuffer,"")) == RC_SUCCESS)
              {
                 ilDataLen = (int)strlen(pclSendBuffer);
                 if ((ilRc=Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
                 {
                    CloseTCP();
                 }
                 else
                 {
                    ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
                 }
              }
           }
        }
        else
        {
           dbg(TRACE,"PrepareData: sql_if() <%s> ",pclSqlBuf);
           dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
           dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
        }
        break;

     case DFR:
        memset(pclSendBuffer,0x00,XL_BUFF);
        ilType = CXX;
        if ((ilRc = ProcessFlights(pcpSelection,ilType,pclSendBuffer,"")) == RC_SUCCESS)
        {
           ilDataLen = (int)strlen(pclSendBuffer);
           if ((ilRc=Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
           {
              CloseTCP();
           }
           else
           {
              ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
           }
        }
        break;

     case SFI:
        dbg(DEBUG,"PrepareData: (SFI) <%s>",pcpSelection);
        memset(pclDBSelection,0x00,L_BUFF);
        /* should HomePort(HOPO) database field be used or not ? */
        if (bgUseHopo == TRUE)
        {
           sprintf(pclDBSelection,"WHERE URNO = %s AND HOPO = '%s' %s",
                   pcpSelection,pcgHomeAp,prgCfg.db_filter);
        }
        else
        {
           sprintf(pclDBSelection,"WHERE URNO = %s %s",
                   pcpSelection,prgCfg.db_filter);
        }
        dbg(DEBUG,"PrepareData: (SFI) <%s>",pclDBSelection);
        sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pcgValid_SFX_Fields,pcgTabEnd,pclDBSelection);
        slFkt = START; 
        slCursor = 0;
        memset(pclTmpSqlAnswer,0x00,XL_BUFF);
        memset(pclData,0x00,DATABLK_SIZE);
        if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
        {
           ilCnt++;
           BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SFX_Fields,0,",");
           strcat(pclData,pclTmpSqlAnswer);
        }
        close_my_cursor(&slCursor);
        if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
        {
           if ((ilRc = CheckFlight(pclData,pcgValid_SFX_Fields,ipCmd,&ilType)) == RC_SUCCESS)
           {
              memset(pclSendBuffer,0x00,XL_BUFF);
              if ((ilRc = ProcessFlights(pclData,ilType,pclSendBuffer,"")) == RC_SUCCESS)
              {
                 ilDataLen = (int)strlen(pclSendBuffer);
                 if ((ilRc=Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
                 {
                    CloseTCP();
                 }
                 else
                 {
                    ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
                 }
              }
           }
        }
        else
        {
           dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
           dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
        }
        break;

     default:
        dbg(TRACE,"PrepareData: unknown command <%d> received!",ipCmd);
     break;
  }
  return ilRc;
}
/* **************************************************************** */
/* The SendEvent routine                                            */
/* prepares an internal CEDA-event                                  */
/* **************************************************************** */
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
                     char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
                     int ipPriority,char *pcpTable,char *pcpType,char *pcpFile)
{
  int ilRc = RC_FAIL;
  int ilLen = 0;
  EVENT   *prlOutEvent = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (ipModIdSend == 0)
  {
     dbg(TRACE,"SendEvent: Can't send event to ModID=<0> !");
  }
  else
  {
     /* size-calculation for prlOutEvent */
     ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
     strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;
		
     /* memory for prlOutEvent */
     if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
     {
        dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
        prlOutEvent = NULL;
     }
     else
     {
        /* clear whole outgoing event */
        memset((void*)prlOutEvent, 0x00, ilLen);

        /* set event structure... */
        prlOutEvent->type = SYS_EVENT;
        prlOutEvent->command = EVENT_DATA;

        if (ipModIdRecv == igSecondQueue)
        {
           prlOutEvent->originator  = (short)igSecondQueue;
        }
        else
        {
           prlOutEvent->originator  = (short)mod_id;
        }
        prlOutEvent->retry_count  = 0;
        prlOutEvent->data_offset  = sizeof(EVENT);
        prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

        /* BC_HEAD-Structure... */
        prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
        prlOutBCHead->rc = (short)RC_SUCCESS;
        strncpy(prlOutBCHead->dest_name,pcgProcessName,10);
        strncpy(prlOutBCHead->recv_name, "EXCO",10);

        /* Cmdblk-Structure... */
        prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
        strcpy(prlOutCmdblk->command,pcpCmd);
        strcpy(prlOutCmdblk->obj_name,pcpTable);
        strcat(prlOutCmdblk->obj_name,pcgTabEnd);
		
        /* setting tw_start entries */
        sprintf(prlOutCmdblk->tw_start,"%s,%s",pcpType,pcpFile);
			
        /* setting tw_end entries */
        sprintf(prlOutCmdblk->tw_end,"%s,%s,%s",pcgHomeAp,pcgTabEnd,pcgProcessName);
	
        /* setting selection inside event */
        strcpy(prlOutCmdblk->data,pcpSelection);
 
        /* setting field-list inside event */
        strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

        /* setting data-list inside event */
        strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);

        if (pcpAddStruct != NULL)
        {
           memcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) +
                  (strlen(pcpFields)+1)) + (strlen(pcpData)+1),pcpAddStruct,ipAddLen);
        }

        /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
        /*snapit((char*)prlOutEvent,ilLen,outp);*/

        dbg(DEBUG,"SendEvent: <%d> --<%s>--> <%d>",prlOutEvent->originator,pcpCmd,ipModIdSend);

        if ((ilRc = que(QUE_PUT,ipModIdSend,ipModIdRecv,ipPriority,ilLen,(char*)prlOutEvent)) != RC_SUCCESS)
        {
           dbg(TRACE,"SendEvent: QUE_PUT to <%d> returns: <%d>",ipModIdSend,ilRc);
           HandleQueErr(ilRc);
        }
        /* free memory */
        free((void*)prlOutEvent); 

        if (ipModIdRecv == igSecondQueue)
        {
           dbg(DEBUG,"SendEvent: LISTEN on <%d> for ACK.",igSecondQueue);
           do	
           {
              ilRc = que(QUE_GETBIG,0,igSecondQueue,PRIORITY_3,igItemLen,(char *)&prgItem);
              /* depending on the size of the received item  */
              /* a realloc could be made by the que function */
              /* so do never forget to set event pointer !!! */
              prgEvent = (EVENT *) prgItem->text;	
              if (ilRc == RC_SUCCESS)
              {
                 /* Acknowledge the item */
                 ilRc = que(QUE_ACK,0,igSecondQueue,0,0,NULL);
                 if (ilRc != RC_SUCCESS) 
                 {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                 } /* fi */
		
                 switch (prgEvent->command)
                 {
                    case EVENT_DATA:
                       if ((ctrl_sta == HSB_STANDALONE) ||
                           (ctrl_sta == HSB_ACTIVE) ||
                           (ctrl_sta == HSB_ACT_TO_SBY))
                       {
                          ilRc = HandleInternalData();
                       }
                       else
                       {
                          dbg(TRACE,"SendEvent: wrong HSB-status <%d>",ctrl_sta);
                          DebugPrintItem(TRACE,prgItem);
                          DebugPrintEvent(TRACE,prgEvent);
                       }/* end of if */
                       break;
                    default:
                       dbg(TRACE,"SendEvent: unknown/invalid event for Ack! Ignoring!");
                       DebugPrintItem(TRACE,prgItem);
                       DebugPrintEvent(TRACE,prgEvent);
                       break;
                 }
              } 
              else
              {
                 /* Handle queuing errors */
                 HandleQueErr(ilRc);
              } /* end else */
           } while (ilRc != RC_SUCCESS);
        }
     }
  }
  return ilRc;
}
/* ******************************************************************** */
/* The TimeToStr() routine                                              */
/* ******************************************************************** */
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType)
{
  int ilRc = 0;
  char pclTime[20];
  struct tm *_tm;
  struct tm rlTm;

  *pclTime = 0x00;
  *pcpTime = 0x00;
  if (lpTime == 0)
  {
     lpTime = time(NULL);
  }
  _tm = (struct tm *) localtime(&lpTime);
  rlTm = *_tm;
  switch (ipType)
  {
     case 0:
        /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
        /* Unusal format because of sccs !! */
        strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
        break;
     case 1:
        /* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
        /* Unusal format because of sccs !! */
        strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
        break;
     case 2:
        /* Unusal format because of sccs !! */
        strftime(pcpTime,15,"%" "a%" "d%" "m%" "y",&rlTm);
        strftime(pclTime,15,"%" "d%" "m%" "y",&rlTm);
        strcpy((char*)&pcpTime[2],pclTime);
        /* according to spec. the day must be all in uppercase letters */
        /* converting second byte from strftime() to uppercase */
        pcpTime[1] = toupper(pcpTime[1]);
        break;
     case 3:
        strftime(pcpTime,15,"%" "u",&rlTm);
        break;
     case 4:
        strftime(pcpTime,15,"%" "d%" "H%" "M%" "S",&rlTm);
        memset(pclTime,0x00,20);	
        strncpy(pclTime,pcpTime,2);
        strncat(pclTime,".",1);
        strncat(pclTime,(char*)&pcpTime[2],6);
        strncpy(pcpTime,pclTime,10);
        break;
     default:
        dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
        break;
  }
}
/* ***************************************************************** */
/* The ProcessFlights routine                                        */
/* ***************************************************************** */
static int ProcessFlights(char *pcpFlights,int ipType,char *pcpSendBuffer,char *pcpFile)
{
  int ilRc = RC_FAIL;
  int ilLines = 0;
  int ilCurLine = 0;
  FILE *fp = NULL;
  char pclTmpBuf[XL_BUFF];
  char *pclTmpPtr = NULL;
  BOOL blConvert = FALSE;
  char pclFileName[S_BUFF];
  int ilDataLen = 0;

  /* count number of datalines */
  if ((ilLines = (int)GetNoOfElements(pcpFlights, '\n')) <= 0)
  {
     dbg(TRACE,"PF: GetNoOfElements() returns <%d>",ilLines);
  }
  else
  {
     dbg(DEBUG,"PF: <%d> record(s) found!",ilLines);
     switch (ipType)
     {
        case SFS:
           dbg(DEBUG,"PF: record type <SFS>!");
           if (pcgUseFtpFile[0]=='Y')
           { /* 20060803 JIM: Prf 8352: FTP or TCP */
              memset(pclFileName,0x00,S_BUFF);
              dbg(DEBUG,"PF: create file <%s/%s>",prgCfg.fs_local_path,pcpFile);
              sprintf(pclFileName,"%s/%s",prgCfg.fs_local_path,pcpFile);
              fp = fopen (pclFileName,"w");
              if (fp == NULL)
              {
                 dbg(TRACE,"PF: open file <%s> failed! Error: <%s>",pclFileName,strerror(errno));
                 blConvert = FALSE;
              }
              else
              {
                 blConvert = TRUE;
              }
           }
           else
           {
              dbg(DEBUG,"PF: using TCP line for flight table.... ");
              blConvert = TRUE;
           }
           break;
        case SFU:
           dbg(DEBUG,"PF: record type <SFX>!");
           blConvert = TRUE;
           break;
        case CXX:
           dbg(DEBUG,"PF: record type <CXX>!");
           blConvert = TRUE;
           break;
     }

     if (blConvert==TRUE)
     {
        /* set temporary pointer to data area */
        pclTmpPtr = pcpFlights; 
        /* over all lines */
        for (ilCurLine=0; ilCurLine<ilLines; ilCurLine++)
        {
           /* get next data for writing... */
           memset((void*)pclTmpBuf, 0x00, XL_BUFF);
           if ((pclTmpPtr = CopyNextField(pclTmpPtr,'\n', pclTmpBuf)) == NULL)
           {
              dbg(TRACE,"PF: CopyNextField returns NULL!");
           }
           else
           {
              switch (ipType)
              {
                 case SFS:
                    if ((ilRc = Convert_SFX(pclTmpBuf)) == RC_SUCCESS)
                    {
                       *pcpSendBuffer = 0x00;
                       if ((ilRc = MakeSendBuffer(FALSE,MSG_UFI,pclTmpBuf,pcpSendBuffer)) == RC_SUCCESS)
                       {
                          if (pcgUseFtpFile[0]=='Y')
                          { /* 20060803 JIM: Prf 8352: FTP or TCP */
                             if (fprintf(fp,"%s\n",pcpSendBuffer) > 0)
                                ilRc = RC_SUCCESS;
                          }
                          else
                          {
                             ilDataLen = (int)strlen(pcpSendBuffer);
                             if ((ilRc=Send_data(igSock,pcpSendBuffer,ilDataLen))==RC_FAIL)
                             {
                                CloseTCP();
                             }
                             else
                             {
                                if ((ilRc = WaitForAck(igSock,pcpSendBuffer,ilDataLen))==RC_FAIL)
                                {
                                   dbg(TRACE,"PF: Closing connection!");
                                   ilCurLine=ilLines;
                                   CloseTCP();
                                }
                             }
                          }
                       }
                       else
                       {
                          dbg(TRACE,"PF: MakeSendBuffer() <SFS> failed!");
                       }
                    }
                    else
                    {
                       dbg(TRACE,"PF: couldn't convert line <%s>",pclTmpBuf);
                    }
                    break;
                 case SFU:
                    /*setting pclData to the first data-field after FTYP-data */
                    /*so that the data-list is in the right format and length */
                    /*for the conversion to MANNESMANN-format */
                    strcpy(pclTmpBuf,(char*)&pclTmpBuf[19]);
                    if ((ilRc = Convert_SFX(pclTmpBuf)) == RC_SUCCESS)
                    {
                       *pcpSendBuffer = 0x00;
                       if ((ilRc = MakeSendBuffer(TRUE,MSG_UFI,pclTmpBuf,pcpSendBuffer)) != RC_SUCCESS)
                       {
                          dbg(TRACE,"PF: MakeSendBuffer() <SFU> failed!");
                       }
                    }
                    else
                    {
                       dbg(TRACE,"PF: couldn't convert line <%s>",pclTmpBuf);
                    }
                    break;
                 case CXX:
                    *pcpSendBuffer = 0x00;
                    if ((ilRc = Convert_CXX(pclTmpBuf)) == RC_SUCCESS)
                    {
                       if ((ilRc = MakeSendBuffer(TRUE,MSG_CXX,pclTmpBuf,pcpSendBuffer)) != RC_SUCCESS)
                       {
                          dbg(TRACE,"PF: MakeSendBuffer() <CXX> failed!");
                       }
                    }
                    break;
              }
           }
        }
     }

     switch(ipType)
     {
        case SFS:
           if (pcgUseFtpFile[0]=='Y')
           { /* 20060803 JIM: Prf 8352: FTP or TCP */
              dbg(DEBUG,"PF: close file <%s/%s>",prgCfg.fs_local_path,pcpFile);
              if (fclose(fp)!=0)
              {
                 dbg(TRACE,"PF: couldn't close file <%s>",pclFileName);
                 ilRc = RC_FAIL;
              }
           }
           break;
        default:
           break;
     }
  } 
  return ilRc;
}
/* *****************************************************************/
/* The Convert_SFX() routine                                       */
/* Converts CEDA-data to the BHS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_SFX(char *pcpData)
{
  int ilRc = RC_FAIL;
  int ilCnt = 0;
  int ilFields = 0;
  int ilVian = 0;
  int ilBytes = 0;
  int ilValidFields = 0;
  int ilActField = 0;
  CAI_OUT prlCaiExt;
  char *pclTmpPtr = NULL;
  char pclTmpBuf[L_BUFF];
  char pclNose[S_BUFF];
  char pclDay[20];
  char pclToday[20];
  char *pclVia = NULL;
  char pclTmpVia[MAX_TELEGRAM_LEN];
  time_t tlStod = 0;
  time_t tlEtod = 0;
  char pclFlno[16];
  char pclStod[16];

  ilFields = (int)GetNoOfElements(pcpData, ',');
  ilValidFields = (int)GetNoOfElements(pcgValid_SFS_Fields, ',');

  if ((ilFields < 0) || (ilFields != ilValidFields))
  {
     dbg(TRACE,"CBF: invalid Nr. of data-fields <%d>",ilFields);
  }
  else
  {
     pclTmpPtr = pcpData;
     memset((char*)&prlCaiExt,0x00,sizeof(CAI_OUT));
  
     /* over all elements */
     for (ilActField=0; ilActField<ilFields; ilActField++)
     {
        /* get next data */
        memset((void*)pclTmpBuf, 0x00, L_BUFF);
        if ((pclTmpPtr = CopyNextField(pclTmpPtr,',',pclTmpBuf)) == NULL)
        {
           dbg(TRACE,"CBF: CopyNextField returns NULL!");
        }
        else
        {
           /* handle/convert field data */
           switch (ilActField)
           {
              case 0: /* flno */
                 strcpy(pclFlno,pclTmpBuf);
                 if ((ilRc = MakeExtFlno(pclTmpBuf)) == RC_SUCCESS)
                 {
                    MakeSingleEntry(pclTmpBuf,8,LEFT,' ',prlCaiExt.flno);
                 }
                 else
                 {
                    ilActField = ilFields;	
                 }
                 break;
              case 1: /* stod */
                 strcpy(pclStod,pclTmpBuf);
                 StrToTime(pclTmpBuf,&tlStod);
                 MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCaiExt.stod);
                 break;
              case 2: /* etod */
                 StrToTime(pclTmpBuf,&tlEtod);
                 MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCaiExt.etod);
                 break;
              case 3: /* alc3 */
                 /* MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlCaiExt.handag);*/
                 /* 20070117 jim: get the Baggage Handling agent according ALC3, max 3 Chars!*/ 
                 GetHandlingAgent(pclTmpBuf,pclFlno,pclStod,prlCaiExt.handag);
                 break;
              case 4: /* act3 */
                 MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlCaiExt.act);
                 break;
              case 5: /* nose */
                 memset(pclNose,0x00,S_BUFF);
                 sprintf(pclNose,"%04d",atoi(pclTmpBuf));
                 MakeSingleEntry(pclNose,4,LEFT,' ',prlCaiExt.cap);
                 break;
              case 6: /* vian */
                 ilVian = atoi(pclTmpBuf);
                 break;
              case 7: /* vial */
                 memset(pclTmpVia,0x00,MAX_TELEGRAM_LEN);
                 for (ilCnt = 0;ilCnt < ilVian;ilCnt++)
                 {
                    if ((pclVia=GetViaByNumber(pclTmpBuf,ilCnt+1,3)) != NULL)
                    {
                       /* copy VIA to a temp-buffer */
                       strncat(pclTmpVia,pclVia,3);
                       /* fill the chute-nr for the VIA with default-spaces */
                       strncat(pclTmpVia,"   ",3);
                       /* fill the chute open time with default-spaces (will be set by Mannesmann) */
                       strncat(pclTmpVia,"            ",12);
                       /* fill the chute close time with default-spaces (will be set by Mannesmann) */
                       strncat(pclTmpVia,"            ",12);
                    }
                 }
                 /*ilBytes = strlen(pclTmpVia);*/
                 /*memcpy(prlCaiExt.dest1,pclTmpVia,ilBytes);*/
                 break;
              case 8: /* des3 */
                 strncat(pclTmpVia,pclTmpBuf,3);
                 strncat(pclTmpVia,"   ",3);
                 strncat(pclTmpVia,"            ",12);
                 strncat(pclTmpVia,"            ",12);
                 ilBytes = strlen(pclTmpVia);
                 memcpy(prlCaiExt.dest1,pclTmpVia,ilBytes);
                 ilRc = RC_SUCCESS;
                 break;
              default: /* unknown */
                 dbg(TRACE,"CBF: don't know how to handle field Nr. <%d>",ilActField);
                 ilRc = RC_FAIL;
                 break;
           }/* end switch */
        }
     }/* for end */

     /* Now cleaning structure from unallowed bytes (0x00 bytes) */
     /* until one byte before the terminating NULL(0x00)-byte */
     CleanMessage((char*)&prlCaiExt,(sizeof(CAI_OUT)-1));
		
     TimeToStr(pclToday,0,0);	
     TimeToStr(pclDay,tlStod,1);	
     if (strncmp(pclToday,pclDay,8) == 0)
     {
        MakeSingleEntry("8",1,LEFT,' ',prlCaiExt.day);
     }
     else
     {
        TimeToStr(pclDay,tlStod,3);	
        MakeSingleEntry(pclDay,1,LEFT,' ',prlCaiExt.day);
     }

     /* setting the time-status flag */
     if (tlEtod <= tlStod)
     {
        MakeSingleEntry("O",1,LEFT,' ',prlCaiExt.status);
     }
     else
     {
        MakeSingleEntry("L",1,LEFT,' ',prlCaiExt.status);
     }

     if (ilRc == RC_SUCCESS)
     {
        /*snapit((char*)&prlCaiExt,sizeof(CAI_OUT),outp);*/
        *pcpData = 0x00;
        strncpy(pcpData,(char*)&prlCaiExt,sizeof(CAI_OUT));
     }
  }
  return ilRc;
}
/* *****************************************************************/
/* The Convert_CXX() routine                                       */
/* Converts CEDA-data to the BHS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_CXX(char *pcpData)
{
  int ilRc = RC_SUCCESS;
  CXX_EXT prlCxxExt;
  char pclDay[20];
  char pclToday[20];
  FI_INT rlFiInt;
	
  time_t tlStod = 0;
  time_t tlEtod = 0;
	
  memset(&rlFiInt,0x00,sizeof(FI_INT));
  get_real_item(rlFiInt.flno,pcpData,4);
  dbg(DEBUG,"Convert_CXX: FLNO=(%s)",rlFiInt.flno); 	
  get_real_item(rlFiInt.stod,pcpData,5);
  dbg(DEBUG,"Convert_CXX: STOD=(%s)",rlFiInt.stod); 	

  memset((char*)&prlCxxExt,0x00,sizeof(CXX_EXT));
  /* flno */
  if ((ilRc = MakeExtFlno(rlFiInt.flno)) == RC_SUCCESS)
  {
     MakeSingleEntry(rlFiInt.flno,8,LEFT,' ',prlCxxExt.flno);
     /* STOD */
     StrToTime(rlFiInt.stod,&tlStod);
     MakeSingleEntry(rlFiInt.stod,12,LEFT,' ',prlCxxExt.stod);

     /* Now cleaning structure from unallowed bytes (0x00  bytes) */
     /* until one byte before the terminating NULL(0x00)-byte */
     CleanMessage((char*)&prlCxxExt,(sizeof(CXX_EXT)-1));

     TimeToStr(pclToday,0,0);	
     TimeToStr(pclDay,tlStod,1);	
     if (strncmp(pclToday,pclDay,8) == 0)
     {
        MakeSingleEntry("8",1,LEFT,' ',prlCxxExt.day);
     }
     else
     {
        TimeToStr(pclDay,tlStod,3);	
        MakeSingleEntry(pclDay,1,LEFT,' ',prlCxxExt.day);
     }
  }

  if (ilRc == RC_SUCCESS)
  {
     /*snapit((char*)&prlCaiExt,sizeof(CXX_EXT),outp);*/
     *pcpData = 0x00;
     strncpy(pcpData,(char*)&prlCxxExt,sizeof(CXX_EXT));
  }
  return ilRc;
}
/* ******************************************************************** */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,char *pcpDest)
{
  int ilSrcLen = 0;
  int ilCnt = 0;
  int ilNrOfFillChars = 0;

  ilSrcLen = (int)strlen(pcpSrc);
  if (ilSrcLen > 0)
  {
     if (ilSrcLen >= ipLen)
     {
        strncpy(pcpDest,pcpSrc,ipLen);
     }
     else
     {
        ilNrOfFillChars = ipLen - ilSrcLen;
        switch (ipAdjust)
        {
           case RIGHT:
              while (ilCnt < ilNrOfFillChars)
              {
                 memcpy(&pcpDest[ilCnt],&cpFill,1);
                 ilCnt++;
              }
              strncat(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
              break;
           case LEFT:
              strncpy(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
              while (ilCnt < ilNrOfFillChars)
              {
                 memcpy(&pcpDest[ilSrcLen+ilCnt],&cpFill,1);
                 ilCnt++;
              }
              break;
        }
     }
  }
}
/* *****************************************************************/
/* The Process_CAI_Telegramm() routine                             */
/* Handles chute allocation messages from BHS-system               */
/* *****************************************************************/
static int Process_CAI_Telegramm(char *pcpData)
{
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  CAI_IN *prlCaiExt = NULL;
  CAI_IN_T *prlCaiExtT = NULL;
  CAI_DB *prlCaiInt = NULL;
  char pclFlno[XS_BUFF];
  char pclDate[XS_BUFF];
  static char pclStaticFlno[XS_BUFF];
  static char pclStaticDate[XS_BUFF];
  char pclTimeBuff[14];
  char pclTmpBuff[100];
  char clIndicator;
  int ilI;
  char pclNewRurn[16];
  char pclNewLnam[16];
  char pclNewTifb[16];
  char pclNewDes3[16];
  int ilRCdb = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char pclSqlBuf[1024];
  char pclChaUrno[1024];

  clIndicator = 0x00;

  prlCaiExt = (CAI_IN*)pcpData;
  prlCaiExtT = (CAI_IN_T*)pcpData;
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     dbg(TRACE,"length: <%d>",strlen((char*)prlCaiExt));
     ilLen = strlen((char*)prlCaiExt);
  }
  else
  {
     dbg(TRACE,"length: <%d>",strlen((char*)prlCaiExtT));
     ilLen = strlen((char*)prlCaiExtT);
  }
  /* 20070117 JIM: corrected igCAI_ShortLength: if set <> 0, use it */
  if (igCAI_ShortLength != 0)
  { /* 20060808 JIM: Check if telex 1 char to short */
     if (ilLen == igCAI_ShortLength)
     { /* 20060808 JIM: probably 'class' was not part of ICD */
        if (prgCfg.ilReceiveTransitInfo == FALSE)
        {
           dbg(TRACE,"CAI short length: <%d>, adding class",strlen((char*)prlCaiExt));
           strcpy(pclTmpBuff,(char*)prlCaiExt->class);
           strcpy(prlCaiExt->flno,pclTmpBuff);
           *prlCaiExt->class='*';
        }
        else
        {
           dbg(TRACE,"CAI short length: <%d>, adding class",strlen((char*)prlCaiExtT));
           strcpy(pclTmpBuff,(char*)prlCaiExtT->class);
           strcpy(prlCaiExtT->flno,pclTmpBuff);
           *prlCaiExtT->class='*';
        }
     }
  }
  memset(pclTimeBuff,0x00,14);
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     strncpy(pclTimeBuff,(char*)&pcpData[sizeof(CAI_IN)],9);
     clIndicator = *prlCaiExt->indi;
  }
  else
  {
     strncpy(pclTimeBuff,(char*)&pcpData[sizeof(CAI_IN_T)],9);
     clIndicator = *prlCaiExtT->indi;
  }
  dbg(DEBUG,"CAI: received telegram at <%s>!",pclTimeBuff);
  if (prgCfg.EmptyChuteDeletes[0]=='Y')
  {
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        if (strncmp(prlCaiExt->chute,"   ",3)==0)
        {
           dbg(DEBUG,"CAI: empty chute received, setting INDI to <D>!");
           clIndicator= 'D';
        }
     }
     else
     {
        if (strncmp(prlCaiExtT->chute,"   ",3)==0)
        {
           dbg(DEBUG,"CAI: empty chute received, setting INDI to <D>!");
           clIndicator= 'D';
        }
     }
  }
  dbg(DEBUG,"CAI: INDI=<%c>",clIndicator);

  memset(pclFlno,0x00,XS_BUFF);
  memset(pclDate,0x00,XS_BUFF);
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     snapit((char*)prlCaiExt,sizeof(CAI_IN),outp);
     strncpy(pclFlno,prlCaiExt->flno,8);
     strncpy(pclDate,prlCaiExt->stod,12);
  }
  else
  {
     snapit((char*)prlCaiExtT,sizeof(CAI_IN_T),outp);
     strncpy(pclFlno,prlCaiExtT->flno,8);
     strncpy(pclDate,prlCaiExtT->stod,12);
  }
  dbg(DEBUG,"CAI: FLNO=<%s>",pclFlno);
  dbg(DEBUG,"CAI: STOD=<%s>",pclDate);

  switch (clIndicator)
  {
     case 'F':
        /* 20070117 JIM: in case of previous was 'F' (first), this may be 'F' also */
        if (clIndicator == cgNextAllowedIndi1 || clIndicator == cgNextAllowedIndi2 || clIndicator == 'F' )
        {
           /* setting the flno and date for comparsion with the 'N' and 'L' packages */
           memset(pclStaticFlno,0x00,XS_BUFF);
           memset(pclStaticDate,0x00,XS_BUFF);
           strncpy(pclStaticFlno,pclFlno,8);
           strncpy(pclStaticDate,pclDate,12);

           if ((ilRc = Build_CAI_Data(pcpData,&prlCaiInt,TRUE)) != RC_SUCCESS)
           {
              dbg(TRACE,"CAI: Build_CAI_Data() failed!");
              igFirstRc = RC_FAIL;
           }
           else
           {
              igFirstRc = RC_SUCCESS;
              if ((ilRc = DeleteOld_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: DeleteOld_CAI_Data() failed!");	
              if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
           }
        }
        else
        {
           dbg(TRACE,"CAI: unexpected indicator <%c> received!",clIndicator);
           dbg(TRACE,"CAI: expected indicators were <%c> or <%c>",cgNextAllowedIndi1,cgNextAllowedIndi2);
        }
        cgNextAllowedIndi1 = 'N';
        cgNextAllowedIndi2 = 'L';
        break;
     case 'N':
        if (igFirstRc == RC_SUCCESS &&
            (clIndicator == cgNextAllowedIndi1 || clIndicator == cgNextAllowedIndi2))
        {
           if (strcmp(pclFlno,pclStaticFlno)==0 && strcmp(pclDate,pclStaticDate)==0)
           {
              if ((ilRc = Build_CAI_Data(pcpData,&prlCaiInt,FALSE)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: Build_CAI_Data() failed!");
              else
              {
                 for (ilI = 1; ilI <= (int)GetNoOfElements(pcgChaInsertFields,','); ilI++)
                 {
                    GetDataItem(pclTmpBuff,pcgChaInsertFields,ilI,',',"","");
                    TrimRight(pclTmpBuff);
                    if (strcmp(pclTmpBuff,"RURN") == 0)
                       GetDataItem(pclNewRurn,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"LNAM") == 0)
                       GetDataItem(pclNewLnam,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"TIFB") == 0)
                       GetDataItem(pclNewTifb,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"DES3") == 0)
                       GetDataItem(pclNewDes3,(char*)prlCaiInt,ilI,'\0',"","");
                 }
                 if (strcmp(pclNewDes3,"@@@") == 0)
                 {
                    sprintf(pclSqlBuf,"SELECT URNO FROM CHATAB WHERE RURN = %s AND LNAM = '%s' AND TIFB = '%s' AND DES3 <> '@@@'",
                            pclNewRurn,pclNewLnam,pclNewTifb);
                    slFkt = START; 
                    slCursor = 0;
                    dbg(DEBUG,"CAI: SQL = <%s>",pclSqlBuf);
                    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclChaUrno);
                    close_my_cursor(&slCursor);
                    if (ilRCdb != DB_SUCCESS)
                    {
                       if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                          dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
                    }
                    else
                       dbg(DEBUG,"CAI: Don't Insert New Record because it's Destination is '@@@'");
                 }
                 else
                 {
                    sprintf(pclSqlBuf,"SELECT URNO FROM CHATAB WHERE RURN = %s AND LNAM = '%s' AND TIFB = '%s' AND DES3 = '@@@'",
                            pclNewRurn,pclNewLnam,pclNewTifb);
                    slFkt = START; 
                    slCursor = 0;
                    dbg(DEBUG,"CAI: SQL = <%s>",pclSqlBuf);
                    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclChaUrno);
                    close_my_cursor(&slCursor);
                    if (ilRCdb == DB_SUCCESS)
                    {
                       dbg(DEBUG,"CAI: Delete Previous Record because it's Destination is '@@@'");
                       ilRc = DeleteOldRecord(pclChaUrno);
                    }
                    if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                       dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
                 }
              }
           }
           else
           {
              dbg(TRACE,"CAI: Date and/or Flight-No. DO NOT match to previous telegram!");
           }
        }
        else
        {
           dbg(TRACE,"CAI: Flight invalid or unexpected indicator <%c> received!",clIndicator);
           dbg(TRACE,"CAI: expected indicators were <%c> or <%c>",cgNextAllowedIndi1,cgNextAllowedIndi2);
        }
        cgNextAllowedIndi1 = 'L';
        cgNextAllowedIndi2 = 'N';
        break;
     case 'L':
        if (igFirstRc == RC_SUCCESS &&
            (clIndicator == cgNextAllowedIndi1 || clIndicator == cgNextAllowedIndi2))
        {
           if (strcmp(pclFlno,pclStaticFlno)==0 && strcmp(pclDate,pclStaticDate)==0)
           {
              if ((ilRc = Build_CAI_Data(pcpData,&prlCaiInt,FALSE)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: Build_CAI_Data() failed!");
              else
              {
                 for (ilI = 1; ilI <= (int)GetNoOfElements(pcgChaInsertFields,','); ilI++)
                 {
                    GetDataItem(pclTmpBuff,pcgChaInsertFields,ilI,',',"","");
                    TrimRight(pclTmpBuff);
                    if (strcmp(pclTmpBuff,"RURN") == 0)
                       GetDataItem(pclNewRurn,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"LNAM") == 0)
                       GetDataItem(pclNewLnam,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"TIFB") == 0)
                       GetDataItem(pclNewTifb,(char*)prlCaiInt,ilI,'\0',"","");
                    else if (strcmp(pclTmpBuff,"DES3") == 0)
                       GetDataItem(pclNewDes3,(char*)prlCaiInt,ilI,'\0',"","");
                 }
                 if (strcmp(pclNewDes3,"@@@") == 0)
                 {
                    sprintf(pclSqlBuf,"SELECT URNO FROM CHATAB WHERE RURN = %s AND LNAM = '%s' AND TIFB = '%s' AND DES3 <> '@@@'",
                            pclNewRurn,pclNewLnam,pclNewTifb);
                    slFkt = START; 
                    slCursor = 0;
                    dbg(DEBUG,"CAI: SQL = <%s>",pclSqlBuf);
                    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclChaUrno);
                    close_my_cursor(&slCursor);
                    if (ilRCdb != DB_SUCCESS)
                    {
                       if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                          dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
                    }
                    else
                       dbg(DEBUG,"CAI: Don't Insert New Record because it's Destination is '@@@'");
                 }
                 else
                 {
                    sprintf(pclSqlBuf,"SELECT URNO FROM CHATAB WHERE RURN = %s AND LNAM = '%s' AND TIFB = '%s' AND DES3 = '@@@'",
                            pclNewRurn,pclNewLnam,pclNewTifb);
                    slFkt = START; 
                    slCursor = 0;
                    dbg(DEBUG,"CAI: SQL = <%s>",pclSqlBuf);
                    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclChaUrno);
                    close_my_cursor(&slCursor);
                    if (ilRCdb == DB_SUCCESS)
                    {
                       dbg(DEBUG,"CAI: Delete Previous Record because it's Destination is '@@@'");
                       ilRc = DeleteOldRecord(pclChaUrno);
                    }
                    if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                       dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
                 }
              }
           }
           else
           {
              dbg(TRACE,"CAI: Date and/or Flight-No. DO NOT match to previous telegram!");
           }
        }
        else
        {
           dbg(TRACE,"CAI: Flight invalid or unexpected indicator <%c> received!",clIndicator);
           dbg(TRACE,"CAI: expected indicators were <%c> or <%c>",cgNextAllowedIndi1,cgNextAllowedIndi2);
        }
        cgNextAllowedIndi1 = 'S';
        cgNextAllowedIndi2 = 'F';
        break;
     case 'S':
        if (clIndicator == cgNextAllowedIndi1 || clIndicator == cgNextAllowedIndi2)
        {
           if ((ilRc = Build_CAI_Data(pcpData,&prlCaiInt,TRUE)) != RC_SUCCESS)
              dbg(TRACE,"CAI: Build_CAI_Data() failed!");
           else
           {
              if ((ilRc = DeleteOld_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: DeleteOld_CAI_Data() failed!");	
              if ((ilRc = SetNew_CAI_Data(prlCaiInt)) != RC_SUCCESS)
                 dbg(TRACE,"CAI: SetNew_CAI_Data() failed!");	
           }
        }
        else
        {
           dbg(TRACE,"CAI: unexpected indicator <%c> received!",clIndicator);
           dbg(TRACE,"CAI: expected indicators were <%c> or <%c>",cgNextAllowedIndi1,cgNextAllowedIndi2);
        }
        cgNextAllowedIndi1 = 'S';
        cgNextAllowedIndi2 = 'F';
        break;
     case 'D': /* 20060803 JIM: PRF 8352: indacator 'D' to delete chutes */
        if ((ilRc = Build_CAI_Data(pcpData,&prlCaiInt,TRUE)) != RC_SUCCESS)
           dbg(TRACE,"CAI: Build_CAI_Data() failed!");
        else
        {
           if ((ilRc = DeleteOld_CAI_Data(prlCaiInt)) != RC_SUCCESS)
              dbg(TRACE,"CAI: DeleteOld_CAI_Data() failed!");	
        }
        cgNextAllowedIndi1 = 'S';
        cgNextAllowedIndi2 = 'F';
        break;
     default:
        dbg(TRACE,"CAI: invalid indicator <%c> received",*prlCaiExt->indi);	
        break;
  }
  return ilRc;
}
/* *****************************************************************/
/* The Build_CAI_Data() routine                                    */
/* *****************************************************************/
static int Build_CAI_Data(char *pcpData,CAI_DB **prpCaiInt,int ipDoSelect)
{
  int ilRc = RC_FAIL;
  static CAI_DB rlCaiInt;
  CAI_IN *prlCaiExt = NULL;
  CAI_IN_T *prlCaiExtT = NULL;
  char pclDBSelection[L_BUFF];
  char *pclDBFields = NULL;
  char *pclDBData = NULL;
  char pclFlno[XS_BUFF];
  char pclDate[XS_BUFF];
  char pclTmpBuff[XS_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclTmpSqlAnswer[S_BUFF];
  short slFkt = 0;
  short slCursor = 0;

  prlCaiExt = (CAI_IN*)pcpData;
  prlCaiExtT = (CAI_IN_T*)pcpData;
  if (ipDoSelect == TRUE)
  {
     memset(pclFlno,0x00,XS_BUFF);
     memset(pclDate,0x00,XS_BUFF);
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        strncpy(pclFlno,prlCaiExt->flno,8);
        strncpy(pclDate,prlCaiExt->stod,12);
     }
     else
     {
        strncpy(pclFlno,prlCaiExtT->flno,8);
        strncpy(pclDate,prlCaiExtT->stod,12);
     }
     MakeIntFlno(pclFlno);
     dbg(DEBUG,"Build_CAI_Data: FLNO=<%s>",pclFlno);
     dbg(DEBUG,"Build_CAI_Data: STOD=<%s>",pclDate);

     if (bgUseHopo == TRUE)
     {
        sprintf(pclDBSelection,"WHERE HOPO='%s' AND FLNO='%s' AND STOD LIKE '%s",
                pcgHomeAp,pclFlno,pclDate);
        strcat(pclDBSelection,"%' ");
        strcat(pclDBSelection,prgCfg.db_filter);
     }
     else
     {
        sprintf(pclDBSelection,"WHERE FLNO='%s' AND STOD LIKE '%s",pclFlno,pclDate);
        strcat(pclDBSelection,"%' ");
        strcat(pclDBSelection,pcgFtypClause);
        strcat(pclDBSelection,prgCfg.db_filter);
     }
     /* setting field list for request */
     pclDBFields = "URNO,FTYP";
     /* setting data list for request */
     pclDBData = " ";
     sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pclDBFields,pcgTabEnd,pclDBSelection);
     slFkt = START; 
     slCursor = 0;
     memset(pclTmpSqlAnswer,0x00,S_BUFF);
     dbg(DEBUG,"Build_CAI_Data: SQL<%s>",pclSqlBuf);
     if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
     {
        dbg(TRACE,"Build_CAI_Data: sql_if() failed with <%d>",ilRc);
     }
     else
     {
        tool_filter_spaces(pclTmpSqlAnswer); 
        GetDataItem(rlCaiInt.rurn,pclTmpSqlAnswer,1,'\0',"","");
        dbg(DEBUG,"Build_CAI_Data: RURN=<%s>",rlCaiInt.rurn);
        GetDataItem(rlCaiInt.ftyp,pclTmpSqlAnswer,2,'\0',"","");
        dbg(DEBUG,"Build_CAI_Data: FTYP=<%s>",rlCaiInt.ftyp);
     }
     close_my_cursor(&slCursor);
  }
  else
  {
     ilRc = RC_SUCCESS;
  }

  if (ilRc == RC_SUCCESS)
  {
     /*--------------------------------------------------------*/
     /* copy/reformat external chute info's to internal format */
     /*--------------------------------------------------------*/
     memset(pclDate,0x00,XS_BUFF);
     TimeToStr(pclDate,0,0);
     strncpy(rlCaiInt.cdat,pclDate,14);
     dbg(DEBUG,"Build_CAI_Data: CDAT=<%s>",rlCaiInt.cdat);
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        strncpy(rlCaiInt.des3,prlCaiExt->des3,3);
        strncpy(rlCaiInt.fcla,prlCaiExt->class,1);
     }
     else
     {
        strncpy(rlCaiInt.des3,prlCaiExtT->des3,3);
        strncpy(rlCaiInt.fcla,prlCaiExtT->class,1);
     }
     dbg(DEBUG,"Build_CAI_Data: DES3=<%s>",rlCaiInt.des3);
     dbg(DEBUG,"Build_CAI_Data: FCLA=<%s>",rlCaiInt.fcla);
     strncpy(rlCaiInt.des4," ",1);
     if (prgCfg.ilReceiveTransitInfo == TRUE)
     {
        strncpy(rlCaiInt.lafx,prlCaiExtT->transit,1);
        dbg(DEBUG,"Build_CAI_Data: LAFX=<%s>",rlCaiInt.lafx);
     }
     if (bgUseHopo == TRUE)
        strncpy(rlCaiInt.hopo,pcgHomeAp,3);
     else
        strncpy(rlCaiInt.hopo," ",1);
     dbg(DEBUG,"Build_CAI_Data: HOPO=<%s>",rlCaiInt.hopo);
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        strncpy(rlCaiInt.indi,prlCaiExt->indi,1);
        strncpy(rlCaiInt.lnam,prlCaiExt->chute,3);
     }
     else
     {
        strncpy(rlCaiInt.indi,prlCaiExtT->indi,1);
        strncpy(rlCaiInt.lnam,prlCaiExtT->chute,3);
     }
     dbg(DEBUG,"Build_CAI_Data: INDI=<%s>",rlCaiInt.indi);
     dbg(DEBUG,"Build_CAI_Data: LNAM=<%s>",rlCaiInt.lnam);
     strncpy(rlCaiInt.lstu,pclDate,14);
     dbg(DEBUG,"Build_CAI_Data: LSTU=<%s>",rlCaiInt.lstu);
     strncpy(rlCaiInt.ltyp,"CHU",3);
     dbg(DEBUG,"Build_CAI_Data: LTYP=<%s>",rlCaiInt.ltyp);
     strncpy(rlCaiInt.rtab,"AFT",3);
     dbg(DEBUG,"Build_CAI_Data: RTAB=<%s>",rlCaiInt.rtab);
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        strncpy(rlCaiInt.stob,prlCaiExt->o_tim,12);
        strncpy(rlCaiInt.stoe,prlCaiExt->c_tim,12);
        strncpy(rlCaiInt.tifb,prlCaiExt->o_tim,12);
        strncpy(rlCaiInt.tife,prlCaiExt->c_tim,12);
     }
     else
     {
        strncpy(rlCaiInt.stob,prlCaiExtT->o_tim,12);
        strncpy(rlCaiInt.stoe,prlCaiExtT->c_tim,12);
        strncpy(rlCaiInt.tifb,prlCaiExtT->o_tim,12);
        strncpy(rlCaiInt.tife,prlCaiExtT->c_tim,12);
     }
     dbg(DEBUG,"Build_CAI_Data: STOB=<%s>",rlCaiInt.stob);
     dbg(DEBUG,"Build_CAI_Data: STOE=<%s>",rlCaiInt.stoe);
     dbg(DEBUG,"Build_CAI_Data: TIFB=<%s>",rlCaiInt.tifb);
     dbg(DEBUG,"Build_CAI_Data: TIFE=<%s>",rlCaiInt.tife);
     strncpy(rlCaiInt.tisb,"S",1);
     dbg(DEBUG,"Build_CAI_Data: TISB=<%s>",rlCaiInt.tisb);
     strncpy(rlCaiInt.tise,"S",1);
     dbg(DEBUG,"Build_CAI_Data: TISE=<%s>",rlCaiInt.tise);
     if (*prgCfg.db_write == 'Y' || *prgCfg.db_write == 'y')
     {	
        /*GetNextNumbers("SNOTAB",pclTmpBuff,1,"GNNV");*/
        GetNextValues(pclTmpBuff,1);
        GetDataItem(rlCaiInt.urno,pclTmpBuff,1,',',"","");
        dbg(DEBUG,"Build_CAI_Data: URNO=<%s>",rlCaiInt.urno);
     }
     else
     {
        dbg(DEBUG,"Build_CAI_Data: URNO stays empty becauseof DB_WRITE != 'Y'.");
        strcpy(rlCaiInt.urno," ");
     }
     memset(pclTmpBuff,0x00,XS_BUFF);
     sprintf(pclTmpBuff,"EXCO-%s",mod_name);
     strcpy(rlCaiInt.usec,pclTmpBuff);
     dbg(DEBUG,"Build_CAI_Data: USEC=<%s>",rlCaiInt.usec);
     strcpy(rlCaiInt.useu,pclTmpBuff);
     dbg(DEBUG,"Build_CAI_Data: USEU=<%s>",rlCaiInt.useu);
     *prpCaiInt = &rlCaiInt;
  }
  return ilRc;
}
/* *****************************************************************/
/* The DeleteOld_CAI_Data() routine                                */
/* *****************************************************************/
static int DeleteOld_CAI_Data(CAI_DB *prpCaiInt)
{
  int ilRc = RC_FAIL;
  char pclDBSelection[L_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclTmpSqlAnswer[S_BUFF];

  if (*prgCfg.db_write != 'Y' && *prgCfg.db_write != 'y')
  {	
     dbg(DEBUG,"DeleteOld_CAI_Data: DB_WRITE != \'Y'. Not deleting!");
     ilRc = RC_SUCCESS;
  }
  else
  {
     if (bgUseHopo == TRUE)
     {
        sprintf(pclDBSelection,"WHERE HOPO='%s' AND RURN=%s",pcgHomeAp,prpCaiInt->rurn);
     }
     else
     {
        sprintf(pclDBSelection,"WHERE RURN=%s",prpCaiInt->rurn);
     }
     ilRc=SendEvent("DRT",pclDBSelection,"","",NULL,0,igModID_Router,igSecondQueue,
                    PRIORITY_3,"CHA","BHSIF-DELETE","");
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"DeleteOld_CAI_Data: DELETE from CHA%s failed with <%d>.",pcgTabEnd,ilRc);
     }
  }
  return ilRc;
}
/* *****************************************************************/
/* The DeleteOldRecord() routine                                   */
/* *****************************************************************/
static int DeleteOldRecord(char *pcpUrno)
{
  int ilRc = RC_FAIL;
  char pclDBSelection[L_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclTmpSqlAnswer[S_BUFF];

  if (*prgCfg.db_write != 'Y' && *prgCfg.db_write != 'y')
  {	
     dbg(DEBUG,"DeleteOldRecord: DB_WRITE != \'Y'. Not deleting!");
     ilRc = RC_SUCCESS;
  }
  else
  {
     sprintf(pclDBSelection,"WHERE URNO = %s",pcpUrno);
     ilRc=SendEvent("DRT",pclDBSelection,"","",NULL,0,igModID_Router,igSecondQueue,
                    PRIORITY_3,"CHA","BHSIF-DELETE","");
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"DeleteOldRecord: DELETE from CHA%s failed with <%d>.",pcgTabEnd,ilRc);
     }
  }
  return ilRc;
}
/* *****************************************************************/
/* The SetNew_CAI_Data() routine                                   */
/* *****************************************************************/
static int SetNew_CAI_Data(CAI_DB *prpCaiInt)
{
  int i = 0;
  int ilRc = RC_FAIL;
  int ilLen = 0;
  char pclSqlBuf[XL_BUFF];
  char pclTmpSqlAnswer[S_BUFF];
  char pclTmpBuff[XL_BUFF];

  if (*prgCfg.db_write != 'Y' && *prgCfg.db_write != 'y')
  {	
     dbg(DEBUG,"SetNew_CAI_Data: DB_WRITE != \'Y'. Not inserting!");
     ilRc = RC_SUCCESS;
  }
  else
  {
     /***************************************************/
     /* formatting the datalist for the sql_if - insert */
     /***************************************************/
     memset(pclTmpBuff,0x00,XL_BUFF);
     for (i=1;i<=(int)GetNoOfElements(pcgChaInsertFields,',');i++)
     {
        memset(pclTmpSqlAnswer,0x00,S_BUFF);
        GetDataItem(pclTmpSqlAnswer,(char*)prpCaiInt,i,'\0',"","");
        /*strncat(pclTmpBuff,"'",1);*/
        strcat(pclTmpBuff,pclTmpSqlAnswer);
        /*strncat(pclTmpBuff,"'",1);*/
        strncat(pclTmpBuff,",",1);
     }
     pclTmpBuff[strlen(pclTmpBuff)-1] = 0x00;

     ilRc=SendEvent("IRT","",pcgChaInsertFields,pclTmpBuff,NULL,0,igSeqWriteQueue,
                    mod_id,PRIORITY_3,"CHA","BHSIF-INSERT","");
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"SetNew_CAI_Data: INSERT into CHA%s failed with <%d>.",pcgTabEnd,ilRc);
     }
  }
  return ilRc;
}
/* *****************************************************************/
/* The Process_CST_Telegramm() routine                             */
/* Handles chute status messages from BHS-system                   */
/* *****************************************************************/
static int Process_CST_Telegramm(char *pcpData)
{
  int ilRc = RC_SUCCESS;
  int ilLen;
  CST_IN *prlCstExt = NULL;
  CST_IN_T *prlCstExtT = NULL;
  CST_DB *prlCstInt = NULL;
  char *pclDBFields = NULL;
  char *pclDBData = NULL;
  char pclTimeBuff[14];
  char pclFlno[XS_BUFF];
  char pclDate[XS_BUFF];
  char pclTmpBuff[100];

  prlCstExt = (CST_IN*)pcpData;
  prlCstExtT = (CST_IN_T*)pcpData;
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     dbg(DEBUG,"length: <%d>",strlen((char*)prlCstExt));
     ilLen = strlen((char*)prlCstExt);
  }
  else
  {
     dbg(DEBUG,"length: <%d>",strlen((char*)prlCstExtT));
     ilLen = strlen((char*)prlCstExtT);
  }
  if (igCAI_ShortLength != 0)
  { /* 20060808 JIM: Check if telex 1 char to short */
     if (ilLen == igCAI_ShortLength)
     { /* 20060808 JIM: probably 'class' was not part of ICD */
        if (prgCfg.ilReceiveTransitInfo == FALSE)
        {
           dbg(TRACE,"CAI short length: <%d>, adding class",strlen((char*)prlCstExt));
           strcpy(pclTmpBuff,(char*)prlCstExt->class);
           strcpy(prlCstExt->flno,pclTmpBuff);
           *prlCstExt->class='*';
        }
        else
        {
           dbg(TRACE,"CAI short length: <%d>, adding class",strlen((char*)prlCstExtT));
           strcpy(pclTmpBuff,(char*)prlCstExtT->class);
           strcpy(prlCstExtT->flno,pclTmpBuff);
           *prlCstExtT->class='*';
        }
     }
  }

  memset(pclTimeBuff,0x00,14);
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     strncpy(pclTimeBuff,(char*)&pcpData[sizeof(CST_IN)],9);
     dbg(DEBUG,"CST: received telegram at <%s>!",pclTimeBuff);
     snapit((char*)prlCstExt,sizeof(CST_IN),outp);
  }
  else
  {
     strncpy(pclTimeBuff,(char*)&pcpData[sizeof(CST_IN_T)],9);
     dbg(DEBUG,"CST: received telegram at <%s>!",pclTimeBuff);
     snapit((char*)prlCstExtT,sizeof(CST_IN_T),outp);
  }

  if ((ilRc = Build_CST_Data(pcpData,&prlCstInt)) != RC_SUCCESS)
  {
     dbg(TRACE,"CST: Build_CST_Data() failed!");
  }
  else
  {
     if ((ilRc = SetNew_CST_Data(prlCstInt)) != RC_SUCCESS)
     {
        dbg(TRACE,"CST: SetNew_CST_Data() failed!");	
     }
  }
  return ilRc;
}
/* *****************************************************************/
/* The Build_CST_Data() routine                                    */
/* *****************************************************************/
static int Build_CST_Data(char *pcpData,CST_DB **prpCstInt)
{
  int ilRc = RC_FAIL;
  CST_IN *prlCstExt = NULL;
  CST_IN_T *prlCstExtT = NULL;
  static CST_DB rlCstInt;
  char pclDBSelection[L_BUFF];
  char *pclDBFields = NULL;
  char *pclDBData = NULL;
  char pclFlno[XS_BUFF];
  char pclDate[XS_BUFF];
  char pclDest[XS_BUFF];
  char pclTmpBuff[XS_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclTmpSqlAnswer[S_BUFF];
  short slFkt = 0;
  short slCursor = 0;

  prlCstExt = (CST_IN*)pcpData;
  prlCstExtT = (CST_IN_T*)pcpData;
  memset((char*)&rlCstInt,0x00,sizeof(CST_DB));
  memset(pclFlno,0x00,XS_BUFF);
  memset(pclDate,0x00,XS_BUFF);
  if (prgCfg.ilReceiveTransitInfo == FALSE)
  {
     strncpy(pclFlno,prlCstExt->flno,8);
     strncpy(pclDate,prlCstExt->stod,12);
  }
  else
  {
     strncpy(pclFlno,prlCstExtT->flno,8);
     strncpy(pclDate,prlCstExtT->stod,12);
  }
  dbg(DEBUG,"CST: FLNO=<%s>",pclFlno);
  MakeIntFlno(pclFlno);
  TrimRight(pclFlno);
  dbg(DEBUG,"CST: FLNO=<%s>",pclFlno);
  dbg(DEBUG,"CST: STOD=<%s>",pclDate);

  if (bgUseHopo == TRUE)
  {
     sprintf(pclDBSelection,"WHERE HOPO='%s' AND FLNO='%s' AND STOD LIKE '%s",
             pcgHomeAp,pclFlno,pclDate);
     strcat(pclDBSelection,"%' ");
     strcat(pclDBSelection,prgCfg.db_filter);
  }
  else
  {
     sprintf(pclDBSelection,"WHERE FLNO='%s' AND STOD LIKE '%s",pclFlno,pclDate);
     strcat(pclDBSelection,"%' ");
     strcat(pclDBSelection,prgCfg.db_filter);
  }
  /* setting field list for request */
  pclDBFields = "URNO,FTYP";
  /* setting data list for request */
  pclDBData = " ";
  sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pclDBFields,pcgTabEnd,pclDBSelection);
  slFkt = START; 
  slCursor = 0;
  memset(pclTmpSqlAnswer,0x00,S_BUFF);
  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
  {
     dbg(TRACE,"Build_CST_Data: sql_if() failed with <%d>",ilRc);
  }
  else
  {
     tool_filter_spaces(pclTmpSqlAnswer); 
     GetDataItem(rlCstInt.rurn,pclTmpSqlAnswer,1,'\0',"","");
     dbg(DEBUG,"Build_CST_Data: RURN=<%s>",rlCstInt.rurn);
     GetDataItem(rlCstInt.ftyp,pclTmpSqlAnswer,2,'\0',"","");
     dbg(DEBUG,"Build_CST_Data: FTYP=<%s>",rlCstInt.ftyp);
  }
  close_my_cursor(&slCursor);

  if (ilRc == RC_SUCCESS)
  {
     /*--------------------------------------------------------*/
     /* copy/reformat external chute open info's to internal format */
     /*--------------------------------------------------------*/
     memset(pclDate,0x00,XS_BUFF);
     TimeToStr(pclDate,0,0);
     strncpy(rlCstInt.lstu,pclDate,14);
     dbg(DEBUG,"Build_CST_Data: LSTU=<%s>",rlCstInt.lstu);
     if (prgCfg.ilReceiveTransitInfo == FALSE)
     {
        strncpy(rlCstInt.chute,prlCstExt->chute,3);
        strncpy(rlCstInt.tifb,prlCstExt->o_tim,12);
        strncpy(rlCstInt.tife,prlCstExt->c_tim,12);
     }
     else
     {
        strncpy(rlCstInt.chute,prlCstExtT->chute,3);
        strncpy(rlCstInt.tifb,prlCstExtT->o_tim,12);
        strncpy(rlCstInt.tife,prlCstExtT->c_tim,12);
     }
     dbg(DEBUG,"Build_CST_Data: CHUTE=<%s>",rlCstInt.chute);
     dbg(DEBUG,"Build_CST_Data: TIFB=<%s>",rlCstInt.tifb);
     dbg(DEBUG,"Build_CST_Data: TIFE=<%s>",rlCstInt.tife);
     strncpy(rlCstInt.tisb,"A",1);
     dbg(DEBUG,"Build_CST_Data: TISB=<%s>",rlCstInt.tisb);
     strncpy(rlCstInt.tise,"A",1);
     dbg(DEBUG,"Build_CST_Data: TISE=<%s>",rlCstInt.tise);
     *prpCstInt = &rlCstInt;
  }
  return ilRc;
}
/* *****************************************************************/
/* The SetNew_CST_Data() routine                                   */
/* *****************************************************************/
static int SetNew_CST_Data(CST_DB *prpCstInt)
{
  int i = 0;
  int ilRc = RC_FAIL;
  int ilLen = 0;
  char pclDBSelection[L_BUFF];
  char pclSqlBuf[XL_BUFF];
  char pclTmpSqlAnswer[S_BUFF];
  char pclTmpBuff[XL_BUFF];
  char	*pclChaUpdateFields = NULL;

  if (*prgCfg.db_write != 'Y' && *prgCfg.db_write != 'y')
  {	
     dbg(DEBUG,"SetNew_CST_Data: DB_WRITE != \'Y'. Not updating!");
     ilRc = RC_SUCCESS;
  }
  else
  {
     memset(pclDBSelection,0x00,L_BUFF);
     if (bgUseHopo == TRUE)
     {
        sprintf(pclDBSelection,"WHERE RURN=%s AND LNAM='%s' AND HOPO='%s'",
                prpCstInt->rurn,prpCstInt->chute,pcgHomeAp);
     }
     else
     {
        sprintf(pclDBSelection,"WHERE RURN=%s AND LNAM='%s'",
                prpCstInt->rurn,prpCstInt->chute);
     }

     str_trm_all(prpCstInt->tifb," ",TRUE);
     if (strlen(prpCstInt->tifb) < 1)
     {
        pclChaUpdateFields = "LSTU,TIFE,TISE";
        sprintf(pclTmpBuff,"%s,%s,%s",prpCstInt->lstu,prpCstInt->tife,prpCstInt->tise);
     }

     str_trm_all(prpCstInt->tife," ",TRUE);
     if (strlen(prpCstInt->tife) < 1)
     {
        pclChaUpdateFields = "LSTU,TIFB,TISB";
        sprintf(pclTmpBuff,"%s,%s,%s",prpCstInt->lstu,prpCstInt->tifb,prpCstInt->tisb);
     }

     if (strlen(prpCstInt->tifb) > 0 && strlen(prpCstInt->tife) > 0)
     {
        pclChaUpdateFields = "LSTU,TIFB,TIFE,TISB,TISE";
        sprintf(pclTmpBuff,"%s,%s,%s,%s,%s",
                prpCstInt->lstu,prpCstInt->tifb,prpCstInt->tife,prpCstInt->tisb,prpCstInt->tise);
     }

     ilRc=SendEvent("URT",pclDBSelection,pclChaUpdateFields,pclTmpBuff,NULL,0,igSeqWriteQueue,
                    mod_id,PRIORITY_3,"CHA","BHSIF-UPDATE","");
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"SetNew_CST_Data: UPDATE on CHA%s failed with <%d>.",pcgTabEnd,ilRc);
     }
  }
  return ilRc;
}
/* ******************************************************************** */
/* The StrToTime() routine                                              */
/* Format of Date has to be CEDA-Format - 14 Byte                       */
/* ******************************************************************** */
static int StrToTime(char *pcpTime,time_t *plpTime)
{
  struct tm *_tm;
  time_t now;
  char  _tmpc[6];

  /*dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
  if (strlen(pcpTime) < 12 )
  {
     *plpTime = time(0L);
     return RC_FAIL;
  } 

  now = time(0L);
  _tm = (struct tm *)localtime(&now);
  _tmpc[2] = '\0';
  _tm -> tm_sec = 0;
  strncpy(_tmpc,pcpTime+10,2);
  _tm -> tm_min = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+8,2);
  _tm -> tm_hour = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+6,2);
  _tm -> tm_mday = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+4,2);
  _tm -> tm_mon = atoi(_tmpc)-1;
  strncpy(_tmpc,pcpTime,4);
  _tmpc[4] = '\0';
  _tm -> tm_year = atoi(_tmpc)-1900;
  _tm -> tm_wday = 0;
  _tm -> tm_yday = 0;
  now = mktime(_tm);
  if (now != (time_t) -1)
  {
     *plpTime = now;
     return RC_SUCCESS;
  }
  *plpTime = time(NULL);
  return RC_FAIL;
}
/******************************************************************************/
/* The GetViaByNumber routine                                                 */
/******************************************************************************/
static char *GetViaByNumber(char *pcpVial,int ipViaNo,int ipVia34)
{
  static char	pclVia[iMIN];

  if (pcpVial == NULL || !strlen(pcpVial) || ipViaNo < 1)
     return NULL;

  memset((void*)pclVia, 0x00, iMIN);
  if (ipVia34 == 3)
     strncpy(pclVia, pcpVial+(1+((ipViaNo-1)*120)),3);
  else
     strncpy(pclVia, pcpVial+(4+((ipViaNo-1)*120)),4);
  return pclVia;
}
/* ******************************************************************** */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
static int MakeSendBuffer(int ipGetNr,int ipCmd,char *pcpDataPointer,char *pcpSendBuffer)
{
  int ilRc = RC_FAIL;
  int ilDataLen = 0;
  int ilSendBufferSize = 0;
  HEAD pclHead;
  TAIL pclTail;
	
  /* HEADER */
  memset((char*)&pclHead,0x00,sizeof(HEAD));
  *(pclHead.start) = SOM;
  *(pclHead.cycle_no) = '0';
  sprintf(pclHead.t_type,"%02d",ipCmd);
  strncpy(pclHead.sender,prgCfg.my_sys_id,2);
  strncpy(pclHead.receiver,prgCfg.remote_sys_id,2);
  if (ipGetNr == FALSE)
  {
     strncpy(pclHead.order_no,"01",2);
  }
  else
  {
     GetMsgNumber(pclHead.order_no);
  }

  /* TAIL */
  memset((char*)&pclTail,0x00,sizeof(TAIL));
  GetMsgTimeStamp(pclTail.time);
  strncpy(pclTail.tail,prgEnd.tail,4);

  ilDataLen = (int)strlen(pcpDataPointer);
  
  ilSendBufferSize = igHeadLen+ilDataLen+igTailLen;

  if (ilSendBufferSize < MAX_TELEGRAM_LEN)
  {
     memcpy((char*)pcpSendBuffer,(char*)&pclHead,igHeadLen);
     memcpy((char*)&pcpSendBuffer[igHeadLen],pcpDataPointer,ilDataLen);
     memcpy((char*)&pcpSendBuffer[igHeadLen+ilDataLen],(char*)&pclTail,igTailLen);
     pcpSendBuffer[ilSendBufferSize] = 0x00;
     /*dbg(DEBUG,"MSB: <%s>",pcpSendBuffer);*/
     ilRc = RC_SUCCESS;
  }
  else
  {
     dbg(TRACE,"MSB: Buffer to BIG! Can't send!");
  }
  return ilRc;
}
/* ******************************************************************** */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
static int CheckFlight(char *pcpData,char *pcpFieldList,int ipActionType,int *ipType)
{
  int ilRc = RC_FAIL;
  char pclToday[20];
  char pclDate[20];
  time_t tlTifd = 0;
  time_t tlToday = 0;
  time_t tlOffsDay = 0;
  FI_INT rlFiInt;

  memset(&rlFiInt,0x00,sizeof(FI_INT));
  get_real_item(rlFiInt.tifd,pcpData,1);
  dbg(DEBUG,"CheckFlight: TIFD=(%s)",rlFiInt.tifd); 	
  get_real_item(rlFiInt.adid,pcpData,2);
  dbg(DEBUG,"CheckFlight: ADID=(%s)",rlFiInt.adid); 	
  get_real_item(rlFiInt.ftyp,pcpData,3);
  dbg(DEBUG,"CheckFlight: FTYP=(%s)",rlFiInt.ftyp); 	

  if (strncmp(rlFiInt.adid,"D",1)!=0 && strncmp(rlFiInt.adid,"B",1)!=0)  
  {
     dbg(DEBUG,"CheckFlight: Arrival flight! Ignoring flight!");
  }
  else
  {
     if (ipActionType==SFU || ipActionType==SFI)
     {
        if (*rlFiInt.ftyp=='O' || *rlFiInt.ftyp=='R')
        {
           *ipType = SFU;	
           ilRc = RC_SUCCESS;
           dbg(DEBUG,"CheckFlight: Flight operational! Sending SFU!"); 
        }
        else
        {
           dbg(DEBUG,"CheckFlight: Flight NOT operational! Discard sending!"); 
        }
     }

     if (ipActionType==CXX)
     {
        if (igTerminalChange == TRUE)
        {
           *ipType = CXX;	
           ilRc = RC_SUCCESS;
           dbg(DEBUG,"CheckFlight: Flight NOT anymore in Terminal List! Sending CXX!"); 
        }
        else if (*rlFiInt.ftyp=='O' || *rlFiInt.ftyp=='R')
        {
           *ipType = SFU;	
           ilRc = RC_SUCCESS;
           dbg(DEBUG,"CheckFlight: Flight NOW operational! Sending SFU!"); 
        }
        else
        {
           *ipType = CXX;	
           ilRc = RC_SUCCESS;
           dbg(DEBUG,"CheckFlight: Flight NOT anymore operational! Sending CXX!"); 
        }
     }

     if (ilRc == RC_SUCCESS)
     {
        memset(pclDate,0x00,10);
        strncpy(pclDate,rlFiInt.tifd,8);
        dbg(DEBUG,"CheckFlight: check flight-date <%s>.",pclDate); 	
        if ((ilRc = CheckValue("SFU-TIFD",pclDate,CFG_NUM)) == RC_SUCCESS)
        {
           strcat(pclDate,"000010");
           StrToTime(pclDate,&tlTifd);

           memset(pclToday,0x00,sizeof(pclToday));
           TimeToStr(pclToday,0,0);
           pclToday[8]=0x00;
           strcat(pclToday,"000010");
           StrToTime(pclToday,&tlToday);
				
           tlOffsDay = tlToday + (SECONDS_PER_DAY * atol(prgCfg.fs_offset));
           if ((tlTifd >= tlToday) && (tlTifd <= tlOffsDay))
           {
              ilRc = RC_SUCCESS;
           }
           else
           {
              dbg(DEBUG,"CheckFlight: Date outside range! Not processing!"); 	
              ilRc = RC_FAIL;
           }
        }
        else
        {
           dbg(TRACE,"CheckFlight: invalid TIFD! Can't process!"); 	
        }
     }
  }
  return ilRc;	
}
/* ******************************************************************** */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
static int CreateFlightTables()
{
  int ilRc = RC_SUCCESS;
  int ilCnt = 0;
  time_t tlToday;
  time_t tlDayToSend;
  char pclToday[20];
  char pclSendDate[20];

  memset(pclToday,0x00,sizeof(pclToday));
  TimeToStr(pclToday,0,0);
  pclToday[8]=0x00;
  strcat(pclToday,"000010");
  StrToTime(pclToday,&tlToday);

  for (ilCnt=0;ilCnt<atoi(prgCfg.fs_offset) && ilRc==RC_SUCCESS;ilCnt++)
  {
     tlDayToSend = tlToday + (SECONDS_PER_DAY * ilCnt);
     memset(pclSendDate,0x00,sizeof(pclSendDate));
     TimeToStr(pclSendDate,tlDayToSend,1);
     if ((ilRc = PrepareData(SFS,"",tlDayToSend)) != RC_SUCCESS)
     {
        dbg(TRACE,"CFT: requesting data for <%s> failed!",pclSendDate); 	
     }
     ilRc = RC_SUCCESS;
  }
  if (ilRc == RC_FAIL)
  {
     dbg(TRACE,"CFT: Error during request creation!");
  }
  return ilRc;
}
/* ******************************************************************** */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
static int SendFileViaFtp(char *pcpHost,char *pcpFile)
{
  int ilRc = RC_SUCCESS;
  char pclRemoteFile[S_BUFF];
  char pclRemoteStamp[S_BUFF];
  char pclLocalStamp[S_BUFF];
  time_t tlDayToSend = 0;
	
  memset(pclLocalStamp,0x00,S_BUFF);

  strncpy(pclLocalStamp,pcpFile,8);
  dbg(DEBUG,"SFVF: call FTPHDL for <%s> to host <%s>.",pcpFile,pcpHost);
  if ((ilRc = CheckValue("SFVF:",pclLocalStamp,CFG_NUM))==RC_SUCCESS)
  {
     strcat(pclLocalStamp,"000010");
     StrToTime(pclLocalStamp,&tlDayToSend);
     TimeToStr(pclRemoteStamp,tlDayToSend,2);
     memset(pclRemoteFile,0x00,S_BUFF);
     sprintf(pclRemoteFile,"%s.DTM",pclRemoteStamp);

     /*****************************************************/
     /* now filling the hyper-dynamic FTPConfig-structure */
     /*****************************************************/
     memset(&prgFtp,0x00,sizeof(FTPConfig));

     prgFtp.iFtpRC = FTP_SUCCESS; /* Returncode of FTP-transmission */
     strcpy(prgFtp.pcCmd,"FTP");
     strcpy(prgFtp.pcHostName,pcpHost);
     strcpy(prgFtp.pcUser,prgCfg.ftp_user);
     strcpy(prgFtp.pcPasswd,prgCfg.ftp_pass);
     prgFtp.cTransferType = CEDA_BINARY;
     prgFtp.iTimeout = igFtpTimeout; /* for tcp_waittimeout */
     prgFtp.lGetFileTimer = igFtpTimeout; /* timeout (seconds) for receiving data */
     prgFtp.lReceiveTimer = igFtpTimeout; /* timeout (seconds) for receiving data */
     prgFtp.iDebugLevel = DEBUG; /* Debugging level of FTPHDL during operation */
     if (strcmp(prgCfg.ftp_client_os,"WIN") == 0)
     {
        prgFtp.iClientOS = OS_WIN; /* Operating system of client */
     }
     if (strcmp(prgCfg.ftp_client_os,"UNIX") == 0)
     {
        prgFtp.iClientOS = OS_UNIX; /* Operating system of client */
     }
     prgFtp.iServerOS = OS_UNIX; /* Operating system of server */
     prgFtp.iRetryCounter = 0; /* number of retries */
     prgFtp.iDeleteRemoteSourceFile = 0; /* yes=1 no=0 */
     prgFtp.iDeleteLocalSourceFile = 0; /* yes=1 no=0 */
     prgFtp.iSectionType = iSEND; /* iSEND or iRECEIVE */
     prgFtp.iInvalidOffset = 0; /* time (min.) the section will be set invalid, after unsuccessfull retry */
     prgFtp.iTryAgainOffset = 0; /* time between retries */
     strcpy(prgFtp.pcHomeAirport,pcgHomeAp); /* HomeAirport */
     strcpy(prgFtp.pcTableExtension,pcgTabEnd); /* Table extension */

     /* local path for FTP */
     strcpy(prgFtp.pcLocalFilePath,prgCfg.fs_local_path);
     /* local filename for FTP */
     strcpy(prgFtp.pcLocalFileName,pcpFile);
     /* local filename after successfull FTP-transmission */
     if (strcmp(prgCfg.after_ftp,"R") == 0)
     {
        sprintf(prgFtp.pcRenameLocalFile,"%s_send",pcpFile);
     }

     /* remote path for FTP */
     strcpy(prgFtp.pcRemoteFilePath,prgCfg.fs_remote_path);
     /* remote filename during FTP-transmission */
     strcpy(prgFtp.pcRemoteFileName,pclRemoteFile);
     /* remote filename after successfull FTP-transmission */
     sprintf(prgFtp.pcRenameRemoteFile,"%s.TBL",pclRemoteStamp);

     if (strcmp(prgCfg.after_ftp,"D") == 0)
     {
        prgFtp.iDeleteLocalSourceFile = 1; /* files will be deleted */
     }
     if (strcmp(prgCfg.after_ftp,"K") == 0)
     {
        prgFtp.iDeleteLocalSourceFile = 0; /* files will not be deleted */
     }
     prgFtp.iSendAnswer = 1; /* yes=1 no=0 */ 
     prgFtp.cStructureCode = CEDA_FILE;
     prgFtp.cTransferMode = CEDA_STREAM;
     prgFtp.iStoreType = CEDA_CREATE;
     prgFtp.data[0] = 0x00;

     ilRc = SendEvent("FTP"," "," ","DYN",(char*)&prgFtp,
                      sizeof(FTPConfig),igModID_Ftphdl,mod_id,PRIORITY_3,"","","");
     if (ilRc != RC_SUCCESS)
     {
        dbg(TRACE,"SFVF: SendEvent() to <%d> returns <%d>!",igModID_Ftphdl,ilRc);
     }
  }
  else
  {
     dbg(TRACE,"SFVF: invalid filename-format! Can't send!");
  }
  return ilRc;
}
/* ******************************************************************** */
/* The MakeIntFlno routine                                              */
/* ******************************************************************** */
static int MakeIntFlno(char *pcpFlno)
{
  int ilCnt = 0;
  int ilAlcCnt = 0;
  int ilFnrCnt = 0;
  int ilBreak = FALSE;
  int ilSufCnt = 0;
  int ilRC = RC_FAIL;
  int ilLen = 0;
  int ilPos = 0;
  char pclTmpFlno[16];
  char pclAlc[10];
  char pclFnr[10];
  char pclTmpFnr[10];
  char pclTmpBuf[10];
  char pclSuffix[10];

  memset(pclAlc,0x00,10);
  memset(pclFnr,0x00,10);
  memset(pclTmpFnr,0x00,10);
  memset(pclTmpBuf,0x00,10);
  memset(pclSuffix,0x00,10);

  str_trm_all(pcpFlno," ",TRUE);
  ilLen = strlen(pcpFlno);
  if (ilLen > 3 && ilLen < 10)
  {
     do
     {
        /* getting the airline */
        if (ilCnt<3 && (isalpha(pcpFlno[ilCnt])!=0 || isdigit(pcpFlno[ilCnt])!=0))
        {
           pclAlc[ilAlcCnt] = pcpFlno[ilCnt];	
           ilAlcCnt++;
        }
        /* getting the flight-nr */
        if (ilCnt>1 && isdigit(pcpFlno[ilCnt])!=0)
        {
           pclFnr[ilFnrCnt] = pcpFlno[ilCnt];	
           ilFnrCnt++;
        }
        /* getting the suffix */
        if (ilCnt>3 && isdigit(pcpFlno[ilCnt])==0)
        {
           pclSuffix[ilSufCnt] = pcpFlno[ilCnt];	
           ilSufCnt++;
        }
        ilCnt++;
     } while(ilCnt < ilLen);
     /* 20070117 JIM: corrected ALC2 in FLNO */
     if ((strlen(pclAlc) == 3) && isdigit(pcpFlno[2])!=0)
     {
        pcpFlno[2]=' ';
     }
     dbg(DEBUG,"MakeIntFlno: AIRLINE:  = <%s><%d>",pclAlc,strlen(pclAlc));
     if ((strlen(pclAlc) == 3) && pclAlc[2]>='0' && pclAlc[2]<='9' )
     {
        pclAlc[2]= 0;
     }
     dbg(DEBUG,"MakeIntFlno: AIRLINE:  = <%s>",pclAlc);
     /* stripping leading zeros */
     ilCnt=0;
     do
     {
        if (pclFnr[ilCnt]!='0')
        {
           strcpy(pclTmpBuf,&pclFnr[ilCnt]);
           ilBreak = TRUE;	
        }
        ilCnt++;	
     } while(ilBreak==FALSE&&ilCnt<strlen(pclFnr));
     strcpy(pclTmpFnr,"00000");
     ilPos = 3 - strlen(pclTmpBuf);
     if (ilPos < 0)
     {
        ilPos = 0;
     } /* end if */
     strcpy(&pclTmpFnr[ilPos],pclTmpBuf);
     dbg(DEBUG,"MakeIntFlno: FLIGHT-NR = <%s>",pclTmpFnr);
     str_trm_all(pclSuffix," ",TRUE);
     dbg(DEBUG,"MakeIntFlno: SUFFIX    = <%s>",pclSuffix);
     if (strlen(pclAlc) > 3)
     {
        dbg(TRACE,"MakeIntFlno: Flight-Airline-code to big!");
     }
     else
     {
        if (strlen(pclAlc) <= 2)
        {
           strcat(pclAlc," ");
        }
        memset(pclTmpFlno,0x00,16);
        memset(pclTmpFlno,0x20,9);
        /*strcpy(pclTmpFlno,pclAlc);*/
        memcpy((char*)&pclTmpFlno[0],pclAlc,3);
        if (strlen(pclTmpFnr) > 5)
        {
           dbg(TRACE,"MakeIntFlno: Flight-Nr to big!");
        }
        else
        {
           /*strcat(pclTmpFlno,pclTmpFnr);*/
           memcpy((char*)&pclTmpFlno[3],pclTmpFnr,strlen(pclTmpFnr));
           ilRC = RC_SUCCESS;
           if (strlen(pclSuffix) > 1)
           {
              dbg(TRACE,"MakeIntFlno: Flight-Suffix to big!");
           }
           else
           {
              /*strcat(pclTmpFlno,pclSuffix);*/
              memcpy((char*)&pclTmpFlno[8],pclSuffix,1);
           }
           dbg(DEBUG,"MakeIntFlno: Reformatted FLNO = <%s>",pclTmpFlno);
           strcpy(pcpFlno,pclTmpFlno);
        }
     }
  }
  else
  {
     dbg(TRACE,"MakeIntFlno: FLIGHT-NR to short/big! Ignoring!");
  }
  return ilRC;
} /* end MakeIntFlno */
/* ******************************************************************** */
/* The MakeExtFlno routine                                              */
/* ******************************************************************** */
static int MakeExtFlno(char *pcpFlno)
{
  int ilCnt = 0;
  int ilAlcCnt = 0;
  int ilFnrCnt = 0;
  int ilBreak = FALSE;
  int ilSufCnt = 0;
  int ilRC = RC_FAIL;
  int ilLen = 0;
  int ilPos = 0;
  char pclTmpFlno[16];
  char pclAlc[10];
  char pclFnr[10];
  char pclTmpFnr[10];
  char pclTmpBuf[10];
  char pclSuffix[10];

  memset(pclAlc,0x00,10);
  memset(pclFnr,0x00,10);
  memset(pclTmpFnr,0x00,10);
  memset(pclTmpBuf,0x00,10);
  memset(pclSuffix,0x00,10);

  str_trm_all(pcpFlno," ",TRUE);
  ilLen = strlen(pcpFlno);
  if (ilLen > 3 && ilLen < 10)
  {
     do
     {
        /* getting the airline */
        if (ilCnt<3 && (isalpha(pcpFlno[ilCnt])!=0 || isdigit(pcpFlno[ilCnt])!=0))
        {
           pclAlc[ilAlcCnt] = pcpFlno[ilCnt];	
           ilAlcCnt++;
        }
        /* getting the flight-nr */
        if (ilCnt>1 && isdigit(pcpFlno[ilCnt])!=0)
        {
           pclFnr[ilFnrCnt] = pcpFlno[ilCnt];	
           ilFnrCnt++;
        }
        /* getting the suffix */
        if (ilCnt>3 && isdigit(pcpFlno[ilCnt])==0)
        {
           pclSuffix[ilSufCnt] = pcpFlno[ilCnt];	
           ilSufCnt++;
        }
        ilCnt++;
     } while(ilCnt < ilLen);
     /*dbg(DEBUG,"MakeExtFlno: AIRLINE   = <%s>",pclAlc);*/
     /* stripping leading zeros */
     ilCnt=0;
     do
     {
        if (pclFnr[ilCnt]!='0')
        {
           strcpy(pclTmpBuf,&pclFnr[ilCnt]);
           ilBreak = TRUE;	
        }
        ilCnt++;	
     } while(ilBreak==FALSE&&ilCnt<strlen(pclFnr));
     strcpy(pclTmpFnr,"00000");
     ilPos = 3 - strlen(pclTmpBuf);
     if (ilPos < 0)
     {
        ilPos = 0;
     } /* end if */
     strcpy(&pclTmpFnr[ilPos],pclTmpBuf);
     /*dbg(DEBUG,"MakeExtFlno: FLIGHT-NR = <%s>",pclTmpFnr);*/
     str_trm_all(pclSuffix," ",TRUE);
     /*dbg(DEBUG,"MakeExtFlno: SUFFIX    = <%s>",pclSuffix);*/
     if (strlen(pclAlc) > 3)
     {
        dbg(TRACE,"MakeExtFlno: Flight-Airline-code to big!");
     }
     else
     {
        if (prgCfg.ExtAlc2AddBlank[0]=='Y')
        {
           dbg(TRACE,"MakeExtFlno: checking ALC2");
           if (strlen(pclAlc) <= 2)
           {
              dbg(TRACE,"MakeExtFlno: adding space to ALC2");
              strcat(pclAlc," ");
           }
        }
        else if (pclAlc[2] ==' ')
        {
           dbg(TRACE,"MakeExtFlno: trim ALC2");
           pclAlc[2]= 0;
        }
        memset(pclTmpFlno,0x00,16);
        /*memset(pclTmpFlno,0x20,9);*/
        /*memcpy((char*)&pclTmpFlno[0],pclAlc,3);*/
        strcpy(pclTmpFlno,pclAlc);
        if (strlen(pclTmpFnr) > 4)
        {
           dbg(TRACE,"MakeExtFlno: Flight-Nr to big! Larger than 4 digits!");
           dbg(TRACE,"MakeExtFlno: Rejecting flight!");
        }
        else
        {
           strcat(pclTmpFlno,pclTmpFnr);
           /*memcpy((char*)&pclTmpFlno[3],pclTmpFnr,strlen(pclTmpFnr));*/
           ilRC = RC_SUCCESS;
           if (strlen(pclSuffix) > 1)
           {
              dbg(TRACE,"MakeExtFlno: Flight-Suffix to big!");
           }
           else
           {
              /*memcpy((char*)&pclTmpFlno[7],pclSuffix,1);*/
              strcat(pclTmpFlno,pclSuffix);
           }
           str_trm_all(pclTmpFlno," ",TRUE);
           dbg(DEBUG,"MakeExtFlno: Reformatted FLNO = <%s>",pclTmpFlno);
           strcpy(pcpFlno,pclTmpFlno);
           ilRC = RC_SUCCESS;
        }
     }
  }
  else
  {
     dbg(TRACE,"MakeExtFlno: FLIGHT-NR to short/big! Ignoring!");
  }
  return ilRC;
} /* end MakeExtFlno */


