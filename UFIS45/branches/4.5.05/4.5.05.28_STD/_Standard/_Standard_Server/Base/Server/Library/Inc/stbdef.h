#ifndef _DEF_mks_version_stbdef_h
  #define _DEF_mks_version_stbdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_stbdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/stbdef.h 1.2 2004/07/27 16:48:29SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/*      Definitions for 'STB' (System Table Builder       */

#ifndef __STBDEF_INC
#define __STBDEF_INC


int blkmove(char *src,char *des,int x);


#define BLANK ' '
#define COMMA ','
#define QUOTE '\042'

/*      Define error return values                         */

#define ERROPNINF   1
#define ERROPOUTF   2
#define ERRSGSNF    3
#define ERRSGSMEM   4
#define ERRDYNMEM   5
#define ERRNOFUNC   6
#define ERRNTSTR    7
#define ERRTABPARM  8
#define ERRMAXTAB   9
#define ERRTABHDR  10
#define ERRSPTSZ   11
#define ERRINCLUDE 12
#define ERRPRTDEV  13
#define ERRFSMEM   14
#define ERRNODESC  18
#define ERRSPTAB   19
#define ERRSTDFLD  20
#define ERRNOINT   21
#define ERRSTAT    22
#define ERRFTAB    23
#define ERRYESNO   25
#define ERRPARMF   26
#define ERRNOHEX   27

#define OUTRECSZ   32
#define GROUPSZ     5
#define GROUPOFFS   2

#define FLDI  1                       /* integer value                      */
#define FLDL  2                       /* long integer                       */
#define FLDS  3                       /* string                             */
#define FLDSB 4                       /* string (padded)                    */
#define FLDX  5                       /* hex field                          */
#define FLDUD 6                       /* status UP/DN                       */
#define FLDYN 7                       /* yes/no field                       */
#define FLD_  999                     /* dummy                              */

struct EMSG                /* STG error message structure    */ 
  {
  int emsgno;              /* Error number                   */
  char *emsgtxt;           /* Error text                     */
  };

struct _tabhdr             /* standard system table header   */
  {
   long t_tabno;            /* # of the table                 */
   long t_offset;           /* offset to the base address     */
   long t_tabsz;            /* size of the table              */
   long t_recsz;            /* size of the record, if a       */
                            /* record table                   */
   long t_lktime;           /* time locked                    */
   long t_dummy2;           /* for 'ssgbyte'                  */
   long t_lktask;           /* # of the task, that locked     */
                            /* the table                      */
   long t_tset;             /* when set, the header locked    */
  };

typedef struct _tabhdr TABHDR;

struct _rechdr            /* standard record header          */
  {
   long r_lktime;          /* time locked                     */
   long r_lktask;          /* # of the task, that locked      */
                           /* the record                      */
   long r_tset;            /* the header locked when set      */
  };

typedef struct _rechdr RECHDR;

struct _sthidx           /* system table index               */
  {
  int sthmax;            /* current max. table no.           */
  TABHDR sthtab[1];      /* the header of the table          */
  };

typedef struct _sthidx STHIDX;

struct _stdfld              /* standard system table field descriptor */
  {
  int std_type;             /* type of the field                      */
  int std_offs;             /* offset of the field                    */
  int std_size;             /* size of the field                      */
  };

typedef struct _stdfld STDFLD;

struct _stddesc                         /* link to field descriptors        */
  {
  int d_tabno;             /* tabel no                         */
  STDFLD *d_std;           /* pointer to field descriptions    */
  };

typedef struct _stddesc STDDESC;

struct LABEL                /* connect a label to a function           */
  {
  char *lblname;            /* pointer to the name of label            */
  int (*lblfunc)();         /* function                                */
  };

/*	T A B L E  D E S C R I P T O R S                           */


static STDFLD desc_tet[] =             /*  T E T A B                        */
  {
    { FLDS,   0,    6,  },
    { FLDI,  18,    1,  },
    { FLDS,  19,   16,  },
    { 0,   0,    0,  },
  };

static STDFLD desc_rot[] =             /*  R O T A B                       */
  {
    { FLDI,   0,    2,  },
    { FLDI,   2,    2,  },
    { FLDI,   4,    2,  },
    { FLDI,   6,    2,  },
    { FLDI,   8,    2,  },
    { FLDI,  10,    2,  },
    { FLDI,  12,    2,  },
    { FLDI,  14,    2,  },
    { FLDI,  16,    2,  },
    { FLDI,  18,    2,  },
    { FLDI,  20,    2,  },
    { FLDI,  22,    2,  },
    { FLDI,  24,    2,  },
    { FLDI,  26,    2,  },
    { FLDI,  28,    2,  },
    { FLDI,  30,    2,  },
    { FLDI,  32,    2,  },
    { 0,   0,    0,  },
  };

static STDFLD desc_pnt[] =             /*  P N T A B                        */
  {
    { FLDS,   0,    8,  },
    { FLDI,   8,    4,  },
    { FLDI,  12,    4,  },
    { FLDI,  16,    2,  },
    { FLDI,  18,    2,  },
    { FLDI,  20,    2,  },
    { FLDI,  22,    2,  },
    { FLDI,  24,    2,  },
    { 0,   0,    0,  },
  };

static STDFLD desc_act[] =             /*  A C T A B                        */
  {
    { FLDS,   0,    3,  },		/* ACT3 */
    { FLDS,   3,    4,  },		/* ACT4 */
    { 0,   0,    0,  },
  };

static STDFLD desc_apt[] =             /*  A P T A B                        */
  {
    { FLDS,   0,    3,  },		/* APC3 */
    { 0,   0,    0,  },
  };

static STDFLD desc_bct[] =             /*  B C T A B                        */
  {
    { FLDSB,   0,    1024,  },		/* RAW */
    { 0,   0,    0,  },
  };

static STDFLD desc_ckt[] =             /*  C K T A B                        */
  {
    { FLDS,   0,    6,  },		/* TableName */
    { FLDSB,   6,   60,  },		/* check statement */
    { 0,   0,    0,  },
  };

static STDFLD desc_bd[] =              /*  B D T A B                        */
  {
    { FLDS,   0,    5,  },		/* STB TableName */
    { FLDS,   5,    6,  },		/* DB TableName */
    { FLDSB,  11,   80,  },		/* field list */
    { FLDSB,  91,   37,  },		/* WHERE statement */
    { 0,   0,    0,  },
  };

static STDFLD desc_al[] =              /*  A L T A B                        */
  {
    { FLDS,   0,    2,  },		/* ALC2 */
    { FLDS,   2,    3,  },		/* ALC3 */
    { 0,   0,    0,  },
  };

static STDFLD desc_de[] =              /*  D E T A B - Delaycodes           */
  {
    { FLDS,   0,    2,  },		/* DECN */
    { FLDS,   2,    2,  },		/* DECA */
    { 0,   0,    0,  },
  };

static STDFLD desc_ar[] =              /*  A R T A B - Aircrafts           */
  {
    { FLDS,   0,    5,  },		/* REGN */
    { FLDS,   5,    3,  },		/* ACT3 */
    { FLDS,   8,    4,  },		/* MING */
    { FLDS,  12,    4,  },		/* MAXG */
    { 0,   0,    0,  },
  };

static STDFLD desc_ga[] =              /*  G A T A B - Gates           */
  {
    { FLDS,   0,    4,  },		/* GNAM */
    { 0,   0,    0,  },
  };

static STDFLD desc_na[] =              /*  N A T A B - Natures           */
  {
    { FLDS,   0,    2,  },		/* FNAT */
    { 0,   0,    0,  },
  };

static STDFLD desc_ps[] =              /*  P S T A B - Positions           */
  {
    { FLDS,   0,    4,  },		/* PNAM */
    { FLDS,   4,    4,  },		/* PDPO */
    { FLDS,   8,    1,  },		/* PCAT */
    { 0,   0,    0,  },
  };

static STDFLD desc_hg[] =              /*  H G T A B - Positions           */
  {
    { FLDS,   0,    3,  },	       /* HAC3 */
    { 0,   0,    0,  },
  };

static STDFLD desc_se[] =              /*  S E T A B - Positions           */
  {
    { FLDS,   0,    6,  },	       /* SEAS */
    { 0,   0,    0,  },
  };

static STDFLD desc_alias[] =           /*  ALIAS TABLE           */
  {
    { FLDS,   0,    4,  },	       /* ALIAS */
    { FLDS,   4,    64,  },	       /* Alias translation */
    { 0,   0,    0,  },
  };

static STDFLD desc_timct[] =           /*  TIME SCHEDULER CONFIG TABLE */
  {
    { FLDI,   0,    2,  },	       /* DAY_OF_WEEK */
    { FLDI,   2,    2,  },	       /* HOUR_OF_DAY */
    { FLDI,   4,    2,  },	       /* MIN_OF_HOUR */
    { FLDI,   6,    2,  },	       /* SEC_OF_MIN  */
    { FLDI,   8,    2,  },	       /* No of Exec Times  */
    { FLDI,   10,    2,  },	       /* Abs. or Rel Flag  */
    { FLDI,   12,    2,  },	       /* Queue Id   */
    { FLDI,   14,    2,  },	       /* Command no   */
    { 0,   0,    0,  },
  };

static STDFLD desc_actct[] =           /*  ACTION SCHEDULER CONFIG TABLE */
  {
    { FLDS,   0,    6,  },	       /* Tablename  TANA */
    { FLDS,   6,    4,  },	       /* Fieldname  FINA */
    { FLDI,   10,    2,  },	       /* Queue Id   */
    { FLDI,   12,    2,  },	       /* Command no   */
    { 0,   0,    0,  },
  };

static STDFLD desc_stidx[] =           /*  STATUS INDEX TABLE */
  {
    { FLDS,   0,    10,  },	       /* Index Name  */
    { FLDI,   10,    2,  },	       /* Index Number */
    { FLDS,   12,    4,  },	       /* Initial Field Name  */
    { FLDS,   16,    4,  },	       /* Runtime Field Name  */
    { 0,   0,    0,  },
  };

static STDFLD desc_cflct[] =           /*  CONFLICT CONFIGURATION TABLE */
  {
    { FLDS,   0,    2,  },	       /* Cflchk Command  */
    { FLDS,   2,    14,  },	       /* Object Identification */
    { FLDI,   16,    2,  },	       /* Time Offset in Minutes */
    { FLDS,   18,    10,  },	       /* Index Name  */
    { FLDI,   28,    2,  },	       /* Conflict Number in MSGTAB */
    { 0,   0,    0,  },
  };

static STDFLD desc_c1[] =              /*  Command TABLE */
  {
    { FLDS,   0,    5,  },	       /* Command  */
    { FLDI,   5,    4,  },	       /* Queu ID */
    { 0,   0,    0,  },
  };

static STDFLD desc_c2[] =              /*  Command TABLE 2 */
  {
    { FLDS,   0,    6,  },	       /* DB-Table name  */
    { FLDS,   6,    5,  },	       /* Command  */
    { FLDI,   11,   4,  },	       /* Queue ID */
    { 0,   0,    0,  },
  };

static STDFLD desc_nm[] =              /*  Name Server Tab           */
  {
    { FLDS,   0,    8,  },	       /* Type */
    { FLDS,   8,    8,  },	       /* Host */
    { 0,   0,    0,  },
  };

static STDFLD desc_mt[] =              /*  MTSHDL Tab           */
  {
    { FLDS,   0,    8,  },	       /* Key */
    { FLDS,   8,    64,  },	       /* Data */
    { 0,   0,    0,  },
  };

static STDFLD desc_gen[] =              /*  EXCO Tab           */
  {
    { FLDS,   0,    4,  },	       /* Process Code */
    { FLDS,   4,    8,  },	       /* Key */
    { FLDS,   12,   68,  },	       /* Data */
    { 0,   0,    0,  },
  };

static STDFLD desc_hsb[] =              /*  HSB Tab           */
  {
    { FLDS,   0,    8,  },	       /* Key */
    { FLDS,   8,    1016,  },	       /* Data */
    { 0,   0,    0,  },
  };

static STDFLD desc_c3[] =              /*  Command TABLE 3*/
  {
    { FLDS,   0,    5,  },	       /* Command  */
    { FLDS,   5,    12,  },	       /* Queu ID */
    { 0,   0,    0,  },
  };

static STDFLD desc_c4[] =              /*  Command TABLE 4 */
  {
    { FLDS,   0,    6,  },	       /* DB-Table name  */
    { FLDS,   6,    5,  },	       /* Command  */
    { FLDS,   11,   12,  },	       /* Queue ID */
    { 0,   0,    0,  },
  };

static STDDESC d_tabs[] =        /* connects the table numbers to the desc-  */
	{                        /* riptors                                  */
    	{ TETAB,  desc_tet,  },
    	{ ROTAB,  desc_rot,  },
    	{ PNTAB,  desc_pnt,  },
    	{ ACTAB,  desc_act,  },
    	{ APTAB,  desc_apt,  },
    	{ CKTAB,  desc_ckt,  },
    	{ BCTAB,  desc_bct,  },
    	{ BDTAB,  desc_bd,   },
    	{ ALTAB,  desc_al,   },
    	{ DETAB,  desc_de,   },
    	{ ARTAB,  desc_ar,   },
    	{ GATAB,  desc_ga,   },
    	{ NATAB,  desc_na,   },
    	{ PSTAB,  desc_ps,   },
    	{ HGTAB,  desc_hg,   },
    	{ SETAB,  desc_se,   },
    	{ ASTAB,  desc_alias, },
    	{ TTTAB,  desc_timct, },
    	{ ATTAB,  desc_actct, },
    	{ IXTAB,  desc_stidx, },
    	{ CTTAB,  desc_cflct, },
    	{ C1TAB,  desc_c1, },
    	{ C2TAB,  desc_c2, },
    	{ NMTAB,  desc_nm, },
    	{ MTTAB,  desc_mt, },
    	{ EXTAB,  desc_gen, },
    	{ HSTAB,  desc_hsb, },
    	{ C3TAB,  desc_c3, },
    	{ C4TAB,  desc_c4, },
    	{    0,   NULL,      },      /* terminates the structure */  };
	
#endif

/****************************************************************************/
/****************************************************************************/
