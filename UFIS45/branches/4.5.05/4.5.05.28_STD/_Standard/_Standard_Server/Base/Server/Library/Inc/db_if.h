#ifndef _DEF_mks_version_db_if_h
  #define _DEF_mks_version_db_if_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_db_if_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/db_if.h 1.10 2010/11/29 15:11:17SGT bst Exp bst(2010/11/29 16:42:00SGT) $";
#endif /* _DEF_mks_version */
#ifndef __DBIF_INC
#define __DBIF_INC

#include "uevent.h"
#include "helpful.h"


/* PRF 6285: No changes for Array features needed */

/******************************
TWE REMOVED ON 05012000

#define SUCCESS         0

#ifndef FAILURE
#define FAILURE        -1
#endif
*******************************/

#define ORA_NOT_FOUND      1
#define MAX_SELECT_ITEMS   300
#define MAX_BIND_ITEMS     300

/*                         Function-Codes:  */
/*  SQL-Statement SELECT                        */

#define START           1  /* Lesen 1. row einer query;  */
#define NEXT            2  /* Lesen naechste row einer query; */
                            
/*   - alle uebrigen SQL-Statements */

#define COMMIT          4  /* Ausfuehren SQL-Statement mit automatischem
                            * COMMIT aller DB-Updates seit letztem COMMIT */

/* Functioncode-Modifier           */
                          
#define REL_CURSOR      8  /* automat. Schliessen des Cursors */        

#define ARRAY          16  /* SQL-Befehl mit host arrays ausfuehren */ 
#define NOACTION        32  /* Ausfuehren SQL-Statement ohne COMMIT */
#define BEGIN_TRANS	64 /* start transaction with select statements */

/*   Alle SQL-Statements werden mit Hilfe eines Cursors ausgefuehrt.
     Das rufende Programm bestimmt mit dem Argument <act_cursor>, ob
     das SQL-Statement in einem neuen Cursor ( = 0 ), oder einem schon
     eroeffneten Cursor ( > 0 ) ausgefuehrt werden soll. Die Benutzung
     eines schon eroeffneten Cursor darf ** NUR ** dann erfolgen, wenn
     es sich um genau dasselbe SQL-Statement wie bei der Eroeffnung des
     Cursors handelt und dabei ausschliesslich Pseudo-Variable zur Ueber-
     gabe von Werten (z.B. in WHERE-Klausel oder SET <column> bei UPDATE-
     Kommandos) verwendet wurden.
     Standardmaessig bleibt der verwendete Cursor nach Ausfuehrung er-
     oeffnet und die interne Cursornummer wird dem rufenden Programm
     uebergeben.
     Das Interface kann bis zu 20 Cursor parallel bearbeiten.
     Mit diesem Vorgehen wird die schnellstmoegliche Ausfuehrung von
     SQL-Befehlen erreicht, vor allem dann, wenn einunddasselbe Statement
     mit unterschiedlichen Werten ausgefuehrt werden muss.
     Um einen Ueberlauf der internen Cursor zu vermeiden, ist es notwendig,
     dass das rufende Programm bei Bedarf bestimmt, welcher Cursor frei-
     gegeben werden kann. Dazu bieten sich 2 Moeglichkeiten:
     1. explicites Schliessen eines Cursors mit dem internen Kommando
        CLOSE
     2. implicites Schliessen eines Cursors nach Ausfuehrung eines SQL-
        Statements durch Verwendung des Functioncode-Modifiers
        REL_CURSOR 
*/

/* defines fuer HandleArray */
#define	iINSERT_ARRAY		0
#define	iUPDATE_ARRAY		1
#define	iFUNCTION_FAIL		-1
#define	iTABLE_FAIL		-2
#define iFIELD_FAIL		-3
#define	iFIELDLENGTH_FAIL	-4
#define iDATA_FAIL		-5
#define	iFDEFAULT_FAIL		-6
#define	iMALLOC_FAIL		-7
#define	iREALLOC_FAIL		-8
#define	iURNO_FAIL		-9

/* defines fuer GetNextNumbers */
#define PERCENT_LEFT 2	/*percent of numbers left in NUMTAB, begin to warn*/

/* ======================================= */

struct _RecFldDescriptor
{
	int FieldLength;
	int DbFieldType;
};
typedef struct _RecFldDescriptor FLD_DESC;

struct _RecordDescriptor
{
	int InitSize;
	int NextSize;
	int CurrSize;
	int UsedSize;
	int LineSize;
	int LineCount;
	int FieldCount;
	FLD_DESC *FieldInfo;
	int AppendBuf;
	char FldSep;
	char RecSep;
	char *Value;
};
typedef struct _RecordDescriptor REC_DESC;

int SqlIfArray(short, short *, char *, char *, short, REC_DESC *);
int GetSqlCmdInfo(char *pcpSqlBuf, REC_DESC *prpRecDesc);

int InitRecordDescriptor(REC_DESC *,int,int,int);
int FreeRecordDescriptor(REC_DESC *);

/* ======================================= */

#ifdef CCSDB_ORACLE
int db_if (short, short *, char *, char *);
int check_ret(int rc);
int get_ora_err(int rc,char *errtxt);
int ResetDBCounter (void);
void HandleRemoteDB (EVENT * prpEvent);
void HandleRemoteDB_TESTSERVER (EVENT * prpEvent);
void PrintSqlca(void);
void PrintOraca(void);
int PrintCursorStatistics(short spCursor);
int PrintAllCursorStatistics(void);
void alter_session_trace_on(void);
void alter_session_trace_off(void);

void TWEget_time(int);

/* vbl diese funktionsprototypen haben hier nichts verloren !!! */

int GetNextVal(char *,int);
int GetNextValues(char *,int); /* optimised version of GetNextVal */
int HandleArray(int, short, char *, UINT, char **, short *, char ***);
int GetNextNumbers(char *,char *,int,char *); /* gets numbers from NUMTAB */
int AddAlert(char *pcpInterface,char *pcpType,char *pcpLastConnection,
	char *pcpConnectionState,char *pcpMessage,int bpDoCommit,int bpSendToSqlhdl,
		char *pcpDestName, char *pcpAlertName, char *pcpTwStart,char *pcpTwEnd);
int AddAlert2(char *pcpInterface,char *pcpType,char *pcpLastConnection,
	char *pcpConnectionState,char *pcpMessage,char *pcpData,int bpDoCommit,
		int bpSendToSqlhdl,char *pcpDestName, char *pcpWksName, 
		char *pcpTwStart,char *pcpTwEnd);
int CheckDBTable ( char *pcpTana, char *pcpConfigFile );


int DB_GetUrnoKeyCode(char *pcpTableName, char *pcpTableKeyCode);
int DB_GetUfisCedaCfg (char *pcpGroup, char *pcpLine, char *pcpDestBuff, char *pcpDefault);
int DB_HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines);

#ifndef DB_IF_INTERNAL
#include "oracle.h"
#endif
#endif

#ifdef CCSDB_INFORMIX
#ifndef INFIF_INTERNAL
#include "infofkt.h"
#endif
#endif
#endif
