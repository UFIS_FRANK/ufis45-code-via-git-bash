<?php

/*
Note: Since PHP 5.1.0 (when the date/time functions were rewritten), every call to a date/time function will generate a E_NOTICE if the timezone isn't valid, and/or a E_STRICT message if using the system settings or the TZ environment variable. 
*/

//date_default_timezone_set("UTC");

//----------------------- Connection Section ----------------------- 
//Define the Connection String
$DatabaseType="oci8";
$dbusername="prm";
$dbpassword="prm";
$database="PRM";
$servername="PRM";



$DatabaseTypeMysql="mysqlt";
$dbusernameMysql="root";
$dbpasswordMysql="";
$databaseMysql="cute_fids";
// Oracle Home variable
//putenv("ORACLE_HOME=/app/home/oracle_client/product/10.1.0.2");

//----------------------- Authorization   Section ----------------------- 
// AuthType Please uncoment what you want

//Authedication Type
//$AuthType="Mysql";
//$AuthType="Oracle";
//$AuthType="Variable"; // + File  , Set the Loginhopo
$AuthType="File";
//if AuthType== FILE || AuthType==Variable specifie the directory with the users,passwd file
$UserFilename="db/users.txt";

	
//THE HOPO  of the Airport
$hopo = "ATH";
$Loginhopo = "ATH"; // Must be provided from SITA


//----------------------- Cput   Section ----------------------- 
// The RSH server For the cput Command
// Leave blank ($RSHIP="";) if the cput command is in the local server

//$RSHIP="ufislh";
$RSHIP="prmappl.aia.gr";
// The comand tha we want to use in order to execute the remote script
//$RSCMD="/usr/bin/rexec -l username -p passwd ";
$RSCMD="/usr/bin/rsh -n ";

//$CputPath="/ceda/etc/cput45";
//The Cput File
$CputPath="/ceda/etc/kshCput";

// Page Title
$PageTitle="PRM INTERFACE INTERNATIONAL";

//----------------------- Messages ------------------------------

//$RedGoodByeMsg="PLEASE REMEMBER TO CLOSE THE CHECK-IN FLAP DOORS <br>THANK YOU !";
$RedGoodByeMsg="";

// Cput parameteres
//$wks="\'".$HostName."\'";
$wks="\'EXCO\'";
$cputuser="\'PRM\'";
$TW_START="\'0\'";
$TW_END="\'$hopo,TAB,PRM\'";

//----------------------- TemplatePath ------------------------------

//TemplatePath
$tmpl_path="templates/";

// Template Array
$tmpl_array=array();
// Main Template name
$maintmpl="";


//------ HG Groups --//

$swiss =   "'TTF','ABP','AAB','BVR','NRO','CJE','BES','GBJ','UCR','ADN','NRP','RTE','OVA','UAR','MMD','AMA','FIF','JTV','DLY'";
$swiss = $swiss ."'AJU','LZR','AMC','AOE','ACG','SFB','ATJ','TSC','FIX','JAR','UMB','LBC','PAJ','LVN','ELG','XPE','ADI','AUF','DEF'";
$swiss = $swiss ."'AJF','AIA','AUL','AXY','BBA','BHP','BLI','BAF','BXA','CKM','BLM','VOL','BPA','BMW','BOO','BAW','LZB','BGF','CBI'";
$swiss = $swiss ."'GLJ','OTL','CLS','EXS','RUS','SDR','FYN','CLA','COE','COA','CYP','DCS','DAF','DTR','DWT','DAL','AMB','UDN','DBK','DUK'";
$swiss = $swiss ."'EAB','EZE','EIR','LBR','EJD','UAE','EUP','GOJ','FPO','VIP','VMP','EJM','IFA','FAH','FLT','FTY','FYA','FYG','FLK','FXR'";
$swiss = $swiss ."'FGL','FUA','GMA','GZP','GAF','GWI','GES','GJA','GDA','GDK','GRE','GZA','HTG','GSJ','HLX','HFA','HEA','HEJ','IMP'";
$swiss = $swiss ."'FHE','HMS','HER','ICJ','ILL','JII','ITN','IFT','IJM','ISF','TIH','JCB','JAG','JCX','JEI','JEK','JDI','JEF','JNV','JEP'";
$swiss = $swiss ."'KLM','LEA','WGT','LTU','MND','MHN','MPJ','TFG','MYO','SUM','MEA','MEM','ISS','MSA','MON','MSI','NKL','NJE','NTJ','NBL'";
$swiss = $swiss ."'NFA','NAX','OAD','PEA','BAT','PMU','PTI','PTG','PWF','PLM','QFA','QTR','MTL','RAQ','RYT','RDP','ROJ','RJA','NAF','RSJ'";
$swiss = $swiss ."'RYR','RLS','MOZ','SNM','SHE','SVW','SIW','SIO','SEH','SRK','KYB','DAT','OKT','SOX','ONG','KSA','SUS','MDT','SVF','SAZ'";
$swiss = $swiss ."'CRX','SWR','TGM','FPG','TAG','ROT','THA','TCX','THZ','AWC','TLY','TSI','TSY','TWJ','TYW','TJS','VEA','VIB','VIK','MOV'";
$swiss = $swiss ."'PRX','VPA','VEX','VLE','JET','QGA','WNR','MJS'";


$oa = "'BJT','ADE','ADR','AEW','ITE','FAS','ACL','CCA','AEA','AEY','BIE','BER','UDC','AXN','AZA'";
$oa = $oa . "'AZE','AIZ','ASE','AEU','AMT','OGE','AUA','AVW','JOR','BAL','TOM','BMA','CFI','CLI','CST'";
$oa = $oa . "'CFG','CRL','CSA','DNV','EDW','FSD','MSR','ELY','ECA','EEZ','JLN','EAF','XLA','FIN','FCA'";
$oa = $oa . "'FTL','EXH','GNJ','TGZ','GJT','GFA','HHI','HLF','OAW','HOA','IBE','IWD','NSK','ISR','JAT'";
$oa = $oa . "'JAF','JXX','KNI','MVD','KIL','LLA','LVG','LTE','LGL','DAN','MPH','BSK','MYT','VKG','NOS'";
$oa = $oa . "'NLY','NRD','NDC','NVR','OLY','OAL','ORF','PEV','PVG','QAJ','REI','RMV','SBU','SOV','SNA'";
$oa = $oa . "'FFD','SUA','SIA','ESK','HSK','SLL','JKK','SEU','SNB','JTT','ERO','SYR','TMI','TCW','TDR'";
$oa = $oa . "'TRA','TVS','TVL','TUB','TFL','TAR','TJT','AUI','UZB','VDA','WLC','XLF'";

$interjet = "'IHE','INJ'";

$ga = "'AEE','EIN','AFL','ALD','AEN','AFM','AAW','AAF','AHR','ARR','BTI','ABR','AFR','MHS','MLD'";
$ga = $ga ."'ADH','SCU','SVK','URG','WLR','DRU','ADB','RNV','KKK','CLU','VBC','BGH','BID','BLF','BLE'";
$ga = $ga ."'BRW','BPS','KRP','CAZ','CLW','CNB','CTN','DGX','DSO','BAG','DMO','DUB','EZY','EZS','EFD'";
$ga = $ga ."'NLK','ELL','JNL','ERJ','EIA','XJC','EXU','CLB','FWK','GSM','FHY','HCY','HUF','IBZ','IRA'";
$ga = $ga ."'IRL','ISD','PJS','JBR','KSJ','KAJ','KGL','KJC','LNX','LOT','DLH','MAH','NOY','EBF','MYW'";
$ga = $ga ."'VCX','OHY','ORE','PIA','POT','PLK','RBB','SDM','SVA','SAS','CGL','SWZ','SXP','IGA','GSW'";
$ga = $ga ."'SPW','SXS','SWT','TAP','TIS','TAY','TUL','THY','UVN','USA','VLG','PIX','WZZ'";


$mailfrom="webmaster";
$APPName="Web PRM";

$APPVersion="4.5.0.1";
$APPReleaseDate="23/07/2008";




?>
