# @(#) $Id :$
# Script to get the version numbers of all UFIS binaries
#
# 20031027 JIM: added output for used libraries
# 20031027 JIM: tested for Sun-OS, Red Hat Linux, HP-UX
#

function DisplayHelp
{ 
   cat <<EOF
   `basename $0`: Script to get the version numbers of all UFIS binaries
   Usage: 
   # get the main version numbers of all UFIS binaries:
   `basename $0`   
   # get the version numbers of all UFIS binaries including the version 
   # numbers of used libraries (verbose output):
   `basename $0` -v
EOF
}

# remember old directory:
OLDWORK=`pwd`

case ${OLDWORK} in
   */ceda/bin)  # for Fritjof: stay in current 
                ;;
            *)  # change to ceda binary dir
                cd /ceda/bin
                ;;
esac

THIS=`basename $0`
LOGFILE=/ceda/tmp/${THIS}.log

function CreateLog
{ # this function extracts a complete list of version strings from all sources for all binaries

  # delete logfile:
  echo >${LOGFILE}
  
  for f in * ; do
    if [ ! -h $f ] ; then
      if [ ! -d $f ] ; then
        case $f in 
          core*|*.*) echo ignoring $f
                 ;;
              *) echo processing $f
                 # run and evaluate checksum
                 CKSUM=`cksum $f | ( read a b c ; echo $a $b )`
                 check=`echo $CKSUM | (read a b ; echo $a )`
                 size=`echo $CKSUM | (read a b ; echo $b )`
                 # get all version strings of binaries and add process name 
                 # and checksum to the line 
                 # delete duplicate lines (i.e. header used in different libs)
                 strings $f | grep "@(#) UFIS" | awk '
                    {
                       a=substr($0,index($0,"@(#)"))
                       print "'$f' '$check' '$size' \t"a
                    }' | sort -u | grep "\$Id. .*\.c " >>${LOGFILE}
                 ;;
        esac
      fi # directories
    else
  	   echo "$f LINK!" >>${LOGFILE}
    fi   # links
  done
}
  
function WorkOnLog
{ # this function reformats the log file for a better process overview

if [ "$1" = "-v" ] ; then
   with_libs=yes  # also get a list of library version numbers
else
   with_libs=no   # supress list of library version numbers
fi

# The following AWK script uses a BEGIN block to initialize the AWK, a
# not named main block to read the log file and an END block to reformat
# the output summary.
awk 'BEGIN {
  # initialization:
  MAX=0
  proc_len    = 7
  prj_len     = 6
  file_len    = 4
  version_len = 7
  date_len    = 4
  time_len    = 4
  usr_len     = 4
  check_len   = 8
  size_len    = 4
  ListOfLibs  = ","
  ListOfProcs = ","
}
{
  # processing all lines:
     proc[MAX]=$1
     lfile[MAX]=$11
     num=split(lfile[MAX],fileparts,"/")
     prj[MAX]=fileparts[3]
     file[MAX]=fileparts[num]
     version[MAX]= $12
     date[MAX]=$13
     time[MAX]=$14
     usr[MAX]=$15
     check[MAX]=$2
     size[MAX]=$3

     ii = length(proc[MAX])   ; if (ii > proc_len   ) {proc_len    = ii}
     ii = length(prj[MAX])    ; if (ii > prj_len    ) {prj_len     = ii}
     ii = length(file[MAX])   ; if (ii > file_len   ) {file_len    = ii}
     ii = length(version[MAX]); if (ii > version_len) {version_len = ii}
     ii = length(date[MAX])   ; if (ii > date_len   ) {date_len    = ii}
     ii = length(time[MAX])   ; if (ii > time_len   ) {time_len    = ii}
     ii = length(usr[MAX])    ; if (ii > usr_len    ) {usr_len     = ii}
     ii = length(check[MAX])  ; if (ii > check_len  ) {check_len   = ii}
     ii = length(size[MAX])   ; if (ii > size_len   ) {size_len    = ii}
    # print MAX,proc[MAX],file[MAX],version[MAX]
     MAX++
}
END {
  # finally, when all lines are read:
  if (MAX>0)
  {
    DOUBLE="=============================================================="\
           "=============================================================="
    print ""
    FMT = sprintf ("%%-%ds: %%-%ds %%-%ds %%%ds %%%ds %%%ds %%%ds %%-%ds",\
              proc_len,date_len,time_len,usr_len,version_len,check_len,size_len,prj_len)
    printf FMT"\n",\
           "process","date","time","user","version","checksum","size","project"
    print substr(DOUBLE,\
               1,proc_len+date_len+time_len+usr_len+version_len+check_len+size_len+prj_len+9)

    # here begins the output loop for process listing
    for (i=1 ; i<MAX; i++)
    {
       if (proc[i]".c" == file[i]) 
       {

        if (index(ListOfProcs,","file[i]",") == 0)
        {
          ListOfProcs=ListOfProcs""file[i]","
        }
         printf FMT"\n",\
           proc[i],date[i],time[i],usr[i],version[i],check[i],size[i],prj[i]
       }  
       else if (check[i] == "LINK!")
       {
          printf FMT"\n",proc[i],"LINK!","","","","","",""
       }
    } # here ends the output loop for process listing
    
    print ""
    if ( "'$with_libs'" == "yes")
    {
      # this loop collects the names of libraries 
      for (i=1 ; i<MAX; i++)
      {
        if (check[i] != "LINK!")
        {
          if (index(ListOfProcs,","file[i]",") == 0)
          {
            if (index(ListOfLibs,","file[i]",") == 0)
            {
              ListOfLibs=ListOfLibs""file[i]","
            }
          }
        }
      }
      FMT = sprintf ("%%-%ds: (%%-%ds): %%-%ds %%-%ds %%%ds %%%ds %%-%ds",\
               file_len,proc_len,date_len,time_len,usr_len,version_len,prj_len)
      printf FMT"\n",\
             "library","process","date","time","user","version","project"
      print substr(DOUBLE,\
                 1,file_len+proc_len+date_len+time_len+usr_len+version_len+prj_len+12)
      tmp=substr(ListOfLibs,2,length(ListOfLibs)-2)
      x=split(tmp,libs,",")

      # here begins the output loop for library listing
      for (y =1 ; y <= x ; y++)
      {
        lib=libs[y]
        j = -1
        for (i=1 ; i<MAX; i++)
        {
           if (lib == file[i]) 
           {
             if (j == -1)
             {
                j = i
             }
             else if (version[i] != version[j])
             {
                j = -2
             }
           }  
        }
        if (j == -1)
        {
           printf FMT"\n",lib,"===not found ===","","","","",""
        }
        else if (j != -2)
        {
           i = j
           printf FMT"\n",\
               lib,"=ALL=",date[i],time[i],usr[i],version[i],prj[i]
        }
        else
        {
           for (i=1 ; i<MAX; i++)
           {
              if (lib == file[i]) 
              {
                printf FMT"\n",\
                lib,proc[i],date[i],time[i],usr[i],version[i],prj[i]
              }  
           }
        }
      } # here ends the output loop for library listing
    }
#    print "Procs: " ListOfProcs
#    print "Libs: "ListOfLibs
#    print proc[0],file[0]
#     print tmp
     print ""
  }
}' ${LOGFILE}

}

if [ "$1" = "" -o "$1" = "-v" ] ; then
   CreateLog
   WorkOnLog $1
else
   DisplayHelp
fi
