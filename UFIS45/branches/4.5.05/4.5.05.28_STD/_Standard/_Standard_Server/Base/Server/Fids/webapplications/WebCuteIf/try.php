<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/

include('ErrorHandler.php'); 

$error =& new ErrorHandler(); 

 

function msg_customize($str, $level, $file, $line) 

{ 

	return "Oh, man...!\t($level) $str : $file at $line.\n"; 

} 

?> 

<html> 

<head><title>Exploring ErrorHandler capabilities</title></head> 

 

<body> 

	<h1>ErrorHandler</h1> 

 

	<h2>What can you benefit from using ErrorHandler?</h2> 

		<p>In short, a lot. It is easy to customize by modifying the .ini file. 

		It takes only a few seconds to switch errorhandling in your application into 

		production state. </p> 

 

	<h2>During developent phase...</h2> 

		<h3>DEBUG</h3> 

		<p>You don't need to pollute the source code with 'var_dump' or 'print_r' 

		function calls. Use ErrorHandler's built-in debug facility! It's a 

		convenient combination of the function print_r() and var_dump(). 

		<ul> 

			<li>you can find out each variables type easily 

			<li>integers printed in decimal, hexadecimal, octal and binary format 

			<li>resource variables printed in a human readable format 

		</ul> 

 

		<h3>Customizable LOGGING</h3> 

		<p>ErrorHandler automatically log these informations if you want. It can 

		handle arbitrary number of log destination (called multiple log earlier). 

		You can specify different log destination per script files. If so, script 

		errors can be reported to its developer, which may ease teamwork. 

		</p> 

 

		<h3>About <a href="javascript:void(ErrorHandler.focus());">CONSOLE window</a></h3> 

		<p>Error messages are queued up and shown in a separate browser window called 

		CONSOLE. ErrorHandler extracts the corresponding piece of code generating the error 

		and displays variables set in that context.</p> 

		<p>CONSOLE is intended to separate error reports from your page design. 

		You may notice it works even if the error rises 

		<span title="<?php echo $URL_not_defined; ?>">at the very centre of an HTML 

		tag</span>.</p> 

		<p>and a lot more... (source extraction around the failed line, variable context)</p> 

 

	<h2>In production state...</h2> 

		<h3>SILENT mode</h3> 

		<p>You change some ini settings and your scripts is ready to publish. One of 

		the main features is SILENT mode. If you turn it on, neither error messages nor 

		CONSOLE window will pop up in case of an accidental error. (DEBUG reports also 

		discarded).</p> 

 

 

		<h3>REPLACE page</h3> 

		<p>If a very serious error occurs and script must be halted, you must inform the 

		users. You can design your own error page and tell ErrorHandler to show this page 

		in such cases. This page can be a local file which can read from disk and replaces 

		the current output content, or it can be an URL, in this case ErrorHandler redirects 

		the client to this page (server-side and client-side redirection supported, too)</p> 

 

		<h3>LOCK .ini file</h3> 

		<p>You don't need to comment out or strip out any ErrorHandler function call, which 

		is placed in the scripts for debugging purpose. You can LOCK the defult .ini settings, 

		that is, ErrorHandler ignores each modification attempt to itself at run-time. 

		</p> 

 

		<p>and a lot more... 

		(mail encryption, customizable error messages, different errorlevels to different 

		reports)</p> 

 

<h1> TEST</h1> 

<?php 

 

class bugs 

{ 

	function bugs() {} 

 

	function notice() { 

		if ($this->_not_set) 

			print '$this->_not_set isn\'t TRUE.<br>'; 

		else 

			return FALSE; 

	} 

 

	function warning() { 

?> 

		<p><font color="red">This is simple text written outside of PHP scope. 

<?php 

		printf('%d'); 

	} 

 

 

	function error() { 

		trigger_error('user\'s error.', E_USER_ERROR); 

	} 

} 

$a = new bugs; 

 

//$error->set_context('exclude', Lookup_ErrorHandler()); # exclude $error object from CONTEXT report 

$error->set_context('strict', FALSE); # strict context report 

 

$a->notice(); 

 

$a->warning(); 

 

$error->set_context('strict', TRUE);  # strict context report 

$a->error(); 

 

$a = 'hello world!'; 

$b = array('a' => array(1=> range(1,10))); 

include('no such file.'); 

 

/* the following function-call causes error which can not be hanlded by 

ErrorHandler(), but you can still play with SILENT REPORT and LOG REPORT. */ 

?> 

</body> 

</html> 

