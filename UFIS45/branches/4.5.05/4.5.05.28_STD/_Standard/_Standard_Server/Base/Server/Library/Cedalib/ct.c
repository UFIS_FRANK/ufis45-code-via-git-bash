#ifndef _DEF_mks_version_ct_c
  #define _DEF_mks_version_ct_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ct_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/ct.c 1.6 2011/08/09 16:27:05SGT bst Exp  $";
#endif /* _DEF_mks_version_ct_c */

/*---------------------------*/
/* THIS IS A MAGIC46 VERSION */
/*---------------------------*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <malloc.h>
#include "ct.h"

#include "glbdef.h"


#define CT_TRUE -1
#define CT_FALSE 0 

#define SORT_ASC        0
#define SORT_DESC       1

#define URNO_SIZE 10

struct _DataLine
{
        long LineChecked;
        long StatusValue;
        long RefLineNo;
        long DbInsFlag;
        long DbUpdFlag;
        long DbDelFlag;
        long FontColor;
        long BackColor;
        long TimeValue;
        long MemorySize;
        long StringSize;
		char *ColStats;
		char *ColProps;
        char *LineTag;
        char *AnyList;
        char *AnyData;
        char *OldData;
        char *SortString;
        char *Values;
};
typedef struct _DataLine DATA_LINE;

struct _DataPool
{
        DATA_LINE *DataLine;
        DATA_LINE *OrigLine;
};
typedef struct _DataPool DATA_POOL;

struct _IndexLine
{
        long Line;
        char *Value;
};
typedef struct _IndexLine INDEX_LINE;

struct _IndexWrapper
{
        INDEX_LINE *IndexLine;
};
typedef struct _IndexWrapper INDEX_WRAPPER;

struct _IndexPool
{
        long             UsedColumns[1000];
        long             ColumnCount;
        long             UsedLines;
        char            *IndexName;
        char            *Columns;
        INDEX_WRAPPER   *IndexObject;
        INDEX_LINE      *AllLines;
	char            *AllValues;
};
typedef struct _IndexPool INDEX_POOL;

/* Entry Point of Named Data Pools */
struct _SubArrays
{
        char ArrayName[32];
        char TableName[32];
        char NumTabKey[16];
        char TrimMethod[4];
        int  UrnoRanges;
        int  UrnoPoolIdx;
        int  TrackChanges;
		int  DataNotNull;
		long FieldCount;
        long UsedLines;
        long FreeLines;
        long InitPoolSize;
        long TotalMemSize;
        long TotalDatSize;
        long SortStrLen;
        char *FieldList;
        char FieldSeparator;
        long llLengthList[1000];
        char AlignmentList[1000];
        char *Properties;
        long UsedIndexes;
        long FreeIndexes;
        long TotalIndexes;
        DATA_POOL *DataPool;
        INDEX_POOL *IndexPool;
};
typedef struct _SubArrays SUB_ARRAY;

/* Main Control of all Array Pools */
struct _MainArray
{
        char MainName[16];
        long UsedArrays;
        long FreeArrays;
        long InitSubArrays;
        long NextSubArrays;
        SUB_ARRAY *SubArray;
};
typedef struct _MainArray MAIN_ARRAY;


/* =========================== */
/* For URNO Pool List Handling */
/* =========================== */

struct _UrnoRange
{
    char UrnoValue[16];
    int  UrnoCount;
};
typedef struct _UrnoRange URNO_RANGE;

/* The URNO Pool gives the possibility */
/* to manage different pools of the same */
/* range code (NUMTAB.KEYS) for several */
/* areas of URNO assignment. */
/* More than one ranges can be grouped  */
/* and managed in one single urno pool. */
struct _UrnoPool
{
    char AreaName[16];  /* Pool Area Name (Like AFTTAB) */
    char PoolName[16];  /* Pool Key Code (NUMTAB.KEYS)  */
    int  UsedLines;
    int  UrnoCount;
    URNO_RANGE *UrnoRange;
};
typedef struct _UrnoPool URNO_POOL;

struct _UrnoPoolArray
{
    int UsedPools;
    URNO_POOL *UrnoPool;
};
typedef struct _UrnoPoolArray URNO_POOL_ARRAY;




/* =========================== */
/* Global Variables */
/* =========================== */

/* Main Entry Point of Data Storage */
static MAIN_ARRAY *MainArray    = NULL;

static MAIN_ARRAY *CurMainArray = NULL;
static SUB_ARRAY  *CurSubArray  = NULL;
static DATA_POOL  *CurDataPool  = NULL;
static DATA_LINE  *CurDataLine  = NULL;
static DATA_LINE  *OrgDataLine  = NULL;


static long CurMainIdx = -1;
static long CurNameIdx = -1;
static long CurLineIdx = -1;


static URNO_POOL_ARRAY *UrnoPoolArray = NULL;



/*==============================
MWO: 12.09.2002
Helper variables
================================*/
/*Used for all internal line activities*/
static char *pcgLineStr;
static long lgLineStrLen;

/*Used for all internal data activities*/
static char *pcgOrigData;
static long lgOrigDataLen;

static char *pcgCurrData;
static long lgCurrDataLen;

static char *pcgPrevData;
static long lgPrevDataLen;

static char *pcgFldList;
static long lgFldListLen;

/* ---------------------------- */
/* Private Function Prototypes  */
/* ---------------------------- */
static long CreateSubArray(char *ArrayName);
static long GetMainArrayIndex(char *MainName);
static long GetSubArrayIndex(char *ArrayName);
static long CreateDataPool(char* ArrayName, long ArrayCount);
static long CreateDataLine(char *LineData, long LineIdx);
static void ShiftDataPool(long LineNo);
static void ShrinkDataPool(char* ArrayName, long LineNo);
static int CT_SortAscend(const void *, const void *);
static int CT_SortDescend(const void *, const void *);
static long GetColumnNameIndex(char* ArrayName, char* ColName);
static long CT_SortIndex(char* ArrayName, char* IndexName);
static INDEX_POOL * GetIndexPoolByName(char* ArrayName, char* IndexName);
static long AdjustStringToCT_SortLen(char* pcpInValue, char cpAlign, long lpInLen, long lpOutLen, char *pcpOutValue);

static void IncreaseStrgValue(char *);

void CT_PrintIndex(char* ArrayName, char* IndexName);

static SUB_ARRAY * GetSubArray(char* ArrayName);


static int InitUrnoPoolArray(void);
static int AttachUrnoPool(char *AreaName, char *PoolName);
static int GetUrnoPoolIndex(char *AreaName, char *PoolName);
static int GetFreeUrnoRange(int PoolIdx);
static int GetValidUrnoRange(int PoolIdx);
static void IncreaseUrnoValue(char *pcpUrnoValue);



/* ============================================================= */
/* ============================================================= */
void CT_PrintIndex(char* ArrayName, char* IndexName)
{
	SUB_ARRAY *pSub = NULL;
	INDEX_POOL *Pool;
	long i;

	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		Pool =  GetIndexPoolByName(ArrayName, IndexName);
		if(Pool != NULL)
		{
			for(i = 0; i < Pool->UsedLines; i++)
			{
				dbg(TRACE,"IDX LINE %d: DAT LINE=%ld, VALUE <%s>", 
				i, Pool->IndexObject[i].IndexLine->Line , Pool->IndexObject[i].IndexLine->Value );
			}
		}
	}
}

/* ============================================================= 
MWO: 12.09.2002
Compare function for index CT_Sorting
================================================================ */
static int CompareIndex(const void *_dp1, const void *_dp2)
{
	const INDEX_WRAPPER *dp1 = (const INDEX_WRAPPER *) _dp1;
	const INDEX_WRAPPER *dp2 = (const INDEX_WRAPPER *) _dp2;
	INDEX_LINE *dl1 = dp1->IndexLine;
	INDEX_LINE *dl2 = dp2->IndexLine;
	return (strcmp(dl1->Value,dl2->Value)); 
} /* End */


/* ------------------------------------------------------------- 
MWO: 11.09.2002
	Init pointer MainArray as the one and only instance
	All SubArrays are provided by one MainArray
------------------------------------------------------------- */
int CT_InitCedaTabBasic()
{
	int ilRet = -1;
	MainArray = (MAIN_ARRAY *)malloc(sizeof(MAIN_ARRAY));
	if (MainArray != NULL)
	{
		strcpy(MainArray->MainName,"MAIN");
		MainArray->UsedArrays = 0;
		MainArray->FreeArrays = 0;
		MainArray->SubArray = NULL;
		ilRet = 0;
	} /* end if */
/*------------------------
Init of global variables
------------------------- */
	pcgLineStr = (char*)malloc((size_t)4096);
	memset(pcgLineStr, 0, 4096);
	lgLineStrLen = 4096;

	pcgOrigData = (char*)malloc((size_t)4096);
	memset(pcgOrigData, 0, 4096);
	lgOrigDataLen = 4096;

	pcgCurrData = (char*)malloc((size_t)4096);
	memset(pcgCurrData, 0, 4096);
	lgCurrDataLen = 4096;

	pcgPrevData = (char*)malloc((size_t)4096);
	memset(pcgPrevData, 0, 4096);
	lgPrevDataLen = 4096;

	pcgFldList = (char*)malloc((size_t)4096);
	memset(pcgFldList, 0, 4096);
	lgFldListLen = 4096;
	return ilRet;
}

/* ============================================================= 
MWO: 10.09.2002
 ============================================================= */
long CT_CreateArray(char *ArrayName)
{
	long NameIdx = -1;

	if (MainArray == NULL)
	{
		CT_InitCedaTabBasic();
	}
	NameIdx = CreateSubArray(ArrayName);
	if (NameIdx > 0)
	{
		CurMainIdx = 0;
		CurNameIdx = NameIdx;
		CurMainArray = MainArray;
		CurSubArray = &CurMainArray->SubArray[NameIdx];
		CurDataPool = CurMainArray->SubArray[NameIdx].DataPool;
	} /* end if */
	else
	{
		CurMainIdx = -1;
		CurNameIdx = -1;
		CurMainArray = NULL;
		CurSubArray = NULL;
		CurDataPool  = NULL;
	} /* end else */
	return NameIdx;
} /* End CT_ActivateSubArray */



/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static long GetMainArrayIndex(char *MainName)
{
	long MainIdx = -1;
	long CurIdx = -1;
	if (MainArray != NULL)
	{
		CurIdx = 1;
		while ((CurIdx <= MainArray[0].UsedArrays) && (MainIdx < 0))
		{
			if (strcmp(MainArray[CurIdx].MainName,MainName) == 0)
			{
				MainIdx = CurIdx;
			} /* end if */
			CurIdx++;
		} /* end while */
	} /* end if */
	return MainIdx;
} /* GetMainArrayIndex */

/* ============================================================= 
MWO: 10.09.2002
 ============================================================= */
static long CreateSubArray(char *ArrayName)
{
	long NameIdx = -1;
	long IntArrayCount = 0;
	SUB_ARRAY *SubArray;
	NameIdx = GetSubArrayIndex(ArrayName);
	if (NameIdx < 0)
	{
		SubArray = MainArray->SubArray;
		if (SubArray == NULL)			
		{
			SubArray = (SUB_ARRAY *)malloc(sizeof(SUB_ARRAY));
			if (SubArray != NULL)	
			{
				SubArray->UrnoRanges = FALSE;
				SubArray->UrnoPoolIdx = -1;
				SubArray->TrackChanges = 0;
				SubArray->DataNotNull = 0;
				SubArray->TrimMethod[0] = 0x00;
				SubArray->UsedLines = 0;
				SubArray->FreeLines = 0;
				SubArray->FieldList = NULL;
				SubArray->Properties = NULL;
				SubArray->FieldCount = 0;
				SubArray->UsedIndexes = 0;
				SubArray->FreeIndexes = 0;
				SubArray->TotalIndexes = 0;
				SubArray->IndexPool = NULL;
				SubArray->DataPool = NULL;
				SubArray->IndexPool = NULL;
				SubArray->InitPoolSize = 1000;
				strcpy(SubArray->ArrayName, ArrayName);
				SubArray->FieldSeparator = ',';
				MainArray->SubArray = SubArray;
				MainArray->UsedArrays = 1;
				MainArray->FreeArrays = 0;
				NameIdx = 0;
			} /* end if */
		} /* end if */
		if ((NameIdx < 0) && (SubArray != NULL))
		{
			NameIdx = MainArray->UsedArrays;
			if (MainArray->FreeArrays == 0)
			{
				IntArrayCount += MainArray->UsedArrays;
				SubArray = (SUB_ARRAY *)realloc(SubArray,sizeof(SUB_ARRAY)*(IntArrayCount+1));
				if (SubArray != NULL)
				{
					strcpy(SubArray[NameIdx].ArrayName, "EMPTY");
					SubArray[NameIdx].UrnoRanges = FALSE;
					SubArray[NameIdx].UrnoPoolIdx = -1;
					SubArray[NameIdx].TrackChanges = 0;
					SubArray[NameIdx].DataNotNull = 0;
					SubArray[NameIdx].TrimMethod[0] = 0x00;
					SubArray[NameIdx].FieldSeparator = ',';
					SubArray[NameIdx].UsedLines = 0;
					SubArray[NameIdx].FreeLines = 0;
					SubArray[NameIdx].FieldList = NULL;
					SubArray[NameIdx].Properties = NULL;
					SubArray[NameIdx].DataPool = NULL;
					SubArray[NameIdx].IndexPool = NULL;
					SubArray[NameIdx].FieldCount = 0;
					SubArray[NameIdx].UsedIndexes = 0;
					SubArray[NameIdx].FreeIndexes = 0;
					SubArray[NameIdx].TotalIndexes = 0;
					SubArray[NameIdx].IndexPool = NULL;
					SubArray[NameIdx].InitPoolSize = 1000;
					NameIdx = MainArray->UsedArrays;
				} /* end if */
				MainArray->SubArray = SubArray;
			} /* end if */
			if (SubArray != NULL)
			{
				strcpy(SubArray[NameIdx].ArrayName, ArrayName);
				MainArray->UsedArrays++;
			} /* end if */
		} /* end if */
		CT_SetLengthList(ArrayName, "");
	} /* end if */
	return NameIdx;
} /* CreateSubArray */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static long GetSubArrayIndex(char *ArrayName)
{
	long NameIdx = -1;
	long CurIdx = 0;
	CurIdx = 0;
	while ((CurIdx < MainArray->UsedArrays) && (NameIdx < 0))
	{
		if (strcmp(MainArray->SubArray[CurIdx].ArrayName,ArrayName) == 0)
		{
			NameIdx = CurIdx;
		} /* end if */
		CurIdx++;
	} /* end while */
	return NameIdx;
} /* GetSubArrayIndex */


/* ============================================================= 
MWO: 12.09.2002 Create a line value within an index
Creates a combinated index from the comma separated Columns
================================================================ */
long CT_CreateIndex(char* ArrayName, char* IndexName, char* Columns)
{
	long		PoolIdx = -1;
	long		i,j;
	long		llPatternCnt;
	long		llColNo;
	long		llIdxLen = 0;
	long		llTmpLen = 0;
	char		cpResult[4096];
	char		pclLinNbrStrg[16];
	INDEX_WRAPPER	*IndexObject = NULL;
	INDEX_POOL	*polPool = NULL;
	INDEX_LINE	*IndexLine = NULL;
	INDEX_LINE	*AllIndexLines = NULL;
	char *pclAllIdxValues = NULL;
        char *pclLinPtr = NULL;



	CurSubArray = GetSubArray(ArrayName);
	if(CurSubArray != NULL)
	{
		polPool = CurSubArray->IndexPool;
		if(polPool == NULL)
		{
			polPool = (INDEX_POOL*)malloc(sizeof(INDEX_POOL));

			if(polPool != NULL)
			{
				polPool->Columns = (char*)malloc(strlen(Columns)+1);
				strcpy(polPool->Columns, Columns);
				polPool->IndexName = (char*)malloc(strlen(IndexName)+1);
				polPool->UsedLines = 0;
				polPool->ColumnCount = 0;
				strcpy(polPool->IndexName, IndexName);
				polPool->IndexObject = NULL;
				CurSubArray->IndexPool = polPool;
				CurSubArray->UsedIndexes++;
				CurSubArray->TotalIndexes++;
				PoolIdx = 0;
			}
		}
		if ((PoolIdx < 0) && (polPool != NULL))
		{
			PoolIdx = CurSubArray->UsedIndexes;
			if(CurSubArray->FreeIndexes == 0)
			{
				polPool = (INDEX_POOL *)realloc(polPool,sizeof(INDEX_POOL)*(CurSubArray->UsedIndexes+1));
				if (polPool != NULL)
				{
					polPool[PoolIdx].Columns = (char*)malloc(strlen(Columns)+1);
					strcpy(polPool[PoolIdx].Columns, Columns);
					polPool[PoolIdx].IndexName = (char*)malloc(strlen(IndexName)+1);
					strcpy(polPool[PoolIdx].IndexName, IndexName);
					polPool[PoolIdx].IndexObject = NULL;
					polPool[PoolIdx].UsedLines = 0;
					polPool[PoolIdx].ColumnCount = 0;
					CurSubArray->UsedIndexes++;
					CurSubArray->TotalIndexes++;
					CurSubArray->IndexPool = polPool;
				}
			}
			else
			{
				PoolIdx = CurSubArray->UsedIndexes;
				CurSubArray->FreeIndexes--;
				CurSubArray->UsedIndexes++;
			
				polPool[PoolIdx].IndexName = (char*)malloc(strlen(IndexName)+1);
				strcpy(polPool[PoolIdx].IndexName, IndexName);
				polPool[PoolIdx].IndexObject = NULL;
				polPool[PoolIdx].UsedLines = 0;
				polPool[PoolIdx].ColumnCount = 0;
			}
		}
		if (polPool != NULL)
		{
			llPatternCnt = CT_CountPattern(Columns, ",");
			polPool[PoolIdx].ColumnCount = llPatternCnt+1;
			llIdxLen = 0;
			for( j = 0; j <= llPatternCnt; j++)
			{
				CT_GetDataItem(cpResult, Columns, j, ',');
				llColNo = atol(cpResult);
				polPool[PoolIdx].UsedColumns[j] = llColNo;
				llIdxLen += CurSubArray->llLengthList[llColNo] + 1;
			}
			llIdxLen += 15;
			IndexObject = (INDEX_WRAPPER*)malloc(sizeof(INDEX_WRAPPER)*(CurSubArray->UsedLines+10));
			if(IndexObject != NULL)
			{
				polPool[PoolIdx].IndexObject = IndexObject;
				polPool[PoolIdx].UsedLines = 0;
				AllIndexLines = (INDEX_LINE*)malloc(sizeof(INDEX_LINE)*(CurSubArray->UsedLines+10));
				polPool[PoolIdx].AllLines = AllIndexLines;
				IndexLine = AllIndexLines;
				i = llIdxLen * (CurSubArray->UsedLines+10);
				pclAllIdxValues  = (char *)malloc(i);
				polPool[PoolIdx].AllValues = pclAllIdxValues;
				pclLinPtr = pclAllIdxValues;
				if ((AllIndexLines != NULL) && (pclAllIdxValues != NULL))
				{
					CurDataPool = CurSubArray->DataPool;
					strcpy(pclLinNbrStrg,"0000000000");
					for( i = 0; i < CurSubArray->UsedLines; i++)
					{
						IndexLine->Line = i;
						IndexLine->Value = pclLinPtr;
						IndexObject[i].IndexLine = IndexLine;
						CurDataLine = CurDataPool[i].DataLine;
						for( j = 0; j <= llPatternCnt; j++)
						{
							llColNo = polPool[PoolIdx].UsedColumns[j];
							llTmpLen=CT_GetDataItem(cpResult,CurDataLine->Values,llColNo,',');
							pclLinPtr += AdjustStringToCT_SortLen(cpResult, 
								CurSubArray->AlignmentList[llColNo], 
								llTmpLen, CurSubArray->llLengthList[llColNo], 
								pclLinPtr);
							*pclLinPtr = ',';
							pclLinPtr++;
						}
						strcpy(pclLinPtr,pclLinNbrStrg);
						pclLinPtr = IndexLine->Value + llIdxLen;
						IndexLine++;
						IncreaseStrgValue(pclLinNbrStrg);
					}
				}
				polPool[PoolIdx].UsedLines = CurSubArray->UsedLines;
/*
dbg(TRACE,"INDEX SORT NOT ACTIVATED");
*/
				CT_SortIndex(ArrayName, IndexName);
			}
		}
	}

	return 0;
}

/* ============================================================= 
MWO: 18.09.2002
Parameters:
-----------
IN:		char* ArrayName,		==> Name of the SubArray
		char* IndexName,		==> Name of the index to be used
		char* SearchValue,		==> Search values comma separated
		short CompareMethod		==> Compare Method: 0=Equal, 1=Starts with
IN/OUT: STR_DESC* prpString,    ==> Return value with line numbers, will
									be reallocated if neccessary

return long  <amount of found lines> OR
			 -1 ==> nothing found or error in IN Parameters
===============================================================*/
void CT_DeleteIndex(char* ArrayName, char* IndexName)
{
	SUB_ARRAY *pSub = NULL;
	long	idxPool = 0;
	long	i = 0;
	INDEX_POOL *Pool = NULL;

	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		for(i = 0; i < pSub->TotalIndexes; i++)
		{
			if(strcmp(pSub->IndexPool[i].IndexName, IndexName) == 0)
			{
				Pool = &pSub->IndexPool[i];
				idxPool = i;
			}
		}
		if(Pool != NULL)
		{
			/* dbg(TRACE,"FREE IDX %d: ALL INDEX VALUES (ARRAY OF CHAR)",idxPool); */
			free(Pool->AllValues);

			/* dbg(TRACE,"FREE IDX %d: ALL INDEX LINES (ARRAY OF LINES)",idxPool); */
			free(Pool->AllLines);

			/* dbg(TRACE,"FREE IDX %d: INDEX OBJECT (ARRAY OF WRAPPER)",idxPool); */
			free(Pool->IndexObject);

			Pool->IndexObject = NULL;
			Pool->UsedLines = 0;

			for(i = idxPool; i < pSub->UsedIndexes-1; i++)
			{
				pSub->IndexPool[i] = pSub->IndexPool[i+1];
			}
			pSub->UsedIndexes--;
			pSub->FreeIndexes++;
		}
	}
}


/* ============================================================= 
MWO: 20.09.2002
Parameters:
-----------
IN:		char* ArrayName,		==> Name of the SubArray
		char* IndexName,		==> Name of the index to be used
		char* SearchValue,		==> Search values comma separated
		short CompareMethod		==> Compare Method: 0=Equal, 1=Starts with
IN/OUT: STR_DESC* prpString,    ==> Return value with line numbers, will
									be reallocated if neccessary

return long  <amount of found lines> OR
			 -1 ==> nothing found or error in IN Parameters
================================================================ */
long CT_GetLinesByIndexValue(char* ArrayName, char* IndexName, char* pcpSearchValues, STR_DESC* prpString, char* pcpCompareMethod)
{
	int ilMin, ilMax, ilCurr, ilTmp;
	int ilIdx=-1;
	int blFound = -1;
	int blAbort = -1;
	int ilComp;
	int ilLineCount;
	char pclTmp[100];
	long llLineCount = 0;		/* Lines in the index */
	long llPat1,				/* Pattern counter for ComparMethod */
		 llPat2;				/* Pattern counter for SerachValues */
	char pclCurrCompare[10];	/* For each loop the current item of CompareMethod */
	char pclCurrStr[4096];      /* For each loop the current item of pcpSearchValues */
	char pclFound[100];			/* this contains 0 or 1 0=Found 1=Not found*/
								/* after the loops strstr will identify if there is a '1' ==>*/
								/* so one field does not fit */
	char pclAbort[100];			/* same as pclFound but for aborting */
	char pclCurrSearchValue[4096];
	int ilStrLen = 0;
	int ilCmpStrLen = 0;
	long i=0;
	int CompareMethod;
	char *pclOut = NULL;
	int ilTmpLen = 0;


	SUB_ARRAY *pSub = NULL;
	INDEX_POOL *Pool;

	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		Pool =  GetIndexPoolByName(ArrayName, IndexName);
		if(Pool == NULL)
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
	llPat1 = CT_CountPattern(pcpCompareMethod, ",");
	llPat2 = CT_CountPattern(pcpSearchValues, "," );
	if(llPat1 != llPat2)
	{
		return -1;
	}
	if(strlen(pcpCompareMethod) == 0 || strlen(pcpSearchValues) == 0)
	{/* No search valid search values found */
		return -1;
	}

	ilMin = 0;
	pclCurrCompare[0]=0x00;
	pclCurrStr[0]=0x00;
	pclFound[0]=0x00;
	pclAbort[0]=0x00;

	sprintf(pclTmp, "%ld", Pool->UsedLines);
	
	ilMax = Pool->UsedLines;
	ilTmp = ilMax * (strlen(pclTmp)+1);
	pclTmp[0]=0x00;

	ilLineCount = ilMax;
	if(ilMax == 0)
	{
		blAbort = 0;
	}
	ilCurr = (int)(ilMax/2);
	/* ----------------------------------------------------------------------
	Strategy: Use the first search value to find the entry point within the 
	index. If found use all search values to check if criteria was fit
	-------------------------------------------------------------------------*/
	while (blFound== -1 && blAbort == -1)
	{
		if((ilMax-ilMin) > 1)
		{
			strcpy(pcgLineStr, Pool->IndexObject[ilCurr].IndexLine->Value);
			ilStrLen = CT_GetDataItem( pclCurrStr, pcgLineStr, 0, ',');
			/* Trim Both */
			ilStrLen = CT_TrimLeft(pclCurrStr, ' ', ilStrLen);
			ilStrLen = CT_TrimRight(pclCurrStr, ' ', ilStrLen);
			ilStrLen = CT_GetDataItem( pclCurrCompare, pcpCompareMethod, 0, ',');
    			/* No Trim */
			ilStrLen = CT_GetDataItem( pclCurrSearchValue, pcpSearchValues, 0, ',');
    			/* No Trim */
			ilStrLen = strlen(pclCurrSearchValue);
			if(pclCurrCompare[0] != 0x00)
			{
				CompareMethod = atoi(pclCurrCompare);
				if(ilStrLen > 0 )
				{
					if(CompareMethod == 0)
					{
						/* Compare equal */
						if(strcmp(pclCurrStr, pclCurrSearchValue) == 0)
						{
							ilIdx = ilCurr;
							blFound = 0;
						}
					}
					if(CompareMethod == 1)
					{
						/* Compare Like 'XXX%' */
						if(strstr(pclCurrStr, pclCurrSearchValue) != NULL)
						{
							ilIdx = ilCurr;
							blFound = 0;
						}
					}
					if(CompareMethod == 2)
					{
						if(strncmp(pclCurrStr, pclCurrSearchValue , ilStrLen) == 0)
						{
							ilIdx = ilCurr;
							blFound = 0;
						}
					}
				}
				if (blFound == -1)
				{
					ilComp = strcmp(pclCurrSearchValue, pclCurrStr);
					/*
					switch(ilComp)
					{
					case 0: 
						ilIdx = ilCurr;
						blFound = 0;
						break;
					case 1:
						ilMin = ilCurr;
						ilCurr += (int)((ilMax-ilCurr)/2);
						break;
					case -1:
						ilMax = ilCurr;
						ilCurr -= (int)((ilMax-ilMin)/2);
						break;
					}
					*/
					if (ilComp == 0)
					{
						ilIdx = ilCurr;
						blFound = 0;
					} else if (ilComp > 0) {
						ilMin = ilCurr;
						ilCurr += (int)((ilMax-ilCurr)/2);
					} else {
						ilMax = ilCurr;
						ilCurr -= (int)((ilMax-ilMin)/2);
					} 
				}
			}/*END if(ilStrLen > 0)*/
		}
		else
		{
			if(ilCurr == 1)
				ilCurr = 0;
			if(ilLineCount == ilCurr)
				ilCurr--;
			strcpy(pcgLineStr, Pool->IndexObject[ilCurr].IndexLine->Value);
			ilStrLen = CT_GetDataItem( pclCurrStr, pcgLineStr, 0, ',');
			/* Trim Both */
			ilStrLen = CT_TrimLeft(pclCurrStr, ' ', ilStrLen);
			ilStrLen = CT_TrimRight(pclCurrStr, ' ', ilStrLen);
			ilStrLen = CT_GetDataItem( pclCurrSearchValue, pcpSearchValues, 0, ',');
    			/* No Trim */
			ilStrLen = CT_GetDataItem( pclCurrCompare, pcpCompareMethod, 0, ',');
    			/* No Trim */
			ilStrLen = strlen(pclCurrStr);
			if(pclCurrCompare[0] != 0x00)
			{
				CompareMethod = atoi(pclCurrCompare);
				if(ilStrLen > 0 )
				{
					if(CompareMethod == 0)
					{
						/* Compare equal */
						if(strcmp(pclCurrStr, pclCurrSearchValue) == 0)
						{
							ilIdx = ilMin;
							blFound = 0;
						}
					}
					if(CompareMethod == 1)
					{
						/* Compare Like 'XXX%' */
						if(strstr(pclCurrStr, pclCurrSearchValue) != NULL)
						{
							ilIdx = ilMin;
							blFound = 0;
						}
					}
					if(CompareMethod == 2)
					{
						if(strncmp(pclCurrStr, pclCurrSearchValue, ilStrLen) == 0)
						{
							ilIdx = ilMin;
							blFound = 0;
						}
					}
				}
				if(blFound == -1)
				{
					blAbort = 0;
				}
			}/*End if(ilStrLen > 0) */
		}
	}
	if(blFound == -1)
	{
		return 0;
	}
	else
	{
		int ilCurr2;
		/* dbg(TRACE,"FOUND: MIN=%d MAX=%d CURR=%d IDX=%d", ilMin, ilMax, ilCurr, ilIdx); */
		blAbort = -1;
		ilCurr2 = ilIdx - 1;
		/* Search to top */
		/* dbg(TRACE,"SEARCH TO TOP FROM %d",ilCurr2); */
		while(ilCurr2 > -1 && blAbort == -1)
		{
			strcpy(pcgLineStr, Pool->IndexObject[ilCurr2].IndexLine->Value); 
			ilStrLen = CT_GetDataItem( pclCurrStr, pcgLineStr, 0, ',');
			/* Trim Both */
			ilStrLen = CT_TrimLeft(pclCurrStr, ' ', ilStrLen);
			ilStrLen = CT_TrimRight(pclCurrStr, ' ', ilStrLen);
			CT_GetDataItem( pclCurrCompare, pcpCompareMethod, 0, ',');
    			/* No Trim */
			CT_GetDataItem( pclCurrSearchValue, pcpSearchValues, 0, ',');
    			/* No Trim */
			ilStrLen = strlen(pclCurrSearchValue);
			if(pclCurrCompare[0] != 0x00)
			{
				CompareMethod = atoi(pclCurrCompare);
				if(ilStrLen > 0 )
				{
					if(CompareMethod == 0)
					{
						/* Compare equal */
						if(strcmp(pclCurrStr, pclCurrSearchValue) != 0)
						{
							blAbort = 0;
						}
					}
					if(CompareMethod == 1)
					{
						/* Compare Like 'XXX%' */
						if(strstr(pclCurrStr, pclCurrSearchValue) == NULL)
						{
							blAbort = 0;
						}
					}
					if(CompareMethod == 2)
					{
						if(strncmp(pclCurrStr, pclCurrSearchValue, ilStrLen) != 0)
						{
							blAbort = 0;
						}
					}
				}
			}
			ilCurr2--;
		}
		if (blAbort == 0)
		{
			ilCurr2++;
		}
		ilCurr2++;
		/* dbg(TRACE,"COLLECT: MIN=%d MAX=%d CURR=%d IDX=%d", ilMin, ilMax, ilCurr2, ilIdx); */
		blAbort = -1;
		/* Search to bottom */
		/* dbg(TRACE,"SEARCH TO BOTTOM AND COLLECT FROM %d",ilCurr2); */
		if(prpString->AllocatedSize == 0)
		{
			prpString->Value = (char*)malloc((size_t)1024);
			prpString->AllocatedSize = 1024;
			prpString->UsedLen = 0;
		}
		pclOut = prpString->Value;
		prpString->Value[0]=0x00;

		while(ilCurr2 < ilLineCount && blAbort == -1 )
		{
			pclFound[0]=0x00;

			for( i = 0; i <= llPat1; i++)
			{
				ilStrLen = CT_GetDataItem( pclCurrStr, Pool->IndexObject[ilCurr2].IndexLine->Value, i, ',');
				/* Trim Both */
				ilStrLen = CT_TrimLeft(pclCurrStr, ' ', ilStrLen);
				ilStrLen = CT_TrimRight(pclCurrStr, ' ', ilStrLen);
				CT_GetDataItem( pclCurrCompare, pcpCompareMethod, i, ',');
    				/* No Trim */
				CT_GetDataItem( pclCurrSearchValue, pcpSearchValues, i, ',');
    				/* No Trim */
				ilStrLen = strlen(pclCurrSearchValue);
				if(pclCurrCompare[0] != 0x00)
				{
					CompareMethod = atoi(pclCurrCompare);
					if(ilStrLen > 0 )
					{
						if(CompareMethod == 0)
						{
							/* Compare equal */
							if(strcmp(pclCurrStr, pclCurrSearchValue) == 0)
							{
								pclFound[i] =  '0';
							}
							else
							{
								if(i == 0)
								{
									blAbort = 0;
								}
								pclFound[i] =  '1';
							}
						}
						if(CompareMethod == 1)
						{
							/* Compare Like 'XXX%' */
							if(strstr(pclCurrStr, pclCurrSearchValue) != NULL)
							{
								pclFound[i] =  '0';
							}
							else
							{
								if(i == 0)
								{
									blAbort = 0;
								}
								pclFound[i] =  '1';
							}
						}
						if(CompareMethod == 2)
						{
							if(strncmp(pclCurrStr, pclCurrSearchValue, ilStrLen) == 0)
							{
								pclFound[i] =  '0';
							}
							else
							{
								if(i == 0)
								{
									blAbort = 0;
								}
								pclFound[i] =  '1';
							}
						}
					}
				}/*End  if(ilStrLen > 0) */
			}/* End for( i = 0; llPat1; i++) */
			pclFound[i] =  0x00;
			if(strstr(pclFound, "1") == NULL)
			{
				if(prpString->UsedLen+10 > prpString->AllocatedSize)
				{
					prpString->Value = (char*)realloc((char*)prpString->Value, prpString->AllocatedSize + 1024);
					prpString->AllocatedSize += 1024;
					pclOut = prpString->Value;
					pclOut += prpString->UsedLen;
				}
				sprintf(pclTmp, "%ld,", Pool->IndexObject[ilCurr2].IndexLine->Line);
				ilTmpLen = strlen(pclTmp);
				strcpy(pclOut, pclTmp);
				prpString->UsedLen += ilTmpLen;
				pclOut += ilTmpLen;
				llLineCount++;
			}
			ilCurr2++;
		}
	}
	if (prpString->UsedLen > 0)
	{
		prpString->Value[strlen(prpString->Value)-1]=0x00;
	}
	return llLineCount;
}

/* ============================================================= 
BST: ??.??.????
MWO: Reengeneerd 10.09.2002
 ============================================================= */
static long CreateDataPool(char* ArrayName, long ArrayCount)
{
	long LineIdx = -1;
	long IntArrayCount = 0;
	DATA_POOL *DataPool = NULL;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		if (ArrayCount < CurSubArray->InitPoolSize)
		{
			IntArrayCount = CurSubArray->InitPoolSize;
		} /* end if */
		else
		{
			IntArrayCount = ArrayCount;
		} /* end else */
		/* dbg(TRACE,"CHECK DATA POOL FOR %d LINES",IntArrayCount); */
		DataPool = CurSubArray->DataPool;
		if (DataPool == NULL)			
		{
			DataPool = (DATA_POOL *)malloc(sizeof(DATA_POOL)*(IntArrayCount+1));
			if (DataPool != NULL)
			{
				DataPool->DataLine = NULL;
				DataPool->OrigLine = NULL;
				CurSubArray->DataPool = DataPool;
				CurSubArray->UsedLines = 0;
				CurSubArray->FreeLines = IntArrayCount;
				for (LineIdx = CurSubArray->UsedLines; LineIdx <= IntArrayCount; LineIdx++)
				{
					DataPool[LineIdx].DataLine = NULL;
					DataPool[LineIdx].OrigLine = NULL;
				} /* end for */
				/* dbg(TRACE,"DATA POOL ALLOCATED"); */
			} /* end if */
		} /* end if */
		if (DataPool != NULL)
		{
			if (CurSubArray->FreeLines < ArrayCount)
			{
				IntArrayCount += CurSubArray->UsedLines-1;
				DataPool = (DATA_POOL *)realloc(DataPool,sizeof(DATA_POOL)*(IntArrayCount+1));
				if (DataPool != NULL)
				{
					for (LineIdx = CurSubArray->UsedLines; LineIdx <= IntArrayCount; LineIdx++)
					{
						DataPool[LineIdx].DataLine = NULL;
						DataPool[LineIdx].OrigLine = NULL;
					} /* end for */
					LineIdx = CurSubArray->UsedLines;
					CurSubArray->FreeLines = IntArrayCount - CurSubArray->UsedLines;
					/* dbg(TRACE,"DATA POOL RE-ALLOCATED"); */
				} /* end if */
				CurSubArray->DataPool = DataPool;
			} /* end if */
		} /* end if */
		if (DataPool != NULL)
		{
			LineIdx = CurSubArray->UsedLines;
			CurSubArray->UsedLines++;
			if(CurSubArray->FreeLines > 0)
			{
				CurSubArray->FreeLines--;
			}
			CurDataPool = DataPool;
			CurLineIdx = LineIdx;
		} /* end if */
		else
		{
			CurDataPool = NULL;
			CurLineIdx = -1;
		} /* end else */
		/* dbg(TRACE,"CT LINE IDX  =%d",LineIdx); */
		/* dbg(TRACE,"CT USED LINES=%d",CurSubArray->UsedLines); */
		/* dbg(TRACE,"CT FREE LINES=%d",CurSubArray->FreeLines); */
	} /* end if */
	return LineIdx;
} /* CreateDataPool */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_InsertTextLine(char* ArrayName, char *LineData)
{
	long CurStrLen = -1;
	long LineIdx = -1;
	LineIdx = CreateDataPool(ArrayName, 1);
	CurStrLen = CreateDataLine(LineData, LineIdx);
	return LineIdx;
} /* End CT_InsertTextLine */


/* ============================================================= 
MWO: 11.09.2002
Get The SubArray pointer by Name
  ============================================================= */
SUB_ARRAY *GetSubArray(char* ArrayName)
{
	SUB_ARRAY *prlDummy = NULL;
	int i;
	if (MainArray != NULL)
	{
		for ( i = 0; i <= MainArray->UsedArrays; i++)
		{
			if(strcmp(MainArray->SubArray[i].ArrayName, ArrayName) == 0)
			{
				CurSubArray = &MainArray->SubArray[i];
				return &MainArray->SubArray[i];
			}
		}
	}

	return prlDummy;
}

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_InsertTextLineAt(char* ArrayName, long LineNo, char *LineData)
{
	long CurStrLen = -1;
	long LineIdx = -1;
	long NewLine = -1;
	NewLine = CreateDataPool(ArrayName, 1);
	if (CurDataPool != NULL)
	{
		LineIdx = LineNo;
		if (LineIdx < 0)
		{
			LineIdx = 0;
		} /* end if */
		if (LineIdx >= CurSubArray->UsedLines)
		{
			LineIdx = CurSubArray->UsedLines-1;
		} /* end if */
		if (LineIdx < CurSubArray->UsedLines)
		{
			ShiftDataPool(LineIdx);
			CurStrLen = CreateDataLine(LineData, LineIdx);
		} /* end if */
	} /* end if */
	return LineIdx;
} /* End CT_InsertTextLineAt */

/* ============================================================= 
MWO: 12.09.2002
CT_Sort for an index
================================================================ */
long CT_SortIndex(char* ArrayName, char* IndexName)
{
	SUB_ARRAY *pSub = NULL;
	INDEX_POOL *Pool;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		Pool =  GetIndexPoolByName(ArrayName, IndexName);
		if(Pool != NULL)
		{
			qsort((INDEX_WRAPPER *)Pool->IndexObject,Pool->UsedLines ,sizeof(INDEX_WRAPPER),CompareIndex);
		}
	}
	return 0L;
}

/* ============================================================= 
MWO: 12.09.2002
return an INDEX_POOL pointer with Name
================================================================ */
INDEX_POOL * GetIndexPoolByName(char* ArrayName, char* IndexName)
{
	long i;
	SUB_ARRAY *pSub = NULL;
	INDEX_POOL *Pool = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		for(i = 0; i < pSub->UsedIndexes; i++)
		{
			Pool = &pSub->IndexPool[i];
			if(strcmp(Pool->IndexName, IndexName) == 0)
			{
				return &pSub->IndexPool[i];
			}
		}
	}
	return NULL;
}
/* ============================================================= 
MWO: 20.09.2002
 ============================================================= */
long CT_Sort(char* ArrayName, char* pcpColumns, int ipCT_SortMethod)
{
	SUB_ARRAY	*pSub = NULL;
	long		i=0,
				j=0;
	long		MaxLine = 0;
	long		ilCount=0;
	char		pclTmpValue[4096];
	char		pclTmp[2048];
	int			ilColumn;

	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		if(strlen(pcpColumns) > 0)
		{
			ilCount = CT_CountPattern(pcpColumns, ",");
			MaxLine = pSub->UsedLines;
			for(i = 0; i < MaxLine; i++)
			{
				pclTmpValue[0] = 0x00;
				for(j = 0; j <= ilCount; j++)
				{
					CT_GetDataItem( pclTmp, pcpColumns, j, ',');
    					/* No Trim */
					if(strlen(pclTmp) > 0)
					{
						ilColumn = atoi(pclTmp);
						CT_GetDataItem( pclTmp, pSub->DataPool[i].DataLine->Values, ilColumn,  ',');
    						/* No Trim */
						if(strlen(pclTmp) > 0)
						{
/* QUICK HACK */
strcpy(pclTmpValue,pclTmp);
							/*
							TEMPORARYLY DEACTIVATED
							AdjustStringToCT_SortLen(pSub, ilColumn, pclTmp, pclTmp);
							strcat(pclTmpValue, pclTmp);
							strcat(pclTmpValue, ",");
							*/	
						}
					}
				}
				sprintf(pclTmp, "%10ld", i);
				strcat(pclTmpValue, pclTmp);
				if(pSub->DataPool[i].DataLine->SortString == NULL)
				{
					pSub->DataPool[i].DataLine->SortString = (char*)malloc(strlen(pclTmpValue)+1);
				}
				else
				{
					pSub->DataPool[i].DataLine->SortString  = (char*)realloc(pSub->DataPool[i].DataLine->SortString, strlen(pclTmpValue)+1);
				}
				strcpy(pSub->DataPool[i].DataLine->SortString , pclTmpValue);
			}
			if(ipCT_SortMethod == SORT_DESC)
			{
				qsort((DATA_POOL *)pSub->DataPool,pSub->UsedLines,sizeof(DATA_POOL),CT_SortDescend);
			}
			if(ipCT_SortMethod == SORT_ASC)
			{
				qsort((DATA_POOL *)pSub->DataPool,pSub->UsedLines,sizeof(DATA_POOL),CT_SortAscend);
			}
		}
	}
	return 0L;
}

/* ============================================================= 
MWO: 20.09.2002
CT_Sort by column names (instead of column numbers)
 ============================================================= */
long CT_SortByColumnNames(char* ArrayName, char* pcpColumns, int ipCT_SortMethod)
{
	SUB_ARRAY	*pSub = NULL;
	long		i=0,
				j=0;
	long		MaxLine = 0;
	long		ilCount=0;
	char		pclTmpValue[4096];
	char		pclTmp[2048];
	int			ilColumn;

	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		if(strlen(pcpColumns) > 0)
		{
			ilCount = CT_CountPattern(pcpColumns, ",");
			MaxLine = pSub->UsedLines;
			for(i = 0; i < MaxLine; i++)
			{
				pclTmpValue[0] = 0x00;
				for(j = 0; j <= ilCount; j++)
				{
					CT_GetDataItem( pclTmp, pcpColumns, j, ',');
    						/* No Trim */
					if(strlen(pclTmp) > 0)
					{
						ilColumn = GetColumnNameIndex(ArrayName, pclTmp);
						CT_GetDataItem( pclTmp, pSub->DataPool[i].DataLine->Values, ilColumn,  ',');
    						/* No Trim */
						if(strlen(pclTmp) > 0)
						{
/* QUICK HACK */
strcpy(pclTmpValue,pclTmp);
							/*
							TEMPORARYLY DEACTIVATED
							AdjustStringToCT_SortLen(pSub, ilColumn, pclTmp, pclTmp);
							strcat(pclTmpValue, pclTmp);
							strcat(pclTmpValue, ",");
							*/
						}
					}
				}
				sprintf(pclTmp, "%10ld", i);
				strcat(pclTmpValue, pclTmp);
				if(pSub->DataPool[i].DataLine->SortString == NULL)
				{
					pSub->DataPool[i].DataLine->SortString = (char*)malloc(strlen(pclTmpValue)+1);
				}
				else
				{
					pSub->DataPool[i].DataLine->SortString = (char*)realloc(pSub->DataPool[i].DataLine->SortString, strlen(pclTmpValue)+1);
				}
				strcpy(pSub->DataPool[i].DataLine->SortString, pclTmpValue);
			}
			if(ipCT_SortMethod == SORT_DESC)
			{
				qsort((DATA_POOL *)pSub->DataPool,pSub->UsedLines,sizeof(DATA_POOL),CT_SortDescend);
			}
			if(ipCT_SortMethod == SORT_ASC)
			{
				qsort((DATA_POOL *)pSub->DataPool,pSub->UsedLines,sizeof(DATA_POOL),CT_SortAscend);
			}
		}
	}
	return 0L;
}


/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static void ShiftDataPool(long LineNo)
{
	long CurLine = 0;
	long MaxLine = 0;
	DATA_POOL HelpLine;
	MaxLine = CurSubArray->UsedLines-1;
	HelpLine = CurDataPool[MaxLine];
	for (CurLine = MaxLine; CurLine > LineNo; CurLine--)
	{
		CurDataPool[CurLine] = CurDataPool[CurLine-1];
	} /* end for */
	CurDataPool[LineNo] = HelpLine;
	return;
} /* End ShiftDataPool */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_InsertBuffer(char* ArrayName, char *BufferData, char *LineSeparator)
{
	long CurStrLen = -1;
	long LineIdx = -1;
	long LineCount = 0;
	long PatLen = 0;
	char *BgnPtr = NULL;
	char *EndPtr = NULL;
	LineCount = CT_CountPattern(BufferData, LineSeparator) + 1;
	/* dbg(TRACE,"CT INSERTING %d LINES",LineCount); */
	LineIdx = CreateDataPool(ArrayName, LineCount);
	/* dbg(TRACE,"CT DATA POOL FIRST LINE IDX=%d ",LineIdx); */
	if (LineIdx >= 0)
	{
		PatLen = strlen(LineSeparator);
		BgnPtr = BufferData;
		EndPtr = BgnPtr;
		CurSubArray->UsedLines--;
		CurSubArray->FreeLines++;
		while (EndPtr != NULL)
		{
			EndPtr = strstr(BgnPtr, LineSeparator);
			if (EndPtr != NULL)
			{
				*EndPtr = '\0';
			} /* end if */
			CurStrLen = CreateDataLine(BgnPtr, LineIdx);
			CurSubArray->UsedLines++;
			CurSubArray->FreeLines--;
			/* dbg(TRACE,"CT LINE CREATED LEN=%d",CurStrLen); */
			LineIdx++;
			if (EndPtr != NULL)
			{
				*EndPtr = LineSeparator[0];
				BgnPtr = EndPtr + PatLen;
			} /* end if */
		} /* end while */
		/* dbg(TRACE,"CT USED LINES=%d",CurSubArray->UsedLines); */
		/* dbg(TRACE,"CT FREE LINES=%d",CurSubArray->FreeLines); */
	} /* end if */
	else
	{
		LineCount = -1;
	} /* end else */
	return LineCount;
} /* End CT_InsertBuffer */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_CountPattern(char *Buffer, char *Pattern)
{
	long Count = 0;
	long PatLen = 0;
	char *Ptr = NULL;
	PatLen = strlen(Pattern);
	Ptr = Buffer;
	while (Ptr != NULL)
	{
		Ptr = strstr(Ptr, Pattern);
		if (Ptr != NULL)
		{
			Count++;
			Ptr += PatLen;
		} /* end if */
	} /* end while */
	return Count;
} /* End */


/* ============================================================= 
BST: ??.??.????
MWO: 12.09.2002 Modified
 ============================================================= */
static long CreateDataLine(char *LineData, long LineIdx)
{
	long CurStrLen = -1;
	long CurLen = 0;
	DATA_LINE *DataLine = NULL;
	DATA_LINE *OrigLine = NULL;
	if (CurDataPool != NULL)
	{
		DataLine = CurDataPool[LineIdx].DataLine;
		if (DataLine == NULL)
		{
			DataLine = (DATA_LINE *)malloc(sizeof(DATA_LINE));
			if (DataLine != NULL)
			{
				DataLine->MemorySize = 0;
				DataLine->StringSize = 0;
				DataLine->Values = NULL;
				DataLine->LineTag = NULL;
				DataLine->AnyList = NULL;
				DataLine->AnyData = NULL;
				DataLine->OldData = NULL;
				DataLine->ColStats = NULL;
				DataLine->ColProps = NULL;
				DataLine->SortString = NULL;
				CurDataPool[LineIdx].DataLine = DataLine;
			} /* end if */
		} /* end if */
		if (DataLine != NULL)
		{
			DataLine->LineChecked = 0;
			DataLine->StatusValue = 0;
			DataLine->DbInsFlag = 0;
			DataLine->DbUpdFlag = 0;
			DataLine->DbDelFlag = 0;
			DataLine->FontColor = 0;
			DataLine->BackColor = 0;
			DataLine->TimeValue = 0;
			if (DataLine->LineTag != NULL)
            {
				DataLine->LineTag[0] = 0x00;
            }
			if (DataLine->AnyList != NULL)
            {
				DataLine->AnyList[0] = 0x00;
            }
			if (DataLine->AnyData != NULL)
            {
				DataLine->AnyData[0] = 0x00;
            }
			if (DataLine->OldData != NULL)
            {
				DataLine->OldData[0] = 0x00;
            }
			CurStrLen = strlen(LineData);
			if (DataLine->Values == NULL)
			{
				DataLine->Values = (char *)malloc(CurStrLen+1);
				if (DataLine->Values != NULL)
				{
					DataLine->MemorySize = CurStrLen+1;
				} /* end if */
			} /* end if */
			if (DataLine->Values != NULL)
			{
				if (DataLine->MemorySize <= CurStrLen)
				{
					DataLine->Values = (char *)realloc(DataLine->Values,CurStrLen+1);
					if (DataLine->Values != NULL)
					{
						DataLine->MemorySize = CurStrLen+1;
					} /* end if */
				} /* end if */
			} /* end if */
			if (DataLine->Values != NULL)
			{
				strcpy(DataLine->Values,LineData);
				DataLine->StringSize = CurStrLen;
				CurDataLine = DataLine;
			} /* end if */
			else
			{
				CurDataLine = NULL;
				CurStrLen = -1;
			} /* end else */
			if (CurSubArray->TrackChanges > 0)
			{
				OrigLine = CurDataPool[LineIdx].OrigLine;
				if (OrigLine == NULL)
				{
					OrigLine = (DATA_LINE *)malloc(sizeof(DATA_LINE));
				} /* end if */
				if (OrigLine != NULL)
				{
					OrigLine->LineTag = NULL;
					OrigLine->AnyList = NULL;
					OrigLine->AnyData = NULL;
					OrigLine->OldData = NULL;
					OrigLine->SortString = NULL;
					OrigLine->MemorySize = 0;
					OrigLine->StringSize = 0;
					OrigLine->Values = NULL;
					OrigLine->Values = (char *)realloc(OrigLine->Values,DataLine->MemorySize);
					if (OrigLine->Values != NULL)
					{
						strcpy(OrigLine->Values,DataLine->Values);
						OrigLine->StringSize = DataLine->StringSize;
						OrigLine->MemorySize = DataLine->MemorySize;
					} /* end if */
					CurDataPool[LineIdx].OrigLine = OrigLine;
					if (CurSubArray->FieldCount > 0)
					{
						CurLen = CurSubArray->FieldCount;
						DataLine->ColStats = (char *)realloc(DataLine->ColStats,CurLen);
						if (DataLine->ColStats != NULL)
						{
							memset(DataLine->ColStats, ' ', CurLen);
							DataLine->ColStats[CurLen-1] = 0x00;
						}
					}
				} /* end if */
				DataLine->DbInsFlag = 0; /* Must be set by caller */
				DataLine->DbUpdFlag = 0;
				DataLine->DbDelFlag = 0;
			}
		} /* end if */
	} /* end if */
	return CurStrLen;
} /* End CreateDataLine */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
void CT_DeleteDataLine(char* ArrayName, long LineNo)
{
	SUB_ARRAY *pSub = NULL;
	long LineIdx = -1;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				ShrinkDataPool(ArrayName, LineNo);
				pSub->UsedLines--;
				pSub->FreeLines++;
			} /* end if */
		} /* end if */
	}
	return;
} /* End */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static void ShrinkDataPool(char* ArrayName, long LineNo)
{
	long CurLine = 0;
	long MaxLine = 0;
	DATA_POOL HelpLine;
	MaxLine = CurSubArray->UsedLines-1;
	HelpLine = CurDataPool[LineNo];
	for (CurLine = LineNo; CurLine < MaxLine; CurLine++)
	{
		CurDataPool[CurLine] = CurDataPool[CurLine+1];
	} /* end for */
	CurDataPool[MaxLine] = HelpLine;
	return;
} /* End ShiftDataPool */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_GetCurrentDataLine(char* ArrayName, char *LineData)
{
	long CurStrLen = -1;
	LineData[0] = 0x00;
	if (CurDataLine != NULL)
	{
		if (CurDataLine->Values != NULL)
		{
			strcpy(LineData,CurDataLine->Values);
			CurStrLen = CurDataLine->StringSize;
		} /* end if */
	} /* end if */
	return CurStrLen;
} /* End CT_GetCurrentDataLine */

/* ============================================================= 
MWO: 10.09.2002
 ============================================================= */
long CT_GetLineValues(char* ArrayName, long LineNo, STR_DESC* prpString)
{
	long LineIdx = -1;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if (pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				LineIdx = LineNo;
				CurDataLine = CurDataPool[LineIdx].DataLine;
				if(prpString->AllocatedSize < CurDataLine->StringSize)
				{
					if (prpString->AllocatedSize <= 0)
					{
						prpString->Value = (char*)malloc(CurDataLine->StringSize+1);
					}
					else
					{
						prpString->Value = (char*)realloc(prpString->Value,CurDataLine->StringSize+1);
					}
					prpString->AllocatedSize = CurDataLine->StringSize+1;
				}
				strcpy(prpString->Value ,CurDataLine->Values);
				prpString->UsedLen = CurDataLine->StringSize;
			} /* end if */
		} /* end if */
	} /* end if */
	return LineIdx;
} /* End CT_GetLineValues */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
long CT_SeekDataPool(char* ArrayName, long LineNo)
{
	long LineIdx = -1;
	if (CurDataPool != NULL)
	{
		LineIdx = LineNo;
		if (LineIdx < 0)
		{
			LineIdx = -1;
		} /* end if */
		if (LineIdx >= CurSubArray->UsedLines)
		{
			LineIdx = CurSubArray->UsedLines-1;
		} /* end if */
		CurLineIdx = LineIdx;
		CurDataLine = CurDataPool[CurLineIdx].DataLine;
	} /* end if */
	return LineIdx;
} /* End CT_SeekDataPool */

/* ============================================================= 
MWO: Needed anymore ???
 ============================================================= */
long CT_GetNextDataLine(char* ArrayName, char *LineData)
{
	long CurStrLen = -1;
	LineData[0] = 0x00;
	if (CurDataPool != NULL)
	{
		if (CurLineIdx < 0)
		{
			CurLineIdx = -1;
		} /* end if */
		if (CurLineIdx >= CurSubArray->UsedLines)
		{
			CurLineIdx = CurSubArray->UsedLines-1;
		} /* end if */
		CurLineIdx++;
		if (CurLineIdx < CurSubArray->UsedLines)
		{
			CurDataLine = CurDataPool[CurLineIdx].DataLine;
			if (CurDataLine != NULL)
			{
				if (CurDataLine->Values != NULL)
				{
					/* Test only */
					sprintf(LineData,"(%02d) <%s>",CurLineIdx,CurDataLine->Values);
					CurStrLen = CurDataLine->StringSize;
				} /* end if */
			} /* end if */
			else
			{
				/* Test only */
				sprintf(LineData,"(%02d) <NULL>",CurLineIdx);
			} 
		} /* end if */
		else
		{
			/* Test only */
			sprintf(LineData,"(%02d) <UNUSED>",CurLineIdx);
		} 

	} /* end if */
	return CurStrLen;
} /* End CT_GetNextDataLine */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to set the fieldlist of spec. ArrayName
 ============================================================= */
long CT_SetFieldList(char* ArrayName, char *FieldList)
{
	long CurStrLen = -1;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		CurStrLen = strlen(FieldList) + 1;
		if (CurSubArray->FieldList == NULL)
		{
			CurSubArray->FieldList = (char *)malloc(CurStrLen);
		} /* end if */
		else
		{
			CurSubArray->FieldList = (char *)realloc(CurSubArray->FieldList,CurStrLen);
		} /* end else */
		if (CurSubArray->FieldList != NULL)
		{
			strcpy(CurSubArray->FieldList,FieldList);
			CurSubArray->FieldCount = CT_CountPattern(FieldList, ",") + 1;
		} /* end if */
		else
		{
			CurStrLen = -1;
			CurSubArray->FieldCount = 0;
		} /* end else */
	} /* end if */
	return CurStrLen;
} /* End CT_SetFieldList */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long CT_GetFieldList(char* ArrayName, char *FieldList)
{
	long CurStrLen = -1;
	FieldList[0] = 0x00;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		if (CurSubArray->FieldList != NULL)
		{
			strcpy(FieldList, CurSubArray->FieldList);
			CurStrLen = strlen(FieldList);
		} /* end if */
	} /* end if */
	return CurStrLen;
} /* End CT_GetFieldList */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long CT_SetLengthList(char* ArrayName, char *LengthList)
{
	int i = 0;
	long count=0;
	long CurStrLen = -1;
	char pclTmp[100];
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		for(i = 0; i < 1000; i++)
		{
			CurSubArray->llLengthList[i] = 16;
			CurSubArray->AlignmentList[i]='L';
		}
		CurStrLen = strlen(LengthList);
		if (CurStrLen > 0)
		{
			count = CT_CountPattern(LengthList, ",");
			for(i = 0; i <= CT_CountPattern(LengthList, ","); i++)
			{
				CT_GetDataItem( pclTmp, LengthList, i, ',');
				CurSubArray->llLengthList[i] = atoi(pclTmp);
			}
		}
	} /* end if */
	return count+1;
} /* End CT_SetLengthList */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long CT_GetLengthList(char* ArrayName, char *LengthList)
{
	long CurStrLen = -1;
/*	LengthList[0] = 0x00;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		if (CurSubArray->LengthList != NULL)
		{
			strcpy(LengthList, CurSubArray->LengthList);
			CurStrLen = strlen(LengthList);
		} 
	} 
*/
	return CurStrLen;
} 


/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long SetArrayProperties(char* ArrayName, char *Properties)
{
	long CurStrLen = -1;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		CurStrLen = strlen(Properties) + 1;
		if (CurSubArray->Properties == NULL)
		{
			CurSubArray->Properties = (char *)malloc(CurStrLen);
		} /* end if */
		else
		{
			CurSubArray->Properties = (char *)realloc(CurSubArray->Properties,CurStrLen);
		} /* end else */
	} /* end if */
	if (CurSubArray->Properties != NULL)
	{
		strcpy(CurSubArray->Properties,Properties);
	} /* end if */
	else
	{
		CurStrLen = -1;
	} /* end else */
	return CurStrLen;
} /* End SetArrayProperties */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long GetArrayProperties(char* ArrayName, char *Properties)
{
	long CurStrLen = -1;
	Properties[0] = 0x00;
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		if (CurSubArray->Properties != NULL)
		{
			strcpy(Properties, CurSubArray->Properties);
			CurStrLen = strlen(Properties);
		} /* end if */
	} /* end if */
	return CurStrLen;
} /* End GetArrayProperties */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
void CT_SetDataPoolAllocSize(char *ArrayName, long AllocSize)
{
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		if (AllocSize > 0)
		{
			CurSubArray->InitPoolSize = AllocSize;
		} /* end if */
	} /* end if */
	return;
} /* End SetDefDataPoolSteps */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static int CT_SortAscend(const void *_dp1, const void *_dp2)
{
	const DATA_POOL *dp1 = (const DATA_POOL *) _dp1;
	const DATA_POOL *dp2 = (const DATA_POOL *) _dp2;
	DATA_LINE *dl1 = dp1->DataLine;
	DATA_LINE *dl2 = dp2->DataLine;
	return (strcmp(dl1->SortString,dl2->SortString));
} /* End */

/* ============================================================= 
BST: ??.??.????
 ============================================================= */
static int CT_SortDescend(const void *_dp1, const void *_dp2)
{
	const DATA_POOL *dp1 = (const DATA_POOL *) _dp1;
	const DATA_POOL *dp2 = (const DATA_POOL *) _dp2;
	DATA_LINE *dl1 = dp1->DataLine;
	DATA_LINE *dl2 = dp2->DataLine;
	return -(strcmp(dl1->SortString,dl2->SortString));
} /* End */

/* ============================================================= 
BST: ??.??.????
MWO: 19.09.2002 Modified to get the fieldlist of spec. ArrayName
 ============================================================= */
long CT_ResetContent(char* ArrayName)
{
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		CurSubArray->FreeLines += CurSubArray->UsedLines;
		CurSubArray->UsedLines = 0;
		return CurSubArray->FreeLines;
	} /* end if */
	return -1;
} /* end */


/******************************************************
 * Function:   CT_GetDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char *pcpInput
 *             IN      int   ipNum     pos. of item in list
 *             IN      char  cpDel     Delimiter
 * Return Code: >=0          number of char. in result string
 *              -1..-2       <number> argument is NULL
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *           
 * Description: Get <ipNum>th item in <cpDel> seperated list
 *              and return the number of chars as return value
 *              and the item in <pcpResult>. 
 *******************************************************/

int CT_GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel)
{
	int ilPos=0;

	if (pcpResult == NULL)
	{
		ilPos = -1;
	}
	else if (pcpInput == NULL)
	{
		ilPos = -2;
	}
	else
	{
		pcpResult[0] = '\0';
		if (ipNum > 0)
		{
			/* Search first Delimiter character */
			while(*pcpInput != '\0')
			{
				if (*pcpInput == cpDel )
				{
					ipNum--;
					if (ipNum == 0)
					{
						pcpInput++;
						break;
					}
				}
				pcpInput++;
			}/* end while*/
		}
		ilPos = 0;
		/* Copy the item value */
		while((*pcpInput != cpDel) && (*pcpInput != '\0') )
		{
			pcpResult[ilPos] = *pcpInput;
			pcpInput++;
			ilPos++;
 		}
		pcpResult[ilPos]='\0';
	} /* else end */

	return ilPos;
} /* end CT_GetDataItem */


long CT_GetDataItemSize(char *pcpInput, long ipNum, char cpDel, long *ipItemBegin)
{
	long ilFirstPos=0;
	long ilLastPos=0;

	if (pcpInput == NULL)
	{
		ilFirstPos = -2;
	}
	else
	{
		if (ipNum > 0)
		{
			/* Search first Delimiter character */
			while(pcpInput[ilFirstPos] != '\0')
			{
				if (pcpInput[ilFirstPos] == cpDel )
				{
					ipNum--;
					if (ipNum == 0)
					{
						ilFirstPos++;
						break;
					}
				}
				ilFirstPos++;
			}/* end while*/
		}
		ilLastPos = ilFirstPos;
		/* Run through the item value */
		while((pcpInput[ilLastPos] != cpDel) && (pcpInput[ilLastPos] != '\0') )
		{
			ilLastPos++;
 		}
	} /* else end */
	*ipItemBegin = ilFirstPos;
	return ilLastPos - ilFirstPos;
} /* end CT_GetDataItemSize */


int CT_TrimLeft(char *pcpString, char cpTrim, int ipSize)
{
	char *pclPtr = NULL;
	char *pclOut = NULL;
	if (*pcpString == cpTrim)
	{
		pclPtr = pcpString;
		while ((*pclPtr == cpTrim) && (*pclPtr != '\0'))
		{
			pclPtr++;
		}
		pclOut = pcpString;
		while (*pclPtr != '\0')
		{
			*pclOut = *pclPtr;
			pclOut++;
			pclPtr++;
		}
		*pclOut = 0x00;
		ipSize = pclOut - pcpString;
	}
	return ipSize;
} /* end CT_TrimLeft */

int CT_TrimRight(char *pcpString, char cpTrim, int ipSize)
{
	char *pclPtr = NULL;
	if (ipSize < 0)
	{
		ipSize = strlen(pcpString);
	}
	pclPtr = pcpString + ipSize -1;
	while ((pclPtr >= pcpString) && (*pclPtr == cpTrim))
	{
		pclPtr--;
	}
	pclPtr++;
	*pclPtr = 0x00;
	ipSize = pclPtr - pcpString;
	return ipSize;
} /* end CT_TrimRight */

int CT_TrimRightDef(char *pcpString, char cpTrim, int ipSize, char *pcpDef)
{
	char *pclPtr = NULL;
	if (ipSize < 0)
	{
		ipSize = strlen(pcpString);
	}
	pclPtr = pcpString + ipSize -1;
	while ((pclPtr >= pcpString) && (*pclPtr == cpTrim))
	{
		pclPtr--;
	}
	pclPtr++;
	*pclPtr = 0x00;
	ipSize = pclPtr - pcpString;
	if (ipSize == 0)
    {
		strcpy(pcpString,pcpDef);
		ipSize = strlen(pcpString);
    }
	return ipSize;
} /* end CT_TrimRight */

/* ============================================================= 
MWO: 22.09.2002
   ============================================================= */
long CT_GetColumnValue(char* ArrayName, long LineNo, long ColNo,  STR_DESC* prpString)
{
	long ItemLength = -1;
        long ItemBegin = -1;
	char *pclPtr = NULL;
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				ItemLength = CT_GetDataItemSize(CurDataLine->Values, ColNo, pSub->FieldSeparator, &ItemBegin); 
				if(prpString->AllocatedSize < ItemLength+1)
				{
					if (prpString->AllocatedSize <= 0)
					{
						prpString->Value = (char*)malloc(ItemLength+1);
					}
					else
					{
						prpString->Value = (char*)realloc(prpString->Value,ItemLength+1);
					}
					prpString->AllocatedSize = ItemLength+1;
				}
				pclPtr = (char *)CurDataLine->Values + ItemBegin;
				strncpy(prpString->Value,pclPtr,ItemLength);
				prpString->Value[ItemLength] = 0x00;
				prpString->UsedLen = ItemLength;
				CT_CheckTrim(prpString->Value, pSub->TrimMethod, " ");
			} /* end if */
		} /* end if */
	} /* end if */
	return ItemLength;
}

/* ============================================================= 
MWO: 22.09.2002
   ============================================================= */
long GetColumnNameIndex(char* ArrayName, char* ColName)
{
	long	ilItemNo = 0;
	long	PatLen = 0;
	char	*Ptr = NULL;
	char	*NextPtr = NULL;
	SUB_ARRAY *pSub = NULL;
	PatLen = strlen(",");
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		if(pSub->FieldList != NULL)
		{
			ilItemNo = CT_GetItemNo(pSub->FieldList, ColName, ",");
			return ilItemNo;
		}
	}
	return -1;
}

/* ============================================================= 
BST: 18.03.2010
   ============================================================= */
long CT_GetColumnNameIndex(char* ArrayName, char* ColName)
{
	long	llItemNo = -1;
	long	PatLen = 0;
	char	*Ptr = NULL;
	char	*NextPtr = NULL;
	SUB_ARRAY *pSub = NULL;
	PatLen = strlen(",");
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		if(pSub->FieldList != NULL)
		{
			llItemNo = CT_GetItemNo(pSub->FieldList, ColName, ",");
		}
	}
	return llItemNo;
}

/* ============================================================= 
MWO: 22.09.2002
   ============================================================= */
long CT_GetColumnValueByName(char* ArrayName, long LineNo, char* ColName, STR_DESC *prpString)
{
	long ColIdx = -1;
	long ColNo;
	char cpResult[4096];
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if(prpString->AllocatedSize == 0)
	{
		prpString->Value = (char*)malloc((size_t)1024);
		prpString->AllocatedSize = 1024;
		prpString->UsedLen = 0;
	}
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				ColNo = GetColumnNameIndex(ArrayName, ColName);
				if(ColNo > -1)
				{
					CurDataLine = CurDataPool[LineNo].DataLine;
					ColIdx = CT_GetDataItem( cpResult, CurDataLine->Values, ColNo, ',');
    					/* No Trim */
					if(prpString->AllocatedSize < (long)strlen(cpResult))
					{
						prpString->Value = (char*)realloc(prpString->Value, strlen(cpResult)+1);
						prpString->AllocatedSize = strlen(cpResult)+1;
						prpString->UsedLen = strlen(cpResult);
					}
					strcpy(prpString->Value, cpResult);
					CT_CheckTrim(prpString->Value, pSub->TrimMethod, " ");
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return ColIdx;
}

/* ============================================================= 
BST: 16.08.2011
   ============================================================= */
int CT_CheckTrim(char *pcpStr, char *pcpHow, char *pcpWhat)
{
  int ilLen = 0;
  ilLen = strlen(pcpStr);
  switch (pcpHow[0])
  {
    case 'L':
      ilLen = CT_TrimLeft(pcpStr, pcpWhat[0], ilLen);
    break;
    case 'B':
      ilLen = CT_TrimLeft(pcpStr, pcpWhat[0], ilLen);
      ilLen = CT_TrimRight(pcpStr, pcpWhat[0], ilLen);
    break;
    case 'R':
      ilLen = CT_TrimRight(pcpStr, pcpWhat[0], ilLen);
    break;
    default:
    break;
  }
  return ilLen;
}

/* ============================================================= 
BST: 01.04.2010
   ============================================================= */
long CT_GetFieldValues(char* ArrayName, long LineNo, char* FldNames, STR_DESC *prpString)
{
	long ColIdx = -1;
	long ColNo;
	long FldNo;
	long MaxNo;
	long NewLen;
	char cpResult[4096];
	char ColName[16];
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if(prpString->AllocatedSize == 0)
	{
		prpString->Value = (char*)malloc((size_t)1024);
		prpString->AllocatedSize = 1024;
		prpString->UsedLen = 0;
	}
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				strcpy(prpString->Value, "");
				NewLen = 0;
				MaxNo = CT_CountPattern(FldNames, ",");
				for (FldNo=0;FldNo<=MaxNo;FldNo++)
				{
					ColNo = CT_GetDataItem(ColName, FldNames, FldNo, ',');
					ColNo = GetColumnNameIndex(ArrayName, ColName);
					if(ColNo > -1)
					{
						ColIdx = CT_GetDataItem(cpResult, CurDataLine->Values, ColNo, ',');
					}
					else
					{
						strcpy(cpResult,"");
					}
					NewLen += strlen(cpResult);
					NewLen++;
					if(prpString->AllocatedSize < NewLen)
					{
						prpString->Value = (char*)realloc(prpString->Value, NewLen+1024);
						prpString->AllocatedSize = NewLen+1024;
					}
					strcat(prpString->Value, cpResult);
					strcat(prpString->Value, ",");
					prpString->UsedLen = NewLen;
				}
				if (NewLen > 0)
				{
					NewLen--;
					prpString->Value[NewLen] = 0x00;
					prpString->UsedLen = NewLen;
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return MaxNo;
}

/* ============================================================= 
BST: 09.03.2011
   ============================================================= */
long CT_GetMultiValues(char* ArrayName, long LineNo, char* FldNames, char *prpString)
{
	long ColIdx = -1;
	long ColNo;
	long FldNo;
	long MaxNo;
	long NewLen;
	char cpResult[4096];
	char ColName[16];
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				strcpy(prpString, "");
				NewLen = 0;
				MaxNo = CT_CountPattern(FldNames, ",");
				for (FldNo=0;FldNo<=MaxNo;FldNo++)
				{
					ColNo = CT_GetDataItem(ColName, FldNames, FldNo, ',');
					ColNo = GetColumnNameIndex(ArrayName, ColName);
					if(ColNo > -1)
					{
						ColIdx = CT_GetDataItem(cpResult, CurDataLine->Values, ColNo, ',');
					}
					else
					{
						strcpy(cpResult,"");
					}
					CT_CheckTrim(cpResult, pSub->TrimMethod, " ");
					NewLen += strlen(cpResult);
					NewLen++;
					strcat(prpString, cpResult);
					strcat(prpString, ",");
				}
				if (NewLen > 0)
				{
					NewLen--;
					prpString[NewLen] = 0x00;
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return MaxNo;
}


/* ============================================================= 
BST: 08.03.2011
   ============================================================= */
long CT_SetFieldValues(char *ArrayName, long LineNo, char *NameList, char *DataList)
{
	long ColIdx = -1;
	long ColNo;
	long FldNo;
	long MaxNo;
	long NamLen;
	long DatLen;
	char FldName[16];
	char FldData[4096];
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				MaxNo = CT_CountPattern(NameList, ",");
				for (FldNo=0;FldNo<=MaxNo;FldNo++)
				{
					NamLen = CT_GetDataItem(FldName, NameList, FldNo, ',');
					DatLen = CT_GetDataItem(FldData, DataList, FldNo, ',');
					ColNo = GetColumnNameIndex(ArrayName, FldName);
					if(ColNo > -1)
					{
						CT_SetColumnValue(ArrayName, LineNo, ColNo, FldData);
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return MaxNo;
}

/* ============================================================= 
BST: 11.08.2011
   ============================================================= */
long CT_SetMultiValues(char *ArrayName, long LineNo, char *NameList, char *DataList)
{
	long ColIdx = -1;
	long ColNo;
	long FldNo;
	long MaxNo;
	long NamLen;
	long DatLen;
	char FldName[16];
	char FldData[4096];
	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				MaxNo = CT_CountPattern(NameList, ",");
				for (FldNo=0;FldNo<=MaxNo;FldNo++)
				{
					NamLen = CT_GetDataItem(FldName, NameList, FldNo, ',');
					DatLen = CT_GetDataItem(FldData, DataList, FldNo, ',');
					ColNo = GetColumnNameIndex(ArrayName, FldName);
					if(ColNo > -1)
					{
						CT_SetColumnValue(ArrayName, LineNo, ColNo, FldData);
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return MaxNo;
}


/* ============================================================= 
MWO: 12.09.2002
================================================================ */
long CT_SetColumnValue(char* ArrayName, long LineNo, long ColNo, char* pcpColumnValue)
{
	long llRet = 0;
	SUB_ARRAY *pSub = NULL;
	char *cpItemValue;
	long llCurrDataLineLen = 0;
	long llOrigDataLineLen = 0;
	long llNewDataLineLen = 0;
	long llLenDiff = 0;
	long llItemCount = 0;
	long llNewLineDataSize = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				llItemCount = CT_CountPattern(CurDataLine->Values, ",");
				if (ColNo <= llItemCount )
				{
					llCurrDataLineLen = strlen(CurDataLine->Values);
					cpItemValue = (char*)malloc(llCurrDataLineLen + 1);
    				/* No Trim */
					CT_GetDataItem(cpItemValue, CurDataLine->Values, ColNo, ',');
					strcpy(pcgPrevData,cpItemValue);
					llLenDiff = strlen(pcpColumnValue) - strlen(cpItemValue);
					if(llLenDiff > 0) 
					{/* Then we have to realloc the line*/
						CurDataLine->Values = (char*)realloc(CurDataLine->Values, llCurrDataLineLen+llLenDiff+1);
						CurDataLine->MemorySize =  llCurrDataLineLen+llLenDiff+1;
					}
					if(lgLineStrLen < CurDataLine->MemorySize)
					{
						pcgLineStr = (char*)realloc(pcgLineStr, CurDataLine->MemorySize+1);
						lgLineStrLen = CurDataLine->MemorySize+1;
					}
					strcpy(pcgLineStr, CurDataLine->Values);
					if ( ColNo == 0 )
					{/*first item*/
						strcpy(CurDataLine->Values,pcpColumnValue);
						strcat(CurDataLine->Values, ",");
						CT_GetItemsFromTo(cpItemValue, pcgLineStr, ColNo+1, llItemCount, ","); 
						strcat(CurDataLine->Values, cpItemValue);
					}
					else if ( ColNo == llItemCount)
					{/*last item*/
						CT_GetItemsFromTo(CurDataLine->Values, pcgLineStr, 0, ColNo-1, ",");
						strcat(CurDataLine->Values, ",");
						strcat(CurDataLine->Values,pcpColumnValue);
					}
					else
					{
						CT_GetItemsFromTo(CurDataLine->Values, pcgLineStr, 0, ColNo-1, ",");
						strcat(CurDataLine->Values, ",");
						strcat(CurDataLine->Values, pcpColumnValue);
						strcat(CurDataLine->Values, ",");
						CT_GetItemsFromTo(cpItemValue, pcgLineStr, ColNo+1, llItemCount, ","); 
						strcat(CurDataLine->Values, cpItemValue);
					}
					if (CurSubArray->TrackChanges > 0)
					{
						if (ColNo < CurSubArray->FieldCount)
						{
							if (CurDataLine->ColStats != NULL)
							{
								OrgDataLine = CurDataPool[LineNo].OrigLine;
								if (OrgDataLine != NULL)
								{
									strcpy(pcgCurrData,pcpColumnValue);
									CT_TrimRight(pcgCurrData, ' ', -1);
									CT_TrimRight(pcgPrevData, ' ', -1);
									if (strcmp(pcgCurrData,pcgPrevData) != 0)
									{
										CT_GetDataItem(pcgOrigData, OrgDataLine->Values, ColNo, ',');
										CT_TrimRight(pcgOrigData, ' ', -1);
										if (strcmp(pcgCurrData,pcgOrigData) != 0)
										{
											if (CurDataLine->ColStats[ColNo] != 'M')
											{
												CurDataLine->ColStats[ColNo] = 'M';
												CurDataLine->DbUpdFlag++;
											}
										}
										else
										{
											if (CurDataLine->ColStats[ColNo] != ' ')
											{
												CurDataLine->ColStats[ColNo] = ' ';
												if (CurDataLine->DbUpdFlag > 0)
												{
													CurDataLine->DbUpdFlag--;
												}
											}
										}
									}
								}
							}
						}
					}
					free (cpItemValue);
				}
			}
		}
	}
	return llRet;
}
/* ============================================================= 
MWO: 12.09.2002
================================================================ */
long CT_SetColumnValueByName(char* ArrayName, long LineNo, char* ColName, char* pcpColumnValue)
{
	long llRet = 0;
	long ColNo = -1;

	SUB_ARRAY *pSub = NULL;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		ColNo = GetColumnNameIndex(ArrayName, ColName);
		if(ColNo > -1)
		{
			CT_SetColumnValue(ArrayName, LineNo, ColNo, pcpColumnValue);
		}
	}

	return ColNo;
}


/* ============================================================= 
BST: 17.03.2010
================================================================ */
long CT_SetLineTag(char* ArrayName, long LineNo, char* pcpTag)
{
	long llRet = 0;
	SUB_ARRAY *pSub = NULL;
	char* cpItemValue;
	long llTagSize = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				llTagSize = strlen(pcpTag);
				CurDataLine->LineTag = (char*)realloc(CurDataLine->LineTag,llTagSize+1);
				strcpy(CurDataLine->LineTag,pcpTag);
			}
		}
	}
	return llRet;
}
long CT_GetLineTag(char* ArrayName, long LineNo, STR_DESC* prpString)
{
	long LineIdx = -1;
	long llTagSize = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if (pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				LineIdx = LineNo;
				CurDataLine = CurDataPool[LineIdx].DataLine;
				if (CurDataLine->LineTag != NULL)
				{
					llTagSize = strlen(CurDataLine->LineTag) + 1;
					if(prpString->AllocatedSize < llTagSize)
					{
						if (prpString->AllocatedSize <= 0)
						{
							prpString->Value = (char*)malloc(llTagSize);
						}
						else
						{
							prpString->Value = (char*)realloc(prpString->Value,llTagSize);
						}
						prpString->AllocatedSize = llTagSize;
					}
					strcpy(prpString->Value ,CurDataLine->LineTag);
					prpString->UsedLen = llTagSize;
				}
				else
				{
					if (prpString->AllocatedSize > 0)
					{
						prpString->Value[0] = 0x00;
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return LineIdx;
}

long CT_SetAnyList(char* ArrayName, long LineNo, char* pcpTag)
{
	long llRet = 0;
	SUB_ARRAY *pSub = NULL;
	char* cpItemValue;
	long llTagSize = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				llTagSize = strlen(pcpTag);
				CurDataLine->AnyList = (char*)realloc(CurDataLine->AnyList,llTagSize+1);
				strcpy(CurDataLine->AnyList,pcpTag);
			}
		}
	}
	return llRet;
}
long CT_GetAnyList(char* ArrayName, long LineNo, STR_DESC* prpString)
{
	long LineIdx = -1;
	long llTagSize = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if (pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				LineIdx = LineNo;
				CurDataLine = CurDataPool[LineIdx].DataLine;
				if (CurDataLine->AnyList != NULL)
				{
					llTagSize = strlen(CurDataLine->AnyList) + 1;
					if(prpString->AllocatedSize < llTagSize)
					{
						if (prpString->AllocatedSize <= 0)
						{
							prpString->Value = (char*)malloc(llTagSize);
						}
						else
						{
							prpString->Value = (char*)realloc(prpString->Value,llTagSize);
						}
						prpString->AllocatedSize = llTagSize;
					}
					strcpy(prpString->Value ,CurDataLine->AnyList);
					prpString->UsedLen = llTagSize;
				}
				else
				{
					if (prpString->AllocatedSize > 0)
					{
						prpString->Value[0] = 0x00;
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return LineIdx;
} 

long CT_SetAnyData(char* ArrayName, long LineNo, char* pcpTag)
{
	long llRet = 0;
	SUB_ARRAY *pSub = NULL;
	char* cpItemValue;
	long llTagSize = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				llTagSize = strlen(pcpTag);
				CurDataLine->AnyData = (char*)realloc(CurDataLine->AnyData,llTagSize+1);
				strcpy(CurDataLine->AnyData,pcpTag);
			}
		}
	}
	return llRet;
}
long CT_GetAnyData(char* ArrayName, long LineNo, STR_DESC* prpString)
{
	long LineIdx = -1;
	long llTagSize = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if (pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				LineIdx = LineNo;
				CurDataLine = CurDataPool[LineIdx].DataLine;
				if (CurDataLine->AnyData != NULL)
				{
					llTagSize = strlen(CurDataLine->AnyData) + 1;
					if(prpString->AllocatedSize < llTagSize)
					{
						if (prpString->AllocatedSize <= 0)
						{
							prpString->Value = (char*)malloc(llTagSize);
						}
						else
						{
							prpString->Value = (char*)realloc(prpString->Value,llTagSize);
						}
						prpString->AllocatedSize = llTagSize;
					}
					strcpy(prpString->Value ,CurDataLine->AnyData);
					prpString->UsedLen = llTagSize;
				}
				else
				{
					if (prpString->AllocatedSize > 0)
					{
						prpString->Value[0] = 0x00;
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return LineIdx;
}


long CT_SetOldData(char* ArrayName, long LineNo, char* pcpTag)
{
	long llRet = 0;
	SUB_ARRAY *pSub = NULL;
	char* cpItemValue;
	long llTagSize = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				llTagSize = strlen(pcpTag);
				CurDataLine->OldData = (char*)realloc(CurDataLine->OldData,llTagSize+1);
				strcpy(CurDataLine->OldData,pcpTag);
			}
		}
	}
	return llRet;
}
long CT_GetOldData(char* ArrayName, long LineNo, STR_DESC* prpString)
{
	long LineIdx = -1;
	long llTagSize = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if (pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				LineIdx = LineNo;
				CurDataLine = CurDataPool[LineIdx].DataLine;
				if (CurDataLine->OldData != NULL)
				{
					llTagSize = strlen(CurDataLine->OldData) + 1;
					if(prpString->AllocatedSize < llTagSize)
					{
						if (prpString->AllocatedSize <= 0)
						{
							prpString->Value = (char*)malloc(llTagSize);
						}
						else
						{
							prpString->Value = (char*)realloc(prpString->Value,llTagSize);
						}
						prpString->AllocatedSize = llTagSize;
					}
					strcpy(prpString->Value ,CurDataLine->OldData);
					prpString->UsedLen = llTagSize;
				}
				else
				{
					if (prpString->AllocatedSize > 0)
					{
						prpString->Value[0] = 0x00;
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return LineIdx;
}



/* ============================================================= 
MWO: 12.09.2002
================================================================ */
int CT_GetItemNo(char* pcpLine, char* pcpItem, char* pcpSepa)
{
	char *ptr;
	long i=0;
	int  count = -1;
	char c;

	ptr = strstr(pcpLine, pcpItem);
	if(ptr != NULL)
	{
		c = *ptr;
		*ptr = 0x00;
		if(ptr == pcpLine)
		{
			count = 0;
		}
		while(pcpLine[i] != 0x00)
		{
			if(pcpLine[i] == pcpSepa[0])
			{
				if(count == -1) 
				{
					count=0;
				}
				count++;
			}
			i++;
		}
		*ptr = c;
	}
	return count;
}

/* ============================================================= 
BST: 11.03.2010 NEW VERSION
copies all items from item position to item position into
pcpResult
================================================================ */
int CT_GetItemsFromTo(char* pcpResult, char* pcpLine, long lpFrom, long lpTo, char* pcpSep)
{
  long llJump = 0;
  long llSepLen = 0;
  long ilStrLen = 0;
  char *pclPtr = NULL;
  char *pclFromPtr = NULL;
  char *pclToPtr = NULL;
  char *pclMaxPtr = NULL;
  llSepLen = strlen(pcpSep);
  ilStrLen = strlen(pcpLine);
  pclMaxPtr = pcpLine + ilStrLen;

  /* STEP 1: Jump to the first desired item */
  /* (Items are counted from zero) */ 
  pclPtr = pcpLine;
  llJump = lpFrom;
  
  while ((pclPtr!=NULL)&&(pclPtr<pclMaxPtr)&&(llJump>0))
  {
    pclPtr = strstr(pclPtr,pcpSep);
    if((pclPtr!=NULL)&&(pclPtr<pclMaxPtr))
    {
      pclPtr += llSepLen;
    }
    llJump--;
  }
  /* pclPtr should now be on the first data character */
  /* of the desired starting item */
  pclFromPtr = pclPtr;

  /* STEP 2: Jump to the last desired item */
  /* (Items are counted fro zero) */ 
  llJump = lpTo - lpFrom + 1;
  while ((pclPtr!=NULL)&&(pclPtr<pclMaxPtr)&&(llJump>0))
  {
    pclPtr = strstr(pclPtr,pcpSep);
    llJump--;
    if((pclPtr!=NULL)&&(pclPtr<pclMaxPtr)&&(llJump>0))
    {
      pclPtr += llSepLen;
    }
  }
  if (pclPtr == NULL)
  {
    pclPtr = pclMaxPtr;
  }
  /* pclPtr should now be on the last data character */
  /* of the desired ending item */
  pclToPtr = pclPtr;

  /* STEP 3: Copy the desired data values */
  if ((pclFromPtr != NULL)&&(pclToPtr != NULL))
  {
    ilStrLen = pclToPtr - pclFromPtr;
    if (ilStrLen < 0)
    {
      ilStrLen = 0;
    }
    if (ilStrLen > 0)
    {
      strncpy(pcpResult,pclFromPtr,ilStrLen);
    }
    pcpResult[ilStrLen] = 0x00;
  }
  else
  {
    pcpResult[0] = 0x00;
    ilStrLen = -1;
  }
  return ilStrLen;
}

/* ============================================================= 
MWO: 16.09.2002
================================================================ */
long CT_GetLineCount(char* ArrayName)
{
	SUB_ARRAY *pSub;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		return pSub->UsedLines;
	}
	return 0;
}

/* ============================================================= 
MWO: 16.09.2002
================================================================ */
long CT_GetLinesByColumnValue(char* ArrayName, char* ColName, char* SearchValue, STR_DESC* prpString, short CompareMethod)
{
	long llLineCount=0;
	long ColNo = -1;
	int ColIdx;
	long llRetStrSize = 0;
	long llReallocCount=1;
	long i;
	int ilStrLen = 0;
	char cpResult[4096];
	char pclTmp[100];

	SUB_ARRAY *pSub=NULL;

	prpString->UsedLen = 0;

	ColNo = GetColumnNameIndex(ArrayName, ColName);
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if(ColNo > -1)
			{
				if(prpString->AllocatedSize == 0)
				{
					prpString->Value = (char*)malloc((size_t)1024);
					prpString->AllocatedSize = 1024;
					prpString->UsedLen = 0;
				}
				prpString->Value[0]=0x00;
				prpString->UsedLen = 0;

				for(i = 0; i < pSub->UsedLines; i++)
				{
					CurDataLine = CurDataPool[i].DataLine;
					ColIdx = CT_GetDataItem( cpResult, CurDataLine->Values, ColNo, ',');
    					/* No Trim */
					if(ColIdx != -3)
					{
						if(CompareMethod == 0)
						{
							/* Compare equal */
							if(strcmp(cpResult, SearchValue) == 0)
							{
								sprintf(pclTmp, "%ld,", i);
								strcat(prpString->Value, pclTmp);
								prpString->UsedLen = strlen(prpString->Value);
								llLineCount++;
							}
						}
						if(CompareMethod == 1)
						{
							/* Compare Like '%XXX%' */
							if(strstr(cpResult, SearchValue) != NULL)
							{
								sprintf(pclTmp, "%ld,", i);
								strcat(prpString->Value, pclTmp);
								prpString->UsedLen = strlen(prpString->Value);
								llLineCount++;
							}
						}
						if(CompareMethod == 2)
						{
							/* Compare Like 'XXX%' */
							if(strncmp(cpResult, SearchValue, ilStrLen) == 0)
							{
								sprintf(pclTmp, "%ld,", i);
								strcat(prpString->Value, pclTmp);
								prpString->UsedLen = strlen(prpString->Value);
								llLineCount++;
							}
						}
						if(prpString->UsedLen+10 > prpString->AllocatedSize)
						{
							prpString->Value = (char*)realloc((char*)prpString->Value, prpString->AllocatedSize + 1024);
							prpString->AllocatedSize += 1024;
						}

					}
				}
			}
		}
	}
	if (prpString->UsedLen > 0)
	{
		prpString->Value[strlen(prpString->Value)-1]=0x00;
	}
	return llLineCount;
}

/* ============================================================= 
MWO: 16.09.2002
================================================================ */
void CT_SetLineStatusValue(char* ArrayName, long LineNo, long Value)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->StatusValue = Value;
			}
		}
	}
	return;
}

/* ============================================================= 
BST: 22.08.2011
================================================================ */
void CT_SetLineCheckedFlag(char* ArrayName, long LineNo, long Value)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->LineChecked = Value;
			}
		}
	}
	return;
}


/* ============================================================= 
BST: 17.03.2010
================================================================ */
void CT_SetLineDbInsFlag(char* ArrayName, long LineNo, long Value)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->DbInsFlag = Value;
			}
		}
	}
	return;
}

void CT_SetLineDbUpdFlag(char* ArrayName, long LineNo, long Value)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->DbUpdFlag = Value;
			}
		}
	}
	return;
}

void CT_SetLineDbDelFlag(char* ArrayName, long LineNo, long Value)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->DbDelFlag = Value;
			}
		}
	}
	return;
}

void CT_ResetLineDbFlags(char* ArrayName, long LineNo)
{
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataPool[LineNo].DataLine->DbInsFlag = 0;
				CurDataPool[LineNo].DataLine->DbUpdFlag = 0;
				CurDataPool[LineNo].DataLine->DbDelFlag = 0;
			}
		}
	}
	return;
}

/* ============================================================= 
BST: 16.03.2010
================================================================ */
long CT_GetLineStatusValue(char* ArrayName, long LineNo)
{
	long llRetVal = -1;
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				llRetVal = CurDataPool[LineNo].DataLine->StatusValue;
			}
		}
	}
	return llRetVal;
}

/* ============================================================= 
BST: 22.08.2011
================================================================ */
long CT_GetLineCheckedFlag(char* ArrayName, long LineNo)
{
	int ilRetVal = -1;
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				ilRetVal = CurDataPool[LineNo].DataLine->LineChecked;
			}
		}
	}
	return ilRetVal;
}

long CT_GetLineDbInsFlag(char* ArrayName, long LineNo)
{
	long llRetVal = -1;
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				llRetVal = CurDataPool[LineNo].DataLine->DbInsFlag;
			}
		}
	}
	return llRetVal;
}
long CT_GetLineDbUpdFlag(char* ArrayName, long LineNo)
{
	long llRetVal = -1;
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				llRetVal = CurDataPool[LineNo].DataLine->DbUpdFlag;
			}
		}
	}
	return llRetVal;
}
long CT_GetLineDbDelFlag(char* ArrayName, long LineNo)
{
	long llRetVal = -1;
	SUB_ARRAY *pSub=NULL;
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				llRetVal = CurDataPool[LineNo].DataLine->DbDelFlag;
			}
		}
	}
	return llRetVal;
}

/* ============================================================= 
MWO: 16.09.2002
================================================================ */
long CT_GetLinesByStatusValue(char* ArrayName, long Value, STR_DESC* prpString, short CompareMethod)
{
	long llLineCount = 0;
	SUB_ARRAY *pSub=NULL;
	long i;
	char pclTmp[100];

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if(prpString->AllocatedSize == 0)
			{
				prpString->Value = (char*)malloc((size_t)1024);
				prpString->AllocatedSize = 1024;
				prpString->UsedLen = 0;
			}
			prpString->Value[0]=0x00;
			for (i = 0; i < pSub->UsedLines; i++)
			{
				if (CompareMethod == 0)
				{
					/* Compare equal */
					if (CurDataPool[i].DataLine->StatusValue == Value)
					{
						sprintf(pclTmp, "%ld,", i);
						strcat(prpString->Value, pclTmp);
						prpString->UsedLen = strlen(prpString->Value);
						llLineCount++;
					}
				}
				if (CompareMethod == 1)
				{
					/* Compare AND */
					if ((CurDataPool[i].DataLine->StatusValue  & Value) == Value)
					{
						sprintf(pclTmp, "%ld,", i);
						strcat(prpString->Value, pclTmp);
						prpString->UsedLen = strlen(prpString->Value);
						llLineCount++;
					}
				}
				if (CompareMethod == 2)
				{
					/* Compare OR */
					if ((CurDataPool[i].DataLine->StatusValue  | Value) == Value)
					{
						sprintf(pclTmp, "%ld,", i);
						strcat(prpString->Value, pclTmp);
						prpString->UsedLen = strlen(prpString->Value);
						llLineCount++;
					}
				}
				if (CompareMethod == 3)
				{
					/* Compare XOR */
					if ((CurDataPool[i].DataLine->StatusValue  ^ Value) == Value)
					{
						sprintf(pclTmp, "%ld,", i);
						strcat(prpString->Value, pclTmp);
						prpString->UsedLen = strlen(prpString->Value);
						llLineCount++;
					}
				}
				if(prpString->UsedLen+10 > prpString->AllocatedSize)
				{
					prpString->Value = (char*)realloc((char*)prpString->Value, prpString->AllocatedSize + 1024);
					prpString->AllocatedSize += 1024;
				}
			}
		}
	}
	if (prpString->UsedLen > 0)
	{
		prpString->Value[strlen(prpString->Value)-1]=0x00;
	}
	return llLineCount;
}


/* ============================================================= 
MWO: 23.09.2002
================================================================ */
long CT_SetAlignmentList(char* ArrayName, char* pcpAlignmentList)
{
	long CurStrLen = -1;
	long llStrLen;
	int i;
	llStrLen = strlen(pcpAlignmentList);
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		for(i = 0; i < 1000; i++)
		{
			CurSubArray->AlignmentList[i]='L';

		}
		for(i = 0; i < llStrLen; i++)
		{
			CurSubArray->AlignmentList[i]= pcpAlignmentList[i];

		}
	}
	return llStrLen;
}

long AdjustStringToCT_SortLen(char* pcpInValue, char cpAlign, long lpInLen, long lpOutLen, char *pcpOutValue)
{
	int  llValueLen;
	int  llSpaceLen;
	char *pclOut = NULL;
	llValueLen = lpInLen;
	if (llValueLen > lpOutLen)
	{
		llValueLen = lpOutLen;
	}
	llSpaceLen = lpOutLen - llValueLen;
	pclOut = pcpOutValue;
	if(cpAlign == 'R')
	{
		if (llSpaceLen > 0)
		{
			memset(pclOut, ' ', llSpaceLen);
			pclOut += llSpaceLen;
		}
		strncpy(pclOut, pcpInValue, llValueLen);
		pclOut += llValueLen;
	}
	else
	{
		strncpy(pclOut, pcpInValue, llValueLen);
		pclOut += llValueLen;
		if (llSpaceLen > 0)
		{
			memset(pclOut, ' ', llSpaceLen);
			pclOut += llSpaceLen;
		}
	}
	*pclOut = 0x00;
	return lpOutLen;
}

/*--------------------------------------------------------------------------------------------
*	Without explanantion: Just a sample for usage
*	---------------------------------------------
*
*	strcpy(pclKeyWord,"{=CMD=}"); 
*	pclDataBegin = CT_GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", CT_TRUE); 
*	if (pclDataBegin != NULL) 
*	{ 
*		omCommand = CString(pclResult);
*	} 
*----------------------------------------------------------------------------------------------*/
char* CT_GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, 
				 char *pcpItemEnd, int bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == CT_TRUE) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == CT_TRUE) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end CT_GetKeyItem */ 

static void IncreaseStrgValue(char *pcpResult)
{
	char *pclPtr = NULL;
	pclPtr = pcpResult + 9;
	(*pclPtr)++;
	while (*pclPtr > '9')
	{
		*pclPtr = '0';
		pclPtr--;
		(*pclPtr)++;
	}
	return;
}



void CT_SetFieldSeparator(char* ArrayName, char NewSeparator)
{
	CurSubArray = GetSubArray(ArrayName);
	if (CurSubArray != NULL)
	{
		CurSubArray->FieldSeparator = NewSeparator;
	}
	return;
}


void CT_InitStringDescriptor(STR_DESC* prpString)
{
	prpString->Value = NULL;
	prpString->AllocatedSize = 0;
	prpString->UsedLen = 0;
	return;
}

/* ======================================================== */
/* URNO POOL ARRAY HANDLING: A Global Variable keeps it all */
/* ======================================================== */
static int InitUrnoPoolArray(void)
{
	int ilRet = -1;
	UrnoPoolArray = (URNO_POOL_ARRAY *)malloc(sizeof(URNO_POOL_ARRAY));
	if (UrnoPoolArray != NULL)
	{
		UrnoPoolArray->UsedPools = 0;
		UrnoPoolArray->UrnoPool = NULL;
		ilRet = 0;
	}
	return ilRet;
}

/* ================================================ */
/* Only called locally */
/* Gives the Pool Index back. */
/* A not yet existing Urno Pool will be created.    */
/* ================================================ */
static int AttachUrnoPool(char *AreaName, char *PoolName)
{
	int  PoolIdx = -1;
	int  PoolCount = 0;
	URNO_POOL *UrnoPool;
	PoolIdx = GetUrnoPoolIndex(AreaName, PoolName);
	if (PoolIdx < 0)
	{
		UrnoPool = UrnoPoolArray->UrnoPool;
		if (UrnoPool == NULL)			
		{
			UrnoPool = (URNO_POOL *)malloc(sizeof(URNO_POOL));
			if (UrnoPool != NULL)	
			{
				UrnoPool->UsedLines = 0;
				UrnoPool->UrnoRange = NULL;
                UrnoPool->UrnoCount = 0;
				strcpy(UrnoPool->AreaName, AreaName);
				strcpy(UrnoPool->PoolName, PoolName);
				UrnoPoolArray->UrnoPool = UrnoPool;
				UrnoPoolArray->UsedPools = 1;
				PoolIdx = 0;
			}
		}
		if ((PoolIdx < 0) && (UrnoPool != NULL))
		{
			PoolCount = UrnoPoolArray->UsedPools + 1;
			UrnoPool = (URNO_POOL *)realloc(UrnoPool,sizeof(URNO_POOL)*(PoolCount));
			PoolIdx = UrnoPoolArray->UsedPools;
			if (UrnoPool != NULL)
			{
				strcpy(UrnoPool[PoolIdx].AreaName, AreaName);
				strcpy(UrnoPool[PoolIdx].PoolName, PoolName);
				UrnoPool[PoolIdx].UrnoRange = NULL;
                UrnoPool[PoolIdx].UrnoCount = 0;
				UrnoPool[PoolIdx].UsedLines = 0;
			}
			UrnoPoolArray->UrnoPool = UrnoPool;
			if (UrnoPool != NULL)
			{
				UrnoPoolArray->UsedPools++;
			}
		}
	}
	return PoolIdx;
} /* AttachUrnoPool */

/* ================================================ */
/* Only called locally */
/* ================================================ */
static int GetUrnoPoolIndex(char *AreaName, char *PoolName)
{
	int PoolIdx = -1;
	int CurIdx = 0;
	CurIdx = 0;
	while ((CurIdx < UrnoPoolArray->UsedPools) && (PoolIdx < 0))
	{
        if ((strcmp(UrnoPoolArray->UrnoPool[CurIdx].AreaName,AreaName) == 0) && 
            (strcmp(UrnoPoolArray->UrnoPool[CurIdx].PoolName,PoolName) == 0))
		{
			PoolIdx = CurIdx;
		}
		CurIdx++;
	}
	return PoolIdx;
} /* GetUrnoPoolIndex */

/* ================================================ */
/* Main Function to Create and Maintain URNO Pools  */
/* ================================================ */
int CT_SetUrnoPool(char *AreaName, char *PoolName, char *FirstValue, int UrnoCount)
{
    int PoolIdx = -1;
    int LineIdx = -1;
    int UrnoStrLen = 0;
    int UrnoStrOff = 0;
    URNO_POOL *UrnoPool;
    if (UrnoPoolArray == NULL)
    {
        InitUrnoPoolArray();
    }
    PoolIdx = AttachUrnoPool(AreaName,PoolName);
    if (PoolIdx >= 0)
    {
        UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
        LineIdx = GetFreeUrnoRange(PoolIdx);
        if (LineIdx >= 0)
        {
          UrnoPool->UrnoRange[LineIdx].UrnoCount = UrnoCount;
          strncpy(UrnoPool->UrnoRange[LineIdx].UrnoValue, "00000000000000000000", URNO_SIZE);
          UrnoStrLen = strlen(FirstValue);
          UrnoStrOff = URNO_SIZE - UrnoStrLen;
          if (UrnoStrOff < 0)
          {
            UrnoStrOff = 0;
          }
          strcpy(&(UrnoPool->UrnoRange[LineIdx].UrnoValue[UrnoStrOff]),FirstValue);
          UrnoPool->UrnoCount += UrnoCount;
        }
	}
	else
	{
      /* something went totally wrong */
      /* So crash please :-) */
      dbg(TRACE,"OOOPS WE SHOULD NEVER COME HERE!");
	}
	return PoolIdx;
} /* End CT_SetUrnoPool */

/* ================================================ */
/* Only Called Internally */
/* Main Function to Create and Maintain URNO Lines  */
/* ================================================ */
static int GetFreeUrnoRange(int PoolIdx)
{
  int LineIdx = -1;
  URNO_POOL *UrnoPool;
  URNO_RANGE *UrnoRange;
  int CurIdx = 0;
  int LineCount = 0;
  CurIdx = 0;
  UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
  while ((CurIdx < UrnoPool->UsedLines) && (LineIdx < 0))
  {
    if (UrnoPool->UrnoRange[CurIdx].UrnoCount <= 0)
    {
      LineIdx = CurIdx;
    }
    CurIdx++;
  }
  if (LineIdx < 0)
  {
    UrnoRange = UrnoPool->UrnoRange;
    if (UrnoRange == NULL)
    {
      UrnoRange = (URNO_RANGE *)malloc(sizeof(URNO_RANGE));
      if (UrnoRange != NULL)	
      {
        UrnoPool->UrnoRange = UrnoRange;
        UrnoPool->UsedLines = 1;
        LineIdx = 0;
      }
    }
    if ((LineIdx < 0) && (UrnoRange != NULL))
    {
      LineCount = UrnoPool->UsedLines + 1;
      UrnoRange = (URNO_RANGE *)realloc(UrnoRange,sizeof(URNO_RANGE)*(LineCount));
      LineIdx = UrnoPool->UsedLines;
      if (UrnoRange != NULL)
      {
        UrnoRange[LineIdx].UrnoValue[0] = 0x00;
        UrnoRange[LineIdx].UrnoCount = 0;
      }
      UrnoPool->UrnoRange = UrnoRange;
      UrnoPool->UsedLines++;
    }
  }
  return LineIdx;
} /* end GetFreeUrnoRange */



/* ================================================ */
/* Function to get the next URNO sequential number (increasing) */
/* ================================================ */
int CT_GetNextUrno(char *AreaName, char *PoolName, char *UrnoValue, int ipTrimLeft)
{
    int PoolIdx = -1;
    int LineIdx = -1;
    int  UrnoCount = -1;
    URNO_POOL *UrnoPool;
    char *pclPtr = NULL;
    if (UrnoPoolArray == NULL)
    {
        InitUrnoPoolArray();
    }
    PoolIdx = AttachUrnoPool(AreaName,PoolName);
    if (PoolIdx >= 0)
    {
        UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
        LineIdx = GetValidUrnoRange(PoolIdx);
        if (LineIdx >= 0)
        {
          pclPtr = UrnoPool->UrnoRange[LineIdx].UrnoValue;
          if (ipTrimLeft == TRUE)
          {
            while (*pclPtr == '0')
            {
              pclPtr++;
            }
          }
          strcpy(UrnoValue, pclPtr);
          IncreaseUrnoValue(UrnoPool->UrnoRange[LineIdx].UrnoValue);
          UrnoPool->UrnoRange[LineIdx].UrnoCount--;;
          UrnoPool->UrnoCount--;
          UrnoCount = UrnoPool->UrnoRange[LineIdx].UrnoCount;
        }
    }
    if (UrnoCount < 0)
    {
        strcpy(UrnoValue, "");
    }
    return UrnoCount;
} /* end CT_GetNextUrno */

/* ===================================================== */
/* Function to get the actual URNO number (no increase)  */
/* ===================================================== */
int CT_GetUrnoStrg(char *AreaName, char *PoolName, char *UrnoValue, int ipTrimLeft)
{
    int PoolIdx = -1;
    int LineIdx = -1;
    int  UrnoCount = -1;
    URNO_POOL *UrnoPool;
    char *pclPtr = NULL;
    if (UrnoPoolArray == NULL)
    {
        InitUrnoPoolArray();
    }
    PoolIdx = AttachUrnoPool(AreaName,PoolName);
    if (PoolIdx >= 0)
    {
        UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
        LineIdx = GetValidUrnoRange(PoolIdx);
        if (LineIdx >= 0)
        {
          pclPtr = UrnoPool->UrnoRange[LineIdx].UrnoValue;
          if (ipTrimLeft == TRUE)
          {
            while (*pclPtr == '0')
            {
              pclPtr++;
            }
          }
          strcpy(UrnoValue, pclPtr);
          UrnoCount = UrnoPool->UrnoRange[LineIdx].UrnoCount;
        }
    }
    if (UrnoCount < 0)
    {
        strcpy(UrnoValue, "");
    }
    return UrnoCount;
} /* end CT_GetUrnoStrg */

/* ================================================ */
/* Only Called Internally */
/* Main Function to Get URNO and Maintain URNO Lines  */
/* ================================================ */
static int GetValidUrnoRange(int PoolIdx)
{
  int LineIdx = -1;
  URNO_POOL *UrnoPool;
  int CurIdx = 0;
  CurIdx = 0;
  UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
  while ((CurIdx < UrnoPool->UsedLines) && (LineIdx < 0))
  {
    if (UrnoPool->UrnoRange[CurIdx].UrnoCount > 0)
    {
      LineIdx = CurIdx;
    }
    CurIdx++;
  }
  return LineIdx;
} /* end GetValidUrnoRange */

/* ================================================ */
/* ================================================ */
static void IncreaseUrnoValue(char *pcpUrnoValue)
{
	char *pclPtr = NULL;
	pclPtr = pcpUrnoValue + URNO_SIZE - 1;
	(*pclPtr)++;
	while (*pclPtr > '9')
	{
		*pclPtr = '0';
		pclPtr--;
		(*pclPtr)++;
	}
	return;
}

/* ================================================ */
/* ================================================ */
void CT_ResetUrnoPool(char *AreaName, char *PoolName)
{
  int PoolIdx = -1;
  int LineIdx = -1;
  URNO_POOL *UrnoPool;

  if (UrnoPoolArray == NULL)
  {
    InitUrnoPoolArray();
  }
  PoolIdx = AttachUrnoPool(AreaName,PoolName);
  if (PoolIdx >= 0)
  {
    LineIdx = 0;
    UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
    while (LineIdx < UrnoPool->UsedLines)
    {
      UrnoPool->UrnoRange[LineIdx].UrnoCount = 0;
      strcpy(UrnoPool->UrnoRange[LineIdx].UrnoValue,"IS_RESET");
      LineIdx++;
    }
    UrnoPool->UrnoCount = 0;
  }
  return;
} /* end CT_ResetUrnoPool */


/* ================================================ */
/* ================================================ */
int CT_GetUrnoCount(char *AreaName, char *PoolName)
{
  int  PoolTotal = -1;
  int PoolIdx = -1;
  URNO_POOL *UrnoPool;

  if (UrnoPoolArray == NULL)
  {
    InitUrnoPoolArray();
  }
  PoolIdx = AttachUrnoPool(AreaName,PoolName);
  if (PoolIdx >= 0)
  {
    UrnoPool = &UrnoPoolArray->UrnoPool[PoolIdx];
    PoolTotal = UrnoPool->UrnoCount;
  }
  return PoolTotal;
} /* end CT_GetUrnoCount */


/* ================================================ */
/* ================================================ */
int CT_SetArrayDataTracking(char *ArrayName, char *TableName, int TrackChanges, int DataCheckMethod)
{
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
        strcpy(pSub->TableName,TableName);
		pSub->TrackChanges = TrackChanges;
		pSub->DataNotNull = DataCheckMethod;
	}
	return 0L;
}

/* ================================================ */
/* ================================================ */
int CT_SetArrayNullValueCheck(char *ArrayName, int DataCheckMethod)
{
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		pSub->DataNotNull = DataCheckMethod;
	}
	return 0L;
}

/* ================================================ */
/* ================================================ */
int CT_GetArrayTableName(char *ArrayName, char *TableName)
{
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
        strcpy(TableName, pSub->TableName);
	}
	return 0L;
}


/* ================================================ */
/* ================================================ */
int CT_SetTrimMethod(char *ArrayName, char *TrimMethod)
{
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		strcpy(pSub->TrimMethod,TrimMethod);
	}
	return 0L;
}

int CT_SetUrnoPoolMethod(char *ArrayName, char *TableName, int UseRanges, char *NumTabKey)
{
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
        strcpy(pSub->TableName,TableName);
        strcpy(pSub->NumTabKey,NumTabKey);
        pSub->UrnoRanges = UseRanges;
	}
	return 0L;
}

int CT_GetUrnoPoolMethod(char *ArrayName, char *TableName, char *NumTabKey)
{
  int ilRc = FALSE;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
        strcpy(TableName, pSub->TableName);
        strcpy(NumTabKey, pSub->NumTabKey);
        ilRc = pSub->UrnoRanges;
	}
  return ilRc;
}


/* ================================================ */
/* Function to get the next internal URNO (increasing) */
/* ================================================ */
int CT_GetNextGridUrno(char *ArrayName, char *UrnoValue, int ipTrimLeft)
{
	int ilRc = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
	    ilRc = CT_GetNextUrno(pSub->TableName, pSub->NumTabKey, UrnoValue, ipTrimLeft);
	}
	return ilRc;
}

/* ================================================ */
/* ================================================ */
int CT_GetGridUrnoCount(char *ArrayName)
{
	int ilRc = 0;
	SUB_ARRAY *pSub = NULL;
	pSub = GetSubArray(ArrayName);
	if(pSub != NULL)
	{
		ilRc = CT_GetUrnoCount(pSub->TableName, pSub->NumTabKey);
	}
	return ilRc;
}

/* ================================================ */
/* ================================================ */
long CT_GetChangedValues(char *ArrayName, char *GetFldLst, long LineNo, char *OutFldLst, char *NewDatLst, char *OldDatLst)
{
	long FldCount = 0;
	long ColNo;
	long FldNo;
	long MaxNo;
	long llLen;
	char ColName[16];
	SUB_ARRAY *pSub = NULL;
	llLen = 0;
	OutFldLst[llLen] = 0x00;	
	NewDatLst[llLen] = 0x00;	
	OldDatLst[llLen] = 0x00;	
	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			if ((LineNo >= 0) && (LineNo < pSub->UsedLines))
			{
				CurDataLine = CurDataPool[LineNo].DataLine;
				OrgDataLine = CurDataPool[LineNo].OrigLine;
				if ((CurDataLine != NULL)&&(OrgDataLine != NULL))
				{
					if (strlen(GetFldLst) > 0)
					{
						strcpy(pcgFldList,GetFldLst);
					}
					else
					{
						strcpy(pcgFldList,pSub->FieldList);
					}
					MaxNo = CT_CountPattern(pcgFldList, ",");
					for (FldNo=0;FldNo<=MaxNo;FldNo++)
					{
						CT_GetDataItem(ColName, pcgFldList, FldNo, ',');
						ColNo = GetColumnNameIndex(ArrayName, ColName);
						if (ColNo >= 0)
						{
							CT_GetDataItem(pcgCurrData, CurDataLine->Values, ColNo, ',');
							CT_TrimRightDef(pcgCurrData, ' ', -1, " ");
							CT_GetDataItem(pcgOrigData, OrgDataLine->Values, ColNo, ',');
							CT_TrimRightDef(pcgOrigData, ' ', -1, " ");
							if (strcmp(pcgCurrData,pcgOrigData) != 0)
							{
								FldCount++;
								strcat(OutFldLst, ColName);
								strcat(OutFldLst, ",");
								strcat(NewDatLst, pcgCurrData);
								strcat(NewDatLst, ",");
								strcat(OldDatLst, pcgOrigData);
								strcat(OldDatLst, ",");
							}
						}
					}
					llLen = strlen(OutFldLst);
					if (llLen > 0)
					{
						llLen--;
						OutFldLst[llLen] = 0x00;	
					}
					llLen = strlen(NewDatLst);
					if (llLen > 0)
					{
						llLen--;
						NewDatLst[llLen] = 0x00;	
					}
					llLen = strlen(OldDatLst);
					if (llLen > 0)
					{
						llLen--;
						OldDatLst[llLen] = 0x00;	
					}
				}
			} /* end if */
		} /* end if */
	} /* end if */
	return FldCount;
}


/* ================================================ */
/* ================================================ */
long CT_GetDistinctList(char *ArrayName, char *ColName, char *StrPrefix, char *StrSuffix, char *StrDelimiter, STR_DESC *prpString)
{
	long llCount = 0;
	long llLineCount=0;
	long ColNo = -1;
	int ColIdx;
	long i;
	int ilStrLen = 0;
	char cpItem[4096];
	char pclTmp[100];

	SUB_ARRAY *pSub=NULL;

	prpString->UsedLen = 0;

	pSub = GetSubArray(ArrayName);
	if( pSub != NULL)
	{
		CurDataPool = pSub->DataPool;
		if (CurDataPool != NULL)
		{
			ColNo = GetColumnNameIndex(ArrayName, ColName);
			if(ColNo > -1)
			{
				if(prpString->AllocatedSize == 0)
				{
					prpString->Value = (char*)malloc((size_t)1024);
					prpString->AllocatedSize = 1024;
					prpString->UsedLen = 0;
				}
				prpString->Value[0]=0x00;
				prpString->UsedLen = 0;

				for(i = 0; i < pSub->UsedLines; i++)
				{
					CurDataLine = CurDataPool[i].DataLine;
					ColIdx = CT_GetDataItem(cpItem, CurDataLine->Values, ColNo, ',');
					ilStrLen = CT_TrimRight(cpItem, ' ', -1);
					if (ilStrLen > 0)
					{
						if (strstr(prpString->Value,cpItem) == NULL)
						{
							llCount++;
							if (llCount > 1)
							{
								strcat(prpString->Value,StrDelimiter);
							}
							strcat(prpString->Value,StrPrefix);
							strcat(prpString->Value,cpItem);
							strcat(prpString->Value,StrSuffix);
						}
					}
				}
			}
		}

	}
	return llCount;
}

/******************************************************************************/
/******************************************************************************/
void CT_CheckEmptyValue(char *pcpData, char *SetValue)
{
  if (pcpData[0] == '\0')
  {
    strcpy(pcpData,SetValue);
  } /* end if */ 
  return;
} /* end CheckEmptyValue */

/******************************************************************************/
/******************************************************************************/
void CT_CheckNullValues(char *pcpDataIn,char *pcpDataOut)
{
  char *c = NULL;
  char *s = NULL;
  char *t = NULL;
  s = pcpDataIn;
  t = pcpDataOut;
  while (*s != '\0')
  {
    if (*s == ',')
    {
      c = s - 1;
      if (c < pcpDataIn)
      {
        c = pcpDataIn;
      }
      if (*c == ',')
      {
        *t = ' ';
        t++;
      }
    }
    *t = *s;
    s++;
    t++;
  }
  c = s - 1;
  if (c < pcpDataIn)
  {
    c = pcpDataIn;
  }
  if ((*c == ',') || (*c == '\0'))
  {
    *t = ' ';
    t++;
  }
  *t = '\0';
  return;
} /* end CT_CheckNullValues */
