<?php 
/*
V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.
  Released under Lesser GPL library license. See License.txt.
*/ 
  
// specific code for tohtml
GLOBAL $gSQLMaxRows,$gSQLBlockRows;
	 
$gSQLMaxRows = 1000; // max no of rows to download
$gSQLBlockRows=28; // max no of rows per table block


 
 
 /* 
 Break the results into pages , we use the $gSQLBlockRows
 as the maximum records per page we also use the $RecordCount var 
 in order to known what is the number of records tha the select statement returns
 */
 
function listpages(&$rs,$RecordCount,$ztabhtml="")
{
$s =''; $flds; $ncols; $i;$rows=0;$hdr;$docnt = false;
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$DatabaseType,$listElement,$movenext,$db,$list_PagingMove,$lastpage,$prevrows;
/* var for the search form */
GLOBAL $allcategory,$fromdateMonth,$fromdateYear,$fromdateDay,$fromdateHour,$fromdateMin,$todateplus,$airline,$flno,$city,$PHP_SELF;
GLOBAL $ol,$masktitle,$type,$debug,$tdiminus,$HeadingTitle;
GLOBAL $TDI1,$TDI2,$TICH,$fromdate;	
	if (! $ztabhtml) $ztabhtml = " BORDER=3 width=50%  bgcolor=\"#77B0E4\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"' ";

//        $hdr = "<TABLE  $ztabhtml>";

//		echo $hdr;
		$cr=1;

		$pagecount=round($RecordCount/$gSQLBlockRows);

		if ($pagecount < $RecordCount/$gSQLBlockRows) { 
		 $pagecount = $pagecount +1;
		}

	switch ($list_PagingMove) {		
        case "Requery":
//            list.Requery
			 break;
        case "   <<   ":
			$rs->movefirst();			
			$cr=1;
			 break;
        case "   <    ":
			if ($prevrows<20) {
				$result=$movenext-$gSQLBlockRows-$prevrows;
			} else {
				$result=$movenext-($gSQLBlockRows*2);
			}
			
			$rs->move($result);
			$cr=($result)+$gSQLBlockRows;
			 break;
        case "    >   ":
			if ($movenext>=$RecordCount) { $movenext=0;}
			$rs->move($movenext);
			$cr=($movenext-1)+$gSQLBlockRows;
			 break;
        case "   >>   ":
				$result=$pagecount*$gSQLBlockRows;
				if ($result > $RecordCount) {
					$result=$result- $RecordCount;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				} else {
					$result= $RecordCount - $result;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				
				}
				$rs->move($result);
				$cr=($result)+$gSQLBlockRows;
				

	 		 break;
    }


		if  ($rs->EOF && $RecordCount >0)  {
			$rs->movefirst();
			$cr=1;
		}

		$currentpage=round( $cr/ $gSQLBlockRows );
		if ($currentpage<=0) {$currentpage=1;}
		if ($RecordCount>$gSQLBlockRows) { ?>
      <FORM ACTION="<?php echo $PHP_SELF ?>" METHOD="POST">
		<TABLE BORDER=0 CELLSPACING=0>
        <TR>
            <TH ALIGN=CENTER class="RowsFound">
                <NOBR><% echo $RecordCount %> <%echo $HeadingTitle%> &nbsp;&nbsp;&nbsp;&nbsp; Page: <% echo $currentpage%> from <%echo  $pagecount %></NOBR>
            </TH>
        </TR>
        <TR>
            <TD align="center">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &lt;&lt;   ">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &lt;    ">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="    &gt;   ">
	                 <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &gt;&gt;   ">
                    <!--INPUT TYPE="Submit" NAME="list_PagingMove" VALUE=" �ߍ������� "-->
            </TD>
        </TR>
        </TABLE>
<?php } ?>
<?php 		// We include the columns ?>
	<TABLE <%echo $ztabhtml%>>
	<TR><?php 	include("include/list_column.php");?></TR>
<?php	if ($rs->EOF) {
		 		// We include the Rows ?>
	<TR><?php include("include/list_no_rows.php");?></TR>
<?php	 }	?>
<?php	   	while (!$rs->EOF) {
 		// We include the Rows ?>
		<TR><?php include("include/list_row.php");?></TR>
<?php		$rows += 1;
		if ($rows >= $gSQLMaxRows) {
			$rows = "<p>Truncated at $gSQLMaxRows</p>";
			break;
		}
	$rs->MoveNext();
		// additional EOF check to prevent a widow header
	        if (( !$rs->EOF && $rows == $gSQLBlockRows)|| ($rs->EOF &&$pagecount>1) ){// && $rs->CurrentRow()<$RecordCount) {
			//if (connection_aborted()) break;// not needed as PHP aborts script, unlike ASP
//					   echo "<tr><td><a href=\"$PHP_SELF?movenext=".($rs->CurrentRow())."\">Move next</a></td></tr>";						
			           echo "</TABLE>\n\n";?>
		<TABLE BORDER=0 CELLSPACING=0>
        <TR>
            <TD align="center">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &lt;&lt;   ">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &lt;    ">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="    &gt;   ">
                    <INPUT TYPE="Submit" NAME="list_PagingMove" VALUE="   &gt;&gt;   ">
                    <!--INPUT TYPE="Submit" NAME="list_PagingMove" VALUE=" �ߍ������� "-->
                    <INPUT TYPE="hidden" NAME="movenext" VALUE="<?php echo $rs->CurrentRow() ?>">
                    <INPUT TYPE="hidden" NAME="prevrows" VALUE="<?php echo $rows ?>">
					<%if ($allcategory) {%>
					<input type="hidden" name="allcategory" value="<%echo $allcategory%>">
					<%}%>
					<%if ($flno) {%>
					<input type="hidden" name="flno" value="<%echo $flno%>">
					<%}%>
					<%if ($airline) {%>
					<input type="hidden" name="airline" value="<%echo $airline%>">
					<%}%>

					<%if ($city) {%>
					<input type="hidden" name="city" value="<%echo $city%>">
					<%}%>
					<%if ($fromdateMonth) {%>
					<input type="hidden" name="fromdateMonth" value="<%echo $fromdateMonth%>">
					<%}%>
					<%if ($fromdateYear) {%>
					<input type="hidden" name="fromdateYear" value="<%echo $fromdateYear%>">
					<%}%>
					<%if ($fromdateDay) {%>
					<input type="hidden" name="fromdateDay" value="<%echo $fromdateDay%>">
					<%}%>
					<%if ($fromdateHour) {%>
					<input type="hidden" name="fromdateHour" value="<%echo $fromdateHour%>">
					<%}%>
					<%if ($fromdateMin) {%>
					<input type="hidden" name="fromdateMin" value="<%echo $fromdateMin%>">
					<%}%>
					<%if ($fromdateMonth) {%>
					<input type="hidden" name="todateMonth" value="<%echo $todateMonth%>">
					<%}%>
					<%if ($todateplus) {%>
					<input type="hidden" name="todateplus" value="<%echo $todateplus%>">
					<%}%>
					<%if ($debug) {%>
					<input type="hidden" name="debug"value="<%echo $debug%>"> 					
					<%}%>

					<input type="hidden" name="type" value="<%echo $type%>">
            </TD>
        </TR>
        </TABLE>
	</FORM>
<?php						return $rows;
//                        echo  $hdr;
		} //else{
		
		
        } // while
		$fdr= "</TABLE>\n\n";
        echo $fdr;
	return $rows;
 }
?>
