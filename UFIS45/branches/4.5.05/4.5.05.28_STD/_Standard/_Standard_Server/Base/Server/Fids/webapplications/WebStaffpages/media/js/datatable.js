var flightData;

function InitOverviewDataTable(initType,cookieSuffix,columnNames)
{
    var domString="";
    //Full
    if (initType==0)  {
        domString='RC<"clear">t<"H"lTfr>t<"F"ip>';
    //NoHide
    } else if  (initType==1) {
        domString='<"H"lTfr>t<"F"ip>';
    } else {
        domString='<"H"lr>t';

    }

    var colDef = new Array();

    if (columnNames.length > 0 ) {
        var splitResult = columnNames.split(",");
        for(i = 0; i < splitResult.length-1; i++){
            tmp_arr = new Array();
            tmp_arr['mDataProp']=splitResult[i];
            // alert(splitResult[i]);
            // colDef.push(tmp_arr);
            colDef[i]=tmp_arr;
        }
    }

    flightData = $('#flightData').dataTable({
        "bJQueryUI": true,
        "bStateSave": true,
        "iDisplayLength": 10,
        "sPaginationType": "full_numbers",
        "sDom": domString,
        "sCookiePrefix": cookieSuffix+"_",
        "aLengthMenu": [[10,20,30,40,50, -1], [10,20,30,40,50, "All"]],
        "oTableTools": {
            "aButtons": [
            "copy",
            {
                "sExtends":    "collection",
                "sButtonText": "Save",
                "aButtons":    [ "csv", "xls", "pdf" ]
            }
            ]
        },
        "aoColumns": colDef
    } );
}
//"bLengthChange": true,
//"bProcessing": true,
//"bInfo": true,

//"bDeferRender": true,
//"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
//                    for(var i=0; i<aData.length; i++) {
//
//                        var value = aData[i];
//
//                        /* numbers less than or equal to 0 should be in red text */
//                        if ( value.indexOf('FF-') != -1 ) {
//                            $('td:eq('+i+')', nRow).addClass('redText');
//                        }
//                    }
//                    return nRow;
//                    },

function RefreshTable(tableId, urlData)
{
    $.getJSON(urlData, null, function( json )
    {
        table = $(tableId).dataTable();
        oSettings = table.fnSettings();

        if (json.aaData.length > 0 ) {


            table.fnClearTable(this);

            for (var i=0; i<json.aaData.length; i++)
            {
                table.oApi._fnAddData(oSettings, json.aaData[i]);
            }

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw(false);
        } else {
            $('#noRows').text(" No Rows found");
            $('#noRows').fadeOut(10000, function() {$('#noRows').text("")});
        }

    });
}


function AutoReload()
{
    var search_form = $('#search_form');
    var RefreshRate =  $("#RefreshRate").val();
    var addQuery = "?TMPL_MENUID=";

    TMPL_MENUID = $("#TMPL_MENUID").val();
    addQuery = addQuery +   TMPL_MENUID

    debug = $("#debug").val();
    addQuery = addQuery +   "&debug="+debug;

    $.each($("input, select, textarea",search_form), function(i,v) {
        var name = $(this).attr('name');
        var val = $(this).val();
        if ( (name.indexOf('Dynamic') != -1 || name.indexOf('TfVal') != -1 || name.indexOf('todateplus') != -1 ) && val.length > 0 )    {
            addQuery = addQuery + "&"+ name +"="+ val;
        }
    });

    //alert(addQuery)
    RefreshTable('#flightData', 'dbinfoAjax.php'+addQuery );
    setTimeout(function(){
        AutoReload();
    }, RefreshRate);
    $('#noRows').text("")
}


$(document).ready(function() {
    $('#openSearch').click(function() {
        $('#dialog-modal').dialog('open');
    });
    $('#submitSearch').click(function() {
        //$("#search_form").submit();
        AutoReload();
        $('#dialog-modal').dialog('close');
    });
    $('#resetSearch').click(function() {
        resetForm('search_form');
    });

    function resetForm(id) {
        $(':input','#'+id)
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
        return false;
    }


    $("#dialog-modal").dialog({ // jQuery UI Dialog
        width: 460,
        autoOpen: false,
        modal: true,
        resizable: true
    });
    $("#DateTime").clock({
        "format":"24",
        "calendar":"false"
    });



});
