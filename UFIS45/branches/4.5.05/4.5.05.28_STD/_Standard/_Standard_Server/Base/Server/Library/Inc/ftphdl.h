#ifndef _DEF_mks_version_ftphdl_h
  #define _DEF_mks_version_ftphdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ftphdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ftphdl.h 1.3 2004/07/27 16:47:46SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :   ??.??.???? RBI                                      */
/*  History       :   05.04.00 RKL  Insert pcOptParaSend                  */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef __FTPHDL_H
#define __FTPHDL_H



#include	<ftptools.h>

#define	cCOMMA ','

#define	iSEND		1
#define	iRECEIVE	2

#define	FTP_SUCCESS	1
#define	FTP_FAIL	2
#define	FTP_RETRY	3
#define	FTP_CMD_UNKNOWN	4
#define	FTP_CMD_IGNORED	5
#define	FTP_INIT_ERROR	6

#define	sTYPE(a) ((a) == iSEND ? "SEND" : ((a) == iRECEIVE ? "RECEIVE" : "UNKNOWN"))
#define sGET_STATE(a) ((a) == iFTP_PART_TIME_INVALID ? "PART TIME INVALID" : ((a) == iFTP_TRY_AGAIN ? "TRY AGAIN" : "UNKNOWN"))
#define sDBGLEVEL(a) ((a) == DEBUG ? "DEBUG" : ((a) == TRACE ? "TRACE" : "OFF"))
#define sOS(a) ((a) == OS_UNIX ? "UNIX" : ((a) == OS_WIN ? "WINDOWS" : "UNKNOWN"))


/* #define FTP_NAME_BUF 64   / * for single names */
/* #define FTP_PATH_BUF 512  / * for pathes */
/* #define FTP_CMD_BUF  1024 / * for command lines */
/* #define FTP_BIG_BUF  2096 / * for long strings */

/* dynamic configure structure... */
typedef struct _FTPConfig
{
	int	iModID;	/* VBL needs it */
	int	iFtpRC;
	char	pcCmd[8];
	char	pcHostName[64];
	char	pcUser[64];
	char	pcPasswd[64];
	char	cTransferType;
	int	iTimeout;
	long	lGetFileTimer;
	long	lReceiveTimer;
	int	iDebugLevel;
	int	iClientOS;
	int	iServerOS;
	int	iRetryCounter;
	int	iDeleteRemoteSourceFile;
	int	iDeleteLocalSourceFile;
	int	iSectionType;	/* send or receive */
	int	iInvalidOffset;
	int	iTryAgainOffset;
	char	pcHomeAirport[8]; /* the HomeAirport */
	char	pcTableExtension[8]; /* the table externsion */
	char	pcLocalFilePath[2*iMIN_BUF_SIZE];
	char	pcLocalFileName[iMIN_BUF_SIZE];
	char	pcRemoteFileName[iMIN_BUF_SIZE];
	char	pcRemoteFilePath[2*iMIN_BUF_SIZE];
	char	pcRenameLocalFile[iMIN_BUF_SIZE];
	char	pcRenameRemoteFile[iMIN_BUF_SIZE];
	int	iSendAnswer;
	char	cStructureCode;
	char	cTransferMode;
	int	iStoreType;
	char	data[1]; /* pointer to next structure... */
} FTPConfig;

/* structs/unions... */
typedef struct _FTPCmd
{
  int iIsDynamic;  /* a flag which shows a dynamic part... */

  char	pcHomeAirport[iMIN]; /* the HomeAirport */
  char	pcTableExtension[iMIN]; /* the table externsion */

  /* invalid timeframe */
  int    iSendAnswer;
  int    iValid;
  int    iTimschNo;
  time_t	tFirstEvent;
  time_t	tEventPeriod;
  int    iErrorCnt;
  int    iTimeInvalidOffset;
  int    iTimeTryAgainOffset;
  char  pcCmd[iMIN];
  int    iSectionType;	/* send or receive */
  char   cStructureCode;
  char   cTransferMode;
  int    iStoreType;
  int    iRetryCounter; 
  int    iDeleteRemoteSourceFile;
  int    iDeleteLocalSourceFile;

  char	pcLocalFilePath[iMAX_BUF_SIZE];
  char	pcLocalFileName[2*iMIN_BUF_SIZE];
  char	pcRemoteFileName[2*iMIN_BUF_SIZE];
  char	pcRemoteFilePath[iMAX_BUF_SIZE];
  char	pcRenameLocalFile[iMIN_BUF_SIZE];
  char	pcRenameRemoteFile[iMIN_BUF_SIZE];
  char  pcRemoteFileAppendExtension[iMIN_BUF_SIZE];
  int    iRemoteFileAppendExtension;

  char  pcSendCommand[iMIN_BUF_SIZE]; /* send command to process */
  char	pcOptSiteCmd[iMAX_BUF_SIZE];
  int    iModID; /* MOD_ID of Process for SendCommand*/

  /* everything received by queue */
  char	pcQUELocalFilePath[iMAX_BUF_SIZE];
  char	pcQUELocalFileName[2*iMIN_BUF_SIZE];
  char	pcQUERemoteFileName[2*iMIN_BUF_SIZE];
  char	pcQUERemoteFilePath[iMAX_BUF_SIZE];

  char	pcAlertName[iMAX_BUF_SIZE];
  CEDA_FTPInfo	rInfo;
} FTPCmd;

typedef struct _FTPMain
{
	int	iAnswerInvalidCmd;
	int	iNoOfCmds;
	FTPCmd *prCmds;
} FTPMain;

#endif  /* __FTPHDL_H */



