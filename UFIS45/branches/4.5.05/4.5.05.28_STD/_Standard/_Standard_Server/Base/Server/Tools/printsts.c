#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/printsts.c 1.1 2003/03/19 17:03:42SGT fei Exp  $";
#endif /* _DEF_mks_version */
static char sccs_printsts[]="@(#) UFIS 4.4 (c) ABB AAT/I printsts.c 44.1 / 00/01/07 09:55:57 / VBL"; 

#include <stdio.h>
#include "glbdef.h"
#include "uevent.h"
#include "quedef.h"
#include "tools.h"
#define U_MAIN
#include "ugccsma.h"
#include "hsbsub.h"

extern int	debug_level=DEBUG;
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*extern FILE *outp;*/

MAIN
{
	int	rc;
	char status[64];

	/* STH_INIT_OLD(); */

	rc = init();
	if(rc != RC_SUCCESS)
	{
		printf("DOWN\n");
		exit(20);
	}/* end of if */

	memset(status,0x00,64);

	rc = get_system_state();

	sts2status(&rc,status);

	printf("%s\n",status);

	exit(rc);
}
