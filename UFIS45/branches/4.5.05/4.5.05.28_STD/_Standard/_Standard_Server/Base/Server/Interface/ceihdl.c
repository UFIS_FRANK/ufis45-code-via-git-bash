#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/ceihdl.c 1.12 2010/10/18 17:33:31SGT heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* Ceda event interface                                                       */
/*                                                                            */
/* Author         : USC                                                       */
/* Date           : 13.04.2004                                                */
/* Description    : Interfaces for exchanging UFIS data with remote UFIS      */
/*                                                                            */
/* Update history :                                                           */
/* 20050408 JIM: PRF 7161: removed old sccs_version, dbg of mks_version       */
/* 20050805 JIM: PRF 7591: default debug_level==OFF, read config RUNTIME_MODE */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "tcpudp.h"
#include "cedatime.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
extern  int snap(char *,long ,FILE *);
extern int MqToCeda(ITEM **, char *);
extern int CedaToMq(ITEM *,char **);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
enum ConnectionType {UNKNOWN=0, RCV, SND};
enum SessionInfo {CCO=0, BYE};
#define NETREAD_SIZE (35000)
#define RC_INTERNAL  -129
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static unsigned long ulgEventCount = 0;
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static int   igConType     = UNKNOWN;
static int   igSocket = 0;
static char  cgRemoteHostName[32];
static char  cgLocalHostName[32];
static char  cgServiceName[32];
static char  cgSpoolDirectory[512];
static int   igLineStatus = 0;
static int   igAlarmed = FALSE;
static int   igReceiveTimeout = 0;
static int   igSendTimeout = 0;
static int   igWaitConnect = 0;
static int   igLastMsgSent = TRUE;
static int   igReceiver = FALSE;
static int   igSender = FALSE;
static int   igLastEventAcked = TRUE;
static int   igSpoolFileExists = FALSE;
static long  igSpooledEvents = 0;
static long  igRecoveredSpoolEvents = 0;
static FILE  *prgSpoolData = NULL;
static FILE  *prgPositionData = NULL;
static FILE  *prgRecoveredData = NULL;
static long  lgEvtCount = 0;
static long  lgRemEvtCount = 0;
static char  cgSpoolFile[512];
static char  cgRecoveredSpoolFile[512];
static char  cgMovedSpoolFile[532];
static char  cgPositionFile[512];
static char  cgMovedPositionFile[512];
static int	 igWaitAfterSend = 0;
static char  cgWaitCommands[100] = "\0";
static char cgUseFixedLengthHeader[10] = "FALSE";
static char cgSkipRootElement[50] = "";
/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int  Init_Process();
static int  Reset(void);                        /* Reset program          */
static void Terminate(void);                    /* Terminate program      */
static void HandleSignal(int);                  /* Handles signals        */
static void HandleErr(int);                     /* Handles general errors */
static void HandleQueErr(int);                  /* Handles queuing errors */
static int  HandleData(void);                   /* Handles event data     */
static void HandleQueues(void);                 /* Waiting for Sts.-switch*/
static int  SendData(char *, int);
static void HandleConnection(void);
static int HandleSendEventData(ITEM *, int);
static int CheckMyQueue(void);
static int HandleReceiveFromRemote(void);
static int HandleSendToRemote(void);
static int HandleSessionData(int );
static int sendSpooledData();
static int spoolData(char *);
static int resumeRecover(FILE *);
static int removeFile(char *);
static int rememberPosition(long );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    char   *pclBuf = NULL;
    int    ilBufLen = 0;
    int    ilRealLen = 0;
    EVENT  *prlEvent = NULL;


    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);

    /* Attach to the MIKE queues */
    do
      {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
	  {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
	  }/* end of if */
      }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }
    else {
      dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/ceihdl.cfg",getenv("CFG_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
      {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
      }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_Process();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");

    HandleConnection();

    return(0);
    
} /* end of MAIN */

/* ******************************************************************** */
/*                                                                      */
/* ******************************************************************** */
static void HandleConnection(void)
{
  int	ilRC	= RC_SUCCESS;

  for(;;) {

    if (igReceiver) {
      if(igSocket == 0) {

	ilRC = RC_FAIL;
	igAlarmed = FALSE;

	dbg(DEBUG,"<HandleConnection> waiting %d seconds for a connect",igWaitConnect);
	alarm(igWaitConnect);
	ilRC = TcpAccept(&igSocket,cgServiceName,cgLocalHostName);
	alarm(0);
	dbg(DEBUG,"<HandleConnection> Inspecting my queu...");
	ilRC = CheckMyQueue();

	if(ilRC != RC_SUCCESS || igAlarmed) { /* not connected */
	  igSocket = 0;
	  dbg(TRACE,"<HandleConnection> No connection request received...");
	  igLineStatus = 1;
	}/* end of if */
	else {
	  dbg(TRACE,"<HandleConnection> accepted on <%d>",igSocket);
	  
	  igLineStatus = 0;
	  
	  ilRC = HandleReceiveFromRemote();
	  if(ilRC != RC_SUCCESS) {
	    dbg(TRACE,"<HandleConnection> HandleActive failed <%d>",ilRC);
	  }/* end of if */
	}/* end of if */
      }
      else {
	ilRC = HandleReceiveFromRemote();
	if(ilRC != RC_SUCCESS) {
	  dbg(TRACE,"<HandleConnection> HandleReceiveFromRemote failed <%d>",ilRC);
	}/* end of if */
      }/* end of if */
    } /* handle receiver */
    else if (igSender) {
      if(igSocket == 0) {
	errno = 0;

	dbg(DEBUG,"<HandleConnection> try to connect");

	ilRC = TcpConnect(&igSocket,cgServiceName,cgRemoteHostName);
	if(ilRC != RC_SUCCESS) {
	  dbg(TRACE,"<HandleConnection> TcpConnect failed <%d>",ilRC);
	  igSocket = 0;
	  igLineStatus = 1;
	  dbg(DEBUG,"<HandleConnection> Checking my queue...");
	  ilRC = CheckMyQueue();
	  dbg(DEBUG,"<HandleConnection> Waiting %d seconds before connect retry...",igWaitConnect);
	  sleep(igWaitConnect);
	}
	else {
	  dbg(TRACE,"<HandleConnection> connected on <%d>",igSocket);

	  igLineStatus = 0;

	  ilRC = HandleSendToRemote();
	  if(ilRC != RC_SUCCESS) {
	    dbg(TRACE,"<HandleConnection> Connection failed <%d>",ilRC);
	  }/* end of if */
	}/* end of if */
      }
      else {
	ilRC = HandleSendToRemote();
	if(ilRC != RC_SUCCESS) {
	  dbg(TRACE,"<HandleConnection> Connection failed <%d>",ilRC);
	}/* end of if */
      }/* end of if */
    } /* end sender */
    else {
      dbg(TRACE,"<HandleConnection> Neither sender nor receiver -- terminating...");
      Terminate();
    }
  }
}/* end of HandleConnection */
/******************************************************************************/
/* Receive data from remote CEI                                               */
/* Convert data                                                               */
/* Send data to appropriate queue using originator of sender                  */
/******************************************************************************/
static int HandleReceiveFromRemote(void)
{
  int	ilRC = RC_SUCCESS;
  int	ilLoop = TRUE;
  int   ilBufLen = 0;
  int   ilRealLen = 0;
  int   ilOriginator;
  short slFkt = 0;
  char  *pclBuf = NULL;
  char  *pclSqlBuf = NULL;
  EVENT *prlEvent = NULL;
  ITEM  *prlItem = NULL;
  BC_HEAD *prlBchead = NULL;
  CMDBLK  *prlCmdblk = NULL;
  EVENT *prlRemQueEvent = NULL;
  char *pclStart;
  char *pclEnd;
  
  dbg(DEBUG,"<HandleReceiveFromRemote> TcpRecv");
	
  do {
    igAlarmed = FALSE;
    dbg(DEBUG,"<HandleReceiveFromRemote> waiting <%d> seconds for data",igReceiveTimeout);
    alarm(igReceiveTimeout);
    if(strcmp(cgUseFixedLengthHeader,"TRUE") == 0)
    {
        memset(pclBuf,0x00,ilBufLen);
        ilRC = TcpRecvFLH(igSocket,&pclBuf,&ilBufLen);
        dbg(DEBUG,"Bytes Received: <%d>, string: <%s>", strlen(pclBuf),pclBuf);
    } else {
        ilRC = TcpRecv(igSocket,&pclBuf,&ilBufLen);
    }
    alarm(0);
    if(ilRC > RC_SUCCESS) {
      ilRealLen = ilRC;
      ulgEventCount++;
      
      prlItem = malloc(sizeof(ITEM));
      if(strlen(cgSkipRootElement) > 1){
          dbg(TRACE,"now skip root-element <%s>",cgSkipRootElement);
          pclStart = strstr(pclBuf,cgSkipRootElement);
          if(pclStart != NULL){
              pclStart = strchr(pclStart,'>') +1;
          }
          if(pclStart != NULL){
              pclEnd = strstr(pclStart,cgSkipRootElement) - 2;
          }
          if ((pclStart != NULL ) && (pclEnd != NULL)){
              /*strncpy(pclStart,pclBuf,pclEnd-pclStart);*/
              pclBuf = pclStart;
              pclEnd ='\0';
          }

          dbg(DEBUG,"pclBuf:<%s>",pclBuf);
      }
      MqToCeda(&prlItem, pclBuf);

      prlEvent = (EVENT *) prlItem->text;

      dbg(DEBUG,"<HandleReceiveFromRemote> <%d><%d><%d>",prlEvent->originator,ulgEventCount,(prlEvent->data_length+sizeof(EVENT)));
	
      ilRC = RC_SUCCESS;
	
      switch(prlEvent->command) {
      case SESS_DATA:
	prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	dbg(DEBUG,"<HandleReceiveFromRemote> Received <%s>",prlCmdblk->command);
	break;
      case TRACE_ON :
	dbg_handle_debug(prlEvent->command);
	break;

      case TRACE_OFF :
	dbg_handle_debug(prlEvent->command);
	break;

      case REMOTE_DB :
	ilRC = que(QUE_PUT,prlEvent->originator,mod_id,PRIORITY_3,ilRealLen,pclBuf);
	if(ilRC != RC_SUCCESS) {
	  HandleQueErr(ilRC);
	  DebugPrintEvent(TRACE,prlEvent);
	}/* end of if */
	break;

      case EVENT_DATA :
	prlRemQueEvent = (EVENT *) prlItem->text;
	lgRemEvtCount++;
	dbg(TRACE,"========== START REMOTE EVENT <%10.10d> ==========",lgRemEvtCount);
	ilOriginator = prlEvent->originator;
	prlEvent->originator = mod_id;
	prlBchead = (BC_HEAD *) (((char *)prlEvent) + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	prlBchead->rc = NETOUT_NO_ACK; /* no acknowledge wanted! */
	/*	ilRC = que(prlItem->function,ilOriginator,prlItem->originator,prlItem->priority,prlItem->msg_length,(char *)prlEvent); */
	ilRC = que(prlItem->function,ilOriginator,mod_id,prlItem->priority,prlItem->msg_length,(char *)prlEvent);

	dbg(DEBUG,"<HandleReceiveFromRemote>Sending Func: <%d> Src: <%d> Dst: <%d> Prio: <%d>",
	    prlItem->function,mod_id,ilOriginator,prlItem->priority);
	DebugPrintItem(DEBUG,prlItem);
	DebugPrintEvent(DEBUG,prlEvent);
	if(ilRC != RC_SUCCESS) {
	  HandleQueErr(ilRC);
	  DebugPrintItem(TRACE,prlItem);
	  DebugPrintEvent(TRACE,prlEvent);
	}/* end of if */
	if (strstr(cgWaitCommands,prlCmdblk->command) != NULL)
	{
		dbg(TRACE,"Command <%s> fits WaitCommands-list <%s>. Wait <%d> seconds.",prlCmdblk->command,cgWaitCommands,igWaitAfterSend);
		sleep(igWaitAfterSend);
	}
	dbg(TRACE,"========== END REMOTE EVENT <%10.10d> ==========",lgRemEvtCount);
	break;

      default :
	DebugPrintEvent(TRACE,prlEvent);
	DebugPrintItem(TRACE,prlItem);
	dbg(TRACE,"<HandleReceiveFromRemote> UNKOWN event command");
	snap(pclBuf,ilBufLen,outp);
	break;
      }/* end of switch */
    }
    else {
      if(ilRC == RC_TIMEOUT) {
	dbg(DEBUG,"<HandleReceiveFromRemote> TcpRecv returned RC_TIMEOUT <%d>",ilRC);
      }
      else {
	dbg(TRACE,"<HandleReceiveFromRemote> TcpRecv failed <%d>",ilRC);
	igSocket = 0;
	ilLoop = FALSE;
      }/* end of if */
    }/* end of if */

    ilRC = CheckMyQueue();
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"<HandleReceiveFromRemote> CheckMyQueue failed <%d>",ilRC);
      ilLoop = FALSE;
    }/* end of if */

  } while(ilLoop == TRUE);

  free(pclBuf); /* was allocated by TcpRecv */
  free(prlItem);
  return(ilRC);
}
/******************************************************************************/
/* Send data to remote CEI                                                    */
/* Convert data                                                               */
/******************************************************************************/
static int HandleSendToRemote(void)
{
  int ilRC = RC_SUCCESS;

  dbg(DEBUG,"<HandleSendToRemote> Entering...");
  for (;;) {
    igAlarmed = FALSE;
    dbg(DEBUG,"<HandleSendToRemote> Trying to send spooled data...");
    sendSpooledData();
    dbg(DEBUG,"<HandleSendToRemote> Waiting <%d> seconds for queue event",igSendTimeout);
    alarm(igSendTimeout);
    ilRC = que(QUE_GETBIG,0,mod_id,0,0,(char *)&prgItem);
    alarm(0);
    if (igAlarmed) {
      dbg(DEBUG,"<HandleSendToRemote> No data from queue sending CCO...");
      ilRC = HandleSessionData(CCO);
      if (ilRC != RC_SUCCESS) {
	if (igLineStatus == 1 || igSocket < 0) {
	  dbg(TRACE,"<HandleSendToRemote> Line down -- trying to reconnect");
	  return RC_FAIL;
	}
      }
      ilRC = QUE_E_NOMSG;
    }
    if(ilRC == RC_SUCCESS) {
      /* Acknowledge the item */
      ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
      if( ilRC != RC_SUCCESS ) {
	/* handle que_ack error */
	HandleQueErr(ilRC);
      }

      prgEvent = (EVENT *) prgItem->text;
      lgEvtCount++;

      switch( prgEvent->command ) {
      case LOG_SWITCH :
	SwitchDebugFile(TRACE,0);
	break;
      
      case TRACE_ON :
	dbg_handle_debug(prgEvent->command);
	break;
      
      case TRACE_OFF :
	dbg_handle_debug(prgEvent->command);
	break;
      
      case SHUTDOWN :
	/*
	**DebugPrintItem(TRACE,prgItem);
	**DebugPrintEvent(TRACE,prgEvent);
	*/
	dbg(TRACE,"<HandleSendToRemote> received SHUTDOWN");
	ilRC = que(QUE_ACK,0,mod_id,PRIORITY_3,0,NULL);
	if(ilRC != RC_SUCCESS) {
	  dbg(TRACE,"<HandleSendToRemote> QUE_ACK failed");
	  HandleQueErr(ilRC);
	}/* end if */
	HandleSessionData(BYE);
	Terminate();
	break;

      case EVENT_DATA :
	dbg(TRACE,"<HandleSendToRemote> received EVENT_DATA");
	ilRC = HandleData();
	if (ilRC != RC_INTERNAL) {
	  dbg(DEBUG,"<HandleSendToRemote> Sending event data...");
	  ilRC = HandleSendEventData(prgItem,prgItem->msg_length);
	  if (ilRC != RC_SUCCESS) {
	    if (igLineStatus == 1 || igSocket < 0) {
	      dbg(TRACE,"<HandleSendToRemote> Line down -- trying to reconnect");
	      return RC_FAIL;
	    }
	  }
	  /*
	  **DebugPrintItem(DEBUG,prgItem);
	  **DebugPrintEvent(DEBUG,prgEvent);
	  */
	}
	break;

      default :
	dbg(TRACE,"<HandleSendToRemote> UNKNOWN CMD");
	DebugPrintItem(TRACE,prgItem);
	DebugPrintEvent(TRACE,prgEvent);
	break;
      }/* end switch */
    }
    else if(ilRC != QUE_E_NOMSG) {
      dbg(TRACE,"<HandleSendToRemote> receive error");
    }
  }
}

/******************************************************************************/
/* Try to reconnect a lost line                                               */
/******************************************************************************/
int reconnectLine()
{
  int ilRC;

  if (igSocket != 0) TcpClose(igSocket);
  igSocket = 0;
  errno = 0;

  dbg(DEBUG,"<reconnectLine> try to reconnect");

  ilRC = TcpConnect(&igSocket,cgServiceName,cgRemoteHostName);
  if(ilRC != RC_SUCCESS) {
    dbg(TRACE,"<reconnectLine> TcpConnect failed <%d>",ilRC);
    igSocket = 0;
    igLineStatus = 1;
  }
  else igLineStatus = 0;
  return ilRC;
}
/******************************************************************************/
/* Send event data                                                            */
/* First the line status is checked and a reconnect is performed, if line     */
/* is down                                                                    */
/* Data is converted to an ASCII (xml like) data stream                       */
/* If send was not successful for some reason, data is spooled                */ 
/******************************************************************************/
static int HandleSendEventData(ITEM *prpItem, int ipItemCount)
{
  char *pclBuff;
  int  ilRC = RC_SUCCESS;

  lgRemEvtCount++;

  dbg(TRACE,"========== START SEND REMOTE EVENT <%10.10d> ==========",lgRemEvtCount);
  if (igLineStatus == 1 || igSocket < 0) {
    dbg(DEBUG,"<HandleSendEventData> Trying to reconnect...");
    ilRC = reconnectLine();
  }

  pclBuff = malloc(sizeof(ITEM));
  CedaToMq(prpItem,&pclBuff);
  ilRC = SendData(pclBuff,strlen(pclBuff));
  if (ilRC != RC_SUCCESS) ilRC = spoolData(pclBuff);
  free(pclBuff);
  
  dbg(TRACE,"========== END SEND REMOTE EVENT <%10.10d> ==========",lgRemEvtCount);
  return ilRC;
}
/******************************************************************************/
/* Open/create a spool file                                                   */
/******************************************************************************/
int openSpoolFile(char *pcpMode)
{
  prgSpoolData = fopen(cgSpoolFile,pcpMode);
  if (prgSpoolData == NULL) {
    dbg(TRACE,"<openSpoolFile> Open of spool file <%s> failed <%s>",cgSpoolFile,strerror(errno));
    return RC_FAIL;
  }
  else return RC_SUCCESS;
}
/******************************************************************************/
/* Open/create a file to write the recovered items                            */
/******************************************************************************/
int openRecoveredSpoolFile(char *pcpMode)
{
  prgRecoveredData = fopen(cgRecoveredSpoolFile,pcpMode);
  if (prgRecoveredData == NULL) {
    dbg(TRACE,"<openRecoveredSpoolFile> Open of spool file <%s> failed <%s>",cgRecoveredSpoolFile,strerror(errno));
    return RC_FAIL;
  }
  else return RC_SUCCESS;
}

/******************************************************************************/
/* Data spooler                                                               */
/* Data is written/appended to a new/existing spool file adding a time stamp  */
/******************************************************************************/
static int spoolData(char *pcpBuff)
{
  char clTimeStamp[15];
  int  ilRC = RC_SUCCESS;
  char clMode[8];

  if (access(cgSpoolFile,F_OK)==0) sprintf(clMode,"a+");
  else sprintf(clMode,"w+");

  if (prgSpoolData == NULL) ilRC = openSpoolFile(clMode);

  if (ilRC == RC_SUCCESS) {
    GetServerTimeStamp("LOC",1,0,clTimeStamp);
    ilRC = fseek(prgSpoolData,0,SEEK_END); /* position to EOF */
    if (ilRC < 0) dbg(TRACE,"<spoolData> Unable to position file pointer <%s>",strerror(errno));
    ilRC = fprintf(prgSpoolData,"<EventEntry>\n<FrameLen>%d</FrameLen>\n<TimeStamp>%s</TimeStamp>\n%s</EventEntry>\n",strlen(pcpBuff),clTimeStamp,pcpBuff);
    if (ilRC<0) {
      dbg(TRACE,"<spoolData> Unable to spool data <%s>",strerror(errno));
    }
    else {
      igSpooledEvents++;
      dbg(DEBUG,"<spoolData> <%ld> Events successfully spooled",igSpooledEvents);
    }
    fclose(prgSpoolData);
    prgSpoolData = NULL;
  }
  return ilRC;
}
/******************************************************************************/
/* Data spooler                                                               */
/* Data is written/appended to a new/existing spool file adding a time stamp  */
/******************************************************************************/
static int spoolRecoveredData(char *pcpBuff)
{
  int  ilRC = RC_SUCCESS;
  char clMode[8];

  if (access(cgRecoveredSpoolFile,F_OK)==0) sprintf(clMode,"a+");
  else sprintf(clMode,"w+");

  if (prgRecoveredData == NULL) ilRC = openRecoveredSpoolFile(clMode);

  if (ilRC == RC_SUCCESS) {
    ilRC = fseek(prgRecoveredData,0,SEEK_END); /* position to EOF */
    if (ilRC < 0) dbg(TRACE,"<spoolData> Unable to position file pointer <%s>",strerror(errno));
    ilRC = fprintf(prgRecoveredData,"%s",pcpBuff);
    if (ilRC<0) {
      dbg(TRACE,"<spoolData> Unable to spool recovered data <%s>",strerror(errno));
    }
    else {
      dbg(DEBUG,"<spoolData> <%ld> Events successfully recovered",igRecoveredSpoolEvents);
    }
    fclose(prgRecoveredData);
    prgRecoveredData = NULL;
  }
  return ilRC;
}

/******************************************************************************/
/* get the next item from buffer                                              */
/* item starts with <EventEntry> end marker is </EventEntry>                  */
/******************************************************************************/
char *getNextItem(FILE *pcpSpoolData,long *lpNextData)
{
  #define FILEBUF_SIZE 8192
  #define LINE_SIZE 256
  static long ilFilePosition;
  static long ilLineCounter;
  static char clLine[LINE_SIZE];
  static char clBuff[FILEBUF_SIZE];
  static int  ilRc = RC_SUCCESS;	
  char *s;
  char *pclBuff;
  
  if (pcpSpoolData == NULL) {
	dbg(TRACE,"<getNextItem> Fatal -- spool file not open");
	return NULL;
  }
  ilFilePosition = ftell(pcpSpoolData); /* get actual position */
  
  if (ilFilePosition<0) {
    dbg(TRACE,"<getNextItem> Position request on spool file <%s> failed <%s>",cgSpoolFile,strerror(errno));
    return NULL;
  }
  if (ilFilePosition == 0) {
	  ilLineCounter = 0;
  }
  dbg(TRACE,"<getNextItem> Spool file starts on %d", ilFilePosition);
  pclBuff = &clBuff[0];
  /* search start of item */
  do {
	s = fgets(pclBuff,sizeof(clBuff),pcpSpoolData);
	ilLineCounter++;
	/*
	dbg(TRACE,"<getNextItem> returned Buffer: <%s>%d",s,strcmp(s,"<EventEntry>"));
	dbg(TRACE,"<getNextItem> filled Buffer: <%s>",pclBuff);
	*/
	if (strstr(s,"<EventEntry>")) {
		dbg(TRACE,"<getNextItem> Found start");
		break;
	}
  } while ( s != NULL);
  if (s == NULL) {
	  dbg(TRACE,"<getNextItem> Error reading spool file, event start not found. Current Buffer: %s", pclBuff);
	  return NULL;
  }
  while ((s=fgets(clLine,sizeof(clLine),pcpSpoolData)) != NULL) {
	ilLineCounter++;
	/*dbg(TRACE,"<getNextItem> Event next line: %s",s);*/
	if (strlen(pclBuff)+LINE_SIZE >= FILEBUF_SIZE) {
		dbg(TRACE,"<getNextItem> Event next line: %s",s);
		dbg(TRACE,"<getNextItem> Event current Buffer: \n%s ",pclBuff);
		dbg(TRACE,"<getNextItem> Buffer will overflow, terminating operation");
		return NULL;
	}
	pclBuff = strcat(pclBuff, &clLine[0]);
	
	/*dbg(TRACE,"<getNextItem> Event current Buffer: %s ",pclBuff);*/
	if (strstr(s,"</EventEntry>")) {
		dbg(TRACE,"<getNextItem> Found end");
		break;
	}
  }
  if (s == NULL && !feof(pcpSpoolData)) {
	  dbg(TRACE,"<getNextItem> Error reading spool file at line <%ld>. Current Buffer: \n%s", ilLineCounter, clBuff);
	  return NULL;
  }
  *lpNextData = ftell(pcpSpoolData);
  
  dbg(TRACE,"<getNextItem> Spool file at line <%ld>.", ilLineCounter);
  return clBuff; 
}
/******************************************************************************/
/* Send data from spooler                                                     */
/******************************************************************************/
static int sendSpooledData()
{
  char *clData;
  int  ilRC = RC_SUCCESS;
  int  ilFileCompleted = FALSE;
  int  ilFirstEntry = TRUE;
  long ilLastSucceeded;
  long ilNextData;
  long ilEofPosition;
  long ilEventCounter = 0;
  long ilFilePosition;

  if (access(cgSpoolFile,F_OK)==0) {
    if (prgSpoolData == NULL) ilRC = openSpoolFile("r+");
    if (ilRC == RC_FAIL) {
      dbg(TRACE,"<sendSpooledData> Fatal error -- unable to open file");
      return RC_FAIL;
    }
    
    /* fetch EOF position */
    ilRC = fseek(prgSpoolData,0,SEEK_END);
    if (ilRC<0) {
      dbg(TRACE,"<sendSpooledData> Unable to position spool file pointer <%s>",strerror(errno));
      return RC_FAIL;
    }
    ilEofPosition = ftell(prgSpoolData);
    if (ilEofPosition<0) {
      dbg(TRACE,"<sendSpooledData> Unable to fetch spool file pointer position <%s>",strerror(errno));
      return RC_FAIL;
    }
    
    dbg(DEBUG,"<sendSpooledData> EOF position is <%ld>",ilEofPosition);

    ilRC = resumeRecover(prgSpoolData);
    
    /* check if file position is behind EOF */
    ilFilePosition = ftell(prgSpoolData);
    if (ilFilePosition >= ilEofPosition) {
	    dbg(DEBUG,"<sendSpooledData> Fileposition <%ld> behind EOF position <%ld> removing position file", ilFilePosition, ilEofPosition);
	    removeFile(cgPositionFile);
	    dbg(DEBUG,"<sendSpooledData> Forced rewind of spool file");
	    rewind(prgSpoolData);
    }
    if (ilRC == RC_SUCCESS) {
      while (ilFileCompleted == FALSE) {
	clData = getNextItem(prgSpoolData,&ilNextData);
	if (clData != NULL) {
	  dbg(DEBUG,"<sendSpooledData> Sending <\n%s>",clData);
	  ilRC = SendData(clData,strlen(clData));
	  /*free(clData);*/
	  if (ilRC == RC_SUCCESS) {
	    ilEventCounter++;
	    igSpooledEvents--;
	    igRecoveredSpoolEvents++;
	    spoolRecoveredData(clData);
	    dbg(TRACE,"<sendSpooledData> <%ld> events recovered",igRecoveredSpoolEvents);
	    if (ilEofPosition <= ilNextData) ilFileCompleted = TRUE;
	    rememberPosition(ilNextData); /* this event was succesfully spooled */
	    if (ilFileCompleted) {
	      removeFile(cgPositionFile);
	      removeFile(cgSpoolFile);
	      removeFile(cgRecoveredSpoolFile);
	      dbg(TRACE,"<sendSpooledData> Spool file completed, spool and position file removed..");
	      break;
	    }
	  } else {
	     /*rememberPosition(ilNextData);*/
	     break;
	  }
	} else {
	    dbg(TRACE,"<getNextItem> spool file corrupted, moving this file away.");
       }
    }
  fclose(prgSpoolData);
  dbg(TRACE,"<getNextItem> spool file closed");
  prgSpoolData = NULL;
  dbg(DEBUG,"<sendSpooledData> <%ld> spooled events successfully sent",ilEventCounter);
  } else dbg(DEBUG,"<sendSpooledData> No spooled data available");
  }
}
/******************************************************************************/
/* Remove a file by path+name                                                 */ 
/******************************************************************************/
static int removeFile(char *pcpFileName)
{
  int ilRC;
  
  remove(pcpFileName);
  dbg(DEBUG,"<removeFile> Removing file <%s>",pcpFileName);
  if (ilRC<0) {
    dbg(TRACE,"<removeFile> Unable to remove file <%s> error <%s>",cgPositionFile,strerror(errno));
    return RC_FAIL;
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* Write fileposition to a specific file                                      */
/******************************************************************************/
static int rememberPosition(long lpPosition)
{
  int ilRC;

  prgPositionData = fopen(cgPositionFile,"w+");
  if (prgPositionData==NULL) {
    dbg(TRACE,"<rememberPosition> Unable to create file <%s> error <%s>",cgPositionFile,strerror(errno));
    return RC_FAIL;
  }
  ilRC = fprintf(prgPositionData,"%ld",lpPosition);
  if (ilRC<0) {
    dbg(TRACE,"<rememberPosition> Unable to read position from file <%s> error <%s>",cgPositionFile,strerror(errno));
    ilRC = RC_FAIL;
  }
  else ilRC = RC_SUCCESS;

  fclose(prgPositionData);
  return ilRC;
}
/******************************************************************************/
/* Read a fileposition from a specific file if the file exists,               */
/* else position to begin of file                                             */
/******************************************************************************/
static int resumeRecover(FILE *prpSpoolData)
{
  long ilPosition = 0;
  int  ilRC;

  if (access(cgPositionFile,R_OK)==0) {
    prgPositionData = fopen(cgPositionFile,"r");
    if (prgPositionData==NULL) {
      dbg(TRACE,"<resumeRecover> Unable to open file <%s> error <%s>",cgPositionFile,strerror(errno));
      return RC_FAIL;
    }
    ilRC = fscanf(prgPositionData,"%ld",&ilPosition);
    if (ilRC<0) {
      dbg(TRACE,"<resumeRecover> Unable to read position from file <%s> error <%s>",cgPositionFile,strerror(errno));
      fclose(prgPositionData);
      return RC_FAIL;
    }
    ilRC = fseek(prpSpoolData,ilPosition,SEEK_SET);
    dbg(DEBUG,"<resumeRecover> Positioning file to <%ld>",ilPosition);
    if (ilRC<0) {
      dbg(TRACE,"<resumeRecover> Unable to position to <%ld> in file <%s> error <%s>",ilPosition,cgSpoolFile,strerror(errno));
      fclose(prgPositionData);
      return RC_FAIL;
    }
    fclose(prgPositionData);
  }
  else rewind(prpSpoolData);
  return RC_SUCCESS;
}
/******************************************************************************/
/* Send session data                                                          */
/* CCO -- is sent whenever triggerred by ntisch as a keep alive synch         */
/* BYE -- is sent when a shutdown was requested to the local process          */
/******************************************************************************/
static int HandleSessionData(int ipMsgTyp)
{
  char clBuff[256];
  char clCmd[16];
  int  ilFunction = SESS_DATA;
  int  ilSendData = FALSE;
  int  ilRC = RC_SUCCESS;

  switch(ipMsgTyp) {
  case CCO:
    sprintf(clCmd,"CCO");
    ilSendData = TRUE;
    break;
  case BYE:
    sprintf(clCmd,"BYE");
    ilSendData = TRUE;
    break;
  default:
    dbg(TRACE,"<HandleSessionData> Invalid message type <%d>",ipMsgTyp);
    break;
  }
  if (ilSendData) {
    sprintf(clBuff,"<EVENT_COMMAND>%d</EVENT_COMMAND>\n<CMD>%s</CMD>\n",ilFunction,clCmd);
    if (igLineStatus == 1 || igSocket < 0) ilRC = reconnectLine();
    if (ilRC != RC_SUCCESS) return ilRC;
    ilRC = SendData(clBuff,strlen(clBuff));
    if (ilRC != RC_SUCCESS) {
      ilRC = SendData(clBuff,strlen(clBuff)); /* try again */
    }
  }
  return ilRC;
}
/******************************************************************************/
/* Check the queue with nowait                                                */
/******************************************************************************/
int CheckMyQueue(void)
{
  int ilRC = RC_SUCCESS;

  igLastEventAcked = FALSE;
  ilRC = que(QUE_GETBIGNW,0,mod_id,0,0,(char *)&prgItem);

  if(ilRC == RC_SUCCESS) {
    prgEvent = (EVENT *) prgItem->text;
    lgEvtCount++;

    switch( prgEvent->command ) {
    case LOG_SWITCH :
      SwitchDebugFile(TRACE,0);
      break;
      
    case TRACE_ON :
      dbg_handle_debug(prgEvent->command);
      break;
      
    case TRACE_OFF :
      dbg_handle_debug(prgEvent->command);
      break;
      
    case SHUTDOWN :
      DebugPrintItem(TRACE,prgItem);
      DebugPrintEvent(TRACE,prgEvent);

      dbg(TRACE,"<CheckMyQueue> received SHUTDOWN");
      ilRC = que(QUE_ACK,0,mod_id,PRIORITY_3,0,NULL);
      if(ilRC != RC_SUCCESS) {
	dbg(TRACE,"<CheckMyQueue> QUE_ACK failed");
	HandleQueErr(ilRC);
      }/* end if */
      HandleSessionData(BYE);
      Terminate();
      break;

    case EVENT_DATA :
      dbg(TRACE,"<CheckMyQueue> received EVENT_DATA");
      ilRC = HandleData();
      if (ilRC != RC_INTERNAL) {
	if (igSender == TRUE) {
	  ilRC = HandleSendEventData(prgItem,prgItem->msg_length);
	  DebugPrintItem(DEBUG,prgItem);
	  DebugPrintEvent(DEBUG,prgEvent);
	}
	else {
	  dbg(TRACE,"<CheckMyQueue> Received send request, but role is receiver -- dropping request");
	  dbg(TRACE,"<CheckMyQueue> Requested Event was:");
	  DebugPrintItem(TRACE,prgItem);
	  DebugPrintEvent(TRACE,prgEvent);
	}
      }
      break;

    case REMOTE_QUE :
      dbg(TRACE,"<CheckMyQueue> received REMOTE_QUE");
      DebugPrintItem(TRACE,prgItem);
      DebugPrintEvent(TRACE,prgEvent);
      break;

    case REMOTE_DB :
      dbg(TRACE,"<CheckMyQueue> got REMOTE_DB");
      DebugPrintItem(TRACE,prgItem);
      DebugPrintEvent(TRACE,prgEvent);
      break;

    default :
      dbg(TRACE,"<CheckMyQueue> UNKNOWN CMD");
      DebugPrintItem(TRACE,prgItem);
      DebugPrintEvent(TRACE,prgEvent);
      break;
    }/* end switch */

    if(igLastEventAcked == FALSE) {
      ilRC = que(QUE_ACK,0,mod_id,PRIORITY_3,0,NULL);
      if(ilRC != RC_SUCCESS) {
	dbg(TRACE,"CheckMyQueue: QUE_ACK failed");
	HandleQueErr(ilRC);
      }/* end if */
    }/* end if */
  }
  else {

    if(ilRC != QUE_E_NOMSG) {
      dbg(TRACE,"CheckMyQueue: QUE_GETNW failed");
      HandleQueErr(ilRC);
    }
    else {
      ilRC = RC_SUCCESS;
    }/* end if */

  }/* end if */

  return(ilRC);
}
/* ******************************************************************** */
/*                                                                      */
/* ******************************************************************** */
static int SendData(char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
	char header[16];
	char trailer[16];
	
	sprintf(header,"<EventEntry>\n");
	sprintf(trailer,"</EventEntry>\n");

	if (igSocket < 0) reconnectLine();
	if (igSocket < 0) {
		dbg(DEBUG,"<SendData> Unable to reconnect...");
		return(RC_FAIL);
	}		
	TcpSend(igSocket,header,13);
  ilRC = TcpSend(igSocket,pcpBuf,ipBufLen);
	TcpSend(igSocket,trailer,14);
        if(ilRC == ipBufLen)
        {
			ulgEventCount++;
			dbg(DEBUG,"<SendData> <%d><%d><%d>",((EVENT *)pcpBuf)->originator,ulgEventCount,ipBufLen);
			ilRC = RC_SUCCESS;
        } else{
			igLastMsgSent = FALSE;
			ulgEventCount--;
			igSocket = 0;
			reconnectLine();
			dbg(TRACE,"<SendData> TcpSend failed <%d>",ilRC);
        }/* end of if */

        return(ilRC);

} /* end of SendData */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_Process()
{
  int  ilRC        = RC_SUCCESS;            /* Return code */
  int  ilTmpCfgInt = 0;
  char clRole[16];
  char clDebug[32] = "\0";

  /* now reading from configfile or from database */
  SetSignals(HandleSignal);
  igInitOK = TRUE;

  debug_level=TRACE;
  memset(cgConfigFile,0x00,128);
  sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  sprintf(cgPositionFile,"/ceda/conf/%s.pos",mod_name);

  dbg(TRACE,"<Init_Process> using <%s> as config-file",cgConfigFile);

  igItemLen = NETREAD_SIZE;
  prgItem = (ITEM *) calloc(1,igItemLen);
  if(prgItem == NULL) {
    dbg(TRACE,"<Init_Process> item calloc failed on <%d> bytes",igItemLen);
    ilRC = RC_FAIL;
  } /* end if */
  prgEvent = (EVENT *)  prgItem->text;

 

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","RHOST",CFG_STRING,(char*)&cgRemoteHostName[0]);
    if(ilRC != RC_SUCCESS) {
	dbg(TRACE,"<Init_Process> RHOST not found in <%s> section [main]",cgConfigFile);
    }/* end of if */
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","LHOST",CFG_STRING,(char*)&cgLocalHostName[0]);
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"<Init_Process> LHOST not found in <%s> section [main]",cgConfigFile);
    }/* end of if */
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","SERVICE",CFG_STRING,(char*)&cgServiceName[0]);
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"<Init_Process> SERVICE not found in <%s> section [main]",cgConfigFile);
    }/* end of if */
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","RCVTMOUT",CFG_INT,(char*)&ilTmpCfgInt);
    if(ilRC != RC_SUCCESS) {
      igReceiveTimeout = 60;
    }
    else {
      igReceiveTimeout = ilTmpCfgInt;
    }/* end of if */
    ilRC = RC_SUCCESS;
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","SNDTMOUT",CFG_INT,(char*)&ilTmpCfgInt);
    if(ilRC != RC_SUCCESS) {
      igSendTimeout = 60;
    }
    else {
      igSendTimeout = ilTmpCfgInt;
    }/* end of if */
    ilRC = RC_SUCCESS;
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","CONRTYWT",CFG_INT,(char*)&ilTmpCfgInt);
    if(ilRC != RC_SUCCESS) {
      igWaitConnect = 10;
    }
    else {
      igWaitConnect = ilTmpCfgInt;
    }/* end of if */
    ilRC = RC_SUCCESS;
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","SPOOLDIRECTORY",CFG_STRING,(char*)&cgSpoolDirectory[0]);
    if(ilRC != RC_SUCCESS) {
      sprintf(cgSpoolFile,"/ceda/conf/%s.xml",mod_name);
      sprintf(cgRecoveredSpoolFile,"/ceda/conf/%s_recovered.xml",mod_name);
    }/* end of if */
    else {
      sprintf(cgSpoolFile,"%s/%s.xml",cgSpoolDirectory,mod_name);
      sprintf(cgRecoveredSpoolFile,"%s/%s_recovered.xml",cgSpoolDirectory,mod_name);
    }
    ilRC = RC_SUCCESS;
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","WAIT_AFTER_SEND",CFG_INT,(char*)&igWaitAfterSend);
    if(ilRC != RC_SUCCESS) {
      igWaitAfterSend = 0;
    }/* end of if */
    ilRC = RC_SUCCESS;
	dbg(TRACE,"<Init_Process> [main]<WAIT_AFTER_SEND>  Value: <%d>",igWaitAfterSend);
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","WAIT_COMMANDS",CFG_STRING,(char*)&cgWaitCommands[0]);
    if(ilRC != RC_SUCCESS) {
      cgWaitCommands[0] = '\0';
    }/* end of if */
	dbg(TRACE,"<Init_Process> [main]<WAIT_COMMANDS>  Value: <%s>",cgWaitCommands);
    ilRC = RC_SUCCESS;
  }/* end of if */

  if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","ROLE",CFG_STRING,(char*)&clRole[0]);
    if(ilRC != RC_SUCCESS) {
      dbg(TRACE,"<Init_Process> Fatal -- no role specified(need to know if Sender or Receiver), exiting");
      exit(0);
    }
    else {
      if (!strcmp(clRole,"SND")) {
	igSender = TRUE;
	igReceiver = FALSE;
	dbg(DEBUG,"<Init_Process> Role is Sender");
      }
      else if (!strcmp(clRole,"RCV")) {
	igSender = FALSE;
	igReceiver = TRUE;
	dbg(DEBUG,"<Init_Process> Role is Receiver");
      }
      else {
	dbg(TRACE,"<Init_Process> Specified Role <%s> not allowed -- exiting");
	exit(0);
      }
    }
	dbg(TRACE,"---------------------- process initialized ------------------");
 if(ilRC == RC_SUCCESS) {
    ilRC = iGetConfigEntry(cgConfigFile,"main","RUNTIME_MODE",CFG_STRING,(char*)&clDebug[0]);
    if(ilRC == RC_SUCCESS) {
       if (strcmp(clDebug,"TRACE")==0)
       {
         debug_level=TRACE;
         dbg(TRACE,"<Init_Process> [main]/RUNTIME_MODE: LOGFILE set to TRACE mode");
       }
       else if (strcmp(clDebug,"DEBUG")==0)
       {
         debug_level=DEBUG;
         dbg(TRACE,"<Init_Process> [main]/RUNTIME_MODE: LOGFILE set to DEBUG mode");
       }
       else
       {  /* entry not DEBUG/TRACE : */
         dbg(TRACE,"<Init_Process> [main]/RUNTIME_MODE: LOGFILE set to OFF mode");
         debug_level=0;
       }/* end of if */
    }
    else
    { /* entry not found: */
      dbg(TRACE,"<Init_Process> [main]/RUNTIME_MODE: LOGFILE set to OFF mode");
      debug_level=0;
    }/* end of if */
    ilRC = RC_SUCCESS;
  }/* end of if */ 
  }

    if (ilRC == RC_SUCCESS) {
        ilRC = iGetConfigEntry(cgConfigFile, "main", "USE_FIXED_LENGHT_HEADER", CFG_STRING, (char*) &cgUseFixedLengthHeader[0]);
        if (ilRC != RC_SUCCESS) {
            cgUseFixedLengthHeader[0] = "FALSE";
        }/* end of if */
        dbg(TRACE, "<Init_Process> [main]<USE_FIXED_LENGHT_HEADER>  Value: <%s>", cgUseFixedLengthHeader);
        ilRC = RC_SUCCESS;
    }/* end of if */


   if (ilRC == RC_SUCCESS) {
        ilRC = iGetConfigEntry(cgConfigFile, "main", "SKIP_ROOT_ELEMENT", CFG_STRING, (char*) &cgSkipRootElement[0]);
        if (ilRC != RC_SUCCESS) {
            cgSkipRootElement[0] = "";
        }/* end of if */

        dbg(TRACE, "<Init_Process> [main]<SKIP_ROOT_ELEMENT>  Value: <%s>", cgUseFixedLengthHeader);
        ilRC = RC_SUCCESS;
    }/* end of if */


  return(ilRC);
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    dbg(TRACE,"Terminate: now leaving ...");
    TcpClose(igSocket);
    if (prgSpoolData != NULL) fclose(prgSpoolData); 
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    dbg(DEBUG,"<HandleSignal> signal <%d> received",pipSig);
    switch(pipSig) {
    case SIGALRM:
      dbg(DEBUG,"<HandleSignal> received alarm");
      igAlarmed = TRUE;
      break;
      
    case SIGPIPE:
      dbg(DEBUG,"<HandleSignal> received broken pipe");
      TcpClose(igSocket);
      igSocket = 0;
      igLineStatus = 1;
      break;
      
    case SIGTERM:
      dbg(TRACE,"<HandleSignal> received termination");
      TcpClose(igSocket);
      igSocket = 0;
      Terminate();
      break;
      
    default:
      dbg(TRACE,"<HandleSignal> signal <%d> received - terminating");
      Terminate();
      break;
    } /* end of switch */
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_Process();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int    ilRC = RC_SUCCESS;            /* Return code */
  int      ilCmd          = 0;

  BC_HEAD *prlBchead       = NULL;
  CMDBLK  *prlCmdblk       = NULL;
  char    *pclSelection    = NULL;
  char    *pclFields       = NULL;
  char    *pclData         = NULL;
  char    *pclRow          = NULL;

  prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
  pclSelection = prlCmdblk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
  pclData      = pclFields + strlen(pclFields) + 1;

  dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCount);
  dbg(DEBUG,"<HandleData> Received <%s>",prlCmdblk->command);
  if (!strcmp(prlCmdblk->command,"CCO")) {
    HandleSessionData(CCO);
    ilRC = RC_INTERNAL;
  }
  else return RC_SUCCESS;
  dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCount);
  return ilRC;
    
} /* end of HandleData */
/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
