#ifndef _DEF_mks_version_fdisxm
  #define _DEF_mks_version_fdisxm
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdisxm[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdisxm.src 1.10 2006/08/28 16:35:16SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  SMI                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDISXM handle functions                                                    */
/* -----------------------                                                    */
/* static int HandleSXM (char *pcpData, int ipTyp)                            */


static int HandleSXM(char *pcpData,int ipTyp)
{
  int ilRC=RC_SUCCESS;
  int ilExit=FALSE;
  int ilPos=0;
  int ilCol=0;
  int ilLine=0;
  int ilCount=0;
  char pclFunc[]="HandleSXM:";
  char pclResult[100];
  char pclTable[10];
  char *pclPtr;
  T_TLXRESULT prlTlxResult;

  time_t tlTime = 0;
  time_t tlNewTime = 0;
  struct tm *_tm;
  struct tm rlTm;
  char pclTimeBuf[20];

  memset(&prlTlxResult,0x00,TLXRESULT);
  strcpy(pcgCsgn,"");

  ilRC = FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"-ARCID",
                        '\n',&ilLine,&ilCol,&ilPos);
  if (ilRC == RC_SUCCESS)
  {
     /* point behind -ARCID */
     ilPos += 6+rgTlxInfo.TxtStart;
     pclPtr = pcpData+ilPos;
     CopyLine(pclResult,pclPtr);
     dbg(DEBUG,"%s Found Flight Info <%s>",pclFunc,pclResult);
     strcpy(pcgCsgn,pclResult);
     ConvertFlightLine(pclResult);
#if 0
     pclPtr = pclResult;
     ilRC = GetAlc(&pclPtr,prgMasterTlx);
     if (ilRC == RC_SUCCESS)
        ilRC = GetFltn(&pclPtr,prgMasterTlx);
     if (ilRC == RC_SUCCESS)
     {
        /* Search first alphanumeric character */
        /* Is alpha character, possible Suffix */
        if ((isalpha(*pclPtr) != 0))
        {
           /* is next character not alphanumeric, then char. is Suffix */
           memset(&prlTlxResult,0x00,TLXRESULT);
           sprintf(prlTlxResult.DValue,"%.1s",pclPtr);	     
           sprintf(prlTlxResult.FValue,"%.1s",pclPtr);	     
           sprintf(prlTlxResult.DName,"Suffix");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
           dbg(DEBUG,"%s Flight number suffix <%s>",pclFunc,prlTlxResult.DValue ); 
        }
     }
#endif
  }

  if (ilRC == RC_SUCCESS && 
      (ilRC = FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-ADEP",
                             '\n',&ilLine,&ilCol,&ilPos)) == RC_SUCCESS)
  {
     memset(&prlTlxResult,0x00,TLXRESULT);
     memset(pclResult,0x00,sizeof(pclResult));
     /* point behind \n-ADEP */
     ilPos += 6+rgTlxInfo.TxtStart;
     while(isalnum(pcpData[ilPos]) == 0)
     {
        ilPos++;
     }
     sprintf(pclTable,"APT%s",pcgTABEnd);
     sprintf(prlTlxResult.DValue,"%4.4s",&pcpData[ilPos]);
     ilCount = 1;
     ilRC = syslibSearchDbData(pclTable,"APC4",prlTlxResult.DValue,
                               "APC3",prlTlxResult.FValue,&ilCount,"\n");
     strcpy(prlTlxResult.DName,"Orig");
     dbg(DEBUG,"%s Changed Orig APC4 <%s> to APC3 <%s>",
         pclFunc,prlTlxResult.DValue,prlTlxResult.FValue);
     strcpy(prlTlxResult.DValue,prlTlxResult.FValue);
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
  }

  if (ilRC==RC_SUCCESS &&
      (ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-ADES",
                           '\n',&ilLine,&ilCol,&ilPos)) == RC_SUCCESS)
  {
     memset(&prlTlxResult,0x00,TLXRESULT);
     memset(pclResult,0x00,sizeof(pclResult));
     /* point behind \n-ADEP */
     ilPos += 6+rgTlxInfo.TxtStart;
     while (isalnum(pcpData[ilPos]) == 0)
     {
        ilPos++;
     }
     sprintf(pclTable,"APT%s",pcgTABEnd);
     sprintf(prlTlxResult.DValue,"%4.4s",&pcpData[ilPos]);
     ilCount = 1;
     ilRC = syslibSearchDbData(pclTable,"APC4",prlTlxResult.DValue,
                               "APC3",prlTlxResult.FValue,&ilCount,"\n");
     strcpy(prlTlxResult.DName,"Dest");
     dbg(DEBUG,"%s Changed Dest APC4 <%s> to APC3 <%s>",
         pclFunc,prlTlxResult.DValue,prlTlxResult.FValue);
     strcpy(prlTlxResult.DValue,prlTlxResult.FValue);
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
  }

  if (ilRC==RC_SUCCESS && 
      (ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-EOBD",
                           '\n',&ilLine,&ilCol,&ilPos)) == RC_SUCCESS)
  {
     memset(&prlTlxResult,0x00,TLXRESULT);
     memset(pclResult,0x00,sizeof(pclResult));
     /* point behind \n-ADEP */
     ilPos += 6+rgTlxInfo.TxtStart;
     while(isalnum(pcpData[ilPos]) == 0)
     {
        ilPos++;
     }
     sprintf(prlTlxResult.DValue,"%2.2s",&pcpData[ilPos]);
     strcpy(prlTlxResult.DName,"FYear");
     strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
     dbg(DEBUG,"%s Found FYear <%s>",pclFunc,prlTlxResult.FValue); 
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);

     memset(&prlTlxResult,0x00,TLXRESULT);
     strcpy(prlTlxResult.DName,"FMonth");
     sprintf(prlTlxResult.DValue,"%2.2s",&pcpData[ilPos+2]);
     strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
     dbg(DEBUG,"%s Found FMonth <%s>",pclFunc,prlTlxResult.FValue); 
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
      
     memset(&prlTlxResult,0x00,TLXRESULT);
     strcpy(prlTlxResult.DName,"FDate");
     sprintf(prlTlxResult.DValue,"%2.2s",&pcpData[ilPos+4]);
     strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
     dbg(DEBUG,"%s Found FDate <%s>",pclFunc,prlTlxResult.FValue); 
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
     sprintf(pcgFltDate,"20%6.6s",&pcpData[ilPos]);
     dbg(DEBUG,"%s Found Flight Date <%s>",pclFunc,pcgFltDate); 
  }

  /* First find Taxitime */
  if (ilRC==RC_SUCCESS &&
      (ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-TAXITIME",
                           '\n',&ilLine,&ilCol,&ilPos)) == RC_SUCCESS)
  {
     memset(&prlTlxResult,0x00,TLXRESULT);
     memset(pclResult,0x00,sizeof(pclResult));
     /* point behind \n-ADEP */
     ilPos += 10+rgTlxInfo.TxtStart;
     while (isalnum(pcpData[ilPos]) == 0)
     {
        ilPos++;
     }
     sprintf(pclResult,"%4.4s",&pcpData[ilPos]);
     dbg(DEBUG,"%s Found TaxiTime <%s>",pclFunc,pclResult); 
  }

  if (ilRC == RC_SUCCESS) 
  { 
     switch (ipTyp)
     {
        case FDI_SAM:
           ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-CTOT",
                               '\n',&ilLine,&ilCol,&ilPos); 
           ilPos += 6+rgTlxInfo.TxtStart;
           break;
        case FDI_SRM:
           ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-NEWCTOT",
                               '\n',&ilLine,&ilCol,&ilPos); 
           ilPos += 9+rgTlxInfo.TxtStart;
           break;
        case FDI_SLC:
           /* SLC Tlx has no CTOT*/
           ilExit = TRUE;
           ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\n-EOBT",
                               '\n',&ilLine,&ilCol,&ilPos); 
           ilPos += 6+rgTlxInfo.TxtStart;
           memset(&prlTlxResult,0x00,TLXRESULT);
           prlTlxResult.FValue[0] = ' ';
           prlTlxResult.DValue[0] = ' ';
           strcpy(prlTlxResult.DName,"Airb");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
           memset(&prlTlxResult,0x00,TLXRESULT);
           prlTlxResult.FValue[0] = ' ';
           prlTlxResult.DValue[0] = ' ';
           strcpy(prlTlxResult.DName,"Offbt");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
           break;
     } /* end of switch*/

     if (ilRC == RC_SUCCESS)
     {
        if (ilExit == FALSE)
        {
           memset(&prlTlxResult,0x00,TLXRESULT);
           /* point behind \n-CTOT */
           while (isalnum(pcpData[ilPos]) == 0)
           {
              ilPos++;
           }
           sprintf(prlTlxResult.DValue,"%4.4s",&pcpData[ilPos]);
           dbg(DEBUG,"%s Found AirbTime <%s>",pclFunc,prlTlxResult.DValue); 
           if (Time2Date(prlTlxResult.FValue, prlTlxResult.DValue) == RC_FAIL)
              return RC_FAIL;
           dbg(DEBUG,"%s Changed to <%s>",pclFunc,prlTlxResult.FValue); 
           StrToTime(prlTlxResult.FValue,&tlTime);
           tlNewTime = tlTime - (SECONDS_PER_MINUTE * atoi(pclResult));		
           _tm = (struct tm *) localtime(&tlNewTime);
           rlTm = *_tm;
           memset(pclTimeBuf,0x00,20);
           strftime(pclTimeBuf,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
           strcpy(prlTlxResult.DName,"Airb");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
           strncpy(prlTlxResult.FValue,pclTimeBuf,14);
           dbg(DEBUG,"%s Calculated Offblock <%s>",pclFunc,prlTlxResult.FValue); 
           strcpy(prlTlxResult.DName,"Offbt");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
        }
#if 0
        else
        {
           memset(&prlTlxResult,0x00,TLXRESULT);
           /* point behind \n-CTOT */
           while (isalnum(pcpData[ilPos]) == 0)
           {
              ilPos++;
           }
           sprintf(prlTlxResult.DValue,"%4.4s",&pcpData[ilPos]);
           dbg(DEBUG,"%s Found OffBlock <%s>",pclFunc,prlTlxResult.DValue); 
           if (Time2Date(prlTlxResult.FValue, prlTlxResult.DValue) == RC_FAIL)
              return RC_FAIL;
           dbg(DEBUG,"%s Changed to <%s>",pclFunc,prlTlxResult.FValue); 
           StrToTime(prlTlxResult.FValue,&tlTime);
           tlNewTime = tlTime + (SECONDS_PER_MINUTE * atoi(pclResult));		
           _tm = (struct tm *) localtime(&tlNewTime);
           rlTm = *_tm;
           memset(pclTimeBuf,0x00,20);
           strftime(pclTimeBuf,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
           strcpy(prlTlxResult.DName,"Offbt");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
           strncpy(prlTlxResult.FValue,pclTimeBuf,14);
           dbg(DEBUG,"%s Calculated Airborne <%s>",pclFunc,prlTlxResult.FValue); 
           strcpy(prlTlxResult.DName,"Airb");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
        }
#endif
     } /*  end of if */
  } /*  end of if */

/*
  if (ilRC == RC_SUCCESS)
     SendTlxRes(ipTyp);
  else
     dbg(TRACE,"%s Error in Telex",pclFunc); 
*/
  SendTlxRes(ipTyp);

  return ilRC;
} /* end of HandleSXM*/



