/***********************************************************************
 init_xbshdl.bas
 This file initializes the basic data of xbshdl. 
 Recordsets, which are opened in this script, remain open
 until the hadler is shutdown. Other functions may use 
 basic data via the name that is stored by the underlaying 
 CEDAArray.
 ***********************************************************************/

static void bas_init_xbshdl()
{
	/* this is the path for basic scripts */
	static const char *pclBasPath = "/ceda/bas/";
	int ilSuccess = -1;
	int	ilHandle  = -1;

	/* Free days per month planned */
	ilSuccess = AddXBSCommand("INIT_4","init_4.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_4>!!!");
	}

	/* Free days in quarter */
	ilSuccess = AddXBSCommand("INIT_7","init_7.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_7>!!!");
	}

	/* Free days in year 	*/
	ilSuccess = AddXBSCommand("INIT_10","init_10.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_10>!!!");
	}

	/* working hours normalized	*/
	ilSuccess = AddXBSCommand("INIT_23","init_23.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_23>!!!");
	}

	/* projection holiday hours	*/
	ilSuccess = AddXBSCommand("INIT_30","init_30.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_30>!!!");
	}

	/* init multi accounts in one event */
	ilSuccess = AddXBSCommand("INIT_MUL","init_multiple.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <INIT_MUL>!!!");
	}

	/* add staff loaded in a DutyRoster-view to prefetch arrays */
	ilSuccess = AddXBSCommand("ADDSTF","init_addstf.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <ADDSTF>!!!");
	}

	/* remove staff from prefetch arrays */
	ilSuccess = AddXBSCommand("DELSTF","init_delstf.bbf",pclBasPath);
	if (ilSuccess < 0)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR calling 'AddXBSCommand' for <DELSTF>!!!");
	}

	/* loading all needed basic data tables */
	ilHandle = OpenACTIONRecordset("STFTAB","STFTAB","","URNO,PDGL,PDKL,PMAK,PMAG,LANM,GEBU,PENO,DODM,DOEM","URNO",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","STFTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("SCOTAB","SCOTAB","","URNO,SURN,VPFR,VPTO,CODE,CWEH,KOST","SURN,VPFR",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","SCOTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("COTTAB","COTTAB","","CTRC,WHPW,INBU,URNO","CTRC",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","COTTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("MFMTAB","MFMTAB","","URNO,CTRC,YEAR,MF01,MF02,MF03,MF04,MF05,MF06,MF07,MF08,MF09,MF10,MF11,MF12","CTRC,YEAR",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","MFMTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("NWHTAB","NWHTAB","","URNO,CTRC,YEAR,HR01,HR02,HR03,HR04,HR05,HR06,HR07,HR08,HR09,HR10,HR11,HR12","CTRC,YEAR",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","NWHTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("COHTAB","COHTAB","ORDER BY CTRC,FAGE","URNO,CTRC,FAGE,HOLD","CTRC,FAGE",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","COHTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("PARTAB","PARTAB","","PAID,VALU,URNO","PAID",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_xbshdl(): ERROR when opening action array  <%s>: <%d>!!!","PARTAB",ilHandle);
	}
}
