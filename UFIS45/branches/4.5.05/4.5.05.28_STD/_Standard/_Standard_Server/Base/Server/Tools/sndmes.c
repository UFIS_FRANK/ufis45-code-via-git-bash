#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/sndmes.c 1.2 2004/01/20 00:24:07SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : MCU and GRM                                               */
/* Date           : 28.06.2001                                                */
/* Description    : sends events logged by evthdl on a que                    */
/*                                                                            */
/* Update history : Additionals for get data from new logging format          */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp      */
/*               to avoid core dump by null pointer in dbg                    */
/* 20040116 JIM: using MAIN instead of 'int main(..)' for dbg of mks_version  */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / MCU";
/* be carefule with strftime or similar functions !!! */

#define SNDMSG_DEBUG	/* used for switch to make an output or to send to queue */
						/* if defined there is only a printout */

#include <tsmhdl.h>

char	*mod_name;
char	__CedaLogFile[128];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/

static int		StrToTime(char *pcpTime,time_t *plpTime);
extern int      AddSecondsToCEDATime(char *,long,int);

int debug_level=0;

long lgTimeOffset = 0L;

	  					/* Get these Parameters from */
						/* Config File (strings only)*/
static t_cfgparam rgCfgTable[] = {
	{ "MAIN",	"EVTPRINTFILE",		CFG_STRING,	NULL },        	/* Handshake cycle in seconds (add 'ok') */
/* End of Records - hands off */
	{ NULL,		NULL,			0,		NULL }
} ;
	
char	*pcgFilename;
int		ignum;

#define ARRAY_SIZE	8192

int TMIGetField(FILE *prpFile, char *pcpOut,int ipMaxLen);
static int 	Init_TMI();



/* 20040116 JIM:  need MAIN instead of "int main(int argc,char **argv)" */
MAIN
{
	int     ilRC			= RC_SUCCESS;
	int		ilDest			= 0;
	int		ilLineNo		= 0;
	long    llLength		= 0;
	long	ilcnt			= 0; 
	int		iloriginator,ilroute,ilfunction,ilpriority,ilmsg_length;
	int		ilMark = RC_SUCCESS;
	EVENT   *prlEvent		= NULL ;
	BC_HEAD rlBcHead;
	CMDBLK  rlCmd;
	FILE	*prlFile		= NULL;
	char    *pclSelection	= NULL;
	char    *pclFields		= NULL;
	char    *pclData			= NULL;
	char	*pclMod ;
	char	clBuffer[ARRAY_SIZE] = "";
	char	clTmp[ARRAY_SIZE] = "";
	char	clDummy[20] = "";
	char	*pclHlp = NULL;
	char	clTime[32] = "";
	char	clEventTime[32] = "";
	char		aclAbsoluteFilename[512];
	time_t tlNow;
	time_t tlEventTime;

		if ( argc < 2 ) 
		{
			printf("Usage: sendevt filename [TimeOffset]\n");
			printf("       sendevt rulfndo00001.xyz\n");
			sleep (5);
			exit;
		}

    mod_name = argv[0];
    /* if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name,'/')!= NULL)
    {
       mod_name= strrchr(mod_name,'/');
       mod_name++;
    }
    sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());

    printf("sndmes: vor initque file = %s\n",argv[1]);
		ilRC = init_que();
		if(ilRC == RC_SUCCESS)
		{
			if (argc > 2)
			{
				lgTimeOffset = atol(argv[2]);
			}
			else
			{
				lgTimeOffset = 0L;
			}
			printf("sndmes: nach initque RC=OK tioffs = %d\n",lgTimeOffset);
		}
		else
		{
			printf("sndmes: init_que() failed <%d>\n",ilRC);
			fflush(stdout);
      /* 20040116 JIM: MAIN is not defined as 'int main', so use of exit 
         instead of return */
			exit (1);
/*  	return(1); */
		}

		prlFile = fopen(argv[1],"r");
		if(prlFile == NULL)
		{
			printf("sndmes: fopen <%s> failed.\n",argv[1]);
			ilRC = RC_FAIL;
		}/* end of if */
		

		if(ilRC == RC_SUCCESS)
		{
			printf("sndmes: fopen <%s> ok.\n",argv[1]);
			fflush(stdout);
			ilcnt = 0;
			while(fgetc(prlFile) != EOF)
			{			
				/*skipped record start*/

				ilcnt++;

				ilRC = TMIGetField(prlFile,clTime,15);
#ifdef SNDMSG_DEBUG
				printf("sndmes: time %15s\n",clTime);
				fflush(stdout);
#endif
				if (lgTimeOffset != 0)
				{
						strcpy(clEventTime,clTime);
						AddSecondsToCEDATime(clEventTime,(time_t)lgTimeOffset,1);
						StrToTime(clEventTime,&tlEventTime);
						tlNow = time(NULL);	
						printf("sndmes: tlNow = %d tlEventTime = %d StrToTime = %s\n",
							tlNow,tlEventTime,clEventTime);
						if(tlNow < tlEventTime)
						{
#ifdef SNDMSG_DEBUG
							printf("sndmes: Event created <%s> waiting %ld seconds for <%s>",
								clTime,tlEventTime - tlNow,clEventTime);
							fflush(stdout);
#endif
							sleep(tlEventTime - tlNow);
						}
				}
				ilRC = TMIGetField(prlFile,clTmp,10);
				iloriginator = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,10);
				ilroute = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,10);
				ilfunction = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,10);
				ilpriority = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,10);
				ilmsg_length = atoi(clTmp);
#ifdef SNDMSG_DEBUG
				printf("sndmes: origin %d route %d funt %d prio %d imsg_len %d\n",
					iloriginator,ilroute,ilfunction,ilpriority,ilmsg_length);
				fflush(stdout);
#endif
				ilRC = TMIGetField(prlFile,rlBcHead.dest_name,10);
				ilRC = TMIGetField(prlFile,rlBcHead.orig_name,10);
				ilRC = TMIGetField(prlFile,rlBcHead.recv_name,10);
				ilRC = TMIGetField(prlFile,clTmp,10); /* bc_num ist not used */
													/* set to -1 to disable logging once more!*/ 
				rlBcHead.bc_num = atoi(clTmp);		/* wird aber �berb�gelt*/
				rlBcHead.bc_num = -1;
				ilRC = TMIGetField(prlFile,rlBcHead.seq_id,10);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.tot_buf = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.act_buf = atoi(clTmp);
				ilRC = TMIGetField(prlFile,rlBcHead.ref_seq_id,10);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.rc = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.tot_size = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.cmd_size = atoi(clTmp);
				ilRC = TMIGetField(prlFile,clTmp,6);
				rlBcHead.data_size = atoi(clTmp);
#ifdef SNDMSG_DEBUG
				printf("sndmes: tot buf %d act buf %d rc %d prio %d tot siz %d cmd siz %d data siz %d\n",
					rlBcHead.tot_buf,rlBcHead.act_buf,rlBcHead.rc,rlBcHead.tot_size,rlBcHead.cmd_size,rlBcHead.data_size);
				fflush(stdout);
#endif
				/* command block */
				ilRC = TMIGetField(prlFile,rlCmd.command,6);
				ilRC = TMIGetField(prlFile,rlCmd.obj_name,33);
				ilRC = TMIGetField(prlFile,rlCmd.order,2);
				ilRC = TMIGetField(prlFile,rlCmd.tw_start,33);
				ilRC = TMIGetField(prlFile,rlCmd.tw_end,33);
#ifdef SNDMSG_DEBUG
				printf("sndmes: cmd %6s objnam %33s order %2s,start %33s,end %33s\n", 
					rlCmd.command,rlCmd.obj_name,rlCmd.order,rlCmd.tw_start,rlCmd.tw_end);
				fflush(stdout);
#endif				/* data block */
				ilRC = TMIGetField(prlFile,clBuffer,ARRAY_SIZE-1);
				pclSelection = (char *) malloc (strlen(clBuffer) + 1);
				if(pclSelection != NULL)
				{
					strcpy(pclSelection,clBuffer);
				}
				else
				{
					printf("sndmes: malloc for selection failed!");
				}
				ilRC = TMIGetField(prlFile,clBuffer,ARRAY_SIZE-1);
				pclFields = (char *) malloc (strlen(clBuffer) + 1);
				if(pclFields != NULL)
				{
					strcpy(pclFields,clBuffer);
				}
				else
				{
					printf("sndmes: malloc for fields failed!");
				}
				ilRC = TMIGetField(prlFile,clBuffer,ARRAY_SIZE-1);
				pclData = (char *) malloc (strlen(clBuffer) + 1);
				if(pclData != NULL)
				{
					strcpy(pclData,clBuffer);
				}
				else
				{
					printf("sndmes: malloc for data failed!");
				}

				/* if read last */
				if(ilRC != RC_SUCCESS)
				{
					ilMark = RC_FAIL;
					/*ilRC = RC_SUCCESS;*/
				}
				if(ilRC == RC_SUCCESS)
				{
					/*send it! */
					llLength = strlen(pclFields) + strlen(pclData) + 
						strlen(pclSelection) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
						sizeof(EVENT) + 200;
					prlEvent = (EVENT *) malloc (llLength);
					if (prlEvent == NULL)
					{
						 ilRC= RC_FAIL;
						 printf("sndmes: Malloc %d for EVENT failed.\n", llLength);
					}/* end if malloc fail */
					else
					{	
						/* copy all into event */
						memset (((char *) prlEvent), 0, llLength); 
						prlEvent->originator = iloriginator;
						prlEvent->data_length =  llLength;
						prlEvent->command = EVENT_DATA;
						prlEvent->data_offset = sizeof(EVENT);
						/*pclHlp = (char *)prlEvent + sizeof(EVENT);*/
						pclHlp = (char *)prlEvent;
						pclHlp +=  sizeof(EVENT);
						memmove(pclHlp, &rlBcHead, sizeof(BC_HEAD));
						pclHlp = ((BC_HEAD *)pclHlp)->data;
						memmove(pclHlp, &rlCmd, sizeof(CMDBLK));
						pclHlp = ((CMDBLK  *) pclHlp)->data;
						memmove(pclHlp, pclSelection, strlen(pclSelection));
						pclHlp += strlen(pclSelection);
						*pclHlp = '\0';
						pclHlp++;
						memmove(pclHlp, pclFields, strlen(pclFields));
						pclHlp += strlen(pclFields);
						*pclHlp = '\0';
						pclHlp++;
						memmove(pclHlp, pclData, strlen(pclData));
						pclHlp += strlen(pclData);
						*pclHlp = '\0';
						
						{

							BC_HEAD *prlBchead       = NULL;
							CMDBLK  *prlCmdblk       = NULL;
							char    *pclSelection    = NULL;
							char    *pclFields       = NULL;
							char    *pclData         = NULL;
							char    *pclTmp          = NULL;
							char    *pclRow          = NULL;

							prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
							prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
							pclSelection = prlCmdblk->data;
							pclFields    = pclSelection + strlen(pclSelection) + 1;
							pclData      = pclFields + strlen(pclFields) + 1;
#ifdef SNDMSG_DEBUG

							printf("sndmes: %s line %d\n",argv[1],ilLineNo++);
							fflush(stdout);
#endif
							/*	core dump GRM ???? ist nicht von mir
							DebugPrintEvent(TRACE,prlEvent);
							DebugPrintBchead(TRACE,prlBchead);
							DebugPrintCmdblk(TRACE,prlCmdblk); 
							*/

#ifdef SNDMSG_DEBUG
							printf("sndmes: Selection: <%8s>\n",pclSelection);
							printf("sndmes: Fields: <%80s>\n",pclFields);
							printf("sndmes: Data: <%80s>\n",pclData);
							fflush(stdout);
#endif
						}

#ifndef SNDMSG_DEBUG
						ilRC = que( ilfunction, ilroute,  iloriginator,
								ilpriority, ilmsg_length, (char *) prlEvent);

						if(ilRC != RC_SUCCESS)
						{
							printf("sndmes: que(QUE_PUT,%d,%d,%d,%d,%x) failed <%d>\n",ilDest,iloriginator,PRIORITY_3,llLength,prlEvent,ilRC);
						}
						else
						{
							printf("sndmes: que(QUE_PUT,%d,%d,%d,%d,%x) ok\n",ilroute,iloriginator,ilpriority,llLength,prlEvent);
						}
#endif
					}
				}/* end of sending */
				free(pclSelection);
				free(pclFields);
				free(pclData);
				free(prlEvent);
				fgetc(prlFile);/*skip \n */
			}/* end of while */
		}/* end of if */
		fclose(prlFile);

} /* end of main */

static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];
	long llUtcDiff;

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */


	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
	{
		time_t utc,local;

	_tm = (struct tm *)gmtime(&now);
	utc = mktime(_tm);
	_tm = (struct tm *)localtime(&now);
	local = mktime(_tm);
	/*dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
		utc,local,local-utc);
		llUtcDiff = local-utc;*/
	}
		/*now +=  + llUtcDiff;*/
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}

int TMIGetField(FILE *prpFile, char *pcpOut, int ipMaxLen)
{
	int ilRC = RC_SUCCESS;
	int	i = 0;
	char	c;

	pcpOut[0] = '\0';

	while(((c = fgetc(prpFile) )!= EOF) && 
				(c != FIELDDELIMITER) && (c != RECORDEND) && i < ipMaxLen)
	{
		pcpOut[i++] = c;
	}
	pcpOut[i] = '\0';
	/*dbg(DEBUG,"GetField: Data:<%s>\n",pcpOut);*/

	return ilRC;
}
