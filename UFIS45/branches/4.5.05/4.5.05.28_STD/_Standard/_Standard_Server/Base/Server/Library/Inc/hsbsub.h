#ifndef _DEF_mks_version_hsbsub_h
  #define _DEF_mks_version_hsbsub_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_hsbsub_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/hsbsub.h 1.2 2004/07/27 16:47:51SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/* ********************************************************************** */

#ifndef _HSBSUB
#define _HSBSUB



#define	HSB_RPC_ERROR	-1
#define STS_LEN 20
#define SYSSTR_LEN 128

#define HSB_PUTQUE			0x14046799
#define HSB_STARTSCRIPT		0x14046798
#define HSB_GETSYSTEMSTATE	0x14046797
#define HSB_SETSYSTEMSTATE	0x14046796

#define APP_OFFLINE	0
#define APP_ONLINE	1

int HsbPutque(char *pcpHost, ITEM *prpItem);
int HsbStartScript(char *pcpHost, char *pcpScript);
int HsbGetSystemState(char *pcpHost, int *pipState);
int HsbSetSystemState(char *pcpHost, int ipState);

int SendRemoteShutdown(int ipModId);
int RemoteQue(int ipFkt,int ipRoute,int ipOrig,int ipPrio,int ipLen, char *pcpData);
int TransferFile(char *pcpFileName);
int SetApplicationState(int ipNewState);
int GetApplicationState(void);
int GetPreviousState(void);
int get_system_state(void);
int set_system_state(int state);
int sts2status(int *sts,char *status);
int status2sts(char *status ,int *sts);
#endif
