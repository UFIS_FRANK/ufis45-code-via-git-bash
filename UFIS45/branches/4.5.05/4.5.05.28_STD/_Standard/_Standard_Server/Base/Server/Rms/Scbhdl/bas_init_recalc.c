/* static int  GetItem(char *pcpSource,int ipIndex,const char *pcpDelimiter,char *pcpItem) */
int  GetItem(char *pcpSource,int ipIndex,const char *pcpDelimiter,char *pcpItem)

{
	char *pclBuffer;
	int ilIndex = -1;
	char *pclToken;

	pclBuffer = (char*)calloc(1,strlen(pcpSource)+2);
	strcpy(pclBuffer,pcpSource);
	pclToken = strtok(pclBuffer,pcpDelimiter);
	while(pclToken)
	{
		++ilIndex;
		if (ilIndex == ipIndex)
		{
			strcpy(pcpItem,pclToken);
			free (pclBuffer);
			return TRUE;
		}

		pclToken = strtok(NULL,pcpDelimiter);
	}

	strcpy(pcpItem,"");
	free (pclBuffer);
	return FALSE;
}

static void ClearFUMO(char *pcpFumo,const char *pcpMonth)
{
	int ilCounter;
	if (strlen(pcpFumo) >= atoi(pcpMonth))
	{
		pcpFumo[atoi(pcpMonth)-1] = '\0';
		for (ilCounter = atoi(pcpMonth); ilCounter <= 12; ilCounter++)
		{
			strcat(pcpFumo,"0");
		}
	}
	else
	{
		strcpy(pcpFumo,"000000000000");
	}
}

/**************************************************************************
 **************************************************************************
 Setzt Kontowerte zur�ck
 **************************************************************************
 **************************************************************************/
static BOOL ClearAccount(const char *pcpStfu,const char *pcpYear,const char *pcpMonth,const char *pcpAccountType,const char *pcpUser,FILE *fp, int ipAccHandle)
{
	static const char *pclAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12";

	char	clAccRecord[1024];
	int		ilCounter;
	Date	olDate;
	char	clTimeNowString[16];
	char	clFumo[16];
	int		ilLen;
	char	clUpdateFieldList[1024];
	char	clUpdateValueList[1024];
	int 	ilStartMonth;

	/* get current time */
	olDate = GetCurrentDate();
	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);
	ilStartMonth = atoi(pcpMonth);

	/* initialize data - put key value in record buffer! (unbelievable) */
	strcpy(clAccRecord,pcpYear);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpStfu);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpAccountType);

	if (ipAccHandle >= 0)
	{
		/* get first record that matches condition */
		ilLen = FindFirstRecordByIndex(ipAccHandle,clAccRecord);
		if (ilLen > 0)
		{
			/* delete values, step through every month's open, close and correction values */
			strcpy(clUpdateFieldList,"");
			strcpy(clUpdateValueList,"");

			for (ilCounter = 1; ilCounter <= 12; ilCounter++)
			{
				char clLocalMonth[4];
				sprintf(clLocalMonth,"%02d",ilCounter);

				/* generate field names and field values */
				if (ilCounter >= ilStartMonth)
				{
					/* in the first month OP01 is untouchable */
					if (ilCounter == 1)
					{
						strcat(clUpdateFieldList,",CO");
						strcat(clUpdateFieldList,clLocalMonth);
						strcat(clUpdateFieldList,",CL");
						strcat(clUpdateFieldList,clLocalMonth);
						strcat(clUpdateValueList,",0,0");
					}
					else
					{
						strcat(clUpdateFieldList,",OP");
						strcat(clUpdateFieldList,clLocalMonth);
						strcat(clUpdateFieldList,",CO");
						strcat(clUpdateFieldList,clLocalMonth);
						strcat(clUpdateFieldList,",CL");
						strcat(clUpdateFieldList,clLocalMonth);
						strcat(clUpdateValueList,",0,0,0");
					}
				}
			}

			GetFieldValueFromDataString("FUMO",(char *)pclAccFields,clAccRecord,clFumo);
			trim(clFumo);
			ClearFUMO(clFumo,pcpMonth);

			strcat(clUpdateFieldList,",FUMO,USEU,LSTU");
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clFumo);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,pcpUser);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clTimeNowString);

			/* set USEU und COxy */
			fprintf(fp,"ClearAccount():Stfu<%s> Year<%s> Month<%s> Type<%s> User<%s>\n",pcpStfu,pcpYear,pcpMonth,pcpAccountType,pcpUser);

			if (UpdateRecord(ipAccHandle,&clUpdateFieldList[1],&clUpdateValueList[1]) != 0)
			{
				fprintf(fp,"ClearAccount(): ERROR actualizing record!\n");
				return FALSE;
			}
		}
		else
		{
			fprintf(fp,"ClearAccount(): No record found for Stfu<%s> Year<%s> Month<%s> Type<%s>\n",pcpStfu,pcpYear,pcpMonth,pcpAccountType);
		}
	}
	else
	{
		/* invalid ACC handle */
		fprintf(fp,"ClearAccount(): No valid ACC-handle parameter <ipAccHandle> = <%d>!\n",ipAccHandle);
		return FALSE;
	}

	return TRUE;
}

static BOOL ClearAccountReversible(const char *pcpStfu,const char *pcpYear,const char *pcpMonth,const char *pcpAccountType,const char *pcpUser,FILE *fp, int ipAccHandle)
{
	static const char *pclAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12";
	char	clAccRecord[1024];
	int		ilCounter;
	Date	olDate;
	char	clTimeNowString[16];
	char	clFumo[16];
	int		ilLen;
	char	clYela[128];
	char	clYecu[128];
	char	clClearValue[128];
	char	clClValue[128];
	char	clUpdateFieldList[1024];
	char	clUpdateValueList[1024];
	int		ilStartMonth;
	double	dlClearValue;

	/* get actual time */
	olDate = GetCurrentDate();
	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);
	ilStartMonth = atoi(pcpMonth);

	/* initialize data */
	strcpy(clAccRecord,pcpYear);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpStfu);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpAccountType);

	if (ipAccHandle >= 0)
	{
		/* a) falls Neurechnung ab Januar: alle OP- und CL Felder m�ssen mit (YECU + YELA) initialisiert werden	*/
		/* b) sonst: alle OP- und CL Felder m�ssen mit CL-Wert des letzten Monats initialisiert werden	*/
		ilLen = FindFirstRecordByIndex(ipAccHandle,clAccRecord);
		if (ilLen > 0)
		{
			if (ilStartMonth == 1)
			{
				/* we have to take last year's balance und current year */
				GetFieldValueFromDataString("YELA",(char *)pclAccFields,clAccRecord,clYela);
				trim(clYela);
				if (strlen(clYela) == 0)
				{
					strcpy(clYela,"0");
				}

				GetFieldValueFromDataString("YECU",(char *)pclAccFields,clAccRecord,clYecu);
				trim(clYecu);
				if (strlen(clYecu) == 0)
				{
					strcpy(clYecu,"0");
				}

				dlClearValue = atof(clYela) + atof(clYecu);
				sprintf(clClearValue,"%-8.2lf",dlClearValue);
				clClearValue[8] = '\0';
				fprintf(fp,"ClearAccountReversible(): YEarLAst = <%s>, YEarCUrrent = <%s>\n",clYela,clYecu);
			}
			else
			{
				sprintf(clClValue,"CL%02d",ilStartMonth-1);
				GetFieldValueFromDataString(clClValue,(char *)pclAccFields,clAccRecord,clClearValue);
				trim(clClearValue);
				if (strlen(clClearValue) == 0)
				{
					strcpy(clClearValue,"0");
				}
				fprintf(fp, "ClearAccountReversible(): old CL-value = <%s>, new CL-value = <%s>\n",clClValue,clClearValue);
			}

			/* delete values, step through every month's open, close and correction values */
			strcpy(clUpdateFieldList,"");
			strcpy(clUpdateValueList,"");

			for (ilCounter = ilStartMonth; ilCounter <= 12; ilCounter++)
			{
				char clLocalMonth[4];
				sprintf(clLocalMonth,"%02d",ilCounter);

				strcat(clUpdateFieldList,",OP");
				strcat(clUpdateFieldList,clLocalMonth);
				strcat(clUpdateFieldList,",CO");
				strcat(clUpdateFieldList,clLocalMonth);
				strcat(clUpdateFieldList,",CL");
				strcat(clUpdateFieldList,clLocalMonth);

				strcat(clUpdateValueList,",");
				strcat(clUpdateValueList,clClearValue);
				strcat(clUpdateValueList,",0,");
				strcat(clUpdateValueList,clClearValue);
			}

			GetFieldValueFromDataString("FUMO",(char *)pclAccFields,clAccRecord,clFumo);
			trim(clFumo);
			ClearFUMO(clFumo,pcpMonth);

			strcat(clUpdateFieldList,",FUMO,USEU,LSTU");
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clFumo);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,pcpUser);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clTimeNowString);

			fprintf(fp,"ClearAccountReversible():Stfu<%s> Year<%s> Month<%s> Type<%s> User<%s>\n",pcpStfu,pcpYear,pcpMonth,pcpAccountType,pcpUser);

			if (UpdateRecord(ipAccHandle,&clUpdateFieldList[1],&clUpdateValueList[1]) != 0)
			{
				fprintf(fp,"ClearAccountReversible(): ERROR actualizing record!\n");
				return FALSE;
			}
		}
	}
	else
	{
		/* invalid ACC handle */
		fprintf(fp,"ClearAccountReversible(): No valid ACC-handle parameter <ipAccHandle> = <%d>!\n",ipAccHandle);
		return FALSE;
	}

	return TRUE;
}

static BOOL ClearAccountReversibleQuarter(const char *pcpStfu,const char *pcpYear,const char *pcpMonth,const char *pcpAccountType,const char *pcpUser,FILE *fp, int ipAccHandle)
{
	static const char *pclAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12";
	char	clAccRecord[1024];
	int		ilCounter;
	Date	olDate;
	char	clTimeNowString[16];
	char	clFumo[128];
	int		ilLen;
	char	clYela[128];
	char	clYecu[128];
	char	clClearValue[128];
	char	clUpdateFieldList[1024];
	char	clUpdateValueList[1024];
	int		ilStartMonth;
	char	clLocalMonth[4];

	/* get actual time */
	olDate = GetCurrentDate();
	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);
	ilStartMonth = atoi(pcpMonth);

	/* initialize data */
	strcpy(clAccRecord,pcpYear);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpStfu);
	strcat(clAccRecord,",");
	strcat(clAccRecord,pcpAccountType);

	if (ipAccHandle >= 0)
	{
		/* a) falls Neurechnung ab Quartal-Start: alle OP- und CL Felder m�ssen mit 0 initialisiert werden	*/
		/* b) sonst: alle OP- und CL Felder des restlichen Quartals m�ssen mit CL-Wert des letzten Monats 	*/
		/*    initialisiert werden, alle weiteren Quartale mit 0	*/

		ilLen = FindFirstRecordByIndex(ipAccHandle,clAccRecord);

		if (ilLen > 0)
		{
			switch(ilStartMonth)
			{
			case 1:
			case 4:
			case 7:
			case 10:
				strcpy(clClearValue,"0");
				fprintf(fp,"ClearAccountReversibleQuarter(): Start of quarter ==> ClearValue = <%s>\n",clClearValue);
			break;
			default:
				{
					char clClValue[128];
					sprintf(clClValue,"CL%02d",ilStartMonth-1);
					GetFieldValueFromDataString(clClValue,(char *)pclAccFields,clAccRecord,clClearValue);
					trim(clClearValue);
					if (strlen(clClearValue) == 0)
					{
						strcpy(clClearValue,"0");
					}
					fprintf(fp,"ClearAccountReversibleQuarter(): old CL-value = <%s>, new CL-value = <%s>\n",clClValue,clClearValue);
				}
			}

			/* delete values, step through every month's open, close and correction values */
			strcpy(clUpdateFieldList,"");
			strcpy(clUpdateValueList,"");

			for (ilCounter = ilStartMonth; ilCounter <= 12; ilCounter++)
			{
				switch (ilCounter)
				{
					case 1:
					case 4:
					case 7:
					case 10:
						strcpy(clClearValue,"0");
					break;
				}

				sprintf(clLocalMonth,"%02d",ilCounter);

				strcat(clUpdateFieldList,",OP");
				strcat(clUpdateFieldList,clLocalMonth);
				strcat(clUpdateFieldList,",CO");
				strcat(clUpdateFieldList,clLocalMonth);
				strcat(clUpdateFieldList,",CL");
				strcat(clUpdateFieldList,clLocalMonth);

				strcat(clUpdateValueList,",");
				strcat(clUpdateValueList,clClearValue);
				strcat(clUpdateValueList,",0,");
				strcat(clUpdateValueList,clClearValue);
			}

			GetFieldValueFromDataString("FUMO",(char *)pclAccFields,clAccRecord,clFumo);
			trim(clFumo);
			ClearFUMO(clFumo,pcpMonth);

			strcat(clUpdateFieldList,",FUMO,USEU,LSTU");
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clFumo);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,pcpUser);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clTimeNowString);

			fprintf(fp,"ClearAccountReversibleQuarter():Stfu<%s> Year<%s> Month<%s> Type<%s> User<%s>\n",pcpStfu,pcpYear,pcpMonth,pcpAccountType,pcpUser);

			if (UpdateRecord(ipAccHandle,&clUpdateFieldList[1],&clUpdateValueList[1]) != 0)
			{
				fprintf(fp,"ClearAccountReversibleQuarter(): ERROR actualizing record!\n");
				return FALSE;
			}
		}
	}
	else
	{
		/* invalid ACC handle */
		fprintf(fp,"ClearAccountReversibleQuarter(): No valid ACC-handle parameter <ipAccHandle> = <%d>!\n",ipAccHandle);
		return FALSE;
	}
	return TRUE;
}

/**************************************************************************
 **************************************************************************
 OpenDRRRecordsetLocal: liest die Datensaetze der Tabelle DRRTAB in einen
 Recordset, der ge�ffnet bleibt.
 R�ckgabe: 0 oder Fehlercode
 **************************************************************************/

static int OpenDRRRecordsetLocal(const char *pcpStfu,const char *pcpStartSday,const char *pcpEndSday,FILE *fp)
{
	static const char *pclDrrFields = "URNO,AVFR,AVTO,BSDU,DRRN,ROSL,ROSS,SBFR,SBTO,SDAY,STFU,HOPO,SCOD,SBLU,USEU,USEC,SCOO,DRS1,DRS2,DRS3,DRS4,BROS";

	int 	ilDrrHandle;
	char	clDrrWhere[256];

	fprintf(fp,"opening DRRHandle...\n");

	strcpy(clDrrWhere,"where STFU='");
	strcat(clDrrWhere,pcpStfu);
	strcat(clDrrWhere,"' and ROSS = 'A' and ( SDAY >='");
	strcat(clDrrWhere,pcpStartSday);
	strcat(clDrrWhere,"' and SDAY <='");
	strcat(clDrrWhere,pcpEndSday);
	strcat(clDrrWhere,"') order by SDAY DESC");

	fprintf(fp,"drrWhere <",clDrrWhere,">!\n");

	ilDrrHandle = OpenRecordset("RECDRR","DRRTAB",clDrrWhere,pclDrrFields,"",0,1);

	if (ilDrrHandle <= -1)
	{
		fprintf(fp,"ERROR opening action array <%s>, <ilDrrHandle> = <%d>","DRRTAB",ilDrrHandle);
		return 0;
	}
	else
	{
		fprintf(fp,"Succeeded in opening action array <%s>","DRRTAB");
		return GetRecordCount(ilDrrHandle);
	}
}

/********************************************************************************************
 ********************************************************************************************
 ****** Einlesen aller DRR Daten
 ********************************************************************************************
 ********************************************************************************************/

static BOOL ReadDrrData(const char *pcpCurrentEmpl,const char *pcpFrom,const char *pcpTo,FILE *fp)
{
	static const char *pclFieldList = "URNO,AVFR,AVTO,BSDU,DRRN,ROSL,ROSS,SBFR,SBTO,SDAY,STFU,HOPO,SCOD,SBLU,USEU,USEC,SCOO,DRS1,DRS2,DRS3,DRS4";

	int 	ilDrrCount;
	int 	ilDrrHandle = -1;
	char	clDrrRecord[400];
	int		ilCount;
	char	clField[256];
	char	clData[400];
	char	clBsdu[12];
	char	clSday[16];
	char	clScod[10];

	/* read shift data */
	ilDrrCount = OpenDRRRecordsetLocal(pcpCurrentEmpl,pcpFrom,pcpTo,fp);
	ilDrrHandle = GetArrayIndex("RECDRR");

	fprintf(fp,"Found <%d> DRR record(s)\n",ilDrrCount);
	fprintf(fp,"Employee URNO = <%s>\n",pcpCurrentEmpl);
	fprintf(fp,"From <pcpFrom> = <%s>\n",pcpFrom);
	fprintf(fp,"To   <pcpTo>   = <%s>\n",pcpTo);

	/* Alle gefundenen DRRs durchgehen */
	if (ilDrrCount > 0 && ilDrrHandle > -1)
	{
		/***** alle DRR Recordsets holen und bearbeiten	***/
		GetFirstRecord(ilDrrHandle,clDrrRecord);

		ilCount = 0;
		do
		{
			++ilCount;

			if (strlen(clDrrRecord) > 0)
			{
				GetFieldValue(ilDrrHandle,"BSDU",clBsdu);

				/*** Pr�fen ob DRR nicht leer ist ***/
				if (strcmp(clBsdu,"0") != 0)
				{
					/*** Datenliste erstellen ***/
					GetFieldValue(ilDrrHandle,"URNO",clData);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"AVFR",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"AVTO",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"BSDU",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"DRRN",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"ROSL",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"ROSS",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SBFR",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SBTO",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SDAY",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"STFU",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"HOPO",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SCOD",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SBLU",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"USEU",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"USEC",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"SCOO",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"DRS1",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"DRS2",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"DRS3",clField);
					strcat(clData,clField);
					strcat(clData,","); 
					GetFieldValue(ilDrrHandle,"DRS4",clField);
					strcat(clData,clField);

					AddEventToFile("IRT","DRRTAB,","",(char *)pclFieldList,clData,"");

					GetFieldValue(ilDrrHandle,"SDAY",clSday);
					GetFieldValue(ilDrrHandle,"SCOD",clScod);
	 				fprintf(fp,"Sday: <%s> Bsdu <%s> Scod <>\n",clSday,clBsdu,clScod);
				}
			}

			GetNextRecord(ilDrrHandle,clDrrRecord);
		}
		while(ilCount < ilDrrCount);
	}

	/* close handle */
	if (ilDrrHandle > -1)
	{
 		fprintf(fp,"ReadDrrData(): DRR-handle will be closed.\n");
		CloseRecordset(ilDrrHandle,0);
	}

	return TRUE;
}

/**************************************************************************
 **************************************************************************
 OpenDRARecordset: liest die Datensaetze der Tabelle DRATAB in einen
  Action Recordset, der ge�ffnet bleibt.
 R�ckgabe: 0 oder Fehlercode
 **************************************************************************/

static int OpenDRARecordsetLocal(const char *pcpStfu,const char *pcpStartSday,const char *pcpEndSday,FILE *fp)
{
	static const char *pclDraFields = "ABFR,ABTO,DRRN,HOPO,SDAC,SDAY,STFU,DRS1,DRS3,DRS4,BSDU";

	int 	ilDraHandle;
	char	clDraWhere[256];

	fprintf(fp,"opening DRAHandle...\n");

	strcpy(clDraWhere,"where STFU='");
	strcat(clDraWhere,pcpStfu);
	strcat(clDraWhere,"' and SDAY >= '");
	strcat(clDraWhere,pcpStartSday);
	strcat(clDraWhere,"' and SDAY <='");
	strcat(clDraWhere,pcpEndSday);
	strcat(clDraWhere,"' order by SDAY");

	fprintf(fp,"draWhere <",clDraWhere,">!\n");

	ilDraHandle = OpenRecordset("DRAREC","DRATAB",clDraWhere,pclDraFields,"",0,1);

	if (ilDraHandle <= -1)
	{
		fprintf(fp,"ERROR opening action array <%s>, <ilDraHandle> = <%d>","DRATAB",ilDraHandle);
		return 0;
	}
	else
	{
		fprintf(fp,"Succeeded in opening action array <%s>","DRATAB");
		return GetRecordCount(ilDraHandle);
	}
}

/********************************************************************************************
 ********************************************************************************************
 ****** Einlesen aller DRA Daten
 ********************************************************************************************
 ********************************************************************************************/

static BOOL ReadDraData(const char *pcpCurrentEmpl,const char *pcpFrom,const char *pcpTo,FILE *fp)
{
	static const char *pclFieldList = "ABFR,ABTO,DRRN,HOPO,SDAC,SDAY,STFU,DRS1,DRS3,DRS4,BSDU";
	int 	ilDraCount;
	int		ilDraHandle;
	char	clDraRecord[100];
	int		ilCount;
	char	clField[100];
	char	clData[100];

	/* read DRA records */
	ilDraCount = OpenDRARecordsetLocal(pcpCurrentEmpl,pcpFrom,pcpTo,fp);
	ilDraHandle = GetArrayIndex("DRAREC");
	fprintf(fp,"Found %d DRA records\n",ilDraCount);

	if (ilDraCount > 0 && ilDraHandle > -1)
	{
		GetFirstRecord(ilDraHandle,clDraRecord);

		ilCount = 0;

		do
		{
			++ilCount;

			if (strlen(clDraRecord) > 0)
			{
				/*** Datenliste erstellen ***/
				GetFieldValue(ilDraHandle,"ABFR",clField);
				strcpy(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"ABTO",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"DRRN",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"HOPO",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"SDAC",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"SDAY",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"STFU",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"DRS1",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"DRS3",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"DRS4",clField);
				strcat(clData,clField);
				strcat(clData,",");
				GetFieldValue(ilDraHandle,"BSDU",clField);
				strcat(clData,clField);

				AddEventToFile("IRT","DRATAB,","",(char *)pclFieldList,clData,"");

				fprintf(fp,"varFieldList <%s>\n",pclFieldList);
	 			fprintf(fp,"Data: <%s>\n",clData);
	 		}

			GetNextRecord(ilDraHandle,clDraRecord);
		}
		while(ilCount < ilDraCount);
	}

	if (ilDraHandle > -1)
	{
 		fprintf(fp,"DraHandle wird geschlossen!\n");
		CloseRecordset(ilDraHandle,0);
	}

	return TRUE;
}




/**************************************************************************
 ****** ADO 11.09.01
 ****** THO 21.12.01
 ****** THO 17.01.02	"RECALC_BC" pseudo command added to send BC
 **************************************************************************/
static void bas_init_recalc()
{

	char	clMaxUrnoSize[12];
	int 	ilMaxUrnoSize;
	char	clRecvName[128];
	char	clDirectory[128];
	char	clFileName[256];
	Date	olDate;
	char	clTimeNowString[128];
	int		ilCountEmpl;
	FILE*	fp;
	char	clFrom[16];
	char	clTo[16];
	char	clYearFrom[6];
	char	clMonFrom[4];
	char	clDayFrom[4];
	int		ilStfuAdd = 0;
	char	clStfuList[2048];
	int		ilEmpCounter;
	char	clCurrentEmpl[128];
	char	clYearTo[6];
	int		ilErrors = 0;
	int		ilAccHandle;
	char	*pclBuff;
	char	*pclStaffUrnos;
	char	clDel = '-';

	
	if (ReadTemporaryField("MaxUrnoSize",clMaxUrnoSize) < 0)
	{
		ilMaxUrnoSize = 75;
	}
	else
	{
		ilMaxUrnoSize = atoi(clMaxUrnoSize);
	}

	/* broadcast receive name */
	ReadTemporaryField("RecvName",clRecvName);

	dbg(DEBUG,"============================= SCRIPT INIT_RECALC START =============================");

	/* path and name of ASCII file to be created */
	strcpy(clDirectory,"/ceda/debug/");
	strcpy(clFileName,clDirectory);
	strcat(clFileName,"recalc.txt");
	fp = fopen(clFileName,"wt");
	fprintf(fp,"***** Output for controlling the new calculation ****\n");

	/* get actual time */
	olDate = GetCurrentDate();
	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);
	fprintf(fp,"Created on: <%s>\n\n",clTimeNowString);

	/* checking the data */
	if (strlen(pcgData) == 0)
	{
		dbg(TRACE,"bas_init_recalc(): Stopped, because <pcgData> = undef!");
		fclose(fp);
		return;
	}
	dbg(DEBUG,"bas_init_recalc(): <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"bas_init_recalc(): Stopped, because <pcgFields> = undef!");
		fclose(fp);
		return;
	}
	dbg(DEBUG,"bas_init_recalc(): <pcgFields> = <%s>",pcgFields);

	/* Get FROM and TO date */
	pclBuff = (char*)calloc ( 1, strlen(pcgData)+1 );
	if (GetFieldValueFromDataString("FROM",pcgFields,pcgData,clFrom)> 0)
	{
		dbg(DEBUG,"bas_init_recalc(): Recalculation triggered by InitAccount!");

		/* checking the employee urnos */
		if (strlen(pcgSelKey) == 0)
		{
			dbg(TRACE,"bas_init_recalc(): Stopped, because <pcgSelKey> = undef!");
			fclose(fp);
			return;
		}
		pclStaffUrnos = (char*)calloc ( 1, strlen(pcgSelKey)+1 );
		strcpy(pclStaffUrnos,pcgSelKey);
		dbg(DEBUG,"bas_init_recalc(): <pcgSelKey> = <%s>",pcgSelKey);
	}
	else
	{
		/* it's a recalculation after release!*/
		/* Check release dates to get the start month for the recalculation */
		dbg(DEBUG,"bas_init_recalc(): Recalculation triggered by release from level 1 to level 2!");
		if (GetDataItem(pclBuff, pcgData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy (clFrom, pclBuff, 8);
			clFrom[8] = '\0';
		}
		else
		{
			dbg(TRACE,"bas_init_recalc(): ERROR - could not get start date of release!");
			fclose(fp);
			return;
		}

		if (GetDataItem(pclBuff, pcgData, 3, clDel, "", "\0\0") > 0)
		{
			ConvertDbStringToClient(pclBuff);
			pclStaffUrnos = (char*)calloc ( 1, strlen(pclBuff)+1 );
			strcpy(pclStaffUrnos,pclBuff);
		}
		else
		{
			dbg(TRACE,"bas_init_recalc(): ERROR - could not get employee URNOs of release!");
			fclose(fp);
			return;
		}
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strncpy(clMonFrom,&clFrom[4],2);
	clMonFrom[2] = '\0';
	strncpy(clDayFrom,&clFrom[6],2);
	clDayFrom[2] = '\0';

	/* always till end of year */
	strcpy(clTo,clYearFrom);
	strcat(clTo,"1231");
	sprintf(clYearTo,"%-4d",atoi(clYearFrom) + 1);

	fprintf(fp,"Calculations from <%s>\n",clFrom);
	fprintf(fp,"Calculations to   <%s>\n",clTo);

	/* count employees */
	ilCountEmpl = CountElements(pclStaffUrnos);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"bas_init_recalc(): Stopped, because number of employees is 0!");
		fclose(fp);
		return;
	}
	dbg(DEBUG,"bas_init_recalc(): <ilCountEmpl> = <%d>",ilCountEmpl);
	fprintf(fp,"Number of employees = <%d>\n", ilCountEmpl);

	/* add staff to prefetch array */
	HandlePrefetchArray(pclStaffUrnos, clYearFrom, clYearTo, 1);

	/* get the ACC handle */
	ilAccHandle = GetArrayIndex("ACCHDL");
	if (ilAccHandle < 0)
	{
		dbg(TRACE,"bas_init_recalc(): Stopped, because could not receive valid ACC handle! <ilAccHandle> = <%d>",ilAccHandle);
		fclose(fp);
		return;
	}

	/* Schleife pro Mitarbeiter */
	for (ilEmpCounter = 0; ilEmpCounter < ilCountEmpl; ilEmpCounter++)
	{
		GetItem(pclStaffUrnos,ilEmpCounter,",",clCurrentEmpl);
		if (strlen(clCurrentEmpl) == 0)
		{
			dbg(TRACE,"bas_init_recalc(): Stopped, because <clCurrentEmpl> = undef!");
			fclose(fp);
			return;
		}

		fprintf(fp,"Employee <%d> of <%d>\n",ilEmpCounter,ilCountEmpl);
		fprintf(fp,"Start reseting account values...\n");

		/* Account zur�cksetzen	*/
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"5","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"8","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"11","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"16","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"17","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"18","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"26","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"27","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversible(clCurrentEmpl,clYearFrom,clMonFrom,"30","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversible(clCurrentEmpl,clYearFrom,clMonFrom,"32","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversible(clCurrentEmpl,clYearFrom,clMonFrom,"33","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"37","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccount(clCurrentEmpl,clYearFrom,clMonFrom,"38","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversibleQuarter(clCurrentEmpl,clYearFrom,clMonFrom,"51","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversible(clCurrentEmpl,clYearFrom,clMonFrom,"60","RECALC",fp,ilAccHandle);
		ilErrors += ClearAccountReversible(clCurrentEmpl,clYearFrom,clMonFrom,"61","RECALC",fp,ilAccHandle);

		fprintf(fp,"Account values reseted!\n");
		fprintf(fp,"<%d> errors occurred!\n",ilErrors);

		/* load DRR-records*/
		fprintf(fp,"Start reading shift data (DRR-records)...!\n");
		ReadDrrData(clCurrentEmpl,clFrom,clTo,fp);

		/* load DRA-records */
		fprintf(fp,"Start reading part day absences (DRA-records)...!\n");
		ReadDraData(clCurrentEmpl,clFrom,clTo,fp);

		fprintf(fp,"Finished reading records. File created.\n");
	}

	/***** add DELSTF command to event file	***/
	if (ilCountEmpl <= ilMaxUrnoSize)
	{
		ilStfuAdd  = ilCountEmpl;
		strcpy(clStfuList,pclStaffUrnos);
	}
	else
	{
		ilStfuAdd = 0;
		/***** Schleife �ber alle Mitarbeiter	****/
		for (ilEmpCounter = 1; ilEmpCounter < ilCountEmpl; ilEmpCounter++)
		{
			GetItem(pclStaffUrnos,ilEmpCounter,",",clCurrentEmpl);
			if (strlen(clCurrentEmpl) == 0)
			{
				dbg(TRACE,"bas_init_recalc(): DELSTF Stopped, because <clCurrentEmpl> = undef!");
				fclose(fp);
				return;
			}

			if (ilStfuAdd == 0)
			{
				strcpy(clStfuList,clCurrentEmpl);
			}
			else
			{
				strcat(clStfuList,",");
				strcat(clStfuList,clCurrentEmpl);
			}

			ilStfuAdd++;

			if (ilStfuAdd == ilMaxUrnoSize)
			{
				AddEventToFile("XBS2","DELSTF",clYearFrom,"",clStfuList,"");
				AddEventToFile("XBS2","DELSTF",clYearTo,"",clStfuList,"");
				ilStfuAdd = 0;
			}
		}
	}

	if (ilStfuAdd > 0)
	{
		AddEventToFile("XBS2","DELSTF",clYearFrom,"",clStfuList,"");
		AddEventToFile("XBS2","DELSTF",clYearTo,"",clStfuList,"");
	}

	/* add send Broacast event to file */
	AddEventToFile("RECALC_BC",clRecvName,pclStaffUrnos,pcgFields,pcgData,"");
	fclose(fp);

	/* automatically close any recordset that was opened with this flag */
	Cleanup();
}

