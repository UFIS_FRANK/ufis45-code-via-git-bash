#ifndef _DEF_mks_version_tcpserver_h
  #define _DEF_mks_version_tcpserver_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tcpserver_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tcpserver.h 1.2 2004/07/27 17:04:37SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
#ifndef 	__TCP_SERVER_H
#define 	__TCP_SERVER_H


/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* for action section */
#define	iACTIVE							1
#define	iNOT_ACTIVE						0
#define	iSECONDS							1
#define	iMICRO_SECONDS					0

/* CCO- or DBL-Check	*/
#define	iCHECK						0
#define	iDBL_CHECK						1

/* diverses */
#define 	iTCP_RESET						-150
#define	iTCP_TERMINATE					-151
#define	iPULL_TCP_ALLOWED				0
#define	iPULL_TCP_NOT_ALLOWED		1
#define	iOFFSET_IS_UNKNOWN			0
#define	iOFFSET_IS_KNOWN				1

/* some (debug) macros */
#define 	CMD_TYPE(a) ((a) == iINVENTORY_CMD ? "Inventory Command(s)" : ((a) == iUPDATE_CMD ? "Update Command(s)" : ((a) == iFREE_QUERY_CMD ? "Free Query Command(s)" :((a) == iINTERNAL_CMD ? "Internal Command(s)": ((a) == iINCOMING_DATA_CMD ? "Incoming Data Command(s)" : "Unknown Command(s)")))))

#define	CTRL_STA(a) ((a) == HSB_STANDBY ? "STANDBY" : ((a) == HSB_COMING_UP ? "COMING UP" : ((a) == HSB_ACTIVE ? "ACTIVE" : ((a) == HSB_STANDALONE ? "STANDALONE" : ((a) == HSB_ACT_TO_SBY ? "ACTIVE TO STANDBY" : "OTHER STATUS")))))

#define	RETURN(a) ((a) == iTCP_RESET ? "RESET" : ((a) == iTCP_TERMINATE ? "TERMINATE" : "UNKNOWN RETURN CODE"))

#define	FKT(a) ((a) == iINSERT_SECTION ? "INSERT" : "DELETE")

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
typedef struct _TCPServerEvent
{
	int 		iLength;
	char		*pcEvent;
} TCPServerEvent;

typedef struct _TCPServerTcpPackage
{
	int 		iLength;
	char		*pcPackage;
} TCPServerTcpPackage;

typedef struct _TCPTable
{
	int		iModID; /* mod-id we send request to, default is router-id */
	int		iNoFields; /* internal field counter */
	char		pcFieldList[2*iMAX_BUF_SIZE]; /* field list for request */
	char		pcSndCmd[10]; /* command we send */
	char		pcTableName[10]; /* the table name */
	char		pcSelection[iMAX_BUF_SIZE]; /* selection could be long */

	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/
	/* add your members here    														  */
	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/

} TCPTable;

typedef struct _TCPCmd
{
	char		pcHomeAirport[iMIN]; /* the HomeAirport */
	char		pcTableExtension[iMIN]; /* the table externsion */
	char		*pcCmd; /* current command */

	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/
	/* add your members here 	    													  */
	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/

	int		iNoTable; /* how many db-tables */
	TCPTable	*prTabDef; /* everything for table-handling */
} TCPCmd;

/* initialization structures */
typedef struct _TCPMain
{
	/* global entries for all following sections */
	int		iTimeUnit;		/* seconds or microseconds */
	int		iCheckConnection; /* watchdog interval... */

	/* next all protocol entries, TCP/IP-Level */
	int		iUseRProt;	/* sould i use send protocol */
	int		iRProtFile; 
	char		pcRProtFile[iMIN_BUF_SIZE]; /* name + path */
	int		iUseSProt;
	int		iSProtFile; /* file-pointer */
	char		pcSProtFile[iMIN_BUF_SIZE];

	/* TCP/IP-specific entries */
	long		lTCPTimeout; /* it depends on TimeUnit, see above */
	char		pcServiceName[iMIN_BUF_SIZE];
	int		iTCPStartSocket;
	int		iTCPWorkSocket;

	/* for nap */
	long		lNapTime;	

	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/
	/* add your members here 			   											  */
	/***********************************************************************/
	/***********************************************************************/
	/***********************************************************************/

	/* all recognized commands */
	int		iNoCmd; /* how many command do we handle */
	TCPCmd	*prCmd; /* all commands and tables and ... */
} TCPMain;

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* communication structures */
/* the entries here depends on the communication you want to realize */

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* some internal structures */

/* length of structure */
#if 0
#define	/* your name */ sizeof(/*your structure*/) 
#endif

#endif /* __TCP_SERVER_H */


