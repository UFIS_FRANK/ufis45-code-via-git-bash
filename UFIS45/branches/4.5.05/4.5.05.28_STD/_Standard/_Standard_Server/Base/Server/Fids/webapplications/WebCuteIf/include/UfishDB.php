<?


    function Currentlogos($FLZTABuse) {
        Global $db,$Session,$utcdate;
        $StrStatus="";
        if ($Session["Type"]=="GATE") {
            $tmp="GTD1";
        } else {
            $tmp="C";
        }

        if ($FLZTABuse==true) {
            //$cmdTempCommandText = "SELECT FLGU FROM FLZTAB WHERE  FLDU in (SELECT URNO FROM FLDTAB WHERE RNAM='" .$Session["Number"]."' AND RTYP='".$tmp."' and DSEQ>0)";
            if (isset($Session["FlightURNO"])) {
                $cmdTempCommandText = "SELECT FLGU FROM FLZTAB WHERE  FLDU =".$Session["FlightURNO"];
            } else {
                $cmdTempCommandText = "SELECT FLGU FROM FLZTAB WHERE  FLDU =0";
            }

        } else {
            $cmdTempCommandText = "SELECT REMA FROM ccatab WHERE  ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='".$tmp."' AND ( ccatab.CKBS BETWEEN  " . CedaSYSDATE ('d',  -1)." AND " . CedaSYSDATE ('n',  -30).") AND  (ccatab.CKES >" . CedaSYSDATE ('h',  0).") AND (CKBA=' ' or CKBA <=  " . CedaSYSDATE ('h',  0)." )AND (CKEA=' ')  ";
        }

        //echo $cmdTempCommandText;
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
            if ($FLZTABuse==true) {
                $StrStatus=trim($rs->fields("FLGU"))."|".trim($rs->fields("LGSN"));
            } else {
                $StrStatus = trim($rs->fields("REMA"));
            }
            $Session["LogoNameALC"]=false;
        }// If End
        if ($rs) $rs->Close();
        return $StrStatus;
    }// End Function






    function Status() {
        Global $db,$Session;
        Global $RemarkGateOpen,$RemarkGateBoarding,$RemarkGateFinalCall,$RemarkGateClose;
        $StrStatus=" NoEntry";
        if ($Session["Type"]=="GATE") {
            $tmp="GTD1";
        } else {
            $tmp="CKIF";
        }

        //Optimizing the Query 23-02-2005
        //$cmdTempCommandText = "select dseq,urno,ctyp,aurn,rurn from fldtab where rnam = '".$Session["Number"]."' and rtyp = '".$tmp."' and urno > 0 order by cdat desc";
        if ($Session["Type"]=="GATE") {
            $StrStatus=" ";
        } else {
            $StrStatus="CLOSED";
        }
        $Session["STATUS"]=0;


        if (isset($Session["FlightURNO"])) {
            if ($Session["Type"]=="GATE") {
                $tmp = "SELECT GD1X openA,GD1Y closeA,REMP FROM AFTTAB WHERE  URNO= " . $Session["FlightURNO"];

            } else {
                $tmp="SELECT CKBA openA,CKEA closeA,'' as REMP FROM CCATAB WHERE CKIC='".$Session["Number"]."' and (FLNU='" . $Session["FlightURNO"] . "' or URNO='" . $Session["FlightURNO"] . "') ";
                $tmp=$tmp ." and ((ckba=' ') or ((ckba< " . CedaSYSDATE ('n',  30)." and ckba<>' ') ))";
            }
            $cmdTempCommandText = $tmp;


            //echo $cmdTempCommandText;
            $rs=$db->Execute($cmdTempCommandText);
            if (!$rs->EOF)  {


                //$Session["Fldaurn"]=trim($rs->fields("aurn"));
                //$Session["Fldrurn"]=trim($rs->fields("rurn"));
                if (strlen(trim($rs->fields("closeA")))==0 && strlen(trim($rs->fields("openA")))>1 ) {
                    $StrStatus="OPEN";
                    //$Session["CTYP"]=$rs->fields("ctyp");
                    $Session["STATUS"]=1;
                } else {
                    $StrStatus="CLOSED";
                    $Session["STATUS"]=0;
                }

                if ($Session["Type"]=="GATE") {
                    $StrStatus=" ";
                    if (trim($rs->fields("REMP"))==$RemarkGateOpen) {$StrStatus=" Open";}
                    if (trim($rs->fields("REMP"))==$RemarkGateBoarding) {$StrStatus=" Boarding";}
                    if (trim($rs->fields("REMP"))==$RemarkGateFinalCall) {$StrStatus=" Final Call";}
                    if (trim($rs->fields("REMP"))==$RemarkGateClose) {$StrStatus=" Closed";}
                }


            }
            if ($rs) $rs->Close();
        }
        return $StrStatus;


    }// End Function

    function CurrentPRemarks() {
        Global $db,$Session;
        $StrStatus="";
        if (isset($Session["FlightURNO"])) {


            if ($Session["Type"]=="GATE") {
                $tmp = "SELECT REMP as Remark FROM AFTTAB WHERE  URNO= " . $Session["FlightURNO"];

            } else {
                $tmp="SELECT DISP as Remark FROM CCATAB WHERE CKIC='".$Session["Number"]."' and (FLNU='" . $Session["FlightURNO"] . "' or URNO='" . $Session["FlightURNO"] . "') ";
                $tmp=$tmp ." and ((ckba=' ') or ((ckba< " . CedaSYSDATE ('n',  30)." and ckba<>' ') ))";
            }

            $cmdTempCommandText = $tmp;
            $rs=$db->Execute($cmdTempCommandText);
            //echo $tmp;
            if (!$rs->EOF)  {
                $StrStatus=trim($rs->fields("Remark"));
            }// If End

            if ($rs) $rs->Close();
        }
        return $StrStatus;
    }// End Function

    function PRemarks() {
        Global $db,$Session,$hopo,$CPRemark,$PRemarksRemTYPGate,$PRemarksRemTYPCheckin;
        //Changed REMT type
        //WAW Project
        if ($Session["Type"]=="GATE") {
            $tmp=$PRemarksRemTYPGate;
        } else {
            $tmp=$PRemarksRemTYPCheckin;
        }

        //Subject to change the RTYP for the GATE / CHECKIN
        // Change the  beme to receive ascii from unicode in the db

        $cmdTempCommandText = "Select distinct  decode(unicode_str(trim(beme)),'', beme,unicode_str(trim(beme))) beme,code,remi,urno from fidtab where remt='".$tmp."' and beme<>' ' and hopo='$hopo' order by beme";
        //echo $cmdTempCommandText;
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
            while (!$rs->EOF) {
                $tmp="";
                $CPRemark = CurrentPRemarks();
                if ($CPRemark==trim($rs->fields("code"))) {$tmp=" selected";}
                echo '<option value="'.trim($rs->fields("code")).'" '.$tmp.'>'.trim($rs->fields("beme"))."\n";
                $rs->MoveNext();
            }
        }// If End
        if ($rs) $rs->Close();

    }// End Function

    function FreeRemarks($line) {
        Global $db,$Session,$error,$FLDRTYPGate,$FLDRTYPCheckin;
        $StrString="";
        if ($Session["Type"]=="GATE") {
            $tmp=$FLDRTYPGate;
        } else {
            $tmp=$FLDRTYPCheckin;
        }

        $cmdTempCommandText = "SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT='".$line."' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = '".$tmp."' AND RNAM = '".$Session["Number"]."' AND DSEQ > 0)";
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
            $StrString=trim($rs->fields("TEXT"));
        }// If End

        if ($rs) $rs->Close();
        return $StrString;
    }// End Function

    function CheckBlock() {
        Global $db,$Session;
        // GFO : 20080724 Correcting the Blocking Check

        $StrString=0;
        if ($Session["Type"]=="GATE") {
            //$tmp="select count(*) as count from BLKTAB,GATTAB where  (((BLKTAB.NAFR< " . CedaSYSDATE ('h',  0)." and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= " . CedaSYSDATE ('h',  0)." or BLKTAB.nato=' ')) and  (BLKTAB.BURN=GATTAB.URNO) AND GATTAB.GNAM='".$Session["Number"]."')";
            $tmp="SELECT COUNT (*) AS COUNT";
            $tmp=$tmp." FROM blktab, gattab";
            $tmp=$tmp." WHERE (    (    (    blktab.nafr < " . CedaSYSDATE ('h',  0) ."";
            $tmp=$tmp." AND blktab.nafr <> ' '";
            $tmp=$tmp." )";
            $tmp=$tmp." AND (   blktab.nato >= " . CedaSYSDATE ('h',  0) ."";
            $tmp=$tmp." OR blktab.nato = ' '";
            $tmp=$tmp." )";
            $tmp=$tmp." )";
            $tmp=$tmp." AND (blktab.days like '%".WeekDay('h',  0)."%')";
            $tmp=$tmp." ";
            $tmp=$tmp."		AND (blktab.tifr <" . CedaSYSTIME('h',0) . " and blktab.tito > " . CedaSYSTIME('h',0) .")";
            $tmp=$tmp." AND (blktab.burn = gattab.urno)";
            $tmp=$tmp." AND gattab.gnam = '".$Session["Number"]."'";
            $tmp=$tmp." )";
        } else {
           // $tmp="select count(*) as count from BLKTAB,CICTAB where (((BLKTAB.NAFR< " . CedaSYSDATE ('h',  0)." and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= " . CedaSYSDATE ('h',  0)." or BLKTAB.nato=' ')) and (BLKTAB.BURN=CICTAB.URNO) AND CICTAB.CNAM='" .$Session["Number"]. "')";
            $tmp="SELECT COUNT (*) AS COUNT";
            $tmp=$tmp." FROM blktab, CICTAB";
            $tmp=$tmp." WHERE (    (    (    blktab.nafr < " . CedaSYSDATE ('h',  0) ."";
            $tmp=$tmp." AND blktab.nafr <> ' '";
            $tmp=$tmp." )";
            $tmp=$tmp." AND (   blktab.nato >= " . CedaSYSDATE ('h',  0) ."";
            $tmp=$tmp." OR blktab.nato = ' '";
            $tmp=$tmp." )";
            $tmp=$tmp." )";
            $tmp=$tmp." AND (blktab.days like '%".WeekDay('h',  0)."%')";
            $tmp=$tmp." ";
            $tmp=$tmp."		AND (blktab.tifr <" . CedaSYSTIME('h',0) . " and blktab.tito > " . CedaSYSTIME('h',0) .")";
            $tmp=$tmp." AND (blktab.burn = CICTAB.URNO)";
            $tmp=$tmp." AND  CICTAB.CNAM = '".$Session["Number"]."'";
            $tmp=$tmp." )";

        }
        //echo $tmp;
        $cmdTempCommandText = $tmp;
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF){
            if ($rs->fields("count")>1) {$StrString=1;}
        }// If End
        //echo $tmp;
        if ($rs) $rs->Close();
        return $StrString;
    }


    //Added alc2 for Gates and Dedicted Checkin IN
    //WAW Project
    function CheckAlloc($ShowResults=0) {
        Global $db,$Session,$error,$FlightTimeDiffGates;
        Global $GateFrom,$GateTo,$CheckinFrom,$CheckinTo,$CommonCheckinFrom,$CommonCheckinTo;
        Global $RemarkGateOpen,$RemarkGateClose;
        
        $gateHint = "/*+ index(afttab AFTTAB_TIFD_NEW) */";

        $StrString=0;
        if ($Session["Type"]=="GATE") {
            $tmp="SELECT ". $gateHint ." jfno,flno,alc3,alc2,urno,'1' as cmd,tifd,GD1X openA,GD1Y closeA,REMP,stod,etod  FROM afttab WHERE ";
            $tmp = $tmp ." ((gtd1 = '".$Session["Number"]."')  ";
            //              $tmp = $tmp ." AND (DECODE(GD1Y,'              ', GD1E, GD1Y) > " . CedaSYSDATE ('n',  0).")   ";
            //                $tmp = $tmp ." AND GD1Y < " . CedaSYSDATE ('n',  10)."   ";
            $tmp = $tmp ." )";
            //              $tmp = $tmp ." AND ((DECODE(GD1X,'              ', GD1B, GD1X) BETWEEN  " . CedaSYSDATE ('n',  -350)." AND " . CedaSYSDATE ('n',  30).") ";
            //      $tmp = $tmp ." AND (GD1B  BETWEEN  " . CedaSYSDATE ('n',  -60)." AND " . CedaSYSDATE ('n',  120)." ";
            //$tmp = $tmp ." )";
            $tmp = $tmp ." AND OFBL = ' '";
            $tmp = $tmp ." AND FTYP='O' AND (TIFD BETWEEN " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo).") ";
            $tmp = $tmp ." UNION ALL SELECT ". $gateHint ." jfno,flno,alc3,alc2,urno,'2' as cmd,tifd,GD2X openA,GD2Y closeA,REMP,stod,etod   FROM afttab WHERE";
            $tmp = $tmp ." ((gtd2 = '".$Session["Number"]."')  ";
            //              $tmp = $tmp ." AND (DECODE(GD2Y,'              ', GD2E, GD2Y) > " . CedaSYSDATE ('n',  -15).")   ";
            //                $tmp = $tmp ." AND GD2Y < " . CedaSYSDATE ('n',  10)."   ";
            //$tmp = $tmp ." )";
            //              $tmp = $tmp ." AND ((DECODE(GD2X,'              ', GD2B, GD2X) BETWEEN  " . CedaSYSDATE ('n',  -350)." AND " . CedaSYSDATE ('n',  30).") ";
            //      $tmp = $tmp ." AND (GD2B  BETWEEN  " . CedaSYSDATE ('n',  -60)." AND " . CedaSYSDATE ('n',  120)." ";
            $tmp = $tmp ." )";
            $tmp = $tmp ." AND OFBL = ' '";
            $tmp = $tmp ." AND FTYP='O' AND (TIFD BETWEEN " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo).") ORDER by TIFD";
            $tmp1="";
            //		echo $tmp;
            //		exit;
        } else {
            //		$tmp="SELECT FLNO, JFNO, ALC3, URNO, 'D' as cmd1 from AFTTAB WHERE (TIFD BETWEEN TO_CHAR(SYSDATE -30/1440,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+720/1440,'YYYYMMDDHH24MISS')) AND FTYP='O'  AND (URNO IN  (SELECT FLNU from CCATAB  WHERE  (((CKIC = '".$Session["Number"]."')  AND ((DECODE(CKBA,'              ', CKBS, CKBA) < TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') AND CKBS<>' ')) AND ((CKEA > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) OR ((CKES LIKE ' %') AND (CKEA LIKE ' %'))  OR (CKEA LIKE ' %'))))))  ORDER by tifd";
            //Optimizing the Query 23-02-2005
            $tmp=" SELECT   afttab.flno, afttab.jfno, afttab.alc3, afttab.alc2,afttab.urno, 'D' AS cmd1,";
            $tmp=$tmp." CKBA openA,CKEA closeA,REMA as REMP,afttab.stod,afttab.etod ";
            $tmp=$tmp."    FROM afttab,ccatab ";
            $tmp=$tmp."   WHERE ";
            $tmp=$tmp."     afttab.urno=ccatab.flnu";
            $tmp=$tmp."   	 AND (afttab.tifd BETWEEN " . CedaSYSDATE ('n',  $CheckinFrom)." AND " . CedaSYSDATE ('n',  $CheckinTo).")";
            $tmp=$tmp."     AND afttab.ftyp = 'O'";
            $tmp=$tmp."     AND  (    ";
            $tmp=$tmp."	 	  	   (ccatab.ckic = '".$Session["Number"]."')";
            //$tmp=$tmp."           AND (";
            //$tmp=$tmp."		         ( DECODE (ccatab.ckba, '              ', ccatab.ckbs, ccatab.ckba) < " . CedaSYSDATE ('n',  30)."";
            //$tmp=$tmp."                   AND ccatab.ckbs <> ' '";
            //$tmp=$tmp."                 )";
            //$tmp=$tmp."               )";
            //$tmp=$tmp."           AND (   ";
            //$tmp=$tmp."		   	   	   (ccatab.ckea > " . CedaSYSDATE ('h',  0)." )";
            //$tmp=$tmp."				   ";
            //$tmp=$tmp."                   OR ((ccatab.ckes LIKE ' %') AND (ccatab.ckea LIKE ' %'))";
            //$tmp=$tmp."                   OR (ccatab.ckea LIKE ' %')";
            //$tmp=$tmp."                )";
            $tmp=$tmp."                 ";
            $tmp=$tmp."               ";
            $tmp=$tmp."            )";
            $tmp=$tmp."  UNION ALL ";
            $tmp=$tmp." SELECT 'Common' AS flno , ' ' AS jfno,alttab.alc3 , alttab.alc2,";
            $tmp = $tmp . "ccatab.urno AS urno,'C' AS cmd1,ckba opena, ckea closea,  ";
            $tmp = $tmp . "rema AS remp,'' as stod, '' as etod ";
            $tmp = $tmp . " FROM alttab, ccatab ";
            $tmp = $tmp . " WHERE ccatab.flnu = alttab.urno ";
            $tmp = $tmp . " AND ccatab.ckic = '".$Session["Number"]."' ";
            $tmp = $tmp . " AND ccatab.ctyp = 'C' ";
            $tmp = $tmp . " AND (ccatab.ckbs BETWEEN " . CedaSYSDATE ('d',  $CommonCheckinFrom)." AND " . CedaSYSDATE ('n',  $CommonCheckinTo).")";
            $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE ('h',  0).") ";


            //$tmp=$tmp." ORDER BY afttab.tifd ";
            //Optimizing the Query 23-02-2005
            //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2, 'C' as cmd1 FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno(+)  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS')) AND  (ccatab.CKES > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) AND (CKBA=' ' or CKBA <=  TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') )AND (CKEA=' ') ";

            //It will not return any data if the the flnu field in the ccatab is empty for the Common Checkin

            //Added ALC2, ALC3, REMA
            //WAW Project
            //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2,alttab.alc3,ccatab.rema, 'C' as cmd1,CKBA openA,CKEA closeA,REMA as REMP FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  " . CedaSYSDATE ('d',  -1)." AND " . CedaSYSDATE ('n',  30).") AND  (ccatab.CKES > " . CedaSYSDATE ('h',  0).") AND (CKBA=' ' or CKBA <=  " . CedaSYSDATE ('h',  0)." )AND (CKEA=' ') ";
                /*
                 $tmp1 = "SELECT ccatab.urno AS urno, ' ' AS jfno, 'Common' AS flno, alttab.alc2,";
                 $tmp1 = $tmp1 . "alttab.alc3, ccatab.rema, 'C' AS cmd1, ckba opena, ckea closea,";
                 $tmp1 = $tmp1 . " rema AS remp ";
                 $tmp1 = $tmp1 . " FROM alttab, ccatab ";
                 $tmp1 = $tmp1 . " WHERE ccatab.flnu = alttab.urno ";
                 $tmp1 = $tmp1 . "   AND ccatab.ckic = '".$Session["Number"]."' ";
                 $tmp1 = $tmp1 . "   AND ccatab.ctyp = 'C' ";
                 $tmp1 = $tmp1 . "   AND (ccatab.ckbs BETWEEN " . CedaSYSDATE ('d',  $CommonCheckinFrom)." AND " . CedaSYSDATE ('n',  $CommonCheckinTo).")";
                 $tmp1 = $tmp1 . "   AND (ccatab.ckes > " . CedaSYSDATE ('h',  0).") ";
                 */
            //$tmp1 = $tmp1 . "   AND (ckba = ' ' OR ckba <= " . CedaSYSDATE ('h',  0).") ";
            //$tmp1 = $tmp1 . "   AND (ckea = ' ') ";

        }

        $cmdTempCommandText = $tmp;
        //echo $tmp;
        //	exit;

        $error->debug("".$tmp, 'CheckAlloc cmdTemp', __FILE__, __LINE__);
        $error->debug("".$tmp1, 'CheckAlloc cmdTemp1', __FILE__, __LINE__);

        $Session["FldRows"]=0;
        $Session["OldCounterOpen"]=True;
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF){

            while (!$rs->EOF) {

                $closeA = trim($rs->fields("closeA"));
                $ctime= CedaSYSDATE('n',0);
                $ctime = str_replace("'" ,"" ,$ctime);

                $biggFive = false;
                if ($Session["Type"]=="GATE" && len($closeA)>0 ) {
                    $closeA = ChopString("n",0,$closeA);
                    $nowtime = ChopString("n",0,$ctime);
                    if (DateDiff("n",$closeA,$nowtime) >= $FlightTimeDiffGates) {
                        $biggFive = true;
                    }
                }
                $Session["GateNumber"] = trim($rs->fields("cmd"));

                if ($biggFive == false) {
                    $tmp="";
                    $StrString=1;

                    $Session["CommonF"]==0;
                    if ($Session["Type"]!="GATE"){
                        if ($Session["GateNumber"]=='D') {
                            $Session["CommonF"]=0; //Dedicated
                        } else {
                            $Session["CommonF"]=1; //Common
                        }
                    }

                    //Added $Session["LogoName"] for Gates and Dedicted Checkin IN
                    //WAW Project
                    //echo $rs->fields("REMP");

                    $Session["LogoNameALC"]=false;
                    if ((strlen(trim($rs->fields("closeA")))==0 && strlen(trim($rs->fields("openA")))>1)  || trim($rs->fields("REMP"))==$RemarkGateOpen || trim($rs->fields("REMP"))==$RemarkGateClose) {

                        if ($Session["Type"]=="GATE") {
                            if (strlen(trim($rs->fields("alc2")))>1) {
                                $Session["LogoName"]=trim($rs->fields("alc2"));
                            } else {
                                $Session["LogoName"]=trim($rs->fields("alc3"));
                            }
                        } else {

                            if (strlen(trim($rs->fields("REMP")))>1) {
                                $Session["LogoName"]=trim($rs->fields("REMP"));

                            } else {
                                if (strlen(trim($rs->fields("alc2")))>1) {
                                    $Session["LogoName"]=trim($rs->fields("alc2"));
                                } else {
                                    $Session["LogoName"]=trim($rs->fields("alc3"));
                                }
                                $Session["LogoNameALC"]=true;
                            }


                        }
                        $Session["FlightURNO"] = trim($rs->fields("urno"));

                        $tmp=" selected";
                    }

                    $optionstr = "";
                    $optionstr_sep = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if ($ShowResults==1) {

                        //				$tmp="";
                        //echo $Session["Fldaurn"]."--".trim($rs->fields("urno"));
                        //if ($Session["STATUS"]==0 && $Session["FldRows"]==0) { $tmp=" selected";}
                        //				if ($Session["STATUS"]==0 ) { $tmp=" selected";}
                        if ($rs->fields("urno")==$Session["FlightURNO"]) { $tmp=" selected";}
                        $Session["OldCounterOpen"]=False;
                        //				$tmp=" selected";
                                        /*
                                         if ($Session["Fldaurn"]== trim($rs->fields("urno")) ) {

                                         $Session["OldCounterOpen"]=False;
                                         }*/

                        //Changed by GFO 20080219
                        //PRF 8841
                        $etod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        $stod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        if ($Session["CommonF"]==0 || $Session["Type"]=="GATE") {
                            $tdi = FindTDI();
                            $stod = date("H:i", ChopString("n",$tdi,$rs->Fields("stod")));

                            if (len(trim($rs->Fields("etod")))>0)  {
                                $etod = date("H:i", ChopString("n",$tdi,$rs->Fields("etod")));
                            }
                        }

                        $optionstr = $optionstr .trim($rs->fields("flno")) .$optionstr_sep;
                        $optionstr = $optionstr .trim($rs->fields("alc2")) .$optionstr_sep;
                        if (len(trim($rs->fields("remp")))>0) {
                            $optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                        } else {
                            if ($Session["Type"]=="GATE") {
                                $optionstr = $optionstr .$optionstr_sep ."&nbsp;&nbsp;&nbsp;";
                            }

                        }

                        $optionstr = $optionstr .$stod .$optionstr_sep;
                        $optionstr = $optionstr .$etod .$optionstr_sep;

                        //$optionstr = $optionstr .$rs->Fields("stod") .$optionstr_sep;
                        //$optionstr = $optionstr .$rs->Fields("etod") .$optionstr_sep;




                        echo "<option value=".trim($rs->fields("urno"))." ".$tmp.">". $optionstr ."\n";
                        $error->debug("".$rs->fields("jfno"), 'lenjfno', __FILE__, __LINE__);
                        //Code Share Flights
                        $lenJFNO=(int)(strlen(trim($rs->fields("jfno")))/9);
                        $error->debug("".$lenJFNO, 'lenjfno', __FILE__, __LINE__);
                        $Sstart=0;
                        $Send=9;
                        //if ($lenJFNO>0) {$Session["CTYP"]="M".$lenJFNO+1;}
                        for($i=0; $i<=$lenJFNO; $i++) {
                            $st1=substr( trim($rs->fields("jfno")), $Sstart, $Send);
                            if (len($st1)>0) {
                                $optionstr = "";
                                $optionstr = $optionstr .trim($st1) .$optionstr_sep;
                                $optionstr = $optionstr .$optionstr_sep ."&nbsp;&nbsp;";
                                if (len(trim($rs->fields("remp")))>0) {
                                    $optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                                } else {
                                    if ($Session["Type"]=="GATE") {
                                        $optionstr = $optionstr .$optionstr_sep ."&nbsp;&nbsp;&nbsp;";
                                    }
                                }
                                //$optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                                $optionstr = $optionstr ."".$stod .$optionstr_sep;
                                $optionstr = $optionstr .$etod .$optionstr_sep;

                                echo "<option value=".trim($rs->fields("urno")).">". $optionstr ."\n";
                                $error->debug("Start".$Sstart ."--End".$Send."--St1".$st1, 'st1', __FILE__, __LINE__);
                            }
                            $Sstart = $Sstart+9;
                        }
                        $Session["FldRows"]=$Session["FldRows"]+1;
                    }
                    //else {
                    //while (!$rs->EOF) {
                    //	$Session["AlocUrno"]=trim($rs->fields("urno"));
                    //$rs->MoveNext();
                    //}
                    //}
                } //End BigFive

                $rs->MoveNext();
            }

        } else {
                /*
                 // If it is a CheckIn then Perhaps it is a COMMON CheckIn
                 if (len($tmp1)>0) {

                        $cmdTempCommandText = $tmp1;
                        $rs1=$db->Execute($cmdTempCommandText);
                        if (!$rs1->EOF)  {
                        while (!$rs1->EOF) {
                        $tmp="";
                        $StrString=1;
                        $Session["CommonF"]=1;
                        $Session["LogoNameALC"]=false;

                        if ((strlen(trim($rs1->fields("closeA")))==0 && strlen(trim($rs1->fields("openA")))>1)  || $rs1->fields("REMP")==$RemarkGateOpen) {
                        if (strlen(trim($rs1->fields("REMP")))>1) {
                        $Session["LogoName"]=trim($rs1->fields("REMP"));
                        } else {
                        if (strlen(trim($rs1->fields("alc2")))>1) {
                        $Session["LogoName"]=trim($rs1->fields("alc2"));
                        } else {
                        $Session["LogoName"]=trim($rs1->fields("alc3"));
                        }
                        $Session["LogoNameALC"]=true;
                        }

                        $Session["FlightURNO"] = trim($rs1->fields("urno"));
                        $tmp=" selected";
                        }

                        //if (strlen(trim($rs->fields("rema")))>1) {
                        //	$Session["LogoName"]=trim($rs->fields("rema"));
                        //}

                        if ($ShowResults==1) {
                        //						$tmp="";
                        //if ($Session["STATUS"]==0 && $Session["FldRows"]==0) { $tmp=" selected";}
                        if ($Session["STATUS"]==0 ) { $tmp=" selected";}
                        $Session["OldCounterOpen"]=False;
                        //						$tmp=" selected";

                        echo "<option value=".trim($rs1->fields("urno"))." ".$tmp.">".trim($rs1->fields("flno"))."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".trim($rs1->fields("alc2"))."\n";

                        //Code Share Flights
                        $lenJFNO=(int)(strlen(trim($rs->fields("jfno")))/9);
                        $error->debug("".$lenJFNO, 'lenjfno', __FILE__, __LINE__);
                        $Sstart=0;
                        $Send=8;
                        //if ($lenJFNO>0) {$Session["CTYP"]="M".$lenJFNO+1;}
                        for($i=0; $i<=$lenJFNO; $i++) {
                        $st1=substr( trim($rs1->fields("jfno")), $Sstart, $Send);
                        if (len($st1)>0) {
                        echo "<option value=".trim($rs1->fields("urno")).">".trim($st1)."\n";
                        }
                        $Sstart = $Sstart+9;
                        }
                        $Session["FldRows"]=$Session["FldRows"]+1;
                        }

                        //else {
                        //while (!$rs->EOF) {
                        //	$Session["AlocUrno"]=trim($rs->fields("urno"));
                        //$rs->MoveNext();
                        //}
                        //}
                        $rs1->MoveNext();
                        }
                        if ($rs1) $rs1->Close();

                        }// If End

                        }// If End
                        */


        }// If End
        if ($rs) $rs->Close();
        //	exit;
        return $StrString;

    }





    function UpdateUfisLH($CallButton) {
        Global $db, $Session, $FlightURNO;
        Global $PRemarkCode,$Text1,$Text2,$RSHIP;
        Global $error,$timer,$FreeTextSelection,$logoSelectionType,$logoSelection,$FLZTABuse;
        Global $FLDRTYPGate,$FLDRTYPCheckin;
        Global $RemarkGateOpen,$RemarkGateBoarding,$RemarkGateFinalCall,$RemarkGateClose;

        ////////////////////////////////////////////////////////////
        //
        //		FLIGHT LISTBOX FOR FLDTAB
        //
        //	If the counter is opened just send the values to the
        //	FLDTAB. The Flight list to show will be queried from
        //	the AODB.
        //	In case of a closed counter create this flight list
        //	from the user's listbox selections and send the string
        //	aling with the other formular values to the
        //	FLDTAB.
        ///////////////////////////////////////////////////////////

        $AOTI=" ";
        $ABTI=" ";
        $AFTI=" ";
        $CTYP=" ";
        $STAT=" ";



        if ($Session["Type"]=="GATE") {
            // SELECT DATES FROM AFTTAB FOR GATES
            $cmdTempCommandText = "SELECT GD1B,GD1E,GD2B,GD2E FROM AFTTAB WHERE  URNO= " . $FlightURNO;
            $rs=$db->Execute($cmdTempCommandText);
            if (!$rs->EOF)  {
                //IF AG1
                $GD1B=$rs->fields("GD1B");
                $GD1E=$rs->fields("GD1E");

                //IF AG2
                $GD2B=$rs->fields("GD2B");
                $GD2E=$rs->fields("GD2E");
                $UPDATEURNO= $FlightURNO;
            }else {
                // EXIT From the Function
                $error->debug('NoData Found :Exit From Update Function Gate Select'.$Session["Number"], 'IBT', __FILE__, __LINE__);
                return 0;
            }// If End
            if ($rs) $rs->Close();



        } else {
            // SELECT VALUES FROM CCATAB FOR CHECKIN
            $cmdTempCommandText = "SELECT URNO,CKBS,CKES FROM CCATAB WHERE CKIC='" .$Session["Number"]. "' and (FLNU='" .$FlightURNO. "' or URNO='" .$FlightURNO. "') ";
            //and ((ckba=' ') or ((ckba< " . CedaSYSDATE ('n',  30)." and ckba<>' ') and (ckea=' ' or ckea> " . CedaSYSDATE ('h',  0).")))";
            $rs=$db->Execute($cmdTempCommandText);
            if (!$rs->EOF)  {
                $CCAURNO=$rs->fields("URNO");
                $CKBS=$rs->fields("CKBS");
                $CKES=$rs->fields("CKES");

                $UPDATEURNO=$CCAURNO;
            }else {
                // EXIT From the Function
                //echo 'exit';
                $error->debug('NoData Found :Exit From Update Function Checkin Select'.$Session["Number"], 'IBT', __FILE__, __LINE__);
                return 0;
            }// If End
            if ($rs) $rs->Close();
            // SELECT URNO FROM FLDTAB
        }



        if (len($PRemarkCode)==0) {$CREC=" ";} else {$CREC=$PRemarkCode;}
        if ($PRemarkCode=="00000000") {$CREC=" ";}

        // Replace any occurence of ',' or '?' with ' '
        if (len($Text1)==0) {$Text1=" ";} else {$Text1=str_replace("," ," " ,$Text1);$Text1=str_replace(";" ,"?" ,$Text1);}
        if (len($Text2)==0) {$Text2=" ";} else {$Text2=str_replace("," ," " ,$Text2);$Text2=str_replace(";" ,"?" ,$Text2);}

        if (len($AOTI)==0) {$AOTI=" ";}
        if (len($ABTI)==0) {$ABTI=" ";}
        if (len($AFTI)==0) {$AFTI=" ";}

        if (len($CREC)==0) {$CREC=" ";}
        if (len($CTYP)==0) {
            // COMMON CHECKIN
            if ($Session["CommonF"]==1) {$CTYP=" ";} else {$CTYP="M";}


        }
        $PicName = $Session["PicName"];
        if ($Session["PicName"]=='None'){
            $PicName = ' ';
        }

        $tmp="";
        if ($Session["Type"]=="GATE") {
            $tmp=$FLDRTYPGate;
        } else {
            $tmp=$FLDRTYPCheckin;
        }
        $cmdTempCommandText = "SELECT URNO FROM FLDTAB WHERE RTYP = '".$tmp."' AND RNAM = '".$Session["Number"]."' AND DSEQ > 0";
        //echo $cmdTempCommandText;

        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
            $UPDATE_FLDURNO = trim($rs->fields("URNO"));
        } else {
            $UPDATE_FLDURNO = 0;
            // EXIT From the Function
            //echo 'exit2';
            //$error->debug('NoData Found :Exit From Update Function Checkin Select'.$Session["Number"], 'SELECT', __FILE__, __LINE__);
            //return 0;
        }
        $Session["UPDATE_FLDURNO"] = $UPDATE_FLDURNO;

        if (len($STAT)==0) {$STAT=" ";}

        if ($CallButton=='LogoTransmit') {
            if ($logoSelection==true) {
                if ($FLZTABuse==true) {
                    // SEND LOGOS INTO FLZTAB
                    // SELECT URNO FROM FLZTAB where fldu = fldtab.urno

                    $cmdTempCommandText = "SELECT URNO FROM FLZTAB WHERE FLDU=" . $UPDATE_FLDURNO." AND SORT = 1";
                    $rs=$db->Execute($cmdTempCommandText);

                    if (!$rs->EOF)  {
                        $FLZURNO=trim($rs->fields("URNO"));
                        $Dbg_level=101;
                        $Dest="\'1200\'";
                        $Prio="\'3\'";
                        $Command="\'URT\'";
                        $Table="\'FLZTAB\'";
                        $Fields="\'FLGU\'";
                        $Data="\'".$Session["AirlineURNO"]."\'";
                        $Selection="\'WHERE URNO = ". trim($FLZURNO) ."\'";
                        $Timeout="\'-1\'";

                        //Send that stuff
                        CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                    } else {
                        $Dbg_level=101;
                        $Dest="\'1200\'";
                        $Prio="\'3\'";
                        $Command="\'IBT\'";
                        $Table="\'FLZTAB\'";
                        $Fields="\'FLDU,FLGU,SORT\'";
                        $Data="\'".$UPDATE_FLDURNO.",".$Session["AirlineURNO"].",1\'";
                        $Selection="\'\'";
                        $Timeout="\'-1\'";

                        //Send that stuff
                        CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                    }// If End
                    if ($rs) $rs->Close();
                    $timer->addmarker("Second Cput (FLZTAB)");
                } else {
                    if ($Session["Type"]=="GATE") {
                        //Nothing
                    } else {

                        $Dbg_level=101;
                        $Dest="\'1200\'";
                        $Prio="\'3\'";
                        $Command="\'URT\'";
                        $Table="\'CCATAB\'";
                        $Fields="\'REMA\'";
                        $Data="\'".$PicName.",CUTE-IF\'";
                        $Selection="\'WHERE URNO = ". trim($CCAURNO) ."\'";
                        $Timeout="\'-1\'";
                        //Send that stuff
                        CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                    }
                }
            }
        }

        if ($CallButton=='Transmit' || $CallButton=='FreeText') {
            if ($FreeTextSelection==true) {
                // SEND FREE TEXTES INTO FXTTAB
                // SELECT URNO FROM FXTTAB where fldu = fldtab.urno

                // FREE TEXT 1
                if (len(trim($Text1))==0 ) {
                    $Text1=" ";
                }

                $cmdTempCommandText = "SELECT URNO FROM FXTTAB WHERE FLDU=" .$UPDATE_FLDURNO." AND SORT = 1";
                //echo $cmdTempCommandText;
                $rs=$db->Execute($cmdTempCommandText);
                if (!$rs->EOF)  {
                    $FLXURNO=trim($rs->fields("URNO"));
                    $Dbg_level=101;
                    $Dest="\'1200\'";
                    $Prio="\'3\'";
                    $Command="\'URT\'";
                    $Table="\'FXTTAB\'";
                    $Fields="\'TEXT\'";
                    $Data="\'".$Text1."\'";
                    $Selection="\'WHERE URNO = ". trim($FLXURNO) ."\'";
                    $Timeout="\'-1\'";

                    //Send that stuff
                    CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                } else {
                    $Dbg_level=101;
                    $Dest="\'1200\'";
                    $Prio="\'3\'";
                    $Command="\'IBT\'";
                    $Table="\'FXTTAB\'";
                    $Fields="\'FLDU,TEXT,SORT\'";
                    $Data="\'".$UPDATE_FLDURNO.",".$Text1.",1\'";
                    $Selection="\'\'";
                    $Timeout="\'-1\'";

                    //Send that stuff
                    CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                }// If End
                if ($rs) $rs->Close();
                $timer->addmarker("3rd Cput (FXTTAB)");




                if (len(trim($Text2))==0 ) {
                    $Text2=" ";
                }
                // FREE TEXT 2
                $cmdTempCommandText = "SELECT URNO FROM FXTTAB WHERE FLDU=" .$UPDATE_FLDURNO." AND SORT = 2";
                $rs=$db->Execute($cmdTempCommandText);
                if (!$rs->EOF)  {
                    $FLXURNO=trim($rs->fields("URNO"));
                    $Dbg_level=101;
                    $Dest="\'1200\'";
                    $Prio="\'3\'";
                    $Command="\'URT\'";
                    $Table="\'FXTTAB\'";
                    $Fields="\'TEXT\'";
                    $Data="\'".$Text2."\'";
                    $Selection="\'WHERE URNO = ". trim($FLXURNO) ."\'";
                    $Timeout="\'-1\'";

                    //Send that stuff
                    CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                } else {
                    $Dbg_level=101;
                    $Dest="\'1200\'";
                    $Prio="\'3\'";
                    $Command="\'IBT\'";
                    $Table="\'FXTTAB\'";
                    $Fields="\'FLDU,TEXT,SORT\'";
                    $Data="\'".$UPDATE_FLDURNO.",".$Text2.",2\'";
                    $Selection="\'\'";
                    $Timeout="\'-1\'";

                    //Send that stuff
                    CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                }// If End
                if ($rs) $rs->Close();
                $timer->addmarker("4th Cput (FXTTAB)");
            }

        }


        //		COUNTER CLOSED -> OPENING IT
        //if ($Session["STATUS"]==0) {
        $currenttime=getCedaTime();
        if ($Session["Type"]=="GATE") {

            // Set gate flag ??
            $Dbg_level=101;
            $Dest="\'1200\'";
            $Prio="\'3\'";
            $Command="\'UFR\'";
            $Table="\'AFTTAB\'";
            if ($Session["STATUS"]==0 &&  $CREC == $RemarkGateBoarding) {
                $Fields="\'REMP,GD".$Session["GateNumber"]."X,GD".$Session["GateNumber"]."Y,LSTU,CHGI,USEU\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
                $CHGI=date("H:i", ChopString ("n", 0, $currenttime)  );
                $Data="\'".$CREC. ",".$currenttime. ",," . $currenttime.",GD".$Session["GateNumber"]."X=".$CHGI.",CUTE-IF\'";
            } elseif ($CREC=="Clear") {
                $Fields="\'REMP,GD".$Session["GateNumber"]."X,GD".$Session["GateNumber"]."Y,LSTU,CHGI,USEU\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
                $CHGI=date("H:i", ChopString ("n", 0, $currenttime));
                $Data="\',,,,ClearGate=".$CHGI.",CUTE-IF\'";
            } else {
                if ($CREC == $RemarkGateOpen) {
                    $Fields="\'REMP,GD".$Session["GateNumber"]."X,GD".$Session["GateNumber"]."Y,LSTU,USEU\'";
                    //$Fields="\'GD2X,LSTU,USEU\'";
                    $Data="\'".$CREC. ",,," . $currenttime.",CUTE-IF\'";

                } else {

                    $Fields="\'REMP,LSTU,USEU\'";
                    //$Fields="\'GD2X,LSTU,USEU\'";
                    $Data="\'".$CREC. "," . $currenttime.",CUTE-IF\'";
                }
            }
            $Selection="\'WHERE URNO = ". trim($FlightURNO) ."\'";
            $Timeout="\'-1\'";

            //Send that stuff
            CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);

        } else {

            $Dbg_level=101;
            $Dest="\'1200\'";
            $Prio="\'3\'";
            $Command="\'URT\'";
            $Table="\'CCATAB\'";
            if ($Session["STATUS"]==0) {
                $Fields="\'DISP,CKBA,CKEA,LSTU,USEU\'";
                //$Data="\'".$currenttime . ",".$CREC. "," . $currenttime.",CUTE-IF\'";
                $Data="\'".$CREC.",".$currenttime . ", ," . $currenttime.",CUTE-IF\'";
            } elseif ($CallButton=="Clear") {
                $Fields="\'CKEA,LSTU,USEU\'";
                //$Data="\'".$currenttime . ",".$CREC. "," . $currenttime.",CUTE-IF\'";
                $Data="\'," . $currenttime.",CUTE-IF\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
            }elseif ($CallButton=='Transmit') {
                $Fields="\'DISP,LSTU,USEU\'";
                $Data="\'".$CREC."," . $currenttime.",CUTE-IF\'";
            }

            $Selection="\'WHERE URNO = ". trim($CCAURNO) ."\'";
            $Timeout="\'-1\'";
            //Send that stuff
            CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
        }

        $timer->addmarker("6th Cput (AFFTAB, CCATAB)");

        return 0;
    }

    function CloseCounter() {

        Global $db, $Session, $FlightURNO,$error;
        Global $RemarkGateClose;

        ///////////////////////////////////////////////////////////
        //
        // FIRST UPDATE FLDTAB: SET DSEQ FOR ACTUAL ENTRIES = -1
        //
        ///////////////////////////////////////////////////////////
        if ($Session["Type"]=="GATE") {
                /*
                 $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'GTD1' AND DSEQ > 0 AND AURN=" . $FlightURNO;
                 $cmdTempCommandText = $tmp;
                 $rs=$db->Execute($cmdTempCommandText);
                 if (!$rs->EOF)  {
                        $FLDURNO=trim($rs->fields("URNO"));
                        }
                        if ($rs) $rs->Close();
                        */
        } else {
            $CCAURNO="";
            //$tmp="SELECT URNO FROM CCATAB WHERE CKIC='".$Session["Number"]."' and (FLNU='" . $FlightURNO . "' or URNO='" . $FlightURNO . "') and ((ckba=' ') or ((ckba< TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') and ckba<>' ') and (ckea=' ' or ckea> TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))))";
            $tmp="SELECT URNO FROM CCATAB WHERE CKIC='".$Session["Number"]."' and (FLNU='" . $FlightURNO . "' or URNO='" . $FlightURNO . "') ";
            $tmp=$tmp ." and ((ckba=' ') or ((ckba< " . CedaSYSDATE ('n',  30)." and ckba<>' ') ))";
            $cmdTempCommandText = $tmp;
            $rs=$db->Execute($cmdTempCommandText);
            if (!$rs->EOF)  {
                $CCAURNO=trim($rs->fields("URNO"));
            } else {
                // EXIT From the Function
                $error->debug("".$tmp, 'Error :Exit From Close Function for Checkin '.$Session["Number"], __FILE__, __LINE__);
                //return;
            }// If End
            if ($rs) $rs->Close();
                /*
                 if ($CCAURNO=="" && $Session["CommonF"]==0) {
                        //The Counter was not Closed corectly
                        $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'CKIF' AND DSEQ > 0 AND AURN=" . $FlightURNO;
                        } else {
                        $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'CKIF' AND DSEQ > 0 AND RURN=" . $CCAURNO;
                        }
                        $cmdTempCommandText = $tmp;
                        $rs=$db->Execute($cmdTempCommandText);

                        if (!$rs->EOF)  {
                        $FLDURNO=trim($rs->fields("URNO"));
                        //echo $FLDURNO;
                        } else {
                        // EXIT From the Function
                        $error->debug("".$tmp, 'Error :Exit From Close Function for Checkin '.$Session["Number"]." ".$CCAURNO, __FILE__, __LINE__);
                        return;
                        }// If End
                        if ($rs) $rs->Close();
                        */
        }
        //	echo $tmp;
        //	exit;
        /*
         $Dbg_level=101;
         $Dest="\'1200\'";
         $Prio="\'3\'";
         $Command="\'URT\'";
         $Table="\'FLDTAB\'";
         $Fields="\'LSTU,DSEQ,ACTI,USEU\'";

         $currenttime=getCedaTime();

         if ($Session["Type"]=="GATE") {
                $Data="\'".$currenttime. ",-1," . $currenttime . ",CUTE-IF\'";
                } else {
                $Data="\'".$currenttime . ",-1, ,CUTE-IF\'";
                }

                $Selection="\'WHERE URNO = ". trim($FLDURNO) ."\'";
                $Timeout="\'-1\'";

                //Send that stuff
                if ((trim($FLDURNO)) != "" ) {
                $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
                }
                */

        $Dbg_level=101;
        $Dest="\'1200\'";
        $Prio="\'3\'";

        $currenttime=getCedaTime();
        if ($Session["Type"]=="GATE") {
            $Command="\'UFR\'";
            $Table="\'AFTTAB\'";

            //$Fields="\'GD1Y or GD2Y,LSTU,USEU\'";
            $Fields="\'GD".$Session["GateNumber"]."Y,REMP,LSTU,CHGI,USEU\'";
            $CHGI=date("H:i", ChopString ("n", 0, $currenttime)  );
            $Data="\'".$currenttime . ",".$RemarkGateClose."," .$currenttime . ",GD".$Session["GateNumber"]."Y=".$CHGI.",CUTE-IF\'";

            $Selection="\'WHERE URNO = ". trim($FlightURNO) ."\'";
        } else {
            // UPDATE CCATAB CKEA,LSTU,USEU
            $Command="\'URT\'";
            $Table="\'CCATAB\'";
            $Fields="\'CKEA,LSTU,USEU\'";

            $Data="\'".$currenttime . "," .$currenttime . ",CUTE-IF\'";
            $Selection="\'WHERE URNO = ". trim($CCAURNO) ."\'";
        }


        $Timeout="\'-1\'";

        //Send that stuff
        if ($Session["Type"]=="GATE") {
            $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
        } else {
            if (($CCAURNO) != "" ) {
                $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
            }
        }

        // Delete From FXTTAB
        if (isset($Session["UPDATE_FLDURNO"])) {
            // DELETE FXTTAB WHERE FLDU = SESSION FLDU
            $Command="\'DRT\'";
            $Table="\'FXTTAB\'";
            $Fields="\'\'";
            $Data="\''";
            $Selection="\'WHERE FLDU = ". $Session["UPDATE_FLDURNO"] ."\'";
            $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
        }
        //Infrom local Session
        $Session["STATUS"]=0;

    }
?>
