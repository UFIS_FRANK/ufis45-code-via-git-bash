<?php
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
class LoginInit {
    // Public Variables
    //	$db=&GLOBALS["db"];

    function Login_Mysql($UserName,$Password) {

        $dbMysql = $GLOBALS["dbMysql"];
        $Session = $GLOBALS["Session"];
        $ClientIP = $GLOBALS["ClientIP"];
        $NoIp = $GLOBALS["NoIp"];



        $logedIn=0;


        $cmdTempCommandText = "SELECT  * from user where dtName='".strtolower($UserName)."' and dtPwd='".strtolower($Password)."'";
        $rs=$dbMysql->Execute($cmdTempCommandText);

        if ($rs )  {

            $Session["userid"]=$rs->fields("pkuserid");
            $Session["username"]=strtolower($rs->fields("dtName"));
            $UserIP=$rs->fields("dtIP");
            //Check the type (Gate or Checkin) and parse the dtName

            if (strstr($Session["username"],"gate")) {
                // It is a Gate
                $Session["Type"]="GATE";
                $Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
            } else {
                // It is a ChekIn
                $Session["Type"]="CHECK-IN";
                $Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));

            }

            if ($NoIp!="true") {
                if ($UserIP!=$ClientIP){
                    header ("Location: error.php?Error=2");
                    exit ;
                }
            }

            //We Must check About The Blocking
            if (CheckBlock()==1) {
                header ("Location: error.php?Error=3");
                exit ;
            }

            // We Must for  allocation
            if (CheckAlloc()==0) {
                header ("Location: error.php?Error=4");
                exit ;
            }



            $logedIn=1;

        } else {
            header ("Location: index.php?Error=1");
            exit ;

        }// If End
        if ($rs) $rs->Close();

        $GLOBALS["Session"] =$Session ;
        return $logedIn;
    }// End Function
    function Login_Oracle($UserName,$Password) {
        //Global $db,$Session,$ClientIP,$NoIp;

        $db = $GLOBALS["db"];
        $Session = $GLOBALS["Session"];
        $ClientIP = $GLOBALS["ClientIP"];
        $NoIp = $GLOBALS["NoIp"];

        $logedIn=0;


        $cmdTempCommandText = "SELECT  * from user where dtName='".strtolower($UserName)."' and dtPwd='".strtolower($Password)."'";
        $rs=$db->Execute($cmdTempCommandText);

        if ($rs )  {

            $Session["userid"]=$rs->fields("pkuserid");
            $Session["username"]=strtolower($rs->fields("dtName"));
            $UserIP=$rs->fields("dtIP");
            //Check the type (Gate or Checkin) and parse the dtName

            if (strstr($Session["username"],"gate")) {
                // It is a Gate
                $Session["Type"]="GATE";
                $Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
            } else {
                // It is a ChekIn
                $Session["Type"]="CHECK-IN";
                $Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));

            }

            if ($NoIp!="true") {
                if ($UserIP!=$ClientIP){
                    header ("Location: error.php?Error=2");
                    exit ;
                }
            }

            //We Must check About The Blocking
            if (CheckBlock()==1) {
                header ("Location: error.php?Error=3");
                exit ;
            }

            // We Must for  allocation
            if (CheckAlloc()==0) {
                header ("Location: error.php?Error=4");
                exit ;
            }



            $logedIn=1;

        } else {
            header ("Location: index.php?Error=1");
            exit ;

        }// If End
        if ($rs) $rs->Close();

        $GLOBALS["Session"] =$Session ;

        return $logedIn;
    }// End Function
    function Login_File($UserName,$Password) {
        //Global $Session,$ClientIP,$NoIp,$UserFilename,$FLZTABuse,$logoSelection,$CheckinPredefinedSelection,$GatePredefinedSelection;

        $Session = $GLOBALS["Session"];
        $ClientIP = $GLOBALS["ClientIP"];
        $NoIp = $GLOBALS["NoIp"];
        $UserFilename = $GLOBALS["UserFilename"];
        $FLZTABuse = $GLOBALS["FLZTABuse"];
       // $logoSelection = $GLOBALS["logoSelection"];
        $CheckinPredefinedSelection = $GLOBALS["CheckinPredefinedSelection"];
        $GatePredefinedSelection = $GLOBALS["GatePredefinedSelection"];

        $CheckinlogoSelection = $GLOBALS["CheckinlogoSelection"];
        $GatelogoSelection = $GLOBALS["GatelogoSelection"];

        $logedIn=0;
        $auth=false;

        // Read the entire file into the variable $file_contents

        //$filename = '/path/to/file.txt';
        $fp = fopen( $UserFilename, 'r' );
        $file_contents = fread( $fp, filesize( $UserFilename ) );
        fclose( $fp );

        // Place the individual lines from the file contents into an array.

        $lines = explode ( "\n", $file_contents );
        $logger = LoggerManager::getRootLogger('CuteIf');

        //$logger->debug("File username : ".$username."-- Password :" .$password);
	    //$logger->debug("SendValue username : ".$UserName."-- Password :" .$Password);
        
        // Split each of the lines into a username and a password pair
        // and attempt to match them to $PHP_AUTH_USER and $PHP_AUTH_PW.

        foreach ( $lines as $line ) {

            list( $userid,$username, $password,$UserIP,$UserlogoSelection,$UserPredefinedSelection ) = explode( ':', $line );

            if ( ( strtolower($username) == strtolower($UserName) ) &&
                ( strtolower($password) == strtolower($Password) ) &&
                strlen($UserName) > 0             ) {


                // A match is found, meaning the user is authenticated.
                // Stop the search.

                $auth = true;
                if ($NoIp!="true") {
                	if ($UserIP!=$ClientIP){
                        
                        $logger->debug('Wrong IP'.$ClientIP);
                    	header ("Location: index.php?Error=2");
                    	exit ;
                	}
            	}
                break;

            }
        }

        if ($auth )  {

            $Session["userid"]=$userid;
            $Session["username"]=strtoupper($username);
            $UserIP=$UserIP;

            $logger->debug('User Auth'.$userid);
            //Check the type (Gate or Checkin) and parse the dtName

            /*
            if ($NoIp!="true") {
                if ($UserIP!=$ClientIP){
                    header ("Location: error.php?Error=2");
                    exit ;
                }
            }
            */
            
            if (strstr($Session["username"],"GATE"))  {
                // It is a Gate
                $Session["Type"]="GATE";
                $Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
                $Session["Number"]=	strtoupper(str_replace('GATE', '', $Session["username"]));
                $Session["PredefinedSelection"] = $GatePredefinedSelection;
                $PredefinedSelection =  $GatePredefinedSelection;
                $Session["logoSelection"]=  $GatelogoSelection;
                $logoSelection=  $GatelogoSelection;
               
            } else {
                // It is a ChekIn
                $Session["Type"]="CHECK-IN";
                $Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
                $Session["Number"]=strtoupper(str_replace( 'CHECKIN','', $Session["username"]));
                $Session["PredefinedSelection"] = $CheckinPredefinedSelection;
                $PredefinedSelection =  $CheckinPredefinedSelection;
                $Session["logoSelection"]=  $CheckinlogoSelection;
                $logoSelection=  $CheckinlogoSelection;
            }

            $Session["logoSelection"] = $logoSelection;
            $GLOBALS["logoSelection"] = $logoSelection;

            $Session["logoSelectionUser"] = true;
            $Session["PredefinedSelectionUser"] = $PredefinedSelection;



            if ($logoSelection==true) {
                if (strlen(trim($UserlogoSelection))>0) {
                    //$Session["logoSelection"] = strtoboolean(strtoupper($UserlogoSelection));
                    $Session["logoSelectionUser"] = strtoboolean(strtoupper(trim($UserlogoSelection)));


                }
            }
            if ($PredefinedSelection==true) {
                if (strlen(trim($UserPredefinedSelection))>0) {
                    //$Session["CheckinPredefinedSelection"] = strtoboolean(strtoupper($UserCheckinPredefinedSelection));
                    $Session["PredefinedSelectionUser"] = strtoboolean(strtoupper(trim($UserPredefinedSelection)));
                    //echo strtoupper($UserCheckinPredefinedSelection) ."aaaaaaaaaa<br>";
                }
            }

            $logger->debug('logoSelectionUser'. $Session["logoSelectionUser"]);
            $logger->debug('PredefinedSelectionUser'. $Session["PredefinedSelectionUser"]);


            $GLOBALS["Session"] =$Session ;

            
            //$logoSelection,$CheckinPredefinedSelection

            //We Must check About The Blocking
            if (CheckBlock()==1) {
                 $logger->debug('CheckBlock');
                header ("Location: error.php?Error=3");
                exit ;
            }
            //exit;
            //$StatusFlag=Status();


            // We Must check for  allocation
            if (CheckAlloc2()==0) {
                 $logger->debug('CheckAlloc2');
                header ("Location: error.php?Error=4");
                exit ;
            }
            //exit;
            // We must Check the Status Of the GateCheckin

            //Find Current Logo
            //$tmplogo = Currentlogos($FLZTABuse);

            //if (strlen(trim($tmplogo))>1) {
            //	$Session["LogoName"] = $tmplogo;
            //}


            //if ($StatusFlag=="OPEN" &&) {
            //	header ("Location: error.php?Error=6&oldUrno=".$Session["Oldurno"]."&urno=".$Session["urno"]);
            //	exit ;
            //}

            $logedIn=1;
            $logger->debug('User Auth LoggedIn'.$logedIn);
        } else {
            header ("Location: index.php?Error=1");
            exit ;

        }// If End
        
        
        return $logedIn;
    }// End Function
    function Login_Variable($UserName,$Password) {
        //Global $Session,$ClientIP,$P01,$P02,$NoIp,$UserFilename,$Loginhopo;

        $Session = $GLOBALS["Session"];
        $ClientIP = $GLOBALS["ClientIP"];
        $NoIp = $GLOBALS["NoIp"];
        $UserFilename = $GLOBALS["UserFilename"];
        $P01 = $GLOBALS["P01"];
        $P02 = $GLOBALS["P02"];
        $Loginhopo = $GLOBALS["Loginhopo"];


        $logedIn=0;
        $auth=false;

        //Inactive Counter
        if ($P02==1){
            header ("Location: error.php?Error=2&P01=".$P01."&P02=".$P02);
            exit ;
        }
        //Change LoginMethod 20/09/2006
        //In order to fullfill the SITA DHCP request

        //New Global Variable P01 - ComputerName
        //Format  ATHCKB001, ATHGBP21A
        //B28:GateB28:test:57.30.224.68 ATH2GBPB28
        //B28A:GateB28A:test:57.30.224.83 ATH2GTCB28

        //		CKB checkin
        //		JWS (finger gates)
        //		GTC (ta diplana apo ola)
        //		GBP (bus gate)


        $MyP01 = "";
        $MyType = "";
        $MyP01 = strtoupper(str_replace($Loginhopo, '', $P01));

        if (strstr($MyP01,"JWS")) {
            // It is a Gate (finger gates)
            $MyType = "GATE";
            $MyP01=	strtoupper(str_replace('JWS', '', $MyP01));
        } elseif (strstr($MyP01,"GTC")) {
            // It is a Gate (A)(bus gate besind)
            $MyType = "GATE";
            $MyP01=strtoupper(str_replace( 'GTC','', $MyP01));
            $MyP01=$MyP01."A";
        } elseif (strstr($MyP01,"GBP")) {
            // It is a Gate (bus gate)
            $MyType = "GATE";
            $MyP01=strtoupper(str_replace( 'GBP','',$MyP01));
        } elseif (strstr($MyP01,"CKB")) {
            // It is a ChekIn
            $MyType = "CHECKIN";
            $MyP01=strtoupper(str_replace( 'CKB','', $MyP01));
        }
        //		echo $logedIn."aa";


        // Read the entire file into the variable $file_contents

        //$filename = '/path/to/file.txt';
        $fp = fopen( $UserFilename, 'r' );
        $file_contents = fread( $fp, filesize( $UserFilename ) );
        fclose( $fp );

        // Place the individual lines from the file contents into an array.

        $lines = explode ( "\n", $file_contents );

        // Split each of the lines into a username and a password pair
        // and attempt to match them to $PHP_AUTH_USER and $PHP_AUTH_PW.

        foreach ( $lines as $line ) {

            list( $userid,$username, $password,$UserIP ) = explode( ':', $line );

            if ( ( strtolower($username) == strtolower($UserName) ) &&
                ( strtolower($password) == strtolower($Password) ) && strlen($UserName) > 0) {

                // A match is found, meaning the user is authenticated.
                // Stop the search.

                $auth = true;
                break;

            }
        }
        //		echo $UserName . "--" .$Password ;
        //exit;
        if ($auth )  {

            $Session["userid"]=$userid;
            $Session["username"]=strtoupper($username);
            $UserIP=$UserIP;

            //Check the type (Gate or Checkin) and parse the dtName
            //Check UserName with The Parameter

            $MyP01 = $MyType . $MyP01;

            if ($MyP01!=strtoupper($username)){
                if ($NoIp!="true") {
                    if ($UserIP!=$ClientIP){
                        header ("Location: error.php?Error=2&P01=".$P01."&P02=".$P02);
                        exit ;
                    }
                }

                //						header ("Location: error.php?Error=2&P01=".$P01); \
                //						exit ;
            }

            if (strstr($Session["username"],"GATE"))  {
                // It is a Gate
                $Session["Type"]="GATE";
                $Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
                $Session["Number"]=	strtoupper(str_replace('GATE', '', $Session["username"]));
            } else {
                // It is a ChekIn
                $Session["Type"]="CHECK-IN";
                $Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
                $Session["Number"]=strtoupper(str_replace( 'CHECKIN','', $Session["username"]));

            }

            $Session["P01"] = $P01;
            $Session["P02"] = $P02;

            //We Must check About The Blocking
            if (CheckBlock()==1) {
                header ("Location: error.php?Error=3&P01=".$P01."&P02=".$P02);
                exit ;
            }

            //$StatusFlag=Status();


            // We Must check for  allocation
            if (CheckAlloc()==0) {
                header ("Location: error.php?Error=4&P01=".$P01."&P02=".$P02);
                exit ;
            }

            // We must Check the Status Of the GateCheckin

            //if ($StatusFlag=="OPEN" &&) {
            //	header ("Location: error.php?Error=6&oldUrno=".$Session["Oldurno"]."&urno=".$Session["urno"]);
            //	exit ;
            //}
            $logedIn=1;

        } else {
            header ("Location: index.php?Error=1&P01=".$P01."&P02=".$P02);
            exit ;

        }// If End
        $GLOBALS["Session"] =$Session ;

        return $logedIn;
    }// End Function
    function logosAirDB() {
        //Global $db,$AirlineURNO,$hopo,$Session;
        $db = $GLOBALS["db"];
        $Session = $GLOBALS["Session"];
        $AirlineURNO = $GLOBALS["AirlineURNO"];
        $hopo = $GLOBALS["hopo"];


        if ($Session["Type"]=="GATE") {
            $tmp="G";
        } else {
            $tmp="C";
        }
        $cmdTempCommandText = "Select decode (trim(alc2),'' ,alc3,alc2) alc2 ,alc2 as alc,alc3,alfn  from alttab order by alfn";
        $rValues = Array();
        $rOutput = Array();
        $rSelected = Array();

        $rs=$db->Execute($cmdTempCommandText);
        //echo         $cmdTempCommandText;

        if (!$rs->EOF)  {
            if ($Session["PicName"]=="None") {
                $tmp=" selected";
                    $rSelected[0] = "None";
                }

            //echo '<option value="None" '.$tmp.'>None';
            $rValues[] = "None";
            $rOutput[] = "None";

            while (!$rs->EOF) {
                $tmp="";
                if ($Session["PicName"]==trim($rs->fields("alc2"))) {
                    $tmp=" selected";
                    $rSelected[0] = trim($rs->fields("alc2"));
                }
                $rValues[] = trim($rs->fields("alc2"));
                $rOutput[] = trim(substr($rs->fields("alfn"), 0, 41)) ."|".trim($rs->fields("alc2"));
               // echo '<option value="'.trim($rs->fields("alc2")).'" '.$tmp.'>'.trim(substr($rs->fields("alfn"), 0, 41)) ."|".trim($rs->fields("alc2"))."\n";
                $rs->MoveNext();
            }
        } else {
            //echo "<option >No Airlines Yet";
            $rValues[] = " ";
            $rOutput[] = "No Airlines Yet";

        }// If End
        if ($rs) $rs->Close();
        return Array($rValues,$rOutput,$rSelected);
        
    }// End Function
    function logosDB() {
        //Global $db,$AirlineURNO,$hopo,$Session;

        $db = $GLOBALS["db"];
        $Session = $GLOBALS["Session"];
        $AirlineURNO = $GLOBALS["AirlineURNO"];
        $hopo = $GLOBALS["hopo"];

        if ($Session["Type"]=="GATE") {
            $tmp="G";
        } else {
            $tmp="C";
        }
        $cmdTempCommandText = "Select distinct beme,code,remi,urno from fidtab where remt='".$tmp."' and beme<>' ' and hopo='$hopo' order by beme";
        $rValues = Array();
        $rOutput = Array();
        $rSelected = Array();

        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
            if ($Session["PicName"]=="None") {
                $tmp=" selected";
                $rSelected[0] = "None";
                }

            //echo '<option value="None" '.$tmp.'>None';
            $rValues[] = "None";
            $rOutput[] = "None";
            while (!$rs->EOF) {
                $tmp="";
                if ($Session["PicName"]==trim($rs->fields("code"))) {
                    //$tmp=" selected";
                    $rSelected[0] = trim($rs->fields("code"));
                }
                $rValues[] = trim($rs->fields("code"));
                $rOutput[] = trim(substr($rs->fields("beme"), 0, 41)) ."|".trim($rs->fields("code"));

                //echo '<option value="'.trim($rs->fields("code")).'" '.$tmp.'>'.trim(substr($rs->fields("beme"), 0, 41)) ."|".trim($rs->fields("code"))."\n";
                $rs->MoveNext();
            }
        } else {
            //echo "<option >No Airlines Yet";
            $rValues[] = " ";
            $rOutput[] = "No Airlines Yet";
        }// If End
        if ($rs) $rs->Close();
        return Array($rValues,$rOutput,$rSelected);
    }// End Function
    function logosDBflg($ALC2="empty") {
        //Global $db,$AirlineURNO,$Session,$DisplayLogoForSpecFlight;

        $db = $GLOBALS["db"];
        $Session = $GLOBALS["Session"];
        $AirlineURNO = $GLOBALS["AirlineURNO"];
        $DisplayLogoForSpecFlight = $GLOBALS["DisplayLogoForSpecFlight"];
        $logger = LoggerManager::getRootLogger('CuteIf');
        $logger->debug('FLDUse :'.$FLDUse);
        $AirlineURNOstr = Currentlogos(true);
         $tok = strtok($AirlineURNOstr,"|");
                $i=0;

                while ($tok) {

                    if ($i==0) {
                        $AirlineURNO=$tok;
                    }


                    $i++;
                    $tok = strtok("|");

                }

        

        $tmp1="";
        $tmp="";
        
        $rValues = Array();
        $rOutput = Array();
        $rSelected = Array();

        if ($Session["Type"]=="GATE") {
            $tmp="G";
        } else {
            $tmp="C";
        }
        //20081212 GFO
        //Changed to Display Logos For not Assigned Airlines

        if ($DisplayLogoForSpecFlight==true ) {
            $tmp1=" and ((alc2='".$Session["ALC2"]."' and alc3='".$Session["ALC3"]."' ) or (alc2=' ' and alc3=' ') )";
        }
        if ($Session["FldRows"]  >1 && $ALC2 == "empty") {
            $tmp1=" and alc2='--' ";
        }
        if ($ALC2!="empty" ) {
            $tmp1=" and (alc2='".$ALC2."' or alc2=' ') ";
        }

        $cmdTempCommandText = "Select lgfn,urno,lgsn from flgtab where type='".$tmp."' ".$tmp1." order by upper(lgfn)";
        $rs=$db->Execute($cmdTempCommandText);

        $logger->debug($cmdTempCommandText);
        if (!$rs->EOF)  {
            //echo '<option value="None" '.$tmp.'>None';
            $rValues[] = "None";
            $rOutput[] = "None";
            $rSelected[0] = "None";
            
            while (!$rs->EOF) {
                $tmp="";
                $logger->debug("Airlineurno :". $AirlineURNO . "--".trim($rs->fields("urno")));
                if ($AirlineURNO==trim($rs->fields("urno"))) {
                    $tmp=" selected";
                     $rSelected[0] = $rs->fields("urno").'|'.trim($rs->fields("lgsn"));
                    }
                     $rValues[] = $rs->fields("urno").'|'.trim($rs->fields("lgsn"));
                $rOutput[] = $rs->fields("lgfn") ." ".$rs->fields("lgsn");//trim(substr($rs->fields("lgfn"), 0, 41));

                //echo '<option value="'.$rs->fields("urno").'|'.trim($rs->fields("lgsn")).'" '.$tmp.'>'.trim(substr($rs->fields("lgfn"), 0, 41)) ."\n";
                $rs->MoveNext();
            }
        } else {
            //echo "<option >No Airlines Yet";

            if ($Session["FldRows"]  >1 && $ALC2 == "empty") {
                $rValues[] = " ";
                $rOutput[] = "Please Select a Flight First";

             } else {
                $rValues[] = " ";
                $rOutput[] = "No Airlines Yet";
             }

        }// If End
        if ($rs) $rs->Close();
        
        return Array($rValues,$rOutput,$rSelected);
    }// End Function
    function logosFile() {
        Global $logoRealPath,$logoExt,$Session;

        $logoRealPath = $GLOBALS["logoRealPath"];
        $logoExt = $GLOBALS["logoExt"];
        $Session = $GLOBALS["Session"];
        

        // Define the full path to your folder from root
        $file_count = 0;
        $path = $logoRealPath;
        $file_types = $logoExt;
        // echo $logoPath.'--'.$path;
        $rValues = Array();
        $rOutput = Array();
        $rSelected = Array();
        // Open the folder
        $dir_handle = opendir($path) or die("Unable to open $path");
        while (false !== ($filename = readdir($dir_handle))) {
            if ($filename != "." && $filename != ".." ) {
                 $new_filename = stristr ($filename, $logoExt);
                 //  echo $new_filename;
                 if ($new_filename== $file_types ) {
                    $files[] = $filename;
                 }
            }
        }
        // Loop through the files
        if (count($files)>0) {
            sort($files);
        }
        if ($Session["PicName"]=="None") {
            $tmp=" selected";
             $rSelected[0] = "None";

            }

        //echo '<option value="None" '.$tmp.'>None';
        //echo '<option value="None" '.$tmp.'>None';
            $rValues[] = "None";
            $rOutput[] = "None";
            if (count($files)>0) {
        foreach ($files as $file) {


            //$extension = file_type($file);
            $new_filename = stristr ($file, $logoExt);
          
           
            $new_filename_value = str_replace ($logoExt,'',$file);
            //if($file != '.' && $file != '..' && in_array($file, $file_types) == true){
               //   if($file != '.' && $file != '..' &&  strlen($new_filename_value)>0){
                $file_count++;
                $tmp="";

                if ($Session["PicName"]==$new_filename_value) {$tmp=" selected";}
                $rValues[] = $new_filename_value;
                $rOutput[] =trim(substr($new_filename_value, 0, 41));

                //echo '<option value="'.$new_filename_value.'" '.$tmp.'>'.trim(substr($new_filename_value, 0, 41)) ."\n";
           // }
        }
            }
        // Close
        closedir($dir_handle);
        return Array($rValues,$rOutput,$rSelected);
    }// End Function



} // End Class


?>
