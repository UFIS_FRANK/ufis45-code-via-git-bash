<?php

include("include/manager.php");
include("include/maininclude.php");

$logger = LoggerManager::getRootLogger('CuteIf');



$smarty = new Smarty;

$smarty->caching=0;
$smarty->compile_check =true;
$smarty->debugging = false;

//Header
$smarty->assign("PageTitle",$PageTitle);
$smarty->assign("CharSet",$CharSet);
$smarty->assign("ContenetExpires",$Cexpires);
$smarty->assign("RefreshRate",$RefreshRate);


//Status & Time
$smarty->assign("Type",$Session["Type"]);
$smarty->assign("TypeNumber",$Session["Number"]);

$smarty->assign("CurrentTime", CurrentTime());


$smarty->assign("RemarkGateOpen", "Gate Open" );
$smarty->assign("RemarkGateBoardingEx", "Boarding Expected" );
$smarty->assign("RemarkGateBoarding", "Boarding" );
$smarty->assign("RemarkGateFinalCall", "Final Call");
$smarty->assign("RemarkGateClose", "Gate Closed" );


//Check Permissions
$smarty->assign("logo_frame" ,$logo_frame);


//Logo Section
$LogoSelection =false;
$LogoSelectionStyle = "display: display";
$AirlineURNO = 0;

//In case we want to disable for Gate
//if (($Session["logoSelection"] == true && $FLZTABuse==true) || ($Session["logoSelection"] == true && $Session["Type"]!="GATE" && $Session["logoSelectionUser"] != false)) {
if (($Session["logoSelection"]) == true &&  $Session["logoSelectionUser"] == true) {
    $LogoSelection =true;
    $AirlineURNOstr = Currentlogos($FLZTABuse);
    $tok = strtok($AirlineURNOstr,"|");
                $i=0;

                while ($tok) {

                    if ($i==0) {
                        $AirlineURNO=$tok;
                        $Session["AirlineURNO"]=$tok;
                    }

                    if ($i==1) {
                        $PicName=$logoPath.$tok.".GIF";
                        $Session["PicName"]=$tok;
                    }

                    $i++;
                    $tok = strtok("|");

                }

    $logger->debug('AirlineURNO  :'.$AirlineURNO);
    
    $smarty->assign("LogopQuery",$pquery->form_remote_tag(array('url'=>'controller.php?task=logopreview','update'=>'#PreviewLogo')));

    $logofunction="logos".$logoSelectionType;
    $rArray = $login->$logofunction();

    $smarty->assign("LogoSelect_values",$rArray[0]);
    $smarty->assign("LogoSelect_output",$rArray[1]);
    $smarty->assign("LogoSelect_selected",$rArray[2]);

}
$smarty->assign("LogoSrc", LogoPreview($AirlineURNOstr,$_GET['Request']));
$smarty->assign("LogoSelection",$LogoSelection);

$logger->debug("LogoSelection ".$LogoSelection);
//Current Status
$logger->debug('Before Status  :'.$Session["STATUS"]);
$smarty->assign("Status",Status());
$logger->debug('After Status  :'.$Session["STATUS"]);

// DIsplay the UnclosedCounter Message 
$DisplayUnclosedMessage = 'false';
$tmpPreviousOpen  = "";
if ( ($Session["FldRows"]>1 && $Session["STATUS"]==1) || ($Session["OldCounterOpen"]==True && $Session["STATUS"]==1)  ) {
    $DisplayUnclosedMessage = true;
    $tmpPreviousOpen = $pquery->visual_effect("show","DisplayUnclosedMessage");
}
$smarty->assign("DisplayUnclosedMessage",$DisplayUnclosedMessage);
$logger->debug("DisplayUnclosedMessage ".$DisplayUnclosedMessage);
$logger->debug("Session_FldRows ".$Session["FldRows"]);
$logger->debug("Session_Status ".$Session["STATUS"]);
$logger->debug("Session_OldCounterOpen ".$Session["OldCounterOpen"]);



//PredefineRemarks
$smarty->assign("PredefinedSelection",false);
//if ( $remarks_file=="remarks.php") {
     if ($Session["PredefinedSelection"] == true &&  $Session["PredefinedSelectionUser"]==true)  {
         //if ($Session["PredefinedSelectionUser"]==true) {
            $rArray = PRemarks();

            $smarty->assign("PRemarkCode_values",$rArray[0]);
            $smarty->assign("PRemarkCode_output",$rArray[1]);
            $smarty->assign("PRemarkCode_selected",$rArray[2]);
            $smarty->assign("PredefinedSelection",true);
        // }
     }
     
//}
$logger->debug('remarks_file'. $remarks_file);
$logger->debug('logoSelection'. $Session["logoSelection"]);
$logger->debug('PredefinedSelection'. $Session["PredefinedSelection"]);

$logger->debug('logoSelectionUser'. $Session["logoSelectionUser"]);
$logger->debug('PredefinedSelectionUser'. $Session["PredefinedSelectionUser"]);

//FreeText
$smarty->assign("FreeTextSelection",$FreeTextSelection);
if ($FreeTextSelection==true && $remarks_file=="remarks.php") {
    $smarty->assign("FreeText1",FreeRemarks(1));
    $smarty->assign("FreeText2",FreeRemarks(2));

}

//Boarding Time 
$smarty->assign("BoardingTime",$enableBoardingTime);

$logger->debug("FreeTextSelection ".$FreeTextSelection);

//Buttons
//Gates
$tmpgate = "";
$tmpcheckin = "";
if ($Session["Type"]=="GATE") {
    //Init Phase
    $cpr = CurrentPRemarks();

    $gtoStyle = "";
    $boaStyle = "";
    $fncStyle = "";
    $gclStyle = "display: none";

    if ($cpr=="") {$clrStyle = "redbutton";} //background-color: red;
    if ($cpr==$RemarkGateOpen) {$gtoStyle = "redbutton";}
    if ($cpr==$RemarkGateBoardingEx) {$boaExStyle = "redbutton";}
    if ($cpr==$RemarkGateBoarding) {$boaStyle = "redbutton";}
    if ($cpr==$RemarkGateFinalCall) {$fncStyle = "redbutton";}
    if ($cpr==$RemarkGateClose) {$gclStyle = "redbutton";}

    $smarty->assign("gtoStyle",$gtoStyle);
    $smarty->assign("boaExStyle",$boaExStyle);
    $smarty->assign("boaStyle",$boaStyle);
    $smarty->assign("fncStyle",$fncStyle);

   
    $smarty->assign("mtf","jQuery(function($){\$(\"#GateBoardingExValue\").mask(\"99:99\");});");


    //$pquery->ID("#Btn_GateBoardingExValue",".mask(\"99:99\")");
    $smarty->assign("Btn_GateBoardingExValue",$Session["BOAE"]);
    
    $logger->debug("CRemark  ".$cpr);


    if ($cpr==$RemarkGateBoarding || $cpr==$RemarkGateFinalCall) {
        //Show Close Button
        $gclStyle = "";
        $tmpgate = $pquery->show("Btn_GateClose");
    }

    $smarty->assign("gclStyle",$gclStyle);


} else {
    //Checkins
    $bValue = "Close";
    $bValueOpen = "Open";
    $bValueTransmit = "Transmit";
    $logger->debug('Session["Button"]  :'.$Session["Button"]);
    if ($RButton == "Close" || $Session["Button"]=="Close") {
        $bValue = "Clear";
    }
    $smarty->assign("bValueTransmit",$bValueTransmit);
   
    
    if ($Session["STATUS"]==1 || $RButton == "Close" || $Session["Button"]=="Close") {
        //$bValueOpen = "Transmit";
        $logger->debug('Inside if for open CheckinCounter  :'.$Session["STATUS"]);
        $smarty->assign("bValue",$bValue);
        $tmpcheckin = $pquery->show("Checkin_BtnClose");
        $smarty->assign("btnhide","");
        $smarty->assign("btnTransmithide","");
        $smarty->assign("btnOpenhide","hide");
    } else {
        $logger->debug('Inside if for Close CheckinCounter  :'.$Session["STATUS"]);
        if ($Session["Button"]=="Close") {
            $bValue = "Clear";
            $tmpcheckin = $pquery->hide("Checkin_BtnOpen");
            $smarty->assign("bValue",$bValue);
            $logger->debug(' btnOpenhide 1 hide ');
            $smarty->assign("btnOpenhide","hide");
            $smarty->assign("btnTransmithide","hide");
            
            $tmpcheckin = $tmpcheckin.";\n".$pquery->show("Checkin_BtnClose");
            $smarty->assign("btnhide","");

        } else {
            $logger->debug('btn 1  :'.$Session["STATUS"]);
            $tmpcheckin = $pquery->hide("Checkin_BtnClose");
            $logger->debug(' btnhide hide ');
            $smarty->assign("btnhide","hide");
            $smarty->assign("btnTransmithide","hide");
        }
    }
    //Open is allowed only if the Now time is after the ckbs 
    if ($openCounterWithinScheduleTimes == true)  {
        $logger->debug('Inside if for open openCounterWithinScheduleTimes  :'.$openCounterWithinScheduleTimes);
        if ( isset($Session["CKICOpen"]) &&  $Session["CKICOpen"] == false)  {
            $logger->debug('Inside if for open Session["CKICOpen"]  :'.$Session["CKICOpen"]);
            $bValueOpen = "";
            $tmpcheckin = $pquery->hide("Checkin_BtnOpen");
            $logger->debug(' btnOpenhide 2 hide ');
            $smarty->assign("btnOpenhide","hide");
            $smarty->assign("btnTransmithide","hide");
        } else {
            if ($Session["Button"] == "Close") {
                $bValue = "Clear";
                $tmpcheckin = $pquery->hide("Checkin_BtnOpen");
                $smarty->assign("bValue", $bValue);
                $logger->debug(' btnOpenhide 1 hide ');
                $smarty->assign("btnOpenhide", "hide");
                $smarty->assign("btnTransmithide","hide");

                $tmpcheckin = $tmpcheckin . ";\n" . $pquery->show("Checkin_BtnClose");
                $smarty->assign("btnhide", "");
            } 
        }

    } else {
            $smarty->assign("btnOpenhide","");
            $smarty->assign("btnTransmithide","");
    }
    
    

    if (($Session["PredefinedSelectionUser"] == true || $FreeTextSelection == true) )   {
    } else {
        $logger->debug(' btnTransmithide 2 hide '.$Session["PredefinedSelectionUser"] ." -- " . $FreeTextSelection);
        $smarty->assign("btnTransmithide","hide");
    }
    
    $logger->debug(' bValueOpen='. $bValueOpen .', bValue= '. $bValue );
    $smarty->assign("bValueOpen",$bValueOpen);
    $pquery->hide("Btn_Transmit");
    
    
    
}

//Test Only
//$smarty->assign("pqeryEndJS1",$pquery->link_to_remote("Ajax Link",array('url'=>'controller.php?task=ajaxlinkc','update'=>'#idtoupdate2','success'=>'workflow("'.$Session["Type"].'");')));


//Bottom
//$Change_Frames = false;
$tmpBtn = "";
//if ($Session["Type"]!="GATE") {
//
//    if ($Session["PredefinedSelection"] == true &&
//        $Session["logoSelection"] ==true ) {
//        if ($Session["PredefinedSelectionUser"] != false &&
//            $Session["logoSelectionUser"] !=false ) {
//            if ($DisplayBoth_Logo_Remark==false) {
//                //hide the LogoSelection;
//
//                 $LogoSelectionStyle = "display: none";
//                 $Change_Frames = true;
//                 $tmpBtn = $pquery->show("Btn_Frame");
//
//                //		<td align="right" valign="top"><input type="Button" name="ButtonFrame"
//                //			value="Enable Logo" onClick="change_frameset()"></td>
//            }
//        }
//    }
//
//}
if (
    $Session["logoSelection"] == true &&  $Session["logoSelectionUser"]==true
    && $Session["PredefinedSelection"] == true &&  $Session["PredefinedSelectionUser"]==true
   )
   {
   if ($Change_Frames==true) {
                //hide the LogoSelection;
                $logger->debug('Inside Global_status  :'. $Session["Global_status"]);
                 if ($Session["Global_status"]=="" ) {
                     $Session["Global_status"] = "Logo";
                      
                 }
                 
                // $Change_Frames = true;
                if ($Session["Global_status"] == "Logo" ) {
                    $PredefinedSelectionStyle = "display: none";
                    $smarty->assign("btnTransmithide","hide");
                    $pquery->hide("Btn_Transmit");
                    $smarty->assign("Btn_FrameTitle","Enable Remarks");

                }
                if ($Session["Global_status"] == "Remarks" ) {
                    $LogoSelectionStyle = "display: none";
                     $smarty->assign("btnTransmithide","");
                     $pquery->show("Btn_Transmit");
                    $smarty->assign("Btn_FrameTitle","Enable Logo");
                }

                $tmpBtn = $pquery->show("Btn_Frame");


                //		<td align="right" valign="top"><input type="Button" name="ButtonFrame"
                //			value="Enable Logo" onClick="change_frameset()"></td>
            }
   }
//$tmpBtn = $pquery->visual_effect("show","Btn_Frame");

$smarty->assign("LogoSelectionStyle",$LogoSelectionStyle);
$smarty->assign("PredefinedSelectionStyle",$PredefinedSelectionStyle);

$smarty->assign("Change_Frames",$Change_Frames);
$logger->debug("Change_Frames ".$Change_Frames);



//JavaScript String
$tmp = "";
$tmp = $tmp.'<script type="text/javascript">';
$tmp = $tmp.'    $(document).ready(function() { ';
//$tmp = $tmp . $pquery->periodically_call_remote(array('url'=>'controller.php?task=status','update'=>'#Status','frequency'=>20));
$tmp = $tmp.  $pquery->periodically_call_remote(array('url'=>'controller.php?task=ctime','update'=>'#idtime','frequency'=>30));
$tmp = $tmp. ";\n";
if (len($tmpgate)>0) {
    $tmp = $tmp. $tmpgate.";\n" ;
}
if (strlen($tmpcheckin)>0) {
    $tmp = $tmp. $tmpcheckin.";\n" ;
}
if (strlen($tmpBtn)>0) {
    $tmp = $tmp. $tmpBtn.";\n" ;
}
if (strlen($tmpPreviousOpen)>0) {
$tmp = $tmp. $tmpPreviousOpen.";\n" ;
}
$tmp = $tmp.'    });';
$tmp = $tmp.'</script>';
$smarty->assign("pqeryEndJS",$tmp);




$smarty->display('cuteif_main.tpl');


//Safely close all appenders with...

LoggerManager::shutdown();


?>
