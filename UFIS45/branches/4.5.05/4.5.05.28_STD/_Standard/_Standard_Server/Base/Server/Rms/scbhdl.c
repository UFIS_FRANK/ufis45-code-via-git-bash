/********************************************************************************/
/*                                                                            	*/
/* ABB ACE/FC Program Skeleton                                                	*/
/*                                                                            	*/
/* Author         : APO,THO,RRO                                            	*/
/* Date           : 2000-2003                                                 	*/
/* Description    : this handler implements the account calculation		*/
/*                                                                            	*/
/* Update history : 16.05.2000 created.                                       	*/
/*		    .....							*/
/*                  2003: completely revised	    		 		*/
/********************************************************************************/
/*                                                                            	*/
/* source-code-control-system version string                                  	*/
#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/scbhdl.c 1.6 2004/12/09 22:17:20SGT rro Exp  $";
#endif /* _DEF_mks_version */

/* be carefule with strftime or similar functions !!!                         	*/
/*                                                                            	*/
/********************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "AATArray.h"
#include "cedatime.h"
#include "libccstr.h"

#ifdef _SUN_UNIX
#include <sys/timeb.h>
#endif

/******************************************************************************/
/* scriba error codes                                                         */
/******************************************************************************/
#define SCRIBA_NO_ERROR				0
#define SCRIBA_NO_BIN				1 
#define SCRIBA_NO_TXT				2
#define SCRIBA_LEX_ERROR			3
#define SCRIBA_SYNTAX_ERROR			4
#define SCRIBA_BUILD_ERROR			5
#define SCRIBA_NESTING_LIMIT_REACHED		6
/* and OpenACTIONRecordset error codes */
#define OPEN_NO_MORE_ARRAYS			-1	/* no more arrays available (more than MAX_ARRAY are open) */
#define OPEN_ERROR_CREATE			-2	/* could not create CEDAArray (lib-failure) */
#define OPEN_ERROR_DELETE			-3	/* could not delete partially created CEDAArray */
#define OPEN_ERROR_FILL				-4	/* could not fill CEDAArray */
#define OPEN_ERROR_TRIGGER_ACTION		-5	/* could not create section in 'action'-handler */
#define OPEN_AMBIGOUS_ARRAYNAME			-6	/* the array name is used */
#define OPEN_INVALID_KEY_FIELDS			-11	/* array does not contain key fields */
#define OPEN_INDEX_NAME_TOO_LARGE		-12	/* index name too long */
#define OPEN_ERROR_CREATE_INDEX			-13	/* error creating index */
#define OPEN_ARRAY_NAME_TOO_LARGE		-14	/* array name too long */
#define OPEN_TABLE_NAME_TOO_LARGE		-15	/* table name too long */
/* CreateActionSection error codes */
#define CREATE_ACTION_INVALID_PARAM		-7	/* invalid parameter */
#define CREATE_ACTION_NO_MORE_STRUCT		-8	/* MAX_ACTION_SECTIONs are in use */
#define CREATE_ACTION_TRIGGER_ERROR		-9	/* error in TriggerAction() */
#define CREATE_ACTION_SECTION_IN_USE		-10	/* a section with the same name exists (or should, at least) */

/******************************************************************************/
/* commands                                                                   */
/******************************************************************************/
#define CMD_MAKE_CFG_FILE		666 /* command to initiate creation of cfg-file */
#define CMD_CLEAR_OPEN_JOBS 		777 /* remove all open jobs from array and file */
#define CMD_ENABLE_EVENT_BUFFERING	778 /* suppress event processing and save events to disk */
#define CMD_DISABLE_EVENT_BUFFERING	779 /* re-enable event processing and process all saved events */
#define CMD_QUEUES_TO_HOLD		780 /* test setting queue to hold */
#define CMD_QUEUES_TO_GO		781 /* test setting queue to hold */
/******************************************************************************/
/* other defines                                                              */
/******************************************************************************/
#define MAX_ARRAY			40	/* maximum number of arrays */ 
#define MAX_INDEX			1	/* maximum number of indixes per array */ 
#define MAX_FIELDS			260	/* maximum number of fields in a record */ 
#define MAX_ACTION_SECTION		30	/* maximum number of dynamic commands from action */
#define MAX_FILENAME			512	/* maximum length of file name */
#define	MAX_ARRAY_NAME			12	/* maximum length of name for CEDAArray */
#define MAX_TABNAME			6	/* length of table name */
#define MAX_SQL_QUERY			512	/* maximum length of sql query */
#define MAX_FIELDLIST			2048	/* maximum length of field list for CEDAArray */
#define MAX_INDEX_NAME			24	/* maximum length of index name for CEDAArray (5 fields + 4*',') */
#define MAX_SCRIPTNAME			2048	/* maximum size of script names to execute */
#define MAX_EXTRA_BUF			2048	/* maximum size of the internal extra buffer (see WriteTemporaryField() for details) */
#define MAX_RECEIVER_DATA		512	/* size of buffer for messages to users/client applications */
#define MAX_TW_SIZE			64	/* size of TW... data */
#define MAX_CMD_SIZE			16	/* maximum size of the command of the last broadcast */
#define URNO_LENGTH			10	/* size of char buffer for single urno */
#define MAX_NEW_URNOS			100	/* maximum number of new urnos to reserve */
#define MAX_ACTION_SECTION_COMMAND	16	/* size of cmd for action section */
#define ACTION_REFILL			0x01	/* refill array if action-updated */
#define ACTION_PRE_SCRIPT		0x02	/* execute script before updating */
#define ACTION_POST_SCRIPT		0x04	/* execute script after updating */
#define MAX_XBS_COMMANDS		100	/* maximum number of configurable basic script */ 
						/* batches (see AddXBSCommand() for details) */
#define MAX_BASIC_ERROR_BUF		2048	/* size of basic interpreter error message buffer */
#define MAX_DATA_SIZE			4096	/* max. size of data block for xbs commands stored to the open jobs file */
#define MAX_OPEN_JOBS			100	/* maximum number of open jobs */ 
#define	TIME1_LATER_THAN_TIME2		1	/* return value of CompareTime() */
#define	TIME1_EQUALS_TIME2		0	/* return value of CompareTime() */
#define	TIME2_LATER_THAN_TIME1		-1	/* return value of CompareTime() */
#define XBS_EXECUTION_NOW		1	/* job has to be executed now */
#define XBS_EXECUTION_LATER		2	/* job has to be executed later but only one time */
#define XBS_EXECUTION_MONTHLY		3	/* job has to be repeatedly one time a month */
#define MAX_CACHED_FIELD_SIZE		128	/* maximum size of a single cached field */
#define MAX_CACHED_TABLES		100	/* maximum number of tables which can be cached into a string */
#define MAX_FIELD_NAME			12	/* maximum size of a field name in table */
#define MAX_BLOCKSIZE			9999	/* maximum size of a data block of a buffered event */
#define MAX_QUEUES_TO_HOLD		512	/* maximum length of list of queues to be set to hold when buffering events */

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;				/* The queue item pointer  */
static EVENT *prgEvent     = NULL;				/* The event pointer */
static ITEM  *prgOutItem   = NULL;				/* The queue item pointer for communicating with ACTION */
static EVENT *prgOutEvent  = NULL;				/* event pointer for outgoing broadcasts */
static int igItemLen = 0;					/* length of incoming item */
static int igInitOK = FALSE;
static char cgConfigFile[512];
static char pcgCfgFile[100];					/* path and name of config file */
static char cgInitScript[512];					/* path and name of initial executed script */
static BOOL bgSendRelAcc = FALSE;				/* Send RELACC broadcast or use old method via ACTION	*/	
static int igWaitForAnswer;					/* timeout for answer on broadcasts */
static int igRetryCreateAction;					/* number of trials to create a daynamic section in the 'action'-handler */
static int igQueOut;
static char pcgRecvName[64]; 					/* BC-Head used as WKS-Name */
static char pcgDestName[64]; 					/* BC-Head used as USR-Name */
static char pcgTwStart[MAX_TW_SIZE];  				/* BC-Head used as Stamp */
static char pcgTwEnd[MAX_TW_SIZE];  				/* BC-Head used as Stamp */  
static char *pcgSelKey = NULL;					/* pointer to the selKey of the last broadcast */
static char *pcgFields = NULL;					/* pointer to the field list of the last broadcast */
static char *pcgData = NULL;					/* pointer to the data-string of the last broadcast */
static char cgExtraFields[MAX_EXTRA_BUF];			/* pointer to an internal extra field list associated with the last broadcast */
static char cgExtraData[MAX_EXTRA_BUF];				/* pointer to an internal extra data string associated with the last broadcast */
static char *pcgOldData = NULL;					/* pointer to the old-data-string of the last broadcast */
static char pcgRcvData[MAX_RECEIVER_DATA] = "";			/* buffer for i/o to client */
static char *pcgZeroString = "\0";				/* that's where pcgOldData points to if there's nomold data */
static char pcgCmd[MAX_CMD_SIZE] = "";				/* buffer for the command of the last broadcast */
static char pcgNextScript[MAX_FILENAME] = "";			/* buffer for the command of the last broadcast */
static char pcgHopo[6] = "";					/* home airport */
/* open job data */
static char pcgOpenJobFileName[MAX_FILENAME];			/* file which stores open jobs to execute at future time, e.g. at 01:00 at night */
static int igHasOpenJobs = FALSE;				/* shows if there are open jobs to execute later on */
static char *pcgCrlf			= "\n";			/* carriage return for open job file */
static char *pcgDelim			= ",";			/* delimiter for open job file */
static struct tm sgOpenJobLastCheck;				/* time of last check, if there are any open jobs */
static int igCommitAfterEachScript;				/* if this parameter is set, commit will be called after each script execution, 
								   else after each event, which is much faster if many scripts write to db */
static int igBreakScriptListExecution;				/* when processing events and executing one script after another a script can */
								/* suppress further execution of other scripts in a script list (connected with */
								/* the current event) setting this int to 0 by calling the */
								/* basic function breakscriptlistexecution. this prevents checking the same */
								/* condition in every script (like 'is this an event sended by RELDPL?') leading */
								/* to immediate stop of processing the script. */
static int igXBSInfoLength;		/* if the info format changes, we only have to change it one time */
								/* FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC */
static int igNextJobToExecuteIndex;				/* index of next open job to execute */
static struct tm sgNextJobToExecute;				/* next time to execute an open job */
 
static long igMaxEventBuffer  = 0;				/* maximum disk space used to buffer events */
static int igEnableEventBuffering = 0;				/* do not process events but save them to disk? */
static char cgEventBuffer[MAX_DATA_SIZE];			/* buffer for creating events to store them to disk */
static char cgEventBufferFileName[MAX_FILENAME];		/* file name and path of event buffer file */
static FILE *phgEventBufferFile = NULL;				/* pointer to event buffer file handle */
static int igHasBufferedEvent = 0;				/* set to true if there are events in file added by script */
static char pcgQueuesToHold[MAX_QUEUES_TO_HOLD];		/* the queues spcified here will be set to 'HOLD' if events from file will be executed */
static int igProcessingBufferedEvents;				/* trigger to supress use of AddEventToFile() if events from file are just processed */

/* accounts	*/
static char pcgNativeAccounts[MAX_SCRIPTNAME];			/* list of native accounts	*/
static char pcgOfflineAccounts[MAX_SCRIPTNAME];			/* list of offline accounts	*/
static BOOL bgRecalcOfflineAccounts;				/* recalculation of offline accounts enabled / disabled	*/
static BOOL blQueAck = TRUE;					/* queue acknowledge necessary or not	*/

/* native script processing*/
typedef void (*NATIVESCRIPTFUNC)();
typedef struct	NativeScript
{
	const char*		pcmScriptName;
	NATIVESCRIPTFUNC	pcmScriptFunc;
	const char*		pcmCheckAccount;
}	NativeScript;

static void bas_init_scbhdl();  /* initializing process scbhdl */
static void bas_init_drr();     /* initializings before any (DRRTAB-)event */
static void bas_exit_drr();     /* ? */
static void bas_init_addstf();  /* adding employees to prefetch */
static void bas_init_delstf();  /* removing employees from prefetch */

static void bas_5();            /* free days in month */
static void bas_8();            /* free days in quarter */
static void bas_11();           /* free days in year */
static void bas_16();           /* number of trainings per month */
static void bas_17();           /* number of "normal" absences per month (without holiday, regular day off, ...) */
static void bas_18();           /* number of illnesses per month */
static void bas_26();           /* actual working hours of month */
static void bas_27();           /* working hours compensation */
static void bas_30();           /* holiday hours (reversible) */
static void bas_32();           /* (total) overtime hours */
static void bas_33();           /* absence compensation hours */
static void bas_37();           /* free saturdays in month */
static void bas_38();           /* free sundays in month */
static void bas_51();           /* internal compensation */
static void bas_60();           /* actual night working hours of month */
static void bas_61();           /* compensation of night working hours of month */
static void bas_dra();		/* daily roster absences support	*/

static void bas_init_xbshdl();  /* initializing process xbshdl */
static void bas_init_23();      /* initializing norm working hours */
static void bas_init_30();      /* initializing holiday hours */
static void bas_init_4();       /* initializing free days in month target */
static void bas_init_7();       /* initializing free days in quarter target */
static void bas_init_10();      /* initializing free days in year target */
static void bas_init_recalc();  /* initialization of recalculation */
static void bas_init_multiple();/* ? */

static NativeScript sgNativeScripts[] = {
	"init_scbhdl",	bas_init_scbhdl,	"",
	"init_drr",	bas_init_drr,		"",
	"exit_drr",	bas_exit_drr,		"",
	"5" ,		bas_5,			"",
	"8" ,		bas_8,			"",
	"11",		bas_11,			"",
	"14",		NULL,			"",
	"16",		bas_16,			"",
	"17",		bas_17,			"",
	"18",		bas_18,			"",
	"19",		NULL,			"",
	"21",		NULL,			"",
	"22",		NULL,			"",
	"22ACC",	NULL,			"",
	"26",		bas_26,			"",
	"27",		bas_27,			"",
	"28",		NULL,			"",
	"29",		NULL,			"",
	"30",		bas_30,			"",
	"31",		NULL,			"",
	"32",		bas_32,			"",
	"33",		bas_33,			"",
	"34",		NULL,			"",
	"35",		NULL,			"",
	"36",		NULL,			"",
	"36DRR",	NULL,			"",
	"37",		bas_37,			"",
	"38",		bas_38,			"",
	"39",		NULL,			"",
	"40",		NULL,			"",
	"42",		NULL,			"",
	"46",		NULL,			"",
	"49",		NULL,			"",
	"51",		bas_51,			"",
	"60",		bas_60,			"",
	"61",		bas_61,			"",
	"62",		NULL,			"",
	"acccor",	NULL,			"",
	"dra",		bas_dra,		"",
	"drs",		NULL,			"",
	"holredu",	NULL,			"",
	"salcomp",	NULL,			"",
	"worktime",	NULL,			"",
	"init_addstf",	bas_init_addstf,	"",
	"init_delstf",	bas_init_delstf,	"",
	"init_xbshdl",	bas_init_xbshdl,	"",
	"init_23",	bas_init_23,		"",
	"init_30",	bas_init_30,		"",
	"init_4",	bas_init_4,		"",
	"init_7",	bas_init_7,		"",
	"init_10",	bas_init_10,		"",
	"init_recalc",	bas_init_recalc,	"",
	"init_multiple",bas_init_multiple,	"",
	NULL,NULL,NULL,NULL,
	};

/*	daylight settings	*/
static char cgTdi1[24];
static char cgTdi2[24];
static char cgTich[24];
static int InitDaylightSettings();
static int GetRealTimeDiff (const char *pcpLocal1,const char *pcpLocal2, time_t *plpDiff);
static void LocalToUtc(char *pcpTime);

/*	temporary variable handling	*/
static HANDLE hTmpFields;
static HANDLE hTmpFieldsInd;
static BOOL InitTmpFields();

/*	account prefetch handling	*/
static HANDLE hAccPreFetch;
static HANDLE hAccPreFetchInd;
static const int silMaxUrnoSize = (ARR_MAXSELECTLEN - 128) / (10+2+1);
static BOOL InitAccountPrefetch();
static BOOL StfToAccPrefetch(const char *pcpStfuList,const char *pcpYear,BOOL bpAddStf);
static int AppendRecords(int ipRSIndex,const char* pcpWhere,BOOL blFirst);
static int OpenACCRecordsetEx(const char *pcpWhere);

/*	account script handling		*/
static HANDLE hAccScripts;
static HANDLE hAccScriptsInd;

static BOOL InitAccountScripts();
static NATIVESCRIPTFUNC FindNativeAccountScript(const char *pcpScriptName,BOOL *pbpOffline,char *pcpCheckAccount);

static BOOL IsBsd(const char *pcpBSDU);
static BOOL IsCodeRegFree(const char *pcpUrno);
static BOOL GetODATypes(const char *pcpUrno,BOOL *pbpIsRegFree,BOOL *pbpIsShiftChangeoverDay,BOOL *pbpIsTraining,BOOL *pbpIsCompensation,BOOL *pbpIsCompensationNight,BOOL *pbpIsHoliday,BOOL *pbpIsIllness);
static int  OpenACCRecordset(const char *pcpStfu,const char *pcpYear);

static BOOL MustHandleEvent(const char *pcpTableName,char *pcpCompare);

static int HandleRelDrr(char *pcpBCData);
static int HandleRelAcc(char *pcpSelKey,char *pcpFields,char *pcpData);

/******************************************************************************/
/* Global variables for database access                                       */
/******************************************************************************/
/* this struct stores all the info we need to create and hold a CEDAArray */
typedef struct Recordset {
	char cHopo[4];
	char cArrayName[MAX_ARRAY_NAME+1];	/* name of CEDAArray */
	char cTabName[MAX_TABNAME+1];		/* name o table */
	char cSelect[MAX_SQL_QUERY];		/* select SQL-statement */
	char cFieldList[MAX_FIELDLIST];		/* fields read */
	char cAddFieldList[1024];		/* user defined additional fields (not used yet) */
	char cAddFieldData[8000];		/* values of additional fields */
	char cIndexName[MAX_INDEX_NAME+1];	/* name of indices */
	char cPreUpdateScript[MAX_FILENAME];	/* if action-updated: script to be executed before update */
	char cPostUpdateScript[MAX_FILENAME];	/* if action-updated: script to be executed after update */

	long lRow;				/* index of current row pointed to by the array cursor  */
	long lRowCount;				/* total number of rows in CEDAArray */
	long lFieldLens[MAX_FIELDS];		/* lengths of fields in CEDAArray */
	long lAddFieldLens[MAX_FIELDS];		/* same for additional fields */
	long lFieldOffset[MAX_FIELDS];		/* offset of first char of field value in data string */
	long lMaxRecordSize;			/* max. size of a single record of specified CEDAArray */

	HANDLE hHandle;				/* handle of CEDAArray */
	HANDLE hIndexHandle;			/* handle of indexes */

	int iIsActionRecordset;			/* action-updated array? */
	int iIsReadOnly;			/* read only? */
	int iActionType;			/* if action-updated: bitmask of ACTION_REFILL & ACTION_PRE_SCRIPT & ACTION_POST_SCRIPT */
	int iAutoClose;				/* if set, this recordset is closed by Cleanup() after script execution */
	int iAutoDelete;			/* if set, this recordset is deleted by CloseRecordset */
	int iIsDeleted;				/* if set, this recordset is deleted !!!!! */
} Recordset;
/* all the recordsets we need */
static Recordset sgRecordsets[MAX_ARRAY];

/******************************************************************************/
/* Global variables for action sections                                       */
/******************************************************************************/
/* store the info about commands, which are sent by 'action' */
typedef struct ActionSection {
	char cCmd[MAX_ACTION_SECTION_COMMAND];
	char cScriptName[MAX_SCRIPTNAME];
	char *pNextScript;
	char cTabName[MAX_TABNAME+1];
	char cFieldList[MAX_FIELDLIST];
	int  iInUse;
} ActionSection;

/* all the ActionSections we need */
ActionSection sgActionSections[MAX_ACTION_SECTION];

/******************************************************************************/
/* Global variables for eXecute Basic Scripts commands                        */
/******************************************************************************/
/* stores the info about XBS commands (see AddXBSCommand() for details about XBS commands) */ 
typedef struct XBSCommand {
	char cCmd[13];
	char cScriptList[MAX_SCRIPTNAME];
	char *pNextScript;
	char cPath[MAX_SCRIPTNAME];
} XBSCommand;
/* mem for all the XBSCommands which are allowed */
XBSCommand sgXBSCommands[MAX_XBS_COMMANDS];

/******************************************************************************/
/* Global variables for caching tables                                        */
/******************************************************************************/
/* struct which stores cached tables */
typedef struct TableCache {
	char cTableName[MAX_TABNAME+1];
	char cActionCmd[MAX_TABNAME+3];
	char cKeyFieldName[MAX_FIELD_NAME+1];
	char cValueFieldName[MAX_FIELD_NAME+1];
	char *pCachedTableString;
	long lNoOfRecords;
	int	 iInUse;
} TableCache;
/* all the TableCache structs we need */
TableCache sgCachedTables[MAX_CACHED_TABLES];
static int igMaxCachedDbFieldSize;		/* this is the maximum size of cached field values. needed by ceda.c to allocate memory */

/******************************************************************************/
/* Global variables open job functionality                                    */
/******************************************************************************/
/* stores the info about open jobs */ 
typedef struct OpenJobs {
	char	cXBSCmd[13];				/* refers to XBSCommand[].cCmd */
	char 	*pcAdditionalInfo;			/* original events additional info buf */
	char	*pcEmplUrnoList;			/* original events list of employee urnos */
	char	cNextExecution[15];			/* next time of execution as string for the timer checking function */
	struct	tm sgNextExecution;			/* next time of execution as calculated in ExecuteOpenJob() */
	int		iExecutionMode;				/* if != 0 this job has to be executed each month at the day of month specified in <cDayAndTime> */
	int		iMonthOffset;				/* the month offset from current date (1 = FROM and TO are set to one month from now, -1 = set to last month) */
} OpenJobs;
/* mem for all the OpenJobs which are allowed */
OpenJobs sgOpenJobs[MAX_OPEN_JOBS];

static int	igBchdlModid = 1900;
static int	igActionModid = 7400; 

/******************************************************************************/
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
/******************************************************************************/

/******************************************************************************/
/* Functions which are available for basic                                    */
/******************************************************************************/
/* create a CEDAArray */
static int OpenRecordset(const char*,const char*,const char*,const char*,const char*,int,int); 
/* create a CEDAArray updated by ACTION-events */
static int OpenACTIONRecordset(const char*,const char*,const char*,const char*,const char*,int,const char*,const char*,const char*,int,int,int); 
/* find the first  record by key beginning to search from the first record */
static int FindFirstRecordByIndex(int,char*);
/* find the next record by key beginning to search from the first record */
static int FindNextRecordByIndex(int,char*);
/* find a record by key beginning to search from the first record */
static int FindRecordByIndex(int,char*,int);
/* find a record with given key value and return specified field values */
static int FindFieldValueByIndex(int,char*,char*);
static int FindFieldValueByIndex_old(int,char*,char*);
/* get the first record */
static int GetFirstRecord(int,char*);
/* get the next record */
static int GetNextRecord(int,char*);
/* destroy a CEDAArray */
static void CloseRecordset(int,int);
/* get the maximum size of a single record */
static int GetMaxRecordSize(int);
/* get the number of records in a CEDAArray */
static int GetRecordCount(int);
/* get the max. size of a certain field */
static int GetMaxFieldSize(int, char*);
/* get the value of a certain field */
static int GetFieldValue(int, char*, char*);
/* locate a field in a CEDAArray's field list */
static int GetFieldIndex(int,char*);
/* insert a record */
static int InsertRecord(int,char*);
/* update the current record */
static int UpdateRecord(int,char*,char*);
/* delete the current record */
static int DeleteRecord(int);
/* get a free urno for inserting records */
static int GetNextUrno(char*);

/*********************************************************************************/
/*** Note: if additional GetLast...() functions will be implemented in future ****/
/*** the data these functions pass to basic scripts has to be saved to disk ******/
/*** when buffering events, else it will be lost when processing a saved event ***/
/*** and therewith execute a script which might call the new GetLast...() ********/
/*** function. All data blocks which are accessible by basic scripts via a *******/
/*** GetLast...() function have to be saved to disk and restored from disk when **/
/*** using the event buffering mechanism. ****************************************/
/*** Events are saved to disk in the functions AddEventToFile() so these *********/
/*** functions have to be updated if additional GetLast...() functions are *******/
/*** implemented. ****************************************************************/
/*********************************************************************************/
/* get the SelKey stored in the last broadcast */
static char* GetLastSelKey(void);
/* get the field list of the last broadcast */
static char* GetLastFieldList(void);
/* get the data-string of the last broadcast */
static char* GetLastData(void);
/* get the old-data-string of the last broadcast */
static char* GetLastOldData(void);
/* get the command of the last broadcast */
static char* GetLastCommand(void);
/* search for an array by name */
static int GetArrayIndex(const char*);
/* create a new dynamic section in 'action'-handler */
static int CreateActionSection(const char*,const char*,const char*,const char*,const char*,int);
/* locate the position of a single field in the given fieldlist and store its value from the data string */
static int GetFieldValueFromDataString(char*,char*,char*,char*);
/* locate the position of a single field in a given mono blocked field list and store its value from the data string */
static int GetFieldValueFromMonoBlockedDataString(const char* pcpField,const char* pcpFieldList,const char* pcpData, char* pcpFieldValue);
/* set next script to execute imediately after executing of current script */
static void SetNextScript(char*);
/* get home airport */
static int GetHopo(char*);
/* add a XBSCommand */
static int AddXBSCommand(const char*,const char*,const char*);
/* add an extra field+fieldvalue to the extra buffer */
static int WriteTemporaryField(char*,char*);
/* read an extra field+fieldvalue from the extra buffer */
static int ReadTemporaryField(char*,char*);
/* print out time stamps. use this function for basic performance tests. */
static void PrintTimeStamp(int);
/* set return buffer, so scripts can return a result string to the client application which sended an 'XSCR' command */
static int SetReceiverBuffer(char*);
/* get the field value from a cached table */
static int GetCachedDbValue(char*,char*,char*,char*,char*);
/* return the max. size of chached field */
static int GetMaxCachedFieldSize(void);
/* stop further execution of scripts from scriptlist */
static void BreakScriptListExecution(void);
/* store the current event in file */
static int AddEventToFile(char*,char*,char*,char*,char*,char*);
/******************************************************************************/
/* End of functions which are available for basic                             */
/******************************************************************************/

/* get a single item from a list */
static int ExtractItemFromDataString(int,char*,char*,char,int);
/* extract script names from comma delimited list of action section struct */
static int GetNextActionScript(int,char*);
/* get the next script to execute from XBSCommand */
static int GetNextXBSScript(int,char*);
/* close recordsets opened by script */
static void Cleanup(void);
/* check the given command string for one of our CEDAArray's names and return the index, if found */
static int CheckCommandAndGetArrayIndex(char*);
/* check the given command string for one of our <sgActionSections[].cCmd> and return the index, if found */
static int CheckCommandAndGetActionStructIndex(char*);
/* execute a script */
static int ExecuteScript(char*,char*);
/* read the configuration file and restore internal parameters */
static int ReadConfig(char*);
/* create the configuration file and save internal parameters */
static int MakeCfgFile(void);
/* special initialization */
static int 	Init_scbhdl(void);
/* initialize global data for accessing database */
static void	InitGlobalArrayVars(void);
/* initialize global data for dynamic sections in the 'action'-handler */
static void InitActionSections(void);
/* reset global data for dynamic sections in the 'action'-handler */
static void DeleteActionSections(void);
/* convert error code to string for debug output */
static char* CedaArrayErrorCodeToString(int);
/* get the index of the first free array */
static int	FindFreeArrayIndex(void);
/* initialize/reset the internal data of a single array */
static void InitArrayData(int);
/* locate the position of a given field in the fieldlist */
static int GetFieldIndex(int,char*);
/* delete a dynamic */
static int DeleteQueue(int,char*);
/* delete dynamic section in 'action'-handler which refers to an open recordset */
static int DeleteArrayActionSection(int);
/* delete dynamic section in 'action'-handler */
static int DeleteActionSection(int);
/* find a free struct to create a new dynamic section in 'action'-handler */
static int GetFreeActionSection(void);
/* add dynamic action events */
static int  TriggerAction(char*,char*,char*);
/*static int TriggerAction(char*,char*,char*,char*,int,BOOL);*/
/* check the queue <ipModId> for messages */
static int CheckQueue(int,ITEM**);
/* wait <ipTimeout> seconds for a broadcast on the queue <ipModId> */
static int WaitAndCheckQueue(int,int,ITEM**);
/* check if there is a dynamic 'action'-section with the given name */
static int IsSectionNameUsed(const char*);
/* reset all members of an action section struct */
static void InitActionSectionStruct(int);
/* initialize global data for XBSCommands */
static void InitXBSCommands(void);
/* search a command in the XBSCommand structs */
static int CheckCommandAndGetXBSCommandIndex(char*);
/* get the index of the next free XBSCommand in global data */
static int FindFreeXBSCommandIndex(const char*);
/**********************************************************/
/* table caching prototypes *******************************/
/**********************************************************/
/* initialize structs for caching tables in strings */
static void InitTableCacheStructs(void);
/* read special tables into single string to speed up access */
static int CacheTable(int,char*,char*,char*);
/* re-initialize all structs for caching tables in strings */
static void ResetTableCacheStructs(void);
/* re-initialize a single struct for caching tables in strings */
static void ResetTableCacheStruct(int);
/* extract field value from cached table string with given key value */
static int ExtractCachedDbValue(char*,char*,char*);
/* check if an action event is relevant for a cached table */
static int CheckCommandAndGetCachedTableIndex(char*);
/* re-read a cached table */
static int RefreshCachedTable(int ipIndex);
/**********************************************************/
/* open job functionality prototypes **********************/
/**********************************************************/
/* initialize array of open jobs */
static void InitOpenJobs(void);
/* get the index of a free array struct */
static int GetFreeOpenJobStruct(void);
/* remove an open job from queue by re-initializing all struct members and releasing all memory */
static void FreeOpenJob(int);
/* re-read open jobs from disk */
static int RestoreOpenJobs(void);
/* read a single open job data block from opened file */
static int GetNextOpenJobFromFile(FILE*,char*);
/* re-initialize array of open jobs */
static void ResetOpenJobs(void);
/* add an open job to the queue / array of open jobs */
static int AddOpenJob(char*,char*,char*,char*);
/* add an open job to the array of open jobs */
static int AddOpenJob2Array(char*,char*,char*,char*);
/* add one month to a time struct (struct tm) */
static int AddMonth(struct tm *,int);
/* remove all open jobs from queue, array and file (used by sendqueuemsg) */
static void ClearOpenJobs(void);
/* calculate the next time of execution of a monthly executed job */
static void SetOpenJobNextExecutionTime(int);
/* save open jobs to disk */
static int WriteOpenJobFile(void);
/* compare two time structs */
static int CompareTime(struct tm, struct tm);
/* execute an open job */
static int ExecuteOpenJob(int);
/* find out which open job has to be executed next */
static int SetNextOpenJob(void);
/* check, if time has come to execute the next open job in queue */
static int CheckTimer(void);
/* remove one single open job from the queue */
static int RemoveSingleOpenJob(int,char*);
/* get the info string of an open job */
static int GetSingleJobInfoString(int,char*,char*,char*);
/**********************************************************/
/* event buffering functionality prototypes ***************/
/**********************************************************/
/* read a buffered event from file */
static int ReadEventFromFile(void);
/* event buffering: read and process all buffered events from file */
static int ProcessAllBufferedEvents(void);
/* set queues to hold or start */
static int SetQueueState(int);

static int	Reset(void);                    /* Reset program          */
static void	Terminate(int ipSleep);         /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static int	HandleData(void);               /* Handles event data     */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/

static BOOL SetAccAction(int ipAccHandle,BOOL bpEnable);
static int SendRelaccBc(char *pcpSelection,char *pcpFields,char *pcpData);
static int SendBc(char *pcpComand,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpData,char *pcpWksName);
static int MyNewToolsSendSql(int ipRouteID,BC_HEAD *prpBchd,CMDBLK *prpCmdblk,char *pcpSelection,char *pcpFields,char *pcpData);

static int DumpArray(Recordset *prpArray);

/******************************************************************************/
/* Extern Function prototypes	                                              */
/******************************************************************************/
extern int GetNoOfElements(char *s, char c);
extern int GetNextDataItem(char *pcpResult,char **pcpInput,char *pcpDel,char *pcpDef,char *pcpTrim);
extern int CountToDelim(char *pcpInput,int ipNum,char cpDelim);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int FindItemInList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
extern int GetWord(char *pcpResult, char *pcpInput,int ipNum);
extern int CedaDateToTimeStruct(struct tm *prpTimeStruct, char *pcpCedaDate);
extern int TimeStructToCedaDate( char *pcpCedaDate,struct tm *prpTimeStruct);
extern int AddSecondsToCEDATime(char *,long,int);
/****************************************************************************************/
/****************************************************************************************/

/* flag for executing binary code */
#define BAFLG_BINARY		0x00000001
#define BAFLG_MAKE_BINARY	0x00000002
#define BAFLG_EXECUTE_BINARY	0x00000004

/* compiled basic script includes	*/
#include "Scbhdl/bas_include.c"
#include "Scbhdl/bas_init_scbhdl.c"
#include "Scbhdl/bas_init_drr.c"
#include "Scbhdl/bas_exit_drr.c"
#include "Scbhdl/bas_5.c"
#include "Scbhdl/bas_8.c"
#include "Scbhdl/bas_11.c"
#include "Scbhdl/bas_16.c"
#include "Scbhdl/bas_17.c"
#include "Scbhdl/bas_18.c"
#include "Scbhdl/bas_26.c"
#include "Scbhdl/bas_27.c"
#include "Scbhdl/bas_30.c"
#include "Scbhdl/bas_32.c"
#include "Scbhdl/bas_33.c"
#include "Scbhdl/bas_37.c"
#include "Scbhdl/bas_38.c"
#include "Scbhdl/bas_51.c"
#include "Scbhdl/bas_60.c"
#include "Scbhdl/bas_61.c"
#include "Scbhdl/bas_dra.c"

#include "Scbhdl/bas_init_xbshdl.c"
#include "Scbhdl/bas_init_23.c"
#include "Scbhdl/bas_init_30.c"
#include "Scbhdl/bas_init_4.c"
#include "Scbhdl/bas_init_7.c"
#include "Scbhdl/bas_init_10.c"
#include "Scbhdl/bas_init_recalc.c"
#include "Scbhdl/bas_init_multiple.c"

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;			/* General initialization	*/
	dbg(TRACE,"MAIN: version <%s>",mks_version);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			check_ret(ilRC);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */
	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
	dbg(TRACE,"MAIN: Module Name = '%s'",mod_name);
	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);

	/*	sprintf(cgConfigFile,"%s/scbhdl",getenv("BIN_PATH"));*/
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_scbhdl();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_scbhdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(10);
	}/* end of if */
	dbg(TRACE,"MAIN: initializing OK");
	ilCnt = 0;
	for(;;)
	{
		/* do we have to check a timer? */
		if (igHasOpenJobs)
		{
			/* yes -> check message queue but don't wait */
			ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		}
		else
		{
			/* no -> wait for event */
/*			dbg(DEBUG,"MAIN: calling que(QUE_GETBIG,...), waiting for event...");*/
			ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		}
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
		if( ilRC == RC_SUCCESS )
		{
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRC != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
					HandleQueErr(ilRC);
				} /* fi */
				Terminate(10);
				break;
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRC != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
					HandleQueErr(ilRC);
				} /* fi */
				Terminate(10);
				break;
			case	CMD_MAKE_CFG_FILE:
				/* Create new Config file in CFG_PATH  */
				MakeCfgFile();
				break;
			case	CMD_CLEAR_OPEN_JOBS:
				/* command to clear the list of open jobs */
				ClearOpenJobs();
				break;
			case	CMD_ENABLE_EVENT_BUFFERING:
				/* stop processing of events and save them to disk instead */
				if (igMaxEventBuffer >= 0)
				{
					dbg(TRACE,"MAIN line <%d>: event buffering to file '%s' enabled!",__LINE__,cgEventBufferFileName);
					igEnableEventBuffering = 1;
				}
				else
				{
					dbg(TRACE,"MAIN line <%d>: event buffering can not be enabled, check config file (MAX_EVENT_BUFFER == -1)!!!",__LINE__);
					igEnableEventBuffering = 0;
				}
				break;
			case	CMD_DISABLE_EVENT_BUFFERING:
				/* re-enable processing of events and first processed all events saved to file before */
				ilRC = ProcessAllBufferedEvents();
				igEnableEventBuffering = 0;
				break;
			case	CMD_QUEUES_TO_HOLD:
				SetQueueState(QUE_HOLD);
				break;
			case	CMD_QUEUES_TO_GO:
				SetQueueState(QUE_GO);
				break;
			case	RESET		:
				ilRC = Reset();
				break;

			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;

			case	TRACE_ON : /* = 7 */
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF : /* = 8 */
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

			if (blQueAck)
			{
				/* Acknowledge the item */
				dbg(TRACE,"MAIN: sending QUE_ACK...");
				ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRC != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
					HandleQueErr(ilRC);
				} /* fi */
			}
			else
			{
				dbg(TRACE,"MAIN: queue acknowledge not necessary !");
				blQueAck = TRUE;
			}
		} /* end if( ilRC == RC_SUCCESS ) */
		else 
		{
			/* Handle queuing errors (except no messages because of que(...,no wait), see above) */
			if (ilRC != QUE_E_NOMSG)
			{
				dbg(TRACE,"MAIN: queuing error (code <%d>)!!!",ilRC);
				HandleQueErr(ilRC);
			}
		} /* end else */
		/* check open job timer now */
		if (CheckTimer())
		{
			/* time to execute open jobs */
			ExecuteOpenJob(igNextJobToExecuteIndex);
		}
	} /* end for */
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_scbhdl()
{
	int ilRc = RC_SUCCESS;			/* Return code */
	int ilCount = 0;			/* for profiling timer initailization */

	/* now reading from configfile or from database */
	igInitOK = TRUE;

	/* initialize trigger for breaking script execution loop */
	igBreakScriptListExecution = 0;

	/* initialize trigger suppressing AddEventToFile() */
	igProcessingBufferedEvents = 0;

	/* if set to 1, commit is called after each script instead of each event */
	/* (which can cause multiple scripts being executed, each of it writing to the database). */
	/* Calling commit after each script is more secure but very much slower than calling after a whole event. */
	igCommitAfterEachScript = 0;

	/* initialize the internal CEDAArray management arrays */
	InitGlobalArrayVars();

	/* initialize the internal 'action' vars */
	InitActionSections();

	/* initialize global data for XBSCommands */
	InitXBSCommands();

	/* initialize structs for caching tables */
	InitTableCacheStructs();

	/* initialize CedaArray interface, max. MAX_ARRAY arrays with max. MAX_INDEX indices per array */
	CEDAArrayInitialize(MAX_ARRAY,MAX_INDEX);

	/* read config file */
	sprintf(pcgCfgFile,"%s/%s.cfg", getenv("CFG_PATH"),mod_name);
	ReadConfig(pcgCfgFile);

	/* initialize temporary fields */
	InitTmpFields();

	/* initialize native account scripts */
	InitAccountScripts();

	/* initialize daylight settings	*/
	InitDaylightSettings();

	/* initialize account prefetch */
	InitAccountPrefetch();

	/* execute init script */
	dbg(DEBUG,"Init_scbhdl(): calling ExecuteScript(cgInitScript)...");
	if (*cgInitScript != '\0') 
	{
		ExecuteScript(cgInitScript,NULL);
	}

	/* initialize xbs info string length */
	igXBSInfoLength = strlen("YYYYMMDD,YYYYMMDD,X,HHMM,DD,V00,YYYYMMDDHHMM");

	/* initialize open job array */
	InitOpenJobs();

	/* check, if there are open jobs to do (look for file /ceda/conf/openjobs.xbs) */
	RestoreOpenJobs();

	/* initialize file bufferig data */
	sprintf(cgEventBufferFileName,"%s/events.%s", getenv("CFG_PATH"),mod_name);

	if (!strcmp(mod_name, "scbhdl"))
	{
		ilRc = TriggerAction("RELDRR", "", "SBC" );
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"Init_scbhdl: TriggerAction <RELDRR> failed <%d>",ilRc);
		else
			dbg(DEBUG,"Init_scbhdl: TriggerAction <RELDRR> OK" );
			
		ilRc = TriggerAction("RELACC", "", "SBC" );
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"Init_scbhdl: TriggerAction <RELACC> failed <%d>",ilRc);
		else
			dbg(DEBUG,"Init_scbhdl: TriggerAction <RELACC> OK" );
			
	}
	else
	{
		dbg(DEBUG,"Init_scbhdl: mod_name is <%s>",mod_name);
	}

	/* set all queues given in <pcgQueuesToHold> to go in case we set them to hold prior and then crashed without setting them back to go */
	SetQueueState(QUE_GO);

	return(ilRc);
} /* end of Init_scbhdl() */

/******************************************************************************/
/* Initialize global data for accessing database.                             */
/******************************************************************************/
static void InitGlobalArrayVars()
{
	int ilLoop;
	/* iterate thru each possible array's data */
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		InitArrayData(ilLoop);
	}
} /* end of InitGlobalArrayVars() */

/******************************************************************************/
/* Initialize global data for dynamic sections in the 'action'-handler.       */
/******************************************************************************/
static void InitActionSections()
{
	int ilLoop;
	/* iterate thru the data */
	for (ilLoop = 0; ilLoop < MAX_ACTION_SECTION; ilLoop += 1)
	{
		InitActionSectionStruct(ilLoop);
	}
} /* end of InitActionSections() */

/******************************************************************************/
/* Initialize a single struct of an action section                            */
/******************************************************************************/
static void InitActionSectionStruct(int ipIndex)
{
	/* check the index */
	if ((ipIndex < 0) || (ipIndex >= MAX_ACTION_SECTION))
	{
		/* error: invalid index */
		dbg(DEBUG,"InitActionSectionStruct: invalid index <%d>!!!",ipIndex);
		return;
	}

	/* reset all struct members */
	strcpy(sgActionSections[ipIndex].cCmd,"");
	strcpy(sgActionSections[ipIndex].cScriptName,"");
	strcpy(sgActionSections[ipIndex].cTabName,"");
	strcpy(sgActionSections[ipIndex].cFieldList,"");
	sgActionSections[ipIndex].iInUse = 0;
	sgActionSections[ipIndex].pNextScript = sgActionSections[ipIndex].cScriptName;
} /* end of InitActionSections() */

/******************************************************************************/
/* Create a dynamic section in the 'action'-handler.                          */
/******************************************************************************/
static int CreateActionSection(const char* pcpSndCmd, const char* pcpScriptName,
							   const char* pcpTabName, const char* pcpFieldList,
							   const char* pcpSectionCmds, int ipIgnoreEmptyFields)
{
	/* index of free struct */
	int ilIndex;
	/* retry counter */
	int ilRetry = 1;
	/* retrun value of function call */
	int ilRc = RC_FAILURE;
	/* for debug output */
	char clScriptName[MAX_SCRIPTNAME];
	int ilCount = 1;
	BOOL blUseDefault = strcmp(pcpTabName,"ACCTAB") == 0;

	/* parameters have to be valid */
	if ((pcpSndCmd == NULL) || (pcpScriptName == NULL) || 
		(pcpTabName == NULL) || (pcpFieldList == NULL))
	{
		/* invalid parameters */
		dbg(TRACE,"CreateActionSection: invalid param!!!");
		return CREATE_ACTION_INVALID_PARAM;
	}

	/* make sure there is not a section with the given name already */
	if (IsSectionNameUsed(pcpSndCmd))
	{
		/* this section is in use */
		dbg(TRACE,"CreateActionSection: section <%s_%s> is in use!!!",mod_name,pcpSndCmd);
		return CREATE_ACTION_SECTION_IN_USE;
	}

	/* find a free struct */
	if ((ilIndex = GetFreeActionSection()) < 0)
	{
		/* no free struct found */
		dbg(TRACE,"CreateActionSection: MAX_ACTION_SECTIONS are in use!!!");
		return CREATE_ACTION_NO_MORE_STRUCT;
	}

	/* store the action info before calling TriggerAction(), in case TriggerAction() 
	   fails AFTER having created the section and we need the info to cleanup what has 
	   been created to the point of failure */
	sgActionSections[ilIndex].iInUse = 1;
	strcpy(sgActionSections[ilIndex].cCmd,pcpSndCmd);
	strcpy(sgActionSections[ilIndex].cScriptName,pcpScriptName);
	strcpy(sgActionSections[ilIndex].cTabName,pcpTabName);
	strcpy(sgActionSections[ilIndex].cFieldList,pcpFieldList);
	sgActionSections[ilIndex].pNextScript = sgActionSections[ilIndex].cScriptName;

	/* debug output */
	while (GetNextActionScript(ilIndex,clScriptName))
	{
		dbg(TRACE,"CreateActionSection: script no. %d: '%s'",ilCount,clScriptName);
		ilCount += 1;
	}
	/* re-init */
	sgActionSections[ilIndex].pNextScript = sgActionSections[ilIndex].cScriptName;

	/* now create the dynamic section in the 'action'-handler */
	for (ilRetry = 1; ilRetry <= igRetryCreateAction && (ilRc != RC_SUCCESS); ilRetry += 1)
	{
		dbg(DEBUG,"CreateActionSection: calling TriggerAction() the %d. time...",ilRetry);
		if ((ilRc = TriggerAction(sgActionSections[ilIndex].cTabName,sgActionSections[ilIndex].cFieldList,NULL)) == RC_SUCCESS)
		{
			/* success -> terminate */
			return 0;
		}
	}
	/* if we get here, an error creating the section occurred -> cleanup and terminate function */
	InitActionSectionStruct(ilIndex);
	/* terminate with error code */
	return CREATE_ACTION_TRIGGER_ERROR;
} /* end of CreateActionSection() */

/******************************************************************************/
/* Check, if there is (or should be) a section in the 'action'-handler,       */
/* which has the name '<mod_name>_<pcpName>'.                                 */
/******************************************************************************/
static int IsSectionNameUsed(const char *pcpName)
{
	int ilLoop;

	/* first check the name of any ActionRecordset (which is used as action command as well) */
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		if ((sgRecordsets[ilLoop].iIsActionRecordset == 1) && 
			(strcmp(sgRecordsets[ilLoop].cArrayName,pcpName) == 0))
		{
			/* there should be a section with the given name -> don't create a second one */
			return 1;
		}
	}

	/* second check other sections allready created */
	for (ilLoop = 0; ilLoop < MAX_ACTION_SECTION; ilLoop += 1)
	{
		if ((sgActionSections[ilLoop].iInUse == 1) && 
			(strcmp(sgActionSections[ilLoop].cCmd,pcpName) == 0))
		{
			/* there should be a section with the given name -> don't create a second one */
			return 1;
		}
	}

	/* third check other sections allready created */
 	for (ilLoop = 0; ilLoop < MAX_CACHED_TABLES; ilLoop += 1)
	{
		if (sgCachedTables[ilLoop].iInUse == 1) 
		{
			if (strcmp(sgCachedTables[ilLoop].cActionCmd,pcpName) == 0)
			{
				/* there should be a section with the given name -> don't create a second one */
				return 1;
			}
		}
		else
		{
			/* from here there won't be any used structs -> break now */
			break;
		}
	}

	/* name not found -> is ok, return false */
	return 0;
}

/******************************************************************************/
/* Find a free struct for a dynamic section.                                  */
/******************************************************************************/
static int GetFreeActionSection()
{
	/* index of struct */
	int ilLoop;
	/* iterate thru the data to find a free struct */
	for (ilLoop = 0; ilLoop < MAX_ACTION_SECTION; ilLoop += 1)
	{
		if (sgActionSections[ilLoop].iInUse == 0)
		{
			/* found a free struct */
/*			dbg(DEBUG,"GetFreeActionSection: free struct for dynamic section at index <%d>",ilLoop);*/
			return ilLoop;
		}
	}
	/* no free struct found */
	dbg(TRACE,"GetFreeActionSection: no free struct to create a dynamic section!!!");
	return -1;
} /* end of GetFreeActionSection() */

/******************************************************************************/
/* Reset global data for dynamic sections in the 'action'-handler.            */
/******************************************************************************/
static void DeleteActionSections()
{
	int ilLoop;
	/* iterate thru the data */
	for (ilLoop = 0; ilLoop < MAX_ACTION_SECTION; ilLoop += 1)
	{
		/* delete and reset the section */
		DeleteActionSection(ilLoop);
	}
} /* end of DeleteActionSections() */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	/* ToDo: test this command */

	int	ilRC = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;						/* loop variable */

	dbg(TRACE,"Reset: now resetting");

	/* close any open recordset (CEDAArray) */
	dbg(TRACE,"Reset: closing recordsets...");
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		if (sgRecordsets[ilLoop].hHandle != -1)
		{
			/* destroy and reinitialize the array */
			CloseRecordset(ilLoop,1);
		}
	}
	/* commit now, if necessary */
	if (!igCommitAfterEachScript)
	{
		dbg(TRACE,"Reset: calling commit_work()...");
		commit_work();
	}

	/* delete action sections */
	dbg(TRACE,"Reset: deleting action sections...");
/*	DeleteActionSections();*/
	/* clear XBSCommands */
	dbg(TRACE,"Reset: removing XBSCommands...");
	InitXBSCommands();
	/* reset cache table structs */
	ResetTableCacheStructs();
	/* re-initialization */
	dbg(TRACE,"Reset: reinitializing...");
	/* read config file */
	sprintf(pcgCfgFile,"%s/%s.cfg", getenv("CFG_PATH"),mod_name);
	ReadConfig(pcgCfgFile);

	/* execute init script */
	if (*cgInitScript != '\0') ExecuteScript(cgInitScript,NULL);

	/* remove all open jobs */
	ResetOpenJobs();

	return ilRC;

} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	int ilLoop;

	/* close any open recordset (CEDAArray) */
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		if (sgRecordsets[ilLoop].hHandle != -1)
		{
			/* destroy the array */
			CloseRecordset(ilLoop,1);
		}
	}
	/* commit now, if necessary */
	if (igCommitAfterEachScript == 0)
	{
		dbg(TRACE,"Terminate: calling commit_work()...");
		commit_work();
	}

	/* delete dynamically created sections */
/*	DeleteActionSections();*/

	/* delete XBSCommands */
	InitXBSCommands();

	/* remove all open jobs to clear memory */
	ResetOpenJobs();

	fclose(outp);

	/* unset SIGCHLD ! DB-Child will terminate ! */
	logoff();
	dbg(TRACE,"Terminate: now leaving ...");

	/* wait for at least 1 second(s). Why??? I don't know!!! Ask MCU! */
	sleep(ipSleep < 1 ? 1 : ipSleep);

	exit(0);

} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
	switch(pipSig)
	{
	default	:
		Terminate(10);
		break;
	} /* end of switch */
	exit(0);

} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;

	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;

	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;

			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;

			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;

			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(10);
				break;

			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				Terminate(10);
				break;
			case	RESET		:
				ilRC = Reset();
				break;

			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
		ilRC = Init_scbhdl();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"Init_scbhdl: init failed!");
		} /* end of if */
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	BC_HEAD *prlBchd = NULL;
	CMDBLK  *prlCmdblk = NULL;
	char    pclHost[16] = "";
	char    pclTable[16] = "";
	char    pclChkCmd[16] = "";
	int	ilIndex = -1; /* index of CEDAArray, if command is sent by 'action' */
	char	clScriptName[MAX_SCRIPTNAME]; /* buffer for next script to execute from script list of action struct */
	char	pclStartInfo[80];
	int	ilOpenJobIndex;

	dbg(TRACE,"================= %s EVENT BEGIN ================",mod_name);
	/* Queue-id of event sender */
	igQueOut = prgEvent->originator;

	/* get broadcast header */
	prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));

	/* Save Global Elements of BC_HEAD */
	memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name)+1));
	strcpy(pclHost,prlBchd->orig_name);                        		/* Host          */
	strcpy(pcgDestName,prlBchd->dest_name);                    		/* UserLoginName */
	strncpy(pcgRecvName, prlBchd->recv_name,sizeof(prlBchd->recv_name));	/* Workstation   */

	/* get command block  */
	prlCmdblk = (CMDBLK *)((char *)prlBchd->data);

	/* Save Global Elements of CMDBLK */
	strcpy(pcgTwStart,prlCmdblk->tw_start);
	strcpy(pcgTwEnd,prlCmdblk->tw_end);

	/*  3 Strings in CMDBLK -> data  */   
	pcgSelKey = (char *)prlCmdblk->data;
	pcgFields = (char *)pcgSelKey + strlen(pcgSelKey) + 1;
	pcgData = (char *)pcgFields + strlen(pcgFields) + 1;
	/* search for '\n' in data block. If found, it marks the beginning of the 'old' data block, i.e. the records data before it was updated */
	pcgOldData = strstr(pcgData,"\n");
	if (pcgOldData != NULL)
	{
		/* old data found -> replace marker ('\n') with 0x00 to zero-terminate new data block */
		*pcgOldData = 0x00;
		/* set pointer position to beginning of old data block */
		pcgOldData++;
	} /* end if */
	else
	{
		/* no old data found */
		pcgOldData = pcgZeroString;
	} /* end else */

	strcpy (pclTable, prlCmdblk->obj_name);		/*  table name  */
	strcpy (pcgCmd, prlCmdblk->command);		/*  actual command  */
	/* eliminate table name from command */

	DebugPrintBchead(TRACE,prlBchd);
	DebugPrintCmdblk(TRACE,prlCmdblk);

	/* initialize extra buffer */
	memset(cgExtraFields,0x00,MAX_EXTRA_BUF);
	memset(cgExtraData,0x00,MAX_EXTRA_BUF);

	/* performance test */
	PrintTimeStamp(1);
	dbg(TRACE,"<igCommitAfterEachScript> = <%d>",igCommitAfterEachScript);

	bgRecalcOfflineAccounts = FALSE;
	WriteTemporaryField("IsRecalc","0");
	WriteTemporaryField("RecvName",pcgRecvName);

	/*  COMPARE COMAND AND GOTO FUNCTION */
	/*---------------------------------------------------------------------------------------------------------------*/
	/*- Command XSCR??? ---------------------------------------------------------------------------------------------*/
	/*- This command is used by client applications on windows machines. We send back an 			      ---*/
	/*- answer to the sender of this command. -----------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------*/
	if (!strcmp(pcgCmd,"XSCR" ))
	{
		/* command EXSCR found -> a client wants us to execute a script */
		dbg(TRACE,"Command XSCR received, executing script '%s'",pcgSelKey);
		/* execute script */
		ExecuteScript(pcgSelKey,pcgData);
        	/* send answer to client */
		(void) tools_send_info_flag(igQueOut,0, pcgDestName, "", pcgRecvName,
									"", "", pcgTwStart, pcgTwEnd, pcgCmd,
									pclTable,pcgSelKey,pcgFields,pcgRcvData,0);
	}
	/*---------------------------------------------------------------------------------------------------------------*/
	/*- XBSCommand??? -----------------------------------------------------------------------------------------------*/
	/*- Check all internal XBSCommand structs if <cCmd> matches the XBSCommand which is given by <pcgTwStart>. ------*/
	/*- If so, we received this command from a client application like the InitAccount-Tool. Like the ActionSection -*/
	/*- functionality we have to execute one or more scripts. -------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------*/
	else if ((!strcmp(pcgCmd,"XBS")) || (!strcmp(pcgCmd,"XBS2")))
	{
		/* search the XBSCommand structs which one has to be used */
		if ((ilIndex = CheckCommandAndGetXBSCommandIndex(pcgTwStart)) != -1)
		{
			/* check job start info */
			if ((GetFieldValueFromDataString("MODE",pcgFields,pcgData,pclStartInfo) <= 0) || 
				(atoi(pclStartInfo) == XBS_EXECUTION_NOW))
			{
				/* execute now */ 
				dbg(DEBUG, "XBSCommand '%s' received",pclTable);
				dbg(DEBUG, "script list is: <%s>",sgXBSCommands[ilIndex].cScriptList);
				dbg(DEBUG, "path is:		<%s>",sgXBSCommands[ilIndex].cPath);
				/* signal ok to user */
				strcpy(pcgRcvData,"OK");
				/* send answer to client */
				(void) tools_send_info_flag(igQueOut,0, pcgDestName, "", pcgRecvName,
											"", "", pcgTwStart, pcgTwEnd, pcgCmd,
											pclTable,pcgSelKey,pcgFields,pcgRcvData,0);
				if (strcmp(pcgTwStart,"RECALC") == 0)
				{
					blQueAck = FALSE;
					bgRecalcOfflineAccounts = TRUE;
					WriteTemporaryField("IsRecalc","1");
					/* Acknowledge the item */
					dbg(TRACE,"MAIN: sending QUE_ACK...");
					ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
					if( ilRC != RC_SUCCESS ) 
					{
						/* handle que_ack error */
						dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
						HandleQueErr(ilRC);
					} /* fi */
				}
				else if (strcmp(pcgTwStart,"INIT_MUL") == 0)
				{
					blQueAck = FALSE;
					bgRecalcOfflineAccounts = FALSE;
					WriteTemporaryField("IsRecalc","0");
					/* Acknowledge the item */
					dbg(TRACE,"MAIN: sending QUE_ACK...");
					ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
					if( ilRC != RC_SUCCESS ) 
					{
						/* handle que_ack error */
						dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
						HandleQueErr(ilRC);
					} /* fi */
				}
				else
				{
					bgRecalcOfflineAccounts = FALSE;
					WriteTemporaryField("IsRecalc","0");
				}

				/* execute next script in script list of action section */
				while (GetNextXBSScript(ilIndex,clScriptName))
				{
					dbg(TRACE,"executing script '%s'",clScriptName);
					ExecuteScript(clScriptName,NULL);
				}

				/* reset next-script-to-execute-pointer */
				sgXBSCommands[ilIndex].pNextScript = sgXBSCommands[ilIndex].cScriptList;
				bgRecalcOfflineAccounts = FALSE;
				WriteTemporaryField("IsRecalc","0");
			}
			else
			{
				/* add to queue, function writes result string to <pcgRcvData> */
				/* ('OK' or error message) */
				AddOpenJob(pcgTwStart,pcgData,pcgSelKey,pcgRcvData);
				/* send answer to client */
				(void) tools_send_info_flag(igQueOut,0, pcgDestName, "", pcgRecvName,
											"", "", pcgTwStart, pcgTwEnd, pcgCmd,
											pclTable,pcgSelKey,pcgFields,pcgRcvData,0);
			}
		}
		else
		{
			dbg(TRACE, "HandleData(): unknown XBSCommand found ('%s')",pcgTwStart); 
			/* copy result/error message to client data block */
			sprintf(pcgRcvData,"command '%s' not found",pcgTwStart);
			/* send answer to client */
			(void) tools_send_info_flag(igQueOut,0, pcgDestName, "", pcgRecvName,
										"", "", pcgTwStart, pcgTwEnd, pcgCmd,
										pclTable,pcgSelKey,pcgFields,pcgRcvData,0);
		}
	}
	/*---------------------------------------------------------------------------------------------------------------*/
	/*- command OJC (Open Job Control)?------------------------------------------------------------------------------*/
	/*- Check the subcommand and remove one or all jobs, show a single or all job states. ---------------------------*/
	/*- The show commands force a dbg() print as well as returning the info in the return buffer. -------------------*/
	/*---------------------------------------------------------------------------------------------------------------*/
	else if (!strcmp(pcgCmd,"OJC" ))
	{
		dbg(DEBUG, "open job control command received, subcommand is '%s'",pclTable);
		/* check subcommand */
		/* REMOVE_SINGLE_JOB */
		if (!strcmp(pclTable,"RSJ"))
		{
			/* remove a single open job */
			RemoveSingleOpenJob(atoi(pcgSelKey),pcgRcvData);
		}
		/* REMOVE_ALL_JOBS */
		else if (!strcmp(pclTable,"RAJ"))
		{
			/* remove all open jobs */
			ClearOpenJobs();
			/* signal success to user */
			strcpy(pcgRcvData,"ok");
			dbg(TRACE, "HandleData(): command OJC subcmd. RMVA executed (all open jobs removed)"); 
		}
		/* GET_JOB_INFO */
		else if (!strcmp(pclTable,"GJI"))
		{
			/* Return a single job's info string to client (FROM,TO,MODE,etc.). 
			   If no index is specified, the first valid job will be returned. 
			   Additionally the next valid job's index (if there is one) will be 
			   delivered, so the client can request all job's info strings in a loop. */
			ilIndex = atoi(pcgSelKey);
			GetSingleJobInfoString(ilIndex,pcgRcvData,pcgSelKey,pcgTwStart);
		}
		/* ALL_JOB_INFO_2_DEBUG */ 
		else if (!strcmp(pclTable,"AJI2D"))
		{
			/* show all info strings of all jobs in the debug file */
			for (ilOpenJobIndex = 0; ilOpenJobIndex < MAX_OPEN_JOBS; ilOpenJobIndex++)
			{
				/* print job info to debug file if valid */
				if ((*sgOpenJobs[ilOpenJobIndex].cXBSCmd != '\0') && 
					(sgOpenJobs[ilOpenJobIndex].pcAdditionalInfo != NULL) &&
					(sgOpenJobs[ilOpenJobIndex].pcEmplUrnoList != NULL)) 
				{
					dbg(TRACE,"----------------------------------------------------");
					dbg(TRACE,"<%.2d>: '%s'",ilOpenJobIndex,sgOpenJobs[ilOpenJobIndex].cXBSCmd);
					dbg(TRACE,"info='%s'",sgOpenJobs[ilOpenJobIndex].pcAdditionalInfo);
					dbg(TRACE,"execute at %s",asctime(&sgOpenJobs[ilOpenJobIndex].sgNextExecution));
				}
			}
			if ((igNextJobToExecuteIndex != -1) && (igHasOpenJobs))
			{
				dbg(TRACE,"----------------------------------------------------");
				dbg(TRACE,"next job: no.%.2d,'%s', execution: %s",igNextJobToExecuteIndex,sgOpenJobs[igNextJobToExecuteIndex].cXBSCmd,asctime(&sgNextJobToExecute));
			}
		}
		/* ALL_JOBS_FULL_2_DEBUG */
		else if (!strcmp(pclTable,"AJFI2D"))
		{
			/* show all info strings of all jobs in the debug file */
			for (ilOpenJobIndex = 0; ilOpenJobIndex < MAX_OPEN_JOBS; ilOpenJobIndex++)
			{
				/* print job info to debug file if valid */
				if ((*sgOpenJobs[ilOpenJobIndex].cXBSCmd != '\0') && 
					(sgOpenJobs[ilOpenJobIndex].pcAdditionalInfo != NULL) &&
					(sgOpenJobs[ilOpenJobIndex].pcEmplUrnoList != NULL)) 
				{
					dbg(TRACE,"----------------------------------------------------");
					dbg(TRACE,"open job at index <%d>, command='%s'",ilOpenJobIndex,sgOpenJobs[ilOpenJobIndex].cXBSCmd);
					dbg(TRACE,"----------------------------------------------------");
					dbg(TRACE,"info:  ",sgOpenJobs[ilOpenJobIndex].pcAdditionalInfo);
					dbg(TRACE,"urnos: ",sgOpenJobs[ilOpenJobIndex].pcEmplUrnoList);
				}
			}
		}

		/* send answer to client */
		(void) tools_send_info_flag(igQueOut,0, pcgDestName, "", pcgRecvName,
									"", "", pcgTwStart, pcgTwEnd, pcgCmd,
									pclTable,pcgSelKey,pcgFields,pcgRcvData,0);
	}
	else if (!strcmp(pcgCmd,"SBC" ))
	{
		if (!strcmp (pclTable,"RELDRR"))
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);

			/* handle the event */
			HandleRelDrr (pcgData);

			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
				HandleQueErr(ilRC);
			} /* fi */
		}
		else if (!strcmp (pclTable,"RELACC"))
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);

			/* handle the event */
			HandleRelAcc (pcgSelKey,pcgFields,pcgData);

			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				dbg(TRACE,"MAIN: error sending QUE_ACK (code <%d>)!!!",ilRC);
				HandleQueErr(ilRC);
			} /* fi */
		}
	}

	/*---------------------------------------------------------------------------------------------------------------*/
	/*-  APO 14.05.2001 do not process table events sended by ACTION which originally came from us                   */
	/*---------------------------------------------------------------------------------------------------------------*/
	/*- dynamic CEDAArray Command??? --------------------------------------------------------------------------------*/
	/*- Check all dynamic CEDAArrays if the array name matches this command. If so we received this command from ----*/
	/*- the ACTION handler and have to update the CEDAArray because there had been a database action on the ---------*/
	/*- table the CEDAArray refers to. ------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------*/
	else if (prgEvent->originator != mod_id)
	{
		char clCompare[256];
		BOOL blHandleEvent = MustHandleEvent(pclTable,clCompare);

	 	if ((ilIndex = CheckCommandAndGetArrayIndex(clCompare)) != -1)
	 	{
			dbg(DEBUG, "ACTION sent command for CEDAArray <%s>",sgRecordsets[ilIndex].cArrayName); 
			/* do action */
			if ((sgRecordsets[ilIndex].iActionType & ACTION_PRE_SCRIPT == ACTION_PRE_SCRIPT) &&
				strcmp(sgRecordsets[ilIndex].cPreUpdateScript,""))
			{
				/* execute script before updating CEDAArray */
				dbg(DEBUG,"executing pre-update script '%s'",sgRecordsets[ilIndex].cPreUpdateScript);
				ExecuteScript(sgRecordsets[ilIndex].cPreUpdateScript,NULL);
			}

			if (blHandleEvent)	/* handle automatic event update for our ceda array's	*/
			{
				dbg(DEBUG,"CEDAArrayEventUpdate");
				ilRC = CEDAArrayEventUpdate(prgEvent);
				if (ilRC != RC_SUCCESS)	/* means update on record not found in our array */
				{
					dbg(DEBUG,"ERROR: CEDAArrayEventUpdate() failed with <ilRC> = <%d> for table <%s>!",ilRC,pclTable);
				}
			}

			if ((sgRecordsets[ilIndex].iActionType & ACTION_POST_SCRIPT == ACTION_POST_SCRIPT) &&
				strcmp(sgRecordsets[ilIndex].cPostUpdateScript,""))
			{
				/* execute script after updating CEDAArray */
				dbg(DEBUG,"executing post-update script '%s'",sgRecordsets[ilIndex].cPostUpdateScript);
				ExecuteScript(sgRecordsets[ilIndex].cPostUpdateScript,NULL);
			}
		}
		/*---------------------------------------------------------------------------------------------------------------*/
		/*- Action Section Command??? -----------------------------------------------------------------------------------*/
		/*- Check all internal ActionSection structs if the section name matches the command. If so, we received this ---*/
		/*- command from the ACTION handler and have to execute the script(s) which are specified in the ActionSection. -*/
		/*---------------------------------------------------------------------------------------------------------------*/
		else 
		{
			if (blHandleEvent)	/* handle automatic event update for our ceda array's */
			{
				dbg(DEBUG,"CEDAArrayEventUpdate");
				ilRC = CEDAArrayEventUpdate(prgEvent);
				if (ilRC != RC_SUCCESS)	/* means update on record not found in our array */
				{
					dbg(DEBUG,"ERROR: CEDAArrayEventUpdate() failed with <ilRC> = <%d> for table <%s>!",ilRC,pclTable);
				}
			}

			if ((ilIndex = CheckCommandAndGetActionStructIndex(pclTable)) != -1)
			{
				dbg(DEBUG, "ACTION sent command for section <%s_%s>",mod_name,sgActionSections[ilIndex].cTabName); 

				/* execute next script in script list of action section */
				while (GetNextActionScript(ilIndex,clScriptName))
				{
					dbg(TRACE,"executing script '%s'",clScriptName);
					ExecuteScript(clScriptName,NULL);
				}
				/* reset next-script-to-execute-pointer */
				sgActionSections[ilIndex].pNextScript = sgActionSections[ilIndex].cScriptName;
			}
			/*---------------------------------------------------------------------------------------------------------------*/
			/*- Update Cached Table  Command??? -----------------------------------------------------------------------------*/
			/*- Check all internal CachedTable structs if the command matches. If so, we received this command from the -----*/
			/*- ACTION handler and have to update this cached table. --------------------------------------------------------*/
			/*---------------------------------------------------------------------------------------------------------------*/
			else if ((ilIndex = CheckCommandAndGetCachedTableIndex(clCompare)) != -1)
			{
				dbg(DEBUG, "ACTION sent command for cached table <%s_%s>",mod_name,sgCachedTables[ilIndex].cActionCmd); 
				/* re-read table */
				RefreshCachedTable(ilIndex);
			}
			else
			{
				dbg(TRACE, "unknown command found"); 
			}
		}
	}
	/*---------------------------------------------------------------------------------------------------------------*/
	/*- Action Section Command??? -----------------------------------------------------------------------------------*/
	/*- Check all internal ActionSection structs if the section name matches the command. If so, we received this ---*/
	/*- command from the ACTION handler and have to execute the script(s) which are specified in the ActionSection. -*/
	/*---------------------------------------------------------------------------------------------------------------*/
	else 
	{
		if ((ilIndex = CheckCommandAndGetActionStructIndex(pclTable)) != -1)
		{
			dbg(DEBUG, "ACTION sent command for table <%s_%s>",mod_name,sgActionSections[ilIndex].cTabName); 

			/* execute next script in script list of action section */
			while (GetNextActionScript(ilIndex,clScriptName))
			{
				dbg(TRACE,"executing script '%s'",clScriptName);
				ExecuteScript(clScriptName,NULL);
			}
			/* reset next-script-to-execute-pointer */
			sgActionSections[ilIndex].pNextScript = sgActionSections[ilIndex].cScriptName;
		}
		/*---------------------------------------------------------------------------------------------------------------*/
		/*- Update Cached Table  Command??? -----------------------------------------------------------------------------*/
		/*- Check all internal CachedTable structs if the command matches. If so, we received this command from the -----*/
		/*- ACTION handler and have to update this cached table. --------------------------------------------------------*/
		/*---------------------------------------------------------------------------------------------------------------*/
		else if ((ilIndex = CheckCommandAndGetCachedTableIndex(pclTable)) != -1)
		{
			dbg(DEBUG, "ACTION sent command for cached table <%s_%s>",mod_name,sgCachedTables[ilIndex].cActionCmd); 
			/* re-read table */
			RefreshCachedTable(ilIndex);
		}
		else
		{
			dbg(TRACE, "unknown command found"); 
		}
	}


	/* now call commit_work() if it was suppressed in CloseRecordset() */
	if (!igCommitAfterEachScript)
	{
		dbg(TRACE,"HandleData: calling commit_work()...");
		commit_work();
	}

	/* performance test */
	PrintTimeStamp(0);

	dbg(TRACE,"================= %s EVENT END ================",mod_name);
                      
	/* if pseudo events have been generated by basic script execute them now */
	if (igHasBufferedEvent)
	{
		dbg(TRACE,"HandleData() line <%d>: events have been generated by script, calling ProcessAllBufferedEvents()",__LINE__);
		ProcessAllBufferedEvents();
	}

	return ilRC;
} /* end of HandleData */

/******************************************************************************/
/* Get the error description of a CEDAArray-Errorcode.                        */
/******************************************************************************/
static char* CedaArrayErrorCodeToString(int ipErrorCode)
{
	switch (ipErrorCode)
	{
	case RC_SUCCESS:
		return "RC_SUCCESS - no error";
	case RC_FAILURE:
		return "RC_FAILURE - general error";
	case RC_INVALID:
		return "RC_INVALID - invalid pointer";
	case RC_USED:
		return "RC_DELETED - data element exists";
	case RC_DELETED:
		return "RC_DELETED - data element was deleted";
	case RC_DUPLICATE:
		return "RC_DUPLICATE - duplicate name for array or key";
	case RC_NOMEM:
		return "RC_NOMEM - no memory";
	case RC_NOTINITIALIZED: 
		return "RC_NOTINITIALIZED - function not initialized";
	case RC_NODATA:
		return "RC_NODATA - record is not containing data";
	case RC_ROWDELETED: 
		return "RC_ROWDELETED - record was deleted";
	case RC_NOTFOUND: 
		return "RC_NOTFOUND - data element not found";
	default: 
		return "RC_??? - unknown error";
	}
} /* end of CedaArrayErrorCodeToString */

/******************************************************************************/
/* Get the index of the first free array.				      */
/******************************************************************************/
static int FindFreeArrayIndex()
{
	int ilLoop;
	/* look for a free array handle */
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		if (sgRecordsets[ilLoop].hHandle == -1)
		{
			/* free array handle found, return it's index */
			return ilLoop;
		}
	}
	dbg(DEBUG,"FindFreeArrayIndex: no free handle found!!!");
	/* no free array handle available */
	return -1;
} /* end of FindFreeArrayIndex */

/******************************************************************************/
/* Initialize/reset the internal data of a single array.                      */
/******************************************************************************/
static void InitArrayData(int ipRSIndex)
{
	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY))
	{
		/* invalid index */
		dbg(DEBUG,"InitArrayData: invalid index <%d>!!!",ipRSIndex);
		return;
	}

	/* reset data */
	strcpy(sgRecordsets[ipRSIndex].cHopo,"");
	strcpy(sgRecordsets[ipRSIndex].cArrayName,"");
	strcpy(sgRecordsets[ipRSIndex].cTabName,"");
	strcpy(sgRecordsets[ipRSIndex].cSelect,"");
	strcpy(sgRecordsets[ipRSIndex].cFieldList,"");
	strcpy(sgRecordsets[ipRSIndex].cAddFieldList,"");
	strcpy(sgRecordsets[ipRSIndex].cAddFieldData,"");
	strcpy(sgRecordsets[ipRSIndex].cIndexName,"");
	strcpy(sgRecordsets[ipRSIndex].cPostUpdateScript,"");
	strcpy(sgRecordsets[ipRSIndex].cPreUpdateScript,"");
	sgRecordsets[ipRSIndex].lRow = ARR_FIRST;
	sgRecordsets[ipRSIndex].lRowCount = 0;
	sgRecordsets[ipRSIndex].hHandle = -1;
	sgRecordsets[ipRSIndex].hIndexHandle = -1;
	sgRecordsets[ipRSIndex].lMaxRecordSize = 0;
	sgRecordsets[ipRSIndex].iIsActionRecordset = 0;
	sgRecordsets[ipRSIndex].iIsReadOnly = 0;
	sgRecordsets[ipRSIndex].iActionType = 0;
	sgRecordsets[ipRSIndex].iAutoClose  = 0;
	sgRecordsets[ipRSIndex].iAutoDelete = 1;
	sgRecordsets[ipRSIndex].iIsDeleted  = 0;
} /* end of InitArrayData*/

/******************************************************************************/
/* Find the given fieldname in the CEDAArrays fieldlist and return the        */
/* position (= zero based index)                                              */
/******************************************************************************/
static int GetFieldIndex(int ipRSIndex, char* pcpFieldName)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* first position of occurance of substring */
	char *pclFoundAt;
	/* index in char array */
	int ilResult;
	/* count of commas (= index of field in field list) */
	int ilFieldIndex = 0;
	/* counter */
	int ilCount = 0;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetFieldIndex: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* check if there's any field in field list at all */
	if (!strcmp(sgRecordsets[ipRSIndex].cFieldList,""))
	{
		/* field list is empty */
		dbg(DEBUG,"GetFieldIndex: field list is empty!!!");
		return -1;
	}

	/* search for the field */
	if ((pclFoundAt = strstr(sgRecordsets[ipRSIndex].cFieldList,pcpFieldName)) != NULL)
	{
		/* absolute position of field name in field list */
		ilResult = pclFoundAt - sgRecordsets[ipRSIndex].cFieldList;
		/* count the commas found up to this position */
		while (ilCount <= ilResult)
		{
			if (sgRecordsets[ipRSIndex].cFieldList[ilCount] == ',')
			{
				/* next field found */
				ilFieldIndex += 1;
			}
			/* check next character */
			ilCount += 1;
		}
		/* return the index */
		return ilFieldIndex;
	}

	/* field not found */
	return -1;
} /* end of GetFieldIndex() */

/****************************************************************/
/* Function: 		GetNextUrno				*/
/* Parameter: 		buffer for new urno			*/
/* Return: 		length of Urno or <0 if error		*/
/* Description: 	Fetches next URNO using Lib functions.	*/
/****************************************************************/
static int GetNextUrno(char* pcpUrno) 
{
	int	ilRc = RC_SUCCESS;	/* Return code  */
	ilRc = GetNextValues(pcpUrno,1);
	dbg(DEBUG,"GetNextUrno: <%s>",pcpUrno);
	return strlen(pcpUrno);
} /* end of GetNextUrno() */


/********************************************************************************/
/* Create the config file and save parameters.					*/
/********************************************************************************/
static int MakeCfgFile()
{
	char pclFunc[]="MakeCfgFile:";  
	char pclList[100];
	char pclCfgFile[100];
	FILE *flFilehdl;

	memset(pclList,0,sizeof(pclList));

	sprintf(pclCfgFile,"%s/%s.cfg.%s", getenv("CFG_PATH"),mod_name,GetTimeStamp());
	dbg(TRACE,"%s Create new cfg file <%s>",pclFunc,pclCfgFile); 

	flFilehdl = fopen(pclCfgFile,"w");

	if (flFilehdl != NULL)
	{
		/* create section GLOBAL */
		fprintf(flFilehdl,"[GLOBAL]\n");
		fprintf(flFilehdl,"* Debug Level [OFF|TRACE|DEBUG] default TRACE\n");
		switch(debug_level)
		{
		case DEBUG: 
			fprintf(flFilehdl,"debug_level = DEBUG\n");
			break;
		case TRACE: 
			fprintf(flFilehdl,"debug_level = TRACE\n");
			break;
		case 0: 
			fprintf(flFilehdl,"debug_level = OFF\n");
			break;
		default: 
			fprintf(flFilehdl,"debug_level = TRACE\n");
			break;
		}
		fprintf(flFilehdl,"* Home Airport.\n");
		fprintf(flFilehdl,"HOPO = %s\n",pcgHopo);
		fprintf(flFilehdl,"* Maximum size of file used for event buffering in MBytes\n");
		fprintf(flFilehdl,"* (0 -> no limit, -1 -> event buffering disabled). \n");
		fprintf(flFilehdl,"* Default=0\n");
		fprintf(flFilehdl,"* Event buffering is used to suppress the processing of events by the handler\n");
		fprintf(flFilehdl,"* without losing the events. All events will be saved to disk in a file which can\n");
		fprintf(flFilehdl,"* have the specified maximum size. The events will be processed when event buffering\n");
		fprintf(flFilehdl,"* is disabled again by sending command DISABLE_EVENT_BUFFERING . \n");
		fprintf(flFilehdl,"MAX_EVENT_BUFFER = %d\n",igMaxEventBuffer);
		fprintf(flFilehdl,"* QUEUES_TO_HOLD is a comma delimited list of queues which are set to hold\n");
		fprintf(flFilehdl,"* before buffered events will be processed. This is done to avoid script executing\n");
		fprintf(flFilehdl,"* handlers like SCBHDL and XBSHDL interfering each others work by working on the same\n");
		fprintf(flFilehdl,"* tables. The queues specified here will be started again after processing all buffered events\n");
		fprintf(flFilehdl,"* and before and after each termination to make sure the affected handlers will not miss events\n");
		fprintf(flFilehdl,"* and (what's more important) will not miss system messages like terminate etc..\n");
		fprintf(flFilehdl,"QUEUES_TO_HOLD = %d\n",pcgQueuesToHold);
		fprintf(flFilehdl,"[GLOBAL_END]\n\n");

		fprintf(flFilehdl,"[INIT]\n");
		fprintf(flFilehdl,"* Functionname which is initially executed (Init_scbhdl).\n");
		fprintf(flFilehdl,"INIT_FUNCTION = %s\n",cgInitScript);
		fprintf(flFilehdl,"[INIT_END]\n\n");

		fprintf(flFilehdl,"[ACTION]\n");
		fprintf(flFilehdl,"* Number of trials do create a dynamic section in ACTION.\n");
		fprintf(flFilehdl,"RETRY_CREATE_SECTION = %d\n",igRetryCreateAction);
		fprintf(flFilehdl,"* Timeout value for waiting for ACTION to respond broadcasts.\n");
		fprintf(flFilehdl,"WAIT_FOR_ANSWER = %d\n",igWaitForAnswer);
		fprintf(flFilehdl,"[ACTION_END]\n\n");

		fprintf(flFilehdl,"[ACCOUNTS]\n");
		fprintf(flFilehdl,"* ACTIVATED specified a list of compiled account scripts to be executed. \n");
		fprintf(flFilehdl,"ACTIVATED = %s\n",pcgNativeAccounts);
		fprintf(flFilehdl,"* OFFLINE specifies a list of account scripts not to be executed. \n");
		fprintf(flFilehdl,"OFFLINE = %s\n",pcgOfflineAccounts);
		fprintf(flFilehdl,"[ACCOUNTS_END]\n\n");

		fflush(flFilehdl);
		fclose(flFilehdl);
	}
	else
	{
		dbg(TRACE,"%s Error: ",pclFunc,strerror(errno)); 
	}

	return RC_SUCCESS;
} /* end of MakeCfgFile() */

/********************************************************************************/
/* Read the config file and restore internal parameters.			*/
/********************************************************************************/
static int ReadConfig(char *pcpCfgFile)
{
	int ilRC = RC_SUCCESS;
	char pclFunc[]="ReadCfg:";
	char pclResult[1000];
	char pclSection[256];
	char pclKeyword[256];
	char pclValue[256];

	/***********************************************************************************************************/
	/** section GLOBAL *****************************************************************************************/
	/***********************************************************************************************************/
	/* Read debug_level*/
	strcpy(pclSection,"GLOBAL");
	strcpy(pclKeyword,"debug_level");  
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pclResult);
	if(ilRC == RC_SUCCESS) 
	{
		dbg(DEBUG,"%s debug_level = <%s>",pclFunc,pclResult); 
		if(strcmp(pclResult,"DEBUG") == 0)
		{
			debug_level = DEBUG;
		}
		else if(strcmp(pclResult,"OFF") == 0)
		{
			debug_level = 0;
		}
		else 
		{
			debug_level = TRACE;
		}
	}
	else 
	{
		debug_level = TRACE;
	}

	/* home airport */
	strcpy(pclKeyword,"HOPO");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pcgHopo);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s HOPO = <%s>",pclFunc,pcgHopo); 
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s HOPO not found!!! Set to default ('FRA')!!!",pclFunc); 
		/* default value */
		strcpy(pcgHopo,"FRA");
	}

	/* read MAX_EVENT_BUFFER in Mega Bytes: this is the maximum size of the file which buffers events */
	/* added with AddEventToFile() or actually created by CreateBufferedEvent(). this lets the handler itself or a */
	/* script which is being executed store events in a file instead of processing them immediatly. */
	/* The events can be executed later by calling ReadEventFromFile(). If the parameter is 0, the file */
	/* size is unlimited. If it is -1, event buffering is not allowed. */
	strcpy(pclKeyword,"MAX_EVENT_BUFFER");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_INT,(char *)&igMaxEventBuffer);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s MAX_EVENT_BUFFER = <%d>",pclFunc,igMaxEventBuffer); 
		/* convert to bytes */
		igMaxEventBuffer = igMaxEventBuffer*1000000;
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s MAX_EVENT_BUFFER not found!!! Set to default (=0 -> unlimited file size for buffering events)!!!",pclFunc); 
		/* default value: unlimited space */
		igMaxEventBuffer = 0;
	}

	/* read QUEUES_TO_HOLD: this is a comma delimited list of queues which are set to hold */
	/* before buffered events will be processed. This is done to avoid script executing */
	/* handlers like SCBHDL and XBSHDL interfering each others work by working on the same */
	/* tables. The queues specified here will be started again after processing all buffered events */
	/* and before and after each termination to make sure the affected handlers will not miss events */
	/* and (what's more important) will not miss system messages like terminate etc.. */
	strcpy(pclKeyword,"QUEUES_TO_HOLD");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pcgQueuesToHold);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s QUEUES_TO_HOLD = <%s>",pclFunc,pcgQueuesToHold); 
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s QUEUES_TO_HOLD not found!!! Set to default (='' -> no queue will be set to 'HOLD')!!!",pclFunc); 
		/* default value: no queue */
		*pcgQueuesToHold = '\0';
	}

	/***********************************************************************************************************/
	/** section INIT *******************************************************************************************/
	/***********************************************************************************************************/
	/* read file name of init script */
	strcpy(pclSection,"INIT");
	strcpy(pclKeyword,"INIT_FUNCTION");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,cgInitScript);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s INIT_FUNCTION = <%s>",pclFunc,cgInitScript); 
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s INIT_FUNCTION not found!!! Set to default ('' -> no init script will be executed)!!!",pclFunc); 
		/* default value */
		*cgInitScript = '\0';
	}

	strcpy(pclKeyword,"SEND_RELACC");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pclValue);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s SEND_RELACC = <%s>",pclFunc,pclValue); 
		if (strcmp(pclValue,"TRUE") == 0)
			bgSendRelAcc = TRUE;
		else
			bgSendRelAcc = FALSE;
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s SEND_RELACC not found!!! Set to default ('FALSE' -> no RELACC will be send)!!!",pclFunc); 
		/* default value */
		bgSendRelAcc = FALSE;
	}

	/***********************************************************************************************************/
	/** section ACTION *****************************************************************************************/
	/***********************************************************************************************************/
	/* read RETRY_CREATE_SECTION */
	strcpy(pclSection,"ACTION");
	strcpy(pclKeyword,"RETRY_CREATE_SECTION");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_INT,(char *)&igRetryCreateAction);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s RETRY_CREATE_SECTION = <%d>",pclFunc,igRetryCreateAction); 
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s RETRY_CREATE_SECTION not found!!! Set to default (= 3)!!!",pclFunc); 
		/* default value */
		igRetryCreateAction = 3;
	}

	/* read WAIT_FOR_ANSWER */
	strcpy(pclKeyword,"WAIT_FOR_ANSWER");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_INT,(char *)&igWaitForAnswer);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s WAIT_FOR_ANSWER = <%d>",pclFunc,igWaitForAnswer); 
	}
	else 
	{
		/* not found */
		dbg(DEBUG,"%s WAIT_FOR_ANSWER not found!!! Set to default (= 30)!!!",pclFunc); 
		/* default value */
		igWaitForAnswer = 100;
	}

	/***********************************************************************************************************/
	/** section ACCOUNTS ***************************************************************************************/
	/***********************************************************************************************************/
	/* native accounts */
	strcpy(pclSection,"ACCOUNTS");
	strcpy(pclKeyword,"ACTIVATED");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pcgNativeAccounts);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s ACTIVATED ACCOUNTS = <%s>",pclFunc,pcgNativeAccounts); 
	}
	else 
	{
		/* not found */
		/* default value */
		pcgNativeAccounts[0] = '\0';
		dbg(DEBUG,"%s ACTIVATED ACCOUNTS not found! Set to default ('%s')!!!",pclFunc,pcgNativeAccounts); 
	}

	/* offline accounts */
	strcpy(pclKeyword,"OFFLINE");
	ilRC = iGetConfigEntry(pcpCfgFile,pclSection,pclKeyword,CFG_STRING,pcgOfflineAccounts);
	if(ilRC == RC_SUCCESS) 
	{
		/* OK */
		dbg(DEBUG,"%s OFF ACCOUNTS = <%s>",pclFunc,pcgOfflineAccounts); 
	}
	else 
	{
		/* not found */
		/* default value */
		pcgOfflineAccounts[0] = '\0';
		dbg(DEBUG,"%s OFF ACCOUNTS not found! Set to default ('%s')!!!",pclFunc,pcgOfflineAccounts); 
	}


	return ilRC;
} /* end of ReadConfig() */

/************************************/
/* Do the calculation of an account */
/************************************/
static int ExecuteScript(char *pcpScriptName, char *pcpCmdLine)
{
	int ilError;
	char pclNextScript[MAX_FILENAME];

	/* for performance test */
	double dlStart, dlFin, dlDuration, dlStartMSec, dlFinMSec;
	struct timeb slStart;
	struct timeb slFinish;

	BOOL blOffline = FALSE;
	BOOL blExecute = TRUE;
	NATIVESCRIPTFUNC plFunc = NULL;
	char clCheckAccount[12];
	clCheckAccount[0] = '\0';
	/* clear next script name */
	strcpy(pcgNextScript,"");

	dbg(TRACE,"ExecuteScript(): Start processing '%s'",pcpScriptName);

	/* get the start time */
#ifdef _SUN_UNIX
	ftime(&slStart);
#elif defined _SINIX
	ftime(&slStart);
#elif defined _LINUX
	ftime(&slStart);
#else
	_ftime(&slStart);
#endif

	/* get the function pointer of the script and check if the account is offline */
	plFunc = FindNativeAccountScript(pcpScriptName,&blOffline,clCheckAccount);
	blExecute = bgRecalcOfflineAccounts;
	if (!blExecute)
	{
		blExecute = !blOffline;
	}

	/* check date of record and contract of employee whether to execute calculation or not */
	if (blExecute && strlen(clCheckAccount) > 0)
	{
		char clScoCode[10];
		char clSday[16];

		if (strlen(pcgData) == NULL)
		{
			dbg(TRACE,"ExecuteScript(): Stopped, because <pcgData> = undef!!!");
			return 4711;
		}

		if (strlen(pcgFields) == 0)
		{
			dbg(TRACE,"ExecuteScript(): Stopped, because <pcgFields> = undef!!!");
			return 4711;
		}

		if (GetFieldValueFromMonoBlockedDataString("SDAY",pcgFields,pcgData,clSday) <= 0)
		{
			dbg(TRACE,"ExecuteScript(): Stopped, because <clSday> = undef!!!");
			return 4711;
		}

		ReadTemporaryField("ScoCode",clScoCode);
		blExecute = CheckCalcAccount(clCheckAccount,clScoCode,clSday);
	}

	/* execute the calculation */
	if (blExecute)
	{
		if (plFunc != NULL)
		{
			(*plFunc)();
			ilError = 0x00000000;
		}
	}

	/* get the finish time */
#ifdef _SUN_UNIX
	ftime(&slFinish);
#elif defined _SINIX
	ftime(&slFinish);
#elif defined _LINUX
	ftime(&slFinish);
#else
	_ftime(&slFinish);
#endif

	/* start and finish time in millesecs */
	dlStartMSec = slStart.millitm;
	dlStartMSec = dlStartMSec / 1000;
	dlStart = (double)slStart.time;
	dlStart += dlStartMSec;
	dlFinMSec = slFinish.millitm;
	dlFinMSec = dlFinMSec / 1000;
	dlFin = (double)slFinish.time;
	dlFin += dlFinMSec;
	/* duration in secs.millesecs */
	dlDuration = dlFin - dlStart;
	dbg(TRACE,"ExecuteScript(): Took %.8f seconds to execute script <%s>.",dlDuration,pcpScriptName);

	/* do we have to execute another script? */
	if (strcmp(pcgNextScript,"") != 0)
	{
		/* yes */
		/* Copy the script name, because <pcgNextScript> must be cleared 
		   before executing the script (see above). If we pass <pcgNextScript>,
		   the interpreter would get "" as filename. <pcgNextScript> must be 
		   cleared so we can determine, if another script shall be executed. */
		strcpy(pclNextScript,pcgNextScript);
		/* here we go again */
		dbg(DEBUG,"ExecuteScript(): Executing script '%s'",pclNextScript);
		return ExecuteScript(pclNextScript,pcpCmdLine);
	}
	return ilError;
} /* end of ExecuteScript() */

/******************************************************************************/
/* Delete a section in the 'action'-handler which is connected to an open     */
/* recordset/CEDAArray. The purpose of this function is to automatically      */
/* update recordsets/CEDAArrays. Since every update is done by a complete     */
/* refill of the recordset/CEDAArray, this mechanism should be used to hold   */
/* basic data only or other rarely changing data                              */
/******************************************************************************/

static int DeleteArrayActionSection(int ipRSIndex)
{
	int           ilRc          = RC_SUCCESS ;    /* Return code */
	EVENT        *prlEvent      = NULL ;
	BC_HEAD      *prlBchead     = NULL ;
	CMDBLK       *prlCmdblk     = NULL ;
	ACTIONConfig *prlAction     = NULL ;
	char         *pclSelection  = NULL ;
	char         *pclFields     = NULL ;
	char         *pclData       = NULL ;
	long          llActSize     = 0 ;
	int           ilAnswerQueue = 0 ;
	char          clQueueName[16] ;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"DeleteArrayActionSection: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
	/* check, if it is an 'action'-updated CEDAArray */
	if (sgRecordsets[ipRSIndex].iIsActionRecordset == 0)
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"DeleteArrayActionSection: CEDAArray at position <%d> is not updated by ACTION-Events!!!",ipRSIndex);
		return -1;
	}

	/* try to get a queue (communication channel between scbhdl and action) */
	sprintf (&clQueueName[0],"%s2", mod_name) ;
	ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
	if (ilRc != RC_SUCCESS)
	{
		/* failure -> write message and terminate */
		dbg(TRACE,"DeleteArrayActionSection: GetDynamicQueue failed <%d>",ilRc);
		return ilRc;
	}
	/* success */

	/* allocate mem for event struct */
	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
	prgOutEvent = realloc(prgOutEvent,llActSize);
	if(prgOutEvent == NULL)
	{
		dbg(TRACE,"DeleteArrayActionSection: realloc out event <%d> bytes failed",llActSize);
		return DeleteQueue(ilAnswerQueue,clQueueName);
	} /* end of if */
	memset((void*)prgOutEvent,0x00,llActSize);

	/* compose the broadcast which goes out to action */
	prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
	/* avoid action's HandleData() terminate too early */
	prlBchead->rc = RC_SUCCESS;
	prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
	pclSelection  = prlCmdblk->data ;
	pclFields     = pclSelection + strlen (pclSelection) + 1 ;
	pclData       = pclFields + strlen (pclFields) + 1 ;
	strcpy (pclData, "DYN") ;
	prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;

	prgOutEvent->type   = USR_EVENT ;
	prgOutEvent->command     = EVENT_DATA ;
	prgOutEvent->originator  = ilAnswerQueue ;
	prgOutEvent->retry_count = 0 ;
	prgOutEvent->data_offset = sizeof(EVENT) ;
	prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

	prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
	prlAction->iIgnoreEmptyFields = 0;
	strcpy(prlAction->pcSndCmd,"");
	sprintf(prlAction->pcSectionName,"%s_%s", mod_name,sgRecordsets[ipRSIndex].cArrayName);
	strcpy(prlAction->pcTableName, sgRecordsets[ipRSIndex].cTabName);
	strcpy(prlAction->pcFields,sgRecordsets[ipRSIndex].cFieldList);
	strcpy(prlAction->pcSectionCommands,"");
	prlAction->iModID = mod_id;
	prlAction->iADFlag = iDELETE_SECTION ;

	/* see () for diagnostic code to copy here, if necessary */

	/* send the broadcast to action (mod_id = 7400) */
	ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
	if(ilRc != RC_SUCCESS)
	{
		/* failure -> write message and terminate function */
		dbg(TRACE,"DeleteArrayActionSection: QUE_PUT (iDELETE_SECTION) failed <%d>",ilRc);
		DeleteQueue(ilAnswerQueue,clQueueName);
		return ilRc;
	}

	/* wait <igWaitForAnswer> sec. for action's answer */
	ilRc = WaitAndCheckQueue(igWaitForAnswer,ilAnswerQueue,&prgOutItem) ;
	if(ilRc != RC_SUCCESS)
	{
		/* action did not answer in time -> print message and terminate function */

		dbg(TRACE,"DeleteArrayActionSection: WaitAndCheckQueue failed <%d>",ilRc) ;
		DeleteQueue(ilAnswerQueue,clQueueName);
		return ilRc;
	}

	/* return code for delete does not matter (see polhdl.c -> TriggerAction() ) */

	/* release the dynamic queue */
	ilRc = DeleteQueue(ilAnswerQueue,clQueueName);
	return (ilRc) ;
} /* end of DeleteArrayActionSection() */

/****************************************************************************************/
/* Add dynamically table events to the action handler.					*/
/*	Input:	<pcpTableName>		-> the table of our interest			*/
/*		<pcpFields>		-> the fields of interest			*/
/*		<pcpSectCmds>		-> comma delimited commands, which are of	*/
/*					   interest					*/
/*          	<pcpSndCmd>         	-> the command we will receive			*/
/*		<ipIgnoreEmptyFields> 	-> if 0, don't send data of fields, whose	*/
/*                                   	   value  are blank or NULL                 	*/
/****************************************************************************************/
static int TriggerAction(char *pcpTableName,char *pcpFields,char *pcpCmd)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  ACTIONConfig rlAction;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;

  sprintf (&clQueueName[0],"%s2", mod_name) ;

  ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
  if (ilRc != RC_SUCCESS)
  {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
  }
  else
  {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
  }/* end of if */

  if (ilRc == RC_SUCCESS)
  {
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + 
	 	sizeof(CMDBLK) + sizeof(ACTIONConfig) + strlen(pcpFields) + 666;
    prgOutEvent = realloc(prgOutEvent,llActSize);
    if(prgOutEvent == NULL)
    {
      dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
      ilRc = RC_FAIL;
    } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
    prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
    prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
    pclSelection  = prlCmdblk->data ;
    pclFields     = pclSelection + strlen (pclSelection) + 1 ;
    strcpy(pclFields,pcpFields);

    pclData       = pclFields + strlen (pclFields) + 1 ;
    strcpy (pclData, "DYN") ;
    memset(&rlAction,0,sizeof(ACTIONConfig));

    prlBchead->rc = RC_SUCCESS;

    prgOutEvent->type        = USR_EVENT ;
    prgOutEvent->command     = EVENT_DATA ;
    prgOutEvent->originator  = ilAnswerQueue ;
    prgOutEvent->retry_count = 0 ;
    prgOutEvent->data_offset = sizeof(EVENT) ;
    prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

    rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
    rlAction.iIgnoreEmptyFields = 0 ; 
    strcpy(rlAction.pcSndCmd, "") ; 
    sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
    strcpy(rlAction.pcTableName, pcpTableName) ; 
    strcpy(rlAction.pcFields, pcpFields);
    if (pcpCmd != NULL)
    {
    	strcpy(rlAction.pcSectionCommands, pcpCmd);
	}
	else
	{
    	strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
	}
    rlAction.iModID = mod_id ;   

    rlAction.iADFlag = iDELETE_SECTION ;
    memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent); 
    if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc);
    }
    else
    {
      ilRc = WaitAndCheckQueue(igWaitForAnswer, ilAnswerQueue,&prgItem);
      if(ilRc != RC_SUCCESS)
      {
        dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc);
      }
    } /* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
    rlAction.iADFlag = iADD_SECTION ;
    memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize,(char *) prgOutEvent);
    if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
      ilRc = WaitAndCheckQueue(igWaitForAnswer,ilAnswerQueue,&prgItem) ;
      if(ilRc != RC_SUCCESS)
      {
        dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
      }
      else
      {
        prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
        prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
        prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
        pclSelection = (char *)    prlCmdblk->data ;
        pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
        pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

        if(strcmp(pclData,"SUCCESS") != 0)
        {
          dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
          DebugPrintItem(DEBUG,prgItem) ;
          DebugPrintEvent(DEBUG,prlEvent) ;
          DebugPrintBchead(DEBUG,prlBchead) ;
          DebugPrintCmdblk(DEBUG,prlCmdblk) ;
          dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
          dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
          dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
          ilRc = RC_FAIL ;
        }
        else
        {
          dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
        }/* end of if */
      }/* end of if */
    }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
    dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
    dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */

  return (ilRc) ;
}


/******************************************************************************/
/* DeleteActionSection: Delete a section in the 'action'-handler              */
/* Return: RC_SUCCES -> everything is ok                                      */
/*         < 0       -> error                                                 */
/******************************************************************************/
static int DeleteActionSection(int ipIndex)
{
	int           ilRc          = RC_SUCCESS ;    /* Return code */
	EVENT        *prlEvent      = NULL ;
	BC_HEAD      *prlBchead     = NULL ;
	CMDBLK       *prlCmdblk     = NULL ;
	ACTIONConfig *prlAction     = NULL ;
	char         *pclSelection  = NULL ;
	char         *pclFields     = NULL ;
	char         *pclData       = NULL ;
	long          llActSize     = 0 ;
	int           ilAnswerQueue = 0 ;
	char          clQueueName[16] ;

	/* check, if index is valid */
	if ((ipIndex < 0) || (ipIndex >= MAX_ACTION_SECTION))
	{
		/* invalid index */
		dbg(DEBUG,"DeleteActionSection: invalid index <%d>!!!",ipIndex);
		return -1;
	}

	/* check, if section is in use */
	if (sgActionSections[ipIndex].iInUse != 1)
	{
		/* section not in use */
		dbg(DEBUG,"DeleteActionSection: section at index <%d> is not in use.",ipIndex);
		return RC_SUCCESS;
	}

	/* try to get a queue (communication channel between scbhdl and action) */
	sprintf (&clQueueName[0],"%s2", mod_name) ;
	ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
	if (ilRc != RC_SUCCESS)
	{
		/* failure -> write message and terminate */
		dbg(TRACE,"DeleteActionSection: GetDynamicQueue failed <%d>",ilRc);
		return ilRc;
	}
	/* success */
	/* allocate mem for event struct */
	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
	prgOutEvent = realloc(prgOutEvent,llActSize);
	if(prgOutEvent == NULL)
	{
		dbg(TRACE,"DeleteActionSection: realloc out event <%d> bytes failed",llActSize);
		/* clear data anyway */
		InitActionSectionStruct(ipIndex);
		return DeleteQueue(ilAnswerQueue,clQueueName);
	} /* end of if */
	memset((void*)prgOutEvent,0x00,llActSize);

	/* compose the broadcast which goes out to action */
	prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT));
	/* avoid action's HandleData() terminate too early */
	prlBchead->rc = RC_SUCCESS;
	prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
	pclSelection  = prlCmdblk->data ;
	pclFields     = pclSelection + strlen (pclSelection) + 1 ;
	pclData       = pclFields + strlen (pclFields) + 1 ;
	strcpy (pclData, "DYN") ;
	prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;

	prgOutEvent->type   = USR_EVENT ;
	prgOutEvent->command     = EVENT_DATA ;
	prgOutEvent->originator  = ilAnswerQueue ;
	prgOutEvent->retry_count = 0 ;
	prgOutEvent->data_offset = sizeof(EVENT) ;
	prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

	prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
	prlAction->iIgnoreEmptyFields = 0;
	strcpy(prlAction->pcSndCmd,"") ;
	sprintf(prlAction->pcSectionName,"%s_%s", mod_name,sgActionSections[ipIndex].cCmd) ;
	strcpy(prlAction->pcTableName, sgActionSections[ipIndex].cTabName) ;
	strcpy(prlAction->pcFields,sgActionSections[ipIndex].cFieldList);
	strcpy(prlAction->pcSectionCommands,"") ;
	prlAction->iModID = mod_id;
	prlAction->iADFlag = iDELETE_SECTION ;

	/* see TriggerAction() for diagnostic code to copy here, if necessary */

	/* send the broadcast to action (mod_id = 7400) */
	ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ; 
	if(ilRc != RC_SUCCESS)
	{
		/* failure -> write message and terminate function */
		dbg(TRACE,"DeleteActionSection: QUE_PUT (iDELETE_SECTION) failed <%d>",ilRc);
		DeleteQueue(ilAnswerQueue,clQueueName);
		/* clear data anyway */
		InitActionSectionStruct(ipIndex);
		return ilRc;
	}

	/* wait <igWaitForAnswer> sec. for action's answer */
	ilRc = WaitAndCheckQueue(igWaitForAnswer,ilAnswerQueue,&prgOutItem) ;
	if(ilRc != RC_SUCCESS)
	{
		/* action did not answer in time -> print message and terminate function */
		dbg(TRACE,"DeleteActionSection: WaitAndCheckQueue failed <%d>",ilRc) ;
		DeleteQueue(ilAnswerQueue,clQueueName);
		/* clear data anyway */
		InitActionSectionStruct(ipIndex);
		return ilRc;
	}

	/* return code for delete does not matter (see polhdl.c -> TriggerAction() ) */

	/* release the dynamic queue */
	ilRc = DeleteQueue(ilAnswerQueue,clQueueName);

	/* clear data */
	InitActionSectionStruct(ipIndex);

	return (ilRc) ;
} /* end of DeleteActionSection() */

/******************************************************************************/
/* Delete a dynamic queue and print out result                                */
/******************************************************************************/
static int DeleteQueue(int ipQueue, char* pcpQueueName)
{
	int ilRc;
	ilRc = que(QUE_DELETE,ipQueue,ipQueue,0,0,0) ;
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"DeleteQueue: que QUE_DELETE <%d> failed <%d>",ipQueue, ilRc) ;
	}
	else
	{
		dbg(DEBUG,"DeleteQueue: queue <%d> <%s> deleted", ipQueue, pcpQueueName) ;
	}/* end of if */
	return ilRc;
} /* end of DeleteQueue() */

/******************************************************************************/
/* Wait <ipTimeout> seconds for a broadcast on the queue <ipModId>.           */
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		nap(50);
		ilWaitCounter += 1;

		/* check the specified queue */
		ilRc = CheckQueue(ipModId,prpItem);
		/* received message but not successful? */
		if((ilRc != RC_SUCCESS) && (ilRc != QUE_E_NOMSG))
		{
			dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
		}/* end of if */

	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if (ilWaitCounter >= ipTimeout)
	{
		/* no message in time received */
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}

	return ilRc;
} /* end of WaitAndCheckQueue() */



/******************************************************************************/
/* Check the queue <ipModId> for messages.                                    */
/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;

	ilItemSize = I_SIZE;

	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
	if(ilRc == RC_SUCCESS)
	{
		prlEvent = (EVENT*) ((*prpItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;

			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;

			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;

			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;

			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;

			case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;

			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;

			case    RESET       :
				ilRc = Reset();
				break;

			case    EVENT_DATA  :
				break;

			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;

			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;

			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,*prpItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;

		} /* end switch */

		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}
	else
	{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */

	return ilRc;
} /* end of CheckQueue() */

/****************************************************************************************/
/****************************************************************************************/
/*	CALLBACKS FOR DATABASE ACCESS														*/
/****************************************************************************************/
/****************************************************************************************/

/******************************************************************************/
/* Open and fill a CEDArray                                                   */
/******************************************************************************/
static int OpenRecordset(const char* pcpArrayName, const char* pcpTabName, const char* pcpWhere, 
						 const char* pcpFieldList, const char* pcpKeyFieldList, 
						 int ipSortDirection, int ipAutoClose)
{
	/* the index of the internal array management that will be returned */
	int ilRSIndex = -1;
	/* return code for CEDAArray functions */
	int ilRC;
	/* counter for for-next-loop */
	int ilCount;
	/* (re)allocation size	*/
	int ilReAllocSize = 100;

	/* check length of array name */
	if (strlen(pcpArrayName) > MAX_ARRAY_NAME)
	{
		/* array name too large*/
		dbg(DEBUG,"OpenRecordset: array name is larger than <%d> chars!!!",MAX_ARRAY_NAME);
		return OPEN_ARRAY_NAME_TOO_LARGE;
	}

	/* check length of table name */
	if (strlen(pcpTabName) > MAX_TABNAME)
	{
		/* table name too large*/
		dbg(DEBUG,"OpenRecordset: table name is larger than <%d> chars!!!",MAX_TABNAME);
		return OPEN_TABLE_NAME_TOO_LARGE;
	}

	/* since the <pcpKeyFieldList> is used to name the index as well it must not be larger than MAX_INDEX_NAME 
	   that means the number of fields used to create an index is limited by MAX_INDEX_NAME */
	if (strlen(pcpKeyFieldList) > MAX_INDEX_NAME)
	{
		/* index field list <=> index name too large*/
		dbg(DEBUG,"OpenRecordset: index name is larger than <%d> chars!!!",MAX_INDEX_NAME);
		return OPEN_INDEX_NAME_TOO_LARGE;
	}

	/* make sure, that key fields for index are in the fieldlist */
	if ((*pcpKeyFieldList != '\0') && (strstr(pcpFieldList,pcpKeyFieldList) == NULL))
	{
		/* can't create an index with fields that are not read into the array */
		dbg(DEBUG,"OpenRecordset: can't create an index with fields that are not read into the array!!!");
		return OPEN_INVALID_KEY_FIELDS;
	}

	/* check, if array already exists	*/
	if ((ilRSIndex = GetArrayIndex(pcpArrayName)) == -1)
	{
		/* find a free index for a new array */
		if ((ilRSIndex = FindFreeArrayIndex()) == -1)
		{
			/* no array available (MAX_ARRAY arrays are in use) */
			dbg(DEBUG,"OpenRecordset: no more arrays available!!!");
			return OPEN_NO_MORE_ARRAYS;
		}

		/* change reallocation size	*/
		if (strcmp(pcpTabName,"ACCTAB") == 0)
			ilReAllocSize = 5000;
		else if (strcmp(pcpTabName,"BSDTAB") == 0)
			ilReAllocSize = 1000;
		else if (strcmp(pcpTabName,"STFTAB") == 0)
			ilReAllocSize = 2000;
		else if (strcmp(pcpTabName,"SORTAB") == 0)
			ilReAllocSize = 5000;
		else if (strcmp(pcpTabName,"SCOTAB") == 0)
			ilReAllocSize = 5000;
		else if (strcmp(pcpTabName,"SPFTAB") == 0)
			ilReAllocSize = 5000;

		/* create a new array */
		if ((ilRC = CEDAArrayCreateInitCount(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,(char *)pcpTabName,(char *)pcpWhere,sgRecordsets[ilRSIndex].cAddFieldList,
						&sgRecordsets[ilRSIndex].lAddFieldLens[0],(char *)pcpFieldList,&sgRecordsets[ilRSIndex].lFieldLens[0],&sgRecordsets[ilRSIndex].lFieldOffset[0],ilReAllocSize)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"OpenRecordset: error creating CEDAArray '%s', handle is <%d> (Error:%s)!!!",pcpArrayName,sgRecordsets[ilRSIndex].hHandle,CedaArrayErrorCodeToString(ilRC));
			if (sgRecordsets[ilRSIndex].hHandle != -1)
			{
				/* clean up any allocated resources */
				if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
				{
					/* an error occurred */
					dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
					return OPEN_ERROR_CREATE;
				}
				/* free the handle */
				sgRecordsets[ilRSIndex].hHandle = -1;
			}
			return OPEN_ERROR_CREATE;
		}

		/* fill the array */
		if ((ilRC = CEDAArrayFill(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,""/* AddFieldData */)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"OpenRecordset: error filling CEDAArray '%s' (%s)!!!",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
			if (sgRecordsets[ilRSIndex].hHandle != -1)
			{
				/* clean up any allocated resources */
				if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
				{
					/* an error occurred */
					dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
					return OPEN_ERROR_FILL;
				}
				/* free the handle */
				sgRecordsets[ilRSIndex].hHandle = -1;
			}
			return OPEN_ERROR_FILL;
		}

		/* create index if necesssary */
		if (*pcpKeyFieldList != '\0')
		{
			/* create up-sorted index */
			if (ipSortDirection && ((ilRC = CEDAArrayCreateSimpleIndexUp(&sgRecordsets[ilRSIndex].hHandle,
																		 (char *)pcpArrayName,
																		 &sgRecordsets[ilRSIndex].hIndexHandle,
																		 (char *)pcpKeyFieldList,
																		 (char *)pcpKeyFieldList)) != RC_SUCCESS))
			{
				/* error creating index */
				dbg(DEBUG,"OpenRecordset: error creating index <%s> for CEDAArray '%s' (%s)!!!",pcpKeyFieldList,pcpArrayName,CedaArrayErrorCodeToString(ilRC));
				if (sgRecordsets[ilRSIndex].hHandle != -1)
				{
					/* clean up any allocated resources */
					if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
					{
						/* an error occurred */
						dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
						return OPEN_ERROR_CREATE_INDEX;
					}
					/* free the handle */
					sgRecordsets[ilRSIndex].hHandle = -1;
				}
				return OPEN_ERROR_CREATE_INDEX;
			}
			/* create down-sorted index */
			else if (!ipSortDirection && ((ilRC = CEDAArrayCreateSimpleIndexDown(&sgRecordsets[ilRSIndex].hHandle,
																				 (char *)pcpArrayName,
																				 &sgRecordsets[ilRSIndex].hIndexHandle,
																				 (char *)pcpKeyFieldList,
																				 (char *)pcpKeyFieldList)) != RC_SUCCESS))
			{
				/* error creating index */
				dbg(DEBUG,"OpenRecordset: error creating index <%s> for CEDAArray '%s' (%s)!!!",pcpKeyFieldList,pcpArrayName,CedaArrayErrorCodeToString(ilRC));
				if (sgRecordsets[ilRSIndex].hHandle != -1)
				{
					/* clean up any allocated resources */
					if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
					{
						/* an error occurred */
						dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
						return OPEN_ERROR_CREATE_INDEX;
					}
					/* free the handle */
					sgRecordsets[ilRSIndex].hHandle = -1;
				}
				return OPEN_ERROR_CREATE_INDEX;
			}
			/* store the name of index */
			strcpy(sgRecordsets[ilRSIndex].cIndexName,pcpKeyFieldList);
		}
	}
	else	/* array already exists	*/
	{
		/*	check if we can reuse it	*/
		if (strcmp(pcpFieldList,sgRecordsets[ilRSIndex].cFieldList) != 0 || strcmp(pcpKeyFieldList,sgRecordsets[ilRSIndex].cIndexName) != 0)
		{
			/* clean up any allocated resources */
			if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
			{
				/* an error occurred */
				dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
				return OPEN_ERROR_FILL;
			}

			/* mark the handle as free	*/
			InitArrayData(ilRSIndex);

			/* create a new array	*/
			return OpenRecordset(pcpArrayName,pcpTabName,pcpWhere,pcpFieldList,pcpKeyFieldList,ipSortDirection,ipAutoClose);
		}

		/*	deactivate index prior to any insertations, if any */
		if (sgRecordsets[ilRSIndex].hIndexHandle != -1)
		{
			ilRC = CEDAArrayDisactivateIndex(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,&sgRecordsets[ilRSIndex].hIndexHandle,(char *)pcpKeyFieldList);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"OpenRecordset: error disactivating index on CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
			}
		}

		if ((ilRC = CEDAArrayRefill(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,(char *)pcpWhere,""/* AddFieldData */,ARR_FIRST)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"OpenRecordset: error refilling CEDAArray '%s' (%s)!!!",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
			if (sgRecordsets[ilRSIndex].hHandle != -1)
			{
				/* clean up any allocated resources */
				if ((ilRC = CEDAArrayDestroy(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName)) != RC_SUCCESS)
				{
					/* an error occurred */
					dbg(TRACE,"OpenRecordset: error destroying CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
					return OPEN_ERROR_FILL;
				}
				/* free the handle */
				sgRecordsets[ilRSIndex].hHandle = -1;
			}
			return OPEN_ERROR_FILL;
		}

		/*	activate index post to any insertations, if any */
		if (sgRecordsets[ilRSIndex].hIndexHandle != -1)
		{
			ilRC = CEDAArrayActivateIndex(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,&sgRecordsets[ilRSIndex].hIndexHandle,(char *)pcpKeyFieldList);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"OpenRecordset: error activating index on CEDAArray '%s' (%s)",pcpArrayName,CedaArrayErrorCodeToString(ilRC));
			}
		}

	}

	/* store the row count */
	CEDAArrayGetRowCount(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,&sgRecordsets[ilRSIndex].lRowCount);

	/* info */ 
	if (debug_level == DEBUG)
	{
		CEDAArrayInfoOneArray(&sgRecordsets[ilRSIndex].hHandle,(char *)pcpArrayName,outp);
	}

	/* now that the array was successfully created and filled, copy it's parameters */
	strcpy(sgRecordsets[ilRSIndex].cArrayName,pcpArrayName);
	strcpy(sgRecordsets[ilRSIndex].cTabName,pcpTabName);
	strcpy(sgRecordsets[ilRSIndex].cSelect,pcpWhere);
	strcpy(sgRecordsets[ilRSIndex].cFieldList,pcpFieldList);
	/* this is no CEDAArray, which is updated automatically by action events */
	sgRecordsets[ilRSIndex].iIsActionRecordset = 0;
	strcpy(sgRecordsets[ilRSIndex].cPostUpdateScript,"");
	strcpy(sgRecordsets[ilRSIndex].cPreUpdateScript,"");
	sgRecordsets[ilRSIndex].iActionType = 0;
	/* this CEDAArray may write and change records */
	sgRecordsets[ilRSIndex].iIsReadOnly = 0;
	/* should Cleanup() close this CEDAArray? */
	sgRecordsets[ilRSIndex].iAutoClose = ipAutoClose;
	sgRecordsets[ilRSIndex].iIsDeleted = 0;

#ifndef _UFIS_43
	/* send any changes to the 'action'-handler */
	CEDAArraySendChanges2ACTION(&sgRecordsets[ilRSIndex].hHandle,sgRecordsets[ilRSIndex].cArrayName,1);
	/* let bchdl create broadcasts on changes */
	CEDAArraySendChanges2BCHDL(&sgRecordsets[ilRSIndex].hHandle,sgRecordsets[ilRSIndex].cArrayName,1);
#endif

	/* calculate the maximum size of a single record in this array */
	sgRecordsets[ilRSIndex].lMaxRecordSize = 0; /* initial size: 0 */
	for (ilCount = 0; (ilCount < MAX_FIELDS) && (sgRecordsets[ilRSIndex].lFieldLens[ilCount] != 0); ilCount += 1)
	{
		/* add the max. size of this field */
		sgRecordsets[ilRSIndex].lMaxRecordSize += sgRecordsets[ilRSIndex].lFieldLens[ilCount];
		/* add one byte for the delimiter or, if last field, the terminating zero */
		sgRecordsets[ilRSIndex].lMaxRecordSize += 1;
	}

	/* return the index of this array in our internal CEDAArray menagement arrays */
	/* use the index as a handle in the executed basic script */ 
	return ilRSIndex;
} /* end of OpenRecordset() */


/******************************************************************************/
/* Open and fill a CEDArray and create a section in the 'action'-handler to   */
/* automatically update the recordset by events which come from 'action'.     */
/* The purpose of this mechanism is to hold basic data in memory for faster   */
/* access.                                                                    */
/******************************************************************************/
static int OpenACTIONRecordset(const char* pcpArrayName, const char* pcpTabName, 
							   const char* pcpWhere, const char* pcpFieldList, 
							   const char* pcpKeyFieldList, int ipSortDirection, 
							   const char* pcpSectCmds, const char* pcpPreUpdateScript, 
							   const char* pcpPostUpdateScript, int ipActionType, 
							   int ipIgnoreEmptyFields, int ipReadOnly)
{
	/* the index of the internal array management that will be returned */
	int ilRSIndex = -1;
	/* return code for CEDAArray functions */
	int ilRC = RC_FAILURE;
	/* retry counter */
	int ilRetry = 1;

	/* first open and fill the CEDAArray */
	if ((ilRSIndex = OpenRecordset(pcpArrayName,pcpTabName,pcpWhere,pcpFieldList,pcpKeyFieldList,ipSortDirection,0)) < 0)
	{
		/* could not open CEDAArray -> exit function */
		return ilRSIndex; /* diagnostic output is already generated by OpenRecordset() */
	}

	/* store the action info before calling TriggerAction(), 
	   in case TriggerAction() fails AFTER having created the section and we have to cleanup */
	sgRecordsets[ilRSIndex].iIsActionRecordset = 1;
	sgRecordsets[ilRSIndex].iIsReadOnly = ipReadOnly;
	sgRecordsets[ilRSIndex].iActionType = ipActionType;
	strcpy(sgRecordsets[ilRSIndex].cPreUpdateScript,pcpPreUpdateScript);
	strcpy(sgRecordsets[ilRSIndex].cPostUpdateScript,pcpPostUpdateScript);
	/* no automatic closing by Cleanup() */
	sgRecordsets[ilRSIndex].iAutoClose = 0;

	/* now create the dynamic section in the 'action'-handler, retry max. <igRetryCreateAction> times */
	for (ilRetry = 1; ilRetry <= igRetryCreateAction && (ilRC != RC_SUCCESS); ilRetry += 1)
	{
		dbg(DEBUG,"OpenACTIONRecordset: calling TriggerAction() the %d. time...",ilRetry);
		if ((ilRC = TriggerAction(sgRecordsets[ilRSIndex].cTabName,sgRecordsets[ilRSIndex].cFieldList,NULL)) == RC_SUCCESS)		{
			/* success -> return the index of this array in our internal CEDAArray menagement arrays */
			/* use the index as a handle in the executed basic script */ 
			return ilRSIndex;
		}
	}
	/* if we get here, TriggerAction failed -> cleanup and terminate with error code */
	CloseRecordset(ilRSIndex,0);
	/* terminate with error code */
	return OPEN_ERROR_TRIGGER_ACTION; /* error codes up to -5 are passed by OpenRecordset() */
} /* end of OpenACTIONRecordset() */

/******************************************************************************/
/* Find first record with the given key value beginning from the first row.   */
/* The buffer with the key value must be large enough to retrieve the found   */
/* record.                                                                    */
/******************************************************************************/
static int FindFirstRecordByIndex(int ipRSIndex, char* pcpKeyValue)
{
	return FindRecordByIndex(ipRSIndex,pcpKeyValue,1);
}

/******************************************************************************/
/* Find next record with the given key value beginning from the first row.    */
/* The buffer with the key value must be large enough to retrieve the found   */
/* record.                                                                    */
/******************************************************************************/
static int FindNextRecordByIndex(int ipRSIndex, char* pcpKeyValue)
{
	return FindRecordByIndex(ipRSIndex,pcpKeyValue,0);
}

/******************************************************************************/
/* Find a record with the given key value beginning from the first row.       */
/* The buffer with the key value must be large enough to retrieve the found   */
/* record.                                                                    */
/******************************************************************************/
static int FindRecordByIndex(int ipRSIndex, char* pcpKeyValue, int ipFirst)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* record */
	char *pclResult      = NULL; /* READ ONLY !!! */

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(DEBUG,"FindRecordByIndex: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/* yes -> set array cursor */
	if (ipFirst)
	{
		dbg(DEBUG,"FindRecordByIndex: setting cursor to ARR_FIRST!");
		sgRecordsets[ipRSIndex].lRow = ARR_FIRST;
	}
	else
	{
		dbg(DEBUG,"FindRecordByIndex: setting cursor to ARR_NEXT!");
		sgRecordsets[ipRSIndex].lRow = ARR_NEXT;
	}

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(DEBUG,"FindRecordByIndex: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/* search for the first record with given key val */
	if ((ilRC = CEDAArrayFindRowPointer(&sgRecordsets[ipRSIndex].hHandle,
										sgRecordsets[ipRSIndex].cArrayName,
										&sgRecordsets[ipRSIndex].hIndexHandle,
										sgRecordsets[ipRSIndex].cIndexName,
										pcpKeyValue,
										&sgRecordsets[ipRSIndex].lRow,
										(void *) &pclResult)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"FindNextRecord: no more records with key <%s> in CEDAArray '%s' or error (%s)",pcpKeyValue,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* get the next row */
	if ((ilRC = CEDAArrayGetFields(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,NULL/* FieldNumbers */,
								   sgRecordsets[ipRSIndex].cFieldList,','/*delimiter*/,1024/* max. bytes */, 
								   sgRecordsets[ipRSIndex].lRow /* cursor of this array */, pcpKeyValue)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"FindNextRecord: no more records in CEDAArray '%s' or error (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* return the length of the record */
	return strlen(pcpKeyValue);
} /* end of FindRecordByIndex() */

/******************************************************************************/
/* Find a record with the given key value beginning from the first row and    */
/* extract the field value of the field spcified by <pcpFieldBuf>.            */
/* <pcpFieldBuf> must be large enough to retrieve the found	field value.      */
/******************************************************************************/
static int FindFieldValueByIndex(int ipRSIndex, char* pcpKeyValue, 
								 char *pcpFieldBuf)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* record */
	char *pclResult      = NULL; /* READ ONLY !!! */
	/* dummy parameter to retrieve the field's index */
	long llFieldIndex = -1;
	/* length of field value after trim right */
	int ilCount = 0;

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(TRACE,"FindFieldValueByIndex: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/**************************************/
	/**************************************/
	/* ARR_FIND does not work!!!          */
	/* Use ARR_FIRST allways.             */
	/**************************************/
	/**************************************/

	dbg(DEBUG,"FindFirstRecord: setting cursor to ARR_FIRST!");
	sgRecordsets[ipRSIndex].lRow = ARR_FIRST;

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(TRACE,"FindFieldValueByIndex: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/* search for the first record with given key val */
	if ((ilRC = CEDAArrayFindRowPointer(&sgRecordsets[ipRSIndex].hHandle,
										sgRecordsets[ipRSIndex].cArrayName,
										&sgRecordsets[ipRSIndex].hIndexHandle,
										sgRecordsets[ipRSIndex].cIndexName,
										pcpKeyValue,
										&sgRecordsets[ipRSIndex].lRow,
										(void *) &pclResult)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"FindFieldValueByIndex: no more records with key <%s> in CEDAArray '%s' or error (%s)",pcpKeyValue,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* check if requested field part of the array's field list */
	if (strstr(sgRecordsets[ipRSIndex].cFieldList,pcpFieldBuf) == NULL)
	{
		/* no -> print message and terminate */
		dbg(TRACE,"FindFieldValueByIndex: field <%s> is not part of CEDAArray '%s' (field list: <%s>)",pcpFieldBuf,sgRecordsets[ipRSIndex].cArrayName,sgRecordsets[ipRSIndex].cFieldList);
		return -3;
	}

	/* get the field value */
	if ((ilRC = CEDAArrayGetField(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&llFieldIndex,pcpFieldBuf,
								  1024/* max. bytes */,sgRecordsets[ipRSIndex].lRow, pcpFieldBuf)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(TRACE,"FindFieldValueByIndex: error getting field value <%s> from array '%s' (%s)",pcpFieldBuf,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -4;
	}

	dbg(DEBUG,"FindFieldValueByIndex: field value for key <%s> = '%s'",pcpKeyValue,pcpFieldBuf);

	/* trim right zero terminate string */
	for (ilCount=sgRecordsets[ipRSIndex].lFieldLens[llFieldIndex]-1; ilCount>=0; ilCount-=1)
	{
		if (*(pcpFieldBuf+ilCount) == ' ')
			*(pcpFieldBuf+ilCount) = '\0';
		else 
			break;
	}

	/* return the true size of the field value */
	return ilCount+1;
} /* end of FindRecordByIndex() */

/******************************************************************************/
/* Find a record with the given key value beginning from the first row and    */
/* extract the field value of the field spcified by <pcpFieldBuf>.            */
/* <pcpFieldBuf> must be large enough to retrieve the found	field value.      */
/******************************************************************************/
static int FindFieldValueByIndex_old(int ipRSIndex, char* pcpKeyValue, 
								 char *pcpFieldBuf)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* dummy parameter to retrieve the field's index */
	long llFieldIndex = -1;
	/* length of field value after trim right */
	int ilCount = 0;

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(TRACE,"FindFirstRecord: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/* set cursor to the first row */
	sgRecordsets[ipRSIndex].lRow = ARR_FIND;

	/* check, if index of array is valid and if there is an index for this array */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1) || 
		(sgRecordsets[ipRSIndex].hIndexHandle == -1))
	{
		/* invalid index or no valid array at this position or no index for this CEDAarray */
		dbg(TRACE,"FindNextRecord: invalid index <%d> or no CEDAArray at this position or no Index for this CEDAArray!!!",ipRSIndex);
		return -1;
	}

	/* search for the first record with given key val */
	if ((ilRC = CEDAArrayFindKey(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,
								 &sgRecordsets[ipRSIndex].hIndexHandle,sgRecordsets[ipRSIndex].cIndexName,
								 &sgRecordsets[ipRSIndex].lRow,1024,pcpKeyValue)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"FindNextRecord: no more records with key <%s> in CEDAArray '%s' or error (%s)",pcpKeyValue,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* check if requested field part of the array's field list */
	if (strstr(sgRecordsets[ipRSIndex].cFieldList,pcpFieldBuf) == NULL)
	{
		/* no -> print message and terminate */
		dbg(TRACE,"FindNextRecord: field <%s> is not part of CEDAArray '%s' (field list: <%s>)",pcpFieldBuf,sgRecordsets[ipRSIndex].cArrayName,sgRecordsets[ipRSIndex].cFieldList);
		return -3;
	}

	/* get the field value */
	if ((ilRC = CEDAArrayGetField(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&llFieldIndex,pcpFieldBuf,
								  1024/* max. bytes */,sgRecordsets[ipRSIndex].lRow, pcpFieldBuf)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(TRACE,"FindNextRecord: error getting field value (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -4;
	}

	/* trim right zero terminate string */
	for (ilCount=sgRecordsets[ipRSIndex].lFieldLens[llFieldIndex]-1; ilCount>=0; ilCount-=1)
	{
		if (*(pcpFieldBuf+ilCount) == ' ')
			*(pcpFieldBuf+ilCount) = '\0';
		else 
			break;
	}

	/* return the true size of the field value */
	return ilCount+1;
} /* end of FindRecordByIndex() */

/******************************************************************************/
/* Read the first record of a CEDAArray							              */
/******************************************************************************/
static int GetFirstRecord(int ipRSIndex, char* pcpRecord)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetFirstRecord: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}

	/* set the cursor to the first row */
	sgRecordsets[ipRSIndex].lRow = ARR_FIRST;

	/* get the next row */
	if ((ilRC = CEDAArrayGetFields(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,NULL/* FieldNumbers */,
								   sgRecordsets[ipRSIndex].cFieldList,','/*delimiter*/,1024/* max. bytes */, 
								   sgRecordsets[ipRSIndex].lRow /* cursor of this array */, pcpRecord)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"GetNextRecord: no more records in CEDAArray '%s' or error (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* return the length of the record */
	return strlen(pcpRecord);
} /* end of GetFirstRecord() */

/******************************************************************************/
/* Get the next record														  */
/******************************************************************************/
static int GetNextRecord(int ipRSIndex, char* pcpRecord)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetNextRecord: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}

	/* set the cursor of this array on the next position */
	sgRecordsets[ipRSIndex].lRow = ARR_NEXT;

	/* get the next row */
	if ((ilRC = CEDAArrayGetFields(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,NULL/* FieldNumbers */,
								   sgRecordsets[ipRSIndex].cFieldList,','/*delimiter*/,1024/* max. bytes */, 
								   sgRecordsets[ipRSIndex].lRow /* cursor of this array */, pcpRecord)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"GetNextRecord: no more records in CEDAArray '%s' or error (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* return the length of the record */
	return strlen(pcpRecord);
} /* end of GetNextRecord() */

/******************************************************************************/
/* Close a CEDAArray. Write to database and delete commands in 'action' first.*/
/******************************************************************************/
static void CloseRecordset(int ipRSIndex, int ipWriteDB)
{
	/* return code for CEDAArray functions */
	int ilRC;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"CloseRecordset: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return;
	}

	/* commit changes, if desired */
	if (ipWriteDB && sgRecordsets[ipRSIndex].iIsDeleted == 0)
	{
		dbg(TRACE,"writing ceda array <%s> to DB",sgRecordsets[ipRSIndex].cArrayName);
		/* write possible changes */
		ilRC = CEDAArrayWriteDB(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		/* check return val, RC_NOTFOUND means array is empty (no error) */
		if ((ilRC != RC_SUCCESS) && (ilRC != RC_NOTFOUND))
		{
			/* an error occurred */
			dbg(TRACE,"CloseRecordset: error writing CEDAArray '%s' (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		}
		dbg(TRACE,"ceda array <%s> written to DB",sgRecordsets[ipRSIndex].cArrayName);
	}

	/* ATTENTION: if <igCommitAfterEachScript> = 0, commit_work must be called elsewhere (i.e. after event)!!!!! */
	if (igCommitAfterEachScript) 
	{
		dbg(TRACE,"CloseRecordset: calling commit_work()...");
		commit_work();
	}

	if (sgRecordsets[ipRSIndex].iAutoDelete && sgRecordsets[ipRSIndex].iIsDeleted == 0)
	{
		/* delete the array */
		if ((ilRC = CEDAArrayDelete(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(TRACE,"CloseRecordset: error deleting CEDAArray '%s' (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		}
		else
		{
			sgRecordsets[ipRSIndex].iIsDeleted = 1;
			dbg(TRACE,"CloseRecordset: ceda array <%s> deleted...",sgRecordsets[ipRSIndex].cArrayName);
		}
	}
} /* end of CloseRecordset() */

/******************************************************************************/
/* Get the maximum size of a single record.                                   */
/******************************************************************************/
static int GetMaxRecordSize(int ipRSIndex)
{
	/* return code for CEDAArray functions */
	int ilRC;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetMaxRecordSize: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* return the maximum record size */
	return sgRecordsets[ipRSIndex].lMaxRecordSize;
} /* end of GetMaxRecordSize() */

/******************************************************************************/
/* Getting the number of records in a recordset.                              */
/******************************************************************************/
static int GetRecordCount(int ipRSIndex)
{
	/* return code for CEDAArray functions */
	int ilRC;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetRecordCount: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* return the record count of this array */
	return sgRecordsets[ipRSIndex].lRowCount;
} /* end of GetRecordCount() */

/******************************************************************************/
/* Get a single field value of the current record.                            */
/******************************************************************************/
static int GetFieldValue(int ipRSIndex, char* pcpFieldName, char* pcpFieldValue)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* dummy parameter to retrieve the field's index */
	long llFieldIndex = -1;
	/* length of field value after trim right */
	int ilCount = 0;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetFieldValue: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* check if requested field part of the array's field list */
	if (strstr(sgRecordsets[ipRSIndex].cFieldList,pcpFieldName) == NULL)
	{
		/* no -> print message and terminate */
		dbg(TRACE,"GetFieldValue: field <%s> is not part of CEDAArray '%s' (field list: <%s>)",pcpFieldName,sgRecordsets[ipRSIndex].cArrayName,sgRecordsets[ipRSIndex].cFieldList);
		return -3;
	}

	/* get the field value */
	if ((ilRC = CEDAArrayGetField(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&llFieldIndex,pcpFieldName,
								  1024/* max. bytes */,ARR_CURRENT, pcpFieldValue)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"GetFieldValue: no more records in CEDAArray '%s' or error (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* trim right zero terminate string */
	for (ilCount=sgRecordsets[ipRSIndex].lFieldLens[llFieldIndex]-1; ilCount>=0; ilCount-=1)
	{
		if (*(pcpFieldValue+ilCount) == ' ')
			*(pcpFieldValue+ilCount) = '\0';
		else 
			break;
	}

	/* return the true size of the field value */
	return ilCount+1;
} /* end of GetFieldValue() */

/******************************************************************************/
/* Get the maximum size of the specified field.                               */
/******************************************************************************/
static int GetMaxFieldSize(int ipRSIndex, char* pcpFieldName)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* index of the field */
	int ilIndex;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"GetMaxFieldSize: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* get the field's index */
	if ((ilIndex = GetFieldIndex(ipRSIndex,pcpFieldName)) == -1)
	{
		/* field not found */
		return 0;
	}

	/* get and return the size of the field */
	return sgRecordsets[ipRSIndex].lFieldLens[ilIndex];
} /* end of GetMaxFieldSize() */

/******************************************************************************/
/* Insert a record.                                                           */
/******************************************************************************/
static int InsertRecord(int ipRSIndex, char* pcpRecord)
{
	/* return code for CEDAArray functions */
	int ilRC;
	/* count var */
	int ilCount = 0;
	/* length of value list */
	int ilRecordSize = 0;

	/* check, if index is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"InsertRecord: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}
 
	/* check, if this CEDAArray is not read only */
	if (sgRecordsets[ipRSIndex].iIsReadOnly != 0)
	{
		/* not allowed */
		dbg(DEBUG,"InsertRecord: CEDAArray at position <%d> is read only!!!",ipRSIndex);
		return -3;
	}

	/* replace delimiter ',' in value list with '\0', which is needed by CEDAArrayAddRow() */
	ilRecordSize = strlen(pcpRecord);
	for (ilCount = 0; ilCount < ilRecordSize; ilCount += 1)
	{
		if (*pcpRecord == ',')
		{
			*pcpRecord = '\0';
		}
		/* check next char */
		pcpRecord += 1;
	}
	/* restore pointer position */
	pcpRecord -= ilRecordSize;

	/* set the cursor position to FIRST */
	sgRecordsets[ipRSIndex].lRow = ARR_FIRST;
	/* add the record */
	if ((ilRC = CEDAArrayAddRow(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&sgRecordsets[ipRSIndex].lRow,pcpRecord)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"InsertRecord: error adding record to CEDAArray '%s' (error: %s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	return 0; /* success */
} /* end of InsertRecord() */

/******************************************************************************/
/* Callback function for updating records.                                    */
/******************************************************************************/
static int UpdateRecord(int ipRSIndex, char* pcpFieldList, char* pcpValueList)
{
	/* return code for CEDAArray functions */
	int ilRC;

	/* check, if recordset handle is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"UpdateRecord: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}

	/* check, if this CEDAArray is not read only */
	if (sgRecordsets[ipRSIndex].iIsReadOnly != 0)
	{
		/* not allowed */
		dbg(DEBUG,"UpdateRecord: CEDAArray at position <%d> is read only!!!",ipRSIndex);
		return -3;
	}

	/* update record */
	if ((ilRC = CEDAArrayPutFields(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,NULL,&sgRecordsets[ipRSIndex].lRow,pcpFieldList,pcpValueList)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"UpdateRecord: error updating record no. %d of CEDAArray '%s' (error: %s)",sgRecordsets[ipRSIndex].lRow,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	return 0; /* success */
} /* end of UpdateRecord() */

/******************************************************************************/
/* Delete the record with the given index.                                    */
/******************************************************************************/
static int DeleteRecord(int ipRSIndex)
{
	/* return code for CEDAArray functions */
	int ilRC;

	/* check, if recordset handle is valid */
	if ((ipRSIndex < 0) || (ipRSIndex >= MAX_ARRAY) || (sgRecordsets[ipRSIndex].hHandle == -1))
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"DeleteRecord: invalid index <%d> or no CEDAArray at this position!!!",ipRSIndex);
		return -1;
	}

	/* check, if this CEDAArray is not read only */
	if (sgRecordsets[ipRSIndex].iIsReadOnly != 0)
	{
		/* not allowed */
		dbg(DEBUG,"DeleteRecord: CEDAArray at position <%d> is read only!!!",ipRSIndex);
		return -3;
	}

	if ((ilRC = CEDAArrayDeleteRow(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,ARR_CURRENT)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"DeleteRecord: error deleting record no. %d from CEDAArray '%s' (error: %s)",sgRecordsets[ipRSIndex].lRow,sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	return 0; /* success */
} /* end of DeleteRecord() */

/****************************************************************************************/
/* Get the index of a CEDAArray with the name <pcpArrayName>. Use this function to      */
/* check, if an array name is ambigous, too. Since the array name is used as command    */
/* for 'action' broadcasts, the name has to be used once at a time.                     */
/****************************************************************************************/
static int GetArrayIndex(const char* pcpArrayName)
{
	static int ilCount;

	/* iterate through the CEDAArrays and check their names */
	for (ilCount = 0; ilCount < MAX_ARRAY; ilCount++)
	{
		if (strcmp(sgRecordsets[ilCount].cArrayName,pcpArrayName) == 0)
		{
			/* array found, return index */
			dbg(DEBUG,"GetArrayIndex: CEDAArray <%s> found at index <%d>",pcpArrayName,ilCount);
			return ilCount;
		}
	}
	dbg(DEBUG,"GetArrayIndex: CEDAArray <%s> not found!!!",pcpArrayName);
	return -1;	/* not found */
} /* end of GetArrayIndex() */

/****************************************************************************************/
/* Check, if the given command contains one of our CEDAArray-names                      */
/* and return the index, if found.                                                      */
/****************************************************************************************/
static int CheckCommandAndGetArrayIndex(char* pcpCmd)
{
	int ilCount;
	/* the position of the substring, if found */
	char *pclFound;
	char clSearchStr[MAX_ARRAY_NAME+2]; /* what we search for: array name + ',' */
	int ilResult;

	/* iterate through the CEDAArrays and check their names */
	for (ilCount = 0; ilCount < MAX_ARRAY; ilCount++)
	{
		/* check only used CEDAArrays */
		if (*sgRecordsets[ilCount].cArrayName != '\0')
		{
			/* create search string */
			strcpy(clSearchStr,sgRecordsets[ilCount].cArrayName);
			strcat(clSearchStr,",");
			
			dbg(DEBUG,"CheckCommandAndGetArrayIndex: checking command <%s>, searching for <%s>",pcpCmd,clSearchStr);
			/* check the command string */ 
			if ((pclFound = strstr(pcpCmd,clSearchStr)) != NULL)
			{
				/* substring found, check position (must be 0) */
				ilResult = pclFound - pcpCmd;
   				if (ilResult == 0)
				{
					dbg(DEBUG,"CheckCommandAndGetArrayIndex: checking command <%s>: <%s> found at index <%d>",pcpCmd,clSearchStr,ilCount);
					return ilCount;
				}
			}
		}
	}
	dbg(DEBUG,"CheckCommandAndGetArrayIndex: no CEDAArray name found in command <%s>!!!",pcpCmd);
	return -1;	/* not found */
} /* end of CheckCommandAndGetArrayIndex() */

/****************************************************************************************/
/* Check, if the given command contains one of our CEDAArray-names and                  */
/* return the index, if found.                                                          */
/****************************************************************************************/
static int CheckCommandAndGetActionStructIndex(char* pcpCmd)
{
	int ilCount;
	/* the position of the substring, if found */
	char *pclFound;
	char clSearchStr[MAX_ACTION_SECTION_COMMAND+1]; /* what we search for: section's cmd parameter + ',' */
	int ilResult;

	/* iterate through <sgActionSections> and check their <cCmd> */
	for (ilCount = 0; ilCount < MAX_ACTION_SECTION; ilCount++)
	{
		/* check only used structs */
		if (*sgActionSections[ilCount].cTabName != '\0')
		{
			/* create search string */
			strcpy(clSearchStr,sgActionSections[ilCount].cTabName);
			/*dbg(DEBUG,"CheckCommandAndGetActionStructIndex: checking table <%s>, searching for <%s>",pcpCmd,clSearchStr);*/
			/* check the command string */ 
			/* check table */
			if ((pclFound = strstr(pcpCmd,clSearchStr)) != NULL)
			{
				/* substring found, check position (must be 0) */
				ilResult = pclFound - pcpCmd;
				dbg(DEBUG,"CheckCommandAndGetActionStructIndex: <%s> found at position <%d>",clSearchStr,ilResult);
	   			if (ilResult == 0)
				{
					dbg(DEBUG,"CheckCommandAndGetActionStructIndex: checking command <%s>: <%s> found at index <%d>",pcpCmd,clSearchStr,ilCount);
					return ilCount;
				}
			}
		}
	}
	dbg(DEBUG,"CheckCommandAndGetActionStructIndex: no command <%s> found in <sgActionSections>!!!",pcpCmd);
	return -1;	/* not found */
} /* end of CheckCommandAndGetActionStructIndex() */

/****************************************************************************************/
/* APO 05.06.2001: - function created                                                   */
/* CheckCommandAndGetCachedTableIndex:                                                  */
/*  Check, if the given command contains one of our cached tables (cActionCmd) and      */
/*  return the index, if found.                                                         */
/* Return: -1   -> command was not found                                                */
/*         >= 0 -> command was found, index of struct                                   */
/****************************************************************************************/
static int CheckCommandAndGetCachedTableIndex(char* pcpCmd)
{
	int ilCount;
	/* the position of the substring, if found */
	char *pclFound;
	char clSearchStr[MAX_TABNAME+4]; /* what we search for: cmd parameter + ',' */
	int ilResult;

	/* iterate through <sgActionSections> and check their <cCmd> */
	for (ilCount = 0; ilCount < MAX_CACHED_TABLES; ilCount++)
	{
		/* check only used structs */
		if (sgCachedTables[ilCount].iInUse != 0)
		{
			/* create search string */
			strcpy(clSearchStr,sgCachedTables[ilCount].cActionCmd);
			strcat(clSearchStr,",");
			dbg(DEBUG,"CheckCommandAndGetCachedTableIndex() line <%d>: checking command <%s>, searching for <%s>",__LINE__,pcpCmd,clSearchStr);
			/* check the command string */ 
			if ((pclFound = strstr(pcpCmd,clSearchStr)) != NULL)
			{
				/* substring found, check position (must be 0) */
				ilResult = pclFound - pcpCmd;
				dbg(DEBUG,"CheckCommandAndGetCachedTableIndex() line <%d>: <%s> found at position <%d>",__LINE__,clSearchStr,ilResult);
	   			if (ilResult == 0)
				{
					dbg(DEBUG,"CheckCommandAndGetCachedTableIndex() line <%d>: command <%s> found at index <%d>",__LINE__,pcpCmd,ilCount);
					return ilCount;
				}
			}
		}
	}
	dbg(DEBUG,"CheckCommandAndGetCachedTableIndex() line <%d>: command <%s> not found in any of <sgCachedTables>!!!",__LINE__,pcpCmd);
	return -1;	/* not found */
} /* end of CheckCommandAndGetActionStructIndex() */

/****************************************************************************************/
/* Get the last broadcasts sel key                                                      */
/****************************************************************************************/
static char* GetLastSelKey()
{
	dbg(DEBUG,"GetLastSelKey: pcgSelKey = <%s>",pcgSelKey);
	return pcgSelKey;
} /* end of GetLastSelKey() */

/****************************************************************************************/
/* Get the field list of the last broadcast                                             */
/****************************************************************************************/
static char* GetLastFieldList(void)
{
	dbg(DEBUG,"GetLastFieldList: pcgFields = <%s>",pcgFields);
	return pcgFields;
} /* end of GetLastFieldList() */

/****************************************************************************************/
/* Get the data-string of the last broadcast                                            */
/****************************************************************************************/
static char* GetLastData(void)
{
	dbg(DEBUG,"GetLastData: pcgData = <%s>",pcgData);
	return pcgData;
} /* end of GetLastData() */

/****************************************************************************************/
/* Get the old-data-string of the last broadcast                                       	*/
/****************************************************************************************/
static char* GetLastOldData(void)
{
	dbg(DEBUG,"GetLastOldData: pcgOldData = <%s>",pcgOldData);
	return pcgOldData;
} /* end of GetLastOldData() */

/****************************************************************************************/
/* Get the command of the last broadcast                                            	*/
/****************************************************************************************/
static char* GetLastCommand(void)
{
	dbg(DEBUG,"GetLastCommand: pcgCmd = <%s>",pcgCmd);
	return pcgCmd;
} /* end of GetLastCommand() */

/****************************************************************************************/
/* Locate the position of a single field in the given fieldlist and return its          */
/* value from the data string, which is stored at the same position.                    */
/* Example: try to find the value for the field 'URNO' in the data string               */
/* 'blabla,peterpan,0,0,,,0001234567,string' with the field list                        */
/* 'FLD1,FLD2,FLD3,FLD4,FLD5,FLD6,FLD7,URNO,FLD8' should copy '0001234567' to the       */
/* buffer <pcpFieldValue>.                                                              */
/****************************************************************************************/
static int GetFieldValueFromDataString(char* pcpField, char* pcpFieldList, char* pcpData, char* pcpFieldValue)
{
	/* pointer to the return buffer */
	char *pclFieldPos, *pclFieldValuePos;
	/* absolute position of field in field list buffer */
	int ilPos = 0;
	/* number of delimiters (',') -> ordinal number of field in field-/data-list */
	int ilNoOfDelim = 0;
	/* length of field name, field value, field list and data */
	int ilFieldLen = 0, ilFieldValueLen = 0, ilFieldListLen = 0, ilDataLen = 0;
	/* trigger for break of loop */
	int blFieldFound = 0, blFieldValueFound = 0;
	/* search field in field list until this position is reached */
	char* pclStopPos;

	/* first clear the return buffer in case it was filled by a prior call with the same pointer */
	*pcpFieldValue = '\0';

	/* check, if one of the parameters is an empty string */
	if ((*pcpField == '\0') || (*pcpFieldList == '\0') || (*pcpData == '\0') || (pcpFieldValue == NULL))
	{
		dbg(DEBUG,"GetFieldValueFromDataString: one of the parameters is an empty string or NULL!!!");
		return -1;
	}

	/* get the length of the field name and field list */
	ilFieldLen = strlen(pcpField);
	ilFieldListLen = strlen(pcpFieldList);

	/* field list must not be shorter than field name */
	if (ilFieldLen > ilFieldListLen)
	{
		/* can not find field because the field list is shorter than the name */
		dbg(DEBUG,"GetFieldValueFromDataString: field name is longer than field list!!!");
		return -2;
	}

	/* search for the field in the field list and count the delimiters (= find out ordinal number of field in list) */
	pclFieldPos = pcpFieldList;
	pclStopPos = pcpFieldList + ilFieldListLen - ilFieldLen;

	/* search for the field */
	while (!blFieldFound && (pclFieldPos <= pclStopPos))
	{
		/* search field from current position; make sure, that the whole field was found, not only a substring (like 'AL' in field 'ALI') */
		if (!strncmp(pclFieldPos,pcpField,ilFieldLen) && 
			((*(pclFieldPos+ilFieldLen) == '\0') || (*(pclFieldPos+ilFieldLen) == ',')))
		{
			/* field found */
			blFieldFound = 1;
			dbg(DEBUG,"GetFieldValueFromDataString: field <%s> found at position <%d>.",pcpField,ilNoOfDelim);
		}
		else 
		{
			if (*pclFieldPos == ',')
			{
				/* delimiter found */
				ilNoOfDelim += 1;
			}
			pclFieldPos += 1;
		}
	}

	/* field found? */
	if (!blFieldFound)
	{
		/* field still not found */
		dbg(DEBUG,"GetFieldValueFromDataString: field not found in field list!!!");
		return -3;
	}

	/* now get the field value from the data string */
	ilDataLen = strlen(pcpData);
	pclStopPos = pcpData + ilDataLen;

	/* first: set up the start position of the field value */
	for (pclFieldValuePos = pcpData;(ilNoOfDelim > 0) && (*pclFieldValuePos != '\0');pclFieldValuePos += 1)
	{
		if (*pclFieldValuePos == ',')
		{
			/* start of next field value found -> count down the delimiter counter */
			ilNoOfDelim -= 1;
		}
	}

	/* everything ok so far? */
	if ((ilNoOfDelim != 0) || (pclFieldValuePos > pclStopPos) || (*pclFieldValuePos == '\0'))
	{
		/* no -> something went wrong */
		dbg(DEBUG,"GetFieldValueFromDataString: error finding field value in data string <%s>!!!",pcpData);
		return -4;
	}

	/* second: the start position is set, now copy bytes from the data string, until the 
	   end or the next delimiter is found */
	while ((pclFieldValuePos <= pclStopPos) && (*pclFieldValuePos != ','))
	{
		/* copy the next byte */
		*(pcpFieldValue+ilFieldValueLen) = *pclFieldValuePos;
		pclFieldValuePos += 1;
		ilFieldValueLen += 1;
	}
	/* terminate the string */
	*(pcpFieldValue+ilFieldValueLen) = '\0';
	dbg(DEBUG,"GetFieldValueFromDataString: value of field <%s> is <%s>, length is <%d>.",pcpField,pcpFieldValue,strlen(pcpFieldValue));

	/* everything is ok */
	return strlen(pcpFieldValue);
}

/****************************************************************************************/
/* Locate the position of a single field in the given fieldlist and return its          */
/* value from the data string, which is stored at the same position.                    */
/* Example: try to find the value for the field 'URNO' in the data string               */
/* 'blabla,peterpan,0,0,,,0001234567,string' with the field list                        */
/* 'FLD1,FLD2,FLD3,FLD4,FLD5,FLD6,FLD7,URNO,FLD8' should copy '0001234567' to the       */
/* buffer <pcpFieldValue>.                                                              */
/****************************************************************************************/
static int GetFieldValueFromMonoBlockedDataString(const	char* pcpField, const char* pcpFieldList, 
												  const	char* pcpData, char* pcpFieldValue)
{
	int	ilItemLen;
	int	ilItemNbr;
	const char *pclPos; 
	char *pclDel;

	ilItemLen = strlen(pcpField) + 1;

	/* first clear the return buffer in case it was filled by a prior call with the same pointer */
	*pcpFieldValue = '\0';

	pclPos = strstr(pcpFieldList,pcpField);
	if (pclPos == NULL)
	{
		/* field still not found */
		dbg(DEBUG,"GetFieldValueFromMonoBlockedDataString: field not found in field list!!!");
		return -3;
	}

	ilItemNbr = (pclPos - pcpFieldList) / ilItemLen;

	pclPos = pcpData;
	while(ilItemNbr > 0 && (pclDel = strchr(pclPos,',')) != NULL)
	{
		--ilItemNbr;
		pclPos = pclDel + 1;
	}

	if (pclPos)
	{
		pclDel = strchr(pclPos,',');
		if (pclDel)
		{
			ilItemLen = pclDel - pclPos;
			strncpy(pcpFieldValue,pclPos,ilItemLen);
			pcpFieldValue[ilItemLen] = '\0';
		}
		else
		{
			strcpy(pcpFieldValue,pclPos);
		}
	}
	else
	{
		/* data still not found */
		dbg(DEBUG,"GetFieldValueFromMonoBlockedDataString: data not found in field list!!!");
		return -4;
	}

	/* everything is ok */
	return strlen(pcpFieldValue);
}

/****************************************************************************************/
/* Set next script to execute imediately after executing of current script.	        */
/* This script will be executed even prior to any other script, which may follow when   */
/* working out the script list of an action section.					*/
/****************************************************************************************/
static void SetNextScript(char* pcpNextScript)
{
	/* valid parameter? */
	if ((pcpNextScript == NULL) || (strlen(pcpNextScript) > MAX_FILENAME))
	{
		/* terminate */
		return;
	}
	/* copy name of script */
	strcpy(pcgNextScript,pcpNextScript);
}

/******************************************************************************/
/* Cleanup() closes any CEDAArray, which is not a action recordset. This      */
/* function should be called after execution of a script, in case a script    */
/* does not close its recordsets.                                             */
/******************************************************************************/
static void Cleanup()
{
	int ilLoop;
	/* check CEDAArrays */
	for (ilLoop = 0; ilLoop < MAX_ARRAY; ilLoop += 1)
	{
		if ((sgRecordsets[ilLoop].hHandle != -1) && (sgRecordsets[ilLoop].iAutoClose == 1))
		{
			/* destroy and reinitialize the array */
			dbg(DEBUG,"Cleanup(): closing CEDAArray '%s' at index <%d>",sgRecordsets[ilLoop].cArrayName,ilLoop);
			CloseRecordset(ilLoop,1);
		}
	}
} /* end of Cleanup() */

/******************************************************************************/
/* GetNextActionScript() extracts the next script name from the script        */
/* list of an action section and stores it in the buffer.                     */
/******************************************************************************/
static int GetNextActionScript(int ipIndex, char* pcpScriptName)
{
	char *pclNextDelimiter = NULL;
	int ilLength = 0;

	/* check the breaking execution loop trigger */
	if (igBreakScriptListExecution)
	{
		/* break was triggered -> return 'no more scripts to execute' and terminate */
		dbg(DEBUG,"GetNextActionScript: stop of script execution was triggered.",ipIndex);
		/* re-initialize trigger */
		igBreakScriptListExecution = 0;
		return 0;
	}

	/* check the index */
	if ((ipIndex < 0) || (ipIndex >= MAX_ACTION_SECTION))
	{
		/* error: invalid index */
		dbg(DEBUG,"GetNextActionScript: invalid index <%d>!!!",ipIndex);
		return 0;
	}

	/* is there a script name anyway? */
	if (*sgActionSections[ipIndex].cScriptName == '\0')
	{
		/* error: invalid index */
		dbg(DEBUG,"GetNextActionScript: <sgActionSections[ipIndex].cScriptName> is empty!!!",ipIndex);
		return 0;
	}

	/* look for next delimiter */
	if ((pclNextDelimiter = strstr(sgActionSections[ipIndex].pNextScript,",")) != NULL)
	{
		/* copy script name up to delimiter position */
		ilLength = pclNextDelimiter-sgActionSections[ipIndex].pNextScript;
		strncpy(pcpScriptName,sgActionSections[ipIndex].pNextScript,ilLength);
		/* zero terminate */
		*(pcpScriptName+ilLength) = '\0';
		/* set the position of the next script beginning */
		sgActionSections[ipIndex].pNextScript = pclNextDelimiter+1;
		/* return: script name found and copied */
		return 1;
	}
	else if (*sgActionSections[ipIndex].pNextScript != '\0')
	{
		/* copy the last script name (which is not delimited by a comma) */
		strcpy(pcpScriptName,sgActionSections[ipIndex].pNextScript);
		/* set the position of the next script beginning */
		sgActionSections[ipIndex].pNextScript += strlen(pcpScriptName);
		/* return: script name found and copied */
		return 1;
	}
	return 0; /* no further script name */
}

/******************************************************************************/
/* GetNextXBSScript() extracts the next script name from the script           */
/* list of a XBSCommand struct and stores it in the buffer. If the path       */
/* buffer of the struct is not empty, it is added to the scriptname.          */
/* Return:	<0:  error                                                    */
/*          	=0:  ok, but no more further scripts                          */
/*		>0:  ok, more scripts to do                                   */
/******************************************************************************/
static int GetNextXBSScript(int ipIndex, char* pcpScriptName)
{
	char *pclNextDelimiter = NULL;
	int ilLength = 0;
	char clScriptName[MAX_SCRIPTNAME];

	/* check the breaking execution loop trigger */
	if (igBreakScriptListExecution)
	{
		/* break was triggered -> return 'no more scripts to execute' and terminate */
		dbg(DEBUG,"GetNextXBSScript: stop of script execution was triggered.",ipIndex);
		/* re-initialize trigger */
		igBreakScriptListExecution = 0;
		return 0;
	}

	/* check the index */
	if ((ipIndex < 0) || (ipIndex >= MAX_XBS_COMMANDS))
	{
		/* error: invalid index */
		dbg(DEBUG,"GetNextXBSScript: invalid index <%d>!!!",ipIndex);
		return -1;
	}

	/* is there a script name anyway? */
	if (*sgXBSCommands[ipIndex].cScriptList == '\0')
	{
		/* error: invalid index */
		dbg(DEBUG,"GetNextXBSScript: <sgXBSCommands[ipIndex].cScriptList> is empty!!!",ipIndex);
		return -2;
	}

	/* init buffer */
	*clScriptName = '\0';

	/* look for next delimiter */
	if ((pclNextDelimiter = strstr(sgXBSCommands[ipIndex].pNextScript,",")) != NULL)
	{
		/* copy script name up to delimiter position */
		ilLength = pclNextDelimiter-sgXBSCommands[ipIndex].pNextScript;
		strncpy(clScriptName,sgXBSCommands[ipIndex].pNextScript,ilLength);
		/* zero terminate */
		clScriptName[ilLength] = '\0';
		/* set the position of the next script beginning */
		sgXBSCommands[ipIndex].pNextScript = pclNextDelimiter+1;
	}
	else if (*sgXBSCommands[ipIndex].pNextScript != '\0')
	{
		/* copy the last script name (which is not delimited by a comma) */
		strcpy(clScriptName,sgXBSCommands[ipIndex].pNextScript);
		/* set the position of the next script beginning */
		sgXBSCommands[ipIndex].pNextScript += strlen(clScriptName);
	}
	else 
	{
		dbg(DEBUG,"GetNextXBSScript: no more scripts to execute.");
		return 0; /* no more scripts to execute */
	}

	/* use path? */
	if (*sgXBSCommands[ipIndex].cPath != '\0')
	{
		/* yes -> copy to buffer */
		strcpy(pcpScriptName,sgXBSCommands[ipIndex].cPath);
		/* append extracted script name */
		strcat(pcpScriptName,clScriptName);
	}
	else
	{
		/* no -> copy scriptname to parameter buffer */
		strcpy(pcpScriptName,clScriptName);
	}

	/* script found and copied */
	return 1;
}

/******************************************************************************/
/* GetHopo() get a copy of the config-param HOPO. Default is 'FRA'.           */
/* Return: length of Hopo (max. 5).                                           */
/******************************************************************************/
static int GetHopo(char *pcpHopo)
{
	if ((pcgHopo == NULL) || (*pcgHopo == '\0'))
	{
		*pcpHopo = '\0';
		return 0;
	}
	/* copy hopo */
	strcpy(pcpHopo,pcgHopo);
	return strlen(pcgHopo);
}

/******************************************************************************/
/* InitXBSCommands(): initialize global data for XBSCommands.                 */
/******************************************************************************/
static void InitXBSCommands()
{
	int ilCount;
	/* initialize all structs */
	for (ilCount = 0; ilCount < MAX_XBS_COMMANDS; ilCount++)
	{
		*sgXBSCommands[ilCount].cCmd = '\0';
		*sgXBSCommands[ilCount].cScriptList = '\0';
		sgXBSCommands[ilCount].pNextScript = sgXBSCommands[ilCount].cScriptList;
		*sgXBSCommands[ilCount].cPath = '\0';
	}
}

/********************************************************************************/
/* FindFreeXBSCommandIndex(): get the index of the next free XBSCommand       	*/
/*			      in global data					*/
/* Return: index (>=0) or error code (<0)                                  	*/
/********************************************************************************/
static int FindFreeXBSCommandIndex(const char* pcpCheckCommand)
{
	int ilCount;
	/* search for free struct, compare existing commands to param, if given */
	for (ilCount = 0; ilCount < MAX_XBS_COMMANDS; ilCount++)
	{
		/* struct in use? */
		if (*sgXBSCommands[ilCount].cCmd == '\0')
		{
			/* no -> return index */
			dbg(DEBUG,"FindFreeXBSCommandIndex(): found free struct for XBSCommand '%s' at index <%d>",pcpCheckCommand,ilCount);
			return ilCount;
		}
		else if ((pcpCheckCommand != NULL) && (strcmp(pcpCheckCommand,sgXBSCommands[ilCount].cCmd) == 0))
		{
			/* command already exists -> return error code */
			dbg(TRACE,"FindFreeXBSCommandIndex(): XBSCommand '%s' is used already at index <%d>",pcpCheckCommand,ilCount);
			return -1;
		}
	}
	/* when we get here, no struct is free -> return error code */
	dbg(TRACE,"FindFreeXBSCommandIndex(): could not add XBSCommand '%s' (no free struct left)!!! ",pcpCheckCommand,ilCount);
	return -2;
}

/******************************************************************************/
/* AddXBSCommand(): Add a XBSCommand to global data.                          */
/* The purpose of XBSCommands is to have a list of scripts executed in        */
/* a defined order. The execution is started by a broadcast, which can be     */
/* sent by any client application. The XBSCommands are configured in the init */
/* script which is executed when the handler starts. Since there is no way to */
/* delete a XBSCommand at runtime without reinitialize the header, it makes   */
/* no sense to call AddXBSCommand() after initialization (though possible,    */
/* it is a function accessible from any basic script at any time).            */
/* XBS stannds for eXecute Basic Script and is the main command in the        */
/* broadcast, the sub command, which identifies the XBSCommand struct is      */
/* coded in the broadcasts selection key buffer.                              */
/* Return:	index (>=0) or error code (<0)                                */
/******************************************************************************/
static int AddXBSCommand(const char *pcpCmd, const char *pcpScriptlist, const char *pcpPath)
{
	int ilIndex = -1;
	/* get free struct */
	if ((ilIndex= FindFreeXBSCommandIndex(pcpCmd)) < 0)
	{
		/* error -> terminate */
		return ilIndex;
	}
	/* struct found, set data */
	strcpy(sgXBSCommands[ilIndex].cCmd,pcpCmd);
	strcpy(sgXBSCommands[ilIndex].cPath,pcpPath);
	strcpy(sgXBSCommands[ilIndex].cScriptList,pcpScriptlist);
	sgXBSCommands[ilIndex].pNextScript = sgXBSCommands[ilIndex].cScriptList;
	/* everything is fine */
	return ilIndex;
}

/******************************************************************************/
/* CheckCommandAndGetXBSCommandIndex(): Search for the gibven command         */
/*  (<pcpCmd>) in the XBSCommand structs.                                     */
/* Return:	the index of the function if found (>=0)                      */
/*			else an error code (<0)	                              */
/******************************************************************************/
static int CheckCommandAndGetXBSCommandIndex(char *pcpCmd)
{
	int ilCount;
	/* search for command in the XBSCommand structs */
	for (ilCount = 0; ilCount < MAX_XBS_COMMANDS; ilCount++)
	{
		/* command found? */
		if ((pcpCmd != NULL) && (strcmp(pcpCmd,sgXBSCommands[ilCount].cCmd) == 0))
		{
			/* command found -> return index */
			dbg(DEBUG,"FindFreeXBSCommandIndex(): found XBSCommand '%s' at index <%d>",pcpCmd,ilCount);
			return ilCount;
		}
	}
	/* when we get here, we did not found the command -> return error code */
	dbg(TRACE,"FindFreeXBSCommandIndex(): could not find XBSCommand '%s'!!! ",pcpCmd,ilCount);
	return -1;
}

/**********************************************************************************/
/* APO 09.05.2001: - function will overwrite existing fields now              	  */
/*                                                                            	  */
/* WriteTemporaryField(): add an extra field + fieldvalue to an internal      	  */
/*	buffer and fieldlist, which is associated with the last broadcast. That   */
/*	means, that these buffers are set to zero length when a broadcast is      */
/*	received. Then any executed basic script can call this function to add a  */
/*  new parameter to this buffers which can be read later on by other scripts.	  */
/*	The parameter can be read by calling ReadTemporaryField() as long as no   */
/*	new broadcast is received (the buffers will be set to zero length then).  */
/*	The fields stored here have nothing to do with database functionality.    */
/*	This is just a mechanism to temporarily store the results of basic        */
/*	scripts. This way scripts can exchange data, so one script does not have  */
/*	to do the same work leading to the same results like a script executed    */
/*	before. Because the scripts are a direct reaction on a broadcast, the     */
/*	lifetime of the data they generate and store here is limited to the       */
/*	lifetime of the initiating broadcast.                                     */
/* Return:	0	-> parameter/field stored                                 */
/*		-1	-> error: buffer is full                                  */
/*		-2	-> error: failed to locate field value in data string     */
/*		-3	-> error: field to add is too large                       */
/*		-4	-> error: field name includes delimiter, i.e. comma       */
/*		-5	-> error: field value includes delimiter, i.e. comma  	  */
/**********************************************************************************/
static int WriteTemporaryField(char *pcpField, char *pcpFieldValue)
{
	char pclData[MAX_DATA_SIZE];
	int  ilRc;
	long llRowNum = ARR_FIRST;
	long llFieldNum;
	char *pclMatchingRow = NULL;

	llRowNum = ARR_FIRST;
	ilRc = AATArrayFindRowPointer(&hTmpFields,"TMPFLD",&hTmpFieldsInd,"NAME",pcpField,&llRowNum,(void *)&pclMatchingRow);
	if (ilRc != RC_SUCCESS)
	{
		sprintf(pclData,"%s,%s",pcpField,pcpFieldValue);
		dbg(DEBUG,"WriteTemporaryField: calling AATArrayAddRow for <%s>.with <%s>..",pcpField,pclData);
		llRowNum = ARR_FIRST;
		delton(pclData);	/* delimiter to nul byte */
		ilRc = AATArrayAddRow(&hTmpFields,"TMPFLD",&llRowNum,pclData);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"WriteTemporaryField: AATArrayAddRow failed for %s with (error: %s)","TMPFLD",CedaArrayErrorCodeToString(ilRc));
			return -1;
		}
	}
	else
	{
		dbg(DEBUG,"WriteTemporaryField: calling AATArrayPutField for <%s>...",pcpField);
		llFieldNum = -1;
		ilRc = AATArrayPutField(&hTmpFields,"TMPFLD",NULL,"VALUE",llRowNum,FALSE,pcpFieldValue);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"WriteTemporaryField: AATArrayPutField failed for %s with (error: %s)","TMPFLD",CedaArrayErrorCodeToString(ilRc));
			return -1;
		}
	}
	return 0;
}

/******************************************************************************/
/* ReadTemporaryField(): read an extra field + fieldvalue from the internal   */
/*	buffer and fieldlist (see WriteTemporaryField() for a detailed        */
/*	description).                                                         */
/* Return:	>=0	-> parameter/field found, size of data                */
/*		-1	-> error: field not found                             */
/******************************************************************************/
static int ReadTemporaryField(char *pcpField, char *pcpFieldValue)
{
	char pclData[MAX_EXTRA_BUF+1];
	int ilRc;
	long llRowNum = ARR_FIRST;
	long llFieldNum = -1;
	char *pclMatchingRow = NULL;

	llRowNum = ARR_FIRST;
	ilRc = AATArrayFindRowPointer(&hTmpFields,"TMPFLD",&hTmpFieldsInd,"NAME",pcpField,&llRowNum,(void *)&pclMatchingRow);

	if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"ReadTemporaryField: calling AATArrayGetField for <%s>...",pcpField);
		ilRc = AATArrayGetField(&hTmpFields,"TMPFLD",&llFieldNum,"VALUE",MAX_EXTRA_BUF+1,llRowNum,(void *)pclData);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"ReadTemporaryField: AATArrayGetField failed for %s with (error: %s)",pcpField,CedaArrayErrorCodeToString(ilRc));
			return 0;
		}
		else
		{
			rtrim(pclData);
			dbg(DEBUG,"ReadTemporaryField: AATArrayGetField for <%s> returns <%s>",pcpField,pclData);
			strcpy(pcpFieldValue,pclData);
			return strlen(pcpFieldValue);
		}
	}
	else
	{
		dbg(DEBUG,"ReadTemporaryField: ERROR: calling AATArrayFindRowPointer failed for <%s>...",pcpField);
	}
}

/******************************************************************************/
/* PrintTimeStamp: prints out a duration message to the log file.             */
/*	If <ipInit> is set (!= 0), then the start time is stored.				  */
/* Return: none                                                               */
/******************************************************************************/
static void PrintTimeStamp(int ipInit)
{
	static struct timeb slStart;
	struct timeb slFinish;
	static int ipFirstCall = 1;
	double dlStartMSec, dlFinMSec;
	double dlStart,dlFin, dlDuration;

	/* very first call of function at runtime or init start time? */
	if ((ipFirstCall == 1) || (ipInit == 1))
	{
		/* get the start time = current time */
#ifdef _SUN_UNIX
		ftime(&slStart);
#elif defined _SINIX
		ftime(&slStart);
#elif defined _LINUX
	ftime(&slStart);
#else
		_ftime(&slStart);
#endif
		/* print message */
		dbg(TRACE,"Start time for time stamp set.");
		/* function called at least one time */
		ipFirstCall = 0;
		return;
	}

	/* get the finish time */
#ifdef _SUN_UNIX
	ftime(&slFinish);
#elif defined _SINIX
	ftime(&slFinish);
#elif defined _LINUX
	ftime(&slStart);
#else
	_ftime(&slFinish);
#endif

	/* start and finish time in millesecs */
	dlStartMSec = slStart.millitm;
	dlStartMSec /= 1000;
	dlStart	= slStart.time + dlStartMSec;
	dlFinMSec = slFinish.millitm;
	dlFinMSec /= 1000;
	dlFin = slFinish.time + dlFinMSec;
	/* duration in secs.millesecs */
	dlDuration = dlFin - dlStart;

	dbg(TRACE,"%lf seconds have passed...",dlDuration);
} /* end of PrintTimeStamp() */

/******************************************************************************/
/* ResetOpenJobs(): re-initialize global data for open jobs (= XBSCommands    */
/*  executed at a certain date/time one time or monthly).	              */
/******************************************************************************/
static void ResetOpenJobs()
{
	int ilCount;
	/* initialize all structs */
	for (ilCount = 0; ilCount < MAX_OPEN_JOBS; ilCount++)
	{
		FreeOpenJob(ilCount);
	}
	/* re-initialize global data */
	igNextJobToExecuteIndex = -1;
	igHasOpenJobs = FALSE;
}

/******************************************************************************/
/* InitOpenJobs(): initialize global data for open jobs (= XBSCommands        */
/*  executed at a certain date/time one time or monthly).	              */
/******************************************************************************/
static void InitOpenJobs()
{
	int ilCount;
	/* initialize all structs */
	for (ilCount = 0; ilCount < MAX_OPEN_JOBS; ilCount++)
	{
		/* free memory and reset data */
		strcpy(sgOpenJobs[ilCount].cXBSCmd,"");
		strcpy(sgOpenJobs[ilCount].cNextExecution,"");
		sgOpenJobs[ilCount].pcAdditionalInfo = NULL;
		sgOpenJobs[ilCount].pcEmplUrnoList = NULL;
		sgOpenJobs[ilCount].iExecutionMode = XBS_EXECUTION_NOW;
		sgOpenJobs[ilCount].iMonthOffset = 0;
	}
}

/******************************************************************************/
/* FreeOpenJob(): releases all memory allocated for an open-job-struct and    */
/*  resets all other data.                                                    */
/******************************************************************************/
static void FreeOpenJob(int ipCount)
{
	/* check index */ 
	if ((ipCount < 0) || (ipCount >= MAX_OPEN_JOBS))
	{
		/* invalid index -> print debug warning and terminate function */
		dbg(TRACE,"FreeOpenJob: invalid index <%d>",ipCount);
		return;
	}

	/* free memory and reset data */
	strcpy(sgOpenJobs[ipCount].cXBSCmd,"");
	strcpy(sgOpenJobs[ipCount].cNextExecution,"");
	if (sgOpenJobs[ipCount].pcAdditionalInfo != NULL)
	{
		free((void*)sgOpenJobs[ipCount].pcAdditionalInfo);
		sgOpenJobs[ipCount].pcAdditionalInfo = NULL;
	}
	if (sgOpenJobs[ipCount].pcEmplUrnoList != NULL)
	{
		free((void*)sgOpenJobs[ipCount].pcEmplUrnoList);
		sgOpenJobs[ipCount].pcEmplUrnoList = NULL;
	}
	sgOpenJobs[ipCount].iExecutionMode = XBS_EXECUTION_NOW;
	sgOpenJobs[ipCount].iMonthOffset = 0;
}

/******************************************************************************/
/* GetFreeOpenJobStruct(): get a free open job struct (command must be blank).*/
/* Return: >= 0 -> the index of the free struct                               */
/*         < 0  -> error, no free struct                                      */
/******************************************************************************/
static int GetFreeOpenJobStruct()
{
	int ilCount;
	/* initialize all structs */
	for (ilCount = 0; ilCount < MAX_OPEN_JOBS; ilCount++)
	{
		/* struct free? */
		if ((*sgOpenJobs[ilCount].cXBSCmd == '\0') && 
			(sgOpenJobs[ilCount].pcAdditionalInfo == NULL) &&
			(sgOpenJobs[ilCount].pcEmplUrnoList == NULL)) 
		{
			/* fee struct found -> return index */
			return ilCount;
		}
	}
	/* no free struct found -> return error code */
	return -1;
}

/******************************************************************************/
/* AddOpenJob2Array: add an open job to the internal array of open job        */
/*  structs.                                                                  */
/* Return: >=0	-> everything ok, return value = index of open job            */
/*	   <0	-> error (see return buffer for detailed error info)          */
/******************************************************************************/
static int AddOpenJob2Array(char *pcpXBSCommand, char *pcpInfo, char *pcpData, 
							char *pcpResultBuf)
{
	int ilExecutionMode, ilMonthOffset;
	char pclHour[3], pclMin[3], pclDay[3], pclFrom[9], pclTo[9],
		 pclFirstExecYear[5], pclFirstExecMonth[3], pclFirstExecDay[3],
		 pclFirstExecHour[3], pclFirstExecMin[3];
	char pclExecutionMode[2], pclMonthOffset[4];
	int ilJobIndex, ilResult;
	time_t rlTimeNow;

	/* check info string, expected: */
	/* 'YYYYMMDD,YYYYMMDD,X,HHMM,DD,VOO,YYYYMMDDHHMM' - FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC */
	/* additionally accepted :		*/
	/* 'YYYYMMDD,YYYYMMDD,X,HHMM,DD,VOO,YYYYMMDDHHMM,NNNNNNNNN' - FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC,BCNUM */
	/* Offset must be '<sign><01-12>' */
	if (strlen(pcpInfo) < strlen("YYYYMMDD,YYYYMMDD,X,HHMM,DD,VOO,YYYYMMDDHHMM"))
	{
		/* error */
		sprintf(pcpResultBuf,"can not add job, erroneous info string length %d",strlen(pcpInfo));
		dbg(TRACE,"AddOpenJob2Array: %s",pcpResultBuf);
		return -1;
	}

	/* get the index of the next free open job struct and store it */
	if ((ilJobIndex = GetFreeOpenJobStruct()) < 0)
	{
		/* error */
		strcpy(pcpResultBuf,"can not add job, no free internal struct left");
		return -2;
	}

	/* extract parameter FROM */
	strncpy(pclFrom,pcpInfo,8);
	/* zero terminate buffer */
	pclFrom[8] = '\0';
	/* extract parameter TO */
	strncpy(pclTo,pcpInfo + 9,8);
	/* zero terminate buffer */
	pclTo[2] = '\0';

	/* extract execution mode */
	strncpy(pclExecutionMode,pcpInfo + 18,1);
	/* zero terminate buffer */
	pclExecutionMode[2] = '\0';
	/* convert to int */
	ilExecutionMode = atoi(pclExecutionMode);

	/* extract execution time: hour */
	strncpy(pclHour,pcpInfo + 20,2);
	/* zero terminate buffer */
	pclHour[2] = '\0';
	/* extract execution time: min */
	strncpy(pclMin,pcpInfo + 22,2);
	/* zero terminate buffer */
	pclMin[2] = '\0';

	/* extract execution day of month */
	strncpy(pclDay,pcpInfo + 25,2);
	/* zero terminate buffer */
	pclDay[2] = '\0';

	/* month offset from current month */
	strncpy(pclMonthOffset,pcpInfo + 28,3);
	/* zero terminate buffer */
	pclMonthOffset[3] = '\0';
	/* convert to int */
	ilMonthOffset = atoi(pclMonthOffset);

	/* extract year of first execution */
	strncpy(pclFirstExecYear,pcpInfo + 32,4);
	/* zero terminate buffer */
	pclFirstExecYear[4] = '\0';
	/* extract month of first execution */
	strncpy(pclFirstExecMonth,pcpInfo+36,2);
	/* zero terminate buffer */
	pclFirstExecMonth[2] = '\0';
	/* extract day of first execution */
	strncpy(pclFirstExecDay,pcpInfo+38,2);
	/* zero terminate buffer */
	pclFirstExecDay[2] = '\0';
	/* extract hour of first execution */
	strncpy(pclFirstExecHour,pcpInfo+40,2);
	/* zero terminate buffer */
	pclFirstExecHour[2] = '\0';
	/* extract min of first execution */
	strncpy(pclFirstExecMin,pcpInfo+42,2);
	/* zero terminate buffer */
	pclFirstExecMin[2] = '\0';

	/* allocate memory for struct members and store data */
	if ((sgOpenJobs[ilJobIndex].pcAdditionalInfo = (char*)malloc(sizeof(char) * (strlen(pcpInfo)+1))) == NULL)
	{
		/* error allocating memory */
		strcpy(pcpResultBuf,"can not add job, error allocating memory for info string");
		return -3;
	}
	if ((sgOpenJobs[ilJobIndex].pcEmplUrnoList = (char*)malloc(sizeof(char) * (strlen(pcpData)+1))) == NULL)
	{
		/* error allocating memory */
		strcpy(pcpResultBuf,"can not add job, error allocating memory for urno list");
		free(sgOpenJobs[ilJobIndex].pcAdditionalInfo);
		return -3;
	}
	strcpy(sgOpenJobs[ilJobIndex].cXBSCmd,pcpXBSCommand);
	strcpy(sgOpenJobs[ilJobIndex].pcAdditionalInfo,pcpInfo);
	strcpy(sgOpenJobs[ilJobIndex].pcEmplUrnoList,pcpData);
	sgOpenJobs[ilJobIndex].iExecutionMode = ilExecutionMode;
	sgOpenJobs[ilJobIndex].iMonthOffset = ilMonthOffset;
	dbg(DEBUG,"AddOpenJob2Array: setting open jobs at index <%d>",ilJobIndex);
	dbg(DEBUG,"AddOpenJob2Array: info  = <%s>",sgOpenJobs[ilJobIndex].pcAdditionalInfo);
	dbg(DEBUG,"AddOpenJob2Array: urnos = <%s>",sgOpenJobs[ilJobIndex].pcEmplUrnoList);

	/* initialize time to execute */
	time(&rlTimeNow);
	sgOpenJobs[ilJobIndex].sgNextExecution = *localtime(&rlTimeNow);
	/* set time (ATTENTION: no validation is done !!! Must be done by client, who sended the event!!) */
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_year = (atoi(pclFirstExecYear) - 1900);
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_mon = (atoi(pclFirstExecMonth) - 1);
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_mday = atoi(pclFirstExecDay);
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_hour = atoi(pclFirstExecHour);
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_min = atoi(pclFirstExecMin);
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_sec = 0;
	sgOpenJobs[ilJobIndex].sgNextExecution.tm_isdst = -1;
	ilResult = mktime(&sgOpenJobs[ilJobIndex].sgNextExecution);
	dbg(DEBUG,"AddOpenJob2Array: execution for open job at index <%d> is set to %s",ilJobIndex,asctime(&sgOpenJobs[ilJobIndex].sgNextExecution));

	/* everything is fine */
	return ilJobIndex;
}

/******************************************************************************/
/* AddOpenJob: add an open job to the file with open jobs. <pcgResultBuf> is  */
/*  set either to 'OK' if everything is ok or to any other error string, if   */
/*  an error occurred, so the user can check this buffer to determine the     */
/*  result of the function.                                                   */
/* Return:	0	-> everything ok                                      */
/*		<0	-> error (see return buffer for detailed error info)  */
/******************************************************************************/
static int AddOpenJob(char *pcpXBSCommand, char *pcpInfo, char *pcpData, 
					  char *pcpResultBuf)
{
	FILE *fOpenJobs;
	char clDataSize[6];
	char clInfoSize[6];
	int ilDataSize;
	int ilIndex;
	time_t rlTimeNow;
	struct tm rlTmTimeNow;

	if ((ilIndex = AddOpenJob2Array(pcpXBSCommand,pcpInfo,pcpData,pcpResultBuf)) < 0)
	{
		/* could not add to internal array -> terminate */
		return ilIndex;
	}

	/* get current time */
	time(&rlTimeNow);
	rlTmTimeNow = *localtime(&rlTimeNow);

	/* check timer: must be future */
	if (CompareTime(sgOpenJobs[ilIndex].sgNextExecution,rlTmTimeNow) != TIME1_LATER_THAN_TIME2)
	{
		strcpy(pcpResultBuf,"could not add job, execution time is in the past");
		dbg(TRACE,"AddOpenJob: can not add open job, timer is in the past (= %s)",asctime(&sgOpenJobs[ilIndex].sgNextExecution));
		FreeOpenJob(ilIndex);
		return -6;
	}

	/* calculate data size */
	ilDataSize = strlen(pcpXBSCommand) + 5 + strlen(pcpInfo) + strlen(pcpData) + 2 * strlen(pcgDelim);
	/* avoid buffer overflow */
	if ((ilDataSize > 99999) || (ilDataSize > MAX_DATA_SIZE)) 
	{
		strcpy(pcpResultBuf,"internal error");
		dbg(TRACE,"AddOpenJob: can not add open job, data size too large (>99999 bytes)");
		return -4;
	}
	sprintf(clDataSize,"%0.5d",ilDataSize);

	sprintf(clInfoSize,"%0.5d",strlen(pcpInfo));

	/* save job to disc */
	/* create filename */
	sprintf(pcgOpenJobFileName,"%s/openjob.%s", getenv("CFG_PATH"),mod_name);
	/* open file */
	if((fOpenJobs = fopen(pcgOpenJobFileName,"a+")) != NULL)
	{
		/* write data size first */
		fwrite(clDataSize,sizeof(char),5,fOpenJobs);
		/* write data */
		fwrite(clInfoSize,sizeof(char),5,fOpenJobs);					/* sizeof info	*/
		fwrite(pcpInfo,sizeof(char),strlen(pcpInfo),fOpenJobs);				/* info		*/
		fwrite(pcgDelim,sizeof(char),strlen(pcgDelim),fOpenJobs);			/* ','		*/
		fwrite(pcpXBSCommand,sizeof(char),strlen(pcpXBSCommand),fOpenJobs); 		/* command	*/
		fwrite(pcgDelim,sizeof(char),strlen(pcgDelim),fOpenJobs);			/* ','		*/
		fwrite(pcpData,sizeof(char),strlen(pcpData),fOpenJobs);				/* data (urnos) */
		fwrite(pcgCrlf,sizeof(char),strlen(pcgCrlf),fOpenJobs);				/* '\n'		*/
		/* cleanup */
		fclose(fOpenJobs);
		/* signal ok to user */
		strcpy(pcpResultBuf,"OK");
		/* set next open job to execute */
		SetNextOpenJob();
	}
	else
	{
		/* error opening file */
		sprintf(pcpResultBuf,"can not add job, error opening file '%s'",pcgOpenJobFileName);
		dbg(TRACE,"AddOpenJob: %s",pcpResultBuf);
		return -5;
	}

	/* everything is fine */
	return 0;
}

/******************************************************************************/
/* RestoreOpenJobs: look for the file /ceda/conf/openjob.<mod_name> and, if   */
/* found, restore the start time for execution of open jobs. This function    */
/* is only executed one time at runtime: directly after startup. Open jobs    */
/* are stored in this file in case the handler was shutdown or crashed.       */
/* This way we can restore open jobs.                                         */
/* Return: 0 if everything is ok, else <0                                     */
/******************************************************************************/
static int RestoreOpenJobs(void)
{
	FILE *fOpenJobs;
	char pclNextBlock[MAX_DATA_SIZE+1], pclInfo[MAX_DATA_SIZE+1];
	char pclData[MAX_DATA_SIZE], pclXBSCommand[13], pclErrorBuf[MAX_DATA_SIZE];
	char clInfoSize[6];
	char *pclDelim, *pclNextDelim;
	int ilBlockSize, ilXBSCmdLength;
	int ilXBSInfoLength;
	int ilXBSDataLength;

	/* initialize data */
	igHasOpenJobs = FALSE;

	/* create filename */
	sprintf(pcgOpenJobFileName,"%s/openjob.%s", getenv("CFG_PATH"),mod_name);

	/* open file */
	if((fOpenJobs = fopen(pcgOpenJobFileName,"r")) == NULL)
	{
		/* error opening file -> terminate */
		dbg(DEBUG,"RestoreOpenJobs: error opening file <%s>",pcgOpenJobFileName);
		return -1;
	}

	/* read open jobs */
	while ((ilBlockSize = GetNextOpenJobFromFile(fOpenJobs,pclNextBlock)) > 0)
	{
		dbg(DEBUG,"RestoreOpenJobs: GetNextOpenJobFromFile() returns <%d>",ilBlockSize);

		/* now extract the info data size	*/
		strncpy(clInfoSize,pclNextBlock,5);
		clInfoSize[5]   = '\0';
		ilXBSInfoLength = atoi(clInfoSize);

		/* check info length	*/
		if (ilXBSInfoLength < igXBSInfoLength)
		{
			/* error: info too short -> terminate */
			dbg(DEBUG,"RestoreOpenJobs: error: XBSInfo length too short (<%d>)",ilXBSInfoLength);
			fclose(fOpenJobs);
			return -3;
		}

		/* extract info block */
		strncpy(pclInfo,pclNextBlock+5,ilXBSInfoLength);
		pclInfo[ilXBSInfoLength] = '\0';

		/* now extract the info data	*/
		if ((pclDelim = strstr(pclNextBlock+5+ilXBSInfoLength,pcgDelim)) == NULL)
		{
			/* error -> terminate */
			dbg(DEBUG,"RestoreOpenJobs: error extracting XBSCommand from <%s>",pclNextBlock);
			fclose(fOpenJobs);
			return -2;
		}

		/* preset info length with info size + info data length */
		ilXBSInfoLength = pclDelim - pclNextBlock;

		/* now extract the command name */
		if ((pclDelim = strstr(pclNextBlock+ilXBSInfoLength+1,pcgDelim)) == NULL)
		{
			/* error -> terminate */
			dbg(DEBUG,"RestoreOpenJobs: error extracting XBSCommand from <%s>",pclNextBlock);
			fclose(fOpenJobs);
			return -2;
		}
		/* check command length */
		ilXBSCmdLength = pclDelim - (pclNextBlock+ilXBSInfoLength+1);
		if (ilXBSCmdLength > 12)
		{
			/* error: command too long -> terminate */
			dbg(DEBUG,"RestoreOpenJobs: error: XBSCommand length too large (<%d>)",ilXBSCmdLength);
			fclose(fOpenJobs);
			return -3;
		}
		strncpy(pclXBSCommand,pclNextBlock+ilXBSInfoLength+1,ilXBSCmdLength);
		pclXBSCommand[ilXBSCmdLength] = '\0';

		/* check data length	*/
		ilXBSDataLength = ilBlockSize - ilXBSCmdLength - ilXBSInfoLength - (2*strlen(pcgDelim));
		if (ilXBSDataLength > MAX_DATA_SIZE)
		{
			/* error: data too long -> terminate */
			dbg(DEBUG,"RestoreOpenJobs: error: XBSData length too large (<%d>)",ilXBSDataLength);
			fclose(fOpenJobs);
			return -3;
		}

		/* the remainder is the urno list */
		strncpy(pclData,pclNextBlock+ilXBSInfoLength+1+ilXBSCmdLength+1,ilXBSDataLength);
		pclData[ilXBSDataLength] = '\0';

		/* now add the job */
		if (AddOpenJob2Array(pclXBSCommand,pclInfo,pclData,pclErrorBuf) < 0)
		{
			dbg(DEBUG,"RestoreOpenJobs: could not add open job ('%s')!!!",pclErrorBuf);
			fclose(fOpenJobs);
			return -4;
		}
	}
	/* no (more) open open jobs -> terminate */
	fclose(fOpenJobs);

	/* set the next open job to execute */
	SetNextOpenJob();

	return 0;
}


/******************************************************************************/
/* GetNextOpenJobFromFile: read the next open job from the opened open job    */
/*  file.                                                                     */
/* Return:	>0	-> datasize, everything is ok                         */
/*		<0	-> error                                              */
/******************************************************************************/
static int GetNextOpenJobFromFile(FILE *hpOpenJobsFile, char* pcgDataBlock)
{
	char clNextBlockSize[6];
	int ilNextBlockSize, ilNumRead;

	/* check next job time (read the first 4 chars, which are the length of the first data block) */
	if (fread(clNextBlockSize,sizeof(char),5,hpOpenJobsFile) != 5)
	{
		/* no data in file or just eof */

		if (!feof(hpOpenJobsFile)) dbg(DEBUG,"GetNextOpenJobFromFile: could not read data size.");
		return -1;
	}

	/* zero terminate block size */
	clNextBlockSize[5] = '\0';
	/* get data size */
	ilNextBlockSize = atoi(clNextBlockSize);
	/* check data size */
	if (ilNextBlockSize > MAX_DATA_SIZE)
	{
		/* data block too large or error (bad file pointer position because of corrupt file) */
		dbg(DEBUG,"GetNextOpenJobFromFile: data block too large.");
		return -2;
	}

	/* read next data block */
	if ((ilNumRead = fread(pcgDataBlock,sizeof(char),ilNextBlockSize,hpOpenJobsFile)) != ilNextBlockSize)
	{
		/* error: no data in file */
		dbg(DEBUG,"GetNextOpenJobFromFile: could not read data block.");
		return -3;
	}
	dbg(DEBUG,"GetNextOpenJobFromFile: next data block = '%s'",pcgDataBlock);
	/* set file pointer to next data block (overread the next '\n') */
	fseek(hpOpenJobsFile,(long)strlen(pcgCrlf),SEEK_CUR);
	return ilNextBlockSize;
}

/******************************************************************************/
/* SetOpenJobNextExecutionTime: sets the next execution time of an open job.  */
/* Return: none                                                               */
/******************************************************************************/
static void SetOpenJobNextExecutionTime(int ipJobIndex)
{
	char pclNextExec[13];		/* to replace the FIRSTEXEC-param in info string, format YYYYMMDDHHMM */
	struct tm rlFrom, rlTo;		/* for constructing the parameters FROM and TO */
	time_t rlTimeNow;			/* for constructing the parameters FROM and TO */
	char pclYearFrom[5], pclYearTo[5], pclMonthFrom[3], pclMonthTo[3], pclDayFrom[3], pclDayTo[3];
	char pclFrom[9], pclTo[9];

	/* calculate new exec time (one month from current) */
	AddMonth(&sgOpenJobs[ipJobIndex].sgNextExecution,1);

	/* initialize time FROM/TO */
	time(&rlTimeNow);
	rlFrom = *localtime(&rlTimeNow);
	rlTo = *localtime(&rlTimeNow);
	/* extract FROM and TO date portions from info string and set time structs */
	strncpy(pclYearFrom,sgOpenJobs[ipJobIndex].pcAdditionalInfo,4);
	pclYearFrom[4] = '\0';
	strncpy(pclMonthFrom,sgOpenJobs[ipJobIndex].pcAdditionalInfo+4,2);
	pclMonthFrom[2] = '\0';
	strncpy(pclDayFrom,sgOpenJobs[ipJobIndex].pcAdditionalInfo+6,2);
	pclDayFrom[2] = '\0';
	strncpy(pclYearTo,sgOpenJobs[ipJobIndex].pcAdditionalInfo+9,4);
	pclYearTo[4] = '\0';
	strncpy(pclMonthTo,sgOpenJobs[ipJobIndex].pcAdditionalInfo+13,2);
	pclMonthTo[2] = '\0';
	strncpy(pclDayTo,sgOpenJobs[ipJobIndex].pcAdditionalInfo+15,2);
	pclDayTo[2] = '\0';
	/* correct time */
	rlFrom.tm_year = (atoi(pclYearFrom) - 1900);
	rlFrom.tm_mon = (atoi(pclMonthFrom) - 1);
	rlFrom.tm_mday = atoi(pclDayFrom);
	if (mktime(&rlFrom) == (time_t)-1)
	{
		dbg(DEBUG,"SetOpenJobNextExecutionTime: mktime() failed, could not set new time!!!");
	}
	rlTo.tm_year = (atoi(pclYearTo) - 1900);
	rlTo.tm_mon = (atoi(pclMonthTo) - 1);
	rlTo.tm_mday = atoi(pclDayTo);
	if (mktime(&rlTo) == (time_t)-1)
	{
		dbg(DEBUG,"SetOpenJobNextExecutionTime: mktime() failed, could not set new time!!!");
	}
	/* calculate new FROM and TO (new exec time plus/minus offset) */
	AddMonth(&rlFrom,1);
	AddMonth(&rlTo,1);

	/*************************************************************************/
	/*************************************************************************/
	/* 3.) store calculations in parameter string ****************************/
	/*************************************************************************/
	/*************************************************************************/
	/* overwrite the parameter FIRSTEXEC in info string so we can restore the info */
	sprintf(pclNextExec,"%.4d%.2d%.2d%.2d%.2d",
						sgOpenJobs[ipJobIndex].sgNextExecution.tm_year+1900,
						sgOpenJobs[ipJobIndex].sgNextExecution.tm_mon+1,
						sgOpenJobs[ipJobIndex].sgNextExecution.tm_mday,
						sgOpenJobs[ipJobIndex].sgNextExecution.tm_hour,
						sgOpenJobs[ipJobIndex].sgNextExecution.tm_min);
	strncpy(sgOpenJobs[ipJobIndex].pcAdditionalInfo+strlen("YYYYMMDD,YYYYMMDD,X,HHMM,DD,V00,"),pclNextExec,12);
	/* overwrite the parameter FROM  */
	sprintf(pclFrom,"%.4d%.2d%.2d",rlFrom.tm_year+1900,rlFrom.tm_mon+1,rlFrom.tm_mday);
	strncpy(sgOpenJobs[ipJobIndex].pcAdditionalInfo,pclFrom,8);
	/* overwrite the parameter TO  */
	sprintf(pclTo,"%.4d%.2d%.2d",rlTo.tm_year+1900,rlTo.tm_mon+1,rlTo.tm_mday);
	strncpy(sgOpenJobs[ipJobIndex].pcAdditionalInfo+strlen("YYYYMMDD,"),pclTo,8);
	dbg(DEBUG,"SetOpenJobNextExecutionTime: new info string of job no.%d: '%s'",ipJobIndex,sgOpenJobs[ipJobIndex].pcAdditionalInfo);
}

/******************************************************************************/
/* AddMonth: adds <ipMonth> to <prpDate>.                                     */
/* Return: 0	-> everything is fine                                         */
/*         <0	-> error                                                      */
/******************************************************************************/
static int AddMonth(struct tm *prpDate, int ipMonth)
{
	int ilMonthCorrection = 0;	/* =1 if we have to determine the last day of month */

	/* if the execution has to be on last day in month we add one month and 
	   substract one day afterwards */
	if (prpDate->tm_mday > 27)
	{
		ilMonthCorrection = 1;
		prpDate->tm_mday = 1;
	}
	/* add the offset to current month */
	prpDate->tm_mon += (ipMonth + ilMonthCorrection);
	/* make sure the year is valid */
	if (prpDate->tm_mon > 11)
	{
		/* we entered the next year */
		prpDate->tm_year += 1;
		prpDate->tm_mon -= 12;
	}
	else if (prpDate->tm_mon < 0)
	{
		/* we entered the year before */
		prpDate->tm_year -= 1;
		prpDate->tm_mon += 12;
	}
	/* make the new time */
	if (mktime(prpDate) == (time_t)-1)
	{
		dbg(DEBUG,"AddMonth: mktime() failed, could not set new time!!!");
		return -1;
	}
	/* time one day if necessary (see abolve) */
	if (ilMonthCorrection)
	{
		prpDate->tm_mday -= 1;
		/* correct the date */
		if (mktime(prpDate) == (time_t)-1)
		{
			dbg(DEBUG,"AddMonth: mktime() failed, could not set new time!!!");
			return -1;
		}
	}
	return 0;
}

/******************************************************************************/
/* ClearOpenJobs: delete data from open job file and reset internal open job  */
/*  data.                                                                     */
/* Return:	none                                                              */
/******************************************************************************/
static void ClearOpenJobs()
{
	FILE *fOpenJobs;
	/* create filename */
	sprintf(pcgOpenJobFileName,"%s/openjob.%s", getenv("CFG_PATH"),mod_name);
	/* open file in overwrite mode to clear file */
	if ((fOpenJobs = fopen(pcgOpenJobFileName,"w")) != NULL)
		fclose(fOpenJobs);
	/* free all memory and re-initialize global data */
	ResetOpenJobs();
}

/******************************************************************************/
/* WriteOpenJobFile: save all open jobs to disk. The existing file will be    */
/*  overwritten.                                                              */
/* Return:	0	-> everything ok                                      */
/*		<0	-> error                                              */
/******************************************************************************/
static int WriteOpenJobFile()
{
	FILE *fOpenJobs;
	char clDataSize[6];
	int ilDataSize;
	int ilCount;

	/* open file in overwrite mode to clear file */
	if ((fOpenJobs = fopen(pcgOpenJobFileName,"w")) == NULL)
	{
		dbg(TRACE,"WriteOpenJobFile: can not open job file '%s'!!!",pcgOpenJobFileName);
		return -1;
	}

	/* examine open job array */
	for (ilCount = 0; ilCount < MAX_OPEN_JOBS; ilCount++)
	{
		/* struct free? */
		if ((*sgOpenJobs[ilCount].cXBSCmd != '\0') && 
			(sgOpenJobs[ilCount].pcAdditionalInfo != NULL) &&
			(sgOpenJobs[ilCount].pcEmplUrnoList != NULL)) 
		{
			/* no -> save to disk */
			/* calculate data size */
			ilDataSize = strlen(sgOpenJobs[ilCount].cXBSCmd) + strlen(sgOpenJobs[ilCount].pcAdditionalInfo) + strlen(sgOpenJobs[ilCount].pcEmplUrnoList) + 2 * strlen(pcgDelim);
			/* avoid buffer overflow */
			if ((ilDataSize > 99999) || (ilDataSize > MAX_DATA_SIZE)) 
			{
				dbg(TRACE,"WriteOpenJobFile: can not add open job, data size too large (>99999 bytes)");
				fclose(fOpenJobs);
				return -2;
			}
			sprintf(clDataSize,"%0.5d",ilDataSize);

			/* write data size first */
			fwrite(clDataSize,sizeof(char),5,fOpenJobs);
			/* write data */
			fwrite(sgOpenJobs[ilCount].pcAdditionalInfo,sizeof(char),strlen(sgOpenJobs[ilCount].pcAdditionalInfo),fOpenJobs);				/* info			*/
			fwrite(pcgDelim,sizeof(char),strlen(pcgDelim),fOpenJobs);			/* ','			*/
			fwrite(sgOpenJobs[ilCount].cXBSCmd,sizeof(char),strlen(sgOpenJobs[ilCount].cXBSCmd),fOpenJobs); /* command		*/
			fwrite(pcgDelim,sizeof(char),strlen(pcgDelim),fOpenJobs);			/* ','			*/
			fwrite(sgOpenJobs[ilCount].pcEmplUrnoList,sizeof(char),strlen(sgOpenJobs[ilCount].pcEmplUrnoList),fOpenJobs);				/* data (urnos) */
			fwrite(pcgCrlf,sizeof(char),strlen(pcgCrlf),fOpenJobs);				/* '\n'			*/
		}
	}
	/* close the file */
	fclose(fOpenJobs);
	/* everything is fine */
	return 0;
}

/****************************************************************************************/
/* SetNextOpenJob: look up the open job array and set the index of the next   		*/
/*	job to execute (<igNextJobToExecuteIndex>) and the time to it's execution	*/
/*  time (<sgNextJobToExecute>).							*/					  
/* Return:	non zero if at least one open job has been found                  	*/
/****************************************************************************************/
static int SetNextOpenJob()
{
	int ilCount;
	time_t rlTimeNow;

	/* initialize global data */
	igNextJobToExecuteIndex = -1;
	igHasOpenJobs = FALSE;

	/* examine open job array */
	for (ilCount = 0; ilCount < MAX_OPEN_JOBS; ilCount++)
	{
		/* struct free? */
		if ((*sgOpenJobs[ilCount].cXBSCmd != '\0') && 
			(sgOpenJobs[ilCount].pcAdditionalInfo != NULL) &&
			(sgOpenJobs[ilCount].pcEmplUrnoList != NULL)) 
		{
			/* no -> check timer */
			if ((igNextJobToExecuteIndex == -1) || 
				(CompareTime(sgNextJobToExecute,sgOpenJobs[ilCount].sgNextExecution) == TIME1_LATER_THAN_TIME2))
			{
				/* first valid job  -> initialize global time to exec with this job's time to exec 
					OR
				   this job's time to exec is prior to the last job's time to exec
				*/
				sgNextJobToExecute = sgOpenJobs[ilCount].sgNextExecution;
				igNextJobToExecuteIndex = ilCount;
				igHasOpenJobs = TRUE;
			}
		}
	}
	return igHasOpenJobs;
}

/******************************************************************************************/
/* CompareTime: compares the given time structs and checks which one is prior.		  */
/* Return: 	                                                                  	  */
/*	TIME1_LATER_THAN_TIME2		-1	-> first time is prior to the second time */
/*	TIME1_EQUALS_TIME2		0	-> the time struct are equal              */
/*	TIME2_LATER_THAN_TIME1		1	-> the second time is prior to the first  */
/******************************************************************************************/
static int CompareTime(struct tm rpTime1, struct tm rpTime2)
{
	char clTime1[15], clTime2[15];
	static int ilCount = 0;

	/* convert time values to strings, so we can compare strings rather than calculating numbers */
	/* output format is '<4-digit-year><3-digit-day-of-year><two-digit-hour><two-digit-minute><two-digit-second>' */
	sprintf(clTime1,"%0.4d%0.3d%0.2d%0.2d%0.2d",rpTime1.tm_year,rpTime1.tm_yday,rpTime1.tm_hour,rpTime1.tm_min,rpTime1.tm_sec);
	sprintf(clTime2,"%0.4d%0.3d%0.2d%0.2d%0.2d",rpTime2.tm_year,rpTime2.tm_yday,rpTime2.tm_hour,rpTime2.tm_min,rpTime2.tm_sec);

	/* compare time */
	return strcmp(clTime1,clTime2);
}

/**********************************************************************************/
/* ExecuteOpenJob: executes the open job with index <ipIndex> and then        	  */
/*	removes it from the array or, if it is a monthly executed job, calculates */
/*	the next execution time.                                                  */
/* Return:	0	-> everything ok                                          */
/*		<0	-> error                                                  */
/**********************************************************************************/
static int ExecuteOpenJob(int ipIndex)
{
	char clScriptName[MAX_SCRIPTNAME]; /* buffer for next script to execute from script list of action struct */
	char *pclFields = "FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC";
	int ilXBSCmdIndex = -1;

	/* make sure the job with the given index is valid */
	if ((*sgOpenJobs[ipIndex].cXBSCmd == '\0') ||
		(sgOpenJobs[ipIndex].pcAdditionalInfo == NULL) ||
		(sgOpenJobs[ipIndex].pcEmplUrnoList == NULL)) 
	{
		/* no -> return */
		dbg(TRACE,"ExecuteOpenJob: job no. %d is not valid!!!",ipIndex);
		return -1;
	}

	/* make sure the XBSCommand is valid */
	if ((ilXBSCmdIndex = CheckCommandAndGetXBSCommandIndex(sgOpenJobs[ipIndex].cXBSCmd)) == -1)
	{
		/* no -> return */
		dbg(TRACE,"ExecuteOpenJob: XBSCommand '%s' is unknown!!!",sgOpenJobs[ipIndex].cXBSCmd);
		return -2;
	}

	/* set global data, so basic scripts can access it via GetLast...() */
	strcpy(pcgCmd,"XBS");
	strcpy(pcgTwStart,sgOpenJobs[ipIndex].cXBSCmd);
	pcgFields = pclFields;
	pcgData = sgOpenJobs[ipIndex].pcAdditionalInfo;
	pcgOldData = pcgZeroString;
	pcgSelKey = sgOpenJobs[ipIndex].pcEmplUrnoList;

	dbg(DEBUG,"ExecuteOpenJob: SELECT   <%s>",pcgSelKey);
	dbg(DEBUG,"ExecuteOpenJob: FIELDS   <%s>",pcgFields);
	dbg(DEBUG,"ExecuteOpenJob: DATA     <%s>",pcgData);
	dbg(DEBUG,"ExecuteOpenJob: OLD DATA <%s>",pcgOldData);
	dbg(DEBUG,"ExecuteOpenJob: TWSTART  <%s>",pcgTwStart);

	/* execute next script in script list of action section */
	while (GetNextXBSScript(ilXBSCmdIndex,clScriptName))
	{
		dbg(TRACE,"ExecuteOpenJob: executing script '%s'",clScriptName);
		ExecuteScript(clScriptName,NULL);
	}
	/* reset next-script-to-execute-pointer */
	sgXBSCommands[ilXBSCmdIndex].pNextScript = sgXBSCommands[ilXBSCmdIndex].cScriptList;
	/* check job mode */
	if (sgOpenJobs[ipIndex].iExecutionMode == XBS_EXECUTION_MONTHLY)
	{
		/* monthly executed job -> set next exec time */
		SetOpenJobNextExecutionTime(ipIndex);
	}
	else
	{
		/* one time jobe -> remove the job */
		FreeOpenJob(ipIndex);
	}
	/* save the current job list to disk */
	WriteOpenJobFile();
	/* set the next open job to execute */
	SetNextOpenJob();
	/* signal everything is fine */
	return 0;
}

/******************************************************************************/
/* CheckTimer: checks, if the execution time for open jobs has come or passed */
/*  since the last check.                                                     */
/* Return: true	 -> execute open jobs now                                     */
/*         false -> no open jobs to be executed now                           */
/******************************************************************************/
static int CheckTimer(void)
{
	time_t rlTimetNow;
	struct tm rlTmNow;
	char clTimeNow[15], clTimeLastCheck[15], clOpenJob[15];
	int ilCmp1, ilCmp2;
	int ilReturn = 0;
	static int ilCount = 0;

	/* first check, if there are open jobs anyway */
	if (!igHasOpenJobs) 
	{
		dbg(DEBUG,"CheckTimer: no open jobs to execute!");
		return ilReturn;
	}

	/* wait a second */
	sleep(1);

	/* get current time */
	time(&rlTimetNow);
	rlTmNow = *localtime(&rlTimetNow);

	/* convert time values to strings, so we can compare strings rather than calculating numbers */
	/* output format is '<4-digit-year><3-digit-day-of-year><two-digit-hour><two-digit-minute><two-digit-second>' */
	sprintf(clTimeNow,"%0.4d%0.3d%0.2d%0.2d%0.2d",rlTmNow.tm_year,rlTmNow.tm_yday,rlTmNow.tm_hour,rlTmNow.tm_min,rlTmNow.tm_sec);
	sprintf(clTimeLastCheck,"%0.4d%0.3d%0.2d%0.2d%0.2d",sgOpenJobLastCheck.tm_year,sgOpenJobLastCheck.tm_yday,sgOpenJobLastCheck.tm_hour,sgOpenJobLastCheck.tm_min,sgOpenJobLastCheck.tm_sec);
	sprintf(clOpenJob,"%0.4d%0.3d%0.2d%0.2d%0.2d",sgNextJobToExecute.tm_year,sgNextJobToExecute.tm_yday,sgNextJobToExecute.tm_hour,sgNextJobToExecute.tm_min,sgNextJobToExecute.tm_sec);

	/* save time of last check (now) */
	sgOpenJobLastCheck = *localtime(&rlTimetNow);

	/* check, if execution time is between time of last check and current time */
	ilCmp1 = strcmp(clOpenJob,clTimeLastCheck);
	ilCmp2 = strcmp(clOpenJob,clTimeNow);
	if ((ilCmp1 >=0) && (ilCmp2 <=0))
	{
		dbg(DEBUG,"CheckTimer: time has come...");
		ilReturn = 1;
		ilCount = 0;
		/* There might be another job which has exactly the same execution time. 
		   So we must set the last check time to the execution time of this job 
		   because otherwise a job with the same exec time would would be out of scope 
		   at the next check and not be executed. We kind of freeze the check time until
		   all jobs are done. So we force the function to check the current time at 
		   least one time again. */
		sgOpenJobLastCheck = sgNextJobToExecute;
	}
	else if (ilCount == 60)
	{
		ilCount = 0;
	}
	ilCount += 1;

	/* return result of check */
	return ilReturn;
}

/******************************************************************************/
/* RemoveSingleOpenJob: remove an open job from the queue.                    */
/* Return:  0  -> everything is OK                                            */
/*          <0 -> error                                                       */
/******************************************************************************/
static int RemoveSingleOpenJob(int ipIndex, char *pcgErrorBuf)
{
	/* check if index is valid */
	if ((ipIndex < 0) || (ipIndex >= MAX_OPEN_JOBS) || 
		(*sgOpenJobs[ipIndex].cXBSCmd == '\0') ||
		(sgOpenJobs[ipIndex].pcAdditionalInfo == NULL) ||
		(sgOpenJobs[ipIndex].pcEmplUrnoList == NULL)) 
	{
		/* signal error to user */
		sprintf(pcgRcvData,"error removing open job: <%d> is no valid index or no open job at this position!!!",ipIndex);
		dbg(TRACE, "RemoveSingleOpenJob(): error: <%d> is no valid index or no open job at this position!!!",ipIndex); 
		return -1;
	}

	/* free memory */
	FreeOpenJob(ipIndex);
	/* save to disk */
	WriteOpenJobFile();
	/* set the next open job to execute */
	SetNextOpenJob();

	/* everything is ok */
	strcpy(pcgRcvData,"ok");
	dbg(TRACE, "RemoveSingleOpenJob(): open job no.<%d> removed",ipIndex); 
	return 0;
}

/******************************************************************************/
/* GetSingleJobInfoString: returns a single job's info string (FROM,TO,MODE,  */
/*	etc.). If no index is specified, the first valid job will be returned.*/ 
/*	Additionally the next valid job's index (if there is one) will be     */
/*	delivered, so the client can request all job's info strings in a loop.*/
/*  The client can see if an error occurred or if there simply is no open job */
/*  at all: If there is no open job, <pcgCurrentJobIndex> is set to "-0001",  */
/*  If an error occurred, <pcgCurrentJobIndex> is set to "-0002".             */
/* Return:	0	->	everything is ok                              */
/*		<0	->	error                                         */
/******************************************************************************/
static int GetSingleJobInfoString(int ipIndex, char *pcgInfoString, 
								  char *pcgNextJobIndex, char* pcgCurrentJobIndex)
{
	int ilIndex; 
	int ilNextJob = -1;

	/* are we supposed to find the first valid index by ourselves? */
	if (ipIndex == -1)
	{
		/* look for the first job */
		for (ilIndex = 0; (ilIndex < MAX_OPEN_JOBS) && (ipIndex == -1); ilIndex += 1)
		{
			if ((*sgOpenJobs[ilIndex].cXBSCmd != '\0') &&
				(sgOpenJobs[ilIndex].pcAdditionalInfo != NULL) &&
				(sgOpenJobs[ilIndex].pcEmplUrnoList != NULL)) 
			{
				/* valid job found */
				ipIndex = ilIndex;
			}
		}
		/* still no job found? */
		if (ipIndex == -1)
		{
			/* no open jobs */
			sprintf(pcgInfoString,"error: <%d> is no valid index or no open job at this position!!!",ipIndex);
			dbg(TRACE, "GetSingleJobInfoString(): error: <%d> is no valid index or no open job at this position!!!",ipIndex); 
			/* copy '-1' to buffer for next job index to signal no further job to client */
			strcpy(pcgNextJobIndex,"-1");
			/* set current job index to -1, so client can verify there's simply no job (no error occurred) */
			strcpy(pcgCurrentJobIndex,"-0001");
			return -2;
		}
	}

	/* now check index again */
	if ((ipIndex < 0) || (ipIndex >= MAX_OPEN_JOBS) || 
		(*sgOpenJobs[ipIndex].cXBSCmd == '\0') ||
		(sgOpenJobs[ipIndex].pcAdditionalInfo == NULL) ||
		(sgOpenJobs[ipIndex].pcEmplUrnoList == NULL)) 
	{
		/* no valid index specified -> signal error to user */
		sprintf(pcgInfoString,"error: <%d> is no valid index or no open job at this position!!!",ipIndex);
		dbg(TRACE, "GetSingleJobInfoString(): error: <%d> is no valid index or no open job at this position!!!",ipIndex); 
		/* copy '-1' to buffer for next job index to signal no further job to client */
		strcpy(pcgNextJobIndex,"-1");
		/* set current job index to -2, so client can verify that an error occurred */
		strcpy(pcgCurrentJobIndex,"-0002");
		return -1;
	}

	/* copy info string to buffer */
	strcpy(pcgInfoString,sgOpenJobs[ipIndex].pcAdditionalInfo);

	/* look for the next job */
	for (ilIndex = ipIndex + 1; (ilIndex < MAX_OPEN_JOBS) && (ilNextJob == -1); ilIndex += 1)
	{
		if ((*sgOpenJobs[ilIndex].cXBSCmd != '\0') &&
			(sgOpenJobs[ilIndex].pcAdditionalInfo != NULL) &&
			(sgOpenJobs[ilIndex].pcEmplUrnoList != NULL)) 
		{
			/* valid job found */
			ilNextJob = ilIndex;
		}
	}
	/* copy index of next job to buffer */
	sprintf(pcgNextJobIndex,"%.4d",ilNextJob);
	/* copy index of current job to buffer */
	sprintf(pcgCurrentJobIndex,"%.4d",ipIndex);
	return 0;
}

/********************************************************************************/
/* SetReceiverBuffer: set the <pcgRcvBuf> param which is sended back to the   	*/
/*  calling application, that sended the 'XSCR' command. This provides a      	*/
/*	mechanism for basic scripts to pass back a string containing the result	*/
/*  of what the script computed. The client application must check if		*/
/*  <pcgRcvBuf> starts with 'Script Basic Error:'. In this case an error	*/
/*  occurred and the content of <pcgRcvBuf> is not the result of script		*/
/*	computation.                                                            */
/* Return: 0 if successful, else -1                                           	*/
/********************************************************************************/
static int SetReceiverBuffer(char* pcpBuffer)
{
	/* copy the buffer */
	if (strlen(pcpBuffer) < MAX_RECEIVER_DATA)
	{
		strcpy(pcgRcvData,pcpBuffer);
		return 0;
	}
	return -1;
}

/******************************************************************************/
/* APO 05.06.2001: - function returns parameter instead of define             */
/* GetMaxCachedFieldSize: returns the maximum size of a cached field. Used    */
/*  in ceda.c to allocate memory.                                             */
/* Return:  MAX_CACHED_FIELD_SIZE                                             */
/******************************************************************************/
static int GetMaxCachedFieldSize(void)
{
	return igMaxCachedDbFieldSize;
}


/******************************************************************************/
/* APO 05.06.2001: - function created                                         */
/* InitTableCacheStructs: initialize structs for caching tables in strings.   */
/* Return:  none                                                              */
/******************************************************************************/
static void InitTableCacheStructs(void)
{
	int ilCount = 0;

	/* Initialize max. cached field size. This value must be updated everytime */
	/* a field value is cached which is bigger than this value (see GetCachedDbValue()) */
	igMaxCachedDbFieldSize = (int)MAX_CACHED_FIELD_SIZE;

	/* initialize all structs */
	for (;ilCount < MAX_CACHED_TABLES; ilCount++)
	{
		sgCachedTables[ilCount].cTableName[0] = '\0';
		sgCachedTables[ilCount].cActionCmd[0] = '\0';
		sgCachedTables[ilCount].cKeyFieldName[0] = '\0';
		sgCachedTables[ilCount].cValueFieldName[0] = '\0';
		sgCachedTables[ilCount].pCachedTableString = NULL;
		sgCachedTables[ilCount].lNoOfRecords = 0;
		sgCachedTables[ilCount].iInUse = 0;
	}
}

/******************************************************************************/
/* APO 05.06.2001: - function created                                         */
/* ResetTableCacheStructs: re-initialize structs for caching tables in        */
/*  strings. Delete sections in action and free any allocated memory.         */
/* Return:  none                                                              */
/******************************************************************************/
static void ResetTableCacheStructs(void)
{
	int ilCount = 0;

	/* Initialize max. cached field size. This value must be updated everytime */
	/* a field value is cached which is bigger than this value (see GetCachedDbValue()) */
	igMaxCachedDbFieldSize = (int)MAX_CACHED_FIELD_SIZE;

	/* re-initialize all structs */
	for (;ilCount < MAX_CACHED_TABLES; ilCount++)
	{
		ResetTableCacheStruct(ilCount);
	}
}

/******************************************************************************/
/* APO 05.06.2001: - function created                                         */
/* ResetTableCacheStruct: re-initialize the struct for caching tables at      */
/* index <ipIndex>. Delete sections in action and free any allocated memory.  */
/* Return:  none                                                              */
/******************************************************************************/
static void ResetTableCacheStruct(int ipIndex)
{
	if (sgCachedTables[ipIndex].iInUse)
	{
		/* release memory */
		if (sgCachedTables[ipIndex].pCachedTableString != NULL)
		{
			free(sgCachedTables[ipIndex].pCachedTableString);
		}
	}
	/* reset data */
	sgCachedTables[ipIndex].cTableName[0] = '\0';
	sgCachedTables[ipIndex].cActionCmd[0] = '\0';
	sgCachedTables[ipIndex].cKeyFieldName[0] = '\0';
	sgCachedTables[ipIndex].cValueFieldName[0] = '\0';
	sgCachedTables[ipIndex].pCachedTableString = NULL;
	sgCachedTables[ipIndex].lNoOfRecords = 0;
	sgCachedTables[ipIndex].iInUse = 0;
}

/******************************************************************************/
/* APO 05.06.2001: - cache table first, if not cached already                 */
/* GetCachedDbValue:                                                          */
/*  locate the cached table <pcpTableName> and extract the field value of the */
/*  record with key field's value = <pcpKeyFieldValue>. If table is not       */
/*  cached already cache it first and then try to find the record.            */
/* Return: > 0 -> record found, length of field value                         */
/*         ==0 -> record not found                                            */
/*         < 0 -> error code                                                  */
/******************************************************************************/
static int GetCachedDbValue(char *pcpTableName, char *pcpKeyFieldName,
							char *pcpKeyFieldValue, char *pcpValueFieldName, 
							char *pcpValueFieldValue)
{
	/* index of free cache table struct */
	int ilIndex = -1;
	/* loop counter */
	int ilLoop = 0;
	/* size of extracted field value */
	int ilFieldSize = 0;
	/* error code of function call */
	int ilError = 0;

	/* check parameters */
	if ((pcpTableName == NULL) || (*pcpTableName == '\0') ||
		(strlen(pcpTableName) > MAX_TABNAME) ||
		(pcpKeyFieldName == NULL) || (*pcpKeyFieldName == '\0') || 
		(strlen(pcpKeyFieldName) > MAX_FIELD_NAME) || 
		(pcpValueFieldName == NULL) || (*pcpValueFieldName == '\0') || 
		(strlen(pcpValueFieldName) > MAX_FIELD_NAME) ||
		(pcpKeyFieldValue == NULL) || (*pcpKeyFieldValue == '\0'))
	{
		/* error -> terminate */
		dbg(TRACE,"GetCachedDbValue: invalid parameter!!!");
		/* return error code (-1 - -3 reserved for ExtractCachedDbValue()) */
		return -4;
	}

	/* initialize buffer */
	*pcpValueFieldValue = '\0';

	/* search table in struct */
	for (ilLoop = 0; ilLoop < MAX_CACHED_TABLES; ilLoop++)
	{
		if (!strcmp(sgCachedTables[ilLoop].cTableName,pcpTableName))
		{
			/* cached table found -> check field names */
			if ((strcmp(sgCachedTables[ilLoop].cKeyFieldName,pcpKeyFieldName) != 0) || 
				(strcmp(sgCachedTables[ilLoop].cValueFieldName,pcpValueFieldName) != 0))
			{
				/* table found, but field names don't match */
				dbg(TRACE,"GetCachedDbValue() line %d: cached table '%s' found but field names don't match",__LINE__,pcpTableName);
				dbg(TRACE,"GetCachedDbValue() line %d: cached table: key field='%s', value field='%s'",__LINE__,sgCachedTables[ilLoop].cKeyFieldName,sgCachedTables[ilLoop].cValueFieldName);
				dbg(TRACE,"GetCachedDbValue() line %d: parameter   : key field='%s', value field='%s'",__LINE__,pcpKeyFieldName,pcpValueFieldName);
				/* return error code (-1 - -3 reserved for ExtractCachedDbValue()) */
				return -5;
			}
			/* extract field value */
			return ExtractCachedDbValue(sgCachedTables[ilLoop].pCachedTableString,pcpKeyFieldValue,pcpValueFieldValue);
		}
		else if (sgCachedTables[ilLoop].iInUse == 0)
		{
			/* Free struct found -> return index */
			/* We can break the loop here, because after the first free index there will never be a used */
			/* struct. So this table is not cached and we have to cache it now. */
			dbg(DEBUG,"GetCachedDbValue() line %d: found free cache table struct for table '%s' at index <%d>",__LINE__,pcpTableName,ilLoop);
			if ((ilError = CacheTable(ilLoop,pcpTableName,pcpKeyFieldName,pcpValueFieldName)) < 0)
			{
				/* an error occurred -> terminate (dbg()-Info is done by CacheTable()) */
				return ilError;
			}
			/* now extract field value */
			return ExtractCachedDbValue(sgCachedTables[ilLoop].pCachedTableString,pcpKeyFieldValue,pcpValueFieldValue);
		}
	}
	/* if we get here, table was not found and no free struct is accessable */
	/* table not cached, but no free struct found */
	dbg(TRACE,"GetCachedDbValue() line %d: could not locate free cache table struct for table '%s'!!!",__LINE__,pcpTableName);
	/* no free struct found */
	return -6;
}

/******************************************************************************/
/* APO 05.06.2001: - function created                                         */
/* ExtractCachedDbValue:                                                      */
/*  get the field value from the cached table string with key value =         */
/*  <pcpKeyValue>.                                                            */
/* Return: > 0 -> record found, length of field value                         */
/*         ==0 -> record not found                                            */
/*         < 0 -> error                                                       */
/******************************************************************************/
static int ExtractCachedDbValue(char *pcpSource, char *pcpKeyFieldValue, 
								char *pcpValueFieldValue)
{
	/* pointer to cached table string */
	char *pclStartPos = NULL, *pclStopPos = NULL;
	int ilCount;

	/* initialize buffer */
	*pcpValueFieldValue = '\0';

	/* check parameter */
	if ((pcpSource == NULL) || (*pcpSource == '\0'))
	{
		dbg(TRACE,"ExtractCachedDbValue() line <%d>: bad parameter <pcpSource>!!!",__LINE__);
		return -1;
	}

	/* search key value in string */
	if ((pclStartPos = strstr(pcpSource,pcpKeyFieldValue)) != NULL)
	{
		/* record found -> extract value of second associated field */
		/* get real start position */
		if ((pclStartPos = strstr(pclStartPos,",")) == NULL)
		{
			/* something's wrong, the delimiter was not found -> terminate with error */
			dbg(TRACE,"ExtractCachedDbValue() line <%d>: delimiter ',' not found!!!",__LINE__);
			return -2;
		}
		/* get stop position */
		if ((pclStopPos = strstr(pclStartPos,"#")) == NULL)
		{
			/* something's wrong, the stop marker was not found -> terminate with error */
			dbg(TRACE,"ExtractCachedDbValue() line <%d>: stop marker '#' not found!!!",__LINE__);
			return -3;
		}
		/* extract field value */
		pclStartPos = pclStartPos + 1; /* start position (+1 = comma) */
		ilCount = pclStopPos - pclStartPos;
		strncpy(pcpValueFieldValue,pclStartPos,ilCount);
		pcpValueFieldValue[ilCount] = '\0';
		dbg(DEBUG,"ExtractCachedDbValue() line <%d>: field value is <%s>",__LINE__,pcpValueFieldValue);
		/* return length of field */
		return ilCount;
	}
	else
	{
		dbg(DEBUG,"ExtractCachedDbValue() line <%d>: key field value <%s> NOT found!!!",__LINE__,pcpKeyFieldValue);
		return 0;
	}
}

/******************************************************************************/
/* APO 05.06.2001: - function created from former CacheTables()               */
/* CacheTable: reads all records (fields <pcpKeyFieldName> and                */
/*  <pcpValueFieldName>) from table <pcpTableName> into the cache table       */
/*  struct at index <ipIndex>. the memory needed to stiore the records is     */
/*  dynamically allocated.                                                    */
/* Return: 0 -> success, else error code (>-6)                                */
/******************************************************************************/
static int CacheTable(int ipIndex, char *pcpTableName, 
					  char *pcpKeyFieldName, char *pcpValueFieldName)
{
	HANDLE slArrayHandle = 0;
	long llRow = -1, llSizeOfCacheString = 0;
	int ilRC;
	int ilCount = 0;
	int ilRetry;
	char clData[1024];
	char clTempData[1024+2]; /* +2 is for '#...#' */
	char clFieldList[MAX_FIELDLIST];
	char clArrayName[MAX_ARRAY_NAME];

	/* check parameters */
	if ((ipIndex < 0) || (ipIndex >= MAX_CACHED_TABLES) || 
		(sgCachedTables[ipIndex].iInUse != 0) ||
		(pcpTableName == NULL) || (*pcpTableName == '\0') || (strlen(pcpTableName) > (MAX_ARRAY_NAME-2)) ||
		(pcpKeyFieldName == NULL) || (*pcpKeyFieldName == '\0') || 
		(pcpValueFieldName == NULL) || (*pcpValueFieldName == '\0') ||
		((strlen(pcpKeyFieldName) + strlen(pcpValueFieldName)) > 254))
	{
		/* bad parameter(s) -> terminate (error code <-6, -1 - -6 are reserved for */
		/* ExtractCachedDbValue() and GetCachedDbValue()) */
		dbg(TRACE,"CacheTable() line <%d>: bad parameters, can't cache table!!!",__LINE__);
		return -7;
	}

	/* create array name (table name + '_C') */
	sprintf(clArrayName,"%s_C",pcpTableName);
	/* create field list */
	sprintf(clFieldList,"%s,%s",pcpKeyFieldName,pcpValueFieldName);

	/* open CEDAArray */
	if ((ilRC = CEDAArrayCreate(&slArrayHandle,clArrayName,pcpTableName,"",NULL,NULL,clFieldList,NULL,0)) != RC_SUCCESS)
	{
		dbg(DEBUG,"CacheTable() line <%d>: CEDAArrayCreate for <%s> failed ('%s')!",__LINE__,clArrayName,CedaArrayErrorCodeToString(ilRC));
		return -8;
	}

	/* fill array */
	if ((ilRC = CEDAArrayFill(&slArrayHandle,clArrayName,NULL)) != RC_SUCCESS)
	{
		dbg(DEBUG,"CacheTable() line <%d>: CEDAArrayFill for <%s> failed ('%s')!",__LINE__,clArrayName,CedaArrayErrorCodeToString(ilRC));
		CEDAArrayDestroy(&slArrayHandle,clArrayName);
		return -9;
	}

	/* get number of rows */
	CEDAArrayGetRowCount(&slArrayHandle,clArrayName,&llRow);
	sgCachedTables[ipIndex].lNoOfRecords = llRow;

	/* loop over array for determine how much memory we have to allocate for the string */
	llRow = ARR_FIRST;
	for (ilCount = 0; ilCount < sgCachedTables[ipIndex].lNoOfRecords; ilCount++)
	{
		/* get field values count sizes */
		if ((ilRC = CEDAArrayGetFields(&slArrayHandle,clArrayName,NULL/* FieldNumbers */,clFieldList,','/*delimiter*/,
									   1024/* max. bytes */,llRow /* cursor of this array */,clData)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"CacheTable() line <%d>: CEDAArrayGetFields for <%s> failed ('%s')!",__LINE__,clArrayName,CedaArrayErrorCodeToString(ilRC));
			CEDAArrayDestroy(&slArrayHandle,clArrayName);
			return -10;
		}
		/* get strlen and add to counter */
		llSizeOfCacheString += strlen(clData) + 2; /* +2 is for '#<record>#' */
		/* set cursor to next row */
		llRow = ARR_NEXT;
	}
	/* plus one for terminating zero */
	llSizeOfCacheString += 1;

	/* allocate memory for cache string */
	if ((sgCachedTables[ipIndex].pCachedTableString = (char*)malloc(sizeof(char) * llSizeOfCacheString)) == NULL)
	{
		/* error allocating memory */
		dbg(DEBUG,"CacheTable() line <%d>: error allocating <%d> bytes of memory!!!",__LINE__,llSizeOfCacheString);
		CEDAArrayDestroy(&slArrayHandle,clArrayName);
		return -11;
	}
	/* initialize buffer */
	memset(sgCachedTables[ipIndex].pCachedTableString,0x00,llSizeOfCacheString);

	/* now loop over array a second time to extract the field values */
	llRow = ARR_FIRST;
	for (ilCount = 0; ilCount < sgCachedTables[ipIndex].lNoOfRecords; ilCount++)
	{
		/* get field values count sizes */
		if ((ilRC = CEDAArrayGetFields(&slArrayHandle,clArrayName,NULL/* FieldNumbers */,clFieldList,','/*delimiter*/,
									   1024/* max. bytes */,llRow /* cursor of this array */,clData)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"CacheTable() line <%d>: CEDAArrayGetFields for <%s> failed  with <%d>!",__LINE__,clArrayName,ilRC);
			CEDAArrayDestroy(&slArrayHandle,clArrayName);
			return -12;
		}
		/* add string */
		sprintf(clTempData,"#%s#",clData);
		strcat(sgCachedTables[ipIndex].pCachedTableString,clTempData);
		/* set cursor to next row */
		llRow = ARR_NEXT;
	}

	/* now copy data to struct */
	strcpy(sgCachedTables[ipIndex].cTableName,pcpTableName);
	strcpy(sgCachedTables[ipIndex].cActionCmd,clArrayName);
	strcpy(sgCachedTables[ipIndex].cKeyFieldName,pcpKeyFieldName);
	strcpy(sgCachedTables[ipIndex].cValueFieldName,pcpValueFieldName);
	sgCachedTables[ipIndex].iInUse = 1;

	/* cleanup */
	CEDAArrayDestroy(&slArrayHandle,clArrayName);

	/* create section in action */
	ilRC = RC_FAILURE;
	for (ilRetry = 1; ilRetry <= igRetryCreateAction && (ilRC != RC_SUCCESS); ilRetry += 1)
	{
		dbg(DEBUG,"CacheTable() line <%d>: calling TriggerAction() the %d. time...",__LINE__,ilRetry);
		if ((ilRC = TriggerAction(sgCachedTables[ipIndex].cTableName,clFieldList,NULL)) == RC_SUCCESS)
		{
			/* success -> terminate */
			return 0;
		}
	}

	/* if we get here, we could not set up a dynamic section in action -> cleanup and leave with error */
	ResetTableCacheStruct(ipIndex);
	return -13;
}

/******************************************************************************/
/* APO 05.06.2001: - function created                                         */
/* RefreshCachedTable: re-reads all records from table                        */
/*  <sgCachedTables[ipIndex].cTableName>. Memory is reallocated.              */
/* Return: 0 -> success, else error code                                      */
/******************************************************************************/
static int RefreshCachedTable(int ipIndex)
{
	HANDLE slArrayHandle = 0;
	long llRow = -1, llSizeOfCacheString = 0;
	int ilRC;
	int ilCount = 0;
	int ilRetry;
	char clData[1024];
	char clTempData[1024+2]; /* +2 is for '#...#' */
	char clFieldList[MAX_FIELDLIST];

	/* check parameters */
	if ((ipIndex < 0) || (ipIndex >= MAX_CACHED_TABLES) || (sgCachedTables[ipIndex].iInUse != 1))
	{
		/* bad parameter(s) -> terminate (error code <-6, -1 - -6 are reserved for */
		/* ExtractCachedDbValue() and GetCachedDbValue()) */
		dbg(TRACE,"RefreshCachedTable() line <%d>: bad parameters, can't re-read table!!!",__LINE__);
		return -1;
	}

	/* create field list */
	sprintf(clFieldList,"%s,%s",sgCachedTables[ipIndex].cKeyFieldName,sgCachedTables[ipIndex].cValueFieldName);

	/* open CEDAArray */
	if ((ilRC = CEDAArrayCreate(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd,sgCachedTables[ipIndex].cTableName,"",NULL,NULL,clFieldList,NULL,0)) != RC_SUCCESS)
	{
		dbg(DEBUG,"RefreshCachedTable() line <%d>: CEDAArrayCreate for <%s> failed ('%s')!",__LINE__,sgCachedTables[ipIndex].cActionCmd,CedaArrayErrorCodeToString(ilRC));
		return -2;
	}

	/* fill array */
	if ((ilRC = CEDAArrayFill(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd,NULL)) != RC_SUCCESS)
	{
		dbg(DEBUG,"RefreshCachedTable() line <%d>: CEDAArrayFill for <%s> failed ('%s')!",__LINE__,sgCachedTables[ipIndex].cActionCmd,CedaArrayErrorCodeToString(ilRC));
		CEDAArrayDestroy(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd);
		return -3;
	}

	/* get number of rows */
	CEDAArrayGetRowCount(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd,&llRow);
	sgCachedTables[ipIndex].lNoOfRecords = llRow;

	/* loop over array for determine how much memory we have to allocate for the string */
	llRow = ARR_FIRST;
	for (ilCount = 0; ilCount < sgCachedTables[ipIndex].lNoOfRecords; ilCount++)
	{
		/* get field values count sizes */
		if ((ilRC = CEDAArrayGetFields(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd,NULL/* FieldNumbers */,clFieldList,','/*delimiter*/,
			1024/* max. bytes */,llRow /* cursor of this array */,clData)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"RefreshCachedTable() line <%d>: CEDAArrayGetFields for <%s> failed ('%s')!",__LINE__,sgCachedTables[ipIndex].cActionCmd,CedaArrayErrorCodeToString(ilRC));
			CEDAArrayDestroy(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd);
			return -4;
		}
		/* get strlen and add to counter */
		llSizeOfCacheString += strlen(clData) + 2; /* +2 is for '#<record>#' */
		/* set cursor to next row */
		llRow = ARR_NEXT;
	}
	/* plus one for terminating zero */
	llSizeOfCacheString += 1;

	/* re-allocate memory for cache string */
	if ((sgCachedTables[ipIndex].pCachedTableString = (char*)realloc(sgCachedTables[ipIndex].pCachedTableString,sizeof(char) * llSizeOfCacheString)) == NULL)
	{
		/* error reallocating memory */
		dbg(DEBUG,"RefreshCachedTable() line <%d>: error reallocating <%d> bytes of memory!!!",__LINE__,llSizeOfCacheString);
		CEDAArrayDestroy(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd);
		return -5;
	}
	/* initialize buffer */
	memset(sgCachedTables[ipIndex].pCachedTableString,0x00,llSizeOfCacheString);

	/* now loop over array a second time to extract the field values */
	llRow = ARR_FIRST;
	for (ilCount = 0; ilCount < sgCachedTables[ipIndex].lNoOfRecords; ilCount++)
	{
		/* get field values count sizes */
		if ((ilRC = CEDAArrayGetFields(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd,NULL/* FieldNumbers */,clFieldList,','/*delimiter*/,
									   1024/* max. bytes */,llRow /* cursor of this array */,clData)) != RC_SUCCESS)
		{
			/* an error occurred */
			dbg(DEBUG,"RefreshCachedTable() line <%d>: CEDAArrayGetFields for <%s> failed  with <%d>!",__LINE__,sgCachedTables[ipIndex].cActionCmd,ilRC);
			CEDAArrayDestroy(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd);
			return -6;
		}
		/* add string */
		sprintf(clTempData,"#%s#",clData);
		strcat(sgCachedTables[ipIndex].pCachedTableString,clTempData);
		/* set cursor to next row */
		llRow = ARR_NEXT;
	}

	/* cleanup */
	CEDAArrayDestroy(&slArrayHandle,sgCachedTables[ipIndex].cActionCmd);

	/* if we get here everything is ok */
	return 0;
}

/******************************************************************************/
/* APO 25.07.2001: - function created                                         */
/* BreakScriptListExecution:                                                  */ 
/*  Set a trigger for the functions GetNextActionScript() and                 */
/*  GetNextXBSScript() which leads to the return value 0 (no more script in   */
/*  list). This mechanism is used to generally stop event processing, e.g.    */
/*  when an event is generated by RELDPL: every script of an affected         */
/*  CEDAActionArray's scriptlist will check this and stop executing, so this  */
/*  check is done x times. To prevent this, the first script which detects    */
/*  that the event is not relevant for the following scripts as well can break*/
/*  the processing loop.                                                      */
/* Return: RC_SUCCES -> everything is ok                                      */
/*         < 0       -> error                                                 */
/******************************************************************************/
static void BreakScriptListExecution(void)
{
	igBreakScriptListExecution = 1;
}

/******************************************************************************/
/* APO 2001/07/31: function taken from v.4.4.1.1.1 of RELDPL                  */
/* ExtractItemFromDataString:                                                 */
/* copy item no. <ipItemNo> from <pcpSource> to <pcpTarget>. The Item is      */
/* supposed to be delimited by <cpDelim> and must not be longer than          */
/* <ilMaxLen - 1>. <ilMaxLen> includes the terminating zero. Item counting    */
/* starts at 1.                                                               */
/* Return:	<0	-> error (function will print error info to log file) */
/*			>0	-> size of item                               */
/*          ==0 -> no source data                                             */
/******************************************************************************/
static int ExtractItemFromDataString(int ipItemNo, char* pcpSource, 
									 char* pcpTarget, char cpDelim,
									 int ipMaxLen)
{
	/* delimiter (= item) counter */
	int ilCountDelim = 1;
	/* length of item */
	int ilLength = 0;
	/* char counter */
	int ilPos;

	if ((pcpSource == NULL) || (*pcpSource == '\0'))
	{
		/* no source string -> terminate */
		dbg(DEBUG,"ExtractItemFromDataString: source string is empty!");
		return 0;
	}

	for (ilPos = 0; ilPos < strlen(pcpSource); ilPos += 1)
	{
		/* this char = delimiter ? */
		if (*(pcpSource+ilPos) == cpDelim)
		{
			/* start of next item in string */
			ilCountDelim += 1;
		}
		/* in scope of item? */
		else if (ilCountDelim == ipItemNo)
		{
			/* max. length reached? */
			if (ilLength >= ipMaxLen)
			{
				/* yes -> can't copy byte, print error and terminate */
				dbg(DEBUG,"ExtractItemFromDataString line <%d>: item no.    = <%d>",__LINE__,ipItemNo);
				dbg(DEBUG,"ExtractItemFromDataString line <%d>: pcpSource   = '%s'",__LINE__,pcpSource);
				dbg(DEBUG,"ExtractItemFromDataString line <%d>: delimiter   = '%c'",__LINE__,cpDelim);
				dbg(DEBUG,"ExtractItemFromDataString line <%d>: max. length = <%d>",__LINE__,ipMaxLen);
				dbg(DEBUG,"ExtractItemFromDataString line <%d>: item exceeds max. length of <%d>!!!",__LINE__,ipMaxLen);
				return -1;
			}
			/* copy next byte from source */
			*(pcpTarget+ilLength) = *(pcpSource+ilPos);
			ilLength += 1;
			/* zero-terminate target buffer */
			*(pcpTarget+ilLength) = '\0';
		}
		else if (ilCountDelim > ipItemNo)
		{
			/* next item reached -> break loop */
			break;
		}
	}

	/* if we get here and the item number is greater than the counter, 
	   there are fewer items in the string than expected */
	if (ilCountDelim < ipItemNo)
	{
		/* dump error info and terminate with error */
		dbg(DEBUG,"ExtractItemFromDataString line <%d>: item no.    = <%d>",__LINE__,ipItemNo);
		dbg(DEBUG,"ExtractItemFromDataString line <%d>: pcpSource   = '%s'",__LINE__,pcpSource);
		dbg(DEBUG,"ExtractItemFromDataString line <%d>: delimiter   = '%c'",__LINE__,cpDelim);
		dbg(DEBUG,"ExtractItemFromDataString line <%d>: max. length = <%d>",__LINE__,ipMaxLen);
		dbg(DEBUG,"ExtractItemFromDataString line <%d>: fewer items in string than expected (<%d> items found in '%s')",__LINE__,ilCountDelim,pcpSource);
		return -2;
	}

	/* everything is ok -> return size of item */
	return ilLength;
}

/******************************************************************************/
/* APO 11.09.2001: - function created                                         */
/* AddEventToFile:                                                            */
/* add the current event to the file used for buffering. Only the data which  */
/* is accessible by basic scripts is being saved to disk, i.e. all data       */
/* accessible by GetLast...() functions. So the input char* will be copied to */
/* the internal buffer <cgEventBuffer> that holds all data originally stored  */
/* in the global event struct <prgItem>. The pointers the GetLast...()        */
/* functions use (like <pcgSelKey>) will point to the appropriate data portion*/
/* in <cgEventBuffer> when the event is restored and processed later.         */
/* Additionally to the GetLast...() data <pcpTwStart> is needed to restore the*/
/* TWSTART info, which is needed to determine which action has to be          */
/* performed.                                                                 */
/* Return: RC_SUCCES  -> everything is ok                                     */
/*         RC_FAILURE -> error, function will print error info to log file    */
/******************************************************************************/
static int AddEventToFile(char *pcpCommand, char* pcpTwStart, char *pcpSelKey,
						  char *pcpFieldList, char *pcpData, char *pcpOldData)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	/* size of data blocks */
	unsigned long llBlockSize = 0;
	/* result of fwrite() */
	int ilNumWritten, ilTotalBytesWritten = 0;
	/* size of data */
	int ilDataSize;

	/* Check if this functionalitiy is allowed at the moment. If a buffered event causes the script execution that */
	/* currently calls this function the mechanism of creating pseudo events is prohibited, else we might end up in an */
	/* infinite loop of script execution. */
	if (igProcessingBufferedEvents == 1)
	{
		dbg(TRACE,"AddEventToFile line <%d>: cannot add event to file while events from same file are processed!!!",__LINE__);
		return RC_FAILURE; /* error */
	}

	/* first make sure that the overall data size is not greater than the */
	/* buffer we use to restore the events in ReadEventFromFile() */
	/* additionally no parameter must be greater than 9999 because that's our max. blocksize */
	ilDataSize = (strlen(pcpCommand) + strlen(pcpTwStart) + strlen(pcpSelKey) + strlen(pcpFieldList) + strlen(pcpData) + strlen(pcpOldData) + 5 /* delimiters */);
	if ((ilDataSize > MAX_DATA_SIZE) || (strlen(pcpTwStart) > MAX_TW_SIZE) || (strlen(pcpCommand) > MAX_CMD_SIZE) || 
		(strlen(pcpSelKey) > MAX_BLOCKSIZE) || (strlen(pcpFieldList) > MAX_BLOCKSIZE) || (strlen(pcpOldData) > MAX_BLOCKSIZE))
	{
		dbg(TRACE,"AddEventToFile line <%d>: invalid parameter(s)!!!",__LINE__);
		dbg(TRACE,"AddEventToFile line <%d>: overall parameter length must be < <%d> bytes",__LINE__,MAX_DATA_SIZE);
		return RC_FAILURE; /* error: data size */
	}

	/* open file */
	if (!igHasBufferedEvent)
	{
		/* create new file */
		if((phgEventBufferFile = fopen(cgEventBufferFileName,"w+")) == NULL)
		{
			dbg(TRACE,"AddEventToFile line <%d>: error opening file '%s' in mode 'w+' ",__LINE__,cgEventBufferFileName);
			return RC_FAILURE; /* error opening file */
		}
	}
	else
	{
		/* append to existing file */
		if((phgEventBufferFile = fopen(cgEventBufferFileName,"a+")) == NULL)
		{
			dbg(TRACE,"AddEventToFile line <%d>: error opening file '%s' in mode 'a+' ",__LINE__,cgEventBufferFileName);
			return RC_FAILURE; /* error opening file */
		}
	}

	/* check if max. length of file is reached */
	if ((igMaxEventBuffer > 0) && ((ftell(phgEventBufferFile) + ilDataSize) > igMaxEventBuffer))
	{
		/* maximum length reached -> close file and terminate */
		dbg(TRACE,"AddEventToFile line <%d>: maximum size of file '%s' reached (config parameter MAX_EVENT_BUFFER = %dMB)",__LINE__,cgEventBufferFileName);
		fclose(phgEventBufferFile);
		return RC_FAILURE;
	}

	/* For each data block write size of data block and then data block itself to file. */
	/* Only the data that can be delivered to scripts via GetLast...() will be saved. */
	/* All other event info like originator etc. will be lost, but could not be accessed by scripts anyway. */
	fprintf(phgEventBufferFile,"%.4u%s%.4u%s%.4u%s%.4u%s%.4u%s%.4u%s\n",strlen(pcpCommand),pcpCommand,strlen(pcpTwStart),pcpTwStart,strlen(pcpSelKey),pcpSelKey,strlen(pcpFieldList),pcpFieldList,strlen(pcpData),pcpData,strlen(pcpOldData),pcpOldData);

	/* now we have events in the file */
	igHasBufferedEvent = 1;

	/* flush and close file */
	fflush(phgEventBufferFile);
	fclose(phgEventBufferFile);

	return RC_SUCCESS;
}

/******************************************************************************/
/* APO 10.09.2001: - function created                                         */
/* ProcessAllBufferedEvents:                                                  */
/* Open event buffer file and read all events and process them. Buffered      */
/* events are only those handled by HandleData(), i.e. no shutdown, terminate */
/* or other system commands. Buffering events to file and processing them     */
/* later is meant to prevent the handler from doing work on the database.     */ 
/* This can be vital e.g. when recalculating accounts (ACCTAB) through XBSHDL,*/
/* then SCBHDL must be prvented from processing IRTs on DRRTAB and other which*/
/* would interfere a running recalculation.                                   */
/* Return: RC_SUCCES  -> everything is ok                                     */
/*         RC_FAILURE -> error, function will print error info to log file    */
/******************************************************************************/
static int ProcessAllBufferedEvents()
{
	/* index of internal data structure */
	int ilIndex = -1;
	/* buffer for extracting parameters from data string */
	char pclStartInfo[80];
	/* buffer for next script to execute from script list of action struct */
	char clScriptName[MAX_SCRIPTNAME];

	/* due to problems when processing buffered events on nizza	*/
	long llFilePos = 0;
	int	 ilFileStat;
	int	 ilRc;
	int	 ilAccHandle = -1;
	BOOL blSendBc = FALSE;
	BOOL blSendBc1= FALSE;

	ilAccHandle = GetArrayIndex("ACCHDL");

	/* make sure AddEventToFile() is not called while we are processing events from file. */
	igProcessingBufferedEvents = 1;

	bgRecalcOfflineAccounts = TRUE;
	WriteTemporaryField("IsRecalc","1");

	SetAccAction(ilAccHandle,FALSE);
	
	/* read all events */
	do
	{
		/* open file */
		if((phgEventBufferFile = fopen(cgEventBufferFileName,"r")) == NULL)
		{
			dbg(TRACE,"ProcessAllBufferedEvents line <%d>: error opening file '%s' for reading",__LINE__,cgEventBufferFileName);
			return RC_FAILURE; /* error opening file */
		}

		ilFileStat = fseek(phgEventBufferFile,llFilePos,SEEK_SET);
		ilRc = ReadEventFromFile();
		if (ilRc != RC_SUCCESS)
			break;
		llFilePos = ftell(phgEventBufferFile);

		/* close file */
		fclose(phgEventBufferFile);

		/*****************************************************************************************************************/
		/*****************************************************************************************************************/
		/*** Following is copied and pasted from HandleData(). Synchronize changes if you do any modifications. **********/
		/*****************************************************************************************************************/
		/*****************************************************************************************************************/
		/* diagnostic output */
		dbg(TRACE,"================= %s PSEUDO EVENT BEGIN ================",mod_name);
		dbg(DEBUG,"HANDLE EVENT FROM FILE");
		dbg(DEBUG,"TWSTART  <%s>",pcgTwStart);
		dbg(DEBUG,"CMD      <%s>",pcgCmd);
		dbg(DEBUG,"SELECT   <%s>",pcgSelKey);
		dbg(DEBUG,"FIELDS   <%s>",pcgFields);
		dbg(DEBUG,"DATA     <%s>",pcgData);
		dbg(DEBUG,"OLD DATA <%s>",pcgOldData);
		/*---------------------------------------------------------------------------------------------------------------*/
		/*- dynamic CEDAArray Command??? --------------------------------------------------------------------------------*/
		/*- Check all dynamic CEDAArrays if the array name matches this command. If so we received this command from ----*/
		/*- the ACTION handler and have to update the CEDAArray because there had been a database action on the ---------*/
		/*-	table the CEDAArray refers to. ------------------------------------------------------------------------------*/
		/*---------------------------------------------------------------------------------------------------------------*/
		if ((*pcgTwStart != '\0') && ((ilIndex = CheckCommandAndGetArrayIndex(pcgTwStart)) != -1))
		{
			dbg(TRACE,"ProcessAllBufferedEvents line <%d>: restoring and processing ACTION command for CEDAArray <%s>",__LINE__,sgRecordsets[ilIndex].cArrayName);
			/* do action */
			if ((sgRecordsets[ilIndex].iActionType & ACTION_PRE_SCRIPT == ACTION_PRE_SCRIPT) &&
				strcmp(sgRecordsets[ilIndex].cPreUpdateScript,""))
			{
				/* execute script before updating CEDAArray */
				dbg(DEBUG,"ProcessAllBufferedEvents line <%d>: executing pre-update script '%s'",__LINE__,sgRecordsets[ilIndex].cPreUpdateScript);
				ExecuteScript(sgRecordsets[ilIndex].cPreUpdateScript,NULL);
			}
			if (sgRecordsets[ilIndex].iActionType & ACTION_REFILL == ACTION_REFILL)
			{
				/* update CEDAArray */
				dbg(DEBUG,"ProcessAllBufferedEvents line <%d>: calling CEDAArrayRefill for '%s'",__LINE__,sgRecordsets[ilIndex].cArrayName);
				/* write possible changes */
				commit_work();
				/* delete all rows and refill CEDAArray */
				CEDAArrayRefill(&sgRecordsets[ilIndex].hHandle,sgRecordsets[ilIndex].cArrayName,sgRecordsets[ilIndex].cSelect,"",ARR_FIRST);
			}
			if ((sgRecordsets[ilIndex].iActionType & ACTION_POST_SCRIPT == ACTION_POST_SCRIPT) &&
				strcmp(sgRecordsets[ilIndex].cPostUpdateScript,""))
			{
				/* execute script after updating CEDAArray */
				dbg(DEBUG,"ProcessAllBufferedEvents line <%d>: executing post-update script '%s'",__LINE__,sgRecordsets[ilIndex].cPostUpdateScript);
				ExecuteScript(sgRecordsets[ilIndex].cPostUpdateScript,NULL);
			}
		}
		/*---------------------------------------------------------------------------------------------------------------*/
		/*- Action Section Command??? -----------------------------------------------------------------------------------*/
		/*- Check all internal ActionSection structs if the section name matches the command. If so, we received this ---*/
		/*- command from the ACTION handler and have to execute the script(s) which are specified in the ActionSection. -*/
		/*---------------------------------------------------------------------------------------------------------------*/
		else if ((*pcgTwStart != '\0') && ((ilIndex = CheckCommandAndGetActionStructIndex(pcgTwStart)) != -1))
		{
			dbg(DEBUG, "ACTION sent command for section <%s_%s>",mod_name,sgActionSections[ilIndex].cCmd); 
			/* execute next script in script list of action section */
			while (GetNextActionScript(ilIndex,clScriptName))
			{
				dbg(TRACE,"executing script '%s'",clScriptName);
				ExecuteScript(clScriptName,NULL);
			}
			/* reset next-script-to-execute-pointer */
			sgActionSections[ilIndex].pNextScript = sgActionSections[ilIndex].cScriptName;
		}
		/*---------------------------------------------------------------------------------------------------------------*/
		/*- XBSCommand??? -----------------------------------------------------------------------------------------------*/
		/*- Check all internal XBSCommand structs if <cCmd> matches the XBSCommand which is given by <pcgTwStart>. ------*/
		/*- If so, we received this command from a client application like the InitAccount-Tool. Like the ActionSection -*/
		/*- functionality we have to execute one or more scripts. -------------------------------------------------------*/
		/*---------------------------------------------------------------------------------------------------------------*/
		else if ((!strcmp(pcgCmd,"XBS")) || (!strcmp(pcgCmd,"XBS2")))
		{
			/* search the XBSCommand structs which one has to be used */
			if ((ilIndex = CheckCommandAndGetXBSCommandIndex(pcgTwStart)) != -1)
			{
				/* execute now */ 
				dbg(DEBUG, "XBSCommand '%s' received",pcgTwStart);
				dbg(DEBUG, "script list is: <%s>",sgXBSCommands[ilIndex].cScriptList);
				dbg(DEBUG, "path is:		<%s>",sgXBSCommands[ilIndex].cPath);

				/* write possible changes before deleting employees */
				if (ilAccHandle >= 0)
				{
					CloseRecordset(ilAccHandle,1);
				}

				/* execute next script in script list of action section */
				while (GetNextXBSScript(ilIndex,clScriptName))
				{
					dbg(TRACE,"executing script '%s'",clScriptName);
					ExecuteScript(clScriptName,NULL);
				}

				/* reset next-script-to-execute-pointer */
				sgXBSCommands[ilIndex].pNextScript = sgXBSCommands[ilIndex].cScriptList;
			}
			else
			{
				dbg(TRACE, "ProcessAllBufferedEvents(): unknown XBSCommand found ('%s')",pcgTwStart); 
			}
		}
		else if (!strcmp(pcgCmd,"RECALC_BC"))
		{
			blSendBc = TRUE;
		}
		else if (!strcmp(pcgCmd,"INIT_BC"))
		{
			blSendBc1 = TRUE;
		}
		else
		{
			int ilDummy = 6;
		}

		dbg(TRACE,"================= %s PSEUDO EVENT END ================",mod_name);
	}
	while (ilRc == RC_SUCCESS);

	bgRecalcOfflineAccounts = FALSE;
	WriteTemporaryField("IsRecalc","0");

	/* AddEventToFile() can write to file again */
	igProcessingBufferedEvents = 0;
	/* no more events in file */
	igHasBufferedEvent	= 0;

	/* write account changes to DB	*/
	if (ilAccHandle >= 0)
	{
		CloseRecordset(ilAccHandle,1);
		SetAccAction(ilAccHandle,TRUE);
		SendRelaccBc(pcgSelKey,pcgFields,pcgData);
	}

	if (blSendBc)
	{
		SendBc("XBS2","SBCTAB",pcgSelKey,pcgFields,pcgData,pcgTwStart);
	}
	else if (blSendBc1)
	{
		SendBc("XBS","SBCTAB",pcgSelKey,pcgFields,pcgData,pcgTwStart);
	}

	return RC_SUCCESS;
}

/******************************************************************************/
/* APO 12.09.2001: - function created                                         */
/* ReadEventFromFile:                                                         */
/* read the next saved event from file. The file must be opened. Expected data*/
/* format is:                                                                 */
/* '<10-digit size of command><command><10-digit size of twstart><twstart>    */
/* <10-digit size of selkey><selkey><10-digit size of field list><field list> */
/* <10-digit size of data><data><10-digit size of old data><old data>'.       */
/* The data will be stored in the global buffer <cgEventBuffer>.              */
/* Return: RC_SUCCES  -> everything is ok                                     */
/*         RC_FAILURE -> error, function will print error info to log file    */
/******************************************************************************/
static int ReadEventFromFile()
{
	/* return of fread() */
	size_t	ilNumRead;

	/* buffer to restore event length */
	char	clBlockSize[12];
	long	llBlockSize = 0;
	int		ilC;
	char*	pclNextBlock = cgEventBuffer; 

	/* check if file handle is ok */
	if (phgEventBufferFile == NULL)
	{
		/* no -> terminate */
		dbg(TRACE,"ReadEventFromFile line <%d>: error: <phgEventBufferFile == NULL>",__LINE__);
		return RC_FAILURE;
	}

	/* initialize buffer */
	memset(cgEventBuffer,MAX_DATA_SIZE,0x00);
	dbg(DEBUG,"ReadEventFromFile line <%d>: reading file '%s'",__LINE__,cgEventBufferFileName);

	for (ilC = 0; ilC < 6; ilC++)
	{
		/* read length of block */
		if ((ilNumRead = fread(clBlockSize,sizeof(char),4,phgEventBufferFile)) != 4)
		{
			/* EOF or error */
			if (ferror(phgEventBufferFile))
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading file '%s': '%s'",__LINE__,cgEventBufferFileName,strerror(errno));
			}
			else if (feof(phgEventBufferFile))
			{
				dbg(DEBUG,"ReadEventFromFile line <%d>: EOF file '%s' reached",__LINE__,cgEventBufferFileName);
			}
			else
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: 0 bytes read from file '%s'!!!",__LINE__,cgEventBufferFileName);
			}
			return RC_FAILURE;
		}

		/* convert to long */
		llBlockSize = atol(clBlockSize);

		if (llBlockSize > 0)
		{
			/* check buffer size	*/
			int ilMaxSize = pclNextBlock - cgEventBuffer + llBlockSize+1;
			if (ilMaxSize > MAX_DATA_SIZE)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading parameter <%d> from file '%s': '%s'",__LINE__,ilC,cgEventBufferFileName,"huge buffer size exceeded");
				return RC_FAILURE;
			}

			/* now read block */
			if ((ilNumRead = fread(pclNextBlock,sizeof(char),llBlockSize,phgEventBufferFile)) != llBlockSize)
			{
				/* EOF or error */
				if (ferror(phgEventBufferFile))
				{
					dbg(TRACE,"ReadEventFromFile line <%d>: error reading parameter <%d> from file '%s': '%s'",__LINE__,ilC,cgEventBufferFileName,strerror(errno));
				}
				else if (feof(phgEventBufferFile))
				{
					dbg(DEBUG,"ReadEventFromFile line <%d>: EOF file '%s' reached",__LINE__,cgEventBufferFileName);
				}
				return RC_FAILURE;
			}
		}


		switch(ilC)
		{
		case 0 :	/*	pcgCmd	*/
			if (llBlockSize <= 0 || llBlockSize > MAX_CMD_SIZE)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgCmd> from file '%s' (length <%d> > MAX_CMD_SIZE)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			strcpy(pcgCmd,pclNextBlock);
		break;
		case 1 :	/* pcgTwStart	*/
			if (llBlockSize <= 0 || llBlockSize > MAX_TW_SIZE)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgTwStart> from file '%s' (length <%d> > MAX_TW_SIZE)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			strcpy(pcgTwStart,pclNextBlock);
		break;
		case 2 :	/* pcgSelKey	*/
			if (llBlockSize < 0)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgSelKey> from file '%s' (length <%d> < 0)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			pcgSelKey = pclNextBlock;
		break;
		case 3 :	/* pcgFields	*/
			if (llBlockSize < 0)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgFields> from file '%s' (length <%d> < 0)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			pcgFields = pclNextBlock;
		break;
		case 4 :	/* pcgData	*/
			if (llBlockSize < 0)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgData> from file '%s' (length <%d> < 0)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			pcgData = pclNextBlock;
		break;
		case 5 :	/* pcgOldData	*/
			if (llBlockSize < 0)
			{
				dbg(TRACE,"ReadEventFromFile line <%d>: error reading <pcgOldData> from file '%s' (length <%d> < 0)",__LINE__,cgEventBufferFileName,llBlockSize);
				return RC_FAILURE;
			}

			pclNextBlock[llBlockSize] = '\0';
			/* set pointer */
			pcgOldData = pclNextBlock;
		break;
		}

		/* set next position */
		pclNextBlock += llBlockSize + 1;
	}


	/* now read the '\n' so we gaot the corrct position next time */
	if ((ilNumRead = fread(pclNextBlock,sizeof(char),strlen("\n"),phgEventBufferFile)) != strlen("\n"))
	{
		/* EOF or error */
		if (ferror(phgEventBufferFile))
		{
			dbg(TRACE,"ReadEventFromFile line <%d>: error reading parameter <%d> from file '%s': '%s'",ilC,__LINE__,cgEventBufferFileName,strerror(errno));
		}
		else if (feof(phgEventBufferFile))
		{
			dbg(DEBUG,"ReadEventFromFile line <%d>: EOF file '%s' reached",__LINE__,cgEventBufferFileName);
		}
		else
		{
			dbg(TRACE,"ReadEventFromFile line <%d>: 0 bytes read from file '%s'!!!",__LINE__,cgEventBufferFileName);
		}
	}

	/* set all other global event data to zero or blank */
	pcgTwEnd[0] = '\0';
	igQueOut = 0;
	pcgRecvName[0] = '\0';
	pcgDestName[0] = '\0';

	return RC_SUCCESS;
}

/******************************************************************************/
/* APO 24.09.2001: - function created                                         */
/* SetQueueState:                                                             */
/* set all queues to hold (<ipNewState> == QUE_HOLD) or start (QUE_GO). All   */
/* queues listed in <pcgQueuesToHold> will be affected.                       */
/* Return: RC_SUCCES  -> everything is ok                                     */
/*         RC_FAILURE -> error, function will print error info to log file    */
/******************************************************************************/
static int SetQueueState(int ipNewState)
{
	/* buffer for single item (queue id) from list */
	char clQueueID[MAX_QUEUES_TO_HOLD];
	/* index number of next item */
	int ilCount = 1;
	/* queue id as integer */
	int ilQueueID;
	/* error code of function calls */
	int ilRC;

	/* determine command */
	if ((ipNewState != QUE_HOLD) && (ipNewState != QUE_GO))
	{
		/* invalid parameter */
		dbg(TRACE,"SetQueueState line <%d>: unknown command <%d>!!!",__LINE__,ipNewState);
		return RC_FAILURE;
	}

	/* do it for each queue id in list */
	while(ExtractItemFromDataString(ilCount,pcgQueuesToHold,clQueueID,',',MAX_QUEUES_TO_HOLD) > 0)
	{
		/* convert queue id to int */
		ilQueueID = atoi(clQueueID);
		dbg(TRACE,"SetQueueState line <%d>: setting queue id <%d> to state '%s'...",__LINE__,ilQueueID,((ipNewState == QUE_HOLD) ? "QUE_HOLD" : "QUE_GO"));
		/* set queue to hold */
		if ((ilRC = que(ipNewState,ilQueueID,mod_id,0,0,NULL)) != RC_SUCCESS)
		{
			HandleQueErr(ilRC);
			dbg(TRACE,"SetQueueState line <%d>: QUE_HOLD for queue id <%d> failed!!!",__LINE__,ilQueueID);
			return RC_FAILURE;
		}
		/* extract next item */
		ilCount += 1;
	}

	return RC_SUCCESS;
}

/*	Find native account script function	*/

static NATIVESCRIPTFUNC FindNativeAccountScript(const char *pcpScriptName,BOOL *pbpOffline,char *pcpCheckAccount)
{
	int		ilRc;
	long	llRowNum = ARR_FIRST;
	char*	pclMatchingRow = NULL; 

	if (pcpScriptName == NULL || pcpScriptName[0] == '\0')
		return NULL;

	if (pbpOffline == NULL || pcpCheckAccount == NULL)
		return NULL;

	*pbpOffline = FALSE;
	pcpCheckAccount[0] = '\0';

	dbg(DEBUG,"FindNativeAccountScript: calling AATArrayFindRowPointer for <%s>...",pcpScriptName);
	ilRc = AATArrayFindRowPointer(&hAccScripts,"ACCSCR",&hAccScriptsInd,"NAME",(char *)pcpScriptName,&llRowNum,(void *)&pclMatchingRow);
	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"FindNativeAccountScript: AATArrayFindRowPointer failed for %s with (error: %s)",pcpScriptName,CedaArrayErrorCodeToString(ilRc));
		*pbpOffline = FALSE;
		return NULL;
	}
	else
	{
		long llFieldNum = 1;
		char clFunc[14];
		char clOffline[4];
		char clNative[4];

		dbg(DEBUG,"FindNativeAccountScript: calling AATArrayGetField for <%s>...",pcpScriptName);
		ilRc = AATArrayGetField(&hAccScripts,"ACCSCR",&llFieldNum,"FUNC",11,llRowNum,(void *)clFunc);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField failed for %s with (error: %s)",pcpScriptName,CedaArrayErrorCodeToString(ilRc));
			*pbpOffline = FALSE;
			return NULL;
		}
		else
		{
			NATIVESCRIPTFUNC plFunc = (NATIVESCRIPTFUNC)atol(clFunc);
			if (plFunc != NULL)
			{
				dbg(DEBUG,"FindNativeAccountScript: calling AATArrayGetField for <%s>...",pcpScriptName);
				llFieldNum = 3;
				ilRc = AATArrayGetField(&hAccScripts,"ACCSCR",&llFieldNum,"ACTIVATED",2,llRowNum,(void *)clNative);
				if (ilRc != RC_SUCCESS)
				{
					dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField failed for %s with (error: %s)",pcpScriptName,CedaArrayErrorCodeToString(ilRc));
					plFunc = NULL;
				}
				else
				{
					if (clNative[0] == '0')
						plFunc = NULL;
					dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField returns native = <%s>",clNative);
				}

			}

			dbg(DEBUG,"FindNativeAccountScript: calling AATArrayGetField for <%s>...",pcpScriptName);
			llFieldNum = 2;
			ilRc = AATArrayGetField(&hAccScripts,"ACCSCR",&llFieldNum,"OFFL",2,llRowNum,(void *)clOffline);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField failed for %s with (error: %s)",pcpScriptName,CedaArrayErrorCodeToString(ilRc));
				*pbpOffline = FALSE;
			}
			else
			{
				if (clOffline[0] == '1')
					*pbpOffline = TRUE;
				else
					*pbpOffline = FALSE;
				dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField returns offline = <%s>",clOffline);
			}

			dbg(DEBUG,"FindNativeAccountScript: calling AATArrayGetField for <%s>...",pcpScriptName);
			llFieldNum = 4;

			ilRc = AATArrayGetField(&hAccScripts,"ACCSCR",&llFieldNum,"CALC",11,llRowNum,(void *)pcpCheckAccount);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField failed for %s with (error: %s)",pcpScriptName,CedaArrayErrorCodeToString(ilRc));
				pcpCheckAccount[0] = '\0';
			}
			else
			{
				rtrim(pcpCheckAccount);
				dbg(DEBUG,"FindNativeAccountScript: AATArrayGetField returns from Check Account = <%s>",pcpCheckAccount);
			}

			return plFunc;
		}
	}
}

/* Initialize account scripts	*/
static BOOL InitAccountScripts()
{
	long llFieldLength[6];
	char pclData[MAX_DATA_SIZE];
	int ilRc;
	int ilC;
	long llRowNum = ARR_FIRST;
	long llFieldNum;
	char *pclToken = NULL;
	char *pclMatchingRow = NULL;

	bgRecalcOfflineAccounts = FALSE;
	WriteTemporaryField("IsRecalc","0");

	llFieldLength[0] = MAX_FILENAME;
	llFieldLength[1] = 10;
	llFieldLength[2] = 1;
	llFieldLength[3] = 1;
	llFieldLength[4] = 1;
	llFieldLength[5] = 10;

	dbg(DEBUG,"InitAccountScripts: calling AATArrayCreate...");
	if ((ilRc = AATArrayCreate(&hAccScripts,"ACCSCR",TRUE,100,5,llFieldLength,"NAME,FUNC,OFFL,ACTIVATED,CALC",NULL)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitAccountScripts: AATArrayCreate failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	dbg(DEBUG,"InitAccountScripts: calling AATArrayCreateSimpleIndex...");
	if ((ilRc = AATArrayCreateSimpleIndex(&hAccScripts,"ACCSCR",&hAccScriptsInd,"NAME","NAME",ARR_DESC,FALSE)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitAccountScripts: AATArrayCreateSimpleIndex failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	/* initialize script array	*/
	for (ilC = 0;sgNativeScripts[ilC].pcmScriptName != NULL;ilC++)
	{
		sprintf(pclData,"/ceda/bas/%s.bbf,%ld,%s,%s,%s",sgNativeScripts[ilC].pcmScriptName,sgNativeScripts[ilC].pcmScriptFunc,"0","0",sgNativeScripts[ilC].pcmCheckAccount);
		dbg(DEBUG,"InitAccountScripts: calling AATArrayAddRow for <%s>...",pclData);
		llRowNum = ARR_FIRST;
		delton(pclData);	/* delimiter to nul byte */
		ilRc = AATArrayAddRow(&hAccScripts,"ACCSCR",&llRowNum,pclData);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"InitAccountScripts: AATArrayAddRow failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
			return FALSE;
		}
	}

	pclToken = strtok(pcgNativeAccounts,",;");
	while (pclToken)
	{
		dbg(DEBUG,"InitAccountScripts: calling AATArrayFindRowPointer for <%s>...",pclToken);
		llRowNum = ARR_FIRST;
		sprintf(pclData,"/ceda/bas/%s.bbf",pclToken);
		ilRc = AATArrayFindRowPointer(&hAccScripts,"ACCSCR",&hAccScriptsInd,"NAME",pclData,&llRowNum,(void *)&pclMatchingRow);
		if (ilRc != RC_SUCCESS)
		{
			/* debug info : native script required but not available	*/
			dbg(TRACE,"InitAccountScripts: native script required for %s but not implemented yet",pclToken);
		}
		else
		{
			dbg(DEBUG,"InitAccountScripts: calling AATArrayPutField for <%s>...",pclToken);
			llFieldNum = 3;
			ilRc = AATArrayPutField(&hAccScripts,"ACCSCR",NULL,"ACTIVATED",llRowNum,FALSE,"1");
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"InitAccountScripts: AATArrayPutField failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
			else
			{
				dbg(TRACE,"InitAccountScripts: native script required for %s",pclToken);
			}
		}

		pclToken = strtok(NULL,",;");
	}

	pclToken = strtok(pcgOfflineAccounts,",;");
	while (pclToken)
	{
		dbg(DEBUG,"InitAccountScripts: calling AATArrayFindRowPointer for <%s>...",pclToken);
		llRowNum = ARR_FIRST;
		sprintf(pclData,"/ceda/bas/%s.bbf",pclToken);
		ilRc = AATArrayFindRowPointer(&hAccScripts,"ACCSCR",&hAccScriptsInd,"NAME",pclData,&llRowNum,(void *)&pclMatchingRow);
		if (ilRc == RC_SUCCESS)
		{
			dbg(DEBUG,"InitAccountScripts: calling AATArrayPutField for <%s>...",pclToken);
			llFieldNum = 2;
			ilRc = AATArrayPutField(&hAccScripts,"ACCSCR",NULL,"OFFL",llRowNum,FALSE,"1");
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"InitAccountScripts: AATArrayPutField failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
			else
			{
				dbg(TRACE,"InitAccountScripts: offline script required for %s",pclToken);
			}
		}
		else
		{
			sprintf(pclData,"/ceda/bas/%s.bbf,%ld,%s,%s,%s,%s",pclToken,NULL,"1","0","1","");
			dbg(DEBUG,"InitAccountScripts: calling AATArrayAddRow for <%s>...",pclToken);
			llRowNum = ARR_FIRST;
			delton(pclData);	/* delimiter to nul byte */
			ilRc = AATArrayAddRow(&hAccScripts,"ACCSCR",&llRowNum,pclData);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"InitAccountScripts: AATArrayAddRow failed for %s with (error: %s)","ACCSCR",CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
			else
			{
				dbg(TRACE,"InitAccountScripts: offline script required for %s",pclToken);
			}
		}

		pclToken = strtok(NULL,",;");
	}

	return TRUE;
} /* end of InitAccountScripts*/

/*	initialize daylight settings	*/
static int InitDaylightSettings()
{
 	short	slCursor;
	short	slSqlFunc;
	char	pclApttab[10];
	char	pclSqlBuf[2560];
	char	pclDataArea[2560];
	char	pclErr[2560];
	int		ilRc;

	strcpy(pclApttab,"APT");
	strcat(pclApttab,"TAB");

	slSqlFunc = START;
	slCursor = 0;
	sprintf(pclSqlBuf,"SELECT TICH,TDI1,TDI2 FROM %s WHERE APC3='%s'",pclApttab,pcgHopo);
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != DB_SUCCESS)
	{
		get_ora_err(ilRc,pclErr);
		dbg(TRACE,"InitDaylightSettings() Error reading UTC-Local difference in APTTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,pclSqlBuf);
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy(cgTdi1,"0");
		strcpy(cgTdi2,"0");
		get_fld(pclDataArea,0,STR,14,cgTich);
		get_fld(pclDataArea,1,STR,14,cgTdi1);
		get_fld(pclDataArea,2,STR,14,cgTdi2);
		rtrim(cgTich);
		if (*cgTich != '\0')
		{
			rtrim(cgTdi1);
			rtrim(cgTdi2);
			sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
			sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
		}
		dbg(TRACE,"InitDaylightSettings() Read %s -> TICH: <%s> TDI1: <%s> TDI2 <%s>",pclApttab,cgTich,cgTdi1,cgTdi2);
	}
	close_my_cursor(&slCursor);
	commit_work();
	slCursor = 0;

	return ilRc;
}

static void LocalToUtc(char *pcpTime)
{
	char *pclTdi = strcmp(pcpTime,cgTich) < 0 ? cgTdi1 : cgTdi2;
	AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
}

static int GetRealTimeDiff (const char *pcpLocal1,const char *pcpLocal2, time_t *plpDiff)
{
	int ilRC = RC_SUCCESS;
	char clTime1[16], clTime2[16];
	struct tm	rlTimeStruct1, rlTimeStruct2;
	time_t llT1, llT2;

	*plpDiff = (time_t)0;
	if ( !pcpLocal1 || ( strlen ( pcpLocal1 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( !pcpLocal2 || ( strlen ( pcpLocal2 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "GetRealTimeDiff: At least one parameter invalid" );
	else
	{
		strcpy ( clTime1, pcpLocal1 );
		strcpy ( clTime2, pcpLocal2 );
		LocalToUtc(clTime1);
		LocalToUtc(clTime2);
		ilRC = CedaDateToTimeStruct(&rlTimeStruct1, clTime1 );
		ilRC |= CedaDateToTimeStruct(&rlTimeStruct2, clTime2 );
	}
	if ( ilRC != RC_SUCCESS )
	{
		dbg ( TRACE, "GetRealTimeDiff: At least one call to CedaDateToTimeStruct failed" );
	}
	else
	{
		llT1 = mktime ( &rlTimeStruct1 );
		llT2 = mktime ( &rlTimeStruct2 );
		*plpDiff = llT2 - llT1;
		dbg ( TRACE, "GetRealTimeDiff: <%s> - <%s> = %ld seconds", pcpLocal2, pcpLocal1, *plpDiff );
	}
	return ilRC;
}

/* handle staff to account prefetch	*/
static BOOL StfToAccPrefetch(const char *pcpStfuList,const char *pcpYear,BOOL bpAddStf)
{
	char *pclStfuList = (char *)pcpStfuList;
	const char* pcpCurrYear = pcpYear;

	char pclData[256];
	char clAccKeyValue[30];
	int ilRc;
	long llRowNum = ARR_FIRST;
	long llFieldNum = -1;
	char *pclMatchingRow = NULL;

	int ilLen;
	char clStfUrno[16];
	char clCount[12];
	int ilCount;
	int i;
	int ilStfuCount;
	char* pclAccWhere = NULL;
	int ilAccHandle;

	/* allocate buffer for Accounts to load */
	if (bpAddStf)
	{
		ilStfuCount = get_no_of_items(pcpStfuList);
		if (ilStfuCount > silMaxUrnoSize)
		{
			return FALSE;
		}

		pclAccWhere = (char *)malloc(ARR_MAXSELECTLEN);
		strcpy(pclAccWhere,"where STFU in (");
	}

	ilStfuCount  = 0;

	/* accounts bereits vorhanden ?	*/
	ilAccHandle = GetArrayIndex("ACCHDL");

	strcpy(clAccKeyValue,pcpCurrYear);
	strcat(clAccKeyValue,",");

	do
	{
		ilLen = GetNextDataItem(clStfUrno,&pclStfuList,",","\0"," ");
		if (ilLen <= 0)
			continue;

		sprintf(&clAccKeyValue[5],"%-10s",clStfUrno);

		dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayFindRowPointer for <%s>...",clAccKeyValue);
		llRowNum = ARR_FIRST;
		ilRc = AATArrayFindRowPointer(&hAccPreFetch,"ACCPRE",&hAccPreFetchInd,"YEAR,STFU",clAccKeyValue,&llRowNum,(void *)&pclMatchingRow);
		if (ilRc != RC_SUCCESS)
		{
			if (bpAddStf)
			{
				sprintf(pclData,"%s,%s",clAccKeyValue,"1");
				dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayAddRow for <%s>.with <%s>..",clStfUrno,pclData);
				llRowNum = ARR_FIRST;
				delton(pclData);	/* delimiter to nul byte */
				ilRc = AATArrayAddRow(&hAccPreFetch,"ACCPRE",&llRowNum,pclData);
				if (ilRc != RC_SUCCESS)
				{
					dbg(DEBUG,"AddStfToAccountPrefetch(): AATArrayAddRow failed for %s with (error: %s)","ACCPRE",CedaArrayErrorCodeToString(ilRc));
					free (pclAccWhere);
					return FALSE;
				}

				dbg(DEBUG,"AddStfToAccountPrefetch(): <%s> added to prefetch array",clAccKeyValue);
				if (ilStfuCount == 0)
				{
					sprintf(pclData,"\'%s\'",clStfUrno);
				}
				else
				{
					sprintf(pclData,",\'%s\'",clStfUrno);
				}

				strcat(pclAccWhere,pclData);
				++ilStfuCount;
			}
			else
			{
				dbg(DEBUG,"AddStfToAccountPrefetch: calling AATArrayFindRowPointer for <%s>... failed with error = %s",clAccKeyValue,CedaArrayErrorCodeToString(ilRc));
			}
		}
		else	/* record found */
		{
			dbg(DEBUG,"AddStfToAccountPrefetch: calling AATArrayGetField for <%s>...",clStfUrno);
			llFieldNum = 2;
			ilRc = AATArrayGetField(&hAccPreFetch,"ACCPRE",&llFieldNum,"COUNT",12,llRowNum,(void *)clCount);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"AddStfToAccountPrefetch: AATArrayGetField failed for %s with (error: %s)",clStfUrno,CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
			else
			{
				if (bpAddStf)
				{
					ilCount = atol(clCount) + 1;
				}
				else
				{
					ilCount = atol(clCount) - 1;
				}

				if (bpAddStf && ilCount == 1)
				{
					if (ilStfuCount == 0)
					{
						sprintf(pclData,"\'%s\'",clStfUrno);
					}
					else
					{
						sprintf(pclData,",\'%s\'",clStfUrno);
					}

					strcat(pclAccWhere,pclData);
					++ilStfuCount;
				}

				if (ilCount > 0)
				{
					/* save number of registered clients for this employee */
					sprintf(clCount,"%ld",ilCount);
					dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayPutField for <%s>...",clStfUrno);
					llFieldNum = 2;
					ilRc = AATArrayPutField(&hAccPreFetch,"ACCPRE",NULL,"COUNT",llRowNum,FALSE,clCount);
					if (ilRc != RC_SUCCESS)
					{
						dbg(DEBUG,"AddStfToAccountPrefetch(): AATArrayPutField failed for %s with (error: %s)",clStfUrno,CedaArrayErrorCodeToString(ilRc));
						return FALSE;
					}

					dbg(DEBUG,"AddStfToAccountPrefetch(): count for <%s> is now %d",clAccKeyValue,ilCount);
				}
				else
				{
					/* delete data of this employee from Prefetch array */
					dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayDeleteRow for row <%ld> of 'ACCPRE'",llRowNum);
					ilRc = AATArrayDeleteRow(&hAccPreFetch,"ACCPRE",llRowNum);
					if (ilRc != RC_SUCCESS)
					{
						dbg(DEBUG,"AddStfToAccountPrefetch(): AATArrayDeleteRow failed for row %ld with (error: %s)",llRowNum,CedaArrayErrorCodeToString(ilRc));
						return FALSE;
					}

					/* delete account data of this employee from ACC array */
					if (ilAccHandle > -1)
					{
						llRowNum = ARR_LAST;
						ilRc = RC_SUCCESS;
						while (ilRc == RC_SUCCESS)
						{
							ilRc = AATArrayFindRowPointer(&sgRecordsets[ilAccHandle].hHandle,sgRecordsets[ilAccHandle].cArrayName,
                           					&sgRecordsets[ilAccHandle].hIndexHandle, sgRecordsets[ilAccHandle].cIndexName,
                           					clAccKeyValue,&llRowNum, (void *)&pclMatchingRow);
                           				if (ilRc == RC_SUCCESS)
                           				{
                           					dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayDeleteRow for row <%ld> of 'ACCHDL'",llRowNum);
                           					AATArrayDeleteRow(&sgRecordsets[ilAccHandle].hHandle,sgRecordsets[ilAccHandle].cArrayName,llRowNum);
                           					llRowNum = ARR_PREV;
                           				}
                           				else
                           				{
                           					dbg(DEBUG,"AddStfToAccountPrefetch(): No more rows found for key <%s>.",clAccKeyValue);
                           				}
						}
					}
					else
					{
						dbg(DEBUG,"AddStfToAccountPrefetch(): Handle 'ACCHDL' invalid, <ilAccHandle> = <%d>. No records deleted.",ilAccHandle);
					}

				}
			}
		}
	}
	while(ilLen > 0 && *pclStfuList != '\0');

	if (ilStfuCount > 0)
	{
		strcat(pclAccWhere,") and year = \'");
		strcat(pclAccWhere,pcpYear);
		strcat(pclAccWhere,"\'");

		/* now add records to acctab	*/
		if (ilAccHandle == -1)
		{
			ilAccHandle = OpenACCRecordsetEx(pclAccWhere);
			if (ilAccHandle < 0)
			{
				dbg(DEBUG,"AddStfToAccountPrefetch: OpenACCRecordsetEx failed for %s with (error: %s)",pclAccWhere,CedaArrayErrorCodeToString(ilRc));
				free (pclAccWhere);
				return FALSE;
			}
		}
		else
		{
			ilRc = AppendRecords(ilAccHandle,pclAccWhere,FALSE);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"AddStfToAccountPrefetch: AppendRecords failed for %s with (error: %s)",pclAccWhere,CedaArrayErrorCodeToString(ilRc));
				free (pclAccWhere);
				return FALSE;
			}
		}
	}

	if (pclAccWhere != NULL)
	{
		free (pclAccWhere);
	}

/****
	DumpArray(&sgRecordsets[ilAccHandle]);
*****/
	return TRUE;
}

/******************************************************************************/
/* Append records to CEDArray                                                 */
/******************************************************************************/
static int AppendRecords(int ipRSIndex,const char* pcpWhere,BOOL blFirst)
{
	int ilRC = RC_SUCCESS;
	int	ilFirst = ARR_LAST;

	/* check, if index of array is valid and if there is an index for this array */
	if (ipRSIndex < 0 || ipRSIndex >= MAX_ARRAY || sgRecordsets[ipRSIndex].hHandle == -1)
	{
		/* invalid index or no valid array at this position */
		dbg(DEBUG,"AppendRecords: invalid index <%d> or no CEDAArray at this position !!!",ipRSIndex);
		return -1;
	}

	/*	deactivate index prior to any insertations, if any */
	if (sgRecordsets[ipRSIndex].hIndexHandle != -1)
	{
		ilRC = CEDAArrayDisactivateIndex(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&sgRecordsets[ipRSIndex].hIndexHandle,sgRecordsets[ipRSIndex].cIndexName);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"AppendRecords: error disactivating index on CEDAArray '%s' (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		}
	}

	/*	dbg(TRACE,"OpenRecordset:  append to CEDAArray '%s'...",pcpArrayName);*/
	if (blFirst)
		ilFirst = ARR_FIRST;
	else
		ilFirst = ARR_LAST;
	if ((ilRC = CEDAArrayRefill(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,(char *)pcpWhere,""/* AddFieldData */,ilFirst)) != RC_SUCCESS)
	{
		/* an error occurred */
		dbg(DEBUG,"OpenRecordset: error appending records to CEDAArray '%s' (%s)!!!",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		/*	activate index post to any insertations, if any */
		if (sgRecordsets[ipRSIndex].hIndexHandle != -1)
		{
			ilRC = CEDAArrayActivateIndex(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&sgRecordsets[ipRSIndex].hIndexHandle,sgRecordsets[ipRSIndex].cIndexName);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"AppendRecords: error activating index on CEDAArray '%s' (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
			}
		}
		return OPEN_ERROR_FILL;
	}

	/*	activate index post to any insertations, if any */
	if (sgRecordsets[ipRSIndex].hIndexHandle != -1)
	{
		ilRC = CEDAArrayActivateIndex(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&sgRecordsets[ipRSIndex].hIndexHandle,sgRecordsets[ipRSIndex].cIndexName);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"AppendRecords: error activating index on CEDAArray '%s' (%s)",sgRecordsets[ipRSIndex].cArrayName,CedaArrayErrorCodeToString(ilRC));
		}
	}

	/* store the row count */
	CEDAArrayGetRowCount(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,&sgRecordsets[ipRSIndex].lRowCount);

	/* info */ 
	if (debug_level == DEBUG) CEDAArrayInfoOneArray(&sgRecordsets[ipRSIndex].hHandle,sgRecordsets[ipRSIndex].cArrayName,outp);

	return RC_SUCCESS;
}



/* check whether staff is prefetched or not	*/
static BOOL IsStfPrefetched(const char *pcpStfu,const char *pcpYear)
{
	char *pclStfUrno = (char *)pcpStfu;
	const char* pcpCurrYear = pcpYear;

	char clAccKeyValue[30];
	int ilRc;
	long llRowNum = ARR_FIRST;
	char *pclMatchingRow = NULL;

	strcpy(clAccKeyValue,pcpCurrYear);
	strcat(clAccKeyValue,",");

	sprintf(&clAccKeyValue[5],"%-10s",pclStfUrno);

	dbg(DEBUG,"StfIsPrefetched(): calling AATArrayFindRowPointer for <%s>...",clAccKeyValue);
	llRowNum = ARR_FIRST;
	ilRc = AATArrayFindRowPointer(&hAccPreFetch,"ACCPRE",&hAccPreFetchInd,"YEAR,STFU",clAccKeyValue,&llRowNum,(void *)&pclMatchingRow);
	if (ilRc == RC_SUCCESS)
		dbg(DEBUG,"StfIsPrefetched(): Record for stfu = <%s> and year <%s> found",pcpStfu,pcpYear);
	else
		dbg(DEBUG,"StfIsPrefetched(): Record for stfu = <%s> and year <%s> not found",pcpStfu,pcpYear);
	
	return ilRc == RC_SUCCESS;
}

/* delete accounts from staff	*/
static BOOL DeleteAccFromStf(const char *pcpStfu,const char *pcpYear)
{
	char clAccKeyValue[30];
	int ilRc;
	long llRowNum = ARR_FIRST;
	char *pclMatchingRow = NULL;

	int ilLen;
	int ilAccHandle;


	/* accounts bereits vorhanden ?	*/
	ilAccHandle = GetArrayIndex("ACCHDL");

	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");

	sprintf(&clAccKeyValue[5],"%-10s",pcpStfu);

	/* delete account data of this employee from ACC array */
	if (ilAccHandle > -1)
	{
		llRowNum = ARR_LAST;
		ilRc = RC_SUCCESS;
		while (ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayFindRowPointer(&sgRecordsets[ilAccHandle].hHandle,sgRecordsets[ilAccHandle].cArrayName,
                     					&sgRecordsets[ilAccHandle].hIndexHandle, sgRecordsets[ilAccHandle].cIndexName,
                       					clAccKeyValue,&llRowNum, (void *)&pclMatchingRow);
                	if (ilRc == RC_SUCCESS)
                        {
                        	dbg(DEBUG,"AddStfToAccountPrefetch(): calling AATArrayDeleteRow for row <%ld> of 'ACCHDL'",llRowNum);
                           	AATArrayDeleteRow(&sgRecordsets[ilAccHandle].hHandle,sgRecordsets[ilAccHandle].cArrayName,llRowNum);
                           	llRowNum = ARR_PREV;
                        }
                        else
                        {
                        	dbg(DEBUG,"AddStfToAccountPrefetch(): No more rows found for key <%s>.",clAccKeyValue);
                        }
		}
		
		return TRUE;
	}
	else
	{
		dbg(DEBUG,"AddStfToAccountPrefetch(): Handle 'ACCHDL' invalid, <ilAccHandle> = <%d>. No records deleted.",ilAccHandle);
		return FALSE;
	}
}


BOOL RefillAccFromStf(const char *pcpStfuList,const char *pcpYear)
{
	char	pclData[256];	
	char	clAccWhere[ARR_MAXSELECTLEN];
	char	clCurrentYear[6];
	char	clCurrentEmpl[12];
	int	ilAccHandle;
	int	ilStfuCount= 0;
	int	ilRc;
	int	ilEmpCounter;
	int	ilCountEmpl;
	
	ilAccHandle = -1;
	ilStfuCount = 0;
	clCurrentYear[0] = '\0';
	
	ilAccHandle = GetArrayIndex("ACCHDL");
	ilCountEmpl = CountElements((char *)pcpStfuList);
	
	for (ilEmpCounter = 0; ilEmpCounter < ilCountEmpl; ilEmpCounter++)
	{
		GetItem((char *)pcpStfuList,ilEmpCounter,",",clCurrentEmpl);

		if (strlen(clCurrentYear) == 0)
		{
			ilStfuCount = 0;
			strcpy(clCurrentYear,pcpYear);
			strcpy(clAccWhere,"where STFU in (");
		}
			
		if (ilStfuCount > silMaxUrnoSize)
		{
			strcat(clAccWhere,") and year = \'");
			strcat(clAccWhere,clCurrentYear);
			strcat(clAccWhere,"\'");

			/* now add records to acctab	*/
			if (ilAccHandle == -1)
			{
				ilAccHandle = OpenACCRecordsetEx(clAccWhere);
				if (ilAccHandle < 0)
				{
					dbg(DEBUG,"RefillAccFromStf: OpenACCRecordsetEx failed for %s with (error: %s)",clAccWhere,CedaArrayErrorCodeToString(ilRc));
					return FALSE;
				}
			}
			else
			{
				ilRc = AppendRecords(ilAccHandle,clAccWhere,FALSE);
				if (ilRc != RC_SUCCESS)
				{
					dbg(DEBUG,"RefillAccFromStf: AppendRecords failed for %s with (error: %s)",clAccWhere,CedaArrayErrorCodeToString(ilRc));
					return FALSE;
				}
			}

			ilStfuCount = 0;
			strcpy(clCurrentYear,pcpYear);
			strcpy(clAccWhere,"where STFU in (");
		}

		if (ilStfuCount == 0)
		{
			sprintf(pclData,"\'%s\'",clCurrentEmpl);
		}
		else
		{
			sprintf(pclData,",\'%s\'",clCurrentEmpl);
		}

		strcat(clAccWhere,pclData);
		++ilStfuCount;
	}
		
	dbg(DEBUG,"RefillAccFromStf: ilStfuCount <%d>",ilStfuCount);
 
	if (ilStfuCount > 0)
	{
		strcat(clAccWhere,") and year = \'");
		strcat(clAccWhere,clCurrentYear);
		strcat(clAccWhere,"\'");

		/* now add records to acctab	*/
		if (ilAccHandle == -1)
		{
			ilAccHandle = OpenACCRecordsetEx(clAccWhere);
			if (ilAccHandle < 0)
			{
				dbg(DEBUG,"RefillAccFromStf: OpenACCRecordsetEx failed for %s with (error: %s)",clAccWhere,CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
		}
		else
		{
			ilRc = AppendRecords(ilAccHandle,clAccWhere,FALSE);
			if (ilRc != RC_SUCCESS)
			{
				dbg(DEBUG,"RefillAccFromStf: AppendRecords failed for %s with (error: %s)",clAccWhere,CedaArrayErrorCodeToString(ilRc));
				return FALSE;
			}
		}
	}

	return TRUE;
}

/* Initialize temporary fields	*/

static BOOL InitTmpFields()
{
	long llFieldLength[2];
	int  ilRc;
	char clDebugLevel[2];
	char clMaxUrnoSize[16];

	llFieldLength[0] = MAX_FILENAME;
	llFieldLength[1] = MAX_EXTRA_BUF;

	dbg(DEBUG,"InitTmpFields: calling AATArrayCreate...");
	if ((ilRc = AATArrayCreate(&hTmpFields,"TMPFLD",TRUE,100,2,llFieldLength,"NAME,VALUE",NULL)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitTmpFields: AATArrayCreate failed for %s with (error: %s)","TMPFLD",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	dbg(DEBUG,"InitTmpFields: calling AATArrayCreateSimpleIndex...");
	if ((ilRc = AATArrayCreateSimpleIndex(&hTmpFields,"TMPFLD",&hTmpFieldsInd,"NAME","NAME",ARR_DESC,FALSE)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitTmpFields: AATArrayCreateSimpleIndex failed for %s with (error: %s)","TMPFLD",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	if (debug_level == DEBUG)
	{
		strcpy(clDebugLevel,"2");
	}
	else if (debug_level == TRACE)
	{
		strcpy(clDebugLevel,"1");
	}
	else
	{
		strcpy(clDebugLevel,"0");
	}

	if (WriteTemporaryField("debug_level",clDebugLevel) != RC_SUCCESS)
		return FALSE;

	sprintf(clMaxUrnoSize,"%d",silMaxUrnoSize);
	if (WriteTemporaryField("MaxUrnoSize",clMaxUrnoSize) != RC_SUCCESS)
		return FALSE;

	return TRUE;
}

/* Initialize account prefetch	*/

static BOOL InitAccountPrefetch()
{
	long llFieldLength[3];
	int  ilRc;

	llFieldLength[0] = 4;	/* size = year	*/
	llFieldLength[1] = 10;	/* size = stfu	*/
	llFieldLength[2] = 10;	/* counter		*/

	dbg(DEBUG,"InitAccountPrefetch: calling AATArrayCreate...");
	if ((ilRc = AATArrayCreate(&hAccPreFetch,"ACCPRE",TRUE,1000,3,llFieldLength,"YEAR,STFU,COUNT",NULL)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitAccountPrefetch: AATArrayCreate failed for %s with (error: %s)","ACCPRE",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	dbg(DEBUG,"InitAccountPrefetch: calling AATArrayCreateSimpleIndex...");
	if ((ilRc = AATArrayCreateSimpleIndex(&hAccPreFetch,"ACCPRE",&hAccPreFetchInd,"YEAR,STFU","YEAR,STFU",ARR_DESC,FALSE)) != RC_SUCCESS)
	{
		dbg(DEBUG,"InitAccountPrefetch: AATArrayCreateSimpleIndex failed for %s with (error: %s)","ACCPRE",CedaArrayErrorCodeToString(ilRc));
		return FALSE;
	}

	return TRUE;
}

static int SendBc(char *pcpComand,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpData,char *pcpWksName)
{
	int ilRc = RC_SUCCESS;
	int ilBcLen;
	int ilLength;
	BC_HEAD rlBcHead;
	CMDBLK rlCmdBlk;

	ilLength = strlen(pcpTable)+strlen(pcpSelection)+strlen(pcpFields);
	ilBcLen = ilLength + strlen(pcpData);

	dbg(DEBUG,"%05d: SendBC: BcLen %d Table %d Selection %d Fields %d Data %d",
		__LINE__, ilBcLen,strlen(pcpTable),
		strlen(pcpSelection),strlen(pcpFields),strlen(pcpData));
	dbg(DEBUG,"%5d: SendBC: %s <%s>",__LINE__,
	pcpComand,pcpFields);
	dbg(DEBUG,"%5d: SendBC: %s <%s>",__LINE__,
		pcpComand,pcpData);
	memset(&rlBcHead,0,sizeof(rlBcHead));
	memset(&rlCmdBlk,0,sizeof(rlCmdBlk));
	strcpy(rlBcHead.dest_name,"INITACC");
	strcpy(rlBcHead.orig_name,"SCBHDL");
	strcpy(rlBcHead.recv_name,pcpWksName);
	strcpy(rlBcHead.seq_id,pcpWksName);

	strcpy(rlCmdBlk.obj_name,pcpTable);
	strcpy(rlCmdBlk.command,pcpComand);
	if (strcmp(pcpComand,"XBS2")== 0)
		strcpy(rlCmdBlk.tw_start,"RECALC");
	else
		strcpy(rlCmdBlk.tw_start,"INIT_MUL");
	sprintf(rlCmdBlk.tw_end,"%s,%s,%s","HEU","TAB","SCBHDL");

	ilRc = MyNewToolsSendSql(igBchdlModid,&rlBcHead,&rlCmdBlk,pcpSelection,pcpFields,pcpData);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"%5d: SendBC failed with ec = %d: for %d <%s>",__LINE__,ilRc,mod_id+1,pcpData);
	}
	else
	{
		dbg(DEBUG,"%5d: SendBC: %d <%s>",__LINE__,mod_id+1,pcpData);
	}

	return ilRc;
}

static int MyNewToolsSendSql(int ipRouteID,BC_HEAD *prpBchd,CMDBLK *prpCmdblk,char *pcpSelection,char *pcpFields,char *pcpData)
{
	int 		ilRc = RC_SUCCESS;
  	static int ilTotalLen=0;
	int		ilLength=0;
  	BC_HEAD 	*prlBchd;		/* Broadcast header		*/
  	CMDBLK  	*prlCmdBlk;		/* Command Block 		*/
  	char 		*pclDataBlk;		/* Data Block			*/
  	char		*pclSel;
  	char		*pclField;
  	static EVENT 	*prlOutEvent = NULL;

	dbg(DEBUG,"Entering new_tools_*. TotalLen<%d>  ilRc<%d> prlOutEvent<%ld>  DataLength<%i>",ilTotalLen,prpBchd->rc,prlOutEvent,strlen(pcpData));
	if( prpBchd->rc!= DB_SUCCESS )
	{
		/* if there was an error then the data area gets copied to the area BCDH->data */
  		ilLength = 	strlen(pcpFields) + strlen(pcpData) + strlen(pcpData) +
  					strlen(prpCmdblk->data) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
  					sizeof(EVENT) + 12; 
  	}
	else
	{
  		ilLength = 	strlen(pcpFields) + strlen(pcpData) + strlen(pcpSelection) + 
    				sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 12; 
    }
    
	dbg(DEBUG,"new_tools_: ilLength<%d>",ilLength);
	if (prlOutEvent==NULL) 
	{
		while(ilTotalLen<ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent = (EVENT *) malloc(ilTotalLen);
		if (prlOutEvent == NULL)
		{
			ilRc= RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc error %s",strerror(errno));
		} 
		else 
		{
			dbg(DEBUG,"new_tools_send_sql: Alloced prlOutEvent(size= %d)",ilTotalLen);
		}
	} 
	else if (ilTotalLen<=ilLength)
	{
		while(ilTotalLen<=ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent=(EVENT *) realloc(prlOutEvent,ilTotalLen);
		if(prlOutEvent==NULL)
		{
			ilRc=RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc: %s", strerror(errno)); 
		} 
		else 
		{
			dbg(DEBUG,"new_tools_send_sql: Realloced prlOutEvent<size %d>",ilTotalLen);    
		}
	} 
	else 
	{
		dbg(DEBUG,"new_tools_send_sql: Already alloced<size %d>",ilTotalLen);
	}
	if(ilRc!=RC_FAIL) 
	{
		memset (((char *) prlOutEvent), 0, ilTotalLen);
		prlOutEvent->data_offset = sizeof(EVENT);
		prlOutEvent->originator = mod_id; /* should B global */
		prlOutEvent->data_length =  ilLength;
		prlOutEvent->command = EVENT_DATA;
 
		prlBchd = (BC_HEAD *) (((char *)prlOutEvent) + sizeof(EVENT));
		memcpy(prlBchd,prpBchd,sizeof(BC_HEAD));
		/* prlBchd->rc=prpBchd->rc; */
		DebugPrintBchead(TRACE,prlBchd);
		if( prpBchd->rc!= DB_SUCCESS ) 
		{
			/* if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */
			strcpy(prlBchd->data,pcpData);
			dbg(DEBUG,"prlBchd->data <%s>|| pcpData<%s>",prlBchd->data,pcpData);
			prlCmdBlk = (CMDBLK *)((char *)prlBchd->data+strlen(pcpData)+1);
			dbg(DEBUG,"new_tool: prlCmblk<%s>",prlCmdBlk);
		}
		else
		{
			prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
		}

		memcpy(prlCmdBlk,prpCmdblk,sizeof(CMDBLK));
		DebugPrintCmdblk(TRACE,prlCmdBlk);

		pclSel = prlCmdBlk->data;
		strcpy (pclSel, pcpSelection);
		pclField = (char *) pclSel + strlen (pclSel) + 1;
		strcpy (pclField, pcpFields);
		pclDataBlk   = pclField + strlen(pclField) + 1;
		strcpy (pclDataBlk, pcpData);
		dbg(DEBUG,"new_tools: Sel<%s> Fields<%s> Data<%s>",pclSel,pclField,pclDataBlk);
		ilRc = que(QUE_PUT,ipRouteID,mod_id,PRIORITY_3,ilLength,(char *)prlOutEvent); 
		dbg(DEBUG,"new_tools_send_sql: Returning rc=%d", ilRc);
	}
	return ilRc;
}

BOOL MustHandleEvent(const char *pcpTableName,char *pcpCompare)
{
	static char *pcsTables;
	if (!strcmp(mod_name, "scbhdl"))
	{
		/* tables of scbhdl */
		pcsTables = "BSDTAB,ODATAB,SCOTAB,COTTAB,STFTAB,HOLTAB,ACITAB,PARTAB";
	}
	else
	{
		/* tables of xbshdl */
		pcsTables = "STFTAB,SCOTAB,COTTAB,MFMTAB,NWHTAB,COHTAB,PARTAB";
	}

	if (strcmp(pcpTableName,"ACCTAB") == 0)
	{
		strcpy(pcpCompare,pcgCmd);
		strcat(pcpCompare,pcpTableName);
		strcat(pcpCompare,",");
	}
	else if (strstr(pcsTables,pcpTableName) != NULL)
	{
		strcpy(pcpCompare,pcpTableName);
		strcat(pcpCompare,",");
	}
	else
	{
		strcpy(pcpCompare,pcgTwStart);
		return FALSE;
	}

	return TRUE;
}

#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->lFieldOffset[ipFieldNo]])

static int DumpArray(Recordset *prpArray)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;
	int	 ilArrayFieldCnt = 1;
	int	 ilCharNo;

	dbg(TRACE,"Arraydump: %s", prpArray->cArrayName);
	dbg(TRACE,"Arraydump: Fields = %s", prpArray->cFieldList);

	for (ilCharNo = 0; ilCharNo < strlen(prpArray->cFieldList); ilCharNo++)
	{
		if (prpArray->cFieldList[ilCharNo] == ',')
			++ilArrayFieldCnt;
	}
	while(CEDAArrayGetRowPointer(&prpArray->hHandle,prpArray->cArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
	{
			int ilFieldNo;

			ilRecord++;
			llRowNum = ARR_NEXT;

			dbg(TRACE,"Record No: %d",ilRecord);
			for (ilFieldNo = 1; ilFieldNo < ilArrayFieldCnt; ilFieldNo++)
			{
				char pclFieldName[24];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,prpArray->cFieldList,ilFieldNo,',',"","\0\0");
				dbg(TRACE,"%s: <%s>",pclFieldName,PFIELDVAL(prpArray,pclRowBuf,ilFieldNo-1));
			}
	}

	return ilRc;

}

static int HandleRelAcc (char *pcpSelKey,char *pcpFields,char *pcpData)
{
	char clYearFrom[6];
	char clYear[6];
	char clCurrentEmpl[12];
	char *clStfuList = NULL;
	int  ilCountEmpl;
	int  ilEmpCounter;		
	int  ilStfuAdd;
	int  ilYear;
	int  ilYearFrom;
	int  ilYearTo;
				
	if (!pcpSelKey || !pcpFields || !pcpData)
		return RC_FAIL;

	dbg(DEBUG,"HandleRelAcc: Selection <%s>", pcpSelKey );
	dbg(DEBUG,"HandleRelAcc: Fields    <%s>", pcpFields );
	dbg(DEBUG,"HandleRelAcc: Data      <%s>", pcpData );
	
	strncpy(clYearFrom,pcpData,4);
	clYearFrom[4] = '\0';
	ilYearFrom = atoi(clYearFrom);
	ilYearTo   = ilYearFrom + 1;

	/* count employees */
	ilCountEmpl = CountElements(pcpSelKey);
	if (ilCountEmpl == 0)
		return RC_SUCCESS;

	clStfuList = (char *)calloc(1,ilCountEmpl * 12);		
	if (clStfuList == NULL)
		return RC_FAIL;

	for (ilYear = ilYearFrom; ilYear <= ilYearTo; ilYear++)
	{
		sprintf(clYear,"%d",ilYear);
				
		ilStfuAdd = 0;	
		for (ilEmpCounter = 0; ilEmpCounter < ilCountEmpl; ilEmpCounter++)
		{
			GetItem(pcpSelKey,ilEmpCounter,",",clCurrentEmpl);
			if (strlen(clCurrentEmpl) == 0)
				continue;
				
			if (IsStfPrefetched(clCurrentEmpl,clYear))
				DeleteAccFromStf(clCurrentEmpl,clYear);
	
			if (ilStfuAdd == 0)
			{
				strcpy(clStfuList,clCurrentEmpl);
			}
			else
			{
				strcat(clStfuList,",");
				strcat(clStfuList,clCurrentEmpl);
			}
	
			ilStfuAdd++;
		}
		
		/* refill the accounts for the staff's	*/
		RefillAccFromStf(clStfuList,clYear);
	}
							
	return RC_SUCCESS;
}

static int HandleRelDrr (char *pcpBCData)
{
	BOOL	blDoCalculation = FALSE;
	int		ilRC = RC_SUCCESS;
	int		ilLen;
	char	*pclBuff;
	char	*pclSelection;
	char	clDel='-';
	char    clStart[10]="", clEnd[10]="", clRelLevels[11];
	int     ilRC1, ilRC2, ilLevel1=0, ilLevel2=0;
	int 	ilIndex;
	char	clScriptName[MAX_SCRIPTNAME];

	if (!pcpBCData)
		return RC_FAIL;

	dbg(DEBUG,"HandleRelDrr: Data <%s>", pcpBCData );

	pclBuff = (char*)calloc ( 1, strlen(pcpBCData)+1 );
	ilLen = strlen(pcpBCData) * 3 / 2 + 130;
	pclSelection = (char*)calloc ( 1, ilLen ) ;

	if (!pclBuff || !pclSelection )
	{
		dbg(TRACE,"HandleRelDrr: calloc of <%d> bytes failed", ilLen );
		if (pclBuff)
		{
			free (pclBuff);
		}
		return RC_NOMEM;
	}
	else
	{
		dbg (DEBUG, "HandleRelDrr: Data len <%d> now allocated <%d>", strlen(pcpBCData), ilLen);
	}

	/* Check release levels, maybe nothing to be done */
	if (GetDataItem(pclBuff, pcpBCData, 4, clDel, "", "\0\0") > 0)
	{
		if (strlen (pclBuff) > 10)
		{
			pclBuff[10] = '\0';		/* to avoid buffer overflow */
		}

		/* we get the DB-string containing e.g. '�' instead of ',' ==> we have to convert it */
		ConvertDbStringToClient(pclBuff);

		ilRC1 = GetDataItem(clRelLevels, pclBuff, 1, ',', "", "\0\0");
		if (ilRC1 > 0)
			ilLevel1 = atoi (clRelLevels);

		ilRC2 = GetDataItem(clRelLevels, pclBuff, 2, ',', "", "\0\0");
		if (ilRC2 > 0)
			ilLevel2 = atoi ( clRelLevels );

		dbg(DEBUG,"HandleRelDrr: Found Release <%d>--><%d>", ilLevel1, ilLevel2);

		if (!ilLevel1 || !ilLevel2)	/* both release levels found ? */
		{
			ilRC = RC_NOT_FOUND;
		}
		else
		{	/* if not released to duty roster level, nothing to do */
			if (ilLevel2 != 2)
			{
				blDoCalculation = FALSE;
				dbg(DEBUG,"HandleRelDrr: No new calculation, because not released to duty roster level (level 2)!");
			}
			else
			{
				blDoCalculation = TRUE;
			}
		}
	}
	else
	{
		dbg(TRACE,"HandleRelDrr: wrong parameter count" );
		return RC_FAIL;
	}

	if (blDoCalculation == TRUE)
	{
		/* Check release dates to get the start month for the recalculation */
		if (GetDataItem(pclBuff, pcpBCData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy (clStart, pclBuff, 8);
			clStart[8] = '\0';
		}

		if (GetDataItem(pclBuff, pcpBCData, 2, clDel, "", "\0\0") > 0)
		{
			strncpy ( clEnd, pclBuff, 8 );
			clEnd[8] = '\0';
		}

		if ((strlen(clEnd)<8) || (strlen (clStart)<8))
		{
			dbg(TRACE,"HandleRelDrr: wrong format of start or end time of release!" );
			ilRC = RC_INIT_FAIL;
		}
		else
		{
			/* start the recalculation */
			bgRecalcOfflineAccounts = TRUE;
			WriteTemporaryField("IsRecalc","1");

			if ((ilIndex = CheckCommandAndGetXBSCommandIndex("RECALC")) != -1)
			{
				while (GetNextXBSScript(ilIndex,clScriptName))
				{
					dbg(DEBUG,"HandleRelDrr: executing script '%s'",clScriptName);
					ExecuteScript(clScriptName,NULL);
				}
				sgXBSCommands[ilIndex].pNextScript = sgXBSCommands[ilIndex].cScriptList;
			}

			bgRecalcOfflineAccounts = FALSE;
			WriteTemporaryField("IsRecalc","0");
		}
	}

	if (pclBuff)
		free (pclBuff);

	if (pclSelection)
		free (pclSelection);

	return ilRC;
}

static BOOL SetAccAction(int ipAccHandle,BOOL bpEnable)
{
	if (bgSendRelAcc && ipAccHandle >= 0)
	{
		if (bpEnable)
      			dbg(DEBUG,"SetAccAction():send changes to Action and Bchdl"); 
		else      			
      			dbg(DEBUG,"SetAccAction():don't send changes to Action and Bchdl"); 
		CEDAArraySendChanges2ACTION(&sgRecordsets[ipAccHandle].hHandle,sgRecordsets[ipAccHandle].cArrayName,bpEnable);
		CEDAArraySendChanges2BCHDL(&sgRecordsets[ipAccHandle].hHandle,sgRecordsets[ipAccHandle].cArrayName,bpEnable);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
static int SendRelaccBc(char *pcpSelection,char *pcpFields,char *pcpData)
{
	if (!bgSendRelAcc)	
		return FALSE;
	dbg(TRACE,"Send to bchdl follows");
	
    	(void) tools_send_info_flag	(
    					/* route_id = */igBchdlModid,
    					/* orig_id  = */0, 
    					/* dest_name= */pcgDestName,
    					/* orig_name= */"",
    					/* recv_name= */pcgRecvName,
    					/* seq_id   = */"",
    					/* refseq_id= */"",
    					/* tw_start = */pcgTwStart,
    					/* tw_end   = */pcgTwEnd,
                                     	/* cmd	    = */"SBC",
                                     	/* table    = */"RELACC",
                                     	/* selection= */pcpSelection,
                                     	/* fields   = */pcpFields,
                                     	/* data     = */pcpData,
                                     	/* flag	    = */0
                                     	);  
                                     	        
	dbg(TRACE,"Send to action follows");
	
	(void) tools_send_info_flag	(
					/* route_id = */igActionModid,
					/* orig_id  = */0, 
					/* dest_name= */pcgDestName,
					/* orig_name= */"",
					/* recv_name= */pcgRecvName,
                                     	/* seq_id   = */"",
                                     	/* refseq_id= */"",
                                     	/* tw_start = */pcgTwStart,
                                     	/* tw_end   = */pcgTwEnd,
                                     	/* cmd	    = */"SBC",
                                     	/* table    = */"RELACC",
                                     	/* selection= */pcpSelection,
                                     	/* fields   = */pcpFields,
                                     	/* data	    = */pcpData,
                                     	/* flag	    = */0
                                     	);          
	return TRUE;                      
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
