#ifndef _DEF_mks_version_hsbsub_c
  #define _DEF_mks_version_hsbsub_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_hsbsub_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/hsbsub.c 1.2 2004/08/10 20:22:00SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/************************************************************************/
/************************************************************************/
/*									*/
/* Hsb Subroutine library source					*/
/*									*/
/************************************************************************/
/************************************************************************/


/***
#define STH_USE
#define STH_IS_USED_IN_LIB
***/

#include	"glbdef.h"
#include    "quedef.h"
#include	"uevent.h"
#include    "initdef.h"
#include    "sthdef.h"
#include	"tools.h"
#include	"dbt.h"
#include	"hsbsub.h"
#include    "debugrec.h"


extern int mod_id;
extern int ctrl_sta;

static int GetSystemStateShm(int *pipState,char *pcpPattern);

/* ******************************************************************** */
/* */
/* ******************************************************************** */
int	SendRemoteShutdown(int ipModId)
{
	int ilRC = RC_SUCCESS;
	EVENT event;


	event.type = USR_EVENT;
	event.command = SHUTDOWN;
	event.originator = mod_id;
	event.retry_count = 0;
	event.data_offset = sizeof(EVENT);
	event.data_length = 0;

	ilRC = RemoteQue(QUE_PUT,ipModId,ipModId,PRIORITY_3,sizeof(EVENT),(char *) &event);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SendRemoteShutdown: RemoteQue failed <%d>",ilRC);
	}/* end of if */

	return(ilRC);

}/* end of SendRemoteShutdown */


/* ******************************************************************** */
/* */
/* ******************************************************************** */
int	RemoteQue(int ipFkt,int ipRoute,int ipOrig,int ipPrio,int ipLen, char *pcpData)
{
	int ilRC = RC_SUCCESS;
	int ilLen = 0;
	EVENT *prlOutEvent = NULL;
	ITEM *prlItem = NULL;
	char *pclData = NULL;

	if((ctrl_sta != HSB_ACTIVE) && ( ctrl_sta != HSB_ACT_TO_SBY))
	{
		return(RC_SUCCESS);
	}

	ilLen = sizeof(EVENT)+sizeof(ITEM)+ipLen;

	prlOutEvent = (EVENT *) malloc(ilLen);
	if(prlOutEvent == NULL)
	{
		ilRC = RC_FAIL;
	}else{
		memset((char *)prlOutEvent,0x00,ilLen);
		prlItem = (ITEM *) ((char *)prlOutEvent + sizeof(EVENT));
		pclData = (char *) prlItem->text;
	}

	/* die folgenden werte sind fuers QUE_PUT an netif */
	prlOutEvent->type = SYS_EVENT;
	prlOutEvent->command = REMOTE_QUE;
	prlOutEvent->originator = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = sizeof(ITEM)+ipLen;


	/* die folgenden werte werden auf dem remoterechner */
	/* als parameter an die que()-funktion uebergeben */
	/* que( prlItem->function,         */
	/*      prlItem->route,            */
	/*      prlItem->originator,       */
	/*      prlItem->priority,         */
	/*      prlItem->msg_length,       */
	/*      (char *) pcpData);         */

	prlItem->function = ipFkt;
	prlItem->route = ipRoute;
	prlItem->priority = ipPrio;
	prlItem->msg_length = ipLen;
	prlItem->originator = ipOrig;

	memcpy(pclData,pcpData,ipLen);

	ilRC = que(QUE_PUT,15432,mod_id,PRIORITY_3,ilLen,(char *)prlOutEvent);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"RemoteQue: que failed <%d>",ilRC);
		DebugPrintEvent(TRACE,prlOutEvent);
		DebugPrintItem(TRACE,prlItem);
		DebugPrintEvent(TRACE,(EVENT *)pcpData);
		DebugPrintEvent(TRACE,(EVENT *)pclData);
		dbg(TRACE,"RemoteQue: set system state to STANDALONE");
		set_system_state(HSB_STANDALONE);
	}/* end of if */

	free(prlOutEvent);

	return(ilRC);

}/* end of RemoteQue */


/* ******************************************************************** */
/* */
/* ******************************************************************** */
int TransferFile(char *pcpFileName)
{
	int ilRC = RC_SUCCESS;
	char clCfgFile[1024];
	char clRemoteHost[128];
	char clSystemStr[1024];

	memset(clCfgFile,0x00,1024);
	memset(clRemoteHost,0x00,128);
	memset(clSystemStr,0x00,1024);

	if(pcpFileName == NULL)
	{	
		return(RC_FAIL);
	}

	if((ctrl_sta != HSB_ACTIVE) && ( ctrl_sta != HSB_ACT_TO_SBY))
	{
		return(RC_SUCCESS);
	}

	sprintf(clCfgFile,"%s/hsb.dat",getenv("CFG_PATH"));

	ilRC = iGetConfigEntry(clCfgFile,"main","RHOST",CFG_STRING,clRemoteHost);
	if(ilRC == RC_SUCCESS)
	{
		sprintf(clSystemStr,"rcp %s %s:%s",pcpFileName,clRemoteHost,pcpFileName);

		ilRC = system(clSystemStr) / 256;

		dbg(DEBUG,"TransferFile: system(%s) retunred <%d>",clSystemStr,ilRC);

		if(ilRC != RC_SUCCESS)
		{
			ilRC = iGetConfigEntry(clCfgFile,"main","RHOST2",CFG_STRING,clRemoteHost);
			if(ilRC == RC_SUCCESS)
			{
				sprintf(clSystemStr,"rcp %s %s:%s",pcpFileName,clRemoteHost,pcpFileName);

				ilRC = system(clSystemStr) / 256;

				dbg(DEBUG,"TransferFile: system(%s) retunred <%d>",clSystemStr,ilRC);

				if(ilRC != RC_SUCCESS)
				{
					dbg(TRACE,"RemoteQue: set system state to STANDALONE");
					set_system_state(HSB_STANDALONE);
				}/* end of if */
			}else{
				dbg(TRACE,"TransferFile: iGetConfigEntry <%s>,main,RHOST2 failed",clCfgFile);
				dbg(TRACE,"RemoteQue: set system state to STANDALONE");
				set_system_state(HSB_STANDALONE);
			}/* end of if */
		}/* end of if */
	}else{
		dbg(TRACE,"TransferFile: iGetConfigEntry <%s>,main,RHOST failed",clCfgFile);
		dbg(TRACE,"RemoteQue: set system state to STANDALONE");
		set_system_state(HSB_STANDALONE);
	}/* end of if */

	return(ilRC);

}/* end of TransferFile */


/* ******************************************************************** */
/* get system status from shared memory                                 */
/* ******************************************************************** */
int SetApplicationState(int ipNewState)
{

	int ilRC = RC_SUCCESS;
	int ilLoop = 0;
	char clStateText[64];
	char clShmRecord[72];
	STHBCB bcb;
	SRCDES rlSearchDescriptor;
	unsigned long ret = STNORMAL;

	switch(ipNewState)
	{
	case APP_OFFLINE :
		memset(clStateText,0x00,64);
		sprintf(clStateText,"OFFLINE");
		break;

	case APP_ONLINE :
		memset(clStateText,0x00,64);
		sprintf(clStateText,"ONLINE");
		break;

	default :
		dbg(TRACE,"SetApplicationState: invalid 1. parameter <%d>",ipNewState);
		ilRC = RC_FAIL;
		break;
	} /* end of switch */

	if(ilRC == RC_SUCCESS)
	{
		memset(clShmRecord,0x00,72);

		sprintf(clShmRecord,"_APPSTS_");

		rlSearchDescriptor.nobytes = strlen(clShmRecord);
		rlSearchDescriptor.increment = 0;

		for (ilLoop = 0; ilLoop < rlSearchDescriptor.nobytes; ilLoop++)
		{
			rlSearchDescriptor.bitmask |= (BITVHO >> ilLoop);
			rlSearchDescriptor.data[ilLoop] = clShmRecord[ilLoop];
		}/* end of for */

		bcb.function = (STRECORD|STSEARCH|STLOCATE);
		bcb.inputbuffer = (char *)&rlSearchDescriptor;
		bcb.outputbuffer = clShmRecord;
		bcb.tabno = HSTAB;
		bcb.recno = 0;

		ret = sth(&bcb, &gbladr);
		if (ret == STNORMAL)
		{
			sprintf(clShmRecord,"_APPSTS_");

			strcat(clShmRecord,clStateText);
	
			bcb.function = (STRECORD|STWRITE);
			bcb.inputbuffer = clShmRecord;
			bcb.outputbuffer = NULL;
			bcb.tabno = HSTAB;

			ret = sth(&bcb, &gbladr);
			if (ret != STNORMAL)
			{
				dbg(TRACE,"SetApplicationState: sth write failed <%d>",ret);
			} /* end if */
		}else{
			dbg(TRACE,"SetApplicationState: sth read failed <%d>",ret);
		} /* end if */
	} /* end if */

	return(ilRC);

}/* end of SetApplicationState */


/* ******************************************************************** */
/* get system status from shared memory                                 */
/* ******************************************************************** */
int GetApplicationState(void)
{
	int ilRC = RC_SUCCESS;
	int ilLoop = 0;
	char clPattern[32];
	STHBCB rlSthControlBlock;
	SRCDES rlSearchDescriptor;
	unsigned long ullRC;
	char clStatusText[1024];
	char *pclStateBuf = NULL;

	memset(clStatusText,0x00,1024);
	sprintf(clPattern,"_APPSTS_");

	/* Args for sth call */

    rlSearchDescriptor.nobytes = strlen(clPattern);
    rlSearchDescriptor.increment = 0;

    for (ilLoop = 0; ilLoop < rlSearchDescriptor.nobytes; ilLoop++)
    {
      rlSearchDescriptor.bitmask |= (BITVHO >> ilLoop);
      rlSearchDescriptor.data[ilLoop] = clPattern[ilLoop];
    }/* end of for */

	rlSthControlBlock.function = (STRECORD|STSEARCH|STLOCATE);
	rlSthControlBlock.inputbuffer = (char *) &rlSearchDescriptor;
    rlSthControlBlock.outputbuffer = (char *) &pclStateBuf;
	rlSthControlBlock.tabno = HSTAB;
	rlSthControlBlock.recno = 0;

	ullRC = sth(&rlSthControlBlock, &gbladr);
	switch (ullRC)
	{
	case  STNORMAL: 
		ilRC = RC_SUCCESS; 
		break;

	case  STINVREC: 
		ilRC = RC_NOT_FOUND; 
		break;

	default: 
		ilRC = RC_FAIL; 
		break;
	} /* switch */

	if(ilRC == RC_FAIL)
	{
		dbg(TRACE,"GetApplicationState: ilRC<%d> ret<%d> recno<%d>",ilRC,ullRC,rlSthControlBlock.recno);
	}else{
		pclStateBuf += strlen(clPattern);

		if(strcmp(pclStateBuf,"OFFLINE") == 0)
		{
			ilRC = APP_OFFLINE;
		}else{
			if(strcmp(pclStateBuf,"ONLINE") == 0)
			{
				ilRC = APP_ONLINE;
			}else{
				ilRC = -1;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of GetApplicationState */


/* ******************************************************************** */
/* get system status from shared memory                                 */
/* ******************************************************************** */
static int GetSystemStateShm(int *pipStatus,char *pcpPattern)
{
	int ilRC = RC_SUCCESS;
	int ilLoop = 0;
	char clPattern[32];
	STHBCB rlSthControlBlock;
	SRCDES rlSearchDescriptor;
	unsigned long ullRC;
	char clStatusText[1024];
	char *pclStatusBuf = NULL;

	memset(clStatusText,0x00,1024);
	/* sprintf(clPattern,"_STATUS_"); */
	sprintf(clPattern,"%s",pcpPattern);

	/* dbg(DEBUG,"GetSystemStateShm: searching for pattern <%s>",clPattern); */

	/* Args for sth call */

    rlSearchDescriptor.nobytes = strlen(clPattern);
    rlSearchDescriptor.increment = 0;

    for (ilLoop = 0; ilLoop < rlSearchDescriptor.nobytes; ilLoop++)
    {
      rlSearchDescriptor.bitmask |= (BITVHO >> ilLoop);
      rlSearchDescriptor.data[ilLoop] = clPattern[ilLoop];
    }/* end of for */

	rlSthControlBlock.function = (STRECORD|STSEARCH|STLOCATE);
	rlSthControlBlock.inputbuffer = (char *) &rlSearchDescriptor;
    rlSthControlBlock.outputbuffer = (char *) &pclStatusBuf;
	rlSthControlBlock.tabno = HSTAB;
	rlSthControlBlock.recno = 0;

	ullRC = sth(&rlSthControlBlock, &gbladr);
	switch (ullRC)
	{
	case  STNORMAL: 
		ilRC = RC_SUCCESS; 
		break;

	case  STINVREC: 
		ilRC = RC_NOT_FOUND; 
		break;

	default: 
		ilRC = RC_FAIL; 
		break;
	} /* switch */

	if(ilRC == RC_FAIL)
	{
		dbg(TRACE,"GetSystemStateShm: ilRC<%d> ret<%d> recno<%d>",ilRC,ullRC,rlSthControlBlock.recno);
	}else{
		/* dbg(DEBUG,"GetSystemStateShm: pattern<%s> record<%d><%s>",clPattern,rlSthControlBlock.recno,pclStatusBuf); */

		pclStatusBuf += strlen(clPattern);

		ilRC = status2sts(pclStatusBuf,pipStatus);

	}/* fi */

	return(ilRC);

}/* end of GetSystemStateShm */


/* ******************************************************************** */
/* get previous status from hsb.dat					*/
/* ******************************************************************** */
int GetPreviousState(void)
{ 
	int ilRC = RC_SUCCESS;
	int	ilStatus = -1;
	
	ilRC = GetSystemStateShm(&ilStatus,"PREV_STS");

	return(ilStatus);
	
}/* end of GetPreviousState */


/* ******************************************************************** */
/* get system status from hsb.dat					*/
/* ******************************************************************** */
int get_system_state(void)
{ 
	int ilRC = RC_SUCCESS;
	int	ilStatus = -1;
	
	ilRC = GetSystemStateShm(&ilStatus,"_STATUS_");

	return(ilStatus);
	
}/* end of get_system_state */


/* ******************************************************************** */
/* send new system status to all other processes			*/
/* ******************************************************************** */
int set_system_state(int state)
{
	extern	mod_id;
	int	rc	= SUCCESS;
	EVENT	lev;
	char	clScriptName[1024];

	if((ctrl_sta == HSB_STANDBY) && (state == HSB_STANDALONE))
	{
		sprintf(clScriptName,"%s/UfisHardSwitch",getenv("ETC_PATH"));

		dbg(TRACE,"set_system_state: try to call <%s>",clScriptName);

		rc = system(clScriptName);

		dbg(TRACE,"set_system_state: <%s> returned <%d>",clScriptName,rc);
	}/* end of if */

	lev.type = SYS_EVENT;
	lev.command = state;
	lev.originator = mod_id;
	lev.retry_count = 0;
	lev.data_offset = 0;
	lev.data_length = 0;

	rc = que(QUE_HSBCMD,QCP_PRIMARY,mod_id,PRIORITY_4,sizeof(EVENT),(char *)&lev);
	if ( rc != SUCCESS)
	{
		dbg(TRACE,"set_system_state: que failed <%d>",rc);
		return(ERROR);
	}else{
		return(SUCCESS);
	}/* end of if */

} /* end of set_system_state */


/* ******************************************************************** */
/* convert an int status to a char status text				*/
/* ******************************************************************** */
int	sts2status(int *sts,char *status)
{
	int	rc=SUCCESS;

	switch(*sts){
	case HSB_DOWN :
		sprintf(status,"DOWN");
		break;
	case HSB_COMING_UP :
		sprintf(status,"COMING_UP");
		break;
	case HSB_STANDALONE :
		sprintf(status,"STANDALONE");
		break;
	case HSB_ACTIVE :
		sprintf(status,"ACTIVE");
		break;
	case HSB_ACT_TO_SBY :
		sprintf(status,"ACT_TO_SBY");
		break;
	case HSB_STANDBY :
		sprintf(status,"STANDBY");
		break;
	case HSB_RPC_ERROR :
		sprintf(status,"NOT CONNECTED");
		break;
	default:
		sprintf(status,"UNDEFINED");
		rc=ERROR;
	} /* end of switch */

	return(rc);

} /* end of sts2status */


/* ******************************************************************** */
/* convert a char status text to an int status				*/
/* ******************************************************************** */
int	status2sts(char *status ,int *sts)
{

	if(strncmp(status,"DOWN",4)==0)
	{
		*sts=HSB_DOWN;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"STANDBY",7)==0)
	{
		*sts=HSB_STANDBY;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"ACTIVE",6)==0)
	{
		*sts=HSB_ACTIVE;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"COMING_UP",10)==0)
	{
		*sts=HSB_COMING_UP;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"STANDALONE",10)==0)
	{
		*sts=HSB_STANDALONE;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"ACT_TO_SBY",10)==0)
	{
		*sts=HSB_ACT_TO_SBY;
		return(SUCCESS);
	}/* end of if */

	if(strncmp(status,"NOT CONNECTED",9)==0)
	{
		*sts=HSB_RPC_ERROR;
		return(SUCCESS);
	}/* end of if */

	return(ERROR);

} /* end of status2sts() */


/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
