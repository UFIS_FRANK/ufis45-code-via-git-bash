// CedaValData.cpp
 
#include <stdafx.h>
#include <CedaValData.h>
#include <resource.h>


void ProcessValCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

int GetYear(CString y){return atoi(y.Left(4));}
int GetMonth(CString y){return atoi(y.Mid(4,2));}
int GetDay(CString y){return atoi(y.Mid(6,2));}
int GetHour(CString y){return atoi(y.Mid(8,2));}
int GetMinute(CString y){return atoi(y.Mid(10,2));}
int GetSecond(CString y){return atoi(y.Mid(12,2));}
bool IsDayOfWeekSet(CString y, int dow){ return (y.Mid(dow,1) == "1");}	// 1=MON, 7=SUN


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaValData::CedaValData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for ValDataStruct
	BEGIN_CEDARECINFO(VALDATA,ValDataRecInfo)
		FIELD_CHAR_TRIM	(Appl,"APPL")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Freq,"FREQ")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Tabn,"TABN")
		FIELD_CHAR_TRIM	(Timf,"TIMF")
		FIELD_CHAR_TRIM	(Timt,"TIMT")
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Urue,"URUE")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_LONG		(Uval,"UVAL")
		FIELD_OLEDATE	(Vafr,"VAFR")
		FIELD_OLEDATE	(Vato,"VATO")
	END_CEDARECINFO //(ValDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ValDataRecInfo)/sizeof(ValDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ValDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"VAL");
	strcpy(pcmListOfFields,"APPL,CDAT,FREQ,LSTU,TABN,TIMF,TIMT,URNO,URUE,USEC,USEU,UVAL,VAFR,VATO");
	
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaValData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	//this Tabel is unvisible
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaValData::Register(void)
{
	ogDdx.Register((void *)this,BC_VAL_CHANGE,	CString("VALDATA"), CString("Val-changed"),	ProcessValCf);
	ogDdx.Register((void *)this,BC_VAL_NEW,		CString("VALDATA"), CString("Val-new"),		ProcessValCf);
	ogDdx.Register((void *)this,BC_VAL_DELETE,	CString("VALDATA"), CString("Val-deleted"),	ProcessValCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaValData::~CedaValData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaValData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omUvalMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaValData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omUvalMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		VALDATA *prlVal = new VALDATA;
		if ((ilRc = GetFirstBufferRecord(prlVal)) == true)
		{
			omData.Add(prlVal);//Update omData
			omUrnoMap.SetAt((void *)prlVal->Urno,prlVal);
			omUvalMap.SetAt((void *)prlVal->Uval,prlVal);
		}
		else
		{
			delete prlVal;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaValData::Insert(VALDATA *prpVal)
{
	prpVal->IsChanged = DATA_NEW;
	if(Save(prpVal) == false) return false; //Update Database
	InsertInternal(prpVal);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaValData::InsertInternal(VALDATA *prpVal)
{
	omData.Add(prpVal);//Update omData
	omUrnoMap.SetAt((void *)prpVal->Urno,prpVal);
	omUvalMap.SetAt((void *)prpVal->Uval,prpVal);
	ogDdx.DataChanged((void *)this, VAL_NEW,(void *)prpVal ); //Update Viewer
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaValData::Delete(long lpUrno)
{
	VALDATA *prlVal = GetValByUrno(lpUrno);
	if (prlVal != NULL)
	{
		prlVal->IsChanged = DATA_DELETED;
		if(Save(prlVal) == false) return false; //Update Database
		DeleteInternal(prlVal);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaValData::DeleteInternal(VALDATA *prpVal)
{
	ogDdx.DataChanged((void *)this,VAL_DELETE,(void *)prpVal); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpVal->Urno);
	omUvalMap.RemoveKey((void *)prpVal->Uval);
	int ilValCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilValCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpVal->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaValData::Update(VALDATA *prpVal)
{
	if (GetValByUrno(prpVal->Urno) != NULL)
	{
		if (prpVal->IsChanged == DATA_UNCHANGED)
		{
			prpVal->IsChanged = DATA_CHANGED;
		}
		if(Save(prpVal) == false) return false; //Update Database
		UpdateInternal(prpVal);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaValData::UpdateInternal(VALDATA *prpVal)
{
	VALDATA *prlVal = GetValByUrno(prpVal->Urno);
	if (prlVal != NULL)
	{
		*prlVal = *prpVal; //Update omData
		ogDdx.DataChanged((void *)this,VAL_CHANGE,(void *)prlVal); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

VALDATA *CedaValData::GetValByUrno(long lpUrno)
{
	VALDATA  *prlVal;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlVal) == TRUE)
	{
		return prlVal;
	}
	return NULL;
}

//--GET-FIRST-BY-UVAL--------------------------------------------------------------------------------------------

VALDATA *CedaValData::GetFirstValByUval(long lpUval)
{
	VALDATA  *prlVal;
	if (omUvalMap.Lookup((void *)lpUval,(void *& )prlVal) == TRUE)
	{
		return prlVal;
	}
	return NULL;
}
//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaValData::ReadSpecialData(CCSPtrArray<VALDATA> *popVal,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","VAL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","VAL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popVal != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			VALDATA *prpVal = new VALDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpVal,CString(pclFieldList))) == true)
			{
				popVal->Add(prpVal);
			}
			else
			{
				delete prpVal;
			}
		}
		if(popVal->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaValData::Save(VALDATA *prpVal)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpVal->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpVal->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpVal);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpVal->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpVal->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpVal);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpVal->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpVal->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessValCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogValData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaValData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlValData;
	prlValData = (struct BcStruct *) vpDataPointer;
	VALDATA *prlVal;
	if(ipDDXType == BC_VAL_NEW)
	{
		prlVal = new VALDATA;
		GetRecordFromItemList(prlVal,prlValData->Fields,prlValData->Data);
		InsertInternal(prlVal);
	}
	if(ipDDXType == BC_VAL_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlValData->Selection);
		prlVal = GetValByUrno(llUrno);
		if(prlVal != NULL)
		{
			GetRecordFromItemList(prlVal,prlValData->Fields,prlValData->Data);
			UpdateInternal(prlVal);
		}
	}
	if(ipDDXType == BC_VAL_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlValData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlValData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlVal = GetValByUrno(llUrno);
		if (prlVal != NULL)
		{
			DeleteInternal(prlVal);
		}
	}
}
//---------------------------------------------------------------------------
bool CedaValData::IsValid(long bpUval,const CString& opTimestamp)
{
	int i,j;

	COleDateTime mytime(GetYear(opTimestamp),GetMonth(opTimestamp),GetDay(opTimestamp),GetHour(opTimestamp),GetMinute(opTimestamp),GetSecond(opTimestamp));

	bool isvalid = false;
	for (int idx = 0; idx < omData.GetSize() && isvalid == false; idx++)
	{
		if (omData[idx].Uval == bpUval)
		{
			CString mydate;
			// pass 1: date from
			if (omData[idx].Vafr.m_status == COleDateTime::valid)
			{
				if (omData[idx].Vafr <= mytime) 
					isvalid = true;
				else
					isvalid = false;
				if (isvalid)
				{
					// pass 2: date until
					if (omData[idx].Vato.m_status == COleDateTime::valid)
					{
						if (omData[idx].Vato >= mytime) 
							isvalid = true;
						else
							isvalid = false;
						if (isvalid)
						{
							// pass 3: day of week
							mydate=omData[idx].Freq;
							if (mydate != "")
							{
								i = mytime.GetDayOfWeek()-1;
								if (i == 0) 
									i=7;		// wrap around for ufis dayofweek format
								isvalid = IsDayOfWeekSet(mydate,i-1);
							}
							// pass 4: daily time frame
							if (isvalid)
							{
								j=atoi(opTimestamp.Mid(10,4));
								if (isvalid && (strcmp(omData[idx].Timf,"") != 0)) isvalid=(atoi(omData[idx].Timf)<=j);
								if (isvalid && (strcmp(omData[idx].Timt,"") != 0)) isvalid=(atoi(omData[idx].Timt)>=j);
							}
						}
					}
				}
			}
			else
				isvalid = true;
		}
	}
	return isvalid;

}