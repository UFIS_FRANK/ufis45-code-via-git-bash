/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CInitialLoad dialog
#ifndef __INITIAL_LOAD_DLG__
#define __INITIAL_LOAD_DLG__

#include "resource.h"

class CInitialLoadDlg : public CDialog
{
// Construction
public:
	CInitialLoadDlg(CWnd* pParent = NULL);   // standard constructor
	
	~CInitialLoadDlg(void);
	void SetProgress(int ipProgress);
	void SetMessage(CString opMessage);

	BOOL DestroyWindow(void);

// Dialog Data
	//{{AFX_DATA(CInitialLoadDlg)
	enum { IDD = IDD_INITIALLOAD };
	CProgressCtrl	m_Progress;
	CListBox	m_MsgList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInitialLoadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInitialLoadDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CInitialLoadDlg *pogInitialLoad;


#endif //__INITIAL_LOAD_DLG__
