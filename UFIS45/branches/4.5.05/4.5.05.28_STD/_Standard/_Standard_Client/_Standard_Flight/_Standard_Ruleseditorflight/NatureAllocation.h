#if !defined(AFX_NATUREALLOCATION_H__E61A6CE1_F723_11D2_A1A9_0000B4984BBE__INCLUDED_)
#define AFX_NATUREALLOCATION_H__E61A6CE1_F723_11D2_A1A9_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NatureAllocation.h : header file
//

#include <CedaNatData.h>
#include <CCSGlobl.h>
/////////////////////////////////////////////////////////////////////////////
// NatureAllocation dialog

class NatureAllocation : public CDialog
{
// Construction
public:
	NatureAllocation(CWnd* pParent, NATDATA *popNat);   // standard constructor

	NATDATA *pomNat;
// Dialog Data
	//{{AFX_DATA(NatureAllocation)
	enum { IDD = IDD_NAT_ALLOC };
	BOOL	m_Alga;
	BOOL	m_Alpo;
	BOOL	m_Albb;
	BOOL	m_Alwr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NatureAllocation)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NatureAllocation)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NATUREALLOCATION_H__E61A6CE1_F723_11D2_A1A9_0000B4984BBE__INCLUDED_)
