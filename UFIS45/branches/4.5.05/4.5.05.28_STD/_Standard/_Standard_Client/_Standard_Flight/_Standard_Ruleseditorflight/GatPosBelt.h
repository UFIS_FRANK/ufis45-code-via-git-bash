#if !defined(AFX_GATPOSBELT_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATPOSBELT_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosBelt.h : header file
//
#include "CcsTable.h"
#include "BltRulesViewer.h"
#include "resource.h"
#include "resrc1.h"
/////////////////////////////////////////////////////////////////////////////
// GatPosRules dialog


class GatPosBelt : public CPropertyPage
{
	DECLARE_DYNCREATE(GatPosBelt)

// Construction
public:
	GatPosBelt();
	~GatPosBelt();

// Dialog Data
	//{{AFX_DATA(GatPosRules)
	enum { IDD = IDD_RULES_BELT };
	CStatic	m_BeltListTable;
	//}}AFX_DATA

	CCSTable *pomBltList;

	BltRulesViewer omBltRulesViewer;

	CUIntArray omBltUrnos;

	void MakeBltLineData(BLTDATA *popBlt, CStringArray &ropArray);
	void GatPosBelt::UpdateView();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GatPosBelt)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	void OnPrintBelt();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	int omBltContextItem;
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GatPosBelt)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnGridDblClk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSBELT_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
