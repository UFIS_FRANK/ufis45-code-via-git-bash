// PrcList.cpp : implementation file
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "PrcList.h"
#include "CCSGlobl.h"
#include "CedaPrcData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrcList dialog
static int CompareToPrc(const PRCDATA **e1, const PRCDATA **e2);


PrcList::PrcList(CWnd* pParent /*=NULL*/)
	: CDialog(PrcList::IDD, pParent)
{
	//{{AFX_DATA_INIT(PrcList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ogPrcData.Read();
	ogPrcData.omData.Sort(CompareToPrc);
}


void PrcList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PrcList)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PrcList, CDialog)
	//{{AFX_MSG_MAP(PrcList)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PrcList message handlers

void PrcList::OnDblclkList() 
{
	OnOK();	
	
}

void PrcList::OnOK() 
{
	int ilIndex = m_List.GetCurSel();
	//ACTDATA *prlAlt;
	if(ilIndex != LB_ERR)
	{
		omRedu = CString(ogPrcData.omData[ilIndex].Redc);
	}
	CDialog::OnOK();
}

BOOL PrcList::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	char pclList[500];
	m_List.SetFont(&ogCourier_Regular_8);
	int ilCount = ogPrcData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		sprintf(pclList, "%5s %-40s", 
			     ogPrcData.omData[i].Redc, ogPrcData.omData[i].Redn);
		m_List.AddString(pclList);
		PRCDATA *prlPrc = &ogPrcData.omData.GetAt(i);
		m_List.SetItemDataPtr( i, (void*)prlPrc);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
static int CompareToPrc(const PRCDATA **e1, const PRCDATA **e2)
{
	return (int)(strcmp((**e1).Redc,(**e2).Redc));
}
