// GatRuleDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Rules.h>
#include <GatRuleDlg.h>
#include <BasicDAta.h>
#include <CedaAltData.h>
#include <CedaAPTData.h>
#include <resrc1.h>
#include <CedaBasicData.h>
#include <CedaAloData.h>
#include <PositionMatrixDlg.h>
#include <AirCraftGroupSelDlg.h>
#include <CedaActData.h>
#include <GroupNamesPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg dialog
IMPLEMENT_SERIAL(GatRuleDlg,CDialog,0);


GatRuleDlg::GatRuleDlg(CWnd* pParent, GATDATA *popGat)
	: CDialog(GatRuleDlg::IDD, pParent)
{
	pomGat = popGat;
	//{{AFX_DATA_INIT(GatRuleDlg)
	m_Flti = _T("");
	m_FltiDep = _T("");
	m_Inbo = FALSE;
	m_Maxp = 0;
	m_Mult = 0;
	m_Natr = _T("");
	m_Outb = FALSE;
	m_Pral = _T("");
	m_Prio = 0;
	m_PDest = _T("");
	m_sequence = 1;
	m_STYP_List = _T("");
	m_Npos1 = _T("");
	m_Npos2 = _T("");
	m_Npos3 = _T("");
	m_Npos4 = _T("");
	m_Npos5 = _T("");
	m_Npos6 = _T("");
	m_Npos7 = _T("");
	m_Npos8 = _T("");
	m_Npos9 = _T("");
	m_Npos10= _T("");
	m_ACParam = _T("");
	m_ACGroup = _T("");
	m_Restr_To1 = FALSE;
	m_Restr_To2 = FALSE;
	m_Restr_To3 = FALSE;
	m_Restr_To4 = FALSE;
	m_Restr_To5 = FALSE;
	m_Restr_To6 = FALSE;
	m_Restr_To7 = FALSE;
	m_Restr_To8 = FALSE;
	m_Restr_To9 = FALSE;
	m_Restr_To10= FALSE;
	//}}AFX_DATA_INIT

	CStringArray olArray;
//	CString olStr = CString(pomGat->Resb);
	
	CString olTmpGatr = pomGat->Gatr;
	if(olTmpGatr.Replace(";","#") < 2)
	{
		olTmpGatr =pomGat->Gatr;
		olTmpGatr.Replace("/",";");
		strcpy(pomGat->Gatr, olTmpGatr.GetBuffer(0));
	}
	
	CString olStr = CString(pomGat->Gatr);
	ExtractItemList(olStr, &olArray, ';');
	if(olArray.GetSize() >= 9)
	{
		m_Flti = olArray[0];//_T("");
		m_Maxp = atoi(olArray[1]);//0;
		m_Mult = atoi(olArray[2]);//0;
		if(olArray[3] == "X")
		{
			m_Inbo = TRUE;//FALSE;
		}
		if(olArray[4] == "X")
		{
			m_Outb = TRUE;//FALSE;
		}

		m_Prio = atoi(olArray[5]);//0;
		m_Natr = olArray[6];//_T("");
		m_Pral = olArray[7];//_T("");
		m_PDest = olArray[8];//_T("");
	}

	// conversion from LIS4.4 to Standard4.5
	if (olArray.GetSize() == 11 && (strcmp("FCO",pcgHome) == 0 || strcmp("LIS",pcgHome) == 0))
	{
		m_FltiDep = "";
		m_sequence  =   atoi(olArray[9]);
		m_STYP_List  =   olArray[10];
	}
	else
	{
		if(olArray.GetSize() >= 10) //Flti departure added
			m_FltiDep = olArray[9];//_T("");
		else
			m_FltiDep = "";//_T("");

		if (olArray.GetSize() >= 11)
			m_sequence  =   atoi(olArray[10]);
		else
			m_sequence  =   1;

		//servicetypes
		if (olArray.GetSize() >= 12)
			m_STYP_List  =   olArray[11];
		else
			m_STYP_List  =   "";

		if (olArray.GetSize() >= 22)
		{
			m_Npos1		=	olArray[12];
			m_Npos2		=	olArray[13];
			m_Npos3		=	olArray[14];
			m_Npos4		=	olArray[15];
			m_Npos5		=	olArray[16];
			m_Npos6		=	olArray[17];
			m_Npos7		=	olArray[18];
			m_Npos8		=	olArray[19];
			m_Npos9		=	olArray[20];
			m_Npos10	=	olArray[21];
		}

		if (olArray.GetSize() >= 24)
		{
			m_ACParam = olArray[22];
			m_ACGroup = olArray[23];
		}

		if (olArray.GetSize() >= 34)
		{
			if(olArray[24] == "0")	m_Restr_To1=	TRUE; else m_Restr_To1=	FALSE;
			if(olArray[25] == "0")	m_Restr_To2=	TRUE; else m_Restr_To2=	FALSE;
			if(olArray[26] == "0")	m_Restr_To3=	TRUE; else m_Restr_To3=	FALSE;
			if(olArray[27] == "0")	m_Restr_To4=	TRUE; else m_Restr_To4=	FALSE;
			if(olArray[28] == "0")	m_Restr_To5=	TRUE; else m_Restr_To5=	FALSE;
			if(olArray[29] == "0")	m_Restr_To6=	TRUE; else m_Restr_To6=	FALSE;
			if(olArray[30] == "0")	m_Restr_To7=	TRUE; else m_Restr_To7=	FALSE;
			if(olArray[31] == "0")	m_Restr_To8=	TRUE; else m_Restr_To8=	FALSE;
			if(olArray[32] == "0")	m_Restr_To9=	TRUE; else m_Restr_To9=	FALSE;
			if(olArray[33] == "0")	m_Restr_To10=	TRUE; else m_Restr_To10=	FALSE;
		}

	}

}

GatRuleDlg ::GatRuleDlg()
{

}

void GatRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatRuleDlg)
	DDX_Control(pDX, IDC_SEQU, m_SequenceCtrl);
	DDX_Control(pDX, IDC_PRIO, m_PrioCtrl);
	DDX_Control(pDX, IDC_MULT, m_MultCtrl);
	DDX_Control(pDX, IDC_MAXP, m_MaxPCtrl);
	DDX_Text(pDX, IDC_FLTI, m_Flti);
	DDX_Check(pDX, IDC_INBO, m_Inbo);
	DDX_Text(pDX, IDC_MAXP, m_Maxp);
	DDV_MinMaxInt(pDX, m_Maxp, 0, 999);
	DDX_Text(pDX, IDC_MULT, m_Mult);
	DDV_MinMaxInt(pDX, m_Mult, 0, 99);
	DDX_Text(pDX, IDC_NATR, m_Natr);
	DDV_MaxChars(pDX, m_Natr, 150);
	DDX_Check(pDX, IDC_OUTB, m_Outb);
	DDX_Text(pDX, IDC_PRAL, m_Pral);
	DDV_MaxChars(pDX, m_Pral, 150);
	DDX_Text(pDX, IDC_PRIO, m_Prio);
	DDV_MinMaxInt(pDX, m_Prio, 0, 9);
	DDX_Text(pDX, IDC_PDEST, m_PDest);
	DDV_MaxChars(pDX, m_PDest, 150);
	DDX_Text(pDX, IDC_FLTIDEP, m_FltiDep);
	DDX_Text(pDX, IDC_SEQU, m_sequence);
	DDV_MinMaxInt(pDX, m_sequence, 1, 999);
	DDX_Text(pDX, IDC_STYP_LIST, m_STYP_List);
	DDV_MaxChars(pDX, m_STYP_List, 150);
	DDX_Text(pDX, IDC_NGAT1, m_Npos1);
	DDV_MaxChars(pDX, m_Npos1, 5);
	DDX_Text(pDX, IDC_NGAT2, m_Npos2);
	DDV_MaxChars(pDX, m_Npos2, 5);
	DDX_Text(pDX, IDC_NGAT3, m_Npos3);
	DDV_MaxChars(pDX, m_Npos3, 5);
	DDX_Text(pDX, IDC_NGAT4, m_Npos4);
	DDV_MaxChars(pDX, m_Npos4, 5);
	DDX_Text(pDX, IDC_NGAT5, m_Npos5);
	DDV_MaxChars(pDX, m_Npos5, 5);
	DDX_Text(pDX, IDC_NGAT6, m_Npos6);
	DDV_MaxChars(pDX, m_Npos6, 5);
	DDX_Text(pDX, IDC_NGAT7, m_Npos7);
	DDV_MaxChars(pDX, m_Npos7, 5);
	DDX_Text(pDX, IDC_NGAT8, m_Npos8);
	DDV_MaxChars(pDX, m_Npos8, 5);
	DDX_Text(pDX, IDC_NGAT9, m_Npos9);
	DDV_MaxChars(pDX, m_Npos9, 5);
	DDX_Text(pDX, IDC_NGAT10, m_Npos10);
	DDV_MaxChars(pDX, m_Npos10, 5);
	DDX_Text(pDX, IDC_ACTYPPARAM,m_ACParam);
	DDV_MaxChars(pDX, m_ACParam, 350);
	DDX_Text(pDX,IDC_ACGROUP,m_ACGroup);
	DDV_MaxChars(pDX,m_ACGroup,350);
	DDX_Check(pDX, IDC_RESTR_TO_1, m_Restr_To1);
	DDX_Check(pDX, IDC_RESTR_TO_2, m_Restr_To2);
	DDX_Check(pDX, IDC_RESTR_TO_3, m_Restr_To3);
	DDX_Check(pDX, IDC_RESTR_TO_4, m_Restr_To4);
	DDX_Check(pDX, IDC_RESTR_TO_5, m_Restr_To5);
	DDX_Check(pDX, IDC_RESTR_TO_6, m_Restr_To6);
	DDX_Check(pDX, IDC_RESTR_TO_7, m_Restr_To7);
	DDX_Check(pDX, IDC_RESTR_TO_8, m_Restr_To8);
	DDX_Check(pDX, IDC_RESTR_TO_9, m_Restr_To9);
	DDX_Check(pDX, IDC_RESTR_TO_10,m_Restr_To10);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatRuleDlg, CDialog)
	//{{AFX_MSG_MAP(GatRuleDlg)
	ON_BN_CLICKED(IDC_ALMATRIX1, OnALMatrix1)
	ON_BN_CLICKED(IDC_ALMATRIX2, OnALMatrix2)
	ON_BN_CLICKED(IDC_ALMATRIX3, OnALMatrix3)
	ON_BN_CLICKED(IDC_ALMATRIX4, OnALMatrix4)
	ON_BN_CLICKED(IDC_ALMATRIX5, OnALMatrix5)
	ON_BN_CLICKED(IDC_ALMATRIX6, OnALMatrix6)
	ON_BN_CLICKED(IDC_ALMATRIX7, OnALMatrix7)
	ON_BN_CLICKED(IDC_ALMATRIX8, OnALMatrix8)
	ON_BN_CLICKED(IDC_ALMATRIX9, OnALMatrix9)
	ON_BN_CLICKED(IDC_ALMATRIX10,OnALMatrix10)
	ON_BN_CLICKED(IDC_APMATRIX1, OnAPMatrix1)
	ON_BN_CLICKED(IDC_APMATRIX11, OnAPMatrix11)
	ON_BN_CLICKED(IDC_APMATRIX12, OnAPMatrix12)
	ON_BN_CLICKED(IDC_APMATRIX13, OnAPMatrix13)
	ON_BN_CLICKED(IDC_APMATRIX14, OnAPMatrix14)
	ON_BN_CLICKED(IDC_APMATRIX15, OnAPMatrix15)
	ON_BN_CLICKED(IDC_APMATRIX16, OnAPMatrix16)
	ON_BN_CLICKED(IDC_APMATRIX17, OnAPMatrix17)
	ON_BN_CLICKED(IDC_APMATRIX18, OnAPMatrix18)
	ON_BN_CLICKED(IDC_APMATRIX19,OnAPMatrix19)
	ON_BN_CLICKED(IDC_BUTTON1,OnACGroup)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg message handlers

BOOL GatRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1435), pomGat->Gnam, pomGat->Term);
	SetWindowText(olCaption);
	// TODO: Add extra initialization here

	

	bool blShowAL = true;
	bool blShowAP = true;
	CString omAlo = "GAT";
	CString olName = omAlo + omAlo;
	ALODATA *polAloc = ogAloData.GetAloByName(olName);
	if (!polAloc)
	{
		blShowAL = false;
		blShowAP = false;
	}

	CStringArray olGrpArray;
	olGrpArray.Add("ALT");
	olGrpArray.Add("APT");

	for (int i = 0; i < olGrpArray.GetSize(); i++)
	{
		CString omGrp = olGrpArray.GetAt(i);
		olName = omAlo + omGrp;
		ALODATA *polAloc = ogAloData.GetAloByName(olName);
		if (!polAloc)
		{
			if (omGrp == "ALT")
				blShowAL = false;
			if (omGrp == "APT")
				blShowAP = false;
		}
	}

	for (i = 0; i < olGrpArray.GetSize(); i++)
	{
		CString omGrp = olGrpArray.GetAt(i);
		olName = omGrp + omGrp;
		polAloc = ogAloData.GetAloByName(olName);
		if (!polAloc)
		{
			if (omGrp == "ALT")
				blShowAL = false;
			if (omGrp == "APT")
				blShowAP = false;
		}
	}

//		blShowAL = true;
//		blShowAP = true;

	if(blShowAL)
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_STATIC_REMOVE);
		CRect olRect;
		if (polWnd)
			polWnd->GetWindowRect( olRect );
		ScreenToClient(olRect);

		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX1);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX2);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX3);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX4);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX5);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX6);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX7);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX8);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX9);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX10);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
	}

	if (blShowAP)
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_APMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX11);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX12);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX13);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX14);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX15);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX16);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX17);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX18);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX19);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_STATIC_REMOVE);
		CRect olRect;
		if (polWnd)
			polWnd->GetWindowRect( olRect );
		ScreenToClient(olRect);

		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX1);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX11);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX12);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX13);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX14);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX15);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX16);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX17);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX18);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX19);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
	}

	if (!blShowAP && !blShowAL)
	{
		int ilNewTop = 0;
		CButton* polWnd = (CButton*) GetDlgItem(IDC_STATIC_REMOVE);
		CRect olRect;
		if (polWnd)
			polWnd->GetWindowRect( olRect );
		ScreenToClient(olRect);

		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT1);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT2);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT3);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT4);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT5);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT6);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT7);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT8);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT9);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_NEXT10);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}

		polWnd = (CButton*) GetDlgItem(IDC_NGAT1);
		if (polWnd)
		{
			CRect olRect1;
			polWnd->GetWindowRect(&olRect1);
			ScreenToClient(olRect1);
			ilNewTop = olRect1.top;
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT2);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT3);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT4);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT5);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT6);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT7);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT8);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT9);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}
		polWnd = (CButton*) GetDlgItem(IDC_NGAT10);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->SetWindowPos( NULL, 0, 0, 0, 0,  SWP_NOZORDER );
			polWnd->MoveWindow(olRect);
		}

		CRect olRect1;
		polWnd = (CButton*) GetDlgItem(IDOK);
		if (polWnd)
		{
			polWnd->GetWindowRect(&olRect1);
			ScreenToClient(olRect1);
			polWnd->SetWindowPos( NULL, olRect1.left, ilNewTop, olRect1.Width(), olRect1.Height(),  SWP_SHOWWINDOW );
		}
		polWnd = (CButton*) GetDlgItem(IDCANCEL);
		if (polWnd)
		{
			polWnd->GetWindowRect(&olRect1);
			ScreenToClient(olRect1);
			polWnd->SetWindowPos( NULL, olRect1.left, ilNewTop, olRect1.Width(), olRect1.Height(),  SWP_SHOWWINDOW );
		}

		this->GetWindowRect(&olRect);
		SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height()/2+60, SWP_SHOWWINDOW);// | SWP_NOMOVE );


	}

	if(!IsAdjacentGatRestriction)
	{
		CButton* polWnd;
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_1);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_2);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_3);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_4);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_5);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_6);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_7);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_8);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_9);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_RESTR_TO_10);		if (polWnd)	polWnd->ShowWindow(SW_HIDE);


	}





	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatRuleDlg::OnOK() 
{

	CString olValues;
	CString olTmp;
	CString olStr;
	CString olMsg;
	CString olTmpTxt;
	m_MaxPCtrl.GetWindowText(olTmpTxt);
	
	int ilLength = olTmpTxt.GetLength();
	bool blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;
	for (int illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
		m_MaxPCtrl.SetFocus();
		return;
	}

	m_PrioCtrl.GetWindowText(olTmpTxt);

	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 1 || ilLength == 0)
		blIsValid = false;
	for (illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1443), GetString(IDS_STRING117), MB_OK);
		m_PrioCtrl.SetFocus();
		return;
	}

	
	m_SequenceCtrl.GetWindowText(olTmpTxt);
	
	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;

	int ilfsc = atoi(olTmpTxt);
	if(ilfsc < 1 || ilfsc > 999)
	{
		blIsValid = false;
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1467), GetString(IDS_STRING117), MB_OK);
		m_SequenceCtrl.SetFocus();
		return;
	}


	
	m_MultCtrl.GetWindowText(olTmpTxt);

	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 2 || ilLength == 0)
		blIsValid = false;
	for (illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1442), GetString(IDS_STRING117), MB_OK);
		m_MultCtrl.SetFocus();
		return;
	}
	
	

	UpdateData(TRUE);

	olValues+=m_Flti+";";
	//m_Flti = olArray[0];//_T(""); Flti for arrival
	olStr.Format("%d", m_Maxp);
	olValues+=olStr+";";
	//m_Maxp = atoi(olArray[1]);//0;
	olStr.Format("%d", m_Mult);
	olValues+=olStr+";";
	//m_Mult = atoi(olArray[2]);//0;
	if(m_Inbo == TRUE)
	{
		olValues+= CString("X") + ";";
		//m_Inbo = TRUE;//FALSE;
	}
	else
	{
		olValues+= CString(" ") + ";";
	}
	if(m_Outb == TRUE)
	{
		olValues+= CString("X") + ";";
		//m_Outb = TRUE;//FALSE;
	}
	else
	{
		olValues+= CString(" ") + ";";
	}

	olStr.Format("%d", m_Prio);
	olValues+=olStr+";";
//	m_Prio = atoi(olArray[5]);//0;
	olValues+=m_Natr+";";
//	m_Natr = olArray[6];//_T("");
	CStringArray olArray;
	ExtractItemList(m_Pral, &olArray, ' ');


	CString olTmp2;	
/*	
	for(int i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olTmp2.GetBuffer(0));
			if(prlAlt == NULL)
			{
				prlAlt = ogAltData.GetAltByAlc3(olTmp2.GetBuffer(0));
				if(prlAlt == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1430), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}
*/

	for(int i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
						
			olTmp2.Remove('!');
			olTmp2.Remove('=');

			int ilColonPos =  olTmp2.Find(":");
			int ilStarPos =  olTmp2.Find("*");


			CString olAirline;
			CString olAirport;
			CString olALGroup;

			if( ilColonPos >= 0)
			{
				olAirport = olTmp2.Right( olTmp2.GetLength() - ilColonPos -1  );

				if( ilStarPos >= 0)
				{
					olALGroup = olTmp2.Left(ilColonPos);
					olALGroup.Remove('*');
				}
				else
				{
					olAirline = olTmp2.Left(ilColonPos);

				}
			}
			else
			{

				if( ilStarPos >= 0)
				{
					olAirline = "";
					olALGroup = olTmp2;
					olALGroup.Remove('*');
				}
				else
				{
					olAirline = olTmp2;
				}
			}


			if( !olAirline.IsEmpty())
			{

				ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olAirline.GetBuffer(0));
				if(prlAlt == NULL)
				{
					prlAlt = ogAltData.GetAltByAlc3(olAirline.GetBuffer(0));
					if(prlAlt == NULL)
					{
						olTmp.Format(GetString(IDS_STRING1430), olAirline);
						olMsg+=olTmp;
					}
				}
			}


			if( !olAirport.IsEmpty())
			{

				if( ogAptData.GetUrnoByApc( olAirport.GetBuffer(0)) == 0)
				{
					olTmp.Format(GetString(IDS_STRING1456), olAirport);
					olMsg+=olTmp;
				}
			}


			if( !olALGroup.IsEmpty())
			{

				if( ogGrnData.GetUrnoByName(olALGroup.GetBuffer(0), "ALTTAB", "POPS") == 0)
				{
					olTmp.Format(GetString(IDS_STRING1492), olALGroup);
					olMsg+=olTmp;
				}

			}


		}

	}




	olValues+=m_Pral+";";

	//servicetypes
	ExtractItemList(m_STYP_List, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 0)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
			CString olUrno;
			if(!ogBCD.GetField("STY", "STYP", olTmp2, "URNO", olUrno))
			{
				olTmp.Format(GetString(IDS_STRING1463), olTmp2);
				olMsg+=olTmp;
			}
		}
	}


	// Airports

	olArray.RemoveAll();

	ExtractItemList(m_PDest, &olArray, ' ');

	olTmp2 = "";	
	
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			olTmp2.Remove('!');
			olTmp2.Remove('=');

			int ilStarPos =  olTmp2.Find("*");
			CString olAirport;
			CString olAPGroup;


			if( ilStarPos >= 0)
			{
				olAirport = "";
				olAPGroup = olTmp2;
				olAPGroup.Remove('*');
			}
			else
			{
				olAirport = olTmp2;
			}


			if( !olAirport.IsEmpty())
			{

				if( ogAptData.GetUrnoByApc( olAirport.GetBuffer(0)) == 0)
				{
					olTmp.Format(GetString(IDS_STRING1456), olAirport);
					olMsg+=olTmp;
				}
			}


			if( !olAPGroup.IsEmpty())
			{

				if( ogGrnData.GetUrnoByName(olAPGroup.GetBuffer(0), "APTTAB", "POPS") == 0)
				{
					olTmp.Format(GetString(IDS_STRING1497), olAPGroup);
					olMsg+=olTmp;
				}

			}


		}

	}


	
	
	
	
	
	
	
	
	olValues+=m_PDest+";";

	olValues+=m_FltiDep+";"; //[8] Flti for departure

	//sequence
	char bufferTmp[128];
	itoa(m_sequence, bufferTmp, 10);
	CString olStrSequ = CString(bufferTmp);
	olValues += olStrSequ+";";

	//servicetypes
	olValues += m_STYP_List+";";

//	next gates
	if(	!m_Npos1.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos1.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos1.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos2.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos2.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos2.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos3.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos3.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos3.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos4.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos4.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos4.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos5.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos5.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos5.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos6.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos6.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos6.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos7.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos7.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos7.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos8.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos8.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos8.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos9.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos9.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos9.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	if(	!m_Npos10.IsEmpty())
	{
		GATDATA *prlPst = ogGatData.GetGatByGnam(m_Npos10.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1485), m_Npos10.GetBuffer(0));
			olMsg+=olTmp;
		}
	}

	olValues += m_Npos1+";";
	olValues += m_Npos2+";";
	olValues += m_Npos3+";";
	olValues += m_Npos4+";";
	olValues += m_Npos5+";";
	olValues += m_Npos6+";";
	olValues += m_Npos7+";";
	olValues += m_Npos8+";";
	olValues += m_Npos9+";";
	olValues += m_Npos10+";";

	//AC Parameter 
	ExtractItemList(m_ACParam, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
		
			ACTDATA *prlAct = ogActData.GetActByAct3(olTmp2.GetBuffer(0));
			if(prlAct == NULL)
			{
				prlAct = ogActData.GetActByAct5(olTmp2.GetBuffer(0));
				if(prlAct == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1431), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}

	olValues += m_ACParam+";";

	//AC Group
	bool ol_GroupFound = FALSE;
	if(!m_ACGroup.IsEmpty())
	{
		CCSPtrArray<GRNDATA> olGrnList;
		char pclTableName[16] = "ACTTAB";
		ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
		int ilCount = olGrnList.GetSize();

		CStringArray olArray;
		ExtractItemList(m_ACGroup, &olArray, ' ');
		CString olTmp2;	
		for(i = 0; i < olArray.GetSize(); i++)
		{
			olTmp2 = olArray[i].GetBuffer(0);

			if(olTmp2.GetLength() > 1)
			{
				if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
				
				for(int i = 0; i < ilCount; i++)
				{
					if(olGrnList[i].Grpn == olTmp2)
					ol_GroupFound = TRUE;
				}
				if(!ol_GroupFound)
				{
					olTmp.Format(GetString(IDS_GROUP_ERROR), olTmp2);
					olMsg+=olTmp;
				}

			}
			ol_GroupFound = FALSE;
		}
	}

	olValues += m_ACGroup+";";

	if(IsAdjacentGatRestriction)
	{
		if(m_Restr_To1)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To2)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To3)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To4)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To5)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To6)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To7)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To8)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To9)	olValues += "0;";	else	olValues += "-1;";
		if(m_Restr_To10)	olValues += "0;";	else	olValues += "-1;";

	}	


	if(olMsg.IsEmpty())
	{

		if (olValues.GetLength() > 2000)
		{
			MessageBox(GetString(IDS_STRING1468), GetString(IDS_STRING117), MB_OK);
			return;
		}

//		strcpy(pomGat->Resb, olValues.GetBuffer(0));
//		strcpy(pomGat->Gatr, olValues.GetBuffer(0));
		strncpy(pomGat->Gatr, olValues.GetBuffer(0), 2000);
		pomGat->IsChanged = DATA_CHANGED;
		ogGatData.SaveGAT( pomGat);
		CDialog::OnOK();
	}
	else
	{
		MessageBox(olMsg, GetString(IDS_STRING117), MB_OK);
	}
}

void GatRuleDlg::OnACGroup()
{

	CAirCraftGroupSelDlg dlg(this, CString("ACTTAB"));
	//dlg.DoModal();
	if (dlg.DoModal() == IDOK)
	{
		m_ACGroup = dlg.omGrpStr;
		UpdateData(FALSE);
	}
}

void GatRuleDlg::OnALMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos1,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix2() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos2,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix3() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos3,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix4() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos4,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix5() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos5,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix6() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos6,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix7() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos7,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix8() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos8,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix9() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos9,"GAT","ALT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnALMatrix10() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos10,"GAT","ALT",this);
	olDlg.DoModal();
}

//##
void GatRuleDlg::OnAPMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos1,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix11() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos2,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix12() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos3,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix13() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos4,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix14() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos5,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix15() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos6,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix16() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos7,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix17() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos8,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix18() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos9,"GAT","APT",this);
	olDlg.DoModal();
}

void GatRuleDlg::OnAPMatrix19() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomGat->Gnam,m_Npos10,"GAT","APT",this);
	olDlg.DoModal();
}
