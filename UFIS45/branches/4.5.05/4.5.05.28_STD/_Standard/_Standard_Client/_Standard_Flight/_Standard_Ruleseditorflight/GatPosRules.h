#if !defined(AFX_GATPOSRULES_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATPOSRULES_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosRules.h : header file
//
//#include "GxGridTab.h"
#include "CcsTable.h"
#include "GatRulesViewer.h"
#include "PosRulesViewer.h"
/////////////////////////////////////////////////////////////////////////////
// GatPosRules dialog


class GatPosRules : public CPropertyPage
{
	DECLARE_DYNCREATE(GatPosRules)

// Construction
public:
	GatPosRules();
	~GatPosRules();

// Dialog Data
	//{{AFX_DATA(GatPosRules)
	enum { IDD = IDD_GR_RULES1 };
	CStatic	m_GateListTable;
	CStatic	m_PosListTable;
	//}}AFX_DATA

	CCSTable *pomPosList;
	CCSTable *pomGatList;

	GatRulesViewer omGatRulesViewer;
	PosRulesViewer omPosRulesViewer;

	CUIntArray omPstUrnos;
	CUIntArray omGatUrnos;

	void MakePosLineData(PSTDATA *popPst, CStringArray &ropArray);
	void MakeGatLineData(GATDATA *popGat, CStringArray &ropArray);
	void GatPosRules::UpdateView();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GatPosRules)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	int omGatContextItem;
	int omPosContextItem;
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GatPosRules)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnGridDblClk(UINT wParam, LONG lParam);
	void OnPrintPosition();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSRULES_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
