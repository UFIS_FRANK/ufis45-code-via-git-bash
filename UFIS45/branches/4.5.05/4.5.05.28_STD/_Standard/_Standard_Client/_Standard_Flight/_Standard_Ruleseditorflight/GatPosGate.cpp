// GatPosGate.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "CedaGatData.h"
#include "GatPosGate.h"
#include "BasicData.h"
#include "GatRuleDlg.h"
#include "DutyPreferences.h"
#include <iomanip.h>
#include <fstream.h>
#include <iostream.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DutyPreferences *pogDutyPreferences;


/////////////////////////////////////////////////////////////////////////////
// GatPosGate property page

IMPLEMENT_DYNCREATE(GatPosGate, CPropertyPage)


static int CompareGnam(const GATDATA **e1, const GATDATA **e2)
{
	return strcmp((**e1).Gnam, (**e2).Gnam);
}

GatPosGate::GatPosGate() : CPropertyPage(GatPosGate::IDD)
{
	//{{AFX_DATA_INIT(GatPosGate)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    pomGatList = new CCSTable;
}

GatPosGate::~GatPosGate()
{
	delete pomGatList;
}

void GatPosGate::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosGate)
	DDX_Control(pDX, IDC_GATE1_LIST, m_GateListTable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosGate, CPropertyPage)
	//{{AFX_MSG_MAP(GatPosGate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnGridDblClk)
	ON_WM_SIZE()
	ON_MESSAGE(CLK_BUTTON_PRINT,OnPrintGates)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosRules message handlers

BOOL GatPosGate::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	int i=0;
	CRect olRect;

	CStringArray olGatHeader;

	ogGatData.omData.Sort(CompareGnam);
	for(i = 0; i < ogGatData.omData.GetSize(); i++)
	{
		omGatUrnos.Add(ogGatData.omData[i].Urno);
	}

	m_GateListTable.GetClientRect(olRect);
	
	pomGatList->SetShowSelection(FALSE);
	pomGatList->SetTableData(this, olRect.left + 13, olRect.right + 13, olRect.top +13, olRect.bottom + 13);

	pomGatList->SetShowSelection(FALSE);
	CString olViewName = "<Default>";

	omGatRulesViewer.Attach(pomGatList);
	omGatRulesViewer.ChangeViewTo(olViewName);

	UpdateView();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

 void GatPosGate::OnSize(UINT nType, int cx, int cy) 
{
	 return;//didn#t work ok
}

void GatPosGate::UpdateView()
{
	CString olViewName = "<Default>";
//	int ilPosH = pomGatList->pomListBox->GetScrollPos(SB_HORZ);
	int ilGatLine = pomGatList->pomListBox->GetTopIndex();
	
	AfxGetApp()->DoWaitCursor(1);
    omGatRulesViewer.ChangeViewTo(olViewName);
	pomGatList->pomListBox->SetTopIndex(ilGatLine);

//    pomGatList->pomListBox->SetScrollPos(SB_HORZ, ilPosH, TRUE);

	AfxGetApp()->DoWaitCursor(-1);
}


void GatPosGate::MakeGatLineData(GATDATA *popGat, CStringArray &ropArray)
{
	CStringArray olResbList;
	CString olTmpGatr = popGat->Gatr;
	if(olTmpGatr.Replace(";","#") < 3)
	{
		olTmpGatr =popGat->Gatr;
		olTmpGatr.Replace("/",";");
		strcpy(popGat->Gatr, olTmpGatr.GetBuffer(0));
	}
	

	ExtractItemList(popGat->Gatr, &olResbList, ';');
	for(int i = 0; i < olResbList.GetSize(); i++)
	{
		ropArray.Add(olResbList[i]);
	}
}
LONG GatPosGate::OnGridDblClk(UINT wParam, LONG lParam)
{

	int ilCol = HIWORD(wParam);
	int ilRow = LOWORD(wParam);
		
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lParam;

	if(polNotify->SourceTable == pomGatList)
	{
		if(ilRow >= 0 && ilRow < omGatUrnos.GetSize())
		{
			long llUrno = (long)omGatUrnos[ilRow];
			GATDATA *polGat = ogGatData.GetGATByUrno(llUrno);
			if(polGat != NULL)
			{
				GatRuleDlg olDlg(this, polGat);
				if(olDlg.DoModal() == IDOK)
				{
					UpdateView();
				}
			}
		}
	}

	return 0L;
}

BOOL GatPosGate::OnKillActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	
	

	return CPropertyPage::OnKillActive();
}

BOOL GatPosGate::OnSetActive() 
{

	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	

	return CPropertyPage::OnSetActive();
}

void GatPosGate::OnPrintGates()
{
	char pclExcelPath[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("FIPS", "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		AfxMessageBox("Unable to Find MSExcel");

	
	ofstream of;
	CString omTableName = "GateRules";
	CString olFileName = omTableName;
	CString opTrenner = ",";

	CString HeadColu1 = "No.";
	CString HeadColu2 = "Gate";
	CString HeadColu3 = "T";
	CString HeadColu4 = "Id-Arr";
	CString HeadColu5 = "Id-Dep";
	CString HeadColu6 = "Max";
	CString HeadColu7 = "M";
	CString HeadColu8 = "I";
	CString HeadColu9 = "O";
	CString HeadColu10 = "P";
	CString HeadColu11 = "Nat";
	CString HeadColu12 = "A/L Parameter";
	CString HeadColu13 = "Best. Parameter";
	CString HeadColu14 = "Sequence";
	CString HeadColu15 = "Service Types";
	CString HeadColu16 = "Next Gates"; 

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;
	
	of  << setw(ilwidth) << HeadColu1
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu2
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu4
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu5
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu6
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu7
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu8
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu9
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu10
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu11
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu12
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu13
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu14
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu15
		<< setw(1) << opTrenner;

		if(omGatRulesViewer.bmNextGate == TRUE)
		{
			of << setw(ilwidth) << HeadColu15 ;
		}

		of << endl;
	
	int ilCount = omGatRulesViewer.omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		GATTABLE_LINEDATA *polPst = &omGatRulesViewer.omLines.GetAt(i);
		if(polPst != NULL)
		{
			CString GateNo ;
			GateNo.Format(_T("%d"),polPst->Rtnr);
			of.setf(ios::left, ios::adjustfield);
			of  << setw(1) << GateNo
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Gate
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->T
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Id
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->IdDEP
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Max
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->M
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->In
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Out
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Prio
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Nat
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->PAL
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->PDest
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Sequ
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Service;
					

				if(omGatRulesViewer.bmNextGate == TRUE)
				{
					of << setw(1) << opTrenner 
						<< setw(1) << polPst->NextGates;
				}
				
							
			of << endl;
				 
		}
	
	}
	of.close();

	char pclTmp[256] ; 
	omFileName.TrimLeft();
	omFileName.TrimRight();
	strcpy(pclTmp ,omFileName);
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		AfxMessageBox("The EXCEL path in Ceda.ini is not proper.Please correct it");

}
