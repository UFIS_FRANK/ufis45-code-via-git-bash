// BltTableViewer.cpp: Implementierung der Klasse BltTableViewer.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CedaBltData.h"
#include "resource.h"
#include "resrc1.h"

#include "BltRulesViewer.h"
#include <math.h>
#include "Rules.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int width = 574;

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

BltRulesViewer::BltRulesViewer()
{
	bmNoUpdatesNow = false;
	

}

BltRulesViewer::~BltRulesViewer()
{
	DeleteAll();
}

void BltRulesViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

#pragma warning( disable : 4100 )  
void BltRulesViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}
 

int BltRulesViewer::CompareBlt(BLTTABLE_LINEDATA *prpBlt1, BLTTABLE_LINEDATA *prpBlt2)
{


     return 0;
}
#pragma warning( default : 4100 )  

/////////////////////////////////////////////////////////////////////////////
// BltRulesViewer -- code specific to this class


void BltRulesViewer::MakeLines()
{
	int ilBltCount = ogBltData.omData.GetSize();
	for(int illc = 0 ; illc < ilBltCount; illc++)
	{		
		BLTDATA * prlBlt = &ogBltData.omData[illc];
		MakeLine(prlBlt);
		
	}
}


void BltRulesViewer::MakeBltLineData(BLTDATA *popBlt, CStringArray &ropArray)
{
	CStringArray olResbList;
	CString olTmpBltr;
	olTmpBltr = popBlt->Bltr;
	if(olTmpBltr.Replace(";","#") < 3)
	{
		olTmpBltr =popBlt->Bltr;
		olTmpBltr.Replace("/",";");
		strcpy(popBlt->Bltr, olTmpBltr.GetBuffer(0));
	}
	
	ExtractItemList(popBlt->Bltr, &olResbList, ';');
	for(int i = 0; i < olResbList.GetSize(); i++)
	{
		ropArray.Add(olResbList[i]);
	}
}

int BltRulesViewer::MakeLine(BLTDATA *prpBlt)
{
/*
	int Rtnr;
	CString Belt;
	CString T;
	CString	Prio;
	CString	Sequ;
	CString	Pax;
	CString	Nat;
	CString	Alc;
	CString	Org;
	CString	Service;
*/

    // Update viewer data for this shift record
   BLTTABLE_LINEDATA rlLine;

	rlLine.Rtnr = omLines.GetSize() +1;
	CStringArray olAcgrList;
	MakeBltLineData(prpBlt, olAcgrList);
	int ilWordCount = olAcgrList.GetSize();

	CString olT;
	int ilActIdx = 0;

	//bnam
	rlLine.Belt = prpBlt->Bnam;
	
	//term
	rlLine.T = prpBlt->Term;
	
/*	//Id
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Id = olT;
		ilActIdx++;
	}
*/
	//prio
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Prio = olT;
		ilActIdx++;
	}
	
	//sequ
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Sequ= olT;
		ilActIdx++;
	}

	//PAX
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];

		if( ( (CRulesApp *)AfxGetApp() )->CanHandleBeltBag() == TRUE)
		{
		}

		rlLine.Pax= olT;
		ilActIdx++;
	}

	//nat
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Nat= olT;
		ilActIdx++;
	}

	//alc
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Alc= olT;
		ilActIdx++;
	}

	//org
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Org = olT;
		ilActIdx++;
	}

	//servicetypes
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Service= olT;
		ilActIdx++;
	}
//	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == TRUE)
	{

		//flight id
		//if(ilWordCount == 8)
		if(ilWordCount >ilActIdx)
		{
			olT = olAcgrList[ilActIdx];
			rlLine.Flti= olT;
			ilActIdx++;
		}
	}

	//AirCraft Param
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.ACParam= olT;
		ilActIdx++;
	}
	else
	{
		rlLine.ACParam = _T("");
		ilActIdx++;
	}
		//AirCraft Group
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.ACGroup= olT;
		ilActIdx++;
	}
	else
	{
		rlLine.ACGroup= _T("");
		ilActIdx++;
	}
	
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBeltBag() == TRUE)
	{
		//bag
		if(ilWordCount >ilActIdx)
		{
			olT = olAcgrList[ilActIdx];
			rlLine.Bag= olT;
			ilActIdx++;

			if(!rlLine.Bag.IsEmpty())
			{
				rlLine.Pax += "/" + rlLine.Bag;

			}
		}
	}
	
	return(CreateLine(&rlLine));
}


int BltRulesViewer::CreateLine(BLTTABLE_LINEDATA *prpBlt)
{
    int ilLineCount = omLines.GetSize();

	BLTTABLE_LINEDATA rlBlt;
	rlBlt = *prpBlt;
    omLines.NewAt(ilLineCount, rlBlt);

    return ilLineCount;
}

void BltRulesViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


BOOL BltRulesViewer::FindLine(long lpRtnr, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
      if (omLines[rilLineno].Rtnr == lpRtnr)
            return TRUE;
	}
    return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// BltRulesViewer - GATTABLE_LINEDATA array maintenance

void BltRulesViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// BltRulesViewer - display drawing routine


void BltRulesViewer::MakeHeaderData()
{
/*
	int Rtnr;
	CString Belt;
	CString T;
	CString	Prio;
	CString	Sequ;
	CString	Pax;
	CString	Nat;
	CString	Alc;
	CString	Org;
	CString	Service;
*/

	TABLE_HEADER_COLUMN rlHeader;
//	int ilColumnNo = -1;

	CStringArray olAcgrList;
	ExtractItemList(LoadStg(IDS_STRING1475), &olAcgrList, ',');
	
	CString olT;
	int ilActIdx = 0;
	int ilWordCount = olAcgrList.GetSize();
	

	//rtnr
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 8 * 3; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.SeparatorType = SEPA_NORMAL;//SEPA_NONE;//SEPA_LIKEVERTICAL
	rlHeader.Text = "";
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//belt
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//term
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//prio
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//sequ
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//pax
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;

		if( ( (CRulesApp *)AfxGetApp() )->CanHandleBeltBag() == TRUE)
		{
			rlHeader.Length = 8 * 7; 
			rlHeader.Text += "/Bag";
		}
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//nat
	rlHeader.Length = 8 * 20; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//alc
	rlHeader.Length = 8 * 20; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//org
	rlHeader.Length = 8 * 20; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//serv
	rlHeader.Length = 8 * 16; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	
	//AirCraft Type Param
	rlHeader.Length = 8 * 20; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//AirCraft Group
	rlHeader.Length = 8 * 20; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	

	//flight id
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == FALSE)
	{
		rlHeader.Length = 8 * 0; 
		rlHeader.Text = "";
		ilActIdx++;
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	}
	else
	{
		rlHeader.Length = 8 * 6; 
		rlHeader.Text = "";
		if(ilWordCount >ilActIdx)
		{
			olT = olAcgrList[ilActIdx];
			rlHeader.Text = olT;
			ilActIdx++;
		}
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();

}



void BltRulesViewer::MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,BLTTABLE_LINEDATA *prpLine)
{

//	BOOL blNewLogicLine = FALSE;

	int ilColumnNo = 0;
	bool bldefekt = false;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Lineno = imTotalLines;//*3)+ilLocalLine;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.SeparatorType = SEPA_NORMAL;
	rlColumnData.Font = &ogCourier_Regular_8;//&ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
	rlColumnData.Text.Format("%d",prpLine->Rtnr);  
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


	rlColumnData.Text = prpLine->Belt;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->T;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
/*
	rlColumnData.Text = prpLine->Id;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
*/
	rlColumnData.Text = prpLine->Prio;
	if (rlColumnData.Text == "")
		rlColumnData.Text = "0";
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Sequ;
	if (rlColumnData.Text == "")
		rlColumnData.Text = "1";
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Pax;
	if (rlColumnData.Text == "")
		rlColumnData.Text = "0";
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Nat;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Alc;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Org;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Service;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	//AC Param
	rlColumnData.Text = prpLine->ACParam;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	//AC Group
	rlColumnData.Text = prpLine->ACGroup;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	//flight id
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == FALSE)
	{
		rlColumnData.Text = "";
		rlColumnData.Columnno = ilColumnNo++;
	}
	else
	{
		rlColumnData.Text = prpLine->Flti;
		rlColumnData.Columnno = ilColumnNo++;
		ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	}
	
}



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void BltRulesViewer::UpdateDisplay()
{

	imTotalLines = 0;
//	BOOL blNewLogicLine = FALSE;


	pomTable->SetShowSelection(FALSE);
	pomTable->ResetContent();
	//pomTable->SetDockingRange(3);
	
	MakeHeaderData();
	pomTable->SetDefaultSeparator();
//	pomTable->SetSelectMode(0);

	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		MakeColumnData(olColList,&omLines[ilLc]);
		imTotalLines++;
		pomTable->AddTextLine(olColList, (void*)&(omLines[ilLc]));
		olColList.DeleteAll();
	}
    pomTable->DisplayTable();
}




