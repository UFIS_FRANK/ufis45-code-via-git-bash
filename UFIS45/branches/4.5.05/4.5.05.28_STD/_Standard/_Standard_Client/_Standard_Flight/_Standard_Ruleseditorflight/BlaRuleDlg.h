#if !defined(AFX_BLARULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_)
#define AFX_BLARULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BlaRuleDlg.h : header file
//
#include "resrc1.h"
#include "resource.h"
#include "CedaBlaData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// BlaRuleDlg dialog

class BlaRuleDlg : public CDialog
{
// Construction
public:
	BlaRuleDlg(CWnd* pParent, BLADATA *popBla);   // standard constructor
	BlaRuleDlg();
	bool GetBlaData(BLADATA *popBla);

// Dialog Data
	//{{AFX_DATA(BlaRuleDlg)
	enum { IDD = IDD_BLA_RULE_DLG };
	CCSEdit	m_FtypCtrl;
	CCSEdit	m_AgrpCtrl;
	CCSEdit	m_OgrpCtrl;
	CCSEdit m_TimeCtrl;
	CString m_Ftyp;
	CString m_Agrp;
	CString m_Ogrp;
	int		m_Time;
	//}}AFX_DATA

	BLADATA *pomBla;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BlaRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:


	// Generated message map functions
	//{{AFX_MSG(BlaRuleDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	void OnACGroup();
	void OnOriGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		DECLARE_SERIAL(BlaRuleDlg)
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLARULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_)
