#if !defined(AFX_FIDSREMARKDLG_H__9F1B4699_D39D_49AA_9737_68EC755ED097__INCLUDED_)
#define AFX_FIDSREMARKDLG_H__9F1B4699_D39D_49AA_9737_68EC755ED097__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsRemarkDlg.h : header file
//
#include "resrc1.h"
/////////////////////////////////////////////////////////////////////////////
// CFidsRemarkDlg dialog

class CFidsRemarkDlg : public CDialog
{
// Construction
public:
	CFidsRemarkDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_Remarks;	
// Dialog Data
	//{{AFX_DATA(CFidsRemarkDlg)
	enum { IDD = IDD_FIDS_REMARK };
		//CListBox m_CL_List;
	CListCtrl m_CL_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidsRemarkDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidsRemarkDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSREMARKDLG_H__9F1B4699_D39D_49AA_9737_68EC755ED097__INCLUDED_)
