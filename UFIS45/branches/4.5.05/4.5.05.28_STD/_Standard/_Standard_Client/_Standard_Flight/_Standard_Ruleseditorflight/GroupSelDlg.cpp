// GroupSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "GroupSelDlg.h"
#include "GroupNamesPage.h"
#include "PstRuleDlg.h"
#include "BltRuleDlg.h"
#include "GatRuleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGroupSelDlg dialog


CGroupSelDlg::CGroupSelDlg(CWnd* pParent, CString& opGrpStr/*=NULL*/)
	: CDialog(CGroupSelDlg::IDD, pParent)
{
	omGrpStr = opGrpStr;
	//{{AFX_DATA_INIT(CGroupSelDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CGroupSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupSelDlg)
		DDX_Control(pDX, IDC_LIST1, m_GroupNameList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGroupSelDlg, CDialog)
	//{{AFX_MSG_MAP(CGroupSelDlg)
	ON_BN_CLICKED(IDOK,OnOk)
	ON_BN_CLICKED(IDCANCEL,OnCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupSelDlg message handlers

BOOL CGroupSelDlg::OnInitDialog() 
{
	CedaGrnData groupPage;
	CString str ;
	CDialog::OnInitDialog();
	
	char pclWhere[100]="";
	CCSPtrArray<GRNDATA> olGrnList;
	char pclTableName[16];
	strcpy(pclTableName,omGrpStr);
	ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
	int ilCount = olGrnList.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		m_GroupNameList.AddString(olGrnList[i].Grpn);
	}
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CGroupSelDlg::OnOk()
{
	omGrpStr.Empty();
	int ilIndex = m_GroupNameList.GetCurSel();
	if (ilIndex != -1)
	{
		m_GroupNameList.GetText(ilIndex,omGrpStr);
	}
	CDialog::OnOK();

}

void CGroupSelDlg::OnCancel()
{

	CDialog::OnCancel();
}
