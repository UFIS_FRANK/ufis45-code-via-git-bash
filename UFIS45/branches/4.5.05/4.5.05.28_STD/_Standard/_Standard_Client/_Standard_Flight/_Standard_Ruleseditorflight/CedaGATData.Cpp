// CedaGATData.cpp
 
#include <stdafx.h>
#include <CedaGATData.h>

// Local function prototype
static void ProcessGATCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------


CedaGATData::CedaGATData()
{
    // Create an array of CEDARECINFO for GATDATA
	BEGIN_CEDARECINFO(GATDATA,GATDataRecInfo)
		FIELD_DATE		(Bnaf,"BNAF")
		FIELD_DATE		(Bnat,"BNAT")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Gnam,"GNAM")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Nobr,"NOBR")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rga1,"RGA1")
		FIELD_CHAR_TRIM	(Rga2,"RGA2")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Resb,"RESB")
		FIELD_CHAR_TRIM	(Rbab,"RBAB")
		FIELD_CHAR_TRIM	(Busg,"BUSG")
		FIELD_CHAR_TRIM	(Term, "TERM")
		FIELD_CHAR_TRIM	(Gatr, "GATR")
	END_CEDARECINFO //(GATDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(GATDataRecInfo)/sizeof(GATDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GATDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"GAT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmGATFieldList,"BNAF,BNAT,CDAT,GNAM,LSTU,NAFR,NATO,NOBR,PRFL,RGA1,RGA2,TELE,URNO,USEC,USEU,VAFR,VATO,RESN,RESB,RBAB,BUSG,TERM,GATR");
	pcmFieldList = pcmGATFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}


void CedaGATData::Register(void)
{
	ogDdx.Register((void *)this,BC_GAT_CHANGE,CString("GATDATA"), CString("GAT-changed"),ProcessGATCf);
	ogDdx.Register((void *)this,BC_GAT_DELETE,CString("GATDATA"), CString("GAT-deleted"),ProcessGATCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGATData::~CedaGATData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGATData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omGnamMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGATData::ReadAllGATs()
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omGnamMap.RemoveAll();
    omData.DeleteAll();
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GATDATA *prpGAT = new GATDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpGAT)) == true)
		{
			prpGAT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpGAT);//Update omData
			omUrnoMap.SetAt((void *)prpGAT->Urno,prpGAT);
			omGnamMap.SetAt(prpGAT->Gnam,prpGAT);
		}
		else
		{
			delete prpGAT;
		}
	}

	//Convert();
    
	return true;
}

void CedaGATData::SaveAll(void)
{
	GATDATA *prlGAT;

	for( int i = 0; i < omData.GetSize(); i++)
	{
		prlGAT = &omData[i];

		UpdateGAT(prlGAT, false);
	}
}

long CedaGATData::GetUrnoByGat(char *pspPst)
{
	GATDATA  *prlPST;
	if(strcmp(pspPst, "") == 0)
	{
		return 0;
	}

	if (omGnamMap.Lookup((LPCSTR)pspPst,(void *&)prlPST) == TRUE)
	{
		return prlPST->Urno;
	}
	return 0;
}

GATDATA * CedaGATData::GetGatByGnam(char *pspGnam)
{
	GATDATA  *prlGAT;
	if (omGnamMap.Lookup(pspGnam,(void *& )prlGAT) == TRUE)
	{
		return prlGAT;
	}
	return NULL;
}

void CedaGATData::Convert()
{
	GATDATA *prlGAT;
	CString olPosr;
	CString olDesList;
	CString olNatList;
	CString olAlcList;

	CStringArray olArray;
	CStringArray olTmpArray;


	for( int i = 0; i < omData.GetSize(); i++)
	{
		prlGAT = &omData[i];
		
		olPosr = CString(prlGAT->Gatr);

		ExtractItemList(olPosr, &olArray, ';');

		if(olArray.GetSize() > 8)
		{

			olDesList = olArray[8];
			olNatList = olArray[6];
			olAlcList = olArray[7];

			if(olDesList.Find('=') < 0)
			{
				ExtractItemList(olDesList, &olTmpArray, ' ');
				olDesList = "";
				for( int j = 0; j < olTmpArray.GetSize(); j++)
				{
					olDesList += CString("=") + olTmpArray[j] + CString(" ");
				}
			}

			if(olNatList.Find('=') < 0)
			{
				ExtractItemList(olNatList, &olTmpArray, ' ');
				olNatList = "";
				for( int j = 0; j < olTmpArray.GetSize(); j++)
				{
					olNatList += CString("=") + olTmpArray[j] + CString(" ");
				}
			}
			if(olAlcList.Find('=') < 0)
			{

				ExtractItemList(olAlcList, &olTmpArray, ' ');
				olAlcList = "";
				for(  int j = 0; j < olTmpArray.GetSize(); j++)
				{
					olAlcList += CString("=") + olTmpArray[j] + CString(" ");
				}

			}
			olArray[8] = olDesList;
			olArray[6] = olNatList;
			olArray[7] = olAlcList;

			olPosr = "";
			for(  int j = 0; j < olArray.GetSize(); j++)
			{
				olPosr += olArray[j] + CString(";");
			}
			
			if(olPosr.GetLength() > 0)
			{
				olPosr = olPosr.Left( olPosr.GetLength() - 1);
			}


			strcpy(prlGAT->Gatr,olPosr);
		}

	}
}






//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGATData::InsertGAT(GATDATA *prpGAT,BOOL bpSendDdx)
{
	GATDATA *prlGAT = new GATDATA;
	memcpy(prlGAT,prpGAT,sizeof(GATDATA));
	prlGAT->IsChanged = DATA_NEW;
	SaveGAT(prlGAT); //Update Database
	InsertGATInternal(prlGAT);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaGATData::InsertGATInternal(GATDATA *prpGAT)
{
	//PrepareGATData(prpGAT);
	ogDdx.DataChanged((void *)this, GAT_CHANGE,(void *)prpGAT ); //Update Viewer
	omData.Add(prpGAT);//Update omData
	omUrnoMap.SetAt((void *)prpGAT->Urno,prpGAT);
	omGnamMap.SetAt(prpGAT->Gnam,prpGAT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGATData::DeleteGAT(long lpUrno)
{
	bool olRc = true;
	GATDATA *prlGAT = GetGATByUrno(lpUrno);
	if (prlGAT != NULL)
	{
		prlGAT->IsChanged = DATA_DELETED;
		olRc = SaveGAT(prlGAT); //Update Database
		DeleteGATInternal(prlGAT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGATData::DeleteGATInternal(GATDATA *prpGAT)
{
	ogDdx.DataChanged((void *)this,GAT_DELETE,(void *)prpGAT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGAT->Urno);
	omGnamMap.RemoveKey(prpGAT->Gnam);
	int ilGATCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGATCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGAT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaGATData::PrepareGATData(GATDATA *prpGAT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGATData::UpdateGAT(GATDATA *prpGAT,BOOL bpSendDdx)
{
	if (GetGATByUrno(prpGAT->Urno) != NULL)
	{
		if (prpGAT->IsChanged == DATA_UNCHANGED)
		{
			prpGAT->IsChanged = DATA_CHANGED;
		}
		SaveGAT(prpGAT); //Update Database
		UpdateGATInternal(prpGAT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGATData::UpdateGATInternal(GATDATA *prpGAT)
{
	GATDATA *prlGAT = GetGATByUrno(prpGAT->Urno);
	if (prlGAT != NULL)
	{
		omGnamMap.RemoveKey(prlGAT->Gnam);
		*prlGAT = *prpGAT; //Update omData
		omGnamMap.SetAt(prlGAT->Gnam,prlGAT);
		ogDdx.DataChanged((void *)this,GAT_CHANGE,(void *)prlGAT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GATDATA *CedaGATData::GetGATByUrno(long lpUrno)
{
	GATDATA  *prlGAT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGAT) == TRUE)
	{
		return prlGAT;
	}
	return NULL;
}

//--READSPECIALGATDATA-------------------------------------------------------------------------------------

bool CedaGATData::ReadSpecial(CCSPtrArray<GATDATA> &ropGat,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		if (CedaAction("RT") == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT",pspWhere) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GATDATA *prpGat = new GATDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpGat)) == true)
		{
			ropGat.Add(prpGat);
		}
		else
		{
			delete prpGat;
		}
	}
    return true;
}

//--EXIST-I------------------------------------------------------------------------------------------------

CString CedaGATData::GATExists(GATDATA *prpGAT)
{
	CString olField = "";
	GATDATA  *prlGAT;

	if (omGnamMap.Lookup((LPCSTR)prpGAT->Gnam,(void *&)prlGAT) == TRUE)
	{
		if(prlGAT->Urno != prpGAT->Urno)
		{
			olField += GetString(IDS_STRING195);
		}
	}

	return olField;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaGATData::ExistGnam(char* popGnam)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE GNAM='%s'",popGnam);
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pcmTableName,"GNAM",pclSelection,"","");
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGATData::SaveGAT(GATDATA *prpGAT)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[3000];

	if (prpGAT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpGAT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGAT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpGAT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGAT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGAT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpGAT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGAT->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpGAT->IsChanged = DATA_UNCHANGED;
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGATCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_GAT_CHANGE :
	case BC_GAT_DELETE :
		((CedaGATData *)popInstance)->ProcessGATBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGATData::ProcessGATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGATData;
	long llUrno;
	prlGATData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlGATData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlGATData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	GATDATA *prlGAT;
	if(ipDDXType == BC_GAT_CHANGE)
	{
		if((prlGAT = GetGATByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlGAT,prlGATData->Fields,prlGATData->Data);
			UpdateGATInternal(prlGAT);
		}
		else
		{
			prlGAT = new GATDATA;
			GetRecordFromItemList(prlGAT,prlGATData->Fields,prlGATData->Data);
			InsertGATInternal(prlGAT);
		}
	}
	if(ipDDXType == BC_GAT_DELETE)
	{
		prlGAT = GetGATByUrno(llUrno);
		if (prlGAT != NULL)
		{
			DeleteGATInternal(prlGAT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
