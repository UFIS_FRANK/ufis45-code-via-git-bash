// GhsList.cpp : implementation file
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "GhsList.h"
#include "DutyPreferences.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GhsList dialog

static int CompareByName(const GHSDATA **e1, const GHSDATA **e2)
{
	return (strcmp((**e1).Lknm, (**e2).Lknm));
}

static void ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

GhsList::GhsList(CWnd* pParent /*=NULL*/, int ipSelectMode, CString opCalledFrom, long *lpUrno)
	: CDialog(GhsList::IDD, pParent)
{
	plmGhsUrno = lpUrno;
	imSelectMode = ipSelectMode;
	omCalledFrom = opCalledFrom;
	prmSelectedGhs = NULL;
	m_nDialogBarHeight = 0;
	if(omCalledFrom != CString("DSR_EINSATZ_DLG"))
	{
		CDialog::Create(GhsList::IDD, pParent);
		ShowWindow(SW_SHOWNORMAL);
	}

	//LoadList();

	//{{AFX_DATA_INIT(GhsList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ogDdx.Register((void *)this,GHS_CHANGE,	CString("GHSDATA"), CString("Ghs-changed"),	ProcessGhsCf);
	ogDdx.Register((void *)this,GHS_NEW,		CString("GHSDATA"), CString("Ghs-new"),		ProcessGhsCf);
	ogDdx.Register((void *)this,GHS_DELETE,	CString("GHSDATA"), CString("Ghs-deleted"),	ProcessGhsCf);
}

GhsList::~GhsList()
{
	omLines.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}
static void ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    GhsList *polDlg = (GhsList *)vpInstance;
	polDlg->UpdateDisplay();
}

void GhsList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GhsList)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GhsList, CDialog)
	//{{AFX_MSG_MAP(GhsList)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GhsList message handlers

BOOL GhsList::OnInitDialog() 
{
	CDialog::OnInitDialog();
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
	CRect olParentRect;
	GetParent()->GetWindowRect(&olParentRect);
    rect.left = olParentRect.left;;
    rect.top = 180;
    rect.right = rect.left+400;//::GetSystemMetrics(SM_CXSCREEN) - 640;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 350;
    MoveWindow(&rect);
	HICON h_icon = AfxGetApp()->LoadIcon(IDI_UFIS);
	SetIcon(h_icon, FALSE);
	pomTable = new CTable;
	pomTable->SetSelectMode(imSelectMode );

   pomTable->tempFlag = 2;
	
	GetClientRect(&rect);
	rect.OffsetRect(0, m_nDialogBarHeight);
	
	//rect.bottom = rect.bottom - 300;
	rect.InflateRect(1, 1);     // hiding the CTable window border
   pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

 	UpdateDisplay();
	//SetWindowPos(&wndTopMost,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
	//CenterWindow();
	return TRUE;
}

void GhsList::OnOK() 
{
	if(omCalledFrom == CString("DSR_EINSATZ_DLG"))
	{
		int ilIdx = pomTable->GetCTableListBox()->GetCurSel();
		if(ilIdx != LB_ERR)
		{
			if(ilIdx < omLines.GetSize())
			{
				*plmGhsUrno = omLines[ilIdx].Urno;
//				prmSelectedGhs = ogGhsData.GetGhsByUrno(omLines[ilIdx].Urno);
//				if(prmSelectedGhs != NULL)
//				{
//					EndDialog(omLines[ilIdx].Urno);
//				}
			}
		}
		CDialog::OnOK();
	}
	else
	{
		DestroyWindow();
	}
	//CDialog::OnOK();
}

BOOL GhsList::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL rc;
	rc = CDialog::DestroyWindow();
	if(rc == TRUE)
	{
		delete this;
		pogGhsList = NULL;
	}

	return rc;
}

void GhsList::UpdateDisplay()
{													 
   pomTable->SetHeaderFields(GetString(IDS_STRING310));
   pomTable->SetFormatList("4|66");

   omLines.DeleteAll();
   pomTable->ResetContent();
   ogGhsData.omData.Sort(CompareByName);
   int ilCount = ogGhsData.omData.GetSize();
   for(int i = 0; i < ilCount; i++)
   {
	   omLines.NewAt(omLines.GetSize(), ogGhsData.omData[i]);
   }
	ilCount = omLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		GHSDATA rlGhs = omLines[i];
		//CreateLine(&rlFlight);
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
	}
	pomTable->DisplayTable();
}

CString GhsList::Format(GHSDATA *prpGhs)
{
   CString s;

	s =  CString(prpGhs->Lkco) + "|";
	s+=  CString(prpGhs->Lknm) + "|";
	return s;
}

void GhsList::CreateLine(GHSDATA *prpGhs)
{
	GHSDATA rlGhs;
	rlGhs = *prpGhs;
	omLines.NewAt(omLines.GetSize(), rlGhs);
}

void GhsList::OnCancel() 
{
	if(omCalledFrom != CString("DSR_EINSATZ_DLG"))
	{
		DestroyWindow();
	}
	else
	{
		CDialog::OnCancel();
	}
	//CDialog::OnCancel();
}
LONG GhsList::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CListBox *polListBox = (CListBox *)lParam;
	int ilMaxItems;
	if ((ilMaxItems = polListBox->GetSelCount()) == LB_ERR)
		return 0L;

	int *pilIndex = new int [ilMaxItems];
	polListBox->GetSelItems(ilMaxItems, pilIndex );
	m_DragDropSource.CreateDWordData(DIT_GHSLIST, ilMaxItems);
	for(int i = 0; i < ilMaxItems; i++)
	{
		GHSDATA rlGhs = omLines[pilIndex[i]];
		m_DragDropSource.AddDWord(rlGhs.Urno);
	}
	m_DragDropSource.BeginDrag();
	delete pilIndex;
	return 0L;
}
