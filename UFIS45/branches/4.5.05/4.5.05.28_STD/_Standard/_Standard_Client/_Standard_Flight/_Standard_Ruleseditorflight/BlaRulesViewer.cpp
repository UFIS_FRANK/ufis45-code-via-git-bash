// BlaTableViewer.cpp: Implementierung der Klasse BlaTableViewer.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CedaBlaData.h"
#include "resource.h"
#include "resrc1.h"

#include "BlaRulesViewer.h"
#include <math.h>
#include "Rules.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int width = 574;

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

BlaRulesViewer::BlaRulesViewer()
{
	bmNoUpdatesNow = false;
	

}

BlaRulesViewer::~BlaRulesViewer()
{
	DeleteAll();
}

void BlaRulesViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

#pragma warning( disable : 4100 )  
void BlaRulesViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}
 

int BlaRulesViewer::CompareBla(BLATABLE_LINEDATA *prpBla1, BLATABLE_LINEDATA *prpBla2)
{


     return 0;
}
#pragma warning( default : 4100 )  

/////////////////////////////////////////////////////////////////////////////
// BlaRulesViewer -- code specific to this class


void BlaRulesViewer::MakeLines()
{
	int ilBlaCount = ogBlaData.omData.GetSize();
	for(int illc = 0 ; illc < ilBlaCount; illc++)
	{		
		BLADATA * prlBla = &ogBlaData.omData[illc];
		MakeLine(prlBla);
		
	}
}


void BlaRulesViewer::MakeBlaLineData(BLADATA *popBla, CStringArray &ropArray)
{
			ropArray.Add(popBla->Ftyp);
		ropArray.Add(popBla->Agrp);
		ropArray.Add(popBla->Ogrp);

}

int BlaRulesViewer::MakeLine(BLADATA *prpBla)
{
/*
	int Rtnr;
	CString Belt;
	CString T;
	CString	Prio;
	CString	Sequ;
	CString	Pax;
	CString	Nat;
	CString	Alc;
	CString	Org;
	CString	Service;
*/

    // Update viewer data for this shift record
   BLATABLE_LINEDATA rlLine;

	CStringArray olBlaList;
	MakeBlaLineData(prpBla, olBlaList);
	int ilWordCount = olBlaList.GetSize();

	rlLine.Urno  = prpBla->Urno;
	rlLine.Ftyp = prpBla->Ftyp;
	rlLine.Agrp = prpBla->Agrp;
	rlLine.Ogrp = prpBla->Ogrp;
	rlLine.Time = prpBla->Time;

	
	return(CreateLine(&rlLine));
}


int BlaRulesViewer::CreateLine(BLATABLE_LINEDATA *prpBla)
{
    int ilLineCount = omLines.GetSize();

	BLATABLE_LINEDATA rlBla;
	rlBla = *prpBla;
    omLines.NewAt(ilLineCount, rlBla);

    return ilLineCount;
}

void BlaRulesViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


BOOL BlaRulesViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
      if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// BlaRulesViewer - GATTABLE_LINEDATA array maintenance

void BlaRulesViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// BlaRulesViewer - display drawing routine


void BlaRulesViewer::MakeHeaderData()
{
/*
	int Urno;
	CString Ftyp;
	CString Agrp;
	CString Ogrp;
	int     Time;
*/

	TABLE_HEADER_COLUMN rlHeader;
//	int ilColumnNo = -1;

	CStringArray olBlaList;
	ExtractItemList(LoadStg(IDS_STRING1477), &olBlaList, ',');
	
	CString olT;
	int ilActIdx = 0;
	int ilWordCount = olBlaList.GetSize();
	


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.SeparatorType = SEPA_NORMAL;//SEPA_NONE;//SEPA_LIKEVERTICAL

	//Ftyp
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "ID";
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//Aircraft Group
	rlHeader.Length = 28 * 3; 
	rlHeader.Text = "AC-Group";
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//Origin Group
	rlHeader.Length = 28 * 3; 
	rlHeader.Text = "Origin Group";
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	//Time
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "Alloc-Time";
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);



	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();

}



void BlaRulesViewer::MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,BLATABLE_LINEDATA *prpLine)
{

//	BOOL blNewLogicLine = FALSE;

	int ilColumnNo = 0;
	bool bldefekt = false;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Lineno = imTotalLines;//*3)+ilLocalLine;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.SeparatorType = SEPA_NORMAL;
	rlColumnData.Font = &ogCourier_Regular_8;//&ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
//	rlColumnData.Text.Format("%d",prpLine->Rtnr);  
//	rlColumnData.Columnno = ilColumnNo++;
//	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


	rlColumnData.Text = prpLine->Ftyp;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Agrp;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
/*
	rlColumnData.Text = prpLine->Id;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
*/
	rlColumnData.Text = prpLine->Ogrp;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	
	rlColumnData.Text.Format("%d",prpLine->Time);
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

}



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void BlaRulesViewer::UpdateDisplay()
{

	imTotalLines = 0;
//	BOOL blNewLogicLine = FALSE;


	pomTable->SetShowSelection(FALSE);
	pomTable->ResetContent();
	//pomTable->SetDockingRange(3);
	
	MakeHeaderData();
	pomTable->SetDefaultSeparator();
//	pomTable->SetSelectMode(0);

	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		MakeColumnData(olColList,&omLines[ilLc]);
		imTotalLines++;
		pomTable->AddTextLine(olColList, (void*)&(omLines[ilLc]));
		olColList.DeleteAll();
	}
    pomTable->DisplayTable();
}




