#ifndef _CEDAGHPDATA_H_
#define _CEDAGHPDATA_H_

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////
struct GHPDATA {

	long  Urno;		//Eindeutige Datensatz-Nr.
	CTime Cdat;		//Erstellungsdatum
	char  Usec[16];	//Anwender (Ersteller)
	CTime Lstu;		//Datum letzte �nderung
	char  Prfl[2];	//Protokollierungskennung
	char  Useu[16];	//Anwender (letzte �nderung)
	CTime Vpfr;		//G�ltig ab
	CTime Vpto;		//G�ltig bis
	char  Prna[68];	//Name (Bezeichnung)
	char  Prsn[14];	//Name (Kurzname)
	char  Prco[12];	//Code (Reduktionsstufe)
	char  Regn[14];	//LFZ-Kennzeichen
	char  Act5[7];	//Flugzeugtyp
	char  Actm[12];		//Gruppenwert Flugzeugtyp
	char  Flca[5];	//Fluggesellschaft Ankunft
	char  Flna[7];	//Flugnummer Ankunft
	char  Flsa[2];	//Flugnummer Suffix Ankunft
	char  Alma[12];		//Gruppenwert Fluggesellschaft
	char  Orig[6];	//Ausgangsflughafen
	char  Vi1a[6];	//Zwischenflughafen Ankunft
	char  Trra[12];	//Verkehrsgebiet Ankunft
	char  Scca[2];		//Anzahl Sektoren Ankunft
	char  Pcaa[12];	//Positionskategorie Ankunft
	char  Ttpa[5];	//Verkehrsart Ankunft
	char  Htpa[5];	//Abfertigungsart Ankunft
	char  Dopa[9];	//G�ltige Tage Ankunft
	char  Flcd[5];	//Fluggesellschaft Abflug
	char  Flnd[7];	//Flugnummer Abflug
	char  Flsd[2];	//Flugnummer Suffix Abflug
	char  Almd[12];		//Gruppenwert Fluggesellschaft
	char  Dest[6];	//Zielflughafen
	char  Vi1d[6];	//Zwischenflughafen Abflug
	char  Trrd[12];	//Verkehrsgebiet Abflug
	char  Sccd[2];		//Anzahl Sektoren Abflug
	char  Pcad[12];	//Positionskategorie Abflug
	char  Ttpd[5];	//Verkehrsart Abflug
	char  Htpd[5];	//Abfertigungsart Abflug
	char  Dopd[9];	//G�ltige Tage Abflug
	char  Osdu[257];	//Andere Standardleistungen (1-n)
	char  Ssic[14];	//Vertragliche Sonderleistungen (1-n)
	char  Rema[258];//Bemerkungen zur Regel
	char  Chgr[258];//�nderungsmitteilung zur Regel
	int   Maxt;		//Maximale Einsatzzeit
	int   Prio;		//Priorit�t der Pr�misse
	char  Ttga[12]; //Verkehrsart Gruppe Ankunft
	char  Ttgd[12]; //Verkehrsart Gruppe Abflug
	char  Htga[12]; //Abfertigungsart Gruppe Ankunft
	char  Htgd[12]; //Abfertigungsart Gruppe Ankunft
	char  Appl[10]; //Application
	char  Flia[6];	//Flug ID f�r Arrival
	char  Flid[6];	//Flug ID f�r Depature
	char  Gcad[12];	//Gates Category / group departure
	char  Lcad[12]; //Lounges(Waitingrooms) Category / group departure 

	//Additionals
	int  IsChanged;	// Check whether Data has Changed f�r Relaese

	GHPDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged=DATA_UNCHANGED;
		Cdat = TIMENULL;
		Lstu = TIMENULL;
		Vpfr = TIMENULL;
		Vpto = TIMENULL;
	}

	
 };	


// the broadcast CallBack function, has to be outside the CedaGhpData class


class CedaGhpData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<GHPDATA> omData;
    CCSPtrArray<GHPDATA> omSelectedData;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRuleGroupMap;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaGhpData();
	~CedaGhpData();

	bool Read(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<GHPDATA> *popGhp,char *pspWhere,char *pspFieldList, bool ipSYS = false);	
    bool Insert(const GHPDATA *prpGhp);    // used in PrePlanTable only
    bool Update(const GHPDATA *prpGhp);    // used in PrePlanTable only
	void ProcessGhpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ClearAll();
	void UpdateInternal(GHPDATA *prpGhp);
	void InsertInternal(GHPDATA *prpGhp);
	void DeleteInternal(GHPDATA *prpGhp);
	GHPDATA * GetGhpByUrno(long lpUrno);

	int  CheckPrsn(char *pspName);
    bool Insert(GHPDATA *prpGhp, bool bpWithSave = true);
    bool Update(GHPDATA *prpGhp, bool bpWithSave = true);
	bool Delete(GHPDATA *prpGhp, bool bpWithSave = true);
	bool Save(GHPDATA *prpGhp);
	CString GetFieldList();
	bool IsDataChanged(GHPDATA *prpOld, GHPDATA *prpNew);
};

extern CedaGhpData ogGhpData;

#endif //_CEDAGHPDATA_H_
