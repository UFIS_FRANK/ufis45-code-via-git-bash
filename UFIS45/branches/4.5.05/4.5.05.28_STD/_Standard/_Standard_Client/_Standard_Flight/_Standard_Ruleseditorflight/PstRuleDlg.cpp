// PstRuleDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Rules.h>
#include <PstRuleDlg.h>

#include <CedaAltData.h>
#include <CedaActData.h>
#include <CedaAcrData.h>		// 050228 MVy: get access to gloabally loaded Aircraft Registration Data
#include <CedapstData.h>
#include <resrc1.h>
#include <PositionMatrixDlg.h>
#include <CedaBasicData.h>
#include <CedaAloData.h>
#include "AirCraftGroupSelDlg.h"
#include "GroupNamesPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <BasicData.h>

/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg dialog

IMPLEMENT_SERIAL(PstRuleDlg,CDialog,0);

PstRuleDlg::PstRuleDlg(CWnd* pParent, PSTDATA *popPst)
	: CDialog(PstRuleDlg::IDD, pParent)
{
	pomPst = popPst;

	//{{AFX_DATA_INIT(PstRuleDlg)
	m_AL_List = _T("");
	m_Exce = _T("");
	m_PDest = _T("");
	m_Heig_Max = _T("");
	m_Heig_Min = _T("");
	m_Leng_Max = _T("");
	m_Leng_Min = _T("");
	m_Max_Ac = _T("");
	m_Nat_Restr = _T("");
	m_Npos1 = _T("");
	m_Npos2 = _T("");
	m_Npos3 = _T("");
	m_Npos4 = _T("");
	m_Npos5 = _T("");
	m_Npos6 = _T("");
	m_Npos7 = _T("");
	m_Npos8 = _T("");
	m_Npos9 = _T("");
	m_Npos10= _T("");
	m_Prio = _T("");
	m_Restr_To1 = _T("");
	m_Restr_To2 = _T("");
	m_Restr_To3 = _T("");
	m_Restr_To4 = _T("");
	m_Restr_To5 = _T("");
	m_Restr_To6 = _T("");
	m_Restr_To7 = _T("");
	m_Restr_To8 = _T("");
	m_Restr_To9 = _T("");
	m_Restr_To10= _T("");
	m_Span_Max = _T("");
	m_Span_Min = _T("");
	m_Sequence = 1;
	m_Gt_Value1 = 0;
	m_Gt_Value2 = 0;
	m_Gt_String = _T("");
	m_STYP_List = _T("");
	m_FltiArr = _T("");
	m_FltiDep = _T("");
	m_ACGroup = _T("");
	m_MaxPax = _T("");
	m_MinPax = _T("");
	//}}AFX_DATA_INIT

	CStringArray olValues;
//	ExtractItemList(pomPst->Acgr, &olValues, '/');
	CString olTmpPosr = popPst->Posr;
	if(olTmpPosr.Replace(";","#") < 3)
	{
		olTmpPosr = popPst->Posr;
		olTmpPosr.Replace("/",";");
		strcpy(pomPst->Posr, olTmpPosr.GetBuffer(0));
	}
	
	ExtractItemList(popPst->Posr, &olValues, ';');

	if (olValues.GetSize() == 12)	// old format
	{
		for(int i = 0; i < olValues.GetSize(); i++)
		{
			olValues[i].TrimRight();
			CStringArray olItems;
			ExtractItemList(olValues[i], &olItems, '-');
			if(i == 0)//wing span
			{
				if(olItems.GetSize() == 2)
				{
					m_Span_Min = olItems[0];
					m_Span_Max = olItems[1];
				}
			}
			if(i == 1)//length
			{
				if(olItems.GetSize() == 2)
				{
					m_Leng_Min = olItems[0];
					m_Leng_Max = olItems[1];
				}
			}
			if(i==2)//height
			{
				if(olItems.GetSize() == 2)
				{
					m_Heig_Min = olItems[0];
					m_Heig_Max = olItems[1];
				}
			}
		}
		//handle the rest
		m_Exce		=	olValues[3];
		m_Npos1		=	olValues[4];
		m_Restr_To1	=	olValues[5];
		m_Npos2		=	olValues[6];
		m_Restr_To2	=	olValues[7];
		m_Max_Ac	=	olValues[8];
		m_Prio		=	olValues[9];
		m_Nat_Restr	=	olValues[10];
		m_AL_List	=	olValues[11];

		m_Restr_To3	=	"0";
		m_Restr_To4	=	"0";
		m_Restr_To5	=	"0";
		m_Restr_To6	=	"0";
		m_Restr_To7	=	"0";
		m_Restr_To8	=	"0";
		m_Restr_To9	=	"0";
		m_Restr_To10=	"0";

		m_Gt_String  =   "   ,,,   ,";
		m_FltiArr = "";
		m_FltiDep = "";
		m_ACGroup = "";
	}
	else if(olValues.GetSize() >= 28)
	{
		for(int i = 0; i < olValues.GetSize(); i++)
		{
			olValues[i].TrimRight();
			CStringArray olItems;
			ExtractItemList(olValues[i], &olItems, '-');
			if(i == 0)//wing span
			{
				if(olItems.GetSize() == 2)
				{
					m_Span_Min = olItems[0];
					m_Span_Max = olItems[1];
				}
			}
			if(i == 1)//length
			{
				if(olItems.GetSize() == 2)
				{
					m_Leng_Min = olItems[0];
					m_Leng_Max = olItems[1];
				}
			}
			if(i==2)//height
			{
				if(olItems.GetSize() == 2)
				{
					m_Heig_Min = olItems[0];
					m_Heig_Max = olItems[1];
				}
			}
		}
		//handle the rest
		m_Exce		=	olValues[3];
		m_Npos1		=	olValues[4];
		m_Restr_To1	=	olValues[5];
		m_Npos2		=	olValues[6];
		m_Restr_To2	=	olValues[7];
		m_Max_Ac	=	olValues[8];
		m_Prio		=	olValues[9];
		m_Nat_Restr	=	olValues[10];
		m_AL_List	=	olValues[11];

		m_Npos3		=	olValues[12];
		m_Restr_To3	=	olValues[13];
		m_Npos4		=	olValues[14];
		m_Restr_To4	=	olValues[15];
		m_Npos5		=	olValues[16];
		m_Restr_To5	=	olValues[17];
		m_Npos6		=	olValues[18];
		m_Restr_To6	=	olValues[19];
		m_Npos7		=	olValues[20];
		m_Restr_To7	=	olValues[21];
		m_Npos8		=	olValues[22];
		m_Restr_To8	=	olValues[23];
		m_Npos9		=	olValues[24];
		m_Restr_To9	=	olValues[25];
		m_Npos10	=	olValues[26];
		m_Restr_To10=	olValues[27];

		//sequence
		if (olValues.GetSize() >= 29)
			m_Sequence  =   atoi(olValues[28]);
		else
			m_Sequence  =   1;

		//groundtime
		if (olValues.GetSize() >= 30)
			m_Gt_String  =   olValues[29];
		else
			m_Gt_String  =   "   ,,,   ,";

		//servicetypes
		if (olValues.GetSize() >= 31)
			m_STYP_List  =   olValues[30];
		else
			m_STYP_List  =   "";

		//ids (Arr/Dep)
		if (olValues.GetSize() >= 32)
			m_FltiArr  =   olValues[31];
		else
			m_FltiArr  =   "";

		if (olValues.GetSize() >= 33)
			m_FltiDep  =   olValues[32];
		else
			m_FltiDep  =   "";

		olValues.GetSize() >= 34 ? m_sPst_Regn = olValues[33] : m_sPst_Regn = "" ;		// 050228 MVy: Aircraft Registrations, extract from read data

		if(olValues.GetSize() >= 35)
			m_ACGroup = olValues[34];
		else
			m_ACGroup = "";

		if(olValues.GetSize() >= 36)
			m_PDest = olValues[35];
		else
			m_PDest = "";

		if(olValues.GetSize() >= 37)
			m_MinPax = olValues[36];
		else
			m_MinPax = "0";
	
		if(olValues.GetSize() >= 38)
			m_MaxPax = olValues[37];
		else
			m_MaxPax = "0";


		if(olValues.GetSize() >= 39)
			m_sPst_Flno = olValues[38];
		else
			m_sPst_Flno = "";


	}
	// conversion from LIS4.4 to Standard4.5
	else if (olValues.GetSize() != 32 && (strcmp("FCO",pcgHome) == 0 || strcmp("LIS",pcgHome) == 0))
	{
		for(int i = 0; i < olValues.GetSize(); i++)
		{
			olValues[i].TrimRight();
			CStringArray olItems;
			ExtractItemList(olValues[i], &olItems, '-');
			if(i == 0)//wing span
			{
				if(olItems.GetSize() == 2)
				{
					m_Span_Min = olItems[0];
					m_Span_Max = olItems[1];
				}
			}
			if(i == 1)//length
			{
				if(olItems.GetSize() == 2)
				{
					m_Leng_Min = olItems[0];
					m_Leng_Max = olItems[1];
				}
			}
			if(i==2)//height
			{
				if(olItems.GetSize() == 2)
				{
					m_Heig_Min = olItems[0];
					m_Heig_Max = olItems[1];
				}
			}
		}
		//handle the rest
		if (olValues.GetSize() > 3)
			m_Exce		=	olValues[3];
		if (olValues.GetSize() > 4)
			m_Npos1		=	olValues[4];
		if (olValues.GetSize() > 5)
			m_Restr_To1	=	olValues[5];
		if (olValues.GetSize() > 6)
			m_Npos2		=	olValues[6];
		if (olValues.GetSize() > 7)
			m_Restr_To2	=	olValues[7];
		if (olValues.GetSize() > 8)
			m_Max_Ac	=	olValues[8];
		if (olValues.GetSize() > 9)
			m_Prio		=	olValues[9];
		if (olValues.GetSize() > 10)
			m_Nat_Restr	=	olValues[10];
		if (olValues.GetSize() > 11)
			m_AL_List	=	olValues[11]; 

		m_Restr_To3	=	"0";
		m_Restr_To4	=	"0";
		m_Restr_To5	=	"0";
		m_Restr_To6	=	"0";
		m_Restr_To7	=	"0";
		m_Restr_To8	=	"0";
		m_Restr_To9	=	"0";
		m_Restr_To10=	"0";

		if (olValues.GetSize() > 12)
			//sequence
			m_Sequence  =   atoi(olValues[12]);

			//groundtime
		if (olValues.GetSize() > 13)
			m_Gt_String  =   olValues[13];
		else
			m_Gt_String  =   "   ,,,   ,";

			//servicetypes
		if (olValues.GetSize() > 14)
			m_STYP_List  =   olValues[14]; 

		if(olValues.GetSize() > 15)
			m_ACGroup = olValues[15]; 
/*
		if (olValues.GetSize() > 13)
			//sequence
			m_Sequence  =   atoi(olValues[13]);

			//groundtime
		if (olValues.GetSize() > 14)
			m_Gt_String  =   olValues[14];

			//servicetypes
		if (olValues.GetSize() > 15)
			m_STYP_List  =   olValues[15]; 
*/
	}
}

PstRuleDlg ::PstRuleDlg()
{

}

void PstRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstRuleDlg)
	DDX_Control(pDX, IDC_COMBO_GT_UNIT2, m_Gt_Unit2);
	DDX_Control(pDX, IDC_COMBO_GT_UNIT1, m_Gt_Unit1);
	DDX_Control(pDX, IDC_EDIT_GT_VALUE1, m_Gt_Value1Ctrl);
	DDX_Control(pDX, IDC_EDIT_GT_VALUE2, m_Gt_Value2Ctrl);
	DDX_Control(pDX, IDC_POS_MAXPAX, m_MaxPaxCtrl);
	DDX_Control(pDX, IDC_POS_MINPAX, m_MinPaxCtrl);
	DDX_Control(pDX, IDC_CHECK_GT_AND, m_Gt_AndCtrl);
	DDX_Control(pDX, IDC_SEQU1, m_SequenceCtrl);
	DDX_Control(pDX, IDC_MATRIX1, m_Matrix1);
	DDX_Text(pDX, IDC_AL_LIST, m_AL_List);
	DDV_MaxChars(pDX, m_AL_List, 300);
	DDX_Text(pDX, IDC_EXCE, m_Exce);
	DDV_MaxChars(pDX, m_Exce, 350);
	DDX_Text(pDX, IDC_PST_RULES_ACR_REGN, m_sPst_Regn);		// 050228 MVy: Registrations
	DDX_Text(pDX, IDC_PST_RULES_FLNO, m_sPst_Flno);		// 050228 MVy: Registrations
	DDV_MaxChars(pDX, m_sPst_Regn, 350);
	DDX_Text(pDX, IDC_HEIG_MAX, m_Heig_Max);
	DDV_MaxChars(pDX, m_Heig_Max, 4);
	DDX_Text(pDX, IDC_HEIG_MIN, m_Heig_Min);
	DDV_MaxChars(pDX, m_Heig_Min, 4);
	DDX_Text(pDX, IDC_LENG_MAX, m_Leng_Max);
	DDV_MaxChars(pDX, m_Leng_Max, 5);
	DDX_Text(pDX, IDC_LENG_MIN, m_Leng_Min);
	DDV_MaxChars(pDX, m_Leng_Min, 5);
	DDX_Text(pDX, IDC_MAX_AC, m_Max_Ac);
	DDV_MaxChars(pDX, m_Max_Ac, 2);
	DDX_Text(pDX, IDC_NAT_RESTR, m_Nat_Restr);
	DDV_MaxChars(pDX, m_Nat_Restr, 150);
	DDX_Text(pDX, IDC_NPOS1, m_Npos1);
	DDV_MaxChars(pDX, m_Npos1, 5);
	DDX_Text(pDX, IDC_NPOS2, m_Npos2);
	DDV_MaxChars(pDX, m_Npos2, 5);
	DDX_Text(pDX, IDC_NPOS3, m_Npos3);
	DDV_MaxChars(pDX, m_Npos3, 5);
	DDX_Text(pDX, IDC_NPOS4, m_Npos4);
	DDV_MaxChars(pDX, m_Npos4, 5);
	DDX_Text(pDX, IDC_NPOS5, m_Npos5);
	DDV_MaxChars(pDX, m_Npos5, 5);
	DDX_Text(pDX, IDC_NPOS6, m_Npos6);
	DDV_MaxChars(pDX, m_Npos6, 5);
	DDX_Text(pDX, IDC_NPOS7, m_Npos7);
	DDV_MaxChars(pDX, m_Npos7, 5);
	DDX_Text(pDX, IDC_NPOS8, m_Npos8);
	DDV_MaxChars(pDX, m_Npos8, 5);
	DDX_Text(pDX, IDC_NPOS9, m_Npos9);
	DDV_MaxChars(pDX, m_Npos9, 5);
	DDX_Text(pDX, IDC_NPOS10, m_Npos10);
	DDV_MaxChars(pDX, m_Npos10, 5);
	DDX_Text(pDX, IDC_PRIO, m_Prio);
	DDV_MaxChars(pDX, m_Prio, 1);
	DDX_Text(pDX, IDC_RESTR_TO1, m_Restr_To1);
	DDX_Text(pDX, IDC_RESTR_TO2, m_Restr_To2);
	DDX_Text(pDX, IDC_RESTR_TO3, m_Restr_To3);
	DDX_Text(pDX, IDC_RESTR_TO4, m_Restr_To4);
	DDX_Text(pDX, IDC_RESTR_TO5, m_Restr_To5);
	DDX_Text(pDX, IDC_RESTR_TO6, m_Restr_To6);
	DDX_Text(pDX, IDC_RESTR_TO7, m_Restr_To7);
	DDX_Text(pDX, IDC_RESTR_TO8, m_Restr_To8);
	DDX_Text(pDX, IDC_RESTR_TO9, m_Restr_To9);
	DDX_Text(pDX, IDC_RESTR_TO10,m_Restr_To10);
	DDX_Text(pDX, IDC_SPAN_MAX, m_Span_Max);
	DDV_MaxChars(pDX, m_Span_Max, 5);
	DDX_Text(pDX, IDC_SPAN_MIN, m_Span_Min);
	DDV_MaxChars(pDX, m_Span_Min, 5);
	DDX_Text(pDX, IDC_SEQU1, m_Sequence);
	DDV_MinMaxInt(pDX, m_Sequence, 1, 999);
	DDX_Text(pDX, IDC_EDIT_GT_VALUE1, m_Gt_Value1);
	DDV_MinMaxInt(pDX, m_Gt_Value1, 0, 9999);
	DDX_Text(pDX, IDC_EDIT_GT_VALUE2, m_Gt_Value2);
	DDV_MinMaxInt(pDX, m_Gt_Value2, 0, 9999);
	DDX_Text(pDX, IDC_STYP_LIST1, m_STYP_List);
	DDV_MaxChars(pDX, m_STYP_List, 150);
	DDX_Text(pDX, IDC_FLTI_POS_ARR, m_FltiArr);
	DDX_Text(pDX, IDC_FLTI_POS_DEP, m_FltiDep);
	DDX_Text(pDX, IDC_ACGROUP,m_ACGroup);
	DDX_Text(pDX, IDC_PDEST2, m_PDest);
	DDX_Text(pDX, IDC_POS_MAXPAX, m_MaxPax);
	DDX_Text(pDX, IDC_POS_MINPAX, m_MinPax);
	DDV_MaxChars(pDX, m_PDest, 150);
	//	DDX_Control(pDX, IDC_FLTI_POS_ARR, m_FltiArr_Ctrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PstRuleDlg, CDialog)
	//{{AFX_MSG_MAP(PstRuleDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_GT_UNIT1, OnSelchangeComboGtUnit1)
	ON_BN_CLICKED(IDC_CHECK_GT_AND, OnCheckGtAnd)
	ON_CBN_SELCHANGE(IDC_COMBO_GT_UNIT2, OnSelchangeComboGtUnit2)
	ON_BN_CLICKED(IDC_MATRIX1, OnMatrix1)
	ON_BN_CLICKED(IDC_MATRIX2, OnMatrix2)
	ON_BN_CLICKED(IDC_MATRIX3, OnMatrix3)
	ON_BN_CLICKED(IDC_MATRIX4, OnMatrix4)
	ON_BN_CLICKED(IDC_MATRIX5, OnMatrix5)
	ON_BN_CLICKED(IDC_MATRIX6, OnMatrix6)
	ON_BN_CLICKED(IDC_MATRIX7, OnMatrix7)
	ON_BN_CLICKED(IDC_MATRIX8, OnMatrix8)
	ON_BN_CLICKED(IDC_MATRIX9, OnMatrix9)
	ON_BN_CLICKED(IDC_MATRIX10,OnMatrix10)
	ON_BN_CLICKED(IDC_ALMATRIX1, OnALMatrix1)
	ON_BN_CLICKED(IDC_ALMATRIX2, OnALMatrix2)
	ON_BN_CLICKED(IDC_ALMATRIX3, OnALMatrix3)
	ON_BN_CLICKED(IDC_ALMATRIX4, OnALMatrix4)
	ON_BN_CLICKED(IDC_ALMATRIX5, OnALMatrix5)
	ON_BN_CLICKED(IDC_ALMATRIX6, OnALMatrix6)
	ON_BN_CLICKED(IDC_ALMATRIX7, OnALMatrix7)
	ON_BN_CLICKED(IDC_ALMATRIX8, OnALMatrix8)
	ON_BN_CLICKED(IDC_ALMATRIX9, OnALMatrix9)
	ON_BN_CLICKED(IDC_ALMATRIX10,OnALMatrix10)
	ON_BN_CLICKED(IDC_APMATRIX1, OnAPMatrix1)
	ON_BN_CLICKED(IDC_APMATRIX2, OnAPMatrix2)
	ON_BN_CLICKED(IDC_APMATRIX3, OnAPMatrix3)
	ON_BN_CLICKED(IDC_APMATRIX4, OnAPMatrix4)
	ON_BN_CLICKED(IDC_APMATRIX5, OnAPMatrix5)
	ON_BN_CLICKED(IDC_APMATRIX6, OnAPMatrix6)
	ON_BN_CLICKED(IDC_APMATRIX7, OnAPMatrix7)
	ON_BN_CLICKED(IDC_APMATRIX8, OnAPMatrix8)
	ON_BN_CLICKED(IDC_APMATRIX9, OnAPMatrix9)
	ON_BN_CLICKED(IDC_APMATRIX10,OnAPMatrix10)
	ON_BN_CLICKED(IDC_BUTTON1,OnACGroupSel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//	checking and other helper functions

// 050228 MVy: check the values in the registrations edit field
//	teach field of the user edit has to be verified against the possible values in database eg.BKK1.BKK.ACR.REGN, 
//	the necessary database values are loaded into global dataspace at program startup, -> ogAcrData
//	fill message text (if any) with the wrong user input in edit field
BOOL PstRuleDlg::CheckRegistrations( CString *psMessage )
{
	CStringArray olArray;
	ExtractItemList( m_sPst_Regn, &olArray, ' ');

	CString sCurrent ;
	for( int i = 0; i < olArray.GetSize(); i++)
	{
		sCurrent = olArray[i].GetBuffer(0);

		if( sCurrent.GetLength() > 1 )
		{
			if(sCurrent[0] == '!' || sCurrent[0] == '=')
				sCurrent = sCurrent.Right( sCurrent.GetLength() - 1 );

			void *p = 0 ;
			UINT ui = ogAcrData.omRegnMap.Lookup( sCurrent, p );
			ASSERT( !ui == !p );	// result set on if pointer to keyvalue set and vice versa

			if( !ui )	// key found
			{
				static CString sTemp ;
				sTemp.Format( GetString(IDS_STRING1490), sCurrent );		// Aircraft registration %s does not exist
				if( psMessage ) *psMessage += sTemp ;
				return TRUE ;		// error occured
			};
		};
	};	// for each value

	return FALSE ;		// no error
};	// CheckRegistrations

/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg message handlers

void PstRuleDlg::OnOK() 
{
	UpdateData(TRUE);
	// TODO: Add extra validation here
	bool blOK=true;
	int i=0;
	CString olMsg;
	CString olTmp;

	CString olSpan,
			olLength,
			olHeight;
	
	//check data for sequence
	CString olTmpTxt;
	m_SequenceCtrl.GetWindowText(olTmpTxt);
	
	int ilLength = olTmpTxt.GetLength();
	bool blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;

	int ilfsc = atoi(olTmpTxt);
	if(ilfsc < 1 || ilfsc > 999)
	{
		blIsValid = false;
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1470), GetString(IDS_STRING117), MB_OK);
		m_SequenceCtrl.SetFocus();
		return;
	}

	// check max AC
	blIsValid = true;
	CString olcharL = m_Max_Ac.Left(1);
	CString olcharR = m_Max_Ac.Right(1);
	if (olcharL.FindOneOf("0123456789") == -1 || olcharR.FindOneOf("0123456789") == -1)
		blIsValid = false;

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1442), GetString(IDS_STRING117), MB_OK);
		CButton* polWnd = (CButton*) GetDlgItem(IDC_MAX_AC);
		if (polWnd)
			polWnd->SetFocus();
		return;
	}
	

	//check data for groundtime
	m_Gt_Value1Ctrl.GetWindowText(olTmpTxt);
	
	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 4 || ilLength == 0)
		blIsValid = false;

	int ilVal1 = atoi(olTmpTxt);
	if(ilVal1 < 0 || ilVal1 > 9999)
	{
		blIsValid = false;
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1471), GetString(IDS_STRING117), MB_OK);
		m_Gt_Value1Ctrl.SetFocus();
		return;
	}

	m_Gt_Value2Ctrl.GetWindowText(olTmpTxt);
	
	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 4 || ilLength == 0)
		blIsValid = false;

	int ilVal2 = atoi(olTmpTxt);
	if(ilVal2 < 0 || ilVal2 > 9999)
	{
		blIsValid = false;
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1471), GetString(IDS_STRING117), MB_OK);
		m_Gt_Value2Ctrl.SetFocus();
		return;
	}


	UpdateData(TRUE);


	CStringArray olArray;
	ExtractItemList(m_AL_List, &olArray, ' ');

	CString olTmp2;	
	
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
						
			olTmp2.Remove('!');
			olTmp2.Remove('=');

			int ilColonPos =  olTmp2.Find(":");
			int ilStarPos =  olTmp2.Find("*");


			CString olAirline;
			CString olAirport;
			CString olALGroup;

			if( ilColonPos >= 0)
			{
				olAirport = olTmp2.Right( olTmp2.GetLength() - ilColonPos -1  );

				if( ilStarPos >= 0)
				{
					olALGroup = olTmp2.Left(ilColonPos);
					olALGroup.Remove('*');
				}
				else
				{
					olAirline = olTmp2.Left(ilColonPos);

				}
			}
			else
			{

				if( ilStarPos >= 0)
				{
					olAirline = "";
					olALGroup = olTmp2;
					olALGroup.Remove('*');
				}
				else
				{
					olAirline = olTmp2;
				}
			}


			if( !olAirline.IsEmpty())
			{

				ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olAirline.GetBuffer(0));
				if(prlAlt == NULL)
				{
					prlAlt = ogAltData.GetAltByAlc3(olAirline.GetBuffer(0));
					if(prlAlt == NULL)
					{
						olTmp.Format(GetString(IDS_STRING1430), olAirline);
						olMsg+=olTmp;
					}
				}
			}


			if( !olAirport.IsEmpty())
			{

				if( ogAptData.GetUrnoByApc( olAirport.GetBuffer(0)) == 0)
				{
					olTmp.Format(GetString(IDS_STRING1456), olAirport);
					olMsg+=olTmp;
				}
			}


			if( !olALGroup.IsEmpty())
			{

				if( ogGrnData.GetUrnoByName(olALGroup.GetBuffer(0), "ALTTAB", "POPS") == 0)
				{
					olTmp.Format(GetString(IDS_STRING1492), olALGroup);
					olMsg+=olTmp;
				}

			}


		}

	}
	// Airports

	olArray.RemoveAll();

	ExtractItemList(m_PDest, &olArray, ' ');

	olTmp2 = "";	
	
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			olTmp2.Remove('!');
			olTmp2.Remove('=');

			int ilStarPos =  olTmp2.Find("*");
			CString olAirport;
			CString olAPGroup;


			if( ilStarPos >= 0)
			{
				olAirport = "";
				olAPGroup = olTmp2;
				olAPGroup.Remove('*');
			}
			else
			{
				olAirport = olTmp2;
			}


			if( !olAirport.IsEmpty())
			{

				if( ogAptData.GetUrnoByApc( olAirport.GetBuffer(0)) == 0)
				{
					olTmp.Format(GetString(IDS_STRING1456), olAirport);
					olMsg+=olTmp;
				}
			}


			if( !olAPGroup.IsEmpty())
			{

				if( ogGrnData.GetUrnoByName(olAPGroup.GetBuffer(0), "APTTAB", "POPS") == 0)
				{
					olTmp.Format(GetString(IDS_STRING1497), olAPGroup);
					olMsg+=olTmp;
				}

			}


		}

	}


	////////////////////////////////////

	ExtractItemList(m_Exce, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
		
			ACTDATA *prlAct = ogActData.GetActByAct3(olTmp2.GetBuffer(0));
			if(prlAct == NULL)
			{
				prlAct = ogActData.GetActByAct5(olTmp2.GetBuffer(0));
				if(prlAct == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1431), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}

	// 050228 MVy: handle Aircraft Registrations only when configuration key POSRULE_WITH_REGN is set
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleAcrRegn() )
	{
		CString sMessage ;
		if( CheckRegistrations( &sMessage ) )
		{
			olMsg += sMessage ;		// 050228 MVy: add error information if registration contains invalid text
		};
	};

	ExtractItemList(m_STYP_List, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 0)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
			CString olUrno;
			if(!ogBCD.GetField("STY", "STYP", olTmp2, "URNO", olUrno))
			{
				olTmp.Format(GetString(IDS_STRING1463), olTmp2);
				olMsg+=olTmp;
			}
		}
	}


	olSpan = m_Span_Max+"-"+m_Span_Min;
	olHeight = m_Heig_Max+"-"+m_Heig_Min;
	olLength = m_Leng_Max+"-"+m_Leng_Min;
	if(m_Max_Ac.IsEmpty())
	{
		m_Max_Ac = "1";
	}
	if(m_Prio.IsEmpty())
	{
		m_Prio = "0";
	}
	
	if(	!m_Npos1.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos1.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos1.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To1.IsEmpty())
		{
			m_Restr_To1	=	"0";
//			olMsg+=GetString(IDS_STRING1433);
		}
	}
	if(	!m_Npos2.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos2.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos2.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To2.IsEmpty())
		{
			m_Restr_To2	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos3.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos3.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos3.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To3.IsEmpty())
		{
			m_Restr_To3	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos4.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos4.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos4.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To4.IsEmpty())
		{
			m_Restr_To4	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos5.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos5.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos5.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To5.IsEmpty())
		{
			m_Restr_To5	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos6.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos6.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos6.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To6.IsEmpty())
		{
			m_Restr_To6	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos7.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos7.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos7.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To7.IsEmpty())
		{
			m_Restr_To7	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos8.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos8.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos8.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To8.IsEmpty())
		{
			m_Restr_To8	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos9.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos9.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos9.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To9.IsEmpty())
		{
			m_Restr_To9	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos10.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos10.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos10.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To10.IsEmpty())
		{
			m_Restr_To10	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	
	bool ol_GroupFound = FALSE;
	if(!m_ACGroup.IsEmpty())
	{
		CCSPtrArray<GRNDATA> olGrnList;
		char pclTableName[16] = "ACTTAB";
		ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
		int ilCount = olGrnList.GetSize();

		CStringArray olArray;
		ExtractItemList(m_ACGroup, &olArray, ' ');
		CString olTmp2;	
		for(i = 0; i < olArray.GetSize(); i++)
		{
			olTmp2 = olArray[i].GetBuffer(0);

			if(olTmp2.GetLength() > 1)
			{
				if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
				
				for(int i = 0; i < ilCount; i++)
				{
					if(olGrnList[i].Grpn == olTmp2)
					ol_GroupFound = TRUE;
				}
				if(!ol_GroupFound)
				{
					olTmp.Format(GetString(IDS_GROUP_ERROR), olTmp2);
					olMsg+=olTmp;
				}

			}
			ol_GroupFound = FALSE;
		}
	}

	if(olMsg.IsEmpty())
	{

		CString olStr;
		if(m_AL_List.GetLength() > 300)
		{
			m_AL_List = m_AL_List.Left(300);
		}
		if(m_STYP_List.GetLength() > 150)
		{
			m_STYP_List = m_STYP_List.Left(150);
		}
		if(m_Exce.GetLength() > 350)
		{
			m_Exce = m_Exce.Left(350);
		}
	
		

		olStr += m_Span_Min+"-"+m_Span_Max+";";
		olStr += m_Leng_Min+"-"+m_Leng_Max+";";
		olStr += m_Heig_Min+"-"+m_Heig_Max+";";
		olStr += m_Exce+";";
		//olStr += m_ACGroup+";";
		olStr += m_Npos1+";";
		olStr += m_Restr_To1+";";
		olStr += m_Npos2+";";
		olStr += m_Restr_To2+";";
		olStr += m_Max_Ac+";";
		olStr += m_Prio+";";
		olStr += m_Nat_Restr+";";
		olStr += m_AL_List+";";

		olStr += m_Npos3+";";
		olStr += m_Restr_To3+";";
		olStr += m_Npos4+";";
		olStr += m_Restr_To4+";";
		olStr += m_Npos5+";";
		olStr += m_Restr_To5+";";
		olStr += m_Npos6+";";
		olStr += m_Restr_To6+";";
		olStr += m_Npos7+";";
		olStr += m_Restr_To7+";";
		olStr += m_Npos8+";";
		olStr += m_Restr_To8+";";
		olStr += m_Npos9+";";
		olStr += m_Restr_To9+";";
		olStr += m_Npos10+";";
		olStr += m_Restr_To10+";";

	//sequence
		char bufferTmp[128];
		itoa(m_Sequence, bufferTmp, 10);
		CString olStrSequ = CString(bufferTmp);
		olStr += olStrSequ+";";

	//groundtime
		CString olUnit1; 
		m_Gt_Unit1.GetLBText( m_Gt_Unit1.GetCurSel(), olUnit1 );

		itoa(m_Gt_Value1, bufferTmp, 10);
		CString olValue1 = CString(bufferTmp);

		if ((olUnit1 == "<" || olUnit1 == "<=" || olUnit1 == "=") && m_Gt_Value1 == 0)
		{
			MessageBox(GetString(IDS_STRING1472), GetString(IDS_STRING117), MB_OK);
			m_Gt_Value1Ctrl.SetFocus();
			return;
		}

		CString olAnd = "";
		if (m_Gt_AndCtrl.GetCheck())
			olAnd = "and";

		CString olUnit2; 
		m_Gt_Unit2.GetLBText( m_Gt_Unit2.GetCurSel(), olUnit2 );

		itoa(m_Gt_Value2, bufferTmp, 10);
		CString olValue2 = CString(bufferTmp);

		if ((olUnit2 == "<" || olUnit2 == "<=") && m_Gt_Value2 == 0 && 	m_Gt_AndCtrl.GetCheck())
		{
			MessageBox(GetString(IDS_STRING1473), GetString(IDS_STRING117), MB_OK);
			m_Gt_Value2Ctrl.SetFocus();
			return;
		}

		if ((olUnit1 == ">" || olUnit1 == ">=") && m_Gt_AndCtrl.GetCheck() && olUnit2 != "   " && m_Gt_Value1 >= m_Gt_Value2)
		{
			MessageBox(GetString(IDS_STRING1474), GetString(IDS_STRING117), MB_OK);
			m_Gt_Value1Ctrl.SetFocus();
			return;
		}

		// store empty value if no unit
		if (olUnit1 == "   ")
			olValue1 = "";

		// no "and" is used, clear all and-values
		if (olUnit2 == "   ")
		{
			olAnd = "";
			olValue2 = "";
		}

		if (olAnd.IsEmpty())
		{
			olUnit2 = "   ";
			olValue2 = "";
		}

		CString olStrGT;
		if (!olUnit1.IsEmpty())
		{
			olStrGT  = olUnit1 + ",";
			olStrGT += olValue1 + ",";
			olStrGT += olAnd + ",";
			olStrGT += olUnit2 + ",";
			olStrGT += olValue2;
		}

		olStr += olStrGT+";";

		//servicetypes
		olStr += m_STYP_List + ";";

		//flightIds (Arr/Dep)
		olStr += m_FltiArr + ";";
		olStr += m_FltiDep;

		// hint: this is the last item in string, so append a semicolon before and NO after
		olStr += ";" + m_sPst_Regn ;		// 050228 MVy: Aircraft Registrations, add before writing

		//Note: this is the last item in string , so append a semicolon before but not after the string.
		olStr += ";" + m_ACGroup;

		olStr += ";" + m_PDest;


		m_MinPaxCtrl.GetWindowText(olTmpTxt);

		olTmpTxt.TrimLeft();


		if (olTmpTxt.FindOneOf("0123456789") == -1 || olTmpTxt.FindOneOf("0123456789") == -1)
		{
			MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
			m_MinPaxCtrl.SetFocus();
			return;
		}

		olStr += ";" + olTmpTxt;


		m_MaxPaxCtrl.GetWindowText(olTmpTxt);

		olTmpTxt.TrimLeft();

		if (olTmpTxt.FindOneOf("0123456789") == -1 || olTmpTxt.FindOneOf("0123456789") == -1)
		{
			MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
			m_MinPaxCtrl.SetFocus();
			return;
		}

		olStr += ";" + olTmpTxt;


		olStr += ";" + m_sPst_Flno ;		


		if (olStr.GetLength() > 2000)
		{
			MessageBox(GetString(IDS_STRING1468), GetString(IDS_STRING117), MB_OK);
			return;
		}

		//store data
		strncpy(pomPst->Posr, olStr.GetBuffer(0), 2000);
		pomPst->IsChanged = DATA_CHANGED;
		ogPstData.SavePST(pomPst);

		CDialog::OnOK();
	}
	else
	{
		MessageBox(olMsg.GetBuffer(0), GetString(IDS_STRING117), MB_OK);
	}

}

BOOL PstRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CWnd *polWnd;

	if( ( (CRulesApp *)AfxGetApp() )->CanHandleCenterLine() == TRUE)
	{
	
		polWnd = (CButton*) GetDlgItem(IDC_REST1);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (1):");
		polWnd = (CButton*) GetDlgItem(IDC_REST2);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (2):");
		polWnd = (CButton*) GetDlgItem(IDC_REST3);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (3):");
		polWnd = (CButton*) GetDlgItem(IDC_REST4);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (4):");
		polWnd = (CButton*) GetDlgItem(IDC_REST5);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (5):");
		polWnd = (CButton*) GetDlgItem(IDC_REST6);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (6):");
		polWnd = (CButton*) GetDlgItem(IDC_REST7);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (7):");
		polWnd = (CButton*) GetDlgItem(IDC_REST8);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (8):");
		polWnd = (CButton*) GetDlgItem(IDC_REST9);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (9):");
		polWnd = (CButton*) GetDlgItem(IDC_REST10);
		if (polWnd)	polWnd->SetWindowText("Centerline Dist to (10):");

		polWnd = (CButton*) GetDlgItem(IDC_M1);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M2);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M3);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M4);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M5);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M6);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M7);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M8);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M9);
		if (polWnd)	polWnd->SetWindowText("m");
		polWnd = (CButton*) GetDlgItem(IDC_M10);
		if (polWnd)	polWnd->SetWindowText("m");


	}

	if( ( (CRulesApp *)AfxGetApp() )->CanHandlePosMinMaxPax() == FALSE)
	{
		polWnd = (CButton*) GetDlgItem(IDC_POS_MINPAX);
		if (polWnd)	polWnd->ShowWindow(FALSE);
		polWnd = (CButton*) GetDlgItem(IDC_POS_MAXPAX);
		if (polWnd)	polWnd->ShowWindow(FALSE);

		polWnd = (CButton*) GetDlgItem(IDC_STATIC_ID_MINPAX);
		if (polWnd)	polWnd->ShowWindow(FALSE);
		polWnd = (CButton*) GetDlgItem(IDC_STATIC_ID_MAXPAX);
		if (polWnd)	polWnd->ShowWindow(FALSE);

	}

	// init groundtime
	m_Gt_Unit1.AddString("   ");
	m_Gt_Unit1.AddString("<");
	m_Gt_Unit1.AddString("<=");
	m_Gt_Unit1.AddString(">");
	m_Gt_Unit1.AddString(">=");
	m_Gt_Unit1.AddString("=");
	m_Gt_Unit1.SetCurSel(0);

	m_Gt_Unit2.AddString("   ");
	m_Gt_Unit2.AddString("<");
	m_Gt_Unit2.AddString("<=");
	m_Gt_Unit2.SetCurSel(1);

	CStringArray olArray;
	ExtractItemList(m_Gt_String, &olArray, ',');

	if (olArray.GetSize() == 5)
	{
		CString olTmp;	
		olTmp = olArray.GetAt(0);
		int ilpos = m_Gt_Unit1.FindStringExact(-1,olTmp);
		m_Gt_Unit1.SetCurSel(ilpos);

		olTmp = olArray.GetAt(1);
		m_Gt_Value1 = atoi(olTmp);

		olTmp = olArray.GetAt(2);
		if (olTmp == "and")
			m_Gt_AndCtrl.SetCheck(1);
		else
			m_Gt_AndCtrl.SetCheck(0);
		
		olTmp = olArray.GetAt(3);
		ilpos = m_Gt_Unit2.FindStringExact(-1,olTmp);
		m_Gt_Unit2.SetCurSel(ilpos);

		olTmp = olArray.GetAt(4);
		m_Gt_Value2 = atoi(olTmp);
	}

	
	UpdateData(FALSE);

	OnSelchangeComboGtUnit1();


	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1436), pomPst->Pnam);
	SetWindowText(olCaption);
/*
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclAlMatrix[64];
	GetPrivateProfileString("FIPS","AL_MATRIX_POS","FALSE", pclAlMatrix, sizeof pclAlMatrix, pclConfigPath);
	if(strcmp(pclAlMatrix, "TRUE") == 0)
*/
	bool blShowAL = true;
	bool blShowAC = true;
	bool blShowAP = true;
	CString omAlo = "POS";
	CString olName = omAlo + omAlo;
	ALODATA *polAloc = ogAloData.GetAloByName(olName);
	if (!polAloc)
	{
		blShowAL = false;
		blShowAC = false;
		blShowAP = false;
	}

	CStringArray olGrpArray;
	olGrpArray.Add("ACT");
	olGrpArray.Add("ALT");
	olGrpArray.Add("APT");

	for (int i = 0; i < olGrpArray.GetSize(); i++)
	{
		CString omGrp = olGrpArray.GetAt(i);
		olName = omAlo + omGrp;
		ALODATA *polAloc = ogAloData.GetAloByName(olName);
		if (!polAloc)
		{
			if (omGrp == "ALT")
				blShowAL = false;
			if (omGrp == "ACT")
				blShowAC = false;
			if (omGrp == "APT")
				blShowAP = false;
		}
	}

	for (i = 0; i < olGrpArray.GetSize(); i++)
	{
		CString omGrp = olGrpArray.GetAt(i);
		olName = omGrp + omGrp;
		polAloc = ogAloData.GetAloByName(olName);
		if (!polAloc)
		{
			if (omGrp == "ALT")
				blShowAL = false;
			if (omGrp == "ACT")
				blShowAC = false;
			if (omGrp == "APT")
				blShowAP = false;
		}
	}

	if(blShowAL)
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_ALMATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}
/*
	char pclApMatrix[64];
	GetPrivateProfileString("FIPS","AP_MATRIX_POS","FALSE", pclApMatrix, sizeof pclApMatrix, pclConfigPath);
	if(strcmp(pclApMatrix, "TRUE") == 0)
*/
	if (blShowAC)
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_MATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_MATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_MATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}
	
	if (blShowAP)
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_APMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CButton* polWnd = (CButton*) GetDlgItem(IDC_APMATRIX1);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX2);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX3);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX4);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX5);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX6);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX7);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX8);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX9);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = (CButton*) GetDlgItem(IDC_APMATRIX10);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	if (!ogFltiPrio.IsEmpty())
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_ID_ARR);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_FLTI_POS_ARR);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_STATIC_ID_DEP);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_FLTI_POS_DEP);
		if (polWnd)
			polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_ID_ARR);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_FLTI_POS_ARR);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_STATIC_ID_DEP);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_FLTI_POS_DEP);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	// 050228 MVy: show aircraft registration edit line depending on ceda configuration
	if( !( (CRulesApp *)AfxGetApp() )->CanHandleAcrRegn() )
	{
		int iCmd = SW_HIDE ;
		CWnd* pWnd = 0 ;

		pWnd = GetDlgItem( IDC_PST_RULES_ACR_REGN__CAPT );
		pWnd->ShowWindow( iCmd );
		
		pWnd = GetDlgItem( IDC_PST_RULES_ACR_REGN );
		pWnd->ShowWindow( iCmd );

		pWnd = GetDlgItem( IDC_PST_RULES_ACR_REGN__EXMP );
		pWnd->ShowWindow( iCmd );
	};


	if( !( (CRulesApp *)AfxGetApp() )->CanHandleFlno() )
	{
		int iCmd = SW_HIDE ;
		CWnd* pWnd = 0 ;

		pWnd = GetDlgItem( IDC_PST_RULES_FLNO__CAPT );
		pWnd->ShowWindow( iCmd );
		
		pWnd = GetDlgItem( IDC_PST_RULES_FLNO );
		pWnd->ShowWindow( iCmd );

	};




	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PstRuleDlg::OnALMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos1,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix2() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos2,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix3() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos3,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix4() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos4,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix5() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos5,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix6() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos6,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix7() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos7,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix8() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos8,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix9() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos9,"POS","ALT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnALMatrix10() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos10,"POS","ALT",this);
	olDlg.DoModal();
}

//##
void PstRuleDlg::OnAPMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos1,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix2() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos2,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix3() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos3,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix4() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos4,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix5() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos5,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix6() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos6,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix7() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos7,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix8() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos8,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix9() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos9,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnAPMatrix10() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos10,"POS","APT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos1,"POS","ACT",this);
	olDlg.DoModal();
}

void PstRuleDlg::OnMatrix2() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
//	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos2,this);
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos2,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix3() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos3,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix4() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos4,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix5() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos5,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix6() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos6,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix7() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos7,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix8() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos8,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix9() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos9,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix10() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos10,"POS","ACT",this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnSelchangeComboGtUnit1() 
{
	// TODO: Add your control notification handler code here

	CString olSelStr ("");
	int ilIdx = m_Gt_Unit1.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		m_Gt_Unit1.GetLBText( ilIdx, olSelStr );
		if (olSelStr == "   " || olSelStr == "<" || olSelStr == "<=" || olSelStr == "=")
		{
			m_Gt_AndCtrl.EnableWindow(FALSE);
			m_Gt_AndCtrl.SetCheck(0);
			m_Gt_Value2Ctrl.EnableWindow(FALSE);
			m_Gt_Unit2.EnableWindow(FALSE);

			if (olSelStr == "   ")
			{
				m_Gt_Value1 = 0;
				m_Gt_Value1Ctrl.SetWindowText("0");
			}
		}
		else
		{
			m_Gt_AndCtrl.EnableWindow(TRUE);
			if (m_Gt_AndCtrl.GetCheck())
			{
				m_Gt_Value2Ctrl.EnableWindow(TRUE);
				m_Gt_Unit2.EnableWindow(TRUE);
			}
			else
			{
				m_Gt_Value2Ctrl.EnableWindow(FALSE);
				m_Gt_Unit2.EnableWindow(FALSE);
			}
		}
	}
}

void PstRuleDlg::OnCheckGtAnd() 
{
	// TODO: Add your control notification handler code here
	
	if (m_Gt_AndCtrl.GetCheck())
	{
		m_Gt_Value2Ctrl.EnableWindow(TRUE);
		m_Gt_Unit2.EnableWindow(TRUE);
	}
	else
	{
		m_Gt_Value2Ctrl.EnableWindow(FALSE);
		m_Gt_Unit2.EnableWindow(FALSE);
	}
}

void PstRuleDlg::OnSelchangeComboGtUnit2() 
{
	// TODO: Add your control notification handler code here
	
	CString olSelStr ("");
	int ilIdx = m_Gt_Unit2.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		m_Gt_Unit2.GetLBText( ilIdx, olSelStr );
		if (olSelStr == "   ")
		{
			m_Gt_Value2 = 0;
			m_Gt_Value2Ctrl.SetWindowText("0");
		}
	}
}

void PstRuleDlg::OnACGroupSel()
{
	CAirCraftGroupSelDlg dlg(this, CString("ACTTAB"));
	//dlg.DoModal();
	if (dlg.DoModal() == IDOK)
	{
		m_ACGroup = dlg.omGrpStr;
		UpdateData(FALSE);
	}
}
