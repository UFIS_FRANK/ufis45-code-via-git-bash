// GrRules2.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "GrRules2.h"
#include "CedaNatData.h"
#include "NatureAllocation.h"
#include "DutyPreferences.h"
#include <iomanip.h>
#include <fstream.h>
#include <iostream.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern DutyPreferences *pogDutyPreferences;


/////////////////////////////////////////////////////////////////////////////
// GrRules2 property page

IMPLEMENT_DYNCREATE(GrRules2, CPropertyPage)

static int CompareTtyp(const NATDATA **e1, const NATDATA **e2)
{
	return strcmp((**e1).Ttyp, (**e2).Ttyp);
}

GrRules2::GrRules2() : CPropertyPage(GrRules2::IDD)
{
//	pomNatList = new GxGridTab(this);
	pomNatList = new CCSTable();

	//{{AFX_DATA_INIT(GrRules2)
	//}}AFX_DATA_INIT
}

GrRules2::~GrRules2()
{
	delete pomNatList;
}

void GrRules2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GrRules2)
	DDX_Control(pDX, IDC_GATPOS2TAB, m_GatPos2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GrRules2, CPropertyPage)
	//{{AFX_MSG_MAP(GrRules2)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnGridDblClk)
	ON_MESSAGE(CLK_BUTTON_PRINT,OnPrintNature)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GrRules2 message handlers

BOOL GrRules2::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	CStringArray olNatHeader;

	ogNatData.omData.Sort(CompareTtyp);
	for(int i = 0; i < ogNatData.omData.GetSize(); i++)
	{
		omNatUrnos.Add(ogNatData.omData[i].Urno);
	}
	
	CRect olRect;

	m_GatPos2.GetClientRect(olRect);
	
	pomNatList->SetShowSelection(FALSE);
	pomNatList->SetTableData(this, olRect.left +13 , olRect.right +13, olRect.top + 13, olRect.bottom +13);


	pomNatList->SetShowSelection(FALSE);
	
	
	omNatRulesViewer.Attach(pomNatList);
	UpdateView();


	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GrRules2::UpdateView()
{

//	CString olViewName = omViewer.GetViewName();
	CString olViewName = "<Default>";


	AfxGetApp()->DoWaitCursor(1);
	omNatRulesViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);
}



LONG GrRules2::OnGridDblClk(UINT wParam, LONG lParam)
{

	int ilCol = HIWORD(wParam);
	int ilRow = LOWORD(wParam);
		
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lParam;


	if(polNotify->SourceTable == pomNatList)
	{
		if(ilRow >= 0 && ilRow < omNatUrnos.GetSize())
		{
			long llUrno = (long)omNatUrnos[ilRow];
			NATDATA *polNat = ogNatData.GetNATByUrno(llUrno);
			if(polNat != NULL)
			{
				NatureAllocation olDlg(this, polNat);
				if(olDlg.DoModal() == IDOK)
				{
					UpdateView();
				}
			}
		}

	}


	return 0L;
}


BOOL GrRules2::OnKillActive() 
{
	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	
	
	return CPropertyPage::OnKillActive();
}


BOOL GrRules2::OnSetActive() 
{
	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	
	return CPropertyPage::OnSetActive();
}

void GrRules2::OnPrintNature()
{
	char pclExcelPath[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("FIPS", "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		AfxMessageBox("Unable to Find MSExcel");

	ofstream of;
	CString omTableName = "NatureRules";
	CString olFileName = omTableName;
	CString opTrenner = ",";

	CString HeadColu1 = "No.";
	CString HeadColu2 = "Nature";
	CString HeadColu3 = "Pos";
	CString HeadColu4 = "Gate";
	CString HeadColu5 = "Baggage Belt";
	CString HeadColu6 = "Lounge";

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;
	
	of  << setw(ilwidth) << HeadColu1
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu2
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu4
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu5
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu6
		<< endl; 
	
	int ilCount = omNatRulesViewer.omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		GR2TABLE_LINEDATA *polPst = &omNatRulesViewer.omLines.GetAt(i);
		if(polPst != NULL)
		{
			CString NatureNo ;
			NatureNo.Format(_T("%d"),polPst->Rtnr);
			of.setf(ios::left, ios::adjustfield);
			of  << setw(1) << NatureNo 
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Nature
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Pos
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Gate
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Belt
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Wro
				<< endl;
		}
	}

	of.close();

	char pclTmp[256] ; 
	omFileName.TrimLeft();
	omFileName.TrimRight();
	strcpy(pclTmp ,omFileName);
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		AfxMessageBox("The EXCEL path in Ceda.ini is not proper.Please correct it");
}

