#ifndef _CEDAGPMDATA_H_
#define _CEDAGPMDATA_H_

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////
struct GPMDATA {

	CTime 	 Cdat; 	// Erstellungsdatum
	int 	 Didu; 	// Schwierigkeit der Regel (Member)
	char 	 Disp[3]; 	// Disponierbar (J/N)
	int 	 Dtim; 	// Einstzzeit
	char 	 Dtyp[3]; 	// Einsatzart
	char 	 Duch[3]; 	// Einsatzkette aufbauen
	int 	 Etot; 	// Fr�hste Auftrags�bernahme
	long 	 Ghsu; 	// Eindeutige Datensatz-Nr. vom Ground Handl. Service
	char 	 Lkco[5]; 	// Code (Kurzname)(1-n)
	CTime 	 Lstu; 	// Datum letzte �nderung
	int 	 Noma; 	// Anzahl der Mitarbeiter
	char 	 Perm[258]; 	// Notwendige Qualifikationen
	char 	 Pfkt[258]; 	// Notwendige Funktionen (GPE)
	int 	 Pota; 	// Nacharbeit
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	int 	 Prio; 	// Priorit�t der Regel (Member)
	int 	 Prta; 	// R�stzeit
	long 	 Rkey; 	// Eindeutige Datensatz-Nr. der Premisse
	char	 Rtyp[2];//Resourcentyp
	int 	 Tfdu; 	// Wegzeit vom Einsatz
	int 	 Ttdu; 	// Wegzeit zum Einsatz
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 	// G�ltig von
	CTime 	 Vato; 	// G�ltig bis
	char 	 Vrgc[258]; 	// G�ltige Ressourcengruppe
	char  Rema[256];//Fids Remarks
	CTime Dube;
	CTime Duen;
	int  IsChanged;	// Check whether Data has Changed f�r Relaese
	bool IsMarked;

	GPMDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged=DATA_UNCHANGED;
		Cdat = TIMENULL;Lstu = TIMENULL;
		Vafr = TIMENULL;Vato = TIMENULL;
		IsMarked = false;
	}

	
 };	


// the broadcast CallBack function, has to be outside the CedaGpmData class


class CedaGpmData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<GPMDATA> omData;
    CCSPtrArray<GPMDATA> omSelectedData;
    CMapPtrToPtr omUrnoMap;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaGpmData();
	~CedaGpmData();

	CTime omOnBlock;
	CTime omOfBlock;
	bool Read(char *pspWhere = NULL);
	
    bool Insert(const GPMDATA *prpGhs);    // used in PrePlanTable only
    bool Update(const GPMDATA *prpGhs);    // used in PrePlanTable only
	void ProcessGpmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ClearAll();

	GPMDATA * GetGpmByUrno(long lpUrno);
	void GetGpmByRkey(CCSPtrArray<GPMDATA> &ropGpm, long lpRkey);

	void InsertInternal(GPMDATA *prpGpm, bool bpWithDDX = true);
	void UpdateInternal(GPMDATA *prpGpm, bool bpWithDDX = true);
	void DeleteInternal(GPMDATA *prpGpm, bool bpWithDDX = true);

	bool ChangeJobTime(long lpUrno, CTime opNewStart, CTime opNewEnd);
    bool Insert(GPMDATA *prpGhs, BOOL bpWithSave = true);
    bool Update(GPMDATA *prpGhs, BOOL bpWithSave = true);
	bool Delete(GPMDATA *prpGhs, BOOL bpWithSave = true);
	bool Save(GPMDATA *prpGhs);
	CString GetFieldList();
	bool IsDataChanged(GPMDATA *prpOld, GPMDATA *prpNew);
};

extern CedaGpmData ogGpmData;

#endif //_CEDAGPMDATA_H_
