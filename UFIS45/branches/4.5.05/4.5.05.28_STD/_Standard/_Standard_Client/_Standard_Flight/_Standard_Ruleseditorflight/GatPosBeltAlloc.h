#if !defined(AFX_GATPOSBELTALLOC_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATPOSBELTALLOC_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosBeltAlloc.h : header file
//
#include "CcsTable.h"
#include "BlaRulesViewer.h"
#include "resource.h"
#include "resrc1.h"
/////////////////////////////////////////////////////////////////////////////
// GatPosRules dialog


class GatPosBeltAlloc : public CPropertyPage
{
	DECLARE_DYNCREATE(GatPosBeltAlloc)

// Construction
public:
	GatPosBeltAlloc();
	~GatPosBeltAlloc();

// Dialog Data
	//{{AFX_DATA(GatPosRules)
	enum { IDD = IDD_RULES_BELT_ALLOC };
	CStatic	m_BlaAllocRuleListTable;
	//}}AFX_DATA

	CCSTable *pomBlaAllocList;

	BlaRulesViewer omBlaRulesViewer;

	CUIntArray omBlaUrnos;

	void MakeBlaLineData(BLADATA *popBla, CStringArray &ropArray);
	void GatPosBeltAlloc::UpdateView();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GatPosBeltAlloc)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	void OnPrintBeltAlloc();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	int omBlaContextItem;
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GatPosBeltAlloc)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnGridDblClk(UINT wParam, LONG lParam);
	LONG GatPosBeltAlloc::OnNewButton(WPARAM wParam, LPARAM lParam);
	LONG OnUpdateButton(WPARAM wParam, LPARAM lParam);
	LONG OnDeleteButton(WPARAM wParam, LPARAM lParam);
	LONG OnCopyButton(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSBELTALLOC_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)

