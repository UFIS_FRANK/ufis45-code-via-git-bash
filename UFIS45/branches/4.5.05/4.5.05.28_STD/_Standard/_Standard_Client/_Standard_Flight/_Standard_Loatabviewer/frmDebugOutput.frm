VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDebugOutput 
   Caption         =   "Debug Output"
   ClientHeight    =   7860
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   10590
   LinkTopic       =   "Form1"
   ScaleHeight     =   7860
   ScaleWidth      =   10590
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView output 
      Height          =   2895
      Left            =   1320
      TabIndex        =   0
      Top             =   1320
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   5106
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmDebugOutput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    output.Appearance = ccFlat
    output.BorderStyle = ccNone
    output.View = lvwReport
    output.GridLines = True
    output.FullRowSelect = True
    output.Font.Name = "Courier New"
End Sub

Private Sub Form_Resize()
    output.Left = 0
    output.Top = 0
    output.Width = Me.ScaleWidth
    output.height = Me.ScaleHeight
End Sub

Public Sub DumpTable(DataTab As TABLib.TAB)
    Dim rowCount As Long
    Dim columns() As String
    Dim column As Variant
    Dim li As ListItem
    Dim i As Long, j As Long
        
    output.HideColumnHeaders = False
    output.ListItems.Clear
    output.ColumnHeaders.Clear
    
    columns = Split(Split(DataTab.Tag, ";")(1), ",")
    
    output.ColumnHeaders.Add , , "Nr."
    For Each column In columns
        output.ColumnHeaders.Add , , column
    Next
    
    For i = 0 To DataTab.GetLineCount - 1
        Set li = output.ListItems.Add(, , CStr(i))
        For j = 0 To UBound(columns)
            li.SubItems(j + 1) = DataTab.GetFieldValue(i, columns(j))
        Next
    Next
    Show
End Sub

Public Sub DumpChanges(changes As Collection)
    Dim values() As String
    Dim change As Variant
    Dim li As ListItem
    Dim i As Long
    
    output.HideColumnHeaders = False
    output.ListItems.Clear
    output.ColumnHeaders.Clear
    output.ColumnHeaders.Add , , "Change"
    output.ColumnHeaders.Add , , "URNO"
    output.ColumnHeaders.Add , , "FLNU"
    output.ColumnHeaders.Add , , "DSSN"
    output.ColumnHeaders.Add , , "TYPE"
    output.ColumnHeaders.Add , , "STYP"
    output.ColumnHeaders.Add , , "SSTP"
    output.ColumnHeaders.Add , , "SSST"
    output.ColumnHeaders.Add , , "VALU"
    
    For Each change In changes
        values = Split(Replace(change, "#", " "), ";")
        Set li = output.ListItems.Add(, , values(0))
        values = Split(values(1), ",")
        For i = 0 To UBound(values)
            li.SubItems(i + 1) = values(i)
        Next
    Next
End Sub

Public Sub DumpText(debugText As String)
    Dim line As Variant
    
    output.HideColumnHeaders = True
    output.ListItems.Clear
    output.ColumnHeaders.Clear
    output.ColumnHeaders.Add , , "Lines"
    
    For Each line In Split(debugText, vbLf)
        output.ListItems.Add , , line
    Next
End Sub
