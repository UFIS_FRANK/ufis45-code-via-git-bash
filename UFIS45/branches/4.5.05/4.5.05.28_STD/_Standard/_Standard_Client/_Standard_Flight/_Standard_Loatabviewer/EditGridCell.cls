VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EditGridCell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarCellType As String
Private mvarDefinition As String
Private mvarRow As String
Private mvarColumn As String
Private mvarTypeKey As String
Private mvarComputeSteps As Collection
Private mvarPriorities As String
Private mvarLastValueSource As String
Private mvarValue As String
Private mvarPreviousValue As String
Private mvarGrid As EditGrid
Private mvarURNO As String

'igu on 05 Apr 2010
Private mvarAutosave As Boolean
Private mvarSaveToExtTable As String

Public Sub Clear()
    mvarLastValueSource = ""
    mvarValue = ""
    mvarPreviousValue = ""
End Sub

Public Property Let Definition(ByVal vData As String)
    Dim definitions() As String
    Dim definitions_1() As String
    Dim i As Long
    mvarDefinition = vData
    
    definitions = Split(mvarDefinition, ";")
    mvarCellType = definitions(0)
    
    If mvarCellType = "Interface" Or mvarCellType = "User" Then
        mvarTypeKey = definitions(1)
        If mvarCellType = "Interface" Then
            If UBound(definitions) < 1 Then
                mvarPriorities = definitions(2)
            End If
        Else
            mvarPriorities = "USR"
        End If
    Else
        mvarPriorities = ""
        If mvarCellType = "Compute" Then
            'Changed on 15/08/2006 to get the celltype
            If InStr(mvarDefinition, ";User") Then
                definitions_1 = Split(mvarDefinition, "User;")
                mvarTypeKey = definitions_1(1)
                ' Changed on 21/08/2006
                mvarPriorities = "ComputeStore"
            End If
            
            Set mvarComputeSteps = New Collection
            For i = 1 To UBound(definitions)
                'Changed on 15/08/2006 to get the celltype
                If InStr(definitions(i), "User") Then
                    Exit For
                End If
                mvarComputeSteps.Add definitions(i)
            Next
        End If
    End If
End Property

Public Property Get Definition() As String
    Definition = mvarDefinition
End Property

Public Property Let ParentGrid(grid As EditGrid)
    Set mvarGrid = grid
End Property

Public Property Get CellType() As String
    CellType = mvarCellType
End Property

Public Property Get TypeKey() As String
    TypeKey = mvarTypeKey
End Property

Public Property Let row(ByVal vData As String)
    mvarRow = vData
End Property

Public Property Get row() As String
    row = mvarRow
End Property

Public Property Let column(ByVal vData As String)
    mvarColumn = vData
End Property

Public Property Get column() As String
    column = mvarColumn
End Property

Public Property Get ComputeSteps() As Collection
    Set ComputeSteps = mvarComputeSteps
End Property

Public Function SetValue(source As String, Value As String) As Boolean
    Dim curValuePriority As Long
    Dim newValuePriority As Long
    Dim Priorities() As String
    Priorities = Split(mvarPriorities, ",")
    
    If mvarCellType = "Interface" Then
        For curValuePriority = 0 To UBound(Priorities)
            If Priorities(curValuePriority) = mvarLastValueSource Then Exit For
        Next
        For newValuePriority = 0 To UBound(Priorities)
            If Priorities(newValuePriority) = source Then Exit For
        Next
        If newValuePriority <= curValuePriority And _
           newValuePriority <= UBound(Priorities) _
        Then
            SetValue = True
        Else
            SetValue = False
        End If
    Else
        If mvarCellType = "Compute" And source <> "Compute" Then
            SetValue = False
        Else
            SetValue = True
        End If
    End If
    If SetValue Then
        If Value <> "" Then
            'kkh - changed on 30/08/2006
            'mvarValue = Abs(Val(Value))
            mvarValue = Fix(Val(Value))
        Else
            mvarValue = Value
        End If
        mvarLastValueSource = source
        UpdateGrid
    End If
End Function

Public Sub ResetState()
    mvarPreviousValue = mvarValue
    ApplyCellStyle
End Sub

Public Sub UpdateGrid()
    mvarGrid.TabControl.SetFieldValues Val(mvarRow) - 1, mvarColumn, mvarValue
    ApplyCellStyle
End Sub

Public Property Get State() As String
    If mvarPreviousValue = mvarValue Then
        State = "Unchanged"
    Else
        If mvarValue = "" Then
            State = "Deleted"
        Else
            If mvarPreviousValue = "" Then
                State = "Inserted"
            Else
                State = "Modified"
            End If
        End If
    End If
End Property

Public Property Get Value() As String
    Value = mvarValue
End Property

Public Property Get LastValueSource() As String
    LastValueSource = mvarLastValueSource
End Property

Public Property Let Priorities(newPriorities As String)
    mvarPriorities = newPriorities
End Property

Public Property Get Priorities() As String
    Priorities = mvarPriorities
End Property

Public Sub ApplyCellStyle()
    Dim style As String
    If mvarCellType = "User" Then
        style = State
    Else
        style = mvarCellType
    End If
    mvarGrid.TabControl.SetCellProperty Val(mvarRow) - 1, mvarGrid.GetGridColumnIndex(mvarColumn), style
    If LoatabEditSetup And InStr(mvarTypeKey, "NA") Then
        mvarGrid.TabControl.SetFieldValues Val(mvarRow) - 1, mvarColumn, "!cfg!"
    End If
End Sub

Public Property Let URNO(newUrno As String)
    mvarURNO = newUrno
End Property

Public Property Get URNO() As String
    URNO = mvarURNO
End Property

'igu on 05 Apr 2010
'add two new properties
'1. autosave when load -> yes/no
'2. save to ext table -> config (blank means no)
Public Property Let AutoSave(ByVal newAutosave As Boolean)
    mvarAutosave = newAutosave
End Property

Public Property Get AutoSave() As Boolean
    AutoSave = mvarAutosave
End Property

Public Property Let SaveToExtTable(ByVal newSaveToExtTable As String)
    mvarSaveToExtTable = newSaveToExtTable
End Property

Public Property Get SaveToExtTable() As String
    SaveToExtTable = mvarSaveToExtTable
End Property

