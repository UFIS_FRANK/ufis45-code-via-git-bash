VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EditGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"EditGridCells"
Option Explicit

Private mvarCells As EditGridCells
Private mvarIniName As String
Private mvarDisplayName As String
Private mvarColumns As Collection
Private mvarColumnNames As Collection
Private mvarRows As Collection
Private mvarRowNames As Collection
Private mvarRowLabels As Boolean
Private mvarSize As String
Private mvarFrame As Frame
Private WithEvents mvarTab As TABLib.TAB
Attribute mvarTab.VB_VarHelpID = -1
Private mvarPriorities As String
Private mvarLastSelectedColumn As Long
Private mvarLastSelectedRow As Long
Private mvarFontSize As String
Private mvarWidth As Long

Private Sub Class_Initialize()
    Set mvarCells = New EditGridCells
    Set mvarCells.ParentGrid = Me
    Set mvarColumns = New Collection
    Set mvarColumnNames = New Collection
    Set mvarRows = New Collection
    Set mvarRowNames = New Collection
End Sub

Public Sub Create(parentForm As Form)
    Set mvarFrame = parentForm.Controls.Add("VB.Frame", mvarIniName & "Frame")
    mvarFrame.Caption = mvarDisplayName
    mvarFrame.Font.Bold = True
    mvarFrame.Visible = True
    
    Set mvarTab = parentForm.Controls.Add("TAB.TABCtrl.1", mvarIniName & "Tab", mvarFrame)
    mvarTab.Left = 90
    mvarTab.Top = 220
    
    PrepareTab
End Sub

Public Sub InitFromConfig(iniFile As String, fontSize As String)
    Dim section As String
    Dim column As Variant
    Dim row As Variant
    Dim values() As String
    Dim cell As EditGridCell
    
    Dim strAutoSaveConfig As String 'igu on 05 Apr 2010
    Dim strSaveToExtTableConfig As String 'igu on 05 Apr 2010
    Dim blnAutoSave As Boolean 'igu on 05 Apr 2010
    Dim strSaveToExtTable As String 'igu on 05 Apr 2010
    Dim intLoc As Integer 'igu on 05 Apr 2010
    Dim intLocEnd As Integer 'igu on 05 Apr 2010
    Dim strFind As String 'igu on 05 Apr 2010
    
    section = "EDIT_" & mvarIniName
       
    mvarFontSize = fontSize
    mvarDisplayName = GetIniEntry(iniFile, section, "", "NAME", "")
    mvarSize = GetIniEntry(iniFile, section, "", "SIZE", "")
    mvarPriorities = GetIniEntry(iniFile, section, "", "INTERFACE_PRIORITY", "")
    
    strAutoSaveConfig = GetIniEntry(iniFile, section, "", "AUTOSAVE_CELLS", "") & "," 'igu on 05 Apr 2010
    strSaveToExtTableConfig = GetIniEntry(iniFile, section, "", "SAVE_TO_EXT_TABLE", "") & ";" 'igu on 05 Apr 2010
    
    If GetIniEntry(iniFile, section, "", "ROW_LABELS", "") = "YES" Then
        mvarRowLabels = True
    Else
        mvarRowLabels = False
    End If
    
    For Each column In Split(GetIniEntry(iniFile, section, "", "COLUMNS", ""), ",")
        values = Split(column, ":")
        mvarColumns.Add values(0)
        mvarColumnNames.Add values(1), values(0)
    Next

    For Each row In Split(GetIniEntry(iniFile, section, "", "ROWS", ""), ",")
        values = Split(row, ":")
        mvarRows.Add values(0)
        mvarRowNames.Add values(1), values(0)
    Next
    
    For Each row In mvarRows
        For Each column In mvarColumns
            'mvarCells.Add (column), (row), GetIniEntry(iniFile, section, "", "CELL_" & column & row, ""), mvarPriorities 'igu on 05 Apr 2010
            
            '-----------
            'igu on 05 Apr 2010
            'start
            '-----------
            strFind = column & row & ","
            blnAutoSave = (InStr(strAutoSaveConfig, strFind) > 0)
            
            strFind = column & row & ":"
            intLoc = InStr(strSaveToExtTableConfig, strFind)
            If intLoc > 0 Then
                intLoc = intLoc + Len(strFind)
                intLocEnd = InStr(intLoc, strSaveToExtTableConfig, ";")
                strSaveToExtTable = Mid(strSaveToExtTableConfig, intLoc, intLocEnd - intLoc)
            Else
                strSaveToExtTable = ""
            End If
            
            mvarCells.Add (column), (row), _
                GetIniEntry(iniFile, section, "", "CELL_" & column & row, ""), _
                mvarPriorities, blnAutoSave, strSaveToExtTable
            '-----------
            'end
            '-----------
        Next
    Next
End Sub

Public Sub Resize(Left As Long, Top As Long, Width As Long, height As Long)
    mvarFrame.Left = Left
    mvarFrame.Top = Top
    If Width > 150 Then
        mvarFrame.Width = Width
    End If
    If height > 350 Then
        mvarFrame.height = height
    End If
    
    mvarTab.Width = mvarWidth / 96 * 1440
    mvarTab.height = mvarFrame.height - 310
End Sub

Private Sub PrepareTab()
    Dim Fields As String
    Dim header As String
    Dim alignment As String
    Dim Width As String
    Dim columnWidth As Long
    Dim item As Variant
    Dim first As Boolean
    
    mvarTab.ResetContent
    mvarTab.HeaderFontSize = mvarFontSize
    mvarTab.fontSize = mvarFontSize
    mvarTab.LineHeight = mvarFontSize
    mvarTab.SetTabFontBold MyFontBold
'    mvarTab.ShowHorzScroller True
    mvarTab.EnableInlineEdit False
    mvarTab.CreateCellObj "Label", 12632256, 0, mvarFontSize, False, False, MyFontBold, 0, ""
    mvarTab.CreateCellObj "Interface", 14737632, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "Compute", 12648447, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "None", mvarTab.EmptyAreaRightColor, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "Unchanged", 16777215, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "Deleted", 8421631, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "Inserted", 12648384, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.CreateCellObj "Modified", 16777152, 0, mvarFontSize, False, False, False, 0, ""
    mvarTab.EmptyAreaBackColor = mvarTab.EmptyAreaRightColor
    mvarTab.GridLineColor = mvarTab.EmptyAreaRightColor
    mvarTab.ShowRowSelection = False
    mvarTab.FontName = "Courier New"
    
    If mvarRowLabels Then
        mvarTab.SetColumnProperty 0, "Label"
        mvarTab.NoFocusColumns = "LABEL"
    End If
    
    first = True
    For Each item In mvarColumns
        If first Then
            first = False
            Fields = item
        Else
            Fields = Fields & "," & item
        End If
    Next
    If mvarRowLabels Then Fields = "LABEL," & Fields
    
    mvarWidth = 0
    columnWidth = mvarFontSize * 6
    first = True
    For Each item In mvarColumnNames
        If first Then
            first = False
            header = item
            Width = columnWidth
            alignment = "C"
        Else
            header = header & "," & item
            Width = Width & "," & columnWidth
            alignment = alignment & "," & "C"
        End If
        mvarWidth = mvarWidth + columnWidth
    Next
    If mvarRowLabels Then
        header = "          ," & header
        Width = columnWidth & "," & Width
        alignment = "L," & alignment
        mvarWidth = mvarWidth + columnWidth
    End If
    
    mvarTab.HeaderString = header
    mvarTab.HeaderAlignmentString = alignment
    mvarTab.ColumnAlignmentString = alignment
    mvarTab.HeaderLengthString = Width
'    mvarTab.ColumnWidthString = Width
    mvarTab.LogicalFieldList = Fields
'    mvarTab.AutoSizeByHeader = True
'    mvarTab.AutoSizeColumns

    mvarTab.Visible = True
End Sub

Public Sub LoadData(DataTab As TABLib.TAB)
    Dim URNO As String
    Dim source As String
    Dim TypeKey As String
    Dim Value As String
    Dim i As Long, j As Long
    Dim cell As EditGridCell
    Dim blankRow As String
    Dim APC3 As String    '------Added By Sai#####(for Joining of Multisector Departure flights(12,Jul,2005)

'    frmDebugOutput.DumpTable DataTab
    
    mvarTab.ResetContent
    mvarCells.ClearAll
    
    blankRow = ""
    For i = 0 To mvarColumns.Count - 1
        blankRow = blankRow & ","
    Next
    If mvarRowLabels Then blankRow = blankRow & ","
    For i = 0 To mvarRows.Count - 1
        mvarTab.InsertTextLine blankRow, True
    Next
        
    If mvarRowLabels Then
        For i = 0 To mvarRowNames.Count - 1
            mvarTab.SetFieldValues i, "LABEL", mvarRowNames(i + 1)
        Next
    End If
  
    For i = 0 To DataTab.GetLineCount - 1
        URNO = DataTab.GetFieldValue(i, "URNO")
        source = DataTab.GetFieldValue(i, "DSSN")
        TypeKey = DataTab.GetFieldValues(i, "TYPE,STYP,SSTP,SSST")
        Value = DataTab.GetFieldValue(i, "VALU")
 '------By Sai#####(for Joining of Multisector Departure flights(12,Jul,2005)----------------
        APC3 = DataTab.GetFieldValue(i, "APC3")
         If (APC3 = "" Or APC3 = Null) Then
             mvarCells.InsertValue URNO, source, TypeKey, Value
        End If
 '----------####End here#########------------------------------------------------------------
    Next
    mvarCells.ComputeAll
    mvarCells.ResetState
    mvarCells.UpdateGrid
    mvarTab.Refresh
End Sub

Public Sub GetChanges(changes As Collection)
    mvarCells.GetChanges changes
End Sub
'kkh to insert / update the lastest figures for Total on board for Departure flight
'Public Sub GetInitialChanges(changes As Collection) 'igu on 10 Nov 2009
'Public Sub GetInitialChanges(changes As Collection, ByVal RowNumber As Integer) 'igu on 10 Nov 2009 'igu on 05 Apr 2010
Public Sub GetInitialChanges(changes As Collection) 'igu on 05 Apr 2010
    'mvarCells.GetInitialChanges changes 'igu on 10 Nov 2009
    'mvarCells.GetInitialChanges changes, RowNumber 'igu on 10 Nov 2009 'igu on 05 Apr 2010
    mvarCells.GetInitialChanges changes 'igu on 05 Apr 2010
End Sub

Public Function GetGridColumnIndex(column As String) As Long
    Dim i As Long
    Dim item As Variant
    
    i = 0
    For Each item In mvarColumns
        If item = column Then Exit For
        i = i + 1
    Next
    If mvarRowLabels Then i = i + 1
    GetGridColumnIndex = i
End Function

Public Function GetGridRowIndex(row As String) As Long
    Dim i As Long
    Dim item As Variant
    
    i = 0
    For Each item In mvarRows
        If item = row Then Exit For
        i = i + 1
    Next
    GetGridRowIndex = i
End Function

Public Function GetCellColumn(ByVal column As Long) As String
    Dim item As Variant
    Dim i As Long
    If mvarRowLabels Then column = column - 1
    i = 0
    For Each item In mvarColumns
        If i = column Then Exit For
        i = i + 1
    Next
    GetCellColumn = item
End Function

Public Function GetCellRow(ByVal row As Long) As String
    GetCellRow = row + 1
End Function

Public Property Get Rows() As Collection
    Set Rows = mvarRows
End Property

Public Property Get RowNames() As Collection
    Set RowNames = mvarRowNames
End Property

Public Property Get columns() As Collection
    Set columns = mvarColumns
End Property

Public Property Get ColumnNames() As Collection
    Set ColumnNames = mvarColumnNames
End Property

Public Property Let IniName(ByVal vData As String)
    mvarIniName = vData
End Property

Public Property Get IniName() As String
    IniName = mvarIniName
End Property

Public Property Get DisplayName() As String
    DisplayName = mvarDisplayName
End Property

Public Property Get Size() As String
    Size = mvarSize
End Property

Public Property Get Cells() As EditGridCells
    Set Cells = mvarCells
End Property

Public Property Get TabControl() As TABLib.TAB
    Set TabControl = mvarTab
End Property

Public Sub SetNextEditableCell(direction As String)
    Dim newCell As EditGridCell
    Dim lastCell As EditGridCell
    Dim item As Variant
    Dim i As Long
    Dim cellFound As Boolean
    cellFound = False
    Set lastCell = mvarCells(GetCellColumn(mvarLastSelectedColumn) & GetCellRow(mvarLastSelectedRow))
    
    If direction = "UP" Then
        For i = mvarLastSelectedRow To 0 Step -1
            Set newCell = mvarCells(lastCell.column & GetCellRow(i))
            If newCell.CellType = "User" And GetGridRowIndex(newCell.row) < GetGridRowIndex(lastCell.row) Then
                cellFound = True
                Exit For
            End If
        Next
    End If
    If direction = "DOWN" Then
        For i = mvarLastSelectedRow To mvarTab.GetLineCount - 1
            Set newCell = mvarCells(lastCell.column & GetCellRow(i))
            If newCell.CellType = "User" And GetGridRowIndex(newCell.row) > GetGridRowIndex(lastCell.row) Then
                cellFound = True
                Exit For
            End If
        Next
    End If
    If direction = "LEFT" Then
        For i = mvarLastSelectedColumn To 0 Step -1
            Set newCell = mvarCells(GetCellColumn(i) & lastCell.row)
            If newCell.CellType = "User" And GetGridColumnIndex(newCell.column) < GetGridColumnIndex(lastCell.column) Then
                cellFound = True
                Exit For
            End If
        Next
    End If
    If direction = "RIGHT" Then
        For i = mvarLastSelectedColumn To mvarTab.GetColumnCount - 1
            Set newCell = mvarCells(GetCellColumn(i) & lastCell.row)
            If newCell.CellType = "User" And GetGridColumnIndex(newCell.column) > GetGridColumnIndex(lastCell.column) Then
                cellFound = True
                Exit For
            End If
        Next
    End If
    If cellFound Then
        mvarTab.EnableInlineEdit False
        mvarTab.EnableInlineEdit True
        mvarTab.SetInplaceEdit GetGridRowIndex(newCell.row), GetGridColumnIndex(newCell.column), False
        mvarLastSelectedColumn = GetGridColumnIndex(newCell.column)
        mvarLastSelectedRow = GetGridRowIndex(newCell.row)
    Else
        mvarTab.EnableInlineEdit False
        mvarTab.EnableInlineEdit True
        mvarTab.SetInplaceEdit mvarLastSelectedRow, mvarLastSelectedColumn, False
    End If
End Sub

Private Sub mvarTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim cell As EditGridCell
    Set cell = mvarCells(GetCellColumn(ColNo) & GetCellRow(LineNo))
    cell.SetValue "USR", mvarTab.GetColumnValue(LineNo, ColNo)
    mvarCells.ComputeAll
    mvarTab.Refresh
End Sub

Private Sub mvarTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    Dim direction As String
    Dim cell As EditGridCell
    Set cell = mvarCells(GetCellColumn(ColNo) & GetCellRow(LineNo))
    If cell.CellType = "User" Then
        mvarTab.EnableInlineEdit True
        mvarLastSelectedColumn = ColNo
        mvarLastSelectedRow = LineNo
    Else
        If ColNo < mvarLastSelectedColumn Then direction = "LEFT"
        If ColNo > mvarLastSelectedColumn Then direction = "RIGHT"
        If LineNo < mvarLastSelectedRow Then direction = "UP"
        If LineNo > mvarLastSelectedRow Then direction = "DOWN"
        SetNextEditableCell direction
    End If
End Sub

Private Sub mvarTab_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim cell As EditGridCell
    Set cell = mvarCells(GetCellColumn(ColNo) & GetCellRow(LineNo))
    cell.SetValue "USR", NewValue
    mvarCells.ComputeAll
    mvarTab.Refresh
End Sub

Private Sub mvarTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim cell As EditGridCell
    If LoatabEditReadOnly Then Exit Sub
    If LineNo < 0 Or ColNo < 0 Then Exit Sub
    If mvarRowLabels And ColNo = 0 Then Exit Sub
    Set cell = mvarCells(GetCellColumn(ColNo) & GetCellRow(LineNo))
    If cell.CellType = "User" Then
        mvarTab.EnableInlineEdit True
        mvarLastSelectedColumn = ColNo
        mvarLastSelectedRow = LineNo
    Else
        mvarTab.EnableInlineEdit False
    End If
End Sub

Private Sub mvarTab_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim message As String
    Dim cell As EditGridCell
    If LineNo < 0 Or ColNo < 0 Then Exit Sub
    If mvarRowLabels And ColNo = 0 Then Exit Sub
    
    Set cell = mvarCells(GetCellColumn(ColNo) & GetCellRow(LineNo))
    message = "Source: " & vbTab & cell.LastValueSource & vbLf
    message = message & "Type: " & vbTab & Replace(cell.TypeKey, "#", "") & vbLf
    
    If LoatabEditSetup Then
        message = message & vbLf & "Debugging Information:" & vbLf & vbLf
        message = message & "URNO:" & vbTab & cell.URNO & vbLf
        message = message & "Value:" & vbTab & cell.Value & vbLf
        message = message & "Config:" & vbTab & cell.Definition & vbLf
        message = message & "Priority:" & vbTab & cell.Priorities & vbLf
        message = message & "Cell:" & vbTab & cell.column & cell.row & vbLf
        message = message & "Section:" & vbTab & mvarIniName & vbLf
        MsgBox message, , frmMain.Caption
    Else
        If cell.CellType = "Interface" Then
            MsgBox message, , frmMain.Caption
        End If
    End If
End Sub

Private Sub Class_Terminate()
    Set mvarCells = Nothing
End Sub

Public Property Get Width() As Long
    Width = mvarWidth
End Property
