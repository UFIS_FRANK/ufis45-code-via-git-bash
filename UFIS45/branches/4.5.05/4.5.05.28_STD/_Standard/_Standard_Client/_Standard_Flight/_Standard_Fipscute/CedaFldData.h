// HEADER 
#ifndef _CEDAFLDDATA_H_ 
#define _CEDAFLDDATA_H_ 

#include "CCSDefines.h"
#include "CCSCedaData.h" 


enum ResTypes{ResNONE, ResBLT, ResGAT, ResCKI, ResPos, ResWro};

#define DBFIELDLEN_RNAM 32
#define DBFIELDLEN_RTYP 12
#define DBFIELDLEN_CREC 12
#define DBFIELDLEN_RTAB 3
#define DBFIELDLEN_CTYP 8
#define DBFIELDLEN_USEC 32
#define DBFIELDLEN_USEU 32

struct FLDDATA 
{
	CTime	Abti;		// actual boarding time
	CTime	Afti;		// actual final call
	long	Aurn;		// Urno der Afttab oder Ccatab
	CTime	Cdat;		// creation date
	char	Crec[DBFIELDLEN_CREC + 1];	// Counter remark code
	char	Ctyp[DBFIELDLEN_CTYP + 1];	// Codeshare type
	long	Dseq;		// Display sequence
	CTime	Lstu;		// time of last update
	char	Rnam[DBFIELDLEN_RNAM + 1];	// Ressource Name
	char	Rtab[DBFIELDLEN_RTAB + 1];	// Related table
	char	Rtyp[DBFIELDLEN_RTYP + 1];	// Ressource Type ('CKIF' fuer Checkin-Counters, 'GTD1' fuer Gates)
	long	Rurn;		// Related urno
	long	Urno;		// Eindeutige Datensatz-Nr.
	char	Usec[DBFIELDLEN_USEC + 1];	// user which create the record
	char	Useu[DBFIELDLEN_USEU + 1];	// user which last updated the record

	ResTypes	ResType;	// Ressource Type

	FLDDATA(void)
	{ 
		Clear();
	}

	void Clear(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno = 0;
		Aurn = 0;
		Rurn = 0;
		Abti = TIMENULL;
		Afti = TIMENULL;
		Dseq = -1;
		ResType = ResNONE;
	}

	const FLDDATA &operator=(const FLDDATA &ropFldData);

}; // end FLDDATA


class CedaFldData : public CCSCedaData
{
public:
	CedaFldData();
	~CedaFldData();

	//CCSPtrArray<FLDDATA> omData;
	FLDDATA rmCuteFldData;
    //CMapPtrToPtr omUrnoMap;
	char pcmFList[512];

	bool ReadCuteRecord(const CString &ropName, ResTypes ipResType, long lpDseq, bool bpDdx = true);

	//void Read( long lpFlightUrno);
	void Clear();

	FLDDATA *GetFld(long lpUrno);
	FLDDATA *GetCuteRecord();

	//////////////////////////////////////

	bool SaveCuteRecord(const CString *popAddWhere = NULL);

	//void DeleteInternal(FLDDATA *prpFld);
	//void InsertInternal(FLDDATA *prpFld);
	//bool UpdateInternal(FLDDATA *prpFld);

	void ProcessFldBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


private:
	bool ProcessFldBcNEW(CString &ropData, CString &ropFields);


};


#endif // _CEDAFLDDATA_H_ 
