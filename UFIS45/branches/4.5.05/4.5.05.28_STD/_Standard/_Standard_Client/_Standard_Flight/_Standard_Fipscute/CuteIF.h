// CuteIF.h : main header file for the CUTEIF application
//

#if !defined(AFX_CUTEIF_H__2C783E34_9F84_11D4_9CBF_00D0B7840078__INCLUDED_)
#define AFX_CUTEIF_H__2C783E34_9F84_11D4_9CBF_00D0B7840078__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
//#include "UFISAmSink.h"
#include "CCSCedaData.h"

class CCuteIFException : public CException
{
	DECLARE_DYNCREATE(CCuteIFException)

public:
	CCuteIFException(int nCode = 0);
	~CCuteIFException() { }

	int m_nErrorCode;
};


/////////////////////////////////////////////////////////////////////////////
// CCuteIFApp:
// See CuteIF.cpp for the implementation of this class
//

class CCuteIFApp : public CWinApp
{
public:
	CCuteIFApp();
	~CCuteIFApp();

	static void TriggerWaitCursor(bool bpWait);
 	static bool DumpDebugLog(const CString &ropStr);
	static bool CheckCedaError(const CCSCedaData &ropCedaData);
	static bool DisableLogos();

	static bool bgLogosAvail;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCuteIFApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCuteIFApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


private:
	static char pcmLogoSourceCedaEntry[256];

	ReadBasicData();

	//bool InitCOMInterface();


};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUTEIF_H__2C783E34_9F84_11D4_9CBF_00D0B7840078__INCLUDED_)
