// Utils.cpp : Defines utility functions
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "BasicData.h"

#include "Utils.h"



int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr < ropStrArray[i])
		{
			break;
		}
	}
	ropStrArray.InsertAt(i, ropStr);
	return i;
}


bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return true;
	}
	return false;
}


int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return i;
	}
	return -1;
}


bool IsRegularFlight(char Ftyp)
{
	return (Ftyp != 'T' && Ftyp != 'G');
}


bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsArrival(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsArrival(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpDes, pcgHome) == 0);
}



bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsDeparture(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsDeparture(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpOrg, pcgHome) == 0);
}
 

bool IsCircular(const char *pcpOrg, const char *pcpDes)
{
	return (IsArrival(pcpOrg, pcpDes) && IsDeparture(pcpOrg, pcpDes));
}


bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsCircular(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));
}


bool DelayCodeToAlpha(char *pcpDcd)
{
	CString olDcdAlpha;
	if (!ogBCD.GetField("DEN", "DECN", CString(pcpDcd), "DECA", olDcdAlpha))
		return false;

	strncpy(pcpDcd, olDcdAlpha, 3);  
	return true;
}


bool DelayCodeToNum(char *pcpDcd)
{
	CString olDcdNum;
	if (!ogBCD.GetField("DEN", "DECA", CString(pcpDcd), "DECN", olDcdNum))
		return false;

	strncpy(pcpDcd, olDcdNum, 3);  
	return true;
}



int FindInListBox(const CListBox &ropListBox, const CString &ropStr)
{
	int ilCount = ropListBox.GetCount();
	CString olItem;
	for (int i = 0; i < ilCount; i++)
	{
		ropListBox.GetText(i, olItem);
		if (olItem == ropStr)
			return i;
	}
	return -1;
}


// A substitute for CListBox::GetSelItems(...)
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems)
{
	int ilAnz = 0;
	// Scan all Items
	for (int ilLBCount = 0; ilLBCount < ropLB.GetCount(); ilLBCount++)
	{
		if (ilAnz >= ipMaxAnz) break;

		if (ropLB.GetSel(ilLBCount) > 0)
		{
			// store index in array
			prpItems[ilAnz] = ilLBCount;
			ilAnz++;
		}
	}
	// return the count of selected items
	return ilAnz;
}




long MyGetUrnoFromSelection(const CString &ropSel)
{
	int ilFirst = ropSel.Find("URNO");
	if (ilFirst == -1) return -1;
	ilFirst += 4;

	CString olNums("0123456789");

	while (ilFirst < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilFirst]) != -1)
			break;
		ilFirst++;
	}
	if (ilFirst >= ropSel.GetLength())
		return -1;

	int ilLast = ilFirst;
	while (ilLast < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilLast]) == -1)
			break;
		ilLast++;
	}

	long llUrno = atol(ropSel.Mid(ilFirst,ilLast-ilFirst));
	if (llUrno == 0)
		return -1;
	else
		return llUrno;
}




bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn)
{
	int ilFltn = atoi(ropUserFltn);
	ropDBFltn.Format("%03d", ilFltn);

	return true;
}



bool GetCurrentUtcTime(CTime &ropUtcTime)
{
	ropUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(ropUtcTime);
	return true;
}


