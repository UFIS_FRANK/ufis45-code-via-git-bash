#if !defined(AFX_ROTATIONHTADLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
#define AFX_ROTATIONHTADLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationHTADlg.h : header file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt:int CheckUpArrOrDEP() neu;
//	171100	rkr	Dialog unabhängig von Datenstruktur


#include <CCSTable.h>
#include <DlgResizeHelper.h>
#include <CedaHtaData.h>

#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// RotationHTADlg dialog

class RotationHTADlg : public CDialog
{
// Construction
public:
	RotationHTADlg(CWnd* pParent = NULL, ROTATIONDLGFLIGHTDATA *prpAFlight = NULL, ROTATIONDLGFLIGHTDATA *prpDFlight = NULL);
		           
	~RotationHTADlg();

// Dialog Data
	//{{AFX_DATA(RotationHTADlg)
	enum { IDD = IDD_HANDLING_AGENT };
	CStatic	m_CS_GMBorder_Arr;
	CStatic	m_CS_GMBorder_Dep;
	CCSButtonCtrl m_CB_Insert_Arr;
	CCSButtonCtrl m_CB_Insert_Dep;
	CCSButtonCtrl m_CB_Delete_Dep;
	CCSButtonCtrl m_CB_Delete_Arr;
	CCSButtonCtrl m_CB_Update_Arr;
	CCSButtonCtrl m_CB_Update_Dep;
	//}}AFX_DATA


	void InitTable();
	void DrawHeader();
	void InitDialog();
	void FillTable();


	CWnd* pomParent;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationHTADlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationHTADlg)
	afx_msg void OnClose();
	afx_msg void OnDeleteArr();
	afx_msg void OnDeleteDep();
	afx_msg void OnInsertArr();
	afx_msg void OnInsertDep();
	afx_msg void OnUpdateArr();
	afx_msg void OnUpdateDep();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CCSTable *pomATable;
	CCSTable *pomDTable;


	ROTATIONDLGFLIGHTDATA *prmAFlight;
	ROTATIONDLGFLIGHTDATA *prmDFlight;

	DlgResizeHelper m_resizeHelper;


	
	CUIntArray omArrHtas;
	CUIntArray omDepHtas;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONHTADLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
