// cflightd.cpp - Read flight data
// 

#include <stdafx.h>
#include <ccsglobl.h>
#include <CedaFlightUtilsData.h>
#include <Fpms.h>
#include <Utils.h>
#include <resrc1.h>
#include <AskBox.h>

#define _MIT_TRACE_		false


static int CompareRotation(const FLIGHTUTILSDATA **e1, const FLIGHTUTILSDATA **e2)
{
	// At first: sort by Rkey
	if((**e1).Rkey != (**e2).Rkey)
	{
		if ((**e1).Rkey < (**e2).Rkey)
			return -1;
		else
			return 1;
	}

	// then sort by adid
	if( strcmp((**e1).Adid ,(**e2).Adid ) != 0)
	{
		return ( strcmp((**e1).Adid , (**e2).Adid) );
	}

	// at last: sort by Tifa
	if ((**e1).Tifa == (**e2).Tifa)
		return 0;
	else if ((**e1).Tifa < (**e2).Tifa)
		return -1;
	else
		return 1;
	
}

static int CompareTifd(const FLIGHTUTILSDATA **e1, const FLIGHTUTILSDATA **e2)
{
		return ((**e1).Tifd == (**e2).Tifd)? 0: 
				((**e1).Tifd >  (**e2).Tifd)? 1: -1;
}

static int CompareTifa(const FLIGHTUTILSDATA **e1, const FLIGHTUTILSDATA **e2)
{
		return ((**e1).Tifa == (**e2).Tifa)? 0: 
				((**e1).Tifa <  (**e2).Tifa)? 1: -1;
}




////////////////////////////////////////////////////////////////////////////
// Constructor   
//
CedaFlightUtilsData::CedaFlightUtilsData() : CCSCedaData(&ogCommHandler)
{
	//pomCommHandler = popCommHandler;
    /* Create an array of CEDARECINFO for FLIGHTUTILSDATA*/
	BEGIN_CEDARECINFO(FLIGHTUTILSDATA,AftDataRecInfo)
		CCS_FIELD_CHAR_TRIM(Act3,"ACT3","Flugzeug-3-Letter Code (IATA)", 1)
		CCS_FIELD_CHAR_TRIM(Act5,"ACT5","Flugzeug-5-Letter Code", 1)
		CCS_FIELD_CHAR_TRIM(Adid,"ADID","", 1)
		CCS_FIELD_CHAR_TRIM(Alc2,"ALC2","Fluggesellschaft (Airline 2-Letter Code)", 1)
		CCS_FIELD_CHAR_TRIM(Alc3,"ALC3","Fluggesellschaft (Airline 3-Letter Code)", 1)
		CCS_FIELD_CHAR_TRIM(Des3,"DES3","Bestimmungsflughafen 3-Lettercode", 1)
		CCS_FIELD_CHAR_TRIM(Des4,"DES4","Bestimmungsflughafen 4-Lettercode", 1)
		CCS_FIELD_CHAR(Flno,0,"FLNO","komplette Flugnummer (Airline, Nummer, Suffix)", 1)
		CCS_FIELD_CHAR_TRIM(Flns,"FLNS","Suffix", 1)
		CCS_FIELD_CHAR_TRIM(Fltn,"FLTN","Flugnummer", 1)
		CCS_FIELD_CHAR_TRIM(Ftyp,"FTYP","Type des Flugs [F, P, R, X, D, T, ...]", 1)
		CCS_FIELD_DATE(Onbl,"ONBL","beste Onblock-Zeit", 1)
		CCS_FIELD_CHAR_TRIM(Org3,"ORG3","Ausgangsflughafen 3-Lettercode", 1)
		CCS_FIELD_CHAR_TRIM(Org4,"ORG4","Ausgangsflughafen 4-Lettercode", 1)
		CCS_FIELD_CHAR_TRIM(Psta,"PSTA","Position Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Pstd,"PSTD","Position Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Regn,"REGN","LFZ-Kennzeichen", 1)
		CCS_FIELD_LONG(Rkey,"RKEY","Rotationsschl�ssel", 1)
		CCS_FIELD_DATE(Stoa,"STOA","Planm��ige Ankunftszeit STA", 1)
		CCS_FIELD_DATE(Stod,"STOD","Planm��ige Abflugzeit", 1)
		CCS_FIELD_DATE(Tifa,"TIFA","Zeitrahmen Ankunft", 1)
		CCS_FIELD_DATE(Tifd,"TIFD","Zeitrahmen Abflug", 1)
		CCS_FIELD_LONG(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
	END_CEDARECINFO //(AFTDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for


	strcpy(pcmTableName,"AFT");
	strcpy(pcmAftFieldList,"ACT3,ACT5,ADID,ALC2,ALC3,DES3,DES4,FLNO,FLNS,FLTN,FTYP,ONBL,ORG3,ORG4,PSTA,PSTD,REGN,RKEY,STOA,STOD,TIFA,TIFD,URNO") ;

	pcmFieldList = pcmAftFieldList;

}; // end Constructor



////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
CedaFlightUtilsData::~CedaFlightUtilsData(void)
{
	if(_MIT_TRACE_) TRACE("CedaFlightUtilsData::~CedaFlightUtilsData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}




////////////////////////////////////////////////////////////////////////////
// alle Datens�tze l�schen   
//
void CedaFlightUtilsData::ClearAll(void)
{
	if(_MIT_TRACE_) TRACE("CedaFlightUtilsData::~CedaFlightUtilsData called\n");
	
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}





////////////////////////////////////////////////////////////////////////////
// Datens�tze aus der DB laden   
//
bool CedaFlightUtilsData::ReadRotation(long lpRkey)
{

	char pclSelection[256];
	sprintf(pclSelection, "WHERE RKEY = %ld", lpRkey);


	if (!ReadFlights(pclSelection))
		return false;

	if (omData.GetSize() <= 0)
		return false;

 	return true;
}

bool CedaFlightUtilsData::ReadAllSelectedRotations(CString& olRkeys)
{
/*	CString olSelection;
	char pclSelection[7000] = "";

	olSelection = CString("WHERE RKEY IN (");
	olSelection += olRkeysLists[0] + CString(")");

	strcpy(pclSelection, olSelection);
*/









	CStringArray olRkeysLists;
	CString olSelection;

	char pclSelection[7000] = "";

	if(olRkeys.IsEmpty())
		return true;

	for(int i = SplitItemList(olRkeys, &olRkeysLists, 250) - 1; i >= 0; i--)
	{

		olSelection = CString("WHERE RKEY IN (");
		olSelection += olRkeysLists[i] + CString(")");
		strcpy(pclSelection, olSelection);

		if (!ReadFlights(pclSelection))
			return false;
	}

	if (omData.GetSize() <= 0)
		return false;

 	return true;
}




bool CedaFlightUtilsData::ReadFlights( char *pcpSelection)
{

	if(pcpSelection == NULL || strlen(pcpSelection) == 0) return false;

	bool blRet = CedaAction("GFR", pcpSelection);
	bool blRc = blRet;

	while( blRc )
	{
		FLIGHTUTILSDATA *prpFlight = new FLIGHTUTILSDATA();
		if ( (blRc = GetFirstBufferRecord(prpFlight)) )
		{
			AddFlightInternal(prpFlight);
		}
		else
		{
			delete prpFlight;
		}
	}

	omData.Sort(CompareRotation);
	// TRACE("\n ---------------------------------------------> %d", omData.GetSize());

	return blRet;
}




////////////////////////////////////////////////////////////////////////////
// Addflight- datensatz einf�gen oder �ndern
//
bool CedaFlightUtilsData::AddFlightInternal(FLIGHTUTILSDATA *prpFlight)
{
	DeleteFlightInternal(prpFlight->Urno);
	omData.Add(prpFlight);
	AddToKeyMap(prpFlight);
	return true;
}



////////////////////////////////////////////////////////////////////////////
// Flug l�schen
//
bool CedaFlightUtilsData::DeleteFlightInternal(long lpUrno)
{
	FLIGHTUTILSDATA *prlFlight;
	
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFlight) == TRUE)
	{
		DeleteFromKeyMap(prlFlight);
		for (int ilLc = omData.GetSize() - 1; ilLc >= 0; ilLc--)
		{
			if (omData[ilLc].Urno == lpUrno)
			{
				omData.DeleteAt(ilLc);
				return true;
			}
		}
	}
    return false;
}


////////////////////////////////////////////////////////////////////////////
// Datensatz in die Keymaps einf�gen 
//
bool CedaFlightUtilsData::AddToKeyMap(FLIGHTUTILSDATA *prpFlight )
{
	
	if (prpFlight == NULL )
		return false;

	// set urno map
	omUrnoMap.SetAt((void *)prpFlight->Urno,prpFlight);
    
	return true;
}




////////////////////////////////////////////////////////////////////////////
// Datensatz aus den Keymaps l�schen 
//
bool CedaFlightUtilsData::DeleteFromKeyMap(const FLIGHTUTILSDATA *prpFlight )
{
	if (prpFlight == NULL)
		return false;

	omUrnoMap.RemoveKey((void *)prpFlight->Urno);
   
	return true;
}




////////////////////////////////////////////////////////////////////////////
// ................................................ Datensatz �ber Urno suchen
//
FLIGHTUTILSDATA *CedaFlightUtilsData::GetFlightByUrno(long lpUrno)
{
	FLIGHTUTILSDATA *prlFlight;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFlight) == TRUE)
		return prlFlight;
	else
		return NULL;
}



////////////////////////////////////////////////////////////////////////////
// returns den Ankunftdatensatz eines Abflugs, falls rotation vorliegt 
//
FLIGHTUTILSDATA *CedaFlightUtilsData::GetArrival(const FLIGHTUTILSDATA *prpFlight)
{
	if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
		return GetFlightByUrno(prpFlight->Urno);

	FLIGHTUTILSDATA *prlFlight;
	int ilCount;
	int i = 0;

	ilCount = omData.GetSize();
	while(i < ilCount) 			
	{
		prlFlight = &omData[i];
		if(prlFlight->Urno == prpFlight->Urno)
			break;
		i++;
	}

	for( i--; i >= 0; i--) 			
	{
		prlFlight = &omData[i];
		if((prlFlight->Urno != prpFlight->Urno) && 
			(prlFlight->Rkey == prpFlight->Rkey) &&
			(strcmp(prlFlight->Ftyp, "G") != 0) &&
			(strcmp(prlFlight->Ftyp, "T") != 0))
			return prlFlight;
	}
	return NULL;
}


////////////////////////////////////////////////////////////////////////////
// returns den Abflugdatensatz einer Ankunft, falls rotation vorliegt 
//
FLIGHTUTILSDATA *CedaFlightUtilsData::GetDeparture(const FLIGHTUTILSDATA *prpFlight)
{
	if (IsDepartureFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
		return GetFlightByUrno(prpFlight->Urno);

	FLIGHTUTILSDATA *prlFlight;
	int ilCount;
	int i = 0;
	ilCount = omData.GetSize();
	while(i < ilCount) 			
	{
		prlFlight = &omData[i];
		if(prlFlight->Urno == prpFlight->Urno)
			break;
		i++;
	}
	for( i++; i < ilCount ; i++) 			
	{
		prlFlight = &omData[i];
		if((prlFlight->Urno != prpFlight->Urno) &&
			(prlFlight->Rkey == prpFlight->Rkey) &&
			(strcmp(prlFlight->Ftyp, "G") != 0) &&
			(strcmp(prlFlight->Ftyp, "T") != 0))
			return prlFlight;
	}
	return NULL;
}



FLIGHTUTILSDATA *CedaFlightUtilsData::GetNextDeparture(const FLIGHTUTILSDATA *prpFlight)
{

	FLIGHTUTILSDATA *prlFlight;
	int ilCount;
	int i = 0;
	ilCount = omData.GetSize();
	while(i < ilCount) 			
	{
		prlFlight = &omData[i];
		if(prlFlight->Urno == prpFlight->Urno)
			break;
		i++;
	}
	for( i++; i < ilCount ; i++) 			
	{
		prlFlight = &omData[i];

		if((prlFlight->Urno != prpFlight->Urno) &&
			(prlFlight->Rkey == prpFlight->Rkey) &&
			(strcmp(prlFlight->Ftyp, "G") != 0) &&
			(strcmp(prlFlight->Ftyp, "T") != 0))
		{
			if (IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]))
				return GetFlightByUrno(prlFlight->Urno);
		}
	}
	return NULL;
}





FLIGHTUTILSDATA *CedaFlightUtilsData::GetLastTowInRot(long lpUrno, long lpRkey)
{
	// ATTENTION: there can be a rotation like 'ATTCTCTTD'!

	FLIGHTUTILSDATA *prlFlight;
	int ilCount = omData.GetSize();

	// search given flight
	int ilPos;
	for (ilPos = 0; ilPos < ilCount && omData[ilPos].Urno != lpUrno; ilPos++);

	if (ilPos >= ilCount) 
		return NULL;
		
	
	// search down - up, because array is sorted!
	for (int i = ilPos + 1; i < ilCount; i++)
	{
		prlFlight = &omData[i];
		if(prlFlight->Rkey != lpRkey || IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]))
		{
			break;
		}
	}



	if (i-1 >= 0)
	{
		prlFlight = &omData[i-1];
		if (!IsRegularFlight(prlFlight->Ftyp[0]) && prlFlight->Rkey == lpRkey)
			return prlFlight;
		else
			return NULL;
	}
	else
	{
		return NULL;
	}
}


FLIGHTUTILSDATA *CedaFlightUtilsData::GetFirstTowInRot(long lpUrno, long lpRkey)
{
	// ATTENTION: there can be a rotation like 'ATTCTCTTD'!

	FLIGHTUTILSDATA *prlFlight;
	int ilCount = omData.GetSize();

	// search given flight
	int ilPos;
	for (ilPos = 0; ilPos < ilCount && omData[ilPos].Urno != lpUrno; ilPos++);

	if (ilPos >= ilCount) 
		return NULL;


	// search up - down, because array is sorted!
	for (int i = ilPos - 1; i >= 0; i--)
	{
		prlFlight = &omData[i];

		if(prlFlight->Rkey != lpRkey || IsArrivalFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]))
		{
			break;
		}
	}

	if (i+1 < ilCount)
	{
		prlFlight = &omData[i+1];
		if (!IsRegularFlight(prlFlight->Ftyp[0]) && prlFlight->Rkey == lpRkey)
			return prlFlight;
		else
			return NULL;
	}
	else
	{
		return NULL;
	}
}




bool CedaFlightUtilsData::UpdateFlight(FLIGHTUTILSDATA *prpFlight, FLIGHTUTILSDATA *prpFlightSave)
{
	if (prpFlight == NULL || prpFlightSave == NULL)
		return false;

	CString olFieldList(pcmFieldList);
	CString olListOfData;

	CString olFlno = prpFlight->Flno;
	olFlno.TrimRight();
	if (olFlno.IsEmpty())
	{
		strcpy(prpFlight->Alc3,"");
		strcpy(prpFlight->Alc2,"");
	}

	MakeCedaData(&omRecInfo, olListOfData, olFieldList, prpFlightSave, prpFlight);
	if(olFieldList.IsEmpty())
		return true;


	char pclFieldList[2048];
	char pclSelection[124];
	char pclData[3072];

	strcpy(pclFieldList, olFieldList);
	strcpy(pclData, olListOfData);
	sprintf(pclSelection, "WHERE URNO = %d", prpFlight->Urno);

	bool blret = CedaAction("UFR",pcmTableName, pclFieldList, pclSelection,"", pclData, "BUF1", false); 
	return blret;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////    Public - Methods ////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////
// Datens�tze verbinden, joinen
//
// if cpPart = ' ' then ask the user which values shall be used for the rotation
bool CedaFlightUtilsData::JoinFlight(long lpUrno1, long lpRkey1, long lpUrno2, long lpRkey2, bool bpSilent, char cpPart /*  = ' ' */, bool bpUseGlobalData)
{
	if((lpUrno1 == 0) || (lpUrno2 == 0))
		return false;

//	ClearAll();

	// read flight data
	if (!bpUseGlobalData)
	{
		ClearAll();
		ReadRotation(lpRkey1);
		ReadRotation(lpRkey2);
	}

	char clChangeActRegn = ' ';
	char clChangePos = ' ';

	FLIGHTUTILSDATA *prlFlight = GetFlightByUrno(lpUrno1);
	FLIGHTUTILSDATA *prlAFlight = GetArrival(prlFlight);
	FLIGHTUTILSDATA *prlADFlight = GetDeparture(prlFlight);

	prlFlight = GetFlightByUrno(lpUrno2);
	FLIGHTUTILSDATA *prlDFlight = NULL;

	if (prlFlight)
		prlDFlight = GetDeparture(prlFlight);
	
	if (prlAFlight == NULL || prlDFlight == NULL)
		return false;


	if (strcmp(prlAFlight->Act3, prlDFlight->Act3) != 0 || strcmp(prlAFlight->Act5, prlDFlight->Act5) != 0 ||
		(strcmp(prlAFlight->Regn, prlDFlight->Regn) != 0 && strlen(prlAFlight->Regn) != 0 && strlen(prlDFlight->Regn) != 0))
	{
		if (cpPart != 'A' && cpPart != 'D' && !bpSilent)
		{
			// Aircrafttypes or registrations are not identical !!
			// ask user which aircrafttype or registration shall be used for the resulting rotation
			CString olButton1Text;
			olButton1Text.Format("%s\n%s / %s", prlAFlight->Regn, prlAFlight->Act3, prlAFlight->Act5);
			CString olButton2Text;
			olButton2Text.Format("%s\n%s / %s", prlDFlight->Regn, prlDFlight->Act3, prlDFlight->Act5);

			CString olMessage;
			olMessage.Format(GetString(IDS_STRING1485), prlAFlight->Flno, prlDFlight->Flno );


			AskBox olDlg(NULL, GetString(IDS_WARNING), olMessage, olButton1Text, olButton2Text);
			int ilRet = olDlg.DoModal();

			if (ilRet == 0)
				return false;

			if(ilRet == 1)
			{
				clChangeActRegn = 'A';
			}
			if(ilRet == 2)
			{
				clChangeActRegn = 'D';				
			}
		}
		else
		{
			// default
			clChangeActRegn = cpPart;
		}
	}
	else if (strcmp(prlAFlight->Regn, prlDFlight->Regn) != 0)
	{
		// registrations are different, but one is empty!
		if (strlen(prlAFlight->Regn) == 0)
		{
			clChangeActRegn = 'D';
		}
		else
		{
			clChangeActRegn = 'A';
		}

	}


	/////////// Check the positions
	FLIGHTUTILSDATA *prlLastBeforeDep = GetLastTowInRot(lpUrno1, lpRkey1);
	if (prlLastBeforeDep == NULL)
	{
		prlLastBeforeDep = prlAFlight;
	}
	if (strcmp(prlLastBeforeDep->Psta, prlDFlight->Pstd) != 0)
	{
		// Positions are different
		if (bpSilent)
		{
			if (prlLastBeforeDep->Onbl != TIMENULL)
			{
				clChangePos = 'A';
			}
			else
			{
				clChangePos = cpPart;
			}
		}
		else
		{
			// ask the user with position shall be used for the rotation
			CString olButton1Text(prlLastBeforeDep->Psta);
			if (IsArrivalFlight(prlLastBeforeDep->Org3, prlLastBeforeDep->Des3, prlLastBeforeDep->Ftyp[0]))
			{
				olButton1Text += " (Arr)";
			}
			else if (!IsRegularFlight(prlLastBeforeDep->Ftyp[0]))
			{
				olButton1Text += " (Tow)";
			}
			CString olButton2Text;
			olButton2Text.Format("%s (Dep)", prlDFlight->Pstd);
			
			AskBox olDlg(NULL, GetString(IDS_WARNING), GetString(IDS_STRING2108), olButton1Text, olButton2Text, GetString(IDS_STRING2109));
			
			//To set the rights to Arrival Position & Departure Position Buttons
			olDlg.bmJoinFlag = true; 

			if (prlLastBeforeDep->Onbl != TIMENULL)
			{
				olDlg.EnableButtons(true, false);
			}
			int ilRet = olDlg.DoModal();

			if (ilRet == 0)
				return false;

			if(ilRet == 1)
			{
				clChangePos = 'A';
			}
			if(ilRet == 2)
			{
				clChangePos = 'D';				
			}

		}
	}

	if (!bpSilent)
	{
		// do you really want to ...
		CString olText;
		//PRF 8183
		if(bgSplitJoinLocal)
		{
			CTime ol_LocalStoa,ol_LocalStod;
			ol_LocalStoa = prlAFlight->Stoa;
			ol_LocalStod = prlDFlight->Stod;
			ogBasicData.UtcToLocal(ol_LocalStoa);
			ogBasicData.UtcToLocal(ol_LocalStod);
			olText.Format("%s\n\n%s (%s Local)   -->   %s (%s Local)\n", GetString(IDS_STRING1475), 
			prlAFlight->Flno, ol_LocalStoa.Format("%d.%m.%y %H:%M"), prlDFlight->Flno, ol_LocalStod.Format("%d.%m.%y %H:%M"));

		}
		else
		{
			olText.Format("%s\n\n%s (%s UTC)   -->   %s (%s UTC)\n", GetString(IDS_STRING1475), 
			prlAFlight->Flno, prlAFlight->Stoa.Format("%d.%m.%y %H:%M"), prlDFlight->Flno, prlDFlight->Stod.Format("%d.%m.%y %H:%M"));
		}

		if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
		{
			return false;
		}
	}


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	//////////  adjust the data of the two flights
	FLIGHTUTILSDATA rlFlightASave = *prlAFlight;
	FLIGHTUTILSDATA rlFlightDSave = *prlDFlight;
	FLIGHTUTILSDATA rlLastBeforeDepSave = *prlLastBeforeDep;

	// adjust the registration and aircrafttypes of the flights if necessary
	CString olRegBak;
	if (clChangeActRegn == 'A')
	{
		strcpy(prlDFlight->Act3, prlAFlight->Act3); 
		strcpy(prlDFlight->Act5, prlAFlight->Act5); 
		strcpy(prlDFlight->Regn, prlAFlight->Regn); 
	}
	else if (clChangeActRegn == 'D')
	{
		// don't set the reg. of the arr. flight before joining, because the reg. of the
		// connected dep. flight (if present) will also be changed!

		// delete the reg. before joining and set it again after joining!
		prlAFlight->Regn[0] = '\0';
		if (prlADFlight)
		{
			// remember the registration of the current departure, because the flight
			// process will also overwrite it!!
			olRegBak = prlADFlight->Regn; 
			prlADFlight->Regn[0] = '\0';
		}
	}


	// adjust the positions of the flights if necessary
	if (clChangePos == 'A')
	{
		strcpy(prlDFlight->Pstd, prlLastBeforeDep->Psta); 
	}
	else if (clChangePos == 'D')
	{
		strcpy(prlLastBeforeDep->Psta, prlDFlight->Pstd); 
	}

	// update flight
	if (clChangeActRegn == 'A' || clChangePos == 'A')
	{
		// update departure flight
		UpdateFlight(prlDFlight, &rlFlightDSave);		
	}
	if (clChangeActRegn == 'D' || clChangePos == 'D')
	{
		// update arrival flight
		UpdateFlight(prlAFlight, &rlFlightASave);
		if (prlAFlight != prlLastBeforeDep)
			UpdateFlight(prlLastBeforeDep, &rlLastBeforeDepSave);		
	}

	// join flights
	bool blRet = JoinFlightDB(lpUrno1, lpRkey1, lpUrno2, lpRkey2);


	if (clChangeActRegn == 'D')
	{
		// reset the registration and aircraft type for the arrival flight
		rlFlightASave = *prlAFlight;
		strcpy(prlAFlight->Act3, prlDFlight->Act3); 
		strcpy(prlAFlight->Act5, prlDFlight->Act5);
		strcpy(prlAFlight->Regn, prlDFlight->Regn); 
		UpdateFlight(prlAFlight, &rlFlightASave);

		if (prlADFlight)
		{
			// restore the registration for the previous departure flight
			rlFlightDSave = *prlADFlight;
			strcpy(prlADFlight->Regn, olRegBak);
			UpdateFlight(prlADFlight, &rlFlightDSave);		
		}
	}


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	if (!bpSilent && !blRet)
	{
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_STRING949), GetString(ST_FEHLER), MB_OK | MB_ICONWARNING);
	}


	return blRet;
}




////////////////////////////////////////////////////////////////////////////
// Datens�tze verbinden, joinen
// Flighth�ndlerkommando: "JOF" 
//
bool CedaFlightUtilsData::JoinFlightDB(long lpUrno1, long lpRkey1, long lpUrno2, long lpRkey2)
{
	if((lpUrno1 == 0) || (lpUrno2 == 0))
		return false;

	long llJoinUrno1 = lpUrno1;
	long llJoinUrno2 = lpUrno2;

	///// join the flights!
	// get the urno of the last towing of the arrival
	FLIGHTUTILSDATA *polLastTow = GetLastTowInRot(lpUrno1, lpRkey1);
	if (polLastTow != NULL)
	{
		llJoinUrno1 = polLastTow->Urno;
	}

	// get the urno of the first towing of the departure
	FLIGHTUTILSDATA *polFirstTow = GetFirstTowInRot(lpUrno2, lpRkey2);
	if (polFirstTow != NULL)
	{
		llJoinUrno2 = polFirstTow->Urno;
	}

	// JOIN
	char pclSelection[124];
	char pclData[256];
	strcpy(pclData, "U");
	char pclField[256];
	strcpy(pclField, "RTYP");
	sprintf(pclSelection,"%d,%d,%d,%d",llJoinUrno1, 0, llJoinUrno2, 0);
	bool blRet =  CedaAction("JOF",pcmTableName, pclField, pclSelection,"", pclData, "BUF1");

	return blRet;
}



////////////////////////////////////////////////////////////////////////////
// Datens�tze splitten
// Flighth�ndlerkommando: "SPR" 
//
bool CedaFlightUtilsData::SplitFlight(long lpUrno, long lpRkey, bool bpSilent, bool bpUseGlobalData)
{
	if(lpUrno == 0)
		return false;

//	ClearAll();

	// read flight data
	if (!bpUseGlobalData)
	{
		ClearAll();
		ReadRotation(lpRkey);
	}

	if (!bpSilent)
	{
		FLIGHTUTILSDATA *prlFlight = GetFlightByUrno(lpUrno);
		if (prlFlight == NULL)
			return false;
		FLIGHTUTILSDATA *prlAFlight = GetArrival(prlFlight);
		FLIGHTUTILSDATA *prlDFlight = GetNextDeparture(prlFlight);
		if (prlAFlight != NULL && prlDFlight != NULL)
		{
			CString olText;
			if(bgSplitJoinLocal)
			{
				CTime ol_LocalStoa,ol_LocalStod;
				ol_LocalStoa = prlAFlight->Stoa;
				ol_LocalStod = prlDFlight->Stod;
				ogBasicData.UtcToLocal(ol_LocalStoa);
				ogBasicData.UtcToLocal(ol_LocalStod);
				olText.Format("%s\n\n%s (%s Local)   -->   %s (%s Local)\n", GetString(IDS_STRING1543), 
				prlAFlight->Flno, ol_LocalStoa.Format("%d.%m.%y %H:%M"), prlDFlight->Flno, ol_LocalStod.Format("%d.%m.%y %H:%M"));

			}
			else
			{
				olText.Format("%s\n\n%s (%s UTC)   -->   %s (%s UTC)\n", GetString(IDS_STRING1543), 
				prlAFlight->Flno, prlAFlight->Stoa.Format("%d.%m.%y %H:%M"), prlDFlight->Flno, prlDFlight->Stod.Format("%d.%m.%y %H:%M"));
			}
			if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			{
				return false;
			}
		}
	}


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	// get last towing of rotation
	FLIGHTUTILSDATA *polLastTow = GetLastTowInRot(lpUrno, lpRkey);
	if (polLastTow != NULL)
	{
		lpUrno = polLastTow->Urno;
		lpRkey = polLastTow->Rkey;
	}

	// split rotation
	char pclSelection[124];
	sprintf(pclSelection,"%d,%d",lpUrno, lpRkey);
	bool blRet =  CedaAction("SPR",pcmTableName, "",pclSelection,"",pcgDataBuf, "BUF1");
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	if (!bpSilent && !blRet)
	{
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_STRING944), GetString(ST_FEHLER), MB_OK | MB_ICONWARNING);
	}

	return blRet;
}




