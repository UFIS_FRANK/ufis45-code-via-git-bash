#if !defined(AFX_EXPANDOVERSEASONS_H__B5956014_E901_4F63_8342_CF189C383AE1__INCLUDED_)
#define AFX_EXPANDOVERSEASONS_H__B5956014_E901_4F63_8342_CF189C383AE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpandOverSeasons.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ExpandOverSeasons window

class ExpandOverSeasons
{
// Construction
protected:
	ExpandOverSeasons();

// Attributes
public:

// Operations
public:
	static bool  Expand(CTime from,CTime to);	
	static void	 ExitApp();
	static int   GetTimeZone(CTime from);
// Overrides
protected:
	CTime	CalculateDayInMonth(int year,const SYSTEMTIME &time);
// Implementation
public:
	virtual ~ExpandOverSeasons();
private:
	static ExpandOverSeasons *pomThis;
	TIME_ZONE_INFORMATION omTZInfo;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPANDOVERSEASONS_H__B5956014_E901_4F63_8342_CF189C383AE1__INCLUDED_)
