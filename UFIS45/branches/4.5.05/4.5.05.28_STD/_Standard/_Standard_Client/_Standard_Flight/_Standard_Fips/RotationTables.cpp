// RotationTables.cpp : implementation file
//

#include <stdafx.h>
#include <FPMS.h>
#include <process.h>

#include <CCSButtonCtrl.h>
#include <ButtonListDlg.h>
#include <resource.h>
#include <resrc1.h>
#include <RotationDlg.h>
#include <RotationTables.h>
#include <RotationISFDlg.h>
#include <RotationTableChart.h>
#include <RotationDlgCedaFlightData.h>
#include <SeasonFlightPropertySheet.H>
#include <PrivList.h>
#include <RotGroundDlg.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
#include <cedabasicdata.h>
#include <konflikte.h>
#include <FlightSearchTableDlg.h>
#include <Utils.h>
#include <CedaFpeData.h>

#include <RotationRegnDlg.h>
#include <RotationAltDlg.h>
#include <RotationActDlg.h>
#include <RotationAptDlg.h>
#include <RotationTableReportSelect.h>
#include <RotationTableReportSelectExcel.h>		// 050302 MVy: selection box for what part should be exported
#include <AwBasicDataDlg.h>
#include <SeasonDlg.h>
#include <CedaAptLocalUtc.h>
#include <AllocationChangeDlg.h>
#include <CedaBlaData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationTables





IMPLEMENT_DYNCREATE(RotationTables, CFrameWnd)

RotationTables::RotationTables()
{
	
	ogRotationFlights.Register();
	
	bmNoUpdatesNow = TRUE;
    imStartTimeScalePos = 120;
    imStartTimeScalePos++;              // plus one for left border of chart
	bmIsViewOpen = FALSE;


//rkr23032001
	// Workaround fuer den MessageHandler beim Beenden des InlineEdit, sonst endlos
	fmDlg1 = false;
	fmDlg2 = false;
//rkr23032001


	Create(NULL, GetString(IDS_STRING1029), /*WS_HSCROLL |*/ WS_OVERLAPPED |  
		WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SYSMENU,
        CRect(0, 70, 1024 /*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
        NULL,NULL,0,NULL);

	m_key = "DialogPosition\\DailyFlightSchedule";
}

RotationTables::RotationTables(CWnd *pParent) : CFrameWnd()
{
	ogRotationFlights.Register();
	Create(NULL, GetString(IDS_STRING1029), (/*WS_HSCROLL |*/ WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SYSMENU),
        CRect(0, 65, 1024, GetSystemMetrics(SM_CYSCREEN)),
        pParent,NULL,0);



//rkr23032001
	// Workaround fuer den MessageHandler beim Beenden des InlineEdit, sonst endlos
	fmDlg1 = false;
	fmDlg2 = false;
//rkr23032001

	m_key = "DialogPosition\\DailyFlightSchedule";
}

RotationTables::~RotationTables()
{
	ogRotationFlights.UnRegister();
	ogRotationFlights.ClearAll();
    omPtrArray.RemoveAll();
	delete pomTSTTime;
	delete pomTSTDate;
}


BEGIN_MESSAGE_MAP(RotationTables, CFrameWnd)
	//{{AFX_MSG_MAP(RotationTables)
    ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
    ON_WM_GETMINMAXINFO()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_BN_CLICKED(IDC_VIEW, OnAnsicht)
	ON_BN_CLICKED(IDC_AUTO_NOW, OnAutoNow)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(ID_ENDE, OnEnde)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_JUSTNOW, OnJustNow)
	ON_BN_CLICKED(IDC_SHOWALLTABLES, OnShowAllTables)
	ON_BN_CLICKED(IDC_DETAILS, OnDetails)
	ON_BN_CLICKED(IDC_GROUND, OnGround)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_MESSAGE(WM_TOUCHANDGO, OnTouchAndGo)
	ON_MESSAGE(WM_PLACEROUND, OnPlaceRound)
	ON_MESSAGE(WM_RETURNTAXI, OnReturnTaxi)
	ON_MESSAGE(WM_RETURNFLIGHT, OnReturnFlight)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_MAXIMIZE_CHILD, OnMaximizeChild)
	ON_CBN_SELCHANGE(IDC_ALLVIEWS, OnSelchangeComboAnsicht)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_MESSAGE(WM_TOWING, OnTowing)
    ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_BN_CLICKED(IDC_INSERT2, OnInsert2)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationTables message handlers

BOOL RotationTables::DestroyWindow() 
{
	pogRotationTables = NULL;
	BOOL blRet = CFrameWnd::DestroyWindow();
	return blRet;
}


void RotationTables::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	//pogKonfliktDlg->ResetContent();
	
	//DestroyWindow();
	CFrameWnd::OnClose();
	//pogRotationTables = NULL;

}


void RotationTables::OnCancel() 
{
	OnClose();
}


void RotationTables::OnEnde() 
{
	OnClose();
}

// 050302 MVy: access RotationTables from untyped pointer array
RotationTableChart* RotationTables::GetChartByIndex( int ndxChart )
{
	RotationTableChart* pChart = 0 ;
	if( ndxChart >= 0 && ndxChart < omPtrArray.GetSize() )
		pChart = (RotationTableChart *)omPtrArray[ ndxChart ];
	else
		{ ASSERT(0); };	// requesting index out of bounds, please check calling function
	return pChart ;
};	// GetChartByIndex

void RotationTables::OnPrint() 
{
	RotationTableReportSelect olDlg (this);
	if(olDlg.DoModal() == IDOK)
	{
		CUIntArray olPrintArray;
		olPrintArray.Add(olDlg.m_check4);
		olPrintArray.Add(olDlg.m_check3);
		olPrintArray.Add(olDlg.m_check2);
		olPrintArray.Add(olDlg.m_check1);
		omViewer.PrintAllTableViews(olPrintArray);
	}

//	omViewer.PrintAllTableViews();
}


void RotationTables::OnDestroy() 
{
	SaveToReg();
	CFrameWnd::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

void RotationTables::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


int RotationTables::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_key = "DialogPosition\\DailyFlightSchedule";

	omViewer.SetViewerKey("ROTATIONFLIGHT");

    static UINT ilIndicators[] = { 0 };
	omDialogBar.Create(this, IDD_ROTATIONTABLE, CBRS_TOP, IDD_ROTATIONTABLE);



	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Ansicht"),(CWnd*)omDialogBar.GetDlgItem(IDC_VIEW));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CC_Ansicht"),(CWnd *)omDialogBar.GetDlgItem(IDC_ALLVIEWS));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Details"),(CWnd *)omDialogBar.GetDlgItem(IDC_DETAILS));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Insert"),(CWnd *)omDialogBar.GetDlgItem(IDC_INSERT));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_JustNow"),(CWnd *)omDialogBar.GetDlgItem(IDC_JUSTNOW));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Print"),(CWnd *)omDialogBar.GetDlgItem(IDC_PRINT));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Insert2"),(CWnd *)omDialogBar.GetDlgItem(IDC_INSERT2));
	SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Search"),(CWnd *)omDialogBar.GetDlgItem(IDC_SEARCH));

	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',(CWnd *)omDialogBar.GetDlgItem(IDC_INSERT));
		SetpWndStatAll('0',(CWnd *)omDialogBar.GetDlgItem(IDC_DETAILS));
		SetpWndStatAll('0',(CWnd *)omDialogBar.GetDlgItem(IDC_INSERT2));
	}
	#else
	{
	}
	#endif

    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));
    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);
	omStatusBar.ShowWindow(SW_SHOWNORMAL);
	CRect olDlgBarRect;
	
	omDialogBar.GetClientRect(&olDlgBarRect);

	m_DialogBarHeight = olDlgBarRect.bottom - olDlgBarRect.top;
    CRect olRect; GetClientRect(&olRect);
	m_DialogBarHeight = 70;
	olRect.top += m_DialogBarHeight;
    
	omClientWnd.Create(NULL, "ClientWnd", WS_CHILD  | WS_VISIBLE, olRect, this, 0, NULL);

	omStatusBar.SetPaneText( 0, "", TRUE );
	omStatusBar.UpdateWindow();




	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];


	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}

	//sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
	//sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());

	CRect olRectTopScale;
	GetClientRect(&olRect);
	
	olRectTopScale.top    = olRect.top + 7;
	olRectTopScale.bottom = olRect.top + 26;
	olRectTopScale.right    = olRect.right - 206;
	olRectTopScale.left    = olRect.right - 280;

    pomTSTDate = new CCS3DStatic(true);
    pomTSTDate->Create(pclDate, WS_CHILD | WS_VISIBLE | SS_CENTER , olRectTopScale, this);
    pomTSTDate->SetTextColor(RGB(255,0,0));

	olRectTopScale.right    = olRect.right - 282;
	olRectTopScale.left    = olRect.right - 384;
    pomTSTTime = new CCS3DStatic(true);
    pomTSTTime->Create(pclTimes, WS_CHILD | WS_VISIBLE | SS_CENTER , olRectTopScale, this);
    //pomTSTTime->SetWindowPos(&wndTopMost, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	pomTSTTime->ShowWindow(SW_HIDE);
	pomTSTDate->ShowWindow(SW_HIDE);

    RotationTableChart *polChart;
    CRect olPrevRect;
	omClientWnd.GetWindowRect(olRect);
	olRect.bottom -= 20;
	omClientWnd.MoveWindow(olRect);
    omClientWnd.GetWindowRect(&olRect);
	olRect.top += 0;;m_DialogBarHeight;

    int ilLastY = 0;
	int ilPart = (int)((olRect.bottom-olRect.top)/4);
    
	for (int i = 0; i < 4; i++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY+ilPart);
        
		if(i == 0)
			polChart = new RotationTableChart(this, UD_ARRIVAL, "Arrival", &omViewer);
		if(i == 1)
			polChart = new RotationTableChart(this, UD_AGROUND, "AGround", &omViewer);
		if(i == 2)
			polChart = new RotationTableChart(this, UD_DGROUND, "DGround", &omViewer);
		if(i == 3)
			polChart = new RotationTableChart(this, UD_DEPARTURE, "Departure", &omViewer);
 
		polChart->SetStatusBar(&omStatusBar);
        polChart->Create(NULL, "FlightTableChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0, NULL);
      

		omPtrArray.Add(polChart);
		ilLastY = ilPart;
	}
	PositionChild();

	omViewer.ShowAllTables();
	UpdateWindow();

	//UpdateView();

    SetTimer(1, (UINT)   10 * 1000, NULL); // show utc-time
	SetTimer(3, (UINT)    1 * 500, NULL); // show ansichtdialog
	SetTimer(15, (UINT)  6 *  10 * 1000, NULL); // konfliktcheck
	//SetTimer(15, (UINT)  30 * 1000, NULL); // konfliktcheck

	UpdateComboBox();

	SetWndPos();

	// update window caption
	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1029);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

	bm_ProcessingTblChg = FALSE;
	lm_lastSelUrno1=0;
	lm_lastSelUrno2=0;

	return 0;
}


void RotationTables::OnShowAllTables()
{
	RotationTableChart *polChart;
	for(int i = 0; i < 4; i++)
	{
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
        polChart->SetState(Maximized);
 		polChart->omTable.ShowWindow(SW_NORMAL);
	}
	PositionChild();
}


bool RotationTables::ShowFlight(long lpUrno)
{
	return omViewer.ShowFlightLine(lpUrno);
}


bool RotationTables::SelectFlight(long lpUrno)
{
	return omViewer.SelectFlightLine(lpUrno);
}


LONG RotationTables::OnMaximizeChild(UINT wParam, LONG lParam)
{
	RotationTableChart *polChart;
	for(int i = 0; i < 4; i++)
	{
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);

        if(polChart == (RotationTableChart *)lParam)
		{
			polChart->SetState(Maximized);
 			polChart->omTable.ShowWindow(SW_NORMAL);
		}
		else
		{
			polChart->SetState(Minimized);
			polChart->omTable.ShowWindow(SW_HIDE);
		}
	}
	PositionChild();
	return 0L;
}



void RotationTables::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    RotationTableChart *polChart;
    //GateChart *polChart;

    //Neuberechnung der H�hen durch Anzahl der Minimierten und Aufteilung
	//des Platzes f�r die Maximierten

	int ilMinimizeCount[4];
	int ilChartHeight[4];
	int ilCountMiniMized = 0;
	int ilHeightOfSingleMin=0;

	for(int i = 0; i < 4; i++)
	{
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->GetState() == Minimized)
		{
			ilMinimizeCount[i] = Minimized;
			ilCountMiniMized++;
			ilHeightOfSingleMin = polChart->GetHeight()+10;
			ilChartHeight[i] = ilHeightOfSingleMin;
		}
		else
		{
			ilMinimizeCount[i] = Maximized;
		}
	}


    int ilLastY = 2;//m_DialogBarHeight;//0;
    omClientWnd.GetClientRect(&olRect);
	int ilTotalHeight = olRect.bottom - olRect.top;
	ilTotalHeight -= (ilCountMiniMized * ilHeightOfSingleMin);
	int ilHeightMaximized = 0;
	if(ilCountMiniMized < 4)
	{
		ilHeightMaximized = (ilTotalHeight / (4 - ilCountMiniMized));// + 10;
	}
	omClientWnd.ClientToScreen(&olRect);
	ScreenToClient(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (RotationTableChart *) omPtrArray.GetAt(ilIndex);

		polChart->GetClientRect(&olChartRect);
	    olChartRect.right = olRect.right;
        olChartRect.top = ilLastY;
		if(ilMinimizeCount[ilIndex] == Minimized)
		{
			ilLastY += ilHeightOfSingleMin;//+10;
	        olChartRect.bottom = ilLastY;
		}
		else
		{
			ilLastY += ilHeightMaximized;
	        olChartRect.bottom = ilLastY;
		}
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}

    omClientWnd.Invalidate(TRUE);
	OnUpdatePrevNext();

}



void RotationTables::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    CWnd::OnGetMinMaxInfo(lpMMI);
}


void RotationTables::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olRect; GetClientRect(&olRect);

    olRect.top += 30;                   // for Dialog Bar & TimeScale
	olRect.bottom -= 20;
    omClientWnd.MoveWindow(&olRect, TRUE);

	omStatusBar.SetPaneText(0, "Text");
    PositionChild();

}

void RotationTables::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
}



LONG RotationTables::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG RotationTables::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    return 0L;
}

void RotationTables::OnUpdatePrevNext(void)
{
	SetFocus();
}


void RotationTables::OnInsert()
{
	RotationISFDlg dlg(this);
	dlg.DoModal();
}

void RotationTables::OnInsert2()
{
	pogSeasonDlg->NewData(this, 0L, 0L, DLG_NEW, bgDailyLocal);
}



void RotationTables::OnDetails()
{
	ROTATIONTABLE_LINEDATA *prlLine;
	RotationTableChart *polChart;
	ROTATIONFLIGHTDATA *prlFlight = NULL;
	int ilLineNo;

	CString olAdid;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		ilLineNo = polChart->omTable.GetCurSel();
		if(ilLineNo >= 0)
		{
			prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

			if(prlLine != NULL)
			{

				if((polChart->imGroupID == UD_ARRIVAL) || (polChart->imGroupID == UD_AGROUND))
					olAdid = "A";
				else
					olAdid = "D";

				prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
				break;
			}
		}
	}
	if(prlFlight == NULL)
		return;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgDailyLocal);
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}



void RotationTables::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
}

void RotationTables::OnJustNow()
{
	omViewer.ShowNextLines();
}


void RotationTables::OnZeit()
{


}

////////////////////////////////////////////////////////////////////////
// RotationTables keyboard handling

void RotationTables::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{

	
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void RotationTables::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}


void RotationTables::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogRotationFlights.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), DAILYSCHEDULES);
}


////////////////////////////////////////////////////////////////////////
// RotationTables -- implementation of DDX call back function

static void FlugplanCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	RotationTables *polDiagram = (RotationTables *)popInstance;

}






LONG RotationTables::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	ROTATIONFLIGHTDATA *prlFlight = NULL;

	char clAdid;

	if(ogPrivList.GetStat("ROTATIONTAB_CB_Details") == '-')
		return 0L;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(&polChart->omTable == olNotify.SourceTable)
		{

			if((polChart->imGroupID == UD_ARRIVAL) || (polChart->imGroupID == UD_AGROUND))
				clAdid = 'A';
			else
				clAdid = 'D';
			
			ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(wParam);

			if(prlLine == NULL)
				return 0L;
			prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);

			if(prlFlight == NULL)
				return 0L;
			break;
		}
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	

	CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgDailyLocal, 'D');

	/*
	if( (CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G"))
	{
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, bgDailyLocal);
	}
	else
	{
		pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgDailyLocal);
	}
	*/

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}


LONG RotationTables::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(&polChart->omTable != olNotify.SourceTable)
		{
	      polChart->omTable.SelectLine(-1);
		}
		else
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);
				if(prlLine != NULL)
				{
					//ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlLine->Urno));
					if(prlLine->FlightPermitColour != WHITE && ogFpeData.bmFlightPermitsEnabled && olNotify.Column == 0)
					{
						StartFlightPermitsAppl(prlLine->Urno);
					}
				}
			}

		}
	}

	return 0L;
}

void RotationTables::StartFlightPermitsAppl(long lpFlightUrno)
{
	ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(lpFlightUrno);
	if(prlFlight == NULL)
		return;

	bool blArr = (!strcmp(prlFlight->Adid,"A")) ? true : false;
	CTime olFlightTime = blArr ? prlFlight->Stoa : prlFlight->Stod;
	ogFpeData.StartFlightPermitsAppl(olFlightTime, blArr, prlFlight->Regn, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns);
}


void RotationTables::SetCurrentTime(RotationTableChart *popChart, int ipColumn, int ipLine)
{
	if (popChart == NULL)
		return;

	if (ipColumn < 0 || ipLine < 0)
		return;


	// get all needed data structures
	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)popChart->omTable.GetTextLineData(ipLine);
	if(prlLine == NULL)
		return;

	TABLE_HEADER_COLUMN *prlHeaderInfo = popChart->omTable.GetHeader(ipColumn);
	if (prlHeaderInfo == NULL)
		return;

	ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
	if(prlFlight == NULL)
		return;



	CTime olCurr = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurr);
	
	char pclValue[512];
	char pclField[512];

	if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
		return;


	sprintf(pclValue, "%s", CTimeToDBString(olCurr, TIMENULL));

	if(popChart->imGroupID == UD_ARRIVAL)
	{

		if (prlHeaderInfo->Text == GetString(IDS_STRING303))   // TMO
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
				return;

			if(ogPrivList.GetStat("ROTATIONTAB_A_UtcTmoa") != '1')
				return;
			strcpy(pclField, "TMAU");			

			if((prlFlight->Land <= olCurr) && (prlFlight->Land != TIMENULL))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING950), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
		
		}

		if (prlHeaderInfo->Text == GetString(IDS_STRING304)) // ATA
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
				return;
			
			if(ogPrivList.GetStat("ROTATIONTAB_A_UtcLand") != '1')
				return;
			strcpy(pclField, "LNDU");			


			if((prlFlight->Onbl <= olCurr) && (prlFlight->Onbl != TIMENULL))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING951), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
		}
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING307))   // ONB
		{
			if(ogPrivList.GetStat("ROTATIONTAB_A_UtcOnbl") != '1')
				return;

			if(strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
				return;

			if(prlFlight->Land == TIMENULL)
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}

			if(prlFlight->Land >= olCurr)
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}

			strcpy(pclField, "ONBU");			
		}
		
	}

	if(popChart->imGroupID == UD_AGROUND)
	{

		if (prlHeaderInfo->Text == GetString(IDS_STRING303))   // TMO
		{
			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
				return;
			
			if(ogPrivList.GetStat("ROTATIONTAB_AG_UtcTmoa") != '1')
				return;

			strcpy(pclField, "TMAU");			

			if((prlFlight->Land <= olCurr) && (prlFlight->Land != TIMENULL))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING950), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
	
		}

		if (prlHeaderInfo->Text == GetString(IDS_STRING304)) // ATA
		{
			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
				return;

			if(ogPrivList.GetStat("ROTATIONTAB_AG_UtcLand") != '1')
				return;

			strcpy(pclField, "LNDU");			

			if((prlFlight->Onbl <= olCurr) && (prlFlight->Onbl != TIMENULL))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING951), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
		}
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING307))   // ONB
		{
			if(ogPrivList.GetStat("ROTATIONTAB_AG_UtcOnbl") != '1')
				return;

			if(strcmp(prlFlight->Ftyp, "T") == 0 || strcmp(prlFlight->Ftyp, "G") == 0)
//				return;
			{
				if(prlFlight->Ofbl == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					return;
				}

				if(prlFlight->Ofbl >= olCurr)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
					return;
				}
			}
			else
			{
				if(prlFlight->Land == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					return;
				}

				if(prlFlight->Land >= olCurr)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
					return;
				}
			}

			strcpy(pclField, "ONBU");			
		}
		
	}

	ROTATIONFLIGHTDATA *prlAFlight = ogRotationFlights.GetArrival(prlFlight);

//	if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING962), GetString(IDS_STRING908), MB_YESNO) == IDNO)
//		return 0L;

	if(popChart->imGroupID == UD_DGROUND)
	{
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING319)) // OFB
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "Z") == 0)
				return;			
			
			if(ogPrivList.GetStat("ROTATIONTAB_DG_UtcOfbl") != '1')
				return;

			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{

				if(prlAFlight->Land == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING960), GetString(IDS_STRING908));
						return;
					}
				}

			}

			if((prlFlight->Airb != TIMENULL) && (prlFlight->Airb < olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}

			strcpy(pclField, "OFBU");			
		}
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING321))   // ATD
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || 
				strcmp(prlFlight->Ftyp, "G") == 0 || strcmp(prlFlight->Ftyp, "Z") == 0)
				return;

			if(ogPrivList.GetStat("ROTATIONTAB_DG_UtcAirb") != '1')
				return;

			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING959), GetString(IDS_STRING908));
						return;
					}
				}

			}

			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl > olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}

			strcpy(pclField, "AIRU");			
		}
		
	}
	if(popChart->imGroupID == UD_DEPARTURE)
	{
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING319)) // OFB
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "Z") == 0)
				return;
			
			if(ogPrivList.GetStat("ROTATIONTAB_D_UtcOfbl") != '1')
				return;
/*
			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908));
					return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908));
					return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING960), GetString(IDS_STRING908));
						return;
					}
				}
			}

			if((prlFlight->Airb != TIMENULL) && (prlFlight->Airb < olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
*/
			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{

				if(prlAFlight->Land == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING960), GetString(IDS_STRING908));
						return;
					}
				}

			}

			if((prlFlight->Airb != TIMENULL) && (prlFlight->Airb < olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}


			strcpy(pclField, "OFBU");			
		}
		
		if (prlHeaderInfo->Text == GetString(IDS_STRING321))   // ATD
		{

			if(strcmp(prlFlight->Ftyp, "B") == 0 || strcmp(prlFlight->Ftyp, "T") == 0 || 
				strcmp(prlFlight->Ftyp, "G") == 0 || strcmp(prlFlight->Ftyp, "Z") == 0)
				return;
			
			if(ogPrivList.GetStat("ROTATIONTAB_D_UtcAirb") != '1')
				return;

/*			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908));
					return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908));
					return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING959), GetString(IDS_STRING908));
						return;
					}
				}

			}

			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl > olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
*/
			if(prlAFlight == NULL)
			{
				if(strcmp(prlFlight->Rtyp, "S") == 0)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return ;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING957), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING958), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return;
				}
				else
				{
					if(prlAFlight->Onbl >= olCurr)
					{
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING959), GetString(IDS_STRING908));
						return;
					}
				}

			}

			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl > olCurr))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}

			strcpy(pclField, "AIRU");			
		}
		
	}


	// save new value in DB
	popChart->omTable.SetIPValue( ipLine, ipColumn, olCurr.Format("%H:%M"));
	char pclSelection[256];
	
	sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
	ogRotationFlights.SaveSpecial(pclSelection, pclField, pclValue);
}



LONG RotationTables::OnTableIPEdit(UINT wParam, LONG lParam)
{
	RotationTableChart *polChart;
    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
	    polChart->omTable.SelectLine(-1);
	}
	return  0L;

}




LONG RotationTables::OnTableRButtonDown(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;
	RotationTableChart *polChart;

	// Deselect all lines in all tables
    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
	    polChart->omTable.SelectLine(-1);
	}


	// get chart and header info
	TABLE_HEADER_COLUMN *prlHeaderInfo = NULL;
	for (i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(&polChart->omTable == olNotify.SourceTable)
		{
			prlHeaderInfo = polChart->omTable.GetHeader(olNotify.Column);
			break;
		}
	}

	if (prlHeaderInfo == NULL)
		return 0L;

	// select line
	polChart->omTable.SelectLine(wParam, TRUE);


	// matching keys pressed?
//rkr26032001
//	if (bgDailyWithAlt && !(GetKeyState(VK_MENU) & 0xff80)) && !(GetKeyState(VK_SHIFT) & 0xff80))
//		return 0L;
	if (bgDailyWithAlt && !(GetKeyState(VK_MENU) & 0xff80))
		return 0L;

	if (!bgDailyWithAlt && (GetKeyState(VK_MENU) & 0xff80))
		return 0L;
//rkr26032001

	// dont modify if postflight
	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(olNotify.Line);
	if(prlLine == NULL)
		return 0L;

	if (omViewer.CheckPostFlight(prlLine))
		return 0L;


	// Fill only dedicated fields with current time
	if((polChart->imGroupID == UD_ARRIVAL) || (polChart->imGroupID == UD_AGROUND))	
	{
		if (prlHeaderInfo->Text == GetString(IDS_STRING303) || // TMO
			prlHeaderInfo->Text == GetString(IDS_STRING304) || // ATA
			prlHeaderInfo->Text == GetString(IDS_STRING307))   // ONB
		{
			SetCurrentTime(polChart, olNotify.Column, olNotify.Line);
		}
	}
	if((polChart->imGroupID == UD_DGROUND) || (polChart->imGroupID == UD_DEPARTURE))	
	{
		if (prlHeaderInfo->Text == GetString(IDS_STRING319) || // OFB
			prlHeaderInfo->Text == GetString(IDS_STRING321))   // ATD
		{
			SetCurrentTime(polChart, olNotify.Column, olNotify.Line);
		}
	}

	return 0L;
}




void RotationTables::UpdateComboBox()
{
	CStringArray olArray;
	CStringArray olStrArr;
	CComboBox *polCB = (CComboBox *)omDialogBar.GetDlgItem(IDC_ALLVIEWS);
	polCB->ResetContent();
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
} 




void RotationTables::ChangeViewTo(const char *pcpViewName)
{



}


void RotationTables::OnSelchangeComboAnsicht() 
{
	CComboBox *polView = (CComboBox *)omDialogBar.GetDlgItem(IDC_ALLVIEWS);

	CString opFrom;
	CString opTo;
	int ilItemID;
	CString olView;

	if ((ilItemID = polView->GetCurSel()) != CB_ERR)
	{
		polView->GetLBText(ilItemID, olView);
 
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(olView);
		omViewer.ShowNextLines();
		omViewer.GetFlightTimeSpan( opFrom,  opTo);
		int ilFrom = atoi(opFrom);	
		int ilTo   = atoi(opTo);	
		if((ilFrom != 0) && (ilTo != 0))
		{
			SetTimer(7, (UINT)  60 * 60  * 1000, NULL);
		}
		else
		{
			KillTimer(7);
		}	
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);
}


void RotationTables::OnAnsicht()
{
	SeasonFlightTablePropertySheet olDlg("ROTATION", this, &omViewer, 0, GetString(IDS_STRING1651) );

	int blRet = olDlg.DoModal();

	if( blRet != IDCANCEL)
	{
		CString opFrom;
		CString opTo;
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		UpdateWindow();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(omViewer.GetViewName());
		omViewer.ShowNextLines();
		omViewer.GetFlightTimeSpan( opFrom,  opTo);
		int ilFrom = atoi(opFrom);	
		int ilTo   = atoi(opTo);	
		if((ilFrom != 0) && (ilTo != 0))
		{
			SetTimer(7, (UINT)  60 * 60  * 1000, NULL);
		}
		else
		{
			KillTimer(7);
		}	

		UpdateComboBox();
	}
	else
	{
		UpdateWindow();
		UpdateComboBox();
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

}



void RotationTables::OnAutoNow()
{

	CButton *polNowButton = (CButton *)omDialogBar.GetDlgItem(IDC_AUTO_NOW);

	if(polNowButton != NULL)
	{

		if( polNowButton->GetCheck() == TRUE)
		{
			omViewer.ShowNextLines();
			omViewer.bmAutoNow = true;

		}
		else
		{
			omViewer.bmAutoNow = false;
		}

	}
}



void RotationTables::OnViewSelChange()
{

	int i = 1;

}




////////////////////////////////////////////////////////////////////////////
// UpdateView
//
void RotationTables::UpdateView()
{
	CString olViewName = "<Default>";
	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	omViewer.ShowNextLines();
	AfxGetApp()->DoWaitCursor(-1);
}





void RotationTables::OnTimer(UINT nIDEvent) 
{
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];

	if (nIDEvent == 1)	// update the timeline (every 1 minutes)
	{
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

	    //sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
	    //sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	    pomTSTTime->SetWindowText(pclTimes);
	    pomTSTDate->SetWindowText(pclDate);
	    pomTSTTime->InvalidateRect(NULL);
	    pomTSTDate->InvalidateRect(NULL);
	    pomTSTTime->UpdateWindow();
	    pomTSTDate->UpdateWindow();

	}

	if (nIDEvent == 15)
	{
		ROTATIONFLIGHTDATA *prlFlight = NULL;
		ROTATIONTABLE_LINEDATA *prlLine;
		int ilFlightCnt = 0;
		ogKonflikte.DisableAttentionButtonsUpdate();
		CTime olTime = CTime::GetCurrentTime();
		TRACE("\nBegin ConflictCheck-sch  %s", olTime.Format("%H:%M:%S"));
		for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
		{
			RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
			int ilLineCnt = polChart->omTable.GetLinesCount();
			for(int j=0; j<ilLineCnt; j++)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(j);
				if(prlLine != NULL)
					{
						prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
						if(prlFlight != NULL)
						{
							ogKonflikte.CheckFlight(prlFlight);
							ilFlightCnt++;
						}
					}
			}
		}
		ogKonflikte.EnableAttentionButtonsUpdate();
		TRACE("\nEnd ConflictCheck-sch  %s, No. of flight: %d", olTime.Format("%H:%M:%S"), ilFlightCnt);
	}


	if (nIDEvent == 3)	// update the timeline (every 5 sec)
	{
		if(ogPrivList.GetStat("ROTATIONTAB_CB_Ansicht") == '1'&&!bgDisableViewAutoShow)//lgp
			OnAnsicht();

		KillTimer(3);
	}

	if (nIDEvent == 7)	// update the timeline (every 1 hour)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omStatusBar.SetWindowText("Daten Werden aktualisiert...");
		omViewer.ChangeViewTo(omViewer.GetViewName());
		omViewer.ShowNextLines();
		omStatusBar.SetWindowText("");
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	//CDialog::OnTimer(nIDEvent);
}




void RotationTables::OnGround()
{
/*
	CFontDialog olDlg;
	LPLOGFONT lpLogFont = new LOGFONT;

	olDlg.DoModal();

	olDlg.GetCurrentFont( lpLogFont );

	omViewer.SetUserFont(lpLogFont);

	delete lpLogFont;
*/

	ROTATIONTABLE_LINEDATA *prlLine;
	RotationTableChart *polChart;
	ROTATIONFLIGHTDATA *prlFlight = NULL;
	int ilLineNo;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		ilLineNo = polChart->omTable.GetCurSel();
		if(ilLineNo >= 0)
		{
			prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

			if(prlLine != NULL)
			{
				prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
				break;
			}
		}
	}
	if(prlFlight == NULL)
		return;
	if(pogRotGroundDlg == NULL)
	{
		pogRotGroundDlg = new RotGroundDlg(this);
		pogRotGroundDlg->Create(IDD_ROTGROUND);
	}

	pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, prlFlight->Adid[0], bgDailyLocal);

	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	//ogRotationDlgFlights.ReadRotation(prlFlight->Rkey, prlFlight->Urno, false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}





LONG RotationTables::OnTouchAndGo( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->imGroupID == UD_ARRIVAL)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				if(prlLine != NULL)
				{
					prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
					if(prlFlight == NULL)
						return 0L;
					break;
				}
			}
			else
				return 0L;
		}
	}

	if(	CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1772),GetString(IDS_STRING510),MB_YESNO ) != IDYES)	
		return 0L;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	
	char pclSelection[256];
	char pclValue[126];
	char pclFields[126];
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);


	sprintf(pclValue,"%s,%s", CTimeToDBString(olCurrUtc, TIMENULL), CTimeToDBString(olCurrUtc, TIMENULL));

	sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);

	strcpy(pclFields, "LNDU,ONBU");

	ogRotationFlights.SaveSpecial(pclSelection,pclFields , pclValue);

	ROTATIONDLGFLIGHTDATA *prlFlight2 = new ROTATIONDLGFLIGHTDATA;

	strcpy(prlFlight2->Ftyp, "O");

	CTimeSpan olSpan(0,0,0,1);
	
	prlFlight2->Stod = olCurrUtc + olSpan;
	prlFlight2->Ofbu = olCurrUtc + olSpan;
	prlFlight2->Airu = olCurrUtc + olSpan;
	prlFlight2->Stoa = olCurrUtc + olSpan;


	strcpy(prlFlight2->Flno, prlFlight->Flno); 
	
	CString olFlno(prlFlight->Flno);
	CString olAlc2;
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	if(olFlno.GetLength() > 1)
	{
		olAlc3 = olFlno.Left(3);
		olAlc3.TrimRight();

		CString olStoStr = 	CTimeToDBString( prlFlight2->Stod, prlFlight2->Stod);
		
		ogBCD.GetField("ALT", "ALC2", "ALC3", olAlc3, olAlc2, olAlc3, olStoStr );

		if(olFlno.GetLength() == 9)
			olFlns = olFlno.Right(1);
		olFltn = olFlno.Mid(3,5);
		olFltn.TrimRight();
	}

	strcpy(prlFlight2->Alc2, olAlc2); 
	strcpy(prlFlight2->Alc3, olAlc3); 
	strcpy(prlFlight2->Fltn, olFltn); 
	strcpy(prlFlight2->Flns, olFlns); 

	strcpy(prlFlight2->Act3, prlFlight->Act3); 
	strcpy(prlFlight2->Act5, prlFlight->Act5); 
	strcpy(prlFlight2->Ttyp, prlFlight->Ttyp); 
	strcpy(prlFlight2->Regn, prlFlight->Regn); 
	strcpy(prlFlight2->Rwyd, prlFlight->Rwya); 
	strcpy(prlFlight2->Ifrd, prlFlight->Ifra); 
	strcpy(prlFlight2->Vian, "0"); 

	strcpy(prlFlight2->Org3, pcgHome); 
	strcpy(prlFlight2->Org4, pcgHome4); 

	strcpy(prlFlight2->Des3, "EDX"); 
	strcpy(prlFlight2->Des4, "EDXX"); 
	

	char pclDay[3];
	GetDayOfWeek(prlFlight2->Stod , pclDay);

	CCSPtrArray<CCADATA> olCins;

	ogRotationDlgFlights.AddSeasonFlights(prlFlight2->Stod, prlFlight2->Stod, CString(pclDay), CString(pclDay), CString("1"), prlFlight2->Stod, prlFlight2->Stod, NULL, prlFlight2, olCins);

	delete prlFlight2;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}



LONG RotationTables::OnPlaceRound( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->imGroupID == UD_DEPARTURE)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				if(prlLine != NULL)
				{
					prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
					if(prlFlight == NULL)
						return 0L;
					break;
				}
			}
			else
				return 0L;
		}
	}

	if((CString(prlFlight->Ftyp) == "B") || (CString(prlFlight->Ftyp) == "Z"))
		return 0L;


	if(	CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1772),GetString(IDS_STRING556),MB_YESNO ) != IDYES)	
		return 0L;
	
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	
	char pclSelection[256];
	char pclValue[126];
	char pclFields[126];
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);

	CString olDBTime = CTimeToDBString(olCurrUtc, TIMENULL);

	sprintf(pclValue,"%s,%s,%s,%s,%s", olDBTime, pcgHome, pcgHome4, prlFlight->Rwyd, prlFlight->Ifrd);

	sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);

	strcpy(pclFields, "STOA,DES3,DES4,RWYA,IFRA");

	ogRotationFlights.SaveSpecial(pclSelection, pclFields, pclValue);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}




LONG RotationTables::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;
	bool blRet = true;
	CString olParam1 = "";
	CString olParam2 = "";

	RotationTableChart *polChart;
	TABLE_HEADER_COLUMN *prlHeaderInfo = NULL;
    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(&polChart->omTable == prlNotify->SourceTable)
		{
			prlHeaderInfo = polChart->omTable.GetHeader(prlNotify->Column);
			break;
		}
	}

//rkr23032011
	// damit, wenn vorher ein Dialog beendet wurde, nicht nochmal diese Funktion durchlaufen wird
	// CCSEdit und CCSTable schicken ein Killfocus
	if (fmDlg1 == true || fmDlg2 == true)
	{
		// R�cksetzung der Flags
		fmDlg1 = false;
		fmDlg2 = false;
		polChart->omTable.EndIPEditByLostFocus(true);
		//return 0L;
	}
//rkr23032011


	if (prlHeaderInfo == NULL)
		return 0L;


	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(prlNotify->Line);
	ROTATIONFLIGHTDATA *prlFlight = NULL;

	if(prlLine == NULL)
		return 0L;
	prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);

	if(prlFlight == NULL)
		return 0L;

	char pclValue[1024] = "";
	char pclField[512] = "";
	CString olTmp3;
	CString olTmp4;

	CString olTmp;
	polChart->omTable.GetTextFieldValue(prlNotify->Line, prlNotify->Column, olTmp);


	if(!prlNotify->IsChanged)
	{
		prlNotify->UserStatus = false;
		return 0L;
	}

	prlNotify->UserStatus = true;
	prlNotify->Status = false;


	CTime olSto;


	if((polChart->imGroupID == UD_ARRIVAL) || (polChart->imGroupID == UD_AGROUND))	
	{

		CStringArray olReqFields;
		CStringArray olForecastData;
		if (blDatGatDura || blDatPosDura)
		{
			olForecastData.Add(CString(prlFlight->Adid));
			olForecastData.Add(CString(prlFlight->Alc2));
			olForecastData.Add(CString(prlFlight->Alc3));
			olForecastData.Add(CString(prlFlight->Act3));
			olForecastData.Add(CString(prlFlight->Act5));
			olForecastData.Add(CString(prlFlight->Ttyp));
		}

		if (prlHeaderInfo->Text == GetString(IDS_STRING296))
		{ //FLNO
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
				strcpy(pclValue," , , , , ");
			}
			else
			{
				char pclFlno[10];
				strcpy(pclFlno, prlNotify->Text);
				CString olFlno;
				CString olAlc2;
				CString olFltn;
				CString olFlns;
				CString olAlc3;
				blRet = ogRotationFlights.CreateFlno( pclFlno, olFlno, olAlc2, olFltn, olFlns);
				if(blRet)
				{

					CString olStoStr = 	CTimeToDBString( prlFlight->Stoa, prlFlight->Stoa);
					
					if( ogBCD.GetField("ALT", "ALC2", "ALC3", olAlc2, olAlc2, olAlc3, olStoStr ))
					{
						strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
						sprintf(pclValue,"%s,%s,%s,%s,%s", olFlno, olAlc3, olAlc2, olFltn, olFlns);
					}
					else
						blRet = false;
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING297))
		{ //CSGN
			strcpy(pclField, "CSGN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING298))
		{ //ORG4
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "ORG4,ORG3");			
				strcpy(pclValue, " , ");
			}
			else
			{
				if(blRet = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
				{
					strcpy(pclField, "ORG4,ORG3");			
					sprintf(pclValue,"%s,%s", olTmp4, olTmp3);

					if((prlNotify->Text == CString(pcgHome4)) || (prlNotify->Text == CString(pcgHome)))
					{
						strcat(pclField, ",STOD");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Stoa, TIMENULL));
					}
				}
				else
				{
					RotationAptDlg olDlg(this, prlNotify->Text);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "ORG4,ORG3");			
						sprintf(pclValue,"%s,%s", olDlg.m_Apc4, olDlg.m_Apc3);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING300))
		{  //IFRA
			if((prlNotify->Text == "") || (prlNotify->Text == "V"))// || (prlNotify->Text == "I"))
			{
				strcpy(pclField, "IFRA");			
				sprintf(pclValue,"%s", prlNotify->Text );
			}
			else
			{
				strcpy(pclField, "IFRA");			
//				sprintf(pclValue,"%s", "V");//prlNotify->Text );
				sprintf(pclValue,"%s", "");//prlNotify->Text );
//				blRet = false;
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING301))
		{ // TTYP
			blRet = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp);
			strcpy(pclField, "TTYP");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING302))
		{ // ETAU
			strcpy(pclField, "ETAU");			

			olSto = prlFlight->Stoa;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2012))
		{ // ETOA
			strcpy(pclField, "ETOA");			
			olSto = prlFlight->Stoa;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING303))
		{ // TMOA
			strcpy(pclField, "TMAU");			
			olSto = prlFlight->Stoa;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING304))
		{ // LAND
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;
			
			strcpy(pclField, "LNDU");
			
			olSto = prlFlight->Stoa;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			if((olTime == TIMENULL) && (prlFlight->Onbl != TIMENULL))
			{
				fmDlg1 = true;
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1424), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}

			if((prlFlight->Onbl <= olTime) && (olTime != TIMENULL) && (prlFlight->Onbl != TIMENULL))
			{
				fmDlg1 = true;
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING951), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}


			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING305))
		{ //RWYA
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "RWYA");			
				strcpy(pclValue," ");
			}
			else
			{

				CString olRnam;
				CString olRnum;
				if(!(blRet = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam )))
				{
					blRet = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
					if (blRet)
						olRnam = prlNotify->Text;
				}

				if(blRet)
				{
					strcpy(pclField, "RWYA");			
					sprintf(pclValue,"%s", olRnam);
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING307))
		{ //ONBL
			strcpy(pclField, "ONBU");			
			olSto = prlFlight->Stoa;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			if(!((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G")))
			{
				if(prlFlight->Land == TIMENULL)
				{
					fmDlg1 = true;
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}
				else
				{
					CString olText = olTime.Format("%H:%M %d.%m.%Y");
					CString olText2 = prlFlight->Land.Format("%H:%M %d.%m.%Y");
					
					if((prlFlight->Land >= olTime) && (olTime != TIMENULL))
					{
						fmDlg1 = true;
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
						return 0L;
					}
				}				
			}

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING308))
		{ //PSTA
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "PSTA");			
				strcpy(pclValue," ");
			}
			else
			{
				blRet  = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );					
				if(blRet)
				{

					char clStat = ogPrivList.GetStat("ROTATIONTAB_IL_PSTA_BY_ONBL");
					
					if(prlFlight->Onbl != TIMENULL && clStat != '1')
					{
						if(clStat == '-')
						{
							fmDlg1 = true;
							CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK);
							return 0L;
						}

						if(clStat == '0')
						{
							fmDlg1 = true;
							if(CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1547), GetString(ST_FRAGE), MB_YESNO) == IDNO)
								return 0L;
						}
					}

					strcpy(pclField, "PSTA");			
					sprintf(pclValue,"%s", prlNotify->Text);


					if(prlFlight->Pabs == TIMENULL)
					{
						strcat(pclField, ",PABS");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa, TIMENULL));			
					}
					if(prlFlight->Paes == TIMENULL)
					{
						CTimeSpan olDura;
						if (!blDatPosDura)
						{
							GetPosDefAllocDur(prlNotify->Text, atoi(prlFlight->Ming), olDura);			
						}
						else
						{
							CMapStringToString olForcastMap;
							CreateForcastMap (olForcastMap, olForecastData);
							olReqFields.RemoveAll();
							olReqFields.Add("BAA4");
							GetDefAllocFC(CString("PST"), prlNotify->Text, olForcastMap, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMap.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
//						CTimeSpan olDura;
//						GetPosDefAllocDur(prlNotify->Text, atoi(prlFlight->Ming), olDura);			
					
						strcat(pclField, ",PAES");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa + olDura, TIMENULL));			
					}


				}
			}
			olParam1 = "PST";
			olParam2 = prlNotify->Text;
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING309))
		{ //GTA1
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTA1,TGA1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT_PORT", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTA1,TGA1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);

					if(prlFlight->Ga1b == TIMENULL)
					{
						strcat(pclField, ",GA1B");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa, TIMENULL));			
					}
					if(prlFlight->Ga1e == TIMENULL)
					{
						CTimeSpan olDura;
						if (!blDatGatDura)
						{
							GetGatDefAllocDur(prlNotify->Text, olDura, true);
						}
						else
						{
							CMapStringToString olForcastMap;
							CreateForcastMap (olForcastMap, olForecastData);
							olReqFields.RemoveAll();
							olReqFields.Add("BAA4");
							GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMap, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMap.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
//						CTimeSpan olDura;
//						GetGatDefAllocDur(prlNotify->Text, olDura);
						
						strcat(pclField, ",GA1E");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa + olDura, TIMENULL));			
					}
				}
			}
			olParam1 = "GAT";
			olParam2 = prlNotify->Text;
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING1151))
		{ //BLT1
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "BLT1,TMB1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "BLT1,TMB1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);


					if(prlFlight->B1bs == TIMENULL)
					{
						strcat(pclField, ",B1BS");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa, TIMENULL));			
					}
					if(prlFlight->B1es == TIMENULL)
					{
						CTimeSpan olDura;
	//					GetBltDefAllocDur(prlNotify->Text, olDura);
					if (!bgUseBeltAllocTime) 
						GetBltDefAllocDur(prlNotify->Text, olDura);
					else
						GetBltDefAllocDur(prlFlight->Flti,prlFlight->Act3, prlFlight->Act5, prlFlight->Org3, prlFlight->Org4,prlNotify->Text, olDura);
					
						strcat(pclField, ",B1ES");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifa + olDura, TIMENULL));			
					}
				}
			}
			olParam1 = "BLT";
			olParam2 = prlNotify->Text;
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING310))
		{ // REGN
			prlNotify->Text.TrimLeft();
			prlNotify->Text.TrimRight();
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue,"");
			}
			else
			{
				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				CString olAct3;
				CString olAct5;
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);
					if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
					{

	//					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
						RotationRegnDlg olDlg(this, olRegn, prlFlight->Act3, prlFlight->Act5);
						if(olDlg.DoModal() == IDOK)
						{
							strcpy(pclField, "REGN,ACT3,ACT5");			
							sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
							blRet = true;
						}
					}
					else
							blRet = false;

					fmDlg1 = true;
				}
			}
		}

		if (prlHeaderInfo->Text == GetString(IDS_STRING311))
		{ //ACT5
			CString olAct3User;
			CString olAct5User;
			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
			prlRecords.DeleteAll();
			if (ilCount > 1)
			{
				olAct5User = prlNotify->Text;
				olAct3User = CString(prlFlight->Act3);
				if (!olAct3User.IsEmpty() && CString(prlFlight->Act5).IsEmpty())
				{
					olAct5User.TrimLeft();
					olAct5User.TrimLeft();
				}
				blRet = OnAct35list(olAct5User, olAct3User);
				if (blRet)
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olAct3User, olAct5User);
				}
			}
			else
			{
				if(ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
					olAct5User = prlNotify->Text;
					olAct3User = olTmp;
					blRet = true;
				}
				else
				{
					blRet = false;
					fmDlg1 = true;
				}
			}

			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{

					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
			
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						if(olAct5User != olAct5)
						{
							if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
							{
								if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
								{
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
									ogBCD.Save("ACR");
								}
								else
								{
									blRet = false;
								}
							}
							else
								blRet = false;
						}
					}
				}
			}
		}

/*
		if (prlHeaderInfo->Text == GetString(IDS_STRING311))
		{ //ACT5
			CString olAct3User;
			CString olAct5User;
				
			if(!prlNotify->Text.IsEmpty() && ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp))
			{
				strcpy(pclField, "ACT3,ACT5");			
				sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
				olAct5User = prlNotify->Text;
				olAct3User = olTmp;
				blRet = true;
			}
			else
			{
				blRet = false;
				fmDlg1 = true;
			}
			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{

					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
			
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						if(olAct5User != olAct5)
						{
							if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
							{
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
								ogBCD.Save("ACR");
							}
							else
							{
								blRet = false;
							}
						}
					}
				}
			}
		}
		*/
	}	// 	if((polChart->imGroupID == UD_ARRIVAL) || (polChart->imGroupID == UD_AGROUND))	

	

	if((polChart->imGroupID == UD_DGROUND) || (polChart->imGroupID == UD_DEPARTURE))	
	{

		ROTATIONFLIGHTDATA *prlAFlight = ogRotationFlights.GetArrival(prlFlight);

		CStringArray olReqFields;
		CStringArray olForecastData;
		if (blDatGatDura || blDatPosDura)
		{
			olForecastData.Add(CString(prlFlight->Adid));
			olForecastData.Add(CString(prlFlight->Alc2));
			olForecastData.Add(CString(prlFlight->Alc3));
			olForecastData.Add(CString(prlFlight->Act3));
			olForecastData.Add(CString(prlFlight->Act5));
			olForecastData.Add(CString(prlFlight->Ttyp));
		}


		if (prlHeaderInfo->Text == 	GetString(IDS_STRING296))
		{ //FLNO
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
				strcpy(pclValue," , , , , ");
			}
			else
			{
				char pclFlno[10];
				strcpy(pclFlno, prlNotify->Text);
				CString olFlno;
				CString olAlc2;
				CString olFltn;
				CString olFlns;
				CString olAlc3;
				blRet = ogRotationFlights.CreateFlno( pclFlno, olFlno, olAlc2, olFltn, olFlns);
				if(blRet)
				{
					CString olStoStr = 	CTimeToDBString( prlFlight->Stod, prlFlight->Stod);
					
					if( ogBCD.GetField("ALT", "ALC2", "ALC3", olAlc2, olAlc2, olAlc3, olStoStr ))
					{
						strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
						sprintf(pclValue,"%s,%s,%s,%s,%s", olFlno, olAlc3, olAlc2, olFltn, olFlns);
					}
					else
						blRet = false;
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING297))
		{ //CSGN
			strcpy(pclField, "CSGN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING315))
		{ //DES4
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "DES4,DES3");			
				strcpy(pclValue, " , ");
			}
			else
			{
				if(blRet = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
				{
					strcpy(pclField, "DES4,DES3");			
					sprintf(pclValue,"%s,%s", olTmp4, olTmp3);
					if((prlNotify->Text == CString(pcgHome4)) || (prlNotify->Text == CString(pcgHome)))
					{
						strcat(pclField, ",STOA");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Stod, TIMENULL));
					}
				}
				else
				{
					RotationAptDlg olDlg(this, prlNotify->Text);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "DES4,DES3");			
						sprintf(pclValue,"%s,%s", olDlg.m_Apc4, olDlg.m_Apc3);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING300))
		{ //IFRA
			if((prlNotify->Text == "") || (prlNotify->Text == "V"))// || (prlNotify->Text == "I"))
			{
				strcpy(pclField, "IFRD");			
				sprintf(pclValue,"%s", prlNotify->Text );
			}
			else
			{
				strcpy(pclField, "IFRD");			
//				sprintf(pclValue,"%s", "V");//prlNotify->Text );
				sprintf(pclValue,"%s", "");//prlNotify->Text );
//				blRet = false;
			}

		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING301))
		{ //TTYP
			blRet = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp);
			strcpy(pclField, "TTYP");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING317))
		{ //ETDU
			strcpy(pclField, "ETDU");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2013))
		{ //ETOD
			strcpy(pclField, "ETOD");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING318))
		{ //ETDC
			strcpy(pclField, "ETDC");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING319))
		{ //OFBL
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;


			TRACE("Airb=%s\n", prlFlight->Airb.Format("%H:%M"));
			TRACE("Std=%s\n", prlFlight->Stod.Format("%H:%M"));


			strcpy(pclField, "OFBU");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			
			if(prlAFlight == NULL)
			{
				fmDlg1 = true;
				if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
					return 0L;
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					fmDlg1 = true;
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING961), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					fmDlg1 = true;
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING962), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
				else
				{
					if((prlAFlight->Onbl >= olTime) && (TIMENULL != olTime) && (TIMENULL != prlAFlight->Onbl))
					{
						fmDlg1 = true;
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING960), GetString(IDS_STRING908));
						return 0L;
					}
				}
			}

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;


			if((olTime != TIMENULL) && (prlFlight->Airb != TIMENULL) && (prlFlight->Airb <= olTime))
			{
				fmDlg1 = true;
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING963), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING320))
		{ //SLOU
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;
			
			strcpy(pclField, "SLOU");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING305))
		{ //RWYD
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "RWYD");			
				strcpy(pclValue," ");
			}
			else
			{
				CString olRnam;
				CString olRnum;
				if(!(blRet = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam )))
				{
					blRet = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
					if (blRet)
						olRnam = prlNotify->Text;
				}

				if(blRet)
				{
					strcpy(pclField, "RWYD");			
					sprintf(pclValue,"%s", olRnam);
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING321))
		{ //AIRB
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			strcpy(pclField, "AIRU");			
			olSto = prlFlight->Stod;

			if (bgDailyLocal) ogBasicData.UtcToLocal(olSto);

			CTime olTime = HourStringToDate(prlNotify->Text, olSto);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			
			if(prlAFlight == NULL)
			{
				if(prlFlight->Ofbl == TIMENULL)
				{
					fmDlg1 = true;
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					fmDlg1 = true;
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING961), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					fmDlg1 = true;
					if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING962), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
				else
				{
					if((prlAFlight->Onbl >= olTime) && (olTime != TIMENULL))
					{
						fmDlg1 = true;
						CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING959), GetString(IDS_STRING908));
						return 0L;
					}
				}

			}
		
			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl >= olTime) && (olTime != TIMENULL))
			{
				fmDlg1 = true;
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING308))
		{ //PSTD
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "PSTD");			
				strcpy(pclValue," ");
			}
			else
			{
				blRet  = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "PSTD");			
					sprintf(pclValue,"%s", prlNotify->Text);

					if(prlFlight->Pdes == TIMENULL)
					{
						strcat(pclField, ",PDES");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd, TIMENULL));			
					}
					if(prlFlight->Pdbs == TIMENULL)
					{
						CTimeSpan olDura;
						if (!blDatPosDura)
						{
							GetPosDefAllocDur(prlNotify->Text, atoi(prlFlight->Ming), olDura);			
						}
						else
						{
							CMapStringToString olForcastMap;
							CreateForcastMap (olForcastMap, olForecastData);
							olReqFields.RemoveAll();
							olReqFields.Add("BAA5");
							GetDefAllocFC(CString("PST"), prlNotify->Text, olForcastMap, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMap.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
//						CTimeSpan olDura;
//						GetPosDefAllocDur(prlNotify->Text, atoi(prlFlight->Ming), olDura);
					
						strcat(pclField, ",PDBS");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd - olDura, TIMENULL));			
					}
				}
			}
			olParam1 = "PST";
			olParam2 = prlNotify->Text;
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2014))
		{ //GTD1
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD1,TGD1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTD1,TGD1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);

					if(prlFlight->Gd1e == TIMENULL)
					{
						strcat(pclField, ",GD1E");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd, TIMENULL));			
					}
					if(prlFlight->Gd1b == TIMENULL)
					{
						CTimeSpan olDura;
						if (!blDatGatDura)
						{
							GetGatDefAllocDur(prlNotify->Text, olDura);
						}
						else
						{
							CMapStringToString olForcastMap;
							CreateForcastMap (olForcastMap, olForecastData);
							olReqFields.RemoveAll();
							olReqFields.Add("BAD4");
							GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMap, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMap.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
//						CTimeSpan olDura;
//						GetGatDefAllocDur(prlNotify->Text, olDura);
					
						strcat(pclField, ",GD1B");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd - olDura, TIMENULL));			
					}

				}
			}
				olParam1 = "GAT";
				olParam2 = prlNotify->Text;
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2015))
		{ //GTD2
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD2,TGD2");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTD2,TGD2");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);

				
					if(prlFlight->Gd2e == TIMENULL)
					{
						strcat(pclField, ",GD2E");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd, TIMENULL));			
					}
					if(prlFlight->Gd2b == TIMENULL)
					{
						CTimeSpan olDura;
						if (!blDatGatDura)
						{
							GetGatDefAllocDur(prlNotify->Text, olDura);
						}
						else
						{
							CMapStringToString olForcastMap;
							CreateForcastMap (olForcastMap, olForecastData);
							olReqFields.RemoveAll();
							olReqFields.Add("BAD5");
							GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMap, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMap.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
//						CTimeSpan olDura;
//						GetGatDefAllocDur(prlNotify->Text, olDura);
					
						strcat(pclField, ",GD2B");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Tifd - olDura, TIMENULL));			
					}
				
				
				
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING310))
		{ //REGN
			prlNotify->Text.TrimLeft();
			prlNotify->Text.TrimRight();
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue,"");
			}
			else
			{

				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				CString olAct3;
				CString olAct5;
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);

					if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
					{

					
						//					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
						RotationRegnDlg olDlg(this, olRegn, prlFlight->Act3, prlFlight->Act5);
						if(olDlg.DoModal() == IDOK)
						{
							strcpy(pclField, "REGN,ACT3,ACT5");			
							sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
							blRet = true;
						}
					}
					else
							blRet = false;
					
						fmDlg1 = true;
				}
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING311))
		{ //ACT5
			CString olAct3User;
			CString olAct5User;
			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
			prlRecords.DeleteAll();
			if (ilCount > 1)
			{
				olAct5User = prlNotify->Text;
				olAct3User = CString(prlFlight->Act3);
				if (!olAct3User.IsEmpty() && CString(prlFlight->Act5).IsEmpty())
				{
					olAct5User.TrimLeft();
					olAct5User.TrimLeft();
				}
				blRet = OnAct35list(olAct5User, olAct3User);
				if (blRet)
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olAct3User, olAct5User);
				}
			}
			else
			{
				if(ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
					olAct5User = prlNotify->Text;
					olAct3User = olTmp;
					blRet = true;
				}
				else
				{
					blRet = false;
					fmDlg1 = true;
				}
			}


			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{

					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						if(prlNotify->Text != olAct5)
						{
							if((ogPrivList.GetStat("REGN_AC_CHANGE") == '1' && ogPrivList.GetStat("REGN_AC_CHANGE") == ' '))
							{

								if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
								{
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
									ogBCD.Save("ACR");
									fmDlg1 = true;
								}
								else
								{
									blRet = false;
								}
							}
							else
								blRet = false;
						}
					}
				}
			}
		}

/*
		if (prlHeaderInfo->Text == GetString(IDS_STRING311))
		{ //ACT5
			CString olAct3User;
			CString olAct5User;

			if(!prlNotify->Text.IsEmpty() && ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp))
			{
				strcpy(pclField, "ACT3,ACT5");			
				sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
				olAct3User = olTmp;
				olAct5User = prlNotify->Text;
				blRet = true;
			}
			else
			{
				blRet = false;
				fmDlg1 = true;
			}
			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{

					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						if(prlNotify->Text != olAct5)
						{
							if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
							{
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
								ogBCD.Save("ACR");
								fmDlg1 = true;
							}
							else
							{
								blRet = false;
							}
						}
					}
				}
			}
		}
*/
	} // 	if((polChart->imGroupID == UD_DGROUND) || (polChart->imGroupID == UD_DEPARTURE))	


	if(blRet)
	{
		if(!olParam2.IsEmpty() && bgShowGATPOS && bgDailyRotChainDlg && !prlNotify->IsEscapeKeyPressed)
		{
			DIAFLIGHTDATA prlDiaFlight ;
			prlDiaFlight =  *prlFlight;
			AllocationChangeDlg olDlg(&prlDiaFlight,olParam1,olParam2,this);
			olDlg.DoModal();			

		}

		char pclSelection[128];
		sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
		ogRotationFlights.SaveSpecial(pclSelection, pclField, pclValue);
		prlNotify->UserStatus = true;
		prlNotify->Status = true;
	}
	else
	{
//		MessageBox(GetString(IDS_STRING965), GetString(ST_FEHLER));
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING965), GetString(ST_FEHLER));
		fmDlg2 = true;
 
		prlNotify->UserStatus = true; 
		prlNotify->Status = false;
	}
	polChart->omTable.EndIPEditByLostFocus(true);

	return 0L;
}

bool RotationTables::OnAct35list(CString& opAct5, CString& opAct3) 
{

	if (opAct5.IsEmpty() && !opAct3.IsEmpty())
	{
		return OnAct3list(opAct5, opAct3);
	}


	bool blRet = false;
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT5+,ACT3,+ACFN+", opAct5);
//	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		opAct3 = polDlg->GetField("ACT3");	
		opAct5 = polDlg->GetField("ACT5");	
		blRet = true;
	}
	delete polDlg;

	return blRet;
}

bool RotationTables::OnAct3list(CString& opAct5, CString& opAct3) 
{

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT5+,ACT3+,ACFN+", opAct3);
//	polDlg->SetSecState("SEASONDLG_CE_Act3");

	bool blRet = false;
	if(polDlg->DoModal() == IDOK)
	{
		opAct3 = polDlg->GetField("ACT3");	
		opAct5 = polDlg->GetField("ACT5");	
		blRet = true;
	}
	delete polDlg;

	return blRet;
}

void RotationTables::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_DAILYSCHEDULE_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

//	top = 65;  
	top = 95;  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}



//	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));
	

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	

}


LONG RotationTables::OnReturnTaxi( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->imGroupID == UD_DGROUND)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				if(prlLine != NULL)
				{
					prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
					if(prlFlight == NULL)
						return 0L;
					break;
				}
			}
			else
				return 0L;
		}
	}

	if((CString(prlFlight->Ftyp) == "B") || (CString(prlFlight->Ftyp) == "Z"))
		return 0L;


	if(prlFlight->Ofbl == TIMENULL)
		return 0L;

	if(	CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1772),GetString(IDS_STRING1778),MB_YESNO ) != IDYES)	
		return 0L;
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	char pclValue[512];
	char pclField[512];
	strcpy(pclValue, "B");
	strcpy(pclField, "FTYP");


	char pclSelection[256];
	sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
	ogRotationFlights.SaveSpecial(pclSelection, pclField, pclValue);


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}





LONG RotationTables::OnReturnFlight( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->imGroupID == UD_DEPARTURE)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				if(prlLine != NULL)
				{
					prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
					if(prlFlight == NULL)
						return 0L;
					break;
				}
			}
			else
				return 0L;
		}
	}


	if((CString(prlFlight->Ftyp) == "B") || (CString(prlFlight->Ftyp) == "Z"))
		return 0L;



	if(	CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1772),GetString(IDS_STRING1697),MB_YESNO ) != IDYES)	
		return 0L;
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	char pclValue[512];
	char pclField[512];
	strcpy(pclValue, "Z");
	strcpy(pclField, "FTYP");


	char pclSelection[256];
	sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
	ogRotationFlights.SaveSpecial(pclSelection, pclField, pclValue);


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}

LONG RotationTables::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

/*
LONG RotationTables::OnTableSelChanged( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

	for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
	{
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);
				ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), !omViewer.CheckPostFlight(prlLine));
				polChart->HandlePostFlight(omViewer.CheckPostFlight(prlLine));
			}
		}
	}

//	omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
	return 0L;
}
*/
LONG RotationTables::OnTableSelChanged( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;
	
	if (bm_ProcessingTblChg == TRUE) return 0;
	

	for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
	{
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				RotationTableChart *polChartTmp;

				polChartTmp = (RotationTableChart *) omPtrArray.GetAt(0);
				
				ROTATIONFLIGHTDATA *prlFlight = NULL;
				prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
				prlFlight = ogRotationFlights.GetJoinFlight( prlFlight );

				long lpRotFlightUrno = 0;

				if (prlFlight!=NULL && prlFlight->Urno!=prlLine->Urno)
				{
					lpRotFlightUrno = prlFlight->Urno;
				}

				{
					if (prlLine->Urno!=lm_lastSelUrno1)
					{
						bm_ProcessingTblChg = TRUE;
						CCS_TRY
						
						int ilGrpId = 0;
						if (i<2) ilGrpId = 2;
						omViewer.HighlightFlightLine(ilGrpId, lpRotFlightUrno, lm_lastSelUrno1, lm_lastSelUrno2);

						CTime olTime = CTime::GetCurrentTime();
						TRACE("\n %s  %d, %d", olTime.Format("%H:%M:%S"), prlLine->Urno, lpRotFlightUrno );

						
						lm_lastSelUrno1 = prlLine->Urno;
						lm_lastSelUrno2 = lpRotFlightUrno;

						//this->SelectFlight( prlLine->Urno);

						CCS_CATCH_ALL
						bm_ProcessingTblChg = FALSE;
					}
				}


				ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), !omViewer.CheckPostFlight(prlLine));
				polChart->HandlePostFlight(omViewer.CheckPostFlight(prlLine));
				
			}
		}
	}
	

	bm_ProcessingTblChg = FALSE;
//	omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
	return 0L;
}	
LONG RotationTables::OnTowing( UINT wParam, LPARAM lParam)
{

	ROTATIONFLIGHTDATA *prlFlight = NULL;
	ROTATIONTABLE_LINEDATA *prlLine;

    for (int i = omPtrArray.GetSize() - 1; i >= 0; i--)
    {
		RotationTableChart *polChart = (RotationTableChart *) omPtrArray.GetAt(i);
		if(polChart->imGroupID == UD_AGROUND)
		{
			int ilLineNo = polChart->omTable.GetCurSel();
			if(ilLineNo >= 0)
			{
				prlLine = (ROTATIONTABLE_LINEDATA*)polChart->omTable.GetTextLineData(ilLineNo);

				if(prlLine != NULL)
				{
					prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);
					if(prlFlight == NULL)
						return 0L;
					break;
				}
			}
			else
				return 0L;
		}
	}

	if(pogRotGroundDlg == NULL)
	{
		pogRotGroundDlg = new RotGroundDlg(this);
		pogRotGroundDlg->Create(IDD_ROTGROUND);
	}

	if (omViewer.CheckPostFlight(prlLine))
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::POSTFLIGHT);
	else
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::UNKNOWN);

	pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	if(prlFlight->Urno > 0)
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, 'A', bgDailyLocal);

	return 0L;
}

void RotationTables::OnExcel() 
{
	// TODO: Add your control notification handler code here
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

    GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

	// 050302 MVy: selection box for what part should be exported
	RotationTableReportSelectExcel olDlg (this);
	if( olDlg.DoModal() != IDOK )
		return ;

	omViewer.CreateExcelFile( pclTrenner
		,olDlg.m_bArrival
		,olDlg.m_bLanded
		,olDlg.m_bDeparture
		,olDlg.m_bAirborn
	);
	CString olFileName= omViewer.omFileName;

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 
//	strcpy(pclTmp, GetString(IDS_STRING1386)); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_EXCELPATH_ERROR), GetString(ST_FEHLER), MB_ICONERROR);

	
}
