#if !defined(BLTCCATABLEDLG_H__INCLUDED_)
#define BLTCCATABLEDLG_H__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BltCcaTableDlg.h : header file
//

#include <BltCcaTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// BltCcaTableDlg dialog

class BltCcaTableDlg : public CDialog
{
// Construction
public:
	BltCcaTableDlg(CWnd* pParent = NULL);   // standard constructor
	~BltCcaTableDlg();

	void Activate(void);

	void ReloadData(DiaCedaFlightData *popFlightData);

	void UpdateCaption(void);
	void SaveToReg();

// Dialog Data
	//{{AFX_DATA(BltCcaTableDlg)
	enum { IDD = IDD_BLTCCA };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltCcaTableDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BltCcaTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnSave();
	afx_msg void OnPrint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CCSTable *pomTable;

	BltCcaTableViewer *pomViewer;

private:
	bool isCreated;
	int imButtonSpace;
	int imMinDlgWidth;
	CString m_key; 

	DiaCedaFlightData *pomFlightData;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(BLTCCATABLEDLG_H__INCLUDED_)
