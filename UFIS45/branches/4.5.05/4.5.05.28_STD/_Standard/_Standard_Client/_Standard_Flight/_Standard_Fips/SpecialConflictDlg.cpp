// SpecialConflictDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <SpecialConflictDlg.h>
#include <RotationDlg.h>
#include <ButtonlistDlg.h>
#include <CCSGlobl.h>
#include <CCSDdx.h>
#include <RotGroundDlg.h>
#include <SeasonDlg.h>
#include <PrivList.h>
#include <Utils.h>
#include <PosDiagram.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <process.h>
#include <resrc1.h>
#include <CCSPrint.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// SpecialConflictDlg dialog




SpecialConflictDlg::SpecialConflictDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SpecialConflictDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SpecialConflictDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	bmIsActiv = false;
	bmIsCreated = false;
    CDialog::Create(SpecialConflictDlg::IDD, pParent);
	bmIsCreated = true;
	m_key = "DialogPosition\\SpecialConflict";
	pomButton = NULL;

}


SpecialConflictDlg::~SpecialConflictDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}



void SpecialConflictDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SpecialConflictDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SpecialConflictDlg, CDialog)
	//{{AFX_MSG_MAP(SpecialConflictDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_WM_TIMER()
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SpecialConflictDlg message handlers

BOOL SpecialConflictDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\ConflictsAndAttention";
	
    SetTimer(39, (UINT)  120 * 60 * 1000, NULL); // cleanup konf


	ogDdx.Register(this, KONF_DELETE,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_INSERT,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_CHANGE,	CString(""), CString(""),	KonfTableCf);


	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );
	InitTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SpecialConflictDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void SpecialConflictDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void SpecialConflictDlg::InitTable() 
{

	CRect rect;
    GetClientRect(&rect);
	//rect.top += 50; 

    pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	//pomTable->SetHeaderSpacing(0);


    rect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);


 

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 1500; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = GetString(IDS_STRING1429);

	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}

void SpecialConflictDlg::Activate() 
{
	pomTable->ResetContent();
	bmAttention = true;
	bmIsActiv = true;
	SetWindowText("Missing Position Confirmation Conflicts");
	ChangeViewTo();
}



void SpecialConflictDlg::ShowDialog() 
{
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	pomTable->ResetContent();
	bmAttention = true;
	bmIsActiv = true;

	SetWindowText("Missing Position Confirmation Conflicts");
	ChangeViewTo();
}





void SpecialConflictDlg::ChangeViewTo()
{
	int ilTopIndex = pomTable->GetCTableListBox()->GetTopIndex();

	DeleteAll();    
	MakeLines();
	UpdateDisplay();

	if (ilTopIndex > omLines.GetSize()-1)
		ilTopIndex = omLines.GetSize()-1;

	pomTable->GetCTableListBox()->SetTopIndex(ilTopIndex);

}


void SpecialConflictDlg::DeleteAll()
{
	omLines.DeleteAll();
}


void SpecialConflictDlg::MakeLines()
{
	KONFENTRY *prlEntry;
	KONFDATA  *prlData;

	for(int i = ogKonflikte.omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &ogKonflikte.omData[i];

		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			prlData = &prlEntry->Data[j];

			if (prlData->KonfId.Type == IDS_STRING2850)
				MakeLine(prlData->KonfId, prlData);
		}
	}
}


 
void SpecialConflictDlg::MakeLine(const KonfIdent &rrpKonfId, KONFDATA *prpData)
{
	if (prpData == NULL)
		return;

	KONF_LINEDATA rlLine;

	rlLine.KonfId = rrpKonfId;
	rlLine.Text = prpData->Text;
	rlLine.TimeOfConflict = prpData->TimeOfConflict;
	if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
	{
		rlLine.clrPriority = prpData->clrPriority ;		// 050303 MVy: PRF6981: set line color
	};
	CreateLine(rlLine);

}



int SpecialConflictDlg::CreateLine(KONF_LINEDATA &rpKonf)
{
	if (FindLine(rpKonf.KonfId) >= 0) return -1;

    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareKonf(&rpKonf, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpKonf);

    return ilLineno;
}


int SpecialConflictDlg::CompareKonf(KONF_LINEDATA *prpKonf1, KONF_LINEDATA *prpKonf2)
{
	int	ilCompareResult = (prpKonf1->TimeOfConflict == prpKonf2->TimeOfConflict)? 0:
		(prpKonf1->TimeOfConflict > prpKonf2->TimeOfConflict)? -1: 1;
	return ilCompareResult;
}



void SpecialConflictDlg::UpdateDisplay()
{
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	pomTable->ResetContent();
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		rlColumnData.Text = omLines[ilLc].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
		{
			olColList[0].BkColor = omLines[ilLc].clrPriority ;		// 050303 MVy: PRF6981: set background color of cell
		};
		pomTable->AddTextLine(olColList , &omLines[ilLc]);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();


	if(pomButton != NULL)
	{
		if(omLines.GetSize() > 0)
		{
			pomButton->SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));						
		}
		else
		{
			pomButton->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

		CString olTmp;
		olTmp.Format("%d", omLines.GetSize());
		pomButton->SetWindowText(olTmp);
		pomButton->UpdateWindow();

	}





	CheckPostFlightPosDia(0,this);
}





static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL)
		return;
	SpecialConflictDlg *polDlg = (SpecialConflictDlg *)popInstance;

	if (ipDDXType == KONF_DELETE)
		polDlg->DdxDeleteKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_INSERT)
		polDlg->DdxInsertKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_CHANGE)
		polDlg->DdxChangeKonf(*(KonfIdent *)vpDataPointer);

}

void SpecialConflictDlg::DdxChangeKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;


	DdxDeleteKonf(rrpIdNotify);

	DdxInsertKonf(rrpIdNotify);

}


void SpecialConflictDlg::DdxInsertKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;


	if (rrpIdNotify.Type != IDS_STRING2850)
		return;


	KONFDATA  *prlData = NULL;
	KONF_LINEDATA rlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	int ilLineNo;

	prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

	if(prlData != NULL)
	{
		if(prlData->SetBy != 0)
		{
			if (prlData->KonfId.Type == IDS_STRING2850)
			{
				rlLine.KonfId = rrpIdNotify;
				rlLine.Text = prlData->Text;
				rlLine.TimeOfConflict = prlData->TimeOfConflict;

				if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_ALL) == true)
				{
				
					ilLineNo = CreateLine(rlLine);

					if (ilLineNo != -1)
					{
						rlColumnData.Text = rlLine.Text;
						olColList.NewAt(olColList.GetSize(), rlColumnData);
						pomTable->InsertTextLine(ilLineNo, olColList , &omLines[ilLineNo]);
						olColList.DeleteAll();
					}
				}
			}
		}
	}
	if(pomButton != NULL)
	{
		if(omLines.GetSize() > 0)
		{
			pomButton->SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));						
		}
		else
		{
			pomButton->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

		CString olTmp;
		olTmp.Format("%d", omLines.GetSize());
		pomButton->SetWindowText(olTmp);
		pomButton->UpdateWindow();

	}

}


void SpecialConflictDlg::DdxDeleteKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;


	if(rrpIdNotify.Type != IDS_STRING2850)
		return;

	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		omLines.DeleteAt(ilLineNo);
		pomTable->DeleteTextLine(ilLineNo);
		CheckPostFlightPosDia(0,this);
	}

	if(pomButton != NULL)
	{
		if(omLines.GetSize() > 0)
		{
			pomButton->SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));						
		}
		else
		{
			pomButton->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

		CString olTmp;
		olTmp.Format("%d", omLines.GetSize());
		pomButton->SetWindowText(olTmp);
		pomButton->UpdateWindow();

	}

}


int SpecialConflictDlg::FindLine(const KonfIdent &rrpKonfId)
{
	for(int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].KonfId == rrpKonfId)
			break;
	}
	return i;
}


LONG SpecialConflictDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	KONF_LINEDATA *prlLine = (KONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	if (ogPosDiaFlightData.bmOffLine)
	{
		// only show resource data in offline mode
 		pogSeasonDlg->NewData(this, ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno), bgGatPosLocal);
	}
	else
	{
		// get flight data
		char clFTyp;
		long llUrno = 0;
		long llRkey;
		CTime olTifad;
		bool blLocal;
		// try dia data
		DIAFLIGHTDATA *prlDiaFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);
		if (prlDiaFlight)
		{
			llUrno = prlDiaFlight->Urno;
			llRkey = prlDiaFlight->Rkey;
			clFTyp = prlDiaFlight->Ftyp[0];
			if (prlLine->KonfId.FPart == 'A')
				olTifad = prlDiaFlight->Tifa;
			else
				olTifad = prlDiaFlight->Tifd;
			blLocal = bgGatPosLocal;
		}
		else
		{
			// try daily data
			ROTATIONFLIGHTDATA *prlDailyFlight = ogRotationFlights.GetFlightByUrno(prlLine->KonfId.FUrno);
			if (prlDailyFlight)
			{
				llUrno = prlDailyFlight->Urno;
				llRkey = prlDailyFlight->Rkey;
				clFTyp = prlDailyFlight->Ftyp[0];
				if (prlLine->KonfId.FPart == 'A')
					olTifad = prlDailyFlight->Tifa;
				else
					olTifad = prlDailyFlight->Tifd;
				blLocal = bgDailyLocal;
			}
			else
			{
				// flight not found!!!
				CFPMSApp::DumpDebugLog("ERROR (SpecialConflictDlg::OnTableLButtonDblclk): FLIGHT NOT FOUND IN INTERNAL DATA!");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				return 0L;
			}
		}


		// display rotation mask!
 		CFPMSApp::ShowFlightRecordData(this, clFTyp, llUrno, llRkey, prlLine->KonfId.FPart, olTifad, blLocal); 



		/*
		// try to show flight in daily rotation dialog mask
		if (!pogRotationDlg->NewData(this, 0L, prlLine->KonfId.FUrno, "X", bgDailyLocal))
		{
 			// show towing dialog mask
			if(pogRotGroundDlg == NULL)
			{
				pogRotGroundDlg = new RotGroundDlg(this);
				pogRotGroundDlg->Create(IDD_ROTGROUND);
			}

			pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
			pogRotGroundDlg->NewData(this, 0L, prlLine->KonfId.FUrno, bgDailyLocal);
		}
		*/
	}
 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}



void SpecialConflictDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == 39)	
	{
		ogKonflikte.CleanUp();

	}
}


LONG SpecialConflictDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG SpecialConflictDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	KONF_LINEDATA *prlTableLine = NULL;
	prlTableLine = (KONF_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->KonfId.FUrno != 0)
		CheckPostFlightPosDia(prlTableLine->KonfId.FUrno,this);
	else
		CheckPostFlightPosDia(0,this);
	
	return 0L;
}




void SpecialConflictDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(bmIsCreated != false && cx > 250)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			pomTable->SetPosition(1, cx+1, 1, cy+1);
		}
	}
}


void SpecialConflictDlg::AttachButton( CCSButtonCtrl *popButton)
{
	pomButton = popButton;

}


void SpecialConflictDlg::DeAttachButton( )
{
	pomButton = NULL;

}



LONG SpecialConflictDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	KONF_LINEDATA *prlLine = (KONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	DIAFLIGHTDATA *prlDiaFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);


	if(prlDiaFlight != NULL)
	{
		if( strcmp(prlDiaFlight->Adid, "D") == 0)
			pogPosDiagram->ShowFlight(NULL, prlDiaFlight);
		else
			pogPosDiagram->ShowFlight(prlDiaFlight, NULL);

	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;

}



