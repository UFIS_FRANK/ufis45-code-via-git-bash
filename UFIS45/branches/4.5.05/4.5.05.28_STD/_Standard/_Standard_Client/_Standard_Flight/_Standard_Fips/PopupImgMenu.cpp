// PopupImgMenu.cpp: implementation of the CPopupImgMenu class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PopupImgMenu.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifndef ICON_WIDTH
#define	ICON_WIDTH	50
#define ICON_HEIGHT	50
#define ICON_HALF_WIDTH 25
#define ICON_SPACE  2
#endif
/////////////////////////////////////////////////////////////////////////////
// CPopupImgMenu dialog

//#define WM_MENU_RANGE_END	WM_USER+4000
#define IDC_CHKIN			WM_MENU_RANGE_END+1
#define IDC_MEAL			WM_MENU_RANGE_END+5
#define IDC_CLEANING		WM_MENU_RANGE_END+10
#define IDC_GATE			WM_MENU_RANGE_END+15
#define IDC_SORTING			WM_MENU_RANGE_END+30

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPopupImgMenu::CPopupImgMenu(CWnd* pParent /*=NULL*/)
	: CDialog(CPopupImgMenu::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPopupImgMenu)
	m_FLINFO = _T("");
	//}}AFX_DATA_INIT
	omParent = pParent;
	bmIsIconCreated = false;
	if (omParent != NULL)
	{
		this->Create( CPopupImgMenu::IDD, pParent );
	}

	pcmImgPath[0] = '\0';

	_tcscpy( pcmArrUrno, "" );
	_tcscpy( pcmDepUrno, "" );

	_tcscpy( psmDepIconToShow, "" );
	_tcscpy( psmArrIconToShow, "" );

	imLeft = 0;
	imTop = 0;
	imMaxWidth = 0;
	pmMaxIconsAvailable = 0;
}

CPopupImgMenu::~CPopupImgMenu()
{
	this->DestroyWindow();
	this->Detach();
}

void CPopupImgMenu::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPopupImgMenu)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPopupImgMenu, CDialog)
	//{{AFX_MSG_MAP(CPopupImgMenu)
	ON_BN_CLICKED(IDC_CHKIN, OnCheckIn)
	ON_BN_CLICKED(IDC_CHKIN+1, OnCheckIn)
	ON_BN_CLICKED(IDC_CHKIN+2, OnCheckIn)
	ON_BN_CLICKED(IDC_CHKIN+3, OnCheckIn)

	ON_BN_CLICKED(IDC_MEAL, OnMeal_Arr)
	ON_BN_CLICKED(IDC_MEAL+1, OnMeal_Dep)
	ON_BN_CLICKED(IDC_MEAL+2, OnMeal_ArrHalf)
	ON_BN_CLICKED(IDC_MEAL+3, OnMeal_DepHalf)

	ON_BN_CLICKED(IDC_GATE, OnGate)
	ON_BN_CLICKED(IDC_GATE+1, OnGate)
	ON_BN_CLICKED(IDC_GATE+2, OnGate)
	ON_BN_CLICKED(IDC_GATE+3, OnGate)

	ON_BN_CLICKED(IDC_SORTING, OnSorting)
	ON_BN_CLICKED(IDC_SORTING+1, OnSorting)

	ON_BN_CLICKED(IDC_CLEANING, OnCleaning)
	ON_MESSAGE(WM_BUTTON_RBUTTONDOWN, OnRightButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPopupImgMenu message handlers

void CPopupImgMenu::OnCheckIn() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "CheckIn <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_CHKIN");
	
}

void CPopupImgMenu::OnCleaning() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnCleaning <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_CLEANING");
}

void CPopupImgMenu::OnGate() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnGate <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_GATE");
}

void CPopupImgMenu::OnSorting() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnSorting <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_SORT");
	
}
void CPopupImgMenu::OnMeal() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnMeal <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_MEAL");
}

void CPopupImgMenu::OnMeal_Arr() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnMeal_Arr <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );
	LaunchFlightDataTool("ALERT_MEAL_ARR");
}

void CPopupImgMenu::OnMeal_ArrHalf() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnMeal_ArrHalf <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );	
	LaunchFlightDataTool("ALERT_MEAL_ARR");
}

void CPopupImgMenu::OnMeal_Dep() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnMeal_Dep <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );	
	LaunchFlightDataTool("ALERT_MEAL_DEP");
}

void CPopupImgMenu::OnMeal_DepHalf() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	//char msg[512]="";
	//sprintf( msg, "OnMeal_DepHalf <%s>,<%s>", pcmArrUrno , pcmDepUrno );
	//MessageBox(msg );	
	LaunchFlightDataTool("ALERT_MEAL_DEP");
}


bool CPopupImgMenu::HideMenu()
{
	bool success=true;
	this->ShowWindow( SW_HIDE );
	return success;
}

bool CPopupImgMenu::ShowMenu( const int ipLeft, const int ipTop, const long lpArrUrno, const char* pspArrIconToShow, 
							 const long lpDepUrno, const char* pspDepIconToShow, const int ipMaxWidth )
{
	char pclArrUrno[32]="\0";
	char pclDepUrno[32]="\0";
	ltoa( lpArrUrno, pclArrUrno, 10 );
	ltoa( lpDepUrno, pclDepUrno, 10 );
	return ShowMenu( ipLeft, ipTop,
				lpArrUrno, pspArrIconToShow,
				lpDepUrno, pspDepIconToShow,
				ipMaxWidth );
}


bool CPopupImgMenu::ShowMenu( const int ipLeft, const int ipTop, 
							 const char* pcpArrUrno, const char* pspArrIconToShow, 
							 const char* pcpDepUrno, const char* pspDepIconToShow, 
							 const int ipMaxWidth )
{
//	char pclInfo[1024]="";
	
//	sprintf(pclInfo, "Popup: <%d>, <%d>, <%s>, <%s>, <%s>, <%s>, <%d>"
//		, ipLeft , ipTop 
//		, pcpArrUrno , pspArrIconToShow
//		, pcpDepUrno , pspDepIconToShow 
//		, ipMaxWidth );

//	MessageBox( pclInfo );
	CreateAllIcons();
	bool success=true;
	
	if ((_tcscmp(pcmArrUrno,pcpArrUrno)==0) & 
		(_tcscmp(pcmDepUrno,pcpDepUrno)==0) &
		(_tcscmp(pspArrIconToShow,psmArrIconToShow)==0) &
		(_tcscmp(pspDepIconToShow,psmDepIconToShow)==0) &
		(imLeft==ipLeft) & (imTop==ipTop) &
		(imMaxWidth==ipMaxWidth))
	{
		this->ShowWindow(SW_SHOW);
	}
	else
	{	
		_tcscpy( pcmArrUrno, pcpArrUrno );
		_tcscpy( pcmDepUrno, pcpDepUrno );
		_tcscpy( psmDepIconToShow, pspDepIconToShow );
		_tcscpy( psmArrIconToShow, pspArrIconToShow );
		imLeft = ipLeft;
		imTop = ipTop;
		imMaxWidth = ipMaxWidth;
	
		CString stArrIconToShow = CString(pspArrIconToShow);//Arrival flight related button chars
		CString stDepIconToShow = CString(pspDepIconToShow);//Departure flight related button chars
		stArrIconToShow.MakeLower();
		stDepIconToShow.MakeLower();

		int ilBtnCnt = _tcslen(pspArrIconToShow);//number of button to display
		if (ilBtnCnt<=0) ilBtnCnt = _tcslen(pspDepIconToShow);

		int i = 0;
		int maxIcons = sizeof( omIMGDATA )/sizeof(omIMGDATA[0]);//max buttons count
		int maxImgs = sizeof( omIMGDATA[0].rmBtnData) /sizeof(omIMGDATA[0].rmBtnData[0]);//maximum image count for an icon
		bool blShow = true;
		if (pmMaxIconsAvailable<maxIcons) maxIcons = pmMaxIconsAvailable;
		if (ilBtnCnt<0) blShow = false;
		else
		{
			ilBtnCnt = 0;
			for (i=0; i<maxIcons; i++)
			{
				try
				{//HideIcon
					
					for (int j=0; j<maxImgs; j++)
						HideIcon(omIMGDATA[i].rmBtnData[j].iconBmpButton, 
						omIMGDATA[i].rmBtnData[j].iconMsgMapId);
				}
				catch(...)
				{
				}
				//Check whether need to show button for this and increase the button count to display
				if ((stArrIconToShow.Find(omIMGDATA[i].iconChar)>=0) || 
					(stDepIconToShow.Find(omIMGDATA[i].iconChar)>=0))
					ilBtnCnt++;
			}

			if (ilBtnCnt>maxIcons) ilBtnCnt = maxIcons;
		}
//		sprintf( pclInfo, "BtnCnt : <%d>" , ilBtnCnt );
//MessageBox( pclInfo );
		if (ilBtnCnt<=0) blShow = false;//Check again to make sure any button to display. In case of invalid icon char

		if (blShow)
		{
			int ilLeft = ipLeft;
			int ilTop = ipTop;
			if (ilTop<0) ilTop = 0;
			int ilMenuWidth = (ICON_WIDTH+ ICON_SPACE) * ilBtnCnt;
			//if ((ilLeft + ilMenuWidth) > (ipLeft + ipMaxWidth) )
			if ((ilLeft + ilMenuWidth) > (ipMaxWidth) )
			{
				ilLeft = ipLeft - ilMenuWidth + 20;
				if (ilLeft<0) ilLeft = 0;
			}
			CRect olRect;  			
			this->MoveWindow( ilLeft, ilTop, ilMenuWidth, ICON_HEIGHT );
			GetClientRect( &olRect );

			int left = olRect.left ;
			int top = olRect.top ;
			for (i=0; i<maxIcons; i++)
			{
				if ( (stArrIconToShow.Find(omIMGDATA[i].iconChar)>=0) &
					 (stDepIconToShow.Find(omIMGDATA[i].iconChar)>=0))
				{//Show Arrival and Departure
					ilBtnCnt++;
					ShowIcon( omIMGDATA[i], left, top, SHOW_ARR_DEP );
					left++;
				}
				else if (stArrIconToShow.Find(omIMGDATA[i].iconChar)>=0)
				{//Show Arrival
					ilBtnCnt++;
					ShowIcon( omIMGDATA[i], left, top, SHOW_ARR_ONLY);
				}
				else if (stDepIconToShow.Find(omIMGDATA[i].iconChar)>=0)
				{//Show Departure
					ilBtnCnt++;
					ShowIcon( omIMGDATA[i], left, top, SHOW_DEP_ONLY);
				}
			}

			this->ShowWindow( SW_SHOW);
		}
		else
		{//No button to show
			this->ShowWindow( SW_HIDE );
		}
	}

	return success;
}


void CPopupImgMenu::ShowIcon( PIM_IMGDATA& prImgData, int &left, const int top, int ipShowInd )
{
	PIM_BTNDATA* olBtnData = NULL;
	olBtnData = &prImgData.rmBtnData[0];

	switch( ipShowInd )
	{
	case SHOW_ARR_ONLY:
		olBtnData->iconBmpButton.SetHasArr( TRUE );
		olBtnData->iconBmpButton.SetHasDep( FALSE );
		break;
	case SHOW_DEP_ONLY:
		olBtnData->iconBmpButton.SetHasArr( FALSE );
		olBtnData->iconBmpButton.SetHasDep( TRUE );
		break;
	case SHOW_ARR_DEP:
		olBtnData->iconBmpButton.SetHasArr( TRUE );
		olBtnData->iconBmpButton.SetHasDep( TRUE );
		break;
	default:
		return;
	}

	GetDlgItem( olBtnData->iconMsgMapId )->EnableWindow(TRUE);
	olBtnData->iconBmpButton.MoveWindow(left, top, ICON_WIDTH, ICON_HEIGHT, TRUE );
	olBtnData->iconBmpButton.ShowWindow( SW_SHOW );
	left = left + ICON_WIDTH + ICON_SPACE;
}


void CPopupImgMenu::PostNcDestroy() 
{	
    CDialog::PostNcDestroy();
    delete this;
}

CString GetConfig( const char* pcpSection, const char* pcpKey )
{
	char pclConfigPath[256];
	char pclCfg[2048];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcpSection, pcpKey, "DEFAULT",
		pclCfg, sizeof pclCfg, pclConfigPath);

	CString slResult = CString(pclCfg) ;

	if(!strcmp(pclCfg, "DEFAULT"))
	{
		slResult.Format( "Missing Configuration <%s>", pcpKey );
		//MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);
		slResult = "";
	}
	else{
		slResult.TrimRight();
		slResult.TrimLeft();
	}
	return CString( slResult );
}

CString GetItem( CString& spStr )
{
	int ilLoc = spStr.Find( ',' );
	CString slResult = "";
	if (ilLoc>=0)
	{
		if(ilLoc>0)
		{
			slResult = spStr.Left( ilLoc );
		}
		spStr = spStr.Right( spStr.GetLength() - ilLoc - 1 );
	}
	else
	{
		slResult = spStr;
		spStr = "";
	}
	slResult.TrimRight();
	slResult.TrimLeft();
	return slResult;
}

void CPopupImgMenu::CreateAllIcons()
{
	int ilMaxIconsCnt = sizeof( omIMGDATA )/sizeof(omIMGDATA[0]);//max buttons count

	if (!bmIsIconCreated)
	{	//Read Config Data from ceda.ini
		CString slImgPath = GetConfig( ogAppName, "PIM_IMG_PATH" );
		CString slImgExt = GetConfig( ogAppName, "PIM_IMG_EXT" );
		CString slIconsInfo = GetConfig( ogAppName, "PIM_CFG" );
		CString slIconChar, slIconName, slIconFilePFix;
		int		ilIconCnt = 0;
		CString slBmpFileName;
		SetImagePath( slImgPath );
		CString slTmp;
		CString slExistingIconChars;
		

		slIconsInfo.TrimLeft();
		slIconsInfo.TrimRight();

		while( !slIconsInfo.IsEmpty())
		{
			slTmp = slIconsInfo;
			slIconChar	= GetItem( slIconsInfo );
			slIconName	= GetItem( slIconsInfo );
			slIconFilePFix = GetItem( slIconsInfo );
			if (slExistingIconChars.Find( slIconChar )>=0)
			{
				CString msg;
				msg.Format(  "Duplicate Setting for popup icon menu in 'PIM_CFG'\n <%s>, <%s>, <%s>", 
					slIconChar, slIconName, slIconFilePFix );
				MessageBox( msg );
			}
			else if (slIconChar.IsEmpty() || slIconName.IsEmpty() || slIconFilePFix.IsEmpty())
			{
				CString msg;
				msg.Format(  "Invalid Setting for popup icon menu in 'PIM_CFG'\n <%s>, <%s>, <%s>", 
					slIconChar, slIconName, slIconFilePFix );
				MessageBox( msg );
			}
			else
			{
				if (ilIconCnt>=ilMaxIconsCnt)
				{
					CString msg;
					msg.Format( "Maximum Count %d is reached." , ilMaxIconsCnt );
					MessageBox( msg );
				}
				else
				{
					slBmpFileName.Format("%s.%s", slIconFilePFix, slImgExt );
					InitAButton( &(omIMGDATA[ilIconCnt]), slIconChar[0], slIconName,
						slBmpFileName, IDC_MEAL + ilIconCnt );
					slExistingIconChars += slIconChar;
					ilIconCnt++;
				}
			}
			if (slTmp.Compare(slIconsInfo)==0) break;
			if (slIconsInfo.Compare(",")==0) slIconsInfo = "";
		}
		pmMaxIconsAvailable = ilIconCnt;
		bmIsIconCreated = true;
	}

}

void CPopupImgMenu::CreateIcon( CImageButton* omBmpBtn, int &left, const int top,  const char* pcpBitMapName, const char* pspName, const int ipMsgMapId, bool bpShowHalf )
{
	char pslName[100];
	if (!pspName) strcpy(pslName, "");
	else strcpy(pslName, pspName );
	omBmpBtn->DestroyWindow();
	
	char pclFileName[512];
	sprintf( pclFileName, "%s%s", pcmImgPath, pcpBitMapName );
	if (bpShowHalf)
	{
		omBmpBtn->Create( pslName, BS_DEFPUSHBUTTON | BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
			CRect( left, top, left + ICON_HALF_WIDTH, top + ICON_HEIGHT), this, ipMsgMapId );
		omBmpBtn->LoadBitmapFromFile( pclFileName, ICON_HALF_WIDTH, ICON_HEIGHT );
	}
	else
	{
		omBmpBtn->Create( pslName, BS_DEFPUSHBUTTON | BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
			CRect( left, top, left + ICON_WIDTH, top + ICON_HEIGHT), this, ipMsgMapId );
		omBmpBtn->LoadBitmapFromFile( pclFileName, ICON_WIDTH, ICON_HEIGHT );
	}
	omBmpBtn->SetHorizontal( true );
	
	
	GetDlgItem( ipMsgMapId )->EnableWindow(FALSE);
	omBmpBtn->SetMoveable(true);
	omBmpBtn->ShowWindow( SW_HIDE );
}

void CPopupImgMenu::HideIcon(CImageButton& omBmpBtn, const int ipMsgMapId)
{
	try{
	if ((&omBmpBtn)!=NULL && ((&omBmpBtn)!=0))
	{
		omBmpBtn.MoveWindow(0,0,1,1,FALSE);
		if (GetDlgItem( ipMsgMapId )!=NULL)
			GetDlgItem( ipMsgMapId )->EnableWindow(FALSE);
		omBmpBtn.ShowWindow( SW_HIDE );
	}
	}catch(...){}
}

void CPopupImgMenu::InitAButton( PIM_IMGDATA* prIMGDATA, const char cpCodeChar, const char* pcpBtnName, 
								 const char* pcpImgFileName,
								 const int ipMsgMapId)
{
	prIMGDATA->iconChar = cpCodeChar;
	strcpy( prIMGDATA->name, pcpBtnName );
	prIMGDATA->rmBtnData[0].iconMsgMapId = ipMsgMapId;
	
	strcpy( prIMGDATA->rmBtnData[0].pcrImgFileName, pcpImgFileName );

	int left=0, top=0;
	bool blHalfSize = false;

	prIMGDATA->rmBtnData[0].iconBmpButton.SetId( CString(pcpBtnName) );
	prIMGDATA->rmBtnData[0].iconBmpButton.SetBtnId( 0 );

	CreateIcon( &(prIMGDATA->rmBtnData[0].iconBmpButton), left, top, 
					prIMGDATA->rmBtnData[0].pcrImgFileName, 
					prIMGDATA->name, prIMGDATA->rmBtnData[0].iconMsgMapId, blHalfSize );

}

void CPopupImgMenu::SetImagePath( const char* pcpImgPath )
{
	strcpy( pcmImgPath, pcpImgPath );
}


void CPopupImgMenu::LaunchFlightDataTool(const CString spMsg) 
{
	char pclConfigPath[256];
	char pclAppPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHT_DATA_TOOL", "DEFAULT",
		pclAppPath, sizeof pclAppPath, pclConfigPath);


	if(!strcmp(pclAppPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);

	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_ARep");

	CString olTime = "U";

	olTime = "L";
//	pcmArrUrno.TrimRight();
//	pcmArrUrno.TrimLeft();
//	pcmDepUrno.TrimRight();
//	pcmDepUrno.TrimLeft();

//	if (strcmp(pcmArrUrno,"")==0) strcpy( pcmArrUrno="0" );
//	if (strcmp(pcmDepUrno,"")==0) strcpy( pcmDepUrno="0" );

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"%s,%s,%s|%s,%s,%s|%d,%d", 
		spMsg,
		pcmArrUrno, pcmDepUrno, 
		olUser, olPermits, olTime, 
		point.x, point.y );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	int ilReturn = _spawnv(_P_NOWAIT,pclAppPath,args);

}


LRESULT CPopupImgMenu::OnRightButtonDown( UINT wParam, LPARAM lParam)
{
	CImageButton* polBtn = (CImageButton*) lParam;
	CString slId = polBtn->GetId();
	int ilIdx = polBtn->GetBtnId();
	char cl = slId[0];
	CString st;
	CString st2;
	CString slMsg;
	slMsg.Format( "ALERT_%s", slId );
	switch( polBtn->GetBtnId())
	{
		case 0://Arrival Only
			st2 = "Arrival Only";
			break;
		case 1://Departure Only
			st2 = "Departure Only";
			break;
		case 2://Arrival Button, with Arrival and Departure
			st2 = "Arrival of Both";
			break;
		case 3://Departure Button, with Arrival and Departure
			st2 = "Departure of Both";
			break;
	}
	HideMenu();
	//st.Format( "%s,%d,<%s>,<%s>,<%s>", slId, polBtn->GetBtnId(), pcmArrUrno , pcmDepUrno, st2 );
	//MessageBox( st );
	LaunchFlightDataTool(slMsg);

	return 0L;
}
