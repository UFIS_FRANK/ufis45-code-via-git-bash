// CedaUaaData.cpp: implementation of the CedaUaaData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <fpms.h>
#include <CedaUaaData.h>

#include <BasicData.h>
#include <CCSGlobl.h>

#include <Utils.h>
#include <CedaBasicData.h>

#include <BasicData.h>
#include <CedaBasicData.h>
#include <RecordSet.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CedaUaaData ogUaaData;

CedaUaaData::CedaUaaData()
{
	BEGIN_CEDARECINFO(UAADATA, UaaDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Grpu,"GRPU")
		FIELD_CHAR_TRIM(Data,"DATA")
		FIELD_CHAR_TRIM(Ufea,"UFEA")
    END_CEDARECINFO

	 // Copy the record structure
    for (int i = 0; i < sizeof(UaaDataRecInfo)/sizeof(UaaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&UaaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	strcpy(pcmTableName,"UAA");

	strcpy( pcmFList, "URNO,CDAT,USEC,GRPU,DATA,UFEA");
	pcmFieldList = pcmFList;

	olMinusCount = 0;
	olAllCount = 0;
	olNoneCount = 0;
	olPlusCount = 0;

}

CedaUaaData::~CedaUaaData()
{
	omRecInfo.DeleteAll();
	ClearAll();
}

CString CedaUaaData::GetViewLoadSettings()
{
	CString olWhere;
	olWhere.Format("WHERE APPL = '%s' AND FENM='%s'", ogAppName,"ViewLoad");
	CString olTmp;

	char pclFields[100] = "URNO,APPL,FENM,TANA,FINA,CDAT,USEC,HOPO";
	char pclTable[100];
	sprintf(pclTable,"FEA%s",pcgTableExt);
	char pclWhere[100];
	sprintf(pclWhere,"WHERE APPL = '%s' AND FENM='%s'", ogAppName,"ViewLoad");
		
	int ilRc = -1;
	ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "",pcgDataBuf, "BUF1");
	
	if (ilRc)
	{
		CString olData;
		if (this->GetBufferLine(0,olData))
		{
			CStringArray olList;
			::ExtractItemList(olData,&olList);
			olTmp = olList[0];
			olTmp.TrimLeft();
			olTmp.TrimRight();
		}
		else
		{
			olTmp="";
		}
		
	}

	return olTmp;
}

bool CedaUaaData::Read()
{

	olMinusCount = 0;
	olAllCount = 0;
	olNoneCount = 0;
	olPlusCount = 0;

	bool ilRc = false;

	char pclCmd[3] = "RT";
	char pclWhere[512] = "";
	char pclData[2024] = "";
	CString olWhere;

	CString strUrno = GetViewLoadSettings();
	strUrno.TrimLeft();
	strUrno.TrimRight();

	if(strUrno.GetLength() > 0)
	{
		olWhere.Format("WHERE UFEA = '%s' ", strUrno);
	}

	CString strProfile = GetProfileList();
	
	if(strProfile.GetLength() > 0)
	{
		olWhere += " AND GRPU IN(" + strProfile + ")";
	}

	
	sprintf(pclWhere,olWhere);
	
	ilRc = CedaAction(pclCmd,pcmTableName,pcmFieldList, pclWhere,"",pclData, "BUF1");

	if (ilRc)
	{
		UAADATA rlUaaData;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			if ((ilRc = GetBufferRecord(ilLc,&rlUaaData)) == true)
			{
				SetPrefix(rlUaaData);
				InsertInternal(&rlUaaData);
			}
		}
		ilRc = true;
	}

	return ilRc;
}
void CedaUaaData::SetPrefix(UAADATA &prpUaa)
{
	CString olData = CString(prpUaa.Data);
	olData.TrimLeft(); 
	olData.TrimRight(); 

	if(olData == "*")
	{
		strcpy(prpUaa.Pref,"*");
		olAllCount++;
	}

	if(olData == "")
	{
		strcpy(prpUaa.Pref," ");
		olNoneCount++;
	}

	if(olData.GetLength() > 2)
	{
		CString prefix = olData.Left(1);
		if(prefix=="+")
		{
			strcpy(prpUaa.Pref,"+");
			olPlusCount++;
		}
		if(prefix=="-")
		{
			strcpy(prpUaa.Pref,"-");
			olMinusCount++;
		}
	}
}
bool CedaUaaData::AddProfileList(CString& prpProfileList)
{
	omProfiles.RemoveAll();
	CString olKey,olValue;
	ExtractItemList(prpProfileList,&omProfiles,';');
	return true;
}

CString CedaUaaData::GetProfileList()
{
	 CString strProfiles("");
	int ilProfileCount = omProfiles.GetSize();

	for (int ilLc = 0; ilLc < ilProfileCount; ilLc++)
	{

		CString strTmp("");
		strTmp.Format("%s", omProfiles[ilLc]);

		if(strProfiles.GetLength()>0)
		{
			strProfiles += ",'" + strTmp + "'";
		}
		else
		{
			strProfiles += "'" + strTmp + "'";
		}
	}

	return strProfiles;
}

bool CedaUaaData::ProcessAirlineCodes()
{
	CStringArray popStrArray;
	GetAccessibleAlt(&popStrArray);

	CString olAlc2,olAlc3,olUrno;
	
	int ilCount = ogBCD.GetDataCount("ALT");
	for (int i = 0; i < ogBCD.GetDataCount("ALT"); i++)
	{
		olAlc2 = ogBCD.GetField("ALT",i,"ALC2");
		olAlc3 = ogBCD.GetField("ALT",i,"ALC3");
		olUrno = ogBCD.GetField("ALT",i,"URNO");

		ALADATA *prlAla = new ALADATA();
		strcpy(prlAla->ALC2,olAlc2);
		strcpy(prlAla->ALC3,olAlc3);
		prlAla->Urno = olUrno.GetLength()>0 ? atol(olUrno) : 0;

		int iID = FindIdInStrArray(popStrArray, olUrno);

		bool isVisible = iID == -1 ? false : true;
		prlAla->IsVisible = isVisible;

		InsertInternalAirline(prlAla);
	}

	return true;
}


CString CedaUaaData::GetAccessibleAlt( CStringArray *popStrArray)
{
	popStrArray->RemoveAll();

	//Get all the Airline Urno
	CStringArray olTmpUrno;
	CStringArray olTmpShowUrno; //Only used to show Urno
	

	CString olAlc2,olAlc3,olUrno;
	for (int i = 0; i < ogBCD.GetDataCount("ALT"); i++)
	{
		olAlc2 = ogBCD.GetField("ALT",i,"ALC2");
		olAlc3 = ogBCD.GetField("ALT",i,"ALC3");
		olUrno = ogBCD.GetField("ALT",i,"URNO");
		olTmpUrno.Add(olUrno);
		olTmpShowUrno.Add(olUrno);
		
	}

	//* found 1st priority
	if(ogUaaData.olAllCount > 0)
	{
		popStrArray->Append(olTmpUrno);
		return "";
	}
	

	int ilCount = omData.GetSize();
	int ilMinusCount=0;
	//Searching for - sign
	UAADATA *prlUaa;
	for (i = 0; i < ilCount; i++)
	{
		prlUaa = &omData[i];
		CString olData = CString(prlUaa->Data);
		olData.TrimLeft(); 
		olData.TrimRight(); 
		//CString strPrefix= CString(prlUaa->Pref);
		CString strPrefix;

		if(olData.GetLength() > 0)
		{
			strPrefix = olData.Left(1);
		}

		if(strPrefix == "-")
		{
			
			if(olData.GetLength() > 2)
			{
				CStringArray olStrArray;
				CString olAirlines = olData.Right(olData.GetLength()-2);
				int ilSizeArray = ExtractItemList(olAirlines, &olStrArray, ':');
				
				//Copy tmp array
				CStringArray olTmp;
				olTmp.Append(olTmpUrno);

				for (int l=0; l<ilSizeArray; l++)
				{
					//Can be ALC2 or ALC3
					CString olAlc = olStrArray.GetAt(l);
					CString olUrno("");
					if (! ogBCD.GetField("ALT", "ALC3", olAlc, "URNO", olUrno))
						ogBCD.GetField("ALT", "ALC2", olAlc, "URNO", olUrno);

					//Find in Accessible array if not removing the Urno
					int iID = -1;

					if(ilMinusCount==0)
					{
						iID = FindIdInStrArray(olTmpShowUrno, olUrno);
					}
					else
					{
						iID = FindIdInStrArray(olTmp, olUrno);
					}
				
					if(iID > -1 ) //Found first record of minus
					{
						if(ilMinusCount==0)
						{
							olTmpShowUrno.RemoveAt(	iID,1);
						}
						else
						{
							olTmp.RemoveAt(iID,1);
						}
					}
					else
					{
						//if not found already removed
					}
				}

				if(ilMinusCount>0)
				{
					//Looping the olTmp to get all accessible Urno
					for (int i=0; i<olTmpShowUrno.GetSize(); i++)
					{
						CString olAlcUrno = olTmpShowUrno.GetAt(i);					
						int iID = FindIdInStrArray(olTmp, olAlcUrno);					
						if(iID > -1)
						{
							olTmp.RemoveAt(	iID,1);
						}
						else
						{

						}
					}

					for (int k=0; k<olTmp.GetSize(); k++)
					{
						CString olAlcUrno = olTmp.GetAt(k);
						olTmpShowUrno.Add(olAlcUrno);
					}
				}
			}

			ilMinusCount++;
		}
	}

	
	CStringArray olPlusUrno;
	for (i = 0; i < ilCount; i++)
	{
		prlUaa = &omData[i];
		CString olData = CString(prlUaa->Data);
		olData.TrimLeft(); 
		olData.TrimRight(); 

		//CString strPrefix= CString(prlUaa->Pref);
		CString strPrefix;

		if(olData.GetLength() > 0)
		{
			strPrefix = olData.Left(1);
		}


		if( strPrefix== "+")
		{
			CStringArray olStrArray;
			CString olAirlines = olData.Right(olData.GetLength()-2);
			int ilSizeArray = ExtractItemList(olAirlines, &olStrArray, ':');
			for (int j=0; j<ilSizeArray; j++)
			{
				CString olAlc = olStrArray.GetAt(j);
				CString olUrno("");
				if (! ogBCD.GetField("ALT", "ALC3", olAlc, "URNO", olUrno))
					ogBCD.GetField("ALT", "ALC2", olAlc, "URNO", olUrno);
				
				int iID = FindIdInStrArray(olPlusUrno, olUrno);
				if(iID == -1)
				{
					olPlusUrno.Add(olUrno);
				}

			}

		}
	}

	//Need to consolidate the accessible urno
	if(ogUaaData.olNoneCount > 0)
	{
		popStrArray->RemoveAll();
	}

	if(ogUaaData.olMinusCount > 0)
	{
		for (int i=0; i<olTmpShowUrno.GetSize(); i++)
		{
			CString olAlcUrno = olTmpShowUrno.GetAt(i);					
			int iID = FindIdInStrArray(*popStrArray, olAlcUrno);					
			if(iID == -1)
			{
				popStrArray->Add(olAlcUrno);
			}
		}
	}

	if(ogUaaData.olPlusCount > 0)
	{
		for (int i=0; i<olPlusUrno.GetSize(); i++)
		{
			CString olAlcUrno = olPlusUrno.GetAt(i);					
			int iID = FindIdInStrArray(*popStrArray, olAlcUrno);					
			if(iID == -1)
			{
				popStrArray->Add(olAlcUrno);
			}
		}
	}

	return "";
}

bool CedaUaaData::IsVisible(CString olAlc2,CString olAlc3)
{
	bool bVisible = false;

	CString olUrno("");
	if (! ogBCD.GetField("ALT", "ALC3", olAlc3, "URNO", olUrno))
		ogBCD.GetField("ALT", "ALC2", olAlc2, "URNO", olUrno);

	long lUrno = olUrno.GetLength()>0 ? atol(olUrno) : 0;

	ALADATA *prlAla = GetAlaByUrno(lUrno);
	if (prlAla != NULL)
	{
		bVisible = prlAla->IsVisible;
	}	
	return bVisible;
}
void CedaUaaData::ClearAll(void)
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();

	omProfiles.RemoveAll();
	omAlineData.DeleteAll();
	omAlineMap.RemoveAll();
}

void CedaUaaData::InsertInternal(UAADATA *prpUaa)
{
	UAADATA *prlUaa = GetUaaByUrno(prpUaa->Urno);
	if (prlUaa == NULL)
	{
		omData.NewAt(omData.GetSize(), *prpUaa);
		omUrnoMap.SetAt((void *)prpUaa->Urno,prpUaa);
	}
	
}

UAADATA *CedaUaaData::GetUaaByUrno(long opUrno)
{
	UAADATA  *prlUaa;
	if (omUrnoMap.Lookup(&opUrno,(void *& )prlUaa) == TRUE)
	{
		return prlUaa;
	}
	return NULL;
}

void CedaUaaData::InsertInternalAirline(ALADATA *prpAla)
{
	ALADATA *prlAla = GetAlaByUrno(prpAla->Urno);
	if (prlAla == NULL)
	{
		omAlineData.NewAt(omAlineData.GetSize(), *prpAla);
		omAlineMap.SetAt((void *)prpAla->Urno,prpAla);
	}
	
}

ALADATA *CedaUaaData::GetAlaByUrno(long opUrno)
{
	ALADATA  *prlALA;
	
	if (omAlineMap.Lookup((void *)opUrno,(void *& )prlALA) == TRUE)
	{
		return prlALA;
	}
	return NULL;
}


