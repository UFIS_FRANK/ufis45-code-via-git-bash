#ifndef _DATASET_H_
#define _DATASET_H_


#include <ccsglobl.h>
#include <BasicData.h>
#include <RecordSet.h>
#include <CcaCedaFlightData.h>
#include <DiaCedaFlightData.h>
#include <CcaDiaViewer.h>
#include <ChaDiaViewer.h>
#include <CCSEdit.h>


#define REGEL_T			21
#define REGEL_A			12 //Annahme
#define REGEL_B			12 //Bereitstellung

#define REGEL_T_GHP			31
#define REGEL_A_GHP			17 //Annahme
#define REGEL_B_GHP			17 //Bereitstellung



#define EQUAL				0
#define FIND_IN_FLIGHT		1
#define FIND_IN_RULE		2
#define FIND_IN_FLIGHT_SPEZ	3

struct PREMIS_ORDER
{
	RecordSet Pgr;
	char	Prio[REGEL_T+1];
	int		UserPrio;
	PREMIS_ORDER(void)
	{
		UserPrio = 0;
		strcpy(Prio, "0000000000000000000");
	}
};


struct RULES_ENTRY
{
	char Prio;			// M or S
	int  Weight;
	char Type;          // E-->Single G-->Group
	CString Urno;
	CString Pnam;
};


/////////////////////////////////////////////////////////////////////////////
// DataSet class

class DataSet
{
// Job creation routine
public:
	DataSet();
	~DataSet();

	long omInternalUrno;

	
	//void GetGhdDataForFlight(char *pspRuleId, long lpRedl, CCSPtrArray<GHDDATA> &olGhds, RKEYLIST *popRotation/*FLIGHTDATA *prpFlight*/, BOOL blCheck, char *pspType = "J");
	//pspState = Ergebnisparameter ==> "NF" ==> NOT FOUND
	//								   "DF" ==> DUPLICATE FOUND

	
	//bool GetGhpDataForFlight(CCSPtrArray<GPMDATA> &ropGpmData, char *pspRuleId, long lpRedl, char *pspDurt, FLIGHTDATA *prpFlightA = NULL, FLIGHTDATA *prpFlightD = NULL, char *pspState = NULL);

	bool DeAssignCca(long lpCcaUrno);


	CStringArray omCcaConflictTextList;
	void GetConflictBrushByCcaStatus(DIACCADATA *prpCca, CcaDIA_BARDATA *prlBar);
	int GetFistCcaConflict(DIACCADATA *prpCca);
	CString GetRulesName(CCSPtrArray<RecordSet> *popGPRs, CString opTable = "PGR") ;

	bool Check(CString &opRuleValue, CString &opFlightValue, int ilCompare, int ipPrio, char *pcpPrio);

	int GetGPMs(CString opRkey, CCSPtrArray<RecordSet> *popGPMs) ;

	void InitCcaConflictStrings()
	{
		omCcaConflictTextList.RemoveAll();
		omCcaConflictTextList.Add(GetString(IDS_STRING1466));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(GetString(IDS_STRING1467));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
	}

	void GetConflictBrushByChaStatus(CHADATA *prpCca, ChaDIA_BARDATA *prlBar);
	
	bool GetGhpDataForFlight(CCSPtrArray<RecordSet> *polGhps, const CCAFLIGHTDATA *prpFlightA = NULL, const CCAFLIGHTDATA *prpFlightD = NULL);
	bool GetGatposCCAGhpDataForFlight(const long& lpGhpUrno, const DIAFLIGHTDATA *prpFlightD, bool& bpPcadFailed, bool& bpGcadFailed, bool& bpWcadFailed);

	bool CheckCCaForOverlapping(CString opCkic);


	void RelHdl( const CCAFLIGHTDATA *prpFlightD , bool bpDdx = false);
	bool CheckDemands( const CCAFLIGHTDATA *prpFlightD, const CCSPtrArray<CCADATA> &ropFlightCcaList, CCSPtrArray<CCADATA> &ropFlightCcaDemList);
	bool CheckDemands( const CCAFLIGHTDATA *prpFlightD , CCSPtrArray<DIACCADATA> &olCcaChangedList);
	void CheckDemandsOld( const CCAFLIGHTDATA *prpFlightD , CCSPtrArray<DIACCADATA> &olCcaChangedList);
	bool GetCcaRes(CCADATA* prpCca, CMapStringToString& ropMapBLUE, CMapStringToString& ropMapRED, CStringArray& opBLUEArray, bool bpOnlyResChain = false);

//	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns, bool bpWithEmptyFields = false);


	void Userallocatevector(CString userdatei);

	void Init();

	static int ilGhpDopaIdx;
	static int ilGhpDopdIdx;
	static int ilGhpMaxtIdx;
	static int ilGhpRegnIdx;
	static int ilGhpAct5Idx;
	static int ilGhpActmIdx;
	static int ilGhpFlcaIdx;
	static int ilGhpFlnaIdx;
	static int ilGhpFlsaIdx;
	static int ilGhpFlcdIdx;
	static int ilGhpFlndIdx;
	static int ilGhpFlsdIdx;
	static int ilGhpAlmaIdx;
	static int ilGhpAlmdIdx;
	static int ilGhpOrigIdx;
	static int ilGhpDestIdx;
	static int ilGhpVi1aIdx;
	static int ilGhpVi1dIdx;
	static int ilGhpTrraIdx;
	static int ilGhpTrrdIdx;
	static int ilGhpSccaIdx;
	static int ilGhpSccdIdx;
	static int ilGhpPcaaIdx;
	static int ilGhpPcadIdx;
	static int ilGhpGcadIdx;
	static int ilGhpWcadIdx;
	static int ilGhpTtpaIdx;
	static int ilGhpTtpdIdx;
	static int ilGhpTtgaIdx;
	static int ilGhpTtgdIdx;
	static int ilGhpHtpaIdx;
	static int ilGhpHtpdIdx;
	static int ilGhpHtgaIdx;
	static int ilGhpHtgdIdx;
	static int ilGhpPrsnIdx;
	static int ilGhpVafrIdx;
	static int ilGhpVatoIdx;
	static int ilGhpPrioIdx;

	static int ilGhpFliaIdx;
	static int ilGhpFlidIdx;

	std::vector <CString> vData;

};

#endif	// _DATASET_H_
