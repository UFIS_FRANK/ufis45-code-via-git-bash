// TimeRange.h: interface for the CTimeRange class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TIMERANGE_H__75A55CA5_BCE3_4A73_B174_B687C479129F__INCLUDED_)
#define AFX_TIMERANGE_H__75A55CA5_BCE3_4A73_B174_B687C479129F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTimeRange  
{
public:
	CTimeRange();
	CTimeRange( const CTime &start, const CTime &end );		// create a time range with sorted times
	virtual ~CTimeRange();

//protected:
	CTime StartTime ;
	CTime EndTime ;

public:
	bool IsInRange( const CTime &time );
	bool IsInRange( const CTimeRange &range );		// completely within ?
	bool IsOverlapping( const CTimeRange &range );		// some overlapping ?
	bool IsEmpty();		// this can only occure on both start and end times are equal
	CTimeRange Clip( CTimeRange &range );		// return the timerange that belongs to both ranges
	CString Dump();
	CTimeSpan GetTimeSpan();		// conversion helper
	bool IsReverse();		// says if the end is before the beginning
};

#endif // !defined(AFX_TIMERANGE_H__75A55CA5_BCE3_4A73_B174_B687C479129F__INCLUDED_)
