// HEADER 
#ifndef _CEDABWADATA_H_ 
#define _CEDABWADATA_H_ 

#include <CCSCedaData.h> 

struct BWADATA 
{
	long 	 Rurn; 	// Eindeutige Datensatz-Nr.
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Tabn[4]; 	
	char 	 Stat[2]; 	
	char 	 Dele[2]; 	
	CTime	Vato;
	CTime	Vafr;
	bool	IsAct;
	bool	IsAlt;
	bool	IsAcr;
	bool	IsApt;
	CString	Act3;
	CString	Act5;
	CString	Alc2;
	CString	Alc3;
	CString	Regn;
	CString	Apc3;
	CString	Apc4;


	//DataCreated by this class
	int      IsChanged;

	BWADATA(void)
	{ 
		//memset(this,'\0',sizeof(*this));
		Vato = TIMENULL;	
		Vafr = TIMENULL;	
		IsAct = false;
		IsAcr = false;
		IsAlt = false;
		IsApt = false;
		IsChanged = DATA_UNCHANGED;
	}

}; // end BWADATA

class CedaBwaData : public CCSCedaData
{
public:
	CedaBwaData();
	~CedaBwaData();

	CCSPtrArray<BWADATA> omData;
    CMapPtrToPtr omUrnoMap;
	void ReadAll();
	void Clear();

	char pcmFList[512];

	bool ResolveBwa(  BWADATA *prpBwa);

	char CheckRegn(CTime opSto, CString olRegn );
	char CheckAlc(CTime opSto, CString olAlc2, CString olAlc3 );
	char CheckAct(CTime opSto, CString olAct3, CString olAct5 );
	char CheckApt(CTime opSto, CString olAct3, CString olAct5 );

	void DeleteInternal(BWADATA *prpBwa);
	void InsertInternal(BWADATA *prpBwa);
	bool UpdateInternal(BWADATA *prpBwa);

	//////////////////////////////////////

	void Register();
	void UnRegister();

	void ProcessBwaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	BWADATA *GetBwaByUrno(long lpUrno);

};

extern CedaBwaData ogBwaData;

#endif // _CEDABWADATA_H_ 
