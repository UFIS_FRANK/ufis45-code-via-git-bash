// Utils.cpp : Defines utility functions
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <BasicData.h>

#include <Utils.h>
#include <DiaCedaFlightData.h>
#include <resrc1.h>
#include <CedaDiaCcaData.h>
#include <CcaCedaFlightData.h>
#include <fpms.h>
#include <process.h>
#include <CedaFlightUtilsData.h>
#include <ExpandOverSeasons.h>
#include <SeasonDlgCedaFlightData.h>
#include <ButtonListDlg.h>
#include <BltDiagram.h>
#include <GatDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <CedaBlaData.h>
#include <CedaGrmData.h>



static int CompareTifd(const FLIGHTUTILSDATA **e1, const FLIGHTUTILSDATA **e2)
{
		return ((**e1).Tifd == (**e2).Tifd)? 0: 
				((**e1).Tifd > (**e2).Tifd)? 1: -1;
}

static int CompareTifa(const FLIGHTUTILSDATA **e1, const FLIGHTUTILSDATA **e2)
{
		return ((**e1).Tifa == (**e2).Tifa)? 0: 
				((**e1).Tifa >  (**e2).Tifa)? 1: -1;
}


int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr < ropStrArray[i])
		{
			break;
		}
	}
	ropStrArray.InsertAt(i, ropStr);
	return i;
}


bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return true;
	}
	return false;
}

void ActivateTimers()
{
	if (pogButtonList)
	{
		pogButtonList->ActivateTimer();
	}

	if (pogBltDiagram)
	{
		pogBltDiagram->ActivateTimer();
	}

	if (pogGatDiagram)
	{
		pogGatDiagram->ActivateTimer();
	}

	if (pogPosDiagram)
	{
		pogPosDiagram->ActivateTimer();
	}

	if (pogWroDiagram)
	{
		pogWroDiagram->ActivateTimer();
	}
}

void DeActivateTimers()
{
	if (pogButtonList)
	{
		pogButtonList->DeActivateTimer();
	}

	if (pogBltDiagram)
	{
		pogBltDiagram->DeActivateTimer();
	}

	if (pogGatDiagram)
	{
		pogGatDiagram->DeActivateTimer();
	}

	if (pogPosDiagram)
	{
		pogPosDiagram->DeActivateTimer();
	}

	if (pogWroDiagram)
	{
		pogWroDiagram->DeActivateTimer();
	}
}

int GetSpecialREQ (const CString& opReq1, const CString& opReq2, CString& opBarSpecialREQ, int& ipRead, int& ipNotRead)
{
//	CString olReq = popFlight->Sreq;
//	olReq = ",5,10";

	int ilReturn = -1;
	ipRead = 0;
	ipNotRead = 0;

	if (!opReq1.IsEmpty())
	{
		CStringArray olReqArray;
		int ilSizeArray = SplitItemList(opReq1, &olReqArray, 1);
		if (ilSizeArray != 3)
		{
			opBarSpecialREQ = "";
			ilReturn = NO_REQ;
		}
		else
		{
			CString olRead = olReqArray.GetAt(1);
			CString olNotRead = olReqArray.GetAt(2);
			ipRead = atoi(olRead);
			ipNotRead = atoi(olNotRead);

			if ( (ipRead+ipNotRead) == 0)
			{
				opBarSpecialREQ = "";
				ilReturn = NO_REQ;
			}
			else
			{
				opBarSpecialREQ = olReqArray.GetAt(1) + CString(",") + olReqArray.GetAt(2);
				if (ipNotRead > 0)
					ilReturn = NOT_ALL_READ;
				else
					ilReturn = REQ;
			}
		}
	}

	if (!opReq2.IsEmpty())
	{
		CStringArray olReqArray;
		int ilSizeArray = SplitItemList(opReq2, &olReqArray, 1);
		if (ilSizeArray != 3)
		{
			opBarSpecialREQ = "";
			ilReturn = NO_REQ;
		}
		else
		{
			CString olRead = olReqArray.GetAt(1); 
			CString olNotRead = olReqArray.GetAt(2);
			ipRead += atoi(olRead);
			ipNotRead += atoi(olNotRead);

			if ( (ipRead+ipNotRead) == 0)
			{
				opBarSpecialREQ = "";
				ilReturn = NO_REQ;
			}
			else
			{
				char buffer[64];
				itoa(ipRead, buffer, 10);
				olRead = CString(buffer);

				itoa(ipNotRead, buffer, 10);
				olNotRead = CString(buffer);

				opBarSpecialREQ = olRead + CString(",") + olNotRead;
				if (ipNotRead > 0)
					ilReturn = NOT_ALL_READ;
				else
					ilReturn = REQ;
			}
		}
	}

	return ilReturn;
}

int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return i;
	}
	return -1;
}


bool IsRegularFlight(char Ftyp)
{
	return (Ftyp != 'T' && Ftyp != 'G');
}


bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsArrival(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsArrival(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpDes, pcgHome) == 0);
}



bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsDeparture(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsDeparture(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpOrg, pcgHome) == 0);
}
 

bool IsCircular(const char *pcpOrg, const char *pcpDes)
{
	return (IsArrival(pcpOrg, pcpDes) && IsDeparture(pcpOrg, pcpDes));
}


bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsCircular(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));
}


bool DelayCodeToAlpha(char *pcpDcd)
{
	CString olDcdAlpha;
	if (!ogBCD.GetField("DEN", "DECN", CString(pcpDcd), "DECA", olDcdAlpha))
		return false;

	strncpy(pcpDcd, olDcdAlpha, 3);  
	return true;
}


bool DelayCodeToNum(char *pcpDcd)
{
	CString olDcdNum;
	if (!ogBCD.GetField("DEN", "DECA", CString(pcpDcd), "DECN", olDcdNum))
		return false;

	strncpy(pcpDcd, olDcdNum, 3);  
	return true;
}



int FindInListBox(const CListBox &ropListBox, const CString &ropStr)
{
	int ilCount = ropListBox.GetCount();
	CString olItem;
	for (int i = 0; i < ilCount; i++)
	{
		ropListBox.GetText(i, olItem);
		if (olItem == ropStr)
			return i;
	}
	return -1;
}


// A substitute for CListBox::GetSelItems(...)
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems)
{
	int ilAnz = 0;
	// Scan all Items
	for (int ilLBCount = 0; ilLBCount < ropLB.GetCount(); ilLBCount++)
	{
		if (ilAnz >= ipMaxAnz) break;

		if (ropLB.GetSel(ilLBCount) > 0)
		{
			// store index in array
			prpItems[ilAnz] = ilLBCount;
			ilAnz++;
		}
	}
	// return the count of selected items
	return ilAnz;
}


COLORREF GetColorOfFlight(char FTyp)
{
	switch (FTyp)
	{
	case 'S': return COLOR_FLIGHT_SCHEDULED;
	case ' ':
	case '\0': return COLOR_FLIGHT_PROGNOSE;
	case 'D': return COLOR_FLIGHT_DIVERTED;
	case 'R': return COLOR_FLIGHT_REROUTED;
	case 'B': return COLOR_FLIGHT_RETTAXI;
	case 'Z': return COLOR_FLIGHT_RETFLIGHT;
	case 'X': return COLOR_FLIGHT_CXX;
	case 'N': return COLOR_FLIGHT_NOOP;
	}

	return COLOR_FLIGHT_OPERATION;
 
}


CString GetUrnoListFromSelection(const CString &ropSel)
{
	int ilFirst = ropSel.Find("(");
	int ilLast = ropSel.Find(")");
	if (ilFirst == -1) return "";
	if (ilLast == -1) return "";

	return  ropSel.Mid(ilFirst +1 , ilLast - ilFirst -1);
}




long MyGetUrnoFromSelection(const CString &ropSel, CString opKeyField)
{

	if(ropSel.Find(" IN ") >= 0) return 0;	
	
	int ilFirst = ropSel.Find(opKeyField);
	if (ilFirst == -1) return -1;
	ilFirst += 4;

	CString olNums("0123456789");

	while (ilFirst < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilFirst]) != -1)
			break;
		ilFirst++;
	}
	if (ilFirst >= ropSel.GetLength())
		return -1;

	int ilLast = ilFirst;
	while (ilLast < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilLast]) == -1)
			break;
		ilLast++;
	}

	long llUrno = atol(ropSel.Mid(ilFirst,ilLast-ilFirst));
	if (llUrno == 0)
		return -1;
	else
		return llUrno;
}








long MyGetUrnoFromSelection(const CString &ropSel)
{

	if(ropSel.Find(" IN ") >= 0) return 0;	
	
	int ilFirst = ropSel.Find("URNO");
	if (ilFirst == -1) return -1;
	ilFirst += 4;

	CString olNums("0123456789");

	while (ilFirst < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilFirst]) != -1)
			break;
		ilFirst++;
	}
	if (ilFirst >= ropSel.GetLength())
		return -1;

	int ilLast = ilFirst;
	while (ilLast < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilLast]) == -1)
			break;
		ilLast++;
	}

	long llUrno = atol(ropSel.Mid(ilFirst,ilLast-ilFirst));
	if (llUrno == 0)
		return -1;
	else
		return llUrno;
}

bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura, CString opAdid, CString opBaa4, CString opBaa5) 
{
	ASSERT(FALSE);
	return false;

	CString olDura;
	int ilDura = 0;
	
	if (opAdid == "A")
		ilDura = atoi(opBaa4);
	else if (opAdid == "D")
		ilDura = atoi(opBaa5);

	if (ilDura > 0)
	{
		ropDura = CTimeSpan(0, 0, ilDura, 0);
	}
	else
	{
		// if no minimal ground time and no default allocation duration exists
		// get the hard coded default allocation 
		ropDura = ogPosDefAllocDur;
	}

	return true;
}


bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("PST", "PNAM", ropPos, "DEFD", olDura))
		olDura = "0";
	
	// get the maximum of minimal ground time and default allocation duration
	int ilDura = max(atoi(olDura), ipMinGT);

	if (ilDura > 0)
	{
		ropDura = CTimeSpan(0, 0, ilDura, 0);
	}
	else
	{
		// if no minimal ground time and no default allocation duration exists
		// get the hard coded default allocation 
		ropDura = ogPosDefAllocDur;
	}

	return true;
}


bool GetGatDefAllocDur(const CString &ropGate, CTimeSpan &ropDura, bool bpArrival)
{
	CString olDura;

	if(bpArrival)
	{
		ropDura = CTimeSpan(0, 0, 15, 0);
		return true;
	}

	if (!ogBCD.GetField("GAT", "GNAM", ropGate, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogGatDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}

bool CheckGroupForValue(char *pcpGrpn,char *pcpAppl, char *pcpTabn,const char *pcpValue)
{

	char clValue[32];

 	strcpy(clValue,pcpValue);
	CCSPtrArray<RecordSet> olGrnRecords;

	 ogBCD.GetRecords("GRN", "GRPN", pcpGrpn, &olGrnRecords);
	//datensatz der Grp
	if (olGrnRecords.GetSize() > 0)
	{
		CString olGrpn = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "GRPN")];
		CString olAppl = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "APPL")];
		CString olTabn = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "TABN")];

		if (!strcmp(olTabn,pcpTabn) && !strcmp(olGrpn,pcpGrpn) && !strcmp(olAppl,pcpAppl))
		{
			CString olGroup = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "URNO")];
			long llGroup = atol(olGroup);

			if (ogGrmData.IsValueInGroup(llGroup,clValue))
			{
				olGrnRecords.DeleteAll();
				return true;
			}
		}
		//datensatz von rules
	}
	olGrnRecords.DeleteAll();
	return false;
}

bool GetBltDefAllocDur(const char *pcpFlightId, const char *pcpAct3, const char *pcpAct5,
					   const char *pcpApc3, const char *pcpApc4, const CString &ropBlt, CTimeSpan &ropDura)
{

	CString olAptUrno;

	CString olActUrno = ogBCD.GetFieldExt("ACT","ACT3","ACT5",pcpAct3,pcpAct5,"URNO");
		olAptUrno = ogBCD.GetFieldExt("APT","APC3","APC4",pcpApc3,pcpApc4,"URNO");

	for ( int ilIndex = 0; ilIndex < ogBlaData.omData.GetSize(); ilIndex++ )
	{
		 BLADATA *prmBla = &ogBlaData.omData[ilIndex];

		 if (!strcmp(prmBla->Ftyp,pcpFlightId))
		 {
			 // CheckGroupForValue(char *pcpGrpn,char *pcpAppl, char *pcpTabn, char *pcpValue)
				 if ( CString(prmBla->Agrp).IsEmpty() ||  CheckGroupForValue(prmBla->Agrp,"POPS", "ACTTAB", (LPCTSTR) olActUrno))
					 if ( CString(prmBla->Ogrp).IsEmpty() || CheckGroupForValue(prmBla->Ogrp,"POPS", "APTTAB",(LPCTSTR) olAptUrno))
					 {
						 ropDura = CTimeSpan(0, 0,prmBla->Time, 0);
						 return true;
					 }
		 }

// 	ogBCD.GetRecords("GRN", "GRPN", olGurn, &olGrnRecords);

	}
	return GetBltDefAllocDur(ropBlt,ropDura);

}

bool GetBltDefAllocDur(const CString &ropBlt, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("BLT", "BNAM", ropBlt, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogBltDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetWroDefAllocDur(const CString &ropWro, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("WRO", "WNAM", ropWro, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogWroDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn)
{
	int ilFltn = atoi(ropUserFltn);
	ropDBFltn.Format("%03d", ilFltn);

	return true;
}



bool GetCurrentUtcTime(CTime &ropUtcTime)
{
	ropUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(ropUtcTime);
	return true;
}

// true/false; outside/inside the timelimit
bool IsPostFlight(const CTime& opTimeFromFlight, CString opPrfl)
{
	#ifdef _FIPSVIEWER
	{
		return true;
	}
	#else
	{
	}
	#endif

	if (bgCheckPRFL)
	{
		if (opPrfl == "U" || opPrfl == " ")
		{
//			return false;
		}
		else
			return true;
	}


	if (opTimeFromFlight == TIMENULL)
		return false;


	if( __CanHandlePositionFlightTodayOnlyCheck() )		// 050310 MVy: today only or period for position flight check
	{
		// 050309 MVy: today only or period, check if today
		if( ogArchivePostFlightTodayOnly == TRUE )
		{
			CTime olUtcTime;
			GetCurrentUtcTime(olUtcTime);
			return olUtcTime.GetDay() != opTimeFromFlight.GetDay();
		};
	};

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

//	die prfung zur archivierung soll auf ganze tage gesetzt werden 
	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight < olBegin)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight > olEnd)
			return true;
	}
	
	return false;
}

// true/false; outside/inside the timelimit
bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime, CString opPrfl)
{
	#ifdef _FIPSVIEWER
	{
		return true;
	}
	#else
	{
	}
	#endif

	if (bgCheckPRFL)
	{

		if (opPrfl == "U" || opPrfl == " ")
		{
//			return false;
		}
		else
			return true;
	}

	if( __CanHandlePositionFlightTodayOnlyCheck() )		// 050310 MVy: today only or period for position flight check
	{
		// 050309 MVy: today only or period, check if today
		if( ogArchivePostFlightTodayOnly == TRUE )
		{
			CTime olUtcTime;
			GetCurrentUtcTime(olUtcTime);
			return olUtcTime.GetDay() != opTimeFromFlight.GetDay();
		};
	};

	if (opTimeFromFlight == TIMENULL)
		return false;

	CTime olTimeFromFlight (opTimeFromFlight);

	if (bpLocalTime)
		ogBasicData.LocalToUtc(olTimeFromFlight);

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

//	die prfung zur archivierung soll auf ganze tage gesetzt werden 
	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	CTime olBeginDay (olBegin.GetYear(),olBegin.GetMonth(),olBegin.GetDay(),0,0,0);
	CTime olEndDay   (olEnd.GetYear(),olEnd.GetMonth(),olEnd.GetDay(),23,59,0);

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (olTimeFromFlight < olBeginDay)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (olTimeFromFlight > olEndDay)
			return true;
	}

	return false;
}



// true/false; outside/inside the timelimit
bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime)
{
	#ifdef _FIPSVIEWER
	{
		return true;
	}
	#else
	{
	}
	#endif

	if( __CanHandlePositionFlightTodayOnlyCheck() )		// 050310 MVy: today only or period for position flight check
	{
		// 050309 MVy: today only or period, check if today
		if( ogArchivePostFlightTodayOnly == TRUE )
		{
			CTime olUtcTime;
			GetCurrentUtcTime(olUtcTime);
			return olUtcTime.GetDay() != opTimeFromFlight.GetDay();
		};
	};

	if (opTimeFromFlight == TIMENULL)
		return false;

	CTime olTimeFromFlight (opTimeFromFlight);

	if (bpLocalTime)
		ogBasicData.LocalToUtc(olTimeFromFlight);

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

//	die prfung zur archivierung soll auf ganze tage gesetzt werden 
	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	CTime olBeginDay (olBegin.GetYear(),olBegin.GetMonth(),olBegin.GetDay(),0,0,0);
	CTime olEndDay   (olEnd.GetYear(),olEnd.GetMonth(),olEnd.GetDay(),23,59,0);

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (olTimeFromFlight < olBeginDay)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (olTimeFromFlight > olEndDay)
			return true;
	}

	return false;
}

// true/false; outside/inside the timelimit
bool IsPostFlight(const CTime& opTimeFromFlight)
{
	#ifdef _FIPSVIEWER
	{
		return true;
	}
	#else
	{
	}
	#endif

	if (opTimeFromFlight == TIMENULL)
		return false;


	if( __CanHandlePositionFlightTodayOnlyCheck() )		// 050310 MVy: today only or period for position flight check
	{
		// 050309 MVy: today only or period, check if today
		if( ogArchivePostFlightTodayOnly == TRUE )
		{
			CTime olUtcTime;
			GetCurrentUtcTime(olUtcTime);
			return olUtcTime.GetDay() != opTimeFromFlight.GetDay();
		};
	};

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

//	die prfung zur archivierung soll auf ganze tage gesetzt werden 
	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight < olBegin)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight > olEnd)
			return true;
	}
	
	return false;
}


BOOL CheckPostFlightPosDia(const long lpBarUrno, CWnd* opCWnd)
{
	BOOL blPost = FALSE;

	if (lpBarUrno > 0)
	{
		DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpBarUrno);
		if (prlFlight != NULL)
		{
			DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlight->Rkey);

			if(prlRkey != NULL)
			{
				DIAFLIGHTDATA *prmAFlight = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
				DIAFLIGHTDATA *prmDFlight = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

				if (prmAFlight && prmDFlight)
				{
					if (prmAFlight->Tifa != TIMENULL && prmDFlight->Tifd != TIMENULL)
					{
						if (IsPostFlight(prmAFlight->Tifa,false,prmAFlight->Prfl) && IsPostFlight(prmDFlight->Tifd,false,prmDFlight->Prfl))
							blPost = TRUE;
					}
					else if (prmDFlight->Tifd != TIMENULL)
					{
						if (IsPostFlight(prmDFlight->Tifd,false,prmDFlight->Prfl))
							blPost = TRUE;
					}
					else if (prmAFlight->Tifa != TIMENULL)
					{
						if (IsPostFlight(prmAFlight->Tifa,false,prmAFlight->Prfl))
							blPost = TRUE;
					}
				}
				else if (!prmAFlight && prmDFlight)
				{
					if (prmDFlight->Tifd != TIMENULL)
					{
						if (IsPostFlight(prmDFlight->Tifd,false,prmDFlight->Prfl))
							blPost = TRUE;
					}
				}
				else if (prmAFlight && !prmDFlight)
				{
					if (prmAFlight->Tifa != TIMENULL)
					{
						if (IsPostFlight(prmAFlight->Tifa,false,prmAFlight->Prfl))
							blPost = TRUE;
					}
				}
			}
		}
	}
		
	if (opCWnd)
	{
		if (blPost)
			ModifyWindowText(opCWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opCWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);
	}
	return blPost;
}

BOOL CheckPostFlightCcaDia(const long lpBarUrno, CWnd* opCWnd)
{
	BOOL blPost = FALSE; 

	if (lpBarUrno > 0)
	{
		DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(lpBarUrno);
		if(prlCca != NULL)
		{
			CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);
			if(prlFlight != NULL)
			{
				if (prlFlight->Tifa != TIMENULL && prlFlight->Tifd != TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifa,false,prlFlight->Prfl) && IsPostFlight(prlFlight->Tifd,false,prlFlight->Prfl))
						blPost = TRUE;
				}
				else if (prlFlight->Tifa != TIMENULL && prlFlight->Tifd == TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifa,false,prlFlight->Prfl))
						blPost = TRUE;
				}
				else if (prlFlight->Tifa == TIMENULL && prlFlight->Tifd != TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifd,false,prlFlight->Prfl))
						blPost = TRUE;
				}

			}
		}
		else
		{
			CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(lpBarUrno);
			if(prlFlight != NULL)
			{
				if (prlFlight->Tifa != TIMENULL && prlFlight->Tifd != TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifa,false,prlFlight->Prfl) && IsPostFlight(prlFlight->Tifd,false,prlFlight->Prfl))
						blPost = TRUE;
				}
				else if (prlFlight->Tifa != TIMENULL && prlFlight->Tifd == TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifa,false,prlFlight->Prfl))
						blPost = TRUE;
				}
				else if (prlFlight->Tifa == TIMENULL && prlFlight->Tifd != TIMENULL)
				{
					if (IsPostFlight(prlFlight->Tifd,false,prlFlight->Prfl))
						blPost = TRUE;
				}

			}
		}
	}

	if (opCWnd)
	{
		if (blPost)
			ModifyWindowText(opCWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opCWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);
	}

	return blPost;
}



BOOL ModifyWindowText(CWnd *opWnd, CString& opStrWnd, BOOL bplClean)
{
	if (opWnd)
	{
		CString olStrWndOld;
		opWnd->GetWindowText(olStrWndOld);

		if (olStrWndOld.Find(opStrWnd) < 0)
		{
			olStrWndOld += 	opStrWnd;
			if (!bplClean)
				opWnd->SetWindowText(olStrWndOld);
		}

		if (bplClean)
		{
			int i = olStrWndOld.Find(opStrWnd);
			if (i >= 0)
			{
				olStrWndOld = olStrWndOld.Left(i);
				opWnd->SetWindowText(olStrWndOld);
			}
		}

		return TRUE;
	}
	else
		return FALSE;

}



bool DBStrIsEmpty(const char *prpStr)
{
	if (prpStr == NULL) 
		return false;

	if (prpStr[0] == '\0')
		return true;
	if (prpStr[0] == ' ' && prpStr[1] == '\0')
		return true;

	return false;
}

bool CallUIFforBLK(CString& opTable, const long lpUrno)
{
/*
	if (!UIFappl)
	{
		UIFappl = new IApplication();
		UIFappl->CreateDispatch("BDPSUIF.Application");
		UIFappl->SetCaller(ogAppName);
		UIFappl->SetUsername(pcgUser);
		UIFappl->SetPassword(pcgPasswd);
	}

	if (UIFappl)
	{
		UIFappl->SetResourceTable(opTable);
		UIFappl->SetResourceUrno(lpUrno);
		UIFappl->DisplayResource();
	}
	return true;
*/
//#########################################################################
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "BDPS-UIF", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_STRING981), GetString(ST_FEHLER), MB_ICONERROR);

//	char buffer[32];
//	ltoa(prlLine->Urno, buffer, 10);
//	char s = '"';

	CString	olTables;
	olTables.Format("Resource:%s:%d",opTable,lpUrno);
	//olTables.Format("%cResource:PST:%d%c",s,prlLine->Urno,s);

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
//	sprintf(slRunTxt,"%c%s%c %c%s%c %c%s%c %c%s%c",s,ogAppName,s,s,pcgUser,s,s,pcgPasswd,s,s,olTables,s);
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

	return true;
}

CString IsDupFlights(long lpUrno, CString opAdid, CString opAlc3, CString opFltn, CString opFlns, CTime opDay, bool bpLocal /*=false*/)
{
	CString olRet;

	if (opAdid.IsEmpty() || opAlc3.IsEmpty() || opFltn.IsEmpty() || opDay == TIMENULL) 
		return olRet;

	CString olFlns = opFlns;
	CTime olDay = opDay;

	if(bpLocal)
		ogBasicData.LocalToUtc(olDay);

	if (olFlns == " " || olFlns.GetLength() == 0)
		olFlns = '#';

	CString olFkey;
	olFkey.Format("%05s%s%s%s%s", opFltn, opAlc3, olFlns, olDay.Format("%Y%m%d"),opAdid);

	char pclSelection[126];
	sprintf(pclSelection, "WHERE FKEY = '%s'", olFkey);

	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.ReadFlights(pclSelection);

	int ilCount = olFlightUtilsData.omData.GetSize();
	if(ilCount > 0)
	{
		if (ilCount == 1)
		{
			FLIGHTUTILSDATA * prlFlight = &olFlightUtilsData.omData[0];
			if (prlFlight && prlFlight->Urno == lpUrno)
				return olRet;
		}

		olRet = CString("\n") + GetString(IDS_STRING1699) + CString("\n\n");
		for(int i = 0; i < ilCount; i++)
		{
			FLIGHTUTILSDATA * prlFlight = NULL;
			prlFlight = &olFlightUtilsData.omData[i];

			if (prlFlight && prlFlight->Urno != lpUrno)
			{
				if(CString(prlFlight->Des3) == CString(pcgHome))
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stoa);
					olRet += GetString(IDS_STRING330)  + CString(":       ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stoa.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Org3) + CString("  (") + CString(prlFlight->Ftyp) + CString(")\n");
				}
				else
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stod);
					olRet += GetString(IDS_STRING338)  + CString(": ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stod.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Des3) + CString("  (") + CString(prlFlight->Ftyp) + CString(")\n");
				}
			}
		}

		olRet += CString("\n") + GetString(IDS_STRING1700);
	}

	return olRet;
}


CString IsDupFlightSeason(long lpUrno, CString opAdid, CString opAlc, CString opFltn, CString opFlns, CTime opDayBeg, CTime opDayEnd, CString opDaySel, bool bpLocal, bool bpInsertion)
{
	CString olText; 

	if (opAdid.IsEmpty() || opAlc.IsEmpty() || opFltn.IsEmpty() || opDayBeg == TIMENULL || opDayEnd == TIMENULL || opDaySel.IsEmpty()) 
		return olText;

	CString olAlc2;
	CString olAlc3;
	if (opAlc.GetLength() != 3)
	{
		olAlc2 = opAlc;
		ogBCD.GetField("ALT", "ALC2", opAlc, "ALC3", olAlc3 );
	}
	else
	{
		olAlc3 = opAlc;
		ogBCD.GetField("ALT", "ALC3", opAlc, "ALC2", olAlc2 );
	}

	if (!olAlc2.IsEmpty())
	{
		CString olFlno = CreateFlno(olAlc2, opFltn, opFlns);
		olText =  IsDupFlight2Season(lpUrno, opAdid, olFlno, opDayBeg, opDayEnd, opDaySel, bpLocal);
	}
	if (!olAlc3.IsEmpty())
	{
		CString olFlno = CreateFlno(olAlc3, opFltn, opFlns);
		olText +=  IsDupFlight2Season(lpUrno, opAdid, olFlno, opDayBeg, opDayEnd, opDaySel, bpLocal);
	}

	if (!olText.IsEmpty())
	{
		if (bpInsertion)
			olText += GetString(IDS_STRING2403);
		else
			olText += GetString(IDS_STRING2432);
	}

	return olText;
}

CString IsDupFlight2Season(long lpUrno, CString opAdid, CString opFlno, CTime opDayBeg, CTime opDayEnd, CString opDaySel, bool bpLocal)
{
	CString olRet;

	if (opAdid.IsEmpty() || opFlno.IsEmpty() || opDayBeg == TIMENULL || opDayEnd == TIMENULL || opDaySel.IsEmpty()) 
		return olRet;

	CTimeSpan olMaxDiff(1,0,0,0);

	//set begday to 00:00
	CTime olDayBeg (opDayBeg.GetYear(), opDayBeg.GetMonth(), opDayBeg.GetDay(), 0, 0, 0); 
	//set endday to 23:59
	CTime olDayEnd (opDayEnd.GetYear(), opDayEnd.GetMonth(), opDayEnd.GetDay(), 23, 59, 59); 

	//expand timeframe for differece utc-local
	CTime olBeg = olDayBeg - olMaxDiff;
	CTime olEnd = olDayEnd + olMaxDiff;

	CString olMaxStr = olEnd.Format( "%Y%m%d%H%M%S" );
	CString olMinStr = olBeg.Format( "%Y%m%d%H%M%S" );

	char pclSelection[1024];
	if (opAdid == "A")
		sprintf(pclSelection, "WHERE (STOA BETWEEN '%s' AND '%s') AND FLNO = '%s' AND FTYP IN ('O','S','X','N')", olMinStr, olMaxStr, opFlno);
	else if (opAdid == "D")
		sprintf(pclSelection, "WHERE (STOD BETWEEN '%s' AND '%s') AND FLNO = '%s' AND FTYP IN ('O','S','X','N')", olMinStr, olMaxStr, opFlno);
	else
		return olRet;

	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.ReadFlights(pclSelection);

	int ilCount = olFlightUtilsData.omData.GetSize();
	if(ilCount > 0)
	{
		if (opAdid == "A")
			olFlightUtilsData.omData.Sort(CompareTifa);

		if (opAdid == "D")
			olFlightUtilsData.omData.Sort(CompareTifd);

		if (ilCount == 1)
		{
			FLIGHTUTILSDATA * prlFlight = &olFlightUtilsData.omData[0];
			if (prlFlight && prlFlight->Urno == lpUrno)
				return olRet;
		}

		bool blFound = false;
		int ilCnt = 0;
		for(int i = 0; i < ilCount; i++)
		{
			FLIGHTUTILSDATA * prlFlight = NULL;
			prlFlight = &olFlightUtilsData.omData[i];

			if (prlFlight && prlFlight->Urno != lpUrno)
			{
				if(CString(prlFlight->Des3) == CString(pcgHome))
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stoa);

					char pclDays[10];
					GetDayOfWeek(prlFlight->Stoa, pclDays);
					
					if( opDaySel.Find(pclDays) >= 0 && (prlFlight->Stoa >= olDayBeg && prlFlight->Stoa <= olDayEnd) && strcmp(prlFlight->Adid, opAdid) == 0 )
//					if (prlFlight->Stoa.GetDay() == opDay.GetDay())
					{
						if (!blFound)
							olRet = CString("\n") + GetString(IDS_STRING1699) + CString("\n\n");

						olRet += GetString(IDS_STRING330)  + CString(":       ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stoa.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Org3) + CString("  (") + GetFtypLabel(CString(prlFlight->Ftyp)) + CString(")\n");
						blFound = true;
						ilCnt++;
						if (ilCnt > 20)
							break;
					}
				}
				else
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stod);

					char pclDays[10];
					GetDayOfWeek(prlFlight->Stod, pclDays);
					
					if( opDaySel.Find(pclDays) >= 0 && (prlFlight->Stod >= olDayBeg && prlFlight->Stod <= olDayEnd) && strcmp(prlFlight->Adid, opAdid) == 0)
//					if (prlFlight->Stod.GetDay() == opDay.GetDay())
					{
						if (!blFound)
							olRet = CString("\n") + GetString(IDS_STRING1699) + CString("\n\n");

						olRet += GetString(IDS_STRING338)  + CString(": ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stod.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Des3) + CString("  (") + GetFtypLabel(CString(prlFlight->Ftyp)) + CString(")\n");
						blFound = true;
						ilCnt++;
						if (ilCnt > 20)
							break;

					}
				}
			}
		}

		if (blFound)
		{
			olRet += CString("\n") + GetString(IDS_STRING1700);
			if (ilCnt > 20)
				olRet += CString("\n") + GetString(IDS_STRING2454);
		}
	}

	return olRet;
}


CString IsDupFlight(long lpUrno, CString opAdid, CString opAlc, CString opFltn, CString opFlns, CTime opDay, bool bpLocal, bool bpInsertion)
{
	CString olText; 

	if (opAdid.IsEmpty() || opAlc.IsEmpty() || opFltn.IsEmpty() || opDay == TIMENULL) 
		return olText;

	CString olAlc2;
	CString olAlc3;
	if (opAlc.GetLength() != 3)
	{
		olAlc2 = opAlc;
		ogBCD.GetField("ALT", "ALC2", opAlc, "ALC3", olAlc3 );
	}
	else
	{
		olAlc3 = opAlc;
		ogBCD.GetField("ALT", "ALC3", opAlc, "ALC2", olAlc2 );
	}

	if (!olAlc2.IsEmpty())
	{
		CString olFlno = CreateFlno(olAlc2, opFltn, opFlns);
		olText =  IsDupFlight2(lpUrno, opAdid, olFlno, opDay, bpLocal);
	}
	if (!olAlc3.IsEmpty())
	{
		CString olFlno = CreateFlno(olAlc3, opFltn, opFlns);
		olText +=  IsDupFlight2(lpUrno, opAdid, olFlno, opDay, bpLocal);
	}

	if (!olText.IsEmpty())
	{
		if (bpInsertion)
			olText += GetString(IDS_STRING2403);
		else
			olText += GetString(IDS_STRING2432);
	}

	return olText;
}

CString IsDupFlight2(long lpUrno, CString opAdid, CString opFlno, CTime opDay, bool bpLocal)
{
	CString olRet;

	if (opAdid.IsEmpty() || opFlno.IsEmpty() || opDay == TIMENULL) 
		return olRet;

	CTimeSpan olMaxDiff(1,0,0,0);
	CTime olBeg = opDay - olMaxDiff;
	CTime olEnd = opDay + olMaxDiff;

	CString olMaxStr = olEnd.Format( "%Y%m%d%H%M%S" );
	CString olMinStr = olBeg.Format( "%Y%m%d%H%M%S" );

	char pclSelection[1024];
	if (opAdid == "A")
		sprintf(pclSelection, "WHERE (STOA BETWEEN '%s' AND '%s') AND FLNO = '%s' AND FTYP IN ('O','S','X','N')", olMinStr, olMaxStr, opFlno);
	else if (opAdid == "D")
		sprintf(pclSelection, "WHERE (STOD BETWEEN '%s' AND '%s') AND FLNO = '%s' AND FTYP IN ('O','S','X','N')", olMinStr, olMaxStr, opFlno);
	else
		return olRet;

	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.ReadFlights(pclSelection);

	int ilCount = olFlightUtilsData.omData.GetSize();
	if(ilCount > 0)
	{
		if (ilCount == 1)
		{
			FLIGHTUTILSDATA * prlFlight = &olFlightUtilsData.omData[0];
			if (prlFlight && prlFlight->Urno == lpUrno)
				return olRet;
		}

		if (opAdid == "A")
			olFlightUtilsData.omData.Sort(CompareTifa);

		if (opAdid == "D")
			olFlightUtilsData.omData.Sort(CompareTifd);


		bool blFound = false;
		for(int i = 0; i < ilCount; i++)
		{
			FLIGHTUTILSDATA * prlFlight = NULL;
			prlFlight = &olFlightUtilsData.omData[i];

			if (prlFlight && prlFlight->Urno != lpUrno)
			{
				if(CString(prlFlight->Des3) == CString(pcgHome))
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stoa);

					if (prlFlight->Stoa.GetDay() == opDay.GetDay() && strcmp(prlFlight->Adid, opAdid) == 0 )
					{
						if (!blFound)
							olRet = CString("\n") + GetString(IDS_STRING1699) + CString("\n\n");

						olRet += GetString(IDS_STRING330)  + CString(":       ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stoa.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Org3) + CString("  (") + GetFtypLabel(CString(prlFlight->Ftyp)) + CString(")\n");
						blFound = true;
					}
				}
				else
				{
					if(bpLocal)
						ogBasicData.UtcToLocal(prlFlight->Stod);

					if (prlFlight->Stod.GetDay() == opDay.GetDay() && strcmp(prlFlight->Adid, opAdid) == 0 )
					{
						if (!blFound)
							olRet = CString("\n") + GetString(IDS_STRING1699) + CString("\n\n");

						olRet += GetString(IDS_STRING338)  + CString(": ") + CString(prlFlight->Flno) + CString(" ") + prlFlight->Stod.Format("%H:%M %d.%m.%Y") + CString("  ") + CString(prlFlight->Des3) + CString("  (") + GetFtypLabel(CString(prlFlight->Ftyp)) + CString(")\n");
						blFound = true;
					}
				}
			}
		}

		if (blFound)
			olRet += CString("\n") + GetString(IDS_STRING1700);
	}

	return olRet;
}

CString CreateFlno(CString opAlc, CString opFltn, CString opFlns)
{

	opAlc.TrimRight();	
	opFltn.TrimRight();	
	
	if(opAlc.IsEmpty() || opFltn.IsEmpty())
		return CString("");


	if(opAlc.GetLength() == 0)
		opAlc = "   ";

	if(opAlc.GetLength() == 1)
		opAlc += "  ";

	if(opAlc.GetLength() == 2)
		opAlc += " ";


	if(opFltn.GetLength() == 0)
		opFltn = "   ";

	if(opFltn.GetLength() == 1)
		opFltn = "00" + opFltn;

	if(opFltn.GetLength() == 2)
		opFltn = "0" + opFltn;

	if(opFltn.GetLength() == 3)
		opFltn += "  ";

	if(opFltn.GetLength() == 4)
		opFltn += " ";

	if(opFlns.GetLength() == 0)
		opFlns = " ";


	CString olFlno = opAlc + opFltn + opFlns;

	olFlno.TrimRight();

	if(olFlno.IsEmpty())
		return "";
	else
		return opAlc + opFltn + opFlns;

}

//TODO use the cviewer or paramters for the possible field checks and the data (at this point only tifa,tifd, ftyp can be used)
//TODO
bool NewPreSelectionCheck(CString &opData, CString &opFields, CString opFtyps, CTime opFrom, CTime opTo, CString opSelection )
{
	// return false means DB-Read has no Sinn
	// return true means DB-Read has Sinn

	CTime olTifa = TIMENULL;
	CTime olTifd = TIMENULL;
	CTime olTifaOld = TIMENULL;
	CTime olTifdOld = TIMENULL;
	CString olFtyp ("");
	CTime olTmp;
	CString olTemp;
	CString olAdid;	

	bool blTif = false;
	bool blReload = false;

	CString olStr;

	bool blChanged = false;


	CString olNotSearch = "TIFA,TIFD,STOD,STOA";

	if (egStructBcfields == EXT_CHANGED_OLDVAL)
	{
		GetListItemByField(opData, opFields, CString("ADID"), olAdid);


		if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
		{
			GetListItemByField(opData, opFields, CString("TIFA"), olTifa);
			GetListItemByField(opData, opFields, CString("#TIFA"), olTifaOld);

			if(olTifa != TIMENULL)
			{
				if (olTifa <= opTo && olTifa >= opFrom)
					blReload = true;
					
				if(olTifaOld != TIMENULL)
				{
					if (olTifaOld > opTo || olTifaOld < opFrom) 
					{
						blChanged = true;
					}
				}
			}
		}

		if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
		{
			GetListItemByField(opData, opFields, CString("TIFD"), olTifd);
			GetListItemByField(opData, opFields, CString("#TIFD"), olTifdOld);

			if(olTifd != TIMENULL)
			{
				if (olTifd < opTo && olTifd > opFrom)
					blReload = true;

				if(olTifdOld != TIMENULL)
				{
					if (olTifdOld > opTo || olTifdOld < opFrom)
					{
						blChanged = true;
					}
				}
			}
		}
		
		if(blReload && !blChanged && opSelection.GetLength() > 0 )
		{
			CStringArray olStrArray;
			int iA = ExtractItemList(opFields, &olStrArray);
			for(int i= olStrArray.GetSize() -1 ; i >= 0 ; i--)
			{
				olStr = olStrArray[i];

				if(!olStr.IsEmpty())
				{
					if(olStr[0] != '#')
						break;

					olStr.Remove('#');

					if(olNotSearch.Find(olStr) < 0 )
					{

						if(opSelection.Find(olStr) >= 0)
							blChanged = true;
					}
				}
			}

		}

		if(blReload && blChanged)
			return true;
		else
			return false;

	}
	else if (egStructBcfields == EXT_CHANGED_NEWVAL)
	{
//		CString olField = "TIFA,TIFD,FTPY,LSTU,[CF],TIFA";
//		CString olData  = "tifa,tifd,ftyp,lstu,,tifa";

		int ilPos = opFields.Find("[CF]",0);
		if (ilPos == -1)
			return false;

		CStringArray olArray;
		ExtractItemList(opFields, &olArray);

		CStringArray olArrayData;
		ExtractItemList(opData, &olArrayData);

		if (olArray.GetSize() != olArrayData.GetSize())
			return false;


		int ilIndex = -1; 
		for (int i = olArray.GetSize() - 1; i >= 0; i--)
		{
			if (olArray.GetAt(i) == "[CF]")
			{
				ilIndex = i;
				break;
			}
		}

		if (ilIndex != -1 && ilIndex < olArray.GetSize()-1)
		{
			CString olNewFields;
			CString olNewData;
			for (int i = ilIndex+1; i < olArray.GetSize(); i++)
			{
				if (i < olArray.GetSize()-1)
					olNewFields += olArray.GetAt(i) + CString(",");
				else
					olNewFields += olArray.GetAt(i);

				if (i < olArrayData.GetSize()-1)
					olNewData += olArrayData.GetAt(i) + CString(",");
				else
					olNewData += olArrayData.GetAt(i);
			}

			// if no field is set as changed -> no check possible -> don't reload
			if (	!GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
				&&	!GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) 
				&&	!GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp) )
				return false;


			GetListItemByField(opData, opFields, CString("ADID"), olAdid);

		//	if(GetListItemByField(opData, opFields, CString("#TIFA"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFA"), olTifa))
					blTif = true;

				if(olTifa != TIMENULL)
				{
					if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifa < opTo && olTifa > opFrom)
							blTif = true;
					}
				}
			}

		//	if(GetListItemByField(opData, opFields, CString("#TIFD"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFD"), olTifd))
					blTif = true;

				if(olTifd != TIMENULL)
				{
					if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifd < opTo && olTifd > opFrom)
							blTif = true;
					}
				}
			}


			if (blTif)	// data is the timeframe -> check the other values if reload
			{
				if(GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp))
				{
					if (!blReload)
					{
						if(!GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
							blReload = true;

						if(opFtyps.Find(olFtyp) >= 0 && opFtyps.Find(olTemp) >= 0)
							blReload = false;
						else
						{
							if(opFtyps.Find(olFtyp) >= 0)
								blReload = true;
						}
					}
				}
			}

			// data is in the timeframe and no reload by the other values needed
			if (blTif && !blReload)
			{
				// only if the timeframe hasn't changed -> reload
				if (	GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
					||	GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) )
					blReload = true;
			}

			return blReload;

		}
		else 
			return false;

	}

	return false;
}

bool CalculateSeasonTimes(CTime& NewBS, CTime& NewES, CTime OldBS, CTime OldES, CTime opDateES, CTimeSpan olStandardDiff, bool bpOverwrite)
{

		CString NewBSstr = NewBS.Format("%d.%m.%Y %H:%M");
		CString NewESstr = NewES.Format("%d.%m.%Y %H:%M");
		CString OldBSstr = OldBS.Format("%d.%m.%Y %H:%M");
		CString OldESstr = OldES.Format("%d.%m.%Y %H:%M");
		CString DateESstr = opDateES.Format("%d.%m.%Y %H:%M");


			if (bgGatPosLocal)
			{
				ogBasicData.UtcToLocal(OldBS); 
				ogBasicData.UtcToLocal(OldES);
			}
	CTimeSpan olOffsetBS (0,0,0,0);
	CTime olDateBS = opDateES;
	if (OldBS != TIMENULL && OldES != TIMENULL)
	{
		CTime olDayBS (OldBS.GetYear(), OldBS.GetMonth(), OldBS.GetDay(), 0, 0, 0, 0);
		CTime olDayES (OldES.GetYear(), OldES.GetMonth(), OldES.GetDay(), 0, 0, 0, 0);

		CTimeSpan olTmp = olDayES - olDayBS;
		long ilDaysOffset = olTmp.GetDays();
		olOffsetBS = CTimeSpan(ilDaysOffset,0,0,0);
		olDateBS = CTime(opDateES - olOffsetBS);
	}

	if (OldBS != TIMENULL || (!bpOverwrite && NewBS == TIMENULL) )
	{
		CTime olFrBS (OldBS.GetYear(), OldBS.GetMonth(), OldBS.GetDay(), OldBS.GetHour(), OldBS.GetMinute(), 0,0);
		CTime olToBS (olDateBS.GetYear(), olDateBS.GetMonth(), olDateBS.GetDay(), OldBS.GetHour(), OldBS.GetMinute(), 0,0);

		int ilPara;

		int ilTZA = ExpandOverSeasons::GetTimeZone(olFrBS);
		int ilTZB = ExpandOverSeasons::GetTimeZone(olToBS);

		if (ilTZA != ilTZB)
		{
			if (ilTZA == 1)
				olToBS = CTime(olDateBS.GetYear(), olDateBS.GetMonth(), olDateBS.GetDay(), OldBS.GetHour(), OldBS.GetMinute(), 0,1);
			NewBS = olToBS;
		}
		else
		{

			ilPara = ilTZA == 2 ? 1 : 0;
			olToBS = CTime(olDateBS.GetYear(), olDateBS.GetMonth(), olDateBS.GetDay(), OldBS.GetHour(), OldBS.GetMinute(), 0,ilPara);
			NewBS = olToBS;
		}

		NewBS = NewBS + olStandardDiff;
	}
	else
		NewBS = TIMENULL;

	if (OldES != TIMENULL || (!bpOverwrite && NewES == TIMENULL) )
	{
		CTime olFrES (OldES.GetYear(), OldES.GetMonth(), OldES.GetDay(), OldES.GetHour(), OldES.GetMinute(), 0,0);
		CTime olToES (opDateES.GetYear(), opDateES.GetMonth(), opDateES.GetDay(), OldES.GetHour(), OldES.GetMinute(), 0,0);

		int ilPara;

		int ilTZA = ExpandOverSeasons::GetTimeZone(olFrES);
		int ilTZB = ExpandOverSeasons::GetTimeZone(olToES);

		if (ilTZA != ilTZB)
		{
			if (ilTZA == 1)
				olToES = CTime(opDateES.GetYear(), opDateES.GetMonth(), opDateES.GetDay(), OldES.GetHour(), OldES.GetMinute(), 0,1);
			NewES = olToES;
		}
		else
		{

			ilPara = ilTZA == 2 ? 1 : 0;
			olToES = CTime(opDateES.GetYear(), opDateES.GetMonth(), opDateES.GetDay(), OldES.GetHour(), OldES.GetMinute(), 0,ilPara);
			NewES = olToES;
		}

		NewES = NewES + olStandardDiff;
	}
	else
		NewES = TIMENULL;


			if (bgGatPosLocal)
			{
				ogBasicData.LocalToUtc(OldBS); 
				ogBasicData.LocalToUtc(OldES);
				ogBasicData.LocalToUtc(NewBS); 
				ogBasicData.LocalToUtc(NewES);
			}


	return true;
}

BOOL WriteDialogToReg(CWnd* opWnd, CString& opKey)
{
	if(opWnd && ::IsWindow(opWnd->m_hWnd) )
	{
		CRect olRect;
		opWnd->GetWindowRect(&olRect);

		CWinApp* pApp = AfxGetApp();

		if (pApp)
		{
			if (pApp->WriteProfileInt(opKey, "Left", olRect.left) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Top", olRect.top) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Width", olRect.Width()) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Height",  olRect.Height()) == FALSE)
				return FALSE;

			return TRUE;
		}
		return FALSE;
	}

	return FALSE;
}

bool GetDialogFromReg(CRect& opRect, CString& opKey)
{
	CWinApp* pApp = AfxGetApp();
	if (pApp && !opKey.IsEmpty())
	{
		UINT left = pApp->GetProfileInt(opKey, "Left",  opRect.left);
		UINT top  = pApp->GetProfileInt(opKey, "Top",   opRect.top);
		UINT cx   = pApp->GetProfileInt(opKey, "Width", opRect.Width());
		UINT cy   = pApp->GetProfileInt(opKey, "Height",opRect.Height());

		opRect = CRect(left,top,left+cx,top+cy);
		return true;
	}

	return false;
}

void CallHistory(CString opField)
{

	if (opField.IsEmpty())// || bgLoacationChangesStarted)
		return;

	CString olWks = CString(ogCommHandler.pcmReqId);
	CString olListener = olWks + CString(pcgUser);

	CString olTable = opField.Left(3);
	CString olField = opField.Mid(3,opField.GetLength()-3);

//	strcpy(pclExcelPath, "C:\\Data_Changes.exe");
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "DATA_CHANGES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_DATACHANGES), GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	CString olTimeMode = "U";
	if (bgGatPosLocal)
		olTimeMode = "L";

	if (!bgUTCCONVERTIONS)
		olTimeMode = "L";


	char slRunTxt[512] = ""; 
	sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", "0", "AFT", "", "LOCATIONS", olTimeMode, olListener, "C", CString(pcgUser));

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
	if (ilReturn != -1)
	{
		bgLoacationChangesStarted = true;
	}
}

bool GetDefAlloc(CString& opAlloc, CString& opData, CString& opFields, CStringArray& opReqData, CStringArray& opReqFields)
{
/*	for (int i=0; i<opReqFields.GetSize(); i++)
	{
		if (opReqFields.GetAt(i) == "BAA5")
			opReqData.Add("40");
		if (opReqFields.GetAt(i) == "BAA4")
			opReqData.Add("20");
		if (opReqFields.GetAt(i) == "GD1B")
			opReqData.Add("50");
		if (opReqFields.GetAt(i) == "GD2B")
			opReqData.Add("60");
		if (opReqFields.GetAt(i) == "GA1E")
			opReqData.Add("70");
		if (opReqFields.GetAt(i) == "GA2E")
			opReqData.Add("80");
	}
*/

//	int ilCount = olFlightUtilsData.omData.GetSize();
	CString olCmd("");
	if (opAlloc == "POS")
	{
		olCmd = "PFC";
		if (!blDatPosDura)
			return false;
	}
	else if (opAlloc == "GAT")
	{
		olCmd = "GFC";
		if (!blDatGatDura)
			return false;
	}
	else
		return false;

//	olCmd = "GFR";
	SeasonDlgCedaFlightData olFlightData;
	olFlightData.GetDefAllocForeCast(olCmd, opData, opFields, opReqData, opReqFields);



/*	if (olArray[0] == "A")
	{
		olDefAlloc = "30";
		if (olArray[2] == "LH")
			olDefAlloc = "40";
		if (olArray[4] == "720")
			olDefAlloc = "50";
	}
	if (olArray[0] == "D")
	{
		olDefAlloc = "60";
		if (olArray[2] == "TP")
			olDefAlloc = "70";
		if (olArray[4] == "721")
			olDefAlloc = "80";
	}
*/
	return true;

}


CString GetCnamExt(CString& opCnam1, CString& opCnam2)
{
	CString olAtr1;
	if (!opCnam1.IsEmpty())
	{
		ogBCD.GetField("CIC", "CNAM",opCnam1, "CATR", olAtr1 );
	}

	if (!opCnam2.IsEmpty())
	{
		CString olAtr2;
		ogBCD.GetField("CIC", "CNAM", opCnam2, "CATR", olAtr2 );
		return opCnam1 + olAtr1 + CString("->") + opCnam2 + olAtr2;
	}
	else
		return opCnam1 + olAtr1;

	return CString("");
}


bool GetDefAllocFC(CString& opAlloc, const CString& opRes, CMapStringToString& opForcastMap, CStringArray& opReqFields)
{
	CString olResNam = "";
	if (opAlloc == "PST")
	{
		olResNam = "PNAM";
	}
	else if (opAlloc == "GAT")
	{
		olResNam = "GNAM";
	}
	else
		return false;

	for (int j=0; j<opReqFields.GetSize(); j++)
	{
//		CString olT = opReqFields.GetAt(j);
	//------------------------------------------------------
		//ALG zur AL
		CString olAlc2;
		CString olAlc3;
		opForcastMap.Lookup("ALC2", olAlc2);
		opForcastMap.Lookup("ALC3", olAlc3);
		CString olAlcUrno = ogBCD.GetFieldExt("ALT","ALC2","ALC3",olAlc2,olAlc3,"URNO");

		//alle eintr�ge in der membertab
		CCSPtrArray<RecordSet> olAlcRecords;
		ogBCD.GetRecords("GRM", "VALU", olAlcUrno, &olAlcRecords);

		// alle Grp
		CMapStringToString olResMapALT;
		for (int i = 0; i < olAlcRecords.GetSize(); i++)
		{
			//verweis auf Grp
			CString olGurn = olAlcRecords[i].Values[ogBCD.GetFieldIndex("GRM", "GURN")];

			CCSPtrArray<RecordSet> olGrnRecords;
			//datensatz der Grp
			ogBCD.GetRecords("GRN", "URNO", olGurn, &olGrnRecords);
			if (olGrnRecords.GetSize() > 0)
			{
				CString olGrpn = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "GRPN")];
				CString olAppl = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "APPL")];
				//datensatz von rules
				if (olAppl == "POPS")
					olResMapALT.SetAt(olGurn, "1");
			}
			olGrnRecords.DeleteAll();
		}
	//------------------------------------------------------
		//ACTG zu ACT
		CString olAct3;
		CString olAct5;
		opForcastMap.Lookup("ACT3", olAct3);
		opForcastMap.Lookup("ACT5", olAct5);
		CString olActUrno = ogBCD.GetFieldExt("ACT","ACT3","ACT5",olAct3,olAct5,"URNO");

		//alle eintr�ge in der membertab
		CCSPtrArray<RecordSet> olActRecords;
		ogBCD.GetRecords("GRM", "VALU", olActUrno, &olActRecords);

		// alle Grp
		CMapStringToString olResMapACT;
		for (i = 0; i < olActRecords.GetSize(); i++)
		{
			//verweis auf Grp
			CString olGurn = olActRecords[i].Values[ogBCD.GetFieldIndex("GRM", "GURN")];

			CCSPtrArray<RecordSet> olGrnRecords;
			//datensatz der Grp
			ogBCD.GetRecords("GRN", "URNO", olGurn, &olGrnRecords);
			if (olGrnRecords.GetSize() > 0)
			{
				CString olGrpn = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "GRPN")];
				CString olAppl = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "APPL")];
				//datensatz von rules
				if (olAppl == "POPS")
					olResMapACT.SetAt(olGurn, "1");
			}
			olGrnRecords.DeleteAll();
		}
	//------------------------------------------------------
		//NATG zu NAT
		CString olNat;
		opForcastMap.Lookup("TTYP", olNat);
		CString olNatUrno = ogBCD.GetField("NAT","TTYP",olNat,"URNO");

		//alle eintr�ge in der membertab
		CCSPtrArray<RecordSet> olNatRecords;
		ogBCD.GetRecords("GRM", "VALU", olNatUrno, &olNatRecords);

		// alle Grp
		CMapStringToString olResMapNAT;
		for (i = 0; i < olNatRecords.GetSize(); i++)
		{
			//verweis auf Grp
			CString olGurn = olNatRecords[i].Values[ogBCD.GetFieldIndex("GRM", "GURN")];

			CCSPtrArray<RecordSet> olGrnRecords;
			//datensatz der Grp
			ogBCD.GetRecords("GRN", "URNO", olGurn, &olGrnRecords);
			if (olGrnRecords.GetSize() > 0)
			{
				CString olGrpn = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "GRPN")];
				CString olAppl = olGrnRecords[0].Values[ogBCD.GetFieldIndex("GRN", "APPL")];
				//datensatz von rules
				if (olAppl == "POPS")
					olResMapNAT.SetAt(olGurn, "1");
			}
			olGrnRecords.DeleteAll();
		}
	//------------------------------------------------------
		//alle res eintr�ge
		CString olResUrno = ogBCD.GetField(opAlloc, olResNam, opRes, "URNO");

		CCSPtrArray<RecordSet> olDatRecords;
		ogBCD.GetRecords("DAT", "ALID", olResUrno, &olDatRecords);
	//------------------------------------------------------
		// duration ermitteln
		int ilMaxGrpCount = 0;
		CString olAdid;
		opForcastMap.Lookup("ADID", olAdid);
		CString olDura ("");
		for (i = 0; i < olDatRecords.GetSize(); i++)
		{
			int ilGrpCount = 0;

			CString olAlgr = olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "ALGR")];
			CString olAcgr = olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "ACGR")];
			CString olNagr = olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "NAGR")];
			CString olFlty = olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "FLTY")];

			if (olAlgr == "0")
				olAlgr.Empty();
			if (olAcgr == "0")
				olAcgr.Empty();
			if (olNagr == "0")
				olNagr.Empty();

			if (olFlty == "1")
				olFlty = "1 ";

			if (olAdid == "A" && olFlty.Left(1) != "1")
				continue;

			if (olAdid == "D" && olFlty.Right(1) != "1")
				continue;

			CString olTmp;
			if (!olAlgr.IsEmpty())
			{
				if (olResMapALT.Lookup(olAlgr, olTmp))
					ilGrpCount++;
				else
					continue;
			}

			if (!olAcgr.IsEmpty())
			{
				if (olResMapACT.Lookup(olAcgr, olTmp))
					ilGrpCount++;
				else
					continue;
			}

			if (!olNagr.IsEmpty())
			{
				if (olResMapNAT.Lookup(olNagr, olTmp))
					ilGrpCount++;
				else
					continue;
			}

			bool blDefaultValue = false;
			if (olAlgr.IsEmpty() && olAcgr.IsEmpty() && olNagr.IsEmpty())
				blDefaultValue = true;

			if (ilGrpCount == 3)
			{
				olDura =  olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "DURA")];
				break;
			}
			else if (ilGrpCount > ilMaxGrpCount || (blDefaultValue && olDura.IsEmpty()) )
			{
				olDura =  olDatRecords[i].Values[ogBCD.GetFieldIndex("DAT", "DURA")];
				ilMaxGrpCount = ilGrpCount;
			}
		}

		if(olDura == "")
		{
			char bufferTmp[128];

			if (opReqFields.GetAt(j) == "BAA4" && opAlloc == "PST")
			{
				itoa(ogDefDurPsta.GetTotalMinutes(), bufferTmp, 10);
			}
			else if (opReqFields.GetAt(j) == "BAA5" && opAlloc == "PST")
			{
				itoa(ogDefDurPstd.GetTotalMinutes(), bufferTmp, 10);
			}
			else if (opReqFields.GetAt(j) == "BAD4")
			{
				itoa(ogDefDurGata.GetTotalMinutes(), bufferTmp, 10);
			}
			else if (opReqFields.GetAt(j) == "BAD5")
			{
				itoa(ogDefDurGatd.GetTotalMinutes(), bufferTmp, 10);
			}
			else if (opReqFields.GetAt(j) == "BAA4" && opAlloc == "GAT")
			{
				itoa(ogDefDurGata.GetTotalMinutes(), bufferTmp, 10);
			}
			else if (opReqFields.GetAt(j) == "BAA5" && opAlloc == "GAT")
			{
				itoa(ogDefDurGata.GetTotalMinutes(), bufferTmp, 10);
			}

			olDura = CString(bufferTmp);
			if(olDura == "")
				olDura = "30";
		}

//		CString olt = opReqFields.GetAt(j);
		opForcastMap.SetAt(opReqFields.GetAt(j),olDura);

		olDatRecords.DeleteAll();
		olAlcRecords.DeleteAll();
		olActRecords.DeleteAll();
		olNatRecords.DeleteAll();
		olResMapALT.RemoveAll();
		olResMapACT.RemoveAll();
		olResMapNAT.RemoveAll();
	}

	return true;
}

void CreateForcastMap(CMapStringToString& opForecastMap, CStringArray& opValues)
{
	ASSERT (opValues.GetSize() == 6);

	opForecastMap.SetAt("ADID",opValues.GetAt(0));
	opForecastMap.SetAt("ALC2",opValues.GetAt(1));
	opForecastMap.SetAt("ALC3",opValues.GetAt(2));
	opForecastMap.SetAt("ACT3",opValues.GetAt(3));
	opForecastMap.SetAt("ACT5",opValues.GetAt(4));
	opForecastMap.SetAt("TTYP",opValues.GetAt(5));
	opForecastMap.SetAt("BAA4","");
	opForecastMap.SetAt("BAA5","");
	opForecastMap.SetAt("BAD4","");
	opForecastMap.SetAt("BAD5","");
/*
	CString olTmp;
	opForecastMap.Lookup("ADID", olTmp);
	opForecastMap.Lookup("ALC2", olTmp);
	opForecastMap.Lookup("ALC3", olTmp);
	opForecastMap.Lookup("ACT3", olTmp);
	opForecastMap.Lookup("ACT5", olTmp);
	opForecastMap.Lookup("TTYP", olTmp);
*/
	return;

}


CString GetFtypLabel(CString opFtyp)
{
	if(opFtyp == "X")
		return CString("Canceled");

	if(opFtyp == "S")
		return CString("Planning");

	if(opFtyp == "O")
		return CString("Operation");

	if(opFtyp == "N")
		return CString("NOOP");

	return "";

}



bool ConvOctStToByteArr( CString pspSt, CString& pbpByteArr)
{
	pbpByteArr = "";
	bool isOk = true;
	int len = 0;
	//len = (strlen( pspSt )/sizeof(char));
	len = pspSt.GetLength();
	int res = len%3;
	len = len - res;
	if (len<0) len = 0;
	int byteCnt = 0;
	char olCh ;
	for (int i=0; i<len; i= i+6)
	{
		if(pspSt.GetLength() > i+5)
		{
			char tmpSt[3];
			tmpSt[0] = pspSt.GetAt(i+3);
			tmpSt[1] = pspSt.GetAt(i+4);
			tmpSt[2] = pspSt.GetAt(i+5);
			tmpSt[3] = '\0';
			BYTE num = 0;
			isOk = ConvOctStToByte( tmpSt, num );
			if (isOk)
			{
				olCh = (char)num;
				pbpByteArr = pbpByteArr + olCh;
				//pbpByteArr[byteCnt++] = num;
			}
			else
			{
				break;
			}
		}
	}
	if (!isOk) byteCnt = 0;
	return isOk;
}


bool ConvOctStToByte( char* st, BYTE& bpByte )
{//Convert 3 Oct Digits in String to Byte
	bpByte = 0;
	bool isOk = true;

	int lowNum,midNum,hiNum;
    if (strlen(st)!=3) isOk = false; //expecting 3 chars only

	if (isOk)
	{
		isOk = ConvOctCharToInt(st[0], hiNum);//convert higher 2 bits
		isOk = ConvOctCharToInt(st[1], midNum); //Convert middle 3 bits
		isOk = ConvOctCharToInt(st[2], lowNum); //Convert lower 3 bits
		if (isOk)
		{
			bpByte = (hiNum << 6 ) + (midNum<<3) + lowNum;
		}
	}
	return isOk;
}


bool ConvOctCharToInt( char cpCh , int& num)
{
	bool isOk = true;
	num = 0;
    switch( cpCh )
	{
		case '0' : num = 0;	break;
		case '1' : num = 1; break;
		case '2' : num = 2; break;
		case '3' : num = 3; break;
		case '4' : num = 4; break;
		case '5' : num = 5; break;
		case '6' : num = 6; break;
		case '7' : num = 7; break;
		default  : isOk = false;
	}
	return isOk;
}



bool DayStringShift( CUIntArray &opDayArray, int ipShift)
{

	if(ipShift == 0 || opDayArray.GetSize() != 7)
		return false;


	CUIntArray olDayArray;

	int ilCount = abs(ipShift);


	for(int j= 0; j < ilCount; j++)
	{
		if(ipShift > 0)
		{
			olDayArray.Add(opDayArray[6]);
			olDayArray.Add(opDayArray[0]);
			olDayArray.Add(opDayArray[1]);
			olDayArray.Add(opDayArray[2]);
			olDayArray.Add(opDayArray[3]);
			olDayArray.Add(opDayArray[4]);
			olDayArray.Add(opDayArray[5]);
		}
		else
		{
			olDayArray.Add(opDayArray[1]);
			olDayArray.Add(opDayArray[2]);
			olDayArray.Add(opDayArray[3]);
			olDayArray.Add(opDayArray[4]);
			olDayArray.Add(opDayArray[5]);
			olDayArray.Add(opDayArray[6]);
			olDayArray.Add(opDayArray[0]);
		}


		opDayArray[0] = olDayArray[0];
		opDayArray[1] = olDayArray[1];
		opDayArray[2] = olDayArray[2];
		opDayArray[3] = olDayArray[3];
		opDayArray[4] = olDayArray[4];
		opDayArray[5] = olDayArray[5];
		opDayArray[6] = olDayArray[6];

	int il0 = opDayArray[0];
	int il1 = opDayArray[1];
	int il2 = opDayArray[2];
	int il3 = opDayArray[3];
	int il4 = opDayArray[4];
	int il5 = opDayArray[5];
	int il6 = opDayArray[6];



		olDayArray.RemoveAll();
	}
	return true;
}



//AM:20100421:Get the config Info from Ceda.ini					
//Return true when there is an entry, otherwise return false					
//Return the value of config in "pcpConfigValue"					
bool GetConfigInfo(const char* pcpConfigName, CString& opConfigValue ) //CString pcpConfigValue)					
{					
	opConfigValue = "";				
	bool blFound = false;				
					
	//strcpy( pcpConfigValue, "" );				
	CString olKey;				
					
	olKey.Format( "%s", pcpConfigName );				
					
	if (ogConfigMap.Lookup( olKey, opConfigValue))				
	{				
		blFound = true;			
	}				
	else				
	{				
		char pclConfigPath[256];			
		char pclValue[512];			
					
		if (getenv("CEDA") == NULL)			
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 		
		else			
			strcpy(pclConfigPath, getenv("CEDA"));		
			GetPrivateProfileString(ogAppName, pcpConfigName, "DEFAULT",		
				pclValue, sizeof pclValue, pclConfigPath);	
					
		if(!strcmp(pclValue, "DEFAULT"))			
		{			
			//char pclErrMsg[256] = "";		
			//sprintf( pclErrMsg, "Unable to get the setting for <%s>. Please check in <%s>", pcpConfigName, pclConfigPath );		
			//MessageBox(pclErrMsg, GetString(ST_FEHLER), MB_ICONERROR);		
			//CFPMSApp::MyTopmostMessageBox(NULL, pclErrMsg, GetString(ST_FEHLER), MB_ICONERROR);		
		}			
		else			
		{			
			opConfigValue.Format( "%s", pclValue );		
			blFound = true;		
					
			CCS_TRY		
			ogConfigMap.SetAt( olKey, pclValue );		
			CCS_CATCH_ALL		
		}			
	}				
					
	return blFound;				
}					



//Launch Tool			
//Pass In			
//  1. opCmd - Command recorganized by the tool			
//  2. opArrUrno - Arrival Flight Urno OR ""			
//  3. opDepUrno - Departure Flight Urno OR ""			
//  4. opPermit  - Permission for this activity			
//         might be based on BDPS-SEC			
//         "1"/"0"/"-"/""			
//  5. opConfigName - Config id defined in CEDA.INI [FIPS] to get the Path of Tool to launch
//  6. opLocalTime - Local/UTC			
DWORD LaunchTool(			
	CString opCmd, 		
	CString opArrUrno, CString opDepUrno, 		
	CString opPermit,		
	CString opConfigName,
	bool opLocalTime)		
{			
	DWORD dlProcId = NULL;		
	if (opArrUrno.IsEmpty() && opDepUrno.IsEmpty()) return dlProcId;		
			
	char pclPath[256] = "";		
	CString olPath;		
			
	if (GetConfigInfo( opConfigName, olPath ))		
	{		
		char slRunTxt[512] = ""; 	
		//CString olUser = ogBasicData.omUserID;	

			
		CString olTime = "U";	
		if (opLocalTime)	
			olTime = "L";
			
		CPoint point;	
		::GetCursorPos(&point);	
			
		sprintf( slRunTxt,"%s,%s,%s|%s,%s,%s|%d,%d|%s", 	
			opCmd, opArrUrno, opDepUrno, pcgUser, opPermit, olTime, point.x, point.y, pcgPasswd );
		//MessageBox( slRunTxt );	
			
		/*	
		char *args[4];	
		args[0] = "child";	
		args[1] = slRunTxt;	
		args[2] = NULL;	
		args[3] = NULL;	
		int ilReturn = _spawnv(_P_NOWAIT, olPath, args);	
		*/	
		dlProcId = startProcess( olPath, CString(slRunTxt));	
	}		
	return dlProcId;		
}			



DWORD LaunchTool(			
	CString opCmd, 		
	CString opArrUrno, CString opDepUrno, 		
	CString opPermit,		
	CString opConfigName,		
	CString processWindowName,		
	DWORD dpPrevId,
	bool opLocalTime)		
{			
	DWORD returnProcId = NULL;		
	bool alive = false;		
	if ( dpPrevId!=NULL && IsProcessAlive( processWindowName, dpPrevId)) alive = true;		
	returnProcId = LaunchTool( opCmd, opArrUrno, opDepUrno,		
		opPermit,	
		opConfigName, opLocalTime);	
			
	if (alive) returnProcId = dpPrevId;		
			
	return returnProcId;		
}			
			
			
			
DWORD startProcess(CString opExePath, CString opArgs )			
{			
  BOOL bWorked;			
  STARTUPINFO suInfo;			
  PROCESS_INFORMATION procInfo;			
			
  char vip[512] = "";			
  sprintf( vip, "%s %s", 			
	  opExePath, opArgs );		
			
			
  memset (&suInfo, 0, sizeof(suInfo));			
  suInfo.cb = sizeof(suInfo);			
			
  bWorked = ::CreateProcess(opExePath,			
		     vip,      // can also be NULL			
			
		     NULL,			
		     NULL,			
		     TRUE,			
		     NORMAL_PRIORITY_CLASS,			
		     NULL,			
		     NULL,			
		     &suInfo,			
		     &procInfo);			
			
/*			
procInfo has these members			
	HANDLE hProcess;   // process handle			
	HANDLE hThread;    // primary thread handle			
	DWORD dwProcessId; // process PID			
	DWORD dwThreadId;  // thread ID			
*/			
			
  return procInfo.dwProcessId;			
}			
			
			
bool IsProcessAlive( CString opProcessWindowName, DWORD dpProcId )			
{			
	HWND windowHandle = FindWindow(NULL, opProcessWindowName);		
	DWORD processID = 0;		
	GetWindowThreadProcessId(windowHandle, &processID);		
	if (dpProcId==(processID)) return true;		
	else return false;		
}			
			
			
void killProcess(DWORD processPid)			
{			
  HANDLE ps = OpenProcess( SYNCHRONIZE|PROCESS_TERMINATE, 			
		                                FALSE, processPid);			
  // processPid = procInfo.dwProcessId;			
			
			
  // This function enumerates all widows,			
			
  // using the EnumWindowsProc callback			
			
  // function, passing the PID of the process			
			
  // you started earlier.			
			
  EnumWindows(EnumWindowsProc, processPid);			
			
  CloseHandle(ps) ;			
}			
			
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)			
{			
  DWORD wndPid;			
  CString Title;			
  // lParam = procInfo.dwProcessId;			
			
			
  // This gets the windows handle and pid of enumerated window.			
			
  GetWindowThreadProcessId(hwnd, &wndPid);			
			
  // This gets the windows title text			
			
  // from the window, using the window handle			
			
  CWnd::FromHandle( hwnd )->GetWindowText(Title);			
			
  //  this makes sure that the PID matches that PID we started, and window			
			
  // text exists, before we kill it . I don't think this is really needed, 			
			
  // I included it because some apps have more than one window.			
			
  if ( wndPid == (DWORD)lParam && Title.GetLength() != 0)			
  {			
	//  Please kindly close this process			
			
	::PostMessage(hwnd, WM_CLOSE, 0, 0);			
	return false;			
  }			
  else			
  {			
	// Keep enumerating			
			
	return true;			
  }			
}			



