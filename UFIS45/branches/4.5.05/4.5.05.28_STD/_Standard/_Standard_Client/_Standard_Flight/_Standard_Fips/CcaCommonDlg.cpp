// CheckinModifyDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include "fpms.h"			// 050324 MVy: need it for CanHandleCheckinCounterRestrictionsForHandlingAgents
#include <CcaCommonDlg.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <BasicData.h>
#include <CcaCedaFlightData.h>
#include <AwBasicDataDlg.h>
#include<AskBox.h>

#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__; 
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonDlg 


CCcaCommonDlg::CCcaCommonDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCcaCommonDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCcaCommonDlg)
	m_Beginn_t = _T("");
	m_Beginn_d = _T("");
	m_ActStartTime = _T("");
	m_ActStartDate = _T("");
	m_Rem = _T("");
	m_Ende_d = _T("");
	m_Ende_t = _T("");
	m_Logo = _T("");
	m_ActEndTime = _T("");
	m_ActEndDate = _T("");
	m_Fluggs = _T("");
	m_Schalter = _T("");
	m_Terminal = _T("");
	m_Disp = _T("");
	m_PreCheckIn = FALSE;
	//}}AFX_DATA_INIT

	m_ulUrnoValidCounter = 0 ;

	CDialog::Create(CCcaCommonDlg::IDD, pParent);
}

CCcaCommonDlg::~CCcaCommonDlg()
{

}


void CCcaCommonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCcaCommonDlg)
	DDX_Control(pDX, IDC_LST_ERROR, m_LstErrors);
	DDX_Control(pDX, IDC_DISP, m_CE_Disp);
	DDX_Control(pDX, IDC_LOGO, m_CE_Logo);
	DDX_Control(pDX, IDC_TERMINAL, m_CE_Terminal);
	DDX_Control(pDX, IDC_SCHALTER, m_CE_Schalter);
	DDX_Control(pDX, IDC_FLUGGS, m_CE_Fluggs);
	DDX_Control(pDX, IDC_BEGINN_D, m_CE_Beginn_d);
	DDX_Control(pDX, IDC_BEGINN_T, m_CE_Beginn_t);
	DDX_Control(pDX, IDC_ENDE_T, m_CE_Ende_t);
	DDX_Control(pDX, IDC_ENDE_D, m_CE_Ende_d);
	DDX_Control(pDX, IDC_ACT_START_TIME, m_CE_ActStartTime);
	DDX_Control(pDX, IDC_ACT_START_DATE, m_CE_ActStartDate);
	DDX_Control(pDX, IDC_ACT_END_TIME, m_CE_ActEndTime);
	DDX_Control(pDX, IDC_ACT_END_DATE, m_CE_ActEndDate);
	DDX_Control(pDX, IDC_BEMERKUNG, m_CE_Rem);
	DDX_Control(pDX, IDC_DCICLOGO1, m_CB_DCicLogo1);
	DDX_Text(pDX, IDC_BEGINN_T, m_Beginn_t);
	DDX_Text(pDX, IDC_BEGINN_D, m_Beginn_d);
	DDX_Text(pDX, IDC_ENDE_D, m_Ende_d);
	DDX_Text(pDX, IDC_ENDE_T, m_Ende_t);
	DDX_Text(pDX, IDC_ACT_START_TIME, m_ActStartTime);
	DDX_Text(pDX, IDC_ACT_START_DATE, m_ActStartDate);
	DDX_Text(pDX, IDC_ACT_END_TIME, m_ActEndTime);
	DDX_Text(pDX, IDC_ACT_END_DATE, m_ActEndDate);
	DDX_Text(pDX, IDC_BEMERKUNG, m_Rem);
	DDX_Text(pDX, IDC_FLUGGS, m_Fluggs);
	DDX_Text(pDX, IDC_SCHALTER, m_Schalter);
	DDX_Text(pDX, IDC_TERMINAL, m_Terminal);
	DDX_Text(pDX, IDC_DISP, m_Disp);
	DDX_Text(pDX, IDC_LOGO, m_Logo);
	DDX_Control(pDX, IDC_FIDS_REM_LIST, m_CB_FidsRemList);
	DDX_Control(pDX, IDC_PRECHECKIN, m_CB_PreCheckIn);
	DDX_Check(pDX, IDC_PRECHECKIN, m_PreCheckIn);
	DDX_Control(pDX, IDC_CATR, m_CE_Catr);
    DDX_Control(pDX, IDC_AIRL_GRP, m_CB_AirlGrpList);

	//}}AFX_DATA_MAP

	m_ulUrnoValidCounter = 0 ;
}


BEGIN_MESSAGE_MAP(CCcaCommonDlg, CDialog)
	//{{AFX_MSG_MAP(CCcaCommonDlg)
	ON_BN_CLICKED(IDC_FIDS_REM_LIST, OnFidsRemList)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_AIRL_GRP,OnAirlGrpList)
	ON_BN_CLICKED(IDC_DCICLOGO1, OnDcicLogo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCcaCommonDlg 

BOOL CCcaCommonDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_CE_Schalter.SetTypeToString("X(5)",5,1);
	m_CE_Schalter.SetBKColor(YELLOW);

	m_CE_Terminal.SetBKColor(SILVER);

	m_CE_Logo.SetWindowText("");

    if(!bgCheckInAirline)
	{
		m_CB_AirlGrpList.ShowWindow(SW_HIDE);
	}
	/*

	if(omModi == "Datensatz �ndern")
	{
		m_CE_Fluggs.SetTypeToString("X(3)",3,2);//x# 
		m_CE_Fluggs.SetReadOnly();
		char clTmp[20];
		CString olAlc;
		sprintf(clTmp,"%d",omCCA.Flnu);
		bool blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc);
		if(blRet == false)
			blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
		m_CE_Fluggs.SetInitText(olAlc);
	}
	else
	*/
	{
		m_CE_Fluggs.SetTypeToString("X(20)",20,0);//x# 
		//m_CE_Fluggs.SetBKColor(YELLOW);
	}


	if(bgFIDSLogo)
	{
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CE_Logo);		
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CB_DCicLogo1);		
		m_CE_Logo.EnableWindow(FALSE);

	}
	else
	{
		SetWndStatAll('-', m_CE_Logo);		
		SetWndStatAll('-', m_CB_DCicLogo1);	
		GetDlgItem(IDC_LOGO_TEXT)->ShowWindow(SW_HIDE);
	}


	m_CE_Beginn_d.SetBKColor(YELLOW);	
	m_CE_Beginn_d.SetTypeToDate(true);
	m_CE_Beginn_t.SetBKColor(YELLOW);
	m_CE_Beginn_t.SetTypeToTime(true);
	m_CE_Ende_d.SetBKColor(YELLOW);			
	m_CE_Ende_d.SetTypeToDate(true);
	m_CE_Ende_t.SetBKColor(YELLOW);
	m_CE_Ende_t.SetTypeToTime(true);
	m_CE_ActStartTime.SetTypeToTime(true);
	m_CE_ActStartDate.SetTypeToDate(true);
	m_CE_ActEndTime.SetTypeToTime(true);
	m_CE_ActEndDate.SetTypeToDate(true);
	
	m_CE_Rem.SetTypeToString("X(60)",60,0);
	m_CE_Disp.SetTypeToString("X(60)",60,0);

	m_PreCheckIn = FALSE;
	m_CB_PreCheckIn.SetCheck(m_PreCheckIn);
/*
	// DXB (Dubai) need other field descriptions at the moment (28.09.2000)
	if (strcmp(pcgHome, "DXB") == 0)
	{
		GetDlgItem(IDC_BEM_TEXT)->SetWindowText(GetString(IDS_STRING2004));
		GetDlgItem(IDC_DISP_TEXT)->SetWindowText(GetString(IDS_STRING2005));
		// and a special button for FidsRemark (02.11.2000)
		GetDlgItem(IDC_FIDS_REM_LIST)->ShowWindow(SW_SHOW);
	}
*/
		GetDlgItem(IDC_FIDS_REM_LIST)->ShowWindow(SW_SHOW);

	if (!bgCnamAtr)
		GetDlgItem(IDC_CATR)->ShowWindow(SW_HIDE);


	m_CE_Terminal.SetFocus();

	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDOK);
	if (plWnd) 
	{
		#ifdef _FIPSVIEWER
		{
			plWnd->EnableWindow(false);
		}
		#else
		{
			plWnd->EnableWindow(true);
		}
		#endif
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

// 050407 MVy: read data from edit windows, pressing ok within an editbox will not cause focus lost before OnOk
void CCcaCommonDlg::UpdateAllFields()
{
	SetTimeSchedule();
	SetTimeActual();
};	// UpdateAllFields

void CCcaCommonDlg::OnOK() 
{
	UpdateAllFields();
	if( !AllowOk() )
	{
		return ;
	};

	//	if (UpdateData(TRUE))
//		UpdateData(FALSE);

	//provide return key if focus is in fluggs (override disp)
	CWnd* hWnd = GetFocus();
//	HWND wnd = hWnd->m_hWnd;
//	HWND wnd1 = m_CE_Fluggs.m_hWnd;
/*
	if (hWnd->m_hWnd == m_CE_Fluggs.m_hWnd)
	{
		CString olAlc;
		m_CE_Fluggs.GetWindowText(olAlc);
		m_CE_Disp.SetInitText(olAlc);
	}
*/

	CString olErrorText;
	CString olKeineDaten = GetString(ST_NODATA);
	CString olNichtFormat = " cannot be handled" ;	//GetString(ST_BADFORMAT);		// 050407 MVy: to build a better sentense
	bool ilStatus = true;
	if(m_CE_Schalter.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CE_Schalter.GetWindowTextLength() == 0)
		{
			olErrorText += GetString(IDS_STRING1516) +  olKeineDaten;
		}
		else	
		{
			olErrorText += GetString(IDS_STRING1516) + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Fluggs.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Fluggs.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1517) +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1517) + olNichtFormat;
			}
		}
	}	
	
	// check scheduled times!!!
	if (ilStatus == true)
	{
		if(m_CE_Beginn_d.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Beginn_d.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Beginn_t.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Beginn_t.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Ende_d.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Ende_d.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Ende_t.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Ende_t.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") + olNichtFormat;
			}
		}
	}

	
	// check actual times!!!
	if (ilStatus == true)
	{
		if(m_CE_ActStartDate.GetStatus() == false && m_CE_ActStartDate.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2092) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActStartTime.GetStatus() == false && m_CE_ActStartTime.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2092) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActEndDate.GetStatus() == false && m_CE_ActEndDate.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2093) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActEndTime.GetStatus() == false && m_CE_ActEndTime.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2093) + CString(" ") + olNichtFormat;
		}
	}



	if (ilStatus == true)
	{
		char clTmp[20];
		CString olTmp;
		bool blRet;
		m_CE_Beginn_d.GetWindowText(m_Beginn_d);
		m_CE_Beginn_t.GetWindowText(m_Beginn_t);
		m_CE_Ende_d.GetWindowText(m_Ende_d);
		m_CE_Ende_t.GetWindowText(m_Ende_t);
		m_CE_ActStartDate.GetWindowText(m_ActStartDate);
		m_CE_ActStartTime.GetWindowText(m_ActStartTime);
		m_CE_ActEndDate.GetWindowText(m_ActEndDate);
		m_CE_ActEndTime.GetWindowText(m_ActEndTime);
		m_CE_Schalter.GetWindowText(omCCA.Ckic,7);

		// Schaltername
		char clCkic[8];
		CString olCkit;
		strcpy(clCkic, omCCA.Ckic);
		blRet = ogBCD.GetField("CIC", "CNAM", clCkic, "TERM", olCkit); // �ber Checkin-Schalter Terminal holen
		if(blRet == true)
		{
			sprintf(clTmp, "%s", olCkit);
			strcpy(omCCA.Ckit, clTmp);
		}
		else	
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING1367);
		}

		CString olAlc;

		// Fluggesellschaft
		m_CE_Fluggs.GetWindowText(olAlc);

		omCCA.Flnu = 0;
		strcpy(omCCA.Flno, "");
		omCCA.Gurn=0;
		bool blAlGroup=true;
		bool blAlc=true;
		CString olTmpAl;
		CString olTmpAlGrp;
		
		if(!olAlc.IsEmpty())
		{
			CString olUpperAlc=olAlc;
			olUpperAlc.MakeUpper();
			blAlc=ogBCD.GetField("ALT","ALC2",olUpperAlc,"URNO",olTmpAl);
			if(blAlc==false)
				blAlc=ogBCD.GetField("ALT","ALC3",olUpperAlc,"URNO",olTmpAl);
			    //airlinegroup
			blAlGroup=ogBCD.GetField("GRN_ALT","GRPN",olAlc,"URNO",olTmpAlGrp);
			olTmpAlGrp=ogBCD.GetField("GRN_ALT","GRPN",olAlc,"URNO");
			if(bgCheckInAirline && blAlc && blAlGroup) 
			{ 
				CString olMessage;
				olMessage.Format(GetString(IDS_AL_OR_ALGRP),olAlc);
				AskBox olDlg(this,GetString(ST_FRAGE),olMessage,GetString(IDS_STRING2772),GetString(IDS_STRING2773));
				blAlc=false;
				blAlGroup=true;
				if(olDlg.DoModal()==1)
				{
					blAlc=true;
					blAlGroup=false;
				}
			}
			 
			if(!blAlc && !blAlGroup)
				blRet= false;




			if(!blRet)
			{
				ilStatus = false;
				olErrorText += GetString(IDS_STRING1368);
			}
			else
			{    
				if(blAlc)
				{
				  omCCA.Flnu = atol(olTmpAl);
				  strcpy (omCCA.Flno,olAlc);
				}
				else if(blAlGroup)
					omCCA.Gurn=atol(olTmpAlGrp);
					else
				{
					omCCA.Flnu=0;
					omCCA.Gurn=0;
				}



			}
		}


		if (ilStatus == true)
		{

			if(m_CB_PreCheckIn.GetCheck())
				strcpy(omCCA.Ctyp,"P");
			else
				strcpy(omCCA.Ctyp,"C");

			m_CE_Rem.GetWindowText(omCCA.Rema,61);
			m_CE_Disp.GetWindowText(omCCA.Disp,61);

			if (!strlen(omCCA.Rema))
				strcpy(omCCA.Rema, " ");

			bool blCcaOpen = omCCA.IsOpen();

			omCCA.Ckba = DateHourStringToDate(m_ActStartDate, m_ActStartTime);
			omCCA.Ckea = DateHourStringToDate(m_ActEndDate, m_ActEndTime);
			omCCA.Ckbs = DateHourStringToDate(m_Beginn_d, m_Beginn_t);
			omCCA.Ckes = DateHourStringToDate(m_Ende_d, m_Ende_t);




			if(omCCA.Ckbs < omCCA.Ckes && 
				(omCCA.Ckba < omCCA.Ckea || omCCA.Ckba == TIMENULL || omCCA.Ckea == TIMENULL))
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				if(bgGatPosLocal) omCcaData.StructLocalToUtc(&omCCA, "");
				
				if(omCCA.Urno <= 0)
				{
					omCCA.Urno = ogBasicData.GetNextUrno("CCA");
					CCADATA *prlNewCca = new CCADATA;
					*prlNewCca = omCCA;
					omCcaData.Insert(prlNewCca);
					if (prlNewCca->IsOpen())
						prlNewCca->UpdateFIDSMonitor();
				}
				else
				{
					omCCA.IsChanged = DATA_CHANGED;
					omCcaData.Save(&omCCA);
					if (omCCA.IsOpen() != blCcaOpen)
						omCCA.UpdateFIDSMonitor();
				}
				if(bgFIDSLogo)
				{

					CString olLogo;
					m_CE_Logo.GetWindowText(olLogo);

					CCSPtrArray<CCADATA> ropCca;
					CStringArray omCicLogoArray;
					
					ropCca.Add(&omCCA);
					omCicLogoArray.Add(olLogo);

					omFlzData.SaveCicLogo(ropCca, omCicLogoArray);

				}

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				ShowWindow(SW_HIDE);
				//CDialog::OnOK();
			}
			else
			{
				Beep(440,70);
				MessageBox(GetString(IDS_STRING1518),GetString(ST_FEHLER),MB_ICONEXCLAMATION);
				m_CE_Schalter.SetFocus();
				m_CE_Schalter.SetSel(0,-1);
			}
		}

	}

	// 050406 MVy: check removed beacuse error info messagebox irritates user
	/*
	if( g_ulHandlingAgent
		&& m_ulUrnoValidCounter )		// this check made sense only if counter is set
	{
		if( !CheckPeriod( m_rngActual, true ) )
		{
			ilStatus = false ;
			olErrorText += "actual not in a valid period" ;
		};

		if( !CheckPeriod( m_rngSchedule ) )
		{
			ilStatus = false ;
			olErrorText += "schedule not in a valid period" ;
		};
	};
	*/

	if (ilStatus == false)
	{
		Beep(440,70);
		MessageBox(olErrorText,GetString(ST_FEHLER),MB_ICONEXCLAMATION);
		m_CE_Schalter.SetFocus();
		m_CE_Schalter.SetSel(0,-1);

		//CDialog::OnCancel();
	}

	m_LstErrors.ResetContent();
	m_CE_Schalter.SetFocus();
}






void CCcaCommonDlg::OnClose() 
{
	m_LstErrors.ResetContent();		// 050324 MVy: otherwise error old messages would appear near visit
	m_ulUrnoValidCounter = 0 ;
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}
 


void CCcaCommonDlg::NewData(CCADATA *popCCA)
{
	omCCA = *popCCA;
	m_PreCheckIn = FALSE;

	if(bgGatPosLocal) omCcaData.StructUtcToLocal(&omCCA, "");

	if(omCCA.Urno <= 0)
	{
		omFlzData.Clear();
	}


	if(omCCA.Urno > 0)
	{
		SetWindowText( GetString(IDS_STRING1519));

		if (strcmp(omCCA.Ctyp,"P") == 0)
			m_PreCheckIn = TRUE;

		m_CB_PreCheckIn.EnableWindow(FALSE);
	}
	else
	{
		SetWindowText(GetString(IDS_STRING1520));
		m_CB_PreCheckIn.EnableWindow(TRUE);
	}
	m_CE_Logo.SetWindowText("");

	///////// GateCKI Logo
	if(omCCA.Urno > 0 &&  bgFIDSLogo)
	{

		CString olWhere;
		char buffer[12];
		ltoa(omCCA.Urno , buffer, 10);
		olWhere += CString(buffer);
		omFlzData.Read(olWhere);

		m_CE_Logo.SetWindowText(omFlzData.GetCicLogoName(omCCA.Urno));
	}
	/////////////////////////



	m_CB_PreCheckIn.SetCheck(m_PreCheckIn); 
	m_CE_Terminal.SetInitText(omCCA.Ckit);
	m_CE_Schalter.SetInitText(omCCA.Ckic);
	m_CE_Fluggs.SetInitText("");


	if (bgCnamAtr)
	{
		CString olAtr1;
		ogBCD.GetField("CIC", "CNAM",omCCA.Ckic, "CATR", olAtr1 );
		m_CE_Catr.SetInitText(olAtr1);

	}

	char clTmp[20];
	CString olAlc;
	olAlc = CString(omCCA.Flno);
	if(olAlc.IsEmpty())
	{
		sprintf(clTmp,"%d",omCCA.Flnu);
		if(!ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc))
			ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
		m_CE_Fluggs.SetInitText(olAlc);
	}
	else
		m_CE_Fluggs.SetInitText(olAlc);

	if(omCCA.Flnu>0)
	{
		sprintf(clTmp,"%d",omCCA.Flnu);
		if(!ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc))
			ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
		m_CE_Fluggs.SetInitText(olAlc);
	}
	else if(omCCA.Gurn>0)
		sprintf(clTmp,"%d",omCCA.Gurn);
	   if(ogBCD.GetField("GRN_ALT","URNO",clTmp,"GRPN",olAlc))
		m_CE_Fluggs.SetInitText(olAlc);




	CTime olTime;
	olTime = omCCA.Ckbs;
	m_CE_Beginn_d.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_Beginn_t.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckes;
	m_CE_Ende_d.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_Ende_t.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckba;
	m_CE_ActStartDate.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_ActStartTime.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckea;
	m_CE_ActEndDate.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_ActEndTime.SetInitText(olTime.Format("%H:%M"));


	m_CE_Rem.SetInitText(omCCA.Rema);
	m_CE_Disp.SetInitText(omCCA.Disp);


	SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	ShowWindow(SW_SHOW);

	m_CE_Terminal.SetFocus();

	OnShowWindow(false, 0);
}



void CCcaCommonDlg::OnFidsRemList() 
{
	CString olText;

	m_CE_Disp.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Disp.SetWindowText(polDlg->GetField("CODE"));
	}
	delete polDlg;
}

void CCcaCommonDlg::OnAirlGrpList() 
{
	CString olText;
	m_CE_Fluggs.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GRN_ALT","GRPN", "GRPN+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Fluggs.SetWindowText(polDlg->GetField("GRPN"));

	}
	delete polDlg;
}


LONG CCcaCommonDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
// begin : reject if opening times
	CString olTmp;

	if((UINT)m_CE_Schalter.imID == wParam)
	{
		m_CE_Schalter.GetWindowText(olTmp);

		CString olGate = omCCA.Ckic;

		if (omCCA.Ckba != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_Schalter.SetInitText(olGate);
				MessageBox( GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
		return 0L;
	}
/*
	if((UINT)m_CE_Fluggs.imID == wParam)
	{
		m_CE_Fluggs.GetWindowText(olTmp);
		m_CE_Disp.SetInitText(olTmp);
		return 0L;
	}
*/
	return 0L;
}

void CCcaCommonDlg::AddErrorString( const char *pcError )
{
	static CString sLast = "" ;
	if( pcError != sLast )
		m_LstErrors.InsertString( 0, pcError );
	sLast = pcError ;
};	// AddErrorString

void CCcaCommonDlg::AddErrorString( CString &sError )
{
	AddErrorString( (char *)(LPCTSTR )sError );
	//m_LstErrors.InsertString( 0, sError );
	//m_LstErrors.AddString( sError );
	//int count = m_LstErrors.GetCount();
	//if( count == LB_ERR ) return ;		// fast get out of this function
	//m_LstErrors..SetTopIndex( count );		// show last item
};	// AddErrorString

// 050324 MVy: IDC_FLUGGS lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Fluggs( CCSEDITNOTIFY* pCcsEditNotify )
{
	CString olAlc;
	CString olAlc3;

	m_CE_Fluggs.GetWindowText(olAlc);

	CString olTmp;
	if(!olAlc.IsEmpty())
	{
		// 050324 MVy: if a handling agent is choosen in setup then check if the entered airline code is valid (=in airline code list)
		if( __CanHandleCheckinCounterRestrictionsForHandlingAgents() )		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
		{
			if( g_ulHandlingAgent )
			{
				if( !IsStringIn( g_strarrHandlingAgentAirlinesCodes, olAlc ) )
				{
					pCcsEditNotify->Status = false ;
					pCcsEditNotify->UserStatus = true ;		// textcolor will change red
					CString sError ;
					sError.Format( "airlinecode %s not valid for agent", olAlc );
					AddErrorString( sError );
				};	// entered airlinecode is not valid
			};	// handling agent choosen
		};	// cannot deal with it by configuration

/*
		olAlc3 = olAlc;
		bool blRet = ogBCD.GetField("ALT", "ALC3", olAlc, "URNO", olTmp);
		if(blRet == false)
			blRet = ogBCD.GetField("ALT", "ALC2", olAlc, "ALC3", olAlc3);
*/
//			m_CE_Disp.GetWindowText(olTmp);

//			CString olActualDispAlc3 = olTmp.Left(3);
//			CString olActualDispAlc = olTmp.Left(olAlc.GetLength());

//			if (!olAlc3.IsEmpty() && olActualDispAlc3 != olAlc3)
//			if (olActualDispAlc != olAlc)

//				m_CE_Disp.SetInitText(olAlc);

	};	// nothing entered
	return 0 ;
};	// OnEditKillfocus_CE_Fluggs

// 050324 MVy: IDC_SCHALTER lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Schalter( CCSEDITNOTIFY* pCcsEditNotify )
{
	CString olCnam;
	m_CE_Schalter.GetWindowText(olCnam);
	CString olAtr1;
	ogBCD.GetField("CIC", "CNAM",olCnam, "CATR", olAtr1 );
	m_CE_Catr.SetInitText(olAtr1);

	// additional check
	//	handling agent choosen in setup and entered checkin counter has no possible occupation period this counter cannot be used
	if(!olCnam.IsEmpty())
	{
		// 050324 MVy: if a handling agent is choosen in setup then check if the entered airline code is valid (=in airline code list)
		if( __CanHandleCheckinCounterRestrictionsForHandlingAgents() )		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
		{
			if( g_ulHandlingAgent )
			{
				unsigned long urnoCounter = GetUrnoByName( "CIC", olCnam );
				long pos = 0 ;
				if( AgentPeriods.FindFirstByCounter( pos, urnoCounter ) )
				{
					m_ulUrnoValidCounter = urnoCounter ;		// store to make later checks easier
				}
				else		// no period -> no chance at all
				{
					pCcsEditNotify->Status = false ;
					pCcsEditNotify->UserStatus = true ;		// textcolor will change red
					CString sError ;
					sError.Format( "no occupation period in counter %s", olCnam );
					AddErrorString( sError );
				};	// entered airlinecode is not valid
			};	// handling agent choosen
		};	// cannot deal with it by configuration
	};	// nothing entered
	return 0 ;
};	// OnEditKillfocus_CE_Schalter

/*
// 050329 MVy: check date and time if they are within a valid perid, return true if ok
bool CCcaCommonDlg::Check_Date( CCSEDITNOTIFY* pCcsEditNotify, CCSEdit& WndDate, CCSEdit& WndTime, char* pcInfo )
{
	if( g_ulHandlingAgent
		&& m_ulUrnoValidCounter )		// this check made sense only if counter is set
	{
		CString sDate ;
		WndDate.GetWindowText( sDate );
		if( !sDate.IsEmpty() )
		{
			CString sTime ;
			WndTime.GetWindowText( sTime );
			CTime date = ToTime( sDate + " " + sTime );
			// if the specified date is not within a occupation period
			if( !AgentPeriods.IsInRange( date, m_ulUrnoValidCounter, g_ulHandlingAgent ) )
			{
				pCcsEditNotify->Status = false ;
				pCcsEditNotify->UserStatus = true ;		// textcolor will change red
				// it would be nice to set time text color to red even if date text is notified
				CString sError ;
				CString sErrorTime ;
				if( !sTime.IsEmpty() ) sErrorTime = " and time" ;
				sError.Format( "%s date%s out of any occupation period", pcInfo, sErrorTime );
				AddErrorString( sError );
				return false ;
			};	// not in a valid range
		}
		else
		{
			CString sError ;
			sError.Format( "%s time without a date", pcInfo );
			AddErrorString( sError );
		};
	};
	return true ;
};	// Check_Date

// 050329 MVy: check date and time, return true if ok
bool CCcaCommonDlg::Check_Beginn( CCSEDITNOTIFY* pCcsEditNotify, CCSEdit& WndDate, CCSEdit& WndTime )
{
	if( !Check_Date( pCcsEditNotify, WndDate, WndTime, "schedule start" ) ) return false ;
	// check end
	return true ;
};	// Check_Beginn
/**/

// 050407 MVy: verify values of fields, return true only if ALL fields are valid
bool CCcaCommonDlg::CheckAllFieldsValid()
{
	if( !CheckPeriod( m_rngSchedule ) ) return false ;
	if( !CheckPeriod( m_rngActual, true ) ) return false ;
	return true ;
};	// CheckAllFieldsValid

// 050407 MVy: helper to enable or disable the ok button, 
//	disable on any error, enable only after checking all fields
bool CCcaCommonDlg::AllowOk( bool error )
{
	bool bAllow = false ;
	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd )
	{
		error ? bAllow = false : bAllow = CheckAllFieldsValid();
		pWnd->EnableWindow( bAllow );		// if one field (current by caller) is bad than a complete check must not be operformed
	};
	return bAllow ;
};	// AllowOk

bool CCcaCommonDlg::CheckPeriod( CTimeRange& range, bool canbeempty )
{
	if( g_ulHandlingAgent
		&& m_ulUrnoValidCounter )		// this check made sense only if counter is set
	{
		if( canbeempty )
		{
			if( m_rngActual.StartTime == -1 
				&& m_rngActual.EndTime == -1 )
				return true ;
		};

		return AgentPeriods.IsInRange( range, m_ulUrnoValidCounter, g_ulHandlingAgent );
	};	// cannot deal with it by configuration
	return true ;
};	// CheckPeriod

CTime CCcaCommonDlg::GetDateTime( CCSEdit& WndDate, CCSEdit& WndTime )
{
	CString sDate ;
	WndDate.GetWindowText( sDate );
	CString sTime ;
	WndTime.GetWindowText( sTime );
	return ToTime( sDate + " " + sTime );
};	// GetDateTime

void CCcaCommonDlg::SetTimeSchedule()
{
	m_rngSchedule.StartTime = GetDateTime( m_CE_Beginn_d, m_CE_Beginn_t );
	m_rngSchedule.EndTime = GetDateTime( m_CE_Ende_d, m_CE_Ende_t );
};	// SetTimeSchedule

void CCcaCommonDlg::SetTimeActual()
{
	m_rngActual.StartTime = GetDateTime( m_CE_ActStartDate, m_CE_ActStartTime );
	m_rngActual.EndTime = GetDateTime( m_CE_ActEndDate, m_CE_ActEndTime );
};	// SetTimeActual



#define DOCHECK( CHK, ERR )\
	bool valid = CHK ;\
	if( !valid )\
		AddErrorString( ERR );\
	else\
		AddErrorString( "" );\
	AllowOk( !valid );


// 050324 MVy: IDC_BEGINN_D lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Beginn_d( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeSchedule();
	DOCHECK( CheckPeriod( m_rngSchedule ), "scheduled not in a valid occupation" );
	//if( !Check_Date( pCcsEditNotify, m_CE_Beginn_d, m_CE_Beginn_t, "schedule start" ) ) return false ;
	//if( !Check_Date( pCcsEditNotify, m_CE_Ende_d,   m_CE_Ende_t,   "schedule end" ) ) return false ;
	return 0 ;
};	// OnEditKillfocus_CE_Beginn_d

// 050324 MVy: IDC_BEGINN_T lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Beginn_t( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeSchedule();
	DOCHECK( CheckPeriod( m_rngSchedule ), "scheduled not in a valid occupation" );
	// this check only makes sense if the corresponding data was set
	//Check_Beginn( pCcsEditNotify, m_CE_Beginn_d, m_CE_Beginn_t );
	return 0 ;
};	// OnEditKillfocus_CE_Beginn_t



// 050329 MVy: IDC_ENDE_D lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Ende_d( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeSchedule();
	DOCHECK( CheckPeriod( m_rngSchedule ), "scheduled not in a valid occupation" );
	//Check_Beginn( pCcsEditNotify, m_CE_Ende_d, m_CE_Ende_t );
	return 0 ;
};	// OnEditKillfocus_CE_Ende_d

// 050324 MVy: IDC_ENDE_T lost focus
LONG CCcaCommonDlg::OnEditKillfocus_CE_Ende_t( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeSchedule();
	DOCHECK( CheckPeriod( m_rngSchedule ), "scheduled not in a valid occupation" );
	// this check only makes sense if the corresponding data was set
	//Check_Ende( pCcsEditNotify, m_CE_Ende_d, m_CE_Ende_t );
	return 0 ;
};	// OnEditKillfocus_CE_Ende_t



LONG CCcaCommonDlg::OnEditKillfocus_CE_ActStartDate( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeActual();
	DOCHECK( CheckPeriod( m_rngActual, true ), "actual not in a valid occupation" );
	return 0 ;
};

LONG CCcaCommonDlg::OnEditKillfocus_CE_ActStartTime( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeActual();
	DOCHECK( CheckPeriod( m_rngActual, true ), "actual not in a valid occupation" );
	return 0 ;
};

LONG CCcaCommonDlg::OnEditKillfocus_CE_ActEndDate( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeActual();
	DOCHECK( CheckPeriod( m_rngActual, true ), "actual not in a valid occupation" );
	return 0 ;
};

LONG CCcaCommonDlg::OnEditKillfocus_CE_ActEndTime( CCSEDITNOTIFY* pCcsEditNotify )
{
	SetTimeActual();
	DOCHECK( CheckPeriod( m_rngActual, true ), "actual not in a valid occupation" );
	return 0 ;
};


#define DISPATCH_START()\
	LONG result = 0 ;\
	CCSEDITNOTIFY *pCcsEditNotify = (CCSEDITNOTIFY *)lParam ;

#define DISPATCH( NAME )\
	if( wParam == m_##NAME.imID )\
	{\
		ASSERT( pCcsEditNotify->SourceControl == &m_##NAME );		/*must be this control!*/\
		result = OnEditKillfocus_##NAME( pCcsEditNotify );\
	}\
	else

#define DISPATCH_END() {};


// 050324 MVy: dispatch kill focus event
LONG CCcaCommonDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	DISPATCH_START()
	DISPATCH( CE_Fluggs )
	DISPATCH( CE_Schalter )
	DISPATCH( CE_Beginn_d )
	DISPATCH( CE_Beginn_t )
	DISPATCH( CE_Ende_d )
	DISPATCH( CE_Ende_t )
	DISPATCH( CE_ActStartDate )
	DISPATCH( CE_ActStartTime )
	DISPATCH( CE_ActEndDate )
	DISPATCH( CE_ActEndTime )
	DISPATCH_END()

	return result ;
};	// OnEditKillfocus event

void CCcaCommonDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if( __CanHandleCheckinCounterRestrictionsForHandlingAgents() )
	{
		int nCmdShow = g_ulHandlingAgent ? SW_SHOW : SW_HIDE ;
		m_LstErrors.ShowWindow( nCmdShow );		// 050324 MVy: this window is only used when agent is active
	}
	else
		m_LstErrors.ShowWindow( SW_HIDE );		// 050324 MVy: this window is only used when agent is active


	m_LstErrors.ResetContent();

}

void CCcaCommonDlg::OnCancel() 
{
	m_LstErrors.ResetContent();
	
	CDialog::OnCancel();
}



void CCcaCommonDlg::OnDcicLogo1() 
{

	CCS_TRY

	CString olLogo;

	m_CE_Logo.GetWindowText(olLogo);


	AwBasicDataDlg *polDlg = NULL;

	polDlg = new AwBasicDataDlg(this, "FLG_CKI","ALC2,ALC3,LGSN,LGFN", "LGSN+", olLogo,true);

	
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Logo.SetWindowText(polDlg->GetField("LGSN") );
	}
	delete polDlg;

	CCS_CATCH_ALL

}

