// RotationTableReportSelect.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationTableReportSelect.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelect dialog


RotationTableReportSelect::RotationTableReportSelect(CWnd* pParent /*=NULL*/)
	: CDialog(RotationTableReportSelect::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationTableReportSelect)
	m_check1 = TRUE;
	m_check2 = TRUE;
	m_check3 = TRUE;
	m_check4 = TRUE;
	//}}AFX_DATA_INIT
}


void RotationTableReportSelect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationTableReportSelect)
	DDX_Check(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK2, m_check2);
	DDX_Check(pDX, IDC_CHECK3, m_check3);
	DDX_Check(pDX, IDC_CHECK4, m_check4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationTableReportSelect, CDialog)
	//{{AFX_MSG_MAP(RotationTableReportSelect)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelect message handlers
