// HEADER 
#ifndef _CEDAFLNODATA_H_ 
#define _CEDAFLNODATA_H_ 

#include <CCSCedaData.h> 

struct FLNODATA 
{
	char	Flno[11];	// Flight Number
	char    Fltn[8];
	char    Flns[2];
	char    Adid[2];

	//DataCreated by this class
	int      IsChanged;

	FLNODATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
//		IsChanged = DATA_UNCHANGED;
	}

}; // end FLNODATA

class CedaFlnoData : public CCSCedaData
{
public:
	CedaFlnoData();
	~CedaFlnoData();

    CCSPtrArray<FLNODATA> omData;
	CMapStringToPtr omFlnoMap;

	void ClearAll();
	BOOL ReadFlnoData(CString opSeason, CString opAlc);

	BOOL AddFlnoInternal(FLNODATA *prpFlno);
	void Reload(CString opAlc);
	FLNODATA *GetFlnoByFlno(char *pcpFlno);
	CString omAlc3;
	CString omSeason;
};

extern CedaFlnoData ogFlnoData;

#endif // _CEDAFLNODATA_H_ 
