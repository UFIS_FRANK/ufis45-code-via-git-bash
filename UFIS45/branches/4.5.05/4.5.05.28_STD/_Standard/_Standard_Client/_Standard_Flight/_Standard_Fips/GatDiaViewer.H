// stviewer.h : header file
//

#ifndef _GATDIAVIEWER_H_
#define _GATDIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <DiaCedaFlightData.h>
#include <CedaBasicData.h>
#include <utils.h>


struct GATDIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct GATDIA_BARDATA 
{
	long Urno;
	char FPart;
	int  Gatno;
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	char AFPart;
	char DFPart;
    CString Text;
    CTime StartTime;
    CTime EndTime;
	CTime GatBegin;
	CTime GatEnd;
	CTime WroBegin;
	CTime WroEnd;
	CTime BltBegin;
	CTime BltEnd;
	CTime Tmo;
	CTime Land;
	CTime Onbl;
	CTime Ofbl;
	CTime Slot;
	CTime Airb;

	CString Belt;
	CString Gate;
	CString Wro;
	CString StatusText;
	CString Paxt;

    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<GATDIA_INDICATORDATA> Indicators;

	CString SpecialREQ;
	int SpecialREQStaus;

	CString AAlert;//Arrival Flight Alert string to use to show the popup icon menu//AM 20080306
	CString DAlert;//Arrival Flight Alert string to use to show the popup icon menu//AM 20080306


	GATDIA_BARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		GatBegin = TIMENULL;
		GatEnd = TIMENULL;
		WroBegin = TIMENULL;
		WroEnd = TIMENULL;
		BltBegin = TIMENULL;
		BltEnd = TIMENULL;
		Tmo = TIMENULL;
		Land = TIMENULL;
		Onbl = TIMENULL;
		Ofbl = TIMENULL; 
		Slot = TIMENULL;
		Airb = TIMENULL;
		TextColor = COLORREF(BLACK);
		SpecialREQ = "";
		SpecialREQStaus = NO_REQ;
		AAlert = "";//AM 20080306
		DAlert = "";//AM 20080306
	}
};
struct GATDIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
    CBrush *SepBrush;
	GATDIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
	}
};
struct GATDIA_LINEDATA 
{
	long Urno;
	CString Gnam;
	CString Term;
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	CTime StartTime;
	CTime EndTime;
    CString Text;
	bool IsExpanded;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<GATDIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<GATDIA_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	bool bmFullHeight;
	// Grouping
	CBrush *BkBrush;
	CBrush *SepBrush;
	GATDIA_LINEDATA(void)
	{
		IsExpanded = false;
		bmFullHeight = false;
	}
};
struct GATDIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;
	CBrush *BkBrush;
	CBrush *SepBrush;

    CCSPtrArray<GATDIA_LINEDATA> Lines;
	bool bmFullHeight;
	GATDIA_GROUPDATA(void)
	{
		bmFullHeight = false;
	}
};

// Attention:
// Element "TimeOrder" in the struct "GATDIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array GATDIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the GATDIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array GATDIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// GatDiagramViewer window

class GatGantt;
class GatChart;
 
class GatDiagramViewer: public CViewer
{
	friend GatGantt;

// Constructions
public:
    GatDiagramViewer();
    ~GatDiagramViewer();

	int  AdjustBar(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight,int &ripChartNo);
	int  FindFirstBar(long lpUrno,int &ripChartNo); 
//	void RuleChanged(CString opName);

	void Attach(CWnd *popAttachWnd); 
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);
	bool CalculateTimes(const DIAFLIGHTDATA &rrpFlight, char cpFPart, CTime &opStart, CTime &opEnd) const;
	bool CalculateTimes(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CTime &opStart, CTime &opEnd);

	int GetItemID(int ipGroupNo, int ipLineNo) const;
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}

    int GetLineCount(int ipGroupno);
	int GetAllLineCount();

	void MakeMasstab();
	void AllowUpdates(BOOL bpNoUpdatesNow);

	void PrintGantt(GatChart *popChart,bool bpFirstChart);
	void PrintGanttEnd();
	void PrintSingleLine(int ipGroupNo, int ipLineNo);


	void ProcessFlightInsert(CUIntArray *popRkeys);
	void ProcessFlightChange(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DIAFLIGHTDATA *prpFlight);
	void ProcessGatChange(RecordSet *prpRecord);
	void ProcessBlkChange( RecordSet *popBlkRecord);

	BOOL bmNoUpdatesNow;
	BOOL igFirstGroupOnPage;

	CTime omMarkTimeStart;
	CTime omMarkTimeEnd;
	
	void ProcessGatBarUpdate(long *prpUrno,int ipDDXType);

	void UpdateLineHeights();
	void SelectBar(const GATDIA_BARDATA *prpBar, bool bpHighLight = false);
	void DeSelectAll();
	COLORREF GetTextColor(const DIAFLIGHTDATA *prpFlight, char cpFPart, int ipBelt);
	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);
	CString GetGroupText(int ipGroupno);
     CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupCount();
	int GetAbsoluteLine(int ipGroupNo, int ipLineNo);

// Internal data processing routines
private:
 	bool IsPassFilter(DIAFLIGHTDATA *prpFlight);
	bool IsPassFilter(RecordSet &ropRec);

	int CompareGroup(GATDIA_GROUPDATA *prpGroup1, GATDIA_GROUPDATA *prpGroup2);
	int CompareLine(GATDIA_LINEDATA *prpLine1, GATDIA_LINEDATA *prpLine2);

	bool MakeGroups();
	bool MakeGroupsOld();
	bool MakeGroupAll();
 	void MakeBars();
//	void MakeLine(GATDIA_GROUPDATA &rrpGroup, RecordSet &ropRec);
//	void MakeLineData(GATDIA_LINEDATA *prlLine, RecordSet &ropRec);
	void MakeLine(GATDIA_GROUPDATA &rrpGroup, RecordSet &ropRec, bool bpRECfromBLK = false);
	void MakeLineData(GATDIA_GROUPDATA &rrpGroup,GATDIA_LINEDATA *prlLine, RecordSet &ropRec, bool bpRECfromBLK = false);
	void MakeTriangleColors(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, GATDIA_BARDATA *prpBar);
	bool MakeBarData(GATDIA_BARDATA *prlBar, const DIAFLIGHTDATA *popFlight, char cpFPart, int ipGatNo);

	void MakeBar(int ipGroupno, int ipLineno, DIAFLIGHTDATA *popFlight, char cpFPart, int ipGatNo);

	//void SetKonfliktColor(int ipGroupno, int ipLineno);
	void SetExpanded(int ipGroupno, int ipLineno);

	bool CheckBkBarParameters(int ipGroupno, int ipLineno, int ipBkBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno, int ipBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno) const;

    GATDIA_GROUPDATA *GetGroup(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);

    GATDIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
	
	
	bool GetGroupAndLineNoOfItem(int ipItemID, int &ripGroupNo, int &ripLineNo) const;
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);

	CBrush *GetBarColor(const DIAFLIGHTDATA *prpFlight, char cpFPart, int ipGatNo);
	CBrush *GetBarColor(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, int ipGatNo);

	long GetRkeyFromRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	//bool GetFlightsInRotation(DiaCedaFlightData::RKEYLIST *popRotation, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	DIAFLIGHTDATA * GetFlightAInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	DIAFLIGHTDATA * GetFlightDInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    GATDIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    GATDIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	GATDIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);
    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	bool GetGroupsFromViewer(CStringArray &ropGroups);
	CString GroupText(int ipGroupNo);
	CString LineText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(GATDIA_BARDATA *prlBar);
	CString StatusText(DiaCedaFlightData::RKEYLIST *popRot);

	int FindGroup(CString opGroupName);
	bool FindLine(CString opGnam, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, 
						CUIntArray &ropLines, CUIntArray &ropBars);

 
	bool IsExpandedLine(int ipGroupno, int ipLineno);

    int CreateGroup(GATDIA_GROUPDATA *prpGroup);
    int CreateLine(GATDIA_GROUPDATA &rrpGroup, GATDIA_LINEDATA *prpLine);
    int CreateBkBar(int ipGroupno, int ipLineno, GATDIA_BKBARDATA *prpBkBar); 
    int CreateBar(int ipGroupno, int ipLineno, GATDIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	GATDIA_INDICATORDATA *prpIndicator);

	void DeleteGroup(int ipGroupno);
	void DeleteLine(int ipGroupno, int ipLineno);
	void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno, bool bpSetOverlapColor = true);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);
    void DeleteAll();

	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintDiaArea(GatChart *popChart);
	void PrintDiagramHeader();

	void PrintPrepareBKLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintBKLine);

	void UtcToLocal(GATDIA_BKBARDATA &ropBkBar) const;
	void UpdateGateLines(CString opPos);
 	bool GetFullHeight(int ipGroupNo, int ipLineno) const;

	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;

	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBkBrush2;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;

    CCSPtrArray<GATDIA_GROUPDATA> omGroups;
	
	CCSPrint *pomPrint;
	CImageList* omImageListNotValid;

};

/////////////////////////////////////////////////////////////////////////////

#endif
