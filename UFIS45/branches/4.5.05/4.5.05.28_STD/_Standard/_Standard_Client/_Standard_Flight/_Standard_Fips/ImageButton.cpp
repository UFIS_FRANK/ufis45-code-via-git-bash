// ImageButton.cpp: implementation of the CImageButton class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImageButton.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CImageButton::CImageButton()
{
	m_bHover = FALSE;
	m_bTracking = FALSE;
	m_bHorizontal = FALSE;
	m_track = NULL;
	m_ToolTip = NULL;
	m_ButtonSize.cx = 0;
	m_ButtonSize.cy = 0;
	m_bAllowMove = FALSE;
	imBtnId = 0;
	cmId = "";
	
	bmHasArr = FALSE;
	bmHasDep = FALSE;
	bmHasInitedRgn = FALSE;
	SetFont("Arial",14);
}

CImageButton::~CImageButton()
{
	delete m_ToolTip;
	omFont.DeleteObject();
}
IMPLEMENT_DYNAMIC(CImageButton, CBitmapButton)

BEGIN_MESSAGE_MAP(CImageButton, CBitmapButton)
	//{{AFX_MSG_MAP(CImageButton)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////
 //	CImageButton message handlers
		
void CImageButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	//	TODO: Add your message handler code here and/or call default
	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = 1;
		m_bTracking = _TrackMouseEvent(&tme);
		m_point = point;
	}
	CBitmapButton::OnMouseMove(nFlags, point);
}

BOOL CImageButton::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_ToolTip != NULL)
		if (::IsWindow(m_ToolTip->m_hWnd)) // Incase m_ToolTip isn't NULL, check to see if its a valid window
			m_ToolTip->RelayEvent(pMsg);		
	return CButton::PreTranslateMessage(pMsg);
}

// Set the tooltip with a string resource
void CImageButton::SetToolTipText(UINT nId, BOOL bActivate)
{
	// load string resource
	m_tooltext.LoadString(nId);
	// If string resource is not empty
	if (m_tooltext.IsEmpty() == FALSE) SetToolTipText(m_tooltext, bActivate);

}

// Set the tooltip with a CString
void CImageButton::SetToolTipText(CString spText, BOOL bActivate)
{
	// We cannot accept NULL pointer
	if (spText.IsEmpty()) return;

	// Initialize ToolTip
	InitToolTip();
	m_tooltext = spText;

	// If there is no tooltip defined then add it
	if (m_ToolTip->GetToolCount() == 0)
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_ToolTip->AddTool(this, m_tooltext, rectBtn, 1);
	}

	// Set text for tooltip
	m_ToolTip->UpdateTipText(m_tooltext, this, 1);
	m_ToolTip->Activate(bActivate);
}

void CImageButton::InitToolTip()
{
	if (m_ToolTip == NULL)
	{
		m_ToolTip = new CToolTipCtrl;
		// Create ToolTip control
		m_ToolTip->Create(this);
		m_ToolTip->Activate(TRUE);
	}
} // End of InitToolTip

// Activate the tooltip
void CImageButton::ActivateTooltip(BOOL bActivate)
{
	// If there is no tooltip then do nothing
	if (m_ToolTip->GetToolCount() == 0) return;

	// Activate tooltip
	m_ToolTip->Activate(bActivate);
} // End of EnableTooltip

void CImageButton::DrawButtonWithText(CDC* pDC, const char* pcpText)
{
	pDC->SetBkMode(TRANSPARENT);
    pDC->SetTextColor( RGB(10,10,200) );
	CRect rect;
	this->GetClientRect(&rect);
      
    HFONT hOldFont=NULL;
    //Sets the font

    if(omFont.GetSafeHandle() != NULL)
    {
        hOldFont =(HFONT) pDC->SelectObject(omFont.GetSafeHandle());
    }
	rect.top = (rect.bottom - rect.top )/2 - 5;
	if (rect.top<0) rect.top = 0;
//  pDC->TextOut(m_nXpos,m_nYpos,m_strText);
    pDC->DrawText( pcpText, &rect , DT_CENTER);

    //Reset the old font
    if(hOldFont != NULL)
    {
        pDC->SelectObject(hOldFont);
    }
}

void CImageButton::SetFont(CString srtFntName_i, int nSize_i)
{
		LOGFONT lfCtrl = {0};
		lfCtrl.lfOrientation	= 0 ;
		lfCtrl.lfEscapement		= 0 ;

		lfCtrl.lfHeight			= nSize_i;
		
		lfCtrl.lfItalic		= FALSE;
		lfCtrl.lfUnderline	= FALSE;
		lfCtrl.lfStrikeOut	= FALSE;

		lfCtrl.lfCharSet		= DEFAULT_CHARSET;
		lfCtrl.lfQuality		= DEFAULT_QUALITY;
		lfCtrl.lfOutPrecision	= OUT_DEFAULT_PRECIS;
		lfCtrl.lfPitchAndFamily	= DEFAULT_PITCH;
		_tcscpy(lfCtrl.lfFaceName, srtFntName_i);
	
		//Normal Font
		lfCtrl.lfWeight			= FW_NORMAL;	
		omFont.CreateFontIndirect( &lfCtrl);
}


void CImageButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	int ilWidth, ilHeight;
	ilWidth = lpDrawItemStruct->rcItem.right-lpDrawItemStruct->rcItem.left;
	ilHeight = lpDrawItemStruct->rcItem.bottom-lpDrawItemStruct->rcItem.top;
	CDC *mydc=CDC::FromHandle(lpDrawItemStruct->hDC);


	if ((m_ButtonSize.cx == 0) || (m_ButtonSize.cy == 0)) // If our Button size if 0, we have no bitmap so draw Regular...
	{
		//UINT style = GetButtonStyle();
		//style &= ~BS_OWNERDRAW;
		//SetButtonStyle(style); //No Bitmap so remove OwnerDraw and Refresh
		CString olStr ;
		this->GetWindowText( olStr );
		DrawButtonWithText( mydc, olStr );
		ShowArrDepIndicator( mydc, ilWidth, ilHeight );
		return;
	}

	CDC * pMemDC = new CDC;
	pMemDC -> CreateCompatibleDC(mydc);

	CBitmap * pOldBitmap;
	pOldBitmap = pMemDC -> SelectObject(&mybitmap);
	
	CPoint point(0,0);	
	// Depending on wether images are horizontal or vertical, pulls out correct image	
	if(lpDrawItemStruct->itemState & ODS_SELECTED)
	{
		mydc->StretchBlt(0,0, ilWidth,
				ilHeight,
				pMemDC,0,0, m_ButtonSize.cx,m_ButtonSize.cy, SRCCOPY );
	}
	else
	{
		
		if(m_bHover)
		{
			mydc->StretchBlt(0,0, ilWidth,
					ilHeight,
					pMemDC,0,0, m_ButtonSize.cx,m_ButtonSize.cy, SRCCOPY );
		}else
		{
			mydc->StretchBlt(0,0, ilWidth,
					ilHeight,
					pMemDC,0,0, m_ButtonSize.cx,m_ButtonSize.cy, SRCCOPY );
		}	
		
	}

	ShowArrDepIndicator( mydc, ilWidth, ilHeight );
	// clean up
	pMemDC -> SelectObject(pOldBitmap);
	delete pMemDC;
}


void CImageButton::ShowArrDepIndicator(CDC* popDc, int ipWidth, int ipHeight)
{
	if (!bmHasInitedRgn)
	{
		omBrushArr.CreateSolidBrush(RGB(0,255,0));
		omBrushDep.CreateSolidBrush(RGB(255,0,0));

		CPoint olPointArr[3];
		olPointArr[0].x = 0;	olPointArr[0].y = 0;
		olPointArr[1].x = ipWidth /3;	olPointArr[1].y = 0;
		olPointArr[2].x = 0;	olPointArr[2].y = ipHeight/3;
		omRgnArr.CreatePolygonRgn(olPointArr,3,WINDING);

		CPoint olPointDep[3];
		olPointDep[0].x = ipWidth;		olPointDep[0].y = ipHeight;
		olPointDep[1].x = ipWidth- ipWidth/3;	olPointDep[1].y =ipHeight;
		olPointDep[2].x = ipWidth;		olPointDep[2].y = ipHeight - ipHeight/3;
		omRgnDep.CreatePolygonRgn(olPointDep,3,WINDING);

		bmHasInitedRgn = TRUE;
	}

	if (bmHasArr)
	{
		popDc->FillRgn( &omRgnArr, &omBrushArr );
	}

	if (bmHasDep)
	{
		popDc->FillRgn( &omRgnDep, &omBrushDep );
	}

}

// Load a bitmap from the resources in the button, the bitmap has to have 3 buttonsstates next to each other: Up/Down/Hover
BOOL CImageButton::LoadBitmap(UINT bitmapid)
{
	UINT style = GetButtonStyle();//If Bitmaps are being loaded and the BS_OWNERDRAW is not set
						// then reset style to OwnerDraw.
	if (!(style & BS_OWNERDRAW))
	{
		style |= BS_OWNERDRAW;
		SetButtonStyle(style);
	}
	mybitmap.Attach(::LoadImage(::AfxGetInstanceHandle(),MAKEINTRESOURCE(bitmapid), IMAGE_BITMAP,0,0,LR_DEFAULTCOLOR));
	BITMAP	bitmapbits;
	mybitmap.GetBitmap(&bitmapbits);
	/* This checks to see which is bigger, the height or width and divides the 
	opposite by 3 to calculate whether the images are laid out vertical or horizontal*/
	if (!m_bHorizontal)
	{
		m_ButtonSize.cy=bitmapbits.bmHeight/3;
		m_ButtonSize.cx=bitmapbits.bmWidth;
	}
	else
	{
		m_ButtonSize.cy=bitmapbits.bmHeight;
		m_ButtonSize.cx=bitmapbits.bmWidth/3;
	}
	SetWindowPos( NULL, 0,0, m_ButtonSize.cx,m_ButtonSize.cy,SWP_NOMOVE   |SWP_NOOWNERZORDER   );
	return TRUE;
}

// Load a bitmap from the resources in the button, the bitmap has to have 3 buttonsstates next to each other: Up/Down/Hover
BOOL CImageButton::LoadBitmapFromFile(LPCTSTR lpszResourceName, int ipWidth, int ipHeight)
{
	UINT style = GetButtonStyle();//If Bitmaps are being loaded and the BS_OWNERDRAW is not set
						// then reset style to OwnerDraw.
	if (!(style & BS_OWNERDRAW))
	{
		style |= BS_OWNERDRAW;
		SetButtonStyle(style);
	}
	
	if ((_access( lpszResourceName, 0 )) != -1 )
	{//File Exist
		try
		{
			mybitmap.Detach();
		}catch(...)
		{
		}
		mybitmap.Attach(::LoadImage(AfxGetInstanceHandle(), 
                         lpszResourceName,
                         IMAGE_BITMAP,
                         ipWidth,ipHeight,
                         LR_LOADFROMFILE | LR_CREATEDIBSECTION));
		m_ButtonSize.cx = ipWidth;
		m_ButtonSize.cy = ipHeight;
	}
	else
	{//File does not exist
		m_ButtonSize.cx = 0;
		m_ButtonSize.cy = 0;
	}
	SetWindowPos( NULL, 0,0, ipWidth,ipHeight,SWP_NOMOVE   |SWP_NOOWNERZORDER   );
	return TRUE;
}

// Load a bitmap from the resources in the button, the bitmap has to have 3 buttonsstates next to each other: Up/Down/Hover
BOOL CImageButton::LoadBitmap(LPCTSTR lpszResourceName)
{
	UINT style = GetButtonStyle();//If Bitmaps are being loaded and the BS_OWNERDRAW is not set
						// then reset style to OwnerDraw.
	if (!(style & BS_OWNERDRAW))
	{
		style |= BS_OWNERDRAW;
		SetButtonStyle(style);
	}
	mybitmap.Attach(::LoadImage(AfxGetInstanceHandle(), 
                         lpszResourceName,
                         IMAGE_BITMAP,
                         0,0,
                         LR_DEFAULTCOLOR));
	BITMAP	bitmapbits;
	mybitmap.GetBitmap(&bitmapbits);
	/* This checks to see which is bigger, the height or width and divides the 
	opposite by 3 to calculate whether the images are laid out vertical or horizontal*/
	if (!m_bHorizontal)
	{
		m_ButtonSize.cy=bitmapbits.bmHeight/3;
		m_ButtonSize.cx=bitmapbits.bmWidth;
	}
	else
	{
		m_ButtonSize.cy=bitmapbits.bmHeight;
		m_ButtonSize.cx=bitmapbits.bmWidth/3;
	}
	SetWindowPos( NULL, 0,0, m_ButtonSize.cx,m_ButtonSize.cy,SWP_NOMOVE   |SWP_NOOWNERZORDER   );
	return TRUE;
}

void CImageButton::SetHorizontal(bool ImagesAreLaidOutHorizontally)
{
	// If true than the images are next to each other horizontally, else they are vertical
	m_bHorizontal = ImagesAreLaidOutHorizontally;
}

LRESULT CImageButton::OnMouseHover(WPARAM wparam, LPARAM lparam) 
{
	// TODO: Add your message handler code here and/or call default
	m_bHover=TRUE;
	/* This line corrects a problem with the tooltips not displaying when 
	the mouse passes over them, if the parent window has not been clicked yet.
	Normally this isn't an issue, but when developing multi-windowed apps, this 
	bug would appear. Setting the ActiveWindow to the parent is a solution to that.
	*/
	::SetActiveWindow(GetParent()->GetSafeHwnd());
	Invalidate();
	DeleteToolTip();
	// Create a new Tooltip with new Button Size and Location
	SetToolTipText(m_tooltext);
	if (m_ToolTip != NULL)
		if (::IsWindow(m_ToolTip->m_hWnd))
			//Display ToolTip
			m_ToolTip->Update();

	return 0;
}


LRESULT CImageButton::OnMouseLeave(WPARAM wparam, LPARAM lparam)
{
	m_bTracking = FALSE;
	m_bHover=FALSE;
	Invalidate();
	return 0;
}

void CImageButton::OnRButtonDown( UINT nFlags, CPoint point )
{
	if (IsMoveable() == TRUE)
	{
		m_point = point;
		CRect rect;
		GetWindowRect(rect);//Tell the tracker where we are
		ScreenToClient(rect);
		m_track = new CRectTracker(&rect, CRectTracker::dottedLine | 
								CRectTracker::resizeInside |
								CRectTracker::hatchedBorder);
		if (nFlags & MK_CONTROL) // If Ctrl + Right-Click then Resize object
		{
			GetWindowRect(rect);
			GetParent()->ScreenToClient(rect);
			m_track->TrackRubberBand(GetParent(), rect.TopLeft());
			m_track->m_rect.NormalizeRect();
		}
		else // If not Ctrl + Right-Click, then Move Object
		{
			m_track->Track(GetParent(), point);
		}
		rect = LPRECT(m_track->m_rect); 
		MoveWindow(&rect,TRUE);//Move Window to our new position
		delete m_track;
		m_track = NULL;
		rect = NULL;
	}
	CBitmapButton::OnRButtonDown(nFlags, point);
}

void CImageButton::DeleteToolTip()
{
	// Destroy Tooltip incase the size of the button has changed.
	if (m_ToolTip != NULL)
	{
		delete m_ToolTip;
		m_ToolTip = NULL;
	}
}


void CImageButton::SetBtnId(const int ipBtnId)
{
	imBtnId = ipBtnId;
}

int CImageButton::GetBtnId()
{
	return imBtnId;
}


void CImageButton::SetId(const CString cpId)
{
	cmId = cpId;
}

CString CImageButton::GetId()
{
	return cmId;
}

void CImageButton::OnLButtonDown( UINT nFlags, CPoint point )
{
	//GetParent()->SendMessage(WM_BUTTON_RBUTTONDOWN,nFlags,(LPARAM) this);
	GetParent()->PostMessage(WM_BUTTON_RBUTTONDOWN,nFlags,(LPARAM) this);
	//CBitmapButton::OnRButtonDown(nFlags, point);
}

void CImageButton::SetHasArr( BOOL bpHasArr )
{
	bmHasArr = bpHasArr;
}

BOOL CImageButton::GetHasArr()
{
	return bmHasArr;
}
void CImageButton::SetHasDep( BOOL bpHasDep )
{
	bmHasDep = bpHasDep;
}

BOOL CImageButton::GetHasDep()
{
	return bmHasDep;
}


