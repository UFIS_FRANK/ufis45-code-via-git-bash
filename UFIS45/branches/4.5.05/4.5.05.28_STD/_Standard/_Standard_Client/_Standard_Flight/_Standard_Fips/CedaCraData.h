// CedaCraData.h   //PRF 8379 

// HEADER 
#ifndef _CEDACRADATA_H_ 
#define _CEDACRADATA_H_ 

#include <CCSCedaData.h> 

struct CRADATA 
{
	long 	 Urno;
	long 	 Curn;
	long 	 Furn;
	char 	 Aloc[12];
	char 	 Oval[12];
	char 	 Nval[12];
	char 	 Usec[34];
	CTime 	 Cdat;
	char 	 Hopo[4];
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	CRADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
	}

}; // end CRAData

class CedaCraData : public CCSCedaData
{
public:
	CedaCraData();
	~CedaCraData();

    CCSPtrArray<CRADATA> omData;
	CMapPtrToPtr omUrnoMap;
	void ClearAll();
	bool ReadCraData();
	bool Insert(CRADATA *prpPar);
	bool InsertDB(CRADATA *prpPar, bool bpDelete = false);
	bool InsertInternal(CRADATA *prpPar);
	bool Update(CRADATA *prpPar);
	bool UpdateInternal(CRADATA *prpPar);
	bool Delete(long lpUrno);
	void DeleteInternal(CRADATA *prpPar);
	bool ReadSpecial(CCSPtrArray<CRADATA> &ropList, char *pspWhere);
	//////////////////////////////////////
	bool Save(CRADATA *prpCra);
	CRADATA*	GetCraByUrno(long lpUrno);

};

extern CedaCraData ogCraData;

#endif // _CEDACRADATA_H_ 
