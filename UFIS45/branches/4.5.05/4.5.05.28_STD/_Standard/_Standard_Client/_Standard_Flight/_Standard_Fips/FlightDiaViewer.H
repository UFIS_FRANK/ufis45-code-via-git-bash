// stviewer.h : header file
//
// Modification History: 
//	081100	rkr	GetTextForStatusBar() added

#ifndef _FLIGHTDIAVIEWER_H_
#define _FLIGHTDIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <DiaCedaFlightData.h>
#include <Utils.h>

struct FLIGHTDIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct FLIGHTDIA_BARDATA 
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CString PrintText;
    CTime StartTime;
    CTime EndTime;
	CTime GatBegin;
	CTime GatEnd;
	CTime WroBegin;
	CTime WroEnd;
	CTime BltBegin;
	CTime BltEnd;
	CTime PosBegin;
	CTime PosEnd;
	CTime CcaBegin;
	CTime CcaEnd;
	CTime Tmo;
	CTime Land;
	CTime Onbl;
	CTime Ofbl;
	CTime Slot;
	CTime Airb;

	CString Cca;
	CString Pos;
	CString Belt;
	CString Gate;
	CString Wro;
	CString StatusText;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    CBrush *MarkerBrushRightSide;
	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<FLIGHTDIA_INDICATORDATA> Indicators;

	CString SpecialREQ;
	int SpecialREQStaus;

	FLIGHTDIA_BARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		GatBegin = TIMENULL;
		GatEnd = TIMENULL;
		WroBegin = TIMENULL;
		WroEnd = TIMENULL;
		BltBegin = TIMENULL;
		BltEnd = TIMENULL;
		Tmo = TIMENULL;
		Land = TIMENULL;
		Onbl = TIMENULL;
		Ofbl = TIMENULL; 
		Slot = TIMENULL;
		Airb = TIMENULL;
		SpecialREQ = "";
		SpecialREQStaus = NO_REQ;
	}
};
struct FLIGHTDIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	FLIGHTDIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
	}
};
struct FLIGHTDIA_LINEDATA 
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	CTime StartTime;
	CTime EndTime;
    CString Text;
	bool IsExpanded;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<FLIGHTDIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<FLIGHTDIA_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	FLIGHTDIA_LINEDATA(void)
	{
		IsExpanded = false;
	}
};
struct FLIGHTDIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;			// Gruppen ID
	CString GroupType;			// 
	CCSPtrArray<CString> Codes; // For future Airlinecodes
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<FLIGHTDIA_LINEDATA> Lines;
};

// Attention:
// Element "TimeOrder" in the struct "FLIGHTDIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array FLIGHTDIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the FLIGHTDIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array FLIGHTDIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// FlightDiagramViewer window

class FlightGantt;

class FlightDiagramViewer: public CViewer
{
	friend FlightGantt;

// Constructions
public:
    FlightDiagramViewer();
    ~FlightDiagramViewer();

	int  AdjustBar(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight);
	int  FindFirstBar(long lpUrno); 

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

// Internal data processing routines
public:
	void DeSelectAll();
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassFilter(DiaCedaFlightData::RKEYLIST *prpRot);
	int CompareGroup(FLIGHTDIA_GROUPDATA *prpGroup1, FLIGHTDIA_GROUPDATA *prpGroup2);
	int CompareLine(FLIGHTDIA_LINEDATA *prpLine1, FLIGHTDIA_LINEDATA *prpLine2);

	void MakeGroups();
	void MakeLines();
	void MakeLinesOfRot(DiaCedaFlightData::RKEYLIST *popRot);
	void MakeLine(DIAFLIGHTDATA *prpA, DIAFLIGHTDATA *prpD);
	void MakeLineData(FLIGHTDIA_LINEDATA *prlLine, const DIAFLIGHTDATA *prpA, const DIAFLIGHTDATA *prpD);
	void MakeTriangleColors(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, FLIGHTDIA_BARDATA *prpBar);

	void MakeBarData(FLIGHTDIA_BARDATA *prlBar, DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight);
	void MakeBar(int ipGroupno, int ipLineno, DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight);

	long GetRkeyFromRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	
	bool GetFlightsInRotation(DiaCedaFlightData::RKEYLIST *popRotation, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	DIAFLIGHTDATA * GetFlightAInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	DIAFLIGHTDATA * GetFlightDInRotation(DiaCedaFlightData::RKEYLIST *popRotation);

	CString GroupText(int ipGroupNo);
	CString LineText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(FLIGHTDIA_BARDATA *prlBar);
	CString StatusText(DiaCedaFlightData::RKEYLIST *popRot);
	int FindGroup(CString opGroupName);
	bool FindLine(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, 
						CUIntArray &ropLines, CUIntArray &ropBars);


// Operations
private:
	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;
public:
    int GetGroupCount();
    FLIGHTDIA_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetLineCount(int ipGroupno);
    FLIGHTDIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	bool IsExpandedLine(int ipGroupno, int ipLineno);
	void SetExpanded(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    FLIGHTDIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    FLIGHTDIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	FLIGHTDIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

    void DeleteAll();
    int CreateGroup(FLIGHTDIA_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
    int CreateLine(int ipGroupno, FLIGHTDIA_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, FLIGHTDIA_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, FLIGHTDIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	FLIGHTDIA_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	void MakeMasstab();

	void UtcToLocal(FLIGHTDIA_BARDATA *prlBar);
	void UtcToLocal(FLIGHTDIA_BKBARDATA *prlBar);


	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);

	//return a string about flightinformation; displayed in the staturbar
    CString GetTextForStatusBar (int ipGroupno, int ipLineno, int ipBarno);
// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	void GetGroupsFromViewer(CCSPtrArray<FLIGHTDIA_GROUPDATA> &ropGroups);

	bool CalculateWro(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlight);
	bool CalculateBlt(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlight);
	bool CalculateCca(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlight);
	bool CalculateGateAD(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlight);
	bool CalculateGateA(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlight);
	bool CalculateGateD(FLIGHTDIA_BARDATA *prpBar, const DIAFLIGHTDATA *prpFlight);

	CBrush *GetBarColor(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	bool CalculateBarColors(FLIGHTDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	CBrush *GetBarColor(DIAFLIGHTDATA *prpFlight, char cpFPart);
	bool SetKonfliktColor(int ipGroupno, int ipLineno);
	void SelectBar(const FLIGHTDIA_BARDATA *prpBar, bool bpHighLight = false);
	COLORREF GetTextColor(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

// Attributes used for filtering condition
private:
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;

// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<FLIGHTDIA_GROUPDATA> omGroups;

	void ProcessFlightBc(int ipDDXType, void *vpDataPointer);
	void ProcessFlightInsert(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightUpdate(DIAFLIGHTDATA *prpFlight);
	void ProcessConflictUpdate(DIAFLIGHTDATA *prpFlight);
	void ProcessPosBarUpdate(long *prpUrno,int ipDDXType);
public:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;
///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;

public:
};

/////////////////////////////////////////////////////////////////////////////

#endif
