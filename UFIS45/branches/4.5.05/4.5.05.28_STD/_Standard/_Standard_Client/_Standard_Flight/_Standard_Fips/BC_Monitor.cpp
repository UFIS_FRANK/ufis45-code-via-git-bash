// BC_Monitor.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "utils.h"
#include "BC_Monitor.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BC_Monitor dialog


BC_Monitor::BC_Monitor(CWnd* pParent /*=NULL*/)
	: CDialog(BC_Monitor::IDD, pParent)
{
	//{{AFX_DATA_INIT(BC_Monitor)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	isCreated = false;
	CDialog::Create(BC_Monitor::IDD, NULL );
	isCreated = true;


	CRect olRect;
	GetWindowRect(olRect);
	CString	m_key = "DialogPosition\\BcMonitor";
	GetDialogFromReg(olRect, m_key);

	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW/* | SWP_NOSIZE*/);

//	SetWindowPos(&wndTopMost, 0,0,0,0, SWP_SHOWWINDOW );
//	MoveWindow(0, 0, 400,400);	
//	ShowWindow(SW_SHOWNORMAL);
	lmCountReRead = 0;

//	CString omFile = "c:\\Ufis\\system\\test.txt";
//	of.open( omFile, ios::out);

}

BC_Monitor::~BC_Monitor()
{
//	of.close();
}

void BC_Monitor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BC_Monitor)
	DDX_Control(pDX, IDC_BCLIST, m_BcList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BC_Monitor, CDialog)
	//{{AFX_MSG_MAP(BC_Monitor)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BC_Monitor message handlers


int BC_Monitor::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void BC_Monitor::InsertBc(CString opString)
{
	int ilCount;
	int i;


	ilCount = m_BcList.GetCount();
	if (ilCount > 100)
	{
		for(i = ilCount; i > 100; i--)
		{
			m_BcList.DeleteString(i); 
		}
	}
	m_BcList.InsertString(0, opString);
	m_BcList.RedrawWindow();

//	of << 2 << endl;

}

void BC_Monitor::InsertCount()
{
	lmCountReRead++;
	CString olText;
	olText.Format("%s %ld", "BC processing ... Inf.-Req.: ", lmCountReRead);
	SetWindowText(olText);

}

void BC_Monitor::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			CRect olR;
			GetClientRect(&olR);
			m_BcList.MoveWindow(olR.left, olR.top, cx, cy);
			//pomTable->SetPosition(1, cx+1, 1/*m_nDialogBarHeight-1*/, cy+1);
		}
	}
	
}

void BC_Monitor::OnCancel() 
{
//	CDialog::OnCancel();
	CString	m_key = "DialogPosition\\BcMonitor";
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
	ShowWindow(SW_HIDE);
	of.close();
}
