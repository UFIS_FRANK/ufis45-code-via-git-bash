#ifndef __CicConfTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>
#include "TableLineData.h"

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

class CICCONFTABLE_LINEDATA : public CTableLineData
{
public:
	long Urno;
	long KKey;
	long FlightUrno;
	CString		Flno;
	CTime		Sto;
	CTime		Sto2;
	CString		Act3;
	CString		OrgDes;
	CTime		Ckbs;
	CTime		Ckes;
	CString     Ckic;
	CString		Nose;
	CString		Ftyp;
	CString		Remark;
	CString		Reason;
	CString		CounterGroup;
	bool	    Error;	
	int			Freq;
	bool		WoDem;

	CICCONFTABLE_LINEDATA(void)
	{ 
		FlightUrno = 0;
		Ckbs = -1;
		Ckes = -1;
		Urno = 0;
		Sto = -1;
		Sto2 = -1;
		KKey = 0;
		Freq	= 0;
		Error = false;
		WoDem = false;
	}
};



/////////////////////////////////////////////////////////////////////////////
// CicConfTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CicConfTableViewer

//@Man:
//@Memo: CicConfTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class CicConfTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CicConfTableViewer();
    //@ManMemo: Default destructor
    ~CicConfTableViewer();

	void UnRegister();

	void ClearAll();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<CICCONFTABLE_LINEDATA> omLines;
// Internal data processing routines
private:
//MWO
	bool IsPassFilter(DIACCADATA *prpCca);
	bool IsPassFilter(CCSPtrArray<DIACCADATA> &opCcas);

	int  CompareFlight(CICCONFTABLE_LINEDATA *prpFlight1, CICCONFTABLE_LINEDATA *prpFlight2);
	
	int CompareLines(CICCONFTABLE_LINEDATA *prpLine1, CICCONFTABLE_LINEDATA *prpLine2);
	
	bool DeleteLine(long lpUrno);
//MWO END


	void MakeLines();
	
	void MakeLineData(CICCONFTABLE_LINEDATA *prpLineData, DIACCADATA *prpCca);
	void MakeLineData(CICCONFTABLE_LINEDATA  *prpLineData, CCSPtrArray<DIACCADATA> &opCcas);


	int MakeLine(DIACCADATA *prpCca);
	bool  MakeLine( CCSPtrArray<DIACCADATA> &opCcas);

	
	void MakeColList(CICCONFTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	
	int  CreateLine(CICCONFTABLE_LINEDATA &rpLine);

	bool FindLine(long lpUrno, int &rilLineno);
	
	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine( int ipLineNo);



// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	void ProcessCcaChange(long *plpCcaUrno);

	bool bmInit;
	void GetCount(int& ipConf, int& ipWoDem);

private:
	CCSTable *pomTable;


// Methods which handle changes (from Data Distributor)
public:


};

#endif //__CicConfTableViewer_H__
