#if !defined(AFX_COLLECTHAIDLG_H__INCLUDED_)
#define AFX_COLLECTHAIDLG_H__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CollectHAIDlg.h : header file
//

#include <CCSTable.h>

#include <resrc1.h>


#define COLLECTHAIDLG_COLCOUNT 4


/////////////////////////////////////////////////////////////////////////////
// CollectHAIDlg dialog

class CollectHAIDlg : public CDialog
{
// Construction
public:
	CollectHAIDlg(CWnd* pParent = NULL);
	~CollectHAIDlg();

	void Clear();
	bool LinesSelected() const;
	CString GetStatus() const;
	bool IsLineSelected(int ipLineNo) const;



// Dialog Data
	//{{AFX_DATA(CollectHAIDlg)
	enum { IDD = IDD_COLLECT_HAI };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA

 
	CCSTable *pomTable;
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CollectHAIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CollectHAIDlg)
	afx_msg void OnClose();
	afx_msg void OnClear();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableLButtonDown( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableRButtonDown( UINT wParam, LPARAM lParam);
  	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CWnd* pomParent;

	void InitTable();
	void DrawHeader();
	void InsertEmptyLines();

 
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLLECTHAIDLG_H__INCLUDED_)
