// HTADlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <HTADlg.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// HTADlg dialog


HTADlg::HTADlg(CWnd* pParent, HTADATA *prpHta, ROTATIONDLGFLIGHTDATA *prpFlight )
	   :CDialog(HTADlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(HTADlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	prmHta = prpHta;
	prmFlight = prpFlight;


	if(prmHta == NULL)
		bmInsert = true;
	else
		bmInsert = false;


}


HTADlg::~HTADlg()
{

	ogDdx.UnRegister(this, NOTUSED);


}


void HTADlg::DoDataExchange(CDataExchange* pDX)
{ 
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HTADlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_HSNA, m_HSNA);
	DDX_Control(pDX, IDC_HTYP, m_HTYP);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	DDX_Control(pDX, IDC_AGENT_NAME, m_Agent_Name);
	DDX_Control(pDX, IDC_TYPE_NAME, m_Type_Name);
	DDX_Control(pDX, IDC_SVC_STDT, m_CE_SVC_Stdt);
	DDX_Control(pDX, IDC_SVC_ENDT, m_CE_SVC_Endt);
	DDX_Control(pDX, IDC_HTYPLIST, m_CB_HTYP);
	DDX_Control(pDX, IDC_HSNALIST, m_CB_HSNA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HTADlg, CDialog)
	//{{AFX_MSG_MAP(HTADlg)
	ON_BN_CLICKED(IDC_HSNALIST, OnHsnalist)
	ON_BN_CLICKED(IDC_HTYPLIST, OnHtyplist)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HTADlg message handlers

BOOL HTADlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//ogBCD.Read(CString("HAI"));

	CString olTemp;
	CString olCaption;

	if(prmFlight != NULL)
	{
		olCaption.LoadString(IDS_STRING2863);

		olCaption += CString(prmFlight->Flno);
	}

	m_CdatD.SetBKColor(SILVER);
	m_CdatT.SetBKColor(SILVER);
	m_Agent_Name.SetBKColor(SILVER);
	m_Type_Name.SetBKColor(SILVER);

	m_LstuD.SetBKColor(SILVER);
	m_LstuT.SetBKColor(SILVER);

	m_Usec.SetBKColor(SILVER);

	m_Useu.SetBKColor(SILVER);

	m_HSNA.SetFormat("x|#x|#x|#x|#x|#");
	m_HSNA.SetTextLimit(1,5);
	m_HSNA.SetTextErrColor(RED);
	m_HSNA.SetBKColor(YELLOW);
	

	m_HTYP.SetFormat("x|#x|#x|#x|#x|#");
	m_HTYP.SetTextLimit(1,5);
	m_HTYP.SetTextErrColor(RED);
	m_HTYP.SetBKColor(YELLOW);

	m_REMA.SetTypeToString("X(127)",127,0);
	m_REMA.SetTextErrColor(RED);

	m_CE_SVC_Stdt.SetTypeToTime(false, true);
	m_CE_SVC_Endt.SetTypeToTime(false, true);


	//UFIS-1070
	if (!bmInsert && !bgHAAllowChangeAgent)
	{
		m_HSNA.SetBKColor(SILVER);
		m_HTYP.SetBKColor(SILVER);
		m_HSNA.EnableWindow(FALSE);
		m_HTYP.EnableWindow(FALSE);
		m_CB_HSNA.ShowWindow( SW_HIDE);
		m_CB_HTYP.ShowWindow( SW_HIDE);
	}

	InitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void HTADlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}


void HTADlg::OnOK() 
{

	CCS_TRY

	UpdateData(true);

	CString olHSNA;
	CString olHTYP;
	CString olREMA;



	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText="";
	bool ilStatus = true;
	if(m_HSNA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + GetString(IDS_STRING2018) + CString("\n") + GetString(ST_BADFORMAT);
	}
	if(m_HTYP.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + GetString(IDS_STRING2016)  + CString("\n") + GetString(ST_BADFORMAT);
	}

	CTime olTime = CTime::GetCurrentTime();


	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_HSNA.GetWindowText(olHSNA);
		m_HTYP.GetWindowText(olHTYP);
		m_REMA.GetWindowText(olREMA);


		if(prmHta != NULL)
		{			
			prmHta->IsChanged =  DATA_CHANGED;

			prmHta->Lstu = olTime;
			strcpy(prmHta->Useu , pcgUser);
		}
		else
		{
			prmHta = new HTADATA;
			/*
			strcpy(prmHta->Hsna, olHSNA);
			strcpy(prmHta->Htyp, olHTYP);
			strcpy(prmHta->Rema, olREMA);
			strcpy(prmHta->Flag, "U");
			*/

			prmHta->IsChanged =  DATA_NEW;

			prmHta->Cdat = olTime;
			strcpy(prmHta->Usec , pcgUser);
			
			//prmHta->Flnu = prmFlight->Urno;

			//ogHtaData.Save(prmHta);
		}

		
		CTime ltFlSchDt = prmFlight->Stoa;
		if (prmFlight->Adid[0] != 'A') ltFlSchDt = prmFlight->Stod;

		m_CE_SVC_Stdt.GetWindowText(m_SVC_Stdt);
		if(!m_SVC_Stdt.IsEmpty())
		{
			prmHta->Stdt = HourStringToDate(m_SVC_Stdt, ltFlSchDt);
		}
		else
		{
			prmHta->Stdt = TIMENULL;
		}

		m_CE_SVC_Endt.GetWindowText(m_SVC_Endt);
		if(!m_SVC_Endt.IsEmpty())
		{
			prmHta->Endt = HourStringToDate(m_SVC_Endt, ltFlSchDt);
			if ((prmHta->Stdt != TIMENULL) && (prmHta->Endt<prmHta->Stdt))
			{
				olErrorText += "\'End Time' must be on or after 'Start Time'";
			}
		}
		else
		{
			prmHta->Endt = TIMENULL;
		}

		if (olErrorText.IsEmpty())
		{
			strcpy(prmHta->Hsna, olHSNA);
			strcpy(prmHta->Htyp, olHTYP);
			strcpy(prmHta->Rema, olREMA);
			strcpy(prmHta->Flag, "U");

			prmHta->Flnu = prmFlight->Urno;

			ogHtaData.Save(prmHta);
		}
		else
		{
			Beep(440,70);
			MessageBox(olErrorText, GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
			m_CE_SVC_Endt.SetFocus();
			m_CE_SVC_Endt.SetSel(0,-1);
			return;
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
		m_HSNA.SetFocus();
		m_HSNA.SetSel(0,-1);
	}

	CDialog::OnOK();

	CCS_CATCH_ALL

}


void HTADlg::OnHsnalist() 
{

	CCS_TRY
	CString olText;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAG","HSNA,HNAM", "HSNA+");
	if(polDlg->DoModal() == IDOK)
	{
		m_HSNA.SetInitText(polDlg->GetField("HSNA"), true);	

		ogBCD.GetField("HAG", "HSNA", polDlg->GetField("HSNA"), "HNAM", olText);			
		m_Agent_Name.SetInitText(olText);

	}
	delete polDlg;

	CCS_CATCH_ALL

}


void HTADlg::OnHtyplist() 
{

	CCS_TRY

	CString olText;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HSNA+");
	if(polDlg->DoModal() == IDOK)
	{
		m_HTYP.SetInitText(polDlg->GetField("HTYP"), true);	
		ogBCD.GetField("HTY", "HTYP", polDlg->GetField("HTYP"), "HNAM", olText);			
		m_Type_Name.SetInitText(olText);


	}
	delete polDlg;

	CCS_CATCH_ALL

}





void HTADlg::InitDialog()
{
	CString olText;	
		
	if(prmHta !=  NULL)
	{
		m_CdatD.SetInitText(prmHta->Cdat.Format("%d.%m.%Y"));
		m_CdatT.SetInitText(prmHta->Cdat.Format("%H:%M"));

		m_LstuD.SetInitText(prmHta->Lstu.Format("%d.%m.%Y"));
		m_LstuT.SetInitText(prmHta->Lstu.Format("%H:%M"));

		m_Usec.SetInitText(prmHta->Usec);
		m_Useu.SetInitText(prmHta->Useu);


		m_HSNA.SetInitText(prmHta->Hsna);
		m_REMA.SetInitText(prmHta->Rema);
		m_HTYP.SetInitText(prmHta->Htyp);

		ogBCD.GetField("HTY", "HTYP", prmHta->Htyp, "HNAM", olText);			
		m_Type_Name.SetInitText(olText);

		ogBCD.GetField("HAG", "HSNA", prmHta->Hsna, "HNAM", olText);			
		m_Agent_Name.SetInitText(olText);

		CTime ltFlSchDt = prmFlight->Stoa;
		if (prmFlight->Adid[0] != 'A') ltFlSchDt = prmFlight->Stod;

		CTime olTime;
		olTime = prmHta->Stdt;
		m_SVC_Stdt = DateToHourDivString(olTime, ltFlSchDt);

		olTime = prmHta->Endt;
		m_SVC_Endt = DateToHourDivString(olTime, ltFlSchDt);

		//m_SVC_Stdt = prmHta->Stdt.Format("%H:%M");
		//m_SVC_Endt = prmHta->Endt.Format("%H:%M");
		
		m_CE_SVC_Stdt.SetInitText(m_SVC_Stdt);
		m_CE_SVC_Stdt.SetWindowText(m_SVC_Stdt);
		
		m_CE_SVC_Endt.SetInitText(m_SVC_Endt);
		m_CE_SVC_Endt.SetWindowText(m_SVC_Endt);
	}

}



LONG HTADlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{		
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	CString olTmp;

	CString olText;
	prlNotify->UserStatus = true;

	if(((UINT)m_HSNA.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		ogBCD.GetField("HAG", "HSNA", prlNotify->Text, "HNAM", olText );
		if(!olText.IsEmpty())
		{
			m_Agent_Name.SetInitText(olText);
			prlNotify->Status = true;
		}
		else
		{
			m_Agent_Name.SetInitText("");
			prlNotify->Status = false;
		}
	}


	if(((UINT)m_HTYP.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		ogBCD.GetField("HTY", "HTYP", prlNotify->Text, "HNAM", olText );
		if(!olText.IsEmpty())
		{
			m_Type_Name.SetInitText(olText);
			prlNotify->Status = true;
		}
		else
		{
			m_Type_Name.SetInitText("");
			prlNotify->Status = false;
		}
	}



	return 0L;

}

