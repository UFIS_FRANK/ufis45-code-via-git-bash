// ReportOvernightTableViewer.cpp : implementation file
// 
// according ot flight cancelled
// Angabe eines TType

#include <stdafx.h>
#include <ReportOvernightTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportOvernightTableViewer
//
static int CompareRotFlight(const ROTATIONDLGFLIGHTDATA **prpFlight1, const ROTATIONDLGFLIGHTDATA **prpFlight2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;
	CString olAriline1 = (**prpFlight1).Alc2;
	if(olAriline1.IsEmpty())
	{
		olAriline1 = (**prpFlight1).Alc3;
	}
	CString olAriline2 = (**prpFlight2).Alc2;
	if(olAriline2.IsEmpty())
	{
		olAriline2 = (**prpFlight2).Alc3;
	}
	olTime1 = (**prpFlight1).Tifa;
	if(olTime1 == TIMENULL)
		olTime1 = (**prpFlight1).Stoa;
	olTime2 = (**prpFlight2).Tifa;
	if(olTime2 == TIMENULL)
		olTime2 = (**prpFlight2).Stoa;

	if(olAriline1 == olAriline2)
	{
		ilCompareResult = (olTime1 == olTime2)? 0:(olTime1 > olTime2)? 1: -1;
	}
	else
	{
		ilCompareResult = (olAriline1 < olAriline2)?  1: -1;
	}

 return ilCompareResult;
}



int ReportOvernightTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportOvernightTableViewer::ReportOvernightTableViewer(RotationDlgCedaFlightData *popData, char *pcpInfo, char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;
	imOvernightCount = 0;
}


ReportOvernightTableViewer::~ReportOvernightTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportOvernightTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportOvernightTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


char* ReportOvernightTableViewer::GetDname()
{

	CCS_TRY
	CCS_CATCH_ALL

	return omDname.GetBuffer(0);

}


void ReportOvernightTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


/////////////////////////////////////////////////////////////////////////////
// ReportOvernightTableViewer -- code specific to this class

void ReportOvernightTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlightDep = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlightDep = NULL;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->omData.GetSize();

	pomData->omData.Sort(CompareRotFlight);
	
//	bool blDifferent = false;
	int ilOvernightCount = 0;

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		CString olFlnos ("");
		bool blDifferent = false;
		ilOvernightCount = 0;
		prlFlight = &(pomData->omData[ilLc]);
		CTime olTifa1 = prlFlight->Tifa;
		if(olTifa1 == TIMENULL)
			olTifa1 = prlFlight->Stoa;

		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{

			prlFlightDep = GetDeparture(prlFlight);
			if(prlFlightDep != NULL)
			{
				CTime olTifd1 = prlFlightDep->Tifd;
				if(olTifd1 == TIMENULL)
					olTifd1 = prlFlightDep->Stod;


				if(olTifa1.GetDay() +1 == olTifd1.GetDay())
				{
					ilOvernightCount++;
					olFlnos += CString(prlFlight->Flno);
					while(!blDifferent)
					{
						if(ilLc + 1 < ilFlightCount)
							prlNextFlight = &(pomData->omData[ilLc + 1]);
						else
							prlNextFlight = NULL;	

					//Arrival
						if(prlNextFlight != NULL)
						{
							if((strcmp(prlNextFlight->Org3, pcgHome) != 0) && (strcmp(prlNextFlight->Des3, pcgHome) == 0))
							{
								
								if((strcmp(prlNextFlight->Alc2, prlFlight->Alc2) == 0) && (strcmp(prlNextFlight->Alc3, prlFlight->Alc3) == 0))
								{
									prlNextFlightDep = GetDeparture(prlNextFlight);
									if(prlNextFlightDep != NULL)
									{
										CTime olTifa2 = prlNextFlight->Tifa;
										if(olTifa2 == TIMENULL)
											olTifa2 = prlNextFlight->Stoa;

										CTime olTifd2 = prlNextFlightDep->Tifd;
										if(olTifd2 == TIMENULL)
											olTifd2 = prlNextFlightDep->Stod;

										if(olTifa2.GetDay() +1 == olTifd2.GetDay() && olTifa1.GetDay() == olTifa2.GetDay())
										{
											olFlnos += CString(",") + CString(prlNextFlight->Flno);
											ilOvernightCount++;
											ilLc++;
										}
										else
										{
											blDifferent = true;
										}
									}
									else
									{
										ilLc++;
									}
									
								}
								else
								{
									blDifferent = true;
								}
							}
							else
							{
								ilLc++;
							}
						}
						else
						{
							blDifferent = true;
						}
					}
			
				}
				if (ilOvernightCount >0)
					ilLineNo = MakeLine(prlFlight, ilOvernightCount, olFlnos);
			}
		}
	}

	ilOvernightCount = 0;
	for(int illc = 0 ; illc < omLines.GetSize(); illc++)
	{
		ilOvernightCount += omLines[illc].OverCount;
		imOvernightCount = ilOvernightCount;
	}
	ilLineNo = MakeLine(NULL, ilOvernightCount, CString (""));

	//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_RB_OVERNIGHT), GetString(REPORTS_RB_OvernightFlightReport), pcmInfo, imOvernightCount, olTimeSet);
	if(bgReports)
	sprintf(pclHeader, "%s%s from %s (Flights: %d) - %s",ogPrefixReports, GetString(REPORTS_RB_OvernightFlightReport), pcmInfo, imOvernightCount, olTimeSet);
	omTableName = pclHeader;

}

		

int ReportOvernightTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight,int ipOvernightCount, CString &olFlnos)
{
    OVERNIGHTTABLE_LINEDATA rlLine;

	MakeLineData(prpAFlight, ipOvernightCount, rlLine, olFlnos);
	if(!FindLine(rlLine))
		return CreateLine(rlLine);

	return -1;
}




bool ReportOvernightTableViewer::FindLine(OVERNIGHTTABLE_LINEDATA &rpLine)
{

	for(int illc = 0 ; illc < omLines.GetSize(); illc++)
	{
		if((rpLine.Alc == omLines[illc].Alc)  && (rpLine.Date == omLines[illc].Date))
		{
			if(rpLine.OverCount > omLines[illc].OverCount)
			{
				omLines[illc].OverCount += rpLine.OverCount;
				omLines[illc].Flno += CString(",") + rpLine.Flno;
			}
			return true;
		}
	}

    return false;

}

void ReportOvernightTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, int ipOvernightCount, OVERNIGHTTABLE_LINEDATA &rpLine, CString &olFlnos)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;

		rpLine.Alc = CString(prpAFlight->Alc2);
		if(rpLine.Alc.IsEmpty())
		{
			rpLine.Alc = CString(prpAFlight->Alc3);
		}
		rpLine.Date =  prpAFlight->Stoa.Format(" %d.%m.%Y ");
		rpLine.OverCount =  ipOvernightCount;
		rpLine.Flno =  olFlnos;
	}
	else
	{
		rpLine.Alc = "-Total-";
		rpLine.Date =  "";
		rpLine.OverCount =  ipOvernightCount;
		rpLine.Flno =  "";
	}

		

    return;

}



int ReportOvernightTableViewer::CreateLine(OVERNIGHTTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportOvernightTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportOvernightTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportOvernightTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportOvernightTableViewer::UpdateDisplay()
{

	pomTable->ResetContent();

	DrawHeader();

	OVERNIGHTTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

    pomTable->DisplayTable();

}




void ReportOvernightTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[4];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1046);//Airlines

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING324);//Date

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 160; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1771);// Overnight flights

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 1000; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_FLIGHTNUMBERS);// Flightnumbers


	for(int ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add( prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportOvernightTableViewer::MakeColList(OVERNIGHTTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Alc;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = prlLine->Date;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text.Format( "%d",prlLine->OverCount);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = prlLine->Flno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}



int ReportOvernightTableViewer::CompareFlight(OVERNIGHTTABLE_LINEDATA *prpLine1, OVERNIGHTTABLE_LINEDATA *prpLine2)
{
	
	const CString &rolAirline1 = prpLine1->Alc;
	const CString &rolAirline2 = prpLine2->Alc;

	if(rolAirline1 == rolAirline2)
	{
		const CString &rolTime1 = prpLine1->Date;
		const CString &rolTime2 = prpLine2->Date;		
		return (rolTime1 == rolTime2)? 0:(rolTime1 > rolTime2)? 1: -1;
	}
	else
	{
		if (rolAirline1 == "-Total-") 
			return 1;
		if (rolAirline2 == "-Total-") 
			return -1;
		return (rolAirline1 == rolAirline2)? 0: (rolAirline1 > rolAirline2)?  1: -1;
	}

//	return 1;

}


ROTATIONDLGFLIGHTDATA* ReportOvernightTableViewer::GetDeparture(ROTATIONDLGFLIGHTDATA *prpFlight)
{
	ROTATIONDLGFLIGHTDATA *prlFlight;
	for(int illc = 0 ; illc < pomData->omData.GetSize(); illc++)
	{
		prlFlight = &(pomData->omData[illc]);
		if((prlFlight->Rkey == prpFlight->Rkey) && (prlFlight->Urno != prpFlight->Urno))
		{
			return prlFlight;
		}
	}
	return NULL;
}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportOvernightTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[4];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1046);//Airlines

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING324);//Date

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 160; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1771);// Overnight flights

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 700; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_FLIGHTNUMBERS);// Flightnumbers




	for(int ili = 0; ili < 4; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}




void ReportOvernightTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(REPORTS_RB_OvernightFlightReport);

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();

			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
			if(bgReports)
			olFooter1.Format("%s -   %s-   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pcgUser );	
			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportOvernightTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;

	pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportOvernightTableViewer::PrintTableLine(OVERNIGHTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
					rlElement.Text = prpLine->Alc;
			}
			break;
			case 1:
			{
					rlElement.Text = prpLine->Date;
			}
			break;
			case 2:
			{

					rlElement.Text.Format("%d",prpLine->OverCount);
			}
			break;
			case 3:
			{

					rlElement.Text = prpLine->Flno;
					rlElement.FrameRight  = PRINT_FRAMETHIN;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool ReportOvernightTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING1046) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING324) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1771)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_FLIGHTNUMBERS)
		<< endl;


	int ilCount = omLines.GetSize();

	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		OVERNIGHTTABLE_LINEDATA rlD = omLines[i];

		olDaten.Add(rlD.Alc);
		olDaten.Add(rlD.Date);
		CString olTmp;
		olTmp.Format("%d",rlD.OverCount);
		olDaten.Add(olTmp);
		olDaten.Add(rlD.Flno);

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << olDaten[0].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[1].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[2].GetBuffer(0)
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << olDaten[3].GetBuffer(0)
			 << endl;

		olDaten.RemoveAll();
	}

	of.close();
	return true;
}
