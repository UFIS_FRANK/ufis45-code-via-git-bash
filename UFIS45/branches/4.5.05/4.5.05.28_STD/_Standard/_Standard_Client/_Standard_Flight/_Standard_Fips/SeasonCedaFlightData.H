#ifndef _SeasonCedaFlightData_H_
#define _SeasonCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaCcaData.h>
#include <SeasonAskDlg.h>

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: SEASONFLIGHTDATAStruct
//@See: SeasonCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

struct RKEYLIST;


 
struct SEASONFLIGHTDATA 
{
	char	 Flti[3];
	char	 Seas[8];
	char	 Blt1[7];
	char	 Blt2[7];
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Dooa[3]; 	// Flugtag der Ankunft
	char 	 Dood[3]; 	// Flugtag des Abflugs
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime 	 Etoa; 	// ETA-Puplikumsanzeige
	CTime 	 Etod; 	// ETD-Publikumsanzeige
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Htyp[4]; 	// Abfertigungsart
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	char 	 Jcnt[3]; 	// Anzahl verbundene Flugnummern
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	long 	 Skey; 	// Eindeutiger Flugschl�ssel (Saisonschl.)
	char 	 Stev[5]; 	// Statistikkennung
	char	 Ste2[4];   // Keycode 2
	char	 Ste3[4];   // Keycode 3
	char	 Ste4[4];   // Keycode 4
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	char 	 Styp[4]; 	// Service Typ (Fluko)
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	char 	 Ttyp[7]; 	// Verkehrsart
	char 	 Vers[22]; 	// Flugplanversion
	char 	 Vian[4]; 	// Anzahl der Zwischenstationen - 1
	char	 Via3[4];
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char	 Ckif[7];
	char	 Ckit[7];
	char	 Adid[2];
	char	 Regn[13];
	char 	 Rem1[258]; 	// Bemerkung zum Flug
	char 	 Rem2[258]; 	// Bemerkung zum Flug
	char 	 Via4[5]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Ckf2[6];	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon) 	
	char 	 Ckt2[6]; 	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon)
	char	 Pcom[201];
	int      Vcnt;//Vip Count 
	char	 Mopa[2]; // For new Mode of Payment implementation
	char 	 Rreq[2];	//Resource required 0 -no 1 -yes

	CTime	 B1ba;
	CTime	 B1ea;


	SEASONFLIGHTDATA(void)
	{ memset(this,'\0',sizeof(*this));
		Etai = -1;
		Etdi = -1;
		Etoa = -1;
		Etod = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		B1ba = -1;
		B1ea = -1;
		Vcnt = 0;
	}

}; // end SEASONFLIGHTDATA
	




struct ISFQUEUEDATA
{
	long	Skey;
	CTime	MaxTifa;
	CTime	MaxTifd;
	CTime	MinTifa;
	CTime	MinTifd;
	int		AnzFlights;
	char	AFlno[10];
	char	DFlno[10];
	ISFQUEUEDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Skey = 0;
		AnzFlights = 0;
		MaxTifa = TIMENULL;
		MaxTifd = TIMENULL;
		MinTifa = TIMENULL;
		MinTifd = TIMENULL;
	};
};


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class SeasonCedaFlightData: public CCSCedaData
{
public:
    SeasonCedaFlightData();
	~SeasonCedaFlightData();

	void SetFieldList(void);
	
	void UnRegister(void);
	void Register(void);

	void SetISFModus(bool blEnable);

	void ClearAll(void);

	CString omFtyps;
	CTime omFrom;
	CTime omTo;

// Attributes
public:
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;
	CMapPtrToPtr omSkeyMap;

    CCSPtrArray<SEASONFLIGHTDATA> omData;
    CCSPtrArray<SEASONFLIGHTDATA> omResultData;
    CCSPtrArray<SEASONFLIGHTDATA> *pomData;
	CString omSelection;
	CString omOrder;

	char pcmAftFieldList[2048];

	SeasonAskDlg *pomAskDlg;

	bool bmRotation ;

	bool bmSelection;
	char pcmGFRcommand[256]; 

// Operations
public:
   

    // internal data access.
	bool AddFlightInternal(SEASONFLIGHTDATA *prpFlight);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = false);
	bool DeleteFlightsInternal(long lpRkey, bool bpDdx = true);

	bool ReadRkeys(CString &olRkeys, char *pcpSelection);

	bool ReadFlights(char *pcpSelection = NULL, bool bpIgnoreViewSel = false);
	bool ReadAllFlights(CString &opWhere, bool bpRotation, bool bpReadAdditional = false);

	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool PreSelectionCheck(CString &opData, CString &opFields, bool bpCheckOldNewVal = false, CString opSelection = "");

	bool UpdateFlight(SEASONFLIGHTDATA *prpFlight, SEASONFLIGHTDATA *prpFlightSave);
	bool UpdateFlight(CString opUrnoList, char *pcpFieldList, char *pcpDataList);
	
	bool IsFlightJoin(long lpUrno);
	bool JoinFlightPreCheck(long &llAUrno, long &llDUrno);

	bool DeleteFlight(CString opUrnoList );

	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ReloadFlights(void *vpDataPointer);
	
	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
	
	SEASONFLIGHTDATA *GetArrival(SEASONFLIGHTDATA *prpFlight);
	SEASONFLIGHTDATA *GetDeparture(SEASONFLIGHTDATA *prpFlight);

	bool AddToKeyMap(SEASONFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(SEASONFLIGHTDATA *prpFlight );
	SEASONFLIGHTDATA *GetFlightByUrno(long lpUrno);

	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);
	//bool ProcessFlightUPS(BcStruct *prlBcStruct);

	void Debug();
	void DebugKeyMap(SEASONFLIGHTDATA *prpFlight);

	void ProcessCcaChange(long *lpUrno);

	CedaDiaCcaData omCcaData;

	void ReadAllCca();



};

struct RKEYLIST
{
	CCSPtrArray<SEASONFLIGHTDATA> Rotation;

};



#endif
