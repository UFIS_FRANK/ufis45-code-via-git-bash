// RotationViaDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <utils.h>
#include <RotationViaDlg.h>
#include <BasicData.h>
#include <AwBasicDataDlg.h>
#include <CedaAptLocalUtc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationViaDlgCf(void *popInstance, int ipDDXType, 
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
RotationViaDlg::RotationViaDlg(CWnd* pParent, CString opAdid, CString opFlno, long lpUrnoFlight, CString opListVia, CTime opRefDat, bool bpLocalTime, bool bpEnable)
	: CDialog(RotationViaDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationVipDlg)
	//}}AFX_DATA_INIT

	pomParent = pParent;//GetParent()
    pomTable = new CCSTable;

	//enable flag for the table
	bmEnable = bpEnable;
	omSecState = '1';

	//fields and text for header in table
	CString olHeader = GetString(IDS_VIATABLE);
	ExtractItemList(olHeader, &omFields, ',');

	//init members with flight
	SetViaList(opAdid, opFlno, lpUrnoFlight, opListVia, opRefDat, bpLocalTime);
}


RotationViaDlg::~RotationViaDlg()
{
	ogDdx.UnRegister(this, NOTUSED);
	delete pomTable;
}

void RotationViaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationVipDlg)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);//border for table
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(RotationViaDlg, CDialog)
	//{{AFX_MSG_MAP(RotationViaDlg)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_MENU_SELECT, OnTableMenuSelect)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
BOOL RotationViaDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	CButton *polButton = (CButton *)GetDlgItem(IDOK);
	CWnd* plWnd = GetDlgItem(IDOK);
	if(ogPrivList.GetStat("VIA_CB_OK") == '0')
	{
		if (plWnd) 
			plWnd->EnableWindow(false);
	}

	if(ogPrivList.GetStat("VIA_CB_OK") == '-')
	{
		if (plWnd) 
			plWnd->ShowWindow(SW_HIDE);
	}
	
	InitTable();
	ShowTable();

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDOK,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDCANCEL,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);


	if (bgRotationDialogsTopmost) // quick fix for UFIS 1540
		SetWindowPos(&wndTopMost , 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE);
	
	return TRUE;
}

void RotationViaDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomTable->DisplayTable();
	}

	this->Invalidate();
}


void RotationViaDlg::InitTable()
{
	//size of table
	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetTableEditable(true);
	pomTable->SetIPEditModus(true);
	pomTable->SetSelectMode(false);
	pomTable->SetShowSelection(false);
//	pomTable->ResetContent();

	DrawHeader();
	pomTable->DisplayTable();
}

void RotationViaDlg::DrawHeader()
{
	TABLE_HEADER_COLUMN* prlHeader[11];
	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;

	for (int i= 0; i < omFields.GetSize(); i++)
	{
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Length = 50; 
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Text = omFields.GetAt(i);

		olHeaderDataArray.Add(prlHeader[i]);
	}

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();

	olHeaderDataArray.DeleteAll();
}

void RotationViaDlg::ShowTable()
{
	CCSEDIT_ATTRIB rlAttrib;
	TABLE_COLUMN   rlColumnData;
	CCSPtrArray<TABLE_COLUMN> olColList;
	CCSPtrArray<VIADATA>      olVias;

	//create VIADATA from AFT.VIAL
	ogBasicData.GetViaArray(&olVias, omViaList);

	//fill table up to 8 lines (not more possible because AFT.VIAL = 1024
	for (int i = olVias.GetSize(); i < 8; i++)
	{
		VIADATA *prlVia = new VIADATA;
		olVias.Add(prlVia);
	}

	//clear table
	pomTable->ResetContent();

	//column attributes
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	//disable edit in table if false
	if (bmEnable)
	{
		rlColumnData.blIsEditable = true;
		rlColumnData.BkColor = RGB(255,255,255);
	}
	else
	{
		rlColumnData.blIsEditable = false;
		rlColumnData.BkColor = RGB(192,192,192);
	}


	//create lines for table
	int ilCountCol = omFields.GetSize();
	int ilCountLine = olVias.GetSize();
	for (int  ilLc = 0; ilLc < ilCountLine ; ilLc++)
	{
		for (int  ilCol = 0; ilCol < ilCountCol ; ilCol++)
		{
			rlColumnData.Text = GetField(&olVias[ilLc], omFields[ilCol]);
			GetAttrib(rlAttrib, omFields[ilCol]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);
		}

		pomTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}

	//menuitem on first column(VIA) -> "List of Airports"
	pomTable->AddMenuItem( -1, 0, 0, 101, GetString(IDS_STRING1711));
	pomTable->AddMenuItem( -1, 1, 0, 102, GetString(IDS_STRING1711));

	//show table
	olVias.DeleteAll();
	pomTable->DisplayTable();

	//set windowtext for current flight
	if (omAdid == "A")
		omCaption.Format(GetString(IDS_VIA_ARR), omFlno);
	else
		omCaption.Format(GetString(IDS_VIA_DEP), omFlno);

	SetWindowText(omCaption);
}

void RotationViaDlg::SetRefDate(CTime opRefDat)
{
		omRefDat = opRefDat;
}

void RotationViaDlg::SetViaList(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime)
{
	bmLocal = bpLocalTime;
	omViaList = opList;

	omRefDat = opRefDat;
	if(omRefDat == TIMENULL)
		omRefDat = CTime::GetCurrentTime();

	omAdid = opAdid;
	omFlno = opFlno;
	omUrnoFlight = lpUrnoFlight;
}

void RotationViaDlg::SetData(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime)
{
	SetViaList(opAdid, opFlno, lpUrnoFlight, opList, opRefDat, bpLocalTime);
	ShowTable();
}

CString RotationViaDlg::GetToolTip()
{
	CString olToolTip;
	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		CString olTmp;
		CString olApc;
		CString olTime;
		for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, j, olTmp);
			if (omFields[j] == "VIA3")
			{
				if (olTmp.IsEmpty())
					continue;
				else
				{
					CString olApc4;
					pomTable->GetTextFieldValue(i, j+1, olApc4);
						 
					olApc = olTmp + "/" + olApc4 + " ";
				}
			}

			if (omFields[j] == "VIA4")
			{
				if (olTmp.IsEmpty())
					continue;
				else
				{
					CString olApc3;
					pomTable->GetTextFieldValue(i, j-1, olApc3);
						 
					olApc = olApc3 + "/" + olTmp + " ";
				}
			}

			if (omAdid == "A")
			{
				if (omFields[j] == "STA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ONBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "STD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "OFBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
			}
			if (omAdid == "D")
			{
				if (omFields[j] == "STD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "OFBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "STA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ONBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
			}
		}

		if (!olApc.IsEmpty())
				olToolTip += " -> " + olApc + " " + olTime;
	}

	return olToolTip;
}

CString RotationViaDlg::GetField(VIADATA *prpVia, CString opField)
{
	CString olRet;
	CTime olTime;

	if(prpVia == NULL)
		return olRet;

	if(opField == "VIA3")
		olRet = CString(prpVia->Apc3);
	else 
	if(opField == "VIA4")
	{
		olRet = CString(prpVia->Apc4);
//		if (olRet.IsEmpty())
//			olRet = CString(prpVia->Apc3);
	}
	else 
	if(opField == "STA")
	{
		olTime = prpVia->Stoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "STD")
	{
		olTime = prpVia->Stod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETA")
	{
		olTime = prpVia->Etoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ONBL")
	{
		olTime = prpVia->Onbl;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATA")
	{
		olTime = prpVia->Land;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETD")
	{
		olTime = prpVia->Etod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "OFBL")
	{
		olTime = prpVia->Ofbl;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATD")
	{
		olTime = prpVia->Airb;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "TR")
		olRet = CString(prpVia->Fids);

	return olRet;
}

void RotationViaDlg::GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField)
{
	if(opField == "VIA4")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 4;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "VIA3")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 3;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "TR")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "X";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 1;
	}
	else 
	{
		ropAttrib.Type = KT_TIME;
		ropAttrib.ChangeDay	= true;
		ropAttrib.TextMaxLenght = 9;
	}
}

LONG RotationViaDlg::OnTableMenuSelect( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	if(!bmEnable)
		return 0L;

	if(wParam == 101)
	{
/*		CCSPtrArray<VIADATA> olVias;
		ogBasicData.GetViaArray(&olVias, omViaList);
		VIADATA *prlVia = NULL;

		//set preselection for apttab
		CString olSelect;
		if (prlNotify->Line < olVias.GetSize())
		{
			prlVia = &olVias[prlNotify->Line];
			if (prlVia)
				olSelect = GetField(prlVia, "VIA3");
		}
*/
		CString olSelect;
		pomTable->GetTextFieldValue(prlNotify->Line, prlNotify->Column, olSelect);

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olSelect);
		if(polDlg->DoModal() == IDOK)
		{
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, polDlg->GetField("APC3"));
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column+1, polDlg->GetField("APC4"));
			pomTable->DisplayTable();
		}
		delete polDlg;
	}

	//list apttab
	if(wParam == 102)
	{
/*		CCSPtrArray<VIADATA> olVias;
		ogBasicData.GetViaArray(&olVias, omViaList);
		VIADATA *prlVia = NULL;

		//set preselection for apttab
		CString olSelect;
		if (prlNotify->Line < olVias.GetSize())
		{
			prlVia = &olVias[prlNotify->Line];
			if (prlVia)
				olSelect = GetField(prlVia, "VIA4");
		}
*/
		CString olSelect;
		pomTable->GetTextFieldValue(prlNotify->Line, prlNotify->Column, olSelect);

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC4,APC3,APFN", "APFN+", olSelect);
		if(polDlg->DoModal() == IDOK)
		{
			CString olApc4 = polDlg->GetField("APC4");
			CString olApc3 = polDlg->GetField("APC3");
//			if (olApc.IsEmpty())
//				olApc = polDlg->GetField("APC3");

			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, olApc4);
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column-1, olApc3);
			//refresh table, otherwise the new entry will only shown on kill focus in column VIA
			pomTable->DisplayTable();
		}
		delete polDlg;
	}

	return TRUE;
}

LONG RotationViaDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;

	if (omFields[prlNotify->Column] == "TR")
		return 0L;

	CString olTmp; 
	CString olTmp2;

//	if(omFields[prlNotify->Line] == "VIA" && prlNotify->Text.GetLength() == 4)
	if(omFields[prlNotify->Column] == "VIA4")// && prlNotify->Text.GetLength() == 4)
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp2;
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
				prlNotify->Text = olTmp;
		}
		pomTable->SetTextFieldValue(prlNotify->Line, prlNotify->Column-1, olTmp);
	}	
	if(omFields[prlNotify->Column] == "VIA3")// && prlNotify->Text.GetLength() == 3)
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp;
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
				prlNotify->Text = olTmp2;
		}
		pomTable->SetTextFieldValue(prlNotify->Line, prlNotify->Column+1, olTmp2);
	}	

	if(omFields[prlNotify->Column] != "VIA3" && omFields[prlNotify->Column] != "VIA4")
	{
		prlNotify->UserStatus = true;
		CTime olTime = HourStringToDate(prlNotify->Text, TIMENULL); 
		if (olTime == TIMENULL)
			prlNotify->Status = false;
	}

	if (!prlNotify->Status)
	{
		if(omFields[prlNotify->Column] == "VIA3")
		{
			MessageBox(GetString(IDS_INV_APC), GetString(ST_FEHLER));
			return 0L;
		}

		if(omFields[prlNotify->Column] == "VIA4")
		{
			MessageBox(GetString(IDS_INV_APC), GetString(ST_FEHLER));
			return 0L;
		}

		MessageBox(GetString(IDS_INV_TIME), GetString(ST_FEHLER));
	}

	return 0L;
}

LONG RotationViaDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	//send parent that someting was changed
	return pomParent->SendMessage(WM_EDIT_CHANGED, wParam, lParam);	
}

void RotationViaDlg::OnCancel() 
{
	//init table with old data
	ShowTable();
	ShowWindow(SW_HIDE);
}

void RotationViaDlg::OnOK() 
{
	//set new memberdata
	CString opAMess = GetStatus();
	char pcVial[1026];
	GetViaList(pcVial);
	omViaList = CString(pcVial);
	ShowTable();
	ShowWindow(SW_HIDE);

	pomParent->SendMessage(WM_VIA, 0, 0);	
	
}

LONG RotationViaDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
//	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
//	pomTable->SelectLine(prlNotify->Line);
	return -1L;
}

LONG RotationViaDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	return 0L;
}

static void RotationViaDlgCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationViaDlg *polDlg = (RotationViaDlg *)popInstance;
}

void RotationViaDlg::SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat)
{
	CString olApc4;
	CString olApc3;
	CTime olTime;

	if (opRefDat == TIMENULL)
		opRefDat = omRefDat;

	if(prpVia == NULL)
		return; 

	if((opField == "VIA4"))
	{
		strcpy(prpVia->Apc4, opValue);	

/*		if(ogBCD.GetField("APT", "APC4", "APC3", opValue, olApc4, olApc3 ))
		{
			strcpy(prpVia->Apc3, olApc3);	
			strcpy(prpVia->Apc4, olApc4);	
		}
		else
		{
			strcpy(prpVia->Apc3, "");	
			strcpy(prpVia->Apc4, "");	
		}
*/
	}	
	else
	if((opField == "VIA3"))
	{
		strcpy(prpVia->Apc3, opValue);	
/*
		if(ogBCD.GetField("APT", "APC3", "APC4", opValue, olApc3, olApc4 ))
		{
			strcpy(prpVia->Apc3, olApc3);	
			strcpy(prpVia->Apc4, olApc4);	
		}
		else
		{
			strcpy(prpVia->Apc3, "");	
			strcpy(prpVia->Apc4, "");	
		}
*/
	}	
	else 
	if(opField == "STA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stoa = olTime;
	}
	else 
	if(opField == "STD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stod = olTime;
	}
	else 
	if(opField == "ETA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etoa = olTime;
	}
	else 
	if(opField == "ONBL")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Onbl = olTime;
	}
	else 
	if(opField == "ATA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Land = olTime;
	}
	else 
	if(opField == "ETD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etod = olTime;
	}
	else 
	if(opField == "OFBL")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Ofbl = olTime;
	}
	else 
	if(opField == "ATD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Airb = olTime;
	}
	else 
	if(opField == "TR")
		strcpy(prpVia->Fids, opValue);	
}

void RotationViaDlg::GetViaList(char *pcpViaList)
{
	CCSPtrArray<VIADATA> olNewVias;
	VIADATA *prlVia;

	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		prlVia = new VIADATA;
		olNewVias.Add(prlVia);

		CString olTmp;
		for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, j, olTmp);
			SetField(prlVia , omFields[j], olTmp);
		}
	}

	CString olViaList = CreateViaList(&olNewVias);
	strncpy(pcpViaList, olViaList, 1024);

	olNewVias.DeleteAll();
}

CString RotationViaDlg::CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat)
{
	CString olVia;
	CString olFids;
	CString olApc3;
	CString olApc4;
	CString	olTmp;
	CString	olTmp2;

	VIADATA *prlVia;

	for(int i = 0; i < popVias->GetSize(); i++)
	{
		prlVia = &(*popVias)[i];


		olApc3 = CString(prlVia->Apc3);
		olApc4 = CString(prlVia->Apc4);
		olFids = CString(prlVia->Fids);


		if(olApc3.GetLength() != 3)
			olApc3 = "   ";

		if(olApc4.GetLength() != 4)
			olApc4 = "    ";

		if(olFids.GetLength() != 1)
			olFids = " ";


		olTmp  =	olFids  +
					olApc3  +
					olApc4  +
					CTimeToDBString(prlVia->Stoa, TIMENULL) +  
					CTimeToDBString(prlVia->Etoa, TIMENULL) +  
					CTimeToDBString(prlVia->Land, TIMENULL) +  
					CTimeToDBString(prlVia->Onbl, TIMENULL) +  
					CTimeToDBString(prlVia->Stod, TIMENULL) + 
					CTimeToDBString(prlVia->Etod, TIMENULL) +  
					CTimeToDBString(prlVia->Ofbl, TIMENULL) +  
					CTimeToDBString(prlVia->Airb, TIMENULL); 
//					CTimeToDBString(prlVia->Airb, TIMENULL) +  
//					CTimeToDBString(prlVia->Ofbl, TIMENULL); 

		olTmp2 = olTmp;
		olTmp2.TrimRight();
		if(!olTmp2.IsEmpty())
			olVia += olTmp;

	}

	return olVia;
}

CString  RotationViaDlg::GetStatus()
{
	CString olRet;
	CString olTmp;

	int ilLines = pomTable->GetLinesCount();
	bool blRet;
	int ilColumn = 0;
	for(int  i = 0; i < ilLines; i++)
	{
		blRet = true;
		ilColumn = 0;

		while(blRet)
		{
			if((blRet = pomTable->GetTextFieldValue(i, ilColumn, olTmp)))
			{
				if(!pomTable->GetCellStatus(i, ilColumn))
				{
					olRet = GetString(IDS_STRING299); // "VIA"
					return olRet;
				}
			}
			ilColumn++;
		}
	}

	return olRet;
}

void RotationViaDlg::Enable( bool bpEnable )
{
	if (omSecState != '1' && bpEnable == true)
		return;

	if (bmEnable == bpEnable)
		return;

	bmEnable = bpEnable;
	if (pomTable) 
	{
		pomTable->SetTableEditable(bmEnable);
		pomTable->SetIPEditModus(bmEnable);
		ShowTable();
	}
}

void RotationViaDlg::SetSecState(char opSecState)
{
	omSecState = opSecState;
	if (opSecState != '1')
	{
		bmEnable = true;
		Enable(false); 
	}
	else
	{
		bmEnable = false;
		Enable(true); 
	}
}
