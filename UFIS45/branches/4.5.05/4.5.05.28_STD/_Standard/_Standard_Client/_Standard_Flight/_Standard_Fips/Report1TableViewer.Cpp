// GefundenefluegeTableViewer.cpp : implementation file
// 
//	Liste der Gefundenen Flugbewegungen

#include <stdafx.h>
#include <Report1TableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// GefundenefluegeTableViewer
//

int GefundenefluegeTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



GefundenefluegeTableViewer::GefundenefluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;
}

GefundenefluegeTableViewer::~GefundenefluegeTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}

void GefundenefluegeTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void GefundenefluegeTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// GefundenefluegeTableViewer -- code specific to this class

void GefundenefluegeTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}

//generate the headerinformation
	//calculate the numbers of ARR and DEP
	imArr = 0;
	imDep = 0;
	for(int j = 0; j < omLines.GetSize(); j++ ) 
	{
		GEFUNDENEFLUEGETABLE_LINEDATA rlLine = omLines[j];
		if (!rlLine.AFlno.IsEmpty())
			imArr++;
		if (!rlLine.DFlno.IsEmpty())
			imDep++;
	}

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER1), GetString(IDS_REPORTS_RB_FlightSameAirline), pcmSelect, pcmInfo, imArr+imDep, imArr, imDep, olTimeSet);
	if(bgReports)
	sprintf(pclHeader, "%s%s with %s from %s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, GetString(IDS_REPORTS_RB_FlightSameAirline), pcmSelect, pcmInfo, imArr+imDep, imArr, imDep, olTimeSet);
	omTableName = pclHeader;

}

		




int GefundenefluegeTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    GEFUNDENEFLUEGETABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void GefundenefluegeTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, GEFUNDENEFLUEGETABLE_LINEDATA &rpLine)
{
	CString olStr;

	rpLine.ArrCanceled = false;
	rpLine.DepCanceled = false;
	if(prpAFlight != NULL)
	{
		if (!CString(prpAFlight->Flno).IsEmpty())
			rpLine.Alc3 =  CString(prpAFlight->Flno).Left(3);

		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.ASeas = prpAFlight->Seas; 
		if (prpAFlight->Ftyp[0] == 'X')
			rpLine.ArrCanceled = true;
/*rkr
		rpLine.AOnbl = prpAFlight->Onbl; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AOnbl);

		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);

		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
*/

		if(bgReportLocal)
		{
			ogBasicData.UtcToLocal(prpAFlight->Onbl);
			ogBasicData.UtcToLocal(prpAFlight->Stoa);
			ogBasicData.UtcToLocal(prpAFlight->Land);
		}

		rpLine.AOnbl = prpAFlight->Onbl; 
		rpLine.AStoa = prpAFlight->Stoa; 
		rpLine.ALand = DateToHourDivString(prpAFlight->Land, prpAFlight->Stoa);
		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);

		if(bgReportLocal)
		{
			ogBasicData.LocalToUtc(prpAFlight->Onbl);
			ogBasicData.LocalToUtc(prpAFlight->Stoa);
			ogBasicData.LocalToUtc(prpAFlight->Land);
		}


		rpLine.ATtyp = prpAFlight->Ttyp; 
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		//rpLine.AVia3 = CString(prpAFlight->Via3);
		rpLine.AAct = CString(prpAFlight->Act3);

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();


	}
	else
	{
		rpLine.Alc3 =  "";
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.ASeas = "";
		rpLine.AOnbl = TIMENULL; 
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = ""; 
		rpLine.ADate = ""; 
		rpLine.ATtyp = ""; 
		rpLine.AOrg3 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
	}


	if(prpDFlight != NULL)
	{
		if (!CString(prpDFlight->Flno).IsEmpty() && rpLine.Alc3.IsEmpty())
			rpLine.Alc3 =  CString(prpDFlight->Flno).Left(3);

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DSeas = prpDFlight->Seas; 
		if (prpDFlight->Ftyp[0] == 'X')
			rpLine.DepCanceled = true;

/*rkr
		rpLine.DOfbl = prpDFlight->Ofbl; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DOfbl);
		rpLine.DStod = prpDFlight->Stod; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = DateToHourDivString(prpDFlight->Ofbl, prpDFlight->Stod);
*/

		if(bgReportLocal)
		{
			ogBasicData.UtcToLocal(prpDFlight->Ofbl);
			ogBasicData.UtcToLocal(prpDFlight->Stod);
			ogBasicData.UtcToLocal(prpDFlight->Airb);
		}

		rpLine.DOfbl = prpDFlight->Ofbl; 
		rpLine.DStod = prpDFlight->Stod; 
		rpLine.DAirb = DateToHourDivString(prpDFlight->Airb, prpDFlight->Stod);
		rpLine.DDate = DateToHourDivString(prpDFlight->Ofbl, prpDFlight->Stod);

		if(bgReportLocal)
		{
			ogBasicData.LocalToUtc(prpDFlight->Ofbl);
			ogBasicData.LocalToUtc(prpDFlight->Stod);
			ogBasicData.LocalToUtc(prpDFlight->Airb);
		}



		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		//rpLine.DVia3 = CString(prpDFlight->Via3);
		rpLine.DAct = CString(prpDFlight->Act3);

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;
		if (rpLine.ASeas.IsEmpty())
			rpLine.ASeas = rpLine.DSeas;

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
		{
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		}
		opVias.DeleteAll();

	
	}
	else
	{
		//rpLine.Alc3 =  "";
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DFlno = "";
		rpLine.DSeas = "";
		rpLine.DOfbl = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DAirb = ""; 
		rpLine.DDate = ""; 
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
	}
    return;
}



int GefundenefluegeTableViewer::CreateLine(GEFUNDENEFLUEGETABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void GefundenefluegeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void GefundenefluegeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// GefundenefluegeTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void GefundenefluegeTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	GEFUNDENEFLUEGETABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void GefundenefluegeTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[18];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
//	prlHeader[1]->Text = GetString(IDS_STRING1079);
	prlHeader[1]->Text = GetString(IDS_STRING332);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 50; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
//	prlHeader[2]->Text = GetString(IDS_STRING1080);
	prlHeader[2]->Text = GetString(IDS_R3_STA);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 90; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING2170);

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 25; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING301);

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 35; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
//	prlHeader[5]->Text = GetString(IDS_STRING1073);
	prlHeader[5]->Text = GetString(IDS_STRING298);
	
	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 35; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING299);

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 55; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
//	prlHeader[7]->Text = GetString(IDS_STRING1074);
	prlHeader[7]->Text = GetString(IDS_STRING337);

	//Abflug
	
	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 67; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	prlHeader[8]->Text = GetString(IDS_STRING1078);

	prlHeader[9] = new TABLE_HEADER_COLUMN;
	prlHeader[9]->Alignment = COLALIGN_CENTER;
	prlHeader[9]->Length = 60; 
	prlHeader[9]->Font = &ogCourier_Bold_10;
//	prlHeader[9]->Text = GetString(IDS_STRING1079);
	prlHeader[9]->Text = GetString(IDS_STRING332);

	prlHeader[10] = new TABLE_HEADER_COLUMN;
	prlHeader[10]->Alignment = COLALIGN_CENTER;
	prlHeader[10]->Length = 50; 
	prlHeader[10]->Font = &ogCourier_Bold_10;
//	prlHeader[10]->Text = GetString(IDS_STRING1080);
	prlHeader[10]->Text = GetString(IDS_R3_STD);

	prlHeader[11] = new TABLE_HEADER_COLUMN;
	prlHeader[11]->Alignment = COLALIGN_CENTER;
	prlHeader[11]->Length = 90; 
	prlHeader[11]->Font = &ogCourier_Bold_10;
	prlHeader[11]->Text = GetString(IDS_STRING2171);

	prlHeader[12] = new TABLE_HEADER_COLUMN;
	prlHeader[12]->Alignment = COLALIGN_CENTER;
	prlHeader[12]->Length = 25; 
	prlHeader[12]->Font = &ogCourier_Bold_10;
	prlHeader[12]->Text = GetString(IDS_STRING301);

	prlHeader[13] = new TABLE_HEADER_COLUMN;
	prlHeader[13]->Alignment = COLALIGN_CENTER;
	prlHeader[13]->Length = 35; 
	prlHeader[13]->Font = &ogCourier_Bold_10;
//	prlHeader[13]->Text = GetString(IDS_STRING1082);
	prlHeader[13]->Text = GetString(IDS_STRING315);

	prlHeader[14] = new TABLE_HEADER_COLUMN;
	prlHeader[14]->Alignment = COLALIGN_CENTER;
	prlHeader[14]->Length = 35; 
	prlHeader[14]->Font = &ogCourier_Bold_10;
	prlHeader[14]->Text = GetString(IDS_STRING299);
	
	prlHeader[15] = new TABLE_HEADER_COLUMN;
	prlHeader[15]->Alignment = COLALIGN_CENTER;
	prlHeader[15]->Length = 40; 
	prlHeader[15]->Font = &ogCourier_Bold_10;
	prlHeader[15]->Text = GetString(IDS_STRING1007);

	prlHeader[16] = new TABLE_HEADER_COLUMN;
	prlHeader[16]->Alignment = COLALIGN_CENTER;
	prlHeader[16]->Length = 25; 
	prlHeader[16]->Font = &ogCourier_Bold_10;
	prlHeader[16]->Text = GetString(IMFK_CXX);


	for(int ili = 0; ili < 17; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}




void GefundenefluegeTableViewer::MakeColList(GEFUNDENEFLUEGETABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
		
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%d.%m.%y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
//		rlColumnData.Text = prlLine->ADate;
		rlColumnData.Text = "";
		if (!prlLine->ADate.IsEmpty() && !prlLine->ALand.IsEmpty())
			rlColumnData.Text = prlLine->ALand + "/" + prlLine->ADate;
		if (!prlLine->ADate.IsEmpty() && prlLine->ALand.IsEmpty())
			rlColumnData.Text =  "     /" + prlLine->ALand;
		if (prlLine->ADate.IsEmpty() && !prlLine->ALand.IsEmpty())
			rlColumnData.Text = prlLine->ALand;

		//rlColumnData.Text = prlLine->AOnbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ATtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AOrg3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AVia3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		/*if(prlLine->AAct5.GetLength())
			rlColumnData.Text = prlLine->AAct5;
		else
			rlColumnData.Text = prlLine->AAct3;*/
		rlColumnData.Text = prlLine->AAct;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//Abflug
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%d.%m.%y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
//		rlColumnData.Text = prlLine->DDate;
		rlColumnData.Text = "";
		if (!prlLine->DDate.IsEmpty() && !prlLine->DAirb.IsEmpty())
			rlColumnData.Text = prlLine->DDate + "/" + prlLine->DAirb;
		if (!prlLine->DDate.IsEmpty() && prlLine->DAirb.IsEmpty())
			rlColumnData.Text = prlLine->DDate;
		if (prlLine->DDate.IsEmpty() && !prlLine->DAirb.IsEmpty())
			rlColumnData.Text = "     /" + prlLine->DAirb;


		//rlColumnData.Text = prlLine->DOfbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DTtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DDes3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DVia3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ASeas;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = " ";

		if (prlLine->ArrCanceled)
			rlColumnData.Text = "X-";
		if (prlLine->DepCanceled)
			rlColumnData.Text = " -X";
		if (prlLine->ArrCanceled && prlLine->DepCanceled)
			rlColumnData.Text = "X-X";
/*
		if (prlLine->Canceled)
			rlColumnData.Text = "X";
		else
			rlColumnData.Text = " ";
*/
		olColList.NewAt(olColList.GetSize(), rlColumnData);

}




int GefundenefluegeTableViewer::CompareFlight(GEFUNDENEFLUEGETABLE_LINEDATA *prpLine1, GEFUNDENEFLUEGETABLE_LINEDATA *prpLine2)
{
	CString olAlNam1 = prpLine1->Alc3;
	CString olAlNam2 = prpLine2->Alc3;

	if(strcmp(olAlNam1,olAlNam2) == 0)
	{
		int	ilCompareResult;
		CTime olTime1;
		CTime olTime2;

		if(prpLine1->AStoa == TIMENULL)
			olTime1 = prpLine1->DStod;
		else
			olTime1 = prpLine1->AStoa;

		if(prpLine2->AStoa == TIMENULL)
			olTime2 = prpLine2->DStod;
		else
			olTime2 = prpLine2->AStoa;

		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
	}
	else
	{
		if(strcmp(olAlNam1,olAlNam2) > 0)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}


/*
		int	ilCompareResult;
		CTime olTime1;
		CTime olTime2;

		if(prpLine1->AStoa == TIMENULL)
			olTime1 = prpLine1->DStod;
		else
			olTime1 = prpLine1->AStoa;

		if(prpLine2->AStoa == TIMENULL)
			olTime2 = prpLine2->DStod;
		else
			olTime2 = prpLine2->AStoa;

		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
*/
}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void GefundenefluegeTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi
	
	TABLE_HEADER_COLUMN *prlHeader[18];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
//	prlHeader[1]->Text = GetString(IDS_STRING1079);
	prlHeader[1]->Text = GetString(IDS_STRING332);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 70; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
//	prlHeader[2]->Text = GetString(IDS_STRING1080);
	prlHeader[2]->Text = GetString(IDS_R3_STA);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 90; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING2170);

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 25; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING301);

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 35; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
//	prlHeader[5]->Text = GetString(IDS_STRING1073);
	prlHeader[5]->Text = GetString(IDS_STRING298);
	
	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 35; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING299);

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 55; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
//	prlHeader[7]->Text = GetString(IDS_STRING1074);
	prlHeader[7]->Text = GetString(IDS_STRING337);

	//Abflug
	
	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 67; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	prlHeader[8]->Text = GetString(IDS_STRING1078);

	prlHeader[9] = new TABLE_HEADER_COLUMN;
	prlHeader[9]->Alignment = COLALIGN_CENTER;
	prlHeader[9]->Length = 80; 
	prlHeader[9]->Font = &ogCourier_Bold_10;
//	prlHeader[9]->Text = GetString(IDS_STRING1079);
	prlHeader[9]->Text = GetString(IDS_STRING332);

	prlHeader[10] = new TABLE_HEADER_COLUMN;
	prlHeader[10]->Alignment = COLALIGN_CENTER;
	prlHeader[10]->Length = 70; 
	prlHeader[10]->Font = &ogCourier_Bold_10;
//	prlHeader[10]->Text = GetString(IDS_STRING1080);
	prlHeader[10]->Text = GetString(IDS_R3_STD);

	prlHeader[11] = new TABLE_HEADER_COLUMN;
	prlHeader[11]->Alignment = COLALIGN_CENTER;
	prlHeader[11]->Length = 90; 
	prlHeader[11]->Font = &ogCourier_Bold_10;
	prlHeader[11]->Text = GetString(IDS_STRING2171);

	prlHeader[12] = new TABLE_HEADER_COLUMN;
	prlHeader[12]->Alignment = COLALIGN_CENTER;
	prlHeader[12]->Length = 25; 
	prlHeader[12]->Font = &ogCourier_Bold_10;
	prlHeader[12]->Text = GetString(IDS_STRING301);

	prlHeader[13] = new TABLE_HEADER_COLUMN;
	prlHeader[13]->Alignment = COLALIGN_CENTER;
	prlHeader[13]->Length = 35; 
	prlHeader[13]->Font = &ogCourier_Bold_10;
//	prlHeader[13]->Text = GetString(IDS_STRING1082);
	prlHeader[13]->Text = GetString(IDS_STRING315);

	prlHeader[14] = new TABLE_HEADER_COLUMN;
	prlHeader[14]->Alignment = COLALIGN_CENTER;
	prlHeader[14]->Length = 35; 
	prlHeader[14]->Font = &ogCourier_Bold_10;
	prlHeader[14]->Text = GetString(IDS_STRING299);
	
	prlHeader[15] = new TABLE_HEADER_COLUMN;
	prlHeader[15]->Alignment = COLALIGN_CENTER;
	prlHeader[15]->Length = 40; 
	prlHeader[15]->Font = &ogCourier_Bold_10;
	prlHeader[15]->Text = GetString(IDS_STRING1007);

	prlHeader[16] = new TABLE_HEADER_COLUMN;
	prlHeader[16]->Alignment = COLALIGN_CENTER;
	prlHeader[16]->Length = 25; 
	prlHeader[16]->Font = &ogCourier_Bold_10;
	prlHeader[16]->Text = GetString(IMFK_CXX);

	for(int ili = 0; ili < 17; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}




void GefundenefluegeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_REPORTS_RB_FlightSameAirline);

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

			if(bgReports)
			olFooter1.Format("%s -   %s-   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pcgUser );
			
			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		//omBitmap.DeleteObject();
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool GefundenefluegeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;

	GetHeader();

	pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			//rlElement.Text   = omHeaderDataArray[i].Text;
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool GefundenefluegeTableViewer::PrintTableLine(GEFUNDENEFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
			case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->AFlno;
			}
			break;
			case 1:
			{
				rlElement.Text		= prpLine->AStoa.Format("%d.%m.%y");
			}
			break;
			case 2:
			{
				rlElement.Text		= prpLine->AStoa.Format("%H:%M");
			}
			break;
			case 3:
			{
//				rlElement.Text		= prpLine->ADate;
				//rlElement.Text		= prpLine->AOnbl.Format("%H:%M");
				rlElement.Text = "";
				if (!prpLine->ADate.IsEmpty() && !prpLine->ALand.IsEmpty())
					rlElement.Text = prpLine->ALand + "/" + prpLine->ADate;
				if (!prpLine->ADate.IsEmpty() && prpLine->ALand.IsEmpty())
					rlElement.Text = "     /" + prpLine->ADate;
				if (prpLine->ADate.IsEmpty() && !prpLine->ALand.IsEmpty())
					rlElement.Text = prpLine->ALand;
			}
			break;
			case 4:
			{
				rlElement.Text		= prpLine->ATtyp;
			}
			break;
			case 5:
			{
				rlElement.Text		= prpLine->AOrg3;
			}
			break;
			case 6:
			{
				rlElement.Text		= prpLine->AVia3;
			}
			break;
			case 7:
			{
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				/*if(prpLine->AAct5.GetLength())
					rlElement.Text	= prpLine->AAct5;
				else
					rlElement.Text	 = prpLine->AAct3;*/
				rlElement.Text	 = prpLine->AAct;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
			case 8:
			{
				rlElement.Text		= prpLine->DFlno;
			}
			break;
			case 9:
			{
				rlElement.Text		= prpLine->DStod.Format("%d.%m.%y");
			}
			break;
			case 10:
			{
				rlElement.Text		= prpLine->DStod.Format("%H:%M");
			}
			break;
			case 11:
			{
//				rlElement.Text		= prpLine->DDate;
				rlElement.Text = "";
				if (!prpLine->DDate.IsEmpty() && !prpLine->DAirb.IsEmpty())
					rlElement.Text = prpLine->DDate + "/" + prpLine->DAirb;
				if (!prpLine->DDate.IsEmpty() && prpLine->DAirb.IsEmpty())
					rlElement.Text = prpLine->DDate;
				if (prpLine->DDate.IsEmpty() && !prpLine->DAirb.IsEmpty())
					rlElement.Text = "     /" + prpLine->DAirb;
				//rlElement.Text		= prpLine->DOfbl.Format("%H:%M");
			}
			break;
			case 12:
			{
				rlElement.Text		= prpLine->DTtyp;
			}
			break;
			case 13:
			{
				rlElement.Text		= prpLine->DDes3;
			}
			break;
			case 14:
			{
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->DVia3;
			}
			break;
			case 15:
			{
				rlElement.Text		= prpLine->ASeas;
			}
			break;
			case 16:
			{
/*				rlElement.FrameRight = PRINT_FRAMETHIN;
				if (prpLine->Canceled)
					rlElement.Text = "X";
				else
					rlElement.Text = " ";
*/

				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = " ";
				if (prpLine->ArrCanceled)
					rlElement.Text = "X-";
				if (prpLine->DepCanceled)
					rlElement.Text = " -X";
				if (prpLine->ArrCanceled && prpLine->DepCanceled)
					rlElement.Text = "X-X";

			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool GefundenefluegeTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING332) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_R3_STA)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2170)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING301)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING298) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING299)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING337)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1078)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING332)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_R3_STD)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2171)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING301)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING315)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING299)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1007)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IMFK_CXX)
		<< endl;


	int ilCount = omLines.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		GEFUNDENEFLUEGETABLE_LINEDATA rlLine = omLines[i];

		CString AOnbl = "";
		if (!rlLine.ADate.IsEmpty() && !rlLine.ALand.IsEmpty())
			AOnbl = rlLine.ALand + "-" + rlLine.ADate;
		if (!rlLine.ADate.IsEmpty() && rlLine.ALand.IsEmpty())
			AOnbl = "XXXX-" + rlLine.ADate;
		if (rlLine.ADate.IsEmpty() && !rlLine.ALand.IsEmpty())
			AOnbl = rlLine.ALand + "-XXXX";

		CString DOnbl = "";
		if (!rlLine.DDate.IsEmpty() && !rlLine.DAirb.IsEmpty())
			DOnbl = rlLine.DDate + "-" + rlLine.DAirb;
		if (!rlLine.DDate.IsEmpty() && rlLine.DAirb.IsEmpty())
			DOnbl = rlLine.DDate + "-XXXX";
		if (rlLine.DDate.IsEmpty() && !rlLine.DAirb.IsEmpty())
			DOnbl = "XXXX-" + rlLine.DAirb;


		CString olCxx;
		if (rlLine.ArrCanceled && rlLine.DepCanceled)
			olCxx = "X-X";
		else
		{
			if (rlLine.ArrCanceled)
				olCxx = "X-";
			else if (rlLine.DepCanceled)
				olCxx = " -X";
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << rlLine.AFlno 
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << rlLine.AStoa.Format("%d/%m/%y")
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << rlLine.AStoa.Format("%H:%M")
		     << setw(1) << opTrenner 
//			 << setw(ilwidth) << rlLine.ADate 
			 << setw(ilwidth) << AOnbl 
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.ATtyp
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.AOrg3
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.AVia3
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.AAct
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DFlno
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DStod.Format("%d/%m/%y")
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DStod.Format("%H:%M")
		     << setw(1) << opTrenner 
//			 << setw(ilwidth) << rlLine.DDate 
			 << setw(ilwidth) << DOnbl 
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DTtyp
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DDes3
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.DVia3
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.ASeas
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << olCxx
			 << endl;

	}

	of.close();
	return true;
}


