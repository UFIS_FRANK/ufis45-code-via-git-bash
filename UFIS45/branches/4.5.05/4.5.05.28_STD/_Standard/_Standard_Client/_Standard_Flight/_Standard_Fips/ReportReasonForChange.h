// ReportReasonForChange.h: interface for the ReportReasonForChange class.

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REPORTREASONFORCHANGE_H__12254D53_C2F6_4CAA_93E5_B491D836ADD4__INCLUDED_)
#define AFX_REPORTREASONFORCHANGE_H__12254D53_C2F6_4CAA_93E5_B491D836ADD4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resrc1.h"
#include <CCSEdit.h>
#include <CCSPtrArray.h>


struct REASON_FOR_CHANGE
{
	long Urno;
	char Flno[12];
	long Furn;
	char Ftyp[3];
	CTime Scdt;
	CTime Cdat;
	char Aloc[13];
	char Code[7];
	char Rema[130];
	char Usec[35];
	char OVal[12];
	char NVal[12];

	REASON_FOR_CHANGE()
	{
		memset(this,'\0',sizeof(*this));
	}
};

class CReportReasonForChange : public CDialog  
{
public:
	CReportReasonForChange(CWnd* popParent,const CString& ropHeadline,CTime* popMinDate, CTime* popMaxDate);
	virtual ~CReportReasonForChange();
	bool ReadReasonForChange(CProgressCtrl* pop_PR_Read,const CString& ropMinStr, const CString& ropMaxStr);

	CTime *pomMinDate;
	CTime *pomMaxDate;

	//{{AFX_DATA(CReportReasonForChange)
	enum { IDD = IDD_REPORTREASONFORCHANGE };
	CCSEdit	m_CE_DateFrom;
	CCSEdit	m_CE_DateTo;
	CCSEdit	m_CE_TimeFrom;
	CCSEdit	m_CE_TimeTo;
	CString	m_DateFrom;
	CString	m_DateTo;
	CString	m_TimeFrom;
	CString	m_TimeTo;
	int     m_Positions;
	int     m_Gates;
	int     m_Sort1;
	int     m_Sort2;
	CButton	m_CB_Sort1;
	CButton	m_CB_Sort2;
	CButton	m_CB_Positions;
	CButton m_CB_Gates;
	//}}AFX_DATA
	CString omHeadline;
	CCSPtrArray<REASON_FOR_CHANGE> omReasonForChangeData;
 	
	//{{AFX_VIRTUAL(CReportReasonForChange)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// Implementierung
protected:
	
	//{{AFX_MSG(CReportReasonForChange)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void SetFieldType();
};

#endif // !defined(AFX_REPORTREASONFORCHANGE_H__12254D53_C2F6_4CAA_93E5_B491D836ADD4__INCLUDED_)
