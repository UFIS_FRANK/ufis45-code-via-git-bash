// CedaUaaData.h: interface for the CedaUaaData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDAUAADATA_H__6CAD590B_0DAC_4197_88F5_9A3C15165370__INCLUDED_)
#define AFX_CEDAUAADATA_H__6CAD590B_0DAC_4197_88F5_9A3C15165370__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

struct UAADATA 
{
	
	long 	 Urno;			// URNO
	CTime 	 Cdat; 			// Creation Date
	char 	 Usec[34]; 		// User Creator
	char	 Grpu[34];		// Group URNO
	char	 Data[1025];	// Data
	char	 Ufea[12];		//Urno of FEATAB
	char	 Pref[1];		//Preceeding sign (* or blank or + or -)
	
	//DataCreated by this class
	int      IsChanged;

	UAADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//IsChanged = DATA_UNCHANGED;
		//Cdat =	TIMENULL;
		Urno = 0L;
		strcpy(Usec," ");
		strcpy(Grpu," ");
		strcpy(Data," ");
		strcpy(Ufea," ");
		strcpy(Pref," ");
	}

}; // end UAADATA

struct ALADATA {

	long 	Urno;
	char	ALC2[2]; // ALC2
	char	ALC3[3];// ALC3
	bool	IsVisible;

	ALADATA(void)
	{
		Urno=0;
		strcpy(ALC2, "");
		strcpy(ALC3, "");
		IsVisible=false; //Default 
	}
};

class CedaUaaData  : public CCSCedaData
{
	// Attributes
public:
    
	CMapPtrToPtr omUrnoMap;
    CCSPtrArray<UAADATA> omData;

	char pcmListOfFields[2048];
	char pcmFList[512];

	CCSPtrArray<CString> omProfileData;
	CStringArray omProfiles;
	
	CCSPtrArray<ALADATA> omAlineData;
	CMapPtrToPtr omAlineMap;

	int olMinusCount;
	int olAllCount;
	int olNoneCount;
	int olPlusCount;

public:
	CedaUaaData();
	virtual ~CedaUaaData();
	CString GetViewLoadSettings();
	bool Read();
	bool AddProfileList(CString& prpProfileList);
	CString GetProfileList();
	bool ProcessAirlineCodes();
	void InsertInternalAirline(ALADATA *prpAla);
	ALADATA *GetAlaByUrno(long opUrno);
	void ClearAll(void);
	bool IsVisible(CString olAlc2,CString olAlc3);

	void InsertInternal(UAADATA *prpUaa);
	UAADATA *GetUaaByUrno(long opUrno);
	void SetPrefix(UAADATA &prpUaa);
	CString GetAccessibleAlt( CStringArray *popStrArray);
	//bool IsVisible(long opUrno);

};

extern CedaUaaData ogUaaData;

#endif // !defined(AFX_CEDAUAADATA_H__6CAD590B_0DAC_4197_88F5_9A3C15165370__INCLUDED_)
