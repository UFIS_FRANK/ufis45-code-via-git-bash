// PosDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <PosDiagram.h>
#include <ButtonListDlg.h>
#include <RotationTables.h>
#include <PosDiaPropertySheet.h>
#include <DiaCedaFlightData.h>
#include <PosChart.h>
#include <PosGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <DataSet.h>
#include <WoResTableDlg.h>
#include <PrivList.h>
#include <Konflikte.h>
#include <SpotAllocateDlg.h>
#include <FlightSearchTableDlg.h>
#include <DiffPosTableDlg.h>
#include <Utils.h>
#include <CcaExpandDlg.h>
#include <SpotAllocation.h>
#include <BltOverviewTableDlg.h>
#include <BltOverviewTableDlg.h>
#include <SpecialConflictDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void PosDiagramCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// PosDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(PosDiagram, CFrameWnd)

PosDiagram::PosDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
	
	if(bgShowMXAC)
		imStartTimeScalePos = 104;
	else
		imStartTimeScalePos = 74;
	
    //imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogPosDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	//	pomChart = NULL;
	
	bmRepaintAll = false;
	pomDiffPosTableDlg = NULL;
	bmWithAcOnGround = bgShowOnGround;
	pomBltOverviewTableDlg = NULL;
	m_key = "DialogPosition\\PositionDiagram";
	
	omArrCutPasteUrno = 0L;
	omDepCutPasteUrno = 0L;
	
    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;
	
}


PosDiagram::~PosDiagram()
{
	pogPosDiagram = NULL;
}


BEGIN_MESSAGE_MAP(PosDiagram, CFrameWnd)
//{{AFX_MSG_MAP(PosDiagram)
ON_WM_CREATE()
ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
ON_WM_DESTROY()
ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
ON_BN_CLICKED(IDC_WORES_PST, OnWoRes)
ON_BN_CLICKED(IDC_SPECIAL_CONFLICT, OnSpecialConflict)
ON_BN_CLICKED(IDC_POSCHECK, OnPosCheck)
ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
ON_WM_CLOSE()
ON_WM_GETMINMAXINFO()
ON_WM_PAINT()
ON_WM_ERASEBKGND()
ON_WM_SIZE()
ON_WM_HSCROLL()
// Grouping
ON_BN_CLICKED(IDC_PREV, OnNextChart)
ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
ON_WM_TIMER()
ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
ON_BN_CLICKED(IDC_ZEIT, OnZeit)
ON_BN_CLICKED(IDC_SEARCH, OnSearch)
ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
ON_BN_CLICKED(IDC_INSERT, OnDaily)
ON_WM_KEYDOWN()
ON_WM_KEYUP()
ON_BN_CLICKED(IDC_PRINT, OnPrint)
ON_WM_ACTIVATE()
ON_BN_CLICKED(IDC_CHECK_AC_ONGROUND, OnCheckAcOnground)
ON_BN_CLICKED(IDC_EXPAND, OnExpand )
ON_BN_CLICKED(IDC_CHANGES2, OnChanges)
ON_BN_CLICKED(IDC_POSOVERVIEW, OnOverview)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//	ON_BN_CLICKED(IDC_TAGES, OnDaily)


/////////////////////////////////////////////////////////////////////////////
// PosDiagram message handlers
#if OLD
void PosDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
	
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
	
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;
	
    olChartRect.top = ilLastY;
	
    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
	
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
	
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);
	
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	//OnUpdatePrevNext();
	UpdateTimeBand();
}

#endif
// Grouping
void PosDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    PosChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        polChart = (PosChart *) omChartArray.GetAt(ilIndex);
		
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
		
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;
		
        olChartRect.top = ilLastY;
		
        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) &&
			(olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}
	
    omClientWnd.Invalidate(TRUE);
	SetAllAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}

// Grouping
void PosDiagram::OnUpdatePrevNext(void)
{
	
	//	return;
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	
	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);
	
	if (imFirstVisibleChart == omChartArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

// Grouping
void PosDiagram::SetPosAreaButtonColor(int ipGroupno)
{
	if (bgDiaGrouping) 
	{
		CTime ilStart = omViewer.GetGeometrieStartTime(); //ogBasicData.GetTime();
		CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
		PosChart *polPosChart;
		int ilColorIndex; //x;
		ilColorIndex = 0; //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
		//	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);

		if(ipGroupno < omChartArray.GetSize())	
		{

			polPosChart = (PosChart *) omChartArray.GetAt(ipGroupno);
			if (polPosChart != NULL)
			{
				CCSButtonCtrl *prlButton =polPosChart->GetChartButtonPtr();
				if (prlButton != NULL)
				{
					if (ilColorIndex == 0) //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
					{
						prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
							::GetSysColor(COLOR_BTNSHADOW),
							::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{
						prlButton->SetColors(ogColors[ilColorIndex],
							::GetSysColor(COLOR_BTNSHADOW),
							::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
			}
		}
	}
}

// Grouping
void PosDiagram::SetAllAreaButtonsColor(void)
{
	if (!bgDiaGrouping) 
		return;

	int ilGroupCount = omViewer.GetGroupCount();
	
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		SetPosAreaButtonColor(ilGroupno);
	}
}


void PosDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
	/*
    if ((omTSStartTime < omStartTime) ||
	(omTSStartTime > omStartTime + omDuration))
    {
	omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
	0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
	*/
	
	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
	
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

#if OLD
CListBox *PosDiagram::GetBottomMostGantt()
{
	return &pomChart->omGantt;
}
#endif

// Grouping
CListBox *PosDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);
	
	// Searching for the bottommost chart
	PosChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		polChart = (PosChart *)omChartArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}
	
	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omChartArray.GetSize()-1))
		return NULL;
	
	return &((PosChart *)omChartArray[ilLc])->omGantt;
}

void PosDiagram::ActivateTimer()
{
	SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
}

void PosDiagram::DeActivateTimer()
{
	KillTimer(1);
}

int PosDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// set timers
    SetTimer(0, (UINT) 60 * 1000, NULL);
	ActivateTimer();
	//    SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
	
	// create dialog bar
	omDialogBar.Create( this, IDD_POSDIAGRAM, CBRS_TOP, IDD_POSDIAGRAM );
	SetWindowPos(&wndTop/*Most*/, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	
    omViewer.Attach(this);
	
	
	UpdateComboBox();
	
	
	// Create 'Offline'-Button
	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	//m_CB_Offline.ShowWindow(SW_HIDE);
#ifdef _FIPSVIEWER
	{
		m_CB_Offline.EnableWindow(false);
	}
#else
	{
	}
#endif
	
	
	
	
	// Create 'Special conflict'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_SPECIAL_CONFLICT);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%d", 0);
	m_CB_SP_Conf.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_SPECIAL_CONFLICT);
    m_CB_SP_Conf.SetFont(polButton->GetFont());
	m_CB_SP_Conf.EnableWindow(true);
	m_CB_SP_Conf.SetSecState(ogPrivList.GetStat("POSDIAGRAMM_CB_Special_Conflict"));	
	
	if(!bgPositionConfirmation)
		m_CB_SP_Conf.ShowWindow(SW_HIDE);
	
	
	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_WORES_PST2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	olTmp.Format("%s (%d)", GetString(IDS_STRING1914),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_WORES_PST);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("POSDIAGRAMM_CB_WORes"));	
	
	
	// Create 'Pos.Check'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_POSCHECK2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_PosCheck.Create(GetString(IDS_STRING2111), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_POSCHECK);
    m_CB_PosCheck.SetFont(polButton->GetFont());
	m_CB_PosCheck.EnableWindow(true);
	m_CB_PosCheck.SetSecState(ogPrivList.GetStat("POSDIAGRAMM_CB_PosCheck"));	
	
	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Changes.Create(GetString(IDS_CHANGES), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES2);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History")); 
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);
	
	
	ResetStateVariables();
	/*
    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
	0, 0, 0);// - CTimeSpan(1, 0, 0, 0);
	
	  omDuration = CTimeSpan(3, 0, 0, 0);
	  omTSDuration = CTimeSpan(0, 6, 0, 0);
	  omTSInterval = CTimeSpan(0, 0, 10, 0);
	*/
    
	// create current time and date
	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = olLocalTime;
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	CRect olRect; GetClientRect(&olRect);
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);
    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	
	// create displayed time
    char olBuf[16];
    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
	
	if(bgShowMXAC)
		omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
		CRect(ilPos, 35, ilPos + 80, 52), this);
	else
		omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
		CRect(ilPos + 11, 35, ilPos + 73, 52), this);
	// CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "PREVX");
	
    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");
	
	
	if(!bgDiaGrouping)
	{
		omBB1.ShowWindow(SW_HIDE);
		omBB2.ShowWindow(SW_HIDE);
	}
	
	// create timescale
	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos , 28, olRect.right - (36 + 2), 62), this, 0, NULL);
	//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
	//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	
	
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olUtcTime);
    
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR, ID_SEPARATOR, ID_SEPARATOR, ID_SEPARATOR };
    //omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
	
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));
	
    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_NORMAL | SBPS_NORMAL, 400);
	
    omStatusBar.GetPaneInfo(1, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(1, nID, SBPS_NORMAL, 130);
	
    omStatusBar.GetPaneInfo(2, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(2, nID, SBPS_STRETCH | SBPS_NORMAL, 600);
	
	
    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
	
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);
	
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	//OnUpdatePrevNext();
	
    olRect.SetRect(olRect.left, 0, olRect.right, 0);
	
	// Create DiffPosTableDlg
	pomDiffPosTableDlg = new DiffPosTableDlg(this);	
	
#if OLD
	// create one chart
    pomChart = new PosChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "PosChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
		olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
#endif
	// Grouping
	int ilLastY = 0;
	PosChart *polChart;
	
	int ilCount = omViewer.GetGroupCount();
	
	if (!bgDiaGrouping) 
		ilCount = 1;
	
	
    for (int ilI = 0; ilI < ilCount; ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new PosChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "PosChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight();
    }
	
	
    OnTimer(0);
	
	
    CButton *polCB;
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Allocate"),polCB);
#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
#else
	{
	}
#endif
	
	//checkbox on ground
	polCB = (CButton*)omDialogBar.GetDlgItem(IDC_CHECK_AC_ONGROUND);
	polCB->SetCheck(bmWithAcOnGround);
	polCB->EnableWindow(true);
	
	if (ogPrivList.GetStat("POSDIAGRAMM_CB_AC_OnGround") == '0')
		polCB->EnableWindow(false);
	if (ogPrivList.GetStat("POSDIAGRAMM_CB_AC_OnGround") == '-')
		polCB->ShowWindow(SW_HIDE);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT);
	SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Print"),polCB);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Search"),polCB);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Ansicht"),polCB);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Expand"),polCB);
#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
#else
	{
	}
#endif
	
	// Create OverviewTableDlg
	if (pomBltOverviewTableDlg == NULL && bgAllocationOverview)
	{
		pomBltOverviewTableDlg = new BltOverviewTableDlg(this,"POS");
		pomBltOverviewTableDlg->SetWindowPos(&wndTop,100,200,850,400, SWP_HIDEWINDOW);
		pomBltOverviewTableDlg->SetResource("POS");
	}
	
	//overviewbutton	
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_POSOVERVIEW);	
	if (polButton && !bgAllocationOverview)
	{
		polButton->EnableWindow(FALSE);
		polButton->ShowWindow(SW_HIDE);
	}
	
	
	
	if (!bgOffRelFuncGatpos)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}
	//SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Save"),polCB);
	
	
	// Register DDX call back function
	//TRACE("PosDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),CString("Redisplay all from What-If"), PosDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),CString("Update Time Band"), PosDiagramCf);	// for updating the yellow lines
	ogDdx.Register(this, SHOW_FLIGHT, CString("STAFFDIAGRAM"),CString("Update Time Band"), PosDiagramCf);	// for updating the yellow lines
	ogDdx.Register(this, DATA_RELOAD,CString(" "), CString("DATA_RELOAD"),PosDiagramCf);
	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),PosDiagramCf);
	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),PosDiagramCf);
	ogDdx.Register(this, D_FLIGHT_CHANGE_WORES, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), PosDiagramCf);
	
	// Connect to the GatPosFlightData-Class
	ogPosDiaFlightData.Connect();
	// Connect to the FlightsWithoutResource - Table
	pogWoResTableDlg->Connect();
	
	
	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);
	
	
	char pclView[100];
	strcpy(pclView, "<Default>");
	ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
	omViewer.SelectView(pclView);
	
	
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
		
		ogSpotAllocation.MakePstBlkData(omStartTime, omStartTime + omDuration);
		
	}
	
	
	ViewSelChange(pclView);
	
	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IMFK_AIRCRAFT_POS);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	MoveWindow(olRect);
	//	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	SetFocusToDiagram();//PRF 8363
	
	
	if(pogSpecialConflictDlg != NULL)
		pogSpecialConflictDlg->AttachButton(&m_CB_SP_Conf);
	
	
	return 0;
	
}

void PosDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void PosDiagram::OnBeenden() 
{
	//ogPosDiaFlightData.UnRegister();	 
	//ogPosDiaFlightData.ClearAll();
	
	DestroyWindow();	
}


void PosDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void PosDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogPosDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), POSDIA);
	/*
	_CrtMemState s1, s2, s3;
	_CrtMemCheckpoint( &s1 );
	RepaintAll(0,0);
	_CrtMemCheckpoint( &s2 );
	
	  if ( _CrtMemDifference( &s3, &s1, &s2) ) 
	  _CrtMemDumpStatistics( &s3 );
	*/
	
	/*
	CTime olCurrT;
	for (int i = 0; i < 1; i++)
	{
	olCurrT = CTime::GetCurrentTime();
	RepaintAll(0,0);
	//TRACE("PosDiagram::OnSearch: Repaint Time = %ld sec \n", (CTime::GetCurrentTime() - olCurrT).GetTotalSeconds());
	}
	*/
	
}


void PosDiagram::OnOffline()
{	
	if(ogPosDiaFlightData.bmOffLine)
	{


		if( bgReleaseRight && ogPrivList.GetStat("GATPOS_RELEASE") != '1')
		{
			ViewSelChange();
		}
		else
		{
			if (MessageBox(GetString(IDS_STRING1772), GetString(IMFK_RELEASE), MB_YESNO | MB_ICONQUESTION ) == IDNO)
				return;
		}


		ogPosDiaFlightData.IsPosGateChanged();

	}
	
	if(bgReasonFlag)
	{
		
		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.bmPosReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this);
			ogPosDiaFlightData.lmPosReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmPosReasonUrno == 0)
				return;
		}
		
		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.bmGatReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this, CString("GATF"), CString(""));
			ogPosDiaFlightData.lmGatReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmGatReasonUrno == 0)
				return;
		}
	}
	
	ogPosDiaFlightData.ToggleOffline();
	
	
	ogPosDiaFlightData.bmGatReason = false;
	ogPosDiaFlightData.lmGatReasonUrno = 0;
	ogPosDiaFlightData.bmPosReason = false;
	ogPosDiaFlightData.lmPosReasonUrno = 0;
	
}


void PosDiagram::OnAnsicht()
{
	PosDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1648) );
	bmIsViewOpen = true;
	int ilDlgRet = olDlg.DoModal();
	bmIsViewOpen = false;
	
	if(ilDlgRet != IDCANCEL)
	{
		ogSpotAllocation.InitializePositionMatrix();
		
		if (olDlg.FilterChanged())
		{
			omArrCutPasteUrno = 0;
			omDepCutPasteUrno = 0;
			LoadFlights();
		}
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.SASCheckAll();
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			
			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}
		
		UpdateDia();
		
		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
		
	}
	
	UpdateComboBox();
	SetFocusToDiagram();//PRF 8363
}



void PosDiagram::ResetStateVariables()
{
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);
	
    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);
	
    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);
	
}



bool PosDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	if (olViewName == "<Default>")
	{
		ogPosDiaFlightData.ClearAll();
		ResetStateVariables();
	}
	
	
	if(IsIconic())  //RST
	{
		return false;
	}
	
	
	
	// set internal variables
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}
	
	
	omViewer.MakeMasstab();
	
	CTime olT = omViewer.GetGeometrieStartTime();
	//	ogBasicData.UtcToLocal(olT); //RSTL
	SetTSStartTime(olT);
	
	omTSDuration = omViewer.GetGeometryTimeSpan();
	//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if (!bgNewGantDefault)
	{
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
	}
	
	if (bgNewGantDefault)
	{
		if (olViewName != "<Default>")
		{
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);
		}
		else
		{
			if(!bgGatPosLocal)
				ogBasicData.LocalToUtc(olTSStartTime);
		}
	}
	
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
	
	if (bgNewGantDefault)
		omTimeScale.SetDisplayStartTime(olTSStartTime);
	
    omTimeScale.Invalidate(TRUE);
	ChangeViewTo(omViewer.GetViewName(), false);
	
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	/*
	char pclView[100];
	strcpy(pclView, omViewer.GetViewName());
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
	*/
	
	pogWoResTableDlg->Rebuild();
	pomDiffPosTableDlg->Rebuild();
	
	return true;
}

bool PosDiagram::WithAcOnGround()
{
	return bmWithAcOnGround;
}

bool PosDiagram::LoadFlights(const char *pspView)
{
	if(pspView == NULL)
	{
		CString olView = omViewer.GetViewName();
		pspView = olView.GetBuffer(0);
	}
	
	
	omViewer.SelectView(pspView);
	if (strcmp(pspView, "<Default>") == 0)
	{
		return false;
	}
	
	
	// get Where-Clause
	CString olWhere;
	bool blRotation;
	CString olFOGTAB = "";
	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, olFOGTAB, bmWithAcOnGround))
		return false;
	// PRF 8382
	// Frames the query for the Genral filter(Unifilter) page.
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}
	
	olWhere.TrimRight();
	if (olWhere.IsEmpty())
		return false;
	
	if (bgMustHaveFLNO) 
		olWhere += CString(" AND FLNO <> ' '");
	
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	// upate status bar
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	
	
	ogPosDiaFlightData.Register();	 
	
	
	// get timeframe
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	
	//check timelimit for on gound (at moment we don#t know if we should use global or local)
	if (bgShowOnGround || bmWithAcOnGround)
	{
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			CTimeSpan olDuration = olTo - olFrom;
			int ilTotalMinutes = olDuration.GetTotalMinutes();
			if (ogOnGroundTimelimit.GetTotalMinutes() >= ilTotalMinutes /*&& !bmMessageDone*/)
			{
				CString olMess;
				CString olTimeLimit;
				int ilHours = ogOnGroundTimelimit.GetTotalHours();
				int ilMinutes = ogOnGroundTimelimit.GetTotalMinutes() - ilHours*60;
				olTimeLimit.Format("%d.%d",ilHours,ilMinutes);
				olMess.Format(GetString(IDS_ONGROUND_TIMELIMIT), olTimeLimit );
				
				if (CFPMSApp::MyTopmostMessageBox(NULL, olMess, GetString(ST_WARNING), MB_OKCANCEL | MB_ICONWARNING) == IDOK)
					bool blok = true;
				else
					return false;
				
				bmMessageDone = true;
			}
		}
	}
	
	// enable/disable now button
	CTime olCurrUtc;
	GetCurrentUtcTime(olCurrUtc);
	
	/*	
	// Enable / Disable 'Now'-Button
	CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);
	if (olFrom < olCurrUtc && olTo > olCurrUtc)
	{
	polCB->EnableWindow(TRUE);
	}
	else
	{
	polCB->EnableWindow(FALSE);
	}
	
	*/
	
	
	ogSpotAllocation.MakePstWroBltBlkData( olFrom - CTimeSpan(1,0,0,0) , olTo + CTimeSpan(1,0,0,0));
	
	
	// set pre-filter
	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	CString olAdidIn ("");
	omViewer.GetAdid(olAdidIn);
	ogPosDiaFlightData.SetPreSelectionAdid(olAdidIn);
	
	// read flights
	//	CString add = "AND (PSTA <> 'GG') AND (PSTD <> 'GG')";// OR PSTD  <> 'GG')";
	//	olWhere += add;
	//	::MessageBox(NULL,olWhere,"",MB_OK);
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, olFOGTAB);
	
	/*
	// set internal variables
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	*/
	
	// upate status bar
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	return true;
}


void PosDiagram::OnDestroy() 
{
	SaveToReg();
	//	if (bgModal == TRUE)
	//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);
	
	
	if(pogSpecialConflictDlg != NULL)
		pogSpecialConflictDlg->DeAttachButton();
	
	
	
	// Unregister DDX call back function
	TRACE("PosDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);
	
	// Disconnect from the GatPosFlightData-Class
	ogPosDiaFlightData.Disconnect();
	// Disconnect from the FlightsWithoutResource - Table
	if (pogWoResTableDlg)
		pogWoResTableDlg->Disconnect();
	
	if (pomDiffPosTableDlg != NULL)
	{
		pomDiffPosTableDlg->DestroyWindow();
		delete pomDiffPosTableDlg;
		pomDiffPosTableDlg = NULL;
	}
	
	if (pomBltOverviewTableDlg)
	{
		delete pomBltOverviewTableDlg;
		pomBltOverviewTableDlg = NULL;
	}
	
	
	CFrameWnd::OnDestroy();
}

void PosDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void PosDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);
	
    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL PosDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);
	
    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void PosDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);
	
    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);
	
    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);
	
    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void PosDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);
	
	CRect olBB1Rect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);
	
	
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);
	
    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);
	
	
	GetClientRect(&olRect);
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));
	
	PositionChild();
}


void PosDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	//CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);
	
	
	//	int ilCurrLine = pomChart->omGantt.GetTopIndex();
	
    long llTotalMin;
    int ilPos;
    
	CTime olTSStartTime = omTSStartTime;
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
    switch (nSBCode)
    {
	case SB_LINEUP :
		//OutputDebugString("LineUp\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
		if (ilPos <= 0)
		{
			ilPos = 0;
			ShowTime(omStartTime);
		}
		else
			ShowTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_LINEDOWN :
		//OutputDebugString("LineDown\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
		if (ilPos >= 1000)
		{
			ilPos = 1000;
			ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
		}
		else
			ShowTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_PAGEUP :
		//OutputDebugString("PageUp\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ)
			- int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
		if (ilPos <= 0)
		{
			ilPos = 0;
			ShowTime(omStartTime);
		}
		else
			ShowTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_PAGEDOWN :
		//OutputDebugString("PageDown\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ)
			+ int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
		if (ilPos >= 1000)
		{
			ilPos = 1000;
			ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
		}
		else
			ShowTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_THUMBTRACK /* pressed, any drag time */:
		//OutputDebugString("ThumbTrack\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		
		SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
		
		olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
		
		omTimeScale.SetDisplayStartTime(olTSStartTime);
		omTimeScale.Invalidate(TRUE);
		SetScrollPos(SB_HORZ, nPos, TRUE);
		omClientWnd.Invalidate(FALSE);
        break;
	case SB_THUMBPOSITION:	// the thumb was just released?
		omClientWnd.Invalidate(FALSE);
		
	case SB_TOP :
        //break;
	case SB_BOTTOM :
		//OutputDebugString("TopBottom\n\r");
        break;
        
	case SB_ENDSCROLL :
		//OutputDebugString("EndScroll\n\r");
		//OutputDebugString("\n\r");
        break;
    }
	
	// 	pomChart->omGantt.SetTopIndex(ilCurrLine);
	
}


void PosDiagram::OnTimer(UINT nIDEvent)
{
	
	if (nIDEvent == 1 && !bgConfCheckRuns)
	{
		DeActivateTimers();
		ogPosDiaFlightData.UpdateBarTimes();
		ActivateTimers();
	}
	
	/*
	if(nIDEvent == 2)
	{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	ogKonflikte.SASCheckAll();
	ChangeViewTo(omViewer.GetViewName(), true);
	
	  KillTimer(2);
	  SetTimer(2, (UINT) 60 * 1000 * 5, NULL);
	  
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		return;
		}
	*/
	
	
	///// Update time line!
	if(bmNoUpdatesNow == FALSE)
	{
		
		//		ogPosDiaFlightData.UpdateBarTimes();
		
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);
		
		char pclTimes[100];
		char pclDate[100];
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -=	ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}
		
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
		
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);
		
		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);
		
		// Grouping
		for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
		{
			if(bgGatPosLocal)
				((PosChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				((PosChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}
		
		CFrameWnd::OnTimer(nIDEvent);
	}
	///// Update time line! (END)
}

void PosDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	omViewer.omMarkTimeStart = opStartTime;
	omViewer.omMarkTimeEnd = opEndTime;
	
	// Grouping
	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		((PosChart *)omChartArray[ilChartNo])->SetMarkTime(opStartTime, opEndTime);
	}
	
	//pomChart->SetMarkTime(opStartTime, opEndTime);
}


LONG PosDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

/* before grouping
LONG PosDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{

  //qqq
  //	UpdateGanttButtons();
  
	int ilItemID = omViewer.GetItemID(HIWORD(lParam), LOWORD(lParam));
	
	  PosGantt *polPosGantt = pomChart -> GetGanttPtr();
	  switch (wParam)
	  {
	  // line message
	  case UD_INSERTLINE :
	  polPosGantt->InsertString(ilItemID, "");
	  polPosGantt->RepaintItemHeight(ilItemID);
	  
		PositionChild();
        break;
        
		  case UD_UPDATELINE :
		  {
		  polPosGantt->RepaintVerticalScale(ilItemID);
		  if (polPosGantt->GetItemHeight(ilItemID) != polPosGantt->GetLineHeight(ilItemID))
		  {
		  polPosGantt->RepaintItemHeight(ilItemID);
		  PositionChild();
		  }
		  else
		  {
		  polPosGantt->RepaintGanttChart(ilItemID);
		  }
		  }
		  break;
		  
			case UD_DELETELINE :
            polPosGantt->DeleteString(ilItemID);
            
			  PositionChild();
			  break;
			  
				case UD_UPDATELINEHEIGHT :
				polPosGantt->RepaintItemHeight(ilItemID);
				
				  PositionChild();
				  break;
				  }
				  
					return 0L;
					}
*/

// Grouping
LONG PosDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);
	
   // PosChart *polPosChart = (PosChart *) omChartArray.GetAt(ipGroupNo);
	PosChart *polPosChart;
	if (bgDiaGrouping) 
	{
		if(omChartArray.GetSize() <= ipGroupNo)
			return 0L;
		polPosChart = (PosChart *) omChartArray.GetAt(ipGroupNo);
	} 
	else 
	{
		if(omChartArray.GetSize() == 0)
			return 0L;
		polPosChart = (PosChart *) omChartArray.GetAt(0);
		if (ipGroupNo > 0) {
			ipLineNo = omViewer.GetAbsoluteLine(ipGroupNo,ipLineNo);
		}
	}

	if(polPosChart == NULL)
	{
		return 0L;
	}
    PosGantt *polPosGantt = polPosChart -> GetGanttPtr();
	if(polPosGantt == NULL)
	{
		return 0L;
	}
	
    switch (wParam)
    {
        // group message
	case UD_INSERTGROUP :
		// CreateChild(ipGroupNo);
			if (bgDiaGrouping) {
		if (ipGroupNo <= imFirstVisibleChart)
		{
			imFirstVisibleChart++;
			OnUpdatePrevNext();
		}
		PositionChild();
			}
        break;
		
	case UD_UPDATEGROUP :
			if (bgDiaGrouping) {
		olStr = omViewer.GetGroupText(ipGroupNo);
		polPosChart->GetChartButtonPtr()->SetWindowText(olStr);
		polPosChart->GetChartButtonPtr()->Invalidate(FALSE);
		
		olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
		polPosChart->GetTopScaleTextPtr()->SetWindowText(olStr);
		polPosChart->GetTopScaleTextPtr()->Invalidate(TRUE);
		SetPosAreaButtonColor(ipGroupNo);
			}
        break;
        
	case UD_DELETEGROUP :
			if (bgDiaGrouping) {
		delete omChartArray.GetAt(ipGroupNo);
		//omChartArray.GetAt(ipGroupNo) -> DestroyWindow();
		omChartArray.RemoveAt(ipGroupNo);
		
		if (ipGroupNo <= imFirstVisibleChart)
		{
			imFirstVisibleChart--;
			OnUpdatePrevNext();
		}
		PositionChild();
			}
        break;
		
        // line message
	case UD_INSERTLINE :
		polPosGantt->InsertString(ipLineNo, "");
		polPosGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
        
	case UD_UPDATELINE :
		polPosGantt->RepaintVerticalScale(ipLineNo);
		polPosGantt->RepaintGanttChart(ipLineNo);
		if (polPosGantt->GetItemHeight(ipLineNo) != polPosGantt->GetLineHeight(ipLineNo))
		{
			polPosGantt->RepaintItemHeight(ipLineNo);
			PositionChild();
		}
		else
		{
			polPosGantt->RepaintGanttChart(ipLineNo);
		}
		SetPosAreaButtonColor(ipGroupNo);
        break;
		
	case UD_DELETELINE :
		polPosGantt->DeleteString(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
	case UD_UPDATELINEHEIGHT :
		polPosGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
    }
	
    return 0L;
}



///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void PosDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		
		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}
	
	ilIndex = polCB->FindString(-1,olViewName);
	
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}


LONG PosDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}


void PosDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	
	//SetFocusToDiagram();//PRF 8363
	
	if (bgNoScroll == TRUE)
		return;
	
	
	for (int ilLc = 0; ilLc < omChartArray.GetSize(); ilLc++)
	{
		PosChart *polChart = (PosChart *)omChartArray[ilLc];
		if (polChart != NULL)
		{
			if (!polChart->omGantt.bmRepaint)
			{
				bmRepaintAll = true;
				return;
			}
		}
	}
	
	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	
	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();
	CCSPtrArray <int> olChartTopIndices;
	olChartTopIndices.RemoveAll();
	
	
	CRect olRect; omTimeScale.GetClientRect(&olRect);
	
	/*
	//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
	ogBasicData.UtcToLocal(olTSStartTime);
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	*/
	// viewer always in UTC
	omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((PosChart *)omChartArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
			
			
			int ilTopIndex = 0;
			if (((PosChart *)omChartArray[ilIndex])->GetGanttPtr() != NULL)
				ilTopIndex = ((PosChart *)omChartArray[ilIndex])->GetGanttPtr()->GetTopIndex();
			
			olChartTopIndices.NewAt(ilIndex,ilTopIndex);
		}
		((PosChart *)omChartArray[ilIndex])->DestroyWindow();
    }
    omChartArray.RemoveAll();
	
    PosChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);
    
    imFirstVisibleChart = 0;
    int ilLastY = 0;
	
	
	int ilCount = omViewer.GetGroupCount();
	
	if (!bgDiaGrouping) 
		ilCount = 1;
	
	
    for (int ilI = 0; ilI < ilCount; ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new PosChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "PosChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
		
		if(bgGatPosLocal)
		{
			polChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		} else
		{
			polChart->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight();
    }
	
	PositionChild();
	
#if 0
	
	int ilChartState;
	int ilTopIndex;
	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.
	pomChart->DestroyWindow();
	//delete pomChart;
	
#endif
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
		imFirstVisibleChart = 0;
	}
	
    olRect.SetRect(olRect.left, 0, olRect.right, 0);
	
	
	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
	
	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	
	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		if (RememberPositions == TRUE)
		{
			int ilChartState = olChartStates[ilChartNo];
			int ilTopIndex = olChartTopIndices[ilChartNo];
			
			((PosChart *)omChartArray[ilChartNo])->SetState(olChartStates[ilChartNo]);
			if (((PosChart *)omChartArray[ilChartNo])->GetGanttPtr() != NULL)
				((PosChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetTopIndex(olChartTopIndices[ilChartNo]);
		}
		((PosChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olCurr);
	}      
	
	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	
	if (pomBltOverviewTableDlg)
		pomBltOverviewTableDlg->Update();
	
	UpdateGanttButtons();
	
	if(pogSpecialConflictDlg != NULL)
		pogSpecialConflictDlg->Activate();
	
	
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void PosDiagram::OnOverview()
{
	if (pomBltOverviewTableDlg != NULL)
		pomBltOverviewTableDlg->Activate();
}

void PosDiagram::UpdateWoResButton()
{
	//qqq
	//return;
	CString olTmp;
	COLORREF olButtonColor;
	
	int ilCount = pogWoResTableDlg->GetCount(POSDIA);
	if(ilCount > 0)
	{
		if(bgFwrPos)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);
		
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
		
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}
	
	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
}

void PosDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);
	
	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}

void PosDiagram::UpdateGanttButtons()
{
	//qqq
	//return;
	// Update 'Flights w/o res.' Button
	UpdateWoResButton();
	/*
	int ilCount = pogWoResTableDlg->GetCount(POSDIA);
	
	  if(ilCount >= 0)
	  {
	  CString olTmp;
	  olTmp.Format("%s (%d)", GetString(IDS_STRING1914),ilCount);
	  
		CButton *polSave = (CButton *)omDialogBar.GetDlgItem(IDC_WORES);	
		if(polSave != NULL)
		polSave->SetWindowText(olTmp);
		}
	*/
	// Update 'Pos. check' Button
	int ilCount = 0;
	if (pomDiffPosTableDlg)
		ilCount = pomDiffPosTableDlg->GetCount();
	
	if(ilCount > 0)
	{
		m_CB_PosCheck.SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));						
	}
	else
	{
		m_CB_PosCheck.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING2627),ilCount);
	m_CB_PosCheck.SetWindowText(olTmp);
	m_CB_PosCheck.UpdateWindow();
}


void PosDiagram::OnViewSelChange()
{
	omArrCutPasteUrno = 0;
	omDepCutPasteUrno = 0;
	
	ViewSelChange();
}


void PosDiagram::ViewSelChange(char *pcpView)
{
	
	omViewer.SelectView(pcpView);
	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );
	
	UpdateDia();	
	SetFocusToDiagram() ; //PRF 8363
						  /*
						  omViewer.MakeMasstab();
						  CTime olT = omViewer.GetGeometrieStartTime();
						  CString olS = olT.Format("%d.%m.%Y-%H:%M");
						  SetTSStartTime(olT);
						  omTSDuration = omViewer.GetGeometryTimeSpan();
						  
							long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
							long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
							nPos = nPos * 1000L / llTotalMin;
							SetScrollPos(SB_HORZ, int(nPos), TRUE);
							
							  //rkr
							  //Set begin to local if local
							  CTime olTSStartTime = omTSStartTime;
							  if(bgGatPosLocal)
							  ogBasicData.UtcToLocal(olTSStartTime);
							  
								omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
								//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
								omTimeScale.Invalidate(TRUE);
								SetTSStartTime(omTSStartTime);
								ChangeViewTo(clText, false);
	*/
}



void PosDiagram::ViewSelChange()
{
	SetFocusToDiagram();//PRF 8363
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	
	ogSpotAllocation.InitializePositionMatrix();
	
	if (!LoadFlights(clText))
		return;
	
	UpdateDia();	
	
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
	
	/*
	if(strcmp(clText, "<Default>") == 0)
	{
	ClearAll();
	return;
	}
	LoadFlights(clText);
	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();
	
	  
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		
		  
			
			  //rkr
			  //Set begin to local if local
			  CTime olTSStartTime = omTSStartTime;
			  if(bgGatPosLocal)
			  ogBasicData.UtcToLocal(olTSStartTime);
			  
				omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
				//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
				omTimeScale.Invalidate(TRUE);
				SetTSStartTime(omTSStartTime);
				
				  ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
				  pogWoResTableDlg->Rebuild();
				  
					ChangeViewTo(clText, false);
	*/
}





void PosDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void PosDiagram::OnZeit()
{
	
	CTime olTime;
	GetCurrentUtcTime(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);
	
	ShowTime(olTime);
	
	
	/*
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	
	  // TODO: Add your command handler code here
	  CTime olTime = CTime::GetCurrentTime();
	  
		if(!bgGatPosLocal)	
		ogBasicData.LocalToUtc(olTime);
		
		  
			//Die wollen local
			olTime -= CTimeSpan(0, 1, 0, 0);
			SetTSStartTime(olTime);
			//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));
			
			  omTimeScale.SetDisplayStartTime(omTSStartTime);
			  omTimeScale.Invalidate(TRUE);
			  
				// update scroll bar position
				long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
				long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
				nPos = nPos * 1000L / llTotalMin;
				SetScrollPos(SB_HORZ, int(nPos), TRUE);
				
				  ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
				  omClientWnd.Invalidate(FALSE);
	*/
}



void PosDiagram::ShowTime(const CTime &ropTime)
{
	
	if(omTSStartTime == ropTime)
		return;
	
	
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));
	
	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)	
		ogBasicData.UtcToLocal(olTSStartTime);
	
	
	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
	
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
    omClientWnd.Invalidate(FALSE);
}



BOOL PosDiagram::DestroyWindow() 
{
	
	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogPosDiagram = NULL; 
	
	//	if (pomChart != NULL)
	//		pomChart->DestroyWindow();
	
	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

void PosDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void PosDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omChartArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void PosDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void PosDiagram::OnLastChart()
{
	imFirstVisibleChart = omChartArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// PosDiagram keyboard handling

void PosDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
	// This statement has to be fixed for using both in Windows 3.11 and NT.
	// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
	// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	
	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void PosDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// PosDiagram -- implementation of DDX call back function



static void PosDiagramCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName)
{
	PosDiagram *polDiagram = (PosDiagram *)popInstance;
	TIMEPACKET *polTimePacket;
	
	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	case SHOW_FLIGHT:
		{
			DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(*((long *)vpDataPointer));
			if (prlFlight != NULL)
			{
				if (IsArrival(prlFlight->Org3, prlFlight->Des3))
					polDiagram->ShowFlight(prlFlight, NULL);
				else
					polDiagram->ShowFlight(NULL, prlFlight);
			}
		}
		break;
	case DATA_RELOAD:
        polDiagram->ViewSelChange();
		break;
	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case D_FLIGHT_CHANGE_WORES:
		polDiagram->UpdateGanttButtons();
		break;
	}
}


void PosDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}


bool PosDiagram::ShowFlight(long lpFlightUrno, bool bpArrival)
{
	
	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpFlightUrno);
	if (prlFlight == NULL)
		return false;
	else
	{
		if (IsArrival(prlFlight->Org3, prlFlight->Des3))
		{
			return ShowFlight(prlFlight, NULL);
		}
		else
		{
			return ShowFlight(NULL, prlFlight);
		}
	}
}


bool PosDiagram::ShowFlight(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight)
{
	if (prpAFlight == NULL && prpDFlight == NULL) 
		return false;
	
	omArrCutPasteUrno = 0;
	omDepCutPasteUrno = 0;
	
	omViewer.DeSelectAll();
	int ilChartNo;
	
	int index = omViewer.AdjustBar(prpAFlight, prpDFlight, ilChartNo);
	if (index != -1)
	{
		PosChart *polChart = (PosChart *) omChartArray[ilChartNo];
		polChart->omGantt.SetTopIndex(index);
	}
	else
	{
		CTime olStart;
		CTime olEnd;
		omViewer.CalculateTimes(prpAFlight, prpDFlight, olStart, olEnd);						
		CTime olTime = olStart - CTimeSpan(0, 1, 0, 0);
		ShowTime(olTime);
		if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
		if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);
		SetMarkTime(olStart, olEnd);
	}
	return true;
	/*
	
	  CTime olStart;
	  CTime olEnd;
	  omViewer.CalculateTimes(prpAFlight, prpDFlight, olStart, olEnd);						
	  CTime olTime = olStart - CTimeSpan(0, 1, 0, 0);
	  ShowTime(olTime);
	  if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
	  if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);
	  SetMarkTime(olStart, olEnd);
	  pomChart->omGantt.SetTopIndex(omViewer.AdjustBar(prpAFlight, prpDFlight));
	  
		return true;
	*/
}



void PosDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}



void PosDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}


void PosDiagram::UpdateTimeBand()
{
	//	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		PosChart *polChart = (PosChart *)omChartArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
	
}


void PosDiagram::OnDaily()
{
	if (pogRotationTables == NULL)
	{
		//pogRotationTables = new RotationTables(this);
		//TO DO
		MessageBox(GetString(IDS_STRING1702), GetString(IDS_WARNING), MB_OK); //IDS_STRING1398 existiert
	}
	else
	{
		pogRotationTables->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


/*
void PosDiagram::OnSave()
{
CCSButtonCtrl *polSave = (CCSButtonCtrl *)omDialogBar.GetDlgItem(IDC_SAVE);	

  
	if(bmOffLine == false)
	{
	if(pogButtonList->bmOnline)
	pogButtonList->OnOnline();
	
	  bmOffLine = true;
	  //ogKonflikte.SetOffline();
	  ogPosDiaFlightData.SetOffLine();
	  polSave->SetWindowText("Speichern");
	  return;
	  
		}
		
		  
			polSave->SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			
			  
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				
				  
					
					  
						ogPosDiaFlightData.Release();	
						
						  ogPosDiaFlightData.omCcaData.Release( &ogPosDiaFlightData.omCcaData.omData);	
						  ogPosDiaFlightData.omCcaData.Release( &ogPosDiaFlightData.omCcaData.omDelData);	
						  ogPosDiaFlightData.omCcaData.omDelData.DeleteAll();
						  
							
							  polSave->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
							  
								//polSave->ShowWindow(SW_HIDE);	
								
								  //SetSaveButtonColor(::GetSysColor(COLOR_BTNFACE));
								  
									polSave->SetWindowText("Local");
									bmOffLine = false;
									
									  //ogPosDiaFlightData.SetOffLine();
									  
										//ogKonflikte.SetOffline();
										
										  if(!pogButtonList->bmOnline)
										  pogButtonList->OnOnline();
										  
											SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
											
											  
												}
*/

void PosDiagram::OnAllocate()
{
	
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}
	
	
	if (bgOffRelFuncGatpos && !ogPosDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
	
	SpotAllocateDlg *polDlg = new SpotAllocateDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		omStatusBar.SetPaneText(0, GetString(IDS_STRING1493));
		omStatusBar.UpdateWindow();
	}
	
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();	
	
	delete polDlg;
	
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}
	
	
}

void PosDiagram::OnChanges()
{
	CallHistory(CString("PSTPOSITION"));
}

void PosDiagram::OnWoRes()
{
	pogWoResTableDlg->Activate(POSDIA);
}


void PosDiagram::OnPosCheck()
{
	pomDiffPosTableDlg->Rebuild();
	pomDiffPosTableDlg->SetWindowPos(this, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
}



void PosDiagram::SetSaveButtonColor(COLORREF opCol)
{
	
	CCSButtonCtrl *polSave = (CCSButtonCtrl *)omDialogBar.GetDlgItem(IDC_SAVE);	
	
	
	if(opCol == ::GetSysColor(COLOR_BTNFACE))
	{
		polSave->ShowWindow(SW_HIDE);
	}
	else
	{
		polSave->ShowWindow(SW_SHOW);
	}
	
	//m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	//m_CB_Ok.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	
	
}


void PosDiagram::OnPrint() 
{
	
	bool blFirstChart = true;
	
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        PosChart *polChart = (PosChart *) omChartArray.GetAt(ilIndex);
		if ((polChart->GetState() == Maximized || polChart->GetState() == Normal) &&
			omViewer.GetLineCount(ilIndex) > 0)
		{
			omViewer.PrintGantt(polChart,blFirstChart);
			blFirstChart = false;
		}
	}
	omViewer.PrintGanttEnd();
}

void PosDiagram::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	pogWoResTableDlg->Activate(POSDIA, false);
	
	
}

void PosDiagram::OnCheckAcOnground() 
{
	// TODO: Add your control notification handler code here
	if ( ((CButton*)omDialogBar.GetDlgItem(IDC_CHECK_AC_ONGROUND))->GetCheck() )
		bmWithAcOnGround = true;
	else
		bmWithAcOnGround = false;
	
	bgShowOnGround = bmWithAcOnGround;
	
	ViewSelChange();
	
}

void PosDiagram::OnExpand()
{
	CString olStev;
	omViewer.GetStev(olStev);
	
	
	long llMinutes = omDuration.GetTotalMinutes();
	
	CTime olRefDate = omStartTime + CTimeSpan(0,0, llMinutes / 2  ,0);
	
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	
	CCAExpandDlg olDlg (this, SUB_MOD_PST, olStev, olRefDate, olFrom, olTo);
	olDlg.DoModal(); 
	
}

bool PosDiagram::GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights)
{
	return omViewer.GetLoadedFlights(opFlights);
}

void PosDiagram::SetFocusToDiagram()
{
	//PRF 8363, Change the focus to gantt chart //
	if (omChartArray.GetSize() > 0)
	{
		PosGantt *polPosGantt = ((PosChart *)omChartArray[0])-> GetGanttPtr();
		CWnd* polTopWindow = CWnd::GetActiveWindow();
		CWnd* polParent = ((PosChart *)omChartArray[0])->GetParent()->GetParent();
		//if(polWroGantt != NULL)
		if(polTopWindow != NULL && polParent != NULL)
		{		
			if(polParent->m_hWnd != NULL && polTopWindow->m_hWnd == polParent->m_hWnd)
				polPosGantt->SetFocus();
		}
	}
}


void PosDiagram::OnSpecialConflict()
{
	if(pogSpecialConflictDlg != NULL)
		pogSpecialConflictDlg->ShowDialog();
}
