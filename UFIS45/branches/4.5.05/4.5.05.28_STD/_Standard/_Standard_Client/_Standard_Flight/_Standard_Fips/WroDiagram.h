#if !defined(AFX_WRODIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
#define AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// WroDiagram.h : header file
//

#include <WroDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
//#include <WroChart.h>
 
/////////////////////////////////////////////////////////////////////////////
// WroDiagram frame

enum 
{
	WRO_ZEITRAUM_PAGE,
	WRO_FLUGSUCHEN_PAGE,
	WRO_GEOMETRIE_PAGE
};

class WroChart;

class WroDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(WroDiagram)
public:
	WroDiagram();           // protected constructor used by dynamic creation
	~WroDiagram();

	CDialogBar omDialogBar;


	void GetCurView( char * pcpView);
	void ViewSelChange(char *pcpView);

    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
	bool ShowFlight(long lpUrno);
	void UpdateWoResButton();
	void UpdateChangesButton(CString& opText, COLORREF opColor);
	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);
	void SaveToReg();
	void ActivateTimer();
	void DeActivateTimer();
	void ShowTime(const CTime &ropTime);
	void SetFocusToDiagram(); //PRF 8363 
// Attributes
public:

	WroDiagramViewer omViewer;

	CString omOldWhere;
	bool LoadFlights(const char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
	CCSButtonCtrl m_CB_Offline;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;

	CCSButtonCtrl m_CB_WoRes;
	CCSButtonCtrl m_CB_Changes;

// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;
	bool bmRepaintAll;
	void RedisplayAll();
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);


private:

	bool UpdateDia();
	void ResetStateVariables();

	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;

	WroChart *pomChart;
	BltOverviewTableDlg *pomBltOverviewTableDlg;
	CString m_key;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WroDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(WroDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWoRes();
	afx_msg void OnBeenden();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnOffline();
	afx_msg void OnInsert();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnExpand();
	afx_msg void OnPrint();
	afx_msg void OnChanges();
	afx_msg void OnAllocate();
	afx_msg void OnOverview();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
