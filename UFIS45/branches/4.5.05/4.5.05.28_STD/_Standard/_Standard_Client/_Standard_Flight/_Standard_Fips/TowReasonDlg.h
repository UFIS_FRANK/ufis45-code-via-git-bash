#ifndef AFX_TOWREASONDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_
#define AFX_TOWREASONDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_

// TowReasonDlg.h : Header-Datei
//
#include <resrc1.h>
#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CTowReasonDlg 

class CTowReasonDlg : public CDialog
{
// Konstruktion
public:
	CTowReasonDlg(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CTowReasonDlg)
	enum { IDD = IDD_ROTGROUND_REASON };
	CButton m_CRB_Posa;
	CButton m_CRB_Posd;
 	CButton m_CRB_Posad;
	//}}AFX_DATA

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CTowReasonDlg)
	CString m_oSetReasonFlag;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL


	bool FillFields(CString opRegn);
	void SaveToReg();


// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CTowReasonDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_TOWREASONDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_
