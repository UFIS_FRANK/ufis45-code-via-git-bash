#ifndef _CCFGD_H_
#define _CCFGD_H_

#include <CCSCedadata.h>
#include <CCSPtrArray.h>
#include <CCSTime.h>




#define MON_COUNT_STRING			"MONITORCOUNT"
#define MON_SEASONSCHEDULE_STRING	"SEASONSCHEDULE"
#define MON_DAILYSCHEDULE_STRING	"DAILYSCHEDULE"
#define MON_SEASONROTDLG_STRING	    "SEASONROTDLG"
#define MON_SEASONBATCH_STRING	    "SEASONBATCH"
#define MON_DAILYROTDLG_STRING	    "DAILYROTDLG"
#define MON_FLIGHTDIA_STRING	    "FLIGHTDIA"
#define MON_CCADIA_STRING			"CCADIA"
#define MON_CHADIA_STRING			"CHADIA"





/////////////////////////////////////////////////////////////////////////////
// Record structure declaration




struct RAW_VIEWDATA
{
	char Ckey[40];
	char Name[100];
	char Type[40];
	char Page[40];
	char Values[2001];
	RAW_VIEWDATA()
	{ memset(this,'\0',sizeof(*this));}
};
struct VIEW_TEXTDATA
{
	CString Page;							// e.g. Rank, Pool, Shift
	CCSPtrArray <CString> omValues;					// e.g. Page=Shift: F1,F50,N1 etc.
};

struct VIEW_TYPEDATA
{
	CString Type;							// e.g. Filter, Group, Sort
	CCSPtrArray <VIEW_TEXTDATA> omTextData; // necessary only with filters
	CCSPtrArray<CString> omValues;					// values only relevant if Type != Filter
};
struct VIEW_VIEWNAMES
{
	CString ViewName;						// e.g. <Default>, Heute, Test etc.
	CCSPtrArray<VIEW_TYPEDATA> omTypeData;
};
struct VIEWDATA
{
	CString Ckey;							// e.g. Staffdia, CCI-Chart etc.
	CCSPtrArray<VIEW_VIEWNAMES> omNameData;
};
//struct CfgDataStruct {
struct CFGDATA {
    // Data fields from table CFGCKI for whatif-rows
    long    Urno;           // Unique Record Number of CFGCKI
    char    Appn[34];       // name of application
    char    Ctyp[34];       // Type of Row; in Whaif constant string "WHAT-IF"
	char    Ckey[34];		// Name of what-if row
    CTime	Vafr;           // Valid from
    CTime	Vato;           // Valid to
	char	Text[2001];		// Parameter string
			// 050311 MVy: Handling Agents, search for 2001 hardcoded in SetupDlg::OnOK()
			// 050323 MVy: Priority Colors, search for 2001 hardcoded in CedaCfgData::SetConflictData()
	char	Pkno[34];		// Staff-/User-ID
    int	    IsChanged;		// Is changed flag

	CFGDATA(void) 
	{ memset(this,'\0',sizeof(*this));
	  strcpy(Appn,"CCS_FPMS");
	  Vafr = CTime((time_t)-1);
	  Vato = CTime((time_t)-1);
	  Urno=0;
	  IsChanged=-1;}

};	

struct CONFLICTDATA	{
	int		imIndex;
	int		imType;
	CString	omName;
	int		imDuration;
	bool	bmGeneralDesktop;
	bool	bmAircraftPositions;
	bool	bmGates;
	bool	bmCheckinCounters;
	bool	bmBaggageBelts;
	bool	bmLounges;
	bool	bmFlightDia;
	COLORREF clrPriority ;		// 050303 MVy: PRF6981: conflict priority
	int	     imPriority;

	CONFLICTDATA()
	{
		imIndex = -1;
		imType = -1;
		imDuration = 0;
		bmGeneralDesktop = false;
		bmAircraftPositions = false;
		bmGates = false;
		bmCheckinCounters = false;
		bmBaggageBelts = false;
		bmLounges = false;
		bmFlightDia = false;
		clrPriority = /*WHITE*/RGB( 255,255,255 );		// 050303 MVy: PRF6981: conflict priority, default color
		imPriority = 50;
	}

	bool operator == (const CONFLICTDATA& rhs)
	{
		bool blResult = imType == rhs.imType;
		blResult = blResult && imIndex == rhs.imIndex;
		blResult = blResult && omName == rhs.omName;
		blResult = blResult && imDuration == rhs.imDuration;
		blResult = blResult && bmGeneralDesktop == rhs.bmGeneralDesktop;
		blResult = blResult && bmAircraftPositions == rhs.bmAircraftPositions;
		blResult = blResult && bmGates == rhs.bmGates;
		blResult = blResult && bmCheckinCounters == rhs.bmCheckinCounters;
		blResult = blResult && bmBaggageBelts == rhs.bmBaggageBelts;
		blResult = blResult && bmLounges == rhs.bmLounges;
		blResult = blResult && bmFlightDia == rhs.bmFlightDia;
		blResult = blResult && clrPriority == rhs.clrPriority ;		// 050303 MVy: PRF6981: conflict priority, otherwise changed data will not be recognized
		blResult = blResult && imPriority == rhs.imPriority;
		return blResult;
	}

	bool operator != (const CONFLICTDATA& rhs)
	{
		return !(*this == rhs);
	}
};

struct USERSETUPDATA
{
	CString MONS;	// Monitos:					1|2|3
	CString RESO;	// Resolution:				800x600|1024x768|1280y1024
	CString GACH;   // Gatechart monitor pos.:	L|M|R
	CString STCH;	// Staffchart monitor pos.:	L|M|R
	CString CCCH;	// CCI-Chart monitor pos.:	L|M|R
	CString FPTB;	// Flightplan monitor pos.:	L|M|R
	CString STTB;	// Stafftable monitor pos.:	L|M|R
	CString GATB;   // Gatetable monitor pos.:	L|M|R
	CString CCTB;   // CCI-Table monitor pos.:	L|M|R
	CString RQTB;   // Requests monitor pos.:	L|M|R
	CString PKTB;	// Peaktable monitor pos.:	L|M|R
	CString CVTB;	// Coverage monitor pos.:	L|M|R
	CString VPTB;	// Prplantable monitor pos.:L|M|R
	CString CFTB;	// Conflicts monitor pos.:	L|M|R
	CString ATTB;	// Info monitor pos.:		L|M|R
	CString INFO;	// Info monitor pos.:		L|M|R
	CString LHHS;	// LH-Host monitor pos.:	L|M|R
	CString TACK;   // Turnaroundbars			J|N
	CString FBCK;	// Flightbars without demands J|N
	CString SBCK;	// Shadowbars				J|N
	CString STCV;   // View: Staffchart
	CString GACV;   // View: Gatechart
	CString CCCV;   // View: CCI-Chart
	CString STLV;   // View: Stafftable
	CString FPLV;   // View: Flighttable
	CString GBLV;   // View: Gatetable
	CString CCBV;   // View: CCI-table
	CString RQSV;   // View: Requests
	CString PEAV;   // View: Peaktable
	CString VPLV;   // View: Preplantable
	CString CONV;   // View: Conflicts
	CString ATTV;   // View: "Achtung" (Attention)
	int     WARN;
	USERSETUPDATA(void)
	{
		MONS = "1";	RESO = "1024x768";
		GACH = "L";	STCH = "L";	CCCH = "L";	FPTB = "L";
		STTB = "L";	GATB = "L";	CCTB = "L";	RQTB = "L";
		PKTB = "L";	CVTB = "L";	VPTB = "L";	CFTB = "L";
		ATTB = "L";	INFO = "L";	LHHS = "L";	TACK = "J";
		FBCK = "J";	SBCK = "J";
		STCV = "<Default>";	GACV = "<Default>";
		CCCV = "<Default>";	STLV = "<Default>";
		FPLV = "<Default>";	GBLV = "<Default>";
		CCBV = "<Default>";	RQSV = "<Default>";
		PEAV = "<Default>";	VPLV = "<Default>";
		CONV = "<Default>";	ATTV = "<Default>";
		WARN = 0;
	}
};

typedef struct CFGDATA SETUPDATA;
//typedef struct CfgDataStruct CFGDATA;

// the broadcast CallBack function, has to be outside the CedaCfgData class
void ProcessCfgCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCfgData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<CFGDATA> omData;
    CCSPtrArray<CFGDATA> omSetupData;
	CCSPtrArray<VIEWDATA> omViews;
    CMapStringToPtr omCkeyMap;
    CMapPtrToPtr    omUrnoMap;
    CMapPtrToPtr    omSetupUrnoMap;
	USERSETUPDATA   rmUserSetup;
	CFGDATA			rmMonitorSetup;
	CFGDATA			rmFontSetup;
	CFGDATA			rmPopsReportDailyAlcSetup;
	CFGDATA			rmConflictSetup;
	CFGDATA			rmGateBufferTimeSetup;
	CFGDATA			rmPosBufferTimeSetup;
	CFGDATA			rmArchivePastBufferTimeSetup;
	CFGDATA			rmArchiveFutureBufferTimeSetup;
	CFGDATA			rmArchivePostFlightTodayOnly ;		// 050309 MVy: today only or period
	CFGDATA			rmHandlingAgent ;		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
	CFGDATA			rmResChainDlg;
	CFGDATA			rmXDaysSetup;
	CFGDATA			rmTimelineBuffer;
	CFGDATA			rmXminutesBufferTimeSetup; 
	CFGDATA		    rmConfPrioSetup;
	CFGDATA			rmWingoverBuffer;
	
	CCSPtrArray<CFGDATA> omBarColorData;
	CMapPtrToPtr         omBarUrnoMap;
	
	// this is the name of a dummy user for default values
	static const char*	pomDefaultName;
	
//	static const int	NUMCONFLICTS;
	int	NUMCONFLICTS;
// Operations
public:
    CedaCfgData();
    //@ManMemo: Destructor, Unregisters the CedaCfgData object from DataDistribution
	~CedaCfgData();

	
	char pcmListOfFields[2048];
	//@ManMemo: Create the Request for What-If
	//bool CreateCfgRequest(const CFGDATA *prpCfgData);
    //@ManMemo: Read all Cfg from database at program start
	bool ReadCfgData();
	//@ManMemo: Delete a Cfg
	bool ChangeCfgData(CFGDATA *prpCfg);
	//@ManMemo: Delete the Cfg
	bool DeleteCfg(long lpUrno);
	//@ManMemo: Adds a Cfg to omData and to the Maps
	bool AddCfg(CFGDATA *prpCfg);
	//@ManMemo: Makes Database-Actions Insert/Update/Delete
	bool SaveCfg(CFGDATA *prpCfg);	
    //@ManMemo: Prepare the data, not used for the moment
	void PrepareCfgData(CFGDATA *prpCfg);
	//@ManMemo: Insert staff data (const CFGDATA *prpCfgData);    // used in PrePlanTable only

	int GetMonitorForWindow(CString opWindow);
	int GetMonitorCount();
	bool ReadMonitorSetup();
	CString GetLkey();


	bool ReadFontSetup();
	bool ReadPopsReportDailyAlcSetup();
	CString GetPopsReportDailyAlcSetup();


	BOOL InterpretSetupString(CString popSetupString, USERSETUPDATA *prpSetupData);
    //@ManMemo: Update cfg data 
    bool UpdateCfg(const CFGDATA *prpCfgData);    // used in PrePlanTable only
	void ProcessCfgBc(enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool InsertCfg(CFGDATA *prpCfgData);
	long  GetUrnoById(char *pclWiid);
	BOOL  GetIdByUrno(long lpUrno,char *pcpWiid);

	CFGDATA *GetBarColorCfgByUrno(long lpUrno);
	bool UpdateBarColorCfgInternal(CFGDATA *prpCfg);
	bool InsertBarColorCfgInternal(CFGDATA *prpCfg);
	bool DeleteBarColorCfgInternal(CFGDATA *prpCfg);

	CFGDATA  * GetCfgByUrno(long lpUrno);
	void SetCfgData();
	void MakeCurrentUser();
	void ClearAllViews();
	void PrepareViewData(CFGDATA *prpCfg);
	VIEWDATA * FindViewData(CFGDATA *prlCfg);
	VIEW_VIEWNAMES * FindViewNameData(RAW_VIEWDATA *prpRawData);
	VIEW_TYPEDATA * FindViewTypeData(RAW_VIEWDATA *prpRawData);
	VIEW_TEXTDATA * FindViewTextData(RAW_VIEWDATA *prpRawData);
	BOOL MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA * prpRawData);
	void MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa = "|");
	void UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName);
	void DeleteViewFromDiagram(CString opDiagram, CString olView);
	//bool ReadConflicts(CStringArray &opLines);
	//bool SaveConflictData(CFGDATA *prpCfg);
	bool GetFlightConfig();


	bool ReadConflictSetup();
	CString GetConflictSetup();

	bool ReadGateBufferTimeSetup();
	CString GetGateBufferTimeSetup();

	bool ReadPosBufferTimeSetup();
	CString GetPosBufferTimeSetup();

	bool ReadXminutesBufferTimeSetup();//PRF 8379 
	CString GetXminutes();  

	bool ReadArchivePastBufferTimeSetup();
	CString GetArchivePastBufferTimeSetup();

	bool ReadArchiveFutureBufferTimeSetup();
	CString GetArchiveFutureBufferTimeSetup();

	bool ReadArchivePostFlightTodayOnly();		// 050309 MVy: today only or period, read from DB
	CString GetArchivePostFlightTodayOnly();		// 050309 MVy: today only or period, get current state

	bool ReadHandlingAgent();		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, read from DB
	void CreateCfgStringForHandlingAgent();		// 050331 MVy: this function will create the string put into database
	//-OO-//CString GetHandlingAgent();		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, get current state
	//-OO-// handling global, global memebers AND function are not necessary
	int UpdateHandlingAgentAirlineCodes();		// 050318 MVy: if handling agent or handling task will change the depending airline codes must be updated by calling this function

	bool ReadTimelineBufferSetup();
	CString GetTimelineBufferSetup();
	bool ReadWingoverlapBufferSetup();
	CString GetWingoverlapBufferSetup();


private:
	bool ReadConfigItem( char* pcDBConfigItemName, CFGDATA& refCfgData, bool bPKNO = true );		// 050311 MVy: helper function to get configuration strings from db

public:
	bool ReadXDaysSetup();
	CString GetXDaysSetup();

	bool ReadConflicts(const CString& opUserName,CStringArray &opLines);
	void CfgRecordFromConflict(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData);
	void GetDefaultConflictSetup(CStringArray &opLines);
	bool ResetToDefault(void);
	bool SaveConflictData(const CString& ropName,CFGDATA *prpCfg);

	void ReadAllConfigData(void);
	int	 GetConflictData(CCSPtrArray<CONFLICTDATA>& ropArray);
	CONFLICTDATA *GetConflictDataById(int ipId);
	CONFLICTDATA *GetConflictDataByType( int iType );		// 050303 MVy: find a globally stored conflict by its type
	int GetConflictPrioByType(int ipId);


	bool CheckKonfliktForDisplay(int ipId, int ipDisplayType);
	CFGDATA MakeConflictData( int ipIndex, const CONFLICTDATA& ropData, CString pcUserId );		// 050506 MVy: create configuration
	CONFLICTDATA* NewConflictData( CString &olRecord );		// 050506 MVy: create configuration; Attention: this function allocates data by a NEW !!!
	BOOL SetConflictData(int ipIndex,const CONFLICTDATA& ropData,bool blSaveDiffOnly = false);		
	bool ResetDefaultConflicts();

	bool ReadResChainDlgSetup();
	CString GetResChainDlgSetup();

	bool ReadNatureBarColorSetup();
	CFGDATA* GetBarColorCfgByNature(CString lpNature,CString pkno);
	CBrush* GetNatureBarColor(CString ttyp);
	COLORREF GetNatureTextColor(CString ttyp);
	COLORREF HexToRGB(CString shex);
	BOOL HEX_2_DEC( const CString& csHex, ULONG& ulDec );
	CBrush* GetDefaultNatureBarColor();
	COLORREF GetDefaultNatureTextColor();
private:
    BOOL CfgExist(long lpUrno);
    bool InsertCfgRecord(CFGDATA *prpCfgData);
    bool UpdateCfgRecord(const CFGDATA *prpCfgData);

	CCSPtrArray<CONFLICTDATA>	omConflictData;
	CMapWordToPtr				omConflictMap;

};

extern CedaCfgData ogCfgData;
#endif
