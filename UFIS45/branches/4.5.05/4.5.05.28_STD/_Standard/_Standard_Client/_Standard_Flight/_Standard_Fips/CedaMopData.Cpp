// CedaMOPData.cpp
 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaMOPData.h>
#include <BasicData.h>



CedaMOPData ogCedaMOPData;

CedaMOPData::CedaMOPData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for MOPData
	BEGIN_CEDARECINFO(MOPDATA,MOPDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Rurn,"RURN")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Valu,"VALU")
		FIELD_CHAR_TRIM	(Ctyp,"CTYP")
		FIELD_CHAR_TRIM	(Avil,"AVIL")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Cref,"CREF")
	
	END_CEDARECINFO //(MOPDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(MOPDataRecInfo)/sizeof(MOPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&MOPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"MOP");
	strcpy(pcmMOPFieldList,"URNO,CDAT,USEC,LSTU,USEU,RURN,VALU,CTYP,HOPO,AVIL,CREF");
	
	
	pcmFieldList = pcmMOPFieldList;
	omData.SetSize(0,1000);
	omAData.SetSize(0,1000);
	omDData.SetSize(0,1000);
}

void CedaMOPData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");	ropType.Add("String");
	ropFields.Add("USEC");	ropType.Add("String");
	ropFields.Add("USEU");	ropType.Add("String");
	ropFields.Add("CDAT");	ropType.Add("Date");
	ropFields.Add("LSTU");	ropType.Add("Date");
	ropFields.Add("HOPO");	ropType.Add("String");
	ropFields.Add("RURN");	ropType.Add("String");
	ropFields.Add("VALU");	ropType.Add("String");
	ropFields.Add("CTYP");	ropType.Add("String");
	ropFields.Add("AVIL");	ropType.Add("String");
	ropFields.Add("CREF");	ropType.Add("String");

	/*
	ropDesription.Add(LoadStg(IDS_STRING439));
	ropDesription.Add(LoadStg(IDS_STRING247));
	ropDesription.Add(LoadStg(IDS_STRING246));
	ropDesription.Add(LoadStg(IDS_STRING440));
	ropDesription.Add(LoadStg(IDS_STRING441));
	ropDesription.Add(LoadStg(IDS_STRING442));
	ropDesription.Add(LoadStg(IDS_STRING245));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING249));
	ropDesription.Add(LoadStg(IDS_STRING248));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING443));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING444));
	ropDesription.Add(LoadStg(IDS_STRING624));*/
}

//---------------------------------------------------------------------------------------------------------
/*
void CedaMOPData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACT_CHANGE,CString("MOPData"), CString("ACT-changed"),ProcessACTCf);
	ogDdx.Register((void *)this,BC_ACT_DELETE,CString("MOPData"), CString("ACT-deleted"),ProcessACTCf);
}
*/
//--~CEDADATA----------------------------------------------------------------------------------------------

CedaMOPData::~CedaMOPData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaMOPData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaMOPData::Read(char *pspWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere != NULL)
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pspWhere);
	}
	else
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	}
	
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		MOPDATA *prpMOP = new MOPDATA;
		if ((ilRc = GetFirstBufferRecord(prpMOP)) == true)
		{
			prpMOP->IsChanged = DATA_UNCHANGED;

		//	CutZeros(prpMOP);

			//omData.Add(prpMOP);//Update omData
			//omUrnoMap.SetAt((void *)prpMOP->Urno,prpMOP);
		}
		else
		{
			delete prpMOP;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaMOPData::InsertMOP(MOPDATA *prpMOP,BOOL bpSendDdx)
{
	prpMOP->IsChanged = DATA_NEW;
//	AddZeros(prpMOP);
	if(SaveMOP(prpMOP) == false) return false; //Update Database
	//InsertMOPInternal(prpMOP);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaMOPData::InsertMOPInternal(MOPDATA *prpMOP,CString ADID)
{
	//CutZeros(prpMOP);
	//ogDdx.DataChanged((void *)this, ACT_CHANGE,(void *)prpMOP ); //Update Viewer

	if(ADID=="A")
	{
		omAData.Add(prpMOP);
	}
	else
	{
		omDData.Add(prpMOP);
	}
	//omData.Add(prpMOP);//Update omData
	//omUrnoMap.SetAt((void *)prpMOP->Urno,prpMOP);	
    return true;
}

void CedaMOPData::InsertMOPInternalData(char* pclData,CString ADID)
{
	//CutZeros(prpMOP);
	//ogDdx.DataChanged((void *)this, ACT_CHANGE,(void *)prpMOP ); //Update Viewer

	if(ADID=="A")
	{
		strcpy(m_pclAData,pclData);
	}
	else
	{
		strcpy(m_pclDData,pclData);
	}
	//omData.Add(prpMOP);//Update omData
	//omUrnoMap.SetAt((void *)prpMOP->Urno,prpMOP);	
   
}

bool CedaMOPData::SaveMOPInternal(long flightUrno,CString ADID)
{


	CString tmp ="";
	tmp.Format("%ld",flightUrno);

	if(ADID=="A")
	{
		int ilACTCount = omAData.GetSize();
		for (int ilLc = 0; ilLc < ilACTCount; ilLc++)
		{
			
			strcpy(omAData[ilLc].Rurn,tmp);
			SaveMOP(&omAData[ilLc]);
		}

		omAData.DeleteAll();
	}
	else
	{
		
		int ilACTCount = omDData.GetSize();
		for (int ilLc = 0; ilLc < ilACTCount; ilLc++)
		{
			strcpy(omDData[ilLc].Rurn,tmp);
			SaveMOP(&omDData[ilLc]);
		}
		omDData.DeleteAll();
	}
	
    return true;
}


//--DELETE-------------------------------------------------------------------------------------------------

bool CedaMOPData::DeleteMOP(long lpUrno)
{
	MOPDATA *prlACT = GetMOPByUrno(lpUrno);
	if (prlACT != NULL)
	{
		prlACT->IsChanged = DATA_DELETED;
		if(SaveMOP(prlACT) == false) return false; //Update Database
		DeleteMOPInternal(prlACT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaMOPData::DeleteMOPInternal(MOPDATA *prpMOP)
{
	ogDdx.DataChanged((void *)this,ACT_DELETE,(void *)prpMOP); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpMOP->Urno);
	int ilACTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilACTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpMOP->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaMOPData::PrepareMOPData(MOPDATA *prpMOP)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaMOPData::UpdateMOP(MOPDATA *prpMOP,BOOL bpSendDdx)
{
	/*
	if (GetMOPByUrno(prpMOP->Urno) != NULL)
	{
		if (prpMOP->IsChanged == DATA_UNCHANGED)
		{
			prpMOP->IsChanged = DATA_CHANGED;
		}
		//AddZeros(prpMOP);
		if(SaveMOP(prpMOP) == false) return false; //Update Database
		UpdateMOPInternal(prpMOP);
	}*/

	if(SaveMOP(prpMOP) == false) return false; //Update Database
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaMOPData::UpdateMOPInternal(MOPDATA *prpMOP)
{
	MOPDATA *prlMOP = GetMOPByUrno(prpMOP->Urno);
	if (prlMOP != NULL)
	{
//		CutZeros(prpMOP);
		*prlMOP = *prpMOP; //Update omData
		//ogDdx.DataChanged((void *)this,ACT_CHANGE,(void *)prlACT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

MOPDATA *CedaMOPData::GetMOPByUrno(long lpUrno)
{
	MOPDATA  *prlACT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlACT) == TRUE)
	{
		return prlACT;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaMOPData::ReadSpecialData(CCSPtrArray<MOPDATA> *popAct,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","MOP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","MOP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAct != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			MOPDATA *prpMOP = new MOPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpMOP,CString(pclFieldList))) == true)
			{
				popAct->Add(prpMOP);
			}
			else
			{
				delete prpMOP;
			}
		}
		if(popAct->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaMOPData::SaveMOP(MOPDATA *prpMOP)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpMOP->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpMOP->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMOP);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpMOP->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMOP->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMOP);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpMOP->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMOP->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}



//Prf: 8795
//--ValidateACTBcData--------------------------------------------------------------------------------------

bool CedaMOPData::ValidateMOPBcData(const long& lrpUrno)
{
	bool blValidateACTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<MOPDATA> olActs;
		/*
		if(!ReadSpecialData(&olActs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateACTBcData = false;
		}
		*/
	}
	return blValidateACTBcData;
}



bool CedaMOPData::ClearData(CString ADID)
{
	if(ADID=="A")
	{
		omAData.DeleteAll();
		strcpy(m_pclAData,""); 
	}
	else
	{
		omDData.DeleteAll();
		strcpy(m_pclDData,"");
	}

	return true;
}



char* CedaMOPData::GetMOPInternalData(CString ADID)
{

	if(ADID=="A")
	{
		return m_pclAData;
		//strcpy(tmp,m_pclAData); 
	}
	else
	{
		return m_pclDData;
		//strcpy(tmp,m_pclDData);
	}

//	return tmp;
}