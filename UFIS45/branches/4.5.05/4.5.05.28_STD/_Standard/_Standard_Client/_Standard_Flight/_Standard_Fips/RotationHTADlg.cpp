// RotationHTADlg.cpp : implementation file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt
//	171100	rkr	Dialog unabhängig von Datenstruktur

#include <stdafx.h>
#include <fpms.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <RotationDlgCedaFlightData.h>
#include <ButtonListDlg.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <RotationHTADlg.h>
#include <AskBox.h>
#include <HTADlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationHTADlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, 
							 CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationHTADlg dialog

RotationHTADlg::RotationHTADlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
	           :CDialog(RotationHTADlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationHTADlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ogDdx.Register(this, BCD_HTA_UPDATE, CString("ROTATIONDLG_HAI"), CString("Hai Update"), RotationHTADlgCf);
	ogDdx.Register(this, BCD_HTA_DELETE, CString("ROTATIONDLG_HAI"), CString("Hai Delete"), RotationHTADlgCf);
	ogDdx.Register(this, BCD_HTA_INSERT, CString("ROTATIONDLG_HAI"), CString("Hai Insert"), RotationHTADlgCf);

	pomParent = pParent;

	prmAFlight = prpAFlight;
	prmDFlight = prpDFlight;

	pomATable = new CCSTable;
	pomDTable = new CCSTable;


}


RotationHTADlg::~RotationHTADlg()
{

	ogDdx.UnRegister(this, NOTUSED);

	delete pomATable;

}


void RotationHTADlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationHTADlg)
	DDX_Control(pDX, IDC_GMBORDER_ARR, m_CS_GMBorder_Arr);
	DDX_Control(pDX, IDC_GMBORDER_DEP, m_CS_GMBorder_Dep);
	DDX_Control(pDX, IDC_DELETE_ARR, m_CB_Delete_Arr);
	DDX_Control(pDX, IDC_DELETE_DEP, m_CB_Delete_Dep);
	DDX_Control(pDX, IDC_INSERT_DEP, m_CB_Insert_Dep);
	DDX_Control(pDX, IDC_INSERT_ARR, m_CB_Insert_Arr);
	DDX_Control(pDX, IDC_UPDATE_ARR, m_CB_Update_Arr);
	DDX_Control(pDX, IDC_UPDATE_DEP, m_CB_Update_Dep);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationHTADlg, CDialog)
	//{{AFX_MSG_MAP(RotationHTADlg)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_DELETE_ARR, OnDeleteArr)
	ON_BN_CLICKED(IDC_DELETE_DEP, OnDeleteDep)
	ON_BN_CLICKED(IDC_INSERT_ARR, OnInsertArr)
	ON_BN_CLICKED(IDC_INSERT_DEP, OnInsertDep)
	ON_BN_CLICKED(IDC_UPDATE_ARR, OnUpdateArr)
	ON_BN_CLICKED(IDC_UPDATE_DEP, OnUpdateDep)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationHTADlg message handlers
BOOL RotationHTADlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_CLOSE,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	InitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void RotationHTADlg::InitDialog()
{
	long lpAFlightUrno = 0;
	long lpDFlightUrno = 0;
	
	if(prmAFlight != NULL)
		lpAFlightUrno = prmAFlight->Urno;

	if(prmDFlight != NULL)
		lpDFlightUrno = prmDFlight->Urno;


	ogHtaData.Read(lpAFlightUrno, lpDFlightUrno);

	CString olCaption("Assigned Handling Agent/Types for Flight   ");


	if(prmAFlight != NULL && prmDFlight != NULL)
	{
		olCaption += CString(prmAFlight->Flno ) + CString(" / ") + CString(prmDFlight->Flno );
	}
	else
	{
		if(prmAFlight != NULL )
		{
			olCaption += CString(prmAFlight->Flno ) + CString(" / ") + CString("---");
		}
		else
		{
			olCaption +=  CString("---")+ CString(" / ") + CString(prmDFlight->Flno ) ;
		}

	}

	this->SetWindowText(olCaption);


	m_CB_Update_Dep.SetSecState( ogPrivList.GetStat("HTA_CHANGE"));		
	m_CB_Update_Arr.SetSecState( ogPrivList.GetStat("HTA_CHANGE"));		

	m_CB_Delete_Arr.SetSecState( ogPrivList.GetStat("HTA_DELETE"));		
	m_CB_Delete_Dep.SetSecState( ogPrivList.GetStat("HTA_DELETE"));		

	m_CB_Insert_Arr.SetSecState( ogPrivList.GetStat("HTA_INSERT"));		
	m_CB_Insert_Dep.SetSecState( ogPrivList.GetStat("HTA_INSERT"));		


	InitTable();

	FillTable();

}

void RotationHTADlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomATable != NULL && ::IsWindow(this->pomATable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder_Arr.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomATable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomATable->DisplayTable();
	}

	if (this->pomDTable != NULL && ::IsWindow(this->pomDTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder_Dep.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomDTable->DisplayTable();
	}

	this->Invalidate();
}

void RotationHTADlg::OnClose() 
{
	CCS_TRY
	CDialog::OnCancel();
	CCS_CATCH_ALL
}


void RotationHTADlg::OnCancel()
{
	CDialog::OnCancel();
}


void RotationHTADlg::OnDeleteArr() 
{
	CCS_TRY

	int ilSel = (pomATable->GetCTableListBox())->GetCurSel();

	if(ilSel < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}

	if(MessageBox(GetString(ST_DATENSATZ_LOESCHEN), GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
	{

		if(ilSel < omArrHtas.GetSize() )	
		{
			ogHtaData.Delete(omArrHtas[ilSel]);
			InitDialog();
		}
	}

	CCS_CATCH_ALL
}

void RotationHTADlg::OnDeleteDep() 
{
	CCS_TRY

	int ilSel = (pomDTable->GetCTableListBox())->GetCurSel();

	if(ilSel < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}

	if(MessageBox(GetString(ST_DATENSATZ_LOESCHEN), GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
	{

		if(ilSel < omDepHtas.GetSize() )	
		{
			ogHtaData.Delete(omDepHtas[ilSel]);
			InitDialog();
		}
	}

	CCS_CATCH_ALL
}






void RotationHTADlg::OnInsertArr() 
{
	CCS_TRY

	if(prmAFlight != NULL)
	{


		HTADlg polDlg(this, NULL, prmAFlight);
		polDlg.DoModal();

		FillTable();
	}

	CCS_CATCH_ALL
}


void RotationHTADlg::OnInsertDep() 
{
	CCS_TRY
	
	if(prmDFlight != NULL)
	{

		HTADlg polDlg(this, NULL, prmDFlight);
		polDlg.DoModal();

		FillTable();

	}

	CCS_CATCH_ALL
}



void RotationHTADlg::OnUpdateArr() 
{
	CCS_TRY

	int ilSel = (pomATable->GetCTableListBox())->GetCurSel();

	if(ilSel < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}
	
	if (ilSel >= omArrHtas.GetSize())
		return;


	HTADATA *prlHta = ogHtaData.GetHta( omArrHtas[ilSel]);


	HTADlg polDlg(this, prlHta, prmAFlight);
	polDlg.DoModal();


	FillTable();

	CCS_CATCH_ALL

}


void RotationHTADlg::OnUpdateDep() 
{
	CCS_TRY

	int ilSel = (pomDTable->GetCTableListBox())->GetCurSel();

	if(ilSel < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}
	
	if (ilSel >= omDepHtas.GetSize())
		return;


	HTADATA *prlHta = ogHtaData.GetHta( omDepHtas[ilSel]);


	HTADlg polDlg(this, prlHta, prmDFlight);
	polDlg.DoModal();


	FillTable();

	CCS_CATCH_ALL

}





LONG RotationHTADlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{

	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;


	if(ogPrivList.GetStat("HTA_CHANGE") == '1')		
	{


		if(olNotify.SourceTable == pomATable)
		{
			OnUpdateArr();
		}

		if(olNotify.SourceTable == pomDTable)
		{
			OnUpdateDep();
		}
	}

	return 0L;

}


void RotationHTADlg::InitTable()
{
	// TODO: Add your control notification handler code here
	CCS_TRY

	CRect olRectBorder;
	m_CS_GMBorder_Arr.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomATable->SetHeaderSpacing(0);
    pomATable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomATable->SetSelectMode(0);
	pomATable->SetShowSelection(true);
	pomATable->ResetContent();


	m_CS_GMBorder_Dep.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomDTable->SetHeaderSpacing(0);
    pomDTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomDTable->SetSelectMode(0);
	pomDTable->SetShowSelection(true);
	pomDTable->ResetContent();


	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// Handling Agent Code
	rlHeader.Length = 20;
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// Handling Agent Name
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// Start Date Time
	rlHeader.Length = 8;
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// End Date Time
	rlHeader.Length = 8;
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// Handling Type Code
	rlHeader.Length = 20; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// Handling Type Name
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	// Remark
	rlHeader.Length = 120; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	// Flag
	rlHeader.Length = 20; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);

	pomATable->SetHeaderFields(omHeaderDataArray);
	pomATable->SetDefaultSeparator();
	pomATable->SetTableEditable(false);
	pomATable->DisplayTable();

	pomDTable->SetHeaderFields(omHeaderDataArray);
	pomDTable->SetDefaultSeparator();
	pomDTable->SetTableEditable(false);
	pomDTable->DisplayTable();

	omHeaderDataArray.DeleteAll();

	CCS_CATCH_ALL
	
}


void RotationHTADlg::DrawHeader()
{
	CCS_TRY

	int  ilTotalLines = 0;
	int  ilColNo = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[8];
	int ilFontFactor=9;

	//Handling Agent code
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 5*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = GetString(IDS_STRING2017); // code
	//Handling Agent name
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 20*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = GetString(IDS_STRING2018); // Handling agent

	//Start Date Time
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 10*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = "Start"; 

	//End Date Time
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 10*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = "End"; 

	//Handling type code
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 5*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = GetString(IDS_STRING2017); // Code

	//Handling type name
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 20*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	//prlHeader[ilColNo]->Text = GetString(IDS_STRING2666); // Handling type
	prlHeader[ilColNo]->Text = "Services"; // Handling type

	// Remark
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 60*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = GetString(ST_EQU_REMA);

	// Flag
	ilColNo++;
	prlHeader[ilColNo] = new TABLE_HEADER_COLUMN;
	prlHeader[ilColNo]->Alignment = COLALIGN_CENTER;
	prlHeader[ilColNo]->Length = 2*ilFontFactor;
	prlHeader[ilColNo]->Font = &ogCourier_Bold_10;
	prlHeader[ilColNo]->Text = "F";

	ilColNo++;

	for(int ili = 0; ili < ilColNo; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomATable->SetHeaderFields(omHeaderDataArray);
	pomDTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomATable->SetDefaultSeparator();
	pomDTable->SetDefaultSeparator();

	CCS_CATCH_ALL
	
}





static void RotationHTADlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, 
							 CString &ropInstanceName)
{

    RotationHTADlg *polDlg = (RotationHTADlg *)popInstance;


	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}












void RotationHTADlg::FillTable()
{

	CCS_TRY

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;
	
	//recordset von allen agents der fluggesellschaft
	CCSPtrArray<RecordSet>rlHais;

	//ogBCD.GetRecords("HAI", "ALTU", olUrnoStr, &rlHais);

	rlColumnData.Alignment = COLALIGN_LEFT;

	omArrHtas.RemoveAll();
	omDepHtas.RemoveAll();


	CCSPtrArray<HTADATA> olData;

	HTADATA *prlHta;


	pomATable->ResetContent();
	pomDTable->ResetContent();

	CTime olTime;
	CTime olFlSchDt;

	if(prmAFlight !=  NULL)
	{

		int ilCount = ogHtaData.GetHtaArray( prmAFlight->Urno, &olData);
		olFlSchDt = prmAFlight->Stoa;

		for(int i = 0; i < ilCount ; i++)
		{
			prlHta = &olData[i];

			omArrHtas.Add(prlHta->Urno);

			//Handling agent code
			rlColumnData.Text = prlHta->Hsna;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			//Handling agent name
			rlColumnData.Text = "";
			ogBCD.GetField("HAG", "HSNA", prlHta->Hsna, "HNAM", rlColumnData.Text);			
			olColList.NewAt(olColList.GetSize(), rlColumnData);


			//Start Time
			olTime = prlHta->Stdt;
			rlColumnData.Text = DateToHourDivString(olTime, olFlSchDt);
			//rlColumnData.Text = prlHta->Stdt.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			//End Time
			olTime = prlHta->Endt;
			rlColumnData.Text = DateToHourDivString(olTime, olFlSchDt);
			//rlColumnData.Text = prlHta->Endt.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Handling type code
			rlColumnData.Text = prlHta->Htyp;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Handling type name
			rlColumnData.Text = "";
			ogBCD.GetField("HTY", "HTYP", prlHta->Htyp, "HNAM", rlColumnData.Text);			
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			rlColumnData.Text = CString(prlHta->Rema);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			rlColumnData.Text = prlHta->Flag;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomATable->AddTextLine(olColList, NULL);
			olColList.DeleteAll();
		}
		olData.RemoveAll();

		DrawHeader();
		pomATable->DisplayTable();

	}


	if(prmDFlight !=  NULL)
	{

		int ilCount = ogHtaData.GetHtaArray( prmDFlight->Urno, &olData);
		olFlSchDt = prmDFlight->Stod;

		for(int i = 0; i < ilCount ; i++)
		{
			prlHta = &olData[i];

			omDepHtas.Add(prlHta->Urno);

			//Handling agent code
			rlColumnData.Text = prlHta->Hsna;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			//Handling agent name
			rlColumnData.Text = "";
			ogBCD.GetField("HAG", "HSNA", prlHta->Hsna, "HNAM", rlColumnData.Text);			
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			//Start Time
			olTime = prlHta->Stdt;
			rlColumnData.Text = DateToHourDivString(olTime, olFlSchDt);
			//rlColumnData.Text = prlHta->Stdt.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			//End Time
			olTime = prlHta->Endt;
			rlColumnData.Text = DateToHourDivString(olTime, olFlSchDt);
			//rlColumnData.Text = prlHta->Endt.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Handling type code
			rlColumnData.Text = prlHta->Htyp;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Handling type name
			rlColumnData.Text = "";
			ogBCD.GetField("HTY", "HTYP", prlHta->Htyp, "HNAM", rlColumnData.Text);			
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = prlHta->Rema;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			rlColumnData.Text = prlHta->Flag;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			

			pomDTable->AddTextLine(olColList, NULL);
			olColList.DeleteAll();
		}
		olData.RemoveAll();

		DrawHeader();
		pomDTable->DisplayTable();

	}




	CCS_CATCH_ALL

}















