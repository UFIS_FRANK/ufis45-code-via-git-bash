
//PSSearchPosDlg.cpp : implementation file
//
//This Class is created by sisl for the support of multiple selection of 
//Positions and Positions Groups in the Flight Search Page.

#include "stdafx.h"
#include "fpms.h"
#include "PSSearchPosDlg.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "CCSBasicFunc.h"
#include "PrivList.h"
#include "afxtempl.h"
#include "AwBasicDataDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// PSSearchPosDlg dialog

PSSearchPosDlg::PSSearchPosDlg(CWnd* pParent, CString opObject, CString opFields, CString opSortFields, CString opSelStrings)
: AwBasicDataDlg(pParent,opObject,opFields,opSortFields,opSelStrings,IDD_PSSEARCH_POSITION ) //: CDialog(PSSearchPosDlg::IDD, pParent)
{
	omObject = opObject;
	omSortFields = opSortFields;
	omFields = opFields;
	pomRecord = NULL;

	imLastSortIndex = 0;

	omSelStrings = opSelStrings;

	cmSecState = '1';
	bmPosGrp = false;
		
	//{{AFX_DATA_INIT(AwBasicDataDlg)
	//}}AFX_DATA_INIT
}

PSSearchPosDlg::PSSearchPosDlg(CWnd * pParent, CString opObject, CString opFields, CString opSortFields, CString opSelStrings,CStringArray &opPosGrpstr,bool bpPosGrp)
: AwBasicDataDlg( pParent,opObject,opFields,opSortFields,opSelStrings,IDD_PSSEARCH_POSITION)// : CDialog(PSSearchPosDlg::IDD, pParent)
{
	omSelStrings = opSelStrings;
	omPosGrparr = &opPosGrpstr;
	bmPosGrp =bpPosGrp;

}
void PSSearchPosDlg::DoDataExchange(CDataExchange* pDX)
{
	AwBasicDataDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSSearchPosDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PSSearchPosDlg, AwBasicDataDlg)
	//{{AFX_MSG_MAP(PSSearchPosDlg)
	
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PSSearchPosDlg message handlers
BOOL PSSearchPosDlg::OnInitDialog() 
{
	AwBasicDataDlg::OnInitDialog();
	if(bmPosGrp==true)
	{
		for(int i=0;i<omPosGrparr->GetSize();i++)
		{
			m_CL_List.AddString(omPosGrparr->GetAt(i));
		

		}


		CStringArray olSelections;
		::ExtractItemList(omSelStrings,&olSelections,';');


		CString str;
		int li_Count = m_CL_List.GetCount();
			
		for( i =0;i<li_Count;i++)
		{
			m_CL_List.GetText(i,str);

			for(int j =0; j < olSelections.GetSize() ;j++)
			{

				if( olSelections.GetAt(j) == str )
					m_CL_List.SetSel(i);
			}

		}

		


		SetWindowText("Position Groups");
	}
	else
	{
		if (cmSecState != '1')
			m_CB_OK.EnableWindow(FALSE);
		
		
		CString olItem;	

		SetWindowText(ogBCD.GetObjectDesc(omObject));
		int ilCount = ogBCD.GetDataCount(omObject);

		m_CL_List.ResetContent( );
		m_CL_List.SetFont(&ogCourier_Regular_10);
		m_CL_List.InitStorage( ilCount, 100 );

		RecordSet olRecord;
		ogBCD.SetSort(omObject, omSortFields, true);

		int ilMaxCount = ogBCD.ShowAllFields(omObject, omFields, &m_CL_List,"");
		
		CStringArray olFields;
		::ExtractItemList(omFields,&olFields,',');

		CStringArray olSelections;
		::ExtractItemList(omSelStrings,&olSelections,',');

		CString olText;
		bool blfound = false;
		for (int i = 0; i < ogBCD.GetDataCount(omObject); i++)
		{
			int ilIndex   = -1;
			int ilCounter = 0;
			for (int j = 0; j < olSelections.GetSize(); j++)
			{
				if (olSelections[j].GetLength() > 0)
				{
					olText = ogBCD.GetField(omObject,i,olFields[j]);
					if (olText == olSelections[j])
					{
						ilIndex = i;
						ilCounter++;
					}
				}
				else
				{
					ilCounter++; 
				}
			}

			if (ilCounter == olSelections.GetSize() && ilIndex != -1)
			{

				m_CL_List.SetCurSel(i);
				m_CL_List.SetTopIndex(i);
				m_CL_List.SetFocus();
				blfound = true;
				break;
			}
		}

		if (!blfound)
		{
			m_CL_List.SetCurSel(0);
			m_CL_List.SetTopIndex(0);
			m_CL_List.SetFocus();
		}


		if (ilMaxCount > 40)
		{

			CWnd *polParent = this->GetParent();
			if (polParent == NULL)
				return TRUE;

			int ilCharPixel = 9;

			ilCount = ilMaxCount - 40;

			CRect olRect;
			
			GetWindowRect(olRect);

			int ilCY = olRect.bottom - olRect.top;
			int ilCX = olRect.right - olRect.left + ilCount * ilCharPixel;

			CRect r;
				
			polParent->GetClientRect(r);

			polParent->ClientToScreen(r);

			int ilMx = ((r.right - r.left) / 2 ) + r.left;
			int ilMy = ((r.bottom - r.top) / 2 ) + r.top;

			SetWindowPos( &wndTop, ilMx - (ilCX / 2), ilMy - (ilCY / 2),  ilCX, ilCY, SWP_NOZORDER | SWP_SHOWWINDOW );
		}
	}
	return FALSE;	// !!!!! hier muss unbedingt false stehen, sonst bekommt das ding den focus nicht !!!!!
//	return TRUE; 
}


CString PSSearchPosDlg::GetField()
{
	return omPostionStr;
}


void PSSearchPosDlg::OnOK() 
{
	
	CString str;
	int li_Count = m_CL_List.GetSelCount();
	if(li_Count > 0)
	{
		int *olSelIndex = new int[li_Count];
		m_CL_List.GetSelItems(li_Count,olSelIndex);
		
		for(int i =0;i<li_Count;i++)
		{
			m_CL_List.GetText(olSelIndex[i],str);

			if(bmPosGrp==true)
				omPostionStr += str + ";";
			else
				omPostionStr += str;
		}
	}

	omPostionStr.TrimLeft();
	omPostionStr.TrimRight();
	CDialog::OnOK();
}




