// SeasonDlg.cpp : implementation file
// Modification History: 
// 161100		rkr		OnAgent ge�ndert (PRF Athen)
// 23-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;
//

#include <stdafx.h>
#include <FPMS.h>
#include <SeasonDlg.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSTable.h>
#include <SeasonDlgCedaFlightData.h>
#include <PrivList.h>
#include <BasicData.h>
#include <resrc1.h>
#include <Utils.h>
#include <SeasonAskDlg.h>
#include <RotationDlg.h>
#include <CcaCedaFlightData.h>
#include <RotationRegnDlg.h>
#include <RotationViaDlg.h>
#include <SpotAllocation.h>
#include <RotationHtaDlg.h>

#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <RotationHAIDlg.h>

#include <CedaFlightUtilsData.h>
#include <CedaAptLocalUtc.h>
#include <process.h>
#include <CedaFpeData.h>
#include <SeasonCollectDlg.h>
#include <RotGDlgCedaFlightData.h>
#include <CedaBlaData.h>
#include <ModeOfPay.h>
#include <CedaBwaData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDC_BTCRC_APAY_DETAILS  41015 
#define IDC_BTCRC_DPAY_DETAILS  41016

#define IDC_CBFPSA	41017   
#define IDC_CBFGA1	41018
#define IDC_CBFGA2	41019
#define IDC_CBFBL1	41020
#define IDC_CBFBL2	41021
#define IDC_CBFPSD	41022   
#define IDC_CBFGD1	41023
#define IDC_CBFGD2	41024

#define IDC_BTCRC_CASH_ARR 41025
#define IDC_BTCRC_CASH_DEP 41026

int CSeasonDlg::CompareCcas(const CCADATA **e1, const CCADATA **e2)
{
	if (strlen((**e1).Ckic) == 0 && (**e1).Ckbs == TIMENULL)
		return 1;
	if (strlen((**e2).Ckic) == 0 && (**e2).Ckbs == TIMENULL)
		return -1;

	if (strlen((**e1).Ckic) == 0)
		return 1;
	if (strlen((**e2).Ckic) == 0)
		return -1;

	if (strcmp((**e1).Ckic, (**e2).Ckic) > 0)
		return 1;
	if (strcmp((**e1).Ckic, (**e2).Ckic) < 0)
		return -1;

	return 0;
}


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// CSeasonDlg dialog

CCSPtrArray<CCADATA> omOldCcaValues;


CSeasonDlg::CSeasonDlg(CWnd* pParent)
	: omCcaData(CCA_SEADLG_NEW, CCA_SEADLG_CHANGE, CCA_SEADLG_DELETE), CDialog(CSeasonDlg::IDD, NULL)
{

	CCS_TRY

	//{{AFX_DATA_INIT(CSeasonDlg)
	m_ADes3 = _T("");
	m_AEtoa = _T("");
	m_AAlc3 = _T("");
	m_AFltn = _T("");
	m_AFlns = _T("");
	m_AFlti = _T("");
	m_ATtyp = _T("");
//	m_AHtyp = _T("");
	m_AStyp = _T("");
	m_AStev = _T("");
	m_ASte2 = _T("");
	m_ASte3 = _T("");
	m_ASte4 = _T("");
	m_AOrg3 = _T("");
	m_AStod = _T("");
	m_AStoa = _T("");
	m_AGta1 = _T("");
	m_AGta2 = _T("");
	m_DOrg3 = _T("");
	m_Pvom = _T("");
	m_Pbis = _T("");
	m_ATage = _T("");
	m_DDes3 = _T("");
	m_DTage = _T("");
	m_DEtdi = _T("");
	m_DStoa = _T("");
	m_DStod = _T("");
	m_DFltn = _T("");
	m_DFlns = _T("");
	m_DFlti = _T("");
	m_DStyp = _T("");
	m_DTtyp = _T("");
	m_DStev = _T("");
	m_DSte2 = _T("");
	m_DSte3 = _T("");
	m_DSte4 = _T("");
	m_DGta1 = _T("");
	m_DGta2 = _T("");
//	m_DHtyp = _T("");
	m_AEtai = _T("");
	m_Freq = _T("");
	m_Ming = _T("");
	m_Act3 = _T("");
	m_ABlt1 = _T("");
	m_ABlt2 = _T("");
	m_DBlt1 = _T("");
	m_DB1bs = _T("");
	m_DB1es = _T("");
	m_DTmb1 = _T("");
	m_DCstd = _T("");
	m_ACsta = _T("");
	m_Act5 = _T("");
	m_ACxx = FALSE;
	m_ADays = _T("");
	m_AExt1 = _T("");
	m_AExt2 = _T("");
	m_ANoop = FALSE;
	m_APstd2 = _T("");
	m_ARem1 = _T("");
	m_AStatus = -1;
	m_DCicf = _T("");
	m_DCict = _T("");
	m_DCxx = FALSE;
	m_DDays = _T("");
	m_DGtd1 = _T("");
	m_DGtd2 = _T("");
	m_DNoop = FALSE;
	m_DRem1 = _T("");
	m_DStatus = -1;
	m_DTmcf = _T("");
	m_DTmct = _T("");
	m_Seas = _T("");
	m_Created = _T("");
	m_ACreated = _T("");
	m_AFluko = _T("");
	m_ALastChange = _T("");
	m_DAlc3 = _T("");
	m_DCreated = _T("");
	m_DFluko = _T("");
	m_DLastChange = _T("");
	m_AB1bs = _T("");
	m_AB1es = _T("");
	m_AB2bs = _T("");
	m_AB2es = _T("");
	m_AGa1b = _T("");
	m_AGa1e = _T("");
	m_AGa2b = _T("");
	m_AGa2e = _T("");
	m_APabs = _T("");
	m_APaes = _T("");
	m_APsta = _T("");
	m_ATet1 = _T("");
	m_ATet2 = _T("");
	m_ATga1 = _T("");
	m_ATga2 = _T("");
	m_ATmb1 = _T("");
	m_ATmb2 = _T("");
	m_DGd1b = _T("");
	m_DGd1e = _T("");
	m_DGd2b = _T("");
	m_DGd2e = _T("");
	m_DPdbs = _T("");
	m_DPdes = _T("");
	m_DTgd1 = _T("");
	m_DTgd2 = _T("");
	m_DTwr1 = _T("");
	m_DW1bs = _T("");
	m_DW1es = _T("");
	m_DWro1 = _T("");
	m_DPstd = _T("");
	m_Nose = _T("");
	m_ACht3 = _T("");
	m_DCht3 = _T("");
	m_ARem2 = _T("");
	m_DRem2 = _T("");
	m_AOrg4 = _T("");
	m_DDes4 = _T("");
	m_DBaz1 = _T("");
	m_DBaz4 = _T("");
	m_ACsgn = _T("");
	m_DCsgn = _T("");
	m_Regn = _T("");
	m_DWro2 = _T("");
	m_DW2bs = _T("");
	m_DW2es = _T("");
	m_DTwr2 = _T("");
	m_DCiFr = _T("");
	//}}AFX_DATA_INIT



	prmAFlight = new SEASONDLGFLIGHTDATA;
	prmDFlight = new SEASONDLGFLIGHTDATA;
	prmAFlightSave = new SEASONDLGFLIGHTDATA;
	prmDFlightSave = new SEASONDLGFLIGHTDATA;
	
    pomAJfnoTable = new CCSTable;
    pomDJfnoTable = new CCSTable;
	pomDCinsTable = new CCSTable;
	
	

	pomArrPermitsButton = NULL;
	pomDepPermitsButton = NULL;

	bmAFltiUserSet = false;
	bmDFltiUserSet = false;

	bmANatureUserSet = false;
	bmDNatureUserSet = false;

	polRotationAViaDlg = NULL;
	polRotationDViaDlg = NULL;

	//Fixed resources
	pomCBFPSA = new CCSButtonCtrl;
	pomCBFGA1 = new CCSButtonCtrl;
	pomCBFGA2 = new CCSButtonCtrl;
	pomCBFBL1 = new CCSButtonCtrl;
	pomCBFBL2 = new CCSButtonCtrl;
	pomCBFPSD = new CCSButtonCtrl;
	pomCBFGD1 = new CCSButtonCtrl;
	pomCBFGD2 = new CCSButtonCtrl;

	m_CashArrBtn =new CCSButtonCtrl;
	m_CashDepBtn=new CCSButtonCtrl;

	m_pToolTip = NULL;


	m_key = "DialogPosition\\SeasonDialog";
    CDialog::Create(CSeasonDlg::IDD, NULL);

	CCS_CATCH_ALL

}



void CSeasonDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, int ipModus, bool bpLocalTime, CString opAdid)
{

	CCS_TRY

	if(!(ogPrivList.GetStat("SEASONDLG_Dialog") == '1' || ogPrivList.GetStat("SEASONDLG_Dialog") == ' ') )	
	{	
		ShowWindow(SW_HIDE);
		return;
	}


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	


	ClearAll();

	omAdid = opAdid;
	bmLocalTime = bpLocalTime;

	imModus = ipModus; /* 	DLG_NEW, DLG_COPY, DLG_CHANGE, DLG_CHANGE_DIADATA */
	pomParent = pParent;
	lmCalledBy = lpUrno;
	lmRkey = lpRkey;

	ogDdx.Register(this, S_DLG_ROTATION_SPLIT, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, S_DLG_FLIGHT_DELETE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, S_DLG_RELOAD_CCA,	CString("SEASONDLG"), CString("Cca-Reload"),	FlightTableCf);
	
	ogDdx.Register((void *)this, FPE_INSERT, CString("FPEDATA"), CString("FPE Insert"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_UPDATE, CString("FPEDATA"), CString("FPE Update"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_DELETE, CString("FPEDATA"), CString("FPE Delete"),	FlightTableCf);

	ogSeasonDlgFlights.ReadRotation( lpRkey, lpUrno);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

	SetWndPos();

	bmInit = true;

	//ShowWindow(SW_SHOW);

	CCS_CATCH_ALL

}



void CSeasonDlg::NewData(CWnd* pParent, const DIAFLIGHTDATA *prpDiaFlight, bool bpLocalTime, CString opAdid)
{

	CCS_TRY

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ClearAll();

	omAdid = opAdid;
	bmLocalTime = bpLocalTime;

	imModus = DLG_CHANGE_DIADATA; /* 	DLG_NEW, DLG_COPY, DLG_CHANGE, DLG_CHANGE_DIADATA */
	pomParent = pParent;

 	lmCalledBy = prpDiaFlight->Urno;
	lmRkey = prpDiaFlight->Rkey;

 	ogDdx.Register(this, S_DLG_ROTATION_SPLIT, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
 	ogDdx.Register(this, S_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, S_DLG_FLIGHT_DELETE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, S_DLG_RELOAD_CCA,	CString("SEASONDLG"), CString("Cca-Reload"),	FlightTableCf);
	
	ogDdx.Register((void *)this, FPE_INSERT, CString("FPEDATA"), CString("FPE Insert"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_UPDATE, CString("FPEDATA"), CString("FPE Update"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_DELETE, CString("FPEDATA"), CString("FPE Delete"),	FlightTableCf);

	DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(prpDiaFlight->Rkey);
	DIAFLIGHTDATA *prlDiaFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
	DIAFLIGHTDATA *prlDiaFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

	ogSeasonDlgFlights.SetRotation(prlDiaFlightA, prlDiaFlightD);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

	SetWndPos();

	bmInit = true;

	//ShowWindow(SW_SHOW);


	CCS_CATCH_ALL

}





CSeasonDlg::~CSeasonDlg()
{

	ogSeasonDlgFlights.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();

	if(pomAJfnoTable != NULL)
		delete pomAJfnoTable;
/*	if(pomAViaCtrl != NULL)
		delete pomAViaCtrl;*/
	if(pomDJfnoTable != NULL)
		delete pomDJfnoTable;
/*	if(pomDViaCtrl != NULL)
		delete pomDViaCtrl;*/
	if(pomDCinsTable != NULL)
		delete pomDCinsTable;

	if(prmDFlight != NULL)
		delete prmDFlight;
	if(prmAFlight != NULL)
		delete prmAFlight;
	if(prmDFlightSave != NULL)
		delete prmDFlightSave;
	if(prmAFlightSave != NULL)
		delete prmAFlightSave;

	if(pomArrPermitsButton)
		delete pomArrPermitsButton;

	if(pomDepPermitsButton)
		delete pomDepPermitsButton;

	/*
	if (m_dlgAModeOfPay)
	{
		m_dlgAModeOfPay->DestroyWindow();
		delete m_dlgAModeOfPay;
		m_dlgAModeOfPay = NULL;
	}
	
	if (m_dlgDModeOfPay)
	{
		m_dlgDModeOfPay->DestroyWindow();
		delete m_dlgDModeOfPay;
		m_dlgDModeOfPay = NULL;
	}
	*/

//	delete m_dlgAModeOfPay;
//	delete m_dlgDModeOfPay;

	omDCins.DeleteAll();
	omOldDCins.DeleteAll();
	omDJfno.DeleteAll();
	omAJfno.DeleteAll();
	omDCinsSave.DeleteAll();
	omOldCcaValues.DeleteAll();

	if (polRotationAViaDlg)
	{
		polRotationAViaDlg->DestroyWindow();
		delete polRotationAViaDlg;
		polRotationAViaDlg = NULL;
	}

	if (polRotationDViaDlg)
	{
		polRotationDViaDlg->DestroyWindow();
		delete polRotationDViaDlg;
		polRotationDViaDlg = NULL;
	}

	if(pogSeasonAskDlg != NULL)
		pogSeasonAskDlg->SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

	delete m_pToolTip;
}




void CSeasonDlg::ClearAll()
{

	CCS_TRY

	bmInit = false;

	ogSeasonDlgFlights.ClearAll();
	omCcaData.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	

	m_Act3 = CString("");
	m_Act5 = CString("");
	m_Regn = CString("");
	m_Nose = CString("");

	bmDlgEnabled = true;

	bmIsAFfnoShown = false;
	bmIsDFfnoShown = false;

	bmAbflug = false;
	bmAnkunft = false;
	bmIsCheckInNew = true;

	


	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	
	delete prlFlight;

	lmCalledBy = 0;
	lmRkey = 0;

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	omDCins.DeleteAll();
	omOldDCins.DeleteAll();
	omDCinsSave.DeleteAll();

	CCS_CATCH_ALL
}


void CSeasonDlg::DoDataExchange(CDataExchange* pDX)
{

	CCS_TRY

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSeasonDlg)
	DDX_Control(pDX, IDC_D_PAY, m_DPay);
	DDX_Control(pDX, IDC_A_PAY, m_APay);
	DDX_Control(pDX, IDC_ACXXREASON, m_CL_ACxxReason);
	DDX_Control(pDX, IDC_DCXXREASON, m_CL_DCxxReason);
	DDX_Control(pDX, IDC_ACBNFES, m_CB_ANfes);
	DDX_Control(pDX, IDC_DCBNFES, m_CB_DNfes);
	DDX_Control(pDX, IDC_ANFES, m_CE_ANfes);
	DDX_Control(pDX, IDC_DNFES, m_CE_DNfes);
	DDX_Control(pDX, IDC_DCINSMAL4, m_CB_DCinsMal4);
	DDX_Control(pDX, IDC_DBAZ4, m_CE_DBaz4);
	DDX_Control(pDX, IDC_DBAZ1, m_CE_DBaz1);
	DDX_Control(pDX, IDC_DREMP, m_CB_DRemp);
	DDX_Control(pDX, IDC_AREMP, m_CB_ARemp);
	DDX_Control(pDX, IDC_DDES4, m_CE_DDes4);
	DDX_Control(pDX, IDC_AORG4, m_CE_AOrg4);
	DDX_Control(pDX, IDC_DREM2, m_CE_DRem2);
	DDX_Control(pDX, IDC_AREM2, m_CE_ARem2);
	DDX_Control(pDX, IDC_STATIC_ACHT3, m_CS_ACht3);
	DDX_Control(pDX, IDC_STATIC_DCHT3, m_CS_DCht3);
	DDX_Control(pDX, IDC_DCHT3, m_CE_DCht3);
	DDX_Control(pDX, IDC_ACHT3, m_CE_ACht3);
	DDX_Control(pDX, IDC_ACSTA, m_CE_ACsta);
	DDX_Control(pDX, IDC_DCSTD, m_CE_DCstd);
	DDX_Control(pDX, IDC_STATIC_NOSE, m_CS_Nose);
	DDX_Control(pDX, IDC_NOSE, m_CE_Nose);
	DDX_Control(pDX, IDOK, m_CB_Ok);
	DDX_Control(pDX, IDC_DDALY, m_CB_DDaly);
	DDX_Control(pDX, IDC_DALC3LIST2, m_CB_DAlc3List2);
	DDX_Control(pDX, IDC_DALC3LIST3, m_CB_DAlc3List3);
	DDX_Control(pDX, IDC_DEFAULT, m_CB_Default);
	DDX_Control(pDX, IDC_DSTODCALC, m_CB_DStodCalc);
	DDX_Control(pDX, IDC_ASTOACALC, m_CB_AStoaCalc);
	DDX_Control(pDX, IDC_ARESET, m_CB_AReset);
	DDX_Control(pDX, IDC_DRESET, m_CB_DReset);
	DDX_Control(pDX, IDC_DAILY_MASK, m_CB_DailyMask);
	DDX_Control(pDX, IDC_NEXTFLIGHT, m_CB_NextFlight);
	DDX_Control(pDX, IDC_PREVFLIGHT, m_CB_PrevFlight);
	DDX_Control(pDX, IDC_ASHOWJFNO, m_CB_AShowJfno);
	DDX_Control(pDX, IDC_ADALY, m_CB_ADaly);
	DDX_Control(pDX, IDC_AAlc3LIST, m_CB_AAlc3List);
	DDX_Control(pDX, IDC_AAlc3LIST3, m_CB_AAlc3List3);
	DDX_Control(pDX, IDC_AAlc3LIST2, m_CB_AAlc3List2);
	DDX_Control(pDX, IDC_DSTATUS, m_CB_DStatus);
	DDX_Control(pDX, IDC_DSHOWJFNO, m_CB_DShowJfno);
	DDX_Control(pDX, IDC_DNOOP, m_CB_DNoop);
	DDX_Control(pDX, IDC_DCXX, m_CB_DCxx);
	DDX_Control(pDX, IDC_ASTATUS, m_CB_AStatus);
	DDX_Control(pDX, IDC_ASTATUS2, m_CB_AStatus2);
	DDX_Control(pDX, IDC_ASTATUS3, m_CB_AStatus3);
	DDX_Control(pDX, IDC_DSTATUS2, m_CB_DStatus2);
	DDX_Control(pDX, IDC_DSTATUS3, m_CB_DStatus3);
	DDX_Control(pDX, IDC_ANOOP, m_CB_ANoop);
	DDX_Control(pDX, IDC_ACXX, m_CB_ACxx);
	DDX_Control(pDX, IDC_STATIC_ANKUNFT, m_CS_Ankunft);
	DDX_Control(pDX, IDC_STATIC_ABFLUG, m_CS_Abflug);
	DDX_Control(pDX, IDC_AORG3LIST, m_CB_AOrg3List);
	DDX_Control(pDX, IDC_DDES3LIST, m_CB_DDes3List);
	DDX_Control(pDX, IDC_DALC3LIST, m_CB_DAlc3List);
	DDX_Control(pDX, IDC_ACT35LIST, m_CB_Act35List);
	DDX_Control(pDX, IDC_DTTYPLIST, m_CB_DTtypList);
	DDX_Control(pDX, IDC_DSTYPLIST, m_CB_DStypList);
	DDX_Control(pDX, IDC_ATTYPLIST, m_CB_ATTypList);
	DDX_Control(pDX, IDC_ASTYPLIST, m_CB_AStypList);
	DDX_Control(pDX, IDC_DWRO1LIST, m_CB_DWro1List);
	DDX_Control(pDX, IDC_DPSTDLIST, m_CB_DPstdList);
	DDX_Control(pDX, IDC_DGTD2LIST, m_CB_DGtd2List);
	DDX_Control(pDX, IDC_DGTD1LIST, m_CB_DGtd1List);
	DDX_Control(pDX, IDC_DCINSLIST4, m_CB_DCinsList4);
	DDX_Control(pDX, IDC_DCINSLIST3, m_CB_DCinsList3);
	DDX_Control(pDX, IDC_DCINSLIST2, m_CB_DCinsList2);
	DDX_Control(pDX, IDC_DCINSLIST1, m_CB_DCinsList1);
	DDX_Control(pDX, IDC_ATMB2, m_CE_ATmb2);
	DDX_Control(pDX, IDC_APSTALIST, m_CB_APstaList);
	DDX_Control(pDX, IDC_AGTA2LIST, m_CB_AGta2List);
	DDX_Control(pDX, IDC_AGTA1LIST, m_CB_AGta1List);
	DDX_Control(pDX, IDC_AEXT2LIST, m_CB_AExt2List);
	DDX_Control(pDX, IDC_AEXT1LIST, m_CB_AExt1List);
	DDX_Control(pDX, IDC_ABLT2LIST, m_CB_ABlt2List);
	DDX_Control(pDX, IDC_ABLT1LIST, m_CB_ABlt1List);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_AGENTS, m_CB_Agents);
	DDX_Control(pDX, IDC_DJFNO_BORDER, m_CE_DJfnoBorder);
	DDX_Control(pDX, IDC_DLASTCHANGE, m_CE_DLastChange);
	DDX_Control(pDX, IDC_DGD2D, m_CB_DGd2d);
	DDX_Control(pDX, IDC_DFLUKO, m_CE_DFluko);
	DDX_Control(pDX, IDC_DCREATED, m_CE_DCreated);
	DDX_Control(pDX, IDC_DALC3, m_CE_DAlc3);
	DDX_Control(pDX, IDC_ALASTCHANGE, m_CE_ALastChange);
	DDX_Control(pDX, IDC_AJFNO_BORDER, m_CE_AJfnoBorder);
	DDX_Control(pDX, IDC_AFLUKO, m_CE_AFluko);
	DDX_Control(pDX, IDC_ACREATED, m_CE_ACreated);
	DDX_Control(pDX, IDC_SEAS, m_CE_Seas);
	DDX_Control(pDX, IDC_DREM1, m_CE_DRem1);
	DDX_Control(pDX, IDC_DGTD2, m_CE_DGtd2);
	DDX_Control(pDX, IDC_DGTD1, m_CE_DGtd1);
	DDX_Control(pDX, IDC_DDAYS, m_CE_DDays);
	DDX_Control(pDX, IDC_AEXT2, m_CE_AExt2);
	DDX_Control(pDX, IDC_AEXT1, m_CE_AExt1);
	DDX_Control(pDX, IDC_ADAYS, m_CE_ADays);
	DDX_Control(pDX, IDC_AREM1, m_CE_ARem1);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ABLT2, m_CE_ABlt2);
	DDX_Control(pDX, IDC_ABLT1, m_CE_ABlt1);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_MING, m_CE_Ming);
	DDX_Control(pDX, IDC_DSTEV, m_CE_DStev);
	DDX_Control(pDX, IDC_DSTE2, m_CE_DSte2);
	DDX_Control(pDX, IDC_DSTE3, m_CE_DSte3);
	DDX_Control(pDX, IDC_DSTE4, m_CE_DSte4);
	DDX_Control(pDX, IDC_DTTYP, m_CE_DTtyp);
	DDX_Control(pDX, IDC_DSTYP, m_CE_DStyp);
	DDX_Control(pDX, IDC_DFLNS, m_CE_DFlns);
	DDX_Control(pDX, IDC_DFLTN, m_CE_DFltn);
	DDX_Control(pDX, IDC_DFLTI, m_CE_DFlti);
	DDX_Control(pDX, IDC_DSTOD, m_CE_DStod);
	DDX_Control(pDX, IDC_DSTOA, m_CE_DStoa);
	DDX_Control(pDX, IDC_DETDI, m_CE_DEtdi);
	DDX_Control(pDX, IDC_AETAI, m_CE_AEtai);
	DDX_Control(pDX, IDC_DDES3, m_CE_DDes3);
	DDX_Control(pDX, IDC_PBIS, m_CE_Pbis);
	DDX_Control(pDX, IDC_PVOM, m_CE_Pvom);
	DDX_Control(pDX, IDC_FREQ, m_CE_Freq);
	DDX_Control(pDX, IDC_DORG3, m_CE_DOrg3);
	DDX_Control(pDX, IDC_AGTA2, m_CE_AGta2);
	DDX_Control(pDX, IDC_AGTA1, m_CE_AGta1);
	DDX_Control(pDX, IDC_ASTOA, m_CE_AStoa);
	DDX_Control(pDX, IDC_ASTOD, m_CE_AStod);
	DDX_Control(pDX, IDC_AORG3, m_CE_AOrg3);
	DDX_Control(pDX, IDC_ASTEV, m_CE_AStev);
	DDX_Control(pDX, IDC_ASTE2, m_CE_ASte2);
	DDX_Control(pDX, IDC_ASTE3, m_CE_ASte3);
	DDX_Control(pDX, IDC_ASTE4, m_CE_ASte4);
	DDX_Control(pDX, IDC_ASTYP, m_CE_AStyp);
	DDX_Control(pDX, IDC_ATTYP, m_CE_ATtyp);
	DDX_Control(pDX, IDC_AFLNS, m_CE_AFlns);
	DDX_Control(pDX, IDC_AFLTN, m_CE_AFltn);
	DDX_Control(pDX, IDC_AFLTI, m_CE_AFlti);
	DDX_Control(pDX, IDC_AALC3, m_CE_AAlc3);
	DDX_Control(pDX, IDC_ADES3, m_CE_ADes3);
	DDX_Text(pDX, IDC_ACSTA, m_ACsta);
	DDX_Text(pDX, IDC_DCSTD, m_DCstd);
	DDX_Text(pDX, IDC_ADES3, m_ADes3);
	DDX_Text(pDX, IDC_AALC3, m_AAlc3);
	DDX_Text(pDX, IDC_AFLTN, m_AFltn);
	DDX_Text(pDX, IDC_AFLNS, m_AFlns);
	DDX_Text(pDX, IDC_AFLTI, m_AFlti);
	DDX_Text(pDX, IDC_ATTYP, m_ATtyp);
	DDX_Text(pDX, IDC_ASTYP, m_AStyp);
	DDX_Text(pDX, IDC_ASTEV, m_AStev);
	DDX_Text(pDX, IDC_ASTE2, m_ASte2);
	DDX_Text(pDX, IDC_ASTE3, m_ASte3);
	DDX_Text(pDX, IDC_ASTE4, m_ASte4);
	DDX_Text(pDX, IDC_AORG3, m_AOrg3);
	DDX_Text(pDX, IDC_ASTOD, m_AStod);
	DDX_Text(pDX, IDC_ASTOA, m_AStoa);
	DDX_Text(pDX, IDC_AGTA1, m_AGta1);
	DDX_Text(pDX, IDC_AGTA2, m_AGta2);
	DDX_Text(pDX, IDC_DORG3, m_DOrg3);
	DDX_Text(pDX, IDC_PVOM, m_Pvom);
	DDX_Text(pDX, IDC_PBIS, m_Pbis);
	DDX_Text(pDX, IDC_DDES3, m_DDes3);
	DDX_Text(pDX, IDC_DETDI, m_DEtdi);
	DDX_Text(pDX, IDC_DSTOA, m_DStoa);
	DDX_Text(pDX, IDC_DSTOD, m_DStod);
	DDX_Text(pDX, IDC_DFLTN, m_DFltn);
	DDX_Text(pDX, IDC_DFLNS, m_DFlns);
	DDX_Text(pDX, IDC_DFLTI, m_DFlti);
	DDX_Text(pDX, IDC_DSTYP, m_DStyp);
	DDX_Text(pDX, IDC_DTTYP, m_DTtyp);
	DDX_Text(pDX, IDC_DSTEV, m_DStev);
	DDX_Text(pDX, IDC_DSTE2, m_DSte2);
	DDX_Text(pDX, IDC_DSTE3, m_DSte3);
	DDX_Text(pDX, IDC_DSTE4, m_DSte4);
	DDX_Text(pDX, IDC_AETAI, m_AEtai);
	DDX_Text(pDX, IDC_FREQ, m_Freq);
	DDX_Text(pDX, IDC_MING, m_Ming);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ABLT1, m_ABlt1);
	DDX_Text(pDX, IDC_ABLT2, m_ABlt2);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Check(pDX, IDC_ACXX, m_ACxx);
	DDX_Text(pDX, IDC_ADAYS, m_ADays);
	DDX_Text(pDX, IDC_AEXT1, m_AExt1);
	DDX_Text(pDX, IDC_AEXT2, m_AExt2);
	DDX_Check(pDX, IDC_ANOOP, m_ANoop);
	DDX_Text(pDX, IDC_AREM1, m_ARem1);
	DDX_Radio(pDX, IDC_ASTATUS, m_AStatus);
	DDX_Check(pDX, IDC_DCXX, m_DCxx);
	DDX_Text(pDX, IDC_DDAYS, m_DDays);
	DDX_Text(pDX, IDC_DGTD1, m_DGtd1);
	DDX_Text(pDX, IDC_DGTD2, m_DGtd2);
	DDX_Check(pDX, IDC_DNOOP, m_DNoop);
	DDX_Text(pDX, IDC_DREM1, m_DRem1);
	DDX_Radio(pDX, IDC_DSTATUS, m_DStatus);
	DDX_Text(pDX, IDC_SEAS, m_Seas);
	DDX_Text(pDX, IDC_ACREATED, m_ACreated);
	DDX_Text(pDX, IDC_AFLUKO, m_AFluko);
	DDX_Text(pDX, IDC_ALASTCHANGE, m_ALastChange);
	DDX_Text(pDX, IDC_DALC3, m_DAlc3);
	DDX_Text(pDX, IDC_DCREATED, m_DCreated);
	DDX_Text(pDX, IDC_DFLUKO, m_DFluko);
	DDX_Text(pDX, IDC_DLASTCHANGE, m_DLastChange);
	DDX_Text(pDX, IDC_AB1BS, m_AB1bs);
	DDX_Control(pDX, IDC_AB1BS, m_CE_AB1bs);
	DDX_Text(pDX, IDC_AB1ES, m_AB1es);
	DDX_Control(pDX, IDC_AB1ES, m_CE_AB1es);
	DDX_Text(pDX, IDC_AB2BS, m_AB2bs);
	DDX_Control(pDX, IDC_AB2BS, m_CE_AB2bs);
	DDX_Text(pDX, IDC_AB2ES, m_AB2es);
	DDX_Control(pDX, IDC_AB2ES, m_CE_AB2es);
	DDX_Text(pDX, IDC_AGA1B, m_AGa1b);
	DDX_Control(pDX, IDC_AGA1B, m_CE_AGa1b);
	DDX_Text(pDX, IDC_AGA1E, m_AGa1e);
	DDX_Control(pDX, IDC_AGA1E, m_CE_AGa1e);
	DDX_Text(pDX, IDC_AGA2B, m_AGa2b);
	DDX_Control(pDX, IDC_AGA2B, m_CE_AGa2b);
	DDX_Text(pDX, IDC_AGA2E, m_AGa2e);
	DDX_Control(pDX, IDC_AGA2E, m_CE_AGa2e);
	DDX_Text(pDX, IDC_APABS, m_APabs);
	DDX_Control(pDX, IDC_APABS, m_CE_APabs);
	DDX_Text(pDX, IDC_APAES, m_APaes);
	DDX_Control(pDX, IDC_APAES, m_CE_APaes);
	DDX_Text(pDX, IDC_APSTA, m_APsta);
	DDX_Control(pDX, IDC_APSTA, m_CE_APsta);
	DDX_Text(pDX, IDC_ATET1, m_ATet1);
	DDX_Control(pDX, IDC_ATET1, m_CE_ATet1);
	DDX_Text(pDX, IDC_ATET2, m_ATet2);
	DDX_Control(pDX, IDC_ATET2, m_CE_ATet2);
	DDX_Text(pDX, IDC_ATGA1, m_ATga1);
	DDX_Control(pDX, IDC_ATGA1, m_CE_ATga1);
	DDX_Text(pDX, IDC_ATGA2, m_ATga2);
	DDX_Control(pDX, IDC_ATGA2, m_CE_ATga2);
	DDX_Text(pDX, IDC_ATMB1, m_ATmb1);
	DDX_Control(pDX, IDC_ATMB1, m_CE_ATmb1);
	DDX_Text(pDX, IDC_ATMB2, m_ATmb2);
	DDX_Text(pDX, IDC_DGD1B, m_DGd1b);
	DDX_Control(pDX, IDC_DGD1B, m_CE_DGd1b);
	DDX_Text(pDX, IDC_DGD1E, m_DGd1e);
	DDX_Control(pDX, IDC_DGD1E, m_CE_DGd1e);
	DDX_Text(pDX, IDC_DGD2B, m_DGd2b);
	DDX_Control(pDX, IDC_DGD2B, m_CE_DGd2b);
	DDX_Text(pDX, IDC_DGD2E, m_DGd2e);
	DDX_Control(pDX, IDC_DGD2E, m_CE_DGd2e);
	DDX_Text(pDX, IDC_DPDBS, m_DPdbs);
	DDX_Control(pDX, IDC_DPDBS, m_CE_DPdbs);
	DDX_Text(pDX, IDC_DPDES, m_DPdes);
	DDX_Control(pDX, IDC_DPDES, m_CE_DPdes);
	DDX_Text(pDX, IDC_DTGD1, m_DTgd1);
	DDX_Control(pDX, IDC_DTGD1, m_CE_DTgd1);
	DDX_Text(pDX, IDC_DTGD2, m_DTgd2);
	DDX_Control(pDX, IDC_DTGD2, m_CE_DTgd2);
	DDX_Text(pDX, IDC_DTWR1, m_DTwr1);
	DDX_Control(pDX, IDC_DTWR1, m_CE_DTwr1);
	DDX_Text(pDX, IDC_DW1BS, m_DW1bs);
	DDX_Control(pDX, IDC_DW1BS, m_CE_DW1bs);
	DDX_Text(pDX, IDC_DW1ES, m_DW1es);
	DDX_Control(pDX, IDC_DW1ES, m_CE_DW1es);
	DDX_Text(pDX, IDC_DWRO1, m_DWro1);
	DDX_Control(pDX, IDC_DWRO1, m_CE_DWro1);
	DDX_Control(pDX, IDC_DCINSBORDER, m_CE_DCinsBorder);
	DDX_Text(pDX, IDC_DPSTD, m_DPstd);
	DDX_Control(pDX, IDC_DPSTD, m_CE_DPstd);
	DDX_Text(pDX, IDC_NOSE, m_Nose);
	DDX_Text(pDX, IDC_ACHT3, m_ACht3);
	DDX_Text(pDX, IDC_DCHT3, m_DCht3);
	DDX_Text(pDX, IDC_AREM2, m_ARem2);
	DDX_Text(pDX, IDC_DREM2, m_DRem2);
	DDX_Text(pDX, IDC_AORG4, m_AOrg4);
	DDX_Text(pDX, IDC_DDES4, m_DDes4);
	DDX_Text(pDX, IDC_DBAZ1, m_DBaz1);
	DDX_Text(pDX, IDC_DBAZ4, m_DBaz4);
	DDX_Control(pDX, IDC_ACSGN, m_CE_ACsgn);
	DDX_Text(pDX, IDC_ACSGN, m_ACsgn);
	DDX_Control(pDX, IDC_DCSGN, m_CE_DCsgn);
	DDX_Text(pDX, IDC_DCSGN, m_DCsgn);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Control(pDX, IDC_DWRO2LIST, m_CB_DWro2List);
	DDX_Text(pDX, IDC_DW2BS, m_DW2bs);
	DDX_Control(pDX, IDC_DW2BS, m_CE_DW2bs);
	DDX_Text(pDX, IDC_DW2ES, m_DW2es);
	DDX_Control(pDX, IDC_DW2ES, m_CE_DW2es);
	DDX_Text(pDX, IDC_DWRO2, m_DWro2);
	DDX_Control(pDX, IDC_DWRO2, m_CE_DWro2);
	DDX_Text(pDX, IDC_DTWR2, m_DTwr2);
	DDX_Control(pDX, IDC_DTWR2, m_CE_DTwr2);
	DDX_Text(pDX, IDC_CIFR_CITO, m_DCiFr);
	DDX_Control(pDX, IDC_CIFR_CITO, m_CE_DCiFr);
	DDX_Control(pDX, IDC_AVIA, m_CB_AVia);
	DDX_Control(pDX, IDC_DVIA, m_CB_DVia);
	DDX_Control(pDX, IDC_COMBO_AHISTORY, m_ComboAHistory);
	DDX_Control(pDX, IDC_COMBO_DHISTORY, m_ComboDHistory);
	DDX_Control(pDX, IDC_EDIT_ADURA, m_CE_ADura);
	DDX_Control(pDX, IDC_EDIT_DDURA, m_CE_DDura);
	DDX_Control(pDX, IDC_DCINSBORDEREXT, m_CE_DCinsBorderExt);
	DDX_Control(pDX, IDC_COMBO_CCA_XN, m_ComboDcins);
	DDX_Text(pDX, IDC_DBLT1, m_DBlt1);
	DDX_Text(pDX, IDC_DTMB1, m_DTmb1);
	DDX_Text(pDX, IDC_DB1BA, m_DB1bs);
	DDX_Text(pDX, IDC_DB1EA, m_DB1es);
	DDX_Control(pDX, IDC_DBLT1, m_CE_DBlt1);
	DDX_Control(pDX, IDC_DBLT1LIST, m_CB_DBlt1List);
	DDX_Control(pDX, IDC_DB1EA, m_CE_DB1es);
	DDX_Control(pDX, IDC_DB1BA, m_CE_DB1bs);
	DDX_Control(pDX, IDC_DTMB1, m_CE_DTmb1);
	//}}AFX_DATA_MAP

	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(CSeasonDlg, CDialog)
	//{{AFX_MSG_MAP(CSeasonDlg)
	ON_BN_CLICKED(IDC_NEXTFLIGHT, OnNextflight)
	ON_BN_CLICKED(IDC_PREVFLIGHT, OnPrevflight)
	ON_BN_CLICKED(IDC_ARRPERMITSBUTTON, onArrPermitsButton)
	ON_BN_CLICKED(IDC_DEPPERMITSBUTTON, onDepPermitsButton)
	ON_BN_CLICKED(IDC_ACBNFES, OnANfes)
	ON_BN_CLICKED(IDC_DCBNFES, OnDNfes)
	ON_BN_CLICKED(IDC_ASHOWJFNO, OnAshowjfno)
	ON_BN_CLICKED(IDC_DSHOWJFNO, OnDshowjfno)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_BN_CLICKED(IDC_DRESET, OnDreset)
	ON_BN_CLICKED(IDC_DAILY_MASK, OnDailyMask)
	ON_BN_CLICKED(IDC_DEFAULT, OnDefault)
	ON_BN_CLICKED(IDC_ARESET, OnAreset)
	ON_BN_CLICKED(IDC_ADALY, OnAdaly)
	ON_BN_CLICKED(IDC_DDALY, OnDdaly)
	ON_BN_CLICKED(IDC_ASTATUS, OnAstatus)
	ON_BN_CLICKED(IDC_ACXX, OnAcxx)
	ON_BN_CLICKED(IDC_DGD2D, OnDgd2d)
	ON_BN_CLICKED(IDC_ANOOP, OnAnoop)
	ON_BN_CLICKED(IDC_DCXX, OnDcxx)
	ON_BN_CLICKED(IDC_DNOOP, OnDnoop)
	ON_BN_CLICKED(IDC_DSTATUS, OnDstatus)
	ON_BN_CLICKED(IDC_AAlc3LIST, OnAAlc3LIST)
	ON_BN_CLICKED(IDC_AAlc3LIST2, OnAAlc3LIST2)
	ON_BN_CLICKED(IDC_AAlc3LIST3, OnAAlc3LIST3)
	ON_BN_CLICKED(IDC_ABLT1LIST, OnAblt1list)
	ON_BN_CLICKED(IDC_ABLT2LIST, OnAblt2list)
	ON_BN_CLICKED(IDC_DBLT1LIST, OnDblt1list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_AEXT1LIST, OnAext1list)
	ON_BN_CLICKED(IDC_AEXT2LIST, OnAext2list)
	ON_BN_CLICKED(IDC_AGTA1LIST, OnAgta1list)
	ON_BN_CLICKED(IDC_AGTA2LIST, OnAgta2list)
	ON_BN_CLICKED(IDC_AORG3LIST, OnAorg3list)
	ON_BN_CLICKED(IDC_APSTALIST, OnApstalist)
	ON_BN_CLICKED(IDC_ASTYPLIST, OnAstyplist)
	ON_BN_CLICKED(IDC_DALC3LIST, OnDalc3list)
	ON_BN_CLICKED(IDC_DALC3LIST2, OnDalc3list2)
	ON_BN_CLICKED(IDC_DALC3LIST3, OnDalc3list3)
	ON_BN_CLICKED(IDC_DCINSLIST1, OnDcinslist1)
	ON_BN_CLICKED(IDC_DCINSLIST2, OnDcinslist2)
	ON_BN_CLICKED(IDC_DCINSLIST3, OnDcinslist3)
	ON_BN_CLICKED(IDC_DCINSLIST4, OnDcinslist4)
	ON_BN_CLICKED(IDC_DDES3LIST, OnDdes3list)
	ON_BN_CLICKED(IDC_DGTD1LIST, OnDgtd1list)
	ON_BN_CLICKED(IDC_DGTD2LIST, OnDgtd2list)
	ON_BN_CLICKED(IDC_DPSTDLIST, OnDpstdlist)
	ON_BN_CLICKED(IDC_DSTYPLIST, OnDstyplist)
	ON_BN_CLICKED(IDC_DTTYPLIST, OnDttyplist)
	ON_BN_CLICKED(IDC_DWRO1LIST, OnDwro1list)
	ON_BN_CLICKED(IDC_ATTYPLIST, OnAttyplist)
	ON_BN_CLICKED(IDC_DCINSMAL4, OnDcinsmal4)
	ON_MESSAGE(WM_VIA, OnViaChanged)
	ON_CBN_SELCHANGE(IDC_AREMP, OnSelchangeAremp)
	ON_CBN_SELCHANGE(IDC_DREMP, OnSelchangeDremp)
	ON_MESSAGE(WM_EDIT_RBUTTONDOWN, OnEditRButtonDown)
	ON_BN_CLICKED(IDC_AGENTS, OnAgent)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_BN_CLICKED(IDC_DWRO2LIST, OnDwro2list)
	ON_BN_CLICKED(IDC_AVIA, OnAVia)
	ON_BN_CLICKED(IDC_DVIA, OnDVia)
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify )
	ON_MESSAGE(WM_EDIT_LBUTTONDBLCLK, OnEditDbClk)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO_AHISTORY, OnSelchangeAHistory)
	ON_CBN_SELCHANGE(IDC_COMBO_DHISTORY, OnSelchangeDHistory)
	ON_CBN_SELCHANGE(IDC_COMBO_CCA_XN, OnSelchangeDcins)
	ON_CBN_SELCHANGE(IDC_ACXXREASON, OnSelchangeACxxReason)
	ON_CBN_SELCHANGE(IDC_DCXXREASON, OnSelchangeDCxxReason)
	ON_BN_CLICKED(IDC_A_PAY, OnAPay)
	ON_BN_CLICKED(IDC_D_PAY, OnDPay)
	ON_BN_CLICKED(IDC_CBFPSA, OnFixedAllocaton) //For Fixed Allocation - PRF 8405
	ON_BN_CLICKED(IDC_ASTATUS2, OnAstatus)
	ON_BN_CLICKED(IDC_ASTATUS3, OnAstatus)
	ON_BN_CLICKED(IDC_DSTATUS2, OnDstatus)
	ON_BN_CLICKED(IDC_DSTATUS3, OnDstatus)
	ON_BN_CLICKED(IDC_CBFGA1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGA2, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFBL1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFBL2, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFPSD, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGD1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGD2, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_BTCRC_CASH_ARR, OnACash) 
	ON_BN_CLICKED(IDC_BTCRC_CASH_DEP, OnDCash) 

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//	ON_BN_CLICKED(IDC_OK, OnOk)

/////////////////////////////////////////////////////////////////////////////
// CSeasonDlg message handlers

BOOL CSeasonDlg::OnInitDialog() 
{

	CCS_TRY

	CDialog::OnInitDialog();


  //  EnableToolTips(TRUE);


	InitTables();	

	//Init editfields

	m_CE_Seas.SetReadOnly();
	m_CE_ADes3.SetReadOnly();
	m_CE_DOrg3.SetReadOnly();
	m_CE_AFluko.SetReadOnly();
	m_CE_ACreated.SetReadOnly();
	m_CE_ALastChange.SetReadOnly();
	m_CE_DFluko.SetReadOnly();
	m_CE_DCreated.SetReadOnly();
	m_CE_DLastChange.SetReadOnly();
	m_CE_DCiFr.SetReadOnly();

	if(strcmp(pcgHome, "ATH") == 0)
	{

		m_CE_AAlc3.SetBKColor(YELLOW);	
		m_CE_AFltn.SetBKColor(YELLOW);
		m_CE_DAlc3.SetBKColor(YELLOW);
		m_CE_DFltn.SetBKColor(YELLOW);
	}

	m_CE_AStoa.SetBKColor(YELLOW);
//	m_CE_AOrg3.SetBKColor(YELLOW);
	m_CE_AOrg4.SetBKColor(YELLOW);

	m_CE_DStod.SetBKColor(YELLOW);
//	m_CE_DDes3.SetBKColor(YELLOW);
	m_CE_DDes4.SetBKColor(YELLOW);


	m_CE_Pvom.SetTypeToDate(true);
	m_CE_Pbis.SetTypeToDate(true);
	m_CE_Freq.SetTypeToString("#",1,0);

	m_CE_Ming.SetTypeToString("###",3,0);

	CRect olRect;

	
	//Ankunft
	//
	m_CE_AAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_AFltn.SetTypeToString("#####",5,2);
	m_CE_AFlns.SetTypeToString("X",1,0);
	m_CE_AFlti.SetTypeToString("A",1,0);
	m_CE_ADays.SetTypeToInt(1,9999999);
	

/*	pomAViaCtrl = new ViaTableCtrl(this,"HEAD,APC3,FIDS,STOA,ETA,ATA,STOD,ETD,ATD");
	
	m_CE_AOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	pomAViaCtrl->SetSecStat(ogPrivList.GetStat("SEASONDLG_CB_AShowVia"));


	olRect.top = olRect.top;
	olRect.bottom = 0;
	olRect.left = olRect.right + 15;
	olRect.right = 0;

	pomAViaCtrl->SetPosition(olRect);
*/


//	m_CE_Act3.SetTypeToString("x|#x|#x|#",3,0);
//	m_CE_Act5.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_Act3.SetTypeToString("XXX",3,0);
	m_CE_Act5.SetTypeToString("XXXXX",5,0);
//	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Act5.SetBKColor(YELLOW);

	if (!bgCheckPRFL)
	{
		m_CE_ACht3.ShowWindow(SW_HIDE);
		m_CS_ACht3.ShowWindow(SW_HIDE);
	}
	else
	{
		m_CE_ACht3.SetTypeToString("X",1,0);
		m_CE_ACht3.ShowWindow(SW_SHOW);
		m_CS_ACht3.ShowWindow(SW_SHOW);
	}


//	m_CS_Nose.ShowWindow(SW_HIDE);		
//	m_CE_Nose.SetSecState('-');		
	m_CE_Nose.SetTypeToString("###",3,0);

	/* //UFIS-1097 - Commented out - Start
	if(bgNatureAutoCalc)
	{
		m_CE_AStyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_ATtyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
		m_CE_AStyp.SetTypeToString("XX",2,0);
	}

	*/ //UFIS-1097 - Commented out - End

	if(bgNatureServiceMand)
	{
		m_CE_AStyp.SetBKColor(YELLOW);
		m_CE_AStyp.SetTypeToString("XX",2,1);
		m_CE_ATtyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
	}	
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_AStyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_AStyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,0);

		if (bgServiceMandatory)
		{
			m_CE_AStyp.SetBKColor(YELLOW);
			m_CE_AStyp.SetTypeToString("XX",2,1);
		}
		if (bgNatureMandatory)
		{
			m_CE_ATtyp.SetBKColor(YELLOW);
			m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
		}		
	}//UFIS-1097 - End

	
//	m_CE_AHtyp.SetTypeToString("XX",2,0);
	
	CString olTmp;
	olTmp.Format("X(%d)", igLengthStev);
	m_CE_AStev.SetTypeToString(olTmp,igLengthStev,0);
	m_CE_ASte2.SetTypeToString("XXX",3,0);
	m_CE_ASte3.SetTypeToString("XXX",3,0);
	m_CE_ASte4.SetTypeToString("XXX",3,0);

	m_CE_AOrg3.SetTypeToString("XXX");
	m_CE_AOrg4.SetTypeToString("XXXX");
	m_CE_AStoa.SetTypeToTime(true, true);
	m_CE_AStod.SetTypeToTime(false, true);
	m_CE_AEtai.SetTypeToTime(false, true);

	m_CE_ANfes.SetTypeToTime(false, true);


	m_CE_APsta.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_APabs.SetTypeToTime(false, true);
	m_CE_APaes.SetTypeToTime(false, true);
	
	
	m_CE_AGta1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AGta2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATga1.SetTypeToString("x",1,0);
	m_CE_ATga2.SetTypeToString("x",1,0);
	m_CE_AGa1b.SetTypeToTime(false, true);
	m_CE_AGa2b.SetTypeToTime(false, true);
	m_CE_AGa1e.SetTypeToTime(false, true);
	m_CE_AGa2e.SetTypeToTime(false, true);
	m_CE_ACsta.SetTypeToTime(false, true);

	
	m_CE_ABlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ABlt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATmb1.SetTypeToString("x",1,0);
	m_CE_ATmb2.SetTypeToString("x",1,0);
	m_CE_AB1bs.SetTypeToTime(false, true);
	m_CE_AB2bs.SetTypeToTime(false, true);
	m_CE_AB1es.SetTypeToTime(false, true);
	m_CE_AB2es.SetTypeToTime(false, true);

	if (bgUseDepBelts) {
		m_CE_DBlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
		m_CE_DB1es.SetTypeToTime(false, true);
		m_CE_DB1bs.SetTypeToTime(false, true);
		m_CE_DTmb1.SetTypeToString("x",1,0);
	}


	// Disable the default menu which appear by rightclicking
	//    on the CCSEdit -box
	m_CE_AB1bs.UnsetDefaultMenu();
	m_CE_AB1es.UnsetDefaultMenu();
	m_CE_AB2bs.UnsetDefaultMenu();
	m_CE_AB2es.UnsetDefaultMenu();
	m_CE_DB1bs.UnsetDefaultMenu();
	m_CE_DB1es.UnsetDefaultMenu();
	
	m_CE_AExt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AExt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATet1.SetTypeToString("x",1,0);
	m_CE_ATet2.SetTypeToString("x",1,0);

	m_CE_ARem1.SetTextLimit(0,255,true);
	m_CE_ARem2.SetTextLimit(0,255,true);
/*		m_CE_ARem1.SetFont(&ogCourier_Regular_9);
		m_CE_ARem1.GetWindowRect(olRect);
		m_CE_ARem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
//		m_CE_DRem1.SetFont(&ogCourier_Regular_9);
//		m_CE_DRem1.GetWindowRect(olRect);
//		m_CE_DRem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);


		m_CE_ARem2.SetFont(&ogCourier_Regular_9);
//		m_CE_DRem2.SetFont(&ogCourier_Regular_9);
		m_CE_ARem2.GetWindowRect(olRect);
		m_CE_ARem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
//		m_CE_DRem2.GetWindowRect(olRect);
//		m_CE_DRem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
*/
	m_CE_ACsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	
	//Abflug
	//

/*	
//	pomDViaCtrl = new ViaTableCtrl(this,"APC3,FIDS,STOA,STOD");
//	pomDViaCtrl = new ViaTableCtrl(this,"HEAD,APC3,FIDS,STOA,ETA,ATA,STOD,ETD,ATD");
	

//	pomDViaCtrl->SetSecStat(ogPrivList.GetStat("SEASONDLG_CB_DShowVia"));


	m_CE_DOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	olRect.top = olRect.top;
	olRect.bottom = 0;
	olRect.left = olRect.right + 15;
	olRect.right = 0;

	pomDViaCtrl->SetPosition(olRect);
*/	
	
	m_CE_DAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_DFltn.SetTypeToString("#####",5,1);
	m_CE_DFlns.SetTypeToString("X",1,0);
	m_CE_DFlti.SetTypeToString("A",1,0);
	m_CE_DDays.SetTypeToInt(1,9999999);
	

	if (!bgCheckPRFL)
	{
		m_CE_DCht3.ShowWindow(SW_HIDE);
		m_CS_DCht3.ShowWindow(SW_HIDE);
	}
	else
	{
		m_CE_DCht3.SetTypeToString("X",1,0);
		m_CE_DCht3.ShowWindow(SW_SHOW);
		m_CS_DCht3.ShowWindow(SW_SHOW);
	}

	/*   //UFIS-1097 - Commented out - Start
	if(bgNatureAutoCalc)
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetBKColor(YELLOW);
		m_CE_DStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_DTtyp.SetBKColor(YELLOW);
		m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
		m_CE_DStyp.SetTypeToString("XX",2,0);

	}
    */  //UFIS-1097 - Commented out - End

	if(bgNatureServiceMand)
	{
		m_CE_DStyp.SetBKColor(YELLOW);
		m_CE_DStyp.SetTypeToString("XX",2,1);
		m_CE_DTtyp.SetBKColor(YELLOW);
		m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
	}
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_DStyp.SetBKColor(YELLOW);
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_DStyp.SetBKColor(YELLOW);
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetTypeToString("XX",2,0);

		if (bgServiceMandatory)
		{
			m_CE_DStyp.SetBKColor(YELLOW);
			m_CE_DStyp.SetTypeToString("XX",2,1);
		}
		if (bgNatureMandatory)
		{
			m_CE_DTtyp.SetBKColor(YELLOW);
			m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
		}		
	}//UFIS-1097 - End

//	m_CE_DHtyp.SetTypeToString("XX",2,0);
	
	olTmp.Format("X(%d)", igLengthStev);
	m_CE_DStev.SetTypeToString(olTmp,igLengthStev,0);
	m_CE_DStev.SetTypeToString("XXXX",3,0);
	m_CE_DStev.SetTypeToString("XXXX",3,0);
	m_CE_DStev.SetTypeToString("XXXX",3,0);

	m_CE_DDes3.SetTypeToString("XXX");
	m_CE_DDes4.SetTypeToString("XXXX");
	m_CE_DStod.SetTypeToTime(true, true);
	m_CE_DStoa.SetTypeToTime(false, true);
	m_CE_DEtdi.SetTypeToTime(false, true);

	m_CE_DPstd.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DPdbs.SetTypeToTime(false, true);
	m_CE_DPdes.SetTypeToTime(false, true);

	m_CE_DGtd1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DGtd2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTgd1.SetTypeToString("x",1,0);
	m_CE_DTgd2.SetTypeToString("x",1,0);
	m_CE_DGd1b.SetTypeToTime(false, true);
	m_CE_DGd2b.SetTypeToTime(false, true);
	m_CE_DGd1e.SetTypeToTime(false, true);
	m_CE_DGd2e.SetTypeToTime(false, true);
	
	m_CE_DWro1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DWro2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTwr1.SetTypeToString("x",1,0);
	m_CE_DTwr2.SetTypeToString("x",1,0);
	m_CE_DW1bs.SetTypeToTime(false, true);
	m_CE_DW1es.SetTypeToTime(false, true);
	m_CE_DW2bs.SetTypeToTime(false, true);
	m_CE_DW2es.SetTypeToTime(false, true);

	m_CE_DNfes.SetTypeToTime(false, true);
	m_CE_DCstd.SetTypeToTime(false, true);

	m_CE_DRem1.SetTextLimit(0,255,true);
	m_CE_DRem2.SetTextLimit(0,255,true);
/*		m_CE_DRem1.SetFont(&ogCourier_Regular_9);
		m_CE_DRem1.GetWindowRect(olRect);
		m_CE_DRem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);

		m_CE_DRem2.SetFont(&ogCourier_Regular_9);
		m_CE_DRem2.GetWindowRect(olRect);
		m_CE_DRem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
*/
	m_CE_DCsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	m_CE_Regn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#",12,0);

	m_CL_ACxxReason.SetDroppedWidth( 200 );	
	m_CL_DCxxReason.SetDroppedWidth( 200 );	

    CRect r;

    m_CL_ACxxReason.GetClientRect(&r);
    m_CL_ACxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_DCxxReason.GetClientRect(&r);
    m_CL_DCxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

 
	if(bgUseRequestedForPrognosis)	
	{
		CRect r2,r3;

		m_CB_AStatus.GetWindowRect(r);
		m_CB_AStatus2.GetWindowRect(r2);
		m_CB_AStatus3.GetWindowRect(r3);

		ScreenToClient(r);
		ScreenToClient(r2);
		ScreenToClient(r3);

		m_CB_AStatus3.MoveWindow(r);
		m_CB_AStatus.MoveWindow(r2);
		m_CB_AStatus2.MoveWindow(r3);

		m_CB_DStatus.GetWindowRect(r);
		m_CB_DStatus2.GetWindowRect(r2);
		m_CB_DStatus3.GetWindowRect(r3);

		ScreenToClient(r);
		ScreenToClient(r2);
		ScreenToClient(r3);

		m_CB_DStatus3.MoveWindow(r);
		m_CB_DStatus.MoveWindow(r2);
		m_CB_DStatus2.MoveWindow(r3);

		m_CB_AStatus3.SetWindowText("Requested");
		m_CB_DStatus3.SetWindowText("Requested");

		DWORD dwStyle1 = WS_GROUP|WS_CHILD|BS_AUTORADIOBUTTON | WS_VISIBLE  ;
		DWORD dwStyle2 = WS_CHILD|BS_AUTORADIOBUTTON | WS_VISIBLE ;

		m_CB_AStatus3.SetButtonStyle(dwStyle1);
		m_CB_DStatus3.SetButtonStyle(dwStyle1);

		m_CB_AStatus.SetButtonStyle(dwStyle2);
		m_CB_DStatus.SetButtonStyle(dwStyle2);
		m_CB_DStatus2.SetButtonStyle(dwStyle2);
		m_CB_DStatus2.SetButtonStyle(dwStyle2);
	}

            
	SetSecState();	

	omOpenTime = CTime::GetCurrentTime();

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_AREMP,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DREMP,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);

	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	//SetTabsOrder(); // not working after a few controls



	//Set the keycode and arousel labels
	SetLabelForKeyCarousel();

	
	SetFixedControls();

	
	

	m_dlgAModeOfPay = NULL;
	m_dlgDModeOfPay = NULL;

	SetCashButtonInBothFlights();

	ArrangeTabsOrder();
	m_CB_Ok.SetFocus();

	CCS_CATCH_ALL

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSeasonDlg::SetTabsOrder()
{
	m_shift = FALSE;//AM:20110405 For Tab Order

	m_tabOrderCnt=0;
	
	
	AddItemForTabOrder(&m_CE_Pvom);
	AddItemForTabOrder(&m_CE_Pbis);
	AddItemForTabOrder(&m_CE_Freq);
	AddItemForTabOrder(&m_CE_Seas);
	AddItemForTabOrder(&m_CE_Regn);
	AddItemForTabOrder(&m_CE_Act3);
	AddItemForTabOrder(&m_CE_Act5);	
	AddItemForTabOrder(&m_CB_Act35List); 
	AddItemForTabOrder(&m_CE_Nose);
	AddItemForTabOrder(&m_CE_Ming);
	AddItemForTabOrder(&m_CB_AAlc3List);// Arrival : Flight 
	AddItemForTabOrder(&m_CE_AAlc3);
	AddItemForTabOrder(&m_CE_AFltn);
	AddItemForTabOrder(&m_CE_AFlns);
	AddItemForTabOrder(&m_CE_AFlti);
	AddItemForTabOrder(&m_CE_ACsgn);// Arrival : Call sign
	AddItemForTabOrder(&m_CB_DAlc3List);// Departure :  Flight  
	AddItemForTabOrder(&m_CE_DAlc3);
	AddItemForTabOrder(&m_CE_DFltn);
	AddItemForTabOrder(&m_CE_DFlns);
	AddItemForTabOrder(&m_CE_DFlti);
	AddItemForTabOrder(&m_CE_DCsgn);// Departure : Call sign
	AddItemForTabOrder(&m_CB_AAlc3List2); //Arrival :Code Share Flights
	AddItemForTabOrder(&m_CB_AAlc3List3);
	AddItemForTabOrder(&m_CB_AShowJfno); // InitDialog
	AddItemForTabOrder(&m_CE_ADays);
	AddItemForTabOrder(&m_CB_ADaly); // Arrival : Daily
	AddItemForTabOrder(&m_CB_DAlc3List2); //Departure :Code Share Flights
	AddItemForTabOrder(&m_CB_DAlc3List3);
	AddItemForTabOrder(&m_CB_DShowJfno);// Departure
	AddItemForTabOrder(&m_CE_DDays);
	AddItemForTabOrder(&m_CB_DDaly);
	AddItemForTabOrder(&m_CE_ACht3); //Arrival :Prfl // EnableArrival 
	AddItemForTabOrder(&m_CB_ACxx);
	AddItemForTabOrder(&m_CL_ACxxReason);
	AddItemForTabOrder(&m_CB_ANoop);
	AddItemForTabOrder(&m_CE_DCht3); //Departure :Prfl 
	AddItemForTabOrder(&m_CB_DCxx);
	AddItemForTabOrder(&m_CL_DCxxReason);
	AddItemForTabOrder(&m_CB_DNoop);
	AddItemForTabOrder(&m_CB_ATTypList);//Arrival :Nature
	AddItemForTabOrder(&m_CE_ATtyp);
	AddItemForTabOrder(&m_CE_AStyp);
	AddItemForTabOrder(&m_CB_AStypList);
	AddItemForTabOrder(&m_CE_AStev);
	AddItemForTabOrder(&m_CE_ASte2);
	AddItemForTabOrder(&m_CE_ASte3);
	AddItemForTabOrder(&m_CE_ASte4);
	AddItemForTabOrder(&m_CB_DTtypList);//Departure :Nature 
	AddItemForTabOrder(&m_CE_DTtyp);
	AddItemForTabOrder(&m_CE_DStyp);
	AddItemForTabOrder(&m_CB_DStypList);
	AddItemForTabOrder(&m_CE_DStev);
	AddItemForTabOrder(&m_CE_DSte2);
	AddItemForTabOrder(&m_CE_DSte3);
	AddItemForTabOrder(&m_CE_DSte4);
	AddItemForTabOrder(&m_CB_AOrg3List);//Arrival :Origin
	AddItemForTabOrder(&m_CE_AOrg3);
	AddItemForTabOrder(&m_CE_AOrg4);
	AddItemForTabOrder(&m_CB_AVia);
	AddItemForTabOrder(&m_CE_ADes3); 
	AddItemForTabOrder(&m_CE_DOrg3);//Departure :Home
	AddItemForTabOrder(&m_CB_DVia);
	AddItemForTabOrder(&m_CB_DDes3List);
	AddItemForTabOrder(&m_CE_DDes3);
	AddItemForTabOrder(&m_CE_DDes4); 
	AddItemForTabOrder(&m_CE_AStod);//Arrival :STD
	AddItemForTabOrder(&m_CE_ACsta);
	AddItemForTabOrder(&m_CE_AStoa);
	AddItemForTabOrder(&m_CE_DStod);// Departure
	AddItemForTabOrder(& m_CE_DCstd);
	AddItemForTabOrder(&m_CE_DCstd);
	AddItemForTabOrder(&m_CB_APstaList);//Arrival :Position
	AddItemForTabOrder(&m_CE_APsta);
	AddItemForTabOrder(&m_CE_ADura);
	AddItemForTabOrder(&m_CE_APabs);
	AddItemForTabOrder(&m_CE_APaes);
	AddItemForTabOrder(&m_CB_AGta1List);//Arrival :Port/ Gate
	AddItemForTabOrder(&m_CE_AGta1);
	AddItemForTabOrder(&m_CE_ATga1);
	AddItemForTabOrder(&m_CE_AGa1b);
	AddItemForTabOrder(&m_CE_AGa1e);
	AddItemForTabOrder(&m_CB_AGta2List);
	AddItemForTabOrder(&m_CE_AGta2);
	AddItemForTabOrder(&m_CE_ATga2);
	AddItemForTabOrder(&m_CE_AGa2b);
	AddItemForTabOrder(&m_CE_AGa2e);
	AddItemForTabOrder(&m_CB_DPstdList);//Departure :Position 
	AddItemForTabOrder(&m_CE_DPstd);
	AddItemForTabOrder(&m_CE_DPdbs);
	AddItemForTabOrder(&m_CE_DDura);
	AddItemForTabOrder(&m_CE_DPdes);
	AddItemForTabOrder(&m_CB_DBlt1List);//Departure :Baggage Belts
	AddItemForTabOrder(&m_CE_DBlt1);
	AddItemForTabOrder(&m_CE_DTmb1);
	AddItemForTabOrder(&m_CE_DB1bs);
	AddItemForTabOrder(&m_CE_DB1es);
	AddItemForTabOrder(&m_CB_DGtd1List);//Departure :Gates
	AddItemForTabOrder(&m_CE_DGtd1);
	AddItemForTabOrder(&m_CE_DTgd1);
	AddItemForTabOrder(&m_CE_DGd1b);
	AddItemForTabOrder(&m_CE_DGd1e);
	AddItemForTabOrder(&m_CB_DGtd2List);
	AddItemForTabOrder(&m_CE_DGtd2);
	AddItemForTabOrder(&m_CE_DTgd2);
	AddItemForTabOrder(&m_CE_DGd2b);
	AddItemForTabOrder(&m_CE_DGd2e);
	AddItemForTabOrder(&m_CB_ABlt1List);// Arrival : Baggage Belts
	AddItemForTabOrder(&m_CE_ABlt1);
	AddItemForTabOrder(&m_CE_ATmb1);
	AddItemForTabOrder(&m_CE_AB1bs);
	AddItemForTabOrder(&m_CE_AB1es);
	AddItemForTabOrder(&m_CB_ABlt2List);
	AddItemForTabOrder(&m_CE_ABlt2);
	AddItemForTabOrder(&m_CE_ATmb2);
	AddItemForTabOrder(&m_CE_AB2bs);
	AddItemForTabOrder(&m_CE_AB2es);
	AddItemForTabOrder(&m_CB_AExt1List);// Arrival :Exits
	AddItemForTabOrder(&m_CE_AExt1);
	AddItemForTabOrder(&m_CE_ATet1);
	AddItemForTabOrder(&m_CB_AExt2List);
	AddItemForTabOrder(&m_CE_AExt2);
	AddItemForTabOrder(&m_CE_ATet2);
	AddItemForTabOrder(&m_CB_DCinsList1);//Departure : CKI Counter
	AddItemForTabOrder(&m_CB_DCinsList2);
	AddItemForTabOrder(&m_CB_DCinsList3);
	AddItemForTabOrder(&m_CB_DCinsList4);
	AddItemForTabOrder(&m_CB_DCinsMal4);
	AddItemForTabOrder(&m_CE_DCiFr);
	AddItemForTabOrder(&m_CE_DCinsBorderExt);
	AddItemForTabOrder(&m_CB_DWro1List);//Departure :Lounges
	AddItemForTabOrder(&m_CE_DWro1);
	AddItemForTabOrder(&m_CE_DTwr1);
	AddItemForTabOrder(&m_CE_DW1bs);
	AddItemForTabOrder(&m_CE_DW1es);
	AddItemForTabOrder(&m_CB_DWro2List);
	AddItemForTabOrder(&m_CE_DWro2);
	AddItemForTabOrder(&m_CE_DTwr2);
	AddItemForTabOrder(&m_CE_DW2bs);
	AddItemForTabOrder(&m_CE_DW2es);
	AddItemForTabOrder(&m_CE_DBaz1);//Departure :Carousel 1/2
	AddItemForTabOrder(&m_CE_DBaz4);
	AddItemForTabOrder(&m_CB_AStatus);// Arrival : Combo box :Planning
	AddItemForTabOrder(&m_CB_DStatus);// Departure : Combo box :Planning
	AddItemForTabOrder(&m_CB_ARemp); // Arrival : FIDS Comment
	AddItemForTabOrder(&m_ComboAHistory); // Arrival : History
	AddItemForTabOrder(&m_CB_DRemp); // Departure:  FIDS Comment
	AddItemForTabOrder(&m_ComboDHistory); // Departure:   History
	AddItemForTabOrder(&m_CE_ARem1); // Arrival :  Comment Flight
	AddItemForTabOrder(&m_CE_ARem2); // Arrival :  Comment Season
	AddItemForTabOrder(&m_CE_DRem1); // Departure:  Comment Flight
	AddItemForTabOrder(&m_CE_DRem2); // Departure:  Comment Season
	AddItemForTabOrder(&m_CE_ACreated); // Arrival : Entered
	AddItemForTabOrder(&m_CE_ALastChange); //Arrival :  Most Recent Change
	AddItemForTabOrder(&m_CE_DCreated); // Departure:  Entered
	AddItemForTabOrder(&m_CE_DLastChange); // Departure:  Most Recent Change
	AddItemForTabOrder(&m_CE_AFluko); // Arrival Flico
	AddItemForTabOrder(&m_CE_DFluko); // Departure Flico

}

void CSeasonDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	if (this->pomDCinsTable != NULL && ::IsWindow(this->pomDCinsTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_DCinsBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		if (bgCnamAtr)
		{
			m_CE_DCinsBorderExt.GetWindowRect(&olrectTable);
			ScreenToClient(olrectTable);
			olrectTable.DeflateRect(2,2);     // hiding the CTable window border
			this->pomDCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_CE_DCinsBorderExt.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorder.right = olRectBorderExt.right;
			m_CE_DCiFr.MoveWindow(olRectBorder,TRUE);

		}

		this->pomDCinsTable->DisplayTable();
	}

	if (this->pomDJfnoTable != NULL && ::IsWindow(this->pomDJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_DJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomDJfnoTable->DisplayTable();
	}

	if (this->pomAJfnoTable != NULL && ::IsWindow(this->pomAJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_AJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomAJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomAJfnoTable->DisplayTable();
	}

	if(m_CL_ACxxReason.m_hWnd != NULL)
	{

		m_CL_ACxxReason.SetDroppedWidth( 200 );	
		m_CL_DCxxReason.SetDroppedWidth( 200 );	

		CRect r;

		m_CL_ACxxReason.GetClientRect(&r);
		m_CL_ACxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_CL_DCxxReason.GetClientRect(&r);
		m_CL_DCxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

	}


	if(pomArrPermitsButton)
	{
		CRect tmpR;
		m_CB_AAlc3List.GetWindowRect(tmpR);
		//ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		tmpR.left -= 27;
		tmpR.right -= 27;
		tmpR.top += 23;
		pomArrPermitsButton->SetWindowPos(this, tmpR.left, tmpR.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	}


	if(pomDepPermitsButton)
	{
		CRect tmpR;
		m_CB_DAlc3List.GetWindowRect(tmpR);
		//ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		tmpR.left -= 27;
		tmpR.right -= 27;
		tmpR.top += 23;
		pomDepPermitsButton->SetWindowPos(this, tmpR.left, tmpR.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	}




	this->Invalidate();

}


void CSeasonDlg::ClearActualTimes()
{
	if (prmAFlight)
	{
		//arr
		prmAFlight->Urno = 0;
		prmAFlight->Rkey = 0;
		prmAFlight->Skey = 0;

		prmAFlight->Cdat = TIMENULL;
		prmAFlight->Lstu = TIMENULL;
		prmAFlight->Tifa = TIMENULL;
		prmAFlight->Tifd = TIMENULL;

		strcpy(prmAFlight->Usec, "");
		strcpy(prmAFlight->Useu, "");
		strcpy(prmAFlight->Dooa, "");
		strcpy(prmAFlight->Dood, "");
		strcpy(prmAFlight->Tisa, "");
		strcpy(prmAFlight->Tisd, "");
		strcpy(prmAFlight->Seas, "");

		prmAFlight->B1ba = TIMENULL;
		prmAFlight->B1ea = TIMENULL;
		prmAFlight->B2ba = TIMENULL;
		prmAFlight->B2ea = TIMENULL;

		prmAFlight->Ga1x = TIMENULL;
		prmAFlight->Ga1y = TIMENULL;
		prmAFlight->Ga2x = TIMENULL;
		prmAFlight->Ga2y = TIMENULL;

		prmAFlight->Gd1x = TIMENULL;
		prmAFlight->Gd1y = TIMENULL;
		prmAFlight->Gd2x = TIMENULL;
		prmAFlight->Gd2y = TIMENULL;

		prmAFlight->Paba = TIMENULL;
		prmAFlight->Paea = TIMENULL;
		prmAFlight->Pdba = TIMENULL;
		prmAFlight->Pdea = TIMENULL;

		prmAFlight->W1ba = TIMENULL;
		prmAFlight->W1ea = TIMENULL;
		prmAFlight->W2ba = TIMENULL;
		prmAFlight->W2ea = TIMENULL;
		prmAFlight->Ofbl = TIMENULL;
		prmAFlight->Onbl = TIMENULL;

		strcpy(prmAFlight->Prfl, "");
	}

	if (prmDFlight)
	{
		//dep
		prmDFlight->Urno = 0;
		prmDFlight->Rkey = 0;
		prmDFlight->Skey = 0;

		prmDFlight->Cdat = TIMENULL;
		prmDFlight->Lstu = TIMENULL;
		prmDFlight->Tifa = TIMENULL;
		prmDFlight->Tifd = TIMENULL;

		strcpy(prmDFlight->Usec, "");
		strcpy(prmDFlight->Useu, "");
		strcpy(prmDFlight->Dooa, "");
		strcpy(prmDFlight->Dood, "");
		strcpy(prmDFlight->Tisa, "");
		strcpy(prmDFlight->Tisd, "");
		strcpy(prmDFlight->Seas, "");

		prmDFlight->B1ba = TIMENULL;
		prmDFlight->B1ea = TIMENULL;
		prmDFlight->B2ba = TIMENULL;
		prmDFlight->B2ea = TIMENULL;

		prmDFlight->Ga1x = TIMENULL;
		prmDFlight->Ga1y = TIMENULL;
		prmDFlight->Ga2x = TIMENULL;
		prmDFlight->Ga2y = TIMENULL;

		prmDFlight->Gd1x = TIMENULL;
		prmDFlight->Gd1y = TIMENULL;
		prmDFlight->Gd2x = TIMENULL;
		prmDFlight->Gd2y = TIMENULL;

		prmDFlight->Paba = TIMENULL;
		prmDFlight->Paea = TIMENULL;
		prmDFlight->Pdba = TIMENULL;
		prmDFlight->Pdea = TIMENULL;

		prmDFlight->W1ba = TIMENULL;
		prmDFlight->W1ea = TIMENULL;
		prmDFlight->W2ba = TIMENULL;
		prmDFlight->W2ea = TIMENULL;
		prmDFlight->Ofbl = TIMENULL;
		prmDFlight->Onbl = TIMENULL;

		strcpy(prmDFlight->Prfl, "");
	}
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the fields and show data in the dialog
bool CSeasonDlg::ConvertStructUtcTOLocal(SEASONDLGFLIGHTDATA *prpAFlight, SEASONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight) 
	{
		ogSeasonDlgFlights.StructUtcToLocal((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpAFlight, true, true);
			ConvertUtcTOLocal (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogSeasonDlgFlights.StructUtcToLocal((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpDFlight, false, true);
			ConvertUtcTOLocal (prpDFlight, false);
		}
	}

	return true;
}

bool CSeasonDlg::ConvertStructLocalTOUtc(SEASONDLGFLIGHTDATA *prpAFlight, SEASONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight)
	{
		ogSeasonDlgFlights.StructLocalToUtc((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpAFlight, true, true);
			ConvertLocalTOUtc (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogSeasonDlgFlights.StructLocalToUtc((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpDFlight, false, true);
			ConvertLocalTOUtc (prpDFlight, false);
		}
	}

	return true;
}

bool CSeasonDlg::ConvertUtcTOLocal (SEASONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(prpFlight->Org4));
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

bool CSeasonDlg::ConvertLocalTOUtc (SEASONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(prpFlight->Org4));
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

//
void CSeasonDlg::InitDialog(bool bpArrival, bool bpDeparture) 
{

	
	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	bmActSelect = true;

	int ilLc ;
	if(imModus == DLG_NEW)
	{
		if(prmDFlight != NULL)
			delete prmDFlight;
		if(prmAFlight != NULL)
			delete prmAFlight;
		prmAFlight = new SEASONDLGFLIGHTDATA;
		prmDFlight = new SEASONDLGFLIGHTDATA;
	}

	if(prmAFlight != NULL)
	{
		//*rkr
		//strcpy(prmAFlight->Prfl,"Z");
		bmAnkunft = true;
		bmAFltiUserSet = false;
		if ((int) strlen(prmAFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmAFlight->Dssf[ogDssfFltiId] == 'U')
				bmAFltiUserSet = true;
		}
		
		bmANatureUserSet = false;
		if ((int) strlen(prmAFlight->Dssf) > ogDssfTtypId && ogDssfTtypId != -1)
		{
			if (prmAFlight->Dssf[ogDssfTtypId] == 'U')
				bmANatureUserSet = true;
		}
		
	}

	if(prmDFlight != NULL)
	{
		bmAbflug = true;
		bmDFltiUserSet = false;
		if ((int) strlen(prmDFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmDFlight->Dssf[ogDssfFltiId] == 'U')
				bmDFltiUserSet = true;
		}
		
		bmDNatureUserSet = false;
		if ((int) strlen(prmDFlight->Dssf) > ogDssfTtypId && ogDssfTtypId != -1)
		{
			if (prmDFlight->Dssf[ogDssfTtypId] == 'U')
				bmDNatureUserSet = true;
		}
	
	}


	if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
	{
		if(bmLocalTime)
			ConvertStructUtcTOLocal(prmAFlight, prmDFlight);
//		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
//		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmAFlight);
	}

	if(imModus == DLG_COPY)
	{
		if(bmLocalTime)
			ConvertStructUtcTOLocal(prmAFlight, prmDFlight);

		ClearActualTimes();
		*prmAFlightSave = *prmAFlight;
		*prmDFlightSave = *prmDFlight;
		strcpy(prmAFlight->Csgn, "");
		strcpy(prmDFlight->Csgn, "");
	}

	

// Global  ///////////////////////////////////////////////////////////////////

	if(bpArrival && bpDeparture)
	{
		if((imModus == DLG_NEW) || (imModus == DLG_COPY))
			m_Freq = "1";
		else
			m_Freq = "";

		if(prmAFlight->Urno > 0 || (imModus == DLG_COPY))
		{
			m_Regn = CString(prmAFlight->Regn);
			m_Act3 = CString(prmAFlight->Act3);
			m_Act5 = CString(prmAFlight->Act5);
			m_Ming = CString(prmAFlight->Ming);
			m_Nose = CString(prmAFlight->Nose);
		}

		if(imModus == DLG_NEW)
		{
			strcpy(prmDFlight->Regn,"");
			strcpy(prmDFlight->Act3,"");
			strcpy(prmDFlight->Act5,"");
			strcpy(prmDFlight->Ming,"");
			strcpy(prmDFlight->Nose,"");

			m_Regn = "";
			m_Act3 = "";
			m_Act5 = "";
			m_Ming = "";
			m_Nose = "";

			strcpy(prmAFlight->Regn,"");
			strcpy(prmAFlight->Act3,"");
			strcpy(prmAFlight->Act5,"");
			strcpy(prmAFlight->Ming,"");
			strcpy(prmAFlight->Nose,"");

		}
		
		if (prmDFlight->Urno > 0 || (imModus == DLG_COPY) )
		{
			if( CString(prmAFlight->Act3).IsEmpty() && CString(prmAFlight->Act5).IsEmpty())
			{
				m_Regn = CString(prmDFlight->Regn);
				m_Act3 = CString(prmDFlight->Act3);
				m_Act5 = CString(prmDFlight->Act5);
				m_Ming = CString(prmDFlight->Ming);
				m_Nose = CString(prmDFlight->Nose);
			}
		}

		m_Seas = CString(prmAFlight->Seas);

		if(m_Seas.IsEmpty())
			m_Seas = CString(prmDFlight->Seas);
		else
			m_Seas += CString(" / ") + CString(prmDFlight->Seas);

		m_CE_Freq.SetInitText(m_Freq);
		m_CE_Seas.SetInitText(m_Seas);
		m_CE_Ming.SetInitText(m_Ming);
		m_CE_Act3.SetInitText(m_Act3);
		m_CE_Act5.SetInitText(m_Act5);
		m_CE_Regn.SetInitText(m_Regn);
		m_CE_Nose.SetInitText(m_Nose);

	}


// Ankunft ///////////////////////////////////////////////////////////////////


	CString olTmp; 
	if((prmAFlight->Urno == prmDFlight->Urno) && (prmAFlight->Urno != 0))
	{
		SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
		*prmDFlight = *prlFlight;
		*prmDFlightSave = *prlFlight;
		delete prlFlight;
		bpDeparture = true;
	}

	if (prmDFlight->Stod != TIMENULL && prmAFlight->Stoa != TIMENULL)
	{
		m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");
		m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");
	}
	else
	{
		if (prmAFlight->Stoa != TIMENULL)
		{
			m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");
			m_Pbis = prmAFlight->Stoa.Format("%d.%m.%Y");
		}
		else if (prmDFlight->Stod != TIMENULL)
		{
			m_Pvom = prmDFlight->Stod.Format("%d.%m.%Y");
			m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");
		}
		else
		{
			if(imModus == DLG_NEW)
			{

				if ((strcmp(pcgHome, "WAW") == 0))		
				{
					m_Pvom = CString("");
					m_Pbis = CString("");
				}
				else
				{

					CTime olAct = CTime::GetCurrentTime();
					m_Pvom = olAct.Format("%d.%m.%Y");
					m_Pbis = olAct.Format("%d.%m.%Y");
				}			
			}
			else
			{
				CTime olAct = CTime::GetCurrentTime();
				m_Pvom = olAct.Format("%d.%m.%Y");
				m_Pbis = olAct.Format("%d.%m.%Y");
			}
		}
	}



	if(bpArrival)
	{

		omAAlc3 = CString(prmAFlight->Alc3);
		omAAlc2 = CString(prmAFlight->Alc2);

		m_AAlc3 = CString(prmAFlight->Flno);
		m_AAlc3 = m_AAlc3.Left(3);
		m_AAlc3.TrimRight();
		m_AFltn = CString(prmAFlight->Fltn);
		m_AFlns = CString(prmAFlight->Flns);
		m_AFlti = prmAFlight->Flti;

		m_CE_AFlti.SetBKColor(RGB(255,255,255));
		if (bmAFltiUserSet)
		{
 			m_CE_AFlti.SetBKColor(RGB(255,0,0));
		}
			

		if(bgNatureAutoCalc)
		{
			m_CE_ATtyp.SetBKColor(RGB(255,255,255));
			if(bmANatureUserSet)
			{
				m_CE_ATtyp.SetBKColor(RGB(255,0,0));
			}
		}

		if(IsCircular(prmAFlight->Org3, prmAFlight->Des3)) 
		{
			olTmp = GetString(IDS_STRING1388) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
			{
				olTmp += prmAFlight->Stoa.Format("%d.%m.%Y");
				m_CE_AStod.SetBKColor(YELLOW);
				m_CE_AStod.SetTypeToTime(true, true);
				m_CE_AStod.SetTextLimit(4,4,false);
			}
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,4,true);
			olTmp = GetString(IDS_STRING1389) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
				olTmp += prmAFlight->Stoa.Format("%d.%m.%Y");
		}
	
		if(ogFpeData.bmFlightPermitsEnabled)
		{
			if(!pomArrPermitsButton)
			{
				CRect tmpR;
				m_CB_AAlc3List2.GetClientRect(tmpR);
				m_CB_AAlc3List2.ClientToScreen(tmpR);
				this->ScreenToClient(tmpR);
				tmpR.left -= 25;
				tmpR.right -= 25;

				pomArrPermitsButton = new CCSButtonCtrl;
				pomArrPermitsButton->Create( "", SW_HIDE | WS_CHILD | BS_OWNERDRAW, tmpR, this, IDC_ARRPERMITSBUTTON ); 
			}
			SetArrPermitsButton();
		}
			
//		m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");

		m_CS_Ankunft.SetWindowText(olTmp);

		m_ADays = "";
		m_CE_ADays.SetInitText(m_ADays);


		if(prmAFlight->Stoa == TIMENULL)
			omARefDat = CTime::GetCurrentTime();
		else
			omARefDat = prmAFlight->Stoa;



		if(bgNightFlightSlot)
		{
			if(prmAFlight->Nfes != TIMENULL)
			{
				m_CE_ANfes.ShowWindow(SW_SHOW);
				m_CB_ANfes.EnableWindow(TRUE);
				m_CE_ANfes.EnableWindow(TRUE);
				m_CE_ANfes.SetInitText( DateToHourDivString(prmAFlight->Nfes, omARefDat) );
				m_CB_ANfes.SetCheck(TRUE);
			}
			else
			{
				m_CE_ANfes.SetInitText( "" );
				m_CE_ANfes.ShowWindow(SW_HIDE);
				m_CE_ANfes.EnableWindow(FALSE);
				m_CB_ANfes.SetCheck(FALSE);
			}
		}

		if (bgSeasonFixedRes) 
		{
			if(strcmp(prmAFlight->Fpsa, "X") == 0)
				pomCBFPSA->SetCheck(TRUE);
			else
				pomCBFPSA->SetCheck(FALSE);

			if(strcmp(prmAFlight->Fbl1, "X") == 0)
				pomCBFBL1->SetCheck(TRUE);
			else
				pomCBFBL1->SetCheck(FALSE);

			if(strcmp(prmAFlight->Fbl2, "X") == 0)
				pomCBFBL2->SetCheck(TRUE);
			else
				pomCBFBL2->SetCheck(FALSE);

			if(strcmp(prmAFlight->Fga1, "X") == 0)
				pomCBFGA1->SetCheck(TRUE);
			else
				pomCBFGA1->SetCheck(FALSE);
		
			if(strcmp(prmAFlight->Fga2, "X") == 0)
				pomCBFGA2->SetCheck(TRUE);
			else
				pomCBFGA2->SetCheck(FALSE);
		}
	
		

		// Weitere Flugnummern
		ogSeasonDlgFlights.GetJfnoArray(&omAJfno, prmAFlight);
		
		pomAJfnoTable->ResetContent();
		
		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;

		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for (ilLc = 0; ilLc < omAJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omAJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omAJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		pomAJfnoTable->DisplayTable();
		
		
		// Vias
		CString olCaption = CString(prmAFlight->Flno);
		olCaption.TrimRight();
		if (olCaption.IsEmpty())
			olCaption = CString(prmAFlight->Csgn);

		if (!polRotationAViaDlg)
		{
			bool blEnable = true;
			if(imModus == DLG_CHANGE_DIADATA || ogPrivList.GetStat("SEASONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", olCaption, prmAFlight->Urno, prmAFlight->Vial, omARefDat, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AShowVia"));
		}
		else
			polRotationAViaDlg->SetData("A", olCaption, prmAFlight->Urno, prmAFlight->Vial, omARefDat, bmLocalTime);


		if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(false);
		}
		else
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(true);
		}


		m_ACxx = FALSE;
		m_CB_ACxx.SetCheck(FALSE);
		m_ANoop = FALSE;
		m_CB_ANoop.SetCheck(FALSE);
		m_AStatus = -1;
		m_CB_AStatus.SetCheck(FALSE);
		m_CB_AStatus2.SetCheck(FALSE);
		m_CB_AStatus3.SetCheck(FALSE);
		
		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_ACxx = TRUE;
			m_CB_ACxx.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "N") == 0)
		{
			m_ANoop = TRUE;
			m_CB_ANoop.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "S") == 0)
		{
			m_AStatus = 0;
			m_CB_AStatus.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "O") == 0)
		{
			m_AStatus = 1;
			m_CB_AStatus2.SetCheck(TRUE);
		}
		if((strcmp(prmAFlight->Ftyp, "") == 0) && (strlen(prmAFlight->Vers) > 0))
		{
			m_AStatus = 2;
			m_CB_AStatus3.SetCheck(TRUE);
		}
		if((strcmp(prmAFlight->Ftyp, "") == 0) && (strlen(prmAFlight->Vers) == 0))
		{
			m_AStatus = 0;
			m_CB_AStatus.SetCheck(TRUE);
		}

		if(bgCxxReason)
		{
			if( CString(prmAFlight->Ftyp)	== "X" )
			{
				if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '-')
				{
					m_CL_ACxxReason.ShowWindow(SW_HIDE);
				}
				else
				{
					m_CL_ACxxReason.ShowWindow(SW_SHOW);
					if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '0')
					{
						m_CL_ACxxReason.EnableWindow(FALSE);
					}
				}
			}
			else
			{
				m_CL_ACxxReason.ShowWindow(SW_HIDE);
			}
		}

		

//*rkr
		m_ACht3 = CString(prmAFlight->Prfl);

		m_ADes3 = CString(pcgHome);
		m_AOrg3 = CString(prmAFlight->Org3);
		m_ATtyp = CString(prmAFlight->Ttyp);
//		m_AHtyp = CString(prmAFlight->Htyp);
		m_AStyp = CString(prmAFlight->Styp);
		m_AStev = CString(prmAFlight->Stev);
		m_ASte2 = CString(prmAFlight->Ste2);
		m_ASte3 = CString(prmAFlight->Ste3);
		m_ASte4 = CString(prmAFlight->Ste4);
		m_AOrg3 = CString(prmAFlight->Org3);
		m_AOrg4 = CString(prmAFlight->Org4);
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ARem1 = CString(prmAFlight->Rem1);
		m_ARem2 = CString(prmAFlight->Rem2);


		char pclDays[10];
		GetDayOfWeek(prmAFlight->Stoa, pclDays);
		m_ADays = CString(pclDays);
			
		m_ACsta = DateToHourDivString(prmAFlight->Csta, omARefDat);
		m_AEtai = DateToHourDivString(prmAFlight->Etai, omARefDat);
		m_AStod = DateToHourDivString(prmAFlight->Stod, omARefDat);
		m_AStoa = DateToHourDivString(prmAFlight->Stoa, omARefDat);
		m_AFluko      = prmAFlight->Fdat.Format("%d.%m.%Y  %H:%M");
		m_ACreated    = prmAFlight->Cdat.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmAFlight->Usec);
		m_ALastChange = prmAFlight->Lstu.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmAFlight->Useu);

		m_APabs = DateToHourDivString(prmAFlight->Pabs, omARefDat);
		m_APaes = DateToHourDivString(prmAFlight->Paes, omARefDat);
		m_AGa1b = DateToHourDivString(prmAFlight->Ga1b, omARefDat);
		m_AGa2b = DateToHourDivString(prmAFlight->Ga2b, omARefDat);
		m_AGa1e = DateToHourDivString(prmAFlight->Ga1e, omARefDat);
		m_AGa2e = DateToHourDivString(prmAFlight->Ga2e, omARefDat);
		m_AB1bs = DateToHourDivString(prmAFlight->B1bs, omARefDat);
		m_AB2bs = DateToHourDivString(prmAFlight->B2bs, omARefDat);
		m_AB1es = DateToHourDivString(prmAFlight->B1es, omARefDat);
		m_AB2es = DateToHourDivString(prmAFlight->B2es, omARefDat);
		
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ATga1 = CString(prmAFlight->Tga1);
		m_ATga2 = CString(prmAFlight->Tga2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_ATmb1 = CString(prmAFlight->Tmb1);
		m_ATmb2 = CString(prmAFlight->Tmb2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ATet1 = CString(prmAFlight->Tet1);
		m_ATet2 = CString(prmAFlight->Tet2);

		m_ACsgn = CString(prmAFlight->Csgn);
//		if(strlen(prmAFlight->Regn) != 0)
//			m_Regn = CString(prmAFlight->Regn);


		if(imModus != DLG_CHANGE)
		{
			m_CE_Pvom.SetInitText(m_Pvom);
			m_CE_Regn.SetInitText("");
		}
		else
		{
			m_CE_Pvom.SetInitText("");
			m_CE_Regn.SetInitText(m_Regn);
		}


		m_CE_ADays.SetInitText(m_ADays);
		m_CE_ACht3.SetInitText(m_ACht3);
		m_CE_ADes3.SetInitText(pcgHome);
 		m_CE_ADes3.SetInitText(pcgHome + CString(" - ") + pcgHome4);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_AOrg4.SetInitText(m_AOrg4);
		m_CE_AAlc3.SetInitText(m_AAlc3);
		m_CE_AFltn.SetInitText(m_AFltn);
		m_CE_AFlns.SetInitText(m_AFlns);
		m_CE_AFlti.SetInitText(m_AFlti);
		m_CE_ATtyp.SetInitText(m_ATtyp);
//		m_CE_AHtyp.SetInitText(m_AHtyp);
		m_CE_AStyp.SetInitText(m_AStyp);
		m_CE_AStev.SetInitText(m_AStev);
		m_CE_ASte2.SetInitText(m_ASte2);
		m_CE_ASte3.SetInitText(m_ASte3);
		m_CE_ASte4.SetInitText(m_ASte4);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_ARem1.SetInitText(m_ARem1);
		m_CE_ARem2.SetInitText(m_ARem2);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AStod.SetInitText(m_AStod);
		m_CE_AStoa.SetInitText(m_AStoa);
		m_CE_ACsta.SetInitText(m_ACsta);


		m_CE_APsta.SetInitText(m_APsta);
		m_CE_APabs.SetInitText(m_APabs);
		m_CE_APaes.SetInitText(m_APaes);



	////////////// CXX Reason

	if(prmAFlight != NULL && bgCxxReason)
	{


		m_CL_ACxxReason.ResetContent();
		m_CL_ACxxReason.AddString(" ");

		CString olCxxr = CString(prmAFlight->Cxxr);
		olCxxr.TrimLeft();

		int ilDIndex = 0;
		CString olItem;
		int ilCount = ogBCD.GetDataCount("CRC_CXX");
		ogBCD.SetSort("CRC_CXX", "CODE+", true);
		for(int i = 0; i < ilCount; i++)
		{
			olItem = ogBCD.GetFields("CRC_CXX",i , "CODE,REMA,URNO" , "   ", FILLBLANK);
			m_CL_ACxxReason.AddString(olItem);
			if( olItem.Find(olCxxr) >= 0 && !olCxxr.IsEmpty())
				ilDIndex = i + 1;
		}
		


		m_CL_ACxxReason.SetCurSel(ilDIndex);

	}	
	////////////// Cxx Reason end


	




		//is an open leg
		if (prmDFlight->Urno == 0 && prmAFlight->Onbl != TIMENULL)
			m_CE_APaes.SetReadOnly(TRUE);
		else
			m_CE_APaes.SetReadOnly(FALSE);

		if (!blDatPosDura)
		{
			m_CE_ADura.ShowWindow(SW_HIDE);
			CWnd* polWnd = (CWnd*) GetDlgItem(IDC_STATIC_ADURA);
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}
		else
		{
/*			strcpy(prmAFlight->Baa4,"80");
			strcpy(prmAFlight->Baa5,"60");
			strcpy(prmDFlight->Baa4,"40");
			strcpy(prmDFlight->Baa5,"20");*/
			m_CE_ADura.SetInitText(CString(prmAFlight->Baa4));
			m_CE_ADura.SetReadOnly(TRUE);
			m_CE_ADura.ShowWindow(SW_SHOW);
			CWnd* polWnd = (CWnd*) GetDlgItem(IDC_STATIC_ADURA);
			if (polWnd)
				polWnd->ShowWindow(SW_SHOW);
		}

		m_CE_AGta1.SetInitText(m_AGta1);
		m_CE_AGta2.SetInitText(m_AGta2);
		m_CE_ATga1.SetInitText(m_ATga1);
		m_CE_ATga2.SetInitText(m_ATga2);
		m_CE_AGa1b.SetInitText(m_AGa1b);
		m_CE_AGa2b.SetInitText(m_AGa2b);
		m_CE_AGa1e.SetInitText(m_AGa1e);
		m_CE_AGa2e.SetInitText(m_AGa2e);
		m_CE_ABlt1.SetInitText(m_ABlt1);
		m_CE_ABlt2.SetInitText(m_ABlt2);
		m_CE_ATmb1.SetInitText(m_ATmb1);
		m_CE_ATmb2.SetInitText(m_ATmb2);
		m_CE_AB1bs.SetInitText(m_AB1bs);
		m_CE_AB2bs.SetInitText(m_AB2bs);
		m_CE_AB1es.SetInitText(m_AB1es);
		m_CE_AB2es.SetInitText(m_AB2es);
		m_CE_AExt1.SetInitText(m_AExt1);
		m_CE_AExt2.SetInitText(m_AExt2);
		m_CE_ATet1.SetInitText(m_ATet1);
		m_CE_ATet2.SetInitText(m_ATet2);


		if (bgUseDepBelts) {
			m_CE_DBlt1.SetInitText(m_DBlt1);
			m_CE_DB1bs.SetInitText(m_DB1bs);
			m_CE_DB1es.SetInitText(m_DB1es);
			m_CE_DTmb1.SetInitText(m_DTmb1);
		}

		m_CE_AFluko.SetInitText(m_AFluko);
		m_CE_ACreated.SetInitText(m_ACreated);
		m_CE_ALastChange.SetInitText(m_ALastChange);

		m_CE_ACsgn.SetInitText(m_ACsgn);
//		m_CE_Regn.SetInitText(m_Regn);

		
	}




// Abflug ///////////////////////////////////////////////////////////////////
	if(bpDeparture)
	{


		if (strlen(prmDFlight->Wro1) > 0 && prmDFlight->W1bs == TIMENULL && prmDFlight->W1es == TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			prmDFlight->W1es = prmDFlight->Tifd;
			
			CTimeSpan olDura;
			GetWroDefAllocDur(prmDFlight->Wro1, olDura);
			prmDFlight->W1bs = prmDFlight->Tifd-olDura;
		}
		if (strlen(prmDFlight->Wro2) > 0 && prmDFlight->W2bs == TIMENULL && prmDFlight->W2es == TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			prmDFlight->W2es = prmDFlight->Tifd;
			
			CTimeSpan olDura;
			GetWroDefAllocDur(prmDFlight->Wro2, olDura);
			prmDFlight->W2bs = prmDFlight->Tifd-olDura;
		}


		if(ogFpeData.bmFlightPermitsEnabled)
		{
			if(!pomDepPermitsButton)
			{
				CRect tmpR;
				m_CB_DAlc3List2.GetClientRect(tmpR);
				m_CB_DAlc3List2.ClientToScreen(tmpR);
				this->ScreenToClient(tmpR);
				tmpR.left -= 25;
				tmpR.right -= 25;

				pomDepPermitsButton = new CCSButtonCtrl;
				pomDepPermitsButton->Create( "", SW_HIDE | WS_CHILD | BS_OWNERDRAW, tmpR, this, IDC_DEPPERMITSBUTTON ); 
			}
			SetDepPermitsButton();
		}

		omDAlc3 = CString(prmDFlight->Alc3);
		omDAlc2 = CString(prmDFlight->Alc2);

		m_DAlc3 = CString(prmDFlight->Flno);
		m_DAlc3 = m_DAlc3.Left(3);
		m_DAlc3.TrimRight();
		m_DFltn = CString(prmDFlight->Fltn);
		m_DFlns = CString(prmDFlight->Flns);
		m_DFlti = prmDFlight->Flti;

		m_CE_DFlti.SetBKColor(RGB(255,255,255));
		if (bmDFltiUserSet)
		{
	 		m_CE_DFlti.SetBKColor(RGB(255,0,0));
		}

		if(bgNatureAutoCalc)
		{
			m_CE_DTtyp.SetBKColor(RGB(255,255,255));
			if(bmDNatureUserSet)
			{
				m_CE_DTtyp.SetBKColor(RGB(255,0,0));
			}
		}

		if(IsArrival(prmDFlight->Org3, prmDFlight->Des3)) 
		{
			olTmp = GetString(IDS_STRING1388) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
			{
				olTmp += prmDFlight->Stod.Format("%d.%m.%Y");
				m_CE_DStoa.SetBKColor(YELLOW);
				m_CE_DStoa.SetTypeToTime(true, true);
				m_CE_DStoa.SetTextLimit(4,4,false);
			}
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,4,true);
			olTmp = GetString(IDS_STRING1390);
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
				olTmp += prmDFlight->Stod.Format("%d.%m.%Y");
		}

		m_CS_Abflug.SetWindowText(olTmp);
		
		
//		m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");

		if(prmDFlight->Stod == TIMENULL)
			omDRefDat = CTime::GetCurrentTime();
		else
			omDRefDat = prmDFlight->Stod;

		m_DOrg3 = CString(pcgHome);



		if(bgNightFlightSlot)
		{
			if(prmDFlight->Nfes != TIMENULL)
			{
				m_CE_DNfes.ShowWindow(SW_SHOW);
				m_CB_DNfes.EnableWindow(TRUE);
				m_CE_DNfes.EnableWindow(TRUE);
				m_CE_DNfes.SetInitText( DateToHourDivString(prmDFlight->Nfes, omDRefDat) );
				m_CB_DNfes.SetCheck(TRUE);
			}
			else
			{
				m_CE_DNfes.SetInitText( "");
				m_CE_DNfes.ShowWindow(SW_HIDE);
				m_CE_DNfes.EnableWindow(FALSE);
				m_CB_DNfes.SetCheck(FALSE);
			}
		}

		if(bgSeasonFixedRes)
		{
			if(strcmp(prmDFlight->Fpsd, "X") == 0)
				pomCBFPSD->SetCheck(TRUE);
			else
				pomCBFPSD->SetCheck(FALSE);

			if(strcmp(prmDFlight->Fgd1, "X") == 0)
				pomCBFGD1->SetCheck(TRUE);
			else
				pomCBFGD1->SetCheck(FALSE);

			if(strcmp(prmDFlight->Fgd2, "X") == 0)
				pomCBFGD2->SetCheck(TRUE);
			else
				pomCBFGD2->SetCheck(FALSE);
		}

		//Weitere Flugnummern

		ogSeasonDlgFlights.GetJfnoArray(&omDJfno, prmDFlight);

		pomDJfnoTable->ResetContent();
		

		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 2;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 1;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;


		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;

		if(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for ( ilLc = 0; ilLc < omDJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omDJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		pomDJfnoTable->DisplayTable();


		// Vias
		CString olCaption = CString(prmDFlight->Flno);
		olCaption.TrimRight();
		if (olCaption.IsEmpty())
			olCaption = CString(prmDFlight->Csgn);

		if (!polRotationDViaDlg)
		{
			bool blEnable = true;
			if(imModus == DLG_CHANGE_DIADATA || ogPrivList.GetStat("SEASONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, omDRefDat, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DShowVia"));
		}
		else
			polRotationDViaDlg->SetData("D", olCaption, prmDFlight->Urno, prmDFlight->Vial, omDRefDat, bmLocalTime);

		if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
		{
			if (polRotationDViaDlg)
				polRotationDViaDlg->Enable(false);
		}
		else
		{
			if (polRotationDViaDlg)
				polRotationDViaDlg->Enable(true);
		}

		m_DCxx = FALSE;
		m_CB_DCxx.SetCheck(FALSE);
		m_DNoop = FALSE;
		m_CB_DNoop.SetCheck(FALSE);
		m_DStatus = -1;
		m_CB_DStatus.SetCheck(FALSE);
		m_CB_DStatus2.SetCheck(FALSE);
		m_CB_DStatus3.SetCheck(FALSE);
		

		m_CB_DGd2d.SetCheck(FALSE);
		if(strcmp(prmDFlight->Gd2d, "X") == 0)
		{
			m_DGd2d = TRUE;
			m_CB_DGd2d.SetCheck(TRUE);
		}
		

		if(strcmp(prmDFlight->Ftyp, "X") == 0)
		{
			m_DCxx = TRUE;
			m_CB_DCxx.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "N") == 0)
		{
			m_DNoop = TRUE;
			m_CB_DNoop.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "S") == 0)
		{
			m_DStatus = 0;
			m_CB_DStatus.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "O") == 0)
		{
			m_DStatus = 1;
			m_CB_DStatus2.SetCheck(TRUE);
		}

		if((strcmp(prmDFlight->Ftyp, "") == 0) && (strlen(prmDFlight->Vers) > 0))
		{
			m_DStatus = 2;
			m_CB_DStatus3.SetCheck(TRUE);
		}
		if((strcmp(prmDFlight->Ftyp, "") == 0) && (strlen(prmDFlight->Vers) == 0))
		{
			m_DStatus = 0;
			m_CB_DStatus.SetCheck(TRUE);
		}

		
		if(bgCxxReason)
		{
			if( CString(prmDFlight->Ftyp)	== "X" )
			{
				if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '-')
				{
					m_CL_DCxxReason.ShowWindow(SW_HIDE);
				}
				else
				{
					m_CL_DCxxReason.ShowWindow(SW_SHOW);
					if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '0')
					{
						m_CL_DCxxReason.EnableWindow(FALSE);
					}
				}
			}
			else
			{
				m_CL_DCxxReason.ShowWindow(SW_HIDE);
			}
		}

		if( (imModus == DLG_NEW) && (strcmp(pcgHome, "AUH") == 0) )
		{

			m_CB_AStatus3.SetCheck(TRUE);
			m_CB_AStatus2.SetCheck(FALSE);
			m_CB_AStatus.SetCheck(FALSE);

			m_CB_DStatus3.SetCheck(TRUE);
			m_CB_DStatus2.SetCheck(FALSE);
			m_CB_DStatus.SetCheck(FALSE);
		}

		m_DCht3 = CString(prmDFlight->Prfl);
		m_DOrg3 = CString(pcgHome);
		m_DDes3 = CString(prmDFlight->Des3);
		m_DDes4 = CString(prmDFlight->Des4);
		m_DTtyp = CString(prmDFlight->Ttyp);
//		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DSte2 = CString(prmDFlight->Ste2);
		m_DSte3 = CString(prmDFlight->Ste3);
		m_DSte4 = CString(prmDFlight->Ste4);
		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);

		m_DRem1 = CString(prmDFlight->Rem1);
		m_DRem2 = CString(prmDFlight->Rem2);

		char pclDays[10];
		GetDayOfWeek(prmDFlight->Stod, pclDays);
		m_DDays = CString(pclDays);


		m_DEtdi = DateToHourDivString(prmDFlight->Etdi, omDRefDat);
		m_DStod = DateToHourDivString(prmDFlight->Stod, omDRefDat);
		m_DStoa = DateToHourDivString(prmDFlight->Stoa, omDRefDat);
		m_DCstd = DateToHourDivString(prmDFlight->Cstd, omDRefDat);

		m_DFluko      = prmDFlight->Fdat.Format("%d.%m.%Y  %H:%M");
		m_DCreated    = prmDFlight->Cdat.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmDFlight->Usec);
		m_DLastChange = prmDFlight->Lstu.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmDFlight->Useu);

		m_DPdbs = DateToHourDivString(prmDFlight->Pdbs, omDRefDat);
		m_DPdes = DateToHourDivString(prmDFlight->Pdes, omDRefDat);
		m_DGd1b = DateToHourDivString(prmDFlight->Gd1b, omDRefDat);
		m_DGd2b = DateToHourDivString(prmDFlight->Gd2b, omDRefDat);
		m_DGd1e = DateToHourDivString(prmDFlight->Gd1e, omDRefDat);
		m_DGd2e = DateToHourDivString(prmDFlight->Gd2e, omDRefDat);
		m_DW1bs = DateToHourDivString(prmDFlight->W1bs, omDRefDat);
		m_DW1es = DateToHourDivString(prmDFlight->W1es, omDRefDat);
		m_DW2bs = DateToHourDivString(prmDFlight->W2bs, omDRefDat);
		m_DW2es = DateToHourDivString(prmDFlight->W2es, omDRefDat);

		m_DDays = CString(prmDFlight->Dood);
		m_DTtyp = CString(prmDFlight->Ttyp);
//		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DSte2 = CString(prmDFlight->Ste2);
		m_DSte3 = CString(prmDFlight->Ste3);
		m_DSte4 = CString(prmDFlight->Ste4);
		m_DDes3 = CString(prmDFlight->Des3);
		m_DPstd = CString(prmDFlight->Pstd);

		if (!m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
		{
			CTimeSpan olDura;
			GetPosDefAllocDur(m_DPstd, atoi(prmDFlight->Ming), olDura);

			if (prmDFlight->Tifd != TIMENULL)
			{
				m_DPdes = DateToHourDivString(prmDFlight->Tifd, omDRefDat);
				m_DPdbs = DateToHourDivString(prmDFlight->Tifd - olDura, omDRefDat);
			}
			else
			{
				m_DPdes = DateToHourDivString(prmDFlight->Stod, omDRefDat);
				m_DPdbs = DateToHourDivString(prmDFlight->Stod - olDura, omDRefDat);
			}
		}


		if (!blDatPosDura)
		{
			m_CE_DDura.ShowWindow(SW_HIDE);
			CWnd* polWnd = (CWnd*) GetDlgItem(IDC_STATIC_DDURA);
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}
		else
		{
			m_CE_DDura.SetInitText(CString(prmDFlight->Baa5));
			m_CE_DDura.SetReadOnly(TRUE);
			m_CE_DDura.ShowWindow(SW_SHOW);
			CWnd* polWnd = (CWnd*) GetDlgItem(IDC_STATIC_DDURA);
			if (polWnd)
				polWnd->ShowWindow(SW_SHOW);
		}


		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);
		m_DTgd1 = CString(prmDFlight->Tgd1);
		m_DTgd2 = CString(prmDFlight->Tgd2);
		m_DWro1 = CString(prmDFlight->Wro1);
		m_DWro2 = CString(prmDFlight->Wro2);
		m_DTwr1 = CString(prmDFlight->Twr1);
//		m_DTwr2 = CString(prmDFlight->Twr1);
		CString olTerm;
		ogBCD.GetField("WRO", "WNAM", m_DWro2, "TERM", olTerm);
		m_DTwr2 = CString(olTerm);



		if(prmDFlight != NULL && bgCxxReason)
		{

			m_CL_DCxxReason.ResetContent();
			m_CL_DCxxReason.AddString(" ");

			CString olCxxr = CString(prmDFlight->Cxxr);
			olCxxr.TrimLeft();

			int ilDIndex = 0;
			CString olItem;
			int ilCount = ogBCD.GetDataCount("CRC_CXX");
			ogBCD.SetSort("CRC_CXX", "CODE+", true);
			for(int i = 0; i < ilCount; i++)
			{
				olItem = ogBCD.GetFields("CRC_CXX",i , "CODE,REMA,URNO" , "   ", FILLBLANK);
				m_CL_DCxxReason.AddString(olItem);
				if( olItem.Find(olCxxr) >= 0 && !olCxxr.IsEmpty())
					ilDIndex = i + 1;
			}
			
			m_CL_DCxxReason.SetCurSel(ilDIndex);

		}	


		if(imModus == DLG_CHANGE)
		{
			if (bgCnamAtr)
			{
				m_DCiFr = GetCnamExt(CString(prmDFlight->Ckif), CString(prmDFlight->Ckit));
			}
			else
			{
				if(CString(prmDFlight->Ckf2).IsEmpty())
					m_DCiFr = CString(prmDFlight->Ckif) + CString("-") + CString(prmDFlight->Ckit);
				else
					m_DCiFr = CString(prmDFlight->Ckif) + CString("-") + CString(prmDFlight->Ckit) + CString(" / ") + CString(prmDFlight->Ckf2) + CString("-") + CString(prmDFlight->Ckt2);
			}
		}
		else
		{
			m_DCiFr = CString("") + CString("-") + CString("");
		}


		m_DCsgn = CString(prmDFlight->Csgn);
//		if(strlen(prmDFlight->Regn) != 0)
//			m_Regn = CString(prmDFlight->Regn);

		if(imModus != DLG_CHANGE)
		{
			m_CE_Pbis.SetInitText(m_Pbis);
			m_CE_Regn.SetInitText("");
		}
		else
		{
			m_CE_Pbis.SetInitText("");
			m_CE_Regn.SetInitText(m_Regn);
		}


		GetDayOfWeek(prmDFlight->Stod, pclDays);
		m_DDays = CString(pclDays);


		m_CE_DCiFr.SetInitText(m_DCiFr);
		m_CE_DCht3.SetInitText(m_DCht3);
		m_CE_DDays.SetInitText(m_DDays);
		m_CE_DOrg3.SetInitText(m_DOrg3);
 		m_CE_DOrg3.SetInitText(pcgHome + CString(" - ") + pcgHome4);
		m_CE_DDes3.SetInitText(m_DDes3);
		m_CE_DDes4.SetInitText(m_DDes4);
		m_CE_DAlc3.SetInitText(m_DAlc3);
		m_CE_DFltn.SetInitText(m_DFltn);
		m_CE_DFlns.SetInitText(m_DFlns);
		m_CE_DFlti.SetInitText(m_DFlti);
		m_CE_DTtyp.SetInitText(m_DTtyp);
//		m_CE_DHtyp.SetInitText(m_DHtyp);
		m_CE_DStyp.SetInitText(m_DStyp);
		m_CE_DStev.SetInitText(m_DStev);
		m_CE_DSte2.SetInitText(m_DSte2);
		m_CE_DSte3.SetInitText(m_DSte3);
		m_CE_DSte4.SetInitText(m_DSte4);
		m_CE_DRem1.SetInitText(m_DRem1);
		m_CE_DRem2.SetInitText(m_DRem2);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DStod.SetInitText(m_DStod);
		m_CE_DStoa.SetInitText(m_DStoa);
		m_CE_DCstd.SetInitText(m_DCstd);

		m_CE_DPstd.SetInitText(m_DPstd);
		m_CE_DPdbs.SetInitText(m_DPdbs);
		m_CE_DPdes.SetInitText(m_DPdes);
		m_CE_DGtd1.SetInitText(m_DGtd1);
		m_CE_DGtd2.SetInitText(m_DGtd2);
		m_CE_DTgd1.SetInitText(m_DTgd1);
		m_CE_DTgd2.SetInitText(m_DTgd2);
		m_CE_DGd1b.SetInitText(m_DGd1b);
		m_CE_DGd2b.SetInitText(m_DGd2b);
		m_CE_DGd1e.SetInitText(m_DGd1e);
		m_CE_DGd2e.SetInitText(m_DGd2e);
		m_CE_DWro1.SetInitText(m_DWro1);
		m_CE_DWro2.SetInitText(m_DWro2);
		m_CE_DTwr1.SetInitText(m_DTwr1);
		m_CE_DTwr2.SetInitText(m_DTwr2);
		m_CE_DW1bs.SetInitText(m_DW1bs);
		m_CE_DW1es.SetInitText(m_DW1es);
		m_CE_DW2bs.SetInitText(m_DW2bs);
		m_CE_DW2es.SetInitText(m_DW2es);
		
		m_CE_DFluko.SetInitText(m_DFluko);
		m_CE_DCreated.SetInitText(m_DCreated);
		m_CE_DLastChange.SetInitText(m_DLastChange);


		m_DBaz1 = CString(prmDFlight->Baz1);
		m_DBaz4 = CString(prmDFlight->Baz4);

		m_CE_DBaz1.SetInitText(m_DBaz1);
		m_CE_DBaz4.SetInitText(m_DBaz4);

		m_CE_DCsgn.SetInitText(m_DCsgn);
//		m_CE_Regn.SetInitText(m_Regn);

		if (bgUseDepBelts) {
			m_DBlt1 = CString(prmDFlight->Blt1);
			m_DTmb1 = CString(prmDFlight->Tmb1);
			m_DB1bs = DateToHourDivString(prmDFlight->B1bs, prmDFlight->Stod);
			m_DB1es = DateToHourDivString(prmDFlight->B1es, prmDFlight->Stod);

		}

		if (bgUseDepBelts) {
			m_CE_DBlt1.SetInitText(m_DBlt1);
			m_CE_DB1bs.SetInitText(m_DB1bs);
			m_CE_DB1es.SetInitText(m_DB1es);
			m_CE_DTmb1.SetInitText(m_DTmb1);
		}

	
		

		if(!bmInit)
		{
			if(prmDFlight->Urno > 0)
			{
				omCcaData.Register();
				CString olFlnu;
				olFlnu.Format("%ld", prmDFlight->Urno);
				omCcaData.SetFilter(false, olFlnu);
				
				ReadCcaData();
			}
		}
		DShowCinsTable();

		bmAutoSetBaz1 = false;
		bmAutoSetBaz4 = false;

		CString olBaz1;
		CString olBaz4;
		CCADATA *prlCca;

		for( int i = 0; i < omDCins.GetSize(); i++)
		{
			prlCca = &omDCins[i];

			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					if(!olBaz1.IsEmpty())
						break;
				}
			}
		}

		if( CString(prmDFlight->Baz1) == olBaz1)
			bmAutoSetBaz1 = true;

		if( CString(prmDFlight->Baz4) == olBaz4)
			bmAutoSetBaz4 = true;


	}

	m_CB_ARemp.ResetContent();
	m_CB_DRemp.ResetContent();


	m_CB_ARemp.AddString(" ");
	m_CB_DRemp.AddString(" ");
	
	int ilAIndex = 0;	
	int ilDIndex = 0;	
	CString olCode;
	CString olItem;

	int ilCount = ogBCD.GetDataCount("FID_ARR");
	ogBCD.SetSort("FID_ARR", ogFIDRemarkField+"+", true);

	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID_ARR",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CB_ARemp.AddString(olItem);
		olCode = ogBCD.GetField("FID_ARR",i , "CODE" );
		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
	}

	ilCount = ogBCD.GetDataCount("FID_DEP");
	ogBCD.SetSort("FID_DEP", ogFIDRemarkField+"+", true);
	for(i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID_DEP",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CB_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID_DEP",i , "CODE" );
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	

	m_CB_ARemp.SetCurSel(ilAIndex);
	m_CB_DRemp.SetCurSel(ilDIndex);

/*	
	int ilCount = ogBCD.GetDataCount("FID");
	ogBCD.SetSort("FID", ogFIDRemarkField+"+", true);

	
	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID",i , "CODE,"+ogFIDRemarkField, "  ", FILLBLANK);
		m_CB_ARemp.AddString(olItem);
		m_CB_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID",i , "CODE" );

		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	
	m_CB_ARemp.SetCurSel(ilAIndex);
	m_CB_DRemp.SetCurSel(ilDIndex);
*/

	char clPaid= ' ';
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		
	}

//	clPaid = GetPaid();

	UpdateCashButton(olAlc3, clPaid);


	if(bpArrival && bpDeparture)
	{
		bmChanged = false;
		EnableGlobal();
		EnableArrival();
		EnableDeparture();
		EnableGatPos();
	}
	else
	{
		if(bpArrival)
		{
			EnableGlobal();
			EnableArrival();
		}
		else
		{
			EnableGlobal();
			EnableDeparture();
		}
	}


	if (prmAFlightSave != NULL)
	{
		if (prmAFlightSave->Jcnt[0] == '\0')	
			strcpy(prmAFlightSave->Jcnt, "0");
	}
	if (prmDFlightSave != NULL)
	{
		if (prmDFlightSave->Jcnt[0] == '\0')	
			strcpy(prmDFlightSave->Jcnt, "0");
	}

	CheckViaButton();
	CheckCodeShareButton();
	HandlePostFlight();

	SetHistoryNamesForEdit();
	InitHistoryCombos();

	InitDcinsCombo();

	if(imModus == DLG_COPY)
	{
		if(!bmChanged)
			m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		bmChanged = true;
	}

	if (bgCnamAtr)
	{
		m_CE_DCinsBorder.ShowWindow(SW_HIDE);

		if (ogFactor_CCA.IsEmpty())
		{
			m_ComboDcins.ShowWindow(SW_HIDE);
		}

		if (!ogFactor_CCA.IsEmpty())
		{
			m_CE_DCinsBorder.ShowWindow(SW_HIDE);
			m_ComboDcins.ShowWindow(SW_SHOW);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_ComboDcins.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorderExt.top = olRectBorder.top;
			m_ComboDcins.MoveWindow(olRectBorderExt,TRUE);
		}
		else
		{
			m_CE_DCinsBorder.ShowWindow(SW_HIDE);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_CE_DCinsBorderExt.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorder.right = olRectBorderExt.right;
			m_CE_DCiFr.MoveWindow(olRectBorder,TRUE);
		}
	}
	else
	{
		m_CE_DCinsBorderExt.ShowWindow(SW_HIDE);
	}

	m_CB_Ok.SetFocus();

}


void CSeasonDlg::SetArrPermitsButton()
{
	if(pomArrPermitsButton == NULL)
		return;

	if(imModus != DLG_NEW && ogFpeData.FtypEnabled(prmAFlight->Ftyp))
	{
		pomArrPermitsButton->EnableWindow(TRUE);
		FPEDATA *prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns, &omArrFlightPermitInfo);
		if(prlFpe != NULL)
			pomArrPermitsButton->SetColors(LIME,GRAY,WHITE);
		else
			pomArrPermitsButton->SetColors(RED,GRAY,WHITE);
	}
	else
	{
		pomArrPermitsButton->EnableWindow(FALSE);
		pomArrPermitsButton->SetColors(GRAY,GRAY,WHITE);
	}
}

void CSeasonDlg::SetDepPermitsButton()
{
	if(pomDepPermitsButton == NULL)
		return;

	if(imModus != DLG_NEW && ogFpeData.FtypEnabled(prmDFlight->Ftyp))
	{
		pomDepPermitsButton->EnableWindow(TRUE);
		FPEDATA *prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns, &omDepFlightPermitInfo);
		if(prlFpe != NULL)
			pomDepPermitsButton->SetColors(LIME,GRAY,WHITE);
		else
			pomDepPermitsButton->SetColors(RED,GRAY,WHITE);
	}
	else
	{
		pomDepPermitsButton->EnableWindow(FALSE);
		pomDepPermitsButton->SetColors(GRAY,GRAY,WHITE);
	}
}

bool CSeasonDlg::SetHistoryNamesForEdit()
{
	//if(prmAFlight->Urno > 0) 
	{
		m_CE_AAlc3.SetName("AFTAALC3C");
		m_CE_AFltn.SetName("AFTAFLTNC");
		m_CE_AFlns.SetName("AFTAFLNSC");
		m_CE_AFlti.SetName("AFTAFLTIC");
		m_CE_ATtyp.SetName("AFTATTYPC");
		m_CE_AStyp.SetName("AFTASTYPC");
		m_CE_AStev.SetName("AFTASTEVC");
		m_CE_ASte2.SetName("AFTASTE2C");
		m_CE_ASte3.SetName("AFTASTE3C");
		m_CE_ASte4.SetName("AFTASTE4C");
		m_CE_AOrg3.SetName("AFTAORG3C");
		m_CE_AOrg4.SetName("AFTAORG4C");
		m_CE_AStoa.SetName("AFTASTOAD");
//		m_CE_ACSta.SetName("AFTACSTAD");
		m_CE_AStod.SetName("AFTASTODD");
		m_CE_ACsta.SetName("AFTACSTAD");
		m_CE_AEtai.SetName("AFTAETAID");
		m_CE_APsta.SetName("AFTAPSTAC");
		m_CE_APabs.SetName("AFTAPABSD");
		m_CE_APaes.SetName("AFTAPAESD");
		m_CE_AGta1.SetName("AFTAGTA1C");
		m_CE_AGta2.SetName("AFTAGTA2C");
		m_CE_AGa1b.SetName("AFTAGA1BD");
		m_CE_AGa2b.SetName("AFTAGA2BD");
		m_CE_AGa1e.SetName("AFTAGA1ED");
		m_CE_AGa2e.SetName("AFTAGA2ED");
		m_CE_ABlt1.SetName("AFTABLT1C");
		m_CE_ABlt2.SetName("AFTABLT2C");
		m_CE_AB1bs.SetName("AFTAB1BSD");
		m_CE_AB2bs.SetName("AFTAB2BSD");
		m_CE_AB1es.SetName("AFTAB1ESD");
		m_CE_AB2es.SetName("AFTAB2ESD");
		m_CE_AExt1.SetName("AFTAEXT1C");
		m_CE_AExt2.SetName("AFTAEXT2C");
		m_CE_ARem1.SetName("AFTAREM1C");
		m_CE_ARem2.SetName("AFTAREM2C");
		m_CE_ACsgn.SetName("AFTACSGNC");
		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");
	}
	//if(prmAFlight->Urno > 0) 
	{
		m_CE_DAlc3.SetName("AFTDALC3C");
		m_CE_DFltn.SetName("AFTDFLTNC");
		m_CE_DFlns.SetName("AFTDFLNSC");
		m_CE_DFlti.SetName("AFTDFLTIC");
		m_CE_DTtyp.SetName("AFTDTTYPC");
		m_CE_DStyp.SetName("AFTDSTYPC");
		m_CE_DStev.SetName("AFTDSTEVC");
		m_CE_DSte2.SetName("AFTDSTE2C");
		m_CE_DSte3.SetName("AFTDSTE3C");
		m_CE_DSte4.SetName("AFTDSTE4C");
		m_CE_DDes3.SetName("AFTDDES3C");
		m_CE_DDes4.SetName("AFTDDES4C");
		m_CE_DStoa.SetName("AFTDSTOAD");
		m_CE_DStod.SetName("AFTDSTODD");
		m_CE_DCstd.SetName("AFTDCSTDD");
//		m_CE_DCStd.SetName("AFTDCSTDD");
		m_CE_DEtdi.SetName("AFTDETDID");
		m_CE_DPstd.SetName("AFTDPSTDC");
		m_CE_DPdbs.SetName("AFTDPDBSD");
		m_CE_DPdes.SetName("AFTDPDESD");

		m_CE_DGtd1.SetName("AFTDGTD1C");
		m_CE_DGtd2.SetName("AFTDGTD2C");

		if (bgUseDepBelts) {
		m_CE_DBlt1.SetName("AFTABLT1C");
		m_CE_DTmb1.SetName("AFTATMB1C");
		m_CE_DB1bs.SetName("AFTAB1BAD");
		m_CE_DB1es.SetName("AFTAB1EAD");
		}

		if (strcmp(pcgHome, "ATH") == 0)
		{
			m_CE_DGtd1.SetName("AFTDGD1PC");
			m_CE_DGtd2.SetName("AFTDGD2PC");
		}
		else
		{
			m_CE_DGtd1.SetName("AFTDGTD1C");
			m_CE_DGtd2.SetName("AFTDGTD2C");
		}

		if (bgUseDepBelts) {
		m_CE_DBlt1.SetName("AFTABLT1C");
		m_CE_DB1bs.SetName("AFTAB1BAD");
		m_CE_DB1es.SetName("AFTAB1EAD");
		}

		m_CE_DGd1b.SetName("AFTDGD1BD");
		m_CE_DGd2b.SetName("AFTDGD2BD");
		m_CE_DGd1e.SetName("AFTDGD1ED");
		m_CE_DGd2e.SetName("AFTDGD2ED");
		m_CE_DWro1.SetName("AFTDWRO1C");
		m_CE_DW1bs.SetName("AFTDW1BSD");
		m_CE_DW1es.SetName("AFTDW1ESD");
		m_CE_DWro2.SetName("AFTDWRO2C");
		m_CE_DW2bs.SetName("AFTDW2BSD");
		m_CE_DW2es.SetName("AFTDW2ESD");
		m_CE_DBaz1.SetName("AFTDBAZ1C");
		m_CE_DBaz4.SetName("AFTDBAZ4C");
		m_CE_DRem1.SetName("AFTDREM1C");
		m_CE_DRem2.SetName("AFTDREM2C");
		m_CE_DCsgn.SetName("AFTDCSGNC");
		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");
	}

	return true;
}

bool CSeasonDlg::InitDcinsCombo()
{
	if(prmDFlight->Urno > 0) 
	{
		if (ogFactor_CCA.IsEmpty())
			m_ComboDcins.ShowWindow(SW_HIDE);


		m_ComboDcins.ResetContent();
		//CString olFieldList = GetString(IDS_DCINS);
		CString olFieldList = ogFactor_CCA;

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboDcins.AddString(olFieldArray.GetAt(l));
		}
		m_ComboDcins.SetCurSel(0);

	}

	return true;
}


bool CSeasonDlg::InitHistoryCombos()
{
	if(prmAFlight->Urno > 0) 
	{
		m_ComboAHistory.ResetContent();
		CString olFieldList = GetString(IDS_AHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboAHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	if(prmDFlight->Urno > 0) 
	{
		m_ComboDHistory.ResetContent();
		CString olFieldList = GetString(IDS_DHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboDHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	return true;
}


void CSeasonDlg::OnSelchangeAHistory() 
{
	CString olOperator;
	int ilCur = m_ComboAHistory.GetCurSel();
	m_ComboAHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_AHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}

void CSeasonDlg::OnSelchangeDHistory() 
{
	CString olOperator;
	int ilCur = m_ComboDHistory.GetCurSel();
	m_ComboDHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_DHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}

void CSeasonDlg::OnSelchangeDcins() 
{
	CString olOperator;
	int ilCur = m_ComboDcins.GetCurSel();
	m_ComboDcins.GetLBText(ilCur, olOperator);

//	CString olFieldList = GetString(IDS_DCINS);
	CString olFieldList = ogFactor_CCA;

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		ilFieldArray = ExtractItemList(olField, &olFieldArray, ' ');

		if (ilFieldArray == 2)
		{
			int ilFactor = atoi(olFieldArray.GetAt(1));
			CallDcins(ilFactor);
		}
	}
	else
		return;

}

bool CSeasonDlg::CallDcins(int ipFactor) 
{
	CCS_TRY

	int i = pomDCinsTable->pomListBox->GetTopIndex();

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;
	CString olCatr;
	CString olDisp;

	if (bgCnamAtr)
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCatr);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 4, olCkes);

		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->GetTextFieldValue(i, 5, olDisp);
		}
	}
	else
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkes);
		
		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->GetTextFieldValue(i, 4, olDisp);
		}
	}


	ogBCD.SetSort("CIC","CNAM+",true);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	if (bgCnamAtr)
	{
		if (j == ilCount)
		{
			for (int k=1; k<ipFactor; k++)
			{
				pomDCinsTable->SetIPValue(i + k, 0, "");
				pomDCinsTable->SetIPValue(i + k, 1, "");
				pomDCinsTable->SetIPValue(i + k, 2, "");
				
				if(bgShowClassInBatchDlg)
				{
					pomDCinsTable->SetIPValue(i + k, 3, "");
				}
			}
		}
		else
		{
			for (int k=1; k<ipFactor; k++)
			{
				if (ogBCD.GetField("CIC",j + k,"CNAM") != "NULL")
				{
					pomDCinsTable->SetIPValue(i + k, 0, ogBCD.GetField("CIC",j + k,"CNAM"));
					pomDCinsTable->SetIPValue(i + k, 1, ogBCD.GetField("CIC",j + k,"CATR"));
					pomDCinsTable->SetIPValue(i + k, 2, ogBCD.GetField("CIC",j + k,"TERM"));
					pomDCinsTable->SetIPValue(i + k, 3, olCkbs);
					pomDCinsTable->SetIPValue(i + k, 4, olCkes); 
					
					if(bgShowClassInBatchDlg)
					{
						pomDCinsTable->SetIPValue(i + k, 5, "DISP"); 
					}
				}
			}
		}
	}
	else
	{
		if (j == ilCount)
		{
			for (int k=1; k<ipFactor; k++)
			{
				pomDCinsTable->SetIPValue(i + k, 0, "");
				pomDCinsTable->SetIPValue(i + k, 1, "");
			}
		}
		else
		{
			for (int k=1; k<ipFactor; k++)
			{
				if (ogBCD.GetField("CIC",j + k,"CNAM") != "NULL")
				{
					pomDCinsTable->SetIPValue(i + k, 0, ogBCD.GetField("CIC",j + k,"CNAM"));
					pomDCinsTable->SetIPValue(i + k, 1, ogBCD.GetField("CIC",j + k,"TERM"));
					pomDCinsTable->SetIPValue(i + k, 2, olCkbs);
					pomDCinsTable->SetIPValue(i + k, 3, olCkes);
						if(bgShowClassInBatchDlg)
					{
						pomDCinsTable->SetIPValue(i + k, 4, "DISP"); 
					}
				}
			}
		}
	}

	CCS_CATCH_ALL

	return true;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the SEASONDLGFLIGHTDATArecords
//
void CSeasonDlg::FillFlightData() 
{

	//CCS_TRY

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	CTime olTime;

	///////////////////////////////////////////
	// globals
	//
	m_CE_Regn.GetWindowText(m_Regn);
	m_Regn.TrimLeft();
	m_Regn.TrimRight();

	m_CE_Pbis.GetWindowText(m_Pbis);
	m_CE_Pvom.GetWindowText(m_Pvom);

	m_CE_ADays.GetWindowText(m_ADays);
	m_CE_DDays.GetWindowText(m_DDays);
	
	char pclDays[10];

	if(	m_Pbis.IsEmpty())
	{
		m_CE_Pbis.SetWindowText(m_Pvom);
		m_Pbis = m_Pvom;
	}	
	
	
	m_CE_Freq.GetWindowText(m_Freq);
	m_CE_Ming.GetWindowText(m_Ming);
	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);
	m_CE_Nose.GetWindowText(m_Nose);


	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		if(m_Freq.IsEmpty())
			m_Freq = "1";
	}


	///////////////////////////////////////////
	// Ankunft
	//
	m_CE_ACht3.GetWindowText(m_ACht3);
	m_CE_AAlc3.GetWindowText(m_AAlc3);
	m_CE_AFltn.GetWindowText(m_AFltn);
	m_CE_AFlns.GetWindowText(m_AFlns);
	m_CE_AFlti.GetWindowText(m_AFlti);
	m_CE_ATtyp.GetWindowText(m_ATtyp);
//	m_CE_AHtyp.GetWindowText(m_AHtyp);
	m_CE_AStyp.GetWindowText(m_AStyp);
	m_CE_AStev.GetWindowText(m_AStev);
	m_CE_ASte2.GetWindowText(m_ASte2);
	m_CE_ASte3.GetWindowText(m_ASte3);
	m_CE_ASte4.GetWindowText(m_ASte4);
//	m_CE_ADes3.GetWindowText(m_ADes3);
	m_CE_AOrg4.GetWindowText(m_AOrg4);
	m_CE_AOrg3.GetWindowText(m_AOrg3);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AStod.GetWindowText(m_AStod);
	m_CE_AStoa.GetWindowText(m_AStoa);
	m_CE_ACsta.GetWindowText(m_ACsta);
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ARem1.GetWindowText(m_ARem1);
	m_CE_ARem2.GetWindowText(m_ARem2);
	
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_APabs.GetWindowText(m_APabs);
	m_CE_APaes.GetWindowText(m_APaes);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	m_CE_AGa1b.GetWindowText(m_AGa1b);
	m_CE_AGa2b.GetWindowText(m_AGa2b);
	m_CE_AGa1e.GetWindowText(m_AGa1e);
	m_CE_AGa2e.GetWindowText(m_AGa2e);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_AB1bs.GetWindowText(m_AB1bs);
	m_CE_AB2bs.GetWindowText(m_AB2bs);
	m_CE_AB1es.GetWindowText(m_AB1es);
	m_CE_AB2es.GetWindowText(m_AB2es);
	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ATet1.GetWindowText(m_ATet1);
	m_CE_ATet2.GetWindowText(m_ATet2);

	m_CE_ACsgn.GetWindowText(m_ACsgn);




	if(m_CE_ATtyp.IsChanged() && (strcmp((LPCSTR)m_ATtyp, prmAFlightSave->Ttyp) != 0))
		strcpy(prmAFlight->Sttt,"U");


	strcpy(prmAFlight->Alc3,omAAlc3);
	strcpy(prmAFlight->Alc2,omAAlc2);
	strcpy(prmAFlight->Fltn,m_AFltn);
	strcpy(prmAFlight->Flns,m_AFlns);
	strcpy(prmAFlight->Flti,m_AFlti);

	//*rkr
	strcpy(prmAFlight->Prfl,m_ACht3);

	strcpy(prmAFlight->Ttyp,m_ATtyp);
//	strcpy(prmAFlight->Htyp,m_AHtyp);
	strcpy(prmAFlight->Styp,m_AStyp);
	strcpy(prmAFlight->Stev,m_AStev);
	strcpy(prmAFlight->Ste2,m_ASte2);
	strcpy(prmAFlight->Ste3,m_ASte3);
	strcpy(prmAFlight->Ste4,m_ASte4);
	
	strcpy(prmAFlight->Org4,m_AOrg4);
	strcpy(prmAFlight->Des4,pcgHome4);
	if (!m_AOrg4.IsEmpty())
		bool bl = ogBCD.GetField("APT", "APC4", m_AOrg4, "APC3", m_AOrg3);
//	strcpy(prmAFlight->Des3,m_ADes3);
	strcpy(prmAFlight->Des3,pcgHome);
	strcpy(prmAFlight->Org3,m_AOrg3);

	strcpy(prmAFlight->Csgn,m_ACsgn);
	strcpy(prmAFlight->Regn, m_Regn);

	omARefDat = DateStringToDate(m_Pvom);

	if(imModus == DLG_CHANGE)
	{
		prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlight->Stoa);
//		prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlightSave->Stoa);
	}
	else
	{
		prmAFlight->Stoa = HourStringToDate( m_AStoa, omARefDat);
	}
	omARefDat = prmAFlight->Stoa;

	if(m_CE_AEtai.IsChanged())	
	{
		if(!m_AEtai.IsEmpty())
		{
			prmAFlight->Etau = HourStringToDate(m_AEtai, prmAFlight->Stoa);
			prmAFlightSave->Etau = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stea, "U") == 0)
			{
				prmAFlight->Etau = TIMENULL;	

			}
			if( strcmp(prmAFlight->Stea, "S") == 0)
			{
				prmAFlight->Etai = TIMENULL;	

			}
		}
	}
//	prmAFlight->Etau = HourStringToDate( m_AEtai, omARefDat);

	prmAFlight->Stod = HourStringToDate( m_AStod, omARefDat);


	if((imModus == DLG_CHANGE) && (prmAFlight->Stoa != TIMENULL))
	{
//		GetDayOfWeek(prmAFlight->Stoa, prmAFlight->Dooa);
	}
	
	strcpy(prmAFlight->Nose,m_Nose);
	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);
	strcpy(prmAFlight->Ext1,m_AExt1);
	strcpy(prmAFlight->Ext2,m_AExt2);

	strcpy(prmAFlight->Rem1,m_ARem1);
	strcpy(prmAFlight->Rem2,m_ARem2);
	if(m_ARem1.IsEmpty())
		strcpy(prmAFlight->Isre,"");
	else
		strcpy(prmAFlight->Isre,"+");

	strcpy(prmAFlight->Ming,m_Ming);
	strcpy(prmAFlight->Act3,m_Act3);
	strcpy(prmAFlight->Act5,m_Act5);

	strcpy(prmAFlight->Psta, m_APsta);
	strcpy(prmAFlight->Gta1, m_AGta1);
	strcpy(prmAFlight->Gta2, m_AGta2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	strcpy(prmAFlight->Blt1, m_ABlt1);
	strcpy(prmAFlight->Blt2, m_ABlt2);
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Ext1, m_AExt1);
	strcpy(prmAFlight->Ext2, m_AExt2);
	strcpy(prmAFlight->Tet1, m_ATet1);
	strcpy(prmAFlight->Tet2, m_ATet2);


	olTime = HourStringToDate( m_ACsta, omARefDat);
	prmAFlight->Csta = olTime;

	olTime = HourStringToDate( m_APabs, omARefDat);
	prmAFlight->Pabs = olTime;
	
	olTime = HourStringToDate( m_APaes, omARefDat);
	prmAFlight->Paes = olTime;

	olTime = HourStringToDate( m_AB1bs, omARefDat);
	prmAFlight->B1bs = olTime;
	
	olTime = HourStringToDate( m_AB2bs, omARefDat);
	prmAFlight->B2bs = olTime;

	olTime = HourStringToDate( m_AB1es, omARefDat);
	prmAFlight->B1es = olTime;

	olTime = HourStringToDate( m_AB2es, omARefDat);
	prmAFlight->B2es = olTime;

	olTime = HourStringToDate( m_AGa1b, omARefDat);
	prmAFlight->Ga1b = olTime;

	olTime = HourStringToDate( m_AGa2b, omARefDat);
	prmAFlight->Ga2b = olTime;

	olTime = HourStringToDate( m_AGa1e, omARefDat);
	prmAFlight->Ga1e = olTime;

	olTime = HourStringToDate( m_AGa2e, omARefDat);
	prmAFlight->Ga2e = olTime;
	

	CString olText;
	if(m_CB_ANfes.GetCheck())
	{
		m_CE_ANfes.GetWindowText(olText);
		olTime = HourStringToDate( olText, omARefDat);
		prmAFlight->Nfes = olTime;
	}
	else
	{
		prmAFlight->Nfes = TIMENULL;
	}


	CString olFlno= ogSeasonDlgFlights.CreateFlno(m_AAlc3, m_AFltn, m_AFlns);
	CString olFlnoT(olFlno);
	olFlnoT.TrimRight();
	if (strcmp(olFlno, prmAFlight->Flno) != 0 && strcmp(olFlnoT, prmAFlight->Flno) != 0)
		strcpy(prmAFlight->Flno, olFlno);
	

	strcpy(prmAFlight->Vers, "");

	if(m_CB_AStatus.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "S");
	}
	if(m_CB_AStatus2.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "O");
	}
	if(m_CB_AStatus3.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "");
		strcpy(prmAFlight->Vers, "PROGNOSE");
	}
	if(m_CB_ACxx.GetCheck() == TRUE)
		strcpy(prmAFlight->Ftyp, "X");

	if(m_CB_ANoop.GetCheck() == TRUE)
		strcpy(prmAFlight->Ftyp, "N");

	int ilCurSel;
	CString olTmp;
	

	if(bgCxxReason)
	{
		if((ilCurSel = m_CL_ACxxReason.GetCurSel()) == CB_ERR || strcmp(prmAFlight->Ftyp, "X") != 0)
		{
			strcpy(prmAFlight->Cxxr, "");
		}
		else
		{
			m_CL_ACxxReason.GetLBText(ilCurSel, olTmp);
			olTmp = olTmp.Left(5);
			olTmp.TrimRight();
			if(!olTmp.IsEmpty())
				strcpy(prmAFlight->Cxxr, olTmp);
			else
				strcpy(prmAFlight->Cxxr, "");
		}

	}





	if((ilCurSel = m_CB_ARemp.GetCurSel()) == CB_ERR)
		strcpy(prmAFlight->Remp, " ");
	else
	{
		m_CB_ARemp.GetLBText(ilCurSel, olTmp);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmAFlight->Remp, olTmp.Left(4));
		else
			strcpy(prmAFlight->Remp, "");
	}

	if (bgSeasonFixedRes) 
	{
		if(pomCBFPSA->GetCheck() == TRUE)
			strcpy(prmAFlight->Fpsa, "X");
		else
			strcpy(prmAFlight->Fpsa, "");

		if(pomCBFBL1->GetCheck() == TRUE)
			strcpy(prmAFlight->Fbl1, "X");
		else
			strcpy(prmAFlight->Fbl1, "");

		if(pomCBFBL2->GetCheck() == TRUE)
			strcpy(prmAFlight->Fbl2, "X");
		else
			strcpy(prmAFlight->Fbl2, "");

		if(pomCBFGA1->GetCheck() == TRUE)
			strcpy(prmAFlight->Fga1, "X");
		else
			strcpy(prmAFlight->Fga1, "");

		if(pomCBFGA2->GetCheck() == TRUE)
			strcpy(prmAFlight->Fga2, "X");
		else
			strcpy(prmAFlight->Fga2, "");
	}

	///////////////////////////////////////////////
	// weitere Flugnummern ankunft
	//
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	strcpy(prmAFlight->Jfno, "");
	int ilJcnt = 0;
	CString olJfno;

	int ilLines = pomAJfnoTable->GetLinesCount();
	for(int i = 0; i < ilLines; i++)
	{
		pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=   ogSeasonDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmAFlight->Jfno, olJfno);
	itoa(ilJcnt, prmAFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	//

	if (polRotationAViaDlg)
	{
		polRotationAViaDlg->GetViaList(prmAFlight->Vial);
		CString olAVial = prmAFlight->Vial;
		olAVial.TrimRight();
		strcpy(prmAFlight->Vial, olAVial);
	}
	
	///////////////////////////////////////////
	// Abflug
	//
	m_CE_DCstd.GetWindowText(m_DCstd);
	m_CE_DCht3.GetWindowText(m_DCht3);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DStod.GetWindowText(m_DStod);
	m_CE_DStoa.GetWindowText(m_DStoa);
	m_CE_DAlc3.GetWindowText(m_DAlc3);
	m_CE_DFltn.GetWindowText(m_DFltn);
	m_CE_DFlns.GetWindowText(m_DFlns);
	m_CE_DFlti.GetWindowText(m_DFlti);
	m_CE_DDes3.GetWindowText(m_DDes3);
	m_CE_DDes4.GetWindowText(m_DDes4);
//	m_CE_DOrg3.GetWindowText(m_DOrg3);
	m_CE_DTtyp.GetWindowText(m_DTtyp);
//	m_CE_DHtyp.GetWindowText(m_DHtyp);
	m_CE_DStyp.GetWindowText(m_DStyp);
	m_CE_DStev.GetWindowText(m_DStev);
	m_CE_DSte2.GetWindowText(m_DSte2);
	m_CE_DSte3.GetWindowText(m_DSte3);
	m_CE_DSte4.GetWindowText(m_DSte4);
	m_CE_DPstd.GetWindowText(m_DPstd);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DRem1.GetWindowText(m_DRem1);
	m_CE_DRem2.GetWindowText(m_DRem2);

	m_CE_DDays.GetWindowText(m_DDays);
	m_CE_DPdbs.GetWindowText(m_DPdbs);
	m_CE_DPdes.GetWindowText(m_DPdes);
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DGd1b.GetWindowText(m_DGd1b);
	m_CE_DGd2b.GetWindowText(m_DGd2b);
	m_CE_DGd1e.GetWindowText(m_DGd1e);
	m_CE_DGd2e.GetWindowText(m_DGd2e);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DWro2.GetWindowText(m_DWro2);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	m_CE_DTwr2.GetWindowText(m_DTwr2);
	m_CE_DW1bs.GetWindowText(m_DW1bs);
	m_CE_DW1es.GetWindowText(m_DW1es);
	m_CE_DW2bs.GetWindowText(m_DW2bs);
	m_CE_DW2es.GetWindowText(m_DW2es);

	m_CE_DCsgn.GetWindowText(m_DCsgn);
	m_DGd2d= m_CB_DGd2d.GetCheck(); 

	if (bgUseDepBelts) {
		m_CE_DBlt1.GetWindowText(m_DBlt1);
		m_CE_DTmb1.GetWindowText(m_DTmb1);
		m_CE_DB1bs.GetWindowText(m_DB1bs);
		m_CE_DB1es.GetWindowText(m_DB1es);
	prmDFlight->B1bs = HourStringToDate( m_DB1bs, prmDFlight->Stod);
	prmDFlight->B1es = HourStringToDate( m_DB1es, prmDFlight->Stod);
	}
	

	if(m_CE_DTtyp.IsChanged() && (strcmp((LPCSTR)m_DTtyp, prmDFlightSave->Ttyp) != 0))
		strcpy(prmDFlight->Sttt,"U");

	strcpy(prmDFlight->Nose,m_Nose);

	strcpy(prmDFlight->Alc3,omDAlc3);
	strcpy(prmDFlight->Alc2,omDAlc2);
	strcpy(prmDFlight->Fltn,m_DFltn);
	strcpy(prmDFlight->Flns,m_DFlns);
	strcpy(prmDFlight->Flti,m_DFlti);
	strcpy(prmDFlight->Prfl,m_DCht3);

	strcpy(prmDFlight->Ttyp,m_DTtyp);
//	strcpy(prmDFlight->Htyp,m_DHtyp);
	strcpy(prmDFlight->Styp,m_DStyp);
	strcpy(prmDFlight->Stev,m_DStev);
	strcpy(prmDFlight->Ste2,m_DSte2);
	strcpy(prmDFlight->Ste3,m_DSte3);
	strcpy(prmDFlight->Ste4,m_DSte4);
	
	strcpy(prmDFlight->Des4,m_DDes4);
	if (!m_DDes4.IsEmpty())
		bool bl = ogBCD.GetField("APT", "APC4", m_DDes4, "APC3", m_DDes3);
	strcpy(prmDFlight->Des3,m_DDes3);
//	strcpy(prmDFlight->Org3,m_DOrg3);
	strcpy(prmDFlight->Org3,pcgHome);
	strcpy(prmDFlight->Org4,pcgHome4);

 	strcpy(prmDFlight->Csgn,m_DCsgn);
	strcpy(prmDFlight->Regn, m_Regn);

	omDRefDat = DateStringToDate(m_Pvom);
  
  
	m_CE_DBaz1.GetWindowText(m_DBaz1);
	m_CE_DBaz4.GetWindowText(m_DBaz4);

	strcpy(prmDFlight->Baz1, m_DBaz1);
	strcpy(prmDFlight->Baz4, m_DBaz4);






	if(imModus == DLG_CHANGE)
	{
		prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlight->Stod);
//		prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlightSave->Stod);
	}
	else
	{
		prmDFlight->Stod = HourStringToDate( m_DStod, omDRefDat);
	}
	omDRefDat = prmDFlight->Stod;



	if(m_CB_DNfes.GetCheck())
	{
		m_CE_DNfes.GetWindowText(olText);
		olTime = HourStringToDate( olText, omDRefDat);
		prmDFlight->Nfes = olTime;
	}
	else
	{
		prmDFlight->Nfes = TIMENULL;
	}





	if((imModus == DLG_CHANGE) && (prmDFlight->Stod != TIMENULL))
	{
//		GetDayOfWeek(prmDFlight->Stod, prmDFlight->Dood);
	}

	if(m_CE_DEtdi.IsChanged())	
	{
		if(!m_DEtdi.IsEmpty())
		{
			prmDFlight->Etdu = HourStringToDate(m_DEtdi, prmDFlight->Stod);
			prmDFlightSave->Etdu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if (strcmp(pcgHome, "FCO") == 0)
			{
				prmDFlight->Etdu = TIMENULL;	
				if (prmDFlightSave->Etdu == TIMENULL)
					prmDFlightSave->Etdu = GetCurrentTime(); 
			}
			else
			{
				if( strcmp(prmDFlight->Sted, "U") == 0)
				{
					prmDFlight->Etdu = TIMENULL;	

				}
				if( strcmp(prmDFlight->Sted, "S") == 0)
				{
					prmDFlight->Etdi = TIMENULL;	
				}
			}
		}
	}
//	prmDFlight->Etdu = HourStringToDate( m_DEtdi, omDRefDat);

	prmDFlight->Stoa = HourStringToDate( m_DStoa, omDRefDat);

	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Wro2, m_DWro2);
	strcpy(prmDFlight->Twr1, m_DTwr1);
//	strcpy(prmDFlight->Twr2, m_DTwr2);

	
	olTime = HourStringToDate( m_DCstd, omDRefDat);
	prmDFlight->Cstd = olTime;

	olTime = HourStringToDate( m_DPdbs, omDRefDat);
	prmDFlight->Pdbs = olTime;

	olTime = HourStringToDate( m_DPdes, omDRefDat);
	prmDFlight->Pdes = olTime;

	olTime = HourStringToDate( m_DGd1b, omDRefDat);
	prmDFlight->Gd1b = olTime;

	olTime = HourStringToDate( m_DGd2b, omDRefDat);
	prmDFlight->Gd2b = olTime;

	olTime = HourStringToDate( m_DGd1e, omDRefDat);
	prmDFlight->Gd1e = olTime;

	olTime = HourStringToDate( m_DGd2e, omDRefDat);
	prmDFlight->Gd2e = olTime;

	olTime = HourStringToDate( m_DW1bs, omDRefDat);
	prmDFlight->W1bs = olTime;

	olTime = HourStringToDate( m_DW1es, omDRefDat);
	prmDFlight->W1es = olTime;

	olTime = HourStringToDate( m_DW2bs, omDRefDat);
	prmDFlight->W2bs = olTime;

	olTime = HourStringToDate( m_DW2es, omDRefDat);
	prmDFlight->W2es = olTime;


	strcpy(prmDFlight->Rem1,m_DRem1);
	strcpy(prmDFlight->Rem2,m_DRem2);
	if(m_DRem1.IsEmpty())
		strcpy(prmDFlight->Isre,"");
	else
		strcpy(prmDFlight->Isre,"+");

	strcpy(prmDFlight->Ming,m_Ming);
	strcpy(prmDFlight->Act3,m_Act3);
	strcpy(prmDFlight->Act5,m_Act5);

		
	if (bgUseDepBelts) {
		strcpy(prmDFlight->Blt1,m_DBlt1);
		strcpy(prmDFlight->Tmb1, m_DTmb1);
		prmDFlight->B1bs = HourStringToDate( m_DB1bs, prmDFlight->Stod);
		prmDFlight->B1es = HourStringToDate( m_DB1es, prmDFlight->Stod);
	}


	olFlno = ogSeasonDlgFlights.CreateFlno(m_DAlc3, m_DFltn, m_DFlns); 
	olFlnoT = olFlno;
	olFlnoT.TrimRight();
	if (strcmp(olFlno, prmDFlight->Flno) != 0 && strcmp(olFlnoT, prmDFlight->Flno) != 0)
		strcpy(prmDFlight->Flno, olFlno);


	strcpy(prmDFlight->Vers, "");
	if(m_CB_DStatus.GetCheck() == TRUE)
	{
		strcpy(prmDFlight->Ftyp, "S");
	}
	if(m_CB_DStatus2.GetCheck() == TRUE)
    {
		strcpy(prmDFlight->Ftyp, "O");
	}
	if(m_CB_DStatus3.GetCheck() == TRUE)
	{
		strcpy(prmDFlight->Ftyp, "");
		strcpy(prmDFlight->Vers, "PROGNOSE");
	}
	if(m_CB_DCxx.GetCheck() == TRUE)
		strcpy(prmDFlight->Ftyp, "X");


	if(bgCxxReason)
	{
		if((ilCurSel = m_CL_DCxxReason.GetCurSel()) == CB_ERR || strcmp(prmDFlight->Ftyp, "X") != 0)
		{
			strcpy(prmDFlight->Cxxr, "");
		}
		else
		{
			m_CL_DCxxReason.GetLBText(ilCurSel, olTmp);
			olTmp = olTmp.Left(5);
			olTmp.TrimRight();
			if(!olTmp.IsEmpty())
				strcpy(prmDFlight->Cxxr, olTmp);
			else
				strcpy(prmDFlight->Cxxr, "");
		}

	}



	if(m_CB_DNoop.GetCheck() == TRUE)
		strcpy(prmDFlight->Ftyp, "N");


	if(m_DGd2d == TRUE)
		strcpy(prmDFlight->Gd2d, "X");
	else
		strcpy(prmDFlight->Gd2d, "");


	if((ilCurSel = m_CB_DRemp.GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Remp, " ");
	else
	{
		m_CB_DRemp.GetLBText(ilCurSel, olTmp);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Remp, olTmp.Left(4));
		else
			strcpy(prmDFlight->Remp, "");
	}


	if (bgSeasonFixedRes) 
	{
		if(pomCBFPSD->GetCheck() == TRUE)
			strcpy(prmDFlight->Fpsd, "X");
		else
			strcpy(prmDFlight->Fpsd, "");

		if(pomCBFGD1->GetCheck() == TRUE)
			strcpy(prmDFlight->Fgd1, "X");
		else
			strcpy(prmDFlight->Fgd1, "");

		if(pomCBFGD2->GetCheck() == TRUE)
			strcpy(prmDFlight->Fgd2, "X");
		else
			strcpy(prmDFlight->Fgd2, "");

	
	}
	///////////////////////////////////////////////
	// weitere Flugnummern abflug
	//

	strcpy(prmDFlight->Jfno, "");
	ilJcnt = 0;
	olJfno = "";

	ilLines = pomDJfnoTable->GetLinesCount();
	for( i = 0; i < ilLines; i++)
	{
		pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=  ogSeasonDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmDFlight->Jfno, olJfno);
	itoa(ilJcnt, prmDFlight->Jcnt, 10);


	////////////////////////////////////
	// Via Abflug

	if (polRotationDViaDlg)
	{
		polRotationDViaDlg->GetViaList(prmDFlight->Vial);
		CString olDVial = prmDFlight->Vial;
		olDVial.TrimRight();
		strcpy(prmDFlight->Vial, olDVial);
	}
		


	////////////////////////////////////
	// CheckinSchalter

	CString olMax = "   ";
	CString olMin = "zzz";

	CString olTmpStr;
	CString olCkic;
	CString olCkit;
	CTime olCkbs;
	CTime olCkes;
	CTime olDRef; 
	CCADATA *prlCca;
	CString olDisp;
	if(pomDCinsTable != NULL)
	{

		if(imModus == DLG_CHANGE)
		{
			olDRef = prmDFlightSave->Stod;

			CString olTest = prmDFlightSave->Stod.Format("%d - %H:%M");

			if(bmLocalTime) ogBasicData.UtcToLocal(olDRef);

		}
		else
		{
			olDRef = omDRefDat;
		}


		ilLines = pomDCinsTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
				pomDCinsTable->GetTextFieldValue(i, 2, olCkit);
			
				pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
				olCkbs = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkbs);

				pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
				olCkes = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkes);

				if(bgShowClassInBatchDlg)
				{
					pomDCinsTable->GetTextFieldValue(i, 5, olDisp);
				}

			}
			else
			{
				pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
				pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
			
				pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
				olCkbs = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkbs);

				pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
				olCkes = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkes);

				if(bgShowClassInBatchDlg)
				{
					pomDCinsTable->GetTextFieldValue(i, 4, olDisp);
				}
			}

			
			if(i >= omDCins.GetSize())
			{
				prlCca = new CCADATA;
				omDCins.Add(prlCca);

				// set cca stod
				prlCca->Stod = olDRef;
				if(bmLocalTime) ogBasicData.LocalToUtc(prlCca->Stod);

				// set cca flno
				olTmpStr = prmDFlight->Flno;
				olTmpStr.TrimRight();
				strcpy(prlCca->Flno, olTmpStr);

				// set cca act3
				strcpy(prlCca->Act3, prmDFlight->Act3);

			}
			else
			{
				prlCca = &omDCins[i];
			}
			strcpy(prlCca->Ckic, olCkic);
			strcpy(prlCca->Ckit, olCkit);


			prlCca->Ckbs = olCkbs;
			prlCca->Ckes = olCkes;
			strcpy(prlCca->Disp, olDisp);


			//if(bmLocalTime) omCcaData.StructLocalToUtc(prlCca);
			//olData.StructLocalToUtc(prlCca);

			olCkic.TrimRight();

			if((olCkic < olMin) && !olCkic.IsEmpty())
				olMin = olCkic;

			if((olCkic > olMax) && !olCkic.IsEmpty())
				olMax = olCkic;
		}

		olMax.TrimRight();
		olMin.TrimRight();

		
		// in Athens the flight set CKIF and CKIT automatically !!
		if (strcmp(pcgHome, "ATH") != 0 && strcmp(pcgHome, "LIS") != 0)
		{
			if(olMax != "   " && !olMax.IsEmpty())
				strcpy(prmDFlight->Ckit, olMax);
			else
				strcpy(prmDFlight->Ckit, "");
		
			if(olMin != "zzz" && !olMin.IsEmpty())
				strcpy(prmDFlight->Ckif, olMin);
			else
				strcpy(prmDFlight->Ckif, "");
		}	
	
		/*
		omToSaveCca.RemoveAll();
		for(int i = 0; i < omDCins.GetSize(); i++)
		{
			omDCins[i].Flnu = prmDFlight->Urno;
			omDCins[i].Stod = prmDFlight->Stod;
			strcpy(omDCins[i].Flno, prmDFlight->Flno);
			if(omDCins[i].Urno == 0)
			{
				if(strcmp(omDCins[i].Ckic, "") != 0 || strcmp(omDCins[i].Ckit, "") != 0 ||
					omDCins[i].Ckbs != TIMENULL || omDCins[i].Ckba != TIMENULL || 
					omDCins[i].Ckes != TIMENULL || omDCins[i].Ckea != TIMENULL)
				{
					omDCins[i].Cdat = CTime::GetCurrentTime();
					strcpy(omDCins[i].Usec, pcgUser);
					omDCins[i].Urno = ogBasicData.GetNextUrno("CCA");	
					omDCins[i].IsChanged = DATA_NEW;
					omToSaveCca.Add(&omDCins[i]);
				}
			}
			else
			{
				for(int j = 0; j < omOldCcaValues.GetSize(); j++)
				{
					CCADATA rlN, rlO;
					rlN = omDCins[i];
					rlO = omOldCcaValues[j];
					CString olN = rlN.Ckbs.Format("%H:%M");
					CString olO = rlO.Ckbs.Format("%H:%M");
					if(omDCins[i].Urno == omOldCcaValues[j].Urno)
					{
						bool blIsChanged = false;
						if(strcmp(omDCins[i].Ckic ,omOldCcaValues[j].Ckic) != 0)
						{
							blIsChanged = true;
						}
						if(strcmp(omDCins[i].Ckit ,omOldCcaValues[j].Ckit) != 0)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckbs != omOldCcaValues[j].Ckbs)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckba != omOldCcaValues[j].Ckba)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckes != omOldCcaValues[j].Ckes)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckea != omOldCcaValues[j].Ckea)
						{
							blIsChanged = true;
						}
						//So dann hat sich was ge�ndert ??
						if(blIsChanged == true)
						{
							strcpy(omDCins[i].Useu, pcgUser);
							omDCins[i].Lstu = olTime;
							omDCins[i].IsChanged = DATA_CHANGED;
							omDCins[i].Stod = prmDFlight->Stod;
							strcpy(omDCins[i].Flno, prmDFlight->Flno);
							omToSaveCca.Add(&omDCins[i]);
							j = omOldCcaValues.GetSize();
						}
					}
				}
			}
		}
		*/
	}

	/////////////////////////////////////////////////////////////////////////
	//// Get carousel times
	CString olBaz1;
	CString olBaz4;
	CTime opStart = TIMENULL;
	CTime opEnd	  = TIMENULL;
	CTime Start = TIMENULL;
	CTime End	  = TIMENULL;

	for( i = 0; i < omDCins.GetSize(); i++)
	{
		prlCca = &omDCins[i];

		if( !CString(prlCca->Ckic).IsEmpty())
		{
			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					//if(!olBaz1.IsEmpty())
					//	break;
				}
			}

			if(prlCca->Ckba != TIMENULL)
			{
				Start = prlCca->Ckba;
			}
			else
			{
				if(prlCca->Ckbs != TIMENULL)
				{
					Start = prlCca->Ckbs;
				}
			}

			if(prlCca->Ckea != TIMENULL)
			{
				End = prlCca->Ckea;
			}
			else
			{
				if(prlCca->Ckes != TIMENULL)
				{
					End = prlCca->Ckes;
				}
			}

			if (Start != TIMENULL && opStart == TIMENULL)
				opStart = Start;

			if (Start < opStart)
				opStart = Start;

			if (End > opEnd)
				opEnd = End;

		}
	}
	//// END (get carousel times)
	////////////////////////////////////////////////////////////////////////

/*
	if((!m_CE_DBaz1.IsChanged() && bmAutoSetBaz1) || CString(prmDFlight->Baz1).IsEmpty() || olBaz1.IsEmpty())
		strcpy(prmDFlight->Baz1, olBaz1);
	if((!m_CE_DBaz4.IsChanged() && bmAutoSetBaz4) || CString(prmDFlight->Baz4).IsEmpty() || olBaz4.IsEmpty())
		strcpy(prmDFlight->Baz4, olBaz4);

	if (m_CE_DBaz1.IsChanged())
		m_CE_DBaz1.GetWindowText(olBaz1);

	if(!CString(prmDFlight->Baz1).IsEmpty())
	{
		prmDFlight->Bao1 = opStart;
		prmDFlight->Bac1 = opEnd;
	}
	else
	{
		prmDFlight->Bao1 = TIMENULL;
		prmDFlight->Bac1 = TIMENULL;
	}

	if(!CString(prmDFlight->Baz4).IsEmpty())
	{
		prmDFlight->Bao4 = opStart;
		prmDFlight->Bac4 = opEnd;
	}
	else
	{
		prmDFlight->Bao4 = TIMENULL;
		prmDFlight->Bac4 = TIMENULL;
	}
*/

	if(bmAutoSetBaz1 && (CString(prmDFlight->Baz1).IsEmpty() || olBaz1.IsEmpty() ||	( !m_CE_DBaz1.IsChanged() )))
	{
		if (m_CE_DBaz1.IsChanged())
			m_CE_DBaz1.GetWindowText(olBaz1);

		strcpy(prmDFlight->Baz1, olBaz1);
		if (!olBaz1.IsEmpty())
		{
			prmDFlight->Bao1 = opStart;
			prmDFlight->Bac1 = opEnd;
		}
		else
		{
			prmDFlight->Bao1 = TIMENULL;
			prmDFlight->Bac1 = TIMENULL;
		}
	}

	if(bmAutoSetBaz4 && (CString(prmDFlight->Baz4).IsEmpty() || olBaz4.IsEmpty() ||(  !m_CE_DBaz4.IsChanged() )))
	{
		if (m_CE_DBaz4.IsChanged())
			m_CE_DBaz4.GetWindowText(olBaz4);

		strcpy(prmDFlight->Baz4, olBaz4);
		if (!olBaz4.IsEmpty())
		{
			prmDFlight->Bao4 = opStart;
			prmDFlight->Bac4 = opEnd;
		}
		else
		{
			prmDFlight->Bao4 = TIMENULL;
			prmDFlight->Bac4 = TIMENULL;
		}
	}


	if(imModus != DLG_CHANGE)
	{

		if(m_Pvom == m_Pbis)
		{
			CTime olTime = DateStringToDate(m_Pbis);
			if( olTime != TIMENULL)
			{
				if(	(m_ADays.IsEmpty()) && (prmAFlight->Stoa != TIMENULL))
				{
					GetDayOfWeek(olTime, pclDays);
					m_ADays = CString(pclDays);
					m_CE_ADays.SetWindowText(m_ADays);
				}

				if(	(m_DDays.IsEmpty()) && (prmDFlight->Stod != TIMENULL))
				{
					GetDayOfWeek(olTime, pclDays);
					m_DDays = CString(pclDays);
					m_CE_DDays.SetWindowText(m_DDays);
				}
			}
		}
	}






//	CCS_CATCH_ALL

}



// Only fill flight data with Pos/Gate/Belt/Wro allocation values!
void CSeasonDlg::FillGatPosFlightData() 
{

	CCS_TRY

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	CTime olTime;


	///////////////////////////////////////////
	// Ankunft
	//
	m_CE_ACsta.GetWindowText(m_ACsta);
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	
	m_CE_APabs.GetWindowText(m_APabs);
	m_CE_APaes.GetWindowText(m_APaes);
	m_CE_AGa1b.GetWindowText(m_AGa1b);
	m_CE_AGa2b.GetWindowText(m_AGa2b);
	m_CE_AGa1e.GetWindowText(m_AGa1e);
	m_CE_AGa2e.GetWindowText(m_AGa2e);
	m_CE_AB1bs.GetWindowText(m_AB1bs);
	m_CE_AB2bs.GetWindowText(m_AB2bs);
	m_CE_AB1es.GetWindowText(m_AB1es);
	m_CE_AB2es.GetWindowText(m_AB2es);

	/* // No Terminals, because DIAFLIGHTDATA - Structure has no terminal fields!
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	*/

	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);

	/*
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	*/

	omARefDat = prmAFlight->Stoa;

	olTime = HourStringToDate( m_APabs, omARefDat);
	prmAFlight->Pabs = olTime;

	olTime = HourStringToDate( m_ACsta, omARefDat);
	prmAFlight->Csta = olTime;
	
	olTime = HourStringToDate( m_APaes, omARefDat);
	prmAFlight->Paes = olTime;

	olTime = HourStringToDate( m_AB1bs, omARefDat);
	prmAFlight->B1bs = olTime;
	
	olTime = HourStringToDate( m_AB2bs, omARefDat);
	prmAFlight->B2bs = olTime;

	olTime = HourStringToDate( m_AB1es, omARefDat);
	prmAFlight->B1es = olTime;

	olTime = HourStringToDate( m_AB2es, omARefDat);
	prmAFlight->B2es = olTime;

	olTime = HourStringToDate( m_AGa1b, omARefDat);
	prmAFlight->Ga1b = olTime;

	olTime = HourStringToDate( m_AGa2b, omARefDat);
	prmAFlight->Ga2b = olTime;

	olTime = HourStringToDate( m_AGa1e, omARefDat);
	prmAFlight->Ga1e = olTime;

	olTime = HourStringToDate( m_AGa2e, omARefDat);
	prmAFlight->Ga2e = olTime;
	

	///////////////////////////////////////////
	// Abflug
	//
	m_CE_DCstd.GetWindowText(m_DCstd);
	m_CE_DPstd.GetWindowText(m_DPstd);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DWro2.GetWindowText(m_DWro2);

	m_CE_DPdbs.GetWindowText(m_DPdbs);
	m_CE_DPdes.GetWindowText(m_DPdes);
	m_CE_DGd1b.GetWindowText(m_DGd1b);
	m_CE_DGd2b.GetWindowText(m_DGd2b);
	m_CE_DGd1e.GetWindowText(m_DGd1e);
	m_CE_DGd2e.GetWindowText(m_DGd2e);
	m_CE_DW1bs.GetWindowText(m_DW1bs);
	m_CE_DW1es.GetWindowText(m_DW1es);
	m_CE_DW2bs.GetWindowText(m_DW2bs);
	m_CE_DW2es.GetWindowText(m_DW2es);

	/* 
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	*/

	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Wro2, m_DWro2);

	/*
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Twr1, m_DTwr1);
	*/

	omDRefDat = prmDFlight->Stod;

	olTime = HourStringToDate( m_DCstd, omDRefDat);
	prmDFlight->Cstd = olTime;

	olTime = HourStringToDate( m_DPdbs, omDRefDat);
	prmDFlight->Pdbs = olTime;

	olTime = HourStringToDate( m_DPdes, omDRefDat);
	prmDFlight->Pdes = olTime;

	olTime = HourStringToDate( m_DGd1b, omDRefDat);
	prmDFlight->Gd1b = olTime;

	olTime = HourStringToDate( m_DGd2b, omDRefDat);
	prmDFlight->Gd2b = olTime;

	olTime = HourStringToDate( m_DGd1e, omDRefDat);
	prmDFlight->Gd1e = olTime;

	olTime = HourStringToDate( m_DGd2e, omDRefDat);
	prmDFlight->Gd2e = olTime;

	olTime = HourStringToDate( m_DW1bs, omDRefDat);
	prmDFlight->W1bs = olTime;

	olTime = HourStringToDate( m_DW1es, omDRefDat);
	prmDFlight->W1es = olTime;

	olTime = HourStringToDate( m_DW2bs, omDRefDat);
	prmDFlight->W2bs = olTime;

	olTime = HourStringToDate( m_DW2es, omDRefDat);
	prmDFlight->W2es = olTime;



	CCS_CATCH_ALL
}



void CSeasonDlg::ProcessFlightChange(SEASONDLGFLIGHTDATA *prpFlight)
{

	CCS_TRY

	if(prpFlight == NULL)
		return;

	if(imModus != DLG_CHANGE && imModus != DLG_CHANGE_DIADATA)
		return;

	SEASONDLGFLIGHTDATA *prlAFlight;
	SEASONDLGFLIGHTDATA *prlDFlight;
	long llAUrno = 0;
	long llDUrno = 0;

	if(strcmp(prpFlight->Des3, pcgHome) == 0)
	{
		prlAFlight = prpFlight;
		prlDFlight = ogSeasonDlgFlights.GetDeparture(prpFlight);

		llAUrno = prlAFlight->Urno;
		if(prlDFlight != NULL)
			llDUrno = prlDFlight->Urno;
	
		if((llAUrno == prmAFlight->Urno) || ((llDUrno == prmDFlight->Urno) && (llDUrno != 0)))
		{
			*prmAFlight		= *prlAFlight;
			*prmAFlightSave = *prlAFlight;

			if(llDUrno != 0)
			{
				*prmDFlight		= *prlDFlight;
				*prmDFlightSave = *prlDFlight;
			}
			else
			{
				prlDFlight = new SEASONDLGFLIGHTDATA;
				*prmDFlight		= *prlDFlight;
				*prmDFlightSave = *prlDFlight;
				delete prlDFlight;
			}

			InitDialog(true, true);

			ReadCcaData();
			DShowCinsTable();
		}

	}
	else
	{
		prlDFlight = prpFlight;
		prlAFlight = ogSeasonDlgFlights.GetArrival(prpFlight);

		llDUrno = prlDFlight->Urno;
		if(prlAFlight != NULL)
			llAUrno = prlAFlight->Urno;
	
		if((llDUrno == prmDFlight->Urno) || ((llAUrno == prmAFlight->Urno) && (llAUrno != 0)))
		{
			*prmDFlight		= *prlDFlight;
			*prmDFlightSave = *prlDFlight;

			if(llAUrno != 0)
			{
				*prmAFlight		= *prlAFlight;
				*prmAFlightSave = *prlAFlight;
			}
			else
			{
				prlAFlight = new SEASONDLGFLIGHTDATA;
				*prmAFlight		= *prlAFlight;
				*prmAFlightSave = *prlAFlight;
				delete prlAFlight;
			}

			InitDialog(true, true);

			ReadCcaData();
			DShowCinsTable();
		}

	}

	char clPaid= ' ';
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		
	}

//	clPaid = GetPaid();

	UpdateCashButton(olAlc3, clPaid);

	CCS_CATCH_ALL

}




void CSeasonDlg::ProcessFlightDelete(SEASONDLGFLIGHTDATA *prpFlight)
{

	CCS_TRY

	if(prpFlight == NULL)
		return;

	if(imModus != DLG_CHANGE)
		return;

	
	
	SEASONDLGFLIGHTDATA *prlFlight;
	if(prpFlight->Urno == prmAFlight->Urno)
	{
		prlFlight = new SEASONDLGFLIGHTDATA;
		*prmAFlight		= *prlFlight;
		*prmAFlightSave = *prlFlight;
		delete prlFlight;
		InitDialog(true, false);
	}

	if(prpFlight->Urno == prmDFlight->Urno)
	{
		prlFlight = new SEASONDLGFLIGHTDATA;
		*prmDFlight		= *prlFlight;
		*prmDFlightSave = *prlFlight;
		delete prlFlight;
		InitDialog(false, true);
	}

	CCS_CATCH_ALL

}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

	CCS_TRY

		CSeasonDlg *polDlg = (CSeasonDlg *)popInstance;

		if (ipDDXType == S_DLG_FLIGHT_CHANGE)
			polDlg->ProcessFlightChange((SEASONDLGFLIGHTDATA *)vpDataPointer);
		if (ipDDXType == S_DLG_FLIGHT_DELETE)
			polDlg->ProcessFlightDelete((SEASONDLGFLIGHTDATA *)vpDataPointer);
		if(ipDDXType == S_DLG_ROTATION_CHANGE)
			polDlg->ProcessRotationChange();
		if(ipDDXType == S_DLG_ROTATION_SPLIT)
			polDlg->ProcessRotationChange((SEASONDLGFLIGHTDATA *)vpDataPointer);


		if (ipDDXType == CCA_SEADLG_CHANGE || ipDDXType == CCA_SEADLG_NEW || ipDDXType == CCA_SEADLG_DELETE)
			polDlg->ProcessCCA((CCADATA *)vpDataPointer, ipDDXType);

		if (ipDDXType == S_DLG_RELOAD_CCA)
			polDlg->ReloadCCA();


		if (ipDDXType == FPE_INSERT || ipDDXType == FPE_UPDATE || ipDDXType == FPE_DELETE)
			polDlg->ProcessFpeChange();
		

	CCS_CATCH_ALL

}




void CSeasonDlg::ProcessFpeChange()
{
	SetArrPermitsButton();
	SetDepPermitsButton();
}



///////////////////////////////////////////////////////////////////////////
// n�chster Flug von der Flugdatenansichtentabelle laden 
//
void CSeasonDlg::OnNextflight()
{
	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	
	CCS_TRY

	NEW_SEASONDLG_DATA rlData;
	rlData.Parent = pomParent;
	rlData.CurrUrno = lmCalledBy;
	ogDdx.DataChanged((void *)this, S_DLG_GET_NEXT,(void *)&rlData );
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CCS_CATCH_ALL

}


///////////////////////////////////////////////////////////////////////////
// vorheriger Flug von der Flugdatenansichtentabelle laden 
//
void CSeasonDlg::OnPrevflight()
{
	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	CCS_TRY

	NEW_SEASONDLG_DATA rlData;

	rlData.Parent = pomParent;
	rlData.CurrUrno = lmCalledBy;

	ogDdx.DataChanged((void *)this, S_DLG_GET_PREV,(void *)&rlData );
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAgent() 
{
	if(prmAFlight->Urno > 0 || prmDFlight->Urno > 0)
	{
		if(bgEnhancedHandlingAgentAssignment )
		{

			ROTATIONDLGFLIGHTDATA rlRotDataArr(*prmAFlight);
			ROTATIONDLGFLIGHTDATA rlRotDataDep(*prmDFlight);

			RotationHTADlg olRotationHTADlg(this, &rlRotDataArr, &rlRotDataDep);
			olRotationHTADlg.DoModal();

		}
		else
		{
			//alc3 der Fluggesellschaften
			CString opAlc3StrArr ("");
			CString opAlc3StrDep ("");

			if(prmAFlight != NULL)
				opAlc3StrArr.Format("%s", prmAFlight->Alc3);
			if(prmDFlight != NULL)
				opAlc3StrDep.Format("%s", prmDFlight->Alc3);

			if (opAlc3StrArr.GetLength() == 0 && opAlc3StrDep.GetLength() == 0)
				return;

			RotationHAIDlg polRotationHAIDlg(this, opAlc3StrArr, opAlc3StrDep);
			if (polRotationHAIDlg.DoModal() == IDOK)
			{
			}
		}
	}
}


LRESULT CSeasonDlg::OnViaChanged(WPARAM wParam,LPARAM lParam) 
{
	ldiv_t div_result;

	if (polRotationAViaDlg && prmAFlight != NULL)
	{
		polRotationAViaDlg->GetViaList(prmAFlight->Vial);
		CString olAVial = prmAFlight->Vial;
		div_result = ldiv( olAVial.GetLength(), 120);
		itoa(div_result.quot, prmAFlight->Vian ,3);
		olAVial.TrimRight();
		strcpy(prmAFlight->Vial, olAVial);
	}

	if (polRotationDViaDlg && prmDFlight != NULL)
	{
		polRotationDViaDlg->GetViaList(prmDFlight->Vial);
		CString olDVial = prmDFlight->Vial;
		div_result = ldiv( olDVial.GetLength(), 120);
		itoa(div_result.quot, prmDFlight->Vian ,3);
		olDVial.TrimRight();
		strcpy(prmDFlight->Vial, olDVial);
	}


	CheckViaButton();

	return 0L;
}


void CSeasonDlg::OnAVia() 
{
//	if(prmAFlight->Urno > 0 || bmAnkunft)
	if(prmAFlight)
	{
		if (!polRotationAViaDlg)
		{
			CString olCaption = CString(prmAFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmAFlight->Csgn);

			bool blEnable = true;
			if(imModus == DLG_CHANGE_DIADATA || ogPrivList.GetStat("SEASONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", olCaption, prmAFlight->Urno, prmAFlight->Vial, omARefDat, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AShowVia"));
		}

		if (imModus == DLG_COPY || imModus == DLG_NEW)
			polRotationAViaDlg->SetRefDate(DateStringToDate(m_Pvom));

		CPoint point;
		::GetCursorPos(&point);
		polRotationAViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}

void CSeasonDlg::OnDVia() 
{
//	if(prmDFlight->Urno > 0 || bmAbflug)
	if(prmDFlight)
	{
		if (!polRotationDViaDlg)
		{
			CString olCaption = CString(prmDFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmDFlight->Csgn);

			bool blEnable = true;
			if(imModus == DLG_CHANGE_DIADATA || ogPrivList.GetStat("SEASONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, omDRefDat, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DShowVia"));
		}

		if (imModus == DLG_COPY || imModus == DLG_NEW)
			polRotationDViaDlg->SetRefDate(DateStringToDate(m_Pvom));

		CPoint point;
		::GetCursorPos(&point);
		polRotationDViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}


///////////////////////////////////////////////////////////////////////////
// Cancel
//
void CSeasonDlg::OnCancel() 
{

	CCS_TRY

	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	bmChanged = false;
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	ogSeasonDlgFlights.ClearAll();
	ClearAll();

	if (polRotationAViaDlg)
		polRotationAViaDlg->ShowWindow(SW_HIDE);
	if (polRotationDViaDlg)
		polRotationDViaDlg->ShowWindow(SW_HIDE);

	ShowWindow(SW_HIDE);

	CCS_CATCH_ALL
}

///////////////////////////////////////////////////////////////////////////
void CSeasonDlg::ResetDatesForFlight() 
{
	if (bmAnkunft && prmAFlight && prmAFlight->Urno != 0)
	{
		CTime olRef = prmAFlightSave->Stoa;
		if (bmLocalTime)
			ogBasicData.UtcToLocal(olRef);

		prmAFlight->Stoa = olRef;
	}
	if (bmAbflug && prmDFlight && prmDFlight->Urno != 0)
	{
		CTime olRef = prmDFlightSave->Stod;
		if (bmLocalTime)
			ogBasicData.UtcToLocal(olRef);

		prmDFlight->Stod = olRef;
	}
}

void CSeasonDlg::ArrivalSchedTifd(CTime& opTime) 
{
	CString olTmpD;
	opTime = TIMENULL;
	if(!m_AStod.IsEmpty())
	{
		CTime olSaveStoa = prmAFlightSave->Stoa;
		if (bgSeasonLocal)
			ogBasicData.UtcToLocal(olSaveStoa);

		m_CE_AStod.GetWindowText(olTmpD);
		opTime = HourStringToDate(olTmpD, olSaveStoa);  
	}

/*
	opTime = TIMENULL;

	if(!m_AStod.IsEmpty())
	{
		opTime = HourStringToDate(m_AStod, prmAFlight->Stoa);
	}
*/
}

void CSeasonDlg::ArrivalSchedTifa(CTime& opTime) 
{
	CString olTmpA;
	opTime = TIMENULL;

	if(!m_AEtai.IsEmpty())
	{
		CTime olSaveStoa = prmAFlightSave->Tifa;
		if (bgSeasonLocal)
			ogBasicData.UtcToLocal(olSaveStoa);

		m_CE_AEtai.GetWindowText(olTmpA);
		opTime = HourStringToDate(olTmpA, olSaveStoa);
	}
	else
	{
		if(!m_AStoa.IsEmpty())
		{
			CTime olSaveStoa = prmAFlightSave->Stoa;
			if (bgSeasonLocal)
				ogBasicData.UtcToLocal(olSaveStoa);

			m_CE_AStoa.GetWindowText(olTmpA);
			opTime = HourStringToDate(olTmpA, olSaveStoa);
		}
	}

/*
	opTime = TIMENULL;

	if(!m_AEtai.IsEmpty())
	{
		opTime = HourStringToDate(m_AEtai, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AStoa.IsEmpty())
		{
			opTime = prmAFlight->Stoa;
		}
	}
*/
}

void CSeasonDlg::DepartureSchedTifd(CTime& opTime) 
{
	CString olTmpD;
	opTime = TIMENULL;
/*
	if(!m_DEtdi.IsEmpty())
	{
		CTime olSaveStod = prmDFlightSave->Tifd;
		if (bgSeasonLocal)
			ogBasicData.UtcToLocal(olSaveStod);

		m_CE_DEtdi.GetWindowText(olTmpD);
		opTime = HourStringToDate(olTmpD, olSaveStod);
	}
	else
	*/
	{
		if(!m_DStod.IsEmpty())
		{
			CTime olSaveStod = prmDFlightSave->Stod;
			if (bgSeasonLocal)
				ogBasicData.UtcToLocal(olSaveStod);

			m_CE_DStod.GetWindowText(olTmpD);
			opTime = HourStringToDate(olTmpD, olSaveStod); 
		}
	}
}

void CSeasonDlg::DepartureSchedTifa(CTime& opTime) 
{
	CString olTmpD;
	opTime = TIMENULL;
	if(!m_DStoa.IsEmpty())
	{
		CTime olSaveStod = prmDFlightSave->Stod;
		if (bgSeasonLocal)
			ogBasicData.UtcToLocal(olSaveStod);

		m_CE_DStoa.GetWindowText(olTmpD);
		opTime = HourStringToDate(olTmpD, olSaveStod);  
//		opTime = HourStringToDate(m_DStoa, prmDFlight->Stod);
	}
}


bool CSeasonDlg::CheckAll2(CString &opGMess, CString &opAMess, CString &opDMess) 
{
	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
	{
		return true;
	}


	if (prmAFlight->Urno != 0)
	{
		CTime SchedATifd = -1;
		CTime SchedATifa = -1;
		ArrivalSchedTifd(SchedATifd);
		ArrivalSchedTifa(SchedATifa);

		CTime AktATifd = -1;
		CTime AktATifa = -1;
//		ArrivalAktTifd(AktATifd);
//		ArrivalAktTifa(AktATifa);

		// check if Tifd at origin is smaller than Tifa at Home
		if((AktATifa != TIMENULL) && (AktATifd != TIMENULL))
		{
			if(AktATifa < AktATifd)
				opAMess += GetString(IDS_STRING2535) + CString("\n");//Actual Time for Arrival at Home before Actual Time of Departure at Origin
		}
		else
		{
			if((SchedATifa != TIMENULL) && (SchedATifd != TIMENULL))
			{
				if(SchedATifa < SchedATifd)
					opAMess += GetString(IDS_STRING2532) + CString("\n");//Planning Time for Arrival at Home before Planning Time of Departure at Origin
			}
		}

	}

	if (prmDFlight->Urno != 0)
	{

		CTime SchedDTifd = -1;
		CTime SchedDTifa = -1;
		DepartureSchedTifd(SchedDTifd);
		DepartureSchedTifa(SchedDTifa);

		CTime AktDTifd = -1;
		CTime AktDTifa = -1;
//		DepartureAktTifd(AktDTifd);
//		DepartureAktTifa(AktDTifa);

		// check if Tifd at origin is smaller than Tifa at Home
		if((AktDTifa != TIMENULL) && (AktDTifd != TIMENULL))
		{
			if(AktDTifa < AktDTifd)
				opAMess += GetString(IDS_STRING2537) + CString("\n");//Actual Time for Departure at Home after Planning Time of Arrival at Destination
		}
		else
		{
			if((SchedDTifa != TIMENULL) && (SchedDTifd != TIMENULL))
			{
				if(SchedDTifa < SchedDTifd)
					opAMess += GetString(IDS_STRING2533) + CString("\n");//Planning Time for Departure at Home after Planning Time of Arrival at Destination
			}
		}
	}

	if (prmDFlight->Urno != 0 && prmAFlight->Urno != 0)
	{
	//	check for tifa/tifd
		CTime ATifa = -1;
		CTime DTifd = -1;

		ArrivalSchedTifa(ATifa);
		DepartureSchedTifd(DTifd);

		if((ATifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(ATifa > DTifd)
				opGMess += GetString(IDS_STRING2534) + CString("\n");//Planning Time for Arrival at Home after Planning Time of Departure at Home
		}
	}

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;

}

void CSeasonDlg::OnOK() 
{

	CCS_TRY

	CString olGMessage;
	CString olAMessage;
	CString olDMessage;

	if(!bmChanged)
		return;


/*
	CString olAlc;
	CString olFltn;
	CString olFlns;
	CString olCsgn;

	if (prmAFlight)
	{
		if ( (imModus == DLG_CHANGE && prmAFlight->Urno != 0) || imModus != DLG_CHANGE)
		{
			m_CE_AAlc3.GetWindowText(olAlc);
			m_CE_AFltn.GetWindowText(olFltn);
			m_CE_AFlns.GetWindowText(olFlns);
			m_CE_ACsgn.GetWindowText(olCsgn);
			CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 
			if (olFlno.IsEmpty() && olCsgn.IsEmpty())
			{
				//"Flightnumber or Callsign must be filled!\n"
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_FLNO_OR_CSGN), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
		}
	}
*/
/*	if (prmDFlight)
	{
		if ( (imModus == DLG_CHANGE && prmDFlight->Urno != 0) || imModus != DLG_CHANGE)
		{
			m_CE_DAlc3.GetWindowText(olAlc);
			m_CE_DFltn.GetWindowText(olFltn);
			m_CE_DFlns.GetWindowText(olFlns);
			m_CE_DCsgn.GetWindowText(olCsgn);
			CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 
			if (olFlno.IsEmpty() && olCsgn.IsEmpty())
			{
				//"Flightnumber or Callsign must be filled!\n"
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_FLNO_OR_CSGN), GetString(ST_FEHLER), MB_ICONWARNING);
				return;
			}
		}
	}
*/
	

	if(imModus == DLG_CHANGE)
	{
		//check of duplicated single flights
		if (prmAFlight && prmAFlight->Urno != 0)
		{
			if(	m_CE_AStoa.IsChanged() || m_CE_AAlc3.IsChanged() || m_CE_AFltn.IsChanged() || m_CE_AFlns.IsChanged())
			{
				CString olAlc;
				CString olFltn;
				CString olFlns;
				CString olStoa;
				m_CE_AAlc3.GetWindowText(olAlc);
				m_CE_AFltn.GetWindowText(olFltn);
				m_CE_AFlns.GetWindowText(olFlns);
				m_CE_AStoa.GetWindowText(olStoa);

				CTime olTimeStoa = HourStringToDate( olStoa, prmAFlight->Stoa);

				CString olText;
				olText =  IsDupFlight(prmAFlight->Urno, "A", olAlc, olFltn, olFlns, olTimeStoa, bmLocalTime);
				if (!olText.IsEmpty())
				{
					if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
					{
						ResetDatesForFlight();
						return;
					}
				}
			}
		}

		if (prmDFlight->Urno != 0)
		{
			if(	m_CE_DStod.IsChanged() || m_CE_DAlc3.IsChanged() || m_CE_DFltn.IsChanged() || m_CE_DFlns.IsChanged())
			{
				CString olAlc;
				CString olFltn;
				CString olFlns;
				CString olStod;
				m_CE_DAlc3.GetWindowText(olAlc);
				m_CE_DFltn.GetWindowText(olFltn);
				m_CE_DFlns.GetWindowText(olFlns);
				m_CE_DStod.GetWindowText(olStod);

				CTime olTimeStod = HourStringToDate( olStod, prmDFlight->Stod);

				CString olText;
				olText =  IsDupFlight(prmDFlight->Urno, "D", olAlc, olFltn, olFlns, olTimeStod, bmLocalTime);
				if (!olText.IsEmpty())
				{
					if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
					{
						ResetDatesForFlight();
						return;
					}
				}
			}
		}
	}


	if (imModus == DLG_CHANGE_DIADATA)
		FillGatPosFlightData();
	else
		FillFlightData();


	if(!CheckAll(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
		{
			ResetDatesForFlight();
			return;
		}

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 
		
		CFPMSApp::MyTopmostMessageBox(this,olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		ResetDatesForFlight();
		return;
	}

	olGMessage.Empty();
	olAMessage.Empty();
	olDMessage.Empty();
	// Second check
	if(!CheckAll2(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
		{
			ResetDatesForFlight();
			return;
		}

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 

		CString olWarningText;
		olWarningText.Format("%s%s%s\n%s", olGMessage, olAMessage, olDMessage, GetString(IDS_STRING2037));
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}



	// additional belt check
	if (!AddBltCheck())
	{
		ResetDatesForFlight();
		return;
	}


	// Arrival Flight ID changed?
	if (strcmp(prmAFlight->Flti, prmAFlightSave->Flti) != 0)
	{
		m_AFlti.TrimRight();
		CString olMess;
 		if (m_AFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmAFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmAFlight->Flno);
		}
		if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}
	
	if(bgNatureAutoCalc)
	{
		if (strcmp(prmAFlight->Ttyp, prmAFlightSave->Ttyp) != 0)
		{
			m_ATtyp.TrimRight();
			CString olMess;
 			if (m_ATtyp.IsEmpty())
			{
				olMess.Format(GetString(IDS_NATURE_AUTOMAT), prmAFlight->Flno);
			}
			else
			{
				olMess.Format(GetString(IDS_NATURE_MANUAL), prmAFlight->Flno);
			}
			if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			{
				ResetDatesForFlight();
				return;
			}
		}
	}


	// Departure Flight ID changed?
	if (strcmp(prmDFlight->Flti, prmDFlightSave->Flti) != 0)
	{
		m_DFlti.TrimRight();
		CString olMess;
 		if (m_DFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmDFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmDFlight->Flno);
		}
		if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}

	
	if(bgNatureAutoCalc)
	{
		if (strcmp(prmDFlight->Ttyp, prmDFlightSave->Ttyp) != 0)
		{
			m_DTtyp.TrimRight();
			CString olMess;
 			if (m_DTtyp.IsEmpty())
			{
				olMess.Format(GetString(IDS_NATURE_AUTOMAT), prmDFlight->Flno);
			}
			else
			{
				olMess.Format(GetString(IDS_NATURE_MANUAL), prmDFlight->Flno);
			}
			if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			{
				ResetDatesForFlight();
				return;
			}
		}
	}
	
	if (imModus == DLG_COPY )
	{
		if (!m_CE_DFlti.IsChanged() && !bmDFltiUserSet)
		{
			prmDFlight->Flti[0]='\0';
		}
		if (!m_CE_AFlti.IsChanged() && !bmAFltiUserSet)
		{
			prmAFlight->Flti[0]='\0';
		}
		
		if(bgNatureAutoCalc)
		{
			//Nature//
			if (!m_CE_DTtyp.IsChanged() && !bmDNatureUserSet)
			{
				prmDFlight->Ttyp[0]='\0';
			}
			if (!m_CE_ATtyp.IsChanged() && !bmANatureUserSet)
			{
				prmAFlight->Ttyp[0]='\0';
			}
		}

	}

	m_CB_Ok.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Ok.UpdateWindow();


	if(bmLocalTime) 
	{
		ConvertStructLocalTOUtc(prmAFlight, prmDFlight);
//		ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
//		ogSeasonDlgFlights.StructLocalToUtc((void*)prmAFlight);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));



	if(imModus == DLG_CHANGE_DIADATA)
	{
		SEASONDLGFLIGHTDATA rlDFlightBak;
		SEASONDLGFLIGHTDATA rlDFlightSaveBak;
		if(prmDFlight->Urno != 0)
		{
			// copy the departure record is necessary because the update of the arrival
			// record can change the departure records!! The reason: The DB is not really 
			// changed, because the app is offline, so the changes will be transmitted
			// directly to the data class and this dialog!
			rlDFlightBak = *prmDFlight;
			rlDFlightSaveBak = *prmDFlightSave;
		}
		
		if(	prmAFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave, DIADATA);
		}
	
		if(prmDFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(&rlDFlightBak, &rlDFlightSaveBak, DIADATA);
		}
	}

	if(imModus == DLG_CHANGE)
	{
		if(	prmAFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave);
		}
	
		if(prmDFlight->Urno != 0)
		{

			omCcaData.UpdateSpecial(omDCins, omDCinsSave, prmDFlight->Urno);
			bmChanged = false;
			//ENDE ACHTUNG MWO das ist nur eine Zwischenl�sung

			ogSeasonDlgFlights.UpdateFlight(prmDFlight, prmDFlightSave);
			GetCcas(); 

//##### PRF 2055 NUR POPS44
			// if both legs cxx or noop -> don�t split the rotation -> join again !
			if(	prmAFlight && prmDFlight && prmAFlight->Urno != 0 && prmDFlight->Urno != 0  && CString(pcgHome) != "WAW")
			{
				if(		(strcmp(prmDFlight->Ftyp, "X") == 0 || strcmp(prmDFlight->Ftyp, "N") == 0)
					&&  (strcmp(prmAFlight->Ftyp, "X") == 0 || strcmp(prmAFlight->Ftyp, "N") == 0) )
				{
					SEASONDLGFLIGHTDATA* prlAFlight = NULL;
					SEASONDLGFLIGHTDATA* prlDFlight = NULL;

					char pclSelectionArr[256];
					sprintf(pclSelectionArr, " URNO = %ld", prmAFlight->Urno);

					ogSeasonDlgFlights.ReadFlights(pclSelectionArr);
					prlAFlight = ogSeasonDlgFlights.GetFlightByUrno(prmAFlight->Urno);

					char pclSelectionDep[256];
					sprintf(pclSelectionDep, " URNO = %ld", prmDFlight->Urno);

					ogSeasonDlgFlights.ReadFlights(pclSelectionDep);
					prlDFlight = ogSeasonDlgFlights.GetFlightByUrno(prmDFlight->Urno);

					if (prlAFlight && prlDFlight)
					{
						CedaFlightUtilsData olFlightUtilsData;
						olFlightUtilsData.JoinFlight(prlAFlight->Urno, prlAFlight->Rkey, prlDFlight->Urno, prlDFlight->Rkey, true);
					}
				}
			}
//##### PRF 2055 NUR POPS44



		}


	}
	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		CTime olDayBeg = DateStringToDate(m_Pvom);
		CTime olDayEnd = DateStringToDate(m_Pbis);
		if (olDayBeg == TIMENULL || olDayEnd == TIMENULL)
		{
			CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING_INVALID_DATE_PROMPT), GetString(IDS_WARNING), MB_OK);
			ResetDatesForFlight();
			return;
		}

		CString olText;		
		CString olArrDays(""); 
		if(bmAnkunft)
		{
			CString olAlc;
			CString olFltn;
			CString olFlns;
			CString olStoa;
			m_CE_AAlc3.GetWindowText(olAlc);
			m_CE_AFltn.GetWindowText(olFltn);
			m_CE_AFlns.GetWindowText(olFlns);
			m_CE_AStoa.GetWindowText(olStoa);

			CTime olTime1 = HourStringToDate(olStoa, olDayBeg);
			CTime olTime2 = HourStringToDate(olStoa, olDayEnd);

			CTime olDay = olDayBeg;
			CTimeSpan olOneDay (1,0,0,0);
			CTimeSpan olTimediff = olDayEnd - olDayBeg;
			int ilAnzDay = olTimediff.GetDays();

			for (int k = 0; k <= ilAnzDay; k++)
			{
				CTime olStoaDay (olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), olTime1.GetHour(), olTime1.GetMinute(), 0);
				char pclDays[10];
				GetDayOfWeek(olStoaDay, pclDays);
				if (m_ADays.Find(pclDays) >= 0)
				{
					CTime olTmp = HourStringToDate(olStoa, olStoaDay);
					GetDayOfWeek(olTmp, pclDays);
					olArrDays += pclDays;
				}
				olDay += olOneDay;
			}

//			olText = IsDupFlightSeason(0, "A", olAlc, olFltn, olFlns, olTime1, olTime2, m_ADays, bmLocalTime, true);
			olText = IsDupFlightSeason(0, "A", olAlc, olFltn, olFlns, olTime1, olTime2, olArrDays, bmLocalTime, true);
		}

		if(bmAbflug)
		{
			CString olAlc;
			CString olFltn;
			CString olFlns;
			CString olStod;
			m_CE_DAlc3.GetWindowText(olAlc);
			m_CE_DFltn.GetWindowText(olFltn);
			m_CE_DFlns.GetWindowText(olFlns);
			m_CE_DStod.GetWindowText(olStod);

			CTime olTime1 = HourStringToDate(olStod, olDayBeg);
			CTime olTime2 = HourStringToDate(olStod, olDayEnd);

			CString olDepDays(""); 
			CString ol_DDays ("");
			if(bmAnkunft)
				ol_DDays = m_ADays;
			else
				ol_DDays = m_DDays;

			CTime olDay = olDayBeg;
			CTimeSpan olOneDay (1,0,0,0);
			CTimeSpan olTimediff = olDayEnd - olDayBeg;
			int ilAnzDay = olTimediff.GetDays();

			for (int k = 0; k <= ilAnzDay; k++)
			{
				CTime olStodDay (olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), olTime1.GetHour(), olTime1.GetMinute(), 0);
				char pclDays[10];
				GetDayOfWeek(olStodDay, pclDays);
				if (ol_DDays.Find(pclDays) >= 0)
				{
					CTime olTmp = HourStringToDate(olStod, olStodDay);
					GetDayOfWeek(olTmp, pclDays);
					olDepDays += pclDays;
				}
				olDay += olOneDay;
			}

			olText += IsDupFlightSeason(0, "D", olAlc, olFltn, olFlns, olTime1, olTime2, olDepDays, bmLocalTime, true);
//			olText += IsDupFlightSeason(0, "D", olAlc, olFltn, olFlns, olTime1, olTime2, m_DDays, bmLocalTime, true);
		}

		if( !olText.IsEmpty())
		{

			if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
			{
				ResetDatesForFlight();
				return;
			}
		}


	

		if (BlackList()) return;//UFIS-1159


		CString olTage;
		if(bmAnkunft && bmAbflug)
		{
			
			if(bgShowPayDetail) 
			{
				prmAFlight->Urno=ogBasicData.GetNextUrno("AFT");
				prmDFlight->Urno=ogBasicData.GetNextUrno("AFT");
			}

			ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, prmDFlight, omDCins);

			if(bgShowPayDetail)
			{
				//if(m_dlgAModeOfPay != NULL)
				{
					char* strField= "COBY,HDAG,BLTO,PRMK,RREQ,RRMK,MOPA";
					char* strPCL= ogCedaMOPData.GetMOPInternalData("A");
					bool result = ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, strField, strPCL);
					bool rt = ogCedaMOPData.SaveMOPInternal(prmAFlight->Urno,"A");

					if(rt)
					{
						ogCedaMOPData.ClearData("A");
						if(imModus == DLG_NEW)
						{
							imModus = DLG_CHANGE;
						}
					}
					
				}
			}

			if(bgShowPayDetail)
			{
				//if(m_dlgDModeOfPay != NULL)
				{	
					char* strField= "COBY,HDAG,BLTO,PRMK,RREQ,RRMK,MOPA";
					char* strPCL= ogCedaMOPData.GetMOPInternalData("D");
					bool result = ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, strField, strPCL);
					bool rt = ogCedaMOPData.SaveMOPInternal(prmDFlight->Urno,"D");

					if(rt)
					{
						ogCedaMOPData.ClearData("D");
						if(imModus == DLG_NEW)
						{
							imModus = DLG_CHANGE;
						}
					}
				}
			}

		}
		else
		{

			if(bmAnkunft)
			{
				if(bgShowPayDetail) 
				{
					prmAFlight->Urno=ogBasicData.GetNextUrno("AFT");
				}
				

				ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, NULL, omDCins);

				if(bgShowPayDetail)
				{
					
					char* strField= "COBY,HDAG,BLTO,PRMK,RREQ,RRMK,MOPA";
					char* strPCL= ogCedaMOPData.GetMOPInternalData("A");
					bool result = ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, strField, strPCL);
					bool rt = ogCedaMOPData.SaveMOPInternal(prmAFlight->Urno,"A");	

					if(rt)
					{
						ogCedaMOPData.ClearData("A");
						if(imModus == DLG_NEW)
						{
							imModus = DLG_CHANGE;
						}

					}
					
				}

				
			}
			else
			{
				if(bgShowPayDetail) 
				{
					prmDFlight->Urno=ogBasicData.GetNextUrno("AFT");
				}

				ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, NULL, prmDFlight, omDCins);

				if(bgShowPayDetail)
				{
					char* strField= "COBY,HDAG,BLTO,PRMK,RREQ,RRMK,MOPA";
					char* strPCL= ogCedaMOPData.GetMOPInternalData("D");
					bool result = ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, strField, strPCL);
					bool rt = ogCedaMOPData.SaveMOPInternal(prmDFlight->Urno,"D");

					if(rt)
					{
						ogCedaMOPData.ClearData("D");
						if(imModus == DLG_NEW)
						{
							imModus = DLG_CHANGE;
						}
					}
					
				}

			

			}
		}
		bmChanged = false;
	}

//MWO
	omOldCcaValues.DeleteAll();
	for(int ili = 0; ili < omOldDCins.GetSize(); ili++)
	{
		omOldCcaValues.NewAt(omOldCcaValues.GetSize(), omOldDCins[ili]);
	}

//END MWO
	

	if(bmLocalTime && imModus != DLG_CHANGE_DIADATA)
	{
		// In DLG_CHANGE_DIADATA mode the new records are already in the local data
		// structures and already changed to local times if necessary
//		ogSeasonDlgFlights.StructUtcToLocal((void*)prmAFlight);
//		ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
		ConvertStructUtcTOLocal(prmAFlight, prmDFlight);
	}
	
	bmChanged = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	char clPaid= ' ';
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		
	}

//	clPaid = GetPaid();

	UpdateCashButton(olAlc3, clPaid);


	CCS_CATCH_ALL

}










void CSeasonDlg::OnAreset() 
{

	CCS_TRY

	CString olPvom; 
	m_CE_Pvom.GetWindowText(olPvom);

	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	delete prlFlight;
	m_ADays = "";
	omAJfno.DeleteAll();
	InitDialog(true,false);
	m_CE_Pvom.SetInitText(olPvom);

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDreset() 
{

	CCS_TRY

	CString olPbis; 
	m_CE_Pbis.GetWindowText(olPbis);

	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	m_DDays = "";
	delete prlFlight;
	omDJfno.DeleteAll();
	InitDialog(false, true);	
	m_CE_Pbis.SetInitText(olPbis);

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDailyMask() 
{
 	pogRotationDlg->NewData(pomParent, lmRkey, lmCalledBy, omAdid, bmLocalTime);
}



void CSeasonDlg::OnDefault() 
{

	CCS_TRY

	Settestdefault();	

	CCS_CATCH_ALL

}


///////////////////////////////////////////////////////////////////////////
// testdefault
//
void CSeasonDlg::Settestdefault() 
{

	CCS_TRY

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	m_ADays = "";
	m_DDays = "";



	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;

	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;


	delete prlFlight;

	InitDialog(true, true);	

	char buffer[32];

	CTimeSpan olSpan(0,0,1,0);


	omOpenTime += olSpan;
	m_CE_AStoa.SetInitText(omOpenTime.Format("%H%M"));

	CTime olCurrTime = omOpenTime + olSpan + olSpan + olSpan + olSpan + olSpan;
	m_CE_DStod.SetInitText(olCurrTime.Format("%H%M"));


	m_CE_Act3.SetInitText("747");
	m_CE_Ming.SetInitText("37");
	m_CE_Pvom.SetInitText(omOpenTime.Format("%d%m%Y"));
	m_CE_Pbis.SetInitText(omOpenTime.Format("%d%m%Y"));
	m_CE_Freq.SetInitText("1");

	m_CE_AOrg3.SetInitText("INN");
	m_CE_AAlc3.SetInitText("DLH");
	m_CE_AFltn.SetInitText(omOpenTime.Format("%H%M"));
	m_CE_AFlns.SetInitText("X");
	m_CE_DFlns.SetInitText("Y");
	m_CE_DDes3.SetInitText("ABZ");
	m_CE_DAlc3.SetInitText("DLH");
	m_CE_DFltn.SetInitText(olCurrTime.Format("%H%M"));

	itoa(GetDayOfWeek(omOpenTime), buffer, 10);
	m_CE_ADays.SetInitText(CString(buffer));
	m_CE_DDays.SetInitText(CString(buffer));


	omAAlc3 = "DLH";
	omAAlc2 = "LH";

	omDAlc3 = "DLH";
	omDAlc2 = "LH";
	bmAnkunft = true;
	bmAbflug = true;


	//m_CE_AEtai.SetInitText("2000");
	//m_CE_AStod.SetInitText("1800");

	//m_CE_DEtdi.SetInitText("2200");
	//m_CE_DStoa.SetInitText("2300");




/*
	m_CE_ATtyp.SetInitText("V");
	m_CE_AHtyp.SetInitText("HC");
	m_CE_AStyp.SetInitText("S");
	m_CE_AStev.SetInitText("K");
	m_CE_APsta.SetInitText("POSI2");
	m_CE_AGta1.SetInitText("GATE1");
	m_CE_AGta2.SetInitText("GATE2");
	m_CE_DFlns.SetInitText("X");
	m_CE_DTtyp.SetInitText("V");
	m_CE_DHtyp.SetInitText("HC");
	m_CE_DStyp.SetInitText("S");
	m_CE_DStev.SetInitText("K");
	m_CE_DPstd.SetInitText("POSI1");
	m_CE_DGta1.SetInitText("GATE1");
	m_CE_DGta2.SetInitText("GATE2");

*/

	CCS_CATCH_ALL

}








LONG CSeasonDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if (!prlNotify->IsChanged)
		return 0L;

	CString olAloc;
	CString olTmp;
	CString olTmp1;

	CTime olTimeRefA = TIMENULL;
	CTime olTimeRefD = TIMENULL;

	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		CString olTmpA;
		CString olTmpD;
		m_CE_AStoa.GetWindowText(olTmpA);
		m_CE_DStod.GetWindowText(olTmpD);
		olTimeRefA = HourStringToDate( olTmpA, prmAFlight->Stoa);
		olTimeRefD = HourStringToDate( olTmpD, prmDFlight->Stod);
	}
	else
	{
		CString olTmpA;
		CString olTmpD;
		m_CE_AStoa.GetWindowText(olTmpA);
		m_CE_DStod.GetWindowText(olTmpD);
		olTimeRefA = HourStringToDate( olTmpA, prmAFlight->Tifa);
		olTimeRefD = HourStringToDate( olTmpD, prmDFlight->Tifd);
//		olTimeRefA = prmAFlight->Tifa;
//		olTimeRefD = prmDFlight->Tifd;
	}


//	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		if(((UINT)m_CE_AStoa.imID == wParam) && (!prlNotify->Text.IsEmpty()))
		{
			if (prlNotify->IsChanged)
			{
				m_CE_AGa1b.GetWindowText(olTmp);
				m_CE_AGa1e.GetWindowText(olTmp1);
				m_CE_AGta1.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStoa = HourStringToDate( prlNotify->Text, olTimeRefA);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefA);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefA);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetGatDefAllocDur(olAloc, olDura, true);
					}
					
					m_CE_AGa1b.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
					m_CE_AGa1e.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}

				m_CE_AGa2b.GetWindowText(olTmp);
				m_CE_AGa2e.GetWindowText(olTmp1);
				m_CE_AGta2.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStoa = HourStringToDate( prlNotify->Text, olTimeRefA);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefA);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefA);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetGatDefAllocDur(olAloc, olDura, true);
					}
					
					m_CE_AGa2b.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
					m_CE_AGa2e.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}

				m_CE_AB1bs.GetWindowText(olTmp);
				m_CE_AB1es.GetWindowText(olTmp1);
				m_CE_ABlt1.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStoa = HourStringToDate( prlNotify->Text, olTimeRefA);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefA);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefA);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
						{
	//						GetBltDefAllocDur(olAloc, olDura);
							if (!bgUseBeltAllocTime) 
							GetBltDefAllocDur(olAloc, olDura);
							else
								GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,olAloc, olDura);
						}
					}
					
					m_CE_AB1bs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
					m_CE_AB1es.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}

				m_CE_AB2bs.GetWindowText(olTmp);
				m_CE_AB2es.GetWindowText(olTmp1);
				m_CE_ABlt2.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStoa = HourStringToDate( prlNotify->Text, olTimeRefA);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefA);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefA);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
						{
//							GetBltDefAllocDur(olAloc, olDura);
							if (!bgUseBeltAllocTime) 
							GetBltDefAllocDur(olAloc, olDura);
							else
								GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,olAloc, olDura);
						}
					}
					
					m_CE_AB2bs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
					m_CE_AB2es.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}

				m_CE_APabs.GetWindowText(olTmp);
				m_CE_APaes.GetWindowText(olTmp1);
				m_CE_APsta.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStoa = HourStringToDate( prlNotify->Text, olTimeRefA);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefA);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefA);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetPosDefAllocDur(olAloc, atoi(prmAFlight->Ming), olDura);
					}
					
					m_CE_APabs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
					m_CE_APaes.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}
			}

//			return 0L;
		}

		if(((UINT)m_CE_DStod.imID == wParam) && (!prlNotify->Text.IsEmpty()))
		{
			if (prlNotify->IsChanged)
			{
				m_CE_DGd1b.GetWindowText(olTmp);
				m_CE_DGd1e.GetWindowText(olTmp1);
				m_CE_DGtd1.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetGatDefAllocDur(olAloc, olDura);
					}
					
					m_CE_DGd1b.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					m_CE_DGd1e.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				}

				m_CE_DGd2b.GetWindowText(olTmp);
				m_CE_DGd2e.GetWindowText(olTmp1);
				m_CE_DGtd2.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetGatDefAllocDur(olAloc, olDura);
					}
					
					m_CE_DGd2b.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					m_CE_DGd2e.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				}

				m_CE_DW1bs.GetWindowText(olTmp);
				m_CE_DW1es.GetWindowText(olTmp1);
				m_CE_DWro1.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetWroDefAllocDur(olAloc, olDura);
					}
					
					m_CE_DW1bs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					m_CE_DW1es.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				}

				m_CE_DW2bs.GetWindowText(olTmp);
				m_CE_DW2es.GetWindowText(olTmp1);
				m_CE_DWro2.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetWroDefAllocDur(olAloc, olDura);
					}
					
					m_CE_DW2bs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					m_CE_DW2es.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				}

				m_CE_DPdbs.GetWindowText(olTmp);
				m_CE_DPdes.GetWindowText(olTmp1);
				m_CE_DPstd.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
							GetPosDefAllocDur(olAloc, atoi(prmDFlight->Ming), olDura);
					}
					
					m_CE_DPdbs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					m_CE_DPdes.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				}
			
				if(pomDCinsTable && strcmp(pcgHome, "HAJ") == 0)
				{
					int ilLines = pomDCinsTable->GetLinesCount();
					for(int i = 0; i < ilLines; i++)
					{
						CString olCic;
						CString olBs;
						CString olEs;
						CString olDisp;

						pomDCinsTable->GetTextFieldValue(i, 0, olCic);
						if (bgCnamAtr)
						{
							pomDCinsTable->GetTextFieldValue(i, 3, olBs);
							pomDCinsTable->GetTextFieldValue(i, 4, olEs);
							if(bgShowClassInBatchDlg)
							{
								pomDCinsTable->GetTextFieldValue(i, 5, olDisp);
							}
						}
						else
						{
							pomDCinsTable->GetTextFieldValue(i, 2, olBs);
							pomDCinsTable->GetTextFieldValue(i, 3, olEs);
							if(bgShowClassInBatchDlg)
							{
								pomDCinsTable->GetTextFieldValue(i , 4, olDisp); 
							}
						}

						CTimeSpan olDura (0,1,0,0);
						CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
						if ((!olBs.IsEmpty() && !olEs.IsEmpty()) || !olCic.IsEmpty())
						{
							if (!olBs.IsEmpty() && !olEs.IsEmpty())
							{
								CTime olBeg = HourStringToDate( olBs, olTimeRefD);
								CTime olEnd = HourStringToDate( olEs, olTimeRefD);
								olDura = olEnd - olBeg;
								if (olDura.GetTotalMinutes() < 0)
									olDura = olBeg - olEnd;
							}

							if (bgCnamAtr)
							{
								pomDCinsTable->SetTextFieldValue(i, 3, DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
								pomDCinsTable->SetTextFieldValue(i, 4, DateToHourDivString(olTimeRefD, olTimeRefD));
								if(bgShowClassInBatchDlg)
								{
									pomDCinsTable->SetTextFieldValue(i, 5, olDisp);
								}
							}
							else
							{
								pomDCinsTable->SetTextFieldValue(i, 2, DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
								pomDCinsTable->SetTextFieldValue(i, 3, DateToHourDivString(olTimeRefD, olTimeRefD));
								if(bgShowClassInBatchDlg)
								{
									pomDCinsTable->SetTextFieldValue(i, 4, olDisp);
								}
							}
						}


					}
					pomDCinsTable->DisplayTable();
				}
			}

				m_CE_DB1bs.GetWindowText(olTmp);
				m_CE_DB1es.GetWindowText(olTmp1);
				m_CE_DBlt1.GetWindowText(olAloc);
				if ( (!olTmp.IsEmpty() && !olTmp1.IsEmpty() ) || !olAloc.IsEmpty())
				{
					CTimeSpan olDura (0,0,0,0);
					CTime olStod = HourStringToDate( prlNotify->Text, olTimeRefD);
					if (!olTmp.IsEmpty() && !olTmp1.IsEmpty())
					{
						CTime olBeg = HourStringToDate( olTmp, olTimeRefD);
						CTime olEnd = HourStringToDate( olTmp1, olTimeRefD);
						olDura = olEnd - olBeg;
						if (olDura.GetTotalMinutes() < 0)
							olDura = olBeg - olEnd;
					}
					else
					{
						if(!olAloc.IsEmpty())
						{
	//						GetBltDefAllocDur(olAloc, olDura);
							if (!bgUseBeltAllocTime) 
							GetBltDefAllocDur(olAloc, olDura);
							else
								GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,olAloc, olDura);
						}
					}
					
					m_CE_DB1bs.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
					m_CE_DB1es.SetWindowText(DateToHourDivString(olTimeRefD + olDura, olTimeRefD));
				}

//			return 0L;
		}
		
	}



	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);

	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		olRegn.TrimRight();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
//		else
//			return 0L;
		
		
		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{
			if((prlNotify->Text != olAct3) && (prlNotify->Text != olAct5))
			{
				if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
				{
					m_CE_Act3.GetWindowText(olAct3);
					m_CE_Act5.GetWindowText(olAct5);

					ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

					ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
					ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
					ogBCD.Save("ACR");
				}
				else
				{
					m_CE_Act3.SetInitText( olAct3, true);
					m_CE_Act5.SetInitText( olAct5, true);
				}
			}
		}
	}


	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		prlNotify->Text.TrimRight();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
//		else
//			return 0L;
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				prlNotify->Status = true;
			}
			else
				m_CE_Regn.SetInitText("", true);

		}
//		return 0L;
	}




	if((UINT)m_CE_ADays.imID == wParam)
	{
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		m_CE_ADays.GetWindowText(m_ADays);;
		if((olVom != TIMENULL) && (olBis != TIMENULL))
		{
			if(!DaysinPeriod(olVom,  olBis,  m_ADays))
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}	
		}

	}
	if((UINT)m_CE_DDays.imID == wParam)
	{
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		m_CE_DDays.GetWindowText(m_DDays);;
		if((olVom != TIMENULL) && (olBis != TIMENULL))
		{
			if(!DaysinPeriod(olVom,  olBis,  m_DDays))
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}	
		}
	}


	if((UINT)m_CE_AOrg3.imID == wParam)
	{
		if(strcmp(prlNotify->Text , pcgHome) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(3,3,false);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1388));
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,3,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1389));
		}
	}

	if((UINT)m_CE_DDes3.imID == wParam)
	{
		if(strcmp(prlNotify->Text , pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(3,3,false);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING1388));
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,3,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING1390));
		}
	}


	if(((UINT)m_CE_AOrg3.imID == wParam) || ((UINT)m_CE_DDes3.imID == wParam))
	{
		prlNotify->UserStatus = true;
		if (prlNotify->Text.IsEmpty())
		{
			prlNotify->Status = true;
//			return 0L;
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp );
			if((UINT)m_CE_AOrg3.imID == wParam)
			{
				CString olOld = "";
				m_CE_AOrg4.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_AOrg4.SetInitText(olTmp, true);
			}
			else
			{
				CString olOld = "";
				m_CE_DDes4.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DDes4.SetInitText(olTmp, true);
			}
		}
//		return 0L;
	}


	if(((UINT)m_CE_AOrg4.imID == wParam) || ((UINT)m_CE_DDes4.imID == wParam))
	{
		prlNotify->UserStatus = true;
		if (prlNotify->Text.IsEmpty())
		{
			prlNotify->Status = false;
//			return 0L;
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("APT", "APC4", prlNotify->Text, "APC3", olTmp );
			if((UINT)m_CE_AOrg4.imID == wParam)
			{
				CString olOld = "";
				m_CE_AOrg3.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_AOrg3.SetInitText(olTmp, true);
			}
			else
			{
				CString olOld = "";
				m_CE_DDes3.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DDes3.SetInitText(olTmp, true);
			}
		}
//		return 0L;
	}



	if((UINT)m_CE_DAlc3.imID == wParam) 
	{
		omDAlc2 = "";
		omDAlc3 = "";

		CString olStod; 
		CTime olTmp;
		if((imModus == DLG_NEW) || (imModus == DLG_COPY))
		{
			m_CE_Pvom.GetWindowText(m_Pvom);
			olTmp = DateStringToDate(m_Pvom); 

			CString olStrStod;
			m_CE_DStod.GetWindowText(olStrStod);
			if (olStrStod.IsEmpty())
				olStrStod = CString("23:59");

			CTime olStodTime = HourStringToDate( olStrStod, olTmp);
		
			
			olStod = CTimeToDBString( olStodTime, olStodTime);
		}
		else
		{
			if(prmDFlight != NULL)
				olStod = CTimeToDBString( prmDFlight->Stod, prmDFlight->Stod);

		}

		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omDAlc2, omDAlc3, olStod );
//		return 0L;
	}
	if((UINT)m_CE_AAlc3.imID == wParam)
	{
		omAAlc2 = "";
		omAAlc3 = "";

		CString olStoa; 
		CTime olTmp;
		if((imModus == DLG_NEW) || (imModus == DLG_COPY))
		{
			m_CE_Pvom.GetWindowText(m_Pvom);
			olTmp = DateStringToDate(m_Pvom); 

			
			
			CString olStrStoa;
			m_CE_AStoa.GetWindowText(olStrStoa);
			if (olStrStoa.IsEmpty())
				olStrStoa = CString("23:59");

			CTime olStoaTime = HourStringToDate( olStrStoa, olTmp);
		
			
			olStoa = CTimeToDBString( olStoaTime, olStoaTime);
			
		}
		else
		{
			if(prmAFlight != NULL)
				olStoa = CTimeToDBString( prmAFlight->Stoa, prmAFlight->Stoa);

		}
		
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omAAlc2, omAAlc3, olStoa );
//		return 0L;
	}

	if(((UINT)m_CE_Act3.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp3 = prlNotify->Text;
			CString olTmp5;
			m_CE_Act5.GetWindowText(olTmp5);
			if (olTmp5.IsEmpty())
				olTmp5 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT3","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
				{
					CString olOld = "";
					m_CE_Act5.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_Act5.SetInitText(olTmp);
				}

				if (ilCount > 1)
				{
					OnAct3list();
				}
			}
		}
	
//		return 0L;
	}


	if(((UINT)m_CE_Act5.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp5 = prlNotify->Text;
			CString olTmp3;
			m_CE_Act3.GetWindowText(olTmp3);
			if (olTmp3.IsEmpty())
				olTmp3 = " ";


			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

//			olWhere.Format("WHERE ACT3 = '%s' AND ACT5 = '%s'",olTmp3, olTmp5);
//			int ilCount = ogBCD.SelectCount("ACT", olWhere);

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();
//				olWhere.Format("WHERE ACT5 = '%s'",prlNotify->Text);
//				int ilCount = ogBCD.SelectCount("ACT", olWhere);

				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
				{
					CString olOld = "";
					m_CE_Act3.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_Act3.SetInitText(olTmp);
				}

				if (ilCount > 1)
					OnAct35list();
			}
		}
//		return 0L;
	}

/*
	if(((UINT)m_CE_Act3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = false;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
		{
			m_CE_Act5.SetInitText(olTmp);
		}
		return 0L;
	}

	if((UINT)m_CE_Act5.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = false;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
		{
			m_CE_Act3.SetInitText(olTmp);
		}
		return 0L;
	}

*/


	if( (UINT)m_CE_AGta1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AGta1.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATga1.GetWindowText(olOld);
				if (olOld != "")
					m_CE_ATga1.SetInitText("",true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT_PORT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AGta1.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATga1.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATga1.SetInitText(olTmp,true);
				}

				m_CE_AGa1b.GetWindowText(olTmp);
				m_CE_AGa1e.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AGa1b.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
		
					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura, true);
					
					m_CE_AGa1e.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}
			}
		}
//		return 0L;
	}



	if( (UINT)m_CE_AGta2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AGta2.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATga2.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATga2.SetInitText("",true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT_PORT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AGta2.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATga2.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATga2.SetInitText(olTmp,true);
				}

					m_CE_AGa2b.GetWindowText(olTmp);
					m_CE_AGa2e.GetWindowText(olTmp1);

					if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
					{
						m_CE_AGa2b.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));

						CTimeSpan olDura;
						GetGatDefAllocDur(prlNotify->Text, olDura, true);
						
						m_CE_AGa2e.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}
			}
		}
//		return 0L;
	}



	if( (UINT)m_CE_DGtd1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_DGtd1.imID == wParam)
			{
				CString olOld = "";
				m_CE_DTgd1.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_DTgd1.SetInitText("",true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_DGtd1.imID == wParam)
				{
					CString olOld = "";
					m_CE_DTgd1.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_DTgd1.SetInitText(olTmp,true);
				}

				m_CE_DGd1e.GetWindowText(olTmp);
				m_CE_DGd1b.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DGd1e.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					m_CE_DGd1b.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
				}
			}
		}
//		return 0L;
	}



	if( (UINT)m_CE_DGtd2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_DGtd2.imID == wParam)
			{
				CString olOld = "";
				m_CE_DTgd2.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_DTgd2.SetInitText("",true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_DGtd2.imID == wParam)
				{
					CString olOld = "";
					m_CE_DTgd2.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_DTgd2.SetInitText(olTmp,true);
				}

					m_CE_DGd2b.GetWindowText(olTmp);
					m_CE_DGd2e.GetWindowText(olTmp1);

					if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
					{
						m_CE_DGd2e.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));

						CTimeSpan olDura;
						GetGatDefAllocDur(prlNotify->Text, olDura);			
						m_CE_DGd2b.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
					}
			}
		}
//		return 0L;
	}




	if( (UINT)m_CE_ABlt1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
				CString olOld = "";
				m_CE_ATmb1.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATmb1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATmb1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATmb1.SetInitText(olTmp,true);

				m_CE_AB1bs.GetWindowText(olTmp);
				m_CE_AB1es.GetWindowText(olTmp1);

				CString olExt;
				if (ogPosDiaFlightData.GetExit(prlNotify->Text, 1, olExt))
				{
					m_CE_AExt1.GetWindowText(olOld);
					if (olExt != olOld)
						m_CE_AExt1.SetInitText(olExt,true);

					if(ogBCD.GetField("EXT", "ENAM", olExt, "TERM", olTmp ))
					{
						m_CE_ATet1.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_ATet1.SetInitText(olTmp, true);
					}
				}

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AB1bs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
				
					CTimeSpan olDura;
//					GetBltDefAllocDur(prlNotify->Text, olDura);
					if (!bgUseBeltAllocTime) 
					GetBltDefAllocDur(prlNotify->Text, olDura);
					else
						GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,prlNotify->Text, olDura);
					m_CE_AB1es.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}
			}
		}
//		return 0L;
	}


	if( (UINT)m_CE_ABlt2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_ATmb2.GetWindowText(olOld);
			if ("" != olOld)
				m_CE_ATmb2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATmb2.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATmb2.SetInitText(olTmp,true);

				m_CE_AB2bs.GetWindowText(olTmp);
				m_CE_AB2es.GetWindowText(olTmp1);

				CString olExt;
				if (ogPosDiaFlightData.GetExit(prlNotify->Text, 2, olExt))
				{
					m_CE_AExt2.GetWindowText(olOld);
					if (olExt != olOld)
						m_CE_AExt2.SetInitText(olExt,true);

					if(ogBCD.GetField("EXT", "ENAM", olExt, "TERM", olTmp ))
					{
						m_CE_ATet2.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_ATet2.SetInitText(olTmp, true);
					}
				}

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AB2bs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));

					CTimeSpan olDura;
//					GetBltDefAllocDur(prlNotify->Text, olDura);
					if (!bgUseBeltAllocTime) 
					GetBltDefAllocDur(prlNotify->Text, olDura);
					else
						GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,prlNotify->Text, olDura);
					m_CE_AB2es.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));
				}
			}
		}
//		return 0L;
	}

	if( (UINT)m_CE_DBlt1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
				CString olOld = "";
				m_CE_DTmb1.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_DTmb1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTmb1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTmb1.SetInitText(olTmp,true);

				m_CE_DB1bs.GetWindowText(olTmp);
				m_CE_DB1es.GetWindowText(olTmp1);

			
				/*
				CString olExt;
				if (ogPosDiaFlightData.GetExit(prlNotify->Text, 1, olExt))
				{
					m_CE_AExt1.GetWindowText(olOld);
					if (olExt != olOld)
						m_CE_AExt1.SetInitText(olExt,true);

					if(ogBCD.GetField("EXT", "ENAM", olExt, "TERM", olTmp ))
					{
						m_CE_ATet1.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_ATet1.SetInitText(olTmp, true);
					}
				}
				*/

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DB1bs.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
				
					CTimeSpan olDura;
//					GetBltDefAllocDur(prlNotify->Text, olDura);					
					if (!bgUseBeltAllocTime) 
					GetBltDefAllocDur(prlNotify->Text, olDura);
					else
						GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,prlNotify->Text, olDura);

					m_CE_DB1es.SetWindowText(DateToHourDivString(olTimeRefD + olDura, olTimeRefD));
				}
			}
		}
//		return 0L;
	}



	if(((UINT)m_CE_AExt1.imID == wParam) || ((UINT)m_CE_AExt2.imID == wParam))
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AExt1.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATet1.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATet1.SetInitText("", true);
			}
			if((UINT)m_CE_AExt2.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATet2.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATet2.SetInitText("", true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("EXT", "ENAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AExt1.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATet1.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATet1.SetInitText(olTmp, true);
				}
				if((UINT)m_CE_AExt2.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATet2.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATet2.SetInitText(olTmp, true);
				}
			}
		}
//		return 0L;
	}

	if((UINT)m_CE_DWro1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTwr1.GetWindowText(olOld);
			if ("" != olOld)
				m_CE_DTwr1.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
	
			
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTwr1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTwr1.SetInitText(olTmp, true);
			
				m_CE_DW1es.GetWindowText(olTmp);
				m_CE_DW1bs.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DW1es.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
					
					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					m_CE_DW1bs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));	
				}
			}
		}
//		return 0L;
	}

	if((UINT)m_CE_DWro2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTwr2.GetWindowText(olOld);
			if ("" != olOld)
				m_CE_DTwr2.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
	
			
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTwr2.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTwr2.SetInitText(olTmp, true);
			
				m_CE_DW2es.GetWindowText(olTmp);
				m_CE_DW2bs.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DW2es.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
					
					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					m_CE_DW2bs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));	
				}
			}
		}
//		return 0L;
	}

	CTime olVom = TIMENULL;
	CTime olBis = TIMENULL;
	CString olASeas;
	CString olDSeas;

	if((UINT)m_CE_Pvom.imID == wParam)
	{
		m_CE_Pbis.GetWindowText(m_Pbis);
		olVom = DateStringToDate(prlNotify->Text); 
		if(!m_Pbis.IsEmpty() && !prlNotify->Text.IsEmpty())
		{
			olBis = DateStringToDate(m_Pbis);
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(olVom > olBis)
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = false;
					m_CE_Pbis.SetStatus(false);
				}
				else
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = true;
					m_CE_Pbis.SetStatus(true);
				}
			}
		}
		if((olVom != TIMENULL) && !CheckUtcPast(olVom))
		{
			prlNotify->UserStatus = true;
			prlNotify->Status = false;
		}
	}


	if((UINT)m_CE_Pbis.imID == wParam)
	{
		m_CE_Pvom.GetWindowText(m_Pvom);
		olBis = DateStringToDate(prlNotify->Text); 
		if(!m_Pvom.IsEmpty() && !prlNotify->Text.IsEmpty())
		{
			olVom = DateStringToDate(m_Pvom);
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(olVom > olBis)
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = false;
					m_CE_Pvom.SetStatus(false);
				}
				else
				{
					m_CE_Pvom.SetStatus(true);
					prlNotify->UserStatus = true;
					prlNotify->Status = true;
				}
			}
		}
		if((olBis != TIMENULL) && !CheckUtcPast(olBis))
		{
			prlNotify->UserStatus = true;
			prlNotify->Status = false;
		}
	}

	if(((UINT)m_CE_Pbis.imID == wParam) || ((UINT)m_CE_Pvom.imID == wParam))
	{
		m_CE_Pvom.GetWindowText(m_Pvom);
		m_CE_Pbis.GetWindowText(m_Pbis);
		olVom = DateStringToDate(m_Pvom); 
		olBis = DateStringToDate(m_Pbis);
		CString olVomStr = CTimeToDBString(olVom,TIMENULL);
		CString olBisStr = CTimeToDBString(olBis,TIMENULL);

		olASeas  = "";
		olDSeas  = "";
		
		if(olVom != TIMENULL)
		{
			ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", olVomStr, "SEAS", olASeas );
		}
		if(olBis != TIMENULL)
		{
			ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", olBisStr, "SEAS", olDSeas );
		}

 		if(olASeas == olDSeas)
		{
			m_Seas = olASeas;
		}
		else
		{
			if(!olASeas.IsEmpty() && !olDSeas.IsEmpty())
				m_Seas = olASeas + CString(" / ") + olDSeas;
			else
			{
				if(!olASeas.IsEmpty())
					m_Seas = olASeas ;
				else
					m_Seas = olDSeas;
			}

		}
		m_CE_Seas.SetInitText(m_Seas);

	}
	

	/////////////////////////////////////////////
	if(prlNotify->Text.IsEmpty())
		return 0L;
	
	if(((UINT)m_CE_ATtyp.imID == wParam) || ((UINT)m_CE_DTtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
//		return 0L;
	}

/*	if(((UINT)m_CE_AHtyp.imID == wParam) || ((UINT)m_CE_DHtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("HTY", "HTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}
*/
	if(((UINT)m_CE_AStyp.imID == wParam) || ((UINT)m_CE_DStyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("STY", "STYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
//		return 0L;
	}

	if( (UINT)m_CE_APsta.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		//changed while fixing for the PRF 8494 by sisl
		if(bgRelatedGatePosition)
		{
			if(prlNotify->Status && prlNotify->IsChanged)
			{

				if( !CString(prmAFlight->Ttyp).IsEmpty() )
				{
					if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmAFlight->Ttyp))
					{
						CString olAirBridge;
						ogBCD.GetField("PST", "URNO", olTmp, "BRGS", olAirBridge);

						if(!olAirBridge.IsEmpty())
						{

							CStringArray olConnectedGates;
							ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
							
							CString olStrGat1="",olStrGat2="";

							if(olConnectedGates.GetSize() > 0)
							{
								if(olConnectedGates.GetSize() >= 2)
								{
									olStrGat1 = olConnectedGates.GetAt(0);	
									olStrGat2 = olConnectedGates.GetAt(1);	
								}
								else
								{
									olStrGat1 = olConnectedGates.GetAt(0);
									olStrGat2 = "";
								}
							}

							m_CE_AGta1.SetWindowText(olStrGat1);
							m_CE_AGta2.SetWindowText(olStrGat2);
						}
						else
						{
							m_CE_AGta1.SetWindowText("");
							m_CE_AGta2.SetWindowText("");
						}
					}
				}
			}
		}
		
		m_CE_APabs.GetWindowText(olTmp);
		m_CE_APaes.GetWindowText(olTmp1);

		if(prlNotify->Status && (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty()))
		{
			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmAFlight->Ming), olDura);

			//is an open leg
			if (prmDFlight->Urno == 0 && prmAFlight->Onbl != TIMENULL)
			{
//				return 0L;
			}
			else
			{

			if((imModus == DLG_NEW) || (imModus == DLG_COPY))
			{
				CTime olARefDat = DateStringToDate(m_Pvom);

				CString olStrStoa;
				m_CE_AStoa.GetWindowText(olStrStoa);
				CTime olStoa = HourStringToDate( olStrStoa, olARefDat);

				m_CE_APabs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));		
				m_CE_APaes.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));		
//				m_CE_APabs.SetWindowText(DateToHourDivString(olStoa, olStoa));		
//				m_CE_APaes.SetWindowText(DateToHourDivString(olStoa + olDura, olStoa));		

				m_CE_DPstd.GetWindowText(m_DPstd);
				m_CE_DPdbs.GetWindowText(m_DPdbs);
				m_CE_DPdes.GetWindowText(m_DPdes);
				if (m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
				{
					CString olStrStod;
					m_CE_DStod.GetWindowText(olStrStod);
					if (!olStrStod.IsEmpty())
					{
						CTime olStod = HourStringToDate( olStrStod, olARefDat);

						CTimeSpan olDuraDep;
						GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDuraDep);

						m_DPdes = DateToHourDivString(olTimeRefD, olTimeRefD);
						m_DPdbs = DateToHourDivString(olTimeRefD - olDuraDep, olTimeRefD);
//						m_DPdes = DateToHourDivString(olStod, olStod);
//						m_DPdbs = DateToHourDivString(olStod - olDuraDep, olStod);

						m_CE_DPstd.SetWindowText(prlNotify->Text);
						m_CE_DPdbs.SetWindowText(m_DPdbs);
						m_CE_DPdes.SetWindowText(m_DPdes);


//////////////////////////////////////

						if(prmDFlight != NULL)
						{
							if(bgRelatedGatePosition)
							{
								if( !CString(prmDFlight->Ttyp).IsEmpty() )
								{
									if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmDFlight->Ttyp))
									{
										CString olAirBridge;
										ogBCD.GetField("PST", "URNO", prlNotify->Text, "BRGS", olAirBridge);

										if(!olAirBridge.IsEmpty())
										{

											CStringArray olConnectedGates;
											ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
											ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
											
											CString olStrGat1="",olStrGat2="";

											if(olConnectedGates.GetSize() > 0)
											{
												if(olConnectedGates.GetSize() >= 2)
												{
													olStrGat1 = olConnectedGates.GetAt(0);	
													olStrGat2 = olConnectedGates.GetAt(1);	
												}
												else
												{
													olStrGat1 = olConnectedGates.GetAt(0);
													olStrGat2 = "";
												}
											}

											m_CE_DGtd1.SetWindowText(olStrGat1);
											m_CE_DGtd2.SetWindowText(olStrGat2);
										}
										else
										{
											m_CE_DGtd1.SetWindowText("");
											m_CE_DGtd2.SetWindowText("");
										}
									}
								}
							}
						}
					


//////////////////////////////////////
					}
				}


			}
			else
			{
				m_CE_APabs.SetWindowText(DateToHourDivString(olTimeRefA, olTimeRefA));
				m_CE_APaes.SetWindowText(DateToHourDivString(olTimeRefA + olDura, olTimeRefA));		
				m_CE_APabs.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));
//				m_CE_APaes.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));		
//
				if (prmDFlight && prmDFlight->Urno > 0 && prmDFlight->Tifd != TIMENULL)
				{
					m_CE_DPstd.GetWindowText(m_DPstd);
					m_CE_DPdbs.GetWindowText(m_DPdbs);
					m_CE_DPdes.GetWindowText(m_DPdes);
					if (m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
					{
						CTimeSpan olDuraDep;
						GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDuraDep);

						m_DPdes = DateToHourDivString(olTimeRefD, olTimeRefD);
						m_DPdbs = DateToHourDivString(olTimeRefD - olDuraDep, olTimeRefD);
//						m_DPdes = DateToHourDivString(prmDFlight->Tifd, omDRefDat);
//						m_DPdbs = DateToHourDivString(prmDFlight->Tifd - olDuraDep, omDRefDat);

						m_CE_DPdbs.SetWindowText(m_DPdbs);
						m_CE_DPdes.SetWindowText(m_DPdes);
					}

//////////////////////////////////////
				if (m_DPstd.IsEmpty())
				{
						m_CE_DPstd.SetWindowText(prlNotify->Text);
						if(prmDFlight != NULL)
						{
							if(bgRelatedGatePosition)
							{

								if( !CString(prmDFlight->Ttyp).IsEmpty() )
								{
									if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmDFlight->Ttyp))
									{
										CString olAirBridge;
										ogBCD.GetField("PST", "PNAM", prlNotify->Text, "BRGS", olAirBridge);

										if(!olAirBridge.IsEmpty())
										{

											CStringArray olConnectedGates;
											ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
											ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
											
											CString olStrGat1="",olStrGat2="";

											if(olConnectedGates.GetSize() > 0)
											{
												if(olConnectedGates.GetSize() >= 2)
												{
													olStrGat1 = olConnectedGates.GetAt(0);	
													olStrGat2 = olConnectedGates.GetAt(1);	
												}
												else
												{
													olStrGat1 = olConnectedGates.GetAt(0);
													olStrGat2 = "";
												}
											}

											m_CE_DGtd1.SetWindowText(olStrGat1);
											m_CE_DGtd2.SetWindowText(olStrGat2);
										}
										else
										{
											m_CE_DGtd1.SetWindowText("");
											m_CE_DGtd2.SetWindowText("");
										}
									}

								}
							}
						}
					


//////////////////////////////////////
					
					}
				}
			}
			}

		}

//		return 0L;
	}


	if((UINT)m_CE_DPstd.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		//Changed for the PRF 8494 
		if(bgRelatedGatePosition)
		{
			if(prlNotify->Status && prlNotify->IsChanged)
			{
				if( !CString(prmDFlight->Ttyp).IsEmpty() )
				{
					if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmDFlight->Ttyp))
					{
						CString olAirBridge;
						ogBCD.GetField("PST", "URNO", olTmp, "BRGS", olAirBridge);

						if(!olAirBridge.IsEmpty())
						{

							CStringArray olConnectedGates;
							ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
							
							CString olStrGat1 = "",olStrGat2 = "";

							if(olConnectedGates.GetSize() > 0)
							{
								if(olConnectedGates.GetSize() >= 2)
								{
									olStrGat1 = olConnectedGates.GetAt(0);	
									olStrGat2 = olConnectedGates.GetAt(1);	
								}
								else
								{
									olStrGat1 = olConnectedGates.GetAt(0);
									olStrGat2 = "";
								}
							}
								
							m_CE_DGtd1.SetWindowText(olStrGat1);
							m_CE_DGtd2.SetWindowText(olStrGat2);	
						}
						else
						{
							m_CE_DGtd1.SetWindowText("");
							m_CE_DGtd2.SetWindowText("");	
						}
					}
				}
			}
		}
		

		m_CE_DPdbs.GetWindowText(olTmp);
		m_CE_DPdes.GetWindowText(olTmp1);

		if(prlNotify->Status && (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty()))
		{
/*
			m_CE_DPdes.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));
	
			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);
			m_CE_DPdbs.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));
*/
			m_CE_DPdes.SetWindowText(DateToHourDivString(olTimeRefD, olTimeRefD));
	
			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);
			m_CE_DPdbs.SetWindowText(DateToHourDivString(olTimeRefD - olDura, olTimeRefD));
		}
	}

	if (blDatPosDura || blDatGatDura)
	{
		CString olRecFieldsFC("");

		if((UINT)m_CE_DPstd.imID == wParam)
		{
			if (blDatPosDura)
				olRecFieldsFC = "PSTD,";
		}

		if((UINT)m_CE_APsta.imID == wParam)
		{
			if (blDatPosDura)
				olRecFieldsFC = "PSTA,";
		}

		if((UINT)m_CE_DGtd1.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTD1,";
		}

		if((UINT)m_CE_DGtd2.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTD2,";
		}

		if((UINT)m_CE_AGta1.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTA1,";
		}

		if((UINT)m_CE_AGta2.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTA2,";
		}

		if((UINT)m_CE_AAlc3.imID == wParam || (UINT)m_CE_ATtyp.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTA1,GTA2,";
			if (blDatPosDura)
				olRecFieldsFC += "PSTA,";
		}

		if((UINT)m_CE_DAlc3.imID == wParam || (UINT)m_CE_DTtyp.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTD1,GTD2,";
			if (blDatPosDura)
				olRecFieldsFC += "PSTD,";
		}

		if((UINT)m_CE_Act5.imID == wParam || (UINT)m_CE_Act3.imID == wParam || (UINT)m_CE_Regn.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTA1,GTA2,GTD1,GTD2,";
			if (blDatPosDura)
				olRecFieldsFC += "PSTA,PSTD,";
		}

		if((UINT)m_CE_AStoa.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTA1,GTA2,PSTA";
		}

		if((UINT)m_CE_DStod.imID == wParam)
		{
			if (blDatGatDura)
				olRecFieldsFC = "GTD1,GTD2,PSTD";
		}

		RecalculateFC (olRecFieldsFC);

		return 0L;
	}
	

	CCS_CATCH_ALL	

	return 0L;
}


void CSeasonDlg::RecalculateFC (CString& opRecFields)
{
	if (opRecFields.IsEmpty())
		return;

	CString olTmpA;
	CString olTmpD;
	m_CE_AStoa.GetWindowText(olTmpA);
	m_CE_DStod.GetWindowText(olTmpD);
	CTime olTimeRefA = HourStringToDate( olTmpA, prmAFlight->Stoa);
	CTime olTimeRefD = HourStringToDate( olTmpD, prmDFlight->Stod);

	CStringArray olReqFields;
	CStringArray olForecastDataArr;
	CStringArray olForecastDataDep;

	olForecastDataArr.Add("A");
	olForecastDataArr.Add(CString(omAAlc2));
	olForecastDataArr.Add(CString(omAAlc3));

	olForecastDataDep.Add("D");
	olForecastDataDep.Add(CString(omDAlc2));
	olForecastDataDep.Add(CString(omDAlc3));

	CString olVal("");
	m_CE_Act3.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	olForecastDataDep.Add(CString(olVal));

	m_CE_Act5.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	olForecastDataDep.Add(CString(olVal));

	m_CE_ATtyp.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	m_CE_DTtyp.GetWindowText(olVal);
	olForecastDataDep.Add(CString(olVal));

	CMapStringToString olForcastMapArr;
	CMapStringToString olForcastMapDep;
	CreateForcastMap (olForcastMapArr, olForecastDataArr);
	CreateForcastMap (olForcastMapDep, olForecastDataDep);

	if (opRecFields.Find("PSTA") != -1 && prmAFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_APsta.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAA4");
			GetDefAllocFC(CString("PST"), olPsta, olForcastMapArr, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 30;
					olDuration = "30";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				m_CE_ADura.GetWindowText(olVal);
				//if (olVal != olDuration)
				{
					m_CE_ADura.SetWindowText(olDuration);

					if (prmDFlight->Urno != 0 && prmAFlight->Onbl != TIMENULL)
					{
						//dont change because foghdl calulates
					}
					else
					{
						CString olBS("");
						m_CE_APabs.GetWindowText(olBS);
						if (!olBS.IsEmpty())
						{
							CTime olBeg = HourStringToDate( olBS, olTimeRefA);
							CString olES("");
							m_CE_APaes.GetWindowText(olES);
							CTime olEnd = HourStringToDate( olES, olTimeRefA);
							CTime olEndNew = olBeg + olDura;
							if (olEnd != olEndNew)
								m_CE_APaes.SetWindowText(DateToHourDivString(olEndNew, olTimeRefA));
						}
					}
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
	olReqFields.RemoveAll();
	if (opRecFields.Find("PSTD") != -1 && prmDFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_DPstd.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAA5");
			GetDefAllocFC(CString("PST"), olPsta, olForcastMapDep, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 30;
					olDuration = "30";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				m_CE_DDura.GetWindowText(olVal);
				//if (olVal != olDuration)
				{
					m_CE_DDura.SetWindowText(olDuration);

					if (prmAFlight->Urno != 0 && prmAFlight->Onbl != TIMENULL)
					{
						//dont change because foghdl calulates
					}
					else
					{
						CString olES("");
						m_CE_DPdes.GetWindowText(olES);
						if (!olES.IsEmpty())
						{
							CTime olEnd = HourStringToDate( olES, olTimeRefD);
							CString olBS("");
							m_CE_DPdbs.GetWindowText(olBS);
							CTime olBeg = HourStringToDate( olBS, olTimeRefD);
							CTime olBegNew = olEnd - olDura;
							if (olBeg != olBegNew)
								m_CE_DPdbs.SetWindowText(DateToHourDivString(olBegNew, olTimeRefD));
						}
					}
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
	olReqFields.RemoveAll();
	if (opRecFields.Find("GTA1") != -1 && prmAFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_AGta1.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAA4");
			GetDefAllocFC(CString("GAT"), olPsta, olForcastMapArr, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 15;
					olDuration = "15";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				CString olBS("");
				m_CE_AGa1b.GetWindowText(olBS);
				if (!olBS.IsEmpty())
				{
					CTime olBeg = HourStringToDate( olBS, olTimeRefA);
					CString olES("");
					m_CE_AGa1e.GetWindowText(olES);
					CTime olEnd = HourStringToDate( olES, olTimeRefA);
					CTime olEndNew = olBeg + olDura;
					if (olEnd != olEndNew)
						m_CE_AGa1e.SetWindowText(DateToHourDivString(olEndNew, olTimeRefA));
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
	olReqFields.RemoveAll();
	if (opRecFields.Find("GTA2") != -1 && prmAFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_AGta2.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAA5");
			GetDefAllocFC(CString("GAT"), olPsta, olForcastMapArr, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 30;
					olDuration = "30";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				CString olBS("");
				m_CE_AGa2b.GetWindowText(olBS);
				if (!olBS.IsEmpty())
				{
					CTime olBeg = HourStringToDate( olBS, olTimeRefA);
					CString olES("");
					m_CE_AGa2e.GetWindowText(olES);
					CTime olEnd = HourStringToDate( olES, olTimeRefA);
					CTime olEndNew = olBeg + olDura;
					if (olEnd != olEndNew)
						m_CE_AGa2e.SetWindowText(DateToHourDivString(olEndNew, olTimeRefA));
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
	olReqFields.RemoveAll();
	if (opRecFields.Find("GTD2") != -1 && prmDFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_DGtd2.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAD5");
			GetDefAllocFC(CString("GAT"), olPsta, olForcastMapDep, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 30;
					olDuration = "30";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				CString olES("");
				m_CE_DGd2e.GetWindowText(olES);
				if (!olES.IsEmpty())
				{
					CTime olEnd = HourStringToDate( olES, olTimeRefD);
					CString olBS("");
					m_CE_DGd2b.GetWindowText(olBS);
					CTime olBeg = HourStringToDate( olBS, olTimeRefD);
					CTime olBegNew = olEnd - olDura;
					if (olBeg != olBegNew)
						m_CE_DGd2b.SetWindowText(DateToHourDivString(olBegNew, olTimeRefD));
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
	olReqFields.RemoveAll();
	if (opRecFields.Find("GTD1") != -1 && prmDFlight->Urno > 0)
	{
		bool blPosCalc = true;
		CString olPsta("");
		m_CE_DGtd1.GetWindowText(olPsta);
		if (olPsta.IsEmpty())
			blPosCalc = false;

		if (blPosCalc)
		{
			olReqFields.Add("BAD4");
			GetDefAllocFC(CString("GAT"), olPsta, olForcastMapDep, olReqFields);

			CTimeSpan olDura (0,0,0,0);
			CString olDuration("");
			for (int i=0; i<olReqFields.GetSize(); i++)
			{
				olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
				int ilDura = atoi(olDuration);
				if (ilDura == 0)
				{
					ilDura = 30;
					olDuration = "30";
				}

				olDura = CTimeSpan(0,0,ilDura,0);

				CString olES("");
				m_CE_DGd1e.GetWindowText(olES);
				if (!olES.IsEmpty())
				{
					CTime olEnd = HourStringToDate( olES, olTimeRefD);
					CString olBS("");
					m_CE_DGd1b.GetWindowText(olBS);
					CTime olBeg = HourStringToDate( olBS, olTimeRefD);
					CTime olBegNew = olEnd - olDura;
					if (olBeg != olBegNew)
						m_CE_DGd1b.SetWindowText(DateToHourDivString(olBegNew, olTimeRefD));
				}
			}
		}
	}
	//-----------------------------------------------------------------------------------
}


LONG CSeasonDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	if((UINT)m_CE_Act3.imID == wParam || (UINT)m_CE_Act5.imID == wParam)
		bmActSelect = false;

// begin : reject if opening times
	CString olTmp;

//	arr-gate1
	if((UINT)m_CE_AGta1.imID == wParam)
	{
		m_CE_AGta1.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta1;

		if (prmAFlightSave->Ga1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta1.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-gate2
	if((UINT)m_CE_AGta2.imID == wParam)
	{
		m_CE_AGta2.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta2;

		if (prmAFlightSave->Ga2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta2.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-belt1
	if((UINT)m_CE_ABlt1.imID == wParam)
	{
		m_CE_ABlt1.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt1;

		if (prmAFlightSave->B1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt1.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-belt2
	if((UINT)m_CE_ABlt2.imID == wParam)
	{
		m_CE_ABlt2.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt2;

		if (prmAFlightSave->B2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt2.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-position
	if((UINT)m_CE_APsta.imID == wParam)
	{
		m_CE_APsta.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Psta;

		if (prmAFlightSave->Onbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_APsta.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-gate1
	if((UINT)m_CE_DGtd1.imID == wParam)
	{
		m_CE_DGtd1.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd1;

		if (prmDFlightSave->Gd1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd1.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-gate2
	if((UINT)m_CE_DGtd2.imID == wParam)
	{
		m_CE_DGtd2.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd2;

		if (prmDFlightSave->Gd2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd2.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-position
	if((UINT)m_CE_DPstd.imID == wParam)
	{
		m_CE_DPstd.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Pstd;

		if (prmDFlightSave->Ofbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DPstd.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro1.imID == wParam)
	{
		m_CE_DWro1.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro1;

		if (prmDFlightSave->W1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro1.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro2.imID == wParam)
	{
		m_CE_DWro2.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro2;

		if (prmDFlightSave->W2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro2.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
// end : reject if opening times

//	arr prfl
	if((UINT)m_CE_ACht3.imID == wParam)
	{
		m_CE_ACht3.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Prfl;

		if (olTmp == "U" || olTmp == " ")
		{
			//nothimg yet
		}
		else
		{
			m_CE_ACht3.SetInitText(olAkt);
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_CHECK_PRFL), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
		}
	}

//	dep prfl
	if((UINT)m_CE_DCht3.imID == wParam)
	{
		m_CE_DCht3.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Prfl;

		if (olTmp == "U" || olTmp == " ")
		{
			//nothimg yet
		}
		else
		{
			m_CE_DCht3.SetInitText(olAkt);
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_CHECK_PRFL), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
		}
	}


	if(!bmChanged)
	{
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	bmChanged = true;

	CCS_CATCH_ALL

	return 0L;
}

LONG CSeasonDlg::OnTableIPEdit( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

//	reject a new checkin-counter if counter is already open
	if(prlNotify && prlNotify->Column == 0 && (prlNotify->SourceTable == pomDCinsTable))
	{
		return CcaHasNoOpentime (prlNotify->Line);
	}

//rkr05042001
	// disable the cic if open (now you can�t hit with TAB)
	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable))
	{
		for (int i = 0; i < omDCinsSave.GetSize(); i++)
		{
			CCADATA *prlCca = &omDCinsSave[i];

			if(prlCca && prlCca->Ckba != TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, false);
			}
			if(prlCca && prlCca->Ckba == TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, true);
			}
		}
	}
//rkr05042001

	return 0L;
}

LONG CSeasonDlg::CcaHasNoOpentime (int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omDCinsSave.GetSize())
	{
		CCADATA *prlCca = &omDCinsSave[ipLineNo];

		if(prlCca && prlCca->Ckba != TIMENULL)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			m_CB_Ok.SetFocus();
			return -1L;
		}
	}

	return 0L;
}

LONG CSeasonDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	CString olTmp;

/*
	if(((prlNotify->SourceTable == pomAViaTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDViaTable) && (prlNotify->Column == 0)))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp );
		return 0L;
	}
	if(((prlNotify->SourceTable == pomAViaTable) && (prlNotify->Column > 1)) ||
	   ((prlNotify->SourceTable == pomDViaTable) && (prlNotify->Column > 1)))
	{

		CTime olStoa;
		CTime olStod;
		CString olTmp, olTmp2;
		pomAViaTable->GetTextFieldValue(prlNotify->Line, 2, olTmp);
		pomAViaTable->GetTextFieldValue(prlNotify->Line, 3, olTmp2);
		olStoa = HourStringToDate(olTmp, omARefDat); 
		olStod = HourStringToDate(olTmp2, omARefDat); 

		if((olStoa != TIMENULL) && (olStod != TIMENULL))
		{
			if(olStoa >= olStod)
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}
		}

	}
*/

	if(((prlNotify->SourceTable == pomAJfnoTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDJfnoTable) && (prlNotify->Column == 0)))
	{
		if(prlNotify->Text.GetLength() == 3)
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );
		}
		prlNotify->UserStatus = true;
		return 0L;
	}

	if((prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "TERM", olTmp ))
		{
			pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(prlNotify->Line, 2, olTmp);
				ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "CATR", olTmp );
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
			}
			else
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
		}
	}

	CCS_CATCH_ALL

	return 0L;
}

void CSeasonDlg::OnAdaly() 
{

	CCS_TRY

	CString olVomStr;
	CString olBisStr;

	m_CE_Pvom.GetWindowText(olVomStr);
	m_CE_Pbis.GetWindowText(olBisStr);

	CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
	CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 

	m_ADays = "1234567";
	if((olVom != TIMENULL) && (olBis != TIMENULL))
	{
		DaysinPeriod(olVom,  olBis,  m_ADays);
	}

	m_CE_ADays.SetInitText(m_ADays);

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDdaly() 
{

	CCS_TRY

	CString olVomStr;
	CString olBisStr;

	m_CE_Pvom.GetWindowText(olVomStr);
	m_CE_Pbis.GetWindowText(olBisStr);

	CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
	CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 

	m_DDays = "1234567";
	if((olVom != TIMENULL) && (olBis != TIMENULL))
	{
		DaysinPeriod(olVom,  olBis,  m_DDays);
	}

	m_CE_DDays.SetInitText(m_DDays);


	CCS_CATCH_ALL

}



/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// CHECKALL

bool CSeasonDlg::CheckAll(CString &opGMess, CString &opAMess, CString &opDMess) 
{

	CCS_TRY

	m_CE_Regn.GetWindowText(m_Regn); 
	m_Regn.Remove('\n');
	m_Regn.TrimLeft();
	m_Regn.TrimRight();
	m_CE_Regn.SetWindowText(m_Regn);

	if(m_ACsgn.IsEmpty() && m_AAlc3.IsEmpty() && m_AFltn.IsEmpty() && m_AFlns.IsEmpty() && m_AStoa.IsEmpty() && m_AOrg3.IsEmpty())
		bmAnkunft = false;
	else
		bmAnkunft = true;

	if(m_DCsgn.IsEmpty() && m_DAlc3.IsEmpty() && m_DFltn.IsEmpty() && m_DFlns.IsEmpty() && m_DStod.IsEmpty() && m_DDes3.IsEmpty())
		bmAbflug = false;
	else
		bmAbflug = true;


	if(!bmAnkunft && !bmAbflug)
	{
		return false;
	}

	bool blRet = CheckGatPos(bmAnkunft, bmAbflug, opGMess, opAMess, opDMess);

	if(imModus == DLG_CHANGE_DIADATA)
	{
		return blRet;
	}


	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		if(!m_CE_Pvom.GetStatus() || !m_CE_Pbis.GetStatus()) 
			opGMess += GetString(IDS_STRING396) + CString("\n");//"Periode\n"; 

		if(!m_CE_Freq.GetStatus())
			opGMess += GetString(IDS_STRING397) + CString("\n");//"Wochenfrequenz\n"; 

		if(!m_CE_Pvom.GetStatus() && bmAnkunft) 
			opGMess += GetString(IDS_STRING398) + CString("\n");//"Ankunftsdatum \n"; 
		if(!m_CE_Pbis.GetStatus() && bmAbflug) 
			opGMess += GetString(IDS_STRING399) + CString("\n");//"Abflugsdatum! \n"; 

	// Check season for selected flight
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		CString olSeason;
		if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olVom, TIMENULL), "SEAS", olSeason ))
		{
			opGMess += GetString(IDS_STRING2173) + CString("\n");//"Wochenfrequenz\n"; 
		}
		if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olBis, TIMENULL), "SEAS", olSeason ))
		{
			opGMess += GetString(IDS_STRING2174) + CString("\n");//"Wochenfrequenz\n"; 
		}

		CString olAStoa ;
		CString olAStod ;
		CString olDStoa ;
		CString olDStod ;

		m_CE_AStoa.GetWindowText(olAStoa);
		m_CE_AStod.GetWindowText(olAStod);
		m_CE_DStoa.GetWindowText(olDStoa);
		m_CE_DStod.GetWindowText(olDStod);

		CTime olCurr = CTime::GetCurrentTime();

		if(!olAStoa.IsEmpty() && !olAStod.IsEmpty() && olVom != TIMENULL)
		{
			CTime olStoa = DateHourStringToDate(olVomStr, olAStoa); 
			CTime olStod = DateHourStringToDate(olVomStr, olAStod); 

			if(olStoa < olStod)
					opAMess += GetString(IDS_STRING2532) + CString("\n");//Planning Time for Arrival at Home before Planning Time of Departure at Origin

		}


		if(!olDStoa.IsEmpty() && !olDStod.IsEmpty() && olVom != TIMENULL)
		{
			CTime olStoa = DateHourStringToDate(olVomStr, olDStoa); 
			CTime olStod = DateHourStringToDate(olVomStr, olDStod); 

			if(olStoa < olStod)
					opDMess += GetString(IDS_STRING2533) + CString("\n");//Planning Time for Arrival at Home before Planning Time of Departure at Origin

		}


		if(!olAStoa.IsEmpty() && !olDStod.IsEmpty() && olVom != TIMENULL)
		{
			CTime olStoa = DateHourStringToDate(olVomStr, olAStoa); 
			CTime olStod = DateHourStringToDate(olVomStr, olDStod); 

			if(olStoa > olStod)
				opGMess += GetString(IDS_STRING2534) + CString("\n");//Planning Time for Arrival at Home after Planning Time of Departure at Home

		}



		m_CE_Pvom.GetWindowText(m_Pvom);
		m_CE_AAlc3.GetWindowText(m_AAlc3);
		m_CE_DAlc3.GetWindowText(m_DAlc3);

		CTime olTmp = DateStringToDate(m_Pvom); 

		CString olStrStod;
		m_CE_DStod.GetWindowText(olStrStod);
		if (olStrStod.IsEmpty())
			olStrStod = CString("23:59");

		CTime olStodTime = HourStringToDate( olStrStod, olTmp);
	
		
		CString olStod = CTimeToDBString( olStodTime, olStodTime);

		if(!olDStod.IsEmpty() && !m_DAlc3.IsEmpty())
		{
			if(!ogBCD.GetField("ALT", "ALC2", "ALC3", m_DAlc3, omDAlc2, omDAlc3, olStod ))
					opDMess += GetString(IDS_STRING438) + CString("\n");
		}


		CString olStrStoa;
		m_CE_AStoa.GetWindowText(olStrStoa);
		if (olStrStoa.IsEmpty())
			olStrStoa = CString("23:59");

		CTime olStoaTime = HourStringToDate( olStrStoa, olTmp);
	
		
		CString olStoa = CTimeToDBString( olStoaTime, olStoaTime);

		if(!olAStoa.IsEmpty() && !m_AAlc3.IsEmpty())
		{
			if(!ogBCD.GetField("ALT", "ALC2", "ALC3", m_AAlc3, omAAlc2, omAAlc3, olStoa ))
					opAMess += GetString(IDS_STRING476) + CString("\n");
		}

	}
	else
	{
		if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
			opGMess += GetString(IDS_STRING341) + CString("\n");//"LFZ-Kennzeichen"; 
	}


	bool blIsIn = true;
	CString olText;
	CString olTmp;

	if (m_Act5.IsEmpty() && m_Act3.IsEmpty())
		blIsIn = false;

	CString olWhere;
	if (m_Act3.IsEmpty())
		m_Act3 = "";
	if (m_Act5.IsEmpty())
		m_Act5 = " ";

	CCSPtrArray<RecordSet> prlRecords;
	int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",m_Act3,m_Act5, &prlRecords);
	prlRecords.DeleteAll();

	if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus() || !blIsIn || ilCount == 0)
		opGMess += GetString(IDS_STRING342);//"A/C Typ\n";  



/*		
	bool blIsIn = true;
	CString olText;
	CString olTmp;
	if(m_Act3.IsEmpty() && m_Act5.IsEmpty())
	{
		opGMess += GetString(IDS_STRING342);//"A/C Typ\n"; 
	}
	else
	{
//		if(!m_CE_Act3.GetStatus() && !m_CE_Act5.GetStatus())
		if (!m_Act5.IsEmpty())
			blIsIn = ogBCD.GetField("ACT", "ACT5", m_Act5, "URNO", olTmp );
		if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus() || !blIsIn)
			opGMess += GetString(IDS_STRING342);//"A/C Typ\n";  
	}
*/

	if(!m_CE_Ming.GetStatus())
		opGMess += GetString(IDS_STRING343) + CString("\n");//"Min.G/T \n"; 


	if (bmAnkunft && bmAbflug)
	{
/*
		if((prmAFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL))
		{
			if(prmAFlight->Stoa > prmDFlight->Stod)
				opGMess += GetString(IDS_STRING1982) + CString("\n");//"STD is before STA!\n";
		}
*/
	}

	CString olBuffer;
	if(bmAnkunft)
	{

		CString olAlc;
		CString olFltn;
		CString olFlns;
		CString olCsgn;
		m_CE_AAlc3.GetWindowText(olAlc);
		m_CE_AFltn.GetWindowText(olFltn);
		m_CE_AFlns.GetWindowText(olFlns);
		m_CE_ACsgn.GetWindowText(olCsgn);
		CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 

		if(strcmp(pcgHome, "ATH") == 0)
		{
			if (olFlno.IsEmpty() )
			{
				//"Flightnumber must be filled!\n"
 				opAMess += GetString(IDS_STRING2903) + CString("\n");
			}
		}
		else
		{
			if (olFlno.IsEmpty() && olCsgn.IsEmpty())
			{
				//"Flightnumber or Callsign must be filled!\n"
 				opAMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");
			}
		}

//		if (m_CE_AFlti.IsChanged() && 
//			(!m_CE_AFlti.GetStatus() || (m_AFlti != "I" && m_AFlti != "D" && !m_AFlti.IsEmpty())))
		if ( m_CE_AFlti.IsChanged() && !m_CE_AFlti.GetStatus() )
			opAMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";
		
		blIsIn = true;
		m_CE_AAlc3.GetWindowText(olText);
		if (!olText.IsEmpty())
		{
			blIsIn = ogBCD.GetField("ALT", "ALC2", olText, "URNO", olTmp );
			if (!blIsIn)
				blIsIn = ogBCD.GetField("ALT", "ALC3", olText, "URNO", olTmp );
		}

//		if(!m_CE_AAlc3.GetStatus() || !m_CE_AFltn.GetStatus() || !m_CE_AFlns.GetStatus() || !blIsIn)
		if(!blIsIn)
			opAMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 


		
		if(imModus != DLG_CHANGE)
		{
			CString olVomStr;
			CString olBisStr;
			m_CE_Pvom.GetWindowText(olVomStr);
			m_CE_Pbis.GetWindowText(olBisStr);
			CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
			CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
			CString olDays = m_ADays;
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(!DaysinPeriod(olVom,  olBis,  olDays))
				{
					opAMess += GetString(IDS_STRING401) + CString("\n");//"Wochentag\n"; 
				}	
			}
		}	

		
		// weitere Flugnummern


		bool bl0;
		bool bl1;
		bool bl2;
		CString  olAlc3;

		int ilLines = pomAJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty()/* && !olFlns.IsEmpty()*/)
			{
				bl0 = pomAJfnoTable->GetCellStatus(i, 0); 
				bl1 = pomAJfnoTable->GetCellStatus(i, 1); 
				bl2 = pomAJfnoTable->GetCellStatus(i, 2); 
				if((!bl0) || (!bl1) || (!bl2))
				{
					olBuffer.Format(GetString(IDS_STRING348), i+1);//"Weitere Flugnummer in Zeile %d\n"
					opAMess += olBuffer; 
				}
			}
		}


//		if(!m_CE_AOrg3.GetStatus() || !m_CE_AOrg4.GetStatus())
//		if(!m_CE_AOrg4.GetStatus())
//			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 

		blIsIn = true;
		CString olText4;
		m_CE_AOrg4.GetWindowText(olText4);
		if (!olText4.IsEmpty())
			blIsIn = ogBCD.GetField("APT", "APC4", olText4, "URNO", olTmp );

		if (!m_CE_AOrg4.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 
/*
		blIsIn = true;
		m_CE_AOrg3.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("APT", "APC3", olText, "URNO", olTmp );
		if(!m_CE_AOrg3.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 
*/
		if(!m_CE_AStoa.GetStatus())
			opAMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";
		
		if(!m_CE_ACsta.GetStatus())
			opAMess += GetString(IDS_STRING2896) + CString("\n");//"Coordinated scheduled time of Arrival.!\n";


		if(!m_CE_AStod.GetStatus())
			opAMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
/*		if((prmAFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL) && bmAnkunft && bmAbflug)
		{
			if(prmAFlight->Stoa <= prmAFlight->Stod)
				opAMess += GetString(IDS_STRING351) + CString("\n");//"Planm��ige Ankunftszeit liegt vor Abflugszeit!\n";
		}
*/
		if(!m_CE_AEtai.GetStatus())
			opAMess += GetString(IDS_STRING352) + CString("\n");//"Erwartete Ankunftszeit!\n";

		if(!m_CE_ANfes.GetStatus())
			opAMess += GetString(IDS_STRING2868) + CString("\n");


		if (polRotationAViaDlg)
			opAMess += polRotationAViaDlg->GetStatus();


		blIsIn = true;
		m_CE_AStyp.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("STY", "STYP", olText, "URNO", olTmp );
		if(!m_CE_AStyp.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING2396) + CString("\n");//"Verkehrsart\n";


		blIsIn = true;
		m_CE_ATtyp.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("NAT", "TTYP", olText, "URNO", olTmp );

		if(!m_CE_ATtyp.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";

		if(!m_CE_AExt1.GetStatus())
			opAMess += GetString(IDS_STRING381) + CString("\n");//"Bezeichnung des Ausganges 1\n";

		if(!m_CE_AExt2.GetStatus())
			opAMess += GetString(IDS_STRING382) + CString("\n");//"Bezeichnung des Ausganges 2\n";

		// Check Status
		if(!m_CB_AStatus.GetCheck() && !m_CB_AStatus2.GetCheck() &&
		   !m_CB_AStatus3.GetCheck() && !m_CB_ACxx.GetCheck() && 
		   !m_CB_ANoop.GetCheck() && !IsCircular(m_AOrg3, m_ADes3))
			opAMess += GetString(IDS_STRING1900) + CString("\n");


		if(bgShowPayDetail && (imModus == DLG_NEW))
		{
			char* strAPCL= ogCedaMOPData.GetMOPInternalData("A");
			CString strAData(strAPCL);
			
			if(strAData.GetLength() <=7)
			{
				opAMess += "Payment/Resource Details" + CString("\n");
					
			}
		}


	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	if(bmAbflug)
	{
		CString olAlc;
		CString olFltn;
		CString olFlns;
		CString olCsgn;
		m_CE_DAlc3.GetWindowText(olAlc);
		m_CE_DFltn.GetWindowText(olFltn);
		m_CE_DFlns.GetWindowText(olFlns);
		m_CE_DCsgn.GetWindowText(olCsgn);
		CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 


		if(strcmp(pcgHome, "ATH") == 0)
		{
			if (olFlno.IsEmpty() )
			{
				//"Flightnumber must be filled!\n"
 				opDMess += GetString(IDS_STRING2903) + CString("\n");
			}
		}
		else
		{
			if (olFlno.IsEmpty() && olCsgn.IsEmpty())
			{
				//"Flightnumber or Callsign must be filled!\n"
 				opDMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");
			}
		}



		blIsIn = true;
		m_CE_DAlc3.GetWindowText(olText);
		if (!olText.IsEmpty())
		{
			blIsIn = ogBCD.GetField("ALT", "ALC2", olText, "URNO", olTmp );
			if (!blIsIn)
				blIsIn = ogBCD.GetField("ALT", "ALC3", olText, "URNO", olTmp );
		}

//		if(!m_CE_DAlc3.GetStatus() || !m_CE_DFltn.GetStatus() || !m_CE_DFlns.GetStatus() || !blIsIn)
		if(!blIsIn)
			opDMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 

//		if (m_CE_DFlti.IsChanged() && 
//			(!m_CE_DFlti.GetStatus() || (m_DFlti != "I" && m_DFlti !="D" && !m_DFlti.IsEmpty())))
		if ( m_CE_DFlti.IsChanged() && !m_CE_DFlti.GetStatus() )
 			opDMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		if(imModus != DLG_CHANGE)
		{
			CString olVomStr;
			CString olBisStr;
			m_CE_Pvom.GetWindowText(olVomStr);
			m_CE_Pbis.GetWindowText(olBisStr);
			CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
			CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
			CString olDays = m_DDays;
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(!DaysinPeriod(olVom,  olBis,  olDays))
				{
					opDMess += GetString(IDS_STRING401) + CString("\n");//"Wochentag\n"; 
				}	
			}
		}		
		

		// weitere Flugnummern

		CString  olAlc3;

		int ilLines = pomDJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty()/* && !olFlns.IsEmpty()*/)
			{
				if((!pomDJfnoTable->GetCellStatus(i, 0)) || (!pomDJfnoTable->GetCellStatus(i, 1)) || (!pomDJfnoTable->GetCellStatus(i, 2)))
				{
					olBuffer.Format(GetString(IDS_STRING348),i+1); ;//"Weitere Flugnummer in Zeile %d\n"
					opDMess += olBuffer; 
				}
			}
		}


//		if(!m_CE_DDes3.GetStatus() || !m_CE_DDes4.GetStatus())
//		if(!m_CE_DDes4.GetStatus())
//			opDMess += GetString(IDS_STRING1137) + CString("\n");//"Zielflughafencode!\n"; 

		blIsIn = true;
		CString olTextD4;
		m_CE_DDes4.GetWindowText(olTextD4);
		if (!olTextD4.IsEmpty())
			blIsIn = ogBCD.GetField("APT", "APC4", olTextD4, "URNO", olTmp );
		if (!m_CE_DDes4.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING1137) + CString("\n");//"Zielflughafencode!\n"; 
/*
		blIsIn = true;
		m_CE_DDes3.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("APT", "APC3", olText, "URNO", olTmp );
		if(!m_CE_DDes3.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING1137) + CString("\n");//"Zielflughafencode!\n"; 
*/
		if(!m_CE_DStod.GetStatus())
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_DCstd.GetStatus())
			opDMess += GetString(IDS_STRING2897) + CString("\n");//"Coordinated scheduled time of Departure.\n";

		if(!m_CE_DStoa.GetStatus())
			opDMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";

		if(!m_CE_DNfes.GetStatus())
			opDMess += GetString(IDS_STRING2869) + CString("\n");


		/*		
		if((prmDFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL) && bmAnkunft && bmAbflug)
		{
			if(prmDFlight->Stod >= prmDFlight->Stoa)
				opDMess += GetString(IDS_STRING402) + CString("\n");//"Planm��ige Abflugszeit liegt vor Ankunftszeit!\n";
		}
		*/

		if(!m_CE_DEtdi.GetStatus())
			opDMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";


		
		if (polRotationDViaDlg)
			opDMess += polRotationDViaDlg->GetStatus();


		blIsIn = true;
		m_CE_DStyp.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("STY", "STYP", olText, "URNO", olTmp );
		if(!m_CE_DStyp.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING2396) + CString("\n");//"Verkehrsart\n";


		blIsIn = true;
		m_CE_DTtyp.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("NAT", "TTYP", olText, "URNO", olTmp );
		if(!m_CE_DTtyp.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";


		// Check Status
		if(!m_CB_DStatus.GetCheck() &&  !m_CB_DStatus2.GetCheck() &&
		   !m_CB_DStatus3.GetCheck() && !m_CB_DCxx.GetCheck() && 
		   !m_CB_DNoop.GetCheck() && !IsCircular(m_DOrg3, m_DDes3))
			opDMess += GetString(IDS_STRING1900) + CString("\n");



		CTime olStoa;
		CTime olStod;
		CString olTmp;
		CString olTmp2;

		// Checkinschalter
		if(pomDCinsTable != NULL)
		{
			ilLines = pomDCinsTable->GetLinesCount();
			for( i = 0; i < ilLines; i++)
			{

				if (bgCnamAtr)
				{
					pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 4, olTmp2);
				}
				else
				{
					pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 3, olTmp2);
				}

				olStoa = HourStringToDate(olTmp, omDRefDat); 
				olStod = HourStringToDate(olTmp2, omDRefDat); 
					
				if(!pomDCinsTable->GetCellStatus(i, 0))
				{
					olBuffer.Format(GetString(IDS_STRING392),i); //"Bezeichnung Check-In Schalter in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 2))
				{
					olBuffer.Format(GetString(IDS_STRING393),i); //"Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 3))
				{
					olBuffer.Format(GetString(IDS_STRING394),i); //"Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if((olStoa != TIMENULL) && (olStod != TIMENULL))
				{
					if(olStoa > olStod)
					{
						// Allocation of Check-In Counter in line %d: Start time is after end time.\n
						olBuffer.Format(GetString(IDS_STRING395),i);
						opDMess += olBuffer; 
					}
				}
				if (olStoa == TIMENULL && olStod != TIMENULL)
				{
					// Allocation of Check-In Counter in line %d: End time without start time!\n
					olBuffer.Format(GetString(IDS_STRING2011), i);
					opDMess += olBuffer; 
				}

			}
		}

		if(bgShowPayDetail && (imModus == DLG_NEW))
		{
			char* strDPCL= ogCedaMOPData.GetMOPInternalData("D");
			CString strDData(strDPCL);
			if(strDData.GetLength() <=7)
			{
				opDMess += "Payment/Resource Details" + CString("\n");
			}

		}
	}

	CCS_CATCH_ALL

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;

}




bool CSeasonDlg::CheckGatPos(bool bpAnkunft, bool bpAbflug, CString &opGMess, CString &opAMess, CString &opDMess) 
{

	if(bpAnkunft)
	{
		CString olText;
		CString olTmp;
		bool blIsIn = true;
		m_CE_APsta.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("PST", "PNAM", olText, "URNO", olTmp );

		if(!m_CE_APsta.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		if(!m_CE_APabs.GetStatus())
			opAMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		if(!m_CE_APaes.GetStatus())
			opAMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmAFlight->Paes != TIMENULL) && (prmAFlight->Paes != TIMENULL))
		{
			if(prmAFlight->Pabs > prmAFlight->Paes)
				opAMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		blIsIn = true;
		m_CE_AGta1.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("GAT_PORT", "GNAM", olText, "URNO", olTmp );

		if(!m_CE_AGta1.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_AGa1b.GetStatus())
			opAMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_AGa1e.GetStatus())
			opAMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmAFlight->Ga1b != TIMENULL) && (prmAFlight->Ga1e != TIMENULL))
		{
			if(prmAFlight->Ga1b > prmAFlight->Ga1e)
				opAMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_AGta2.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("GAT_PORT", "GNAM", olText, "URNO", olTmp );

		if(!m_CE_AGta2.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_AGa2b.GetStatus())
			opAMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_AGa2e.GetStatus())
			opAMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmAFlight->Ga2b != TIMENULL) && (prmAFlight->Ga1e != TIMENULL))
		{
			if(prmAFlight->Ga2b > prmAFlight->Ga2e)
				opAMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_ABlt1.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("BLT", "BNAM", olText, "URNO", olTmp );

		if(!m_CE_ABlt1.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING373) + CString("\n");//"Bezeichnung des Gep�ckbandes 1\n";

		if(!m_CE_AB1bs.GetStatus())
			opAMess += GetString(IDS_STRING374) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 1\n";

		if(!m_CE_AB1es.GetStatus())
			opAMess += GetString(IDS_STRING375) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1\n";

		if((prmAFlight->B1bs != TIMENULL) && (prmAFlight->B1es != TIMENULL))
		{
			if(prmAFlight->B1bs > prmAFlight->B1es)
				opAMess += GetString(IDS_STRING376) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_ABlt2.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("BLT", "BNAM", olText, "URNO", olTmp );

		if(!m_CE_ABlt2.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING377) + CString("\n");//"Bezeichnung des Gep�ckbandes 2\n";

		if(!m_CE_AB2bs.GetStatus())
			opAMess += GetString(IDS_STRING378) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 2\n";

		if(!m_CE_AB2es.GetStatus())
			opAMess += GetString(IDS_STRING379) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2\n";

		if((prmAFlight->B2bs != TIMENULL) && (prmAFlight->B2es != TIMENULL))
		{
			if(prmAFlight->B2bs > prmAFlight->B2es)
				opAMess += GetString(IDS_STRING380) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_AExt1.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("EXT", "ENAM", olText, "URNO", olTmp );

		if(!m_CE_AExt1.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING2394) + CString("\n");//"Bezeichnung des Exit 1\n";

		blIsIn = true;
		m_CE_AExt2.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("EXT", "ENAM", olText, "URNO", olTmp );

		if(!m_CE_AExt2.GetStatus() || !blIsIn)
			opAMess += GetString(IDS_STRING2395) + CString("\n");//"Bezeichnung des Exit 2\n";
	}


	if(bpAbflug)
	{
		CString olText;
		CString olTmp;
		bool blIsIn = true;
		m_CE_DPstd.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("PST", "PNAM", olText, "URNO", olTmp );

		if(!m_CE_DPstd.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		if(!m_CE_DPdbs.GetStatus())
			opDMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		if(!m_CE_DPdes.GetStatus())
			opDMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmDFlight->Pdes != TIMENULL) && (prmDFlight->Pdes != TIMENULL))
		{
			if(prmDFlight->Pdbs > prmDFlight->Pdes)
				opDMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_DGtd1.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("GAT_GATE", "GNAM", olText, "URNO", olTmp );

		if(!m_CE_DGtd1.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_DGd1b.GetStatus())
			opDMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_DGd1e.GetStatus())
			opDMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmDFlight->Gd1b != TIMENULL) && (prmDFlight->Gd1e != TIMENULL))
		{
			if(prmDFlight->Gd1b > prmDFlight->Gd1e)
				opDMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_DGtd2.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("GAT_GATE", "GNAM", olText, "URNO", olTmp );

		if(!m_CE_DGtd2.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_DGd2b.GetStatus())
			opDMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_DGd2e.GetStatus())
			opDMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmDFlight->Gd2b != TIMENULL) && (prmDFlight->Gd1e != TIMENULL))
		{
			if(prmDFlight->Gd2b > prmDFlight->Gd2e)
				opDMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		blIsIn = true;
		m_CE_DWro1.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("WRO", "WNAM", olText, "URNO", olTmp );

		if(!m_CE_DWro1.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		blIsIn = true;
		m_CE_DWro2.GetWindowText(olText);
		if (!olText.IsEmpty())
			blIsIn = ogBCD.GetField("WRO", "WNAM", olText, "URNO", olTmp );

		if(!m_CE_DWro2.GetStatus() || !blIsIn)
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DW1bs.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW1es.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if(!m_CE_DW2bs.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW2es.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if((prmDFlight->W1bs != TIMENULL) && (prmDFlight->W1es != TIMENULL))
		{
			if(prmDFlight->W1bs > prmDFlight->W1es)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}

		if((prmDFlight->W2bs != TIMENULL) && (prmDFlight->W2es != TIMENULL))
		{
			if(prmDFlight->W2bs > prmDFlight->W2es)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}
	}


	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;
}




void CSeasonDlg::OnAstatus() 
{
	
	CCS_TRY

	OnEditChanged(0,0);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ANoop.SetCheck(FALSE);

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDgd2d() 
{
	OnEditChanged(0,0);
}


void CSeasonDlg::OnAcxx() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_AStatus.SetCheck(FALSE);
	m_CB_AStatus2.SetCheck(FALSE);
	m_CB_AStatus3.SetCheck(FALSE);
	m_CB_ANoop.SetCheck(FALSE);
	//if(m_CB_ACxx.GetState())
	//	m_CB_AStatus.SetCheck(TRUE);


	if(m_CB_ACxx.GetCheck()  && bgCxxReason)
	{
		if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '-')
		{
			m_CL_ACxxReason.ShowWindow(SW_HIDE);
		}
		else
		{
			m_CL_ACxxReason.ShowWindow(SW_SHOW);
			if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '0')
			{
				m_CL_ACxxReason.EnableWindow(FALSE);
			}
		}
	}
	else
	{
		m_CL_ACxxReason.ShowWindow(SW_HIDE);
	}



	CCS_CATCH_ALL

}

void CSeasonDlg::OnAnoop() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_AStatus.SetCheck(FALSE);
	m_CB_AStatus2.SetCheck(FALSE);
	m_CB_AStatus3.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	//if(m_CB_ANoop.GetState())
	//	m_CB_AStatus.SetCheck(TRUE);

	CCS_CATCH_ALL

}


void CSeasonDlg::EnableDepBelts(boolean bpEnable) {

	if (bgUseDepBelts) {
		if (bpEnable) {
	m_CB_DBlt1List.EnableWindow(true);
	m_CE_DTmb1.EnableWindow(true);
	m_CE_DBlt1.EnableWindow(true);
	m_CE_DB1es.EnableWindow(true);
	m_CE_DB1bs.EnableWindow(true);

	} else {
	m_CB_DBlt1List.EnableWindow(false);
	m_CE_DTmb1.EnableWindow(false);
	m_CE_DBlt1.EnableWindow(false);
	m_CE_DB1es.EnableWindow(false);
	m_CE_DB1bs.EnableWindow(false);
/*
	m_CE_DTmb1.SetWindowText("");
	m_CE_DBlt1.SetWindowText("");
	m_CE_DB1es.SetWindowText("");
	m_CE_DB1bs.SetWindowText("");
	*/
	}
	}
}



void CSeasonDlg::OnDcxx() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DStatus.SetCheck(FALSE);
	m_CB_DStatus2.SetCheck(FALSE);
	m_CB_DStatus3.SetCheck(FALSE);
	m_CB_DNoop.SetCheck(FALSE);
	//if(m_CB_DCxx.GetState())
	//	m_CB_DStatus.SetCheck(TRUE);


	if(m_CB_DCxx.GetCheck()  && bgCxxReason)
	{
		if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '-')
		{
			m_CL_DCxxReason.ShowWindow(SW_HIDE);
		}
		else
		{
			m_CL_DCxxReason.ShowWindow(SW_SHOW);
			if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '0')
			{
				m_CL_DCxxReason.EnableWindow(FALSE);
			}
		}
	}
	else
	{
		m_CL_DCxxReason.ShowWindow(SW_HIDE);
	}

	if(m_CB_DCxx.GetCheck()) {
		EnableDepBelts(true);
	} else {
		EnableDepBelts(false);
	}
	CCS_CATCH_ALL
	
}

void CSeasonDlg::OnDnoop() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DStatus.SetCheck(FALSE);
	m_CB_DStatus2.SetCheck(FALSE);
	m_CB_DStatus3.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	//if(m_CB_DNoop.GetState())
		//m_CB_DStatus.SetCheck(TRUE);

	CCS_CATCH_ALL	

}

void CSeasonDlg::OnDstatus() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DNoop.SetCheck(FALSE);

	CCS_CATCH_ALL

}









void CSeasonDlg::InitTables()
{

	CCS_TRY

	int ili;
	CRect olRectBorder;
	m_CE_AJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomAJfnoTable->SetHeaderSpacing(0);
	pomAJfnoTable->SetMiniTable();
	pomAJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomAJfnoTable->SetIPEditModus(true);

    pomAJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomAJfnoTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomAJfnoTable->SetShowSelection(false);
	pomAJfnoTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[5];
	TABLE_HEADER_COLUMN rlHeader;

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomAJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomAJfnoTable->SetDefaultSeparator();
	pomAJfnoTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomAJfnoTable->DisplayTable();





	m_CE_DJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomDJfnoTable->SetHeaderSpacing(0);
	pomDJfnoTable->SetMiniTable();
	pomDJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomDJfnoTable->SetIPEditModus(true);

	pomDJfnoTable->SetSelectMode(0);
    pomDJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);


	pomDJfnoTable->SetShowSelection(false);
	pomDJfnoTable->ResetContent();
	
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomDJfnoTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDJfnoTable->DisplayTable();




	if (bgCnamAtr)
		m_CE_DCinsBorderExt.GetWindowRect( olRectBorder );
	else
		m_CE_DCinsBorder.GetWindowRect( olRectBorder );

	pomDCinsTable->ResetContent();
	pomDCinsTable->SetHeaderSpacing(0);
	pomDCinsTable->SetMiniTable();
	pomDCinsTable->SetTableEditable(true);

	//rkr04042001
	pomDCinsTable->SetIPEditModus(true);

	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomDCinsTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
//	pomDCinsTable->SetSelectMode(0);


	pomDCinsTable->SetShowSelection(false);
	pomDCinsTable->ResetContent();
	
	int ilCol = 4;
	if (bgCnamAtr)
	{
		prlHeader[0] = new TABLE_HEADER_COLUMN;
		prlHeader[0]->Alignment = COLALIGN_CENTER;
		prlHeader[0]->Length = 40; 
		prlHeader[0]->Font = &ogCourier_Regular_10;
		prlHeader[0]->Text = CString("");

		prlHeader[1] = new TABLE_HEADER_COLUMN;
		prlHeader[1]->Alignment = COLALIGN_CENTER;
		prlHeader[1]->Length = 40; 
		prlHeader[1]->Font = &ogCourier_Regular_10;
		prlHeader[1]->Text = CString("");

		prlHeader[2] = new TABLE_HEADER_COLUMN;
		prlHeader[2]->Alignment = COLALIGN_CENTER;
		prlHeader[2]->Length = 12; 
		prlHeader[2]->Font = &ogCourier_Regular_10;
		prlHeader[2]->Text = CString("");

		prlHeader[3] = new TABLE_HEADER_COLUMN;
		prlHeader[3]->Alignment = COLALIGN_CENTER;
		prlHeader[3]->Length = 46; 
		prlHeader[3]->Font = &ogCourier_Regular_10;
		prlHeader[3]->Text = CString("");

		prlHeader[4] = new TABLE_HEADER_COLUMN;
		prlHeader[4]->Alignment = COLALIGN_CENTER;
		prlHeader[4]->Length = 46; 
		prlHeader[4]->Font = &ogCourier_Regular_10;
		prlHeader[4]->Text = CString("");
		ilCol = 5;

		if(bgShowClassInBatchDlg)
		{
			prlHeader[5] = new TABLE_HEADER_COLUMN;
			prlHeader[5]->Alignment = COLALIGN_CENTER;
			prlHeader[5]->Length = 46; 
			prlHeader[5]->Font = &ogCourier_Regular_10;
			prlHeader[5]->Text = CString("");
			ilCol = 6;
		}

		
	}
	else
	{
		prlHeader[0] = new TABLE_HEADER_COLUMN;
		prlHeader[0]->Alignment = COLALIGN_CENTER;
		prlHeader[0]->Length = 40; 
		prlHeader[0]->Font = &ogCourier_Regular_10;
		prlHeader[0]->Text = CString("");

		prlHeader[1] = new TABLE_HEADER_COLUMN;
		prlHeader[1]->Alignment = COLALIGN_CENTER;
		prlHeader[1]->Length = 12; 
		prlHeader[1]->Font = &ogCourier_Regular_10;
		prlHeader[1]->Text = CString("");

		prlHeader[2] = new TABLE_HEADER_COLUMN;
		prlHeader[2]->Alignment = COLALIGN_CENTER;
		prlHeader[2]->Length = 46; 
		prlHeader[2]->Font = &ogCourier_Regular_10;
		prlHeader[2]->Text = CString("");

		prlHeader[3] = new TABLE_HEADER_COLUMN;
		prlHeader[3]->Alignment = COLALIGN_CENTER;
		prlHeader[3]->Length = 46; 
		prlHeader[3]->Font = &ogCourier_Regular_10;
		prlHeader[3]->Text = CString("");

			ilCol = 4;
		if(bgShowClassInBatchDlg)
		{
			prlHeader[4] = new TABLE_HEADER_COLUMN;
			prlHeader[4]->Alignment = COLALIGN_CENTER;
			prlHeader[4]->Length = 46; 
			prlHeader[4]->Font = &ogCourier_Regular_10;
			prlHeader[4]->Text = CString("");
				ilCol = 5;
		}
	}

	for(ili = 0; ili < ilCol; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDCinsTable->SetHeaderFields(omHeaderDataArray);
	pomDCinsTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDCinsTable->DisplayTable();

	CCS_CATCH_ALL

}

void CSeasonDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}

BOOL CSeasonDlg::DestroyWindow() 
{

	BOOL blRet;
	CCS_TRY

	SaveToReg();

	
	


	
	blRet =  CDialog::DestroyWindow();
	if(blRet)
	{

		delete this;
		pogSeasonDlg = NULL;
	}


	return blRet;

	CCS_CATCH_ALL
	return blRet;

}




void CSeasonDlg::OnAAlc3LIST() 
{

	CCS_TRY
			
	//AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACR","REGN,ACT3,ACT5", "REGN+");

	//polDlg->DoModal();


	CString olText;
	m_CE_AAlc3.GetWindowText(olText);

	CString olSel= omAAlc2 + "," + omAAlc3;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALC2+,ALC3+,ALFN+", olSel);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omAAlc2 = polDlg->GetField("ALC2");	
		omAAlc3 = polDlg->GetField("ALC3");	
		if(omAAlc2.IsEmpty())
			m_CE_AAlc3.SetInitText(omAAlc3, true);	
		else
			m_CE_AAlc3.SetInitText(omAAlc2, true);	


		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTA1,GTA2,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTA,";

		RecalculateFC (olRecFieldsFC);

	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDalc3list() 
{

	CCS_TRY

	CString olText;
	m_CE_DAlc3.GetWindowText(olText);

	CString olSel = omDAlc2 + "," + omDAlc3;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, CString("ALT"), CString("ALC2,ALC3,ALFN"), CString("ALC2+,ALC3+,ALFN+"), olSel);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omDAlc2 = polDlg->GetField("ALC2");	
		omDAlc3 = polDlg->GetField("ALC3");	
		if(omDAlc2.IsEmpty())
			m_CE_DAlc3.SetInitText(omDAlc3, true);	
		else
			m_CE_DAlc3.SetInitText(omDAlc2, true);	

		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTD1,GTD2,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTD,";

		RecalculateFC (olRecFieldsFC);

	}
	delete polDlg;

CCS_CATCH_ALL

}



void CSeasonDlg::OnAblt1list() 
{

	CCS_TRY

	CString olText;
	m_CE_ABlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ABlt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_ABlt1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAblt2list() 
{
	
	CCS_TRY

	CString olText;
	m_CE_ABlt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ABlt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt2.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_ABlt2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDblt1list() 
{

	CCS_TRY

	CString olText;
	m_CE_DBlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DBlt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DBlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_DTmb1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DBlt1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAct35list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	bool blSelAct3 = false;
	if (prmAFlightSave && CString(prmAFlightSave->Act5).IsEmpty())
		blSelAct3 = true;
	if (prmDFlightSave && CString(prmDFlightSave->Act5).IsEmpty())
		blSelAct3 = true;

	if (olText5.IsEmpty() && !olText3.IsEmpty() && blSelAct3)
	{
		OnAct3list();
		return;
	}

	CCS_TRY


	CString olSel = olText5 + "," + olText3;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT5+,ACT3+,ACFN+", olSel );
	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		bmActSelect = true;

		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTD1,GTD2,GTA1,GTA1,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTD,PSTA,";

		RecalculateFC (olRecFieldsFC);
	}
	delete polDlg;

	CCS_CATCH_ALL	

}

void CSeasonDlg::OnAct3list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	bool blSelAct5 = false;
	if (prmAFlightSave && CString(prmAFlightSave->Act3).IsEmpty())
		blSelAct5 = true;
	if (prmDFlightSave && CString(prmDFlightSave->Act3).IsEmpty())
		blSelAct5 = true;

	if (olText3.IsEmpty() && !olText5.IsEmpty() && blSelAct5)
	{
		OnAct35list();
		return;
	}

	CCS_TRY

	CString olSel = olText3 + "," + olText5;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", olSel);
	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		bmActSelect = true;

		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTD1,GTD2,GTA1,GTA1,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTD,PSTA,";

		RecalculateFC (olRecFieldsFC);
	}
	delete polDlg;

	CCS_CATCH_ALL	

}


/*
void CSeasonDlg::OnAct35list() 
{

	CCS_TRY

	CString olText;
	m_CE_Act5.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+", olText);
	polDlg->SetSecState("SEASONDLG_CE_Act3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		m_CE_Act3.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL	

}
*/


void CSeasonDlg::OnAext1list() 
{

	CCS_TRY

	CString olText;
	m_CE_AExt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AExt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt1.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet1.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAext2list() 
{

	CCS_TRY

	CString olText;
	m_CE_AExt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AExt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt2.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet2.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAgta1list() 
{

	CCS_TRY

	CString olText;
	m_CE_AGta1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_PORT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AGta1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAgta2list() 
{

	CCS_TRY

	CString olText;
	m_CE_AGta2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_PORT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AGta2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAorg3list() 
{

	CString olText;
	CString olFields ("APC3,APC4,APFN");
	m_CE_AOrg3.GetWindowText(olText);
	if (olText.IsEmpty())
	{
		m_CE_AOrg4.GetWindowText(olText);
		olFields = "APC4,APC3,APFN";
	}

//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT", olFields, "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg4.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC3"), true);
		m_CE_DDes4.SetFocus();	
	}
	delete polDlg;

/*
	CCS_TRY

	CString olText;
	m_CE_AOrg4.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AOrg4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_AOrg4.SetInitText(polDlg->GetField("APC4"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL
*/
}



void CSeasonDlg::OnApstalist() 
{

	CCS_TRY

	CString olText;
	m_CE_APsta.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_APsta.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_APsta.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}



void CSeasonDlg::OnDdes3list() 
{
	CString olText;
	CString olFields ("APC3,APC4,APFN");
	m_CE_DDes3.GetWindowText(olText);
	if (olText.IsEmpty())
	{
		m_CE_DDes4.GetWindowText(olText);
		olFields = "APC4,APC3,APFN";
	}

//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT", olFields, "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);
		m_CE_DDes4.SetFocus();	
	}
	delete polDlg;

/*
	CCS_TRY

	CString olText;
	m_CE_DDes4.GetWindowText(olText);


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL
*/
}


void CSeasonDlg::OnDgtd1list() 
{

	CCS_TRY

	CString olText;
	m_CE_DGtd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_GATE","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DGtd1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDgtd2list() 
{

	CCS_TRY

	CString olText;
	m_CE_DGtd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_GATE","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DGtd2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDpstdlist() 
{

	CCS_TRY

	CString olText;
	m_CE_DPstd.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DPstd");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DPstd.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_DPstd.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}



void CSeasonDlg::OnDwro1list() 
{

	CCS_TRY

	CString olText;
	m_CE_DWro1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DWro1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro1.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDwro2list() 
{

	CCS_TRY

	CString olText;
	m_CE_DWro2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DWro2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro2.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDttyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DTtyp.GetWindowText(olText);
 
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DTtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DTtyp.SetInitText(polDlg->GetField("TTYP"), true);	

		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTD1,GTD2,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTD,";

		RecalculateFC (olRecFieldsFC);
	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnAttyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_ATtyp.GetWindowText(olText);
 
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ATtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ATtyp.SetInitText(polDlg->GetField("TTYP"), true);	

		CString olRecFieldsFC("");
		if (blDatGatDura)
			olRecFieldsFC = "GTA1,GTA2,";
		if (blDatPosDura)
			olRecFieldsFC += "PSTA,";

		RecalculateFC (olRecFieldsFC);

	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnDstyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAstyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_AStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


/*
void CSeasonDlg::OnAhtyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_AHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnDhtyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}

*/


/////////////////////////////////////////////////////////////////////////////////
//Tables !!





void CSeasonDlg::OnAAlc3LIST2() 
{

	CCS_TRY

	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex();
		
	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAAlc3LIST3() 
{

	CCS_TRY
	
	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDalc3list2() 
{

	CCS_TRY

	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL
	
}

void CSeasonDlg::OnDalc3list3() 
{

	CCS_TRY

	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnDcinslist1() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist2() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 1; 

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist3() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 2;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist4() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 3;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}








void CSeasonDlg::OnDcinsmal4() 
{

	CCS_TRY

	int i = pomDCinsTable->pomListBox->GetTopIndex();

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;
	CString olCatr;
	CString olDisp;

	if (bgCnamAtr)
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCatr);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 4, olCkes);
		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->GetTextFieldValue(i, 5, olDisp);
		}
	}
	else
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkes);
		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->GetTextFieldValue(i, 4, olDisp);
		}
	}

	bool blNumericSort = false;


	if(strcmp(pcgHome, "LIS") == 0)
		blNumericSort = true;

	ogBCD.SetSort("CIC","CNAM+",true,blNumericSort);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	if (bgCnamAtr)
	{
		if (j == ilCount)
		{
			pomDCinsTable->SetIPValue(i + 1, 0, "");
			pomDCinsTable->SetIPValue(i + 1, 1, "");
			pomDCinsTable->SetIPValue(i + 1, 2, "");
			pomDCinsTable->SetIPValue(i + 2, 0, "");
			pomDCinsTable->SetIPValue(i + 2, 1, "");
			pomDCinsTable->SetIPValue(i + 2, 2, "");
			pomDCinsTable->SetIPValue(i + 3, 0, "");
			pomDCinsTable->SetIPValue(i + 3, 1, "");
			pomDCinsTable->SetIPValue(i + 3, 2, "");
			if(bgShowClassInBatchDlg)
			{
				pomDCinsTable->SetIPValue(i + 1, 3, "");
				pomDCinsTable->SetIPValue(i + 2, 3, "");
				pomDCinsTable->SetIPValue(i + 3, 3, "");
			}
		}
		else
		{
			if (ogBCD.GetField("CIC",j + 1,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
//				pomDCinsTable->SetIPValue(i + 1, 2, ogBCD.GetField("CIC",j + 1,"TERM"));
				pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				
				
			}
			if (ogBCD.GetField("CIC",j + 2,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
				pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				pomDCinsTable->SetIPValue(i + 2, 2, ogBCD.GetField("CIC",j + 2,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 3,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
				pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				pomDCinsTable->SetIPValue(i + 3, 2, ogBCD.GetField("CIC",j + 3,"TERM"));
			}
/*
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
			pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
			pomDCinsTable->SetIPValue(i + 1, 2, ogBCD.GetField("CIC",j + 1,"TERM"));
			pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
			pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"CATR"));
			pomDCinsTable->SetIPValue(i + 2, 2, ogBCD.GetField("CIC",j + 2,"TERM"));
			pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
			pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"CATR"));
			pomDCinsTable->SetIPValue(i + 3, 2, ogBCD.GetField("CIC",j + 3,"TERM"));
*/
		}

		pomDCinsTable->SetIPValue(i + 1, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 1, 4, olCkes);

		pomDCinsTable->SetIPValue(i + 2, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 2, 4, olCkes);
		
		pomDCinsTable->SetIPValue(i + 3, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 3, 4, olCkes);

		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->SetIPValue(i + 1, 5, olDisp);
			pomDCinsTable->SetIPValue(i + 2, 5, olDisp);
			pomDCinsTable->SetIPValue(i + 3, 5, olDisp);
		}
	}
	else
	{
		if (j == ilCount)
		{
			pomDCinsTable->SetIPValue(i + 1, 0, "");
			pomDCinsTable->SetIPValue(i + 1, 1, "");
			pomDCinsTable->SetIPValue(i + 2, 0, "");
			pomDCinsTable->SetIPValue(i + 2, 1, "");
			pomDCinsTable->SetIPValue(i + 3, 0, "");
			pomDCinsTable->SetIPValue(i + 3, 1, "");

					
		}
		else
		{
			if (ogBCD.GetField("CIC",j + 1,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
				pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 2,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
				pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 3,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
				pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"TERM"));
			}
/*
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
			pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"TERM"));
			pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
			pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"TERM"));
			pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
			pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"TERM"));
*/
		}

		pomDCinsTable->SetIPValue(i + 1, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 1, 3, olCkes);

		pomDCinsTable->SetIPValue(i + 2, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 2, 3, olCkes);
		
		pomDCinsTable->SetIPValue(i + 3, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 3, 3, olCkes);

		if(bgShowClassInBatchDlg)
		{
			pomDCinsTable->SetIPValue(i + 1, 4, olDisp);
			pomDCinsTable->SetIPValue(i + 2, 4, olDisp);
			pomDCinsTable->SetIPValue(i + 3, 4, olDisp);
		}	
	}

	CCS_CATCH_ALL

}

void CSeasonDlg::ProcessRotationChange(SEASONDLGFLIGHTDATA *prpFlight)
{
	if (prpFlight)
		NewData(this, prpFlight->Rkey, prpFlight->Urno, imModus, bmLocalTime, prpFlight->Adid);

	return;
}


void CSeasonDlg::ProcessRotationChange()
{


	SEASONDLGFLIGHTDATA *prlFlight;
	SEASONDLGFLIGHTDATA *prlFlight2;


	prlFlight = new SEASONDLGFLIGHTDATA;

	*prmAFlight		= *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight		= *prlFlight;
	*prmDFlightSave = *prlFlight;
	delete prlFlight;


	if((prlFlight = ogSeasonDlgFlights.GetFlightByUrno(lmCalledBy)) == NULL)
	{
		//InitDialog(true, true);
	}
	else
	{

		if((strcmp(prlFlight->Des4, pcgHome4) == 0) && (!((strcmp(prlFlight->Org4, pcgHome4) == 0) && omAdid == "D")))
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}

		/*


		if(strcmp(prlFlight->Des4, pcgHome4) == 0)
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}
		*/
	}
	InitDialog(true, true);

	ReadCcaData();
	DShowCinsTable();

	char clPaid = ' ';
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
	}

//	clPaid = GetPaid();
	UpdateCashButton(olAlc3, clPaid);

}

void CSeasonDlg::ReloadCCA()
{

	CCS_TRY

	if (prmDFlight->Urno == 0)
		return;

	CString olWhere;
	olWhere.Format("WHERE FLNU = %d", prmDFlight->Urno);
	char pclWhere[512];
	strcpy(pclWhere,olWhere);
	omCcaData.Read(pclWhere, true);
	
	CCAFLIGHTDATA rlCcaFlight;
	if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
	rlCcaFlight = *prmDFlight;
	if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
	DShowCinsTable();

	CCS_CATCH_ALL

}

void CSeasonDlg::SetSecState()
{

	CCS_TRY
	// Global
	
		m_CB_Ok.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_OK"));
		m_CB_PrevFlight.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ZURUECK"));
		m_CB_NextFlight.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_WEITER"));
		m_CB_AReset.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ARESET"));
		m_CB_DReset.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DRESET"));
		m_CB_DailyMask.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DailyMask"));
		m_CB_Agents.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_Agents"));
		m_CE_Seas.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Seas"));		
		m_CE_Ming.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Ming"));		
		m_CE_Pbis.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Pbis"));		
		m_CE_Pvom.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Pvom"));		
		m_CE_Freq.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Freq"));		
		m_CE_Act3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Act3"));		
		m_CE_Regn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Regn"));		

	// arrival
		if (bgCheckPRFL)
		{
			CWnd* polWnd = GetDlgItem(IDC_STATIC_ACHT3); 

			if(ogPrivList.GetStat("SEASONDLG_CE_APrfl") == '-')
			{
				if (polWnd)
					polWnd->ShowWindow(SW_HIDE);
			}
			else if (ogPrivList.GetStat("SEASONDLG_CE_APrfl") == '0')
			{
				if (polWnd)
				{
					polWnd->EnableWindow(FALSE);
					polWnd->ShowWindow(SW_SHOW);
				}
			}
			else if (ogPrivList.GetStat("SEASONDLG_CE_APrfl") == '1')
			{
				if (polWnd)
				{
					polWnd->EnableWindow(TRUE);
					polWnd->ShowWindow(SW_SHOW);
				}
			}
			m_CE_ACht3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APrfl"));		
		}

		if(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno") == '-')
			m_CB_AShowJfno.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno"));


		if(ogPrivList.GetStat("SEASONDLG_CB_AShowVia") == '-')
			m_CB_AVia.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AShowVia"));

		if(ogPrivList.GetStat("SEASONDLG_CL_AHistory") == '-')
		{
			CWnd* polWnd = GetDlgItem(IDC_STATIC_AHISTORY); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}

		m_CB_ADaly.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ADaly"));
		m_CB_AAlc3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List"));
		m_CB_AAlc3List2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List23"));
		m_CB_AAlc3List3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List23"));
		m_CB_AStatus.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus"));
		m_CB_AStatus2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus2"));
		m_CB_AStatus3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus3"));
		m_CB_AOrg3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AOrg3List"));
		m_CB_Act35List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_Act35List"));
		m_CB_ATTypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ATTypList"));
		m_CB_AStypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStypList"));
//		m_CB_AHtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AHtypList"));
		m_CB_APstaList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_APstaList"));
		m_CB_AGta2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AGta2List"));
		m_CB_AGta1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AGta1List"));
		m_CB_AExt2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AExt2List"));
		m_CB_AExt1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AExt1List"));
		m_CB_ABlt2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ABlt2List"));
		m_CB_ABlt1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ABlt1List"));
		m_CB_ANoop.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ANoop"));
		m_CB_ACxx.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ACxx"));
		m_CE_ATmb2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATmb2"));	
		m_CE_ALastChange.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ALastChange"));	
		m_CE_AFluko.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFluko"));		
		m_CE_ACreated.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ACreated"));	
		m_CE_AExt2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AExt2"));		
		m_CE_AExt1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AExt1"));		
		m_CE_ARem1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ARem1"));		
		m_CE_ARem2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ARem1"));		
		m_CE_Act5.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Act5"));		
		m_CE_ABlt2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ABlt2"));		
		m_CE_ABlt1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ABlt1"));		
		m_CE_AEtai.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AEtai"));		
		m_CE_AGta2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGta2"));		
		m_CE_AGta1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGta1"));		
		m_CE_AStoa.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStoa"));		
		m_CE_AStod.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStod"));		
		m_CE_AOrg3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AOrg3"));		
		m_CE_AOrg4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AOrg4"));		
		m_CE_AStev.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStev"));		
		m_CE_ASte2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ASte2"));		
		m_CE_ASte3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ASte3"));		
		m_CE_ASte4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ASte4"));		
		m_CE_AStyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStyp"));		
//		m_CE_AHtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AHtyp"));		
		m_CE_ATtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATtyp"));		
		m_CE_AFlns.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFlns"));		
		m_CE_AFltn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFltn"));		
		m_CE_AFlti.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFlti"));		
		m_CE_AAlc3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AAlc3"));		
		m_CE_ADes3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ADes3"));		
		m_CE_ADays.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ADays"));
		m_CE_APsta.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APsta"));
		m_CE_APabs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APabs"));
		m_CE_APaes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APaes"));
		m_CE_ATet1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATet1"));
		m_CE_ATet2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATet2"));
		m_CE_AGa1b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa1b"));
		m_CE_AGa2b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa2b"));
		m_CE_AGa1e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa1e"));
		m_CE_AGa2e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa2e"));
		m_CE_ATmb1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATmb1"));
		m_CE_AB1bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB1bs"));
		m_CE_AB2bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB2bs"));
		m_CE_AB1es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB1es"));
		m_CE_AB2es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB2es"));
		m_CE_ATga1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATga1"));
		m_CE_ATga2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATga2"));
		m_CE_ACsgn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ACsgn"));
		
		if(bgShowCSTAD)
		{
			m_CE_ACsta.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ACSta"));
			m_CE_DCstd.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DCStd"));
		}
		else
		{
			m_CE_ACsta.ShowWindow(SW_HIDE);
			m_CE_DCstd.ShowWindow(SW_HIDE);
		}


	// departure
		if (bgCheckPRFL)
		{
			CWnd* polWnd = GetDlgItem(IDC_STATIC_DCHT3); 

			if(ogPrivList.GetStat("SEASONDLG_CE_DPrfl") == '-')
			{
				if (polWnd)
					polWnd->ShowWindow(SW_HIDE);
			}
			else if (ogPrivList.GetStat("SEASONDLG_CE_DPrfl") == '0')
			{
				if (polWnd)
				{
					polWnd->EnableWindow(FALSE);
					polWnd->ShowWindow(SW_SHOW);
				}
			}
			else if (ogPrivList.GetStat("SEASONDLG_CE_DPrfl") == '1')
			{
				if (polWnd)
				{
					polWnd->EnableWindow(TRUE);
					polWnd->ShowWindow(SW_SHOW);
				}
			}
			m_CE_DCht3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPrfl"));		
		}


		if(ogPrivList.GetStat("SEASONDLG_CB_DShowVia") == '-')
			m_CB_DVia.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DShowVia"));

		if(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno") == '-')
			m_CB_DShowJfno.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno"));

		if(ogPrivList.GetStat("SEASONDLG_CL_DHistory") == '-')
		{
			CWnd* polWnd = GetDlgItem(IDC_STATIC_DHISTORY); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}
		m_CB_DDaly.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DDaly"));
		m_CB_DStatus.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus"));
		m_CB_DStatus2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus2"));
		m_CB_DStatus3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus3"));
		m_CB_DDes3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DDes3List"));
		m_CB_DAlc3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List"));
		m_CB_DAlc3List2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List23"));
		m_CB_DAlc3List3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List23"));
		m_CB_DTtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DTtypList"));
		m_CB_DStypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStypList"));
//		m_CB_DHtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DHtypList"));
		m_CB_DWro1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DWro1List"));
		m_CB_DWro2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DWro2List"));
		m_CB_DPstdList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DPstdList"));
		m_CB_DGtd2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DGtd2List"));
		m_CB_DGtd1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DGtd1List"));
		m_CB_DCinsList1.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList4.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsMal4.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		
		
		if (bgUseDepBelts) {
			m_CB_DBlt1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DBlt1List"));	
			m_CE_DBlt1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DBlt1"));	
			m_CE_DTmb1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTmb1"));
			m_CE_DB1es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DB1es"));		
			m_CE_DB1bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DB1bs"));
	
		} else {
			CWnd* polWnd = GetDlgItem(IDC_DBLT1); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DTMB1); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DB1EA); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DB1BA); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DBLTLABEL); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DBLT1LIST); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			
		}
		


		if(ogPrivList.GetStat("SEASONDLG_CB_DCinsList") == '-')
		{
				m_ComboDcins.ShowWindow(SW_HIDE);
		}
		else if(ogPrivList.GetStat("SEASONDLG_CB_DCinsList") == '0')
		{
				m_ComboDcins.EnableWindow(FALSE);
		}
		if (ogFactor_CCA.IsEmpty())
		{
				m_ComboDcins.ShowWindow(SW_HIDE);
		}


		m_CB_DCxx.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCxx"));
		m_CB_DNoop.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DNoop"));
		m_CE_DLastChange.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DLastChange"));	
		m_CE_DFluko.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFluko"));		
		m_CE_DCreated.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DCreated"));	
		m_CE_DAlc3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DAlc3"));		
		m_CE_DRem1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DRem1"));		
		m_CE_DRem2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DRem1"));		
		m_CE_DGtd2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGtd2"));		
		m_CE_DGtd1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGtd1"));		
//		m_CE_DHtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DHtyp"));		
		m_CE_DStev.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStev"));		
		m_CE_DSte2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DSte2"));		
		m_CE_DSte3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DSte3"));		
		m_CE_DSte4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DSte4"));		
		m_CE_DTtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTtyp"));		
		m_CE_DStyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStyp"));		
		m_CE_DFlns.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFlns"));		
		m_CE_DFltn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFltn"));		
		m_CE_DFlti.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFlti"));		
		m_CE_DStod.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStod"));		
		m_CE_DStoa.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStoa"));		
		m_CE_DEtdi.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DEtdi"));		
		m_CE_DDes3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDes3"));		
		m_CE_DDes4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDes4"));		
		m_CE_DOrg3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DOrg3"));		
		m_CE_DDays.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDays"));
		m_CE_DPstd.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPstd"));
		m_CE_DPdbs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPdbs"));
		m_CE_DPdes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPdes"));
		m_CE_DTgd1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTgd1"));
		m_CE_DTgd2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTgd2"));
		m_CE_DGd1b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd1b"));
		m_CE_DGd2b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd2b"));
		m_CE_DGd1e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd1e"));
		m_CE_DGd2e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd2e"));
		m_CE_DWro1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DWro1"));
		m_CE_DWro2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DWro2"));
		m_CE_DTwr1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTwr1"));
		m_CE_DTwr2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTwr2"));
		m_CE_DW1bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW1bs"));
		m_CE_DW1es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW1es"));
		m_CE_DW2bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW2bs"));
		m_CE_DW2es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW2es"));

		m_CE_DBaz1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DBaz1"));
		m_CE_DBaz4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DBaz4"));
		m_CB_DGd2d.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DGd2d"));

		m_CE_DCsgn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DCsgn"));

		if (!bgFlightIDEditable)
		{
			// enable editing of flight id
//			m_CE_DFlti.SetSecState('0');
//			m_CE_AFlti.SetSecState('0');
		}


		if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '-' || !bgCxxReason)
		{
				m_CL_ACxxReason.ShowWindow(SW_HIDE);
		}
		else if(ogPrivList.GetStat("SEASONDLG_CE_ACxxReason") == '0')
		{
				m_CL_ACxxReason.EnableWindow(FALSE);
		}

		if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '-' || !bgCxxReason)
		{
				m_CL_DCxxReason.ShowWindow(SW_HIDE);
		}
		else if(ogPrivList.GetStat("SEASONDLG_CE_DCxxReason") == '0')
		{
				m_CL_DCxxReason.EnableWindow(FALSE);
		}


		if(bgNightFlightSlot)
		{
			m_CE_ANfes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ANfes"));
			m_CB_ANfes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ANfes"));
			m_CE_DNfes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DNfes"));
			m_CB_DNfes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DNfes"));
		}
		else
		{
			m_CE_ANfes.ShowWindow(SW_HIDE);
			m_CB_ANfes.ShowWindow(SW_HIDE);
			m_CE_DNfes.ShowWindow(SW_HIDE);
			m_CB_DNfes.ShowWindow(SW_HIDE);
		}



	CCS_CATCH_ALL

}



void CSeasonDlg::EnableGatPos()
{
	if(imModus != DLG_CHANGE_DIADATA) return;
	
	
	bool blAEnable = false;
	bool blDEnable = false;

	if (polRotationAViaDlg)
		polRotationAViaDlg->Enable(blAEnable);
	if (polRotationDViaDlg)
		polRotationDViaDlg->Enable(blDEnable);
/*
	pomAViaCtrl->pomTable->SetTableEditable(false);
	pomDViaCtrl->pomTable->SetTableEditable(false);
//	pomAViaCtrl->pomTable->ShowWindow(SW_HIDE);
	pomDViaCtrl->Enable(false);
	pomAViaCtrl->Enable(false);
*/
	if(prmAFlight->Urno > 0)  
		blAEnable = TRUE;

	// Arrival
	m_CB_APstaList.EnableWindow(blAEnable);
	m_CB_AGta2List.EnableWindow(blAEnable);
	m_CB_AGta1List.EnableWindow(blAEnable);
 	m_CB_ABlt2List.EnableWindow(blAEnable);
	m_CB_ABlt1List.EnableWindow(blAEnable);
 
	m_CE_ABlt2.EnableWindow(blAEnable);
	m_CE_ATmb2.EnableWindow(blAEnable);
	m_CE_ABlt1.EnableWindow(blAEnable);
	m_CE_ATmb1.EnableWindow(blAEnable);
 	m_CE_AGta2.EnableWindow(blAEnable);
	m_CE_ATga2.EnableWindow(blAEnable);
	m_CE_AGta1.EnableWindow(blAEnable);
	m_CE_ATga1.EnableWindow(blAEnable);
 	m_CE_AB1bs.EnableWindow(blAEnable);
	m_CE_AB1es.EnableWindow(blAEnable);
	m_CE_AB2bs.EnableWindow(blAEnable);
	m_CE_AB2es.EnableWindow(blAEnable);
	m_CE_AGa1b.EnableWindow(blAEnable);
	m_CE_AGa1e.EnableWindow(blAEnable);
	m_CE_AGa2b.EnableWindow(blAEnable);
	m_CE_AGa2e.EnableWindow(blAEnable);
	m_CE_APsta.EnableWindow(blAEnable);
	m_CE_APabs.EnableWindow(blAEnable);
	m_CE_APaes.EnableWindow(blAEnable);

	m_CB_AExt2List.EnableWindow(blAEnable);
	m_CB_AExt1List.EnableWindow(blAEnable);
	m_CB_AExt2List.EnableWindow(blAEnable);
	m_CB_AExt1List.EnableWindow(blAEnable);
	m_CE_AExt2.EnableWindow(blAEnable);
	m_CE_AExt1.EnableWindow(blAEnable);
	m_CE_ATet2.EnableWindow(blAEnable);
	m_CE_ATet1.EnableWindow(blAEnable);

	// Departure
	if(prmDFlight->Urno > 0)
		blDEnable = TRUE;

	m_CB_DWro1List.EnableWindow(blDEnable);
	m_CB_DWro2List.EnableWindow(blDEnable);
	m_CB_DPstdList.EnableWindow(blDEnable);
	m_CB_DGtd2List.EnableWindow(blDEnable);
	m_CB_DGtd1List.EnableWindow(blDEnable);

	m_CE_DGtd2.EnableWindow(blDEnable);
	//m_CE_DTgd2.EnableWindow(blDEnable);
	m_CE_DGtd1.EnableWindow(blDEnable);
	//m_CE_DTgd1.EnableWindow(blDEnable);
 	m_CE_DGd1b.EnableWindow(blDEnable);
	m_CE_DGd1e.EnableWindow(blDEnable);
	m_CE_DGd2b.EnableWindow(blDEnable);
	m_CE_DGd2e.EnableWindow(blDEnable);
	m_CE_DW1bs.EnableWindow(blDEnable);
	m_CE_DW1es.EnableWindow(blDEnable);
	m_CE_DW2bs.EnableWindow(blDEnable);
	m_CE_DW2es.EnableWindow(blDEnable);
	m_CE_DWro1.EnableWindow(blDEnable);
	m_CE_DWro2.EnableWindow(blDEnable);
	//m_CE_DTwr1.EnableWindow(blDEnable);
	m_CE_DPstd.EnableWindow(blDEnable);
	m_CE_DPdbs.EnableWindow(blDEnable);
	m_CE_DPdes.EnableWindow(blDEnable);
//###
	if (blDEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		pomDCinsTable->SetTableEditable(false);
		//SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CE_DCins"),pomDCinsTable);
	}
	m_CB_DCinsMal4.EnableWindow(false);
	m_CE_DBaz1.EnableWindow(blDEnable);
	m_CE_DBaz4.EnableWindow(blDEnable);

	m_CE_DTwr1.EnableWindow(blDEnable);
	m_CE_DTwr2.EnableWindow(blDEnable);
	m_CE_DTgd1.EnableWindow(blDEnable);
	m_CE_DTgd2.EnableWindow(blDEnable);

	m_CB_DCinsList4.EnableWindow(false);
	m_CB_DCinsList3.EnableWindow(false);
	m_CB_DCinsList2.EnableWindow(false);
	m_CB_DCinsList1.EnableWindow(false);

//####
	// Global
 	m_CB_Ok.EnableWindow(blAEnable || blDEnable);
	m_CB_AReset.EnableWindow(FALSE);
	m_CB_DReset.EnableWindow(FALSE);
	m_CB_DailyMask.EnableWindow(FALSE);
		
	m_CB_DDaly.EnableWindow(FALSE);
	m_CB_ADaly.EnableWindow(FALSE);
		
	m_CE_DDays.SetReadOnly();
	m_CE_ADays.SetReadOnly();
		
	m_CE_Pvom.SetReadOnly();
	m_CE_Pbis.SetReadOnly();
	m_CE_Freq.SetReadOnly();

	return;
}






void CSeasonDlg::EnableArrival()
{

	CCS_TRY

	BOOL blEnable = FALSE;

	char clStat;

	if(imModus != DLG_CHANGE_DIADATA && ((prmAFlight->Urno > 0) || (imModus != DLG_CHANGE)))  
		blEnable = TRUE;


	
	if(!blEnable)
	{
		m_CE_Freq.EnableWindow(blEnable);
		AShowJfnoButton();
	}



	if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		m_CE_ARem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(blEnable);

		m_CE_ARem1.EnableWindow(blEnable);
	}

	clStat = ogPrivList.GetStat("SEASONDLG_CL_ARemp");

	m_CB_ARemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CB_ARemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CB_ARemp.EnableWindow(FALSE);
		}
		else
		{
			m_CB_ARemp.ShowWindow(SW_HIDE);
		}

	}
	//----------
	clStat = ogPrivList.GetStat("SEASONDLG_CL_AHistory");

	m_ComboAHistory.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboAHistory.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboAHistory.EnableWindow(FALSE);
		}
		else
		{
			m_ComboAHistory.ShowWindow(SW_HIDE);
		}
	}
	//----------



	m_CE_ACsgn.EnableWindow(blEnable);
	m_CE_ACsta.EnableWindow(blEnable);
	m_CB_AOrg3List.EnableWindow(blEnable);
	m_CB_ATTypList.EnableWindow(blEnable);
	m_CB_AStypList.EnableWindow(blEnable);
//	m_CB_AHtypList.EnableWindow(blEnable);
	m_CE_ATmb2.EnableWindow(blEnable);
	m_CB_APstaList.EnableWindow(blEnable);
	m_CB_AGta2List.EnableWindow(blEnable);
	m_CB_AGta1List.EnableWindow(blEnable);
	m_CB_AExt2List.EnableWindow(blEnable);
	m_CB_AExt1List.EnableWindow(blEnable);
	m_CB_ABlt2List.EnableWindow(blEnable);
	m_CB_ABlt1List.EnableWindow(blEnable);
	m_CE_ALastChange.EnableWindow(blEnable);
	m_CE_AFluko.EnableWindow(blEnable);
	m_CB_ADaly.EnableWindow(blEnable);
	m_CE_ACreated.EnableWindow(blEnable);
	m_CB_AAlc3List.EnableWindow(blEnable);

	m_CB_AStatus.EnableWindow(blEnable);
	m_CB_AStatus2.EnableWindow(blEnable);
	m_CB_AStatus3.EnableWindow(blEnable);
	m_CE_ARem2.EnableWindow(blEnable);
	m_CE_ACht3.EnableWindow(blEnable);
	m_CB_ANoop.EnableWindow(blEnable);
	m_CE_AExt2.EnableWindow(blEnable);
	m_CE_AExt1.EnableWindow(blEnable);
	m_CE_ADays.EnableWindow(blEnable);
	m_CB_ACxx.EnableWindow(blEnable);
	m_CE_ABlt2.EnableWindow(blEnable);
	m_CE_ABlt1.EnableWindow(blEnable);
	m_CE_AEtai.EnableWindow(blEnable);
	m_CE_Freq.EnableWindow(blEnable);
	m_CE_AGta2.EnableWindow(blEnable);
	m_CE_AGta1.EnableWindow(blEnable);
	m_CE_AStoa.EnableWindow(blEnable);
	m_CE_AStod.EnableWindow(blEnable);
	m_CE_AOrg4.EnableWindow(blEnable);
	m_CE_AOrg3.EnableWindow(blEnable);
	m_CE_AStev.EnableWindow(blEnable);
	if (bgUseAddKeyfields)
	{
		m_CE_ASte2.ShowWindow(SW_SHOW);
		m_CE_ASte3.ShowWindow(SW_SHOW);
		m_CE_ASte4.ShowWindow(SW_SHOW);
		m_CE_ASte2.EnableWindow(blEnable);
		m_CE_ASte3.EnableWindow(blEnable);
		m_CE_ASte4.EnableWindow(blEnable);
	} else
	{
		m_CE_ASte2.ShowWindow(SW_HIDE);
		m_CE_ASte3.ShowWindow(SW_HIDE);
		m_CE_ASte4.ShowWindow(SW_HIDE);
	}

	m_CE_AStyp.EnableWindow(blEnable);
//	m_CE_AHtyp.EnableWindow(blEnable);
	m_CE_ATtyp.EnableWindow(blEnable);
	m_CE_AFlns.EnableWindow(blEnable);
	m_CE_AFltn.EnableWindow(blEnable);
	m_CE_AFlti.EnableWindow(blEnable);
	m_CE_AAlc3.EnableWindow(blEnable);
	m_CE_ADes3.EnableWindow(blEnable);
	m_CE_AB1bs.EnableWindow(blEnable);
	m_CE_AB1es.EnableWindow(blEnable);
	m_CE_AB2bs.EnableWindow(blEnable);
	m_CE_AB2es.EnableWindow(blEnable);
	m_CE_AGa1b.EnableWindow(blEnable);
	m_CE_AGa1e.EnableWindow(blEnable);
	m_CE_AGa2b.EnableWindow(blEnable);
	m_CE_AGa2e.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CE_ATet1.EnableWindow(blEnable);
	m_CE_ATet2.EnableWindow(blEnable);
	m_CE_ATga1.EnableWindow(blEnable);
	m_CE_ATga2.EnableWindow(blEnable);
	m_CE_ATmb1.EnableWindow(blEnable);
	m_CE_APabs.EnableWindow(blEnable);
	m_CE_APaes.EnableWindow(blEnable);
	m_CE_ACht3.EnableWindow(blEnable);

	m_CB_ANfes.EnableWindow(blEnable);


	if(imModus == DLG_CHANGE)
	{
		m_CB_ADaly.EnableWindow(FALSE);
		m_CE_ADays.EnableWindow(FALSE);
	}
	else
	{
		m_CB_ADaly.EnableWindow(blEnable);
		m_CE_ADays.EnableWindow(blEnable);
	}



	if((bmIsAFfnoShown ||  strcmp(pcgHome, "ATH") == 0) && omAJfno.GetSize() > 0)
		OnAshowjfno();
	else
		AShowJfnoButton();

	m_CB_AShowJfno.EnableWindow(blEnable);


	if(( CString(prmAFlight->Ftyp)	== "Z" ) || (CString(prmAFlight->Ftyp)	== "B" ))
	{
		m_CE_AOrg3.EnableWindow(FALSE);	
		m_CE_AOrg4.EnableWindow(FALSE);	
		m_CB_AOrg3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_AOrg3.EnableWindow(blEnable);	
		m_CE_AOrg4.EnableWindow(blEnable);	
		m_CB_AOrg3List.EnableWindow(blEnable);	
	}


	if(ogFpeData.bmFlightPermitsEnabled && pomArrPermitsButton != NULL)
		pomArrPermitsButton->ShowWindow(blEnable ? SW_SHOW : SW_HIDE);

		m_APay.ShowWindow(SW_HIDE);
		if(bgShowPayDetail)
		{
			if(imModus == DLG_NEW)
			{
				m_APay.ShowWindow(SW_SHOW);

				if(bgShowCashButtonInBothFlight)
				{
					m_CashArrBtn->ShowWindow(SW_HIDE);
				}
			}
			else
			{
				CString olAMopa= GetMopa(prmAFlight->Urno);

				CString olSelection;
				olSelection.Format("WHERE URNO='%d'",prmAFlight->Urno);

				CString olDummyTableName="AFT";
				ogBCD.SetObject(olDummyTableName, "URNO,RREQ");
				ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
				ogBCD.Read(olDummyTableName,olSelection);

					
				int ndxRreg = ogBCD.GetFieldIndex( olDummyTableName, "RREQ" );	
				int nItems = ogBCD.GetDataCount( olDummyTableName );

				CString olARreg;
				for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
				{
					RecordSet record ;
					if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
					{
						olARreg = record[ ndxRreg ];
						
					}
				}		
				if(olAMopa=="B" || olARreg=="1")
				{
					m_APay.ShowWindow(SW_SHOW);
				}
			}
		}

	if(bgShowCashButtonInBothFlight)
	{
		m_CashArrBtn->EnableWindow(blEnable);
	}

	CCS_CATCH_ALL

}

void CSeasonDlg::SetModus(int ipModus)
{
	if (ipModus == DLG_CHANGE_DIADATA || ipModus == DLG_CHANGE)
		imModus = ipModus;
}



void CSeasonDlg::EnableGlobal()
{

	CCS_TRY

	BOOL blEnable = FALSE;


	if(imModus != DLG_CHANGE_DIADATA &&  ((prmAFlight->Urno > 0) || (prmDFlight->Urno > 0)  || (imModus != DLG_CHANGE)))
		blEnable = TRUE;

	if(imModus == DLG_CHANGE)
		m_CE_Regn.SetReadOnly(false);
	else
	{
		if (bgEnableRegInsert)
			m_CE_Regn.SetReadOnly(false);
		else
			m_CE_Regn.SetReadOnly(true);
	}

	m_CB_Act35List.EnableWindow(blEnable);
	m_CE_Act5.EnableWindow(blEnable);
	m_CE_Act3.EnableWindow(blEnable);
	m_CE_Ming.EnableWindow(blEnable);
	m_CE_Seas.EnableWindow(blEnable);
	m_CB_Ok.EnableWindow(blEnable);
	m_CE_Nose.EnableWindow(blEnable);

	if (ogPrivList.GetStat("SEASONDLG_CB_DailyMask") == '1')
		m_CB_DailyMask.EnableWindow(blEnable);
	else
		m_CB_DailyMask.EnableWindow(FALSE);


	// additional Text for caption
	CString olTimes;
	if (bmLocalTime)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);


	if(imModus == DLG_CHANGE_DIADATA)
	{
		SetWindowText(GetString(IDS_STRING403)+" ("+olTimes+")");
	}
	else if(imModus == DLG_CHANGE)
	{
		SetWindowText(GetString(IDS_STRING403)+" ("+olTimes+")");

		m_CB_PrevFlight.EnableWindow(TRUE);
		m_CB_NextFlight.EnableWindow(TRUE);
		
		m_CB_AReset.EnableWindow(FALSE);
		m_CB_DReset.EnableWindow(FALSE);
		
		m_CB_DDaly.EnableWindow(FALSE);
		m_CB_ADaly.EnableWindow(FALSE);
		
		m_CE_DDays.SetReadOnly();
		m_CE_ADays.SetReadOnly();
		
		m_CE_Pvom.SetReadOnly();
		m_CE_Pbis.SetReadOnly();
		m_CE_Freq.SetReadOnly();
	}
	else
	{
		SetWindowText(GetString(IDS_STRING404)+" ("+olTimes+")");

		m_CB_PrevFlight.EnableWindow(FALSE);
		m_CB_NextFlight.EnableWindow(FALSE);
		
		m_CB_AReset.EnableWindow(TRUE);
		m_CB_DReset.EnableWindow(TRUE);
		
		m_CB_DailyMask.EnableWindow(FALSE);

		m_CB_DDaly.EnableWindow(TRUE);
		m_CB_ADaly.EnableWindow(TRUE);
		
		m_CE_ADays.SetBKColor(YELLOW);
		m_CE_DDays.SetBKColor(YELLOW);

		m_CE_DDays.SetReadOnly(false);
		m_CE_ADays.SetReadOnly(false);
		
		m_CE_Pvom.SetReadOnly(false);
		m_CE_Pbis.SetReadOnly(false);

		m_CE_Freq.SetReadOnly(false);
		m_CE_Pvom.SetBKColor(YELLOW);
		m_CE_Pbis.SetBKColor(YELLOW);
	}


	CCS_CATCH_ALL

}



void CSeasonDlg::EnableDeparture()
{

	CCS_TRY

	BOOL blEnable = FALSE;

	if(imModus != DLG_CHANGE_DIADATA && ((prmDFlight->Urno > 0) || (imModus != DLG_CHANGE)))
		blEnable = TRUE;


	if(blEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		pomDCinsTable->SetTableEditable(true);
		SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CE_DCins"),pomDCinsTable);
	}
	else
	{
		pomDCinsTable->ShowWindow(SW_HIDE);
	}


	if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		m_CE_DRem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(blEnable);

		m_CE_DRem1.EnableWindow(blEnable);
	}



	char clStat = ogPrivList.GetStat("SEASONDLG_CL_DRemp");

	m_CB_DRemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CB_DRemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CB_DRemp.EnableWindow(FALSE);
		}
		else
		{
			m_CB_DRemp.ShowWindow(SW_HIDE);
		}

	}

	//----------
	clStat = ogPrivList.GetStat("SEASONDLG_CL_DHistory");

	m_ComboDHistory.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboDHistory.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboDHistory.EnableWindow(FALSE);
		}
		else
		{
			m_ComboDHistory.ShowWindow(SW_HIDE);
		}
	}

	//----------
	clStat = ogPrivList.GetStat("SEASONDLG_CB_DCinsList");

	m_ComboDcins.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboDcins.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboDcins.EnableWindow(FALSE);
		}
		else
		{
			m_ComboDcins.ShowWindow(SW_HIDE);
		}
	}

	if (ogFactor_CCA.IsEmpty())
	{
		m_ComboDcins.ShowWindow(SW_HIDE);
	}
	//---------


	m_CE_DCsgn.EnableWindow(blEnable);

	m_CE_DCstd.EnableWindow(blEnable);
	m_CE_DBaz1.EnableWindow(blEnable);
	m_CE_DBaz4.EnableWindow(blEnable);

	m_CB_DCinsMal4.EnableWindow(blEnable);
	m_CB_DDes3List.EnableWindow(blEnable);
	m_CB_DAlc3List.EnableWindow(blEnable);
	m_CB_DTtypList.EnableWindow(blEnable);
	m_CB_DStypList.EnableWindow(blEnable);
	m_CB_Default.EnableWindow(blEnable);
	m_CE_DLastChange.EnableWindow(blEnable);
	m_CE_DFluko.EnableWindow(blEnable);
	m_CE_DCreated.EnableWindow(blEnable);
	m_CE_DAlc3.EnableWindow(blEnable);
	m_CE_DCht3.EnableWindow(blEnable);
	m_CB_DWro1List.EnableWindow(blEnable);
	m_CB_DWro2List.EnableWindow(blEnable);
	m_CB_DPstdList.EnableWindow(blEnable);
	m_CB_DGtd2List.EnableWindow(blEnable);
	m_CB_DGtd1List.EnableWindow(blEnable);
	m_CB_DCinsList4.EnableWindow(blEnable);
	m_CB_DCinsList3.EnableWindow(blEnable);
	m_CB_DCinsList2.EnableWindow(blEnable);
	m_CB_DCinsList1.EnableWindow(blEnable);
	m_CB_DStatus.EnableWindow(blEnable);
	m_CE_DRem2.EnableWindow(blEnable);
	m_CB_DNoop.EnableWindow(blEnable);
	m_CE_DGtd2.EnableWindow(blEnable);
	m_CE_DGtd1.EnableWindow(blEnable);
	m_CB_DCxx.EnableWindow(blEnable);
	m_CB_DStatus3.EnableWindow(blEnable);
	m_CB_DStatus2.EnableWindow(blEnable);
	m_CE_DDes3.EnableWindow(blEnable);
	m_CE_DDes4.EnableWindow(blEnable);
	m_CE_DStev.EnableWindow(blEnable);

	if (bgUseAddKeyfields) 
	{
		m_CE_DSte2.ShowWindow(SW_SHOW);
		m_CE_DSte3.ShowWindow(SW_SHOW);
		m_CE_DSte4.ShowWindow(SW_SHOW);
		m_CE_DSte2.EnableWindow(blEnable);
		m_CE_DSte3.EnableWindow(blEnable);
		m_CE_DSte4.EnableWindow(blEnable);
	} else
	{
		m_CE_DSte2.ShowWindow(SW_HIDE);
		m_CE_DSte3.ShowWindow(SW_HIDE);
		m_CE_DSte4.ShowWindow(SW_HIDE);
	}


	m_CE_DTtyp.EnableWindow(blEnable);
	m_CE_DStyp.EnableWindow(blEnable);
	m_CE_DFlns.EnableWindow(blEnable);
	m_CE_DFltn.EnableWindow(blEnable);
	m_CE_DFlti.EnableWindow(blEnable);
	m_CE_DStod.EnableWindow(blEnable);
	m_CE_DStoa.EnableWindow(blEnable);
	m_CE_DEtdi.EnableWindow(blEnable);
	m_CE_DOrg3.EnableWindow(blEnable);
	m_CE_DGd1b.EnableWindow(blEnable);
	m_CE_DGd1e.EnableWindow(blEnable);
	m_CE_DGd2b.EnableWindow(blEnable);
	m_CE_DGd2e.EnableWindow(blEnable);
	m_CE_DTgd1.EnableWindow(blEnable);
	m_CE_DTgd2.EnableWindow(blEnable);
	m_CE_DTwr1.EnableWindow(blEnable);
	m_CE_DTwr2.EnableWindow(blEnable);
	m_CE_DW1bs.EnableWindow(blEnable);
	m_CE_DW1es.EnableWindow(blEnable);
	m_CE_DW2bs.EnableWindow(blEnable);
	m_CE_DW2es.EnableWindow(blEnable);
	m_CE_DWro1.EnableWindow(blEnable);
	m_CE_DWro2.EnableWindow(blEnable);
	m_CE_DPstd.EnableWindow(blEnable);
	m_CE_DPdbs.EnableWindow(blEnable);
	m_CE_DPdes.EnableWindow(blEnable);

	m_CB_DNfes.EnableWindow(blEnable);

	m_CE_DJfnoBorder.EnableWindow(FALSE);
	m_CE_DCinsBorder.EnableWindow(FALSE);
	m_CE_DCinsBorderExt.EnableWindow(FALSE);

	m_CE_DCht3.EnableWindow(blEnable);

	if((bmIsDFfnoShown ||  strcmp(pcgHome, "ATH") == 0) && omDJfno.GetSize() > 0)
		OnDshowjfno();
	else
		DShowJfnoButton();


	m_CB_DShowJfno.EnableWindow(blEnable);

	if(imModus == DLG_CHANGE)
	{
		m_CB_DDaly.EnableWindow(FALSE);
		m_CE_DDays.EnableWindow(FALSE);
	}
	else
	{
		m_CB_DDaly.EnableWindow(blEnable);
		m_CE_DDays.EnableWindow(blEnable);
	}



	if(( CString(prmDFlight->Ftyp)	== "Z" ) || (CString(prmDFlight->Ftyp)	== "B" ))
	{
		m_CE_DDes3.EnableWindow(FALSE);	
		m_CE_DDes4.EnableWindow(FALSE);	
		m_CB_DDes3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DDes3.EnableWindow(blEnable);	
		m_CE_DDes4.EnableWindow(blEnable);	
		m_CB_DDes3List.EnableWindow(blEnable);	
	}


	if(ogFpeData.bmFlightPermitsEnabled && pomDepPermitsButton != NULL)
		pomDepPermitsButton->ShowWindow(blEnable ? SW_SHOW : SW_HIDE);


	if (prmDFlight->Urno > 0) 
	{
		EnableDepBelts(true);
	}

	
	m_DPay.ShowWindow(SW_HIDE);
	if(bgShowPayDetail)
	{
		if(imModus == DLG_NEW)
		{
			m_DPay.ShowWindow(SW_SHOW);
			//m_DPay.ShowWindow(SW_SHOW);

			
		}
		else
		{
			CString olSelection;
			olSelection.Format("WHERE URNO='%d'",prmDFlight->Urno);

			CString olDummyTableName="AFT";
			ogBCD.SetObject(olDummyTableName, "URNO,RREQ,MOPA");
			ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
			ogBCD.Read(olDummyTableName,olSelection);

				
			int ndxRreg = ogBCD.GetFieldIndex( olDummyTableName, "RREQ" );	
			int ndxMopa = ogBCD.GetFieldIndex( olDummyTableName, "MOPA" );
			int nItems = ogBCD.GetDataCount( olDummyTableName );


			CString olARreg,olAMopa;
			for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
			{
				RecordSet record ;
				if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
				{
					olARreg = record[ ndxRreg ];
					olAMopa = record[ ndxMopa ];
				}
			}		

			if(olAMopa=="B" || olARreg=="1")
			{
				m_DPay.ShowWindow(SW_SHOW);
			}
		
		}
	}

	if(bgShowCashButtonInBothFlight)
	{
		if(imModus == DLG_NEW)
		{
			m_CashArrBtn->ShowWindow(SW_HIDE);
			m_CashDepBtn->ShowWindow(SW_HIDE);
		}
		else
		{
			m_CashArrBtn->ShowWindow(SW_HIDE);
			m_CashArrBtn->EnableWindow(blEnable);

			m_CashDepBtn->ShowWindow(SW_SHOW);
			m_CashDepBtn->EnableWindow(blEnable);
		}
		
	}

	CCS_CATCH_ALL

}

CTime CSeasonDlg::ConvertFlightPermitTime(CTime opTime)
{
	if(bmLocalTime)
		CedaAptLocalUtc::AptLocalToUtc(opTime, CString(pcgHome4));
	return opTime;
}

void CSeasonDlg::onArrPermitsButton() 
{
	ogFpeData.StartFlightPermitsAppl(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns);
}

void CSeasonDlg::onDepPermitsButton() 
{
	ogFpeData.StartFlightPermitsAppl(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns);
}

void CSeasonDlg::ReadCcaData()
{
	if (prmDFlight->Urno == 0)
		return;

	CString olWhere;
	olWhere.Format("FLNU = %d", prmDFlight->Urno);
	omCcaData.ReadSpecial(olWhere);
	
	CCAFLIGHTDATA rlCcaFlight;
/*
	if(bmLocalTime) ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
	rlCcaFlight = *prmDFlight;
 	if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
*/
	if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
	rlCcaFlight = *prmDFlight;
	if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
}



void CSeasonDlg::GetCcas()
{

	if(prmDFlight->Urno <= 0)
		return;


	// copy ccas to member array
	omDCins.DeleteAll();
	for(int i = 0; i < omCcaData.omData.GetSize(); i++)
	{
		omDCins.New(omCcaData.omData[i]);
		if(imModus == DLG_COPY)
		{
			omDCins[i].Urno = 0;
			omDCins[i].Flnu = 0;
		}
	}
	omDCins.Sort(CompareCcas);	


	// create 'save'-records
	omDCinsSave.DeleteAll();
	omOldDCins.DeleteAll();
	omOldCcaValues.DeleteAll();
	for(i = 0; i < omDCins.GetSize(); i++)
	{
		omOldDCins.New(omDCins[i]);
		omOldCcaValues.New(omDCins[i]);

		omDCinsSave.New(omDCins[i]);
	}


}




void CSeasonDlg::DShowCinsTable()
{

	CCS_TRY

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;
	CCSEDIT_ATTRIB rlAttribC5;
	CCSEDIT_ATTRIB rlAttribC6;//Added by Christine

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;
	
	
	pomDCinsTable->ResetContent();


	if(ogPrivList.GetStat("SEASONDLG_CE_DCins") != '1')
		rlColumnData.BkColor = RGB(192,192,192);


	if (!bgCnamAtr)
	{
		rlAttribC1.Style = ES_UPPERCASE;


		rlAttribC2.Format = "X";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 1;
		rlAttribC2.Style = ES_UPPERCASE;

		rlAttribC3.Type = KT_TIME;
		rlAttribC3.ChangeDay	= true;
		rlAttribC3.TextMaxLenght = 7;

		rlAttribC4.Type = KT_TIME;
		rlAttribC4.ChangeDay	= true;
		rlAttribC4.TextMaxLenght = 7;

		
		rlAttribC5.Type = KT_STRING;
		rlAttribC6.Type = KT_STRING;
		
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.Alignment = COLALIGN_LEFT;
/*
//workaround
//		CString olCkicUrnos ("86384929 86382934");
		CString olCkicUrnos (""); 
		char buffer[32];
		for(int i = omDCins.GetSize() -1; i >= 0; i--)
		{
			CCADATA *prlCca = &omDCins[i];

			ltoa(prlCca->Urno, buffer, 10);
			CString olCkic = buffer;
			if (olCkicUrnos.Find(olCkic) == -1)
			{
				olCkicUrnos += ltoa(prlCca->Urno,buffer, 10) + CString(",") ;
			}
			else
			{
				omDCins.RemoveAt(i);
			}
		}
//workaround
*/

		CTime olTmpTime;
		for (ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDCins[ilLc].Ckic;
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDCins[ilLc].Ckit;
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			olTmpTime = omDCins[ilLc].Ckbs;
			if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
			rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat); //omDCins[ilLc].Ckbs.Format("%H:%M");
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			olTmpTime = omDCins[ilLc].Ckes;
			if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
			rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat);//omDCins[ilLc].Ckes.Format("%H:%M");
			rlColumnData.EditAttrib = rlAttribC4;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			if(bgShowClassInBatchDlg)
			{
				rlColumnData.Text = omDCins[ilLc].Disp;
				rlColumnData.EditAttrib = rlAttribC5;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
			}

			pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		for (ilLc = omDCins.GetSize(); ilLc < 30; ilLc++)
		{
			TRACE("ilLc=%d" , ilLc);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC4;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			if(bgShowClassInBatchDlg)
			{
				rlColumnData.Text = "";
				rlColumnData.EditAttrib = rlAttribC5;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
			}

			pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
	}
	else
	{
		rlAttribC1.Style = ES_UPPERCASE;
		rlAttribC2.Style = ES_UPPERCASE;

		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;

		rlAttribC4.Type = KT_TIME;
		rlAttribC4.ChangeDay	= true;
		rlAttribC4.TextMaxLenght = 7;

		rlAttribC5.Type = KT_TIME;
		rlAttribC5.ChangeDay	= true;
		rlAttribC5.TextMaxLenght = 7;

		rlAttribC6.Type = KT_STRING;


		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.Alignment = COLALIGN_LEFT;

		CTime olTmpTime;

		for (ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDCins[ilLc].Ckic;
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			CString olAtr("");
			CString olnam = omDCins[ilLc].Ckic;
			ogBCD.GetField("CIC", "CNAM", omDCins[ilLc].Ckic, "CATR", olAtr );
			rlColumnData.Text = olAtr;
			rlColumnData.EditAttrib = rlAttribC2;
			rlColumnData.blIsEditable = false;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = omDCins[ilLc].Ckit;
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.blIsEditable = true;

			olTmpTime = omDCins[ilLc].Ckbs;
			if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
			rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat); //omDCins[ilLc].Ckbs.Format("%H:%M");
			rlColumnData.EditAttrib = rlAttribC4;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			olTmpTime = omDCins[ilLc].Ckes;
			if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
			rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat);//omDCins[ilLc].Ckes.Format("%H:%M");
			rlColumnData.EditAttrib = rlAttribC5;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			if(bgShowClassInBatchDlg)
			{
				rlColumnData.Text = omDCins[ilLc].Disp;
				rlColumnData.EditAttrib = rlAttribC6;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
			}

			pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		for (ilLc = omDCins.GetSize(); ilLc < 30; ilLc++)
		{
			TRACE("ilLc=%d" , ilLc);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC4;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC5;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			if(bgShowClassInBatchDlg)
			{
				rlColumnData.Text = "";
				rlColumnData.EditAttrib = rlAttribC6;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
			}
			pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
	}

	pomDCinsTable->DisplayTable();

	CCS_CATCH_ALL

}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Ankunft
//
void CSeasonDlg::OnAshowjfno()  
{

	CCS_TRY

	if(!bmIsAFfnoShown)
	{
		char clStat = ogPrivList.GetStat("SEASONDLG_CB_AShowJfno");

		m_CB_AShowJfno.ShowWindow(SW_HIDE);
		m_CE_AJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_AAlc3List2.ShowWindow(SW_SHOW);
		m_CB_AAlc3List3.ShowWindow(SW_SHOW);
	    pomAJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno"),pomAJfnoTable);
		
		if(clStat == '1')
			pomAJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsAFfnoShown = true;

	CCS_CATCH_ALL

}


void CSeasonDlg::AShowJfnoButton() 
{

	CCS_TRY

	m_CB_AShowJfno.ShowWindow(SW_SHOW);
	m_CE_AJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_AAlc3List2.ShowWindow(SW_HIDE);
	m_CB_AAlc3List3.ShowWindow(SW_HIDE);
    pomAJfnoTable->ShowWindow(SW_HIDE);
	bmIsAFfnoShown = false;

	CCS_CATCH_ALL

}





///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Abflug
//
void CSeasonDlg::OnDshowjfno() 
{

	CCS_TRY

	if(!bmIsDFfnoShown)
	{
		char clStat = ogPrivList.GetStat("SEASONDLG_CB_DShowJfno");
		m_CB_DShowJfno.ShowWindow(SW_HIDE);

		m_CE_DJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_DAlc3List2.ShowWindow(SW_SHOW);
		m_CB_DAlc3List3.ShowWindow(SW_SHOW);
	    pomDJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno"),pomDJfnoTable);
		if(clStat == '1')
			pomDJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsDFfnoShown = true;

	CCS_CATCH_ALL

}


void CSeasonDlg::DShowJfnoButton() 
{

	CCS_TRY

	m_CB_DShowJfno.ShowWindow(SW_SHOW);

	m_CE_DJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_DAlc3List2.ShowWindow(SW_HIDE);
	m_CB_DAlc3List3.ShowWindow(SW_HIDE);

    pomDJfnoTable->ShowWindow(SW_HIDE);
	bmIsDFfnoShown = false;

	CCS_CATCH_ALL

}




void CSeasonDlg::ProcessCCA(CCADATA *prpCca, int ipDDXType)
{

	CCS_TRY

	if((prmDFlight->Urno <= 0) || (imModus != DLG_CHANGE))
		return;

	if (ipDDXType == CCA_SEADLG_NEW || ipDDXType == CCA_SEADLG_DELETE)
	{
		if (ipDDXType == CCA_SEADLG_DELETE)
		{
			// delete the record in omCcaData at first
			omCcaData.DeleteInternal(prpCca, false);

		}

		CCAFLIGHTDATA rlCcaFlight;
/*
		if(bmLocalTime) ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
		rlCcaFlight = *prmDFlight;
 		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
*/
		if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
		rlCcaFlight = *prmDFlight;
		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

		omCcaData.CheckDemands(&rlCcaFlight);
	}


	GetCcas();
	DShowCinsTable();

	CCS_CATCH_ALL

}



void CSeasonDlg::SetWndPos()
{

	CCS_TRY

	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONROTDLG_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = ( (bottom - (olRect.bottom -olRect.top ) ) / 2);  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}



	if (bgRotationDialogsTopmost) // quick fix for UFIS 1540
		SetWindowPos(&wndTopMost , 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
	else
		SetWindowPos(&wndTop , 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);

	// SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);

	CCS_CATCH_ALL

}





void CSeasonDlg::OnSelchangeAremp() 
{
	OnEditChanged(0,0);	
	
}

void CSeasonDlg::OnSelchangeDremp() 
{
	OnEditChanged(0,0);	
	
}


LRESULT CSeasonDlg::OnEditRButtonDown(UINT wParam, LPARAM lParam) 
{
	// Insert current time in baggage-belt begin- and end-editbox
 	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *) lParam;
 	CTime olTime = CTime::GetCurrentTime();
	if (!bmLocalTime)
	{
		ogBasicData.LocalToUtc(olTime);
	}

	if (prlNotify->SourceControl == &m_CE_AB1bs)
	{
 		m_CE_AB1bs.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB1es)
	{
 		m_CE_AB1es.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2bs)
	{
 		m_CE_AB2bs.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2es)
	{
 		m_CE_AB2es.SetWindowText(DateToHourDivString(olTime, olTime));
	}

	return 0L;
}

BOOL CSeasonDlg::HandlePostFlight()
{
	// postflight: no changes allowed
	if (CheckPostFlight())
	{
		if (GetDlgItem(IDOK)) 
			m_CB_Ok.EnableWindow(FALSE);

		if(bgShowCashButtonInBothFlight)
		{
			m_CashArrBtn->EnableWindow(FALSE);
			m_CashDepBtn->EnableWindow(FALSE);
		}
		
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		return TRUE;
	}

	return FALSE;
}

BOOL CSeasonDlg::CheckPostFlight()
{
	BOOL blPost = FALSE;

	CString olAPrfl = prmAFlight->Prfl;
	CString olDPrfl = prmDFlight->Prfl;

	if (bgCheckPRFL)
	{
		if(ogPrivList.GetStat("SEASONDLG_CE_APrfl") == '1')
			olAPrfl = " ";

		if(ogPrivList.GetStat("SEASONDLG_CE_DPrfl") == '1')
			olDPrfl = " ";
	}


	if (prmAFlight && prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,olAPrfl) && IsPostFlight(prmDFlight->Tifd,bmLocalTime,olDPrfl))
				blPost = TRUE;
		}
		else if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime,olDPrfl))
				blPost = TRUE;
		}
		else if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,olAPrfl))
				blPost = TRUE;
		}
	}
	else if (!prmAFlight && prmDFlight)
	{
		if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime,olDPrfl))
				blPost = TRUE;
		}
	}
	else if (prmAFlight && !prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,olAPrfl))
				blPost = TRUE;
		}
	}

	if (blPost)
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
	else
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

	return blPost;
}




bool CSeasonDlg::AddBltCheck()
{
	CString olWarningText;
	bool blWarning = false;
	CString olAddText;

	if (!bgExtBltCheck)
		return true;

	// only if arrival flight present
	if (prmAFlight->Urno > 0) {


//prf 8360 In season - and rotationdiaolgthe input of different belt types shall be possible. 
	/*
	//// check the belt type of the two baggage belts
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		CString olBlt1Type;
		CString olBlt2Type;

		// get both belt types
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt1, "BLTT", olBlt1Type);
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt2, "BLTT", olBlt2Type);

		if (!olBlt1Type.IsEmpty() && !olBlt2Type.IsEmpty() &&
			olBlt1Type != olBlt2Type)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2045) + CString("\n\n") + GetString(IDS_STRING2046), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
			return false;
		}
	}
	*/


	// only proceed if the baggage belt allocation values or STA, ETA has been changed!
/*	if (!m_CE_ABlt1.IsChanged() && !m_CE_ATmb1.IsChanged() && !m_CE_AB1bs.IsChanged() && !m_CE_AB1es.IsChanged() &&
		!m_CE_ABlt2.IsChanged() && !m_CE_ATmb2.IsChanged() && !m_CE_AB2bs.IsChanged() && !m_CE_AB2es.IsChanged() &&
		!m_CE_AStoa.IsChanged() && !m_CE_AEtai.IsChanged())
		return true;
*/
		if (m_CE_ABlt1.IsChanged() || m_CE_ATmb1.IsChanged() || m_CE_AB1bs.IsChanged() || m_CE_AB1es.IsChanged() ||
		m_CE_ABlt2.IsChanged() || m_CE_ATmb2.IsChanged() || m_CE_AB2bs.IsChanged() || m_CE_AB2es.IsChanged() ||
		m_CE_AStoa.IsChanged() || m_CE_AEtai.IsChanged() )
		{


	//// check opening time of belt 1
	if (!prmAFlight->CheckBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 1
	if (!prmAFlight->CheckBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check opening time of belt 2
	if (!prmAFlight->CheckBltOpeningTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2034), olAddText);

		olWarningText += olTmp;
 		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 2
	if (!prmAFlight->CheckBltClosingTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2036), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check if two belts are allocated
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		olWarningText += GetString(IDS_STRING2043);
		olWarningText += '\n';
		blWarning = true;
	}
	}
	}
	if (bgUseDepBelts && prmDFlight->Urno > 0) {
	// only proceed if the baggage belt allocation values or STD, ETD has been changed!
	if (m_CE_DBlt1.IsChanged() || m_CE_DTmb1.IsChanged() || m_CE_DB1bs.IsChanged() || m_CE_DB1es.IsChanged() ||
		m_CE_DStod.IsChanged() || m_CE_DEtdi.IsChanged() )
	{

	//// check opening time of belt 1
	if (!prmDFlight->CheckDepBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}

	
	//// check closing time of belt 1
	if (!prmDFlight->CheckDepBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	}
	}


	// Display message box with warnings
	if (blWarning)
	{
		olWarningText += CString("\n") + GetString(IDS_STRING2037);
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	return true;
}

void CSeasonDlg::CheckViaButton(void)
{
	CString olButtonText;
	COLORREF olButtonColor;

	//arr
	int ilCount = atoi(prmAFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_AVia.SetWindowText(olButtonText);
	m_CB_AVia.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_AVia.UpdateWindow();
	CRect olRect;
	m_CB_AVia.GetWindowRect( olRect );


	//dep
	ilCount = atoi(prmDFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_DVia.SetWindowText(olButtonText);
	m_CB_DVia.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_DVia.UpdateWindow();
	
	return; 
}

void CSeasonDlg::CheckCodeShareButton(void) 
{
	CString olButtonText;
	COLORREF olButtonColor;

	//arr
	int ilCount = omAJfno.GetSize();
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_CODE_SHARE), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_CODE_SHARE), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_AShowJfno.SetWindowText(olButtonText);
	m_CB_AShowJfno.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_AShowJfno.UpdateWindow();


	//dep
	ilCount = omDJfno.GetSize();
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_CODE_SHARE), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_CODE_SHARE), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_DShowJfno.SetWindowText(olButtonText);
	m_CB_DShowJfno.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_DShowJfno.UpdateWindow();
	
	return; 
}

bool CSeasonDlg::GetToolTips(CPoint opPoint,TOOLINFO *pTI, CWnd* opWnd, UINT ID, CString opToolTip) const
{
	if(!opWnd || !pTI)
		return false;

	CRect olRect;
	opWnd->GetWindowRect(olRect);
	this->ScreenToClient(olRect);
	if (olRect.PtInRect(opPoint))
	{
		CString olToolTip = GetString(ID);

		if (!opToolTip.IsEmpty())
			olToolTip += opToolTip;

		// fill tooltip structure
		return FillToolTip(olToolTip, pTI, opWnd);
	}

	return false;
}

bool CSeasonDlg::GetToolTips(CPoint opPoint,TOOLINFO *pTI, CWnd* opWnd, UINT ID, CString& opTab, CString& opFields, CString& opField) const
{
	if(!opWnd || !pTI)
		return false;

	CRect olRect;
	opWnd->GetWindowRect(olRect);
	this->ScreenToClient(olRect);
	if (olRect.PtInRect(opPoint))
	{
		CString olToolTip = GetString(ID);

		CString olEntry;
		opWnd->GetWindowText(olEntry);
		if (!olEntry.IsEmpty())
		{
			CString olTmp;
			ogBCD.GetField(opTab, opField, olEntry, opFields, olTmp );
			olToolTip += olTmp;
		}

		// fill tooltip structure
		return FillToolTip(olToolTip, pTI, opWnd);
	}

	return false;
}

bool CSeasonDlg::GetToolTips(CPoint opPoint,TOOLINFO *pTI, CWnd* opWnd, UINT ID, CString& opTab, CString& opFields, CString& opPreSyntax, CString& opField, CString& opEntry) const
{
	if(!opWnd || !pTI)
		return false;

	CRect olRect;
	opWnd->GetWindowRect(olRect);
	this->ScreenToClient(olRect);
	if (olRect.PtInRect(opPoint))
	{
		CStringArray olStrArrayFields;
		CStringArray olStrArrayPre;
		int ilAnzFields = ExtractItemList(opFields,    &olStrArrayFields, ',');
		int ilAnzPre    = ExtractItemList(opPreSyntax, &olStrArrayPre,    '#');
		ASSERT(ilAnzFields == ilAnzPre);

		CString olToolTip = GetString(ID);

		if (opEntry.IsEmpty())
			opWnd->GetWindowText(opEntry);

		if (!opEntry.IsEmpty())
		{
			for (int i=0; i<ilAnzFields; i++)
			{
				CString olTmp;
				ogBCD.GetField(opTab, opField, opEntry, olStrArrayFields.GetAt(i), olTmp);
				if (!olTmp.IsEmpty() && i < ilAnzPre)
					olToolTip += olStrArrayPre.GetAt(i) + olTmp;
			}
		}

		// fill tooltip structure
		return FillToolTip(olToolTip, pTI, opWnd);
	}

	return false;
}

bool CSeasonDlg::GetToolTipsCodeShare(CPoint opPoint,TOOLINFO *pTI, CWnd* opWnd, UINT ID, CString opJfno) const
{
	if(!opWnd || !pTI)
		return false;

	CRect olRect;
	opWnd->GetWindowRect(olRect);
	this->ScreenToClient(olRect);
	if (olRect.PtInRect(opPoint))
	{
		CString olToolTip = GetString(ID);

		if (!opJfno.IsEmpty())
		{
			CString olJfno (opJfno);
			while(!olJfno.IsEmpty())
			{
				if (olJfno.GetLength() < 9)
				{
					int anz = 9 - olJfno.GetLength();
					for (int i=0; i<anz; i++) 
						olJfno.Insert(10000, ' ');
				}

				CString olAlc3 = olJfno.Left(3);
				CString olFltn = olJfno.Mid(3,5);
				CString olFlns = olJfno.Mid(8,1);
				olAlc3.TrimLeft();
				olFlns.TrimLeft();
				olFltn.TrimLeft();
				olJfno = olJfno.Right(olJfno.GetLength() - 9);
				CString olTmp = olAlc3 + olFltn + olFlns + " / ";
				olToolTip += olTmp;
			}
		}

		// fill tooltip structure
		return FillToolTip(olToolTip, pTI, opWnd);
	}

	return false;
}

bool CSeasonDlg::FillToolTip(CString& opToolTip, TOOLINFO *pTI, CWnd* opWnd) const
{
	char *olpChar = new char [256];
	sprintf(olpChar, opToolTip);
	pTI->cbSize = sizeof(*pTI);
	pTI->uFlags = TTF_IDISHWND;
	pTI->hwnd   = m_hWnd;
	pTI->uId    = (UINT) opWnd->m_hWnd;
	pTI->lpszText = olpChar;

	return true;
}

int CSeasonDlg::OnToolHitTest(CPoint opPoint,TOOLINFO *pTI) const
{
	CString olT ("#");

	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACHT3), IDS_PRFL) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCHT3), IDS_PRFL) ) 
		return 1;

//pure discribtion
	//buttonlist
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DAILY_MASK), TP_DAILYMASK) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGENTS), TP_AGENTS) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDOK), TP_OK) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDCANCEL), TP_CANCEL) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ARESET), TP_RESET_ARR) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DRESET), TP_RESET_DEP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_PREVFLIGHT), TP_PREV) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_NEXTFLIGHT), TP_NEXT) )
		return 1;


	//flightnumber
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AFLTN), TP_FLTN) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AFLNS), TP_FLNS) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AFLTI), TP_FLTI) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACSGN), TP_CSGN) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DFLTN), TP_FLTN) ) 
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DFLNS), TP_FLNS) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DFLTI), TP_FLTI) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCSGN), TP_CSGN) )
		return 1;

	//times for flight
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTOD), TP_STOD) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTOD), TP_STOD) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTOA), TP_STOA) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTOA), TP_STOA) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AETAI), TP_ETA) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DETDI), TP_ETD) )
		return 1;

	//times allocation
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_APABS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGA1B), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGA2B), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AB1BS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AB2BS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DPDBS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGD1B), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGD2B), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DW1BS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DW2BS), TP_BEG_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_APAES), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGA1E), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGA2E), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AB1ES), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AB2ES), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DPDES), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGD1E), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGD2E), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DW1ES), TP_END_SCHEDULE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DW2ES), TP_END_SCHEDULE) )
		return 1;

	//allocation
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATGA1), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATGA2), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTGD1), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTGD2), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATMB1), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATMB2), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATET1), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATET2), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTWR1), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTWR2), TP_TERMINAL) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AEXT1), TP_ALLOC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AEXT2), TP_ALLOC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DBAZ1), TP_ALLOC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DBAZ4), TP_ALLOC) )
		return 1;
	//cic
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCINSMAL4), TP_4CIC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_CIFR_CITO), TP_CIFRTO) )
		return 1;

	//state
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACXX), TP_CXX) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCXX), TP_CXX) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ANOOP), TP_NOOP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DNOOP), TP_NOOP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTATUS), TP_STAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTATUS2), TP_STAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTATUS3), TP_STAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTATUS), TP_STAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTATUS2), TP_STAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTATUS3), TP_STAT) )
		return 1;

	//trafficdays
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ADALY), TP_DAILY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DDALY), TP_DAILY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ADAYS), TP_TR_DAYS) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DDAYS), TP_TR_DAYS) )
		return 1;

	//others
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTEV), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTE2), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTE3), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTE4), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTEV), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTE2), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTE3), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTE4), TP_KEY) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AREMP), TP_REMP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DREMP), TP_REMP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_MING), TP_MING) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_NOSE), TP_NOSE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_FREQ), TP_FREQ) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_PVOM), TP_PVOM) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_PBIS), TP_PBIS) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_SEAS), TP_SEAS) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACREATED), TP_CREATED) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCREATED), TP_CREATED) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ALASTCHANGE), TP_LASTCHANGE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DLASTCHANGE), TP_LASTCHANGE) )
		return 1;

//Listing of tables
	//Apt
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AORG3LIST), TP_APT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DDES3LIST), TP_APT) )
		return 1;

	//Alc
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AAlc3LIST), TP_ALC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AAlc3LIST2), TP_ALC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AAlc3LIST3), TP_ALC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DALC3LIST), TP_ALC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DALC3LIST2), TP_ALC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DALC3LIST3), TP_ALC) )
		return 1;

	//allocatios
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_APSTALIST), TP_PST) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DPSTDLIST), TP_PST) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGTD1LIST), TP_GAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGTD2LIST), TP_GAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGTA1LIST), TP_GAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGTA2LIST), TP_GAT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ABLT1LIST), TP_BLT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ABLT2LIST), TP_BLT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AEXT1LIST), TP_EXT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AEXT2LIST), TP_EXT) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DWRO1LIST), TP_WRO) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DWRO2LIST), TP_WRO) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCINSLIST1), TP_CIC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCINSLIST2), TP_CIC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCINSLIST3), TP_CIC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DCINSLIST4), TP_CIC) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATTYPLIST), TP_NATURE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTTYPLIST), TP_NATURE) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTYPLIST), TP_STEV) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTYPLIST), TP_STEV) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACT35LIST), TP_ACT) )
		return 1;

//discrition with more information
	//airports
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ADES3), TP_HOME, CString("APT"), CString("APFN"), CString("APC3")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DORG3), TP_HOME, CString("APT"), CString("APFN"), CString("APC3")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_AORG3), TP_ORG3, CString("APT"), CString("APFN"), CString("APC3")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_AORG4), TP_ORG4, CString("APT"), CString("APFN"), CString("APC4")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DDES3), TP_DES3, CString("APT"), CString("APFN"), CString("APC3")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DDES4), TP_DES4, CString("APT"), CString("APFN"), CString("APC4")) )
		return 1;

	//nature
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ATTYP), TP_NAT, CString("NAT"), CString("TNAM"), CString("TTYP")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DTTYP), TP_NAT, CString("NAT"), CString("TNAM"), CString("TTYP")) )
		return 1;

	//servie
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASTYP), TP_STY, CString("STY"), CString("SNAM"), CString("STYP")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSTYP), TP_STY, CString("STY"), CString("SNAM"), CString("STYP")) )
		return 1;

	//allocations
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_APSTA), TP_ALLOC_DEFD, CString("PST"), CString("DEFD"), CString("PNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DPSTD), TP_ALLOC_DEFD, CString("PST"), CString("DEFD"), CString("PNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGTA1), TP_ALLOC_DEFD, CString("GAT"), CString("DEFD"), CString("GNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_AGTA2), TP_ALLOC_DEFD, CString("GAT"), CString("DEFD"), CString("GNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGTD1), TP_ALLOC_DEFD, CString("GAT"), CString("DEFD"), CString("GNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DGTD2), TP_ALLOC_DEFD, CString("GAT"), CString("DEFD"), CString("GNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ABLT1), TP_ALLOC_DEFD, CString("BLT"), CString("DEFD"), CString("BNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ABLT2), TP_ALLOC_DEFD, CString("BLT"), CString("DEFD"), CString("BNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DWRO1), TP_ALLOC_DEFD, CString("WRO"), CString("DEFD"), CString("WNAM")) )
		return 1;
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DWRO2), TP_ALLOC_DEFD, CString("WRO"), CString("DEFD"), CString("WNAM")) )
		return 1;

	//Airline
	CString olEntry;
	m_CE_AAlc3.GetWindowText(olEntry);
	if (olEntry.GetLength() != 3)
		ogBCD.GetField("ALT", "ALC2", olEntry, "ALC3", olEntry);

	CString olPreTips = CString("") + olT + CString(" / ") + olT + CString(" - ") + olT + GetString(TP_CASH);
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_AALC3), TP_AL, CString("ALT"), CString("ALC2,ALC3,ALFN,CASH"), olPreTips, CString("ALC3"), olEntry) )
		return 1;

	m_CE_DAlc3.GetWindowText(olEntry);
	if (olEntry.GetLength() != 3)
		ogBCD.GetField("ALT", "ALC2", olEntry, "ALC3", olEntry);

	olPreTips = CString("") + olT + CString(" / ") + olT + CString(" - ") + olT + GetString(TP_CASH);
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DALC3), TP_AL, CString("ALT"), CString("ALC2,ALC3,ALFN,CASH"), olPreTips, CString("ALC3"), olEntry) )
		return 1;

	//act
	olEntry.Empty();
	olPreTips = CString("") + olT + GetString(TP_WS) + olT + GetString(TP_HE) + olT + GetString(TP_LE);
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACT3), TP_ACT3, CString("ACT"), CString("ACFN,ACWS,ACHE,ACLE"), olPreTips, CString("ACT3"), olEntry) )
		return 1;

	olEntry.Empty();
	olPreTips = CString("") + olT + GetString(TP_WS) + olT + GetString(TP_HE) + olT + GetString(TP_LE);
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ACT5), TP_ACT5, CString("ACT"), CString("ACFN,ACWS,ACHE,ACLE"), olPreTips, CString("ACT5"), olEntry) )
		return 1;

	//regn
	olEntry.Empty();
	olPreTips = GetString(TP_IATA) + olT + GetString(TP_ICAO);
	if (GetToolTips(opPoint, pTI, (CWnd*) GetDlgItem(IDC_REGN), TP_REGN, CString("ACR"), CString("ACT3,ACT5"), olPreTips, CString("REGN"), olEntry) )
		return 1;

//code share
	if (GetToolTipsCodeShare(opPoint, pTI, (CWnd*) GetDlgItem(IDC_ASHOWJFNO), TP_CODE_SHARE, CString(prmAFlight->Jfno)) )
		return 1;
	if (GetToolTipsCodeShare(opPoint, pTI, (CWnd*) GetDlgItem(IDC_DSHOWJFNO), TP_CODE_SHARE, CString(prmDFlight->Jfno)) )
		return 1;

//via
	CString olToolTip;
	if (polRotationAViaDlg)
		olToolTip = polRotationAViaDlg->GetToolTip();
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_AVIA), TP_VIA, olToolTip) )
		return 1;

	olToolTip.Empty();
	if (polRotationDViaDlg)
		olToolTip = polRotationDViaDlg->GetToolTip();
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_DVIA), TP_VIA, olToolTip) )
		return 1;

//general, don#t aktivate
/*
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_STATIC_ANKUNFT), TP_ARR) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_STATIC_ABFLUG), TP_DEP) )
		return 1;
	if (GetToolTips (opPoint, pTI, (CWnd*) GetDlgItem(IDC_STATIC_ROT), TP_ROT) )
		return 1;
*/


	if(ogFpeData.bmFlightPermitsEnabled)
	{
		CWnd *olpWnd = (CWnd*) GetDlgItem(IDC_ARRPERMITSBUTTON);
		CRect olRect;
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "";
			char *olpChar = new char [256];
			olToolTip = omArrFlightPermitInfo;
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;
		}

		olpWnd = (CWnd*) GetDlgItem(IDC_DEPPERMITSBUTTON);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "";
			char *olpChar = new char [256];
			olToolTip = omDepFlightPermitInfo;
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;
		}
	}

	return -1;
}

BOOL CSeasonDlg::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        // idFrom is actually the HWND of the tool
        nID = ::GetDlgCtrlID((HWND)nID);
        if(nID)
        {
            pTTT->lpszText = MAKEINTRESOURCE(nID);
            pTTT->hinst = AfxGetResourceHandle();
            return(TRUE);
        }
    }
    return(FALSE);
}

LRESULT CSeasonDlg::OnEditDbClk( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if (prlNotify->Name.IsEmpty())
		return 0L;

	CallHistory(prlNotify->Name);

	CCS_CATCH_ALL	

	return 0L;
}

void CSeasonDlg::CallHistory(CString opField)
{
	if (opField.IsEmpty())
		return;

	CString olTable = opField.Left(3);
	CString olField = opField.Mid(4,4);
	CString olAdid = opField.Mid(3,1);
	CString olType = opField.Right(1);

	//----------
	char clStat;
	clStat = ogPrivList.GetStat("SEASONDLG_CL_AHistory");

	if(olAdid == "A")
	{
		if(clStat != '1')
			return;
	}

	clStat = ogPrivList.GetStat("SEASONDLG_CL_DHistory"); 

	if(olAdid == "D")
	{
		if(clStat != '1')
			return;
	}

	clStat = ogPrivList.GetStat("SEASONDLG_CL_RHistory"); 

	if(olAdid == "R")
	{
		if(clStat != '1')
			return;
	}
	//----------


	char Abuffer[20];
	char Dbuffer[20];


	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "DATA_CHANGES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		MessageBox(GetString(IDS_DATACHANGES), GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}
	char slRunTxt[512] = ""; 

	CString olTimeMode = "U";
	if (bmLocalTime)
		olTimeMode = "L";

	if (!bgUTCCONVERTIONS)
		olTimeMode = "L";


	CString olTowingUrnos = "0";
	if(olAdid == "A" || olAdid == "D")
	{
		char pclSelection[256];
		char pclUrno[12];

		//if(olField.CompareNoCase("XXXX") == 0) //Meant for "All Fields" history
		{			
			RotGDlgCedaFlightData olGDlgCedaFlightData;
			if (olAdid == "D")
				sprintf(pclSelection, "WHERE RKEY = %ld AND FTYP = '%c'", olGDlgCedaFlightData.GetRkey(prmDFlight->Urno), 'T');
			else
				sprintf(pclSelection, "WHERE RKEY = %ld AND FTYP = '%c'", olGDlgCedaFlightData.GetRkey(prmAFlight->Urno), 'T');

			CCSPtrArray<ROTGDLGFLIGHTDATA> olRotGDlgFlightData;
			if(olGDlgCedaFlightData.ReadSpecial(&olRotGDlgFlightData,pclSelection,"URNO,FTYP,RKEY",true) == true)
			{
				olTowingUrnos.Empty();
				for(int i = 0 ; i < olRotGDlgFlightData.GetSize(); i++)
				{
					ROTGDLGFLIGHTDATA* prpFlight = &olRotGDlgFlightData[i];
					olTowingUrnos += CString(itoa(prpFlight->Urno,pclUrno,10)) + (i < (olRotGDlgFlightData.GetSize() - 1) ? ";" : "");
				}
				if(olAdid != "A")
				{
					if(olTowingUrnos.GetLength() > 1)
					{
						olTowingUrnos += ";" + CString(ltoa(prmDFlight->Urno,Dbuffer,10));
					}
					else
					{
						 olTowingUrnos = CString(ltoa(prmDFlight->Urno,Dbuffer,10));
					}
				}				
			}
		}
	}


	if (olAdid == "A")
	{
		sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), olTowingUrnos, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
	}
	else
	{
		if (olAdid == "D")
		{
			if (strcmp(pcgHome, "WAW") == 0 && olTowingUrnos != "0")
				sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", olTowingUrnos, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
			else
				sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", ltoa(prmDFlight->Urno,Dbuffer,10), olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
		}
		else
		{
			sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), olTowingUrnos/*ltoa(prmDFlight->Urno,Dbuffer,10)*/, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
		}
	}


	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

/*
void CSeasonDlg::OnEditDbClk( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	if((UINT)m_CE_DPstd.imID == wParam)
	{
		CString olText;
		m_CE_DPstd.GetWindowText(olText);

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
		polDlg->SetSecState("SEASONDLG_CE_DPstd");
		if(polDlg->DoModal() == IDOK)
		{
			m_CE_DPstd.SetInitText(polDlg->GetField("PNAM"), true);	
			m_CE_DPstd.SetFocus();
		}
		delete polDlg;

		return;
	}
	

	CCS_CATCH_ALL	

	return;
}
*/



void CSeasonDlg::OnDNfes()
{
	if(m_CB_DNfes.GetCheck())
	{
		m_CE_DNfes.ShowWindow(SW_SHOW);
		m_CE_DNfes.EnableWindow(TRUE);
	}
	else
	{
		m_CE_DNfes.ShowWindow(SW_HIDE);
		m_CE_DNfes.EnableWindow(FALSE);
	}
	OnEditChanged( 0,0);

}



void CSeasonDlg::OnANfes()
{
	if(m_CB_ANfes.GetCheck())
	{
		m_CE_ANfes.ShowWindow(SW_SHOW);
		m_CE_ANfes.EnableWindow(TRUE);
	}
	else
	{
		m_CE_ANfes.ShowWindow(SW_HIDE);
		m_CE_ANfes.EnableWindow(FALSE);
	}
	OnEditChanged( 0,0);

}


void CSeasonDlg::OnSelchangeACxxReason() 
{
	OnEditChanged(0,0);	
}

void CSeasonDlg::OnSelchangeDCxxReason() 
{
	OnEditChanged(0,0);	
}

//20110405 Tab Order

BOOL CSeasonDlg::ProcessTab( bool shiftTab )
{
	BOOL processed = FALSE;
	CWnd* lpCurrentItem ;
	CWnd* lpNextItem=NULL;
	lpCurrentItem = this->GetFocus();

	lpNextItem = GetNextTabItem( lpCurrentItem, shiftTab );
	if (lpNextItem !=NULL )
	{
		TRACE("- ProcessTab - before setfocus\n");
		lpNextItem->SetFocus();
		processed = TRUE;
	}
	

	return processed;
}

BOOL CSeasonDlg::PreTranslateMessage(MSG* pMsg)
{

	/*
	switch(pMsg->message) {
	case WM_KEYDOWN:
		if (pMsg->wParam == VK_SHIFT) m_shift = TRUE;
		if (pMsg->wParam == VK_TAB) {
			TRACE("- GOT SHIFT-TAB - PRESSING\n");
			if ( ProcessTab(m_shift) ) return TRUE;
			//return FALSE;
		}
		break;
	case WM_KEYUP:
		if (pMsg->wParam == VK_SHIFT) m_shift = FALSE;
		break;
	}
	*/

	if (NULL != m_pToolTip)
            m_pToolTip->RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}


int CSeasonDlg::AddItemForTabOrder(CWnd* item)
{
	m_tabOrder[m_tabOrderCnt] = item;
	m_tabOrderCnt++;
	return m_tabOrderCnt;																
}

CWnd* CSeasonDlg::GetNextTabItem(CWnd* item, BOOL backward )
{
	CWnd* lItem = NULL;
	int lIdx = 0;
	for ( lIdx=0; lIdx< m_tabOrderCnt; lIdx++)
	{
		if (item==m_tabOrder[lIdx])
		{
			if (backward)
			{
				if (lIdx>0) lItem = m_tabOrder[lIdx-1];
				//else lItem = m_tabOrder[m_tabOrderCnt-1];
			}
			else
			{
				if (lIdx<m_tabOrderCnt-1) lItem = m_tabOrder[lIdx+1];
				//else lItem = m_tabOrder[0];
			}
			break;
		}
	}
	return lItem;
}

void CSeasonDlg::SetLabelForKeyCarousel()
{
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pc[100];
	GetPrivateProfileString( ogAppName, "LABLE_KEYCODE", "", pc, sizeof( pc ) -1, pclConfigPath );
	
	if(strlen(pc) > 0)
	{
		((CStatic*)GetDlgItem(IDC_KEY1))->SetWindowText(pc);
		((CStatic*)GetDlgItem(IDC_KEY2))->SetWindowText(pc);
	}

	char pc2[100];
	GetPrivateProfileString( ogAppName, "LABLE_CAROUSEL", "", pc2, sizeof( pc2 ) -1, pclConfigPath);
	
	
	if(strlen(pc2) > 0)
	{
		((CStatic*)GetDlgItem(IDC_CAR))->SetWindowText(pc2);	
	}
}



void CSeasonDlg::OnAPay() 
{
		
		CModeOfPay dlg  = new CModeOfPay(this,prmAFlight->Urno, "A");
		m_dlgAModeOfPay= &dlg;
		
		
		m_dlgAModeOfPay->SetFlightNo(prmAFlight->Urno);
		m_dlgAModeOfPay->SetFlightType("A");

		
		if(imModus == DLG_NEW)
		{
			m_dlgAModeOfPay->SetMode("Insert");
		}
		else
		{
			m_dlgAModeOfPay->SetMode("Update");
		}
		
		int ret = m_dlgAModeOfPay->DoModal();

		if(imModus == DLG_NEW && ret == IDCANCEL)
		{
			m_dlgAModeOfPay = NULL;
			this->OnCancel();
		}
	
}

void CSeasonDlg::OnDPay() 
{
	
		
	CModeOfPay dlg  = new CModeOfPay(this,prmDFlight->Urno, "D");
	m_dlgDModeOfPay= &dlg;
	
	m_dlgDModeOfPay->SetFlightNo(prmDFlight->Urno);
	m_dlgDModeOfPay->SetFlightType("D");
	if(imModus == DLG_NEW)
	{
		m_dlgDModeOfPay->SetMode("Insert");
	}
	else
	{
		m_dlgDModeOfPay->SetMode("Update");
	}
	int ret = m_dlgDModeOfPay->DoModal();
	if(imModus == DLG_NEW && ret==IDCANCEL)
	{
		m_dlgDModeOfPay = NULL;
		this->OnCancel();
	}
	
}


void CSeasonDlg::SetFixedControls()
{
	CCS_TRY
	CRect olWindowRect;
	CRect olWindowCBRect;
	DWORD checkboxStyle = WS_VISIBLE | BS_AUTOCHECKBOX |WS_CHILD | WS_TABSTOP;

	((CWnd*)GetDlgItem(IDC_APSTALIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFPSA->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFPSA);
	
	
	((CWnd*)GetDlgItem(IDC_AGTA1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGA1->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFGA1);

	
	((CWnd*)GetDlgItem(IDC_AGTA2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGA2->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFGA2);

	((CWnd*)GetDlgItem(IDC_ABLT1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFBL1->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFBL1);

	((CWnd*)GetDlgItem(IDC_ABLT2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFBL2->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFBL2);

	
	((CWnd*)GetDlgItem(IDC_DPSTDLIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFPSD->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFPSD);


	((CWnd*)GetDlgItem(IDC_DGTD1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGD1->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFGD1);

	((CWnd*)GetDlgItem(IDC_DGTD2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGD2->Create(NULL, checkboxStyle,olWindowCBRect,this,IDC_CBFGD2);

	m_pToolTip = new CToolTipCtrl;
	if(!m_pToolTip->Create(this))
	{
	   TRACE("Unable To create ToolTip\n");
	   
	}

    m_pToolTip->AddTool(pomCBFPSA,"Fixed");
	m_pToolTip->AddTool(pomCBFGA1,"Fixed");
	m_pToolTip->AddTool(pomCBFGA2,"Fixed");
	m_pToolTip->AddTool(pomCBFBL1,"Fixed");
	m_pToolTip->AddTool(pomCBFBL2,"Fixed");
	m_pToolTip->AddTool(pomCBFPSD,"Fixed");
	m_pToolTip->AddTool(pomCBFGD1,"Fixed");
	m_pToolTip->AddTool(pomCBFGD2,"Fixed");

   m_pToolTip->Activate(TRUE);

	if(!bgSeasonFixedRes)
	{
		pomCBFPSA->ShowWindow(SW_HIDE);
		pomCBFGA1->ShowWindow(SW_HIDE);
		pomCBFGA2->ShowWindow(SW_HIDE);
		pomCBFBL1->ShowWindow(SW_HIDE);
		pomCBFBL2->ShowWindow(SW_HIDE);
		pomCBFPSD->ShowWindow(SW_HIDE);
		pomCBFGD1->ShowWindow(SW_HIDE);
		pomCBFGD2->ShowWindow(SW_HIDE);
	}

	CCS_CATCH_ALL

}

//For Fixed Allocation - PRF 8405
void CSeasonDlg::OnFixedAllocaton()
{
	CCS_TRY

	OnEditChanged(0,0);
	
	CCS_CATCH_ALL
}


void CSeasonDlg::OnACash() 
{
	if(bgShowCashButtonInBothFlight)
	{
		if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
		{
			ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");
			}
		}
		else
		{
			ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");
			}
		}
	}
	
}

void CSeasonDlg::OnDCash() 
{
	if(bgShowCashButtonInBothFlight)
	{
		if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
		{
			ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
			}
		}
		else
		{
			ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");

			}
		}
	}
	
}


/*

char CSeasonDlg::GetPaid()
{
	char clPaid;
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		CString olPaid;
		CString olTmpValu;
		olTmpValu.Format("%ld",prmAFlight->Urno);

		ogBCD.GetField("AFT", "URNO", olTmpValu, "PAID", olPaid);

		if(olPaid.GetLength() == 0)
			clPaid = ' ';
		else
			clPaid = olPaid.GetAt(0);
	}
	else
	{
		
		CString olPaid;
		CString olTmpValu;
		olTmpValu.Format("%ld",prmDFlight->Urno);

		ogBCD.GetField("AFT", "URNO", olTmpValu, "PAID", olPaid);

		if(olPaid.GetLength() == 0)
			clPaid = ' ';
		else
			clPaid = olPaid.GetAt(0);
	}
	return clPaid;
}
*/
char CSeasonDlg::GetPaid(long flighturno)
{
	char clPaid;	
	CString olSelection;
	olSelection.Format("WHERE URNO='%d'",flighturno);

	CString olDummyTableName="AFT";
	ogBCD.SetObject(olDummyTableName, "URNO,PAID");
	ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
	ogBCD.Read(olDummyTableName,olSelection);

	int ndxPaid = ogBCD.GetFieldIndex( olDummyTableName, "PAID");	
	int nItems = ogBCD.GetDataCount( olDummyTableName );

	CString olPaid;
	for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
	{
		RecordSet record ;
		if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
		{
			olPaid = record[ ndxPaid ];
			
		}
	}

	if(olPaid.GetLength() == 0)
		clPaid = ' ';
	else
		clPaid = olPaid.GetAt(0);
		
	
	return clPaid;
}

CString CSeasonDlg::GetMopa(long flighturno)
{
//	char clPaid;	
	CString olSelection;
	olSelection.Format("WHERE URNO='%d'",flighturno);

	CString olDummyTableName="AFT";
	ogBCD.SetObject(olDummyTableName, "URNO,MOPA");
	ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
	ogBCD.Read(olDummyTableName,olSelection);

	int ndxPaid = ogBCD.GetFieldIndex( olDummyTableName, "MOPA");	
	int nItems = ogBCD.GetDataCount( olDummyTableName );

	CString olPaid;
	for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
	{
		RecordSet record ;
		if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
		{
			olPaid = record[ ndxPaid ];
			
		}
	}


	
	return olPaid;
}




//UFIS-1159 Check BlackList when "Insert" new flight from FIPS
bool CSeasonDlg::BlackList()
{
	bool blBlackListStop = false;

	CCS_TRY
	if(bgBlackList)
	{
		CString olList = "";

		if( prmAFlight->Ftyp[0] != 'X' && prmAFlight->Ftyp[0] != 'N')
		{
			char clRegn =' ';
			char clAct =' ';
			char clAlt =' ';
			char clDAlt = ' ';
			char clAApt =' ';
			char clDApt =' ';

			if( !CString(prmAFlight->Regn).IsEmpty())
				clRegn = ogBwaData.CheckRegn( prmAFlight->Stoa, CString(prmAFlight->Regn));

			if( !CString(prmAFlight->Act3).IsEmpty() && !CString(prmAFlight->Act5).IsEmpty())
				clAct = ogBwaData.CheckAct( prmAFlight->Stoa, CString(prmAFlight->Act3), CString(prmAFlight->Act5));

			if( !CString(prmAFlight->Alc2).IsEmpty() && !CString(prmAFlight->Alc3).IsEmpty())
				clAlt = ogBwaData.CheckAlc( prmAFlight->Stoa, CString(prmAFlight->Alc2), CString(prmAFlight->Alc3));

			if( !CString(prmDFlight->Alc2).IsEmpty() && 
				(CString(prmDFlight->Alc2)!= CString(prmAFlight->Alc2)) && 
				!CString(prmDFlight->Alc3).IsEmpty())
				clDAlt = ogBwaData.CheckAlc( prmDFlight->Stod, CString(prmDFlight->Alc2), CString(prmDFlight->Alc3));

			if( !CString(prmAFlight->Org3).IsEmpty() && !CString(prmAFlight->Org4).IsEmpty() )
					clAApt = ogBwaData.CheckApt( prmAFlight->Stoa, CString(prmAFlight->Org3), CString(prmAFlight->Org4));

			if( !CString(prmDFlight->Des3).IsEmpty() && !CString(prmDFlight->Des4).IsEmpty())
					clDApt = ogBwaData.CheckApt( prmDFlight->Stod, CString(prmDFlight->Des3), CString(prmDFlight->Des4));

			



			if(clRegn != ' ')
				olList = CString(prmAFlight->Regn) + CString(" (") + clRegn + CString(") \n");
			if(clAct != ' ')
				olList += CString(prmAFlight->Act3) + CString("/") + CString(prmAFlight->Act3) + CString(" (") + clAct + CString(") \n");
			if(clAlt != ' ')
				olList += CString(prmAFlight->Alc2) + CString("/") + CString(prmAFlight->Alc3) + CString(" (") + clAlt + CString(") \n");
			if(clAApt!= ' ')
			{
				olList += CString(prmAFlight->Org3) + CString("/") + CString(prmAFlight->Org4) + CString(" (") + clAApt + CString(") \n");
				
			}
			
			//Checking Via
			CString olVias(prmAFlight->Vial);
			CString olApc3;
			CString olApc4;
			CString clVia;
			while(olVias.IsEmpty() != TRUE)
			{
				if(olVias.GetLength() < 120)
				{
					olVias += "                                                                                                                             ";
					olVias = olVias.Left(120);
				}
				olApc3 = olVias.Mid(1,3);
				olApc3.TrimLeft();

				olApc4 = olVias.Mid(4,4);
				olApc4.TrimLeft();

				if(olApc3.GetLength() > 0 || olApc4.GetLength() >0)
				{
					clVia = ogBwaData.CheckApt( prmAFlight->Stoa, olApc3, olApc4);
					if(clVia != ' ')
					{
						olList += olApc3 + CString("/") + olApc4 + CString(" (") + clVia + CString(")\n");
					}
				}
				if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);
			}

			if(clDAlt != ' ')
				olList += CString(prmDFlight->Alc2) + CString("/") + CString(prmDFlight->Alc3) + CString(" (") + clDAlt + CString(") \n");
			if(clDApt!= ' ')
			{
				olList += CString(prmDFlight->Des3) + CString("/") + CString(prmDFlight->Des4) + CString(" (") + clDApt + CString(") \n");

			}

			//Checking Via
			CString olDVias(prmDFlight->Vial);
			CString olDApc3;
			CString olDApc4;
			CString clDVia;
			while(olDVias.IsEmpty() != TRUE)
			{
				if(olDVias.GetLength() < 120)
				{
					olDVias += "                                                                                                                             ";
					olDVias = olDVias.Left(120);
				}
				olDApc3 = olDVias.Mid(1,3);
				olDApc3.TrimLeft();

				olDApc4 = olDVias.Mid(4,4);
				olDApc4.TrimLeft();

				if(olDApc3.GetLength() > 0 || olDApc4.GetLength() >0)
				{
					clDVia = ogBwaData.CheckApt( prmDFlight->Stod, olDApc3, olDApc4);
					if(clDVia != ' ')
					{
						olList += olDApc3 + CString("/") + olDApc4 + CString(" (") + clDVia + CString(")\n");
					}
				}
				if(olDVias.GetLength() >= 120)
				olDVias = olDVias.Right(olDVias.GetLength() - 120);
			}
		

		}

		//Prompt for confirmation
		if (!olList.IsEmpty())
		{
			//CString olTmp;
			//olTmp.Format(GetString(IDS_STRING2931), olList);
			if(CFPMSApp::MyTopmostMessageBox(this, "Black/WatchList\n\n" + olList + "\n\nDo you want to continue?", GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) != IDYES)
				blBlackListStop = true;
		}
	}



	CCS_CATCH_ALL
	return blBlackListStop;
}


void CSeasonDlg::UpdateCashButton(const CString &ropAlc3, char cpPaid) 
{
	
	

	//need to update with the cash from AFTTAB
	CString olCash(""); 
	

	if (strcmp(pcgHome, "BKK") == 0) // for bankok only
	{
		cpPaid = ' ';
		if(bgShowCashButtonInBothFlight)
		{
			

			//cpPaid = prmAFlight->Paid[0];
			cpPaid = GetPaid(prmAFlight->Urno);

			if(cpPaid == 'B')
			{
				m_CashArrBtn->ShowWindow(SW_SHOW);
				m_CashArrBtn->SetWindowText(GetString(IDS_STRING2939));
				m_CashArrBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else if(cpPaid == 'P')
			{
				m_CashArrBtn->ShowWindow(SW_SHOW);
				m_CashArrBtn->SetWindowText(GetString(IDS_STRING1248));
				m_CashArrBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				
			}
			else
			{
				m_CashArrBtn->ShowWindow(SW_HIDE);
			}

			
			//is it a postflight?
			HandlePostFlight();


			cpPaid = GetPaid(prmDFlight->Urno);

			if(cpPaid == 'B')
			{
				
				m_CashDepBtn->ShowWindow(SW_SHOW);
				m_CashDepBtn->SetWindowText(GetString(IDS_STRING2939));
				m_CashDepBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else if(cpPaid == 'P')
			{
				m_CashDepBtn->ShowWindow(SW_SHOW);
				m_CashDepBtn->SetWindowText(GetString(IDS_STRING1248));
				m_CashDepBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				
			}
			else
			{
				m_CashDepBtn->ShowWindow(SW_HIDE);
			}

			
			//is it a postflight?
			HandlePostFlight();

			if(imModus == DLG_NEW)
			{
				m_CashArrBtn->ShowWindow(SW_HIDE);
				m_CashDepBtn->ShowWindow(SW_HIDE);
			}
		}
	}
	else
	{
		if(bgShowCashButtonInBothFlight)
		{


			CString olAMopa("");
			olAMopa= GetMopa(prmAFlight->Urno);
			olAMopa.TrimRight();
			CString olDMopa("");
			olDMopa= GetMopa(prmDFlight->Urno);
			olDMopa.TrimRight();

			

			if(olAMopa.GetLength()>0)
			{
				olCash = olAMopa;
			}

			if (olCash == "K")
				m_CashArrBtn->ShowWindow(SW_HIDE);
			else
			{
				cpPaid=GetPaid(prmAFlight->Urno);
				if(cpPaid == 'B')
				{
					m_CashArrBtn->ShowWindow(SW_SHOW);
					m_CashArrBtn->SetWindowText(GetString(IDS_STRING2939));
					m_CashArrBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else if(cpPaid == 'K')
				{
					if(olAMopa.GetLength()>0)
					{
						m_CashArrBtn->ShowWindow(SW_SHOW);
						m_CashArrBtn->SetWindowText(GetString(IDS_STRING2939));
						m_CashArrBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{	
						m_CashArrBtn->ShowWindow(SW_SHOW);
						m_CashArrBtn->SetWindowText(GetString(IDS_STRING1248));
						m_CashArrBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
				else
				{
					m_CashArrBtn->ShowWindow(SW_SHOW);
					m_CashArrBtn->SetWindowText(GetString(IDS_STRING1248));
					m_CashArrBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				
				
				//is it a postflight?
				HandlePostFlight();
			}

			if(olDMopa.GetLength()>0)
			{
				olCash = olDMopa;
			}

			
			if (olCash == "K")
				m_CashDepBtn->ShowWindow(SW_HIDE);
			else
			{
				cpPaid=GetPaid(prmDFlight->Urno);
				if(cpPaid == 'B')
				{
					m_CashDepBtn->ShowWindow(SW_SHOW);
					m_CashDepBtn->SetWindowText(GetString(IDS_STRING2939));
					m_CashDepBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else if(cpPaid == 'K')
				{
					if(olDMopa.GetLength()>0)
					{
						m_CashDepBtn->ShowWindow(SW_SHOW);
						m_CashDepBtn->SetWindowText(GetString(IDS_STRING2939));
						m_CashDepBtn->SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{
						m_CashDepBtn->ShowWindow(SW_SHOW);
						m_CashDepBtn->SetWindowText(GetString(IDS_STRING1248));
						m_CashDepBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
				else
				{
					m_CashDepBtn->ShowWindow(SW_SHOW);
					m_CashDepBtn->SetWindowText(GetString(IDS_STRING1248));
					m_CashDepBtn->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}

				
				
				
				//is it a postflight?
				HandlePostFlight();


				if(imModus == DLG_NEW)
				{
					m_CashArrBtn->ShowWindow(SW_HIDE);
					m_CashDepBtn->ShowWindow(SW_HIDE);
				}
			}



		}
	}
} 

void CSeasonDlg::SetCashButtonInBothFlights()
{
	

	CRect olWindowRect;
	CRect olWindowRect2;
	CRect olWindowBTRect;

	((CWnd*)this->GetDlgItem(IDC_ADALY))->GetWindowRect(olWindowRect2);
	ScreenToClient(olWindowRect2);

	
	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_ACSGN))->GetFont();
	DWORD buttonStyle = WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW;

	((CWnd*)this->GetDlgItem(IDC_ACSGN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.right + 55;
	olWindowBTRect.right 	= olWindowRect.right  + olWindowRect2.Width() + 55;

	olWindowBTRect.bottom 	= olWindowRect.bottom;
	m_CashArrBtn->Create("Cash", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_CASH_ARR);
	m_CashArrBtn->SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_DCSGN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);


	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.right + 20;
	olWindowBTRect.right 	= olWindowRect.right  + olWindowRect2.Width()  + 20;

	m_CashDepBtn->Create("Cash", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_CASH_DEP);
	m_CashDepBtn->SetFont(tmp);

	m_CashArrBtn->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));
	m_CashDepBtn->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));

	if(!bgShowCashButtonInBothFlight)
	{
		m_CashArrBtn->ShowWindow(SW_HIDE);
		m_CashDepBtn->ShowWindow(SW_HIDE);
	}

}


void CSeasonDlg::ArrangeTabsOrder()
{
	
	int arrControlsID[500];
	
	int i =0;


	arrControlsID[i]=IDCANCEL;i++;
	arrControlsID[i]=IDC_PREVFLIGHT;i++;
	arrControlsID[i]=IDC_NEXTFLIGHT;i++;
	arrControlsID[i]=IDC_ARESET;i++;
	arrControlsID[i]=IDC_DRESET;i++;
	arrControlsID[i]=IDC_AGENTS; i++;
	arrControlsID[i]=IDC_DAILY_MASK; i++;
	
	
	arrControlsID[i]=IDC_PVOM; i++;
	arrControlsID[i]=IDC_PBIS; i++;
	arrControlsID[i]=IDC_FREQ; i++;
	arrControlsID[i]=IDC_SEAS; i++;
	arrControlsID[i]=IDC_REGN; i++;
	arrControlsID[i]=IDC_ACT3; i++;
	arrControlsID[i]=IDC_ACT5; i++;	
	arrControlsID[i]=IDC_ACT35LIST; i++; 
	arrControlsID[i]=IDC_NOSE; i++;
	arrControlsID[i]=IDC_MING; i++;
	arrControlsID[i]=IDC_AAlc3LIST; i++;// Arrival : Flight 
	arrControlsID[i]=IDC_AALC3; i++;
	arrControlsID[i]=IDC_AFLTN; i++;	
	arrControlsID[i]=IDC_AFLNS; i++;
	arrControlsID[i]=IDC_AFLTI; i++;
	arrControlsID[i]=IDC_ACSGN; i++;// Arrival : Call sign
	arrControlsID[i]=IDC_DALC3LIST; i++;// Departure :  Flight  
	arrControlsID[i]=IDC_DALC3; i++;
	arrControlsID[i]=IDC_DFLTN; i++;
	arrControlsID[i]=IDC_DFLNS; i++;
	arrControlsID[i]=IDC_DFLTI; i++;
	arrControlsID[i]=IDC_DCSGN; i++;// Departure : Call sign
	arrControlsID[i]=IDC_AAlc3LIST2; i++; //Arrival :Code Share Flights
	arrControlsID[i]=IDC_AAlc3LIST3; i++;
	arrControlsID[i]=IDC_ASHOWJFNO; i++; // InitDialog
	arrControlsID[i]=IDC_ADAYS; i++;
	arrControlsID[i]=IDC_DDAYS; i++; // Arrival : Daily
	arrControlsID[i]=IDC_DALC3LIST2; i++; //Departure :Code Share Flights
	arrControlsID[i]=IDC_DALC3LIST3; i++;
	arrControlsID[i]=IDC_DSHOWJFNO; i++;// Departure
	arrControlsID[i]=IDC_DDAYS; i++;
	arrControlsID[i]=IDC_DDALY; i++;
	arrControlsID[i]=IDC_ACHT3; i++; //Arrival :Prfl // EnableArrival 
	arrControlsID[i]=IDC_ACXX; i++;
	arrControlsID[i]=IDC_ACXXREASON; i++;
	arrControlsID[i]=IDC_ANOOP; i++;
	arrControlsID[i]=IDC_DCHT3; i++; //Departure :Prfl 
	arrControlsID[i]=IDC_DCXX; i++;
	arrControlsID[i]=IDC_DCXXREASON; i++;
	arrControlsID[i]=IDC_DNOOP; i++;
	arrControlsID[i]=IDC_ATTYPLIST; i++;//Arrival :Nature
	arrControlsID[i]=IDC_ATTYP; i++;
	arrControlsID[i]=IDC_ASTYP; i++;
	arrControlsID[i]=IDC_ASTYPLIST; i++;
	arrControlsID[i]=IDC_ASTEV; i++;
	arrControlsID[i]=IDC_ASTE2; i++;
	arrControlsID[i]=IDC_ASTE3; i++;
	arrControlsID[i]=IDC_ASTE4; i++;
	arrControlsID[i]=IDC_DTTYPLIST; i++;//Departure :Nature 
	arrControlsID[i]=IDC_DTTYP; i++;
	arrControlsID[i]=IDC_DSTYP; i++;
	arrControlsID[i]=IDC_DSTYPLIST; i++;
	arrControlsID[i]=IDC_DSTEV; i++;
	arrControlsID[i]=IDC_DSTE2; i++;
	arrControlsID[i]=IDC_DSTE3; i++;
	arrControlsID[i]=IDC_DSTE4; i++;
	arrControlsID[i]=IDC_AORG3LIST; i++;//Arrival :Origin
	arrControlsID[i]=IDC_AORG3; i++;
	arrControlsID[i]=IDC_AORG4; i++;
	arrControlsID[i]=IDC_AVIA; i++;
	arrControlsID[i]=IDC_ADES3; i++; 
	arrControlsID[i]=IDC_DORG3; i++;//Departure :Home
	arrControlsID[i]=IDC_DVIA; i++;
	arrControlsID[i]=IDC_DDES3LIST; i++;
	arrControlsID[i]=IDC_DDES3; i++;
	arrControlsID[i]=IDC_DDES4; i++; 
	arrControlsID[i]=IDC_ASTOD; i++;//Arrival :STD
	arrControlsID[i]=IDC_ACSTA; i++;
	arrControlsID[i]=IDC_ASTOA; i++;
	arrControlsID[i]=IDC_DSTOD; i++;// Departure
	arrControlsID[i]=IDC_DCBNFES; i++;
	arrControlsID[i]=IDC_DSTOA; i++;

	arrControlsID[i]=IDC_ANFES; i++;
	arrControlsID[i]=IDC_AETAI; i++;
	arrControlsID[i]=IDC_DETDI; i++;
	arrControlsID[i]=IDC_DNFES; i++;

	arrControlsID[i]= IDC_DCSTD; i++;
	arrControlsID[i]=IDC_APSTALIST; i++;//Arrival :Position
	arrControlsID[i]=IDC_APSTA; i++;
	arrControlsID[i]=IDC_EDIT_ADURA; i++;
	arrControlsID[i]=IDC_APABS; i++;
	arrControlsID[i]=IDC_APAES; i++;
	arrControlsID[i]=IDC_AGTA1LIST; i++;//Arrival :Port/ Gate
	arrControlsID[i]=IDC_AGTA1; i++;
	arrControlsID[i]=IDC_ATGA1; i++;
	arrControlsID[i]=IDC_AGA1B; i++;
	arrControlsID[i]=IDC_AGA1E; i++;
	arrControlsID[i]=IDC_AGTA2LIST; i++;
	arrControlsID[i]=IDC_AGTA2; i++;
	arrControlsID[i]=IDC_ATGA2; i++;
	arrControlsID[i]=IDC_AGA2B; i++;
	arrControlsID[i]=IDC_AGA2E; i++;
	arrControlsID[i]=IDC_DPSTDLIST; i++;//Departure :Position 
	arrControlsID[i]=IDC_DPSTD; i++;
	arrControlsID[i]=IDC_DPDBS; i++;
	arrControlsID[i]=IDC_EDIT_DDURA; i++;
	arrControlsID[i]=IDC_DPDES; i++;
	arrControlsID[i]=IDC_DBLT1LIST; i++;//Departure :Baggage Belts
	arrControlsID[i]=IDC_DBLT1; i++;
	arrControlsID[i]=IDC_DTMB1; i++;
	arrControlsID[i]=IDC_DB1BA; i++;
	arrControlsID[i]=IDC_DB1EA; i++;
	arrControlsID[i]=IDC_DGTD1LIST; i++;//Departure :Gates
	arrControlsID[i]=IDC_DGTD1; i++;
	arrControlsID[i]=IDC_DTGD1; i++;
	arrControlsID[i]=IDC_DGD1B; i++;
	arrControlsID[i]=IDC_DGD1E; i++;
	arrControlsID[i]=IDC_DGTD2LIST; i++;
	arrControlsID[i]=IDC_DGTD2; i++;
	arrControlsID[i]=IDC_DTGD2; i++;
	arrControlsID[i]=IDC_DGD2B; i++;
	arrControlsID[i]=IDC_DGD2E; i++;
	arrControlsID[i]=IDC_ABLT1LIST; i++;// Arrival : Baggage Belts
	arrControlsID[i]=IDC_ABLT1; i++;
	arrControlsID[i]=IDC_ATMB1; i++;
	arrControlsID[i]=IDC_AB1BS; i++;
	arrControlsID[i]=IDC_AB1ES; i++;
	arrControlsID[i]=IDC_ABLT2LIST; i++;
	arrControlsID[i]=IDC_ABLT2; i++;
	arrControlsID[i]=IDC_ATMB2; i++;
	arrControlsID[i]=IDC_AB2BS; i++;
	arrControlsID[i]=IDC_AB2ES; i++;
	arrControlsID[i]=IDC_AEXT1LIST; i++;// Arrival :Exits
	arrControlsID[i]=IDC_AEXT1; i++;
	arrControlsID[i]=IDC_ATET1; i++;
	arrControlsID[i]=IDC_AEXT2LIST; i++;
	arrControlsID[i]=IDC_AEXT2; i++;
	arrControlsID[i]=IDC_ATET2; i++;
	arrControlsID[i]=IDC_DCINSLIST1; i++;//Departure : CKI Counter
	arrControlsID[i]=IDC_DCINSLIST2; i++;
	arrControlsID[i]=IDC_DCINSLIST3; i++;
	arrControlsID[i]=IDC_DCINSLIST4; i++;
	arrControlsID[i]=IDC_DCINSMAL4; i++;
	arrControlsID[i]=IDC_CIFR_CITO; i++;
	arrControlsID[i]=IDC_DCINSBORDEREXT; i++;
	arrControlsID[i]=IDC_DWRO1LIST; i++;//Departure :Lounges
	arrControlsID[i]=IDC_DWRO1; i++;
	arrControlsID[i]=IDC_DTWR1; i++;
	arrControlsID[i]=IDC_DW1BS; i++;
	arrControlsID[i]=IDC_DW1ES; i++;
	arrControlsID[i]=IDC_DWRO2LIST; i++;
	arrControlsID[i]=IDC_DWRO2; i++;
	arrControlsID[i]=IDC_DTWR2; i++;
	arrControlsID[i]=IDC_DW2BS; i++;
	arrControlsID[i]=IDC_DW2ES; i++;
	arrControlsID[i]=IDC_DBAZ1; i++;//Departure :Carousel 1/2
	arrControlsID[i]=IDC_DBAZ4; i++;
	arrControlsID[i]=IDC_ASTATUS; i++;// Arrival : Combo box :Planning
	arrControlsID[i]=IDC_ASTATUS2; i++;// Arrival : Combo box :Operation
	arrControlsID[i]=IDC_ASTATUS3; i++;// Arrival : Combo box ://lgp
	arrControlsID[i]=IDC_DSTATUS; i++;// Departure : Combo box :Planning
	arrControlsID[i]=IDC_DSTATUS2; i++;// Departure : Combo box :Operation
	arrControlsID[i]=IDC_DSTATUS3; i++;// Departure : Combo box ://lgp
	arrControlsID[i]=IDC_AREMP; i++; // Arrival : FIDS Comment
	arrControlsID[i]=IDC_COMBO_AHISTORY; i++; // Arrival : History
	arrControlsID[i]=IDC_DREMP; i++; // Departure:  FIDS Comment
	arrControlsID[i]=IDC_COMBO_DHISTORY; i++; // Departure:   History
	arrControlsID[i]=IDC_AREM1; i++; // Arrival :  Comment Flight
	arrControlsID[i]=IDC_AREM2; i++; // Arrival :  Comment Season
	arrControlsID[i]=IDC_DREM1; i++; // Departure:  Comment Flight
	arrControlsID[i]=IDC_DREM2; i++; // Departure:  Comment Season
	arrControlsID[i]=IDC_ACREATED; i++; // Arrival : Entered
	arrControlsID[i]=IDC_ALASTCHANGE; i++; //Arrival :  Most Recent Change
	arrControlsID[i]=IDC_DCREATED; i++; // Departure:  Entered
	arrControlsID[i]=IDC_DLASTCHANGE; i++; // Departure:  Most Recent Change
	arrControlsID[i]=IDC_AFLUKO; i++; // Arrival Flico
	arrControlsID[i]=IDC_DFLUKO; i++; // Departure Flico


	CWnd* pWndPrev = ((CWnd*)this->GetDlgItem(IDOK));
	CWnd* pWnd = NULL;

	for (int j=0;j<i ;j++)
	{
		pWnd = ((CWnd*)this->GetDlgItem(arrControlsID[j]));
		if(pWnd->IsWindowEnabled())
		{
			pWnd->SetWindowPos(pWndPrev,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
			pWndPrev = pWnd;
		}
	}

}
