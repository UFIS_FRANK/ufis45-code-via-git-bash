// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <RecordSet.h>
#include <CedaParData.h>
#include <CedaValData.h>
#include <CedaAptLocalUtc.h>



////////////////////// Stringhandling /////////////////////////////////////////////////////////////

// String mit Zeichen bis zur L�nge ipLength auff�llen
int FillRightByChar( CString &opStr, char cpSign, int ipLength )
{
	// hhb 7/98, return : Anzahl neue Zeichen
	int ipAnhang = ipLength - opStr.GetLength() ;
	
	if( ipAnhang > 0 )
	{
		opStr += CString( cpSign, ipAnhang ) ; 
		return( ipAnhang ) ;
	}
	else
		return( 0 ) ;

}

int FillLeftByChar( CString &opStr, char cpSign, int ipLength )
{
	// hhb 7/98, return : Anzahl neue Zeichen
	int ipAnhang = ipLength - opStr.GetLength() ;
	
	if( ipAnhang > 0 )
	{
		opStr = CString( cpSign, ipAnhang ) + opStr; 
		return( ipAnhang ) ;
	}
	else
		return( 0 ) ;

}

// Linken Anteil von String besorgen   
int CutLeft(CString & ropLeft, CString & ropLeftover, const char * OneOfSep)
{
	// liefert alle Zeichen von ropLeft bis 1. Auftreten von einem Zeichen aus "OneOfSep"
	// in ropLeft zur�ck, den Rest von ropLeft in ropLeftover
	// der Separator wird entfernt

	int ilSepPos = ropLeft.FindOneOf( OneOfSep ) ;	

	if( ilSepPos < 0 ) 
	{
		// kein Separator => alles in ropLeft !
		ropLeftover.Empty() ;
	}
	else
	{
		ropLeftover = ropLeft.Right( ropLeft.GetLength() - ilSepPos - 1 ) ;
		ropLeft = ropLeft.Left( ilSepPos ) ;
	}
	return( ropLeftover.GetLength() ) ;

}

int CutLeft(CString & ropLeft, CString & ropLeftover, int ipNo)
{
	// liefert "ipNo" Zeichen von ropLeft in "ropLeft" zur�ck, 
	// den Rest von ropLeft in "ropLeftover"

	ropLeftover = ropLeft.Right( ropLeft.GetLength() - ipNo ) ;
	ropLeft = ropLeft.Left( ipNo ) ;
	return( ropLeftover.GetLength() ) ;
}


// ......... String( dd.mm.yyyy-dd.mm.yyyy ) in Kurzform aufbereiten
void ShortRangeString( CString &ropStr )
{
	CString olFrom = ropStr.Left(10) ; 
	CString olTo = ropStr.Mid(11,10) ;

	if( ropStr.GetLength() == cimFullRangeLen )
	{
	
		if( olFrom == olTo ) ropStr = olFrom ;	// Eintagsflieger (dd.mm.yy)
		else
		{
			if( olFrom.Right(4) == olTo.Right(4) ) // gleiches Jahr (dd.mm.-dd.mm.yy)
			{
				ropStr = olFrom.Left(6)+"-"+olTo.Left(6)+olFrom.Right(4) ;
			}
			else	//  (dd.mm.yy-dd.mm.yy)
			{
				char pslShortJahr[4+1] ;
				strcpy( pslShortJahr, olFrom.Right(4) ) ;
				int ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr = olFrom.Left(6)+pslShortJahr+CString("-") ;
				strcpy( pslShortJahr, olTo.Right(4) ) ;
				ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr += olTo.Left(6)+pslShortJahr ;
			}
		}
	}
}

// 050303 MVy: get the first string from a delimited source string
//	the source data are modified!
CString ExtractFirstToken( CString& source, CString delimiter )
{
	CString found ;
	int pos = source.Find( delimiter );
	if( pos > -1 )	// substring found
	{
		found = source.Left( pos );
		int len = pos + delimiter.GetLength();
		ASSERT( len <= source.GetLength() );
		source.Delete( 0, len );
	}
	else		// no more delimters, so we are at end. Use the rest of the string 
	{
		found = source ;
		source.Empty();
	};
	return found ;
};	// ExtractFirstToken

// 050331 MVy: find a string with begin and end and extract it from source; useful for configuration tupels like ITEM1=VALUE1;ITEM2=VALUE2
//	hint: return value is empty if not found or really empty, no difference in handling
CString ExtractValue( CString &source, char* start, char terminator )
{
	CString sValue ;
	int length = strlen( start );
	int pos = source.Find( start );
	if( pos > -1 )
	{
		int end = source.Find( terminator, pos + length );
		if( end == -1 ) end = source.GetLength();		// no terminator found, so we are at the end of the source string
		int count = end - ( pos + length -1 );
		sValue = source.Mid( pos + length, count );		// store value
		source.Delete( pos, end - pos +1 );		// remove key, value and termiantor
	};
	return sValue ;
};	// ExtractValue

// 050303 MVy: convert the string to long value, even if it is a sedecimal value written like 0x90ABCDEF
long ToLong( CString source )
{
	source.TrimLeft();		// remove leading spaces
	source.MakeUpper();
	int radix = 10 ;
	if( source.GetLength() > 2 )		// at least three chars for sth. like 0x0
	{
		if( source.Mid( 0, 2 ) == "0X" )		// a sedecimal number followes ... hopefully
		{
			char* pStop = 0 ;
			long value = strtol( source, &pStop, 16 );		// 0x... wiis interpreted by this standard lib conversion function automatically, but maybe we will do some more user specific conversions later herein
			return value ;
		};
	};
	return atol( source );		// handle string in default manner
};	// ToLong

// 050318 MVy: helper function
CString ToString( unsigned long value )
{
	CString sValue ;
	sValue.Format( "%d", value );
	return sValue ;
};	// ToString

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
			{
				CString olTmp = olData[i];
				if(olTmp.GetLength() == 12)
					olTmp += CString("00");

				opData = DBStringToDateTime(olTmp);
			}
			return true; 
		}
	}
	return false;
}



void TraceFieldAndData(CString &opFieldList,CString &opListOfData, CString opMessage)
{

	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opListOfData, &olData);

	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
	TRACE("\n-- %s --- Fields:<%d>   Data:<%d>", opMessage, ilFields, ilData);


	if(ilFields == ilData)
	{
		for( int i = 0; i < ilData; i++)
		{
			TRACE("\n<%s> <%d> <%s>", olFields[i], olData[i].GetLength(), olData[i]);
		}
	}


	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
}





int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}


int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	else
	{
		popStrArray->Add(" ");
	}
	return	popStrArray->GetSize();

}




int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}






CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}


CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';

	imNextGMUAmount = 7;
	imNextGMUAmountAFT = 7;
	imNextGMUAmountCCA = 7;
	imNextGMUAmountCFL = 7;

	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(ogAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
	if (omNotValidBrushes)
	{
		for( int ilLc = 0; ilLc < MAXNVBRUSHES; ilLc++)
		{
			CBrush *olBrush = omNotValidBrushes[ilLc];
			if (olBrush)
				delete olBrush;
		}
	}
}






int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}






void CBasicData::SetLocalDiff()
{
	int		ilTdi1;
	int		ilTdi2;
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			olTich = DBStringToDateTime((*prlRecord)[0]);
			omTich = olTich;
			ilTdi1 = atoi((*prlRecord)[1]);
			ilTdi2 = atoi((*prlRecord)[2]);
			
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			
			CTimeSpan	olTdi1(0,ilHour ,ilMin ,0);

			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			
			CTimeSpan	olTdi2(0,ilHour ,ilMin ,0);

			olCurr = CTime::GetCurrentTime();

			if(olTich != TIMENULL) 
			{
				omLocalDiff1 = olTdi1;
				omLocalDiff2 = olTdi2;
			}
		}
	}
	olData.DeleteAll();
}

// the same functions are in CCSCedaData !!!!

void CBasicData::LocalToUtc(CTime &opTime)
{
	CedaAptLocalUtc::AptLocalToUtc (opTime, "");

	return;

	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

void CBasicData::UtcToLocal(CTime &opTime)
{
	CedaAptLocalUtc::AptUtcToLocal (opTime, "");

	return;

	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);



	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}



int CBasicData::GetViaArray(CCSPtrArray<VIADATA> *opVias, CString opViaList)
{
	opVias->DeleteAll();


	CString olVias(opViaList);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                                                 ";
				olVias = olVias.Left(120);
			}


			olFids = olVias.Left(1);
			olApc3 = olVias.Mid(1,3);
			olApc4 = olVias.Mid(4,4);
			prlVia->Stoa = DBStringToDateTime(olVias.Mid(8,14));
			prlVia->Etoa = DBStringToDateTime(olVias.Mid(22,14));
			prlVia->Land = DBStringToDateTime(olVias.Mid(36,14));
			prlVia->Onbl = DBStringToDateTime(olVias.Mid(50,14));
			prlVia->Stod = DBStringToDateTime(olVias.Mid(64,14));
			prlVia->Etod = DBStringToDateTime(olVias.Mid(78,14));
			prlVia->Ofbl = DBStringToDateTime(olVias.Mid(92,14));
			prlVia->Airb = DBStringToDateTime(olVias.Mid(106,14));			
			olFids.TrimLeft();
			olApc3.TrimLeft();
			olApc4.TrimLeft();
			sprintf(prlVia->Fids, olFids);
			sprintf(prlVia->Apc3, olApc3);
			sprintf(prlVia->Apc4, olApc4);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

			//if(olVias.GetLength() == 120)
			//	break;

	}
	return opVias->GetSize();
}

BOOL LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}

bool CBasicData::AddBlockingImages(CImageList& ropImageList)
{
	static bool blAddDone = false;

//	int l = sizeof (omNotValidBrushes);
	if (blAddDone)
		return false;

	if (!ropImageList.Create(16,16,ILC_COLOR4,10,10))
		return false;

	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclBitmapCount[24];
    GetPrivateProfileString("GATPOS", "BlockedBitmapCount", "0", pclBitmapCount, sizeof pclBitmapCount, pclConfigPath);
	int ilBitmapCount = atoi(pclBitmapCount);

	for (int i = 0; i < ilBitmapCount; i++)
	{
		char pclBlockedBitmap[256];
		char pclBitmap[256];
		sprintf(pclBitmap,"BlockedBitmap%d",i+1);
	    GetPrivateProfileString("GATPOS",pclBitmap, "", pclBlockedBitmap, sizeof pclBlockedBitmap, pclConfigPath);

		HBITMAP hBitmap;
		HPALETTE hPalette;

		if (! LoadBitmapFromBMPFile(pclBlockedBitmap,&hBitmap,&hPalette))
			return false;

		CBitmap* polBitmap1 = new CBitmap;
		polBitmap1->Attach(hBitmap);

		if (i < MAXNVBRUSHES)
			omNotValidBrushes[i] = new CBrush(polBitmap1);

		if (ropImageList.Add(polBitmap1,(CBitmap *)NULL) < 0)
		{
			delete polBitmap1;
			return false;
		}

		polBitmap1->Detach();
		delete polBitmap1;

	}

	blAddDone = true;

	return true;//CArray
}

CBrush* CBasicData::GetBlockingBrushAt(int& ipIndex)
{
	if (ipIndex >= 0 && ipIndex < MAXNVBRUSHES)
	{
		return omNotValidBrushes[ipIndex];
	}

	return NULL;
}

bool CBasicData::IsGatPosEnabled()
{ 
	static int ilIsGatPosEnabled = -1;
	if (ilIsGatPosEnabled == -1)
	{
//		ogParData.Read();
//		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_GAT_POS");
		if (olValue.CompareNoCase("Y") == 0)
			ilIsGatPosEnabled = 1;		
		else
			ilIsGatPosEnabled = 0;		
	}

	return ilIsGatPosEnabled != 0;
}

bool CBasicData::IsExtLmEnabled()
{ 
	static int ilIsExtLmEnabled = -1;
	if (ilIsExtLmEnabled == -1)
	{
//		ogParData.Read();
//		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_EXT_LM");
		if (olValue.CompareNoCase("Y") == 0)
			ilIsExtLmEnabled = 1;		
		else
			ilIsExtLmEnabled = 0;		
	}

	return ilIsExtLmEnabled != 0;
}

bool CBasicData::IsTerminalRestrictionForAirlinesAvailable()
{
	return CedaObject::IsFieldAvailable("ALT","TERG");
}

void CBasicData::LoadParameters()
{
	// aktuelle Zeit erhalten
	COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olDefaultValidFrom	= ("19901010101010");
	CString olDefaultValidTo	= "";

	// Falls Parameter nicht existiert. anlegen und Defaultwert setzen.
	// General

	char pclWhere[128];
			
	sprintf(pclWhere,"WHERE APPL IN ('%s','%s')", "BDPS-UIF", "GLOBAL");

	ogParData.Read();
	ogValData.Read(pclWhere);

	ogParData.GetParById("GLOBAL","ID_GAT_POS",olStringNow,"Y","GATPOS enabled","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	ogParData.GetParById("GLOBAL","ID_EXT_LM", olStringNow,"N","Extended Location Management enabled","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	
}

WORD CBasicData::AircraftTypeDescriptionLength()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA,FELE";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'ACT' AND Fina = 'ACFN'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
		if (ilRc)
		{
			CString olData;
			if (this->GetBufferLine(0,olData))
			{
				CStringArray olList;
				::ExtractItemList(olData,&olList);
				ilRc = atoi(olList[2]);
			}
			else
				ilRc = 0;
		}
	}

	return ilRc;	
}

// 050318 MVy: helper function for specific access to table field
//	attention: returns empty string even if there are no data in field, but asserts 
CString GetFieldByUrno( CString sTable, unsigned long urno, CString sColumn )
{
	CString sItem = "" ;
	CString sUrno ;
	sUrno.Format( "%d", urno );
	VERIFY( ogBCD.GetField( sTable, "URNO", sUrno, sColumn, sItem ) );		// data should be there, maybe using wrong table
	return sItem ;
};	// GetFieldByUrno

// 050318 MVy: helper function for specific access to table field, 
//	hint: assuming, the name column looks like tablename.first.character + "NAM"
//	attention: returns empty string even if there are no data in field, but asserts 
CString GetNameByUrno( CString sTable, unsigned long urno )
{
	CString sItem = "" ;
	ASSERT( sTable == "HAG" | sTable == "HTY" | sTable == "CIC" );
	CString sUrno ;
	sUrno.Format( "%d", urno );
	CString sColumn = sTable.Left(1) + "NAM" ;
	VERIFY( ogBCD.GetField( sTable, "URNO", sUrno, sColumn, sItem ) );		// data should be there, maybe using wrong table or name column does not exist
	return sItem ;
};	// GetNameByUrno

// 050318 MVy: helper function for specific access to table field
//	hint: assuming, the name column looks like tablename.first.character + "NAM"
//	attention: returns empty string even if there are no data in field, but asserts 
unsigned long GetUrnoByName( CString sTable, CString sSearch )
{
	if( sSearch.IsEmpty() ) return 0 ;		// availd quering db for empty entry
	CString sItem ;
	ASSERT( sTable == "HAG" | sTable == "HTY" | sTable == "CIC" );
	CString sColumn = sTable.Left(1) + "NAM" ;
	unsigned long urno = 0 ;
	if( ogBCD.GetField( sTable, sColumn, sSearch, "URNO", sItem ) )
		urno = ToLong( sItem );		// 050406 MVy: expected name need not exist in database
	return urno ;
};	// GetUrnoByAbrv

CString GetAbrvByUrno( CString sTable, unsigned long urno )
{
	CString sItem = "" ;
	ASSERT( sTable == "HAG" );
	CString sUrno ;
	sUrno.Format( "%d", urno );
	CString sColumn = sTable.Left(1) + "SNA" ;
	VERIFY( ogBCD.GetField( sTable, "URNO", sUrno, sColumn, sItem ) );		// data should be there, maybe using wrong table or name column does not exist
	return sItem ;
};	// GetAbrvByUrno

unsigned long GetUrnoByAbrv( CString sTable, CString sSearch )
{
	if( sSearch.IsEmpty() ) return 0 ;		// availd quering db for empty entry
	CString sItem ;
	ASSERT( sTable == "HAG" );
	CString sColumn = sTable.Left(1) + "SNA" ;
	unsigned long urno = 0 ;
	if( ogBCD.GetField( sTable, sColumn, sSearch, "URNO", sItem ) )
		urno = ToLong( sItem );		// 050406 MVy: expected name need not exist in database
	return urno ;
};	// GetUrnoByAbrv

// 050324 MVy: helper function; return the ALC3 code and if not available the ALC2 code
// fill in the variables if specified
CString GetAlcByUrno( unsigned long urnoAirline, CString *psValue1st, CString *psValue2nd )
{
	CString sValue = "" ;
	int ndxColALC2 = ogBCD.GetFieldIndex( "ALT", "ALC2" );
	int ndxColALC3 = ogBCD.GetFieldIndex( "ALT", "ALC3" );
	RecordSet record ;
	if( ogBCD.GetRecord( "ALT", "URNO", ToString( urnoAirline ), record ) )		// fetch airline record by urno
	{
		ASSERT( 0 <= ndxColALC2 && ndxColALC2 < record.Values.GetSize() );
		CString sAlc2 = record[ ndxColALC2 ];
		if( psValue1st ) *psValue1st = sAlc2 ;
		ASSERT( 0 <= ndxColALC3 && ndxColALC3 < record.Values.GetSize() );
		CString sAlc3 = record[ ndxColALC3 ];
		if( psValue2nd ) *psValue2nd = sAlc3 ;
		sAlc2.IsEmpty() ? sValue = sAlc3 : sValue = sAlc2 ;
		ASSERT( !sValue.IsEmpty() );		// oops ... no airline code at all in ALT table, must be a database inconsistency
	};
	return sValue ;
};	// GetAlcByUrno

// 050318 MVy: fill a string array with airline codes from referring to handling agent HAG.HSNA and handling task type HTY.TASK
//	"airline urnos" = select ALTU from HAI where HSNA and TASK
//	"airline codes" = select ALC2, ALC3 from ALT where URNO in "airline urnos"
unsigned long GetAirlineCodes( unsigned long urnoAgent, unsigned long urnoTask, CStringArray& strarrAirlineCodes )
{
	strarrAirlineCodes.RemoveAll();
	strarrAirlineCodes.Add( "" );		// 050426 MVy: empty 

	if( !urnoAgent ) return 0 ;		// faster take out off this function
	if( !urnoTask ) return 0 ;		// faster take out off this function

	int ndxColALTU = ogBCD.GetFieldIndex( "HAI", "ALTU" );

	CCSPtrArray< RecordSet > rsarrOccupations ;
	CString sHSNA = GetAbrvByUrno( "HAG", urnoAgent );
	CString sTASK = GetNameByUrno( "HTY", urnoTask );
	int iCount = ogBCD.GetRecordsExt( "HAI", "HSNA", "TASK", sHSNA, sTASK, &rsarrOccupations );
	ASSERT( iCount == rsarrOccupations.GetSize() );
	if( !iCount ) return 0 ;		// faster take out off this function
	for( int ndxOccupation = 0 ; ndxOccupation < iCount ; ndxOccupation++ )		// for each occupation
	{
		RecordSet* pRecord = &rsarrOccupations[ ndxOccupation ];
		ASSERT( 0 <= ndxColALTU && ndxColALTU < pRecord->Values.GetSize() );

		unsigned long urnoAirline = ToLong( (*pRecord )[ ndxColALTU ] );
		CString sAlc2 ;
		CString sAlc3 ;
		if( !GetAlcByUrno( urnoAirline, &sAlc2, &sAlc3 ).IsEmpty() )
		{
			if( !sAlc2.IsEmpty() ) strarrAirlineCodes.Add( sAlc2 );
			if( !sAlc3.IsEmpty() ) strarrAirlineCodes.Add( sAlc3 );
		}
		else		// oops, airline urno not found
		{
			TRACE( "%s(%d): GetAirlineCodes(), database inconsistency, airline urno not found in ALT\n", __FILE__, __LINE__ );
			//ASSERT(0);
		};
	};

#ifdef _DEBUG
	// 050323 MVy: dump information
	{
		int size = strarrAirlineCodes.GetSize();
		TRACE( "airlinecodes (%d) for agent '%s' and task '%s': ", size, sHSNA, sTASK );
		for( int ndxALC = 0 ; ndxALC < size ; ndxALC++ )
		{
			TRACE( "%s,", strarrAirlineCodes[ ndxALC ] );
		};
		TRACE( "\n" );
	}
#endif		// _DEBUG

	rsarrOccupations.DeleteAll();

	return strarrAirlineCodes.GetSize();
};	// GetAirlineCodes

// 050323 MVy: return if array contains an identical string
bool IsStringIn( CStringArray& array, const char* search )
{
	//ASSERT( strcmp( search, "" ) );
	for( int ndx = 0 ; ndx < array.GetSize() ; ndx++ )
		if( array[ ndx ] == search ) 
			return true ;
	return false ;
};	// IsStringIn

long MakeYear( CString& sYear )
{
	if( sYear.GetLength() == 3 )
		sYear = sYear.Right( 1 );		// someone forgot the last digit
	long lYear = ToLong( sYear );
	if( 35 < lYear && lYear <= 99 )
		lYear += 1900 ;
	else
	if( 0 < lYear && lYear <= 35 )
		lYear += 2000 ;

	if( lYear < 1970 )
		lYear = 1970 ;
	else
	if( lYear > 2037 )
		lYear = 2037 ;
	return lYear ;
};	// MakeYear


// 050324 MVy: format with two dots like 24.3.2005 should be detected, too
bool MakePartDate( const CString &sText, struct tm &time )
{
	int length = sText.GetLength();
	int dot1 = sText.FindOneOf( "./-" );
	if( dot1 > -1 )
	{
		char cFind = sText[ dot1 ];		// CString::FindOneOf cannot handle start index
		int dot2 = sText.Find( cFind, dot1 +1 );
		if( dot2 > -1 )
		{
			CString sDay = sText.Mid( 0, dot1 );
			CString sMonth = sText.Mid( dot1 +1, dot2 - dot1 -1 );
			int space = dot2 +1 ;
			while( space < length && isdigit( sText[ space ] ) && space < dot2 +4 +1 )
				space++ ;
			//int space = sText.Find( ' ', dot2 );
			//if( space == -1 ) space = sText.GetLength() -1 ;		// no space -> use all
			//if( space > dot2 + 4 ) space = dot2 + 4 ;		// year can only have 4 digits and no leading space!
			CString sYear = sText.Mid( dot2 +1, space - dot2 -1 );
			long lYear = MakeYear( sYear );		// // check year

			// check month
			long lMonth = ToLong( sMonth );
			if( lMonth < 0 )
				lMonth = 1 ;
			else
			if( lMonth > 12 )
				lMonth = 12 ;

			// check day
			long lDay = ToLong( sDay );
			if( lDay < 0 )
				lDay = 1 ;
			else
			if( lDay > 31 )
				lDay = 31 ;

			time.tm_year = lYear - 1900 ;
			time.tm_mon  = lMonth - 1 ;
			time.tm_mday = lDay ;

			return true ;
		};
	}
	else
	{
		// maybe the format is mixed date / time in dotted and plain like 07042005 8:00
		// then there must be a separator usually a space !

		// scan until no numbers
		int digit = 0 ;
		while( digit < length && isdigit( sText[ digit ] ) )
			digit++ ;

		int start = 0 ;
		int end = digit ;

		if( end - start == 8 )
		{
			time.tm_mday = ToLong( sText.Mid( start + 0, 2 ) );
			time.tm_mon  = ToLong( sText.Mid( start + 2, 2 ) ) - 1 ;
			time.tm_year = ToLong( sText.Mid( start + 4, 4 ) ) - 1900 ;
		}
		else
		if( end - start == 6 )
		{
			time.tm_mday = ToLong( sText.Mid( start + 0, 2 ) );
			time.tm_mon  = ToLong( sText.Mid( start + 2, 2 ) ) - 1 ;
			long lYear = MakeYear( sText.Mid( start + 4, 2 ) );
			time.tm_year = lYear - 1900 ;
		}
		else
			return false ;		// do not know how to deal with data

		return true ;
	};
	return false ;
};	// MakePartDate

// 050324 MVy: format with a colon like 11:30 should be detected, too
bool MakePartTime( const CString &sText, struct tm &time )
{
	int length = sText.GetLength();
	int colon = sText.Find( ':' );
	if( colon > -1 )
	{
		// left of colon is the hour
		int hour = colon -1 ;
		while( hour > -1 && isdigit( sText[ hour ] ) && hour > colon -3 )
			hour-- ;
		CString sHour = sText.Mid( hour +1, colon - hour -1 );
		time.tm_hour = ToLong( sHour );

		int colon2 = sText.Find( ':', colon +1 );
		if( colon2 > -1 )
		{
			CString sMinute = sText.Mid( colon +1, colon2 - colon -1 );
			time.tm_min  = ToLong( sMinute );
			CString sSecond = sText.Mid( colon2 +1, 2 );
			time.tm_sec  = ToLong( sSecond );
		}
		else
		{
			CString sMinute = sText.Mid( colon +1, 2 );
			time.tm_min  = ToLong( sMinute );
		};

		// us time format:
		//	00:00-00:59 can only be AM
		//	01:00-11:59 can be AM or PM
		//	12:00-12:59 can only be PM
		//	13:00-23:59 does not exist
		if( sText.FindOneOf( "Pp" ) > -1 
			&& ( time.tm_hour > 0 && time.tm_hour < 12 ) )
			time.tm_hour += 12 ;

		return true ;
	}
	else
	{
		// maybe the format is mixed date / time in dotted and plain like 7.4.2005 0800
		// then there must be a separator usually a space !
		// scan from end

		// scan until no numbers
		int digit = length -1 ;
		while( digit >= 0 && isdigit( sText[ digit ] ) )
			digit-- ;

		int start = digit +1 ;
		int end = length ;

		if( end - start == 2 )
		{
			time.tm_hour = ToLong( sText.Mid( start + 0, 2 ) );
		}
		else
		if( end - start == 4 )
		{
			time.tm_hour = ToLong( sText.Mid( start + 0, 2 ) );
			time.tm_min  = ToLong( sText.Mid( start + 2, 2 ) );
		}
		else
		if( end - start == 6 )
		{
			time.tm_hour = ToLong( sText.Mid( start + 0, 2 ) );
			time.tm_min  = ToLong( sText.Mid( start + 2, 2 ) );
			time.tm_sec  = ToLong( sText.Mid( start + 4, 2 ) );
		}
		else
			return false ;		// do not know how to deal with data

		return true ;
	};

	return false ;
};	// MakePartTime

// 050322 MVy: format like YYYYMMDDhhmmss or hhmm
//		the function returns -1 on invalid date
//		converting only a time withou a date the lowest possible date 1.1.1970 is assumed
CTime ToTime( const CString &sText, const char* pcDefault )
{
	struct tm time ;
	memset( &time, 0, sizeof( time ) );

	if( !MakePartDate( sText, time )
		|| !MakePartTime( sText, time ) )		// ! both functions must be called !
	{
		// handle time string like plain number string
		int length = sText.GetLength();

		// if string contains only numbers and spaces remove the spaces
		bool bSpaceFound = false ;
		bool bFoundOthers = false ;
		for( int pos = 0 ; pos < length ; pos++ )
		{
			bSpaceFound = sText[ pos ] == ' ' ;
			if( !( isdigit( sText[ pos ] ) || bSpaceFound ) )
			{
				bFoundOthers = true ;
				break ;
			};
		};

		CString sTextCopy = sText ;		// need modifiable string
		if( !bFoundOthers && bSpaceFound )		// only digits and spaces so remove all spaces
		{
			VERIFY( sTextCopy.Remove( ' ' ) );		// must contain spaces otherwise bSpaceFound cant be set before, maybe some error in loop ?
			length = sTextCopy.GetLength();		// length has changed
		};

		if( length == 14 )		// complete date and time
		{
			time.tm_year = ToLong( sTextCopy.Mid(  0, 4 ) ) - 1900 ;
			time.tm_mon  = ToLong( sTextCopy.Mid(  4, 2 ) ) - 1 ;
			time.tm_mday = ToLong( sTextCopy.Mid(  6, 2 ) );
			time.tm_hour = ToLong( sTextCopy.Mid(  8, 2 ) );
			time.tm_min  = ToLong( sTextCopy.Mid( 10, 2 ) );
			time.tm_sec  = ToLong( sTextCopy.Mid( 12, 2 ) );
		}
		else
		if( length == 8 )		// only date, year at first
		{
			time.tm_year = ToLong( sTextCopy.Mid(  0, 4 ) ) - 1900 ;
			time.tm_mon  = ToLong( sTextCopy.Mid(  4, 2 ) ) - 1 ;
			time.tm_mday = ToLong( sTextCopy.Mid(  6, 2 ) );
		}
		else
		if( length == 6 )		// only date, year at last, attention cannot be differentiated with single time format including seconds
		{
			time.tm_mday = ToLong( sTextCopy.Mid(  0, 2 ) );
			time.tm_mon  = ToLong( sTextCopy.Mid(  2, 2 ) ) - 1 ;
			long lYear = MakeYear( sTextCopy.Mid(  4, 2 ) );
			time.tm_year = lYear - 1900 ;
		}
		else
		if( length == 4 )		// short time
		{
			time.tm_year = 70 ;		// must be set minimally for mktime to calculate a valid value, otherwise result is -1 (as checked in assertion)
			time.tm_mon = 1 ;		// must be set minimally for mktime to calculate a valid value, otherwise result is -1 (as checked in assertion)
			time.tm_hour = ToLong( sTextCopy.Mid(  0, 2 ) );
			time.tm_min  = ToLong( sTextCopy.Mid(  2, 2 ) );
		}
		else
		if( length == 0 )		// use value specified in "default" parameter
		{
			if( pcDefault )
				return ToTime( pcDefault );
			else
			{
				//ASSERT( 0 );	// unhandled time format string
				TRACE( "%s(%d): ToTime(), no default time available\n", __FILE__, __LINE__ );
			};
		}
		else
		{
			//ASSERT( 0 );	// unhandled time format string
			TRACE( "%s(%d): ToTime(), unhandled time format string\n", __FILE__, __LINE__ );
		};
	};	// dottet time string

	time_t tt = mktime( &time );
	//ASSERT( tt != (time_t )-1 );		// bad guy ... someone fed me up with invalid data
	if( tt == (time_t )-1 )
	{
		TRACE( "%s(%d): ToTime(), mktime cant convert data, set to 0x%0.8x\n", __FILE__, __LINE__, tt );
	};
	return tt ;
};	// ToTime

// 050322 MVy: convert a long to a string and fix the size of the result string
CString ToFormat( long value, unsigned long size )
{
	CString format ;
	format.Format( "%%0.%dd", size );
	CString sValue ;
	sValue.Format( format, value );
	return sValue ;
};	// ToFormat

// 050322 MVy: convert a time into most common string format like YYYYMMDDhhmmss
CString FromTime( CTime time, const char* format )
{
	CString sTime ;
	sTime += ToFormat( time.GetYear(), 4 );
	sTime += ToFormat( time.GetMonth(), 2 );
	sTime += ToFormat( time.GetDay(), 2 );
	sTime += ToFormat( time.GetHour(), 2 );
	sTime += ToFormat( time.GetMinute(), 2 );
	sTime += ToFormat( time.GetSecond(), 2 );
	ASSERT( sTime.GetLength() == 14 );
	ASSERT( sTime.Left(0) != '0' );
	return sTime ;
};	// FromTime

// 050322 MVy: data table access helper function
CString GetOCCValue( const RecordSet* rec, const char* col )
{
	CString value = "" ;
	try
	{
		int ndx = ogBCD.GetFieldIndex( "OCC", col );
		value = rec->Values.GetAt( ndx );
	}
	catch( CException ex )
	{
		TRACE( "%s(%d): GetOCCValue(), accessing column failed.\n", __FILE__, __LINE__ );
	};
	return value ;
};	// GetOCCValue


// 050425 MVy: set time values of a datetime
CTime OverwriteTime( CTime &datetime, unsigned char hour, unsigned char minute, unsigned char second )
{
	ASSERT( INRANGE( 0, hour, 23 ) );
	ASSERT( INRANGE( 0, minute, 59 ) );
	ASSERT( INRANGE( 0, second, 59 ) );
	struct tm time = *datetime.GetLocalTm(NULL);		// make a copy
	time.tm_hour = hour ;
	time.tm_min = minute ;
	time.tm_sec = second ;
	datetime = mktime( &time );
	return datetime ;
};	// OverwriteTime

// 050425 MVy: overwrite the time hh:mm:ss of the first time by the values of the second time;
//	useful for sth. like concatting an only date value with a only time value
CTime OverwriteTime( CTime &datetime, const CTime &newtime )
{
	return OverwriteTime( 
		datetime, 
		newtime.GetHour(), 
		newtime.GetMinute(), 
		newtime.GetSecond()
	);
};	// OverwriteTime

// 050425 MVy: overwrite the date dd.mm.yy of the first time by the values of the second time;
//	useful for sth. like concatting an only date value with a only time value
CTime OverwriteDate( CTime &datetime, const CTime &newdate )
{
	struct tm time = *datetime.GetLocalTm();		// make a copy
	time.tm_year = newdate.GetYear();
	time.tm_mon = newdate.GetMonth();
	time.tm_mday = newdate.GetDay();
	datetime = mktime( &time );
	return datetime ;
};	// OverwriteTime


///This function is used to get the connected gates for a position , PRF 8494.
int CBasicData::GetRelatedGatesForPosition(CString lpPosUrno,CStringArray& ropGates)
{

	CString olStrAlocUrno = ogBCD.GetField("ALO","ALOC","GATPOS","URNO");

	
	CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olStrAlocUrno,lpPosUrno,"URNO");
	CString olGatName;

	CCSPtrArray<RecordSet> prlRecords;
	ogBCD.GetRecords("SGM", "USGR", olSgrUrno, &prlRecords);
	for (int i = 0; i < prlRecords.GetSize(); i++)
	{
		if (ogBCD.GetField("GAT","URNO",prlRecords[i].Values[ogBCD.GetFieldIndex("SGM","UVAL")],"GNAM",olGatName ))
			ropGates.Add(olGatName);
		else
			continue;
	}
	return ropGates.GetSize();
}




long CBasicData::GetNextUrno(CString opTable)
{
	long llUrno;

	if(bgNumTabKeys)
	{
		if(opTable == "CCA")
		{
			if(omNewUrnosCCA.GetSize() == 0)
			{
				GetManyUrnos(imNextGMUAmountCCA, omNewUrnosCCA, "CCATAB");
				if(imNextGMUAmountCCA <= 240)
					imNextGMUAmountCCA = imNextGMUAmountCCA + imNextGMUAmountCCA;
			}

			llUrno = omNewUrnosCCA[0];
			omNewUrnosCCA.RemoveAt(0);
			return llUrno;
		}


		if(opTable == "AFT")
		{
			if(omNewUrnosAFT.GetSize() == 0)
			{
				GetManyUrnos(imNextGMUAmountAFT, omNewUrnosAFT, "AFTTAB");
				if(imNextGMUAmountAFT <= 240)
					imNextGMUAmountAFT = imNextGMUAmountAFT + imNextGMUAmountAFT;
			}

			llUrno = omNewUrnosAFT[0];
			omNewUrnosAFT.RemoveAt(0);
			return llUrno;
		}
		if(opTable == "CFL")
		{
			if(omNewUrnosCFL.GetSize() == 0)
			{
				GetManyUrnos(imNextGMUAmountCFL, omNewUrnosCFL, "CFLTAB");
				if(imNextGMUAmountCFL <= 240)
					imNextGMUAmountCFL = imNextGMUAmountCFL + imNextGMUAmountCFL;
			}

			llUrno = omNewUrnosCFL[0];
			omNewUrnosCFL.RemoveAt(0);
			return llUrno;
		}
	}


	if(omNewUrnos.GetSize() == 0)
	{
		GetManyUrnos(imNextGMUAmount, omNewUrnos);
		if(imNextGMUAmount <= 240)
			imNextGMUAmount = imNextGMUAmount + imNextGMUAmount;
	}


	llUrno = omNewUrnos[0];

	omNewUrnos.RemoveAt(0);

	return llUrno;

}



bool CBasicData::GetManyUrnos(int ipAnz, CUIntArray &opUrnos, CString opTable)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[100000];
	char pclSelection[124];

	strcpy(pclSelection, opTable);

	while(ilRc == false)
	{
		sprintf(pclTmpDataBuf, "%d", ipAnz);
		ilRc = CedaAction("GMU", pclSelection, "", pclTmpDataBuf);
	}

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= ipAnz) ; ilItemNo++ )
		{
			char pclTmpBuf[128];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			opUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}


