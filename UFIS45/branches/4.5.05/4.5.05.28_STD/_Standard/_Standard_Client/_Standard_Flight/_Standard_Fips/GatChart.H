// stafchrt.h : header file
//

#ifndef _GATCHRT_
#define _GATCHRT_

#include <GatGantt.h>
#include <ccsdragdropctrl.h>
#include <CCS3dStatic.h>
#include <CCSGlobl.h>
#include <CCSTimeScale.h>

#ifndef _CHART_STATE_
#define _CHART_STATE_

//enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

/////////////////////////////////////////////////////////////////////////////
// GatChart frame

class GatDiagramViewer;
class GatDiagram;

class GatChart : public CFrameWnd
{
	friend GatDiagram;

    DECLARE_DYNCREATE(GatChart)
public:
    GatChart();           // protected constructor used by dynamic creation
    virtual ~GatChart();
    int GetHeight();
	// Grouping
	  CCSButtonCtrl  *GetChartButtonPtr(void) { return &omButton; };
 
    int GetState(void) { return imState; };
    void SetState(int ipState) { imState = ipState; };
	// Grouping
	int GetGroupNo() { return imGroupNo; };
 
private:
    
    CCSTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CCSTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    GatDiagramViewer *GetViewer(void) { return pomViewer; };
	// Grouping
	  void SetViewer(GatDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
		imGroupNo = ipGroupNo;
    };
    void SetMarkTime(CTime opStatTime, CTime opEndTime);
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    GatGantt *GetGanttPtr(void) { return &omGantt; };
    CCS3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };
	CCS3DStatic *GetCountTextPtr(void) { return pomCountText; };

    int imState;
    int imHeight;
    
    CCS3DStatic *pomTopScaleText;
	CCS3DStatic *pomCountText;
    
    CCSTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    GatDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
    CTimeSpan omInterval;
    GatGantt omGantt;

    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
    int imStartTopScaleTextPos;
    int imStartVerticalScalePos;

// Drag-and-drop section
    CCSDragDropCtrl m_ChartWindowDragDrop;
    CCSDragDropCtrl m_CountTextDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;

//-DTT Jul.22-----------------------------------------------------------
// No more routine ProcessPartTimeAssignment(), we use a new dialog in
// file "dassignp.h" instead.
//	void ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//  void ProcessPartTimeAssignment(CCSDragDropCtrl *popDragDropCtrl);
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//----------------------------------------------------------------------


// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(GatChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
	// Grpouping
   afx_msg void OnChartButton();
    afx_msg LONG OnChartButtonRButtonDown(UINT, LONG);
 	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    // Grouping 
	CCSButtonCtrl omButton;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _GATCHRT_
