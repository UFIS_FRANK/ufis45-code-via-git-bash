// DailyScheduleTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <process.h>
#include <fpms.h>
#include <CCSDefines.h>
#include <CedaCfgData.h>
#include <CViewer.h>
#include <PrivList.h>
#include <SeasonFlightPropertySheet.H>
#include <CCSGlobl.h>
#include <CCSddx.h>
#include <DailyScheduleTableDlg.h>
#include <CCSBcHandle.h>
#include <AskBox.h>
#include <RotationDlg.h>
#include <RotGroundDlg.h>
#include <cedabasicdata.h>
#include <RotationRegnDlg.h>
#include <RotationAltDlg.h>
#include <RotationActDlg.h>
#include <RotationAptDlg.h>
#include <ReportSelectDlg.h>
#include <PrivList.h>
#include <FlightSearchTableDlg.h>
#include <RotationCedaFlightData.h>
#include <CedaFlightUtilsData.h>
#include <Utils.h>
#include <CedaAptLocalUtc.h>
#include <AwBasicDataDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void DailyTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// DailyScheduleTableDlg dialog


DailyScheduleTableDlg::DailyScheduleTableDlg(const CStringArray &opColumnsToShow, CWnd* pParent /*=NULL*/)
	: CDialog(DailyScheduleTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DailyScheduleTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomDailyScheduleTable = NULL;
	
	omViewer.SetViewerKey("DAILYFLIGHT");
	omViewer.SetColumnsToShow(opColumnsToShow);
	omColumnsToShow.Copy(opColumnsToShow);

	// Workaround fuer den MessageHandler beim Beenden des InlineEdit, sonst endlos
	fmDlg1 = false;
	fmDlg2 = false;

	CDialog::Create(DailyScheduleTableDlg::IDD, pParent);
	m_key = "DialogPosition\\DailyRotations";

}


DailyScheduleTableDlg::~DailyScheduleTableDlg()
{

	ogDailyCedaFlightData.ClearAll();
	ogDdx.UnRegister(this,NOTUSED);

	delete pomDailyScheduleTable;

}


void DailyScheduleTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DailyScheduleTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DailyScheduleTableDlg, CDialog)
	//{{AFX_MSG_MAP(DailyScheduleTableDlg)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SCHLIESSEN, OnSchliessen)
	ON_CBN_SELCHANGE(IDC_COMBO_ANSICHT, OnSelchangeComboAnsicht)
	ON_BN_CLICKED(IDC_BUTTON_ANSICHT, OnButtonAnsicht)
	ON_BN_CLICKED(IDC_TEILEN, OnTeilen)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_BN_CLICKED(IDC_AENDERN, OnAendern)
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_WM_LBUTTONUP()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DailyScheduleTableDlg message handlers

BOOL DailyScheduleTableDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\DailyRotations";

	omViewer.SetParentDlg(this);

	
	// TODO: Add extra initialization here
	omDragDropTarget.RegisterTarget(this, this);
//	omDragDropObject.RegisterTarget(this, this);
	CRect olRect;

    GetClientRect(&olRect);
	// Hoehe der Buttonleiste des Dilaogs
    imDialogBarHeight = olRect.bottom - olRect.top;

	// set caption
	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1552);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONSCHEDULE_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;


	ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
	left = (int)((ilCXMonitor-ilWidth)/2);
	right = left + ilWidth;

//	top = 65 ;
	top = 95 ;
	bottom = ::GetSystemMetrics(SM_CYSCREEN);

//	MoveWindow(CRect(left, top, right, bottom));
//	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );

    pomDailyScheduleTable = new CCSTable;
	// kein Multiselect
	pomDailyScheduleTable->SetSelectMode(0/*LBS_MULTIPLESEL | LBS_EXTENDEDSEL*/);
	pomDailyScheduleTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomDailyScheduleTable->SetHeaderSpacing(0);

	// um InlineEdit zu erm�glichen
	pomDailyScheduleTable->SetTableEditable(true);
	// die ALT-Taste muss gleichzeitig gedrueckt werden
//rkr26032001
//	pomDailyScheduleTable->SetIPEditModus(false);
	pomDailyScheduleTable->SetIPEditModus(!bgDailyWithAlt);


    GetClientRect(&olRect);

	// Hoehe der Buttonleiste ber�cksichtigen
	olRect.top = olRect.top + imDialogBarHeight;

    olRect.InflateRect(1, 1);

	pomDailyScheduleTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	omViewer.Attach(pomDailyScheduleTable);

	omViewer.ChangeViewTo("<Default>");
	UpdateComboAnsicht();
	UpdateView();
		
	ogDdx.Register(this, S_DLG_GET_PREV, CString("FLIGHTDLG"), CString("Flight Update/new"), DailyTableCf);
	ogDdx.Register(this, S_DLG_GET_NEXT, CString("FLIGHTDLG"), CString("Flight Update/new"), DailyTableCf);

	ShowWindow(SW_SHOWNORMAL);



    CButton *polCB;

	polCB = (CButton *) GetDlgItem(IDC_BUTTON_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("DAILYTAB_CB_View"),polCB);

	polCB = (CButton *) GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("DAILYTAB_CB_Search"),polCB);

	polCB = (CButton *) GetDlgItem(IDC_TEILEN);
	SetpWndStatAll(ogPrivList.GetStat("DAILYTAB_CB_Split"),polCB);


	polCB = (CButton *) GetDlgItem(IDC_PRINT);
	SetpWndStatAll(ogPrivList.GetStat("DAILYTAB_CB_Print"),polCB);


	polCB = (CButton *) GetDlgItem(IDC_EXCEL);
	SetpWndStatAll(ogPrivList.GetStat("DAILYTAB_CB_Excel"),polCB);

	/*
	m_resizeHelper.Init(this->m_hWnd);

	m_resizeHelper.Fix(IDC_BUTTON_ANSICHT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_COMBO_ANSICHT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SEARCH,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_EXCEL,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PRINT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SCHLIESSEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_TEILEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	*/

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DailyScheduleTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



void DailyScheduleTableDlg::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void DailyScheduleTableDlg::SetColumnsToShow(const CStringArray &opColumnsToShow)
{

	omViewer.SetColumnsToShow(opColumnsToShow);

}


void DailyScheduleTableDlg::UpdateComboAnsicht()
{
	CStringArray olArray;
	CStringArray olStrArr;
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);
	polCB->ResetContent();
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
} 


void DailyScheduleTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomDailyScheduleTable != NULL && ::IsWindow(this->pomDailyScheduleTable->m_hWnd))
	{
		CRect olrectTable;
		GetClientRect(&olrectTable);
		olrectTable.DeflateRect(1,1);     // hiding the CTable window border
		this->pomDailyScheduleTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top + imDialogBarHeight, olrectTable.bottom);

		this->pomDailyScheduleTable->DisplayTable();
	}

	this->Invalidate();

/*
	CDialog::OnSize(nType, cx, cy);

	if(pomDailyScheduleTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.top = rect.top + imDialogBarHeight;
		rect.bottom = rect.bottom;// - imDialogBarHeight;

		rect.InflateRect(1, 1);     // hiding the CTable window border
    
		pomDailyScheduleTable->SetPosition(rect.left, rect.right, rect.top, rect.bottom);
	}
*/	
}


void DailyScheduleTableDlg::OnPrint() 
{

	omViewer.PrintTableView();
	
}


void DailyScheduleTableDlg::OnExcel() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

	CString olFileName;

	omViewer.CreateExcelFile(pclTrenner);

	olFileName = omViewer.omFileName;

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 
//	strcpy(pclTmp, GetString(IDS_STRING1386)); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_EXCELPATH_ERROR), GetString(ST_FEHLER), MB_ICONERROR);

/*
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeperator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);


	omViewer.CreateExcelFile(pclTrenner);


	char pclTmp[256];

	strcpy(pclTmp, GetString(IDS_STRING1386)); 

	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv(_P_NOWAIT, pclExcelPath, args);
*/	
}


void DailyScheduleTableDlg::OnAendern() 
{

	DAILYSCHEDULE_LINEDATA *prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(pomDailyScheduleTable->GetCurrentLine());
	if (prlTableLine != NULL)
	{
		DAILYFLIGHTDATA *prlFlight;

		if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);

		if(prlFlight != NULL)
		{
			CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, prlFlight->Adid[0], prlFlight->Tifa, bgDailyLocal, 'D');
		}

	}
	
}


void DailyScheduleTableDlg::OnSchliessen() 
{

	if (bgPrintDlgOpen)
		return;

	if(pomDailyScheduleTable != NULL)
		pomDailyScheduleTable->DestroyWindow();
	CDialog::OnCancel();
	CDialog::DestroyWindow();
	
	pomDailyScheduleTable = NULL;
	pogDailyScheduleTableDlg = NULL;
	delete this;
}


void DailyScheduleTableDlg::OnCancel() 
{

	if (bgPrintDlgOpen)
		return;
	else
		CDialog::OnCancel();

}


void DailyScheduleTableDlg::UpdateView()
{

	CString olViewName = "<Default>";

	if (olViewName.IsEmpty() == TRUE)
	{
	//	olViewName = ogCfgData.rmUserSetup.STLV;
	}

	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomDailyScheduleTable->GetCTableListBox()->SetFocus();

}


void DailyScheduleTableDlg::ShowAnsicht()
{

	if(ogPrivList.GetStat("DAILYTAB_CB_View") != '1')
		return;

	SeasonFlightTablePropertySheet olDlg("DAILY", this, &omViewer, 0, GetString(IDS_STRING2750));
	olDlg.m_PSUniSortPage.InitFieldAndDescArrays(omColumnsToShow, omColumnsToShow);

	if(olDlg.DoModal() != IDCANCEL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		UpdateWindow();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(omViewer.GetViewName());
		UpdateComboAnsicht();
	}
	else
	{
		UpdateWindow();
		UpdateComboAnsicht();
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


void DailyScheduleTableDlg::OnButtonAnsicht() 
{

	ShowAnsicht();
	HandleWindowText(omViewer.CheckPostFlight(NULL));

}


void DailyScheduleTableDlg::OnSelchangeComboAnsicht()
{

	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);

	int ilItemID;
	CString olView;

	if ((ilItemID = polView->GetCurSel()) != CB_ERR)
	{
		polView->GetLBText(ilItemID, olView);
 
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(olView);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	HandleWindowText(omViewer.CheckPostFlight(NULL));
}


void DailyScheduleTableDlg::OnTeilen() 
{

	int ilAnz = -1;

	ilAnz = (pomDailyScheduleTable->GetCTableListBox())->GetCurSel();
	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING975), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	DAILYSCHEDULE_LINEDATA *prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(pomDailyScheduleTable->GetCurrentLine());
	BOOL bplPost = omViewer.CheckPostFlight(prlTableLine);
	HandleWindowText(bplPost);
	if (bplPost)
	{
		MessageBox(GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
		return;
	}

	if (prlTableLine != NULL && ((prlTableLine->AUrno != 0) && (prlTableLine->DUrno != 0)) && (prlTableLine->AUrno != prlTableLine->DUrno != 0) )
	{
		CedaFlightUtilsData olFlightUtilsData;
		olFlightUtilsData.SplitFlight(prlTableLine->AUrno, ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)->Rkey, false);		
	}	
}



void DailyScheduleTableDlg::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogDailyCedaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), DAILYROTATIONS);
}


void DailyScheduleTableDlg::SelectFlight(long lpUrno)
{
	omViewer.SelectLine(lpUrno);
}


bool DailyScheduleTableDlg::ShowFlight(long lpUrno)
{	
	
	return omViewer.ShowFlight(lpUrno);
}



LONG DailyScheduleTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{
	//if( omDragDropObject.GetDataCount() ) return -1L ;

	if(ogPrivList.GetStat("DAILYTAB_CB_JOIN") != '1') return 0L;

	DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA*)pomDailyScheduleTable->GetTextLineData(wParam);
	if( !prlLine ) return -1L;

	CPoint point;
	::GetCursorPos(&point);
	pomDailyScheduleTable->pomListBox->ScreenToClient(&point);

	int ilLine = pomDailyScheduleTable->GetLinenoFromPoint(point);
	int ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(point);

	CString sColName = omViewer.GetColumnByIndex( ilCol );
	TRACE( "drag begin: %s\n", sColName );
	if( sColName.IsEmpty() ) return -1L;		// weder Arrival noch Departure

	long llUrno2 = 0 ;		// put into d&d structure only if second URNO is set
	switch( sColName[0] )
	{
		case 'R': return -1L; break ;
		case 'A': llUrno2 =	prlLine->AUrno ; break ;	// Arrival-Teil der Zeile
		case 'D': llUrno2 =	prlLine->DUrno ; break ;	// Departure-Teil der Zeile
		case 'X': return -1L ; break ;		// 050311 MVy: don�t know, I start my new internal column names with an "X"
		default: ASSERT(0); break ;		// unhandled data column under cursor, what comes here?
	};	// switch

	if( !llUrno2 ) return -1L ;
	omDragDropObject.CreateDWordData(DIT_FLIGHT, 1);
	omDragDropObject.AddDWord(llUrno2);

	//if( !omDragDropObject.m_hDataSource ) return -1 ;		// 050310 MVy: sometime m_hDataSource is NULL -> this will crash in ole d&d routines with an assertion  BUT  a got a crash with a filled handle valid???, too ... :-(((

	// 050311 MVy: check this
	try
	{
		ogBcHandle.BufferBc();
		omDragDropObject.BeginDrag();
		ogBcHandle.ReleaseBuffer();
	}
	catch(...)
	{
		char pclExcText[512]="";
		sprintf( pclExcText, "%s(%d) : %s\n", __FILE__, __LINE__, "Drag Drop Failed" );
		TRACE( pclExcText );
	};

	return 0L;
}


LONG DailyScheduleTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	if(ogPrivList.GetStat("DAILYTAB_CB_JOIN") != '1') return 0L;

	CPoint olDropPosition;
	::GetCursorPos(&olDropPosition);
	pomDailyScheduleTable->pomListBox->ScreenToClient(&olDropPosition);    

	int ilLine = pomDailyScheduleTable->GetLinenoFromPoint(olDropPosition);
	int ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(olDropPosition);
	if( ilCol < 0 || ilLine < 0 || 0 > ilLine || ilLine > omViewer.omLines.GetSize()-1 ) return -1L;		// position not valid, no data area under cursor

	int ilClass = omDragDropTarget.GetDataClass();
	if( ilClass != DIT_FLIGHT ) return -1L;		// position not usable, no flight under cursor

	long llUrno = omDragDropTarget.GetDataDWord(0);
	ASSERT( llUrno );		// really valid URNO ?

	DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA *) pomDailyScheduleTable->GetTextLineData(ilLine);
	if(prlLine == NULL) return -1L;		// position not usable, no filled data row under cursor

	CString sColName = omViewer.GetColumnByIndex(ilCol);
	TRACE( "drag over: %s\n", sColName );
	if( sColName.IsEmpty() ) return -1L;		// weder Arrival noch Departure

	long llUrno2 = 0 ;		// check only if second URNO is set
	switch( sColName[0] )
	{
		case 'A': llUrno2 = prlLine->AUrno; break ;		// Arrival
		case 'D': llUrno2 = prlLine->DUrno; break ;		// Departure
		case 'R': return -1L ; break ;
		case 'X': return -1L ; break ;		// 050311 MVy: don�t know, I start my new internal column names with an "X"
		default: ASSERT(0); break ;		// unhandled data column under cursor, what comes here?
	};	// switch

	if( llUrno2 && !ogDailyCedaFlightData.JoinFlightPreCheck( llUrno, llUrno2) ) return -1L;		// not my partner

	return 0L;
}

////////////////////////////////////////////////////////////////////////////
// Drop einer Drag&Drop Aktion, wo auch immer.
//
LONG DailyScheduleTableDlg::OnDrop(UINT i, LONG l)
{

	if(ogPrivList.GetStat("DAILYTAB_CB_JOIN") != '1')
		return 0L;


	if(OnDragOver(0,0) != 0L) //Last validation, sicher ist sicher
		return -1L;


	int ilLine = -1;
	int ilCol = -1;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
    pomDailyScheduleTable->pomListBox->ScreenToClient(&olDropPosition);    

	ilLine = pomDailyScheduleTable->GetLinenoFromPoint(olDropPosition);
	ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(olDropPosition);

	if (!(0 <= ilLine && ilLine <= omViewer.omLines.GetSize()-1))
		return -1L;	

	if(ilCol < 0 || ilLine < 0)
		return -1L;	


	long llUrno1 = 0;
	long llUrno2 = 0;

	DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA *) pomDailyScheduleTable->GetTextLineData(pomDailyScheduleTable->GetLinenoFromPoint(olDropPosition));

    int ilClass = omDragDropTarget.GetDataClass(); 


	llUrno1 = omDragDropTarget.GetDataDWord(0);


	// Arrival-Teil der Zeile
	if(omViewer.GetColumnByIndex(ilCol).Left(1) == "A")
	{
		llUrno2 = prlLine->AUrno;
		if(!ogDailyCedaFlightData.JoinFlightPreCheck(llUrno1, llUrno2))
		{
			MessageBox(GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
			return -1L;
		}
	}
	// Departure-Teil der Zeile
	if (omViewer.GetColumnByIndex(ilCol).Left(1) == "D")
	{	
		llUrno2 = prlLine->DUrno;
		if(!ogDailyCedaFlightData.JoinFlightPreCheck(llUrno1, llUrno2))
		{
			MessageBox(GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
			return -1L;
		}
	}
	// weder Arrival noch Departure
	if (omViewer.GetColumnByIndex(ilCol) == "")
		return -1L;
	
	if (omViewer.GetColumnByIndex(ilCol).Left(1) == "R")
		return -1L;

	DAILYFLIGHTDATA *prlAFlight = ogDailyCedaFlightData.GetFlightByUrno(llUrno1);
	DAILYFLIGHTDATA *prlDFlight = ogDailyCedaFlightData.GetFlightByUrno(llUrno2);

	Verbinden(prlAFlight, prlDFlight);
 	
	return -1L;

}


void DailyScheduleTableDlg::Verbinden(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight) 
{

	if(prpAFlight == NULL || prpDFlight == NULL)
		return;

	BOOL bplPost = omViewer.CheckPostFlight(prpAFlight, prpDFlight);
	HandleWindowText(bplPost);
	if (bplPost)
	{
		MessageBox(GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
		return;
	}

	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.JoinFlight(prpAFlight->Urno, prpAFlight->Rkey, prpDFlight->Urno, prpDFlight->Rkey, false);
	
	return;
}


LONG DailyScheduleTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{

	// anhand der Mousposition die Spalte ermitteln
	int ilCol = 0;
	CPoint point;
    ::GetCursorPos(&point);
    pomDailyScheduleTable->pomListBox->ScreenToClient(&point);    

	UINT ipItem = wParam;
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ipItem);

	// Inhalt in der Zeile?
	if (prlTableLine != NULL)
	{
		// Spaltennummer
		ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(point);
	}

	DAILYFLIGHTDATA *prlFlight;
	if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);

	// Behandlung: Groundmovement
	if (strcmp (prlFlight->Ftyp, "G") == 0)
	{
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
/*
			//the towingdialog now check postflight by themself
			if (omViewer.CheckPostFlight(prlTableLine))
				pogRotGroundDlg->setStatus(RotGroundDlg::Status::POSTFLIGHT);
			else
				pogRotGroundDlg->setStatus(RotGroundDlg::Status::UNKNOWN);
*/
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		// gueltiger Wert der Spalte
		if (ilCol < 0)
			return 0L;

		// Klick auf Arrival oder Departure
		char clFPart = ' ';
		if (omViewer.GetColumnByIndex(ilCol).Left(1) == "A")
		{
			clFPart = 'A';
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			if(prlFlight == NULL) 
				return 0L;
		}
		else
		{
			clFPart = 'D';
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			if(prlFlight == NULL) 
				return 0L;
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, clFPart, bgDailyLocal, false);
	}
	else
	{
		if (prlTableLine != NULL)
		{
			char clAdid='X';
			//DAILYFLIGHTDATA *prlFlight;

			if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			{
				prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
				clAdid = 'D';
			}
			else
			{
				clAdid = 'A';
			}
			
			// Rotationsmaske aufrufen
			if(prlFlight != NULL)
			{
				CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgDailyLocal, 'D');
			}
		}
	}

	return 0L;

}



LONG DailyScheduleTableDlg::OnTableRButtonDown(UINT wParam, LONG lParam)
{

	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	pomDailyScheduleTable->SelectLine(wParam, TRUE);


	DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA*)pomDailyScheduleTable->GetTextLineData(wParam);
	
	DAILYFLIGHTDATA *prlFlight = NULL;
	DAILYFLIGHTDATA *prlAFlight = NULL;

	if(prlLine == NULL)
		return 0L;

	// matching keys pressed? 
//rkr26032001
	if (bgDailyWithAlt && !(GetKeyState(VK_MENU) & 0xff80))
		return 0L;

	if (!bgDailyWithAlt && (GetKeyState(VK_MENU) & 0xff80))
		return 0L;
//rkr26032001



	if (omViewer.CheckPostFlight(prlLine))
		return 0L;

	int ilCurrCol = olNotify.Column;
	if(ilCurrCol < 0)
		return 0L;


	CString olColomnName;
	// Name der aktuellen Spalte
	olColomnName = omViewer.GetColumnByIndex(ilCurrCol);
	// ersten Buchstaben abschneiden
	olColomnName = olColomnName.Right(olColomnName.GetLength() - 1);

	// Mausklick im Ankunfts- oder Abflugssteil --> entsprechenden Flug holen
	if(omViewer.GetColumnByIndex(ilCurrCol).Left(1) == "A")
	{
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno);
	}

	if(omViewer.GetColumnByIndex(ilCurrCol).Left(1) == "D")
	{
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno);
	}
		
	// kein Flug --> und tschues...
	if(prlFlight == NULL)
		return 0L;

	// Kann die aktuelle Zeit abgespeichert werden
	bool blSave = false; 

	// aktuelle Zeit
	CTime olCurrTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrTime);
	

	char pclValue[512];
	char pclField[64];

	// aktuelle Zeit konvertieren
	sprintf(pclValue, "%s", CTimeToDBString(olCurrTime, TIMENULL));

	// Flug gecanceld oder NoOp
	if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
		return 0L;


	if((CString(prlFlight->Ftyp) == "B") || (CString(prlFlight->Ftyp) == "Z"))
		return 0L;


	// Ankunftsteil der Zeile
	if(omViewer.GetColumnByIndex(ilCurrCol).Left(1) == "A")
	{
		if (olColomnName == "TMOA")
		{
			if((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G"))
				return 0L;

			if(ogPrivList.GetStat("ROTATIONTAB_A_UtcTmoa") != '1')
				return 0L;

			strcpy(pclField, "TMAU");			

			if((prlFlight->Land <= olCurrTime) && (prlFlight->Land != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING950), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}
			blSave = true;
		
		}

		if (olColomnName == "LAND")
		{

			if((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G"))
				return 0L;
			
			if(ogPrivList.GetStat("ROTATIONTAB_A_UtcLand") != '1')
				return 0L;
			
			strcpy(pclField, "LNDU");			

			if((prlFlight->Onbl <= olCurrTime) && (prlFlight->Onbl != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING951), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}
			blSave = true;
		}

		if(olColomnName == "AIRB")
		{
			if(ogPrivList.GetStat("ROTATIONTAB_AG_UtcOnbl") != '1')
				return 0L;

			if(!((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G")))
			{
//rkr230401
				CTime olTifa = TIMENULL;
				FlightDataTifa(prlFlight, olTifa);

				// tifa before atd
				if((olTifa <= olCurrTime) && (olCurrTime != TIMENULL) && (olTifa != TIMENULL))
				{
					MessageBox(GetString(IDS_STRING351), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				//atd before ofbl
				if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl >= olCurrTime) && (olCurrTime != TIMENULL))
				{
					MessageBox(GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				// no ofbl
				if(prlFlight->Ofbl == TIMENULL)
				{
					if(MessageBox(GetString(IDS_STRING387), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}


/*				if(prlFlight->Land == TIMENULL)
				{
					MessageBox(GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				if(prlFlight->Land >= olCurrTime)
				{
					MessageBox(GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}*/
//rkr230401
			}

			strcpy(pclField, "AIRU");
			blSave = true;
		}
	}

	// Abflugsteil der Zeile
	if(omViewer.GetColumnByIndex(ilCurrCol).Left(1) == "D")
	{
		// holen des Ankunftsdatensatzes, um die Landezeit zu checken
		prlAFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno);
		if (prlAFlight == NULL)
			return 0L;

		if(olColomnName == "AIRB")
		{
			if(ogPrivList.GetStat("ROTATIONTAB_AG_UtcOnbl") != '1')
				return 0L;

			if(!((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G")))
			{
				if(prlAFlight->Land == TIMENULL)
				{
					MessageBox(GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				if(prlAFlight->Land >= olCurrTime)
				{
					MessageBox(GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}
			}

			strcpy(pclField, "AIRU");
			blSave = true;
		}
	}

	if (blSave)
	{
		pomDailyScheduleTable->SetIPValue(olNotify.Line, ilCurrCol, olCurrTime.Format("%H:%M"));

		char pclSelection[256];
		sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
		ogDailyCedaFlightData.SaveSpecial(pclSelection, pclField, pclValue);
	}

	return 0L;

}



LONG DailyScheduleTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	int ilLineNo = pomDailyScheduleTable->GetCurSel();;
	if(ilLineNo >= 0)
	{
		DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA*)pomDailyScheduleTable->GetTextLineData(ilLineNo);
		if(prlLine != NULL)
		{
			BOOL bplPost = omViewer.CheckPostFlight(prlLine);
			HandlePostFlight(bplPost);
			//ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlLine->Urno));
		}
	}

	return 0L;

}


LONG DailyScheduleTableDlg::OnTableIPEdit(UINT wParam, LONG lParam)
{

	// keine Zeile selektieren
	pomDailyScheduleTable->SelectLine(-1);

	return  0L;

}

bool DailyScheduleTableDlg::OnAct35list(CString& opAct5, CString& opAct3) 
{
	bool blRet = false;
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT3+,ACT5,+ACFN+", opAct5);
//	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		opAct3 = polDlg->GetField("ACT3");	
		opAct5 = polDlg->GetField("ACT5");	
		blRet = true;
	}
	delete polDlg;

	return blRet;
}

bool DailyScheduleTableDlg::OnAct3list(CString& opAct5, CString& opAct3) 
{
	if (opAct3.IsEmpty() && !opAct5.IsEmpty())
	{
		return OnAct35list(opAct5, opAct3);
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", opAct3);
//	polDlg->SetSecState("SEASONDLG_CE_Act3");

	bool blRet = false;
	if(polDlg->DoModal() == IDOK)
	{
		opAct3 = polDlg->GetField("ACT3");	
		opAct5 = polDlg->GetField("ACT5");	
		blRet = true;
	}
	delete polDlg;

	return blRet;
}

LONG DailyScheduleTableDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;
	bool blRet = true;
	int ilDaten = 0;		// (1=nur Ankunft, 2=nur Abflug, 3=beides, 0=sonst)
	
	// damit, wenn vorher ein Dialog beendet wurde, nicht nochmal diese Funktion durchlaufen wird
	// CCSEdit und CCSTable schicken ein Killfocus
	if (fmDlg1 == true || fmDlg2 == true)
	{
		// R�cksetzung der Flags
		fmDlg1 = false;
		fmDlg2 = false;
		return 0L;
	}

	DAILYSCHEDULE_LINEDATA *prlLine = (DAILYSCHEDULE_LINEDATA*)pomDailyScheduleTable->GetTextLineData(prlNotify->Line);
	DAILYFLIGHTDATA *prlFlight = NULL;
	DAILYFLIGHTDATA *prlAFlight = NULL;

	// wenn keine Daten -> abhauen
	if (prlLine == NULL)
		return 0L;

	// ermittlen, wo Daten stehen
	if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno)) != NULL)
		ilDaten = 1;	// nur Anknunft
	if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno)) != NULL && ilDaten == 0)
		ilDaten = 2;	// nur Abflug
	if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno)) != NULL && ilDaten == 1)
		ilDaten = 3;	// Ankunft und Abflug
	// Datenpointer zuruecksetzen
	prlFlight = NULL;


	if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno)) == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno);

	// wenn keine Daten -> abhauen
	if (prlFlight == NULL)
		return 0L;

	char pclValue[1024] = "";
	char pclField[512] = "";
	CString olTmp3;
	CString olTmp4;
	// Name der zu bearbeitenden Spalte (prlNotify->Column) - ohne den ersten Buchstaben
	CString olColomnName;

	// Holen des Zelleninhaltes der editierten Zelle
	CString olTmp;
	pomDailyScheduleTable->GetTextFieldValue(prlNotify->Line, prlNotify->Column, olTmp);


	if(!prlNotify->IsChanged)
	{
		prlNotify->UserStatus = false;
		return 0L;
	}

	prlNotify->UserStatus = true;
	prlNotify->Status = false;
	bool blRotationData = false;

	// Name der Spalte
	olColomnName = omViewer.GetColumnByIndex(prlNotify->Column);
	// ersten Buchstaben abschneiden
	olColomnName = olColomnName.Right(olColomnName.GetLength() - 1);

	// -------
	// Rotation
	// -------
	if (omViewer.GetColumnByIndex(prlNotify->Column).Left(1) == "R")
	{
		if (olColomnName == "ACT5" || olColomnName == "ACT3")
		{
			blRotationData = true;
			CString olAct3User;
			CString olAct5User;
			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords); 
			prlRecords.DeleteAll();
			if (ilCount > 1)
			{
				olAct3User = prlNotify->Text;
				olAct5User = CString(prlFlight->Act5);
				if (!olAct5User.IsEmpty() && CString(prlFlight->Act3).IsEmpty())
				{
					olAct3User.TrimLeft();
					olAct3User.TrimLeft();
				}
				blRet = OnAct3list(olAct5User, olAct3User);
				if (blRet)
				{
					if (olAct3User.IsEmpty())
						olAct3User = " ";
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olAct3User, olAct5User);
				}
			}
			else
			{
				if(ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
					olAct3User = prlNotify->Text;
					olAct5User = olTmp;
					blRet = true;
				}
				else
				{
					blRet = false;
					fmDlg1 = true;
				}
			}
			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{
					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
					
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						
					
							if(olAct5User != olAct5)
							{
								if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
								{
									if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
									{
										ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
										ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
										ogBCD.Save("ACR");
									}
									else
									{
										blRet = false;
									}
								}
								else
										blRet = false;

								fmDlg1 = true;
							}
						
					}
				}
			}
		}

		if (olColomnName == "REGN")
		{
			blRotationData = true;
			prlNotify->Text.TrimLeft();
			prlNotify->Text.TrimRight();
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue,"");
			}
			else
			{
				CString olAct3;
				CString olAct5;

				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);

					if(ogPrivList.GetStat("REGN_INSERT") == '1' || ogPrivList.GetStat("REGN_INSERT") == ' ')
					{


	//					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
						RotationRegnDlg olDlg(this, olRegn, prlFlight->Act3, prlFlight->Act5);
						if(olDlg.DoModal() == IDOK)
						{
							strcpy(pclField, "REGN,ACT3,ACT5");			
							sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
							blRet = true;
						}
					}
					else
						blRet = false;

					fmDlg1 = true;
				}
			}
		}
	}

/*
	if (omViewer.GetColumnByIndex(prlNotify->Column).Left(1) == "R")
	{
		if (olColomnName == "ACT5" || olColomnName == "ACT3")
		{
			blRotationData = true;
			CString olAct3User;
			CString olAct5User;
			
			if(!prlNotify->Text.IsEmpty() && ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp))
			{
				strcpy(pclField, "ACT3,ACT5");			
				sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				olAct3User = prlNotify->Text;
				olAct5User = olTmp;
				blRet = true;
			}
			else
			{
				blRet = false;
				fmDlg1 = true;
			}
			
			if(blRet)
			{
				CString olRegn(prlFlight->Regn);
				if(!olRegn.IsEmpty())
				{
					CString olWhere;
					olRegn.TrimLeft();
					if(!olRegn.IsEmpty())
					{
						olWhere.Format("WHERE REGN = '%s'", olRegn);
						ogBCD.Read( "ACR", olWhere);
					}
					
					
					CString olAct3;
					CString olAct5;
					if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
					{
						if(olAct5User != olAct5)
						{
							if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
							{
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
								ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
								ogBCD.Save("ACR");
							}
							else
							{
								blRet = false;
							}
							fmDlg1 = true;
						}
					}
				}
			}
		}

		if (olColomnName == "REGN")
		{
			blRotationData = true;
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue," ");
			}
			else
			{
				CString olAct3;
				CString olAct5;

				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);

					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "REGN,ACT3,ACT5");			
						sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
	}
*/

	// -------
	// Ankunft
	// -------
	if (omViewer.GetColumnByIndex(prlNotify->Column).Left(1) == "A")
	{
		if (ilDaten == 2)
			return 0L;

		if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno)) == NULL)
			return 0L;

		// Daten Departure holen
		DAILYFLIGHTDATA *prlDFlight = ogDailyCedaFlightData.GetDeparture(prlFlight);

/*		if (olColomnName == "ACT5" || olColomnName == "ACT3")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "ACT3,ACT5");			
				strcpy(pclValue," , ");
			}
			else
			{
				CString olAct3User;
				CString olAct5User;
					
//				if(blRet = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp))
				if(blRet = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp))
				{
					strcpy(pclField, "ACT3,ACT5");			
//					sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
//					olAct5User = prlNotify->Text;
//					olAct3User = olTmp;
					olAct3User = prlNotify->Text;
					olAct5User = olTmp;
				}
				else
				{
//					RotationActDlg olDlg(this, prlNotify->Text);
					RotationActDlg olDlg(this, olTmp);
					if(olDlg.DoModal() == IDOK)
					{
						blRet = true;
						strcpy(pclField, "ACT3,ACT5");			
						sprintf(pclValue,"%s,%s", olDlg.m_Act3, olDlg.m_Act5);
						olAct5User = olDlg.m_Act5;
						olAct3User = olDlg.m_Act3;
					}
					fmDlg1 = true;
				}
				
				if(blRet)
				{
					CString olRegn(prlFlight->Regn);
					if(!olRegn.IsEmpty())
					{
						CString olWhere;
						olRegn.TrimLeft();
						if(!olRegn.IsEmpty())
						{
							olWhere.Format("WHERE REGN = '%s'", olRegn);
							ogBCD.Read( "ACR", olWhere);
						}
						
						
						CString olAct3;
						CString olAct5;
						if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
						{
							if(olAct5User != olAct5)
							{
								if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
								{
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
									ogBCD.Save("ACR");
								}
								else
								{
									blRet = false;
								}
								fmDlg1 = true;
							}
						}
					}
				}
			}
		}*/
		if (olColomnName == "AIRB")
		{
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			strcpy(pclField, "AIRU");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
//?			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			if (bgDailyLocal)
			{
				if (bgRealLocal)
					CedaAptLocalUtc::AptLocalToUtc(olTime, CString(prlFlight->Org4));
				else
					ogBasicData.LocalToUtc(olTime);
			}

			CTime olTifa = TIMENULL;
			FlightDataTifa(prlFlight, olTifa);

			// tifa before atd
			if((olTifa <= olTime) && (olTime != TIMENULL) && (olTifa != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING351), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}

			//atd before ofbl
			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl >= olTime) && (olTime != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}

			// no ofbl
			if(prlFlight->Ofbl == TIMENULL)
			{
				if(MessageBox(GetString(IDS_STRING387), GetString(IDS_STRING908), MB_YESNO) == IDNO)
					return 0L;
			}

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "CSGN")
		{
			strcpy(pclField, "CSGN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "SLOT" || olColomnName == "SLOU")
		{
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;
			
			strcpy(pclField, "SLOU");			

			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stoa);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "ETDC")
		{
			strcpy(pclField, "ETDC");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "ETAI")
		{
			strcpy(pclField, "ETAU");

			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stoa);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if (!CheckArrivalInputTimes(prlFlight, prlDFlight, olTime))
				return 0L;

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "FLNO")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
				strcpy(pclValue," , , , , ");
			}
			else
			{
				char pclFlno[10];
				strcpy(pclFlno, prlNotify->Text);
				CString olFlno;
				CString olAlc2;
				CString olFltn;
				CString olFlns;
				CString olAlc3;
				blRet = ogRotationFlights.CreateFlno( pclFlno, olFlno, olAlc2, olFltn, olFlns);
				if(blRet)
				{
					CString olStoStr = 	CTimeToDBString( prlFlight->Stoa, prlFlight->Stoa);
					
					if( ogBCD.GetField("ALT", "ALC2", "ALC3", olAlc2, olAlc2, olAlc3, olStoStr ))
					{
						strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
						sprintf(pclValue,"%s,%s,%s,%s,%s", olFlno, olAlc3, olAlc2, olFltn, olFlns);
					}
					else
						blRet = false;
				}
			}
		}
		if (olColomnName == "FTYP")
		{
		}
		if (olColomnName == "GTA1")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTA1,TGA1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTA1,TGA1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "GTA2")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTA2,TGA2");
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );
				if(blRet)
				{
					strcpy(pclField, "GTA2,TGA2");
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "GTD1")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD1,TGD1");
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );
				if(blRet)
				{
					strcpy(pclField, "GTD1,TGD1");
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "GTD2")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD2,TGD2");
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTD2,TGD2");
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "IFRA")
		{
			// Text kann nur I (Instrumental) V (Visual) oder Leereintrag sein
			if((prlNotify->Text == "I") || (prlNotify->Text == "V") || (prlNotify->Text == ""))
			{
				strcpy(pclField, "IFRA");			
				sprintf(pclValue,"%s", prlNotify->Text );
			}
			else
			{
				blRet = false;
			}
		}
		if (olColomnName == "ISRE")
		{
		}
		if (olColomnName == "LAND")
		{
			// Flug gestrichen? --> keine Angaben
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;
			
			// LAND wird vom Flighthandler beschrieben --> Eingabe des Users in LNDU
			strcpy(pclField, "LNDU");			

			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stoa);

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if((olTime == TIMENULL) && (prlFlight->Onbl != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING1424), GetString(ST_FEHLER), MB_ICONWARNING);
				fmDlg1 = true;
				return 0L;
			}
			// Zeiten inhaltlich richtig?
			if((prlFlight->Onbl <= olTime) && (olTime != TIMENULL) && (prlFlight->Onbl != TIMENULL))
			{
				MessageBox(GetString(IDS_STRING951), GetString(ST_FEHLER), MB_ICONWARNING);
				fmDlg1 = true;
				return 0L;
			}



			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "LSTU")
		{
		}
		if (olColomnName == "ONBE")
		{
		}
		if (olColomnName == "ONBL")
		{
			strcpy(pclField, "ONBU");			

			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stoa);

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			if(!((CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G")))
			{
				if(prlFlight->Land == TIMENULL)
				{
					MessageBox(GetString(IDS_STRING952), GetString(ST_FEHLER), MB_ICONWARNING);
					fmDlg1 = true;
					return 0L;
				}
				else
				{
					CString olText = olTime.Format("%H:%M %d.%m.%Y");
					CString olText2 = prlFlight->Land.Format("%H:%M %d.%m.%Y");
					
					if((prlFlight->Land >= olTime) && (olTime != TIMENULL))
					{
						MessageBox(GetString(IDS_STRING953), GetString(ST_FEHLER), MB_ICONWARNING);
						fmDlg1 = true;
						return 0L;
					}
				}				
			}

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "ORG4" || olColomnName == "ORG3")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "ORG4,ORG3");			
				strcpy(pclValue, " , ");
			}
			else
			{
				if(blRet = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
				{
					strcpy(pclField, "ORG4,ORG3");			
					sprintf(pclValue,"%s,%s", olTmp4, olTmp3);

					if((prlNotify->Text == CString(pcgHome4)) || (prlNotify->Text == CString(pcgHome)))
					{
						strcat(pclField, ",STOD");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Stoa, TIMENULL));
					}
				}
				else
				{
					RotationAptDlg olDlg(this, prlNotify->Text);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "ORG4,ORG3");			
						sprintf(pclValue,"%s,%s", olDlg.m_Apc4, olDlg.m_Apc3);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
		if (olColomnName == "PSTA")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "PSTA");			
				strcpy(pclValue," ");
			}
			else
			{
				blRet  = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "PSTA");			
					sprintf(pclValue,"%s", prlNotify->Text);
				}
			}
		}
/*
		if (olColomnName == "REGN")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue," ");
			}
			else
			{
				CString olAct3;
				CString olAct5;

				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);

					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "REGN,ACT3,ACT5");			
						sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
*/
		if (olColomnName == "RWYA")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "RWYA");			
				strcpy(pclValue," ");
			}
			else
			{
				CString olRnam;
				CString olRnum;
				if(!(blRet = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam )))
					blRet = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );

				if(blRet)
				{
					strcpy(pclField, "RWYA");
					sprintf(pclValue,"%s", olRnam);
				}
			}
		}
		if (olColomnName == "STAB")
		{
		}
		if (olColomnName == "STOADate")
		{
			strcpy(pclField, "STOA");
			CString olTime = prlFlight->Stoa.Format("%H:%M");
			CTime olDate = DateHourStringToDate(prlNotify->Text, olTime);
			if (olDate == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			sprintf(pclValue,"%s", CTimeToDBString(olDate, TIMENULL));
		}
		if (olColomnName == "STOATime")
		{
			strcpy(pclField, "STOA");
			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);

			if (olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if (!CheckArrivalInputTimes(prlFlight, prlDFlight, olTime))
				return 0L;

			//check of duplicated single flights
			if (bgDailyLocal) ogBasicData.UtcToLocal(olTime);
			CString	olText = IsDupFlight(prlFlight->Urno, "A", CString(prlFlight->Alc3), CString(prlFlight->Fltn), CString(prlFlight->Flns), olTime, bgDailyLocal);
			if (!olText.IsEmpty())
			{
				if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
					return 0L;
			}

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "TIFA")
		{
		}
		if (olColomnName == "TIFD")
		{
		}
		if (olColomnName == "TMOA")
		{
			strcpy(pclField, "TMAU");			
			CTime olRefDate = prlFlight->Stoa;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stoa);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			// no checkin rotationdlg!!
//			if (!CheckArrivalInputTimes(prlFlight, prlDFlight, olTime))
//				return 0L;

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "USEU")
		{
		}
		if (olColomnName == "VIA4")
		{
		}
		if (olColomnName == "VIAN")
		{
			strcpy(pclField, "VIAN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "TTYP")
		{
			blRet = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp);
			strcpy(pclField, "TTYP");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "CHGI")
		{
		}
		if (olColomnName == "REMP")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REMP");
				strcpy(pclValue," ");
			}
			else
			{
				CString olCode;
				// schauen, ob Eingabe des Users in Stammdaten vorhanden
				blRet = ogBCD.GetField("FID", "CODE", prlNotify->Text, "CODE", olCode);

				if(blRet)
				{
					strcpy(pclField, "REMP");
					sprintf(pclValue,"%s", olCode);
				}
			}
		}
		if (olColomnName == "RTYP")
		{
		}
		if (olColomnName == "ADID")
		{
		}
		if (olColomnName == "NXTI")
		{
		}
		if (olColomnName == "BLT1")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "BLT1,TMB1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "BLT1,TMB1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "PABA")
		{
		}
		if (olColomnName == "PAEA")
		{
		}
		if (olColomnName == "PDBA")
		{
		}
		if (olColomnName == "PDEA")
		{
		}
		if (olColomnName == "MING")
		{
		}
		if (olColomnName == "ADER")
		{
		}
		if (olColomnName == "VIAL")
		{
			strcpy(pclField, "VIAL");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
	}
	// ------
	// Abflug
	// ------
	if (omViewer.GetColumnByIndex(prlNotify->Column).Left(1) == "D")
	{
		if (ilDaten == 1)
			return 0L;
		
		if ((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno)) == NULL)
			return 0L;
		
		// Daten der Ankunft holen
		prlAFlight = ogDailyCedaFlightData.GetArrival(prlFlight);

/*		if (olColomnName == "ACT5" || olColomnName == "ACT3")
		{
			CString olAct3User;
			CString olAct5User;
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "ACT3,ACT5");			
				strcpy(pclValue," , ");
			}
			else
			{
				if(blRet = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp))
				{
					strcpy(pclField, "ACT3,ACT5");			
					sprintf(pclValue,"%s,%s", olTmp, prlNotify->Text);
					olAct3User = olTmp;
					olAct5User = prlNotify->Text;
				}
				else
				{
					RotationActDlg olDlg(this, prlNotify->Text);
					if(olDlg.DoModal() == IDOK)
					{
						blRet = true;
						strcpy(pclField, "ACT3,ACT5");			
						sprintf(pclValue,"%s,%s", olDlg.m_Act3, olDlg.m_Act5);
						olAct3User = olDlg.m_Act3;
						olAct5User = olDlg.m_Act5;
					}
					fmDlg1 = true;
				}
				
				if(blRet)
				{
					CString olRegn(prlFlight->Regn);
					if(!olRegn.IsEmpty())
					{

						
						CString olWhere;
						olRegn.TrimLeft();
						if(!olRegn.IsEmpty())
						{
							olWhere.Format("WHERE REGN = '%s'", olRegn);
							ogBCD.Read( "ACR", olWhere);
						}
						
						CString olAct3;
						CString olAct5;
						if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
						{
							if(prlNotify->Text != olAct5)
							{
								if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
								{
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3User, false);
									ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5User, false);
									ogBCD.Save("ACR");
								}
								else
								{
									blRet = false;
								}
								fmDlg1 = true;
							}
						}
					}
				}
			}
		}
*/
		if (olColomnName == "AIRB")
		{
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			strcpy(pclField, "AIRU");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			
			if(prlAFlight == NULL)
			{
				if(prlFlight->Ofbl == TIMENULL)
				{
					fmDlg1 = true;
					if(MessageBox(GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					fmDlg1 = true;
					if(MessageBox(GetString(IDS_STRING961), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					fmDlg1 = true;
					if(MessageBox(GetString(IDS_STRING962), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
				else
				{
					if((prlAFlight->Onbl >= olTime) && (olTime != TIMENULL))
					{
						fmDlg1 = true;
						MessageBox(GetString(IDS_STRING959), GetString(IDS_STRING908));
						return 0L;
					}
				}

			}
		
			if((prlFlight->Ofbl != TIMENULL) && (prlFlight->Ofbl >= olTime) && (olTime != TIMENULL))
			{
				fmDlg1 = true;
				MessageBox(GetString(IDS_STRING964), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "CSGN")
		{
			strcpy(pclField, "CSGN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "DES3" || olColomnName == "DES4")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "DES4,DES3");			
				strcpy(pclValue, " , ");
			}
			else
			{
				if(blRet = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
				{
					strcpy(pclField, "DES4,DES3");			
					sprintf(pclValue,"%s,%s", olTmp4, olTmp3);
					if((prlNotify->Text == CString(pcgHome4)) || (prlNotify->Text == CString(pcgHome)))
					{
						strcat(pclField, ",STOA");			
						strcat(pclValue, ",");			
						strcat(pclValue, CTimeToDBString(prlFlight->Stod, TIMENULL));
					}
				}
				else
				{
					RotationAptDlg olDlg(this, prlNotify->Text);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "DES4,DES3");			
						sprintf(pclValue,"%s,%s", olDlg.m_Apc4, olDlg.m_Apc3);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
		if (olColomnName == "ETDC")
		{
			strcpy(pclField, "ETDC");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "ETDI")
		{
			strcpy(pclField, "ETDU");

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if (!CheckDepartureInputTimes(prlFlight, prlAFlight, olTime))
				return 0L;

			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "FLNO")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
				strcpy(pclValue," , , , , ");
			}
			else
			{
				char pclFlno[10];
				strcpy(pclFlno, prlNotify->Text);
				CString olFlno;
				CString olAlc2;
				CString olFltn;
				CString olFlns;
				CString olAlc3;
				blRet = ogRotationFlights.CreateFlno( pclFlno, olFlno, olAlc2, olFltn, olFlns);
				if(blRet)
				{
					CString olStoStr = 	CTimeToDBString( prlFlight->Stod, prlFlight->Stod);
					
					if( ogBCD.GetField("ALT", "ALC2", "ALC3", olAlc2, olAlc2, olAlc3, olStoStr ))
					{
						strcpy(pclField, "FLNO,ALC3,ALC2,FLTN,FLNS");			
						sprintf(pclValue,"%s,%s,%s,%s,%s", olFlno, olAlc3, olAlc2, olFltn, olFlns);
					}
					else
						blRet = false;
				}
			}
		}
		if (olColomnName == "FTYP")
		{
		}
		if (olColomnName == "GTD1")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD1,TGD1");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTD1,TGD1");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "GTD2")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "GTD2,TGD2");			
				strcpy(pclValue," , ");
			}
			else
			{
				blRet  = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "GTD2,TGD2");			
					sprintf(pclValue,"%s,%s", prlNotify->Text, olTmp);
				}
			}
		}
		if (olColomnName == "IFRD")
		{
			if((prlNotify->Text == "I") || (prlNotify->Text == "V") || (prlNotify->Text == ""))
			{
				strcpy(pclField, "IFRD");			
				sprintf(pclValue,"%s", prlNotify->Text );
			}
			else
			{
				blRet = false;
			}
		}
		if (olColomnName == "ISRE")
		{
		}
		if (olColomnName == "LSTU")
		{
		}
		if (olColomnName == "OFBL")
		{
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;

			strcpy(pclField, "OFBU");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);

			if(prlAFlight == NULL)
			{
				fmDlg1 = true;
				if(MessageBox(GetString(IDS_STRING956), GetString(IDS_STRING908), MB_YESNO) == IDNO)
					return 0L;
			}
			else
			{
				if(prlAFlight->Land == TIMENULL)
				{
					fmDlg1 = true;
					if(MessageBox(GetString(IDS_STRING961), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
			
				if(prlAFlight->Onbl == TIMENULL)
				{
					fmDlg1 = true;
					if(MessageBox(GetString(IDS_STRING962), GetString(IDS_STRING908), MB_YESNO) == IDNO)
						return 0L;
				}
				else
				{
					if((prlAFlight->Onbl >= olTime) && (TIMENULL != olTime) && (TIMENULL != prlAFlight->Onbl))
					{
						fmDlg1 = true;
						MessageBox(GetString(IDS_STRING960), GetString(IDS_STRING908));
						return 0L;
					}
				}
			}

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;


			if((olTime != TIMENULL) && (prlFlight->Airb != TIMENULL) && (prlFlight->Airb <= olTime))
			{
				fmDlg1 = true;
				MessageBox(GetString(IDS_STRING963), GetString(ST_FEHLER), MB_ICONWARNING);
				return 0L;
			}

//			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "ONBL")
		{
		}
		if (olColomnName == "PSTA")
		{
		}
		if (olColomnName == "PSTD")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "PSTD");			
				strcpy(pclValue," ");
			}
			else
			{
				blRet  = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );					
				if(blRet)
				{
					strcpy(pclField, "PSTD");			
					sprintf(pclValue,"%s", prlNotify->Text);
				}
			}
		}
/*		if (olColomnName == "REGN")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REGN");			
				strcpy(pclValue," ");
			}
			else
			{

				CString olWhere;
				prlNotify->Text.TrimLeft();
				if(!prlNotify->Text.IsEmpty())
				{
					olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
					ogBCD.Read( "ACR", olWhere);
				}
				
				
				CString olAct3;
				CString olAct5;
				if(blRet = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
				{
					strcpy(pclField, "REGN,ACT3,ACT5");			
					sprintf(pclValue,"%s,%s,%s", prlNotify->Text, olAct3, olAct5);
				}
				else
				{
					CString	olRegn(prlNotify->Text);

					RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
					if(olDlg.DoModal() == IDOK)
					{
						strcpy(pclField, "REGN,ACT3,ACT5");			
						sprintf(pclValue,"%s,%s,%s", olDlg.m_Regn, olDlg.m_Act3, olDlg.m_Act5);
						blRet = true;
					}
					fmDlg1 = true;
				}
			}
		}
*/
		if (olColomnName == "REMP")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "REMP");
				strcpy(pclValue," ");
			}
			else
			{
				CString olCode;
				blRet = ogBCD.GetField("FID", "CODE", prlNotify->Text, "CODE", olCode);

				if(blRet)
				{
					strcpy(pclField, "REMP");
					sprintf(pclValue,"%s", olCode);
				}
			}
		}
		if (olColomnName == "RKEY")
		{
		}
		if (olColomnName == "RWYD")
		{
			if(prlNotify->Text.IsEmpty())
			{
				strcpy(pclField, "RWYD");			
				strcpy(pclValue," ");
			}
			else
			{
				CString olRnam;
				CString olRnum;
				if(!(blRet = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam )))
					blRet = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );

				if(blRet)
				{
					strcpy(pclField, "RWYD");			
					sprintf(pclValue,"%s", olRnam);
				}
			}
		}
		if (olColomnName == "SLOT" || olColomnName == "SLOU")
		{
			if((strcmp(prlFlight->Ftyp, "X") == 0) || (strcmp(prlFlight->Ftyp, "N") == 0))
				return 0L;
			
			strcpy(pclField, "SLOU");			

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);
//			CTime olTime = HourStringToDate(prlNotify->Text, prlFlight->Stod);
			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "STAB")
		{
		}
		if (olColomnName == "STODDate")
		{
			strcpy(pclField, "STOD");
			CString olTime = prlFlight->Stod.Format("%H:%M");
			CTime olDate = DateHourStringToDate(prlNotify->Text, olTime);
			if (olDate == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;
			sprintf(pclValue,"%s", CTimeToDBString(olDate, TIMENULL));
		}
		if (olColomnName == "STODTime")
		{
			strcpy(pclField, "STOD");

			CTime olRefDate = prlFlight->Stod;
			if (bgDailyLocal) ogBasicData.UtcToLocal(olRefDate);

			CTime olTime = HourStringToDate(prlNotify->Text, olRefDate);

			if(olTime == TIMENULL && !prlNotify->Text.IsEmpty())
				blRet = false;

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			if (!CheckDepartureInputTimes(prlFlight, prlAFlight, olTime))
				return 0L;

			//check of duplicated single flights
			if (bgDailyLocal) ogBasicData.UtcToLocal(olTime);
			CString	olText = IsDupFlight(prlFlight->Urno, "D", CString(prlFlight->Alc3), CString(prlFlight->Fltn), CString(prlFlight->Flns), olTime, bgDailyLocal);
			if (!olText.IsEmpty())
			{
				if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
					return 0L;
			}

			if (bgDailyLocal) ogBasicData.LocalToUtc(olTime);
			sprintf(pclValue,"%s", CTimeToDBString(olTime, TIMENULL));
		}
		if (olColomnName == "TIFA")
		{
		}
		if (olColomnName == "TIFD")
		{
		}
		if (olColomnName == "USEU")
		{
		}
		if (olColomnName == "VIA4")
		{
		}
		if (olColomnName == "VIAN")
		{
			strcpy(pclField, "VIAN");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "TTYP")
		{
			blRet = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp);
			strcpy(pclField, "TTYP");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
		if (olColomnName == "CHGI")
		{
		}
		if (olColomnName == "RTYP")
		{
		}
		if (olColomnName == "ADID")
		{
		}
		if (olColomnName == "NXTI")
		{
		}
		if (olColomnName == "BLT1")
		{
		}
		if (olColomnName == "PABA")
		{
		}
		if (olColomnName == "PAEA")
		{
		}
		if (olColomnName == "PDBA")
		{
		}
		if (olColomnName == "PDEA")
		{
		}
		if (olColomnName == "MING")
		{
		}
		if (olColomnName == "ADER")
		{
		}
		if (olColomnName == "VIAL")
		{
			strcpy(pclField, "VIAL");			
			sprintf(pclValue,"%s", prlNotify->Text);
		}
	}

	// Speichern?
	if(blRet) 
	{
		char pclSelection[128];
		if (blRotationData && prlLine->AUrno > 0)
		{
//			sprintf(pclSelection,"WHERE URNO=%ld OR URNO=%ld", prlLine->AUrno, prlLine->DUrno); 
			sprintf(pclSelection,"WHERE URNO=%ld", prlLine->AUrno);
			ogDailyCedaFlightData.SaveSpecial(pclSelection, pclField, pclValue);
		}
		else if (blRotationData && prlLine->DUrno > 0)
		{

			sprintf(pclSelection,"WHERE URNO=%ld", prlLine->DUrno);
			ogDailyCedaFlightData.SaveSpecial(pclSelection, pclField, pclValue);
		}
		else
		{
			sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
			ogDailyCedaFlightData.SaveSpecial(pclSelection, pclField, pclValue);
		}

		prlNotify->UserStatus = true;
		// Eingaben alle ok
		prlNotify->Status = true;

/*
		char pclSelection[128];
		sprintf(pclSelection,"WHERE URNO=%ld", prlFlight->Urno);
		ogDailyCedaFlightData.SaveSpecial(pclSelection, pclField, pclValue);
		prlNotify->UserStatus = true;
		// Eingaben alle ok
		prlNotify->Status = true;
*/
	}
	else
	{
		MessageBox(GetString(IDS_STRING965), GetString(ST_FEHLER));
		fmDlg2 = true;
		prlNotify->UserStatus = true;
		// Eingaben nicht ok
		prlNotify->Status = false;
	}

	pomDailyScheduleTable->EndIPEditByLostFocus(true);

	return 0L;

}

void DailyScheduleTableDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	//if( nChar == VK_ESCAPE ) omDragDropObject.CancelDrag();
	
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);

}

void DailyScheduleTableDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{

	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);

}



static void DailyTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    DailyScheduleTableDlg *polDlg = (DailyScheduleTableDlg*)popInstance;

}

BOOL DailyScheduleTableDlg::HandleWindowText (BOOL bplPost)
{
	CWnd* opWnd = (CWnd*) this;
	if (opWnd)
	{
		if (bplPost)
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

		return TRUE;
	}

	return FALSE;
}

BOOL DailyScheduleTableDlg::HandlePostFlight(BOOL bplPost)
{
	// postflight: no changes allowed
	HandleWindowText(bplPost);
	if (bplPost)
	{
		CButton* polCB = (CButton *) GetDlgItem(IDC_TEILEN);
		if (polCB) 
			polCB->EnableWindow(FALSE);
		
		return TRUE;
	}
	else
	{
		CButton* polCB = (CButton *) GetDlgItem(IDC_TEILEN);
		if (polCB) 
			polCB->EnableWindow(TRUE);
		
		return TRUE;
	}

	return FALSE;
}



LONG DailyScheduleTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ipItem);

	if (prlTableLine)
	{
		char clAdid='X';
		DAILYFLIGHTDATA *prlFlight = NULL;

		if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		else
			clAdid = 'A';
		
		// entsprechenden Diaolg aufrufen
		if(prlFlight)
			CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgDailyLocal, 'D');
	}

	return 0L;
}

void DailyScheduleTableDlg::FlightDataTifd(DAILYFLIGHTDATA *prpFlight, CTime& opTime) 
{
	opTime = TIMENULL;

	if (!prpFlight)
		return;

	if(prpFlight->Airb != TIMENULL)
	{
		opTime = prpFlight->Airb;
	}
	else
	{
		if(prpFlight->Ofbl != TIMENULL)
		{
			opTime = prpFlight->Ofbl;
		}
		else
		{
			if(prpFlight->Etdi != TIMENULL)
			{
				opTime = prpFlight->Etdi;
			}
			else
			{
				if(prpFlight->Stod != TIMENULL)
				{
					opTime = prpFlight->Stod;
				}
			}
		}
	}
}

void DailyScheduleTableDlg::FlightDataTifa(DAILYFLIGHTDATA *prpFlight, CTime& opTime) 
{
	opTime = TIMENULL;

	if (!prpFlight)
		return;

	if(prpFlight->Land != TIMENULL)
	{
		opTime = prpFlight->Land;
	}
	else
	{
		if(prpFlight->Onbl != TIMENULL)
		{
			opTime = prpFlight->Onbl;
		}
		else
		{
			if(prpFlight->Etai != TIMENULL)
			{
				opTime = prpFlight->Etai;
			}
			else
			{
				if(prpFlight->Stoa != TIMENULL)
				{
					opTime = prpFlight->Stoa;
				}
			}
		}
	}
}

bool DailyScheduleTableDlg::CheckArrivalInputTimes(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpDFlight, const CTime& opTime) 
{
	if (!prpFlight)
		return false;

	CTime olTifd = TIMENULL;
	FlightDataTifd(prpFlight, olTifd);

	CTime olTifdDep = TIMENULL;
	if (prpDFlight)
		FlightDataTifd(prpDFlight, olTifdDep);

	// tifd before tifa; "STD is before STA!\n";
	if((olTifdDep <= opTime) && (opTime != TIMENULL) && (olTifdDep != TIMENULL))
	{
		MessageBox(GetString(IDS_STRING1982), GetString(ST_FEHLER), MB_ICONWARNING);
		return false;
	}

	//Planm��ige Ankunftszeit(Home) liegt vor Abflugszeit(Origin)!\n
	if((olTifd >= opTime) && (opTime != TIMENULL) && (olTifd != TIMENULL))
	{
		MessageBox(GetString(IDS_STRING351), GetString(ST_FEHLER), MB_ICONWARNING);
		return false;
	}

	return true;
}

bool DailyScheduleTableDlg::CheckDepartureInputTimes(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpAFlight, const CTime& opTime) 
{
	//note: don�t check arrival times from the departure leg, ATH don�t wan�t it because we can�t edit this times
	if (!prpFlight)
		return false;

	if (!prpAFlight)
		return true;

	CTime olTifa = TIMENULL;
	if (prpAFlight)
		FlightDataTifa(prpAFlight, olTifa);

	// tifd before tifa; "STD is before STA!\n";
	if((olTifa >= opTime) && (opTime != TIMENULL) && (olTifa != TIMENULL))
	{
		MessageBox(GetString(IDS_STRING1982), GetString(ST_FEHLER), MB_ICONWARNING);
		return false;
	}

	return true;
}

LONG DailyScheduleTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomDailyScheduleTable->pomListBox->GetCurSel();    
	
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ilLineNo);
//	HandlePostFlight(omViewer.CheckPostFlight(prlTableLine));

	omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
	HandlePostFlight(omViewer.CheckPostFlight(prlTableLine));
	return 0L;
}

void DailyScheduleTableDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	//omDragDropObject.CancelDrag();

	CDialog::OnLButtonUp(nFlags, point);
}
