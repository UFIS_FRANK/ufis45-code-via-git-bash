// stafchrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <FPMS.h>
#include <CCSButtonCtrl.h>
#include <CCSClientWnd.h>
#include <CCSTimeScale.h>
#include <ccsdragdropctrl.h>
#include <ChaDiaViewer.h>
#include <ChaDiagram.h>
//----------------------------------------------------------------------

#include <chaChart.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ChaChart

IMPLEMENT_DYNCREATE(ChaChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ChaChart::ChaChart()
{
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    
    pomTopScaleText = NULL;
	pomCountText = NULL;
	imStartVerticalScalePos = 30;    
	imStartTopScaleTextPos = 100;
	lmBkColor = lgBkColor;
}

ChaChart::~ChaChart()
{
    if (pomTopScaleText != NULL)
        delete pomTopScaleText;
	if (pomCountText != NULL)
		delete pomCountText;
}

BEGIN_MESSAGE_MAP(ChaChart, CFrameWnd)
    //{{AFX_MSG_MAP(ChaChart)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_CCSBUTTON_RBUTTONDOWN, OnChartButtonRButtonDown)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_COMMAND(31, OnMenuAssign)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int ChaChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
		return imStartVerticalScalePos + omGantt.GetGanttChartHeight() + 2;
        //return imStartVerticalScalePos + imHeight + 2;
}

/////////////////////////////////////////////////////////////////////////////
// ChaChart message handlers


int ChaChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    CString olStr; GetWindowText(olStr);
    omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 110, 4 + 20), this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
   
	//
    pomCountText = new CCS3DStatic(TRUE);
    pomCountText->Create("999", WS_CHILD | WS_VISIBLE,
        CRect(130 + 10, 4 + 1, imStartTopScaleTextPos - 10, (4 + 20) - 1), this);
	//
    
	CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new CCS3DStatic(TRUE);
    pomTopScaleText->Create("", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);
    m_CountTextDragDrop.RegisterTarget(pomCountText, this);
    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);

    ////
    // read all data
    //SetState(Maximized);
    
    omGantt.SetTimeScale(GetTimeScale());
	omGantt.SetTopScaleText(pomTopScaleText);

    omGantt.SetViewer(GetViewer());
    omGantt.SetStatusBar(GetStatusBar());
    omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
	GetViewer()->MakeMasstab();
    omGantt.SetFonts(GetViewer()->GetGeometryFontIndex(), GetViewer()->GetGeometryFontIndex());
    omGantt.SetVerticalScaleColors(/*BLUE*/NAVY, SILVER, NAVY, YELLOW);
	omGantt.SetGanttChartColors(NAVY, SILVER, NAVY, YELLOW);
   omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);

//    olStr = GetViewer()->GetGroupText(GetGroupNo());
	olStr = " ";
    omButton.SetWindowText(olStr);
	omButton.ShowWindow(SW_HIDE);

#ifndef	PICHIT_FIXED_THE_COUNTER
	char clBuf[255];
	int ilCount = GetViewer()->GetAllLineCount();
	sprintf(clBuf, "%d", ilCount);
	pomCountText->SetWindowText(clBuf);
#else
	char clBuf[255];
	int ilCount = GetViewer()->GetLineCount(GetGroupNo());
	pomCountText->SetWindowText(clBuf);
#endif
	pomCountText->ShowWindow(SW_HIDE);
    //olStr = GetViewer()->GetGroupTopScaleText(GetGroupNo());
    //pomTopScaleText->SetWindowText(olStr);
    
    pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
	//pomTopScaleText->ShowWindow(SW_HIDE);
    return 0;
}

void ChaChart::SetMarkTime(CTime opStatTime, CTime opEndTime)
{
	omGantt.SetMarkTime(opStatTime, opEndTime);
}

void ChaChart::OnDestroy() 
{
//	if (bgModal == TRUE)
//		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartWindowDragDrop.Revoke();
    m_ChartButtonDragDrop.Revoke();
    m_CountTextDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	CFrameWnd::OnDestroy();
}

void ChaChart::OnSize(UINT nType, int cx, int cy) 
{
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("ChaChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);

    olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom + 4);
    //TRACE("ChaChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omGantt.MoveWindow(&olRect, FALSE);
}

BOOL ChaChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void ChaChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //


//Leftscale Header
/*	int ilIdx2 = GetViewer()->GetGeometryFontIndex();
	if(ilIdx2 > 9)
		ilIdx2 = 9;
	CFont *polVerticalScaleFont;
    polVerticalScaleFont = &ogScalingFonts[ilIdx2];//popVerticalScaleFont;
	CFont *pOldFont = dc.SelectObject(polVerticalScaleFont);
	CUIntArray olGutterPositions;

	dc.SetTextColor(COLORREF(NAVY));
	dc.SetBkMode(TRANSPARENT);
	CRect rect = CRect(olClientRect.left, 0, imStartTopScaleTextPos, imStartVerticalScalePos );
	CRect textRect;
	int left = rect.left + imHorizontalPos;
	rect.right = imHorizontalPos - 2;

	textRect.top = rect.top + (int)((rect.bottom - rect.top)/3);
	textRect.bottom = rect.bottom;
	int ilx = 2;
	textRect.right = (int)((imStartTopScaleTextPos/2.2)/2);
	olGutterPositions.Add(textRect.right);
	textRect.left = ilx;

	dc.DrawText(GetString(IDS_STRING1201), GetString(IDS_STRING1201).GetLength(), &textRect, DT_LEFT);

	textRect.left = textRect.right;
	textRect.right = textRect.right*2;
	olGutterPositions.Add(textRect.right);
	textRect.left += 4;
	dc.DrawText(GetString(IDS_STRING1202), GetString(IDS_STRING1202).GetLength(), &textRect, DT_LEFT);
	textRect.left -= 4;
//
	int ilNOffset = (int)((imStartTopScaleTextPos-textRect.right)/5);
	textRect.left = textRect.right;
	textRect.right = textRect.right + ilNOffset;
	olGutterPositions.Add(textRect.right);
	dc.DrawText(CString(""), CString("").GetLength(), &textRect, DT_CENTER);

	textRect.left = textRect.right;
	textRect.right = textRect.right + ilNOffset;
	olGutterPositions.Add(textRect.right);
	dc.DrawText(GetString(IDS_STRING308), GetString(IDS_STRING308).GetLength(), &textRect, DT_CENTER);

	textRect.left = textRect.right;
	textRect.right = textRect.right + ilNOffset;
	olGutterPositions.Add(textRect.right);
	dc.DrawText(GetString(IDS_STRING1151), GetString(IDS_STRING1151).GetLength(), &textRect, DT_CENTER);

	textRect.left = textRect.right;
	textRect.right = textRect.right + ilNOffset;
	olGutterPositions.Add(textRect.right);
	dc.DrawText(GetString(IDS_STRING1203), GetString(IDS_STRING1203).GetLength(), &textRect, DT_CENTER);

	textRect.left = textRect.right;
	textRect.right = textRect.right + ilNOffset;
	olGutterPositions.Add(textRect.right);
	dc.DrawText(GetString(IDS_STRING1204), GetString(IDS_STRING1204).GetLength(), &textRect, DT_CENTER);


	dc.SetBkColor(COLORREF(SILVER));
	dc.SetTextColor(COLORREF(NAVY));

	//Divide Vertical Scale into all Parts
	ilx = 2;
	textRect.right = (int)((imStartTopScaleTextPos/2.2)/2);
	textRect.left = ilx;

	CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
	CPen penWhite(PS_SOLID, 0, ::GetSysColor(COLOR_BTNHIGHLIGHT));
	dc.SelectObject(&penBlack);
	for(int i = 0; i < olGutterPositions.GetSize()-1; i++)
	{
		dc.MoveTo(olGutterPositions[i], rect.top);
		dc.LineTo(olGutterPositions[i], rect.bottom);
	}
	dc.SelectObject(&penWhite);
	for(i = 0; i < olGutterPositions.GetSize()-1; i++)
	{
		dc.MoveTo(olGutterPositions[i]+1, rect.top);
		dc.LineTo(olGutterPositions[i]+1, rect.bottom);
	}

	dc.SelectObject(pOldFont);
    dc.SelectObject(polOldPen);
*/
}

void ChaChart::OnChartButton()
{
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

	GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

LONG ChaChart::OnChartButtonRButtonDown(UINT wParam, LONG lParam)
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); i > 0; i--)
	     menu.RemoveMenu(i, MF_BYPOSITION);
    menu.AppendMenu(MF_STRING,31, GetString(IDS_STRING1489));	// Confirm

	CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
	ClientToScreen(&olPoint);
    menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);

	return 0L;
}

void ChaChart::OnMenuAssign()
{

}

void ChaChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void ChaChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}


LONG ChaChart::OnDragOver(UINT wParam, LONG lParam)
{
	return -1L;	// cannot accept this object
//----------------------------------------------------------------------
}

LONG ChaChart::OnDrop(UINT wParam, LONG lParam)
{
    return -1L;
//----------------------------------------------------------------------
}

LONG ChaChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

//-DTT Jul.23-----------------------------------------------------------
LONG ChaChart::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

LONG ChaChart::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}
//----------------------------------------------------------------------
