VERSION 5.00
Begin VB.Form PanicTools 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "User Data Export Tools"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3975
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "PanicTools.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   3975
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox Check4 
      Caption         =   "User Action Log"
      Enabled         =   0   'False
      Height          =   240
      Index           =   2
      Left            =   2130
      TabIndex        =   21
      Tag             =   "TXM"
      Top             =   3570
      Width           =   1695
   End
   Begin VB.CheckBox Check3 
      Caption         =   "Imbis Tables"
      Height          =   240
      Index           =   2
      Left            =   180
      TabIndex        =   20
      Tag             =   "TXM"
      Top             =   3570
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox Check3 
      Caption         =   "Imbis Spooler"
      Height          =   240
      Index           =   1
      Left            =   180
      TabIndex        =   19
      Tag             =   "TXM"
      Top             =   3330
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox Check4 
      Caption         =   "BC Dropped"
      Height          =   240
      Index           =   1
      Left            =   2130
      TabIndex        =   18
      Tag             =   "TXM"
      Top             =   3330
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox Check4 
      Caption         =   "BC Reveived"
      Height          =   240
      Index           =   0
      Left            =   2130
      TabIndex        =   17
      Tag             =   "TXM"
      Top             =   3090
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox Check3 
      Caption         =   "Main Data Pool"
      Height          =   240
      Index           =   0
      Left            =   180
      TabIndex        =   16
      Tag             =   "TXM"
      Top             =   3090
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Save Data Logging"
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   3
      Left            =   2040
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2700
      Width           =   1845
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Save Data Storage"
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   2
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2700
      Width           =   1845
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Flight Interface"
      Height          =   240
      Index           =   5
      Left            =   2130
      TabIndex        =   13
      Tag             =   "TXM"
      Top             =   2250
      Width           =   1695
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Pending Telexes"
      Height          =   240
      Index           =   4
      Left            =   2130
      TabIndex        =   12
      Tag             =   "TXM"
      Top             =   2010
      Width           =   1695
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Created Telexes"
      Height          =   240
      Index           =   3
      Left            =   2130
      TabIndex        =   11
      Tag             =   "TXM"
      Top             =   1770
      Width           =   1695
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Online Sent"
      Height          =   240
      Index           =   2
      Left            =   2130
      TabIndex        =   10
      Tag             =   "TXM"
      Top             =   1530
      Width           =   1695
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Online Received"
      Height          =   240
      Index           =   1
      Left            =   2130
      TabIndex        =   9
      Tag             =   "TXM"
      Top             =   1290
      Width           =   1695
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Main Dialog"
      Height          =   240
      Index           =   0
      Left            =   2130
      TabIndex        =   8
      Tag             =   "TXM"
      Top             =   1050
      Width           =   1695
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Save Data Grids"
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   1
      Left            =   2040
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   660
      Width           =   1845
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Flight Interface"
      Height          =   240
      Index           =   5
      Left            =   180
      TabIndex        =   6
      Tag             =   "INF"
      Top             =   2250
      Width           =   1695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Pending Telexes"
      Height          =   240
      Index           =   4
      Left            =   180
      TabIndex        =   5
      Tag             =   "PND"
      Top             =   2010
      Width           =   1695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Created Telexes"
      Height          =   240
      Index           =   3
      Left            =   180
      TabIndex        =   4
      Tag             =   "SND"
      Top             =   1770
      Width           =   1695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Online Sent"
      Height          =   240
      Index           =   2
      Left            =   180
      TabIndex        =   3
      Tag             =   "OUT"
      Top             =   1530
      Width           =   1695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Online Received"
      Height          =   240
      Index           =   1
      Left            =   180
      TabIndex        =   2
      Tag             =   "RCV"
      Top             =   1290
      Width           =   1695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Main Dialog"
      Height          =   240
      Index           =   0
      Left            =   180
      TabIndex        =   1
      Tag             =   "TXM"
      Top             =   1050
      Width           =   1695
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Screen Snap Shots"
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   0
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   660
      Width           =   1845
   End
   Begin VB.CheckBox chkPanel 
      Enabled         =   0   'False
      Height          =   1635
      Index           =   0
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   960
      Width           =   1845
   End
   Begin VB.CheckBox chkPanel 
      Enabled         =   0   'False
      Height          =   1635
      Index           =   1
      Left            =   2040
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   960
      Width           =   1845
   End
   Begin VB.CheckBox chkPanel 
      Enabled         =   0   'False
      Height          =   915
      Index           =   2
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   3000
      Width           =   1845
   End
   Begin VB.CheckBox chkPanel 
      Enabled         =   0   'False
      Height          =   915
      Index           =   3
      Left            =   2040
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   3000
      Width           =   1845
   End
   Begin VB.Frame Frame1 
      Height          =   555
      Left            =   90
      TabIndex        =   26
      Top             =   0
      Width           =   3810
      Begin VB.CheckBox chkTool 
         Caption         =   "Save All Topics"
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   4
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   180
         Width           =   1875
      End
   End
   Begin VB.Frame Frame2 
      Height          =   555
      Left            =   90
      TabIndex        =   27
      Top             =   3930
      Width           =   3810
      Begin VB.CheckBox chkExit 
         Caption         =   "Done"
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   0
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   180
         Width           =   1875
      End
   End
End
Attribute VB_Name = "PanicTools"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SnapShotPath As String

Private Sub chkExit_Click(Index As Integer)
    Unload Me
End Sub

Private Sub chkTool_Click(Index As Integer)
    Dim CurTab As TABLib.Tab
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpTxt3 As String
    Dim tmpShot As String
    Dim tmpCnt As Integer
    Dim BcLogShown As Boolean
    Dim wndHnd As Long
    Dim i As Integer
    Dim K As Integer
    If chkTool(Index).Value = 1 Then
        If chkTool(4).Value = 0 Then
            SnapShotPath = UFIS_TMP
            SnapShotPath = SnapShotPath & "\Export\TelexPool\" & GetTimeStamp(0)
            CheckAndMakeDir SnapShotPath
        End If
        Select Case Index
            Case 0
                BcLogShown = False
                If TelexPoolHead.ActionPanel.Visible = False Then
                    TelexPoolHead.chkBcFlow.Value = 1
                    BcLogShown = True
                End If
                For i = 0 To Check1.UBound
                    If Check1(i).Value = 1 Then
                        tmpTxt1 = Check1(i).Tag
                        tmpTxt2 = "0"
                        tmpCnt = Val(tmpTxt2) + 1
                        tmpTxt2 = CStr(tmpCnt)
                        tmpTxt2 = ""
                        tmpShot = "TLX_" & tmpTxt1 & tmpTxt2
                        tmpShot = SnapShotPath & "\" & tmpShot & ".bmp"
                        Select Case tmpTxt1
                            Case "TXM"
                                TelexPoolHead.Show
                                TelexPoolHead.Refresh
                                DoEvents
                                wndHnd = TelexPoolHead.hwnd
                            Case "RCV"
                                RcvTelex.Show
                                RcvTelex.Refresh
                                DoEvents
                                wndHnd = RcvTelex.hwnd
                            Case "OUT"
                                OutTelex.Show
                                OutTelex.Refresh
                                DoEvents
                                wndHnd = OutTelex.hwnd
                            Case "SND"
                                SndTelex.Show
                                SndTelex.Refresh
                                DoEvents
                                wndHnd = SndTelex.hwnd
                            Case "PND"
                                PndTelex.Show
                                PndTelex.Refresh
                                DoEvents
                                wndHnd = PndTelex.hwnd
                            Case "INF"
                                RcvInfo.Show
                                RcvInfo.Refresh
                                DoEvents
                                wndHnd = RcvInfo.hwnd
                            Case Else
                                wndHnd = Me.hwnd
                                Me.Show
                                Me.Refresh
                                DoEvents
                        End Select
                        GetWindowScreenshot wndHnd, tmpShot
                    End If
                Next
                If BcLogShown = True Then TelexPoolHead.chkBcFlow.Value = 1
                chkTool(Index).Value = 0
                chkTool(Index).Refresh
                TelexPoolHead.Show
                Me.Show
            Case 1
                For i = 0 To Check2.UBound
                    If Check2(i).Value = 1 Then
                        tmpTxt1 = Check1(i).Tag
                        tmpTxt2 = "1"
                        tmpTxt2 = ""
                        Select Case tmpTxt1
                            Case "TXM"
                                For K = 0 To TelexPoolHead.tabTelexList.UBound
                                    Set CurTab = TelexPoolHead.tabTelexList(K)
                                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                    SaveTabOcxData CurTab, tmpShot
                                Next
                            Case "RCV"
                                For K = 0 To RcvTelex.tabTelexList.UBound
                                    Set CurTab = RcvTelex.tabTelexList(K)
                                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                    SaveTabOcxData CurTab, tmpShot
                                Next
                            Case "OUT"
                                K = 0
                                Set CurTab = OutTelex.tabTelexList
                                tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                SaveTabOcxData CurTab, tmpShot
                            Case "SND"
                                K = 0
                                Set CurTab = SndTelex.tabTelexList
                                tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                SaveTabOcxData CurTab, tmpShot
                            Case "PND"
                                K = 0
                                Set CurTab = PndTelex.tabTelexList
                                tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                SaveTabOcxData CurTab, tmpShot
                            Case "INF"
                                For K = 0 To RcvInfo.tabTelexList.UBound
                                    Set CurTab = RcvInfo.tabTelexList(K)
                                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                                    SaveTabOcxData CurTab, tmpShot
                                Next
                            Case Else
                        End Select
                    End If
                Next
                chkTool(Index).Value = 0
            Case 2
                If Check3(0).Value = 1 Then
                    tmpTxt1 = "MEM"
                    tmpTxt2 = ""
                    For K = 0 To DataPool.TelexData.UBound
                        Set CurTab = DataPool.TelexData(K)
                        tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_DATA_" & CStr(K)
                        tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                        SaveTabOcxData CurTab, tmpShot
                    Next
                End If
                If Check3(1).Value = 1 Then
                    tmpTxt1 = "IMB"
                    tmpTxt2 = ""
                    K = 0
                    Set CurTab = BcSpooler.BcSpoolerTab
                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_BCSP_" & CStr(K)
                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                    SaveTabOcxData CurTab, tmpShot
                End If
                If Check3(2).Value = 1 Then
                    tmpTxt1 = "IMB"
                    tmpTxt2 = ""
                    K = 1
                    Set CurTab = UfisImbis.BcSpoolerTab
                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_BCSP_" & CStr(K)
                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                    SaveTabOcxData CurTab, tmpShot
                    For K = 0 To UfisImbis.BcTblTab.UBound
                        Set CurTab = UfisImbis.BcTblTab(K)
                        tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_BTBL_" & CStr(K)
                        tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                        SaveTabOcxData CurTab, tmpShot
                    Next
                End If
                chkTool(Index).Value = 0
            Case 3
                If Check4(0).Value = 1 Then
                    K = 0
                    tmpTxt1 = "LOG"
                    tmpTxt2 = ""
                    Set CurTab = TelexPoolHead.BcLog(K)
                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                    SaveTabOcxData CurTab, tmpShot
                End If
                If Check4(1).Value = 1 Then
                    K = 1
                    tmpTxt1 = "LOG"
                    tmpTxt2 = ""
                    Set CurTab = TelexPoolHead.BcLog(K)
                    tmpShot = "TLX_" & tmpTxt1 & tmpTxt2 & "_GRID_" & CStr(K)
                    tmpShot = SnapShotPath & "\" & tmpShot & ".dat"
                    SaveTabOcxData CurTab, tmpShot
                End If
                chkTool(Index).Value = 0
            Case 4
                SnapShotPath = UFIS_TMP
                SnapShotPath = SnapShotPath & "\Export\TelexPool\" & GetTimeStamp(0)
                CheckAndMakeDir SnapShotPath
                chkTool(0).Value = 1
                chkTool(1).Value = 1
                chkTool(2).Value = 1
                chkTool(3).Value = 1
                chkTool(Index).Value = 0
            Case Else
                chkTool(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub SaveTabOcxData(UseTab As TABLib.Tab, UsePath As String)
    UseTab.WriteToFile UsePath, False
End Sub
Private Sub Form_Load()
    If FormIsVisible("TelexPoolHead") Then Check1(0).Value = 1
    If FormIsVisible("RcvTelex") Then Check1(1).Value = 1
    If FormIsVisible("OutTelex") Then Check1(2).Value = 1
    If FormIsVisible("SndTelex") Then Check1(3).Value = 1
    If FormIsVisible("PndTelex") Then Check1(4).Value = 1
    If FormIsVisible("RcvInfo") Then Check1(5).Value = 1
    If FormIsVisible("TelexPoolHead") Then Check2(0).Value = 1
    If FormIsVisible("RcvTelex") Then Check2(1).Value = 1
    If FormIsVisible("OutTelex") Then Check2(2).Value = 1
    If FormIsVisible("SndTelex") Then Check2(3).Value = 1
    If FormIsVisible("PndTelex") Then Check2(4).Value = 1
    If FormIsVisible("RcvInfo") Then Check2(5).Value = 1
End Sub
