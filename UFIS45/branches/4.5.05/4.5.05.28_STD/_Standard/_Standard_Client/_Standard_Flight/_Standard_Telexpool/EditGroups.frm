VERSION 5.00
Begin VB.Form EditGroups 
   Caption         =   "Edit Address Group"
   ClientHeight    =   2490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10200
   LinkTopic       =   "Form1"
   ScaleHeight     =   2490
   ScaleWidth      =   10200
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtGrpOwner 
      Height          =   285
      Left            =   5880
      TabIndex        =   9
      Top             =   240
      Width           =   2895
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9240
      TabIndex        =   7
      Top             =   1920
      Width           =   735
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9240
      TabIndex        =   6
      Top             =   1440
      Width           =   735
   End
   Begin VB.TextBox txtGrpAddr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   1920
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   1200
      Width           =   6855
   End
   Begin VB.TextBox txtGrpDesc 
      Height          =   285
      Left            =   1920
      TabIndex        =   4
      Top             =   720
      Width           =   6855
   End
   Begin VB.TextBox txtGrpName 
      Height          =   285
      Left            =   1920
      TabIndex        =   3
      Top             =   240
      Width           =   2895
   End
   Begin VB.Label Label4 
      Caption         =   "Owner :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5160
      TabIndex        =   8
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Address List :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Group Description :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Group Name :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "EditGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()

    EditGroups.Visible = False
End Sub

Private Sub cmdOK_Click()
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ErrorMsg As String

    ErrorMsg = ""
    If Len(txtGrpName.Text) = 0 Then
        ErrorMsg = "Missing Group Name!"
    End If
    If Len(txtGrpAddr.Text) = 0 Then
        ErrorMsg = "Missing Telex Addresses!"
    End If
    If Len(txtGrpName.Text) = 0 And Len(txtGrpAddr.Text) = 0 Then
        ErrorMsg = "Missing Group Name and Telex Addresses!"
    End If
    If Len(ErrorMsg) > 0 Then
        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", ErrorMsg, "hand", "", UserAnswer
    Else
        If Len(txtGrpDesc.Text) = 0 Then
            txtGrpDesc.Text = " "
        End If
        If txtGrpName.Tag = "INS" Then
            ActResult = ""
            ActCmd = "IRT"
            ActTable = "TTPTAB"
            ActFldLst = "URNO,HOPO,TYPE,GRPN,GRDE,GRAL,TPOW"
            ActCondition = ""
            ActOrder = ""
            EditTelex.chkSelAddr(1).Tag = UfisServer.UrnoPoolGetNext
            ActDatLst = EditTelex.chkSelAddr(1).Tag & "," & _
                        UfisServer.HOPO & ",G," & _
                        txtGrpName.Text & "," & _
                        txtGrpDesc.Text & "," & _
                        CleanString(txtGrpAddr.Text, FOR_SERVER, False) & "," & _
                        txtGrpOwner.Text
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        Else
            ActResult = ""
            ActCmd = "URT"
            ActTable = "TTPTAB"
            ActFldLst = "GRPN,GRDE,GRAL"
            ActCondition = "WHERE URNO = " & txtGrpName.Tag
            ActOrder = ""
            ActDatLst = txtGrpName.Text & "," & _
                        txtGrpDesc.Text & "," & _
                        CleanString(txtGrpAddr.Text, FOR_SERVER, False)
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        End If
        EditTelex.LoadGroupData
        EditGroups.Visible = False
    End If
End Sub

Private Sub txtGrpAddr_KeyPress(KeyAscii As Integer)
    If EditTelex.chkLcase.Value = 0 Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

