VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSnapDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' *************************************************************************
'  Copyright �2007-08 Karl E. Peterson
'  All Rights Reserved, http://vb.mvps.org/
' *************************************************************************
'  You are free to use this code within your own applications, but you
'  are expressly forbidden from selling or otherwise distributing this
'  source code, non-compiled, without prior written consent.
' *************************************************************************
Option Explicit

' Win32 API declarations
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal length As Long)
Private Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" (ByVal uiAction As Long, ByVal uiParam As Long, pvParam As Any, ByVal fWinIni As Long) As Long
Private Declare Function GetSystemMetrics Lib "User32.dll" (ByVal nIndex As Long) As Long
Private Declare Function MonitorFromWindow Lib "User32.dll" (ByVal hwnd As Long, ByVal dwFlags As Long) As Long  ' HMONITOR
Private Declare Function GetMonitorInfo Lib "User32.dll" Alias "GetMonitorInfoA" (ByVal hMonitor As Long, ByRef lpMI As Any) As Long

Private Const SPI_GETWORKAREA As Long = 48
Private Const SM_CMONITORS As Long = 80

Private Const MONITORINFOF_PRIMARY As Long = 1
Private Const MONITOR_DEFAULTTONULL     As Long = 0
Private Const MONITOR_DEFAULTTOPRIMARY  As Long = 1
Private Const MONITOR_DEFAULTTONEAREST  As Long = 2

' Watched messages
Private Const WM_WINDOWPOSCHANGING As Long = &H46
Private Const WM_WINDOWPOSCHANGED As Long = &H47
Private Const WM_MOUSEWHEEL = &H20A

Private Type WINDOWPOS
   hwnd As Long
   hWndInsertAfter As Long
   X As Long
   Y As Long
   cx As Long
   cy As Long
   flags As Long
End Type

Private Type RECT
   Left As Long
   Top As Long
   Right As Long
   Bottom As Long
End Type

Private Type MonitorInfo
   cbSize As Long
   rcMonitor As RECT
   rcWork As RECT
   dwFlags As Long
End Type

Implements IHookSink

' Member variables
Private m_hWnd As Long
Private m_SnapGap As Long

' Default values.
Private Const defSnapGap As Long = 10

' **************************************************************
'  Initialization/Termination
' **************************************************************
Private Sub Class_Initialize()
   ' Set defaults
   m_SnapGap = defSnapGap
End Sub

Private Sub Class_Terminate()
   If m_hWnd Then UnhookWindow (m_hWnd)
End Sub

' **************************************************************
'  Hooking interface
' **************************************************************
Private Function IHookSink_WindowProc( _
   hwnd As Long, msg As Long, wp As Long, lp As Long) As Long
   Dim pos As WINDOWPOS
   Dim isCancelled As Boolean
   ' Handle each message appropriately.
   Select Case msg
      Case WM_WINDOWPOSCHANGING
         ' Snag copy of position structure, process it,
         ' and pass back to Windows any changes.
         Call CopyMemory(pos, ByVal lp, Len(pos))
         Call SnapToDesktopEdge(pos)
         Call CopyMemory(ByVal lp, pos, Len(pos))
    Case WM_MOUSEWHEEL
        isCancelled = False
        UfisMouseWheelRolled ByVal hwnd, ByVal msg, ByVal wp, ByVal lp, isCancelled
        If isCancelled Then
            IHookSink_WindowProc = InvokeWindowProc(hwnd, msg, wp, lp)
        End If
      Case Else
         ' Just allow default processing for everything else.
         IHookSink_WindowProc = InvokeWindowProc(hwnd, msg, wp, lp)
   End Select
End Function

' **************************************************************
'  Public Properties
' **************************************************************
Public Property Let hwnd(ByVal NewVal As Long)
   ' Unhook previous window if need be.
   If m_hWnd Then
      Call UnhookWindow(m_hWnd)
   End If
   
   ' Store handle and hook new window.
   If IsWindow(NewVal) Then
      m_hWnd = NewVal
      Call HookWindow(m_hWnd, Me)
   End If
End Property

Public Property Get hwnd() As Long
   ' Return handle for window we're monitoring.
   hwnd = m_hWnd
End Property

Public Property Let SnapGap(ByVal NewVal As Long)
   ' Store tolerance for snapping to edge of screen.
   If NewVal > 0 Then m_SnapGap = NewVal
End Property

Public Property Get SnapGap() As Long
   ' Return tolerance for snapping to edge of screen.
   SnapGap = m_SnapGap
End Property

' **************************************************************
'  Private Methods
' **************************************************************
Private Sub SnapToDesktopEdge(pos As WINDOWPOS)
   Dim mon As RECT
   
   ' Get coordinates for main work area.
   mon = GetWorkArea()
   
   ' Snap X axis
   If Abs(pos.X - mon.Left) <= m_SnapGap Then
      pos.X = mon.Left
   ElseIf Abs(pos.X + pos.cx - mon.Right) <= m_SnapGap Then
      pos.X = mon.Right - pos.cx
   End If
   
   ' Snap Y axis
   If Abs(pos.Y - mon.Top) <= m_SnapGap Then
      pos.Y = mon.Top
   ElseIf Abs(pos.Y + pos.cy - mon.Bottom) <= m_SnapGap Then
      pos.Y = mon.Bottom - pos.cy
   End If
End Sub

Private Function GetWorkArea() As RECT
   Dim hMonitor As Long
   Dim mi As MonitorInfo

   ' Default to using traditional method, as fallback for cases where
   ' only one monitor is being used or new multimonitor method fails.
   Call SystemParametersInfo(SPI_GETWORKAREA, 0&, GetWorkArea, 0&)

   ' Use newer multimonitor method to support cases of 2 or more.
   If GetSystemMetrics(SM_CMONITORS) > 1 Then
      ' Get handle to monitor that has bulk of window within it.
      hMonitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST)
      If hMonitor Then
         mi.cbSize = Len(mi)
         Call GetMonitorInfo(hMonitor, mi)
         GetWorkArea = mi.rcWork
      End If
   End If
End Function
