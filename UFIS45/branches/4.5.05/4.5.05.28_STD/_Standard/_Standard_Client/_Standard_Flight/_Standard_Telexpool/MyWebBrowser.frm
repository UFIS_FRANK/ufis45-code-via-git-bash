VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MyWebBrowser 
   Caption         =   "UFIS Web Browser"
   ClientHeight    =   6525
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12840
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MyWebBrowser.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6525
   ScaleWidth      =   12840
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox SplitHorz 
      BorderStyle     =   0  'None
      Height          =   90
      Index           =   0
      Left            =   0
      ScaleHeight     =   90
      ScaleWidth      =   9885
      TabIndex        =   30
      Top             =   1770
      Width           =   9885
      Begin VB.Image Image1 
         Height          =   240
         Left            =   30
         Picture         =   "MyWebBrowser.frx":08CA
         Top             =   30
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.CheckBox chkUfis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   0
      Left            =   6060
      Picture         =   "MyWebBrowser.frx":0A14
      Style           =   1  'Graphical
      TabIndex        =   25
      Tag             =   "exit"
      ToolTipText     =   "Close"
      Top             =   0
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.PictureBox RightPanel 
      Height          =   795
      Index           =   0
      Left            =   7050
      ScaleHeight     =   735
      ScaleWidth      =   615
      TabIndex        =   23
      Top             =   0
      Width           =   675
      Begin VB.Image MyBorder 
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "MyWebBrowser.frx":0F9E
         Stretch         =   -1  'True
         Top             =   0
         Width           =   690
      End
   End
   Begin VB.CheckBox chkUfis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   3
      Left            =   6540
      Picture         =   "MyWebBrowser.frx":2160
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "exit"
      ToolTipText     =   "Close"
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   450
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   435
      Index           =   3
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   5265
      TabIndex        =   8
      Top             =   840
      Width           =   5265
      Begin VB.CheckBox chkTask 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   10
         Left            =   4050
         Picture         =   "MyWebBrowser.frx":26EA
         Style           =   1  'Graphical
         TabIndex        =   18
         Tag             =   "find"
         ToolTipText     =   "Find on Page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   9
         Left            =   3600
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "print"
         ToolTipText     =   "Print"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   8
         Left            =   3150
         Picture         =   "MyWebBrowser.frx":2C74
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "pagesetup"
         ToolTipText     =   "Page Setup"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   7
         Left            =   2700
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "preview"
         ToolTipText     =   "Print Preview"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   6
         Left            =   2250
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "search"
         ToolTipText     =   "Search"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   5
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "home"
         ToolTipText     =   "Home"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   4
         Left            =   1350
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "reload"
         ToolTipText     =   "Refresh"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   3
         Left            =   900
         Picture         =   "MyWebBrowser.frx":31FE
         Style           =   1  'Graphical
         TabIndex        =   11
         Tag             =   "stop"
         ToolTipText     =   "Stop"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   1
         Left            =   0
         Picture         =   "MyWebBrowser.frx":3588
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "back"
         ToolTipText     =   "Back"
         Top             =   0
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   2
         Left            =   450
         Picture         =   "MyWebBrowser.frx":36D2
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   "forward"
         ToolTipText     =   "Forward"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   2
      Left            =   0
      ScaleHeight     =   345
      ScaleWidth      =   8085
      TabIndex        =   5
      Top             =   1350
      Width           =   8085
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Panic"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   2700
         Style           =   1  'Graphical
         TabIndex        =   29
         Tag             =   "panic"
         ToolTipText     =   "Create an Issue Related to TelexPool"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Patches"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   28
         Tag             =   "patches"
         ToolTipText     =   "Retrieve TelexPool Patches"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Issues"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   27
         Tag             =   "openissues"
         ToolTipText     =   "Look at Open TelexPool Issues"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Notes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   24
         Tag             =   "releasenotes"
         ToolTipText     =   "Open the Release Note of this Version"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   435
      Index           =   1
      Left            =   5460
      ScaleHeight     =   435
      ScaleWidth      =   3705
      TabIndex        =   4
      Top             =   840
      Visible         =   0   'False
      Width           =   3705
      Begin VB.TextBox txtURL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   3045
      End
      Begin VB.CheckBox chkGo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   3060
         Picture         =   "MyWebBrowser.frx":381C
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.TextBox txtURL 
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   6
         Text            =   "about:blank"
         Top             =   0
         Width           =   3045
      End
      Begin MSComctlLib.ProgressBar barMain 
         Height          =   120
         Left            =   1560
         TabIndex        =   21
         Top             =   285
         Visible         =   0   'False
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
      End
      Begin VB.Shape barLabel 
         Height          =   150
         Left            =   90
         Top             =   270
         Width           =   1365
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Index           =   0
      Left            =   0
      ScaleHeight     =   720
      ScaleWidth      =   5295
      TabIndex        =   2
      Top             =   0
      Width           =   5355
      Begin VB.CheckBox chkUfis 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   2
         Left            =   1230
         Picture         =   "MyWebBrowser.frx":3DA6
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Show URL Box"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkUfis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   1
         Left            =   780
         Picture         =   "MyWebBrowser.frx":4330
         Style           =   1  'Graphical
         TabIndex        =   19
         Tag             =   "exit"
         ToolTipText     =   "Close"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkUfisTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Index           =   0
         Left            =   0
         MaskColor       =   &H00FFFFFF&
         Picture         =   "MyWebBrowser.frx":48BA
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "ufishome"
         ToolTipText     =   "UFIS-AS Home Page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   780
      End
   End
   Begin SHDocVwCtl.WebBrowser webMain 
      Height          =   3795
      Left            =   0
      TabIndex        =   0
      Top             =   2190
      Width           =   9975
      ExtentX         =   17595
      ExtentY         =   6694
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin MSComctlLib.StatusBar stbMain 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   6240
      Width           =   12840
      _ExtentX        =   22648
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   18529
            MinWidth        =   8819
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   5400
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":54FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":5C0E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":6320
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":6A32
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":7144
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":7856
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":7F68
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":8842
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":8EBC
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":9536
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":9C30
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MyWebBrowser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim DontRefresh As Boolean
Dim WithEvents objDocument As HTMLDocument
Attribute objDocument.VB_VarHelpID = -1

Private Enum BrowserNavConstants
    navOpenInNewWindow = &H1
    navNoHistory = &H2
    navNoReadFromCache = &H4
    navNoWriteToCache = &H8
    navAllowAutosearch = &H10
    navBrowserBar = &H20
    navHyperlink = &H40
End Enum

Private Sub WebNavigate(URL As String, Optional Flags, Optional Target, Optional PostData, Optional Headers)
    Screen.MousePointer = 11
    webMain.Navigate URL, Flags, Target, PostData, Headers
End Sub

Private Function CheckPopup() As Boolean
    On Error GoTo ErrHandle
    If webMain.Document.activeElement.tagName = "BODY" Or _
        webMain.Document.activeElement.tagName = "IFRAME" Then
        CheckPopup = False
    Else
        CheckPopup = True
    End If
    
    Exit Function
    
ErrHandle:
    If Err.Number = 91 Then
        CheckPopup = False
    End If
End Function

'Private Function CheckLock() As Boolean
'On Error Resume Next
'Dim strRestrict As Variant, i As Integer, intCount As Integer, objRange As Object
'strRestrict = Array("amateur", "cock", "penis", "tit", "mature", "anal", "oral", "vaginal", "swallow", "blowjob", "sex", "porn", "hot", "babe")
'
'Set objRange = webMain.Document.body.createTextRange
'
''This error is thrown when no page has been loaded yet and there is no
''body to use the createTextRange method on.
'If Err.Number = 91 Then
'    CheckLock = True
'    Exit Function
'End If
'
''Searching for text in the page
'For i = 0 To UBound(strRestrict)
'    If objRange.findText(strRestrict(i)) = True Then
'        intCount = intCount + 1
'    End If
'Next i
'
'If intCount > 4 Then
'    CheckLock = False
'Else
'    CheckLock = True
'End If
'
'Set objRange = Nothing
'End Function
'
'Private Function CheckVBWM(ByVal URL As String) As Boolean
'If InStr(1, Split(URL, "/")(2), "vbwm.com") = 0 Then
'    CheckVBWM = False
'Else
'    CheckVBWM = True
'End If
'End Function
'
Private Sub CheckCommands()
    If webMain.QueryStatusWB(OLECMDID_PRINT) = 0 Then
        'tlbMain.Buttons.Item("print").Enabled = False
    Else
        'tlbMain.Buttons.Item("print").Enabled = True
    End If
    
    If webMain.QueryStatusWB(OLECMDID_PRINTPREVIEW) = 0 Then
        'tlbMain.Buttons.Item("preview").Enabled = False
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("preview").Enabled = False
    Else
        'tlbMain.Buttons.Item("preview").Enabled = True
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("preview").Enabled = True
    End If
    
    If webMain.QueryStatusWB(OLECMDID_PAGESETUP) = 0 Then
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("page").Enabled = False
    Else
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("page").Enabled = True
    End If
    
    If webMain.Document Is Nothing Then
        'tlbMain.Buttons.Item("find").Enabled = False
    Else
        'tlbMain.Buttons.Item("find").Enabled = True
    End If
End Sub

Private Sub chkGo_Click(Index As Integer)
    Dim tmpData As String
    On Error Resume Next
    If chkGo(Index).Value = 1 Then
        chkGo(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                If Not DontRefresh Then WebNavigate txtURL(0).Text
            Case Else
        End Select
    Else
        chkGo(Index).BackColor = vbButtonFace
        'tmpData = webMain.LocationURL
        'txtURL(0).Text = tmpData
        webMain.SetFocus
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim tmpTag As String
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                'ButtonPanel(2).Visible = True
                'ButtonPanel(1).Visible = False
            Case Else
                tmpTag = chkTask(Index).Tag
                WebMainTask Index, tmpTag
        End Select
    Else
        chkTask(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                'ButtonPanel(1).Visible = True
                'ButtonPanel(2).Visible = False
            Case Else
        End Select
    End If
End Sub
Private Sub WebMainTask(Index As Integer, TagCode As String)
    Dim tmpData As String
    On Error Resume Next
    Select Case TagCode
        Case "back"
            txtURL(0).Text = "Going Back"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            Err.Clear
            webMain.GoBack
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "forward"
            txtURL(0).Text = "Going Forward"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoForward
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "stop"
            txtURL(0).Text = "Stopped"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.Stop
            chkTask(Index).Value = 0
            chkTask(Index).Enabled = False
            chkGo(0).Value = 0
        Case "reload"
            txtURL(0).Text = "Refresh"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.Refresh
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "home"
            txtURL(0).Text = "Going Home"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoHome
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "search"
            txtURL(0).Text = "Invoking Web Search"
            Screen.MousePointer = 11
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoSearch
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
            Screen.MousePointer = 0
        Case "preview"
            Screen.MousePointer = 11
            webMain.ExecWB OLECMDID_PRINTPREVIEW, OLECMDEXECOPT_DODEFAULT
            chkTask(Index).Value = 0
            Screen.MousePointer = 0
        Case "pagesetup"
            Screen.MousePointer = 11
            webMain.ExecWB OLECMDID_PAGESETUP, OLECMDEXECOPT_DODEFAULT
            chkTask(Index).Value = 0
            Me.Refresh
            Screen.MousePointer = 0
        Case "print"
            webMain.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT
        Case "lock"
            'If Button.Value = tbrPressed Then
            '    tlbMain.Buttons.Item("vbwm").Value = tbrUnpressed
            'Else
            '    tlbMain.Buttons.Item("vbwm").Value = tbrUnpressed
            'End If
        Case "vbwm"
            'If Button.Value = tbrPressed Then
            '    tlbMain.Buttons.Item("lock").Value = tbrUnpressed
            'Else
            '    tlbMain.Buttons.Item("lock").Value = tbrUnpressed
            'End If
        Case "find"
            'frmFind.Show 1
        'Case "ufis"
        '    Me.Caption = "TelexPool Release Notes and Issues"
        '    txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
        '
        '    WebNavigate txtURL(0).Text
        Case Else
    End Select
End Sub

Private Sub chkUfis_Click(Index As Integer)
    Dim tmpTag As String
    Dim SetEnable As Boolean
    If chkUfis(Index).Value = 1 Then
        chkUfis(Index).BackColor = LightGreen
        Select Case Index
            'Case 0
                'ButtonPanel(1).Tag = CStr(CInt(ButtonPanel(1).Visible))
                'ButtonPanel(1).Visible = False
                'chkUfis(2).Enabled = False
                'SetEnable = Not chkUfisTask(0).Enabled
                'EnableUfisTasks SetEnable
                'chkUfis(Index).Value = 0
            Case 1
                Me.Hide
                chkUfis(Index).Value = 0
                'TelexPoolHead.chkWeb.Value = 0
            Case 2
                ButtonPanel(1).Visible = Not ButtonPanel(1).Visible
                chkUfis(Index).Value = 0
            Case 3
                Me.Hide
                chkUfis(Index).Value = 0
                'TelexPoolHead.chkWeb.Value = 0
            Case Else
                tmpTag = chkUfis(Index).Tag
                WebUfisTask chkUfis(Index), tmpTag
        End Select
    Else
        Select Case Index
            Case 0
                'ButtonPanel(2).Visible = False
                'tmpTag = ButtonPanel(1).Tag
                'ButtonPanel(1).Visible = Val(tmpTag)
                'chkUfis(2).Enabled = True
                'EnableUfisTasks False
            Case 2
                'ButtonPanel(1).Visible = False
            Case Else
        End Select
        chkUfis(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub EnableUfisTasks(SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To chkUfisTask.UBound
        chkUfisTask(i).Enabled = SetEnable
        chkUfisTask(i).Value = 0
    Next
End Sub

Private Sub ToggleUfisTasks(Index As Integer, ForWhat As String)
    Dim i As Integer
    Dim cnt As Integer
    Dim tmpTag As String
    If ForWhat = "DN" Then
        For i = 0 To chkUfisTask.UBound
            If i <> Index Then chkUfisTask(i).Value = 0
        Next
    End If
    If ForWhat = "UP" Then
        cnt = 0
        For i = 0 To chkUfisTask.UBound
            If chkUfisTask(i).Value = 1 Then cnt = cnt + 1
        Next
        If cnt = 0 Then
            Me.Caption = "UFIS Web Browser"
            stbMain.Panels(2).Text = ""
            txtURL(1).Visible = False
            tmpTag = txtURL(1).Tag
            txtURL(1).Tag = ""
            If tmpTag <> "" Then
                If tmpTag <> txtURL(0).Text Then
                    txtURL(0).Text = tmpTag
                    chkGo(0).Value = 1
                End If
            End If
        End If
    End If
End Sub

Private Sub WebUfisTask(CurButton As CheckBox, TagCode As String)
    On Error Resume Next
    txtURL(1).Visible = True
    txtURL(1).ZOrder
    If txtURL(1).Tag = "" Then txtURL(1).Tag = txtURL(0).Text
    Select Case TagCode
        Case "ufishome"
            Me.Caption = "UFIS Airport Solutions Home Page"
            txtURL(1).Text = "UFIS-AS Home Page"
            txtURL(0).Text = "http://www.ufis-as.com"
            chkGo(0).Value = 1
        Case "releasenotes"
            Me.Caption = "Telex Pool Release Note"
            txtURL(1).Text = "TelexPool Release Note on Jira"
            'txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
            txtURL(0).Text = "http://ufisonline.jira.com/wiki/x/pQFy"
            chkGo(0).Value = 1
        Case "openissues"
            Me.Caption = "Open Issues (Telex Pool)"
            txtURL(1).Text = "TelexPool Component on Jira"
            txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
            chkGo(0).Value = 1
        Case Else
            txtURL(1).Visible = False
            txtURL(1).Tag = ""
    End Select
End Sub

Private Sub chkUfisTask_Click(Index As Integer)
    Dim tmpTag As String
    If chkUfisTask(Index).Value = 1 Then
        chkUfisTask(Index).BackColor = LightGreen
        ToggleUfisTasks Index, "DN"
        tmpTag = chkUfisTask(Index).Tag
        WebUfisTask chkUfisTask(Index), tmpTag
    Else
        chkUfisTask(Index).BackColor = vbButtonFace
        ToggleUfisTasks Index, "UP"
    End If
End Sub

Private Sub Form_Load()
    chkUfis(3).Top = 0
    RightPanel(0).Top = chkUfis(3).Height - 30
    MyBorder(0).Height = Screen.Height
    InitMyButtons
    chkUfis(3).ZOrder
    SplitHorz(0).Left = 0
    SplitHorz(0).Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    webMain.Left = 0
    webMain.Top = SplitHorz(0).Top + SplitHorz(0).Height
    WebNavigate txtURL(0).Text
End Sub
Private Sub InitMyButtons()
    ButtonPanel(0).Top = 0
    ButtonPanel(0).Left = 0
    
    'Web Browser Function Buttons
    Set ButtonPanel(3).Container = ButtonPanel(0)
    ButtonPanel(3).Width = chkTask(10).Left + chkTask(10).Width
    ButtonPanel(3).Left = chkUfis(1).Left + chkUfis(1).Width
    ButtonPanel(3).Top = 0
    ButtonPanel(3).Height = chkTask(1).Height
    ButtonPanel(3).BackColor = vbButtonFace
    ButtonPanel(3).Visible = True
    chkUfis(2).Left = ButtonPanel(3).Left + ButtonPanel(3).Width
    
    'URL Textbox and Go!
    Set ButtonPanel(1).Container = ButtonPanel(0)
    ButtonPanel(1).Left = chkUfis(2).Left + chkUfis(2).Width
    ButtonPanel(1).Top = 0
    ButtonPanel(1).Height = chkTask(1).Height
    ButtonPanel(1).BackColor = vbButtonFace
    ButtonPanel(1).Visible = True
    
    'Ufis Function Buttons
    Set ButtonPanel(2).Container = ButtonPanel(0)
    ButtonPanel(2).Left = chkUfisTask(0).Left + chkUfisTask(0).Width
    ButtonPanel(2).Top = ButtonPanel(3).Top + ButtonPanel(3).Height
    ButtonPanel(2).Height = 300
    ButtonPanel(2).BackColor = vbButtonFace
    ButtonPanel(2).Visible = True
    
    chkTask(4).Picture = imlToolbarIcons.ListImages(4).Picture  'reload
    chkTask(5).Picture = imlToolbarIcons.ListImages(5).Picture  'home
    chkTask(6).Picture = imlToolbarIcons.ListImages(6).Picture  'search
    chkTask(7).Picture = imlToolbarIcons.ListImages(9).Picture  'preview
    chkTask(9).Picture = imlToolbarIcons.ListImages(8).Picture  'print
    
    barLabel.Left = -15
    barMain.Left = 0
    barMain.ZOrder
End Sub
Private Sub Form_Resize()
    Dim NewSize As Long
    Dim NewLeft As Long
    NewSize = Me.ScaleWidth
    NewLeft = NewSize - chkUfis(3).Width
    chkUfis(3).Left = NewLeft
    NewSize = NewLeft
    RightPanel(0).Left = NewLeft
    If NewSize > 600 Then
        ButtonPanel(0).Width = NewSize
        NewSize = NewSize - (webMain.Left * 2)
        If NewSize > 600 Then
            webMain.Width = NewSize
        End If
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(1).Left
        If NewSize > 900 Then
            ButtonPanel(1).Width = NewSize
            NewLeft = NewSize - chkGo(0).Width
            chkGo(0).Left = NewLeft
            NewSize = NewLeft
            txtURL(0).Width = NewSize
            txtURL(1).Width = NewSize
            barMain.Width = NewSize
            barLabel.Width = NewSize + 30
        End If
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(2).Left
        If NewSize > 900 Then
            ButtonPanel(2).Width = NewSize
        End If
    End If
    NewSize = Me.ScaleHeight - stbMain.Height - RightPanel(0).Top
    If NewSize > 600 Then
        RightPanel(0).Height = NewSize
    End If
    NewSize = Me.ScaleHeight - stbMain.Height - webMain.Top
    If NewSize > 600 Then
        webMain.Height = NewSize
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
Set objDocument = Nothing
End Sub

Private Sub txtURL_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtURL_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        chkGo(0).Value = 1
    End If
End Sub

Private Sub webMain_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    chkTask(3).Enabled = True
    'CheckCommands
End Sub

Private Sub webMain_CommandStateChange(ByVal Command As Long, ByVal Enable As Boolean)
    Select Case Command
        Case 1 'Forward
            'tlbMain.Buttons.Item("forward").Enabled = Enable
            chkTask(2).Enabled = Enable
        Case 2 'Back
            'tlbMain.Buttons.Item("back").Enabled = Enable
            chkTask(1).Enabled = Enable
    End Select
End Sub

Private Sub webMain_DocumentComplete(ByVal pDisp As Object, URL As Variant)
    Dim tmpData As String
    'tlbMain.Buttons.Item("stop").Enabled = False
    chkTask(3).Enabled = False
    If InStr(1, URL, "about:blank") = 0 Then stbMain.Panels.Item(2).Text = webMain.LocationName
    CheckCommands
    chkGo(0).Value = 0
    tmpData = webMain.LocationURL
    txtURL(0).Text = tmpData
    Screen.MousePointer = 0
End Sub

Private Sub webMain_DownloadBegin()
    barMain.Visible = True
    Screen.MousePointer = 11
    Me.Refresh
End Sub

Private Sub webMain_DownloadComplete()
    barMain.Visible = False
    Screen.MousePointer = 0
End Sub

Private Sub webMain_NavigateComplete2(ByVal pDisp As Object, URL As Variant)
    Set objDocument = webMain.Document
    Screen.MousePointer = 0
End Sub

Private Sub webMain_NavigateError(ByVal pDisp As Object, URL As Variant, Frame As Variant, StatusCode As Variant, Cancel As Boolean)
    'It seems that errors will no longer crash the application
    'as it could be observed before in FlightData Tool.
    'MsgBox "error"
End Sub

Private Sub webMain_NewWindow2(ppDisp As Object, Cancel As Boolean)
    'If tlbMain.Buttons.Item("popup").Value = tbrPressed Then
    '    If CheckPopup = False Then
    '        Cancel = True
    '        'stbMain.Panels.Item(3).Text = "A pop-up window has been blocked."
    '    End If
    'End If
End Sub

Private Sub webMain_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
    On Error Resume Next
    barMain.Max = ProgressMax
    barMain.Value = Progress
End Sub
