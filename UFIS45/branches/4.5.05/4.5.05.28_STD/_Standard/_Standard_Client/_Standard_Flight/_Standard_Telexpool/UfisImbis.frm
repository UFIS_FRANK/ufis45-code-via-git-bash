VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form UfisImbis 
   Caption         =   "UFIS IMBIS"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8580
   Icon            =   "UfisImbis.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3090
   ScaleWidth      =   8580
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   2805
      Width           =   8580
      _ExtentX        =   15134
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12039
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   7620
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   30
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   6570
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   30
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Timer BcSpoolTimer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   7080
      Top             =   510
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   6285
      TabIndex        =   0
      Top             =   0
      Width           =   6345
      Begin VB.CheckBox chkWork 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   5250
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "AutoSize"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Spooling"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   4200
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "ScrollBar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Scrolling"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   1035
      End
   End
   Begin TABLib.TAB BcSpoolerTab 
      Height          =   1635
      Left            =   30
      TabIndex        =   1
      Top             =   420
      Width           =   4515
      _Version        =   65536
      _ExtentX        =   7964
      _ExtentY        =   2884
      _StockProps     =   64
   End
   Begin TABLib.TAB BcTblTab 
      Height          =   525
      Index           =   0
      Left            =   30
      TabIndex        =   7
      Top             =   2190
      Visible         =   0   'False
      Width           =   4545
      _Version        =   65536
      _ExtentX        =   8017
      _ExtentY        =   926
      _StockProps     =   64
   End
End
Attribute VB_Name = "UfisImbis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Spooling As Boolean
Dim Scrolling As Boolean
Public Sub AdjustSystemStatus(ForWhat As String, SetMsg As String)
    Dim LineNo As Long
    Dim MaxLine As Long
    If LoggingIsActive Then
        Select Case ForWhat
            Case "NEWBC"
                BcSpoolerTab.InsertTextLine SetMsg, False
                MaxLine = BcSpoolerTab.GetLineCount
                If MaxLine > MaxLoggingLines Then
                    BcSpoolerTab.DeleteLine 0
                End If
                LineNo = BcSpoolerTab.GetLineCount - 1
                DeployNewMessage LineNo
            Case Else
        End Select
    End If
End Sub
Private Sub DeployNewMessage(LineNo As Long)
    Dim CurTblName As String
    Dim NewRec As String
    Dim idx As Integer
    Dim IsNew As Boolean
    Dim MaxLine As Long
    CurTblName = Trim(BcSpoolerTab.GetFieldValue(LineNo, "BTBL"))
    If CurTblName = "" Then CurTblName = "NONAME"
    idx = GetTabIndexByTblName(CurTblName)
    If idx < 0 Then
        idx = BcTblTab.UBound + 1
        Load BcTblTab(idx)
        InitNewBcTab BcTblTab(idx), CurTblName
        BcTblTab(idx).MyName = CurTblName
        BcTblTab(idx).Visible = True
        BcTblTab(idx).ZOrder
        IsNew = True
    End If
    NewRec = BcSpoolerTab.GetLineValues(LineNo)
    
    BcTblTab(idx).InsertTextLine NewRec, False
    MaxLine = BcTblTab(idx).GetLineCount
    If MaxLine > MaxLoggingLines Then
        BcTblTab(idx).DeleteLine 0
    End If
    BcTblTab(idx).Tag = "NEW"
    BcTblTab(idx).RedrawTab
    If IsNew Then Form_Resize
End Sub
Private Function GetTabIndexByTblName(GetName As String) As Integer
    Dim idx As Integer
    Dim i As Integer
    idx = -1
    For i = 0 To BcTblTab.UBound
        If BcTblTab(i).MyName = GetName Then
            idx = i
            Exit For
        End If
    Next
    GetTabIndexByTblName = idx
End Function

Private Sub chkTask_Click(Index As Integer)
    Dim CurPath As String
    Dim CurName As String
    Dim ChkName As String
    Dim CurFile As String
    Dim i As Integer
    On Error GoTo errhdl
    If chkTask(Index).Value = 1 Then
        Select Case Index
            Case 0
                CurPath = "d:\tmp\"
                For i = 1 To BcTblTab.UBound
                    CurName = "BCTBLTAB_" & CStr(i) & ".dat"
                    CurFile = CurPath & CurName
                    BcTblTab(i).WriteToFile CurFile, False
                Next
            Case 1
                CurPath = "d:\tmp\"
                Err.Clear
                i = 1
                While (i >= 0) And (Err.Description = "")
                    If i > BcTblTab.UBound Then
                        Load BcTblTab(i)
                        InitNewBcTab BcTblTab(i), BcTblTab(i).MyName
                    End If
                    CurName = "BCTBLTAB_" & CStr(i) & ".dat"
                    CurFile = CurPath & CurName
                    ChkName = Dir(CurFile)
                    If UCase(CurName) = UCase(ChkName) Then
                        BcTblTab(i).ResetContent
                        BcTblTab(i).ReadFromFile CurFile
                        BcTblTab(i).Visible = True
                        i = i + 1
                    Else
                        i = -1
                    End If
                Wend
            Case Else
        End Select
        chkTask(Index).Value = 0
    End If
    Exit Sub
errhdl:
    Exit Sub
End Sub

Private Sub Form_Load()
    InitNewBcTab BcSpoolerTab, "BC Spooler"
    UfisImbisIsActive = True
End Sub
Private Sub InitNewBcTab(UseTab As TABLib.Tab, TableName As String)
    Dim BcFields As String
    
    UseTab.ResetContent
    BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
    UseTab.HeaderString = BcFields & Space(500)
    UseTab.LogicalFieldList = BcFields
    UseTab.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10"
    UseTab.FontName = "Arial"
    UseTab.FontSize = 14
    UseTab.lineHeight = 15
    UseTab.MainHeader = True
    UseTab.SetMainHeaderFont 14, False, False, True, 0, "Arial"
    UseTab.SetMainHeaderValues "13", TableName & " Message Data,System", ""
    UseTab.LifeStyle = True
    UseTab.SetFieldSeparator (Chr(16))
    UseTab.AutoSizeByHeader = True
    UseTab.AutoSizeColumns

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = True
            Me.Hide
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    Dim NewTop As Long
    Dim i As Integer
    NewSize = Me.ScaleWidth - 60
    If NewSize > 660 Then
        BcSpoolerTab.Width = NewSize
        NewSize = NewSize - 60
        LeftWidth = NewSize / 2
        RightWidth = NewSize - LeftWidth
        NewTop = BcSpoolerTab.Top + BcSpoolerTab.Height
        NewSize = Me.ScaleHeight - StatusBar1.Height - NewTop - 60
        If BcTblTab.UBound > 2 Then
            NewSize = NewSize / ((BcTblTab.count) / 2)
            If NewSize < 300 Then NewSize = 300
        End If
        For i = 1 To BcTblTab.UBound
            If (i Mod 2) = 0 Then
                BcTblTab(i).Top = NewTop
                BcTblTab(i).Left = LeftWidth + 30
                If RightWidth > 300 Then BcTblTab(i).Width = RightWidth
                If NewSize > 300 Then BcTblTab(i).Height = NewSize
                NewTop = NewTop + NewSize
            Else
                BcTblTab(i).Top = NewTop
                BcTblTab(i).Left = 30
                If NewSize > 300 Then BcTblTab(i).Height = NewSize
                If LeftWidth > 300 Then BcTblTab(i).Width = LeftWidth
            End If
        Next
    End If
End Sub
Private Sub chkWork_Click(Index As Integer)
    Dim i As Integer
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                BcSpoolerTab.AutoSizeColumns
                For i = 1 To BcTblTab.UBound
                    BcTblTab(i).AutoSizeColumns
                Next
                chkWork(Index).Value = 0
            Case 1
                Spooling = True
                'BcSpoolReleased = False
                BcSpoolTimer.Enabled = False
            Case 2
                BcSpoolerTab.Refresh
                chkWork(Index).Value = 0
            Case 3
                BcSpoolerTab.ShowHorzScroller True
                BcSpoolerTab.Refresh
            Case 4
                Scrolling = True
            Case 5
                Me.Hide
                chkWork(Index).Value = 0
            Case Else
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
            Case 1
                Spooling = False
                'BcSpoolReleased = True
                'BcSpoolTimer.Enabled = True
            Case 2
            Case 3
                BcSpoolerTab.ShowHorzScroller False
                BcSpoolerTab.Refresh
            Case 4
                Scrolling = False
            Case 5
            Case Else
        End Select
    End If
End Sub

Private Sub BcSpoolTimer_Timer()
    'If BcSpoolerTab.GetLineCount > 0 Then
        GetBcFromSpooler
    'End If
End Sub

Public Sub GetBcFromSpooler()
    Dim ReqId As String
    Dim DestName As String
    Dim RecvName As String
    Dim CedaCmd As String
    Dim ObjName As String
    Dim Seq As String
    Dim tws As String
    Dim twe As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    Dim BcNum As String
    Dim GotNew As Boolean
    Dim i As Integer
    If Not Spooling Then
        If BcSpoolerTab.GetLineCount > 0 Then
            ''BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
            ''TabColNo = "0   ,1   ,2   ,3   ,4   ,5   ,6   ,7   ,8   ,9   ,10  ,11  ,12
            'Seq = BcSpoolerTab.GetColumnValue(0, 0)
            'BcNum = BcSpoolerTab.GetColumnValue(0, 1)
            'ReqId = BcSpoolerTab.GetColumnValue(0, 2)
            'CedaCmd = BcSpoolerTab.GetColumnValue(0, 3)
            'ObjName = BcSpoolerTab.GetColumnValue(0, 4)
            'CedaSqlKey = BcSpoolerTab.GetColumnValue(0, 5)
            'Data = BcSpoolerTab.GetColumnValue(0, 6)
            'Fields = BcSpoolerTab.GetColumnValue(0, 7)
            'tws = BcSpoolerTab.GetColumnValue(0, 8)
            'twe = BcSpoolerTab.GetColumnValue(0, 9)
            'DestName = BcSpoolerTab.GetColumnValue(0, 10)
            'RecvName = BcSpoolerTab.GetColumnValue(0, 11)
            ''BcRema = BcSpoolerTab.GetColumnValue(0, 12)
            'BcSpoolerTab.DeleteLine 0
            'HandleBroadcast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, tws, twe, CedaSqlKey, Fields, Data, BcNum
        End If
    End If
    'BcSpoolTimer.Enabled = Not Spooling
    'If BcSpoolerTab.GetLineCount = 0 Then
        'BcSpoolTimer.Enabled = False
        'BcSpoolReleased = False
    'ElseIf Scrolling Then
        For i = 1 To BcTblTab.UBound
            If BcTblTab(i).Tag = "NEW" Then
                BcTblTab(i).OnVScrollTo BcTblTab(i).GetLineCount
                BcTblTab(i).Tag = ""
                GotNew = True
            End If
        Next
        If GotNew Then
            BcSpoolerTab.OnVScrollTo BcSpoolerTab.GetLineCount
        End If
    'End If
End Sub


