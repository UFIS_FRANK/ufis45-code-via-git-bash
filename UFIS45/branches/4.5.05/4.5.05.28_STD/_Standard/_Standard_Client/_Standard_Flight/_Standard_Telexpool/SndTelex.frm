VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form SndTelex 
   Caption         =   "Created Telexes"
   ClientHeight    =   2355
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3510
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SndTelex.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   2355
   ScaleWidth      =   3510
   StartUpPosition =   3  'Windows Default
   Tag             =   "Created Telexes"
   Begin VB.PictureBox ListPanel 
      Height          =   1245
      Left            =   0
      ScaleHeight     =   1185
      ScaleWidth      =   3375
      TabIndex        =   4
      Top             =   300
      Width           =   3435
      Begin TABLib.TAB tabTelexList 
         Height          =   1170
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   3030
         _Version        =   65536
         _ExtentX        =   5345
         _ExtentY        =   2064
         _StockProps     =   64
         Columns         =   9
         Lines           =   10
      End
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Editor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "SndTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PrevWindowState As Integer
Private m_Snap As CSnapDialog
'Private WithEvents UfisMouseWheel As CMouseWheel
Public Function DeleteLineByUrno(BcSel As String) As Boolean
    Dim RetVal As Boolean
    Dim tmpUrno As String
    Dim tmpHitList As String
    Dim LineNo As Long
    RetVal = False
    tmpUrno = GetUrnoFromSqlKey(BcSel)
    tmpHitList = tabTelexList.GetLinesByColumnValue(9, tmpUrno, 0)
    If tmpHitList <> "" Then
        LineNo = Val(tmpHitList)
        tabTelexList.DeleteLine LineNo
        SndTelex.Caption = CStr(tabTelexList.GetLineCount) & " " & SndTelex.Tag
        RetVal = True
    End If
    DeleteLineByUrno = RetVal
End Function
Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList.LockScroll True
    Else
        tabTelexList.LockScroll False
    End If
End Sub

Private Sub Form_Activate()
    Static AlreadyDone As Boolean
    If Not AlreadyDone Then
        AlreadyDone = True
        tabTelexList.Refresh
        Me.Refresh
        DoEvents
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_Load()
        If UseSnapFeature Then
            If Not ApplIsInDesignMode Then
                Set m_Snap = New CSnapDialog
                m_Snap.hwnd = Me.hwnd
            End If
        End If
        'Set UfisMouseWheel = New CMouseWheel
        'Set UfisMouseWheel.Form = Me
        'UfisMouseWheel.SubClassHookForm
    InitForm 6
    InitPosSize
    If Not AutoRefreshOnline Then
        chkLoad.BackColor = DarkGreen
        chkLoad.ForeColor = vbWhite
    End If
End Sub
Private Sub InitPosSize()
    Dim MainTop As Long
    Dim MainHight As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim MyPlace As Long
    MainHight = MaxScreenHeight
    MainTop = (Screen.Height - MainHight) / 2
    MyPlace = GetMyStartupWindowPlace(Me.Name)
    Me.Width = 3600
    If OnlineWindows > 0 Then
        NewHeight = MainHight / OnlineWindows
        NewTop = MainTop + (NewHeight * (MyPlace - 1))
        Me.Top = NewTop
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = NewHeight
        If Not MainIsOnline Then Me.Show
    Else
        Me.Top = 3675
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = 6000
    End If
End Sub

Private Sub InitForm(ipLines As Integer)
    tabTelexList.ResetContent
    tabTelexList.FontName = "Courier New"
    tabTelexList.HeaderFontSize = MyFontSize
    tabTelexList.FontSize = MyFontSize
    tabTelexList.lineHeight = MyFontSize
    tabTelexList.SetTabFontBold MyFontBold
    tabTelexList.Top = 0
    tabTelexList.Left = 0
    tabTelexList.Width = ListPanel.ScaleWidth
    tabTelexList.Height = ipLines * tabTelexList.lineHeight * Screen.TwipsPerPixelY
    ListPanel.Height = tabTelexList.Height + 60
    tabTelexList.HeaderLengthString = "12,12,32,22,61,47,41,41,41,50,50,50,500"
    tabTelexList.HeaderString = "S,W,Type,ST,Flight,Registr,Plan,Time1,Time2,URNO,Index,FLNU," & Space(500)
    tabTelexList.SetUniqueFields "9"
    tabTelexList.DateTimeSetColumn 6
    tabTelexList.DateTimeSetInputFormatString 6, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 6, "hh':'mm"
    tabTelexList.DateTimeSetColumn 7
    tabTelexList.DateTimeSetInputFormatString 7, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 7, "hh':'mm"
    tabTelexList.DateTimeSetColumn 8
    tabTelexList.DateTimeSetInputFormatString 8, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 8, "hh':'mm"
    tabTelexList.LifeStyle = True
    tabTelexList.ShowHorzScroller True
    tabTelexList.AutoSizeByHeader = True
    tabTelexList.AutoSizeColumns
    Me.Height = ListPanel.Top + ListPanel.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    If (PrevWindowState = vbNormal) And (Me.WindowState = vbNormal) And (Not SystemResizing) Then LastResizeName = Me.Name
    If Me.WindowState <> vbMinimized Then
        ListPanel.Width = Me.ScaleWidth - 2 * ListPanel.Left
        tabTelexList.Width = ListPanel.ScaleWidth
        If Me.ScaleHeight > 500 Then
            ListPanel.Height = Me.ScaleHeight - ListPanel.Top - ListPanel.Left
            tabTelexList.Height = ListPanel.ScaleHeight
        End If
        SyncWindowSize Me.Left, Me.Width
    End If
End Sub
Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = LightGreen
        chkLoad.ForeColor = vbBlack
        chkLoad.Refresh
        TelexPoolHead.RefreshOnlineWindows "SND"
        tabTelexList.AutoSizeColumns
        tabTelexList.Refresh
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    On Error Resume Next
    If OpenPool.Value = 1 Then
        SwitchMainWindows "CREATE"
        OpenPool.Value = 0
    End If
End Sub

Private Sub tabTelexList_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    Select Case Key
        Case 13
        Case 46
            DeleteTelex LineNo
        Case Else
    End Select
End Sub
Private Sub DeleteTelex(LineNo As Long)
    Dim AskRetVal As Integer
    Dim MsgText As String
    Dim tmpUrno As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    
    If LineNo >= 0 Then
        MsgText = "Do you want to delete this telex?"
        AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "Telex Deletion", MsgText, "ask", "Yes,No", UserAnswer)
        If AskRetVal = 1 Then
            'AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC CSGN Update", "Still under construction.", "hand", "", UserAnswer)
            'AskRetVal = 1
            If UfisServer.HostName <> "LOCAL" Then
                tmpUrno = Trim(tabTelexList.GetColumnValue(LineNo, 9))
                If tmpUrno <> "" Then
                    Screen.MousePointer = 11
                    clSqlKey = "WHERE URNO=" & tmpUrno
                    RetCode = UfisServer.CallCeda(RetVal, "DRT", DataPool.TableName, "URNO", tmpUrno, clSqlKey, "", 0, True, False)
                    Screen.MousePointer = 0
                End If
            End If
        End If
    End If
End Sub
Private Sub tabTelexList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If Not NewMsgFromBcHdl Then
        If (Selected) And (LineNo >= 0) Then
            tabTelexList.Refresh
            ShowTelex LineNo, True
        End If
    End If
End Sub

Private Sub ShowTelex(LineNo As Long, UseFlags As Boolean)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    Dim IsOk As Boolean
    tabTelexList.Refresh
    tmpWsta = Trim(tabTelexList.GetColumnValue(LineNo, 1))
    tmpUrno = Trim(tabTelexList.GetColumnValue(LineNo, 9))
    If tmpUrno <> "" Then
        Screen.MousePointer = 11
        clSqlKey = "WHERE URNO=" & tmpUrno
        'If (UpdateWstaOnClick = True) And (tmpWsta = "") Then
        If tmpWsta = "" Then
            If UfisServer.HostName <> "LOCAL" Then
                RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", "O", clSqlKey, "", 0, True, False)
            End If
            tabTelexList.SetColumnValue LineNo, 1, "O"
        End If
        Screen.MousePointer = 11
        IsOk = CreateTelex.OpenCreatedTelex(tmpUrno)
        tabTelexList.SetFocus
        If Not IsOk Then
            Screen.MousePointer = 11
            If UfisServer.HostName <> "LOCAL" Then
                TelexPoolHead.LoadTelexData 3, "", clSqlKey, "", "", "CRT"
            End If
        End If
        Screen.MousePointer = 0
    End If
End Sub



Private Sub tabTelexList_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "SND.TABTLX.X"
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = tabTelexList
    SetMouseWheelObjectCode tmpCode
End Sub
