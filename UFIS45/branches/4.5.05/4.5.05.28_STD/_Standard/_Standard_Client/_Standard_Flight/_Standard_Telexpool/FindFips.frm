VERSION 5.00
Begin VB.Form FindFips 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Find Window"
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   4290
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   4290
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMsgBox 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   60
      TabIndex        =   4
      Top             =   390
      Width           =   1275
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton cmdEnumWindows 
      Caption         =   "EnumWin"
      Height          =   315
      Left            =   1800
      TabIndex        =   3
      Top             =   870
      Width           =   855
   End
   Begin VB.CommandButton cmdFindWindow 
      Caption         =   "FindWin"
      Height          =   315
      Left            =   930
      TabIndex        =   2
      Top             =   870
      Width           =   855
   End
   Begin VB.TextBox txtWindowTitle 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   60
      TabIndex        =   0
      Text            =   "FIPS"
      Top             =   60
      Width           =   1275
   End
   Begin VB.CommandButton cmdAppActivate 
      Caption         =   "AppActiv"
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   870
      Width           =   855
   End
End
Attribute VB_Name = "FindFips"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ActionButton As CheckBox
Dim WinFound As Integer
Dim WinNotFound As Integer

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lparam As Long) As Long

Public Sub FipsStatusStart(LookName As String, PushButton As CheckBox, SetFound As Integer, SetNotFound As Integer)
    Timer1.Enabled = False
    txtWindowTitle.Text = LookName
    Set ActionButton = PushButton
    WinFound = SetFound
    WinNotFound = SetNotFound
    Timer1.Enabled = True
End Sub

Public Sub FipsStatusStop(LookName As String, PushButton As CheckBox)
    Timer1.Enabled = False
End Sub

' Use AppActivate to see if the window exists.
Private Sub cmdAppActivate_Click()
    On Error Resume Next
    If txtWindowTitle.Text <> "" Then
        AppActivate txtWindowTitle.Text
        If Err.Number <> 0 Then
            txtMsgBox.Text = "Does not exist."
        Else
            txtMsgBox.Text = "Window exists."
        End If
    End If
End Sub

' Use EnumWindows to see if the window exists.
Private Sub cmdEnumWindows_Click()
    If txtWindowTitle.Text <> "" Then
        txtMsgBox.Text = ""
        TargetName = txtWindowTitle.Text
        TargetHwnd = 0
        
        ' Examine the window names.
        EnumWindows AddressOf WindowEnumerator, 0
    
        ' See if we got an hwnd.
        If TargetHwnd = 0 Then
            txtMsgBox.Text = "Does not exist."
            ActionButton.Value = WinNotFound
        Else
            txtMsgBox.Text = "Window exists."
            ActionButton.Value = WinFound
        End If
    End If
End Sub

' Use FindWindow to see if the window exists.
Private Sub cmdFindWindow_Click()
    If txtWindowTitle.Text <> "" Then
        If FindWindow(ByVal 0, txtWindowTitle.Text) = 0 Then
            txtMsgBox.Text = "Does not exist."
        Else
            txtMsgBox.Text = "Window exists."
        End If
    End If
End Sub


Private Sub Timer1_Timer()
    cmdEnumWindows_Click
End Sub
