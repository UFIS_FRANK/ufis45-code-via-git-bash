VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form SearchList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Search List"
   ClientHeight    =   5865
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6300
   Icon            =   "SearchList.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5865
   ScaleWidth      =   6300
   StartUpPosition =   2  'CenterScreen
   Begin TABLib.TAB tabSearchList 
      Height          =   5295
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   6255
      _Version        =   65536
      _ExtentX        =   11033
      _ExtentY        =   9340
      _StockProps     =   64
   End
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      ScaleHeight     =   615
      ScaleWidth      =   6300
      TabIndex        =   1
      Top             =   5250
      Width           =   6300
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "Close"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         TabIndex        =   2
         Top             =   120
         Width           =   1215
      End
   End
End
Attribute VB_Name = "SearchList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private IsFormActivated As Boolean
Private ParentForm As Form
Private KeyColumn As Long

Public Sub SetParentForm(pParentForm As Form)
    Set ParentForm = pParentForm
End Sub

Public Sub SetMasterTab(TabObject As TABLib.Tab, ByVal ColumnIndexList As String, ByVal pKeyColumn As Long)
    Dim vntColumnIndexes As Variant
    Dim vntColumnIndex As Variant
    Dim vntHeaderString As Variant
    Dim vntHeaderAlignmentString As Variant
    Dim vntHeaderLengthString As Variant
    Dim vntLogicalFieldList As Variant
    Dim strHeaderString As String
    Dim strHeaderAlignmentString As String
    Dim strHeaderLengthString As String
    Dim strLogicalFieldList As String
    Dim sngWidth As Single

    KeyColumn = pKeyColumn
    
    vntColumnIndexes = Split(ColumnIndexList, ",")
    vntHeaderString = Split(TabObject.HeaderString, ",")
    vntHeaderAlignmentString = Split(TabObject.HeaderAlignmentString, ",")
    vntHeaderLengthString = Split(TabObject.HeaderLengthString, ",")
    vntLogicalFieldList = Split(TabObject.LogicalFieldList, ",")
    
    strHeaderString = ""
    strHeaderAlignmentString = ""
    strHeaderLengthString = ""
    strLogicalFieldList = ""
    sngWidth = 0
    For Each vntColumnIndex In vntColumnIndexes
        strHeaderString = strHeaderString & "," & vntHeaderString(vntColumnIndex)
        strHeaderAlignmentString = strHeaderAlignmentString & "," & vntHeaderAlignmentString(vntColumnIndex)
        strHeaderLengthString = strHeaderLengthString & "," & vntHeaderLengthString(vntColumnIndex)
        strLogicalFieldList = strLogicalFieldList & "," & vntLogicalFieldList(vntColumnIndex)
        
        sngWidth = sngWidth + 15.1875 * Val(vntHeaderLengthString(vntColumnIndex))
    Next vntColumnIndex
    If strHeaderString <> "" Then strHeaderString = Mid(strHeaderString, 2)
    If strHeaderAlignmentString <> "" Then strHeaderAlignmentString = Mid(strHeaderAlignmentString, 2)
    If strHeaderLengthString <> "" Then strHeaderLengthString = Mid(strHeaderLengthString, 2)
    If strLogicalFieldList <> "" Then strLogicalFieldList = Mid(strLogicalFieldList, 2)
    
    tabSearchList.ResetContent
    tabSearchList.HeaderString = strHeaderString
    tabSearchList.HeaderAlignmentString = strHeaderAlignmentString
    tabSearchList.HeaderLengthString = strHeaderLengthString
    tabSearchList.LogicalFieldList = strLogicalFieldList
    tabSearchList.ShowHorzScroller True
    tabSearchList.ShowVertScroller True
    
    Me.Width = sngWidth + 250
End Sub

Public Sub PopulateList(rsView As ADODB.Recordset)
    tabSearchList.ResetContent
    
    If Not (rsView Is Nothing) Then
        If Not (rsView.BOF And rsView.EOF) Then rsView.MoveFirst
        Do While Not rsView.EOF
            tabSearchList.InsertTextLine rsView.Fields("DATA").Value, False
            
            rsView.MoveNext
        Loop
        tabSearchList.Refresh
    End If
End Sub

Private Sub cmdClose_Click()
    Me.Hide
End Sub

Private Sub Form_Activate()
    If Not IsFormActivated Then
        If pblnIsOnTop Then
            SetFormOnTop Me, pblnIsOnTop
        End If
        IsFormActivated = True
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next

    If UnloadMode = 0 Then
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    If Me.WindowState <> vbMinimized Then
        tabSearchList.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight - picButtons.Height
    End If
End Sub

Private Sub picButtons_Resize()
    cmdClose.Left = picButtons.Width - cmdClose.Width - 120
End Sub

Private Sub tabSearchList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strKey As String
    
    If Selected Then
        If Not (ParentForm Is Nothing) Then
            strKey = tabSearchList.GetColumnValue(LineNo, KeyColumn)
            
            ParentForm.ChangeRowSelection strKey
        End If
    End If
End Sub
