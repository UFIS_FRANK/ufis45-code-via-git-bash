Attribute VB_Name = "CDIDModule"
Option Explicit

'Public Enum
Public Enum TableOperationType
    toInsert = 0
    toEdit = 1
    toDelete = 2
End Enum

Public Enum FlightTypeEnum
    ftypArrival = 1
    ftypDeparture = 2
    ftypRotation = 4
End Enum

Public Enum enmFormType
    ftyArrival = 0
    ftyDeparture = 1
End Enum

Private Declare Function SetParent Lib "user32" _
    (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
    (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, _
    lParam As Any) As Long
    
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
    (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, _
    ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Public Declare Function OpenIcon Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function GetForegroundWindow Lib "user32.dll" () As Long

Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
   (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) _
   As Long

Public Const GWL_STYLE = (-16)
Public Const WS_CHILD = &H40000000

Public Const GW_HWNDPREV = 3

Private Const CB_FINDSTRING = &H14C
Private Const CB_FINDSTRINGEXACT = &H158

Private Const SW_SHOWNORMAL = 1

'Public Variables for Configurations
Public IsUTCTimeZone As Boolean
Public colConfigs As CollectionExtended
Public pblnIsStandAlone As Boolean
Public pblnIsOnTop As Boolean
Public pblnFlightListWindow As Boolean
Public pblnFlightListMainButtons As Boolean
Public pblnShowRightButtons As Boolean
Public pblnShowFlightList As Boolean
Public pblnShowFlightFilterOnStartup As Boolean
Public pblnShowFlightDetails As Boolean
Public pblnHandleBc As Boolean

'Public Variables for Data Sources
Public pstrBasicData As String
Public pstrDataSource As String
Public colDataSources As New CollectionExtended

'Public Variables for Parameters
Public colParameters As New CollectionExtended

'Public Variable for UFISServer
Public oUfisServer As UfisServer

'Public Variable for Local Time/UTC Diff
Public colUTCTimeDiff As New CollectionExtended

Public SearchViewForm As Form
Public BcLogForm As Form
Public pintBcLogFileHandle As Integer

Public pstrAftUrnoList As String

Public pblnCanExitApp As Boolean

Public Function ReadFlightRecordByUrno(ByVal Urno As String, ByVal sAppType As String) As Boolean
    Dim vUrno As Variant
    Dim strWhere As String
    Dim oUFISRec As UFISRecordset
    Dim strUrno As String
    Dim colLocalParams As New CollectionExtended
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    Dim rsAft As ADODB.Recordset
    Dim intPos As Integer
    
    On Error GoTo ErrRead

    ReadFlightRecordByUrno = False
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, sAppType, "DATA_SOURCE", "AFTTAB")
    End If
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = ""
        vUrno = Split(Urno, ",")
        If vUrno(0) <> "" Then
            'strWhere = "(URNO = " & vUrno(0) & " AND ADID = 'A')" 'igu on 20/02/2012
            strWhere = "(URNO = " & vUrno(0) & ")" 'igu on 20/02/2012
        End If
        If vUrno(1) <> "" Then
            If strWhere <> "" Then
                strWhere = strWhere & " OR "
            End If
            'strWhere = strWhere & "(URNO = " & vUrno(1) & " AND ADID <> 'A')" 'igu on 20/02/2012
            strWhere = strWhere & "(URNO = " & vUrno(1) & ")" 'igu on 20/02/2012
        End If
        
        colLocalParams.Add "ADID", "OrderByClause"
        colLocalParams.Add strWhere, "WhereClause"
        
        Call AddDataSource(sAppType, colDataSources, "AFTTAB", colLocalParams)
        
        Set rsAft = colDataSources.Item("AFTTAB")
        If Not (rsAft Is Nothing) Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                strUrno = ""
                rsAft.MoveFirst
                Do While Not rsAft.EOF
                    strUrno = strUrno & "," & rsAft.Fields("URNO").Value
                    
                    rsAft.MoveNext
                Loop
                strUrno = Mid(strUrno, 2)
                rsAft.MoveFirst
                
                colParameters.Add strUrno, "AftUrnoList"
                
                vntDataSources = Split(pstrDataSource, ",")
                For Each vntDataSource In vntDataSources
                    If vntDataSource <> "AFTTAB" Then
                        Call AddDataSource(sAppType, colDataSources, vntDataSource, colParameters)
                    End If
                Next vntDataSource
                
                ReadFlightRecordByUrno = True
            End If
        End If
    End If
    
ErrRead:
End Function

Public Function Repeat(ByVal Number As Integer, ByVal Text As String)
    Dim Result As String
    Dim i As Integer

    Result = ""
    For i = 1 To Number
        Result = Result & Text
    Next

    Repeat = Result
End Function

Public Function UCaseAsc(ByVal AscCode As Integer) As Long
    UCaseAsc = Asc(UCase(Chr(AscCode)))
End Function

Public Function BuildSQLDateWhereClause(ByVal FieldName As String, ByVal Date1 As Date, _
ByVal Date2 As Date, Optional ByVal LocalDate As Boolean = True, Optional ByVal TimeDiff As Integer) As String
    
    Dim strRet As String
    
    If LocalDate Then
        If Date1 = Date2 Then
            Date2 = DateAdd("s", -1, LocalDateToUTC(DateAdd("D", 1, Date2), TimeDiff))
        Else
            Date2 = LocalDateToUTC(Date2, TimeDiff)
        End If
        Date1 = LocalDateToUTC(Date1, TimeDiff)
    
        strRet = FieldName & " BETWEEN '" & Format(Date1, "YYYYMMDDhhmmss") & "' " & _
                "AND '" & Format(Date2, "YYYYMMDDhhmmss") & "'"
    Else
        If Date1 = Date2 Then
            strRet = FieldName & " LIKE '" & Format(Date1, "YYYYMMDD") & "%'"
        Else
            strRet = FieldName & " BETWEEN '" & Format(Date1, "YYYYMMDD") & "000000' " & _
                "AND '" & Format(Date2, "YYYYMMDD") & "235959'"
        End If
    End If
    
    BuildSQLDateWhereClause = strRet
End Function

Public Function BuildSQLDateTimeWhereClause(ByVal FieldName As String, ByVal Date1 As Date, _
ByVal Date2 As Date, Optional ByVal LocalDate As Boolean = True, Optional ByVal TimeDiff As Integer) As String
    
    Dim strRet As String
    
    If LocalDate Then
        Date1 = LocalDateToUTC(Date1, TimeDiff)
        Date2 = LocalDateToUTC(Date2, TimeDiff)
    End If
    
    strRet = FieldName & " BETWEEN '" & Format(Date1, "YYYYMMDDhhmmss") & "' " & _
            "AND '" & Format(Date2, "YYYYMMDDhhmmss") & "'"
    
    BuildSQLDateTimeWhereClause = strRet
End Function

Public Function InsertUpdateRecords(ByVal TableName As String, ByVal FieldList As String, _
ByVal DataList As String) As String
    Dim strResult As String
    
    'oUfisServer.CallCeda strResult, "REL", TableName, FieldList, DataList, "LATE,NOBC,NOACTION", "", 0, False, False
    oUfisServer.CallCeda strResult, "REL", TableName, FieldList, DataList, "", "", 0, False, False
    
    InsertUpdateRecords = strResult
End Function

Public Function IsInList(ByVal StringCheck As String, ByVal StringMatch As String, Optional ByVal Separator As String = ",") As Boolean
    Dim vntStringMatch As Variant
    Dim i As Integer, j As Integer
    
    vntStringMatch = Split(StringMatch, Separator)
    
    IsInList = False
    For i = 0 To UBound(vntStringMatch)
        If InStr(StringCheck, vntStringMatch(i)) > 0 Then
            IsInList = True
            Exit For
        End If
    Next i
End Function

Public Function IsSummer(rsSeason As ADODB.Recordset, ByVal dtmDate As Date) As Boolean
    rsSeason.Find "VPTO >= #" & Format(dtmDate, "YYYY/MM/DD") & "#", , , 1
    Do While Not rsSeason.EOF
        If rsSeason.Fields("VPFR").Value <= dtmDate Then
            IsSummer = (Year(rsSeason.Fields("VPFR").Value) = Year(rsSeason.Fields("VPTO").Value))
            
            Exit Do
        End If
        
        rsSeason.MoveNext
    Loop
End Function

Public Function GetUTCTimeDiff(rsSeason As ADODB.Recordset, ByVal TimeDiffCollection As CollectionExtended, ByVal dtmDate As Date) As Long
    Dim strKey As String
    
    If IsSummer(rsSeason, dtmDate) Then
        strKey = "SU"
    Else
        strKey = "WI"
    End If
    
    GetUTCTimeDiff = Val(TimeDiffCollection.Item(strKey))
End Function

Public Function LocalDateToUTC(ByVal LocalDate As Date, ByVal TimeDiff, Optional rsSeason As ADODB.Recordset) As Date
    Dim lngTimeDiff As Long
    Dim i As Integer
    Dim dtmUtcDate As Date
    
    If IsObject(TimeDiff) Then
        If TypeOf TimeDiff Is CollectionExtended Then
            If rsSeason Is Nothing Then
                lngTimeDiff = TimeDiff.Item(1)
            Else
                'Find the best possible Time Diff
                For i = 1 To TimeDiff.Count
                    lngTimeDiff = TimeDiff.Item(i)
                    dtmUtcDate = DateAdd("H", -(lngTimeDiff / 60), LocalDate)
                    If UTCDateToLocal(dtmUtcDate, TimeDiff, rsSeason) = LocalDate Then
                        Exit For
                    End If
                Next i
            End If
        End If
    Else
        lngTimeDiff = Val(TimeDiff)
    End If
    
    LocalDateToUTC = DateAdd("H", -(lngTimeDiff / 60), LocalDate)
End Function

Public Sub UpdateRecord(rsUpd As ADODB.Recordset, ByVal CedaCmd As String, ByVal Fields As String, _
ByVal Data As String, ByVal Urno As String, Optional ByVal KeyField As String = "URNO")
    Dim aFields As Variant
    Dim aData As Variant
    Dim i As Integer
    Dim fld As ADODB.Field
    Dim strData As String
    Dim sFields As String
    Dim aFilter As Variant
    Dim aUrno As Variant
    Dim strFilter As String

    aFields = Split(Fields, ",")
    aData = Split(Data, ",")
    
    sFields = ""
    For Each fld In rsUpd.Fields
        sFields = sFields & "," & fld.Name
    Next fld
    If sFields <> "" Then sFields = Mid(sFields, 2)
    
    Select Case CedaCmd
        Case "UFR", "DFR", "URT", "DRT"
            If KeyField = "URNO" Then
                rsUpd.Find KeyField & " = " & Urno, , , 1
            Else
                aFilter = Split(KeyField, ",")
                aUrno = Split(Urno, ",")
                strFilter = ""
                For i = 0 To UBound(aFilter)
                    strFilter = strFilter & " AND " & aFilter(i) & " = " & aUrno(i)
                Next i
                If strFilter <> "" Then strFilter = Mid(strFilter, 6)
                rsUpd.Filter = strFilter
            End If
        Case "IFR", "IRT"
            rsUpd.AddNew
    End Select
    
    If Not rsUpd.EOF Then
        Select Case CedaCmd
            Case "DFR", "DRT"
                Do While Not rsUpd.EOF
                    rsUpd.Delete
                    
                    If KeyField = "URNO" Then
                        Exit Do
                    End If
                    
                    rsUpd.MoveNext
                Loop
            Case "UFR", "URT", "IFR", "IRT"
                Do While Not rsUpd.EOF
                    For i = 0 To UBound(aFields)
                        If InStr(sFields, aFields(i)) > 0 Then
                            strData = aData(i)
                            
                            Set fld = rsUpd.Fields(aFields(i))
                            Select Case fld.Type
                                Case adDate
                                    If Trim(strData) = "" Then
                                        fld.Value = Null
                                    Else
                                        fld.Value = UfisLib.CedaFullDateToVb(strData)
                                    End If
                                Case adInteger, adBigInt, adSingle, adDouble
                                    If Trim(strData) = "" Then
                                        fld.Value = Null
                                    Else
                                        fld.Value = Val(strData)
                                    End If
                                Case Else
                                    fld.Value = UfisLib.CleanString(strData, _
                                                SERVER_TO_CLIENT, _
                                                True)
                            End Select
                        End If
                    Next i
                    rsUpd.Update
                    
                    If CedaCmd = "IFR" Or CedaCmd = "IRT" Then
                        Exit Do
                    Else
                        If KeyField = "URNO" Then
                            Exit Do
                        End If
                    End If
                
                    rsUpd.MoveNext
                Loop
        End Select
    End If
    
    If KeyField <> "URNO" Then
        rsUpd.Filter = adFilterNone
    End If
End Sub

Public Sub UpdateRecords(rsUpd As ADODB.Recordset, ByVal CedaCmd As String, ByVal Fields As String, _
ByVal Data As String, ByVal UrnoList As String, Optional ByVal KeyField As String = "URNO")
    Dim aFields As Variant
    Dim aData As Variant
    Dim aUrnoList As Variant
    Dim i As Integer
    Dim sFields As String
    Dim sData As String
    
    aFields = Split(Fields, vbLf)
    aData = Split(Data, vbLf)
    aUrnoList = Split(UrnoList, vbLf)

    For i = 0 To UBound(aUrnoList)
        If aUrnoList(i) <> "" Then
            If Fields = "" Then
                sFields = ""
            Else
                If UBound(aFields) < i Then
                    sFields = aFields(0)
                Else
                    sFields = aFields(i)
                End If
            End If
            
            If Data = "" Then
                sData = ""
            Else
                If UBound(aData) < i Then
                    sData = aData(0)
                Else
                    sData = aData(i)
                End If
            End If
            
            Call UpdateRecord(rsUpd, CedaCmd, Fields, sData, aUrnoList(i), KeyField)
        End If
    Next i
End Sub

Public Sub UpdateViewCombo(cboView As ComboBox, ByVal CedaCmd As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim i As Integer
    Dim ViewCount As Integer
    Dim blnFound As Boolean
    Dim rsView As ADODB.Recordset

    Set rsView = colDataSources.Item("VCDTAB")
    
    Select Case CedaCmd
        Case "IRT"
            If Trim(rsView.Fields("PKNO").Value) = LoginUserName Then
                blnFound = False
                
                ViewCount = cboView.ListCount
                For i = 0 To ViewCount - 1
                    If cboView.ItemData(i) = rsView.Fields("URNO").Value Then
                        cboView.List(i) = GetViewName(rsView.Fields("TEXT").Value)
                        blnFound = True
                        Exit Sub
                    End If
                Next i
                If Not blnFound Then
                    cboView.AddItem GetViewName(rsView.Fields("TEXT").Value)
                    cboView.ItemData(cboView.NewIndex) = rsView.Fields("URNO").Value
                End If
            End If
        Case "URT"
            If InStr(ChangedFields, "TEXT") > 0 Then
                ViewCount = cboView.ListCount
                For i = 0 To ViewCount - 1
                    If cboView.ItemData(i) = rsView.Fields("URNO").Value Then
                        cboView.List(i) = GetViewName(rsView.Fields("TEXT").Value)
                        
                        Exit Sub
                    End If
                Next i
            End If
        Case "DRT"
            If Urno <> "" Then
                ViewCount = cboView.ListCount
                For i = 0 To ViewCount - 1
                    If cboView.ItemData(i) = Val(Urno) Then
                        If cboView.ListIndex = i Then
                            cboView.ListIndex = 0
                        End If
                        cboView.RemoveItem i
                        
                        Exit Sub
                    End If
                Next i
            End If
    End Select
End Sub

Public Function UTCDateToLocal(ByVal UTCDate As Date, ByVal TimeDiff, Optional rsSeason As ADODB.Recordset) As Date
    Dim lngTimeDiff As Long
    
    If IsObject(TimeDiff) Then
        If TypeOf TimeDiff Is CollectionExtended Then
            If rsSeason Is Nothing Then
                lngTimeDiff = TimeDiff.Item(1)
            Else
                lngTimeDiff = GetUTCTimeDiff(rsSeason, TimeDiff, UTCDate)
            End If
        End If
    Else
        lngTimeDiff = Val(TimeDiff)
    End If

    UTCDateToLocal = DateAdd("H", (lngTimeDiff / 60), UTCDate)
End Function

Public Function CheckValidDateExt(ByVal sDate As String, Optional ByVal AllowNull As Boolean = True) As String
    Const VALID_DATE_SEP = "-./ "
    
    Dim nLoc As Integer
    Dim i As Integer
    Dim sSep As String
    Dim vDate As Variant
    Dim sDay As String
    Dim sMonth As String
    Dim sYear As String
    Dim dtmDate As Date
    Dim nDateLength As Integer
        
    CheckValidDateExt = "ERROR"
    
    If Trim(sDate) = "" Then
        If AllowNull Then CheckValidDateExt = "NULL"
        
        Exit Function
    End If
    
    On Error GoTo ErrHandle
        
    'check for separator
    nLoc = 0
    For i = 1 To Len(VALID_DATE_SEP)
        sSep = Mid(VALID_DATE_SEP, i, 1)
        nLoc = InStr(sDate, sSep)
        If nLoc > 0 Then Exit For
    Next i
    
    If nLoc > 0 Then 'Has separator
        vDate = Split(sDate, sSep)
        If UBound(vDate) <> 2 Then
            Exit Function
        End If
        
        sDay = vDate(0)
        sMonth = vDate(1)
        sYear = vDate(2)
    Else 'No separator
        nDateLength = Len(sDate)
        If nDateLength <> 6 And nDateLength <> 8 Then
            Exit Function
        End If
        
        sDay = Left(sDate, 2)
        sMonth = Mid(sDate, 3, 2)
        sYear = Right(sDate, nDateLength - 4)
    End If
    If Len(sYear) = 2 Then
        sYear = Left(CStr(Year(Date)), 2) & sYear
    End If
        
    sDate = Format(sYear, "0000") & Format(sMonth, "00") & Format(sDay, "00")
    
    If Not CheckValidDate(sDate) Then
        Exit Function
    End If
    
    'Get the date
    dtmDate = DateSerial(CInt(sYear), CInt(sMonth), CInt(sDay))
        
    CheckValidDateExt = Format(dtmDate, "dd-mm-yyyy")
    
    Exit Function
    
ErrHandle:
End Function

Public Function CheckValidTimeExt(ByVal sTime As String, Optional ByVal AllowNull As Boolean = True) As String
    Dim nLoc As Integer
    Dim sTimePart As String
    Dim sAddPart As String
    Dim sSign As String
    Dim sHour As String
    Dim sMinute As String
    Dim sSep As String
    Dim blnAdd As Boolean
    Dim i As Integer
    Dim nAdjust As Integer
        
    CheckValidTimeExt = "ERROR"
    
    If Trim(sTime) = "" Then
        If AllowNull Then CheckValidTimeExt = "NULL"
        
        Exit Function
    End If
    
    'look for plus or minus sign
    blnAdd = False
    sSign = "+"
    nLoc = InStr(sTime, sSign)
    If nLoc > 0 Then
        blnAdd = True
    Else
        sSign = "-"
        nLoc = InStr(sTime, sSign)
        If nLoc > 0 Then
            blnAdd = True
        End If
    End If
    
    If blnAdd Then
        sTimePart = Trim(Left(sTime, nLoc - 1))
        sAddPart = Trim(Mid(sTime, nLoc + 1))
    Else
        sTimePart = Trim(sTime)
    End If
    
    'check for the additional part
    If blnAdd Then
        If sAddPart = "" Then
            Exit Function
        Else
            If CStr(Abs(CInt(Val(sAddPart)))) <> sAddPart Then
                Exit Function
            End If
        End If
    End If
    
    'check for time part
    'this is valid 20:10 20.10 20 10 2010
    nAdjust = 0
    sSep = ":"
    nLoc = InStr(sTimePart, sSep)
    If nLoc = 0 Then
        sSep = "."
        nLoc = InStr(sTimePart, sSep)
        If nLoc = 0 Then
            sSep = " "
            nLoc = InStr(sTimePart, sSep)
            If nLoc = 0 Then
                nLoc = 3
                nAdjust = 1
            End If
        End If
    End If
    
    sHour = Trim(Left(sTimePart, nLoc - 1))
    sMinute = Trim(Mid(sTimePart, nLoc + 1 - nAdjust))
    
    'cek for hour
    If sHour = "" Then
        Exit Function
    Else
        If CStr(Abs(CInt(Val(sHour)))) <> sHour Then
            If Format(CStr(Abs(CInt(Val(sHour)))), "00") <> sHour Then
                Exit Function
            End If
        Else
            If CInt(sHour) > 23 Then
                Exit Function
            End If
        End If
    End If
    
    'cek for minute
    If sMinute = "" Then
        Exit Function
    Else
        If CStr(Abs(CInt(Val(sMinute)))) <> sMinute Then
            If Format(CStr(Abs(CInt(Val(sMinute)))), "00") <> sMinute Then
                Exit Function
            End If
        Else
            If CInt(sMinute) > 59 Then
                Exit Function
            End If
        End If
    End If
    
    CheckValidTimeExt = Format(sHour, "00") & ":" & Format(sMinute, "00")
    If blnAdd Then
        CheckValidTimeExt = CheckValidTimeExt & ";" & sSign & sAddPart
    End If
End Function

Public Function CheckValidDuration(ByVal sDuration As String, Optional ByVal MaxLength As Integer = 4, _
Optional ByVal AllowNull As Boolean = True) As String
    Dim sMinute As String
        
    CheckValidDuration = "ERROR"
    
    If Trim(sDuration) = "" Then
        If AllowNull Then CheckValidDuration = "NULL"
        
        Exit Function
    End If
    
    'Check the length
    Select Case Len(sDuration)
        Case MaxLength
            'Do nothing
        Case Is > MaxLength
            Exit Function
        Case Else
            sDuration = String(MaxLength - Len(sDuration), "0")
    End Select
    
    'All characters must be numeric
    If Format(Val(sDuration), String(MaxLength, "0")) <> sDuration Then
        Exit Function
    End If
    
    'check for minute part (last 2 chars)
    sMinute = Right(sDuration, 2)
    If CInt(sMinute) > 59 Then
        Exit Function
    End If
    
    CheckValidDuration = sDuration
End Function

Public Sub CloneThisTab(UseTab As TABLib.Tab, NewTab As TABLib.Tab)
    NewTab.ResetContent
    NewTab.FontName = UseTab.FontName
    NewTab.FontSize = UseTab.FontSize
    NewTab.HeaderFontSize = UseTab.HeaderFontSize
    NewTab.LineHeight = UseTab.LineHeight
    NewTab.LeftTextOffset = UseTab.LeftTextOffset
    NewTab.SetTabFontBold True
    NewTab.HeaderString = UseTab.HeaderString
    NewTab.HeaderAlignmentString = UseTab.HeaderAlignmentString
    NewTab.HeaderLengthString = UseTab.HeaderLengthString
    NewTab.LogicalFieldList = UseTab.LogicalFieldList
    NewTab.ColumnAlignmentString = UseTab.ColumnAlignmentString
    NewTab.ColumnWidthString = UseTab.ColumnWidthString
    NewTab.SetMainHeaderValues UseTab.GetMainHeaderRanges, UseTab.GetMainHeaderValues, UseTab.GetMainHeaderColors
    NewTab.SetMainHeaderFont UseTab.HeaderFontSize, False, False, True, 0, UseTab.FontName
    NewTab.MainHeader = UseTab.MainHeader
    NewTab.GridLineColor = UseTab.GridLineColor
    NewTab.LifeStyle = UseTab.LifeStyle
    NewTab.CursorLifeStyle = UseTab.CursorLifeStyle
    NewTab.SelectColumnBackColor = UseTab.SelectColumnBackColor
    NewTab.SelectColumnTextColor = UseTab.SelectColumnTextColor
    NewTab.ShowHorzScroller True
    NewTab.ShowVertScroller True
    NewTab.AutoSizeByHeader = UseTab.AutoSizeByHeader
End Sub

Public Sub CloneThisTabWithData(UseTab As TABLib.Tab, NewTab As TABLib.Tab)
    Dim i As Integer, j As Integer
    Dim strLine As String
    
    Call CloneThisTab(UseTab, NewTab)
    
    NewTab.ShowHorzScroller False
    
    For i = 0 To UseTab.GetLineCount - 1
        strLine = UseTab.GetFieldValues(i, UseTab.LogicalFieldList)
        
        NewTab.InsertTextLine strLine, False
        'If i Mod 2 = 1 Then
        '    NewTab.SetLineColor i, vbBlack, &HFFC0B0
        'End If
    Next i
    
    'NewTab.SetHeaderColors &HC3893F, vbWhite
End Sub

Public Function ShortTimeToFullDateTime(ByVal BaseDate As Date, ByVal UFISHour As String) As Date
    Dim nLoc As Integer
    Dim nDayAdd As Integer
    Dim dtmTime As Date
    
    nLoc = InStr(UFISHour, "+")
    If nLoc <= 0 Then
        nLoc = InStr(UFISHour, "-")
    End If
    If nLoc > 0 Then
        nDayAdd = CInt(Mid(UFISHour, nLoc))
    Else
        nDayAdd = 0
    End If
    dtmTime = TimeSerial(CInt(Left(UFISHour, 2)), CInt(Mid(UFISHour, 4, 2)), 0)
    ShortTimeToFullDateTime = DateAdd("d", nDayAdd, BaseDate) + dtmTime
End Function

Public Function FormWithinForm(Parent As Object, Child As Object)
    On Error Resume Next
    
    SetParent Child.hwnd, Parent.hwnd
    SetWindowLong Child.hwnd, GWL_STYLE, WS_CHILD
    FormWithinForm = (Err.Number = 0 And Err.LastDllError = 0)
End Function

Public Sub FillViewComboBox(rsData As ADODB.Recordset, ByVal cbo As ComboBox)
    cbo.Clear
    If Not rsData Is Nothing Then
        If Not (rsData.BOF And rsData.EOF) Then
            rsData.MoveFirst
            Do While Not rsData.EOF
                If Trim(rsData.Fields("PKNO").Value) = LoginUserName Then
                    cbo.AddItem GetViewName(rsData.Fields("TEXT").Value)
                    cbo.ItemData(cbo.NewIndex) = rsData.Fields("URNO").Value
                End If
                
                rsData.MoveNext
            Loop
        End If
    End If
End Sub

Public Function FindInCombobox(pCB As ComboBox, ByVal pstrComboBItem As String, Optional ByVal FindExact As Boolean = True) As Long
    Dim lngComboIndex As Long
    Dim lngFindBehaviour As Long
    
    If FindExact Then
        lngFindBehaviour = CB_FINDSTRINGEXACT
    Else
        lngFindBehaviour = CB_FINDSTRING
    End If
    
    ' lngComboIndex is the ComboIndex if the item is found
    lngComboIndex = SendMessage(pCB.hwnd, lngFindBehaviour, -1, ByVal pstrComboBItem)
    
    FindInCombobox = lngComboIndex
End Function

Public Function FullDateTimeToShortTime(ByVal BaseDate As Date, ByVal FullDate As Date) As String
    Dim strAdd As String
    Dim nDayAdd As Integer
    Dim dtmTime As Date
    
    If FullDate <> CDate(0) Then
        nDayAdd = DateDiff("d", BaseDate, FullDate)
        If nDayAdd = 0 Then
            strAdd = ""
        Else
            strAdd = CStr(nDayAdd)
            If nDayAdd > 0 Then
                strAdd = "+" & strAdd
            End If
        End If
        dtmTime = TimeSerial(Hour(FullDate), Minute(FullDate), 0)
        FullDateTimeToShortTime = Format(dtmTime, "hh:mm") & strAdd
    End If
End Function

Public Function GetViewName(ByVal ViewText As String) As String
    Dim intLoc As Integer
    Dim strText As String
    Dim strFind As String
    
    strText = ViewText
    strFind = "VIEW="
    intLoc = InStr(strText, strFind)
    If intLoc > 0 Then
        strText = Mid(strText, intLoc + Len(strFind))
        strFind = ";"
        intLoc = InStr(strText, strFind)
        If intLoc > 0 Then
            strText = Left(strText, intLoc - 1)
        End If
        GetViewName = strText
    End If
End Function

Public Function GetViaListString(ByVal ViaList As String, Optional ByVal ViaNumber As Integer = 8) As String
    Const SEP_CHAR = "/"
    Dim i As Integer
    Dim strRet As String
    Dim strVia As String
    
    strRet = ""
    For i = 1 To ViaNumber
        strVia = Mid(ViaList, 120 * i - 119, 120)
        strVia = Mid(strVia, 2, 3)
        If strVia = "" Then Exit For
        
        strRet = strRet & SEP_CHAR & strVia
    Next i
    If strRet <> "" Then strRet = Mid(strRet, 2)
    GetViaListString = strRet
End Function

Public Function GetCSLeg(ByVal ViaListString As String, ByVal OrgDes As String, ByVal sAdid As String) As String
    Const SEP_CHAR = "/"
    Dim strSep As String
    
    If Trim(ViaListString) = "" Then
        strSep = ""
    Else
        strSep = SEP_CHAR
    End If
    
    If sAdid = "A" Then
        GetCSLeg = OrgDes & strSep & ViaListString
    Else
        GetCSLeg = ViaListString & strSep & OrgDes
    End If
End Function

Public Function ValidateCSLeg(rsData As ADODB.Recordset, ByVal CSLeg As String) As Boolean
    Const SEP_CHAR = "/"
    Dim vntLegs As Variant
    Dim vntLeg As Variant
    
    ValidateCSLeg = True

    If Trim(CSLeg) = "" Then
        ValidateCSLeg = False
        Exit Function
    End If
    
    vntLegs = Split(CSLeg, SEP_CHAR)
    For Each vntLeg In vntLegs
        If Trim(vntLeg) = "" Then
            ValidateCSLeg = False
            Exit For
        End If
        
        rsData.Find "APC3 = '" & vntLeg & "'", , , 1
        If rsData.EOF Then
            ValidateCSLeg = False
            Exit For
        End If
    Next vntLeg
End Function

Public Function GetFullViaList(ByVal ViaString As String, rsVia As ADODB.Recordset, ByVal sAdid As String, _
Optional ByVal IncludeViaNumber As Boolean = True, Optional ByVal AdditionalInfo As String = "") As String
    Dim vntVias As Variant
    Dim vntVia As Variant
    Dim strVia As String
    Dim strVial As String
    Dim strFirstLastVia As String
    Dim strOrgDes As String
    Dim intOrgDesIndex As Integer
    Dim intFirstLastViaIndex As Integer
    Dim intViaCount As Integer
    Dim i As Integer
    Dim strRet As String
    
    If IncludeViaNumber Then
        strRet = String(5, ",")
    Else
        strRet = String(4, ",")
    End If
    If Trim(ViaString) <> "" Then
        vntVias = Split(ViaString, "/")
        intViaCount = UBound(vntVias)
        If sAdid = "A" Then
            intOrgDesIndex = 0
            intFirstLastViaIndex = 1
        Else
            intOrgDesIndex = intViaCount
            intFirstLastViaIndex = intOrgDesIndex - 1
        End If
        
        strVial = ""
        strFirstLastVia = ","
        For i = 0 To intViaCount
            vntVia = vntVias(i)
            
            strVia = ""
            rsVia.Find "APC3 = '" & vntVia & "'", , , 1
            If rsVia.EOF Then
                If i = intOrgDesIndex Then
                    strOrgDes = vntVia & ","
                Else
                    strVia = Space(120)
                End If
                If i = intFirstLastViaIndex Then
                    strFirstLastVia = vntVia & ","
                End If
            Else
                If i = intOrgDesIndex Then
                    strOrgDes = vntVia & "," & RTrim(rsVia.Fields("APC4").Value)
                Else
                    strVia = Space(1) & PadR(vntVia, 3) & PadR(RTrim(rsVia.Fields("APC4").Value), 4)
                    strVia = strVia & AdditionalInfo
                    
                    strVia = PadR(strVia, 120)
                End If
                If i = intFirstLastViaIndex Then
                    strFirstLastVia = vntVia & "," & RTrim(rsVia.Fields("APC4").Value)
                End If
            End If
            
            If i <> intOrgDesIndex Then
                strVial = strVial & strVia
            End If
        Next i
        
        strRet = strOrgDes & "," & strFirstLastVia & ","
        If IncludeViaNumber Then
            strRet = strRet & CStr(intViaCount) & ","
        End If
        strRet = strRet & strVial
    End If
    
    GetFullViaList = strRet
End Function

Public Function GetViaNumber(ByVal ViaString As String) As Integer
    If Trim(ViaString) = "" Then
        GetViaNumber = 0
    Else
        GetViaNumber = UBound(Split(ViaString, "/"))
    End If
End Function

Public Function GetViaList(ByVal ViaString As String, ViaTab As TABLib.Tab, Optional ByVal AdditionalInfo As String = "") As String
    Dim vntVias As Variant
    Dim vntVia As Variant
    Dim strVia As String
    Dim strFind As String
    
    Dim strRet As String
    
    strRet = ""
    If Trim(ViaString) <> "" Then
        vntVias = Split(ViaString, "/")
        For Each vntVia In vntVias
            strFind = ViaTab.GetLinesByColumnValue(0, vntVia, 0)
            If Not (InStr(strFind, ",") > 0 Or IsNumeric(strFind)) Then
                strVia = Space(120)
            Else
                If InStr(strFind, ",") > 0 Then
                    strFind = Split(strFind, ",")(0)
                End If
                strVia = Space(1) & PadR(vntVia, 3) & PadR(ViaTab.GetFieldValue(Val(strFind), "APC4"), 4)
                
                strVia = strVia & AdditionalInfo
                
                strVia = PadR(strVia, 120)
            End If
            
            strRet = strRet & strVia
        Next vntVia
    End If
    
    GetViaList = strRet
End Function

Public Function PadR(ByVal Text As String, ByVal Length As Integer) As String
    Dim nLen As Integer
    
    nLen = Len(Text)
    Select Case nLen
        Case Length
            PadR = Text
        Case Is < Length
            PadR = Text & Space(Length - Len(Text))
        Case Else
            PadR = Left(Text, Length)
    End Select
End Function

Public Sub ShowFloatingMessage(ByVal MessageText As String, Optional ByVal TextColor As ColorConstants = vbButtonText, _
Optional ByVal CenterOnForm As Boolean = True, Optional ByVal Left As Single = 0, Optional ByVal Top As Single = 0)
    
    Const MIN_HEIGHT = 825
    Const MIN_WIDTH = 2535
    
    Dim nHeight As Single
    Dim nWidth As Single
    Dim nLeft As Single
    Dim nTop As Single
    
    With MyMainForm
        .lblMsg.Caption = MessageText
        .lblMsg.ForeColor = TextColor
        nWidth = .lblMsg.Width + 480
        nHeight = .lblMsg.Height + 480
        
        .lblMsg.Left = 240
        
        If nWidth < MIN_WIDTH Then nWidth = MIN_WIDTH
        If nHeight < MIN_HEIGHT Then nHeight = MIN_HEIGHT
        .picMsg.Width = nWidth
        .picMsg.Height = nHeight
        If CenterOnForm Then
            nLeft = (.picWorkArea.Width - .picMsg.Width) / 2
'            nTop = .picMainButtons.Height
'            If .picFlightData.Visible Then
'                nTop = nTop + .picFlightData.Height
'            End If
'            nTop = nTop + (.picWorkArea.Height - .picMsg.Height) / 2
            nTop = (.picWorkArea.Height - .picMsg.Height) / 2
        
            .picMsg.Left = nLeft
            .picMsg.Top = nTop
        Else
            .picMsg.Left = Left
            .picMsg.Top = Top
        End If
        .shpMsg.Width = .picMsg.Width - 5
        .shpMsg.Height = .picMsg.Height - 5
        .picMsg.Visible = True
        .picMsg.ZOrder
    End With
    
    DoEvents
End Sub

Public Sub HideFloatingMessage()

    MyMainForm.picMsg.Visible = False

End Sub

Public Function GetNewUrno(Optional ByVal UrnoCount As Long = 1) As String
    oUfisServer.UrnoPoolInit UrnoCount
    oUfisServer.UrnoPoolPrepare UrnoCount

    GetNewUrno = oUfisServer.UrnoPoolGetNext
End Function

Public Function GetUTCLocalTimeDiff(Optional ByVal IniFile As String = "") As Integer
    Dim tmpTag As String
    
    oUfisServer.SetUtcTimeDiff
    If IniFile <> "" Then
        tmpTag = GetIniEntry(IniFile, "MAIN", "", "UTC_TIME_DIFF", CStr(UtcTimeDiff))
        UtcTimeDiff = Val(tmpTag)
    End If
    GetUTCLocalTimeDiff = UtcTimeDiff
End Function

Private Sub CentreFormOnScreen(FormObject As Form)
    Dim nLeft As Single
    Dim nTop As Single
    Dim nWidth As Single
    Dim nHeight As Single

    nLeft = (Screen.Width - FormObject.Width) / 2
    If nLeft < 0 Then nLeft = 0
    nTop = (Screen.Height - FormObject.Height) / 2
    If nTop < 0 Then nTop = 0

    FormObject.Move nLeft, nTop
End Sub

Public Sub ChangeFormBorderStyle(FormToChange As Form, ByVal BorderStyle As FormBorderStyleConstants)
    FormToChange.BorderStyle = BorderStyle
    FormToChange.Caption = FormToChange.Caption
End Sub

Public Sub ClearTabLines(TabObject As TABLib.Tab)
    Dim LineCount As Long
    
    LineCount = TabObject.GetLineCount
    Do While LineCount > 0
        TabObject.DeleteLine 0
        
        LineCount = TabObject.GetLineCount
    Loop
End Sub

Public Function DateTimeToDate(ByVal DateTimeValue As Date) As Date
    DateTimeToDate = DateSerial(Year(DateTimeValue), Month(DateTimeValue), Day(DateTimeValue))
End Function

Public Function NVL(ByVal Source As Variant, ByVal ResultIfNull As Variant) As Variant
    If IsNull(Source) Then
        NVL = ResultIfNull
    Else
        NVL = Source
    End If
End Function

Public Function ReadConfigurations(ByVal ConfigFileName As String) As CollectionExtended
    Dim intFile As Integer
    Dim strLine As String
    Dim strSection As String
    Dim strKey As String
    Dim strDefinition As String
    Dim intPos As Integer
    Dim ilCount As Integer
    
    On Error GoTo HandleError
    
    Set ReadConfigurations = New CollectionExtended
    
    intFile = FreeFile
    Open ConfigFileName For Input As #intFile
    Do While (Not EOF(intFile))
        Line Input #intFile, strLine
        strLine = Trim(strLine)
        If Left(strLine, 1) = "*" Or Left(strLine, 1) = "#" Then 'Comment
            'Ignore
        Else
            If Left(strLine, 1) = "[" And Right(strLine, 1) = "]" Then 'New Section
                strSection = Mid(strLine, 2, Len(strLine) - 2)
            Else 'Entry
                intPos = InStr(strLine, "=")
                If intPos > 0 Then
                    strKey = strSection & Chr(0) & RTrim(Left(strLine, intPos - 1))
                    strDefinition = EvaluateParameters(colParameters, LTrim(Mid(strLine, intPos + 1)))
                    
                    If Not ReadConfigurations.Exists(strKey) Then
                        ReadConfigurations.Add strDefinition, strKey
                    End If
                End If
            End If
        End If
    Loop
    Close #intFile
    
    Exit Function
    
HandleError:
End Function

Public Sub SetWindowSizePos(oForm As Form, ConfigCollection As CollectionExtended, ByVal sAppType As String)
    Dim strPos As String
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single

    sngWidth = CSng(GetConfigEntry(ConfigCollection, sAppType, "WINDOW_WIDTH", "18780"))
    sngHeight = CSng(GetConfigEntry(ConfigCollection, sAppType, "WINDOW_HEIGHT", "14265"))
    
    strPos = GetConfigEntry(ConfigCollection, sAppType, "WINDOW_POS", "CENTER")
    Select Case strPos
        Case "CENTER"
            sngLeft = (Screen.Width - sngWidth) / 2
            sngTop = (Screen.Height - sngHeight) / 2
        Case "LEFT"
            sngLeft = 0
            sngTop = 0
        Case "RIGHT"
            sngLeft = (Screen.Width - sngWidth)
            sngTop = 0
        Case Else
            sngLeft = CSng(GetConfigEntry(ConfigCollection, sAppType, "WINDOW_LEFT", "0"))
            sngTop = CSng(GetConfigEntry(ConfigCollection, sAppType, "WINDOW_TOP", "0"))
    End Select
    If sngLeft < 0 Then sngLeft = 0
    If sngTop < 0 Then sngTop = 0

    oForm.Move sngLeft, sngTop, sngWidth, sngHeight
End Sub

Public Function GetConfigEntry(ConfigCollection As CollectionExtended, ByVal sAppType As String, _
ByVal Key As String, ByVal DefaultVal As String) As String
    Dim strValidAppType As String
    Dim strReturn As String
    Dim strKey As String
    
    strReturn = DefaultVal
    strKey = sAppType & Chr(0) & Key
    If ConfigCollection.Exists(strKey) Then
        strReturn = ConfigCollection.Item(strKey)
    Else
        strKey = "MAIN" & Chr(0) & "VALID_COMMAND"
        If ConfigCollection.Exists(strKey) Then
            strValidAppType = ConfigCollection.Item(strKey)
        End If
        
        If InStr(strValidAppType & ",", sAppType & ",") > 0 Then
            strKey = "MAIN" & Chr(0) & Key
            If ConfigCollection.Exists(strKey) Then
                strReturn = ConfigCollection.Item(strKey)
            End If
        End If
    End If
    
    GetConfigEntry = strReturn
End Function

Public Function AddDataSource(AppType As String, colDataSources As CollectionExtended, ByVal DataSourceName As String, _
colParameters As CollectionExtended, Optional ByVal KeyColumn, Optional ByVal UseTabObject As Boolean = False, _
Optional ByVal SplitWhereClause As Boolean = False, Optional ByVal Separator As String = "|", _
Optional ByVal OverwriteDataSource As Boolean = True) As String
    Dim oUFISRec As UFISRecordset
    Dim rsResult As ADODB.Recordset
    Dim strDataTableName As String
    Dim strDataFields As String
    Dim strDataTypes As String
    Dim strDataSizes As String
    Dim strDataOrder As String
    Dim strIndexHint As String
    Dim intOrderType As Integer
    Dim strDataWhere As String
    Dim vntDataWhere As Variant
    Dim strKeyList As String
    
    strKeyList = ""
    
    'get the default config
    strDataTableName = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TABLE", DataSourceName)
    strDataFields = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_FIELDS", "")
    strDataTypes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TYPES", "")
    strDataSizes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_SIZES", "")
    strDataOrder = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER", "")
    strDataWhere = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_WHERE", "")
    strIndexHint = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_INDEX_HINT", "")
    'order type:
    '0 - use select * order by (request to server), then sort it in client
    '1 - use select * order by (request to server), but don't sort it in client
    '2 - do not use select * order by (request to server), but sort it in client instead (DEFAULT)
    intOrderType = Val(GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER_TYPE", "2"))
    
    'get app specific config
    If AppType <> "" Then
        strDataTableName = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TABLE_" & AppType, strDataTableName)
        strDataFields = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_FIELDS_" & AppType, strDataFields)
        strDataTypes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TYPES_" & AppType, strDataTypes)
        strDataSizes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_SIZES_" & AppType, strDataSizes)
        strDataOrder = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER_" & AppType, strDataOrder)
        strDataWhere = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_WHERE_" & AppType, strDataWhere)
        strIndexHint = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_INDEX_HINT_" & AppType, strIndexHint)
        intOrderType = Val(GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER_TYPE_" & AppType, CStr(intOrderType)))
    End If
    
    strDataOrder = EvaluateParameters(colParameters, strDataOrder)
    vntDataWhere = EvaluateParameters(colParameters, strDataWhere, SplitWhereClause, Separator)
    
    If Not (intOrderType >= 0 And intOrderType <= 2) Then intOrderType = 2
    
    Set oUFISRec = New UFISRecordset
    With oUFISRec
        Set .UfisServer = oUfisServer
        .ConvertSpecialChars = True
        .CheckOldVersionWhenConverting = True
        If strDataTableName = "AFTTAB" Then
            .CommandType = cmdGetFlightRecords
        Else
            .CommandType = cmdReadTable
        End If
        .TableName = strDataTableName
        .ColumnList = strDataFields
        If strDataOrder <> "" Then
            If intOrderType <> 2 Then
                .OrderByClause = strDataOrder
            End If
        End If
        If IsArray(vntDataWhere) Then
            .WhereClause = vntDataWhere
        Else
            If vntDataWhere <> "" Then
                .WhereClause = vntDataWhere
            End If
        End If
        .IndexHint = strIndexHint
        If strDataTypes <> "" Then
            .ColumnTypeList = strDataTypes
        End If
        If strDataSizes <> "" Then
            .ColumnSizeList = strDataSizes
        End If
        
        If Not IsMissing(KeyColumn) Then
            .KeyColumn = KeyColumn
            .PopulateKeyStringList = True
        End If
        
        Set rsResult = .CreateRecordset(UseTabObject)
        If strDataOrder <> "" Then
            If intOrderType <> 1 Then
                rsResult.Sort = strDataOrder
            End If
        End If
        If Not IsMissing(KeyColumn) Then
            strKeyList = .KeyStringList
        End If
        
        If colDataSources.Exists(DataSourceName) Then
            If OverwriteDataSource Then
                colDataSources.Remove DataSourceName
                
                colDataSources.Add rsResult, DataSourceName
            Else
                Call CombineDataSource(colDataSources.Item(DataSourceName), rsResult, KeyColumn)
            End If
        Else
            colDataSources.Add rsResult, DataSourceName
        End If
    End With
    
    AddDataSource = strKeyList
End Function

Public Function EvaluateParameters(colParameters As CollectionExtended, ByVal Text As String, _
Optional ByVal SplitWhereClause As Boolean = False, Optional ByVal Separator As String = "|") As Variant
    Const PARAM_BEGIN = "{?"
    Const PARAM_END = "}"
    
    Dim vntReturn As Variant
    Dim intStart As Integer
    Dim intPosBegin As Integer
    Dim intPosEnd As Integer
    Dim strParamKey As String
    Dim strParamValue As String
    Dim vntParamValues As Variant
    Dim vntParamValueItem As Variant
    Dim strArrayParamKey As String
    Dim i As Integer
    
    intStart = 1
    Do
        intPosBegin = InStr(intStart, Text, PARAM_BEGIN)
        If intPosBegin > 0 Then
            intPosEnd = InStr(intPosBegin + 2, Text, PARAM_END)
            If intPosEnd > 0 Then
                strParamKey = Mid(Text, intPosBegin + 2, intPosEnd - intPosBegin - 2)
                If colParameters.Exists(strParamKey) Then
                    strParamValue = colParameters.Item(strParamKey)
                    If SplitWhereClause Then
                        If InStr(strParamValue, Separator) > 0 Then
                            strArrayParamKey = strParamKey
                            vntParamValues = Split(strParamValue, Separator)
                            
                            strParamValue = PARAM_BEGIN & strParamKey & PARAM_END
                        Else
                            Text = Left(Text, intPosBegin - 1) & strParamValue & Mid(Text, intPosEnd + 1)
                        End If
                    Else
                        Text = Left(Text, intPosBegin - 1) & strParamValue & Mid(Text, intPosEnd + 1)
                    End If
                
                    intStart = intStart + Len(strParamValue)
                Else
                    intStart = intPosEnd + 1
                End If
            Else
                intPosBegin = 0
            End If
        End If
    Loop Until intPosBegin = 0
    
    'Evaluate array parameters
    If IsEmpty(vntParamValues) Then
        vntReturn = Text
    Else
        vntReturn = ""
        For Each vntParamValueItem In vntParamValues
            vntReturn = vntReturn & Chr(0) & Replace(Text, PARAM_BEGIN & strArrayParamKey & PARAM_END, vntParamValueItem)
        Next vntParamValueItem
        If vntReturn <> "" Then
            vntReturn = Mid(vntReturn, 2)
            vntReturn = Split(vntReturn, Chr(0))
        End If
    End If
    
    EvaluateParameters = vntReturn
End Function

Private Sub CombineDataSource(MainDataSource As ADODB.Recordset, SubDataSource As ADODB.Recordset, Optional ByVal KeyColumn)
    Dim fld As ADODB.Field
    Dim strSep As String
    
    Do While Not SubDataSource.EOF
        If IsMissing(KeyColumn) Then
            MainDataSource.AddNew
        Else
            Set fld = SubDataSource.Fields(KeyColumn)
            Select Case fld.Type
                Case adVarChar
                    strSep = "'"
                Case adDate
                    strSep = "#"
                Case Else
                    strSep = ""
            End Select
        
            MainDataSource.Find KeyColumn & " = " & strSep & fld.Value & strSep, , , 1
            If MainDataSource.EOF Then
                MainDataSource.AddNew
            End If
        End If
        For Each fld In MainDataSource.Fields
            fld.Value = SubDataSource.Fields(fld.Name).Value
        Next fld
        MainDataSource.Update
        
        SubDataSource.MoveNext
    Loop
End Sub

Public Function GetParamDateArray(ByVal StartDate As String, ByVal StartTime As String, _
ByVal EndDate As String, ByVal EndTime As String, _
ByVal RelStart As String, ByVal RelEnd As String, _
ByVal RelDateBase As Date, Optional ByVal DefaultStartDate, Optional ByVal DefaultEndDate) As Variant
    Dim dtmDate(2) As Variant

    dtmDate(2) = 0 'Flag for absolute date

    If IsMissing(DefaultStartDate) Then
        If StartDate <> "" Then
            DefaultStartDate = DateSerial(CInt(Right(StartDate, 4)), _
                                          CInt(Mid(StartDate, 4, 2)), _
                                          CInt(Left(StartDate, 2)))
        Else
            DefaultStartDate = DateSerial(Year(Date), Month(Date), Day(Date))
        End If
    End If
    If IsMissing(DefaultEndDate) Then
        DefaultEndDate = DefaultStartDate + TimeSerial(23, 59, 0)
    End If

    If StartDate = "" And StartTime = "" Then 'user didn't put start date/time
        'check for relative date
        If RelStart = "" Then 'no rel start
            dtmDate(0) = DefaultStartDate 'default: today 00:00
        Else
            dtmDate(0) = DateAdd("h", -Val(RelStart), RelDateBase) 'point to rel date
        End If
    ElseIf StartDate <> "" And StartTime = "" Then 'user put start date only
        dtmDate(0) = DateSerial(CInt(Right(StartDate, 4)), _
                                CInt(Mid(StartDate, 4, 2)), _
                                CInt(Left(StartDate, 2)))
    ElseIf StartDate = "" And StartTime <> "" Then 'user put start time only
        dtmDate(0) = DefaultStartDate + _
                     TimeSerial(CInt(Left(StartTime, 2)), _
                                CInt(Right(StartTime, 2)), 0) 'default: today + time
    Else 'user put both start date/time
        dtmDate(0) = DateSerial(CInt(Right(StartDate, 4)), _
                                CInt(Mid(StartDate, 4, 2)), _
                                CInt(Left(StartDate, 2))) + _
                     TimeSerial(CInt(Left(StartTime, 2)), _
                                CInt(Right(StartTime, 2)), 0)
    End If
    
    If EndDate = "" And EndTime = "" Then 'user didn't put end date/time
        'check for relative date
        If RelEnd = "" Then 'no rel end
            dtmDate(1) = DefaultEndDate 'default: startdate + 23:59
        Else
            dtmDate(1) = DateAdd("h", Val(RelEnd), RelDateBase) 'point to rel date
            
            dtmDate(2) = 1 'Flag for relative date
        End If
    ElseIf EndDate <> "" And EndTime = "" Then 'user put end date only
        dtmDate(1) = DateSerial(CInt(Right(EndDate, 4)), _
                                CInt(Mid(EndDate, 4, 2)), _
                                CInt(Left(EndDate, 2))) + _
                     TimeSerial(23, 59, 0)
    ElseIf EndDate = "" And EndTime <> "" Then 'user put end time only
        dtmDate(1) = DefaultStartDate + _
                     TimeSerial(CInt(Left(EndTime, 2)), _
                                CInt(Right(EndTime, 2)), 0) 'default: startdate + time
    Else 'user put both end date/time
        dtmDate(1) = DateSerial(CInt(Right(EndDate, 4)), _
                                CInt(Mid(EndDate, 4, 2)), _
                                CInt(Left(EndDate, 2))) + _
                     TimeSerial(CInt(Left(EndTime, 2)), _
                                CInt(Right(EndTime, 2)), 0)
    End If
    
    GetParamDateArray = dtmDate
End Function

Public Function GetFileName(cdgSave As CommonDialog, ByVal DefaultDir As String, ByVal FileName As String, ByVal ExportType As String) As String
    Dim strFileName As String
    Dim strTitle As String
    Dim strDefaultExt As String
    Dim strFileFilter As String
    
    On Error GoTo ErrGetFileName
    
    strTitle = "Export Report to "
    If ExportType = "excel" Then 'Excel
        strFileFilter = "Excel File"
        strDefaultExt = "xls"
    Else 'PDF
        strFileFilter = "PDF File"
        strDefaultExt = "pdf"
    End If
    strTitle = strTitle & strFileFilter
    strFileFilter = strFileFilter & "s (*." & strDefaultExt & ")|*." & strDefaultExt
    
    With cdgSave
        .CancelError = True
        .DialogTitle = strTitle
        .FileName = FileName
        .DefaultExt = strDefaultExt
        .InitDir = DefaultDir
        .Filter = "All Files (*.*)|*.*|" & strFileFilter
        .FilterIndex = 2
        .Flags = cdlOFNPathMustExist Or cdlOFNOverwritePrompt
        .ShowSave
    
        strFileName = .FileName
    End With
    
ErrGetFileName:
    GetFileName = strFileName
End Function

Public Function CreateFolder(ByVal FolderPath As String) As Boolean
    Dim aPath As Variant
    Dim i As Integer
    Dim sPath As String

    On Error GoTo ErrCreate
    
    CreateFolder = False
    
    aPath = Split(FolderPath, "\")
    sPath = aPath(0)
    For i = 1 To UBound(aPath)
        sPath = sPath & "\" & aPath(i)
        
        If Dir(sPath, vbDirectory) = "" Then
            MkDir sPath
        End If
    Next i
    
    CreateFolder = True
    
ErrCreate:
End Function

Public Function OpenFileUsingDefaultApp(oParent As Object, ByVal FullFileName As String) As Long
    Dim strDir As String
    
    strDir = Left(FullFileName, 3)
    OpenFileUsingDefaultApp = _
        ShellExecute(oParent.hwnd, vbNullString, FullFileName, vbNullString, strDir, SW_SHOWNORMAL)
End Function

Public Function IsDataInViewCriteria(rs As ADODB.Recordset, ByVal ParamVal As String, ParamArray ListOfFields() As Variant) As Boolean
    Dim FieldName As Variant
    
    IsDataInViewCriteria = True
    
    If Trim(ParamVal) <> "" Then
        IsDataInViewCriteria = False
        For Each FieldName In ListOfFields
            If Not IsDataInViewCriteria Then
                IsDataInViewCriteria = (InStr("," & ParamVal & ",", "," & rs.Fields(FieldName).Value & ",") > 0)
            End If
            If IsDataInViewCriteria Then Exit For
        Next FieldName
    End If
End Function

Public Function GetParamValList(ViewParamCollection As CollectionExtended, ParamKeyArray As Variant, ParamValArray As Variant) As String
    Dim strReturn As String
    Dim i As Integer
    
    strReturn = ""
    For i = 0 To UBound(ParamKeyArray)
        If ViewParamCollection.Item(ParamKeyArray(i)) = "1" Then
            strReturn = strReturn & "," & ParamValArray(i)
        End If
    Next i
    If strReturn <> "" Then strReturn = Mid(strReturn, 2)
    
    GetParamValList = strReturn
End Function

Public Function BuildSimpleWhereClause(ViewParamCollection As CollectionExtended, ByVal ParamKey As String, _
ByVal FieldName As String, Optional ByVal MultiValues As Boolean = False, _
Optional ByVal IsString As Boolean = True, Optional ByVal Prefix As String = "AND") As String
    Dim strParamVal As String
    Dim strReturn As String
    Dim strQuotationMark As String
    Dim strOperator As String
    
    If IsString Then strQuotationMark = "'"
    
    strParamVal = ViewParamCollection.Item(ParamKey)
    If Trim(strParamVal) <> "" Then
        strParamVal = strQuotationMark & strParamVal & strQuotationMark
        If MultiValues Then
            strParamVal = "(" & strParamVal & ")"
            If IsString Then
                strParamVal = Replace(strParamVal, ",", strQuotationMark & "," & strQuotationMark)
            End If
            strOperator = " IN "
        Else
            strOperator = " = "
        End If
        strReturn = Prefix & " " & FieldName & " " & strOperator & " " & strParamVal
    End If
    
    BuildSimpleWhereClause = strReturn
End Function

Public Function BuildExtendedWhereClause(ViewParamCollection As CollectionExtended, ByVal ParamKey As String, _
FieldNameArray As Variant, FieldSizeArray As Variant, Optional ByVal MultiValues As Boolean = False, _
Optional ByVal Prefix As String = "AND") As String
    Dim strParamVal As String
    Dim vntParamVal As Variant
    Dim strReturn As String
    Dim strOperator As String
    Dim strValList() As String
    Dim i As Integer
    Dim j As Integer
    
    strParamVal = ViewParamCollection.Item(ParamKey)
    If Trim(strParamVal) <> "" Then
        ReDim strValList(UBound(FieldNameArray))
    
        vntParamVal = Split(strParamVal, ",")
        strReturn = ""
        For i = 0 To UBound(vntParamVal)
            strParamVal = vntParamVal(i)
            For j = 0 To UBound(FieldSizeArray)
                If Len(strParamVal) <= FieldSizeArray(j) Then
                    strValList(j) = strValList(j) & "," & strParamVal
                    Exit For
                End If
            Next j
        Next i
        
        For i = 0 To UBound(strValList)
            If strValList(i) <> "" Then
                strValList(i) = Mid(strValList(i), 2)
                strValList(i) = "'" & strValList(i) & "'"
                If MultiValues Then
                    strValList(i) = "(" & Replace(strValList(i), ",", "','") & ")"
                    strOperator = " IN "
                Else
                    strOperator = " = "
                End If
            
                If strReturn <> "" Then
                    strReturn = strReturn & " OR "
                End If
                strReturn = FieldNameArray(i) & " " & strOperator & " " & strValList(i)
            End If
        Next i
        strReturn = Prefix & " (" & strReturn & ")"
    End If
    
    BuildExtendedWhereClause = strReturn
End Function

Public Function AptUTCTimeDiff(rsAirport As ADODB.Recordset, ByVal APC3 As String, ByVal DefaultTimeDiff As Long) As CollectionExtended
    Dim WinterTimeDiff As Long
    Dim SummerTimeDiff As Long
    
    WinterTimeDiff = DefaultTimeDiff
    SummerTimeDiff = DefaultTimeDiff
    If Not (rsAirport Is Nothing) Then
        rsAirport.Find "APC3 = '" & APC3 & "'", , , 1
        If Not rsAirport.EOF Then
            If Not IsNull(rsAirport.Fields("TDIW").Value) Then
                WinterTimeDiff = rsAirport.Fields("TDIW").Value
            End If
            If Not IsNull(rsAirport.Fields("TDIS").Value) Then
                SummerTimeDiff = rsAirport.Fields("TDIS").Value
            End If
        End If
    End If
    
    Set AptUTCTimeDiff = New CollectionExtended
    AptUTCTimeDiff.Add WinterTimeDiff, "WI"
    AptUTCTimeDiff.Add SummerTimeDiff, "SU"
End Function

