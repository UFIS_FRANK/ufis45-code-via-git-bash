VERSION 5.00
Begin VB.Form FlightDetails 
   Caption         =   "Flight Details"
   ClientHeight    =   1185
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13425
   LinkTopic       =   "Form1"
   ScaleHeight     =   1185
   ScaleWidth      =   13425
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picDetails 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   120
      ScaleHeight     =   855
      ScaleWidth      =   8220
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   8220
      Begin VB.OptionButton optSelFlight 
         Caption         =   "&DEP"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   555
         Width           =   555
      End
      Begin VB.OptionButton optSelFlight 
         Caption         =   "&ARR"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   240
         Width           =   555
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Routing"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3450
         TabIndex        =   26
         Top             =   0
         Width           =   1005
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   6
         Left            =   3240
         TabIndex        =   25
         Top             =   225
         Width           =   1305
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   6
         Left            =   3240
         TabIndex        =   24
         Top             =   555
         Width           =   1305
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   585
         TabIndex        =   23
         Top             =   225
         Width           =   615
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   22
         Top             =   225
         Width           =   675
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   585
         TabIndex        =   21
         Top             =   555
         Width           =   615
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   20
         Top             =   555
         Width           =   675
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Sched."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4440
         TabIndex        =   19
         Top             =   0
         Width           =   1005
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   2
         Left            =   1920
         TabIndex        =   18
         Top             =   555
         Width           =   1305
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   2
         Left            =   1920
         TabIndex        =   17
         Top             =   225
         Width           =   1305
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   3
         Left            =   4590
         TabIndex        =   16
         Top             =   555
         Width           =   720
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   3
         Left            =   4590
         TabIndex        =   15
         Top             =   225
         Width           =   720
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   4
         Left            =   5340
         TabIndex        =   14
         Top             =   225
         Width           =   720
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   4
         Left            =   5340
         TabIndex        =   13
         Top             =   555
         Width           =   720
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Estim."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5190
         TabIndex        =   12
         Top             =   0
         Width           =   1035
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Actual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   6000
         TabIndex        =   11
         Top             =   0
         Width           =   885
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   5
         Left            =   6090
         TabIndex        =   10
         Top             =   555
         Width           =   720
      End
      Begin VB.Label lblArr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   5
         Left            =   6090
         TabIndex        =   9
         Top             =   225
         Width           =   720
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   7
         Left            =   6840
         TabIndex        =   8
         Tag             =   "SHARED"
         Top             =   555
         Width           =   780
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Aircraft / Type"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   6885
         TabIndex        =   7
         Top             =   0
         Width           =   1275
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   8
         Left            =   7620
         TabIndex        =   6
         Tag             =   "SHARED"
         Top             =   555
         Width           =   570
      End
      Begin VB.Label lblDep 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   9
         Left            =   6840
         TabIndex        =   5
         Tag             =   "SHARED"
         Top             =   240
         Width           =   1350
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Flight No."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   720
         TabIndex        =   3
         Top             =   0
         Width           =   1005
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2100
         TabIndex        =   4
         Top             =   0
         Width           =   1005
      End
   End
End
Attribute VB_Name = "FlightDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public CurrentUrno As String

Public Sub RefreshFlightDetails(ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim strUrno_Arr As String
    Dim strUrno_Dep As String
    Dim blnArr As Boolean
    
    strUrno_Arr = optSelFlight(0).Tag
    strUrno_Dep = optSelFlight(1).Tag
    
    If strUrno_Arr = Urno Then
        blnArr = True
        strUrno_Dep = ""
    ElseIf strUrno_Dep = Urno Then
        blnArr = False
        strUrno_Arr = ""
    Else
        strUrno_Arr = ""
        strUrno_Dep = ""
    End If

    If (strUrno_Arr = "" And strUrno_Dep = "") Then Exit Sub
    
    If ChangedFields <> "" Then
        If Not IsRefreshNeeded(blnArr, ChangedFields) Then Exit Sub
    End If

    Call DisplayFlightDetails(blnArr)
End Sub

Private Function IsRefreshNeeded(ByVal IsArrival As Boolean, ByVal ChangedFields As String) As Boolean
    Const ARR_FIELD_LIST = "FLNO,STOA,ETAI,LAND,ORG3"
    Const DEP_FIELD_LIST = "FLNO,STOD,ETDI,AIRB,DES3"
    Const BTH_FIELD_LIST = "VIAN,VIAL,ACT3,ACT5,REGN"
    
    Dim strFields As String
    
    If IsArrival Then
        strFields = ARR_FIELD_LIST
    Else
        strFields = DEP_FIELD_LIST
    End If
    strFields = strFields & "," & BTH_FIELD_LIST
    
    IsRefreshNeeded = IsInList(ChangedFields, strFields)
End Function

Private Sub DisplayFlightDetails(ByVal IsArrival As Boolean, Optional ByVal DisplayAC As Boolean = True)
    Dim strFlno As String
    Dim intLoc As Integer
    Dim strSTD As String
    Dim strETD As String
    Dim strLAD As String
    Dim strRTG As String
    Dim strVia As String
    Dim strRouting As String
    Dim dtmSTD As Date
    Dim dtmDate As Date
    Dim lngDateDiff As Long
    Dim vntLabelArray As Variant
    Dim rsAft As ADODB.Recordset

    Set rsAft = colDataSources.Item("AFTTAB")
    
    If IsArrival Then
        strSTD = "STOA"
        strETD = "ETAI"
        strLAD = "LAND"
        strRTG = "ORG3"
        Set vntLabelArray = lblArr
    Else
        strSTD = "STOD"
        strETD = "ETDI"
        strLAD = "AIRB"
        strRTG = "DES3"
        Set vntLabelArray = lblDep
    End If
    
    strFlno = Trim(rsAft.Fields("FLNO").Value)
    intLoc = InStr(strFlno, " ")
    If intLoc > 0 Then
        vntLabelArray(0).Caption = Left(strFlno, intLoc - 1)
        vntLabelArray(1).Caption = Mid(strFlno, intLoc + 1)
    Else
        vntLabelArray(0).Caption = Left(strFlno, 3)
        vntLabelArray(1).Caption = Mid(strFlno, 4)
    End If
    
    dtmSTD = UTCDateToLocal(rsAft.Fields(strSTD).Value, UtcTimeDiff)
    vntLabelArray(2).Caption = UCase(Format(dtmSTD, "dd mmm yyyy"))

    vntLabelArray(3).Caption = Format(dtmSTD, "hh:mm")
    
    If Not IsNull(rsAft.Fields(strETD).Value) Then
        dtmDate = UTCDateToLocal(rsAft.Fields(strETD).Value, UtcTimeDiff)
        vntLabelArray(4).Caption = Format(dtmDate, "hh:mm")
        lngDateDiff = DateDiff("d", dtmSTD, dtmDate)
        Select Case lngDateDiff
            Case 0
            Case Is < 0
                vntLabelArray(4).Caption = vntLabelArray(4).Caption & CStr(lngDateDiff)
            Case Is > 0
                vntLabelArray(4).Caption = vntLabelArray(4).Caption & "+" & CStr(lngDateDiff)
        End Select
    End If
    
    If Not IsNull(rsAft.Fields(strLAD).Value) Then
        dtmDate = UTCDateToLocal(rsAft.Fields(strLAD).Value, UtcTimeDiff)
        vntLabelArray(5).Caption = Format(dtmDate, "hh:mm")
        lngDateDiff = DateDiff("d", dtmSTD, dtmDate)
        Select Case lngDateDiff
            Case 0
            Case Is < 0
                vntLabelArray(5).Caption = vntLabelArray(4).Caption & CStr(lngDateDiff)
            Case Is > 0
                vntLabelArray(5).Caption = vntLabelArray(4).Caption & "+" & CStr(lngDateDiff)
        End Select
    End If
    
    strVia = ""
    strRouting = rsAft.Fields(strRTG).Value
    If Not IsNull(rsAft.Fields("VIAN").Value) Then
        If Val(rsAft.Fields("VIAN").Value) > 0 Then  'has via
            strVia = GetViaListString(rsAft.Fields("VIAL").Value, rsAft.Fields("VIAN").Value)
        End If
    End If
    If strVia <> "" Then
        If IsArrival Then
            strRouting = strRouting & "/" & strVia
        Else
            strRouting = strVia & "/" & strRouting
        End If
    End If
    vntLabelArray(6).Caption = strRouting
    
    If DisplayAC Then
        lblDep(7).Caption = Trim(rsAft.Fields("ACT5").Value)
        lblDep(8).Caption = Trim(rsAft.Fields("ACT3").Value)
        lblDep(9).Caption = Trim(rsAft.Fields("REGN").Value)
    End If
End Sub

Public Sub ShowFlightDetails(ByVal ArrivalURNO As String, ByVal DepartureURNO As String, _
Optional ByVal Reselect As Boolean = True)
    Dim blnIsArrSelected As Boolean
    Dim i As Integer
    Dim rsAft As ADODB.Recordset

    Set rsAft = colDataSources.Item("AFTTAB")
    
    blnIsArrSelected = Not (optSelFlight(1).Value)
    
    For i = 0 To lblArr.UBound
        lblArr(i).Caption = ""
    Next i
    For i = 0 To lblDep.UBound
        lblDep(i).Caption = ""
    Next i
    optSelFlight(0).Tag = ArrivalURNO
    optSelFlight(1).Tag = DepartureURNO
    optSelFlight(0).Enabled = False
    optSelFlight(1).Enabled = False
    
    If Not (rsAft.BOF And rsAft.EOF) Then
        If ArrivalURNO <> "" Then
            rsAft.Find "URNO = " & ArrivalURNO, , adSearchForward, 1
            If Not rsAft.EOF Then
                Call DisplayFlightDetails(True)
                
                optSelFlight(0).Enabled = True
            End If
        End If
            
        If DepartureURNO <> "" Then
            rsAft.Find "URNO = " & DepartureURNO, , adSearchForward, 1
            If Not rsAft.EOF Then
                Call DisplayFlightDetails(False)
                
                optSelFlight(1).Enabled = True
            End If
        End If
    End If
    
    If Reselect Then
        If optSelFlight(0).Enabled And blnIsArrSelected Then
            optSelFlight(0).Value = True
            Call optSelFlight_Click(0)
        ElseIf optSelFlight(1).Enabled And Not blnIsArrSelected Then
            optSelFlight(1).Value = True
            Call optSelFlight_Click(1)
        Else
            If optSelFlight(0).Enabled Then
                optSelFlight(0).Value = True
                Call optSelFlight_Click(0)
            Else
                optSelFlight(1).Value = True
                Call optSelFlight_Click(1)
            End If
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim sngTop As Single
    Dim sngLeft As Single
    
    sngTop = (Me.ScaleHeight - picDetails.Height) / 2
    If sngTop < 0 Then sngTop = 0
    
    sngLeft = (Me.ScaleWidth - picDetails.Width) / 2
    If sngLeft < 0 Then sngLeft = 0
    
    picDetails.Move sngLeft, sngTop
End Sub

Private Sub optSelFlight_Click(Index As Integer)
    Dim i As Integer
    
    optSelFlight(0).BackColor = vbButtonFace
    optSelFlight(1).BackColor = vbButtonFace
    For i = 0 To lblArr.UBound
        lblArr(i).BackColor = NormalGray
    Next i
    For i = 0 To 6
        lblDep(i).BackColor = NormalGray
    Next i
            
    If optSelFlight(Index).Value Then
        If Index = 0 Then 'Arr
            optSelFlight(Index).BackColor = vbYellow
            
            For i = 0 To lblArr.UBound
                lblArr(i).BackColor = vbWindowBackground
            Next i
        Else 'Dep
            optSelFlight(Index).BackColor = vbGreen
            
            For i = 0 To 6
                lblDep(i).BackColor = vbWindowBackground
            Next i
        End If
        
        CurrentUrno = optSelFlight(Index).Tag
        MyMainForm.LoadData CurrentUrno
    End If
End Sub

