VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form CedaSpooler 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "AODB Transaction Spooler"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4170
   Icon            =   "CedaSpooler.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   4170
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   28
      Top             =   3075
      Width           =   4170
      _ExtentX        =   7355
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   7303
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   240
      Left            =   60
      TabIndex        =   17
      Top             =   3180
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   423
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Import Transaction Statistic"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Left            =   30
      TabIndex        =   4
      Top             =   30
      Width           =   4095
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   1275
         Left            =   120
         TabIndex        =   21
         Top             =   1650
         Width           =   3885
         Begin VB.CheckBox chkWork 
            Caption         =   "Yes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   810
            Width           =   855
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "No"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   2460
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   810
            Width           =   855
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Details"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   1590
            Style           =   1  'Graphical
            TabIndex        =   25
            Top             =   810
            Width           =   855
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   480
            Left            =   90
            Picture         =   "CedaSpooler.frx":014A
            ScaleHeight     =   480
            ScaleWidth      =   480
            TabIndex        =   23
            Top             =   180
            Width           =   480
         End
         Begin VB.Label Label1 
            Caption         =   "Do you want to proceed ?"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   5
            Left            =   930
            TabIndex        =   24
            Top             =   330
            Width           =   2565
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1185
            Index           =   9
            Left            =   0
            TabIndex        =   22
            Top             =   90
            Width           =   3855
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   795
         Left            =   120
         TabIndex        =   18
         Top             =   270
         Width           =   3885
         Begin VB.Label Label1 
            Caption         =   "The invoked import procedure creates the following amount of AODB transactions:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   8
            Left            =   90
            TabIndex        =   20
            Top             =   210
            Width           =   3675
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   675
            Index           =   7
            Left            =   0
            TabIndex        =   19
            Top             =   90
            Width           =   3855
         End
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Split"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   3300
         TabIndex        =   14
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   2520
         TabIndex        =   12
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Update"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   1740
         TabIndex        =   10
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Insert"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   960
         TabIndex        =   8
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Join"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   180
         TabIndex        =   6
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label CmdCounter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FF00FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   3240
         TabIndex        =   13
         Tag             =   "SPR"
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label CmdCounter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   2460
         TabIndex        =   11
         Tag             =   "DFR"
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label CmdCounter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1680
         TabIndex        =   9
         Tag             =   "UFR"
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label CmdCounter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   7
         Tag             =   "IFR"
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label CmdCounter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Tag             =   "JOF"
         Top             =   1320
         Width           =   735
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Step"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   600
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   7740
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Exclude"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   1470
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   7740
      Width           =   855
   End
   Begin TABLib.TAB CedaOut 
      Height          =   1545
      Index           =   1
      Left            =   60
      TabIndex        =   0
      Top             =   3570
      Width           =   9495
      _Version        =   65536
      _ExtentX        =   16748
      _ExtentY        =   2725
      _StockProps     =   64
   End
   Begin TABLib.TAB CedaOut 
      Height          =   2625
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   5130
      Width           =   9495
      _Version        =   65536
      _ExtentX        =   16748
      _ExtentY        =   4630
      _StockProps     =   64
   End
   Begin TABLib.TAB HelperTab 
      Height          =   2475
      Index           =   0
      Left            =   2760
      TabIndex        =   15
      Top             =   7500
      Visible         =   0   'False
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin TABLib.TAB HelperTab 
      Height          =   2475
      Index           =   1
      Left            =   3630
      TabIndex        =   16
      Top             =   7830
      Visible         =   0   'False
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   4366
      _StockProps     =   64
   End
End
Attribute VB_Name = "CedaSpooler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CurNoopBuffer As String

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0  'No (Close)
                chkWork(4).Value = 0
                ResetNoopLineStatus
                chkWork(Index).Value = 0
            Case 1  'Transmit
                chkWork(4).Value = 0
                SendOutBufferToCeda True
                If (StartedAsCompareTool) And (NoopFlights.chkSpool(3).Value = 0) Then
                    NoopFlights.CheckFullImport "MANUALLY"
                End If
                chkWork(Index).Value = 0
            Case 4  'Details
                ShowCedaOutDetails
                'NoopFlights.chkWork(13).Value = 1
            Case Else
                chkWork(Index).Value = 0
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        chkWork(Index).Refresh
        Select Case Index
            Case 0  'No (Close)
            Case 1  'Transmit
            Case 4
                RestoreNoopMaster
            Case Else
        End Select
    End If

End Sub

Private Sub CmdCounter_DblClick(Index As Integer)
    Dim CurTag As String
    
    If chkWork(4).Value = 1 Then
        CurTag = CmdCounter(Index).Tag
        ShowCedaCmdDetails CurTag
    End If
    
End Sub

Private Sub CmdCounter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If chkWork(4).Value = 1 Then
        If Val(CmdCounter(Index).Caption) > 0 Then CmdCounter(Index).MousePointer = 14 Else CmdCounter(Index).MousePointer = 0
    End If
End Sub

Private Sub Form_Load()
    CedaOut(0).ResetContent
    CedaOut(0).FontName = "Courier New"
    CedaOut(0).HeaderString = "GrpIdx,TWS,FKEY,CMD,SQL KEY,SQL FIELDS,SQL DATA,Remarks" & Space(100)
    CedaOut(0).HeaderLengthString = "100,100,100,100,100,100,100,120"
    CedaOut(0).ColumnWidthString = "10,32,40,4,64,1000,2000,2000"
    CedaOut(0).HeaderAlignmentString = "L,L,L,L,L,L,L,L"
    CedaOut(0).SetFieldSeparator Chr(30)
    CedaOut(0).ShowHorzScroller True
    CedaOut(0).ShowVertScroller True
    CedaOut(0).AutoSizeByHeader = True
    CedaOut(0).AutoSizeColumns
    
    'CedaOut(1).Visible = False
    CedaOut(1).ResetContent
    CedaOut(1).FontName = "Courier New"
    CedaOut(1).HeaderString = "FKEY"
    CedaOut(1).HeaderLengthString = "200"
    CedaOut(1).ColumnWidthString = "22"
    CedaOut(1).HeaderAlignmentString = "L"
    CedaOut(1).ShowVertScroller True
    CedaOut(1).AutoSizeByHeader = True
    CedaOut(1).AutoSizeColumns
    Me.Top = NoopFlights.Top + ((NoopFlights.Height - Me.Height) / 2)
    Me.Left = NoopFlights.Left + ((NoopFlights.Width - Me.Width) / 2)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        chkWork(0).Value = 1
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewHeight As Long
    NewHeight = 5445
    'CedaOut(0).Top = CedaOut(1).Top
    NewHeight = Me.ScaleHeight - CedaOut(0).Top - 60
    If NewHeight > 500 Then CedaOut(0).Height = NewHeight
    NewWidth = Me.ScaleWidth - (CedaOut(0).Left * 2)
    If NewWidth > 500 Then CedaOut(0).Width = NewWidth
End Sub

Public Sub SendOutBufferToCeda(UpdateLists As Boolean)
    Dim OutLineNo As Long
    Dim OutMaxLine As Long
    Dim RefLineNo As Long
    Dim MaxPackets As Long
    Dim PacketNbr As Integer
    Dim TotalPackets As Integer
    Dim OutLineCount As Long
    Dim FkeyLines As Long
    Dim OutGrpKey As String
    Dim CurGrpKey As String
    Dim CurFkey As String
    Dim CurDate As String
    Dim MinDate As String
    Dim MaxDate As String
    Dim OutRecord As String
    Dim OutBuffer As String
    Dim SqlKey As String
    Dim TwsKey As String
    Dim TotPacketKey As String
    Dim CurPacketKey As String
    Dim OutFkeyList As String
    NoopFlights.SetProgressBar 0, 1, 10, "Sorting Transactions ..."
    If CedaIsConnected Then
        UfisServer.BcSpool True
    End If
    MaxPackets = 500
    CedaOut(0).Sort 0, True, True
    CedaOut(0).AutoSizeColumns
    CedaOut(0).Refresh
    CurGrpKey = ""
    OutBuffer = ""
    OutLineCount = 0
    NoopFlights.SetProgressBar 0, 3, -1, ""
    OutMaxLine = CedaSpooler.CedaOut(0).GetLineCount - 1
    TotalPackets = Int(OutMaxLine / MaxPackets) + 1
    PacketNbr = 0
    MinDate = "99999999"
    MaxDate = "00000000"
    TotPacketKey = UfisServer.UrnoPoolGetNext
    NoopFlights.SetProgressBar 0, 1, OutMaxLine, "Sending Transactions to AODB ..."
    For OutLineNo = 0 To OutMaxLine
        OutRecord = CedaOut(0).GetLineValues(OutLineNo)
        OutGrpKey = GetItem(OutRecord, 1, Chr(30))
        OutGrpKey = GetItem(OutGrpKey, 1, ",")
        If (OutGrpKey <> CurGrpKey) And (OutLineCount >= MaxPackets) Then
            NoopFlights.SetProgressBar 0, 2, OutLineNo, ""
            OutBuffer = Left(OutBuffer, Len(OutBuffer) - 1)
            'MsgBox "Output Stopped"
            PacketNbr = PacketNbr + 1
            TwsKey = ".NBC." & TotPacketKey & "," & CStr(PacketNbr) & "," & CStr(TotalPackets)
            UfisServer.TwsCode.Text = TwsKey
            CurPacketKey = UfisServer.UrnoPoolGetNext
            SqlKey = "CMDS," & CurPacketKey & "," & CStr(OutLineCount) & "," & CStr(OutLineNo + 1) & "," & CStr(OutMaxLine + 1)
            OutFkeyList = DataSystemType & "," & MinDate & "," & MaxDate & "," & MainPacketMinDate & "," & MainPacketMaxDate
            UfisServer.CallCeda CedaDataAnswer, "IMPM", "AFTTAB", OutFkeyList, OutBuffer, SqlKey, "", 0, False, False
            OutBuffer = ""
            OutLineCount = 0
            MinDate = "99999999"
            MaxDate = "00000000"
        End If
        CurFkey = GetItem(OutRecord, 3, Chr(30))
        If Left(CurFkey, 1) = "{" Then
            CurFkey = Mid(CurFkey, 4)
            CurDate = Mid(GetItem(CurFkey, 1, ","), 10, 8)
            If CurDate < MinDate Then MinDate = CurDate
            If CurDate > MaxDate Then MaxDate = CurDate
            CurDate = Mid(GetItem(CurFkey, 2, ","), 10, 8)
        Else
            CurDate = Mid(CurFkey, 10, 8)
        End If
        If CurDate < MinDate Then MinDate = CurDate
        If CurDate > MaxDate Then MaxDate = CurDate
        OutBuffer = OutBuffer & OutRecord & vbLf
        OutLineCount = OutLineCount + 1
        If UpdateLists Then UpdateCompareLists OutLineNo
        CurGrpKey = OutGrpKey
        If CedaIsConnected Then
            UfisServer.GetNextBcFromSpool True
        End If
    Next
    If OutLineCount > 0 Then
        OutBuffer = Left(OutBuffer, Len(OutBuffer) - 1)
        'MsgBox "Output Stopped"
        TwsKey = ".NBC." & TotPacketKey & "," & CStr(TotalPackets) & "," & CStr(TotalPackets)
        UfisServer.TwsCode.Text = TwsKey
        CurPacketKey = UfisServer.UrnoPoolGetNext
        SqlKey = "CMDS," & CurPacketKey & "," & CStr(OutLineCount) & "," & CStr(OutMaxLine + 1) & "," & CStr(OutMaxLine + 1)
        OutFkeyList = DataSystemType & "," & MinDate & "," & MaxDate & "," & MainPacketMinDate & "," & MainPacketMaxDate
        UfisServer.CallCeda CedaDataAnswer, "IMPM", "AFTTAB", OutFkeyList, OutBuffer, SqlKey, "", 0, False, False
        OutBuffer = ""
        OutLineCount = 0
    End If
    CedaOut(0).ResetContent
    CedaOut(0).Refresh
    NoopFlights.SetProgressBar 0, 3, -1, ""
    If UpdateLists Then
        NoopFlights.NoopList(0).Refresh
        NoopFlights.NoopList(1).Refresh
        NoopFlights.NoopList(2).Refresh
    End If
    If CedaIsConnected Then
        UfisServer.BcSpool False
    End If
    Me.Hide
End Sub
Private Sub StoreFkeyList(OutLineNo As Long)
    Dim GrpLineIdx As String
    Dim CurFkey As String
    Dim RefLineNo As Long
    GrpLineIdx = CedaOut(0).GetColumnValue(OutLineNo, 1) '& ",-1"
    RefLineNo = Val(GetItem(GrpLineIdx, 1, ","))
    If RefLineNo >= 0 Then
        CurFkey = NoopFlights.NoopList(0).GetColumnValue(RefLineNo, NoopFlights.FkeyCol)
        If CurFkey = "" Then CurFkey = NoopFlights.NoopList(1).GetColumnValue(RefLineNo, NoopFlights.FkeyCol)
        CurFkey = TurnImpFkeyToAftFkey(CurFkey)
        CedaOut(1).InsertTextLine CurFkey, False
    End If
End Sub
Private Sub UpdateCompareLists(OutLineNo As Long)
    Dim GrpLineIdx As String
    Dim CurCmd As String
    Dim CurLineNo As Long
    Dim DepLineNo As Long
    GrpLineIdx = CedaOut(0).GetColumnValue(OutLineNo, 1) & ",-1"
    CurLineNo = Val(GetItem(GrpLineIdx, 1, ","))
    DepLineNo = Val(GetItem(GrpLineIdx, 2, ","))
    CurCmd = CedaOut(0).GetColumnValue(OutLineNo, 2)
    Select Case CurCmd
        Case "UFR"
            HandleUfr CurLineNo
        Case "IFR"
            HandleIfr CurLineNo, CedaOut(0).GetColumnValue(OutLineNo, 4), CedaOut(0).GetColumnValue(OutLineNo, 5)
        Case "DFR"
        Case "SPR"
        Case "JOF"
            HandleJof CurLineNo, DepLineNo
        Case Else
    End Select
End Sub

Private Sub HandleUfr(LineNo As Long)
    Dim UpdColList As String
    Dim ColNbr As String
    Dim ColNo As Long
    Dim ColIdx As Long
    Dim ColData As String
    If LineNo >= 0 Then
        UpdColList = NoopFlights.NoopList(0).GetLineTagKeyItem(LineNo, "", "{=UPD=}", "{=")
        ColIdx = 0
        ColNbr = GetRealItem(UpdColList, ColIdx, ",")
        While ColNbr <> ""
            ColNo = Val(ColNbr)
            ColData = NoopFlights.NoopList(0).GetColumnValue(LineNo, ColNo)
            NoopFlights.NoopList(1).SetColumnValue LineNo, ColNo, ColData
            ColIdx = ColIdx + 1
            ColNbr = GetRealItem(UpdColList, ColIdx, ",")
        Wend
        NoopFlights.NoopList(0).SetLineColor LineNo, NoopFlights.CliFkeyCnt(6).ForeColor, NoopFlights.CliFkeyCnt(6).BackColor
        NoopFlights.NoopList(1).SetLineColor LineNo, NoopFlights.SrvFkeyCnt(6).ForeColor, NoopFlights.SrvFkeyCnt(6).BackColor
        SetLineProcessed LineNo
    End If
End Sub

Private Sub HandleIfr(LineNo As Long, FieldList As String, DataList As String)
    Dim NewUrno As String
    Dim ImpData As String
    If LineNo >= 0 Then
        ImpData = NoopFlights.NoopList(0).GetLineValues(LineNo)
        NoopFlights.NoopList(1).UpdateTextLine LineNo, ImpData, False
        NewUrno = GetFieldValue("URNO", DataList, FieldList)
        NoopFlights.NoopList(1).SetColumnValue LineNo, NoopFlights.UrnoCol, NewUrno
        NoopFlights.NoopList(1).SetColumnValue LineNo, NoopFlights.RkeyCol, NewUrno
        SetLineProcessed LineNo
    End If
End Sub

Private Sub HandleJof(ArrLine As Long, DepLine As Long)
    'Dim tmpAdid As String
    Dim SrvArrUrno As String
    'Dim ArrLine As Long
    'Dim DepLine As Long
    If (ArrLine >= 0) And (DepLine >= 0) Then
        'tmpAdid = NoopFlights.NoopList(1).GetColumnValue(LineNo1, NoopFlights.AdidCol)
        'Select Case tmpAdid
        '    Case "A"
        '        ArrLine = LineNo1
        '        DepLine = LineNo2
        '    Case "D"
        '        ArrLine = LineNo2
        '        DepLine = LineNo1
        '    Case Else
        '    ArrLine = -1
        'End Select
        'If ArrLine >= 0 Then
            SrvArrUrno = NoopFlights.NoopList(1).GetColumnValue(ArrLine, NoopFlights.UrnoCol)
            'NoopFlights.NoopList(1).SetColumnValue ArrLine, NoopFlights.RkeyCol, SrvArrUrno
            NoopFlights.NoopList(1).SetColumnValue DepLine, NoopFlights.RkeyCol, SrvArrUrno
            NoopFlights.NoopList(0).ResetCellDecoration ArrLine, 0
            NoopFlights.NoopList(0).ResetCellDecoration DepLine, 0
            NoopFlights.NoopList(1).ResetCellDecoration ArrLine, 0
            NoopFlights.NoopList(1).ResetCellDecoration DepLine, 0
            SetLineProcessed ArrLine
            SetLineProcessed DepLine
        'End If
    End If
End Sub

Private Sub SetLineProcessed(LineNo As Long)
    Dim LineIdx As String
    Dim LineList As String
    Dim tmpLineNo As Long
    Dim tmpStatus As Long
    NoopFlights.NoopList(0).SetColumnValue LineNo, 0, "P"
    NoopFlights.NoopList(1).SetColumnValue LineNo, 0, "P"
    NoopFlights.NoopList(0).SetColumnValue LineNo, NoopFlights.DoneCol, "1"
    NoopFlights.NoopList(1).SetColumnValue LineNo, NoopFlights.DoneCol, "1"
    tmpStatus = NoopFlights.NoopList(0).GetLineStatusValue(LineNo)
    NoopFlights.NoopList(0).SetLineStatusValue LineNo, tmpStatus Or NoopFlights.IsImported
    tmpStatus = NoopFlights.NoopList(1).GetLineStatusValue(LineNo)
    NoopFlights.NoopList(1).SetLineStatusValue LineNo, tmpStatus Or NoopFlights.IsImported
    NoopFlights.NoopList(0).SetLineColor LineNo, NoopFlights.CliFkeyCnt(6).ForeColor, NoopFlights.CliFkeyCnt(6).BackColor
    NoopFlights.NoopList(1).SetLineColor LineNo, NoopFlights.SrvFkeyCnt(6).ForeColor, NoopFlights.SrvFkeyCnt(6).BackColor
    LineIdx = Trim(Str(LineNo))
    LineList = NoopFlights.NoopList(2).GetLinesByIndexValue("LineIdx", LineIdx, 0)
    If LineList <> "" Then
        tmpLineNo = Val(LineList)
        NoopFlights.NoopList(2).SetLineColor tmpLineNo, NoopFlights.CliFkeyCnt(6).ForeColor, NoopFlights.CliFkeyCnt(6).BackColor
    End If
End Sub

Public Sub PrepareUserInfo()
    Dim i As Integer
    Dim CurCmd As String
    Dim LineList As String
    Me.Top = NoopFlights.Top + ((NoopFlights.Height - Me.Height) / 2)
    Me.Left = NoopFlights.Left + ((NoopFlights.Width - Me.Width) / 2)
    CedaOut(0).Sort 0, True, True
    CedaOut(0).AutoSizeColumns
    CedaOut(0).Refresh
    CedaOut(0).IndexCreate "CedaCmd", 3
    CedaOut(0).SetInternalLineBuffer True
    For i = 0 To 4
        CurCmd = CmdCounter(i).Tag
        CmdCounter(i).Caption = CedaOut(0).GetLinesByIndexValue("CedaCmd", CurCmd, 0)
        If Val(CmdCounter(i).Caption) > 0 Then
            Label1(i).Enabled = True
        Else
            Label1(i).Enabled = False
        End If
    Next
    CedaOut(0).SetInternalLineBuffer False
    CedaOut(0).AutoSizeColumns
End Sub
Private Sub ResetNoopLineStatus()
    Dim CurLine As Long
    Dim TwsIdx As String
    Dim LineNbr As String
    Dim LineNo As Long
    Dim MaxLine As Long
    MaxLine = CedaSpooler.CedaOut(0).GetLineCount - 1
    NoopFlights.SetProgressBar 0, 1, MaxLine, "Restoring Transaction List ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 200 = 0 Then NoopFlights.SetProgressBar 0, 2, CurLine, ""
        TwsIdx = CedaOut(0).GetColumnValue(CurLine, 1)
        LineNbr = GetItem(TwsIdx, 1, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            NoopFlights.NoopList(0).SetLineStatusValue LineNo, 0
            NoopFlights.NoopList(1).SetLineStatusValue LineNo, 0
        End If
        LineNbr = GetItem(TwsIdx, 2, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            NoopFlights.NoopList(0).SetLineStatusValue LineNo, 0
            NoopFlights.NoopList(1).SetLineStatusValue LineNo, 0
        End If
    Next
    CedaOut(0).ResetContent
    CedaOut(0).Refresh
    CedaOut(1).ResetContent
    CedaOut(1).Refresh
    NoopFlights.SetProgressBar 0, 3, -1, ""
    Me.Hide
End Sub

Private Sub ShowCedaOutDetails()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNbr As String
    Dim LineNo As Long
    Dim tmpNoopLine As String
    Dim TwsIdx As String
    MaxLine = NoopFlights.NoopList(2).GetLineCount - 1
    CurNoopBuffer = NoopFlights.NoopList(2).GetBuffer(0, MaxLine, vbLf)
    NoopFlights.NoopList(2).ResetContent
    NoopFlights.NoopList(2).Refresh
    MaxLine = CedaOut(0).GetLineCount - 1
    NoopFlights.SetProgressBar 0, 1, MaxLine, "Creating Detail View ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 200 = 0 Then NoopFlights.SetProgressBar 0, 2, CurLine, ""
        TwsIdx = CedaOut(0).GetColumnValue(CurLine, 1)
        LineNbr = GetItem(TwsIdx, 1, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            tmpNoopLine = NoopFlights.NoopList(0).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
            If GetItem(tmpNoopLine, 1, ",") = "" Then
                tmpNoopLine = NoopFlights.NoopList(1).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
                If GetItem(tmpNoopLine, 1, ",") = "" Then tmpNoopLine = ",,,,-1,"
            End If
            NoopFlights.NoopList(2).InsertTextLine tmpNoopLine, False
        End If
        LineNbr = GetItem(TwsIdx, 2, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            tmpNoopLine = NoopFlights.NoopList(0).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
            If GetItem(tmpNoopLine, 1, ",") = "" Then
                tmpNoopLine = NoopFlights.NoopList(1).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
                If GetItem(tmpNoopLine, 1, ",") = "" Then tmpNoopLine = ",,,,-1,"
            End If
            NoopFlights.NoopList(2).InsertTextLine tmpNoopLine, False
        End If
    Next
    NoopFlights.SetProgressBar 0, 3, -1, ""
    NoopFlights.AdjustScrollMaster -1, -1, "", True
End Sub

Private Sub ShowCedaCmdDetails(CurCedaCmd As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNbr As String
    Dim LineNo As Long
    Dim tmpNoopLine As String
    Dim TwsIdx As String
    NoopFlights.NoopList(2).ResetContent
    NoopFlights.NoopList(2).Refresh
    CedaOut(0).SetInternalLineBuffer True
    MaxLine = Val(CedaOut(0).GetLinesByIndexValue("CedaCmd", CurCedaCmd, 0)) - 1
    NoopFlights.SetProgressBar 0, 1, MaxLine, "Creating Detail View ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 200 = 0 Then NoopFlights.SetProgressBar 0, 2, CurLine, ""
        LineNo = CedaOut(0).GetNextResultLine
        TwsIdx = CedaOut(0).GetColumnValue(LineNo, 1)
        LineNbr = GetItem(TwsIdx, 1, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            tmpNoopLine = NoopFlights.NoopList(0).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
            If GetItem(tmpNoopLine, 1, ",") = "" Then
                tmpNoopLine = NoopFlights.NoopList(1).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
                If GetItem(tmpNoopLine, 1, ",") = "" Then tmpNoopLine = ",,,,-1,"
            End If
            NoopFlights.NoopList(2).InsertTextLine tmpNoopLine, False
        End If
        LineNbr = GetItem(TwsIdx, 2, ",")
        If LineNbr <> "" Then
            LineNo = Val(LineNbr)
            tmpNoopLine = NoopFlights.NoopList(0).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
            If GetItem(tmpNoopLine, 1, ",") = "" Then
                tmpNoopLine = NoopFlights.NoopList(1).GetColumnValues(LineNo, NoopFlights.RotaSelFields) & "," & Str(LineNo) & ","
                If GetItem(tmpNoopLine, 1, ",") = "" Then tmpNoopLine = ",,,,-1,"
            End If
            NoopFlights.NoopList(2).InsertTextLine tmpNoopLine, False
        End If
    Next
    CedaOut(0).SetInternalLineBuffer False
    NoopFlights.SetProgressBar 0, 3, -1, ""
    NoopFlights.AdjustScrollMaster -1, -1, "", True
End Sub

Private Sub RestoreNoopMaster()
    NoopFlights.NoopList(2).ResetContent
    NoopFlights.NoopList(2).InsertBuffer CurNoopBuffer, vbLf
    NoopFlights.NoopList(2).Refresh
    CurNoopBuffer = ""
    NoopFlights.AdjustScrollMaster -2, -1, "", True
End Sub

