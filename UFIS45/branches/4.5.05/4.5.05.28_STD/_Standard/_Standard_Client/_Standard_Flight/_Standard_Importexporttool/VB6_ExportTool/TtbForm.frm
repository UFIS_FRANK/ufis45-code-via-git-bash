VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form TtbForm 
   Caption         =   "TTB Export"
   ClientHeight    =   9405
   ClientLeft      =   60
   ClientTop       =   1590
   ClientWidth     =   14985
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9405
   ScaleWidth      =   14985
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox RejectCountValue 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2760
      TabIndex        =   27
      Top             =   8950
      Width           =   615
   End
   Begin VB.TextBox ReadCountValue 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      TabIndex        =   26
      Top             =   8950
      Width           =   615
   End
   Begin VB.CommandButton CloseBtn 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13920
      TabIndex        =   14
      Top             =   240
      Width           =   975
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1155
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   14955
      Begin VB.ComboBox cbSeason 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   720
         Sorted          =   -1  'True
         TabIndex        =   0
         Top             =   240
         Width           =   960
      End
      Begin VB.TextBox ValidFrom 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox ValidTo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         TabIndex        =   2
         Top             =   720
         Width           =   975
      End
      Begin VB.ComboBox cbFiles 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8520
         Sorted          =   -1  'True
         TabIndex        =   7
         Top             =   720
         Width           =   3375
      End
      Begin VB.TextBox OutFileName 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   8520
         TabIndex        =   12
         Top             =   240
         Width           =   3375
      End
      Begin VB.CommandButton FileNameBtn 
         Caption         =   "File Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7440
         TabIndex        =   11
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton ReadBtn 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12000
         TabIndex        =   8
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton StoreBtn 
         Caption         =   "Store"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12000
         TabIndex        =   13
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton CreateBtn 
         Caption         =   "Create"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12960
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton DeleteBtn 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   13920
         TabIndex        =   10
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox AirpFilter 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         TabIndex        =   3
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox AirlFilter 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         TabIndex        =   5
         Top             =   720
         Width           =   1935
      End
      Begin VB.CommandButton AirpOptBtn 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7080
         TabIndex        =   4
         Top             =   300
         Width           =   255
      End
      Begin VB.CommandButton AirlOptBtn 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7080
         TabIndex        =   6
         Top             =   775
         Width           =   255
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Season"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   21
         Top             =   300
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Valid from"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1680
         TabIndex        =   20
         Top             =   300
         Width           =   1000
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "to"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1680
         TabIndex        =   19
         Top             =   780
         Width           =   1000
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Files"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7440
         TabIndex        =   18
         Top             =   780
         Width           =   855
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Airport Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   17
         Top             =   300
         Width           =   1140
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Airline Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   16
         Top             =   780
         Width           =   1140
      End
   End
   Begin TABLib.TAB TAB1 
      Height          =   7215
      Left            =   0
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   1560
      Width           =   14925
      _Version        =   65536
      _ExtentX        =   26326
      _ExtentY        =   12726
      _StockProps     =   64
   End
   Begin VB.Label RejectCountLabel 
      Caption         =   "Recs rej :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1920
      TabIndex        =   25
      Top             =   9000
      Width           =   855
   End
   Begin VB.Label ReadCountLabel 
      Caption         =   "Recs read :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      TabIndex        =   24
      Top             =   9000
      Width           =   975
   End
   Begin VB.Label HeaderName 
      Caption         =   "TTB Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   22
      Top             =   1200
      Width           =   2055
   End
End
Attribute VB_Name = "TtbForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim ilRC As Integer
    Dim seaTab As String
    Dim ttbFiles As String
    Dim vpfr As String
    Dim vpto As String
    Dim nextTabLineNo As Long
    Dim prevBufLine As String
    Dim ignoreLine As Boolean
    Dim readCount As Long
    Dim rejectCount As Long
    Dim emptyFreq As Boolean

Private Sub Form_Load()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim NoOfLines As Integer
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    TAB1.LineHeight = 18
    TAB1.FontName = "Courier New"
    TAB1.HeaderFontSize = 17
    TAB1.FontSize = 17
    TAB1.SetTabFontBold True
    TAB1.EnableHeaderSizing True
    TAB1.ShowHorzScroller True
    TAB1.EnableInlineEdit False
    TAB1.InplaceEditUpperCase = False
    TAB1.EmptyAreaRightColor = 12632256
    TAB1.Visible = False

    TtbForm.Caption = MainForm.GetText("TtbForm", "TtbForm", 1)
    FileNameBtn.Caption = MainForm.GetText("TtbForm", "FileNameBtn", 1)
    ReadBtn.Caption = MainForm.GetText("TtbForm", "ReadBtn", 1)
    StoreBtn.Caption = MainForm.GetText("TtbForm", "StoreBtn", 1)
    CreateBtn.Caption = MainForm.GetText("TtbForm", "CreateBtn", 1)
    DeleteBtn.Caption = MainForm.GetText("TtbForm", "DeleteBtn", 1)
    CloseBtn.Caption = MainForm.GetText("TtbForm", "CloseBtn", 1)
    Label1.Caption = MainForm.GetText("TtbForm", "Label1", 1)
    Label2.Caption = MainForm.GetText("TtbForm", "Label2", 1)
    Label3.Caption = MainForm.GetText("TtbForm", "Label3", 1)
    Label4.Caption = MainForm.GetText("TtbForm", "Label4", 1)
    Label5.Caption = MainForm.GetText("TtbForm", "Label5", 1)
    Label6.Caption = MainForm.GetText("TtbForm", "Label6", 1)
    ReadCountLabel.Caption = MainForm.GetText("TtbForm", "Label7", 1)
    RejectCountLabel.Caption = MainForm.GetText("TtbForm", "Label8", 1)
    
    cbSeason.Clear
    seaTab = ""
    TableName = "SEA" & MainForm.tableExt
    fieldNames = "SEAS,VPFR,VPTO"
    tabSelection = "ORDER BY VPFR"
    ilRC = MainForm.Ufis.CallServer("RT", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbSeason.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            seaTab = seaTab & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    
    cbFiles.Clear
    ttbFiles = ""
    TableName = "TTB"
    fieldNames = "FILENAMES"
    tabSelection = cbSeason.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            ttbFiles = ttbFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    
    HeaderName.Caption = MainForm.GetText("TtbForm", "Header1", 1)
    Screen.MousePointer = vbArrow
End Sub

Private Sub CloseBtn_Click()
    TtbForm.Visible = False
    Unload TtbForm
End Sub

Private Sub AirpFilter_keyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub AirlFilter_keyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub AirpOptBtn_Click()

    If AirpOptBtn.Caption = "+" Then
        AirpOptBtn.Caption = "-"
    Else
        AirpOptBtn.Caption = "+"
    End If
    
End Sub

Private Sub AirlOptBtn_Click()

    If AirlOptBtn.Caption = "+" Then
        AirlOptBtn.Caption = "-"
    Else
        AirlOptBtn.Caption = "+"
    End If
    
End Sub

Private Sub cbFiles_Click()

    If cbFiles.Text <> "" Then
        ReadBtn_Click
    End If
End Sub

Private Sub FileNameBtn_Click()

    OutFileName.Text = ""
    MainForm.CommonDialog1.DialogTitle = MainForm.GetText("TtbForm", "DialogTitle1", 1)
    MainForm.CommonDialog1.Filter = MainForm.GetText("TtbForm", "DialogFilter1", 1)
    MainForm.CommonDialog1.FilterIndex = 1
    MainForm.CommonDialog1.ShowOpen
    OutFileName.Text = MainForm.CommonDialog1.FileName
    If OutFileName.Text <> "" Then
        StoreBtn_Click
    End If
    
End Sub

Private Sub cbSeason_Click()
    Dim selSeason As String
    Dim NoOfLines As Integer
    Dim i As Integer
    Dim il As Long
    Dim curLine As String
    Dim tmpSeason As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    
    selSeason = Trim(cbSeason.Text)
    If selSeason = "" Then
        MsgBox (MainForm.GetText("TtbForm", "MsgBox1", 1))
        Exit Sub
    End If
    
    NoOfLines = MainForm.GetLineCount(seaTab)
    For il = 1 To NoOfLines
        curLine = MainForm.GetLine(seaTab, il)
        tmpSeason = Trim(GetItem(curLine, 1, ","))
        If selSeason = tmpSeason Then
            tmpVpfr = Trim(GetItem(curLine, 2, ","))
            tmpVpto = Trim(GetItem(curLine, 3, ","))
            tmpVpfr = Mid(tmpVpfr, 7, 2) & "." & Mid(tmpVpfr, 5, 2) & "." & Mid(tmpVpfr, 1, 4)
            tmpVpto = Mid(tmpVpto, 7, 2) & "." & Mid(tmpVpto, 5, 2) & "." & Mid(tmpVpto, 1, 4)
        End If
    Next il
    ValidFrom.Text = tmpVpfr
    ValidTo.Text = tmpVpto
    vpfr = tmpVpfr
    vpto = tmpVpto

    Screen.MousePointer = vbHourglass
    cbFiles.Clear
    ttbFiles = ""
    TableName = "TTB"
    fieldNames = "FILENAMES"
    tabSelection = cbSeason.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            ttbFiles = ttbFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    Screen.MousePointer = vbArrow
    
End Sub

Private Sub ReadBtn_Click()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim tmpVpfr As String
    Dim tmpVpto As String

    If cbFiles.Text = "" Then
        MsgBox (MainForm.GetText("TtbForm", "MsgBox2", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    tmpVpfr = ValidFrom.Text
    If Mid(tmpVpfr, 3, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 2) & "." & Mid(tmpVpfr, 3)
    End If
    If Mid(tmpVpfr, 6, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 5) & "." & Mid(tmpVpfr, 6)
    End If
    ValidFrom.Text = tmpVpfr
    tmpVpto = ValidTo.Text
    If Mid(tmpVpto, 3, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 2) & "." & Mid(tmpVpto, 3)
    End If
    If Mid(tmpVpto, 6, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 5) & "." & Mid(tmpVpto, 6)
    End If
    ValidTo.Text = tmpVpto
    TAB1.ResetContent
    TAB1.HeaderString = cbFiles.Text
    TAB1.HeaderLengthString = 1000
    TableName = "TTB"
    fieldNames = "READ"
    tabSelection = cbFiles.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        FillTable tmpVpfr, tmpVpto
    End If
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow

End Sub

Private Sub StoreBtn_Click()
    Dim i As Long
    Dim NoOfOutputLines As Long
    Dim bufLine As String
    
    If OutFileName.Text = "" Then
        MsgBox (MainForm.GetText("TtbForm", "MsgBox3", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    Open OutFileName.Text For Output As #1
        NoOfOutputLines = TAB1.GetLineCount
        For i = 0 To NoOfOutputLines - 1
            bufLine = GetItem(TAB1.GetLineValues(i), 1, ",")
            If Mid(bufLine, 1, 2) = "20" Then
                Mid(bufLine, 9, 1) = ","
                Mid(bufLine, 18, 1) = ","
                Mid(bufLine, 25, 1) = ","
                Mid(bufLine, 33, 1) = ","
                Mid(bufLine, 37, 1) = ","
                Mid(bufLine, 42, 1) = ","
                Mid(bufLine, 47, 1) = ","
                Mid(bufLine, 51, 1) = ","
                Mid(bufLine, 55, 1) = ","
                Mid(bufLine, 61, 1) = ","
                Mid(bufLine, 63, 1) = ","
                Mid(bufLine, 67, 1) = ","
            Else
                If Mid(bufLine, 1, 2) = "Ti" Or Mid(bufLine, 1, 2) = "--" Then
                Else
                    If Len(bufLine) >= 39 Then
                        Mid(bufLine, 4, 1) = ","
                        Mid(bufLine, 37, 1) = ","
                        Mid(bufLine, 39, 1) = ","
                    End If
                End If
            End If
            Print #1, bufLine
        Next i
    Close #1
    Screen.MousePointer = vbArrow
    MsgBox (MainForm.GetText("TtbForm", "MsgBox4", 1))

End Sub
    
Private Sub CreateBtn_Click()
    Dim selSeason As String
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    
    selSeason = Trim(cbSeason.Text)
    If selSeason = "" Then
        MsgBox (MainForm.GetText("TtbForm", "MsgBox1", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    tmpVpfr = ValidFrom.Text
    If Mid(tmpVpfr, 3, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 2) & "." & Mid(tmpVpfr, 3)
    End If
    If Mid(tmpVpfr, 6, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 5) & "." & Mid(tmpVpfr, 6)
    End If
    ValidFrom.Text = tmpVpfr
    tmpVpto = ValidTo.Text
    If Mid(tmpVpto, 3, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 2) & "." & Mid(tmpVpto, 3)
    End If
    If Mid(tmpVpto, 6, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 5) & "." & Mid(tmpVpto, 6)
    End If
    ValidTo.Text = tmpVpto
    TAB1.ResetContent
    TAB1.HeaderString = cbFiles.Text
    TAB1.HeaderLengthString = 1000
    TableName = "TTB"
    fieldNames = "CREATE"
    tabSelection = selSeason
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(0), 1, ",")
        cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(0), 2, ",")
        cbFiles.Text = GetItem(MainForm.Ufis.GetBufferLine(0), 1, ",")
        TAB1.HeaderString = cbFiles.Text
        FillTable tmpVpfr, tmpVpto
    End If
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow

End Sub
    
Private Sub DeleteBtn_Click()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim NoOfLines As Integer
    Dim i As Integer
    
    If cbFiles.Text = "" Then
        MsgBox (MainForm.GetText("TtbForm", "MsgBox2", 1))
        Exit Sub
    End If
    Select Case MsgBox(MainForm.GetText("TtbForm", "MsgBox5", 1), vbOKCancel, MainForm.GetText("TtbForm", "MsgBox5", 2))
        Case vbCancel
            Exit Sub
        Case Else
            'just continue
    End Select
    Screen.MousePointer = vbHourglass
    TableName = "TTB"
    fieldNames = "DELETE"
    tabSelection = cbFiles.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        cbFiles.Clear
        ttbFiles = ""
        TableName = "TTB"
        fieldNames = "FILENAMES"
        tabSelection = cbSeason.Text
        ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
        If ilRC = 0 Then
            NoOfLines = MainForm.Ufis.GetBufferCount
            For i = 0 To NoOfLines - 1
                cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
                ttbFiles = ttbFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
            Next i
        End If
    End If
    Screen.MousePointer = vbArrow

End Sub

Private Sub FillTable(newVpfr As String, newVpto As String)
    Dim NoOfLines As Long
    Dim i As Long
    Dim tmpAirp As String
    Dim tmpAirl As String
    Dim bufLine As String
    Dim j As Integer
    
    prevBufLine = ""
    ignoreLine = False
    readCount = 0
    rejectCount = 0
    ReadCountValue.Text = readCount
    RejectCountValue.Text = rejectCount
    ReadCountValue.Refresh
    RejectCountValue.Refresh
    NoOfLines = MainForm.Ufis.GetBufferCount
    If vpfr <> newVpfr Or vpto <> newVpto Or AirpFilter.Text <> "" Or AirlFilter.Text <> "" Then
        newVpfr = Mid(newVpfr, 7) & Mid(newVpfr, 4, 2) & Mid(newVpfr, 1, 2)
        newVpto = Mid(newVpto, 7) & Mid(newVpto, 4, 2) & Mid(newVpto, 1, 2)
        If Len(AirpFilter.Text) > 0 Then
            tmpAirp = Trim(AirpFilter.Text)
            i = InStr(tmpAirp, ",")
            While i > 0
                Mid(tmpAirp, i, 1) = " "
                i = InStr(tmpAirp, ",")
            Wend
            AirpFilter.Text = Format(tmpAirp, ">")
        End If
        If Len(AirlFilter.Text) > 0 Then
            tmpAirl = Trim(AirlFilter.Text)
            i = InStr(tmpAirl, ",")
            While i > 0
                Mid(tmpAirl, i, 1) = " "
                i = InStr(tmpAirl, ",")
            Wend
            AirlFilter.Text = Format(tmpAirl, ">")
        End If
        nextTabLineNo = 0
        For i = 1 To NoOfLines - 1
            bufLine = MainForm.Ufis.GetBufferLine(i)
            readCount = readCount + 1
            ReadCountValue.Text = readCount
            ReadCountValue.Refresh
            If i = 1 Then
                bufLine = bufLine & " (" & cbSeason.Text & ": " & ValidFrom.Text & " - " & ValidTo.Text & ")"
            End If
            CheckLine bufLine, newVpfr, newVpto
        Next i
        i = TAB1.GetLineCount
        While i >= 3
            If Mid(TAB1.GetLineValues(i - 1), 1, 4) = "----" And Mid(TAB1.GetLineValues(i - 2), 1, 4) = "----" Then
                TAB1.DeleteLine (i - 1)
                TAB1.DeleteLine (i - 2)
                TAB1.DeleteLine (i - 3)
                i = i - 3
            Else
                i = i - 1
            End If
        Wend
    Else
        For i = 1 To NoOfLines - 1
            bufLine = MainForm.Ufis.GetBufferLine(i)
            readCount = readCount + 1
            ReadCountValue.Text = readCount
            ReadCountValue.Refresh
            If i = 1 Then
                bufLine = bufLine & " (" & cbSeason.Text & ": " & ValidFrom.Text & " - " & ValidTo.Text & ")"
            End If
            j = InStr(bufLine, ",")
            While j > 0
                Mid(bufLine, j, 1) = " "
                j = InStr(bufLine, ",")
            Wend
            TAB1.InsertTextLineAt i - 1, bufLine, False
        Next i
    End If
    
End Sub

Private Sub CheckLine(bufLine As String, curVpfr As String, curVpto As String)
    Dim lineVpfr As String
    Dim lineVpto As String
    Dim tmpIncl As Boolean
    Dim tmpAirp1 As String
    Dim tmpAirp2 As String
    Dim tmpAirl As String
    Dim noWords As Integer
    Dim i As Integer
    Dim tmpFilter As String
    Dim j As Integer
    Dim savBufLine As String
    
    j = InStr(bufLine, ",")
    While j > 0
        Mid(bufLine, j, 1) = " "
        j = InStr(bufLine, ",")
    Wend
    
    If Mid(bufLine, 1, 2) = "20" Then
        lineVpfr = Mid(bufLine, 1, 8)
        lineVpto = Mid(bufLine, 10, 8)
        tmpIncl = True
        If Len(AirpFilter.Text) > 0 Then
            tmpAirp1 = Mid(bufLine, 34, 3)
            tmpAirp2 = Mid(bufLine, 48, 3)
            noWords = MainForm.WordCount(AirpFilter.Text)
            If AirpOptBtn.Caption = "+" Then
                tmpIncl = False
                i = 1
                While i <= noWords And tmpIncl = False
                    tmpFilter = MainForm.GetWord(AirpFilter.Text, i)
                    If tmpAirp1 = tmpFilter Or tmpAirp2 = tmpFilter Then
                        tmpIncl = True
                    End If
                    i = i + 1
                Wend
            Else
                i = 1
                While i <= noWords And tmpIncl = True
                    tmpFilter = MainForm.GetWord(AirpFilter.Text, i)
                    If tmpAirp1 = tmpFilter Or tmpAirp2 = tmpFilter Then
                        tmpIncl = False
                    End If
                    i = i + 1
                Wend
            End If
        End If
        If Len(AirlFilter.Text) > 0 And tmpIncl = True Then
            tmpAirl = Trim(Mid(bufLine, 52, 3))
            noWords = MainForm.WordCount(AirlFilter.Text)
            If AirlOptBtn.Caption = "+" Then
                tmpIncl = False
                i = 1
                While i <= noWords And tmpIncl = False
                    tmpFilter = MainForm.GetWord(AirlFilter.Text, i)
                    If tmpAirl = tmpFilter Then
                        tmpIncl = True
                    End If
                    i = i + 1
                Wend
            Else
                i = 1
                While i <= noWords And tmpIncl = True
                    tmpFilter = MainForm.GetWord(AirlFilter.Text, i)
                    If tmpAirl = tmpFilter Then
                        tmpIncl = False
                    End If
                    i = i + 1
                Wend
            End If
        End If
        If tmpIncl = True Then
            If Val(curVpto) < Val(lineVpfr) Or Val(curVpfr) > Val(lineVpto) Then
                emptyFreq = True
            Else
                If Val(lineVpfr) < Val(curVpfr) Then
                    bufLine = curVpfr & Mid(bufLine, 9)
                End If
                If Val(lineVpto) > Val(curVpto) Then
                    bufLine = Mid(bufLine, 1, 9) & curVpto & Mid(bufLine, 18)
                End If
                bufLine = CompressFreq(bufLine)
            End If
            If emptyFreq = True Then
                rejectCount = rejectCount + 1
                RejectCountValue.Text = rejectCount
                RejectCountValue.Refresh
                If nextTabLineNo >= 2 And Len(prevBufLine) > 33 And _
                   Mid(prevBufLine, 48, 15) = Mid(bufLine, 48, 15) And _
                   Mid(prevBufLine, 19, 6) = Mid(bufLine, 19, 6) Then
                    bufLine = prevBufLine
                    prevBufLine = TAB1.GetLineValues(nextTabLineNo - 2)
                    i = InStr(prevBufLine, ",")
                    If i > 0 Then
                        prevBufLine = Mid(prevBufLine, 1, i - 1)
                    End If
                    If Len(prevBufLine) > 33 And _
                       Mid(prevBufLine, 33) = Mid(bufLine, 33) And _
                       Mid(prevBufLine, 19, 6) = Mid(bufLine, 19, 6) Then
                        savBufLine = bufLine
                        bufLine = CompressBuffer(prevBufLine, bufLine)
                        If bufLine <> savBufLine Then
                            nextTabLineNo = nextTabLineNo - 1
                            TAB1.DeleteLine nextTabLineNo
                            TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
                            nextTabLineNo = nextTabLineNo + 1
                        End If
                    End If
                    prevBufLine = TAB1.GetLineValues(nextTabLineNo - 1)
                    i = InStr(prevBufLine, ",")
                    If i > 0 Then
                        prevBufLine = Mid(prevBufLine, 1, i - 1)
                    End If
                End If
                ignoreLine = True
            Else
                bufLine = CompressPeriod(bufLine)
                If ignoreLine = True And Len(prevBufLine) > 33 And _
                   Mid(prevBufLine, 33) = Mid(bufLine, 33) And _
                   Mid(prevBufLine, 19, 6) = Mid(bufLine, 19, 6) Then
                    bufLine = CompressBuffer(prevBufLine, bufLine)
                End If
                prevBufLine = bufLine
                ignoreLine = False
                TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
                nextTabLineNo = nextTabLineNo + 1
            End If
        Else
            rejectCount = rejectCount + 1
            RejectCountValue.Text = rejectCount
            RejectCountValue.Refresh
        End If
    Else
        TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
        nextTabLineNo = nextTabLineNo + 1
    End If
    
End Sub

Function CompressFreq(curLine As String) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim oldDoop As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim tmpDay As Long
    Dim i As Integer
    
    emptyFreq = False
    tmpVpfr = Mid(curLine, 1, 8)
    tmpVpto = Mid(curLine, 10, 8)
    If Val(tmpVpto) - Val(tmpVpfr) < 6 Then
        oldDoop = Mid(curLine, 26, 7)
        newDoop = "......."
        For tmpDay = Val(tmpVpfr) To Val(tmpVpto)
            tmpWeek = Weekday(CedaDateToVb(CStr(tmpDay)), vbMonday)
            i = InStr(oldDoop, tmpWeek)
            If i > 0 Then
                If i = 1 Then
                    newDoop = CStr(i) & Mid(newDoop, 2)
                Else
                    If i = 7 Then
                        newDoop = Mid(newDoop, 1, 6) & CStr(i)
                    Else
                        newDoop = Mid(newDoop, 1, i - 1) & CStr(i) & Mid(newDoop, i + 1)
                    End If
                End If
            End If
        Next tmpDay
        curLine = Mid(curLine, 1, 25) & newDoop & Mid(curLine, 33)
        If newDoop = "......." Then
            emptyFreq = True
        End If
    End If
                
    CompressFreq = curLine
End Function

Function CompressPeriod(curLine As String) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim i As Integer
    
    tmpVpfr = Mid(curLine, 1, 8)
    newDoop = Mid(curLine, 26, 7)
    tmpWeek = Weekday(CedaDateToVb(tmpVpfr), vbMonday)
    i = InStr(newDoop, tmpWeek)
    While i <= 0
        tmpVpfr = DateAdd("n", 1440, CedaDateToVb(tmpVpfr))
        tmpVpfr = "20" & Mid(tmpVpfr, 7) & Mid(tmpVpfr, 4, 2) & Mid(tmpVpfr, 1, 2)
        tmpWeek = Weekday(CedaDateToVb(tmpVpfr), vbMonday)
        i = InStr(newDoop, tmpWeek)
    Wend
    tmpVpto = Mid(curLine, 10, 8)
    newDoop = Mid(curLine, 26, 7)
    tmpWeek = Weekday(CedaDateToVb(tmpVpto), vbMonday)
    i = InStr(newDoop, tmpWeek)
    While i <= 0
        tmpVpto = DateAdd("n", -1440, CedaDateToVb(tmpVpto))
        tmpVpto = "20" & Mid(tmpVpto, 7) & Mid(tmpVpto, 4, 2) & Mid(tmpVpto, 1, 2)
        tmpWeek = Weekday(CedaDateToVb(tmpVpto), vbMonday)
        i = InStr(newDoop, tmpWeek)
    Wend
    curLine = tmpVpfr & " " & tmpVpto & Mid(curLine, 18)
    
    CompressPeriod = curLine
End Function

Function CompressBuffer(curLine1 As String, curLine2 As String) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim oldDoop1 As String
    Dim oldDoop2 As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim tmpDay As Long
    Dim i As Integer
    
    tmpVpto = Mid(curLine1, 10, 8)
    tmpVpfr = Mid(curLine2, 1, 8)
    If Val(tmpVpfr) - Val(tmpVpto) <= 1 Then
        tmpVpfr = Mid(curLine1, 1, 8)
        tmpVpto = Mid(curLine2, 10, 8)
        oldDoop1 = Mid(curLine1, 26, 7)
        oldDoop2 = Mid(curLine2, 26, 7)
        newDoop = "......."
        For tmpDay = Val(tmpVpfr) To Val(tmpVpto)
            tmpWeek = Weekday(CedaDateToVb(CStr(tmpDay)), vbMonday)
            i = InStr(oldDoop1, tmpWeek)
            If i <= 0 Then
                i = InStr(oldDoop2, tmpWeek)
            End If
            If i > 0 Then
                If i = 1 Then
                    newDoop = CStr(i) & Mid(newDoop, 2)
                Else
                    If i = 7 Then
                        newDoop = Mid(newDoop, 1, 6) & CStr(i)
                    Else
                        newDoop = Mid(newDoop, 1, i - 1) & CStr(i) & Mid(newDoop, i + 1)
                    End If
                End If
            End If
        Next tmpDay
        curLine2 = Mid(curLine2, 1, 25) & newDoop & Mid(curLine2, 33)
        curLine2 = tmpVpfr & " " & tmpVpto & Mid(curLine2, 18)
        nextTabLineNo = nextTabLineNo - 1
        TAB1.DeleteLine nextTabLineNo
    End If
    
    CompressBuffer = curLine2
End Function

