VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MainDialog 
   BackColor       =   &H00FFFFFF&
   ClientHeight    =   11340
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   15045
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "MainDialog.frx":030A
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   11340
   ScaleWidth      =   15045
   StartUpPosition =   3  'Windows Default
   WindowState     =   1  'Minimized
   Begin VB.PictureBox PopUpPanel 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1485
      Left            =   8580
      ScaleHeight     =   1455
      ScaleWidth      =   2625
      TabIndex        =   180
      Top             =   7470
      Visible         =   0   'False
      Width           =   2655
      Begin VB.Timer PopUpOutTimer 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   1740
         Top             =   1020
      End
      Begin VB.Timer PopUpTimer 
         Enabled         =   0   'False
         Interval        =   500
         Left            =   2160
         Top             =   1020
      End
      Begin VB.Label PopUpToolTip 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " ToolTipText "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   183
         Top             =   1110
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label PopUpReport 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Status report of the system."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   660
         Left            =   15
         TabIndex        =   182
         Top             =   420
         Width           =   1890
         WordWrap        =   -1  'True
      End
      Begin VB.Label PopUpLabel 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Just a Fine Short Text"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1560
         TabIndex        =   181
         Top             =   30
         Width           =   930
         WordWrap        =   -1  'True
      End
      Begin VB.Image PopUpHarry 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   0
         Top             =   0
         Width           =   645
      End
      Begin VB.Image PopUpPic 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   750
         Top             =   0
         Width           =   645
      End
   End
   Begin VB.PictureBox StatusPanel 
      BackColor       =   &H00C0C0C0&
      Height          =   675
      Left            =   1830
      ScaleHeight     =   615
      ScaleWidth      =   9735
      TabIndex        =   175
      Top             =   2910
      Visible         =   0   'False
      Width           =   9795
   End
   Begin VB.PictureBox SidePanel 
      BackColor       =   &H80000003&
      Height          =   2115
      Left            =   11940
      ScaleHeight     =   2055
      ScaleWidth      =   1740
      TabIndex        =   174
      Top             =   1620
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.PictureBox LeftPanel 
      BackColor       =   &H80000003&
      Height          =   2115
      Left            =   60
      ScaleHeight     =   2055
      ScaleWidth      =   1650
      TabIndex        =   173
      Top             =   1620
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.PictureBox TaskPanel 
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   1830
      ScaleHeight     =   255
      ScaleWidth      =   9735
      TabIndex        =   172
      Top             =   2280
      Visible         =   0   'False
      Width           =   9795
   End
   Begin VB.PictureBox ToolPanel 
      BackColor       =   &H0000C0C0&
      Height          =   585
      Left            =   1830
      ScaleHeight     =   525
      ScaleWidth      =   9735
      TabIndex        =   171
      Top             =   1620
      Visible         =   0   'False
      Width           =   9795
   End
   Begin VB.PictureBox MsgPanel 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      ForeColor       =   &H80000008&
      Height          =   570
      Index           =   0
      Left            =   60
      ScaleHeight     =   540
      ScaleWidth      =   3765
      TabIndex        =   157
      Top             =   8880
      Visible         =   0   'False
      Width           =   3795
      Begin VB.Image Image1 
         Height          =   240
         Left            =   60
         Picture         =   "MainDialog.frx":074C
         Top             =   60
         Width           =   240
      End
      Begin VB.Label MsgLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "This will take a few seconds. Please wait ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   390
         TabIndex        =   159
         Top             =   270
         Width           =   3195
      End
      Begin VB.Label MsgLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Initializing The Chart Environment ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   420
         TabIndex        =   158
         Top             =   60
         Width           =   2535
      End
   End
   Begin VB.TextBox Text1 
      Height          =   435
      Left            =   6090
      TabIndex        =   155
      Text            =   "test1,1564468446,1564593055|UFIS$ADMIN,1,L|1351,58|"
      Top             =   9000
      Visible         =   0   'False
      Width           =   7485
   End
   Begin VB.Frame fraFolderPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5085
      Index           =   0
      Left            =   60
      TabIndex        =   140
      Top             =   10200
      Visible         =   0   'False
      Width           =   13515
      Begin VB.CheckBox chkCedaSend 
         Caption         =   "Transmit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   154
         Top             =   3810
         Width           =   1035
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   5
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   152
         Top             =   3300
         Width           =   8805
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   150
         Top             =   2820
         Width           =   8805
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   145
         Top             =   2370
         Width           =   8805
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   144
         Top             =   1920
         Width           =   1365
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   143
         Top             =   1470
         Width           =   1365
      End
      Begin VB.TextBox txtCedaCmd 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   142
         Top             =   1020
         Width           =   1365
      End
      Begin VB.ComboBox cboCedaCmd 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   960
         TabIndex        =   141
         Top             =   330
         Width           =   8895
      End
      Begin VB.Label Label7 
         Caption         =   "DAT"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   330
         TabIndex        =   153
         Top             =   3360
         Width           =   465
      End
      Begin VB.Label Label7 
         Caption         =   "FLD"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   330
         TabIndex        =   151
         Top             =   2910
         Width           =   465
      End
      Begin VB.Label Label7 
         Caption         =   "SEL"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   330
         TabIndex        =   149
         Top             =   2430
         Width           =   465
      End
      Begin VB.Label Label7 
         Caption         =   "TBL"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   330
         TabIndex        =   148
         Top             =   1980
         Width           =   465
      End
      Begin VB.Label Label7 
         Caption         =   "TWS"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   330
         TabIndex        =   147
         Top             =   1530
         Width           =   465
      End
      Begin VB.Label Label7 
         Caption         =   "CMD"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   330
         TabIndex        =   146
         Top             =   1080
         Width           =   465
      End
   End
   Begin VB.Timer MainTimer 
      Index           =   4
      Interval        =   60000
      Left            =   12060
      Top             =   7020
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   3
      Interval        =   50
      Left            =   11580
      Top             =   7020
   End
   Begin VB.PictureBox SystabPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2535
      Left            =   8130
      ScaleHeight     =   2475
      ScaleWidth      =   3525
      TabIndex        =   78
      Top             =   4380
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkTask 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   164
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "To Chart"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   163
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Sort Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   162
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Re-Sync"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   161
         Top             =   0
         Width           =   1035
      End
      Begin TABLib.TAB SysTab 
         Height          =   645
         Left            =   0
         TabIndex        =   79
         Top             =   300
         Visible         =   0   'False
         Width           =   1875
         _Version        =   65536
         _ExtentX        =   3307
         _ExtentY        =   1138
         _StockProps     =   64
      End
      Begin TABLib.TAB RotationData 
         Height          =   645
         Left            =   0
         TabIndex        =   156
         Top             =   1020
         Visible         =   0   'False
         Width           =   1905
         _Version        =   65536
         _ExtentX        =   3360
         _ExtentY        =   1138
         _StockProps     =   64
      End
      Begin TABLib.TAB ConnexData 
         Height          =   645
         Left            =   0
         TabIndex        =   166
         Top             =   1740
         Visible         =   0   'False
         Width           =   1905
         _Version        =   65536
         _ExtentX        =   3360
         _ExtentY        =   1138
         _StockProps     =   64
      End
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   2
      Interval        =   50
      Left            =   11100
      Top             =   7020
   End
   Begin VB.PictureBox DelayPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2535
      Left            =   11760
      ScaleHeight     =   2475
      ScaleWidth      =   1935
      TabIndex        =   64
      Top             =   4380
      Visible         =   0   'False
      Width           =   1995
      Begin VB.PictureBox DlyEditPanel 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   30
         ScaleHeight     =   375
         ScaleWidth      =   1845
         TabIndex        =   65
         TabStop         =   0   'False
         Top             =   120
         Width           =   1845
         Begin VB.CheckBox chkDlyCheck 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   780
            Style           =   1  'Graphical
            TabIndex        =   69
            Top             =   30
            Visible         =   0   'False
            Width           =   180
         End
         Begin VB.TextBox txtDlyInput 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   1110
            MaxLength       =   5
            TabIndex        =   68
            Text            =   "00:15"
            Top             =   30
            Width           =   675
         End
         Begin VB.CheckBox chkDlyTick 
            Height          =   210
            Index           =   0
            Left            =   60
            TabIndex        =   66
            Top             =   75
            Width           =   195
         End
         Begin VB.Label lblDlyReason 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "A/C-Chg"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   300
            TabIndex        =   67
            Top             =   75
            Width           =   735
         End
      End
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Task"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   9030
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   7020
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   1000
      Left            =   10620
      Top             =   7020
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   60
      ScaleHeight     =   4275
      ScaleWidth      =   6315
      TabIndex        =   6
      Top             =   4395
      Width           =   6375
      Begin TABLib.TAB TabPropBag 
         Height          =   915
         Index           =   0
         Left            =   2370
         TabIndex        =   177
         Top             =   2160
         Visible         =   0   'False
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   1614
         _StockProps     =   64
      End
      Begin VB.CheckBox chkTabCoverIsVisible 
         Caption         =   "Show Cover"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   2400
         TabIndex        =   170
         Top             =   1800
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.PictureBox TabCover 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Height          =   2415
         Index           =   0
         Left            =   3900
         ScaleHeight     =   2415
         ScaleWidth      =   2355
         TabIndex        =   169
         Top             =   1800
         Visible         =   0   'False
         Width           =   2355
         Begin VB.PictureBox TabCoverPic 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            ForeColor       =   &H80000008&
            Height          =   5400
            Index           =   0
            Left            =   150
            ScaleHeight     =   5370
            ScaleWidth      =   7170
            TabIndex        =   176
            TabStop         =   0   'False
            Top             =   150
            Visible         =   0   'False
            Width           =   7200
            Begin VB.Image TabCoverBmp 
               Height          =   5400
               Index           =   0
               Left            =   0
               Picture         =   "MainDialog.frx":0896
               Top             =   360
               Width           =   7200
            End
         End
      End
      Begin VB.TextBox txtTabEditLookUp 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   60
         TabIndex        =   115
         Text            =   "TabEditLookUpField"
         Top             =   3150
         Visible         =   0   'False
         Width           =   1875
      End
      Begin VB.PictureBox TabPanel 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   1275
         Index           =   0
         Left            =   60
         ScaleHeight     =   1275
         ScaleWidth      =   2235
         TabIndex        =   98
         Top             =   1800
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   0
         Left            =   2010
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   96
         TabStop         =   0   'False
         Top             =   3150
         Visible         =   0   'False
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   0
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   1
         Left            =   2205
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   95
         TabStop         =   0   'False
         Top             =   3300
         Visible         =   0   'False
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   1
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   2
         Left            =   2415
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   94
         TabStop         =   0   'False
         Top             =   3480
         Visible         =   0   'False
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   2
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin TABLib.TAB SelTab1 
         Height          =   1695
         Index           =   0
         Left            =   5250
         TabIndex        =   92
         Top             =   30
         Visible         =   0   'False
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   2990
         _StockProps     =   64
      End
      Begin VB.PictureBox MinMaxPanel 
         BackColor       =   &H0080C0FF&
         BorderStyle     =   0  'None
         Height          =   1650
         Index           =   0
         Left            =   2400
         ScaleHeight     =   1650
         ScaleWidth      =   2865
         TabIndex        =   58
         Top             =   60
         Visible         =   0   'False
         Width           =   2865
         Begin VB.Frame fraTabCaption 
            BorderStyle     =   0  'None
            Caption         =   "Frame9"
            Height          =   255
            Index           =   0
            Left            =   2070
            TabIndex        =   110
            Top             =   30
            Width           =   435
         End
         Begin VB.Frame fraMaxButton 
            BackColor       =   &H000000FF&
            BorderStyle     =   0  'None
            Height          =   660
            Index           =   0
            Left            =   60
            TabIndex        =   80
            Top             =   330
            Visible         =   0   'False
            Width           =   2745
            Begin VB.Frame fraSigns 
               BorderStyle     =   0  'None
               Height          =   270
               Index           =   0
               Left            =   540
               TabIndex        =   112
               Top             =   0
               Width           =   375
               Begin VB.Shape BcSign2 
                  FillColor       =   &H0000FFFF&
                  Height          =   105
                  Index           =   0
                  Left            =   15
                  Shape           =   3  'Circle
                  Top             =   135
                  Width           =   105
               End
               Begin VB.Shape BcSign1 
                  FillColor       =   &H0000FF00&
                  Height          =   105
                  Index           =   0
                  Left            =   15
                  Shape           =   3  'Circle
                  Top             =   15
                  Width           =   105
               End
            End
            Begin VB.CheckBox chkMaxSec 
               Caption         =   "^"
               BeginProperty Font 
                  Name            =   "Arial Black"
                  Size            =   9.75
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   1935
               Style           =   1  'Graphical
               TabIndex        =   87
               ToolTipText     =   "Maximize grid in vertical frame"
               Top             =   0
               Width           =   270
            End
            Begin VB.CheckBox chkHide 
               Caption         =   "X"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   2475
               Style           =   1  'Graphical
               TabIndex        =   82
               ToolTipText     =   "Close section window"
               Top             =   0
               Width           =   270
            End
            Begin VB.CheckBox chkMax 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "Arial Black"
                  Size            =   9.75
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   2205
               Style           =   1  'Graphical
               TabIndex        =   81
               ToolTipText     =   "Maximize grid in application window"
               Top             =   0
               Width           =   270
            End
            Begin VB.Label lblTabUtl 
               Alignment       =   2  'Center
               BorderStyle     =   1  'Fixed Single
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   0
               TabIndex        =   113
               Tag             =   "CLOSED"
               Top             =   0
               Width           =   270
            End
            Begin VB.Label lblTabPos 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   " 0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   675
               TabIndex        =   85
               ToolTipText     =   "Current Cursor Position on Line .."
               Top             =   0
               Width           =   630
            End
            Begin VB.Label lblTabCnt 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   " 0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   1305
               TabIndex        =   84
               ToolTipText     =   "Total Amount of Records in the Grid"
               Top             =   0
               Width           =   630
            End
            Begin VB.Label lblTabCtl 
               Alignment       =   2  'Center
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   270
               TabIndex        =   91
               Top             =   0
               Width           =   270
            End
         End
         Begin VB.Label lblTabCaption 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "TabCaption"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   90
            TabIndex        =   86
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label lblTabCaptionShadow 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "TabCaptionShadow"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   0
            Left            =   105
            TabIndex        =   109
            Top             =   45
            Width           =   1875
         End
      End
      Begin VB.PictureBox fraVtSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   1515
         Index           =   0
         Left            =   2160
         MousePointer    =   9  'Size W E
         ScaleHeight     =   1515
         ScaleWidth      =   135
         TabIndex        =   22
         Top             =   0
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.PictureBox fraCrSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   0
         Left            =   2160
         MousePointer    =   15  'Size All
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   21
         Top             =   1590
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.PictureBox fraHzSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   0
         Left            =   0
         MousePointer    =   7  'Size N S
         ScaleHeight     =   135
         ScaleWidth      =   2085
         TabIndex        =   20
         Top             =   1590
         Visible         =   0   'False
         Width           =   2085
      End
      Begin TABLib.TAB FileData 
         Height          =   1005
         Index           =   0
         Left            =   30
         TabIndex        =   7
         Top             =   540
         Visible         =   0   'False
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   1773
         _StockProps     =   64
         Columns         =   10
         FontName        =   "Arial"
      End
      Begin TABLib.TAB SelTab2 
         Height          =   1695
         Index           =   0
         Left            =   5760
         TabIndex        =   93
         Top             =   30
         Visible         =   0   'False
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   2990
         _StockProps     =   64
      End
      Begin TABLib.TAB TabLookUp 
         Height          =   465
         Index           =   0
         Left            =   30
         TabIndex        =   111
         Top             =   30
         Visible         =   0   'False
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   820
         _StockProps     =   64
         Columns         =   10
         FontName        =   "Arial"
      End
      Begin VB.TextBox txtTabEdit 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         TabIndex        =   114
         Text            =   "TabEditTextField"
         Top             =   3480
         Visible         =   0   'False
         Width           =   1875
      End
   End
   Begin VB.PictureBox BottomPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   12750
      TabIndex        =   5
      Top             =   9630
      Visible         =   0   'False
      Width           =   12810
      Begin VB.CheckBox chkTest 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00E0E0E0&
         Height          =   210
         Index           =   1
         Left            =   0
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   192
         Tag             =   "-1"
         Top             =   210
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox chkTest 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00E0E0E0&
         Height          =   210
         Index           =   0
         Left            =   0
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   191
         Tag             =   "-1"
         Top             =   0
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   2
         Left            =   10470
         TabIndex        =   15
         Top             =   30
         Visible         =   0   'False
         Width           =   885
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   330
         Index           =   1
         Left            =   8100
         TabIndex        =   12
         Top             =   60
         Visible         =   0   'False
         Width           =   1935
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   0
            Width           =   855
         End
         Begin VB.TextBox txtFeld 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   870
            Locked          =   -1  'True
            TabIndex        =   13
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   0
         Left            =   30
         TabIndex        =   9
         Top             =   30
         Visible         =   0   'False
         Width           =   1755
         Begin VB.TextBox txtFeld 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   870
            Locked          =   -1  'True
            TabIndex        =   11
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   1575
      Left            =   0
      ScaleHeight     =   1515
      ScaleWidth      =   13710
      TabIndex        =   4
      Top             =   0
      Width           =   13770
      Begin VB.CommandButton Command2 
         Caption         =   "Random VERS"
         Height          =   315
         Left            =   7200
         TabIndex        =   179
         Top             =   30
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   315
         Left            =   6000
         TabIndex        =   178
         Top             =   30
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.TextBox Text2 
         Height          =   315
         Left            =   6120
         TabIndex        =   165
         Text            =   "Text2"
         Top             =   0
         Visible         =   0   'False
         Width           =   7515
      End
      Begin VB.PictureBox FontSliderPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   3780
         ScaleHeight     =   315
         ScaleWidth      =   1035
         TabIndex        =   99
         Top             =   60
         Visible         =   0   'False
         Width           =   1035
         Begin VB.Frame Frame5 
            BackColor       =   &H80000015&
            BorderStyle     =   0  'None
            Height          =   15
            Index           =   1
            Left            =   0
            TabIndex        =   107
            Top             =   300
            Width           =   1035
         End
         Begin VB.Frame Frame1 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   15
            Index           =   1
            Left            =   0
            TabIndex        =   106
            Top             =   0
            Width           =   1020
         End
         Begin VB.Frame Frame2 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   1
            Left            =   0
            TabIndex        =   105
            Top             =   0
            Width           =   15
         End
         Begin VB.Frame Frame3 
            BackColor       =   &H80000016&
            BorderStyle     =   0  'None
            Height          =   15
            Index           =   1
            Left            =   15
            TabIndex        =   104
            Top             =   15
            Width           =   990
         End
         Begin VB.Frame Frame4 
            BackColor       =   &H80000016&
            BorderStyle     =   0  'None
            Height          =   270
            Index           =   1
            Left            =   15
            TabIndex        =   103
            Top             =   15
            Width           =   15
         End
         Begin VB.Frame Frame6 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   15
            Index           =   1
            Left            =   15
            TabIndex        =   102
            Top             =   285
            Width           =   1005
         End
         Begin VB.Frame Frame7 
            BackColor       =   &H80000015&
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   1
            Left            =   1020
            TabIndex        =   101
            Top             =   0
            Width           =   15
         End
         Begin VB.Frame Frame8 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   285
            Index           =   1
            Left            =   1005
            TabIndex        =   100
            Top             =   15
            Width           =   15
         End
         Begin MSComctlLib.Slider FontSlider 
            Height          =   315
            Left            =   -30
            TabIndex        =   108
            ToolTipText     =   "Font Size Slider"
            Top             =   15
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   556
            _Version        =   393216
            LargeChange     =   1
            TickStyle       =   3
         End
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   0
         Left            =   2640
         Picture         =   "MainDialog.frx":C6C9
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   97
         Top             =   90
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox FlightPanel 
         AutoRedraw      =   -1  'True
         Height          =   975
         Left            =   0
         ScaleHeight     =   915
         ScaleWidth      =   13605
         TabIndex        =   35
         Top             =   420
         Width           =   13665
         Begin VB.PictureBox FlightPanelCover 
            AutoRedraw      =   -1  'True
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   2
            Left            =   7260
            ScaleHeight     =   975
            ScaleWidth      =   1440
            TabIndex        =   132
            Top             =   0
            Width           =   1440
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   6
               Left            =   30
               TabIndex        =   139
               Top             =   585
               Width           =   780
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   7
               Left            =   810
               TabIndex        =   138
               Top             =   585
               Width           =   570
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   8
               Left            =   30
               TabIndex        =   137
               Top             =   255
               Width           =   1350
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   6
               Left            =   30
               TabIndex        =   136
               Top             =   585
               Width           =   780
            End
            Begin VB.Label Label9 
               BackStyle       =   0  'Transparent
               Caption         =   "Aircraft / Type"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   135
               TabIndex        =   135
               Top             =   30
               Width           =   1275
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   7
               Left            =   810
               TabIndex        =   134
               Top             =   585
               Width           =   570
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   8
               Left            =   30
               TabIndex        =   133
               Top             =   255
               Width           =   1350
            End
         End
         Begin VB.PictureBox FlightPanelCover 
            AutoRedraw      =   -1  'True
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   1
            Left            =   5880
            ScaleHeight     =   975
            ScaleWidth      =   1350
            TabIndex        =   124
            Top             =   0
            Width           =   1350
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   15
               Left            =   690
               TabIndex        =   131
               ToolTipText     =   "Departure Gate"
               Top             =   585
               Width           =   600
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   14
               Left            =   60
               TabIndex        =   130
               ToolTipText     =   "Departure Stand"
               Top             =   585
               Width           =   600
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   15
               Left            =   690
               TabIndex        =   129
               ToolTipText     =   "Arrival Gate"
               Top             =   255
               Width           =   600
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   14
               Left            =   60
               TabIndex        =   128
               ToolTipText     =   "Arrival Stand"
               Top             =   255
               Width           =   600
            End
            Begin VB.Label Label3 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "P.Bay / Gate"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   -30
               TabIndex        =   127
               Top             =   30
               Width           =   1365
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   13
               Left            =   60
               TabIndex        =   126
               Top             =   255
               Width           =   270
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   13
               Left            =   60
               TabIndex        =   125
               Top             =   585
               Width           =   270
            End
         End
         Begin VB.PictureBox FlightPanelCover 
            AutoRedraw      =   -1  'True
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   0
            Left            =   9540
            ScaleHeight     =   975
            ScaleWidth      =   1350
            TabIndex        =   116
            Top             =   0
            Visible         =   0   'False
            Width           =   1350
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   10
               Left            =   60
               TabIndex        =   123
               Top             =   585
               Width           =   270
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   10
               Left            =   60
               TabIndex        =   122
               Top             =   255
               Width           =   270
            End
            Begin VB.Label Label2 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "Mvt/Req. Logs"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   0
               TabIndex        =   121
               Top             =   30
               Width           =   1365
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   11
               Left            =   330
               TabIndex        =   120
               ToolTipText     =   "Total Read Spec. Requirements"
               Top             =   255
               Width           =   480
            End
            Begin VB.Label lblArr 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   12
               Left            =   810
               TabIndex        =   119
               ToolTipText     =   "Total Not Read Spec. Requirements"
               Top             =   255
               Width           =   480
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   11
               Left            =   330
               TabIndex        =   118
               ToolTipText     =   "Total Read Spec. Requirements"
               Top             =   585
               Width           =   480
            End
            Begin VB.Label lblDep 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   12
               Left            =   810
               TabIndex        =   117
               ToolTipText     =   "Total Not Read Spec. Requirements"
               Top             =   585
               Width           =   480
            End
         End
         Begin VB.Timer FlightPanelTimer 
            Enabled         =   0   'False
            Interval        =   10
            Left            =   11130
            Top             =   480
         End
         Begin VB.CheckBox chkSelToggle 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   30
            Style           =   1  'Graphical
            TabIndex        =   77
            Top             =   30
            Visible         =   0   'False
            Width           =   555
         End
         Begin VB.CheckBox chkSelFlight 
            Caption         =   "&DEP"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   30
            Style           =   1  'Graphical
            TabIndex        =   76
            Top             =   585
            Width           =   555
         End
         Begin VB.CheckBox chkSelFlight 
            Caption         =   "&ARR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   30
            Style           =   1  'Graphical
            TabIndex        =   75
            Top             =   255
            Width           =   555
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   7
            Left            =   12720
            TabIndex        =   74
            Text            =   "DepFields"
            Top             =   600
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   6
            Left            =   11820
            TabIndex        =   73
            Text            =   "ArrFields"
            Top             =   600
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   5
            Left            =   10890
            TabIndex        =   72
            Text            =   "AllFields"
            Top             =   330
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   4
            Left            =   12720
            TabIndex        =   63
            Text            =   "DepRec"
            Top             =   330
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   3
            Left            =   12720
            TabIndex        =   62
            Text            =   "DepUrno"
            Top             =   0
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   2
            Left            =   11820
            TabIndex        =   61
            Text            =   "ArrRec"
            Top             =   330
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   1
            Left            =   11820
            TabIndex        =   60
            Text            =   "ArrUrno"
            Top             =   0
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   0
            Left            =   10890
            TabIndex        =   59
            Text            =   "TurnType"
            Top             =   0
            Visible         =   0   'False
            Width           =   945
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   2190
            TabIndex        =   57
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Flight No."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   780
            TabIndex        =   56
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label11 
            BackStyle       =   0  'Transparent
            Caption         =   "Remarks"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   8880
            TabIndex        =   55
            Top             =   30
            Width           =   1245
         End
         Begin VB.Label lblDep 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   8760
            TabIndex        =   54
            Top             =   585
            Width           =   1710
         End
         Begin VB.Label lblArr 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   8760
            TabIndex        =   53
            Top             =   255
            Width           =   1710
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   5100
            TabIndex        =   52
            Top             =   255
            Width           =   720
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   5100
            TabIndex        =   51
            Top             =   585
            Width           =   720
         End
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Actual"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   5010
            TabIndex        =   50
            Top             =   30
            Width           =   885
         End
         Begin VB.Label Label5 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Estim."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   4110
            TabIndex        =   49
            Top             =   30
            Width           =   1035
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   4260
            TabIndex        =   48
            Top             =   585
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   4260
            TabIndex        =   47
            Top             =   255
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   3420
            TabIndex        =   46
            Top             =   255
            Width           =   720
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   3420
            TabIndex        =   45
            Top             =   585
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   2010
            TabIndex        =   44
            Top             =   255
            Width           =   1305
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   2010
            TabIndex        =   43
            Top             =   585
            Width           =   1305
         End
         Begin VB.Label Label4 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Sched."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   3270
            TabIndex        =   42
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   1230
            TabIndex        =   41
            Top             =   585
            Width           =   675
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   615
            TabIndex        =   40
            Top             =   585
            Width           =   615
         End
         Begin VB.Label lblDepRow 
            BackStyle       =   0  'Transparent
            Caption         =   "DEP:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000C000&
            Height          =   225
            Left            =   90
            TabIndex        =   39
            Top             =   630
            Width           =   495
         End
         Begin VB.Label lblArrRow 
            BackStyle       =   0  'Transparent
            Caption         =   "ARR:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   225
            Left            =   90
            TabIndex        =   38
            Top             =   300
            Width           =   495
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   1230
            TabIndex        =   37
            Top             =   255
            Width           =   675
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   615
            TabIndex        =   36
            Top             =   255
            Width           =   615
         End
      End
      Begin VB.TextBox txtEdit 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2580
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkImpTyp 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1500
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   60
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Work"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   450
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   60
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Frame fraTopButtonPanel 
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   0
         Left            =   30
         TabIndex        =   8
         Top             =   60
         Width           =   375
      End
      Begin VB.Label lblContext 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "15JUN03"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   4860
         TabIndex        =   33
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   10410
      Left            =   13800
      ScaleHeight     =   10350
      ScaleWidth      =   1155
      TabIndex        =   2
      Top             =   0
      Width           =   1215
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Rotations"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   7
         Left            =   0
         TabIndex        =   193
         Top             =   9690
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Server P."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   6
         Left            =   0
         TabIndex        =   190
         Top             =   9480
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Bottom P."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   0
         TabIndex        =   189
         Top             =   9270
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Status P."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   4
         Left            =   0
         TabIndex        =   188
         Top             =   9060
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Task Pan."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   3
         Left            =   0
         TabIndex        =   187
         Top             =   8850
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Tool Pan."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   0
         TabIndex        =   186
         Top             =   8640
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Right Pan."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   0
         TabIndex        =   185
         Top             =   8430
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CheckBox chkTaskPanel 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Left Panel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   0
         TabIndex        =   184
         Top             =   8220
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.Timer RightPanelTimer 
         Enabled         =   0   'False
         Interval        =   3000
         Left            =   120
         Top             =   3690
      End
      Begin VB.CheckBox chkTerminate 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   30
         Width           =   315
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTabIsVisible 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         TabIndex        =   23
         Tag             =   "-1"
         Top             =   2790
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.Frame fraYButtonPanel 
         BorderStyle     =   0  'None
         Height          =   1650
         Index           =   0
         Left            =   60
         TabIndex        =   17
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkAppl 
            Caption         =   "Break"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   90
            Tag             =   "BREAK"
            ToolTipText     =   "Saves your work for a break"
            Top             =   1320
            Width           =   1035
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "S1"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   89
            Tag             =   "SETUP"
            ToolTipText     =   "Flight Marker ON/OFF"
            Top             =   990
            Width           =   525
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "S2"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   88
            Tag             =   "SETUP"
            ToolTipText     =   "User Marker ON/OFF"
            Top             =   990
            Width           =   495
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "LOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   71
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to SIN Local Times"
            Top             =   660
            Width           =   525
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   70
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to UTC Times"
            Top             =   660
            Width           =   495
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "About"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   19
            Tag             =   "ABOUT"
            Top             =   330
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   18
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   167
            Tag             =   "ABOUT"
            Top             =   660
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   168
            Tag             =   "ABOUT"
            Top             =   990
            Width           =   1035
         End
      End
      Begin VB.HScrollBar ImpScroll 
         Height          =   315
         Left            =   60
         TabIndex        =   3
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.PictureBox ServerPanel 
         AutoRedraw      =   -1  'True
         BackColor       =   &H000080FF&
         Height          =   525
         Left            =   0
         ScaleHeight     =   465
         ScaleWidth      =   1245
         TabIndex        =   30
         Top             =   4470
         Visible         =   0   'False
         Width           =   1305
         Begin VB.Label lblSrvName 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Server"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000009&
            Height          =   225
            Index           =   0
            Left            =   150
            TabIndex        =   31
            Top             =   0
            Width           =   735
         End
         Begin VB.Shape ServerSignal 
            FillColor       =   &H0000FF00&
            FillStyle       =   0  'Solid
            Height          =   165
            Index           =   4
            Left            =   60
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            FillColor       =   &H0000C0C0&
            FillStyle       =   0  'Solid
            Height          =   165
            Index           =   3
            Left            =   900
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   2
            Left            =   690
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   1
            Left            =   480
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   0
            Left            =   270
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
      End
      Begin VB.Line linLight 
         BorderColor     =   &H00FFFFFF&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   2760
         Y2              =   2760
      End
      Begin VB.Line linDark 
         BorderColor     =   &H00404040&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   4200
         Y2              =   4200
      End
      Begin VB.Label lblTabName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   300
         TabIndex        =   27
         Top             =   3120
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblTabShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   90
         TabIndex        =   25
         Top             =   3390
         Visible         =   0   'False
         Width           =   690
      End
   End
   Begin TABLib.TAB HelperTab 
      Height          =   2535
      Index           =   0
      Left            =   6480
      TabIndex        =   1
      Top             =   4380
      Visible         =   0   'False
      Width           =   795
      _Version        =   65536
      _ExtentX        =   1402
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   1000
      Left            =   10140
      Top             =   7020
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   11025
      Width           =   15045
      _ExtentX        =   26538
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18150
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1429
            MinWidth        =   1058
            TextSave        =   "12/7/2007"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1270
            MinWidth        =   1058
            TextSave        =   "5:02 AM"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TABLib.TAB HelperTab 
      Height          =   2535
      Index           =   1
      Left            =   7290
      TabIndex        =   83
      Top             =   4380
      Visible         =   0   'False
      Width           =   795
      _Version        =   65536
      _ExtentX        =   1402
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin VB.PictureBox MsgPanel 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   570
      Index           =   1
      Left            =   90
      ScaleHeight     =   570
      ScaleWidth      =   3795
      TabIndex        =   160
      Top             =   8910
      Visible         =   0   'False
      Width           =   3795
   End
   Begin VB.Image PicHarry 
      Height          =   1920
      Index           =   0
      Left            =   6540
      Picture         =   "MainDialog.frx":C71D
      Top             =   6990
      Width           =   1920
   End
   Begin VB.Image imgCurPosDn 
      Height          =   240
      Left            =   11880
      Picture         =   "MainDialog.frx":1875F
      Top             =   7560
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosUp 
      Height          =   240
      Left            =   12150
      Picture         =   "MainDialog.frx":18CE9
      Top             =   7560
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPos 
      Height          =   240
      Index           =   0
      Left            =   11310
      Picture         =   "MainDialog.frx":19273
      Top             =   8160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAnyPic1 
      Height          =   240
      Index           =   0
      Left            =   11610
      Picture         =   "MainDialog.frx":197FD
      ToolTipText     =   "Steps through the tool box"
      Top             =   8160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpB 
      Height          =   240
      Left            =   11580
      Picture         =   "MainDialog.frx":19D87
      Top             =   7560
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpA 
      Height          =   240
      Left            =   11310
      Picture         =   "MainDialog.frx":1A311
      Top             =   7560
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgInfo1 
      Height          =   240
      Left            =   11310
      Picture         =   "MainDialog.frx":1A89B
      Top             =   7830
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgExtract 
      Height          =   240
      Left            =   11910
      Picture         =   "MainDialog.frx":1A9E5
      Top             =   7830
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgInfo2 
      Height          =   240
      Left            =   11610
      Picture         =   "MainDialog.frx":1AF6F
      Top             =   7830
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAnyPic2 
      Height          =   240
      Index           =   0
      Left            =   11910
      Picture         =   "MainDialog.frx":1B0B9
      Top             =   8160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Menu FDCPopup 
      Caption         =   "FDCPopup"
      Visible         =   0   'False
      Begin VB.Menu ShowMe 
         Caption         =   "Show Me"
      End
      Begin VB.Menu ExitMe 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ShowMaxButtons As Integer
Dim MainSections As Integer
Dim MaxMainButtonRow As Integer
Dim MaxMainButtonIdx As Integer
Dim CountMainTabs As Integer
Dim CountSubTabs As Integer
Dim MinFrameSize As Long
Dim MinLeftWidth As Long
Dim MaxLeftWidth As Long
Dim MinRightWidth As Long
Dim MaxRightWidth As Long
Dim SplitterMoves As Boolean
Dim FullMaximizedTab As Integer
Dim LeftMaximizedTab As Integer
Dim RightMaximizedTab As Integer
Dim SetMyTop As Long
Dim SetMyLeft As Long
Dim SetMyHeight As Long
Dim SetMyWidth As Long
Dim RefreshOnBc As Boolean
Dim LastFocusIndex As Integer
Dim CurFocusIndex As Integer
Dim CurImpButtonIndex As Integer
Dim FocusSyncInternal As Boolean
Dim DontSyncRota As Boolean
Dim TabEditLookUpTabObj As TABLib.Tab
Dim TabEditLookUpType As String
Dim TabEditLookUpIndex As Integer
Dim TabEditLookUpFixed As Integer
Dim TabEditInline As Boolean
Dim TabEditInsert As Boolean
Dim TabEditUpdate As Boolean
Dim TabEditIsActive As Boolean
Dim TabEditLineNo As Long
Dim TabEditColNo As Long
Dim TabEditHitEnter As Boolean
Dim TabEditLookUpValueCol As Long
Dim TabEditIsUpperCase As Boolean
Dim AutoSaveAodb As Boolean
Dim AutoTabRead As Boolean
Dim AutoRestore As Boolean
Public WithEvents MsExcel As Excel.Application
Attribute MsExcel.VB_VarHelpID = -1
Dim ExcelJustOpened As Boolean
Public ExcelIsOpen As Boolean
Dim ExcelShutDown As Boolean
Public MsWkBook As Workbook
Public MsWkSheet As Worksheet
Public WithEvents MsWord As Word.Application
Attribute MsWord.VB_VarHelpID = -1
Dim MsWordJustOpened As Boolean
Public MsWordIsOpen As Boolean
Dim MsWordShutDown As Boolean
Dim ExcelFields As String
Dim ExcelUpdateAllowed As Boolean
Dim ExcelRoSaveAllowed As Boolean
Dim SplitPosH As Long
Dim SplitPosV As Long
Dim MaxFormHeight As Long
Dim MaxFormWidth As Long
Dim ApplComingUp As Boolean
Dim ReorgFlightPanel As Boolean
Dim ReorgFlightToggle As Boolean
Dim TimerIsActive As Boolean
Dim ButtonPushedInternal As Boolean
Dim ListOfCedaTimeFields As String
Public CurrentTabFont As String
Dim CurrentTimeZone As String
Dim CurAutoSortCfg As String
Dim CurPrintLayout As String
Public DontCheck As Boolean
Dim RecoverFileName As String
Dim RecoverTitle As String
Dim AutoRecover As Boolean
Dim LightLinIdx As Integer
Dim DarkLinIdx As Integer
Dim ShowMinMaxPanels As Boolean
Dim ShowScenarioTools As Boolean
Dim SystabActivated As Boolean
Dim ConnexActivated As Boolean
Dim AutoHideSynchroTabs As Boolean

Private Sub ArrangeWorkButtons(CurPanelIndex As Integer, IniSection As String, DefaultList As String, MaxMainRows As Integer)
    Dim IniKeyWord As String
    Dim PanelTag As String
    Dim ButtonList As String
    Dim ButtonDefine As String
    Dim ButtonType As String
    Dim ButtonCapt As String
    Dim ButtonFunc As String
    Dim ButtonParam As String
    Dim UsedButtons As Integer
    Dim UsedLabels As Integer
    Dim UsedFields As Integer
    Dim UseIniSection As String
    Dim PanelIndex As Integer
    Dim LeftPos As Long
    Dim i As Long
    PanelIndex = CurPanelIndex + MaxMainRows
    If PanelIndex > fraTopButtonPanel.UBound Then
        Load fraTopButtonPanel(PanelIndex)
        Set fraTopButtonPanel(PanelIndex).Container = TopPanel
    End If
    UsedButtons = 0
    For i = 0 To chkWork.UBound
        If chkWork(i).Tag <> "" Then UsedButtons = UsedButtons + 1
    Next
    UsedLabels = 0
    For i = 0 To lblContext.UBound
        If lblContext(i).Tag <> "" Then UsedLabels = UsedLabels + 1
    Next
    UsedFields = 0
    For i = 0 To txtEdit.UBound
        If txtEdit(i).Tag <> "" Then UsedFields = UsedFields + 1
    Next
    
    UseIniSection = GetIniEntry(myIniFullName, IniSection, "", "BUTTON_SECTION", IniSection)
    PanelTag = CStr(UsedButtons)
    UsedButtons = UsedButtons - 1
    UsedLabels = UsedLabels - 1
    UsedFields = UsedFields - 1
    IniKeyWord = "BUTTONS_ROW_" & CStr(CurPanelIndex)
    ButtonList = GetIniEntry(myIniFullName, UseIniSection, "", IniKeyWord, DefaultList)
    FlightPanel.Visible = False
    If InStr(ButtonList, "FLIGHT_PANEL") > 0 Then
        ButtonType = GetRealItem(ButtonList, 0, ",")
        ButtonCapt = GetRealItem(ButtonList, 1, ",")
        ButtonFunc = GetRealItem(ButtonList, 2, ",")
        ButtonParam = GetRealItem(ButtonList, 3, ",")
        FlightPanel.Top = 0
        FlightPanel.Left = 0
        FlightPanel.Visible = True
        fraTopButtonPanel(PanelIndex).Width = 0
        fraTopButtonPanel(PanelIndex).Tag = ButtonList
        fraTopButtonPanel(PanelIndex).Visible = False
        TopPanel.Tag = CStr(PanelIndex) & ",-1"
    Else
        i = 0
        LeftPos = 0
        ButtonDefine = GetRealItem(ButtonList, i, "|")
        While ButtonDefine <> ""
            ButtonType = GetRealItem(ButtonDefine, 0, ",")
            ButtonCapt = GetRealItem(ButtonDefine, 1, ",")
            ButtonFunc = GetRealItem(ButtonDefine, 2, ",")
            ButtonParam = GetRealItem(ButtonDefine, 3, ",")
            Select Case ButtonType
                Case "B"    'Normal Button (CheckBox)
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 1035
                    chkWork(UsedButtons).Caption = ButtonCapt
                    chkWork(UsedButtons).Tag = ButtonFunc & "," & ButtonType & "," & ButtonParam
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                    If ButtonFunc = "BOOKMARK" Then
                        BookMarkButtonIdx = UsedButtons
                        chkWork(UsedButtons).Enabled = False
                    End If
                Case "T", "P"
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 525
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 0, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 0, ";") & "," & ButtonType & "," & CStr(UsedButtons + 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 510
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 1, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 1, ";") & "," & ButtonType & "," & CStr(UsedButtons - 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                Case "D", "I"   'Double Buttons
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 720
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 0, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 0, ";") & "," & ButtonType & "1," & CStr(UsedButtons + 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 315
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 1, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 1, ";") & "," & ButtonType & "2," & CStr(UsedButtons - 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                Case "L"
                    UsedLabels = UsedLabels + 1
                    If UsedLabels > lblContext.UBound Then Load lblContext(UsedLabels)
                    Set lblContext(UsedLabels).Container = fraTopButtonPanel(PanelIndex)
                    lblContext(UsedLabels).Top = 0
                    lblContext(UsedLabels).Left = LeftPos
                    lblContext(UsedLabels).Caption = ButtonCapt
                    lblContext(UsedLabels).Tag = ButtonFunc & "," & ButtonType
                    lblContext(UsedLabels).Visible = True
                    LeftPos = LeftPos + lblContext(UsedLabels).Width + 15
                Case "F"
                    UsedFields = UsedFields + 1
                    If UsedFields > txtEdit.UBound Then Load txtEdit(UsedFields)
                    Set txtEdit(UsedFields).Container = fraTopButtonPanel(PanelIndex)
                    txtEdit(UsedFields).Top = 0
                    txtEdit(UsedFields).Left = LeftPos
                    txtEdit(UsedFields).Text = ButtonCapt
                    txtEdit(UsedFields).Tag = ButtonFunc & "," & ButtonType
                    txtEdit(UsedFields).Visible = True
                    LeftPos = LeftPos + txtEdit(UsedFields).Width + 15
                Case "FS"
                    Set FontSliderPanel.Container = fraTopButtonPanel(PanelIndex)
                    FontSliderPanel.Left = LeftPos
                    FontSliderPanel.Top = 0
                    FontSliderPanel.Visible = True
                    LeftPos = LeftPos + FontSliderPanel.Width + 15
                Case Else
            End Select
            i = i + 1
            ButtonDefine = GetRealItem(ButtonList, i, "|")
        Wend
        If CurPanelIndex = 0 Then
            'To be extended for other types as well
            MaxMainButtonIdx = UsedButtons
        End If
        If LeftPos > 150 Then fraTopButtonPanel(PanelIndex).Width = LeftPos - 15
        fraTopButtonPanel(PanelIndex).Tag = PanelTag & "," & CStr(UsedButtons)
        TopPanel.Tag = CStr(PanelIndex) & ",-1"
    End If
End Sub

Private Sub cboCedaCmd_Change(Index As Integer)
    Dim tmpText As String
    Dim tmpLine As Integer
    tmpText = cboCedaCmd(Index).Text
    tmpLine = cboCedaCmd(Index).ListIndex
End Sub

Private Sub cboCedaCmd_Click(Index As Integer)
    Dim tmpText As String
    Dim tmpCfgSection As String
    Dim tmpLine As Integer
    Dim tmpCfgKey As String
    Dim tmpCfgDat As String
    tmpText = cboCedaCmd(Index).Text
    tmpLine = cboCedaCmd(Index).ListIndex + 1
    tmpCfgSection = cboCedaCmd(Index).Tag
    tmpCfgKey = tmpCfgSection & "_BOX_" & CStr(tmpLine)
    txtCedaCmd(0).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "CMD", "")
    txtCedaCmd(1).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "TWS", "")
    txtCedaCmd(2).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "TBL", "")
    txtCedaCmd(3).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "SEL", "")
    txtCedaCmd(4).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "FLD", "")
    txtCedaCmd(5).Text = GetIniEntry(myIniFullName, tmpCfgKey, "", "DAT", "")
End Sub

Private Sub chkAppl_Click(Index As Integer)
    Dim myControls() As Control
    Dim CompLine As String
    Dim CompList As String
    Dim tmpData As String
    Dim tmpTag As String
    If chkAppl(Index).Value = 1 Then chkAppl(Index).BackColor = LightGreen Else chkAppl(Index).BackColor = MyOwnButtonFace
    tmpTag = Trim(chkAppl(Index).Tag)
    Select Case Index
        Case 0  'Close/Exit
            If chkAppl(Index).Value = 1 Then
                If (chkTerminate.Value = 0) And (ApplShortCode = "FDC") Then
                    chkAppl(Index).Value = 0
                    If tmpTag = "ICON" Then
                        Me.WindowState = vbMinimized
                    ElseIf tmpTag = "HIDE" Then
                        Me.Hide
                    ElseIf tmpTag = "TRAY" Then
                        Me.Icon = MyOwnForm.MyIcons(0).Picture
                        Hook Me.hwnd   ' Set up our handler
                        AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, "A/C Change Monitor"
                        ApplIsInTray = True
                        Me.Hide
                    Else
                        Me.WindowState = vbMinimized
                    End If
                Else
                    tmpData = Me.Tag
                    If tmpData = "" Then
                        If (chkAppl(Index).Tag = "EXIT") Or (chkTerminate.Value = 1) Then
                            ShutDownApplication True
                        Else
                            ShutDownApplication False
                        End If
                        chkAppl(Index).Value = 0
                        chkTerminate.Value = 0
                    Else
                        Unload Me
                    End If
                End If
            End If
        Case 1  'About
            If chkAppl(Index).Value = 1 Then
                CompLine = "BcProxy.exe,"
                CompLine = CompLine & UfisServer.GetComponentVersion("BcProxy")
                CompList = CompList & CompLine & vbLf
                'CompLine = "UfisAppMng.exe,"
                'CompLine = CompLine & UfisServer.GetComponentVersion("ufisappmng")
                'CompList = CompList & CompLine & vbLf
                MyAboutBox.SetComponents myControls(), CompList
                If MainLifeStyle Then MyAboutBox.SetLifeStyle = True
                If GridLifeStyle Then MyAboutBox.VersionTab.LifeStyle = True
                MyAboutBox.Show , Me
                If OnTop.Value = 1 Then SetFormOnTop MyAboutBox, True
                chkAppl(Index).Value = 0
            End If
        Case 2  'Hidden (Help)
            If chkAppl(Index).Value = 1 Then
                'This enables Help with F1 key
                'But we can't manage the Ontop position
                'App.HelpFile = "c:\ufis\help\" & App.EXEName & ".chm"
                'DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
                ShowHtmlHelp Me, "", ""
                chkAppl(Index).Value = 0
            End If
        Case 3  'Hidden (Setup)
            'Still nothing
        Case 4  'Break
            RecoverFileWrite -1, "Break", True
            tmpData = "The module environment has been saved." & vbNewLine
            tmpData = tmpData & "Have a nice break and see you back soon ..."
            If (MyMsgBox.CallAskUser(0, 0, 0, "Application Control", tmpData, "write", "OK", UserAnswer) = 1) Then DoNothing
            ShutDownApplication False
        Case Else
            
    End Select
End Sub

Private Sub CheckIdentify()
    Dim FldName As String
    Dim FldItem As String
    Dim tmpAdid As String
    Dim tmpRkey As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim AdidCol As Long
    Dim RkeyCol As Long
    FldName = "RKEY"
    FldItem = TranslateFieldItems(FldName, FileData(0).LogicalFieldList)
    RkeyCol = Val(FldItem)
    FileData(0).IndexCreate FldName, RkeyCol
    FldItem = TranslateFieldItems(FldName, FileData(1).LogicalFieldList)
    RkeyCol = Val(FldItem)
    FileData(1).IndexCreate FldName, RkeyCol
    FileData(1).SetInternalLineBuffer True
    MaxLine = FileData(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData(0).GetFieldValue(CurLine, "ADID")
        Select Case tmpAdid
            Case "A"
                tmpRkey = FileData(0).GetFieldValue(CurLine, "RKEY")
                LineNo = Val(FileData(1).GetLinesByIndexValue("RKEY", tmpRkey, 0))
                If LineNo = 0 Then FileData(0).SetLineColor CurLine, vbBlack, vbYellow
            Case "B"
                FileData(0).SetLineColor CurLine, vbBlack, vbCyan
            Case Else
                FileData(0).SetLineColor CurLine, vbWhite, vbRed
        End Select
    Next
    FileData(1).SetInternalLineBuffer False
    FileData(0).SetInternalLineBuffer True
    MaxLine = FileData(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData(1).GetFieldValue(CurLine, "ADID")
        Select Case tmpAdid
            Case "B"
                FileData(1).SetLineColor CurLine, vbBlack, vbCyan
            Case "D"
                tmpRkey = FileData(1).GetFieldValue(CurLine, "RKEY")
                LineNo = Val(FileData(0).GetLinesByIndexValue("RKEY", tmpRkey, 0))
                If LineNo = 0 Then FileData(1).SetLineColor CurLine, vbBlack, vbGreen
            Case Else
                FileData(0).SetLineColor CurLine, vbWhite, vbRed
        End Select
    Next
    FileData(1).SetInternalLineBuffer False
    
    FileData(0).Refresh
End Sub

Private Sub chkCedaSend_Click(Index As Integer)
    Dim tmpCmd As String
    Dim tmpTws As String
    Dim tmpTbl As String
    Dim tmpSel As String
    Dim tmpFld As String
    Dim tmpDat As String
    Dim CedaResult As String
    Dim iRet As Integer
    If chkCedaSend(Index).Value = 1 Then
        chkCedaSend(Index).BackColor = LightGreen
        tmpCmd = txtCedaCmd(0).Text
        tmpTws = txtCedaCmd(1).Text
        tmpTbl = txtCedaCmd(2).Text
        tmpSel = txtCedaCmd(3).Text
        tmpFld = txtCedaCmd(4).Text
        tmpDat = txtCedaCmd(5).Text
        UfisServer.TwsCode = tmpTws
        If tmpCmd <> "" Then
            iRet = UfisServer.CallCeda(CedaResult, tmpCmd, tmpTbl, tmpFld, tmpDat, tmpSel, "", 0, True, False)
        End If
        chkCedaSend(Index).Value = 0
    Else
        chkCedaSend(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkDlyTick_Click(Index As Integer)
    'MsgBox DelayPanel.Tag
    If chkDlyTick(Index).Value = 1 Then
        txtDlyInput(Index).BackColor = vbWhite
        txtDlyInput(Index).Locked = False
        txtDlyInput(Index).SetFocus
    Else
        txtDlyInput(Index).Text = txtDlyInput(Index).Tag
        txtDlyInput(Index).BackColor = NormalGray
        txtDlyInput(Index).ForeColor = vbBlack
        txtDlyInput(Index).Locked = True
    End If
End Sub

Private Sub chkDlyTick_GotFocus(Index As Integer)
    lblDlyReason(Index).ForeColor = vbYellow
End Sub

Private Sub chkDlyTick_LostFocus(Index As Integer)
    lblDlyReason(Index).ForeColor = vbBlack
End Sub

Private Sub chkHide_Click(Index As Integer)
    If chkHide(Index).Value = 1 Then
        chkTabIsVisible(Index).Value = 0
        chkHide(Index).Value = 0
    End If
End Sub

Private Sub chkImpTyp_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpCode As String
    Dim LastIndex As Integer
    Dim i As Integer
    FullMaximizedTab = -1
    LeftMaximizedTab = -1
    RightMaximizedTab = -1
    tmpCode = GetRealItem(picBall(Index).Tag, 0, "|")
    If Left(tmpCode, 1) <> "Y" Then
        If chkImpTyp(Index).Value = 1 Then
            ApplicationIsReadyForUse = False
            SectionUsingSyncCursor = False
            chkImpTyp(Index).BackColor = vbYellow
            tmpTag = TopPanel.Tag
            LastIndex = Val(GetRealItem(tmpTag, 1, ","))
            StopNestedCalls = True
            If LastIndex >= 0 Then chkImpTyp(LastIndex).Value = 0
            StopNestedCalls = False
            For i = MaxMainButtonIdx + 1 To chkWork.UBound
                chkWork(i).Enabled = True
            Next
            Me.Refresh
            InitButtonSection Index
            tmpTag = TopPanel.Tag
            TopPanel.Tag = GetRealItem(tmpTag, 0, ",") & "," & CStr(Index)
            FontSlider_Scroll
            AdjustSplitters
            CurImpButtonIndex = Index
            ApplicationIsReadyForUse = True
            InitUfisIntro Me, WorkArea, 0, 0
            SetUfisIntroFrames False
            If chkTabIsVisible(0).Value = 1 Then
                'chkTabIsVisible(0).Value = 0
                'chkTabIsVisible(0).Value = 1
            Else
            End If
            'chkTabIsVisible_Click 0
            Form_Resize
            If AutoRestore Then
                PushWorkButton "READLOCAL", 1
            Else
                RecoverFileRead True
            End If
            Me.SetFocus
        Else
            ApplicationIsReadyForUse = False
            chkImpTyp(Index).BackColor = MyOwnButtonFace
            ResetMain
            TopPanel.Tag = "0,-1"
            If Not StopNestedCalls Then
                SetUfisIntroFrames True
                CurImpButtonIndex = -1
                ApplicationIsReadyForUse = True
            End If
        End If
    Else
        If chkImpTyp(Index).Value = 0 Then
            chkImpTyp(Index).BackColor = MyOwnButtonFace
            ApplicationIsReadyForUse = False
            SectionUsingSyncCursor = False
            tmpTag = TopPanel.Tag
            LastIndex = Val(GetRealItem(tmpTag, 1, ","))
            If (LastIndex >= 0) And (LastIndex <> Index) Then
                chkImpTyp(LastIndex).Value = 0
                tmpCode = GetRealItem(picBall(LastIndex).Tag, 0, "|")
                If Left(tmpCode, 1) = "Y" Then chkImpTyp(LastIndex).Value = 1
            End If
            For i = 0 To chkWork.UBound
                chkWork(i).Enabled = True
            Next
            InitButtonSection Index
            tmpTag = TopPanel.Tag
            TopPanel.Tag = GetRealItem(tmpTag, 0, ",") & "," & CStr(Index)
            FontSlider_Scroll
            AdjustSplitters
            CurImpButtonIndex = Index
            ApplicationIsReadyForUse = True
            Form_Resize
            RecoverFileRead True
            Me.SetFocus
        Else
            ApplicationIsReadyForUse = False
            chkImpTyp(Index).BackColor = vbYellow
            ResetMain
            TopPanel.Tag = "0,-1"
        End If
    End If
    If ApplShortCode = "FDC" Then
        chkUtc(0).Value = 1
        chkUtc(1).Value = 1
    End If
    Text2.Text = Command
    Text2.ZOrder
End Sub

Private Sub InitButtonSection(Index As Integer)
    Dim tmpSections As String
    Dim SectionKey As String
    Dim SectionGroups As String
    Dim tmpData As String
    Dim tmpValue As Long
    Dim CurItem As Long
    Dim CurIdx As Integer
    Dim ButtonRows As Integer
    Dim i As Integer
    
    TabArrFlightsIdx = -1
    TabDepFlightsIdx = -1
    TabArrCnxPaxIdx = -1
    TabDepCnxPaxIdx = -1
    
    GetApplPosSize chkImpTyp(Index).Tag, True
    WorkArea.Visible = False
    
    CurrentTabFont = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "SECTION_GRID_FONT", "Courier New")
    
    tmpSections = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "BUTTON_SECTION", chkImpTyp(Index).Tag)
    ButtonRows = Val(GetIniEntry(myIniFullName, tmpSections, "", "WORK_BUTTON_ROWS", "3"))
    For CurIdx = 1 To ButtonRows
        ArrangeWorkButtons CurIdx, chkImpTyp(Index).Tag, "", MaxMainButtonRow
    Next
    ArrangeTopArea chkImpTyp(Index).Tag
    tmpSections = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "BOTTOM_SECTION", chkImpTyp(Index).Tag)
    ArrangeBottomAraea tmpSections
    WorkArea.Visible = True
    Form_Resize
    Me.Refresh
    'WorkArea.Visible = False
    Me.Caption = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "APPL_TITLE", Me.Caption)
    DataAreaColor = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "WORK_AREA_COLOR", CStr(MainAreaColor)))
    If DataAreaColor <> MainAreaColor Then
        WorkAreaColor = DataAreaColor
    End If
    SplitPosH = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "H_SPLIT_PERCENT", "50"))
    SplitPosV = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "V_SPLIT_PERCENT", "50"))
    myIniSection = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "LAYOUT_SECTION", chkImpTyp(Index).Tag)
    If myIniSection <> "NONE" Then
        tmpSections = GetRealItem(myIniSection, 1, "|")
        If tmpSections = "" Then
            MainSections = Abs(Val(GetIniEntry(myIniFullName, myIniSection, "", "MAIN_SECTIONS", "1"))) - 1
            tmpSections = GetIniEntry(myIniFullName, myIniSection, "", "DATA_SECTIONS", "MAIN")
        Else
            MainSections = 0
            myIniSection = GetRealItem(myIniSection, 0, "|")
        End If
        
        tmpData = GetIniEntry(myIniFullName, myIniSection, "", "MINMAX_PANELS", "YES")
        If tmpData = "YES" Then ShowMinMaxPanels = True Else ShowMinMaxPanels = False
        
        ListOfCedaTimeFields = ""
        SectionGroups = GetIniEntry(myIniFullName, myIniSection, "", "SECTION_GROUP", "")
        If SectionGroups <> "" Then SectionGroups = SectionGroups & "|"
        CurItem = 0
        CurIdx = 0
        SectionKey = GetRealItem(tmpSections, CurItem, ",")
        AdjustSectionGroups -1, "-.-.-.-", SectionGroups, MainSections, tmpData
        While SectionKey <> ""
            If InStr(SectionGroups, SectionKey) = 0 Then SectionGroups = SectionGroups & SectionKey & ","
            InitTabLayout CurIdx, SectionKey, SectionGroups
            CurIdx = CurIdx + 1
            CurItem = CurItem + 1
            SectionKey = GetRealItem(tmpSections, CurItem, ",")
        Wend
        ListOfCedaTimeFields = Mid(ListOfCedaTimeFields, 2)
        
        DefineGroupLevels tmpSections, SectionGroups
    End If
        
    'Under Construction
    SystabActivated = False
    SystabPanel.Visible = False
    tmpData = GetIniEntry(myIniFullName, myIniSection, "", "SYSTAB_PANELS", "NO")
    If tmpData = "YES" Then
        SystabPanel.Visible = True
        SysTab.Visible = True
        SystabActivated = True
        Form_Resize
    End If
    
    RotationActivated = False
    tmpData = GetIniEntry(myIniFullName, myIniSection, "", "ROTATION_DATA", "NO")
    If tmpData = "YES" Then
        tmpData = GetIniEntry(myIniFullName, myIniSection, "", "ROTATIONPANEL", "NO")
        If tmpData = "YES" Then SystabPanel.Visible = True
        RotationActivated = True
        RotationData.Visible = True
        Form_Resize
    End If
    ConnexActivated = False
    tmpData = GetIniEntry(myIniFullName, myIniSection, "", "CONX_DATAGRID", "NO")
    If tmpData = "YES" Then
        tmpData = GetIniEntry(myIniFullName, myIniSection, "", "ROTATIONPANEL", "NO")
        If tmpData = "YES" Then SystabPanel.Visible = True
        ConnexActivated = True
        ConnexData.Visible = True
        Form_Resize
    End If
        
    InitFolderSections Index
    InitParentChildRelation
    
    If RotationActivated = True Then InitRotationGrid
    'If ConnexActivated = True Then InitConnexGrid
        
    fraVtSplitter(0).Height = Screen.Height
    If MainLifeStyle Then DrawBackGround fraVtSplitter(0), WorkAreaColor, False, True
    myIniSection = chkImpTyp(Index).Tag
    WorkArea.Visible = True
    StatusBar.ZOrder
    CheckReadOnlyMode
    Me.Refresh
    
    
    
    Set TabRotFlightsTab = RotationData
    Set TabCnxFlightsTab = ConnexData
    
    
End Sub

Private Sub InitParentChildRelation()
    Dim CurIdx As Integer
    Dim TabIdx As Integer
    Dim tmpSyncParent As String
    Dim tmpParentName As String
    Dim tmpParentKeys As String
    Dim tmpParentField As String
    Dim tmpChildName As String
    Dim tmpChildKeys As String
    Dim tmpChildField As String
    Dim tmpChildren As String
    Dim tmpCfgFrame As String
    Dim tmpCfgTabKey As String
    Dim tmpSyncFlightPanel As String
    Dim UsingParentSync As Boolean
    UsingParentSync = False
    'This function will be a replacement of the previous
    '"SYNC_FRAME1" feature because it is easier to configure.
    'Currently I don't have the time to re-implement everything
    'what the FRAME1 feature already does and thus I do the trick
    'to create the FRAME1 string internally as "configuration".
    'First we collect all Parent-Child relations from the ini file.
    For CurIdx = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(CurIdx).Visible = True Then
            tmpSyncParent = GetTabProperty(CurIdx, "SYNC_PARENT", 1, "")
            tmpSyncParent = Replace(tmpSyncParent, " ", "", 1, -1, vbBinaryCompare)
            If tmpSyncParent <> "" Then
                tmpParentKeys = GetItem(tmpSyncParent, 1, "=")
                tmpParentName = GetItem(tmpParentKeys, 1, ".")
                tmpParentField = GetItem(tmpParentKeys, 2, ".")
                tmpChildKeys = GetItem(tmpSyncParent, 2, "=")
                'ChildName might be just "ME", so we ignore it here
                'tmpChildName = GetItem(tmpChildKeys, 1, ".")
                tmpChildField = GetItem(tmpChildKeys, 2, ".")
                TabIdx = GetGridIdxBySectionName(tmpParentName)
                If TabIdx >= 0 Then
                    tmpChildName = GetTabProperty(CurIdx, "SECTION_KEY", 1, "")
                    tmpChildKeys = tmpChildName & "." & tmpChildField
                    tmpChildren = GetTabProperty(TabIdx, "MY_CHILDREN", 1, "")
                    tmpChildren = tmpChildren & tmpParentKeys & ":" & tmpChildKeys & "|"
                    SetTabProperty TabIdx, "MY_CHILDREN", tmpChildren, ""
                    'Create the child keys formatted as used internally
                    'and push them back into the properties
                    tmpChildKeys = tmpChildKeys & ":" & tmpParentKeys
                    SetTabProperty CurIdx, "SYNC_PARENT", tmpChildKeys, ""
                    UsingParentSync = True
                End If
            End If
        End If
    Next
    'Now we can run through all grids and collect
    'the configured relations in order to create
    'the FRAME1 string like a manually entered configuration.
    '1.) For each grid we first must synchronize all own children.
    '2.) Then we must synchronize the parent of the actual grid.
    '3.) Then we synchronize the children of the parent.
    '4.) Then we take the parent of the parent.
    '5.) And finally the children of the parent of the parent.
    If UsingParentSync = True Then
        For CurIdx = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(CurIdx).Visible = True Then
                tmpCfgFrame = ""
                
                '1.) For each grid we first must synchronize all own children.
                tmpChildren = GetTabProperty(CurIdx, "MY_CHILDREN", 1, "")
                If tmpChildren <> "" Then tmpCfgFrame = tmpCfgFrame & tmpChildren & "|"
                'Get my parent key relation
                tmpSyncParent = GetTabProperty(CurIdx, "SYNC_PARENT", 1, "")
                If tmpSyncParent <> "" Then
                    '2.) Then we must synchronize the parent of the actual grid.
                    tmpCfgFrame = tmpCfgFrame & tmpSyncParent & "|"
                    'now we need the parent grid properties
                    tmpParentKeys = GetItem(tmpSyncParent, 2, ":")
                    tmpParentName = GetItem(tmpParentKeys, 1, ".")
                    TabIdx = GetGridIdxBySectionName(tmpParentName)
                    If TabIdx >= 0 Then
                        '3.) Then we synchronize the children of the parent.
                        tmpChildren = GetTabProperty(TabIdx, "MY_CHILDREN", 1, "")
                        If tmpChildren <> "" Then tmpCfgFrame = tmpCfgFrame & tmpChildren & "|"
                        tmpSyncParent = GetTabProperty(TabIdx, "SYNC_PARENT", 1, "")
                        If tmpSyncParent <> "" Then
                            '4.) Then we take the parent of the parent.
                            tmpCfgFrame = tmpCfgFrame & tmpSyncParent & "|"
                            tmpParentKeys = GetItem(tmpSyncParent, 2, ":")
                            tmpParentName = GetItem(tmpParentKeys, 1, ".")
                            TabIdx = GetGridIdxBySectionName(tmpParentName)
                            If TabIdx >= 0 Then
                                '5.) And finally the children of the parent of the parent.
                                tmpChildren = GetTabProperty(TabIdx, "MY_CHILDREN", 1, "")
                                If tmpChildren <> "" Then tmpCfgFrame = tmpCfgFrame & tmpChildren & "|"
                            End If
                        End If
                    End If
                End If
                If tmpCfgFrame <> "" Then
                    'Now we must put the result into the configuration area
                    
                    tmpCfgTabKey = GetTabProperty(CurIdx, "SECTION_KEY", 1, "")
                    tmpCfgTabKey = tmpCfgTabKey & "_REFERENCE"
                    
                    'PROBLEM: Synchronization of the FlightDataPanel!
                    tmpSyncFlightPanel = HiddenData.GetConfigValues(tmpCfgTabKey, "SYNC")
                    tmpSyncFlightPanel = GetItem(tmpSyncFlightPanel, 1, "{")
                    If tmpSyncFlightPanel <> "" Then tmpCfgFrame = tmpSyncFlightPanel & "|" & tmpCfgFrame
                    
                    HiddenData.UpdateTabConfig tmpCfgTabKey, "SYNC", tmpCfgFrame
                    SectionUsingSyncCursor = True
                End If
            End If
        Next
    End If
    
End Sub

Private Function GetGridIdxBySectionName(GridName As String) As Integer
    'This function has been introduced for future use
    'Currently we just call the old version
    GetGridIdxBySectionName = GetTabIdxByName(GridName)
End Function
Private Sub AdjustSystabPanel()
    Dim NewSize As Long
    If RotationActivated = True Then
        NewSize = 0
        NewSize = NewSize + Val(GetItem(RotationData.HeaderLengthString, 1, ",")) * 15
        NewSize = NewSize + Val(GetItem(RotationData.HeaderLengthString, 2, ",")) * 15
        NewSize = NewSize + Val(GetItem(RotationData.HeaderLengthString, 3, ",")) * 15
        NewSize = NewSize + Val(GetItem(RotationData.HeaderLengthString, 4, ",")) * 15
        If RotationData.GetLineCount > 1 Then NewSize = NewSize + 16 * 15
        RotationData.Width = NewSize
        SystabPanel.Width = NewSize + 75
        Form_Resize
    ElseIf ConnexActivated = True Then
        NewSize = 0
        NewSize = NewSize + Val(GetItem(ConnexData.HeaderLengthString, 1, ",")) * 15
        NewSize = NewSize + Val(GetItem(ConnexData.HeaderLengthString, 2, ",")) * 15
        NewSize = NewSize + Val(GetItem(ConnexData.HeaderLengthString, 3, ",")) * 15
        NewSize = NewSize + Val(GetItem(ConnexData.HeaderLengthString, 4, ",")) * 15
        If ConnexData.GetLineCount > 1 Then NewSize = NewSize + 16 * 15
        ConnexData.Width = NewSize
        SystabPanel.Width = NewSize + 75
        Form_Resize
    End If
End Sub
Private Sub InitFolderSections(Index As Integer)
    Dim tmpCfgData As String
    myIniSection = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "FOLDER_SECTION", "")
    If myIniSection <> "" Then
        'This is related to the dialog mask
        'for configurable Ceda transactions
        '(Preparation of the Scenario Server)
        tmpCfgData = GetIniEntry(myIniFullName, myIniSection, "", "FOLDER_1_TYPE", "")
        If tmpCfgData <> "" Then
            fraFolderPanel(0).Top = WorkArea.Top
            fraFolderPanel(0).Left = WorkArea.Left
            fraFolderPanel(0).Height = WorkArea.Height
            fraFolderPanel(0).Width = WorkArea.Width
            fraFolderPanel(0).Visible = True
            fraFolderPanel(0).ZOrder
            FillComboBox cboCedaCmd(0), tmpCfgData
        End If
    End If
End Sub
Private Sub FillComboBox(CurCombo As ComboBox, CfgSection As String)
    Dim i As Integer
    Dim tmpCfgDat As String
    Dim tmpCfgKey As String
    CurCombo.Tag = CfgSection
    i = 0
    tmpCfgDat = "START"
    While tmpCfgDat <> ""
        i = i + 1
        tmpCfgKey = CfgSection & "_BOX_" & CStr(i)
        tmpCfgDat = GetIniEntry(myIniFullName, tmpCfgKey, "", "TEXT", "")
        If tmpCfgDat <> "" Then
            'tmpCfgDat = tmpCfgDat & Space(10) & "|" & tmpCfgKey & "|"
            CurCombo.AddItem tmpCfgDat
        End If
    Wend
End Sub
Private Sub DefineGroupLevels(SectionNames As String, SectionGroups As String)
    Dim tmpSecGroup As String
    Dim tmpTabGroup As String
    Dim tmpGrpName As String
    Dim tmpTabName As String
    Dim grpItm As Integer
    Dim tabItm As Integer
    Dim GrpMainText As String
    Dim GrpMembText As String
    Dim GrpMembList As String
    Dim GrpMainIdx As Integer
    Dim GrpMembIdx As Integer
    grpItm = 0
    tmpSecGroup = "START"
    While tmpSecGroup <> ""
        grpItm = grpItm + 1
        tmpSecGroup = GetItem(SectionGroups, grpItm, "|")
        If tmpSecGroup <> "" Then
            'Begin of a new group
            tmpGrpName = GetItem(tmpSecGroup, 1, ":")
            tmpTabGroup = GetItem(tmpSecGroup, 2, ":")
            If (InStr(tmpGrpName, "[") = 0) And (InStr(tmpGrpName, "]") = 0) Then
                If tmpTabGroup = "" Then
                    'No group leader, only members
                    GrpMainIdx = -1
                    GrpMainText = "-"
                    tmpTabGroup = tmpGrpName
                    tmpGrpName = ""
                End If
                If tmpGrpName <> "" Then
                    'Found the group leader
                    GrpMainIdx = CInt(GetRealItemNo(SectionNames, tmpGrpName))
                    If GrpMainIdx >= 0 Then GrpMainText = lblTabName(GrpMainIdx).Caption
                    FileData(GrpMainIdx).myTag = GrpMainText & "," & GrpMainText & "|" & CStr(GrpMainIdx) & ":"
                    GrpMembList = tmpGrpName
                End If
                If tmpTabGroup <> "" Then
                    'Now look for members
                    tabItm = 0
                    tmpTabName = "START"
                    While tmpTabName <> ""
                        tabItm = tabItm + 1
                        tmpTabName = GetItem(tmpTabGroup, tabItm, ",")
                        If tmpTabName <> "" Then
                            If (InStr(tmpTabName, "[") = 0) And (InStr(tmpTabName, "]") = 0) Then
                                GrpMembIdx = CInt(GetRealItemNo(SectionNames, tmpTabName))
                                If GrpMembIdx >= 0 Then
                                    GrpMembText = lblTabName(GrpMembIdx).Caption
                                    If GrpMainIdx >= 0 Then
                                        FileData(GrpMembIdx).myTag = GrpMainText & "," & GrpMembText & "|" & CStr(GrpMainIdx) & "|," & GrpMembList & "," & tmpTabGroup & ","
                                        FileData(GrpMainIdx).myTag = FileData(GrpMainIdx).myTag & CStr(GrpMembIdx) & ","
                                    Else
                                        FileData(GrpMembIdx).myTag = GrpMainText & "," & GrpMembText & "|" & CStr(GrpMainIdx) & "|," & tmpTabName & "," & tmpTabName & ","
                                    End If
                                End If
                            End If
                        End If
                    Wend
                End If
                If GrpMainIdx >= 0 Then FileData(GrpMainIdx).myTag = FileData(GrpMainIdx).myTag & "|," & GrpMembList & "," & tmpTabGroup & ","
            End If
        End If
    Wend
End Sub
Private Sub ArrangeTopArea(ApplType As String)
    Dim TopPos As Long
    Dim i As Integer
    Dim j As Integer
    Dim MaxI As Integer
    Dim tmpTag As String
    Dim tmpParam As String
    Dim tmpData As String
    Dim iFrom As Integer
    Dim iTo As Integer
    TopPos = 30
    iFrom = 0
    If MaxMainButtonRow > 0 Then
        fraTopButtonPanel(0).Top = TopPos
        TopPos = fraTopButtonPanel(0).Top + fraTopButtonPanel(0).Height + 45
        iFrom = 1
    End If

    For i = iFrom To MaxMainButtonRow
        fraTopButtonPanel(i).Top = TopPos
        TopPos = fraTopButtonPanel(i).Top + fraTopButtonPanel(i).Height + 15
    Next
    
    MaxI = Val(GetRealItem(TopPanel.Tag, 0, ","))
    For i = MaxMainButtonRow + 1 To MaxI
        tmpTag = fraTopButtonPanel(i).Tag
        If InStr(tmpTag, "FLIGHT_PANEL") > 0 Then
            tmpParam = GetItem(tmpTag, 4, ",")
            If tmpParam = "TOP" Then TopPos = 30
            TopPos = TopPos - 15
            fraTopButtonPanel(i).Visible = False
            FlightPanel.Top = TopPos
            FlightPanel.Visible = True
            FlightPanel.Width = 60000
            DrawBackGround FlightPanel, WorkAreaColor, True, True
            For j = 0 To FlightPanelCover.UBound
                DrawBackGround FlightPanelCover(j), WorkAreaColor, True, True
            Next
            FlightPanel.ZOrder
            TopPos = TopPos + FlightPanel.Height
        Else
            'TopPos = TopPos - 15
            iFrom = Val(GetRealItem(tmpTag, 0, ","))
            iTo = Val(GetRealItem(tmpTag, 1, ","))
            If iTo >= iFrom Then
                fraTopButtonPanel(i).Top = TopPos
                fraTopButtonPanel(i).Height = chkWork(0).Height
                fraTopButtonPanel(i).Visible = True
                TopPos = fraTopButtonPanel(i).Top + fraTopButtonPanel(i).Height + 15
            End If
        End If
    Next
    TopPanel.Height = TopPos + 75
    TopPanel.Width = 60000
    If MainLifeStyle Then DrawBackGround TopPanel, WorkAreaColor, True, True
    
    'QUICK HACK --------------------
    If ApplShortCode = "FDC" Then TopPanel.Height = 0
    tmpData = GetIniEntry(myIniFullName, ApplType, "", "HIDE_BUTTONROW", "NO")
    If tmpData = "YES" Then TopPanel.Height = 0
    WorkArea.Top = TopPanel.Height
    WorkArea.Height = BottomPanel.Top - WorkArea.Top
    DelayPanel.Top = WorkArea.Top
    DelayPanel.Height = WorkArea.Height
    SystabPanel.Top = WorkArea.Top
    SystabPanel.Height = WorkArea.Height
End Sub
Private Sub ArrangeBottomAraea(CfgSection As String)
    Dim tmpData As String
    tmpData = GetIniEntry(myIniFullName, CfgSection, "", "SHOW_BOTTOM_PANEL", "NO")
    If Left(tmpData, 1) = "Y" Then
        BottomPanel.Visible = True
        BottomPanel.Width = 60000
        If MainLifeStyle Then DrawBackGround BottomPanel, WorkAreaColor, True, True
    Else
        BottomPanel.Visible = False
    End If
End Sub

Private Sub InitTabLayout(Index As Integer, SectionKey As String, SectionGroups As String)
    Dim tmpHeaderTitle As String
    Dim tmpHeaderWidth As String
    Dim tmpHeaderColor As String
    Dim tmpSqlConfig As String
    Dim tmpFieldNames As String
    Dim tmpFields As String
    Dim tmpKeys As String
    Dim tmpValues As String
    Dim tmpSqlFields As String
    Dim tmpSqlKey As String
    Dim tmpSystCols As String
    Dim TabCfgLine As String
    Dim tmpSortKeys As String
    Dim tmpStr As String
    Dim tmpRef As String
    Dim tmpItem As String
    Dim tmpPrnt As String
    Dim tmpGrpItems As String
    Dim tmpDataSource As String
    Dim FillMethod As String
    Dim tmpItem1 As String
    Dim tmpItem2 As String
    Dim ItemNo As Long
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim ReturnColor As Long
    Dim i As Integer
    ReturnColor = -1
    If Index > chkTabIsVisible.UBound Then
        Load TabPanel(Index)
        Load TabCover(Index)
        Load FileData(Index)
        Load TabLookUp(Index)
        Load txtTabEditLookUp(Index)
        Load SelTab1(Index)
        Load SelTab2(Index)
        Load TabPropBag(Index)
        Load fraHzSplitter(Index)
        Load fraCrSplitter(Index)
        Load chkTabIsVisible(Index)
        Load chkTabCoverIsVisible(Index)
        Load lblTabName(Index)
        Load lblTabShadow(Index)
        Load MinMaxPanel(Index)
        Load fraMaxButton(Index)
        Load chkHide(Index)
        Load chkMax(Index)
        Load chkMaxSec(Index)
        Load lblTabPos(Index)
        Load lblTabCtl(Index)
        Load lblTabUtl(Index)
        Load lblTabCnt(Index)
        Load imgCurPos(Index)
        Load imgAnyPic1(Index)
        Load imgAnyPic2(Index)
        Load fraSigns(Index)
        Load BcSign1(Index)
        Load BcSign2(Index)
        Load fraTabCaption(Index)
        Load lblTabCaption(Index)
        Load lblTabCaptionShadow(Index)
    End If
        
    chkTabIsVisible(Index).Value = 0
    AdjustSectionGroups Index, SectionKey, SectionGroups, MainSections, tmpGrpItems
    
    If Index <= FileData.UBound Then
        Set TabPanel(Index).Container = WorkArea
        Set TabCover(Index).Container = WorkArea
        Set TabPropBag(Index).Container = TabCover(Index)
        Set FileData(Index).Container = TabPanel(Index)
        Set TabLookUp(Index).Container = TabPanel(Index)
        Set txtTabEditLookUp(Index).Container = TabPanel(Index)
        Set MinMaxPanel(Index).Container = TabPanel(Index)
        Set fraMaxButton(Index).Container = MinMaxPanel(Index)
        Set chkHide(Index).Container = fraMaxButton(Index)
        Set chkMax(Index).Container = fraMaxButton(Index)
        Set chkMaxSec(Index).Container = fraMaxButton(Index)
        Set lblTabPos(Index).Container = fraMaxButton(Index)
        Set lblTabCtl(Index).Container = fraMaxButton(Index)
        Set lblTabUtl(Index).Container = fraMaxButton(Index)
        Set lblTabCnt(Index).Container = fraMaxButton(Index)
        Set imgCurPos(Index).Container = fraMaxButton(Index)
        Set imgAnyPic1(Index).Container = fraMaxButton(Index)
        Set imgAnyPic2(Index).Container = fraMaxButton(Index)
        Set fraSigns(Index).Container = fraMaxButton(Index)
        Set BcSign1(Index).Container = fraSigns(Index)
        Set BcSign2(Index).Container = fraSigns(Index)
        Set fraTabCaption(Index).Container = MinMaxPanel(Index)
        Set lblTabCaption(Index).Container = fraTabCaption(Index)
        Set lblTabCaptionShadow(Index).Container = fraTabCaption(Index)
        
        TabPanel(Index).ZOrder
        TabCover(Index).ZOrder
        fraHzSplitter(Index).ZOrder
        fraCrSplitter(Index).ZOrder
        imgAnyPic1(Index).ZOrder
        imgAnyPic2(Index).ZOrder
        imgCurPos(Index).ZOrder
        lblTabName(Index).ZOrder
        chkTabIsVisible(Index).ZOrder
        fraTabCaption(Index).ZOrder
        lblTabCaptionShadow(Index).ZOrder
        lblTabCaption(Index).ZOrder
        
        TabPropBag(Index).Visible = False
        TabPropBag(Index).ResetContent
        TabPropBag(Index).HeaderString = "KeyCode,Configuration,Values"
        TabPropBag(Index).AutoSizeByHeader = True
        TabPropBag(Index).SetFieldSeparator Chr(15)
        
        TabPanel(Index).Visible = False
        TabCover(Index).Visible = False
        
        FileData(Index).Visible = True
        fraMaxButton(Index).Visible = True
        chkHide(Index).Visible = True
        chkMax(Index).Visible = True
        chkMaxSec(Index).Visible = True
        lblTabPos(Index).Visible = True
        lblTabCtl(Index).Visible = True
        lblTabUtl(Index).Visible = True
        lblTabCnt(Index).Visible = True
        imgAnyPic1(Index).Visible = True
        imgAnyPic2(Index).Visible = False
        imgCurPos(Index).Visible = False
        fraSigns(Index).Visible = True
        BcSign1(Index).Visible = True
        BcSign2(Index).Visible = True
        fraTabCaption(Index).Visible = True
        lblTabCaption(Index).Visible = True
        lblTabCaptionShadow(Index).Visible = True
        If MainLifeStyle Then
            fraHzSplitter(Index).Width = 60000
            DrawBackGround fraHzSplitter(Index), WorkAreaColor, True, True
            ReturnColor = DrawBackGround(fraCrSplitter(Index), WorkAreaColor, True, True)
        End If
        
        If Index <= MainSections Then
            chkTabIsVisible(Index).Tag = "L,-1"
        Else
            chkTabIsVisible(Index).Tag = "R,-1"
        End If
        
        MinMaxPanel(Index).Visible = ShowMinMaxPanels
        
        FileData(Index).ResetContent
        MaxCol = FileData(Index).GetColumnCount - 1
        For CurCol = 0 To MaxCol
            FileData(Index).DateTimeResetColumn CurCol
        Next
        If ShowMinMaxPanels = False Then FileData(Index).Top = 0
        FileData(Index).FontName = CurrentTabFont
        FileData(Index).FontSize = FontSlider.Value + 6
        FileData(Index).HeaderFontSize = FontSlider.Value + 6
        FileData(Index).LineHeight = FontSlider.Value + 8
        FileData(Index).LeftTextOffset = 1
        FileData(Index).SetTabFontBold True
        FileData(Index).MainHeader = True
        'FileData(Index).GridlineColor = vbBlack
        If GridLifeStyle Then
            FileData(Index).LifeStyle = True
            FileData(Index).CursorLifeStyle = True
            FileData(Index).SelectColumnBackColor = RGB(235, 235, 255)
            FileData(Index).SelectColumnTextColor = vbBlack
            'FileData(Index).ColumnSelectionLifeStyle = True
        End If
        SelTab1(Index).ResetContent
        SelTab1(Index).HeaderString = "LINE,COLS"
        SelTab2(Index).ResetContent
        SelTab2(Index).HeaderString = "LINE,COLS"
        lblTabName(Index).Caption = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SHORT_TITLE", SectionKey)
        lblTabShadow(Index).Caption = lblTabName(Index).Caption
        lblTabCaption(Index).Caption = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_TAB_CAPTION", lblTabName(Index).Caption)
        lblTabCaptionShadow(Index).Caption = lblTabCaption(Index).Caption
        tmpStr = lblTabCaption(Index).Caption
        tmpStr = Replace(tmpStr, "&&", "&", 1, -1, vbBinaryCompare)
        tmpStr = Replace(tmpStr, ":", "", 1, -1, vbBinaryCompare)
        lblTabName(Index).ToolTipText = "Show/Hide: " & tmpStr
        fraTabCaption(Index).Width = lblTabCaptionShadow(Index).Left + lblTabCaptionShadow(Index).Width + 30
        
        InitTabProperty Index, SectionKey, "SECTION_KEY", SectionKey
        InitTabProperty Index, SectionKey, "INDEX_NAMES", ""
        InitTabProperty Index, SectionKey, "SYNC_PARENT", ""
        InitTabProperty Index, SectionKey, "MY_CHILDREN", ""
        InitTabProperty Index, SectionKey, "MARK_FRAME1", ""
        InitTabProperty Index, SectionKey, "LINE_COLORS", ""
        InitTabProperty Index, SectionKey, "REPLACE_VAL", ""
        InitTabProperty Index, SectionKey, "LINE_COLORS", ""
        
        InitAftVersStatusInfo Index, SectionKey
        
        tmpSortKeys = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SORT_FIELDS", "")
        tmpFieldNames = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_NAMES", "????")
        tmpFieldNames = AddAftVersFields(Index, tmpFieldNames, 0)
        If tmpFieldNames = "????" Then tmpFieldNames = "NO CONFIGURATION FOUND!"
        tmpSystCols = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYSTEM_COLS", "NO")
        If tmpSystCols = "YES" Then
            FileData(Index).LogicalFieldList = "'NO','ID','DO'," & tmpFieldNames & ",'RM','SY','ST'"
            tmpStr = CStr(ItemCount(tmpFieldNames, ","))
            tmpHeaderTitle = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_MAIN_HEADER", lblTabName(Index).Caption)
            tmpHeaderWidth = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_SIZE", tmpStr)
            tmpHeaderColor = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_COLOR", "")
            If tmpHeaderTitle <> "" Then
                tmpHeaderWidth = "3," & tmpHeaderWidth & ",3"
                tmpHeaderTitle = "Status," & tmpHeaderTitle & ",System"
                If tmpHeaderColor <> "" Then tmpHeaderColor = "12632256," & tmpHeaderColor & ",12632256"
            End If
            FileData(Index).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, CurrentTabFont
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_GRID_HEADER", tmpFieldNames)
            FileData(Index).HeaderString = "Line,I,A," & tmpStr & ",Remark,System Cells,System Status"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_WIDTH", "10")
            FileData(Index).HeaderLengthString = "5,1,1," & tmpStr & ",10,10,10"
            FileData(Index).ColumnWidthString = "5,1,1," & tmpStr & ",10,10,10"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_ALIGN", "")
            tmpStr = Replace(tmpStr, " ", "", 1, -1, vbBinaryCompare)
            If tmpStr <> "" Then
                tmpStr = "R,L,L," & tmpStr & ",L,L,L"
                FormatTimeFields FileData(Index), tmpStr
                tmpStr = Replace(tmpStr, "CDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CD", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDO", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTO", "C", 1, -1, vbBinaryCompare)
                FileData(Index).ColumnAlignmentString = tmpStr
                FileData(Index).HeaderAlignmentString = tmpStr
            End If
        Else
            FileData(Index).LogicalFieldList = tmpFieldNames & ",'S1','C1','S2','C2','S3','C3','S4','C4'"
            tmpStr = CStr(ItemCount(tmpFieldNames, ","))
            tmpHeaderTitle = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_MAIN_HEADER", lblTabName(Index).Caption)
            tmpHeaderTitle = AddAftVersFields(Index, tmpHeaderTitle, -1)
            tmpHeaderWidth = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_SIZE", tmpStr)
            tmpHeaderWidth = AddAftVersFields(Index, tmpHeaderWidth, -2)
            tmpHeaderColor = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_COLOR", "")
            tmpHeaderColor = AddAftVersFields(Index, tmpHeaderColor, -3)
            FileData(Index).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, CurrentTabFont
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_GRID_HEADER", tmpFieldNames)
            'QuickHack
            tmpStr = Replace(tmpStr, "@", Space(100), 1, -1, vbBinaryCompare)
            tmpStr = AddAftVersFields(Index, tmpStr, 3)
            FileData(Index).HeaderString = tmpStr & ",1,C1,2,C2,3,C3,4,C4"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_WIDTH", "10")
            tmpStr = AddAftVersFields(Index, tmpStr, 1)
            'FileData(Index).HeaderLengthString = tmpStr & ",-1,-1,-1,-1,-1,-1,-1,-1"
            FileData(Index).HeaderLengthString = tmpStr & ",10,10,10,10,10,10,10,10"
            FileData(Index).ColumnWidthString = tmpStr & ",10,10,10,10,10,10,10,10"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_ALIGN", "")
            tmpStr = AddAftVersFields(Index, tmpStr, 2)
            tmpStr = Replace(tmpStr, " ", "", 1, -1, vbBinaryCompare)
            If tmpStr <> "" Then
                FormatTimeFields FileData(Index), tmpStr
                tmpStr = Replace(tmpStr, "CDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CD", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDO", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTO", "C", 1, -1, vbBinaryCompare)
                FileData(Index).ColumnAlignmentString = tmpStr & ",L,L,L,L,L,L,L,L"
                FileData(Index).HeaderAlignmentString = tmpStr & ",L,L,L,L,L,L,L,L"
            End If
            tmpHeaderWidth = tmpHeaderWidth & ",8"
            tmpHeaderTitle = tmpHeaderTitle & ",System"
            If tmpHeaderColor <> "" Then tmpHeaderColor = tmpHeaderColor & ",12632256"
        End If
        
        FileData(Index).SetMainHeaderValues tmpHeaderWidth, tmpHeaderTitle, tmpHeaderColor
        FileData(Index).CreateCellObj "ColMarker", LightestYellow, vbBlack, (FontSlider.Value + 6), False, False, True, 0, CurrentTabFont
        FileData(Index).CreateCellObj "Marker", vbBlue, vbWhite, (FontSlider.Value + 6), False, False, True, 0, CurrentTabFont
        FileData(Index).CreateDecorationObject "DecoMarkerLB", "L,L,T,B,R", "-1,2,2,2,2", Str(LightestBlue) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(DarkGray) & "," & Str(DarkGray)
        FileData(Index).CreateDecorationObject "DecoMarkerLR", "L,L,T,B,R", "-1,2,2,2,2", Str(LightestRed) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(DarkGray) & "," & Str(DarkGray)
        FileData(Index).CreateDecorationObject "BookMark", "BTL", "8", Trim(Str(LightRed))
        FileData(Index).ShowHorzScroller True
        FileData(Index).AutoSizeByHeader = True
        FileData(Index).AutoSizeColumns
        
        TabLookUp(Index).ResetContent
        TabLookUp(Index).FontName = CurrentTabFont
        TabLookUp(Index).FontSize = FontSlider.Value + 6
        TabLookUp(Index).HeaderFontSize = FontSlider.Value + 6
        TabLookUp(Index).LineHeight = FontSlider.Value + 8
        TabLookUp(Index).LeftTextOffset = 1
        TabLookUp(Index).SetTabFontBold True
        TabLookUp(Index).MainHeader = False
        TabLookUp(Index).MainHeaderOnly = True
        TabLookUp(Index).GridlineColor = vbBlack
        If GridLifeStyle Then
            TabLookUp(Index).LifeStyle = True
            TabLookUp(Index).CursorLifeStyle = True
            TabLookUp(Index).SelectColumnBackColor = RGB(235, 235, 255)
            TabLookUp(Index).SelectColumnTextColor = vbBlack
        End If
        TabLookUp(Index).HeaderString = FileData(Index).HeaderString
        TabLookUp(Index).HeaderLengthString = FileData(Index).HeaderLengthString
        TabLookUp(Index).LogicalFieldList = FileData(Index).LogicalFieldList
        TabLookUp(Index).Height = TabLookUp(Index).LineHeight * 15
        tmpStr = CreateEmptyLine(TabLookUp(Index).LogicalFieldList)
        TabLookUp(Index).ColumnAlignmentString = Replace(tmpStr, ",", "C,", 1, -1, vbBinaryCompare) & "C"
        TabLookUp(Index).CreateDecorationObject "BookMark", "BTL", "8", Trim(Str(LightRed))
                
        If ReturnColor >= 0 Then FileData(Index).EmptyAreaRightColor = ReturnColor
        chkTabIsVisible(Index).Visible = True
        lblTabName(Index).Visible = True
        lblTabName(Index).Refresh
        lblTabShadow(Index).Visible = True
        lblTabShadow(Index).Refresh
        chkTabIsVisible(Index).Enabled = True
        chkTabIsVisible(Index).Value = 1
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ACTIVE_MODE", "YES")
        If Left(tmpStr, 1) = "N" Then
            chkTabIsVisible(Index).Value = 0
            chkTabIsVisible(Index).Enabled = True
        End If
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HIDDEN_MODE", "NO")
        If Left(tmpStr, 1) = "Y" Then
            chkTabIsVisible(Index).Value = 0
            chkTabIsVisible(Index).Enabled = False
        End If
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_BOX_ENABLED", "YES")
        If Left(tmpStr, 1) = "N" Then
            chkTabIsVisible(Index).Enabled = False
        End If
        FileData(Index).myName = "MAIN_FILEDATA_" & CStr(Index)
        BcSign1(Index).Tag = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FLIGHT_CODE", "")
                
        tmpSqlFields = ""
        tmpSqlKey = ""
        tmpSqlConfig = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_AODB_READER", "")
        If tmpSqlConfig <> "" Then
            ItemNo = 1
            tmpFields = GetRealItem(tmpSqlConfig, ItemNo, "|")
            tmpKeys = GetRealItem(tmpSqlConfig, ItemNo + 1, "|")
            While (tmpFields <> "") Or (tmpKeys <> "")
                If (tmpFields = "") Or (tmpFields = "?") Then tmpFields = FileData(Index).LogicalFieldList
                tmpSqlFields = tmpSqlFields & tmpFields & Chr(14)
                tmpSqlKey = tmpSqlKey & tmpKeys & Chr(14)
                ItemNo = ItemNo + 2
                tmpFields = GetRealItem(tmpSqlConfig, ItemNo, "|")
                tmpKeys = GetRealItem(tmpSqlConfig, ItemNo + 1, "|")
            Wend
        End If
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_DIALOG_ZONE", "")
        If tmpStr <> "" Then CreateDelayPanel Index, tmpStr, SectionKey
        
        If tmpSqlFields <> "" Then tmpSqlFields = Left(tmpSqlFields, Len(tmpSqlFields) - 1)
        If tmpSqlKey <> "" Then tmpSqlKey = Left(tmpSqlKey, Len(tmpSqlKey) - 1)
        TabCfgLine = FileData(Index).myName & Chr(15)
        TabCfgLine = TabCfgLine & "0" & Chr(15)
        TabCfgLine = TabCfgLine & "V" & Chr(15)
        TabCfgLine = TabCfgLine & "IMP" & Chr(15)
        TabCfgLine = TabCfgLine & "SYSTEM" & Chr(15)
        TabCfgLine = TabCfgLine & GetRealItem(tmpSqlConfig, 0, "|") & Chr(15)
        TabCfgLine = TabCfgLine & tmpSqlFields & Chr(15)
        TabCfgLine = TabCfgLine & tmpSqlKey & Chr(15)
        TabCfgLine = TabCfgLine & FileData(Index).LogicalFieldList & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_CAPT", "")
        tmpItem = GetRealItem(tmpStr, 1, "|")
        If tmpItem = "" Then tmpStr = tmpStr & "|" & tmpStr
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ACTION_CODE", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ACTION_CALL", tmpStr)
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_TYPE", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDIT_FIELDS", "")
        tmpItem = ""
        tmpPrnt = ""
        If tmpStr <> "" Then
            tmpItem = GetRealItem(tmpStr, 1, "|")
            tmpPrnt = GetRealItem(tmpStr, 2, "|")
            tmpStr = GetRealItem(tmpStr, 0, "|")
            If tmpItem = "" Then tmpItem = tmpStr
            SetTabEditColumns Index, tmpItem
            If tmpPrnt = "" Then tmpPrnt = tmpItem
        End If
        tmpStr = tmpStr & "|" & tmpItem & "|" & tmpSortKeys & "|" & tmpPrnt & "|" & "STIM,ETIM"
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_MAPP", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        TabCfgLine = TabCfgLine & SectionKey & Chr(15)
        TabCfgLine = TabCfgLine & "" & Chr(15)
        HiddenData.CheckTabConfig TabCfgLine
        
        tmpRef = SectionKey & "_REFERENCE"
        tmpFields = "TYPE" & Chr(15) & "CODE" & Chr(15) & "SYNC" & Chr(15) & "NAME" & Chr(15) & "FIDX" & Chr(15) & "SORT" & Chr(15) & "DTAB" & Chr(15) & "DFLD" & Chr(15) & "DSRC"
        tmpValues = "IMP" & Chr(15) & tmpRef & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYNC_FRAME1", "")
        tmpStr = tmpStr & "{" & GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYNC_FRAME2", "")
        If tmpStr <> "{" Then SectionUsingSyncCursor = True 'Else SectionUsingSyncCursor = False
        tmpStr = tmpStr & "{" & GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYNC_CURSOR", "")
        
        
        tmpValues = tmpValues & tmpStr & Chr(15)
        tmpValues = tmpValues & "MAIN_FILEDATA_" & CStr(Index) & Chr(15)
        tmpValues = tmpValues & CStr(Index) & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_AUTO_SORTER", "")
        tmpValues = tmpValues & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SECTION_GROUP", tmpGrpItems)
        tmpValues = tmpValues & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ALTER_COLOR", "")
        tmpValues = tmpValues & tmpStr & Chr(15)
        FillMethod = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FILL_METHOD", "")
        tmpStr = GetItem(FillMethod, 2, ":")
        If tmpStr = "LOG_SPLIT" Then
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_VALUE_WIDTH", "20")
            FillMethod = FillMethod & "|SIZE=" & tmpStr
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_IGNORE_VALU", "")
            FillMethod = FillMethod & "|IGNORE=" & tmpStr
        End If
        tmpValues = tmpValues & FillMethod & Chr(15)
        HiddenData.UpdateTabConfig tmpRef, tmpFields, tmpValues
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_BOOKMARK_ID", "")
        If tmpStr <> "" Then
            tmpItem1 = GetItem(tmpStr, 1, ",")
            tmpItem2 = GetItem(tmpStr, 2, ",")
            MyBookMarks.CloneTabLayout Val(tmpItem1) - 1, FileData(Index), tmpItem2
        End If
        Set SelTab1(Index).Container = TabCover(Index)
        CloneGridLayout SelTab1(Index), FileData(Index)
        SelTab1(Index).Visible = True
        
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYSTEM_CODE", "")
        Select Case tmpStr
            Case "ARRFLT"
                TabArrFlightsIdx = Index
                Set TabArrFlightsTab = FileData(TabArrFlightsIdx)
            Case "ARRCNXPAX"
                TabArrCnxPaxIdx = Index
                Set TabArrCnxPaxTab = FileData(TabArrCnxPaxIdx)
            Case "ARRCNXBAG"
                TabArrCnxBagIdx = Index
                Set TabArrCnxBagTab = FileData(TabArrCnxBagIdx)
            Case "ARRCNXULD"
                TabArrCnxUldIdx = Index
                Set TabArrCnxUldTab = FileData(TabArrCnxUldIdx)
            Case "DEPFLT"
                TabDepFlightsIdx = Index
                Set TabDepFlightsTab = FileData(TabDepFlightsIdx)
            Case "DEPCNXPAX"
                TabDepCnxPaxIdx = Index
                Set TabDepCnxPaxTab = FileData(TabDepCnxPaxIdx)
            Case "DEPCNXBAG"
                TabDepCnxBagIdx = Index
                Set TabDepCnxBagTab = FileData(TabDepCnxBagIdx)
            Case "DEPCNXULD"
                TabDepCnxUldIdx = Index
                Set TabDepCnxUldTab = FileData(TabDepCnxUldIdx)
            Case Else
        End Select
    
    End If
End Sub

Private Function AddAftVersFields(Index As Integer, ItemList As String, ForWhat As Integer)
    Dim Result As String
    Dim tmpVers As String
    Dim tmpChar As String
    Dim i As Integer
    Dim j As Integer
    Result = ItemList
    tmpVers = GetTabProperty(Index, "STATUS_VERS", 1, "")
    tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
    If tmpVers <> "" Then
        Result = ""
        j = Len(tmpVers)
        If ForWhat >= 0 Then
            For i = 1 To j
                tmpChar = Mid(tmpVers, i, 1)
                Select Case ForWhat
                    Case 0  'FIELD_NAMES
                        Result = Result & "'P" & tmpChar & "',"
                    Case 1  'FIELD_WIDTH
                        Result = Result & "10,"
                    Case 2  'FIELD_ALIGN
                        Result = Result & "C,"
                    Case 3  'GRID_HEADER
                        Result = Result & "S" & CStr(i) & ","
                    Case 4  'Just Commas
                        Result = Result & ","
                    Case Else
                End Select
            Next
        Else
            Select Case ForWhat
                Case -1 'MAIN_HEADER
                    Result = "Status Icons,"
                Case -2 'HEADER_SIZE
                    Result = CStr(j) & ","
                Case -3 'HEADER_COLOR
                    Result = ","
                Case Else
            End Select
        End If
        Result = Result & ItemList
    End If
    AddAftVersFields = Result
End Function
Private Function InitAftVersStatusInfo(Index As Integer, SectionKey As String) As String
    Dim CfgVers As String
    Dim CfgPics As String
    Dim CfgCapt As String
    Dim CfgText As String
    Dim PicChar As String
    Dim PicObjID As String
    Dim PicFileName As String
    Dim PicFullName As String
    Dim iPos As Integer
    Dim iLen As Integer
    CfgVers = InitTabProperty(Index, SectionKey, "STATUS_VERS", "")
    CfgPics = InitTabProperty(Index, SectionKey, "STATUS_PICS", "")
    CfgCapt = InitTabProperty(Index, SectionKey, "STATUS_CAPT", "")
    CfgText = InitTabProperty(Index, SectionKey, "STATUS_TEXT", "")
    If CfgVers <> "" Then
        iLen = Len(CfgVers)
        For iPos = 1 To iLen
            PicChar = Mid(CfgVers, iPos, 1)
            PicObjID = "PIC_" & PicChar
            PicFileName = GetItem(CfgPics, iPos, ",")
            PicFullName = StatusPicPath & "\" & PicFileName
            FileData(Index).CreateCellBitmpapObj PicObjID, PicFullName
        Next
    End If
End Function

Private Sub AdjustSectionGroups(Index As Integer, SectionKey As String, SectionGroups As String, MaxMain As Integer, GroupItems As String)
    Dim tmpSecGroup As String
    Dim tmpTabGroup As String
    Dim tmpGrpName As String
    Dim tmpTabName As String
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim grpItm As Integer
    Dim tabItm As Integer
    Dim LinCnt As Integer
    
    GroupItems = ""
    NewTop = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height + 60
    LightLinIdx = -1
    DarkLinIdx = -1
    LinCnt = 0
    grpItm = 0
    tmpSecGroup = "START"
    While tmpSecGroup <> ""
        grpItm = grpItm + 1
        tmpSecGroup = GetItem(SectionGroups, grpItm, "|")
        If tmpSecGroup <> "" Then
            'Begin of a new group
            tmpGrpName = GetItem(tmpSecGroup, 1, ":")
            tmpTabGroup = GetItem(tmpSecGroup, 2, ":")
            If (InStr(tmpGrpName, "[") = 0) And (InStr(tmpGrpName, "]") = 0) Then
                If tmpTabGroup = "" Then
                    'No group leader, only members
                    NewLeft = chkTabIsVisible(0).Left
                    tmpTabGroup = tmpGrpName
                    tmpGrpName = ""
                End If
                If tmpGrpName <> "" Then
                    LinCnt = LinCnt + 1
                    If tmpGrpName = SectionKey Then
                        'Found the group leader, can exit all loops
                        NewLeft = chkTabIsVisible(0).Left
                        GroupItems = tmpSecGroup
                        tmpTabGroup = ""
                        tmpSecGroup = ""
                    Else
                        'Must look for members
                        NewLeft = chkTabIsVisible(0).Left + 90
                        NewTop = NewTop + chkTabIsVisible(0).Height + 45
                    End If
                End If
                If tmpTabGroup <> "" Then
                    tabItm = 0
                    tmpTabName = "START"
                    While tmpTabName <> ""
                        tabItm = tabItm + 1
                        tmpTabName = GetItem(tmpTabGroup, tabItm, ",")
                        If tmpTabName <> "" Then
                            If (InStr(tmpTabName, "[") = 0) And (InStr(tmpTabName, "]") = 0) Then
                                LinCnt = LinCnt + 1
                                If (tmpGrpName = "") And (LinCnt = (MaxMain + 2)) Then NewTop = NewTop + 75
                                If tmpTabName = SectionKey Then
                                    'Found member, exit all loops
                                    tmpTabGroup = ""
                                    tmpSecGroup = ""
                                Else
                                    NewTop = NewTop + chkTabIsVisible(0).Height + 45
                                End If
                            Else
                                NewTop = SetPanelBorders(tmpTabName, NewTop)
                            End If
                        End If
                    Wend
                End If
                If tmpSecGroup <> "" Then NewTop = NewTop + 60
            Else
                NewTop = SetPanelBorders(tmpGrpName, NewTop)
            End If
        End If
    Wend
    If Index >= 0 Then
        chkTabIsVisible(Index).Top = NewTop
        chkTabIsVisible(Index).Left = NewLeft
        lblTabName(Index).Top = chkTabIsVisible(Index).Top
        lblTabName(Index).Left = chkTabIsVisible(Index).Left + chkTabIsVisible(Index).Width + 30
        lblTabShadow(Index).Top = chkTabIsVisible(Index).Top + 15
        lblTabShadow(Index).Left = chkTabIsVisible(Index).Left + chkTabIsVisible(Index).Width + 45
    End If
End Sub
Private Function SetPanelBorders(UsePattern As String, CurTop As Long) As Long
    Dim NewTop As Long
    Dim i As Integer
    Dim j As Integer
    NewTop = CurTop
    j = Len(UsePattern)
    For i = 1 To j
        Select Case Mid(UsePattern, i, 1)
            Case "["
                LightLinIdx = LightLinIdx + 1
                If LightLinIdx > linLight.UBound Then
                    Load linLight(LightLinIdx)
                    Set linLight(LightLinIdx).Container = RightPanel
                End If
                linLight(LightLinIdx).Y1 = NewTop
                linLight(LightLinIdx).Y2 = NewTop
                linLight(LightLinIdx).Visible = True
                NewTop = NewTop + 15
            Case "]"
                DarkLinIdx = DarkLinIdx + 1
                If DarkLinIdx > linDark.UBound Then
                    Load linDark(DarkLinIdx)
                    Set linDark(DarkLinIdx).Container = RightPanel
                End If
                linDark(DarkLinIdx).Y1 = NewTop
                linDark(DarkLinIdx).Y2 = NewTop
                linDark(DarkLinIdx).Visible = True
                NewTop = NewTop + 15
            Case " "
                NewTop = NewTop + 90
            Case Else
        End Select
    Next
    NewTop = NewTop + 75
    SetPanelBorders = NewTop
End Function
Private Sub SetTabEditColumns(Index As Integer, EditFields As String)
    Dim tmpCols As String
    Dim tmpFields As String
    Dim CurItem As Long
    Dim tmpFldNam As String
    tmpFields = FileData(Index).LogicalFieldList
    tmpCols = ""
    CurItem = 0
    tmpFldNam = GetRealItem(tmpFields, CurItem, ",")
    While tmpFldNam <> ""
        If InStr(EditFields, tmpFldNam) = 0 Then tmpCols = tmpCols & CStr(CurItem) & ","
        CurItem = CurItem + 1
        tmpFldNam = GetRealItem(tmpFields, CurItem, ",")
    Wend
    If tmpCols <> "" Then
        tmpCols = Left(tmpCols, Len(tmpCols) - 1)
        FileData(Index).NoFocusColumns = tmpCols
    End If
End Sub
Private Sub FormatTimeFields(UseTab As TABLib.Tab, ColAlign As String)
    Dim ItemNo As Long
    Dim tmpStr As String
    Dim tmpFld As String
    Dim CedaTimeField As Boolean
    ItemNo = 0
    tmpStr = Trim(GetRealItem(ColAlign, ItemNo, ","))
    tmpFld = GetRealItem(UseTab.LogicalFieldList, ItemNo, ",")
    While tmpStr <> ""
        Select Case tmpStr
            Case "FDT", "CDT"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDateTime
                CedaTimeField = True
            Case "FDP", "CD"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDatePart
                CedaTimeField = True
            Case "FTP", "CT"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatTimePart
                CedaTimeField = True
            Case "FDO"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDD"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDateOnly
                CedaTimeField = True
            Case "FTO"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "hhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatTimeOnly
                CedaTimeField = True
            Case Else
                UseTab.DateTimeResetColumn ItemNo
                CedaTimeField = False
        End Select
        If CedaTimeField = True Then
            If InStr(ListOfCedaTimeFields, tmpFld) = 0 Then ListOfCedaTimeFields = ListOfCedaTimeFields & "," & tmpFld
        End If
        ItemNo = ItemNo + 1
        tmpStr = GetRealItem(ColAlign, ItemNo, ",")
        tmpFld = GetRealItem(UseTab.LogicalFieldList, ItemNo, ",")
    Wend
End Sub
Private Sub CleanSystemFields(UseTab As TABLib.Tab)
    Dim Index As Integer
    Dim SysFldLst As String
    Dim SysFldDat As String
    Dim LineStamp As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    Index = UseTab.Index
    SysFldLst = "'NO','ID','DO','RM','SY','ST','LG','S1','C1','S2','C2','S3','C3','S4','C4'"
    SysFldLst = AddAftVersFields(Index, SysFldLst, 0)
    MaxLine = UseTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        LineStamp = Right("000000" & CStr(CurLine), 6)
        SysFldDat = AddAftVersFields(Index, "", 4)
        SysFldDat = SysFldDat & CStr(CurLine + 1) & ",,,,,,,,,,," & LineStamp & ",,,,"
        UseTab.SetFieldValues CurLine, SysFldLst, SysFldDat
    Next
    CheckAftVersIcons UseTab, -1
    CleanGridFields UseTab
End Sub
Private Sub CleanGridFields(UseTab As TABLib.Tab)
    Dim Index As Integer
    Dim CfgEntry As String
    Dim CfgRule As String
    Dim CfgFldLst As String
    Dim CfgLookVal As String
    Dim CfgReplVal As String
    Dim GridFldLst As String
    Dim GridFldDat As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    Dim iItm As Integer
    Index = UseTab.Index
    CfgEntry = GetTabProperty(Index, "REPLACE_VAL", 1, "")
    If CfgEntry <> "" Then
        iItm = 0
        CfgRule = "START"
        While CfgRule <> ""
            iItm = iItm + 1
            CfgRule = GetItem(CfgEntry, iItm, "|")
            If CfgRule <> "" Then
                CfgFldLst = GetItem(CfgRule, 1, ":")
                tmpData = GetItem(CfgRule, 2, ":")
                GridFldLst = CfgFldLst
                CfgLookVal = "," & GetItem(tmpData, 1, "=") & ","
                CfgReplVal = "," & GetItem(tmpData, 2, "=") & ","
                MaxLine = UseTab.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    GridFldDat = "," & UseTab.GetFieldValues(CurLine, GridFldLst) & ","
                    tmpData = "START" & GridFldDat
                    While tmpData <> GridFldDat
                        tmpData = GridFldDat
                        GridFldDat = Replace(GridFldDat, CfgLookVal, CfgReplVal, 1, -1, vbBinaryCompare)
                    Wend
                    GridFldDat = Mid(GridFldDat, 2, Len(GridFldDat) - 2)
                    UseTab.SetFieldValues CurLine, GridFldLst, GridFldDat
                Next
            End If
        Wend
    End If
    SetGridLineColors UseTab, -1
End Sub
Private Sub SetGridLineColors(UseTab As TABLib.Tab, LineNo As Long)
    Dim Index As Integer
    Dim CfgEntry As String
    Dim CfgRule As String
    Dim CheckRule As String
    Dim ColorCodes As String
    Dim LookFldLst As String
    Dim LookDatLst As String
    Dim LookFldNam As String
    Dim LookFldDat As String
    Dim GridFldDat As String
    Dim CheckCond As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseForeColor As Long
    Dim UseBackColor As Long
    Dim SetColor As Boolean
    Dim iCfg As Integer
    Index = UseTab.Index
    CfgEntry = GetTabProperty(Index, "LINE_COLORS", 1, "")
    If CfgEntry <> "" Then
        MaxLine = UseTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            iCfg = 0
            CfgRule = "START"
            While CfgRule <> ""
                iCfg = iCfg + 1
                CfgRule = GetItem(CfgEntry, iCfg, "|")
                If CfgRule <> "" Then
                    SetColor = False
                    CheckRule = GetItem(CfgRule, 1, ":")
                    GetKeyItem LookFldLst, CheckRule, "FIELDS(", ")"
                    GetKeyItem LookDatLst, CheckRule, "DATA(", ")"
                    GetKeyItem CheckCond, CheckRule, ")", "DATA("
                    LookFldNam = LookFldLst
                    LookFldDat = LookDatLst
                    GridFldDat = UseTab.GetFieldValue(CurLine, LookFldNam)
                    Select Case CheckCond
                        Case "=", "=="
                            If GridFldDat = LookFldDat Then SetColor = True
                        Case "<"
                            If Val(GridFldDat) < Val(LookFldDat) Then SetColor = True
                        Case "<="
                            If Val(GridFldDat) <= Val(LookFldDat) Then SetColor = True
                        Case ">"
                            If Val(GridFldDat) > Val(LookFldDat) Then SetColor = True
                        Case ">="
                            If Val(GridFldDat) >= Val(LookFldDat) Then SetColor = True
                        Case "<>", "!="
                            If GridFldDat <> LookFldDat Then SetColor = True
                        Case Else
                    End Select
                    If SetColor = True Then
                        ColorCodes = GetItem(CfgRule, 2, ":")
                        tmpData = GetItem(ColorCodes, 1, ",")
                        UseBackColor = TranslateColorCode(tmpData, vbWhite)
                        tmpData = GetItem(ColorCodes, 2, ",")
                        UseForeColor = TranslateColorCode(tmpData, vbBlack)
                        UseTab.SetLineColor CurLine, UseForeColor, UseBackColor
                        CfgRule = ""
                    End If
                End If
            Wend
        Next
    End If
End Sub
Private Sub CheckAftVersIcons(UseTab As TABLib.Tab, LineNo As Long)
    Dim Index As Integer
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim CurCol As Long
    Dim LogoCol As Long
    Dim tmpVers As String
    Dim CfgVers As String
    Dim CfgPics As String
    Dim LogoObjList As String
    Dim tmpPicName As String
    Dim tmpBmpName As String
    Dim PicObjID As String
    Dim PicFileName As String
    Dim PicFullName As String
    Dim VersPicPath As String
    Dim AllVers As String
    Dim tmpChar As String
    Dim tmpFlno As String
    Dim tmpAlc As String
    Dim iLen As Integer
    Dim iMid As Integer
    Dim iPos As Integer
    VersPicPath = StatusPicPath
    Index = UseTab.Index
    CfgVers = GetTabProperty(Index, "STATUS_VERS", 1, "")
    LogoCol = CLng(GetRealItemNo(UseTab.LogicalFieldList, "'LG'"))
    If LogoCol >= 0 Then
        LogoObjList = GetTabProperty(Index, "LOGO_OBJECTS", 1, "")
        If LogoObjList = "" Then
            PicFullName = StatusPicPath & "\logos\" & "NOLOGO.bmp"
            UseTab.CreateCellBitmpapObj "NOLOGO", PicFullName
            LogoObjList = "NOLOGO"
        End If
    End If
    If (CfgVers <> "") Or (LogoCol >= 0) Then
        CfgPics = GetTabProperty(Index, "STATUS_PICS", 1, "")
        iLen = Len(CfgVers)
        If LineNo >= 0 Then
            BgnLine = LineNo
            MaxLine = LineNo
        Else
            BgnLine = 0
            MaxLine = UseTab.GetLineCount - 1
        End If
        For CurLine = BgnLine To MaxLine
            If CfgVers <> "" Then
                tmpVers = UseTab.GetFieldValue(CurLine, "VERS")
                tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
                AllVers = CfgVers
                iLen = Len(tmpVers)
                'Set the pictures
                For iMid = 1 To iLen
                    tmpChar = Mid(tmpVers, iMid, 1)
                    iPos = InStr(CfgVers, tmpChar)
                    If iPos > 0 Then
                        Mid(AllVers, iPos, 1) = "-"
                        CurCol = CLng(iPos) - 1
                        PicObjID = "PIC_" & tmpChar
                        UseTab.SetCellBitmapProperty CurLine, CurCol, PicObjID
                        UseTab.SetColumnValue CurLine, CurCol, "__"
                    End If
                Next
                'Remove unused pictures
                iLen = Len(AllVers)
                For iMid = 1 To iLen
                    tmpChar = Mid(AllVers, iMid, 1)
                    iPos = InStr(CfgVers, tmpChar)
                    If iPos > 0 Then
                        CurCol = CLng(iPos) - 1
                        PicObjID = "PIC_" & tmpChar
                        UseTab.ResetCellBitmapProperty CurLine, CurCol
                        UseTab.SetColumnValue CurLine, CurCol, ""
                    End If
                Next
            End If
            If LogoCol >= 0 Then
                tmpFlno = UseTab.GetFieldValue(CurLine, "FLNO")
                tmpAlc = Trim(Left(tmpFlno, 3))
                PicObjID = tmpAlc & "_LG"
                If InStr(LogoObjList, PicObjID) = 0 Then
                    LogoObjList = LogoObjList & "," & PicObjID
                    'QuickHack: Tab.ocx cannot handle missing files
                    'So we first load an existing bmp
                    PicFullName = StatusPicPath & "\logos\" & "NOLOGO.bmp"
                    UseTab.CreateCellBitmpapObj PicObjID, PicFullName
                    'A missing logo will not override the dummy logo
                    PicFullName = StatusPicPath & "\logos\" & tmpAlc & "_S.bmp"
                    UseTab.CreateCellBitmpapObj PicObjID, PicFullName
                End If
                UseTab.SetCellBitmapProperty CurLine, LogoCol, PicObjID
                'UseTab.SetColumnValue CurLine, LogoCol, "_____"
            End If
        Next
        If LogoCol >= 0 Then SetTabProperty Index, "LOGO_OBJECTS", LogoObjList, ""
        UseTab.AutoSizeColumns
        UseTab.Refresh
    End If
End Sub
Private Sub chkMax_Click(Index As Integer)
    On Error Resume Next
    'If chkMax(Index).Value = 1 Then
        If chkMax(Index).Caption = "+" Then
            chkMax(Index).Caption = "-"
            chkMax(Index).ToolTipText = "Restore section windows"
            chkMax(Index).BackColor = LightGreen
            ArrangeMaximizedTabs Index, "FULL"
            FileData(Index).SetFocus
        Else
            chkMax(Index).Caption = "+"
            chkMax(Index).ToolTipText = "Maximize grid in application window"
            chkMax(Index).BackColor = vbButtonFace
            ArrangeMaximizedTabs Index, "FULL"
            FileData(Index).SetFocus
        End If
        'chkMax(Index).Value = 0
    'End If
End Sub

Private Sub chkMaxSec_Click(Index As Integer)
    Dim SizeType As String
    On Error Resume Next
    'If chkMaxSec(Index).Value = 1 Then
        If chkMaxSec(Index).Caption = "^" Then
            chkMaxSec(Index).Caption = "-"
            chkMaxSec(Index).ToolTipText = "Restore section windows"
            chkMaxSec(Index).BackColor = LightGreen
        Else
            chkMaxSec(Index).Caption = "^"
            chkMaxSec(Index).ToolTipText = "Maximize grid in vertical frame"
            chkMaxSec(Index).BackColor = vbButtonFace
        End If
        If TabPanel(Index).Left < fraVtSplitter(0).Left Then SizeType = "LEFT"
        If TabPanel(Index).Left > fraVtSplitter(0).Left Then SizeType = "RIGHT"
        ArrangeMaximizedTabs Index, SizeType
        FileData(Index).SetFocus
        'chkMaxSec(Index).Value = 0
    'End If
End Sub

Private Sub chkTabCoverIsVisible_Click(Index As Integer)
    If chkTabCoverIsVisible(Index).Value = 1 Then
        TabCover(Index).Top = TabPanel(Index).Top + FileData(Index).Top
        TabCover(Index).Height = TabPanel(Index).Height - FileData(Index).Top
        SelTab1(Index).Top = 0
        SelTab1(Index).Left = FileData(Index).Left
        SelTab1(Index).Width = FileData(Index).Width
        SelTab1(Index).Height = FileData(Index).Height
        SelTab1(Index).HeaderLengthString = FileData(Index).HeaderLengthString
        If AutoHideSynchroTabs = True Then TabCover(Index).Visible = True
    Else
        TabCover(Index).Visible = False
        TabCover(Index).Top = TabPanel(Index).Top
        TabCover(Index).Height = TabPanel(Index).Height
    End If
End Sub

Private Sub chkTaskPanel_Click(Index As Integer)
    If chkTaskPanel(Index).Value = 1 Then
        Select Case Index
            Case 0
                LeftPanel.Visible = True
            Case 1
                SidePanel.Visible = True
            Case 2
                ToolPanel.Visible = True
            Case 3
                TaskPanel.Visible = True
            Case 4
                StatusPanel.Visible = True
            Case 5
                BottomPanel.Visible = True
            Case 6
                ServerPanel.Visible = True
            Case 7
                SystabPanel.Visible = True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                LeftPanel.Visible = False
            Case 1
                SidePanel.Visible = False
            Case 2
                ToolPanel.Visible = False
            Case 3
                TaskPanel.Visible = False
            Case 4
                StatusPanel.Visible = False
            Case 5
                BottomPanel.Visible = False
            Case 6
                ServerPanel.Visible = False
            Case 7
                SystabPanel.Visible = False
            Case Else
        End Select
    End If
    Form_Resize
End Sub

Private Sub chkTerminate_Click()
    If chkTerminate.Value = 1 Then
        chkTerminate.BackColor = LightGreen
        chkAppl(0).Value = 1
    Else
        chkTerminate.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTest_Click(Index As Integer)
    Dim SetVisible As Boolean
    Dim i As Integer
    If Index = 0 Then
        If chkTest(Index).Value = 1 Then SetVisible = True
        For i = 0 To chkTaskPanel.UBound
            chkTaskPanel(i).Visible = SetVisible
        Next
    End If
End Sub

Private Sub Command1_Click()
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If (chkTabIsVisible(i).Visible) And (chkTabIsVisible(i).Value = 1) Then
            TabPropBag(i).Top = 300
            TabPropBag(i).Left = 300
            TabPropBag(i).Width = TabCover(i).Width - 600
            TabPropBag(i).Height = TabCover(i).Height - 600
            TabPropBag(i).Visible = True
            TabPropBag(i).AutoSizeColumns
            TabCover(i).Visible = True
        End If
    Next
End Sub

Private Sub Command2_Click()
    Dim i As Integer
    Dim j As Integer
    Dim iPos As Integer
    Dim iLen As Integer
    Dim dRand As Double
    Dim dHigh As Double
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CfgVers As String
    Dim AftVers As String
    Dim tmpData As String
    For i = 0 To 1
        CfgVers = GetTabProperty(i, "STATUS_VERS", 1, "")
        iLen = Len(CfgVers)
        dHigh = CDbl(iLen + 1)
        If CfgVers <> "" Then
            MaxLine = FileData(i).GetLineCount - 1
            For CurLine = 0 To MaxLine
                AftVers = Trim(FileData(i).GetFieldValue(CurLine, "VERS"))
                If AftVers = "" Then
                    AftVers = Space(iLen)
                    For j = 1 To 2
                        dRand = RandomReal(dHigh, 0)
                        iPos = CInt(dRand)
                        If (iPos > 0) And (iPos <= iLen) Then
                            If Mid(CfgVers, iPos, 1) <> "A" Then
                                Mid(AftVers, iPos, 1) = Mid(CfgVers, iPos, 1)
                            End If
                        End If
                    Next
                    tmpData = FileData(i).GetFieldValue(CurLine, "BAA3")
                    If tmpData <> "" Then
                        iPos = InStr(CfgVers, "A")
                        If iPos > 0 Then
                            Mid(AftVers, iPos, 1) = "A"
                        End If
                    End If
                    AftVers = Replace(AftVers, " ", "", 1, -1, vbBinaryCompare)
                    FileData(i).SetFieldValues CurLine, "VERS", AftVers
                End If
            Next
        End If
        FileData(i).AutoSizeColumns
        FileData(i).Refresh
        CheckAftVersIcons FileData(i), -1
    Next
End Sub

Private Sub FileData_OnHScroll(Index As Integer, ByVal ColNo As Long)
    If Not StopNestedCalls Then
        StopNestedCalls = True
        TabLookUp(Index).OnHScrollTo ColNo
        StopNestedCalls = False
    End If
End Sub

Private Sub FileData_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Static LastLineNo As Long
    Static LastColno As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim CurField As String
    Dim LogoPath As String
    Dim CheckPath As String
    Dim tmpData As String
    Dim CurItem As Integer
    If LineNo >= 0 Then
        If (LineNo <> LastLineNo) Or (ColNo <> LastColno) Then
            PopUpTimer.Enabled = False
            PopUpPanel.Visible = False
            CurItem = CInt(ColNo) + 1
            CurField = GetItem(FileData(Index).LogicalFieldList, CurItem, ",")
            Select Case CurField
                Case "'LG'"
                    LogoPath = FileData(Index).GetBitmapFileName(LineNo, ColNo)
                    If LogoPath <> "" Then
                        If InStr(LogoPath, "NOLOGO") = 0 Then
                            CheckPath = Dir(LogoPath)
                            If CheckPath <> "" Then
                                NewTop = WorkArea.Top + TabPanel(Index).Top + FileData(Index).Top
                                NewTop = NewTop + ((LineNo - FileData(Index).GetVScrollPos + 2) * (FileData(Index).LineHeight * 15))
                                NewTop = NewTop + 15
                                PopUpPanel.Top = NewTop
                                NewLeft = GetLeftPosInGridDisplay(FileData(Index), ColNo + 2)
                                NewLeft = NewLeft + FileData(Index).Left
                                PopUpPanel.Left = NewLeft
                                PopUpHarry.Visible = False
                                PopUpLabel.Visible = False
                                PopUpReport.Visible = False
                                PopUpPic.Left = 0
                                PopUpPic.Top = 0
                                PopUpPic.Picture = LoadPicture(LogoPath)
                                PopUpPanel.Width = PopUpPic.Width + 30
                                PopUpPanel.Height = PopUpPic.Height + 30
                                PopUpTimer.Tag = "4000"
                                PopUpTimer.Enabled = True
                            End If
                        End If
                    End If
                Case "'PA'"
                    LogoPath = FileData(Index).GetBitmapFileName(LineNo, ColNo)
                    If LogoPath <> "" Then
                        If InStr(LogoPath, "NOLOGO") = 0 Then
                            CheckPath = Dir(LogoPath)
                            If CheckPath <> "" Then
                                NewTop = WorkArea.Top + TabPanel(Index).Top + FileData(Index).Top
                                NewTop = NewTop + ((LineNo - FileData(Index).GetVScrollPos + 2) * (FileData(Index).LineHeight * 15))
                                NewTop = NewTop + 15
                                PopUpPanel.Top = NewTop
                                NewLeft = GetLeftPosInGridDisplay(FileData(Index), ColNo + 2)
                                NewLeft = NewLeft + FileData(Index).Left
                                PopUpPanel.Left = NewLeft
                                PopUpHarry.Visible = True
                                PopUpHarry.Picture = PicHarry(0).Picture
                                PopUpPic.Picture = LoadPicture(LogoPath)
                                PopUpPic.Left = PopUpHarry.Left + PopUpHarry.Width + 30
                                PopUpPanel.Width = PopUpPic.Left + PopUpPic.Width + 60
                                PopUpPanel.Height = PopUpHarry.Height + 30
                                
                                PopUpLabel.Caption = "Transfer"
                                PopUpLabel.Left = PopUpHarry.Left + 1350 - (PopUpLabel.Width \ 2)
                                PopUpLabel.Top = PopUpHarry.Top + 240 - (PopUpLabel.Height \ 2)
                                PopUpLabel.Visible = True
                                
                                PopUpReport.Top = PopUpHarry.Top + 1260
                                tmpData = FileData(Index).GetFieldValue(LineNo, "BAA3")
                                If tmpData = "" Then tmpData = "??"
                                PopUpReport.Caption = tmpData & " Short Connections (PAX/BAG)"
                                'PopUpReport.Caption = "N.A."
                                'PopUpReport.Caption = "This is the actual status report of the system."
                                PopUpReport.Left = PopUpPanel.ScaleWidth - PopUpReport.Width
                                PopUpReport.BackColor = LightestYellow
                                PopUpReport.Visible = True
                                
                                PopUpTimer.Tag = "8000"
                                PopUpTimer.Enabled = True
                            End If
                        End If
                    End If
                Case Else
            End Select
        End If
    Else
        'Tab.ocx doesn't fire the event, so we need a second timer for PopUps
        'And unfortunately tooltips of grid headers are not possible
        PopUpTimer.Enabled = False
        PopUpPanel.Visible = False
    End If
    LastLineNo = LineNo
    LastColno = ColNo
End Sub
Private Function GetLeftPosInGridDisplay(UseTab As TABLib.Tab, ColNo As Long) As Long
    Dim LengthList As String
    Dim LengthItem As String
    Dim FieldLeft As Long
    Dim CurLength As Long
    Dim BgnCol As Long
    Dim CurCol As Long
    LengthList = UseTab.HeaderLengthString
    FieldLeft = 0
    BgnCol = UseTab.GetHScrollPos
    For CurCol = BgnCol To ColNo - 1
        LengthItem = GetRealItem(LengthList, CurCol, ",")
        CurLength = Val(LengthItem) * 15
        FieldLeft = FieldLeft + CurLength
    Next
    GetLeftPosInGridDisplay = FieldLeft + 15
End Function

Private Sub FlightPanelTimer_Timer()
    FlightPanelCover(0).Left = FlightPanelCover(0).Left - 30
    If FlightPanelCover(0).Left < 7590 Then
        FlightPanelCover(0).Left = 7620
        FlightPanelTimer.Enabled = False
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim tmpText As String
    Dim KeyCols As String
    Dim KeyVals As String
    Dim iCnt As Integer
    CheckKeyboard Shift
    Select Case KeyCode
        Case vbKeyF3
            If TabEditLookUpType = "LOOK" Then
                If TabEditLookUpIndex >= 0 Then
                    If txtTabEditLookUp(TabEditLookUpIndex).Visible Then
                        'GetLookUpColsAndKeys TabEditLookUpIndex, KeyCols, KeyVals
                        imgAnyPic2(TabEditLookUpIndex).Picture = imgLookUpA.Picture
                        SyncCursorBusy = True
                        SearchInTabList FileData(TabEditLookUpIndex), TabEditColNo, txtTabEditLookUp(TabEditLookUpIndex).Text, False, "", ""
                        SyncCursorBusy = False
                    ElseIf TabEditLookUpFixed >= 0 Then
                        iCnt = GetLookUpColsAndKeys(TabEditLookUpFixed, KeyCols, KeyVals)
                        If iCnt > 0 Then
                            If iCnt = 1 Then imgAnyPic2(TabEditLookUpIndex).Picture = imgLookUpA.Picture Else imgAnyPic2(TabEditLookUpIndex).Picture = imgLookUpB.Picture
                            tmpText = TabLookUp(TabEditLookUpFixed).GetColumnValue(0, TabEditLookUpValueCol)
                            SearchInTabList FileData(TabEditLookUpFixed), TabEditLookUpValueCol, tmpText, False, KeyCols, KeyVals
                        End If
                    ElseIf TabEditLookUpValueCol >= 0 Then
                        iCnt = GetLookUpColsAndKeys(TabEditLookUpIndex, KeyCols, KeyVals)
                        If iCnt > 0 Then
                            If iCnt = 1 Then imgAnyPic2(TabEditLookUpIndex).Picture = imgLookUpA.Picture Else imgAnyPic2(TabEditLookUpIndex).Picture = imgLookUpB.Picture
                            tmpText = TabEditLookUpTabObj.GetColumnValue(TabEditLineNo, TabEditLookUpValueCol)
                            SearchInTabList FileData(TabEditLookUpIndex), TabEditLookUpValueCol, tmpText, False, KeyCols, KeyVals
                        End If
                    End If
                End If
            End If
        Case Else
    End Select
End Sub
Private Function GetLookUpColsAndKeys(Index As Integer, KeyCols As String, KeyVals As String) As Integer
    Dim tmpTag As String
    KeyCols = ""
    KeyVals = ""
    If Index >= 0 Then
        tmpTag = TabLookUp(Index).GetLineTag(0)
        GetKeyItem KeyCols, tmpTag, "{=COLS=}", "{="
        GetKeyItem KeyVals, tmpTag, "{=KEYS=}", "{="
    End If
    GetLookUpColsAndKeys = ItemCount(KeyCols, ",")
End Function
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub imgAnyPic1_Click(Index As Integer)
    Dim tmpTag As String
    FileData(Index).SetFocus
    tmpTag = lblTabUtl(Index).Tag
    Select Case tmpTag
        Case "LOOK"
            lblTabUtl(Index).Tag = "CLOSED"
            imgAnyPic2(Index).Visible = False
            imgAnyPic1(Index).ToolTipText = "Steps through the tool box"
        Case Else
            lblTabUtl(Index).Tag = "LOOK"
            imgAnyPic2(Index).Picture = imgLookUpA.Picture
            imgAnyPic1(Index).ToolTipText = "Look-Up Column Key Value"
            imgAnyPic2(Index).Visible = True
    End Select
    If (imgAnyPic2(Index).Visible = True) And (TabLookUp(Index).Visible = False) Then
        TabLookUp(Index).HeaderLengthString = FileData(Index).HeaderLengthString
        TabLookUp(Index).InsertTextLine CreateEmptyLine(TabLookUp(Index).LogicalFieldList), False
        TabLookUp(Index).SetCurrentSelection 0
        TabLookUp(Index).Visible = True
        FileData(Index).Top = FileData(Index).Top + TabLookUp(Index).Height
        FileData(Index).Height = FileData(Index).Height - TabLookUp(Index).Height
    ElseIf imgAnyPic2(Index).Visible = False Then
        FileData(Index).Top = FileData(Index).Top - TabLookUp(Index).Height
        FileData(Index).Height = FileData(Index).Height + TabLookUp(Index).Height
        TabLookUp(Index).Visible = False
        TabLookUp(Index).ResetContent
        lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
    End If
    SetLookUpKeyValues Index
    If lblTabUtl(Index).Tag <> "CLOSED" Then
        lblTabCtl(Index).BackColor = vbYellow
    Else
        lblTabCtl(Index).BackColor = MinMaxPanel(Index).BackColor
    End If
    GetLookUpKeyValues Index
End Sub

Private Sub imgAnyPic2_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpText As String
    If TabEditLookUpFixed >= 0 Then lblTabUtl(TabEditLookUpFixed).BackColor = MinMaxPanel(TabEditLookUpFixed).BackColor
    tmpTag = lblTabUtl(Index).Tag
    Select Case tmpTag
        Case "LOOK"
            If TabLookUp(Index).Tag <> "" Then
                If Index <> TabEditLookUpFixed Then
                    lblTabUtl(Index).BackColor = vbRed
                    GetLookUpKeyValues Index
                    TabEditLookUpFixed = Index
                Else
                    lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
                    GetLookUpKeyValues Index
                    TabEditLookUpFixed = -1
                End If
            End If
        Case Else
    End Select
End Sub
 
Private Sub lblTabCaptionShadow_DblClick(Index As Integer)
    If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
End Sub

Private Sub lblTabName_DblClick(Index As Integer)
    If (chkTabIsVisible(Index).Enabled = False) And (chkTabIsVisible(Index).Value = 1) Then
        If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
    End If
End Sub

Private Sub PopUpOutTimer_Timer()
    PopUpOutTimer.Enabled = False
    PopUpPanel.Visible = False
End Sub

Private Sub PopUpTimer_Timer()
    Dim StayTime As Long
    PopUpTimer.Enabled = False
    PopUpPanel.Visible = True
    PopUpPanel.ZOrder
    PopUpPic.Refresh
    StayTime = Val(PopUpTimer.Tag)
    If StayTime < 500 Then StayTime = 500
    If StayTime > 10000 Then StayTime = 10000
    PopUpOutTimer.Interval = StayTime
    PopUpOutTimer.Enabled = True
End Sub

Private Sub RotationData_BoolPropertyChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    Dim tmpGocx As String
    If LineNo >= 0 Then
        If ColNo = 0 Then
            tmpGocx = RotationData.GetFieldValue(LineNo, "GOCX")
            If tmpGocx = "" Then tmpGocx = "??????"
            If NewValue = True Then
                Select Case tmpGocx
                    Case "------"
                        RotationData.SetLineColor LineNo, vbBlack, LightGreen
                    Case "??????"
                        RotationData.SetLineColor LineNo, vbBlack, vbWhite
                    Case Else
                        RotationData.SetLineColor LineNo, vbBlack, vbWhite
                End Select
            Else
                Select Case tmpGocx
                    Case "------"
                        RotationData.SetLineColor LineNo, vbBlack, LightGray
                    Case "??????"
                        RotationData.SetLineColor LineNo, vbBlack, vbWhite
                    Case Else
                        RotationData.SetLineColor LineNo, vbBlack, LightRed
                End Select
            End If
        End If
    End If
End Sub

Public Sub RotationData_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim ArrIndx As String
    Dim DepIndx As String
    Dim MarkFields As String
    If LineNo >= 0 Then
        If Selected = True Then
            If DontSyncRota = False Then
                DontSyncRota = True
                If SyncCursorBusy = False Then
                    ArrIndx = RotationData.GetFieldValue(LineNo, "AIDX")
                    If ArrIndx <> "" Then
                        MarkFields = GetTabProperty(TabArrFlightsIdx, "MARK_FRAME1", 1, "FLNO")
                        ArrIndx = RotationData.GetFieldValue(LineNo, "RIDX")
                        MarkLookupLines TabArrFlightsTab, "RIDX", ArrIndx, True, MarkFields, True, "'S1'", "DecoMarkerLB"
                        CheckSynchroTabs TabArrFlightsIdx, True
                    Else
                        DepIndx = RotationData.GetFieldValue(LineNo, "DIDX")
                        If DepIndx <> "" Then
                            MarkFields = GetTabProperty(TabDepFlightsIdx, "MARK_FRAME1", 1, "FLNO")
                            DepIndx = RotationData.GetFieldValue(LineNo, "RIDX")
                            MarkLookupLines TabDepFlightsTab, "RIDX", DepIndx, True, MarkFields, True, "'S1'", "DecoMarkerLB"
                            CheckSynchroTabs TabDepFlightsIdx, True
                        End If
                    End If
                    RotationData.Refresh
                End If
                DontSyncRota = False
            End If
            If FormIsVisible("StatusChart") Then
                If SyncOriginator <> "GOC" Then
                    StatusChart.ScrollSyncExtern LineNo
                End If
            End If
            If FormIsVisible("ConnexChart") Then
                If SyncOriginator <> "CNX" Then
                    ConnexChart.ScrollSyncExtern LineNo
                End If
            End If
        End If
    End If
End Sub

Private Sub RotationData_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Static LastColno As Long
    Dim SortCol As Long
    Dim TimeCol As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColName As String
    Dim SortVal1 As String
    Dim SortVal2 As String
    Dim SortKeys As String
    If LineNo = -1 Then
        If ColNo >= 0 Then
            Screen.MousePointer = 11
            ColName = GetRealItem(RotationData.LogicalFieldList, ColNo, ",")
            Select Case ColName
                Case "DRGN"
                    TimeCol = GetRealItemNo(RotationData.LogicalFieldList, "BEST")
                Case "AFLT"
                    TimeCol = GetRealItemNo(RotationData.LogicalFieldList, "TIFA")
                Case "DFLT"
                    TimeCol = GetRealItemNo(RotationData.LogicalFieldList, "TIFD")
                Case Else
                    TimeCol = -1
            End Select
            If TimeCol >= 0 Then
                SortCol = GetRealItemNo(RotationData.LogicalFieldList, "SORT")
                MaxLine = RotationData.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    SortVal1 = RotationData.GetColumnValue(CurLine, ColNo)
                    SortVal1 = GetItem(SortVal1, "1", "/")
                    SortVal1 = Left(SortVal1 & "------------", 12)
                    SortVal2 = RotationData.GetColumnValue(CurLine, TimeCol)
                    SortKeys = SortVal1 & SortVal2
                    RotationData.SetColumnValue CurLine, SortCol, SortKeys
                Next
                If ColNo <> LastColno Then
                    RotationData.Sort CStr(SortCol), True, True
                Else
                    RotationData.Sort CStr(SortCol), False, True
                End If
                RotationData.AutoSizeColumns
                RotationData.Refresh
            End If
            Screen.MousePointer = 0
        End If
        LastColno = ColNo
    End If
End Sub

Private Sub SelTab1_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    'Text2.Text = CStr(Index) & "/" & CStr(TabCover.UBound)
End Sub

Private Sub TabCover_Click(Index As Integer)
    'Text2.Text = CStr(Index) & "/" & CStr(TabCover.UBound)
End Sub

Private Sub TabLookUp_OnHScroll(Index As Integer, ByVal ColNo As Long)
    If Not StopNestedCalls Then
        StopNestedCalls = True
        FileData(Index).OnHScrollTo ColNo
        StopNestedCalls = False
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetAllFormsOnTop True
        'SetFormOnTop Me, True
    Else
        OnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub chkSelDeco_Click(Index As Integer)
    If chkSelDeco(Index).Value = 1 Then
        chkSelDeco(Index).BackColor = LightGreen
    Else
        chkSelDeco(Index).BackColor = vbButtonFace
    End If
    If Not SyncCursorBusy Then
        If LastFocusIndex >= 0 Then
            If Index = 0 Then CheckSynchroTabs LastFocusIndex, False
            If Index = 1 Then CheckTabAutoMarker LastFocusIndex
            FileData(LastFocusIndex).SetFocus
        End If
    End If
End Sub

Private Sub chkSelFlight_Click(Index As Integer)
    Dim i As Integer
    If chkSelFlight(Index).Value = 1 Then
        If Index = 0 Then
            chkSelFlight(Index).BackColor = vbYellow
        Else
            chkSelFlight(Index).BackColor = vbGreen
        End If
        SetWorkButtonCaption "PRT_FLT", chkSelFlight(Index).Caption, ""
        If chkSelToggle(0).Value = 0 Then
            ReorgFlightToggle = True
            If (Index = 0) And (chkSelFlight(1).Value = 1) Then
                chkSelFlight(1).Value = 0
            ElseIf (Index = 1) And (chkSelFlight(0).Value = 1) Then
                chkSelFlight(0).Value = 0
            End If
            ReorgFlightToggle = False
        End If
        If Not FocusSyncInternal Then
            For i = 0 To chkTabIsVisible.UBound
                If chkTabIsVisible(i).Value = 1 Then
                    If BcSign1(i).Tag = chkSelFlight(Index).Caption Then
                        FileData(i).SetFocus
                        Exit For
                    End If
                End If
            Next
        End If
    Else
        chkSelFlight(Index).BackColor = vbButtonFace
        If Not ButtonPushedInternal Then
            If chkSelToggle(0).Value = 0 Then
                If (Index = 0) And (chkSelFlight(1).Value = 0) Then
                    chkSelFlight(1).Value = 1
                ElseIf (Index = 1) And (chkSelFlight(0).Value = 0) Then
                    chkSelFlight(0).Value = 1
                End If
            End If
        End If
    End If
    ToggleFlightPanel
    If Not ReorgFlightToggle Then ToggleArrDepView
End Sub
Private Sub ToggleFlightPanel()
    Dim i As Integer
    Dim SetBackColor As Long
    If txtAftData(2).Text <> "" Then
        If (chkSelFlight(0).Value = 0) And (chkSelFlight(1).Value = 1) Then
            SetBackColor = NormalGray
        ElseIf (chkSelFlight(0).Value = 1) And (chkSelFlight(1).Value = 1) Then
            SetBackColor = vbYellow
        Else
            SetBackColor = vbWhite
        End If
        For i = 0 To lblArr.UBound
            lblArr(i).BackColor = SetBackColor
        Next
        For i = 6 To 8
            lblArr(i).BackColor = vbWhite
        Next
    End If
    If txtAftData(4).Text <> "" Then
        If (chkSelFlight(1).Value = 0) And (chkSelFlight(0).Value = 1) Then
            SetBackColor = NormalGray
        ElseIf (chkSelFlight(1).Value = 1) And (chkSelFlight(0).Value = 1) Then
            SetBackColor = vbGreen
        Else
            SetBackColor = vbWhite
        End If
        For i = 0 To lblDep.UBound
            lblDep(i).BackColor = SetBackColor
        Next
        For i = 6 To 8
            lblDep(i).BackColor = vbWhite
        Next
    End If
End Sub
Private Sub ToggleArrDepView()
    Dim i As Integer
    Dim tmpFlnu As String
    Dim AftFlnu As String
    Dim tmpLenList As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim itm As Integer
    
    If FipsIsConnected Then
        If chkSelFlight(1).Value = 1 Then
            AftFlnu = txtAftData(3).Text
        Else
            AftFlnu = txtAftData(1).Text
        End If
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                tmpMaskType = HiddenData.GetConfigValues(FileData(i).myName, "MASK")
                tmpLayout = GetRealItem(tmpMaskType, 1, ",")
                If tmpLayout = "REQU" Then
                    tmpLenList = FileData(i).HeaderLengthString
                    If chkSelFlight(0).Value <> chkSelFlight(1).Value Then
                        For itm = 2 To 5
                            SetItem tmpLenList, itm, ",", "-1"
                        Next
                    Else
                        For itm = 2 To 5
                            SetItem tmpLenList, itm, ",", "80"
                        Next
                    End If
                    FileData(i).HeaderLengthString = tmpLenList
                    If Not ReorgFlightPanel Then PushWorkButton "READAODB", 1
                    MaxLine = FileData(i).GetLineCount - 1
                    If (chkSelFlight(0).Value = 1) And (chkSelFlight(1).Value = 1) Then
                        For CurLine = 0 To MaxLine
                            tmpFlnu = FileData(i).GetFieldValue(CurLine, "FLNU")
                            If tmpFlnu = txtAftData(3).Text Then FileData(i).SetLineColor CurLine, vbBlack, vbGreen
                            If tmpFlnu = txtAftData(1).Text Then FileData(i).SetLineColor CurLine, vbBlack, vbYellow
                            If tmpFlnu = "" Then FileData(i).SetLineColor CurLine, vbBlack, vbWhite
                        Next
                    End If
                End If
            End If
            FileData(i).Refresh
        Next
    End If
End Sub

Private Sub chkSelToggle_Click(Index As Integer)
    If chkSelToggle(Index).Value = 1 Then
        chkSelToggle(Index).BackColor = LightBlue
        If (chkSelFlight(0).Value = 0) And (chkSelFlight(1).Value = 0) Then
            ReorgFlightPanel = True
        End If
        chkSelFlight(0).Value = 1
        chkSelFlight(1).Value = 1
        If ReorgFlightPanel Then
            ReorgFlightPanel = False
            ToggleArrDepView
        End If
    Else
        chkSelToggle(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTabIsVisible_Click(Index As Integer)
    Dim TabName As String
    Dim TabKey As String
    Dim tmpTag As String
    Dim tmpList As String
    Dim CountSplitter As Integer
    Dim TabCnt As Integer
    Dim i As Integer
    Dim j As Integer
    Dim n As Integer
    
'If ApplicationIsReadyForUse = True Then
    
    'If Index = FullMaximizedTab Then ArrangeMaximizedTabs Index, "FULL"
    'If Index = LeftMaximizedTab Then ArrangeMaximizedTabs Index, "LEFT"
    'If Index = RightMaximizedTab Then ArrangeMaximizedTabs Index, "RIGHT"
    If Index = FullMaximizedTab Then chkMax(Index).Value = 0
    If Index = LeftMaximizedTab Then chkMaxSec(Index).Value = 0
    If Index = RightMaximizedTab Then chkMaxSec(Index).Value = 0
    If chkTabIsVisible(Index).Value <> 2 Then
        For i = 0 To chkTabIsVisible.UBound
            FileData(i).Visible = False
            fraCrSplitter(i).Visible = False
            fraHzSplitter(i).Visible = False
        Next
        For i = 0 To fraVtSplitter.UBound
            fraVtSplitter(i).Visible = False
        Next
        CountSplitter = 0
        CountMainTabs = 0
        CountSubTabs = 0
        tmpList = ""
        tmpTag = ""
        TabCnt = 0
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Value = 1 Then
                TabCnt = TabCnt + 1
                tmpList = tmpList & CStr(i)
                If Left(chkTabIsVisible(i).Tag, 1) = "L" Then
                    chkTabIsVisible(i).Tag = "L,-1"
                    If CountMainTabs > 0 Then
                        CountSplitter = CountSplitter + 1
                        tmpTag = "L," & CStr(CountSplitter)
                    End If
                    CountMainTabs = CountMainTabs + 1
                Else
                    chkTabIsVisible(i).Tag = "R,-1"
                    If CountSubTabs > 0 Then
                        CountSplitter = CountSplitter + 1
                        tmpTag = "R," & CStr(CountSplitter)
                    End If
                    CountSubTabs = CountSubTabs + 1
                End If
                If tmpTag <> "" Then
                    chkTabIsVisible(j).Tag = tmpTag
                    fraHzSplitter(CountSplitter).Visible = True
                    fraCrSplitter(CountSplitter).Visible = True
                    fraHzSplitter(CountSplitter).Tag = tmpList
                    tmpTag = ""
                End If
                FileData(i).Visible = True
                tmpList = CStr(i) & ","
                j = i
            End If
        Next
        If (CountMainTabs > 0) And (CountSubTabs > 0) Then
            fraVtSplitter(0).Visible = True
            If ApplicationIsReadyForUse = True Then SetUfisIntroFrames False
        Else
            For i = 0 To fraCrSplitter.UBound
                fraCrSplitter(i).Visible = False
            Next
        End If
        ArrangeLayout -1
        LastFocusIndex = -1
        If FileData(Index).Visible Then
            If chkTabIsVisible(Index).Value = 1 Then
                'CheckTabAutoMarker CurFocusIndex
                'FileData(Index).SetFocus
                'LastFocusIndex = Index
            End If
        End If
        If chkTabIsVisible(Index).Value = 0 Then
            If MainLifeStyle Then
                If (CountMainTabs + CountSubTabs) = 0 Then
                    DrawBackGround WorkArea, WorkAreaColor, True, True
                End If
            End If
        End If
        If (ApplicationIsReadyForUse = True) And (TimerIsActive = False) Then
            TabName = HiddenData.GetConfigValues(FileData(Index).myName, "NAME")
            TabKey = TabName & "_REFERENCE"
            tmpList = HiddenData.GetConfigValues(TabKey, "DTAB")
            If tmpList <> "" Then
                MainTimer(2).Tag = "CHECK_DTAB," & Str(Index) & "," & tmpList
                MainTimer(2).Enabled = True
            End If
        End If
        If ApplicationIsReadyForUse Then
            If TabCnt = 0 Then
                SetUfisIntroFrames True
            Else
                SetUfisIntroFrames False
            End If
        End If
        If CurFocusIndex >= 0 Then
            If chkTabIsVisible(CurFocusIndex).Value = 1 Then
                If FileData(CurFocusIndex).Visible Then
                    FileData(CurFocusIndex).SetFocus
                End If
            End If
        End If
    End If
    
'End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim SortCol As Long
    Dim i As Integer
    On Error Resume Next
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = LightGreen
        chkTask(Index).Refresh
        Select Case Index
            Case 0
                If ApplicationIsReadyForUse = True Then
                    InitFlightPanel True, -1, "", "", ""
                    PushWorkButton "READAODB", 1
                    For i = 0 To chkTabIsVisible.UBound
                        If chkTabIsVisible(i).Value = 1 Then
                            FileData(i).SetFocus
                            Exit For
                        End If
                    Next
                Else
                    chkTask(Index).Value = 0
                End If
            Case 1
                FillRotationGrid
                If FormIsLoaded("StatusChart") = True Then
                    StatusChart.GetFlightRotations
                End If
                chkTask(Index).Value = 0
            Case 2
                Screen.MousePointer = 11
                SortCol = GetRealItemNo(RotationData.LogicalFieldList, "RIDX")
                RotationData.Sort CStr(SortCol), True, True
                SortCol = GetRealItemNo(RotationData.LogicalFieldList, "BEST")
                RotationData.Sort CStr(SortCol), True, True
                RotationData.AutoSizeColumns
                RotationData.Refresh
                RotationData_SendLButtonDblClick -1, -1
                Screen.MousePointer = 0
                chkTask(Index).Value = 0
            Case 3
                If FormIsLoaded("StatusChart") = False Then
                    PushWorkButton "GOCC_CHART", 1
                Else
                    StatusChart.GetFlightRotations
                End If
                chkTask(Index).Value = 0
            Case 4
                If FormIsLoaded("ConnexChart") = False Then
                    PushWorkButton "CONX_CHART", 1
                Else
                    ConnexChart.GetFlightRotations
                End If
                chkTask(Index).Value = 0
            Case Else
        End Select
    Else
        chkTask(Index).BackColor = vbButtonFace
    End If
End Sub
Public Sub PushWorkButton(ButtonContext As String, SetValue As Integer)
    Dim i As Integer
    If ButtonContext <> "" Then
        For i = 0 To chkWork.UBound
            If chkWork(i).Visible Then
                If InStr(chkWork(i).Tag, ButtonContext) = 1 Then
                    chkWork(i).Value = SetValue
                    Exit For
                End If
            End If
        Next
    End If
End Sub
Public Sub SetWorkButtonCaption(ButtonContext As String, UseCaption As String, UseToolTip As String)
    Dim i As Integer
    For i = 0 To chkWork.UBound
        If chkWork(i).Visible Then
            If InStr(chkWork(i).Tag, ButtonContext) = 1 Then
                chkWork(i).Caption = UseCaption
                chkWork(i).ToolTipText = UseToolTip
                Exit For
            End If
        End If
    Next
End Sub

Public Function GetButtonIndexByName(ButtonContext As String) As Integer
    Dim ObjIdx As Integer
    Dim i As Integer
    ObjIdx = -1
    For i = 0 To chkWork.UBound
        If chkWork(i).Visible Then
            If InStr(chkWork(i).Tag, ButtonContext) = 1 Then
                ObjIdx = i
                Exit For
            End If
        End If
    Next
    GetButtonIndexByName = ObjIdx
End Function
Public Sub HighlightWorkButton(ButtonContext As String, UseForeColor As Long, UseBackColor As Long)
    Dim i As Integer
    For i = 0 To chkWork.UBound
        If chkWork(i).Visible Then
            If InStr(chkWork(i).Tag, ButtonContext) > 0 Then
                chkWork(i).ForeColor = UseForeColor
                chkWork(i).BackColor = UseBackColor
                Exit For
            End If
        End If
    Next
End Sub

Private Sub chkUtc_Click(Index As Integer)
    If chkUtc(Index).Value = 1 Then chkUtc(Index).BackColor = LightGreen Else chkUtc(Index).BackColor = MyOwnButtonFace
    Select Case Index
        Case 0  'To UTC
            If chkUtc(Index).Value = 1 Then
                chkUtc(1).Value = 0
                CheckTimeToggle False
                CurrentTimeZone = "UTC"
            Else
                chkUtc(1).Value = 1
            End If
        Case 1  'To Local
            If chkUtc(Index).Value = 1 Then
                chkUtc(0).Value = 0
                CheckTimeToggle True
                CurrentTimeZone = "Local at " & HomeAirport
            Else
                chkUtc(0).Value = 1
            End If
    End Select
End Sub
Private Sub CheckTimeToggle(SetUtcToLocal As Boolean)
    Dim tmpData As String
    Dim tmpFields As String
    Dim tmpItem As String
    Dim ItemNo As Long
    Dim ColNo As Long
    Dim i As Integer
    If Not ApplComingUp Then
        Screen.MousePointer = 11
        If FlightPanel.Visible Then
            If FipsIsConnected Then
                tmpFields = txtAftData(5).Text
                tmpData = ""
                ShowArrRecord tmpData, tmpFields
                ShowDepRecord tmpData, tmpFields
            Else
                tmpFields = txtAftData(6).Text
                tmpData = ""
                ShowArrRecord tmpData, tmpFields
                tmpFields = txtAftData(7).Text
                tmpData = ""
                ShowDepRecord tmpData, tmpFields
            End If
        End If
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                If (FipsIsConnected) And (InStr(FileData(i).LogicalFieldList, "LINO") > 0) Then
                    tmpFields = HiddenData.GetConfigValues(FileData(i).myName, "EDIT")
                    tmpFields = GetRealItem(tmpFields, 4, "|")
                Else
                    tmpFields = ListOfCedaTimeFields
                End If
                tmpData = TranslateFieldItems(tmpFields, FileData(i).LogicalFieldList)
                If tmpData <> "" Then
                    ItemNo = 0
                    tmpItem = GetRealItem(tmpData, ItemNo, ",")
                    While tmpItem <> ""
                        ColNo = Val(tmpItem)
                        If SetUtcToLocal Then
                            FileData(i).DateTimeSetUTCOffsetMinutes ColNo, UtcTimeDiff
                            SelTab1(i).DateTimeSetUTCOffsetMinutes ColNo, UtcTimeDiff
                        Else
                            FileData(i).DateTimeSetUTCOffsetMinutes ColNo, 0
                            SelTab1(i).DateTimeSetUTCOffsetMinutes ColNo, 0
                        End If
                        ItemNo = ItemNo + 1
                        tmpItem = GetRealItem(tmpData, ItemNo, ",")
                    Wend
                    FileData(i).Refresh
                    SelTab1(i).Refresh
                End If
            End If
        Next
        TranslateLogTimeFieldValues
        Screen.MousePointer = 0
    End If
End Sub
Private Function CheckTimeDisplay(ChkDateTime As String, IsUtc As Boolean)
    Dim Result As String
    Result = ChkDateTime
    If chkUtc(0).Value = 1 Then
        If IsUtc Then
            Result = ChkDateTime
        Else
            Result = ApcLocalToUtc(HomeAirport, ChkDateTime)
        End If
    ElseIf chkUtc(1).Value = 1 Then
        If IsUtc Then
            Result = ApcUtcToLocal(HomeAirport, ChkDateTime, False)
        Else
            Result = ChkDateTime
        End If
    End If
    CheckTimeDisplay = Result
End Function

Private Sub chkWork_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpTyp As String
    Dim tmpToggleIdx As Integer
    If ApplicationIsReadyForUse = True Then
        If Not FipsIsConnected Then
            If Not ExcelIsOpen Then OnTop.Value = 1
            OnTop.Value = 0
        End If
        tmpTag = chkWork(Index).Tag
        tmpFunc = GetRealItem(tmpTag, 0, ",")
        tmpTyp = GetRealItem(tmpTag, 1, ",")
        Select Case tmpTyp
            Case "T"
                tmpToggleIdx = Val(GetRealItem(tmpTag, 2, ","))
                If chkWork(Index).Value = 1 Then
                    chkWork(tmpToggleIdx).Value = 0
                Else
                    chkWork(tmpToggleIdx).Value = 1
                End If
            Case "D2"
                tmpToggleIdx = Val(GetRealItem(tmpTag, 2, ","))
                If chkWork(Index).Value = 1 Then
                    chkWork(tmpToggleIdx).Enabled = False
                    chkWork(tmpToggleIdx).Value = 1
                Else
                    chkWork(tmpToggleIdx).Value = 0
                    chkWork(tmpToggleIdx).Enabled = True
                End If
            Case "I2"
                tmpToggleIdx = Val(GetRealItem(tmpTag, 2, ","))
                If chkWork(Index).Value = 1 Then
                    chkWork(tmpToggleIdx).Enabled = False
                    'chkWork(tmpToggleIdx).Value = 1
                Else
                    'chkWork(tmpToggleIdx).Value = 0
                    chkWork(tmpToggleIdx).Enabled = True
                End If
            Case Else
        End Select
        If chkWork(Index).Value = 1 Then
            chkWork(Index).BackColor = LightGreen
            chkWork(Index).ForeColor = vbBlack
            chkWork(Index).Refresh
            CheckWorkButton Me, chkWork(Index)
        Else
            UnCheckWorkButton Me, chkWork(Index)
            chkWork(Index).ForeColor = vbButtonText
            chkWork(Index).BackColor = MyOwnButtonFace
            chkWork(Index).Refresh
        End If
        If (Not TabEditInline) And (Not RefreshOnBc) And (Not ExcelJustOpened) Then
            If LastFocusIndex >= 0 Then
                If chkTabIsVisible(LastFocusIndex).Value = 1 Then
                    FileData(LastFocusIndex).SetFocus
                End If
            End If
        End If
        ExcelJustOpened = False
    Else
        chkWork(Index).Value = 0
    End If
End Sub

Private Sub CheckWorkButton(CurForm As Form, CurButton As CheckBox)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpType As String
    Dim tmpPara As String
    Dim tmpArgs As String
    Dim i As Integer
    tmpTag = CurButton.Tag
    tmpFunc = GetRealItem(tmpTag, 0, ",")
    tmpType = GetRealItem(tmpTag, 1, ",")
    tmpPara = GetRealItem(tmpTag, 2, ",")
    Select Case tmpFunc
        Case "TAB_INSERT"
            If CurButton.Enabled Then HandleTabInsert CurForm, CurButton
        Case "TAB_EDIT_INSERT"
            HandleTabEditInsert CurForm, CurButton
        Case "TAB_UPDATE"
            If CurButton.Enabled Then HandleTabUpdate CurForm, CurButton
        Case "TAB_EDIT_UPDATE"
            HandleTabEditUpdate CurForm, CurButton
        Case "TAB_DELETE"
            HandleTabDelete CurForm, CurButton
        Case "TAB_READ"
            HandleTabRead CurForm, CurButton
        Case "AUTO_TAB_READ"
            HandleAutoTabRead CurForm, CurButton
        Case "TAB_PRINT"
            HandleTabPrint CurForm, CurButton
        Case "AODB_FILTER"
            HandleAodbFilter CurForm, CurButton
        Case "EXTRACT"
            HandleExtract CurForm, CurButton
        Case "FILEOPEN"
            HandleFileOpen CurForm, CurButton
        Case "READAODB"
            HandleReadAodb CurForm, CurButton
        Case "READLOCAL"
            HandleReadLocal CurForm, CurButton
        Case "SAVEAODB"
            If CurButton.Enabled Then
                HandleSaveAodb True
                CurButton.Value = 0
            End If
        Case "AUTO_SAVEAODB"
            HandleAutoSaveAodb CurForm, CurButton
        Case "SAVELOCAL"
            HandleSaveLocal CurForm, CurButton
        Case "EXCEL", "TO_EXCEL"
            HandleExcel CurForm, CurButton
        Case "TO_WORD"
            HandleMsWord CurForm, CurButton
        Case "AUTO_SORT"
            If (CurAutoSortCfg <> "") And (CurAutoSortCfg <> tmpTag) Then
                PushWorkButton CurAutoSortCfg, 0
            End If
            CurAutoSortCfg = tmpTag
            tmpArgs = GetItem(tmpPara, 2, ";")
            tmpPara = GetItem(tmpPara, 1, ";")
            CheckAutoSortConfig -1, "", tmpPara
            PushWorkButton tmpArgs, 1
        Case "PRT_FLTS", "PRT_USER"
            CurPrintLayout = tmpFunc
        Case "BOOKMARK"
            If BookMarkTotal > 0 Then
                MyBookMarks.Show , Me
            Else
                CurButton.Value = 0
            End If
        Case "SHOW_PROGRESS"
            WorkProgress.ShowProgress CurForm, CurButton, False
        Case "AUTO_RESTORE"
            AutoRestore = True
            If TabsFillCount(True, i) = 0 Then PushWorkButton "READLOCAL", 1
        Case "RESET_TABS"
            HandleResetAllTabs CurForm, CurButton
        Case "GOCC_CHART"
            HandleOpenGoccChart CurForm, CurButton, True
        Case "CONX_CHART"
            HandleOpenConxChart CurForm, CurButton, True
        
        Case Else
            'If tmpTyp <> "T" Then CurButton.Value = 0
    End Select
End Sub
Private Sub UnCheckWorkButton(CurForm As Form, CurButton As CheckBox)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpTyp As String
    tmpTag = CurButton.Tag
    tmpFunc = GetRealItem(tmpTag, 0, ",")
    tmpTyp = GetRealItem(tmpTag, 1, ",")
    Select Case tmpFunc
        Case "TAB_READ"
            HandleTabRead CurForm, CurButton
        Case "AUTO_TAB_READ"
            HandleAutoTabRead CurForm, CurButton
        Case "TAB_EDIT_INSERT"
            HandleTabEditInsert CurForm, CurButton
        Case "TAB_EDIT_UPDATE"
            HandleTabEditUpdate CurForm, CurButton
        Case "AUTO_SAVEAODB"
            HandleAutoSaveAodb CurForm, CurButton
        Case "EXCEL", "TO_EXCEL"
            HandleExcel CurForm, CurButton
        Case "TO_WORD"
            HandleMsWord CurForm, CurButton
        Case "BOOKMARK"
            MyBookMarks.Hide
        Case "SHOW_PROGRESS"
            WorkProgress.Hide
        Case "AUTO_RESTORE"
            AutoRestore = False
        Case "RESET_TABS"
            CurButton.Caption = "Reset"
        Case "GOCC_CHART"
            HandleOpenGoccChart CurForm, CurButton, False
        Case "CONX_CHART"
            HandleOpenConxChart CurForm, CurButton, False
        Case Else
    End Select
End Sub
Private Sub HandleOpenGoccChart(CurForm As Form, CurButton As CheckBox, ShowForm As Boolean)
    Dim FirstCall As Boolean
    Screen.MousePointer = 11
    FirstCall = False
    If FormIsLoaded("StatusChart") = False Then
        MsgPanel(0).Left = CurButton.Left + (CurButton.Width / 2)
        MsgPanel(0).Top = CurButton.Top + CurButton.Height + 210
        MsgPanel(1).Left = MsgPanel(0).Left + 60
        MsgPanel(1).Top = MsgPanel(0).Top + 60
        MsgPanel(1).ZOrder
        MsgPanel(0).ZOrder
        MsgPanel(0).Visible = True
        MsgPanel(1).Visible = True
        MsgPanel(1).Refresh
        MsgPanel(0).Refresh
        Load StatusChart
        FirstCall = True
        MsgPanel(1).Visible = False
        MsgPanel(0).Visible = False
    End If
    Screen.MousePointer = 11
    If ShowForm = True Then
        StatusChart.Show
        StatusChart.Form_Activate
        StatusChart.SetFocus
        StatusChart.Refresh
        If FirstCall = True Then
            StatusChart.GetFlightRotations
        End If
    Else
        StatusChart.Hide
    End If
    Screen.MousePointer = 0
End Sub
Private Sub HandleOpenConxChart(CurForm As Form, CurButton As CheckBox, ShowForm As Boolean)
    Dim FirstCall As Boolean
    Screen.MousePointer = 11
    FirstCall = False
    If FormIsLoaded("ConnexChart") = False Then
        MsgPanel(0).Left = CurButton.Left + (CurButton.Width / 2)
        MsgPanel(0).Top = CurButton.Top + CurButton.Height + 210
        MsgPanel(1).Left = MsgPanel(0).Left + 60
        MsgPanel(1).Top = MsgPanel(0).Top + 60
        MsgPanel(1).ZOrder
        MsgPanel(0).ZOrder
        MsgPanel(0).Visible = True
        MsgPanel(1).Visible = True
        MsgPanel(1).Refresh
        MsgPanel(0).Refresh
        Load ConnexChart
        FirstCall = True
        MsgPanel(1).Visible = False
        MsgPanel(0).Visible = False
    End If
    Screen.MousePointer = 11
    If ShowForm = True Then
        ConnexChart.Show
        ConnexChart.Form_Activate
        ConnexChart.SetFocus
        ConnexChart.Refresh
        If FirstCall = True Then
            ConnexChart.GetFlightRotations
        End If
    Else
        ConnexChart.Hide
    End If
    Screen.MousePointer = 0
End Sub
Private Sub HandleResetAllTabs(CurForm As Form, CurButton As CheckBox)
    Dim i As Integer
    If CurButton.Enabled = True Then
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                FileData(i).ResetContent
                FileData(i).Refresh
            End If
        Next
        If fraFolderPanel(0).Visible = True Then
            cboCedaCmd(0).Text = ""
            For i = 0 To txtCedaCmd.UBound
                txtCedaCmd(i).Text = ""
            Next
        End If
        RotationData.ResetContent
        RotationData.Refresh
        ConnexData.ResetContent
        ConnexData.Refresh
        CurButton.Value = 0
    Else
        'CurButton.Caption = "Auto"
    End If
End Sub
Private Sub HandleMsWord(CurForm As Form, CurButton As CheckBox)
    Dim tmpLine As String
  
    On Error Resume Next
    If (Not (MsWord Is Nothing)) Or (CurButton.Value = 0) Then
        If (Not (MsWord Is Nothing)) Then
            MsWordShutDown = True
            MsWord.Quit wdDoNotSaveChanges
            MsWordShutDown = False
            Set MsWord = Nothing
        End If
        MsWordIsOpen = False
        OnTop.Enabled = True
    End If
    
    If CurButton.Value = 1 Then
        OnTop.Value = 0
        OnTop.Enabled = False
    
        Set MsWord = New Word.Application
        MsWord.Visible = True
        MsWordIsOpen = True
        
        MsWord.Documents.Add Template:="Normal", NewTemplate:=False, DocumentType:=0
        MsWord.Selection.Style = MsWord.ActiveDocument.Styles("Heading 1")
        MsWord.Selection.TypeText Text:="An empty page for any purposes..." & vbNewLine
        
        'MsWord.Selection.Fields.Add Range:=MsWord.Selection.Range, Type:=wdFieldEmpty, Text:="TOC", PreserveFormatting:=True
        
        
        'MsWord.Selection.Style = MsWord.ActiveDocument.Styles("Heading 2")
        'MsWord.Selection.TypeText Text:="..." & vbNewLine
        
        'MsWord.Selection.Style = MsWord.ActiveDocument.Styles("Heading 3")
        'MsWord.Selection.TypeText Text:="To Do:" & vbNewLine
        
        MsWord.Selection.Style = MsWord.ActiveDocument.Styles("Normal")
        MsWord.Selection.Font.Name = "Arial"
        MsWord.Selection.Font.Size = 10
        MsWord.Selection.TypeText Text:=vbNewLine
        
        MsWord.Selection.TypeText Text:="You might copy and paste data from your Excel export and create a printout using MS Word." & vbNewLine
       
        'tmpLine = ""
        'tmpLine = tmpLine & "DATA1" & vbTab & "DATA2" & vbTab & "DATA3"
        'MsWord.Selection.TypeText Text:=tmpLine & vbNewLine & vbNewLine
        'MsWord.Selection.TypeText Text:=tmpLine & vbNewLine
        
        MsWord.Selection.TypeText Text:=vbNewLine
        
        MsWord.ActiveDocument.Tables.Add Range:=MsWord.Selection.Range, NumRows:=10, NumColumns:=5, DefaultTableBehavior:=wdWord9TableBehavior, AutoFitBehavior:=wdAutoFitFixed
        With MsWord.Selection.Tables(1)
            If .Style <> "Table Grid" Then
                .Style = "Table Grid"
            End If
            .ApplyStyleHeadingRows = True
            .ApplyStyleLastRow = True
            .ApplyStyleFirstColumn = True
            .ApplyStyleLastColumn = True
        End With
        
        
    End If
    
End Sub

Private Sub HandleTabPrint(CurForm As Form, CurButton As CheckBox)
    If FipsIsConnected Then
        HandleTabPrintFips CurForm, CurButton
    Else
        HandleTabPrintFlights CurForm, CurButton
    End If
End Sub

Private Sub HandleTabPrintFips(CurForm As Form, CurButton As CheckBox)
    Dim Index As Integer
    Dim CurOnTopValue As Integer
    Dim CurFields As String
    Dim CurHead As String
    Dim CurAlign As String
    Dim CurWidth As String
    Dim tmpPrintFields As String
    Dim tmpCurFields As String
    Dim tmpCurHead As String
    Dim tmpCurAlign As String
    Dim tmpCurWidth As String
    Dim tmpArrData As String
    Dim tmpDepData As String
    Dim tmpActData As String
    Dim FieldCnt As Long
    Dim ItemNo As Long
    Dim tmpFldNam As String
    Dim tmpData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpCapt As String
    
    tmpPrintFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
    tmpPrintFields = GetRealItem(tmpPrintFields, 3, "|")
    
    tmpCurFields = FileData(LastFocusIndex).LogicalFieldList
    tmpCurHead = FileData(LastFocusIndex).GetHeaderText
    tmpCurWidth = FileData(LastFocusIndex).HeaderLengthString
    tmpCurAlign = FileData(LastFocusIndex).ColumnAlignmentString
    
    CurFields = ""
    CurHead = ""
    CurWidth = ""
    CurAlign = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    While tmpFldNam <> ""
        CurFields = CurFields & tmpFldNam & ","
        CurHead = CurHead & GetFieldValue(tmpFldNam, tmpCurHead, tmpCurFields) & ","
        CurWidth = CurWidth & GetFieldValue(tmpFldNam, tmpCurWidth, tmpCurFields) & ","
        CurAlign = CurAlign & GetFieldValue(tmpFldNam, tmpCurAlign, tmpCurFields) & ","
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    Wend
    FieldCnt = ItemCount(CurFields, ",") - 1
    tmpArrData = ""
    tmpDepData = ""
    For Index = 0 To 5
        tmpArrData = tmpArrData & lblArr(Index).Caption & ","
        tmpDepData = tmpDepData & lblDep(Index).Caption & ","
    Next
    tmpActData = ""
    For Index = 6 To 8
        tmpData = lblArr(Index).Caption
        If tmpData = "" Then tmpData = lblDep(Index).Caption
        tmpActData = tmpActData & tmpData & ","
    Next
    tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
    tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
    If tmpCall = "ROT" Then tmpFltRel = tmpCode Else tmpFltRel = tmpCall
    Select Case tmpFltRel
        Case "ARR"
            tmpDepData = "--,--,--,--,--,--,"
        Case "DEP"
            tmpArrData = "--,--,--,--,--,--,"
        Case Else
    End Select
    tmpCapt = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
    tmpCapt = GetRealItem(tmpCapt, 1, "|")
    
    CurOnTopValue = OnTop.Value
    OnTop.Value = 0
    Dim rpt As New PrintData
    Load rpt
    rpt.InitReportHeader "FIPS", "Print Report: " & Me.Caption, tmpCapt, tmpArrData, tmpDepData, tmpActData, "", ""
    rpt.InitPrintLayout FileData(LastFocusIndex), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "", CLng(FontSlider.Value + 6)
    rpt.Show , Me
    rpt.Refresh
    OnTop.Value = CurOnTopValue
    CurButton.Value = 0
End Sub

Private Sub HandleTabPrintFlights(CurForm As Form, CurButton As CheckBox)
    Dim tmpRptCaption As String
    Dim Index As Integer
    Dim CurOnTopValue As Integer
    Dim TabName As String
    Dim tmpSection As String
    Dim CurFields As String
    Dim CurHead As String
    Dim CurAlign As String
    Dim CurWidth As String
    Dim tmpPrintFields As String
    Dim tmpCurFields As String
    Dim tmpCurHead As String
    Dim tmpCurAlign As String
    Dim tmpCurWidth As String
    Dim tmpArrData As String
    Dim tmpDepData As String
    Dim tmpActData As String
    Dim FieldCnt As Long
    Dim ItemNo As Long
    Dim tmpFldNam As String
    Dim tmpData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpCapt As String
    Dim tmpGroup As String
    Dim tmpItem1 As String
    Dim tmpItem2 As String
    Dim PrintLayout As String
    Dim DatTabNam As String
    Dim DatTabFld As String
    Dim DatTabIdx As Integer
    Dim tmpTurnType As String
    Dim FltTabNam As String
    Dim FltTabFld As String
    Dim FltTabIdx As Integer
    Dim LineNo As Long
    Dim i As Integer
    
    On Error Resume Next
    RecoverFileWrite CurButton.Index, "CallPrint", False
    
    tmpSection = myIniSection & "_PRINT"
    TabName = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "NAME")
    
    tmpData = GetIniEntry(myIniFullName, tmpSection, "", "ENTRY_POINT", "")
    i = 0
    tmpGroup = "START"
    While tmpGroup <> ""
        i = i + 1
        tmpGroup = GetItem(tmpData, i, "|")
        If tmpGroup <> "" Then
            tmpItem1 = GetItem(tmpGroup, 1, ":")
            tmpItem2 = GetItem(tmpGroup, 2, ":")
            If InStr(tmpItem1, TabName) > 0 Then
                PrintLayout = tmpItem2
                tmpGroup = ""
            End If
        End If
    Wend
    
    tmpCapt = ""
    tmpData = ""
    If CurPrintLayout <> "" Then
        tmpCapt = GetIniEntry(myIniFullName, tmpSection, "", CurPrintLayout & "_" & PrintLayout & "_PAGE_TITLE", "")
        tmpData = GetIniEntry(myIniFullName, tmpSection, "", CurPrintLayout & "_" & PrintLayout & "_PRINT_LIST", "")
    End If
    If tmpCapt = "" Then tmpCapt = GetIniEntry(myIniFullName, tmpSection, "", PrintLayout & "_PAGE_TITLE", Me.Caption)
    If tmpData = "" Then tmpData = GetIniEntry(myIniFullName, tmpSection, "", PrintLayout & "_PRINT_LIST", "")
    DatTabNam = GetItem(tmpData, 1, ":")
    DatTabFld = GetItem(tmpData, 2, ":")
    DatTabIdx = GetTabIdxByName(DatTabNam)
    
    tmpPrintFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
    tmpPrintFields = DatTabFld
    
    tmpCurFields = FileData(DatTabIdx).LogicalFieldList
    tmpCurHead = FileData(DatTabIdx).GetHeaderText
    tmpCurWidth = FileData(DatTabIdx).HeaderLengthString
    tmpCurAlign = FileData(DatTabIdx).ColumnAlignmentString
    
    CurFields = ""
    CurHead = ""
    CurWidth = ""
    CurAlign = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    While tmpFldNam <> ""
        CurFields = CurFields & tmpFldNam & ","
        CurHead = CurHead & GetFieldValue(tmpFldNam, tmpCurHead, tmpCurFields) & ","
        CurWidth = CurWidth & GetFieldValue(tmpFldNam, tmpCurWidth, tmpCurFields) & ","
        CurAlign = CurAlign & GetFieldValue(tmpFldNam, tmpCurAlign, tmpCurFields) & ","
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    Wend
    FieldCnt = ItemCount(CurFields, ",") - 1
    
    tmpArrData = ""
    tmpDepData = ""
    For Index = 0 To 5
        tmpArrData = tmpArrData & lblArr(Index).Caption & ","
        tmpDepData = tmpDepData & lblDep(Index).Caption & ","
    Next
    tmpActData = ""
    For Index = 6 To 8
        tmpData = lblArr(Index).Caption
        If tmpData = "" Then tmpData = lblDep(Index).Caption
        tmpActData = tmpActData & tmpData & ","
    Next
    tmpTurnType = "ROT"
    If chkSelFlight(0).Value = 1 Then tmpTurnType = "ARR"
    If chkSelFlight(1).Value = 1 Then tmpTurnType = "DEP"
    
    CurOnTopValue = OnTop.Value
    OnTop.Value = 0
    Dim rpt As New PrintData
    'Set rpt = New PrintData
    Load rpt
    'Load PrintData
    Select Case CurPrintLayout
        Case "PRT_FLTS"
            tmpRptCaption = "Print Report: " & Me.Caption & " (Flights)"
            rpt.InitReportHeader "FLTS", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            rpt.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S1'", CLng(FontSlider.Value + 6)
            'PrintData.InitReportHeader "FLTS", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            'PrintData.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S1'", CLng(FontSlider.Value + 6)
        Case "PRT_USER"
            tmpRptCaption = "Print Report: " & Me.Caption & " (User)"
            LineNo = FileData(DatTabIdx).GetCurrentSelected
            tmpData = FileData(DatTabIdx).GetFieldValue(LineNo, "USEC")
            tmpCapt = tmpCapt & " ( Login Name: " & tmpData & " )"
            tmpArrData = "--,--,--,--,--,--,"
            tmpDepData = "--,--,--,--,--,--,"
            tmpActData = "--,--,--,"
            rpt.InitReportHeader "USER", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            rpt.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S2'", CLng(FontSlider.Value + 6)
            'PrintData.InitReportHeader "XUSER", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            'PrintData.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S2'", CLng(FontSlider.Value + 6)
        Case Else
            tmpRptCaption = "Print Report: " & Me.Caption & " (Default)"
            rpt.InitReportHeader "FLTS", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            rpt.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S1'", CLng(FontSlider.Value + 6)
            'PrintData.InitReportHeader "FLTS", tmpRptCaption, tmpCapt, tmpArrData, tmpDepData, tmpActData, tmpTurnType, CurrentTimeZone
            'PrintData.InitPrintLayout FileData(DatTabIdx), FieldCnt, CurHead, CurFields, CurAlign, CurWidth, "'S1'", CLng(FontSlider.Value + 6)
    End Select
    rpt.Show , Me
    'rpt.Show vbModal, Me
    'Unload rpt
    'Set rpt = Nothing
    'PrintData.Show vbModal, Me
    'Unload PrintData
    
    OnTop.Value = CurOnTopValue
    CurButton.Value = 0
End Sub

Private Sub HandleTabEditInsert(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    Dim ColNo As Long
    If CurButton.Enabled = True Then
        If CurButton.Value = 1 Then
            PushWorkButton "TAB_EDIT_UPDATE", 0
            PushWorkButton "AUTO_TAB_READ", 0
            TabEditInline = True
            TabEditInsert = True
            TabEditUpdate = False
            If LastFocusIndex >= 0 Then
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
                FileData(LastFocusIndex).EnableInlineEdit True
                FileData(LastFocusIndex).PostEnterBehavior = 3
                FileData(LastFocusIndex).InplaceEditSendKeyEvents = True
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If LineNo >= 0 Then
                    InsertEmptyTabLine LastFocusIndex, LineNo
                Else
                    LineNo = FileData(LastFocusIndex).GetLineCount - 1
                    FileData(LastFocusIndex).OnVScrollTo LineNo - VisibleTabLines(FileData(LastFocusIndex)) + 1
                End If
                ColNo = GetFirstEditColNo(LastFocusIndex)
                If ColNo >= 0 Then
                    FileData(LastFocusIndex).SetCurrentSelection LineNo
                    FileData(LastFocusIndex).SetLineTag LineNo, FileData(LastFocusIndex).GetLineValues(LineNo)
                    FileData(LastFocusIndex).SetFocus
                    FileData(LastFocusIndex).SetInplaceEdit LineNo, ColNo, False
                    TabEditIsActive = True
                    TabEditLineNo = LineNo
                    TabEditColNo = ColNo
                Else
                    CurButton.Value = 0
                End If
            End If
        Else
            If LastFocusIndex >= 0 Then
                FileData(LastFocusIndex).EnableInlineEdit False
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
            End If
            TabEditInline = False
            TabEditInsert = False
            TabEditIsActive = False
            TabEditLineNo = -1
            TabEditColNo = -1
        End If
    End If
End Sub
Private Sub InsertEmptyTabLine(Index As Integer, UseLineNo As Long)
    Dim tmpData As String
    Dim LineNo As Long
    If UseLineNo >= 0 Then LineNo = UseLineNo Else LineNo = FileData(Index).GetLineCount
    tmpData = CreateEmptyLine(FileData(Index).LogicalFieldList)
    FileData(Index).InsertTextLineAt LineNo, tmpData, False
    FileData(Index).SetLineStatusValue LineNo, 99
End Sub
Private Sub RemoveEmptyTabLines(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim SelLine As Long
    Dim LineStatus As Long
    SelLine = FileData(Index).GetCurrentSelected
    MaxLine = FileData(Index).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        LineStatus = FileData(Index).GetLineStatusValue(CurLine)
        If LineStatus = 99 Then
            FileData(Index).DeleteLine CurLine
            If CurLine < SelLine Then SelLine = SelLine - 1
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    If SelLine >= 0 Then FileData(Index).SetCurrentSelection SelLine
End Sub
Private Sub HandleTabEditUpdate(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    Dim ColNo As Long
    If CurButton.Enabled = True Then
        If CurButton.Value = 1 Then
            PushWorkButton "TAB_EDIT_INSERT", 0
            PushWorkButton "AUTO_TAB_READ", 0
            TabEditInline = True
            TabEditUpdate = True
            TabEditInsert = False
            If LastFocusIndex >= 0 Then
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
                TabEditLineNo = -1
                TabEditColNo = -1
                FileData(LastFocusIndex).EnableInlineEdit True
                FileData(LastFocusIndex).PostEnterBehavior = 3
                FileData(LastFocusIndex).InplaceEditSendKeyEvents = True
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If LineNo >= 0 Then
                    ColNo = GetFirstEditColNo(LastFocusIndex)
                    If ColNo >= 0 Then
                        FileData(LastFocusIndex).SetLineTag LineNo, FileData(LastFocusIndex).GetLineValues(LineNo)
                        FileData(LastFocusIndex).SetFocus
                        FileData(LastFocusIndex).SetInplaceEdit LineNo, ColNo, False
                        TabEditIsActive = True
                        TabEditLineNo = LineNo
                        TabEditColNo = ColNo
                    End If
                End If
            End If
        Else
            If LastFocusIndex >= 0 Then
                FileData(LastFocusIndex).EnableInlineEdit False
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
            End If
            TabEditInline = False
            TabEditUpdate = False
            TabEditIsActive = False
            TabEditLineNo = -1
        End If
    End If
End Sub
Private Sub HandleAutoSaveAodb(CurForm As Form, CurButton As CheckBox)
    If CurButton.Value = 1 Then
        AutoSaveAodb = True
    Else
        AutoSaveAodb = False
    End If
End Sub
Private Sub HandleAutoTabRead(CurForm As Form, CurButton As CheckBox)
    Dim DoIt As Boolean
    If CurButton.Value = 1 Then
        DoIt = False
        If CurButton.Enabled Then
            If LastFocusIndex >= 0 Then
                If InStr(FileData(LastFocusIndex).LogicalFieldList, "READ") > 0 Then
                    DoIt = True
                End If
            End If
        End If
        If DoIt Then
            PushWorkButton "TAB_EDIT_UPDATE", 0
            PushWorkButton "TAB_EDIT_INSERT", 0
            PushWorkButton "AUTO_SAVEAODB", 1
            AutoTabRead = True
        Else
            CurButton.Value = 0
        End If
    Else
        AutoTabRead = False
        PushWorkButton "AUTO_SAVEAODB", 0
    End If
End Sub
Private Sub HandleExcel(CurForm As Form, CurButton As CheckBox)
    Dim CurFile As String
    Dim tmpFile As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CntLine As Long
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim NewSize As Long
    Dim CurGrid As Integer
    Dim GridCnt As Integer
    Dim i As Integer
    Dim j As Integer
    Dim iB1 As Integer
    Dim iB2 As Integer
    Dim iB3 As Integer
    Dim tmpTag As String
    Dim tmpTask As String
    Dim tmpFunc As String
    Dim tmpTagItem1 As String
    Dim tmpTagItem2 As String
    Dim tmpLineData As String
    Dim tmpData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpCapt As String
    Dim tmpType As String
    Dim tmpPath As String
    Dim tmpFlno As String
    Dim tmpDate As String
    Dim tmpChkVal As String
    Dim tmpChkFld As String
    Dim tmpBookContext As String
    Dim ChkCol As Long
    Dim ChkLin As Long
    Dim FirstBook As Integer
    Dim iFn As Integer
    Dim tmpFileName As String
    Dim fiLen As Long
    Dim fiBin As String
    
    On Error Resume Next
    If (Not (MsExcel Is Nothing)) Or (CurButton.Value = 0) Then
        If (Not (MsExcel Is Nothing)) Then
            ExcelShutDown = True
            MsWkBook.Saved = True
            MsExcel.Quit
            ExcelShutDown = False
        End If
        ExcelIsOpen = False
        OnTop.Enabled = True
    End If
    If CurButton.Value = 1 Then
        If FipsIsConnected Then
            OnTop.Value = 0
            OnTop.Enabled = False
            ExcelJustOpened = True
            tmpData = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_UPDATES", "NO")
            If tmpData = "YES" Then ExcelUpdateAllowed = True Else ExcelUpdateAllowed = False
            tmpData = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_RO_SAVE", "NO")
            If tmpData = "YES" Then ExcelRoSaveAllowed = True Else ExcelRoSaveAllowed = False
            tmpType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
            tmpType = GetRealItem(tmpType, 1, ",")
            tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
            tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
            If tmpCall = "ROT" Then tmpFltRel = tmpCode Else tmpFltRel = tmpCall
            Select Case tmpFltRel
                Case "ARR"
                    tmpFlno = Trim(Replace(lblArr(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                    tmpDate = Trim(Left(lblArr(2).Tag, 8))
                Case "DEP"
                    tmpFlno = Trim(Replace(lblDep(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                    tmpDate = Trim(Left(lblDep(2).Tag, 8))
                Case "ROT"
                    tmpFlno = Trim(Replace(lblDep(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                    tmpDate = Trim(Left(lblDep(2).Tag, 8))
                    If tmpFlno = "" Then
                        tmpFlno = Trim(Replace(lblArr(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                        tmpDate = Trim(Left(lblArr(2).Tag, 8))
                    End If
            End Select
            If tmpFlno = "" Then tmpFlno = "NONE"
            If tmpDate = "" Then tmpDate = "NONE"
            tmpPath = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_WKBOOKS", UFIS_TMP)
            CurFile = tmpPath & "\" & tmpType & "_" & tmpFltRel & "_" & tmpFlno & "_" & tmpDate & ".xls"
            Set MsExcel = New Application
            MsExcel.Workbooks.Add
            Set MsWkBook = MsExcel.Workbooks(1)
            Set MsWkSheet = MsWkBook.Worksheets(1)
            Kill CurFile
            MsWkBook.SaveAs CurFile
            MsExcel.WindowState = xlNormal
            MsExcel.Top = (Me.Top / 20) + 40
            MsExcel.Left = Me.Left / 20
            MsExcel.Width = Me.Width / 20
            NewSize = Me.Height / 20
            MsExcel.Height = NewSize
            MsExcel.Visible = True
            MsWkSheet.Name = "LogData"
            MsExcel.Windows.Arrange ArrangeStyle:=xlVertical
            MsExcel.Windows(1).WindowState = xlMaximized
            ExcelFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
            ExcelFields = GetRealItem(ExcelFields, 3, "|")
            MaxLine = FileData(LastFocusIndex).GetLineCount - 1
            MaxCol = ItemCount(ExcelFields, ",")
            MsWkSheet.Cells.NumberFormat = "@"
            For CurLine = 0 To MaxLine
                i = CInt(CurLine) + 1
                tmpLineData = FileData(LastFocusIndex).GetFieldValues(CurLine, ExcelFields)
                For CurCol = 1 To MaxCol
                    j = CInt(CurCol)
                    tmpData = GetItem(tmpLineData, j, ",")
                    tmpData = CleanString(tmpData, SERVER_TO_CLIENT, False)
                    MsWkSheet.Cells(i, j) = tmpData
                Next
            Next
            tmpData = "1:" & CStr(j)
            MsWkSheet.Range(tmpData).Columns.AutoFit
            tmpData = "1:" & CStr(i)
            MsWkSheet.Range(tmpData).Rows.AutoFit
            MsWkBook.Save
        Else
            CntLine = TabsFillCount(False, GridCnt)
            GridCnt = GridCnt + 1
            If CntLine > 0 Then
                OnTop.Value = 0
                OnTop.Enabled = False
                iB1 = 1
                iB2 = -1
                iB3 = -1
                If chkSelDeco(0).Value = 1 Then iB2 = 0
                If chkSelDeco(1).Value = 1 Then iB3 = 0
                tmpFunc = WorkProgress.SelectFeature("Transferring Data Grids to Excel ...", iB1, iB2, iB3, False, True)
                Select Case tmpFunc
                    Case "0,1,0,0,"
                        tmpChkFld = "'S1'"
                        tmpBookContext = "Flight"
                    Case "0,0,1,0,"
                        tmpChkFld = "'S2'"
                        tmpBookContext = "User"
                    Case "1,0,0,0,"
                        tmpChkFld = ""
                        tmpBookContext = "All"
                    Case "0,0,0,1,"
                        tmpChkFld = "'C3'"
                        tmpBookContext = "Marker"
                    Case Else
                        tmpChkFld = ""
                        tmpBookContext = ""
                End Select
                Me.Refresh
                    
                If tmpBookContext <> "" Then
                    tmpPath = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_WKBOOKS", UFIS_TMP)
                    CurFile = tmpPath & "\" & tmpBookContext & "AuditLog.xls"
                    tmpFile = CurFile
                    If InStr(CurFile, "?") > 0 Then
                        CurFile = Replace(CurFile, "?", "", 1, -1, vbBinaryCompare)
                        tmpFile = CurFile
                        CurFile = SaveLoadedFile(CurFile, "Excel Workbook", "Excel Workbook|*.xls")
                        Me.Refresh
                    End If
                    If CurFile <> "" Then
                        If CurFile <> tmpFile Then
                            tmpFile = Dir(CurFile)
                            If tmpFile <> "" Then
                                tmpData = "Do you want to overwrite the existing file?" & vbNewLine
                                tmpData = tmpData & ""
                                i = MyMsgBox.CallAskUser(0, 0, 0, "Data Transfer to Excel", tmpData, "stopit", "Yes,No", UserAnswer)
                                If i <> 1 Then CurFile = ""
                                OnTop.Value = 0
                                OnTop.Value = 1
                                OnTop.Value = 0
                            End If
                        End If
                    End If
                    If CurFile <> "" Then
                        Kill CurFile
                        
                        FirstBook = -1
                        WorkProgress.ResetDataGrid
                        WorkProgress.Show , Me
                        WorkProgress.CreateNewEntry "Application", "Excel", "Waiting For Interface", False
                        
                        ExcelJustOpened = True
                        Set MsExcel = New Application
                        MsExcel.Workbooks.Add
                        Set MsWkBook = MsExcel.Workbooks(1)
                        MsWkBook.SaveAs CurFile
                        MsExcel.WindowState = xlNormal
                        MsExcel.Top = (Me.Top / 20) + 25
                        MsExcel.Left = Me.Left / 20
                        NewSize = Me.Width / 20
                        If NewSize > 900 Then NewSize = 900
                        MsExcel.Width = NewSize
                        NewSize = Me.Height / 20
                        If NewSize > 300 Then NewSize = 300
                        MsExcel.Height = NewSize
                        MsExcel.Visible = True
                        MsExcel.Windows.Arrange ArrangeStyle:=xlVertical
                        MsExcel.Windows(1).WindowState = xlMaximized
                        
                        WorkProgress.ProgTab.SetFieldValues 0, "VALU,VAL1,VAL2", "Connection Established,1,1"
                        WorkProgress.Top = Me.Top + Me.Height - WorkProgress.Height - StatusBar.Height
                        Me.Refresh
                        WorkProgress.Refresh
                        
                        If GridCnt > MsWkBook.Worksheets.Count Then
                            MsWkBook.Worksheets.Add , , (GridCnt - MsWkBook.Worksheets.Count)
                        End If
                        
                        tmpTask = "Transfer to Excel"
                        GridCnt = 0
                        For CurGrid = 0 To FileData.UBound
                            If chkTabIsVisible(CurGrid).Value = 1 Then
                                SetProgressValue CurGrid, tmpTask, 0, 10, 0, 0, True
                                GridCnt = GridCnt + 1
                                Set MsWkSheet = MsWkBook.Worksheets(GridCnt)
                                tmpTag = CleanGridName(CurGrid)
                                MsWkSheet.Name = tmpTag
                                MsWkSheet.Activate
                                ExcelFields = FileData(CurGrid).LogicalFieldList
                                MaxLine = FileData(CurGrid).GetLineCount - 1
                                MaxCol = ItemCount(ExcelFields, ",")
                                ChkCol = -1
                                If tmpChkFld <> "" Then
                                    ChkCol = CLng(GetRealItemNo(FileData(CurGrid).LogicalFieldList, tmpChkFld))
                                End If
                                iFn = FreeFile
                                tmpFileName = UFIS_TMP & "\UfisToExcel.txt"
                                Open tmpFileName For Output As #iFn
                                ChkLin = 0
                                For CurLine = 0 To MaxLine
                                    If CurLine Mod 1000 = 0 Then
                                        SetProgressValue CurGrid, tmpTask, CurLine, MaxLine, CurLine, MaxLine, False
                                    End If
                                    tmpChkVal = "S"
                                    If ChkCol >= 0 Then
                                        tmpChkVal = FileData(CurGrid).GetColumnValue(CurLine, ChkCol)
                                    End If
                                    If tmpChkVal = "S" Then
                                        tmpLineData = FileData(CurGrid).GetFieldValues(CurLine, ExcelFields)
                                        tmpLineData = Replace(tmpLineData, ",", Chr(9), 1, -1, vbBinaryCompare)
                                        Print #iFn, tmpLineData
                                        ChkLin = ChkLin + 1
                                    End If
                                Next
                                Close #iFn
                                Open tmpFileName For Binary Access Read As #iFn
                                fiLen = LOF(iFn)
                                fiBin = Space(fiLen)
                                Get #iFn, 1, fiBin
                                Close #iFn
                                MsWkSheet.Cells.NumberFormat = "@"
                                Clipboard.Clear
                                Clipboard.SetText fiBin
                                MsWkSheet.Paste
                                Clipboard.Clear
                                fiBin = ""
                                MsExcel.Selection.Columns.AutoFit
                                MsWkSheet.Range("A1").Select
                                MaxLine = MaxLine + 1
                                SetProgressValue CurGrid, tmpTask, 10, 10, MaxLine, MaxLine, False
                                If (FirstBook < 1) And (ChkLin > 0) Then FirstBook = CurGrid + 1
                            End If
                        Next
                        WorkProgress.CreateNewEntry "-----------", "----------", "All Tasks Finished ------", False
                        
                        GridCnt = GridCnt + 1
                        Set MsWkSheet = MsWkBook.Worksheets(GridCnt)
                        MsWkSheet.Name = "GridHeaders"
                        MsWkSheet.Activate
                        MsWkSheet.Cells.NumberFormat = "@"
                        GridCnt = 0
                        For CurGrid = 0 To FileData.UBound
                            If chkTabIsVisible(CurGrid).Value = 1 Then
                                tmpTag = CleanGridName(CurGrid)
                                GridCnt = GridCnt + 1
                                Clipboard.Clear
                                Clipboard.SetText tmpTag
                                MsWkSheet.Paste Destination:=MsWkSheet.Range("A" & CStr(GridCnt))
                                ExcelFields = FileData(CurGrid).HeaderString
                                ExcelFields = Replace(ExcelFields, ",", Chr(9), 1, -1, vbBinaryCompare)
                                Clipboard.Clear
                                Clipboard.SetText ExcelFields
                                MsWkSheet.Paste Destination:=MsWkSheet.Range("B" & CStr(GridCnt))
                                GridCnt = GridCnt + 1
                                ExcelFields = FileData(CurGrid).LogicalFieldList
                                If InStr(ExcelFields, "VALU") > 0 Then
                                    ExcelFields = "(Times of Field 'VALU' in " & CurrentTimeZone & ")," & ExcelFields
                                Else
                                    ExcelFields = "," & ExcelFields
                                End If
                                ExcelFields = Replace(ExcelFields, ",", Chr(9), 1, -1, vbBinaryCompare)
                                Clipboard.Clear
                                Clipboard.SetText ExcelFields
                                MsWkSheet.Paste Destination:=MsWkSheet.Range("A" & CStr(GridCnt))
                                GridCnt = GridCnt + 1
                            End If
                        Next
                        MsWkSheet.Cells.Select
                        MsExcel.Selection.Columns.AutoFit
                        MsWkSheet.Range("A1").Select
                        If FirstBook < 1 Then FirstBook = 1
                        Set MsWkSheet = MsWkBook.Worksheets(FirstBook)
                        MsWkSheet.Activate
                        MsWkBook.Save
                        WorkProgress.Hide
                        ExcelIsOpen = True
                    Else
                        ExcelJustOpened = False
                        ExcelIsOpen = False
                        CurButton.Value = 0
                        OnTop.Value = 0
                        OnTop.Value = 1
                        OnTop.Value = 0
                        OnTop.Enabled = True
                    End If
                Else
                    ExcelJustOpened = False
                    ExcelIsOpen = False
                    CurButton.Value = 0
                    OnTop.Value = 0
                    OnTop.Value = 1
                    OnTop.Value = 0
                    OnTop.Enabled = True
                End If
            Else
                tmpData = "Sorry. There is nothing to be" & vbNewLine
                tmpData = tmpData & "transferred to Excel."
                If MyMsgBox.CallAskUser(0, 0, 0, "Data Transfer to Excel", tmpData, "stopit", "", UserAnswer) = 1 Then DoNothing
                ExcelJustOpened = False
                ExcelIsOpen = False
                CurButton.Value = 0
                OnTop.Value = 0
                OnTop.Value = 1
                OnTop.Value = 0
                OnTop.Enabled = True
            End If
        End If
    End If
End Sub

Private Sub HandleTabRead(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    If CurButton.Value = 1 Then
        If LastFocusIndex < 0 Then LastFocusIndex = 0
        ReadMask.SetReadOnly False
        Set ReadMaskButton = CurButton
        Set ReadMaskTab = FileData(LastFocusIndex)
        ReadMask.Left = Me.Left + ((Me.Width - ReadMask.Width) / 2)
        ReadMask.Top = Me.Top + ((Me.Height - ReadMask.Height) / 2)
        CheckMonitorArea ReadMask
        ReadMask.Show , Me
        SetFormOnTop ReadMask, True
        ReadMaskIsOpen = True
        If LastFocusIndex >= 0 Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then LineNo = 0
            FileData(LastFocusIndex).SetCurrentSelection LineNo
        End If
    Else
        Unload ReadMask
        ReadMaskIsOpen = False
    End If
End Sub
Private Sub HandleSaveAodb(UsingRelCmd As Boolean)
    Dim UseRelCmd As Boolean
    Dim CntInsert As Long
    Dim CntUpdate As Long
    Dim CntDelete As Long
    Dim CurLine As Long
    Dim tmpUrno As String
    Dim tmpArea As String
    Dim tmpMaskType As String
    Dim tmpIndx As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpSqlTab As String
    Dim tmpSqlFld As String
    Dim tmpSqlDat As String
    Dim tmpSqlKey As String
    Dim tmpRelHdlCmd As String
    Dim Index As Integer
    
    'UfisServer.ConnectToCeda
    For Index = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(Index).Visible Then
            UseRelCmd = UsingRelCmd
            Screen.MousePointer = 11
            SetSortLineNoAsc Index
            tmpSqlTab = HiddenData.GetConfigValues(FileData(Index).myName, "DTAB")
            tmpSqlFld = HiddenData.GetConfigValues(FileData(Index).myName, "DFLD")
            tmpSqlFld = CreateCleanFieldList(tmpSqlFld)
            tmpRelHdlCmd = ""
            FileData(Index).SetInternalLineBuffer True
            CntInsert = Val(FileData(Index).GetLinesByStatusValue(2, 0))
            If CntInsert > 0 Then
                UfisServer.UrnoPoolInit 50
                UfisServer.UrnoPoolPrepare CntInsert
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",IRT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & vbLf
                While CntInsert >= 0
                    CntInsert = FileData(Index).GetNextResultLine
                    If CntInsert >= 0 Then
                        tmpUrno = UfisServer.UrnoPoolGetNext
                        FileData(Index).SetFieldValues CntInsert, "URNO", tmpUrno
                        tmpSqlDat = FileData(Index).GetFieldValues(CntInsert, tmpSqlFld)
                        tmpSqlDat = CleanNullValues(tmpSqlDat)
                        tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & vbLf
                        FileData(Index).SetLineColor CntInsert, vbBlack, vbWhite
                        FileData(Index).SetLineStatusValue CntInsert, 0
                    End If
                Wend
            End If
            CntUpdate = Val(FileData(Index).GetLinesByStatusValue(1, 0))
            'If CntUpdate < 3 Then
            '    UseRelCmd = False
            'End If
            If CntUpdate > 0 Then
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",URT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & ",[URNO=:VURNO]" & vbLf
                While CntUpdate >= 0
                    CntUpdate = FileData(Index).GetNextResultLine
                    If CntUpdate >= 0 Then
                        tmpUrno = FileData(Index).GetFieldValue(CntUpdate, "URNO")
                        tmpSqlDat = FileData(Index).GetFieldValues(CntUpdate, tmpSqlFld)
                        tmpSqlDat = CleanNullValues(tmpSqlDat)
                        tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & "," & tmpUrno & vbLf
                        If Not UseRelCmd Then
                            tmpSqlKey = "WHERE URNO=" & tmpUrno
                            UfisServer.CallCeda CedaDataAnswer, "URT", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
                        End If
                        If Index = Val(DelayPanel.Tag) Then
                            If DelayPanel.Visible Then
                                tmpArea = FileData(Index).GetFieldValue(CntUpdate, "AREA")
                                If tmpArea = "B" Then
                                    tmpIndx = FileData(Index).GetFieldValue(CntUpdate, "INDX")
                                    CheckDlyIndex Index, CntUpdate, tmpIndx, False
                                End If
                            End If
                        End If
                        FileData(Index).SetLineColor CntUpdate, vbBlack, vbWhite
                        FileData(Index).SetLineStatusValue CntUpdate, 0
                    End If
                Wend
            End If
            CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
            If CntDelete > 0 Then
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",DRT,-1,[URNO=:VURNO]" & vbLf
                While CntDelete > 0
                    CntDelete = FileData(Index).GetNextResultLine
                    If CntDelete >= 0 Then
                        tmpUrno = FileData(Index).GetFieldValue(CntDelete, "URNO")
                        If tmpUrno <> "" Then
                            tmpRelHdlCmd = tmpRelHdlCmd & tmpUrno & vbLf
                        End If
                        FileData(Index).DeleteLine CntDelete
                    End If
                    CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
                Wend
            End If
            tmpMaskType = HiddenData.GetConfigValues(FileData(Index).myName, "MASK")
            tmpEditor = GetRealItem(tmpMaskType, 0, ",")
            tmpLayout = GetRealItem(tmpMaskType, 1, ",")
            If Not UseRelCmd Then tmpRelHdlCmd = ""
            If tmpRelHdlCmd <> "" Then
                UfisServer.CallCeda CedaDataAnswer, "REL", tmpSqlTab, tmpSqlFld, tmpRelHdlCmd, "LATE,NOBC,NOACTION", "", 0, False, False
                tmpSqlFld = ""
                tmpSqlDat = txtAftData(1).Text & "," & txtAftData(3).Text
                tmpSqlKey = UfisServer.CdrhdlSock.LocalHostName
                tmpSqlKey = tmpSqlKey & "," & CStr(App.ThreadID) & "," & tmpLayout
                tmpSqlKey = tmpSqlKey & "," & HiddenData.GetConfigValues(FileData(Index).myName, "CALL")
                UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
            End If
            FileData(Index).AutoSizeColumns
            FileData(Index).Refresh
            If Index = Val(DelayPanel.Tag) Then
                If DelayPanel.Visible Then
                    ClearDlyIndex Index
                    InitDelayPanel Index, -1, ""
                End If
            End If
            Select Case tmpLayout
                Case "MVLG"
                    SetMvlgStatistic Index
                Case "REQU"
                    SetRequStatistic Index
                Case Else
            End Select
            FileData(Index).SetInternalLineBuffer False
            Screen.MousePointer = 0
        End If
    Next
    CheckAftTabTriggers
    If AutoSaveAodb Then
        HighlightWorkButton "SAVEAODB", vbBlack, LightGreen
    Else
        HighlightWorkButton "SAVEAODB", vbButtonText, MyOwnButtonFace
    End If
    
    'UfisServer.DisconnectFromCeda
End Sub
Private Sub CheckDlyIndex(Index As Integer, LineNo As Long, IndxVal As String, DeleteIt As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpIndx As String
    Dim tmpArea As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        If CurLine <> LineNo Then
            tmpArea = FileData(Index).GetFieldValue(CurLine, "AREA")
            If tmpArea = "B" Then
                tmpIndx = FileData(Index).GetFieldValue(CurLine, "INDX")
                If tmpIndx = IndxVal Then
                    If DeleteIt Then
                        FileData(Index).SetLineStatusValue CurLine, 4
                    Else
                        FileData(Index).SetLineStatusValue CurLine, 3
                    End If
                End If
            End If
        End If
    Next
    If DeleteIt Then ClearDlyIndex Index
End Sub
Private Sub ClearDlyIndex(Index As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    MaxLine = FileData(Index).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If FileData(Index).GetLineStatusValue(CurLine) = 4 Then
            FileData(Index).DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    FileData(Index).Refresh
End Sub
Private Sub CheckAftTabTriggers()
    Dim tmpSqlTab As String
    Dim tmpSqlFld As String
    Dim tmpSqlDat As String
    Dim tmpSqlKey As String
    Dim tmpCapt As String
    Dim tmpTag As String
    Dim SaveArr As Boolean
    Dim SaveDep As Boolean
    Dim i As Integer
    SaveArr = False
    SaveDep = False
    For i = 10 To 12
        tmpCapt = Trim(lblArr(i).Caption)
        tmpTag = Trim(lblArr(i).Tag)
        If tmpCapt <> tmpTag Then SaveArr = True
        tmpCapt = Trim(lblDep(i).Caption)
        tmpTag = Trim(lblDep(i).Tag)
        If tmpCapt <> tmpTag Then SaveDep = True
    Next
    If SaveArr Then
        If txtAftData(1).Text <> "" Then
            tmpSqlFld = "BAA1,BAA2,BAA3"
            tmpSqlDat = ""
            tmpSqlDat = tmpSqlDat & lblArr(10).Tag & ","
            tmpSqlDat = tmpSqlDat & lblArr(11).Tag & ","
            tmpSqlDat = tmpSqlDat & lblArr(12).Tag
            tmpSqlKey = "WHERE URNO=" & txtAftData(1)
            tmpSqlTab = "AFTTAB"
            UfisServer.CallCeda CedaDataAnswer, "UFR", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        End If
    End If
    If SaveDep Then
        If txtAftData(3).Text <> "" Then
            tmpSqlFld = "BAA1,BAA2,BAA3"
            tmpSqlDat = ""
            tmpSqlDat = tmpSqlDat & lblDep(10).Tag & ","
            tmpSqlDat = tmpSqlDat & lblDep(11).Tag & ","
            tmpSqlDat = tmpSqlDat & lblDep(12).Tag
            tmpSqlKey = "WHERE URNO=" & txtAftData(3)
            tmpSqlTab = "AFTTAB"
            UfisServer.CallCeda CedaDataAnswer, "UFR", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        End If
    End If
    For i = 10 To 12
        lblArr(i).Caption = lblArr(i).Tag
        lblDep(i).Caption = lblDep(i).Tag
    Next
End Sub
Private Sub SetMvlgStatistic(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpCall As String
    Dim tmpText As String
    Dim tmpFlnu As String
    Dim ArrFlag As String
    Dim DepFlag As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
        tmpText = Trim(FileData(Index).GetFieldValue(CurLine, "TEXT"))
        If tmpFlnu = txtAftData(1).Text Then
            If tmpText <> "" Then ArrFlag = "X"
        End If
        If tmpFlnu = txtAftData(3).Text Then
            If tmpText <> "" Then DepFlag = "X"
        End If
    Next
    tmpCall = HiddenData.GetConfigValues(FileData(Index).myName, "CALL")
    If (tmpCall = "ARR") Or (tmpCall = "ROT") Then lblArr(10).Tag = ArrFlag
    If (tmpCall = "DEP") Or (tmpCall = "ROT") Then lblDep(10).Tag = DepFlag
End Sub
Private Sub SetRequStatistic(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ArrIsRead As Long
    Dim ArrUnRead As Long
    Dim DepIsRead As Long
    Dim DepUnRead As Long
    Dim tmpRead As String
    Dim tmpFlnu As String
    Dim tmpCheck As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpCheck = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
        If tmpCheck <> "," Then
            tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
            tmpRead = Trim(FileData(Index).GetFieldValue(CurLine, "READ"))
            If tmpFlnu = txtAftData(1) Then
                If tmpRead = "X" Then ArrUnRead = ArrUnRead + 1
                If tmpRead = "" Then ArrIsRead = ArrIsRead + 1
            End If
            If tmpFlnu = txtAftData(3) Then
                If tmpRead = "X" Then DepUnRead = DepUnRead + 1
                If tmpRead = "" Then DepIsRead = DepIsRead + 1
            End If
        End If
    Next
    If (chkSelFlight(0).Value = 1) Or (chkSelFlight(0).Value = chkSelFlight(1).Value) Then
        If ArrIsRead > 0 Then lblArr(11).Tag = CStr(ArrIsRead) Else lblArr(11).Tag = ""
        If ArrUnRead > 0 Then lblArr(12).Tag = CStr(ArrUnRead) Else lblArr(12).Tag = ""
    End If
    If (chkSelFlight(1).Value = 1) Or (chkSelFlight(0).Value = chkSelFlight(1).Value) Then
        If DepIsRead > 0 Then lblDep(11).Tag = CStr(DepIsRead) Else lblDep(11).Tag = ""
        If DepUnRead > 0 Then lblDep(12).Tag = CStr(DepUnRead) Else lblDep(12).Tag = ""
    End If
End Sub
Private Sub HandleTabInsert(CurForm As Form, CurButton As CheckBox)
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpFields As String
    Dim tmpDispFields As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpMapp As String
    Dim tmpData As String
    Dim tmpCapt As String
    Dim NewData As String
    Dim tmpTicked As String
    Dim MaxLine As Long
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
            tmpEditor = GetRealItem(tmpMaskType, 0, ",")
            tmpLayout = GetRealItem(tmpMaskType, 1, ",")
            tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
            tmpFields = GetRealItem(tmpFields, 0, "|")
            tmpMapp = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MAPP")
            tmpCapt = "Insert " & HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
            tmpCapt = GetRealItem(tmpCapt, 0, "|")
            tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
            tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
            If tmpCall = "ROT" Then
                If tmpCode = "ROT" Then tmpFltRel = "SET,DEP" Else tmpFltRel = "FIX," & tmpCode
            Else
                tmpFltRel = "FIX," & tmpCall
            End If
            tmpFltRel = tmpFltRel & "," & lblArr(0).Tag & "," & lblDep(0).Tag
            tmpData = ""
            tmpDispFields = "USEC,CDAT,USEU,LSTU"
            tmpDispData = ""
            NewData = EditMask.GetEditorValues(Me, tmpCapt, tmpLayout, tmpFltRel, tmpMapp, tmpData, tmpDispFields, tmpDispData)
            If NewData <> "" Then
                tmpTicked = GetRealItem(NewData, 1, vbLf)
                MaxLine = FileData(LastFocusIndex).GetLineCount
                NewLine = CreateEmptyLine(FileData(LastFocusIndex).LogicalFieldList)
                FileData(LastFocusIndex).InsertTextLine NewLine, False
                tmpFields = tmpFields & ",USEC,USEU,CDAT,LSTU,FLNU"
                NewData = GetRealItem(NewData, 0, vbLf)
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                Select Case tmpTicked
                    Case "0"
                        NewData = NewData & "," & txtAftData(1).Text
                    Case "1"
                        NewData = NewData & "," & txtAftData(3).Text
                    Case Else
                        NewData = NewData & ",0"
                End Select
                tmpMaskType = GetRealItem(tmpMaskType, 1, ",")
                Select Case tmpMaskType
                    Case "DLLG"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                    Case "DLYS"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "B"
                    Case "MVLG"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                    Case "REQU"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                        If InStr(tmpFields, "READ") = 0 Then
                            tmpFields = tmpFields & ",READ"
                            NewData = NewData & "," & "X"
                        End If
                        tmpFields = tmpFields & ",ADID,FLNO,STIM,ETIM"
                        Select Case tmpTicked
                            Case "0"
                                NewData = NewData & "," & "A"
                                NewData = NewData & "," & lblArr(0).Tag
                                NewData = NewData & "," & lblArr(2).Tag
                                NewData = NewData & "," & lblArr(4).Tag
                            Case "1"
                                NewData = NewData & "," & "D"
                                NewData = NewData & "," & lblDep(0).Tag
                                NewData = NewData & "," & lblDep(2).Tag
                                NewData = NewData & "," & lblDep(4).Tag
                            Case Else
                                NewData = NewData & ",A,??,,"
                        End Select
                    Case Else
                End Select
                FileData(LastFocusIndex).SetFieldValues MaxLine, tmpFields, NewData
                FileData(LastFocusIndex).SetLineStatusValue MaxLine, 2
                FileData(LastFocusIndex).SetLineColor MaxLine, vbBlack, LightGreen
                FileData(LastFocusIndex).OnVScrollTo MaxLine
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                FileData(LastFocusIndex).AutoSizeColumns
                FileData(LastFocusIndex).Refresh
                FileData(LastFocusIndex).OnVScrollTo MaxLine - VisibleTabLines(FileData(LastFocusIndex)) + 1
            End If
        End If
    End If
    CurButton.Value = 0
End Sub

Private Sub HandleTabUpdate(CurForm As Form, CurButton As CheckBox)
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpFields As String
    Dim tmpDispFields As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpMapp As String
    Dim tmpData As String
    Dim tmpCapt As String
    Dim tmpFlnu As String
    Dim tmpUrno As String
    Dim NewData As String
    Dim tmpTicked As String
    Dim CurStatus As Long
    Dim LineNo As Long
    Dim IsChanged As Boolean
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then
                If FileData(LastFocusIndex).GetLineCount > 0 Then
                    FileData(LastFocusIndex).SetCurrentSelection 0
                    LineNo = 0
                End If
            End If
            If LineNo >= 0 Then
                tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
                tmpEditor = GetRealItem(tmpMaskType, 0, ",")
                tmpLayout = GetRealItem(tmpMaskType, 1, ",")
                tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
                tmpFields = GetRealItem(tmpFields, 0, "|")
                tmpMapp = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MAPP")
                tmpCapt = "Update " & HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
                tmpCapt = GetRealItem(tmpCapt, 0, "|")
                tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
                tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
                tmpUrno = FileData(LastFocusIndex).GetFieldValue(LineNo, "URNO")
                tmpFlnu = FileData(LastFocusIndex).GetFieldValue(LineNo, "FLNU")
                If tmpCall = "ROT" Then
                    If tmpCode = "ROT" Then
                        If tmpFlnu = txtAftData(1) Then tmpCall = "ARR"
                        If tmpFlnu = txtAftData(3) Then tmpCall = "DEP"
                        tmpFltRel = "SET," & tmpCall
                    Else
                        tmpFltRel = "FIX," & tmpCode
                    End If
                Else
                    If tmpFlnu = txtAftData(1) Then tmpCall = "ARR"
                    If tmpFlnu = txtAftData(3) Then tmpCall = "DEP"
                    tmpFltRel = "FIX," & tmpCall
                End If
                tmpFltRel = tmpFltRel & "," & lblArr(0).Tag & "," & lblDep(0).Tag
                tmpFltRel = tmpFltRel & "," & tmpUrno
                
                tmpData = FileData(LastFocusIndex).GetFieldValues(LineNo, tmpFields)
                tmpDispFields = "USEC,CDAT,USEU,LSTU"
                tmpDispData = FileData(LastFocusIndex).GetFieldValues(LineNo, tmpDispFields)
                
                NewData = EditMask.GetEditorValues(Me, tmpCapt, tmpLayout, tmpFltRel, tmpMapp, tmpData, tmpDispFields, tmpDispData)
                
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If NewData <> "" Then
                    tmpTicked = GetRealItem(NewData, 1, vbLf)
                    NewData = GetRealItem(NewData, 0, vbLf)
                    IsChanged = False
                    Select Case tmpTicked
                        Case "0"
                            If tmpFlnu <> txtAftData(1) Then
                                tmpFields = tmpFields & ",FLNU"
                                NewData = NewData & "," & txtAftData(1).Text
                            End If
                        Case "1"
                            If tmpFlnu <> txtAftData(3) Then
                                tmpFields = tmpFields & ",FLNU"
                                NewData = NewData & "," & txtAftData(3).Text
                            End If
                        Case Else
                            NewData = NewData & ",0"
                    End Select
                    If NewData <> tmpData Then IsChanged = True
                    If IsChanged Then
                        CurStatus = FileData(LastFocusIndex).GetLineStatusValue(LineNo)
                        If CurStatus < 2 Then
                            tmpFields = tmpFields & ",USEU,LSTU"
                            NewData = NewData & "," & LoginUserName
                            NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                        End If
                        FileData(LastFocusIndex).SetFieldValues LineNo, tmpFields, NewData
                        If CurStatus < 2 Then
                            FileData(LastFocusIndex).SetLineStatusValue LineNo, 1
                            FileData(LastFocusIndex).SetLineColor LineNo, vbBlack, LightYellow
                        End If
                        HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    End If
                    FileData(LastFocusIndex).AutoSizeColumns
                    FileData(LastFocusIndex).Refresh
                End If
            End If
        End If
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleTabDelete(CurForm As Form, CurButton As CheckBox)
    Dim CurLine As Long
    Dim tmpCapt As String
    Dim tmpMsg As String
    Dim CurStatus As Long
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            CurLine = FileData(LastFocusIndex).GetCurrentSelected
            If CurLine >= 0 Then
                tmpCapt = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
                tmpCapt = GetRealItem(tmpCapt, 0, "|")
                tmpMsg = "Do you want to delete this entry?"
                CurStatus = FileData(LastFocusIndex).GetLineStatusValue(CurLine)
                
                If MyMsgBox.CallAskUser(0, 0, 0, tmpCapt, tmpMsg, "stop2", "Yes,No", UserAnswer) = 1 Then
                    Select Case CurStatus
                        'Case 1  'Update
                        Case 2  'Insert
                            FileData(LastFocusIndex).DeleteLine CurLine
                        Case 3  'Delete
                        Case Else
                            FileData(LastFocusIndex).SetLineStatusValue CurLine, 3
                            FileData(LastFocusIndex).SetLineColor CurLine, vbBlack, LightRed
                            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    End Select
                Else
                
                End If
            End If
        End If
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleAodbFilter(CurForm As Form, CurButton As CheckBox)
    Dim FilterResult As String
    Dim tmpPatchCodes As String
    Dim tmpPatchData As String
    Dim tmpTask As String
    Dim tmpZone As String
    Dim TabName As String
    Dim tmpData As String
    Dim tmpUniqueList As String
    Dim UniqueColumns As String
    Dim i As Integer
    Dim MaxLine As Long
    Dim ColNo As Long
    FilterResult = FilterDialog.InitMyLayout(Me, True, "AODB_FILTER")
    CurForm.Refresh
    StopAllLoops = False
    AodbCallInProgress = True
    
    If FilterResult <> "" Then
        tmpTask = WorkProgress.SelectFeature("Loading Data From Server ...", 1, 1, 1, True, False)
        
        WorkProgress.Show , Me
        Me.Refresh
        DoEvents
    
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                FileData(i).ResetContent
                FileData(i).ColSelectionRemoveAll
                FileData(i).ShowHorzScroller False
                FileData(i).ShowVertScroller False
                FileData(i).Refresh
                lblTabPos(i).Caption = ""
                lblTabPos(i).Tag = ""
                lblTabCnt(i).Caption = ""
                lblTabCnt(i).Tag = ""
            End If
        Next
        PushWorkButton "AUTO_SORT", 0
        ButtonPushedInternal = True
        chkSelFlight(0).Value = 0
        chkSelFlight(0).Value = 0
        ResetFlightPanel
        ButtonPushedInternal = False
        MyBookMarks.ResetAll
        Me.Refresh
        WorkProgress.ResetDataGrid
        tmpTask = "Waiting For Server"
        tmpPatchCodes = GetRealItem(FilterResult, 0, Chr(16))
        tmpPatchData = GetRealItem(FilterResult, 1, Chr(16))
        tmpZone = GetItem(tmpPatchData, 1, Chr(15))
        Select Case tmpZone
            Case "LOC"
                chkUtc(1).Value = 1
            Case "UTC"
                chkUtc(0).Value = 1
            Case Else
        End Select
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
                'Just a QUICKHACK (myIniSection is unknown here)
                tmpUniqueList = GetIniEntry(myIniFullName, myIniSection & "_LAYOUT", "", TabName & "_UNIQUE_DATA", "")
                UniqueColumns = GetFieldColNoList(FileData(i), tmpUniqueList)
                SetProgressValue i, tmpTask, 0, 10, 0, 0, True
                HiddenData.ReadAodbData FileData(i), False, UniqueColumns, tmpPatchCodes, tmpPatchData, True
                CleanSystemFields FileData(i)
                MaxLine = FileData(i).GetLineCount
                lblTabCnt(i).Caption = CStr(MaxLine)
                FileData(i).AutoSizeColumns
                FileData(i).Refresh
                TabLookUp(i).HeaderLengthString = FileData(i).HeaderLengthString
                Me.Refresh
                If MaxLine > 0 Then
                    SetProgressValue i, "Loading AODB Data", 10, 10, -1, MaxLine, False
                Else
                    WorkProgress.RemoveEntry
                End If
            End If
            If StopAllLoops Then Exit For
        Next
        If StopAllLoops Then
            For i = 0 To chkTabIsVisible.UBound
                If chkTabIsVisible(i).Visible Then
                    FileData(i).ResetContent
                    FileData(i).ColSelectionRemoveAll
                    FileData(i).ShowHorzScroller False
                    FileData(i).ShowVertScroller False
                    FileData(i).Refresh
                    lblTabPos(i).Caption = ""
                    lblTabPos(i).Tag = ""
                    lblTabCnt(i).Caption = ""
                    lblTabCnt(i).Tag = ""
                End If
            Next
        End If
        CheckDataSourceConfig
        TranslateLogTimeFieldValues
        Me.Refresh
        WorkProgress.CreateNewEntry "-----------", "----------", "Server Tasks Finished ------", False
        WorkProgress.CreateNewEntry "Internal", "All Grids", "Data Index Creation", False
        PushWorkButton "AUTO_SORT", 1
        Me.Refresh
        DoEvents
        CreateSynchroTabIndexes -1
        CheckAlternateColors -1
        AdjustServerSignals False, 1
        For i = 0 To FileData.UBound
            If chkTabIsVisible(i).Visible Then
                ColNo = CLng(GetRealItemNo(FileData(i).LogicalFieldList, "'S3'"))
                If ColNo >= 0 Then FileData(i).IndexCreate "BookMark", ColNo
                FileData(i).ShowHorzScroller True
                FileData(i).ShowVertScroller True
            End If
        Next
        WorkProgress.UpdateProgress 1, 1, 1, 1
        WorkProgress.CreateNewEntry "-----------", "----------", "Internal Tasks Ready ------", False
        WorkProgress.Hide
        Me.Refresh
        Screen.MousePointer = 0
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "SIMULATE_AFTVERS", "NO")
        If tmpData = "YES" Then Command2_Click
        If RotationActivated = True Then FillRotationGrid
        'QuickHack!
        On Error Resume Next
        If FileData(0).GetLineCount > 0 Then
            FileData(0).SetCurrentSelection 0
            chkSelFlight(0).Value = 1
        Else
            FileData(1).SetCurrentSelection 0
            chkSelFlight(1).Value = 1
        End If
        CurButton.Value = 0
        DontCheck = True
        PushWorkButton "SAVELOCAL", 1
        DontCheck = False
    End If
    If StopAllLoops Then WorkProgress.Hide
    StopAllLoops = False
    AodbCallInProgress = False
    CurButton.Value = 0
End Sub
Private Sub HandleReadAodb(CurForm As Form, CurButton As CheckBox)
    Dim FilterResult As String
    Dim tmpPatchCodes As String
    Dim tmpPatchData As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim LineNo As Long
    Dim SortColNo As String
    Dim i As Integer
    If (chkUtc(0).Value = 0) And (chkUtc(1).Value = 0) Then
        If FipsIsConnected Then
            If FipsShowsUtc Then
                chkUtc(0).Value = 1
                chkUtc(0).Refresh
            Else
                chkUtc(1).Value = 1
                chkUtc(1).Refresh
            End If
        End If
    End If
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            If (Not RefreshOnBc) Or (i = LastFocusIndex) Then
                FileData(i).ColSelectionRemoveAll
                FileData(i).ResetContent
                FileData(i).Refresh
            End If
        End If
    Next
    'Attention:
    'PatchCodes must be evaluated per TAB !!
    ' .... to do ....
    
    tmpPatchCodes = GetRealItem(FilterResult, 0, Chr(16))
    tmpPatchData = GetRealItem(FilterResult, 1, Chr(16))
    Select Case txtAftData(0).Text
        Case "ARR"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(1).Text
        Case "DEP"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(3).Text
        Case "ROT"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(1).Text & "," & txtAftData(3).Text
        Case Else
            tmpPatchCodes = ""
            tmpPatchData = ""
    End Select
    
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            If (Not RefreshOnBc) Or (i = LastFocusIndex) Then
                tmpMaskType = HiddenData.GetConfigValues(FileData(i).myName, "MASK")
                tmpLayout = GetRealItem(tmpMaskType, 1, ",")
                If tmpLayout = "REQU" Then
                    If chkSelFlight(0).Value <> chkSelFlight(1).Value Then
                        If chkSelFlight(0).Value = 1 Then tmpPatchData = txtAftData(1).Text
                        If chkSelFlight(1).Value = 1 Then tmpPatchData = txtAftData(3).Text
                    End If
                End If
                HiddenData.ReadAodbData FileData(i), False, "", tmpPatchCodes, tmpPatchData, True
                CleanSystemFields FileData(i)
                SortColNo = TranslateFieldItems("LINO", FileData(i).LogicalFieldList)
                If SortColNo <> "" Then FileData(i).Sort SortColNo, True, True
                FileData(i).AutoSizeColumns
                If InStr(FileData(i).LogicalFieldList, "LINO") > 0 Then InsertEmptyTabLine i, -1
                FileData(i).Refresh
                CheckMinMaxPanel i
                If DelayPanel.Visible Then
                    If Val(DelayPanel.Tag) = i Then
                        InitDelayPanel i, -1, ""
                    End If
                End If
            End If
        End If
    Next
    CreateSynchroTabIndexes -1
    If Not AutoSaveAodb Then HighlightWorkButton "SAVEAODB", vbButtonText, MyOwnButtonFace
    CurButton.Value = 0
End Sub

Private Sub HandleExtract(CurForm As Form, CurButton As CheckBox)
    Dim NewForm As MainDialog
    Set NewForm = New MainDialog
    NewForm.Caption = "Extract Data"
    NewForm.Tag = "EXTRACT,1"
    NewForm.chkAppl(0).Caption = "Close"
    NewForm.chkAppl(0).Tag = "CLOSE"
    Me.Refresh
    NewForm.Show
    CurButton.Value = 0
End Sub
Private Sub HandleSaveLocal(CurForm As Form, CurButton As CheckBox)
    Dim FileCount As Integer
    Dim myLoadName As String
    Dim myLoadPath As String
    Dim tmpTag As String
    Dim tmpImpTag As String
    Dim tmpImpCap As String
    Dim tmpDcut As String
    Dim tmpData As String
    Dim tmpFileName As String
    Dim CntLine As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim fn As Integer
    Dim i As Integer
    CntLine = TabsFillCount(True, i)
    If CntLine > 0 Then
        If CurImpButtonIndex >= 0 Then
            tmpImpTag = chkImpTyp(CurImpButtonIndex).Tag
            'tmpImpCap = Replace(chkImpTyp(CurImpButtonIndex).Caption, " ", "", 1, -1, vbBinaryCompare)
            tmpImpCap = chkImpTyp(CurImpButtonIndex).Caption
            myLoadPath = UFIS_TMP
            myLoadName = ApplShortCode & "_" & tmpImpTag & ".pth"
            tmpFileName = myLoadPath & "\" & myLoadName
            If DontCheck = False Then
                myLoadName = "USR" & "_" & tmpImpTag & ".pth"
                tmpFileName = myLoadPath & "\" & myLoadName
                tmpFileName = SaveLoadedFile(tmpFileName, tmpImpCap & " Backup", tmpImpCap & " Prefix|USR_" & tmpImpTag & "_*.txt")
            End If
            If tmpFileName <> "" Then
                FileCount = GetFileAndPath(tmpFileName, myLoadName, myLoadPath)
                myLoadName = Replace(myLoadName, ".pth", "", 1, -1, vbBinaryCompare)
                myLoadName = Replace(myLoadName, ".txt", "", 1, -1, vbBinaryCompare)
                'myLoadName = Replace(myLoadName, "_", "", 1, -1, vbBinaryCompare)
                myLoadName = Replace(myLoadName, " ", "", 1, -1, vbBinaryCompare)
                myLoadName = Replace(myLoadName, "&", "", 1, -1, vbBinaryCompare)
                myLoadName = Replace(myLoadName, "/", "", 1, -1, vbBinaryCompare)
                myLoadName = Replace(myLoadName, ".", "", 1, -1, vbBinaryCompare) & "_"
                Screen.MousePointer = 11
                For i = 0 To FileData.UBound
                    If chkTabIsVisible(i).Visible Then
                        tmpTag = CleanGridName(i)
                        tmpFileName = myLoadPath & "\" & myLoadName & tmpTag
                        FileData(i).WriteToFile tmpFileName & "_" & CStr(i) & ".txt", False
                    End If
                Next
                tmpFileName = myLoadPath & "\" & myLoadName & "Basics"
                For i = 0 To HiddenData.BasicDataTab.UBound
                    If HiddenData.BasicDataTab(i).GetLineCount > 0 Then
                        HiddenData.BasicDataTab(i).WriteToFile tmpFileName & "_" & CStr(i) & ".txt", False
                    End If
                Next
                fn = FreeFile
                tmpFileName = myLoadPath & "\" & myLoadName & "All_Bulk.txt"
                Open tmpFileName For Output As #fn
                For i = 0 To FileData.UBound
                    If chkTabIsVisible(i).Visible Then
                        'If InStr(FileData(i).LogicalFieldList, "DCUT") > 0 Then
                            MaxLine = FileData(i).GetLineCount - 1
                            For CurLine = 0 To MaxLine
                                'tmpDcut = Trim(FileData(i).GetFieldValue(CurLine, "DCUT"))
                                'If tmpDcut <> "" Then
                                    tmpTag = FileData(i).GetLineTag(CurLine)
                                    If Trim(tmpTag) <> "" Then
                                        tmpData = "{=IDX=}" & CStr(i) & "{=ROW=}" & CStr(CurLine) & "{=TAG=}" & tmpTag & "{="
                                        Print #fn, tmpData
                                    End If
                                'End If
                            Next
                        'End If
                    End If
                Next
                Close #fn
                
                fn = FreeFile
                tmpFileName = myLoadPath & "\" & myLoadName & "All_Data_Env.txt"
                Open tmpFileName For Output As #fn
                tmpData = "{=VPFR=}" & DataFilterVpfr & "{=VPTO=}" & DataFilterVpto
                Print #fn, tmpData
                Close #fn
                
                CurButton.Value = 0
                RecoverFileWrite -1, "Other", True
                Screen.MousePointer = 0
            End If
        End If
    Else
        'tmpData = "Sorry. There is nothing to be" & vbNewLine
        'tmpData = tmpData & "saved on the local disk."
        'If MyMsgBox.CallAskUser(0, 0, 0, "Local Backup Pre-Check", tmpData, "stopit", "", UserAnswer) = 1 Then DoNothing
        OnTop.Value = 0
        OnTop.Value = 1
        OnTop.Value = 0
        CurButton.Value = 0
    End If
End Sub
Private Sub HandleFileOpen(CurForm As Form, CurButton As CheckBox)
    Dim tmpFileResult As String
    Dim tmpFileFilter As String
    Dim tmpFilePath As String
    tmpFilePath = GetIniEntry(myIniFullName, myIniSection, "", "FILE_PATH", "")
    tmpFileFilter = GetIniEntry(myIniFullName, myIniSection, "", "FILE_FILTER", "{All Files}{*.*}")
    'tmpFileResult = FileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0)
    CurButton.Value = 0
End Sub
Private Sub HandleReadLocal(CurForm As Form, CurButton As CheckBox)
    Dim tmpData As String
    Dim tmpVal As String
    Dim tmpTag As String
    Dim tmpImpTag As String
    Dim tmpFileName As String
    Dim tmpFldNam As String
    Dim tmpTaskTitle As String
    Dim tmpTask As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ColNo As Long
    Dim i As Integer
    Dim j As Long
    Dim fn As Integer
    'On Error GoTo myExit
    On Error Resume Next
    tmpTaskTitle = "Restoring Grids From Disk ..."
    If AutoRecover = True Then tmpTaskTitle = RecoverTitle
    MaxLine = BookMarkTotal
    BookMarkTotal = 0
    'If TabsFillCount(True, i) > 0 Then
    '    tmpTask = WorkProgress.SelectFeature(tmpTaskTitle, 1, -1, -1, False, True)
    'Else
        tmpTask = WorkProgress.SelectFeature(tmpTaskTitle, 1, -1, -1, False, False)
        tmpTask = "1"
    'End If
    BookMarkTotal = MaxLine
    
    tmpTask = GetItem(tmpTask, 1, ",")
    If tmpTask = "1" Then
        WorkProgress.Show , Me
        Me.Refresh
        DoEvents
        
        Screen.MousePointer = 11
        tmpImpTag = chkImpTyp(CurImpButtonIndex).Tag
        If Not CedaIsConnected Then
            tmpFileName = UFIS_TMP & "\" & ApplShortCode & "_" & tmpImpTag & "_Basics"
            For i = 0 To HiddenData.BasicDataTab.UBound
                HiddenData.BasicDataTab(i).ResetContent
                HiddenData.BasicDataTab(i).ReadFromFile tmpFileName & "_" & CStr(i) & ".txt"
                For j = 0 To 1
                    tmpFldNam = GetRealItem(HiddenData.BasicDataTab(i).LogicalFieldList, j, ",")
                    HiddenData.BasicDataTab(i).IndexCreate tmpFldNam, j
                Next
            Next
        End If
        
        For i = 0 To FileData.UBound
            FileData(i).ResetContent
            FileData(i).ShowHorzScroller False
            FileData(i).ShowVertScroller False
            FileData(i).Refresh
            lblTabPos(i).Caption = ""
            lblTabPos(i).Tag = ""
            lblTabCnt(i).Caption = ""
            lblTabCnt(i).Tag = ""
        Next
        PushWorkButton "AUTO_SORT", 0
        ButtonPushedInternal = True
        chkSelFlight(0).Value = 0
        chkSelFlight(0).Value = 0
        ResetFlightPanel
        ButtonPushedInternal = False
        MyBookMarks.ResetAll
        Me.Refresh
        WorkProgress.ResetDataGrid
        tmpTask = "Reading From File"
        For i = 0 To FileData.UBound
            If chkTabIsVisible(i).Visible Then
                SetProgressValue i, tmpTask, 0, 100, 1, 0, True
                tmpTag = CleanGridName(i)
                tmpFileName = UFIS_TMP & "\" & ApplShortCode & "_" & tmpImpTag & "_" & tmpTag
                FileData(i).ReadFromFile tmpFileName & "_" & CStr(i) & ".txt"
                MaxLine = FileData(i).GetLineCount
                lblTabCnt(i).Caption = CStr(MaxLine)
                FileData(i).AutoSizeColumns
                FileData(i).Refresh
                TabLookUp(i).HeaderLengthString = FileData(i).HeaderLengthString
                Me.Refresh
                SetProgressValue i, tmpTask, 100, 100, 1, MaxLine, False
            End If
        Next
        tmpFileName = UFIS_TMP & "\" & ApplShortCode & "_" & tmpImpTag & "_All_Bulk.txt"
        fn = FreeFile
        Open tmpFileName For Input As #fn
        While Not EOF(fn)
            Line Input #fn, tmpData
            GetKeyItem tmpVal, tmpData, "{=IDX=}", "{="
            i = Val(tmpVal)
            GetKeyItem tmpVal, tmpData, "{=ROW=}", "{="
            CurLine = Val(tmpVal)
            GetKeyItem tmpTag, tmpData, "{=TAG=}", "{="
            FileData(i).SetLineTag CurLine, tmpTag
        Wend
        Close #fn
        
        fn = FreeFile
        tmpFileName = UFIS_TMP & "\" & ApplShortCode & "_" & tmpImpTag & "_All_Data_Env.txt"
        Open tmpFileName For Input As #fn
        Line Input #fn, tmpData
        GetKeyItem DataFilterVpfr, tmpData, "{=VPFR=}", "{="
        GetKeyItem DataFilterVpto, tmpData, "{=VPTO=}", "{="
        Close #fn
        
        WorkProgress.CreateNewEntry "-----------", "----------", "Data Tasks Ready ------", False
        Me.Refresh
        DoEvents
        WorkProgress.CreateNewEntry "Internal", "All Grids", "Data Index Creation", False
        PushWorkButton "AUTO_SORT", 1
        CreateSynchroTabIndexes -1
        CheckAlternateColors -1
        For i = 0 To FileData.UBound
            If chkTabIsVisible(i).Visible Then
                ColNo = CLng(GetRealItemNo(FileData(i).LogicalFieldList, "'S3'"))
                If ColNo >= 0 Then FileData(i).IndexCreate "BookMark", ColNo
                ColNo = CLng(GetRealItemNo(FileData(i).LogicalFieldList, "VALU"))
                If ColNo >= 0 Then FileData(i).SetColumnProperty ColNo, "ColMarker"
                CheckAftVersIcons FileData(i), -1
                CleanGridFields FileData(i)
                FileData(i).ShowHorzScroller True
                FileData(i).ShowVertScroller True
            End If
        Next
        WorkProgress.UpdateProgress 1, 1, 1, 1
        WorkProgress.CreateNewEntry "-----------", "----------", "Internal Tasks Ready ------", False
        WorkProgress.Hide
        Me.Refresh
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "SIMULATE_AFTVERS", "NO")
        If tmpData = "YES" Then Command2_Click
        If RotationActivated = True Then FillRotationGrid
        Screen.MousePointer = 0
        If FileData(0).GetLineCount > 0 Then
            FileData(0).SetCurrentSelection 0
        ElseIf FileData(1).GetLineCount > 0 Then
            FileData(1).SetCurrentSelection 0
        End If
        chkUtc(1).Value = 0
        chkUtc(1).Value = 1
    End If
    CurButton.Value = 0
    Exit Sub
myExit:
    'Unused
    WorkProgress.Hide
    Me.Refresh
    Screen.MousePointer = 0
    CurButton.Value = 0
End Sub

Private Sub FileData_CloseInplaceEdit(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        If TabEditLineNo >= 0 Then CheckChangesAndSave Index, TabEditLineNo
        If (Not TabEditHitEnter) And (Not TabEditInsert) Then
            TabEditIsActive = False
            TabEditLineNo = -1
            TabEditColNo = -1
        End If
    End If
End Sub

Private Sub FileData_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    Dim tmpData As String
    Dim LineStatus As Long
    If LineNo >= 0 Then
        TabEditIsActive = True
        LineStatus = FileData(Index).GetLineStatusValue(LineNo)
        tmpData = FileData(Index).GetLineTag(LineNo)
        If tmpData = "" Then
            tmpData = FileData(Index).GetLineValues(LineNo)
            FileData(Index).SetLineTag LineNo, tmpData
        End If
        If (LineStatus <> 99) Or (TabEditInsert = True) Then
            If ColNo <> TabEditColNo Then
                If TabEditColNo >= 0 Then SetAdidFlightRelation Index, LineNo, TabEditColNo
            End If
            If LineNo <> TabEditLineNo Then
                If TabEditLineNo >= 0 Then
                    CheckChangesAndSave Index, TabEditLineNo
                End If
            Else
                If ColNo = TabEditColNo Then
                    CheckChangesAndSave Index, TabEditLineNo
                End If
            End If
            TabEditIsActive = True
            TabEditLineNo = LineNo
            TabEditColNo = ColNo
            If (TabEditHitEnter = True) And (TabEditInsert = True) Then
                FileData(Index).EnableInlineEdit False
                If LineNo >= (FileData(Index).GetLineCount - 1) Then
                    TabEditLineNo = LineNo + 1
                End If
                InsertEmptyTabLine Index, TabEditLineNo
                tmpData = FileData(Index).GetLineValues(TabEditLineNo)
                FileData(Index).SetLineTag TabEditLineNo, tmpData
                TabEditColNo = GetFirstEditColNo(Index)
                FileData(Index).EnableInlineEdit True
                FileData(Index).SetCurrentSelection TabEditLineNo
                FileData(Index).SetInplaceEdit TabEditLineNo, TabEditColNo, False
                TabEditHitEnter = False
                TabEditIsActive = True
            End If
        Else
            FileData(Index).EnableInlineEdit False
            FileData(Index).EnableInlineEdit True
        End If
    End If
End Sub
Private Sub CheckChangesAndSave(Index As Integer, LineNo As Long)
    Dim FlnuValue As Long
    Dim LineStatus As Long
    Dim tmpData As String
    Dim tmpTag As String
    Dim tmpFields As String
    Dim NewData As String
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpAdid As String
    Dim tmpTime As String
    Dim tmpFlnu As String
    Dim tmpFltRel As String
    Dim CheckAdid As String
    Dim MsgText As String
    
    tmpData = FileData(Index).GetLineValues(LineNo)
    tmpTag = FileData(Index).GetLineTag(LineNo)
    If tmpTag = "" Then
        tmpTag = tmpData
        FileData(Index).SetLineTag LineNo, tmpData
    End If
    If tmpTag <> tmpData Then
        tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
        tmpLayout = GetRealItem(tmpMaskType, 1, ",")
        tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
        tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
        If tmpCall = "ROT" Then
            tmpFltRel = tmpCode
        Else
            tmpFltRel = tmpCall
        End If
        tmpAdid = Trim(FileData(Index).GetFieldValue(LineNo, "ADID"))
        Select Case tmpFltRel
            Case "ARR"
                tmpAdid = "A"
            Case "DEP"
                tmpAdid = "D"
            Case Else
        End Select
        CheckAdid = tmpAdid
        MsgText = ""
        If tmpAdid <> "" Then
            Select Case CheckAdid
                Case "A"
                    FlnuValue = Val(txtAftData(1).Text)
                    If FlnuValue <= 0 Then
                        MsgText = "Sorry, the Arrival Flight doesn't exist."
                    End If
                Case "D"
                    FlnuValue = Val(txtAftData(3).Text)
                    If FlnuValue <= 0 Then
                        MsgText = "Sorry, the Departure Flight doesn't exist."
                    End If
                Case Else
                    FlnuValue = Val(txtAftData(1).Text)
                    If FlnuValue <= 0 Then tmpAdid = "D"
                    FlnuValue = Val(txtAftData(3).Text)
                    If FlnuValue <= 0 Then tmpAdid = "A"
            End Select
        End If
        If MsgText = "" Then
            LineStatus = FileData(Index).GetLineStatusValue(LineNo)
            If (LineStatus = 99) Or (LineStatus = 2) Then
                tmpFields = "USEC,CDAT"
                NewData = LoginUserName
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
            End If
            If (LineStatus <= 1) Or (LineStatus = 3) Then
                tmpFields = "USEU,LSTU"
                NewData = LoginUserName
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
            End If
            
            tmpFields = tmpFields & ",FLNU"
            Select Case tmpAdid
                Case "A"
                    NewData = NewData & "," & txtAftData(1).Text
                Case "D"
                    NewData = NewData & "," & txtAftData(3).Text
                Case Else
                    If LineNo > 0 Then
                        tmpFlnu = FileData(Index).GetFieldValue(LineNo - 1, "FLNU")
                    Else
                        tmpFlnu = "-1"
                        If (chkSelFlight(0).Value = 1) And (chkSelFlight(1).Value = 0) Then
                            tmpFlnu = txtAftData(1).Text
                        End If
                        If (chkSelFlight(1).Value = 1) And (chkSelFlight(0).Value = 0) Then
                            tmpFlnu = txtAftData(3).Text
                        End If
                    End If
                    NewData = NewData & "," & tmpFlnu
            End Select
            
            Select Case tmpLayout
                Case "DLLG"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "A"
                Case "DLYS"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "B"
                Case "MVLG"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "A"
                Case "REQU"
                    tmpFields = tmpFields & ",AREA,READ"
                    tmpTime = Trim(FileData(Index).GetFieldValue(LineNo, "TIME"))
                    If (tmpTime <> "") Or (tmpAdid <> "") Then
                        NewData = NewData & ",A,X"
                    Else
                        NewData = NewData & ",A,"
                    End If
                    tmpFields = tmpFields & ",ADID,FLNO,STIM,ETIM"
                    Select Case tmpAdid
                        Case "A"
                            NewData = NewData & "," & "A"
                            NewData = NewData & "," & lblArr(0).Tag
                            NewData = NewData & "," & lblArr(2).Tag
                            NewData = NewData & "," & lblArr(4).Tag
                        Case "D"
                            NewData = NewData & "," & "D"
                            NewData = NewData & "," & lblDep(0).Tag
                            NewData = NewData & "," & lblDep(2).Tag
                            NewData = NewData & "," & lblDep(4).Tag
                        Case Else
                            If (tmpFlnu <> "-1") And (tmpAdid = "") Then
                                NewData = NewData & "," & tmpAdid & ",,,"
                            Else
                                NewData = NewData & "," & tmpAdid & ",????,,"
                            End If
                    End Select
                Case Else
            End Select
            
            FileData(Index).SetFieldValues LineNo, tmpFields, NewData
            
            If (TabEditUpdate = True) Or (LineStatus < 2) Then
                If LineStatus < 2 Then
                    FileData(Index).SetLineStatusValue LineNo, 1
                End If
            End If
            If TabEditInsert = True Then
                If LineStatus = 99 Then
                    FileData(Index).SetLineStatusValue LineNo, 2
                End If
            End If
            
            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
            If AutoSaveAodb Then HandleSaveAodb True
            tmpData = FileData(Index).GetLineValues(LineNo)
            FileData(Index).SetLineTag LineNo, tmpData
        Else
            If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "stop2", "", UserAnswer) = 1 Then DoNothing
        End If
    End If
End Sub
Private Sub SetAdidFlightRelation(Index As Integer, LineNo As Long, ColNo As Long)
    Dim FlnuValue As Long
    Dim tmpFields As String
    Dim NewData As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim tmpAdid As String
    Dim tmpTime As String
    Dim CheckAdid As String
    Dim tmpFlnu As String
    tmpFields = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
    If (tmpFields = "ADID") Or (tmpFields = "TIME") Then
        tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
        tmpLayout = GetRealItem(tmpMaskType, 1, ",")
        If tmpLayout = "REQU" Then
            tmpTime = Trim(FileData(Index).GetFieldValue(LineNo, "TIME"))
            tmpAdid = Trim(FileData(Index).GetFieldValue(LineNo, "ADID"))
            If (tmpTime <> "") And (tmpAdid = "") Then
                If chkSelFlight(0).Value = 1 Then
                    tmpAdid = "A"
                Else
                    tmpAdid = "D"
                End If
            End If
            CheckAdid = tmpAdid
            If tmpAdid <> "" Then
                Select Case CheckAdid
                    Case "A"
                        FlnuValue = Val(txtAftData(1).Text)
                        If FlnuValue <= 0 Then tmpAdid = "D"
                    Case "D"
                        FlnuValue = Val(txtAftData(3).Text)
                        If FlnuValue <= 0 Then tmpAdid = "A"
                    Case Else
                        FlnuValue = Val(txtAftData(1).Text)
                        If FlnuValue <= 0 Then tmpAdid = "D"
                        FlnuValue = Val(txtAftData(3).Text)
                        If FlnuValue <= 0 Then tmpAdid = "A"
                End Select
            End If
            tmpFields = "ADID,FLNO,STIM,ETIM,FLNU"
            NewData = tmpAdid
            Select Case tmpAdid
                Case "A"
                    NewData = NewData & "," & lblArr(0).Tag
                    NewData = NewData & "," & lblArr(2).Tag
                    NewData = NewData & "," & lblArr(4).Tag
                    NewData = NewData & "," & txtAftData(1).Text
                Case "D"
                    NewData = NewData & "," & lblDep(0).Tag
                    NewData = NewData & "," & lblDep(2).Tag
                    NewData = NewData & "," & lblDep(4).Tag
                    NewData = NewData & "," & txtAftData(3).Text
                Case Else
                    If LineNo > 0 Then
                        tmpFlnu = FileData(Index).GetFieldValue(LineNo - 1, "FLNU")
                    Else
                        tmpFlnu = "-1"
                    End If
                    If (tmpFlnu <> "-1") And (tmpAdid = "") Then
                        NewData = NewData & ",,,," & tmpFlnu
                    Else
                        If (chkSelFlight(0).Value = 1) And (chkSelFlight(1).Value = 0) Then
                            tmpFlnu = txtAftData(1).Text
                        End If
                        If (chkSelFlight(1).Value = 1) And (chkSelFlight(0).Value = 0) Then
                            tmpFlnu = txtAftData(3).Text
                        End If
                        If tmpFlnu <> "-1" Then
                            NewData = NewData & ",,,," & tmpFlnu
                        Else
                            NewData = NewData & ",????,,," & tmpFlnu
                        End If
                    End If
            End Select
            FileData(Index).SetFieldValues LineNo, tmpFields, NewData
            FileData(Index).Refresh
        End If
    End If
End Sub
Private Sub AutoSetReadStatus(Index As Integer, LineNo As Long)
    Dim tmpRead As String
    Dim LineStatus As Long
    Dim tmpCheck As String
    Dim tmpData As String
    If InStr(FileData(Index).LogicalFieldList, "READ") > 0 Then
        LineStatus = FileData(Index).GetLineStatusValue(LineNo)
        If LineStatus < 2 Then
            tmpCheck = Trim(FileData(Index).GetFieldValues(LineNo, "ADID,TIME"))
            If tmpCheck <> "," Then
                tmpRead = Trim(FileData(Index).GetFieldValue(LineNo, "READ"))
                If tmpRead = "" Then tmpRead = "X" Else tmpRead = ""
                tmpData = tmpRead
                tmpData = tmpData & "," & LoginUserName
                tmpData = tmpData & "," & GetTimeStamp(-UtcTimeDiff)
                FileData(Index).SetFieldValues LineNo, "READ,USEU,LSTU", tmpData
                FileData(Index).SetLineStatusValue LineNo, 1
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                If AutoSaveAodb Then HandleSaveAodb False
                tmpData = FileData(Index).GetLineValues(LineNo)
                FileData(Index).SetLineTag LineNo, tmpData
            End If
        End If
    End If
End Sub
Private Sub SetSortLineNoAsc(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim GroupLine As Long
    Dim SortLine As Long
    Dim OldSortVal As String
    Dim NewSortVal As String
    Dim NewMainVal As String
    Dim tmpTimeStr As String
    Dim NewValues As String
    Dim OldValues As String
    Dim tmpData As String
    Dim tmpFlnu As String
    Dim tmpAdid As String
    Dim LineStatus As Long
    If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
        GroupLine = 0
        SortLine = 0
        NewMainVal = ""
        MaxLine = FileData(Index).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineStatus = FileData(Index).GetLineStatusValue(CurLine)
            If LineStatus <> 99 Then
                tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
                If tmpFlnu = txtAftData(1).Text Then tmpAdid = "A"
                If tmpFlnu = txtAftData(3).Text Then tmpAdid = "D"
                tmpTimeStr = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
                If tmpTimeStr = "," Then tmpTimeStr = ""
                OldSortVal = Trim(FileData(Index).GetFieldValue(CurLine, "LINO"))
                If (tmpTimeStr <> "") Or (NewMainVal = "") Then
                    GroupLine = GroupLine + 1
                    NewMainVal = Right("000" & CStr(GroupLine), 3)
                    SortLine = 1
                End If
                NewSortVal = tmpAdid & NewMainVal & "." & Right("000" & CStr(SortLine), 3)
                If NewSortVal <> OldSortVal Then
                    FileData(Index).SetFieldValues CurLine, "LINO", NewSortVal
                    If LineStatus < 1 Then FileData(Index).SetLineStatusValue CurLine, 1
                    tmpData = FileData(Index).GetLineValues(CurLine)
                    FileData(Index).SetLineTag CurLine, tmpData
                End If
                SortLine = SortLine + 1
            End If
        Next
    End If
End Sub
Private Sub SetSortLineNoDesc(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim GroupLine As Long
    Dim SortLine As Long
    Dim OldSortVal As String
    Dim NewSortVal As String
    Dim NewMainVal As String
    Dim tmpTimeStr As String
    Dim NewValues As String
    Dim OldValues As String
    Dim tmpData As String
    Dim tmpFlnu As String
    Dim tmpAdid As String
    Dim LineStatus As Long
    If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
        GroupLine = 1000
        SortLine = 1000
        NewMainVal = ""
        MaxLine = FileData(Index).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineStatus = FileData(Index).GetLineStatusValue(CurLine)
            If LineStatus <> 99 Then
                tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
                If tmpFlnu = txtAftData(1).Text Then tmpAdid = "A"
                If tmpFlnu = txtAftData(3).Text Then tmpAdid = "D"
                tmpTimeStr = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
                If tmpTimeStr = "," Then tmpTimeStr = ""
                OldSortVal = Trim(FileData(Index).GetFieldValue(CurLine, "LINO"))
                If (tmpTimeStr <> "") Or (NewMainVal = "") Then
                    GroupLine = GroupLine - 1
                    NewMainVal = Right("000" & CStr(GroupLine), 3)
                    SortLine = 999
                End If
                NewSortVal = tmpAdid & NewMainVal & "." & Right("000" & CStr(SortLine), 3)
                If NewSortVal <> OldSortVal Then
                    FileData(Index).SetFieldValues CurLine, "LINO", NewSortVal
                    If LineStatus < 1 Then FileData(Index).SetLineStatusValue CurLine, 1
                    tmpData = FileData(Index).GetLineValues(CurLine)
                    FileData(Index).SetLineTag CurLine, tmpData
                End If
                SortLine = SortLine - 1
            End If
        Next
    End If
End Sub

Private Sub FileData_GotFocus(Index As Integer)
    Dim RetVal As Boolean
    Dim LineNo As Long
    Dim tmpColors As String
    Dim tmpTag As String
    Dim tmpName As String
    Dim i As Integer
    If Not SplitterMoves Then
        CurFocusIndex = Index
        chkTabIsVisible(Index).Appearance = 0
        lblTabName(Index).ForeColor = vbWhite
        lblTabShadow(Index).ForeColor = vbBlack
        tmpColors = BuildMainHeaderColor(ItemCount(FileData(Index).GetMainHeaderRanges, ","), CStr(LightestBlue))
        FileData(Index).SetMainHeaderValues FileData(Index).GetMainHeaderRanges, FileData(Index).GetMainHeaderValues, tmpColors
        MinMaxPanel(Index).BackColor = vbBlue
        fraTabCaption(Index).BackColor = MinMaxPanel(Index).BackColor
        lblTabCaption(Index).ForeColor = vbWhite
        lblTabCaptionShadow(Index).ForeColor = vbBlack
        fraMaxButton(Index).BackColor = MinMaxPanel(Index).BackColor
        fraSigns(Index).BackColor = fraMaxButton(Index).BackColor
        lblTabPos(Index).ForeColor = lblTabCaption(Index).ForeColor
        lblTabCnt(Index).ForeColor = lblTabCaption(Index).ForeColor
        If lblTabUtl(Index).Tag <> "CLOSED" Then
            tmpTag = lblTabUtl(Index).Tag
            If tmpTag = "LOOK" Then
                lblTabCtl(Index).BackColor = vbYellow
            Else
                lblTabCtl(Index).BackColor = MinMaxPanel(Index).BackColor
            End If
        Else
            lblTabCtl(Index).BackColor = MinMaxPanel(Index).BackColor
        End If
        FileData(Index).Refresh
        If TabEditLookUpFixed < 0 Then
            If TabEditLookUpIndex >= 0 Then
                lblTabUtl(TabEditLookUpIndex).BackColor = MinMaxPanel(TabEditLookUpIndex).BackColor
            End If
            RetVal = GetLookUpKeyValues(Index)
            If RetVal = True Then
                lblTabUtl(Index).BackColor = vbYellow
            ElseIf Index <> TabEditLookUpIndex Then
                lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
                If TabEditLookUpIndex >= 0 Then lblTabUtl(TabEditLookUpIndex).BackColor = vbYellow
            Else
                lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
            End If
            If (LastFocusIndex >= 0) And (LastFocusIndex <> Index) And (LastFocusIndex <> TabEditLookUpIndex) Then
                lblTabUtl(LastFocusIndex).BackColor = MinMaxPanel(LastFocusIndex).BackColor
            End If
        ElseIf TabEditLookUpFixed <> Index Then
            lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
        End If
        LastFocusIndex = Index
        If ReadMaskIsOpen Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then LineNo = 0
            FileData(LastFocusIndex).SetCurrentSelection LineNo
        End If
        tmpTag = FileData(Index).myTag
        For i = 0 To 1
            If chkSelFlight(i).Tag <> "" Then
                tmpName = "," & chkSelFlight(i).Tag & ","
                If InStr(tmpTag, tmpName) > 0 Then
                    FocusSyncInternal = True
                    If FileData(Index).GetLineCount > 0 Then
                        chkSelFlight(i).Value = 1
                    Else
                        ButtonPushedInternal = True
                        chkSelFlight(i).Value = 0
                        ButtonPushedInternal = False
                    End If
                    FocusSyncInternal = False
                End If
            End If
        Next
    End If
End Sub

Private Sub FileData_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    'Tab.ocx fires twice!
    Static lastKey As Integer
    If LineNo >= 0 Then
        If FipsIsConnected Then
            If Not ReadOnlyUser Then
                If FipsIsConnected Then
                    If Not TabEditIsActive Then
                        If Key = 13 Then PushWorkButton "TAB_EDIT_UPDATE", 1
                        If Key = 45 Then PushWorkButton "TAB_EDIT_INSERT", 1
                        If Key = 46 Then PushWorkButton "TAB_DELETE", 1
                    Else
                        If Key = 13 Then TabEditHitEnter = True
                    End If
                End If
            End If
        Else
            If Key = 13 Then
                If lastKey <> Key Then
                    MyBookMarks.SetBookMark FileData(Index), LineNo
                    FileData(Index).Refresh
                    lastKey = Key
                Else
                    lastKey = -1
                End If
            End If
        End If
    End If
End Sub

Private Sub FileData_LostFocus(Index As Integer)
    Dim tmpColors As String
    If (Not SplitterMoves) And (Not RefreshOnBc) Then
        chkTabIsVisible(Index).Appearance = 1
        lblTabName(Index).ForeColor = vbBlack
        lblTabShadow(Index).ForeColor = vbWhite
        tmpColors = ""
        FileData(Index).SetMainHeaderValues FileData(Index).GetMainHeaderRanges, FileData(Index).GetMainHeaderValues, tmpColors
        MinMaxPanel(Index).BackColor = LightGrey
        fraTabCaption(Index).BackColor = MinMaxPanel(Index).BackColor
        lblTabCaption(Index).ForeColor = vbBlack
        lblTabCaptionShadow(Index).ForeColor = vbWhite
        fraMaxButton(Index).BackColor = MinMaxPanel(Index).BackColor
        fraSigns(Index).BackColor = MinMaxPanel(Index).BackColor
        lblTabPos(Index).ForeColor = lblTabCaption(Index).ForeColor
        lblTabCnt(Index).ForeColor = lblTabCaption(Index).ForeColor
        lblTabCtl(Index).BackColor = MinMaxPanel(Index).BackColor
        If TabEditLookUpFixed < 0 Then
            lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
        ElseIf TabEditLookUpFixed <> Index Then
            lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
        End If
        FileData(Index).Refresh
    End If
End Sub

Private Function BuildMainHeaderColor(MaxColNo As Long, ColorValue As String) As String
    Dim tmpColors As String
    Dim i As Long
    For i = 1 To MaxColNo
        tmpColors = tmpColors & CStr(LightestBlue) & ","
    Next
    BuildMainHeaderColor = tmpColors
End Function

Private Sub FileData_OnVScroll(Index As Integer, ByVal LineNo As Long)
    Dim VisLines As Long
    Dim CurLine As Long
    Dim TopLine As Long
    Dim BotLine As Long
    If Not FipsIsConnected Then
        CurLine = FileData(Index).GetCurrentSelected
        If CurLine >= 0 Then
            VisLines = VisibleTabLines(FileData(Index))
            TopLine = LineNo
            BotLine = TopLine + VisLines - 1
            If CurLine < TopLine Then
                imgCurPos(Index).Picture = imgCurPosUp.Picture
                imgCurPos(Index).Visible = True
            ElseIf CurLine > BotLine Then
                imgCurPos(Index).Picture = imgCurPosDn.Picture
                imgCurPos(Index).Visible = True
            Else
                imgCurPos(Index).Visible = False
            End If
        End If
    End If
End Sub

Private Sub FileData_PackageReceived(Index As Integer, ByVal lpPackage As Long)
    Dim tmpTask As String
    Dim CurCount As Long
    Dim CurTotal As Long
    Dim CurValue As Long
    tmpTask = "Got Data Package"
    If ServerSignal(1).FillColor = vbCyan Then
        ServerSignal(1).FillStyle = 0
        ServerSignal(1).FillColor = vbGreen
        ServerSignal(1).Refresh
    Else
        ServerSignal(1).FillStyle = 0
        ServerSignal(1).FillColor = vbCyan
        ServerSignal(1).Refresh
    End If
    Me.Refresh
    CurCount = WorkProgress.GetProgressValue("VAL1")
    CurTotal = WorkProgress.GetProgressValue("VAL2")
    CurCount = CurCount + 1
    CurTotal = CurTotal + lpPackage
    CurValue = CurCount
    If CurValue > 10 Then CurValue = 1
    WorkProgress.SetTaskText tmpTask
    SetProgressValue Index, tmpTask, CurValue, 10, CurCount, CurTotal, False
End Sub

Private Sub FileData_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpText As String
    Dim tmpCapt As String
    Dim tmpRead As String
    Dim tmpUrno As String
    Dim PanelNo As Integer
    Dim IsRead As Integer
    Dim UseColor As Long
    If Selected = True Then
        lblTabPos(Index).Caption = CStr(LineNo + 1)
        imgCurPos(Index).Tag = FileData(Index).GetFieldValue(LineNo, "'S3'") & "," & CStr(LineNo - FileData(Index).GetVScrollPos)
        imgCurPos(Index).Visible = False
    End If
    If SyncCursorBusy = False Then
        If (Selected) And (LineNo >= 0) Then
            If ReadMaskIsOpen Then
                tmpUrno = FileData(Index).GetFieldValue(LineNo, "URNO")
                tmpText = FileData(Index).GetFieldValue(LineNo, "TEXT")
                tmpRead = Trim(FileData(Index).GetFieldValue(LineNo, "READ"))
                If tmpRead = "" Then
                    If InStr(FileData(Index).LogicalFieldList, "READ") > 0 Then IsRead = 1 Else IsRead = -1
                Else
                    IsRead = 0
                End If
                tmpCapt = HiddenData.GetConfigValues(FileData(Index).myName, "CAPT")
                tmpCapt = GetRealItem(tmpCapt, 0, "|")
                ReadMask.DisplayText FileData(Index), tmpText, tmpCapt, tmpUrno, IsRead
            End If
            If SectionUsingSyncCursor = True Then CheckSynchroTabs Index, True
        End If
        If DelayPanel.Visible Then
            If Val(DelayPanel.Tag) = Index Then
                tmpText = FileData(Index).GetFieldValue(LineNo, "INDX")
                If tmpText <> "" Then
                    PanelNo = Val(tmpText)
                    If (PanelNo >= 0) And (PanelNo <= DlyEditPanel.UBound) Then
                        If Selected Then
                            UseColor = vbYellow
                        Else
                            If chkDlyTick(PanelNo).Value = 1 Then
                                UseColor = vbWhite
                            Else
                                UseColor = NormalGray
                            End If
                        End If
                    End If
                    txtDlyInput(PanelNo).BackColor = UseColor
                End If
            End If
        End If
    End If
End Sub

Private Sub FileData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LastColno As Long
    Dim SortCol As Long
    Dim LineStatus As Long
    Dim tmpFldName As String
    Dim tmpFields As String
    Dim tmpSort As String
    If LineNo >= 0 Then
        If FipsIsConnected Then
            If (Not TabEditInline) And (Not AutoTabRead) And (Not ReadOnlyUser) Then
                LineStatus = FileData(Index).GetLineStatusValue(LineNo)
                If LineStatus <> 99 Then
                    PushWorkButton "TAB_EDIT_UPDATE", 1
                Else
                    PushWorkButton "TAB_EDIT_INSERT", 1
                End If
            ElseIf AutoTabRead Then
                AutoSetReadStatus Index, LineNo
            End If
        Else
            If ApplShortCode = "FDC" Then
                CallFipsFlight FileData(Index), LineNo
            Else
                MyBookMarks.SetBookMark FileData(Index), LineNo
            End If
        End If
    ElseIf LineNo = -1 Then
        Screen.MousePointer = 11
        If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then RemoveEmptyTabLines Index
        SortCol = ColNo
        LastColno = FileData(Index).CurrentSortColumn
        tmpSort = ""
        If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
            SortCol = -1
            tmpFldName = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
            If tmpFldName <> "LINO" Then
                SortCol = -1
                tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
                tmpFields = GetRealItem(tmpFields, 2, "|")
                If InStr(tmpFields, tmpFldName) > 0 Then
                    tmpSort = TranslateFieldItems("'S1'", FileData(Index).LogicalFieldList)
                    SortCol = Val(tmpSort)
                End If
            End If
        End If
        FileData(Index).ColSelectionRemoveAll
        If SortCol >= 0 Then
            If ColNo <> LastColno Then
                If tmpSort <> "" Then
                    SetSortLineNoAsc Index
                    PrepareSortKeys Index, tmpFldName
                End If
                FileData(Index).Sort CStr(SortCol), True, True
            Else
                If FileData(Index).SortOrderASC Then
                    If tmpSort <> "" Then
                        SetSortLineNoDesc Index
                        PrepareSortKeys Index, tmpFldName
                    End If
                    FileData(Index).Sort CStr(SortCol), False, True
                Else
                    If tmpSort <> "" Then
                        SetSortLineNoAsc Index
                        PrepareSortKeys Index, tmpFldName
                    End If
                    FileData(Index).Sort CStr(SortCol), True, True
                End If
            End If
            FileData(Index).ColSelectionAdd ColNo
        End If
        If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then InsertEmptyTabLine Index, -1
        FileData(Index).AutoSizeColumns
        FileData(Index).Refresh
        TabLookUp(Index).HeaderLengthString = FileData(Index).HeaderLengthString
        If InStr(FileData(Index).LogicalFieldList, "LINO") = 0 Then
            CheckAlternateColors Index
            imgCurPos_Click Index
            PushWorkButton CurAutoSortCfg, 0
        End If
        Screen.MousePointer = 0
    ElseIf LineNo < -1 Then
        If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
        FileData(Index).SetFocus
    End If
End Sub
Private Sub PrepareSortKeys(Index As Integer, SortField As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpFields As String
    Dim tmpFldVal As String
    Dim tmpOldVal As String
    Dim tmpLino As String
    Dim tmpData As String
    If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
        MaxLine = FileData(Index).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpFldVal = Trim(FileData(Index).GetFieldValue(CurLine, SortField))
            If tmpFldVal = "" Then tmpFldVal = tmpOldVal
            tmpLino = FileData(Index).GetFieldValue(CurLine, "LINO")
            tmpData = tmpFldVal & "." & tmpLino
            FileData(Index).SetFieldValues CurLine, "'S1'", tmpData
            tmpOldVal = tmpFldVal
        Next
    End If
End Sub
Private Sub ArrangeMaximizedTabs(Index As Integer, CheckSizeType As String)
    Dim tmpTag As String
    Dim SizeType As String
    Dim TabIndex As Integer
    SizeType = CheckSizeType
    Select Case SizeType
        Case "FULL"
            TabIndex = FullMaximizedTab
        Case "LEFT"
            TabIndex = LeftMaximizedTab
        Case "RIGHT"
            TabIndex = RightMaximizedTab
        Case Else
            TabIndex = -1
    End Select
    If TabIndex >= 0 Then
        tmpTag = FileData(TabIndex).Tag
        Select Case SizeType
            Case "FULL"
                FullMaximizedTab = -1
                If (Index <> LeftMaximizedTab) And (Index <> RightMaximizedTab) Then
                    TabPanel(TabIndex).Top = Val(GetRealItem(tmpTag, 0, ","))
                    TabPanel(TabIndex).Left = Val(GetRealItem(tmpTag, 1, ","))
                    TabPanel(TabIndex).Width = Val(GetRealItem(tmpTag, 2, ","))
                    TabPanel(TabIndex).Height = Val(GetRealItem(tmpTag, 3, ","))
                End If
                chkMaxSec(TabIndex).Enabled = True
                fraVtSplitter(0).Visible = True
            Case "LEFT"
                TabPanel(TabIndex).Top = Val(GetRealItem(tmpTag, 0, ","))
                TabPanel(TabIndex).Left = Val(GetRealItem(tmpTag, 1, ","))
                TabPanel(TabIndex).Width = Val(GetRealItem(tmpTag, 2, ","))
                TabPanel(TabIndex).Height = Val(GetRealItem(tmpTag, 3, ","))
                LeftMaximizedTab = -1
            Case "RIGHT"
                TabPanel(TabIndex).Top = Val(GetRealItem(tmpTag, 0, ","))
                TabPanel(TabIndex).Left = Val(GetRealItem(tmpTag, 1, ","))
                TabPanel(TabIndex).Width = Val(GetRealItem(tmpTag, 2, ","))
                TabPanel(TabIndex).Height = Val(GetRealItem(tmpTag, 3, ","))
                RightMaximizedTab = -1
            Case Else
        End Select
        TabCover(TabIndex).Top = TabPanel(TabIndex).Top
        TabCover(TabIndex).Left = TabPanel(TabIndex).Left
        TabCover(TabIndex).Width = TabPanel(TabIndex).Width
        TabCover(TabIndex).Height = TabPanel(TabIndex).Height
        ArrangeLayout 0
        Form_Resize
    ElseIf Index >= 0 Then
        tmpTag = ""
        tmpTag = tmpTag & CStr(TabPanel(Index).Top) & ","
        tmpTag = tmpTag & CStr(TabPanel(Index).Left) & ","
        tmpTag = tmpTag & CStr(TabPanel(Index).Width) & ","
        tmpTag = tmpTag & CStr(TabPanel(Index).Height)
        TabPanel(Index).Top = TabPanel(0).Top
        Select Case SizeType
            Case "FULL"
                FullMaximizedTab = Index
                TabPanel(Index).Left = TabPanel(0).Left
                chkMaxSec(Index).Enabled = False
                If (Index <> LeftMaximizedTab) And (Index <> RightMaximizedTab) Then FileData(Index).Tag = tmpTag
                fraVtSplitter(0).Visible = False
            Case "LEFT"
                FileData(Index).Tag = tmpTag
                LeftMaximizedTab = Index
            Case "RIGHT"
                FileData(Index).Tag = tmpTag
                RightMaximizedTab = Index
            Case Else
        End Select
        TabCover(Index).Top = TabPanel(Index).Top
        TabCover(Index).Left = TabPanel(Index).Left
        TabCover(Index).Width = TabPanel(Index).Width
        TabCover(Index).Height = TabPanel(Index).Height
        ArrangeLayout 0
        TabPanel(Index).ZOrder
        TabCover(Index).ZOrder
    End If
End Sub

Private Sub AdjustSplitters()
    Dim i As Integer
    For i = 0 To fraVtSplitter.UBound
        fraVtSplitter(i).ZOrder
    Next
    For i = 0 To fraHzSplitter.UBound
        fraHzSplitter(i).ZOrder
    Next
    For i = 0 To fraCrSplitter.UBound
        fraCrSplitter(i).ZOrder
    Next
End Sub

Private Sub FileData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        If FipsIsConnected Then
            PushWorkButton "TAB_READ", 1
            ReadMask.SetReadOnly True
        End If
    End If
End Sub

Private Sub FontSlider_Scroll()
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            FileData(i).FontSize = FontSlider.Value + 6
            FileData(i).HeaderFontSize = FontSlider.Value + 6
            FileData(i).LineHeight = FontSlider.Value + 8
            FileData(i).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, CurrentTabFont
            FileData(i).CreateCellObj "ColMarker", LightestYellow, vbBlack, (FontSlider.Value + 6), False, False, True, 0, CurrentTabFont
            FileData(i).CreateCellObj "Marker", vbBlue, vbWhite, (FontSlider.Value + 6), False, False, True, 0, CurrentTabFont
            FileData(i).AutoSizeColumns
            FileData(i).Refresh
            TabLookUp(i).HeaderLengthString = FileData(i).HeaderLengthString
            FileData_OnVScroll i, FileData(i).GetVScrollPos
        End If
    Next
    FontSlider.ToolTipText = "Font Size Slider (" & CurrentTabFont & " " & CStr(FontSlider.Value) & ")"
End Sub

Private Sub Form_Activate()
    Static AlreadyShown As Boolean
    Dim CurX As Long
    Dim CurY As Long
    Dim tmpIdx As Integer
    Dim tmpCfgList As String
    If Not AlreadyShown Then
        AlreadyShown = True
        ApplicationIsReadyForUse = False
        Me.WindowState = vbNormal
        Me.Height = Screen.Height + 180
        Me.Width = Screen.Width + 180
        MaxFormHeight = Me.Height
        MaxFormWidth = Me.Width
        If MainLifeStyle Then
            lblArrRow.ForeColor = vbYellow
            lblDepRow.ForeColor = vbGreen
            DrawBackGround RightPanel, WorkAreaColor, True, True
            DrawBackGround ServerPanel, MainAreaColor, True, True
            DrawBackGround TopPanel, WorkAreaColor, True, True
        Else
            lblArrRow.ForeColor = vbButtonText
            lblDepRow.ForeColor = vbButtonText
        End If
        Me.Width = SetMyWidth
        Me.Height = SetMyHeight
        Me.Left = SetMyLeft
        Me.Top = SetMyTop
        Me.Show
        CheckFipsConnection
        If FipsIsConnected Then
            chkSelDeco(0).Visible = False
            chkSelDeco(1).Visible = False
            fraYButtonPanel(0).Height = chkSelDeco(0).Top
        Else
            chkSelDeco(0).Visible = True
            chkSelDeco(1).Visible = True
            fraYButtonPanel(0).Height = chkAppl(4).Top + chkAppl(4).Height + 15
            chkUtc(0).Value = 1
        End If
        If ApplShortCode = "FDC" Then
            chkSelDeco(0).Visible = False
            chkSelDeco(1).Visible = False
            'fraYButtonPanel(0).Height = chkSelDeco(0).Top
            fraYButtonPanel(0).Height = chkAppl(2).Top
            chkUtc(1).Value = 1
        End If
        tmpCfgList = GetIniEntry(myIniFullName, "MAIN", "", "MAIN_MAX_BUTTON", "")
        If tmpCfgList <> "" Then
            tmpIdx = Val(tmpCfgList)
            If (tmpIdx > 0) And (tmpIdx <= chkAppl.UBound) Then
                fraYButtonPanel(0).Height = chkAppl(tmpIdx).Top
            End If
        End If
        chkTabIsVisible(0).Top = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height + 120
        ServerSignal(2).FillStyle = 0
        ServerSignal(2).FillColor = vbYellow
        Me.Refresh
        If Not CedaIsConnected Then
            ServerIsAvailable = False
            UfisServer.ConnectToCeda
        End If
        If CedaIsConnected Then
            ServerIsAvailable = True
            'UfisServer.DisconnectFromCeda
        End If
        AdjustServerSignals False, 0
        tmpCfgList = GetIniEntry(myIniFullName, "MAIN", "", "INIT_BASICS", "NO")
        If tmpCfgList = "YES" Then
            WorkArea.AutoRedraw = False
            CurX = 3000
            CurY = 3000
            PrintBackGroundText WorkArea, 18, CurX, CurY, "Loading Basic Data"
            CurX = CurX + 600
            CurY = CurY + 600
            PrintBackGroundText WorkArea, 14, CurX, CurY, "Airline Codes  ..."
            HiddenData.InitBasicData 0, "ALTTAB", "ALC3,ALC2,ALFN", ""
            CurY = CurY + 450
            PrintBackGroundText WorkArea, 14, CurX, CurY, "Aircraft Types  ..."
            HiddenData.InitBasicData 1, "ACTTAB", "ACT3,ACT5,ACFN", ""
            CurY = CurY + 450
            PrintBackGroundText WorkArea, 14, CurX, CurY, "Airport Codes  ..."
            HiddenData.InitBasicData 2, "APTTAB", "APC3,APC4,APFN", ""
            CurY = CurY + 450
            PrintBackGroundText WorkArea, 14, CurX, CurY, "System Fields  ..."
            HiddenData.InitBasicData 3, "SYSTAB", "TANA,FINA,TYPE,FELE,ADDI,URNO", "TANA='AFT'"
            HiddenData.BasicDataTab(3).Sort "1", True, True
            HiddenData.BasicDataTab(3).Sort "0", True, True
            CurY = CurY + 450
            PrintBackGroundText WorkArea, 18, CurX, CurY, "Ready."
            WorkArea.AutoRedraw = True
        End If
        UfisServer.ConnectToBcProxy
        
        If Not SlideActive Then InitUfisIntro Me, WorkArea, 0, 0
        SetUfisIntroFrames True
        
        If MainTimer(1).Tag <> "" Then
            MainTimer(1).Interval = 5
            MainTimer(1).Enabled = True
        Else
            DrawBackGround WorkArea, WorkAreaColor, True, True
            ApplicationIsReadyForUse = True
            Form_Resize
            'If Not SlideActive Then InitUfisIntro Me, WorkArea, 0, 0
            'SetUfisIntroFrames True
        End If
        AdjustServerSignals False, 2
        MainTimer_Timer 4
        Me.SetFocus
    End If
End Sub
Private Sub CheckFipsConnection()
    Dim tmpCmd As String
    Dim tmpKey As String
    Dim tmpParam As String
    Dim tmpCfg As String
    Dim tmpAreaLine As String
    Dim tmpAreaName As String
    Dim i As Integer
    'MainTimer(1).Tag = ""
    FipsIsConnected = False
    tmpCmd = Command
    If tmpCmd <> "" Then
        tmpParam = UCase(GetRealItem(tmpCmd, 0, ","))
        tmpKey = "PARAM_" & tmpParam
        tmpAreaLine = GetIniEntry(myIniFullName, "FIPS_CONNECTION", "", tmpKey, "")
        tmpAreaName = GetRealItem(tmpAreaLine, 0, ",")
        If tmpAreaName <> "" Then
            For i = 0 To chkImpTyp.UBound
                If chkImpTyp(i).Tag = tmpAreaName Then
                    MainTimer(1).Tag = "AUTO," & CStr(i)
                    FlightPanel.Tag = tmpAreaLine & "|" & tmpCmd
                    tmpParam = GetRealItem(tmpCmd, 1, "|")
                    LoginUserName = GetRealItem(tmpParam, 0, ",")
                    tmpCfg = GetRealItem(tmpParam, 1, ",")
                    If tmpCfg = "0" Then ReadOnlyUser = True Else ReadOnlyUser = False
                    tmpCfg = GetRealItem(tmpParam, 2, ",")
                    If tmpCfg <> "" Then
                        Select Case tmpCfg
                            Case "U"
                                FipsShowsUtc = True
                            Case "L"
                                FipsShowsUtc = False
                            Case Else
                                tmpCfg = ""
                        End Select
                    End If
                    If tmpCfg = "" Then
                        tmpCfg = Trim(GetIniEntry(CedaIniFile, "FIPS", "FPMS", "DAILYLOCAL", "FALSE"))
                        Select Case UCase(tmpCfg)
                            Case "TRUE"
                                FipsShowsUtc = False
                            Case "FALSE"
                                FipsShowsUtc = True
                            Case Else
                                FipsShowsUtc = True
                        End Select
                    End If
                    tmpParam = GetRealItem(tmpCmd, 2, "|")
                    SetMyLeft = Val(GetRealItem(tmpParam, 0, ",")) * 15
                    SetMyTop = Val(GetRealItem(tmpParam, 1, ",")) * 15
                    OnTop.Value = 1
                    FipsIsConnected = True
                    Exit For
                End If
            Next
        End If
    End If
    If Not FipsIsConnected Then
        'SetMyLeft = 0
        'SetMyTop = 0
        'For SnapshotTool only
        OnTop.Value = 1
        
        'MainTimer(1).Tag = "AUTO,0"
    End If
End Sub
Private Sub Form_Load()
    SetMyWidth = 1024 * 15
    SetMyHeight = 768 * 15
    SetMyTop = (Screen.Height - SetMyHeight) / 2
    SetMyLeft = (Screen.Width - SetMyWidth) / 2
    Me.Top = Screen.Height + 300
    Me.Left = 0
    LastFocusIndex = -1
    SetButtonFaceStyle Me
    RecoverFileName = UFIS_TMP & "\" & App.EXEName & "_Recover.txt"
    InitMyForm
    InitApplication
    ShowScenarioTools = True
    ShowScenarioTools = False
End Sub

Private Sub InitApplication()
    Dim tmpData As String
    Me.Icon = MyOwnForm.Icon
    GetApplPosSize "MAIN", False
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_LIFE_STYLE", "YES"))
    If Left(tmpData, 1) = "Y" Then MainLifeStyle = True Else MainLifeStyle = False
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "GRID_LIFE_STYLE", "YES"))
    If Left(tmpData, 1) = "Y" Then GridLifeStyle = True Else GridLifeStyle = False
    'MyPrintLogo = GetIniEntry(myIniFullName, "MAIN", "", "PRINT_LOGO", "c:\ufis\system\CustLogo.bmp")
    MyPrintLogo = GetIniEntry(myIniFullName, "MAIN", "", "PRINT_LOGO", UFIS_SYSTEM & "\CustLogo.bmp")
    FormatDateTime = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_TIME", "DDMMMYY'/'hh':'mm"))
    FormatDatePart = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_PART", "DDMMMYY"))
    FormatTimePart = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_TIME_PART", "hh':'mm"))
    FormatDateOnly = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_ONLY", "DDMMMYY"))
    FormatTimeOnly = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_TIME_ONLY", "hh':'mm"))
    TopPanel.Tag = "0,-1"
    InitValidSections ""
    FullMaximizedTab = -1
    LeftMaximizedTab = -1
    RightMaximizedTab = -1
End Sub
Private Sub GetApplPosSize(CfgSection As String, SetIt As Boolean)
    SetMyTop = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_TOP", CStr(SetMyTop / 15))) * 15
    SetMyLeft = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_LEFT", CStr(SetMyLeft / 15))) * 15
    SetMyHeight = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_HEIGHT", CStr(SetMyHeight / 15))) * 15
    SetMyWidth = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_WIDTH", CStr(SetMyWidth / 15))) * 15
    If SetIt Then
        If Me.WindowState = vbNormal Then
            Me.Width = SetMyWidth
            Me.Height = SetMyHeight
            Me.Left = SetMyLeft
            Me.Top = SetMyTop
            CheckMonitorArea Me
        End If
    End If
End Sub
Public Sub InitValidSections(CfgSections As String)
    Dim DefaultButtons As String
    Dim ValidSections As String
    Dim tmpParam As String
    Dim tmpKeyWord As String
    Dim LastButton As Integer
    Dim ItmNbr As Integer
    Dim tmpData As String
    Dim ButtonIndex As Integer
    Dim BreakOut As Boolean
    Dim LeftPos As Long
    Dim TopPos As Long
    Dim i As Integer
    Me.Caption = GetIniEntry(myIniFullName, "MAIN", "", "APPL_TITLE", Me.Caption)
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "APPL_EXIT", "Exit,EXIT"))
    chkAppl(0).Caption = GetRealItem(tmpData, 0, ",")
    chkAppl(0).Tag = GetRealItem(tmpData, 1, ",")
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "SERVER_SIGNALS", "YES"))
    If Left(tmpData, 1) = "Y" Then
        ServerPanel.Visible = True
        ServerPanel.Tag = "SHOW"
    Else
        ServerPanel.Visible = False
        ServerPanel.Tag = "HIDE"
    End If
    
    tmpKeyWord = "MAIN_BUTTONS"
    MaxMainButtonRow = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_BUTTON_ROWS", "1")) - 1
    DefaultButtons = Trim(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_BUTTONS", ""))
    ArrangeWorkButtons 0, "MAIN", DefaultButtons, MaxMainButtonRow
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ShowMaxButtons = Abs(Val(GetIniEntry(myIniFullName, "MAIN", "", "MAX_BUTTONS", "12"))) - 1
    If ShowMaxButtons < 1 Then ShowMaxButtons = 1
    If ShowMaxButtons > 12 Then ShowMaxButtons = 12
    If MaxMainButtonRow > 0 Then fraTopButtonPanel(0).Tag = "-1,-1"
    LastButton = Val(GetRealItem(fraTopButtonPanel(0).Tag, 1, ","))
    If LastButton >= 0 Then
        LeftPos = chkWork(LastButton).Left + chkWork(LastButton).Width + 15
    Else
        LeftPos = 0
    End If
    TopPos = 0
    For ButtonIndex = 0 To chkImpTyp.UBound
        chkImpTyp(ButtonIndex).Visible = False
        chkImpTyp(ButtonIndex).Value = 0
    Next
    ValidSections = GetIniEntry(myIniFullName, "MAIN", "", "APPL_TYPES", CfgSections)
    ButtonIndex = -1
    ItmNbr = 0
    BreakOut = False
    Do
        ItmNbr = ItmNbr + 1
        tmpData = Trim(GetItem(ValidSections, ItmNbr, ","))
        If tmpData <> "" Then
            ButtonIndex = ButtonIndex + 1
            If ButtonIndex > chkImpTyp.UBound Then
                Load chkImpTyp(ButtonIndex)
                Load picBall(ButtonIndex)
            End If
            Set chkImpTyp(ButtonIndex).Container = fraTopButtonPanel(0)
            Set picBall(ButtonIndex).Container = fraTopButtonPanel(0)
            If ButtonIndex <= chkImpTyp.UBound Then
                chkImpTyp(ButtonIndex).Top = TopPos
                chkImpTyp(ButtonIndex).Left = LeftPos
                picBall(ButtonIndex).Top = TopPos + 15
                picBall(ButtonIndex).Left = LeftPos + 15
                picBall(ButtonIndex).BackColor = vbButtonFace
                picBall(ButtonIndex).Visible = False
                chkImpTyp(ButtonIndex).Tag = tmpData
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_VISIBLE", "YES"))
                If (Left(tmpData, 1) = "Y") And (ButtonIndex <= ShowMaxButtons) Then
                    chkImpTyp(ButtonIndex).Visible = True
                    LeftPos = LeftPos + chkImpTyp(ButtonIndex).Width + 15
                Else
                    chkImpTyp(ButtonIndex).Visible = False
                End If
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_CAPTION", ""))
                If tmpData <> "" Then
                    chkImpTyp(ButtonIndex).Caption = tmpData
                    chkImpTyp(ButtonIndex).ToolTipText = LTrim(Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "TOOLTIP_TEXT", "")) & " ")
                Else
                    chkImpTyp(ButtonIndex).Caption = "???" & Str(ItmNbr)
                    chkImpTyp(ButtonIndex).ToolTipText = "Missing configuration key: 'BUTTON_CAPTION' "
                    chkImpTyp(ButtonIndex).Tag = "NOTHING"
                End If
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_TOGGLED", "NO"))
                picBall(ButtonIndex).Tag = tmpData & "|"
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_ACTIVATE", "NO"))
                If (Left(tmpData, 1) = "Y") Then
                    MainTimer(1).Tag = "PUSH," & CStr(ButtonIndex)
                End If
            End If
        Else
            BreakOut = True
        End If
    Loop While Not BreakOut
    If LeftPos > 150 Then fraTopButtonPanel(0).Width = LeftPos - 15
    TopPos = 30
    For i = 0 To MaxMainButtonRow
        fraTopButtonPanel(i).Top = TopPos
        fraTopButtonPanel(i).Height = chkImpTyp(0).Height
        fraTopButtonPanel(i).Visible = True
        TopPos = TopPos + fraTopButtonPanel(i).Height + 15
    Next
    TopPanel.Height = TopPos + 75
    WorkArea.Top = TopPanel.Top + TopPanel.Height
    ResetMain
    ImpScroll.Visible = False
    If ButtonIndex < 0 Then
        OnTop.Value = 0
        'MsgBox "No valid configuration found."
    Else
        If ButtonIndex > ShowMaxButtons Then
            'ImpScroll.Tag = Trim(Str(ShowMaxButtons))
            'ImpScroll.Max = ButtonIndex - ShowMaxButtons
            'ImpScroll.Left = LeftPos - Screen.TwipsPerPixelX
            'ImpScroll.Visible = True
            'ImpScroll.ZOrder
        End If
    End If
End Sub

Private Sub InitMyForm()
    Dim i As Integer
    Dim tmpData As String
    TopPanel.Top = 0
    TopPanel.Left = 0
    WorkArea.Top = TopPanel.Height
    WorkArea.Left = 0
    ServerPanel.Left = -30
    ServerPanel.Height = BottomPanel.Height + 15
    ServerPanel.Width = RightPanel.Width + 60
    TopPanel.BackColor = MyOwnButtonFace
    WorkArea.BackColor = MyOwnButtonFace
    BottomPanel.BackColor = MyOwnButtonFace
    RightPanel.BackColor = MyOwnButtonFace
    DelayPanel.BackColor = MyOwnButtonFace
    SystabPanel.BackColor = MyOwnButtonFace
    ServerPanel.BackColor = MyOwnButtonFace
    fraMaxButton(0).BackColor = LightGray
    fraSigns(0).BackColor = LightGray
    'Define the height of the tool panel per window
    fraMaxButton(0).Height = fraSigns(0).Height
    MinMaxPanel(0).Height = fraMaxButton(0).Height
    MinMaxPanel(0).BackColor = LightGray
    MinMaxPanel(0).Top = 15
    MinMaxPanel(0).Left = 15
    fraMaxButton(0).Top = 0
    fraTabCaption(0).Top = 0
    fraTabCaption(0).Left = 0
    fraTabCaption(0).Height = MinMaxPanel(0).Height
    fraTabCaption(0).BackColor = LightGray
    lblTabCaption(0).Left = 60
    lblTabCaptionShadow(0).Top = lblTabCaption(0).Top + 15
    lblTabCaptionShadow(0).Left = lblTabCaption(0).Left + 15
    TabPanel(0).Left = 0
    TabCover(0).Left = 0
    TabCover(0).BackColor = RGB(235, 235, 255)
    'lblTabCtl(0).BorderStyle = 0
    lblTabUtl(0).BorderStyle = 0
    lblTabCtl(0).BackColor = LightGray
    lblTabUtl(0).BackColor = LightGray
    imgCurPos(0).Top = lblTabCtl(0).Top + 15
    imgCurPos(0).Left = lblTabCtl(0).Left + 15
    imgAnyPic1(0).Top = lblTabCtl(0).Top + 15
    imgAnyPic1(0).Left = lblTabCtl(0).Left + 15
    imgAnyPic2(0).Top = lblTabUtl(0).Top + 15
    imgAnyPic2(0).Left = lblTabUtl(0).Left + 15
    fraSigns(0).Top = 0
    fraSigns(0).Width = 135
    fraSigns(0).Left = lblTabPos(0).Left - fraSigns(0).Width
    
    FileData(0).Top = MinMaxPanel(0).Height + 30
    FileData(0).Left = 15
    TabLookUp(0).Top = MinMaxPanel(0).Height + 30
    TabLookUp(0).Left = 15
    
    fraVtSplitter(0).Top = 0
    fraVtSplitter(0).Left = 9000
    fraVtSplitter(0).Tag = "-1,-1"
    fraHzSplitter(0).Tag = "-1,-1"
    fraCrSplitter(0).Tag = "-1,-1"
    fraTopButtonPanel(0).Top = 30
    TopPanel.ZOrder
    BottomPanel.ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    MaxLeftWidth = 2500
    MaxRightWidth = 4000
    MinLeftWidth = 900
    MinRightWidth = 900
    MinFrameSize = MinLeftWidth + MinRightWidth
    ImpScroll.ZOrder
    lblSrvName(0).Caption = UfisServer.HostName.Text
    Load lblSrvName(1)
    lblSrvName(1).ForeColor = vbWhite
    lblSrvName(0).ForeColor = vbBlack
    lblSrvName(1).Visible = True
    lblSrvName(0).Left = 0
    lblSrvName(1).Left = lblSrvName(0).Left + 15
    lblSrvName(1).Top = lblSrvName(0).Top + 15
    lblSrvName(0).Width = RightPanel.Width - 90
    lblSrvName(1).Width = RightPanel.Width - 90
    lblSrvName(0).ZOrder
    ResetMain
    tmpData = GetIniEntry(myIniFullName, "MAIN", "", "FONT_SIZE", "8,11,32")
    FontSlider.Min = Val(GetItem(tmpData, 1, ","))
    FontSlider.Max = Val(GetItem(tmpData, 3, ","))
    FontSlider.Value = Val(GetItem(tmpData, 2, ","))
'    Me.Top = -Screen.Height - 300
'    Me.Left = 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        chkAppl(0).Value = 1
    End If
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim MaxTop As Long
    Dim i As Integer
    'RightPanel.Visible = False
    RightPanel.ZOrder
    RightPanel.Left = Me.ScaleWidth - RightPanel.Width
    If SidePanel.Visible Then SidePanel.Left = RightPanel.Left - SidePanel.Width
    NewHeight = Me.ScaleHeight - StatusBar.Height
    If NewHeight > (fraYButtonPanel(0).Top + chkAppl(1).Top + 30) Then RightPanel.Height = NewHeight
    NewTop = TopPanel.Height
    LeftPanel.Top = NewTop
    If ToolPanel.Visible Then
        ToolPanel.Top = NewTop
        NewTop = NewTop + ToolPanel.Height
    End If
    If TaskPanel.Visible Then
        TaskPanel.Top = NewTop
        NewTop = NewTop + TaskPanel.Height
    End If
    WorkArea.Top = NewTop
    NewLeft = 0
    If LeftPanel.Visible Then
        LeftPanel.Left = 0
        NewLeft = NewLeft + LeftPanel.Width
    End If
    WorkArea.Left = NewLeft
    ToolPanel.Left = WorkArea.Left
    TaskPanel.Left = WorkArea.Left
    StatusPanel.Left = WorkArea.Left

    NewHeight = Me.ScaleHeight - TopPanel.Height - StatusBar.Height
    If BottomPanel.Visible Then NewHeight = NewHeight - BottomPanel.Height
    If NewHeight > 1500 Then LeftPanel.Height = NewHeight
    If ToolPanel.Visible Then NewHeight = NewHeight - ToolPanel.Height
    If TaskPanel.Visible Then NewHeight = NewHeight - TaskPanel.Height
    If StatusPanel.Visible Then NewHeight = NewHeight - StatusPanel.Height
    If NewHeight > 1500 Then
        WorkArea.Height = NewHeight
        DelayPanel.Height = NewHeight
        SystabPanel.Height = NewHeight
        If NewHeight > (SysTab.Top + 300) Then
            SysTab.Height = NewHeight - SysTab.Top - 75
            RotationData.Top = SysTab.Top
            RotationData.Height = SysTab.Height
            ConnexData.Top = SysTab.Top
            ConnexData.Height = SysTab.Height
        End If
    End If
    SidePanel.Top = LeftPanel.Top
    SidePanel.Height = LeftPanel.Height
    NewWidth = Me.ScaleWidth - RightPanel.Width
    If NewWidth >= MinFrameSize Then
        TopPanel.Width = NewWidth
        If FlightPanel.Visible Then
            FlightPanel.Width = NewWidth - 45
            If NewWidth > (lblArr(9).Left + 300) Then
                lblArr(9).Width = NewWidth - lblArr(9).Left - 210
                lblDep(9).Width = NewWidth - lblDep(9).Left - 210
            End If
        End If
        BottomPanel.Width = NewWidth
    End If
    If LeftPanel.Visible Then NewWidth = NewWidth - LeftPanel.Width
    If SidePanel.Visible Then NewWidth = NewWidth - SidePanel.Width
    If NewWidth >= MinFrameSize Then
        If DelayPanel.Visible Then
            NewWidth = NewWidth - DelayPanel.Width
            DelayPanel.Left = NewWidth
        End If
        If SystabPanel.Visible Then
            NewWidth = NewWidth - SystabPanel.Width
            SystabPanel.Left = NewWidth
        End If
        If NewWidth >= MinFrameSize Then
            WorkArea.Width = NewWidth
            ToolPanel.Width = NewWidth
            TaskPanel.Width = NewWidth
            StatusPanel.Width = NewWidth
        End If
    End If
    If ServerPanel.Tag = "SHOW" Then
        MaxTop = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height + 90
        For i = chkTabIsVisible.UBound To 0 Step -1
            If chkTabIsVisible(i).Visible Then
                MaxTop = chkTabIsVisible(i).Top + chkTabIsVisible(i).Height + 90
                Exit For
            End If
        Next
        NewTop = RightPanel.ScaleHeight - ServerPanel.Height + 45
        ServerPanel.Top = NewTop
        If NewTop > MaxTop Then ServerPanel.Visible = True Else ServerPanel.Visible = False
    End If
    RightPanel.Visible = True
    RightPanel.Refresh
    NewTop = Me.ScaleHeight - StatusBar.Height
    If BottomPanel.Visible Then NewTop = NewTop - BottomPanel.Height
    If StatusPanel.Visible Then
        NewTop = NewTop - StatusPanel.Height
        StatusPanel.Top = NewTop
        NewTop = NewTop + StatusPanel.Height
    End If
    If BottomPanel.Visible Then BottomPanel.Top = NewTop
    ArrangeLayout -1
    If MainLifeStyle Then
        If (CountMainTabs + CountSubTabs) = 0 Then DrawBackGround WorkArea, WorkAreaColor, True, True
        DrawBackGround DelayPanel, WorkAreaColor, False, True
        DrawBackGround SystabPanel, WorkAreaColor, True, True
        'DrawBackGround RightPanel, WorkAreaColor, True, True
        MaxFormHeight = Me.Height
        'DrawBackGround TopPanel, WorkAreaColor, True, True
        'DrawBackGround BottomPanel, WorkAreaColor, True, True
        'DrawBackGround FlightPanel, WorkAreaColor, True, True
        'FlightPanel.Refresh
        MaxFormWidth = Me.Width
    End If
    fraFolderPanel(0).Top = WorkArea.Top
    fraFolderPanel(0).Left = WorkArea.Left
    fraFolderPanel(0).Height = WorkArea.Height
    fraFolderPanel(0).Width = WorkArea.Width
    For i = 0 To UfisIntroFrame.UBound
        UfisIntroFrame(i).Top = (WorkArea.ScaleHeight - UfisIntroFrame(i).Height) / 2
        UfisIntroFrame(i).Left = (WorkArea.ScaleWidth - UfisIntroFrame(i).Width) / 2
    Next
    CheckUfisIntroResize
End Sub
Private Sub ArrangeLayout(Index As Integer)
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim FraLeft As Long
    Dim NewTop As Long
    Dim LeftTop As Long
    Dim RightTop As Long
    Dim LeftHeight As Long
    Dim LeftRest As Long
    Dim RightHeight As Long
    Dim RightRest As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    Dim ReturnColor As Long
    Dim tmpTag As String
    Dim LCnt As Integer
    Dim RCnt As Integer
    Dim i As Integer
    Dim j As Integer
    Dim CheckAll As Boolean
    If ApplicationIsReadyForUse Then
        AdjustTabPanels -1, False
        LeftRest = -1
        RightRest = -1
        LCnt = 0
        RCnt = 0
        If Index <= 0 Then
            NewHeight = WorkArea.ScaleHeight - 15
            NewWidth = WorkArea.ScaleWidth
            If CountMainTabs > 0 Then
                If CountMainTabs = 2 Then
                    LeftHeight = ((NewHeight - fraHzSplitter(0).Height) * SplitPosV / 100)
                    LeftRest = NewHeight - LeftHeight - fraHzSplitter(0).Height
                Else
                    LeftHeight = (NewHeight - (fraHzSplitter(0).Height * CLng(CountMainTabs - 1))) / CLng(CountMainTabs)
                    LeftRest = NewHeight - ((LeftHeight + fraHzSplitter(0).Height)) * CLng(CountMainTabs - 1)
                End If
            End If
            If CountSubTabs > 0 Then
                RightHeight = (NewHeight - (fraHzSplitter(0).Height * CLng(CountSubTabs - 1))) / CLng(CountSubTabs)
                RightRest = NewHeight - ((RightHeight + fraHzSplitter(0).Height)) * CLng(CountSubTabs - 1)
            End If
            If (CountMainTabs > 0) And (CountSubTabs > 0) Then
                LeftWidth = fraVtSplitter(0).Left
                If Index < 0 Then LeftWidth = NewWidth * SplitPosH / 100
                FraLeft = LeftWidth
                NewLeft = FraLeft + fraVtSplitter(0).Width
                RightWidth = NewWidth - NewLeft
                fraVtSplitter(0).Left = FraLeft
                fraVtSplitter(0).Height = NewHeight
                SplitPosH = LeftWidth * 100 / NewWidth
            ElseIf CountMainTabs > 0 Then
                NewLeft = NewWidth
                FraLeft = NewLeft
                LeftWidth = NewWidth
            ElseIf CountSubTabs > 0 Then
                NewLeft = 0
                FraLeft = NewWidth
                RightWidth = NewWidth
            End If
            fraVtSplitter(0).Refresh
            If RightWidth < MinRightWidth Then RightWidth = MinRightWidth
        End If
        CheckAll = True
        If FullMaximizedTab >= 0 Then
            TabCover(FullMaximizedTab).Width = NewWidth
            TabCover(FullMaximizedTab).Height = NewHeight
            TabPanel(FullMaximizedTab).Width = NewWidth
            TabPanel(FullMaximizedTab).Height = NewHeight
            AdjustTabPanels -1, True
            CheckAll = False
        ElseIf (LeftMaximizedTab >= 0) And (RightMaximizedTab >= 0) Then
            TabCover(LeftMaximizedTab).Height = NewHeight
            TabCover(LeftMaximizedTab).Width = LeftWidth
            TabCover(RightMaximizedTab).Left = NewLeft
            TabCover(RightMaximizedTab).Height = NewHeight
            TabCover(RightMaximizedTab).Width = RightWidth
            TabPanel(LeftMaximizedTab).Height = NewHeight
            TabPanel(LeftMaximizedTab).Width = LeftWidth
            TabPanel(RightMaximizedTab).Left = NewLeft
            TabPanel(RightMaximizedTab).Height = NewHeight
            TabPanel(RightMaximizedTab).Width = RightWidth
            AdjustTabPanels -1, True
            CheckAll = False
        ElseIf LeftMaximizedTab >= 0 Then
            TabCover(LeftMaximizedTab).Height = NewHeight
            TabCover(LeftMaximizedTab).Width = LeftWidth
            TabPanel(LeftMaximizedTab).Height = NewHeight
            TabPanel(LeftMaximizedTab).Width = LeftWidth
        ElseIf RightMaximizedTab >= 0 Then
            TabCover(RightMaximizedTab).Left = NewLeft
            TabCover(RightMaximizedTab).Height = NewHeight
            TabCover(RightMaximizedTab).Width = RightWidth
            TabPanel(RightMaximizedTab).Left = NewLeft
            TabPanel(RightMaximizedTab).Height = NewHeight
            TabPanel(RightMaximizedTab).Width = RightWidth
        End If
        If CheckAll Then
            If Index <= 0 Then
                LeftTop = 0
                For i = 0 To chkTabIsVisible.UBound
                    If chkTabIsVisible(i).Value = 1 Then
                        tmpTag = chkTabIsVisible(i).Tag
                        j = Val(Mid(tmpTag, 3))
                        If (Left(tmpTag, 1) = "L") And (LeftMaximizedTab < 0) Then
                            'Left MainTabs
                            LCnt = LCnt + 1
                            If (LCnt = CountMainTabs) And LeftRest > 0 Then LeftHeight = LeftRest
                            If Index < 0 Then
                                TabCover(i).Top = LeftTop
                                TabPanel(i).Top = LeftTop
                                If LeftHeight >= 600 Then
                                    TabCover(i).Height = LeftHeight
                                    TabPanel(i).Height = LeftHeight
                                End If
                            End If
                            If (LeftMaximizedTab < 0) Or (Index = LeftMaximizedTab) Then
                                TabCover(i).Left = 0
                                TabCover(i).Width = LeftWidth
                                TabPanel(i).Left = 0
                                TabPanel(i).Width = LeftWidth
                            End If
                            LeftTop = LeftTop + LeftHeight
                            If Index < 0 Then
                                If j >= 0 Then
                                    fraHzSplitter(j).Top = LeftTop
                                    fraCrSplitter(j).Top = LeftTop
                                End If
                                LeftTop = LeftTop + fraHzSplitter(0).Height
                            End If
                            If j >= 0 Then
                                fraCrSplitter(j).Left = fraVtSplitter(0).Left - 150
                                fraHzSplitter(j).Left = 0
                                fraHzSplitter(j).Width = LeftWidth
                                fraHzSplitter(j).Refresh
                                fraCrSplitter(j).Refresh
                            End If
                        ElseIf (Left(tmpTag, 1) = "R") And (RightMaximizedTab < 0) Then
                            'Right SubTabs
                            RCnt = RCnt + 1
                            If (RCnt = CountSubTabs) And RightRest > 0 Then RightHeight = RightRest
                            If Index < 0 Then
                                TabCover(i).Top = RightTop
                                TabPanel(i).Top = RightTop
                                If RightHeight >= 600 Then
                                    TabCover(i).Height = RightHeight
                                    TabPanel(i).Height = RightHeight
                                End If
                            End If
                            If (RightMaximizedTab < 0) Or (Index = RightMaximizedTab) Then
                                TabCover(i).Left = NewLeft
                                TabCover(i).Width = RightWidth
                                TabPanel(i).Left = NewLeft
                                TabPanel(i).Width = RightWidth
                            End If
                            RightTop = RightTop + RightHeight
                            If Index < 0 Then
                                If j >= 0 Then
                                    fraHzSplitter(j).Top = RightTop
                                    fraCrSplitter(j).Top = RightTop
                                End If
                                RightTop = RightTop + fraHzSplitter(0).Height
                            End If
                            If j >= 0 Then
                                fraCrSplitter(j).Left = fraVtSplitter(0).Left + 105
                                fraHzSplitter(j).Left = NewLeft
                                fraHzSplitter(j).Width = RightWidth
                                fraHzSplitter(j).Refresh
                                fraCrSplitter(j).Refresh
                            End If
                        End If
                    End If
                Next
            End If
            AdjustTabPanels -1, True
        End If
    End If
End Sub
Private Sub AdjustTabPanels(Index As Integer, SetVisible As Boolean)
    Dim tmpTag As String
    Dim AdjustMyTab As Boolean
    Dim i As Integer
    Dim j As Integer
    For i = 0 To chkTabIsVisible.UBound
        If SetVisible = True Then
            If chkTabIsVisible(i).Value = 1 Then
                TabPanel(i).Visible = False
                TabCover(i).Visible = False
                tmpTag = chkTabIsVisible(i).Tag
                j = Val(Mid(tmpTag, 3))
                If j >= 0 Then
                    fraCrSplitter(j).Visible = False
                    fraHzSplitter(j).Visible = False
                End If
                tmpTag = Left(tmpTag, 1)
                If FullMaximizedTab >= 0 Then AdjustMyTab = False Else AdjustMyTab = True
                If i = FullMaximizedTab Then AdjustMyTab = True
                If (LeftMaximizedTab >= 0) And (tmpTag = "L") And (i <> LeftMaximizedTab) Then AdjustMyTab = False
                If (RightMaximizedTab >= 0) And (tmpTag = "R") And (i <> RightMaximizedTab) Then AdjustMyTab = False
                If (AdjustMyTab = True) Or (i = Index) Then
                    MinMaxPanel(i).Width = TabPanel(i).Width
                    fraMaxButton(i).Left = MinMaxPanel(i).Width - fraMaxButton(i).Width - 30
                    FileData(i).Width = TabPanel(i).Width - 30
                    FileData(i).Height = TabPanel(i).Height - FileData(i).Top - 15
                    FileData_OnVScroll i, FileData(i).GetVScrollPos
                    TabLookUp(i).Width = TabPanel(i).Width - 30
                    If chkTabCoverIsVisible(i).Value = 1 Then
                        TabCover(i).Top = TabPanel(i).Top + FileData(i).Top + 15
                        TabCover(i).Height = TabPanel(i).Height - FileData(i).Top - 15
                        SelTab1(i).Height = FileData(i).Height
                        SelTab1(i).Width = FileData(i).Width
                        If AutoHideSynchroTabs = True Then TabCover(i).Visible = True
                        TabCover(i).Refresh
                    End If
                    TabPanel(i).Visible = True
                    TabPanel(i).Refresh
                    If (j >= 0) And (i <> RightMaximizedTab) And (i <> FullMaximizedTab) Then
                        fraCrSplitter(j).Visible = True
                        fraHzSplitter(j).Visible = True
                    End If
                End If
            End If
            
        Else
            TabPanel(i).Visible = False
            TabCover(i).Visible = False
        End If
    Next
    If (FullMaximizedTab < 0) And (CountSubTabs > 0) Then
        fraVtSplitter(0).Visible = True
    Else
        fraVtSplitter(0).Visible = False
    End If
End Sub
Private Function CheckMaximezedTabIdx(Index As Integer) As Boolean
    Dim CheckIt As Boolean
    CheckIt = True
    If Index = FullMaximizedTab Then CheckIt = False
    If Index = LeftMaximizedTab Then CheckIt = False
    If Index = RightMaximizedTab Then CheckIt = False
    CheckMaximezedTabIdx = CheckIt
End Function
Private Sub CheckMinMaxPanel(Index As Integer)
    Dim VisibleLines As Long
    Dim PanelOffset As Long
    MinMaxPanel(Index).Width = FileData(Index).Width
    fraMaxButton(Index).Left = MinMaxPanel(Index).Width - fraMaxButton(Index).Width
    If (chkTabIsVisible(Index).Value = 1) And (ShowMinMaxPanels = True) Then
        MinMaxPanel(Index).Visible = True
        MinMaxPanel(Index).ZOrder
        MinMaxPanel(Index).Refresh
    End If
End Sub
Private Sub fraCrSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitterMoves = True
    fraCrSplitter(0).Tag = CStr(X) & "," & CStr(Y)
    CheckTabsSelected fraHzSplitter(Index).Tag, True
End Sub
Private Sub fraCrSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    CheckTabsSelected fraHzSplitter(Index).Tag, False
    fraCrSplitter(0).Tag = "-1,-1"
    SplitterMoves = False
    If LastFocusIndex >= 0 Then FileData(LastFocusIndex).SetFocus
End Sub
Private Sub fraCrSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewLeft As Long
    Dim NewRight As Long
    Dim NewTop As Long
    Dim NewHeight1 As Long
    Dim NewHeight2 As Long
    Dim oldX As Long
    Dim newX As Long
    Dim OldY As Long
    Dim NewY As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    Dim ReturnColor As Long
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = fraCrSplitter(0).Tag
    If tmpTag = "" Then tmpTag = "-1,-1"
    If tmpTag <> "-1,-1" Then
        oldX = CLng(GetItem(tmpTag, 1, ","))
        newX = CLng(X)
        If oldX <> newX Then
            tmpDiff = newX - oldX
            NewLeft = fraCrSplitter(Index).Left + tmpDiff
            tmpDiff = fraVtSplitter(0).Left - fraCrSplitter(Index).Left
            NewRight = WorkArea.ScaleWidth - NewLeft - fraVtSplitter(0).Width
            If (NewLeft >= MinLeftWidth) And (NewRight >= MinRightWidth) Then
                fraVtSplitter(0).Left = NewLeft + tmpDiff
                ArrangeLayout 0
            End If
        End If
        OldY = CLng(GetItem(tmpTag, 2, ","))
        NewY = CLng(Y)
        If OldY <> NewY Then
            tmpDiff = NewY - OldY
            NewTop = fraHzSplitter(Index).Top + tmpDiff
            tmpTag = fraHzSplitter(Index).Tag
            i1 = Val(GetRealItem(tmpTag, 0, ","))
            i2 = Val(GetRealItem(tmpTag, 1, ","))
            NewHeight1 = NewTop - TabPanel(i1).Top
            NewHeight2 = TabPanel(i2).Top + TabPanel(i2).Height - NewTop - fraHzSplitter(0).Height
            If (NewHeight1 > 900) And (NewHeight2 > 900) Then
                TabPanel(i1).Visible = False
                TabPanel(i2).Visible = False
                TabCover(i1).Visible = False
                TabCover(i2).Visible = False
                fraCrSplitter(Index).Visible = False
                fraHzSplitter(Index).Visible = False
                fraVtSplitter(0).Refresh
                If NewHeight1 >= MinMaxPanel(i1).Height Then
                    TabCover(i1).Height = NewHeight1
                    TabPanel(i1).Height = NewHeight1
                End If
                fraHzSplitter(Index).Top = NewTop
                fraCrSplitter(Index).Top = NewTop
                NewTop = NewTop + fraHzSplitter(0).Height
                TabCover(i2).Top = NewTop
                TabPanel(i2).Top = NewTop
                If NewHeight2 >= MinMaxPanel(i2).Height Then
                    TabCover(i2).Height = NewHeight2
                    TabPanel(i2).Height = NewHeight2
                End If
                fraHzSplitter(Index).Visible = True
                fraHzSplitter(Index).Refresh
                fraCrSplitter(Index).Visible = True
                fraCrSplitter(Index).Refresh
                AdjustTabPanels -1, True
            End If
        End If
    End If
End Sub

Private Sub fraHzSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitterMoves = True
    fraHzSplitter(0).Tag = CStr(X) & "," & CStr(Y)
    CheckTabsSelected fraHzSplitter(Index).Tag, True
End Sub
Private Sub fraHzSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    CheckTabsSelected fraHzSplitter(Index).Tag, False
    fraHzSplitter(0).Tag = "-1,-1"
    If (Index = 1) And (CountMainTabs = 2) Then
        SplitPosV = (FileData(0).Height + MinMaxPanel(0).Height - 15) * 100 / (WorkArea.Height - fraHzSplitter(0).Height)
    End If
    SplitterMoves = False
    If LastFocusIndex >= 0 Then
        FileData(LastFocusIndex).SetFocus
    End If
End Sub
Private Sub fraHzSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewTop As Long
    Dim NewHeight1 As Long
    Dim NewHeight2 As Long
    Dim OldY As Long
    Dim NewY As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    Dim ReturnColor As Long
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = fraHzSplitter(0).Tag
    If tmpTag <> "-1,-1" Then
        OldY = CLng(GetItem(tmpTag, 2, ","))
        NewY = CLng(Y)
        If OldY <> NewY Then
            tmpDiff = NewY - OldY
            NewTop = fraHzSplitter(Index).Top + tmpDiff
            tmpTag = fraHzSplitter(Index).Tag
            i1 = Val(GetRealItem(tmpTag, 0, ","))
            i2 = Val(GetRealItem(tmpTag, 1, ","))
            NewHeight1 = NewTop - TabPanel(i1).Top
            NewHeight2 = TabPanel(i2).Top + TabPanel(i2).Height - NewTop - fraHzSplitter(0).Height
            If (NewHeight1 > 900) And (NewHeight2 > 900) Then
                TabPanel(i1).Visible = False
                TabCover(i1).Visible = False
                TabPanel(i2).Visible = False
                TabCover(i2).Visible = False
                fraCrSplitter(Index).Visible = False
                fraHzSplitter(Index).Visible = False
                fraVtSplitter(0).Refresh
                If NewHeight1 >= MinMaxPanel(i1).Height Then
                    TabCover(i1).Height = NewHeight1
                    TabPanel(i1).Height = NewHeight1
                End If
                fraHzSplitter(Index).Top = NewTop
                fraCrSplitter(Index).Top = NewTop
                NewTop = NewTop + fraHzSplitter(0).Height
                TabCover(i2).Top = NewTop
                TabPanel(i2).Top = NewTop
                If NewHeight2 >= MinMaxPanel(i1).Height Then
                    TabCover(i2).Height = NewHeight2
                    TabPanel(i2).Height = NewHeight2
                End If
                fraHzSplitter(Index).Visible = True
                fraHzSplitter(Index).Refresh
                fraCrSplitter(Index).Visible = True
                fraCrSplitter(Index).Refresh
                AdjustTabPanels -1, True
            End If
        End If
    End If
End Sub

Private Sub fraMaxButton_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    FileData(Index).SetFocus
End Sub

Private Sub fraVtSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraVtSplitter(0).Tag = CStr(X) & "," & CStr(Y)
End Sub

Private Sub fraVtSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraVtSplitter(0).Tag = "-1,-1"
    If LastFocusIndex >= 0 Then FileData(LastFocusIndex).SetFocus
End Sub
Private Sub fraVtSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewLeft As Long
    Dim NewRight As Long
    Dim oldX As Long
    Dim newX As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    tmpTag = fraVtSplitter(Index).Tag
    If tmpTag <> "-1,-1" Then
        If Index = 0 Then
            oldX = CLng(GetItem(tmpTag, 1, ","))
            newX = CLng(X)
            If oldX <> newX Then
                tmpDiff = newX - oldX
                NewLeft = fraVtSplitter(Index).Left + tmpDiff
                NewRight = WorkArea.ScaleWidth - NewLeft - fraVtSplitter(Index).Width
                If (NewLeft >= MinLeftWidth) And (NewRight >= MinRightWidth) Then
                    fraVtSplitter(Index).Left = NewLeft
                    ArrangeLayout 0
                End If
            End If
        End If
    End If
End Sub
Private Sub CheckTabsSelected(TabList As String, Selected As Boolean)
    Dim tmpTag As String
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = TabList
    i1 = Val(GetRealItem(tmpTag, 0, ","))
    i2 = Val(GetRealItem(tmpTag, 1, ","))
    If Selected Then
        If i1 >= 0 Then
            chkTabIsVisible(i1).Appearance = 0
            'lblTabName(i1).FontBold = True
        End If
        If i2 >= 0 Then
            chkTabIsVisible(i2).Appearance = 0
            'lblTabName(i2).FontBold = True
        End If
    Else
        If i1 >= 0 Then
            chkTabIsVisible(i1).Appearance = 1
            'lblTabName(i1).FontBold = False
        End If
        If i2 >= 0 Then
            chkTabIsVisible(i2).Appearance = 1
            'lblTabName(i2).FontBold = False
        End If
    End If
End Sub
Private Sub ResetMain()
    Dim ColNo As Long
    Dim MaxCol As Long
    Dim i As Integer
    ResetFlightPanel
    WorkArea.Visible = False
    For i = 0 To FileData.UBound
        FileData(i).ResetContent
        FileData(i).ColSelectionRemoveAll
        MaxCol = FileData(i).GetColumnCount - 1
        For ColNo = 0 To MaxCol
            FileData(i).ResetColumnProperty ColNo
        Next
    Next
    For i = MaxMainButtonRow + 1 To fraTopButtonPanel.UBound
        fraTopButtonPanel(i).Visible = False
    Next
    For i = 0 To chkTabIsVisible.UBound
        chkTabIsVisible(i).Value = 0
        chkTabIsVisible(i).Visible = False
        chkTabIsVisible(i).Tag = ""
        lblTabName(i).Visible = False
        lblTabShadow(i).Visible = False
    Next
    For i = 0 To MinMaxPanel.UBound
        MinMaxPanel(i).Visible = False
    Next
    For i = 0 To fraHzSplitter.UBound
        fraCrSplitter(i).Visible = False
        fraHzSplitter(i).Visible = False
    Next
    For i = 0 To fraVtSplitter.UBound
        fraVtSplitter(i).Visible = False
    Next
    For i = 0 To linLight.UBound
        linLight(i).Visible = False
    Next
    For i = 0 To linDark.UBound
        linDark(i).Visible = False
    Next
    For i = 0 To TabPanel.UBound
        TabPanel(i).Visible = False
        TabCover(i).Visible = False
        chkTabCoverIsVisible(i).Value = 0
    Next
    For i = 0 To chkWork.UBound
        If i > MaxMainButtonIdx Then
            chkWork(i).Enabled = False
            chkWork(i).Tag = ""
            chkWork(i).Value = 0
            chkWork(i).BackColor = MyOwnButtonFace
        End If
    Next
    Unload FilterDialog
    Unload MyBookMarks
    HiddenData.RemoveImpConfig
    TopPanel.Height = fraTopButtonPanel(0).Top + fraTopButtonPanel(0).Height + 75
    WorkArea.Top = TopPanel.Height
    'fraTopButtonPanel(0).Visible = True
    Form_Resize
    WorkArea.Visible = True
    AdjustServerSignals False, 0
    TabEditLookUpFixed = -1
    TabEditLookUpValueCol = -1
    TabEditLookUpType = "CLOSED"
    TabEditLookUpIndex = -1
    TabEditColNo = -1
    Me.Refresh
End Sub

Public Function AdjustServerSignals(JustReading As Boolean, SignalIdx As Integer)
    If UfisServer.HostName.Text = "LOCAL" Then
        ServerSignal(3).FillStyle = 0
        ServerSignal(3).FillColor = vbBlue
    Else
        If ServerIsAvailable Then
            ServerSignal(3).FillStyle = 0
            ServerSignal(3).FillColor = vbGreen
            If CedaIsConnected Then
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbGreen
            Else
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbBlue
            End If
            If JustReading Then
                ServerSignal(2).FillColor = vbGreen
                ServerSignal(2).Refresh
                ServerSignal(SignalIdx).FillColor = vbYellow
                ServerSignal(SignalIdx).FillStyle = 0
                ServerSignal(SignalIdx).Refresh
            Else
                ServerSignal(SignalIdx).FillColor = vbGreen
                ServerSignal(SignalIdx).FillStyle = 1
                ServerSignal(SignalIdx).Refresh
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbBlue
                ServerSignal(2).Refresh
            End If
        Else
            ServerSignal(3).FillStyle = 0
            ServerSignal(3).FillColor = vbRed
            ServerSignal(2).FillStyle = 0
            ServerSignal(2).FillColor = vbRed
            ServerSignal(0).FillStyle = 0
            ServerSignal(0).FillColor = vbRed
            ServerSignal(1).FillStyle = 0
            ServerSignal(1).FillColor = vbRed
        End If
    End If
End Function

Private Sub imgCurPos_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpData As String
    Dim LineStamp As String
    Dim HitList As String
    Dim SetLineNo As Long
    Dim OldOffset As Long
    Dim NewScroll As Long
    Dim VisLines As Long
    tmpTag = Trim(imgCurPos(Index).Tag)
    LineStamp = GetItem(tmpTag, 1, ",")
    If LineStamp <> "" Then
        HitList = FileData(Index).GetLinesByIndexValue("BookMark", LineStamp, 0)
        If HitList <> "" Then
            tmpData = GetItem(tmpTag, 2, ",")
            OldOffset = Val(tmpData)
            SetLineNo = Val(HitList)
            VisLines = VisibleTabLines(FileData(Index))
            NewScroll = GetBestScroll(SetLineNo, OldOffset, VisLines)
            FileData(Index).OnVScrollTo NewScroll
            FileData(Index).SetCurrentSelection SetLineNo
        End If
    End If
End Sub

Private Sub lblDlyReason_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If Not ReadOnlyUser Then
            lblDlyReason(Index).Tag = CStr(chkDlyTick(Index).Value)
            chkDlyTick(Index).Value = 2
        End If
    End If
End Sub

Private Sub lblDlyReason_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If Not ReadOnlyUser Then
            If Val(lblDlyReason(Index).Tag) = 1 Then
                chkDlyTick(Index).Value = 0
            Else
                chkDlyTick(Index).Value = 1
            End If
        End If
    End If
End Sub

Private Sub lblTabCaption_DblClick(Index As Integer)
    If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
End Sub

Private Sub lblTabCaption_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    FileData(Index).SetFocus
    TabCover(Index).Visible = False
End Sub

Private Sub lblTabCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    FileData(Index).SetFocus
    If TabCover(Index).Visible = True Then
        TabCover(Index).Visible = False
    ElseIf chkTabCoverIsVisible(Index).Value = 1 Then
        'If AutoHideSynchroTabs = True Then TabCover(Index).Visible = True
    End If
End Sub

Private Sub lblTabName_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If chkTabIsVisible(Index).Enabled Then
            lblTabName(Index).Tag = CStr(chkTabIsVisible(Index).Value)
            chkTabIsVisible(Index).Value = 2
        End If
    End If
End Sub

Private Sub lblTabName_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If chkTabIsVisible(Index).Enabled Then
            If Val(lblTabName(Index).Tag) = 1 Then
                chkTabIsVisible(Index).Value = 0
            Else
                chkTabIsVisible(Index).Value = 1
            End If
        End If
    End If
End Sub

Private Sub lblTabPos_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    FileData(Index).SetFocus
    If TabCover(Index).Visible = True Then
        'TabCover(Index).Visible = False
    ElseIf chkTabCoverIsVisible(Index).Value = 1 Then
        If AutoHideSynchroTabs = True Then TabCover(Index).Visible = True
    End If
End Sub

Private Sub MainTimer_Timer(Index As Integer)
    Dim CurActionCmd As String
    Dim tmpData As String
    Dim tmpTag As String
    Dim tmpCode As String
    Dim i As Integer
    Dim NowTime As String
    If Index = 4 Then
        NowTime = Format(Now, "hhmm")
        Select Case NowTime
            Case "0900" To "0930"
                chkAppl(4).Caption = "Coffee"
            Case "1200" To "1400"
                chkAppl(4).Caption = "Lunch"
            Case "1800" To "2000"
                chkAppl(4).Caption = "Diner"
            Case Else
                chkAppl(4).Caption = "Break"
        End Select
    Else
        CurActionCmd = GetRealItem(MainTimer(Index).Tag, 0, ",")
        Select Case CurActionCmd
            Case "REFRESH"
                MainTimer(Index).Enabled = False
                WorkArea.Refresh
            Case "PUSH"
                MainTimer(Index).Enabled = False
                tmpData = GetRealItem(MainTimer(Index).Tag, 1, ",")
                i = Val(tmpData)
                chkImpTyp(i).Value = 1
                tmpCode = GetRealItem(picBall(i).Tag, 0, "|")
                If Left(tmpCode, 1) = "Y" Then chkImpTyp(i).Value = 0
            Case "AUTO"
                MainTimer(Index).Enabled = False
                tmpData = GetRealItem(MainTimer(Index).Tag, 1, ",")
                i = Val(tmpData)
                chkImpTyp(i).Value = 1
                tmpCode = GetRealItem(picBall(i).Tag, 0, "|")
                If Left(tmpCode, 1) = "Y" Then chkImpTyp(i).Value = 0
                chkTask(0).Value = 1    'AutoLoad (Refresh)
            Case "STOP_EXCEL"
                MainTimer(Index).Enabled = False
                PushWorkButton "EXCEL", 0
                PushWorkButton "TO_EXCEL", 0
            Case "STOP_WORD"
                MainTimer(Index).Enabled = False
                PushWorkButton "TO_WORD", 0
            Case "CHECK_DTAB"
                TimerIsActive = True
                MainTimer(Index).Enabled = False
                tmpData = MainTimer(Index).Tag
                CheckDtabGroup tmpData
                MainTimer(Index).Tag = ""
                TimerIsActive = False
            Case "EXIT"
                MainTimer(Index).Enabled = False
                chkTerminate.Value = 1
            Case Else
                MainTimer(Index).Enabled = False
        End Select
    End If
End Sub

Private Sub ResetFlightPanel()
    Dim i As Integer
    For i = 0 To lblArr.UBound
        lblArr(i).Caption = ""
        lblArr(i).BackColor = DarkGray
        lblArr(i).Tag = ""
        lblDep(i).Caption = ""
        lblDep(i).BackColor = DarkGray
        lblDep(i).Tag = ""
    Next
    For i = 0 To txtAftData.UBound
        txtAftData(i).Text = ""
    Next
    ButtonPushedInternal = True
    chkSelToggle(0).Value = 0
    chkSelFlight(0).Value = 0
    chkSelFlight(1).Value = 0
    ButtonPushedInternal = False
    FlightPanel.Refresh
End Sub
Private Sub InitFlightPanel(ResetIt As Boolean, Index As Integer, ArrDepKey As String, FldList As String, DatList As String)
    Dim tmpTag As String
    Dim tmpFipsCfg As String
    Dim tmpFipsParam As String
    Dim tmpFipsTurnType As String
    Dim tmpArrUrno As String
    Dim tmpDepUrno As String
    Dim CedaData As String
    Dim tmpRec As String
    Dim tmpArrRec As String
    Dim tmpDepRec As String
    Dim tmpUrno As String
    Dim AllFldLst As String
    Dim SqlKey As String
    Dim tmpMyCall As String
    Dim MsgText As String
    Dim ArrColor As Long
    Dim DepColor As Long
    Dim FlnuValue As Long
    Dim i As Integer
    If ResetIt Then
        ReorgFlightPanel = True
        AllFldLst = "FLNO,STOD,STOA,ETDI,ETAI,OFBL,ONBL,ACT3,ACT5,REGN,TIFA,TIFD,REM1,BAA1,BAA2,BAA3,PSTA,PSTD,GTA1,GTD1,URNO"
        If FipsIsConnected Then
            ResetFlightPanel
            tmpTag = FlightPanel.Tag
            tmpFipsCfg = GetRealItem(tmpTag, 0, "|")
            tmpFipsParam = GetRealItem(tmpTag, 1, "|")
            tmpFipsTurnType = GetRealItem(tmpFipsCfg, 1, ",")
            tmpArrUrno = GetRealItem(tmpFipsParam, 1, ",")
            tmpDepUrno = GetRealItem(tmpFipsParam, 2, ",")
            If (tmpArrUrno <> "") Or (tmpDepUrno <> "") Then
                SqlKey = "WHERE URNO=" & tmpArrUrno & " OR URNO=" & tmpDepUrno
                'UfisServer.ConnectToCeda
                UfisServer.CallCeda CedaData, "GFR", "AFTTAB", AllFldLst, "", SqlKey, "", 0, True, False
                'UfisServer.DisconnectFromCeda
            Else
                CedaData = ""
            End If
            CedaData = Replace(CedaData, vbCr, "", 1, -1, vbBinaryCompare)
            tmpRec = GetRealItem(CedaData, 0, vbLf)
            tmpUrno = GetFieldValue("URNO", tmpRec, AllFldLst)
            If tmpUrno = tmpArrUrno Then tmpArrRec = tmpRec
            If tmpUrno = tmpDepUrno Then tmpDepRec = tmpRec
            tmpRec = GetRealItem(CedaData, 1, vbLf)
            tmpUrno = GetFieldValue("URNO", tmpRec, AllFldLst)
            If tmpUrno = tmpArrUrno Then tmpArrRec = tmpRec
            If tmpUrno = tmpDepUrno Then tmpDepRec = tmpRec
            Select Case tmpFipsTurnType
                Case "ARR"
                    If tmpArrRec <> "" Then ArrColor = vbWhite Else ArrColor = vbRed
                    If tmpDepRec <> "" Then DepColor = NormalGray Else DepColor = DarkGray
                    If MainLifeStyle Then lblArrRow.ForeColor = vbYellow Else lblArrRow.ForeColor = vbButtonText
                    If MainLifeStyle Then lblDepRow.ForeColor = NormalGray Else lblDepRow.ForeColor = vbButtonText
                Case "DEP"
                    If tmpDepRec <> "" Then DepColor = vbWhite Else DepColor = vbRed
                    If tmpArrRec <> "" Then ArrColor = NormalGray Else ArrColor = DarkGray
                    If MainLifeStyle Then lblDepRow.ForeColor = vbGreen Else lblDepRow.ForeColor = vbButtonText
                    If MainLifeStyle Then lblArrRow.ForeColor = vbBlack Else lblArrRow.ForeColor = vbButtonText
                Case "ROT"
                    If tmpArrRec <> "" Then ArrColor = vbWhite Else ArrColor = vbRed
                    If tmpDepRec <> "" Then DepColor = vbWhite Else DepColor = vbRed
                    If MainLifeStyle Then lblArrRow.ForeColor = vbYellow Else lblArrRow.ForeColor = vbButtonText
                    If MainLifeStyle Then lblDepRow.ForeColor = vbGreen Else lblDepRow.ForeColor = vbButtonText
                Case Else
            End Select
            For i = 0 To lblArr.UBound
                lblArr(i).BackColor = ArrColor
                lblDep(i).BackColor = DepColor
            Next
            ShowArrRecord tmpArrRec, AllFldLst
            ShowDepRecord tmpDepRec, AllFldLst
            txtAftData(0).Text = tmpFipsTurnType
            txtAftData(1).Text = tmpArrUrno
            txtAftData(2).Text = tmpArrRec
            txtAftData(3).Text = tmpDepUrno
            txtAftData(4).Text = tmpDepRec
            txtAftData(5).Text = AllFldLst
            For i = 0 To chkTabIsVisible.UBound
                If chkTabIsVisible(i).Visible Then
                    HiddenData.UpdateTabConfig FileData(i).myName, "CALL", tmpFipsTurnType
                End If
            Next
            FlightPanel.Refresh
            MsgText = ""
            For i = 0 To chkTabIsVisible.UBound
                If chkTabIsVisible(i).Visible Then
                    tmpMyCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                    Select Case tmpMyCall
                        Case "ARR"
                            FlnuValue = Val(tmpArrUrno)
                            If FlnuValue <= 0 Then MsgText = "Warning: The Arrival Flight doesn't exist."
                            chkSelFlight(0).Value = 1
                            chkSelFlight(1).Value = 0
                            chkSelFlight(0).Enabled = False
                            chkSelFlight(1).Enabled = False
                            chkSelToggle(0).Enabled = False
                        Case "DEP"
                            FlnuValue = Val(tmpDepUrno)
                            If FlnuValue <= 0 Then MsgText = "Warning: The Departure Flight doesn't exist."
                            chkSelFlight(0).Value = 0
                            chkSelFlight(1).Value = 1
                            chkSelFlight(0).Enabled = False
                            chkSelFlight(1).Enabled = False
                            chkSelToggle(0).Enabled = False
                        Case "ROT"
                            chkSelFlight(0).Value = 0
                            chkSelFlight(1).Value = 1
                            chkSelFlight(0).Enabled = True
                            chkSelFlight(1).Enabled = True
                            chkSelToggle(0).Enabled = True
                        Case Else
                    End Select
                End If
            Next
            If MsgText <> "" Then
                If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "info", "", UserAnswer) = 1 Then DoNothing
            End If
        Else
            chkSelToggle(0).Visible = False
            chkSelFlight(0).Caption = "ARR"
            chkSelFlight(1).Caption = "DEP"
            Select Case ArrDepKey
                Case "A"
                    ShowArrRecord DatList, FldList
                    tmpArrUrno = GetFieldValue("URNO", DatList, FldList)
                    If chkSelFlight(0).Value = 1 Then
                        If tmpArrUrno <> "" Then ArrColor = vbWhite Else ArrColor = DarkGray
                    Else
                        ArrColor = NormalGray
                    End If
                    For i = 0 To lblArr.UBound
                        lblArr(i).BackColor = ArrColor
                    Next
                    txtAftData(0).Text = "ROT"
                    txtAftData(1).Text = tmpArrUrno
                    txtAftData(2).Text = DatList
                    txtAftData(6).Text = FldList
                    If tmpArrUrno <> "" Then
                        For i = 6 To 8
                            lblArr(i).BackColor = vbWhite
                        Next
                    End If
                Case "D"
                    ShowDepRecord DatList, FldList
                    tmpDepUrno = GetFieldValue("URNO", DatList, FldList)
                    If chkSelFlight(1).Value = 1 Then
                        If tmpDepUrno <> "" Then DepColor = vbWhite Else DepColor = DarkGray
                    Else
                        DepColor = NormalGray
                    End If
                    For i = 0 To lblDep.UBound
                        lblDep(i).BackColor = DepColor
                    Next
                    txtAftData(0).Text = "ROT"
                    txtAftData(3).Text = tmpDepUrno
                    txtAftData(4).Text = DatList
                    txtAftData(7).Text = FldList
                    If tmpDepUrno <> "" Then
                        For i = 6 To 8
                            lblDep(i).BackColor = vbWhite
                        Next
                    End If
                Case Else
            End Select
            
        End If
        ReorgFlightPanel = False
    End If
End Sub
Public Sub ShowArrRecord(ArrData As String, FieldList As String)
    Dim FldNam As String
    Dim FldVal As String
    Dim tmpVal As String
    Dim ItemNo As Long
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        FldVal = GetRealItem(ArrData, ItemNo, ",")
        Select Case FldNam
            Case "FLNO"
                If ArrData = "" Then FldVal = lblArr(0).Tag
                lblArr(0).Tag = FldVal
                tmpVal = Trim(Left(FldVal, 3))
                lblArr(0).Caption = tmpVal
                tmpVal = Trim(Mid(FldVal, 4))
                lblArr(1).Caption = tmpVal
            Case "STOA"
                If ArrData = "" Then FldVal = lblArr(2).Tag
                lblArr(2).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Left(FldVal, 8)
                tmpVal = MyDateFormat(tmpVal, False)
                lblArr(2).Caption = tmpVal
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(3).Caption = tmpVal
            Case "ETAI"
                If ArrData = "" Then FldVal = lblArr(4).Tag
                lblArr(4).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(4).Caption = tmpVal
            Case "ONBL"
                If ArrData = "" Then FldVal = lblArr(5).Tag
                lblArr(5).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(5).Caption = tmpVal
            Case "ACT5"
                If ArrData = "" Then FldVal = lblArr(6).Tag
                lblArr(6).Tag = FldVal
                lblArr(6).Caption = FldVal
                If FldVal <> "" Then lblArr(6).ZOrder
            Case "ACT3"
                If ArrData = "" Then FldVal = lblArr(7).Tag
                lblArr(7).Tag = FldVal
                lblArr(7).Caption = FldVal
                If FldVal <> "" Then lblArr(7).ZOrder
            Case "REGN"
                If ArrData = "" Then FldVal = lblArr(8).Tag
                lblArr(8).Tag = FldVal
                lblArr(8).Caption = FldVal
                If ArrData <> "" Then lblArr(8).ZOrder
            Case "REM1"
                If ArrData = "" Then FldVal = lblArr(9).Tag
                lblArr(9).Tag = FldVal
                lblArr(9).Caption = FldVal
            Case "BAA1"
                If ArrData = "" Then FldVal = lblArr(10).Tag
                lblArr(10).Tag = FldVal
                lblArr(10).Caption = FldVal
            Case "BAA2"
                If ArrData = "" Then FldVal = lblArr(11).Tag
                lblArr(11).Tag = FldVal
                lblArr(11).Caption = FldVal
            Case "BAA3"
                If ArrData = "" Then FldVal = lblArr(12).Tag
                lblArr(12).Tag = FldVal
                lblArr(12).Caption = FldVal
            Case "PSTA"
                If ArrData = "" Then FldVal = lblArr(14).Tag
                lblArr(14).Tag = FldVal
                lblArr(14).Caption = FldVal
            Case "GTA1"
                If ArrData = "" Then FldVal = lblArr(15).Tag
                lblArr(15).Tag = FldVal
                lblArr(15).Caption = FldVal
            Case Else
        End Select
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
End Sub
Public Sub ShowDepRecord(DepData As String, FieldList As String)
    Dim FldNam As String
    Dim FldVal As String
    Dim tmpVal As String
    Dim ItemNo As Long
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        FldVal = GetRealItem(DepData, ItemNo, ",")
        Select Case FldNam
            Case "FLNO"
                If DepData = "" Then FldVal = lblDep(0).Tag
                lblDep(0).Tag = FldVal
                tmpVal = Trim(Left(FldVal, 3))
                lblDep(0).Caption = tmpVal
                tmpVal = Trim(Mid(FldVal, 4))
                lblDep(1).Caption = tmpVal
            Case "STOD"
                If DepData = "" Then FldVal = lblDep(2).Tag
                lblDep(2).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Left(FldVal, 8)
                tmpVal = MyDateFormat(tmpVal, False)
                lblDep(2).Caption = tmpVal
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(3).Caption = tmpVal
            Case "ETDI"
                If DepData = "" Then FldVal = lblDep(4).Tag
                lblDep(4).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(4).Caption = tmpVal
            Case "OFBL"
                If DepData = "" Then FldVal = lblDep(5).Tag
                lblDep(5).Tag = FldVal
                FldVal = CheckTimeDisplay(FldVal, True)
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(5).Caption = tmpVal
            Case "ACT5"
                If DepData = "" Then FldVal = lblDep(6).Tag
                lblDep(6).Tag = FldVal
                lblDep(6).Caption = FldVal
                If FldVal <> "" Then lblDep(6).ZOrder
            Case "ACT3"
                If DepData = "" Then FldVal = lblDep(7).Tag
                lblDep(7).Tag = FldVal
                lblDep(7).Caption = FldVal
                If FldVal <> "" Then lblDep(7).ZOrder
            Case "REGN"
                If DepData = "" Then FldVal = lblDep(8).Tag
                lblDep(8).Tag = FldVal
                lblDep(8).Caption = FldVal
                If FldVal <> "" Then lblDep(8).ZOrder
            Case "REM1"
                If DepData = "" Then FldVal = lblDep(9).Tag
                lblDep(9).Tag = FldVal
                lblDep(9).Caption = FldVal
            Case "BAA1"
                If DepData = "" Then FldVal = lblDep(10).Tag
                lblDep(10).Tag = FldVal
                lblDep(10).Caption = FldVal
            Case "BAA2"
                If DepData = "" Then FldVal = lblDep(11).Tag
                lblDep(11).Tag = FldVal
                lblDep(11).Caption = FldVal
            Case "BAA3"
                If DepData = "" Then FldVal = lblDep(12).Tag
                lblDep(12).Tag = FldVal
                lblDep(12).Caption = FldVal
            Case "PSTD"
                If DepData = "" Then FldVal = lblDep(14).Tag
                lblDep(14).Tag = FldVal
                lblDep(14).Caption = FldVal
            Case "GTD1"
                If DepData = "" Then FldVal = lblDep(15).Tag
                lblDep(15).Tag = FldVal
                lblDep(15).Caption = FldVal
            Case Else
        End Select
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
End Sub

Private Sub CreateDelayPanel(Index As Integer, CfgList As String, SectionKey As String)
    Dim ItemNo As Long
    Dim GrpNo As Long
    Dim i As Integer
    Dim IdxItm As String
    Dim IdxGrp As String
    Dim IdxVal As Integer
    Dim CaptionList As String
    Dim tmpCapt As String
    Dim tmpStr As String
    Dim NewTop As Long
    Dim NewLeft As Long
    tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_DIALOG_INDX", "")
    
    For i = 0 To DlyEditPanel.UBound
        DlyEditPanel(i).Visible = False
    Next
    NewTop = Val(GetRealItem(CfgList, 3, "|")) * 15
    If NewTop < 30 Then NewTop = DlyEditPanel(0).Top
    NewLeft = DlyEditPanel(0).Left
    
    i = 0
    GrpNo = 0
    CaptionList = GetRealItem(CfgList, GrpNo, "|")
    IdxGrp = GetRealItem(tmpStr, GrpNo, "|")
    While GrpNo < 3
        ItemNo = 0
        tmpCapt = GetRealItem(CaptionList, ItemNo, ",")
        IdxItm = GetRealItem(IdxGrp, ItemNo, ",")
        While tmpCapt <> ""
            i = i + 1
            If i > DlyEditPanel.UBound Then
                Load DlyEditPanel(i)
                Load chkDlyTick(i)
                Load lblDlyReason(i)
                Load txtDlyInput(i)
                Load chkDlyCheck(i)
                Set DlyEditPanel(i).Container = DelayPanel
                Set chkDlyTick(i).Container = DlyEditPanel(i)
                Set lblDlyReason(i).Container = DlyEditPanel(i)
                Set txtDlyInput(i).Container = DlyEditPanel(i)
                Set chkDlyCheck(i).Container = DlyEditPanel(i)
            End If
            lblDlyReason(i).Caption = tmpCapt
            If IdxItm <> "" Then IdxVal = Val(IdxItm) Else IdxVal = i
            'lblDlyReason(i).Tag = Right("00" & CStr(IdxVal), 2)
            chkDlyCheck(i).Tag = Right("00" & CStr(IdxVal), 2)
            'lblDlyReason(i).ToolTipText = CStr(i) & " / " & chkDlyCheck(i).Tag
            txtDlyInput(i).Text = ":"
            txtDlyInput(i).BackColor = NormalGray
            chkDlyTick(i).Value = 0
            chkDlyTick(i).Visible = True
            lblDlyReason(i).Visible = True
            txtDlyInput(i).Visible = True
            'chkDlyCheck(i).Visible = True
            DlyEditPanel(i).Top = NewTop
            DlyEditPanel(i).Left = NewLeft
            DlyEditPanel(i).Tag = CStr(GrpNo)
            
            DlyEditPanel(i).Visible = True
            If MainLifeStyle Then DrawBackGround DlyEditPanel(i), WorkAreaColor, False, True
            NewTop = NewTop + DlyEditPanel(i).Height
            ItemNo = ItemNo + 1
            tmpCapt = GetRealItem(CaptionList, ItemNo, ",")
            IdxItm = GetRealItem(IdxGrp, ItemNo, ",")
        Wend
        NewTop = NewTop + 60
        GrpNo = GrpNo + 1
        CaptionList = GetRealItem(CfgList, GrpNo, "|")
        IdxGrp = GetRealItem(tmpStr, GrpNo, "|")
    Wend
    DelayPanel.Tag = CStr(Index)
    DelayPanel.Visible = True
    Form_Resize
End Sub

Private Sub InitDelayPanel(Index As Integer, UrnoLine As Long, UseValue As String)
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim tmpArea As String
    Dim tmpData As String
    Dim PanelNo As Integer
    If UrnoLine < 0 Then
        For PanelNo = 0 To DlyEditPanel.UBound
            chkDlyTick(PanelNo).Value = 0
            txtDlyInput(PanelNo).Text = ":"
            txtDlyInput(PanelNo).Tag = ":"
            txtDlyInput(PanelNo).Locked = True
            txtDlyInput(PanelNo).BackColor = NormalGray
            If ReadOnlyUser Then chkDlyTick(PanelNo).Enabled = False Else chkDlyTick(PanelNo).Enabled = True
        Next
        BgnLine = 0
        MaxLine = FileData(Index).GetLineCount - 1
    Else
        BgnLine = UrnoLine
        MaxLine = UrnoLine
    End If
    For CurLine = BgnLine To MaxLine
        tmpArea = FileData(Index).GetFieldValue(CurLine, "AREA")
        If tmpArea = "B" Then
            tmpData = FileData(Index).GetFieldValue(CurLine, "INDX")
            If tmpData <> "" Then
                PanelNo = GetDlyPanelIndex(tmpData)
                If (PanelNo >= 0) And (PanelNo <= DlyEditPanel.UBound) Then
                    tmpData = FileData(Index).GetFieldValue(CurLine, "TIME")
                    If UseValue <> "" Then tmpData = UseValue
                    txtDlyInput(PanelNo).Tag = tmpData
                    If UrnoLine >= 0 Then chkDlyTick(PanelNo).Value = 0
                    txtDlyInput(PanelNo).Text = tmpData
                End If
            End If
        End If
    Next
End Sub
Private Function GetDlyPanelIndex(IndxValue As String) As Integer
    Dim PanelNo As Integer
    Dim i As Integer
    PanelNo = -1
    For i = 0 To DlyEditPanel.UBound
        If chkDlyCheck(i).Tag = IndxValue Then
            PanelNo = i
            Exit For
        End If
    Next
    GetDlyPanelIndex = PanelNo
End Function

Private Sub MinMaxPanel_DblClick(Index As Integer)
    If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
End Sub

Private Sub MinMaxPanel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    FileData(Index).SetFocus
    'If TabCover(Index).Visible = True Then
    '    TabCover(Index).Visible = False
    'ElseIf chkTabCoverIsVisible(Index).Value = 1 Then
    '    'If AutoHideSynchroTabs = True Then TabCover(Index).Visible = True
    'End If
End Sub

Private Sub MsExcel_SheetChange(ByVal Sh As Object, ByVal Target As Excel.Range)
    Static AlreadyShown As Long
    Dim tmpMsg As String
    Dim tmpLineData As String
    Dim tmpData As String
    Dim i As Integer
    Dim j As Integer
    Dim CurRow As Integer
    Dim MaxRow As Integer
    Dim CurCol As Integer
    Dim MaxCol As Integer
    Dim MaxAreas As Integer
    Dim ColCount As Integer
    On Error Resume Next
    If Not ExcelJustOpened Then
        If FipsIsConnected Then
            If (Not ExcelUpdateAllowed) Or (ReadOnlyUser) Then
                'Permission denied
                If Sh.Name = MsWkSheet.Name Then
                    If (AlreadyShown Mod 5) = 0 Then
                        MsExcel.WindowState = xlMinimized
                        Me.SetFocus
                        tmpMsg = "Sorry. You shouldn't change anything."
                        If ReadOnlyUser Then tmpMsg = tmpMsg & vbNewLine & "(You are a 'Read Only' User)"
                        If AlreadyShown >= 10 Then tmpMsg = tmpMsg & vbNewLine & "Excel will be closed now."
                        If MyMsgBox.CallAskUser(0, 0, 0, "Updating the Worksheet ?", tmpMsg, "stopit", "", UserAnswer) = 1 Then DoNothing
                        SetFormOnTop Me, False
                        MsExcel.WindowState = xlNormal
                    End If
                    ExcelJustOpened = True
                    CurRow = Target.Row
                    MaxRow = CurRow + Target.Rows.Count - 1
                    CurCol = Target.Column
                    MaxCol = CurCol + Target.Columns.Count - 1
                    For i = CurRow To MaxRow
                        tmpLineData = FileData(LastFocusIndex).GetFieldValues(CLng(i - 1), ExcelFields)
                        For j = CurCol To MaxCol
                            tmpData = GetItem(tmpLineData, j, ",")
                            tmpData = CleanString(tmpData, SERVER_TO_CLIENT, False)
                            MsWkSheet.Cells(i, j) = tmpData
                        Next
                    Next
                    MsWkBook.Saved = True
                    ExcelJustOpened = False
                    If AlreadyShown >= 10 Then
                        PushWorkButton "EXCEL", 0
                        AlreadyShown = -1
                    End If
                    AlreadyShown = AlreadyShown + 1
                End If
            End If
        Else
            If (Not ExcelUpdateAllowed) Or (ReadOnlyUser) Then
                'Permission denied
                If (AlreadyShown Mod 5) = 0 Then
                    MsExcel.WindowState = xlMinimized
                    Me.SetFocus
                    tmpMsg = "Sorry. You shouldn't change anything."
                    If ReadOnlyUser Then tmpMsg = tmpMsg & vbNewLine & "(You are a 'Read Only' User)"
                    'If AlreadyShown >= 10 Then tmpMsg = tmpMsg & vbNewLine & "Excel will be closed now."
                    If MyMsgBox.CallAskUser(0, 0, 0, "Updating the Worksheet ?", tmpMsg, "stopit", "", UserAnswer) = 1 Then DoNothing
                    SetFormOnTop Me, False
                    MsExcel.WindowState = xlNormal
                End If
                ExcelJustOpened = True
                MsExcel.Undo
                AlreadyShown = AlreadyShown + 1
                ExcelJustOpened = False
            End If
        End If
    End If
End Sub

Private Sub MsExcel_WorkbookBeforeClose(ByVal Wb As Excel.Workbook, Cancel As Boolean)
    If (Not ExcelShutDown) And (Not ExcelJustOpened) Then
        MainTimer(1).Tag = "STOP_EXCEL"
        MainTimer(1).Interval = 500
        MainTimer(1).Enabled = True
    End If
End Sub

Private Sub MsExcel_WorkbookBeforeSave(ByVal Wb As Excel.Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
    Dim tmpMsg As String
    If (Not ExcelShutDown) And (Not ExcelJustOpened) Then
        'Permission denied
        If (ReadOnlyUser) And (Not ExcelRoSaveAllowed) Then
            MsExcel.WindowState = xlMinimized
            Me.SetFocus
            tmpMsg = "Sorry. Access denied."
            tmpMsg = tmpMsg & vbNewLine & "(You are a 'Read Only' User)"
            If MyMsgBox.CallAskUser(0, 0, 0, "Saving the Worksheet ?", tmpMsg, "stop2", "", UserAnswer) = 1 Then DoNothing
            Cancel = True
            SetFormOnTop Me, False
            MsExcel.WindowState = xlNormal
        End If
    End If
End Sub

Private Sub MsWord_Quit()
    'If (Not ExcelShutDown) And (Not ExcelJustOpened) Then
        MainTimer(3).Tag = "STOP_WORD"
        MainTimer(3).Interval = 500
        MainTimer(3).Enabled = True
    'End If
End Sub

Private Sub RightPanel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
'    If Not RightPanelTimer.Enabled Then
'        For i = 0 To chkTabIsVisible.UBound
'            If chkTabIsVisible(i).Visible Then
'                If chkTabIsVisible(i).Appearance = 0 Then
'                    lblTabName(i).ForeColor = vbYellow
'                    lblTabShadow(i).ForeColor = vbBlack
'                Else
'                    lblTabName(i).ForeColor = vbWhite
'                    lblTabShadow(i).ForeColor = vbBlack
'                End If
'            End If
'        Next
'    End If
'    RightPanelTimer.Enabled = False
'    RightPanelTimer.Enabled = True
End Sub

Private Sub RightPanelTimer_Timer()
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
                If chkTabIsVisible(i).Appearance = 0 Then
                    lblTabName(i).ForeColor = vbBlack
                    lblTabShadow(i).ForeColor = vbWhite
                Else
                    lblTabName(i).ForeColor = vbBlack
                    lblTabShadow(i).ForeColor = vbWhite
                End If
        End If
    Next
    RightPanelTimer.Enabled = False
End Sub

Private Sub TabLookUp_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim FldName As String
    Dim SysFldList As String
    FldName = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
    If InStr(FldName, "'") > 0 Then
        SysFldList = AddAftVersFields(Index, "", 0) 'Status Icons
        If InStr(SysFldList, FldName) > 0 Then
            'SyncCursorBusy = True
            SearchInTabList FileData(Index), ColNo, "__", False, "", ""
            'SyncCursorBusy = False
        End If
    End If
End Sub

Private Sub TabLookUp_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim FldName As String
    Dim SysFldList As String
    FldName = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
    If InStr(FldName, "'") > 0 Then
        SysFldList = AddAftVersFields(Index, "", 0) 'Status Icons
        If InStr(SysFldList, FldName) > 0 Then
            SyncCursorBusy = True
            SearchInTabList FileData(Index), ColNo, "__", False, "", ""
            SyncCursorBusy = False
        End If
    ElseIf InStr(ListOfCedaTimeFields, FldName) > 0 Then
    Else
        AdjustTabEditField Index, "LOOK", TabLookUp(Index), LineNo, ColNo
    End If
End Sub

Private Sub AdjustTabEditField(Index As Integer, TabType As String, CurTab As TABLib.Tab, LineNo As Long, ColNo As Long)
    TabEditLookUpIndex = Index
    TabEditColNo = ColNo
    TabEditLineNo = LineNo
    TabEditLookUpType = TabType
    Set TabEditLookUpTabObj = CurTab
    SetTxtTabEditLookUpPosition Index
End Sub

Private Sub SetTxtTabEditLookUpPosition(Index As Integer)
    Dim tmpName As String
    Dim FieldTop As Long
    Dim FieldLeft As Long
    Dim LengthList As String
    Dim LengthItem As String
    Dim CurLength As Long
    Dim BgnCol As Long
    Dim CurCol As Long
    If TabEditLookUpType = "LOOK" Then
        TabLookUp(Index).ResetLineDecorations 0
        TabLookUp(Index).SetDecorationObject 0, TabEditColNo, "BookMark"
    End If
    FieldTop = TabLookUp(Index).Top
    txtTabEditLookUp(Index).Top = FieldTop - 30
    LengthList = TabLookUp(Index).HeaderLengthString
    FieldLeft = TabLookUp(Index).Left
    BgnCol = TabLookUp(Index).GetHScrollPos
    For CurCol = BgnCol To TabEditColNo - 1
        LengthItem = GetRealItem(LengthList, CurCol, ",")
        CurLength = Val(LengthItem) * 15
        FieldLeft = FieldLeft + CurLength
    Next
    txtTabEditLookUp(Index).Left = FieldLeft + 15
    LengthItem = GetRealItem(LengthList, TabEditColNo, ",")
    CurLength = Val(LengthItem) * 15
    If CurLength < 300 Then CurLength = 300
    txtTabEditLookUp(Index).Width = CurLength
    txtTabEditLookUp(Index).Height = TabLookUp(Index).LineHeight * 15
    txtTabEditLookUp(Index).Text = TabLookUp(Index).GetColumnValue(TabEditLineNo, TabEditColNo)
    txtTabEditLookUp(Index).Tag = txtTabEditLookUp(Index).Text
    TabEditIsUpperCase = True
    tmpName = GetRealItem(TabLookUp(Index).LogicalFieldList, TabEditColNo, ",")
    If InStr("USEC,USEU,MEAN,VALU", tmpName) > 0 Then TabEditIsUpperCase = False
    txtTabEditLookUp(Index).FontBold = True
    txtTabEditLookUp(Index).Font = CurrentTabFont
    txtTabEditLookUp(Index).FontSize = FontSlider.Value
    txtTabEditLookUp(Index).ZOrder
    txtTabEditLookUp(Index).Visible = True
    txtTabEditLookUp(Index).SetFocus
    SetTextSelected
End Sub

Private Sub TabLookUp_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Static LastLineNo As Long
    Static LastColno As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim CurField As String
    Dim tmpData As String
    Dim CurItem As Integer
    If LineNo >= 0 Then
        If (LineNo <> LastLineNo) Or (ColNo <> LastColno) Then
            PopUpTimer.Enabled = False
            PopUpPanel.Visible = False
            CurItem = CInt(ColNo) + 1
            CurField = GetItem(FileData(Index).LogicalFieldList, CurItem, ",")
            Select Case CurField
                Case "'LG'"
                Case "'PA'"
                Case Else
            End Select
        End If
    End If
    LastLineNo = LineNo
    LastColno = ColNo
End Sub

Private Sub txtDlyInput_Change(Index As Integer)
    Dim tmpTxt As String
    Dim CheckIt As Boolean
    If chkDlyTick(Index).Value = 1 Then
        tmpTxt = Trim(txtDlyInput(Index).Text)
        CheckIt = CheckTimeValues(tmpTxt, False, False)
        If CheckIt Then
            CheckNewDlysEntry Index, tmpTxt
        Else
            If Len(tmpTxt) >= 4 Then
                txtDlyInput(Index).BackColor = vbRed
                txtDlyInput(Index).ForeColor = vbWhite
            Else
                txtDlyInput(Index).BackColor = vbWhite
                txtDlyInput(Index).ForeColor = vbBlack
            End If
        End If
    End If
End Sub
Private Sub txtDlyInput_KeyPress(Index As Integer, KeyAscii As Integer)
    If Not ((KeyAscii >= 48 And KeyAscii <= 57) Or (KeyAscii = 8) Or (Chr(KeyAscii) = ":")) Then
        KeyAscii = 0
    End If
End Sub
Private Sub CheckNewDlysEntry(Index As Integer, DlytValue As String)
    Dim HitList As String
    Dim tmpFields As String
    Dim NewLine As String
    Dim NewData As String
    Dim tmpDlyr As String
    Dim tmpCapt As String
    Dim tmpMsg As String
    Dim TabIdx As Integer
    Dim MaxLine As Long
    Dim LineStatus As Long
    Dim ColNo As Long
    Dim FlnuValue As Long
    Dim MsgText As String
    FlnuValue = Val(txtAftData(3).Text)
    If FlnuValue > 0 Then
        'tmpDlyr = Right("00" & CStr(Index), 2)
        tmpDlyr = chkDlyCheck(Index).Tag
        TabIdx = Val(DelayPanel.Tag)
        ColNo = Val(TranslateFieldItems("INDX", FileData(TabIdx).LogicalFieldList))
        HitList = FileData(TabIdx).GetLinesByColumnValue(ColNo, tmpDlyr, 0)
        If HitList = "" Then
            If Len(DlytValue) = 5 Then
                'INSERT
                tmpFields = "AREA,TIME,INDX,CONT,USEC,USEU,CDAT,LSTU,FLNU"
                NewData = "B," & DlytValue & "," & tmpDlyr
                NewData = NewData & "," & Left(lblDlyReason(Index).Caption, 10)
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                NewData = NewData & "," & txtAftData(3).Text
                NewLine = CreateEmptyLine(FileData(TabIdx).LogicalFieldList)
                FileData(TabIdx).InsertTextLineAt 0, NewLine, False
                MaxLine = 0
                FileData(TabIdx).SetFieldValues MaxLine, tmpFields, NewData
                FileData(TabIdx).SetLineStatusValue MaxLine, 2
                FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightGreen
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                If AutoSaveAodb Then HandleSaveAodb True
            End If
        Else
            'UPDATE (or DELETE)
            MaxLine = Val(HitList)
            LineStatus = FileData(TabIdx).GetLineStatusValue(MaxLine)
            If Len(DlytValue) > 0 Then
                If Len(DlytValue) = 5 Then
                    tmpFields = "TIME,CONT,USEU,LSTU"
                    NewData = DlytValue
                    NewData = NewData & "," & Left(lblDlyReason(Index).Caption, 10)
                    NewData = NewData & "," & LoginUserName
                    NewData = NewData & "," & GetTimeStamp(-UtcTimeDiff)
                    FileData(TabIdx).SetFieldValues MaxLine, tmpFields, NewData
                    If LineStatus <> 2 Then
                        FileData(TabIdx).SetLineStatusValue MaxLine, 1
                        FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightYellow
                        HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                        If AutoSaveAodb Then HandleSaveAodb True
                    End If
                End If
            Else
                If LineStatus <> 3 Then
                    tmpCapt = HiddenData.GetConfigValues(FileData(TabIdx).myName, "CAPT")
                    tmpCapt = GetRealItem(tmpCapt, 0, "|")
                    tmpMsg = "Do you want to delete this entry?"
                    If MyMsgBox.CallAskUser(0, 0, 0, tmpCapt, tmpMsg, "stop2", "Yes,No", UserAnswer) = 1 Then
                        If LineStatus <> 2 Then
                            FileData(TabIdx).SetLineStatusValue MaxLine, 3
                            FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightRed
                            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                            If AutoSaveAodb Then HandleSaveAodb True
                        Else
                            FileData(TabIdx).DeleteLine MaxLine
                        End If
                    End If
                End If
            End If
        End If
        FileData(TabIdx).AutoSizeColumns
        FileData(TabIdx).Refresh
    Else
        MsgText = "Sorry, the Departure Flight doesn't exist."
        If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "stop2", "", UserAnswer) = 1 Then DoNothing
    End If
End Sub

Private Sub txtDlyInput_GotFocus(Index As Integer)
    If chkDlyTick(Index).Value = 1 Then SetTextSelected
End Sub

Public Sub EvaluateBc(BcCmd As String, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim tmpUrno As String
    Dim tmpFlnu As String
    If FipsIsConnected Then
        tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
        If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(BcSel)
        If tmpUrno <> "" Then
            If InStr("AFTTAB", BcTbl) > 0 Then
                If tmpUrno = txtAftData(1).Text Then
                    ShowArrRecord BcDat, BcFld
                End If
                If tmpUrno = txtAftData(3).Text Then
                    ShowDepRecord BcDat, BcFld
                End If
            End If
            If InStr("DLYTAB,SRLTAB,FMLTAB", BcTbl) > 0 Then
                tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
                Select Case BcCmd
                    Case "IRT"
                        If (tmpFlnu = txtAftData(1).Text) Or (tmpFlnu = txtAftData(3).Text) Then
                            HandleBcInsert -1, BcTbl, BcSel, BcFld, BcDat
                        End If
                    Case "URT"
                        If (tmpFlnu = txtAftData(1).Text) Or (tmpFlnu = txtAftData(3).Text) Then
                            HandleBcUpdate -1, BcTbl, BcSel, BcFld, BcDat
                        End If
                    Case "DRT"
                        HandleBcDelete -1, BcTbl, BcSel, BcFld, BcDat
                    Case "REFR"
                        HandleBcRefresh -1, BcTbl, BcSel, BcFld, BcDat
                    Case Else
                End Select
                If EditMaskIsOpen Then
                    If tmpUrno = EditMask.Tag Then
                        EditMask.UpdateFieldValues BcFld, BcDat
                    End If
                End If
                If ReadMaskIsOpen Then
                    If tmpUrno = ReadMask.Tag Then
                        ReadMaskTab.SetCurrentSelection ReadMaskTab.GetCurrentSelected
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub HandleBcRefresh(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim CurFocusTab As Integer
    Dim tmpTbl As String
    Dim tmpArrFlnu As String
    Dim tmpDepFlnu As String
    Dim tmpArea As String
    Dim tmpMyCall As String
    Dim tmpBcCall As String
    Dim tmpMyMask As String
    Dim tmpBcMask As String
    Dim tmpMyWks As String
    Dim tmpBcWks As String
    Dim tmpMyPid As String
    Dim tmpBcPid As String
    Dim LineNo As Long
    Dim DoIt As Boolean
    tmpArrFlnu = GetRealItem(BcDat, 0, ",")
    tmpDepFlnu = GetRealItem(BcDat, 1, ",")
    If (tmpArrFlnu = txtAftData(1).Text) Or (tmpDepFlnu = txtAftData(3).Text) Then
        If TabIdx < 0 Then
            i1 = 0
            i2 = chkTabIsVisible.UBound
        Else
            i1 = TabIdx
            i2 = TabIdx
        End If
        For i = i1 To i2
            If chkTabIsVisible(i).Visible Then
                tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
                If tmpTbl = BcTbl Then
                    DoIt = True
                    tmpMyWks = UfisServer.CdrhdlSock.LocalHostName
                    tmpBcWks = GetRealItem(BcSel, 0, ",")
                    tmpMyPid = CStr(App.ThreadID)
                    tmpBcPid = GetRealItem(BcSel, 1, ",")
                    tmpMyMask = HiddenData.GetConfigValues(FileData(i).myName, "MASK")
                    tmpMyMask = GetRealItem(tmpMyMask, 1, ",")
                    tmpBcMask = GetRealItem(BcSel, 2, ",")
                    tmpMyCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                    tmpBcCall = GetRealItem(BcSel, 3, ",")
                    If (tmpMyWks = tmpBcWks) And (tmpMyPid = tmpBcPid) Then DoIt = False
                    If tmpMyCall <> tmpBcCall Then DoIt = False
                    If tmpMyMask <> tmpBcMask Then DoIt = False
                    If DoIt Then
                        RefreshOnBc = True
                        CurFocusTab = LastFocusIndex
                        LastFocusIndex = i
                        PushWorkButton "READAODB", 1
                        LastFocusIndex = CurFocusTab
                        RefreshOnBc = False
                    End If
                End If
            End If
        Next
    End If
End Sub

Private Sub HandleBcInsert(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpFlnu As String
    Dim tmpArea As String
    Dim tmpCall As String
    Dim tmpData As String
    Dim UrnoLine As Long
    Dim LineNo As Long
    Dim DoIt As Boolean
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    For i = i1 To i2
        If chkTabIsVisible(i).Visible Then
            tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
            If tmpTbl = BcTbl Then
                UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                DoIt = True
                Select Case BcTbl
                    Case "DLYTAB"
                        tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                        If i = Val(DelayPanel.Tag) Then
                            If tmpArea <> "B" Then DoIt = False
                        Else
                            If tmpArea <> "A" Then DoIt = False
                        End If
                    Case "FMLTAB"
                        tmpCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                        tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
                        If tmpCall = "ARR" And tmpFlnu <> txtAftData(1).Text Then DoIt = False
                        If tmpCall = "DEP" And tmpFlnu <> txtAftData(3).Text Then DoIt = False
                    Case Else
                End Select
                If DoIt Then
                    If UrnoLine < 0 Then
                        LineNo = FileData(i).GetLineCount
                        FileData(i).InsertTextLine CreateEmptyLine(FileData(i).LogicalFieldList), False
                        FileData(i).SetFieldValues LineNo, BcFld, BcDat
                        tmpData = FileData(i).GetLineValues(LineNo)
                        FileData(i).SetLineTag LineNo, tmpData
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then InitDelayPanel i, LineNo, ""
                        End If
                    Else
                        HandleBcUpdate i, BcTbl, BcSel, BcFld, BcDat
                    End If
                End If
            End If
        End If
    Next
End Sub
Private Sub HandleBcUpdate(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpFlnu As String
    Dim tmpArea As String
    Dim tmpCall As String
    Dim tmpIndx As String
    Dim tmpData As String
    Dim UrnoLine As Long
    Dim DoIt As Boolean
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    For i = i1 To i2
        If chkTabIsVisible(i).Visible Then
            tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
            If tmpTbl = BcTbl Then
                UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                DoIt = True
                Select Case BcTbl
                    Case "DLYTAB"
                        tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                        If i = Val(DelayPanel.Tag) Then
                            If tmpArea <> "B" Then DoIt = False
                        Else
                            If tmpArea <> "A" Then DoIt = False
                        End If
                    'Case "FMLTAB"
                    '    tmpCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                    '    tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
                    '    If tmpCall = "ARR" And tmpFlnu <> txtAftData(1).Text Then DoIt = False
                    '    If tmpCall = "DEP" And tmpFlnu <> txtAftData(3).Text Then DoIt = False
                    Case Else
                End Select
                If DoIt Then
                    If UrnoLine >= 0 Then
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then InitDelayPanel i, UrnoLine, ":"
                        End If
                        FileData(i).SetFieldValues UrnoLine, BcFld, BcDat
                        tmpData = FileData(i).GetLineValues(UrnoLine)
                        FileData(i).SetLineTag UrnoLine, tmpData
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then
                                InitDelayPanel i, UrnoLine, ""
                                tmpIndx = FileData(i).GetFieldValue(UrnoLine, "INDX")
                                CheckDlyIndex i, UrnoLine, tmpIndx, True
                            End If
                        End If
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                    End If
                End If
            End If
        End If
    Next
End Sub
Private Sub HandleBcDelete(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpArea As String
    Dim UrnoLine As Long
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(BcSel)
    If tmpUrno <> "" Then
        For i = i1 To i2
            If chkTabIsVisible(i).Visible Then
                tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
                If tmpTbl = BcTbl Then
                    UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                    If UrnoLine >= 0 Then
                        If i = Val(DelayPanel.Tag) Then
                            tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                            If (DelayPanel.Visible) And (tmpArea = "B") Then InitDelayPanel i, UrnoLine, ":"
                        End If
                        FileData(i).DeleteLine UrnoLine
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                    End If
                End If
            End If
        Next
    End If
End Sub
Private Function GetUrnoLine(CurTab As TABLib.Tab, CurUrno As String) As Long
    Dim UrnoLine As Long
    Dim UrnoCol As Long
    Dim tmpStrg As String
    Dim HitList As String
    UrnoLine = -1
    tmpStrg = TranslateFieldItems("URNO", CurTab.LogicalFieldList)
    If tmpStrg <> "" Then
        UrnoCol = Val(tmpStrg)
        HitList = CurTab.GetLinesByColumnValue(UrnoCol, CurUrno, 0)
        If HitList <> "" Then
            UrnoLine = Val(HitList)
        End If
    End If
    GetUrnoLine = UrnoLine
End Function
Private Function GetFirstEditColNo(Index As Integer) As Long
    Dim CurCol As Long
    Dim FirstCol As Long
    Dim tmpList As String
    Dim tmpLenList As String
    Dim tmpColNbr As String
    Dim tmpColLen As String
    FirstCol = -1
    tmpLenList = FileData(Index).HeaderLengthString
    tmpList = "," & FileData(Index).NoFocusColumns & ","
    CurCol = 0
    tmpColLen = GetRealItem(tmpLenList, CurCol, ",")
    While ((FirstCol < 0) And (tmpColLen <> ""))
        If Val(tmpColLen) > 0 Then
            tmpColNbr = "," & CStr(CurCol) & ","
            If InStr(tmpList, tmpColNbr) = 0 Then FirstCol = CurCol
        End If
        CurCol = CurCol + 1
        tmpColLen = GetRealItem(tmpLenList, CurCol, ",")
    Wend
    GetFirstEditColNo = FirstCol
End Function
Private Sub CheckReadOnlyMode()
    Dim i As Integer
    Dim SetEnable As Boolean
    Dim tmpTag As String
    If ReadOnlyUser = True Then
        For i = 0 To chkWork.UBound
            If chkWork(i).Visible Then
                tmpTag = chkWork(i).Tag
                tmpTag = GetRealItem(tmpTag, 0, ",")
                SetEnable = False
                Select Case tmpTag
                    Case "TAB_READ"
                    Case "TAB_UPDATE", "TAB_EDIT_UPDATE"
                    Case "TAB_INSERT", "TAB_EDIT_INSERT"
                    Case "TAB_DELETE"
                    Case "SAVEAODB", "AUTO_SAVEAODB"
                    Case Else
                        SetEnable = True
                End Select
                chkWork(i).Enabled = SetEnable
            End If
        Next
    End If
End Sub

Private Sub CheckSynchroTabs(Index As Integer, RowSelChg As Boolean)
    Dim SyncRule As String
    Dim SyncStep As String
    Dim srcRule As String
    Dim srcTabKey As String
    Dim srcTabName As String
    Dim srcFldNam As String
    Dim srcFldVal As String
    Dim srcTabIdx As String
    Dim tgtList As String
    Dim tgtRule As String
    Dim tgtTabName As String
    Dim tgtTabKey As String
    Dim tgtTabidx As String
    Dim tgtFldNam As String
    Dim selFldNam As String
    Dim srcTabAdid As String
    Dim srcTabFields As String
    Dim srcTabRecord As String
    Dim tmpAdidList As String
    Dim RotArrIndx As String
    Dim RotDepIndx As String
    Dim stpItem As Integer
    Dim tgtItem As Integer
    Dim srcIndex As Integer
    Dim tgtIndex As Integer
    Dim LineNo As Long
    Dim SelLineNo As Long
    Dim FirstChecked As Boolean
    
    srcTabName = HiddenData.GetConfigValues(FileData(Index).myName, "NAME")
    srcTabKey = srcTabName & "_REFERENCE"
    SyncRule = HiddenData.GetConfigValues(srcTabKey, "SYNC")
    SyncRule = GetItem(SyncRule, 1, "{")
    If SyncRule <> "" Then
        FirstChecked = False
        tmpAdidList = ""
        stpItem = 0
        SyncStep = "START"
        While SyncStep <> ""
            stpItem = stpItem + 1
            SyncStep = GetItem(SyncRule, stpItem, "|")
            If SyncStep <> "" Then
                srcRule = GetItem(SyncStep, 1, ":")
                srcTabName = GetItem(srcRule, 1, ".")
                srcFldNam = GetItem(srcRule, 2, ".")
                srcTabKey = srcTabName & "_REFERENCE"
                srcTabIdx = HiddenData.GetConfigValues(srcTabKey, "FIDX")
                If srcTabIdx <> "" Then
                    srcIndex = Val(srcTabIdx)
                    If srcFldNam <> "FP" Then
                        srcFldVal = "----"
                        LineNo = FileData(srcIndex).GetCurrentSelected
                        If LineNo >= 0 Then srcFldVal = FileData(srcIndex).GetFieldValue(LineNo, srcFldNam)
                        If Not FirstChecked Then
                            If (srcFldVal <> lblTabPos(srcIndex).Tag) Or (RowSelChg = False) Then
                                selFldNam = GetItem(FileData(srcIndex).LogicalFieldList, 1, ",")
                                selFldNam = GetTabProperty(srcIndex, "MARK_FRAME1", 1, selFldNam)
                                If chkSelDeco(0).Value = 1 Then
                                    MarkLookupLines FileData(srcIndex), srcFldNam, srcFldVal, True, selFldNam, False, "'S1'", "DecoMarkerLB"
                                Else
                                    MarkLookupLines FileData(srcIndex), srcFldNam, srcFldVal, False, selFldNam, False, "'S1'", "DecoMarkerLB"
                                End If
                                lblTabPos(srcIndex).Tag = srcFldVal
                                If FileData(srcIndex).GetCurrentSelected < 0 Then
                                    chkTabCoverIsVisible(srcIndex).Value = 1
                                    If chkTabIsVisible(srcIndex).Value = 1 Then
                                        If AutoHideSynchroTabs = True Then TabCover(srcIndex).Visible = True
                                    End If
                                Else
                                    chkTabCoverIsVisible(srcIndex).Value = 0
                                    TabCover(srcIndex).Visible = False
                                End If
                            End If
                            FirstChecked = True
                        End If
                        tgtList = GetItem(SyncStep, 2, ":")
                        If tgtList <> "" Then
                            tgtItem = 0
                            tgtRule = "START"
                            While tgtRule <> ""
                                tgtItem = tgtItem + 1
                                tgtRule = GetItem(tgtList, tgtItem, ",")
                                If tgtRule <> "" Then
                                    tgtTabName = GetItem(tgtRule, 1, ".")
                                    tgtFldNam = GetItem(tgtRule, 2, ".")
                                    tgtTabKey = tgtTabName & "_REFERENCE"
                                    tgtTabidx = HiddenData.GetConfigValues(tgtTabKey, "FIDX")
                                    If tgtTabidx <> "" Then
                                        tgtIndex = Val(tgtTabidx)
                                        If (srcFldVal <> lblTabPos(tgtIndex).Tag) Or (RowSelChg = False) Then
                                            selFldNam = GetItem(FileData(tgtIndex).LogicalFieldList, 1, ",")
                                            selFldNam = GetTabProperty(tgtIndex, "MARK_FRAME1", 1, selFldNam)
                                            If chkSelDeco(0).Value = 1 Then
                                                MarkLookupLines FileData(tgtIndex), tgtFldNam, srcFldVal, True, selFldNam, True, "'S1'", "DecoMarkerLB"
                                            Else
                                                MarkLookupLines FileData(tgtIndex), tgtFldNam, srcFldVal, False, selFldNam, True, "'S1'", "DecoMarkerLB"
                                            End If
                                            If FileData(tgtIndex).GetCurrentSelected < 0 Then
                                                If chkTabIsVisible(tgtIndex).Value = 1 Then
                                                    chkTabCoverIsVisible(tgtIndex).Value = 1
                                                    If AutoHideSynchroTabs = True Then TabCover(tgtIndex).Visible = True
                                                End If
                                            Else
                                                chkTabCoverIsVisible(tgtIndex).Value = 0
                                                TabCover(tgtIndex).Visible = False
                                            End If
                                            lblTabPos(tgtIndex).Tag = srcFldVal
                                            srcTabAdid = ""
                                            If srcTabName = chkSelFlight(0).Tag Then srcTabAdid = "A"
                                            If srcTabName = chkSelFlight(1).Tag Then srcTabAdid = "D"
                                            If InStr(tmpAdidList, srcTabAdid) > 0 Then srcTabAdid = ""
                                            If srcTabAdid <> "" Then
                                                srcTabFields = FileData(srcIndex).LogicalFieldList
                                                LineNo = FileData(srcIndex).GetCurrentSelected
                                                If LineNo >= 0 Then
                                                    srcTabRecord = FileData(srcIndex).GetLineValues(LineNo)
                                                Else
                                                    srcTabRecord = CreateEmptyLine(srcTabFields)
                                                End If
                                                InitFlightPanel True, srcIndex, srcTabAdid, srcTabFields, srcTabRecord
                                                tmpAdidList = tmpAdidList & srcTabAdid
                                            End If
                                        End If
                                    End If
                                End If
                            Wend
                        End If
                    End If
                End If
            End If
        Wend
    End If
    CheckTabAutoMarker Index
    If (RotationActivated = True) And (RowSelChg = True) Then
        RotArrIndx = ""
        LineNo = TabArrFlightsTab.GetCurrentSelected
        If LineNo >= 0 Then RotArrIndx = TabArrFlightsTab.GetFieldValue(LineNo, "'C4'")
        RotDepIndx = ""
        LineNo = TabDepFlightsTab.GetCurrentSelected
        If LineNo >= 0 Then RotDepIndx = TabDepFlightsTab.GetFieldValue(LineNo, "'C4'")
        FocusSyncInternal = True
        If RotArrIndx = "" Then
            RotArrIndx = RotDepIndx
            chkSelFlight(1).Value = 1
        Else
            chkSelFlight(0).Value = 1
        End If
        FocusSyncInternal = False
        If DontSyncRota = False Then
            MarkLookupLines RotationData, "RIDX", RotArrIndx, True, "DRGN,AFLT,DFLT", True, "'S1'", "DecoMarkerLB"
        Else
            MarkLookupLines RotationData, "RIDX", RotArrIndx, True, "DRGN,AFLT,DFLT", False, "'S1'", "DecoMarkerLB"
        End If
    End If
    'CheckSynchroCursors Index
End Sub
Private Sub ShowTabCover(Index As Integer, ShowPanel As Boolean)
    If ShowPanel = True Then
        If AutoHideSynchroTabs = True Then TabCover(Index).Visible = True
    Else
        TabCover(Index).Visible = False
    End If
End Sub
Private Function GetTabProperty(Index As Integer, Keyword As String, GetCol As Long, RetDef As String) As String
    Dim Result As String
    Dim tmpHit As String
    Dim LineNo As Long
    Result = ""
    LineNo = -1
    tmpHit = TabPropBag(Index).GetLinesByColumnValue(0, Keyword, 0)
    If tmpHit <> "" Then
        LineNo = Val(tmpHit)
        Result = TabPropBag(Index).GetColumnValue(LineNo, GetCol)
        Result = Trim(Result)
    End If
    If Result = "" Then Result = RetDef
    GetTabProperty = Result
End Function
Private Function InitTabProperty(Index As Integer, SectionKey As String, Keyword As String, SetDef As String) As String
    Dim tmpStr As String
    Dim tmpProp As String
    tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_" & Keyword, SetDef)
    If tmpStr <> "" Then
        tmpProp = Keyword & Chr(15) & tmpStr & Chr(15) & " "
        TabPropBag(Index).InsertTextLine tmpProp, False
    End If
    InitTabProperty = tmpStr
End Function
Private Function SetTabProperty(Index As Integer, Keyword As String, PropItems As String, PropValues As String) As Long
    Dim tmpProp As String
    Dim tmpHit As String
    Dim LineNo As Long
    LineNo = -1
    tmpHit = TabPropBag(Index).GetLinesByColumnValue(0, Keyword, 0)
    If tmpHit <> "" Then
        LineNo = Val(tmpHit)
        TabPropBag(Index).DeleteLine LineNo
    End If
    tmpProp = Keyword & Chr(15) & PropItems & Chr(15) & PropValues
    TabPropBag(Index).InsertTextLine tmpProp, False
    LineNo = TabPropBag(Index).GetLineCount - 1
    SetTabProperty = LineNo
End Function
Private Sub CheckSynchroCursors(Index As Integer)
    Dim SyncRule As String
    Dim SyncStep As String
    Dim srcRule As String
    Dim srcTabKey As String
    Dim srcTabName As String
    Dim srcFldNam As String
    Dim srcFldVal As String
    Dim srcTabIdx As String
    Dim tgtList As String
    Dim tgtRule As String
    Dim tgtTabName As String
    Dim tgtTabKey As String
    Dim tgtTabidx As String
    Dim tgtFldNam As String
    Dim srcTabAdid As String
    Dim srcTabFields As String
    Dim srcTabRecord As String
    Dim tmpAdidList As String
    Dim stpItem As Integer
    Dim tgtItem As Integer
    Dim srcIndex As Integer
    Dim tgtIndex As Integer
    Dim LineNo As Long
    Dim FirstChecked As Boolean
    
    srcTabName = HiddenData.GetConfigValues(FileData(Index).myName, "NAME")
    srcTabKey = srcTabName & "_REFERENCE"
    SyncRule = HiddenData.GetConfigValues(srcTabKey, "SYNC")
    SyncRule = GetItem(SyncRule, 3, "{")
    If SyncRule <> "" Then
        FirstChecked = False
        tmpAdidList = ""
        stpItem = 0
        SyncStep = "START"
        While SyncStep <> ""
            stpItem = stpItem + 1
            SyncStep = GetItem(SyncRule, stpItem, "|")
            If SyncStep <> "" Then
                srcRule = GetItem(SyncStep, 1, ":")
                srcTabName = GetItem(srcRule, 1, ".")
                srcFldNam = GetItem(srcRule, 2, ".")
                srcTabKey = srcTabName & "_REFERENCE"
                srcTabIdx = HiddenData.GetConfigValues(srcTabKey, "FIDX")
                If srcTabIdx <> "" Then
                    srcIndex = Val(srcTabIdx)
                    srcFldVal = "----"
                    LineNo = FileData(srcIndex).GetCurrentSelected
                    If (LineNo >= 0) And (chkSelDeco(1).Value = 1) Then srcFldVal = FileData(srcIndex).GetFieldValue(LineNo, srcFldNam)
                    If Not FirstChecked Then
                        If srcFldVal <> lblTabCnt(srcIndex).Tag Then
                            MarkLookupLines FileData(srcIndex), srcFldNam, srcFldVal, True, srcFldNam, False, "'S2'", "DecoMarkerLR"
                            lblTabCnt(srcIndex).Tag = srcFldVal
                        End If
                        FirstChecked = True
                    End If
                    tgtList = GetItem(SyncStep, 2, ":")
                    If tgtList <> "" Then
                        tgtItem = 0
                        tgtRule = "START"
                        While tgtRule <> ""
                            tgtItem = tgtItem + 1
                            tgtRule = GetItem(tgtList, tgtItem, ",")
                            If tgtRule <> "" Then
                                tgtTabName = GetItem(tgtRule, 1, ".")
                                tgtFldNam = GetItem(tgtRule, 2, ".")
                                tgtTabKey = tgtTabName & "_REFERENCE"
                                tgtTabidx = HiddenData.GetConfigValues(tgtTabKey, "FIDX")
                                If tgtTabidx <> "" Then
                                    tgtIndex = Val(tgtTabidx)
                                    'If chkTabIsVisible(tgtIndex).Value = 1 Then
                                        If srcFldVal <> lblTabCnt(tgtIndex).Tag Then
                                            MarkLookupLines FileData(tgtIndex), tgtFldNam, srcFldVal, True, tgtFldNam, False, "'S2'", "DecoMarkerLR"
                                            lblTabCnt(tgtIndex).Tag = srcFldVal
                                        End If
                                    'End If
                                End If
                            End If
                        Wend
                    End If
                End If
            End If
        Wend
    End If
End Sub

Private Sub CheckTabAutoMarker(Index As Integer)
    Dim SyncRule As String
    Dim SyncStep As String
    Dim srcRule As String
    Dim srcTabKey As String
    Dim srcTabName As String
    Dim srcFldNam As String
    Dim srcFldVal As String
    Dim srcTabIdx As String
    Dim tgtList As String
    Dim tgtRule As String
    Dim tgtTabName As String
    Dim tgtTabKey As String
    Dim tgtTabidx As String
    Dim tgtFldNam As String
    Dim srcTabAdid As String
    Dim srcTabFields As String
    Dim srcTabRecord As String
    Dim tmpAdidList As String
    Dim stpItem As Integer
    Dim tgtItem As Integer
    Dim srcIndex As Integer
    Dim tgtIndex As Integer
    Dim LineNo As Long
    Dim FirstChecked As Boolean
    
    srcTabName = HiddenData.GetConfigValues(FileData(Index).myName, "NAME")
    srcTabKey = srcTabName & "_REFERENCE"
    SyncRule = HiddenData.GetConfigValues(srcTabKey, "SYNC")
    SyncRule = GetItem(SyncRule, 2, "{")
    If SyncRule <> "" Then
        FirstChecked = False
        tmpAdidList = ""
        stpItem = 0
        SyncStep = "START"
        While SyncStep <> ""
            stpItem = stpItem + 1
            SyncStep = GetItem(SyncRule, stpItem, "|")
            If SyncStep <> "" Then
                srcRule = GetItem(SyncStep, 1, ":")
                srcTabName = GetItem(srcRule, 1, ".")
                srcFldNam = GetItem(srcRule, 2, ".")
                srcTabKey = srcTabName & "_REFERENCE"
                srcTabIdx = HiddenData.GetConfigValues(srcTabKey, "FIDX")
                If srcTabIdx <> "" Then
                    srcIndex = Val(srcTabIdx)
                    If srcFldNam <> "FP" Then
                        srcFldVal = "----"
                        LineNo = FileData(srcIndex).GetCurrentSelected
                        If (LineNo >= 0) And (chkSelDeco(1).Value = 1) Then srcFldVal = FileData(srcIndex).GetFieldValue(LineNo, srcFldNam)
                        If Not FirstChecked Then
                            If srcFldVal <> lblTabCnt(srcIndex).Tag Then
                                MarkLookupLines FileData(srcIndex), srcFldNam, srcFldVal, True, srcFldNam, False, "'S2'", "DecoMarkerLR"
                                lblTabCnt(srcIndex).Tag = srcFldVal
                            End If
                            FirstChecked = True
                        End If
                        tgtList = GetItem(SyncStep, 2, ":")
                        If tgtList <> "" Then
                            tgtItem = 0
                            tgtRule = "START"
                            While tgtRule <> ""
                                tgtItem = tgtItem + 1
                                tgtRule = GetItem(tgtList, tgtItem, ",")
                                If tgtRule <> "" Then
                                    tgtTabName = GetItem(tgtRule, 1, ".")
                                    tgtFldNam = GetItem(tgtRule, 2, ".")
                                    tgtTabKey = tgtTabName & "_REFERENCE"
                                    tgtTabidx = HiddenData.GetConfigValues(tgtTabKey, "FIDX")
                                    If tgtTabidx <> "" Then
                                        tgtIndex = Val(tgtTabidx)
                                        'If chkTabIsVisible(tgtIndex).Value = 1 Then
                                            If srcFldVal <> lblTabCnt(tgtIndex).Tag Then
                                                MarkLookupLines FileData(tgtIndex), tgtFldNam, srcFldVal, True, tgtFldNam, False, "'S2'", "DecoMarkerLR"
                                                lblTabCnt(tgtIndex).Tag = srcFldVal
                                            End If
                                        'End If
                                    End If
                                End If
                            Wend
                        End If
                    End If
                End If
            End If
        Wend
    End If
End Sub


Private Sub CheckAutoSortConfig(Index As Integer, SortFields As String, SortItem As String)
    Dim TabName As String
    Dim TabKey As String
    Dim SortList As String
    Dim tmpFldNam As String
    Dim useItem As Integer
    Dim itm As Integer
    Dim FldColNo As Integer
    Dim i As Integer
    Dim LineNo As Long
    Screen.MousePointer = 11
    useItem = Val(SortItem)
    If useItem <= 0 Then useItem = 1
    For i = 0 To chkTabIsVisible.UBound
        If (i = Index) Or (Index < 0) Then
            If chkTabIsVisible(i).Visible Then
                lblTabPos(i).Tag = ""
                TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
                TabKey = TabName & "_REFERENCE"
                SortList = ""
                If Index >= 0 Then SortList = SortFields
                If SortList = "" Then SortList = HiddenData.GetConfigValues(TabKey, "SORT")
                SortList = GetItem(SortList, useItem, "|")
                If SortList <> "" Then
                    FileData(i).ColSelectionRemoveAll
                    Me.Refresh
                    itm = 0
                    tmpFldNam = "START"
                    While tmpFldNam <> ""
                        itm = itm + 1
                        tmpFldNam = GetItem(SortList, itm, ",")
                        If tmpFldNam <> "" Then
                            FldColNo = GetRealItemNo(FileData(i).LogicalFieldList, tmpFldNam)
                            If FldColNo >= 0 Then FileData(i).Sort CStr(FldColNo), True, True
                        End If
                    Wend
                    If FldColNo >= 0 Then FileData(i).ColSelectionAdd FldColNo
                    FileData(i).AutoSizeColumns
                    TabLookUp(i).HeaderLengthString = FileData(i).HeaderLengthString
                    imgCurPos_Click i
                End If
            End If
        End If
    Next
    If Index >= 0 Then
        LineNo = FileData(Index).GetCurrentSelected
        FileData(Index).SetCurrentSelection LineNo
    Else
        If LastFocusIndex >= 0 Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            FileData(LastFocusIndex).SetCurrentSelection LineNo
        End If
    End If
    CheckAlternateColors Index
    Screen.MousePointer = 0
End Sub

Private Sub CreateSynchroTabIndexes(Index As Integer)
    Dim TabKey As String
    Dim TabName As String
    Dim SyncRule As String
    Dim SyncStep As String
    Dim tgtList As String
    Dim tgtRule As String
    Dim tgtTabName As String
    Dim tgtTabKey As String
    Dim tgtTabidx As String
    Dim tgtFldNam As String
    Dim srcTabName As String
    Dim srcTabSync As String
    Dim srcTabAdid As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim tmpFldRel As String
    Dim usedIndexes As String
    Dim tmpChk As String
    Dim stpItem As Integer
    Dim tgtItem As Integer
    Dim syncIndex As Integer
    Dim FldColNo As Long
    Dim i As Integer
    usedIndexes = ":"
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
            TabKey = TabName & "_REFERENCE"
            SyncRule = HiddenData.GetConfigValues(TabKey, "SYNC")
            SyncCursorBusy = True
            tmpChk = GetItem(SyncRule, 1, "{")
            If tmpChk <> "" Then
                chkSelDeco(0).Enabled = True
                chkSelDeco(0).Value = 1
            End If
            tmpChk = GetItem(SyncRule, 2, "{")
            If tmpChk <> "" Then
                chkSelDeco(1).Enabled = True
                chkSelDeco(1).Value = 1
            End If
            SyncCursorBusy = False
            SyncRule = Replace(SyncRule, "{", "|", 1, -1, vbBinaryCompare)
            
            If SyncRule <> "" Then
                stpItem = 0
                SyncStep = "START"
                While SyncStep <> ""
                    stpItem = stpItem + 1
                    SyncStep = GetItem(SyncRule, stpItem, "|")
                    If SyncStep <> "" Then
                        tgtList = GetItem(SyncStep, 2, ":")
                        If tgtList <> "" Then
                            tgtItem = 0
                            tgtRule = "START"
                            While tgtRule <> ""
                                tgtItem = tgtItem + 1
                                tgtRule = GetItem(tgtList, tgtItem, ",")
                                If tgtRule <> "" Then
                                    If InStr(usedIndexes, tgtRule) = 0 Then
                                        usedIndexes = usedIndexes & tgtRule & ":"
                                        tgtTabName = GetItem(tgtRule, 1, ".")
                                        tgtFldNam = GetItem(tgtRule, 2, ".")
                                        tgtTabKey = tgtTabName & "_REFERENCE"
                                        tgtTabidx = HiddenData.GetConfigValues(tgtTabKey, "FIDX")
                                        If tgtTabidx <> "" Then
                                            syncIndex = Val(tgtTabidx)
                                            FldColNo = CLng(GetRealItemNo(FileData(syncIndex).LogicalFieldList, tgtFldNam))
                                            If FldColNo >= 0 Then
                                                StatusBar.Panels(2).Text = "Creating internal index on " & tgtRule & " ..."
                                                FileData(syncIndex).IndexDestroy tgtFldNam
                                                FileData(syncIndex).IndexCreate tgtFldNam, FldColNo
                                                StatusBar.Panels(2).Text = ""
                                            End If
                                        End If
                                    End If
                                End If
                            Wend
                        Else
                            srcTabName = GetItem(SyncStep, 1, ".")
                            srcTabSync = GetItem(SyncStep, 2, ".")
                            srcTabAdid = GetItem(SyncStep, 3, ".")
                            If srcTabSync = "FP" Then
                                Select Case srcTabAdid
                                    Case "A"
                                        chkSelFlight(0).Tag = srcTabName
                                    Case "D"
                                        chkSelFlight(1).Tag = srcTabName
                                End Select
                            End If
                        End If
                    End If
                Wend
            End If
        End If
    Next
End Sub

Private Sub CheckDtabGroup(DtabFullList As String)
    Dim DtabList As String
    Dim srcCmd As String
    Dim srcTabIdx As String
    Dim tgtTabName As String
    Dim tgtTabKey As String
    Dim tgtTabidx As String
    Dim SrcIdx As Integer
    Dim TgtIdx As Integer
    Dim tgtVal As Integer
    Dim itm As Integer
    srcCmd = GetItem(DtabFullList, 1, ",")
    If srcCmd = "CHECK_DTAB" Then
        srcTabIdx = GetItem(DtabFullList, 2, ",")
        SrcIdx = Val(srcTabIdx)
        DtabList = GetItem(DtabFullList, 2, ":")
        itm = 0
        tgtTabName = "START"
        While tgtTabName <> ""
            itm = itm + 1
            tgtTabName = GetItem(DtabList, itm, ",")
            If tgtTabName <> "" Then
                tgtTabKey = tgtTabName & "_REFERENCE"
                tgtTabidx = HiddenData.GetConfigValues(tgtTabKey, "FIDX")
                If tgtTabidx <> "" Then
                    TgtIdx = Val(tgtTabidx)
                    tgtVal = chkTabIsVisible(SrcIdx).Value
                    If tgtVal = 1 Then
                        tgtVal = Val(lblTabShadow(TgtIdx).Tag)
                    Else
                        lblTabShadow(TgtIdx).Tag = CStr(chkTabIsVisible(TgtIdx).Value)
                    End If
                    chkTabIsVisible(TgtIdx).Value = tgtVal
                End If
            End If
        Wend
    End If
End Sub

Private Sub CheckAlternateColors(Index As Integer)
    Dim TabName As String
    Dim TabKey As String
    Dim ColorRule As String
    Dim KeyFields As String
    Dim KeyColors As String
    Dim ColorCode As String
    Dim UseColor1 As Long
    Dim UseColor2 As Long
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If (i = Index) Or (Index < 0) Then
            If chkTabIsVisible(i).Visible Then
                TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
                TabKey = TabName & "_REFERENCE"
                ColorRule = HiddenData.GetConfigValues(TabKey, "DFLD")
                If ColorRule <> "" Then
                    ColorRule = Replace(ColorRule, "|", ":", 1, -1, vbBinaryCompare)
                    KeyFields = GetItem(ColorRule, 1, ":")
                    KeyColors = GetItem(ColorRule, 2, ":")
                    ColorCode = GetItem(KeyColors, 1, ",")
                    UseColor1 = TranslateColorCode(ColorCode, vbWhite)
                    ColorCode = GetItem(KeyColors, 2, ",")
                    UseColor2 = TranslateColorCode(ColorCode, vbWhite)
                    TabAlternateColors FileData(i), KeyFields, UseColor1, UseColor2
                End If
            End If
        End If
    Next
End Sub

Private Sub CheckDataSourceConfig()
    Dim TabName As String
    Dim TabKey As String
    Dim DsrcRule As String
    Dim DsrcName As String
    Dim DsrcMeth As String
    Dim DsrcFidx As String
    Dim DsrcList As String
    Dim SrcIdx As Integer
    Dim i As Integer
    'Step 1: Look for Logging Split definitions
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
            TabKey = TabName & "_REFERENCE"
            DsrcRule = HiddenData.GetConfigValues(TabKey, "DSRC")
            If DsrcRule <> "" Then
                DsrcName = GetItem(DsrcRule, 1, ":")
                DsrcMeth = GetItem(DsrcRule, 2, ":")
                DsrcList = GetItem(DsrcRule, 3, ":")
                TabKey = DsrcName & "_REFERENCE"
                DsrcFidx = HiddenData.GetConfigValues(TabKey, "FIDX")
                If DsrcFidx <> "" Then
                    SrcIdx = Val(DsrcFidx)
                    Select Case DsrcMeth
                        Case "LOG_SPLIT"
                            SplitLogValueList SrcIdx, i, DsrcList
                        Case "LOG_FETCH"
                            TranslateLogValueList SrcIdx, i
                        Case Else
                    End Select
                End If
            End If
        End If
    Next
    'Step 2: Look for Collection of Split Values
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            TabName = HiddenData.GetConfigValues(FileData(i).myName, "NAME")
            TabKey = TabName & "_REFERENCE"
            DsrcRule = HiddenData.GetConfigValues(TabKey, "DSRC")
            If DsrcRule <> "" Then
                DsrcName = GetItem(DsrcRule, 1, ":")
                DsrcMeth = GetItem(DsrcRule, 2, ":")
                DsrcList = GetItem(DsrcRule, 3, ":")
                TabKey = DsrcName & "_REFERENCE"
                DsrcFidx = HiddenData.GetConfigValues(TabKey, "FIDX")
                If DsrcFidx <> "" Then
                    SrcIdx = Val(DsrcFidx)
                    Select Case DsrcMeth
                        Case "LOG_COLLECT"
                            CollectLogValueList SrcIdx, i, True
                        Case "LOG_CHANGES"
                            CollectLogValueList SrcIdx, i, False
                        Case Else
                    End Select
                End If
            End If
        End If
    Next

End Sub

Private Sub TranslateLogValueList(SrcIdx As Integer, TgtIdx As Integer)
    Dim empData As String
    Dim NewData As String
    Dim tmpFldLst1 As String
    Dim tmpFldLst2 As String
    Dim tmpFlst As String
    Dim tmpFval As String
    Dim tmpFldNam As String
    Dim itm As Integer
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim NewLine As Long
    Dim prvLine As Long
    Dim i As Integer
    FileData(TgtIdx).ResetContent
    empData = CreateEmptyLine(FileData(TgtIdx).LogicalFieldList)
    tmpFldLst1 = ""
    tmpFldLst2 = ""
    itm = 0
    tmpFldNam = "START"
    While tmpFldNam <> ""
        itm = itm + 1
        tmpFldNam = GetItem(FileData(TgtIdx).LogicalFieldList, itm, ",")
        If tmpFldNam <> "" Then
            If InStr(FileData(SrcIdx).LogicalFieldList, tmpFldNam) > 0 Then
                tmpFldLst1 = tmpFldLst1 & "," & tmpFldNam
            Else
                tmpFldLst2 = tmpFldLst2 & "," & tmpFldNam
            End If
        End If
    Wend
    tmpFldLst1 = Mid(tmpFldLst1, 2)
    tmpFldLst2 = Mid(tmpFldLst2, 2)
    
    MaxLine = FileData(SrcIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FileData(TgtIdx).InsertTextLine empData, False
        NewLine = FileData(TgtIdx).GetLineCount - 1
        NewData = FileData(SrcIdx).GetFieldValues(CurLine, tmpFldLst1)
        FileData(TgtIdx).SetFieldValues NewLine, tmpFldLst1, NewData
        tmpFlst = FileData(SrcIdx).GetFieldValue(CurLine, "FLST")
        tmpFval = FileData(SrcIdx).GetFieldValue(CurLine, "FVAL")
        tmpFlst = Replace(tmpFlst, Chr(30), ",", 1, -1, vbBinaryCompare)
        tmpFval = Replace(tmpFval, Chr(30), ",", 1, -1, vbBinaryCompare)
        HelperTab(0).ResetContent
        HelperTab(0).LogicalFieldList = tmpFlst
        HelperTab(0).HeaderString = tmpFlst
        HelperTab(0).InsertTextLine tmpFval, False
        
        itm = 0
        tmpFldNam = "START"
        While tmpFldNam <> ""
            itm = itm + 1
            tmpFldNam = GetItem(tmpFldLst2, itm, ",")
            If tmpFldNam <> "" Then
                NewData = HelperTab(0).GetFieldValue(0, tmpFldNam)
                If NewData <> "" Then
                    FileData(TgtIdx).SetFieldValues NewLine, tmpFldNam, NewData
                End If
            End If
        Wend
        
    Next
    FileData(TgtIdx).AutoSizeColumns
    FileData(TgtIdx).Refresh
    lblTabCnt(TgtIdx).Caption = CStr(FileData(TgtIdx).GetLineCount)
    TabLookUp(TgtIdx).HeaderLengthString = FileData(TgtIdx).HeaderLengthString
End Sub

Private Sub SplitLogValueList(SrcIdx As Integer, TgtIdx As Integer, UseFlightFields As String)
    Dim tmpTask As String
    Dim EmptyData As String
    Dim BaseLine As String
    Dim FlightFields As String
    Dim IgnoreFields As String
    Dim MaxSize As String
    Dim newFields As String
    Dim NewData As String
    Dim tmpFldLst1 As String
    Dim tmpFldLst2 As String
    Dim tmpFlst As String
    Dim tmpViaFlst As String
    Dim tmpFval As String
    Dim tmpSfkt As String
    Dim tmpData As String
    Dim tmpFina As String
    Dim tmpMean As String
    Dim tmpType As String
    Dim tmpValu As String
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpDcut As String
    Dim CurOrno As String
    Dim OldOrno As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim tmpFlnoFields As String
    Dim tmpFlnoValues As String
    Dim useFlnoFields As String
    Dim itm As Integer
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim NewLine As Long
    Dim StepLine As Long
    Dim StepFrom As Long
    Dim ColCnt As Long
    Dim CurCol As Long
    Dim CurViaApc3 As String
    Dim ViaColCnt As Long
    Dim ViaCurCol As Long
    Dim ViaMaxLin As Long
    Dim ViaCurLin As Long
    Dim hlpMaxLine As Long
    Dim hlpCurLine As Long
    Dim hlpCurFina As String
    Dim hlpOldFina As String
    Dim hlpCurValu As String
    Dim hlpOldValu As String
    Dim hlpResult As String
    Dim LineStamp As String
    Dim i As Integer
    Dim ShowVal As Boolean
    Dim HitCount As Long
    Dim FirstLine As Long
    Dim CurCount As Long
    Dim MaxCount As Long
    Dim MaxLen As Integer
    tmpTask = "Creating History"
    
    FlightFields = UseFlightFields
    tmpFlnoFields = GetItem(FlightFields, 1, "|")
    MaxSize = GetItem(FlightFields, 2, "|")
    MaxSize = GetItem(MaxSize, 2, "=")
    IgnoreFields = GetItem(FlightFields, 3, "|")
    IgnoreFields = GetItem(IgnoreFields, 2, "=")
    
    SetTabSortCols FileData(SrcIdx), "TIME,ORNO"
    Screen.MousePointer = 11
    FileData(TgtIdx).ResetContent
    
    MaxLen = Val(MaxSize)
    If MaxLen < 12 Then MaxLen = 12
    If MaxSize = "" Then MaxLen = 20
    
    EmptyData = CreateEmptyLine(FileData(TgtIdx).LogicalFieldList)
    tmpFldLst1 = ""
    tmpFldLst2 = ""
    itm = 0
    tmpFldNam = "START"
    While tmpFldNam <> ""
        itm = itm + 1
        tmpFldNam = GetItem(FileData(TgtIdx).LogicalFieldList, itm, ",")
        If tmpFldNam <> "" Then
            If InStr(FileData(SrcIdx).LogicalFieldList, tmpFldNam) > 0 Then
                tmpFldLst1 = tmpFldLst1 & "," & tmpFldNam
            Else
                tmpFldLst2 = tmpFldLst2 & "," & tmpFldNam
            End If
        End If
    Wend
    tmpFldLst1 = Mid(tmpFldLst1, 2)
    tmpFldLst2 = Mid(tmpFldLst2, 2)
    
    newFields = "FINA,VALU,DCUT"
    
    'Must clone target tab first
    HelperTab(1).ResetContent
    HelperTab(1).LogicalFieldList = FileData(TgtIdx).LogicalFieldList
    HelperTab(1).HeaderString = FileData(TgtIdx).LogicalFieldList
    HelperTab(1).HeaderLengthString = FileData(1).HeaderLengthString
    HelperTab(1).ResetContent
    
    OldOrno = "OLD"
    MaxLine = FileData(SrcIdx).GetLineCount - 1
    'Note: We must run behind the last valid line!
    MaxLine = MaxLine + 1
    
    SetProgressValue TgtIdx, tmpTask, CurLine, MaxLine, CurCount, MaxCount, True
    
    For CurLine = 0 To MaxLine
        If CurLine Mod 500 = 0 Then SetProgressValue TgtIdx, tmpTask, CurLine, MaxLine, CurCount, MaxCount, False
        CurOrno = FileData(SrcIdx).GetFieldValue(CurLine, "ORNO")
        If CurOrno <> OldOrno Then
            hlpMaxLine = HelperTab(1).GetLineCount - 1
            If hlpMaxLine >= 0 Then
                'Evaluate history of changes per flight record and fields
                'Must sort by fields and time first
                SetTabSortCols HelperTab(1), "TIME,FINA"
                hlpOldFina = ".OLD."
                hlpOldValu = ".OLD."
                hlpCurLine = 0
                While hlpCurLine <= hlpMaxLine
                    hlpCurFina = HelperTab(1).GetFieldValue(hlpCurLine, "FINA")
                    hlpCurValu = RTrim(HelperTab(1).GetFieldValue(hlpCurLine, "VALU"))
                    If hlpCurFina = hlpOldFina Then
                        If hlpCurValu = hlpOldValu Then
                            'Remove unchanged field value
                            HelperTab(1).DeleteLine hlpCurLine
                            hlpCurLine = hlpCurLine - 1
                            hlpMaxLine = hlpMaxLine - 1
                        Else
                            hlpOldValu = hlpCurValu
                        End If
                    Else
                        'Always keep the first occurance of a field value
                        'But remove initially empty fields
                        If hlpCurValu = "" Then
                            HelperTab(1).DeleteLine hlpCurLine
                            hlpCurLine = hlpCurLine - 1
                            hlpMaxLine = hlpMaxLine - 1
                        Else
                            hlpOldFina = hlpCurFina
                            hlpOldValu = hlpCurValu
                        End If
                    End If
                    hlpCurLine = hlpCurLine + 1
                Wend
                'Now we can transfer the remaining lines
                hlpMaxLine = HelperTab(1).GetLineCount - 1
                If hlpMaxLine >= 0 Then
                    hlpResult = HelperTab(1).GetBuffer(0, hlpMaxLine, vbLf)
                    StepFrom = FileData(TgtIdx).GetLineCount
                    FileData(TgtIdx).InsertBuffer hlpResult, vbLf
                    hlpResult = ""
                    CurCount = CurCount + 1
                    MaxCount = FileData(TgtIdx).GetLineCount - 1
                    For StepLine = StepFrom To MaxCount
                        tmpDcut = FileData(TgtIdx).GetFieldValue(StepLine, "DCUT")
                        If tmpDcut = "*" Then
                            tmpValu = FileData(TgtIdx).GetFieldValue(StepLine, "VALU")
                            FileData(TgtIdx).SetLineTag StepLine, tmpValu
                            tmpValu = Left(tmpValu, MaxLen)
                            FileData(TgtIdx).SetFieldValues StepLine, "VALU", tmpValu
                        End If
                    Next
                End If
                HelperTab(1).ResetContent
            End If
            OldOrno = CurOrno
        End If
        
        tmpSfkt = FileData(SrcIdx).GetFieldValue(CurLine, "SFKT")
        tmpFlst = FileData(SrcIdx).GetFieldValue(CurLine, "FLST")
        tmpFval = FileData(SrcIdx).GetFieldValue(CurLine, "FVAL")
        tmpFlst = Replace(tmpFlst, Chr(30), Chr(4), 1, -1, vbBinaryCompare)
        tmpFval = Replace(tmpFval, Chr(30), Chr(4), 1, -1, vbBinaryCompare)
        FileData(SrcIdx).SetFieldValues CurLine, "FVAL,FLST", tmpFval & "," & tmpFlst
        tmpFlst = Replace(tmpFlst, Chr(4), ",", 1, -1, vbBinaryCompare)
        tmpFval = Replace(tmpFval, Chr(4), ",", 1, -1, vbBinaryCompare)
        HelperTab(0).ResetContent
        HelperTab(0).LogicalFieldList = tmpFlst
        HelperTab(0).HeaderString = tmpFlst
        HelperTab(0).InsertTextLine tmpFval, False
        
        tmpFlnoValues = Trim(HelperTab(0).GetFieldValues(0, "FLNO"))
        If tmpFlnoValues = "" Then
            useFlnoFields = Replace(tmpFlnoFields, "FLNO", "CSGN", 1, -1, vbBinaryCompare)
        Else
            useFlnoFields = tmpFlnoFields
        End If
        tmpFlnoValues = HelperTab(0).GetFieldValues(0, useFlnoFields)
        
        HelperTab(1).InsertTextLine EmptyData, False
        NewLine = HelperTab(1).GetLineCount - 1
        NewData = FileData(SrcIdx).GetFieldValues(CurLine, tmpFldLst1)
        HelperTab(1).SetFieldValues NewLine, tmpFldLst1, NewData
        HelperTab(1).SetFieldValues NewLine, tmpFlnoFields, tmpFlnoValues
        BaseLine = HelperTab(1).GetLineValues(NewLine)
        HelperTab(1).DeleteLine NewLine
        
        ColCnt = HelperTab(0).GetColumnCount - 1
        For CurCol = 0 To ColCnt
            tmpFldNam = GetRealItem(tmpFlst, CurCol, ",")
            tmpFldVal = RTrim(HelperTab(0).GetColumnValue(0, CurCol))
            ShowVal = True
            If InStr(IgnoreFields, tmpFldNam) > 0 Then ShowVal = False
            If (tmpSfkt = "IFR") And (tmpFldVal = "") Then ShowVal = False
            If ShowVal = True Then
                If tmpFldNam = "VIAL" Then
                    MyConfig.BreakDownVialList tmpFldVal
                    ViaMaxLin = MyConfig.VialTab(0).GetLineCount - 1
                    ViaColCnt = MyConfig.VialTab(0).GetColumnCount - 1
                    tmpViaFlst = MyConfig.VialTab(0).LogicalFieldList
                    For ViaCurLin = 0 To ViaMaxLin
                        CurViaApc3 = RTrim(MyConfig.VialTab(0).GetColumnValue(ViaCurLin, 1))
                        For ViaCurCol = 0 To ViaColCnt
                            tmpFldNam = Left(GetRealItem(tmpViaFlst, ViaCurCol, ","), 3) & CStr(ViaCurLin + 1)
                            tmpFldVal = RTrim(MyConfig.VialTab(0).GetColumnValue(ViaCurLin, ViaCurCol))
                            ShowVal = True
                            If InStr(IgnoreFields, tmpFldNam) > 0 Then ShowVal = False
                            If (tmpSfkt = "IFR") And (tmpFldVal = "") Then ShowVal = False
                            If ShowVal = True Then
                                HelperTab(1).InsertTextLine BaseLine, False
                                NewLine = HelperTab(1).GetLineCount - 1
                                tmpDcut = "V-" & CurViaApc3
                                NewData = tmpFldNam & "," & tmpFldVal & "," & tmpDcut
                                HelperTab(1).SetFieldValues NewLine, newFields, NewData
                            End If
                        Next
                    Next
                Else
                    HelperTab(1).InsertTextLine BaseLine, False
                    NewLine = HelperTab(1).GetLineCount - 1
                    tmpDcut = ""
                    If Len(tmpFldVal) > MaxLen Then tmpDcut = "*"
                    NewData = tmpFldNam & "," & tmpFldVal & "," & tmpDcut
                    HelperTab(1).SetFieldValues NewLine, newFields, NewData
                End If
            End If
        Next
    Next
    HelperTab(0).ResetContent
    HelperTab(1).ResetContent
    FileData(TgtIdx).AutoSizeColumns
    FileData(TgtIdx).Refresh
    TabLookUp(TgtIdx).HeaderLengthString = FileData(TgtIdx).HeaderLengthString
    Me.Refresh
    SetProgressValue TgtIdx, tmpTask, 100, 100, CurCount, MaxCount, False
    lblTabCnt(TgtIdx).Caption = CStr(FileData(TgtIdx).GetLineCount)
    
    MaxLine = FileData(TgtIdx).GetLineCount - 1
    tmpTask = "Translate Fieldvalues"
    SetProgressValue TgtIdx, tmpTask, 0, MaxLine, CurCount, MaxCount, True
    
    For CurLine = 0 To MaxLine
        If CurLine Mod 500 = 0 Then SetProgressValue TgtIdx, tmpTask, CurLine, MaxLine, CurLine, MaxCount, False
        tmpFina = FileData(TgtIdx).GetFieldValue(CurLine, "FINA")
        tmpData = GetLookupFieldValues(HiddenData.BasicDataTab(3), "FINA", tmpFina, "ADDI,TYPE", HitCount, FirstLine)
        If HitCount = 0 Then
            tmpDcut = FileData(TgtIdx).GetFieldValue(CurLine, "DCUT")
            If Left(tmpDcut, 1) = "V" Then
                CurViaApc3 = GetItem(tmpDcut, 2, "-")
                tmpDcut = "V"
                tmpValu = FileData(TgtIdx).GetFieldValue(CurLine, "VALU")
                If Len(tmpValu) >= 12 Then
                    tmpDcut = "T"
                    FileData(TgtIdx).SetLineTag CurLine, tmpValu
                End If
                tmpMean = MyConfig.TranslateViaField(tmpFina, CurViaApc3)
                FileData(TgtIdx).SetFieldValues CurLine, "MEAN,DCUT", tmpMean & "," & tmpDcut
            End If
        Else
            tmpMean = GetItem(tmpData, 1, ",")
            tmpMean = Left(tmpMean, 20)
            FileData(TgtIdx).SetFieldValues CurLine, "MEAN", tmpMean
            tmpType = GetItem(tmpData, 2, ",")
            If tmpType = "DATE" Then
                tmpValu = FileData(TgtIdx).GetFieldValue(CurLine, "VALU")
                FileData(TgtIdx).SetFieldValues CurLine, "DCUT", "T"
                FileData(TgtIdx).SetLineTag CurLine, tmpValu
            End If
            If tmpFina = "CHGI" Then
                tmpValu = FileData(TgtIdx).GetFieldValue(CurLine, "VALU")
                FileData(TgtIdx).SetFieldValues CurLine, "DCUT", "C"
                FileData(TgtIdx).SetLineTag CurLine, tmpValu
            End If
        End If
        LineStamp = Right("000000" & CStr(CurLine), 6)
        FileData(TgtIdx).SetFieldValues CurLine, "'S3','C3'", LineStamp & ","
    Next
    CurCol = CLng(GetRealItemNo(FileData(TgtIdx).LogicalFieldList, "VALU"))
    FileData(TgtIdx).SetColumnProperty CurCol, "ColMarker"
    FileData(TgtIdx).AutoSizeColumns
    FileData(TgtIdx).Refresh
    lblTabCnt(TgtIdx).Caption = CStr(FileData(TgtIdx).GetLineCount)
    TabLookUp(TgtIdx).HeaderLengthString = FileData(TgtIdx).HeaderLengthString
    Me.Refresh
    SetProgressValue TgtIdx, tmpTask, 100, 100, MaxCount, MaxCount, False
    Screen.MousePointer = 0
End Sub

Private Sub TranslateLogTimeFieldValues()
    Dim LineNo As Long
    Dim ColNo As Long
    Dim tmpValu As String
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            If InStr(FileData(i).LogicalFieldList, "DCUT") > 0 Then
                ColNo = CLng(GetRealItemNo(FileData(i).LogicalFieldList, "DCUT"))
                FileData(i).SetInternalLineBuffer True
                LineNo = Val(FileData(i).GetLinesByColumnValue(ColNo, "T", 0))
                If LineNo > 0 Then
                    While LineNo >= 0
                        LineNo = FileData(i).GetNextResultLine
                        tmpValu = FileData(i).GetLineTag(LineNo)
                        If tmpValu <> "" Then
                            If chkUtc(1).Value = 1 Then
                                tmpValu = ApcUtcToLocal("", tmpValu, True)
                            End If
                            tmpValu = CreateCedaDateTimeSsimFormat(tmpValu)
                            FileData(i).SetFieldValues LineNo, "VALU", tmpValu
                        End If
                    Wend
                End If
                FileData(i).SetInternalLineBuffer False
                FileData(i).SetInternalLineBuffer True
                LineNo = Val(FileData(i).GetLinesByColumnValue(ColNo, "C", 0))
                If LineNo > 0 Then
                    While LineNo >= 0
                        LineNo = FileData(i).GetNextResultLine
                        tmpValu = FileData(i).GetLineTag(LineNo)
                        If tmpValu <> "" Then
                            If chkUtc(1).Value = 1 Then tmpValu = ApcChgiUtcToLocal("", tmpValu)
                            FileData(i).SetFieldValues LineNo, "VALU", tmpValu
                        End If
                    Wend
                End If
                FileData(i).SetInternalLineBuffer False
                FileData(i).Refresh
            End If
        End If
    Next
End Sub

Private Function ApcChgiUtcToLocal(AptCode As String, ChgiValue As String) As String
    Dim Result As String
    Dim tmpTime As String
    Dim tmpHH As String
    Dim tmpMM As String
    Dim tmpTxt As String
    Dim TotalMin As Integer
    Dim ValHH As Integer
    Dim ValMM As Integer
    Result = ""
    tmpTxt = GetItem(ChgiValue, 1, "=")
    tmpTime = GetItem(ChgiValue, 2, "=")
    tmpHH = GetItem(tmpTime, 1, ":")
    tmpMM = GetItem(tmpTime, 2, ":")
    TotalMin = Val(tmpHH) * 60 + Val(tmpMM) + UtcTimeDiff
    ValHH = TotalMin \ 60
    ValMM = TotalMin - ValHH * 60
    If ValHH >= 24 Then ValHH = ValHH - 24
    If ValMM >= 60 Then
        ValHH = ValHH + 1
        ValMM = ValMM - 60
    End If
    tmpHH = Right("00" & CStr(ValHH), 2)
    tmpMM = Right("00" & CStr(ValMM), 2)
    Result = tmpTxt & "=" & tmpHH & ":" & tmpMM
    ApcChgiUtcToLocal = Result
End Function
Private Sub CollectLogValueList(SrcIdx As Integer, TgtIdx As Integer, KeepValues As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim CurUrno As String
    Dim OldUrno As String
    Dim CurOrno As String
    Dim OldOrno As String
    Dim tmpFina As String
    Dim tmpValu As String
    Dim EmptyData As String
    Dim NewFldList As String
    Dim NewDatList As String
    Dim FixFldList As String
    Dim FixDatList As String
    Dim tmpTask As String
    Dim CurCount As Long
    Dim MaxCount As Long
    
    If KeepValues = True Then
        tmpTask = "Collecting Records"
    Else
        tmpTask = "Collecting Updates"
    End If
    
    Screen.MousePointer = 11
    SetTabSortCols FileData(SrcIdx), "URNO,ORNO"
    FileData(TgtIdx).ResetContent
    
    OldUrno = ".OLD."
    FixFldList = "TIME,USEC,SFKT,ORNO,URNO"
    EmptyData = CreateEmptyLine(FileData(TgtIdx).LogicalFieldList)
    NewFldList = ""
    NewDatList = ""
    LineNo = -1
    MaxLine = FileData(SrcIdx).GetLineCount - 1
    'Must run behind the last line
    MaxLine = MaxLine + 1
    
    SetProgressValue TgtIdx, tmpTask, CurLine, MaxLine, CurCount, MaxCount, True
    
    For CurLine = 0 To MaxLine
        If CurLine Mod 500 = 0 Then SetProgressValue TgtIdx, tmpTask, CurLine, MaxLine, CurCount, MaxCount, False
        CurUrno = FileData(SrcIdx).GetFieldValue(CurLine, "URNO")
        CurOrno = FileData(SrcIdx).GetFieldValue(CurLine, "ORNO")
        If CurUrno <> OldUrno Then
            If NewFldList <> "" Then
                FileData(TgtIdx).InsertTextLine EmptyData, False
                LineNo = FileData(TgtIdx).GetLineCount - 1
                FileData(TgtIdx).SetFieldValues LineNo, FixFldList, FixDatList
                FileData(TgtIdx).SetFieldValues LineNo, NewFldList, NewDatList
                MaxCount = MaxCount + 1
            End If
            If KeepValues = True Then
                If CurOrno = OldOrno Then
                    If LineNo >= 0 Then EmptyData = FileData(TgtIdx).GetLineValues(LineNo)
                Else
                    EmptyData = CreateEmptyLine(FileData(TgtIdx).LogicalFieldList)
                    CurCount = CurCount + 1
                End If
            Else
                CurCount = CurLine + 1
            End If
            FixDatList = FileData(SrcIdx).GetFieldValues(CurLine, FixFldList)
            NewFldList = ""
            NewDatList = ""
            OldUrno = CurUrno
            OldOrno = CurOrno
            LineNo = -1
        End If
        tmpFina = FileData(SrcIdx).GetFieldValue(CurLine, "FINA")
        If InStr(FileData(TgtIdx).LogicalFieldList, tmpFina) > 0 Then
            tmpValu = RTrim(FileData(SrcIdx).GetFieldValue(CurLine, "VALU"))
            If tmpValu = "" Then tmpValu = "#"
            NewFldList = NewFldList & tmpFina & ","
            NewDatList = NewDatList & tmpValu & ","
        End If
    Next
    FileData(TgtIdx).AutoSizeColumns
    FileData(TgtIdx).Refresh
    MaxCount = FileData(TgtIdx).GetLineCount
    lblTabCnt(TgtIdx).Caption = CStr(MaxCount)
    TabLookUp(TgtIdx).HeaderLengthString = FileData(TgtIdx).HeaderLengthString
    Me.Refresh
    SetProgressValue TgtIdx, tmpTask, 100, 100, CurCount, MaxCount, False
    Screen.MousePointer = 0
End Sub

Private Sub SetProgressValue(Index As Integer, CurTask As String, CurValue As Long, MaxValue As Long, CntValue As Long, TtlValue As Long, newEntry As Boolean)
    Dim tmpTag As String
    Dim GrpAreaText As String
    Dim GrpNameText As String
    tmpTag = GetItem(FileData(Index).myTag, 1, "|")
    GrpAreaText = GetItem(tmpTag, 1, ",")
    GrpNameText = GetItem(tmpTag, 2, ",")
    If newEntry = True Then WorkProgress.CreateNewEntry GrpAreaText, GrpNameText, CurTask, False
    WorkProgress.SetTaskText CurTask
    WorkProgress.UpdateProgress CurValue, MaxValue, CntValue, TtlValue
End Sub

Public Function CleanGridName(Index As Integer) As String
    Dim Result As String
    Dim tmpTag As String
    Dim tmpTagItem1 As String
    Dim tmpTagItem2 As String
    tmpTag = FileData(Index).myTag
    tmpTagItem1 = GetItem(tmpTag, 1, "|")
    tmpTagItem2 = GetItem(tmpTag, 2, "|")
    If InStr(tmpTagItem2, ":") > 0 Then tmpTag = GetItem(tmpTagItem1, 1, ",") Else tmpTag = tmpTagItem1
    tmpTag = Replace(tmpTag, ",", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, "'", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, " ", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, ".", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, "&", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, "/", "", 1, -1, vbBinaryCompare)
    tmpTag = Replace(tmpTag, "-", "", 1, -1, vbBinaryCompare)
    Result = tmpTag
    CleanGridName = Result
End Function

Private Function GetTabIdxByName(TabName As String) As Integer
    Dim srcTabKey As String
    Dim srcTabIdx As String
    Dim srcIndex As Integer
    srcIndex = -1
    srcTabKey = TabName & "_REFERENCE"
    srcTabIdx = HiddenData.GetConfigValues(srcTabKey, "FIDX")
    If srcTabIdx <> "" Then srcIndex = Val(srcTabIdx)
    GetTabIdxByName = srcIndex
End Function

Private Function TabsFillCount(AllTabs As Boolean, GridCount As Integer) As Long
    Dim CntLine As Long
    Dim MaxLine As Long
    Dim CurGrid As Integer
    GridCount = 0
    CntLine = 0
    For CurGrid = 0 To FileData.UBound
        If AllTabs Then
            If chkTabIsVisible(CurGrid).Visible Then
                MaxLine = FileData(CurGrid).GetLineCount
                CntLine = CntLine + MaxLine
                GridCount = GridCount + 1
            End If
        Else
            If chkTabIsVisible(CurGrid).Value = 1 Then
                MaxLine = FileData(CurGrid).GetLineCount
                CntLine = CntLine + MaxLine
                GridCount = GridCount + 1
            End If
        End If
    Next
    TabsFillCount = CntLine
End Function

Public Sub RecoverFileWrite(Index As Integer, MyContext As String, WriteAll As Boolean)
    Dim fn As Integer
    Dim tmpObj As String
    Dim tmpIdx As String
    Dim tmpKey As String
    Dim tmpVal As String
    Dim ObjIdx As Integer
    If Not FipsIsConnected Then
        fn = FreeFile
        Close #fn
        Open RecoverFileName For Output As #fn
        Print #fn, MyContext
        For ObjIdx = 0 To chkWork.UBound
            If (chkWork(ObjIdx).Visible) And (ObjIdx <> Index) Then
                RecoverWriteObj fn, "WKB", CStr(ObjIdx), "-", CStr(chkWork(ObjIdx).Value)
            End If
        Next
        For ObjIdx = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(ObjIdx).Visible Then
                RecoverWriteObj fn, "TAB", CStr(ObjIdx), "SCRL", CStr(FileData(ObjIdx).GetVScrollPos)
                RecoverWriteObj fn, "TAB", CStr(ObjIdx), "CURS", CStr(FileData(ObjIdx).GetCurrentSelected)
            End If
        Next
        RecoverWriteObj fn, "UTC", "0", "-", CStr(chkUtc(0).Value)
        RecoverWriteObj fn, "UTC", "1", "-", CStr(chkUtc(1).Value)
        RecoverWriteObj fn, "SEL", "0", "-", CStr(chkSelDeco(0).Value)
        RecoverWriteObj fn, "SEL", "1", "-", CStr(chkSelDeco(1).Value)
        Close #fn
    End If
End Sub

Private Sub RecoverWriteObj(FiNo As Integer, tmpObj As String, tmpIdx As String, tmpKey As String, tmpVal As String)
    Dim tmpData As String
    tmpData = tmpObj & "," & tmpIdx & "," & tmpKey & "," & tmpVal
    Print #FiNo, tmpData
End Sub
Public Sub RecoverFileRead(ResetAll As Boolean)
    Dim fn As Integer
    Dim tmpAction As String
    Dim tmpData As String
    On Error GoTo myExit
    If Not FipsIsConnected Then
        fn = FreeFile
        Open RecoverFileName For Input As #fn
        Line Input #fn, tmpAction
        Select Case tmpAction
            Case "Exit"
                AutoRecover = False
            Case "Break"
                AutoRecover = True
                RecoverTitle = "Welcome back!   Restoring the data grids and buttons now ..."
            Case "CallPrint"
                AutoRecover = True
                RecoverTitle = "Sorry, beg your pardon!   Now recovering from an unexpected termination ..."
            Case "Other"
                AutoRecover = True
                RecoverTitle = "Recovering from an unexpected termination ..."
            Case Else
                AutoRecover = False
                RecoverTitle = ""
        End Select
        If AutoRecover = True Then
            PushWorkButton "READLOCAL", 1
            While Not EOF(fn)
                Line Input #fn, tmpData
                RecoverSetObjects tmpData
            Wend
        End If
        Close #fn
        AutoRecover = False
        WorkProgress.Hide
        Me.Refresh
    End If
    Exit Sub
myExit:
    Close #fn
    AutoRecover = False
    WorkProgress.Hide
    Me.Refresh
End Sub

Private Sub RecoverSetObjects(KeyList As String)
    Dim tmpObj As String
    Dim tmpIdx As String
    Dim tmpKey As String
    Dim tmpVal As String
    Dim LngVal As Long
    Dim IntVal As Integer
    Dim ObjIdx As Integer
    On Error Resume Next
    tmpObj = GetItem(KeyList, 1, ",")
    tmpIdx = GetItem(KeyList, 2, ",")
    tmpKey = GetItem(KeyList, 3, ",")
    tmpVal = GetItem(KeyList, 4, ",")
    ObjIdx = Val(tmpIdx)
    LngVal = Val(tmpVal)
    IntVal = Val(tmpVal)
    Select Case tmpObj
        Case "UTC"
            chkUtc(ObjIdx).Value = IntVal
        Case "WKB"
            chkWork(ObjIdx).Value = IntVal
        Case "TAB"
            Select Case tmpKey
                Case "SCRL"
                    FileData(ObjIdx).OnVScrollTo LngVal
                Case "CURS"
                    FileData(ObjIdx).SetCurrentSelection LngVal
                Case Else
            End Select
        Case Else
    End Select
End Sub

Private Sub txtTabEditLookUp_Change(Index As Integer)
    SyncCursorBusy = True
    SearchInTabList FileData(Index), TabEditColNo, txtTabEditLookUp(Index).Text, True, "", ""
    SyncCursorBusy = False
End Sub

Private Sub txtTabEditLookUp_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Dim RetVal As Boolean
    Select Case KeyCode
        'Case vbKeyEscape
            'retVal = SetLookUpKeyValues(Index)
            'txtTabEditLookUp(Index).Text = txtTabEditLookUp(Index).Tag
            'FileData(Index).SetFocus
        Case vbKeyReturn, vbKeyEscape
            RetVal = SetLookUpKeyValues(Index)
            FileData(Index).SetFocus
            FileData(Index).SetCurrentSelection FileData(Index).GetCurrentSelected
            If RetVal = False Then TabEditLookUpIndex = -1
        Case Else
    End Select
End Sub

Private Sub txtTabEditLookUp_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Chr(KeyAscii)
        Case ",", vbLf, vbCr
            KeyAscii = 0
        Case Else
            If TabEditIsUpperCase Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End Select
End Sub

Private Sub txtTabEditLookUp_LostFocus(Index As Integer)
    Dim CurKey As Integer
    Dim tmpText As String
    Dim CurCol As Long
    CurKey = keyCheck
    txtTabEditLookUp(Index).Visible = False
    If Trim(txtTabEditLookUp(Index).Text) <> "" Then TabEditLookUpValueCol = TabEditColNo
    TabLookUp(Index).SetColumnValue TabEditLineNo, TabEditColNo, txtTabEditLookUp(Index).Text
    Select Case CurKey
        Case vbKeyTab
            SetLookUpKeyValues Index
            If KeybdIsShift = False Then
                If TabEditColNo < (TabLookUp(Index).GetColumnCount - 1) Then TabEditColNo = TabEditColNo + 1
            Else
                If TabEditColNo > 0 Then TabEditColNo = TabEditColNo - 1
            End If
            SetTxtTabEditLookUpPosition Index
        Case Else
            If TabEditLookUpType = "LOOK" Then
                TabLookUp(Index).ResetLineDecorations 0
                tmpText = ""
                If TabEditLookUpValueCol >= 0 Then tmpText = TabLookUp(Index).GetColumnValue(0, TabEditLookUpValueCol)
                If Trim(tmpText) = "" Then
                    TabEditLookUpValueCol = -1
                    For CurCol = 0 To TabLookUp(Index).GetColumnCount - 1
                        If Trim(TabLookUp(Index).GetColumnValue(0, CurCol)) <> "" Then
                            TabEditLookUpValueCol = CurCol
                            Exit For
                        End If
                    Next
                End If
                If TabEditLookUpValueCol >= 0 Then
                    TabLookUp(Index).SetDecorationObject 0, TabEditLookUpValueCol, "BookMark"
                    TabLookUp(Index).Refresh
                    If Index = TabEditLookUpFixed Then
                        lblTabUtl(Index).BackColor = vbRed
                    Else
                        lblTabUtl(Index).BackColor = vbYellow
                    End If
                Else
                    If Index = TabEditLookUpFixed Then TabEditLookUpFixed = -1
                    lblTabUtl(Index).BackColor = MinMaxPanel(Index).BackColor
                End If
                TabEditColNo = TabEditLookUpValueCol
                SetLookUpKeyValues Index
            End If
    End Select
End Sub

Private Sub WorkArea_DblClick()
    Dim tmpData As String
    If SlideActive = True Then
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "SCENARIO_TOOLBARS", "NO")
        If tmpData = "YES" Then ShowScenarioTools = True Else ShowScenarioTools = False
        If ShowScenarioTools = True Then
            OnTop.Value = 0
            SlideControls.Show , Me
        End If
    End If
End Sub

Private Function SetLookUpKeyValues(Index) As Boolean
    Dim tmpCols As String
    Dim tmpKeys As String
    Dim tmpData As String
    Dim tmpLineTag As String
    Dim tmpTabTag As String
    Dim CurCol As Long
    Dim MaxCol As Long
    SetLookUpKeyValues = False
    tmpCols = ""
    tmpKeys = ""
    tmpLineTag = ""
    tmpTabTag = ""
    MaxCol = TabLookUp(Index).GetColumnCount - 1
    For CurCol = 0 To MaxCol
        tmpData = Trim(TabLookUp(Index).GetColumnValue(0, CurCol))
        If tmpData <> "" Then
            tmpCols = tmpCols & CStr(CurCol) & ","
            tmpKeys = tmpKeys & tmpData & ","
        End If
    Next
    If tmpCols <> "" Then
        tmpCols = Left(tmpCols, Len(tmpCols) - 1)
        tmpKeys = Left(tmpKeys, Len(tmpKeys) - 1)
        tmpLineTag = tmpLineTag & "{=COLS=}" & tmpCols
        tmpLineTag = tmpLineTag & "{=KEYS=}" & tmpKeys
        tmpTabTag = tmpTabTag & "{=IDX=}" & CStr(Index)
        tmpTabTag = tmpTabTag & "{=COL=}" & CStr(TabEditLookUpValueCol)
        tmpTabTag = tmpTabTag & "{=TYP=}" & TabEditLookUpType
        SetLookUpKeyValues = True
    Else
        If Index = TabEditLookUpIndex Then TabEditLookUpIndex = -1
    End If
    TabLookUp(Index).SetLineTag 0, tmpLineTag
    TabLookUp(Index).Tag = tmpTabTag
End Function

Private Function GetLookUpKeyValues(Index) As Boolean
    Dim tmpData As String
    Dim tmpTabTag As String
    GetLookUpKeyValues = False
    tmpTabTag = TabLookUp(Index).Tag
    If tmpTabTag <> "" Then
        'GetKeyItem tmpData, tmpTabTag, "{=IDX=}", "{="
        TabEditLookUpIndex = Index
        GetKeyItem tmpData, tmpTabTag, "{=COL=}", "{="
        TabEditLookUpValueCol = Val(tmpData)
        GetKeyItem TabEditLookUpType, tmpTabTag, "{=TYP=}", "{="
        Set TabEditLookUpTabObj = TabLookUp(Index)
        GetLookUpKeyValues = True
    End If
End Function

Private Sub CallFipsFlight(CurTab As TABLib.Tab, LineNo As Long)
    Dim RetVal As Boolean
    Dim tmpData As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpUrno As String
    Dim FipsFldLst As String
    Dim FipsDatLst As String
    Dim FipsMsgTxt As String
    Dim CurFlightRecord As String
    CurFlightRecord = CurTab.GetLineValues(LineNo)
    tmpUrno = Trim(CurTab.GetFieldValue(LineNo, "URNO"))
    
    If tmpUrno <> "" Then
        'VPFR , VPTO, ALC3, Fltn, Flns, Regn, Time, FLNU
        '20001210000000,20001210235900,CSA,420, , , ,27641709
        FipsFldLst = "VPFR,VPTO,ALC3,FLTN,FLNS,REGN,TIME,FLNU"
        FipsDatLst = ""
        'RetVal = GetValidTimeFrame(tmpVpfr, tmpVpto, True)
        RetVal = True
        If RetVal = True Then
            FipsDatLst = FipsDatLst & tmpVpfr & ","
            FipsDatLst = FipsDatLst & tmpVpto & ","
            tmpData = Trim(GetFieldValue("ALC3", CurFlightRecord, CurTab.LogicalFieldList))
            'If tmpData = "" Then tmpData = Trim(txtFlca.Text)
            FipsDatLst = FipsDatLst & tmpData & ","
            tmpData = Trim(GetFieldValue("FLTN", CurFlightRecord, CurTab.LogicalFieldList))
            'If tmpData = "" Then tmpData = txtFltn.Tag
            FipsDatLst = FipsDatLst & tmpData & ","
            tmpData = Trim(GetFieldValue("FLNS", CurFlightRecord, CurTab.LogicalFieldList))
            'If tmpData = "" Then tmpData = Trim(txtFlns.Text)
            FipsDatLst = FipsDatLst & tmpData & ","
            'tmpData = Trim(txtRegn.Text)
            tmpData = ""
            'tmpData = Replace(tmpData, "-", "", 1, -1, vbBinaryCompare)
            FipsDatLst = FipsDatLst & tmpData & ","
            tmpData = Trim(GetFieldValue("TIME", CurFlightRecord, CurTab.LogicalFieldList))
            FipsDatLst = FipsDatLst & tmpData & ","
            'tmpData = Trim(GetFieldValue("FLNU", CurFlightRecord, CurTab.LogicalFieldList))
            'If Val(tmpData) < 1 Then tmpData = ""
            tmpData = tmpUrno
            FipsDatLst = FipsDatLst & tmpData
            FipsMsgTxt = FipsFldLst & vbLf & FipsDatLst
            'CheckFormOnTop Me, False
            UfisServer.SendMessageToFips FipsMsgTxt
        Else
            'MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Filter", "The actual filter does not match your selection.", "stop", "", UserAnswer
        End If
    Else
        'MyMsgBox.CallAskUser 0, 0, 0, "Show Flight", "No telex selected.", "hand", "", UserAnswer
    End If
End Sub

Public Sub MessageFromFips(MsgFields As String, MsgData As String)
    Dim AftUrno As String
    Dim CurUrno As String
    Dim CurTabRec As String
    Dim UrnoCol As Long
    Dim BcsqCol As Long
    Dim MinLine As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    If ApplShortCode = "FDC" Then
        On Error Resume Next
        If ApplIsInTray Then
            Unhook    ' Return event control to windows
            Me.WindowState = vbNormal
            Me.Show
            RemoveIconFromTray
            ApplIsInTray = False
        End If
        MainDialog.Icon = MyOwnForm.MyIcons(0).Picture
        AftUrno = GetFieldValue("URNO", MsgData, MsgFields)
        If AftUrno <> "" Then
            UrnoCol = GetRealItemNo(FileData(0).LogicalFieldList, "URNO")
            If UrnoCol >= 0 Then
                BcsqCol = GetRealItemNo(FileData(0).LogicalFieldList, "'S3'")
                If BcsqCol >= 0 Then
                    FileData(0).Sort CStr(BcsqCol), False, True
                    MinLine = 0
                    MaxLine = FileData(0).GetLineCount - 1
                    CurLine = MaxLine
                    While CurLine >= MinLine
                        CurUrno = FileData(0).GetColumnValue(CurLine, UrnoCol)
                        If CurUrno = AftUrno Then
                            CurTabRec = FileData(0).GetLineValues(CurLine)
                            FileData(0).DeleteLine CurLine
                            FileData(0).InsertTextLineAt 0, CurTabRec, False
                            MinLine = MinLine + 1
                            CurLine = CurLine + 1
                        End If
                        CurLine = CurLine - 1
                    Wend
                    FileData(0).Refresh
                    FileData(0).SetCurrentSelection 0
                End If
            End If
        End If
    End If
End Sub

' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    SetForegroundWindow Me.hwnd
    PopupMenu FDCPopup, vbPopupMenuRightButton
End Sub

Private Sub ShowMe_Click()
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
    ApplIsInTray = False
End Sub

Private Sub ExitMe_Click()
    On Error Resume Next
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
    ApplIsInTray = False
    MainTimer(3).Tag = "EXIT"
    MainTimer(3).Enabled = True
End Sub

Private Sub InitRotationGrid()
    Dim RotaFields As String
    RotationData.Top = 0
    RotationData.Left = 0
    RotationData.ResetContent
    If MainLifeStyle Then
        RotationData.LifeStyle = True
        RotationData.CursorLifeStyle = True
    End If
    RotaFields = "VIEW,DRGN,AFLT,DFLT,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,'S1',SORT"
    RotationData.LogicalFieldList = RotaFields
    RotationData.HeaderString = "CH,Aircraft,Arr. Flight ,Dep. Flight ,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,S1,SORT"
    RotationData.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    RotationData.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
    RotationData.FontName = CurrentTabFont
    RotationData.FontSize = FontSlider.Value + 6
    RotationData.HeaderFontSize = FontSlider.Value + 6
    RotationData.LineHeight = FontSlider.Value + 8
    RotationData.LeftTextOffset = 1
    RotationData.SetTabFontBold True
    RotationData.MainHeader = True
    RotationData.SetMainHeaderValues "4,18", "Rotation on Ground,System", "12632256,12632256"
    RotationData.SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, CurrentTabFont
    RotationData.CreateDecorationObject "DecoMarkerLB", "L,L,T,B,R", "-1,2,2,2,2", Str(LightestBlue) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(DarkGray) & "," & Str(DarkGray)
    RotationData.SetColumnBoolProperty 0, "1", "0"
    RotationData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'RotationData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'RotationData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'RotationData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    RotationData.AutoSizeByHeader = True
    RotationData.AutoSizeColumns
    AdjustSystabPanel
    RotationData.DeleteLine 0
    'RotationData.ShowVertScroller True
    RotationData.Visible = True
    RotationData.ZOrder
    
End Sub
Private Sub InitConnexGrid()
    Dim RotaFields As String
    ConnexData.Top = 0
    ConnexData.Left = 0
    ConnexData.ResetContent
    If MainLifeStyle Then
        ConnexData.LifeStyle = True
        ConnexData.CursorLifeStyle = True
    End If
    RotaFields = "VIEW,DRGN,AFLT,DFLT,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,'S1',SORT"
    ConnexData.LogicalFieldList = RotaFields
    ConnexData.HeaderString = "CH,Aircraft,Arr. Flight ,Dep. Flight ,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,S1,SORT"
    ConnexData.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    ConnexData.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
    ConnexData.FontName = CurrentTabFont
    ConnexData.FontSize = FontSlider.Value + 6
    ConnexData.HeaderFontSize = FontSlider.Value + 6
    ConnexData.LineHeight = FontSlider.Value + 8
    ConnexData.LeftTextOffset = 1
    ConnexData.SetTabFontBold True
    ConnexData.MainHeader = True
    ConnexData.SetMainHeaderValues "4,18", "Rotation on Ground,System", "12632256,12632256"
    ConnexData.SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, CurrentTabFont
    ConnexData.CreateDecorationObject "DecoMarkerLB", "L,L,T,B,R", "-1,2,2,2,2", Str(LightestBlue) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(DarkGray) & "," & Str(DarkGray)
    ConnexData.SetColumnBoolProperty 0, "1", "0"
    ConnexData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'ConnexData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'ConnexData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    'ConnexData.InsertTextLine "0,ABCDE,XX 123/12,XX 123/12", False
    ConnexData.AutoSizeByHeader = True
    ConnexData.AutoSizeColumns
    AdjustSystabPanel
    ConnexData.DeleteLine 0
    'ConnexData.ShowVertScroller True
    ConnexData.Visible = True
    ConnexData.ZOrder
    
End Sub

Private Sub FillRotationGrid()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ArrLine As Long
    Dim RotLine As Long
    Dim HitCount As Long
    Dim ColNo As Long
    Dim ArrUrno As String
    Dim ArrRkey As String
    Dim ArrAurn As String
    Dim ArrFtyp As String
    Dim ArrRegn As String
    Dim ArrFlno As String
    Dim ArrStoa As String
    Dim ArrTifa As String
    Dim ArrTisa As String
    Dim ArrIndx As String
    Dim DepUrno As String
    Dim DepRkey As String
    Dim DepAurn As String
    Dim DepFtyp As String
    Dim DepRegn As String
    Dim DepFlno As String
    Dim DepStod As String
    Dim DepTifd As String
    Dim DepTisd As String
    Dim DepIndx As String
    Dim RotBest As String
    Dim RotStat As String
    Dim RotaRec As String
    Dim tmpData As String
    Dim RotaIdx As String
    Dim ArrFltFields As String
    Dim DepFltFields As String
    Dim ArrRotFields As String
    Dim DepRotFields As String
    Dim ArrFltData As String
    Dim DepFltData As String
    Dim ArrFound As Boolean
    Dim LoadArr As Boolean
    If (TabArrFlightsIdx >= 0) And (TabDepFlightsIdx >= 0) Then
        RotationData.ResetContent
        RotationData.ShowHorzScroller False
        RotationData.Refresh
        ColNo = CLng(GetRealItemNo(TabArrFlightsTab.LogicalFieldList, "'C4'"))
        TabArrFlightsTab.IndexDestroy "RIDX"
        MaxLine = TabArrFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            TabArrFlightsTab.SetColumnValue CurLine, ColNo, ""
        Next
        ColNo = CLng(GetRealItemNo(TabDepFlightsTab.LogicalFieldList, "'C4'"))
        TabDepFlightsTab.IndexDestroy "RIDX"
        MaxLine = TabDepFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            TabDepFlightsTab.SetColumnValue CurLine, ColNo, ""
        Next
        Me.Refresh
        RotLine = -1
        Screen.MousePointer = 11
        MaxLine = TabDepFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            DepFtyp = TabDepFlightsTab.GetFieldValue(CurLine, "FTYP")
            If InStr("TG", DepFtyp) = 0 Then
                DepUrno = TabDepFlightsTab.GetFieldValue(CurLine, "URNO")
                DepRkey = TabDepFlightsTab.GetFieldValue(CurLine, "RKEY")
                DepAurn = TabDepFlightsTab.GetFieldValue(CurLine, "AURN")
                DepRegn = TabDepFlightsTab.GetFieldValue(CurLine, "REGN")
                DepFlno = TabDepFlightsTab.GetFieldValue(CurLine, "FLNO")
                If DepFlno = "" Then DepFlno = TabDepFlightsTab.GetFieldValue(CurLine, "CSGN")
                DepStod = TabDepFlightsTab.GetFieldValue(CurLine, "STOD")
                DepTifd = TabDepFlightsTab.GetFieldValue(CurLine, "TIFD")
                DepTisd = TabDepFlightsTab.GetFieldValue(CurLine, "TISD")
                DepFlno = DepFlno & "/" & Mid(DepStod, 7, 2)
                DepIndx = TabDepFlightsTab.GetFieldValue(CurLine, "'S3'")
                If DepRkey <> DepUrno Then
                    'This is a rotation
                    ArrUrno = GetLookupFieldValues(TabArrFlightsTab, "URNO", DepAurn, "URNO", HitCount, ArrLine)
                    If HitCount > 0 Then
                        ArrFtyp = TabArrFlightsTab.GetFieldValue(ArrLine, "FTYP")
                        ArrRegn = TabArrFlightsTab.GetFieldValue(ArrLine, "REGN")
                        ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "FLNO")
                        If ArrFlno = "" Then ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "CSGN")
                        ArrStoa = TabArrFlightsTab.GetFieldValue(ArrLine, "STOA")
                        ArrRkey = TabArrFlightsTab.GetFieldValue(ArrLine, "RKEY")
                        ArrTifa = TabArrFlightsTab.GetFieldValue(ArrLine, "TIFA")
                        ArrTisa = TabArrFlightsTab.GetFieldValue(ArrLine, "TISA")
                        ArrFlno = ArrFlno & "/" & Mid(ArrStoa, 7, 2)
                        ArrIndx = TabArrFlightsTab.GetFieldValue(ArrLine, "'S3'")
                        If InStr("OA", DepTisd) > 0 Then
                            RotBest = DepTifd
                        ElseIf InStr("O", ArrTisa) > 0 Then
                            RotBest = DepTifd
                        ElseIf InStr("XND", ArrFtyp) > 0 Then
                            RotBest = DepTifd
                        Else
                            RotBest = CedaDateTimeAdd(ArrTifa, 30)
                        End If
                        If RotBest > DepTifd Then RotBest = DepTifd
                        ArrFound = True
                        LoadArr = False
                    Else
                        'Joined Arrival Flight obviously not loaded
                        'To be handled in a second step of validation
                        ArrFound = False
                        LoadArr = True
                    End If
                Else
                    'Departure Flight not joined to an arrival
                    ArrFound = False
                    LoadArr = False
                End If
                If ArrFound = False Then
                    ArrUrno = ""
                    ArrFtyp = "-"
                    ArrRegn = ""
                    ArrFlno = ""
                    ArrStoa = ""
                    ArrRkey = ""
                    ArrTifa = ""
                    ArrTisa = "-"
                    ArrIndx = ""
                    RotBest = DepTifd
                    RotStat = DepTisd
                    ArrLine = -1
                End If
                If InStr("XND", ArrFtyp) > 0 Then ArrTisa = "X"
                If InStr("XND", DepFtyp) > 0 Then DepTisd = "X"
                RotStat = ArrTisa & DepTisd
                RotLine = RotLine + 1
                RotaIdx = Right("000000" & CStr(RotLine), 6)
                
                'RotaFields = "DRGN,AFLT,DFLT,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,SORT"
                RotaRec = "1,"
                RotaRec = RotaRec & DepRegn & "," & ArrFlno & "," & DepFlno & "," & ArrRegn & ","
                RotaRec = RotaRec & ArrStoa & "," & DepStod & "," & ArrUrno & "," & DepUrno & ","
                RotaRec = RotaRec & ArrRkey & "," & DepRkey & "," & ArrTifa & "," & DepTifd & ","
                RotaRec = RotaRec & RotBest & "," & RotStat & ","
                RotaRec = RotaRec & RotaIdx & "," & ArrIndx & "," & DepIndx & ","
                RotaRec = RotaRec & "" & "," & "" & ","
                RotaRec = RotaRec & "" & "," & ""
                RotationData.InsertTextLine RotaRec, False
                TabDepFlightsTab.SetFieldValues CurLine, "'C4'", RotaIdx
                If ArrLine >= 0 Then
                    If (ArrRkey <> DepRkey) Or (ArrRegn <> DepRegn) Or (ArrUrno <> DepAurn) Then
                        RotationData.SetLineColor RotLine, vbBlack, vbRed
                    End If
                    TabArrFlightsTab.SetFieldValues ArrLine, "'C4'", RotaIdx
                End If
            Else
                RotaRec = ""
            End If
        Next
        'Transfer single arrival flights
        DepFtyp = "-"
        DepUrno = ""
        DepRkey = ""
        DepAurn = ""
        DepRegn = ""
        DepFlno = ""
        DepStod = ""
        DepTifd = ""
        DepTisd = "-"
        DepFlno = ""
        DepIndx = ""
        MaxLine = TabArrFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            RotaIdx = Trim(TabArrFlightsTab.GetFieldValue(CurLine, "'C4'"))
            If RotaIdx = "" Then
                ArrFtyp = TabDepFlightsTab.GetFieldValue(CurLine, "FTYP")
                If InStr("TG", ArrFtyp) = 0 Then
                    ArrLine = CurLine
                    ArrUrno = TabArrFlightsTab.GetFieldValue(ArrLine, "URNO")
                    ArrRegn = TabArrFlightsTab.GetFieldValue(ArrLine, "REGN")
                    ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "FLNO")
                    If ArrFlno = "" Then ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "CSGN")
                    ArrStoa = TabArrFlightsTab.GetFieldValue(ArrLine, "STOA")
                    ArrRkey = TabArrFlightsTab.GetFieldValue(ArrLine, "RKEY")
                    ArrTifa = TabArrFlightsTab.GetFieldValue(ArrLine, "TIFA")
                    ArrFlno = ArrFlno & "/" & Mid(ArrStoa, 7, 2)
                    ArrIndx = TabArrFlightsTab.GetFieldValue(ArrLine, "'S3'")
                    ArrFound = True
                    LoadArr = False
                    If InStr("XND", ArrFtyp) > 0 Then
                        RotBest = CedaDateTimeAdd(ArrTifa, 15)
                        ArrTisa = "X"
                    Else
                        RotBest = CedaDateTimeAdd(ArrTifa, 30)
                    End If
                    RotStat = ArrTisa & DepTisd
                    RotLine = RotLine + 1
                    RotaIdx = Right("000000" & CStr(RotLine), 6)
                    RotaRec = "1,"
                    RotaRec = RotaRec & ArrRegn & "," & ArrFlno & "," & DepFlno & "," & DepRegn & ","
                    RotaRec = RotaRec & ArrStoa & "," & DepStod & "," & ArrUrno & "," & DepUrno & ","
                    RotaRec = RotaRec & ArrRkey & "," & DepRkey & "," & ArrTifa & "," & DepTifd & ","
                    RotaRec = RotaRec & RotBest & "," & RotStat & ","
                    RotaRec = RotaRec & RotaIdx & "," & ArrIndx & "," & DepIndx & ","
                    RotaRec = RotaRec & "" & "," & "" & ","
                    RotaRec = RotaRec & "" & "," & ""
                    RotationData.InsertTextLine RotaRec, False
                    TabArrFlightsTab.SetFieldValues CurLine, "'C4'", RotaIdx
                End If
            End If
        Next
        RotationData.AutoSizeColumns
        RotationData.Refresh
        AdjustSystabPanel
        RotationData.ShowHorzScroller True
        TabArrFlightsTab.AutoSizeColumns
        TabDepFlightsTab.AutoSizeColumns
        Screen.MousePointer = 0
        Me.Refresh
        ColNo = CLng(GetRealItemNo(RotationData.LogicalFieldList, "BEST"))
        RotationData.Sort CStr(ColNo), True, True
        RotationData.Refresh
        ColNo = CLng(GetRealItemNo(TabArrFlightsTab.LogicalFieldList, "'C4'"))
        TabArrFlightsTab.IndexCreate "RIDX", ColNo
        ColNo = CLng(GetRealItemNo(TabDepFlightsTab.LogicalFieldList, "'C4'"))
        TabDepFlightsTab.IndexCreate "RIDX", ColNo
        ColNo = CLng(GetRealItemNo(RotationData.LogicalFieldList, "RIDX"))
        RotationData.IndexCreate "RIDX", ColNo
        ColNo = CLng(GetRealItemNo(RotationData.LogicalFieldList, "AURN"))
        RotationData.IndexCreate "AURN", ColNo
        ColNo = CLng(GetRealItemNo(RotationData.LogicalFieldList, "DURN"))
        RotationData.IndexCreate "DURN", ColNo
        If FormIsLoaded("StatusChart") = True Then
            StatusChart.GetFlightRotations
        End If
        If FormIsLoaded("ConnexChart") = True Then
            ConnexChart.GetFlightRotations
        End If
        RotationData_SendLButtonDblClick -1, -1
    End If
End Sub

Private Sub FillConnexGrid()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ArrLine As Long
    Dim RotLine As Long
    Dim HitCount As Long
    Dim ColNo As Long
    Dim ArrUrno As String
    Dim ArrRkey As String
    Dim ArrAurn As String
    Dim ArrFtyp As String
    Dim ArrRegn As String
    Dim ArrFlno As String
    Dim ArrStoa As String
    Dim ArrTifa As String
    Dim ArrTisa As String
    Dim ArrIndx As String
    Dim DepUrno As String
    Dim DepRkey As String
    Dim DepAurn As String
    Dim DepFtyp As String
    Dim DepRegn As String
    Dim DepFlno As String
    Dim DepStod As String
    Dim DepTifd As String
    Dim DepTisd As String
    Dim DepIndx As String
    Dim RotBest As String
    Dim RotStat As String
    Dim RotaRec As String
    Dim tmpData As String
    Dim RotaIdx As String
    Dim ArrFltFields As String
    Dim DepFltFields As String
    Dim ArrRotFields As String
    Dim DepRotFields As String
    Dim ArrFltData As String
    Dim DepFltData As String
    Dim ArrFound As Boolean
    Dim LoadArr As Boolean
    If (TabArrFlightsIdx >= 0) And (TabDepFlightsIdx >= 0) Then
        ConnexData.ResetContent
        ConnexData.ShowHorzScroller False
        ConnexData.Refresh
        ColNo = CLng(GetRealItemNo(TabArrFlightsTab.LogicalFieldList, "'C4'"))
        TabArrFlightsTab.IndexDestroy "RIDX"
        MaxLine = TabArrFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            TabArrFlightsTab.SetColumnValue CurLine, ColNo, ""
        Next
        ColNo = CLng(GetRealItemNo(TabDepFlightsTab.LogicalFieldList, "'C4'"))
        TabDepFlightsTab.IndexDestroy "RIDX"
        MaxLine = TabDepFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            TabDepFlightsTab.SetColumnValue CurLine, ColNo, ""
        Next
        Me.Refresh
        RotLine = -1
        Screen.MousePointer = 11
        MaxLine = TabDepFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            DepFtyp = TabDepFlightsTab.GetFieldValue(CurLine, "FTYP")
            If InStr("TG", DepFtyp) = 0 Then
                DepUrno = TabDepFlightsTab.GetFieldValue(CurLine, "URNO")
                DepRkey = TabDepFlightsTab.GetFieldValue(CurLine, "RKEY")
                DepAurn = TabDepFlightsTab.GetFieldValue(CurLine, "AURN")
                DepRegn = TabDepFlightsTab.GetFieldValue(CurLine, "REGN")
                DepFlno = TabDepFlightsTab.GetFieldValue(CurLine, "FLNO")
                If DepFlno = "" Then DepFlno = TabDepFlightsTab.GetFieldValue(CurLine, "CSGN")
                DepStod = TabDepFlightsTab.GetFieldValue(CurLine, "STOD")
                DepTifd = TabDepFlightsTab.GetFieldValue(CurLine, "TIFD")
                DepTisd = TabDepFlightsTab.GetFieldValue(CurLine, "TISD")
                DepFlno = DepFlno & "/" & Mid(DepStod, 7, 2)
                DepIndx = TabDepFlightsTab.GetFieldValue(CurLine, "'S3'")
                If DepRkey <> DepUrno Then
                    'This is a rotation
                    ArrUrno = GetLookupFieldValues(TabArrFlightsTab, "URNO", DepAurn, "URNO", HitCount, ArrLine)
                    If HitCount > 0 Then
                        ArrFtyp = TabArrFlightsTab.GetFieldValue(ArrLine, "FTYP")
                        ArrRegn = TabArrFlightsTab.GetFieldValue(ArrLine, "REGN")
                        ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "FLNO")
                        If ArrFlno = "" Then ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "CSGN")
                        ArrStoa = TabArrFlightsTab.GetFieldValue(ArrLine, "STOA")
                        ArrRkey = TabArrFlightsTab.GetFieldValue(ArrLine, "RKEY")
                        ArrTifa = TabArrFlightsTab.GetFieldValue(ArrLine, "TIFA")
                        ArrTisa = TabArrFlightsTab.GetFieldValue(ArrLine, "TISA")
                        ArrFlno = ArrFlno & "/" & Mid(ArrStoa, 7, 2)
                        ArrIndx = TabArrFlightsTab.GetFieldValue(ArrLine, "'S3'")
                        If InStr("OA", DepTisd) > 0 Then
                            RotBest = DepTifd
                        ElseIf InStr("O", ArrTisa) > 0 Then
                            RotBest = DepTifd
                        ElseIf InStr("XND", ArrFtyp) > 0 Then
                            RotBest = DepTifd
                        Else
                            RotBest = CedaDateTimeAdd(ArrTifa, 60)
                        End If
                        ArrFound = True
                        LoadArr = False
                    Else
                        'Joined Arrival Flight obviously not loaded
                        'To be handled in a second step of validation
                        ArrFound = False
                        LoadArr = True
                    End If
                Else
                    'Departure Flight not joined to an arrival
                    ArrFound = False
                    LoadArr = False
                End If
                If ArrFound = False Then
                    ArrFtyp = "-"
                    ArrRegn = ""
                    ArrFlno = ""
                    ArrStoa = ""
                    ArrRkey = ""
                    ArrTifa = ""
                    ArrTisa = "-"
                    ArrIndx = ""
                    RotBest = DepTifd
                    RotStat = DepTisd
                    ArrLine = -1
                End If
                If InStr("XND", ArrFtyp) > 0 Then ArrTisa = "X"
                If InStr("XND", DepFtyp) > 0 Then DepTisd = "X"
                RotStat = ArrTisa & DepTisd
                RotLine = RotLine + 1
                RotaIdx = Right("000000" & CStr(RotLine), 6)
                
                'RotaFields = "DRGN,AFLT,DFLT,ARGN,STOA,STOD,AURN,DURN,AKEY,DKEY,TIFA,TIFD,BEST,STAT,RIDX,AIDX,DIDX,GOCX,CONX,SORT"
                RotaRec = "1,"
                RotaRec = RotaRec & DepRegn & "," & ArrFlno & "," & DepFlno & "," & ArrRegn & ","
                RotaRec = RotaRec & ArrStoa & "," & DepStod & "," & ArrUrno & "," & DepUrno & ","
                RotaRec = RotaRec & ArrRkey & "," & DepRkey & "," & ArrTifa & "," & DepTifd & ","
                RotaRec = RotaRec & RotBest & "," & RotStat & ","
                RotaRec = RotaRec & RotaIdx & "," & ArrIndx & "," & DepIndx & ","
                RotaRec = RotaRec & "" & "," & "" & ","
                RotaRec = RotaRec & "" & "," & ""
                ConnexData.InsertTextLine RotaRec, False
                TabDepFlightsTab.SetFieldValues CurLine, "'C4'", RotaIdx
                If ArrLine >= 0 Then
                    If (ArrRkey <> DepRkey) Or (ArrRegn <> DepRegn) Or (ArrUrno <> DepAurn) Then
                        ConnexData.SetLineColor RotLine, vbBlack, vbRed
                    End If
                    TabArrFlightsTab.SetFieldValues ArrLine, "'C4'", RotaIdx
                End If
            Else
                RotaRec = ""
            End If
        Next
        'Transfer single arrival flights
        DepFtyp = "-"
        DepUrno = ""
        DepRkey = ""
        DepAurn = ""
        DepRegn = ""
        DepFlno = ""
        DepStod = ""
        DepTifd = ""
        DepTisd = "-"
        DepFlno = ""
        DepIndx = ""
        MaxLine = TabArrFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            RotaIdx = Trim(TabArrFlightsTab.GetFieldValue(CurLine, "'C4'"))
            If RotaIdx = "" Then
                ArrFtyp = TabDepFlightsTab.GetFieldValue(CurLine, "FTYP")
                If InStr("TG", ArrFtyp) = 0 Then
                    ArrLine = CurLine
                    ArrRegn = TabArrFlightsTab.GetFieldValue(ArrLine, "REGN")
                    ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "FLNO")
                    If ArrFlno = "" Then ArrFlno = TabArrFlightsTab.GetFieldValue(ArrLine, "CSGN")
                    ArrStoa = TabArrFlightsTab.GetFieldValue(ArrLine, "STOA")
                    ArrRkey = TabArrFlightsTab.GetFieldValue(ArrLine, "RKEY")
                    ArrTifa = TabArrFlightsTab.GetFieldValue(ArrLine, "TIFA")
                    ArrFlno = ArrFlno & "/" & Mid(ArrStoa, 7, 2)
                    ArrIndx = TabArrFlightsTab.GetFieldValue(ArrLine, "'S3'")
                    ArrFound = True
                    LoadArr = False
                    If InStr("XND", ArrFtyp) > 0 Then
                        RotBest = CedaDateTimeAdd(ArrTifa, 15)
                        ArrTisa = "X"
                    Else
                        RotBest = CedaDateTimeAdd(ArrTifa, 60)
                    End If
                    RotStat = ArrTisa & DepTisd
                    RotLine = RotLine + 1
                    RotaIdx = Right("000000" & CStr(RotLine), 6)
                    RotaRec = "1,"
                    RotaRec = RotaRec & ArrRegn & "," & ArrFlno & "," & DepFlno & "," & DepRegn & ","
                    RotaRec = RotaRec & ArrStoa & "," & DepStod & "," & ArrUrno & "," & DepUrno & ","
                    RotaRec = RotaRec & ArrRkey & "," & DepRkey & "," & ArrTifa & "," & DepTifd & ","
                    RotaRec = RotaRec & RotBest & "," & RotStat & ","
                    RotaRec = RotaRec & RotaIdx & "," & ArrIndx & "," & DepIndx & ","
                    RotaRec = RotaRec & "" & "," & "" & ","
                    RotaRec = RotaRec & "" & "," & ""
                    ConnexData.InsertTextLine RotaRec, False
                    TabArrFlightsTab.SetFieldValues CurLine, "'C4'", RotaIdx
                End If
            End If
        Next
        ConnexData.AutoSizeColumns
        ConnexData.Refresh
        AdjustSystabPanel
        ConnexData.ShowHorzScroller True
        TabArrFlightsTab.AutoSizeColumns
        TabDepFlightsTab.AutoSizeColumns
        Screen.MousePointer = 0
        Me.Refresh
        ColNo = CLng(GetRealItemNo(ConnexData.LogicalFieldList, "BEST"))
        ConnexData.Sort CStr(ColNo), True, True
        ConnexData.Refresh
        ColNo = CLng(GetRealItemNo(TabArrFlightsTab.LogicalFieldList, "'C4'"))
        TabArrFlightsTab.IndexCreate "RIDX", ColNo
        ColNo = CLng(GetRealItemNo(TabDepFlightsTab.LogicalFieldList, "'C4'"))
        TabDepFlightsTab.IndexCreate "RIDX", ColNo
        ColNo = CLng(GetRealItemNo(ConnexData.LogicalFieldList, "RIDX"))
        ConnexData.IndexCreate "RIDX", ColNo
        If FormIsVisible("StatusChart") = True Then
            StatusChart.GetFlightRotations
        End If
        'RotationData_SendLButtonDblClick -1, -1
    End If
End Sub


