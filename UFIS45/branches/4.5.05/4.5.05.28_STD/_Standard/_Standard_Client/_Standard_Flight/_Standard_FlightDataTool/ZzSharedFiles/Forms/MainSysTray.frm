VERSION 5.00
Begin VB.Form MainSysTray 
   Caption         =   "Main Form in System Tray"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   Begin VB.Menu mnuCAPPopup 
      Caption         =   "CAPPopup"
      Visible         =   0   'False
      Begin VB.Menu mnuCapReload 
         Caption         =   "Reload View"
      End
      Begin VB.Menu mnuCapSaveCharts 
         Caption         =   "Save Charts"
      End
      Begin VB.Menu mnuCapShowMain 
         Caption         =   "Show Main"
      End
      Begin VB.Menu mnuCapHideMain 
         Caption         =   "Hide Main"
      End
      Begin VB.Menu mnuCapCloseMenu 
         Caption         =   "Close Menu"
      End
      Begin VB.Menu mnuCapExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "MainSysTray"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Sub ShowApplInTray(IconIdx As Integer, IconTip As String)
    Me.Icon = MyOwnForm.MyIcons(IconIdx).Picture
    HandleApplInTray "SET", IconTip
End Sub
Public Sub RemoveApplFromTray()
    HandleApplInTray "DEL", ""
End Sub
Public Sub ModifyIconInTray(IconIdx As Integer, IconTip As String)
    Me.Icon = MyOwnForm.MyIcons(IconIdx).Picture
    HandleApplInTray "CHG", IconTip
End Sub
Private Sub HandleApplInTray(ForWhat As String, IconTip As String)
    Dim tmpTip As String
    Select Case ForWhat
        Case "SET"
            If Not ApplIsInTray Then
                Hook Me.hWnd   ' Set up our handler
                tmpTip = IconTip
                If tmpTip = "" Then
                    tmpTip = "UFIS Cappuccino" & vbLf & "Status:"
                End If
                AddIconToTray Me.hWnd, Me.Icon, Me.Icon.Handle, tmpTip
                ApplIsInTray = True
                Me.Hide
            End If
        Case "CHG"
            If ApplIsInTray Then
                ChangeIconInTray Me.Icon, IconTip
            End If
        Case "DEL"
            If ApplIsInTray Then
                Unhook    ' Return event control to windows
                RemoveIconFromTray
                ApplIsInTray = False
            End If
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    Me.Top = Screen.Height + 600
End Sub

' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    Select Case MyTrayMenu
        Case "CAP"
            PopupMenu mnuCAPPopup, vbPopupMenuRightButton
        Case Else
    End Select
End Sub

'Private Sub ShowMe_Click()
'    Unhook    ' Return event control to windows
'    RemoveIconFromTray
'    ApplIsInTray = False
'End Sub

Private Sub mnuCapExit_Click()
    Dim RECT As POINTAPI
    'On Error Resume Next
    GetCursorPos RECT
    MyMsgPosX = CLng(RECT.X) * 15
    MainDialog.MainTimer(3).Tag = "EXIT"
    MainDialog.MainTimer(3).Enabled = True
End Sub
Private Sub mnuCapReload_Click()
    'MsgBox "CAP Reload"
    FilterAutoReset = True
    ChartAutoSaveReset = True
    MainDialog.PushWorkButton "AODB_RELOAD", 1
End Sub
Private Sub mnuCapSaveCharts_Click()
    'MsgBox "CAP Save Charts"
    If Not ChartAutoSaveIsBusy Then
        ChartAutoSaveReset = True
        CreateAllGanttCharts
        SaveGanttChartPictures "TIST", "US"
        ChartAutoSaveReset = True
    End If
End Sub
Private Sub mnuCapShowMain_Click()
    Dim RECT As POINTAPI
    'On Error Resume Next
    If ApplIsHidden Then
        If AllowOpenCappuccino Then
            MainDialog.HandleMyApplReLogin "INIT"
        Else
            GetCursorPos RECT
            MyMsgPosX = CLng(RECT.X) * 15
            If MyMsgBox.CallAskUser(0, MyMsgPosX, 0, "FDT Capture", "Access denied.", "stop", "", UserAnswer) > 0 Then DoNothing
            MyMsgPosX = 0
            MyMsgPosY = 0
        End If
    Else
        MainDialog.WindowState = vbNormal
        MainDialog.Show
    End If
End Sub
Private Sub mnuCapHideMain_Click()
    MainDialog.WindowState = vbNormal
    MinScreenTopPos = Screen.Height + 600
    MainDialog.Top = MinScreenTopPos
    ApplIsHidden = True
End Sub
Private Sub mnuCapCloseMenu_Click()
    'Do nothing
End Sub

