VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MyWebBrowser 
   Caption         =   "UFIS ACDM"
   ClientHeight    =   8100
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13215
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MyWebBrowser.frx":0000
   LinkTopic       =   "UfisWebTool"
   LockControls    =   -1  'True
   ScaleHeight     =   8100
   ScaleWidth      =   13215
   Begin VB.Timer AppTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   20
      Left            =   10230
      Top             =   900
   End
   Begin VB.Timer TcpRcvTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   20
      Left            =   10230
      Top             =   450
   End
   Begin VB.Timer TcpRcvTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   20
      Left            =   10230
      Top             =   0
   End
   Begin VB.Timer TcpMsgTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   20
      Left            =   7830
      Top             =   30
   End
   Begin VB.Timer TcpMsgTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   20
      Left            =   7830
      Top             =   480
   End
   Begin VB.PictureBox SlidePanel 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   2985
      Index           =   0
      Left            =   1290
      ScaleHeight     =   2985
      ScaleWidth      =   690
      TabIndex        =   58
      Top             =   5100
      Visible         =   0   'False
      Width           =   690
      Begin VB.PictureBox ToolPanel 
         Height          =   2115
         Index           =   0
         Left            =   60
         ScaleHeight     =   2055
         ScaleWidth      =   3840
         TabIndex        =   59
         Top             =   60
         Width           =   3900
         Begin VB.CheckBox chkTool 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   1530
            Style           =   1  'Graphical
            TabIndex        =   60
            Tag             =   "releasenotes"
            Top             =   1710
            UseMaskColor    =   -1  'True
            Width           =   900
         End
         Begin VB.Label SlideLabel 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Label1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   1665
            TabIndex        =   61
            Top             =   450
            Width           =   555
         End
      End
      Begin VB.Timer SlideTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   20
         Left            =   0
         Top             =   2490
      End
   End
   Begin VB.PictureBox MainFrame 
      Height          =   4695
      Index           =   3
      Left            =   2580
      ScaleHeight     =   4635
      ScaleWidth      =   10515
      TabIndex        =   54
      Top             =   3030
      Visible         =   0   'False
      Width           =   10575
      Begin VB.CommandButton CfgSplit 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   120
         Index           =   1
         Left            =   0
         TabIndex        =   74
         Top             =   3480
         Width           =   7035
      End
      Begin VB.PictureBox CfgPanel 
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   2
         Left            =   0
         ScaleHeight     =   300
         ScaleWidth      =   10365
         TabIndex        =   71
         Top             =   3720
         Width           =   10365
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   0
            Picture         =   "MyWebBrowser.frx":08CA
            Style           =   1  'Graphical
            TabIndex        =   73
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   330
            Picture         =   "MyWebBrowser.frx":0A14
            Style           =   1  'Graphical
            TabIndex        =   72
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.Label ImpLabel 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FF0000&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Index           =   2
            Left            =   690
            TabIndex        =   77
            Top             =   15
            Width           =   690
         End
         Begin VB.Label ImpLabel 
            BackColor       =   &H0080FF80&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "FLTREC"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   1
            Left            =   1410
            TabIndex        =   76
            Top             =   15
            Width           =   690
         End
      End
      Begin VB.CommandButton CfgSplit 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   120
         Index           =   0
         Left            =   60
         TabIndex        =   64
         Top             =   1650
         Width           =   7035
      End
      Begin VB.PictureBox CfgPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   1
         Left            =   0
         ScaleHeight     =   300
         ScaleWidth      =   10365
         TabIndex        =   63
         Top             =   1860
         Width           =   10365
         Begin VB.PictureBox ChkPanel 
            BackColor       =   &H0080C0FF&
            Height          =   300
            Index           =   1
            Left            =   1410
            ScaleHeight     =   240
            ScaleWidth      =   8595
            TabIndex        =   86
            Top             =   0
            Width           =   8655
            Begin VB.CheckBox ImpIata 
               BackColor       =   &H000000C0&
               Caption         =   "SSIM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   2400
               TabIndex        =   93
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Value           =   1  'Checked
               Width           =   1200
            End
            Begin VB.CheckBox ImpTrig 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   7200
               TabIndex        =   92
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpType 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   6000
               TabIndex        =   91
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpRmrk 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   4800
               TabIndex        =   90
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpSsim 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   3600
               TabIndex        =   89
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpOrig 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   1200
               TabIndex        =   88
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpName 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   240
               TabIndex        =   87
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
         End
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   330
            Picture         =   "MyWebBrowser.frx":0B5E
            Style           =   1  'Graphical
            TabIndex        =   70
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   0
            Picture         =   "MyWebBrowser.frx":0CA8
            Style           =   1  'Graphical
            TabIndex        =   69
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.Label ImpLabel 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FF0000&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Index           =   3
            Left            =   690
            TabIndex        =   94
            Top             =   15
            Width           =   690
         End
      End
      Begin VB.PictureBox CfgPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   0
         Left            =   0
         ScaleHeight     =   300
         ScaleWidth      =   10395
         TabIndex        =   62
         Top             =   0
         Width           =   10395
         Begin VB.PictureBox ChkPanel 
            BackColor       =   &H00FFC0C0&
            Height          =   300
            Index           =   0
            Left            =   1410
            ScaleHeight     =   240
            ScaleWidth      =   8565
            TabIndex        =   78
            Top             =   0
            Width           =   8625
            Begin VB.CheckBox ImpIata 
               BackColor       =   &H000000C0&
               Caption         =   "SSIM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   2400
               TabIndex        =   85
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Value           =   1  'Checked
               Width           =   1200
            End
            Begin VB.CheckBox ImpTrig 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   7200
               TabIndex        =   84
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpType 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   6000
               TabIndex        =   83
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpRmrk 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   4800
               TabIndex        =   82
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpSsim 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   3600
               TabIndex        =   81
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpOrig 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   1200
               TabIndex        =   80
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
            Begin VB.CheckBox ImpName 
               BackColor       =   &H000000C0&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   240
               TabIndex        =   79
               Tag             =   "panic"
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   1200
            End
         End
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   330
            Picture         =   "MyWebBrowser.frx":0DF2
            Style           =   1  'Graphical
            TabIndex        =   68
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CheckBox chkImpTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   0
            Picture         =   "MyWebBrowser.frx":0F3C
            Style           =   1  'Graphical
            TabIndex        =   67
            Tag             =   "panic"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.Label ImpLabel 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FF0000&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Index           =   4
            Left            =   690
            TabIndex        =   95
            Top             =   15
            Width           =   690
         End
      End
      Begin TABLib.TAB CfgTab 
         Height          =   465
         Index           =   0
         Left            =   30
         TabIndex        =   55
         Top             =   420
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin TABLib.TAB CfgTab 
         Height          =   465
         Index           =   1
         Left            =   30
         TabIndex        =   56
         Top             =   2310
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin TABLib.TAB RecTab 
         Height          =   465
         Index           =   0
         Left            =   30
         TabIndex        =   65
         Top             =   960
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin TABLib.TAB RecTab 
         Height          =   465
         Index           =   1
         Left            =   0
         TabIndex        =   66
         Top             =   2910
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin TABLib.TAB RecTab 
         Height          =   465
         Index           =   2
         Left            =   30
         TabIndex        =   75
         Top             =   4080
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   820
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox MainFrame 
      Height          =   1965
      Index           =   2
      Left            =   1380
      ScaleHeight     =   1905
      ScaleWidth      =   1065
      TabIndex        =   47
      Top             =   3030
      Visible         =   0   'False
      Width           =   1125
      Begin TABLib.TAB ImpTab 
         Height          =   1185
         Index           =   0
         Left            =   30
         TabIndex        =   52
         Top             =   60
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   2090
         _StockProps     =   64
      End
      Begin TABLib.TAB ImpTab 
         Height          =   1185
         Index           =   1
         Left            =   30
         TabIndex        =   53
         Top             =   1290
         Width           =   3255
         _Version        =   65536
         _ExtentX        =   5741
         _ExtentY        =   2090
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox MainFrame 
      BorderStyle     =   0  'None
      Height          =   1965
      Index           =   1
      Left            =   60
      ScaleHeight     =   1965
      ScaleWidth      =   1185
      TabIndex        =   46
      Top             =   5400
      Visible         =   0   'False
      Width           =   1185
      Begin SHDocVwCtl.WebBrowser webMain 
         Height          =   1455
         Left            =   0
         TabIndex        =   48
         Top             =   0
         Width           =   2985
         ExtentX         =   5265
         ExtentY         =   2566
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         NoWebView       =   0   'False
         HideFileNames   =   0   'False
         SingleClick     =   0   'False
         SingleSelection =   0   'False
         NoFolders       =   0   'False
         Transparent     =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   "http:///"
      End
   End
   Begin VB.PictureBox MainFrame 
      Height          =   1965
      Index           =   0
      Left            =   60
      ScaleHeight     =   1905
      ScaleWidth      =   1155
      TabIndex        =   45
      Top             =   3030
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   2
      Left            =   60
      ScaleHeight     =   345
      ScaleWidth      =   7695
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   1110
      Visible         =   0   'False
      Width           =   7695
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   7
         Left            =   4500
         Style           =   1  'Graphical
         TabIndex        =   51
         Tag             =   "panic"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Internet"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   3600
         Style           =   1  'Graphical
         TabIndex        =   50
         Tag             =   "panic"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Release"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   5
         Left            =   2700
         Style           =   1  'Graphical
         TabIndex        =   49
         Tag             =   "panic"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Panic"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   5400
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "panic"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Patches"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   41
         Tag             =   "patches"
         ToolTipText     =   "Retrieve TelexPool Patches"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Issues"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   40
         Tag             =   "openissues"
         ToolTipText     =   "Look at Open TelexPool Issues"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkUfisTask 
         Caption         =   "Notes"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   39
         Tag             =   "releasenotes"
         ToolTipText     =   "Open the Release Note of this Version"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   6
      Left            =   8910
      ScaleHeight     =   495
      ScaleWidth      =   1515
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   2130
      Visible         =   0   'False
      Width           =   1515
      Begin VB.CheckBox chkUfis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   1
         Left            =   0
         Picture         =   "MyWebBrowser.frx":1086
         Style           =   1  'Graphical
         TabIndex        =   37
         Tag             =   "exit"
         ToolTipText     =   "Close"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkUfis 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   2
         Left            =   450
         Picture         =   "MyWebBrowser.frx":1610
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Show URL Box"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   7
      Left            =   4800
      ScaleHeight     =   495
      ScaleWidth      =   4005
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   2130
      Visible         =   0   'False
      Width           =   4005
      Begin VB.PictureBox SubPanel 
         BackColor       =   &H0080C0FF&
         BorderStyle     =   0  'None
         Height          =   465
         Index           =   1
         Left            =   3000
         ScaleHeight     =   465
         ScaleWidth      =   945
         TabIndex        =   96
         TabStop         =   0   'False
         Top             =   0
         Width           =   945
         Begin VB.CheckBox chkRdy 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   1
            Left            =   450
            Picture         =   "MyWebBrowser.frx":1B9A
            Style           =   1  'Graphical
            TabIndex        =   102
            Tag             =   "exit"
            ToolTipText     =   "Close"
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   450
         End
         Begin VB.CheckBox chkRdy 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   0
            Left            =   0
            Picture         =   "MyWebBrowser.frx":2124
            Style           =   1  'Graphical
            TabIndex        =   97
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   450
         End
      End
      Begin VB.PictureBox SubPanel 
         BackColor       =   &H0080C0FF&
         BorderStyle     =   0  'None
         Height          =   465
         Index           =   0
         Left            =   0
         ScaleHeight     =   465
         ScaleWidth      =   1575
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   0
         Width           =   1575
         Begin VB.CheckBox chkImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   2
            Left            =   900
            Picture         =   "MyWebBrowser.frx":226E
            Style           =   1  'Graphical
            TabIndex        =   57
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   450
         End
         Begin VB.CheckBox chkImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   0
            Left            =   0
            Picture         =   "MyWebBrowser.frx":23B8
            Style           =   1  'Graphical
            TabIndex        =   34
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   450
         End
         Begin VB.CheckBox chkImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   1
            Left            =   450
            Picture         =   "MyWebBrowser.frx":2942
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   450
         End
      End
      Begin MSComctlLib.ProgressBar ImpBar 
         Height          =   150
         Index           =   0
         Left            =   1710
         TabIndex        =   31
         Top             =   270
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   265
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
      End
      Begin VB.Label ImpLabel 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   1710
         TabIndex        =   30
         Top             =   0
         Width           =   1140
      End
   End
   Begin MSWinsockLib.Winsock MySock 
      Index           =   0
      Left            =   7830
      Top             =   930
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   285
      Index           =   5
      Left            =   60
      ScaleHeight     =   285
      ScaleWidth      =   7695
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   2700
      Visible         =   0   'False
      Width           =   7695
      Begin MSComctlLib.ProgressBar barMain 
         Height          =   150
         Index           =   1
         Left            =   0
         TabIndex        =   27
         Top             =   0
         Visible         =   0   'False
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   265
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   4
      Left            =   60
      ScaleHeight     =   495
      ScaleWidth      =   4665
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   2130
      Visible         =   0   'False
      Width           =   4665
      Begin VB.CheckBox chkDummy 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   3600
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   900
      End
      Begin VB.CheckBox chkAcdm 
         Caption         =   "Weather Forecast"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   1
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   25
         Tag             =   "http://www.wetter.de/wettervorhersage/49-2502-82/wetter-duesseldorf.html"
         ToolTipText     =   "Weather Forecast"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1800
      End
      Begin VB.CheckBox chkAcdm 
         Caption         =   "DUS - Airport Home"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   24
         Tag             =   "http://www.duesseldorf-international.de"
         ToolTipText     =   "Related Airport (DUS)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1800
      End
   End
   Begin VB.PictureBox SplitHorz 
      BorderStyle     =   0  'None
      Height          =   90
      Index           =   0
      Left            =   0
      ScaleHeight     =   90
      ScaleWidth      =   7755
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   930
      Width           =   7755
      Begin VB.Image Image1 
         Height          =   240
         Left            =   30
         Picture         =   "MyWebBrowser.frx":2ECC
         Top             =   30
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.CheckBox chkUfis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   0
      Left            =   6060
      Picture         =   "MyWebBrowser.frx":3016
      Style           =   1  'Graphical
      TabIndex        =   20
      Tag             =   "exit"
      ToolTipText     =   "Close"
      Top             =   0
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.PictureBox RightPanel 
      Height          =   795
      Index           =   0
      Left            =   7050
      ScaleHeight     =   735
      ScaleWidth      =   615
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   0
      Width           =   675
      Begin VB.Image MyBorder 
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "MyWebBrowser.frx":35A0
         Stretch         =   -1  'True
         Top             =   0
         Width           =   690
      End
   End
   Begin VB.CheckBox chkUfis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   3
      Left            =   6540
      Picture         =   "MyWebBrowser.frx":4762
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "exit"
      ToolTipText     =   "Close"
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   450
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   3
      Left            =   60
      ScaleHeight     =   495
      ScaleWidth      =   5565
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1560
      Visible         =   0   'False
      Width           =   5565
      Begin VB.CheckBox chkUrl 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   4950
         Picture         =   "MyWebBrowser.frx":4CEC
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Show URL Box"
         Top             =   0
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.CheckBox chkWeb 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   0
         Picture         =   "MyWebBrowser.frx":5276
         Style           =   1  'Graphical
         TabIndex        =   43
         Tag             =   "exit"
         ToolTipText     =   "Close"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   10
         Left            =   4500
         Picture         =   "MyWebBrowser.frx":5800
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "find"
         ToolTipText     =   "Find on Page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   9
         Left            =   4050
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "print"
         ToolTipText     =   "Print"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   8
         Left            =   3600
         Picture         =   "MyWebBrowser.frx":5D8A
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "pagesetup"
         ToolTipText     =   "Page Setup"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   7
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "preview"
         ToolTipText     =   "Print Preview"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   6
         Left            =   2700
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "search"
         ToolTipText     =   "Search"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   5
         Left            =   2250
         Style           =   1  'Graphical
         TabIndex        =   11
         Tag             =   "home"
         ToolTipText     =   "Home"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   4
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "reload"
         ToolTipText     =   "Refresh"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   3
         Left            =   1350
         Picture         =   "MyWebBrowser.frx":6314
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   "stop"
         ToolTipText     =   "Stop"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   1
         Left            =   450
         Picture         =   "MyWebBrowser.frx":669E
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "back"
         ToolTipText     =   "Back"
         Top             =   0
         Width           =   450
      End
      Begin VB.CheckBox chkTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   2
         Left            =   900
         Picture         =   "MyWebBrowser.frx":67E8
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "forward"
         ToolTipText     =   "Forward"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   1
      Left            =   5700
      ScaleHeight     =   495
      ScaleWidth      =   2055
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1560
      Visible         =   0   'False
      Width           =   2055
      Begin VB.TextBox txtURL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkGo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Index           =   0
         Left            =   1320
         Picture         =   "MyWebBrowser.frx":6932
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin VB.TextBox txtURL 
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   4
         TabStop         =   0   'False
         Text            =   "about:blank"
         Top             =   0
         Width           =   1125
      End
      Begin MSComctlLib.ProgressBar barMain 
         Height          =   120
         Index           =   0
         Left            =   570
         TabIndex        =   17
         Top             =   285
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
      End
      Begin VB.Shape barLabel 
         Height          =   150
         Left            =   90
         Top             =   270
         Width           =   405
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Index           =   0
      Left            =   0
      ScaleHeight     =   720
      ScaleWidth      =   5295
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Width           =   5355
      Begin VB.CheckBox chkUfisTask 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Index           =   0
         Left            =   0
         MaskColor       =   &H00FFFFFF&
         Picture         =   "MyWebBrowser.frx":6EBC
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   "ufishome"
         ToolTipText     =   "UFIS-AS Home Page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   780
      End
   End
   Begin MSComctlLib.StatusBar stbMain 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7815
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   19191
            MinWidth        =   8819
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   5400
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":7AFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":8210
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":8922
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":9034
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":9746
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":9E58
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":A56A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":AE44
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":B4BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":BB38
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyWebBrowser.frx":C232
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB TcpMsgSpooler 
      Height          =   1365
      Index           =   0
      Left            =   8280
      TabIndex        =   98
      Top             =   0
      Visible         =   0   'False
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TcpMsgSpooler 
      Height          =   1365
      Index           =   1
      Left            =   9240
      TabIndex        =   99
      Top             =   0
      Visible         =   0   'False
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TcpRcvSpooler 
      Height          =   1365
      Index           =   0
      Left            =   10680
      TabIndex        =   100
      Top             =   0
      Visible         =   0   'False
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TcpRcvSpooler 
      Height          =   1365
      Index           =   1
      Left            =   11640
      TabIndex        =   101
      Top             =   0
      Visible         =   0   'False
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   2408
      _StockProps     =   64
   End
End
Attribute VB_Name = "MyWebBrowser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AppIsGoingDown As Boolean
Dim MyAppIsBusyNow As Boolean

Dim MainCaption As String
Dim SrvMsgRcv As String
Dim UseImpType As String
Dim UseImpConf As String
Dim UseImpFile As String
Dim UseOutFile As String

Dim WebToolIsConnected As Boolean
Dim IsOkToProceed As Boolean
Dim WebAppIsReadyForUse As Boolean
Dim StartedAsWebTool As Boolean
Dim InfoServerIsConnected As Boolean
Dim CurBarIdx As Integer
Dim CurPanIdx As Integer
Dim CurWrkIdx As Integer
Dim MySockIdx As Integer

Dim DontRefresh As Boolean
Dim WithEvents objDocument As HTMLDocument
Attribute objDocument.VB_VarHelpID = -1

Private Enum BrowserNavConstants
    navOpenInNewWindow = &H1
    navNoHistory = &H2
    navNoReadFromCache = &H4
    navNoWriteToCache = &H8
    navAllowAutosearch = &H10
    navBrowserBar = &H20
    navHyperlink = &H40
End Enum

Private Sub WebNavigate(URL As String, Optional Flags, Optional Target, Optional PostData, Optional Headers)
    Screen.MousePointer = 11
    webMain.Navigate URL, Flags, Target, PostData, Headers
End Sub

Private Function CheckPopup() As Boolean
    On Error GoTo ErrHandle
    If webMain.Document.activeElement.tagName = "BODY" Or _
        webMain.Document.activeElement.tagName = "IFRAME" Then
        CheckPopup = False
    Else
        CheckPopup = True
    End If
    
    Exit Function
    
ErrHandle:
    If Err.Number = 91 Then
        CheckPopup = False
    End If
End Function

'Private Function CheckLock() As Boolean
'On Error Resume Next
'Dim strRestrict As Variant, i As Integer, intCount As Integer, objRange As Object
'strRestrict = Array("amateur", "cock", "penis", "tit", "mature", "anal", "oral", "vaginal", "swallow", "blowjob", "sex", "porn", "hot", "babe")
'
'Set objRange = webMain.Document.body.createTextRange
'
''This error is thrown when no page has been loaded yet and there is no
''body to use the createTextRange method on.
'If Err.Number = 91 Then
'    CheckLock = True
'    Exit Function
'End If
'
''Searching for text in the page
'For i = 0 To UBound(strRestrict)
'    If objRange.findText(strRestrict(i)) = True Then
'        intCount = intCount + 1
'    End If
'Next i
'
'If intCount > 4 Then
'    CheckLock = False
'Else
'    CheckLock = True
'End If
'
'Set objRange = Nothing
'End Function
'
'Private Function CheckVBWM(ByVal URL As String) As Boolean
'If InStr(1, Split(URL, "/")(2), "vbwm.com") = 0 Then
'    CheckVBWM = False
'Else
'    CheckVBWM = True
'End If
'End Function
'
Private Sub CheckCommands()
    If webMain.QueryStatusWB(OLECMDID_PRINT) = 0 Then
        'tlbMain.Buttons.Item("print").Enabled = False
    Else
        'tlbMain.Buttons.Item("print").Enabled = True
    End If
    
    If webMain.QueryStatusWB(OLECMDID_PRINTPREVIEW) = 0 Then
        'tlbMain.Buttons.Item("preview").Enabled = False
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("preview").Enabled = False
    Else
        'tlbMain.Buttons.Item("preview").Enabled = True
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("preview").Enabled = True
    End If
    
    If webMain.QueryStatusWB(OLECMDID_PAGESETUP) = 0 Then
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("page").Enabled = False
    Else
        'tlbMain.Buttons.Item("preview").ButtonMenus.Item("page").Enabled = True
    End If
    
    If webMain.Document Is Nothing Then
        'tlbMain.Buttons.Item("find").Enabled = False
    Else
        'tlbMain.Buttons.Item("find").Enabled = True
    End If
End Sub

Private Sub AppTimer_Timer(Index As Integer)
    If Not MyAppIsBusyNow Then
        AppTimer(Index).Enabled = False
        ShutDownNow
    End If
End Sub

Private Sub ButtonPanel_Resize(Index As Integer)
    Dim NewLeft As Long
    Dim NewSize As Long
    NewSize = ButtonPanel(Index).ScaleWidth
    Select Case Index
        Case 0
            'Main Panel
        Case 1
            'URL Panel
            NewLeft = NewSize - chkGo(0).Width
            chkGo(0).Left = NewLeft
            NewSize = NewLeft
            txtURL(0).Width = NewSize
            txtURL(1).Width = NewSize
            barMain(0).Width = NewSize
            barLabel.Width = NewSize + 30
        Case 2
            'Ufis Functions
        Case 3
            'Web Browser Functions
        Case 4
            'DUS Demo Features
            NewSize = NewSize - chkDummy(0).Left
            If NewSize > 150 Then
                chkDummy(0).Width = NewSize
            End If
        Case 5
            'Progress Bar
        Case 6
            'Ufis Top Buttons
        Case 7
            'Import File Progress
            NewLeft = NewSize - SubPanel(1).Width
            SubPanel(1).Left = NewLeft
            NewSize = NewSize - SubPanel(0).Width - SubPanel(1).Width
            If NewSize >= 60 Then
                ImpLabel(0).Width = NewSize
                ImpBar(0).Width = NewSize
            End If
        Case Else
    End Select
End Sub

Private Sub CfgPanel_Resize(Index As Integer)
    Dim NewLeft As Long
    Dim NewWidth As Long
    Select Case Index
        Case 0
            NewWidth = CfgPanel(Index).ScaleWidth - ChkPanel(Index).Left
            If NewWidth > 120 Then ChkPanel(Index).Width = NewWidth
        Case 1
            NewWidth = CfgPanel(Index).ScaleWidth - ChkPanel(Index).Left
            If NewWidth > 120 Then ChkPanel(Index).Width = NewWidth
        Case 2
            NewWidth = CfgPanel(Index).ScaleWidth - ImpLabel(1).Left
            If NewWidth > 120 Then ImpLabel(1).Width = NewWidth
        Case Else
    End Select
End Sub

Private Sub CfgTab_OnHScroll(Index As Integer, ByVal ColNo As Long)
    RecTab(Index).OnHScrollTo ColNo
End Sub

Private Sub chkAcdm_Click(Index As Integer)
    Dim UrlName As String
    Dim tmpTag As String
    Dim idx As Integer
    If chkAcdm(Index).Value = 1 Then
        chkAcdm(Index).BackColor = LightGreen
        tmpTag = ButtonPanel(4).Tag
        If tmpTag <> "" Then
            idx = Val(tmpTag)
            chkAcdm(idx).Value = 0
        End If
        ButtonPanel(4).Tag = CStr(Index)
        UrlName = chkAcdm(Index).Tag
        If UrlName = "" Then UrlName = "about:blank"
        txtURL(0).Text = UrlName
        CurBarIdx = 1
        CurPanIdx = 5
        chkGo(0).Value = 0
        DoEvents
        chkGo(0).Value = 1
        DoEvents
    Else
        chkAcdm(Index).BackColor = vbButtonFace
        ButtonPanel(4).Tag = ""
        UrlName = "about:blank"
        txtURL(0).Text = UrlName
        stbMain.Panels(2).Text = ""
        CurBarIdx = 1
        CurPanIdx = 5
        chkGo(0).Value = 0
        DoEvents
        chkGo(0).Value = 1
        DoEvents
    End If
End Sub

Private Sub chkGo_Click(Index As Integer)
    Dim tmpData As String
    On Error Resume Next
    If chkGo(Index).Value = 1 Then
        chkGo(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                If Not DontRefresh Then WebNavigate txtURL(0).Text
            Case Else
        End Select
    Else
        chkGo(Index).BackColor = vbButtonFace
        'tmpData = webMain.LocationURL
        'txtURL(0).Text = tmpData
        webMain.SetFocus
    End If
End Sub

Private Sub chkImp_Click(Index As Integer)
    If chkImp(Index).Value = 1 Then
        chkImp(Index).BackColor = LightGreen
        chkImp(Index).Refresh
        Select Case Index
            Case 0
                InitMyButtons "CONF"
                chkImp(Index).Value = 0
            Case 1
                InitMyButtons "FILE"
                chkImp(Index).Value = 0
            Case 2
                'CheckImportTask
                'chkImp(Index).Value = 0
            Case Else
            chkImp(Index).Value = 0
        End Select
    Else
        chkImp(Index).BackColor = vbButtonFace
        chkImp(Index).Refresh
    End If
End Sub
Private Sub CheckImportTask()
    MyAppIsBusyNow = True
    'UseImpType = "D:\Ufis\System\SCORE_ACL_CFG.csv"
    'UseImpConf = "D:\Ufis\Data\ImportTool\LYS\COHOR_LYS_CFG.csv"
    'UseImpFile = "D:\Ufis\Data\ImportTool\LYS\CohorS110725.TXT"
    'UseOutFile = "D:\Ufis\Data\ImportTool\LYS\ScoreS110725.TXT"
    IsOkToProceed = True
    ImpBar(0).Visible = True
    ImpLabel(0).Caption = UseImpFile
    SendMsgToServer 1, "INFO", "Configuration Check"
    CheckConfigAndFile UseImpType, UseImpConf, UseImpFile, UseOutFile
    Me.Refresh
    If IsOkToProceed = True Then
        SendMsgToServer 1, "INFO", "File Conversion"
        HandleFileConversion
        Me.Refresh
    End If
    If IsOkToProceed = True Then
        chkRdy(0).Value = 1
        SendMsgToServer 1, "INFO", "Saving Import File"
        SaveConversionResult
        SendMsgToServer 1, "DONE", UseOutFile
        chkRdy(0).Value = 0
        Me.Refresh
    End If
    MyAppIsBusyNow = False
    ImpBar(0).Visible = False
End Sub

Private Sub SaveConversionResult()
    Dim RecLine As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim fn As Integer
    fn = FreeFile()
    Open UseOutFile For Output As #fn
    MaxLine = ImpTab(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        RecLine = ImpTab(1).GetColumnValue(CurLine, 0)
        Print #fn, RecLine
    Next
    Close #fn
    
End Sub
Private Sub HandleFileConversion()
    Dim ImpSrcRec As String
    Dim ImpTgtRec As String
    Dim SrcPosDat As String
    Dim SrcLenDat As String
    Dim SrcFldDat As String
    Dim SrcNulRec As String
    Dim SrcTrgFld As String
    Dim SrcTrgCod As String
    Dim SrcTrgDat As String
    Dim SrcRotFld As String
    Dim SrcArrFld As String
    Dim SrcDepFld As String
    Dim UseTgtFld As String
    Dim TgtFldDat As String
    Dim TgtNulRec As String
    Dim ImpNulRec As String
    Dim TgtLenDat As String
    
    Dim BarMax As Integer
    Dim BarVal As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RecLine As Long
    Dim CurCol As Long
    Dim MaxSrcCol As Long
    Dim MaxTgtCol As Long
    Dim MaxRecCol As Long
    Dim SrcPosLine As Long
    Dim SrcLenLine As Long
    Dim SrcRotLine As Long
    Dim SrcArrLine As Long
    Dim SrcDepLine As Long
    Dim SrcAdidCol As Long
    
    Dim TgtPosLine As Long
    Dim TgtLenLine As Long
    
    Dim SrcFldPos As Integer
    Dim SrcFldLen As Integer
    Dim TgtFldPos As Integer
    Dim TgtFldLen As Integer
    
    Dim RefreshSteps As Integer
    Dim VisLines As Long
    Dim ScrlLine As Long
    Dim CntDown As Long
    
    Dim FileTypeIsFlight
    RefreshSteps = 50
    VisLines = ((RecTab(2).Height / 15) / RecTab(2).lineHeight) - 3
    
    FileTypeIsFlight = True
    SrcTrgFld = ImpTrig(1).Caption
    SrcTrgCod = "ADID"
    SrcAdidCol = GetRealItemNo(CfgTab(1).LogicalFieldList, SrcTrgFld)
    RecTab(0).ResetContent
    RecTab(1).ResetContent
    RecTab(2).ResetContent
    ImpTab(1).ResetContent
    RecTab(2).ShowHorzScroller False
    RecTab(0).Refresh
    RecTab(1).Refresh
    RecTab(2).Refresh
    ImpTab(1).Refresh
    
    MaxTgtCol = CfgTab(0).GetColumnCount
    TgtNulRec = CreateEmptyRec(CInt(MaxTgtCol))
    MaxSrcCol = CfgTab(1).GetColumnCount
    SrcNulRec = CreateEmptyRec(CInt(MaxSrcCol))
    MaxRecCol = RecTab(2).GetColumnCount
    ImpNulRec = CreateEmptyRec(CInt(MaxRecCol))
    
    MaxLine = ImpTab(0).GetLineCount
    If MaxLine > 30000 Then
        BarMax = 30000
        BarVal = 0
    Else
        BarMax = CInt(MaxLine)
        BarVal = 0
    End If
    ImpBar(0).Max = BarMax
    ImpBar(0).Value = 1
    
    SrcPosLine = 1
    SrcLenLine = 2
    SrcRotLine = 4
    SrcArrLine = 5
    SrcDepLine = 6
    TgtLenLine = 2
    
    
    CntDown = MaxLine
    ImpLabel(2).Caption = CStr(CntDown)
    ImpLabel(2).Refresh
    MaxLine = MaxLine - 1
    For CurLine = 0 To MaxLine
        CntDown = CntDown - 1
        ImpSrcRec = ImpTab(0).GetColumnValue(CurLine, 0)
        If CurLine Mod RefreshSteps = 0 Then
            ImpLabel(2).Caption = CStr(CntDown)
            ImpLabel(2).Refresh
            ImpLabel(1).Caption = ImpSrcRec
            ImpLabel(1).Refresh
            BarVal = BarVal + RefreshSteps
            If BarVal < BarMax Then
                ImpBar(0).Value = BarVal
            End If
        End If
        RecTab(0).ResetContent
        RecTab(0).InsertTextLine TgtNulRec, False
        RecTab(1).ResetContent
        RecTab(1).InsertTextLine SrcNulRec, False
        For CurCol = 1 To MaxSrcCol
            SrcPosDat = Trim(CfgTab(1).GetColumnValue(SrcPosLine, CurCol))
            SrcLenDat = Trim(CfgTab(1).GetColumnValue(SrcLenLine, CurCol))
            If (SrcPosDat <> "") And (SrcLenDat <> "") Then
                SrcFldPos = Val(SrcPosDat)
                SrcFldLen = Val(SrcLenDat)
                SrcFldDat = Mid(ImpSrcRec, SrcFldPos, SrcFldLen)
                RecTab(1).SetColumnValue 0, CurCol, SrcFldDat
            End If
        Next
        RecTab(1).RedrawTab
        For CurCol = 1 To MaxSrcCol
            If FileTypeIsFlight Then
                Select Case SrcTrgCod
                    Case "ADID"
                        SrcRotFld = CfgTab(1).GetColumnValue(SrcRotLine, CurCol)
                        UseTgtFld = Trim(SrcRotFld)
                        If UseTgtFld = "" Then
                            SrcArrFld = ""
                            SrcDepFld = ""
                            SrcTrgDat = RecTab(1).GetColumnValue(0, SrcAdidCol)
                            Select Case SrcTrgDat
                                Case "A"
                                    SrcArrFld = CfgTab(1).GetColumnValue(SrcArrLine, CurCol)
                                    UseTgtFld = Trim(SrcArrFld)
                                Case "D"
                                    SrcDepFld = CfgTab(1).GetColumnValue(SrcDepLine, CurCol)
                                    UseTgtFld = Trim(SrcDepFld)
                                Case Else
                            End Select
                        End If
                        If UseTgtFld <> "" Then
                            SrcFldDat = RecTab(1).GetColumnValue(0, CurCol)
                            RecTab(0).SetFieldValues 0, UseTgtFld, SrcFldDat
                        End If
                    Case Else
                End Select
            Else
            End If
        Next
        RecTab(0).RedrawTab
        RecTab(2).InsertTextLine ImpNulRec, False
        RecLine = RecTab(2).GetLineCount - 1
        For CurCol = 1 To MaxTgtCol
            SrcFldDat = RecTab(0).GetColumnValue(0, CurCol)
            TgtLenDat = CfgTab(0).GetColumnValue(TgtLenLine, CurCol)
            TgtFldLen = Val(TgtLenDat)
            TgtFldDat = Left(SrcFldDat & "             ", TgtFldLen)
            RecTab(2).SetColumnValue RecLine, CurCol - 1, TgtFldDat
        Next
        If RecLine Mod RefreshSteps = 0 Then
            ScrlLine = RecLine - VisLines
            RecTab(2).OnVScrollTo ScrlLine
            RecTab(2).Refresh
            RecTab(1).Refresh
            RecTab(0).Refresh
            DoEvents
        End If
    Next
    ImpLabel(2).Caption = CStr(CntDown)
    ImpLabel(2).Refresh
    ImpLabel(1).Caption = ImpSrcRec
    ImpLabel(1).Refresh
    RecTab(2).OnVScrollTo 0
    RecTab(2).AutoSizeColumns
    RecTab(2).ShowHorzScroller True
    RecTab(2).Refresh
    ImpTab(1).ResetContent
    MaxLine = RecTab(2).GetLineCount - 1
    For CurLine = 0 To MaxLine
        ImpSrcRec = RecTab(2).GetLineValues(CurLine)
        ImpTgtRec = Replace(ImpSrcRec, ",", "", 1, -1, vbBinaryCompare)
        ImpTab(1).InsertTextLine ImpTgtRec, False
    Next
    ImpTab(1).Refresh
End Sub
Private Function CreateEmptyRec(FldCount As Integer) As String
    Dim OutRec As String
    OutRec = String(FldCount - 1, ",")
    CreateEmptyRec = OutRec
End Function
Private Sub CheckConfigAndFile(FileType As String, FileConf As String, FileData As String, FileSave As String)
    Dim TypeCheck As String
    Dim ConfCheck As String
    Dim FileCheck As String
    Dim SaveCheck As String
    Dim PathCheck As String
    
    Dim TypePath As String
    Dim ConfPath As String
    Dim DataPath As String
    Dim SavePath As String
    
    Dim TypeName As String
    Dim ConfName As String
    Dim DataName As String
    Dim SaveName As String
    
    Dim ErrMsgTxt As String
    Dim IsOk As Boolean
    
    IsOk = True
    ErrMsgTxt = ""
    ImpBar(0).Max = 4
    ImpBar(0).Value = 1
    
    GetFileAndPath FileSave, SaveName, SavePath
    If SavePath <> "" Then
        PathCheck = Dir(SavePath, vbDirectory)
        If PathCheck = "" Then
            IsOk = False
            ErrMsgTxt = ErrMsgTxt & "Cannot find import path" & vbNewLine
            ErrMsgTxt = ErrMsgTxt & "(" & SavePath & ")" & vbNewLine
        End If
    Else
        IsOk = False
        ErrMsgTxt = ErrMsgTxt & "Missing import path" & vbNewLine
    End If
    
    If SaveName <> "" Then
        SaveCheck = Dir(FileSave)
        If SaveCheck <> "" Then
            'File already exists
        End If
    Else
        IsOk = False
        ErrMsgTxt = ErrMsgTxt & "Missing import file name" & vbNewLine
    End If
    
    GetFileAndPath FileType, TypeName, TypePath
    If TypeName <> "" Then
        TypeCheck = Dir(FileType)
        If TypeCheck <> "" Then
            InitMyButtons "CONF"
            LoadFileTypeConfig 0, FileType, TypeCheck
        Else
            IsOk = False
            ErrMsgTxt = ErrMsgTxt & "Cannot find Import Type" & vbNewLine
            ErrMsgTxt = ErrMsgTxt & "(" & FileType & ")" & vbNewLine
        End If
    Else
        IsOk = False
        ErrMsgTxt = ErrMsgTxt & "Missing Import Type File" & vbNewLine
    End If
    
    ImpBar(0).Value = 2
    GetFileAndPath FileConf, ConfName, ConfPath
    If ConfName <> "" Then
        ConfCheck = Dir(FileConf)
        If ConfCheck <> "" Then
            InitMyButtons "CONF"
            LoadFileTypeConfig 1, FileConf, ConfCheck
        Else
            IsOk = False
            ErrMsgTxt = ErrMsgTxt & "Cannot find File Description" & vbNewLine
            ErrMsgTxt = ErrMsgTxt & "(" & FileConf & ")" & vbNewLine
        End If
    Else
        IsOk = False
        ErrMsgTxt = ErrMsgTxt & "Missing Import Conf File" & vbNewLine
    End If
    
    ImpBar(0).Value = 3
    GetFileAndPath FileData, DataName, DataPath
    If DataName <> "" Then
        FileCheck = Dir(FileData)
        If FileCheck <> "" Then
            'InitMyButtons "FILE"
        Else
            IsOk = False
            ErrMsgTxt = ErrMsgTxt & "Cannot find Import File" & vbNewLine
            ErrMsgTxt = ErrMsgTxt & "(" & FileData & ")" & vbNewLine
        End If
    Else
        IsOk = False
        ErrMsgTxt = ErrMsgTxt & "Missing Import Data File" & vbNewLine
    End If
    
    ImpBar(0).Value = 4
    IsOkToProceed = IsOk
    If IsOk Then
        InitRecTabs "RECTAB", ""
        ReadImportFile 0, FileData, FileCheck
    Else
        SlideLabel(0).Caption = ErrMsgTxt
        ShowSlidePanel 0, chkImp(2).Left
    End If
End Sub
Private Sub ReadImportFile(Index As Integer, FullPath As String, FileName As String)
    Dim ImpLine As String
    Dim ImpCode As String
    Dim fn As Integer
    ImpTab(Index).ResetContent
    On Error Resume Next
    ImpLabel(0).Caption = FullPath
    fn = FreeFile()
    Open FullPath For Input As #fn
    While Not EOF(fn)
        Line Input #fn, ImpLine
        ImpTab(Index).InsertTextLine ImpLine, False
    Wend
    Close fn
    ImpTab(Index).Refresh
End Sub
Private Sub LoadFileTypeConfig(Index As Integer, FullPath As String, FileName As String)
    Dim CfgLine As String
    Dim CfgCode As String
    Dim FldLst As String
    Dim FldLog As String
    Dim HdrLst As String
    Dim HdrLen As String
    Dim FldLen As String
    Dim FldPos As String
    Dim FldFmt As String
    Dim ColAdj As String
    Dim RefGen As String
    Dim RefArr As String
    Dim RefDep As String
    Dim MainHdrCols As String
    Dim MainHdrTitel As String
    Dim FormatName As String
    Dim tmpData As String
    Dim ColCnt As Long
    Dim CurCol As Long
    Dim k As Integer
    Dim fn As Integer
    On Error Resume Next
    ImpName(Index).Value = 0
    ImpOrig(Index).Value = 0
    ImpSsim(Index).Value = 0
    ImpRmrk(Index).Value = 0
    ImpType(Index).Value = 0
    ImpTrig(Index).Value = 0
    fn = FreeFile()
    Open FullPath For Input As #fn
    While Not EOF(fn)
        Line Input #fn, CfgLine
        CfgLine = Replace(CfgLine, ";", ",", 1, -1, vbBinaryCompare)
        CfgCode = GetItem(CfgLine, 1, ",")
        CfgCode = UCase(CfgCode)
        Select Case CfgCode
            Case "LIST HEADER"
                HdrLst = CfgLine
            Case "FIELD NAME"
                FldLst = CfgLine
            Case "FIELD POSITION"
                FldPos = CfgLine
            Case "FIELD FORMAT"
                FldFmt = CfgLine
            Case "FIELD LENGTH"
                HdrLen = CfgLine
                FldLen = CfgLine
            Case "FIELD REF GEN"
                RefGen = CfgLine
            Case "FIELD REF ARR"
                RefArr = CfgLine
            Case "FIELD REF DEP"
                RefDep = CfgLine
            Case "DESCRIPTIVE NAME"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpName(Index).Caption = tmpData
                ImpName(Index).Value = 1
            Case "ORIGINATOR"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpOrig(Index).Caption = tmpData
                ImpOrig(Index).Value = 1
            Case "SSIM TYPE"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpSsim(Index).Caption = tmpData
                ImpSsim(Index).Value = 1
            Case "SSIM FORMAT"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpRmrk(Index).Caption = tmpData
                ImpRmrk(Index).Value = 1
            Case "RECORD TYPE"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpType(Index).Caption = tmpData
                ImpType(Index).Value = 1
            Case "TYPE INDICATOR"
                tmpData = GetItem(CfgLine, 2, ",")
                ImpTrig(Index).Caption = tmpData
                ImpTrig(Index).Value = 1
            Case Else
        End Select
    Wend
    Close fn
    k = InStr(HdrLen, ",")
    HdrLen = "10" & Mid(HdrLen, k) & ",100"
    HdrLst = HdrLst & ",Remark"
    FldLst = FldLst & ",0000"
    If FldLog = "" Then
        FldLog = FldLst
    End If
    CfgTab(Index).ResetContent
    CfgTab(Index).HeaderString = HdrLst
    CfgTab(Index).HeaderLengthString = HdrLen
    CfgTab(Index).LogicalFieldList = FldLog
    ColCnt = CfgTab(Index).GetColumnCount - 1
    ColAdj = "L,"
    For CurCol = 1 To ColCnt
        ColAdj = ColAdj & "C,"
    Next
    ColAdj = ColAdj & "L"
    CfgTab(Index).ColumnAlignmentString = ColAdj
    CfgTab(Index).HeaderAlignmentString = ColAdj
    MainHdrCols = CStr(ColCnt) & ",1"
    FormatName = GetItem(FileName, 1, ".")
    MainHdrTitel = "Format Definition (" & FormatName & "),Remarks"
    CfgTab(Index).SetMainHeaderValues MainHdrCols, MainHdrTitel, ""
    CfgTab(Index).AutoSizeByHeader = True
    CfgTab(Index).InsertTextLine FldLst, False
    CfgTab(Index).InsertTextLine FldPos, False
    CfgTab(Index).InsertTextLine FldLen, False
    CfgTab(Index).InsertTextLine FldFmt, False
    CfgTab(Index).InsertTextLine RefGen, False
    CfgTab(Index).InsertTextLine RefArr, False
    CfgTab(Index).InsertTextLine RefDep, False
    CfgTab(Index).ShowHorzScroller True
    CfgTab(Index).AutoSizeColumns
    CfgTab(Index).Refresh
    CloneGridLayout RecTab(Index), CfgTab(Index)
    RecTab(Index).ShowVertScroller False
    RecTab(Index).ShowHorzScroller False
    RecTab(Index).Height = RecTab(Index).lineHeight * 15
    RecTab(Index).HeaderLengthString = CfgTab(Index).HeaderLengthString
    RecTab(Index).MainHeaderOnly = True
    RecTab(Index).MainHeader = False
    RecTab(Index).CursorLifeStyle = False
    RecTab(Index).SelectBackColor = vbWhite
    RecTab(Index).SelectTextColor = vbBlack
    RecTab(Index).ResetContent
    RecTab(Index).InsertTextLine ",,,", False
    RecTab(Index).SetCurrentSelection 0
    RecTab(Index).Refresh
    ArrangeCfgPanel
End Sub

Private Sub chkRdy_Click(Index As Integer)
    If chkRdy(Index).Value = 1 Then
        chkRdy(Index).BackColor = LightGreen
        chkRdy(Index).Refresh
        Select Case Index
            Case 0
            Case 1
                Me.WindowState = vbMinimized
                Me.Hide
                chkRdy(Index).Value = 0
            Case Else
            chkRdy(Index).Value = 0
        End Select
    Else
        chkRdy(Index).BackColor = vbButtonFace
        chkRdy(Index).Refresh
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim tmpTag As String
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                'ButtonPanel(2).Visible = True
                'ButtonPanel(1).Visible = False
            Case Else
                tmpTag = chkTask(Index).Tag
                WebMainTask Index, tmpTag
        End Select
    Else
        chkTask(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                'ButtonPanel(1).Visible = True
                'ButtonPanel(2).Visible = False
            Case Else
        End Select
    End If
End Sub
Private Sub WebMainTask(Index As Integer, TagCode As String)
    Dim tmpData As String
    On Error Resume Next
    Select Case TagCode
        Case "back"
            txtURL(0).Text = "Going Back"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            Err.Clear
            webMain.GoBack
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "forward"
            txtURL(0).Text = "Going Forward"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoForward
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "stop"
            txtURL(0).Text = "Stopped"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.Stop
            chkTask(Index).Value = 0
            chkTask(Index).Enabled = False
            chkGo(0).Value = 0
        Case "reload"
            txtURL(0).Text = "Refresh"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.Refresh
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "home"
            txtURL(0).Text = "Going Home"
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoHome
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
        Case "search"
            txtURL(0).Text = "Invoking Web Search"
            Screen.MousePointer = 11
            DontRefresh = True
            chkGo(0).Value = 1
            DontRefresh = False
            webMain.GoSearch
            chkTask(Index).Value = 0
            If Err.Description <> "" Then
                chkTask(Index).Enabled = False
            End If
            Screen.MousePointer = 0
        Case "preview"
            Screen.MousePointer = 11
            webMain.ExecWB OLECMDID_PRINTPREVIEW, OLECMDEXECOPT_DODEFAULT
            chkTask(Index).Value = 0
            Screen.MousePointer = 0
        Case "pagesetup"
            Screen.MousePointer = 11
            webMain.ExecWB OLECMDID_PAGESETUP, OLECMDEXECOPT_DODEFAULT
            chkTask(Index).Value = 0
            Me.Refresh
            Screen.MousePointer = 0
        Case "print"
            webMain.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT
        Case "lock"
            'If Button.Value = tbrPressed Then
            '    tlbMain.Buttons.Item("vbwm").Value = tbrUnpressed
            'Else
            '    tlbMain.Buttons.Item("vbwm").Value = tbrUnpressed
            'End If
        Case "vbwm"
            'If Button.Value = tbrPressed Then
            '    tlbMain.Buttons.Item("lock").Value = tbrUnpressed
            'Else
            '    tlbMain.Buttons.Item("lock").Value = tbrUnpressed
            'End If
        Case "find"
            'frmFind.Show 1
        'Case "ufis"
        '    Me.Caption = "TelexPool Release Notes and Issues"
        '    txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
        '
        '    WebNavigate txtURL(0).Text
        Case Else
    End Select
End Sub

Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 1 Then
        ShowSlidePanel Index, -1
        chkTool(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkUfis_Click(Index As Integer)
    Dim tmpTag As String
    Dim SetEnable As Boolean
    If chkUfis(Index).Value = 1 Then
        chkUfis(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                'Unload Me
                ShutDownApp
            Case 1
                'Me.Hide
                'Unload Me
                ShutDownApp
                'chkUfis(Index).Value = 0
            Case 2
                'ButtonPanel(1).Visible = Not ButtonPanel(1).Visible
                'chkUfis(Index).Value = 0
            Case 3
                'Me.Hide
                'Unload Me
                ShutDownApp
                'chkUfis(Index).Value = 0
            Case Else
                tmpTag = chkUfis(Index).Tag
                WebUfisTask chkUfis(Index), tmpTag
        End Select
    Else
        Select Case Index
            Case 0
                'ButtonPanel(2).Visible = False
                'tmpTag = ButtonPanel(1).Tag
                'ButtonPanel(1).Visible = Val(tmpTag)
                'chkUfis(2).Enabled = True
                'EnableUfisTasks False
            Case 2
                'ButtonPanel(1).Visible = False
            Case Else
        End Select
        chkUfis(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub EnableUfisTasks(SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To chkUfisTask.UBound
        chkUfisTask(i).Enabled = SetEnable
        chkUfisTask(i).Value = 0
    Next
End Sub

Private Sub ToggleUfisTasks(Index As Integer, ForWhat As String)
    Dim i As Integer
    Dim cnt As Integer
    Dim tmpTag As String
    If ForWhat = "DN" Then
        For i = 0 To chkUfisTask.UBound
            If i <> Index Then chkUfisTask(i).Value = 0
        Next
    End If
    If ForWhat = "UP" Then
        cnt = 0
        For i = 0 To chkUfisTask.UBound
            If chkUfisTask(i).Value = 1 Then cnt = cnt + 1
        Next
        If cnt = 0 Then
            Me.Caption = "UFIS ACDM"
            stbMain.Panels(2).Text = ""
            txtURL(1).Visible = False
            tmpTag = txtURL(1).Tag
            txtURL(1).Tag = ""
            If tmpTag <> "" Then
                If tmpTag <> txtURL(0).Text Then
                    txtURL(0).Text = tmpTag
                    chkGo(0).Value = 1
                End If
            End If
        End If
    End If
End Sub

Private Sub WebUfisTask(CurButton As CheckBox, TagCode As String)
    On Error Resume Next
    txtURL(1).Visible = True
    txtURL(1).ZOrder
    If txtURL(1).Tag = "" Then txtURL(1).Tag = txtURL(0).Text
    Select Case TagCode
        Case "ufishome"
            Me.Caption = "UFIS Airport Solutions Home Page"
            txtURL(1).Text = "UFIS-AS Home Page"
            txtURL(0).Text = "http://www.ufis-as.com"
            chkGo(0).Value = 1
        Case "releasenotes"
            Me.Caption = "Telex Pool Release Note"
            txtURL(1).Text = "TelexPool Release Note on Jira"
            txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
            chkGo(0).Value = 1
        Case "openissues"
            Me.Caption = "Open Issues (Telex Pool)"
            txtURL(1).Text = "TelexPool Component on Jira"
            txtURL(0).Text = "http://ufisonline.jira.com/browse/UFIS/component/10080"
            chkGo(0).Value = 1
        Case Else
            txtURL(1).Visible = False
            txtURL(1).Tag = ""
    End Select
End Sub

Private Sub chkUfisTask_Click(Index As Integer)
    Dim tmpTag As String
    If chkUfisTask(Index).Value = 1 Then
        Select Case Index
            Case 5
                InitMyButtons "INIT"
                chkUfisTask(Index).Value = 0
            Case 6
                InitMyButtons "WEB"
                chkUfisTask(Index).Value = 0
            Case 7
                InitMyButtons "FILE"
                chkUfisTask(Index).Value = 0
            Case Else
            chkUfisTask(Index).BackColor = LightGreen
            ToggleUfisTasks Index, "DN"
            tmpTag = chkUfisTask(Index).Tag
            WebUfisTask chkUfisTask(Index), tmpTag
        End Select
    Else
        chkUfisTask(Index).BackColor = vbButtonFace
        Select Case Index
            Case 5
            Case 6
            Case 7
            Case Else
            ToggleUfisTasks Index, "UP"
        End Select
    End If
End Sub

Private Sub chkUrl_Click(Index As Integer)
    If chkUrl(Index).Value = 1 Then
        Select Case Index
            Case 0
                ButtonPanel(1).Visible = Not ButtonPanel(1).Visible
            Case Else
        End Select
    Else
    End If
End Sub

Private Sub Form_Activate()
    If Not WebAppIsReadyForUse Then
        WebAppIsReadyForUse = True
        OpenedFromFips = True
        SetFormOnTop Me, True, True
        'MySock(0).LocalHostName
        MySock(0).LocalPort = "4438"
        MySock(0).Listen
        'chkAcdm(1).Value = 1
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    MainCaption = "UFIS ACDM"
    Me.Width = 700 * 15
    Me.Height = 600 * 15
    CurBarIdx = 0
    CurPanIdx = -1
    CurWrkIdx = -1
    'InfoFromServer(0).Text = ""
    'InfoFromServer(1).Text = ""
    chkUfis(3).Top = 0
    RightPanel(0).Top = chkUfis(3).Height - 30
    MyBorder(0).Height = Screen.Height
    ButtonPanel(0).Top = 0
    ButtonPanel(0).Left = 0
    SplitHorz(0).Left = 0
    SplitHorz(0).Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    NewLeft = 0
    NewTop = SplitHorz(0).Top + SplitHorz(0).Height
    For i = 0 To MainFrame.UBound
        MainFrame(i).Left = NewLeft
        MainFrame(i).Top = NewTop
    Next
    InitMyButtons "INIT"
    
    WebNavigate txtURL(0).Text
    ImpTab(0).Top = 0
    ImpTab(0).Left = 0
    ImpTab(1).Left = 0
    InitImpTabs "BASICS", ""
    ImpTab(0).Height = (ImpTab(0).lineHeight * 15) * 7
    ImpTab(1).Top = ImpTab(0).Top + ImpTab(0).Height

    'CfgPanel(0).BackColor = ImpName(0).BackColor
    'CfgPanel(1).BackColor = ImpName(1).BackColor
    InitCfgPanels "BASICS", ""
    
    InitCfgTabs "BASICS", ""
    
    CfgTab(0).Height = (CfgTab(0).lineHeight * 15) * 7
    CfgTab(1).Height = (CfgTab(1).lineHeight * 15) * 10
    
    'CfgSplit(0).Height = 150
    CfgSplit(0).Left = 0
    CfgSplit(1).Left = 0
    CfgTab(0).Left = 0
    CfgTab(1).Left = 0
    RecTab(0).Left = 0
    RecTab(1).Left = 0
    RecTab(2).Left = 0
    CfgPanel(0).Left = 0
    CfgPanel(1).Left = 0
    CfgPanel(2).Left = 0
    
    ArrangeCfgPanel
    
    InitSlidePanels -1
    InitTcpMsgSpooler 0
    InitTcpMsgSpooler 1
    InitRcvMsgSpooler 0
    InitRcvMsgSpooler 1
        
    HandleCmdLine
    
End Sub
Private Sub HandleCmdLine()
    Dim CurCmdLine
    Dim UsePosData As String
    Dim UseTopData As String
    CurCmdLine = Command$
    GetKeyItem UsePosData, CurCmdLine, "POS=", " "
    If UsePosData = "" Then UsePosData = CStr((Screen.Width - Me.Width) / 2)
    Me.Left = Val(UsePosData)
    GetKeyItem UseTopData, CurCmdLine, "TOP=", " "
    UseTopData = CStr((Screen.Height - Me.Height) / 2)
    Me.Top = Val(UseTopData)
End Sub
Private Sub ArrangeCfgPanel()
    Dim NewTop As Long
    NewTop = 0
    CfgPanel(0).Top = NewTop
    NewTop = NewTop + CfgPanel(0).Height
    CfgTab(0).Top = NewTop
    NewTop = NewTop + CfgTab(0).Height
    RecTab(0).Top = NewTop
    NewTop = NewTop + RecTab(0).Height
    CfgSplit(0).Top = NewTop
    NewTop = NewTop + CfgSplit(0).Height
    CfgPanel(1).Top = NewTop
    NewTop = NewTop + CfgPanel(1).Height
    CfgTab(1).Top = NewTop
    NewTop = NewTop + CfgTab(1).Height
    RecTab(1).Top = NewTop
    NewTop = NewTop + RecTab(1).Height
    CfgSplit(1).Top = NewTop
    NewTop = NewTop + CfgSplit(1).Height
    CfgPanel(2).Top = NewTop
    NewTop = NewTop + CfgPanel(2).Height
    RecTab(2).Top = NewTop
End Sub

Private Sub InitImpTabs(ForWhat As String, Param As String)
    Dim i As Integer
    Dim FldLst As String
    Dim HdrLst As String
    Dim HdrLen As String
    Select Case ForWhat
        Case "BASICS"
            i = 0
            FldLst = "LINE,RMRK"
            HdrLst = "Line Content,Remark"
            HdrLen = "1000,1000"
            ImpTab(i).ResetContent
            ImpTab(i).HeaderString = HdrLst
            ImpTab(i).HeaderLengthString = HdrLen
            ImpTab(i).FontName = "Courier New"
            ImpTab(i).FontSize = 16
            ImpTab(i).HeaderFontSize = 16
            ImpTab(i).SetTabFontBold True
            ImpTab(i).lineHeight = 16
            ImpTab(i).MainHeader = True
            ImpTab(i).SetMainHeaderFont 16, False, False, True, 0, "Courier New"
            ImpTab(i).SetMainHeaderValues "1,1", "Import File Content,Remark", ""
            ImpTab(i).LifeStyle = True
            i = 1
            FldLst = "LINE,RMRK"
            HdrLst = "Lines,Remark"
            HdrLen = "1000,1000"
            ImpTab(i).ResetContent
            ImpTab(i).HeaderString = HdrLst
            ImpTab(i).HeaderLengthString = HdrLen
            ImpTab(i).FontName = "Courier New"
            ImpTab(i).FontSize = 16
            ImpTab(i).HeaderFontSize = 16
            ImpTab(i).SetTabFontBold True
            ImpTab(i).lineHeight = 16
            ImpTab(i).MainHeader = True
            ImpTab(i).SetMainHeaderFont 16, False, False, True, 0, "Courier New"
            ImpTab(i).SetMainHeaderValues "1,1", "SCORE File Content,Remark", ""
            ImpTab(i).LifeStyle = True
        Case Else
    End Select
End Sub
Private Sub InitCfgPanels(ForWhat As String, Param As String)
    Dim BckColor As Long
    Dim i As Integer
    For i = 0 To 1
        ImpName(i).Left = 60
        CfgPanel(i).Height = 300
        ChkPanel(i).Top = -30
        ChkPanel(i).Height = CfgPanel(i).Height + 60
        ChkPanel(i).Width = ImpTrig(i).Left + ImpTrig(i).Width
        CfgPanel(i).BackColor = vbButtonFace
        BckColor = ChkPanel(i).BackColor
        ImpName(i).BackColor = BckColor
        ImpOrig(i).BackColor = BckColor
        ImpIata(i).BackColor = BckColor
        ImpSsim(i).BackColor = BckColor
        ImpRmrk(i).BackColor = BckColor
        ImpType(i).BackColor = BckColor
        ImpTrig(i).BackColor = BckColor
    Next
End Sub
Private Sub InitCfgTabs(ForWhat As String, Param As String)
    Dim i As Integer
    Dim FldLst As String
    Dim HdrLst As String
    Dim HdrLen As String
    Select Case ForWhat
        Case "BASICS"
            i = 0
            FldLst = "LINE,RMRK"
            HdrLst = "LINE,REMARK"
            HdrLen = "1000,1000"
            CfgTab(i).ResetContent
            CfgTab(i).HeaderString = HdrLst
            CfgTab(i).HeaderLengthString = HdrLen
            CfgTab(i).FontName = "Arial"
            CfgTab(i).FontSize = 16
            CfgTab(i).HeaderFontSize = 16
            CfgTab(i).SetTabFontBold True
            CfgTab(i).lineHeight = 16
            CfgTab(i).MainHeader = True
            CfgTab(i).SetMainHeaderFont 16, False, False, True, 0, "Arial"
            CfgTab(i).SetMainHeaderValues "1,1", "SCORE File Content,Remark", ""
            CfgTab(i).LifeStyle = True
            CloneGridLayout RecTab(i), CfgTab(i)
            i = 1
            FldLst = "LINE,RMRK"
            HdrLst = "LINE,REMARK"
            HdrLen = "1000,1000"
            CfgTab(i).ResetContent
            CfgTab(i).HeaderString = HdrLst
            CfgTab(i).HeaderLengthString = HdrLen
            CfgTab(i).FontName = "Arial"
            CfgTab(i).FontSize = 16
            CfgTab(i).HeaderFontSize = 16
            CfgTab(i).SetTabFontBold True
            CfgTab(i).lineHeight = 16
            CfgTab(i).MainHeader = True
            CfgTab(i).SetMainHeaderFont 16, False, False, True, 0, "Arial"
            CfgTab(i).SetMainHeaderValues "1,1", "SCORE File Content,Remark", ""
            CfgTab(i).LifeStyle = True
        Case "RECTAB"
            i = 2
            RecTab(i).ResetContent
            'HdrLst = CfgTab(0).HeaderString
            'HdrLen = CfgTab(0).HeaderLengthString
        Case Else
    End Select
    
End Sub
Private Sub InitRecTabs(ForWhat As String, AnyData As String)
    Dim FldLst As String
    Dim HdrLst As String
    Dim HdrLen As String
    Dim ColAdj As String
    Dim MainHdrCols As String
    Dim MainHdrTitel As String
    Dim tmpData As String
    Dim ColCnt As Integer
    Dim CurCol As Integer
    Dim i As Integer
    i = 2
    RecTab(i).ResetContent
    ColCnt = CInt(CfgTab(0).GetColumnCount) - 2
    FldLst = CleanRecList(CfgTab(0).LogicalFieldList, ColCnt)
    HdrLst = CleanRecList(CfgTab(0).HeaderString, ColCnt)
    HdrLen = CleanRecList(CfgTab(0).HeaderLengthString, ColCnt)
    RecTab(i).HeaderString = FldLst
    RecTab(i).HeaderLengthString = HdrLen
    RecTab(i).LogicalFieldList = FldLst
    RecTab(i).FontName = CfgTab(0).FontName
    RecTab(i).FontSize = CfgTab(0).FontSize
    RecTab(i).SetTabFontBold True
    RecTab(i).lineHeight = CfgTab(0).lineHeight
    RecTab(i).LifeStyle = True
    RecTab(i).CursorLifeStyle = True
    RecTab(i).ShowHorzScroller True
    RecTab(i).AutoSizeByHeader = True
    RecTab(i).AutoSizeColumns
    RecTab(i).Refresh
    MainFrame_Resize 3
End Sub
Private Function CleanRecList(RecLst As String, ColCnt As Integer) As String
    Dim OutLst As String
    Dim TmpLst As String
    Dim tmpDat As String
    Dim k As Integer
    Dim i As Integer
    k = InStr(RecLst, ",")
    If k > 0 Then
        k = k + 1
        TmpLst = Mid(RecLst, k)
    Else
        TmpLst = RecLst
    End If
    OutLst = ""
    For i = 1 To ColCnt
        tmpDat = GetItem(TmpLst, i, ",")
        OutLst = OutLst & tmpDat & ","
    Next
    OutLst = Left(OutLst, Len(OutLst) - 1)
    CleanRecList = OutLst
End Function

Private Sub InitMyButtons(ForWhat As String)
    Dim SetMeUp As String
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim NewWrkIdx As Integer
    Dim i As Integer
    For i = 0 To ButtonPanel.UBound
        ButtonPanel(i).Visible = False
    Next
    Select Case ForWhat
        Case "INIT"
            SetMeUp = "02"
            NewWrkIdx = 0
            MainCaption = "UFIS ACDM Services"
        Case "FILE"
            SetMeUp = "027"
            NewTop = 300
            NewWrkIdx = 2
            MainCaption = "UFIS ACDM Import Manager (File)"
        Case "CONF"
            SetMeUp = "027"
            NewTop = 300
            NewWrkIdx = 3
            MainCaption = "UFIS ACDM Import Manager (Config)"
        Case "WEB"
            SetMeUp = "0231"
            NewTop = 300
            NewWrkIdx = 1
            MainCaption = "UFIS ACDM Web Adapter"
        Case Else
    End Select
    If InStr(SetMeUp, "0") > 0 Then
        ButtonPanel(0).Visible = True
    End If
    If InStr(SetMeUp, "2") > 0 Then
        'UFis Function Buttons
        Set ButtonPanel(2).Container = ButtonPanel(0)
        ButtonPanel(2).Left = chkUfisTask(0).Left + chkUfisTask(0).Width
        ButtonPanel(2).Top = 0
        ButtonPanel(2).Height = 300
        ButtonPanel(2).BackColor = vbButtonFace
        ButtonPanel(2).Visible = True
    End If
    
    If InStr(SetMeUp, "3") > 0 Then
        'Web Browser Function Buttons
        Set ButtonPanel(3).Container = ButtonPanel(0)
        ButtonPanel(3).Visible = True
        If chkUrl(0).Visible Then
            ButtonPanel(3).Width = chkUrl(0).Left + chkUrl(0).Width
        Else
            ButtonPanel(3).Width = chkTask(10).Left + chkTask(10).Width
        End If
        ButtonPanel(3).Left = chkUfisTask(0).Left + chkUfisTask(0).Width
        ButtonPanel(3).Top = NewTop
        ButtonPanel(3).Height = chkTask(1).Height
        ButtonPanel(3).BackColor = vbButtonFace
        chkTask(4).Picture = imlToolbarIcons.ListImages(4).Picture  'reload
        chkTask(5).Picture = imlToolbarIcons.ListImages(5).Picture  'home
        chkTask(6).Picture = imlToolbarIcons.ListImages(6).Picture  'search
        chkTask(7).Picture = imlToolbarIcons.ListImages(9).Picture  'preview
        chkTask(9).Picture = imlToolbarIcons.ListImages(8).Picture  'print
    End If
        
    If InStr(SetMeUp, "1") > 0 Then
        'URL Textbox and Go!
        Set ButtonPanel(1).Container = ButtonPanel(0)
        ButtonPanel(1).Left = ButtonPanel(3).Left + ButtonPanel(3).Width
        ButtonPanel(1).Top = NewTop
        ButtonPanel(1).Height = chkTask(1).Height
        ButtonPanel(1).BackColor = vbButtonFace
        ButtonPanel(1).Visible = True
    End If
    
    
    If InStr(SetMeUp, "4") > 0 Then
        'ACDM Functions
        Set ButtonPanel(4).Container = ButtonPanel(0)
        ButtonPanel(4).Left = chkUfis(2).Left '+ chkUfis(2).Width
        ButtonPanel(4).Top = NewTop
        ButtonPanel(4).Height = chkTask(1).Height
        ButtonPanel(4).BackColor = vbButtonFace
        ButtonPanel(4).Visible = True
    End If
    
    If InStr(SetMeUp, "5") > 0 Then
        'Status Bar
        'Set ButtonPanel(5).Container = ButtonPanel(0)
        ButtonPanel(5).Left = 1020
        ButtonPanel(5).Height = stbMain.Height - 60
        ButtonPanel(5).BackColor = vbButtonFace
        ButtonPanel(5).Visible = False
    End If
    
    If InStr(SetMeUp, "7") > 0 Then
        'Import Tool Functions
        Set ButtonPanel(7).Container = ButtonPanel(0)
        ButtonPanel(7).Left = chkUfisTask(0).Left + chkUfisTask(0).Width
        ButtonPanel(7).Height = chkUfis(1).Height
        ButtonPanel(7).Top = NewTop
        ButtonPanel(7).BackColor = vbButtonFace
        SubPanel(0).Left = 0
        SubPanel(0).Height = ButtonPanel(7).Height
        SubPanel(0).Width = chkImp.Count * chkImp(0).Width
        NewLeft = SubPanel(0).Left + SubPanel(0).Width
        ImpLabel(0).Left = NewLeft
        ImpBar(0).Left = NewLeft
        SubPanel(1).Height = ButtonPanel(7).Height
        SubPanel(1).Width = chkRdy.Count * chkRdy(0).Width
        ButtonPanel(7).Visible = True
    End If
    
    barLabel.Left = -15
    barMain(0).Left = 0
    barMain(0).ZOrder
    barMain(1).Left = 0
    barMain(1).Height = ButtonPanel(5).ScaleHeight - 30
    
    If NewWrkIdx <> CurWrkIdx Then
        If CurWrkIdx >= 0 Then MainFrame(CurWrkIdx).Visible = False
        CurWrkIdx = NewWrkIdx
        MainFrame(CurWrkIdx).Visible = True
    End If
    
    Me.Caption = MainCaption
    
    Form_Resize
    
End Sub
Private Sub Form_Resize()
    Dim tmpTag As String
    Dim NewSize As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    NewSize = Me.ScaleWidth
    NewLeft = NewSize - chkUfis(3).Width
    chkUfis(3).Left = NewLeft
    NewSize = NewLeft
    RightPanel(0).Left = NewLeft
    If NewSize > 600 Then
        ButtonPanel(0).Width = NewSize
        MainFrame(CurWrkIdx).Width = NewSize
        SplitHorz(0).Width = NewSize
        barMain(1).Width = ButtonPanel(5).ScaleWidth
        ButtonPanel(5).Left = (Me.ScaleWidth - ButtonPanel(5).Width) \ 2
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(1).Left
        If NewSize > 900 Then ButtonPanel(1).Width = NewSize
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(2).Left
        If NewSize > 900 Then ButtonPanel(2).Width = NewSize
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(4).Left
        If NewSize > 900 Then ButtonPanel(4).Width = NewSize
        NewSize = ButtonPanel(0).ScaleWidth - ButtonPanel(7).Left
        If NewSize > 900 Then ButtonPanel(7).Width = NewSize
    End If
    NewSize = Me.ScaleHeight - stbMain.Height - RightPanel(0).Top
    If NewSize > 600 Then RightPanel(0).Height = NewSize
    NewTop = Me.ScaleHeight - ButtonPanel(5).Height
    ButtonPanel(5).Top = NewTop
    NewSize = Me.ScaleHeight - stbMain.Height - MainFrame(CurWrkIdx).Top
    If NewSize > 600 Then MainFrame(CurWrkIdx).Height = NewSize
    tmpTag = CStr(Me.Width \ 15) & "/" & CStr(Me.Height \ 15)
    stbMain.Panels(1).Text = tmpTag
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set objDocument = Nothing
End Sub

Private Sub InfoFromServer_Change(Index As Integer)
    Dim tmpText As String
'    Select Case Index
'        Case 0
'            tmpText = InfoFromServer(Index).Text
'            InfoFromServer(Index).Text = ""
'            Select Case tmpText
'                Case "W"
'                    chkAcdm(1).Value = 1
'                Case "A"
'                    chkAcdm(0).Value = 1
'                Case "C"
'                    Unload Me
'                Case Else
'            End Select
'        Case Else
'    End Select
End Sub
Private Sub HandleSrvCommand(GotCmd As String, GotMsg As String)
    Dim tmpData As String
    'MsgBox GotCmd
    Select Case GotCmd
        Case "IMP"
            GetKeyItem UseImpType, GotMsg, "[TYPE]", "[/TYPE]"
            GetKeyItem UseImpConf, GotMsg, "[CONF]", "[/CONF]"
            GetKeyItem UseImpFile, GotMsg, "[FILE]", "[/FILE]"
            GetKeyItem UseOutFile, GotMsg, "[SAVE]", "[/SAVE]"
            chkImp(2).Value = 1
            CheckImportTask
            chkImp(2).Value = 0
        Case "POS"
            tmpData = GotMsg
            Me.Left = Val(tmpData)
            SendMsgToServer 1, "GPS", "ON POS"
        Case "CLS"
            ShutDownApp
        Case "HIDE"
            Me.WindowState = vbMinimized
            Me.Refresh
            Me.Hide
        Case "SHOW"
            Me.Show
            Me.Refresh
            Me.WindowState = vbNormal
        Case "ICON"
            Me.WindowState = vbMinimized
        Case "NORM"
            Me.WindowState = vbNormal
        Case Else
    End Select
End Sub
Private Sub ShutDownApp()
    AppIsGoingDown = True
    AppTimer(0).Enabled = True
End Sub
Private Sub ShutDownNow()
    AppIsGoingDown = True
    Dim i As Integer
    For i = 0 To TcpMsgTimer.UBound
        TcpMsgTimer(i).Enabled = False
    Next
    For i = 0 To TcpRcvTimer.UBound
        TcpRcvTimer(i).Enabled = False
    Next
    MySock(0).Close
    Unload Me
End Sub

Private Sub MainFrame_Resize(Index As Integer)
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    NewWidth = MainFrame(Index).ScaleWidth
    NewHeight = MainFrame(Index).ScaleHeight
    Select Case Index
        Case 0
        Case 1
            If NewWidth > 600 Then webMain.Width = NewWidth
            If NewHeight > 600 Then webMain.Height = NewHeight
        Case 2
            If NewWidth > 600 Then
                ImpTab(0).Width = NewWidth
                ImpTab(1).Width = NewWidth
            End If
            NewHeight = NewHeight - ImpTab(0).Height
            If NewHeight > 600 Then
                ImpTab(1).Height = NewHeight
            End If
        Case 3
            If NewWidth > 600 Then
                CfgTab(0).Width = NewWidth
                CfgTab(1).Width = NewWidth
                RecTab(0).Width = NewWidth
                RecTab(1).Width = NewWidth
                RecTab(2).Width = NewWidth
                CfgSplit(0).Width = NewWidth
                CfgSplit(1).Width = NewWidth
                CfgPanel(0).Width = NewWidth
                CfgPanel(1).Width = NewWidth
                CfgPanel(2).Width = NewWidth
            End If
            NewHeight = NewHeight - RecTab(2).Top
            If NewHeight > 600 Then
                RecTab(2).Height = NewHeight
            End If
        Case Else
    End Select
End Sub

Private Sub MySock_Close(Index As Integer)
    If Index = 1 Then
        'Unload Me
        WebToolIsConnected = False
        ShutDownApp
    End If
End Sub

Private Sub MySock_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    If MySock(Index).State <> sckClosed Then MySock(Index).Close
    MySockIdx = 1
    Load MySock(MySockIdx)
    MySock(MySockIdx).accept requestID
    WebToolIsConnected = True
    SrvMsgRcv = ""
End Sub

Private Sub MySock_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim tmpText As String
    Dim CurMsgRcv As String
    Dim CurMsgCmd As String
    Dim CurMsgDat As String
    Dim SrvMsgLen As Long
    Dim CurMsgLen As Long
    tmpText = ""
    MySock(Index).GetData tmpText, vbString
    SrvMsgRcv = SrvMsgRcv & tmpText
    CurMsgLen = InStr(SrvMsgRcv, "[/DAT]")
    If CurMsgLen > 0 Then
        CurMsgLen = CurMsgLen + 5
        CurMsgRcv = Left(SrvMsgRcv, CurMsgLen)
        GetKeyItem CurMsgCmd, CurMsgRcv, "[CMD=]", "[/CMD]"
        GetKeyItem CurMsgDat, CurMsgRcv, "[DAT=]", "[/DAT]"
        SrvMsgRcv = Mid(SrvMsgRcv, CurMsgLen + 1)
        SpoolRcvMessage Index, CurMsgCmd, CurMsgDat
    End If
End Sub

Private Sub txtURL_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtURL_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        chkGo(0).Value = 1
    End If
End Sub

Private Sub webMain_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    chkTask(3).Enabled = True
    'CheckCommands
End Sub

Private Sub webMain_CommandStateChange(ByVal Command As Long, ByVal Enable As Boolean)
    Select Case Command
        Case 1 'Forward
            'tlbMain.Buttons.Item("forward").Enabled = Enable
            chkTask(2).Enabled = Enable
        Case 2 'Back
            'tlbMain.Buttons.Item("back").Enabled = Enable
            chkTask(1).Enabled = Enable
    End Select
End Sub

Private Sub webMain_DocumentComplete(ByVal pDisp As Object, URL As Variant)
    Dim tmpData As String
    'tlbMain.Buttons.Item("stop").Enabled = False
    chkTask(3).Enabled = False
    If InStr(1, URL, "about:blank") = 0 Then stbMain.Panels.Item(2).Text = webMain.LocationName
    CheckCommands
    chkGo(0).Value = 0
    tmpData = webMain.LocationURL
    txtURL(0).Text = tmpData
    Screen.MousePointer = 0
    'webMain.SetFocus
End Sub

Private Sub webMain_DownloadBegin()
    barMain(CurBarIdx).Visible = True
    If CurPanIdx >= 0 Then
        ButtonPanel(CurPanIdx).Visible = True
    End If
    Form_Resize
    Screen.MousePointer = 11
    Me.Refresh
End Sub

Private Sub webMain_DownloadComplete()
    barMain(CurBarIdx).Visible = False
    If CurPanIdx >= 0 Then
        ButtonPanel(5).Visible = False
    End If
    Form_Resize
    Screen.MousePointer = 0
End Sub

Private Sub webMain_NavigateComplete2(ByVal pDisp As Object, URL As Variant)
    Set objDocument = webMain.Document
    Screen.MousePointer = 0
End Sub

Private Sub webMain_NavigateError(ByVal pDisp As Object, URL As Variant, Frame As Variant, StatusCode As Variant, Cancel As Boolean)
    'It seems that errors will no longer crash the application
    'as it could be observed before in FlightData Tool.
    'MsgBox "error"
End Sub

Private Sub webMain_NewWindow2(ppDisp As Object, Cancel As Boolean)
    'If tlbMain.Buttons.Item("popup").Value = tbrPressed Then
    '    If CheckPopup = False Then
    '        Cancel = True
    '        'stbMain.Panels.Item(3).Text = "A pop-up window has been blocked."
    '    End If
    'End If
End Sub

Private Sub webMain_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
    On Error Resume Next
    barMain(CurBarIdx).Max = ProgressMax
    barMain(CurBarIdx).Value = Progress
End Sub

Private Sub ShowSlidePanel(Index As Integer, SetLeft As Long)
    Dim tmpTag As String
    Dim i As Integer
    SlideTimer(Index).Enabled = False
    tmpTag = SlideTimer(Index).Tag
    If tmpTag = "" Then
        tmpTag = "CLS"
    End If
    If (tmpTag = "CLS") Or (tmpTag = "MVU") Then SlideTimer(Index).Tag = "OPN"
    If (tmpTag = "OPN") Or (tmpTag = "MVD") Then SlideTimer(Index).Tag = "CLS"
    If SetLeft >= 0 Then SlidePanel(Index).Left = SetLeft
    SlideTimer(Index).Enabled = True
End Sub

Private Sub SlideTimer_Timer(Index As Integer)
    Dim tmpTag As String
    Dim CurTop As Long
    Dim NewTop As Long
    Dim MinTop As Long
    Dim MaxTop As Long
    SlideTimer(Index).Enabled = False
    tmpTag = SlideTimer(Index).Tag
    Select Case tmpTag
        Case "OPN"
            If Not SlidePanel(Index).Visible Then
                SlidePanel(Index).Top = SplitHorz(0).Top + SplitHorz(0).Height - SlidePanel(Index).Height
                SlidePanel(Index).Visible = True
            End If
            SlideTimer(Index).Tag = "MVD"
            SlideTimer(Index).Enabled = True
        Case "MVD"
            CurTop = SlidePanel(Index).Top
            MaxTop = SplitHorz(0).Top + SplitHorz(0).Height
            NewTop = CurTop + 150
            If NewTop < MaxTop Then
                SlidePanel(Index).Top = NewTop
                SlideTimer(Index).Enabled = True
            Else
                SlidePanel(Index).Top = MaxTop
                SlideTimer(Index).Tag = "OPN"
                SlideTimer(Index).Enabled = False
            End If
        Case "MVU"
            CurTop = SlidePanel(Index).Top
            MinTop = SplitHorz(0).Top + SplitHorz(0).Height - SlidePanel(Index).Height
            NewTop = CurTop - 150
            If NewTop > MinTop Then
                SlidePanel(Index).Top = NewTop
                SlideTimer(Index).Enabled = True
            Else
                SlidePanel(Index).Top = MinTop
                SlideTimer(Index).Tag = "CLS"
                SlidePanel(Index).Visible = False
                SlideTimer(Index).Enabled = False
            End If
        Case "CLS"
            SlideTimer(Index).Tag = "MVU"
            SlideTimer(Index).Enabled = True
        Case Else
            SlideTimer(Index).Enabled = False
    End Select
End Sub

Private Sub InitSlidePanels(Index As Integer)
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    If Index >= 0 Then
        i1 = Index
        i2 = Index
    Else
        i1 = 0
        i2 = SlidePanel.UBound
    End If
    For i = i1 To i2
        NewLeft = ToolPanel(i).Left
        NewWidth = ToolPanel(i).Width + (NewLeft * 2) + 15
        SlidePanel(i).Width = NewWidth
        NewTop = ToolPanel(i).Top
        NewHeight = ToolPanel(i).Height + (NewTop * 2) + 15
        SlidePanel(i).Height = NewHeight
        SlidePanel(i).ZOrder
    Next
    SetMyZOrder
End Sub

Private Sub SetMyZOrder()
    ButtonPanel(0).ZOrder
    SplitHorz(0).ZOrder
    RightPanel(0).ZOrder
    stbMain.ZOrder
End Sub

Private Sub TcpMsgTimer_Timer(Index As Integer)
    Dim MsgCmd As String
    Dim MsgData As String
    Dim TcpMsg As String
    Dim LineNo As Long
    TcpMsgTimer(Index).Enabled = False
    LineNo = TcpMsgSpooler(Index).GetLineCount
    If WebToolIsConnected Then
        If LineNo > 0 Then
            MsgCmd = TcpMsgSpooler(Index).GetColumnValue(0, 0)
            MsgData = TcpMsgSpooler(Index).GetColumnValue(0, 1)
            TcpMsg = CreateOutMessage(MsgCmd, MsgData)
            MySock(Index).SendData TcpMsg
            TcpMsgSpooler(Index).DeleteLine 0
        End If
        LineNo = TcpMsgSpooler(Index).GetLineCount
    End If
    If LineNo > 0 Then
        TcpMsgTimer(Index).Enabled = True
    End If
End Sub

Private Sub SendMsgToServer(Index As Integer, SendCmd As String, SendMsg As String)
    SpoolOutMessage Index, SendCmd, SendMsg
End Sub
Private Function SpoolOutMessage(Index As Integer, SendCmd As String, SendMsg As String) As Long
    Dim NewRec As String
    Dim LineNo As Long
    NewRec = SendCmd & Chr(16) & SendMsg
    TcpMsgSpooler(Index).InsertTextLine NewRec, False
    LineNo = TcpMsgSpooler(Index).GetLineCount
    TcpMsgTimer(Index).Enabled = True
    SpoolOutMessage = LineNo
End Function

Private Function CreateOutMessage(SendCmd As String, SendMsg As String) As String
    Dim MsgOut As String
    MsgOut = ""
    MsgOut = MsgOut & "[CMD=]" & SendCmd & "[/CMD]"
    MsgOut = MsgOut & "[DAT=]" & SendMsg & "[/DAT]"
    CreateOutMessage = MsgOut
End Function

Private Sub InitTcpMsgSpooler(Index)
    TcpMsgSpooler(Index).ResetContent
    TcpMsgSpooler(Index).HeaderString = "CMD,DATA"
    TcpMsgSpooler(Index).HeaderLengthString = "50,1000"
    TcpMsgSpooler(Index).SetFieldSeparator Chr(16)
End Sub


Private Sub InitRcvMsgSpooler(Index)
    TcpRcvSpooler(Index).ResetContent
    TcpRcvSpooler(Index).HeaderString = "CMD,DATA"
    TcpRcvSpooler(Index).HeaderLengthString = "50,1000"
    TcpRcvSpooler(Index).SetFieldSeparator Chr(16)
End Sub

Private Function SpoolRcvMessage(Index As Integer, SendCmd As String, SendMsg As String) As Long
    Dim NewRec As String
    Dim LineNo As Long
    NewRec = SendCmd & Chr(16) & SendMsg
    TcpRcvSpooler(Index).InsertTextLine NewRec, False
    LineNo = TcpRcvSpooler(Index).GetLineCount
    TcpRcvTimer(Index).Enabled = True
    SpoolRcvMessage = LineNo
End Function

Private Sub TcpRcvTimer_Timer(Index As Integer)
    Dim MsgCmd As String
    Dim MsgData As String
    Dim TcpMsg As String
    Dim LineNo As Long
    TcpRcvTimer(Index).Enabled = False
    LineNo = TcpRcvSpooler(Index).GetLineCount
    If WebToolIsConnected Then
        If LineNo > 0 Then
            MsgCmd = TcpRcvSpooler(Index).GetColumnValue(0, 0)
            MsgData = TcpRcvSpooler(Index).GetColumnValue(0, 1)
            TcpRcvSpooler(Index).DeleteLine 0
            If LineNo > 1 Then TcpRcvTimer(Index).Enabled = True
            HandleSrvCommand MsgCmd, MsgData
        End If
    End If
End Sub




