VERSION 5.00
Begin VB.Form MyPopupMenus 
   Caption         =   "Popup Menus"
   ClientHeight    =   450
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   450
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnuMain 
      Caption         =   "Main"
      Index           =   0
      Begin VB.Menu mnutest 
         Caption         =   "Test"
         Index           =   0
      End
      Begin VB.Menu mnutest 
         Caption         =   "Test 2"
         Index           =   1
      End
      Begin VB.Menu mnutest 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnutest 
         Caption         =   "Test 3"
         Checked         =   -1  'True
         Index           =   3
      End
   End
End
Attribute VB_Name = "MyPopupMenus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    On Error Resume Next
    Shell "c:\ufis\system\UfisDlgRun.exe", vbNormalNoFocus
    'Here we can configure the used menues
End Sub

Public Sub CheckMenuCall(MyForm As Form)
    'Still under construction
    On Error Resume Next
    AppActivate "UfisDlgRun"
    Me.SetFocus
    mnutest(3).Tag = "CHK"
    mnutest(0).Caption = "Hallo"
    PopupMenu mnuMain(0)
End Sub

Private Sub mnuMain_Click(Index As Integer)
    'MsgBox "MAIN"
    'Here we can prepare the called submenue
End Sub

Private Sub mnuTest_Click(Index As Integer)
    'MsgBox "Test: " & Index
    If mnutest(Index).Tag = "CHK" Then
        mnutest(Index).Checked = Not mnutest(Index).Checked
    Else
    '    raiseevent
    End If
End Sub

