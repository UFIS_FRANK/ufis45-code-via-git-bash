VERSION 5.00
Begin VB.Form MyOwnForm 
   Caption         =   "Form1"
   ClientHeight    =   8490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13260
   Icon            =   "MyOwnForm.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8490
   ScaleWidth      =   13260
   StartUpPosition =   3  'Windows Default
   Begin VB.Image MyLogos 
      Height          =   510
      Index           =   1
      Left            =   7080
      Picture         =   "MyOwnForm.frx":08CA
      Top             =   1170
      Width           =   1200
   End
   Begin VB.Image MyLogos 
      Height          =   510
      Index           =   0
      Left            =   7080
      Picture         =   "MyOwnForm.frx":28EC
      Top             =   600
      Width           =   1200
   End
   Begin VB.Image MyPictures 
      Height          =   3450
      Index           =   2
      Left            =   570
      Picture         =   "MyOwnForm.frx":37CE
      Top             =   4080
      Width           =   6105
   End
   Begin VB.Image MyPictures 
      Height          =   480
      Index           =   0
      Left            =   60
      Picture         =   "MyOwnForm.frx":483C0
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyPictures 
      Height          =   3450
      Index           =   1
      Left            =   570
      Picture         =   "MyOwnForm.frx":49002
      Top             =   570
      Width           =   6105
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   0
      Left            =   0
      Picture         =   "MyOwnForm.frx":8DBF4
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   1
      Left            =   540
      Picture         =   "MyOwnForm.frx":8E4BE
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   2
      Left            =   1080
      Picture         =   "MyOwnForm.frx":8ED88
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   3
      Left            =   1620
      Picture         =   "MyOwnForm.frx":8F652
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   4
      Left            =   2160
      Picture         =   "MyOwnForm.frx":8FF1C
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   5
      Left            =   2700
      Picture         =   "MyOwnForm.frx":907E6
      Top             =   0
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   6
      Left            =   3240
      Picture         =   "MyOwnForm.frx":910B0
      Top             =   0
      Width           =   480
   End
End
Attribute VB_Name = "MyOwnForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
