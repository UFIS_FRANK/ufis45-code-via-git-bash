VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form GanttLinesCfg 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormLine"
   ClientHeight    =   2160
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6570
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   6570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton InsertNewLine 
      Caption         =   "Insert &New Line"
      Height          =   285
      Left            =   105
      TabIndex        =   14
      ToolTipText     =   "Inserts a new Line: 1st Tabvalues ,sep 2nd Key for line, 3rd Lineno"
      Top             =   210
      Width           =   1470
   End
   Begin VB.TextBox txtNLTabtext 
      Height          =   270
      Left            =   1680
      TabIndex        =   13
      Text            =   "Will,Smith"
      ToolTipText     =   "Insert table text comma separated"
      Top             =   210
      Width           =   1290
   End
   Begin VB.TextBox txtNLLineKey 
      Height          =   270
      Left            =   3045
      TabIndex        =   12
      Text            =   "0"
      ToolTipText     =   "Insert key for line"
      Top             =   210
      Width           =   555
   End
   Begin VB.TextBox txtNLLinenumber 
      Height          =   270
      Left            =   3675
      TabIndex        =   11
      Text            =   "-1"
      ToolTipText     =   "Insert line number"
      Top             =   210
      Width           =   630
   End
   Begin VB.CommandButton btmDeleteLine 
      Caption         =   "Delete Line"
      Height          =   285
      Left            =   105
      TabIndex        =   10
      Top             =   525
      Width           =   1470
   End
   Begin VB.TextBox txtDeleteLine 
      Height          =   285
      Left            =   1680
      TabIndex        =   9
      ToolTipText     =   "Insert line number to delete"
      Top             =   525
      Width           =   975
   End
   Begin VB.CommandButton SetLineSeparator 
      Caption         =   "SetLineSeparator"
      Height          =   285
      Left            =   105
      TabIndex        =   8
      Top             =   1155
      Width           =   1695
   End
   Begin VB.CommandButton DeleteLineSeparator 
      Caption         =   "DeleteLineSeparator"
      Height          =   285
      Left            =   105
      TabIndex        =   7
      Top             =   1470
      Width           =   1695
   End
   Begin VB.TextBox txtSetLineSeparatorLineNo 
      Height          =   285
      Left            =   1995
      TabIndex        =   6
      Text            =   "0"
      ToolTipText     =   "LineNo"
      Top             =   1155
      Width           =   540
   End
   Begin VB.TextBox txtSetLineSeparatorHeight 
      Height          =   285
      Left            =   5145
      TabIndex        =   5
      Text            =   "1"
      ToolTipText     =   "Height of the Pen"
      Top             =   1155
      Width           =   435
   End
   Begin VB.TextBox txtSetLineSeparatorColor 
      Height          =   285
      Left            =   2625
      TabIndex        =   4
      Text            =   "&HFF"
      ToolTipText     =   "Color"
      Top             =   1155
      Width           =   1170
   End
   Begin VB.TextBox txtSetLineSeparatorPen 
      Height          =   285
      Left            =   3885
      TabIndex        =   3
      Text            =   "1"
      ToolTipText     =   "Pen (0 - 4)"
      Top             =   1155
      Width           =   540
   End
   Begin VB.TextBox txtDeleteLineSeparatorLineNo 
      Height          =   285
      Left            =   1995
      TabIndex        =   2
      Text            =   "0"
      ToolTipText     =   "LineNo"
      Top             =   1470
      Width           =   540
   End
   Begin VB.TextBox txtSetLineSeparatorTotalHeight 
      Height          =   285
      Left            =   4515
      TabIndex        =   1
      Text            =   "8"
      ToolTipText     =   "Total Height"
      Top             =   1155
      Width           =   540
   End
   Begin VB.TextBox txtSetLineSeparatorBegin 
      Height          =   285
      Left            =   5670
      TabIndex        =   0
      Text            =   "3"
      ToolTipText     =   "Space between bottom of line and Pen"
      Top             =   1155
      Width           =   540
   End
   Begin MSComctlLib.Slider Slider1 
      Height          =   255
      Left            =   4620
      TabIndex        =   15
      Top             =   525
      Width           =   1680
      _ExtentX        =   2963
      _ExtentY        =   450
      _Version        =   393216
      Min             =   5
      Max             =   100
      SelStart        =   20
      TickFrequency   =   10
      Value           =   20
   End
   Begin VB.Frame Frame1 
      Height          =   960
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   4425
   End
   Begin VB.Frame Frame2 
      Height          =   960
      Left            =   0
      TabIndex        =   17
      Top             =   945
      Width           =   6420
   End
   Begin VB.Label Label2 
      Caption         =   "Line Height"
      Height          =   225
      Left            =   4830
      TabIndex        =   18
      Top             =   210
      Width           =   960
   End
End
Attribute VB_Name = "GanttLinesCfg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btmDeleteLine_Click()
    Dim llDeleteLine As Long
    If (IsNumeric(txtDeleteLine.Text) = True) Then
        If txtDeleteLine.Text > -1 Then
            llDeleteLine = txtDeleteLine.Text
            GanttConfig.Gantt.DeleteLine (llDeleteLine)
        End If
    End If
    GanttConfig.Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub DeleteLineSeparator_Click()
    GanttConfig.Gantt.DeleteLineSeparator txtDeleteLineSeparatorLineNo.Text
    GanttConfig.Gantt.RefreshArea "GANTT"
End Sub

Private Sub InsertNewLine_Click()
    GanttConfig.Gantt.AddLineAt txtNLLinenumber.Text, txtNLLineKey.Text, txtNLTabtext.Text
    GanttConfig.Gantt.RefreshArea "GANTT"
End Sub
Private Sub SetLineSeparator_Click()
    GanttConfig.Gantt.SetLineSeparator txtSetLineSeparatorLineNo.Text, _
                                 txtSetLineSeparatorHeight.Text, _
                                 txtSetLineSeparatorColor.Text, _
                                 txtSetLineSeparatorPen.Text, _
                                 txtSetLineSeparatorTotalHeight.Text, _
                                 txtSetLineSeparatorBegin.Text
    GanttConfig.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Slider1_Click()
    GanttConfig.Gantt.TabLineHeight = Slider1.SelStart
    GanttConfig.Gantt.RefreshArea "GANTT,TAB"
End Sub
