VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MyConfig 
   Caption         =   "Process Configuration Layout"
   ClientHeight    =   5385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7950
   Icon            =   "MyConfig.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5385
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   285
      Left            =   1290
      TabIndex        =   3
      Top             =   60
      Width           =   2175
   End
   Begin TABLib.TAB VialTab 
      Height          =   1995
      Index           =   0
      Left            =   30
      TabIndex        =   2
      Top             =   2610
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   3519
      _StockProps     =   64
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB CfgTab 
      Height          =   2145
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   360
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   3784
      _StockProps     =   64
   End
End
Attribute VB_Name = "MyConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ViaFldNamLst As String
Dim ViaFldTxtLst As String

Public Sub BreakDownVialList(AftVial As String)
    Dim ViaBuf As String
    Dim EmptyLine As String
    Dim FldNamLst As String
    Dim FldLenLst As String
    Dim FldNam As String
    Dim FldLen As String
    Dim FldVal As String
    Dim DatPos As Long
    Dim DatLen As Long
    Dim ViaRec As String
    Dim AftBufLen As Long
    Dim ViaBufLen As Long
    Dim i As Integer
    VialTab(0).ResetContent
    FldNamLst = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR"
    FldLenLst = "1,3,4,14,14,14,14,14,14,14,14"
    EmptyLine = CreateEmptyLine(FldNamLst) & ","
    ViaBuf = AftVial & Space(120)
    ViaBufLen = Len(ViaBuf)
    AftBufLen = Len(AftVial)
    ViaRec = ""
    i = 0
    DatPos = 1
    While DatPos < ViaBufLen
        i = i + 1
        FldNam = GetItem(FldNamLst, i, ",")
        FldLen = GetItem(FldLenLst, i, ",")
        If FldNam <> "" Then
            DatLen = Val(FldLen)
            FldVal = RTrim(Mid(ViaBuf, DatPos, DatLen))
            ViaRec = ViaRec & FldVal & ","
            DatPos = DatPos + DatLen
        Else
            If ViaRec <> EmptyLine Then
                VialTab(0).InsertTextLine ViaRec, False
                ViaRec = ""
                i = 0
            Else
                DatPos = ViaBufLen
            End If
        End If
    Wend
    VialTab(0).AutoSizeColumns
    VialTab(0).Refresh
End Sub

Public Function TranslateViaField(ViaField As String, ViaApc3 As String) As String
    Dim Result As String
    Dim tmpFld As String
    Dim i As Integer
    Result = ""
    tmpFld = Left(ViaField, 3)
    i = GetRealItemNo(ViaFldNamLst, tmpFld)
    If i >= 0 Then
        Result = Trim(GetRealItem(ViaFldTxtLst, CLng(i), ","))
        Result = Result & Right(ViaField, 1)
        If ViaApc3 <> "" Then Result = Result & " (" & ViaApc3 & ")"
    End If
    TranslateViaField = Result
End Function

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    ViaFldNamLst = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR"
    ViaFldTxtLst = ""
    ViaFldTxtLst = ViaFldTxtLst & "Display of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "IATA 3LC of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ICAO 4LC of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "STA at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ETA at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "LND at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ONB at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "STD at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ETD at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "OFB at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "AIR at VIA" & ","
    InitConfig
End Sub
Private Sub InitConfig()
    CfgTab(0).ResetContent
    CfgTab(0).FontName = "Courier New"
    CfgTab(0).SetTabFontBold True
    CfgTab(0).ShowHorzScroller True
    CfgTab(0).HeaderFontSize = 17
    CfgTab(0).FontSize = 17
    CfgTab(0).LineHeight = 17
    CfgTab(0).EmptyAreaRightColor = vbWhite
    CfgTab(0).HeaderString = "NAME,TYPE,SIZE,MAIN,FILE,VIEW,COMP,ROTA,Meaning,Internal Remark"
    CfgTab(0).HeaderLengthString = "40,40,40,40,40,40,40,40,150,4000"
    CfgTab(0).ColumnWidthString = "4,4,4,4,4,4,4,4,40,100"
    CfgTab(0).ColumnAlignmentString = "L,L,R,R,R,R,R,R,L,L"
    CfgTab(0).InsertTextLine "VPFR,DATE,8,,,,,,Valid Period Begin,", False
    CfgTab(0).InsertTextLine "VPTO,DATE,8,,,,,,Valid Period End,", False
    CfgTab(0).InsertTextLine "DPFR,DATE,8,,,,,,Period Begin First Sector,", False
    CfgTab(0).InsertTextLine "STOD,DATI,12,,,,,,Scheduled Time of Departure,", False
    CfgTab(0).InsertTextLine "STOA,DATI,12,,,,,,Scheduled Time of Arrival,", False
    CfgTab(0).InsertTextLine "SKED,DATI,12,,,,,,Scheduled Time (Arr/Dep),", False
    CfgTab(0).InsertTextLine "FREQ,FREQ,7,,,,,,Any Daily Frequency,", False
    CfgTab(0).InsertTextLine "FRQW,LONG,1,,,,,,Weekly Frequency,", False
    CfgTab(0).InsertTextLine "FREA,FREQ,7,,,,,,Frequency Arrival at Home,", False
    CfgTab(0).InsertTextLine "FRED,FREQ,7,,,,,,Frequency Departure at Home,", False
    CfgTab(0).InsertTextLine "FRQA,FREQ,7,,,,,,Frequency Arrival at any Station,", False
    CfgTab(0).InsertTextLine "FRQD,FREQ,7,,,,,,Frequency Departure at any Station,", False
    CfgTab(0).AutoSizeByHeader = True
    CfgTab(0).AutoSizeColumns
    
    VialTab(0).ResetContent
    VialTab(0).FontName = "Courier New"
    VialTab(0).SetTabFontBold True
    VialTab(0).ShowHorzScroller True
    VialTab(0).HeaderFontSize = 15
    VialTab(0).FontSize = 15
    VialTab(0).LineHeight = 15
    VialTab(0).EmptyAreaRightColor = vbWhite
    VialTab(0).LogicalFieldList = "FLG0,AP30,AP40,STA0,ETA0,LND0,ONB0,STD0,ETD0,OFB0,AIR0,REMA"
    VialTab(0).HeaderString = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR,REM"
    VialTab(0).HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10"
    VialTab(0).ColumnWidthString = "1,3,4,14,14,14,14,14,14,14,14,100"
    VialTab(0).ColumnAlignmentString = "L,L,L,C,C,C,C,C,C,C,C,L"
    VialTab(0).AutoSizeByHeader = True
    VialTab(0).AutoSizeColumns
End Sub

Private Sub Form_Resize()
    'CfgTab(0).Width = Me.ScaleWidth - (2 * CfgTab(0).Left)
    'CfgTab(0).Height = Me.ScaleHeight - CfgTab(0).Top - 30
End Sub
