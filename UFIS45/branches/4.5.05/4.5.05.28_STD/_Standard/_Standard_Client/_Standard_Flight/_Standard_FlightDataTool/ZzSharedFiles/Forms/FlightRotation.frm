VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FlightRotation 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Aircraft Turnaround"
   ClientHeight    =   9705
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11985
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightRotation.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9705
   ScaleWidth      =   11985
   Visible         =   0   'False
   Begin VB.PictureBox MyBitMap 
      BackColor       =   &H00FFFFFF&
      Height          =   435
      Left            =   5550
      ScaleHeight     =   375
      ScaleWidth      =   855
      TabIndex        =   235
      Top             =   8400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Info"
      Height          =   285
      Index           =   13
      Left            =   10485
      Style           =   1  'Graphical
      TabIndex        =   209
      Top             =   45
      Width           =   855
   End
   Begin VB.PictureBox VSplit 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   1035
      Left            =   5940
      ScaleHeight     =   1035
      ScaleWidth      =   90
      TabIndex        =   206
      Top             =   2760
      Visible         =   0   'False
      Width           =   90
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Flights"
      Height          =   285
      Index           =   12
      Left            =   1785
      Style           =   1  'Graphical
      TabIndex        =   205
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Cfg"
      Height          =   285
      Index           =   11
      Left            =   11355
      Style           =   1  'Graphical
      TabIndex        =   204
      Top             =   45
      Width           =   615
   End
   Begin VB.Frame ArrivalData 
      Caption         =   "Arrival Flight"
      Height          =   5415
      Left            =   90
      TabIndex        =   104
      Top             =   1950
      Width           =   5835
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   43
         Left            =   4920
         TabIndex        =   215
         Tag             =   "B1EA,TIME"
         Text            =   "05:00"
         Top             =   3840
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   42
         Left            =   4080
         TabIndex        =   214
         Tag             =   "B1BA,TIME"
         Text            =   "04:50"
         Top             =   3840
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   41
         Left            =   3420
         TabIndex        =   213
         Tag             =   "BLT1"
         Text            =   "B23"
         Top             =   3540
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   40
         Left            =   4920
         TabIndex        =   211
         Tag             =   "B1ES,TIME"
         Text            =   "05:00"
         Top             =   3540
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   39
         Left            =   4080
         TabIndex        =   210
         Tag             =   "B1BS,TIME"
         Text            =   "04:50"
         Top             =   3540
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   38
         Left            =   3240
         TabIndex        =   165
         Tag             =   "AURN"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   37
         Left            =   2220
         TabIndex        =   164
         Tag             =   "RKEY"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   36
         Left            =   1200
         TabIndex        =   163
         Tag             =   "URNO"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox ArrButton 
         Height          =   285
         Index           =   3
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   160
         Top             =   945
         Value           =   1  'Checked
         Width           =   450
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1800
         TabIndex        =   147
         Tag             =   "FLNS"
         Text            =   "M"
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   1230
         TabIndex        =   146
         Tag             =   "FLTN"
         Text            =   "1234"
         Top             =   630
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   600
         TabIndex        =   145
         Tag             =   "FLNO"
         Text            =   "BMW"
         Top             =   630
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   360
         TabIndex        =   144
         Tag             =   "FLTI"
         Text            =   "M"
         Top             =   630
         Width           =   225
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   6
         Left            =   600
         TabIndex        =   143
         Tag             =   "OFBL,TIME,OFBU"
         Text            =   "23:55-1"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   600
         TabIndex        =   142
         Tag             =   "ORG3"
         Text            =   "LHR"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   7
         Left            =   1440
         TabIndex        =   141
         Tag             =   "AIRB,TIME,AIRU"
         Text            =   "00:05"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   8
         Left            =   2340
         TabIndex        =   140
         Tag             =   "VIA3"
         Text            =   "SKG"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   9
         Left            =   2340
         TabIndex        =   139
         Text            =   "03:50"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   10
         Left            =   2340
         TabIndex        =   138
         Text            =   "03:50"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   11
         Left            =   3180
         TabIndex        =   137
         Text            =   "04:00"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   12
         Left            =   4080
         TabIndex        =   136
         Tag             =   "DES3"
         Text            =   "ATH"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   13
         Left            =   4080
         TabIndex        =   135
         Tag             =   "STOA,TIME"
         Text            =   "04:50"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   14
         Left            =   4080
         TabIndex        =   134
         Tag             =   "LAND,TIME,LNDU"
         Text            =   "04:50"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   15
         Left            =   4920
         TabIndex        =   133
         Tag             =   "ONBL,TIME,ONBU"
         Text            =   "05:00"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   16
         Left            =   4080
         TabIndex        =   132
         Tag             =   "PSTA"
         Text            =   "B23"
         Top             =   2460
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   17
         Left            =   4710
         TabIndex        =   131
         Tag             =   "GTA1"
         Text            =   "19"
         Top             =   2460
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   18
         Left            =   600
         TabIndex        =   130
         Text            =   "KLM"
         Top             =   945
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   19
         Left            =   1230
         TabIndex        =   129
         Text            =   "001"
         Top             =   945
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   20
         Left            =   1800
         TabIndex        =   128
         Text            =   " "
         Top             =   945
         Width           =   315
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   21
         Left            =   600
         TabIndex        =   127
         Tag             =   "ETOA,TIME"
         Text            =   "04:50"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   22
         Left            =   4080
         TabIndex        =   126
         Tag             =   "ETAI,TIME,ETAU"
         Text            =   "04:50"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   23
         Left            =   4920
         TabIndex        =   125
         Tag             =   "NXTI,TIME"
         Text            =   "05:00"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   24
         Left            =   4080
         TabIndex        =   124
         Tag             =   "TMOA,TIME,TMAU"
         Text            =   "04:40"
         Top             =   3120
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   25
         Left            =   600
         TabIndex        =   123
         Tag             =   "DCD1"
         Text            =   "83"
         Top             =   3150
         Width           =   465
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   26
         Left            =   1080
         TabIndex        =   122
         Tag             =   "DTD1"
         Text            =   "0003"
         Top             =   3150
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   27
         Left            =   1650
         TabIndex        =   121
         Tag             =   "DCD2"
         Text            =   "ES"
         Top             =   3150
         Width           =   465
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   28
         Left            =   2130
         TabIndex        =   120
         Tag             =   "DTD2"
         Text            =   "0002"
         Top             =   3150
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Height          =   285
         Index           =   29
         Left            =   600
         TabIndex        =   119
         Text            =   "PX40/50/3/NIL"
         Top             =   3450
         Width           =   2085
      End
      Begin VB.TextBox ArrFld 
         Height          =   285
         Index           =   30
         Left            =   600
         TabIndex        =   118
         Top             =   3750
         Width           =   2085
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Telex"
         Height          =   285
         Index           =   0
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   117
         Top             =   4170
         Width           =   855
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Divert"
         Height          =   285
         Index           =   2
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   116
         Top             =   4170
         Width           =   855
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Cancel"
         Height          =   285
         Index           =   1
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   115
         Top             =   4170
         Width           =   855
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   330
         Left            =   90
         TabIndex        =   110
         Top             =   285
         Width           =   3345
         Begin VB.TextBox ArrDay 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   0
            Left            =   30
            Locked          =   -1  'True
            TabIndex        =   114
            Tag             =   "STOA,DATE"
            Text            =   "24.07.2000"
            Top             =   15
            Width           =   975
         End
         Begin VB.TextBox ArrFld 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   33
            Left            =   1290
            TabIndex        =   113
            Tag             =   "TTYP"
            Text            =   "12345"
            Top             =   15
            Width           =   735
         End
         Begin VB.TextBox ArrFld 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   34
            Left            =   1020
            TabIndex        =   112
            Tag             =   "FTYP"
            Text            =   "S"
            Top             =   15
            Width           =   255
         End
         Begin VB.TextBox ArrFld 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   35
            Left            =   2040
            TabIndex        =   111
            Tag             =   "HDLL"
            Text            =   "12345"
            Top             =   15
            Visible         =   0   'False
            Width           =   1290
         End
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   600
         TabIndex        =   109
         Tag             =   "STOD,TIME"
         Text            =   "23:50-1"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Height          =   765
         Index           =   31
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   108
         Tag             =   "REM1"
         Text            =   "FlightRotation.frx":030A
         Top             =   4560
         Width           =   5655
      End
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   107
         Top             =   3870
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   32
         Left            =   120
         TabIndex        =   106
         Tag             =   "ADID"
         Text            =   "A"
         Top             =   630
         Width           =   225
      End
      Begin VB.PictureBox LogoPanel 
         BackColor       =   &H00E0E0E0&
         Height          =   600
         Index           =   0
         Left            =   2130
         ScaleHeight     =   540
         ScaleWidth      =   1230
         TabIndex        =   105
         Top             =   630
         Width           =   1290
         Begin VB.Image FlcLogo 
            Height          =   510
            Index           =   0
            Left            =   15
            Picture         =   "FlightRotation.frx":031B
            Top             =   15
            Width           =   1200
         End
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "FB/LB:"
         Height          =   225
         Index           =   13
         Left            =   3390
         TabIndex        =   216
         Top             =   3870
         Width           =   615
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "Belt:"
         Height          =   225
         Index           =   12
         Left            =   2940
         TabIndex        =   212
         Top             =   3570
         Width           =   405
      End
      Begin VB.Label ArrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "RESTRICTED"
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   3480
         TabIndex        =   172
         Top             =   300
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.Label ArrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "GENERAL AV"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   3480
         TabIndex        =   171
         Top             =   630
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.Label ArrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "CANCELLED"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   3480
         TabIndex        =   169
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "Origin"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   159
         Top             =   1320
         Width           =   585
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "(Home)"
         Height          =   195
         Index           =   2
         Left            =   3990
         TabIndex        =   158
         Top             =   1320
         Width           =   1065
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "Via"
         Height          =   195
         Index           =   1
         Left            =   2460
         TabIndex        =   157
         Top             =   1320
         Width           =   585
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Plan:"
         Height          =   225
         Index           =   3
         Left            =   120
         TabIndex        =   156
         Top             =   1890
         Width           =   495
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Act.:"
         Height          =   225
         Index           =   4
         Left            =   120
         TabIndex        =   155
         Top             =   2190
         Width           =   615
      End
      Begin VB.Label ArrLbl 
         Caption         =   "ETA:"
         Height          =   225
         Index           =   5
         Left            =   120
         TabIndex        =   154
         Top             =   2850
         Width           =   495
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Dly.:"
         Height          =   225
         Index           =   6
         Left            =   120
         TabIndex        =   153
         Top             =   3180
         Width           =   555
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Pos/Gate:"
         Height          =   225
         Index           =   7
         Left            =   3180
         TabIndex        =   152
         Top             =   2490
         Width           =   945
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "EA/NI:"
         Height          =   225
         Index           =   8
         Left            =   3240
         TabIndex        =   151
         Top             =   2850
         Width           =   795
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "TMO:"
         Height          =   225
         Index           =   9
         Left            =   3510
         TabIndex        =   150
         Top             =   3150
         Width           =   525
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Pax:"
         Height          =   225
         Index           =   10
         Left            =   120
         TabIndex        =   149
         Top             =   3480
         Width           =   555
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Bag:"
         Height          =   225
         Index           =   11
         Left            =   120
         TabIndex        =   148
         Top             =   3780
         Width           =   555
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   85
      Top             =   9420
      Width           =   11985
      _ExtentX        =   21140
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15954
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Frame ElseData 
      Caption         =   "Towing Information"
      Height          =   1275
      Left            =   90
      TabIndex        =   68
      Top             =   7710
      Width           =   5835
      Begin VB.PictureBox ChartPanel 
         BackColor       =   &H00E0E0E0&
         Height          =   885
         Index           =   0
         Left            =   3210
         ScaleHeight     =   825
         ScaleWidth      =   2475
         TabIndex        =   173
         Top             =   300
         Width           =   2535
         Begin VB.PictureBox ChartPanel 
            BackColor       =   &H00400000&
            BorderStyle     =   0  'None
            Height          =   825
            Index           =   1
            Left            =   0
            ScaleHeight     =   825
            ScaleWidth      =   2475
            TabIndex        =   230
            Top             =   0
            Visible         =   0   'False
            Width           =   2475
            Begin VB.Label TowOnb 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFF80&
               Caption         =   "B23"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   150
               Left            =   1320
               TabIndex        =   234
               Top             =   570
               Width           =   360
            End
            Begin VB.Label TowOfb 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFF80&
               Caption         =   "B23"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   150
               Left            =   840
               TabIndex        =   233
               Top             =   570
               Width           =   420
            End
            Begin VB.Label DepPos 
               Alignment       =   2  'Center
               BackColor       =   &H0000FF00&
               Caption         =   "B23"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   150
               Left            =   1710
               TabIndex        =   232
               Top             =   300
               Width           =   720
            End
            Begin VB.Label ArrPos 
               Alignment       =   2  'Center
               BackColor       =   &H0000FFFF&
               Caption         =   "B23"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   150
               Left            =   30
               TabIndex        =   231
               Top             =   30
               Width           =   780
            End
         End
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   8
         Left            =   870
         TabIndex        =   92
         Text            =   "23:55-1"
         Top             =   900
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   7
         Left            =   2340
         TabIndex        =   91
         Text            =   "23:55-1"
         Top             =   900
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1710
         TabIndex        =   90
         Text            =   "B23"
         Top             =   900
         Width           =   615
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   6
         Left            =   870
         TabIndex        =   88
         Text            =   "23:55-1"
         Top             =   300
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   2340
         TabIndex        =   87
         Text            =   "23:55-1"
         Top             =   300
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   1710
         TabIndex        =   86
         Text            =   "B23"
         Top             =   300
         Width           =   615
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   1710
         TabIndex        =   71
         Text            =   "B23"
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2340
         TabIndex        =   70
         Text            =   "23:55-1"
         Top             =   600
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   870
         TabIndex        =   69
         Text            =   "23:55-1"
         Top             =   600
         Width           =   825
      End
      Begin VB.Label ElseLbl 
         Caption         =   "Tow-In:"
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   93
         Top             =   930
         Width           =   765
      End
      Begin VB.Label ElseLbl 
         Caption         =   "Tow-Out"
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   89
         Top             =   330
         Width           =   765
      End
      Begin VB.Label ElseLbl 
         Caption         =   "Current:"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   72
         Top             =   630
         Width           =   765
      End
   End
   Begin VB.Frame GeneralData 
      Caption         =   "Communication"
      Height          =   1275
      Left            =   6060
      TabIndex        =   65
      Top             =   7710
      Width           =   5835
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   0
         Left            =   1290
         TabIndex        =   78
         Top             =   300
         Width           =   4455
      End
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   2
         Left            =   1290
         TabIndex        =   67
         Top             =   900
         Width           =   4455
      End
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   1
         Left            =   1290
         TabIndex        =   66
         Top             =   600
         Width           =   4455
      End
      Begin VB.Label RemLbl 
         Caption         =   "SI to send:"
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   75
         Top             =   930
         Width           =   1125
      End
      Begin VB.Label RemLbl 
         Caption         =   "SI Received:"
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   74
         Top             =   630
         Width           =   1125
      End
      Begin VB.Label RemLbl 
         Caption         =   "ACARS Msg:"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   73
         Top             =   330
         Width           =   1065
      End
   End
   Begin VB.Frame DepartureData 
      Caption         =   "Departure Flight"
      Height          =   5415
      Left            =   6060
      TabIndex        =   13
      Top             =   1950
      Width           =   5835
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   23
         Left            =   4050
         TabIndex        =   225
         Tag             =   "BOAO,TIME"
         Text            =   "04:40"
         Top             =   4110
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   22
         Left            =   4890
         TabIndex        =   224
         Tag             =   "FCAL,TIME"
         Text            =   "04:50"
         Top             =   4110
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   21
         Left            =   4050
         TabIndex        =   223
         Tag             =   "REMP"
         Text            =   "19"
         Top             =   3180
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   20
         Left            =   4050
         TabIndex        =   221
         Tag             =   "GD1X,TIME"
         Text            =   "04:40"
         Top             =   3810
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   19
         Left            =   4890
         TabIndex        =   220
         Tag             =   "GD1Y,TIME"
         Text            =   "04:50"
         Top             =   3810
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   18
         Left            =   4050
         TabIndex        =   219
         Tag             =   "GD1B,TIME"
         Text            =   "04:40"
         Top             =   3510
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   17
         Left            =   4890
         TabIndex        =   218
         Tag             =   "GD1E,TIME"
         Text            =   "04:50"
         Top             =   3510
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   16
         Left            =   3390
         TabIndex        =   217
         Tag             =   "GTD1"
         Text            =   "19"
         Top             =   3510
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   15
         Left            =   4890
         TabIndex        =   170
         Tag             =   "STOA,TIME"
         Text            =   "03:50"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   14
         Left            =   3390
         TabIndex        =   168
         Tag             =   "AURN"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   13
         Left            =   2370
         TabIndex        =   167
         Tag             =   "RKEY"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   12
         Left            =   1350
         TabIndex        =   166
         Tag             =   "URNO"
         Text            =   "1234567890"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   11
         Left            =   4890
         TabIndex        =   162
         Tag             =   "ETAI,TIME,ETAU"
         Text            =   "03:50"
         Top             =   2160
         Width           =   825
      End
      Begin VB.CheckBox DepButton 
         Height          =   285
         Index           =   3
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   161
         Top             =   945
         Value           =   1  'Checked
         Width           =   450
      End
      Begin VB.PictureBox LogoPanel 
         BackColor       =   &H00E0E0E0&
         Height          =   600
         Index           =   1
         Left            =   2130
         ScaleHeight     =   540
         ScaleWidth      =   1230
         TabIndex        =   81
         Top             =   630
         Width           =   1290
         Begin VB.Image FlcLogo 
            Height          =   510
            Index           =   1
            Left            =   15
            Top             =   15
            Width           =   1200
         End
      End
      Begin VB.TextBox DepFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   7
         Left            =   360
         TabIndex        =   80
         Tag             =   "FLTI"
         Text            =   "M"
         Top             =   630
         Width           =   225
      End
      Begin VB.TextBox DepFld 
         Height          =   765
         Index           =   6
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   79
         Tag             =   "REM1"
         Top             =   4560
         Width           =   5655
      End
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   77
         Top             =   3870
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Transfer"
         Height          =   285
         Index           =   2
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   4170
         Width           =   855
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   1770
         TabIndex        =   60
         Tag             =   "BAAA"
         Text            =   "0045/0060H"
         Top             =   1560
         Width           =   1245
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   1860
         TabIndex        =   59
         Tag             =   "PAX3"
         Text            =   "19"
         Top             =   3450
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1230
         TabIndex        =   58
         Tag             =   "PAX2"
         Text            =   "19"
         Top             =   3450
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   600
         TabIndex        =   57
         Tag             =   "PAX1"
         Text            =   "19"
         Top             =   3450
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   4380
         TabIndex        =   56
         Tag             =   "CTOT,TIME"
         Text            =   "05:00"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   61
         Left            =   1800
         TabIndex        =   43
         Tag             =   "FLNS"
         Text            =   "M"
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   60
         Left            =   1230
         TabIndex        =   42
         Tag             =   "FLTN"
         Text            =   "1234"
         Top             =   630
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   59
         Left            =   600
         TabIndex        =   41
         Tag             =   "FLNO"
         Text            =   "BMW"
         Top             =   630
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   40
         Tag             =   "ADID"
         Text            =   "D"
         Top             =   630
         Width           =   225
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   57
         Left            =   600
         TabIndex        =   39
         Tag             =   "STOD,TIME"
         Text            =   "06:00"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   56
         Left            =   600
         TabIndex        =   38
         Tag             =   "OFBL,TIME,OFBU"
         Text            =   "06:00"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   55
         Left            =   600
         TabIndex        =   37
         Tag             =   "ORG3"
         Text            =   "ATH"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   54
         Left            =   1440
         TabIndex        =   36
         Tag             =   "AIRB,TIME,AIRU"
         Text            =   "06:05"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   53
         Left            =   3540
         TabIndex        =   35
         Tag             =   "VIA3"
         Text            =   "SKG"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   52
         Left            =   3540
         TabIndex        =   34
         Text            =   "03:50"
         Top             =   1860
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   51
         Left            =   3540
         TabIndex        =   33
         Text            =   "03:50"
         Top             =   2160
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   49
         Left            =   4890
         TabIndex        =   32
         Tag             =   "DES3"
         Text            =   "LHR"
         Top             =   1560
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   45
         Left            =   600
         TabIndex        =   31
         Tag             =   "PSTD"
         Text            =   "B23"
         Top             =   2460
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   44
         Left            =   1230
         TabIndex        =   30
         Tag             =   "GTD1"
         Text            =   "19"
         Top             =   2460
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   43
         Left            =   600
         TabIndex        =   29
         Text            =   "KLM"
         Top             =   945
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   42
         Left            =   1230
         TabIndex        =   28
         Text            =   "001"
         Top             =   945
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   41
         Left            =   1800
         TabIndex        =   27
         Text            =   " "
         Top             =   945
         Width           =   315
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   40
         Left            =   600
         TabIndex        =   26
         Tag             =   "ETOD,TIME"
         Text            =   "04:50"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   39
         Left            =   2700
         TabIndex        =   25
         Tag             =   "NXTI,TIME"
         Text            =   "04:50"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   38
         Left            =   3540
         TabIndex        =   24
         Tag             =   "SLOT,TIME,SLOU"
         Text            =   "05:00"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   37
         Left            =   1860
         TabIndex        =   23
         Tag             =   "ETDI,TIME,ETDU"
         Text            =   "04:40"
         Top             =   2820
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   36
         Left            =   600
         TabIndex        =   22
         Tag             =   "DCD1"
         Text            =   "83"
         Top             =   3150
         Width           =   465
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   35
         Left            =   1080
         TabIndex        =   21
         Tag             =   "DTD1"
         Text            =   "0003"
         Top             =   3150
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   34
         Left            =   1650
         TabIndex        =   20
         Tag             =   "DCD2"
         Text            =   "ES"
         Top             =   3150
         Width           =   465
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   33
         Left            =   2130
         TabIndex        =   19
         Tag             =   "DTD2"
         Text            =   "0002"
         Top             =   3150
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Height          =   285
         Index           =   31
         Left            =   600
         TabIndex        =   18
         Tag             =   "BAGN"
         Top             =   3750
         Width           =   615
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Telex"
         Height          =   285
         Index           =   0
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   4170
         Width           =   855
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Cancel"
         Height          =   285
         Index           =   1
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   4170
         Width           =   855
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   330
         Left            =   90
         TabIndex        =   14
         Top             =   285
         Width           =   3345
         Begin VB.TextBox DepFld 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   10
            Left            =   2040
            TabIndex        =   84
            Tag             =   "HDLL"
            Text            =   "12345"
            Top             =   15
            Width           =   1290
         End
         Begin VB.TextBox DepFld 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   9
            Left            =   1290
            TabIndex        =   83
            Tag             =   "TTYP"
            Text            =   "12345"
            Top             =   15
            Width           =   735
         End
         Begin VB.TextBox DepFld 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   8
            Left            =   1020
            TabIndex        =   82
            Tag             =   "FTYP"
            Text            =   "S"
            Top             =   15
            Width           =   255
         End
         Begin VB.TextBox DepDay 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   0
            Left            =   30
            Locked          =   -1  'True
            TabIndex        =   15
            Tag             =   "STOD,DATE"
            Text            =   "24.07.2000"
            Top             =   15
            Width           =   975
         End
      End
      Begin VB.Label CurFlag 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   4680
         TabIndex        =   229
         Top             =   3180
         Width           =   1035
      End
      Begin VB.Label DepLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "Status:"
         Height          =   225
         Index           =   7
         Left            =   3390
         TabIndex        =   228
         Top             =   3210
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "GO/GC:"
         Height          =   225
         Index           =   6
         Left            =   3420
         TabIndex        =   227
         Top             =   3840
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "BO/FC:"
         Height          =   225
         Index           =   5
         Left            =   3300
         TabIndex        =   226
         Top             =   4140
         Width           =   705
      End
      Begin VB.Label DepLbl 
         Caption         =   "Gate:"
         Height          =   225
         Index           =   4
         Left            =   2910
         TabIndex        =   222
         Top             =   3540
         Width           =   495
      End
      Begin VB.Label DepFlag 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "CANCELLED"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   208
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.Label DepFlag 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "GENERAL AV"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   3480
         TabIndex        =   207
         Top             =   630
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.Label CurFlag 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   3030
         TabIndex        =   175
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label CurFlag 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   4380
         TabIndex        =   174
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Flight Time"
         Height          =   225
         Index           =   3
         Left            =   1800
         TabIndex        =   64
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label DepLbl 
         Caption         =   "CTOT: A"
         Height          =   225
         Index           =   2
         Left            =   4470
         TabIndex        =   63
         Top             =   2610
         Width           =   705
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Slot"
         Height          =   225
         Index           =   1
         Left            =   3660
         TabIndex        =   62
         Top             =   2610
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "EA:"
         Height          =   225
         Index           =   0
         Left            =   3210
         TabIndex        =   61
         Top             =   2190
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "(Home)"
         Height          =   195
         Index           =   23
         Left            =   720
         TabIndex        =   55
         Top             =   1320
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Dest."
         Height          =   195
         Index           =   22
         Left            =   5070
         TabIndex        =   54
         Top             =   1320
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Via"
         Height          =   195
         Index           =   21
         Left            =   3660
         TabIndex        =   53
         Top             =   1320
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Caption         =   "Plan:"
         Height          =   225
         Index           =   20
         Left            =   120
         TabIndex        =   52
         Top             =   1890
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Caption         =   "Act.:"
         Height          =   225
         Index           =   19
         Left            =   120
         TabIndex        =   51
         Top             =   2190
         Width           =   615
      End
      Begin VB.Label DepLbl 
         Caption         =   "ETD:"
         Height          =   225
         Index           =   18
         Left            =   120
         TabIndex        =   50
         Top             =   2850
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Caption         =   "Dly.:"
         Height          =   225
         Index           =   17
         Left            =   120
         TabIndex        =   49
         Top             =   3180
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "P/G:"
         Height          =   225
         Index           =   16
         Left            =   120
         TabIndex        =   48
         Top             =   2490
         Width           =   525
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "ED"
         Height          =   225
         Index           =   15
         Left            =   2040
         TabIndex        =   47
         Top             =   2610
         Width           =   525
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "NI"
         Height          =   225
         Index           =   14
         Left            =   2820
         TabIndex        =   46
         Top             =   2610
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "Pax:"
         Height          =   225
         Index           =   13
         Left            =   120
         TabIndex        =   45
         Top             =   3480
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "Bag:"
         Height          =   225
         Index           =   12
         Left            =   120
         TabIndex        =   44
         Top             =   3780
         Width           =   555
      End
   End
   Begin VB.Frame AircraftData 
      Caption         =   " Aircraft"
      Height          =   945
      Left            =   90
      TabIndex        =   11
      Top             =   675
      Width           =   11805
      Begin VB.TextBox AcrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   1440
         TabIndex        =   94
         Text            =   "DATA LINK"
         Top             =   555
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   6
         Left            =   5400
         TabIndex        =   103
         Tag             =   "ANNX"
         Text            =   "12"
         Top             =   555
         Width           =   345
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   4770
         TabIndex        =   102
         Tag             =   "ENTY"
         Text            =   "12345"
         Top             =   555
         Width           =   615
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   4080
         TabIndex        =   101
         Tag             =   "ACTI"
         Text            =   "B747X"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   3390
         TabIndex        =   100
         Tag             =   "ACT5"
         Text            =   "B747Z"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2850
         TabIndex        =   99
         Tag             =   ",NAME"
         Text            =   "Eleftherios Venizelos"
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   2850
         TabIndex        =   98
         Tag             =   "ACT3"
         Text            =   "W74"
         Top             =   555
         Width           =   525
      End
      Begin VB.Frame AcrSign 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   1440
         TabIndex        =   96
         Top             =   555
         Width           =   1365
         Begin VB.TextBox AcrFlag 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   1
            Left            =   0
            TabIndex        =   97
            Top             =   0
            Width           =   1365
         End
      End
      Begin VB.PictureBox LogoPanel 
         Height          =   600
         Index           =   2
         Left            =   90
         ScaleHeight     =   540
         ScaleWidth      =   1230
         TabIndex        =   95
         Top             =   240
         Width           =   1290
         Begin VB.Image FlcLogo 
            Height          =   510
            Index           =   2
            Left            =   15
            Picture         =   "FlightRotation.frx":11FD
            Top             =   15
            Width           =   1200
         End
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1440
         TabIndex        =   12
         Tag             =   "REGN"
         Text            =   "SXBKF"
         Top             =   240
         Width           =   1365
      End
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Lock"
      Enabled         =   0   'False
      Height          =   285
      Index           =   10
      Left            =   9615
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Object"
      Enabled         =   0   'False
      Height          =   285
      Index           =   9
      Left            =   8745
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Call"
      Enabled         =   0   'False
      Height          =   285
      Index           =   8
      Left            =   7875
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Insert"
      Enabled         =   0   'False
      Height          =   285
      Index           =   7
      Left            =   7005
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Agents"
      Height          =   285
      Index           =   6
      Left            =   6135
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Status"
      Height          =   285
      Index           =   5
      Left            =   5265
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Rotation"
      Height          =   285
      Index           =   4
      Left            =   4395
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Join"
      Height          =   285
      Index           =   3
      Left            =   3525
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Split"
      Height          =   285
      Index           =   2
      Left            =   2655
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Close"
      Height          =   285
      Index           =   1
      Left            =   915
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Save"
      Height          =   285
      Index           =   0
      Left            =   45
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   45
      Width           =   855
   End
   Begin VB.Frame ArrAircraft 
      Caption         =   " Aircraft of Arrival Flight"
      Height          =   945
      Left            =   90
      TabIndex        =   192
      Top             =   660
      Visible         =   0   'False
      Width           =   5835
      Begin VB.TextBox AcrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   5
         Left            =   1440
         TabIndex        =   203
         Text            =   "DATA LINK"
         Top             =   555
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   86
         Left            =   5400
         TabIndex        =   202
         Tag             =   "ANNX"
         Text            =   "12"
         Top             =   555
         Width           =   345
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   85
         Left            =   4770
         TabIndex        =   201
         Tag             =   "ENTY"
         Text            =   "12345"
         Top             =   555
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   84
         Left            =   4080
         TabIndex        =   200
         Tag             =   "ACTI"
         Text            =   "B747X"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   83
         Left            =   3390
         TabIndex        =   199
         Tag             =   "ACT5"
         Text            =   "B747Z"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   81
         Left            =   2850
         TabIndex        =   198
         Tag             =   ",NAME"
         Text            =   "Eleftherios Venizelos"
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   82
         Left            =   2850
         TabIndex        =   197
         Tag             =   "ACT3"
         Text            =   "W74"
         Top             =   555
         Width           =   525
      End
      Begin VB.Frame AcrSign 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   285
         Index           =   2
         Left            =   1440
         TabIndex        =   195
         Top             =   555
         Width           =   1365
         Begin VB.TextBox AcrFlag 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   4
            Left            =   0
            TabIndex        =   196
            Top             =   0
            Width           =   1365
         End
      End
      Begin VB.PictureBox LogoPanel 
         Height          =   600
         Index           =   4
         Left            =   90
         ScaleHeight     =   540
         ScaleWidth      =   1230
         TabIndex        =   194
         Top             =   240
         Width           =   1290
         Begin VB.Image FlcLogo 
            Height          =   510
            Index           =   4
            Left            =   15
            Picture         =   "FlightRotation.frx":20DF
            Top             =   15
            Visible         =   0   'False
            Width           =   1200
         End
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   80
         Left            =   1440
         TabIndex        =   193
         Tag             =   "REGN"
         Text            =   "SXBKF"
         Top             =   240
         Width           =   1365
      End
   End
   Begin VB.Frame DepAircraft 
      Caption         =   " Aircraft of Departue Flight"
      Height          =   945
      Left            =   6060
      TabIndex        =   180
      Top             =   660
      Visible         =   0   'False
      Width           =   5835
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   70
         Left            =   1440
         TabIndex        =   191
         Tag             =   "REGN"
         Text            =   "SXBKF"
         Top             =   240
         Width           =   1365
      End
      Begin VB.PictureBox LogoPanel 
         Height          =   600
         Index           =   3
         Left            =   90
         ScaleHeight     =   540
         ScaleWidth      =   1230
         TabIndex        =   190
         Top             =   240
         Width           =   1290
         Begin VB.Image FlcLogo 
            Height          =   510
            Index           =   3
            Left            =   15
            Picture         =   "FlightRotation.frx":2FC1
            Top             =   15
            Visible         =   0   'False
            Width           =   1200
         End
      End
      Begin VB.Frame AcrSign 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   1440
         TabIndex        =   188
         Top             =   555
         Width           =   1365
         Begin VB.TextBox AcrFlag 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   3
            Left            =   0
            TabIndex        =   189
            Top             =   0
            Width           =   1365
         End
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   72
         Left            =   2850
         TabIndex        =   187
         Tag             =   "ACT3"
         Text            =   "W74"
         Top             =   555
         Width           =   525
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   71
         Left            =   2850
         TabIndex        =   186
         Tag             =   ",NAME"
         Text            =   "Eleftherios Venizelos"
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   73
         Left            =   3390
         TabIndex        =   185
         Tag             =   "ACT5"
         Text            =   "B747Z"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   74
         Left            =   4080
         TabIndex        =   184
         Tag             =   "ACTI"
         Text            =   "B747X"
         Top             =   555
         Width           =   675
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   75
         Left            =   4770
         TabIndex        =   183
         Tag             =   "ENTY"
         Text            =   "12345"
         Top             =   555
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   76
         Left            =   5400
         TabIndex        =   182
         Tag             =   "ANNX"
         Text            =   "12"
         Top             =   555
         Width           =   345
      End
      Begin VB.TextBox AcrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   1440
         TabIndex        =   181
         Text            =   "DATA LINK"
         Top             =   555
         Visible         =   0   'False
         Width           =   1365
      End
   End
   Begin VB.Label RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   3
      Left            =   -30
      TabIndex        =   179
      Top             =   9030
      Visible         =   0   'False
      Width           =   12045
   End
   Begin VB.Label RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "THE INTERFACE TO SITA TELEX NETWORK WILL BE DOWN FOR TWO HOURS"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   2
      Left            =   -30
      TabIndex        =   178
      Top             =   7380
      Width           =   12045
   End
   Begin VB.Label RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "THE DEPARTURE FLIGHT WILL BE DELAYED DUE TO LATE ARRIVAL"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   1
      Left            =   -30
      TabIndex        =   177
      Top             =   1620
      Width           =   12045
   End
   Begin VB.Label RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "HEAVY FOG IN NORTHERN INDIA - ALL FLIGHTS CANCELLED"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   0
      Left            =   -30
      TabIndex        =   176
      Top             =   390
      Width           =   12045
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   12600
      Y1              =   375
      Y2              =   375
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   -15
      X2              =   12600
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000015&
      X1              =   -30
      X2              =   12600
      Y1              =   360
      Y2              =   360
   End
End
Attribute VB_Name = "FlightRotation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Form
Private MyCallButton As Control
Private CurTelexRecord As String
Private CurFlightRecord As String
Private LastInput As String
Private FormIsUp As Boolean
Dim ArrFields As String
Dim DepFields As String
Dim AllFields As String
Dim DspRefresh As Boolean
Dim ArrFldChgList As String
Dim DepFldChgList As String
Dim AcrFldChgList As String
Dim ErrFldChgList As String
Dim ArrRec As String
Dim DepRec As String

Public Sub ShowRotationData(Index As Integer, LineNo As Long, DataTab As TABLib.Tab, GetRkey As Boolean)
    Dim AftRkey As String
    Dim AftUrno As String
    Dim AftAdid As String
    Dim SqlCmd As String
    Dim SqlTbl As String
    Dim SqlFld As String
    Dim SqlKey As String
    Dim SqlDat As String
    Dim SqlRec As String
    Dim AnyRec As String
    Dim iRet As Integer
    Me.Refresh
    AftRkey = ""
    If GetRkey = False Then AftRkey = DataTab.GetFieldValue(LineNo, "RKEY")
    If AftRkey = "" Then
        AftUrno = DataTab.GetFieldValue(LineNo, "URNO")
        If AftUrno <> "" Then
            SqlKey = "WHERE URNO=" & AftUrno
            SqlCmd = "RTA"
            SqlFld = "RKEY"
            SqlTbl = "AFTTAB"
            iRet = UfisServer.CallCeda(AftRkey, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "", 0, True, False)
        End If
    End If
    If AftRkey = "" Then
        Me.Hide
        Me.Refresh
        Exit Sub
    End If
    ActDspRotRkey = AftRkey
    Me.Show
    Me.Refresh
    If AllFields = "" Then InitMyForm
    ResetDisplay
    ArrRec = ""
    DepRec = ""
    CheckInfoFlags
    Me.Refresh
    SqlKey = "WHERE RKEY=" & AftRkey & " AND ADID IN ('A','D')"
    SqlCmd = "RTA"
    SqlFld = AllFields
    SqlTbl = "AFTTAB"
    iRet = UfisServer.CallCeda(SqlRec, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "", 0, True, False)
    AnyRec = GetItem(SqlRec, 1, vbNewLine)
    AftAdid = GetFieldValue("ADID", AnyRec, SqlFld)
    Select Case AftAdid
        Case "A"
            ArrRec = AnyRec
        Case "D"
            DepRec = AnyRec
    End Select
    AnyRec = GetItem(SqlRec, 2, vbNewLine)
    AftAdid = GetFieldValue("ADID", AnyRec, SqlFld)
    Select Case AftAdid
        Case "A"
            ArrRec = AnyRec
        Case "D"
            DepRec = AnyRec
    End Select
    ArrFldChgList = ""
    DspRefresh = True
    SetAirlineLogo 2, SqlFld, ArrRec
    SetAirlineLogo 0, SqlFld, ArrRec
    SetAirlineLogo 1, SqlFld, DepRec
    Me.Refresh
    
    DisplayAllFields AcrFld, "ACR", SqlFld, ArrRec
    DisplayAllFields ArrDay, "DAY", SqlFld, ArrRec
    DisplayAllFields ArrFld, "ARR", SqlFld, ArrRec
    
    DisplayAllFields AcrFld, "ACR", SqlFld, DepRec
    DisplayAllFields DepDay, "DAY", SqlFld, DepRec
    DisplayAllFields DepFld, "DEP", SqlFld, DepRec
    DspRefresh = False
    ArrFldChgList = ""
    DepFldChgList = ""
    AcrFldChgList = ""
    ErrFldChgList = ""
    Me.Refresh
    CheckInfoFlags
    CreateMiniChart
    Me.Refresh
End Sub
Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
    CurTelexRecord = TaskValue
End Sub
Private Sub CheckInfoFlags()
    Dim tmpData As String
    tmpData = Trim(AcrFld(0).Text)
    If (ArrRec <> "") And (DepRec <> "") Then
        If tmpData = "" Then
            Me.Caption = "Scheduled Turnaround"
        Else
            Me.Caption = "Aircraft Turnaround"
        End If
    ElseIf (ArrRec = "") And (DepRec = "") Then
        Me.Caption = "Empty Turnaround"
    ElseIf ArrRec <> "" Then
        Me.Caption = "Open Arrival Leg"
    ElseIf DepRec <> "" Then
        Me.Caption = "Unmatched Departure Flight"
    End If
    ArrFlag(1).Visible = False
    If ArrRec <> "" Then
        tmpData = Trim(ArrFld(1).Text)
        If tmpData = "" Then ArrFlag(1).Visible = True
    End If
    DepFlag(1).Visible = False
    If DepRec <> "" Then
        tmpData = Trim(DepFld(59).Text)
        If tmpData = "" Then DepFlag(1).Visible = True
    End If
    PatchInfoText
End Sub
Private Sub SetAirlineLogo(Index As Integer, FldList As String, DatList As String)
    Dim AftAlc2 As String
    Dim FullPath As String
    Dim LogoName As String
    Dim LogoPath As String
    On Error GoTo NoLogo
    If DatList <> "" Then
        If Index < 2 Then
            AftAlc2 = GetFieldValue("ALC2", DatList, FldList)
        Else
            'AftAlc2 = GetFieldValue("ALC2", DatList, FldList)
            AftAlc2 = ""
        End If
        If AftAlc2 <> "" Then
            LogoName = AftAlc2 & "_S.bmp"
            LogoPath = StatusPicPath & "\logos\"
            FullPath = LogoPath & LogoName
            FlcLogo(Index).Picture = LoadPicture(FullPath)
            FlcLogo(Index).Left = (LogoPanel(Index).ScaleWidth - FlcLogo(Index).Width) / 2
            FlcLogo(Index).Top = (LogoPanel(Index).ScaleHeight - FlcLogo(Index).Height) / 2
            LogoPanel(Index).BackColor = GetPixelColor(MyBitMap)
        Else
            FlcLogo(Index).Picture = LoadPicture("")
            LogoPanel(Index).BackColor = LightGrey
        End If
    Else
        FlcLogo(Index).Picture = LoadPicture("")
        LogoPanel(Index).BackColor = LightGrey
    End If
    Exit Sub
NoLogo:
    FlcLogo(Index).Picture = LoadPicture("")
    LogoPanel(Index).BackColor = LightGrey
End Sub
Private Sub InitMyForm()
    ArrFields = ""
    DepFields = ""
    AllFields = ""
    CollectAllFields AcrFld, "ARRDEP"
    CollectAllFields ArrFld, "ARR"
    CollectAllFields DepFld, "DEP"
    If Len(ArrFields) > 0 Then ArrFields = Left(ArrFields, Len(ArrFields) - 1)
    If Len(DepFields) > 0 Then DepFields = Left(DepFields, Len(DepFields) - 1)
    If Len(AllFields) > 0 Then AllFields = Left(AllFields, Len(AllFields) - 1)
    If InStr(AllFields, "ALC2") = 0 Then AllFields = "ALC2," & AllFields
    If InStr(AllFields, "FTYP") = 0 Then AllFields = "FTYP," & AllFields
    If InStr(AllFields, "ADID") = 0 Then AllFields = "ADID," & AllFields
    If InStr(AllFields, "URNO") = 0 Then AllFields = "URNO," & AllFields
    CleanDataModel
    ResetDisplay
End Sub
Private Sub CleanDataModel()
    'HardCoded HotFix for ATH4.4
    AllFields = Replace(AllFields, ",HDLL", "", 1, -1, vbBinaryCompare)
    AllFields = Replace(AllFields, ",BAAA", "", 1, -1, vbBinaryCompare)
End Sub
Private Sub ResetDisplay()
    DspRefresh = True
    ClearAllFields AcrFld
    ClearAllFields ArrDay
    ClearAllFields DepDay
    ClearAllFields ArrFld
    ClearAllFields DepFld
    ClearAllFields RemFld
    ClearAllFields ElseFld
    ArrFlag(0).Visible = False
    DepFlag(0).Visible = False
    ArrFld(35).Visible = True
    DepFld(10).Visible = True
    DspRefresh = True
End Sub

Private Sub ClearAllFields(ActFldArray As Variant)
    Dim ActFld As Control
    For Each ActFld In ActFldArray
        ActFld.Text = ""
        ActFld.Tag = GetItem(ActFld.Tag, 1, "|")
        ActFld.BackColor = LightGrey
    Next
End Sub
Private Sub CollectAllFields(ActFldArray As Variant, ForWhat As String)
    Dim ActFld As Control
    Dim tmpTag As String
    Dim FldName As String
    For Each ActFld In ActFldArray
        tmpTag = ActFld.Tag
        FldName = GetItem(tmpTag, 1, ",")
        If FldName <> "" Then
            Select Case ForWhat
                Case "ARR"
                    If InStr(ArrFields, FldName) = 0 Then ArrFields = ArrFields & FldName & ","
                Case "DEP"
                    If InStr(DepFields, FldName) = 0 Then DepFields = DepFields & FldName & ","
                Case "ARRDEP"
                    If InStr(ArrFields, FldName) = 0 Then ArrFields = ArrFields & FldName & ","
                    If InStr(DepFields, FldName) = 0 Then DepFields = DepFields & FldName & ","
                Case Else
            End Select
            If InStr(AllFields, FldName) = 0 Then AllFields = AllFields & FldName & ","
        End If
    Next
End Sub
Private Sub DisplayAllFields(ActFldArray As Variant, ForWhat As String, RecFields As String, RecData As String)
    Dim ActFld As Control
    Dim tmpTag As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim FldData As String
    Dim DspData As String
    Dim DayData As String
    Dim AddDiff As String
    Dim FldColor As Long
    Dim DayDiff As Long
    Dim ShowCxxFlag As Boolean
    Dim FldTime
    Dim DayTime
    If RecData <> "" Then
        Select Case ForWhat
            Case "ARR"
                tmpTag = ArrDay(0).Tag
                DayData = GetItem(tmpTag, 2, "|")
                If DayData <> "" Then
                    DayTime = CedaFullDateToVb(DayData)
                    DayTime = DateAdd("n", UtcTimeDisp, DayTime)
                End If
            Case "DEP"
                tmpTag = DepDay(0).Tag
                DayData = GetItem(tmpTag, 2, "|")
                If DayData <> "" Then
                    DayTime = CedaFullDateToVb(DayData)
                    DayTime = DateAdd("n", UtcTimeDisp, DayTime)
                End If
            Case Else
        End Select
        For Each ActFld In ActFldArray
            tmpTag = ActFld.Tag
            FldProp = GetItem(tmpTag, 1, "|")
            FldName = GetItem(FldProp, 1, ",")
            FldType = GetItem(FldProp, 2, ",")
            If FldType = "" Then FldType = FldName
            If FldName <> "" Then
                FldColor = 0
                FldData = GetFieldValue(FldName, RecData, RecFields)
                If FldData <> "" Then
                    DspData = FldData
                    Select Case FldType
                        Case "TIME"
                            FldTime = CedaFullDateToVb(FldData)
                            FldTime = DateAdd("n", UtcTimeDisp, FldTime)
                            DspData = Format(FldTime, "hh:mm")
                            If DayData <> "" Then
                                DayDiff = DateDiff("d", DayTime, FldTime)
                                If DayDiff > 0 Then
                                    AddDiff = "+" & CStr(DayDiff)
                                    If Len(AddDiff) > 3 Then AddDiff = "+**"
                                ElseIf DayDiff < 0 Then
                                    AddDiff = CStr(DayDiff)
                                    If Len(AddDiff) > 3 Then AddDiff = "-**"
                                Else
                                    AddDiff = ""
                                End If
                                DspData = DspData & AddDiff
                            End If
                        Case "DATE"
                            FldTime = CedaFullDateToVb(FldData)
                            FldTime = DateAdd("n", UtcTimeDisp, FldTime)
                            DspData = Format(FldTime, "dd.mm.yyyy")
                        Case "FLNO"
                            DspData = Left(FldData, 3)
                            DspData = Trim(DspData)
                        Case "STYP", "TTYP"
                            FldColor = vbButtonFace
                        Case "FTYP"
                            DspData = FldData
                            If ForWhat = "ARR" Then
                                Select Case FldData
                                    Case "X"
                                       ShowCxxFlag = True
                                       ArrFlag(0).Caption = "CANCELLED"
                                       ArrFlag(0).BackColor = vbRed
                                       ArrFlag(0).ForeColor = vbWhite
                                    Case "S"
                                       ShowCxxFlag = True
                                       ArrFlag(0).Caption = "SCHEDULED"
                                       ArrFlag(0).BackColor = vbYellow
                                       ArrFlag(0).ForeColor = vbBlack
                                    Case Else
                                       ShowCxxFlag = False
                                End Select
                            End If
                            If ForWhat = "DEP" Then
                                Select Case FldData
                                    Case "X"
                                       ShowCxxFlag = True
                                       DepFlag(0).Caption = "CANCELLED"
                                       DepFlag(0).BackColor = vbRed
                                       DepFlag(0).ForeColor = vbWhite
                                    Case "S"
                                       ShowCxxFlag = True
                                       DepFlag(0).Caption = "SCHEDULED"
                                       DepFlag(0).BackColor = vbYellow
                                       DepFlag(0).ForeColor = vbBlack
                                    Case Else
                                       ShowCxxFlag = False
                                End Select
                            End If
                            FldColor = vbButtonFace
                        Case Else
                            DspData = FldData
                    End Select
                    ActFld.Text = DspData
                    ActFld.Tag = FldProp & "|" & FldData
                End If
                Select Case FldType
                    Case "STYP", "TTYP", "HDLL"
                        FldColor = vbButtonFace
                    Case Else
                End Select
                If FldColor = 0 Then
                    Select Case ForWhat
                        Case "ARR"
                            FldColor = vbWhite
                            If ShowCxxFlag Then ArrFlag(0).Visible = ShowCxxFlag
                        Case "DEP"
                            FldColor = vbWhite
                            If ShowCxxFlag Then DepFlag(0).Visible = ShowCxxFlag
                        Case "DAY"
                            FldColor = vbButtonFace
                        Case Else
                            FldColor = vbWhite
                    End Select
                End If
                ActFld.BackColor = FldColor
                Select Case ForWhat
                    Case "ARR"
                        If ShowCxxFlag Then
                            ArrFlag(0).Visible = ShowCxxFlag
                            ArrFld(35).Visible = False
                        End If
                    Case "DEP"
                        If ShowCxxFlag Then
                            DepFlag(0).Visible = ShowCxxFlag
                            DepFld(10).Visible = False
                        End If
                    Case Else
                End Select
            End If
        Next
    End If
End Sub

Private Sub AcrFld_Change(Index As Integer)
    Dim retval As Integer
    Dim tmpTag As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim OldData As String
    Dim DspData As String
    Dim NewData As String
    Dim NewCeda As String
    Dim FldFlag As String
    If Not DspRefresh Then
        tmpTag = AcrFld(Index).Tag
        FldProp = GetItem(tmpTag, 1, "|")
        FldName = GetItem(FldProp, 1, ",")
        FldType = GetItem(FldProp, 2, ",")
        If FldType = "" Then FldType = FldName
        OldData = GetItem(tmpTag, 2, "|")
        DspData = AcrFld(Index).Text
        FldFlag = "|D#" & CStr(Index) & "|"
        retval = FormatNewFieldValue(Index, FldName, FldType, OldData, DspData, NewCeda, NewData)
        tmpTag = FldProp & "|" & OldData & "|" & NewCeda
        AcrFld(Index).Tag = tmpTag
        Select Case retval
            Case -1
                AcrFld(Index).BackColor = vbRed
                AcrFld(Index).ForeColor = vbWhite
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 0
                AcrFld(Index).BackColor = vbWhite
                AcrFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                AcrFldChgList = Replace(AcrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                DspRefresh = True
                AcrFld(Index).Text = NewData
                DspRefresh = False
            Case 1
                AcrFld(Index).BackColor = LightestGreen
                AcrFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                If InStr(AcrFldChgList, FldFlag) = 0 Then AcrFldChgList = AcrFldChgList & FldFlag
            Case 2
                AcrFld(Index).BackColor = LightestRed
                AcrFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 3
                AcrFld(Index).BackColor = LightYellow
                AcrFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
        End Select
        AdjustSaveButton
    End If
End Sub

Private Sub ArrButton_Click(Index As Integer)
'    txtDummy.SetFocus

End Sub

Private Sub ArrFld_Change(Index As Integer)
    Dim retval As Integer
    Dim tmpTag As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim OldData As String
    Dim DspData As String
    Dim NewData As String
    Dim NewCeda As String
    Dim FldFlag As String
    If Not DspRefresh Then
        tmpTag = ArrFld(Index).Tag
        FldProp = GetItem(tmpTag, 1, "|")
        FldName = GetItem(FldProp, 1, ",")
        FldType = GetItem(FldProp, 2, ",")
        If FldType = "" Then FldType = FldName
        OldData = GetItem(tmpTag, 2, "|")
        DspData = ArrFld(Index).Text
        FldFlag = "|D#" & CStr(Index) & "|"
        retval = FormatNewFieldValue(Index, FldName, FldType, OldData, DspData, NewCeda, NewData)
        tmpTag = FldProp & "|" & OldData & "|" & NewCeda
        ArrFld(Index).Tag = tmpTag
        Select Case retval
            Case -1
                ArrFld(Index).BackColor = vbRed
                ArrFld(Index).ForeColor = vbWhite
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 0
                ArrFld(Index).BackColor = vbWhite
                ArrFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                ArrFldChgList = Replace(ArrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                DspRefresh = True
                ArrFld(Index).Text = NewData
                DspRefresh = False
            Case 1
                ArrFld(Index).BackColor = LightestGreen
                ArrFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                ArrFldChgList = ArrFldChgList & FldFlag
            Case 2
                ArrFld(Index).BackColor = LightestRed
                ArrFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 3
                ArrFld(Index).BackColor = LightYellow
                ArrFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
        End Select
        AdjustSaveButton
    End If
End Sub

Private Sub chkAssign_Click(Index As Integer)
    'txtDummy.SetFocus
    If chkAssign(Index).Value = 1 Then
        'ShowChildForm Me, chkAssign(0), AssignFlight, ""
    Else
        'Unload AssignFlight
    End If
End Sub

Private Sub DepButton_Click(Index As Integer)
    'txtDummy.SetFocus

End Sub

Private Sub DepFld_Change(Index As Integer)
    Dim retval As Integer
    Dim tmpTag As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim OldData As String
    Dim DspData As String
    Dim NewData As String
    Dim NewCeda As String
    Dim FldFlag As String
    If Not DspRefresh Then
        tmpTag = DepFld(Index).Tag
        FldProp = GetItem(tmpTag, 1, "|")
        FldName = GetItem(FldProp, 1, ",")
        FldType = GetItem(FldProp, 2, ",")
        If FldType = "" Then FldType = FldName
        OldData = GetItem(tmpTag, 2, "|")
        DspData = DepFld(Index).Text
        FldFlag = "|D#" & CStr(Index) & "|"
        retval = FormatNewFieldValue(Index, FldName, FldType, OldData, DspData, NewCeda, NewData)
        tmpTag = FldProp & "|" & OldData & "|" & NewCeda
        DepFld(Index).Tag = tmpTag
        Select Case retval
            Case -1
                DepFld(Index).BackColor = vbRed
                DepFld(Index).ForeColor = vbWhite
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 0
                DepFld(Index).BackColor = vbWhite
                DepFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                DepFldChgList = Replace(DepFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                DspRefresh = True
                'DepFld(Index).Text = NewData
                DspRefresh = False
            Case 1
                DepFld(Index).BackColor = LightestGreen
                DepFld(Index).ForeColor = vbBlack
                ErrFldChgList = Replace(ErrFldChgList, FldFlag, "", 1, -1, vbBinaryCompare)
                DepFldChgList = DepFldChgList & FldFlag
            Case 2
                DepFld(Index).BackColor = LightestRed
                DepFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
            Case 3
                DepFld(Index).BackColor = LightYellow
                DepFld(Index).ForeColor = vbBlack
                ErrFldChgList = ErrFldChgList & FldFlag
        End Select
        AdjustSaveButton
    End If
End Sub
Private Sub AdjustSaveButton()
    Dim SaveIt As Boolean
    SaveIt = False
    If ArrFldChgList <> "" Then SaveIt = True
    If DepFldChgList <> "" Then SaveIt = True
    If SaveIt Then
        MainButton(0).BackColor = vbRed
        MainButton(0).ForeColor = vbWhite
    Else
        MainButton(0).BackColor = vbButtonFace
        MainButton(0).ForeColor = vbBlack
    End If
    If ErrFldChgList <> "" Then
        MainButton(0).Enabled = False
        MainButton(0).BackColor = LightestRed
        MainButton(0).ForeColor = vbBlack
    Else
        MainButton(0).Enabled = True
    End If
End Sub
Public Function FormatNewFieldValue(Index As Integer, FldName As String, FldType As String, OldData As String, UseDspData As String, NewCeda As String, RetData As String) As Integer
    Dim retval As Integer
    Dim GetRet As Boolean
    Dim FldStrg As String
    Dim NewTime As Date
    Dim OldTime As Date
    Dim OldValu As String
    Dim NewValu As String
    Dim OldStrg As String
    Dim NewStrg As String
    Dim NewData As String
    Dim DataLen As Integer
    Dim OutData As String
    Dim OldCeda As String
    Dim DspData As String
    Dim StrgOff As String
    Dim TimeOff As Long
    retval = 0
    If FldType = "" Then FldType = FldName
    OldCeda = OldData
    DspData = UseDspData
    Select Case FldType
        Case "TIME"
            DataLen = Len(DspData)
            If InStr(DspData, "+") > 0 Then
                DspData = GetItem(UseDspData, 1, "+")
                StrgOff = GetItem(UseDspData, 2, "+")
                DataLen = Len(DspData)
                If StrgOff = "" Then DataLen = 3
            End If
            If InStr(DspData, "-") > 0 Then
                DspData = GetItem(UseDspData, 1, "-")
                StrgOff = GetItem(UseDspData, 2, "-")
                DataLen = Len(DspData)
                If StrgOff = "" Then DataLen = 3
            End If
            TimeOff = Val(StrgOff)
            If DataLen = 5 Then
                If Mid(DspData, 3, 1) = ":" Then
                    NewValu = Replace(DspData, ":", "", 1, -1, vbBinaryCompare)
                    GetRet = CheckValidTime(NewValu, "")
                    If GetRet = True Then
                        If OldCeda = "" Then
                            OldCeda = GetItem(DepDay(0).Tag, 2, "|")
                        End If
                        OldTime = CedaFullDateToVb(OldCeda)
                        OldTime = DateAdd("n", UtcTimeDisp, OldTime)
                        OldStrg = Format(OldTime, "yyyymmddhhmmss")
                        NewStrg = OldStrg
                        Mid(NewStrg, 9, 4) = NewValu
                        NewTime = CedaFullDateToVb(NewStrg)
                        NewTime = DateAdd("n", -UtcTimeDisp, NewTime)
                        'NewTime = DateAdd("d", TimeOff, NewTime)
                        NewStrg = Format(NewTime, "yyyymmddhhmmss")
                        NewCeda = NewStrg
                    Else
                        retval = 2
                        NewCeda = "{=ERR=}"
                    End If
                    OutData = DspData
                Else
                    retval = 2
                    OutData = DspData
                    NewCeda = "{=ERR=}"
                End If
            ElseIf DataLen = 4 Then
                If InStr(DspData, ":") = 0 Then
                    NewValu = DspData
                    GetRet = CheckValidTime(NewValu, "")
                    If GetRet = True Then
                        If OldCeda = "" Then
                            OldCeda = GetItem(DepDay(0).Tag, 2, "|")
                        End If
                        OldTime = CedaFullDateToVb(OldCeda)
                        OldTime = DateAdd("n", UtcTimeDisp, OldTime)
                        OldStrg = Format(OldTime, "yyyymmddhhmmss")
                        NewStrg = OldStrg
                        Mid(NewStrg, 9, 4) = NewValu
                        NewTime = CedaFullDateToVb(NewStrg)
                        NewTime = DateAdd("n", -UtcTimeDisp, NewTime)
                        'NewTime = DateAdd("d", TimeOff, NewTime)
                        NewStrg = Format(NewTime, "yyyymmddhhmmss")
                        NewCeda = NewStrg
                    Else
                        retval = 2
                        NewCeda = "{=ERR=}"
                    End If
                Else
                    NewData = OldData
                    retval = 3
                End If
                OutData = DspData
            ElseIf DataLen > 5 Then
                retval = -1
                OutData = DspData
                NewCeda = "{=ERR=}"
            ElseIf DataLen = 0 Then
                OutData = DspData
                NewData = " "
            Else
                retval = 3
                OutData = DspData
                NewCeda = "{=UPD=}"
            End If
            If (OldData <> "") And (NewCeda = OldCeda) Then
                OutData = Left(DspData, 2) & ":" & Right(DspData, 2)
            End If
            
'        Case "DATE"
'            FldTime = CedaFullDateToVb(FldData)
'            FldTime = DateAdd("n", UtcTimeDisp, FldTime)
'            DspData = Format(FldTime, "dd.mm.yyyy")
'        Case "FLNO"
'            DspData = Left(FldData, 3)
'            DspData = Trim(DspData)
'        Case "STYP", "TTYP"
'            FldColor = vbButtonFace
'        Case "FTYP"
'            DspData = FldData
'            Select Case FldData
'                Case "X"
'                   ShowCxxFlag = True
'                Case Else
'                   ShowCxxFlag = False
'            End Select
'            FldColor = vbButtonFace
        Case Else
            NewCeda = DspData
            OutData = DspData
    End Select
    RetData = OutData
    If (retval >= 0) And (retval < 2) Then
        If OldCeda <> NewCeda Then
            retval = 1
        Else
            retval = 0
        End If
    End If
    FormatNewFieldValue = retval
End Function

Private Sub Form_Activate()
'    With MyParent
'        OnTop.Value = .OnTop.Value
'    End With
    If Not FormIsUp Then
        'Form_Resize
        'MainButton(1).SetFocus
        Me.Refresh
        FormIsUp = True
    End If
    Me.Refresh
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Me.Hide
End Sub

Private Sub Form_Load()
    FormIsUp = False
    Set ArrFlag(0).Container = Frame1
    ArrFlag(0).Left = ArrFld(35).Left
    ArrFlag(0).Top = ArrFld(35).Top
    ArrFlag(0).ZOrder
    Set DepFlag(0).Container = Frame2
    DepFlag(0).Left = DepFld(10).Left
    DepFlag(0).Top = DepFld(10).Top
    DepFlag(0).ZOrder
    InitMyForm
    Form_Resize
    Me.Top = MainDialog.Top + ((MainDialog.Height - Me.Height) / 2)
    Me.Left = MainDialog.Left + ((MainDialog.Width - Me.Width) / 2)
    Load InfoConfig
End Sub

Private Sub Form_Resize()
    Dim NewTop As Long
    Dim SizeDiff As Long
    NewTop = Line1.y1
    NewTop = NewTop + 0
    If (RotFlag(0).Visible) Or (Not FormIsUp) Then
        'NewTop = NewTop + 30
        RotFlag(0).Top = NewTop
        NewTop = NewTop + RotFlag(0).Height
    End If
    VSplit.Top = NewTop - 30
    NewTop = NewTop + 60
    AircraftData.Top = NewTop
    ArrAircraft.Top = NewTop
    DepAircraft.Top = NewTop
    NewTop = NewTop + AircraftData.Height
    If (RotFlag(1).Visible) Or (Not FormIsUp) Then
        NewTop = NewTop + 30
        RotFlag(1).Top = NewTop
        NewTop = NewTop + RotFlag(1).Height
    End If
    NewTop = NewTop + 60
    ArrivalData.Top = NewTop
    DepartureData.Top = NewTop
    NewTop = NewTop + ArrivalData.Height
    If (RotFlag(2).Visible) Or (Not FormIsUp) Then
        NewTop = NewTop + 30
        RotFlag(2).Top = NewTop
        NewTop = NewTop + RotFlag(2).Height
    End If
    NewTop = NewTop + 60
    ElseData.Top = NewTop
    GeneralData.Top = NewTop
    NewTop = NewTop + ElseData.Height
    RotFlag(3).Visible = False
    If RotFlag(3).Visible Then
        NewTop = NewTop + 30
        RotFlag(3).Top = NewTop
        NewTop = NewTop + RotFlag(3).Height
    End If
    VSplit.Height = RotFlag(2).Top - VSplit.Top + 30
    NewTop = NewTop + 30
    NewTop = NewTop + StatusBar1.Height
    SizeDiff = Me.Height - Me.ScaleHeight
    Me.Height = NewTop + SizeDiff
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'MyCallButton.Value = 0
End Sub

Private Sub OnTop_Click()
    'MyParent.OnTop.Value = OnTop.Value
    'Me.SetFocus
    'txtDummy.SetFocus
End Sub

Private Sub MainButton_Click(Index As Integer)
    If MainButton(Index).Value = 1 Then
        MainButton(Index).BackColor = LightGreen
        MainButton(Index).ForeColor = vbBlack
        Select Case Index
            Case 0  'Save
                SendUpdatesToCeda
                MainButton(Index).Value = 0
            Case 1  'Close
                Me.Hide
                MainButton(Index).Value = 0
            Case 11
                InfoConfig.Show , Me
            Case 12 'Flights
                MainButton(13).Enabled = False
                AircraftData.Visible = False
                ArrAircraft.Visible = True
                DepAircraft.Visible = True
                RotFlag(1).Visible = False
                VSplit.Visible = True
                RotFlag(0).Visible = True
                RotFlag(1).Visible = False
                RotFlag(2).Visible = True
                RotFlag(3).Visible = False
                Form_Resize
            Case 13 'Info
                RotFlag(0).Visible = Not RotFlag(0).Visible
                RotFlag(1).Visible = RotFlag(0).Visible
                RotFlag(2).Visible = RotFlag(0).Visible
                RotFlag(3).Visible = RotFlag(0).Visible
                Form_Resize
                MainButton(Index).Value = 0
            Case Else
                MainButton(Index).Value = 0
        End Select
    Else
        MainButton(Index).BackColor = vbButtonFace
        Select Case Index
            Case 12
                AircraftData.Visible = True
                ArrAircraft.Visible = False
                DepAircraft.Visible = False
                RotFlag(1).Visible = True
                VSplit.Visible = False
                MainButton(13).Enabled = True
                Form_Resize
            Case 11
                InfoConfig.Hide
            Case Else
        End Select
    End If
End Sub

Private Sub SendUpdatesToCeda()
    Dim AftUrno As String
    Dim tmpTag As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim UpdName As String
    Dim OldData As String
    Dim NewData As String
    Dim CurItem As String
    Dim FldItem As String
    Dim SqlTbl As String
    Dim SqlFld As String
    Dim SqlDat As String
    Dim SqlKey As String
    Dim SqlCmd As String
    Dim CedaResp As String
    Dim Itm As Integer
    Dim idx As Integer
    If ErrFldChgList <> "" Then
        Return
    End If
    SqlFld = ""
    SqlDat = ""
    AcrFldChgList = Replace(AcrFldChgList, "||", "|", 1, -1, vbBinaryCompare)
    ArrFldChgList = Replace(ArrFldChgList, "||", "|", 1, -1, vbBinaryCompare)
    DepFldChgList = Replace(DepFldChgList, "||", "|", 1, -1, vbBinaryCompare)
    AftUrno = ArrFld(36).Text
    If AcrFldChgList <> "" Then
        Itm = 1
        CurItem = "START"
        While CurItem <> ""
            Itm = Itm + 1
            CurItem = GetItem(AcrFldChgList, Itm, "|")
            If CurItem <> "" Then
                FldItem = GetItem(CurItem, 2, "#")
                idx = Val(FldItem)
                tmpTag = AcrFld(idx).Tag
                FldProp = GetItem(tmpTag, 1, "|")
                FldName = GetItem(FldProp, 1, ",")
                FldType = GetItem(FldProp, 2, ",")
                UpdName = GetItem(FldProp, 3, ",")
                If UpdName = "" Then UpdName = FldName
                OldData = GetItem(tmpTag, 2, "|")
                NewData = GetItem(tmpTag, 3, "|")
                If NewData = "" Then NewData = " "
                If OldData <> NewData Then
                    SqlFld = SqlFld & "," & UpdName
                    SqlDat = SqlDat & "," & NewData
                End If
            End If
        Wend
    End If
    If ArrFldChgList <> "" Then
        Itm = 1
        CurItem = "START"
        While CurItem <> ""
            Itm = Itm + 1
            CurItem = GetItem(ArrFldChgList, Itm, "|")
            If CurItem <> "" Then
                FldItem = GetItem(CurItem, 2, "#")
                idx = Val(FldItem)
                tmpTag = ArrFld(idx).Tag
                FldProp = GetItem(tmpTag, 1, "|")
                FldName = GetItem(FldProp, 1, ",")
                FldType = GetItem(FldProp, 2, ",")
                UpdName = GetItem(FldProp, 3, ",")
                If UpdName = "" Then UpdName = FldName
                OldData = GetItem(tmpTag, 2, "|")
                NewData = GetItem(tmpTag, 3, "|")
                If NewData = "" Then NewData = " "
                If OldData <> NewData Then
                    SqlFld = SqlFld & "," & UpdName
                    SqlDat = SqlDat & "," & NewData
                End If
            End If
        Wend
    End If
    If SqlFld <> "" Then
        SqlFld = Mid(SqlFld, 2)
        SqlDat = Mid(SqlDat, 2)
        SqlCmd = "UFR"
        SqlTbl = "AFTTAB"
        SqlKey = "WHERE URNO=" & AftUrno
        UfisServer.CallCeda CedaResp, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "", 0, True, False
    End If
    SqlFld = ""
    SqlDat = ""
    AftUrno = DepFld(12).Text
    If AcrFldChgList <> "" Then
        Itm = 1
        CurItem = "START"
        While CurItem <> ""
            Itm = Itm + 1
            CurItem = GetItem(AcrFldChgList, Itm, "|")
            If CurItem <> "" Then
                FldItem = GetItem(CurItem, 2, "#")
                idx = Val(FldItem)
                tmpTag = AcrFld(idx).Tag
                FldProp = GetItem(tmpTag, 1, "|")
                FldName = GetItem(FldProp, 1, ",")
                FldType = GetItem(FldProp, 2, ",")
                UpdName = GetItem(FldProp, 3, ",")
                If UpdName = "" Then UpdName = FldName
                OldData = GetItem(tmpTag, 2, "|")
                NewData = GetItem(tmpTag, 3, "|")
                If NewData = "" Then NewData = " "
                If OldData <> NewData Then
                    SqlFld = SqlFld & "," & UpdName
                    SqlDat = SqlDat & "," & NewData
                End If
            End If
        Wend
    End If
    If DepFldChgList <> "" Then
        Itm = 1
        CurItem = "START"
        While CurItem <> ""
            Itm = Itm + 1
            CurItem = GetItem(DepFldChgList, Itm, "|")
            If CurItem <> "" Then
                FldItem = GetItem(CurItem, 2, "#")
                idx = Val(FldItem)
                tmpTag = DepFld(idx).Tag
                FldProp = GetItem(tmpTag, 1, "|")
                FldName = GetItem(FldProp, 1, ",")
                FldType = GetItem(FldProp, 2, ",")
                UpdName = GetItem(FldProp, 3, ",")
                If UpdName = "" Then UpdName = FldName
                OldData = GetItem(tmpTag, 2, "|")
                NewData = GetItem(tmpTag, 3, "|")
                If NewData = "" Then NewData = " "
                If OldData <> NewData Then
                    SqlFld = SqlFld & "," & UpdName
                    SqlDat = SqlDat & "," & NewData
                End If
            End If
        Wend
    End If
    If SqlFld <> "" Then
        SqlFld = Mid(SqlFld, 2)
        SqlDat = Mid(SqlDat, 2)
        SqlCmd = "UFR"
        SqlTbl = "AFTTAB"
        SqlKey = "WHERE URNO=" & AftUrno
        UfisServer.CallCeda CedaResp, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "", 0, True, False
    End If
End Sub

Public Sub PatchInfoText()
    Dim CodeList As String
    Dim KeyCode As String
    Dim KeyField As String
    Dim KeyRec As String
    Dim KeyPatch As String
    Dim InfoText As String
    Dim Itm As Integer
    Dim inf As Integer
    CodeList = "REGN,FLNA,FLND,ORIG,DEST,VIAA,VIAD"
    For inf = 0 To RotFlag.UBound
        InfoText = RotFlag(inf).Tag
        Itm = 0
        KeyCode = "START"
        While KeyCode <> ""
            Itm = Itm + 1
            KeyCode = GetItem(CodeList, Itm, ",")
            If KeyCode <> "" Then
                Select Case KeyCode
                    Case "REGN"
                        KeyField = "REGN"
                        KeyRec = ArrRec
                    Case "FLNA"
                        KeyField = "FLNO"
                        KeyRec = ArrRec
                    Case "FLND"
                        KeyField = "FLNO"
                        KeyRec = DepRec
                    Case "ORIG"
                        KeyField = "ORG3"
                        KeyRec = ArrRec
                    Case "DEST"
                        KeyField = "DES3"
                        KeyRec = DepRec
                    Case "VIAA"
                        KeyField = "VIA3"
                        KeyRec = ArrRec
                    Case "VIAD"
                        KeyField = "VIA3"
                        KeyRec = DepRec
                    Case Else
                    KeyField = ""
                End Select
                If KeyField <> "" Then
                    KeyCode = "{" & KeyCode & "}"
                    KeyPatch = GetFieldValue(KeyField, KeyRec, AllFields)
                    If KeyPatch = "" Then KeyPatch = "..."
                    InfoText = Replace(InfoText, KeyCode, KeyPatch, 1, -1, vbBinaryCompare)
                End If
            End If
        Wend
        RotFlag(inf).Caption = InfoText
    Next
End Sub

Private Sub CreateMiniChart()
    Dim ArrText As String
    Dim DspBoxLen As Long
    Dim ArrBarBgn As Long
    Dim ArrBarLen As Long
    Dim ArrBarTop As Long
    Dim DepText As String
    Dim DepBarBgn As Long
    Dim DepBarLen As Long
    Dim DepBarTop As Long
    DspBoxLen = ChartPanel(1).ScaleWidth - 60
    ArrText = Trim(ArrFld(16).Text)
    DepText = Trim(DepFld(45).Text)
    If (ArrText <> "") And (DepText <> "") Then
        ArrPos.Caption = ArrText
        ArrBarBgn = 30
        ArrBarTop = 30
        ArrBarLen = DspBoxLen / 2
        DepBarLen = DspBoxLen - ArrBarLen
        DepBarBgn = DspBoxLen - DepBarLen + 30
        If ArrText = DepText Then
            DepBarTop = ArrBarTop
        Else
            DepBarTop = ArrBarTop + ArrPos.Height
        End If
        ArrPos.Left = ArrBarBgn
        ArrPos.Top = ArrBarTop
        ArrPos.Width = ArrBarLen
        DepPos.Caption = DepText
        DepPos.Top = DepBarTop
        DepPos.Left = DepBarBgn
        DepPos.Width = DepBarLen
    End If
    
End Sub
