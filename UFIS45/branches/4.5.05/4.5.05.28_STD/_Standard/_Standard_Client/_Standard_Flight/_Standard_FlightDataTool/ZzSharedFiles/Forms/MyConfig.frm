VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MyConfig 
   Caption         =   "Process Configuration Layout"
   ClientHeight    =   5385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7950
   Icon            =   "MyConfig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5385
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   8
      Top             =   5070
      Width           =   7950
      _ExtentX        =   14023
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Table Keys"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   30
      Width           =   1155
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Table Fields"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   2370
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   30
      Width           =   1155
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Data Model"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   30
      Width           =   1155
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   30
      Width           =   1155
   End
   Begin TABLib.TAB VialTab 
      Height          =   915
      Index           =   0
      Left            =   30
      TabIndex        =   1
      Top             =   3240
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin TABLib.TAB CfgTab 
      Height          =   915
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   360
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin TABLib.TAB CfgTab 
      Height          =   915
      Index           =   1
      Left            =   30
      TabIndex        =   2
      Top             =   1320
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin TABLib.TAB CfgTab 
      Height          =   915
      Index           =   2
      Left            =   30
      TabIndex        =   3
      Top             =   2280
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   1614
      _StockProps     =   64
   End
End
Attribute VB_Name = "MyConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ViaFldNamLst As String
Dim ViaFldTxtLst As String
Dim CfgTabIdx As Integer
Dim ChkBtnIdx As Integer
Public Function GetMandatoryTableFields(CfgSection As String, TableKey As String, UseFlds As String, UseHead As String, UseAlgn As String, UseWith As String, UseSize As String, UseProp As String) As Boolean
    Dim RetVal As Boolean
    Dim CurFlds As String
    Dim OutFlds As String
    Dim OutHead As String
    Dim OutAlgn As String
    Dim OutWith As String
    Dim OutSize As String
    Dim OutProp As String
    Dim CfgFlds As String
    Dim CfgHead As String
    Dim CfgAlgn As String
    Dim CfgWith As String
    Dim CfgSize As String
    Dim CfgProp As String
    Dim CurItem As String
    Dim HitLine As String
    Dim LineNo As Long
    Dim Itm As Integer
    RetVal = True
    CurFlds = UseFlds
    OutFlds = ""
    OutHead = ""
    OutAlgn = ""
    OutWith = ""
    OutSize = ""
    OutProp = ""
    HitLine = CfgTab(1).GetLinesByColumnValue(0, TableKey, 0)
    If HitLine = "" Then
        InitMandatoryFdsFields CfgSection, TableKey, False
        HitLine = CfgTab(1).GetLinesByColumnValue(0, TableKey, 0)
    End If
    If HitLine <> "" Then
        LineNo = Val(HitLine)
        CfgFlds = CfgTab(1).GetFieldValue(LineNo, "FLDS")
        CfgHead = CfgTab(1).GetFieldValue(LineNo, "HEAD")
        CfgAlgn = CfgTab(1).GetFieldValue(LineNo, "ALGN")
        CfgWith = CfgTab(1).GetFieldValue(LineNo, "WITH")
        CfgSize = CfgTab(1).GetFieldValue(LineNo, "SIZE")
        CfgProp = CfgTab(1).GetFieldValue(LineNo, "PROP")
        CurItem = "START"
        Itm = 0
        While CurItem <> ""
            Itm = Itm + 1
            CurItem = GetItem(CfgFlds, Itm, ",")
            If CurItem <> "" Then
                If InStr(CurFlds, CurItem) = 0 Then
                    OutFlds = OutFlds & CurItem & ","
                    OutHead = OutHead & GetItem(CfgHead, Itm, ",") & ","
                    OutAlgn = OutAlgn & GetItem(CfgAlgn, Itm, ",") & ","
                    OutWith = OutWith & GetItem(CfgWith, Itm, ",") & ","
                    OutSize = OutSize & GetItem(CfgSize, Itm, ",") & ","
                    OutProp = OutProp & GetItem(CfgProp, Itm, ",") & ","
                End If
            End If
        Wend
        If OutFlds <> "" Then
            OutFlds = Left(OutFlds, Len(OutFlds) - 1)
            OutHead = Left(OutHead, Len(OutHead) - 1)
            OutAlgn = Left(OutAlgn, Len(OutAlgn) - 1)
            OutWith = Left(OutWith, Len(OutWith) - 1)
            OutSize = Left(OutSize, Len(OutSize) - 1)
            OutProp = Left(OutProp, Len(OutProp) - 1)
        End If
    End If
    UseFlds = OutFlds
    UseHead = OutHead
    UseAlgn = OutAlgn
    UseWith = OutWith
    UseSize = OutSize
    UseProp = OutProp
    RetVal = True
    GetMandatoryTableFields = RetVal
End Function
Public Function GetFdsTableKeyFields(TableKey As String) As String
    Dim OutKey As String
    OutKey = GetFdsHardCodeKeys(TableKey)
    GetFdsTableKeyFields = OutKey
End Function

Public Sub BreakDownVialList(AftVial As String)
    Dim ViaBuf As String
    Dim EmptyLine As String
    Dim FldNamLst As String
    Dim FldLenLst As String
    Dim FldNam As String
    Dim FldLen As String
    Dim FldVal As String
    Dim DatPos As Long
    Dim DatLen As Long
    Dim ViaRec As String
    Dim AftBufLen As Long
    Dim ViaBufLen As Long
    Dim i As Integer
    VialTab(0).ResetContent
    FldNamLst = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR"
    FldLenLst = "1,3,4,14,14,14,14,14,14,14,14"
    EmptyLine = CreateEmptyLine(FldNamLst) & ","
    ViaBuf = AftVial & Space(120)
    ViaBufLen = Len(ViaBuf)
    AftBufLen = Len(AftVial)
    ViaRec = ""
    i = 0
    DatPos = 1
    While DatPos < ViaBufLen
        i = i + 1
        FldNam = GetItem(FldNamLst, i, ",")
        FldLen = GetItem(FldLenLst, i, ",")
        If FldNam <> "" Then
            DatLen = Val(FldLen)
            FldVal = RTrim(Mid(ViaBuf, DatPos, DatLen))
            ViaRec = ViaRec & FldVal & ","
            DatPos = DatPos + DatLen
        Else
            If ViaRec <> EmptyLine Then
                VialTab(0).InsertTextLine ViaRec, False
                ViaRec = ""
                i = 0
            Else
                DatPos = ViaBufLen
            End If
        End If
    Wend
    VialTab(0).AutoSizeColumns
    VialTab(0).Refresh
End Sub

Public Function TranslateViaField(ViaField As String, ViaApc3 As String) As String
    Dim Result As String
    Dim tmpFld As String
    Dim i As Integer
    Result = ""
    tmpFld = Left(ViaField, 3)
    i = GetRealItemNo(ViaFldNamLst, tmpFld)
    If i >= 0 Then
        Result = Trim(GetRealItem(ViaFldTxtLst, CLng(i), ","))
        Result = Result & Right(ViaField, 1)
        If ViaApc3 <> "" Then Result = Result & " (" & ViaApc3 & ")"
    End If
    TranslateViaField = Result
End Function

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        Select Case Index
            Case 0
                Me.Hide
                chkWork(Index).Value = 0
            Case 1 To 3
                If ChkBtnIdx >= 0 Then chkWork(ChkBtnIdx).Value = 0
                CfgTabIdx = Index - 1
                CfgTab(CfgTabIdx).Visible = True
                CfgTab(CfgTabIdx).ZOrder
                ChkBtnIdx = Index
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
            Case 1 To 3
                If CfgTabIdx >= 0 Then CfgTab(CfgTabIdx).Visible = False
                ChkBtnIdx = -1
                CfgTabIdx = -1
            Case Else
        End Select
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    For i = 0 To VialTab.UBound
        VialTab(i).Top = CfgTab(0).Top
        VialTab(i).Left = CfgTab(0).Left
        VialTab(i).ZOrder
    Next
    For i = 0 To CfgTab.UBound
        CfgTab(i).Top = CfgTab(0).Top
        CfgTab(i).Left = CfgTab(0).Left
        CfgTab(i).Visible = False
    Next
    
    ViaFldNamLst = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR"
    ViaFldTxtLst = ""
    ViaFldTxtLst = ViaFldTxtLst & "Display of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "IATA 3LC of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ICAO 4LC of VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "STA at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ETA at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "LND at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ONB at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "STD at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "ETD at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "OFB at VIA" & ","
    ViaFldTxtLst = ViaFldTxtLst & "AIR at VIA" & ","
    InitConfig
    ChkBtnIdx = -1
    CfgTabIdx = -1
    'Me.Show
End Sub
Private Sub InitConfig()
    Dim i As Integer
    i = 0
    CfgTab(i).ResetContent
    CfgTab(i).FontName = "Courier New"
    CfgTab(i).SetTabFontBold True
    CfgTab(i).ShowHorzScroller True
    CfgTab(i).HeaderFontSize = 17
    CfgTab(i).FontSize = 17
    CfgTab(i).LineHeight = 17
    CfgTab(i).EmptyAreaRightColor = vbWhite
    CfgTab(i).HeaderString = "NAME,TYPE,SIZE,MAIN,FILE,VIEW,COMP,ROTA,Meaning,Internal Remark"
    CfgTab(i).HeaderLengthString = "40,40,40,40,40,40,40,40,150,4000"
    CfgTab(i).ColumnWidthString = "4,4,4,4,4,4,4,4,40,100"
    CfgTab(i).ColumnAlignmentString = "L,L,R,R,R,R,R,R,L,L"
    CfgTab(i).InsertTextLine "VPFR,DATE,8,,,,,,Valid Period Begin,", False
    CfgTab(i).InsertTextLine "VPTO,DATE,8,,,,,,Valid Period End,", False
    CfgTab(i).InsertTextLine "DPFR,DATE,8,,,,,,Period Begin First Sector,", False
    CfgTab(i).InsertTextLine "STOD,DATI,12,,,,,,Scheduled Time of Departure,", False
    CfgTab(i).InsertTextLine "STOA,DATI,12,,,,,,Scheduled Time of Arrival,", False
    CfgTab(i).InsertTextLine "SKED,DATI,12,,,,,,Scheduled Time (Arr/Dep),", False
    CfgTab(i).InsertTextLine "FREQ,FREQ,7,,,,,,Any Daily Frequency,", False
    CfgTab(i).InsertTextLine "FRQW,LONG,1,,,,,,Weekly Frequency,", False
    CfgTab(i).InsertTextLine "FREA,FREQ,7,,,,,,Frequency Arrival at Home,", False
    CfgTab(i).InsertTextLine "FRED,FREQ,7,,,,,,Frequency Departure at Home,", False
    CfgTab(i).InsertTextLine "FRQA,FREQ,7,,,,,,Frequency Arrival at any Station,", False
    CfgTab(i).InsertTextLine "FRQD,FREQ,7,,,,,,Frequency Departure at any Station,", False
    CfgTab(i).AutoSizeByHeader = True
    CfgTab(i).AutoSizeColumns
    
    
    i = 2
    CfgTab(i).ResetContent
    CfgTab(i).FontName = "Courier New"
    CfgTab(i).SetTabFontBold True
    CfgTab(i).ShowHorzScroller True
    CfgTab(i).HeaderFontSize = 17
    CfgTab(i).FontSize = 17
    CfgTab(i).LineHeight = 17
    CfgTab(i).EmptyAreaRightColor = vbWhite
    CfgTab(i).HeaderString = "TABLE,TYPE,KEY FIELD RULE,TABLE KEYS,Internal Remark"
    
    CfgTab(i).HeaderLengthString = "40,40,40,40,4000"
    CfgTab(i).ColumnWidthString = "10,10,10,10,10"
    CfgTab(i).ColumnAlignmentString = "L,L,L,L,L"
    CfgTab(i).AutoSizeByHeader = True
    CfgTab(i).AutoSizeColumns
    
    i = 0
    VialTab(i).ResetContent
    VialTab(i).FontName = "Courier New"
    VialTab(i).SetTabFontBold True
    VialTab(i).ShowHorzScroller True
    VialTab(i).HeaderFontSize = 15
    VialTab(i).FontSize = 15
    VialTab(i).LineHeight = 15
    VialTab(i).EmptyAreaRightColor = vbWhite
    VialTab(i).LogicalFieldList = "FLG0,AP30,AP40,STA0,ETA0,LND0,ONB0,STD0,ETD0,OFB0,AIR0,REMA"
    VialTab(i).HeaderString = "FLG,AP3,AP4,STA,ETA,LND,ONB,STD,ETD,OFB,AIR,REM"
    VialTab(i).HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10"
    VialTab(i).ColumnWidthString = "1,3,4,14,14,14,14,14,14,14,14,100"
    VialTab(i).ColumnAlignmentString = "L,L,L,C,C,C,C,C,C,C,C,L"
    VialTab(i).AutoSizeByHeader = True
    VialTab(i).AutoSizeColumns
    
    InitMandatoryFdsFields "", "", True
End Sub

Public Sub InitMandatoryFdsFields(CfgSection As String, ListOfTables As String, ResetTab As Boolean)
    Dim UseTables As String
    Dim TableKey As String
    Dim tmpItem As String
    Dim tmpLine As String
    Dim i As Integer
    Dim Itm As Integer
    i = 1
    If ResetTab Then
        CfgTab(i).ResetContent
        CfgTab(i).SetFieldSeparator ","
        CfgTab(i).FontName = "Courier New"
        CfgTab(i).SetTabFontBold True
        CfgTab(i).ShowHorzScroller True
        CfgTab(i).HeaderFontSize = 17
        CfgTab(i).FontSize = 17
        CfgTab(i).LineHeight = 17
        CfgTab(i).EmptyAreaRightColor = vbWhite
        CfgTab(i).HeaderString = "TABLE,TYPE,FIELDS,HEADER,ALIGN,WIDTH,SIZE,Internal Remark"
        CfgTab(i).LogicalFieldList = "TBLN,TYPE,FLDS,HEAD,ALGN,WITH,SIZE,PROP,Internal Remark"
        CfgTab(i).HeaderLengthString = "40,40,40,40,40,40,40,40"
        CfgTab(i).ColumnWidthString = "10,10,10,10,10,10,10,10"
        CfgTab(i).ColumnAlignmentString = "L,L,L,L,L,L,L,L"
        CfgTab(i).SetFieldSeparator ";"
    End If
    
    UseTables = ListOfTables
    If Left(UseTables, 1) = "," Then UseTables = Mid(UseTables, 2)
    If Right(UseTables, 1) = "," Then UseTables = Left(UseTables, Len(UseTables) - 1)
    Itm = 0
    tmpItem = "START"
    While tmpItem <> ""
        Itm = Itm + 1
        tmpItem = GetItem(UseTables, Itm, ",")
        If tmpItem <> "" Then
            TableKey = tmpItem
            tmpLine = GetFdsMandFields(CfgSection, TableKey)
            CfgTab(i).InsertTextLine tmpLine, False
        End If
    Wend
    CfgTab(i).AutoSizeByHeader = True
    CfgTab(i).AutoSizeColumns

End Sub
Public Function GetCompareCtrlString(CurTab As TABLib.Tab, ForWhat As Integer) As String
    Dim CtrlStrg As String
    Dim FldList As String
    Dim FldName As String
    Dim ColCnt As Long
    Dim Itm As Integer
    Dim FldCnt As Integer
    ColCnt = CurTab.GetColumnCount
    CtrlStrg = String$(ColCnt, "1")
    FldList = CurTab.LogicalFieldList
    FldCnt = CInt(ColCnt)
    For Itm = 1 To FldCnt
        FldName = GetItem(FldList, Itm, ",")
        If Left(FldName, 1) = "'" Then Mid(CtrlStrg, Itm) = "0"
        If ForWhat = 0 Then
            'Comparison For Update
            If InStr("FTYP", FldName) > 0 Then Mid(CtrlStrg, Itm) = "I"
            If InStr("URNO,FLNU,AURN,RKEY,SKEY,RURN,RTAB", FldName) > 0 Then Mid(CtrlStrg, Itm) = "0"
            If InStr("'AF','AT','DF','DT'", FldName) > 0 Then Mid(CtrlStrg, Itm) = "R"
        End If
        If ForWhat = 1 Then
            'Just a remark: All fields for Insert
        End If
    Next
    GetCompareCtrlString = CtrlStrg
End Function

Private Function GetFdsMandFields(CfgSection As String, TableKeyName As String) As String
    Dim RetVal As Boolean
    Dim TableKey As String
    Dim UseFlds As String
    Dim UseHead As String
    Dim UseAlgn As String
    Dim UseWith As String
    Dim UseSize As String
    Dim UseProp As String
    Dim tmpLine As String
    TableKey = TableKeyName
    UseFlds = ""
    UseHead = ""
    UseAlgn = ""
    UseWith = ""
    UseSize = ""
    UseProp = ""
    If TableKey <> "" Then
        RetVal = GetFdsHardCodeDefaults(TableKey, UseFlds, UseHead, UseAlgn, UseWith, UseSize, UseProp)
        If CfgSection <> "" Then
            UseFlds = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_NAMES"), UseFlds)
            UseAlgn = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_ALIGN"), UseAlgn)
            UseWith = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_WIDTH"), UseWith)
            UseProp = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_PROPS"), UseProp)
            'Yet Unused
            'UseHead = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_TITLE"), UseHead)
            'UseSize = GetIniEntry(myIniFullName, CfgSection, "", (TableKey & "_WIDTH"), UseSize)
            UseHead = UseFlds
            UseSize = UseWith
        End If
    End If
    tmpLine = TableKey & ";" & "ANY" & ";"
    tmpLine = tmpLine & UseFlds & ";"
    tmpLine = tmpLine & UseHead & ";"
    tmpLine = tmpLine & UseAlgn & ";"
    tmpLine = tmpLine & UseWith & ";"
    tmpLine = tmpLine & UseSize & ";"
    tmpLine = tmpLine & UseProp & ";"
    tmpLine = tmpLine & ""
    GetFdsMandFields = tmpLine
End Function
Private Function GetFdsHardCodeDefaults(TableKeyName As String, UseFlds As String, UseHead As String, UseAlgn As String, UseWith As String, UseSize As String, UseProp As String) As Boolean
    Dim RetVal As Boolean
    RetVal = True
    Select Case TableKeyName
        Case "AFTTAB:PLAIN:ANY"
            UseFlds = "CSGN,STOA,STOD,ADID,FTYP,FLNO,ALC2,ALC3,FLTN,FLNS,FKEY,TIFA,TIFD,TISA,TISD,ORG3,ORG4,DES3,DES4,HOPO,REGN,ACT3,ACT5,RTYP,URNO,RKEY,AURN,SKEY"
            UseAlgn = "L,L,L,L,L,L,L,L,R,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,R,R,R,R"
            UseWith = "7,14,14,1,1,9,2,3,5,1,18,14,14,1,1,3,4,3,4,3,12,3,5,1,10,10,10,10"
            UseProp = "H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H"
            UseHead = UseFlds
            UseSize = UseWith
        Case "CCATAB:PLAIN:ANY"
            UseFlds = "CKIC,CKBS,CKES,CKIF,CKIT,CTYP,FLNO,STOD,FLNU,URNO"
            UseAlgn = "L,L,L,L,L,L,L,L,R,R"
            UseWith = "6,14,14,4,4,1,9,14,10,10"
            UseProp = "H,H,H,H,H,H,H,H,H,H"
            UseHead = UseFlds
            UseSize = UseWith
        Case Else
            RetVal = False
    End Select
    GetFdsHardCodeDefaults = RetVal
End Function
Private Function GetFdsHardCodeKeys(TableKeyName As String) As String
    Dim OutKey As String
    Select Case TableKeyName
        Case "AFTTAB:PLAIN:ANY"
            'OutKey = "FKEY,FTYP"
            'OutKey = "RULE:FKEY,VFTP"
            OutKey = "RULE:FLNO,SKDD,ADID,VFTP,VSFX"
        Case "CCATAB:PLAIN:ANY"
            'OutKey = "FLNO,STOD,CKIC,CKBS"
            'OutKey = "RULE:FLNO,SKDD,CKIC,CKBS"
            OutKey = "RULE:FLNO,SKDD,CKIC"
        Case Else
            OutKey = ""
    End Select
    GetFdsHardCodeKeys = OutKey
End Function
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        chkWork(0).Value = 1
    End If
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim i As Integer
    NewWidth = Me.ScaleWidth - (CfgTab(0).Left * 2)
    NewHeight = Me.ScaleHeight - CfgTab(0).Top - StatusBar1.Height
    For i = 0 To CfgTab.UBound
        If NewWidth > 600 Then CfgTab(i).Width = NewWidth
        If NewHeight > 600 Then CfgTab(i).Height = NewHeight
    Next
    For i = 0 To VialTab.UBound
        If NewWidth > 600 Then VialTab(i).Width = NewWidth
        If NewHeight > 600 Then VialTab(i).Height = NewHeight
    Next
End Sub

Public Function GetImpButtonFunctions(Index As Integer, UseDef As String) As String
    Dim UseTag As String
    UseTag = ""
    Select Case Index
        Case -1
        Case Else
            'UseTag = UseTag & "{=B=}" & UseDef
            'UseTag = UseTag & "{=F=}" & UseDef
            'UseTag = UseTag & "{=C=}" & UseDef
            UseTag = UseTag & UseDef
    End Select
    GetImpButtonFunctions = UseTag
End Function
