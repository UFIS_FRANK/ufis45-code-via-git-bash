VERSION 5.00
Begin VB.Form UfisWeb 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "UFIS Corporate Network"
   ClientHeight    =   2265
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6015
   Icon            =   "UfisWeb.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "UfisWeb.frx":0442
   ScaleHeight     =   2265
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   570
      Top             =   3450
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   120
      Top             =   3450
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   360
      Left            =   90
      ScaleHeight     =   330
      ScaleWidth      =   1035
      TabIndex        =   0
      Top             =   1500
      Width           =   1065
      Begin VB.Image Image2 
         Height          =   450
         Left            =   0
         Picture         =   "UfisWeb.frx":51B48
         Top             =   -60
         Width           =   25380
      End
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "COLLABORATIVE AIRPORTS"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   8.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   90
      TabIndex        =   3
      Top             =   510
      Width           =   2610
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "UFIS - CAM"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   21.75
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   90
      TabIndex        =   2
      Top             =   -60
      Width           =   2670
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Initializing ..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   1920
      Width           =   5685
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   2
      Left            =   0
      Picture         =   "UfisWeb.frx":57F2A
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   1
      Left            =   660
      Picture         =   "UfisWeb.frx":58BF4
      Top             =   960
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   0
      Left            =   90
      Picture         =   "UfisWeb.frx":594BE
      Top             =   960
      Width           =   480
   End
   Begin VB.Line Line1 
      BorderColor     =   &H000000FF&
      X1              =   90
      X2              =   3105
      Y1              =   480
      Y2              =   480
   End
   Begin VB.Image Image3 
      Height          =   555
      Left            =   2730
      Picture         =   "UfisWeb.frx":59D88
      Top             =   210
      Width           =   2595
   End
End
Attribute VB_Name = "UfisWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AttCnt As Integer
Dim ConResult As String

Public Function UfisConnect(MyParent As Form, ForWhat As String, SubText As String) As String
    AttCnt = 0
    Label2.Caption = ForWhat
    Label3.Caption = SubText
    Timer1_Timer
    Me.Show vbModal, MyParent
    UfisConnect = ConResult
End Function

Private Sub Timer1_Timer()
    Dim NewLeft As Long
    Dim NewRight As Long
    NewLeft = Image2.Left - 270
    NewRight = Image2.Width + NewLeft
    If (Timer1.Enabled) And (NewRight > 0) Then
        Image2.Left = NewLeft
    Else
        Image2.Left = -540
        Select Case AttCnt
            Case 0
                Label1.Caption = "Connecting ..."
                Image2.Left = -540
                AttCnt = 0
                Timer1.Interval = 75
                Timer1.Enabled = True
                Timer2.Enabled = True
            Case 1
                Label1.Caption = "First Retry ..."
            Case 2
                Label1.Caption = "Second Retry ..."
            Case 3
                Label1.Caption = "Third Retry ..."
            Case Else
                Timer1.Enabled = False
                Label1.Caption = "Network not available"
                AttCnt = -270
        End Select
        AttCnt = AttCnt + 1
    End If
    DoEvents
End Sub

Private Sub Timer2_Timer()
    If AttCnt < 0 Then
        AttCnt = AttCnt + 1
        If AttCnt = 0 Then Unload Me
        If AttCnt < 0 Then
            Label1.Caption = "Network not available (" & CStr(Abs(AttCnt)) & ")"
        End If
    End If
    Image1(2).Picture = Image1(0).Picture
    Image1(0).Picture = Image1(1).Picture
    Image1(1).Picture = Image1(2).Picture
    DoEvents
End Sub
