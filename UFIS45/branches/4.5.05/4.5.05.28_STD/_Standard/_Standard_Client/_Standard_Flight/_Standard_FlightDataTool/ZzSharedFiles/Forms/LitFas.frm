VERSION 5.00
Begin VB.Form LitFas 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFF00&
   BorderStyle     =   0  'None
   ClientHeight    =   6225
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6735
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "LitFas.frx":0000
   ScaleHeight     =   415
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   449
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox PopUpButtons 
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Index           =   0
      Left            =   2295
      MousePointer    =   1  'Arrow
      Picture         =   "LitFas.frx":0152
      ScaleHeight     =   39
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   146
      TabIndex        =   8
      Top             =   3150
      Visible         =   0   'False
      Width           =   2190
      Begin VB.PictureBox chkTaskMid 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   300
         Index           =   0
         Left            =   690
         Picture         =   "LitFas.frx":449C
         ScaleHeight     =   300
         ScaleWidth      =   810
         TabIndex        =   9
         Top             =   180
         Width           =   810
      End
      Begin VB.PictureBox chkTaskLft 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   0
         Left            =   270
         Picture         =   "LitFas.frx":47A0
         ScaleHeight     =   300
         ScaleWidth      =   810
         TabIndex        =   10
         Top             =   180
         Width           =   810
      End
      Begin VB.PictureBox chkTaskRgt 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   0
         Left            =   1110
         Picture         =   "LitFas.frx":4AA4
         ScaleHeight     =   300
         ScaleWidth      =   810
         TabIndex        =   11
         Top             =   180
         Width           =   810
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Index           =   2
      Left            =   2280
      MouseIcon       =   "LitFas.frx":4DA8
      Picture         =   "LitFas.frx":4EFA
      ScaleHeight     =   375
      ScaleWidth      =   2190
      TabIndex        =   7
      Top             =   840
      Visible         =   0   'False
      Width           =   2190
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   60
      Index           =   1
      Left            =   2280
      MouseIcon       =   "LitFas.frx":7A34
      Picture         =   "LitFas.frx":7B86
      ScaleHeight     =   60
      ScaleWidth      =   2190
      TabIndex        =   6
      Top             =   750
      Visible         =   0   'False
      Width           =   2190
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   660
      Index           =   0
      Left            =   2280
      MouseIcon       =   "LitFas.frx":82A8
      Picture         =   "LitFas.frx":83FA
      ScaleHeight     =   660
      ScaleWidth      =   2190
      TabIndex        =   5
      Top             =   60
      Visible         =   0   'False
      Width           =   2190
      Begin VB.Image TopIconL 
         Height          =   240
         Index           =   0
         Left            =   45
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":CFDC
         Top             =   405
         Width           =   240
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   60
      Top             =   60
   End
   Begin VB.PictureBox LeftSlider 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1920
      Index           =   0
      Left            =   45
      Picture         =   "LitFas.frx":D126
      ScaleHeight     =   1920
      ScaleWidth      =   2190
      TabIndex        =   4
      Top             =   1260
      Visible         =   0   'False
      Width           =   2190
      Begin VB.Timer LeftTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   20
         Left            =   0
         Top             =   0
      End
   End
   Begin VB.PictureBox MyHeadPanel 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFF00&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Index           =   0
      Left            =   1080
      ScaleHeight     =   11
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   146
      TabIndex        =   1
      Top             =   4890
      Visible         =   0   'False
      Width           =   2190
      Begin VB.Image MyHeadPic 
         Height          =   105
         Index           =   0
         Left            =   0
         Picture         =   "LitFas.frx":1AD68
         Top             =   0
         Width           =   1875
      End
   End
   Begin VB.PictureBox PopUpPanel 
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1905
      Index           =   0
      Left            =   2295
      MousePointer    =   1  'Arrow
      Picture         =   "LitFas.frx":1B5A2
      ScaleHeight     =   127
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   146
      TabIndex        =   0
      Top             =   1260
      Visible         =   0   'False
      Width           =   2190
      Begin VB.PictureBox PopUpFrame 
         BackColor       =   &H00C0C000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Index           =   0
         Left            =   105
         ScaleHeight     =   1485
         ScaleWidth      =   1980
         TabIndex        =   2
         Top             =   135
         Width           =   1980
         Begin VB.Label PopUpText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            ForeColor       =   &H00000000&
            Height          =   885
            Index           =   0
            Left            =   510
            MousePointer    =   1  'Arrow
            TabIndex        =   3
            Top             =   0
            Visible         =   0   'False
            Width           =   1665
         End
      End
      Begin VB.Image RightIconGroupL 
         Height          =   240
         Index           =   0
         Left            =   1650
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":2902C
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image RightIconGroupM 
         Height          =   240
         Index           =   0
         Left            =   1785
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":29176
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image RightIconGroupR 
         Height          =   240
         Index           =   0
         Left            =   1920
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":292C0
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image LeftIconGroupR 
         Height          =   240
         Index           =   0
         Left            =   315
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":2940A
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image LeftIconGroupM 
         Height          =   240
         Index           =   0
         Left            =   180
         MousePointer    =   1  'Arrow
         Picture         =   "LitFas.frx":29554
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image LeftIconGroupL 
         Height          =   240
         Index           =   0
         Left            =   45
         MouseIcon       =   "LitFas.frx":2969E
         MousePointer    =   99  'Custom
         Picture         =   "LitFas.frx":299A8
         Top             =   1650
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.Image PicPool 
      Height          =   480
      Index           =   1
      Left            =   3900
      Picture         =   "LitFas.frx":29AF2
      Top             =   4800
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image PicPool 
      Height          =   480
      Index           =   0
      Left            =   3660
      Picture         =   "LitFas.frx":29DFC
      Top             =   4800
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picPen 
      Height          =   105
      Index           =   2
      Left            =   1080
      Picture         =   "LitFas.frx":2A106
      Top             =   5400
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.Image picPen 
      Height          =   105
      Index           =   1
      Left            =   1080
      Picture         =   "LitFas.frx":2A940
      Top             =   5250
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.Image picPen 
      Height          =   105
      Index           =   0
      Left            =   1080
      Picture         =   "LitFas.frx":2B17A
      Top             =   5100
      Visible         =   0   'False
      Width           =   1875
   End
End
Attribute VB_Name = "LitFas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LeftOffset As Integer
Dim FrameOffSet As Integer
Dim TopOffset As Integer
Dim LeftSpace As Integer


Const AC_SRC_OVER = &H0
Private Type BLENDFUNCTION
  BlendOp As Byte
  BlendFlags As Byte
  SourceConstantAlpha As Byte
  AlphaFormat As Byte
End Type
Private Declare Function AlphaBlend Lib "msimg32.dll" (ByVal hDC As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal hDC As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal BLENDFUNCT As Long) As Long
Private Declare Sub RtlMoveMemory Lib "kernel32.dll" (Destination As Any, Source As Any, ByVal Length As Long)

Const HTCAPTION = 2
Const WM_NCLBUTTONDOWN = &HA1

Private Declare Function ReleaseCapture Lib "user32" () As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long) As Long

Private Declare Function CreateRectRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long
Private Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

'Private Declare Function GetObject Lib "gdi32" Alias "GetObjectA" (ByVal hObject As Long, ByVal nCount As Long, lpObject As Any) As Long
Private Declare Function GetDIBits Lib "gdi32" (ByVal aHDC As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long
'Private Declare Function SetDIBits Lib "gdi32" (ByVal hdc As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long
'Private Type BITMAP '14 bytes
'    bmType As Long
'    bmWidth As Long
'    bmHeight As Long
'    bmWidthBytes As Long
'    bmPlanes As Integer
'    bmBitsPixel As Integer
'    bmBits As Long
'End Type
Private Type BITMAPINFOHEADER '40 bytes
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type
Private Type RGBQUAD
    rgbBlue As Byte
    rgbGreen As Byte
    rgbRed As Byte
    rgbReserved As Byte
End Type
Private Type BITMAPINFO
    bmiHeader As BITMAPINFOHEADER
    bmiColors As RGBQUAD
End Type
Private Const DIB_RGB_COLORS = 0&
Private Const BI_RGB = 0&

Private Const pixR As Integer = 3
Private Const pixG As Integer = 2
Private Const pixB As Integer = 1
Private Sub UnRGB(ByRef color As Long, ByRef r As Byte, ByRef g As Byte, ByRef b As Byte)
    r = color And &HFF&
    g = (color And &HFF00&) \ &H100&
    b = (color And &HFF0000) \ &H10000
End Sub
' Restrict the form to its "transparent" pixels.
Private Sub TrimPicture(ByVal pic As PictureBox, ByVal transparent_color As Long)
Const RGN_OR = 2
Dim bitmap_info As BITMAPINFO
Dim pixels() As Byte
Dim bytes_per_scanLine As Integer
Dim pad_per_scanLine As Integer
Dim transparent_r As Byte
Dim transparent_g As Byte
Dim transparent_b As Byte
Dim wid As Integer
Dim hgt As Integer
Dim x As Integer
Dim y As Integer
Dim start_x As Integer
Dim stop_x As Integer
Dim combined_rgn As Long
Dim new_rgn As Long

    ' Prepare the bitmap description.
    With bitmap_info.bmiHeader
        .biSize = 40
        .biWidth = pic.ScaleWidth
        ' Use negative height to scan top-down.
        .biHeight = -pic.ScaleHeight
        .biPlanes = 1
        .biBitCount = 32
        .biCompression = BI_RGB
        bytes_per_scanLine = ((((.biWidth * .biBitCount) + 31) \ 32) * 4)
        pad_per_scanLine = bytes_per_scanLine - (((.biWidth * .biBitCount) + 7) \ 8)
        .biSizeImage = bytes_per_scanLine * Abs(.biHeight)
    End With

    ' Load the bitmap's data.
    wid = pic.ScaleWidth
    hgt = pic.ScaleHeight
    ReDim pixels(1 To 4, 0 To wid - 1, 0 To hgt - 1)
    GetDIBits pic.hDC, pic.Image, _
        0, pic.ScaleHeight, pixels(1, 0, 0), _
        bitmap_info, DIB_RGB_COLORS

    ' Break the transparent color into its components.
    UnRGB transparent_color, transparent_r, transparent_g, transparent_b

    ' Create the PictureBox's regions.
    For y = 0 To hgt - 1
        ' Create a region for this row.
        x = 1
        Do While x < wid
            start_x = 0
            stop_x = 0

            ' Find the next non-transparent column.
            Do While x < wid
                If pixels(pixR, x, y) <> transparent_r Or _
                   pixels(pixG, x, y) <> transparent_g Or _
                   pixels(pixB, x, y) <> transparent_b _
                Then
                    Exit Do
                End If
                x = x + 1
            Loop
            start_x = x

            ' Find the next transparent column.
            Do While x < wid
                If pixels(pixR, x, y) = transparent_r And _
                   pixels(pixG, x, y) = transparent_g And _
                   pixels(pixB, x, y) = transparent_b _
                Then
                    Exit Do
                End If
                x = x + 1
            Loop
            stop_x = x

            ' Make a region from start_x to stop_x.
            If start_x < wid Then
                If stop_x >= wid Then stop_x = wid - 1

                ' Create the region.
                new_rgn = CreateRectRgn( _
                    start_x, y, stop_x, y + 1)

                ' Add it to what we have so far.
                If combined_rgn = 0 Then
                    combined_rgn = new_rgn
                Else
                    CombineRgn combined_rgn, _
                        combined_rgn, new_rgn, RGN_OR
                    DeleteObject new_rgn
                End If
            End If
        Loop
    Next y
    
    ' Restrict the PictureBox to the region.
    SetWindowRgn pic.hWnd, combined_rgn, True
    new_rgn = CreateRectRgn(0, 0, Me.ScaleWidth, Me.ScaleHeight)
    new_rgn = combined_rgn
    
    SetWindowRgn Me.hWnd, new_rgn, True
    DeleteObject new_rgn
    DeleteObject combined_rgn
End Sub
' Restrict the form to its "transparent" pixels.
Private Sub TrimMyForm(ByVal transparent_color As Long)
Const RGN_OR = 2
Dim bitmap_info As BITMAPINFO
Dim pixels() As Byte
Dim bytes_per_scanLine As Integer
Dim pad_per_scanLine As Integer
Dim transparent_r As Byte
Dim transparent_g As Byte
Dim transparent_b As Byte
Dim wid As Integer
Dim hgt As Integer
Dim x As Integer
Dim y As Integer
Dim start_x As Integer
Dim stop_x As Integer
Dim combined_rgn As Long
Dim new_rgn As Long
Dim idx As Integer

    ' Prepare the bitmap description.
    With bitmap_info.bmiHeader
        .biSize = 40
        .biWidth = Me.ScaleWidth
        ' Use negative height to scan top-down.
        .biHeight = -Me.ScaleHeight
        .biPlanes = 1
        .biBitCount = 32
        .biCompression = BI_RGB
        bytes_per_scanLine = ((((.biWidth * .biBitCount) + 31) \ 32) * 4)
        pad_per_scanLine = bytes_per_scanLine - (((.biWidth * .biBitCount) + 7) \ 8)
        .biSizeImage = bytes_per_scanLine * Abs(.biHeight)
    End With

    ' Load the bitmap's data.
    wid = Me.ScaleWidth
    hgt = Me.ScaleHeight
    ReDim pixels(1 To 4, 0 To wid - 1, 0 To hgt - 1)
    GetDIBits Me.hDC, Me.Image, _
        0, Me.ScaleHeight, pixels(1, 0, 0), _
        bitmap_info, DIB_RGB_COLORS

    ' Break the transparent color into its components.
    UnRGB transparent_color, transparent_r, transparent_g, transparent_b

    ' Create the metureBox's regions.
    For y = 0 To hgt - 1
        ' Create a region for this row.
        x = 1
        Do While x < wid
            start_x = 0
            stop_x = 0

            ' Find the next non-transparent column.
            Do While x < wid
                If pixels(pixR, x, y) <> transparent_r Or _
                   pixels(pixG, x, y) <> transparent_g Or _
                   pixels(pixB, x, y) <> transparent_b _
                Then
                    Exit Do
                End If
                x = x + 1
            Loop
            start_x = x

            ' Find the next transparent column.
            Do While x < wid
                If pixels(pixR, x, y) = transparent_r And _
                   pixels(pixG, x, y) = transparent_g And _
                   pixels(pixB, x, y) = transparent_b _
                Then
                    Exit Do
                End If
                x = x + 1
            Loop
            stop_x = x

            ' Make a region from start_x to stop_x.
            If start_x < wid Then
                If stop_x >= wid Then stop_x = wid - 1

                ' Create the region.
                new_rgn = CreateRectRgn( _
                    start_x, y, stop_x, y + 1)

                ' Add it to what we have so far.
                If combined_rgn = 0 Then
                    combined_rgn = new_rgn
                Else
                    CombineRgn combined_rgn, _
                        combined_rgn, new_rgn, RGN_OR
                    DeleteObject new_rgn
                End If
            End If
        Loop
    Next y
    
    start_x = 0
    stop_x = LeftOffset + PopUpPanel(0).Width
    y = MyHeadPanel(0).Height
    hgt = PopUpPanel(0).Height
    For idx = 1 To 10
        start_x = LeftOffset
        'new_rgn = CreateRectRgn(start_x, y, stop_x, y + hgt)
        'CombineRgn combined_rgn, combined_rgn, new_rgn, RGN_OR
        'DeleteObject new_rgn
        y = y + hgt
        
        'start_x = PopUpSignal(0).Left
        'new_rgn = CreateRectRgn(start_x, y, start_x + PopUpSignal(0).Width, y + PopUpSignal(0).Height)
        'CombineRgn combined_rgn, combined_rgn, new_rgn, RGN_OR
        'DeleteObject new_rgn
        'y = y + PopUpSignal(0).Height
        
        
        'start_x = PopUpButtons(0).Left
        'new_rgn = CreateRectRgn(start_x, y, start_x + PopUpButtons(0).Width, y + PopUpButtons(0).Height)
        'CombineRgn combined_rgn, combined_rgn, new_rgn, RGN_OR
        'DeleteObject new_rgn
        'Y = Y + PopUpButtons(0).Height + FrameOffSet
    Next
    ' Restrict the metureBox to the region.
    SetWindowRgn Me.hWnd, combined_rgn, True
    DeleteObject combined_rgn
End Sub


Public Sub AddNewPopUpMessage(AnyMsg As String)
    Static MsgNo As Integer
    Dim tmpMsg As String
    Dim tmpRpl As String
    Dim idx As Integer
    
    idx = PopUpPanel.UBound + 1
    'If idx > 0 Then
        Load PopUpPanel(idx)
        'We set it invisible over top first
        PopUpPanel(idx).Top = -PopUpPanel(idx).Height
        PopUpPanel(idx).Left = PopUpPanel(0).Left
        Load PopUpFrame(idx)
        Set PopUpFrame(idx).Container = PopUpPanel(idx)
        Load PopUpText(idx)
        Set PopUpText(idx).Container = PopUpFrame(idx)
        PopUpText(idx).Visible = True
        PopUpFrame(idx).Visible = True
        
        Load LeftIconGroupL(idx)
        Set LeftIconGroupL(idx).Container = PopUpPanel(idx)
        LeftIconGroupL(idx).MouseIcon = PicPool(0).Picture
        LeftIconGroupL(idx).Visible = True
        Load LeftIconGroupM(idx)
        Set LeftIconGroupM(idx).Container = PopUpPanel(idx)
        LeftIconGroupM(idx).Visible = True
        Load LeftIconGroupR(idx)
        Set LeftIconGroupR(idx).Container = PopUpPanel(idx)
        LeftIconGroupR(idx).Visible = True
        
        Load RightIconGroupL(idx)
        Set RightIconGroupL(idx).Container = PopUpPanel(idx)
        RightIconGroupL(idx).Visible = True
        Load RightIconGroupM(idx)
        Set RightIconGroupM(idx).Container = PopUpPanel(idx)
        RightIconGroupM(idx).Visible = True
        Load RightIconGroupR(idx)
        Set RightIconGroupR(idx).Container = PopUpPanel(idx)
        RightIconGroupR(idx).Visible = True
        
        PopUpPanel(idx).Visible = True
        
        'Load PopUpSignal(idx)
        'PopUpSignal(idx).Top = -PopUpSignal(idx).Height
        'PopUpSignal(idx).Left = PopUpSignal(0).Left
        'PopUpSignal(idx).Visible = True
        
        Load PopUpButtons(idx)
        PopUpButtons(idx).Top = -PopUpButtons(idx).Height
        PopUpButtons(idx).Left = PopUpButtons(0).Left
        Load chkTaskLft(idx)
        Set chkTaskLft(idx).Container = PopUpButtons(idx)
        TrimPicture chkTaskLft(idx), vbMagenta
        chkTaskLft(idx).Visible = True
        Load chkTaskMid(idx)
        Set chkTaskMid(idx).Container = PopUpButtons(idx)
        TrimPicture chkTaskMid(idx), vbMagenta
        chkTaskMid(idx).Visible = True
        Load chkTaskRgt(idx)
        Set chkTaskRgt(idx).Container = PopUpButtons(idx)
        TrimPicture chkTaskRgt(idx), vbMagenta
        chkTaskRgt(idx).Visible = True
        chkTaskLft(idx).ZOrder
        chkTaskRgt(idx).ZOrder
        chkTaskMid(idx).ZOrder
        PopUpButtons(idx).Visible = True
        SetButtonCaption chkTaskMid(idx), "Confirm"
        Load LeftSlider(idx)
        Load LeftTimer(idx)
        LeftSlider(idx).Left = PopUpPanel(idx).Left
        LeftSlider(idx).Tag = ""
        PopUpPanel(idx).ZOrder
        'LeftSlider(idx).Visible = True
    'Else
    'End If
    MsgNo = MsgNo + 1
    'tmpMsg = "(Notification No." & CStr(MsgNo) & ")"
    'tmpMsg = tmpMsg & vbNewLine & AnyMsg
    'tmpMsg = vbNewLine
    tmpMsg = ""
    'tmpMsg = tmpMsg & vbNewLine
    tmpMsg = tmpMsg & "An update on flight SQ123/24" & vbNewLine & "has been rejected." & vbNewLine & "Please inform your Administrator to update the related CGMS basic data a.s.a.p."
    tmpRpl = CStr(MsgNo)
    tmpRpl = Right("000" & tmpRpl, 3)
    tmpRpl = "SQ" & tmpRpl & "/24 (E7)"
    tmpMsg = Replace(tmpMsg, "SQ123/24", tmpRpl, 1, 1, vbBinaryCompare)
    PopUpText(idx).Caption = tmpMsg
    PopUpText(idx).Tag = tmpMsg
    AdjustMyHeight
    Me.Show
End Sub
Private Function GetFreePopupIndex() As Integer

End Function
Private Sub AdjustMyHeight()
    Dim tmpText As String
    Dim tmpPatch As String
    Dim NewSize As Long
    Dim NewTop As Integer
    Dim ibgn As Integer
    Dim iend As Integer
    Dim idx As Integer
    Dim iCnt As Integer
    Dim iMax As Integer
    Dim ivis As Integer
    NewSize = 0
    iMax = 3
    iCnt = 0
    ibgn = -1
    iend = -1
    For idx = 1 To PopUpPanel.UBound
        If PopUpPanel(idx).Visible Then
            iCnt = iCnt + 1
            If ibgn < 0 Then ibgn = idx
            If iCnt <= iMax Then
                ivis = iCnt
                iend = idx
            End If
        End If
    Next
    iMax = iCnt
    
    If ibgn < 0 Then ibgn = 0
    If iend < 0 Then iend = ibgn
    
    TopPanel(1).Visible = True
    TopPanel(2).Visible = False
    NewTop = TopPanel(0).Top + TopPanel(0).Height
    TopPanel(1).Top = NewTop
    NewTop = NewTop + TopPanel(1).Height
    NewTop = NewTop + FrameOffSet
    'NewTop = FrameOffSet
    If (iend > 0) And (iend <= PopUpPanel.UBound) Then
        For idx = iend To ibgn Step -1
            If PopUpPanel(idx).Visible Then
                PopUpPanel(idx).Top = NewTop
                LeftSlider(idx).Top = NewTop
                'tmpText = PopUpText(idx).Tag
                tmpPatch = "Notification " & CStr(ivis) & "/" & CStr(iMax) & ""
                'tmpText = tmpPatch & vbNewLine & tmpText '& vbNewLine & tmpPatch
                'PopUpText(idx).Caption = tmpText
                PopUpText(idx).ToolTipText = tmpPatch
                NewTop = NewTop + PopUpPanel(idx).Height
                'PopUpSignal(idx).Top = NewTop
                'NewTop = NewTop + PopUpSignal(idx).Height
                PopUpButtons(idx).Top = NewTop
                NewTop = NewTop + PopUpButtons(idx).Height
                NewTop = NewTop + FrameOffSet
                ivis = ivis - 1
            End If
        Next
    End If
    NewSize = (NewTop * 15)
    Me.Height = NewSize
    Me.Top = Screen.Height - NewSize
    'SetFormOnTop Me, True
End Sub

Private Sub chkTaskMid_Click(Index As Integer)
    'If chkTaskMid(Index).Value = 1 Then
    '    chkTaskMid(Index).BackColor = LightGreen
    '    chkTaskMid(Index).Refresh
        PopUpPanel(Index).Visible = False
        LeftTimer(Index).Enabled = False
        LeftSlider(Index).Tag = ""
        LeftSlider(Index).Visible = False
        'PopUpSignal(Index).Visible = False
        PopUpButtons(Index).Visible = False
        AdjustMyHeight
    '    chkTaskMid(Index).Value = 0
    'Else
    '    chkTaskMid(Index).BackColor = vbButtonFace
    '    chkTaskMid(Index).Refresh
    'End If
End Sub

Private Sub chkTaskMid_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    chkTaskMid(Index).Top = chkTaskMid(Index).Top + 1
    chkTaskMid(Index).Left = chkTaskMid(Index).Left + 1
End Sub

Private Sub chkTaskMid_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    chkTaskMid(Index).Top = chkTaskMid(Index).Top - 1
    chkTaskMid(Index).Left = chkTaskMid(Index).Left - 1
End Sub

Private Sub Form_Activate()
    Static IsDone As Boolean
    If Not IsDone Then
        IsDone = True
        AdjustMyHeight
        Me.Show
    End If
End Sub

Private Sub Form_Click()
    'Me.Hide
End Sub

Private Sub Form_Load()
    Dim idx As Integer
    Dim i As Integer
    Dim NewTop As Integer
    Dim DspWidth As Integer
    Dim TopDiff As Integer
    TopOffset = TopPanel(0).Top + TopPanel(0).Height
    'TopOffset = 10
    FrameOffSet = 8
    
    LeftSpace = LeftSlider(0).Width
    
    LeftOffset = LeftSpace
    'LeftOffset = LeftOffset + PopUpButtons(0).Left
    MyHeadPanel(0).Picture = MyHeadPic(0).Picture
    TrimPicture MyHeadPanel(0), vbCyan
    
    TrimPicture chkTaskRgt(0), vbMagenta
    
    
    
    'MyHeadPanel(0).Left = LeftOffset
    'MyHeadPanel(0).Width = PopUpPanel(0).Width
    'MyHeadPanel(0).ZOrder
    PopUpText(0).Top = 0
    PopUpText(0).Left = 0
    PopUpText(0).Width = PopUpFrame(0).ScaleWidth
    PopUpText(0).Height = PopUpFrame(0).ScaleHeight
    'TopLabel(1).Top = 0
    'TopLabel(1).Left = 0
    'TopLabel(1).Width = XPopUpFrame(1).ScaleWidth
    'TopLabel(1).Height = XPopUpFrame(1).ScaleHeight
    'TopPanel(0).Visible = True
    'TopLabel(1).Visible = True
    
    If PopUpButtons(0).Width >= PopUpPanel(0).Width Then
        LeftOffset = LeftOffset + ((PopUpButtons(0).Width - PopUpPanel(0).Width) / 2)
    Else
        LeftOffset = LeftOffset + ((PopUpPanel(0).Width - PopUpButtons(0).Width) / 2)
    End If
    
    PopUpPanel(0).Top = 0
    PopUpPanel(0).Left = LeftOffset
    PopUpFrame(0).Visible = True
    PopUpText(0).Visible = True
    PopUpPanel(0).Visible = False
    
    LeftSlider(0).Width = 0
    LeftSlider(0).Left = LeftOffset - LeftSlider(0).Width
    LeftSlider(0).Height = PopUpFrame(0).Height
    
    
    'PopUpSignal(0).Left = LeftOffset + (PopUpPanel(0).Width - PopUpSignal(0).Width) / 2
    PopUpButtons(0).Left = LeftOffset + (PopUpPanel(0).Width - PopUpButtons(0).Width) / 2
    
    If PopUpPanel(0).Width > PopUpButtons(0).Width Then
        DspWidth = ((PopUpPanel(0).Left + PopUpPanel(0).Width) * 15) + (FrameOffSet * 15)
    Else
        DspWidth = ((PopUpButtons(0).Left + PopUpButtons(0).Width) * 15) + (FrameOffSet * 15)
    End If
    
    Me.Width = DspWidth
    Me.Left = Screen.Width - (Me.Width)
    Me.Height = Screen.Height '+ ((PopUpPanel(0).Height * 15) * 2)
    
    TopPanel(0).Left = LeftOffset + (PopUpPanel(0).Width - TopPanel(0).Width)
    For i = 0 To TopPanel.UBound
        TopPanel(i).Left = TopPanel(0).Left
        TopPanel(i).Visible = True
    Next
    
    MyHeadPanel(0).Left = LeftOffset + (PopUpPanel(0).Width - MyHeadPic(0).Width) - 6
    MyHeadPanel(0).Top = TopPanel(0).Top + TopPanel(0).Height - MyHeadPanel(0).Height - 5
    'Me.AutoRedraw = True
    
    NewTop = TopOffset
    'Me.PaintPicture MyHeadPanel(0).Picture, MyHeadPanel(0).Left, NewTop
    
    'TopDiff = 0
    'For i = 1 To 3
    '    TopDiff = TopDiff + MyHeadPanel(i).Height
    '    MyHeadPanel(i).Picture = MyHeadPic(i).Picture
    '    'TrimPicture MyHeadPanel(i), vbCyan
    '    MyHeadPanel(i).Left = LeftOffset - MyHeadPic(i).Width
    'Next
    'TopDiff = TopDiff - MyHeadPanel(3).Height
    
    'NewTop = TopOffset + MyHeadPanel(0).Height + FrameOffSet
    NewTop = TopOffset + FrameOffSet
    For idx = 1 To 3
        'Me.PaintPicture PopUpPanel(0).Picture, PopUpPanel(0).Left, NewTop
        'NewTop = NewTop + PopUpPanel(0).Height
        'Me.PaintPicture PopUpSignal(0).Picture, PopUpSignal(0).Left, NewTop
        'NewTop = NewTop + PopUpSignal(0).Height
        'Me.PaintPicture PopUpButtons(0).Picture, PopUpButtons(0).Left, NewTop
        'NewTop = NewTop + PopUpButtons(0).Height + FrameOffSet
    Next
    'NewTop = 0
    'For i = 0 To TopPanel.UBound
    '    TopPanel(i).Visible = True
    '    TopLabel(i).Visible = True
    '    TopPanel(i).Top = NewTop
    '    TopPanel(i).Left = TopPanel(0).Left
    '    NewTop = NewTop + TopPanel(i).Height
    'Next
    
    'TopPanel(0).ZOrder
    'TopPanel(0).Top = -TopPanel(0).Height - 60
    'TopPanel(0).Visible = False
    idx = 0
    If idx = 0 Then
        SetLayeredForm
        'Me.Picture = Me.Image
        'Me.AutoRedraw = False
        'Me.BackColor = DarkGrey
    End If
        
    MyHeadPanel(0).Visible = True
    MyHeadPanel(0).ZOrder
        
    Timer1_Timer
    Timer1.Enabled = True
    'Me.Height = Screen.Height
    Form_Activate
    'Me.Height = (NewTop * 15)
    'Me.Show

End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ReturnVal As Long
    If Button = 1 Then
        MyHeadPic(0).Picture = picPen(2).Picture
        MyHeadPic(0).Refresh
        x = ReleaseCapture()
        ReturnVal = SendMessage(Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
        MyHeadPic(0).Picture = picPen(0).Picture
        MyHeadPic(0).Refresh
    End If
End Sub


Private Sub MyHeadPic_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ReturnVal As Long
    If Button = 1 Then
        MyHeadPic(Index).Picture = picPen(2).Picture
        MyHeadPic(Index).Refresh
        x = ReleaseCapture()
        ReturnVal = SendMessage(Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
        MyHeadPic(Index).Picture = picPen(0).Picture
        MyHeadPic(Index).Refresh
    End If
End Sub

Private Sub MyHeadPic_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    'If Button = 1 Then
    '    MyHeadPic(Index).Picture = picPen(1).Picture
    'End If
End Sub

Private Sub LeftIconGroupL_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim tmpTag As String
    LeftTimer(Index).Enabled = False
    tmpTag = LeftSlider(Index).Tag
    If tmpTag = "" Then tmpTag = "CLS"
    Select Case tmpTag
        Case "OPN"
            tmpTag = "CLS"
        Case "CLS"
            tmpTag = "OPN"
        Case Else
    End Select
    LeftSlider(Index).Tag = tmpTag
    LeftSlider(Index).Visible = True
    LeftTimer(Index).Enabled = True
End Sub

Private Sub PopUpPanel_Click(Index As Integer)
    'PopUpPanel(Index).Visible = False
    'AdjustMyHeight
End Sub

Private Sub PopUpText_Click(Index As Integer)
    'PopUpPanel(Index).Visible = False
    'AdjustMyHeight
End Sub
Private Sub SetLayeredForm()
   Dim lStyle As Long
   Dim transColor As Long
   transColor = vbCyan
   lStyle = GetWindowLong(Me.hWnd, GWL_EXSTYLE)
   lStyle = lStyle Or WS_EX_LAYERED
   SetWindowLong Me.hWnd, GWL_EXSTYLE, lStyle
   SetLayeredWindowAttributes Me.hWnd, transColor, 255, LWA_COLORKEY Or LWA_ALPHA
End Sub

Private Sub LeftTimer_Timer(Index As Integer)
    Dim tmpTag As String
    Dim NewLeft As Long
    Dim EndLeft As Long
    LeftTimer(Index).Enabled = False
    tmpTag = LeftSlider(Index).Tag
    Select Case tmpTag
        Case "OPN"
            EndLeft = PopUpPanel(Index).Left - LeftSlider(Index).Width
            NewLeft = LeftSlider(Index).Left - 8
            If NewLeft < EndLeft Then NewLeft = EndLeft
            If NewLeft >= EndLeft Then
                LeftSlider(Index).Left = NewLeft
                If NewLeft > EndLeft Then
                    LeftTimer(Index).Enabled = True
                Else
                    LeftIconGroupL(Index).MouseIcon = PicPool(1).Picture
                End If
            End If
        Case "CLS"
            EndLeft = PopUpPanel(Index).Left
            NewLeft = LeftSlider(Index).Left + 8
            If NewLeft > EndLeft Then NewLeft = EndLeft
            If NewLeft <= EndLeft Then
                LeftSlider(Index).Left = NewLeft
                If NewLeft < EndLeft Then
                    LeftTimer(Index).Enabled = True
                Else
                    LeftIconGroupL(Index).MouseIcon = PicPool(0).Picture
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub Timer1_Timer()
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim tmpTime As String
    Dim i As Integer
    On Error Resume Next
    Timer1.Enabled = False
    tmpTime = Format(Now, "ddd dd mmm yy - hh:mm:ss")
    tmpTime = UCase(tmpTime)
    NewSize = TextWidth(tmpTime)
    NewLeft = ((TopPanel(0).Width - NewSize) / 2) * 15
    NewLeft = NewLeft + 30
    TopPanel(0).Cls
    TopPanel(0).ForeColor = vbBlack
    TopPanel(0).CurrentX = NewLeft
    TopPanel(0).CurrentY = 165
    TopPanel(0).Print tmpTime
    TopPanel(0).ForeColor = vbWhite
    TopPanel(0).CurrentX = NewLeft - 15
    TopPanel(0).CurrentY = 150
    TopPanel(0).Print tmpTime
    
    Timer1.Enabled = True
End Sub
Private Sub SetButtonCaption(UseButton As PictureBox, SetCaption As String)
    Dim NewLeft As Long
    Dim NewSize As Long
    UseButton.Cls
    NewSize = TextWidth(SetCaption)
    NewLeft = ((UseButton.Width - NewSize) / 2) * 15
    UseButton.CurrentX = NewLeft
    UseButton.CurrentY = 45
    UseButton.Print SetCaption
End Sub
Private Sub TopLabel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ReturnVal As Long
    Dim idx As Integer
    idx = 0
    If Button = 1 Then
        MyHeadPic(idx).Picture = picPen(2).Picture
        MyHeadPic(idx).Refresh
        x = ReleaseCapture()
        ReturnVal = SendMessage(Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
        MyHeadPic(idx).Picture = picPen(0).Picture
        MyHeadPic(idx).Refresh
    End If
End Sub

Private Sub TopPanel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ReturnVal As Long
    Dim idx As Integer
    idx = 0
    If Button = 1 Then
        MyHeadPic(idx).Picture = picPen(2).Picture
        MyHeadPic(idx).Refresh
        x = ReleaseCapture()
        ReturnVal = SendMessage(Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
        MyHeadPic(idx).Picture = picPen(0).Picture
        MyHeadPic(idx).Refresh
    End If
End Sub
