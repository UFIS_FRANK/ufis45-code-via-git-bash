VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Begin VB.Form StatusChart 
   Caption         =   "UFIS GOCC Status Chart"
   ClientHeight    =   9600
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15135
   Icon            =   "StatusChart.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9600
   ScaleWidth      =   15135
   Begin VB.PictureBox DepFlight 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   330
      Index           =   0
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   7335
      TabIndex        =   68
      Top             =   5850
      Visible         =   0   'False
      Width           =   7365
      Begin VB.Image picDepStat 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   15
         Picture         =   "StatusChart.frx":014A
         Stretch         =   -1  'True
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblDepFtyp 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   300
         TabIndex        =   93
         ToolTipText     =   "Status"
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblDepPstd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4935
         TabIndex        =   79
         ToolTipText     =   "Bay"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblDepOfbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4305
         TabIndex        =   78
         ToolTipText     =   "ATD (OFB)"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1680
         TabIndex        =   77
         Top             =   15
         Width           =   300
      End
      Begin VB.Label lblDepVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   2535
         TabIndex        =   76
         ToolTipText     =   "Next Station"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblDepEtdi 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3705
         TabIndex        =   75
         ToolTipText     =   "ETD"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepStod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3105
         TabIndex        =   74
         ToolTipText     =   "STD"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepDes3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1995
         TabIndex        =   73
         ToolTipText     =   "Destination"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblDepFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   585
         TabIndex        =   72
         Top             =   15
         Width           =   1110
      End
      Begin VB.Label lblDepGtd1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5430
         TabIndex        =   71
         ToolTipText     =   "Gate"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblDepRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5955
         TabIndex        =   70
         ToolTipText     =   "Aircraft"
         Top             =   15
         Width           =   855
      End
      Begin VB.Label lblDepAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6795
         TabIndex        =   69
         ToolTipText     =   "Type"
         Top             =   15
         Width           =   525
      End
   End
   Begin VB.PictureBox ArrFlight 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   330
      Index           =   0
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   7335
      TabIndex        =   56
      Top             =   5460
      Visible         =   0   'False
      Width           =   7365
      Begin VB.Image picArrStat 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   7050
         Picture         =   "StatusChart.frx":0294
         Stretch         =   -1  'True
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblArrFtyp 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6765
         TabIndex        =   92
         ToolTipText     =   "Status"
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblArrAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6225
         TabIndex        =   67
         ToolTipText     =   "Type"
         Top             =   15
         Width           =   525
      End
      Begin VB.Label lblArrRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5385
         TabIndex        =   66
         ToolTipText     =   "Aircraft"
         Top             =   15
         Width           =   855
      End
      Begin VB.Label lblArrGta1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4860
         TabIndex        =   65
         ToolTipText     =   "Gate"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblArrFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   15
         TabIndex        =   64
         Top             =   15
         Width           =   1110
      End
      Begin VB.Label lblArrOrg3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1425
         TabIndex        =   63
         ToolTipText     =   "Origin"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblArrStoa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   2535
         TabIndex        =   62
         ToolTipText     =   "STA"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrEtai 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3135
         TabIndex        =   61
         ToolTipText     =   "ETA"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1965
         TabIndex        =   60
         ToolTipText     =   "Previous Station"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblArrFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1110
         TabIndex        =   59
         Top             =   15
         Width           =   300
      End
      Begin VB.Label lblArrOnbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3735
         TabIndex        =   58
         ToolTipText     =   "ATA (ONB)"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrPsta 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4365
         TabIndex        =   57
         ToolTipText     =   "Bay"
         Top             =   15
         Width           =   510
      End
   End
   Begin VB.PictureBox TimeScroll2 
      Height          =   1125
      Index           =   0
      Left            =   10200
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   43
      Top             =   1020
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   83
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   82
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   270
         Width           =   255
      End
   End
   Begin VB.PictureBox TimeScroll1 
      Height          =   1125
      Index           =   0
      Left            =   1380
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   40
      Top             =   1020
      Visible         =   0   'False
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   84
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   81
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll3 
      Height          =   585
      Index           =   0
      Left            =   10170
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   25
      Top             =   4530
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   86
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll1 
      Height          =   585
      Index           =   0
      Left            =   1350
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   23
      Top             =   4530
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   85
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll2 
      Height          =   315
      Index           =   0
      Left            =   1680
      ScaleHeight     =   255
      ScaleWidth      =   8415
      TabIndex        =   21
      Top             =   4530
      Width           =   8475
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   0
         LargeChange     =   30
         Left            =   0
         Max             =   3600
         SmallChange     =   5
         TabIndex        =   22
         Top             =   0
         Width           =   8415
      End
   End
   Begin VB.PictureBox BottomRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1680
      ScaleHeight     =   240
      ScaleWidth      =   8415
      TabIndex        =   20
      Top             =   4830
      Width           =   8475
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                                         "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   1
         Left            =   60
         TabIndex        =   47
         Top             =   15
         Width           =   1845
      End
   End
   Begin VB.PictureBox TopRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1710
      ScaleHeight     =   240
      ScaleWidth      =   8415
      TabIndex        =   19
      Top             =   1020
      Width           =   8475
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                     "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   0
         Left            =   60
         TabIndex        =   46
         Top             =   15
         Width           =   945
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00800000&
      Height          =   2175
      Index           =   0
      Left            =   1680
      ScaleHeight     =   2115
      ScaleWidth      =   8415
      TabIndex        =   18
      Top             =   2250
      Width           =   8475
      Begin VB.PictureBox VScrollArea 
         BackColor       =   &H00E0E0E0&
         Height          =   1845
         Index           =   0
         Left            =   90
         ScaleHeight     =   1785
         ScaleWidth      =   8205
         TabIndex        =   29
         Top             =   90
         Width           =   8265
         Begin VB.PictureBox HScrollArea 
            Height          =   1485
            Index           =   0
            Left            =   960
            ScaleHeight     =   1425
            ScaleWidth      =   6165
            TabIndex        =   34
            Top             =   120
            Width           =   6225
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               FillColor       =   &H00FFFFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   1
               Left            =   120
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   90
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   30
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   89
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox ChartLine2 
               AutoRedraw      =   -1  'True
               Height          =   420
               Index           =   0
               Left            =   210
               ScaleHeight     =   360
               ScaleWidth      =   5775
               TabIndex        =   36
               Top             =   120
               Width           =   5835
               Begin VB.Image picSortTime 
                  Height          =   240
                  Index           =   0
                  Left            =   2790
                  Picture         =   "StatusChart.frx":03DE
                  ToolTipText     =   "Time Check Point"
                  Top             =   -30
                  Width           =   240
               End
               Begin VB.Image picDep1 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   4680
                  Picture         =   "StatusChart.frx":0528
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep4 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   5490
                  Picture         =   "StatusChart.frx":0C2A
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr4 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   2520
                  Picture         =   "StatusChart.frx":11B4
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep3 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   5220
                  Picture         =   "StatusChart.frx":12FE
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep2 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   4950
                  Picture         =   "StatusChart.frx":1888
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr3 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   2190
                  Picture         =   "StatusChart.frx":1E12
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr2 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   1890
                  Picture         =   "StatusChart.frx":239C
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Label lblArrJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   420
                  TabIndex        =   98
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   690
                  TabIndex        =   97
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   825
               End
               Begin VB.Label lblDepJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3540
                  TabIndex        =   96
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblDepJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3870
                  TabIndex        =   95
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   765
               End
               Begin VB.Image picArr1 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   1560
                  Picture         =   "StatusChart.frx":27DE
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Label lblBarLineColor 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00808080&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   300
                  Index           =   0
                  Left            =   30
                  TabIndex        =   94
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   285
               End
               Begin VB.Label lblTowBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   3060
                  TabIndex        =   88
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.Label lblDepBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   3480
                  TabIndex        =   87
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   360
                  TabIndex        =   55
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   1005
               End
            End
            Begin VB.PictureBox TimeLine2 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   6090
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   35
               Top             =   0
               Width           =   45
            End
         End
         Begin VB.PictureBox RightScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1485
            Index           =   0
            Left            =   7260
            ScaleHeight     =   1425
            ScaleWidth      =   795
            TabIndex        =   32
            Top             =   120
            Width           =   855
            Begin VB.PictureBox ChartLine3 
               AutoRedraw      =   -1  'True
               Height          =   420
               Index           =   0
               Left            =   90
               ScaleHeight     =   360
               ScaleWidth      =   555
               TabIndex        =   33
               Top             =   120
               Width           =   615
            End
         End
         Begin VB.PictureBox LeftScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1485
            Index           =   0
            Left            =   30
            ScaleHeight     =   1425
            ScaleWidth      =   795
            TabIndex        =   30
            Top             =   120
            Width           =   855
            Begin VB.PictureBox ChartLine1 
               AutoRedraw      =   -1  'True
               Height          =   420
               Index           =   0
               Left            =   90
               ScaleHeight     =   360
               ScaleWidth      =   555
               TabIndex        =   31
               Top             =   120
               Width           =   615
            End
         End
      End
   End
   Begin VB.PictureBox VertScroll2 
      Height          =   2175
      Index           =   0
      Left            =   10290
      ScaleHeight     =   2115
      ScaleWidth      =   255
      TabIndex        =   15
      Top             =   2250
      Width           =   315
      Begin VB.VScrollBar VScroll2 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   17
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox TimePanel 
      BackColor       =   &H00E0E0E0&
      Height          =   825
      Index           =   0
      Left            =   1710
      ScaleHeight     =   765
      ScaleWidth      =   8415
      TabIndex        =   14
      Top             =   1320
      Width           =   8475
      Begin VB.PictureBox TimeScale 
         AutoRedraw      =   -1  'True
         Height          =   825
         Index           =   0
         Left            =   240
         ScaleHeight     =   765
         ScaleWidth      =   7755
         TabIndex        =   28
         Top             =   -30
         Width           =   7815
         Begin VB.Timer TimeScaleTimer 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   1950
            Top             =   0
         End
         Begin VB.PictureBox TimeLine1 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            ForeColor       =   &H80000008&
            Height          =   1440
            Index           =   0
            Left            =   6090
            ScaleHeight     =   1410
            ScaleWidth      =   15
            TabIndex        =   37
            Top             =   255
            Width           =   45
         End
         Begin VB.Label lblDate 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4905
            TabIndex        =   91
            Top             =   540
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   1
            Left            =   6180
            Picture         =   "StatusChart.frx":2EE0
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   0
            Left            =   5820
            Picture         =   "StatusChart.frx":302A
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblBgnTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H000080FF&
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   810
            TabIndex        =   80
            Top             =   540
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblMin10 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   0
            Left            =   5370
            TabIndex        =   54
            Top             =   345
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin30 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   5280
            TabIndex        =   53
            Top             =   300
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   60
            Index           =   0
            Left            =   5550
            TabIndex        =   52
            Top             =   450
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin5 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   120
            Index           =   0
            Left            =   5460
            TabIndex        =   51
            Top             =   390
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin60 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   5190
            TabIndex        =   50
            Top             =   240
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblDuration 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "01:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1830
            TabIndex        =   49
            Top             =   525
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.Label lblHour 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "15:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4995
            TabIndex        =   48
            Top             =   45
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.Label lblEndTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "16:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   3390
            TabIndex        =   39
            Top             =   30
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblCurTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "00:00:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   5760
            TabIndex        =   38
            Top             =   30
            Width           =   735
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      Height          =   465
      Left            =   120
      ScaleHeight     =   405
      ScaleWidth      =   10425
      TabIndex        =   13
      Top             =   510
      Visible         =   0   'False
      Width           =   10485
   End
   Begin VB.PictureBox VertScroll1 
      Height          =   2175
      Index           =   0
      Left            =   1230
      ScaleHeight     =   2115
      ScaleWidth      =   255
      TabIndex        =   12
      Top             =   2250
      Visible         =   0   'False
      Width           =   315
      Begin VB.VScrollBar VScroll1 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   16
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox ButtonPanel 
      AutoRedraw      =   -1  'True
      Height          =   375
      Left            =   120
      ScaleHeight     =   315
      ScaleWidth      =   10425
      TabIndex        =   11
      Top             =   120
      Width           =   10485
      Begin VB.CheckBox chkWork 
         Caption         =   "Overlap"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox LeftPanel 
      Height          =   4065
      Index           =   0
      Left            =   120
      ScaleHeight     =   4005
      ScaleWidth      =   915
      TabIndex        =   10
      Top             =   1200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   9090
      Left            =   10740
      ScaleHeight     =   9030
      ScaleWidth      =   1155
      TabIndex        =   0
      Top             =   120
      Width           =   1215
      Begin VB.PictureBox ComPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   112
         Top             =   4950
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdComPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   119
            Top             =   0
            Width           =   255
         End
         Begin VB.Image picComPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":3174
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblComPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   115
            Top             =   270
            Width           =   255
         End
         Begin VB.Label lblComPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   114
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblComPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Cm"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   113
            Top             =   -15
            Width           =   810
         End
         Begin VB.Label lblComPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   116
            Top             =   270
            Width           =   270
         End
      End
      Begin VB.PictureBox DepPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   107
         Top             =   3540
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdDepPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   118
            Top             =   0
            Width           =   255
         End
         Begin VB.Label lblDepPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Dep"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   111
            Top             =   -15
            Width           =   810
         End
         Begin VB.Label lblDepPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   110
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblDepPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   109
            Top             =   270
            Width           =   255
         End
         Begin VB.Image picDepPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":35B6
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblDepPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   108
            Top             =   270
            Width           =   270
         End
      End
      Begin VB.PictureBox ArrPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   102
         Top             =   2850
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdArrPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   117
            Top             =   0
            Width           =   255
         End
         Begin VB.Image picArrPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":39F8
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblArrPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   106
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblArrPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   105
            Top             =   270
            Width           =   255
         End
         Begin VB.Label lblArrPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   104
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblArrPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Arr"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   103
            Top             =   -15
            Width           =   810
         End
      End
      Begin VB.CheckBox chkTitle 
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         Style           =   1  'Graphical
         TabIndex        =   101
         Tag             =   "-1"
         Top             =   1500
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Frame fraYButtonPanel 
         BorderStyle     =   0  'None
         Height          =   990
         Index           =   0
         Left            =   60
         TabIndex        =   3
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   8
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   7
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to UTC Times"
            Top             =   330
            Width           =   495
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "LOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   6
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to Local Times"
            Top             =   330
            Width           =   525
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "RS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   5
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   660
            Width           =   495
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "LS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   4
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   660
            Width           =   525
         End
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTerminate 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.Label lblTxtBackLight 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   15
         TabIndex        =   120
         Top             =   1785
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.Label lblTxtText 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   330
         TabIndex        =   100
         Top             =   1470
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblTxtShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ShadoW"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   480
         TabIndex        =   99
         Top             =   1470
         Visible         =   0   'False
         Width           =   720
      End
      Begin VB.Line linLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   1440
         Y2              =   1440
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   9
      Top             =   9285
      Width           =   15135
      _ExtentX        =   26696
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18309
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1429
            MinWidth        =   1058
            TextSave        =   "12/4/2007"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1270
            MinWidth        =   1058
            TextSave        =   "8:23 AM"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   6
      Left            =   1980
      Picture         =   "StatusChart.frx":3E3A
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   5
      Left            =   1680
      Picture         =   "StatusChart.frx":3F84
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   4
      Left            =   1380
      Picture         =   "StatusChart.frx":430E
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   3
      Left            =   1080
      Picture         =   "StatusChart.frx":4698
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   2
      Left            =   780
      Picture         =   "StatusChart.frx":4A22
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   1
      Left            =   480
      Picture         =   "StatusChart.frx":4B6C
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   0
      Left            =   180
      Picture         =   "StatusChart.frx":4CB6
      Top             =   7560
      Width           =   240
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   4
      Left            =   1350
      Picture         =   "StatusChart.frx":4E00
      Stretch         =   -1  'True
      Top             =   7110
      Width           =   270
   End
   Begin VB.Image picApplIcon 
      Height          =   240
      Left            =   150
      Picture         =   "StatusChart.frx":4F4A
      Top             =   6450
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   3
      Left            =   1050
      Picture         =   "StatusChart.frx":5094
      Stretch         =   -1  'True
      Top             =   7110
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   750
      Picture         =   "StatusChart.frx":561E
      Stretch         =   -1  'True
      Top             =   7110
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   450
      Picture         =   "StatusChart.frx":5A60
      Stretch         =   -1  'True
      Top             =   7110
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   150
      Picture         =   "StatusChart.frx":5EA2
      Stretch         =   -1  'True
      Top             =   7110
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   810
      Picture         =   "StatusChart.frx":622C
      Stretch         =   -1  'True
      Top             =   6780
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   480
      Picture         =   "StatusChart.frx":666E
      Stretch         =   -1  'True
      Top             =   6780
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   150
      Picture         =   "StatusChart.frx":69F8
      Stretch         =   -1  'True
      Top             =   6780
      Width           =   270
   End
End
Attribute VB_Name = "StatusChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ChartBarBackColor As Long
Dim MinimumTop As Long
Dim MinimumLeft As Long
Dim MinuteWidth As Long
Dim MaxChartHeight As Long
Dim MaxChartWidth As Long
Dim MaxBarCnt As Integer
Dim CurBarMax As Integer
Dim CurChartHeight As Long
Dim CurChartWidth As Long
Dim CurScrollHeight As Long
Dim CurScrollWidth As Long
Dim CurBarIdx As Integer
Dim PrvBarIdx As Integer
Dim DataWindowBeginUtc As String
Dim DataWindowBeginLoc As String
Dim ServerUtcTimeStr As String
Dim ServerLocTimeStr As String
Dim ChartName As String
Dim LastLocTimeCheck
Dim LastUtcTimeCheck
Dim ServerUtcTimeVal
Dim TimeScaleUtcBegin
Dim ShowAllIcons As Boolean

Public Sub ScrollSyncExtern(RotLine As Long)
    Dim tmpGidx As String
    Dim tmpBest As String
    Dim tmpBestPos As Long
    Dim NewScroll As Integer
    Dim tmpScrVal As Integer
    Dim PosIsValid As Boolean
    Dim CedaTime
    If RotLine >= 0 Then
        tmpGidx = TabRotFlightsTab.GetFieldValue(RotLine, "GOCX")
        If tmpGidx <> "------" Then
            tmpScrVal = Val(tmpGidx)
            VScroll2(0).Value = tmpScrVal
            tmpBest = TabRotFlightsTab.GetFieldValue(RotLine, "BEST")
            PosIsValid = GetTimeScalePos(tmpBest, CedaTime, tmpBestPos)
            tmpBestPos = tmpBestPos \ MinuteWidth
            If tmpBestPos > 3600 Then tmpBestPos = 3600
            If tmpBestPos < 0 Then tmpBestPos = 0
            tmpBestPos = tmpBestPos - (TimePanel(0).Width \ MinuteWidth \ 2)
            If tmpBestPos < 0 Then tmpBestPos = 0
            NewScroll = CInt(tmpBestPos)
            HScroll2(0).Value = NewScroll
            'tmpScrVal = Val(tmpGidx)
            'VScroll2(0).Value = tmpScrVal
            HighlightCurrentBar tmpScrVal, "EXT"
            If SyncOriginator = "EXT" Then SyncOriginator = ""
        End If
    End If
    
End Sub
Private Sub ArrFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ChartLine1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ChartLine2_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub
Private Sub HighlightCurrentBar(Index As Integer, KeyOrig As String)
    Dim LineNo As Long
    Dim CurGocx As String
    Dim iCol As Integer
    If SyncOriginator = "" Then SyncOriginator = KeyOrig
    CurBarIdx = Index
    If (PrvBarIdx <> CurBarIdx) And (PrvBarIdx >= 0) Then
        ChartLine2(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
        ChartLine2(PrvBarIdx).Refresh
        HighlightStatusPics PrvBarIdx, False
    End If
    iCol = Val(lblBarLineColor(CurBarIdx).Tag)
    DrawBackGround ChartLine2(CurBarIdx), iCol, True, True
    ChartLine2(CurBarIdx).Refresh
    HighlightStatusPics CurBarIdx, True
    If SyncOriginator = ChartName Then
        'This is a QuickHack
        CurGocx = Right("000000" & CStr(Index), 6)
        MarkLookupLines TabRotFlightsTab, "GOCX", CurGocx, True, "DRGN,AFLT,DFLT", True, "'S1'", "DecoMarkerLB"
        LineNo = TabRotFlightsTab.GetCurrentSelected
        MainDialog.RotationData_RowSelectionChanged LineNo, True
    End If
    PrvBarIdx = Index
    If SyncOriginator = KeyOrig Then SyncOriginator = ""
End Sub
Private Sub HighlightStatusPics(Index As Integer, SetFlag As Boolean)
    SetArrPicHighLight picArr1(Index).Tag, SetFlag
    SetArrPicHighLight picArr2(Index).Tag, SetFlag
    SetArrPicHighLight picArr3(Index).Tag, SetFlag
    SetArrPicHighLight picArr4(Index).Tag, SetFlag
    SetDepPicHighLight picDep1(Index).Tag, SetFlag
    SetDepPicHighLight picDep2(Index).Tag, SetFlag
    SetDepPicHighLight picDep3(Index).Tag, SetFlag
    SetDepPicHighLight picDep4(Index).Tag, SetFlag
End Sub
Private Sub SetArrPicHighLight(PicTag As String, SetFlag As Boolean)
    Dim tmpIdx As String
    Dim jIdx As Integer
    tmpIdx = GetItem(PicTag, 1, ",")
    If tmpIdx <> "" Then
        jIdx = Val(tmpIdx)
        If jIdx >= 0 Then
            If SetFlag Then
                lblArrPicText(jIdx).BackColor = vbWhite
                lblArrPicType(jIdx).BackColor = vbWhite
                lblArrPicCount(jIdx).BackColor = vbWhite
            Else
                lblArrPicText(jIdx).BackColor = LightGrey
                lblArrPicType(jIdx).BackColor = LightGrey
                lblArrPicCount(jIdx).BackColor = LightGrey
            End If
            lblArrPicText(jIdx).Refresh
        End If
    End If
End Sub
Private Sub SetDepPicHighLight(PicTag As String, SetFlag As Boolean)
    Dim tmpIdx As String
    Dim jIdx As Integer
    tmpIdx = GetItem(PicTag, 1, ",")
    If tmpIdx <> "" Then
        jIdx = Val(tmpIdx)
        If jIdx >= 0 Then
            If SetFlag Then
                lblDepPicText(jIdx).BackColor = vbWhite
                lblDepPicType(jIdx).BackColor = vbWhite
                lblDepPicCount(jIdx).BackColor = vbWhite
            Else
                lblDepPicText(jIdx).BackColor = LightGrey
                lblDepPicType(jIdx).BackColor = LightGrey
                lblDepPicCount(jIdx).BackColor = LightGrey
            End If
            lblDepPicText(jIdx).Refresh
        End If
    End If
End Sub

Private Sub ChartLine3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                Me.Hide
                MainDialog.PushWorkButton "GOCC_CHART", False
                chkAppl(Index).Value = 0
            Case Else
        End Select
    Else
        chkAppl(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkScroll1_Click(Index As Integer)
    If chkScroll1(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll1(0).Visible = True Then
                    VertScroll1(0).Visible = False
                    TimeScroll1(0).Visible = False
                Else
                    VertScroll1(0).Visible = True
                    TimeScroll1(0).Visible = True
                End If
                chkScroll1(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll1(Index).Value = 0
        End Select
    Else
    End If
End Sub
Private Sub chkScroll2_Click(Index As Integer)
    If chkScroll2(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll2(0).Visible = True Then
                    VertScroll2(0).Visible = False
                    TimeScroll2(0).Visible = False
                Else
                    VertScroll2(0).Visible = True
                    TimeScroll2(0).Visible = True
                End If
                chkScroll2(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll2(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub chkSelDeco_Click(Index As Integer)
    If chkSelDeco(Index).Value = 1 Then
        chkSelDeco(Index).BackColor = LightGreen
        If Index = 0 Then
            ToggleLeftScaleBars True
        Else
            ToggleRightScaleBars True
        End If
    Else
        If Index = 0 Then
            ToggleLeftScaleBars False
        Else
            ToggleRightScaleBars False
        End If
        chkSelDeco(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ToggleLeftScaleBars(ShowLeftScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrFlight.UBound
        ArrFlight(i).Visible = False
        If ShowLeftScale = True Then
            Set ArrFlight(i).Container = ChartLine1(i)
            ArrFlight(i).Left = 15
            ArrFlight(i).Visible = lblArrBar(i).Visible
        Else
            Set ArrFlight(i).Container = ChartLine2(i)
            NewLeft = lblArrBar(i).Left - ArrFlight(i).Width - 30
            ArrFlight(i).Left = NewLeft
            ArrFlight(i).Visible = lblArrBar(i).Visible
        End If
    Next
    Screen.MousePointer = 0
    LeftScale(0).Visible = ShowLeftScale
    Form_Resize
End Sub
Private Sub ToggleRightScaleBars(ShowRightScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To DepFlight.UBound
        DepFlight(i).Visible = False
        If ShowRightScale = True Then
            Set DepFlight(i).Container = ChartLine3(i)
            DepFlight(i).Left = 15
            DepFlight(i).Visible = lblDepBar(i).Visible
        Else
            Set DepFlight(i).Container = ChartLine2(i)
            NewLeft = lblDepBar(i).Left + lblDepBar(i).Width + 30
            DepFlight(i).Left = NewLeft
            DepFlight(i).Visible = lblDepBar(i).Visible
        End If
    Next
    Screen.MousePointer = 0
    RightScale(0).Visible = ShowRightScale
    Form_Resize
End Sub

Private Sub chkTime1_Click(Index As Integer)
    If chkTime1(Index).Value = 1 Then
        chkTime1(Index).BackColor = LightGreen
        chkTime1(0).Value = 1
        chkTime1(1).Value = 1
    Else
        chkTime1(0).Value = 0
        chkTime1(1).Value = 0
        chkTime1(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTitle_Click(Index As Integer)
    Dim tmpTag As String
    tmpTag = chkTitle(Index).Tag
    If chkTitle(Index).Value = 1 Then
        Select Case tmpTag
            Case "TASKS"
                ToggleTaskBars True, "AD"
            Case "AFSA"
                ToggleTaskBars True, "A"
                chkTitle(Index).BackColor = LightYellow
            Case "AFSD"
                ToggleTaskBars True, "D"
                chkTitle(Index).BackColor = LightGreen
            Case Else
                chkTitle(Index).Value = 0
        End Select
    Else
        Select Case tmpTag
            Case "TASKS"
                ToggleTaskBars False, "AD"
            Case "AFSA"
                ToggleTaskBars False, "A"
            Case "AFSD"
                ToggleTaskBars False, "D"
            Case Else
        End Select
        chkTitle(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ToggleTaskBars(SetFlag As Boolean, ForWhat As String)
    Dim iBar As Integer
    For iBar = 0 To ChartLine2.UBound
        If (ForWhat = "A") Or (ForWhat = "AD") Then
            If lblArrBar(iBar).Visible = True Then
                lblArrJob1(iBar).Visible = SetFlag
                lblArrJob2(iBar).Visible = SetFlag
            End If
        End If
        If (ForWhat = "D") Or (ForWhat = "AD") Then
            If lblDepBar(iBar).Visible = True Then
                lblDepJob1(iBar).Visible = SetFlag
                lblDepJob2(iBar).Visible = SetFlag
            End If
        End If
    Next
End Sub
Private Sub chkUtc_Click(Index As Integer)
    If chkUtc(Index).Value = 1 Then
        chkUtc(Index).BackColor = LightGreen
        If Index = 0 Then chkUtc(1).Value = 0 Else chkUtc(0).Value = 0
        CreateTimeScaleArea TimeScale(0)
        TimeScaleTimer_Timer
        If Index = 1 Then
            ToggleFlightBarTimes UtcTimeDiff
        Else
            ToggleFlightBarTimes 0
        End If
    Else
        If Index = 0 Then chkUtc(1).Value = 1 Else chkUtc(0).Value = 1
        chkUtc(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub CreateTimeScaleArea(TimeScale As PictureBox)
    Dim iMin As Integer
    Dim iMin5 As Integer
    Dim iMin10 As Integer
    Dim iMin30 As Integer
    Dim iMin60 As Integer
    Dim iHour As Integer
    Dim iBar As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff1 As Long
    Dim LeftOff2 As Long
    Dim TopPos As Long
    Dim DisplayDateTime As String
    Dim CurDate As String
    Dim CurSsimDate As String
    Dim CurUtcNowTime
    Dim CurLocNowTime
    
    If DataWindowBeginUtc = "" Then
        LastLocTimeCheck = Now
        'Create a Default UTC Time
        CurUtcNowTime = DateAdd("n", -UtcTimeDiff, LastLocTimeCheck)
        LastUtcTimeCheck = CurUtcNowTime
        'Create a Default Data Window Begin
        CurUtcNowTime = DateAdd("h", -2, CurUtcNowTime)
        DataWindowBeginUtc = Format(CurUtcNowTime, "YYYYMMDDhhmmss")
    End If
    
    If ServerUtcTimeStr = "" Then
        'Create a Default Server Reference Time
        ServerUtcTimeVal = LastUtcTimeCheck
        ServerUtcTimeStr = Format(ServerUtcTimeVal, "YYYYMMDDhhmmss")
    End If
    
    If DataFilterVpfr <> "" Then DataWindowBeginUtc = DataFilterVpfr
    'We always start at full hours in Utc
    Mid(DataWindowBeginUtc, 11) = "0000"
    lblBgnTime(0).Tag = DataWindowBeginUtc
    
    CurUtcNowTime = CedaFullDateToVb(DataWindowBeginUtc)
    TimeScaleUtcBegin = CurUtcNowTime
    CurLocNowTime = DateAdd("n", UtcTimeDiff, CurUtcNowTime)
    DataWindowBeginLoc = Format(CurLocNowTime, "YYYYMMDDhhmmss")
    
    If chkUtc(0).Value = 1 Then
        DisplayDateTime = DataWindowBeginUtc
    Else
        DisplayDateTime = DataWindowBeginLoc
    End If
    
    lblBgnTime(0).Caption = DisplayDateTime
    
    iHour = Val(Mid(DisplayDateTime, 9, 2)) - 1
    CurDate = Left(DisplayDateTime, 8)
    CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
    lblBgnTime(0).Caption = CurSsimDate
    
    lblMin1(0).Width = 15
    lWidth = 60
    MinuteWidth = lWidth
    LeftOff1 = (lblHour(0).Width / 2) - 15
    LeftOff2 = (lblDate(0).Width / 2) - 15
    iMin5 = -1
    iMin10 = -1
    iMin30 = -1
    iMin60 = -1
    LeftPos = 1200
    For iMin = 0 To 3600
        If iMin > lblMin1.UBound Then
            Load lblMin1(iMin)
            Set lblMin1(iMin).Container = TimeScale
        End If
        lblMin1(iMin).Left = LeftPos + 15
        lblMin1(iMin).Visible = True
        If iMin Mod 5 = 0 Then
            iMin5 = iMin5 + 1
            If iMin5 > lblMin5.UBound Then
                Load lblMin5(iMin5)
                Set lblMin5(iMin5).Container = TimeScale
            End If
            lblMin5(iMin5).Left = LeftPos
            lblMin5(iMin5).Visible = True
        End If
        If iMin Mod 10 = 0 Then
            iMin10 = iMin10 + 1
            If iMin10 > lblMin10.UBound Then
                Load lblMin10(iMin10)
                Set lblMin10(iMin10).Container = TimeScale
            End If
            lblMin10(iMin10).Left = LeftPos
            lblMin10(iMin10).Visible = True
        End If
        If iMin Mod 30 = 0 Then
            iMin30 = iMin30 + 1
            If iMin30 > lblMin30.UBound Then
                Load lblMin30(iMin30)
                Set lblMin30(iMin30).Container = TimeScale
            End If
            lblMin30(iMin30).Left = LeftPos
            lblMin30(iMin30).Visible = True
        End If
        If iMin Mod 60 = 0 Then
            iMin60 = iMin60 + 1
            If iMin60 > lblMin60.UBound Then
                Load lblMin60(iMin60)
                Set lblMin60(iMin60).Container = TimeScale
                Load lblHour(iMin60)
                Set lblHour(iMin60).Container = TimeScale
                Load lblDate(iMin60)
                Set lblDate(iMin60).Container = TimeScale
            End If
            lblMin60(iMin60).Left = LeftPos
            lblMin60(iMin60).Visible = True
            lblHour(iMin60).Left = LeftPos - LeftOff1
            iHour = iHour + 1
            If iHour > 23 Then
                iHour = 0
                CurDate = CedaDateAdd(CurDate, 1)
                CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
            End If
            lblHour(iMin60).Caption = Right("00" & CStr(iHour), 2) & ":00"
            lblHour(iMin60).ToolTipText = CurSsimDate
            lblHour(iMin60).Tag = CurDate
            lblHour(iMin60).Visible = True
            lblDate(iMin60).Caption = CurSsimDate
            lblDate(iMin60).Tag = CurDate
            lblDate(iMin60).Left = LeftPos - LeftOff2
            lblDate(iMin60).Visible = True
        End If
        LeftPos = LeftPos + lWidth
    Next
    CurChartWidth = LeftPos + 1200
    TimeScaleTimer.Enabled = True
End Sub

Public Sub CreateChartBarLayout(NeededBars As Integer)
    Dim iBar As Integer
    Dim iMin As Integer
    Dim iMax As Integer
    Dim iUse As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff As Long
    Dim TopPos As Long
    Me.MousePointer = 11
    WorkArea(0).Visible = False
    iMin = ChartLine2.UBound
    If iMin > 0 Then
        TopPos = ChartLine2(iMin).Top + ChartLine2(0).Height
        iMin = iMin + 1
    Else
        TopPos = ChartLine2(iMin).Top
    End If
    iMax = NeededBars - 1
    If iMax > MaxBarCnt Then iMax = MaxBarCnt
    If (iMax >= CurBarMax) Then
        For iBar = iMin To iMax
            If iBar > ChartLine2.UBound Then
                Load ChartLine1(iBar)
                Load ChartLine2(iBar)
                Load ChartLine3(iBar)
                Load lblArrBar(iBar)
                Load lblArrJob1(iBar)
                Load lblArrJob2(iBar)
                Load picArr1(iBar)
                Load picArr2(iBar)
                Load picArr3(iBar)
                Load picArr4(iBar)
                Load lblDepBar(iBar)
                Load lblDepJob1(iBar)
                Load lblDepJob2(iBar)
                Load picDep1(iBar)
                Load picDep2(iBar)
                Load picDep3(iBar)
                Load picDep4(iBar)
                Load lblTowBar(iBar)
                Load lblBarLineColor(iBar)
                Load picSortTime(iBar)
                
                Set ChartLine1(iBar).Container = LeftScale(0)
                Set ChartLine2(iBar).Container = HScrollArea(0)
                Set ChartLine3(iBar).Container = RightScale(0)
                
                Set lblArrBar(iBar).Container = ChartLine2(iBar)
                Set lblArrJob1(iBar).Container = ChartLine2(iBar)
                Set lblArrJob2(iBar).Container = ChartLine2(iBar)
                Set picArr1(iBar).Container = ChartLine2(iBar)
                Set picArr2(iBar).Container = ChartLine2(iBar)
                Set picArr3(iBar).Container = ChartLine2(iBar)
                Set picArr4(iBar).Container = ChartLine2(iBar)
                Set lblDepBar(iBar).Container = ChartLine2(iBar)
                Set lblDepJob1(iBar).Container = ChartLine2(iBar)
                Set lblDepJob2(iBar).Container = ChartLine2(iBar)
                Set picDep1(iBar).Container = ChartLine2(iBar)
                Set picDep2(iBar).Container = ChartLine2(iBar)
                Set picDep3(iBar).Container = ChartLine2(iBar)
                Set picDep4(iBar).Container = ChartLine2(iBar)
                Set lblTowBar(iBar).Container = ChartLine2(iBar)
                Set lblBarLineColor(iBar).Container = ChartLine2(iBar)
                Set picSortTime(iBar).Container = ChartLine2(iBar)
                'Set picSortTime(iBar).Container = HScrollArea(0)
            End If
            ChartLine1(iBar).Top = TopPos
            ChartLine2(iBar).Top = TopPos
            ChartLine3(iBar).Top = TopPos
            'picSortTime(iBar).Top = TopPos
            
            lblDepBar(iBar).ZOrder
                    
            CreateArrFlightPanel iBar
            Set ArrFlight(iBar).Container = ChartLine1(iBar)
            ArrFlight(iBar).Visible = True
            
            CreateDepFlightPanel iBar
            Set DepFlight(iBar).Container = ChartLine3(iBar)
            DepFlight(iBar).Visible = True
            
            ChartLine1(iBar).Visible = True
            ChartLine2(iBar).Visible = True
            ChartLine3(iBar).Visible = True
            
            'lblArrFlno(iBar).Caption = CStr(iBar)
            
            TopPos = TopPos + ChartLine2(0).Height
        Next
        CurBarMax = iMax
    Else
        iMin = NeededBars
        iMax = CurBarMax
        CurBarMax = iMin - 1
    End If
    TopPos = ChartLine2(CurBarMax).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 30
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    TimeLine2(0).ZOrder
    RangeLine(0).Height = CurChartHeight
    RangeLine(0).ZOrder
    RangeLine(1).Height = CurChartHeight
    RangeLine(1).ZOrder
    TopPos = VScrollArea(0).Height
    VScroll1(0).Max = CurBarMax
    VScroll2(0).Max = CurBarMax
    WorkArea(0).Visible = True
    Me.MousePointer = 0
End Sub
Private Sub CreateArrFlightPanel(iBar As Integer)
    If iBar > ArrFlight.UBound Then
        Load ArrFlight(iBar)
        Load picArrStat(iBar)
        Load lblArrFtyp(iBar)
        Load lblArrFlno(iBar)
        Load lblArrFlti(iBar)
        Load lblArrOrg3(iBar)
        Load lblArrVia3(iBar)
        Load lblArrStoa(iBar)
        Load lblArrEtai(iBar)
        Load lblArrOnbl(iBar)
        Load lblArrPsta(iBar)
        Load lblArrGta1(iBar)
        Load lblArrRegn(iBar)
        Load lblArrAct3(iBar)
        Set picArrStat(iBar).Container = ArrFlight(iBar)
        Set lblArrFtyp(iBar).Container = ArrFlight(iBar)
        Set lblArrFlno(iBar).Container = ArrFlight(iBar)
        Set lblArrFlti(iBar).Container = ArrFlight(iBar)
        Set lblArrOrg3(iBar).Container = ArrFlight(iBar)
        Set lblArrVia3(iBar).Container = ArrFlight(iBar)
        Set lblArrStoa(iBar).Container = ArrFlight(iBar)
        Set lblArrEtai(iBar).Container = ArrFlight(iBar)
        Set lblArrOnbl(iBar).Container = ArrFlight(iBar)
        Set lblArrPsta(iBar).Container = ArrFlight(iBar)
        Set lblArrGta1(iBar).Container = ArrFlight(iBar)
        Set lblArrRegn(iBar).Container = ArrFlight(iBar)
        Set lblArrAct3(iBar).Container = ArrFlight(iBar)
    End If
    ArrFlight(iBar).Top = 15
    'ArrFlight(iBar).Height = 330
    picArrStat(iBar).Visible = True
    lblArrFtyp(iBar).Visible = True
    lblArrFlno(iBar).Visible = True
    lblArrFlti(iBar).Visible = True
    lblArrOrg3(iBar).Visible = True
    lblArrVia3(iBar).Visible = True
    lblArrStoa(iBar).Visible = True
    lblArrEtai(iBar).Visible = True
    lblArrOnbl(iBar).Visible = True
    lblArrPsta(iBar).Visible = True
    lblArrGta1(iBar).Visible = True
    lblArrRegn(iBar).Visible = True
    lblArrAct3(iBar).Visible = True
End Sub

Private Sub CreateDepFlightPanel(iBar As Integer)
    If iBar > DepFlight.UBound Then
        Load DepFlight(iBar)
        Load picDepStat(iBar)
        Load lblDepFtyp(iBar)
        Load lblDepFlno(iBar)
        Load lblDepFlti(iBar)
        Load lblDepDes3(iBar)
        Load lblDepVia3(iBar)
        Load lblDepStod(iBar)
        Load lblDepEtdi(iBar)
        Load lblDepOfbl(iBar)
        Load lblDepPstd(iBar)
        Load lblDepGtd1(iBar)
        Load lblDepRegn(iBar)
        Load lblDepAct3(iBar)
        Set picDepStat(iBar).Container = DepFlight(iBar)
        Set lblDepFtyp(iBar).Container = DepFlight(iBar)
        Set lblDepFlno(iBar).Container = DepFlight(iBar)
        Set lblDepFlti(iBar).Container = DepFlight(iBar)
        Set lblDepDes3(iBar).Container = DepFlight(iBar)
        Set lblDepVia3(iBar).Container = DepFlight(iBar)
        Set lblDepStod(iBar).Container = DepFlight(iBar)
        Set lblDepEtdi(iBar).Container = DepFlight(iBar)
        Set lblDepOfbl(iBar).Container = DepFlight(iBar)
        Set lblDepPstd(iBar).Container = DepFlight(iBar)
        Set lblDepGtd1(iBar).Container = DepFlight(iBar)
        Set lblDepRegn(iBar).Container = DepFlight(iBar)
        Set lblDepAct3(iBar).Container = DepFlight(iBar)
    End If
    DepFlight(iBar).Top = 15
    'DepFlight(iBar).Height = 330
    picDepStat(iBar).Visible = True
    lblDepFtyp(iBar).Visible = True
    lblDepFlno(iBar).Visible = True
    lblDepFlti(iBar).Visible = True
    lblDepDes3(iBar).Visible = True
    lblDepVia3(iBar).Visible = True
    lblDepStod(iBar).Visible = True
    lblDepEtdi(iBar).Visible = True
    lblDepOfbl(iBar).Visible = True
    lblDepPstd(iBar).Visible = True
    lblDepGtd1(iBar).Visible = True
    lblDepRegn(iBar).Visible = True
    lblDepAct3(iBar).Visible = True
End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0
                ShowBarsOverlapped True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                ShowBarsOverlapped False
            Case Else
        End Select
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ShowBarsOverlapped(ShowOverlap As Boolean)
    Dim ArrLeft As Long
    Dim DepLeft As Long
    Dim iBar As Integer
    Screen.MousePointer = 11
    For iBar = 0 To ChartLine2.UBound
        If (lblArrFtyp(iBar).Tag <> "") And (lblDepFtyp(iBar).Tag <> "") Then
            If ShowOverlap Then
                ArrFlight(iBar).Top = -30
                lblArrBar(iBar).Top = 0
                DepFlight(iBar).Top = 60
                lblDepBar(iBar).Top = 90
            Else
                ArrFlight(iBar).Top = 15
                lblArrBar(iBar).Top = 45
                DepFlight(iBar).Top = 15
                lblDepBar(iBar).Top = 45
            End If
            'End If
        End If
    Next
    Screen.MousePointer = 0
End Sub

Private Sub cmdArrPicMode_Click(Index As Integer)
    Dim NewSize As Long
    If cmdArrPicMode(Index).Tag = "360" Then
        cmdArrPicMode(Index).Tag = "240"
    Else
        cmdArrPicMode(Index).Tag = "360"
    End If
    NewSize = Val(cmdArrPicMode(Index).Tag)
    AdjustArrIconSize Index, NewSize
End Sub

Private Sub cmdDepPicMode_Click(Index As Integer)
    Dim NewSize As Long
    If cmdDepPicMode(Index).Tag = "360" Then
        cmdDepPicMode(Index).Tag = "240"
    Else
        cmdDepPicMode(Index).Tag = "360"
    End If
    NewSize = Val(cmdDepPicMode(Index).Tag)
    AdjustDepIconSize Index, NewSize
End Sub

Private Sub DepFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Public Sub Form_Activate()
    Static IsActivated As Boolean
    Dim tmpWidth As Long
    Dim tmpHeight As Long
    Dim tmpSize As Long
    If IsActivated = False Then
        If MainLifeStyle Then
            tmpWidth = Screen.Width + 2100
            tmpHeight = Screen.Height + 2100
            ButtonPanel.Width = tmpWidth
            tmpSize = ButtonPanel.Height
            ButtonPanel.Height = tmpSize * 2
            DrawBackGround ButtonPanel, WorkAreaColor, True, True
            ButtonPanel.Height = tmpSize
            
            RightPanel.Height = tmpHeight
            DrawBackGround RightPanel, WorkAreaColor, True, True
            
            tmpSize = TimeScale(0).Height
            TimeScale(0).Height = tmpSize * 6
            DrawBackGround TimeScale(0), MainAreaColor, True, True
            TimeScale(0).Height = tmpSize
            
            TopRemark(0).Width = tmpWidth
            tmpSize = TopRemark(0).Height
            TopRemark(0).Height = tmpSize * 2
            DrawBackGround TopRemark(0), MainAreaColor, True, True
            TopRemark(0).Height = tmpSize
            
            BottomRemark(0).Width = tmpWidth
            tmpSize = BottomRemark(0).Height
            BottomRemark(0).Height = tmpSize * 2
            DrawBackGround BottomRemark(0), MainAreaColor, True, True
            BottomRemark(0).Height = tmpSize
            
            WorkArea(0).Height = tmpHeight
            WorkArea(0).Width = tmpWidth
            DrawBackGround WorkArea(0), MainAreaColor, True, True
            'DrawBackGround TopPanel, WorkAreaColor, True, True
        Else
            'lblArrRow.ForeColor = vbButtonText
            'lblDepRow.ForeColor = vbButtonText
        End If
        Me.Refresh
        Me.Top = 0
        Me.Height = Screen.Height - 300
        Me.Refresh
        IsActivated = True
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ScrollIt As Boolean
    Dim NxtIdx As Integer
    Dim TopIdx As Integer
    Dim BotIdx As Integer
    Dim LinCnt As Integer
    Dim NewVal As Integer
    If Shift = 1 Then ScrollIt = True Else ScrollIt = False
    TopIdx = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height / ChartLine2(0).Height)
    BotIdx = TopIdx + LinCnt - 1
    Select Case KeyCode
        Case vbKeyDown
            NxtIdx = CurBarIdx + 1
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                VScroll2(0).Value = VScroll2(0).Value + 1
            End If
            KeyCode = 0
        Case vbKeyUp
            NxtIdx = CurBarIdx - 1
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                VScroll2(0).Value = VScroll2(0).Value - 1
            End If
            KeyCode = 0
        Case vbKeyPageDown
            NxtIdx = CurBarIdx + LinCnt
            If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                NxtIdx = VScroll2(0).Value + LinCnt
                If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyPageUp
            NxtIdx = CurBarIdx - LinCnt
            If NxtIdx < 0 Then NxtIdx = 0
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                NxtIdx = VScroll2(0).Value - LinCnt
                If NxtIdx < 0 Then NxtIdx = 0
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyLeft
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value - 5
                Case 1
                    NewVal = HScroll2(0).Value - 1
                Case 2
                    NewVal = HScroll2(0).Value - 20
                Case Else
                    NewVal = HScroll2(0).Value - 5
            End Select
            If NewVal < 0 Then NewVal = 0
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case vbKeyRight
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value + 5
                Case 1
                    NewVal = HScroll2(0).Value + 1
                Case 2
                    NewVal = HScroll2(0).Value + 20
                Case Else
                    NewVal = HScroll2(0).Value + 5
            End Select
            If NewVal > HScroll2(0).Max Then NewVal = HScroll2(0).Max
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    ChartName = "GOC"
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ChartBarBackColor = LightGray
    lblBarLineColor(0).BackColor = ChartBarBackColor
    lblBarLineColor(0).Tag = "7"
    MinimumTop = 30
    MinimumLeft = 30
    CurChartWidth = 0
    CurChartHeight = 0
    Me.Top = Screen.Height + 1000
    Me.Left = 0
    Me.Width = Screen.Width
    Me.Height = Screen.Height
    'UtcTimeDiff = 420
    chkUtc(1).Value = 1
    chkSelDeco(0).Value = 1
    chkSelDeco(1).Value = 1
    'We have a restriction of a maximum height of a picture box (Panel)
    'So this is the maximum number of bars on the chart:
    MaxBarCnt = CInt(245745 \ ChartLine2(0).Height) - 1
    'Maximum is 584, but together with the ConnexChart
    'we run out of memory so we set it to 350
    MaxBarCnt = 350
    CurBarMax = 0
    GetChartConfig
    InitChartArea
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    chkAppl(0).Value = 1
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    
    VScroll1(0).Visible = False
    VScroll2(0).Visible = False
    HScroll2(0).Visible = False
    
    NewTop = MinimumTop
    ButtonPanel.Top = NewTop
    RightPanel.Top = NewTop
    NewLeft = MinimumLeft
    ButtonPanel.Left = NewLeft
    TopPanel.Left = NewLeft
    LeftPanel(0).Left = NewLeft
    
    NewWidth = Me.ScaleWidth
    NewLeft = NewWidth - RightPanel.Width - 15
    RightPanel.Left = NewLeft
    NewWidth = NewLeft - MinimumLeft - 15
    If NewWidth > 300 Then
        ButtonPanel.Width = NewWidth
        TopPanel.Width = NewWidth
    End If
    NewLeft = RightPanel.Left - VertScroll2(0).Width - 15
    VertScroll2(0).Left = NewLeft
    HorizScroll3(0).Left = NewLeft
    TimeScroll2(0).Left = NewLeft
    
    NewHeight = Me.ScaleHeight - StatusBar.Height - MinimumTop
    If NewHeight > 300 Then
        RightPanel.Height = NewHeight
    End If
    
    NewTop = MinimumTop
    NewTop = NewTop + ButtonPanel.Height + 15
    If TopPanel.Visible Then
        TopPanel.Top = NewTop
        NewTop = NewTop + TopPanel.Height + 15
    End If
    LeftPanel(0).Top = NewTop
    TopRemark(0).Top = NewTop
    TimeScroll1(0).Top = NewTop
    TimeScroll2(0).Top = NewTop
    
    NewHeight = NewHeight - NewTop + MinimumTop
    If NewHeight > 300 Then
        LeftPanel(0).Height = NewHeight
    End If
    
    NewTop = NewTop + TopRemark(0).Height '+ 15
    
    'TimeScroll1(0).Top = NewTop
    'TimeScroll2(0).Top = NewTop
    TimePanel(0).Top = NewTop
    
    NewTop = NewTop + TimePanel(0).Height + 15
    VertScroll1(0).Top = NewTop
    WorkArea(0).Top = NewTop
    VertScroll2(0).Top = NewTop
    
    NewLeft = MinimumLeft
    If LeftPanel(0).Visible Then
        NewLeft = NewLeft + LeftPanel(0).Width + 15
    End If
    TimeScroll1(0).Left = NewLeft
    TopRemark(0).Left = NewLeft
    
    NewWidth = RightPanel.Left - NewLeft - 15
    If NewWidth > 300 Then
        'TopRemark(0).Width = NewWidth
        'BottomRemark(0).Width = NewWidth
    End If
    
    If VertScroll1(0).Visible Then
        VertScroll1(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewLeft = NewLeft + VertScroll1(0).Width + 15
        WorkArea(0).Left = NewLeft
        HorizScroll2(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    Else
        WorkArea(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            NewLeft = NewLeft + HorizScroll1(0).Width + 15
            HorizScroll2(0).Left = NewLeft
            NewWidth = NewWidth - HorizScroll1(0).Width - 15
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    End If
    
    TimePanel(0).Left = WorkArea(0).Left
    TimePanel(0).Width = WorkArea(0).Width
    TopRemark(0).Left = WorkArea(0).Left
    TopRemark(0).Width = WorkArea(0).Width
    
    BottomRemark(0).Left = HorizScroll2(0).Left
    BottomRemark(0).Width = HorizScroll2(0).Width
    
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomRemark(0).Height
    BottomRemark(0).Top = NewTop - 30
    NewTop = NewTop - BottomRemark(0).Height - 15
    HorizScroll1(0).Top = NewTop
    HorizScroll2(0).Top = NewTop
    HorizScroll3(0).Top = NewTop
    NewHeight = NewTop - WorkArea(0).Top - 15
    If NewHeight > 300 Then
        VertScroll1(0).Height = NewHeight
        VScroll1(0).Height = NewHeight - 60
        WorkArea(0).Height = NewHeight
        VertScroll2(0).Height = NewHeight
        VScroll2(0).Height = NewHeight - 60
    End If
    
    ArrangeChartArea
    
    VScroll1(0).Visible = True
    VScroll1(0).Refresh
    VScroll2(0).Visible = True
    VScroll2(0).Refresh
    HScroll2(0).Visible = True
    HScroll2(0).Refresh
  
    WorkArea(0).Refresh
    
    Me.Refresh
End Sub

Private Sub InitChartArea()
    Dim NewColor As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Screen.MousePointer = 11
    
    InitRightPanel
    
    NewColor = ChartBarBackColor
    ChartLine1(0).BackColor = NewColor
    ChartLine2(0).BackColor = NewColor
    ChartLine3(0).BackColor = NewColor
    ArrFlight(0).BackColor = NewColor
    DepFlight(0).BackColor = NewColor
    LeftScale(0).BackColor = NewColor
    RightScale(0).BackColor = NewColor
    
    lblBgnTime(0).BackColor = LightGray
    
    NewLeft = -30
    VScrollArea(0).Left = NewLeft
    HScrollArea(0).Left = NewLeft
    LeftScale(0).Left = NewLeft
    
    NewLeft = 30
    ChartLine1(0).Left = NewLeft
    ChartLine3(0).Left = NewLeft
    
    NewLeft = -30
    ChartLine2(0).Left = NewLeft
    
    NewTop = -30
    VScrollArea(0).Top = NewTop
    HScrollArea(0).Top = NewTop
    LeftScale(0).Top = NewTop
    RightScale(0).Top = NewTop
    
    NewTop = 30
    ChartLine1(0).Top = NewTop
    ChartLine2(0).Top = NewTop
    ChartLine3(0).Top = NewTop
    
    NewTop = -30
    TimePanel(0).Height = TimeScroll1(0).Height - 300
    TimeScale(0).Top = NewTop
    TimeScale(0).Height = TimePanel(0).Height + 60
    
    CreateTimeScaleArea TimeScale(0)
    HScrollArea(0).Width = CurChartWidth
    VScrollArea(0).Width = CurChartWidth
    ChartLine1(0).Width = ArrFlight(0).Width + 90
    LeftScale(0).Width = ChartLine1(0).Width + 120
    ChartLine2(0).Width = CurChartWidth
    ChartLine3(0).Width = DepFlight(0).Width + 90
    RightScale(0).Width = ChartLine3(0).Width + 120
    
    Screen.MousePointer = 11
    CreateChartBarLayout 680
    Screen.MousePointer = 11
    
    TimeLine2(0).ZOrder
    lblCurTime(0).ZOrder
    LeftScale(0).ZOrder
    RightScale(0).ZOrder
    LeftPanel(0).ZOrder
    ButtonPanel.ZOrder
    TopPanel.ZOrder
    TimePanel(0).ZOrder
    TopRemark(0).ZOrder
    WorkArea(0).ZOrder
    BottomRemark(0).ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    Screen.MousePointer = 0
    
End Sub

Private Sub ArrangeChartArea()
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewValue As Long
    
    NewWidth = WorkArea(0).Width + 60
    VScrollArea(0).Width = NewWidth
    NewLeft = VScrollArea(0).Width - RightScale(0).Width - 90
    RightScale(0).Left = NewLeft
    RightScale(0).Refresh
    
    TimeScale(0).Left = HScrollArea(0).Left
    TimeScale(0).Width = HScrollArea(0).Width
    TimeScale(0).Refresh
    
    CurScrollWidth = CurChartWidth - WorkArea(0).Width
    NewValue = CLng(HScroll2(0).Value)
    AdjustHorizScroll NewValue
    
End Sub

Private Sub HScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub HScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub AdjustHorizScroll(ScrollValue As Long)
    Dim NewLeft As Long
    NewLeft = ScrollValue * MinuteWidth
    HScrollArea(0).Left = -NewLeft
    WorkArea(0).Refresh
    TimeScale(0).Left = -NewLeft
    TimeScale(0).Refresh
End Sub

Private Sub icoCflRange_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = CStr(X)
    RangeLine(Index).Left = icoCflRange(Index).Left + 105
    RangeLine(Index).Visible = True
    If Index = 0 Then
        ShowTimeRangeDuration True, RangeLine(Index).Left, TimeLine1(0).Left
    Else
        ShowTimeRangeDuration True, TimeLine1(0).Left, RangeLine(Index).Left
    End If
End Sub

Private Sub icoCflRange_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oldX As Long
    Dim newX As Long
    Dim diffX As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MinLeft As Long
    Dim DurLeft As Long
    Dim DurRight As Long
    oldX = CLng(Val(icoCflRange(Index).Tag))
    newX = CLng(X)
    If oldX > 0 Then
        If oldX <> newX Then
            diffX = newX - oldX
            NewLeft = icoCflRange(Index).Left + diffX
            NewLeft = (NewLeft \ MinuteWidth) * MinuteWidth
            If Index = 0 Then
                MinLeft = lblMin1(0).Left
                MaxLeft = TimeLine1(0).Left - (MinuteWidth * 10)
                DurLeft = NewLeft + 105
                DurRight = TimeLine1(0).Left
            Else
                MinLeft = TimeLine1(0).Left + (MinuteWidth * 10)
                MaxLeft = CurChartWidth - 1200
                DurRight = NewLeft + 105
                DurLeft = TimeLine1(0).Left
            End If
            If (NewLeft >= MinLeft) And (NewLeft <= MaxLeft) Then
                RangeLine(Index).Visible = False
                HScrollArea(0).Refresh
                icoCflRange(Index).Left = NewLeft
                NewLeft = NewLeft + 105
                RangeLine(Index).Left = NewLeft
                RangeLine(Index).Visible = True
                RangeLine(Index).Refresh
                ShowTimeRangeDuration True, DurLeft, DurRight
            End If
        End If
    End If
End Sub
Private Function ShowTimeRangeDuration(ShowLabel As Boolean, CurLeft As Long, CurRight As Long) As Long
    Dim tmpLeft As Long
    Dim tmpRight As Long
    Dim tmpWidth As Long
    Dim tmpDuration As Long
    Dim tmpVal As Long
    Dim tmpText As String
    lblDuration.Visible = False
    If CurRight > CurLeft Then
        tmpLeft = CurLeft
        tmpRight = CurRight
    Else
        tmpLeft = CurRight
        tmpRight = CurLeft
    End If
    tmpWidth = tmpRight - tmpLeft + 15
    lblDuration.Left = tmpLeft
    lblDuration.Width = tmpWidth
    tmpDuration = tmpWidth \ MinuteWidth
    tmpVal = tmpDuration Mod 60
    tmpText = Right("00" & CStr(tmpVal), 2)
    tmpVal = (tmpDuration - tmpVal) \ 60
    tmpText = Right("00" & CStr(tmpVal), 2) & ":" & tmpText
    lblDuration.Caption = tmpText
    lblDuration.Visible = ShowLabel
    lblDuration.Refresh
    ShowTimeRangeDuration = tmpDuration
End Function

Private Sub icoCflRange_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = "-1"
    RangeLine(Index).Visible = False
    lblDuration.Visible = False
End Sub

Private Sub lblArrAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrBar(Index), True
End Sub
Private Sub lblArrBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblArrBar(Index), False
End Sub

Private Sub lblArrJob1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrJob1(Index), True
End Sub
Private Sub lblArrJob1_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblArrJob1(Index), False
End Sub

Private Sub lblArrJob2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrJob2(Index), True
End Sub
Private Sub lblArrJob2_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblArrJob2(Index), False
End Sub
Private Sub BringArrPanelToFront(Index As Integer)
    ArrFlight(Index).ZOrder
    picArr1(Index).ZOrder
    picArr2(Index).ZOrder
    picArr3(Index).ZOrder
    picArr4(Index).ZOrder
End Sub


Private Sub lblArrEtai_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrGta1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOnbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOrg3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub AdjustArrIconSize(jIdx As Integer, NewSize As Long)
    Dim tmpIdxTag As String
    Dim iBar As Integer
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim MaxHeight As Long
    Dim NewLeft As Long
    NewHeight = NewSize
    MaxHeight = ChartLine2(0).Height - 60
    If NewHeight > MaxHeight Then NewHeight = MaxHeight
    NewTop = (MaxHeight - NewHeight) \ 2
    tmpIdxTag = CStr(jIdx)
    For iBar = 0 To CurBarMax
        If (jIdx < 0) Or (picArr1(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr1(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr2(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr2(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr3(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr3(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr4(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr4(iBar), NewTop, NewHeight
        NewLeft = lblArrBar(iBar).Left + 15
        picArr1(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr1(iBar).Width
        picArr2(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr2(iBar).Width
        picArr3(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr3(iBar).Width
        picArr4(iBar).Left = NewLeft
    Next
End Sub
Private Sub AdjustDepIconSize(jIdx As Integer, NewSize As Long)
    Dim tmpIdxTag As String
    Dim iBar As Integer
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim MaxHeight As Long
    Dim NewLeft As Long
    NewHeight = NewSize
    MaxHeight = ChartLine2(0).Height - 60
    If NewHeight > MaxHeight Then NewHeight = MaxHeight
    NewTop = (MaxHeight - NewHeight) \ 2
    tmpIdxTag = CStr(jIdx)
    For iBar = 0 To CurBarMax
        If (jIdx < 0) Or (picDep1(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep1(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep2(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep2(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep3(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep3(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep4(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep4(iBar), NewTop, NewHeight
        NewLeft = lblDepBar(iBar).Left + lblDepBar(iBar).Width - 15
        NewLeft = NewLeft - picDep4(iBar).Width
        picDep4(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep3(iBar).Width
        picDep3(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep2(iBar).Width
        picDep2(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep1(iBar).Width
        picDep1(iBar).Left = NewLeft
    Next
End Sub
Private Sub SetIconDimension(CurImg As Image, SetTop As Long, SetSize As Long)
    CurImg.Top = SetTop
    CurImg.Height = SetSize
    CurImg.Width = SetSize
End Sub
Private Sub lblArrPsta_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrStoa_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepBar(Index), True
End Sub
Private Sub lblDepBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblDepBar(Index), False
End Sub

Private Sub lblDepJob1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepJob1(Index), True
End Sub
Private Sub lblDepJob1_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblDepJob1(Index), False
End Sub
Private Sub lblDepJob2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepJob2(Index), True
End Sub
Private Sub lblDepJob2_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblDepJob2(Index), False
End Sub

Private Sub BringDepPanelToFront(Index As Integer)
    DepFlight(Index).ZOrder
    picDep1(Index).ZOrder
    picDep2(Index).ZOrder
    picDep3(Index).ZOrder
    picDep4(Index).ZOrder
End Sub

Private Sub lblDepDes3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepEtdi_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepGtd1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepOfbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepPstd_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepStod_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblTowBar_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ShowRotaTimeFrame(Index As Integer, CurBarObj As Label, ShowRange As Boolean)
    Dim RotLeft As Long
    Dim RotRight As Long
    If ShowRange = True Then
        RotLeft = CurBarObj.Left - 15
        RotRight = CurBarObj.Left + CurBarObj.Width - 15
        RangeLine(0).Left = RotLeft
        RangeLine(1).Left = RotRight
        icoCflRange(0).Left = RotLeft - 105
        icoCflRange(1).Left = RotRight - 105
        RangeLine(0).Visible = True
        RangeLine(1).Visible = True
        icoCflRange(0).Visible = True
        icoCflRange(1).Visible = True
    Else
        RangeLine(0).Visible = False
        RangeLine(1).Visible = False
        icoCflRange(0).Visible = False
        icoCflRange(1).Visible = False
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        OnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub picArr1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr4_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringArrPanelToFront Index
End Sub

Private Sub picArrPic_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iBar As Integer
    Dim iNxt As Integer
    Dim iMax As Integer
    Dim iLoop1 As Integer
    Dim iLoop2 As Integer
    Dim iGotBar As Integer
    Dim LookVal As String
    Dim CurScrollValue As Integer
    Dim NewScrollValue As Integer
    Dim TopIdx As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim BarLeft As Long
    Dim WinLeft As Long
    Dim BarRight As Long
    Dim WinRight As Long
    Dim NewLeft As Long
    Dim ScrollIt As Boolean
    picArrPic(Index).Top = picArrPic(Index).Top + 15
    picArrPic(Index).Left = picArrPic(Index).Left + 15
    picArrPic(Index).Refresh
    LookVal = CStr(Index)
    If CurBarIdx < 0 Then CurBarIdx = VScroll2(0).Value
    iGotBar = -1
    iNxt = CurBarIdx + 1
    iMax = CurBarMax
    iLoop1 = 0
    While (iLoop1 < 2) And (iGotBar < 0)
        iLoop1 = iLoop1 + 1
        iBar = iNxt
        While (iBar <= iMax) And (iGotBar < 0)
            If picArr1(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr2(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr3(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr4(iBar).Tag = LookVal Then iGotBar = iBar
            iBar = iBar + 1
        Wend
        If iGotBar < 0 Then
            iNxt = 0
            iMax = CurBarIdx
        End If
    Wend
    If iGotBar >= 0 Then
        CurScrollValue = VScroll2(0).Value
        LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height))
        TopIdx = CurScrollValue
        BotIdx = TopIdx + LinCnt - 1
        NewScrollValue = -1
        If iGotBar < TopIdx Then NewScrollValue = iGotBar
        If iGotBar > BotIdx Then NewScrollValue = iGotBar - LinCnt + 1
        If NewScrollValue >= 0 Then VScroll2(0).Value = NewScrollValue
        BarLeft = lblArrBar(iGotBar).Left
        BarRight = BarLeft + 2000
        BarLeft = BarLeft - ArrFlight(iGotBar).Width
        WinLeft = Abs(HScrollArea(0).Left)
        WinRight = WinLeft + WorkArea(0).Width
        ScrollIt = False
        If BarLeft < WinLeft Then
            NewLeft = BarLeft
            ScrollIt = True
        End If
        If BarRight > WinRight Then
            NewLeft = BarRight - WorkArea(0).Width
            ScrollIt = True
        End If
        If ScrollIt = True Then
            NewScrollValue = CInt(NewLeft \ MinuteWidth)
            If NewScrollValue < 0 Then NewScrollValue = 0
            If NewScrollValue > HScroll2(0).Max Then NewScrollValue = HScroll2(0).Max
            HScroll2(0).Value = NewScrollValue
        End If
        HighlightCurrentBar iGotBar, ChartName
    End If
End Sub

Private Sub picArrPic_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    picArrPic(Index).Top = picArrPic(Index).Top - 15
    picArrPic(Index).Left = picArrPic(Index).Left - 15
    picArrPic(Index).Refresh
End Sub

Private Sub picDep1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep4_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    BringDepPanelToFront Index
End Sub

Private Sub picDepPic_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iBar As Integer
    Dim iNxt As Integer
    Dim iMax As Integer
    Dim iLoop1 As Integer
    Dim iLoop2 As Integer
    Dim iGotBar As Integer
    Dim LookVal As String
    Dim CurScrollValue As Integer
    Dim NewScrollValue As Integer
    Dim TopIdx As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim BarLeft As Long
    Dim WinLeft As Long
    Dim BarRight As Long
    Dim WinRight As Long
    Dim NewLeft As Long
    Dim ScrollIt As Boolean
    picDepPic(Index).Top = picDepPic(Index).Top + 15
    picDepPic(Index).Left = picDepPic(Index).Left + 15
    picDepPic(Index).Refresh
    LookVal = CStr(Index)
    If CurBarIdx < 0 Then CurBarIdx = VScroll2(0).Value
    iGotBar = -1
    iNxt = CurBarIdx + 1
    iMax = CurBarMax
    iLoop1 = 0
    While (iLoop1 < 2) And (iGotBar < 0)
        iLoop1 = iLoop1 + 1
        iBar = iNxt
        While (iBar <= iMax) And (iGotBar < 0)
            If picDep1(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep2(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep3(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep4(iBar).Tag = LookVal Then iGotBar = iBar
            iBar = iBar + 1
        Wend
        If iGotBar < 0 Then
            iNxt = 0
            iMax = CurBarIdx
        End If
    Wend
    If iGotBar >= 0 Then
        CurScrollValue = VScroll2(0).Value
        LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height))
        TopIdx = CurScrollValue
        BotIdx = TopIdx + LinCnt - 1
        NewScrollValue = -1
        If iGotBar < TopIdx Then NewScrollValue = iGotBar
        If iGotBar > BotIdx Then NewScrollValue = iGotBar - LinCnt + 1
        If NewScrollValue >= 0 Then VScroll2(0).Value = NewScrollValue
        BarLeft = lblDepBar(iGotBar).Left + lblDepBar(iGotBar).Width
        BarRight = BarLeft + DepFlight(iGotBar).Width
        BarLeft = BarLeft - 2000
        WinLeft = Abs(HScrollArea(0).Left)
        WinRight = WinLeft + WorkArea(0).Width
        ScrollIt = False
        If BarLeft < WinLeft Then
            NewLeft = BarLeft
            ScrollIt = True
        End If
        If BarRight > WinRight Then
            NewLeft = BarRight - WorkArea(0).Width
            ScrollIt = True
        End If
        If ScrollIt = True Then
            NewScrollValue = CInt(NewLeft \ MinuteWidth)
            If NewScrollValue < 0 Then NewScrollValue = 0
            If NewScrollValue > HScroll2(0).Max Then NewScrollValue = HScroll2(0).Max
            HScroll2(0).Value = NewScrollValue
        End If
        HighlightCurrentBar iGotBar, ChartName
    End If
End Sub

Private Sub picDepPic_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    picDepPic(Index).Top = picDepPic(Index).Top - 15
    picDepPic(Index).Left = picDepPic(Index).Left - 15
    picDepPic(Index).Refresh
End Sub

Private Sub picDepStat_DblClick(Index As Integer)
    Dim tmpTag As String
    Dim tmpIdx As Integer
    tmpTag = picDepStat(Index).Tag
    tmpIdx = Val(tmpTag) + 1
    If tmpIdx > picMarker.UBound Then tmpIdx = 0
    picDepStat(Index).Picture = picMarker(tmpIdx).Picture
    picDepStat(Index).Tag = CStr(tmpIdx)
End Sub

Private Sub TimeScaleTimer_Timer()
    Dim DisplayTime As String
    Dim DisplayDate As String
    Dim NewLeft As Long
    Dim LeftOff As Long
    Dim TimeOff As Long
    Dim RetVal As Boolean
    Dim CurLocTime
    Dim CurUtcTime
    Dim SecDiff
    
    'The time synchronization will be done based on the server time.
    'So we must calculate the differences of the elapsed time since the last timer event.
    CurLocTime = Now
    SecDiff = DateDiff("s", LastLocTimeCheck, CurLocTime)
    LastLocTimeCheck = CurLocTime
    ServerUtcTimeVal = DateAdd("s", SecDiff, ServerUtcTimeVal)
    LastUtcTimeCheck = ServerUtcTimeVal
    If chkUtc(0).Value = 1 Then
        DisplayTime = Format(LastUtcTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastUtcTimeCheck, "YYYYMMDD")
    Else
        DisplayTime = Format(LastLocTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastLocTimeCheck, "YYYYMMDD")
    End If
    DisplayDate = DecodeSsimDayFormat(DisplayDate, "CEDA", "SSIM2")
    lblCurTime(0).Caption = DisplayTime
    lblCurTime(0).ToolTipText = DisplayDate
    
    RetVal = GetTimeScalePos("", ServerUtcTimeVal, NewLeft)
    If RetVal = True Then
        NewLeft = NewLeft - 15
        TimeOff = NewLeft - TimeLine1(0).Left
        If TimeOff <> 0 Then
            lblCurTime(0).Visible = False
            TimeLine1(0).Visible = False
            TimeLine2(0).Visible = False
            LeftOff = TimeLine1(0).Left - lblCurTime(0).Left
            TimeLine1(0).Left = NewLeft
            TimeLine2(0).Left = NewLeft
            lblCurTime(0).Left = NewLeft - LeftOff
            If chkTime1(0).Value = 1 Then
                If HScroll2(0).Value < 3600 Then
                    HScroll2(0).Value = HScroll2(0).Value + 1
                End If
            End If
            TimeLine2(0).Visible = True
            TimeLine1(0).Visible = True
            lblCurTime(0).Visible = True
        End If
    Else
        lblCurTime(0).Visible = False
        TimeLine1(0).Visible = False
        TimeLine2(0).Visible = False
    End If
End Sub

Private Function GetTimeScalePos(CedaUtcTime As String, CedaTimeValue, ScalePos As Long) As Boolean
    Dim MinDiff As Long
    If CedaUtcTime <> "" Then CedaTimeValue = CedaFullDateToVb(CedaUtcTime)
    MinDiff = CLng(DateDiff("n", TimeScaleUtcBegin, CedaTimeValue))
    If (MinDiff >= 0) And (MinDiff <= CLng(lblMin1.UBound)) Then
        ScalePos = lblMin1(CInt(MinDiff)).Left
        GetTimeScalePos = True
    Else
        ScalePos = MinDiff * MinuteWidth
        GetTimeScalePos = False
    End If
End Function
Private Sub VScroll1_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll1_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub VScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub AdjustVertScroll(ScrollValue As Long)
    Dim NewTop As Long
    NewTop = ScrollValue * ChartLine2(0).Height
    VScrollArea(0).Top = -NewTop
    WorkArea(0).Refresh
End Sub

Private Sub ToggleFlightBarTimes(TimeOffs As Integer)
    Dim i As Integer
    For i = 0 To CurBarMax
        SetTimeFieldCaption lblArrStoa(i), TimeOffs
        SetTimeFieldCaption lblArrEtai(i), TimeOffs
        SetTimeFieldCaption lblArrOnbl(i), TimeOffs
        SetTimeFieldCaption lblDepStod(i), TimeOffs
        SetTimeFieldCaption lblDepEtdi(i), TimeOffs
        SetTimeFieldCaption lblDepOfbl(i), TimeOffs
    Next
End Sub
Private Sub SetTimeFieldCaption(CurLbl As Label, TimeOffs As Integer)
    Dim tmpTag As String
    Dim tmpTxt As String
    Dim tmpTime
    tmpTag = CurLbl.Tag
    tmpTxt = ""
    If tmpTag <> "" Then
        tmpTime = CedaFullDateToVb(tmpTag)
        tmpTime = DateAdd("n", TimeOffs, tmpTime)
        tmpTxt = Format(tmpTime, "hh:mm")
    End If
    CurLbl.Caption = tmpTxt
End Sub

Public Sub GetFlightRotations()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DepLine As Long
    Dim ArrLine As Long
    Dim ArrRotLine As Long
    Dim DepRotLine As Long
    Dim iBar As Integer
    Dim jBar As Integer
    Dim i As Integer
    Dim tmpData As String
    Dim tmpAidx As String
    Dim tmpDidx As String
    Dim tmpRidx As String
    Dim tmpGidx As String
    Dim tmpView As String
    Dim tmpHitLine As String
    Dim RotBest As String
    Dim ArrTifa As String
    Dim DepTifd As String
    Dim DepOfbl As String
    Dim TopPos As Long
    Dim RotScalePos As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DepLeft As Long
    Dim ArrLeft As Long
    Dim PicLeft As Long
    Dim RotWidth As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim MinDepWidth As Long
    Dim MaxDepWidth As Long
    Dim MinArrWidth As Long
    Dim MinRotWidth As Long
    Dim MaxArrWidth As Long
    Dim IdxColNo As Long
    Dim IsValidRotScalePos As Boolean
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim ArrVisible As Boolean
    Dim DepVisible As Boolean
    Dim ShowJobs As Boolean
    Dim RotBestValue
    Dim DepTifdValue
    Dim ArrTifaValue
    
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "SHOW_ALL_ICONS", "NO"))
    If tmpData = "YES" Then ShowAllIcons = True Else ShowAllIcons = False
    
    For i = 0 To lblArrPicCount.UBound
        lblArrPicCount(i).Caption = ""
        lblArrPicCount(i).Tag = ""
    Next
    For i = 0 To lblDepPicCount.UBound
        lblDepPicCount(i).Caption = ""
        lblDepPicCount(i).Tag = ""
    Next
    chkSelDeco(1).Value = 1
    chkSelDeco(0).Value = 1
    LeftScale(0).Refresh
    RightScale(0).Refresh
    CreateTimeScaleArea TimeScale(0)
    MaxDepWidth = MinuteWidth * 60
    MaxArrWidth = MinuteWidth * 60
    MinDepWidth = MinuteWidth * 20
    MinArrWidth = MinuteWidth * 20
    MinRotWidth = MinuteWidth * 15
    TabRotFlightsTab.IndexDestroy "GOCX"
    ShowJobs = False
    iBar = 0
    MaxLine = TabRotFlightsTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        If iBar <= ChartLine2.UBound Then
            tmpView = "1"
            tmpView = TabRotFlightsTab.GetFieldValue(CurLine, "VIEW")
            If tmpView = 1 Then
                ArrLine = -1
                DepLine = -1
                ArrRotLine = -1
                DepRotLine = -1
                tmpRidx = TabRotFlightsTab.GetFieldValue(CurLine, "RIDX")
                tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                If tmpHitLine <> "" Then ArrRotLine = Val(tmpHitLine)
                tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                If tmpHitLine <> "" Then DepRotLine = Val(tmpHitLine)
                If ArrRotLine >= 0 Then
                    tmpAidx = TabRotFlightsTab.GetFieldValue(CurLine, "AIDX")
                    tmpData = TabArrFlightsTab.GetFieldValue(ArrRotLine, "'S3'")
                    If tmpAidx = tmpData Then
                        ArrLine = ArrRotLine
                    Else
                        'Here we have a problem because
                        'the index doesn't point to the flight line
                        ArrLine = ArrRotLine
                    End If
                End If
                If DepRotLine >= 0 Then
                    tmpDidx = TabRotFlightsTab.GetFieldValue(CurLine, "DIDX")
                    tmpData = TabDepFlightsTab.GetFieldValue(DepRotLine, "'S3'")
                    If tmpDidx = tmpData Then
                        DepLine = DepRotLine
                    Else
                        'Here we have a problem because
                        'the index doesn't point to the flight line
                        DepLine = DepRotLine
                    End If
                End If
                tmpGidx = Right("000000" & CStr(iBar), 6)
                GetArrFlightData ArrLine, iBar
                GetDepFlightData DepLine, iBar
                picSortTime(iBar).Visible = False
                RotBest = TabRotFlightsTab.GetFieldValue(CurLine, "BEST")
                IsValidRotScalePos = GetTimeScalePos(RotBest, RotBestValue, RotScalePos)
                picSortTime(iBar).Left = RotScalePos - (picSortTime(iBar).Width \ 2) - 15
                If (ArrLine >= 0) And (DepLine >= 0) Then
                    'ROTATION
                    DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                    IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                    ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                    IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                    RotWidth = DepScalePos - ArrScalePos + 15
                    If RotWidth >= MinRotWidth Then
                        lblBarLineColor(iBar).BackColor = ChartBarBackColor
                        lblBarLineColor(iBar).Tag = "7"
                    Else
                        lblBarLineColor(iBar).BackColor = vbRed
                        lblBarLineColor(iBar).Tag = "1"
                    End If
                    ArrWidth = RotWidth \ 2
                    DepWidth = RotWidth - ArrWidth
                    If DepWidth > MaxDepWidth Then DepWidth = MaxDepWidth
                    If DepWidth < MinDepWidth Then DepWidth = MinDepWidth
                    DepLeft = DepScalePos - DepWidth
                    ArrWidth = RotWidth
                    If ArrWidth < MinArrWidth Then ArrWidth = MinArrWidth
                    ArrLeft = ArrScalePos
                    ArrVisible = True
                    DepVisible = True
                ElseIf DepLine >= 0 Then
                    'SINGLE DEPARTURE
                    DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                    IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                    DepWidth = MaxDepWidth
                    DepLeft = DepScalePos - DepWidth
                    ArrLeft = 0
                    ArrWidth = 150
                    ArrVisible = False
                    DepVisible = True
                Else
                    'SINGLE ARRIVAL
                    ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                    IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                    ArrWidth = MaxArrWidth
                    ArrLeft = ArrScalePos
                    DepLeft = 0
                    DepWidth = 150
                    ArrVisible = True
                    DepVisible = False
                End If
                lblArrBar(iBar).Left = ArrLeft
                lblArrBar(iBar).Width = ArrWidth
                lblArrBar(iBar).Visible = ArrVisible
                lblArrJob1(iBar).Left = ArrLeft
                lblArrJob1(iBar).Width = ArrWidth
                lblArrJob2(iBar).Left = ArrLeft
                lblArrJob2(iBar).Width = ArrWidth
                lblArrJob1(iBar).ZOrder
                lblArrJob2(iBar).ZOrder
               
                lblArrJob1(iBar).Visible = False
                lblArrJob2(iBar).Visible = False
               
                picArr1(iBar).Visible = False
                picArr2(iBar).Visible = False
                picArr3(iBar).Visible = False
                picArr4(iBar).Visible = False
                If ArrVisible = True Then
                    PicLeft = ArrLeft
                    If picArr1(iBar).Tag <> "" Then
                        picArr1(iBar).Left = PicLeft
                        picArr1(iBar).Visible = True
                        picArr1(iBar).ZOrder
                        PicLeft = PicLeft + picArr1(iBar).Width
                    End If
                    If picArr2(iBar).Tag <> "" Then
                        picArr2(iBar).Left = PicLeft
                        picArr2(iBar).Visible = True
                        picArr2(iBar).ZOrder
                        PicLeft = PicLeft + picArr2(iBar).Width
                    End If
                    If picArr3(iBar).Tag <> "" Then
                        picArr3(iBar).Left = PicLeft
                        picArr3(iBar).Visible = True
                        picArr3(iBar).ZOrder
                        PicLeft = PicLeft + picArr3(iBar).Width
                    End If
                    If picArr4(iBar).Tag <> "" Then
                        picArr4(iBar).Left = PicLeft
                        picArr4(iBar).Visible = True
                        picArr4(iBar).ZOrder
                    End If
                End If
               
                lblDepBar(iBar).Left = DepLeft
                lblDepBar(iBar).Width = DepWidth
                lblDepBar(iBar).Visible = DepVisible
                lblDepJob1(iBar).Left = DepLeft
                lblDepJob1(iBar).Width = DepWidth
                lblDepJob2(iBar).Left = DepLeft
                lblDepJob2(iBar).Width = DepWidth
                
                lblDepBar(iBar).ZOrder
                lblDepJob1(iBar).ZOrder
                lblDepJob2(iBar).ZOrder
                
                lblDepJob1(iBar).Visible = False
                lblDepJob2(iBar).Visible = False
                
                picDep1(iBar).Visible = False
                picDep2(iBar).Visible = False
                picDep3(iBar).Visible = False
                picDep4(iBar).Visible = False
                If DepVisible = True Then
                    PicLeft = DepLeft + DepWidth
                    If picDep4(iBar).Tag <> "" Then
                        PicLeft = PicLeft - picDep4(iBar).Width
                        picDep4(iBar).Left = PicLeft
                        picDep4(iBar).Visible = True
                        picDep4(iBar).ZOrder
                    End If
                    If picDep3(iBar).Tag <> "" Then
                        PicLeft = PicLeft - picDep3(iBar).Width
                        picDep3(iBar).Left = PicLeft
                        picDep3(iBar).Visible = True
                        picDep3(iBar).ZOrder
                    End If
                    If picDep2(iBar).Tag <> "" Then
                        PicLeft = PicLeft - picDep2(iBar).Width
                        picDep2(iBar).Left = PicLeft
                        picDep2(iBar).Visible = True
                        picDep2(iBar).ZOrder
                    End If
                    If picDep1(iBar).Tag <> "" Then
                        PicLeft = PicLeft - picDep1(iBar).Width
                        picDep1(iBar).Left = PicLeft
                        picDep1(iBar).Visible = True
                        picDep1(iBar).ZOrder
                    End If
                End If
                picSortTime(iBar).Visible = True
                picSortTime(iBar).ZOrder
                
                SetArrBarColors ArrLine, DepLine, iBar
                SetDepBarColors ArrLine, DepLine, iBar
                
                tmpGidx = tmpView & "," & tmpGidx
                TabRotFlightsTab.SetFieldValues CurLine, "VIEW,GOCX", tmpGidx
                TabRotFlightsTab.SetLineColor CurLine, vbBlack, vbWhite
                ChartLine2(iBar).BackColor = lblBarLineColor(iBar).BackColor
                iBar = iBar + 1
            Else
                TabRotFlightsTab.SetFieldValues CurLine, "VIEW,GOCX", tmpView & ",------"
                TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
            End If
        Else
            tmpView = "0"
            TabRotFlightsTab.SetFieldValues CurLine, "VIEW,GOCX", tmpView & ",------"
            TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
        End If
    Next
    jBar = iBar - 1
    If jBar < 0 Then jBar = 0
    TopPos = ChartLine2(jBar).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 60
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    RangeLine(0).Height = CurChartHeight
    RangeLine(1).Height = CurChartHeight
    VScroll1(0).Max = jBar
    VScroll2(0).Max = jBar
    
    TabRotFlightsTab.AutoSizeColumns
    IdxColNo = CLng(GetRealItemNo(TabRotFlightsTab.LogicalFieldList, "GOCX"))
    TabRotFlightsTab.IndexCreate "GOCX", IdxColNo
    TabRotFlightsTab.Refresh
    Me.Refresh
    chkSelDeco(1).Value = 0
    chkSelDeco(0).Value = 0
    LeftScale(0).Refresh
    RightScale(0).Refresh
    Me.Refresh
End Sub

Private Sub GetArrFlightData(AftLineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim iBar As Integer
    Dim ArrFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim ArrFldItm As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then TimeOffs = UtcTimeDiff Else TimeOffs = 0
    CurLine = AftLineNo
    iBar = BarIdx
    ClearArrFlight iBar
    ArrFields = "FTYP,FLNO,FLTI,ORG3,VIA3,STOA,ETAI,ONBL,PSTA,GTA1,REGN,ACT3,VERS"
    ArrFldItm = 1
    FldName = "START"
    While FldName <> ""
        FldName = GetItem(ArrFields, ArrFldItm, ",")
        If FldName <> "" Then
            FldValue = Trim(TabArrFlightsTab.GetFieldValue(CurLine, FldName))
            Select Case FldName
                Case "FTYP"
                    lblArrFtyp(iBar).Tag = FldValue
                    lblArrFtyp(iBar).Caption = FldValue
                Case "FLNO"
                    lblArrFlno(iBar).Tag = FldValue
                    lblArrFlno(iBar).Caption = FldValue
                Case "FLTI"
                    lblArrFlti(iBar).Tag = FldValue
                    lblArrFlti(iBar).Caption = FldValue
                Case "ORG3"
                    lblArrOrg3(iBar).Tag = FldValue
                    lblArrOrg3(iBar).Caption = FldValue
                Case "VIA3"
                    lblArrVia3(iBar).Tag = FldValue
                    lblArrVia3(iBar).Caption = FldValue
                Case "STOA"
                    lblArrStoa(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrStoa(iBar), TimeOffs
                Case "ETAI"
                    lblArrEtai(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrEtai(iBar), TimeOffs
                Case "ONBL"
                    lblArrOnbl(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrOnbl(iBar), TimeOffs
                Case "PSTA"
                    lblArrPsta(iBar).Tag = FldValue
                    lblArrPsta(iBar).Caption = FldValue
                Case "GTA1"
                    lblArrGta1(iBar).Tag = FldValue
                    lblArrGta1(iBar).Caption = FldValue
                Case "REGN"
                    lblArrRegn(iBar).Tag = FldValue
                    lblArrRegn(iBar).Caption = FldValue
                Case "ACT3"
                    lblArrAct3(iBar).Tag = FldValue
                    lblArrAct3(iBar).Caption = FldValue
                Case "VERS"
                    If ShowAllIcons Then
                        If FldValue = "" Then FldValue = "ABCDEFG"
                    End If
                    CreateArrStatusPics iBar, FldValue
                Case Else
            End Select
        End If
        ArrFldItm = ArrFldItm + 1
    Wend
    ArrFlight(iBar).Visible = True
End Sub
Private Sub SetArrBarColors(ArrLineNo As Long, DepLineNo As Long, BarIdx As Integer)
    Dim iBar As Integer
    Dim ArrFtyp As String
    Dim ArrTisa As String
    Dim DepTisd As String
    If ArrLineNo >= 0 Then
        iBar = BarIdx
        SetArrPanelColors iBar, vbBlack, vbWhite
        ArrFtyp = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "FTYP"))
        If ArrFtyp = "" Then ArrFtyp = " "
        If InStr("XND", ArrFtyp) > 0 Then
            lblArrBar(iBar).BackColor = vbRed
            lblArrBar(iBar).ForeColor = vbWhite
            lblArrFlno(iBar).BackColor = vbRed
            lblArrFlno(iBar).ForeColor = vbWhite
            lblArrBar(iBar).ToolTipText = ""
            Select Case ArrFtyp
                Case "X"
                    lblArrBar(iBar).Caption = "CXX"
                    picArrStat(iBar).Picture = picAftFtyp(3).Picture
                Case "N"
                    lblArrBar(iBar).Caption = "NOP"
                    picArrStat(iBar).Picture = picAftFtyp(4).Picture
                Case "D"
                    lblArrBar(iBar).Caption = "DIV"
                    picArrStat(iBar).Picture = picAftFtyp(5).Picture
                Case Else
                    'We never come here
            End Select
        Else
            Select Case ArrFtyp
                Case "O"
                    picArrStat(iBar).Picture = picAftFtyp(0).Picture
                Case "S"
                    picArrStat(iBar).Picture = picAftFtyp(1).Picture
                Case " "
                    picArrStat(iBar).Picture = picAftFtyp(2).Picture
                Case Else
                    picArrStat(iBar).Picture = picAftFtyp(2).Picture
            End Select
            lblArrBar(iBar).Caption = ""
            ArrTisa = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "TISA"))
            If ArrTisa = "" Then ArrTisa = " "
            DepTisd = ""
            If DepLineNo >= 0 Then DepTisd = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "TISD"))
            If DepTisd <> "A" Then
                Select Case ArrTisa
                    Case "S"
                        lblArrBar(iBar).BackColor = RGB(235, 235, 255)
                        lblArrBar(iBar).ToolTipText = "Scheduled"
                    Case "E"
                        lblArrBar(iBar).BackColor = vbWhite
                        lblArrBar(iBar).ToolTipText = "Estimated"
                    Case "T"
                        lblArrBar(iBar).BackColor = LightYellow
                        lblArrBar(iBar).ToolTipText = "Ten Miles Out"
                    Case "L"
                        lblArrBar(iBar).BackColor = LightGreen
                        lblArrBar(iBar).ToolTipText = "Landed"
                    Case "O"
                        lblArrBar(iBar).BackColor = vbGreen
                        lblArrBar(iBar).ToolTipText = "Onblock"
                    Case Else
                        lblArrBar(iBar).BackColor = vbMagenta
                        lblArrBar(iBar).ToolTipText = "Time Status Unknown"
                End Select
            Else
                SetArrPanelColors iBar, vbBlack, LightGrey
                lblArrBar(iBar).BackColor = LightGrey
                lblArrBar(iBar).ToolTipText = "Already Departed"
            End If
        End If
    End If
End Sub

Private Sub CreateArrStatusPics(iBar As Integer, FldValue As String)
    Dim i As Integer
    Dim j As Integer
    Dim iLen As Integer
    Dim iCnt As Integer
    Dim jIdx As Integer
    Dim tmpChar As String
    Dim tmpVers As String
    tmpVers = UCase(Trim(FldValue))
    tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
    iCnt = 0
    iLen = Len(tmpVers)
    For i = 1 To iLen
        tmpChar = Mid(tmpVers, i, 1)
        jIdx = -1
        For j = 0 To lblArrPicType.UBound
            If lblArrPicType(j).Tag = tmpChar Then
                jIdx = j
                Exit For
            End If
        Next
        If jIdx >= 0 Then
            If iCnt < 4 Then
                iCnt = iCnt + 1
                Select Case iCnt
                    Case 1
                        SetArrStatusPic iBar, jIdx, picArr1(iBar)
                    Case 2
                        SetArrStatusPic iBar, jIdx, picArr2(iBar)
                    Case 3
                        SetArrStatusPic iBar, jIdx, picArr3(iBar)
                    Case 4
                        SetArrStatusPic iBar, jIdx, picArr4(iBar)
                    Case Else
                End Select
            End If
        End If
    Next
    'Clear unused pics
    iCnt = iCnt + 1
    For i = iCnt To 4
        Select Case i
            Case 1
                ClearArrStatusPic iBar, picArr1(iBar)
            Case 2
                ClearArrStatusPic iBar, picArr2(iBar)
            Case 3
                ClearArrStatusPic iBar, picArr3(iBar)
            Case 4
                ClearArrStatusPic iBar, picArr4(iBar)
            Case Else
        End Select
    Next
End Sub
Private Sub SetArrStatusPic(iBar As Integer, jIdx As Integer, CurPic As Image)
    Dim PicCnt As Integer
    Dim tmpTag As String
    CurPic.Picture = picArrPic(jIdx).Picture
    PicCnt = Val(lblArrPicCount(jIdx).Caption) + 1
    lblArrPicCount(jIdx).Caption = CStr(PicCnt)
    tmpTag = CStr(jIdx)
    CurPic.Tag = tmpTag
    tmpTag = lblArrPicText(jIdx).Caption
    CurPic.ToolTipText = tmpTag
End Sub
Private Sub ClearArrStatusPic(iBar As Integer, CurPic As Image)
    CurPic.Tag = ""
End Sub

Private Sub GetDepFlightData(AftLineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim iBar As Integer
    Dim DepFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim DepFldItm As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then TimeOffs = UtcTimeDiff Else TimeOffs = 0
    CurLine = AftLineNo
    iBar = BarIdx
    ClearDepFlight iBar
    DepFields = "FTYP,FLNO,FLTI,DES3,VIA3,STOD,ETDI,OFBL,PSTD,GTD1,REGN,ACT3,VERS"
    DepFldItm = 1
    FldName = "START"
    While FldName <> ""
        FldName = GetItem(DepFields, DepFldItm, ",")
        If FldName <> "" Then
            FldValue = TabDepFlightsTab.GetFieldValue(CurLine, FldName)
            Select Case FldName
                Case "FTYP"
                    lblDepFtyp(iBar).Tag = FldValue
                    lblDepFtyp(iBar).Caption = FldValue
                Case "FLNO"
                    lblDepFlno(iBar).Tag = FldValue
                    lblDepFlno(iBar).Caption = FldValue
                Case "FLTI"
                    lblDepFlti(iBar).Tag = FldValue
                    lblDepFlti(iBar).Caption = FldValue
                Case "DES3"
                    lblDepDes3(iBar).Tag = FldValue
                    lblDepDes3(iBar).Caption = FldValue
                Case "VIA3"
                    lblDepVia3(iBar).Tag = FldValue
                    lblDepVia3(iBar).Caption = FldValue
                Case "STOD"
                    lblDepStod(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepStod(iBar), TimeOffs
                Case "ETDI"
                    lblDepEtdi(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepEtdi(iBar), TimeOffs
                Case "OFBL"
                    lblDepOfbl(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepOfbl(iBar), TimeOffs
                Case "PSTD"
                    lblDepPstd(iBar).Tag = FldValue
                    lblDepPstd(iBar).Caption = FldValue
                Case "GTD1"
                    lblDepGtd1(iBar).Tag = FldValue
                    lblDepGtd1(iBar).Caption = FldValue
                Case "REGN"
                    lblDepRegn(iBar).Tag = FldValue
                    lblDepRegn(iBar).Caption = FldValue
                Case "ACT3"
                    lblDepAct3(iBar).Tag = FldValue
                    lblDepAct3(iBar).Caption = FldValue
                Case "VERS"
                    If ShowAllIcons Then
                        If FldValue = "" Then FldValue = "ABCDEFG"
                    End If
                    CreateDepStatusPics iBar, FldValue
                Case Else
            End Select
        End If
        DepFldItm = DepFldItm + 1
    Wend
    DepFlight(iBar).Visible = True
End Sub

Private Sub SetDepBarColors(ArrLineNo As Long, DepLineNo As Long, BarIdx As Integer)
    Dim iBar As Integer
    Dim DepFtyp As String
    Dim ArrTisa As String
    Dim DepTisd As String
    If DepLineNo >= 0 Then
        iBar = BarIdx
        SetDepPanelColors iBar, vbBlack, vbWhite
        DepFtyp = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "FTYP"))
        If DepFtyp = "" Then DepFtyp = " "
        If InStr("XND", DepFtyp) > 0 Then
            lblDepBar(iBar).BackColor = vbRed
            lblDepBar(iBar).ForeColor = vbWhite
            lblDepFlno(iBar).BackColor = vbRed
            lblDepFlno(iBar).ForeColor = vbWhite
            lblDepBar(iBar).ToolTipText = ""
            Select Case DepFtyp
                Case "X"
                    lblDepBar(iBar).Caption = "CXX"
                    picDepStat(iBar).Picture = picAftFtyp(3).Picture
                Case "N"
                    lblDepBar(iBar).Caption = "NOP"
                    picDepStat(iBar).Picture = picAftFtyp(4).Picture
                Case "D"
                    lblDepBar(iBar).Caption = "DIV"
                    picDepStat(iBar).Picture = picAftFtyp(5).Picture
                Case Else
                    'We never come here
            End Select
        Else
            Select Case DepFtyp
                Case "O"
                    picDepStat(iBar).Picture = picAftFtyp(0).Picture
                Case "S"
                    picDepStat(iBar).Picture = picAftFtyp(1).Picture
                Case " "
                    picDepStat(iBar).Picture = picAftFtyp(2).Picture
                Case Else
                    picDepStat(iBar).Picture = picAftFtyp(2).Picture
            End Select
            lblDepBar(iBar).Caption = ""
            DepTisd = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "TISD"))
            If DepTisd = "" Then DepTisd = " "
            'ArrTisa = ""
            'If ArrLineNo >= 0 Then ArrTisa = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "TISA"))
            'If ArrTisa <> "?" Then
                Select Case DepTisd
                    Case "S"
                        lblDepBar(iBar).BackColor = RGB(235, 235, 255)
                        lblDepBar(iBar).ToolTipText = "Scheduled"
                    Case "E"
                        lblDepBar(iBar).BackColor = vbWhite
                        lblDepBar(iBar).ToolTipText = "Estimated"
                    Case "O"
                        lblDepBar(iBar).BackColor = LightBlue
                        lblDepBar(iBar).ToolTipText = "Offblock"
                    Case "A"
                        lblDepBar(iBar).BackColor = LightGrey
                        lblDepBar(iBar).ToolTipText = "Airborne"
                        SetDepPanelColors iBar, vbBlack, LightGrey
                    Case Else
                        lblDepBar(iBar).BackColor = vbMagenta
                        lblDepBar(iBar).ToolTipText = "Time Status Unknown"
                End Select
            'Else
                'SetArrPanelColors iBar, vbBlack, LightGrey
                'lblArrBar(iBar).BackColor = LightGrey
                'lblArrBar(iBar).ToolTipText = "Already Departed"
            'End If
        End If
    End If
End Sub


Private Sub CreateDepStatusPics(iBar As Integer, FldValue As String)
    Dim i As Integer
    Dim j As Integer
    Dim iLen As Integer
    Dim iCnt As Integer
    Dim jIdx As Integer
    Dim tmpChar As String
    Dim tmpVers As String
    tmpVers = UCase(Trim(FldValue))
    tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
    'tmpVers = "BC"
    iCnt = 0
    iLen = Len(tmpVers)
    For i = 1 To iLen
        tmpChar = Mid(tmpVers, i, 1)
        jIdx = -1
        For j = 0 To lblDepPicType.UBound
            If lblDepPicType(j).Tag = tmpChar Then
                jIdx = j
                Exit For
            End If
        Next
        If jIdx >= 0 Then
            If iCnt < 4 Then
                iCnt = iCnt + 1
                Select Case iCnt
                    Case 1
                        SetDepStatusPic iBar, jIdx, picDep4(iBar)
                    Case 2
                        SetDepStatusPic iBar, jIdx, picDep3(iBar)
                    Case 3
                        SetDepStatusPic iBar, jIdx, picDep2(iBar)
                    Case 4
                        SetDepStatusPic iBar, jIdx, picDep1(iBar)
                    Case Else
                End Select
            End If
        End If
    Next
    'Clear unused pics
    iCnt = iCnt + 1
    For i = iCnt To 4
        Select Case i
            Case 1
                ClearDepStatusPic iBar, picDep4(iBar)
            Case 2
                ClearDepStatusPic iBar, picDep3(iBar)
            Case 3
                ClearDepStatusPic iBar, picDep2(iBar)
            Case 4
                ClearDepStatusPic iBar, picDep1(iBar)
            Case Else
        End Select
    Next
End Sub
Private Sub SetDepStatusPic(iBar As Integer, jIdx As Integer, CurPic As Image)
    Dim tmpTag As String
    Dim PicCnt As Integer
    CurPic.Picture = picDepPic(jIdx).Picture
    PicCnt = Val(lblDepPicCount(jIdx).Caption) + 1
    lblDepPicCount(jIdx).Caption = CStr(PicCnt)
    tmpTag = CStr(jIdx)
    CurPic.Tag = tmpTag
    CurPic.Visible = True
    tmpTag = lblDepPicText(jIdx).Caption
    CurPic.ToolTipText = tmpTag
End Sub
Private Sub ClearDepStatusPic(iBar As Integer, CurPic As Image)
    CurPic.Tag = ""
    CurPic.Visible = False
End Sub

Private Sub ClearArrFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    lblArrFtyp(iBar).Tag = FldValue
    lblArrFtyp(iBar).Caption = FldValue
    lblArrFlno(iBar).Tag = FldValue
    lblArrFlno(iBar).Caption = FldValue
    lblArrFlti(iBar).Tag = FldValue
    lblArrFlti(iBar).Caption = FldValue
    lblArrOrg3(iBar).Tag = FldValue
    lblArrOrg3(iBar).Caption = FldValue
    lblArrVia3(iBar).Tag = FldValue
    lblArrVia3(iBar).Caption = FldValue
    lblArrStoa(iBar).Tag = FldValue
    lblArrStoa(iBar).Caption = FldValue
    lblArrEtai(iBar).Tag = FldValue
    lblArrEtai(iBar).Caption = FldValue
    lblArrOnbl(iBar).Tag = FldValue
    lblArrOnbl(iBar).Caption = FldValue
    lblArrPsta(iBar).Tag = FldValue
    lblArrPsta(iBar).Caption = FldValue
    lblArrGta1(iBar).Tag = FldValue
    lblArrGta1(iBar).Caption = FldValue
    lblArrRegn(iBar).Tag = FldValue
    lblArrRegn(iBar).Caption = FldValue
    lblArrAct3(iBar).Tag = FldValue
    lblArrAct3(iBar).Caption = FldValue
    picArr1(iBar).Tag = FldValue
    picArr2(iBar).Tag = FldValue
    picArr3(iBar).Tag = FldValue
    picArr4(iBar).Tag = FldValue
End Sub
Private Sub SetArrPanelColors(iBar As Integer, SetForeColor As Long, SetBackColor As Long)
    lblArrFtyp(iBar).ForeColor = SetForeColor
    lblArrFtyp(iBar).BackColor = SetBackColor
    lblArrFlno(iBar).ForeColor = SetForeColor
    lblArrFlno(iBar).BackColor = SetBackColor
    lblArrFlti(iBar).ForeColor = SetForeColor
    lblArrFlti(iBar).BackColor = SetBackColor
    lblArrOrg3(iBar).ForeColor = SetForeColor
    lblArrOrg3(iBar).BackColor = SetBackColor
    lblArrVia3(iBar).ForeColor = SetForeColor
    lblArrVia3(iBar).BackColor = SetBackColor
    lblArrStoa(iBar).ForeColor = SetForeColor
    lblArrStoa(iBar).BackColor = SetBackColor
    lblArrEtai(iBar).ForeColor = SetForeColor
    lblArrEtai(iBar).BackColor = SetBackColor
    lblArrOnbl(iBar).ForeColor = SetForeColor
    lblArrOnbl(iBar).BackColor = SetBackColor
    lblArrPsta(iBar).ForeColor = SetForeColor
    lblArrPsta(iBar).BackColor = SetBackColor
    lblArrGta1(iBar).ForeColor = SetForeColor
    lblArrGta1(iBar).BackColor = SetBackColor
    lblArrRegn(iBar).ForeColor = SetForeColor
    lblArrRegn(iBar).BackColor = SetBackColor
    lblArrAct3(iBar).ForeColor = SetForeColor
    lblArrAct3(iBar).BackColor = SetBackColor
End Sub


Private Sub ClearDepFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    lblDepFtyp(iBar).Tag = FldValue
    lblDepFtyp(iBar).Caption = FldValue
    lblDepFlno(iBar).Tag = FldValue
    lblDepFlno(iBar).Caption = FldValue
    lblDepFlti(iBar).Tag = FldValue
    lblDepFlti(iBar).Caption = FldValue
    lblDepDes3(iBar).Tag = FldValue
    lblDepDes3(iBar).Caption = FldValue
    lblDepVia3(iBar).Tag = FldValue
    lblDepVia3(iBar).Caption = FldValue
    lblDepStod(iBar).Tag = FldValue
    lblDepStod(iBar).Caption = FldValue
    lblDepEtdi(iBar).Tag = FldValue
    lblDepEtdi(iBar).Caption = FldValue
    lblDepOfbl(iBar).Tag = FldValue
    lblDepOfbl(iBar).Caption = FldValue
    lblDepPstd(iBar).Tag = FldValue
    lblDepPstd(iBar).Caption = FldValue
    lblDepGtd1(iBar).Tag = FldValue
    lblDepGtd1(iBar).Caption = FldValue
    lblDepRegn(iBar).Tag = FldValue
    lblDepRegn(iBar).Caption = FldValue
    lblDepAct3(iBar).Tag = FldValue
    lblDepAct3(iBar).Caption = FldValue
    picDep1(iBar).Tag = FldValue
    picDep2(iBar).Tag = FldValue
    picDep3(iBar).Tag = FldValue
    picDep4(iBar).Tag = FldValue
End Sub
Private Sub SetDepPanelColors(iBar As Integer, SetForeColor As Long, SetBackColor As Long)
    lblDepFtyp(iBar).ForeColor = SetForeColor
    lblDepFtyp(iBar).BackColor = SetBackColor
    lblDepFlno(iBar).ForeColor = SetForeColor
    lblDepFlno(iBar).BackColor = SetBackColor
    lblDepFlti(iBar).ForeColor = SetForeColor
    lblDepFlti(iBar).BackColor = SetBackColor
    lblDepDes3(iBar).ForeColor = SetForeColor
    lblDepDes3(iBar).BackColor = SetBackColor
    lblDepVia3(iBar).ForeColor = SetForeColor
    lblDepVia3(iBar).BackColor = SetBackColor
    lblDepStod(iBar).ForeColor = SetForeColor
    lblDepStod(iBar).BackColor = SetBackColor
    lblDepEtdi(iBar).ForeColor = SetForeColor
    lblDepEtdi(iBar).BackColor = SetBackColor
    lblDepOfbl(iBar).ForeColor = SetForeColor
    lblDepOfbl(iBar).BackColor = SetBackColor
    lblDepPstd(iBar).ForeColor = SetForeColor
    lblDepPstd(iBar).BackColor = SetBackColor
    lblDepGtd1(iBar).ForeColor = SetForeColor
    lblDepGtd1(iBar).BackColor = SetBackColor
    lblDepRegn(iBar).ForeColor = SetForeColor
    lblDepRegn(iBar).BackColor = SetBackColor
    lblDepAct3(iBar).ForeColor = SetForeColor
    lblDepAct3(iBar).BackColor = SetBackColor
End Sub


Private Sub InitRightPanel()
    Dim CfgVers As String
    Dim CfgPics As String
    Dim CfgCapt As String
    Dim CfgText As String
    Dim tmpVers As String
    Dim tmpPics As String
    Dim tmpCapt As String
    Dim tmpText As String
    Dim LoopIdx As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TxtIdx As Integer
    Dim LinIdx As Integer
    Dim ArrIdx As Integer
    Dim DepIdx As Integer
    Dim IdxCnt As Integer
    Dim IdxMax As Integer
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_TEXT", "")
 
    TxtIdx = 0
    LinIdx = 0
    NewLeft = 60
    NewTop = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height + 90
    CreateLinLine LinIdx, NewTop, vbWhite
    NewTop = NewTop + 30
    CreateChkTitle TxtIdx, "Inbound", "AFSA", NewTop
    lblTxtBackLight(TxtIdx).BackColor = LightYellow
    NewTop = NewTop + lblTxtText(0).Height + 30
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    LinIdx = LinIdx + 1
    NewTop = NewTop + 15
    CreateLinLine LinIdx, NewTop, vbWhite
    
    NewTop = NewTop + 45
    IdxCnt = Len(CfgVers)
    IdxMax = IdxCnt - 1
    For ArrIdx = 0 To IdxMax
        LoopIdx = CLng(ArrIdx)
        tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
        tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
        tmpText = GetRealItem(CfgText, LoopIdx, ",")
        tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
        CreateArrPicPanel ArrIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + ArrPicPanel(0).Height + 30
    Next
    If IdxCnt > 4 Then
        IdxMax = IdxMax + 1
        tmpVers = CfgVers
        tmpCapt = "?"
        tmpText = "Others"
        tmpPics = ""
        CreateArrPicPanel IdxMax, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + ArrPicPanel(0).Height + 30
    End If
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_TEXT", "")
    
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    NewTop = NewTop + 120
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbWhite
    NewTop = NewTop + 30
    TxtIdx = TxtIdx + 1
    CreateChkTitle TxtIdx, "Outbound", "AFSD", NewTop
    lblTxtBackLight(TxtIdx).BackColor = LightGreen
    NewTop = NewTop + lblTxtText(0).Height + 30
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    LinIdx = LinIdx + 1
    NewTop = NewTop + 15
    CreateLinLine LinIdx, NewTop, vbWhite
    
    NewTop = NewTop + 45
    IdxCnt = Len(CfgVers)
    IdxMax = IdxCnt - 1
    For DepIdx = 0 To IdxMax
        LoopIdx = CLng(DepIdx)
        tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
        tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
        tmpText = GetRealItem(CfgText, LoopIdx, ",")
        tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
        CreateDepPicPanel DepIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + DepPicPanel(0).Height + 30
    Next
    If IdxCnt > 4 Then
        IdxMax = IdxMax + 1
        tmpVers = CfgVers
        tmpCapt = "?"
        tmpText = "Others"
        tmpPics = ""
        CreateDepPicPanel IdxMax, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + DepPicPanel(0).Height + 30
    End If
    
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_TEXT", "")
    If CfgVers <> "" Then
        NewTop = NewTop + 120
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbWhite
        NewTop = NewTop + 30
        TxtIdx = TxtIdx + 1
        CreateChkTitle TxtIdx, "Common", "COMM", NewTop
        lblTxtBackLight(TxtIdx).BackColor = vbBlue
        lblTxtText(TxtIdx).ForeColor = vbWhite
        lblTxtShadow(TxtIdx).ForeColor = vbBlack
        
        NewTop = NewTop + lblTxtText(0).Height + 30
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbBlack
        LinIdx = LinIdx + 1
        NewTop = NewTop + 15
        CreateLinLine LinIdx, NewTop, vbWhite
        
        NewTop = NewTop + 45
        IdxCnt = Len(CfgVers)
        IdxMax = IdxCnt - 1
        For ArrIdx = 0 To IdxMax
            LoopIdx = CLng(ArrIdx)
            tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
            tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
            tmpText = GetRealItem(CfgText, LoopIdx, ",")
            tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
            CreateComPicPanel ArrIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
            NewTop = NewTop + ComPicPanel(0).Height + 30
        Next
        
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbBlack
    End If
    
End Sub
Private Sub CreateLinLine(LinIdx, UseTop As Long, UseColor As Long)
    If LinIdx > linLine.UBound Then
        Load linLine(LinIdx)
        Set linLine(LinIdx).Container = RightPanel
    End If
    linLine(LinIdx).Y1 = UseTop
    linLine(LinIdx).Y2 = UseTop
    linLine(LinIdx).BorderColor = UseColor
    linLine(LinIdx).Visible = True
End Sub
Private Sub CreateArrPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > ArrPicPanel.UBound Then
        Load ArrPicPanel(Index)
        Load cmdArrPicMode(Index)
        Load lblArrPicText(Index)
        Load lblArrPicBack(Index)
        Load lblArrPicType(Index)
        Load lblArrPicCount(Index)
        Load picArrPic(Index)
        Set ArrPicPanel(Index).Container = RightPanel
        Set cmdArrPicMode(Index).Container = ArrPicPanel(Index)
        Set lblArrPicText(Index).Container = ArrPicPanel(Index)
        Set lblArrPicBack(Index).Container = ArrPicPanel(Index)
        Set lblArrPicType(Index).Container = ArrPicPanel(Index)
        Set lblArrPicCount(Index).Container = ArrPicPanel(Index)
        Set picArrPic(Index).Container = ArrPicPanel(Index)
    End If
    NewTop = TopPos
    ArrPicPanel(Index).Top = NewTop
    lblArrPicText(Index).BackColor = LightGrey
    lblArrPicType(Index).BackColor = LightGrey
    lblArrPicCount(Index).BackColor = LightGrey
    lblArrPicBack(Index).BackColor = LightestYellow
    lblArrPicText(Index).Caption = ""
    lblArrPicType(Index).Caption = ""
    lblArrPicCount(Index).Caption = ""
    
    lblArrPicText(Index).Caption = VersText
    lblArrPicType(Index).Caption = VersCapt
    lblArrPicType(Index).Tag = VersCode
    picArrPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picArrPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdArrPicMode(Index).Visible = True
    lblArrPicText(Index).Visible = True
    lblArrPicBack(Index).Visible = True
    lblArrPicType(Index).Visible = True
    lblArrPicCount(Index).Visible = True
    picArrPic(Index).Visible = True
    picArrPic(Index).ZOrder
    ArrPicPanel(Index).Visible = True
    
End Sub

Private Sub CreateDepPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > DepPicPanel.UBound Then
        Load DepPicPanel(Index)
        Load cmdDepPicMode(Index)
        Load lblDepPicText(Index)
        Load lblDepPicBack(Index)
        Load lblDepPicType(Index)
        Load lblDepPicCount(Index)
        Load picDepPic(Index)
        Set DepPicPanel(Index).Container = RightPanel
        Set cmdDepPicMode(Index).Container = DepPicPanel(Index)
        Set lblDepPicText(Index).Container = DepPicPanel(Index)
        Set lblDepPicBack(Index).Container = DepPicPanel(Index)
        Set lblDepPicType(Index).Container = DepPicPanel(Index)
        Set lblDepPicCount(Index).Container = DepPicPanel(Index)
        Set picDepPic(Index).Container = DepPicPanel(Index)
    End If
    NewTop = TopPos
    DepPicPanel(Index).Top = NewTop
    lblDepPicText(Index).BackColor = LightGrey
    lblDepPicType(Index).BackColor = LightGrey
    lblDepPicCount(Index).BackColor = LightGrey
    lblDepPicBack(Index).BackColor = LightestGreen
    lblDepPicText(Index).Caption = ""
    lblDepPicType(Index).Caption = ""
    lblDepPicCount(Index).Caption = ""
    
    lblDepPicText(Index).Caption = VersText
    lblDepPicType(Index).Caption = VersCapt
    lblDepPicType(Index).Tag = VersCode
    picDepPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picDepPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdDepPicMode(Index).Visible = True
    lblDepPicText(Index).Visible = True
    lblDepPicBack(Index).Visible = True
    lblDepPicType(Index).Visible = True
    lblDepPicCount(Index).Visible = True
    picDepPic(Index).Visible = True
    picDepPic(Index).ZOrder
    DepPicPanel(Index).Visible = True
    
End Sub

Private Sub CreateComPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > ComPicPanel.UBound Then
        Load ComPicPanel(Index)
        Load cmdComPicMode(Index)
        Load lblComPicText(Index)
        Load lblComPicBack(Index)
        Load lblComPicType(Index)
        Load lblComPicCount(Index)
        Load picComPic(Index)
        Set ComPicPanel(Index).Container = RightPanel
        Set cmdComPicMode(Index).Container = ComPicPanel(Index)
        Set lblComPicText(Index).Container = ComPicPanel(Index)
        Set lblComPicBack(Index).Container = ComPicPanel(Index)
        Set lblComPicType(Index).Container = ComPicPanel(Index)
        Set lblComPicCount(Index).Container = ComPicPanel(Index)
        Set picComPic(Index).Container = ComPicPanel(Index)
    End If
    NewTop = TopPos
    ComPicPanel(Index).Top = NewTop
    cmdComPicMode(Index).BackColor = LightGrey
    lblComPicText(Index).BackColor = LightGrey
    lblComPicType(Index).BackColor = LightGrey
    lblComPicCount(Index).BackColor = LightGrey
    lblComPicBack(Index).BackColor = vbWhite
    lblComPicText(Index).Caption = ""
    lblComPicType(Index).Caption = ""
    lblComPicCount(Index).Caption = ""
    
    lblComPicText(Index).Caption = VersText
    lblComPicType(Index).Caption = VersCapt
    lblComPicType(Index).Tag = VersCode
    picComPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picComPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdComPicMode(Index).Visible = True
    lblComPicText(Index).Visible = True
    lblComPicBack(Index).Visible = True
    lblComPicType(Index).Visible = True
    lblComPicCount(Index).Visible = True
    picComPic(Index).Visible = True
    picComPic(Index).ZOrder
    ComPicPanel(Index).Visible = True
    
End Sub


Private Sub CreateChkTitle(TxtIdx As Integer, UseText As String, UseTag As String, UseTop As Long)
    Dim NewLeft As Long
    If TxtIdx > lblTxtText.UBound Then
        Load lblTxtBackLight(TxtIdx)
        Set lblTxtBackLight(TxtIdx).Container = RightPanel
        Load chkTitle(TxtIdx)
        Set chkTitle(TxtIdx).Container = RightPanel
        Load lblTxtText(TxtIdx)
        Set lblTxtText(TxtIdx).Container = RightPanel
        Load lblTxtShadow(TxtIdx)
        Set lblTxtShadow(TxtIdx).Container = RightPanel
    End If
    lblTxtBackLight(TxtIdx).Top = UseTop
    lblTxtBackLight(TxtIdx).Visible = True
    chkTitle(TxtIdx).Top = UseTop + 15
    chkTitle(TxtIdx).Left = 15
    chkTitle(TxtIdx).Visible = True
    NewLeft = chkTitle(TxtIdx).Width + 45
    Set3DText UseText, lblTxtText(TxtIdx), lblTxtShadow(TxtIdx), NewLeft, UseTop + 15, True
    chkTitle(TxtIdx).Tag = UseTag
End Sub
Private Sub Set3DText(UseText As String, TextObj As Label, ShadObj As Label, UseLeft As Long, UseTop As Long, SetDark As Boolean)
    TextObj.Visible = False
    ShadObj.Visible = False
    TextObj.Caption = UseText
    ShadObj.Caption = UseText
    TextObj.Top = UseTop
    ShadObj.Top = UseTop + 15
    TextObj.Left = UseLeft
    ShadObj.Left = UseLeft + 15
    TextObj.Visible = True
    ShadObj.Visible = True
    ShadObj.Refresh
    ShadObj.ZOrder
    TextObj.ZOrder
    TextObj.Refresh
End Sub

Private Sub GetChartConfig()
    Dim tmpCfg As String
    Dim tmpData As String
    On Error Resume Next
    Me.Caption = GetIniEntry(myIniFullName, "GOCC_CHART", "", "GOCC_CHART_TITLE", Me.Caption)
    tmpCfg = GetIniEntry(myIniFullName, "GOCC_CHART", "", "GOCC_CHART_ICON", "")
    If tmpCfg <> "" Then
        If InStr(tmpCfg, "\") = 0 Then tmpCfg = UFIS_SYSTEM & "\" & tmpCfg
        picApplIcon.Picture = LoadPicture(tmpCfg)
    End If
    Me.Icon = picApplIcon.Picture
    
End Sub
