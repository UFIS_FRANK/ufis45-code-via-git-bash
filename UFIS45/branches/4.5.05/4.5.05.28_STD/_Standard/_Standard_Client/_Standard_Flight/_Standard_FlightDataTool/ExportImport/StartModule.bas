Attribute VB_Name = "StartModule"
Option Explicit

Sub Main()
    Dim LoginIsOk As Boolean
    Dim tmpCfgCode As String
    Dim CmdLineCode As String
    Dim CmdLineData As String
    Dim i As Integer
    
    CurScreenWidth = Screen.Width
    MaxScreenWidth = CLng(1280 * 15) * 4
    If CurScreenWidth < MaxScreenWidth Then MaxScreenWidth = CurScreenWidth
    
    CreateGanttOnDemandOnly = True
    
    CurCedaOcx = 0
    CurCedaIdx = 0
    CmdLineText = Command
    
    UtcTimeDiff = 0
    UserMustLogIn = True
    
    ApplMainCode = "FDS"
    ApplIsFdsFile = False
    ApplIsFdsLoad = False
    ApplIsFdsMain = False
    
    ApplFuncCode = "MAIN"
    ApplIsFdsMain = True
    CurCedaIdx = 0
    
    'ApplFuncCode = "FILE"
    'ApplIsFdsFile = True
    
    'ApplFuncCode = "LOAD"
    'ApplIsFdsLoad = True
    'CurCedaIdx = 2
    
    UfisProjectName = "UNKNOWN"
    UfisServerFunction = "Unknown"
    LoginUserName = "DATATOOL"
    ApplicationIsReadyForUse = False
    ReadOnlyUser = False
    BookMarkButtonIdx = -1
    MyOwnButtonFace = vbButtonFace
    
    'MyOwnButtonFace = LightGrey
    'MyOwnButtonFace = LightRed
    'MyOwnButtonFace = RGB(235, 235, 235)
    
    MyDateInputFormat = "dd.mm.yyyy"
    MyTimeInputFormat = "hh:mm"
    Load BcSpooler
    Load UfisServer
    For i = 0 To UfisServer.aCeda.UBound
        UfisServer.aCeda(i).StayConnected = True
    Next
    'UfisServer.aCeda(CurCedaOcx).StayConnected = True
    'UfisServer.Show
    
    If CmdLineText <> "" Then
        If InStr(CmdLineText, "/DEVLP") > 0 Then
            HiddenData.Show
            HiddenData.Refresh
            MyConfig.Show
            CmdLineText = Replace(CmdLineText, "/DEVLP", "", 1, -1, vbBinaryCompare)
        End If
    End If
    
    If CmdLineText <> "" Then
        GetKeyItem CmdLineCode, CmdLineText, "[FC]", "["
        If CmdLineCode <> "" Then
            ApplIsFdsFile = False
            ApplIsFdsLoad = False
            ApplIsFdsMain = False
            Select Case CmdLineCode
                Case "MAIN"
                    ApplFuncCode = "MAIN"
                    ApplIsFdsMain = True
                    CmdLineText = ""
                Case "LOAD"
                    ApplFuncCode = "LOAD"
                    ApplIsFdsLoad = True
                    CmdLineText = ""
                Case "FILE"
                    ApplFuncCode = "FILE"
                    ApplIsFdsFile = True
                    CmdLineText = ""
                Case Else
                    ApplFuncCode = "MAIN"
                    ApplIsFdsMain = True
                    CmdLineText = ""
            End Select
        End If
    End If
    
    ApplModuleKey = ApplMainCode & "-" & ApplFuncCode
    
    LoadCfgFromUseFile
    
    If Not ApplGotCfgFromFile Then
        LoginIsOk = DetermineIniFile
        If Not LoginIsOk Then
            MsgBox "Sorry. Couldn't find the INI file!" & vbNewLine & myIniFullName
            End
        End If
    End If
    Load MyConfig
    Load HiddenData
    
    UfisProjectName = GetIniEntry(myIniFullName, "MAIN", "", "SYSTEM_TITLE", UfisProjectName)
    UfisServer.CreateListOfFdtServers
    UfisServerFunction = UfisServer.FuncName.Text
    
    StatusPicPath = GetIniEntry(myIniFullName, "MAIN", "", "APPL_STATUS_PICS", UFIS_SYSTEM)
    If Left(StatusPicPath, 1) = "?" Then Mid(StatusPicPath, 1) = Left(UFIS_SYSTEM, 1)
    MyIntroPicture = "Ufis" & App.EXEName & "_Intro_" & ApplFuncCode
    MyIntroPicture = GetIniEntry(myIniFullName, "MAIN", "", "APPL_INTRO_IMAGE", MyIntroPicture)
    
    'UfisServer.Show
    'UfisServer.TwsCode.Text = ".NBC."
    UfisServer.TwsCode.Text = ".."
    
    If UfisServer.HostName <> "LOCAL" Then
        'LoginIsOk = LoginProcedure
    Else
        LoginIsOk = True
    End If
    LoginIsOk = True

    MainDialog.Show
    ApplicationIsStarted = True
End Sub

Public Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    On Error Resume Next
    'Set UfisServer.LoginCall.UfisComCtrl = UfisServer.aCeda(CurCedaOcx)
'    UfisServer.LoginCall.ApplicationName = "SnapShotTool"
    'LoginAnsw = UfisServer.LoginCall.DoLoginSilentMode("UFIS$ADMIN", "Passwort")
    'tmpRegStrg = ""
    'tmpRegStrg = tmpRegStrg & "SnapShotTool" & ","
    '                  FKTTAB:  SUBD   ,  FUNC  ,    FUAL              ,TYPE,STAT
    'tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    'UfisServer.LoginCall.RegisterApplicationString = tmpRegStrg
    'UfisServer.LoginCall.ShowLoginDialog
    'LoginAnsw = UfisServer.LoginCall.GetPrivileges("InitModu")
    If LoginAnsw <> "" Then LoginProcedure = True Else LoginProcedure = False
    'MsgBox UfisServer.LoginCall.BuildDate
    LoginProcedure = True

'    UfisServer.LoginCall.ApplicationName = "SnapShotTool"
'    UfisServer.LoginCall.ShowLoginDialog


End Function

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    If (SetValue = False) Or ((SetValue = True) And (MainDialog.OnTop.Value = 1)) Then
        If FormIsVisible("EditMask") Then SetFormOnTop EditMask, SetValue
        If FormIsVisible("ReadMask") Then SetFormOnTop ReadMask, SetValue
        If FormIsVisible("FilterDialog") Then SetFormOnTop FilterDialog, SetValue
        If FormIsVisible("MyBookMarks") Then SetFormOnTop MyBookMarks, SetValue
        'If FormIsVisible("StatusChart") Then SetFormOnTop StatusChart, SetValue
        SetFormOnTop MainDialog, SetValue
    End If
End Sub

Public Sub HandleBroadCastSpooler(BcLineCount As Long, BcEvent As String)
    Dim BcCnt As Long
    UfisBroadCastIsAvailable = True
    BcCnt = BcLineCount
    MainDialog.AdjustBroadCastPanel BcLineCount, BcEvent, 0
End Sub
Public Sub HandleBroadcast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, tws As String, twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim tmpCOD As String
    Dim tmpWKS As String
    Dim tmpCmd As String
    Dim tmpUrno As String
    UfisBroadCastIsAvailable = True
    If ApplFuncCode <> "DACO" Then
        MainDialog.HandleBroadcast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, tws, twe, CedaSqlKey, Fields, Data, BcNum
    ElseIf (ApplFuncCode = "DACO") And InStr("AFTTAB,DLYTAB,SRLTAB,FMLTAB", ObjName) > 0 Then
        MainDialog.EvaluateBc CedaCmd, ObjName, CedaSqlKey, Fields, Data
    ElseIf CedaCmd = "CLO" Then
        MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
        End
    'ElseIf CedaCmd = "TIME" Then
    '    MainDialog.AdjustBroadCastPanel -1, tws, 1
    End If
End Sub
Public Sub HandleEventsFromCdrhdl(Index As Integer, Cmd As String, Msg As String, Data As String)
    Dim tmpText As String
    tmpText = Cmd
    If ApplWaitsForNetwork = True Then
        Select Case Cmd
            Case "ERR"
                tmpText = GetItem(Msg, 1, ":") & ":" & vbLf
                tmpText = tmpText & GetItem(Msg, 2, ":")
                MainDialog.CheckNetwork 1, True, -1, -1, -1, tmpText
            Case "CON"
                MainDialog.CheckNetwork 1, False, -1, -1, -1, tmpText
            Case Else
        End Select
    Else
    End If
End Sub
Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub HandleDisplayChanged()
    
End Sub

Public Sub HandleSysColorsChanged()
    
End Sub

Public Sub HandleTimeChanged()
    
End Sub

Public Sub ShutDownApplication(AskUser As Boolean)
    Dim retval As Integer
    retval = 1
    If AskUser Then
        retval = MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer)
    End If
    If retval = 1 Then
        ShutDownRequested = True
        If AskUser Then MainDialog.RecoverFileWrite -1, "Exit", True
        UfisServer.aCeda(CurCedaOcx).CleanupCom
        On Error Resume Next
        CloseHtmlHelp
        If MainDialog.ExcelIsOpen Then
            MainDialog.MsWkBook.Saved = True
            MainDialog.MsWkBook.Close
            MainDialog.MsExcel.Quit
        End If
        MainDialog.MsWord.Quit wdDoNotSaveChanges
        End
    End If
    'If not, we come back
End Sub

Public Sub RefreshMain()
    'do nothing
End Sub

Public Function TranslateItemList(CurItemList As String, CheckList As String, AllItems As Boolean) As String
    Dim Result As String
    Dim MainItem As String
    Dim SubItem As String
    Dim MainItemNo As Integer
    Dim SubItemNo As Integer
    Dim CurItemNo As Integer
    Dim CurItemTxt As String
    Result = CurItemList
    MainItemNo = 1
    MainItem = Trim(GetItem(CurItemList, MainItemNo, ","))
    While MainItem <> ""
        SubItemNo = 1
        SubItem = Trim(GetItem(MainItem, SubItemNo, "+"))
        While SubItem <> ""
            If Len(SubItem) = 4 Then
                CurItemNo = GetItemNo(CheckList, SubItem)
                If CurItemNo > 0 Then
                    CurItemTxt = Trim(Str(CurItemNo))
                    Result = Replace(Result, SubItem, CurItemTxt, 1, -1, vbBinaryCompare)
                Else
                    If AllItems Then
                        Result = Replace(Result, SubItem, "-1", 1, -1, vbBinaryCompare)
                    End If
                End If
            End If
            SubItemNo = SubItemNo + 1
            SubItem = Trim(GetItem(MainItem, SubItemNo, "+"))
        Wend
        MainItemNo = MainItemNo + 1
        MainItem = Trim(GetItem(CurItemList, MainItemNo, ","))
    Wend
    TranslateItemList = Result
End Function

Public Sub CreateTypeList(ResultList As String, CheckList As String, FieldType As String, ItemOffset As Integer)
'    Dim CurFldNam As String
'    Dim LineList As String
'    Dim LineNbr As String
'    Dim CurLine As Long
'    Dim LineNo As Long
'    ResultList = ""
'    LineList = MyConfig.CfgTab(0).GetLinesByColumnValue(1, FieldType, 0)
'    CurLine = 0
'    LineNbr = GetRealItem(LineList, CurLine, ",")
'    While LineNbr <> ""
'        LineNo = Val(LineNbr)
'        CurFldNam = MyConfig.CfgTab(0).GetColumnValue(LineNo, 0)
'        AddItemToList ResultList, CheckList, CurFldNam, ItemOffset
'        CurLine = CurLine + 1
'        LineNbr = GetRealItem(LineList, CurLine, ",")
'    Wend
End Sub

Public Sub AddItemToList(ResultList As String, ItemList As String, ItemName As String, ItemOffset As Integer)
    Dim ItemNo As Integer
    ItemNo = GetItemNo(ItemList, ItemName)
    If ItemNo > 0 Then
        If ResultList <> "" Then ResultList = ResultList & ","
        ResultList = ResultList & Trim(Str(ItemNo + ItemOffset))
    End If
End Sub

Public Function TranslateRecordData(CurRecData As String, InFieldList As String, OutFieldList As String) As String
    Dim NewRecData As String
    Dim CurItemNo As Integer
    Dim CurFldNam As String
    Dim CurFldNbr As Integer
    'If InStr(InFieldList, "ADID") = 0 Then
        NewRecData = ""
        NewRecData = NewRecData & GetItem(CurRecData, 1, ",") & ","
        NewRecData = NewRecData & GetItem(CurRecData, 2, ",") & ","
        NewRecData = NewRecData & GetItem(CurRecData, 3, ",") & ","
        CurFldNbr = 1
        CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
        While CurFldNam <> ""
            CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
            If CurItemNo > 3 Then
                NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
            Else
                NewRecData = NewRecData & ","
            End If
            CurFldNbr = CurFldNbr + 1
            CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
        Wend
    'Else
    '    NewRecData = TranslateRecordFromFlight(CurRecData, InFieldList, OutFieldList)
    'End If
    TranslateRecordData = NewRecData
End Function
Public Function TranslateRecordFromFlight(CurRecData As String, InFieldList As String, OutFieldList As String) As String
    Dim NewRecData As String
    Dim CurItemNo As Integer
    Dim CurFldNam As String
    Dim CurFldNbr As Integer
    Dim CurAdid As String
    Dim OutAdidFields As String
    
    NewRecData = ""
    NewRecData = NewRecData & GetItem(CurRecData, 1, ",") & ","
    NewRecData = NewRecData & GetItem(CurRecData, 2, ",") & ","
    NewRecData = NewRecData & GetItem(CurRecData, 3, ",") & ","
    
    CurItemNo = GetItemNo(InFieldList, "ADID") + 3
    CurAdid = GetItem(CurRecData, CurItemNo, ",")
    'HardCoded to Old DataModel !!
    '"FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,ACT3,NOSE,ORG3,VSA3,STOA,STOD,DAOF,VSD3,DES3,NATA,NATD,FREW,REGN,FTYP"
    '"FLCB,FLTB,....,....,SKED,SKED,....,....,....,....,....,SKED,....,....,....,....,....,....,....,....,...."
    '"....,....,FLCB,FLTB,SKED,SKED,....,....,....,....,....,....,SKED,....,....,....,....,....,....,....,...."
    Select Case CurAdid
        Case "A"
            OutAdidFields = "FLCB,FLTB,....,....,SKED,SKED,....,....,....,....,....,SKED,....,....,....,....,TTYP,....,....,....,...."
        Case "D"
            OutAdidFields = "....,....,FLCB,FLTB,SKED,SKED,....,....,....,....,....,....,SKED,....,....,....,....,TTYP,....,....,...."
        Case Else
    End Select
    
    CurFldNbr = 1
    CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
    While CurFldNam <> ""
        CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
        If CurItemNo > 3 Then
            NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
        Else
            CurFldNam = GetItem(OutAdidFields, CurFldNbr, ",")
            CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
            If CurItemNo > 3 Then
                NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
            Else
                NewRecData = NewRecData & ","
            End If
        End If
        CurFldNbr = CurFldNbr + 1
        CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
    Wend
    TranslateRecordFromFlight = NewRecData
End Function

Public Function ResolveDataLists(LookField As String, UseDataField As String, ListofData As String, ListOfFields As String) As String
    Dim Result As String
    Result = GetFieldValue(UseDataField, ListofData, ListOfFields)
    Result = ExtractFromItemList(LookField, Result)
    ResolveDataLists = Result
End Function
Public Function ExtractFromItemList(LookField As String, DataList As String) As String
    Dim Result As String
    Dim ItemTxt As String
    Dim ItemNbr As Integer
    Dim ItemCnt As Integer
    ItemCnt = ItemCount(DataList, "|")
    Select Case LookField
        Case "ORG3"
            Result = GetItem(DataList, 1, "|")
            Result = GetItem(Result, 1, ":")
            If Result = HomeAirport Then
                Result = GetItem(DataList, 2, "|")
                Result = GetItem(Result, 1, ":")
            End If
        Case "DES3"
            ItemNbr = ItemCount(DataList, "|")
            If ItemNbr > 0 Then
                Result = GetItem(DataList, ItemNbr, "|")
                Result = GetItem(Result, 1, ":")
            End If
            If Result = HomeAirport Then
                Result = GetItem(DataList, ItemNbr - 1, "|")
                Result = GetItem(Result, 1, ":")
            End If
        Case "VSA3"
            ItemTxt = GetItem(DataList, 1, "|")
            ItemTxt = GetItem(ItemTxt, 1, ":")
            If ItemTxt = HomeAirport Then ItemNbr = 1 Else ItemNbr = 0
            ItemTxt = "START"
            While ItemTxt <> ""
                ItemNbr = ItemNbr + 1
                ItemTxt = Trim(GetItem(DataList, ItemNbr, "|"))
                ItemTxt = GetItem(ItemTxt, 1, ":")
                If ItemTxt = HomeAirport Then
                    If ItemNbr > 2 Then
                        Result = GetItem(DataList, ItemNbr - 1, "|")
                        Result = GetItem(Result, 1, ":")
                    End If
                    ItemTxt = ""
                End If
            Wend
        Case "VSD3"
            ItemNbr = 0
            ItemTxt = "START"
            While ItemTxt <> ""
                ItemNbr = ItemNbr + 1
                ItemTxt = Trim(GetItem(DataList, ItemNbr, "|"))
                ItemTxt = GetItem(ItemTxt, 1, ":")
                If ItemTxt = HomeAirport Then
                    If (ItemCnt - ItemNbr) > 1 Then
                        Result = GetItem(DataList, ItemNbr + 1, "|")
                        Result = GetItem(Result, 1, ":")
                    End If
                    ItemTxt = ""
                End If
            Wend
        Case Else
    End Select
    ExtractFromItemList = Result
End Function
Public Function ShiftFrqd(UseFrqd As String, ShiftValue As Integer) As String
    Dim tmpFrqd As String
    Dim iVal As Integer
    Dim i As Integer
    tmpFrqd = Trim(UseFrqd)
    If tmpFrqd <> "" Then
        tmpFrqd = "......."
        For i = 1 To 7
            If Mid(UseFrqd, i, 1) <> "." Then
                iVal = i + ShiftValue
                If iVal > 7 Then iVal = iVal - 7
                If iVal < 1 Then iVal = 7 - iVal
                Mid(tmpFrqd, iVal, 1) = Trim(Str(iVal))
            End If
        Next
    End If
    ShiftFrqd = tmpFrqd
End Function

Public Function FormatFrequency(FreqDays As String, FormatType As String, IsValid As Boolean) As String
    Dim Result As String
    Dim tmpPos As Integer
    Dim tmpChr As String
    Dim tmpVal As String
    Dim FreqType As Integer
    Dim i As Integer
    Dim l As Integer
    IsValid = True
    Result = Trim(FreqDays)
    If Result <> "" Then
        FreqType = -1
        Select Case FormatType
            Case "123"
                FreqType = 0
            Case "111"
                FreqType = 1
            Case Else
            If (InStr(FreqDays, "0") > 0) Or (InStr(FreqDays, "1111") > 0) Then
                'Might be more analysis needed
                FreqType = 1
            Else
                FreqType = 0
            End If
        End Select
        Result = "......."
        Select Case FreqType
            Case 0  'Type 1234567
                l = Len(FreqDays)
                For i = 1 To l
                    tmpChr = Mid(FreqDays, i, 1)
                    tmpPos = Val(tmpChr)
                    If (tmpPos > 0) And (tmpPos < 8) Then
                        If Mid(Result, tmpPos, 1) = "." Then
                            Mid(Result, tmpPos, 1) = tmpChr
                        Else
                            IsValid = False
                        End If
                    Else
                        If InStr(". -", tmpChr) = 0 Then IsValid = False
                    End If
                    If Not IsValid Then Exit For
                Next
            Case 1  'Type 0011001
                l = Len(FreqDays)
                If l > 7 Then l = 7
                For i = 1 To l
                    tmpChr = Mid(FreqDays, i, 1)
                    If tmpChr = "1" Then
                        Mid(Result, i, 1) = Trim(Str(i))
                    End If
                Next
            Case Else
                Result = FreqDays
        End Select
        If Not IsValid Then Result = FreqDays
    End If
    FormatFrequency = Result
End Function


Public Function ApcLocalToUtc(AptCode As String, LocValue As String) As String
    Dim NewValue As String
    Dim CurLen As Integer
    Dim UtcOffset As Double
    Dim TimeValue
    UtcOffset = UtcTimeDiff
    CurLen = Len(LocValue)
    If CurLen = 8 Then
        'Date
        TimeValue = CedaDateToVb(LocValue)
        TimeValue = DateAdd("n", -(UtcOffset), TimeValue)
        NewValue = Format(TimeValue, "yyyymmdd")
    ElseIf (CurLen = 12) Or (CurLen = 14) Then
        'Date and Time
        TimeValue = CedaFullDateToVb(LocValue)
        TimeValue = DateAdd("n", -(UtcOffset), TimeValue)
        NewValue = Format(TimeValue, "yyyymmddhhmm")
    Else
        NewValue = LocValue
    End If
    ApcLocalToUtc = NewValue
End Function

Public Function ApcUtcToLocal(AptCode As String, UtcValue As String, KeepSeconds As Boolean) As String
    Dim NewValue As String
    Dim CurLen As Integer
    Dim UtcOffset As Double
    Dim TimeValue
    UtcOffset = UtcTimeDiff
    CurLen = Len(UtcValue)
    If CurLen = 8 Then
        'Date
        TimeValue = CedaDateToVb(UtcValue)
        TimeValue = DateAdd("n", UtcOffset, TimeValue)
        NewValue = Format(TimeValue, "yyyymmdd")
    ElseIf (CurLen = 12) Or (CurLen >= 14) Then
        'Date and Time
        TimeValue = CedaFullDateToVb(UtcValue)
        TimeValue = DateAdd("n", UtcOffset, TimeValue)
        If KeepSeconds = True Then
            NewValue = Format(TimeValue, "yyyymmddhhmmss")
        Else
            NewValue = Format(TimeValue, "yyyymmddhhmm")
        End If
    Else
        NewValue = UtcValue
    End If
    ApcUtcToLocal = NewValue
End Function

Public Function TurnImpFkeyToAftFkey(CurFkey As String) As String
    TurnImpFkeyToAftFkey = Mid(CurFkey, 4, 5) & Left(CurFkey, 3) & Mid(CurFkey, 9)
End Function

Public Function TurnAftFkey(AftFkey As String) As String
   TurnAftFkey = Mid(AftFkey, 6, 3) + Left(AftFkey, 5) + Mid(AftFkey, 9)
End Function

Private Function DetermineIniFile() As Boolean
    Dim tmpStr As String
    Dim tmpCmdLine As String
    Dim CmdParam As String
    Dim CfgName As String
    Dim UseName As String
    Dim IniFound As Boolean
    Dim UsingDefault As Boolean
    On Error Resume Next
    tmpCmdLine = UCase(CmdLineText)
    IniFound = False
    UsingDefault = False
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    myIniPath = UFIS_SYSTEM
    CmdParam = UCase(GetItem(tmpCmdLine, 1, ","))
    If tmpCmdLine = ApplStartupFile Then
        CmdParam = ""
        ApplStartupFile = ""
        tmpCmdLine = ""
    End If
    If CmdParam <> "" Then
        CfgName = App.EXEName & UfisServer.HOPO & "_" & CmdParam
        myIniFile = CfgName & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
        If UCase(tmpStr) <> UCase(myIniFile) Then
            CfgName = App.EXEName & "_" & CmdParam
            myIniFile = CfgName & ".ini"
            myIniFullName = myIniPath & "\" & myIniFile
        End If
        tmpStr = Dir(myIniFullName)
        If UCase(tmpStr) = UCase(myIniFile) Then IniFound = True
    End If
    
    If IniFound = False Then
        CfgName = App.EXEName & UfisServer.HOPO
        myIniFile = CfgName & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
        If UCase(tmpStr) <> UCase(myIniFile) Then
            CfgName = App.EXEName
            myIniFile = CfgName & ".ini"
            myIniFullName = myIniPath & "\" & myIniFile
        End If
        tmpStr = Dir(myIniFullName)
        If UCase(tmpStr) = UCase(myIniFile) Then IniFound = True
        CmdParam = UCase(GetItem(tmpCmdLine, 1, ","))
        UsingDefault = True
    End If
    
    If (IniFound = True) And (UsingDefault = True) And (CmdParam <> "") Then
        IniFound = False
        tmpStr = "PARAM_" & CmdParam
        tmpStr = GetIniEntry(myIniFullName, "FIPS_CONNECTION", "", tmpStr, "")
        If tmpStr <> "" Then IniFound = True
        If IniFound = False Then
            UseName = GetIniEntry(myIniFullName, "CONFIGURATION", "", CmdParam, "")
            If UseName <> "" Then
                myIniFile = UseName
                myIniFullName = myIniPath & "\" & UseName
                tmpStr = Dir(myIniFullName)
                If UCase(tmpStr) = UCase(UseName) Then IniFound = True
            End If
        End If
        If IniFound = False Then
            CfgName = App.EXEName & "_" & CmdParam
            myIniFile = CfgName & ".ini"
            myIniFullName = myIniPath & "\" & myIniFile
        End If
    End If
    
    DetermineIniFile = IniFound
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function

Public Function GetDrawColor(DrawHeight As Long, MaxHeight As Long, MyColor As Integer) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = -1
    If (MyColor >= 0) And (DrawHeight > 1) Then
        intFormHeight = DrawHeight
        iColor = MyColor
        sngBlueCur = intBLUESTART

        sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
        For intY = 0 To intFormHeight Step intBANDHEIGHT
            If iColor And intBlue Then iBlue = sngBlueCur
            If iColor And intRed Then iRed = sngBlueCur
            If iColor And intGreen Then iGreen = sngBlueCur
            If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
            If iColor And intBackRed Then iRed = 255 - sngBlueCur
            If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
            sngBlueCur = sngBlueCur + sngBlueStep
            If intY >= MaxHeight Then
                ReturnColor = RGB(iRed, iGreen, iBlue)
                Exit For
            End If
        Next intY
    End If
    If ReturnColor < 0 Then ReturnColor = vbWhite
    GetDrawColor = ReturnColor
End Function

Public Sub PrintBackGroundText(MyPanel As PictureBox, CurFontSize As Integer, CurX As Long, CurY As Long, myText As String, EndX As Long, EndY As Long)
    Dim UseSize As Integer
    UseSize = CurFontSize
    If UseSize = 0 Then
        UseSize = 12
    End If
    MyPanel.Font = "Arial"
    MyPanel.FontBold = True
    MyPanel.FontSize = Abs(UseSize)
    'Quickhack to make it compatible
    If (UseSize < 0) Or (UseSize > 12) Then
        MyPanel.CurrentX = CurX + 30
        MyPanel.CurrentY = CurY + 30
        MyPanel.ForeColor = vbBlack
        MyPanel.Print myText;
        MyPanel.CurrentX = CurX
        MyPanel.CurrentY = CurY
        MyPanel.ForeColor = vbWhite
        MyPanel.Print myText;
    Else
        MyPanel.CurrentX = CurX - 15
        MyPanel.CurrentY = CurY - 15
        MyPanel.ForeColor = vbWhite
        MyPanel.Print myText;
        MyPanel.CurrentX = CurX
        MyPanel.CurrentY = CurY
        MyPanel.ForeColor = vbBlack
        MyPanel.Print myText;
    End If
    EndX = MyPanel.CurrentX
    EndY = MyPanel.CurrentY
    'MyPanel.Refresh
    'DoEvents
End Sub
Public Function TranslateFieldItems(FldNames As String, FldList As String) As String
    Dim Result As String
    Dim CurItem As Long
    Dim ItemNo As Integer
    Dim FldName As String
    Result = ""
    CurItem = 0
    FldName = GetRealItem(FldNames, CurItem, ",")
    While FldName <> ""
        ItemNo = GetRealItemNo(FldList, FldName)
        If ItemNo >= 0 Then Result = Result & CStr(ItemNo) & ","
        CurItem = CurItem + 1
        FldName = GetRealItem(FldNames, CurItem, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    TranslateFieldItems = Result
End Function

Public Sub ShowBasicDetails(UseType As String, tmpText As String)
    Dim tmpSqlKey As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim retval As Integer
    Dim CurItm As Integer
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim MsgTxt As String
    Dim tmpFlno As String
    Dim tmpFlca As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim ReqButtons As String
    Dim GetValue As String
    Dim MsgContext As String
    Dim AtrSqlKey As String
    Screen.MousePointer = 11
    If tmpText <> "" Then
        MsgContext = ""
        ReqButtons = ""
        tmpTable = ""
        Select Case UseType
            Case "ALC"
                tmpTable = "ALTTAB"
                tmpFields = "ALC2,ALC3,ALFN,CTRY,CASH,ADD1,ADD2,ADD3,ADD4,PHON,TFAX,SITA,TELX,WEBS,BASE"
                If Len(tmpText) = 2 Then
                    tmpSqlKey = "WHERE ALC2='" & tmpText & "'"
                End If
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ALC3='" & tmpText & "'"
                End If
                'ReqButtons = "OK,Flights"
            Case "FLNO"
                'If Not FilterCalledAsDispo Then
                '    tmpFlno = StripAftFlno(tmpText, tmpFlca, tmpFltn, tmpFlns)
                '    ShowFlights.Show
                '    If Not StartedAsCompareTool Then
                '        ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpFlca, tmpFltn, tmpFlns
                '    Else
                '        ShowFlights.LoadFlightData MinImpVpfr, MaxImpVpto, tmpFlca, tmpFltn, tmpFlns
                '    End If
                'End If
            Case "APT"
                GetValue = ""
                tmpTable = "APTTAB"
                tmpFields = "APC3,APC4,APSN,APFN,APN2,APN3,APN4,LAND,APTT,TDIS,TDIW"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE APC3='" & tmpText & "'"
                    AtrSqlKey = tmpSqlKey
                    GetValue = HiddenData.BasicDataLookUp("ATRTAB", "APC3", tmpText, "ATRP", True)
                End If
                If Len(tmpText) = 4 Then
                    tmpSqlKey = "WHERE APC4='" & tmpText & "'"
                    AtrSqlKey = tmpSqlKey
                    GetValue = HiddenData.BasicDataLookUp("ATRTAB", "APC4", tmpText, "ATRP", True)
                End If
                If GetValue <> "" Then
                    MsgContext = "Restriction:" & vbNewLine
                    MsgContext = MsgContext & GetValue
                    ReqButtons = "OK,Curfew"
                End If
            Case "ACT"
                tmpTable = "ACTTAB"
                tmpFields = "ACT3,ACT5,ACTI,ACFN,SEAF,SEAB,SEAE,SEAT,ENTY"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ACT3='" & tmpText & "'"
                Else
                    tmpSqlKey = "WHERE ACT5='" & tmpText & "'"
                End If
            Case Else
        End Select
        If tmpTable <> "" Then
            retval = UfisServer.CallCeda(UserAnswer, "RT", tmpTable, tmpFields, "", tmpSqlKey, "", 0, True, False)
            MsgTxt = ""
            CurItm = 0
            Do
                CurItm = CurItm + 1
                tmpFldNam = GetItem(tmpFields, CurItm, ",")
                If tmpFldNam <> "" Then
                    tmpFldVal = GetItem(UserAnswer, CurItm, ",")
                    If tmpFldVal <> "" Then
                        MsgTxt = MsgTxt & tmpFldNam & ": " & tmpFldVal & vbNewLine
                    End If
                End If
            Loop While tmpFldNam <> ""
            If MsgTxt <> "" Then
                MsgTxt = Left(MsgTxt, Len(MsgTxt) - 2)
            Else
                MsgTxt = "'" & tmpText & "' is not in your basic data."
            End If
            If MsgContext <> "" Then
                MsgTxt = MsgTxt & vbNewLine & MsgContext
            End If
            If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Information", MsgTxt, "infomsg", ReqButtons, UserAnswer) = 2 Then
                'Me.Refresh
                UserAnswer = Replace(UserAnswer, "&", "", 1, -1, vbBinaryCompare)
                Select Case UserAnswer
                    Case "Flights"
                        'If Not FilterCalledAsDispo Then
                        '    ShowFlights.Show , MainDialog
                        '    ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpText, tmpFltn, tmpFlns
                        'End If
                    Case "Curfew"
                        BasicDetails.Show , MainDialog
                        BasicDetails.Refresh
                        BasicDetails.LoadAtrTabData 0, AtrSqlKey
                        BasicDetails.Refresh
                    Case Else
                End Select
            End If
        End If
    End If
    Screen.MousePointer = 0
End Sub

