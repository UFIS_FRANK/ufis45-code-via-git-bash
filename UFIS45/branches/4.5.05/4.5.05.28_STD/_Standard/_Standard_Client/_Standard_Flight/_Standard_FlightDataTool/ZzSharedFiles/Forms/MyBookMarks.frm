VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MyBookMarks 
   Caption         =   "Bookmarks"
   ClientHeight    =   2490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11010
   Icon            =   "MyBookMarks.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2490
   ScaleWidth      =   11010
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   3825
      Left            =   30
      ScaleHeight     =   3765
      ScaleWidth      =   3855
      TabIndex        =   1
      Top             =   30
      Width           =   3915
      Begin VB.CheckBox chkWork 
         Caption         =   "Reset 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Reset 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   1050
      End
      Begin TABLib.TAB BookMark 
         Height          =   3225
         Index           =   1
         Left            =   2220
         TabIndex        =   3
         Top             =   630
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65536
         _ExtentX        =   4048
         _ExtentY        =   5689
         _StockProps     =   64
      End
      Begin TABLib.TAB BookMark 
         Height          =   3225
         Index           =   0
         Left            =   0
         TabIndex        =   4
         Top             =   630
         Visible         =   0   'False
         Width           =   2115
         _Version        =   65536
         _ExtentX        =   3731
         _ExtentY        =   5689
         _StockProps     =   64
      End
      Begin VB.Label GridLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   30
         TabIndex        =   6
         Top             =   390
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label GridLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   2250
         TabIndex        =   5
         Top             =   390
         Visible         =   0   'False
         Width           =   570
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   2205
      Width           =   11010
      _ExtentX        =   19420
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16325
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MyBookMarks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AlreadyActive As Boolean
Dim MarkColNo(0 To 1) As Long

Public Sub CloneTabLayout(Index As Integer, UseTab As TABLib.Tab, MarkField As String)
    Dim i As Integer
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim tmpData As String
    BookMark(Index).ResetContent
    BookMark(Index).FontName = MainDialog.CurrentTabFont
    BookMark(Index).FontSize = MainDialog.FontSlider.Value + 6
    BookMark(Index).HeaderFontSize = MainDialog.FontSlider.Value + 6
    BookMark(Index).LineHeight = MainDialog.FontSlider.Value + 6
    BookMark(Index).LeftTextOffset = 1
    BookMark(Index).SetTabFontBold True
    BookMark(Index).MainHeader = True
    'BookMark(Index).GridlineColor = vbBlack
    If GridLifeStyle Then
        BookMark(Index).LifeStyle = True
        BookMark(Index).CursorLifeStyle = True
        BookMark(Index).SelectColumnBackColor = RGB(235, 235, 255)
        BookMark(Index).SelectColumnTextColor = vbBlack
    End If
    BookMark(Index).LogicalFieldList = UseTab.LogicalFieldList
    BookMark(Index).HeaderString = UseTab.HeaderString
    BookMark(Index).SetMainHeaderValues UseTab.GetMainHeaderRanges, UseTab.GetMainHeaderValues, UseTab.GetMainHeaderColors
    BookMark(Index).SetMainHeaderFont MainDialog.FontSlider.Value + 6, False, False, True, 0, MainDialog.CurrentTabFont
    BookMark(Index).ColumnAlignmentString = UseTab.ColumnAlignmentString
    BookMark(Index).HeaderAlignmentString = UseTab.HeaderAlignmentString
    MaxCol = UseTab.GetColumnCount - 1
    For CurCol = 0 To MaxCol
        tmpData = UseTab.DateTimeGetInputFormatString(CurCol)
        If tmpData <> "" Then
            BookMark(Index).DateTimeSetColumn CurCol
            BookMark(Index).DateTimeSetInputFormatString CurCol, tmpData
            BookMark(Index).DateTimeSetOutputFormatString CurCol, UseTab.DateTimeGetOutputFormatString(CurCol)
            BookMark(Index).DateTimeSetUTCOffsetMinutes CurCol, UseTab.DateTimeGetUTCOffsetMinutes(CurCol)
        End If
    Next
    BookMark(Index).ShowHorzScroller True
    BookMark(Index).ShowVertScroller True
    BookMark(Index).AutoSizeByHeader = True
    BookMark(Index).AutoSizeColumns
    MarkColNo(Index) = GetRealItemNo(UseTab.LogicalFieldList, MarkField)
    If MarkColNo(Index) < 0 Then MarkColNo(Index) = 0
    i = UseTab.Index
    GridLabel(Index).Visible = True
    GridLabel(Index).Tag = CStr(i)
    BookMark(Index).Visible = True
End Sub

Public Sub SetBookMark(UseTab As TABLib.Tab, LineNo As Long)
    Dim tmpTag As String
    Dim tmpData As String
    Dim UseLineStamp As String
    Dim SetLineStamp As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SetLine As Long
    Dim ColNo As Long
    Dim Index As Integer
    Dim TabIdx As Integer
    Dim j As Integer
    Index = -1
    TabIdx = UseTab.Index
    For j = 0 To 1
        tmpTag = GridLabel(j).Tag
        If tmpTag <> "" Then
            If Val(tmpTag) = TabIdx Then Index = j
        End If
    Next
    If Index >= 0 Then
        If GridLabel(Index).Caption = "" Then
            GridLabel(Index).Caption = "(" & CStr(Index + 1) & ") " & MainDialog.CleanGridName(TabIdx)
        End If
        UseLineStamp = UseTab.GetFieldValue(LineNo, "'S3'")
        SetLineStamp = ""
        MaxLine = BookMark(Index).GetLineCount - 1
        SetLine = -1
        CurLine = -1
        While (CurLine <= MaxLine) And (SetLineStamp <> UseLineStamp)
            CurLine = CurLine + 1
            SetLineStamp = BookMark(Index).GetFieldValue(CurLine, "'S3'")
        Wend
        SetLine = CurLine
        If SetLine > MaxLine Then SetLine = -1
        If SetLine < 0 Then
            BookMark(Index).InsertTextLine UseTab.GetLineValues(LineNo), False
            SetLine = BookMark(Index).GetLineCount - 1
            UseTab.SetDecorationObject LineNo, MarkColNo(Index), "'S3'"
            UseTab.SetFieldValues LineNo, "'C3'", "S"
            CheckChartMarker TabIdx, LineNo, True
        Else
            UseTab.ResetCellDecoration LineNo, MarkColNo(Index)
            UseTab.SetFieldValues LineNo, "'C3'", "-"
            CheckChartMarker TabIdx, LineNo, False
            BookMark(Index).DeleteLine SetLine
        End If
        If SetLine >= 0 Then
            BookMark(Index).SetCurrentSelection SetLine
            BookMark(Index).OnVScrollTo SetLine - 5
        End If
        BookMark(Index).AutoSizeColumns
        BookMark(Index).Refresh
        BookMarkSet1 = BookMark(0).GetLineCount
        BookMarkSet2 = BookMark(1).GetLineCount
        BookMarkTotal = BookMarkSet1 + BookMarkSet2
        If BookMarkButtonIdx >= 0 Then
            If BookMarkTotal > 0 Then
                MainDialog.chkWork(BookMarkButtonIdx).Enabled = True
            Else
                MainDialog.chkWork(BookMarkButtonIdx).Enabled = False
            End If
        End If
    End If
End Sub
Private Sub CheckChartMarker(Index, LineNo, SetMarker As Boolean)
    Dim tmpRidx As String
    Dim tmpGocx As String
    Dim tmpConx As String
    Dim tmpHit As String
    Dim RotLine As Long
    If RotationActivated = True Then
        If Index = TabArrFlightsIdx Then
            tmpRidx = TabArrFlightsTab.GetFieldValue(LineNo, "'C4'")
            If tmpRidx <> "" Then
                tmpHit = TabRotFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                If tmpHit <> "" Then
                    RotLine = Val(tmpHit)
                    tmpConx = TabRotFlightsTab.GetFieldValue(RotLine, "CONX")
                    If tmpConx <> "" Then
                        ConnexChart.ToggleFlightMarker "A", Val(tmpConx), SetMarker
                    End If
                End If
            End If
        End If
    End If

End Sub

Public Sub ResetAll()
    ResetBookMarks 0
    ResetBookMarks 1
End Sub

Private Sub ResetBookMarks(Index)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim HitList As String
    Dim LineStamp As String
    Dim SetLineNo As Long
    Dim i As Integer
    i = Val(GridLabel(Index).Tag)
    MaxLine = BookMark(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        LineStamp = BookMark(Index).GetFieldValue(CurLine, "'S3'")
        HitList = MainDialog.FileData(i).GetLinesByIndexValue("'S3'", LineStamp, 0)
        If HitList <> "" Then
            SetLineNo = Val(HitList)
            MainDialog.FileData(i).ResetCellDecoration SetLineNo, MarkColNo(Index)
            MainDialog.FileData(i).SetFieldValues SetLineNo, "'C3'", "-"
            CheckChartMarker i, SetLineNo, False
        End If
    Next
    MainDialog.FileData(i).Refresh
    BookMark(Index).ResetContent
    BookMark(Index).Refresh
    BookMarkSet1 = BookMark(0).GetLineCount
    BookMarkSet2 = BookMark(1).GetLineCount
    BookMarkTotal = BookMarkSet1 + BookMarkSet2
    If BookMarkButtonIdx >= 0 Then
        If BookMarkTotal > 0 Then
            MainDialog.chkWork(BookMarkButtonIdx).Enabled = True
        Else
            MainDialog.chkWork(BookMarkButtonIdx).Enabled = False
        End If
    End If
End Sub

Private Sub BookMark_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim HitList As String
    Dim LineStamp As String
    Dim SetLineNo As Long
    Dim i As Integer
    LineStamp = BookMark(Index).GetFieldValue(LineNo, "'S3'")
    i = Val(GridLabel(Index).Tag)
    HitList = MainDialog.FileData(i).GetLinesByIndexValue("'S3'", LineStamp, 0)
    If HitList <> "" Then
        SetLineNo = Val(HitList)
        MainDialog.FileData(i).OnVScrollTo SetLineNo
        MainDialog.FileData(i).SetCurrentSelection SetLineNo
    End If

End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                Me.Hide
                MainDialog.PushWorkButton "BOOKMARK", 0
                chkWork(Index).Value = 0
            Case 1
                ResetBookMarks 0
                chkWork(Index).Value = 0
            Case 2
                ResetBookMarks 1
                chkWork(Index).Value = 0
            Case Else
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub Form_Load()
    GridLabel(0).Caption = ""
    GridLabel(1).Caption = ""
    MarkColNo(0) = 4
    MarkColNo(1) = 4
    If Not AlreadyActive Then
        Me.Width = 700 * 15
        Me.Left = MainDialog.Left + ((MainDialog.Width - Me.Width) / 2)
        Me.Height = 300 * 15
        Me.Top = MainDialog.Top + (MainDialog.Height - Me.Height) / 2
        AlreadyActive = True
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        chkWork(0).Value = 1
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleHeight - StatusBar1.Height - 30
    If NewSize > 600 Then
        WorkArea.Height = NewSize
        NewSize = NewSize - BookMark(0).Top - 60
        If NewSize > 300 Then
            BookMark(0).Height = NewSize
            BookMark(1).Height = NewSize
        End If
    End If
    NewSize = Me.ScaleWidth - (WorkArea.Left * 2)
    If NewSize > 1500 Then
        WorkArea.Width = NewSize
        NewSize = (NewSize / 2) - (BookMark(0).Left * 2) - 60
        If NewSize > 300 Then
            BookMark(0).Width = NewSize
            BookMark(1).Left = BookMark(0).Left + NewSize + (BookMark(0).Left * 2) + 60
            BookMark(1).Width = NewSize
            GridLabel(0).Left = BookMark(0).Left + 30
            GridLabel(1).Left = BookMark(1).Left + 30
        End If
    End If
    DrawBackGround WorkArea, 7, True, True
End Sub
