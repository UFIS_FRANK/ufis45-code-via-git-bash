VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form DbfReader 
   Caption         =   "UFIS File Reader"
   ClientHeight    =   6570
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10380
   Icon            =   "DbfReader.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6570
   ScaleWidth      =   10380
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkTimes 
      Caption         =   "HP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   56
      ToolTipText     =   "Local Times related to HOPO"
      Top             =   30
      Width           =   435
   End
   Begin TABLib.TAB AodbTab 
      Height          =   2475
      Left            =   7020
      TabIndex        =   41
      Top             =   720
      Visible         =   0   'False
      Width           =   1005
      _Version        =   65536
      _ExtentX        =   1773
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.Frame fraExplain 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   3360
      TabIndex        =   40
      Top             =   4260
      Visible         =   0   'False
      Width           =   3285
   End
   Begin VB.Frame fraTimeFilter 
      Caption         =   "Date/Time Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   30
      TabIndex        =   35
      Top             =   4260
      Visible         =   0   'False
      Width           =   3225
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   2445
         TabIndex        =   53
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2115
         TabIndex        =   51
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   2445
         TabIndex        =   46
         Top             =   300
         Width           =   315
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2115
         TabIndex        =   45
         Top             =   300
         Width           =   315
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   780
         TabIndex        =   47
         Top             =   630
         Width           =   555
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1350
         TabIndex        =   48
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1680
         TabIndex        =   49
         Top             =   630
         Width           =   315
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   780
         TabIndex        =   42
         Top             =   300
         Width           =   555
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1350
         TabIndex        =   43
         Top             =   300
         Width           =   315
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1680
         TabIndex        =   44
         Top             =   300
         Width           =   315
      End
      Begin VB.CheckBox chkFromTo 
         Caption         =   "To"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   "Confirm Time Filter"
         Top             =   630
         Width           =   660
      End
      Begin VB.CheckBox ResetFilter 
         Caption         =   "From"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Reset Time Filter"
         Top             =   300
         Width           =   660
      End
      Begin VB.OptionButton optUtc 
         BackColor       =   &H0080FF80&
         Caption         =   "U"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2820
         Style           =   1  'Graphical
         TabIndex        =   37
         ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.OptionButton optLocal 
         Caption         =   "L"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2820
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
         Top             =   630
         Width           =   300
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1995
         TabIndex        =   52
         Top             =   660
         Width           =   135
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1995
         TabIndex        =   50
         Top             =   330
         Width           =   135
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Fields"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame fraImpPanel 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   285
      Index           =   1
      Left            =   5970
      TabIndex        =   26
      Top             =   360
      Visible         =   0   'False
      Width           =   4245
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   13
         Left            =   0
         TabIndex        =   33
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   12
         Left            =   600
         TabIndex        =   32
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   1200
         TabIndex        =   31
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   1800
         TabIndex        =   30
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   2400
         TabIndex        =   29
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   3000
         TabIndex        =   28
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   3600
         TabIndex        =   27
         Top             =   0
         Width           =   585
      End
   End
   Begin VB.Frame fraImpPanel 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   285
      Index           =   0
      Left            =   5970
      TabIndex        =   18
      Top             =   30
      Visible         =   0   'False
      Width           =   4245
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   3600
         TabIndex        =   25
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   3000
         TabIndex        =   24
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   2400
         TabIndex        =   23
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1800
         TabIndex        =   22
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1200
         TabIndex        =   21
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   600
         TabIndex        =   20
         Top             =   0
         Width           =   585
      End
      Begin VB.Label lblImpCnt 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   19
         Top             =   0
         Width           =   585
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB HelperTab 
      Height          =   1875
      Left            =   8190
      TabIndex        =   13
      Top             =   810
      Visible         =   0   'False
      Width           =   945
      _Version        =   65536
      _ExtentX        =   1667
      _ExtentY        =   3307
      _StockProps     =   64
   End
   Begin VB.Frame ErrorFrame 
      Height          =   795
      Left            =   0
      TabIndex        =   6
      Top             =   3345
      Width           =   3795
      Begin VB.CheckBox chkWork 
         Caption         =   "Save As"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   450
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Index           =   5
         Left            =   60
         Picture         =   "DbfReader.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   150
         Width           =   735
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Proceed"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   450
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Stop"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   150
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Warnings:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1950
         TabIndex        =   10
         Top             =   510
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Errors:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1950
         TabIndex        =   9
         Top             =   210
         Width           =   675
      End
      Begin VB.Label ErrCnt 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2880
         TabIndex        =   8
         ToolTipText     =   "Error Counter"
         Top             =   150
         Width           =   855
      End
      Begin VB.Label WarnCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2880
         TabIndex        =   7
         ToolTipText     =   "Warning Counter"
         Top             =   450
         Width           =   855
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6285
      Width           =   10380
      _ExtentX        =   18309
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15214
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4830
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "SSIM"
      Top             =   30
      Width           =   855
   End
   Begin VB.TextBox UseFileName 
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Text            =   "D:\Ufis\Data\ImportTool\BKK\GOCC\TG_AODBSSNFLT_20080215053004.txt"
      Top             =   360
      Width           =   4275
   End
   Begin TABLib.TAB DataTab 
      Height          =   2535
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   3195
      _Version        =   65536
      _ExtentX        =   5636
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB FlightData 
      Height          =   2535
      Left            =   3270
      TabIndex        =   16
      Top             =   720
      Visible         =   0   'False
      Width           =   3195
      _Version        =   65536
      _ExtentX        =   5636
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin VB.CheckBox chkTimes 
      Caption         =   "UT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   55
      ToolTipText     =   "Show UTC Times"
      Top             =   30
      Width           =   405
   End
   Begin VB.CheckBox chkTimes 
      Caption         =   "LT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   3930
      Style           =   1  'Graphical
      TabIndex        =   54
      ToolTipText     =   "Show Local Times"
      Top             =   30
      Width           =   435
   End
End
Attribute VB_Name = "DbfReader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim FileIsValid As Boolean
Dim ShowErrorPanel As Boolean
Private Type DBF_HEADER
    DbfVers As String * 4
    DbfRecCnt As Integer
    DbfRecCntx As Integer
    DbfHdrLen As Integer
    DbfRecLen As Integer
    DbfEtc As String * 20
End Type
Private Type DBF_FIELD
    DbfFldName As String * 11
    DbfFldType As String * 1
    DbfFldEtc1 As String * 4
    DbfFldLen As String * 1
    DbfFldDig As String * 1
    DbfFldEtc2 As String * 14
End Type
Private Type DAT_FIELD
    FldNam As String * 16
    FldTyp As String * 4
    FldDig As Integer
    FldLen As Integer
    FldPos As Integer
    FldEnd As Integer
End Type
Dim ShowDetails As Boolean
Dim SsimFldList As String
Dim SsimLenList As String
Dim SsimHedList As String
Dim ShowAodbReader As Boolean
Dim ShownModal As Boolean
Dim IgnoreThisEvent As Boolean
Dim SsimUtvhChecked As Boolean

Public Sub OpenDbfFile(DbfFileName As String, MyParent As Form, ShowMe As Boolean)
    UseFileName.Text = DbfFileName
    If ShowMe Then
        Me.Top = MyParent.Top + 150 * Screen.TwipsPerPixelY
        Me.Left = MyParent.Left + 150 * Screen.TwipsPerPixelX
        Me.Show
    End If
    chkWork(0).Tag = "DBF"
    chkWork(2).Value = 0
    chkWork(0).Value = 1
End Sub
Public Sub OpenAnyFile(CurFileName As String, MyParent As Form, ShowMe As Boolean)
    'PreCheckFile = False
    UseFileName.Text = CurFileName
    If ShowMe Then
        Me.Top = MyParent.Top + 150 * Screen.TwipsPerPixelY
        Me.Left = MyParent.Left + 150 * Screen.TwipsPerPixelX
        Me.Show
    End If
    chkWork(0).Tag = "ANY"
    chkWork(2).Value = 0
    chkWork(0).Value = 1
End Sub
Public Sub PreCheckAnyFile(CurFileName As String)
    'PreCheckFile = True
    UseFileName.Text = CurFileName
    chkWork(0).Tag = "ANY"
    chkWork(2).Value = 0
    chkWork(0).Value = 1
End Sub
Private Sub ReadDbfFile()
    Dim CurFileName As String
    Dim DbfFile As Integer
    Dim FldCnt As Integer
    Dim CurFld As Integer
    Dim ChrPos As Integer
    Dim FilPos As Long
    Dim CurRec As Long
    Dim LinCnt As Long
    Dim FldList As String
    Dim LenList As String
    Dim tmpFldNam As String
    Dim tmpFldLen As Integer
    Dim tmpFldPos As Integer
    Dim tmpDbfRec As String
    Dim tmpFldDat As String
    Dim tmpDatRec As String
    Dim DbfHeaderLine As DBF_HEADER
    Dim DbfFieldLine As DBF_FIELD
    Dim DbfDataField(256) As DAT_FIELD
    CurFileName = UseFileName.Text
    If UCase(Right(Dir(CurFileName), 4)) = ".DBF" Then
        Screen.MousePointer = 11
        FldList = ""
        LenList = ""
        DataTab.ResetContent
        DataTab.SetFieldSeparator ","
        DbfFile = FreeFile
        Open CurFileName For Binary As #DbfFile
        Get #DbfFile, 1, DbfHeaderLine
        FldCnt = (DbfHeaderLine.DbfHdrLen / 32) - 1
        tmpFldPos = 2
        For CurFld = 1 To FldCnt
            Get #DbfFile, , DbfFieldLine
            tmpFldNam = Left(DbfFieldLine.DbfFldName, 11)
            ChrPos = InStr(tmpFldNam, Chr(0))
            If ChrPos > 1 Then tmpFldNam = Left(tmpFldNam, ChrPos - 1)
            tmpFldNam = Trim(tmpFldNam)
            FldList = FldList & tmpFldNam & ","
            DbfDataField(CurFld).FldNam = tmpFldNam
            tmpFldLen = Asc(DbfFieldLine.DbfFldLen)
            DbfDataField(CurFld).FldLen = tmpFldLen
            LenList = LenList & Trim(Str(tmpFldLen * Screen.TwipsPerPixelX)) & ","
            DbfDataField(CurFld).FldPos = tmpFldPos
            tmpFldPos = tmpFldPos + tmpFldLen - 1
            DbfDataField(CurFld).FldEnd = tmpFldPos
            tmpFldPos = tmpFldPos + 1
        Next
        DataTab.HeaderString = FldList
        DataTab.HeaderLengthString = LenList
        DataTab.SetFieldSeparator ";"
        DataTab.AutoSizeByHeader = True
        DataTab.ShowVertScroller False
        DataTab.ShowHorzScroller True
        DataTab.HeaderFontSize = 17
        DataTab.FontSize = 17
        DataTab.LineHeight = 17
        DataTab.SetTabFontBold True
        DataTab.RedrawTab
        Me.Refresh
        LinCnt = 0
        FilPos = DbfHeaderLine.DbfHdrLen + 1 - DbfHeaderLine.DbfRecLen
        For CurRec = 1 To DbfHeaderLine.DbfRecCnt
            FilPos = FilPos + DbfHeaderLine.DbfRecLen
            tmpDbfRec = Space(DbfHeaderLine.DbfRecLen)
            Get #DbfFile, FilPos, tmpDbfRec
            If (Left(tmpDbfRec, 1) <> "*") Or (chkWork(2).Value = 1) Then
                tmpDatRec = ""
                For CurFld = 1 To FldCnt
                    tmpFldDat = Mid(tmpDbfRec, DbfDataField(CurFld).FldPos, DbfDataField(CurFld).FldLen)
                    tmpFldDat = Replace(tmpFldDat, ";", ",", 1, -1, vbBinaryCompare)
                    tmpDatRec = tmpDatRec & tmpFldDat & ";"
                Next
                DataTab.InsertTextLine tmpDatRec, False
                If Left(tmpDbfRec, 1) = "*" Then
                    DataTab.SetLineColor LinCnt, vbBlack, LightRed
                    DataTab.SetLineStatusValue LinCnt, 1
                End If
                LinCnt = LinCnt + 1
                If LinCnt Mod 30 = 0 Then
                    DataTab.OnVScrollTo LinCnt - 30
                    DataTab.RedrawTab
                    Me.Refresh
                End If
            End If
        Next
        Close DbfFile
        DataTab.AutoSizeColumns
        DataTab.ShowVertScroller True
        DataTab.OnVScrollTo 0
        DataTab.RedrawTab
        Me.Refresh
        Screen.MousePointer = 0
    Else
        MsgBox "File Not Found." & vbNewLine & CurFileName
    End If
End Sub

Private Sub AodbTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        If Not ShownModal Then
            'frmTelex.ShowTelex AodbTab.GetColumnValue(LineNo, 1), AodbTab.GetColumnValue(LineNo, 2)
        End If
    End If
End Sub

Private Sub chkTimes_Click(Index As Integer)
    If chkTimes(Index).Value = 1 Then
        chkTimes(Index).BackColor = LightGreen
        If Not IgnoreThisEvent Then
            IgnoreThisEvent = True
            Select Case Index
                Case 0
                    chkTimes(1).Value = 0
                    chkTimes(2).Value = 0
                    'NextTimeMode = "U"
                Case 1
                    chkTimes(0).Value = 0
                    'NextTimeMode = "L"
                Case 2
                    chkTimes(0).Value = 0
                    chkTimes(1).Value = 1
                    'NextTimeMode = "H"
            End Select
            'If DataTimeMode <> "-" Then
            '    If DataTimeMode <> "U" Then ToggleSsimLocalToUtc DataTimeMode, "U"
            '    If NextTimeMode <> "U" Then ToggleSsimLocalToUtc DataTimeMode, NextTimeMode
            'Else
                'DataTimeMode = NextTimeMode
            'End If
            IgnoreThisEvent = False
        End If
    Else
        chkTimes(Index).BackColor = vbButtonFace
        If Not IgnoreThisEvent Then
            Select Case Index
                Case 0
                    chkTimes(1).Value = 1
                Case 1
                    chkTimes(2).Value = 0
                    chkTimes(0).Value = 1
                Case 2
                    chkTimes(1).Value = 0
            End Select
        End If
    End If
End Sub

Private Sub ToggleSsimLocalToUtc(CurTimeMode As String, NewTimeMode As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurDateDiff As Long
    Dim tmpVpfr As String
    Dim NewVpfr As String
    Dim tmpVpto As String
    Dim tmpStod As String
    Dim tmpStdp As String
    Dim tmpStoa As String
    Dim tmpStap As String
    Dim tmpFrqd As String
    Dim NewFrqd As String
    Dim tmpUtvo As String
    Dim tmpUtvd As String
    Dim NewDateTime As String
    Dim NewDate As String
    Dim NewTime As String
    Dim NewRecData As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim IsValid As Boolean
    Dim LocalToUtc As Boolean
    
    If CurTimeMode <> "-" Then
        Select Case CurTimeMode
            Case "U"
                tmpTxt1 = "UTC"
                LocalToUtc = False
            Case "L"
                tmpTxt1 = "Local Times"
                LocalToUtc = True
            Case "H"
                tmpTxt1 = HomeAirport & " Local Times"
                LocalToUtc = True
            Case Else
        End Select
        Select Case NewTimeMode
            Case "U"
                tmpTxt2 = "UTC"
            Case "L"
                tmpTxt2 = "Local Times"
            Case "H"
                tmpTxt2 = HomeAirport & " Local Times"
            Case Else
        End Select
        
        StatusBar1.Panels(2) = "Toggle " & tmpTxt1 & " to " & tmpTxt2 & " ..."
        Screen.MousePointer = 11
        Me.Refresh
        MaxLine = FlightData.GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
            tmpVpfr = DecodeSsimDayFormat(tmpVpfr, "SSIM2", "CEDA")
            tmpVpto = FlightData.GetFieldValue(CurLine, "VPTO")
            tmpVpto = DecodeSsimDayFormat(tmpVpto, "SSIM2", "CEDA")
            tmpStod = FlightData.GetFieldValue(CurLine, "STOD")
            tmpStoa = FlightData.GetFieldValue(CurLine, "STOA")
            tmpStdp = FlightData.GetFieldValue(CurLine, "STDP")
            tmpStap = FlightData.GetFieldValue(CurLine, "STAP")
            tmpFrqd = FlightData.GetFieldValue(CurLine, "FRQD")
            tmpFrqd = FormatFrequency(tmpFrqd, "123", IsValid)
            
            Select Case CurTimeMode
                Case "U"
                    If NewTimeMode = "L" Then
                        tmpUtvo = FlightData.GetFieldValue(CurLine, "UTVO")
                        tmpUtvd = FlightData.GetFieldValue(CurLine, "UTVD")
                    End If
                    If NewTimeMode = "H" Then
                        tmpUtvo = FlightData.GetFieldValue(CurLine, "UTVH")
                        tmpUtvd = FlightData.GetFieldValue(CurLine, "UTVH")
                    End If
                Case "L"
                    tmpUtvo = FlightData.GetFieldValue(CurLine, "UTVO")
                    tmpUtvd = FlightData.GetFieldValue(CurLine, "UTVD")
                Case "H"
                    tmpUtvo = FlightData.GetFieldValue(CurLine, "UTVH")
                    tmpUtvd = FlightData.GetFieldValue(CurLine, "UTVH")
            End Select
            
            If LocalToUtc Then
                If Len(tmpUtvo) = 5 Then
                    If Left(tmpUtvo, 1) = "-" Then Mid(tmpUtvo, 1) = "+" Else Mid(tmpUtvo, 1) = "-"
                Else
                    tmpUtvo = "-" & tmpUtvo
                End If
                If Len(tmpUtvd) = 5 Then
                    If Left(tmpUtvd, 1) = "-" Then Mid(tmpUtvd, 1) = "+" Else Mid(tmpUtvd, 1) = "-"
                Else
                    tmpUtvd = "-" & tmpUtvd
                End If
            End If
            
            NewRecData = ""
            NewDateTime = CedaTimeAddHhMm(tmpVpfr, tmpStod, tmpUtvo)
            NewDate = Left(NewDateTime, 8)
            NewVpfr = NewDate
            NewDate = DecodeSsimDayFormat(NewDate, "CEDA", "SSIM2")
            NewRecData = NewRecData & NewDate & ","
            
            NewDateTime = CedaTimeAddHhMm(tmpVpto, tmpStod, tmpUtvo)
            NewDate = Left(NewDateTime, 8)
            NewDate = DecodeSsimDayFormat(NewDate, "CEDA", "SSIM2")
            NewRecData = NewRecData & NewDate & ","
            NewTime = Right(NewDateTime, 4)
            NewRecData = NewRecData & NewTime & ","
            
            NewDateTime = CedaTimeAddHhMm(tmpVpfr, tmpStdp, tmpUtvo)
            NewTime = Right(NewDateTime, 4)
            NewRecData = NewRecData & NewTime & ","
            
            CurDateDiff = CedaDateDiff(tmpVpfr, NewVpfr)
            If CurDateDiff <> 0 Then
                NewFrqd = ShiftFrqd(tmpFrqd, CInt(CurDateDiff))
            Else
                NewFrqd = tmpFrqd
            End If
            NewRecData = NewRecData & NewFrqd & ","
            
            NewDateTime = CedaTimeAddHhMm(tmpVpfr, tmpStoa, tmpUtvd)
            NewTime = Right(NewDateTime, 4)
            NewRecData = NewRecData & NewTime & ","
            NewDateTime = CedaTimeAddHhMm(tmpVpfr, tmpStap, tmpUtvd)
            NewTime = Right(NewDateTime, 4)
            NewRecData = NewRecData & NewTime & ","
            
            FlightData.SetFieldValues CurLine, "VPFR,VPTO,STOD,STDP,FRQD,STOA,STAP", NewRecData
            If CurLine Mod 100 = 0 Then
                FlightData.OnVScrollTo CurLine
                FlightData.Refresh
                Me.Refresh
                DoEvents
            End If
        Next
        Screen.MousePointer = 0
        FlightData.OnVScrollTo 0
        FlightData.Refresh
        StatusBar1.Panels(2) = ""
        Me.Refresh
        DoEvents
    End If
    
    CurTimeMode = NewTimeMode

End Sub

Private Function CedaTimeAddHhMm(CedaDateValue As String, CedaTimeValue As String, AddHhMm As String) As String
    Dim Result As String
    Dim tmpData As String
    Dim tmpDiff As String
    Dim tmpOffs As Double
    Dim tmpSign As String
    Dim tmpTime
    Result = ""
    tmpData = CedaDateValue & CedaTimeValue
    tmpTime = CedaFullDateToVb(tmpData)
    tmpDiff = AddHhMm
    tmpSign = "+"
    If Len(tmpDiff) = 5 Then
        tmpSign = Left(tmpDiff, 1)
        tmpDiff = Mid(tmpDiff, 2)
    End If
    tmpOffs = Val(Mid(tmpDiff, 1, 2)) * 60
    tmpOffs = tmpOffs + Val(Mid(tmpDiff, 3, 2))
    If tmpSign = "-" Then tmpOffs = -tmpOffs
    tmpTime = DateAdd("n", tmpOffs, tmpTime)
    Result = Format(tmpTime, "yyyymmddhhmm")
    CedaTimeAddHhMm = Result
End Function

Private Sub chkWork_Click(Index As Integer)
    Dim tmpType As String
    Dim RetVal As Integer
    Dim MsgTxt As String
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightestGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0  'Load
                'StopFileInput = False
                ShowDetails = False
                chkWork(1).Visible = True
                chkWork(4).Visible = True
                chkWork(6).Visible = False
                fraImpPanel(0).Visible = False
                fraImpPanel(1).Visible = False
                tmpType = chkWork(Index).Tag
                'chkWork(7).Enabled = False
                chkWork(7).Left = chkWork(2).Left + chkWork(2).Width + 15
                chkWork(8).Left = chkWork(7).Left + chkWork(7).Width + 15
                Select Case tmpType
                    Case "DBF"
                        ReadDbfFile
                    Case "ANY"
                        chkWork(3).Value = 0
                        chkWork(4).Value = 0
                        ErrCnt.Caption = "0"
                        WarnCnt.Caption = "0"
                        ShowErrorPanel = False
                        Form_Resize
                        Me.Refresh
                        'ReadAnyFile DataTab, UseFileName.Text
                    Case "SSIM"
                        ShowDetails = True
                        ReadSsimFile
                        SplitSsimFile
                    Case "AODB"
                        ReadFromAodb
                    Case Else
                End Select
                chkWork(Index).Value = 0
            Case 1  'Close
                'StopFileInput = True
                MainDialog.chkAppl(10).Value = 0
                MainDialog.MousePointer = 0
                chkWork(Index).Value = 0
                chkWork(Index).Caption = "Close"
                chkWork(0).Enabled = False
                Me.Refresh
                ShownModal = False
                Me.Hide
            Case 3
                FileIsValid = False
                MainDialog.chkAppl(10).Value = 0
                Me.Hide
                chkWork(Index).Value = 0
            Case 4
                ClearOutCskedErrors
                FileIsValid = True
                MainDialog.chkAppl(10).Value = 0
                Me.Hide
            Case 5
                MsgTxt = ""
                MsgTxt = MsgTxt & "Do you want to reduce the view" & vbNewLine
                MsgTxt = MsgTxt & "to the erroneous lines only ?"
                RetVal = MyMsgBox.CallAskUser(0, 0, 0, "File Consistency Check", MsgTxt, "ask", "Yes,No", UserAnswer)
                Me.Refresh
                If RetVal = 1 Then
                    chkWork(6).Visible = True
                    chkWork(4).Visible = False
                    ShrinkToErrors
                End If
                chkWork(Index).Value = 0
            Case 6
                'MainDialog.SaveLoadedFile Index, DbfReader.DataTab
                chkWork(Index).Value = 0
            Case 7  'Details (SSIM)
                FlightData.Visible = True
                DataTab.Visible = False
                fraImpPanel(1).Visible = True
                fraImpPanel(0).Visible = False
                Me.Refresh
                SplitSsimFile
                chkWork(8).Visible = True
                chkWork(8).Enabled = True
                chkTimes(0).Enabled = True
                chkTimes(1).Enabled = True
                chkTimes(2).Enabled = True
            Case 8
                Screen.MousePointer = 11
                FlightData.HeaderString = SsimFldList
                FlightData.AutoSizeColumns
                Screen.MousePointer = 0
            Case Else
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 7  'Details (SSIM)
                DataTab.Visible = True
                FlightData.Visible = False
                fraImpPanel(0).Visible = True
                fraImpPanel(1).Visible = False
                chkWork(8).Enabled = False
                chkTimes(0).Enabled = False
                chkTimes(1).Enabled = False
                chkTimes(2).Enabled = False
            Case 8
                Screen.MousePointer = 11
                FlightData.HeaderString = SsimHedList
                FlightData.AutoSizeColumns
                Screen.MousePointer = 0
            Case Else
        End Select
    End If
End Sub

Private Sub ShrinkToErrors()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineStatus As Long
    MaxLine = DataTab.GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        LineStatus = DataTab.GetLineStatusValue(CurLine)
        If LineStatus <> 9 Then
            DataTab.DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    DataTab.Refresh
End Sub

Private Sub ErrCnt_Click()
    If chkWork(7).Value = 0 Then
        JumpToErrorLine DataTab, ErrCnt.BackColor
    Else
        JumpToErrorLine FlightData, ErrCnt.BackColor
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    DataTab.HeaderString = "File Content,Remark"
    DataTab.LogicalFieldList = "IMPL,ERRE"
    DataTab.HeaderLengthString = "1000,1000"
    DataTab.ResetContent
    DataTab.RedrawTab
    FlightData.Top = DataTab.Top
    FlightData.Left = DataTab.Left
    fraImpPanel(1).Top = fraImpPanel(0).Top
    fraImpPanel(1).Left = fraImpPanel(0).Left
    chkTimes(2).ToolTipText = "Local Times related to " & HomeAirport
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        chkWork(1).Value = 1
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    Dim LeftVal As Long
    NewVal = Me.ScaleHeight - DataTab.Top - StatusBar1.Height - 3 * Screen.TwipsPerPixelY
    If NewVal > 250 Then DataTab.Height = NewVal
    NewVal = Me.ScaleWidth - (DataTab.Left * 2)
    If NewVal > 250 Then
        If ShowAodbReader Then
            LeftVal = (NewVal - 60) / 2
            NewVal = NewVal - LeftVal
            DataTab.Width = LeftVal
            AodbTab.Left = DataTab.Left + LeftVal + 60
            AodbTab.Width = NewVal - 75
        Else
            DataTab.Width = NewVal
        End If
    End If
    If ShowErrorPanel Then
        ErrorFrame.Visible = True
        UseFileName.Left = ErrorFrame.Left + ErrorFrame.Width + 45
    Else
        ErrorFrame.Visible = False
        UseFileName.Left = DataTab.Left
    End If
    NewVal = Me.ScaleWidth - (UseFileName.Left) - DataTab.Left
    If NewVal > 250 Then UseFileName.Width = NewVal
    NewVal = Me.ScaleWidth - (fraExplain.Left)
    If NewVal > 250 Then fraExplain.Width = NewVal
    FlightData.Width = DataTab.Width
    FlightData.Height = DataTab.Height
    AodbTab.Top = DataTab.Top
    AodbTab.Height = DataTab.Height
    DoEvents
End Sub
Public Sub PreCheckCskedImpFile()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FlightLine As Long
    Dim FlightKey As String
    Dim FlightTime As String
    Dim SectorKey As String
    Dim SectorDepTime As String
    Dim CurSectorNbr As String
    Dim LastSectorNbr As String
    Dim FirstOrig As String
    Dim CurOrig As String
    Dim LastDest As String
    Dim CurDest As String
    Dim tmpLine As String
    Dim FirstChar As String
    Dim HeaderFound As Boolean
    Dim FooterFound As Boolean
    Dim RouteFound As Boolean
    Dim HomeFound As Boolean
    Dim FlightFound As Boolean
    
    DataTab.HeaderFontSize = 17
    DataTab.FontSize = 17
    DataTab.LineHeight = 17
    DataTab.SetTabFontBold True
    DataTab.ShowHorzScroller True
    DataTab.AutoSizeColumns
    DataTab.Refresh
    MaxLine = DataTab.GetLineCount - 1
    FlightKey = ""
    For CurLine = 0 To MaxLine
        DataTab.SetColumnValue CurLine, 1, CStr(CurLine + 1)
        tmpLine = DataTab.GetColumnValue(CurLine, 0)
        FirstChar = Left(tmpLine, 1)
        If Not FooterFound Then
            Select Case FirstChar
                Case "0"    'Header
                    If Not HeaderFound Then
                        HeaderFound = True
                        RouteFound = True
                    Else
                        SetErrorRemark DataTab, CurLine, "Second Header", True, -1
                    End If
                Case "1"    'Flight Line
                    If HeaderFound Then
                        If FlightFound Then
                            If Not RouteFound Then
                                SetErrorRemark DataTab, FlightLine, "No Sectors Found", True, -1
                            ElseIf Not HomeFound Then
                                SetErrorRemark DataTab, FlightLine, "Not connecting " & HomeAirport, True, -1
                            Else
                                If (FirstOrig <> HomeAirport) And (LastDest <> HomeAirport) Then
                                    SetErrorRemark DataTab, FlightLine, "No Arr/Dep at " & HomeAirport, True, -1
                                End If
                            End If
                        End If
                        FlightKey = Mid(tmpLine, 2, 20)
                        FlightTime = Mid(tmpLine, 10, 12)
                        FlightLine = CurLine
                        FlightFound = True
                        RouteFound = False
                        HomeFound = False
                    Else
                        SetErrorRemark DataTab, CurLine, "Garbage before Header", False, -1
                    End If
                Case "2"    'Sector Line
                    If HeaderFound Then
                        If FlightFound Then
                            SectorKey = Mid(tmpLine, 2, 20)
                            If SectorKey = FlightKey Then
                                CurOrig = Trim(Mid(tmpLine, 23, 5))
                                CurDest = Trim(Mid(tmpLine, 28, 5))
                                CurSectorNbr = Mid(tmpLine, 22, 1)
                                SectorDepTime = Mid(tmpLine, 33, 12)
                                If CurOrig = HomeAirport Then HomeFound = True
                                If CurDest = HomeAirport Then HomeFound = True
                                If Not RouteFound Then
                                    FirstOrig = CurOrig
                                    If Val(CurSectorNbr) <> 1 Then
                                        SetErrorRemark DataTab, CurLine, "First sector missing", True, -1
                                    ElseIf SectorDepTime <> FlightTime Then
                                        SetErrorRemark DataTab, CurLine, "Wrong sector Date/Time", True, -1
                                    End If
                                Else
                                    If LastDest = CurOrig Then
                                        If Val(CurSectorNbr) <> (Val(LastSectorNbr) + 1) Then SetErrorRemark DataTab, CurLine, "Missing sector" & Str((Val(LastSectorNbr) + 1)), True, -1
                                    Else
                                        SetErrorRemark DataTab, CurLine, "Wrong Station (" & LastDest & ")", True, -1
                                    End If
                                End If
                                LastDest = CurDest
                                LastSectorNbr = CurSectorNbr
                            Else
                                SetErrorRemark DataTab, CurLine, "Sector of another Flight", True, -1
                            End If
                        Else
                            If Not RouteFound Then
                                SetErrorRemark DataTab, CurLine, "Sector without Flight", True, -1
                                FlightKey = Mid(tmpLine, 2, 20)
                                FlightFound = True
                                FlightLine = -1
                            End If
                        End If
                        RouteFound = True
                    Else
                        SetErrorRemark DataTab, CurLine, "Garbage before Header", False, -1
                    End If
                Case "9"    'Footer
                    If FlightFound Then
                        If Not RouteFound Then
                            SetErrorRemark DataTab, FlightLine, "No Sectors Found", True, -1
                        ElseIf Not HomeFound Then
                            SetErrorRemark DataTab, FlightLine, "Not connecting " & HomeAirport, True, -1
                        Else
                            If (FirstOrig <> HomeAirport) And (LastDest <> HomeAirport) Then
                                SetErrorRemark DataTab, FlightLine, "No Arr/Dep at " & HomeAirport, True, -1
                            End If
                        End If
                    End If
                    FooterFound = True
                Case "*"    'Comment
                    DataTab.SetLineColor CurLine, vbBlack, vbYellow
                Case Else   'Error
                    If HeaderFound Then
                        SetErrorRemark DataTab, CurLine, "Wrong Line Type", True, -1
                    Else
                        SetErrorRemark DataTab, CurLine, "Garbage before Header", False, -1
                    End If
            End Select
        Else
            Select Case FirstChar
                Case "*"
                    DataTab.SetLineColor CurLine, vbBlack, vbYellow
                Case "0"
                    SetErrorRemark DataTab, CurLine, "Second Header", True, -1
                Case "9"
                    SetErrorRemark DataTab, CurLine, "Second Footer", True, -1
                Case Else
                    SetErrorRemark DataTab, CurLine, "Garbage behind Footer", False, -1
            End Select
        End If
    Next
    If Not HeaderFound Then
        DataTab.InsertTextLineAt 0, "?", False
        SetErrorRemark DataTab, 0, "Missing Header Line", True, -1
    End If
    If Not FooterFound Then
        DataTab.InsertTextLine "?", False
        MaxLine = DataTab.GetLineCount - 1
        SetErrorRemark DataTab, MaxLine, "Missing Footer Line", True, -1
    End If
    DataTab.AutoSizeByHeader = True
    DataTab.AutoSizeColumns
    DataTab.Refresh
    tmpLine = DataTab.GetLinesByBackColor(vbRed)
    If tmpLine <> "" Then ErrCnt.Caption = CStr(ItemCount(tmpLine, ","))
    tmpLine = DataTab.GetLinesByBackColor(LightestRed)
    If tmpLine <> "" Then WarnCnt.Caption = CStr(ItemCount(tmpLine, ","))
End Sub

Private Sub ClearOutCskedErrors()
    Dim CurLine As Long
    Dim ClrLine As Long
    Dim EndLine As Long
    Dim MaxLine As Long
    Dim FlightLine As Long
    Dim tmpLine As String
    Dim FirstChar As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim ClearFlight As Boolean
    Dim ClearList As String
    MaxLine = DataTab.GetLineCount - 1
    ClearFlight = False
    FlightLine = -1
    For CurLine = 0 To MaxLine
        tmpLine = DataTab.GetColumnValue(CurLine, 0)
        FirstChar = Left(tmpLine, 1)
        DataTab.GetLineColor CurLine, CurForeColor, CurBackColor
        Select Case CurBackColor
            Case vbYellow, LightestRed
                DataTab.SetColumnValue CurLine, 1, "DEL(0):" & CStr(CurLine)
                DataTab.SetLineStatusValue CurLine, 1
                FirstChar = "xxx"
            Case Else
        End Select
        Select Case FirstChar
            Case "xxx"
            Case "1"
                If (ClearFlight) And (FlightLine >= 0) Then
                    EndLine = CurLine - 1
                    For ClrLine = FlightLine To EndLine
                        DataTab.SetColumnValue ClrLine, 1, "DEL(1):" & CStr(CurLine)
                        DataTab.SetLineStatusValue ClrLine, 1
                    Next
                    ClearFlight = False
                    FlightLine = -1
                End If
                FlightLine = CurLine
                If CurBackColor = vbRed Then ClearFlight = True
            Case "2"
                If CurBackColor = vbRed Then ClearFlight = True
                If FlightLine < 0 Then FlightLine = CurLine
            Case "9"
                If (ClearFlight) And (FlightLine >= 0) Then
                    EndLine = CurLine - 1
                    For ClrLine = FlightLine To EndLine
                        DataTab.SetColumnValue ClrLine, 1, "DEL(1):" & CStr(CurLine)
                        DataTab.SetLineStatusValue ClrLine, 1
                    Next
                    ClearFlight = False
                    FlightLine = -1
                End If
                If CurBackColor = vbRed Then
                    DataTab.SetColumnValue CurLine, 1, "DEL(3):" & CStr(CurLine)
                    DataTab.SetLineStatusValue CurLine, 1
                End If
            Case Else
                Select Case CurBackColor
                    Case vbRed
                        If (FirstChar <> "1") And (FirstChar <> "2") Then
                            DataTab.SetColumnValue CurLine, 1, "DEL(2):" & CStr(CurLine)
                            DataTab.SetLineStatusValue CurLine, 1
                        End If
                    Case Else
                End Select
        End Select
    Next
    If (ClearFlight) And (FlightLine >= 0) Then
        For ClrLine = FlightLine To MaxLine
            DataTab.SetColumnValue ClrLine, 1, "DEL(4):" & CStr(CurLine)
            DataTab.SetLineStatusValue ClrLine, 1
        Next
    End If
    DataTab.Refresh
    HelperTab.ResetContent
    ClearList = DataTab.GetLinesByStatusValue(1, 0)
    HelperTab.InsertBuffer ClearList, ","
    MaxLine = HelperTab.GetLineCount - 1
    For CurLine = MaxLine To 0 Step -1
        ClrLine = Val(HelperTab.GetColumnValue(CurLine, 0))
        DataTab.DeleteLine ClrLine
    Next
    DataTab.Refresh
    HelperTab.ResetContent
End Sub

Private Sub SetErrorRemark(CurTab As TABLib.Tab, LineNo As Long, ErrText As String, IsFatal As Boolean, ErrColor As Long)
    Dim CurErrTxt As String
    Dim UseBackColor As Long
    Dim UseTextColor As Long
    UseBackColor = ErrColor
    UseTextColor = vbBlack
    If UseBackColor < 0 Then
        If IsFatal Then
            UseBackColor = vbRed
            UseTextColor = vbWhite
        Else
            UseBackColor = LightestRed
        End If
    End If
    If LineNo >= 0 Then
        CurErrTxt = CurTab.GetFieldValue(LineNo, "ERRE")
        If CurErrTxt <> "" Then CurErrTxt = CurErrTxt & "|"
        CurErrTxt = CurErrTxt & ErrText
        CurTab.SetFieldValues LineNo, "ERRE", CurErrTxt
        CurTab.SetLineColor LineNo, UseTextColor, UseBackColor
        CurTab.SetLineStatusValue LineNo, 9
    End If
End Sub

Public Function CheckErrorsModal(MyParent As Form) As Boolean
    FileIsValid = True
    'ErrCnt.Caption = "3"
    If (Val(ErrCnt.Caption) + Val(WarnCnt.Caption)) > 0 Then
        chkWork(1).Visible = False
        ErrorFrame.Top = -90
        FileIsValid = False
        ShowErrorPanel = True
        If ShowDetails Then
            chkWork(7).Left = ErrorFrame.Left + ErrorFrame.Width + 30
            chkWork(8).Left = chkWork(7).Left + chkWork(7).Width + 15
            fraImpPanel(0).Left = chkWork(8).Left + chkWork(8).Width + 30
            fraImpPanel(1).Left = fraImpPanel(0).Left
        End If
        Form_Resize
        ErrorFrame.ZOrder
        Me.Show vbModal
    End If
    MainDialog.chkAppl(4).Width = 645
    MainDialog.chkAppl(10).Visible = True
    DoEvents
    CheckErrorsModal = FileIsValid
End Function
Private Sub JumpToErrorLine(CurTab As TABLib.Tab, ErrColor As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    CurLine = CurTab.GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = CurTab.GetLineCount - 1
    While CurLine <= MaxLine
        CurLine = CurLine + 1
        CurTab.GetLineColor CurLine, ForeColor, BackColor
        If BackColor = ErrColor Then
            If CurLine > 10 Then CurScroll = CurLine - 10 Else CurScroll = 0
            CurTab.OnVScrollTo CurScroll
            CurTab.SetCurrentSelection CurLine
            CurLine = MaxLine + 1
        End If
    Wend
End Sub

Private Sub lblImpCnt_Click(Index As Integer)
    If chkWork(7).Value = 0 Then
        JumpToErrorLine DataTab, lblImpCnt(Index).BackColor
    Else
        JumpToErrorLine FlightData, lblImpCnt(Index).BackColor
    End If
End Sub

Private Sub txtVpfr_Change(Index As Integer)
    If Not CheckDateInput(False) Then DoNothing
End Sub

Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_Change(Index As Integer)
    If Not CheckDateInput(False) Then DoNothing
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub WarnCnt_Click()
    If chkWork(7).Value = 0 Then
        JumpToErrorLine DataTab, WarnCnt.BackColor
    Else
        JumpToErrorLine FlightData, WarnCnt.BackColor
    End If
End Sub

Public Sub OpenSsimFile(CurFileName As String, MyParent As Form, ShowMe As Boolean)
    UseFileName.Text = CurFileName
    DataTab.HeaderString = "File Content,Remark"
    DataTab.LogicalFieldList = "IMPL,ERRE"
    If ShowMe Then
        Me.Top = MyParent.Top + 150 * Screen.TwipsPerPixelY
        Me.Left = MyParent.Left + 150 * Screen.TwipsPerPixelX
        Me.Show , MyParent
    End If
    chkWork(0).Tag = "SSIM"
    chkWork(2).Value = 0
    chkWork(0).Value = 1
End Sub
Private Sub ReadSsimFile()
    Dim CurFileName As String
    Dim CurFileNbr As Integer
    Dim LineText As String
    Dim LineCount As Long
    Dim CurItm As Long
    Dim NewRec As String
    Dim i As Integer
    'StopFileInput = False
    chkWork(7).Value = 0
    For i = 0 To 13
        lblImpCnt(i).Caption = ""
    Next
    
    SsimFldList = ""
    SsimFldList = SsimFldList & "SSIM,FLNS,ALDS,FLTN,ITVI,LSQN,STYP,VPFR,VPTO,FRQD,FRQW,"
    SsimFldList = SsimFldList & "ORG3,STDP,STOD,UTVO,PTDS,DES3,STOA,STAP,UTVD,PTAS,ACT3,"
    SsimFldList = SsimFldList & "PRBD,PRBM,MSVN,JOAD,DMM1,ITVO,OWNE,COCE,CACE,"
    SsimFldList = SsimFldList & "OFAD,OFNO,OFAR,OFNS,NOLM,FTSL,CSAD,TRRC,TRRO,DMM2,ACCV,BINF,RSNO,"
    SsimFldList = SsimFldList & "ERRE,IPFR,IPTO,SEAS,UTVH"
    SsimLenList = ""
    SsimLenList = SsimLenList & "1,1,3,4,2,2,1,7,7,7,1,3,4,4,5,2,3,4,4,5,2,3,"
    SsimLenList = SsimLenList & "20,5,10,9,8,1,3,3,3,"
    SsimLenList = SsimLenList & "3,4,1,1,1,1,1,11,1,11,20,2,6,"
    SsimLenList = SsimLenList & "100,1,1,6,5"
    SsimHedList = ""
    SsimHedList = SsimHedList & "T,S,AL,FLT,IV,LN,S, Begin,  End,OP Days,W,"
    SsimHedList = SsimHedList & "ORG,STDP,STOD,Diff.,D,DES,STOA,STAP,Diff.,A,A/C,"
    SsimHedList = SsimHedList & "Res.Booking,Mod,Meal,AD,,I,O,Crew,Cabin,"
    SsimHedList = SsimHedList & "AD,FN,AR,S,N,T,S,Tr.R.,O,,Version,Inf,LineNo,"
    SsimHedList = SsimHedList & "Remarks,I,I,SC,UTDHP"
    FlightData.ResetContent
    FlightData.HeaderString = SsimHedList
    FlightData.LogicalFieldList = SsimFldList
    FlightData.HeaderLengthString = SsimLenList
    FlightData.ColumnWidthString = SsimLenList
    FlightData.FontName = "Courier New"
    FlightData.HeaderFontSize = 17
    FlightData.FontSize = 17
    FlightData.LineHeight = 17
    FlightData.GridlineColor = vbBlack
    FlightData.SetTabFontBold True
    FlightData.ShowHorzScroller True
    FlightData.AutoSizeByHeader = True
    FlightData.AutoSizeColumns
    
    CurFileName = UseFileName.Text
    CurFileNbr = FreeFile
    DataTab.ResetContent
    DataTab.SetFieldSeparator ","
    DataTab.HeaderString = "File Content && Consistency Check,Remarks"
    DataTab.LogicalFieldList = "IMPL,ERRE"
    DataTab.HeaderLengthString = "1000,1000"
    DataTab.ColumnWidthString = "1000,1000"
    DataTab.FontName = "Courier New"
    DataTab.SetFieldSeparator Chr(15)
    DataTab.HeaderFontSize = 17
    DataTab.FontSize = 17
    DataTab.LineHeight = 17
    DataTab.SetTabFontBold True
    DataTab.ShowVertScroller True
    DataTab.ShowHorzScroller True
    DataTab.ResetContent
    DataTab.Refresh
    chkWork(3).Value = 0
    chkWork(4).Value = 0
    ErrCnt.Caption = "0"
    WarnCnt.Caption = "0"
    ShowErrorPanel = False
    Form_Resize
    StatusBar1.Panels(2) = "Reading File Data ..."
    Me.Refresh
    Open CurFileName For Input As #CurFileNbr
    While (Not EOF(CurFileNbr)) 'And (Not StopFileInput)
        Line Input #CurFileNbr, LineText
        DataTab.InsertBuffer LineText, vbLf
        LineCount = LineCount + 1
        If LineCount Mod 500 = 0 Then
            DataTab.OnVScrollTo LineCount
            DataTab.Refresh
            StatusBar1.Panels(1) = CStr(LineCount)
            DoEvents
        End If
    Wend
    Close CurFileNbr
    'If StopFileInput Then
    '    DataTab.ResetContent
    'End If
    DataTab.OnVScrollTo 0
    DataTab.AutoSizeByHeader = True
    DataTab.AutoSizeColumns
    DataTab.Refresh
    Me.Refresh
    DoEvents
End Sub

Public Sub PreCheckSsimImpFile()
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim LineCnt As Long
    Dim ValidCnt As Long
    Dim CurLegNo As Long
    Dim PrvLegNo As Long
    Dim FltLineBgn As Long
    Dim FltLineCur As Long
    Dim FltLineEnd As Long
    Dim FlightFound As Boolean
    Dim FlightClosed As Boolean
    Dim FlightError As Boolean
    Dim FlightType As Integer
    Dim ChkType As Integer
    Dim LineData As String
    Dim tmpLine As String
    Dim tmpFT As String
    Dim tmpLinNbr As String
    Dim TimeMode As String
    Dim CurFlno As String
    Dim CurLegn As String
    Dim CurOrg3 As String
    Dim CurDes3 As String
    Dim PrvFlno As String
    Dim PrvLegn As String
    Dim PrvOrg3 As String
    Dim PrvDes3 As String
    Dim AllowRotation As Boolean
    Dim CheckLegNo As Boolean
    Dim i As Integer
    
    StatusBar1.Panels(2) = "Pre-Check Flight Data ..."
    DataTab.OnVScrollTo 0
    ValidCnt = 0
    FlightClosed = True
    FlightFound = False
    FlightError = False
    FlightType = 0
    AllowRotation = False
    TimeMode = "?"
    MaxLine = DataTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        CheckLegNo = True
        LineData = DataTab.GetColumnValue(CurLine, 0)
        tmpFT = Left(LineData, 1)
        If tmpFT <> "0" Then
            tmpLinNbr = Mid(LineData, 195, 6)
            LineCnt = Val(tmpLinNbr)
            ValidCnt = ValidCnt + 1
            If ValidCnt <> LineCnt Then
                ValidCnt = LineCnt
                SetErrorRemark DataTab, CurLine, "LINE NBR", True, -1
            End If
        End If
        Select Case tmpFT
            Case 0
                DataTab.SetLineColor CurLine, vbBlack, LightBlue
            Case 1
                DataTab.SetLineColor CurLine, vbWhite, vbBlue
            Case 2
                DataTab.SetLineColor CurLine, vbBlack, vbCyan
                TimeMode = Mid(LineData, 2, 1)
                'DataTimeMode = "-"
                'Select Case TimeMode
                '    Case "L"
                '        chkTimes(1).Value = 1
                '        FileTimeMode = "L"
                '    Case "U", "G"
                '        chkTimes(0).Value = 1
                '        FileTimeMode = "U"
                '    Case Else
                '        FileTimeMode = "?"
                'End Select
                'SsimSchedBegin = Mid(LineData, 15, 7)
                'SsimSchedEnd = Mid(LineData, 22, 7)
            Case 3
                CurFlno = Mid(LineData, 2, 10)
                CurLegn = Mid(LineData, 12, 2)
                CurOrg3 = Mid(LineData, 37, 3)
                CurDes3 = Mid(LineData, 55, 3)
                CurLegNo = Val(CurLegn)
                If FlightFound Then
                    If PrvFlno = CurFlno Then
                        If FlightType = 0 Then DataTab.SetLineColor CurLine, vbBlack, LightYellow
                        If FlightType = 2 Then DataTab.SetLineColor CurLine, vbBlack, LightestGreen
                        If FlightError Then DataTab.SetLineColor CurLine, vbBlack, LightestRed
                        PrvLegNo = PrvLegNo + 1
                        If CurLegNo <> PrvLegNo Then
                            SetErrorRemark DataTab, CurLine, "LEG " & CStr(PrvLegNo), True, -1
                            FlightError = True
                        End If
                        If CurOrg3 <> PrvDes3 Then
                            SetErrorRemark DataTab, CurLine, PrvDes3 & "-" & CurOrg3, True, -1
                            FlightError = True
                        End If
                        ChkType = FlightType
                        Select Case ChkType
                            Case 0  'Arrival not yet found
                                If CurDes3 = HomeAirport Then
                                    FlightType = FlightType + 1
                                    If Not FlightError Then DataTab.SetLineColor CurLine, vbBlack, vbYellow
                                End If
                            Case 1 'Arrival already found
                                If CurOrg3 = HomeAirport Then
                                    If AllowRotation Then
                                        FlightType = FlightType + 2
                                        If Not FlightError Then DataTab.SetLineColor FltLineBgn, vbBlack, vbWhite
                                    Else
                                        CheckLegNo = False
                                        FlightFound = False
                                        FlightError = False
                                        FlightType = 0
                                        FltLineBgn = -1
                                        FltLineEnd = -1
                                    End If
                                End If
                            Case 2  'Single Departure
                            Case 3  'Rotation via HOPO
                        End Select
                        FltLineEnd = CurLine
                    Else
                        Select Case FlightType
                            Case 0  'Not connected to HOPO
                                For FltLineCur = FltLineBgn To FltLineEnd
                                    SetErrorRemark DataTab, FltLineCur, HomeAirport & "?", False, LightGrey
                                Next
                            Case 1  'Single Arrival
                            Case 2  'Single Departure
                            Case 3  'Rotation via HOPO
                            Case Else
                        End Select
                        FlightFound = False
                        FlightError = False
                        FlightType = 0
                        FltLineBgn = -1
                        FltLineEnd = -1
                    End If
                End If
                If Not FlightFound Then
                    FltLineBgn = CurLine
                    If (CheckLegNo) And (CurLegNo <> 1) Then
                        SetErrorRemark DataTab, CurLine, "LEG 1", True, -1
                        FlightError = True
                    End If
                    DataTab.SetLineColor CurLine, vbBlack, LightYellow
                    If CurDes3 = HomeAirport Then
                        FlightType = FlightType + 1
                        If Not FlightError Then DataTab.SetLineColor CurLine, vbBlack, vbYellow
                    End If
                    If CurOrg3 = HomeAirport Then
                        FlightType = FlightType + 2
                        If Not FlightError Then DataTab.SetLineColor CurLine, vbBlack, vbGreen
                    End If
                    If FlightType = 3 Then
                        SetErrorRemark DataTab, CurLine, CurOrg3 & "-" & CurDes3, True, -1
                        FlightError = True
                    End If
                    FltLineEnd = CurLine
                    FlightFound = True
                End If
                PrvLegNo = CurLegNo
                PrvFlno = CurFlno
                PrvOrg3 = CurOrg3
                PrvDes3 = CurDes3
            Case 4
                DataTab.SetLineColor CurLine, vbBlack, vbMagenta
            Case 5
                DataTab.SetLineColor CurLine, vbBlack, vbCyan
            Case Else
        End Select
    Next
    If FlightFound Then
        Select Case FlightType
            Case 0  'Not connected
                For FltLineCur = FltLineBgn To FltLineEnd
                    SetErrorRemark DataTab, FltLineCur, HomeAirport & "?", False, LightGrey
                Next
            Case 1  'Arrival
            Case 2  'Departure
            Case 3
            Case Else
        End Select
    End If
    DataTab.ShowHorzScroller True
    DataTab.AutoSizeByHeader = True
    DataTab.AutoSizeColumns
    DataTab.RedrawTab
    DataTab.SetInternalLineBuffer True
    tmpLine = DataTab.GetLinesByBackColor(vbRed)
    If Val(tmpLine) > 0 Then ErrCnt.Caption = tmpLine
    tmpLine = DataTab.GetLinesByBackColor(LightestRed)
    If Val(tmpLine) > 0 Then WarnCnt.Caption = tmpLine
    For i = 0 To 6
        lblImpCnt(i).Caption = Trim(DataTab.GetLinesByBackColor(lblImpCnt(i).BackColor))
    Next
    DataTab.SetInternalLineBuffer False
    chkWork(7).Value = 1
    chkWork(7).Value = 0
    chkWork(7).Enabled = True
    DoEvents
End Sub

Private Sub SplitSsimFile()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim tmpLine As String
    Dim fltLine As String
    Dim tmpSsim As String
    Dim LenList As String
    Dim tmpLen As String
    Dim tmpData As String
    Dim NewData As String
    Dim tmpSeas As String
    Dim tmpNolm As String
    Dim tmpCsad As String
    Dim tmpVpfr As String
    Dim NewVpfr As String
    Dim tmpVpto As String
    Dim NewVpto As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpUtvo As String
    Dim tmpUtvd As String
    Dim tmpUtcd As String
    Dim MaxCol As Long
    Dim CurCol As Long
    Dim ChrPos As Long
    Dim FldLen As Long
    Dim CurTextColor As Long
    Dim CurBackColor As Long
    Dim CurLineCount As Long
    Dim i As Integer
    LenList = FlightData.ColumnWidthString
    MaxCol = ItemCount(LenList, ",") - 6
    MaxLine = FlightData.GetLineCount
    CurLineCount = 0
    If MaxLine <= 0 Then
        Screen.MousePointer = 11
        fraImpPanel(0).Visible = False
        fraImpPanel(1).Visible = False
        StatusBar1.Panels(2) = "Split Flight Data ..."
        Me.Refresh
        MaxLine = DataTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpLine = DataTab.GetColumnValue(CurLine, 0)
            tmpSsim = Mid(tmpLine, 1, 1)
            Select Case tmpSsim
                Case 0
                Case 1
                Case 2
                Case 3
                    DataTab.GetLineColor CurLine, CurTextColor, CurBackColor
                    If (CurBackColor <> vbRed) And (CurBackColor <> LightestRed) Then
                        fltLine = ""
                        ChrPos = 1
                        For CurCol = 0 To MaxCol
                            tmpLen = GetRealItem(LenList, CurCol, ",")
                            FldLen = Val(tmpLen)
                            tmpData = Trim(Mid(tmpLine, ChrPos, FldLen))
                            fltLine = fltLine & tmpData & ","
                            ChrPos = ChrPos + FldLen
                        Next
                        fltLine = fltLine & DataTab.GetColumnValue(CurLine, 1) & ",,,,"
                        FlightData.InsertTextLine fltLine, False
                        FlightData.SetLineColor CurLineCount, CurTextColor, CurBackColor
                        CurLineCount = CurLineCount + 1
                        If CurLine Mod 200 = 0 Then
                            If CurLine = 200 Then
                                FlightData.AutoSizeColumns
                            End If
                            FlightData.OnVScrollTo CurLine
                            FlightData.Refresh
                            Me.Refresh
                            DoEvents
                        End If
                    End If
                Case 4
                Case 5
                Case Else
            End Select
        Next
        FlightData.OnVScrollTo 0
        FlightData.AutoSizeColumns
        FlightData.Refresh
        StatusBar1.Panels(2) = "Check Operational Legs ..."
        MaxLine = FlightData.GetLineCount - 1
        For CurLine = 0 To MaxLine
            If CurLine Mod 200 = 0 Then
                FlightData.OnVScrollTo CurLine
                FlightData.Refresh
                Me.Refresh
                DoEvents
            End If
            tmpNolm = Trim(FlightData.GetFieldValue(CurLine, "NOLM"))
            If tmpNolm <> "" Then SetErrorRemark FlightData, CurLine, "NOP.LEG", False, lblImpCnt(6).BackColor
            tmpCsad = Trim(FlightData.GetFieldValue(CurLine, "CSAD"))
            If (tmpCsad <> "") And (tmpCsad <> "X") Then
                SetErrorRemark FlightData, CurLine, "CODE SHARE", False, lblImpCnt(6).BackColor
            End If
        Next
        FlightData.OnVScrollTo 0
        FlightData.AutoSizeColumns
        FlightData.Refresh
        DoEvents
        FlightData.SetInternalLineBuffer True
        For i = 7 To 13
            lblImpCnt(i).Caption = Trim(FlightData.GetLinesByBackColor(lblImpCnt(i).BackColor))
        Next
        FlightData.SetInternalLineBuffer False
        
        StatusBar1.Panels(2) = "Check Operational Periods ..."
        MaxLine = FlightData.GetLineCount - 1
        For CurLine = 0 To MaxLine
            If CurLine Mod 200 = 0 Then
                FlightData.OnVScrollTo CurLine
                FlightData.Refresh
                Me.Refresh
                DoEvents
            End If
            tmpVpfr = Trim(FlightData.GetFieldValue(CurLine, "VPFR"))
            tmpVpto = Trim(FlightData.GetFieldValue(CurLine, "VPTO"))
            If tmpVpfr = "00XXX00" Then
                'NewVpfr = SsimSchedBegin & ",?"
                'FlightData.SetFieldValues CurLine, "VPFR,IPFR", NewVpfr
            End If
            If tmpVpto = "00XXX00" Then
                'NewVpto = SsimSchedEnd
                'If NewVpto = "00XXX00" Then
                '    tmpVpfr = DecodeSsimDayFormat(tmpVpfr, "SSIM2", "CEDA")
                '    tmpData = SeasonData.GetValidSeason(tmpVpfr)
                '    NewVpto = GetItem(tmpData, 3, ",")
                'End If
                'If NewVpto = "" Then
                '    NewVpto = "00XXX00,?"
                'Else
                '    NewVpto = NewVpto & ",X"
                'End If
                'FlightData.SetFieldValues CurLine, "VPTO,IPTO", NewVpto
            End If
        Next
        FlightData.OnVScrollTo 0
        FlightData.AutoSizeColumns
        FlightData.Refresh
        StatusBar1.Panels(2) = ""
        Me.Refresh
        DoEvents
        
        'If ImportHopoLocal Then
            StatusBar1.Panels(2) = "Check " & HomeAirport & " UTC Time Variations ..."
            MaxLine = FlightData.GetLineCount - 1
            For CurLine = 0 To MaxLine
                If CurLine Mod 200 = 0 Then
                    FlightData.OnVScrollTo CurLine
                    FlightData.Refresh
                    Me.Refresh
                    DoEvents
                End If
                tmpOrg3 = FlightData.GetFieldValue(CurLine, "ORG3")
                tmpDes3 = FlightData.GetFieldValue(CurLine, "DES3")
                tmpUtcd = ""
                If tmpOrg3 = HomeAirport Then
                    tmpVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                    tmpUtvo = FlightData.GetFieldValue(CurLine, "UTVO")
                    tmpUtcd = tmpUtvo
                End If
                If tmpDes3 = HomeAirport Then
                    tmpVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                    tmpUtvd = FlightData.GetFieldValue(CurLine, "UTVD")
                    tmpUtcd = tmpUtvd
                End If
                If tmpUtcd <> "" Then
                    If tmpVpfr <> "00XXX00" Then
                        NewVpfr = DecodeSsimDayFormat(tmpVpfr, "SSIM2", "CEDA")
                        'tmpData = SeasonData.GetValidSeason(NewVpfr)
                        'NewData = GetItem(tmpData, 2, vbLf)
                        'If NewData <> "" Then
                        '    LineNo = Val(NewData)
                        '    SeasonData.SeaList(0).SetColumnValue LineNo, 7, tmpUtcd
                        'End If
                    End If
                End If
            Next
            FlightData.OnVScrollTo 0
            FlightData.AutoSizeColumns
            FlightData.Refresh
            StatusBar1.Panels(2) = ""
            Me.Refresh
            DoEvents
            'SeasonData.SeaList(0).AutoSizeColumns
            
            StatusBar1.Panels(2) = "Set all UTC Time Variations to " & HomeAirport & " LT ..."
            MaxLine = FlightData.GetLineCount - 1
            For CurLine = 0 To MaxLine
                If CurLine Mod 200 = 0 Then
                    FlightData.OnVScrollTo CurLine
                    FlightData.Refresh
                    Me.Refresh
                    DoEvents
                End If
                tmpUtcd = ""
                tmpVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                If tmpVpfr <> "00XXX00" Then
                    NewVpfr = DecodeSsimDayFormat(tmpVpfr, "SSIM2", "CEDA")
                    'tmpData = SeasonData.GetValidSeason(NewVpfr)
                    'tmpUtcd = GetItem(tmpData, 8, ",")
                    'If tmpUtcd = "" Then tmpUtcd = "????"
                    'NewData = GetItem(tmpData, 1, ",") & "," & tmpUtcd
                    'FlightData.SetFieldValues CurLine, "SEAS,UTVH", NewData
                End If
            Next
            FlightData.OnVScrollTo 0
            FlightData.AutoSizeColumns
            FlightData.Refresh
            StatusBar1.Panels(2) = ""
            Me.Refresh
            DoEvents
        'End If
        
        Screen.MousePointer = 0
        
        CollectSsimFlights
    End If
End Sub

Private Sub CollectSsimFlights()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurTextColor As Long
    Dim CurBackColor As Long
    Dim CurAdid As String
    Dim CurVpfr As String
    Dim CurVpto As String
    Dim CurFrqd As String
    Dim CurStod As String
    Dim CurStoa As String
    Dim CurOrg3 As String
    Dim CurDes3 As String
    Dim fltOper As String
    Dim fltData As String
    Dim fltRout As String
    Dim fltAirc As String
    Dim fltAppd As String
    Dim LineTag As String
    Dim fltViaCnt As Integer
    Dim FltType As Integer
    Dim FltBgnLine As Long
    Dim FltEndLine As Long
    Dim IsValid As Boolean
    
    'ImportHopoLocal = False
    'If Not ImportHopoLocal Then
    '    chkTimes(0).Value = 1
    'Else
    '    chkTimes(2).Value = 1
    'End If
    
    Screen.MousePointer = 11
    StatusBar1.Panels(2) = "Collect Flight Routing ..."
    MaxLine = FlightData.GetLineCount - 1
    FltBgnLine = -1
    FltEndLine = -1
    FltType = 0
    fltViaCnt = 0
    For CurLine = 0 To MaxLine
        If CurLine Mod 200 = 0 Then
            FlightData.OnVScrollTo CurLine
            FlightData.Refresh
            Me.Refresh
            DoEvents
        End If
        CurOrg3 = FlightData.GetFieldValue(CurLine, "ORG3")
        CurDes3 = FlightData.GetFieldValue(CurLine, "DES3")
        CurStod = FlightData.GetFieldValue(CurLine, "STOD")
        CurStoa = FlightData.GetFieldValue(CurLine, "STOA")
        FlightData.GetLineColor CurLine, CurTextColor, CurBackColor
        Select Case CurBackColor
            Case lblImpCnt(13).BackColor    '(vbGreen) Begin of Departure Flight
                Select Case FltType
                    Case 1  'Arrival (Error)
                    Case 2  'Save Departure
                        fltRout = CStr(fltViaCnt) & "," & fltRout
                        LineTag = CurAdid & ";" & fltOper & ";" & fltData & ";" & fltAirc & ";" & fltRout & ";" & fltAppd
                        FlightData.SetLineTag FltBgnLine, LineTag
                    Case Else
                End Select
                FltType = 2
                CurAdid = "D"
                CurVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                CurVpfr = DecodeSsimDayFormat(CurVpfr, "SSIM2", "CEDA")
                CurVpto = FlightData.GetFieldValue(CurLine, "VPTO")
                CurVpto = DecodeSsimDayFormat(CurVpto, "SSIM2", "CEDA")
                CurFrqd = FlightData.GetFieldValue(CurLine, "FRQD")
                CurFrqd = FormatFrequency(CurFrqd, "123", IsValid)
                fltOper = CurVpfr & "," & CurVpto & ","
                fltOper = fltOper & CurFrqd & ","
                fltOper = fltOper & FlightData.GetFieldValues(CurLine, "FRQW")
                fltData = FlightData.GetFieldValues(CurLine, "STYP,ALDS,FLTN")
                fltData = fltData & FlightData.GetFieldValue(CurLine, "FLNS") & ","
                fltData = fltData & CurStod & ",,"
                fltAirc = FlightData.GetFieldValue(CurLine, "ACT3") & ",,"
                fltRout = CurOrg3 & ":-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                fltAppd = CurVpfr & "," & CurFrqd & "," & FlightData.GetFieldValues(CurLine, "ACCV")
                fltViaCnt = 2
                FltBgnLine = CurLine
                FltEndLine = CurLine
            Case lblImpCnt(12).BackColor      '(LightGreen) Via Stations of Departure
                Select Case FltType
                    Case 0  'Error
                    Case 1  'Arrival (Error)
                    Case 2  'Departure
                        fltRout = fltRout & "-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                        fltViaCnt = fltViaCnt + 1
                        FltEndLine = CurLine
                    Case Else
                End Select
            Case lblImpCnt(10).BackColor    '(LightestYellow) Begin of Arrival Flight
                Select Case FltType
                    Case 2  'Save Departure
                        fltRout = CStr(fltViaCnt) & "," & fltRout
                        LineTag = CurAdid & ";" & fltOper & ";" & fltData & ";" & fltAirc & ";" & fltRout & ";" & fltAppd
                        FlightData.SetLineTag FltBgnLine, LineTag
                        FltType = 0
                    Case Else
                End Select
                Select Case FltType
                    Case 0  'First Arrival Leg
                        CurAdid = "A"
                        fltOper = ""
                        fltData = ""
                        fltRout = CurOrg3 & ":-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                        CurVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                        CurVpfr = DecodeSsimDayFormat(CurVpfr, "SSIM2", "CEDA")
                        CurVpto = FlightData.GetFieldValue(CurLine, "VPTO")
                        CurVpto = DecodeSsimDayFormat(CurVpto, "SSIM2", "CEDA")
                        CurFrqd = FlightData.GetFieldValue(CurLine, "FRQD")
                        CurFrqd = FormatFrequency(CurFrqd, "123", IsValid)
                        fltAppd = CurVpfr & "," & CurFrqd & ","
                        fltViaCnt = 2
                        FltBgnLine = CurLine
                    Case 1  'MultiLeg Arrival
                        fltRout = fltRout & "-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                        fltViaCnt = fltViaCnt + 1
                End Select
                FltEndLine = CurLine
                FltType = 1
            Case lblImpCnt(11).BackColor    '(vbYellow) End of Arrival Flight
                Select Case FltType
                    Case 2  'Save Departure
                        fltRout = CStr(fltViaCnt) & "," & fltRout
                        LineTag = CurAdid & ";" & fltOper & ";" & fltData & ";" & fltAirc & ";" & fltRout & ";" & fltAppd
                        FlightData.SetLineTag FltBgnLine, LineTag
                        FltType = 0
                    Case Else
                End Select
                CurVpfr = FlightData.GetFieldValue(CurLine, "VPFR")
                CurVpfr = DecodeSsimDayFormat(CurVpfr, "SSIM2", "CEDA")
                CurVpto = FlightData.GetFieldValue(CurLine, "VPTO")
                CurVpto = DecodeSsimDayFormat(CurVpto, "SSIM2", "CEDA")
                CurFrqd = FlightData.GetFieldValue(CurLine, "FRQD")
                CurFrqd = FormatFrequency(CurFrqd, "123", IsValid)
                Select Case FltType
                    Case 0  'First Arrival Leg
                        CurAdid = "A"
                        fltRout = CurOrg3 & ":-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                        fltAppd = CurVpfr & "," & CurFrqd & "," & FlightData.GetFieldValues(CurLine, "ACCV")
                        fltViaCnt = 2
                    Case 1  'Last Arrival Leg
                        fltRout = fltRout & "-" & CurStod & "|" & CurDes3 & ":" & CurStoa
                        fltViaCnt = fltViaCnt + 1
                        fltAppd = fltAppd & FlightData.GetFieldValues(CurLine, "ACCV")
                    Case Else
                End Select
                If CurStoa <= CurStod Then
                    ShiftFltPeriod CurVpfr, CurVpto, CurFrqd
                End If
                fltOper = CurVpfr & "," & CurVpto & ","
                fltOper = fltOper & CurFrqd & ","
                fltOper = fltOper & FlightData.GetFieldValues(CurLine, "FRQW")
                fltData = FlightData.GetFieldValues(CurLine, "STYP,ALDS,FLTN")
                fltData = fltData & FlightData.GetFieldValue(CurLine, "FLNS") & ","
                fltData = fltData & CurStoa
                fltAirc = FlightData.GetFieldValue(CurLine, "ACT3") & ",,"
                fltRout = CStr(fltViaCnt) & "," & fltRout
                LineTag = CurAdid & ";" & fltOper & ";" & fltData & ";" & fltAirc & ";" & fltRout & ";" & fltAppd
                FlightData.SetLineTag CurLine, LineTag
                FltType = 0
                FltBgnLine = -1
                FltEndLine = -1
            Case Else
        End Select
    Next
    
    Select Case FltType
        Case 1  'Arrival (Error)
        Case 2  'Save Departure
            fltRout = CStr(fltViaCnt) & "," & fltRout
            LineTag = CurAdid & ";" & fltOper & ";" & fltData & ";" & fltAirc & ";" & fltRout & ";" & fltAppd
            FlightData.SetLineTag FltBgnLine, LineTag
        Case Else
    End Select
    
    FlightData.OnVScrollTo 0
    FlightData.Refresh
    StatusBar1.Panels(2) = ""
    Screen.MousePointer = 0
    DoEvents
End Sub
Private Sub ShiftFltPeriod(AftVpfr As String, AftVpto As String, AftFrqd As String)
    If AftVpfr <> "00XXX00" Then AftVpfr = CedaDateAdd(AftVpfr, 1)
    If AftVpto <> "00XXX00" Then AftVpto = CedaDateAdd(AftVpto, 1)
    AftFrqd = ShiftFrqd(AftFrqd, 1)
End Sub

Public Sub ReadAodbData()
    UseFileName.Visible = False
    fraTimeFilter.Top = 360
    fraTimeFilter.Left = 30
    fraExplain.Top = fraTimeFilter.Top
    fraExplain.Left = fraTimeFilter.Left + fraTimeFilter.Width + 60
    fraTimeFilter.Visible = True
    fraExplain.Visible = True
    DataTab.Top = fraTimeFilter.Top + fraTimeFilter.Height + 60
    DataTab.ResetContent
    DataTab.SetFieldSeparator ","
    DataTab.HeaderString = "TR.ID,SCORE Telex Lines,URNO,TIME"
    DataTab.LogicalFieldList = ""
    DataTab.HeaderLengthString = "80,1000,100,1000"
    DataTab.ColumnWidthString = "10,1000,10,1000"
    DataTab.FontName = "Courier New"
    DataTab.HeaderFontSize = 17
    DataTab.FontSize = 17
    DataTab.LineHeight = 17
    DataTab.SetTabFontBold False
    
    DataTab.SetMainHeaderValues "2,2", "Extracted SCORE Transaction,System", "12632256,12632256"
    DataTab.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    DataTab.MainHeader = True
    
    DataTab.LifeStyle = True
    DataTab.ResetContent
    DataTab.Refresh
    chkWork(0).Tag = "SSIM"
    ShowAodbReader = True
    
    AodbTab.ResetContent
    AodbTab.SetFieldSeparator ","
    AodbTab.HeaderString = "AODB SCORE Telexes"
    AodbTab.LogicalFieldList = ""
    AodbTab.HeaderLengthString = "1000"
    AodbTab.ColumnWidthString = "10"
    AodbTab.HeaderFontSize = 17
    AodbTab.FontSize = 17
    AodbTab.LineHeight = 17
    AodbTab.FontName = "Courier New"
    AodbTab.SetTabFontBold False
    AodbTab.SetMainHeaderValues "1", "Uploaded Telex Records", "12632256"
    AodbTab.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    AodbTab.MainHeader = True
    AodbTab.LifeStyle = True
    AodbTab.ResetContent
    AodbTab.Refresh
    AodbTab.Visible = True
    
    InitTimePanel
    
    Me.Caption = "UFIS AODB Reader (SCORE)"
    ShownModal = True
    Me.Show vbModal
End Sub

Private Sub ReadFromAodb()
    Dim SqlFld As String
    Dim SqlKey As String
    Dim SqlVpfr As String
    Dim SqlVpto As String
    Dim SqlSpfr As String
    Dim SqlSpto As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LinCount As Long
    Dim HlpLine As Long
    Dim HlpMax As Long
    Dim TlxText As String
    Dim TlxUrno As String
    Dim TlxTime As String
    Dim NewData As String
    Dim TlxLine As String
    Dim RetVal As Boolean
    Dim TimeBegin
    Dim TimeEnd
    Dim TimeLoop
    
    SqlFld = "URNO,TXT1,TXT2,TIME"
    
    RetVal = CheckDateInput(True)
    If RetVal = True Then
        Screen.MousePointer = 11
        AodbTab.ResetContent
        AodbTab.SetFieldSeparator ","
        AodbTab.HeaderString = SqlFld
        AodbTab.LogicalFieldList = SqlFld
        AodbTab.HeaderLengthString = "80,1000,1000,100"
        AodbTab.ColumnWidthString = "10,1000,1000,14"
        AodbTab.HeaderFontSize = 17
        AodbTab.FontSize = 17
        AodbTab.LineHeight = 17
        AodbTab.FontName = "Courier New"
        AodbTab.SetTabFontBold False
        AodbTab.SetMainHeaderValues "4", "Uploaded Telex Records", "12632256"
        AodbTab.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
        AodbTab.MainHeader = True
        AodbTab.LifeStyle = True
        AodbTab.ResetContent
        AodbTab.ShowHorzScroller True
        AodbTab.Refresh
        AodbTab.Visible = True
        
        AodbTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        AodbTab.CedaHopo = UfisServer.HOPO
        AodbTab.CedaIdentifier = "IDX"
        AodbTab.CedaPort = "3357"
        AodbTab.CedaReceiveTimeout = "250"
        AodbTab.CedaRecordSeparator = vbLf
        AodbTab.CedaSendTimeout = "250"
        AodbTab.CedaServerName = UfisServer.HostName
        AodbTab.CedaTabext = UfisServer.TblExt
        'AodbTab.CedaUser = gsUserName
        AodbTab.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        DataTab.ResetContent
        DataTab.ShowHorzScroller True
        DataTab.Refresh
        StatusBar1.Panels(1).Text = "Loading:"
        
        SqlSpfr = txtVpfr(0).Tag
        SqlSpto = txtVpto(0).Tag
        TimeBegin = CedaFullDateToVb(SqlSpfr)
        TimeEnd = CedaFullDateToVb(SqlSpto)
        While TimeBegin <= TimeEnd
            TimeLoop = DateAdd("n", 1440, TimeBegin)
            If TimeLoop > TimeEnd Then TimeLoop = TimeEnd
            SqlVpfr = Format(TimeBegin, "dd.mmm.yyyy \/ hh:mmz")
            SqlVpto = Format(TimeLoop, "dd.mmm.yyyy \/ hh:mmz")
            StatusBar1.Panels(2).Text = SqlVpfr & "  to  " & SqlVpto & " ...."
            SqlVpfr = Format(TimeBegin, "yyyymmddhhmm") & "00"
            SqlVpto = Format(TimeLoop, "yyyymmddhhmm") & "59"
            SqlKey = "WHERE TIME BETWEEN '" & SqlVpfr & "' AND '" & SqlVpto & "' AND TTYP='SCOR'"
            AodbTab.CedaAction "RTA", "TLXTAB", SqlFld, "", SqlKey
            MaxLine = AodbTab.GetLineCount
            AodbTab.OnVScrollTo MaxLine
            AodbTab.Refresh
            TimeBegin = DateAdd("n", 1, TimeLoop)
        Wend
        MaxLine = AodbTab.GetLineCount
        AodbTab.OnVScrollTo 0
        AodbTab.Refresh
        TimeBegin = CedaFullDateToVb(SqlSpfr)
        SqlVpfr = Format(TimeBegin, "dd.mmm.yyyy \/ hh:mmz")
        SqlVpto = Format(TimeEnd, "dd.mmm.yyyy \/ hh:mmz")
        StatusBar1.Panels(2).Text = "Loaded:  " & SqlVpfr & "  to  " & SqlVpto & " (" & CStr(MaxLine) & " Telexes)"
        
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            TlxText = AodbTab.GetColumnValue(CurLine, 1) & AodbTab.GetColumnValue(CurLine, 2)
            TlxText = CleanString(TlxText, SERVER_TO_CLIENT, False)
            TlxUrno = AodbTab.GetColumnValue(CurLine, 0)
            TlxTime = AodbTab.GetColumnValue(CurLine, 3)
            HelperTab.ResetContent
            HelperTab.InsertBuffer TlxText, vbLf
            HlpMax = HelperTab.GetLineCount - 1
            For HlpLine = 0 To HlpMax
                TlxLine = HelperTab.GetColumnValue(HlpLine, 0)
                NewData = Left(TlxLine, 8) & ","
                NewData = NewData & TlxLine & ","
                NewData = NewData & TlxUrno & ","
                NewData = NewData & TlxTime
                DataTab.InsertTextLine NewData, False
            Next
            LinCount = DataTab.GetLineCount
            DataTab.OnVScrollTo LinCount
        Next
        'ShowAodbReader = False
        'AodbTab.ResetContent
        'AodbTab.Visible = False
        DataTab.AutoSizeColumns
        DataTab.OnVScrollTo 0
        DataTab.Sort "0", True, True
        DataTab.Refresh
        
        LinCount = DataTab.GetLineCount
        StatusBar1.Panels(1).Text = CStr(LinCount) & " Lines"
        chkWork(1).Caption = "Import"
        Screen.MousePointer = 0
    End If
    
End Sub

Private Function CheckDateInput(FullCheck As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim Spfr As String
    Dim Spto As String
    Dim chkSpfr As Boolean
    Dim chkSpto As Boolean
    Dim ReqTxt As String
    RetVal = True
    Spfr = ""
    Spfr = Spfr & Trim(txtVpfr(0).Text)
    Spfr = Spfr & Trim(txtVpfr(1).Text)
    Spfr = Spfr & Trim(txtVpfr(2).Text)
    Spfr = Spfr & Trim(txtVpfr(3).Text)
    Spfr = Spfr & Trim(txtVpfr(4).Text)
    Spto = ""
    Spto = Spto & Trim(txtVpto(0).Text)
    Spto = Spto & Trim(txtVpto(1).Text)
    Spto = Spto & Trim(txtVpto(2).Text)
    Spto = Spto & Trim(txtVpto(3).Text)
    Spto = Spto & Trim(txtVpto(4).Text)
    If FullCheck Then
        chkSpfr = CheckValidCedaTime(Spfr)
        chkSpto = CheckValidCedaTime(Spto)
        If (chkSpfr = True) And (chkSpto = True) Then
            txtVpfr(0).Tag = Spfr
            txtVpto(0).Tag = Spto
        Else
            ReqTxt = ""
            ReqTxt = ReqTxt & "Invalid Time Filter Data:" & vbNewLine & vbNewLine
            If chkSpfr = False Then ReqTxt = ReqTxt & "Date/Time From ..." & vbNewLine
            If chkSpto = False Then ReqTxt = ReqTxt & "Date/Time To ....." & vbNewLine
            If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", ReqTxt, "stop", "", UserAnswer) >= 0 Then DoNothing
            RetVal = False
        End If
    Else
        txtVpfr(0).Tag = Spfr
        txtVpto(0).Tag = Spto
    End If
    CheckDateInput = RetVal
End Function

Private Sub InitTimePanel()
    Dim UseServerTime
    Dim RelTime
    Dim CedaTime As String
    Dim tmpData As String
    Dim strWkdy As String
    'UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    'If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    'strWkdy = Trim(Str(Weekday(UseServerTime, vbMonday)))
    'If strWkdy = "1" Then
    '    RelTime = DateAdd("n", -(1440 * 3), UseServerTime)
    'Else
    '    RelTime = DateAdd("n", -(1440 * 1), UseServerTime)
    'End If
    'CedaTime = Format(RelTime, "yyyymmddhhmm")
    'txtVpfr(0).Text = Mid(CedaTime, 1, 4)
    'txtVpfr(1).Text = Mid(CedaTime, 5, 2)
    'txtVpfr(2).Text = Mid(CedaTime, 7, 2)
    'txtVpfr(3).Text = Mid(CedaTime, 9, 2)
    'txtVpfr(4).Text = Mid(CedaTime, 11, 2)
    'RelTime = DateAdd("n", 120, UseServerTime)
    'CedaTime = Format(RelTime, "yyyymmddhhmm")
    'txtVpto(0).Text = Mid(CedaTime, 1, 4)
    'txtVpto(1).Text = Mid(CedaTime, 5, 2)
    'txtVpto(2).Text = Mid(CedaTime, 7, 2)
    'txtVpto(3).Text = Mid(CedaTime, 9, 2)
    'txtVpto(4).Text = Mid(CedaTime, 11, 2)
End Sub
