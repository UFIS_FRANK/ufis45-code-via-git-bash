VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FileDialog 
   Caption         =   "UFIS File Manager"
   ClientHeight    =   7635
   ClientLeft      =   1935
   ClientTop       =   1905
   ClientWidth     =   10485
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FileDialog.frx":0000
   LockControls    =   -1  'True
   ScaleHeight     =   7635
   ScaleWidth      =   10485
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   7350
      Width           =   10485
      _ExtentX        =   18494
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16166
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox HorizSplitter 
      Height          =   60
      Index           =   0
      Left            =   0
      ScaleHeight     =   0
      ScaleWidth      =   5865
      TabIndex        =   24
      Top             =   4590
      Visible         =   0   'False
      Width           =   5925
   End
   Begin VB.PictureBox VertSplitter 
      Height          =   5355
      Index           =   1
      Left            =   8670
      ScaleHeight     =   5295
      ScaleWidth      =   0
      TabIndex        =   23
      Top             =   0
      Width           =   60
   End
   Begin VB.PictureBox VertSplitter 
      Height          =   5325
      Index           =   0
      Left            =   6000
      MousePointer    =   9  'Size W E
      ScaleHeight     =   5265
      ScaleWidth      =   0
      TabIndex        =   19
      Top             =   0
      Width           =   60
   End
   Begin VB.PictureBox CollectPanel 
      BackColor       =   &H0000C0C0&
      Height          =   5370
      Left            =   6090
      ScaleHeight     =   5310
      ScaleWidth      =   2490
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   2550
      Begin TABLib.TAB tabFoundFiles 
         Height          =   855
         Index           =   1
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   1508
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   7350
      Left            =   8745
      ScaleHeight     =   7290
      ScaleWidth      =   1680
      TabIndex        =   8
      Top             =   0
      Width           =   1740
      Begin VB.CheckBox chkClear 
         Caption         =   "Clear"
         Enabled         =   0   'False
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   2835
         Width           =   675
      End
      Begin VB.CheckBox chkClearAll 
         Caption         =   "All"
         Enabled         =   0   'False
         Height          =   300
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   2835
         Width           =   375
      End
      Begin VB.CheckBox chkAll 
         Caption         =   "All"
         Enabled         =   0   'False
         Height          =   300
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   2520
         Width           =   375
      End
      Begin VB.CheckBox chkAdd 
         Caption         =   "Add"
         Enabled         =   0   'False
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   2520
         Width           =   675
      End
      Begin VB.CheckBox chkNew 
         Caption         =   "Reset"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   810
         Width           =   1065
      End
      Begin VB.CheckBox chkPanel 
         Caption         =   "Results"
         Enabled         =   0   'False
         Height          =   300
         Left            =   60
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   1440
         Width           =   1065
      End
      Begin VB.CheckBox chkStop 
         Caption         =   "Stop"
         Height          =   300
         Left            =   60
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   21
         Tag             =   "ABOUT"
         Top             =   1440
         Width           =   1065
      End
      Begin VB.CheckBox chkCollect 
         Caption         =   "Collection"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2205
         Width           =   1065
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Search"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1125
         Width           =   1065
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkOk 
         Caption         =   "Open"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   60
         Width           =   1065
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Show All"
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1755
         Width           =   1065
      End
   End
   Begin VB.PictureBox SearchPanel 
      Height          =   1515
      Left            =   0
      ScaleHeight     =   1455
      ScaleWidth      =   5115
      TabIndex        =   5
      Top             =   4710
      Visible         =   0   'False
      Width           =   5175
      Begin TABLib.TAB tabFoundFiles 
         Height          =   855
         Index           =   0
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   1508
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox DirListPanel 
      BackColor       =   &H00E0E0E0&
      Height          =   3735
      Left            =   0
      ScaleHeight     =   3675
      ScaleWidth      =   5865
      TabIndex        =   0
      Top             =   795
      Width           =   5925
      Begin VB.PictureBox VSplit 
         BackColor       =   &H0000FF00&
         BorderStyle     =   0  'None
         Height          =   3585
         Left            =   3240
         ScaleHeight     =   3585
         ScaleWidth      =   90
         TabIndex        =   20
         Top             =   0
         Width           =   90
      End
      Begin VB.FileListBox filList 
         Height          =   1455
         Index           =   1
         Left            =   3450
         TabIndex        =   12
         Top             =   1560
         Visible         =   0   'False
         Width           =   2355
      End
      Begin VB.ComboBox cboFileType 
         Height          =   315
         ItemData        =   "FileDialog.frx":030A
         Left            =   60
         List            =   "FileDialog.frx":030C
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   3270
         Width           =   3045
      End
      Begin VB.DriveListBox drvList 
         Height          =   315
         Left            =   60
         TabIndex        =   4
         Top             =   60
         Width           =   3045
      End
      Begin VB.DirListBox dirList 
         Height          =   2790
         Left            =   60
         TabIndex        =   3
         Top             =   450
         Width           =   3045
      End
      Begin VB.FileListBox filList 
         Height          =   1260
         Index           =   0
         Left            =   3450
         MultiSelect     =   2  'Extended
         TabIndex        =   2
         Top             =   45
         Width           =   2355
      End
      Begin VB.TextBox txtFileName 
         Height          =   315
         Left            =   3435
         TabIndex        =   1
         Text            =   "*.*"
         Top             =   3270
         Width           =   2355
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      Height          =   780
      Left            =   0
      ScaleHeight     =   720
      ScaleWidth      =   5865
      TabIndex        =   18
      Top             =   0
      Width           =   5925
      Begin VB.CheckBox chkMain 
         Enabled         =   0   'False
         Height          =   300
         Index           =   3
         Left            =   1770
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   375
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Enabled         =   0   'False
         Height          =   300
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   375
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Enabled         =   0   'False
         Height          =   300
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   60
         Visible         =   0   'False
         Width           =   4995
      End
      Begin VB.CheckBox chkMain 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   0
         Left            =   60
         Picture         =   "FileDialog.frx":030E
         Style           =   1  'Graphical
         TabIndex        =   22
         Tag             =   "ABS.CLEAR:3,40,42,33"
         Top             =   60
         Width           =   615
      End
   End
End
Attribute VB_Name = "FileDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SearchFlag As Integer   ' Used as flag for cancel and other operations.
Dim CurFileList As Integer
Dim MyResultList As String
Dim MaxResultLine As Long
Dim CurResultLine As Long
Dim SlideShow As Boolean
Dim SlideDontShow As Boolean

Public Function GetNextResultLine(Index As Integer, SlidePath As String, SlideName As String) As Long
    Dim VisLines As Long
    Dim TopLine As Long
    Dim CurTopLine As Long
    Dim BotLine As Long
    Dim CurLine As Long
    Dim SetScroll As Long
    MaxResultLine = tabFoundFiles(Index).GetLineCount
    If MaxResultLine = 0 Then
        If Index = 0 Then
            Index = 1
        Else
            Index = 0
        End If
        MaxResultLine = tabFoundFiles(Index).GetLineCount
    End If
    If MaxResultLine > 0 Then
        CurResultLine = tabFoundFiles(Index).GetCurrentSelected
        CurResultLine = CurResultLine + 1
        If CurResultLine >= MaxResultLine Then CurResultLine = 0
        SlidePath = tabFoundFiles(Index).GetFieldValue(CurResultLine, "PATH")
        SlideName = tabFoundFiles(Index).GetFieldValue(CurResultLine, "FILE")
        SlideDontShow = True
        tabFoundFiles(Index).SetCurrentSelection CurResultLine
        VisLines = CLng(tabFoundFiles(Index).Height \ (tabFoundFiles(Index).LineHeight * 15)) - 1
        TopLine = tabFoundFiles(Index).GetVScrollPos
        BotLine = TopLine + VisLines
        CurLine = CurResultLine
        CurTopLine = CurLine - VisLines + 1
        If (CurLine >= TopLine) And (CurLine < BotLine) Then
            SetScroll = -1
        Else
            If CurLine >= BotLine Then
                SetScroll = CurLine - VisLines + 2
            End If
        End If
        
        If SetScroll >= 0 Then
            tabFoundFiles(Index).OnVScrollTo SetScroll
        End If
        
        SlideDontShow = False
    End If
    GetNextResultLine = MaxResultLine - CurResultLine
End Function

Public Sub SelectSlides(SetShow As Boolean)
    SlideShow = SetShow
    If SlideShow = True Then
        If txtFileName.Text = "*.*" Then
            txtFileName.Text = "*.bmp;*.jpg"
        End If
    Else
        Me.Hide
    End If
End Sub

Public Sub SetTopPanelValues(chkButton As CheckBox)
    chkMain(0).Picture = chkButton.Picture
End Sub

Public Function GetFileOpenDialog(MainPanel As Integer, UsedPath As String, TypeList As String, ListItem As Integer, ToolPanel As Integer, ForWhat As String) As String
    Dim FileTypes As String
    Dim tmpEntry As String
    Dim tmpContext As String
    Dim tmpFilter As String
    Dim tmpMulti As String
    Dim ItemNo As Long
    Dim i As Integer
    On Error Resume Next
    Select Case ForWhat
        Case "FILE.OPEN"
            SlideShow = False
        Case "SLIDE.SHOW"
            SlideShow = True
        Case Else
            SlideShow = False
    End Select
    MyResultList = ""
    FileTypes = TypeList
    If FileTypes = "" Then FileTypes = "{All Files}{*.*}"
    If UsedPath <> "" Then
        drvList.Drive = UsedPath
        dirList.Path = UsedPath
    End If
    cboFileType.Clear
    ItemNo = 0
    tmpEntry = GetRealItem(FileTypes, ItemNo, "|")
    While tmpEntry <> ""
        tmpContext = Trim(GetRealItem(tmpEntry, 0, "}"))
        tmpContext = Replace(tmpContext, "{", "", 1, -1, vbBinaryCompare)
        tmpFilter = Trim(GetRealItem(tmpEntry, 1, "}"))
        tmpFilter = Replace(tmpFilter, "{", "", 1, -1, vbBinaryCompare)
        tmpMulti = Trim(GetRealItem(tmpEntry, 2, "}"))
        tmpMulti = Replace(tmpMulti, "{", "", 1, -1, vbBinaryCompare)
        tmpEntry = Trim(tmpContext) & Space(100) & "|" & Trim(tmpFilter) & "|" & Trim(tmpMulti)
        cboFileType.AddItem tmpEntry
        ItemNo = ItemNo + 1
        tmpEntry = GetRealItem(FileTypes, ItemNo, "|")
    Wend
    cboFileType.ListIndex = ListItem
    If Me.Width < 6255 Then Me.Width = 6255
    Me.Show vbModal
    UsedPath = dirList.Path
    GetFileOpenDialog = MyResultList
End Function

Private Sub CloseFileDialog()
    chkClose.Value = 0
    chkOk.Value = 0
    Me.Hide
End Sub

Private Sub cboFileType_Click()
    Dim tmpMulti As String
    tmpMulti = GetRealItem(cboFileType.Text, 2, "|")
    If tmpMulti = "M" Then
        If CurFileList <> 0 Then
            CurFileList = 0
            filList(0).Top = filList(1).Top
            filList(0).Left = filList(1).Left
            filList(0).Height = filList(1).Height
            filList(0).Width = filList(1).Width
            filList(0).Path = filList(1).Path
            filList(0).Visible = True
            filList(1).Visible = False
            chkCollect.Value = 0
            chkCollect.Enabled = True
        End If
    Else
        If CurFileList <> 1 Then
            CurFileList = 1
            filList(1).Top = filList(0).Top
            filList(1).Left = filList(0).Left
            filList(1).Height = filList(0).Height
            filList(1).Width = filList(0).Width
            filList(1).Path = filList(0).Path
            filList(1).Visible = True
            filList(0).Visible = False
            chkCollect.Value = 0
            chkCollect.Enabled = False
        End If
    End If
    txtFileName.Text = GetRealItem(cboFileType.Text, 1, "|")
End Sub

Private Sub chkAdd_Click()
    Dim SelPath As String
    Dim SelFile As String
    Dim newEntry As String
    Dim i As Integer
    Dim FoundOne As Boolean
    If chkAdd.Value = 1 Then
        SelPath = dirList.Path
        chkCollect.Value = 1
        FoundOne = False
        For i = 0 To filList(CurFileList).ListCount - 1
            If filList(CurFileList).Selected(i) = True Then
                SelFile = filList(CurFileList).List(i)
                newEntry = "," & SelFile & "," & SelPath & ","
                tabFoundFiles(1).InsertTextLine newEntry, False
                FoundOne = True
            End If
        Next i
        If (FoundOne = False) Or (chkAll.Value = 1) Then
            For i = 0 To filList(CurFileList).ListCount - 1
                SelFile = filList(CurFileList).List(i)
                newEntry = "," & SelFile & "," & SelPath & ","
                tabFoundFiles(1).InsertTextLine newEntry, False
            Next i
        End If
        tabFoundFiles(1).AutoSizeColumns
        tabFoundFiles(1).Refresh
        chkAll.Value = 0
        chkAdd.Value = 0
    End If
End Sub

Private Sub chkAll_Click()
    chkAdd.Value = chkAll.Value
End Sub

Private Sub chkClear_Click()
    Dim LineNo As Long
    If chkClear.Value = 1 Then
        LineNo = tabFoundFiles(1).GetCurrentSelected
        tabFoundFiles(1).DeleteLine LineNo
        If LineNo > tabFoundFiles(1).GetLineCount - 1 Then LineNo = LineNo - 1
        tabFoundFiles(1).SetCurrentSelection LineNo
        chkClear.Value = 0
    End If
End Sub

Private Sub chkClearAll_Click()
    If chkClearAll.Value = 1 Then
        tabFoundFiles(1).ResetContent
        tabFoundFiles(1).AutoSizeColumns
        chkClearAll.Value = 0
    End If
End Sub

Private Sub chkClose_Click()
    Dim MaxLine As Long
    If chkClose.Value = 1 Then
        MyResultList = ""
        If SlideShow Then
            MaxLine = tabFoundFiles(0).GetLineCount + tabFoundFiles(1).GetLineCount
            If MaxLine > 0 Then MyResultList = "OK"
        End If
        CloseFileDialog
    End If
End Sub

Private Sub chkCollect_Click()
    If chkCollect.Value = 1 Then
        chkCollect.BackColor = LightGreen
        tabFoundFiles(1).SetUniqueFields "1,2"
        chkAdd.Enabled = True
        chkAll.Enabled = True
        chkClear.Enabled = True
        chkClearAll.Enabled = True
        CollectPanel.Visible = True
        'VertSplitter.Visible = True
        If Me.Width < 8325 Then Me.Width = 8325
        Form_Resize
    Else
        chkCollect.BackColor = vbButtonFace
        CollectPanel.Visible = False
        chkAdd.Enabled = False
        chkAll.Enabled = False
        chkClear.Enabled = False
        chkClearAll.Enabled = False
        'VertSplitter.Visible = False
        Form_Resize
    End If
End Sub

Private Sub chkMain_Click(Index As Integer)
    chkMain(Index).Value = 0
End Sub

Private Sub chkNew_Click()
    tabFoundFiles(0).ResetContent
    tabFoundFiles(0).Refresh
    'tabFoundFiles(1).ResetContent
    'tabFoundFiles(1).Refresh
    chkNew.Value = 0
End Sub

Private Sub chkOk_Click()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SelType As String
    Dim SelPath As String
    Dim SelFile As String
    Dim newEntry As String
    Dim i As Integer
    If chkOk.Value = 1 Then
        If SlideShow = False Then
            MyResultList = ""
            If chkCollect.Value = 1 Then
                MaxLine = tabFoundFiles(1).GetLineCount - 1
                For CurLine = 0 To MaxLine
                    SelType = tabFoundFiles(1).GetFieldValue(CurLine, "TYPE")
                    SelPath = tabFoundFiles(1).GetFieldValue(CurLine, "PATH")
                    SelFile = tabFoundFiles(1).GetFieldValue(CurLine, "FILE")
                    MyResultList = MyResultList & SelPath & "\" & SelFile & vbLf
                Next
            Else
                SelPath = dirList.Path
                For i = 0 To filList(CurFileList).ListCount - 1
                    If filList(CurFileList).Selected(i) = True Then
                        MyResultList = MyResultList & SelPath & "\" & filList(CurFileList).List(i) & vbLf
                    End If
                Next i
            End If
            chkOk.Value = 0
            If MyResultList <> "" Then CloseFileDialog
        Else
            SelPath = dirList.Path
            MaxLine = tabFoundFiles(0).GetLineCount + tabFoundFiles(1).GetLineCount
            If MaxLine = 0 Then
                chkCollect.Value = 1
                For i = 0 To filList(CurFileList).ListCount - 1
                    If filList(CurFileList).Selected(i) = True Then
                        SelFile = filList(CurFileList).List(i)
                        newEntry = "," & SelFile & "," & SelPath & ","
                        tabFoundFiles(1).InsertTextLine newEntry, False
                    End If
                Next i
            End If
            MaxLine = tabFoundFiles(1).GetLineCount
            If MaxLine = 0 Then
                For i = 0 To filList(CurFileList).ListCount - 1
                    SelFile = filList(CurFileList).List(i)
                    newEntry = "," & SelFile & "," & SelPath & ","
                    tabFoundFiles(1).InsertTextLine newEntry, False
                Next i
            End If
            tabFoundFiles(1).AutoSizeColumns
            MaxLine = tabFoundFiles(1).GetLineCount
            If MaxLine = 0 Then
                chkShow.Value = 1
            Else
                MyResultList = "OK"
                CloseFileDialog
            End If
        End If
    End If
End Sub

Private Sub chkPanel_Click()
    If chkPanel.Value = 1 Then
        chkPanel.BackColor = LightGreen
        If Me.Height < 7500 Then Me.Height = 7500
        HorizSplitter(0).Top = 4500
        SearchPanel.Visible = True
        HorizSplitter(0).Visible = True
        Form_Resize
    Else
        chkPanel.BackColor = vbButtonFace
        SearchPanel.Visible = False
        HorizSplitter(0).Visible = False
        Form_Resize
    End If
End Sub

Private Sub chkSearch_Click()
    Dim tmpText As String
    If chkSearch.Value = 1 Then
        chkSearch.BackColor = LightGreen
        chkPanel.Value = 1
        chkPanel.Enabled = False
        chkStop.Visible = True
        chkStop.ZOrder
        Me.MousePointer = 11
        ResetSearch
        PerformSearch
        tabFoundFiles(0).OnVScrollTo 0
        MaxResultLine = tabFoundFiles(0).GetLineCount
        StatusBar1.Panels(1).Text = CStr(MaxResultLine)
        chkSearch.Value = 0
    Else
        chkSearch.BackColor = vbButtonFace
        SearchFlag = False
        chkStop.Visible = False
        chkPanel.Enabled = True
    End If
    Me.MousePointer = 0
End Sub

Private Sub PerformSearch()
    ' Initialize for search, then perform recursive search.
    Dim FirstPath As String
    Dim DirCount As Integer
    Dim NumFiles As Integer
    Dim Result As Integer
    ' Update dirList.Path if it is different from the currently
    ' selected directory, otherwise perform the search.
    If dirList.Path <> dirList.List(dirList.ListIndex) Then
        dirList.Path = dirList.List(dirList.ListIndex)
        Exit Sub         ' Exit so user can take a look before searching.
    End If

    ResetSearch
    filList(CurFileList).Pattern = txtFileName.Text
    FirstPath = dirList.Path
    DirCount = dirList.ListCount

    ' Start recursive direcory search.
    NumFiles = 0                       ' Reset found files indicator.
    Result = DirDiver(FirstPath, DirCount, "")
    filList(CurFileList).Path = dirList.Path
    'cmdSearch.Caption = "&Reset"
    'cmdSearch.SetFocus
    tabFoundFiles(0).AutoSizeColumns
    
End Sub
Private Function DirDiver(NewPath As String, DirCount As Integer, BackUp As String) As Integer
    '  Recursively search directories from NewPath down...
    '  NewPath is searched on this recursion.
    '  BackUp is origin of this recursion.
    '  DirCount is number of subdirectories in this directory.
    Static FirstErr As Integer
    Dim DirsToPeek As Integer, AbandonSearch As Integer, ind As Integer
    Dim OldPath As String, ThePath As String, entry As String
    Dim RetVal As Integer
    Dim tmpCount As Long
    SearchFlag = True           ' Set flag so the user can interrupt.
    DirDiver = False            ' Set to True if there is an error.
    RetVal = DoEvents()         ' Check for events (for instance, if the user chooses Cancel).
    If SearchFlag = False Then
        DirDiver = True
        Exit Function
    End If
    On Local Error GoTo DirDriverHandler
    DirsToPeek = dirList.ListCount                  ' How many directories below this?
    Do While DirsToPeek > 0 And SearchFlag = True
        OldPath = dirList.Path                      ' Save old path for next recursion.
        dirList.Path = NewPath
        If dirList.ListCount > 0 Then
            ' Get to the node bottom.
            dirList.Path = dirList.List(DirsToPeek - 1)
            AbandonSearch = DirDiver((dirList.Path), DirCount%, OldPath)
        End If
        ' Go up one level in directories.
        DirsToPeek = DirsToPeek - 1
        If AbandonSearch = True Then Exit Function
    Loop
    ' Call function to enumerate files.
    If filList(CurFileList).ListCount > 0 Then
        'If Len(dirList.Path) <= 3 Then             ' Check for 2 bytes/character
        '    ThePath = dirList.Path                  ' If at root level, leave as is...
        'Else
        '    ThePath = dirList.Path + "\"            ' Otherwise put "\" before the filename.
        'End If
        ThePath = dirList.Path                  ' If at root level, leave as is...
        For ind = 0 To filList(CurFileList).ListCount - 1        ' Add conforming files in this directory to the list box.
            entry = filList(CurFileList).List(ind) & "," & ThePath
            tabFoundFiles(0).InsertTextLine entry, False
            tmpCount = tabFoundFiles(0).GetLineCount
            If tmpCount Mod 200 = 0 Then
                tabFoundFiles(0).OnVScrollTo tmpCount - 20
            End If
        Next ind
    End If
    If BackUp <> "" Then        ' If there is a superior directory, move it.
        dirList.Path = BackUp
    End If
    Exit Function
DirDriverHandler:
    If Err = 7 Then             ' If Out of Memory error occurs, assume the list box just got full.
        DirDiver = True         ' Create Msg and set return value AbandonSearch.
        MsgBox "You've filled the list box. Abandoning search..."
        Exit Function           ' Note that the exit procedure resets Err to 0.
    Else                        ' Otherwise display error message and quit.
        MsgBox Error
        End
    End If
End Function

Private Sub chkShow_Click()
    Dim ShowFile As String
    On Error Resume Next
    If chkShow.Value = 1 Then
        chkShow.BackColor = LightGreen
        If SlideShow = True Then
            MaxResultLine = tabFoundFiles(0).GetLineCount + tabFoundFiles(1).GetLineCount
            If MaxResultLine <= 0 Then chkSearch.Value = 1
            MaxResultLine = tabFoundFiles(0).GetLineCount - 1
            CurResultLine = -1
            chkShow.Value = 0
            MyResultList = "OK"
            CloseFileDialog
        Else
            chkShow.Value = 0
        End If
    Else
        chkShow.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkStop_Click()
    If chkStop.Value = 1 Then
        If chkSearch.Value = 1 Then
            chkSearch.Value = 0
        End If
        chkStop.Value = 0
    Else
    End If
End Sub

Private Sub DirList_Change()
    If dirList.ListCount > 0 Then dirList.TopIndex = dirList.TopIndex + 1
    filList(CurFileList).Path = dirList.Path
End Sub

Private Sub DirList_LostFocus()
    dirList.Path = dirList.List(dirList.ListIndex)
End Sub

Private Sub DrvList_Change()
    On Error GoTo DriveHandler
    dirList.Path = drvList.Drive
    Exit Sub

DriveHandler:
    drvList.Drive = dirList.Path
    Exit Sub
End Sub

Private Sub filList_Click(Index As Integer)
    Dim SelPath As String
    Dim SelFile As String
    Dim newEntry As String
    SelFile = filList(Index).List(filList(Index).ListIndex)
    SelPath = dirList.Path
    newEntry = SelPath & "\" & SelFile
    StatusBar1.Panels(2).Text = newEntry
    If (SlideShow = True) And (SlideDontShow = False) Then
        CheckUfisIntroPicture -2, False, 0, SelFile, SelPath
    End If
End Sub

Private Sub filList_DblClick(Index As Integer)
    Dim SelPath As String
    Dim SelFile As String
    Dim newEntry As String
    If SlideShow Then chkCollect.Value = 1
    SelFile = filList(Index).List(filList(Index).ListIndex)
    SelPath = dirList.Path
    If chkCollect.Value = 1 Then
        newEntry = "," & SelFile & "," & SelPath & ","
        InsertCollectionTab newEntry
    Else
        MyResultList = SelPath & "\" & SelFile
        CloseFileDialog
    End If
End Sub
Private Sub InsertCollectionTab(newEntry As String)
    tabFoundFiles(1).InsertTextLine newEntry, False
    tabFoundFiles(1).AutoSizeColumns
End Sub

Private Sub Form_Load()
    Dim i As Integer
    CurFileList = 0
    DirListPanel.Left = 0
    SearchPanel.Left = 0
    tabFoundFiles(0).HeaderString = "Found Files,Path" & Space(1000)
    tabFoundFiles(0).LogicalFieldList = "FILE,PATH,INFO"
    tabFoundFiles(0).HeaderLengthString = "10,10,10"
    tabFoundFiles(0).ColumnWidthString = "32,128,1"
    tabFoundFiles(1).HeaderString = "FP,Collected Files               ,Path" & Space(1000)
    tabFoundFiles(1).LogicalFieldList = "TYPE,FILE,PATH,INFO"
    tabFoundFiles(1).HeaderLengthString = "10,10,10,10"
    tabFoundFiles(1).ColumnWidthString = "2,32,128,1"
    For i = 0 To 1
        tabFoundFiles(i).FontName = "MS Sans Serif"
        tabFoundFiles(i).SetTabFontBold True
        tabFoundFiles(i).LifeStyle = True
        tabFoundFiles(i).AutoSizeByHeader = True
        tabFoundFiles(i).ResetContent
        tabFoundFiles(i).AutoSizeColumns
    Next
    RightPanel.Width = chkOk.Width + (chkOk.Left * 2) + 60
    RightPanel.BackColor = vbButtonFace
    VSplit.BackColor = DirListPanel.BackColor
    Me.Width = 6255
    Me.Height = 5310
    Me.Top = UfisPicForm.Top + (UfisPicForm.Height - Me.Height) \ 2
    Me.Left = UfisPicForm.Left + (UfisPicForm.Width - Me.Width) \ 2
End Sub

Private Sub Form_Paint()
    DrawBackGround TopPanel, 7, True, True
    DrawBackGround RightPanel, 7, True, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case 0
            MyResultList = ""
            Cancel = True
            CloseFileDialog
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewLeft As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    NewHeight = Me.ScaleHeight - StatusBar1.Height
    If NewHeight > 150 Then
        CollectPanel.Height = NewHeight
        VertSplitter(0).Height = NewHeight
        VertSplitter(1).Height = NewHeight
        tabFoundFiles(1).Height = NewHeight - 60
    End If
    NewWidth = Me.ScaleWidth - RightPanel.Width - VertSplitter(1).Width
    VertSplitter(1).Left = NewWidth
    If chkCollect.Value = 1 Then
        NewWidth = NewWidth - CollectPanel.Width
        CollectPanel.Left = NewWidth
        NewWidth = NewWidth - VertSplitter(0).Width
        tabFoundFiles(1).Width = CollectPanel.Width - 60
    End If
    VertSplitter(0).Left = NewWidth
    If NewWidth > 600 Then
        TopPanel.Width = NewWidth
        HorizSplitter(0).Width = NewWidth
    End If
    If NewWidth > 3000 Then
        DirListPanel.Width = NewWidth
        SearchPanel.Width = NewWidth
        tabFoundFiles(0).Width = NewWidth - 60
        NewWidth = DirListPanel.ScaleWidth - (dirList.Left * 2)
        LeftWidth = ((NewWidth - VSplit.Width) / 2)
        drvList.Width = LeftWidth
        dirList.Width = LeftWidth
        cboFileType.Width = LeftWidth
        NewLeft = drvList.Left + LeftWidth
        VSplit.Left = NewLeft
        NewLeft = NewLeft + VSplit.Width
        filList(CurFileList).Left = NewLeft
        txtFileName.Left = NewLeft
        RightWidth = NewWidth - LeftWidth - VSplit.Width
        filList(CurFileList).Width = RightWidth
        txtFileName.Width = RightWidth
    End If
    If chkPanel.Value = 1 Then
        SearchPanel.Top = HorizSplitter(0).Top + HorizSplitter(0).Height
        NewHeight = Me.ScaleHeight - StatusBar1.Height - SearchPanel.Top
        If NewHeight > 1500 Then
            SearchPanel.Height = NewHeight
            tabFoundFiles(0).Height = NewHeight - 60
        End If
    Else
        NewHeight = Me.ScaleHeight - StatusBar1.Height
        HorizSplitter(0).Top = NewHeight
    End If
    NewHeight = HorizSplitter(0).Top - DirListPanel.Top
    If NewHeight > 1200 Then
        DirListPanel.Height = NewHeight
        VSplit.Height = NewHeight
        txtFileName.Top = NewHeight - txtFileName.Height - 120
        cboFileType.Top = txtFileName.Top
        NewHeight = txtFileName.Top - dirList.Top - 30
        dirList.Height = NewHeight
        NewHeight = txtFileName.Top - filList(CurFileList).Top - 30
        filList(CurFileList).Height = NewHeight
    End If
    'DrawBackGround TopPanel, 7, True, True
    'DrawBackGround RightPanel, 7, True, True
    Me.Refresh
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'End
End Sub

Private Sub ResetSearch()
    tabFoundFiles(0).ResetContent
    tabFoundFiles(0).Refresh
    SearchFlag = False  ' Flag indicating search in progress.
End Sub

Private Sub tabFoundFiles_GotFocus(Index As Integer)
    tabFoundFiles(Index).SetCurrentSelection tabFoundFiles(Index).GetCurrentSelected
End Sub

Private Sub tabFoundFiles_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpName As String
    Dim tmpPath As String
    If (LineNo >= 0) And (Selected) Then
        tmpPath = tabFoundFiles(Index).GetFieldValue(LineNo, "PATH")
        tmpName = tabFoundFiles(Index).GetFieldValue(LineNo, "FILE")
        StatusBar1.Panels(2).Text = tmpPath & "\" & tmpName
        If (SlideShow = True) And (SlideDontShow = False) Then
            CheckUfisIntroPicture -2, False, 0, tmpName, tmpPath
        End If
    End If
End Sub

Private Sub tabFoundFiles_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpLine As String
    If LineNo < 0 Then
        Screen.MousePointer = 11
        tabFoundFiles(Index).Sort CStr(ColNo), True, True
        tabFoundFiles(Index).Refresh
        Screen.MousePointer = 0
    Else
        If Index = 0 Then
            If chkCollect.Value = 1 Then
                tmpLine = "," & tabFoundFiles(0).GetLineValues(LineNo)
                InsertCollectionTab tmpLine
                tabFoundFiles(0).DeleteLine LineNo
            Else
                If SlideShow = False Then
                    MyResultList = tabFoundFiles(0).GetFieldValue(LineNo, "PATH") & "\"
                    MyResultList = MyResultList & tabFoundFiles(0).GetFieldValue(LineNo, "FILE")
                    CloseFileDialog
                End If
            End If
        Else
            tabFoundFiles(Index).DeleteLine LineNo
            If LineNo > tabFoundFiles(Index).GetLineCount - 1 Then LineNo = LineNo - 1
            tabFoundFiles(Index).SetCurrentSelection LineNo
        End If
        tabFoundFiles(Index).AutoSizeColumns
        tabFoundFiles(Index).Refresh
        CurResultLine = LineNo
    End If
End Sub

Private Sub txtFileName_Change()
    ' Update file list box if user changes pattern.
    On Error Resume Next
    filList(CurFileList).Pattern = txtFileName.Text
End Sub

Private Sub txtFileName_GotFocus()
    txtFileName.SelStart = 0          ' Highlight the current entry.
    txtFileName.SelLength = Len(txtFileName.Text)
End Sub

