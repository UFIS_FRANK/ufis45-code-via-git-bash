using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace FlightPermits
{
	/// <summary>
	/// Summary description for frmSelAlc.
	/// </summary>
	public class frmSelAlc : System.Windows.Forms.Form
	{
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button OkButton;

		private IDatabase omDB = null;
		private ITable omAltTab = null; // airline type table containing ALC2/ALC3
		ArrayList omAirlineCodes = new ArrayList();
		private System.Windows.Forms.Button CancButton;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.ListView listView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmSelAlc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmSelAlc));
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.OkButton = new System.Windows.Forms.Button();
			this.CancButton = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.listView1 = new System.Windows.Forms.ListView();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGrid1
			// 
			this.dataGrid1.AlternatingBackColor = System.Drawing.SystemColors.Window;
			this.dataGrid1.CaptionVisible = false;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.GridLineColor = System.Drawing.SystemColors.Control;
			this.dataGrid1.HeaderBackColor = System.Drawing.SystemColors.Control;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.LinkColor = System.Drawing.SystemColors.HotTrack;
			this.dataGrid1.Location = new System.Drawing.Point(8, 8);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
			this.dataGrid1.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.dataGrid1.Size = new System.Drawing.Size(205, 177);
			this.dataGrid1.TabIndex = 0;
			this.dataGrid1.DoubleClick += new System.EventHandler(this.dataGrid1_DoubleClick);
			// 
			// OkButton
			// 
			this.OkButton.Location = new System.Drawing.Point(24, 344);
			this.OkButton.Name = "OkButton";
			this.OkButton.TabIndex = 1;
			this.OkButton.Text = "&OK";
			this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
			// 
			// CancButton
			// 
			this.CancButton.Location = new System.Drawing.Point(112, 344);
			this.CancButton.Name = "CancButton";
			this.CancButton.TabIndex = 2;
			this.CancButton.Text = "&Cancel";
			this.CancButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// listBox1
			// 
			this.listBox1.Location = new System.Drawing.Point(10, 199);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(253, 95);
			this.listBox1.TabIndex = 3;
			// 
			// listView1
			// 
			this.listView1.Location = new System.Drawing.Point(319, 201);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(170, 145);
			this.listView1.TabIndex = 4;
			// 
			// frmSelAlc
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(525, 381);
			this.Controls.Add(this.listView1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.CancButton);
			this.Controls.Add(this.OkButton);
			this.Controls.Add(this.dataGrid1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSelAlc";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Airline Codes";
			this.Load += new System.EventHandler(this.frmSelAlc_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmSelAlc_Load(object sender, System.EventArgs e)
		{
			IDatabase omDB = UT.GetMemDB();
			if(omDB != null)
			{
				ITable omAltTab = omDB["ALT"];
				if(omAltTab != null)
				{
					for( int i = 0; i < omAltTab.Count; i++)
					{
						IRow olRow = omAltTab[i];
						omAirlineCodes.Add(new AirlineCode(olRow["ALC2"], olRow["ALC3"]));
					}
					dataGrid1.DataSource = omAirlineCodes;
				}
			}
		}

		private void OkButton_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.OK;
			this.Close();
		}

		private void CancelButton_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void dataGrid1_DoubleClick(object sender, System.EventArgs e)
		{
			int x = 1;
			x++;
		}
	}
}
