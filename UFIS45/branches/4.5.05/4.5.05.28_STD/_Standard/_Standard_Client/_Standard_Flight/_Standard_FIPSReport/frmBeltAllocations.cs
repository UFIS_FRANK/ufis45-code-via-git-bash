using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmBeltAllocations.
	/// </summary>
	public class frmBeltAllocations : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO') OR " +
			"(TIFA BETWEEN '@@FROM' AND '@@TO')) AND (FTYP='O' OR FTYP='S') AND STAT<>'DEL' " ;
		/// <summary>
		/// The where statement for STEV field of AFTTAB.
		/// </summary>
		private string strTerminalWhere = " AND (STEV = '@@STEV')";
        /// <summary>
        /// The where statement for FLTI field of AFTTAB.
        /// </summary>
        private string strFlightIDWhere = " AND (FLTI = '@@FLTI')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
        private string strLogicalFields = "FLNO,ORG3,STOA,ETAI,TIFA,TERM,BLT1,BLT2";
        private string strLogicalFieldsExtra = ",PSTA,LAND,ONBL,BAS1,BAE1";

		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader = "Flight  ,From  ,Schedule  ,Estimated  ,Actual  ,Term, Belt1, Belt2";
        private string strTabHeaderExtra = ",Position Arr,Land,Onbl,FB,LB ";

		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "60,40,70,70,70,20,60,60";
        private string strTabHeaderLensExtra = ",40,70,70,70,70";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// </summary>
        private string strExcelTabHeaderLens = "60,40,130,130,130,20,60,60";
        private string strExcelTabHeaderLensExtra = ",40,130,130,130,130";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

        /// <summary>
        /// Boolean for showing FlightID Filter
        /// </summary>
        private bool bmShowFlightID = false;
        /// <summary>
        /// Boolean for showing ExtraFields Filter
        /// </summary>
        private bool bmShowExtraFields = false;


		#endregion _My Members
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox cbRotations;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.TextBox txtTerminal;
		private System.Windows.Forms.Label lblTerminal;
        private TextBox txtFlightID;
        private Label lblFlightID;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmBeltAllocations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBeltAllocations));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.tabExcelResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtFlightID = new System.Windows.Forms.TextBox();
            this.lblFlightID = new System.Windows.Forms.Label();
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.cbRotations = new System.Windows.Forms.CheckBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 112);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(724, 326);
            this.panelBody.TabIndex = 3;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Controls.Add(this.tabExcelResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(724, 310);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(724, 310);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // tabExcelResult
            // 
            this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
            this.tabExcelResult.Name = "tabExcelResult";
            this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
            this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
            this.tabExcelResult.TabIndex = 1;
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(724, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.txtFlightID);
            this.panelTop.Controls.Add(this.lblFlightID);
            this.panelTop.Controls.Add(this.txtTerminal);
            this.panelTop.Controls.Add(this.lblTerminal);
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.cbRotations);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(724, 112);
            this.panelTop.TabIndex = 2;
            // 
            // txtFlightID
            // 
            this.txtFlightID.Location = new System.Drawing.Point(284, 33);
            this.txtFlightID.Name = "txtFlightID";
            this.txtFlightID.Size = new System.Drawing.Size(72, 20);
            this.txtFlightID.TabIndex = 32;
            this.txtFlightID.Visible = false;
            // 
            // lblFlightID
            // 
            this.lblFlightID.Location = new System.Drawing.Point(230, 34);
            this.lblFlightID.Name = "lblFlightID";
            this.lblFlightID.Size = new System.Drawing.Size(48, 23);
            this.lblFlightID.TabIndex = 33;
            this.lblFlightID.Text = "FlightID:";
            this.lblFlightID.Visible = false;
            // 
            // txtTerminal
            // 
            this.txtTerminal.Location = new System.Drawing.Point(92, 35);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(128, 20);
            this.txtTerminal.TabIndex = 3;
            // 
            // lblTerminal
            // 
            this.lblTerminal.Location = new System.Drawing.Point(12, 36);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(64, 23);
            this.lblTerminal.TabIndex = 31;
            this.lblTerminal.Text = "Terminal:";
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(396, 72);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 8;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // cbRotations
            // 
            this.cbRotations.Location = new System.Drawing.Point(463, 33);
            this.cbRotations.Name = "cbRotations";
            this.cbRotations.Size = new System.Drawing.Size(104, 24);
            this.cbRotations.TabIndex = 25;
            this.cbRotations.Text = "Rotation View";
            this.cbRotations.Visible = false;
            this.cbRotations.CheckedChanged += new System.EventHandler(this.cbRotations_CheckedChanged);
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(496, 56);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 22;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(496, 72);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 21;
            this.progressBar1.Visible = false;
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(244, 72);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 6;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(168, 72);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 5;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(320, 72);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Close";
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(92, 72);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(267, 3);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(128, 20);
            this.dtTo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(232, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(92, 4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(128, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            // 
            // frmBeltAllocations
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(724, 438);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBeltAllocations";
            this.Text = "Baggage Belt Usage ";
            this.Load += new System.EventHandler(this.frmBeltAllocations_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frm_Closing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmBeltAllocations_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmBeltAllocations_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{

            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
            strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
            strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");

            if (strDateDisplayFormat.Length == 0)
            {
                strDateDisplayFormat = strReportDateTimeFormat;
            }
            else
            {
                strReportDateTimeFormat = strDateDisplayFormat;
            }
            if (strExcelDateTimeFormat.Length == 0)
            {
                strExcelDateTimeFormat = strReportDateTimeFormat;
            }

            if (myIni.IniReadValue("FIPSREPORT", "BELT_FLIGHTID").Length > 0)
            {
                if (myIni.IniReadValue("FIPSREPORT", "BELT_FLIGHTID").CompareTo("TRUE") == 0)
                {
                    bmShowFlightID = true;
                    lblFlightID.Show();
                    txtFlightID.Show();

                }
            }
            if (myIni.IniReadValue("FIPSREPORT", "BELT_EXTRAFIELDS").Length > 0)
            {
                if (myIni.IniReadValue("FIPSREPORT", "BELT_EXTRAFIELDS").CompareTo("TRUE") == 0)
                {
                    bmShowExtraFields = true;
                }
            }


			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
            if (bmShowExtraFields)
            {
                strTabHeader += strTabHeaderExtra;
                strLogicalFields += strLogicalFieldsExtra;
                strTabHeaderLens += strTabHeaderLensExtra;
            }

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;

			tabExcelResult.ResetContent();
			tabExcelResult.HeaderString = strTabHeader;
			tabExcelResult.LogicalFieldList = strLogicalFields;
			tabExcelResult.HeaderLengthString = strTabHeaderLens;

			

			PrepareReportData("Report");
			PrepareReportData("Excel");
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];
				if(bmShowStev == false)
				{
					bmShowStev = true;
					lblTerminal.Text = strTerminalLabel + ":";
					Helper.RemoveOrReplaceString(5,ref strTabHeader,',',strTerminalHeader,false);
				}
			}
			else
			{
				Helper.RemoveOrReplaceString(5,ref strLogicalFields,',',"",true);
				Helper.RemoveOrReplaceString(5,ref strTabHeader,',',"",true);
				Helper.RemoveOrReplaceString(5,ref strTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(5,ref strExcelTabHeaderLens,',',"",true);
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			//No filter necessary for this report
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData("Report");
			PrepareReportData("Excel");
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
            String aftFields =  "URNO,FLNO,ADID,ORG3,STOA,ETAI,TIFA,ACT3,TTYP,BLT1,BLT2,STEV";
            String aftFieldsLen = "10,12,1,3,14,14,14,3,5,5,5,1";
            String aftTimeFields = "STOA,TIFA,ETAI";
            if (bmShowExtraFields)
            {
                aftFields += ",PSTA,LAND,ONBL,BAS1,BAE1";
                aftFieldsLen += ",5,14,14,14,14";
                aftTimeFields += ",LAND,ONBL,BAS1,BAE1";
            }
            myAFT = myDB.Bind("AFT", "AFT", aftFields, 
							  aftFieldsLen,
                              aftFields);
			myAFT.Clear();
            myAFT.TimeFields = aftTimeFields;

			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
            string strFligthID = "";
            if (bmShowFlightID == true)
            {
                strFligthID = txtFlightID.Text.Trim();
            }

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strTerminal != "")
				{
					strTmpWhere += strTerminalWhere;
				}

                if (strFligthID != "")
                {
                    strTmpWhere += strFlightIDWhere;
                }

				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@STEV",strTerminal);
                strTmpWhere = strTmpWhere.Replace("@@FLTI", strFligthID);

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData(string strReportOrExcel)
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			if(strReportOrExcel.Equals("Report") == true)
			{
				lblProgress.Text = "Preparing Data";
				lblProgress.Refresh();
				progressBar1.Visible = true;
				progressBar1.Value = 0;
			}
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);
			
			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					//"URNO,FLNO,ADID,ORG3,STOA,ETAI,TIFA,ACT3,TTYP,BLT1,BLT2"
					//FLNO,ORG3,STOA,ETAI,TIFA,BLT1,BLT2
					imArrivals++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += myAFT[i]["ORG3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOA"], strDateTimeFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["ETAI"], strDateTimeFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFA"], strDateTimeFormat) + ",";

					if(bmShowStev == true)
					{
						strValues += myAFT[i]["STEV"] + ",";
					}
					strValues += myAFT[i]["BLT1"] + ",";
                    strValues += myAFT[i]["BLT2"] ;// "\n";
                    if (bmShowExtraFields)
                    {

                        strValues += "," +myAFT[i]["PSTA"] + ",";
                        strValues += Helper.DateString(myAFT[i]["LAND"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[i]["ONBL"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[i]["BAS1"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[i]["BAE1"], strDateTimeFormat) ;
                    }
                    strValues += "\n";

					sb.Append(strValues);
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				lblProgress.Text = "";
				progressBar1.Visible = false;
				tabResult.Sort("4", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");				
				tabExcelResult.Sort("4", true, true);			
			}
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString() + ")";
			strReportHeader = "Baggage Belt Usage";
			strReportSubHeader = strSubHeader;

            if (bmShowExtraFields)
            {
                strExcelTabHeaderLens = strExcelTabHeaderLens + strExcelTabHeaderLensExtra;
                prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, strReportHeader, strSubHeader, "", 10);
                rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);
                rpt.SetExcelExportTabResult(tabExcelResult);
                //rpt.TextBoxCanGrow = false;
                frmPrintPreview frm = new frmPrintPreview(rpt);
                if (Helper.UseSpreadBuilder() == true)
                {
                    activeReport = rpt;
                    frm.UseSpreadBuilder(true);
                    frm.GetBtnExcelExport().Click += new EventHandler(frmBeltAllocations_Click);
                }
                else
                {
                    frm.UseSpreadBuilder(false);
                }
                frm.Show();
            }
            else
            {
                rptFIPS rpt = new rptFIPS(tabResult, strReportHeader, strSubHeader, "", 10);
                rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);
                rpt.SetExcelExportTabResult(tabExcelResult);
                frmPrintPreview frm = new frmPrintPreview(rpt);
                if (Helper.UseSpreadBuilder() == true)
                {
                    activeReport = rpt;
                    frm.UseSpreadBuilder(true);
                    frm.GetBtnExcelExport().Click += new EventHandler(frmBeltAllocations_Click);
                }
                else
                {
                    frm.UseSpreadBuilder(false);
                }

                frm.Show();
            }
            
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData("Report");
			PrepareReportData("Excel");
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void cbRotations_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmBeltAllocations_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmBeltAllocations_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmBeltAllocations_MouseLeave);
		}

		private void frmBeltAllocations_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmBeltAllocations_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmBeltAllocations_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmBeltAllocations_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLens,10,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmBeltAllocations_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
