using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using ZedGraph;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmSpot_UsingRate.
	/// </summary>
	public class frmSpot_UsingRate : System.Windows.Forms.Form
	{
		#region _MyMembers
		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereAft = "WHERE ((TIFA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' AND PSTA<>' ') OR " +
			                                "(TIFD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO' AND PSTD<>' ')) AND " +
											"FTYP IN ('O','T','G','S')";
		/// <summary>
		/// Where statement for the PSTTAB
		/// </summary>
		private string strWherePst = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		/// <summary>
		/// Where statement for the BLKTAB
		/// </summary>
		private string strWhereBlk = "WHERE BURN IN (@@URNOS) AND TABN='PST'";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the PSTTAB
		/// </summary>
		private ITable myPST;
		/// <summary>
		/// ITable object for the BLKTAB
		/// </summary>
		private ITable myBLK;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// for Rotations
		/// </summary>
		private ITable myROT;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Chart values for the positions
		/// </summary>
		private double [] YFlightPositions;
		/// <summary>
		/// Chart values for the blockings/none validities
		/// </summary>
		private double [] YBlockingData;
		/// <summary>
		/// Loaded Timeframe From 
		/// </summary>
		private DateTime TimeFrameFrom;
		/// <summary>
		/// Loaded Timeframe To 
		/// </summary>
		private DateTime TimeFrameTo;
		/// <summary>
		/// Necessary for none valid times
		/// </summary>
		private DateTime TIMENULL = new DateTime(0);
		/// <summary>
		/// To have an end of time value
		/// </summary>
		private DateTime ENDOFTIME = new DateTime(3000,12,31,0,0,0);

		#endregion _MyMembers

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private ZedGraph.ZedGraphControl zGraph;
		private System.Windows.Forms.CheckBox cbInvalidSpots;
		private System.Windows.Forms.Button btnPrintPreview;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Button buttonHelp;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmSpot_UsingRate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmSpot_UsingRate));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.zGraph = new ZedGraph.ZedGraphControl();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.cbInvalidSpots = new System.Windows.Forms.CheckBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 108);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(928, 494);
			this.panelBody.TabIndex = 5;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.zGraph);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(928, 478);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabResult.Location = new System.Drawing.Point(0, 366);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(928, 112);
			this.tabResult.TabIndex = 1;
			this.tabResult.Visible = false;
			// 
			// zGraph
			// 
			this.zGraph.Dock = System.Windows.Forms.DockStyle.Fill;
			this.zGraph.IsEnableHPan = true;
			this.zGraph.IsEnableVPan = true;
			this.zGraph.IsEnableZoom = true;
			this.zGraph.IsScrollY2 = false;
			this.zGraph.IsShowContextMenu = true;
			this.zGraph.IsShowHScrollBar = false;
			this.zGraph.IsShowPointValues = false;
			this.zGraph.IsShowVScrollBar = false;
			this.zGraph.IsZoomOnMouseCenter = false;
			this.zGraph.Location = new System.Drawing.Point(0, 0);
			this.zGraph.Name = "zGraph";
			this.zGraph.PanButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.PanButtons2 = System.Windows.Forms.MouseButtons.Middle;
			this.zGraph.PanModifierKeys = System.Windows.Forms.Keys.Shift;
			this.zGraph.PanModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.PointDateFormat = "g";
			this.zGraph.PointValueFormat = "G";
			this.zGraph.ScrollMaxX = 0;
			this.zGraph.ScrollMaxY = 0;
			this.zGraph.ScrollMaxY2 = 0;
			this.zGraph.ScrollMinX = 0;
			this.zGraph.ScrollMinY = 0;
			this.zGraph.ScrollMinY2 = 0;
			this.zGraph.Size = new System.Drawing.Size(928, 478);
			this.zGraph.TabIndex = 0;
			this.zGraph.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
			this.zGraph.ZoomModifierKeys = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomStepFraction = 0.1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(928, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.cbInvalidSpots);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(928, 108);
			this.panelTop.TabIndex = 4;
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(176, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 4;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// cbInvalidSpots
			// 
			this.cbInvalidSpots.Location = new System.Drawing.Point(96, 32);
			this.cbInvalidSpots.Name = "cbInvalidSpots";
			this.cbInvalidSpots.Size = new System.Drawing.Size(128, 24);
			this.cbInvalidSpots.TabIndex = 2;
			this.cbInvalidSpots.Text = "&Show invalid spots";
			this.cbInvalidSpots.CheckedChanged += new System.EventHandler(this.cbInvalidSpots_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(492, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(492, 72);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(96, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 3;
			this.btnLoadPrint.Text = "&Load";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(256, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "&Close";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(336, 72);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmSpot_UsingRate
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(928, 602);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmSpot_UsingRate";
			this.Text = "Position Usage";
			this.Load += new System.EventHandler(this.frmSpot_UsingRate_Load);
			this.HelpRequested += new HelpEventHandler(frmSpot_UsingRate_HelpRequested);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmSpot_UsingRate_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			zGraph.Visible = false;
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy";//"dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy"; //"dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Prepare the chart values according to the 
		/// flight data
		/// </summary>
		private void PrepareFlightData()
		{
			//Prepare the hour data to get the timeframes in hours
			//to allocate the double list for the flight curve
			DateTime datFrom;
			DateTime datTo;
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 0,0,0);
			datTo   = new DateTime(datTo.Year, datTo.Month, datTo.Day, 23,59,59);
			TimeFrameFrom = datFrom;
			TimeFrameTo = datTo;
			TimeSpan ts = TimeFrameTo - TimeFrameFrom;
			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			YFlightPositions = new double[ilHours];
			YBlockingData    = new double[ilHours];
			for(int i = 0; i < tabResult.GetLineCount(); i++)
			{
				DateTime olStart = TIMENULL; 
				DateTime olEnd = TIMENULL;
				DateTime olTifa, olTifd;
				if(tabResult.GetFieldValues(i, "ADID") == "R" && 
					tabResult.GetFieldValues(i, "TIFA") != "" && 
					tabResult.GetFieldValues(i, "TIFD") != "")
				{
					olTifa = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "TIFA"));
					olTifd = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "TIFD"));
					olStart = olTifa;
					olEnd   = olTifd;
				}
				if(tabResult.GetFieldValues(i, "ADID") == "A")
				{	
					olTifa = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "TIFA"));
					olStart = olTifa;
					TimeSpan olDura = new TimeSpan(0,0,0,0);
					if(tabResult.GetFieldValues(i, "PAEA") != "" && tabResult.GetFieldValues(i, "PABA") != "" )
					{
						olDura = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "PAEA")) - UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "PABA"));
					}
					if(olDura.TotalMinutes == 0)
					{
						int Ming = 0;
						int ilDura = 30;
						if(tabResult.GetFieldValues(i, "MING_A") != "")
							Ming = Convert.ToInt32(tabResult.GetFieldValues(i, "MING_A"));
						if(Ming > ilDura) 
							ilDura = Ming;
						olDura = new TimeSpan(0,0,ilDura,0);				
					}
					olEnd = olStart + olDura;
				}
				if(tabResult.GetFieldValues(i, "ADID") == "D")
				{
					olTifd = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "TIFD"));
					olStart = olTifd;
					TimeSpan olDura = new TimeSpan(0,0,0,0);
					if(tabResult.GetFieldValues(i, "PDEA") != "" && tabResult.GetFieldValues(i, "PDBA") != "" )
					{
						olDura = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "PDEA")) - UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "PDBA"));
					}
					if(olDura.TotalMinutes == 0)
					{
						int Ming = 0;
						int ilDura = 30;
						if(tabResult.GetFieldValues(i, "MING_D") != "")
							Ming = Convert.ToInt32(tabResult.GetFieldValues(i, "MING_D"));
						if(Ming > ilDura) 
							ilDura = Ming;
						olDura = new TimeSpan(0,0,ilDura,0);				
					}
					olEnd = olTifd;
					olStart = olTifd - olDura;				
				}
				if(olEnd == TIMENULL && olStart == TIMENULL)
				{
					;//Do nothing
				}
				else
				{
					//Fill the entire arry due to undefined end
					//and start earlier than timeframe and for 
					//the case that start and end exceed both the timeframe
					if(olStart != TIMENULL)
					{
						if((olEnd == TIMENULL && olStart < datFrom) || (olStart <= datFrom && olEnd >= datTo))
						{
							for(int j = 0; j < YFlightPositions.Length; j++)
							{
								YFlightPositions[j]++;
							}
						}
						else
						{
							//for further proceeding set olEnd = datTo
							if(olStart < TimeFrameFrom) 
								olStart = TimeFrameFrom;
							if(olEnd > TimeFrameTo) 
								olEnd = TimeFrameTo;
						}
						if(olStart >= datFrom && olEnd != TIMENULL)
						{
							int fromIDX = 0;
							decimal fromDBL = 0;
							decimal toDBL = 0;
							int toIDX = 0;
							TimeSpan tmpTS = olStart - datFrom;
							fromDBL  = (decimal)tmpTS.TotalMinutes / 60;
							fromDBL = decimal.Truncate(fromDBL);
							fromIDX = Convert.ToInt32(fromDBL);
		
							tmpTS = olEnd - datFrom;
							// old way   toIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);

							toDBL  = (decimal)tmpTS.TotalMinutes / 60;
							toDBL = decimal.Truncate(toDBL);
							toIDX = Convert.ToInt32(toDBL);
							
							
							for(int j = fromIDX; j <= toIDX; j++)
							{
								YFlightPositions[j]++;
							}
						}
					}

				}
			}
		}
		/// <summary>
		/// Prepares the chart values according to the
		/// Blocking/validity data
		/// </summary>
		private void PrepareBlockingData()
		{
			TimeSpan ts = TimeFrameTo - TimeFrameFrom;
			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			YBlockingData    = new double[ilHours];
			if(myPST == null)
				return;
			for(int i = 0; i < myPST.Count; i++)
			{
				DateTime vafr = TIMENULL;
				DateTime vato = TIMENULL;
				DateTime olStart = TIMENULL;
				DateTime olEnd = TIMENULL;
				DateTime olStart2 = TIMENULL;
				DateTime olEnd2 = TIMENULL;

				if(myPST[i]["PNAM"] == "F60")
				{
					int ooo = 0;
				}
				if(myPST[i]["VAFR"] != "")
					vafr = myPST[i].FieldAsDateTime("VAFR");
				if(myPST[i]["VATO"] != "")
					vato = myPST[i].FieldAsDateTime("VATO");
				else
				{
					vato = ENDOFTIME;
				}

				if(IsReallyOverlapped(vafr, vato, TimeFrameFrom, TimeFrameTo) || 
				   IsWithIn(vafr, vato, TimeFrameFrom, TimeFrameTo))
				{
					//Valid is full inside the timefram so 
					// we need a timeframe1 before validfrom
					// and another one after valid to
					if(IsWithIn(vafr, vato, TimeFrameFrom, TimeFrameTo))
					{
						olStart = TimeFrameFrom;
						olEnd   = vafr;
						olStart2 = vato;
						olEnd2   = TimeFrameTo;
					}
					//Valid from start within the timeframe
					else if(IsBetween( vafr, TimeFrameFrom, TimeFrameTo) == true)
					{
						olStart = TimeFrameFrom;
						olEnd   = vafr;
					}
					//valid to ends within the timeframe
					else if(IsBetween(vato, TimeFrameFrom, TimeFrameTo) == true)
					{
						olStart = vato;
						olEnd   = TimeFrameTo;
					}
				}
				// Valid to ends before timeframe
				if(vato < TimeFrameFrom )
				{
					olStart = TimeFrameFrom;
					olEnd   = TimeFrameTo;
				}
				// Valid from start after timeframe
				if(vafr > TimeFrameTo)
				{
					olStart = TimeFrameFrom;
					olEnd   = TimeFrameTo;
				}
				//Now fill the double [] YBlockingData array
				if(olStart >= TimeFrameFrom && olEnd != TIMENULL)
				{
					int fromIDX = 0;
					int toIDX = 0;
					TimeSpan tmpTS = olStart - TimeFrameFrom;
					fromIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					tmpTS = olEnd - TimeFrameFrom;
					toIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					for(int j = fromIDX; j < toIDX; j++)
					{
						YBlockingData[j]++;
					}
				}

			}
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			//Clear the tab

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", 
				"RKEY,URNO,ADID,FLNO,PSTA,PSTD,STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING", 
				"10,10,1,12,5,5,14,14,14,14,14,14,14,14,14,14,14,14,4",
				"RKEY,URNO,ADID,FLNO,PSTA,PSTD,STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereAft;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo );
				strTmpWhere += " [ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			//Filter out the FTYPs which are selected due to rotations
			
			// for debugging
			//strTmpWhere = "WHERE RKEY = 1751876273";
			//myAFT.Load(strTmpWhere);

			
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "X" ||
				   myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			
			//Now reading the PSTTAB with the valid from /to records
			myDB.Unbind("PST");
			myPST = myDB.Bind("PST", "PST", "URNO,PNAM,VAFR,VATO", "10,5,14,14", "URNO,PNAM,VAFR,VATO");
			myPST.Clear();
			myPST.TimeFields = "VAFR,VATO";
			myPST.TimeFieldsInitiallyInUtc = true;
			myPST.Load("");
			myPST.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			//Now reading the BLKTAB with the valid from /to records
			string strBURNs = "";
			for(int i = 0; i < myPST.Count; i++)
			{
				strBURNs += myPST[i]["URNO"] + ",";

			}
			if(strBURNs != "")
			{
				strBURNs = strBURNs.Remove(strBURNs.Length-1, 1);
			}
			myDB.Unbind("BLK");
			myBLK = myDB.Bind("BLK", "BLK", "BURN,NAFR,NATO", "10,14,14", "BURN,NAFR,NATO");
			myBLK.Clear();
			myBLK.TimeFields = "NAFR,NATO";
			myBLK.TimeFieldsInitiallyInUtc = true;
			string strTmpWhereBlk = strWhereBlk;
			strTmpWhereBlk = strTmpWhereBlk.Replace("@@URNOS", strBURNs);
			myBLK.Load(strTmpWhereBlk);
			myBLK.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}
		/// <summary>
		/// Initialize the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = "RKEY_A,URNO_A,ADID,FLNO_A,PSTA,STOA,TIFA,MING_A,RKEY_D,URNO_D,FLNO_D,PSTD,STOD,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING_D";
			tabResult.LogicalFieldList = tabResult.HeaderString;
			tabResult.HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100";
		}
		/// <summary>
		/// Prepare Rotations
		/// </summary>
		private void MakeRotations2()
		{
			InitTab();

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			StringBuilder sb = new StringBuilder(10000);
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						imArrivals++;
						imDepartures++;
						//RKEY_A,URNO_A,ADID,FLNO_A,PSTA,STOA,TIFA,MING_A,RKEY_D,URNO_D,FLNO_D,PSTD,STOD,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING_D
						//Arr: ,,,,,,,,
						//DEP: ,,,,,,,,,,,,,,
						//The arrival part of rotation
						strValues += myAFT[llCurr]["RKEY"] + ",";
						strValues += myAFT[llCurr]["URNO"] + ",";
						strValues += "R,";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["PSTA"] + ",";
						strValues += myAFT[llCurr]["STOA"] + ",";
						strValues += myAFT[llCurr]["TIFA"] + ",";
						strValues += myAFT[llCurr]["MING"] + ",";
						//The departure part of the rotation
						strValues += myAFT[llCurr+1]["RKEY"] + ",";
						strValues += myAFT[llCurr+1]["URNO"] + ",";
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["PSTD"] + ",";
						strValues += myAFT[llCurr+1]["STOD"] + ",";
						strValues += myAFT[llCurr+1]["TIFD"] + ",";
						strValues += myAFT[llCurr+1]["PABA"] + ",";
						strValues += myAFT[llCurr+1]["PABS"] + ",";
						strValues += myAFT[llCurr+1]["PAEA"] + ",";
						strValues += myAFT[llCurr+1]["PAES"] + ",";
						strValues += myAFT[llCurr+1]["PDBA"] + ",";
						strValues += myAFT[llCurr+1]["PDBS"] + ",";
						strValues += myAFT[llCurr+1]["PDEA"] + ",";
						strValues += myAFT[llCurr+1]["PDES"] + ",";
						strValues += myAFT[llCurr+1]["MING"] + "\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						imArrivals++;
						imDepartures++;
						//The arrival part of rotation
						strValues += myAFT[llCurr+1]["RKEY"] + ",";
						strValues += myAFT[llCurr+1]["URNO"] + ",";
						strValues += "R,";
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["PSTA"] + ",";
						strValues += myAFT[llCurr+1]["STOA"] + ",";
						strValues += myAFT[llCurr+1]["TIFA"] + ",";
						strValues += myAFT[llCurr+1]["MING"] + ",";
						//The departure part of the rotation
						strValues += myAFT[llCurr]["RKEY"] + ",";
						strValues += myAFT[llCurr]["URNO"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["PSTD"] + ",";
						strValues += myAFT[llCurr]["STOD"] + ",";
						strValues += myAFT[llCurr]["TIFD"] + ",";
						strValues += myAFT[llCurr]["PABA"] + ",";
						strValues += myAFT[llCurr]["PABS"] + ",";
						strValues += myAFT[llCurr]["PAEA"] + ",";
						strValues += myAFT[llCurr]["PAES"] + ",";
						strValues += myAFT[llCurr]["PDBA"] + ",";
						strValues += myAFT[llCurr]["PDBS"] + ",";
						strValues += myAFT[llCurr]["PDEA"] + ",";
						strValues += myAFT[llCurr]["PDES"] + ",";
						strValues += myAFT[llCurr]["MING"] + "\n";
						sb.Append(strValues);
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{
					ilStep = 1;
					if( strCurrAdid == "A")
					{
						imArrivals++;
						//DEP: ,,,,,,,,,,,,,,
						//The arrival part of rotation
						strValues += myAFT[llCurr]["RKEY"] + ",";
						strValues += myAFT[llCurr]["URNO"] + ",";
						strValues += "A,";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["PSTA"] + ",";
						strValues += myAFT[llCurr]["STOA"] + ",";
						strValues += myAFT[llCurr]["TIFA"] + ",";
						strValues += myAFT[llCurr]["MING"] + ",";
						strValues += ",,,,,,,,,,,,,,\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D")
					{
						imDepartures++;
						//The departure part of the rotation
						strValues += ",,D,,,,,,";
						strValues += myAFT[llCurr]["RKEY"] + ",";
						strValues += myAFT[llCurr]["URNO"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["PSTD"] + ",";
						strValues += myAFT[llCurr]["STOD"] + ",";
						strValues += myAFT[llCurr]["TIFD"] + ",";
						strValues += myAFT[llCurr]["PABA"] + ",";
						strValues += myAFT[llCurr]["PABS"] + ",";
						strValues += myAFT[llCurr]["PAEA"] + ",";
						strValues += myAFT[llCurr]["PAES"] + ",";
						strValues += myAFT[llCurr]["PDBA"] + ",";
						strValues += myAFT[llCurr]["PDBS"] + ",";
						strValues += myAFT[llCurr]["PDEA"] + ",";
						strValues += myAFT[llCurr]["PDES"] + ",";
						strValues += myAFT[llCurr]["MING"] + "\n";
						sb.Append(strValues);
					}
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
		}

		/// <summary>
		/// Prepare Rotations
		/// </summary>
		private void MakeRotations()
		{
			InitTab();
			myDB.Unbind("ROT");
			myROT =  myDB.Bind("ROT", "", 
				"RKEY_A,URNO_A,ADID,FLNO_A,RKEY_D,URNO_D,FLNO_D,PSTA,PSTD,STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING_A,MING_D", 
				"10,10,1,12,10,10,12,5,5,14,14,14,14,14,14,14,14,14,14,14,14,4,4", "");
			myROT.Clear();
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						imArrivals++;
						imDepartures++;
						
						IRow row = myROT.CreateEmptyRow();
						//The arrival part of rotation
						row["RKEY_A"] = myAFT[llCurr]["RKEY"];
						row["URNO_A"] = myAFT[llCurr]["URNO"];
						row["ADID"] = "R";
						row["FLNO_A"] = myAFT[llCurr]["FLNO"];
						row["PSTA"] = myAFT[llCurr]["PSTA"];
						row["STOA"] = myAFT[llCurr]["STOA"];
						row["TIFA"] = myAFT[llCurr]["TIFA"];
						row["PABA"] = myAFT[llCurr]["PABA"];
						row["PABS"] = myAFT[llCurr]["PABS"];
						row["PAEA"] = myAFT[llCurr]["PAEA"];
						row["PAES"] = myAFT[llCurr]["PAES"];
						row["MING_A"] = myAFT[llCurr]["MING"];
						//-----------------------------------The departure part of rotation
						row["RKEY_D"] = myAFT[llCurr+1]["RKEY"];
						row["URNO_D"] = myAFT[llCurr+1]["URNO"];
						row["FLNO_D"] = myAFT[llCurr+1]["FLNO"];
						row["PSTD"] = myAFT[llCurr+1]["PSTD"];
						row["STOD"] = myAFT[llCurr+1]["STOD"];
						row["TIFD"] = myAFT[llCurr+1]["TIFD"];
						row["PDBA"] = myAFT[llCurr+1]["PDBA"];
						row["PDBS"] = myAFT[llCurr+1]["PDBS"];
						row["PDEA"] = myAFT[llCurr+1]["PDEA"];
						row["PDES"] = myAFT[llCurr+1]["PDES"];
						row["MING_D"] = myAFT[llCurr+1]["MING"];
						myROT.Add(row);
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						IRow row = myROT.CreateEmptyRow();
						//The arrival part of rotation
						row["RKEY_A"] = myAFT[llCurr+1]["RKEY"];
						row["URNO_A"] = myAFT[llCurr+1]["URNO"];
						row["ADID"] = "R";
						row["FLNO_A"] = myAFT[llCurr+1]["FLNO"];
						row["PSTA"] = myAFT[llCurr+1]["PSTA"];
						row["STOA"] = myAFT[llCurr+1]["STOA"];
						row["TIFA"] = myAFT[llCurr+1]["TIFA"];
						row["PABA"] = myAFT[llCurr+1]["PABA"];
						row["PABS"] = myAFT[llCurr+1]["PABS"];
						row["PAEA"] = myAFT[llCurr+1]["PAEA"];
						row["PAES"] = myAFT[llCurr+1]["PAES"];
						row["MING_A"] = myAFT[llCurr+1]["MING"];
						//-----------------------------------The departure part of rotation
						row["RKEY_D"] = myAFT[llCurr]["RKEY"];
						row["URNO_D"] = myAFT[llCurr]["URNO"];
						row["FLNO_D"] = myAFT[llCurr]["FLNO"];
						row["PSTD"] = myAFT[llCurr]["PSTD"];
						row["STOD"] = myAFT[llCurr]["STOD"];
						row["TIFD"] = myAFT[llCurr]["TIFD"];
						row["PDBA"] = myAFT[llCurr]["PDBA"];
						row["PDBS"] = myAFT[llCurr]["PDBS"];
						row["PDEA"] = myAFT[llCurr]["PDEA"];
						row["PDES"] = myAFT[llCurr]["PDES"];
						row["MING_D"] = myAFT[llCurr]["MING"];
						myROT.Add(row);
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{
					ilStep = 1;
					if( strCurrAdid == "A")
					{
						IRow row = myROT.CreateEmptyRow();
						//The arrival part of rotation
						row["RKEY_A"] = myAFT[llCurr]["RKEY"];
						row["URNO_A"] = myAFT[llCurr]["URNO"];
						row["ADID"] = "A";
						row["FLNO_A"] = myAFT[llCurr]["FLNO"];
						row["PSTA"] = myAFT[llCurr]["PSTA"];
						row["STOA"] = myAFT[llCurr]["STOA"];
						row["TIFA"] = myAFT[llCurr]["TIFA"];
						row["PABA"] = myAFT[llCurr]["PABA"];
						row["PABS"] = myAFT[llCurr]["PABS"];
						row["PAEA"] = myAFT[llCurr]["PAEA"];
						row["PAES"] = myAFT[llCurr]["PAES"];
						row["MING_A"] = myAFT[llCurr]["MING"];
						myROT.Add(row);
					}
					if( strCurrAdid == "D")
					{
						IRow row = myROT.CreateEmptyRow();
						row["RKEY_D"] = myAFT[llCurr]["RKEY"];
						row["URNO_D"] = myAFT[llCurr]["URNO"];
						row["ADID"] = "D";
						row["FLNO_D"] = myAFT[llCurr]["FLNO"];
						row["PSTD"] = myAFT[llCurr]["PSTD"];
						row["STOD"] = myAFT[llCurr]["STOD"];
						row["TIFD"] = myAFT[llCurr]["TIFD"];
						row["PDBA"] = myAFT[llCurr]["PDBA"];
						row["PDBS"] = myAFT[llCurr]["PDBS"];
						row["PDEA"] = myAFT[llCurr]["PDEA"];
						row["PDES"] = myAFT[llCurr]["PDES"];
						row["MING_D"] = myAFT[llCurr]["MING"];
						myROT.Add(row);
					}
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			int ccc = myROT.Count;
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			MakeRotations2();
			PrepareFlightData();
			PrepareBlockingData();
			PrintGraph();
			RunReport();
			this.Cursor = Cursors.Arrow;
			zGraph.Visible = true;
			zGraph.Refresh();
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = imDepartures + imArrivals;
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
//			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, 
//				"Overnight Flights: NAT: " + strNatureCode + " APT: " + strAirportCode + " Duration: " + dtDuration.Value.ToShortTimeString(), 
//				strSubHeader, "", 8);
//			frmPrintPreview frm = new frmPrintPreview(rpt);
//			frm.Show();
		}
		/// <summary>
		/// Setup and draw the chart
		/// </summary>
		private void PrintGraph()
		{
			if(YFlightPositions == null)
				return;
			
			// MaxBarValue stores maximum of all bars during the loaded timeframe
			int MaxBarValue = 0;

			ZedGraph.GraphPane myPane = zGraph.GraphPane;
			myPane.GraphItemList.Clear();
			zGraph.Controls.Clear();
			myPane.CurveList.Clear();
			// Set the titles and axis labels
			myPane.Title = "Position Usage";
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			myPane.XAxis.Title = "time (in hours)";
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.YAxis.Title = "Number of Spots/Parking Stands";
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			ZedGraph.BarItem mySecondCurve = null;
			// Make up some random data points
			string [] str = new string[YFlightPositions.Length];
			int ilCurrHour = 0;
			for(int i = 0; i < YFlightPositions.Length; i++)
			{
				if(ilCurrHour == 24)
				{
					ilCurrHour = 0;
				}
				str[i] = ilCurrHour.ToString();
				ilCurrHour++;
			}
			MaxBarValue = 0;
			for (int i = 0; i < YFlightPositions.Length; i++)
			{
				if(YFlightPositions[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YFlightPositions[i]);

			}

			// Add a bar to the graph
			ZedGraph.BarItem myCurve = myPane.AddBar( "Occupied Spots", null, YFlightPositions, Color.White );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;
			if(cbInvalidSpots.Checked == true)
			{
				ZedGraph.BarItem myCurve2 = myPane.AddBar( "Not valid Spots", null, YBlockingData, Color.Red );
				mySecondCurve = myCurve2;
				myCurve2.Bar.Fill = new Fill( Color.Red, Color.White, Color.Red, 0F );
				myCurve2.Bar.Border.IsVisible = false;
				for (int i = 0; i < YBlockingData.Length; i++)
				{
					if(YBlockingData[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YBlockingData[i]);

				}
			}

			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = true;
			
			myPane.AxisChange( zGraph.CreateGraphics());
			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(YFlightPositions.Length <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxBarValue)*MaxBarValue;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
										
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point
								if(cbInvalidSpots.Checked == false)
								{
									text.Location.AlignH =  AlignH.Center;
								}
								else
								{
									if(curve == myCurve)
									{
										text.Location.AlignH =  AlignH.Right;
									}
									if(curve == mySecondCurve)
									{
										text.Location.AlignH =  AlignH.Left;
									}
								}
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}

					ord++;
				}
			}
			zGraph.Show();
			zGraph.Refresh();

		}
		#region ------------- Overlapping testers
		/// <summary>
		/// Detects overlapping when start of time frame 1 and frame 2 are not equal.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is overlapped, false not</returns>
		internal bool IsReallyOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2)
		{
			if((start1 < end2) && (start2 < end1)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether time frame 1 is within time frame 2.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is within, false = outside</returns>
		internal bool IsWithIn(DateTime start1, DateTime end1, DateTime start2, DateTime end2)	
		{
			if((start1 >= start2) && (end1 <= end2)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between a time frame.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of time frame.</param>
		/// <param name="end">End of time frame.</param>
		/// <returns>True = is between, false is not.</returns>
		internal bool IsBetween(DateTime val, DateTime start, DateTime end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between an integer range.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of integer range.</param>
		/// <param name="end">End of integer range.</param>
		/// <returns>True = is between, false is not.</returns>
		internal bool IsBetween(int val, int start, int end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Checks if time frame 1 and time frame 2 are overlapping
		/// </summary>
		/// <param name="start1">Start of time frame 1.</param>
		/// <param name="end1">End of time frame 1.</param>
		/// <param name="start2">Start of time frame 2.</param>
		/// <param name="end2">End of time frame 2.</param>
		/// <returns>True = is overlapped, false is not</returns>
		internal bool IsOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2) 
		{
			if((start1 <= end1) && (start2 <= end1)) return true;
			else return false;
		}
		#endregion ------------- Overlapping testers

		/// <summary>
		/// Indicates, whether the second bar group for not valid sports has to be
		/// visualized.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args.</param>
		private void cbInvalidSpots_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbInvalidSpots.Checked == true)
			{
				PrepareBlockingData();
			}
			PrintGraph();
			zGraph.Refresh();
		}
		/// <summary>
		/// Starts the report.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			//rptGenericChart rpt = new rptGenericChart(zGraph.GraphPane.Image);
			string strHeader1 = "Position Usage";
			string strHeader2 = "From: " + TimeFrameFrom.ToString("dd.MM.yy'/'HH:mm") + 
								"  To: " + TimeFrameTo.ToString("dd.MM.yy'/'HH:mm");
			rptSpotUsing rpt = new rptSpotUsing(strHeader1, strHeader2, YFlightPositions, YBlockingData, cbInvalidSpots.Checked,
				                                "Position Usage", "Number of Spot/Parking Stands",
				                                "Time (Hours)", "Occupied Spots", "Not valid Spots");
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();

		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmSpot_UsingRate_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
