using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using ZedGraph;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCheckin_statisticGraph.
	/// </summary>
	public class frmCheckin_statisticGraph : System.Windows.Forms.Form
	{
		#region _MyMembers
		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strAftWhere = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO') OR (TIFD BETWEEN '@@FROM' AND '@@TO'))) AND FTYP IN ('S', 'O') AND ADID='D'" ;
		/// <summary>
		/// The where statement for the dedicated check counters.
		/// </summary>
		private string strWhereCca = "WHERE FLNU IN (@@FLNU) AND CKIC<>' '";
		/// <summary>
		/// The Where statement for the common check-in counters.
		/// </summary>
		private string strCommonCCAWhere = "WHERE CKBS<='@@TO' AND CKES>='@@FROM' AND CKIC<>' ' AND CTYP='C'";
		/// <summary>
		/// Where statement for the CICTAB
		/// </summary>
		private string strWhereCic = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		/// <summary>
		/// Where statement for the STEV field of AFTTAB.
		/// </summary>
		private string strTerminalWhere = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the CCATAB
		/// </summary>
		private ITable myCCA;
		/// <summary>
		/// ITable object for the PSTTAB
		/// </summary>
		private ITable myCIC;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Chart values for the positions
		/// </summary>
		private double [] YCheckinCounters;
		/// <summary>
		/// Chart values for the blockings/none validities
		/// </summary>
		private double [] YBlockingData;
		/// <summary>
		/// Loaded Timeframe From 
		/// </summary>
		private DateTime TimeFrameFrom;
		/// <summary>
		/// Loaded Timeframe To 
		/// </summary>
		private DateTime TimeFrameTo;
		/// <summary>
		/// Necessary for none valid times
		/// </summary>
		private DateTime TIMENULL = new DateTime(0);
		/// <summary>
		/// To have an end of time value
		/// </summary>
		private DateTime ENDOFTIME = new DateTime(3000,12,31,0,0,0);
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

		#endregion _MyMembers
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblResults;
		private ZedGraph.ZedGraphControl zGraph;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.CheckBox cbInvalidCounters;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
		private System.Windows.Forms.TextBox txtTerminal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCheckin_statisticGraph()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCheckin_statisticGraph));
			this.panelTop = new System.Windows.Forms.Panel();
			this.txtTerminal = new System.Windows.Forms.TextBox();
			this.lblTerminal = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.cbInvalidCounters = new System.Windows.Forms.CheckBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.lblResults = new System.Windows.Forms.Label();
			this.zGraph = new ZedGraph.ZedGraphControl();
			this.tabResult = new AxTABLib.AxTAB();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.txtTerminal);
			this.panelTop.Controls.Add(this.lblTerminal);
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.cbInvalidCounters);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(928, 108);
			this.panelTop.TabIndex = 5;
			// 
			// txtTerminal
			// 
			this.txtTerminal.Location = new System.Drawing.Point(320, 35);
			this.txtTerminal.Name = "txtTerminal";
			this.txtTerminal.Size = new System.Drawing.Size(72, 20);
			this.txtTerminal.TabIndex = 3;
			this.txtTerminal.Text = "";
			// 
			// lblTerminal
			// 
			this.lblTerminal.Location = new System.Drawing.Point(256, 38);
			this.lblTerminal.Name = "lblTerminal";
			this.lblTerminal.Size = new System.Drawing.Size(56, 23);
			this.lblTerminal.TabIndex = 31;
			this.lblTerminal.Text = "Terminal:";
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(336, 72);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 7;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(176, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 5;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// cbInvalidCounters
			// 
			this.cbInvalidCounters.Location = new System.Drawing.Point(96, 32);
			this.cbInvalidCounters.Name = "cbInvalidCounters";
			this.cbInvalidCounters.Size = new System.Drawing.Size(152, 24);
			this.cbInvalidCounters.TabIndex = 2;
			this.cbInvalidCounters.Text = "&Show invalid counters";
			this.cbInvalidCounters.CheckedChanged += new System.EventHandler(this.cbInvalidCounters_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(472, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(472, 72);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(96, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 4;
			this.btnLoadPrint.Text = "&Load";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(256, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// lblResults
			// 
			this.lblResults.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 108);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(928, 16);
			this.lblResults.TabIndex = 6;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// zGraph
			// 
			this.zGraph.Dock = System.Windows.Forms.DockStyle.Fill;
			this.zGraph.IsEnableHPan = true;
			this.zGraph.IsEnableVPan = true;
			this.zGraph.IsEnableZoom = true;
			this.zGraph.IsScrollY2 = false;
			this.zGraph.IsShowContextMenu = true;
			this.zGraph.IsShowHScrollBar = false;
			this.zGraph.IsShowPointValues = false;
			this.zGraph.IsShowVScrollBar = false;
			this.zGraph.IsZoomOnMouseCenter = false;
			this.zGraph.Location = new System.Drawing.Point(0, 124);
			this.zGraph.Name = "zGraph";
			this.zGraph.PanButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.PanButtons2 = System.Windows.Forms.MouseButtons.Middle;
			this.zGraph.PanModifierKeys = System.Windows.Forms.Keys.Shift;
			this.zGraph.PanModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.PointDateFormat = "g";
			this.zGraph.PointValueFormat = "G";
			this.zGraph.ScrollMaxX = 0;
			this.zGraph.ScrollMaxY = 0;
			this.zGraph.ScrollMaxY2 = 0;
			this.zGraph.ScrollMinX = 0;
			this.zGraph.ScrollMinY = 0;
			this.zGraph.ScrollMinY2 = 0;
			this.zGraph.Size = new System.Drawing.Size(928, 478);
			this.zGraph.TabIndex = 7;
			this.zGraph.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
			this.zGraph.ZoomModifierKeys = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomStepFraction = 0.1;
			this.zGraph.Load += new System.EventHandler(this.zGraph_Load);
			// 
			// tabResult
			// 
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabResult.Location = new System.Drawing.Point(0, 490);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(928, 112);
			this.tabResult.TabIndex = 8;
			this.tabResult.Visible = false;
			// 
			// frmCheckin_statisticGraph
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(928, 602);
			this.Controls.Add(this.tabResult);
			this.Controls.Add(this.zGraph);
			this.Controls.Add(this.lblResults);
			this.Controls.Add(this.panelTop);
			this.Name = "frmCheckin_statisticGraph";
			this.Text = "Check-in Counter Usage";
			this.Load += new System.EventHandler(this.frmCheckin_statisticGraph_Load);
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmCheckin_statisticGraph_HelpRequested);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmCheckin_statisticGraph_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			SetupReportTabHeader();
			InitTimePickers();
			zGraph.Visible = false;
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			tabResult.ResetContent ();
			MakeCheckinData();
			PrepareFlightData();
			PrepareBlockingData();
			PrintGraph();
			RunReport();
			this.Cursor = Cursors.Arrow;
			zGraph.Visible = true;
			zGraph.Refresh();
		}
		/// <summary>
		/// Prepare Rotations
		/// </summary>
		private void MakeCheckinData()
		{
			InitTab();

			myAFT.Sort("RKEY", true);
			//int llCurr = 0;
			int ilStep = 1;
			StringBuilder sb = new StringBuilder(10000);
			for(int llCurr=0; llCurr< myCCA.Count;llCurr++)
			{
				string strValues = "";
						imDepartures++;
						//FLNU,CKIC,CKBS,CKES,CKBA,CKEA,URNO
						strValues += myCCA[llCurr]["FLNU"] + ",";
						strValues += myCCA[llCurr]["CKIC"] + ",";
						strValues += myCCA[llCurr]["CKBS"] + ",";
						strValues += myCCA[llCurr]["CKES"] + ",";
						strValues += myCCA[llCurr]["CKBA"] + ",";
						strValues += myCCA[llCurr]["CKEA"] + ",";
						strValues += myCCA[llCurr]["URNO"] + "\n";
						sb.Append(strValues);
			}//end of for
			tabResult.InsertBuffer(sb.ToString(), "\n");
		}
		/// <summary>
		/// Initialize the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,URNO";
			tabResult.LogicalFieldList = tabResult.HeaderString;
			tabResult.HeaderLengthString = "100,100,100,100,100,100,100";		
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			string[] strLabelHeaderEntry = new string[2];
			if((bmShowStev = Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,false)) == true)
			{
				lblTerminal.Text = strLabelHeaderEntry[0] + ":";
			}
			else
			{
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Prepares the chart values according to the
		/// Blocking/validity data
		/// </summary>
		private void PrepareBlockingData()
		{
			TimeSpan ts = TimeFrameTo - TimeFrameFrom;
			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			YBlockingData    = new double[ilHours];
			if(myCIC == null)
				return;
			for(int i = 0; i < myCIC.Count; i++)
			{
				DateTime vafr = TIMENULL;
				DateTime vato = TIMENULL;
				DateTime olStart = TIMENULL;
				DateTime olEnd = TIMENULL;
				DateTime olStart2 = TIMENULL;
				DateTime olEnd2 = TIMENULL;

				if(myCIC[i]["VAFR"] != "")
					vafr = myCIC[i].FieldAsDateTime("VAFR");
				if(myCIC[i]["VATO"] != "")
					vato = myCIC[i].FieldAsDateTime("VATO");
				else
				{
					vato = ENDOFTIME;
				}

				if(IsReallyOverlapped(vafr, vato, TimeFrameFrom, TimeFrameTo) || 
					IsWithIn(vafr, vato, TimeFrameFrom, TimeFrameTo))
				{
					//Valid is full inside the timefram so 
					// we need a timeframe1 before validfrom
					// and another one after valid to
					if(IsWithIn(vafr, vato, TimeFrameFrom, TimeFrameTo))
					{
						olStart = TimeFrameFrom;
						olEnd   = vafr;
						olStart2 = vato;
						olEnd2   = TimeFrameTo;
					}
						//Valid from start within the timeframe
					else if(IsBetween( vafr, TimeFrameFrom, TimeFrameTo) == true)
					{
						olStart = TimeFrameFrom;
						olEnd   = vafr;
					}
						//valid to ends within the timeframe
					else if(IsBetween(vato, TimeFrameFrom, TimeFrameTo) == true)
					{
						olStart = vato;
						olEnd   = TimeFrameTo;
					}
				}
				// Valid to ends before timeframe
				if(vato < TimeFrameFrom )
				{
					olStart = TimeFrameFrom;
					olEnd   = TimeFrameTo;
				}
				// Valid from start after timeframe
				if(vafr > TimeFrameTo)
				{
					olStart = TimeFrameFrom;
					olEnd   = TimeFrameTo;
				}
				//Now fill the double [] YBlockingData array
				if(olStart >= TimeFrameFrom && olEnd != TIMENULL)
				{
					int fromIDX = 0;
					int toIDX = 0;
					TimeSpan tmpTS = olStart - TimeFrameFrom;
					fromIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					tmpTS = olEnd - TimeFrameFrom;
					toIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					for(int j = fromIDX; j < toIDX; j++)
					{
						YBlockingData[j]++;
					}
				}

			}
		}
		/// <summary>
		/// Setup and draw the chart
		/// </summary>
		private void PrintGraph()
		{
			if(YCheckinCounters == null)
				return;
			
			// MaxBarValue stores maximum of all bars during the loaded timeframe
			int MaxBarValue = 0;
			ZedGraph.BarItem mySecondCurve = null;

			ZedGraph.GraphPane myPane = zGraph.GraphPane;
			myPane.GraphItemList.Clear();
			zGraph.Controls.Clear();
			myPane.CurveList.Clear();
			// Set the titles and axis labels
			myPane.Title = "Check-in Counter Usage";
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			myPane.XAxis.Title = "time (in hours)";
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.YAxis.Title = "Number of Check-in Counters allocated";
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			// Make up some random data points
			string [] str = new string[YCheckinCounters.Length];
			int ilCurrHour = 0;
			for(int i = 0; i < YCheckinCounters.Length; i++)
			{
				if(ilCurrHour == 24)
				{
					ilCurrHour = 0;
				}
				str[i] = ilCurrHour.ToString();
				ilCurrHour++;
			}
			MaxBarValue = 0;
			for (int i = 0; i < YCheckinCounters.Length; i++)
			{
				if(YCheckinCounters[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YCheckinCounters[i]);
			}
			// Add a bar to the graph
			ZedGraph.BarItem myCurve = myPane.AddBar( "Allocated Check-in Counters", null, YCheckinCounters, Color.White );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;
			if(cbInvalidCounters.Checked == true)
			{
				ZedGraph.BarItem myCurve2 = myPane.AddBar( "Not valid Check-in Counters", null, YBlockingData, Color.Red );
				mySecondCurve = myCurve2;
				myCurve2.Bar.Fill = new Fill( Color.Red, Color.White, Color.Red, 0F );
				myCurve2.Bar.Border.IsVisible = false;
				for (int i = 0; i < YBlockingData.Length; i++)
				{
					if(YBlockingData[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YBlockingData[i]);
				}
			}
			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = true;
			
			myPane.AxisChange( zGraph.CreateGraphics());
			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(YCheckinCounters.Length <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxBarValue)*MaxBarValue;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
							
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point
								if(cbInvalidCounters.Checked == false)
								{
									text.Location.AlignH =  AlignH.Center;
								}
								else
								{
									if(curve == myCurve)
									{
										text.Location.AlignH =  AlignH.Right;
									}
									if(curve == mySecondCurve)
									{
										text.Location.AlignH =  AlignH.Left;
									}
								}
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}
					ord++;
				}
			}
			zGraph.Show();
			zGraph.Refresh();
		}
		#region ------------- Overlapping testers
		/// <summary>
		/// Detects overlapping when start of time frame 1 and frame 2 are not equal.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is overlapped, false not</returns>
		internal bool IsReallyOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2)
		{
			if((start1 < end2) && (start2 < end1)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether time frame 1 is within time frame 2.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is within, false = outside</returns>
		internal bool IsWithIn(DateTime start1, DateTime end1, DateTime start2, DateTime end2)	
		{
			if((start1 >= start2) && (end1 <= end2)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between a time frame.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of time frame.</param>
		/// <param name="end">End of time frame.</param>
		/// <returns>True = is between, false is not.</returns>
		internal bool IsBetween(DateTime val, DateTime start, DateTime end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between an integer range.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of integer range.</param>
		/// <param name="end">End of integer range.</param>
		/// <returns>True = is between, false is not.</returns>
		internal bool IsBetween(int val, int start, int end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Checks if time frame 1 and time frame 2 are overlapping
		/// </summary>
		/// <param name="start1">Start of time frame 1.</param>
		/// <param name="end1">End of time frame 1.</param>
		/// <param name="start2">Start of time frame 2.</param>
		/// <param name="end2">End of time frame 2.</param>
		/// <returns>True = is overlapped, false is not</returns>
		internal bool IsOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2) 
		{
			if((start1 <= end1) && (start2 <= end1)) return true;
			else return false;
		}
		#endregion ------------- Overlapping testers
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = imDepartures ;
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			//			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, 
			//				"Overnight Flights: NAT: " + strNatureCode + " APT: " + strAirportCode + " Duration: " + dtDuration.Value.ToShortTimeString(), 
			//				strSubHeader, "", 8);
			//			frmPrintPreview frm = new frmPrintPreview(rpt);
			//			frm.Show();
		}
		/// <summary>
		/// Prepare the chart values according to the 
		/// flight data
		/// </summary>
		private void PrepareFlightData()
		{
			//Prepare the hour data to get the timeframes in hours
			//to allocate the double list for the flight curve
			DateTime datFrom;
			DateTime datTo;
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 0,0,0);
			datTo   = new DateTime(datTo.Year, datTo.Month, datTo.Day, 23,59,59);
			TimeFrameFrom = datFrom;
			TimeFrameTo = datTo;
			TimeSpan ts = TimeFrameTo - TimeFrameFrom;
			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			YCheckinCounters = new double[ilHours];
			YBlockingData    = new double[ilHours];
			for(int i = 0; i < tabResult.GetLineCount(); i++)
			{
			//	FLNU,CKIC,CKBS,CKES,CKBA,CKEA,URNO;

				DateTime olStart = TIMENULL; 
				DateTime olEnd = TIMENULL;
				DateTime olckba, olckea;
				if(	tabResult.GetFieldValues(i, "CKBA") != "" && 
					tabResult.GetFieldValues(i, "CKEA") != "")
				{
					olckba = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "CKBA"));
					olckea = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "CKEA"));
					olStart = olckba;
					olEnd   = olckea;
				}
				else
				{
					olckba = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "CKBS"));
					olckea = UT.CedaFullDateToDateTime(tabResult.GetFieldValues(i, "CKES"));
					olStart = olckba;
					olEnd   = olckea;
				}

				if(olEnd == TIMENULL && olStart == TIMENULL)
				{
					;//Do nothing
				}
				else
				{
					//Fill the entire arry due to undefined end
					//and start earlier than timeframe and for 
					//the case that start and end exceed both the timeframe
					if(olStart != TIMENULL)
					{
						if((olEnd == TIMENULL && olStart < datFrom) || (olStart <= datFrom && olEnd >= datTo))
						{
							for(int j = 0; j < YCheckinCounters.Length; j++)
							{
								YCheckinCounters[j]++;
							}
						}
						else
						{
							//for further proceeding set olEnd = datTo
							if(olStart < TimeFrameFrom) 
								olStart = TimeFrameFrom;
							if(olEnd > TimeFrameTo) 
								olEnd = TimeFrameTo;
						}
						if(olStart >= datFrom && olEnd != TIMENULL)
						{
							int fromIDX = 0;
							int toIDX = 0;
							TimeSpan tmpTS = olStart - datFrom;
							fromIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
							tmpTS = olEnd - datFrom;
							toIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
							for(int j = fromIDX; j < toIDX; j++)
							{
								YCheckinCounters[j]++;
							}
						}
					}
				}
			}
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		/// 
		private void LoadReportData()
		{
			string strTmpWhere;
			string strTmpAFTWhere = strAftWhere;
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			myDB.Unbind("AFT");
			myDB.Unbind("CCA");
			myDB.Unbind ("CIC");
			//Load the data from AFTTAB
			myAFT = myDB.Bind("AFT", "AFT", "RKEY,FLNO,DES3,STOD,TIFA,TIFD,ACT3,VIA3,URNO,MING,STEV", "10,12,3,14,14,14,3,3,10,4,1", "RKEY,FLNO,DES3,STOD,TIFA,TIFD,ACT3,VIA3,URNO,MING,STEV");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.TimeFieldsInitiallyInUtc = true;

			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			//myAFT.Load(strTmpAFTWhere);
			//Prepare loop reading day by day
			// Extract the true Nature code from the selected entry of the combobox
			//Now reading day by day the flight records in order to 
			//not stress the database too much
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strAftWhere;
				if(strTerminal != "")
				{
					strTmpAFTWhere += strTerminalWhere;
				}
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@STEV",strTerminal);

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			myAFT.Sort("STOD", true);
			//----------------------------------------------------------------------
			//Load dedicated check-in counter according to the AFT.URNO->CCA.FLNU
	//		myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,URNO", "10,5,14,14,14,14,14", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,URNO");
			myCCA.Clear();
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) ilTotal = 1;
			string strFlnus = "";
			string strTmpCCAWhere = strWhereCca;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus += myAFT[i]["URNO"] + ",";
				if((i % 300) == 0 && i > 0)
				{
					loops++;
					int percent = Convert.ToInt32((loops * 100)/ilTotal);
					if(percent >100)percent = 100;
					progressBar1.Value = percent;
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpCCAWhere = strWhereCca;
					strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
					myCCA.Load(strTmpCCAWhere);
					strFlnus = "";
				}
			}
			//Load the rest of the dedicated Counters.
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpCCAWhere = strWhereCca;
				strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
				myCCA.Load(strTmpCCAWhere);
				strFlnus = "";
			}
			myCCA.Sort("CKBS,CKBA,CKES,CKEA", true);
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
			//-----------------------------------------------------------------------------------
			//Now reading the CICTAB with the valid from /to records
		//	myDB.Unbind("CIC");
			myCIC = myDB.Bind("CIC", "CIC", "URNO,CNAM,VAFR,VATO", "10,5,14,14", "URNO,CNAM,VAFR,VATO");
			myCIC.Clear();
			myCIC.TimeFields = "VAFR,VATO";
			myCIC.TimeFieldsInitiallyInUtc = true;
			string strTmpWhereCic = strWhereCic;
			myCIC.Load("");
			myCIC.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			
			//rptGenericChart rpt = new rptGenericChart(zGraph.GraphPane.Image);
			string strHeader1 = "Check-in Counter Usage";
			string strHeader2 = "From: " + TimeFrameFrom.ToString("dd.MM.yy'/'HH:mm") + 
				"  To: " + TimeFrameTo.ToString("dd.MM.yy'/'HH:mm");
			rptSpotUsing rpt = new rptSpotUsing(strHeader1, strHeader2, YCheckinCounters, YBlockingData, cbInvalidCounters.Checked,"Check-in Counter Usage","Number of allocated Check-in Counters","Time (Hours)","Allocated Check-in Counters","Not Valid Check-in Counters");
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		private void cbInvalidCounters_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbInvalidCounters.Checked == true)
			{
				PrepareBlockingData();
			}
			PrintGraph();
			zGraph.Refresh();
		}

		private void zGraph_Load(object sender, System.EventArgs e)
		{
		
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			myDB.Unbind("CCA");
			myDB.Unbind("CIC");
			this.Close ();
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmCheckin_statisticGraph_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
} 
