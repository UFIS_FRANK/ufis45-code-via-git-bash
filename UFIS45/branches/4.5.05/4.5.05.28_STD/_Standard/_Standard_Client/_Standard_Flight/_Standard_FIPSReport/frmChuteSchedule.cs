using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using System.Text.RegularExpressions;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmChuteSchedule.
	/// </summary>
	public class frmChuteSchedule : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strAftWhere = "WHERE ( (STOD BETWEEN '@@FROM' AND '@@TO') OR " +
			" (TIFD BETWEEN '@@FROM' AND '@@TO')) AND FTYP IN ('S', 'O') AND ADID='D'" ;
		/// <summary>
		/// The where statement for the dedicated chutes.
		/// </summary>
		private string strDedicatedCHAWhere = "WHERE RURN IN (@@RURN) AND LNAM<>' ' AND DES3 <> '@@@'";
		/// <summary>
		/// The where statement for the STEV field.
		/// </summary>
		private string strTerminalWhere     = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "XXX,FLNO,ACTYPE,ROUTING,DES,STD,TERM,SUM,CHUTE,MINUTES,TIMEFRAME";
		private string strColWidths = "3,10,3,5,3,14,3,3,5,5,15";
		private string strColAlignment = "L,L,L,L,L,L,L,R,L,R,L";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader = "XXX,Flight,A/C  ,Routing  ,Des  ,STD  ,Term, Chutes,Chute from - to,Minutes,From - to";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "0,70,60,60,40,70,20,50,95,60,120";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// </summary>
		private string strExcelTabHeaderLens = "0,70,60,60,40,130,20,70,95,60,120";		
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the chutes table CHATAB
		/// </summary>
		private ITable myCHA;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		private ITable myBLK;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Necessary for none valid times
		/// </summary>
		private DateTime TIMENULL = new DateTime(0);
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;
		#endregion _My Members ------------------------------------------

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
		private System.Windows.Forms.TextBox txtTerminal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmChuteSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmChuteSchedule));
			this.panelTop = new System.Windows.Forms.Panel();
			this.txtTerminal = new System.Windows.Forms.TextBox();
			this.lblTerminal = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.txtTerminal);
			this.panelTop.Controls.Add(this.lblTerminal);
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(728, 136);
			this.panelTop.TabIndex = 0;
			// 
			// txtTerminal
			// 
			this.txtTerminal.Location = new System.Drawing.Point(92, 40);
			this.txtTerminal.Name = "txtTerminal";
			this.txtTerminal.Size = new System.Drawing.Size(124, 20);
			this.txtTerminal.TabIndex = 3;
			this.txtTerminal.Text = "";
			// 
			// lblTerminal
			// 
			this.lblTerminal.Location = new System.Drawing.Point(16, 43);
			this.lblTerminal.Name = "lblTerminal";
			this.lblTerminal.Size = new System.Drawing.Size(60, 15);
			this.lblTerminal.TabIndex = 31;
			this.lblTerminal.Text = "Terminal:";
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(396, 84);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 8;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(492, 64);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 20;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(492, 80);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 84);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 6;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 84);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 5;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 84);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 84);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 13;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 136);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(728, 294);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(728, 278);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(728, 278);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// tabExcelResult
			// 
			this.tabExcelResult.ContainingControl = this;
			this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
			this.tabExcelResult.Name = "tabExcelResult";
			this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
			this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
			this.tabExcelResult.TabIndex = 1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(728, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmChuteSchedule
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(728, 430);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmChuteSchedule";
			this.Text = "Chute Allocation";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmChuteSchedule_Closing);
			this.Load += new System.EventHandler(this.frmChuteSchedule_Load);
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmChuteSchedule_HelpRequested);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmChuteSchedule_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return;
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData("Report");
			PrepareReportData("Excel");			
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; 
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData("Report");
			PrepareReportData("Excel");
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;			
			tabResult.ColumnWidthString = strColWidths;
			tabResult.ColumnAlignmentString = strColAlignment;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;

			tabExcelResult.ResetContent();
			tabExcelResult.HeaderString = strTabHeader;
			tabExcelResult.LogicalFieldList = strLogicalFields;
			tabExcelResult.HeaderLengthString = strTabHeaderLens;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				bmShowStev = true;
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];				
				lblTerminal.Text = strTerminalLabel + ":";
				Helper.RemoveOrReplaceString(6,ref strTabHeader,',',strTerminalHeader,false);				
			}
			else
			{	
				Helper.RemoveOrReplaceString(6,ref strColWidths,',',"",true);
				Helper.RemoveOrReplaceString(6,ref strColAlignment,',',"",true);
				Helper.RemoveOrReplaceString(6,ref strLogicalFields,',',"",true);
				Helper.RemoveOrReplaceString(6,ref strTabHeader,',',"",true);
				Helper.RemoveOrReplaceString(6,ref strTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(6,ref strExcelTabHeaderLens,',',"",true);
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere = strAftWhere;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			myDB.Unbind("AFT");
			myDB.Unbind("DED_CHA");
			//Load the data from AFTTAB
			myAFT = myDB.Bind("AFT", "AFT", "FLNO,DES3,STOD,TIFD,ACT3,VIA3,URNO,STEV", "12,3,14,14,3,3,10,1", "FLNO,DES3,STOD,TIFD,ACT3,VIA3,URNO,STEV");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.TimeFieldsInitiallyInUtc = true;
			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strAftWhere;
				if(strTerminal != "")
				{
					strTmpAFTWhere += strTerminalWhere;
				}
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@STEV",strTerminal);

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			myAFT.Sort("STOD", true);
			//-------------------------------------------------------------------
			//Load chutes according to the AFT.URNO->CHA.RURN
			myDB.Unbind("DED_CHA");
			myCHA = myDB.Bind("DED_CHA", "CHA", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP,CHUF,CHUT,ANZ", "10,5,14,14,14,14,3,5,5,3", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP");
			//myCHA.TimeFields = "STOB,STOE,TIFB,TIFE";
			//myCHA.TimeFieldsInitiallyInUtc = true;
			myCHA.Clear();
			lblProgress.Text = "Loading Chute Data";
			lblProgress.Refresh();
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) ilTotal = 1;
			string strRurns = "";
			string strTmpCHAWhere = strDedicatedCHAWhere;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strRurns += myAFT[i]["URNO"] + ",";
				if((i % 300) == 0 && i > 0)
				{
					loops++;
					int percent = Convert.ToInt32((loops * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
					strRurns = strRurns.Remove(strRurns.Length-1, 1);
					strTmpCHAWhere = strDedicatedCHAWhere;
					strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
					myCHA.Load(strTmpCHAWhere);
					strRurns = "";
				}
			}
			//Load the rest of the dedicated Chutes.
			if(strRurns.Length > 0)
			{
				strRurns = strRurns.Remove(strRurns.Length-1, 1);
				strTmpCHAWhere = strDedicatedCHAWhere;
				strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
				myCHA.Load(strTmpCHAWhere);
				strRurns = "";
			}
			myCHA.Sort("STOB,TIFB,STOE,TIFE", true);
			myCHA.CreateIndex("RURN", "RURN");
			lblProgress.Text = "";
			progressBar1.Hide();
		}



		private void CompressChuteAllocations(string strFlightValues,IRow[] rows,StringBuilder sb)
		{
			// copy members to array list
			ArrayList myRows = new ArrayList(rows);
			// initialize members
			int ilAnz  = 1;
			for (int i = 0; i < myRows.Count; i++)
			{
				((IRow)myRows[i])["ANZ"] = 	ilAnz.ToString();
				((IRow)myRows[i])["CHUF"]=	((IRow)myRows[i])["LNAM"];
				((IRow)myRows[i])["CHUT"]=	((IRow)myRows[i])["LNAM"];
			}

			for (int i = 0; i < myRows.Count; i++)
			{
				ilAnz = int.Parse(((IRow)myRows[i])["ANZ"]);
				int ilCHUF = CalculateIntValueOfString(((IRow)myRows[i])["CHUF"]);
				int ilCHUT = CalculateIntValueOfString(((IRow)myRows[i])["CHUT"]);
				for (int j = 0; j < myRows.Count; j++)
				{					
					if (((IRow)myRows[i])["LTYP"] == ((IRow)myRows[j])["LTYP"] && 
						((IRow)myRows[i])["STOB"] == ((IRow)myRows[j])["STOB"] &&
						((IRow)myRows[i])["STOE"] == ((IRow)myRows[j])["STOE"])
					{
						if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF ||
							CalculateIntValueOfString(((IRow)myRows[j])["CHUF"]) - 1 == ilCHUT )
						{								
							ilAnz = int.Parse(((IRow)myRows[i])["ANZ"]) + int.Parse(((IRow)myRows[j])["ANZ"]);
							((IRow)myRows[i])["ANZ"] = ilAnz.ToString();

							if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF )
							{								
								((IRow)myRows[i])["CHUF"] = ((IRow)myRows[j])["CHUF"];
							}
							else
							{								
								((IRow)myRows[i])["CHUT"] = ((IRow)myRows[j])["CHUT"];
							}
							
							myRows.RemoveAt(j);
							if ( j < i ) 
							{
								i--;
							}
							i--;
							break;
						}
					}
				}
			}

			string strValues;
			string strChuteFrom;
			string strChuteTo;
			DateTime datChuteFrom;
			DateTime datChuteTo;
			TimeSpan timeSpan;
			string strTmpFrom;
			string strTmpTo;
			for (int i = 0; i < myRows.Count; i++)
			{
				strChuteFrom = "";
				strChuteTo = "";
				strTmpFrom = "";
				strTmpTo = "";
				datChuteFrom = TIMENULL;
				datChuteTo = TIMENULL;

				strValues = strFlightValues + ((IRow)myRows[i])["ANZ"] + ",";
				strValues += ((IRow)myRows[i])["CHUF"] + "-" + ((IRow)myRows[i])["CHUT"] + ",";
				
				strChuteFrom = ((IRow)myRows[i])["TIFB"] == "" ? ((IRow)myRows[i])["STOB"] : ((IRow)myRows[i])["TIFB"];
				strChuteTo   = ((IRow)myRows[i])["TIFE"] == "" ? ((IRow)myRows[i])["STOE"] : ((IRow)myRows[i])["TIFE"];  
								
				if(strChuteFrom != "")
				{
					if(strChuteFrom.Length == 12)
					{
						strChuteFrom = strChuteFrom + "00";
					}
					datChuteFrom = UT.CedaFullDateToDateTime(strChuteFrom);
				}
				if(strChuteTo != "")
				{
					if(strChuteTo.Length == 12)
					{
						strChuteTo = strChuteTo + "00";
					}
					datChuteTo = UT.CedaFullDateToDateTime(strChuteTo);
				}
				
				if(UT.IsTimeInUtc == false)				
				{			
					if(datChuteFrom != TIMENULL)
					{
						datChuteFrom = UT.UtcToLocal(datChuteFrom);
					}
					if(datChuteTo != TIMENULL)
					{
						datChuteTo = UT.UtcToLocal(datChuteTo);
					}
				}

				if(datChuteFrom != TIMENULL && datChuteTo != TIMENULL)
				{
					strTmpFrom = datChuteFrom.ToShortTimeString();
					strTmpTo = datChuteTo.ToShortTimeString();
					timeSpan = datChuteTo - datChuteFrom;
					strValues += timeSpan.TotalMinutes.ToString() + "," + strTmpFrom + "-" + strTmpTo + "\n";
				}
				else
				{
					strValues += "," + "\n";
				}
				sb.Append(strValues);
			}
		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData(string strReportOrExcel)
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			int ilLine = 0;
			myCHA = myDB["DED_CHA"];
			if(strReportOrExcel.Equals("Report") == true)
			{
				lblProgress.Text = "Preparing Data";
				lblProgress.Refresh();
				progressBar1.Visible = true;
				progressBar1.Value = 0;
			}
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			if(myCHA != null)
			{
				StringBuilder sb = new StringBuilder(10000);
				
				string strDateTimeFormat = strReportDateTimeFormat;
				if(strReportOrExcel.Equals("Excel") == true)
				{
					strDateTimeFormat = strExcelDateTimeFormat;
				}

				for(int i = 0; i < myAFT.Count; i++)
				{	
					imDepartures++;
					imTotalFlights++;
					if( i % 100 == 0)
					{
						int percent = Convert.ToInt32((i * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;
					}
					string strFlightValues = "";
					string strValues = "";
					ilLine = tabResult.GetLineCount() - 1;
					strFlightValues += "," + myAFT[i]["FLNO"] + ",";
					strFlightValues += myAFT[i]["ACT3"]+ ",";
					strFlightValues += myAFT[i]["VIA3"] + ",";
					strFlightValues += myAFT[i]["DES3"] + ",";
					strFlightValues += Helper.DateString(myAFT[i]["STOD"], strDateTimeFormat) + ",";
					if(bmShowStev == true)
					{
						strFlightValues += myAFT[i]["STEV"] + ",";
					}
					IRow [] rows = myCHA.RowsByIndexValue("RURN", myAFT[i]["URNO"]);
					if(rows.Length == 0)
					{
						strValues = strFlightValues + ",,,\n";
						sb.Append(strValues);
					}
					else
					{
						this.CompressChuteAllocations(strFlightValues,rows,sb);
					}
				}
				if(strReportOrExcel.Equals("Report") == true)
				{
					tabResult.InsertBuffer(sb.ToString(), "\n");
					tabResult.Refresh();
				}
				else if(strReportOrExcel.Equals("Excel") == true)
				{
					tabExcelResult.InsertBuffer(sb.ToString(), "\n");
					tabExcelResult.Refresh();
				}
			}
			
			if(strReportOrExcel.Equals("Report") == true)
			{
				progressBar1.Visible = false;
				lblProgress.Text = "";			
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			strReportHeader = frmMain.strPrefixVal + "Chute Allocation"; 
			strReportSubHeader = strSubHeader;
			rptFIPS rpt = new rptFIPS(tabResult,strReportHeader , strSubHeader, "", 10);
			rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);
			rpt.SetExcelExportTabResult(tabExcelResult);
			frmPrintPreview frm = new frmPrintPreview(rpt);			
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmChuteSchedule_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmChuteSchedule_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
			myDB.Unbind("DED_CHA");			
		}
		/// <summary>
		/// This function returns the int value of a string.
		/// </summary>
		/// <param name="strFlightValues"></param>
		/// <returns></returns>
		private int CalculateIntValueOfString(string strFlightValues)
		{
			string strNew = "";
			int total = 0;
			if(IsAlpha(strFlightValues))
			{
				char ab;
				IEnumerator strFltValEnum = strFlightValues.GetEnumerator();
				int CharCount = 0;
				while( strFltValEnum.MoveNext( ) )
				{
					CharCount++;
					ab = (char) strFltValEnum.Current;
					if(Char.IsNumber(ab))
						strNew += ab;
				}
				total = int.Parse(strNew);	
			}
			else
			{
				total = int.Parse(strFlightValues);
			}
			return total ;
		}
		/// <summary>
		/// Checks if a character is Alphabet or Numeric.
		/// </summary>
		/// <param name="strToCheck"></param>
		/// <returns></returns>
		public bool IsAlpha(string strToCheck)
		{
			Regex objAlphaPattern=new Regex("[A-Z]");

			return objAlphaPattern.IsMatch(strToCheck);
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmChuteSchedule_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmChuteSchedule_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmChuteSchedule_MouseLeave);
		}

		private void frmChuteSchedule_MouseWheel(object sender, MouseEventArgs e)
		{			
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmChuteSchedule_MouseEnter(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = true;
		}

		private void frmChuteSchedule_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmChuteSchedule_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLens,10,1,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmChuteSchedule_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
