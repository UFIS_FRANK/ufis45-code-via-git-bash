using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucView.
	/// </summary>
	public class ucView : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.ComboBox cbViews;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public delegate void ViewChangedEvent(string view);

		#region implementation data
		private ReportViewer	viewer	= new ReportViewer();
		private	frmView			view	= new frmView();
		public	event ViewChangedEvent viewEvent;	
		#endregion

		public ReportViewer	Viewer
		{
			get
			{
				return this.viewer;
			}
		}

		public string Report
		{
			get
			{
				return this.viewer.Report;
			}

			set
			{
				this.viewer.Report = value;
			}
		}

		public ucView()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.viewer.viewEvent +=new UserControls.ReportViewer.ViewChangedEvent(viewer_viewEvent);
			this.viewer.Load();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.viewer.viewEvent -=new UserControls.ReportViewer.ViewChangedEvent(viewer_viewEvent);
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnView = new System.Windows.Forms.Button();
			this.cbViews = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// btnView
			// 
			this.btnView.Location = new System.Drawing.Point(0, 8);
			this.btnView.Name = "btnView";
			this.btnView.TabIndex = 0;
			this.btnView.Text = "&View";
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// cbViews
			// 
			this.cbViews.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViews.Location = new System.Drawing.Point(96, 8);
			this.cbViews.Name = "cbViews";
			this.cbViews.Size = new System.Drawing.Size(184, 21);
			this.cbViews.Sorted = true;
			this.cbViews.TabIndex = 1;
			this.cbViews.SelectedIndexChanged += new System.EventHandler(this.cbViews_SelectedIndexChanged);
			// 
			// ucView
			// 
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.Controls.Add(this.cbViews);
			this.Controls.Add(this.btnView);
			this.Name = "ucView";
			this.Size = new System.Drawing.Size(304, 40);
			this.Load += new System.EventHandler(this.ucView_Load);
			this.ResumeLayout(false);

		}
		#endregion


		private void btnView_Click(object sender, System.EventArgs e)
		{
			string currentView = this.cbViews.Text;

			this.view.Text = "View " + this.Report;
			DialogResult result = this.view.ShowDialog();

			this.cbViews.Items.Clear();
			this.cbViews.Items.AddRange(this.viewer.Views);
			// check on default viewer
			if (this.cbViews.FindStringExact("<DEFAULT>") < 0)
				this.cbViews.Items.Add("<DEFAULT>");

			if (result == DialogResult.OK)
			{
				this.cbViews.Text = this.viewer.View;
			}
			else
			{
				if (this.cbViews.FindStringExact(currentView) < 0)
					this.viewer.View = "<DEFAULT>";
				else
					this.viewer.View = currentView;

				this.cbViews.Text = this.viewer.View;
			}

			this.cbViews.Refresh();

			if (result == DialogResult.OK)
			{
				if (this.viewEvent != null)
				{
					this.viewEvent(this.cbViews.Text);
				}
			}

		}

		private void ucView_Load(object sender, System.EventArgs e)
		{
			this.cbViews.Items.Clear();
			this.cbViews.Items.AddRange(this.viewer.Views);
			// check on default viewer
			if (this.cbViews.FindStringExact("<DEFAULT>") < 0)
				this.cbViews.Items.Add("<DEFAULT>");
			this.cbViews.Text = this.viewer.View;
			this.cbViews.Refresh();

			this.view.Viewer = this.viewer;
		
		}

		private void cbViews_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.viewer.View != this.cbViews.Text)
			{
				this.viewer.View = this.cbViews.Text;
				if (this.viewEvent != null)
				{
					this.viewEvent(this.viewer.View);
				}
			}
		}

		private void viewer_viewEvent(string view)
		{
			ucView_Load(this,null);

		}
	}
}
