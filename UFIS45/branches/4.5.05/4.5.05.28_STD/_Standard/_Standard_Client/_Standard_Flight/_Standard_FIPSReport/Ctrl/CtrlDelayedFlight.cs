﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;
using Ufis.Data;



namespace FIPS_Reports.Ctrl
{
    public class CtrlDelayedFlight
    {
        private bool _useDelayCodeDecs = false;

        public bool UseDelayCodeDecs
        {
            get { return _useDelayCodeDecs; }
        }

        private void CheckToUseDelayCodeDecs()
        {
            ITable mySys;
            MyDB.Unbind("SYS");
            mySys = MyDB.Bind("SYS", "SYS", "TANA,FINA", "14,14", "TANA,FINA");
            mySys.Clear();
            mySys.Load("WHERE TANA='DEN' AND FINA='DECS'");

            if (mySys.Count > 0) _useDelayCodeDecs = true;
            else _useDelayCodeDecs = false;
        }

        #region Where Clause
        //igu on 31/05/2011
        private const string strAFTWhereRaw = @"
 (<AFT.>FTYP='O' AND (
 (<AFT.>ADID='A' AND <AFT.>DES3 = '@@HOPO' AND (<AFT.>TIFA BETWEEN '@@FROM' AND '@@TO')<<ARRDELAYTIME>>) OR
 (<AFT.>ADID='D' AND <AFT.>ORG3 = '@@HOPO' AND (<AFT.>TIFD BETWEEN '@@FROM' AND '@@TO')<<DEPDELAYTIME>>) ) )";
        private const string strAFTWhereRawArr = @"
 (<AFT.>FTYP='O' AND (
 (<AFT.>ADID='A' AND <AFT.>DES3 = '@@HOPO' AND (<AFT.>TIFA BETWEEN '@@FROM' AND '@@TO')<<ARRDELAYTIME>>) ) )";
        private const string strAFTWhereRawDep = @"
 (<AFT.>FTYP='O' AND (
 (<AFT.>ADID='D' AND <AFT.>ORG3 = '@@HOPO' AND (<AFT.>TIFD BETWEEN '@@FROM' AND '@@TO')<<DEPDELAYTIME>>) ) )";
        private string[] arrAftWhereRaw = { strAFTWhereRawDep, strAFTWhereRawArr };
        private string[] arrAftOrder = { "STOD", "STOA" };
        //
        private const string WC_ARR_DELAY = " AND TO_DATE(TRIM(<AFT.>STOA),'YYYYMMDDHH24MISS')+<<DELAYTIME>>/(1440)<= TO_DATE( NVL(TRIM(<AFT.>ONBL),TRIM(<AFT.>STOA)), 'YYYYMMDDHH24MISS')";
        private const string WC_DEP_DELAY = " AND TO_DATE(TRIM(<AFT.>STOD),'YYYYMMDDHH24MISS')+<<DELAYTIME>>/(1440)<= TO_DATE( NVL(TRIM(<AFT.>OFBL),TRIM(<AFT.>STOD)), 'YYYYMMDDHH24MISS')";
        /*
 ((<AFT.>STOA BETWEEN '@@FROM' AND '@@TO') AND <AFT.>DES3 = '@@HOPO' AND <AFT.>FTYP = 'O') OR 
 ((<AFT.>TIFA BETWEEN '@@FROM' AND '@@TO') AND <AFT.>DES3 = '@@HOPO' AND <AFT.>FTYP = 'O') OR 
 ((<AFT.>STOD BETWEEN '@@FROM' AND '@@TO') AND <AFT.>ORG3 = '@@HOPO' AND <AFT.>FTYP = 'O') OR 
 ((<AFT.>TIFD BETWEEN '@@FROM' AND '@@TO') AND <AFT.>ORG3 = '@@HOPO' AND <AFT.>FTYP = 'O'))";
         * */
        private const string strAFTWhereDelayPart = " (<AFT.>DCD1='@@DEN' OR <AFT.>DCD2='@@DEN')";
        private const string strAFTWhereDelayQuery = " ((<AFT.>DCD1='@@DECA' OR <AFT.>DCD2='@@DECA') OR (<AFT.>DCD1='@@DECN' OR <AFT.>DCD2='@@DECN'))";
        private const string strAFTWhereDelayQueryNotActual = " ((  (<AFT.>DCD1='@@DECA' OR <AFT.>DCD2='@@DECA') and trim(<AFT.>DTD1) >= '@@DTD' ) OR ( (<AFT.>DCD1='@@DECN' OR <AFT.>DCD2='@@DECN') and trim(<AFT.>DTD2) >= '@@DTD' ))";

        private const string strWhereAirlinePart = " AND (<AFT.>ALC2='@@ALC2')";
        private const string strWhereAirline3Part = " AND (<AFT.>ALC3='@@ALC2')";
        private const string strTerminalWhere = " AND (<AFT.>STEV = '@@STEV')";
        #endregion

        #region DB Related
        /// <summary>
        /// The one and only IDatabase obejct.
        /// </summary>
        private IDatabase _MyDB;
        /// <summary>
        /// ITable object for the AFTTAB
        /// </summary>
        private ITable myAFT;
        /// <summary>
        /// ITable object for DCFTAB
        /// </summary>
        private ITable myDCF;
        /// <summary>
        /// ITable object for the nature codes of DENTAB
        /// </summary>
        private ITable myDEN;

        private IDatabase MyDB
        {
            get
            {
                if (_MyDB == null)
                    _MyDB = UT.GetMemDB();

                return _MyDB;
            }
        }
        #endregion

        private CtrlDelayedFlight()
        {
            CheckToUseDelayCodeDecs();
        }




        private static CtrlDelayedFlight _this = null;

        public static CtrlDelayedFlight GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlDelayedFlight();
            }
            return _this;
        }

        private bool IsEmpty(ref string str)
        {
            bool empty = false;
            if (string.IsNullOrEmpty(str)) empty = true;
            else if (str.Trim() == "") empty = true;

            if (empty) str = "";

            return empty;
        }

        private void PrepareWhereClauseWithDelay(
            string delayCodeDECA,
            string delayCodeDECN,
            string delayCodeDECS,
            long frDelayMinutes,
            string curAftWhere,
            string[] arrCurAftWhere,
            out string[] arrAFTWhere,
            out string strDCFWhere,
            Boolean bmCalculateActualDelay
            )
        {
            int i;

            string strAFTWhere = "";
            arrAFTWhere = new string[2];
            strDCFWhere = "";

            string strDCFWhereDelay = "";
            string strAFTWhereDelay = "";
            string delayMinutes = "";

            int dHour = 0;
            int dMin = int.Parse(frDelayMinutes.ToString());

            if (frDelayMinutes >= 60)
            {
                dHour = int.Parse(frDelayMinutes.ToString()) / 60;
            }


            if (dHour == 0)
            {
                if (dMin < 10)
                {
                    delayMinutes = "000" + dMin;
                }
                else
                {
                    delayMinutes = "00" + dMin;
                }



            }
            else if (dHour < 10)
            {
                dMin = (dHour * 60) - int.Parse(frDelayMinutes.ToString());
                if (dMin < 10)
                {
                    delayMinutes = "0" + dMin;
                }
                else
                {
                    delayMinutes = dMin.ToString();
                }
                delayMinutes = "0" + dHour + "" + dMin;

            }
            else
            {
                dMin = (dHour * 60) - int.Parse(frDelayMinutes.ToString());
                if (dMin < 10)
                {
                    delayMinutes = "0" + dMin;
                }
                else
                {
                    delayMinutes = dMin.ToString();
                }
                delayMinutes = dHour + "" + dMin;
            }








            //strDCFWhereDelay = string.Format("TO_NUMBER(NVL(<<DCF.>>DURA,'-1'))>={0}", frDelayMinutes);
            //strDCFWhereDelay = string.Format("<<DCF.>>DURA>='{0}'", (frDelayMinutes.ToString()).PadLeft(4, '0'));
            strDCFWhereDelay = "";

            if ((!IsEmpty(ref delayCodeDECA)) || (!IsEmpty(ref delayCodeDECN)) || (!IsEmpty(ref delayCodeDECS)))
            {
                //---
                //commented:igu on 19/05/2011
                //omit where clause part of AFTTAB.DCD1 and DCD2
                //if (delayCodeDECA.Trim().Length > 0 && delayCodeDECN.Trim().Length > 0)
                //{
                //    if (bmCalculateActualDelay)
                //    {
                //        strAFTWhereDelay = strAFTWhereDelayQuery;
                //        strAFTWhereDelay = strAFTWhereDelay.Replace("@@DECA", delayCodeDECA.Trim());
                //        strAFTWhereDelay = strAFTWhereDelay.Replace("@@DECN", delayCodeDECN.Trim());
                //        //strDecn = delayCodeDECA.Trim() + "<" + delayCodeDECN.Trim() + ">";
                //    }
                //    else
                //    {
                //        strAFTWhereDelay = strAFTWhereDelayQueryNotActual;
                //        strAFTWhereDelay = strAFTWhereDelay.Replace("@@DECA", delayCodeDECA.Trim());
                //        strAFTWhereDelay = strAFTWhereDelay.Replace("@@DECN", delayCodeDECN.Trim());



                //        strAFTWhereDelay = strAFTWhereDelay.Replace("@@DTD",delayMinutes );
                //    }



                //}
                //else
                //{

                //        strAFTWhereDelay = strAFTWhereDelayPart;
                //        if (delayCodeDECA.Trim().Length > 0)
                //        {
                //            strAFTWhereDelay = strAFTWhereDelay.Replace("@@DEN", delayCodeDECA.Trim());
                //            //strDecn = delayCodeDECA.Trim();
                //        }
                //        else if (delayCodeDECN.Trim().Length > 0)
                //        {
                //            strAFTWhereDelay = strAFTWhereDelay.Replace("@@DEN", delayCodeDECN.Trim());
                //            //strDecn = delayCodeDECN.Trim();
                //        }



                //}
                //---

                //igu on 31/05/2011
                string strWhere = string.Format(@"SELECT UNIQUE AFT.URNO 
                     FROM AFT{0} AFT LEFT OUTER JOIN 
                     DCF{0} DCF ON AFT.URNO=DCF.FURN WHERE " + curAftWhere, "TAB");
                string[] arrWhere = new string[2];
                for (i = 0; i < arrCurAftWhere.Length; i++)
                    arrWhere[i] = string.Format(@"SELECT UNIQUE AFT.URNO 
                         FROM AFT{0} AFT LEFT OUTER JOIN 
                         DCF{0} DCF ON AFT.URNO=DCF.FURN WHERE " + arrCurAftWhere[i], "TAB");


                string stConj = "";
                strDCFWhereDelay = " <AND>  (";
                if (delayCodeDECA.Trim() != "")
                {
                    strDCFWhereDelay += stConj + "DCF.DECA='" + delayCodeDECA.Trim() + "'";
                }
                if (delayCodeDECN.Trim() != "")
                {
                    if (delayCodeDECA.Trim() != "")
                        stConj = " AND ";
                    strDCFWhereDelay += stConj + "DCF.DECN='" + delayCodeDECN.Trim() + "'";
                }
                if (delayCodeDECS.Trim() != "")
                {
                    stConj = " AND ";
                    strDCFWhereDelay += stConj + "DCF.DECS='" + delayCodeDECS.Trim() + "'";
                }
                strDCFWhereDelay += ")";
                if (!bmCalculateActualDelay)
                {
                    if (frDelayMinutes > 0)
                    {

                        strDCFWhereDelay += " AND  trim(DCF.DURA) >= '" + delayMinutes + "'";
                    }
                }

                //igu on 19/05/2011
                //strAFTWhere = string.Format(@" WHERE URNO IN ({0} AND (({1}) OR ({2})))",
                //    strWhere,
                //    strAFTWhereDelay,
                //    strDCFWhereDelay).Replace("<AFT.>", "AFT.").Replace("<AND>", "");

                //igu on 19/05/2011
                strAFTWhere = string.Format(@" WHERE URNO IN ({0} AND ({1}))",
                    strWhere,
                    strDCFWhereDelay).Replace("<AFT.>", "AFT.").Replace("<AND>", "");
                for (i = 0; i < arrWhere.Length; i++)
                    arrAFTWhere[i] = string.Format(@" WHERE URNO IN ({0} AND ({1}))",
                        arrWhere[i],
                        strDCFWhereDelay).Replace("<AFT.>", "AFT.").Replace("<AND>", "");

                strDCFWhere = string.Format(" WHERE FURN IN ({0}{1})",
                    strWhere,
                    strDCFWhereDelay).Replace("<AFT.>", "AFT.").Replace("<<DCF.>>", "DCF.").Replace("<AND>", " AND ");
            }
            else
            {
                strAFTWhere = "WHERE " + curAftWhere.Replace("<AFT.>", "");
                for (i = 0; i < arrAFTWhere.Length; i++)
                    arrAFTWhere[i] = "WHERE " + arrCurAftWhere[i].Replace("<AFT.>", "");
                strDCFWhere = string.Format(" WHERE FURN IN (SELECT UNIQUE AFT.URNO FROM AFT{0} AFT WHERE {1}) {2}",
                    "TAB",
                    curAftWhere.Replace("<AFT.>", "AFT."),
                    strDCFWhereDelay).Replace("<<DCF.>>", "");
            }
        }

        /// <summary>
        /// Loads all necessary tables with filter criteria from the 
        /// database ==> to memoryDB.
        /// </summary>
        /// <param name="datFrom">From Date Time</param>
        /// <param name="datTo">To Date Time</param>
        /// <param name="delayCodes"></param>
        public void LoadData(DateTime dtFrom, DateTime dtTo,
            string delayCodeDECA,
            string delayCodeDECN,
            string delayCodeDECS,
            string strTerminal,
            string strAlc,
            long frDelayMinutes,
            Boolean bmCalculateActualDelay)
        {
            string[] arrTmpAFTWhere = arrAftWhereRaw; //igu on 31/05/2011
            int i; //igu on 31/05/2011
            string strTmpAFTWhere = strAFTWhereRaw;
            //Clear the tab
            //tabResult.ResetContent();
            //tabExcelResult.ResetContent();

            // In the first step change the times to UTC if this is
            // necessary.
            DateTime datFrom;
            DateTime datTo;

            string strDateFrom = "";
            string strDateTo = "";

            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtFrom);
                datTo = UT.LocalToUtc(dtTo);
            }
            else
            {
                datFrom = dtFrom;
                datTo = dtTo;
            }

            strDateFrom = UT.DateTimeToCeda(datFrom);
            strDateTo = UT.DateTimeToCeda(datTo);

            // Extract the true Nature code from the selected entry of the combobox

            //strAlc2 = txtAirline.Text;

            if (strAlc != "" && strAlc.Length == 2)
            {
                strTmpAFTWhere += strWhereAirlinePart;
                for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
                    arrTmpAFTWhere[i] += strWhereAirlinePart;
            }
            else
            {
                if (strAlc != "" && strAlc.Length == 3)
                {
                    strTmpAFTWhere += strWhereAirline3Part;
                    for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
                        arrTmpAFTWhere[i] += strWhereAirline3Part;
                }
            }

            if (strTerminal.Length > 0)
            {
                strTmpAFTWhere += strTerminalWhere;
                for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
                    arrTmpAFTWhere[i] += strTerminalWhere;
            }

            strTmpAFTWhere = strTmpAFTWhere.Replace("@@ALC2", strAlc);
            strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
            strTmpAFTWhere = strTmpAFTWhere.Replace("@@STEV", strTerminal);
            for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
            {
                arrTmpAFTWhere[i] = arrTmpAFTWhere[i].Replace("@@ALC2", strAlc);
                arrTmpAFTWhere[i] = arrTmpAFTWhere[i].Replace("@@HOPO", UT.Hopo);
                arrTmpAFTWhere[i] = arrTmpAFTWhere[i].Replace("@@STEV", strTerminal);
            }

            //string strAFTWhereDelayPart = "";
            //string strDCFWhereDelayPart = "";
            string strTmpDCFWhere = "";

            string stArrDelayTimeWhere = "";
            string stDepDelayTimeWhere = "";

            if (bmCalculateActualDelay)
            {
                if (frDelayMinutes > 0)
                {
                    stArrDelayTimeWhere = WC_ARR_DELAY.Replace("<<DELAYTIME>>", frDelayMinutes.ToString());
                    stDepDelayTimeWhere = WC_DEP_DELAY.Replace("<<DELAYTIME>>", frDelayMinutes.ToString());
                }
            }

            strTmpAFTWhere = strTmpAFTWhere.Replace("<<ARRDELAYTIME>>", stArrDelayTimeWhere)
                    .Replace("<<DEPDELAYTIME>>", stDepDelayTimeWhere);
            for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
                arrTmpAFTWhere[i] = arrTmpAFTWhere[i].Replace("<<ARRDELAYTIME>>", stArrDelayTimeWhere)
                    .Replace("<<DEPDELAYTIME>>", stDepDelayTimeWhere);


            PrepareWhereClauseWithDelay(delayCodeDECA, delayCodeDECN, delayCodeDECS,
                frDelayMinutes, strTmpAFTWhere, arrTmpAFTWhere,
                out arrTmpAFTWhere, out strTmpDCFWhere, bmCalculateActualDelay);

            //Load the data from AFTTAB
            MyDB.Unbind("AFT");
            myAFT = MyDB.Bind("AFT", "AFT",
                "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,GTA1,GTA2,GTD1,GTD2,REGN,PSTA,PSTD,TTYP,LAND,AIRB,ALC2,ALC3,STEV",
                "14,10,2,3,3,14,14,14,14,14,14,3,2,2,4,4,5,5,5,5,12,5,5,5,14,14,2,3,1",
                "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,GTA1,GTA2,GTD1,GTD2,REGN,PSTA,PSTD,TTYP,LAND,AIRB,ALC2,ALC3,STEV");
            myAFT.Clear();
            myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,LAND,AIRB";

            myAFT.TimeFieldsInitiallyInUtc = true;

            MyDB.Unbind("DCF");
            //myDCF = MyDB.Bind("DCF", "DCF", 
            //    "FURN,DSEQ,DECA,DECN,DECS,DURA,MEAN",
            //    "14,3,3,3,3,5,100",
            //    "FURN,DSEQ,DECA,DECN,DECS,DURA,MEAN"); //igu on 03/03/2010
            myDCF = MyDB.Bind("DCF", "DCF",
                "FURN,DSEQ,DECA,DECN,DECS,DURA,MEAN,DURN,USEQ,USEC,URNO",
                "14,3,3,3,3,5,100,10,2,32,10",
                "FURN,DSEQ,DECA,DECN,DECS,DURA,MEAN,DURN,USEQ,USEC,URNO"); //igu on 03/03/2010
            myDCF.Clear();
            //Prepare loop reading day by day
            //string strTerminal = "";
            //if (bmShowStev == true)
            //{
            //    strTerminal = txtTerminal.Text.Trim();
            //}
            for (i = 0; i < arrTmpAFTWhere.Length; i++) //igu on 31/05/2011
            {
                DateTime datReadFrom = datFrom;
                DateTime datReadTo;
                TimeSpan tsDays = (datTo - datFrom);
                //progressBar1.Value = 0;
                int ilTotal = Convert.ToInt32(tsDays.TotalDays);
                if (ilTotal == 0) ilTotal = 1;
                //lblProgress.Text = "Loading Data";
                //lblProgress.Refresh();
                //progressBar1.Show();
                int loopCnt = 1;
                do
                {
                    int percent = Convert.ToInt32((loopCnt * 100) / ilTotal);
                    if (percent > 100)
                        percent = 100;

                    if (ProgressEvent != null)
                    {
                        ProgressEvent(percent);
                    }
                    //progressBar1.Value = percent;
                    datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
                    if (datReadTo > datTo) datReadTo = datTo;
                    strDateFrom = UT.DateTimeToCeda(datReadFrom);
                    strDateTo = UT.DateTimeToCeda(datReadTo);
                    //patch the where statement according to the user's entry

                    //string strAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom).Replace("@@TO", strDateTo);
                    string strAFTOrder = " ORDER BY " + arrAftOrder[i];
                    string strAFTWhere = arrTmpAFTWhere[i].Replace("@@FROM", strDateFrom).Replace("@@TO", strDateTo);              

                    //Load the data from AFTTAB
                    myAFT.Load(strAFTWhere + strAFTOrder);

                    if (i == 0)
                    {
                        string strDCFWhere = strTmpDCFWhere.Replace("@@FROM", strDateFrom).Replace("@@TO", strDateTo);
                        myDCF.Load(strDCFWhere);
                    }
                    datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
                    loopCnt++;
                } while (datReadFrom <= datReadTo);
            }
            myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
            //lblProgress.Text = "";
            //progressBar1.Hide();
        }

        /// <summary>
        /// Check whethere delay time meet with filter criteria
        /// </summary>
        /// <param name="row">AFT Row</param>
        /// <param name="llTxtDelay">Delay in Minutes</param>
        /// <returns></returns>
        public static bool IsDelayTimeMeetFilterCriteria(IRow row, long llTxtDelay)
        {
            bool blIsDelayTimeOK = false;

            if (row["ADID"] == "A")
            {
                long llDelay = 0;
                if (row["STOA"] != "" && row["ONBL"] != "" && row["STOA"] != row["ONBL"])
                {
                    DateTime olStoa = UT.CedaFullDateToDateTime(row["STOA"]);
                    DateTime olOnbl = UT.CedaFullDateToDateTime(row["ONBL"]);
                    TimeSpan olDelay = olOnbl - olStoa;

                    llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                    if (llDelay >= llTxtDelay)
                    {
                        blIsDelayTimeOK = true;
                    }
                }
            }

            if (row["ADID"] == "D")
            {
                long llDelay = 0;
                if (row["STOD"] != "" && row["OFBL"] != "" && row["STOD"] != row["OFBL"])
                {
                    DateTime olStod = UT.CedaFullDateToDateTime(row["STOD"]);
                    DateTime olOfbl = UT.CedaFullDateToDateTime(row["OFBL"]);
                    TimeSpan olDelay = olOfbl - olStod;

                    llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                    if (llDelay >= llTxtDelay)
                    {
                        blIsDelayTimeOK = true;
                    }
                }
            }

            return blIsDelayTimeOK;
        }

        ////igu on 23/03/2011
        ////change the way of displaying of DelayData

        /// <summary>
        /// Compute maximum number of delays
        /// </summary>
        /// <param name="myDCF">AFT Table</param>
        /// <param name="myDCF">DCF Table</param>
        /// <param name="myDEN">DEN Table</param>
        /// <param name="UseDecs">Use Decs?</param>
        /// <param name="FlightDelaysCollectionObject">FlightDelaysCollection Object</param>
        /// <returns>DelayCount class</returns>
        public static DelayCount ComputeMaxDelayCnt(ITable myAFT, ITable myDCF, ITable myDEN,
            bool UseDecs, out FlightDelaysCollection FlightDelaysCollectionObject)
        {
            DelayCount delayCount = new DelayCount();

            FlightDelaysCollectionObject = new FlightDelaysCollection();

            myDCF.CreateIndex("FURN", "FURN");
            myDCF.Sort("FURN", true);

            for (int i = 0; i < myAFT.Count; i++)
            {
                DelayData delayData = null;
                DelayDataCollection delayDataCollection = new DelayDataCollection();

                IRow[] myRows = myDCF.RowsByIndexValue("FURN", myAFT[i]["URNO"]);
                if (myRows.Length == 0)
                {//No DCF Data. Take from flight data.
                    delayDataCollection.Add(new DelayData("1", myAFT[i]["DCD1"], myAFT[i]["DTD1"]));
                    delayDataCollection.Add(new DelayData("2", myAFT[i]["DCD2"], myAFT[i]["DTD2"]));
                }
                else
                {//Take from DCF Data
                    foreach (IRow dcfRow in myRows)
                    {
                        string delayCode = dcfRow["DECA"] + "|" + dcfRow["DECN"] + "|" + dcfRow["DECS"];
                        if (delayCode.Trim() == "||")
                        {
                            IRow[] denRows = myDEN.RowsByIndexValue("URNO", dcfRow["DURN"]);
                            if (denRows.Length > 0)
                            {
                                if (UseDecs)
                                {
                                    delayData = new DelayData(dcfRow["URNO"], denRows[0]["DECN"], denRows[0]["DECA"],
                                                              denRows[0]["DECS"], dcfRow["DURA"], dcfRow["DSEQ"],
                                                              dcfRow["USEQ"], dcfRow["USEC"]);
                                }
                                else
                                {
                                    delayData = new DelayData(dcfRow["URNO"], denRows[0]["DECN"], denRows[0]["DECA"],
                                                              dcfRow["DURA"], dcfRow["DSEQ"], dcfRow["USEQ"],
                                                              dcfRow["USEC"]);
                                }
                            }
                            else
                            {
                                if (UseDecs)
                                {
                                    delayData = new DelayData(dcfRow["URNO"], "", "",
                                                              "", dcfRow["DURA"], dcfRow["DSEQ"], dcfRow["USEQ"],
                                                              dcfRow["USEC"]);
                                }
                                else
                                {
                                    delayData = new DelayData(dcfRow["URNO"], "", "",
                                                              "", dcfRow["DSEQ"], dcfRow["USEQ"], dcfRow["USEC"]);
                                }
                            }
                        }
                        else
                        {
                            if (UseDecs)
                            {
                                delayData = new DelayData(dcfRow["URNO"], dcfRow["DECN"], dcfRow["DECA"],
                                                          dcfRow["DECS"], dcfRow["DURA"], dcfRow["DSEQ"],
                                                          dcfRow["USEQ"], dcfRow["USEC"]);
                            }
                            else
                            {
                                delayData = new DelayData(dcfRow["URNO"], dcfRow["DECN"], dcfRow["DECA"],
                                                          dcfRow["DURA"], dcfRow["DSEQ"], dcfRow["USEQ"],
                                                          dcfRow["USEC"]);
                            }
                        }
                        delayDataCollection.Add(delayData);
                    }
                }

                if (myAFT[i]["ADID"] == "A")
                {
                    delayCount.ArrDelayCodeCount = Math.Max(delayCount.ArrDelayCodeCount, delayDataCollection.Sort());
                }
                else
                {
                    delayCount.DepDelayCodeCount = Math.Max(delayCount.DepDelayCodeCount, delayDataCollection.Sort());
                }

                FlightDelays flightDelays = new FlightDelays(myAFT[i]["URNO"], delayDataCollection);
                FlightDelaysCollectionObject.Add(flightDelays);
            }

            return delayCount;
        }

        ////----

        // Define a delegate named LogHandler, which will encapsulate
        // any method that takes a string as the parameter and returns no value
        public delegate void ProgressHandler(int pctCompleted);

        // Define an Event based on the above Delegate
        public event ProgressHandler ProgressEvent;
    }

    ////igu on 23/03/2011
    ////change the way of displaying of DelayData  
    public class DelayCount
    {
        private int _arrDelayCodeCount;
        private int _depDelayCodeCount;

        public int ArrDelayCodeCount
        {
            set { _arrDelayCodeCount = value; }
            get { return _arrDelayCodeCount; }
        }

        public int DepDelayCodeCount
        {
            set { _depDelayCodeCount = value; }
            get { return _depDelayCodeCount; }
        }

        public DelayCount() : this(2, 2) { }

        public DelayCount(int arrDelayCount, int depDelayCount)
        {
            _arrDelayCodeCount = arrDelayCount;
            _depDelayCodeCount = depDelayCount;
        }
    }

    public class FlightDelaysCollection
    {
        private Dictionary<string, int> _flightIndex = new Dictionary<string, int>();
        private List<FlightDelays> _flightDelaysList = new List<FlightDelays>();

        public List<FlightDelays> FlightDelaysList
        {
            get { return _flightDelaysList; }
        }

        public void Add(FlightDelays FlightDelaysObject)
        {
            if (!_flightIndex.ContainsKey(FlightDelaysObject.FlightUrno)) //igu on 31/05/2011
            {
                _flightIndex.Add(FlightDelaysObject.FlightUrno, _flightDelaysList.Count);
                _flightDelaysList.Add(FlightDelaysObject);
            }
        }

        public FlightDelays GetFlightDelays(string FlightUrno)
        {
            int _index = 0;
            if (_flightIndex.TryGetValue(FlightUrno, out _index))
            {
                return _flightDelaysList[_index];
            }
            else
            {
                return null;
            }
        }
    }

    public class FlightDelays
    {
        private string _flightUrno = "";
        private DelayDataCollection _delayDataCollection = new DelayDataCollection();

        public string FlightUrno
        {
            set { _flightUrno = value; }
            get { return _flightUrno; }
        }

        public DelayDataCollection DelayDataCollection
        {
            set { _delayDataCollection = value; }
            get { return _delayDataCollection; }
        }

        public FlightDelays(string FlightUrno, DelayDataCollection DelayDataCollectionObject)
        {
            _flightUrno = FlightUrno;
            _delayDataCollection = DelayDataCollectionObject;
        }
    }

    public class DelayDataCollection
    {
        private List<DelayData> _delayDataList = new List<DelayData>();

        public List<DelayData> DelayDataList
        {
            get { return _delayDataList; }
        }

        public void Add(DelayData DelayDataObject)
        {
            _delayDataList.Add(DelayDataObject);
        }

        public int Sort()
        {
            int delayCount = 0;

            if (_delayDataList.Count > 0)
            {
                string currMainCode = string.Empty;

                List<DelayData> delayMainDataList = new List<DelayData>();
                List<DelayData> delaySubDataList = new List<DelayData>();

                _delayDataList.Sort(new DelayDataComparer());
                if (string.IsNullOrEmpty(_delayDataList[0].UserSeq))
                {
                    _delayDataList.Sort(new DelayDataComparer(DelayDataComparer.DelayDataComparerTypes.DSEQ_USEQ));

                    while (_delayDataList.Count > 0)
                    {
                        int index = 0;
                        DelayData currentDelayData = _delayDataList[index];
                        string decn = currentDelayData.NumericCode;
                        string deca = currentDelayData.AlphaNumericCode;
                        string decs = currentDelayData.Subcode;
                        string usec = currentDelayData.CreatedBy;
                        List<DelayData> foundDelays = new List<DelayData>();

                        foreach (DelayData delayData in _delayDataList)
                        {
                            if (decn == delayData.NumericCode &&
                                deca == delayData.AlphaNumericCode &&
                                delayData.Subcode == "" &&
                                usec == delayData.CreatedBy)
                            {
                                foundDelays.Add(delayData);
                            }
                        }

                        if (foundDelays.Count == 0)
                        {
                            delayMainDataList.Add(currentDelayData);
                            _delayDataList.Remove(currentDelayData);
                        }
                        else
                        {
                            currentDelayData = foundDelays[0];

                            decn = currentDelayData.NumericCode;
                            deca = currentDelayData.AlphaNumericCode;
                            usec = currentDelayData.CreatedBy;

                            delayMainDataList.Add(currentDelayData);
                            _delayDataList.Remove(currentDelayData);

                            List<DelayData> foundSubDelays = new List<DelayData>();

                            foreach (DelayData subDelayData in _delayDataList)
                            {
                                if (decn == subDelayData.NumericCode &&
                                   deca == subDelayData.AlphaNumericCode &&
                                   subDelayData.Subcode != "" &&
                                   usec == subDelayData.CreatedBy)
                                {
                                    foundSubDelays.Add(subDelayData);

                                    if (foundDelays.Count > 1)
                                    {
                                        break;
                                    }
                                }
                            }

                            foreach (DelayData subDelayFound in foundSubDelays)
                            {
                                currentDelayData = subDelayFound;

                                delayMainDataList.Add(currentDelayData);
                                _delayDataList.Remove(currentDelayData);
                            }
                        }
                    }

                    _delayDataList = delayMainDataList;
                }

                delayMainDataList = new List<DelayData>();

                //now sort again
                for (int i = 0; i < _delayDataList.Count; i++)
                {
                    DelayData curDelayData = _delayDataList[i];
                    DelayData nextDelayData = null;
                    if (i < _delayDataList.Count - 1)
                    {
                        nextDelayData = _delayDataList[i + 1];
                    }

                    if (string.IsNullOrEmpty(curDelayData.Subcode)) //Main Code
                    {
                        delayMainDataList.Add(curDelayData);
                        if (nextDelayData != null)
                        {
                            if (string.IsNullOrEmpty(nextDelayData.Subcode)) //Maincode
                            {
                                delaySubDataList.Add(new DelayData("", "", "", " ", "", "", "", ""));
                            }
                            else //Subcode
                            {
                                if (curDelayData.NumericCode == nextDelayData.NumericCode &&
                                    curDelayData.AlphaNumericCode == nextDelayData.AlphaNumericCode)
                                {
                                    delaySubDataList.Add(nextDelayData);
                                    i++;
                                }
                                else
                                {
                                    delaySubDataList.Add(new DelayData("", "", "", " ", "", "", "", ""));
                                }
                            }
                        }
                        else
                        {
                            delaySubDataList.Add(new DelayData("", "", "", " ", "", "", "", ""));
                        }
                    }
                    else //Subcode
                    {
                        delayMainDataList.Add(new DelayData("", "", ""));
                        delaySubDataList.Add(curDelayData);
                    }
                }

                delayCount = delayMainDataList.Count;

                delayMainDataList.AddRange(delaySubDataList);
                _delayDataList = delayMainDataList;
            }

            return delayCount;
        }
    }

    public class DelayData
    {
        public const string DEFAULT_DURA = "";

        string _urno = string.Empty;
        string _numericCode = string.Empty;
        string _alphaNumericCode = string.Empty;
        string _subcode = string.Empty;
        string _duration = string.Empty;
        string _dataSourceSeq = string.Empty;
        string _userSeq = string.Empty;
        string _createdBy = string.Empty;

        bool _useSubcode = true;
        bool _useDCF = true;

        private string NVL(string Exp1, string Exp2)
        {
            //if Exp1 is not null then return Exp1
            if (Exp1 != null)
            {
                return Exp1;
            }
            else if ((Exp1 == null) && (Exp2 != null))
            {
                //If oExp1 is null return oExp2
                return Exp2;
            }
            else
                //If both of them are null return nothing
                return null;
        }

        public string Urno
        {
            get { return _urno; }
            set { _urno = value; }
        }

        public string NumericCode
        {
            get { return _numericCode; }
            set { _numericCode = (string)NVL(value, ""); }
        }

        public string AlphaNumericCode
        {
            get { return _alphaNumericCode; }
            set { _alphaNumericCode = NVL(value, ""); }
        }

        public string Subcode
        {
            get { return _subcode; }
            set { _subcode = NVL(value, ""); }
        }

        public string Duration
        {
            get { return _duration; }
            set { _duration = NVL(value, ""); }
        }

        public string DataSourceSeq
        {
            get { return _dataSourceSeq; }
            set { _dataSourceSeq = NVL(value, ""); }
        }

        public string UserSeq
        {
            get { return _userSeq; }
            set { _userSeq = NVL(value, ""); }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = NVL(value, ""); }
        }

        public string FullCode
        {
            get
            {
                string fullCode = string.Empty;

                if (_useDCF)
                {
                    fullCode = _alphaNumericCode + "|" + _numericCode;
                    if (_useSubcode)
                    {
                        fullCode += "|" + _subcode;
                    }
                }
                else
                {
                    fullCode = _alphaNumericCode;
                }

                if (fullCode.Replace("|", "").Trim().Length == 0)
                {
                    fullCode = string.Empty;
                }

                return fullCode;
            }
        }

        public DelayData(string urno, string delayCode, string duration)
            : this(urno, "", delayCode, "", duration, "", "", "")
        {
            _useDCF = false;
        }

        public DelayData(string urno, string numericCode, string alphaNumericCode,
            string duration, string dataSourceSeq, string userSeq, string createdBy)
            : this(urno, numericCode, alphaNumericCode, "", duration, "", "", createdBy)
        {
            _useSubcode = false;
        }

        public DelayData(string urno, string numericCode, string alphaNumericCode,
            string subcode, string duration, string dataSourceSeq, string userSeq,
            string createdBy)
        {
            _urno = urno;
            _numericCode = numericCode;
            _alphaNumericCode = alphaNumericCode;
            _subcode = subcode;
            _duration = duration;
            _dataSourceSeq = dataSourceSeq;
            _userSeq = userSeq;
            _createdBy = createdBy;
        }
    }

    public class DelayDataComparer : IComparer<DelayData>
    {

        #region IComparer<DelayData> Members

        public enum DelayDataComparerTypes
        {
            DSEQ_USEQ,
            USEQ
        }

        private DelayDataComparerTypes _delayDataComparerTypes;

        public DelayDataComparer()
            : this(DelayDataComparerTypes.USEQ) { }

        public DelayDataComparer(DelayDataComparerTypes delayDataComparerTypes)
        {
            _delayDataComparerTypes = delayDataComparerTypes;
        }

        public int Compare(DelayData x, DelayData y)
        {
            //int result = (x._tsDelayDura.CompareTo(y._tsDelayDura));
            //if (result == 0)
            //{
            //    result = x.Urno.CompareTo(y.Urno);
            //}

            int result = 0;
            if (_delayDataComparerTypes == DelayDataComparerTypes.DSEQ_USEQ)
            {
                result = (x.DataSourceSeq.CompareTo(y.DataSourceSeq));
            }
            if (result == 0)
            {
                result = x.UserSeq.CompareTo(y.UserSeq);
            }

            return result;
        }

        #endregion
    }
    ////----
}

