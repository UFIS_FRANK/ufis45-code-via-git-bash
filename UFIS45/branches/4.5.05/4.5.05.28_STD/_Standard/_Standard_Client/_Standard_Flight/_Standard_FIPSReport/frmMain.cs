using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;
using System.Diagnostics;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region My Members
		/// <summary>
		/// The one and only IDatabase member reference within 
		/// the application. This is reused in all other modules.
		/// </summary>
		private IDatabase myDB;
		private ITable myCfg;
		private int isCustomerPVG = -1;
		public string    [] args;

		#endregion My Members
		public static string strPrefixVal = ""; 

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblVersion;
		private System.Windows.Forms.Button btnFlightsByNatureCode;
		private System.Windows.Forms.CheckBox cbTimeInLocal;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblServer;
		private System.Windows.Forms.Button btnCounterSchedule;
		private System.Windows.Forms.Button btnStatisticCancelledFlights;
		private System.Windows.Forms.Button btnBeltAllocations;
		private System.Windows.Forms.Button btnOvernightFlights;
		private System.Windows.Forms.Button btnInventory;
		private System.Windows.Forms.Button btnGPUUsage;
		private System.Windows.Forms.Button btnDailyFlightLog;
		private System.Windows.Forms.Button btnSpotUsingRate;
		private System.Windows.Forms.Button btnStatistAccToArrDep;
		private System.Windows.Forms.Button btnStatistCancelledFlightSummary;
		private System.Windows.Forms.Button btnStatistOvernightArrSummary;
		private System.Windows.Forms.Button btnStatistAccArrDep;
		private System.Windows.Forms.Button btnAircraftMovementsPerHour;
		private System.Windows.Forms.Button btnSeatStatisDomIntComb;
		private System.Windows.Forms.Button btnStatistDomesticIntComb;
		private System.Windows.Forms.Button btnStatistRushHour;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button7;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Button button_ConfigLoadPax;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Button buttonChuteAllocation;
        private Button btnNightOperations;
        private Button btnNightOperationsSummary;
        private Button btnDeIcing;
        private Button btnFlightAllocation;
		private AxAATLOGINLib.AxAatLogin LoginControl;

		/// <summary>
		/// Constructor of the main form.
		/// </summary>
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				} 
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblServer = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.cbTimeInLocal = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFlightAllocation = new System.Windows.Forms.Button();
            this.btnDeIcing = new System.Windows.Forms.Button();
            this.btnNightOperationsSummary = new System.Windows.Forms.Button();
            this.btnNightOperations = new System.Windows.Forms.Button();
            this.buttonChuteAllocation = new System.Windows.Forms.Button();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.button_ConfigLoadPax = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSeatStatisDomIntComb = new System.Windows.Forms.Button();
            this.btnStatistDomesticIntComb = new System.Windows.Forms.Button();
            this.btnStatistRushHour = new System.Windows.Forms.Button();
            this.btnAircraftMovementsPerHour = new System.Windows.Forms.Button();
            this.btnStatistAccArrDep = new System.Windows.Forms.Button();
            this.btnStatistOvernightArrSummary = new System.Windows.Forms.Button();
            this.btnStatistCancelledFlightSummary = new System.Windows.Forms.Button();
            this.btnStatistAccToArrDep = new System.Windows.Forms.Button();
            this.btnSpotUsingRate = new System.Windows.Forms.Button();
            this.btnDailyFlightLog = new System.Windows.Forms.Button();
            this.btnGPUUsage = new System.Windows.Forms.Button();
            this.btnInventory = new System.Windows.Forms.Button();
            this.btnOvernightFlights = new System.Windows.Forms.Button();
            this.btnBeltAllocations = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnStatisticCancelledFlights = new System.Windows.Forms.Button();
            this.btnCounterSchedule = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFlightsByNatureCode = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblServer);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblVersion);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.LoginControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(722, 64);
            this.panel1.TabIndex = 0;
            // 
            // lblServer
            // 
            this.lblServer.Location = new System.Drawing.Point(128, 4);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(136, 16);
            this.lblServer.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(80, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Server:";
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(608, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(96, 23);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "Version: 4.5.1.2";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(480, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "� 2005, 2008 UFIS Airport Solutions GmbH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // LoginControl
            // 
            this.LoginControl.Enabled = true;
            this.LoginControl.Location = new System.Drawing.Point(0, 0);
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            this.LoginControl.RightToLeft = false;
            this.LoginControl.Size = new System.Drawing.Size(100, 50);
            this.LoginControl.TabIndex = 0;
            this.LoginControl.Text = null;
            this.LoginControl.Visible = false;
            // 
            // cbTimeInLocal
            // 
            this.cbTimeInLocal.Checked = true;
            this.cbTimeInLocal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTimeInLocal.Location = new System.Drawing.Point(500, 401);
            this.cbTimeInLocal.Name = "cbTimeInLocal";
            this.cbTimeInLocal.Size = new System.Drawing.Size(124, 16);
            this.cbTimeInLocal.TabIndex = 23;
            this.cbTimeInLocal.Text = "Times in Local Time";
            this.cbTimeInLocal.CheckedChanged += new System.EventHandler(this.cbTimeInLocal_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel2.Controls.Add(this.btnFlightAllocation);
            this.panel2.Controls.Add(this.btnDeIcing);
            this.panel2.Controls.Add(this.btnNightOperationsSummary);
            this.panel2.Controls.Add(this.btnNightOperations);
            this.panel2.Controls.Add(this.buttonChuteAllocation);
            this.panel2.Controls.Add(this.buttonHelp);
            this.panel2.Controls.Add(this.button_ConfigLoadPax);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.btnSeatStatisDomIntComb);
            this.panel2.Controls.Add(this.btnStatistDomesticIntComb);
            this.panel2.Controls.Add(this.btnStatistRushHour);
            this.panel2.Controls.Add(this.btnAircraftMovementsPerHour);
            this.panel2.Controls.Add(this.btnStatistAccArrDep);
            this.panel2.Controls.Add(this.btnStatistOvernightArrSummary);
            this.panel2.Controls.Add(this.btnStatistCancelledFlightSummary);
            this.panel2.Controls.Add(this.btnStatistAccToArrDep);
            this.panel2.Controls.Add(this.btnSpotUsingRate);
            this.panel2.Controls.Add(this.btnDailyFlightLog);
            this.panel2.Controls.Add(this.btnGPUUsage);
            this.panel2.Controls.Add(this.btnInventory);
            this.panel2.Controls.Add(this.btnOvernightFlights);
            this.panel2.Controls.Add(this.btnBeltAllocations);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.btnStatisticCancelledFlights);
            this.panel2.Controls.Add(this.btnCounterSchedule);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnFlightsByNatureCode);
            this.panel2.Controls.Add(this.cbTimeInLocal);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(722, 430);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // btnFlightAllocation
            // 
            this.btnFlightAllocation.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnFlightAllocation.Location = new System.Drawing.Point(372, 341);
            this.btnFlightAllocation.Name = "btnFlightAllocation";
            this.btnFlightAllocation.Size = new System.Drawing.Size(324, 23);
            this.btnFlightAllocation.TabIndex = 37;
            this.btnFlightAllocation.Text = "Flight Allocation Plan";
            this.btnFlightAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFlightAllocation.Click += new System.EventHandler(this.btnFlightAllocation_Click);
            // 
            // btnDeIcing
            // 
            this.btnDeIcing.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDeIcing.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDeIcing.Location = new System.Drawing.Point(15, 341);
            this.btnDeIcing.Name = "btnDeIcing";
            this.btnDeIcing.Size = new System.Drawing.Size(324, 23);
            this.btnDeIcing.TabIndex = 36;
            this.btnDeIcing.Text = "De-icing";
            this.btnDeIcing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeIcing.UseVisualStyleBackColor = false;
            this.btnDeIcing.Click += new System.EventHandler(this.btnDeIcing_Click);
            // 
            // btnNightOperationsSummary
            // 
            this.btnNightOperationsSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNightOperationsSummary.Location = new System.Drawing.Point(372, 312);
            this.btnNightOperationsSummary.Name = "btnNightOperationsSummary";
            this.btnNightOperationsSummary.Size = new System.Drawing.Size(324, 23);
            this.btnNightOperationsSummary.TabIndex = 35;
            this.btnNightOperationsSummary.Text = "Night Operations (Summary)";
            this.btnNightOperationsSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNightOperationsSummary.Click += new System.EventHandler(this.btnNightOperationsSummary_Click);
            // 
            // btnNightOperations
            // 
            this.btnNightOperations.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNightOperations.Location = new System.Drawing.Point(372, 285);
            this.btnNightOperations.Name = "btnNightOperations";
            this.btnNightOperations.Size = new System.Drawing.Size(324, 23);
            this.btnNightOperations.TabIndex = 34;
            this.btnNightOperations.Text = "Night Operations";
            this.btnNightOperations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNightOperations.Click += new System.EventHandler(this.btnNightOperations_Click);
            // 
            // buttonChuteAllocation
            // 
            this.buttonChuteAllocation.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.buttonChuteAllocation.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonChuteAllocation.Location = new System.Drawing.Point(16, 369);
            this.buttonChuteAllocation.Name = "buttonChuteAllocation";
            this.buttonChuteAllocation.Size = new System.Drawing.Size(324, 23);
            this.buttonChuteAllocation.TabIndex = 11;
            this.buttonChuteAllocation.Text = "Chute Allocation";
            this.buttonChuteAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonChuteAllocation.UseVisualStyleBackColor = false;
            this.buttonChuteAllocation.Click += new System.EventHandler(this.buttonChuteAllocation_Click);
            // 
            // buttonHelp
            // 
            this.buttonHelp.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(628, 398);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(68, 23);
            this.buttonHelp.TabIndex = 24;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.UseVisualStyleBackColor = false;
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // button_ConfigLoadPax
            // 
            this.button_ConfigLoadPax.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_ConfigLoadPax.Location = new System.Drawing.Point(372, 228);
            this.button_ConfigLoadPax.Name = "button_ConfigLoadPax";
            this.button_ConfigLoadPax.Size = new System.Drawing.Size(324, 23);
            this.button_ConfigLoadPax.TabIndex = 19;
            this.button_ConfigLoadPax.Text = "Load and Pax";
            this.button_ConfigLoadPax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ConfigLoadPax.Click += new System.EventHandler(this.button_ConfigLoadPax_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button8.Location = new System.Drawing.Point(16, 398);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(324, 23);
            this.button8.TabIndex = 33;
            this.button8.Text = "Delay Reasons (Summary)";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button7.Location = new System.Drawing.Point(372, 284);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(324, 23);
            this.button7.TabIndex = 21;
            this.button7.Text = "Reason for Changes";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Location = new System.Drawing.Point(372, 369);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(172, 23);
            this.button2.TabIndex = 22;
            this.button2.Text = "Abnormal Flights at Pudong Airport";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(372, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(324, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Unavailable Resources ";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSeatStatisDomIntComb
            // 
            this.btnSeatStatisDomIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSeatStatisDomIntComb.Location = new System.Drawing.Point(372, 256);
            this.btnSeatStatisDomIntComb.Name = "btnSeatStatisDomIntComb";
            this.btnSeatStatisDomIntComb.Size = new System.Drawing.Size(324, 23);
            this.btnSeatStatisDomIntComb.TabIndex = 20;
            this.btnSeatStatisDomIntComb.Text = "Seats According to Airline/Destination/Combined";
            this.btnSeatStatisDomIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSeatStatisDomIntComb.Click += new System.EventHandler(this.btnSeatStatisDomIntComb_Click);
            // 
            // btnStatistDomesticIntComb
            // 
            this.btnStatistDomesticIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistDomesticIntComb.Location = new System.Drawing.Point(16, 144);
            this.btnStatistDomesticIntComb.Name = "btnStatistDomesticIntComb";
            this.btnStatistDomesticIntComb.Size = new System.Drawing.Size(324, 23);
            this.btnStatistDomesticIntComb.TabIndex = 4;
            this.btnStatistDomesticIntComb.Text = "Flights According to Domestic/International/Combined";
            this.btnStatistDomesticIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistDomesticIntComb.Click += new System.EventHandler(this.btnStatistDomesticIntComb_Click);
            // 
            // btnStatistRushHour
            // 
            this.btnStatistRushHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistRushHour.Location = new System.Drawing.Point(372, 60);
            this.btnStatistRushHour.Name = "btnStatistRushHour";
            this.btnStatistRushHour.Size = new System.Drawing.Size(324, 23);
            this.btnStatistRushHour.TabIndex = 13;
            this.btnStatistRushHour.Text = "Check-in Counter Usage";
            this.btnStatistRushHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistRushHour.Click += new System.EventHandler(this.btnStatistRushHour_Click);
            // 
            // btnAircraftMovementsPerHour
            // 
            this.btnAircraftMovementsPerHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAircraftMovementsPerHour.Location = new System.Drawing.Point(16, 172);
            this.btnAircraftMovementsPerHour.Name = "btnAircraftMovementsPerHour";
            this.btnAircraftMovementsPerHour.Size = new System.Drawing.Size(324, 23);
            this.btnAircraftMovementsPerHour.TabIndex = 5;
            this.btnAircraftMovementsPerHour.Text = "Aircraft Movements per Hour";
            this.btnAircraftMovementsPerHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAircraftMovementsPerHour.Click += new System.EventHandler(this.btnAircraftMovementsPerHour_Click);
            // 
            // btnStatistAccArrDep
            // 
            this.btnStatistAccArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistAccArrDep.Location = new System.Drawing.Point(16, 60);
            this.btnStatistAccArrDep.Name = "btnStatistAccArrDep";
            this.btnStatistAccArrDep.Size = new System.Drawing.Size(324, 23);
            this.btnStatistAccArrDep.TabIndex = 1;
            this.btnStatistAccArrDep.Text = "Flights According to Arrival / Departure ";
            this.btnStatistAccArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistAccArrDep.Click += new System.EventHandler(this.btnStatistAccArrDep_Click);
            // 
            // btnStatistOvernightArrSummary
            // 
            this.btnStatistOvernightArrSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistOvernightArrSummary.Location = new System.Drawing.Point(16, 228);
            this.btnStatistOvernightArrSummary.Name = "btnStatistOvernightArrSummary";
            this.btnStatistOvernightArrSummary.Size = new System.Drawing.Size(324, 23);
            this.btnStatistOvernightArrSummary.TabIndex = 7;
            this.btnStatistOvernightArrSummary.Text = "Overnight Flights (Summary)";
            this.btnStatistOvernightArrSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistOvernightArrSummary.Click += new System.EventHandler(this.btnStatistOvernightArrSummary_Click);
            // 
            // btnStatistCancelledFlightSummary
            // 
            this.btnStatistCancelledFlightSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistCancelledFlightSummary.Location = new System.Drawing.Point(16, 284);
            this.btnStatistCancelledFlightSummary.Name = "btnStatistCancelledFlightSummary";
            this.btnStatistCancelledFlightSummary.Size = new System.Drawing.Size(324, 23);
            this.btnStatistCancelledFlightSummary.TabIndex = 9;
            this.btnStatistCancelledFlightSummary.Text = "Cancelled Flights (Summary)";
            this.btnStatistCancelledFlightSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistCancelledFlightSummary.Click += new System.EventHandler(this.btnStatistCancelledFlightSummary_Click);
            // 
            // btnStatistAccToArrDep
            // 
            this.btnStatistAccToArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatistAccToArrDep.Location = new System.Drawing.Point(16, 88);
            this.btnStatistAccToArrDep.Name = "btnStatistAccToArrDep";
            this.btnStatistAccToArrDep.Size = new System.Drawing.Size(324, 23);
            this.btnStatistAccToArrDep.TabIndex = 2;
            this.btnStatistAccToArrDep.Text = "Flights According to Arrival / Departure  (Summary)";
            this.btnStatistAccToArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistAccToArrDep.Click += new System.EventHandler(this.btnStatistAccToArrDep_Click);
            // 
            // btnSpotUsingRate
            // 
            this.btnSpotUsingRate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSpotUsingRate.Location = new System.Drawing.Point(372, 88);
            this.btnSpotUsingRate.Name = "btnSpotUsingRate";
            this.btnSpotUsingRate.Size = new System.Drawing.Size(324, 23);
            this.btnSpotUsingRate.TabIndex = 14;
            this.btnSpotUsingRate.Text = "Position Usage";
            this.btnSpotUsingRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSpotUsingRate.Click += new System.EventHandler(this.btnSpotUsingRate_Click);
            // 
            // btnDailyFlightLog
            // 
            this.btnDailyFlightLog.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDailyFlightLog.Location = new System.Drawing.Point(16, 116);
            this.btnDailyFlightLog.Name = "btnDailyFlightLog";
            this.btnDailyFlightLog.Size = new System.Drawing.Size(324, 23);
            this.btnDailyFlightLog.TabIndex = 3;
            this.btnDailyFlightLog.Text = "Daily Flight Log";
            this.btnDailyFlightLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDailyFlightLog.Click += new System.EventHandler(this.btnDailyFlightLog_Click);
            // 
            // btnGPUUsage
            // 
            this.btnGPUUsage.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGPUUsage.Location = new System.Drawing.Point(372, 144);
            this.btnGPUUsage.Name = "btnGPUUsage";
            this.btnGPUUsage.Size = new System.Drawing.Size(324, 23);
            this.btnGPUUsage.TabIndex = 16;
            this.btnGPUUsage.Text = "GPU Usage";
            this.btnGPUUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGPUUsage.Click += new System.EventHandler(this.btnGPUUsage_Click);
            // 
            // btnInventory
            // 
            this.btnInventory.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnInventory.Location = new System.Drawing.Point(372, 200);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(324, 23);
            this.btnInventory.TabIndex = 18;
            this.btnInventory.Text = "Inventory (A/C on Ground)";
            this.btnInventory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            // 
            // btnOvernightFlights
            // 
            this.btnOvernightFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOvernightFlights.Location = new System.Drawing.Point(16, 200);
            this.btnOvernightFlights.Name = "btnOvernightFlights";
            this.btnOvernightFlights.Size = new System.Drawing.Size(324, 23);
            this.btnOvernightFlights.TabIndex = 6;
            this.btnOvernightFlights.Text = "Overnight Flights";
            this.btnOvernightFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOvernightFlights.Click += new System.EventHandler(this.btnOvernightFlights_Click);
            // 
            // btnBeltAllocations
            // 
            this.btnBeltAllocations.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnBeltAllocations.Location = new System.Drawing.Point(372, 116);
            this.btnBeltAllocations.Name = "btnBeltAllocations";
            this.btnBeltAllocations.Size = new System.Drawing.Size(324, 23);
            this.btnBeltAllocations.TabIndex = 15;
            this.btnBeltAllocations.Text = "Baggage Belt Usage ";
            this.btnBeltAllocations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBeltAllocations.Click += new System.EventHandler(this.btnBeltAllocations_Click);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Location = new System.Drawing.Point(16, 312);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(324, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Delayed Flights";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnStatisticCancelledFlights
            // 
            this.btnStatisticCancelledFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStatisticCancelledFlights.Location = new System.Drawing.Point(16, 256);
            this.btnStatisticCancelledFlights.Name = "btnStatisticCancelledFlights";
            this.btnStatisticCancelledFlights.Size = new System.Drawing.Size(324, 23);
            this.btnStatisticCancelledFlights.TabIndex = 8;
            this.btnStatisticCancelledFlights.Text = "Cancelled Flights";
            this.btnStatisticCancelledFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatisticCancelledFlights.Click += new System.EventHandler(this.btnStatisticCancelledFlights_Click);
            // 
            // btnCounterSchedule
            // 
            this.btnCounterSchedule.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCounterSchedule.Location = new System.Drawing.Point(372, 32);
            this.btnCounterSchedule.Name = "btnCounterSchedule";
            this.btnCounterSchedule.Size = new System.Drawing.Size(324, 23);
            this.btnCounterSchedule.TabIndex = 12;
            this.btnCounterSchedule.Text = "Check-in Counter Allocation";
            this.btnCounterSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCounterSchedule.Click += new System.EventHandler(this.btnCounterSchedule_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(722, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please click one of the following buttons to activate the respective report:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFlightsByNatureCode
            // 
            this.btnFlightsByNatureCode.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnFlightsByNatureCode.Location = new System.Drawing.Point(16, 32);
            this.btnFlightsByNatureCode.Name = "btnFlightsByNatureCode";
            this.btnFlightsByNatureCode.Size = new System.Drawing.Size(324, 23);
            this.btnFlightsByNatureCode.TabIndex = 0;
            this.btnFlightsByNatureCode.Text = "Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg.";
            this.btnFlightsByNatureCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFlightsByNatureCode.Click += new System.EventHandler(this.btnFlightsByNatureCode_Click);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(722, 494);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "FIPS Reports";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmMain_HelpRequested);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// Instantiates the frmMain, which is the
		/// main form of the application and starts it.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			frmMain olDlg = new frmMain();
			olDlg.args = args; 
			Application.Run(olDlg);
			//Application.Run(new frmMain());
		}

		/// <summary>
		/// Opens the Flights by Nature Code Filter dialog
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnFlightsByNatureCode_Click(object sender, System.EventArgs e)
		{
			frmFlightByNatureCode frm = new frmFlightByNatureCode();
			frm.ShowDialog(this);
		}
		/// <summary>
		/// Initialize the main form of the application.
		/// Connect to CEDA, init the version and so on.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">Event arguments</param>
		private void frmMain_Load(object sender, System.EventArgs e)
		{
            string sPath = System.Environment.GetEnvironmentVariable("UFISSYSTEM")+"\\ceda.ini";
			//Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            Ufis.Utils.IniFile myIni = new IniFile(sPath);
			UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			strPrefixVal = myIni.IniReadValue("FIPS", "PREFIX_REPORTS");

			string strUTCConvertion = "";
			strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
			if (strUTCConvertion == "NO")
			{
				UT.UTCOffset = 0;
			}
			else
			{
				DateTime datLocal = DateTime.Now;
				DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
				TimeSpan span = datLocal - datUTC;

				UT.UTCOffset = (short)span.Hours;
			}

            try
            {
                myDB = UT.GetMemDB();
                bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "FIPS_RPT");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connecting to Server Failed!","Connection Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

			UT.IsTimeInUtc = false;

			lblVersion.Text = "Version: ";
			lblVersion.Text += Application.ProductVersion;
			lblServer.Text = UT.ServerName;
			//check whether Reason for change is enabled or not
			string strReasonForChange = myIni.IniReadValue("FIPS","REASONFORCHANGES");
			if(strReasonForChange.Equals("TRUE") || strReasonForChange.Equals("ENABLE"))
				this.button7.Show();
			else
				this.button7.Hide();
			if(myIni.IniReadValue("FIPS","CHUTE_DISPLAY").CompareTo("TRUE") != 0)
			{
				buttonChuteAllocation.Hide();
			}

            if (myIni.IniReadValue("FIPS", "NIGHT_FLIGHT_SLOT").CompareTo("TRUE") != 0)
            {
                btnNightOperations.Hide();
                btnNightOperationsSummary.Hide();
            }

			// check, if column for Abnormal flight report is available in delay code table
			this.button2.Enabled = false;
			this.button2.Visible = false;
			
			///Changed for the Login dialog to pop while the application is started.
			///This is added while solving the PRF 8523
			string iltet;
			if(args.Length == 0 )
				iltet = "";			
			
			if(!LoginProcedure())
			{
				Application.Exit();
			}
            InitDeIce();
            if (myIni.IniReadValue("FIPS", "ALLOCATION_PLAN").CompareTo("TRUE") != 0)
            {
                btnFlightAllocation.Hide();
            }
            CheckingButtonLocation();

		}
        /// <summary>
        /// Move the button according to show hide
        /// </summary>
        private void CheckingButtonLocation()
        {            
            if (btnNightOperationsSummary.Visible == false)
            {
                btnFlightAllocation.Location = btnNightOperationsSummary.Location;
            }
            if (btnNightOperations.Visible == false)
            {
                btnFlightAllocation.Location = btnNightOperations.Location;
            }
        }
        private void InitDeIce()
        {
            string st = Ctrl.CtrlConfig.GetConfigInfo(@"C:\UFIS\System\",
                "FIPSREPORT",
                Ctrl.CtrlDeIce.CEDA_INI_CONFIG, "N");
            if (Ctrl.CtrlConfig.GetConfigInfo(@"C:\UFIS\System\Ceda.ini",
                "FIPSREPORT",
                Ctrl.CtrlDeIce.CEDA_INI_CONFIG, "N") == "Y")
            {
                Ctrl.CtrlDeIce.GetInstance().Init(this.LoginControl);
                this.btnDeIcing.Visible = true;
            }
            else
            {
                this.btnDeIcing.Visible = false;
            }
        }

		/// <summary>
		/// Sets the UT namespace internal indicator to local times or UTC times,
		/// depending on the user's click onto the cbTimeInLocal check box.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">Event arguments</param>
		private void cbTimeInLocal_CheckedChanged(object sender, System.EventArgs e)
		{
			UT.IsTimeInUtc = !cbTimeInLocal.Checked;
		}
		/// <summary>
		/// Opens the Counter Schedule Filter dialog
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnCounterSchedule_Click(object sender, System.EventArgs e)
		{
			frmCounterSchedule frm = new frmCounterSchedule();
			frm.ShowDialog();
		}
		/// <summary>
		/// Open the cancelled flights
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnStatisticCancelledFlights_Click(object sender, System.EventArgs e)
		{
			frmCancelled_Flights frm = new frmCancelled_Flights();
			frm.ShowDialog();
		}
		/// <summary>
		/// Opens delayed flights
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void button4_Click(object sender, System.EventArgs e)
		{
			frmDelayed_Flights frm = new frmDelayed_Flights();
			frm.ShowDialog();
		}
		/// <summary>
		/// Opens the belt allocations
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnBeltAllocations_Click(object sender, System.EventArgs e)
		{
			frmBeltAllocations frm = new frmBeltAllocations();
			frm.ShowDialog();
		}
		/// <summary>
		/// Opens the overnight flights.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnOvernightFlights_Click(object sender, System.EventArgs e)
		{
			frmOvernightFlights frm = new frmOvernightFlights();
			frm.ShowDialog();
		}
		/// <summary>
		/// Opens the Inventory list (flights on ground)
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnInventory_Click(object sender, System.EventArgs e)
		{
			ITable mySYS = myDB.Bind("SYS", "SYS", "TANA", "10", "TANA");
			mySYS.Load("WHERE TANA='FOG'");
			if(mySYS.Count > 0)
			{
				frmInventory frm = new frmInventory();
				frm.ShowDialog();
			}
			else
			{
				MessageBox.Show(this, "Flights on Ground is not supported (FOGTAB missing)!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			myDB.Unbind("SYS");
		}
		/// <summary>
		/// Opens the form for GPU usage
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnGPUUsage_Click(object sender, System.EventArgs e)
		{
			frmGPUUsage frm = new frmGPUUsage();
			frm.ShowDialog();
		}
		/// <summary>
		/// Opens the form for Daily flight log.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnDailyFlightLog_Click(object sender, System.EventArgs e)
		{
			try
			{
				frmDaily_FlightLog frm = new frmDaily_FlightLog();
				frm.ShowDialog();
			}
			catch(Exception ex)
			{
				MessageBox.Show(this, ex.Message);
			}
		}


		private void panel2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void btnAircraftsByHour_Click(object sender, System.EventArgs e)
		{
		}

		private void btnSpotUsingRate_Click(object sender, System.EventArgs e)
		{
			frmSpot_UsingRate frm = new frmSpot_UsingRate();
			frm.ShowDialog();
		}

		private void btnStatistAccToArrDep_Click(object sender, System.EventArgs e)
		{
			frmStatistics_Arr_Dep frm = new frmStatistics_Arr_Dep();
			frm.ShowDialog();
		}

		private void btnStatistCancelledFlightSummary_Click(object sender, System.EventArgs e)
		{
			frmCancelled_FlightDetail frm = new frmCancelled_FlightDetail();
			frm.ShowDialog();
		}

		private void btnStatistOvernightArrSummary_Click(object sender, System.EventArgs e)
		{
			frmOverNight_FlightSummary frm = new frmOverNight_FlightSummary();
			frm.ShowDialog();
		}

		private void btnStatistAccArrDep_Click(object sender, System.EventArgs e)
		{
			frmStatisticsAccordingToArrivalDeparture frm = new frmStatisticsAccordingToArrivalDeparture();
			frm.ShowDialog();
		}


    	private void btnAircraftMovementsPerHour_Click(object sender, System.EventArgs e)
		{
			frmAircraftMovementsperHour frm = new frmAircraftMovementsperHour();
			frm.ShowDialog();
		}

		private void btnStatistRushHour_Click(object sender, System.EventArgs e)
		{
			frmCheckin_statisticGraph frm = new frmCheckin_statisticGraph();
			frm.ShowDialog();
		}

		private void btnStatistDomesticIntComb_Click(object sender, System.EventArgs e)
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string[] strFltiAndDesc;
			string strFltiDesc = myIni.IniReadValue("FIPSREPORT","FLIGHTS_ACCORDING_FLTI");
			if(strFltiDesc.Length > 0)
			{
				strFltiAndDesc = strFltiDesc.Split(',');				
				if(strFltiAndDesc.Length < 4 || strFltiAndDesc.Length%2 != 0 || strFltiAndDesc.Length > 8)
				{
					MessageBox.Show(this,"FLIGHTS_ACCORDING_FLTI entry is corrupted","Ceda.ini Entry",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
					return;
				}
			}
			frmFlightStatisticsDomesticInternationalCombined frm = new frmFlightStatisticsDomesticInternationalCombined();
			frm.ShowDialog();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			frmBlockedTimes frm = new frmBlockedTimes();
			frm.ShowDialog();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			frmAbnormal_Flights frm = new frmAbnormal_Flights();
			frm.ShowDialog();
		}

		private bool IsColumnForAbnormalFlightsReportAvailable()
		{
			if (myDB == null)
				return false;
			return myDB.Reader.CedaAction("RT","SYSTAB","TANA,FINA","","WHERE Tana = 'DEN' AND Fina = 'RCOL'") && myDB.Reader.GetBufferCount() == 1;
		}

		/// <summary>
		/// This function pops up the login dialog and opens the application or closes the application
		/// depending on the user rights.This is done for the PRF 8523
		/// </summary>
		/// <returns></returns>
		private bool LoginProcedure()
		{
			string LoginAnsw = "";
			string tmpRegStrg = "";
			string strRet = "";

			LoginControl.ApplicationName = "FIPSReport";
			LoginControl.VersionString = Application.ProductVersion;
			LoginControl.InfoCaption = "Info about FIPSReport";
			LoginControl.InfoButtonVisible = true;
			LoginControl.InfoUfisVersion = "Ufis Version 4.5";
			LoginControl.InfoAppVersion = "FIPSReport " + Application.ProductVersion.ToString();
			LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
			LoginControl.UserNameLCase = true;

			tmpRegStrg = "FIPSReport" + ",";
			tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
			LoginControl.RegisterApplicationString = tmpRegStrg;
			/*			
			args = new string[3];

			args[0] =  "UFIS$ADMIN";
			args[1] =  "Passwort";
			*/
			if(args.Length == 0 )
			{
				strRet = LoginControl.ShowLoginDialog();

			}
			else
			{
                string userId = args[0];
                string password = "";
                bool hasUserIdNPwd = false;
                string[] stParams = args[0].Split('|');
                if (stParams.Length >= 4)
                {
                    string[] stTemps = stParams[1].Split(',');
                    userId = stTemps[0];
                    password = stParams[3];
                    hasUserIdNPwd = true;
                }
                else if (args.Length==2)
                {
                    password = args[1];
                    hasUserIdNPwd = true;
                }
                if (hasUserIdNPwd)
                {
                    strRet = LoginControl.DoLoginSilentMode(userId, password);
                }
				if(strRet != "OK")
					strRet = LoginControl.ShowLoginDialog();

			}


			LoginAnsw = LoginControl.GetPrivileges("InitModu");
			if( LoginAnsw != "" && strRet != "CANCEL" )
			{
				return true; 
			}
			else  
				return false;
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			frmStatDelayReasons frmStatDelayRes = new frmStatDelayReasons();
			frmStatDelayRes.ShowDialog();
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			frmReasonForChanges frmReasonForChanges = new frmReasonForChanges();
			frmReasonForChanges.ShowDialog();
		}

		private void btnSeatStatisDomIntComb_Click(object sender, System.EventArgs e)
		{
			frmSeatStatisticsAccDomInt frmSeatStatDomInt = new frmSeatStatisticsAccDomInt();
			frmSeatStatDomInt.ShowDialog();
		}

		private void button_ConfigLoadPax_Click(object sender, System.EventArgs e)
		{
			frmCongfigLoadPax frm = new frmCongfigLoadPax();
			frm.ShowDialog();
		}

		private void buttonChuteAllocation_Click(object sender, System.EventArgs e)
		{
			frmChuteSchedule frm = new frmChuteSchedule();
			frm.ShowDialog();
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmMain_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}

        private void btnNightOperations_Click(object sender, EventArgs e)
        {
            frmNightOperations frmNightOperationsRes = new frmNightOperations();
            frmNightOperationsRes.ShowDialog();

        }

        private void btnNightOperationsSummary_Click(object sender, EventArgs e)
        {
            frmNightFlightSummary frmNightFlightSummaryRes = new frmNightFlightSummary();
            frmNightFlightSummaryRes.ShowDialog();

        }

        private void btnDeIcing_Click(object sender, EventArgs e)
        {
            //AxDeicingClass.DeicingDialogClass deIcing = new AxDeicingClass.DeicingDialogClass();
            
            //deIcing.AppName = LoginControl.ApplicationName;// "FIPS-Report";
            //deIcing.AppVersion = LoginControl.InfoAppVersion;// "4.6.1.9";
            ////deIcing.IniFilePath = @"d:\Ufis\System\De-icing.ini";
            //deIcing.HandleBc = true;
            //AxDeicingClass.TimeZoneEnum timeZone = AxDeicingClass.TimeZoneEnum.UTC ; 
            //deIcing.set_TimeZone(ref timeZone);
            //deIcing.UserName = LoginControl.GetUserName();// "UFIS$ADMIN";
            //deIcing.ShowSplashScreen = false;
            //deIcing.LoadBasicData();


            //deIcing.ShowDialog(true);

            Ctrl.CtrlDeIce.GetInstance().Show();
        }
        //Phyoe Khaing Min
        //17-Feb-2012
        //Allocation Planning
        private void btnFlightAllocation_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\AllocationPlan.exe";
            UT.UserName = LoginControl.GetUserName();
            //ProcessStartInfo vPStartInfo = new ProcessStartInfo();
            //vPStartInfo.FileName = (AppPath);
            string Arguments = "ALPR|" + UT.UserName;
            Process.Start(AppPath, Arguments); 
        }


	}
}