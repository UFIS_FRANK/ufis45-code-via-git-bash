﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports.Ctrl
{
    public class CtrlAircraftPosition
    {
        #region Singleton
        private static CtrlAircraftPosition _this = null;
        private CtrlAircraftPosition() { }
        public static CtrlAircraftPosition GetInstance()
        {
            if (_this == null) _this = new CtrlAircraftPosition();
            return _this;
        }
        #endregion

        #region DB Related
        /// <summary>
        /// The one and only IDatabase obejct.
        /// </summary>
        private IDatabase _MyDB;
        /// <summary>
        /// ITable object for the PSTTAB (Aircraft Position)
        /// </summary>
        private ITable myPST;

        private ITable myGRN;//Aircraft Position Group
        private ITable myGRM;//Aircraft Position Group Relation

        private IDatabase MyDB
        {
            get
            {
                if (_MyDB == null)
                    _MyDB = UT.GetMemDB();

                return _MyDB;
            }
        }
        #endregion


        public List<string> GetPositionsForAircraftGroupName(string stAircraftPosGrpName)
        {
            List<string> ls = new List<string>();
            myGRN.CreateIndex("GRPN","GRPN");

            foreach (IRow groupRow in myGRN.RowsByIndexValue("GRPN", stAircraftPosGrpName))
            {
                foreach (IRow grmRow in myGRM.RowsByIndexValue("GURN", groupRow["URNO"]))
                {
                    foreach (IRow pstRow in myPST.RowsByIndexValue("URNO", grmRow["VALU"]))
                    {
                        if (!ls.Contains(pstRow["PNAM"])) ls.Add(pstRow["PNAM"]);
                    }
                }
            }

            return ls;
        }

        public string GetPositionsForAircraftGroupNameInString(string stAircraftPosGrpName, 
            string separator, string quotationMark)
        {
            string st = "";
            List<string> ls = GetPositionsForAircraftGroupName(stAircraftPosGrpName);
            if (ls.Count > 0)
            {
                //st = quotationMark + ls[0] + quotationMark;
                for (int i = 0; i < ls.Count; i++)
                {
                    st += separator + quotationMark + ls[i] + quotationMark;
                }
            }

            return st;
        }
    }
}
