using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using System.Text;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmStatLoadPax_ArrDep.This Class is created for the support of the Statistics of the 
	/// Load and Pax in the FIPS Report.This is done while solving the PRF 8523
	/// </summary>
	public class frmStatLoadPax_ArrDep : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG')) AND (FTYP='O' OR FTYP='S')";
		private string strCCAWhereRaw = "where FLNU in (@@FLNU)";
		private string strTabHeaderArrival = "Flight ,ORG ,STA ,ATA ,A/C ,REG ,NA ,ALC ,POS ,Gate ,Belt ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";
		private string strAPXWhereRaw = "where FLNU in (@@FLNU)";
		private string strLOAWhereRaw = "where FLNU in (@@FLNU) "+
			"AND DSSN IN ('USR','LDM','MVT','KRI') "+
			"AND TYPE IN ('PAX','PAD','LOA') "+
			"AND APC3=' '";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,NA,ALC,POS,GATE,BELT,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";

		/// <summary>
		/// The length of each column
		/// </summary>
		private string strTabHeaderLensArrival = "60,30,50,50,40,50,40,40,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The length of each column for excel export
		/// </summary>
		private string strExcelTabHeaderLensArrival = "60,30,105,105,40,50,40,40,40,50,80,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The length of each column for excel export with date format same as report format
		/// </summary>
		private string strExcelHeaderLensArrival = "60,30,50,50,40,50,40,40,40,50,80,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
		private string strTabHeaderDeparture = "Flight ,DES ,STD ,ATD ,A/C ,REG ,NA ,ALC ,Cki ,Gate ,POS ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsDeparture = "FLNO,DES,STOD,ATD,AC_TYP,REG,NA,ALC,CKIN,GATE,POS,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
		
		/// <summary>
		/// The length of each column
		/// </summary>
		private String strTabHeaderLensDeparture = "60,30,50,50,40,40,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The length of each column for excel export
		/// </summary>
		private String strExcelTabHeaderLensDeparture = "60,30,105,105,40,40,40,40,140,50,40,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The length of each column for excel export with date format same as report format
		/// </summary>
		private String strExcelHeaderLensDeparture = "60,30,50,50,40,40,40,40,140,50,40,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		private ITable myCCA;
		private ITable myAPX;
		private ITable myLOA;
	
		private int iTotalFlights = 0;
		/// <summary>
		/// DateTime prefix to identify fields as date/time field
		/// </summary>
		private string strDateTimePrefix="<@DT@>";
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";

		#endregion _My Members

		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButtonDeparture;
		private System.Windows.Forms.RadioButton radioButtonArrival;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.CheckBox cbLoadInfo;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmStatLoadPax_ArrDep()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatLoadPax_ArrDep));
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.radioButtonDeparture = new System.Windows.Forms.RadioButton();
			this.radioButtonArrival = new System.Windows.Forms.RadioButton();
			this.btnLoad = new System.Windows.Forms.Button();
			this.cbLoadInfo = new System.Windows.Forms.CheckBox();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.lblProgress = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(88, 8);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(88, 20);
			this.dtFrom.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = "Date :";
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.Transparent;
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.radioButtonDeparture);
			this.groupBox1.Controls.Add(this.radioButtonArrival);
			this.groupBox1.Location = new System.Drawing.Point(8, 40);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(392, 64);
			this.groupBox1.TabIndex = 11;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Flights...";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(64, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(216, 16);
			this.label3.TabIndex = 9;
			this.label3.Text = "Which Flights should be shown";
			// 
			// radioButtonDeparture
			// 
			this.radioButtonDeparture.Location = new System.Drawing.Point(232, 32);
			this.radioButtonDeparture.Name = "radioButtonDeparture";
			this.radioButtonDeparture.TabIndex = 8;
			this.radioButtonDeparture.Text = "Departure";
			this.radioButtonDeparture.CheckedChanged += new System.EventHandler(this.radioButtonDeparture_CheckedChanged);
			// 
			// radioButtonArrival
			// 
			this.radioButtonArrival.Location = new System.Drawing.Point(64, 32);
			this.radioButtonArrival.Name = "radioButtonArrival";
			this.radioButtonArrival.Size = new System.Drawing.Size(92, 24);
			this.radioButtonArrival.TabIndex = 7;
			this.radioButtonArrival.Text = "Arrival";
			this.radioButtonArrival.CheckedChanged += new System.EventHandler(this.radioButtonArrival_CheckedChanged);
			// 
			// btnLoad
			// 
			this.btnLoad.BackColor = System.Drawing.SystemColors.Control;
			this.btnLoad.Location = new System.Drawing.Point(8, 136);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 12;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// cbLoadInfo
			// 
			this.cbLoadInfo.Location = new System.Drawing.Point(424, 56);
			this.cbLoadInfo.Name = "cbLoadInfo";
			this.cbLoadInfo.Size = new System.Drawing.Size(168, 24);
			this.cbLoadInfo.TabIndex = 14;
			this.cbLoadInfo.Text = "Use Loading Information";
			this.cbLoadInfo.CheckedChanged += new System.EventHandler(this.cbLoadInfo_CheckedChanged);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(83, 136);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 15;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(164, 136);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 16;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(240, 136);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 17;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(340, 136);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 18;
			this.progressBar1.Visible = false;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(339, 112);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 19;
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 168);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 344);
			this.panelBody.TabIndex = 20;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 328);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabResult.ContainingControl = this;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 328);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmStatLoadPax_ArrDep
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(752, 510);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.cbLoadInfo);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.dtFrom);
			this.Controls.Add(this.label1);
			this.Name = "frmStatLoadPax_ArrDep";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Statistics of Load and Pax ...";
			this.Load += new System.EventHandler(this.frmStatLoadPax_ArrDep_Load);
			this.groupBox1.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStatLoadPax_ArrDep_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			this.radioButtonArrival.Checked = true;
			InitTimePickers();
			InitTab();
		}

		private void InitTimePickers()
		{
			DateTime olFrom;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			dtFrom.Value = olFrom;
			dtFrom.CustomFormat = "dd.MM.yyyy";
		}

		/// <summary>
		///  This function initializes the table to be displayed 
		/// </summary>
		private void InitTab()
		{
			tabExcelResult.ResetContent();
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
				strExcelTabHeaderLensArrival = strExcelHeaderLensArrival;
				strExcelTabHeaderLensDeparture = strExcelHeaderLensDeparture;
			}
			else if(strExcelDateTimeFormat.Equals(strReportDateTimeFormat) == true)
			{
				strExcelTabHeaderLensArrival = strExcelHeaderLensArrival;
				strExcelTabHeaderLensDeparture = strExcelHeaderLensDeparture;
			}

			if(this.radioButtonArrival.Checked)
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Report");

				tabExcelResult.HeaderString = strTabHeaderArrival;
				tabExcelResult.LogicalFieldList = strLogicalFieldsArrival;
				tabExcelResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Excel");
			}
			else
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Report");

				tabExcelResult.HeaderString = strTabHeaderDeparture;
				tabExcelResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabExcelResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Excel");
			}		
		}
		/// <summary>
		///  Function handler for the "Load" Button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(this.radioButtonArrival.Checked)
			{
				PrepareReportDataArrival("Report");
				PrepareReportDataArrival("Excel");
			}
			else
			{
				PrepareReportDataDeparture("Report");
				PrepareReportDataDeparture("Excel");
			}
			this.Cursor = Cursors.Arrow;
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// LoadReportData reads the respective data and stores it in the respective members.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			strTmpWhere = strWhereRaw;
			
			tabResult.ResetContent();
			tabExcelResult.ResetContent();
			DateTime datFrom;
			DateTime datTo;

			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 23,59,0);
			
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
                datTo =  UT.LocalToUtc(datTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(datTo);
			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3"
							, "10,9,3,14,14,3,14,14,3,12,5,5,5,5,5,5,5,5,1,3,3,3,3,3,3,6,7,6,6,5,3"
							, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3");

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo ;
			TimeSpan tsDays = (datTo - datFrom);

			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;

				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@ORIG",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@DEST",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
				//strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom = datReadFrom.AddDays(1);//+=  new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
					myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}

			LoadCheckInCounterData();
			LoadPaxAndLoadData();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}
		/// <summary>
		/// PrepareReportDataDeparture prepares the display on the table.
		/// </summary>
		private void PrepareReportDataDeparture(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strCknCtr;
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;
			//IRow [] rows1;			

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"] == "D") 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["DES3"]).Append(",");
					if(strReportOrExcel.Equals("Report") == true)
					{
						strData.Append(Helper.DateString(myAFT[i]["STOD"],strDateDisplayFormat)).Append(",");
						strData.Append(Helper.DateString(myAFT[i]["AIRB"],strDateDisplayFormat)).Append(",");
					}
					else if(strReportOrExcel.Equals("Excel") == true)
					{
						strData.Append(strDateTimePrefix + myAFT[i]["STOD"]).Append(",");
						strData.Append(strDateTimePrefix + myAFT[i]["AIRB"]).Append(",");
					}
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");

					rows = myCCA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
					strCknCtr = "";
					for(int j = 0; j < rows.Length; j++)
						strCknCtr += rows[j]["CKIC"] + "/ ";

					if(rows.Length == 0)
						strData.Append(",");
					else
						strData.Append(strCknCtr.Substring(0,strCknCtr.Length-2)).Append(",");
					if ( !myAFT[i]["GTD2"].Equals(""))
						strData.Append(myAFT[i]["GTD1"]).Append("/").Append(myAFT[i]["GTD2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTD1"]).Append(",");
					strData.Append(myAFT[i]["PSTD"]).Append(",");
					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					} 
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");
						rows =myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
				
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				tabResult.Sort("2", true, true);
				tabResult.Refresh();
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");
				tabExcelResult.Sort("2", true, true);
			}
		}

		/// <summary>
		/// Loads the Check-In counter data corresponding to the flights 
		/// selected from the AFT table.
		/// </summary>
		private void LoadCheckInCounterData() 
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			StringBuilder strFlnus = new StringBuilder();
			string strTmpQry;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA"
				, "FLNU,CKIC"
				, "10,5"
				, "FLNU,CKIC");
			myCCA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strCCAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myCCA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strCCAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myCCA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}
		/// <summary>
		/// Reads the Load and Pax data from the respective tables
		/// </summary>
		private void LoadPaxAndLoadData()
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			string strTmpQry;
			StringBuilder strFlnus = new StringBuilder();

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Load Data";
			lblProgress.Refresh();
			progressBar1.Show();

			if (cbLoadInfo.Checked) 
			{
				myDB.Unbind("LOA");
				myLOA = myDB.Bind("LOA","LOA"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
					,"10,10,14,40,1,3,3,3,3,6"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU");
				myLOA.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strLOAWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myLOA.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strLOAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myLOA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myLOA.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
			else
			{
				myDB.Unbind("APX");
				myAPX = myDB.Bind("APX","APX"
					,"FLNU,URNO,TYPE,PAXC"
					,"10,10,3,6"
					,"FLNU,URNO,TYPE,PAXC");
				myAPX.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strAPXWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myAPX.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strAPXWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myAPX.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myAPX.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
		}
		/// <summary>
		/// Prepares the report to be displayed on the table(Arrival Report).
		/// </summary>
		private void PrepareReportDataArrival(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}
			
			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if ((myAFT[i]["ADID"] == "A") 
					 || (myAFT[i]["ADID"].Equals("B"))) 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["ORG3"]).Append(",");
					if(strReportOrExcel.Equals("Report") == true)
					{
						strData.Append(Helper.DateString(myAFT[i]["STOA"],strDateDisplayFormat)).Append(",");
						strData.Append(Helper.DateString(myAFT[i]["ATA"],strDateDisplayFormat)).Append(",");
					}
					else if(strReportOrExcel.Equals("Excel") == true)
					{
						strData.Append(strDateTimePrefix + myAFT[i]["STOA"]).Append(",");
						strData.Append(strDateTimePrefix + myAFT[i]["ATA"]).Append(",");
					}
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");
					strData.Append(myAFT[i]["PSTA"]).Append(",");
					if ( !myAFT[i]["GTA2"].Equals(""))
						strData.Append(myAFT[i]["GTA1"]).Append("/").Append(myAFT[i]["GTA2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTA1"]).Append(",");
					if (!myAFT[i]["BLT2"].Equals(""))
						strData.Append(myAFT[i]["BLT1"]).Append("-").Append(myAFT[i]["BLT2"]).Append(",");
					else
						strData.Append(myAFT[i]["BLT1"]).Append(",");
					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					}
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");

						rows = myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(),"\n");
				tabResult.Sort("2", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(),"\n");
				tabExcelResult.Sort("2", true, true);
			}
		}
		/// <summary>
		/// This function gets the Load data and prepares it to be displayed on the table.
		/// </summary>
		/// <param name="dType"></param>
		/// <param name="strAftUrno"></param>
		/// <param name="styp"></param>
		/// <param name="sstp"></param>
		/// <param name="ssst"></param>
		/// <returns></returns>
		private string GetLoadData(string dType, string strAftUrno, string styp, string sstp, string ssst)
		{
			string strLDM = "";
			string strMVT = "";
			string strKRI = "";
			
			IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
			for(int i = 0; i < rows.Length; i++)
			{
				if (rows[i]["TYPE"].Equals(dType))
				{
					if ( rows[i]["STYP"].Equals(styp) 
						&& rows[i]["SSTP"].Equals(sstp) 
						&& rows[i]["SSST"].Equals(ssst))
					{
						if(rows[i]["DSSN"]=="USR")
							return rows[i]["VALU"];
						if(rows[i]["DSSN"] == "LDM")
							strLDM = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "MVT")
							strMVT = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "KRI")
							strKRI = rows[i]["VALU"];
					}
				}
			}
			if(strLDM != "") return strLDM;
			if(strMVT != "") return strMVT;
			if(strKRI != "") return strKRI;
			return "";
		}
		/// <summary>
		/// This is the function handler for the radio button change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			//InitTab();
			if(radioButtonArrival.Checked)
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;

				tabExcelResult.HeaderString = strTabHeaderArrival;
				tabExcelResult.LogicalFieldList = strLogicalFieldsArrival;
				tabExcelResult.HeaderLengthString = strTabHeaderLensArrival;

				if(tabResult.GetLineCount() != 0)
				{
					tabResult.ResetContent();
					PrepareReportDataArrival("Report");
					tabExcelResult.ResetContent();
					PrepareReportDataArrival("Excel");
				}
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
		}
		/// <summary>
		/// this is the function handler for the radio button change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			//InitTab();
			if(radioButtonDeparture.Checked)
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;

				tabExcelResult.HeaderString = strTabHeaderDeparture;
				tabExcelResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabExcelResult.HeaderLengthString = strTabHeaderLensDeparture;
				if(tabResult.GetLineCount() != 0)
				{
					tabResult.ResetContent();
					PrepareReportDataDeparture("Report");
					tabExcelResult.ResetContent();
					PrepareReportDataDeparture("Excel");
				}
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
		}
		/// <summary>
		/// This is the function handler for the Print Preview button change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Prepares the Print Preview page
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			if (radioButtonArrival.Checked)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");
			strSubHeader.Append("From: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'23:59"));
			strSubHeader.Append(" (Flights: ").Append(iTotalFlights.ToString());
			if (radioButtonArrival.Checked)
				strSubHeader.Append("/ARR:").Append(iTotalFlights.ToString())
					.Append(" /DEP:0)"); 
			else
				strSubHeader.Append("/ARR:0")
					.Append(" /DEP:").Append(iTotalFlights.ToString()).Append(")"); 

			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabExcelResult,frmMain.strPrefixVal + "Statistics - Load Pax Arrival/Departure",strSubHeader.ToString(),"",8);
			if(this.radioButtonArrival.Checked)
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensArrival);
			}
			else
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensDeparture);
			}
			rpt.SetDateTimeFieldPrefix(strDateTimePrefix);
			rpt.SetExcelDateTimeFormat(strExcelDateTimeFormat);
			rpt.SetReportDateTimeFormat(strReportDateTimeFormat);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();

		}
		/// <summary>
		/// Function handler for the Print Button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(this.radioButtonArrival.Checked == false)
			{
				PrepareReportDataDeparture("Report");
				PrepareReportDataDeparture("Excel");
			}
			else
			{
				PrepareReportDataArrival("Report");
				PrepareReportDataArrival("Excel");
			}
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}
		/// <summary>
		/// Function handler for the double click of the LButton on the table header
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void cbLoadInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			tabResult.ResetContent();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			tabExcelResult.ResetContent();
		}
	}
}
