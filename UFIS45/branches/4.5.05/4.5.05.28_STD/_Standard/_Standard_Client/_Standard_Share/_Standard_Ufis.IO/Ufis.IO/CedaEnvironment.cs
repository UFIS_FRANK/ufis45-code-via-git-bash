﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.IO
{
    public class CedaEnvironment
    {
        public enum CedaEnvironmentVariable
        {
            Ceda,
            UfisAppl,
            UfisHelp,
            UfisSystem,
            UfisTmp
        }

        public static string GetEnvironmentVariable(CedaEnvironmentVariable variable)
        {
            string strVariable = string.Empty;
            string strDefault = string.Empty;
            switch (variable)
            {
                case CedaEnvironmentVariable.Ceda:
                    strVariable = "CEDA";
                    strDefault = string.Format("{0}\\Ceda.ini", GetEnvironmentVariable(CedaEnvironmentVariable.UfisSystem));
                    break;
                case CedaEnvironmentVariable.UfisAppl:
                    strVariable = "UFISAPPL";
                    strDefault = @"C:\Ufis\Appl";
                    break;
                case CedaEnvironmentVariable.UfisHelp:
                    strVariable = "UFISHELP";
                    strDefault = @"C:\Ufis\Help";
                    break;
                case CedaEnvironmentVariable.UfisSystem:
                    strVariable = "UFISSYSTEM";
                    strDefault = @"C:\Ufis\System";
                    break;
                case CedaEnvironmentVariable.UfisTmp:
                    strVariable = "UFISTMP";
                    strDefault = @"C:\Ufis\Tmp";
                    break;
            }

            string strReturn = Environment.GetEnvironmentVariable(strVariable);
            if (string.IsNullOrEmpty(strReturn)) strReturn = strDefault;

            return strReturn;
        }
    }
}
