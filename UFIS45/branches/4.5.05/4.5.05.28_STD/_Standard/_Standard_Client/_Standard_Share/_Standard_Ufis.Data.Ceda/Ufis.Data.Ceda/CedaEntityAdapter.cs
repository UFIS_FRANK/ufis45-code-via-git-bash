﻿using System;
using System.Collections.Generic;
using Ufis.Entities;
using System.Reflection;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents a class to provide ceda specific functionality to retrieve and save back
    /// data to the data source.
    /// </summary>
    public class CedaEntityAdapter : EntityAdapterBase
    {
        ///// <summary>
        ///// Represents the command for batch update operation.
        ///// </summary>
        private class BatchCommand
        {
            /// <summary>
            /// Gets or sets the command text for this BatchCommand instance.
            /// </summary>
            public string CommandText { get; set; }
            /// <summary>
            /// Gets or sets the list of System.Data.DataRow objects of this BatchCommand instance.
            /// </summary>
            public List<Entity> EntityCollection { get; set; }

            /// <summary>
            /// Initializes the new instance of BatchCommand class.
            /// </summary>
            public BatchCommand()
            {
                CommandText = string.Empty;
                EntityCollection = new List<Entity>();
            }
        }

        private readonly CedaDataConverter _cedaDataConverter = new CedaDataConverter();
        private DataCommand _cedaCommand;
        private List<BatchCommand> _batchCommands;

        /// <summary>
        /// Returns the unique record identifier.
        /// </summary>
        /// <returns></returns>
        public override object GetUniqueId()
        {
            return CedaUtils.GetUrno(DataContext);
        }

        /// <summary>
        /// Create the collection of entities with the records fetched from the data source
        /// by executing the Ufis.Data.DataCommand object.
        /// </summary>
        /// <param name="selectCommand">A Ufis.Data.DataCommand to select records from data source.</param>
        public override EntityCollectionBase<T> CreateEntityCollection<T>(DataCommand selectCommand)
        {
            EntityCollectionBase<T> entityCollection = new CedaEntityCollection<T>(DataContext);
            Fill<T>(entityCollection, selectCommand);
            return entityCollection;
        }

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records fetched from the data source
        /// by executing the Ufis.Data.DataCommand object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="selectCommand">A Ufis.Data.DataCommand to select records from data source.</param>
        protected override void DoFill<T>(EntityCollectionBase<T> entityList, DataCommand selectCommand)
        {
            try
            {
                DataRowCollection rows = DataContext.ExecuteQueryCommand(selectCommand);

                CedaSqlCommand command = (CedaSqlCommand)selectCommand;
                entityList.ColumnList = command.ColumnList;

                DoFill<T>(entityList, rows);
            }
            catch (DataException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records from
        /// the Ufis.Data.DataRowCollection object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="rows">A Ufis.Data.DataRowCollection that contains the records.</param>  
        protected override void DoFill<T>(EntityCollectionBase<T> entityList, DataRowCollection rows)
        {
            try
            {
                Type type = typeof(T);
                EntityTableMapping entityTableMapping = DataContext.EntityTableMappings[type];
                if (entityTableMapping != null)
                {
                    _cedaDataConverter.FillEntityList<T>(entityList, rows, entityTableMapping.ColumnMappings,
                        entityList.ColumnList, entityList.DataContext.DefaultTimeZoneInfo, true);
                }
            }
            catch (DataException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements for specified Ufis.Entities.Entity object. 
        /// This command will ignore Ufis.Data.EntityAdapterBase.UpdateBatchSize setting.
        /// </summary>
        /// <param name="entity">The Ufis.Entities.Entity object used to update the data source.</param>
        /// <param name="columnList">List of columns to update to the data source.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        protected override void DoUpdate(Entity entity, string columnList, TimeZoneInfo timeZoneInfo)
        {
            Type entityType = entity.GetType();
            EntityTableMapping entityTableMapping = DataContext.EntityTableMappings[entityType];

            string strTableName = entityTableMapping.TableName;
            string strWhereClause = string.Empty;

            Entity.EntityState entityState = entity.GetEntityState();
            if (entityState == Entity.EntityState.Modified || entityState == Entity.EntityState.Deleted)
            {
                if (!(entity is BaseEntity))
                {
                    if (ContinueUpdateOnError)
                        return;
                    else
                        throw new DataException("Cannot update or delete records because no identity attribute defined!");
                }
                else
                {
                    string strIdFieldName = string.Empty;
                    int intFieldValue = 0;

                    EntityColumnMapping entityColumnMapping = entityTableMapping.ColumnMappings["URNO"];
                    if (entityColumnMapping == null)
                    {
                        throw new DataException("Cannot update or delete records because no identity attribute defined!");
                    }
                    strIdFieldName = entityColumnMapping.ColumnName;
                    intFieldValue = ((BaseEntity)entity).Urno;

                    strWhereClause = String.Format("WHERE {0} = {1}", strIdFieldName, intFieldValue);
                }
            }

            string strColumnList = string.Empty;
            string strValueList = string.Empty;
            if (entityState == Entity.EntityState.Added || entityState == Entity.EntityState.Modified)
            {
                string[] arrColumns = columnList.Split(',');

                EntityColumnMappingCollection entityColumnMappings = entityTableMapping.ColumnMappings;
                foreach (string strColumnName in arrColumns)
                {
                    EntityColumnMapping entityColumnMapping = entityColumnMappings[strColumnName];
                    if (entityColumnMapping != null)
                    {
                        //exclude readonly fields
                        if ((!entityColumnMapping.IsReadOnly) || (entityState == Entity.EntityState.Added && entityColumnMapping.IsPrimaryKey))
                        {
                            PropertyInfo info = entityColumnMapping.PropertyInfo;
                            object objValue = info.GetValue(entity, null);
                            //check if urno is not yet filled
                            if (entityColumnMapping.IsPrimaryKey && (int)objValue <= 0)
                                objValue = GetUniqueId();

                            strColumnList += "," + strColumnName;
                            strValueList += "," + _cedaDataConverter.DataItemToCedaItem(objValue, info.PropertyType, timeZoneInfo, 
                                entityColumnMapping.BoolConverter, entityColumnMapping.MaxLength, false);
                        }
                    }
                }

                strColumnList = strColumnList.Substring(1);
                strValueList = strValueList.Substring(1);
            }

            switch (entityState)
            {
                case Entity.EntityState.Added:
                    _cedaCommand = new CedaSqlInsertCommand()
                    {
                        TableName = strTableName,
                        ColumnList = strColumnList,
                        ValueList = strValueList
                    };
                    if (strTableName == "AFTTAB")
                        _cedaCommand.CommandText = "IFR";
                    break;
                case Entity.EntityState.Modified:
                    _cedaCommand = new CedaSqlUpdateCommand()
                    {
                        TableName = strTableName,
                        ColumnList = strColumnList,
                        ValueList = strValueList,
                        WhereClause = strWhereClause
                    };
                    if (strTableName == "AFTTAB")
                        _cedaCommand.CommandText = "UFR";
                    break;
                case Entity.EntityState.Deleted:
                    _cedaCommand = new CedaSqlDeleteCommand()
                    {
                        TableName = strTableName,
                        WhereClause = strWhereClause
                    };
                    if (strTableName == "AFTTAB")
                        _cedaCommand.CommandText = "DFR";
                    break;
            }

            try
            {
                DataContext.ExecuteNonQueryCommand(_cedaCommand);
                if (AcceptChangesDuringUpdate) entity.AcceptChanges();
            }
            catch (DataException cedaEx)
            {
                if (!ContinueUpdateOnError) throw cedaEx;
            }
            catch (Exception ex)
            {
                if (!ContinueUpdateOnError) throw ex;
            }
        }

        protected override void UpdateBatch<T>(EntityCollectionBase<T> entityList)
        {
            _batchCommands = CreateBatchCommand<T>(entityList, entityList.ColumnList, entityList.DataContext.DefaultTimeZoneInfo);
            UpdateBatch();
        }
        
        /// <summary>
        /// Creates batch command.
        /// </summary>
        /// <param name="entityList">The list of Ufis.Entities.Entity object used to update the data source.</param>
        /// <param name="columnList">List of columns to update to the data source.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        /// <returns></returns>
        private List<BatchCommand> CreateBatchCommand<T>(IList<T> entityList, string columnList, TimeZoneInfo timeZoneInfo)
        {
            int iUpdateBatchSize = UpdateBatchSize;
            if (iUpdateBatchSize == 0) iUpdateBatchSize = int.MaxValue;

            List<BatchCommand> commands = new List<BatchCommand>();

            int iRowCount = 0;
            BatchCommand batchCommand = new BatchCommand();
            foreach (T item in entityList)
            {
                object objItem = (object)item;
                if (objItem is Entity)
                {
                    Entity entity = (Entity)objItem;

                    string command = BuildBatchCommandText(entity, columnList, timeZoneInfo);
                    if (!string.IsNullOrEmpty(command))
                    {
                        batchCommand.CommandText += command;
                        batchCommand.EntityCollection.Add(entity);

                        if ((++iRowCount) == iUpdateBatchSize)
                        {
                            commands.Add(batchCommand);
                            batchCommand = new BatchCommand();
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(batchCommand.CommandText))
            {
                commands.Add(batchCommand);
            }

            return commands;
        }

        /// <summary>
        /// Creates a single batch command text.
        /// </summary>
        /// <param name="entity">The Ufis.Entities.Entity object used to create a single batch command text.</param>
        /// <param name="columnList">List of columns to update to the data source.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        /// <returns></returns>
        private string BuildBatchCommandText(Entity entity, string columnList, TimeZoneInfo timeZoneInfo)
        {
            string strResult = string.Empty;
            Entity.EntityState entityState = entity.GetEntityState();

            if (entityState == Entity.EntityState.Unchanged)
            {
                return strResult;
            }

            Type entityType = entity.GetType();
            EntityTableMapping entityTableMapping = DataContext.EntityTableMappings[entityType];
            string strTableName = entityTableMapping.TableName;

            string strKeyColumn = string.Empty;
            string strKeyValue = string.Empty;
            if (entityState == Entity.EntityState.Modified || entityState == Entity.EntityState.Deleted)
            {
                if (!(entity is BaseEntity))
                {
                    if (ContinueUpdateOnError)
                        return strResult;
                    else
                        throw new DataException("Cannot update or delete records because no identity attribute defined!");
                }
                else
                {
                    string strIdFieldName = string.Empty;
                    int intFieldValue = 0;

                    if (entityTableMapping != null)
                    {
                        EntityColumnMapping entityColumnMapping = entityTableMapping.ColumnMappings["URNO"];
                        if (entityColumnMapping == null)
                        {
                            throw new DataException("Cannot update or delete records because no identity attribute defined!");
                        }
                        strIdFieldName = entityColumnMapping.ColumnName;
                        intFieldValue = ((BaseEntity)entity).Urno;

                        strKeyColumn = string.Format(",[{0}=:V{0}]", strIdFieldName);
                        strKeyValue = "," + intFieldValue.ToString();
                    }
                    else
                    {
                        object[] arrAttributes = entityType.GetProperty("Urno").GetCustomAttributes(typeof(EntityAttribute), false);
                        if (arrAttributes.Length > 0)
                        {
                            strIdFieldName = ((EntityAttribute)arrAttributes[0]).SerializedName;
                            intFieldValue = ((BaseEntity)entity).Urno;

                            strKeyColumn = string.Format(",[{0}=:V{0}]", strIdFieldName);
                            strKeyValue = "," + intFieldValue.ToString();
                        }
                    }
                }
            }

            string strColumnList = string.Empty;
            string strValueList = string.Empty;
            string[] arrColumns = columnList.Split(',');
            if (entityState == Entity.EntityState.Added || entityState == Entity.EntityState.Modified)
            {
                if (entityTableMapping != null)
                {
                    EntityColumnMappingCollection entityColumnMappings = entityTableMapping.ColumnMappings;
                    foreach (string strColumnName in arrColumns)
                    {
                        EntityColumnMapping entityColumnMapping = entityColumnMappings[strColumnName];
                        if (entityColumnMapping != null)
                        {
                            PropertyInfo info = entityColumnMapping.PropertyInfo;
                            object objValue = info.GetValue(entity, null);

                            strColumnList += "," + strColumnName;
                            strValueList += "," + _cedaDataConverter.DataItemToCedaItem(objValue, info.PropertyType, timeZoneInfo,
                                entityColumnMapping.BoolConverter, entityColumnMapping.MaxLength, false);
                        }
                    }
                }
                else
                {
                    PropertyInfo[] arrInfos = entityType.GetProperties();
                    foreach (PropertyInfo info in arrInfos)
                    {
                        object[] arrAttributes = info.GetCustomAttributes(typeof(EntityAttribute), false);
                        if (arrAttributes.Length > 0)
                        {
                            EntityAttribute entityAttribute = (EntityAttribute)arrAttributes[0];
                            string strColumnName = entityAttribute.SerializedName;
                            if (Array.IndexOf(arrColumns, strColumnName) >= 0)
                            {
                                Ufis.Data.EntityColumnMapping.BooleanConverter boolConverter =
                                    new EntityColumnMapping.BooleanConverter()
                                    {
                                        TrueValue = entityAttribute.TrueValue,
                                        FalseValue = entityAttribute.FalseValue
                                    };
                                object objValue = info.GetValue(entity, null);

                                strColumnList += "," + strColumnName;
                                strValueList += "," + _cedaDataConverter.DataItemToCedaItem(objValue, info.PropertyType, timeZoneInfo, 
                                    boolConverter, entityAttribute.MaxLength, false);
                            }
                        }
                    }
                }

                strColumnList = strColumnList.Substring(1);
                strValueList = strValueList.Substring(1);
            }

            string strCedaCommandText = string.Empty;
            string strFieldCount = arrColumns.Length.ToString();
            string strFieldList = string.Empty;
            string strValList = string.Empty;
            switch (entityState)
            {
                case Entity.EntityState.Added:
                    strCedaCommandText = "IRT";
                    strFieldList = strColumnList;
                    strValList = strValueList;
                    break;
                case Entity.EntityState.Modified:
                    strCedaCommandText = "URT";
                    strFieldList = strColumnList + strKeyColumn;
                    strValList = strValueList + strKeyValue;
                    break;
                case Entity.EntityState.Deleted:
                    strCedaCommandText = "DRT";
                    strFieldCount = "-1";
                    strFieldList = strKeyColumn.Substring(1);
                    strValList = strKeyValue.Substring(1);
                    break;
            }

            const string BATCH_COMMAND_FORMAT = "*CMD*,{0},{1},{2},{3}\n{4}\n";
            strResult = string.Format(BATCH_COMMAND_FORMAT, strTableName, strCedaCommandText, strFieldCount, strFieldList, strValList);

            return strResult;
        }

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements in batch mode.
        /// </summary>
        private void UpdateBatch()
        {
            foreach (BatchCommand batchCommand in _batchCommands)
            {
                _cedaCommand = new CedaBatchUpdateCommand()
                {
                    BatchCommandText = batchCommand.CommandText,
                    Options = (SendBroadcastOnUpdateBatch ? string.Empty : "LATE,NOBC,NOACTION")
                };
                try
                {
                    DataContext.ExecuteNonQueryCommand(_cedaCommand);

                    if (AcceptChangesDuringUpdate)
                    {
                        foreach (Entity entity in batchCommand.EntityCollection)
                        {
                            entity.AcceptChanges();
                        }
                    }
                }
                catch (DataException cedaEx)
                {
                    foreach (Entity entity in batchCommand.EntityCollection)
                    {
                        entity.SetErrorMessage(cedaEx.Message);
                    }
                    if (!ContinueUpdateOnError) throw cedaEx;
                }
                catch (Exception ex)
                {
                    foreach (Entity entity in batchCommand.EntityCollection)
                    {
                        entity.SetErrorMessage(ex.Message);
                    }
                    if (!ContinueUpdateOnError) throw ex;
                }
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityAdapter class.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.Ceda.CedaEntityAdapter class.</param>
        public CedaEntityAdapter(EntityDataContextBase dataContext) : base(dataContext)
        {
            
        }
    }
}
