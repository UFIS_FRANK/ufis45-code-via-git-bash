﻿using System;
using System.Collections.Generic;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents an SQL specific command class. This class is inherited from Ufis.Data.DataCommand class.
    /// </summary>
    public class CedaSqlCommand : DataCommand
    {
        /// <summary>
        /// Gets or sets the database table name.
        /// </summary>
        public string TableName
        {
            get { return base.Parameters[0].ToString(); }
            set { base.Parameters[0] = value; }
        }

        /// <summary>
        /// Gets or sets the list of columns to be fetched.
        /// </summary>
        public string ColumnList
        {
            get { return base.Parameters[1].ToString(); }
            set { base.Parameters[1] = value; }
        }

        /// <summary>
        /// Gets or sets the list of columns' values.
        /// </summary>
        public string ValueList
        {
            get { return base.Parameters[2].ToString(); }
            set { base.Parameters[2] = value; }
        }

        /// <summary>
        /// Gets or sets the SQL where clause.
        /// </summary>
        public string WhereClause
        {
            get { return base.Parameters[3].ToString(); }
            set { base.Parameters[3] = value; }
        }

        /// <summary>
        /// Gets or sets the number of rows to get from server.
        /// </summary>
        public int NumberOfRows
        {
            get { return int.Parse(base.Parameters[4].ToString()); }
            set { base.Parameters[4] = value; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlCommand class.
        /// </summary>
        public CedaSqlCommand() : base() 
        {
            base.Parameters = new CedaCommandParameterCollection();
        }
    }

    /// <summary>
    /// Represents an SQL Select specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlSelectCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlSelectCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlSelectCommand class.
        /// </summary>
        public CedaSqlSelectCommand()
        {
            base.CommandText = "RT";
        }
    }

    /// <summary>
    /// Represents an SQL Insert specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlInsertCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlInsertCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the SQL where clause.
        /// </summary>
        public new string WhereClause
        {
            get { return base.WhereClause; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlInsertCommand class.
        /// </summary>
        public CedaSqlInsertCommand()
        {
            base.CommandText = "IRT";
        }
    }

    /// <summary>
    /// Represents an SQL Update specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlUpdateCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlUpdateCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlUpdateCommand class.
        /// </summary>
        public CedaSqlUpdateCommand()
        {
            base.CommandText = "URT";
        }
    }

    /// <summary>
    /// Represents an SQL Delete specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlDeleteCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlDeleteCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns to be fetched.
        /// </summary>
        public new string ColumnList
        {
            get { return base.ColumnList; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlDeleteCommand class.
        /// </summary>
        public CedaSqlDeleteCommand()
        {
            base.CommandText = "DRT";
        }
    }

    /// <summary>
    /// Represents an SQL Select Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlSelectFlightCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }
        
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlSelectFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlSelectFlightCommand class.
        /// </summary>
        public CedaSqlSelectFlightCommand()
        {
            base.TableName = "AFTTAB";
            base.CommandText = "GFR";
        }
    }

    /// <summary>
    /// Represents an SQL Insert Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlInsertFlightCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlInsertFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the SQL where clause.
        /// </summary>
        public new string WhereClause
        {
            get { return base.WhereClause; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlInsertFlightCommand class.
        /// </summary>
        public CedaSqlInsertFlightCommand()
        {
            base.TableName = "AFTTAB";
            base.CommandText = "IFR";
        }
    }

    /// <summary>
    /// Represents an SQL Update Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlUpdateFlightCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlUpdateFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlUpdateFlightCommand class.
        /// </summary>
        public CedaSqlUpdateFlightCommand()
        {
            base.TableName = "AFTTAB";
            base.CommandText = "UFR";
        }
    }

    /// <summary>
    /// Represents an SQL Delete Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaSqlDeleteFlightCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlDeleteFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns to be fetched.
        /// </summary>
        public new string ColumnList
        {
            get { return base.ColumnList; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaSqlDeleteFlightCommand class.
        /// </summary>
        public CedaSqlDeleteFlightCommand()
        {
            base.TableName = "AFTTAB";
            base.CommandText = "DFR";
        }
    }

    /// <summary>
    /// Represents a specific command class for batch update. This class is inherited from Ufis.Data.Ceda.CedaCommand class.
    /// </summary>
    public class CedaBatchUpdateCommand : DataCommand
    {
        /// <summary>
        /// Gets or sets the batch command text for this instance of Ufis.Data.Ceda.CedaBatchUpdateCommand.
        /// </summary>
        public string BatchCommandText
        {
            get { return base.Parameters[2].ToString(); }
            set { base.Parameters[2] = value; }
        }

        /// <summary>
        /// Gets or sets the options for this instance of Ufis.Data.Ceda.CedaBatchUpdateCommand.
        /// </summary>
        public string Options
        {
            get { return base.Parameters[3].ToString(); }
            set { base.Parameters[3] = value; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaBatchUpdateCommand class.
        /// </summary>
        public CedaBatchUpdateCommand()
        {
            base.CommandText = "REL";
            base.Parameters = new CedaCommandParameterCollection();
        }
    }

    /// <summary>
    /// Represents a ceda command class for retriving Urno. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaGetUrnoCommand : CedaSqlCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaSqlSelectCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets or sets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }

        /// <summary>
        /// Gets or sets the list of columns to be fetched.
        /// </summary>
        public new string ColumnList
        {
            get { return base.ColumnList; }
        }

        /// <summary>
        /// Gets or sets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Gets or sets the SQL where clause.
        /// </summary>
        public new string WhereClause
        {
            get { return base.WhereClause; }
        }

        /// <summary>
        /// Gets or sets the number of rows to get from server.
        /// </summary>
        public new int NumberOfRows
        {
            get { return int.Parse(base.Parameters[4].ToString()); }
            set
            {
                base.Parameters[4] = value; 
                base.ValueList = value.ToString();                
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaGetUrnoCommand class.
        /// </summary>
        public CedaGetUrnoCommand()
            : base()
        {
            base.CommandText = "GMU";
            base.TableName = string.Empty;
            base.ColumnList = "*";
            base.WhereClause = string.Empty;
        }
    }
}
