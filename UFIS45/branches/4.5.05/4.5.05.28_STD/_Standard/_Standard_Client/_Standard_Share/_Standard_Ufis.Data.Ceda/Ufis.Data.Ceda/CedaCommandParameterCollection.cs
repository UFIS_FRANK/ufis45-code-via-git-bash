﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents a collection of parameters relevant to an Ufis.Data.Ceda.CedaCommand.
    /// </summary>
    public class CedaCommandParameterCollection : DataCommandParameterCollection
    {
        /// <summary>
        /// Initializes the new instance of CedaCommandParameterCollection class.
        /// </summary>
        public CedaCommandParameterCollection()
        {
            for (int i = 0; i < 5 ; i++)
            {
                base.Add("");
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="index">The zero-based index parameter to retrieve.</param>
        /// <returns></returns>
        public new string this[int index]
        {
            get { return base[index].ToString(); }
            set { base[index] = value; }
        }
    }
}
