﻿#define UseUtf8Encoding

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Provides a way of sending command to CEDA.
    /// <para>
    /// This class is responsible to perform the classical SQL statements as well as 
    /// other specialized server actions. This is the trigger to manager the server actions 
    /// from the client application point of view.
    /// </para>
    /// </summary>
    public class CedaDataWriter
    {
        //********************************
        // internally used members
        //********************************
        private Socket _socket = null;
        private bool _bConnectionOpenedOutside;
        private DataRowCollection _rows;

        private string strTwe;
        private string strTws;
        private string strSeq;
        private string strReqId;
        private string strDest1;
        private string strDest2;
        private string strCommandText;
        private string strCommandObject;
        private string strOrigName;
        private string strSelection;
        private string strFieldList;
        private string strLastErrorMessage;
        private static string strStaticRest = "";

        /// <summary>
        /// Socket error codes that could occur during server communication.
        /// </summary>
        enum ErrorCodes
        {
            /// <summary>
            /// Fail.
            /// </summary>
            RC_FAIL = -1,
            /// <summary>
            /// No error
            /// </summary>
            RC_SUCCESS = 0,
            /// <summary>
            /// Empty message occured.
            /// </summary>
            RC_EMPTY_MSG = 1,
            /// <summary>
            /// Server was shut down.
            /// </summary>
            RC_SHUTDOWN = -5,
            /// <summary>
            /// Comminication failed.
            /// </summary>
            RC_COMM_FAIL = -2,
            /// <summary>
            /// Intitialisation failed.
            /// </summary>
            RC_INIT_FAIL = -3,
            /// <summary>
            /// CEDA error.
            /// </summary>
            RC_CEDA_FAIL = -4,
            /// <summary>
            /// Already init.
            /// </summary>
            RC_ALREADY_INIT = -6,
            /// <summary>
            /// Nothing found.
            /// </summary>
            RC_NOT_FOUND = -7,
            /// <summary>
            /// Data was cut.
            /// </summary>
            RC_DATA_CUT = -8,
            /// <summary>
            /// Timeout.
            /// </summary>
            RC_TIMEOUT = -9,
            /// <summary>
            /// Tried to reconnect.
            /// </summary>
            RC_ALREADY_CONNECTED = -10,
            /// <summary>
            /// No answer.
            /// </summary>
            RC_MISSING_ANSWER = -11,
            //Server busy.
            RC_BUSY = -31,
            /// <summary>
            /// To many applications connected.
            /// </summary>
            RC_TO_MANY_APP = -32,
        }

        private const int BC_HEAD_DEST_NAME_OFFSET = 0;
        private const int BC_HEAD_ORIG_NAME_OFFSET = 10;
        private const int BC_HEAD_RECV_NAME_OFFSET = 20;
        private const int BC_HEAD_BC_NUM_OFFSET = 30;
        private const int BC_HEAD_SEQ_ID_OFFSET = 32;
        private const int BC_HEAD_TOT_BUF_OFFSET = 42;
        private const int BC_HEAD_ACT_BUF_OFFSET = 44;
        private const int BC_HEAD_REQ_SEQ_ID_OFFSET = 46;
        private const int BC_HEAD_RC_OFFSET = 56;
        private const int BC_HEAD_TOT_SIZE_OFFSET = 58;
        private const int BC_HEAD_CMD_SIZE_OFFSET = 60;
        private const int BC_HEAD_DATA_SIZE_OFFSET = 62;
        private const int BC_HEAD_DATA_OFFSET = 64;

        private const int BC_HEAD_SIZE = 66;


        private const int CMDBLK_COMMAND_OFFSET = 0;
        private const int CMDBLK_OBJ_NAME_OFFSET = 6;
        private const int CMDBLK_ORDER_OFFSET = 39;
        private const int CMDBLK_TW_START_OFFSET = 41;
        private const int CMDBLK_TW_END_OFFSET = 74;
        private const int CMDBLK_DATA_OFFSET = 107;

        private const int CMDBLK_SIZE = 108;

        private const int COMMIF_COMMAND_OFFSET = 0;
        private const int COMMIF_LENGTH_OFFSET = 4;
        private const int COMMIF_DATA_OFFSET = 8;

        private const int COMMIF_SIZE = 12;

        private const int DATAPACKETLEN = 1024 - 12;
        private const int NETREAD_SIZE = 0x8000;
        private const int PACKET_LEN = 1024;
        private const int NET_SHUTDOWN = 0x1;
        private const int NET_DATA = 0x2;
        private const int PACKET_DATA = 65436;	// i.e. -100
        private const int PACKET_START = 65336;	// i.e. -200
        private const int PACKET_END = 65236;	// i.e. -300


        private const short SELECTION = 0;
        private const short FIELDS = 1;
        private const short DATA = 2;

        //********************************
        // public members
        //********************************

        /// <summary>
        /// Gets or sets the Ufis.Data.DataCommand used by this instance of 
        /// Ufis.Data.Ceda.CedaDataWriter
        /// </summary>
        public DataCommand Command { get; set; }

        /// <summary>
        /// Gets or sets the Ufis.Data.Ceda.CedaConection used by this instance of 
        /// Ufis.Data.Ceda.CedaDataWriter
        /// </summary>
        public CedaConnection Connection { get; set; }

        /// <summary>
        /// Gets the last error message. This can be an SQL error or a socket error.
        /// </summary>
        /// <remarks>none.</remarks>
        public string LastErrorMessage
        {
            get { return strLastErrorMessage; }
        }

        /// <summary>
        /// Gets the Ufis.Data.Ceda.CedaDataRowCollection fetched from the server.
        /// </summary>
        public DataRowCollection Rows
        {
            get { return _rows; }
        }

        /// <summary>
        /// Connects to the ceda based on connection object
        /// </summary>
        private void ConnectToCeda()
        {
            DataErrorCollection errors = new DataErrorCollection();
            DataError error;
            bool bConnected = false;

            CedaConnection connection = this.Connection;
            if (connection.State == ConnectionState.Open)
            {
                _bConnectionOpenedOutside = true;
                bConnected = true;
                _socket = connection.WriterSocket;
            }
            else
            {
                _bConnectionOpenedOutside = false;

                string[] arrServers = this.Connection.ServerList.Split(',');
                foreach (string strServer in arrServers)
                {
                    _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    error = ConnectToCedaServer(_socket, strServer, connection.WriterPort);
                    if (error == null)
                        bConnected = true;
                    else
                        errors.Add(error);

                    if (bConnected) break;
                }
            }

            if (!bConnected) throw new DataException(errors);
        }

        /// <summary>
        /// Connects to the ceda based on connection object
        /// </summary>
        /// <param name="socket">The System.Net.Sockets.Socket object to perform the connection.</param>
        /// <param name="serverName">The name of server to connect to.</param>
        /// <param name="port">The port number to connect to.</param>
        /// <returns>Ufis.Data.Ceda.CedaError object if connection failed or null if succeeded</returns>
        private DataError ConnectToCedaServer(Socket socket, string serverName, int port)
        {
            DataError error = null;

            try
            {
                IPHostEntry ipHostEntry = Dns.GetHostEntry(serverName);
                IPAddress ipAddress = ipHostEntry.AddressList[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);
                socket.Connect(ipEndPoint);
            }
            catch (Exception ex)
            {
                error = new DataError("Unable to connect to server " + serverName, ex);
            }

            return error;
        }

        /// <summary>
        /// Disconnects from the ceda server
        /// </summary>
        private void DisconnectFromCeda()
        {
            if (this._socket != null)
            {
                this._socket.Shutdown(SocketShutdown.Both);
                this._socket.Close();
                this._socket = null;
            }
        }

        /// <summary>
        /// Executes the command against CEDA server.
        /// </summary>
        public void Execute()
        {
            CedaConnection connection = this.Connection;
            string strWorkstationName = connection.WorkstationName;
            string strUserName = connection.UserName;
            string strCommandText = this.Command.CommandText;
            DataCommandParameterCollection commandParams = this.Command.Parameters;
            int iTimeout = this.Command.CommandTimeout;
            if (string.IsNullOrEmpty(strTwe))
            {
                strTwe = string.Format("{0},{1},{2},{3}", 
                    connection.HomeAirport, connection.TableExtension,
                    connection.ApplicationName, connection.ApplicationVersion);
            }

            try
            {
                ConnectToCeda();

                ErrorCodes ec = CallCeda(strCommandText, commandParams, iTimeout, strWorkstationName, strUserName, strTwe);

                if (!this._bConnectionOpenedOutside) DisconnectFromCeda();

                if (ec != ErrorCodes.RC_SUCCESS)
                {
                    string[] arrErrors = new string[] 
                        { LastErrorMessage,
                          "Error returned from Ceda server: " + Enum.GetName(typeof(ErrorCodes), ec) };
                    throw new DataException(arrErrors);
                }
            }
            catch (DataException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Executes the command against CEDA server and returns Ufis.Data.Ceda.CedaDataRowCollection object fetched from CEDA server.
        /// </summary>
        public DataRowCollection ExecuteQuery()
        {
            DataRowCollection rows = null;

            try
            {
                this.Execute();

                rows = this._rows;
            }
            catch (DataException ex)
            {
                throw ex;
            }

            return rows;
        }

        private int CalcBufLen(string selection, string fields, string data)
        {
            int iCmdSize, iDataSize, iTotSize;

            iCmdSize = CMDBLK_SIZE - 1 + selection.Length + fields.Length;

            iDataSize = data.Length;

            iTotSize = BC_HEAD_SIZE - 1 + iCmdSize + iDataSize + 4;

            return iTotSize;
        }

        private ErrorCodes SendBuffer(byte[] pbcHead, int bufLen)
        {
            Encoding encoding = Encoding.GetEncoding(this.Connection.WindowsCodePage);

            byte[] arrData = new byte[DATAPACKETLEN + COMMIF_SIZE];
            int iRemain = bufLen;
            int iOffs = 0;
            int iSendLen = 0;
            do
            {
                if (iRemain > DATAPACKETLEN)
                {
                    byte[] arrCommand = BitConverter.GetBytes((ushort)PACKET_DATA);
                    arrData[COMMIF_COMMAND_OFFSET + 0] = arrCommand[1];
                    arrData[COMMIF_COMMAND_OFFSET + 1] = arrCommand[0];

                    byte[] arrLength = BitConverter.GetBytes((uint)DATAPACKETLEN);
                    arrData[COMMIF_LENGTH_OFFSET + 0] = arrLength[3];
                    arrData[COMMIF_LENGTH_OFFSET + 1] = arrLength[2];
                    arrData[COMMIF_LENGTH_OFFSET + 2] = arrLength[1];
                    arrData[COMMIF_LENGTH_OFFSET + 3] = arrLength[0];

                    Array.Copy(pbcHead, iOffs, arrData, COMMIF_DATA_OFFSET, DATAPACKETLEN);
                    iSendLen = DATAPACKETLEN + COMMIF_SIZE;

                    iRemain -= DATAPACKETLEN;
                    iOffs += DATAPACKETLEN;
                }
                else
                {
                    byte[] arrCommand = BitConverter.GetBytes((ushort)PACKET_END);
                    arrData[COMMIF_COMMAND_OFFSET + 0] = arrCommand[1];
                    arrData[COMMIF_COMMAND_OFFSET + 1] = arrCommand[0];

                    byte[] arrLength = BitConverter.GetBytes((uint)(iRemain));
                    arrData[COMMIF_LENGTH_OFFSET + 0] = arrLength[3];
                    arrData[COMMIF_LENGTH_OFFSET + 1] = arrLength[2];
                    arrData[COMMIF_LENGTH_OFFSET + 2] = arrLength[1];
                    arrData[COMMIF_LENGTH_OFFSET + 3] = arrLength[0];

                    Array.Copy(pbcHead, iOffs, arrData, COMMIF_DATA_OFFSET, iRemain);
                    iSendLen = iRemain + COMMIF_SIZE;

                    iRemain = 0;
                }

                int iReturnCode = this._socket.Send(arrData, iSendLen, SocketFlags.None);
                if (iReturnCode != iSendLen)
                {
                    System.Diagnostics.Trace.WriteLine("send failed");
                    return ErrorCodes.RC_COMM_FAIL;
                } /* end if */
                else
                {
                    do
                    {
                        byte[] arrAck = new byte[COMMIF_SIZE];
                        iReturnCode = this._socket.Receive(arrAck, 0);
                        if (iReturnCode != COMMIF_SIZE)
                        {
                            System.Diagnostics.Trace.WriteLine("receive ACK failed");
                            //$$							Sleep(1);
                        } /* end if */
                    } while (iReturnCode <= 0);
                }

            }
            while (iRemain > 0);

            return ErrorCodes.RC_SUCCESS;
        }

        private ErrorCodes CallCeda(string commandText, DataCommandParameterCollection parameters, 
            int timeout, string workstation, string user, string twe)
        {
            return CallCeda(workstation, user, workstation, commandText,
                parameters[0].ToString(), strSeq, strTws, twe, parameters[3].ToString(),
                parameters[1].ToString(), parameters[2].ToString(), timeout.ToString());
        }

        private ErrorCodes CallCeda(string reqId, string dest1, string dest2, string command,
            string obj, string seq, string tws, string twe,
            string selection, string fields, string data, string timeout)
        {

            DateTime dateStart = DateTime.Now;
            /*UT.LogMsg(String.Format("CallCeda Start : {0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}.{6:000}, ",
                dt1.Year, dt1.Month, dt1.Day, dt1.Hour, dt1.Minute, dt1.Second, dt1.Millisecond) +
                "sel <" + opSelection + "> fld <" + opFields + "> data <" + opData + ">"
                + ", reqId<" + opReq_id + ">"
                + ", Dest1<" + opDest1 + ">, Dest2<" + opDest2 + ">"
                + ", Cmd<" + opCmd + ">, Obj<" + opObj + ">"
                + ", Seq<" + opSeq + ">, Tws<" + opTws + ">"
                + ", Twe<" + opTwe + ">, Sel<" + opSelection + ">"
                + ", TimeOut<" + opTimeout + ">"
                );*/
            int iBufLen = CalcBufLen(selection, fields, data);
            byte[] arrBcHead = new Byte[iBufLen];
            Array.Clear(arrBcHead, 0, arrBcHead.Length);

            Encoding encoding = Encoding.GetEncoding(this.Connection.WindowsCodePage);

            // copy all data to the structure
            encoding.GetBytes(dest1, 0, System.Math.Min(dest1.Length, 10), arrBcHead, BC_HEAD_DEST_NAME_OFFSET);		// dest_name
            encoding.GetBytes(timeout, 0, System.Math.Min(timeout.Length, 10), arrBcHead, BC_HEAD_ORIG_NAME_OFFSET); // orig_name 	
            encoding.GetBytes(dest2, 0, System.Math.Min(dest2.Length, 10), arrBcHead, BC_HEAD_RECV_NAME_OFFSET);    	// recv_name

            byte[] arrBcnum = BitConverter.GetBytes((short)0);
            arrBcHead[BC_HEAD_BC_NUM_OFFSET] = arrBcnum[0];		// bc_num
            arrBcHead[BC_HEAD_BC_NUM_OFFSET + 1] = arrBcnum[1];

            encoding.GetBytes(reqId, 0, System.Math.Min(reqId.Length, 10), arrBcHead, BC_HEAD_SEQ_ID_OFFSET);				// seq_id    	

            byte[] arTotBuf = BitConverter.GetBytes((short)0);		// tot_buf
            arrBcHead[BC_HEAD_TOT_BUF_OFFSET] = arTotBuf[0];
            arrBcHead[BC_HEAD_TOT_BUF_OFFSET + 1] = arTotBuf[1];

            byte[] arrActBuf = BitConverter.GetBytes((short)0);		// act_buf
            arrBcHead[BC_HEAD_ACT_BUF_OFFSET] = arrActBuf[0];
            arrBcHead[BC_HEAD_ACT_BUF_OFFSET + 1] = arrActBuf[1];

            encoding.GetBytes(command, 0, Math.Min(command.Length, 6), arrBcHead, BC_HEAD_DATA_OFFSET + CMDBLK_COMMAND_OFFSET);	// command
            encoding.GetBytes(obj, 0, Math.Min(obj.Length, 33), arrBcHead, BC_HEAD_DATA_OFFSET + CMDBLK_OBJ_NAME_OFFSET);											// obj_name     
            encoding.GetBytes(seq, 0, Math.Min(seq.Length, 2), arrBcHead, BC_HEAD_DATA_OFFSET + CMDBLK_ORDER_OFFSET);
            encoding.GetBytes(tws, 0, Math.Min(tws.Length, 33), arrBcHead, BC_HEAD_DATA_OFFSET + CMDBLK_TW_START_OFFSET);
            encoding.GetBytes(twe, 0, Math.Min(twe.Length, 33), arrBcHead, BC_HEAD_DATA_OFFSET + CMDBLK_TW_END_OFFSET);

            int iCmdSize = 4 - 1 + selection.Length + fields.Length;
            byte[] arrCmdSize = BitConverter.GetBytes((short)iCmdSize);
            arrBcHead[BC_HEAD_CMD_SIZE_OFFSET] = arrCmdSize[0];
            arrBcHead[BC_HEAD_CMD_SIZE_OFFSET + 1] = arrCmdSize[1];

            byte[] arrDataSize = BitConverter.GetBytes((short)data.Length);
            arrBcHead[BC_HEAD_DATA_SIZE_OFFSET] = arrDataSize[0];
            arrBcHead[BC_HEAD_DATA_SIZE_OFFSET + 1] = arrDataSize[1];

            int iTotSize = BC_HEAD_SIZE - 1 + iCmdSize + data.Length;
            byte[] arrTotSize = BitConverter.GetBytes((short)iTotSize);
            arrBcHead[BC_HEAD_TOT_SIZE_OFFSET] = arrTotSize[0];
            arrBcHead[BC_HEAD_TOT_SIZE_OFFSET + 1] = arrTotSize[1];

            int iIndex = BC_HEAD_DATA_OFFSET + CMDBLK_SIZE - 1;
            encoding.GetBytes(selection, 0, selection.Length, arrBcHead, iIndex);
            iIndex += selection.Length + 1;

            encoding.GetBytes(fields, 0, fields.Length, arrBcHead, iIndex);
            iIndex += fields.Length + 1;

            this.ConvertStringToBytes(encoding, data, arrBcHead, iIndex);
            DateTime dateEnd = DateTime.Now;
            TimeSpan ts = dateEnd.Subtract(dateStart);
            dateStart = dateEnd;
            //UT.LogMsg("CallCeda: b4 send time : " + ts.TotalMilliseconds.ToString());
            SendBuffer(arrBcHead, iBufLen);
            dateEnd = DateTime.Now;
            ts = dateEnd.Subtract(dateStart);
            dateStart = dateEnd;
            //UT.LogMsg("CallCeda: send time : " + ts.TotalMilliseconds.ToString());

            ErrorCodes ecTotalReturn;
            ecTotalReturn = ReceiveUfisData();
            return ecTotalReturn;
        }

        private int StrLen(byte[] data, int offset)
        {
            for (int i = offset; i < data.Length; i++)
            {
                if (data[i] == 0)
                {
                    return i - offset;
                }
            }

            return 0;
        }

        private string ConvertBytesToString(Encoding encoding, byte[] totalDataBuffer, int index, int transferBytes)
        {
#if	UseUtf8Encoding
            // we can't use UTF8Encoder class because this class tries to convert 2 bytes to a single unicode character
            return encoding.GetString(totalDataBuffer, index, transferBytes);
#else
            string strTotalDataBuffer = string.Empty;
            StringBuilder builder = new StringBuilder(transferBytes);
            byte[]	arrPair = new byte[2]; 
            arrPair[1] = 0;
            for (int i = 0; i < transferBytes; i++)
            {
                arrPair[0] = totalDataBuffer[index+i];
                builder.Append(BitConverter.ToChar(arrPair,0));
            }
            return builder.ToString();
#endif
        }

        private void ConvertStringToBytes(Encoding encoding, string data, byte[] arrBCHead, int index)
        {
#if	UseUtf8Encoding
            encoding.GetBytes(data, 0, data.Length, arrBCHead, index);
#else
            for (int i = 0; i < data.Length; i++)
            {
                byte[] arrResult = BitConverter.GetBytes(data[i]);
                arrBCHead[index+i] = arrResult[0];				
            }
#endif

        }

        private bool AddToList(Encoding encoding, ref string list, byte[] actualData, int offset, ref int length)
        {
            bool bFound = false;

            int iLen = StrLen(actualData, offset);

            if (iLen < length)
            {
                bFound = true;
                list += this.ConvertBytesToString(encoding, actualData, offset, iLen);
                length -= iLen + 1;
            }

            if (!bFound)
            {
                list += this.ConvertBytesToString(encoding, actualData, offset, length);
            }

            return bFound;
        }

        private bool MakeRow(Encoding encoding, byte[] dataBuf, int offset, int command, int length, bool firstLine)
        {
            string strDat = string.Empty;
            string strNewStr = string.Empty;
            string strRest = string.Empty;

            if (firstLine)
            {
                strStaticRest = string.Empty;
            }

            if (strStaticRest.Length != 0)
            {
                int iDatLen;
                string strTmpBuf = this.ConvertBytesToString(encoding, dataBuf, offset, length);
                strDat = strStaticRest + strTmpBuf;	//pclDataBuf;
                iDatLen = strDat.Length;
                length += strStaticRest.Length;
            }
            else
            {
                strDat = this.ConvertBytesToString(encoding, dataBuf, offset, length);
            }

            int iIdx = strDat.IndexOf("\n");
            int iCrIdx = strDat.IndexOf("\r");
            if (iIdx == -1)
            {
                strRest = strDat.Substring(0, length);
            }

            while (iIdx != -1)
            {
                if (iIdx != 0)
                {
                    if (iCrIdx != -1)
                    {
                        strNewStr = strDat.Substring(0, iIdx - 1);
                        int iRight = strDat.Length - iIdx - 1;
                        strRest = strDat.Substring(strDat.Length - iRight);
                        length -= (iIdx + 1);   /// + 1 wg CR
                    }
                    else
                    {
                        strNewStr = strDat.Substring(0, iIdx);
                        int iRight = strDat.Length - iIdx - 1;
                        strRest = strDat.Substring(strDat.Length - iRight);
                        length -= (iIdx + 1);
                    }
                    _rows.AddRow(strNewStr, ',');
                    int iTest = strNewStr.Length;
                    strDat = strRest;
                    iIdx = strDat.IndexOf("\n");
                    iCrIdx = strDat.IndexOf("\r");
                }
                else
                {
                    if (iCrIdx != -1)
                    {
                        strNewStr = strDat.Substring(0, iIdx - 1);
                        int iRight = strDat.Length - iIdx - 1;
                        strRest = strDat.Substring(strDat.Length - iRight);
                        length -= (iIdx + 1);   /// + 1 wg CR
                    }
                    else
                    {
                        strNewStr = strDat.Substring(0, iIdx);
                        int iRight = strDat.Length - iIdx - 1;
                        strRest = strDat.Substring(strDat.Length - iRight);
                        length -= (iIdx + 1);
                    }
                    int iTest = strNewStr.Length;
                    strDat = strRest;
                    iIdx = strDat.IndexOf("\n");
                    iCrIdx = strDat.IndexOf("\r");
                }
            }
            if ((ushort)command == PACKET_DATA)
            {
                strStaticRest = strRest.Substring(0, length);
            }
            else if ((ushort)command == PACKET_END)
            {
                if (length > 1)
                {
                    if (strDat.Length >= length)
                    {
                        string strEndString = new string('\0', 5);
                        int iEndStringLoc = strDat.IndexOf(strEndString);
                        if (iEndStringLoc < 0) iEndStringLoc = length;
                        _rows.AddRow(strDat.Substring(0, iEndStringLoc), ',');
                    }
                    else
                    {
                        _rows.AddRow(strDat, ',');
                    }
                }
            }
            return true;
        }


        private ErrorCodes ReceiveUfisData()
        {
            this._rows.Clear();
            this.strSelection = string.Empty;
            this.strFieldList = string.Empty;
            this.strLastErrorMessage = string.Empty;

            Encoding encoding = Encoding.GetEncoding(this.Connection.WindowsCodePage);

            ErrorCodes errorCodes = ErrorCodes.RC_SUCCESS;
            //int packet_len = NETREAD_SIZE;
            int iReturnCode = 0;
            //int count = 0;
            int iCurrLen = 0;
            int iPLen = 0;
            byte[] arrData = new byte[PACKET_LEN];

            bool bReady = false;
            bool bFirstLine = true;
            bool bFirstData = true;
            short sStatus = SELECTION;
            bool bDelFound = false;

            while (!bReady)
            {
                int iCurrPData = 0;	// offset
                iCurrLen = 0;
                iPLen = PACKET_LEN;

                while ((errorCodes == ErrorCodes.RC_SUCCESS) && (iCurrLen < PACKET_LEN) && (iCurrLen < iPLen))
                {
                    iReturnCode = _socket.Receive(arrData, iCurrPData, PACKET_LEN - iCurrLen, SocketFlags.None);
                    System.Diagnostics.Trace.WriteLine(iReturnCode.ToString());
                    if (iReturnCode <= 0)
                    {
                        bReady = true;
                        errorCodes = ErrorCodes.RC_FAIL;
                    }
                    else
                    {
                        if (iReturnCode == 0)
                        {
                            // MCU 96.10.11 we should return RC_FAIL if the connection has been closed
                            errorCodes = ErrorCodes.RC_FAIL;
                        }
                        else
                        {
                            iCurrLen += iReturnCode;
                            iCurrPData += iReturnCode;
                            if (iCurrLen >= COMMIF_SIZE)
                            {
                                /* How many bytes will come ? */
                                byte[] olLength = new Byte[4];
                                olLength[0] = arrData[COMMIF_LENGTH_OFFSET + 3];
                                olLength[1] = arrData[COMMIF_LENGTH_OFFSET + 2];
                                olLength[2] = arrData[COMMIF_LENGTH_OFFSET + 1];
                                olLength[3] = arrData[COMMIF_LENGTH_OFFSET];
                                iPLen = BitConverter.ToInt32(olLength, 0) + COMMIF_SIZE;
                            }
                            errorCodes = ErrorCodes.RC_SUCCESS;
                        }
                    }
                }

                int iDataLen = 0;
                int iActData = 0;
                bool bReady2 = true;

                if (errorCodes == ErrorCodes.RC_SUCCESS)
                {
                    byte[] arrAck = new byte[COMMIF_SIZE];
                    byte[] arrCmd = BitConverter.GetBytes((ushort)PACKET_DATA);
                    arrAck[COMMIF_COMMAND_OFFSET] = arrCmd[1];
                    arrAck[COMMIF_COMMAND_OFFSET + 1] = arrCmd[0];

                    byte[] arrLength = BitConverter.GetBytes(COMMIF_SIZE);
                    arrAck[COMMIF_LENGTH_OFFSET] = arrLength[3];
                    arrAck[COMMIF_LENGTH_OFFSET] = arrLength[2];
                    arrAck[COMMIF_LENGTH_OFFSET] = arrLength[1];
                    arrAck[COMMIF_LENGTH_OFFSET] = arrLength[0];

                    _socket.Send(arrAck, 0, COMMIF_SIZE, SocketFlags.None);

                    arrCmd[0] = arrData[COMMIF_COMMAND_OFFSET + 1];
                    arrCmd[1] = arrData[COMMIF_COMMAND_OFFSET];
                    int iCmd = BitConverter.ToInt16(arrCmd, 0);

                    arrLength[0] = arrData[COMMIF_LENGTH_OFFSET + 3];
                    arrLength[1] = arrData[COMMIF_LENGTH_OFFSET + 2];
                    arrLength[2] = arrData[COMMIF_LENGTH_OFFSET + 1];
                    arrLength[3] = arrData[COMMIF_LENGTH_OFFSET];
                    int iLength = BitConverter.ToInt32(arrLength, 0);

                    if (bFirstLine)
                    {
                        //	int ilGarbageLen = sizeof(BC_HEAD) -1 + sizeof(CMDBLK) - 1;
                        int iGarbageLen = BC_HEAD_SIZE - 1 + CMDBLK_SIZE - 1 - 1;
                        iDataLen = iLength - iGarbageLen;

                        int iTest = iGarbageLen + 8;
                        int iBcHead = COMMIF_DATA_OFFSET;
                        int iRc = BitConverter.ToInt16(arrData, iBcHead + BC_HEAD_RC_OFFSET);
                        if (iRc < 0)
                        {
                            sStatus = 14; //to prevent that switch/case of status will be entered
                            int iData = iBcHead + BC_HEAD_DATA_OFFSET;
                            strLastErrorMessage = this.ConvertBytesToString(encoding, arrData, iData, StrLen(arrData, iData));
                            errorCodes = ErrorCodes.RC_FAIL;
                        }
                        else
                        {
                            int iCmdBlk = iBcHead + BC_HEAD_DATA_OFFSET;			//	(CMDBLK  *) BcHead->data;
                            iActData = iCmdBlk + CMDBLK_DATA_OFFSET;			//	(char *) CmdBlk->data;
                            int iSelKey = iActData;								//	(char *) CmdBlk->data;

                            int iFldLst = iSelKey + StrLen(arrData, iSelKey) + 1;
                            int iDatLst = iFldLst + StrLen(arrData, iFldLst) + 1;

                            bFirstLine = false;
                            int iSeqId = iBcHead + BC_HEAD_SEQ_ID_OFFSET;
                            strReqId = this.ConvertBytesToString(encoding, arrData, iSeqId, StrLen(arrData, iSeqId));

                            int iDest1 = iBcHead + BC_HEAD_DEST_NAME_OFFSET;
                            strDest1 = this.ConvertBytesToString(encoding, arrData, iDest1, StrLen(arrData, iDest1));

                            int iDest2 = iBcHead + BC_HEAD_RECV_NAME_OFFSET;
                            strDest2 = this.ConvertBytesToString(encoding, arrData, iDest2, StrLen(arrData, iDest2));

                            int iCommand = iCmdBlk + CMDBLK_COMMAND_OFFSET;
                            strCommandText = this.ConvertBytesToString(encoding, arrData, iCommand, StrLen(arrData, iCommand));

                            int iObject = iCmdBlk + CMDBLK_OBJ_NAME_OFFSET;
                            strCommandObject = this.ConvertBytesToString(encoding, arrData, iObject, StrLen(arrData, iObject));

                            int iSeq = iBcHead + BC_HEAD_SEQ_ID_OFFSET;
                            strSeq = this.ConvertBytesToString(encoding, arrData, iSeq, StrLen(arrData, iSeq));

                            int iTws = iCmdBlk + CMDBLK_TW_START_OFFSET;
                            strTws = this.ConvertBytesToString(encoding, arrData, iTws, StrLen(arrData, iTws));

                            int iTwe = iCmdBlk + CMDBLK_TW_END_OFFSET;
                            strTwe = this.ConvertBytesToString(encoding, arrData, iTwe, StrLen(arrData, iTwe));

                            int iOrig = iBcHead + BC_HEAD_ORIG_NAME_OFFSET;
                            strOrigName = this.ConvertBytesToString(encoding, arrData, iOrig, StrLen(arrData, iOrig));
                        }
                    }
                    else
                    {
                        iActData = COMMIF_DATA_OFFSET;
                        iDataLen = iLength;
                    }
                    do
                    {
                        switch (sStatus)
                        {
                            case SELECTION:
                                bDelFound = AddToList(encoding, ref this.strSelection, arrData, iActData, ref iDataLen);
                                break;
                            case FIELDS:
                                bDelFound = AddToList(encoding, ref this.strFieldList, arrData, iActData, ref iDataLen);
                                break;
                            case DATA:
                                MakeRow(encoding, arrData, iActData, iCmd, iDataLen, bFirstData);
                                bFirstData = false;
                                break;
                        };
                        bReady2 = true;

                        if (sStatus != DATA)
                        {
                            if (bDelFound)
                            {
                                iActData += StrLen(arrData, iActData) + 1;
                                sStatus++;
                                bReady2 = false;
                                bDelFound = false;
                            }
                        }
                    } while (!bReady2);

                    if ((ushort)iCmd == PACKET_END)
                    {
                        bReady = true;
                    }

                } /* fi */
            } /* end while */
            return errorCodes;
        }



        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaDataWriter class.
        /// </summary>
        public CedaDataWriter() : this(null, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaDataWriter class.
        /// </summary>
        /// <param name="connection">The Ufis.Data.Ceda.CedaConection used by this instance of 
        /// Ufis.Data.Ceda.CedaDataWriter</param>
        /// <param name="command">The Ufis.Data.DataCommand used by this instance of 
        /// Ufis.Data.Ceda.CedaDataWriter</param>
        public CedaDataWriter(CedaConnection connection, DataCommand command)
        {
            this.Connection = connection;
            this.Command = command;

            //default values
            this._rows = new DataRowCollection();
            
            this.strCommandText = string.Empty;
            this.strDest1 = string.Empty;
            this.strDest2 = string.Empty;
            this.strCommandObject = string.Empty;
            this.strOrigName = string.Empty;
            this.strReqId = string.Empty;
            this.strSelection = string.Empty;
            this.strSeq = string.Empty;
            this.strTwe = string.Empty;
            this.strTws = string.Empty;
        }
    }
}
