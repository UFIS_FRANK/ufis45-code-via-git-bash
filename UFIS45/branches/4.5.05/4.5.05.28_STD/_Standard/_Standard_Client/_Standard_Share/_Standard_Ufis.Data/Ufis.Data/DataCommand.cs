﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Data
{
    public class DataCommand
    {
        const int DEFAULT_COMMAND_TIMEOUT = 240;

        /// <summary>
        /// Gets or sets the timeout parameter in seconds.
        /// </summary>
        public int CommandTimeout { get; set; }
        /// <summary>
        /// Gets or sets the command text to execute.
        /// </summary>
        public string CommandText { get; set; }
        /// <summary>
        /// Gets or sets the Ufis.Data.DataCommandParameterCollectionBase.
        /// </summary>
        public DataCommandParameterCollection Parameters { get; set; }

        /// <summary>
        /// Resets the Ufis.Data.DataCommandBase.CommandTimeout property to the default value.
        /// </summary>
        public void ResetCommandTimeout()
        {
            CommandTimeout = DEFAULT_COMMAND_TIMEOUT;
        }
    }
}
