﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Ufis.Data
{
    /// <summary>
    /// Represents the mapping for entity to database's table.
    /// </summary>
    public class EntityTableMapping
    {
        private Type _entityType;
        private readonly EntityColumnMappingCollection _entityColumnMaps;

        /// <summary>
        /// Gets or sets the entity type.
        /// </summary>
        public Type EntityType
        {
            get
            {
                return _entityType;
            }

            set
            {
                if (!typeof(Entity).IsAssignableFrom(value))
                {
                    throw new ArgumentException("EntityType");
                }
                else
                {
                    _entityType = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the table name.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Gets the entity column mappings for this instance of Ufis.Entities.EntityTableMapping.
        /// </summary>
        public EntityColumnMappingCollection ColumnMappings 
        {
            get
            {
                return _entityColumnMaps;
            }
        }

        /// <summary>
        /// Creates a new instance of Ufis.Entities.EntityTableMapping object.
        /// </summary>
        public EntityTableMapping()
        {
            _entityColumnMaps = new EntityColumnMappingCollection();
        }
    }
}
