// BDPSPASS.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <BDPSPASS.h>
#include <BDPSPASSDlg.h>
#include <CCSGlobl.h>
#include <GUILng.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSApp

BEGIN_MESSAGE_MAP(CBDPSPASSApp, CWinApp)
	//{{AFX_MSG_MAP(CBDPSPASSApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSApp construction

CBDPSPASSApp::CBDPSPASSApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CBDPSPASSApp::~CBDPSPASSApp()
{
	delete CGUILng::TheOne();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBDPSPASSApp object

CBDPSPASSApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSApp initialization


BOOL CBDPSPASSApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// read ceda.ini
	ReadCfgFile();
	if (!AfxOleInit())
	{

		return FALSE;
	}

	strncpy(CCSCedaData::pcmTableExt, pcgTableExt,3);
	strncpy(CCSCedaData::pcmHomeAirport, pcgHome,3);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);

	// Check condition of the communication with the server
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogBCD.pomCommHandler = &ogCommHandler;
	CGUILng* ogGUILng = CGUILng::TheOne();

	char pclConfigFile[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigFile, getenv("CEDA"));
	char pclTmp[100];
    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, pclConfigFile);
	ogGUILng->SetLanguage(CString(pclTmp));	
    GetPrivateProfileString(pcgAppName, "DBParam", "", pclTmp, sizeof pclTmp, pclConfigFile);
	if(!strcmp(pclTmp,"1"))
	{
		ogGUILng->SetStoreInDB(CString(pclTmp));
	}



	CBDPSPASSDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


void CBDPSPASSApp::ReadCfgFile(void)
{
	char pclConfigFile[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigFile, getenv("CEDA"));


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigFile);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigFile);
	strcat(pcgTableExt," ");



} // end ReadCfgFile()
