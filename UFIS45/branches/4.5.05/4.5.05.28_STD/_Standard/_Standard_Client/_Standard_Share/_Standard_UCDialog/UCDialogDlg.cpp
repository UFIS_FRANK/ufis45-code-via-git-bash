// UCDialogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UCDialog.h"
#include "UCDialogDlg.h"
#include "CGlobal.h"
#include "ClntTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUCDialogDlg dialog

CUCDialogDlg::CUCDialogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUCDialogDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUCDialogDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_UFISAmSink.SetLauncher(this);
	mobjUtilUC = new UtilUC();
	m_bAirportFlag = false;
}

void CUCDialogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUCDialogDlg)
	DDX_Control(pDX, IDC_UCEDITCTRL1, m_UCEdit1);
	DDX_Control(pDX, IDC_UCEDITCTRL2, m_UCEdit2);
	DDX_Control(pDX, IDC_UCEDITCTRL3, m_UCEdit3);
	DDX_Control(pDX, IDC_UCEDITCTRL4, m_UCEdit4);
	DDX_Control(pDX, IDC_UCEDITCTRL5, m_UCEdit5);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUCDialogDlg, CDialog)
	//{{AFX_MSG_MAP(CUCDialogDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUCDialogDlg message handlers

BOOL CUCDialogDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	InitCOMInterface();
	  // return TRUE  unless you set the focus to a control
	CString olCmdLine = AfxGetApp()->m_lpCmdLine;
	
	BYTE byteArr[200];
	char* st = new char[400];
	
	CStringArray olArray ;
	int k ;
	CString str;
	for(int j = 0 ; j <4;j++)
	{
		k = olCmdLine.Find(_T(","));
		str = olCmdLine.Left(k);
		olArray.Add(olCmdLine.Left(k));
		olCmdLine =	olCmdLine.Mid(k+1);
	}

	olArray.Add(olCmdLine);

	//Finding if its a Airport Telugu dialog or FIDS Remark
	CWnd *polWnd;
	if(olArray.GetAt(0) == _T("Airport"))
	{
		polWnd = GetDlgItem(IDC_STATIC_AB1);
		polWnd->ShowWindow(TRUE);

		polWnd = GetDlgItem(IDC_STATIC_AB2);
		polWnd->ShowWindow(TRUE);

		polWnd = GetDlgItem(IDC_STATIC_AB3);
		polWnd->ShowWindow(TRUE);
		
		polWnd = GetDlgItem(IDC_STATIC_AB4);
		polWnd->ShowWindow(TRUE);

		SetWindowText(_T("Airport Name Dialog"));

		m_bAirportFlag = true;

	}
	else
	{
		polWnd = GetDlgItem(IDC_STATIC_CM1);
		polWnd->ShowWindow(TRUE);

		polWnd = GetDlgItem(IDC_STATIC_CM2);
		polWnd->ShowWindow(TRUE);

		polWnd = GetDlgItem(IDC_STATIC_CM3);
		polWnd->ShowWindow(TRUE);
		
		polWnd = GetDlgItem(IDC_STATIC_AB4);
		polWnd->ShowWindow(0);

		polWnd = GetDlgItem(IDC_UCEDITCTRL5);
		polWnd->ShowWindow(0);

		SetWindowText(_T("FIDS Remark Dialog"));
		
		m_bAirportFlag = false;
	}

	CString strr;
	strr = olArray.GetAt(1);
	int i = 0;
	for(i = 0;i<strr.GetLength();i++)
	{
	
		st[i] = (char)strr.GetAt(i);
	}
	st[i] = '\0';
	short cnt = 0;
	mobjUtilUC->ConvOctStToByteArr( st, byteArr, cnt);
	m_UCEdit1.SetUCText( (LPUNKNOWN)byteArr, cnt );
	
	//Editbox 2 
	//	buf = " ";
	strr = olArray.GetAt(2);
	for(i = 0;i<strr.GetLength();i++)
	{
	
		st[i] = (char)strr.GetAt(i);
	}
	
	st[i] = '\0';

	//st = buf1;
	cnt = 0;
	mobjUtilUC->ConvOctStToByteArr( st, byteArr, cnt);
	m_UCEdit3.SetUCText( (LPUNKNOWN)byteArr, cnt );

	//Edit box 3
	//buf = " ";
	strr = olArray.GetAt(3);
	for(i = 0;i<strr.GetLength();i++)
	{
	
		st[i] = (char)strr.GetAt(i);
	}

	st[i] = '\0';

	//st = buf2;
	cnt = 0;
	mobjUtilUC->ConvOctStToByteArr( st, byteArr, cnt);
	m_UCEdit4.SetUCText( (LPUNKNOWN)byteArr, cnt );

	//Edit box 4 
	//buf = " ";
	strr = olArray.GetAt(4);
	for(i = 0;i<strr.GetLength();i++)
	{
	
		st[i] = (char)strr.GetAt(i);
	}
	st[i] = '\0';

	//st = buf3;
	//cnt = 0;
	mobjUtilUC->ConvOctStToByteArr( st, byteArr, cnt);
	m_UCEdit5.SetUCText( (LPUNKNOWN)byteArr, cnt );

	delete[] st;

	return TRUE;  // return TRUE  unless y set the focus to a control
}

void CUCDialogDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUCDialogDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUCDialogDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CUCDialogDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
	BYTE byteArr[200] = {14,30,14,35,14,32,14,50,14,30};
	short cnt = 10;
//	cnt = m_UCEdit1.GetUCText( (LPUNKNOWN)byteArr );	
//	m_UCEdit2.SetUCText( (LPUNKNOWN)byteArr, cnt );
	char* st = new char[400];
	//strcpy(st,"0E1E0E230E20");
	//UtilUC* objUtilUC = new UtilUC();
	//cnt = m_UCEdit1.GetUCText( (LPUNKNOWN)byteArr );
	//if (objUtilUC->ConvByteArrToHexSt( byteArr, cnt, st ))
	{
	//	MessageBox( ( st ));
	}
	//objUtilUC->ConvHexStToByteArr( st, byteArr, cnt );
	//m_UCEdit2.SetUCText( (LPUNKNOWN)byteArr, cnt );



	cnt = m_UCEdit1.GetUCText( (LPUNKNOWN)byteArr );
	if (mobjUtilUC->ConvByteArrToOctSt( byteArr, cnt, st ))
	{
		int i = 0;
		//MessageBox( ( st ));
	}
	mobjUtilUC->ConvOctStToByteArr( st, byteArr, cnt );
	m_UCEdit2.SetUCText( (LPUNKNOWN)byteArr, cnt );
         
	char buffer[1000] = ""; 
	 
	//First Edit Box 
	CString olFinalData = ""; 
	cnt = m_UCEdit1.GetUCText( (LPUNKNOWN)byteArr ); 
	mobjUtilUC->ConvByteArrToOctSt( byteArr, cnt, st ); 
	olFinalData = st; 
 
	//Second Edit Box 
	cnt = m_UCEdit3.GetUCText( (LPUNKNOWN)byteArr ); 
	mobjUtilUC->ConvByteArrToOctSt( byteArr, cnt, st ); 
	olFinalData = olFinalData + _T(",") + st; 
 
	//Third Edit Box 
	cnt = m_UCEdit4.GetUCText( (LPUNKNOWN)byteArr ); 
	mobjUtilUC->ConvByteArrToOctSt( byteArr, cnt, st ); 
	olFinalData = olFinalData + _T(",") + st; 
 
	//Fourth Edit Box  
	cnt = m_UCEdit5.GetUCText( (LPUNKNOWN)byteArr ); 
	mobjUtilUC->ConvByteArrToOctSt( byteArr, cnt, st ); 
	olFinalData = olFinalData + _T(",") + st; 
	
	if(m_bAirportFlag)
		olFinalData = olFinalData + _T(",") + _T("Airport");
	else
		olFinalData = olFinalData + _T(",") + _T("FidsRemark");	

	/*for(int k =0;k<olFinalData.GetLength();k++) 
	{ 
		buffer[k] = olFinalData.GetAt(k); 
	} */
	
	_bstr_t tmpStr = olFinalData.GetBuffer(olFinalData.GetLength());
	
	try
	{
		pConnect->TransferData(static_cast<long>(Bdps),static_cast<long>(UCDialog),tmpStr);
	}
	catch(_com_error error)
	{
		AfxMessageBox(_T("Could not connect to UFISAppManager!!\n Abort the Opreation\n"));
	}
	 
	CDialog::OnOK(); 
}

void CUCDialogDlg::OnCancel()
{
	_bstr_t tmpStr = "6B4CCF9C-5AA9-4ede-B298-08BAAAC9FB88";
	pConnect->TransferData(static_cast<long>(Bdps),static_cast<long>(UCDialog),tmpStr);
	CDialog::OnCancel();
}


void CUCDialogDlg::ExternCall(LPCSTR popMessage)
{
	//TRACE("CCuteIFDlg::ExternCall: %s\n", popMessage);

	// Buffer Message
	CString olMessage(popMessage);
	omExtMesBuffer.Add(olMessage);
	// Set Timer
    SetTimer(1, (UINT) 1 * 1000, NULL); // process new message in one sec
}

void CUCDialogDlg::OnTimer(UINT nIDEvent)
{
	
	AfxMessageBox(_T("Timer Called"));
}


bool CUCDialogDlg::InitCOMInterface()
{

	HRESULT olRes = CoInitialize(NULL);
	if (olRes == E_INVALIDARG)
		int asdf = 0;
	if (olRes == E_OUTOFMEMORY)
		int asdf = 0;
	if (olRes == E_UNEXPECTED)
		int asdf = 0;
	if (olRes == S_FALSE)
		int asdf = 0;
	if (olRes == RPC_E_CHANGED_MODE)
		int asdf = 0;

 	if(olRes != S_OK && olRes != S_FALSE)
	{
		//AfxMessageBox(_T("CoInitializeEx failed!"));
		return false;
	}
	

	try
	{
		pConnect = IUFISAmPtr(CLSID_UFISAm);
		_bstr_t tmpStr = "71425678-6CF3-438f-AFA7-946DF4D5674F";
		pConnect->TransferData(static_cast<long>(Bdps),static_cast<long>(UCDialog),tmpStr);

	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}


	try
	{
		BOOL Res = m_UFISAmSink.Advise(pConnect,IID_IUFISAmEventSink);
		ASSERT(Res == TRUE);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}
	/*
	 * Assign AppTag immediately after advising the sink!
	 */
	
	return true;
}
