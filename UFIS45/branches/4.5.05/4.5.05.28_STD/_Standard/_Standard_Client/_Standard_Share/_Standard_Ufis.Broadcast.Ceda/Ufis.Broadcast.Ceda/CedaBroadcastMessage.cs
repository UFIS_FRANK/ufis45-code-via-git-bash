﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Broadcast.Ceda
{
    public class CedaBroadcastMessage : BroadcastMessageBase
    {
        public string RequestId;
        public string Destination1;
        public string Destination2;
        public string Command;
        public string Object;
        public string Sequence;
        public string Tws;
        public string Twe;
        public string Selection;
        public string Fields;
        public string Data;
        public string BroadcastNumber;
        public string Attachment;
        public string Additional;
    }
}
