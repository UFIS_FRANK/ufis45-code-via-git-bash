// Aatedit5Ctl.cpp : Implementation of the CAatedit5Ctrl ActiveX Control class.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	AATEdit5:	ActiveX Wrapper for CCSEdit
 *
 *	This ActiveX control is a wrapper for the existing functionality
 *	of the old CCSEdit MFC Control.
 *	
 *	ABB Airport Technologies GmbH 2001
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/ID		09/02/2001		Initial version
 *		cla AAT/ID		21/02/2001		SetTypeToInt added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "aatedit5.h"
#include "Aatedit5Ctl.h"
#include "Aatedit5Ppg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*
 * User defined commands
 * sent by CCSEdit
 * need to catch them
 */
#define WM_AAT_KILLFOCUS_CMD                (WM_USER + 510)
#define WM_AAT_LBUTTONDOWN_CMD			    (WM_USER + 520)  
#define WM_AAT_CHANGE_CMD					(WM_USER + 518)  

IMPLEMENT_DYNCREATE(CAatedit5Ctrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAatedit5Ctrl, COleControl)
	//{{AFX_MSG_MAP(CAatedit5Ctrl)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CAatedit5Ctrl, COleControl)
	//{{AFX_DISPATCH_MAP(CAatedit5Ctrl)
	DISP_FUNCTION(CAatedit5Ctrl, "SetBkColor", SetBkColor, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToDouble", SetTypeToDouble, VT_EMPTY, VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToMoney", SetTypeToMoney, VT_EMPTY, VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToTime", SetTypeToTime, VT_EMPTY, VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToDate", SetTypeToDate, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "SetInitText", SetInitText, VT_EMPTY, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "SetReadOnly", SetReadOnly, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTextConstraint", SetTextConstraint, VT_EMPTY, VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "SetFormat", SetFormat, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CAatedit5Ctrl, "SetRegularExpression", SetRegularExpression, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CAatedit5Ctrl, "CheckRegMatch", CheckRegMatch, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAatedit5Ctrl, "SetRange", SetRange, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetPrecision", SetPrecision, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTextColor", SetTextColor, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTextChangedColor", SetTextChangedColor, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "GetWindowText", GetWindowText, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatedit5Ctrl, "SetSecState", SetSecState, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CAatedit5Ctrl, "GetTextColor", GetTextColor, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTextErrColor", SetTextErrColor, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "GetStatus", GetStatus, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAatedit5Ctrl, "SetStatus", SetStatus, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(CAatedit5Ctrl, "IsChanged", IsChanged, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToString", SetTypeToString, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTypeToInt", SetTypeToInt, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAatedit5Ctrl, "SetTextLimit", SetTextLimit, VT_EMPTY, VTS_I4 VTS_I4 VTS_BOOL)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CAatedit5Ctrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CAatedit5Ctrl, COleControl)
	//{{AFX_EVENT_MAP(CAatedit5Ctrl)
	EVENT_CUSTOM("Change", FireChange, VTS_NONE)
	EVENT_CUSTOM("LButtonDown", FireLButtonDown, VTS_NONE)
	EVENT_CUSTOM("KillFocus", FireKillFocus, VTS_NONE)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAatedit5Ctrl, 1)
	PROPPAGEID(CAatedit5PropPage::guid)
END_PROPPAGEIDS(CAatedit5Ctrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAatedit5Ctrl, "AATEDIT5.Aatedit5Ctrl.1",
	0xc3865245, 0xfe62, 0x11d4, 0x90, 0xbc, 0, 0x1, 0x2, 0x4, 0xaa, 0x51)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CAatedit5Ctrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DAatedit5 =
		{ 0xc3865243, 0xfe62, 0x11d4, { 0x90, 0xbc, 0, 0x1, 0x2, 0x4, 0xaa, 0x51 } };
const IID BASED_CODE IID_DAatedit5Events =
		{ 0xc3865244, 0xfe62, 0x11d4, { 0x90, 0xbc, 0, 0x1, 0x2, 0x4, 0xaa, 0x51 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwAatedit5OleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAatedit5Ctrl, IDS_AATEDIT5, _dwAatedit5OleMisc)


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::CAatedit5CtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAatedit5Ctrl

BOOL CAatedit5Ctrl::CAatedit5CtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AATEDIT5,
			IDB_AATEDIT5,
			afxRegApartmentThreading,
			_dwAatedit5OleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::CAatedit5Ctrl - Constructor

CAatedit5Ctrl::CAatedit5Ctrl()
{
	InitializeIIDs(&IID_DAatedit5, &IID_DAatedit5Events);
	omEditRect = CRect(0,0,150,23);
	SetInitialSize(150,23);

	// TODO: Initialize your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::~CAatedit5Ctrl - Destructor

CAatedit5Ctrl::~CAatedit5Ctrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::OnDraw - Drawing function

void CAatedit5Ctrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::DoPropExchange - Persistence support

void CAatedit5Ctrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::OnResetState - Reset control to default state

void CAatedit5Ctrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl::AboutBox - Display an "About" box to the user

void CAatedit5Ctrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_AATEDIT5);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl message handlers

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::OnCreate()
// 
// * Description : Creates the CCSEdit Control
// 
// 
// * Author : [cla AAT/ID], Created : [09.02.01 11:20:14]
// 
// * Returns : [int] - TRUE/FALSE
// 
// * Function parameters : 
// [lpCreateStruct] - The CREATESTRUCT structure defines the 
//                    initialization parameters passed to the window procedure 
//                    of an application. 
// 
// ==================================================================
int CAatedit5Ctrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	/*
	 * Get the actual parameters from the CREATESTRUCT
	 */
	omEditRect = CRect(0,0,lpCreateStruct->cx,lpCreateStruct->cy);
	// TODO: Add your specialized creation code here
	if(omEdit.Create(ES_AUTOHSCROLL | WS_BORDER | WS_VISIBLE,omEditRect,this,512) == FALSE)
	{
		AfxMessageBox(_T("Could not create CCSEdit"));
		return -1;
	}
	/*
	 * Create the font the Workbench
	 * Just a quick hack, shall be removed by means of IFont OCX 96 spec ...
	 */
	
	
//	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfHeight = - MulDiv(8, (this->GetDC())->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    omMSSansSerif_Regular_8.CreateFontIndirect(&logFont);
	omEdit.SetEditFont(&omMSSansSerif_Regular_8);
	return 0;
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetBkColor()
// 
// * Description : Sets the back color of the 
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:25:46]
// 
// * Returns : [void] - 
// 
// * Function parameters : 
// [BackColor] - COLORREF
// 
// ==================================================================
void CAatedit5Ctrl::SetBkColor(long BackColor) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetBKColor(BackColor);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToDouble()
// 
// * Description : Sets the type to double
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:27:39]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [PrePoint] - Pre point
// [PostPoint] - Post point
// [From] - lower range
// [To] - upper range
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToDouble(long PrePoint, long PostPoint, double From, double To) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTypeToDouble(PrePoint,PostPoint,From,To);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToMoney()
// 
// * Description : Sets type to currency
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:28:46]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [PrePoint] - Pre point
// [PostPoint] - Post point
// [From] - lower range
// [To] - upper range
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToMoney(long PrePoint, long PostPoint, double From, double To) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTypeToMoney(PrePoint,PostPoint,From,To);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToTime()
// 
// * Description : Sets type to time
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:30:33]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [Req] - field required
// [ChangeDay] - ???
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToTime(BOOL Req, BOOL ChangeDay) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTypeToTime(Req,ChangeDay);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToDate()
// 
// * Description : Sets type to date
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:31:15]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [Req] - field required
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToDate(BOOL Req) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTypeToDate(Req);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetInitText()
// 
// * Description : Sets the initial text string
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:32:36]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [IniText] - initial text string
// 
// ==================================================================
void CAatedit5Ctrl::SetInitText(LPCTSTR IniText,BOOL ChangeColor) 
{
	// TODO: Add your dispatch handler code here
	CString olStr(IniText);

	omEdit.SetInitText(olStr,ChangeColor);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetReadOnly()
// 
// * Description : Sets the edit field to read-only mode
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:33:22]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [ReadOnly] - TRUE/FALSE
// 
// ==================================================================
void CAatedit5Ctrl::SetReadOnly(BOOL ReadOnly) 
{
	// TODO: Add your dispatch handler code here
	if(ReadOnly == TRUE)
	{
		omEdit.SetReadOnly(true);
	}
	else
	{
		omEdit.SetReadOnly(false);
	}

}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTextConstraint()
// 
// * Description : Sets a text constraint for strings
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:34:37]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [MinLength] - minmal length
// [MaxLength] - maximal length
// [EmptyValid] - empty string valid
// 
// ==================================================================
void CAatedit5Ctrl::SetTextConstraint(long MinLength, long MaxLength, BOOL EmptyValid) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTextLimit(MinLength,MaxLength,EmptyValid);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetFormat()
// 
// * Description : Sets a format string, bla bla seems to be obsolete
// * see also the regular exprssion stuff ...
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:35:37]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [FormatStr] - ??? - see parameter list in CCSEdit.h
// 
// ==================================================================
void CAatedit5Ctrl::SetFormat(LPCTSTR FormatStr) 
{
	// TODO: Add your dispatch handler code here
	CString olStr(FormatStr);

	omEdit.SetFormat(olStr);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetRegularExpression()
// 
// * Description : Set a regular expression
// 
// 
// * Author : [cla AAT/ID], Created : [12.02.01 15:26:26]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [Expr] - regular expression
// 
// ==================================================================
void CAatedit5Ctrl::SetRegularExpression(LPCTSTR Expr) 
{
	// TODO: Add your dispatch handler code here
	CString olExpr(Expr);

	omEdit.SetRegularExpression(olExpr);

}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::CheckRegMatch()
// 
// * Description : Performs the pattern matching algo.
// 
// 
// * Author : [cla AAT/ID], Created : [12.02.01 15:24:47]
// 
// * Returns : [BOOL] - Matches or not
// 
// * Function parameters : 
// [Val] - operand string
// 
// ==================================================================
BOOL CAatedit5Ctrl::CheckRegMatch(LPCTSTR Val) 
{
	// TODO: Add your dispatch handler code here
	CString olVal(Val);
	bool olRet;

	olRet = omEdit.CheckRegMatch(olVal);
	if(olRet == true)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetRange()
// 
// * Description : Set the range for double and integers
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:37:08]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [From] - lower boundary
// [To] - upper boundary
// 
// ==================================================================
void CAatedit5Ctrl::SetRange(long From, long To) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetRange(From,To);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetPrecision()
// 
// * Description : Sets the precision for double
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:37:57]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [PrePoint] - Pre point
// [PostPoint] - Post point
// 
// ==================================================================
void CAatedit5Ctrl::SetPrecision(long PrePoint, long PostPoint) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetPrecision(PrePoint,PostPoint);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTextColor()
// 
// * Description : Sets the text color
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:38:48]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [TextColor] - COLORREF
// 
// ==================================================================
void CAatedit5Ctrl::SetTextColor(long TextColor) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTextColor(TextColor);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTextChangedColor()
// 
// * Description : Sets the color when text has changed
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:39:16]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [TextChangedColor] - COLORREF
// 
// ==================================================================
void CAatedit5Ctrl::SetTextChangedColor(long TextChangedColor) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetTextChangedColor(TextChangedColor);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::GetWindowText()
// 
// * Description : Returns the String associated to the Edit control
// 
// 
// * Author : [cla AAT/ID], Created : [12.02.01 15:32:06]
// 
// * Returns : [BSTR] - edit field string
// 
// * Function parameters : 
// 
// ==================================================================
BSTR CAatedit5Ctrl::GetWindowText() 
{
	CString strResult;
	// TODO: Add your dispatch handler code here
	omEdit.GetWindowText(strResult);

	return strResult.AllocSysString();
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetSecState()
// 
// * Description : Sets the security state for BDPS-SEC invisible ... bla bla
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:43:03]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [key] - state
// 
// ==================================================================
void CAatedit5Ctrl::SetSecState(LPCTSTR key) 
{
	// TODO: Add your dispatch handler code here
	/*
	 * Extract the first char and convert it 
	 * from TCHAR to char
	 */
	TCHAR olKey;
	olKey = key[0];
	omEdit.SetSecState(static_cast<char>(olKey));
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::GetTextColor()
// 
// * Description : Retrieves the current text color
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:44:15]
// 
// * Returns : [long] - COLORREF
// 
// * Function parameters : 
// 
// ==================================================================
long CAatedit5Ctrl::GetTextColor() 
{
	// TODO: Add your dispatch handler code here

	return omEdit.GetTextColor();
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTextErrColor()
// 
// * Description : Sets the text error color
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:45:29]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [ErrColor] - COLORREF
// 
// ==================================================================
void CAatedit5Ctrl::SetTextErrColor(long ErrColor) 
{
	// TODO: Add your dispatch handler code here

}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::GetStatus()
// 
// * Description : ???
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:46:32]
// 
// * Returns : [BOOL] - TRUE/FALSE
// 
// * Function parameters : 
// 
// ==================================================================
BOOL CAatedit5Ctrl::GetStatus() 
{
	// TODO: Add your dispatch handler code here

	return omEdit.GetStatus();
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetStatus()
// 
// * Description : ???
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:47:32]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [status] - TRUE/FALSE
// 
// ==================================================================
void CAatedit5Ctrl::SetStatus(BOOL status) 
{
	// TODO: Add your dispatch handler code here
	omEdit.SetStatus(status);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::IsChanged()
// 
// * Description : has the content of the edit control changed
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 11:48:14]
// 
// * Returns : [BOOL] - TRUE/FALSE
// 
// * Function parameters : 
// 
// ==================================================================
BOOL CAatedit5Ctrl::IsChanged() 
{
	// TODO: Add your dispatch handler code here

	return omEdit.IsChanged();
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToString()
// 
// * Description : Sets type to string
// 
// 
// * Author : [cla AAT/ID], Created : [13.02.01 13:41:04]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [Format] - ???
// [MaxLength] - maximal length
// [MinLength] - minimal length
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToString(LPCTSTR Format, long MaxLength, long MinLength) 
{
	// TODO: Add your dispatch handler code here
	CString olStr(Format);

	omEdit.SetTypeToString(olStr,MaxLength,MinLength);
}

void CAatedit5Ctrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	COleControl::OnLButtonDown(nFlags, point);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::WindowProc()
// 
// * Description : Window proc of the control to catch
// *			   WM_USER defined massages sent by CCSEdit
// *			   Taken from Http://www.eed.usv.ro/misc/doc/prog/c/mfc/listview/ctrl_vs_view_undoc.html
// *               Don't worry take my code as an example ...
// 
// 
// * Author : [cla AAT/ID], Created : [15.02.01 15:03:05]
// 
// * Returns : [LRESULT] - TRUE/FALSE
// 
// * Function parameters : 
// [message] - message id
// [wParam] - arbitrary parameter
// [lParam] - arbitrary parameter
// 
// ==================================================================
LRESULT CAatedit5Ctrl::WindowProc(UINT message,WPARAM wParam,LPARAM lParam)
{
	switch(message)
	{
		case WM_AAT_CHANGE_CMD:
			this->FireChange();
			break;
		case WM_AAT_LBUTTONDOWN_CMD:
			this->FireLButtonDown();
			break;
		case WM_AAT_KILLFOCUS_CMD:
			this->FireKillFocus();
			break;
	}

	LRESULT lResult = 0;	
	if (!OnWndMsg(message, wParam, lParam, &lResult))
		lResult = DefWindowProc(message, wParam, lParam);	
	return lResult;			
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTypeToInt()
// 
// * Description : Sets type to int
// 
// 
// * Author : [cla AAT/ID], Created : [21.02.01 15:31:32]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [MaxVal] - Maximum value
// [MinVal] - Minimum value
// 
// ==================================================================
void CAatedit5Ctrl::SetTypeToInt(long MinVal, long MaxVal) 
{
	// TODO: Add your dispatch handler code here

	omEdit.SetTypeToInt(MinVal,MaxVal);
}

// ==================================================================
// 
// FUNCTION :  CAatedit5Ctrl::SetTextLimit()
// 
// * Description : Sets the text limit
// 
// 
// * Author : [cla AAT/ID], Created : [21.02.01 15:51:52]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [MinLen] - Minimum text length
// [MaxLen] - maximum text length
// [EmptyValid] - Is an empty string valid
// 
// ==================================================================
void CAatedit5Ctrl::SetTextLimit(long MinLen, long MaxLen,BOOL EmptyValid) 
{
	// TODO: Add your dispatch handler code here

	omEdit.SetTextLimit(MinLen,MaxLen,EmptyValid);
}