//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by UCom.rc
//
#define IDS_UCOM                        1
#define IDB_UCOM                        1
#define IDS_UCOM_PPG                    2
#define IDS_UCOM_PPG_CAPTION            200
#define IDD_PROPPAGE_UCOM               200
#define IDP_SOCKETS_INIT_FAILED         201

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
