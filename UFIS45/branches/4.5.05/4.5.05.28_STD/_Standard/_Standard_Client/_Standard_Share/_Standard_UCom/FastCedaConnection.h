#if !defined(AFX_FASTCEDACONNECTION_H__5CF58162_A5D0_11D5_8046_00D0B7E2A467__INCLUDED_)
#define AFX_FASTCEDACONNECTION_H__5CF58162_A5D0_11D5_8046_00D0B7E2A467__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FastCedaConnection.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FastCedaConnection command target
#include "blockingsocket.h"

class FastCedaConnection : public CObject
{
// Attributes
public:
	typedef void (*EVENTCALLBACK)(CCmdTarget *pCtrl,long ipPackage);

// Operations
public:
	FastCedaConnection();
	virtual ~FastCedaConnection();

	CString	omAppl,
			omCommand,
			omConnect,
			omFields,
			omHopo,
			omIdentifier,
			omPort,
			omServer,
			omTabext,
			omTable,
			omSepa,
			omUser,
			omWhere,
			omWks;

	bool	bmError;

	CString omErrorText;
	CStringArray	omData;

	//Timeout parameters
	int				imSendTimeout;
	int				imReceiveTimeout;

	//Packet size
	long			lmPacketSize;

	//Packet receive event callback
	EVENTCALLBACK	pfmCallback;
	CCmdTarget*		pomControl;

	//Error simulation
	CString	omSimErr;

	//return 0 for SUCCESS of -1 for Error
	int CedaAction();

	// in case of bmError = true the error text will be returned
	CString GetError();

	// returns the amount of records to enable the user to iterate 
	// through the databuffer an retrieve line by line
	long GetDataCount();

	// returns the record at the specified index
	CString GetDataRecord(long ipIdx);

	//Returns the whole data buffer with "\n" separator
	CString GetDataBuffer();

	//frees all allocated memory
	void ClearDataBuffer();

//********************************
// internally used members
//********************************
private:
	CBlockingSocket	*pomSocket;
	bool			bmIsConnected;
	char			*pcmDataBuffer;
	long			lmCurrentPacketSize;

	int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa);

	//connects to the server in case of empty string 
	// the socket connects to the server specified in omServer
	int ConnectToCeda(CString opConnectionstring = "");

	// Close the established connection
	void ResetConnection();

	//This method is responsible to extract the values behind a keyword
	// in the syntax "{=CMD=}" or "{=HOP=}"
	char *GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
					 char *pcpTextBuff, char *pcpKeyWord, 
					 char *pcpItemEnd, bool bpCopyData);

	// Send Data
	bool SendData();

	// Receive data
	bool ReceiveData();

	// Receive packet data
	bool ReceivePacketData(bool bpFirstPacket,bool& bpCompleted);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FASTCEDACONNECTION_H__5CF58162_A5D0_11D5_8046_00D0B7E2A467__INCLUDED_)
