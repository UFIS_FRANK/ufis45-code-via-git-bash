// UComCtl.cpp : Implementation of the CUComCtrl ActiveX Control class.

#include "stdafx.h"
#include "UCom.h"
#include "UComCtl.h"
#include "UComPpg.h"
#include "VersionInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUComCtrl, COleControl)


int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa)
{
	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}

/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUComCtrl, COleControl)
	//{{AFX_MSG_MAP(CUComCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUComCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUComCtrl)
	DISP_PROPERTY_NOTIFY(CUComCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUComCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUComCtrl, "SQLAccessMethod", m_SQLAccessMethod, OnSQLAccessMethodChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUComCtrl, "SQLConnectionString", m_SQLConnectionString, OnSQLConnectionStringChanged, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "ApplicationName", GetApplicationName, SetApplicationName, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "Port", GetPort, SetPort, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "Hopo", GetHopo, SetHopo, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "ServerName", GetServerName, SetServerName, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "TableExtension", GetTableExtension, SetTableExtension, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "UserName", GetUserName, SetUserName, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "WorkstationName", GetWorkstationName, SetWorkstationName, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "SendTimeoutSeconds", GetSendTimeoutSeconds, SetSendTimeoutSeconds, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "ReceiveTimeoutSeconds", GetReceiveTimeoutSeconds, SetReceiveTimeoutSeconds, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "RecordSeparator", GetRecordSeparator, SetRecordSeparator, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "CedaIdentifier", GetCedaIdentifier, SetCedaIdentifier, VT_BSTR)
	DISP_PROPERTY_EX(CUComCtrl, "PacketSize", GetPacketSize, SetPacketSize, VT_I4)
	DISP_PROPERTY_EX(CUComCtrl, "ErrorSimulation", GetErrorSimulation, SetErrorSimulation, VT_BSTR)
	DISP_FUNCTION(CUComCtrl, "GetLastError", GetLastError, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CUComCtrl, "CedaAction", CedaAction, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUComCtrl, "GetBufferCount", GetBufferCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CUComCtrl, "GetRecord", GetRecord, VT_BSTR, VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUComCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUComCtrl)
	EVENT_CUSTOM("PackageReceived", FirePackageReceived, VTS_I4)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUComCtrl, 1)
	PROPPAGEID(CUComPropPage::guid)
END_PROPPAGEIDS(CUComCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUComCtrl, "UCOM.UComCtrl.1",
	0xf9478336, 0x76ba, 0x11d6, 0x80, 0x67, 0, 0x1, 0x2, 0x22, 0x5, 0xe4)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUComCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUCom =
		{ 0xf9478334, 0x76ba, 0x11d6, { 0x80, 0x67, 0, 0x1, 0x2, 0x22, 0x5, 0xe4 } };
const IID BASED_CODE IID_DUComEvents =
		{ 0xf9478335, 0x76ba, 0x11d6, { 0x80, 0x67, 0, 0x1, 0x2, 0x22, 0x5, 0xe4 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUComOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUComCtrl, IDS_UCOM, _dwUComOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::CUComCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUComCtrl

BOOL CUComCtrl::CUComCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UCOM,
			IDB_UCOM,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUComOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::CUComCtrl - Constructor

CUComCtrl::CUComCtrl()
{
	InitializeIIDs(&IID_DUCom, &IID_DUComEvents);
	GetVersionInfo();

	m_SQLAccessMethod = "CEDA";

	this->omFastCeda.pfmCallback = EventCallback;
	this->omFastCeda.pomControl  = this;

	// TODO: Initialize your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::~CUComCtrl - Destructor

CUComCtrl::~CUComCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::OnDraw - Drawing function

void CUComCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->SetTextColor(RGB(128,0,128));
	pdc->SetBkColor (RGB(255,255,255));
	CRect olRect = rcBounds;
	pdc->DrawText ("UCom", olRect, DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	//pdc->TextOut(rcBounds.left + 2, rcBounds.top + 2, CString("UCom"));// Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::DoPropExchange - Persistence support

void CUComCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl::OnResetState - Reset control to default state

void CUComCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUComCtrl message handlers


BSTR CUComCtrl::GetApplicationName() 
{
	CString strResult;
	strResult = omFastCeda.omAppl;
	return strResult.AllocSysString();
}

void CUComCtrl::SetApplicationName(LPCTSTR lpszNewValue) 
{
	omFastCeda.omAppl = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetPort() 
{
	CString strResult;
	strResult = omFastCeda.omPort;
	return strResult.AllocSysString();
}

void CUComCtrl::SetPort(LPCTSTR lpszNewValue) 
{
	omFastCeda.omPort = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetHopo() 
{
	CString strResult;
	strResult = omFastCeda.omHopo;
	return strResult.AllocSysString();
}

void CUComCtrl::SetHopo(LPCTSTR lpszNewValue) 
{
	omFastCeda.omHopo = lpszNewValue;
	SetModifiedFlag();
}

BSTR CUComCtrl::GetServerName() 
{
	CString strResult;
	strResult = omFastCeda.omServer;
	return strResult.AllocSysString();
}

void CUComCtrl::SetServerName(LPCTSTR lpszNewValue) 
{
	omFastCeda.omServer = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetTableExtension() 
{
	CString strResult;
	strResult = omFastCeda.omTabext;
	return strResult.AllocSysString();
}

void CUComCtrl::SetTableExtension(LPCTSTR lpszNewValue) 
{
	omFastCeda.omTabext = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetUserName() 
{
	CString strResult;
	strResult = omFastCeda.omUser;
	return strResult.AllocSysString();
}

void CUComCtrl::SetUserName(LPCTSTR lpszNewValue) 
{
	omFastCeda.omUser = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetWorkstationName() 
{
	CString strResult;
	strResult = omFastCeda.omWks;

	return strResult.AllocSysString();
}

void CUComCtrl::SetWorkstationName(LPCTSTR lpszNewValue) 
{
	omFastCeda.omWks = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetSendTimeoutSeconds() 
{
	CString strResult;
	strResult.Format ("%d", omFastCeda.imSendTimeout);
	return strResult.AllocSysString();
}

void CUComCtrl::SetSendTimeoutSeconds(LPCTSTR lpszNewValue) 
{
	omFastCeda.imSendTimeout = atoi (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetReceiveTimeoutSeconds() 
{
	CString strResult;
	strResult.Format ("%d", omFastCeda.imReceiveTimeout);
	return strResult.AllocSysString();
}

void CUComCtrl::SetReceiveTimeoutSeconds(LPCTSTR lpszNewValue) 
{
	omFastCeda.imReceiveTimeout = atoi (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetRecordSeparator() 
{
	CString strResult;
	strResult = omFastCeda.omSepa;
	return strResult.AllocSysString();
}

void CUComCtrl::SetRecordSeparator(LPCTSTR lpszNewValue) 
{
	omFastCeda.omSepa = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetCedaIdentifier() 
{
	CString strResult;
	strResult = omFastCeda.omIdentifier;
	return strResult.AllocSysString();
}

void CUComCtrl::SetCedaIdentifier(LPCTSTR lpszNewValue) 
{
	omFastCeda.omIdentifier = CString (lpszNewValue);
	SetModifiedFlag();
}

BSTR CUComCtrl::GetLastError() 
{
	CString strResult;
	strResult = omFastCeda.GetError();
	return strResult.AllocSysString();
}

BOOL CUComCtrl::CedaAction(LPCTSTR Command, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere) 
{
	BOOL blRet;
	omFastCeda.omCommand	= Command;
	omFastCeda.omTable		= DBTable;
	omFastCeda.omFields		= DBFields;
	omFastCeda.omWhere		= DBWhere;

	if(m_SQLAccessMethod == "ODBC")
	{
		CString olCmd = CString(Command);
		if(m_SQLConnectionString == "")
		{
			omFastCeda.omErrorText = "No Connection String!!\nUnable to connect to Database";
			return FALSE;
		}
		omUfisOdbc.omConnectionString = m_SQLConnectionString;
		omUfisOdbc.RemoveInternalData();
		if(olCmd == "RT" || olCmd == "RTA" || olCmd == "Select" || olCmd == "SELECT")
		{
			if(omUfisOdbc.CallDB("Select", CString(DBTable), CString(DBFields), CString(DBWhere), "") != 0)
			{
				blRet = FALSE;
			}
			else
			{
				blRet = TRUE;
			}
		}
	}

	if(m_SQLAccessMethod == "CEDA")
	{
		if (omFastCeda.CedaAction() == 0)
		{
			blRet = TRUE;
		}
		else
		{
			blRet = FALSE;
		}
	}
	return blRet;
}

long CUComCtrl::GetBufferCount() 
{
	long llCount;
	if(m_SQLAccessMethod == "ODBC")
	{
		llCount = omUfisOdbc.omData.GetSize();
	}
	if(m_SQLAccessMethod == "CEDA")
	{
		llCount = omFastCeda.GetDataCount();
	}
	return llCount;
}

BSTR CUComCtrl::GetRecord(long RecordNo) 
{
	CString strResult;
	if(m_SQLAccessMethod == "ODBC")
	{
		if (RecordNo < omUfisOdbc.omData.GetSize())
		{
			strResult = omUfisOdbc.omData[RecordNo];
		}
	}
	if(m_SQLAccessMethod == "CEDA")
	{
		if (RecordNo < omFastCeda.GetDataCount())
		{
			strResult = omFastCeda.omData[RecordNo];
		}
	}
	return strResult.AllocSysString();
}

void CUComCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUComCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUComCtrl::GetVersionInfo()
{
	HMODULE hModule = AfxGetInstanceHandle();
	VersionInfo olVersionInfo;
	if (VersionInfo::GetVersionInfo(hModule,olVersionInfo))
	{
		m_version = olVersionInfo.omFileVersion;
	}

	m_buildDate = CString(__DATE__) + " - " + CString(__TIME__);

}

void CUComCtrl::OnSQLAccessMethodChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUComCtrl::OnSQLConnectionStringChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

long CUComCtrl::GetPacketSize() 
{
	// TODO: Add your property handler here
	return this->omFastCeda.lmPacketSize;
}

void CUComCtrl::SetPacketSize(long nNewValue) 
{
	// TODO: Add your property handler here
	this->omFastCeda.lmPacketSize = nNewValue;

	SetModifiedFlag();
}

void CUComCtrl::EventCallback(CCmdTarget *popControl,long ipPackage)
{
	CUComCtrl *polControl = (CUComCtrl *)popControl;
	if (polControl != NULL)
	{
		polControl->FirePackageReceived(ipPackage);
	}
}

BSTR CUComCtrl::GetErrorSimulation() 
{
	// TODO: Add your property handler here
	return this->omFastCeda.omSimErr.AllocSysString();
}

void CUComCtrl::SetErrorSimulation(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omFastCeda.omSimErr = lpszNewValue;

	SetModifiedFlag();
}
