﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Utilities;
using System.Windows.Input;
using Ufis.MVVM.Command;
using Ufis.Entities;
using Ufis.Data;

namespace Ufis.Flight.Filter
{
    /// <summary>
    /// Represents the view model for flight filter window.
    /// </summary>
    public class FlightFilterViewModel : PanelWorkspaceViewModel
    {
        protected override string TargetName { get { return null; } }

        private FlightFilter _filter;
        private ICommand _applyFilterCommand;
        private bool _canFilterByRegistrationNumber = true;
        private bool _canFilterByFlightTypes = true;

        private FlightType _flightTypes = FlightType.Unspecified;
        private IList<EntCodeName> _visibleFlightTypes = FlightTypesBuilder.GetDefaultFlightTypeList();
        
        private int _relativeRangeSmallStep = 1;
        private int _relativeRangeLargeStep = 2;
        private int _minimumRelativeRange = -24;
        private int _maximumRelativeRange = 24;
        private int _relativeRangeTickFrequency = 1;

        #region Event Handlers
        
        /// <summary>
        /// Raised when a filter is requested to be applied.
        /// </summary>
        public event EventHandler RequestFilter;

        private void OnRequestFilter(EventArgs e)
        {
            EventHandler handler = RequestFilter;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Gets or set the Ufis.Flight.Filter.FlightFilter object for this view model.
        /// </summary>
        public FlightFilter Filter
        {
            get
            {
                if (_filter == null)
                    _filter = CreateDefaultFlightFilter();

                if (_filter.DateFilterTypeCollection.CurrentItem == null)
                    _filter.DateFilterTypeCollection.MoveCurrentTo(_filter.DateFilterType);
                
                return _filter;
            }
            set
            {
                if (_filter == value)
                    return;

                _filter = value;
                OnPropertyChanged("Filter");
            }
        }

        /// <summary>
        /// Gets or sets whether user can filter by registration number.
        /// </summary>
        public bool CanFilterByRegistrationNumber
        {
            get
            {
                return _canFilterByRegistrationNumber;
            }
            set
            {
                if (_canFilterByRegistrationNumber == value)
                    return;

                _canFilterByRegistrationNumber = value;
                OnPropertyChanged("CanFilterByRegistrationNumber");
            }
        }

        /// <summary>
        /// Gets or sets whether user can filter by flight operation type.
        /// </summary>
        public bool CanFilterByFlightTypes
        {
            get 
            { 
                return _canFilterByFlightTypes; 
            }
            set
            {
                if (_canFilterByFlightTypes == value)
                    return;

                _canFilterByFlightTypes = value;
                OnPropertyChanged("CanFilterByFlightTypes");
            }
        }

        /// <summary>
        /// Gets or sets the small step of range for relative date.
        /// </summary>
        public int RelativeRangeSmallStep
        {
            get
            {
                return _relativeRangeSmallStep;
            }
            set
            {
                if (_relativeRangeSmallStep == value)
                    return;

                _relativeRangeSmallStep = value;
                OnPropertyChanged("RelativeRangeSmallStep");
            }
        }

        /// <summary>
        /// Gets or sets the large step of range for relative date.
        /// </summary>
        public int RelativeRangeLargeStep
        {
            get
            {
                return _relativeRangeLargeStep;
            }
            set
            {
                if (_relativeRangeLargeStep == value)
                    return;

                _relativeRangeLargeStep = value;
                OnPropertyChanged("RelativeRangeLargeStep");
            }
        }

        /// <summary>
        /// Gets or sets the minimum range for relative date.
        /// </summary>
        public int MinimumRelativeRange
        {
            get
            {
                return _minimumRelativeRange;
            }
            set
            {
                if (_minimumRelativeRange == value)
                    return;

                _minimumRelativeRange = value;
                OnPropertyChanged("MinimumRelativeRange");
            }
        }

        /// <summary>
        /// Gets or sets the maximum range for relative date.
        /// </summary>
        public int MaximumRelativeRange
        {
            get
            {
                return _maximumRelativeRange;
            }
            set
            {
                if (_maximumRelativeRange == value)
                    return;

                _maximumRelativeRange = value;
                OnPropertyChanged("MaximumRelativeRange");
            }
        }

        /// <summary>
        /// Gets or sets the tick frequency for relative date.
        /// </summary>
        public int RelativeRangeTickFrequency
        {
            get
            {
                return _relativeRangeTickFrequency;
            }
            set
            {
                if (_relativeRangeTickFrequency == value)
                    return;

                _relativeRangeTickFrequency = value;
                OnPropertyChanged("RelativeRangeTickFrequency");
            }
        }

        /// <summary>
        /// Gets or sets the Ufis.Data.FlightTypes available for this view model.
        /// </summary>
        public FlightType FlightTypes
        {
            get
            {
                return _flightTypes;
            }
            set
            {
                if (_flightTypes == value)
                    return;

                _flightTypes = value;
                OnPropertyChanged("FlightTypes");

                VisibleFlightTypes = FlightTypesBuilder.GetFlightTypeList(_flightTypes);
            }
        }

        /// <summary>
        /// Gets the list of visible flight operational types.
        /// </summary>
        public IList<EntCodeName> VisibleFlightTypes
        {
            get
            {
                return _visibleFlightTypes;
            }
            private set
            {
                if (_visibleFlightTypes == value)
                    return;

                _visibleFlightTypes = value;
                OnPropertyChanged("VisibleFlightTypes");
            }
        }

        /// <summary>
        /// Returns a command that applies the flight filter.
        /// </summary>
        public ICommand ApplyFilterCommand
        {
            get
            {
                if (_applyFilterCommand == null)
                {
                    _applyFilterCommand = new RelayCommand(ApplyFilter);
                }
                return _applyFilterCommand;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Applies the filter.
        /// </summary>
        /// <param name="parameter"></param>
        public void ApplyFilter(object parameter)
        {
            if (Filter.DateFilterType.HasValue && Filter.DateFilterType.Value == FlightDateFilterType.RelativeDate)
            {
                DateTime dtmNow = DateTime.Now;

                Filter.AbsoluteStartDate = dtmNow.AddHours(Filter.RelativeStartRange);
                Filter.AbsoluteEndDate = dtmNow.AddHours(Filter.RelativeEndRange);
            }
            
            OnRequestFilter(new EventArgs());

            CloseCommand.Execute(null);
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes the new instance of Ufis.Flight.Filter.FlightFilterViewModel object.
        /// </summary>
        /// <param name="targetName">The name of DevExpress docking layout group object which will host this workspace.</param>
        public FlightFilterViewModel(string targetName) : this(targetName, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Flight.Filter.FlightFilterViewModel object.
        /// </summary>
        /// <param name="targetName">The name of DevExpress docking layout group object which will host this workspace.</param>
        /// <param name="flightFilter">The Ufis.Flight.Filter.FlightFilter object.</param>
        public FlightFilterViewModel(string targetName, FlightFilter flightFilter)
            : base(targetName)
        {
            DisplayName = "Filter Flights";
            Glyph = ImageGetter.GetImage("filter_16x16.png");

            if (flightFilter == null)
                flightFilter = CreateDefaultFlightFilter();

            _filter = flightFilter;
        }

        #endregion

        #region Private Helpers

        private FlightFilter CreateDefaultFlightFilter()
        {
            FlightFilter flightFilter = new FlightFilter()
                {
                    DateFilterType = FlightDateFilterType.AbsoluteDate,
                    AbsoluteStartDate = DateTime.Today,
                    AbsoluteEndDate = DateTime.Today.AddDays(1).AddSeconds(-1)
                };
            return flightFilter;
        }

        #endregion
    }
}
