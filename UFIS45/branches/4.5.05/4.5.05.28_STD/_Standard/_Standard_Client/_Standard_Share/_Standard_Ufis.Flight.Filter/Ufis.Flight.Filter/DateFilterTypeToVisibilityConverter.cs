﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Windows;

namespace Ufis.Flight.Filter
{
    /// <summary>
    /// Converts Ufis.Flight.Filter.FlightDateFilterType object to System.Windows.Visibility.Visible 
    /// if it's name is the same as the conversion parameter.
    /// Otherwise converts it to System.Windows.Visibility.Collapsed.
    /// </summary>
    [ValueConversion(typeof(FlightDateFilterType), typeof(Visibility))]
    public class DateFilterTypeToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility result = Visibility.Collapsed;

            if (value is FlightDateFilterType && parameter != null)
            {
                FlightDateFilterType dateFilterType = (FlightDateFilterType)value;
                if (Enum.GetName(typeof(FlightDateFilterType), dateFilterType) == parameter.ToString())
                    result = Visibility.Visible;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
