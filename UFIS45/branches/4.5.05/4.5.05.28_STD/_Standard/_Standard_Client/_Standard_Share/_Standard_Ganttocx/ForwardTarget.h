//
// Forwarding class for drag & drop operations
// Fritz Onion, February 1999
//

template <class T>
class CForwardingTarget : public COleDropTarget
{
private:
		T* m_pForward;

public:
		CForwardingTarget() : m_pForward(NULL){} 

		BOOL Register(T* pForward)
		{ m_pForward = pForward;  return COleDropTarget::Register(pForward); }
		virtual DROPEFFECT OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject,
						DWORD dwKeyState, CPoint point)
		{ return m_pForward->OnDragEnter(pWnd, pDataObject, dwKeyState, point); }
		virtual DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject,
						DWORD dwKeyState, CPoint point)
		{ return m_pForward->OnDragOver(pWnd, pDataObject, dwKeyState, point); }
		virtual BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject,
               DROPEFFECT dropEffect, CPoint point)
		{ return m_pForward->OnDrop(pWnd, pDataObject, dropEffect, point); }
		virtual void OnDragLeave(CWnd* pWnd)
		{ m_pForward->OnDragLeave(pWnd); }
};