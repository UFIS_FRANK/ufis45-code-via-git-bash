// UGanttPpg.cpp : Implementation of the CUGanttPropPage property page class.

#include <stdafx.h>
#include <UGantt.h>
#include <UGanttPpg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUGanttPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUGanttPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUGanttPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUGanttPropPage, "UGANTT.UGanttPropPage.1",
	0x6782e139, 0x3223, 0x11d4, 0x99, 0x6a, 0, 0, 0x86, 0x3d, 0xe9, 0x5c)


/////////////////////////////////////////////////////////////////////////////
// CUGanttPropPage::CUGanttPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUGanttPropPage

BOOL CUGanttPropPage::CUGanttPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UGANTT_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttPropPage::CUGanttPropPage - Constructor

CUGanttPropPage::CUGanttPropPage() :
	COlePropertyPage(IDD, IDS_UGANTT_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUGanttPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT

	SetHelpInfo(_T("Names to appear in the control"), _T("UGANTT.HLP"), 0);
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttPropPage::DoDataExchange - Moves data between page and properties

void CUGanttPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUGanttPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttPropPage message handlers
