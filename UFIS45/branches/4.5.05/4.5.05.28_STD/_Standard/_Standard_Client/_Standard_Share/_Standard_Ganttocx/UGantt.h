#if !defined(AFX_UGANTT_H__6782E13E_3223_11D4_996A_0000863DE95C__INCLUDED_)
#define AFX_UGANTT_H__6782E13E_3223_11D4_996A_0000863DE95C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UGantt.h : main header file for UGANTT.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include <resource.h>       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUGanttApp : See UGantt.cpp for implementation.

class CUGanttApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UGANTT_H__6782E13E_3223_11D4_996A_0000863DE95C__INCLUDED)
