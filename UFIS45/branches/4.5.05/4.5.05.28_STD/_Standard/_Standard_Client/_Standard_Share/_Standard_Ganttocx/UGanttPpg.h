#if !defined(AFX_UGANTTPPG_H__6782E148_3223_11D4_996A_0000863DE95C__INCLUDED_)
#define AFX_UGANTTPPG_H__6782E148_3223_11D4_996A_0000863DE95C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UGanttPpg.h : Declaration of the CUGanttPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUGanttPropPage : See UGanttPpg.cpp.cpp for implementation.

class CUGanttPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUGanttPropPage)
	DECLARE_OLECREATE_EX(CUGanttPropPage)

// Constructor
public:
	CUGanttPropPage();

// Dialog Data
	//{{AFX_DATA(CUGanttPropPage)
	enum { IDD = IDD_PROPPAGE_UGANTT };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUGanttPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UGANTTPPG_H__6782E148_3223_11D4_996A_0000863DE95C__INCLUDED)
