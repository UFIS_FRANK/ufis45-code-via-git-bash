#if !defined(AFX_BC_SIMPLE_H__89E22A24_8014_4E6A_9AE2_985470C94212__INCLUDED_)
#define AFX_BC_SIMPLE_H__89E22A24_8014_4E6A_9AE2_985470C94212__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BC_Simple.h : main header file for BC_SIMPLE.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleApp : See BC_Simple.cpp for implementation.

class CBC_SimpleApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BC_SIMPLE_H__89E22A24_8014_4E6A_9AE2_985470C94212__INCLUDED)
