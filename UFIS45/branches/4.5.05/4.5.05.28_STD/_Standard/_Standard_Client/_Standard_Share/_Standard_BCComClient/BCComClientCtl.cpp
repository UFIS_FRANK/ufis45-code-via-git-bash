// BCComClientCtl.cpp : Implementation of the CBCComClientCtrl ActiveX Control class.

#include "stdafx.h"
#include "BCComClient.h"
#include "BCComClientCtl.h"
#include "BCComClientPpg.h"
#include "BcComServerTarget.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString IDP_OLE_INIT_FAILED = "OLE initialization failed.  Make sure that the OLE libraries are the correct version.";


CBCComClientCtrl *pomThis;

IMPLEMENT_DYNCREATE(CBCComClientCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBCComClientCtrl, COleControl)
	//{{AFX_MSG_MAP(CBCComClientCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CBCComClientCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CBCComClientCtrl)
	DISP_PROPERTY_NOTIFY(CBCComClientCtrl, "AmIConnected", m_amIConnected, OnAmIConnectedChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CBCComClientCtrl, "BCConnectionType", m_bCConnectionType, OnBCConnectionTypeChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CBCComClientCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CBCComClientCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_FUNCTION(CBCComClientCtrl, "ConnectToBcComServer", ConnectToBcComServer, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CBCComClientCtrl, "DisconnectFromBcComServer", DisconnectFromBcComServer, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CBCComClientCtrl, "SetFilter", SetFilter, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BOOL VTS_BSTR)
	DISP_FUNCTION(CBCComClientCtrl, "TransmitFilter", TransmitFilter, VT_EMPTY, VTS_NONE)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CBCComClientCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()

/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CBCComClientCtrl, COleControl)
	//{{AFX_EVENT_MAP(CBCComClientCtrl)
	EVENT_CUSTOM("OnBc", FireOnBc, VTS_I4  VTS_I4  VTS_I4  VTS_BSTR)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CBCComClientCtrl, 1)
	PROPPAGEID(CBCComClientPropPage::guid)
END_PROPPAGEIDS(CBCComClientCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBCComClientCtrl, "BCCOMCLIENT.BCComClientCtrl.1",
	0xea6de32c, 0xd8c1, 0x474d, 0x96, 0xdd, 0x90, 0xad, 0xdf, 0xa6, 0x87, 0x67)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CBCComClientCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DBCComClient =
		{ 0xa605f53d, 0x67ad, 0x4b35, { 0xb9, 0x73, 0xc7, 0x9, 0x5d, 0x67, 0x1, 0x28 } };
const IID BASED_CODE IID_DBCComClientEvents =
		{ 0xb3d898c0, 0xdb03, 0x47ef, { 0x8e, 0x6d, 0xca, 0xd0, 0x7, 0xa3, 0x72, 0x27 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwBCComClientOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CBCComClientCtrl, IDS_BCCOMCLIENT, _dwBCComClientOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::CBCComClientCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CBCComClientCtrl

BOOL CBCComClientCtrl::CBCComClientCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_BCCOMCLIENT,
			IDB_BCCOMCLIENT,
			afxRegInsertable | afxRegApartmentThreading,
			_dwBCComClientOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::CBCComClientCtrl - Constructor

CBCComClientCtrl::CBCComClientCtrl()
{
	pomThis = this;

	InitializeIIDs(&IID_DBCComClient, &IID_DBCComClientEvents);

	bmIsConnected = false;
	m_amIConnected = FALSE;
	bmTimerIsSet = false;

	//Read CEDA.ini to identify which Connection shall be used
	// BCCONNECTIONTYPE=UDP		==> BCProxy will be used (this is the default)
	// BCCONNECTIONTYPE=TCPIP	==> BCComServer will be used
	m_bCConnectionType = "TCPIP";

	char pclConfigPath[256];
	char pclConntype[100];
    if (getenv("CEDA") == NULL)
	{
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
    else
	{
        strcpy(pclConfigPath, getenv("CEDA"));
	}
    GetPrivateProfileString("GLOBAL", "BCCONNECTIONTYPE", "TCPIP", pclConntype, sizeof pclConntype, pclConfigPath);
	m_bCConnectionType = CString(pclConntype);
		
	m_version = "4.5.0.1";
	m_buildDate = CString(__DATE__) + " - " + CString(__TIME__);

	pomBcComServerTarget = 	new CBcComServerTarget(this);
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::~CBCComClientCtrl - Destructor

CBCComClientCtrl::~CBCComClientCtrl()
{
	DisconnectFromBcComServer();
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::OnDraw - Drawing function

void CBCComClientCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	CBrush olBlackBrush(COLORREF(RGB(0,0,0)));
	int ilH = rcBounds.bottom - rcBounds.top;
	CString olS;
	olS = "BCComClient";
	CSize ilTH = pdc->GetTextExtent(olS);
	ilH = (int)(ilH/2);
	ilH -= (int)(ilTH.cy/2);
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));
	pdc->FrameRect(rcBounds, &olBlackBrush);
	pdc->SetTextColor(COLORREF(RGB(0,0,255)));
	pdc->SetBkMode(TRANSPARENT);
	pdc->TextOut(0, ilH, olS.GetBuffer(0));
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::DoPropExchange - Persistence support

void CBCComClientCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::OnResetState - Reset control to default state

void CBCComClientCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl::AboutBox - Display an "About" box to the user

void CBCComClientCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_BCCOMCLIENT);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl message handlers

void CBCComClientCtrl::OnAmIConnectedChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}


BOOL CBCComClientCtrl::ConnectToBcComServer() 
{
	return this->pomBcComServerTarget->ConnectToBcComServer();
}

BOOL CBCComClientCtrl::DisconnectFromBcComServer() 
{
	return this->pomBcComServerTarget->DisconnectFromBcComServer();
}

void CBCComClientCtrl::SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application) 
{
	// TODO: Add your dispatch handler code here
	this->pomBcComServerTarget->SetFilter(strTable, strCommand, bOwnBC, application);
}

void CBCComClientCtrl::TransmitFilter() 
{
	this->pomBcComServerTarget->TransmitFilter();
}

void CBCComClientCtrl::OnBCConnectionTypeChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}


void CBCComClientCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CBCComClientCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

