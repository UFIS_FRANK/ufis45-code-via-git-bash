#if !defined(AFX_BCCOMCLIENTCTL_H__9C490231_62D7_44F5_B141_C834C28A92FF__INCLUDED_)
#define AFX_BCCOMCLIENTCTL_H__9C490231_62D7_44F5_B141_C834C28A92FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BCComClientCtl.h : Declaration of the CBCComClientCtrl ActiveX Control class.
class CBcComServerTarget;

/////////////////////////////////////////////////////////////////////////////
// CBCComClientCtrl : See BCComClientCtl.cpp for implementation.

class CBCComClientCtrl : public COleControl
{
	DECLARE_DYNCREATE(CBCComClientCtrl)

// Constructor
public:
	CBCComClientCtrl();

	bool bmIsConnected;
	bool bmTimerIsSet;
	CStringArray omSpooler;




// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBCComClientCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CBCComClientCtrl();

	DECLARE_OLECREATE_EX(CBCComClientCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CBCComClientCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CBCComClientCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CBCComClientCtrl)		// Type name and misc status


// Message maps
	//{{AFX_MSG(CBCComClientCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CBCComClientCtrl)
	BOOL m_amIConnected;
	afx_msg void OnAmIConnectedChanged();
	CString m_bCConnectionType;
	afx_msg void OnBCConnectionTypeChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	afx_msg BOOL ConnectToBcComServer();
	afx_msg BOOL DisconnectFromBcComServer();
	afx_msg void SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application);
	afx_msg void TransmitFilter();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CBCComClientCtrl)
	void FireOnBc(long dataSize, long currentPackage, long totalPackages, LPCTSTR packageData)
		{FireEvent(eventidOnBc,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_BSTR), dataSize, currentPackage, totalPackages, packageData);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CBCComClientCtrl)
	dispidAmIConnected = 1L,
	dispidBCConnectionType = 2L,
	dispidVersion = 3L,
	dispidBuildDate = 4L,
	dispidConnectToBcComServer = 5L,
	dispidDisconnectFromBcComServer = 6L,
	dispidSetFilter = 7L,
	dispidTransmitFilter = 8L,
	eventidOnBc = 1L,
	//}}AFX_DISP_ID
	};
private:
	CBcComServerTarget	*pomBcComServerTarget;
	friend class CBcComServerTarget;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCCOMCLIENTCTL_H__9C490231_62D7_44F5_B141_C834C28A92FF__INCLUDED)
