VERSION 5.00
Begin VB.UserControl AATLoginControl 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   CanGetFocus     =   0   'False
   ClientHeight    =   1050
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1080
   InvisibleAtRuntime=   -1  'True
   ScaleHeight     =   1050
   ScaleWidth      =   1080
   ToolboxBitmap   =   "AATLoginControl.ctx":0000
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "ULogin Control"
      BeginProperty Font 
         Name            =   "Modern"
         Size            =   12
         Charset         =   255
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   585
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   840
   End
End
Attribute VB_Name = "AATLoginControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Default Property Values:
Private Const m_def_Version = ""
Private Const m_def_BuildDate = "N/A"
Private Const m_def_UserNameLCase = False
Private Const m_def_InfoCaption = "Info about this application"
Private Const m_def_InfoAAT = "ABB Airport Technologies GmbH / Information Systems"
Private Const m_def_InfoUfisVersion = "UFIS Version 4.5"
Private Const m_def_InfoAppVersion = "LoginCtrl 01.01.20xx / 4.5.x.x"
Private Const m_def_InfoCopyright = "Copyright 20xx AAT/I"
Private Const m_def_InfoButtonVisible = 0
Private Const m_def_LoginAttempts = 3
Private Const m_def_RegisterApplicationString = ""
Private Const m_def_ApplicationName = ""
Private Const m_def_VersionString = ""

'Property Variables:
Private m_Version As String
Private m_BuildDate As String
Private m_UserNameLCase As Boolean
Private m_InfoAppVersion As String
Private m_UfisComCtrl As Object
Private m_InfoCaption As String
Private m_InfoAAT As String
Private m_InfoUfisVersion As String
Private m_InfoCopyright As String
Private m_InfoButtonVisible As Boolean
Private m_LoginAttempts As Integer
Private m_RegisterApplicationString As String
Private m_ApplicationName As String
Private m_VersionString As String

'RRO: WARNING! UNCOMMENT THE FOLLOWING COMMENTED LINES ONLY FOR DEBUGGING (STANDALONE)!
'Private Sub UserControl_Initialize()
'    'ShowLoginDialog
'    MsgBox "UserControl_Initialize"
'End Sub
'Private Sub UserControl_Terminate()
'    MsgBox "UserControl_Terminate"
'End Sub

'Initialize Properties for User Control
Private Sub UserControl_InitProperties()
    m_RegisterApplicationString = m_def_RegisterApplicationString
    m_ApplicationName = m_def_ApplicationName
    m_LoginAttempts = m_def_LoginAttempts
    m_VersionString = m_def_VersionString
    m_InfoButtonVisible = m_def_InfoButtonVisible
    m_InfoUfisVersion = m_def_InfoUfisVersion
    m_InfoAppVersion = m_def_InfoAppVersion
    m_InfoCopyright = m_def_InfoCopyright
    m_InfoAAT = m_def_InfoAAT
    m_InfoCaption = m_def_InfoCaption

    Globals.ApplicationName = m_ApplicationName
    Globals.ApplicationRegisterString = m_RegisterApplicationString
    Globals.ogLoginAttempts = m_LoginAttempts
    Globals.gVersionString = m_VersionString
    Globals.Info_ShowButton = m_InfoButtonVisible
    Globals.ogUfisVersion = m_InfoUfisVersion
    Globals.ogApplication = m_InfoAppVersion
    Globals.ogCopyright = m_InfoCopyright
    Globals.ogAAT = m_InfoAAT
    Globals.ogInfoCaption = m_InfoCaption
    m_UserNameLCase = m_def_UserNameLCase
    m_Version = m_def_Version
    m_BuildDate = m_def_BuildDate
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    m_RegisterApplicationString = PropBag.ReadProperty("RegisterApplicationString", m_def_RegisterApplicationString)
    m_ApplicationName = PropBag.ReadProperty("ApplicationName", m_def_ApplicationName)
    m_LoginAttempts = PropBag.ReadProperty("LoginAttempts", m_def_LoginAttempts)
    m_VersionString = PropBag.ReadProperty("VersionString", m_def_VersionString)
    m_InfoButtonVisible = PropBag.ReadProperty("InfoButtonVisible", m_def_InfoButtonVisible)
    m_InfoUfisVersion = PropBag.ReadProperty("InfoUfisVersion", m_def_InfoUfisVersion)
    m_InfoAppVersion = PropBag.ReadProperty("InfoAppVersion", m_def_InfoAppVersion)
    m_InfoCopyright = PropBag.ReadProperty("InfoCopyright", m_def_InfoCopyright)
    m_InfoAAT = PropBag.ReadProperty("InfoAAT", m_def_InfoAAT)
    m_InfoCaption = PropBag.ReadProperty("InfoCaption", m_def_InfoCaption)

    Globals.ApplicationName = m_ApplicationName
    Globals.ApplicationRegisterString = m_RegisterApplicationString
    Globals.ogLoginAttempts = m_LoginAttempts
    Globals.gVersionString = m_VersionString
    Globals.Info_ShowButton = m_InfoButtonVisible
    Globals.ogUfisVersion = m_InfoUfisVersion
    Globals.ogApplication = m_InfoAppVersion
    Globals.ogCopyright = m_InfoCopyright
    Globals.ogAAT = m_InfoAAT
    Globals.ogInfoCaption = m_InfoCaption

    Set m_UfisComCtrl = PropBag.ReadProperty("UfisComCtrl", Nothing)
    m_UserNameLCase = PropBag.ReadProperty("UserNameLCase", m_def_UserNameLCase)
    m_Version = PropBag.ReadProperty("Version", m_def_Version)
    m_BuildDate = PropBag.ReadProperty("BuildDate", m_def_BuildDate)
End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("RegisterApplicationString", m_RegisterApplicationString, m_def_RegisterApplicationString)
    Call PropBag.WriteProperty("ApplicationName", m_ApplicationName, m_def_ApplicationName)
    Call PropBag.WriteProperty("LoginAttempts", m_LoginAttempts, m_def_LoginAttempts)
    Call PropBag.WriteProperty("VersionString", m_VersionString, m_def_VersionString)
    Call PropBag.WriteProperty("InfoButtonVisible", m_InfoButtonVisible, m_def_InfoButtonVisible)
    Call PropBag.WriteProperty("InfoUfisVersion", m_InfoUfisVersion, m_def_InfoUfisVersion)
    Call PropBag.WriteProperty("InfoAppVersion", m_InfoAppVersion, m_def_InfoAppVersion)
    Call PropBag.WriteProperty("InfoCopyright", m_InfoCopyright, m_def_InfoCopyright)
    Call PropBag.WriteProperty("InfoAAT", m_InfoAAT, m_def_InfoAAT)
    Call PropBag.WriteProperty("InfoCaption", m_InfoCaption, m_def_InfoCaption)
    Call PropBag.WriteProperty("UfisComCtrl", m_UfisComCtrl, Nothing)
    Call PropBag.WriteProperty("UserNameLCase", m_UserNameLCase, m_def_UserNameLCase)
    Call PropBag.WriteProperty("Version", m_Version, m_def_Version)
    Call PropBag.WriteProperty("BuildDate", m_BuildDate, m_def_BuildDate)
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13
Public Function ShowLoginDialog() As String
    Dim ilLoginAttempts As Integer

    If TypeName(Globals.UfisComControl) = "UfisCom" Then

        If m_LoginAttempts > 0 Then
            ilLoginAttempts = m_LoginAttempts
        Else
            ilLoginAttempts = m_def_LoginAttempts
        End If

        While Globals.strLoginResult <> "OK" And Globals.strLoginResult <> "CANCEL" And ilLoginAttempts > 0
            frmLogin.Show vbModal
            ilLoginAttempts = ilLoginAttempts - 1
        Wend
    Else
        MsgBox "There's no UfisCom-Control connected to the 'UfisComCtrl'-Property." & vbCrLf & "Use something like 'Set AATLoginControl1.UfisComCtrl = UfisCom1'."
        Globals.strLoginResult = "ERROR"
    End If

    ShowLoginDialog = Globals.strLoginResult
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13
Public Function DoLoginSilentMode(ByVal strUserName As String, ByVal strUserPassword As String) As String
    On Error GoTo ErrHdl
    Dim strTable As String
    Dim strFieldlist As String
    Dim strWhere As String
    Dim strData As String
    Dim strWkst As String

    Dim llRecords As Long
    Dim i As Long

    Dim tmpStr As String
    Dim strItem As String
    Dim strKey As String

    If TypeName(Globals.UfisComControl) = "UfisCom" Then

        ' set some important variables
        If Globals.ApplicationName = "" Then
            Globals.ApplicationName = "AbsPlan"
        End If
        Globals.strLoginResult = ""
        Globals.TableExtension = frmLogin.GetIniEntry("", "GLOBAL", Globals.ApplicationName, "TABLEEXTENSION", "TAB")
        Globals.HomeAirport = frmLogin.GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOMEAIRPORT", "")
        Globals.HostType = frmLogin.GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOSTTYPE", "UNKNOWN")
        Globals.HostName = frmLogin.GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOSTNAME", "")
        Globals.UserName = strUserName
        Globals.UserPassword = strUserPassword

        frmLogin.ConnectToCeda

        strTable = Globals.HomeAirport
        strFieldlist = "USID,PASS,APPL,WKST"
        strWhere = ""
        strWkst = Globals.UfisComControl.GetWorkstationName()
        strData = Globals.UserName + "," + Globals.UserPassword + "," + Globals.ApplicationName + "," + strWkst

        ' Read from ceda
        Globals.UfisComControl.Twe = Globals.HomeAirport + "," + Globals.TableExtension + "," + Globals.ApplicationName
        If Globals.UfisComControl.CallServer("GPR", strTable, strFieldlist, strData, strWhere, "360") = 0 Then
            If Globals.UfisComControl.GetBufferLine(0) = "[PRV]" Then

                ' delete all entries in the lookup-collection
                For i = 1 To Globals.colStatus.Count
                   Globals.colStatus.Remove 1
                Next i

                ' add the entries to the collection
                llRecords = Globals.UfisComControl.GetBufferCount
                For i = 1 To (llRecords - 1)
                    tmpStr = Globals.UfisComControl.GetBufferLine(i)
                    strKey = Trim(frmLogin.GetItem(tmpStr, 2, ","))
                    strItem = Trim(frmLogin.GetItem(tmpStr, 3, ","))
                    If DoesKeyExist(Globals.colStatus, strKey) = False Then
                        Globals.colStatus.Add Item:=strItem, Key:=strKey
                    End If
                Next i
                DoLoginSilentMode = "OK"
            Else
                DoLoginSilentMode = Globals.UfisComControl.LastErrorMessage
            End If
        Else
            DoLoginSilentMode = "ERROR"
        End If
    Else
        MsgBox "There's no UfisCom-Control connected to the 'UfisComCtrl'-Property." & vbCrLf & "Use something like 'Set AATLoginControl1.UfisComCtrl = UfisCom1'."
        DoLoginSilentMode = "ERROR"
    End If

    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "AATLoginControl.GetStat", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "AATLoginControl.GetStat", Err
    Err.Clear
    Resume Next
End Function


'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,
Public Property Get RegisterApplicationString() As String
    RegisterApplicationString = Globals.ApplicationRegisterString
End Property

Public Property Let RegisterApplicationString(ByVal New_RegisterApplicationString As String)
    m_RegisterApplicationString = New_RegisterApplicationString
    Globals.ApplicationRegisterString = New_RegisterApplicationString
    PropertyChanged "RegisterApplicationString"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,
Public Property Get ApplicationName() As String
Attribute ApplicationName.VB_Description = "sets the name of the application, where the login control gets its data from the ceda.ini"
    ApplicationName = Globals.ApplicationName
End Property

Public Property Let ApplicationName(ByVal New_ApplicationName As String)
    m_ApplicationName = New_ApplicationName
    Globals.ApplicationName = New_ApplicationName
    PropertyChanged "ApplicationName"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=7,0,0,3
Public Property Get LoginAttempts() As Integer
    LoginAttempts = Globals.ogLoginAttempts 'm_LoginAttempts
End Property

Public Property Let LoginAttempts(ByVal New_LoginAttempts As Integer)
    m_LoginAttempts = New_LoginAttempts
    Globals.ogLoginAttempts = New_LoginAttempts
    PropertyChanged "LoginAttempts"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,
Public Property Get VersionString() As String
    VersionString = Globals.gVersionString
End Property

Public Property Let VersionString(ByVal New_VersionString As String)
    m_VersionString = New_VersionString
    Globals.gVersionString = New_VersionString
    PropertyChanged "VersionString"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13
Public Function GetPrivileges(strItem As String) As String
Attribute GetPrivileges.VB_Description = "Delivers the user's right ('0', '1',...) of the item you are asking for."
    GetPrivileges = GetStat(strItem)
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13
Public Function GetUserName() As String
Attribute GetUserName.VB_Description = "Delivers the name of the actual user"
    GetUserName = Globals.UserName
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13
Public Function GetUserPassword() As String
    GetUserPassword = Globals.UserPassword
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=0,0,0,0
Public Property Get InfoButtonVisible() As Boolean
    InfoButtonVisible = Globals.Info_ShowButton
End Property

Public Property Let InfoButtonVisible(ByVal New_InfoButtonVisible As Boolean)
    m_InfoButtonVisible = New_InfoButtonVisible
    Globals.Info_ShowButton = New_InfoButtonVisible
    PropertyChanged "InfoButtonVisible"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,"UFIS Version 4.4
Public Property Get InfoUfisVersion() As String
Attribute InfoUfisVersion.VB_Description = "Like ""UFIS Version 4.4"""
    InfoUfisVersion = Globals.ogUfisVersion
End Property

Public Property Let InfoUfisVersion(ByVal New_InfoUfisVersion As String)
    m_InfoUfisVersion = New_InfoUfisVersion
    Globals.ogUfisVersion = New_InfoUfisVersion
    PropertyChanged "InfoUfisVersion"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,Copyright 2001 AAT/I
Public Property Get InfoCopyright() As String
Attribute InfoCopyright.VB_Description = "Like ""Copyright 2001 AAT/I"""
    InfoCopyright = Globals.ogCopyright
End Property

Public Property Let InfoCopyright(ByVal New_InfoCopyright As String)
    m_InfoCopyright = New_InfoCopyright
    Globals.ogCopyright = New_InfoCopyright
    PropertyChanged "InfoCopyright"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,ABB Airport Technologies GmbH / Information Systems
Public Property Get InfoAAT() As String
Attribute InfoAAT.VB_Description = "like ""ABB Airport Technologies GmbH / Information Systems"""
    InfoAAT = Globals.ogAAT
End Property

Public Property Let InfoAAT(ByVal New_InfoAAT As String)
    m_InfoAAT = New_InfoAAT
    Globals.ogAAT = New_InfoAAT
    PropertyChanged "InfoAAT"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,Info about AbsencePlanning
Public Property Get InfoCaption() As String
Attribute InfoCaption.VB_Description = "Like ""Info about AbsencePlanning"""
    InfoCaption = Globals.ogInfoCaption
End Property

Public Property Let InfoCaption(ByVal New_InfoCaption As String)
    m_InfoCaption = New_InfoCaption
    Globals.ogInfoCaption = New_InfoCaption
    PropertyChanged "InfoCaption"
End Property
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=9,0,0,0
Public Property Get UfisComCtrl() As Object
Attribute UfisComCtrl.VB_Description = "Give the AATLoginControl the UfisCom so that it can communicate with the database."
    Set UfisComCtrl = Globals.UfisComControl
End Property

Public Property Set UfisComCtrl(ByRef New_UfisComCtrl As Object)
    Set Globals.UfisComControl = New_UfisComCtrl
    PropertyChanged "UfisComCtrl"
End Property
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,LoginCtrl 01.01.2001 / 4.4.1.1
Public Property Get InfoAppVersion() As String
Attribute InfoAppVersion.VB_Description = "Like ""AbsencePlanning 01.08.2001 / 4.4.1.1"""
    InfoAppVersion = Globals.ogApplication
End Property

Public Property Let InfoAppVersion(ByVal New_InfoAppVersion As String)
    m_InfoAppVersion = New_InfoAppVersion
    Globals.ogApplication = New_InfoAppVersion
    PropertyChanged "InfoAppVersion"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=0,0,0,False
Public Property Get UserNameLCase() As Boolean
    UserNameLCase = Globals.ogUserNameLCase
End Property

Public Property Let UserNameLCase(ByVal New_UserNameLCase As Boolean)
    m_UserNameLCase = New_UserNameLCase
    Globals.ogUserNameLCase = New_UserNameLCase
    PropertyChanged "UserNameLCase"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,1,1,
Public Property Get Version() As String
    m_Version = App.Major & "." & App.Minor & ".0." & App.Revision
    Version = m_Version
End Property

Public Property Let Version(ByVal New_Version As String)
    If Ambient.UserMode = False Then Err.Raise 387
    If Ambient.UserMode Then Err.Raise 382
    m_Version = New_Version
    PropertyChanged "Version"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,1,1,N/A
Public Property Get BuildDate() As String
    BuildDate = m_BuildDate
End Property

Public Property Let BuildDate(ByVal New_BuildDate As String)
    If Ambient.UserMode = False Then Err.Raise 387
    If Ambient.UserMode Then Err.Raise 382
    m_BuildDate = New_BuildDate
    PropertyChanged "BuildDate"
End Property

Private Function GetStat(ByRef strFunc As String) As String
    On Error GoTo ErrHdl
    Dim strKey As String
    strKey = Trim(CStr(strFunc))

    If DoesKeyExist(Globals.colStatus, strKey) Then
        GetStat = Globals.colStatus.Item(strKey)
    Else
        GetStat = ""
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "AATLoginControl.GetStat", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "AATLoginControl.GetStat", Err
    Err.Clear
    Resume Next
End Function

Private Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
End Function

Function WriteErrToLog(sFileName As String, sFunction As String, ErrObj As ErrObject) As Boolean
    On Error Resume Next
    Dim ilFileNumber As Integer
    Dim sText As String

    sText = CStr(Now) + " " + sFunction + vbCrLf + _
        "  error-description : " + CStr(ErrObj.Description) + vbCrLf + _
        "  error-number      : " + CStr(ErrObj.Number) + vbCrLf + _
        "  error-LastDllError: " + CStr(ErrObj.LastDllError) + vbCrLf

    ilFileNumber = FreeFile

    Open sFileName For Append As #ilFileNumber
        Print #ilFileNumber, sText
    Close #ilFileNumber
End Function

