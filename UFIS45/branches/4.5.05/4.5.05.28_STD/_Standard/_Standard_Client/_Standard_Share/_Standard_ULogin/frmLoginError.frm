VERSION 5.00
Begin VB.Form frmLoginError 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1620
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   Icon            =   "frmLoginError.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   108
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   StartUpPosition =   2  'CenterScreen
   Tag             =   "1030"
   Begin VB.CommandButton cmdRetry 
      Caption         =   "&Retry"
      Default         =   -1  'True
      Height          =   360
      Left            =   1110
      TabIndex        =   0
      Tag             =   "1031"
      Top             =   1080
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   360
      Left            =   2430
      TabIndex        =   1
      Tag             =   "1032"
      Top             =   1080
      Width           =   1140
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      Picture         =   "frmLoginError.frx":030A
      ScaleHeight     =   615
      ScaleWidth      =   615
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label1 
      Height          =   615
      Left            =   1080
      TabIndex        =   3
      Top             =   240
      Width           =   3495
   End
End
Attribute VB_Name = "frmLoginError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bmRetry As Boolean

Private Sub cmdCancel_Click()
    bmRetry = False
    frmLoginError.Hide
End Sub

Private Sub cmdRetry_Click()
    bmRetry = True
    frmLoginError.Hide
End Sub

Private Sub Form_Load()
    frmLoginError.Caption = LoadResString(1030)
    Label1.Caption = Globals.UfisComControl.LastErrorMessage
End Sub
