VERSION 5.00
Begin VB.Form frmRegister 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6165
   Icon            =   "frmRegister.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   6165
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   480
      Top             =   1680
   End
   Begin VB.CommandButton cmdRegister 
      Caption         =   "&Registrieren"
      Height          =   360
      Left            =   3832
      TabIndex        =   2
      Tag             =   "1012"
      Top             =   2640
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   360
      Left            =   2512
      TabIndex        =   1
      Tag             =   "1011"
      Top             =   2640
      Width           =   1140
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "&Starten"
      Default         =   -1  'True
      Height          =   360
      Left            =   1192
      TabIndex        =   0
      Tag             =   "1010"
      Top             =   2640
      Width           =   1140
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   0
      Picture         =   "frmRegister.frx":030A
      ScaleHeight     =   1095
      ScaleWidth      =   975
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label2 
      Height          =   375
      Left            =   1560
      TabIndex        =   5
      Top             =   2040
      Width           =   3855
   End
   Begin VB.Label Label1 
      Height          =   1455
      Left            =   1560
      TabIndex        =   4
      Top             =   360
      Width           =   3735
   End
End
Attribute VB_Name = "frmRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ilSeconds As Integer

Private Sub Form_Load()
    On Error GoTo ErrHdl
    Dim strCaptionTime As String
    Dim strCaptionText As String
    Dim llPos As Integer

    cmdRegister.Caption = LoadResString(cmdRegister.Tag)
    cmdCancel.Caption = LoadResString(cmdCancel.Tag)
    cmdStart.Caption = LoadResString(cmdStart.Tag)

    ' is the application registered yet?
    If Globals.colStatus.Count = 1 Then         ' ...no, it isn't
        cmdStart.Visible = False

        strCaptionText = LoadResString(1014)
        llPos = InStr(strCaptionText, "XXX")
        strCaptionText = Left(strCaptionText, llPos - 1) + _
                         Globals.ApplicationName + _
                         Right(strCaptionText, Len(strCaptionText) - llPos - 2)

        strCaptionTime = ""

    ElseIf Globals.colStatus.Count > 1 Then     ' ...yes, it is
        strCaptionText = LoadResString(1015)
        llPos = InStr(strCaptionText, "XXX")
        strCaptionText = Left(strCaptionText, llPos - 1) + _
                         Globals.ApplicationName + _
                         Right(strCaptionText, Len(strCaptionText) - llPos - 2)

        strCaptionTime = LoadResString(1013)
        llPos = InStr(strCaptionTime, "XXX")
        strCaptionTime = Left(strCaptionTime, llPos - 1) + _
                         "15" + _
                         Right(strCaptionTime, Len(strCaptionTime) - llPos - 2)

        ilSeconds = 15
        Timer1.Enabled = True
    End If

    frmRegister.Caption = Globals.ApplicationName
    Label1.Caption = strCaptionText
    Label2.Caption = strCaptionTime
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmRegister.Form_Load", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmRegister.Form_Load", Err
    Err.Clear
    Resume Next
End Sub

Private Sub cmdStart_Click()
    Globals.strLoginResult = "OK"
    Unload frmLogin
    Unload frmRegister
End Sub

Private Sub cmdCancel_Click()
    Globals.strLoginResult = "CANCEL"
    Unload frmLogin
    Unload frmRegister
End Sub

Private Sub cmdRegister_Click()
    On Error GoTo ErrHdl
    Dim strTable As String
    Dim strFieldlist As String
    Dim strWhere As String
    Dim strData As String
    Dim strWkst As String

    ' stop the timer and set the new label2's caption
    Timer1.Enabled = False
    Label2.Caption = LoadResString(1016)

    ' register the application
    If Globals.ApplicationRegisterString = "" Then
        Globals.ApplicationRegisterString = "AbsPlan,InitModu,InitModu,Initialisieren (InitModu),B,-" + _
            ",OnlyOwnDetailedWindow,m_OnlyOwnDetailedWindow,Action,A,1" + _
            ",OnlyOwnGroup,m_OnlyOwnGroup,Action,A,1"
    End If

    strTable = Globals.HomeAirport
    strFieldlist = "APPL,SUBD,FUNC,FUAL,TYPE,STAT"
    strWhere = "" '"WHERE URNO > '0'"
    strWkst = Globals.UfisComControl.GetWorkstationName()
    strData = Globals.UserName + "," + Globals.UserPassword + "," + Globals.ApplicationName + "," + strWkst

    Globals.UfisComControl.Twe = Globals.HomeAirport + "," + Globals.TableExtension + "," + Globals.ApplicationName
    If Globals.UfisComControl.CallServer("SMI", strTable, strFieldlist, Globals.ApplicationRegisterString, strWhere, "360") = 0 Then
    Else
    End If

    Globals.strLoginResult = "CANCEL"
    Timer1.Enabled = False
    Unload frmLogin
    Unload frmRegister
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmRegister.cmdRegister_Click", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmRegister.cmdRegister_Click", Err
    Err.Clear
    Resume Next
End Sub

Private Sub Timer1_Timer()
    On Error GoTo ErrHdl
    Dim strCaption As String
    Dim llPos As Integer

    If ilSeconds = 1 Then
        cmdStart_Click
        Exit Sub
    End If

    If ilSeconds > 1 Then
        ilSeconds = ilSeconds - 1
        strCaption = LoadResString(1013)
        llPos = InStr(strCaption, "XXX")
        strCaption = Left(strCaption, llPos - 1) + _
                     CStr(ilSeconds) + _
                     Right(strCaption, Len(strCaption) - llPos - 2)
        Label2.Caption = strCaption
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmRegister.Timer1_Timer", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmRegister.Timer1_Timer", Err
    Err.Clear
    Resume Next
End Sub

Function WriteErrToLog(sFileName As String, sFunction As String, ErrObj As ErrObject) As Boolean
    On Error Resume Next
    Dim sText As String
    sText = CStr(Now) + " " + sFunction + vbCrLf + _
        "  error-description : " + CStr(ErrObj.Description) + vbCrLf + _
        "  error-number      : " + CStr(ErrObj.Number) + vbCrLf + _
        "  error-LastDllError: " + CStr(ErrObj.LastDllError) + vbCrLf

    Open sFileName For Append As #1
        Print #1, sText
    Close #1
End Function


