VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6180
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   6180
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.CommandButton cmdInfo 
      Caption         =   "&Info"
      Height          =   360
      Left            =   4800
      TabIndex        =   8
      Top             =   1440
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtVersionString 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      Height          =   195
      IMEMode         =   3  'DISABLE
      Left            =   2160
      TabIndex        =   7
      Text            =   "VersionString"
      Top             =   2760
      Width           =   3975
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   360
      Left            =   3120
      TabIndex        =   4
      Tag             =   "1005"
      Top             =   1440
      Width           =   1140
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   360
      Left            =   1800
      TabIndex        =   3
      Tag             =   "1004"
      Top             =   1440
      Width           =   1140
   End
   Begin VB.TextBox txtPassword 
      BackColor       =   &H0080FFFF&
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   1800
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   720
      Width           =   3615
   End
   Begin VB.TextBox txtUserName 
      BackColor       =   &H0080FFFF&
      Height          =   360
      Left            =   1800
      TabIndex        =   1
      Top             =   240
      Width           =   3630
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000016&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   120
      Picture         =   "frmLogin.frx":030A
      ScaleHeight     =   735
      ScaleWidth      =   6015
      TabIndex        =   6
      Top             =   2280
      Width           =   6015
   End
   Begin VB.Label lblLabels 
      Caption         =   "Password:"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Tag             =   "1003"
      Top             =   840
      Width           =   1080
   End
   Begin VB.Label lblLabels 
      Caption         =   "Username:"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   5
      Tag             =   "1002"
      Top             =   360
      Width           =   1080
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Const COLOR_ACTIVECAPTION = 2

Private Sub cmdInfo_Click()
    frmInfo.Show vbModal
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHdl
    Dim sBuffer As String
    Dim lSize As Long
    Dim strLastUserName As String

'setting the user-name
    'read user-name from the registry
    strLastUserName = GetSetting(AppName:="ULogin", _
                                section:="Login", _
                                Key:="LastUserName", _
                                Default:="")

    If Len(strLastUserName) = 0 Then
        ' set the system's user name (only the first time)
        sBuffer = Space$(255)
        lSize = Len(sBuffer)
        Call GetUserName(sBuffer, lSize)
        If lSize > 0 Then
            strLastUserName = Left$(sBuffer, lSize)
            If Globals.ogUserNameLCase = True Then
                strLastUserName = LCase(strLastUserName)
            End If
        Else
            strLastUserName = ""
        End If
    End If
    txtUserName.Text = strLastUserName
    txtUserName.SelStart = 0
    txtUserName.SelLength = Len(txtUserName.Text)
    txtPassword.Text = ""

    'looking after the InfoButton
    If Globals.Info_ShowButton = False Then
        cmdInfo.Visible = False
    Else
        cmdInfo.Visible = True
    End If

    ' set some important variables
    If Trim(Globals.gVersionString) <> "" Then
        txtVersionString.Text = Globals.gVersionString
    Else
        txtVersionString.Visible = False
    End If
    If Globals.ApplicationName = "" Then
        Globals.ApplicationName = "AbsPlan"
    End If
    Globals.strLoginResult = ""
    Globals.TableExtension = GetIniEntry("", "GLOBAL", Globals.ApplicationName, "TABLEEXTENSION", "TAB")
    Globals.HomeAirport = GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOMEAIRPORT", "")
    Globals.HostType = GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOSTTYPE", "UNKNOWN")
    Globals.HostName = GetIniEntry("", "GLOBAL", Globals.ApplicationName, "HOSTNAME", "")

    frmLogin.Caption = LoadResString(1000) + " '" + Globals.ApplicationName + "' " + _
                       LoadResString(1001) + " " + LCase(Globals.HostName) + " / " + UCase(Globals.HostType)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.Form_Load", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.Form_Load", Err
    Err.Clear
    Resume Next
End Sub

Private Sub cmdOK_Click()
    On Error GoTo ErrHdl

    Dim strTable As String
    Dim strFieldlist As String
    Dim strWhere As String
    Dim strData As String
    Dim strWkst As String

    Dim llRecords As Long
    Dim i As Long

    Dim tmpStr As String
    Dim strItem As String
    Dim strKey As String
    Dim strVersion As String

    Dim ilLoginAttempts As Integer

    ConnectToCeda
    ilLoginAttempts = Globals.ogLoginAttempts
    
    Globals.UserName = txtUserName.Text
    Globals.UserPassword = txtPassword.Text

    strTable = Globals.HomeAirport
    strFieldlist = "USID,PASS,APPL,WKST"
    strWhere = "" '"WHERE URNO > '0'"
    strWkst = Globals.UfisComControl.GetWorkstationName()
    strData = Globals.UserName + "," + Globals.UserPassword + "," + Globals.ApplicationName + "," + strWkst

    ' Read from ceda
    Globals.UfisComControl.UserName = Globals.UserName
    If InStr(1, Globals.gVersionString, ".") > 1 Then
        strVersion = Right(Globals.gVersionString, Len(Globals.gVersionString) - InStr(1, Globals.gVersionString, ".") + 2)
    End If
    Globals.UfisComControl.Twe = Globals.HomeAirport + "," + Globals.TableExtension + "," + Globals.ApplicationName & "," & strVersion
    If Globals.UfisComControl.CallServer("GPR", strTable, strFieldlist, strData, strWhere, "360") = 0 Then
        If Globals.UfisComControl.GetBufferLine(0) = "[PRV]" Then

            ' delete all entries in the lookup-collection
            If Globals.colStatus.Count > 0 Then
                For i = 1 To Globals.colStatus.Count
                   Globals.colStatus.Remove 1
                Next i
            End If

            ' add the entries to the collection
            llRecords = Globals.UfisComControl.GetBufferCount
            For i = 1 To (llRecords - 1)
                tmpStr = Globals.UfisComControl.GetBufferLine(i)
                strKey = Trim(GetItem(tmpStr, 2, ","))
                strItem = Trim(GetItem(tmpStr, 3, ","))
                If DoesKeyExist(Globals.colStatus, strKey) = False Then
                    Globals.colStatus.Add Item:=strItem, Key:=strKey
                End If
            Next i

            'save the UserName in the registry for the next login
            SaveSetting "ULogin", "Login", "LastUserName", Globals.UserName
            ' look, if the person has got admin-rights
            If GetStat("InitModu") = "1" Then
                frmLogin.Hide
                frmRegister.Show vbModal
            Else
                Globals.strLoginResult = "OK"
                frmLogin.Hide
            End If
        Else
            Globals.strLoginResult = "ERROR"
        End If
    Else    ' an error occured!
        frmLoginError.Show vbModal
        If frmLoginError.bmRetry = True Then
            Globals.strLoginResult = "ERROR"
        Else
            Globals.strLoginResult = "CANCEL"
        End If
    End If

    cmdCancel_Click
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.cmdOK_Click", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.cmdOK_Click", Err
    Err.Clear
    Resume Next
End Sub

Private Sub cmdCancel_Click()
    On Error GoTo ErrHdl
    If Globals.strLoginResult = "" Then Globals.strLoginResult = "CANCEL"
    Unload frmLogin
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.cmdCancel_Click", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.cmdCancel_Click", Err
    Err.Clear
    Resume Next
End Sub

Private Sub Form_Terminate()
    On Error GoTo ErrHdl
    If Globals.strLoginResult = "" Then Globals.strLoginResult = "CANCEL"
    Unload frmLogin
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.Form_Terminate", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.Form_Terminate", Err
    Err.Clear
    Resume Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ErrHdl
    If Globals.strLoginResult = "" Then Globals.strLoginResult = "CANCEL"
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.Form_Unload", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.Form_Unload", Err
    Err.Clear
    Resume Next
End Sub

Public Function ConnectToCeda() As Boolean
    On Error GoTo ErrHdl
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sUser As String
    Dim sConnectType As String
    Dim ret As Integer

    Globals.CedaIsConnected = False

    UfisComControl.CleanupCom

    sHopo = Globals.HomeAirport
    sServer = Globals.HostName
    sTableExt = Globals.TableExtension
    sConnectType = "CEDA"
    sUser = Globals.UserName

    Globals.UfisComControl.SetCedaPerameters sUser, sHopo, sTableExt

    ret = Globals.UfisComControl.InitCom(sServer, sConnectType)

    If ret = 0 Then
        MsgBox "Connection to CEDA failed!" & vbCrLf & _
        "HomeAirport:    " & sHopo & vbCrLf & _
        "HostName:       " & sServer & vbCrLf & _
        "TableExtension: " & sTableExt & vbCrLf & _
        "ConnectType:    " & sConnectType, vbCritical, "No connection!"
        strLoginResult = "ERROR"
    Else
        Globals.CedaIsConnected = True
    End If

    ConnectToCeda = Globals.CedaIsConnected
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.ConnectToCeda", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.ConnectToCeda", Err
    Err.Clear
    Resume Next
End Function

Public Function GetIniEntry(cpFileName As String, cpSection1 As String, cpSection2 As String, cpKeyWord As String, cpDefault As String) As String
    On Error GoTo ErrHdl
    Dim IniFileName As String
    Dim TextLine As String
    Dim clHeader As String
    Dim clResult As String
    Dim blLoopEnd As Boolean
    Dim blSectionFound As Boolean
    Dim blBlockFound As Boolean
    Dim ilPos As Integer
    Dim ilCount As Integer

    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    IniFileName = cpFileName
    If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
    clResult = ""
    blLoopEnd = False
    blBlockFound = False
    ilCount = 0
    Do While (Not blLoopEnd) And (ilCount < 2)
        ilCount = ilCount + 1
        blSectionFound = False
        If ilCount = 1 Then
            clHeader = "[" & cpSection1 & "]"
        Else
            clHeader = "[" & cpSection2 & "]"
        End If
        Open IniFileName For Input As #1
        Do While (Not EOF(1)) And (Not blLoopEnd)
            Line Input #1, TextLine
            TextLine = LTrim(TextLine)
            If InStr(TextLine, "[") = 1 Then 'New Section
                If Not blSectionFound Then
                    If InStr(TextLine, clHeader) = 1 Then
                        blSectionFound = True 'Given Section found
                    End If
                Else
                    blLoopEnd = True
                End If
            Else
                If blSectionFound Then
                    If blBlockFound = True Then
                        If InStr(TextLine, "BLOCK_END") = 1 Then
                            blLoopEnd = True
                        Else
                            clResult = clResult & TextLine & vbNewLine
                        End If
                    Else
                        If InStr(TextLine, cpKeyWord) = 1 Then
                            'KeyWord found
                            ilPos = InStr(TextLine, "=")
                            If ilPos > 0 Then
                                clResult = Trim(Mid$(TextLine, ilPos + 1))
                                If clResult = "BLOCK_BEGIN" Then
                                    blBlockFound = True
                                    clResult = ""
                                Else
                                    blLoopEnd = True
                                End If
                            Else
                                'Nothing, we search until the end of section
                            End If
                        End If
                    End If
                End If
            End If
        Loop
        Close #1
        If clResult = "" Then
            blLoopEnd = False
        End If
    Loop
    If clResult = "" Then
        clResult = cpDefault
    End If
    GetIniEntry = clResult
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.GetIniEntry", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.GetIniEntry", Err
    Err.Clear
    Resume Next
End Function

Private Function GetStat(ByRef strFunc As String) As String
    On Error GoTo ErrHdl
    Dim strKey As String
    strKey = Trim(CStr(strFunc))

    If DoesKeyExist(Globals.colStatus, strKey) Then
        GetStat = Globals.colStatus.Item(strKey)
    Else
        GetStat = ""
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\ULoginErr.txt", "frmLogin.GetStat", Err
    WriteErrToLog UFIS_TMP & "\ULoginErr.txt", "frmLogin.GetStat", Err
    Err.Clear
    Resume Next
End Function

Private Function DoesKeyExist(ByRef refCollection As Collection, ByRef refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
End Function

Public Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
    Dim Result
    Dim ilSepLen As Integer
    Dim ilFirstPos As Integer
    Dim ilLastPos As Integer
    Dim ilItmLen As Integer
    Dim ilItmNbr As Integer

    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function

Function WriteErrToLog(sFileName As String, sFunction As String, ErrObj As ErrObject) As Boolean
    On Error Resume Next
    Dim sText As String
    sText = CStr(Now) + " " + sFunction + vbCrLf + _
        "  error-description : " + CStr(ErrObj.Description) + vbCrLf + _
        "  error-number      : " + CStr(ErrObj.Number) + vbCrLf + _
        "  error-LastDllError: " + CStr(ErrObj.LastDllError) + vbCrLf

    Open sFileName For Append As #1
        Print #1, sText
    Close #1
End Function


