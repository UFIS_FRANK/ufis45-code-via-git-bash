// Default string length.
#define STR_LEN   50

#define MAXBCNUM 30000

// Globally used function prototypes
BOOL InitApplication (HANDLE hInst);
BOOL InitInstance(HANDLE hInstance, int nCmdShow);

// Function prototypes

long FAR PASCAL MainWndProc (HWND hWnd, unsigned message,
                             WORD wParam, LONG lParam);

long DoCommands (HWND hWnd, unsigned message, WORD wParam, LONG lParam);

long FAR PASCAL AddRecordDlg (HWND hWnd, unsigned message,
                              WORD wParam, LONG lParam);

long FAR PASCAL GetRecordDlg (HWND hWnd, unsigned message,
                              WORD wParam, LONG lParam);


// defines for the file messages
// These are sent by FILESERV.DLL when it needs
// to access the file

#define UFIS_SYNC  WM_USER+20
#define UFIS_QUIT WM_USER+21
#define UFIS_TIMER WM_USER+22

// String Table Defines
#define IDS_PROGNAME       1
#define IDS_MAINCLASSNAME  3


// Global variables defined in this header.  One .C file (HIDINIT.C) should have
// IN_INIT #define'd prior to including this header -- it's in that
// C module where these variables will reside.

#ifdef IN_INIT
#define EXTERN
#endif

#ifndef IN_INIT
#define EXTERN extern
#endif

EXTERN HANDLE ghInst;                     // Handle to this instance
EXTERN HWND   ghWnd;                      // Handle to main window

