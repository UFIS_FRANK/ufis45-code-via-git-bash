#if !defined(AFX_TAXIWAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_TAXIWAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TaxiwayDlg.h : header file
//
#include <CedaTWYData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// TaxiwayDlg dialog

class TaxiwayDlg : public CDialog
{
// Construction
public:
	TaxiwayDlg(TWYDATA *popTWY,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(TaxiwayDlg)
	enum { IDD = IDD_TAXIWAYDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NATOD;
	CCSEdit	m_NATOT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_RESN;
	CCSEdit	m_TNAM;
	CCSEdit	m_RGRW;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_HOME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TaxiwayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TaxiwayDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	TWYDATA *pomTWY;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TAXIWAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B067__INCLUDED_)
