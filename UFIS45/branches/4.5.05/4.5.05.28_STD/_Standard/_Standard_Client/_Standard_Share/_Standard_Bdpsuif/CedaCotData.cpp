// CedaCotData.cpp
 
#include <stdafx.h>
#include <CedaCotData.h>
#include <resource.h>

void ProcessCotCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCotData::CedaCotData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for COTDATA
	BEGIN_CEDARECINFO(COTDATA,CotDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Ctrn,"CTRN")
		FIELD_CHAR_TRIM	(Dptc,"DPTC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Whpw,"WHPW")
		FIELD_CHAR_TRIM	(Inbu,"INBU")
	END_CEDARECINFO //(COTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(CotDataRecInfo)/sizeof(CotDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CotDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"COT");
	strcpy(pcmListOfFields,"CDAT,CTRC,CTRN,DPTC,LSTU,PRFL,REMA,URNO,USEC,USEU,WHPW,INBU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaCotData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("CTRC");
	ropFields.Add("CTRN");
	ropFields.Add("DPTC");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("WHPW");
	ropFields.Add("INBU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING41));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING318));
	ropDesription.Add(LoadStg(IDS_STRING893));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCotData::Register(void)
{
	ogDdx.Register((void *)this,BC_COT_CHANGE,	CString("COTDATA"), CString("Cot-changed"),	ProcessCotCf);
	ogDdx.Register((void *)this,BC_COT_NEW,		CString("COTDATA"), CString("Cot-new"),		ProcessCotCf);
	ogDdx.Register((void *)this,BC_COT_DELETE,	CString("COTDATA"), CString("Cot-deleted"),	ProcessCotCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCotData::~CedaCotData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCotData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCotData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		COTDATA *prlCot = new COTDATA;
		if ((ilRc = GetFirstBufferRecord(prlCot)) == true)
		{
			omData.Add(prlCot);//Update omData
			omUrnoMap.SetAt((void *)prlCot->Urno,prlCot);
		}
		else
		{
			delete prlCot;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCotData::Insert(COTDATA *prpCot)
{
	prpCot->IsChanged = DATA_NEW;
	if(Save(prpCot) == false) return false; //Update Database
	InsertInternal(prpCot);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCotData::InsertInternal(COTDATA *prpCot)
{
	ogDdx.DataChanged((void *)this, COT_NEW,(void *)prpCot ); //Update Viewer
	omData.Add(prpCot);//Update omData
	omUrnoMap.SetAt((void *)prpCot->Urno,prpCot);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCotData::Delete(long lpUrno)
{
	COTDATA *prlCot = GetCotByUrno(lpUrno);
	if (prlCot != NULL)
	{
		prlCot->IsChanged = DATA_DELETED;
		if(Save(prlCot) == false) return false; //Update Database
		DeleteInternal(prlCot);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCotData::DeleteInternal(COTDATA *prpCot)
{
	ogDdx.DataChanged((void *)this,COT_DELETE,(void *)prpCot); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCot->Urno);
	int ilCotCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCotCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCot->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCotData::Update(COTDATA *prpCot)
{
	if (GetCotByUrno(prpCot->Urno) != NULL)
	{
		if (prpCot->IsChanged == DATA_UNCHANGED)
		{
			prpCot->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCot) == false) return false; //Update Database
		UpdateInternal(prpCot);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCotData::UpdateInternal(COTDATA *prpCot)
{
	COTDATA *prlCot = GetCotByUrno(prpCot->Urno);
	if (prlCot != NULL)
	{
		*prlCot = *prpCot; //Update omData
		ogDdx.DataChanged((void *)this,COT_CHANGE,(void *)prlCot); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

COTDATA *CedaCotData::GetCotByUrno(long lpUrno)
{
	COTDATA  *prlCot;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCot) == TRUE)
	{
		return prlCot;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCotData::ReadSpecialData(CCSPtrArray<COTDATA> *popCot,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","COT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","COT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCot != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			COTDATA *prpCot = new COTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCot,CString(pclFieldList))) == true)
			{
				popCot->Add(prpCot);
			}
			else
			{
				delete prpCot;
			}
		}
		if(popCot->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCotData::Save(COTDATA *prpCot)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCot->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCot->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCot);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCot->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCot->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCot);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCot->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCot->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCotCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogCotData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCotData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCotData;
	prlCotData = (struct BcStruct *) vpDataPointer;
	COTDATA *prlCot;
	if(ipDDXType == BC_COT_NEW)
	{
		prlCot = new COTDATA;
		GetRecordFromItemList(prlCot,prlCotData->Fields,prlCotData->Data);
		if(ValidateCotBcData(prlCot->Urno)) //Prf: 8795
		{
			InsertInternal(prlCot);
		}
		else
		{
			delete prlCot;
		}
	}
	if(ipDDXType == BC_COT_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlCotData->Selection);
		prlCot = GetCotByUrno(llUrno);
		if(prlCot != NULL)
		{
			GetRecordFromItemList(prlCot,prlCotData->Fields,prlCotData->Data);
			if(ValidateCotBcData(prlCot->Urno)) //Prf: 8795
			{
				UpdateInternal(prlCot);
			}
			else
			{
				DeleteInternal(prlCot);
			}
		}
	}
	if(ipDDXType == BC_COT_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlCotData->Selection);

		prlCot = GetCotByUrno(llUrno);
		if (prlCot != NULL)
		{
			DeleteInternal(prlCot);
		}
	}
}

//Prf: 8795
//--ValidateCotBcData--------------------------------------------------------------------------------------

bool CedaCotData::ValidateCotBcData(const long& lrpUrno)
{
	bool blValidateCotBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<COTDATA> olCots;
		if(!ReadSpecialData(&olCots,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateCotBcData = false;
		}
	}
	return blValidateCotBcData;
}

//---------------------------------------------------------------------------------------------------------
