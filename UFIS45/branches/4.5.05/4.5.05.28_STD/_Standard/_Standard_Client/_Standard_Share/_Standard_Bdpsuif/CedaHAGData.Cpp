// CedaHAGData.cpp
 
#include <stdafx.h>
#include <CedaHAGData.h>
#include <resource.h>


// Local function prototype
static void ProcessHAGCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaHAGData::CedaHAGData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for HAGDATA
	BEGIN_CEDARECINFO(HAGDATA,HAGDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Hsna,"HSNA")
		FIELD_CHAR_TRIM	(Hnam,"HNAM")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_CHAR_TRIM	(Faxn,"FAXN")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
	END_CEDARECINFO //(HAGDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(HAGDataRecInfo)/sizeof(HAGDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HAGDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"HAG");
	strcpy(pcmHAGFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,HSNA,HNAM,TELE,FAXN,VAFR,VATO");
	pcmFieldList = pcmHAGFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaHAGData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("FAXN");
	ropFields.Add("HNAM");
	ropFields.Add("HSNA");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("TELE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING44));
	ropDesription.Add(LoadStg(IDS_STRING490));
	ropDesription.Add(LoadStg(IDS_STRING491));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING294));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//----------------------------------------------------------------------------------------------------
 
void CedaHAGData::Register(void)
{
	ogDdx.Register((void *)this,BC_HAG_CHANGE,CString("HAGDATA"), CString("HAG-changed"),ProcessHAGCf);
	ogDdx.Register((void *)this,BC_HAG_DELETE,CString("HAGDATA"), CString("HAG-deleted"),ProcessHAGCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaHAGData::~CedaHAGData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaHAGData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omHnamMap.RemoveAll();
    omHsnaMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaHAGData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omHsnaMap.RemoveAll();
    omHnamMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		HAGDATA *prpHAG = new HAGDATA;
		if ((ilRc = GetFirstBufferRecord(prpHAG)) == true)
		{
			prpHAG->IsChanged = DATA_UNCHANGED;
			omData.Add(prpHAG);//Update omData
			omUrnoMap.SetAt((void *)prpHAG->Urno,prpHAG);
			omHnamMap.SetAt(prpHAG->Hnam,prpHAG);
			omHsnaMap.SetAt(prpHAG->Hsna,prpHAG);
		}
		else
		{
			delete prpHAG;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaHAGData::InsertHAG(HAGDATA *prpHAG,BOOL bpSendDdx)
{
	prpHAG->IsChanged = DATA_NEW;
	if(SaveHAG(prpHAG) == false) return false; //Update Database
	InsertHAGInternal(prpHAG);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaHAGData::InsertHAGInternal(HAGDATA *prpHAG)
{
	//PrepareHAGData(prpHAG);
	ogDdx.DataChanged((void *)this, HAG_CHANGE,(void *)prpHAG ); //Update Viewer
	omData.Add(prpHAG);//Update omData
	omUrnoMap.SetAt((void *)prpHAG->Urno,prpHAG);
	omHnamMap.SetAt(prpHAG->Hnam,prpHAG);
	omHsnaMap.SetAt(prpHAG->Hsna,prpHAG);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaHAGData::DeleteHAG(long lpUrno)
{
	HAGDATA *prlHAG = GetHAGByUrno(lpUrno);
	if (prlHAG != NULL)
	{
		prlHAG->IsChanged = DATA_DELETED;
		if(SaveHAG(prlHAG) == false) return false; //Update Database
		DeleteHAGInternal(prlHAG);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaHAGData::DeleteHAGInternal(HAGDATA *prpHAG)
{
	ogDdx.DataChanged((void *)this,HAG_DELETE,(void *)prpHAG); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpHAG->Urno);
	omHnamMap.RemoveKey(prpHAG->Hnam);
	omHsnaMap.RemoveKey(prpHAG->Hsna);
	int ilHAGCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilHAGCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpHAG->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaHAGData::PrepareHAGData(HAGDATA *prpHAG)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaHAGData::UpdateHAG(HAGDATA *prpHAG,BOOL bpSendDdx)
{
	if (GetHAGByUrno(prpHAG->Urno) != NULL)
	{
		if (prpHAG->IsChanged == DATA_UNCHANGED)
		{
			prpHAG->IsChanged = DATA_CHANGED;
		}
		if(SaveHAG(prpHAG) == false) return false; //Update Database
		UpdateHAGInternal(prpHAG);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaHAGData::UpdateHAGInternal(HAGDATA *prpHAG)
{
	HAGDATA *prlHAG = GetHAGByUrno(prpHAG->Urno);
	if (prlHAG != NULL)
	{
		*prlHAG = *prpHAG; //Update omData
		ogDdx.DataChanged((void *)this,HAG_CHANGE,(void *)prlHAG); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

HAGDATA *CedaHAGData::GetHAGByUrno(long lpUrno)
{
	HAGDATA  *prlHAG;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlHAG) == TRUE)
	{
		return prlHAG;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaHAGData::ReadSpecialData(CCSPtrArray<HAGDATA> *popHag,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","HAG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","HAG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popHag != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			HAGDATA *prpHag = new HAGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpHag,CString(pclFieldList))) == true)
			{
				popHag->Add(prpHag);
			}
			else
			{
				delete prpHag;
			}
		}
		if(popHag->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaHAGData::SaveHAG(HAGDATA *prpHAG)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpHAG->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpHAG->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpHAG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpHAG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHAG->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpHAG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpHAG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHAG->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

int	CedaHAGData::GetShortNameList(CStringArray& ropNames)
{
	for (int i = 0;i < omData.GetSize(); i++)
	{
		ropNames.Add(omData[i].Hsna);		
	}

	return omData.GetSize();
}

HAGDATA* CedaHAGData::GetHagByUrno(long lpUrno)
{
	return(GetHAGByUrno(lpUrno));
}

HAGDATA* CedaHAGData::GetHagByName(const CString& ropName)
{
	HAGDATA *prlHag = NULL;
	omHnamMap.Lookup(ropName, (void *&) prlHag);
	return prlHag;
}

HAGDATA* CedaHAGData::GetHagByHsna(const CString& ropName)
{
	HAGDATA *prlHag = NULL;
	omHsnaMap.Lookup(ropName, (void *&) prlHag);
	return prlHag;
}


//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessHAGCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_HAG_CHANGE :
	case BC_HAG_DELETE :
		((CedaHAGData *)popInstance)->ProcessHAGBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaHAGData::ProcessHAGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlHAGData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlHAGData->Selection);

	HAGDATA *prlHAG = GetHAGByUrno(llUrno);
	if(ipDDXType == BC_HAG_CHANGE)
	{
		if (prlHAG != NULL)
		{
			GetRecordFromItemList(prlHAG,prlHAGData->Fields,prlHAGData->Data);
			if(ValidateHAGBcData(prlHAG->Urno)) //Prf: 8795
			{
				UpdateHAGInternal(prlHAG);
			}
			else
			{
				DeleteHAGInternal(prlHAG);
			}
		}
		else
		{
			prlHAG = new HAGDATA;
			GetRecordFromItemList(prlHAG,prlHAGData->Fields,prlHAGData->Data);
			if(ValidateHAGBcData(prlHAG->Urno)) //Prf: 8795
			{
				InsertHAGInternal(prlHAG);
			}
			else
			{
				delete prlHAG;
			}
		}
	}
	if(ipDDXType == BC_HAG_DELETE)
	{
		if (prlHAG != NULL)
		{
			DeleteHAGInternal(prlHAG);
		}
	}
}

//Prf: 8795
//--ValidateHAGBcData--------------------------------------------------------------------------------------

bool CedaHAGData::ValidateHAGBcData(const long& lrpUrno)
{
	bool blValidateHAGBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<HAGDATA> olHags;
		if(!ReadSpecialData(&olHags,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateHAGBcData = false;
		}
	}
	return blValidateHAGBcData;
}

//---------------------------------------------------------------------------------------------------------
