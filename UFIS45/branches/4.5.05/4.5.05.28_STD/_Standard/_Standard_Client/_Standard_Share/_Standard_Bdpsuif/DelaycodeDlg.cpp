// DelaycodeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <DelaycodeDlg.h>
#include <CedaALTData.h>
#include <PrivList.h>
#include <CedaParData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DelaycodeDlg dialog


DelaycodeDlg::DelaycodeDlg(DENDATA *popDEN,CWnd* pParent /*=NULL*/) : CDialog(DelaycodeDlg::IDD, pParent)
{
	pomDEN = popDEN;

	//{{AFX_DATA_INIT(DelaycodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DelaycodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DelaycodeDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_DECN,	 m_DECN);
	DDX_Control(pDX, IDC_DECA,	 m_DECA);
	DDX_Control(pDX, IDC_DECS,	 m_DECS);//AM:20100913 - For MultiDelayCode
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_ALC3,	 m_ALC3);
	DDX_Control(pDX, IDC_DENA,	 m_DENA);
	DDX_Control(pDX, IDC_CB_REPORT_COLUMN, m_RCOL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DelaycodeDlg, CDialog)
	//{{AFX_MSG_MAP(DelaycodeDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DelaycodeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL DelaycodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING167) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("DELAYCODEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomDEN->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomDEN->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomDEN->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomDEN->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomDEN->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomDEN->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	//	alphanumeric input for AIA only
	if ("ATH" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
	{
		m_DECN.SetFormat("XXXX");
	}
	else	// for all others number format only
	{
		m_DECN.SetFormat("####");
	}
	m_DECN.SetTextLimit(1,4);
	m_DECN.SetBKColor(YELLOW);
	m_DECN.SetTextErrColor(RED);
	m_DECN.SetInitText(pomDEN->Decn);
	m_DECN.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_DECN"));
	//------------------------------------
	//	alphanumeric input for AIA only
	if ("ATH" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
	{
		m_DECA.SetFormat("XXXX"); 
	}
	else	// for all others letters only
	{
		m_DECA.SetFormat("xxxx");
	}
	m_DECA.SetTextLimit(1,4);
//	m_DECA.SetBKColor(YELLOW);
	m_DECA.SetTextErrColor(RED);
	m_DECA.SetInitText(pomDEN->Deca);

	m_DECA.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_DECA"));
	//------------------------------------
	//AM:20100913 - For MultiDelayCode

	m_DECS.SetFormat("A|#");
	m_DECS.SetTextLimit(1,2);
	m_DECS.SetTextErrColor(RED);
	
	if (bgDEN_DECS_Exist)
	{
		m_DECS.SetInitText(pomDEN->Decs);
		m_DECS.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_DECS"));
		//m_DECS.SetSecState('1');
	}
	else
	{
		m_DECS.SetSecState('-');
	}
	//------------------------------------
	m_DENA.SetTypeToString("X(75)",75,1);
	m_DENA.SetBKColor(YELLOW);
	m_DENA.SetTextErrColor(RED);
	m_DENA.SetInitText(pomDEN->Dena);
	m_DENA.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_DENA"));
	//------------------------------------
	m_ALC3.SetFormat("x|#x|#x|#");
	m_ALC3.SetTextLimit(-1,-1,true);
	m_ALC3.SetTextErrColor(RED);
	m_ALC3.SetInitText(pomDEN->Alc3);
	m_ALC3.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_ALC3"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomDEN->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomDEN->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomDEN->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomDEN->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("DELAYCODEDLG.m_VATO"));
	//------------------------------------

	if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
	{
		UINT start = IDS_STRING1110;

		for (int i = 0; i < 16; i++)
		{
			CString	olFormat(LoadStg(start + i));		
			CStringArray olMsg;
			::ExtractItemList(olFormat,&olMsg,',');

			int ind = this->m_RCOL.AddString(olMsg[0]);
			if (ind >= 0)
			{
				int column = atoi(olMsg[1]);
				this->m_RCOL.SetItemData(ind,column);
			}
		}

		if (strlen(pomDEN->RCol) > 0)
		{
			int currColumn	= atoi(pomDEN->RCol);
			for (int i = 0; i < this->m_RCOL.GetCount(); i++)
			{
				if (this->m_RCOL.GetItemData(i) == currColumn)
				{
					this->m_RCOL.SetCurSel(i);
					break;
				}
			}
		}
	}
	else
	{
		CWnd *polWnd = this->GetDlgItem(IDC_LBL_REPORT_COLUMN);
		if (polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

		polWnd = this->GetDlgItem(IDC_CB_REPORT_COLUMN);
		if (polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->EnableWindow(FALSE);
		}
	}

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void DelaycodeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_DECN.GetStatus() == false)
	{
		if(m_DECN.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING322) + ogNotFormat;
		}
	}
	if(m_DECA.GetStatus() == false)
	{
		if(m_DECA.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING323) + ogNotFormat;
		}
	}
	if(m_DECN.GetWindowTextLength() == 0 && m_DECA.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING324);
	}
	if(m_DENA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DENA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING325) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING325) + ogNotFormat;
		}
	}
	if(m_ALC3.GetStatus() == false )
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING326) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	/////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olAlc3,olDeca,olDecn,olDecs;
		char clWhere[100];

		m_ALC3.GetWindowText(olAlc3);
		if(olAlc3.GetLength() == 3)
		{
			sprintf(clWhere,"WHERE ALC3='%s'",olAlc3);
			if(ogALTData.ReadSpecialData(NULL,clWhere,"ALC3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING327);
			}
		}
		CCSPtrArray<DENDATA> olDenCPA;
		m_DECA.GetWindowText(olDeca);
		m_DECN.GetWindowText(olDecn);

		CString olWhere = "";
		olWhere.Format( "WHERE ALC3='%s' AND DECA='%s' AND DECN='%s'",olAlc3,olDeca,olDecn );
		CString olField = "URNO,ALC3,DECA,DECN";
		
		if(olAlc3.GetLength() == 0) olAlc3 = " ";
		if(olDeca.GetLength() == 0) olDeca = " ";
		if(olDecn.GetLength() == 0) olDecn = " ";

		if (bgDEN_DECS_Exist)
		{
			m_DECS.GetWindowText(olDecs);
			if(olDecs.GetLength() == 0) olDecs = " ";
			olField += ",DECS";
			olWhere += " AND DECS='" + olDecs + "'";
		}

		char clField[200];
		sprintf(clWhere, olWhere );
		sprintf(clField, olField );
		//sprintf(clWhere,"WHERE ALC3='%s' AND DECA='%s' AND DECN='%s'",olAlc3,olDeca,olDecn);
		//if(ogDENData.ReadSpecialData(&olDenCPA,clWhere,"URNO,ALC3,DECA,DECN",false) == true)
		if(ogDENData.ReadSpecialData(&olDenCPA,clWhere,clField,false) == true)
		{
			if(olDenCPA[0].Urno != pomDEN->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING244);
			}
		}
		olDenCPA.DeleteAll();
	}

	/////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_ALC3.GetWindowText(pomDEN->Alc3,4);
		m_DECA.GetWindowText(pomDEN->Deca,3);
		m_DECN.GetWindowText(pomDEN->Decn,3);

		if (bgDEN_DECS_Exist)
		{
			CString st = "";
			m_DECS.GetWindowText( st );
			st.MakeUpper();
			if (st.GetLength()<1) st = " ";
			else st = st.Left( 1 );
			strcpy( pomDEN->Decs, st );
			//m_DECS.GetWindowText(pomDEN->Decs,2);
		}

		CString olTemp;
		m_DENA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomDEN->Dena,olTemp);

		pomDEN->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomDEN->Vato = DateHourStringToDate(olVatod,olVatot);

		if (this->m_RCOL.GetCurSel() != CB_ERR && this->m_RCOL.GetItemData(this->m_RCOL.GetCurSel()) != -1)
		{
			CString olResult;
			olResult.Format("%u",this->m_RCOL.GetItemData(this->m_RCOL.GetCurSel()));
			strcpy(pomDEN->RCol,olResult);
		}
		else
		{
			pomDEN->RCol[0] = '\0';
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DECN.SetFocus();
		m_DECN.SetSel(0,-1);

	}
}

//----------------------------------------------------------------------------------------

LONG DelaycodeDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_DECA.imID && m_DECA.GetWindowTextLength() > 0)
	{
		m_DECN.SetBKColor(WHITE);
	}
	if(lParam == (UINT)m_DECA.imID && m_DECA.GetWindowTextLength() == 0)
	{
		m_DECN.SetBKColor(YELLOW);
	}
	if(lParam == (UINT)m_DECN.imID && m_DECN.GetWindowTextLength() > 0)
	{
		m_DECA.SetBKColor(WHITE);
	}
	if(lParam == (UINT)m_DECN.imID && m_DECN.GetWindowTextLength() == 0)
	{
		m_DECA.SetBKColor(YELLOW);
	}
	return 0L;
}

//----------------------------------------------------------------------------------------
