// TimeParametersDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <TimeParametersDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld TimeParametersDlg 
//----------------------------------------------------------------------------------------

TimeParametersDlg::TimeParametersDlg(TIPDATA *popTip,CWnd* pParent): CDialog(TimeParametersDlg::IDD, pParent)
{
	pomTip = popTip;
	//{{AFX_DATA_INIT(TimeParametersDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void TimeParametersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TimeParametersDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_TIPU_H, m_TIPUH);
	DDX_Control(pDX, IDC_TIPU_M, m_TIPUM);
	DDX_Control(pDX, IDC_TIPU_S, m_TIPUS);
	DDX_Control(pDX, IDC_TIPU_D, m_TIPUD);
	DDX_Control(pDX, IDC_TIPC,	 m_TIPC);
	DDX_Control(pDX, IDC_TIPV,	 m_TIPV);
	DDX_Control(pDX, IDC_TIPT,	 m_TIPT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TimeParametersDlg, CDialog)
	//{{AFX_MSG_MAP(TimeParametersDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten TimeParametersDlg 
//----------------------------------------------------------------------------------------

BOOL TimeParametersDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING197) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("TIMEPARAMETERSDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomTip->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomTip->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomTip->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomTip->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomTip->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomTip->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_TIPC.SetFormat("x|#x|#x|#x|#");
	m_TIPC.SetTextLimit(1,4);
	m_TIPC.SetBKColor(YELLOW);  
	m_TIPC.SetTextErrColor(RED);
	m_TIPC.SetInitText(pomTip->Tipc);
	m_TIPC.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_TIPC"));
	//------------------------------------
	m_TIPV.SetFormat("####");
	m_TIPV.SetTextLimit(1,4);
	m_TIPV.SetBKColor(YELLOW);  
	m_TIPV.SetTextErrColor(RED);
	m_TIPV.SetInitText(pomTip->Tipv);
	m_TIPV.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_TIPV"));
	//------------------------------------
	if     (strcmp(pomTip->Tipu, "H") == 0) m_TIPUH.SetCheck(1);
	else if(strcmp(pomTip->Tipu, "M") == 0) m_TIPUM.SetCheck(1);
	else if(strcmp(pomTip->Tipu, "S") == 0) m_TIPUS.SetCheck(1);
	else if(strcmp(pomTip->Tipu, "D") == 0) m_TIPUD.SetCheck(1);
	else m_TIPUM.SetCheck(1);

	clStat = ogPrivList.GetStat("TIMEPARAMETERSDLG.m_TIPU");
	SetWndStatAll(clStat,m_TIPUH);
	SetWndStatAll(clStat,m_TIPUM);
	SetWndStatAll(clStat,m_TIPUS);
	SetWndStatAll(clStat,m_TIPUD);
	//--------------------------------------
	m_TIPT.SetTypeToString("X(128)",128,1);
	m_TIPT.SetBKColor(YELLOW);  
	m_TIPT.SetTextErrColor(RED);
	m_TIPT.SetInitText(pomTip->Tipt);
	m_TIPT.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_TIPT"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetBKColor(YELLOW);  
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetInitText(pomTip->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetBKColor(YELLOW);  
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetInitText(pomTip->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomTip->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomTip->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("TIMEPARAMETERSDLG.m_VATO"));
	//------------------------------------
	
	return TRUE; 
}

//----------------------------------------------------------------------------------------

void TimeParametersDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_TIPC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_TIPC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_TIPV.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_TIPV.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING587) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING587) + ogNotFormat;
		}
	}
	if(m_TIPT.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_TIPT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING501) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING501) + ogNotFormat;
		}
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	//////////////////////////////////////////////////////////////////////////////
	//Time-Check//
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}
	////////////////////
	if(ilStatus == true)
	{
		if(m_TIPUD.GetCheck() == 1)
		{
			CString olTipv;
			m_TIPV.GetWindowText(olTipv);
			if(CheckHHMMValid(olTipv) == FALSE)
			{
				olErrorText += LoadStg(IDS_STRING587) + LoadStg(IDS_STRING589);
				ilStatus = false;
			}
		}
	}
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olTipc;
		char clWhere[100];

		CCSPtrArray<TIPDATA> olTipList;
		m_TIPC.GetWindowText(olTipc);
		sprintf(clWhere,"WHERE TIPC='%s'",olTipc);
		if(ogTipData.ReadSpecialData(&olTipList,clWhere,"URNO,TIPC",false) == true)
		{
			if(olTipList[0].Urno != pomTip->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olTipList.DeleteAll();
	}
    ///////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		CString olText;
		m_TIPC.GetWindowText(olText); strcpy(pomTip->Tipc, olText);
		m_TIPV.GetWindowText(olText); strcpy(pomTip->Tipv, olText);
		////////////////////////////
		if     (m_TIPUH.GetCheck() == 1) strcpy(pomTip->Tipu, "H");
		else if(m_TIPUM.GetCheck() == 1) strcpy(pomTip->Tipu, "M");
		else if(m_TIPUS.GetCheck() == 1) strcpy(pomTip->Tipu, "S");
		else if(m_TIPUD.GetCheck() == 1) strcpy(pomTip->Tipu, "D");
		//wandelt Return in Blank//
		CString olTemp;
		m_TIPT.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomTip->Tipt,olTemp);
		////////////////////////////
		pomTip->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomTip->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_TIPC.SetFocus();
		m_TIPC.SetSel(0,-1);
	}
}
//----------------------------------------------------------------------------------------
