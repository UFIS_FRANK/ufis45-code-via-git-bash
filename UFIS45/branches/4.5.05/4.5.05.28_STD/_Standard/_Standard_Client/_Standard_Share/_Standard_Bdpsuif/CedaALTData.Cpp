// CedaALTData.cpp
 
#include <stdafx.h>
#include <CedaALTData.h>
#include <resource.h>


// Local function prototype
static void ProcessALTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CString	Help;
CedaALTData::CedaALTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for ALTDATA
	BEGIN_CEDARECINFO(ALTDATA,ALTDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Alc2,"ALC2")
		FIELD_CHAR_TRIM	(Alc3,"ALC3")
		FIELD_CHAR_TRIM	(Alfn,"ALFN")
		FIELD_CHAR_TRIM	(Cash,"CASH")
		FIELD_CHAR_TRIM	(Term,"TERM")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
		FIELD_CHAR_TRIM	(Akey,"AKEY")
		FIELD_CHAR_TRIM	(Rprt,"RPRT")
		FIELD_CHAR_TRIM	(Wrko,"WRKO")
		FIELD_CHAR_TRIM	(Admd,"ADMD")
		FIELD_CHAR_TRIM	(Add1,"ADD1")
		FIELD_CHAR_TRIM	(Add2,"ADD2")
		FIELD_CHAR_TRIM	(Add3,"ADD3")
		FIELD_CHAR_TRIM	(Add4,"ADD4")
		FIELD_CHAR_TRIM	(Base,"BASE")
		FIELD_CHAR_TRIM	(Cont,"CONT")
		FIELD_CHAR_TRIM	(Ctry,"CTRY")
		FIELD_CHAR_TRIM	(Emps,"EMPS")
		FIELD_CHAR_TRIM	(Exec,"EXEC")
		FIELD_CHAR_TRIM	(Fond,"FOND")
		FIELD_CHAR_TRIM	(Iano,"IANO")
		FIELD_CHAR_TRIM	(Iata,"IATA")
		FIELD_CHAR_TRIM	(Ical,"ICAL")
		FIELD_CHAR_TRIM	(Icao,"ICAO")
		FIELD_CHAR_TRIM	(Lcod,"LCOD")
		FIELD_CHAR_TRIM	(Phon,"PHON")
		FIELD_CHAR_TRIM	(Self,"SELF")
		FIELD_CHAR_TRIM	(Sita,"SITA")
		FIELD_CHAR_TRIM	(Telx,"TELX")
		FIELD_CHAR_TRIM	(Text,"TEXT")
		FIELD_CHAR_TRIM	(Tfax,"TFAX")
		FIELD_CHAR_TRIM	(Webs,"WEBS")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Doin,"DOIN")
		FIELD_CHAR_TRIM	(Terg,"TERG")
		FIELD_CHAR_TRIM	(Terp,"TERP")
		FIELD_CHAR_TRIM	(Terb,"TERB")
		FIELD_CHAR_TRIM	(Terw,"TERW")
		FIELD_CHAR_TRIM (Hsna,"HSNA")
		FIELD_CHAR_TRIM (Task,"TASK")
		FIELD_CHAR_TRIM (Ofst,"OFST")
	END_CEDARECINFO //(ALTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(ALTDataRecInfo)/sizeof(ALTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ALTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	
	strcpy(pcmTableName,"ALT");

	strcpy(pcmALTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ALC2,ALC3,ALFN,CASH,TERM,VAFR,VATO,AKEY,RPRT,WRKO,ADMD,ADD1,ADD2,ADD3,ADD4,BASE,CONT,CTRY,EMPS,EXEC,FOND,IANO,IATA,ICAL,ICAO,LCOD,PHON,SELF,SITA,TELX,TEXT,TFAX,WEBS,HOME,DOIN");

	char pclConfigPath[256];
	char pclUseOffsetForCCA[64];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "USE_OFFSET_CCA", "FALSE",pclUseOffsetForCCA , sizeof pclUseOffsetForCCA, pclConfigPath);


	if(strcmp(pclUseOffsetForCCA,"TRUE")==0)
	{
		bgUseOffsetForCCA = true;
	}
	else
	{	
		bgUseOffsetForCCA = false;
	}


	//Added by Christine
	char pclModeOfPay[256];
	GetPrivateProfileString(ogAppName, "NEW_MODE_OF_PAYMENT", "FALSE",pclModeOfPay, sizeof pclModeOfPay, pclConfigPath);
	if (stricmp(pclModeOfPay,"TRUE") == 0)
	{
		bgModeOfPay=true;
	}
	else
	{
		bgModeOfPay=false;
	}

	//Added by Christine
	char pclShowTaxi[256];
	GetPrivateProfileString(ogAppName, "ENABLE_TAXIWAY", "FALSE",pclShowTaxi, sizeof pclShowTaxi, pclConfigPath);
	if (stricmp(pclShowTaxi,"TRUE") == 0)
	{
		bgShowTaxi=true;
	}
	else
	{
		bgShowTaxi=false;
	}


	pcmFieldList = pcmALTFieldList;
	omData.SetSize(0,1000);
}

//---------------------------------------------------------------------------------------------------

void CedaALTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ALC2");	ropDesription.Add(LoadStg(IDS_STRING435));	ropType.Add("String");	
	ropFields.Add("ALC3");	ropDesription.Add(LoadStg(IDS_STRING436));	ropType.Add("String");	
	ropFields.Add("ALFN");	ropDesription.Add(LoadStg(IDS_STRING437));	ropType.Add("String");	
	ropFields.Add("CASH");	ropDesription.Add(LoadStg(IDS_STRING438));	ropType.Add("String");	
	ropFields.Add("CDAT");	ropDesription.Add(LoadStg(IDS_STRING343));	ropType.Add("Date");	
	ropFields.Add("LSTU");	ropDesription.Add(LoadStg(IDS_STRING344));	ropType.Add("Date");	
	ropFields.Add("PRFL");	ropDesription.Add(LoadStg(IDS_STRING345));	ropType.Add("String");	
	ropFields.Add("TERM");	ropDesription.Add(LoadStg(IDS_STRING1104));	ropType.Add("String");	
	ropFields.Add("URNO");	ropDesription.Add(LoadStg(IDS_STRING346));	ropType.Add("String");	
	ropFields.Add("USEC");	ropDesription.Add(LoadStg(IDS_STRING347));	ropType.Add("String");	
	ropFields.Add("USEU");	ropDesription.Add(LoadStg(IDS_STRING348));	ropType.Add("String");	
	ropFields.Add("VAFR");	ropDesription.Add(LoadStg(IDS_STRING230));	ropType.Add("Date");	
	ropFields.Add("VATO");	ropDesription.Add(LoadStg(IDS_STRING231));	ropType.Add("Date");	
	ropFields.Add("AKEY");	ropDesription.Add(LoadStg(IDS_STRING109));	ropType.Add("String");	
	ropFields.Add("RPRT");	ropDesription.Add(LoadStg(IDS_STRING707));	ropType.Add("String");	
	ropFields.Add("WRKO");	ropDesription.Add(LoadStg(IDS_STRING708));	ropType.Add("String");	
	ropFields.Add("ADMD");	ropDesription.Add(LoadStg(IDS_STRING636));	ropType.Add("String");	
	ropFields.Add("ADD1");	ropDesription.Add(LoadStg(IDS_STRING685));	ropType.Add("String");	
	ropFields.Add("ADD2");	ropDesription.Add(LoadStg(IDS_STRING686));	ropType.Add("String");	
	ropFields.Add("ADD3");	ropDesription.Add(LoadStg(IDS_STRING687));	ropType.Add("String");	
	ropFields.Add("ADD4");	ropDesription.Add(LoadStg(IDS_STRING688));	ropType.Add("String");	
	ropFields.Add("BASE");	ropDesription.Add(LoadStg(IDS_STRING689));	ropType.Add("String");	
	ropFields.Add("CONT");	ropDesription.Add(LoadStg(IDS_STRING690));	ropType.Add("String");	
	ropFields.Add("CTRY");	ropDesription.Add(LoadStg(IDS_STRING691));	ropType.Add("String");	
	ropFields.Add("EMPS");	ropDesription.Add(LoadStg(IDS_STRING692));	ropType.Add("String");	
	ropFields.Add("EXEC");	ropDesription.Add(LoadStg(IDS_STRING693));	ropType.Add("String");	
	ropFields.Add("FOND");	ropDesription.Add(LoadStg(IDS_STRING694));	ropType.Add("String");	
	ropFields.Add("IANO");	ropDesription.Add(LoadStg(IDS_STRING695));	ropType.Add("String");	
	ropFields.Add("IATA");	ropDesription.Add(LoadStg(IDS_STRING696));	ropType.Add("String");	
	ropFields.Add("ICAL");	ropDesription.Add(LoadStg(IDS_STRING697));	ropType.Add("String");	
	ropFields.Add("ICAO");	ropDesription.Add(LoadStg(IDS_STRING698));	ropType.Add("String");	
	ropFields.Add("LCOD");	ropDesription.Add(LoadStg(IDS_STRING699));	ropType.Add("String");	
	ropFields.Add("PHON");	ropDesription.Add(LoadStg(IDS_STRING700));	ropType.Add("String");	
	ropFields.Add("SELF");	ropDesription.Add(LoadStg(IDS_STRING701));	ropType.Add("String");	
	ropFields.Add("SITA");	ropDesription.Add(LoadStg(IDS_STRING702));	ropType.Add("String");	
	ropFields.Add("TELX");	ropDesription.Add(LoadStg(IDS_STRING703));	ropType.Add("String");	
	ropFields.Add("TEXT");	ropDesription.Add(LoadStg(IDS_STRING704));	ropType.Add("String");	
	ropFields.Add("TFAX");	ropDesription.Add(LoadStg(IDS_STRING705));	ropType.Add("String");	
	ropFields.Add("WEBS");	ropDesription.Add(LoadStg(IDS_STRING706));	ropType.Add("String");	
	ropFields.Add("HOME");	ropDesription.Add(LoadStg(IDS_STRING711));	ropType.Add("String");	
	ropFields.Add("DOIN");	ropDesription.Add(LoadStg(IDS_STRING712));	ropType.Add("String");	

	if (ogBasicData.IsTerminalRestrictionForAirlinesAvailable())
	{
		ropFields.Add("TERG");	ropDesription.Add(LoadStg(IDS_STRING1105));	ropType.Add("String");
		ropFields.Add("TERP");	ropDesription.Add(LoadStg(IDS_STRING1106));	ropType.Add("String");
		ropFields.Add("TERB");	ropDesription.Add(LoadStg(IDS_STRING1107));	ropType.Add("String");
		ropFields.Add("TERW");	ropDesription.Add(LoadStg(IDS_STRING1108));	ropType.Add("String");

	}
	if(ogBasicData.IsPrmHandlingAgentEnabled() == true)
	{
		ropFields.Add("HSNA");	ropDesription.Add(LoadStg(IDS_STRING1152));	ropType.Add("String");
	}

	if(ogBasicData.UseHandlingAgentFilter() == true)
	{
		ropFields.Add("HSNA");	ropDesription.Add(LoadStg(IDS_STRING1152));	ropType.Add("String");
		ropFields.Add("TASK");	ropDesription.Add(LoadStg(IDS_STRING1160));	ropType.Add("String");
	}

	if(bgUseOffsetForCCA)
	{
		ropFields.Add("OFST"); //Offset for CCA - Added by Christine 
		ropDesription.Add(LoadStg(IDS_STRING32819));//Offset for CKIC
		ropType.Add("String");
	}

}

//-----------------------------------------------------------------------------------------------

void CedaALTData::Register(void)
{
	ogDdx.Register((void *)this,BC_ALT_CHANGE,CString("ALTDATA"), CString("ALT-changed"),ProcessALTCf);
	ogDdx.Register((void *)this,BC_ALT_DELETE,CString("ALTDATA"), CString("ALT-deleted"),ProcessALTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaALTData::~CedaALTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaALTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaALTData::Read(char *pcpWhere)
{
	SetFieldList();

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
	{
#if 0
		if (ogBasicData.UseHandlingAgentFilter() == true)
		{
			if (strstr(pcpWhere,"HSNA") != NULL || strstr(pcpWhere,"TASK") != NULL)
			{
				char clSelOri[1024];
				char clSel2[1024];
				char pclNewSelection[1024];
				char *pclTableSave;
				char *pclOrder = NULL;
				char *pclHsna = NULL;

				
				pclHsna = strstr(pcpWhere,"HSNA");

				strcpy(clSelOri,pcpWhere);
				pclHsna = strstr(clSelOri,"HSNA");
				if (pclHsna != NULL) 
				{
					strcpy(clSel2,pclHsna);
					pclOrder = strstr(clSel2,"ORDER");
					if (pclOrder != NULL)
					{
						*pclOrder = '\0';
					}
					*pclHsna = '\0';
					strcpy(pclNewSelection,clSelOri);
					strcat(pclNewSelection," HAITAB.");
						strcat(pclNewSelection,clSel2);
						strcat(pclNewSelection," AND ALTU=ALTTAB.URNO ");

					pclOrder = strstr(pcpWhere,"ORDER");
					if (pclOrder != NULL)
					{
					strcat(pclNewSelection,pclOrder);
					}
				}
				strcpy(pcmTableName,"ALTTAB,HAITAB");
				
				ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pclNewSelection);
				strcpy(pcmTableName,"ALT");
			}
		} 
	
#endif

ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	}
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ALTDATA *prpALT = new ALTDATA;
		if ((ilRc = GetFirstBufferRecord(prpALT)) == true)
		{
			prpALT->IsChanged = DATA_UNCHANGED;
			Help = prpALT->Term;
			Help.Replace('?',',');
			strncpy(prpALT->Term,Help,66);
			//ogBasicData.UtcToLocal(prpALT->Cdat);
			//ogBasicData.UtcToLocal(prpALT->Lstu);
			omData.Add(prpALT);//Update omData
			omUrnoMap.SetAt((void *)prpALT->Urno,prpALT);
		}
		else
		{
			delete prpALT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaALTData::InsertALT(ALTDATA *prpALT,BOOL bpSendDdx)
{
	prpALT->IsChanged = DATA_NEW;
	if(SaveALT(prpALT) == false) return false; //Update Database
	InsertALTInternal(prpALT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaALTData::InsertALTInternal(ALTDATA *prpALT)
{
	//PrepareALTData(prpALT);
	ogDdx.DataChanged((void *)this, ALT_CHANGE,(void *)prpALT ); //Update Viewer
	omData.Add(prpALT);//Update omData
	omUrnoMap.SetAt((void *)prpALT->Urno,prpALT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaALTData::DeleteALT(long lpUrno)
{
	ALTDATA *prlALT = GetALTByUrno(lpUrno);
	if (prlALT != NULL)
	{
		prlALT->IsChanged = DATA_DELETED;
		if(SaveALT(prlALT) == false) return false; //Update Database
		DeleteALTInternal(prlALT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaALTData::DeleteALTInternal(ALTDATA *prpALT)
{
	ogDdx.DataChanged((void *)this,ALT_DELETE,(void *)prpALT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpALT->Urno);
	int ilALTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilALTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpALT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaALTData::PrepareALTData(ALTDATA *prpALT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaALTData::UpdateALT(ALTDATA *prpALT,BOOL bpSendDdx)
{
	if (GetALTByUrno(prpALT->Urno) != NULL)
	{
		if (prpALT->IsChanged == DATA_UNCHANGED)
		{
			prpALT->IsChanged = DATA_CHANGED;
		}
		if(SaveALT(prpALT) == false) return false; //Update Database
		UpdateALTInternal(prpALT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaALTData::UpdateALTInternal(ALTDATA *prpALT)
{
	ALTDATA *prlALT = GetALTByUrno(prpALT->Urno);

	if (prlALT != NULL)
	{
		*prlALT = *prpALT; //Update omData
		ogDdx.DataChanged((void *)this,ALT_CHANGE,(void *)prlALT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ALTDATA *CedaALTData::GetALTByUrno(long lpUrno)
{
	ALTDATA  *prlALT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlALT) == TRUE)
	{
		return prlALT;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaALTData::ReadSpecialData(CCSPtrArray<ALTDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ALT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ALT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAlt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ALTDATA *prpAlt = new ALTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAlt,CString(pclFieldList))) == true)
			{
				Help = prpAlt->Term;
				Help.Replace('?',',');
				strncpy(prpAlt->Term,Help,66);
				popAlt->Add(prpAlt);
			}
			else
			{
				delete prpAlt;
			}
		}
		if(popAlt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaALTData::SaveALT(ALTDATA *prpALT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];
	CString olWhere;

	if (prpALT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpALT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpALT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpALT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpALT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpALT);
		strcpy(pclData,olListOfData);
		if(bgModeOfPay)
		{
			ilRc = CedaAction("NTS",pclSelection,"",pclData);
			
		}
		else
		{
			ilRc = CedaAction("URT",pclSelection,"",pclData);			
		}
		prpALT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpALT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--Get sorted ALC3 list-----------------------------------------------------------------------------------

CString CedaALTData::GetSortedAlc3List(const CString& ropDelimiter)
{
	CString olResult;
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		olResult += CString(this->omData[i].Alc3) + ropDelimiter;		
	}

	return SortItemList(olResult,ropDelimiter[0]);
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessALTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ALT_CHANGE :
	case BC_ALT_DELETE :
		((CedaALTData *)popInstance)->ProcessALTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaALTData::ProcessALTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlALTData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlALTData->Selection);

	ALTDATA *prlALT = GetALTByUrno(llUrno);
	if(ipDDXType == BC_ALT_CHANGE)
	{
		if(prlALT != NULL)
		{
			GetRecordFromItemList(prlALT,prlALTData->Fields,prlALTData->Data);
			if(ValidateALTBcData(prlALT->Urno)) //Prf: 8795
			{
				UpdateALTInternal(prlALT);
			}
			else
			{
				DeleteALTInternal(prlALT);
			}
		}
		else
		{
			prlALT = new ALTDATA;
			GetRecordFromItemList(prlALT,prlALTData->Fields,prlALTData->Data);
			if(ValidateALTBcData(prlALT->Urno)) //Prf: 8795
			{
				InsertALTInternal(prlALT);
			}
			else
			{
				delete prlALT;
			}
		}
	}
	if(ipDDXType == BC_ALT_DELETE)
	{
		if (prlALT != NULL)
		{
			DeleteALTInternal(prlALT);
		}
	}
}
//Prf: 8795
//--ValidateALTBcData--------------------------------------------------------------------------------------

bool CedaALTData::ValidateALTBcData(const long& lrpUrno)
{
	bool blValidateALTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ALTDATA> olAlts;
		if(!ReadSpecialData(&olAlts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateALTBcData = false;
		}
	}
	return blValidateALTBcData;
}

//---------------------------------------------------------------------------------------------------------
bool CedaALTData::MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
	return MakeCedaData(&omRecInfo,pcpListOfData, pcpFieldList, pvpSaveDataStruct, pvpChangedDataStruct);
}

//---------------------------------------------------------------------------------------------------------
bool CedaALTData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
    pcpListOfData = "";
	char pclBuf[128];
    char buf[33];   // ltoa() need buffer 33 bytes
    CString sSave;
    CString sChanged;
    CString sOrg;
    CString pclFieldList;
	int ilFieldListPos = 0;
	int ilFieldListLength = 0;
	int ilTmpPos = 0;
	int ilLc = 0;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();

    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;

        void *pSave    = (void *)((char *)pvpSaveDataStruct + (*pomRecInfo)[fieldno].Offset);
        void *pChanged = (void *)((char *)pvpChangedDataStruct + (*pomRecInfo)[fieldno].Offset);

        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) 
		{
        case CEDACHAR:
			{
				sSave = (length == 0)? CString(" "): CString((char *)pSave).Left(length);
				MakeCedaString(sSave);
				sChanged = (length == 0)? CString(" "): CString((char *)pChanged).Left(length);
				MakeCedaString(sChanged);
			}
            break;
        case CEDAINT:
			{
				sSave	 = CString(itoa(*(int *)pSave, buf, 10));
				sChanged = CString(itoa(*(int *)pChanged, buf, 10));
			}
            break;
        case CEDALONG:
			{
				sSave	 = CString(ltoa(*(long *)pSave, buf, 10));
				sChanged = CString(ltoa(*(long *)pChanged, buf, 10));
			}
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			{
				/*
			
				if(strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == "CDAT" || strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == "LSTU")
				{
					ogBasicData.LocalToUtc((CTime&)pSave);
					ogBasicData.LocalToUtc((CTime&)pChanged);
				}
			*/
				
				sSave = ((CTime *)pSave)->Format("%Y%m%d%H%M00");
				sChanged = ((CTime *)pChanged)->Format("%Y%m%d%H%M00");
				sOrg = ((CTime *)pChanged)->Format("%Y%m%d%H%M%S");
				
			}
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			{
			/*
				if(strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == "CDAT" || strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == "LSTU")
				{
					ogBasicData.LocalToUtc((CTime&)pSave);
					ogBasicData.LocalToUtc((CTime&)pChanged);
				}
				*/
				sSave = ((COleDateTime *)pSave)->Format("%Y%m%d%H%M%S");
				sChanged = ((COleDateTime *)pChanged)->Format("%Y%m%d%H%M%S");
			}
            break;
		case CEDADOUBLE:
			{
				sprintf(pclBuf,"%.2f",(double *)pSave);
				sSave = CString(pclBuf);
				sprintf(pclBuf,"%.2f",(double *)pChanged);
				sChanged = CString(pclBuf);
			}
			break;
        default:    // invalid field type
            return false;
        }

		ilTmpPos = ilFieldListPos;
		ilFieldListLength = pcpFieldList.GetLength();

		for( ilFieldListPos++ ; ((ilFieldListPos < ilFieldListLength)  && (pcpFieldList[ilFieldListPos] != ',')); ilFieldListPos++);

		if(sChanged != sSave)
		{
			if(type == CEDADATE)
				pcpListOfData += sOrg + ",";
			else
				pcpListOfData += sChanged + ",";

// TVO 02.02.2000
			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos - 1) )
//			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos ) )
				pclFieldList += pcpFieldList.Mid(ilTmpPos, ilFieldListPos - ilTmpPos);
		}

    }
	if(!pclFieldList.IsEmpty())
	{
		if(pclFieldList[0] == ',')
			pclFieldList.SetAt(0,' ');
		if(pclFieldList[pclFieldList.GetLength()-1] == ',')
			pclFieldList.SetAt(pclFieldList.GetLength()-1,' ');
		pclFieldList.TrimRight();
		pclFieldList.TrimLeft();
	}
	pcpFieldList = pclFieldList;
	if(!pcpListOfData.IsEmpty())
	{
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);
	}
    return true;
}

//---------------------------------------------------------------------------------------------------------
bool CedaALTData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			//ogBasicData.LocalToUtc((CTime&)p);
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				//ogBasicData.LocalToUtc((CTime&)p);
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}


void CedaALTData::SetFieldList(void)
{
	if (ogBasicData.IsTerminalRestrictionForAirlinesAvailable())
	{
		if( CString(pcmFieldList).Find("TERG,TERP,TERB,TERW") < 0 )
		{
			strcat(pcmFieldList,",TERG,TERP,TERB,TERW");
		}
	}
	if(ogBasicData.IsPrmHandlingAgentEnabled() == true)
	{
		if( CString(pcmFieldList).Find("HSNA") < 0 )
		{
			strcat(pcmFieldList,",HSNA");
		}
	}

	if(ogBasicData.UseHandlingAgentFilter() == true)
	{
		if( CString(pcmFieldList).Find("HSNA") < 0 )
		{
			strcat(pcmFieldList,",HSNA");
		}
		if( CString(pcmFieldList).Find("TASK") < 0 )
		{
			strcat(pcmFieldList,",HSNA");
		}
	}

	if(bgUseOffsetForCCA)
	{
		if( CString(pcmFieldList).Find("OFST") < 0 )
		{
			strcat(pcmFieldList,",OFST");
		}
	}

}