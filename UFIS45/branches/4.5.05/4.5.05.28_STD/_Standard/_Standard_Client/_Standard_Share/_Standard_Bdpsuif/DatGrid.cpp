// DatGrid.cpp : implementation file
//

#include <stdafx.h>
#include <DatGrid.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <resrc1.h>
#include <bdpsuif.h>
#include <CedaGrnData.h>
#include <CedaAloData.h>
#include <CedaAltData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDatGrid

CDatGrid::CDatGrid()
{
	this->lmAlid = -1;
	this->lmAloc = -1;
}

CDatGrid::~CDatGrid()
{

}


BEGIN_MESSAGE_MAP(CDatGrid, CGXGridWnd)
	//{{AFX_MSG_MAP(CDatGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


bool CDatGrid::Initialize(const CString& ropAloc,long lpAlid,bool bpCopy,CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly)
{
	this->CGXGridWnd::Initialize();
	this->SetColCount(7);

	this->GetParam()->EnableTrackRowHeight(FALSE);		//disable rowsizing
	this->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	this->GetParam()->EnableSelection(GX_SELROW);

	//*** HIDE URNO AND BSDU ***
	this->SetColWidth(0,0,20);	// Row number
	this->SetColWidth(1,1,0);
	this->SetColWidth(2,2,110);
	this->SetColWidth(3,3,110);
	this->SetColWidth(4,4,100);
	this->SetColWidth(5,5,100);
	this->SetColWidth(6,6,90);
	this->SetColWidth(7,7,0);

	this->HideCols(1,1);
	this->HideCols(7,7);

	this->SetValueRange(CGXRange(0,1), GetListItem(LoadStg(IDS_STRING1089),1,true,'|'));
	this->SetValueRange(CGXRange(0,2), GetListItem(LoadStg(IDS_STRING1089),2,true,'|'));
	this->SetValueRange(CGXRange(0,3), GetListItem(LoadStg(IDS_STRING1089),3,true,'|'));
	this->SetValueRange(CGXRange(0,4), GetListItem(LoadStg(IDS_STRING1089),4,true,'|'));
	this->SetValueRange(CGXRange(0,5), GetListItem(LoadStg(IDS_STRING1089),5,true,'|'));
	this->SetValueRange(CGXRange(0,6), GetListItem(LoadStg(IDS_STRING1089),6,true,'|'));
	this->SetValueRange(CGXRange(0,7), GetListItem(LoadStg(IDS_STRING1089),7,true,'|'));

	this->omAloc = ropAloc;
	this->lmAlid = lpAlid;
	this->bmCopy = bpCopy;

	this->Fill(ropDatPtr,bpReadOnly);


	return true;
}


void CDatGrid::Fill(CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly)
{
	int ilDataCount;
	CString olWhere;

	long llUrno = ogAloData.GetAloUrnoByName(this->omAloc);
	if (llUrno == 0)
		return;
	else
	{
		this->lmAloc = llUrno;
	}

	olWhere.Format("WHERE ALOC = '%ld' AND ALID = '%ld'",llUrno,this->lmAlid);
	ogDatData.ReadSpecialData(&ropDatPtr,olWhere.GetBuffer(0),"",false);

	// get number of rows
	ilDataCount = ropDatPtr.GetSize();
	int	iDatCount	= ilDataCount;
	this->SetRowCount(ilDataCount);

	// fill the grid with the already existing data
	for(int i = 0; i < ilDataCount; i++)
	{
		if (bmCopy == 1)
		{
			this->SetValueRange(CGXRange(i+1,1),"0");
		}
		else
		{
			this->SetValueRange(CGXRange(i+1,1),ropDatPtr[i].Urno);
		}

		this->SetValueRange(CGXRange(i+1,2),ropDatPtr[i].Alc3);
		GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(ropDatPtr[i].Algr);
		if (prlGrn == NULL)
			this->SetValueRange(CGXRange(i+1,3),"");
		else
			this->SetValueRange(CGXRange(i+1,3),prlGrn->Grpn);

		if (ropDatPtr[i].Pafr < 0)
			this->SetValueRange(CGXRange(i+1,4),"");
		else
			this->SetValueRange(CGXRange(i+1,4),ropDatPtr[i].Pafr);

		if (ropDatPtr[i].Pato < 0)
			this->SetValueRange(CGXRange(i+1,5),"");
		else
			this->SetValueRange(CGXRange(i+1,5),ropDatPtr[i].Pato);

		this->SetValueRange(CGXRange(i+1,6),ropDatPtr[i].Dura);

		if (bmCopy == 1)
		{
			ropDatPtr[i].IsChanged = DATA_NEW;
			this->SetValueRange(CGXRange(i+1,7),"NEW");
		}
		else
		{
			this->SetValueRange(CGXRange(i+1,7),"OK");
		}
	}

	ilDataCount = ogALTData.omData.GetSize(); //number of rows
	omAllALT = ogALTData.GetSortedAlc3List("\n");

	ilDataCount = ogGrnData.omData.GetSize(); //number of rows
	omAllGRN = ogGrnData.GetSortedGrpnList("\n");

	// set cell styles

	if (iDatCount > 0)
	{
		this->SetStyleRange(CGXRange(1,2,iDatCount,2),
				CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(omAllALT)); 

		this->SetStyleRange(CGXRange(1,3,iDatCount,3),
				CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(omAllGRN)); 

		this->SetStyleRange(CGXRange(1,4,iDatCount,4),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

		this->SetStyleRange(CGXRange(1,5,iDatCount,5),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

		this->SetStyleRange(CGXRange(1,6,iDatCount,6),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));
	}

	if (bpReadOnly)
		this->SetReadOnly(true);
}

//---------------------------------------------------------------------------
void CDatGrid::Delete(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr) 
{
	ROWCOL iCol,iRow;
	this->GetCurrentCell(iRow,iCol);

	if (iRow > 0)
	{
		if (&ropDatPtr[iRow - 1] != NULL)
		{
			DATDATA *prlDat = new DATDATA;
			*prlDat = ropDatPtr[iRow - 1];
			prlDat->IsChanged = DATA_DELETED;
			ropDeleteDatPtr.Add(prlDat);
			ropDatPtr.DeleteAt(iRow - 1);

			this->RemoveRows(iRow,iRow);

			this->SetCurrentCell(0,0);
		}
	}
			
}

//---------------------------------------------------------------------------
void CDatGrid::Insert(CCSPtrArray<DATDATA>& ropDatPtr) 
{
	long iRows = this->GetRowCount()+1;
	this->InsertRows(iRows,1);

	DATDATA *prlDat = new DATDATA;
	prlDat->Urno	= 0;
	prlDat->IsChanged = DATA_NEW;

	ropDatPtr.Add(prlDat);

	this->SetValueRange(CGXRange(iRows,1),prlDat->Urno);
	this->SetValueRange(CGXRange(iRows,2),prlDat->Alc3);

	GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(prlDat->Algr);
	if (prlGrn == NULL)
		this->SetValueRange(CGXRange(iRows,3),"");
	else
		this->SetValueRange(CGXRange(iRows,3),prlGrn->Grpn);

	if (prlDat->Pafr < 0)
		this->SetValueRange(CGXRange(iRows,4),"");
	else
		this->SetValueRange(CGXRange(iRows,4),prlDat->Pafr);

	if (prlDat->Pato < 0)
		this->SetValueRange(CGXRange(iRows,5),"");
	else
		this->SetValueRange(CGXRange(iRows,5),prlDat->Pato);

	this->SetValueRange(CGXRange(iRows,6),prlDat->Dura);
	this->SetValueRange(CGXRange(iRows,7),"NEW");


	this->SetStyleRange(CGXRange(1,2,iRows,2),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllALT)); 

	this->SetStyleRange(CGXRange(1,3,iRows,3),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllGRN)); 

	this->SetStyleRange(CGXRange(1,4,iRows,4),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,5,iRows,5),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,6,iRows,6),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetCurrentCell(iRows,2);
}

//---------------------------------------------------------------------------
void CDatGrid::Copy(CCSPtrArray<DATDATA>& ropDatPtr) 
{
	ROWCOL iCol,iRow;
	this->GetCurrentCell(iRow,iCol);

	if (iRow > 0)
	{
		if (&ropDatPtr[iRow - 1] != NULL)
		{
			DATDATA *prlDat = new DATDATA;
			*prlDat = ropDatPtr[iRow - 1];
			prlDat->IsChanged = DATA_NEW;
			ropDatPtr.Add(prlDat);

			long iRows = this->GetRowCount()+1;
			this->InsertRows(iRows,1);

#if	1
			if (this->CopyCells(CGXRange(iRow,1,iRow,7),iRows,1))
			{
				this->SetValueRange(CGXRange(iRows,1),"0");
				this->SetValueRange(CGXRange(iRows,7),"NEW");
			}
			
#else
			this->SetValueRange(CGXRange(iRows,1),prlDat->Urno);

			this->SetValueRange(CGXRange(iRows,2),prlDat->Alc3);
			GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(prlDat->Algr);
			if (prlGrn == NULL)
				this->SetValueRange(CGXRange(iRows,3),"");
			else
				this->SetValueRange(CGXRange(iRows,3),prlGrn->Grpn);

			if (prlDat->Pafr < 0)
				this->SetValueRange(CGXRange(iRows,4),"");
			else
				this->SetValueRange(CGXRange(iRows,4),prlDat->Pafr);

			if (prlDat->Pato < 0)
				this->SetValueRange(CGXRange(iRows,5),"");
			else
				this->SetValueRange(CGXRange(iRows,5),prlDat->Pato);

			this->SetValueRange(CGXRange(iRows,6),prlDat->Dura);
			this->SetValueRange(CGXRange(iRows,7),"NEW");
#endif

			this->SetStyleRange(CGXRange(1,2,iRows,2),
					CGXStyle()
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX) 
					.SetChoiceList(omAllALT)); 

			this->SetStyleRange(CGXRange(1,3,iRows,3),
					CGXStyle()
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX) 
					.SetChoiceList(omAllGRN)); 

			this->SetStyleRange(CGXRange(1,4,iRows,4),
					CGXStyle()
					.SetControl(GX_IDS_CTRL_MASKEDIT)
					.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

			this->SetStyleRange(CGXRange(1,5,iRows,5),
					CGXStyle()
					.SetControl(GX_IDS_CTRL_MASKEDIT)
					.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

			this->SetStyleRange(CGXRange(1,6,iRows,6),
					CGXStyle()
					.SetControl(GX_IDS_CTRL_MASKEDIT)
					.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

			this->SetCurrentCell(iRows,2);
		}
	}
			
}


//---------------------------------------------------------------------------
void CDatGrid::Update(CCSPtrArray<DATDATA>& ropDatPtr)
{
	long iRows = this->GetRowCount()+1;
	this->InsertRows(iRows,1);

	this->SetValueRange(CGXRange(iRows,1),ogBasicData.GetNextUrno());
	this->SetValueRange(CGXRange(iRows,7),"NEW");


	this->SetStyleRange(CGXRange(1,2,iRows,2),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllALT)); 

	this->SetStyleRange(CGXRange(1,3,iRows,3),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllGRN)); 

	this->SetStyleRange(CGXRange(1,4,iRows,4),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,5,iRows,5),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,6,iRows,6),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetCurrentCell(iRows,2);
}

//---------------------------------------------------------------------------
bool CDatGrid::FinalCheck(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr,CString& ropErrorText)
{
	bool blStatus = true;
	CString olLine;
	CString olStatus; 
	for (int i = 1; i <= this->GetRowCount(); i++)
	{
		olLine.Format("%d ",i);

		olStatus = GetValueRowCol(i,7);
		if (olStatus == "NEW" || olStatus == "UPD")
		{
//			perform logical checks
			CString olAlc3 = this->GetValueRowCol(i,2);
			if (!olAlc3.IsEmpty())
			{
				if (this->omAllALT.Find(olAlc3+"\n") < 0 && this->omAllALT.Find(olAlc3) + olAlc3.GetLength() != this->omAllALT.GetLength())
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING327);	// invalid airline code
				
				}
			}

			CString olGrpn = this->GetValueRowCol(i,3);
			if (!olGrpn.IsEmpty())
			{
				if (this->omAllGRN.Find(olGrpn+"\n") < 0 && this->omAllGRN.Find(olGrpn) + olGrpn.GetLength() != this->omAllGRN.GetLength())
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING28);	// invalid group code
				
				}
			}


			CString olPafr = GetValueRowCol(i,4);
			CString olPato = GetValueRowCol(i,5);
			if (!olPafr.IsEmpty() && !olPato.IsEmpty())
			{
				if (atol(olPafr) > atol(olPato))
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING112);	// min mustn't be greater than max

				}
			}

			if (blStatus == true)
			{
				strcpy(ropDatPtr[i-1].Alc3,olAlc3);				

				GRNDATA *prlGrn = ogGrnData.GetGrnByGrpn("ALTTAB",olGrpn);				
				if (prlGrn == NULL)
					ropDatPtr[i-1].Algr = 0;
				else
					ropDatPtr[i-1].Algr = prlGrn->Urno;

				CString olPafr = GetValueRowCol(i,4);
				if (olPafr.IsEmpty()) 
					ropDatPtr[i-1].Pafr = -1;				
				else
					ropDatPtr[i-1].Pafr = atol(olPafr);				

				CString olPato = GetValueRowCol(i,5);
				if (olPato.IsEmpty())
					ropDatPtr[i-1].Pato = -1;				
				else
					ropDatPtr[i-1].Pato = atol(olPato);				

				ropDatPtr[i-1].Dura = atol(GetValueRowCol(i,6));

				if (ropDatPtr[i-1].IsChanged == DATA_NEW)
				{
					ropDatPtr[i-1].Aloc = this->lmAloc;
					ropDatPtr[i-1].Alid = this->lmAlid;
					ropDatPtr[i-1].Cdat = CTime::GetCurrentTime();
					ropDatPtr[i-1].Cdat = CTime::GetCurrentTime();
					strcpy(ropDatPtr[i-1].Usec,cgUserName);
				}
				else if (ropDatPtr[i-1].IsChanged == DATA_UNCHANGED)
				{
					ropDatPtr[i-1].IsChanged = DATA_CHANGED;
				}
			}
		}
	}

	return blStatus;
}
/////////////////////////////////////////////////////////////////////////////
// CDatGrid message handlers
BOOL CDatGrid::SubClassDlgItem(UINT nID, CWnd *pParent)
{
	CWnd::SubclassDlgItem(nID,pParent);
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
BOOL CDatGrid::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)	
{
	//--- Check f�r StingRay Fehler
	if (nCol > GetColCount())
		return FALSE;


	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if (nRow == 0 && nCol != 0)
	{
		CGXSortInfoArray  sortInfo;
		sortInfo .SetSize(1);
		
		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == TRUE)
		{
			sortInfo[0].sortOrder = CGXSortInfo::descending;
			bmSortAscend = FALSE;
		}
		else
		{
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = TRUE;
		}
		
		sortInfo[0].nRC = nCol;                       
		sortInfo[0].sortType = CGXSortInfo::autodetect;  
		SortRows( CGXRange().SetTable(), sortInfo); 

		//--- Merke welche Spalte f�r sorting verwendet wurde

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, nCol, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = TRUE;
		else
		  bmSortNumerical = FALSE;
	}
	return FALSE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CDatGrid::OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	if (nChar==9)
		return FALSE;
	else
	{
		CGXGridCore::OnGridKeyDown(nChar,nRepCnt,nFlags);
		return TRUE;
	}
}

BOOL CDatGrid::OnDeleteCell()
{
	return true;
}

BOOL CDatGrid::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	if (nCol == 2)
	{
		CString olValue = this->GetValueRowCol(nRow,nCol);
		olValue.MakeUpper();
		this->SetValueRange(CGXRange(nRow,nCol),olValue);
	}

	SetValueRange(CGXRange(nRow,7),"UPD");
	return true;
}

void CDatGrid::OnModifyCell (ROWCOL nRow, ROWCOL nCol)
{
#if	0
	if (nCol == 2)
	{
		CGXComboBox *polCtrl = (CGXComboBox *)this->GetControl(nRow,nCol);
		if (polCtrl != NULL)
		{
			CString olValue;
			if (polCtrl->GetValue(olValue))
			{
				olValue.MakeUpper();
				polCtrl->SetValue(olValue);
			}
		}
	}
#endif
}