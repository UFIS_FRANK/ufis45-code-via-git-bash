// CedaScoData.h

#ifndef __CEDAPSCODATA__
#define __CEDAPSCODATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

#include <resrc1.h>		// main symbols#


//---------------------------------------------------------------------------------------------------------

struct SCODATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	char Code[7];		// Codereferenz COT.CODE
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis
	char Kost[9];		// Kostenstelle
	char Cweh[5];		// vertragl. Wochenstunden [HHMM]
	char Kstn[29];		// Kostenstellennummer

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SCODATA(void)
	{ memset(this,'\0',sizeof(*this));
	  //Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end SCODataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaScoData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omStaffUrnoMap;

    CCSPtrArray<SCODATA> omData;

	char pcmListOfFields[2048];

// OScoations
public:
    CedaScoData();
	~CedaScoData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SCODATA *prpSco);
	bool InsertInternal(SCODATA *prpSco);
	bool Update(SCODATA *prpSco);
	bool UpdateInternal(SCODATA *prpSco);
	bool Delete(long lpUrno);
	bool DeleteInternal(SCODATA *prpSco);
	bool ReadScocialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SCODATA *prpSco);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SCODATA  *GetScoByUrno(long lpUrno);
	SCODATA *GetValidScoBySurn(long lpSurn);
	void GetAllScoBySurn(long lpSurn,CCSPtrArray<SCODATA> &ropList);


	// Private methods
private:
    void PrepareScoData(SCODATA *prpScoData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSCODATA__
