// CedaFIDData.cpp
 
#include <stdafx.h>
#include <CedaFIDData.h>
#include <resource.h>


// Local function prototype
static void ProcessFIDCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaFIDData::CedaFIDData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for FIDDATA
	BEGIN_CEDARECINFO(FIDDATA,FIDDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_CHAR_TRIM	(Beme,"BEME")
		FIELD_CHAR_TRIM	(Bemd,"BEMD")
		FIELD_CHAR_TRIM	(Bet3,"BET3")
		FIELD_CHAR_TRIM	(Bet4,"BET4")
		FIELD_CHAR_TRIM	(Remi,"REMI")
		FIELD_CHAR_TRIM	(Remt,"REMT")
		FIELD_CHAR_TRIM	(Blkc,"BLKC")
		FIELD_CHAR_TRIM	(Conr,"CONR")
	END_CEDARECINFO //(FIDDATA)

	bmRemarkExists = false;	

	// Copy the record structure
	for (int i=0; i< sizeof(FIDDataRecInfo)/sizeof(FIDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FIDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"FID");
	strcpy(pcmFIDFieldList,"CDAT,LSTU,PRFL,URNO,USEC,USEU,VAFR,VATO,CODE,BEME,BEMD,BET3,BET4,REMI,REMT,BLKC,CONR");
	pcmFieldList = pcmFIDFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaFIDData::AppendFields()
{
	if(ogBasicData.DoesFieldExist(pcmTableName,"REMA") == true)
	{	
		if(strstr(pcmFieldList,"REMA") != NULL)
			return;

		bmRemarkExists = true;

		BEGIN_CEDARECINFO(FIDDATA,FIDDataRecInfo)
			FIELD_CHAR_TRIM (Rema,"REMA")
		END_CEDARECINFO

		for (int i=0; i< sizeof(FIDDataRecInfo)/sizeof(FIDDataRecInfo[0]) ; i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&FIDDataRecInfo[0],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}

		strcat(pcmFIDFieldList,",REMA");		
		pcmFieldList = pcmFIDFieldList;
	}
}
//----------------------------------------------------------------------------------------------------

void CedaFIDData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BEME");
	ropFields.Add("BEMD");
	ropFields.Add("BET3");
	ropFields.Add("BET4");
	ropFields.Add("CDAT");
	ropFields.Add("CODE");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("REMI");
	ropFields.Add("REMT");
	ropFields.Add("BLKC");
	ropFields.Add("CONR");

	ropDesription.Add(LoadStg(IDS_STRING37)+CString(" 1"));
	ropDesription.Add(LoadStg(IDS_STRING37)+CString(" 2"));
	ropDesription.Add(LoadStg(IDS_STRING37)+CString(" 3"));
	ropDesription.Add(LoadStg(IDS_STRING37)+CString(" 3"));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING494));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING583));
	ropDesription.Add(LoadStg(IDS_STRING584));
	ropDesription.Add(LoadStg(IDS_STRING585));
	ropDesription.Add(LoadStg(IDS_STRING1018));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

	if(bmRemarkExists == true)
	{
		ropFields.Add("REMA");
		ropDesription.Add(LoadStg(IDS_STRING1129));
		ropType.Add("String");
	}
}

//----------------------------------------------------------------------------------------------------

void CedaFIDData::Register(void)
{
	ogDdx.Register((void *)this,BC_FID_CHANGE,CString("FIDDATA"), CString("FID-changed"),ProcessFIDCf);
	ogDdx.Register((void *)this,BC_FID_DELETE,CString("FIDDATA"), CString("FID-deleted"),ProcessFIDCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaFIDData::~CedaFIDData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaFIDData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaFIDData::Read(char *pcpWhere)
{
    // Select data from the database
	AppendFields();
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		FIDDATA *prpFID = new FIDDATA;
		if ((ilRc = GetFirstBufferRecord(prpFID)) == true)
		{
			prpFID->IsChanged = DATA_UNCHANGED;
			omData.Add(prpFID);//Update omData
			omUrnoMap.SetAt((void *)prpFID->Urno,prpFID);
		}
		else
		{
			delete prpFID;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaFIDData::InsertFID(FIDDATA *prpFID,BOOL bpSendDdx)
{
	prpFID->IsChanged = DATA_NEW;
	if(SaveFID(prpFID) == false) return false; //Update Database
	InsertFIDInternal(prpFID);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaFIDData::InsertFIDInternal(FIDDATA *prpFID)
{
	//PrepareFIDData(prpFID);
	ogDdx.DataChanged((void *)this, FID_CHANGE,(void *)prpFID ); //Update Viewer
	omData.Add(prpFID);//Update omData
	omUrnoMap.SetAt((void *)prpFID->Urno,prpFID);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaFIDData::DeleteFID(long lpUrno)
{
	FIDDATA *prlFID = GetFIDByUrno(lpUrno);
	if (prlFID != NULL)
	{
		prlFID->IsChanged = DATA_DELETED;
		if(SaveFID(prlFID) == false) return false; //Update Database
		DeleteFIDInternal(prlFID);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaFIDData::DeleteFIDInternal(FIDDATA *prpFID)
{
	ogDdx.DataChanged((void *)this,FID_DELETE,(void *)prpFID); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpFID->Urno);
	int ilFIDCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilFIDCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpFID->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaFIDData::PrepareFIDData(FIDDATA *prpFID)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaFIDData::UpdateFID(FIDDATA *prpFID,BOOL bpSendDdx)
{
	if (GetFIDByUrno(prpFID->Urno) != NULL)
	{
		if (prpFID->IsChanged == DATA_UNCHANGED)
		{
			prpFID->IsChanged = DATA_CHANGED;
		}
		if(SaveFID(prpFID) == false) return false; //Update Database
		UpdateFIDInternal(prpFID);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaFIDData::UpdateFIDInternal(FIDDATA *prpFID)
{
	FIDDATA *prlFID = GetFIDByUrno(prpFID->Urno);
	if (prlFID != NULL)
	{
		*prlFID = *prpFID; //Update omData
		ogDdx.DataChanged((void *)this,FID_CHANGE,(void *)prlFID); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

FIDDATA *CedaFIDData::GetFIDByUrno(long lpUrno)
{
	FIDDATA  *prlFID;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFID) == TRUE)
	{
		return prlFID;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaFIDData::ReadSpecialData(CCSPtrArray<FIDDATA> *popFid,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","FID",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","FID",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popFid != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			FIDDATA *prpFid = new FIDDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpFid,CString(pclFieldList))) == true)
			{
				popFid->Add(prpFid);
			}
			else
			{
				delete prpFid;
			}
		}
		if(popFid->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaFIDData::SaveFID(FIDDATA *prpFID)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpFID->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpFID->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpFID);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpFID->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFID->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpFID);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpFID->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFID->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessFIDCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_FID_CHANGE :
	case BC_FID_DELETE :
		((CedaFIDData *)popInstance)->ProcessFIDBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaFIDData::ProcessFIDBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFIDData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlFIDData->Selection);

	FIDDATA *prlFID = GetFIDByUrno(llUrno);
	if(ipDDXType == BC_FID_CHANGE)
	{
		if (prlFID != NULL)
		{
			GetRecordFromItemList(prlFID,prlFIDData->Fields,prlFIDData->Data);
			if(ValidateFIDBcData(prlFID->Urno)) //Prf: 8795
			{
				UpdateFIDInternal(prlFID);
			}
			else
			{
				DeleteFIDInternal(prlFID);
			}
		}
		else
		{
			prlFID = new FIDDATA;
			GetRecordFromItemList(prlFID,prlFIDData->Fields,prlFIDData->Data);
			if(ValidateFIDBcData(prlFID->Urno)) //Prf: 8795
			{
				InsertFIDInternal(prlFID);
			}
			else
			{
				delete prlFID;
			}
		}
	}
	if(ipDDXType == BC_FID_DELETE)
	{
		if (prlFID != NULL)
		{
			DeleteFIDInternal(prlFID);
		}
	}
}

//Prf: 8795
//--ValidateFIDBcData--------------------------------------------------------------------------------------

bool CedaFIDData::ValidateFIDBcData(const long& lrpUrno)
{
	bool blValidateFidBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<FIDDATA> olFids;
		if(!ReadSpecialData(&olFids,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateFidBcData = false;
		}
	}
	return blValidateFidBcData;
}

//---------------------------------------------------------------------------------------------------------
