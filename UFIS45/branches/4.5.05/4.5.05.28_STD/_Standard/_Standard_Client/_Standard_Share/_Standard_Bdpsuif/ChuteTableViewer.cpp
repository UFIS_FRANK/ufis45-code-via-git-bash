// ChuteTableViewer.cpp
//

#include <stdafx.h>
#include <ChuteTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ChuteTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ChuteTableViewer
//

ChuteTableViewer::ChuteTableViewer(CCSPtrArray<CHUDATA> *popData)
{
	pomData = popData;	
    pomChuteTable = NULL;
    ogDdx.Register(this, CHU_CHANGE, CString("CHUTETABLEVIEWER"), CString("Chute Update/new"), ChuteTableCf);
    ogDdx.Register(this, CHU_DELETE, CString("CHUTETABLEVIEWER"), CString("Chute Delete"), ChuteTableCf);
}

//-----------------------------------------------------------------------------------------------

ChuteTableViewer::~ChuteTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::Attach(CCSTable *popTable)
{
    pomChuteTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();
    MakeLines();
	UpdateChute();
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::MakeLines()
{
	int ilChuteCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilChuteCount; ilLc++)
	{
		CHUDATA *prlChuteData = &pomData->GetAt(ilLc);
		MakeLine(prlChuteData);
	}
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::MakeLine(CHUDATA *prpChute)
{

    // Update viewer data for this shift record
    CHUTETABLE_LINEDATA rlChute;
	rlChute.Urno = prpChute->Urno;
	rlChute.Cnam = prpChute->Cnam;
	rlChute.Term = prpChute->Term;
	rlChute.Sort = prpChute->Sort;
	rlChute.Maxf = prpChute->Maxf;
	rlChute.Vafr = prpChute->Vafr.Format("%d.%m.%Y %H:%M");
	rlChute.Vato = prpChute->Vato.Format("%d.%m.%Y %H:%M");
	CreateLine(&rlChute);
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::CreateLine(CHUTETABLE_LINEDATA *prpChute)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	{
        if (CompareChute(prpChute, &omLines[ilLineno-1]) >= 0)
		{
	        break;
		}
	}
	CHUTETABLE_LINEDATA rlChute;
	rlChute = *prpChute;
    omLines.NewAt(ilLineno, rlChute);
}

//-----------------------------------------------------------------------------------------------
// UpdateChute: Load data selected by filter conditions to the Chute by using "omTable"

void ChuteTableViewer::UpdateChute()
{
	int ilLines = omLines.GetSize();	
	omHeaderDataArray.DeleteAll();

	pomChuteTable->SetShowSelection(TRUE);
	pomChuteTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olHeader(LoadStg(IDS_STRING1158));

	int ilPos = 1;
	rlHeader.Length = 50;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader); //Chute Name

	rlHeader.Length = 50;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Terminal

	rlHeader.Length = 50;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Sorter

	rlHeader.Length = 100;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Max. Flights

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader); // Valid from

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader); // Valid to

	pomChuteTable->SetHeaderFields(omHeaderDataArray);
	pomChuteTable->SetDefaultSeparator();
	pomChuteTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Cnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Sort;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Maxf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomChuteTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomChuteTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

static void ChuteTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ChuteTableViewer *polViewer = (ChuteTableViewer *)popInstance;
    if (ipDDXType == CHU_CHANGE)
		polViewer->ProcessChuteChange((CHUDATA *)vpDataPointer);
    else if (ipDDXType == CHU_DELETE)
		polViewer->ProcessChuteDelete((CHUDATA *)vpDataPointer);
}


//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::ProcessChuteChange(CHUDATA *prpChute)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpChute->Cnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChute->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChute->Sort;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChute->Maxf;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChute->Vafr.Format("%d.%m.%Y %H:%M");;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChute->Vato.Format("%d.%m.%Y %H:%M");;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpChute->Urno, ilItem))
	{
        CHUTETABLE_LINEDATA *prlLine = &omLines[ilItem];
		prlLine->Urno = prpChute->Urno;
		prlLine->Cnam = prpChute->Cnam;
		prlLine->Term = prpChute->Term;
		prlLine->Sort = prpChute->Sort;
		prlLine->Maxf = prpChute->Maxf;
		prlLine->Vafr = prpChute->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpChute->Vato.Format("%d.%m.%Y %H:%M");
		pomChuteTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomChuteTable->DisplayTable();
	}
	else
	{
		MakeLine(prpChute);
		if (FindLine(prpChute->Urno, ilItem))
		{
	        CHUTETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomChuteTable->AddTextLine(olLine, (void *)prlLine);
				pomChuteTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::ProcessChuteDelete(CHUDATA *prpChute)
{
	int ilItem;
	if (FindLine(prpChute->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomChuteTable->DeleteTextLine(ilItem);
		pomChuteTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

int ChuteTableViewer::CompareChute(CHUTETABLE_LINEDATA *prpChute1, CHUTETABLE_LINEDATA *prpChute2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ChuteTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}

//-----------------------------------------------------------------------------------------------
void ChuteTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ChuteTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING1157);
	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ )
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ChuteTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ChuteTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,CHUTETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Cnam;
			}
			break;
		case 1:
			{
				rlElement.Text		= prpLine->Term;
			}
			break;
		case 2:
			{
				rlElement.Text		= prpLine->Sort;
			}
			break;
		case 3:
			{
				rlElement.Text		= prpLine->Maxf;
			}
			break;
		case 4:
			{
				rlElement.Text		= prpLine->Vafr;
			}
			break;
		case 5:
			{
				rlElement.Text		= prpLine->Vato;
			}
			break;
		default :
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}
