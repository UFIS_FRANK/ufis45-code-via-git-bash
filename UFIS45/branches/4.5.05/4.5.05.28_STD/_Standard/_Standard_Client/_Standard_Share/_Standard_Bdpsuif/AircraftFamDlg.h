#if !defined(AFX_AIRCRAFTDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_AIRCRAFTDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AircraftFamDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AircraftFamDlg dialog
//#include "CedaSphData.h"
#include <CCSEdit.h>
#include <CedaAFMData.h>

class AircraftFamDlg : public CDialog
{
// Construction
public:
	AircraftFamDlg(AFMDATA *popAfm, CWnd* pParent = NULL);   // standard constructor

	AFMDATA *pomAfm;
// Dialog Data
	//{{AFX_DATA(AircraftFamDlg)
	enum { IDD = IDD_AIRCRAFTTYP };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_AFMC;
	CCSEdit	m_ANAM;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AircraftFamDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AircraftFamDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRCRAFTDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
