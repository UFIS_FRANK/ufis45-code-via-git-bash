#if !defined(AFX_GATPOSNOTAVAILABLEDLG_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_)
#define AFX_GATPOSNOTAVAILABLEDLG_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosNotAvailableDlg.h : header file
//

#include <NotAvailableDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg dialog

class GatPosNotAvailableDlg : public NotAvailableDlg
{
// Construction
public:
	GatPosNotAvailableDlg(BLKDATA *popBlk,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GatPosNotAvailableDlg)
	enum { IDD = IDD_GATPOS_NOTAVAILABLEDLG };
		// NOTE: the ClassWizard will add data members here
	AatBitmapComboBox	m_BlockingBitmap;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosNotAvailableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosNotAvailableDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnRadioLocal();
	afx_msg void OnRadioUtc();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSNOTAVAILABLEDLG_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_)
