#ifndef __HANDLINGAGENTTABLEVIEWER_H__
#define __HANDLINGAGENTTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaHAGData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct HANDLINGAGENTTABLE_LINEDATA
{
	long	Urno;
	CString	Hsna;
	CString	Hnam;
	CString	Tele;
	CString	Faxn;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// HandlingagentTableViewer

class HandlingagentTableViewer : public CViewer
{
// Constructions
public:
    HandlingagentTableViewer(CCSPtrArray<HAGDATA> *popData);
    ~HandlingagentTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(HAGDATA *prpHandlingagent);
	int CompareHandlingagent(HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent1, HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent2);
    void MakeLines();
	void MakeLine(HAGDATA *prpHandlingagent);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(HANDLINGAGENTTABLE_LINEDATA *prpLine);
	void ProcessHandlingagentChange(HAGDATA *prpHandlingagent);
	void ProcessHandlingagentDelete(HAGDATA *prpHandlingagent);
	bool FindHandlingagent(char *prpHandlingagentKeya, char *prpHandlingagentKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomHandlingagentTable;
	CCSPtrArray<HAGDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<HANDLINGAGENTTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(HANDLINGAGENTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__HANDLINGAGENTTABLEVIEWER_H__
