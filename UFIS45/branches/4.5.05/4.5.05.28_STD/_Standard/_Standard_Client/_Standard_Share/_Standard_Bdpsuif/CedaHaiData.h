// CedaHaiData.h

#ifndef __CEDAHAIDATA__
#define __CEDAHAIDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct HAIDATA 
{
	char 	 Hsna[6]; 	// Handling Agent
	char 	 Task[130]; 	// Abfertigungsart
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	long 	 Altu; 		// Urno of ALTTAB


	HAIDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end HaiDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaHaiData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omAltuMap;

    CCSPtrArray<HAIDATA> omData;

	char pcmHaiFieldList[512];

// Operations
public:
    CedaHaiData();
	~CedaHaiData();

	void Register(void);
	void ClearAll(void);

    bool Read(const char *pcpWhere = NULL);
	bool InsertHaiInternal(HAIDATA *prpHai);
	HAIDATA  *GetHaiByAltu(long lpUrno);

	// Private methods
private:
	CString omWhere; //Prf: 8795
};

extern CedaHaiData ogHaiData;
//---------------------------------------------------------------------------------------------------------

#endif //__CEDAHAIDATA__
