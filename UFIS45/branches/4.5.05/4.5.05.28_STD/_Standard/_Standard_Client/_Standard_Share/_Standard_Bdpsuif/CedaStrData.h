// CedaStrData.h

#ifndef __CEDASTRDATA__
#define __CEDASTRDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct STRDATA 
{
	char 	 Act3[5]; 	// Flugzeugtyp
	long 	 Actm;	 	// Flugzeugtyp Gruppe (URNO)
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Daya[9]; 	// Verkehrstage Ankunft
	char 	 Dayd[9]; 	// Verkehrstage Abflug
	char 	 Flca[5]; 	// Flugnummer Ankunft Code
	char 	 Flcd[5]; 	// Flugnummer Abflug Code
	char 	 Flna[7]; 	// Flugnummer Ankunft Nummer
	char 	 Flnd[7]; 	// Flugnummer Abflug Nummer
	char 	 Flsa[3]; 	// Flugnummer Ankunft Suffix
	char 	 Flsd[3]; 	// Flugnummer Abflug Suffix
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis


	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	STRDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
	}

}; // end StrDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaStrData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STRDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaStrData();
	~CedaStrData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STRDATA *prpStr);
	bool InsertInternal(STRDATA *prpStr);
	bool Update(STRDATA *prpStr);
	bool UpdateInternal(STRDATA *prpStr);
	bool Delete(long lpUrno);
	bool DeleteInternal(STRDATA *prpStr);
	bool ReadSpecialData(CCSPtrArray<STRDATA> *popStr,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STRDATA *prpStr);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STRDATA  *GetStrByUrno(long lpUrno);


	// Private methods
private:
    void PrepareStrData(STRDATA *prpStrData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDASTRDATA__
