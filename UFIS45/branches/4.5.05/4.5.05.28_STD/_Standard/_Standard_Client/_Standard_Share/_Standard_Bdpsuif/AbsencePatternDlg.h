#if !defined(AFX_ABSENCEPATTERNDLG_H__AB545C03_B315_11D5_9129_0050DADD7302__INCLUDED_)
#define AFX_ABSENCEPATTERNDLG_H__AB545C03_B315_11D5_9129_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AbsencePatternDlg.h : header file
//
#include "CedaMawData.h"
#include "CedaMaaData.h"
#include "GridControl.h"
#include "CCSEdit.h"

class CGridControl;

#define  ABSCOLCOUNT 3

/////////////////////////////////////////////////////////////////////////////
// AbsencePatternDlg dialog

class AbsencePatternDlg : public CDialog
{
// Construction
public:
	AbsencePatternDlg(MAWDATA *popMaw, CCSPtrArray<MAADATA> *popMaaData, CWnd* pParent = NULL);   // standard constructor
	~AbsencePatternDlg();

	MAWDATA *pomMaw;

	CCSPtrArray<MAADATA> *pomMaaData;

// Dialog Data
	//{{AFX_DATA(AbsencePatternDlg)
	enum { IDD = IDD_ABSENCEPATTERN_DLG };
	CButton	m_Button_OK;
	CButton	m_Button_New;
	CButton	m_Button_Delete;
	CButton	m_Button_Change;
	CCSEdit	m_NAME;
	CCSEdit	m_DFLT;
	CString	m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AbsencePatternDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	CGridControl *pomAbsenceGrid;
	void FillAbsenceGrid();
	void InitAbsenceColWidths();
	void InitAbsenceGrid();

protected:

	// Generated message map functions
	//{{AFX_MSG(AbsencePatternDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButtonChange();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonNew();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABSENCEPATTERNDLG_H__AB545C03_B315_11D5_9129_0050DADD7302__INCLUDED_)
