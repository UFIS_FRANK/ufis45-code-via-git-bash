#ifndef __DiensteUndAbwesenheitenTableViewer_H__
#define __DiensteUndAbwesenheitenTableViewer_H__

#include <stdafx.h>
#include <CedaOdaData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct DIENSTEUNDABWESENHEITENTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString	 Sdas; 	// SAP-Code
	CString	 Sdac; 	// Code
	CString	 Sdan; 	// Bezeichnung
	CString	 Sdak; 	// Kurzbezeichnung
	//uhi 23.3.01
	//CString	 Ctrc; 	// Vertragsart.code
	//CString	 Cthg; 	// Vac. Reduction.code
	//CString	 Dptc;  // Organisationseinheit,code
	
	CString  Abfr;	// Von Uhrzeit
	CString  Abto;  // Bis Uhrzeit
	CString  Blen;  // Pausenlšnge
	//CString	 Dura; 	// Dauer (Ersatzstunden)
	CString	 Rema; 	// Bemerkungen

};

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenTableViewer

	  
class DiensteUndAbwesenheitenTableViewer : public CViewer
{
// Constructions
public:
    DiensteUndAbwesenheitenTableViewer(CCSPtrArray<ODADATA> *popData);
    ~DiensteUndAbwesenheitenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ODADATA *prpDiensteUndAbwesenheiten);
	int CompareDiensteUndAbwesenheiten(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten1, DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten2);
    void MakeLines();
	void MakeLine(ODADATA *prpDiensteUndAbwesenheiten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpLine);
	void ProcessDiensteUndAbwesenheitenChange(ODADATA *prpDiensteUndAbwesenheiten);
	void ProcessDiensteUndAbwesenheitenDelete(ODADATA *prpDiensteUndAbwesenheiten);
	bool FindDiensteUndAbwesenheiten(char *prpDiensteUndAbwesenheitenKeya, char *prpDiensteUndAbwesenheitenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomDiensteUndAbwesenheitenTable;
	CCSPtrArray<ODADATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<DIENSTEUNDABWESENHEITENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__DiensteUndAbwesenheitenTableViewer_H__
