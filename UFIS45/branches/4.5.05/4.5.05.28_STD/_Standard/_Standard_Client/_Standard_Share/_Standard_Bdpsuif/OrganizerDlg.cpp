// OrganizerDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <OrganizerDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld OrganizerDlg 


OrganizerDlg::OrganizerDlg(CHTDATA *popCht,CWnd* pParent): CDialog(OrganizerDlg::IDD, pParent)
{
	pomCht = popCht;
	//{{AFX_DATA_INIT(OrganizerDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void OrganizerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OrganizerDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_CHTC,	 m_CHTC);
	DDX_Control(pDX, IDC_CHTN,	 m_CHTN);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OrganizerDlg, CDialog)
	//{{AFX_MSG_MAP(OrganizerDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Zuordnungsmakros f�r Nachrichten ein
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten OrganizerDlg 
//----------------------------------------------------------------------------------------

BOOL OrganizerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING193) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ORGANIZERDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCht->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCht->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCht->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCht->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCht->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCht->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CHTC.SetFormat("x|#x|#x|#");
	m_CHTC.SetTextLimit(1,3);
	m_CHTC.SetBKColor(YELLOW);  
	m_CHTC.SetTextErrColor(RED);
	m_CHTC.SetInitText(pomCht->Chtc);
	//m_CHTC.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_CHTC"));
	//------------------------------------
	m_CHTN.SetTypeToString("X(60)",60,1);
	m_CHTN.SetBKColor(YELLOW);  
	m_CHTN.SetTextErrColor(RED);
	m_CHTN.SetInitText(pomCht->Chtn);
	//m_CHTN.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_CHTN"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomCht->Vafr.Format("%d.%m.%Y"));
	//m_VAFRD.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomCht->Vafr.Format("%H:%M"));
	//m_VAFRT.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate(true);
	m_VATOD.SetTextErrColor(RED);
	//m_VATOD.SetBKColor(YELLOW);
	m_VATOD.SetInitText(pomCht->Vato.Format("%d.%m.%Y"));
	//m_VATOD.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime(true);
	m_VATOT.SetTextErrColor(RED);
	//m_VATOT.SetBKColor(YELLOW);
	m_VATOT.SetInitText(pomCht->Vato.Format("%H:%M"));
	//m_VATOT.SetSecState(ogPrivList.GetStat("ORGANIZERDLG.m_VATO"));
	//------------------------------------

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void OrganizerDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CHTC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CHTC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_CHTN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CHTN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING93) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING93) + ogNotFormat;
		}
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	//~~~~~ 13.08.99 SHA 
	//*** VALID TO KEIN MANDATORY FIELD ! ***
	/*
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VATOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VATOT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}*/ 

	///////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olChtc;
		char clWhere[100];
		CCSPtrArray<CHTDATA> olChtCPA;

		m_CHTC.GetWindowText(olChtc);
		sprintf(clWhere,"WHERE CHTC='%s'",olChtc);
		if(ogChtData.ReadSpecialData(&olChtCPA,clWhere,"URNO,CHTC",false) == true)
		{
			if(olChtCPA[0].Urno != pomCht->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olChtCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_CHTC.GetWindowText(pomCht->Chtc,4);
		m_CHTN.GetWindowText(pomCht->Chtn,61);
		pomCht->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomCht->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CHTC.SetFocus();
		m_CHTC.SetSel(0,-1);
  }

}

//----------------------------------------------------------------------------------------


