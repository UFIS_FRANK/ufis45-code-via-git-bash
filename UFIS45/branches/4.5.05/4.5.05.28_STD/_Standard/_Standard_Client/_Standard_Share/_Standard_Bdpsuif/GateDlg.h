#if !defined(AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GateDlg.h : header file
//
#include <CedaGATData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CedaBlkData.h>
#include <CedaDatData.h>

/////////////////////////////////////////////////////////////////////////////
// GateDlg dialog

class GateDlg : public CDialog
{
// Construction
public:
	GateDlg(GATDATA *popGAT,CWnd* pParent = NULL);   // standard constructor
	~GateDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;
	CCSPtrArray<DATDATA> omDatPtrA;
	CCSPtrArray<DATDATA> omDeleteDatPtrA;

// Dialog Data
	//{{AFX_DATA(GateDlg)
	enum { IDD = IDD_GATEDLG };
	CCSEdit	m_DEFD;
	CCSEdit	m_TEL4;
	CCSEdit	m_TEL3;
	CCSEdit	m_TEL2;
	CButton	m_OK;
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CButton	m_BUSG;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_GNAM;
	CCSEdit	m_GRUP;
	CCSEdit	m_NOBR;
	CCSEdit	m_RBAB;
	CCSEdit	m_RGA1;
	CCSEdit	m_RGA2;
	CCSEdit	m_TELE;
	CCSEdit	m_TERM;
	CCSEdit	m_HOME;
	CCSEdit	m_GTID;
	CCSEdit	m_GTYP;
	CButton	m_UTC;
	CButton	m_LOCAL;
	CButton	m_APIS;
	CButton m_ABPR;
	CButton m_FIDS;
	CCSEdit m_GCAP;
	CCSEdit m_NOST;
	CCSEdit m_NOBP;
	CCSEdit m_NOXM;
	CCSEdit m_DCST;
//	CButton	m_FCOM;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GateDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

	GATDATA		*pomGAT;

	CCSTable	*pomTable;
	int			imLastSelection;

	virtual	void MakeNoavTable(CString opTabn, CString opBurn);
	virtual	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
	virtual	void UpdateNoavTable();

	GateDlg(GATDATA *popGAT,int ipDlg,CWnd* pParent = NULL);   // standard constructor
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_)
