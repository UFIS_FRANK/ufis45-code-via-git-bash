#ifndef __ReduktionenTableViewer_H__
#define __ReduktionenTableViewer_H__

#include <stdafx.h>
#include <CedaPrcData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct REDUKTIONENTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Redc; 	// Kurzname Reduktion
	CString  Redl;	//Level
	CString  Redn; 	// Bezeichnung Reduktion

};

/////////////////////////////////////////////////////////////////////////////
// ReduktionenTableViewer

	  
class ReduktionenTableViewer : public CViewer
{
// Constructions
public:
    ReduktionenTableViewer(CCSPtrArray<PRCDATA> *popData);
    ~ReduktionenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PRCDATA *prpReduktionen);
	int CompareReduktionen(REDUKTIONENTABLE_LINEDATA *prpReduktionen1, REDUKTIONENTABLE_LINEDATA *prpReduktionen2);
    void MakeLines();
	void MakeLine(PRCDATA *prpReduktionen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(REDUKTIONENTABLE_LINEDATA *prpReduktionen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(REDUKTIONENTABLE_LINEDATA *prpLine);
	void ProcessReduktionenChange(PRCDATA *prpReduktionen);
	void ProcessReduktionenDelete(PRCDATA *prpReduktionen);
	bool FindReduktionen(char *prpReduktionenKeya, char *prpReduktionenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomReduktionenTable;
	CCSPtrArray<PRCDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<REDUKTIONENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(REDUKTIONENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ReduktionenTableViewer_H__
