// CedaDSCData.cpp
 
#include <stdafx.h>
#include <CedaDSCData.h>
#include <CedaDPTData.h>
#include <resource.h>
#include <CedaBasicData.h>


// Local function prototype
static void ProcessDSCCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDSCData::CedaDSCData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for DSCDATA
	BEGIN_CEDARECINFO(DSCDATA,DSCDATARecInfo)
		FIELD_LONG		(Urno,"URNO")		
		FIELD_CHAR_TRIM	(Hopo,"HOPO")		
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Devn,"DEVN")
		FIELD_CHAR_TRIM (Vafr,"VAFR")
		FIELD_CHAR_TRIM (Vato,"VATO")
	END_CEDARECINFO //(DSCDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(DSCDATARecInfo)/sizeof(DSCDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DSCDATARecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"DSC");	
	strcpy(pcmDSCFieldList,"URNO,HOPO,USEC,USEU,CDAT,LSTU,DEVN,VAFR,VATO");
	pcmFieldList = pcmDSCFieldList;
	omData.SetSize(0,1000);	
}

//---------------------------------------------------------------------------------------------------

void CedaDSCData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
	ropFields.Add("HOPO"); ropDesription.Add(LoadStg(IDS_STRING711)); ropType.Add("String");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("DEVN"); ropDesription.Add(LoadStg(IDS_STRING935)); ropType.Add("String");
	ropFields.Add("VAFR"); ropDesription.Add(LoadStg(IDS_STRING230)); ropType.Add("String");
	ropFields.Add("VATO"); ropDesription.Add(LoadStg(IDS_STRING231)); ropType.Add("String");

}

//-----------------------------------------------------------------------------------------------

void CedaDSCData::Register(void)
{
	ogDdx.Register((void *)this,BC_DSC_CHANGE,CString("DSCDATA"), CString("DSC-changed"),ProcessDSCCf);
	ogDdx.Register((void *)this,BC_DSC_DELETE,CString("DSCDATA"), CString("DSC-deleted"),ProcessDSCCf);	
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDSCData::~CedaDSCData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDSCData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDSCData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DSCDATA *prpDSC = new DSCDATA;
		if ((ilRc = GetFirstBufferRecord(prpDSC)) == true)
		{
			prpDSC->IsChanged = DATA_UNCHANGED;
			omData.Add(prpDSC);//Update omData
			omUrnoMap.SetAt((void *)prpDSC->Urno,prpDSC);
		}
		else
		{
			delete prpDSC;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDSCData::InsertDSC(DSCDATA *prpDSC,BOOL bpSendDdx)
{
	prpDSC->IsChanged = DATA_NEW;
	if (SaveDSC(prpDSC) == false) 
		return false; //Update Database
	InsertDSCInternal(prpDSC);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaDSCData::InsertDSCInternal(DSCDATA *prpDSC)
{
	//PrepareDSCDATA(prpDSC);
	ogDdx.DataChanged((void *)this, DSC_CHANGE,(void *)prpDSC ); //Update Viewer
	omData.Add(prpDSC);//Update omData
	omUrnoMap.SetAt((void *)prpDSC->Urno,prpDSC);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDSCData::DeleteDSC(long lpUrno)
{
	DSCDATA *prlDSC = GetDSCByUrno(lpUrno);
	if (prlDSC != NULL)
	{
		prlDSC->IsChanged = DATA_DELETED;
		if(SaveDSC(prlDSC) == false) return false; //Update Database
		DeleteDSCInternal(prlDSC);

		char pclWhere[100];
		sprintf(pclWhere,"WHERE RURN = '%ld'",lpUrno);
		DPTDATA* polDPTData;
		CCSPtrArray<DPTDATA> olDPTDataArray;
		ogDPTData.ReadSpecialData(&olDPTDataArray,pclWhere,"URNO",false);
		for(int i = 0 ; i < olDPTDataArray.GetSize(); i++)
		{
			polDPTData = &olDPTDataArray[i];
			ogDPTData.DeleteDPT(polDPTData->Urno);
		}
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDSCData::DeleteDSCInternal(DSCDATA *prpDSC)
{
	ogDdx.DataChanged((void *)this,DSC_DELETE,(void *)prpDSC); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpDSC->Urno);
	int ilDSCCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDSCCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDSC->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaDSCData::PrepareDSCData(DSCDATA *prpDSC)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDSCData::UpdateDSC(DSCDATA *prpDSC,BOOL bpSendDdx)
{
	if (GetDSCByUrno(prpDSC->Urno) != NULL)
	{
		if (prpDSC->IsChanged == DATA_UNCHANGED)
		{
			prpDSC->IsChanged = DATA_CHANGED;
		}
		if(SaveDSC(prpDSC) == false) return false; //Update Database
		UpdateDSCInternal(prpDSC);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDSCData::UpdateDSCInternal(DSCDATA *prpDSC)
{
	DSCDATA *prlDSC = GetDSCByUrno(prpDSC->Urno);
	if (prlDSC != NULL)
	{
		*prlDSC = *prpDSC; //Update omData
		ogDdx.DataChanged((void *)this,DSC_CHANGE,(void *)prlDSC); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DSCDATA *CedaDSCData::GetDSCByUrno(long lpUrno)
{
	DSCDATA  *prlDSC;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDSC) == TRUE)
	{
		return prlDSC;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaDSCData::ReadSpecialData(CCSPtrArray<DSCDATA> *popDSCDataArray,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","DSC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","DSC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popDSCDataArray != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DSCDATA *prpDSC = new DSCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDSC,CString(pclFieldList))) == true)
			{
				popDSCDataArray->Add(prpDSC);
			}
			else
			{
				delete prpDSC;
			}
		}
		if(popDSCDataArray->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaDSCData::SaveDSC(DSCDATA *prpDSC)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpDSC->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDSC->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDSC);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDSC->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDSC->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDSC);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDSC->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDSC->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDSCCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DSC_CHANGE :
	case BC_DSC_DELETE :
		((CedaDSCData *)popInstance)->ProcessDSCBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDSCData::ProcessDSCBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDSCData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDSCData->Selection);

	DSCDATA *prlDSC = GetDSCByUrno(llUrno);
	if(ipDDXType == BC_DSC_CHANGE)
	{
		if(prlDSC != NULL)
		{
			GetRecordFromItemList(prlDSC,prlDSCData->Fields,prlDSCData->Data);
			if(ValidateDSCBcData(prlDSC->Urno))
			{
				UpdateDSCInternal(prlDSC);
			}
			else
			{
				DeleteDSCInternal(prlDSC);
			}
		}
		else
		{
			prlDSC = new DSCDATA;
			GetRecordFromItemList(prlDSC,prlDSCData->Fields,prlDSCData->Data);
			if(ValidateDSCBcData(prlDSC->Urno))
			{
				InsertDSCInternal(prlDSC);
			}
			else
			{
				delete prlDSC;
			}
		}
	}
	if(ipDDXType == BC_DSC_DELETE)
	{
		if (prlDSC != NULL)
		{
			DeleteDSCInternal(prlDSC);
		}
	}
}

//--ValidateDSCBcData--------------------------------------------------------------------------------------

bool CedaDSCData::ValidateDSCBcData(const long& lrpUrno)
{
	bool blValidateDSCBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DSCDATA> olDSCs;
		if(!ReadSpecialData(&olDSCs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateDSCBcData = false;
		}
	}
	return blValidateDSCBcData;
}
//---------------------------------------------------------------------------------------------------------
