// FunktionenTableViewer.cpp 
//

#include <stdafx.h>
#include <FunktionenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void FunktionenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// FunktionenTableViewer
//

FunktionenTableViewer::FunktionenTableViewer(CCSPtrArray<PFCDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomFunktionenTable = NULL;
    ogDdx.Register(this, PFC_CHANGE, CString("FUNKTIONENTABLEVIEWER"), CString("Funktionen Update"), FunktionenTableCf);
    ogDdx.Register(this, PFC_NEW,    CString("FUNKTIONENTABLEVIEWER"), CString("Funktionen New"),    FunktionenTableCf);
    ogDdx.Register(this, PFC_DELETE, CString("FUNKTIONENTABLEVIEWER"), CString("Funktionen Delete"), FunktionenTableCf);
}

//-----------------------------------------------------------------------------------------------

FunktionenTableViewer::~FunktionenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::Attach(CCSTable *popTable)
{
    pomFunktionenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::MakeLines()
{
	int ilFunktionenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilFunktionenCount; ilLc++)
	{
		PFCDATA *prlFunktionenData = &pomData->GetAt(ilLc);
		MakeLine(prlFunktionenData);
	}
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::MakeLine(PFCDATA *prpFunktionen)
{

    //if( !IsPassFilter(prpFunktionen)) return;

    // Update viewer data for this shift record
    FUNKTIONENTABLE_LINEDATA rlFunktionen;

	rlFunktionen.Urno = prpFunktionen->Urno;
	rlFunktionen.Dptc = prpFunktionen->Dptc;
	rlFunktionen.Fctc = prpFunktionen->Fctc;
	rlFunktionen.Fctn = prpFunktionen->Fctn;
	//uhi 11.5.01
	//rlFunktionen.Prio = prpFunktionen->Prio;
	rlFunktionen.Prfl = prpFunktionen->Prfl;
	rlFunktionen.Rema = prpFunktionen->Rema;

	CreateLine(&rlFunktionen);
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::CreateLine(FUNKTIONENTABLE_LINEDATA *prpFunktionen)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFunktionen(prpFunktionen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	FUNKTIONENTABLE_LINEDATA rlFunktionen;
	rlFunktionen = *prpFunktionen;
    omLines.NewAt(ilLineno, rlFunktionen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FunktionenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFunktionenTable->SetShowSelection(TRUE);
	pomFunktionenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 42;	// Code 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING269),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332;	// Identifier
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING269),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60;	// Org
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING269),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//uhi 11.5.01
	/*rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING269),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);*/
	rlHeader.Length = 496; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING269),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomFunktionenTable->SetHeaderFields(omHeaderDataArray);
	pomFunktionenTable->SetDefaultSeparator();
	pomFunktionenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Fctc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fctn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dptc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//Prio 11.5.01
		/*rlColumnData.Text = omLines[ilLineNo].Prio;
		olColList.NewAt(olColList.GetSize(), rlColumnData);*/
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomFunktionenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFunktionenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString FunktionenTableViewer::Format(FUNKTIONENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool FunktionenTableViewer::FindFunktionen(char *pcpFunktionenKeya, char *pcpFunktionenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpFunktionenKeya) &&
			 (omLines[ilItem].Keyd == pcpFunktionenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void FunktionenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FunktionenTableViewer *polViewer = (FunktionenTableViewer *)popInstance;
    if (ipDDXType == PFC_CHANGE || ipDDXType == PFC_NEW) polViewer->ProcessFunktionenChange((PFCDATA *)vpDataPointer);
    if (ipDDXType == PFC_DELETE) polViewer->ProcessFunktionenDelete((PFCDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::ProcessFunktionenChange(PFCDATA *prpFunktionen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpFunktionen->Fctc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFunktionen->Fctn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFunktionen->Dptc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//uhi 11.5.01
	/*rlColumn.Text = prpFunktionen->Prio;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);*/
	rlColumn.Text = prpFunktionen->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpFunktionen->Urno, ilItem))
	{
        FUNKTIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpFunktionen->Urno;
		prlLine->Dptc = prpFunktionen->Dptc;
		prlLine->Fctc = prpFunktionen->Fctc;
		prlLine->Fctn = prpFunktionen->Fctn;
		//uhi 11.5.01
		//prlLine->Prio = prpFunktionen->Prio;
		prlLine->Rema = prpFunktionen->Rema;

		pomFunktionenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomFunktionenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpFunktionen);
		if (FindLine(prpFunktionen->Urno, ilItem))
		{
	        FUNKTIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomFunktionenTable->AddTextLine(olLine, (void *)prlLine);
				pomFunktionenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::ProcessFunktionenDelete(PFCDATA *prpFunktionen)
{
	int ilItem;
	if (FindLine(prpFunktionen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomFunktionenTable->DeleteTextLine(ilItem);
		pomFunktionenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool FunktionenTableViewer::IsPassFilter(PFCDATA *prpFunktionen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int FunktionenTableViewer::CompareFunktionen(FUNKTIONENTABLE_LINEDATA *prpFunktionen1, FUNKTIONENTABLE_LINEDATA *prpFunktionen2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpFunktionen1->Tifd == prpFunktionen2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpFunktionen1->Tifd > prpFunktionen2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool FunktionenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FunktionenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING177);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FunktionenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FunktionenTableViewer::PrintTableLine(FUNKTIONENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Fctc;
				break;
			case 1:
				rlElement.Text		= prpLine->Fctn;
				break;
			case 2:
				rlElement.Text		= prpLine->Dptc;
				break;
			//uhi 11.5.01
			/*case 3:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Prio;
				break;*/
			case 3:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
