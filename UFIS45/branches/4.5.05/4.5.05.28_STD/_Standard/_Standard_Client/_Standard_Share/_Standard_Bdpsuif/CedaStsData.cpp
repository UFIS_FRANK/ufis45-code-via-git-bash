// CedaStsData.cpp
 
#include <stdafx.h>
#include <CedaStsData.h>
#include <resource.h>


void ProcessStsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaStsData::CedaStsData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STSDataStruct
	BEGIN_CEDARECINFO(STSDATA,STSDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Stsc,"STSC")
		FIELD_CHAR_TRIM	(Stsd,"STSD")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(STSDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(STSDataRecInfo)/sizeof(STSDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STSDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STS");
	strcpy(pcmListOfFields,"CDAT,LSTU,REMA,STSC,STSD,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaStsData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("REMA");
	ropFields.Add("STSC");
	ropFields.Add("STSD");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING805));
	ropDesription.Add(LoadStg(IDS_STRING817));
	ropDesription.Add(LoadStg(IDS_STRING501));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaStsData::Register(void)
{
	ogDdx.Register((void *)this,BC_STS_CHANGE,	CString("STSDATA"), CString("Sts-changed"),	ProcessStsCf);
	ogDdx.Register((void *)this,BC_STS_NEW,		CString("STSDATA"), CString("Sts-new"),		ProcessStsCf);
	ogDdx.Register((void *)this,BC_STS_DELETE,	CString("STSDATA"), CString("Sts-deleted"),	ProcessStsCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaStsData::~CedaStsData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaStsData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaStsData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STSDATA *prlSts = new STSDATA;
		if ((ilRc = GetFirstBufferRecord(prlSts)) == true)
		{
			omData.Add(prlSts);//Update omData
			omUrnoMap.SetAt((void *)prlSts->Urno,prlSts);
		}
		else
		{
			delete prlSts;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaStsData::Insert(STSDATA *prpSts)
{
	prpSts->IsChanged = DATA_NEW;
	if(Save(prpSts) == false) return false; //Update Database
	InsertInternal(prpSts);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaStsData::InsertInternal(STSDATA *prpSts)
{
	ogDdx.DataChanged((void *)this, STS_NEW,(void *)prpSts ); //Update Viewer
	omData.Add(prpSts);//Update omData
	omUrnoMap.SetAt((void *)prpSts->Urno,prpSts);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaStsData::Delete(long lpUrno)
{
	STSDATA *prlSts = GetStsByUrno(lpUrno);
	if (prlSts != NULL)
	{
		prlSts->IsChanged = DATA_DELETED;
		if(Save(prlSts) == false) return false; //Update Database
		DeleteInternal(prlSts);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaStsData::DeleteInternal(STSDATA *prpSts)
{
	ogDdx.DataChanged((void *)this,STS_DELETE,(void *)prpSts); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSts->Urno);
	int ilStsCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilStsCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSts->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaStsData::Update(STSDATA *prpSts)
{
	if (GetStsByUrno(prpSts->Urno) != NULL)
	{
		if (prpSts->IsChanged == DATA_UNCHANGED)
		{
			prpSts->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSts) == false) return false; //Update Database
		UpdateInternal(prpSts);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaStsData::UpdateInternal(STSDATA *prpSts)
{
	STSDATA *prlSts = GetStsByUrno(prpSts->Urno);
	if (prlSts != NULL)
	{
		*prlSts = *prpSts; //Update omData
		ogDdx.DataChanged((void *)this,STS_CHANGE,(void *)prlSts); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STSDATA *CedaStsData::GetStsByUrno(long lpUrno)
{
	STSDATA  *prlSts;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSts) == TRUE)
	{
		return prlSts;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaStsData::ReadSpecialData(CCSPtrArray<STSDATA> *popSts,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSts != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STSDATA *prpSts = new STSDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSts,CString(pclFieldList))) == true)
			{
				popSts->Add(prpSts);
			}
			else
			{
				delete prpSts;
			}
		}
		if(popSts->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaStsData::Save(STSDATA *prpSts)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSts->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSts->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSts);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSts->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSts->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSts);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSts->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSts->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessStsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogStsData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaStsData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlStsData;
	prlStsData = (struct BcStruct *) vpDataPointer;
	STSDATA *prlSts;
	if(ipDDXType == BC_STS_NEW)
	{
		prlSts = new STSDATA;
		GetRecordFromItemList(prlSts,prlStsData->Fields,prlStsData->Data);
		if(ValidateStsBcData(prlSts->Urno)) //Prf: 8795
		{
			InsertInternal(prlSts);
		}
		else
		{
			delete prlSts;
		}
	}
	if(ipDDXType == BC_STS_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlStsData->Selection);
		prlSts = GetStsByUrno(llUrno);
		if(prlSts != NULL)
		{
			GetRecordFromItemList(prlSts,prlStsData->Fields,prlStsData->Data);
			if(ValidateStsBcData(prlSts->Urno)) //Prf: 8795
			{
				UpdateInternal(prlSts);
			}
			else
			{
				DeleteInternal(prlSts);
			}
		}
	}
	if(ipDDXType == BC_STS_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlStsData->Selection);

		prlSts = GetStsByUrno(llUrno);
		if (prlSts != NULL)
		{
			DeleteInternal(prlSts);
		}
	}
}

//Prf: 8795
//--ValidateStsBcData--------------------------------------------------------------------------------------

bool CedaStsData::ValidateStsBcData(const long& lrpUrno)
{
	bool blValidateStsBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<STSDATA> olStss;
		if(!ReadSpecialData(&olStss,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateStsBcData = false;
		}
	}
	return blValidateStsBcData;
}

//---------------------------------------------------------------------------------------------------------
