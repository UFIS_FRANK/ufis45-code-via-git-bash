// CedaDPTData.h

#ifndef __CEDADPTDATA__
#define __CEDADPTDATA__

#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct DPTDATA 
{
	long 	 Urno;          //Unique record number
	char     Hopo[4];       //Home airport
	char 	 Usec[33];      //User who created record
	char 	 Useu[33];      //User who updated record
	CTime	 Cdat;          //Creation date/time
	CTime	 Lstu;          //Last update date/time
	long     Rurn;          //Related URNO from DSCTAB
	char     Defl[2];       //Default Flag
    char     Doop[8];       //Days of operation
	char     Dbgn[5];       //Time of schedule begin
	char     Dend[5];       //Time of schedule end
	char     Dpid[33];      //Display ID
	char     Dffd[9];       //Filter Field
	char     Dfco[129];     //Filter Contents
	char     Dter[6];       //Terminal
	char     Dare[11];      //Device Area
	char     Algc[41];      //Airline codes

	//DataCreated by this class
	int		 IsChanged;

	DPTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}
};


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDPTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;	
    CCSPtrArray<DPTDATA> omData;

// Operations
public:
    CedaDPTData();
	~CedaDPTData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<DPTDATA> *popDPTArray,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertDPT(DPTDATA *prpDPT,BOOL bpSendDdx = TRUE);
	bool InsertDPTInternal(DPTDATA *prpDPT);
	bool UpdateDPT(DPTDATA *prpDPT,BOOL bpSendDdx = TRUE);
	bool UpdateDPTInternal(DPTDATA *prpDPT);
	bool DeleteDPT(long lpUrno);
	bool DeleteDPTInternal(DPTDATA *prpDPT);
	DPTDATA  *GetDPTByUrno(long lpUrno);	
	bool SaveDPT(DPTDATA *prpDPT);
	char pcmDPTFieldList[2048];
	void ProcessDPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;}
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;}
	
private:
    void PrepareDPTData(DPTDATA *prpDPTData);	
	CString omWhere;
	bool ValidateDPTBcData(const long& lrpUnro);	
};


//---------------------------------------------------------------------------------------------------------

#endif //__CedaDPTData__
