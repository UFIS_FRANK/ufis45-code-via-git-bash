// AirportArrivalDeskDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <AirportArrivalDeskDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AirportArrivalDeskDlg dialog


AirportArrivalDeskDlg::AirportArrivalDeskDlg(AADDATA *popAad,CWnd* pParent) : CDialog(AirportArrivalDeskDlg::IDD, pParent)
{
	pomAad = popAad;

	//{{AFX_DATA_INIT(AirportArrivalDeskDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void AirportArrivalDeskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AirportArrivalDeskDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_VPFR_D, m_VPFRD);
	DDX_Control(pDX, IDC_VPFR_T, m_VPFRT);
	DDX_Control(pDX, IDC_VPTO_D, m_VPTOD);
	DDX_Control(pDX, IDC_VPTO_T, m_VPTOT);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_DEID,	 m_DEID);
	DDX_Control(pDX, IDC_NAME,	 m_NAME);
	DDX_Control(pDX, IDC_MOTI,	 m_MOTI);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AirportArrivalDeskDlg, CDialog)
	//{{AFX_MSG_MAP(AirportArrivalDeskDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AirportArrivalDeskDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AirportArrivalDeskDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING32806) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("AIRPORTARRIVALDESKDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomAad->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomAad->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomAad->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomAad->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomAad->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomAad->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DEID.SetFormat("x|#x|#x|#x|#x|#");
	m_DEID.SetTextLimit(1,5);
	m_DEID.SetBKColor(YELLOW);  
	m_DEID.SetTextErrColor(RED);
	m_DEID.SetInitText(pomAad->Deid);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_NAME.SetFormat("X(30)");
	m_NAME.SetTextLimit(0,30);
	m_NAME.SetTextErrColor(RED);
	m_NAME.SetInitText(pomAad->Name);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_MOTI.SetFormat("X(82)");
	m_MOTI.SetTextLimit(0,82);
	m_MOTI.SetTextErrColor(RED);
	m_MOTI.SetInitText(pomAad->Moti);

	//------------------------------------
	m_VPFRD.SetTypeToDate(true);
	m_VPFRD.SetTextErrColor(RED);
	m_VPFRD.SetBKColor(YELLOW);
	m_VPFRD.SetInitText(pomAad->Vafr.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_VPFRT.SetTypeToTime(true);
	m_VPFRT.SetTextErrColor(RED);
	m_VPFRT.SetBKColor(YELLOW);
	m_VPFRT.SetInitText(pomAad->Vafr.Format("%H:%M"));
	//------------------------------------
	m_VPTOD.SetTypeToDate(false);
	m_VPTOD.SetTextErrColor(RED);
	m_VPTOD.SetInitText(pomAad->Vato.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_VPTOT.SetTypeToTime(false);
	m_VPTOT.SetTextErrColor(RED);
	m_VPTOT.SetInitText(pomAad->Vato.Format("%H:%M"));
	//------------------------------------

	m_NATOD.SetTypeToDate(true);
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomAad->Nato.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime(true);
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomAad->Nato.Format("%H:%M"));
	//------------------------------------

	m_NAFRD.SetTypeToDate(false);
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomAad->Nafr.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime(false);
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomAad->Nafr.Format("%H:%M"));
	//------------------------------------



	return TRUE;  
}

//----------------------------------------------------------------------------------------

void AirportArrivalDeskDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_DEID.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DEID.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32807) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32807) + ogNotFormat;
		}
	}
	if(m_NAME.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32808) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32808) + ogNotFormat;
		}
	}

	if(m_MOTI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_MOTI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32809) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32809) + ogNotFormat;
		}
	}

	if(m_VPFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VPTOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPTOT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	CString olVpfrd,olVpfrt,olVptod,olVptot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VPFRD.GetWindowText(olVpfrd);
	m_VPFRT.GetWindowText(olVpfrt);
	m_VPTOD.GetWindowText(olVptod);
	m_VPTOT.GetWindowText(olVptot);

	if(m_VPFRD.GetStatus() == true && m_VPFRT.GetStatus() == true && m_VPTOD.GetStatus() == true && m_VPTOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVpfrd,olVpfrt);
		olTmpTimeTo = DateHourStringToDate(olVptod,olVptot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo <= olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	///////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);

	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo <= olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING32811);
		}
	}
 


	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDeid;
		char clWhere[100];
		CCSPtrArray<AADDATA> olAadCPA;

		m_DEID.GetWindowText(olDeid);
		sprintf(clWhere,"WHERE DEID='%s'",olDeid);
		if(ogAadData.ReadSpecialData(&olAadCPA,clWhere,"URNO,DEID",false) == true)
		{
			if(olAadCPA[0].Urno != pomAad->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32807) + LoadStg(IDS_EXIST);
			}
		}
		olAadCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_DEID.GetWindowText(pomAad->Deid,6);
		m_NAME.GetWindowText(pomAad->Name,31);
		m_MOTI.GetWindowText(pomAad->Moti,83);
		pomAad->Vafr = DateHourStringToDate(olVpfrd,olVpfrt);
		pomAad->Vato = DateHourStringToDate(olVptod,olVptot);
		pomAad->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		pomAad->Nato = DateHourStringToDate(olNatod,olNatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DEID.SetFocus();
		m_DEID.SetSel(0,-1);
  }

}

//----------------------------------------------------------------------------------------


