// UtilUC.h: interface for the UtilUC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_)
#define AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class UtilUC  
{
	public:
	UtilUC();
	virtual ~UtilUC();
	bool ConvByteArrToOctSt( BYTE* pbpByteArr, int len, CString& );
	bool ConvByteToOctSt( BYTE bpByte, char* pcpSt );
	bool ConvIntToOctChar( int num, char& ch );

	bool ConvOctStToByteArr( CString pspSt, CString&);
	bool ConvOctStToByte( char* st, BYTE& bpByte );
	bool ConvOctCharToInt( char cpCh , int& num);

	

};

	DWORD LaunchTool(			
		CString opCmd, 		
		CString opArrUrno, CString opDepUrno, 		
		CString opPermit,		
		CString opConfigName,		
		CString processWindowName,		
		DWORD dpPrevId)	;	
				
	bool IsProcessAlive( CString opProcessWindowName, DWORD dpProcId );			
	DWORD startProcess(CString opExePath, CString opArgs );			
	void killProcess(DWORD processPid);			
	static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);	
	bool GetConfigInfo(const char* pcpConfigName, CString& opConfigValue );	
	BOOL IsPrivateProfileOn(CString key, CString defaultValue);

#endif // !defined(AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_)