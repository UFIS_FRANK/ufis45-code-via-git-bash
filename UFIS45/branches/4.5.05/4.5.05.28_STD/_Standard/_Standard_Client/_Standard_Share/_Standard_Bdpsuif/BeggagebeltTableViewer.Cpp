// BeggagebeltTableViewer.cpp 
//

#include <stdafx.h>
#include <BeggagebeltTableViewer.h>
#include <CedaBasicData.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>

 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void BeggagebeltTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// BeggagebeltTableViewer
//

BeggagebeltTableViewer::BeggagebeltTableViewer(CCSPtrArray<BLTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomBeggagebeltTable = NULL;
    ogDdx.Register(this, BLT_CHANGE, CString("BEGGAGEBELTTABLEVIEWER"), CString("Beggagebelt Update/new"), BeggagebeltTableCf);
    ogDdx.Register(this, BLT_DELETE, CString("BEGGAGEBELTTABLEVIEWER"), CString("Beggagebelt Delete"), BeggagebeltTableCf);
	if (ogBasicData.IsGatPosEnabled())
	{
		ogDdx.Register(this, SGM_NEW,	 CString("BEGGAGEBELTTABLEVIEWER"), CString("SGM New"   ), BeggagebeltTableCf);
		ogDdx.Register(this, SGM_CHANGE, CString("BEGGAGEBELTTABLEVIEWER"), CString("SGM Update"), BeggagebeltTableCf);
		ogDdx.Register(this, SGM_DELETE, CString("BEGGAGEBELTTABLEVIEWER"), CString("SGM Delete"), BeggagebeltTableCf);
	}
}

//-----------------------------------------------------------------------------------------------

BeggagebeltTableViewer::~BeggagebeltTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::Attach(CCSTable *popTable)
{
    pomBeggagebeltTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::MakeLines()
{
	int ilBeggagebeltCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilBeggagebeltCount; ilLc++)
	{
		BLTDATA *prlBeggagebeltData = &pomData->GetAt(ilLc);
		MakeLine(prlBeggagebeltData);
	}
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::MakeLine(BLTDATA *prpBeggagebelt)
{
    //if( !IsPassFilter(prpBeggagebelt)) return;

    // Update viewer data for this shift record
    BEGGAGEBELTTABLE_LINEDATA rlBeggagebelt;
	rlBeggagebelt.Urno = prpBeggagebelt->Urno; 
	rlBeggagebelt.Bnam = prpBeggagebelt->Bnam; 
	rlBeggagebelt.Term = prpBeggagebelt->Term;
	rlBeggagebelt.Tele = prpBeggagebelt->Tele;
	rlBeggagebelt.Bltt = prpBeggagebelt->Bltt;
	rlBeggagebelt.Stat = prpBeggagebelt->Stat;
	rlBeggagebelt.Vafr = prpBeggagebelt->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlBeggagebelt.Vato = prpBeggagebelt->Vato.Format("%d.%m.%Y %H:%M"); 

	if (ogBasicData.IsGatPosEnabled())
	{
		CStringArray olExits;
		ogBasicData.GetExitsForBaggageBelt(prpBeggagebelt->Urno,olExits);
		for (int i = 0; i < olExits.GetSize(); i++)
		{
			if (!rlBeggagebelt.Exits.IsEmpty())	
				rlBeggagebelt.Exits +=',';
			rlBeggagebelt.Exits += olExits[i];
		}
	}

	CreateLine(&rlBeggagebelt);
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::CreateLine(BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareBeggagebelt(prpBeggagebelt, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	BEGGAGEBELTTABLE_LINEDATA rlBeggagebelt;
	rlBeggagebelt = *prpBeggagebelt;
    omLines.NewAt(ilLineno, rlBeggagebelt);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void BeggagebeltTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	omHeaderDataArray.DeleteAll();

	pomBeggagebeltTable->SetShowSelection(TRUE);
	pomBeggagebeltTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING890);
	}
	else
	{
		olString = LoadStg(IDS_STRING256);
	}

	int ilPos = 1;
	rlHeader.Length = 42;												// Name
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70;												// Terminal
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83;												// Phone no.
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;												// Valid from
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;												// Valid to
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30;												// Type
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50;												// Status
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if (ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 133;											// Exits
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	pomBeggagebeltTable->SetHeaderFields(omHeaderDataArray);

	pomBeggagebeltTable->SetDefaultSeparator();
	pomBeggagebeltTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Bnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bltt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Stat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Exits;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomBeggagebeltTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomBeggagebeltTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString BeggagebeltTableViewer::Format(BEGGAGEBELTTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL BeggagebeltTableViewer::FindBeggagebelt(char *pcpBeggagebeltKeya, char *pcpBeggagebeltKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpBeggagebeltKeya) &&
			 (omLines[ilItem].Keyd == pcpBeggagebeltKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void BeggagebeltTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    BeggagebeltTableViewer *polViewer = (BeggagebeltTableViewer *)popInstance;
    switch(ipDDXType)
	{
	case BLT_CHANGE:
		polViewer->ProcessBeggagebeltChange((BLTDATA *)vpDataPointer);
	break;
	case BLT_DELETE:
		polViewer->ProcessBeggagebeltDelete((BLTDATA *)vpDataPointer);
	break;
	case SGM_NEW:
	case SGM_CHANGE:
	case SGM_DELETE:
		polViewer->ProcessBeggagebeltChange((SGMDATA *)vpDataPointer);
	break;
	}
} 


//-----------------------------------------------------------------------------------------------
void BeggagebeltTableViewer::ProcessBeggagebeltChange(SGMDATA *prpSgm)
{
	if (ogBasicData.IsGatPosEnabled())
	{
		int ilItem = 0;

		if (FindLine(prpSgm->Valu, ilItem))
		{
			BLTDATA *prpBlt = ogBLTData.GetBLTByUrno(prpSgm->Valu);
			if (prpBlt)
			{
				ProcessBeggagebeltChange(prpBlt);
			}
		}
	}
}


//-----------------------------------------------------------------------------------------------
void BeggagebeltTableViewer::ProcessBeggagebeltChange(BLTDATA *prpBeggagebelt)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpBeggagebelt->Bnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Bltt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBeggagebelt->Stat;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	CStringArray olExits;
	if (ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text ="";
		ogBasicData.GetExitsForBaggageBelt(prpBeggagebelt->Urno,olExits);
		for (int i = 0; i < olExits.GetSize(); i++)
		{
			if (!rlColumn.Text.IsEmpty())	
				rlColumn.Text +=',';
			rlColumn.Text += olExits[i];
		}

		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpBeggagebelt->Urno, ilItem))
	{
        BEGGAGEBELTTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpBeggagebelt->Urno;
		prlLine->Bnam = prpBeggagebelt->Bnam;
		prlLine->Term = prpBeggagebelt->Term;
		prlLine->Tele = prpBeggagebelt->Tele;
		prlLine->Bltt = prpBeggagebelt->Bltt;
		prlLine->Stat = prpBeggagebelt->Stat;
		prlLine->Vafr = prpBeggagebelt->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpBeggagebelt->Vato.Format("%d.%m.%Y %H:%M");

		if (ogBasicData.IsGatPosEnabled())
		{
			for (int i = 0; i < olExits.GetSize(); i++)
			{
				if (!prlLine->Exits.IsEmpty())	
					prlLine->Exits +=',';
				prlLine->Exits += olExits[i];
			}
		}
		
		pomBeggagebeltTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomBeggagebeltTable->DisplayTable();
	}
	else
	{
		MakeLine(prpBeggagebelt);
		if (FindLine(prpBeggagebelt->Urno, ilItem))
		{
	        BEGGAGEBELTTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomBeggagebeltTable->AddTextLine(olLine, (void *)prlLine);
				pomBeggagebeltTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::ProcessBeggagebeltDelete(BLTDATA *prpBeggagebelt)
{
	int ilItem;
	if (FindLine(prpBeggagebelt->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomBeggagebeltTable->DeleteTextLine(ilItem);
		pomBeggagebeltTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL BeggagebeltTableViewer::IsPassFilter(BLTDATA *prpBeggagebelt)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int BeggagebeltTableViewer::CompareBeggagebelt(BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt1, BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpBeggagebelt1->Tifd == prpBeggagebelt2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpBeggagebelt1->Tifd > prpBeggagebelt2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL BeggagebeltTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void BeggagebeltTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING179);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool BeggagebeltTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool BeggagebeltTableViewer::PrintTableLine(BEGGAGEBELTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Bnam;
				}
			break;
			case 1:
				{
					rlElement.Text		= prpLine->Term;
				}
			break;
			case 2:
				{
					rlElement.Text		= prpLine->Tele;
				}
			break;
			case 3:
				{
					rlElement.Text		= prpLine->Vafr;
				}
			break;
			case 4:
				{
					rlElement.Text		= prpLine->Vato;
				}
			break;
			case 5:
				{
					rlElement.Text		= prpLine->Bltt;
				}
			break;
			case 6:
				{
					if (!ogBasicData.IsGatPosEnabled())
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
					}
					rlElement.Text		= prpLine->Stat;
				}
			break;
			case 7:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		 = prpLine->Exits;
				}
			break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool BeggagebeltTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "BLTTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "BLTTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING890);
	}
	else
	{
		olString = LoadStg(IDS_STRING256);
	}

	CStringArray olStrArray;
	ExtractItemList(olString,&olStrArray,'|');

	of	<< setw(0) << olStrArray[0]	//CString("Name");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[1]	//CString("Terminal");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[2]	//CString("Phone number");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[3] //CString("Valid from");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[4] //CString("Valid to");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[5] //CString("Type");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[6] //CString("Status");
		;
	if (ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << opTrenner
			<< setw(0) << olStrArray[7]	//CString("Exits");
			<< endl;
	}
	else
	{
		of << endl;
	}


	
	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		BEGGAGEBELTTABLE_LINEDATA rlLine = omLines[i];
		CString olExits;
		CStringArray olExitsList;
		ogBasicData.GetExitsForBaggageBelt(rlLine.Urno,olExitsList);			
		for (int j = 0; j < olExitsList.GetSize(); j++)
		{
			if (!olExits.IsEmpty())			
				olExits += ropListSeparator;
			olExits += olExitsList[j];
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Bnam							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Term							// Terminal
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Tele							// Phone number
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vafr							// Valid from
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vato							// Valid to
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Bltt							// Type
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Stat							// Status
			 ;
		if (ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0) << opTrenner 
				<< setw(0) << olExits							// Exits
				<< endl; 
		}
		else
		{
			of	<< endl;
		}
		
	}

	of.close();
	return true;
}
