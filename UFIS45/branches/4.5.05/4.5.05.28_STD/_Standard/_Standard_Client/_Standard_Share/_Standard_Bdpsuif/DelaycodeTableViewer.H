#ifndef __DELAYCODETABLEVIEWER_H__
#define __DELAYCODETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaDENData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct DELAYCODETABLE_LINEDATA
{
	long	Urno;
	CString	Decn;
	CString	Deca;
	CString Decs;//AM:20100913 - For MultiDelayCode
	CString	Alc3;
	CString	Dena;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// DelaycodeTableViewer

class DelaycodeTableViewer : public CViewer
{
// Constructions
public:
    DelaycodeTableViewer(CCSPtrArray<DENDATA> *popData);
    ~DelaycodeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(DENDATA *prpDelaycode);
	int CompareDelaycode(DELAYCODETABLE_LINEDATA *prpDelaycode1, DELAYCODETABLE_LINEDATA *prpDelaycode2);
    void MakeLines();
	void MakeLine(DENDATA *prpDelaycode);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(DELAYCODETABLE_LINEDATA *prpDelaycode);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(DELAYCODETABLE_LINEDATA *prpLine);
	void ProcessDelaycodeChange(DENDATA *prpDelaycode);
	void ProcessDelaycodeDelete(DENDATA *prpDelaycode);
	bool FindDelaycode(char *prpDelaycodeKeya, char *prpDelaycodeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomDelaycodeTable;
	CCSPtrArray<DENDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<DELAYCODETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(DELAYCODETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__DELAYCODETABLEVIEWER_H__
