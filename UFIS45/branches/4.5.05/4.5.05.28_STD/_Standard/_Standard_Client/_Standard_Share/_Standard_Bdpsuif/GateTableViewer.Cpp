// GateTableViewer.cpp 
// 

#include <stdafx.h>
#include <GateTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void GateTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// GateTableViewer
//

GateTableViewer::GateTableViewer(CCSPtrArray<GATDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomGateTable = NULL;
    ogDdx.Register(this, GAT_CHANGE, CString("GATETABLEVIEWER"), CString("Gate Update/new"), GateTableCf);
    ogDdx.Register(this, GAT_DELETE, CString("GATETABLEVIEWER"), CString("Gate Delete"), GateTableCf);
	if (ogBasicData.IsGatPosEnabled())
	{
		ogDdx.Register(this, SGM_NEW,	 CString("GATETABLEVIEWER"), CString("SGM New"   ), GateTableCf);
		ogDdx.Register(this, SGM_CHANGE, CString("GATETABLEVIEWER"), CString("SGM Update"), GateTableCf);
		ogDdx.Register(this, SGM_DELETE, CString("GATETABLEVIEWER"), CString("SGM Delete"), GateTableCf);
	}
}

//-----------------------------------------------------------------------------------------------

GateTableViewer::~GateTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::Attach(CCSTable *popTable)
{
    pomGateTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::MakeLines()
{
	int ilGateCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilGateCount; ilLc++)
	{
		GATDATA *prlGateData = &pomData->GetAt(ilLc);
		MakeLine(prlGateData);
	}
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::MakeLine(GATDATA *prpGate)
{
	//if( !IsPassFilter(prpGate)) return;

    // Update viewer data for this shift record
    GATETABLE_LINEDATA rlGate;

	rlGate.Urno = prpGate->Urno; 
	rlGate.Gnam = prpGate->Gnam; 
	rlGate.Busg = prpGate->Busg; 
	rlGate.Rga1 = prpGate->Rga1; 
	rlGate.Rga2 = prpGate->Rga2; 
	rlGate.Rbab = prpGate->Rbab; 
	rlGate.Term = prpGate->Term; 
	rlGate.Tele = prpGate->Tele; 
	rlGate.Nobr = prpGate->Nobr; 
	rlGate.Vafr = prpGate->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlGate.Vato = prpGate->Vato.Format("%d.%m.%Y %H:%M"); 
	rlGate.Gtid = prpGate->Gtid; 
	rlGate.Gtyp = prpGate->Gtyp; 

	if (ogBasicData.IsGatPosEnabled())
	{
		rlGate.BaggageBelts = "";
		CStringArray olBaggageBelts;
		ogBasicData.GetBaggageBeltsForGate(prpGate->Urno,olBaggageBelts);
		for (int i = 0; i < olBaggageBelts.GetSize(); i++)
		{
			if (!rlGate.BaggageBelts.IsEmpty())
				rlGate.BaggageBelts += ',';
			rlGate.BaggageBelts += olBaggageBelts[i];
		}

		rlGate.Lounges = "";
		CStringArray olLounges;
		ogBasicData.GetWaitingRoomsForGate(prpGate->Urno,olLounges);
		for (i = 0; i < olLounges.GetSize(); i++)
		{
			if (!rlGate.Lounges.IsEmpty())
				rlGate.Lounges += ',';
			rlGate.Lounges += olLounges[i];
		}
	}

	CreateLine(&rlGate);
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::CreateLine(GATETABLE_LINEDATA *prpGate)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareGate(prpGate, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	GATETABLE_LINEDATA rlGate;
	rlGate = *prpGate;
    omLines.NewAt(ilLineno, rlGate);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void GateTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	omHeaderDataArray.DeleteAll();

	pomGateTable->SetShowSelection(TRUE);
	pomGateTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING892);
	}
	else
	{
		olString = LoadStg(IDS_STRING259);
	}

	int ilPos = 1;
	rlHeader.Length = 42;											// Name 
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// B
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	if (!ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 42;											// Pos. 1
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');		
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
		rlHeader.Length = 42;											// Pos. 2
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');		
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
		rlHeader.Length = 42;											// Belt
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else
	{
		ilPos += 3;
	}

	rlHeader.Length = 8;												// T
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83;												// Phone 1 dep
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 17;												// Br
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 80;												// Valid from
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 80;												// Valid to
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 10;												// D
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 6;												// Type
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if (ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 133;											// Baggage belts
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

		rlHeader.Length = 133;											// Lounges
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	pomGateTable->SetHeaderFields(omHeaderDataArray);

	pomGateTable->SetDefaultSeparator();
	pomGateTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Gnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Busg == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if (!ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Rga1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Rga2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Rbab;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Nobr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gtid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].BaggageBelts;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = omLines[ilLineNo].Lounges;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomGateTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomGateTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString GateTableViewer::Format(GATETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL GateTableViewer::FindGate(char *pcpGateKeya, char *pcpGateKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpGateKeya) &&
			 (omLines[ilItem].Keyd == pcpGateKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void GateTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    GateTableViewer *polViewer = (GateTableViewer *)popInstance;
    switch(ipDDXType)
	{
	case GAT_CHANGE:
		polViewer->ProcessGateChange((GATDATA *)vpDataPointer);
	break;
	case GAT_DELETE:
		polViewer->ProcessGateDelete((GATDATA *)vpDataPointer);
	break;
	case SGM_NEW:
	case SGM_CHANGE:
	case SGM_DELETE:
		polViewer->ProcessGateChange((SGMDATA *)vpDataPointer);
	break;
	}
} 

//-----------------------------------------------------------------------------------------------
void GateTableViewer::ProcessGateChange(SGMDATA *prpSgm)
{
	if (ogBasicData.IsGatPosEnabled())
	{
		int ilItem = 0;

		if (FindLine(prpSgm->Valu, ilItem))
		{
			GATDATA *prpGate = ogGATData.GetGATByUrno(prpSgm->Valu);
			if (prpGate)
			{
				ProcessGateChange(prpGate);
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------
void GateTableViewer::ProcessGateChange(GATDATA *prpGate)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpGate->Gnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpGate->Busg == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if (!ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text = prpGate->Rga1;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpGate->Rga2;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpGate->Rbab;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	rlColumn.Text = prpGate->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGate->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpGate->Nobr;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpGate->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGate->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGate->Gtid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGate->Gtyp;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	CStringArray olBaggageBelts;
	CStringArray olLounges;
	if (ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text ="";
		ogBasicData.GetBaggageBeltsForGate(prpGate->Urno,olBaggageBelts);
		for (int i = 0; i < olBaggageBelts.GetSize(); i++)
		{
			if (!rlColumn.Text.IsEmpty())	
				rlColumn.Text +=',';
			rlColumn.Text += olBaggageBelts[i];
		}

		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text ="";
		ogBasicData.GetWaitingRoomsForGate(prpGate->Urno,olLounges);
		for (i = 0; i < olLounges.GetSize(); i++)
		{
			if (!rlColumn.Text.IsEmpty())	
				rlColumn.Text +=',';
			rlColumn.Text += olLounges[i];
		}

		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

	}

	if (FindLine(prpGate->Urno, ilItem))
	{
        GATETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpGate->Urno; 
		prlLine->Gnam = prpGate->Gnam; 
		prlLine->Busg = prpGate->Busg; 
		prlLine->Rga1 = prpGate->Rga1; 
		prlLine->Rga2 = prpGate->Rga2; 
		prlLine->Rbab = prpGate->Rbab; 
		prlLine->Term = prpGate->Term;
		prlLine->Tele = prpGate->Tele; 
		prlLine->Nobr = prpGate->Nobr; 
		prlLine->Vafr = prpGate->Vafr.Format("%d.%m.%Y %H:%M"); 
		prlLine->Vato = prpGate->Vato.Format("%d.%m.%Y %H:%M"); 
		
		if (ogBasicData.IsGatPosEnabled())
		{
			prlLine->BaggageBelts = "";
			for (int i = 0; i < olBaggageBelts.GetSize(); i++)
			{
				if (!prlLine->BaggageBelts.IsEmpty())	
					prlLine->BaggageBelts +=',';
				prlLine->BaggageBelts += olBaggageBelts[i];
			}

			prlLine->Lounges = "";
			for (i = 0; i < olLounges.GetSize(); i++)
			{
				if (!prlLine->Lounges.IsEmpty())	
					prlLine->Lounges +=',';
				prlLine->Lounges += olLounges[i];
			}

		}

		pomGateTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomGateTable->DisplayTable();
	}
	else
	{
		MakeLine(prpGate);
		if (FindLine(prpGate->Urno, ilItem))
		{
	        GATETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomGateTable->AddTextLine(olLine, (void *)prlLine);
				pomGateTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::ProcessGateDelete(GATDATA *prpGate)
{
	int ilItem;
	if (FindLine(prpGate->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomGateTable->DeleteTextLine(ilItem);
		pomGateTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL GateTableViewer::IsPassFilter(GATDATA *prpGate)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int GateTableViewer::CompareGate(GATETABLE_LINEDATA *prpGate1, GATETABLE_LINEDATA *prpGate2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpGate1->Tifd == prpGate2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpGate1->Tifd > prpGate2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void GateTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL GateTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void GateTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void GateTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING178);
 	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool GateTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
/*		if(i!=4&&i!=13)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
*/			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
//		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool GateTableViewer::PrintTableLine(GATETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
/*		if(i!=4&&i!=13)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
*/			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			if (!ogBasicData.IsGatPosEnabled())
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Gnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Busg;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Rga1;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Rga2;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Rbab;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Nobr;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 9:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 10:
					{
						rlElement.Text		= prpLine->Gtid;
					}
					break;
				case 11:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Gtyp;
					}
					break;
				}
			}
			else
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Gnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Busg;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Nobr;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Gtid;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Gtyp;
					}
					break;
				case 9:
					{
						rlElement.Text = prpLine->BaggageBelts;
					}
					break;
				case 10:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text = prpLine->Lounges;
					}
				break;
				}
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
//		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool GateTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "GATTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "GATTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING892);
	}
	else
	{
		olString = LoadStg(IDS_STRING259);
	}

	CStringArray olStrArray;
	ExtractItemList(olString,&olStrArray,'|');

	of	<< setw(0) << olStrArray[0]	//CString("Name");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[1]	//CString("B");
		<< setw(0) << opTrenner
	;

	if (!ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << olStrArray[2]	//CString("Pos. 1");
			<< setw(0) << opTrenner
			<< setw(0) << olStrArray[3]	//CString("Pos. 2");
			<< setw(0) << opTrenner
			<< setw(0) << olStrArray[4]	//CString("Belt");
			<< setw(0) << opTrenner
		;
	}

	of	<< setw(0) << olStrArray[5]	//CString("T");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[6]	//CString("Phone 1 dep");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[7]	//CString("Br");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[8]//CString("Valid from");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[9]//CString("Valid to");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[10]//CString("D");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[11]//CString("Type");
		;

	if (ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << opTrenner
			<< setw(0) << olStrArray[12]	//CString("Baggage belts");
			<< setw(0) << opTrenner
			<< setw(0) << olStrArray[13]	//CString("Lounges");
			<< endl;
	}
	else
	{
		of << endl;
	}


	
	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		GATETABLE_LINEDATA rlLine = omLines[i];

		CString olBaggageBelts;
		CStringArray olBaggageBeltsList;
		ogBasicData.GetBaggageBeltsForGate(rlLine.Urno,olBaggageBeltsList);			
		for (int j = 0; j < olBaggageBeltsList.GetSize(); j++)
		{
			if (!olBaggageBelts.IsEmpty())			
				olBaggageBelts += ropListSeparator;
			olBaggageBelts += olBaggageBeltsList[j];
		}

		CString olLounges;
		CStringArray olLoungesList;
		ogBasicData.GetWaitingRoomsForGate(rlLine.Urno,olLoungesList);			
		for (j = 0; j < olLoungesList.GetSize(); j++)
		{
			if (!olLounges.IsEmpty())			
				olLounges += ropListSeparator;
			olLounges += olLoungesList[j];
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Gnam							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Busg							// B
		     << setw(0) << opTrenner 
		;
		if (!ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0) << rlLine.Rga1							// Pos. 1
				<< setw(0) << opTrenner 
				<< setw(0) << rlLine.Rga2							// Pos. 2
				<< setw(0) << opTrenner 
				<< setw(0) << rlLine.Rbab							// Belt
				<< setw(0) << opTrenner 
			;
		}

		of	<< setw(0) << rlLine.Term							// T
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Tele							// Phone 1 dep
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Nobr							// Br
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vafr							// Valid from
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vato							// Valid to
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Gtid							// D
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Gtyp							// Type
			 ;
		if (ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0) << opTrenner 
				<< setw(0) << olBaggageBelts					// Baggage belts
			    << setw(0) << opTrenner 
				<< setw(0) << olLounges							// Lounges
				<< endl; 
		}
		else
		{
			of	<< endl;
		}
		
	}

	of.close();
	return true;
}

//-----------------------------------------------------------------------------------------------
