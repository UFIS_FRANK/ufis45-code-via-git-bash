#ifndef __EquipmentTypeTableViewer_H__
#define __EquipmentTypeTableViewer_H__

#include <stdafx.h>
#include <CedaEqtData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct EQUIPMENTTYPETABLE_LINEDATA
{
	long 		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Code;
	CString		Name;
};

/////////////////////////////////////////////////////////////////////////////
// EquipmentTypeTableViewer

	  
class EquipmentTypeTableViewer : public CViewer
{
// Constructions
public:
    EquipmentTypeTableViewer(CCSPtrArray<EQTDATA> *popData);
    ~EquipmentTypeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(EQTDATA *prpEquipmentType);
	int CompareEquipmentType(EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType1, EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType2);
    void MakeLines();
	void MakeLine(EQTDATA *prpEquipmentType);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(EQUIPMENTTYPETABLE_LINEDATA *prpLine);
	void ProcessEquipmentTypeChange(EQTDATA *prpEquipmentType);
	void ProcessEquipmentTypeDelete(EQTDATA *prpEquipmentType);
	bool FindEquipmentType(char *prpEquipmentTypeKeya, char *prpEquipmentTypeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomEquipmentTypeTable;
	CCSPtrArray<EQTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<EQUIPMENTTYPETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(EQUIPMENTTYPETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__EquipmentTypeTableViewer_H__
