// SorDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <SorDlg.h>
#include <CedaBasicData.h>
#include <CedaOrgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSorDlg 


CSorDlg::CSorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSorDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

CSorDlg::CSorDlg(CString Code, CString Odgc, CString Lead, CWnd* pParent /*=NULL*/)	: CDialog(CSorDlg::IDD, pParent)
{
	omCode = Code;
	omOdgc = Odgc;
	omLead = Lead;

	//{{AFX_DATA_INIT(CSco_Dlg)
	m_CODE = _T("");
	m_ODGC = _T("");
	m_LEAD = _T("");
	//}}AFX_DATA_INIT
}



void CSorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSorDlg)
	DDX_Control(pDX, IDC_COMBO2, m_CB_COT);
	DDX_Control(pDX, IDC_COMBO1, m_CB_SOR);
	DDX_Control(pDX, IDC_LEAD, m_ctrl_Lead);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSorDlg, CDialog)
	//{{AFX_MSG_MAP(CSorDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CSorDlg 
//---------------------------------------------------------------------------
BOOL CSorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	SetWindowText(LoadStg(IDS_STRING927));

	CWnd *polWnd = GetDlgItem(IDC_STATIC_GROUPCODE);
	ogBasicData.SetWindowStat("MITARBEITER-ORGDLG.m_COT",polWnd);
	ogBasicData.SetWindowStat("MITARBEITER-ORGDLG.m_COT",&m_CB_COT);

	ogBasicData.SetWindowStat("MITARBEITER-ORGDLG.m_LEAD",&m_ctrl_Lead);

	polWnd = GetDlgItem(IDOK);
	ogBasicData.SetWindowStat("MITARBEITER-ORGDLG.m_OK",polWnd);


	ReadORGData();
	InitCombo1();

	InitCombo2();

	ReadSORData();
	SetSORData();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
//---------------------------------------------------------------------------
int CSorDlg::ReadORGData()
{
	char querystr[1024];
	ogBCD.SetObject("ORG","URNO,DPT1,DPT2");
		
	sprintf(querystr,"");

	ogBCD.Read("ORG",querystr);

	return ogBCD.GetDataCount("ORG");

}
//---------------------------------------------------------------------------
void CSorDlg::ReadSORData()
{
		char querystr[1024];

		ogBCD.SetObject("SOR","URNO,SURN,CODE,VPFR,VPTO,ODGC,LEAD");
		
		im_S_UrnoPos=ogBCD.GetFieldIndex("SOR","URNO");	// Eindeutige Datensatz-Nr.
		im_S_SurnPos=ogBCD.GetFieldIndex("SOR","SURN");	// Referenz auf STF.URNO
		im_S_CodePos=ogBCD.GetFieldIndex("SOR","CODE");	// Codereferenz ORG.CODE
		im_S_VpfrPos=ogBCD.GetFieldIndex("SOR","VPFR");	// G�ltig von
		im_S_VptoPos=ogBCD.GetFieldIndex("SOR","VPTO");	// G�ltig bis
		im_S_OdgcPos=ogBCD.GetFieldIndex("SOR","ODGC");	// Code der	Dienstgruppe
		im_S_LeadPos=ogBCD.GetFieldIndex("SOR","LEAD");	// MA ist Leiter

		sprintf(querystr,"Where SURN='%s'",m_SURN);

		ogBCD.Read("SOR",querystr);

		int il = ogBCD.GetDataCount("SOR");
}
//---------------------------------------------------------------------------
int CSorDlg::ReadODGData(CString opOrgu)
{
	char querystr[1024];
	ogBCD.SetObject("ODG","URNO,HOPO,ODGC,ODGN,ORGU,REMA");
		
	sprintf(querystr,"where ORGU = '%s'", opOrgu);

	ogBCD.Read("ODG",querystr);
	
	return ogBCD.GetDataCount("ODG");
	
}
//---------------------------------------------------------------------------
void CSorDlg::InitCombo1()
{
	// alle vorhandenen Eintr�ge l�schen
	CString opCODE;

	while (m_CB_SOR.GetCount() > 0) m_CB_SOR.DeleteString(0);

	for(int i=0;i<ogBCD.GetDataCount("ORG");i++)
	{
		opCODE = ogBCD.GetField("ORG",i,"DPT1");
		m_CB_SOR.AddString(opCODE);
	}
	if (!omCode.IsEmpty())
	{	//select!
		m_CB_SOR.SelectString(-1 , (LPCTSTR)  omCode);
	}


}
//---------------------------------------------------------------------------
void CSorDlg::InitCombo2()
{
	// alle vorhandenen Eintr�ge l�schen
	CString opCODE, opORGU;
	long ilOrgUrno = 0;

	while (m_CB_COT.GetCount() > 0) m_CB_COT.DeleteString(0);

	//uhi 15.3.01
	if(omCode.IsEmpty())
		return;

	char querystr[1024];
	sprintf(querystr,"where DPT1 = '%s'", omCode); 
	
	CCSPtrArray<ORGDATA> olList;
	if(ogOrgData.ReadSpecialData(&olList, querystr, "URNO,DPT1,DPTN", false) == false)
		return;
	
	char pclUrno[20]="";
	sprintf(pclUrno, "%ld", olList[0].Urno);
	
	ReadODGData(pclUrno);
	
	for(int i=0;i<ogBCD.GetDataCount("ODG");i++)
	{
		opCODE = ogBCD.GetField("ODG",i,"ODGC");
		int ret = m_CB_COT.FindString(-1,opCODE);
		if (ret<0)
			m_CB_COT.AddString(opCODE);
	}
	if (!omOdgc.IsEmpty())
	{	//select!
		m_CB_COT.SelectString(-1 , (LPCTSTR)  omOdgc);
	}
}
//---------------------------------------------------------------------------
void CSorDlg::SetSORData()
{
	if (omLead == 'x') m_ctrl_Lead.SetCheck(1);
}
//---------------------------------------------------------------------------
void CSorDlg::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	SaveSORData();
	
	CDialog::OnOK();
}
//---------------------------------------------------------------------------
bool CSorDlg::SaveSORData()
{
	m_CB_SOR.GetWindowText(omCode);
	m_CB_COT.GetWindowText(omOdgc);

	(m_ctrl_Lead.GetCheck() == 1) ? omLead = 'x' : omLead = ' ';

	return true;
}

//---------------------------------------------------------------------------


void CSorDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	//uhi 16.3.01
	CString olText;
	if(m_CB_SOR.GetCurSel() == CB_ERR)
		return;
	m_CB_SOR.GetLBText(m_CB_SOR.GetCurSel(), olText);
	if(omCode != olText)
		omCode = olText;
		InitCombo2();
}
