// AirPortWet.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <AirPortWet.h>
#include <CedaAPTData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAirPortWet dialog
//---------------------------------------------------------------------------

CAirPortWet::CAirPortWet(AWIDATA *prpAwi,CWnd* pParent /*=NULL*/)
	: CDialog(CAirPortWet::IDD, pParent)
{
	
	pomAwi = prpAwi;

	//{{AFX_DATA_INIT(CAirPortWet)
	//}}AFX_DATA_INIT
}


void CAirPortWet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAirPortWet)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_APC3, m_Apc3);
	DDX_Control(pDX, IDC_APC4, m_Apc4);
	DDX_Control(pDX, IDC_HUMI, m_Humi);
	DDX_Control(pDX, IDC_MSGT, m_Msgt);
	DDX_Control(pDX, IDC_TEMP, m_Temp);
	DDX_Control(pDX, IDC_VISI, m_Visi);
	DDX_Control(pDX, IDC_WCOD, m_Wcod);
	DDX_Control(pDX, IDC_WDIR, m_Wdir);
	DDX_Control(pDX, IDC_WIND, m_Wind);
	DDX_Control(pDX, IDC_VAFR_D, m_VaFrD);
	DDX_Control(pDX, IDC_VAFR_T, m_VaFrT);
	DDX_Control(pDX, IDC_VATO_D2, m_VaToD);
	DDX_Control(pDX, IDC_VATO_T2, m_VaToT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAirPortWet, CDialog)
	//{{AFX_MSG_MAP(CAirPortWet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAirPortWet message handlers
//---------------------------------------------------------------------------

BOOL CAirPortWet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING201) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("AIRPORTWETDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_Humi.SetFormat("x|#x|#x|#");
	m_Humi.SetTextLimit(0,3);
	m_Humi.SetTextErrColor(RED);
	m_Humi.SetInitText(pomAwi->Humi);
	//m_Humi.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_HUMI"));
	//------------------------------------
	m_Temp.SetFormat("x|#x|#x|#");
	m_Temp.SetTextLimit(0,3);
	m_Temp.SetTextErrColor(RED);
	m_Temp.SetInitText(pomAwi->Temp);
	//m_Temp.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_TEMP"));
	//------------------------------------
	m_Visi.SetFormat("x|#x|#x|#");
	m_Visi.SetTextLimit(0,3);
	m_Visi.SetTextErrColor(RED);
	m_Visi.SetInitText(pomAwi->Visi);
	//m_Visi.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VISI"));
	//------------------------------------
	m_Wcod.SetFormat("x|#x|#x|#");
	m_Wcod.SetTextLimit(1,3);
	m_Wcod.SetBKColor(YELLOW);  
	m_Wcod.SetTextErrColor(RED);
	m_Wcod.SetInitText(pomAwi->Wcod);
	//m_Wcod.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_WCOD"));
	//------------------------------------
	m_Wdir.SetFormat("x|#x|#x|#");
	m_Wdir.SetTextLimit(0,3);
	m_Wdir.SetTextErrColor(RED);
	m_Wdir.SetInitText(pomAwi->Wdir);
	//m_Wdir.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_WDIR"));
	//------------------------------------
	m_Wind.SetFormat("x|#x|#x|#");
	m_Wind.SetTextLimit(0,3);
	m_Wind.SetTextErrColor(RED);
	m_Wind.SetInitText(pomAwi->Wind);
	//m_Wind.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_WIND"));
	//------------------------------------
	m_Apc3.SetFormat("x|#x|#x|#");
	m_Apc3.SetTextErrColor(RED);
	m_Apc3.SetInitText(pomAwi->Apc3);
	//m_Apc3.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_APC3"));
	//------------------------------------
	m_Apc4.SetFormat("x|#x|#x|#x|#");
	m_Apc4.SetBKColor(YELLOW);  
	m_Apc4.SetTextErrColor(RED);
	m_Apc4.SetInitText(pomAwi->Apc4);
	//m_Apc4.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_APC4"));
	//------------------------------------
	m_VaFrD.SetTypeToDate(true);
	m_VaFrD.SetTextErrColor(RED);
	m_VaFrD.SetBKColor(YELLOW);
	m_VaFrD.SetInitText(pomAwi->Vafr.Format("%d.%m.%Y"));
	//m_VaFrD.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VaFrT.SetTypeToTime(true);
	m_VaFrT.SetTextErrColor(RED);
	m_VaFrT.SetBKColor(YELLOW);
	m_VaFrT.SetInitText(pomAwi->Vafr.Format("%H:%M"));
	//m_VaFrT.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VAFR"));
	//------------------------------------
	m_VaToD.SetTypeToDate();
	m_VaToD.SetTextErrColor(RED);
	m_VaToD.SetInitText(pomAwi->Vato.Format("%d.%m.%Y"));
	//m_VaToD.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VaToT.SetTypeToTime();
	m_VaToT.SetTextErrColor(RED);
	m_VaToT.SetInitText(pomAwi->Vato.Format("%H:%M"));
	//m_VaToT.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VATO"));
	//------------------------------------
	m_Msgt.SetTypeToString("X(60)",60,0);
	m_Msgt.SetTextErrColor(RED);
	m_Msgt.SetInitText(pomAwi->Msgt);
	//m_Msgt.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_MSGT"));

	return TRUE;
}

//---------------------------------------------------------------------------

void CAirPortWet::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	CString olTmp;
	if(m_Apc3.GetStatus() == false)
	{
		m_Apc3.GetWindowText(olTmp);
		if(olTmp.GetLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING290) + ogNotFormat;
		}
	}
	if(m_Apc4.GetStatus() == false)
	{
		ilStatus = false;
		m_Apc4.GetWindowText(olTmp);
		if(olTmp.GetLength() == 0)
			olErrorText += LoadStg(IDS_STRING300) + ogNoData;
		else
			olErrorText += LoadStg(IDS_STRING300) + ogNotFormat;
	}
	if(m_Humi.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING603) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_Temp.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING604) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_Visi.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING605) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_Wcod.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING606) + ogNotFormat;//+ CString(" 3") 
	}
	if(m_Wdir.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING607) + ogNotFormat;//+ CString(" 4") 
	}
	if(m_Wind.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING611) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_Msgt.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING608) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_VaFrD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VaFrD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VaFrT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VaFrT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VaToD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VaToT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VaFrD.GetWindowText(olVafrd);
	m_VaFrT.GetWindowText(olVafrt);
	m_VaToD.GetWindowText(olVatod);
	m_VaToT.GetWindowText(olVatot);

	if((m_VaFrD.GetWindowTextLength() != 0 && m_VaFrT.GetWindowTextLength() == 0) || (m_VaFrD.GetWindowTextLength() == 0 && m_VaFrT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VaToD.GetWindowTextLength() != 0 && m_VaToT.GetWindowTextLength() == 0) || (m_VaToD.GetWindowTextLength() == 0 && m_VaToT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VaFrD.GetStatus() == true && m_VaFrT.GetStatus() == true && m_VaFrD.GetStatus() == true && m_VaFrT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olApc3,olApc4;
		char clWhere[100];

		m_Apc4.GetWindowText(olApc4);
		sprintf(clWhere,"WHERE APC4='%s'",olApc4);
		if(ogAPTData.ReadSpecialData(NULL,clWhere,"APC4",false) == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING33);
		}
		m_Apc3.GetWindowText(olApc3);
		if(olApc3.GetLength() == 3)
		{
			sprintf(clWhere,"WHERE APC3='%s'",olApc3);
			if(ogAPTData.ReadSpecialData(NULL,clWhere,"APC3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING33);
			}
		}

		CCSPtrArray<AWIDATA> olAwiCPA;
		sprintf(clWhere,"WHERE APC4='%s'",olApc4);
		if(ogAwiData.ReadSpecialData(&olAwiCPA,clWhere,"URNO,APC4,VPFR,VPTO",false) == true)
		{
			if(olAwiCPA[0].Urno != pomAwi->Urno)
			{
				CTime olThisVafr, olThisVato, olReadVafr, olReadVato;
				olThisVafr = DateHourStringToDate(olVafrd,olVafrt);
				olThisVato = DateHourStringToDate(olVatod,olVatot);
				olReadVafr = olAwiCPA[0].Vafr;
				olReadVato = olAwiCPA[0].Vato;
				if(olThisVato == -1)
					olThisVato = CTime(2037, 12, 31, 23, 59, 0);
				if(olReadVato == -1)
					olReadVato = CTime(2037, 12, 31, 23, 59, 0);
				if(olReadVafr <= olThisVato && olReadVato >= olThisVafr)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING615) + CString("\n");
				}
			}
		}
		olAwiCPA.DeleteAll();



	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		m_Apc3.GetWindowText(pomAwi->Apc3,4);
		m_Apc4.GetWindowText(pomAwi->Apc4,5);
		m_Humi.GetWindowText(pomAwi->Humi,4);
		m_Temp.GetWindowText(pomAwi->Temp,4);
		m_Visi.GetWindowText(pomAwi->Visi,4);
		m_Wcod.GetWindowText(pomAwi->Wcod,4);
		m_Wdir.GetWindowText(pomAwi->Wdir,4);
		m_Wind.GetWindowText(pomAwi->Wind,4);
		m_Msgt.GetWindowText(pomAwi->Msgt,256);
		pomAwi->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomAwi->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_Apc3.SetFocus();
		m_Apc3.SetSel(0,-1);
	}
}

//---------------------------------------------------------------------------

void CAirPortWet::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------
