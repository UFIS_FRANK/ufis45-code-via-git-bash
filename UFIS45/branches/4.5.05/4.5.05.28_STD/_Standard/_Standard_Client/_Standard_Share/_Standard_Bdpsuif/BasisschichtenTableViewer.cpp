// BasisschichtenTableViewer.cpp 
//

#include <stdafx.h>
#include <BasisschichtenTableViewer.h>
#include <CedaOrgData.h>
#include <CedaPfcData.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void BasisschichtenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// BasisschichtenTableViewer
//

BasisschichtenTableViewer::BasisschichtenTableViewer(CCSPtrArray<BSDDATA> *popData)
{
	pomData = popData;

	ogOrgData.Read();
	ogPfcData.Read();

	bmIsFromSearch = FALSE;
    pomBasisschichtenTable = NULL;
    ogDdx.Register(this, BSD_CHANGE, CString("BASISSCHICHTENTABLEVIEWER"), CString("Basisschichten Update"), BasisschichtenTableCf);
    ogDdx.Register(this, BSD_NEW,    CString("BASISSCHICHTENTABLEVIEWER"), CString("Basisschichten New"),    BasisschichtenTableCf);
    ogDdx.Register(this, BSD_DELETE, CString("BASISSCHICHTENTABLEVIEWER"), CString("Basisschichten Delete"), BasisschichtenTableCf);
}

//-----------------------------------------------------------------------------------------------

BasisschichtenTableViewer::~BasisschichtenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::Attach(CCSTable *popTable)
{
    pomBasisschichtenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::ChangeViewTo(const char *pcpViewName)
{
	CStringArray olPossibleFilters;
	this->GetFilterPage(olPossibleFilters);
	if (olPossibleFilters.GetSize() == 1)
	{
		olPossibleFilters.RemoveAll();
		olPossibleFilters.Add("SORFILTER");
		olPossibleFilters.Add("SPFFILTER");
		this->AddFilterPage(olPossibleFilters);

		CStringArray olAllFilter;
		olAllFilter.Add(LoadStg(IDS_ALL));
		this->SetFilter("SORFILTER",olAllFilter);
		this->SetFilter("SPFFILTER",olAllFilter);
	}

    DeleteAll();    
    PrepareFilter();
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::MakeLines()
{
	int ilBasisschichtenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilBasisschichtenCount; ilLc++)
	{
		BSDDATA *prlBasisschichtenData = &pomData->GetAt(ilLc);
		MakeLine(prlBasisschichtenData);
	}
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::MakeLine(BSDDATA *prpBasisschichten)
{

	CString out;
    if( !IsPassFilter(prpBasisschichten)) 
		return;

    // Update viewer data for this shift record
    BASISSCHICHTENTABLE_LINEDATA rlBasisschichten;

	rlBasisschichten.Urno = prpBasisschichten->Urno;
	rlBasisschichten.Bsdc = prpBasisschichten->Bsdc;
	rlBasisschichten.Bsdk = prpBasisschichten->Bsdk;
	rlBasisschichten.Bsdn = prpBasisschichten->Bsdn;
	rlBasisschichten.Bsds = prpBasisschichten->Bsds;
	rlBasisschichten.Ctrc = prpBasisschichten->Ctrc;
	rlBasisschichten.Esbg = prpBasisschichten->Esbg;
	rlBasisschichten.Lsen = prpBasisschichten->Lsen;
	
	//SHA20000112
	//rlBasisschichten.Sdu1 = prpBasisschichten->Sdu1;
	out = DBmin2Gui(prpBasisschichten->Sdu1);
	rlBasisschichten.Sdu1=out;

	rlBasisschichten.Sex1 = prpBasisschichten->Sex1;
	rlBasisschichten.Ssh1 = prpBasisschichten->Ssh1;
	rlBasisschichten.Bkf1 = prpBasisschichten->Bkf1;
	rlBasisschichten.Bkt1 = prpBasisschichten->Bkt1;
	rlBasisschichten.Bkr1 = prpBasisschichten->Bkr1;
	
	//SHA20000112
	out = DBmin2Gui(prpBasisschichten->Bkd1);
	//rlBasisschichten.Bkd1 = prpBasisschichten->Bkd1;
	rlBasisschichten.Bkd1 = out;
	
	rlBasisschichten.Bewc = prpBasisschichten->Bewc;
	//uhi 16.3.01
	rlBasisschichten.Vafr = prpBasisschichten->Vafr.Format("%d.%m.%Y %H:%M");
	rlBasisschichten.Vato = prpBasisschichten->Vato.Format("%d.%m.%Y %H:%M");

	rlBasisschichten.Rema = prpBasisschichten->Rema;
	rlBasisschichten.Type = prpBasisschichten->Type;

	CreateLine(&rlBasisschichten);
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::CreateLine(BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareBasisschichten(prpBasisschichten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	BASISSCHICHTENTABLE_LINEDATA rlBasisschichten;
	rlBasisschichten = *prpBasisschichten;
    omLines.NewAt(ilLineno, rlBasisschichten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void BasisschichtenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 13;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomBasisschichtenTable->SetShowSelection(TRUE);
	pomBasisschichtenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 67; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 160;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 45; //!
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//rlHeader.Length = 33; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//rlHeader.Length = 33; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 45;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//uhi 16.3.01
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	rlHeader.Length = 200; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING255),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomBasisschichtenTable->SetHeaderFields(omHeaderDataArray);

	pomBasisschichtenTable->SetDefaultSeparator();
	pomBasisschichtenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
	CString out;

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Bsdc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bsdk;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bsdn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bsds;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Esbg;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lsen;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Sdu1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		//rlColumnData.Text = omLines[ilLineNo].Sex1;
		//olColList.NewAt(olColList.GetSize(), rlColumnData);
		//rlColumnData.Text = omLines[ilLineNo].Ssh1;
		//olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Bkd1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Bkf1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bkt1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bkr1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bewc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Type;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//uhi 16.3.01
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomBasisschichtenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomBasisschichtenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString BasisschichtenTableViewer::Format(BASISSCHICHTENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool BasisschichtenTableViewer::FindBasisschichten(char *pcpBasisschichtenKeya, char *pcpBasisschichtenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpBasisschichtenKeya) &&
			 (omLines[ilItem].Keyd == pcpBasisschichtenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void BasisschichtenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    BasisschichtenTableViewer *polViewer = (BasisschichtenTableViewer *)popInstance;
    if (ipDDXType == BSD_CHANGE || ipDDXType == BSD_NEW) polViewer->ProcessBasisschichtenChange((BSDDATA *)vpDataPointer);
    if (ipDDXType == BSD_DELETE) polViewer->ProcessBasisschichtenDelete((BSDDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::ProcessBasisschichtenChange(BSDDATA *prpBasisschichten)
{
	CString out;
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpBasisschichten->Bsdc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bsdk;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bsdn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bsds;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Ctrc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Esbg;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Lsen;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	out = DBmin2Gui(prpBasisschichten->Sdu1);
	rlColumn.Text=out;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//rlColumn.Text = prpBasisschichten->Sex1;
	//olLine.NewAt(olLine.GetSize(), rlColumn);
	//rlColumn.Text = prpBasisschichten->Ssh1;
	//olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Alignment = COLALIGN_RIGHT;
	out = DBmin2Gui(prpBasisschichten->Bkd1);
	rlColumn.Text = out;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpBasisschichten->Bkf1;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bkt1;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bkr1;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Bewc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Type;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	//uhi 16.3.01
	rlColumn.Text = prpBasisschichten->Vafr.Format("%d.%m.%Y %H:%M");
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBasisschichten->Vato.Format("%d.%m.%Y %H:%M");
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpBasisschichten->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpBasisschichten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomBasisschichtenTable->DeleteTextLine(ilItem);

		MakeLine(prpBasisschichten);
		if (FindLine(prpBasisschichten->Urno, ilItem))
		{
	        BASISSCHICHTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomBasisschichtenTable->AddTextLine(olLine, (void *)prlLine);
				pomBasisschichtenTable->DisplayTable();
			}
		}
		else
		{
			pomBasisschichtenTable->DisplayTable();
		}
	}
	else
	{
		MakeLine(prpBasisschichten);
		if (FindLine(prpBasisschichten->Urno, ilItem))
		{
	        BASISSCHICHTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomBasisschichtenTable->AddTextLine(olLine, (void *)prlLine);
				pomBasisschichtenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::ProcessBasisschichtenDelete(BSDDATA *prpBasisschichten)
{
	int ilItem;
	if (FindLine(prpBasisschichten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomBasisschichtenTable->DeleteTextLine(ilItem);
		pomBasisschichtenTable->DisplayTable();
	}
}

void BasisschichtenTableViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	CString olText;
	//int ilLc,ilCount;

	////Variablen f�r PSFlightPage

////Variablen f�r PSSpfPage
	omCMapForPfc.RemoveAll();		// Function
	bmUseAllPfc   = false;

////Variablen f�r PSSorPage
	omCMapForOrg.RemoveAll();		// organisation unit
	bmUseAllOrg  = false;

    GetFilter("SORFILTER", olFilterValues);
		
    int ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_ALL))
		{
			bmUseAllOrg = true;
		}
		else
		{
			for (int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForOrg[olFilterValues[ilLc]] = NULL;
			}
		}
	}

    GetFilter("SPFFILTER", olFilterValues);
		
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_ALL))
		{
			bmUseAllPfc = true;
		}
		else
		{
			for (int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForPfc[olFilterValues[ilLc]] = NULL;
			}
		}
	}

}

//-----------------------------------------------------------------------------------------------

bool BasisschichtenTableViewer::IsPassFilter(BSDDATA *prpBasisschichten)
{
	void *p;

	bool blIsPfcOk	= false;
	bool blIsOrgOk	= false;

	if(prpBasisschichten == NULL)
		return false;

	if (bmUseAllPfc == false)
	{
		blIsPfcOk = (omCMapForPfc.Lookup(CString(prpBasisschichten->Fctc),p) == TRUE) ? true:false;
	}
	else
	{
		blIsPfcOk = true;
	}

	if (bmUseAllOrg == false)
	{
		blIsOrgOk = (omCMapForOrg.Lookup(CString(prpBasisschichten->Dpt1),p) == TRUE) ? true:false;
	}
	else
	{
		blIsOrgOk = true;
	}


	return (blIsPfcOk && blIsOrgOk);

}

//-----------------------------------------------------------------------------------------------

int BasisschichtenTableViewer::CompareBasisschichten(BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten1, BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpBasisschichten1->Tifd == prpBasisschichten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpBasisschichten1->Tifd > prpBasisschichten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool BasisschichtenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void BasisschichtenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING164);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool BasisschichtenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
//		if(i!=13)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool BasisschichtenTableViewer::PrintTableLine(BASISSCHICHTENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	CString out;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
//		if(i!=15)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Bsdc;
				break;
			case 1:
				rlElement.Text = prpLine->Bsdk;
				break;
			case 2:
				rlElement.Text = prpLine->Bsdn;
				break;
			case 3:
				rlElement.Text = prpLine->Bsds;
				break;
			case 4:
				rlElement.Text = prpLine->Ctrc;
				break;
			case 5:
				rlElement.Text = prpLine->Esbg;
				break;
			case 6:
				rlElement.Text = prpLine->Lsen;
				break;
			case 7:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text = prpLine->Sdu1;
				break;
			case 8:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text = prpLine->Bkd1;
				break;
			case 9:
				rlElement.Text = prpLine->Bkf1;
				break;
			case 10:
				rlElement.Text = prpLine->Bkt1;
				break;
			case 11:
				rlElement.Text = prpLine->Bkr1;
				break;
			case 12:
				rlElement.Text = prpLine->Bewc;
				break;
			case 13:
				rlElement.Text = prpLine->Type;
				break;
			case 14:
				rlElement.Text = prpLine->Vafr;
				break;
			case 15:
				rlElement.Text = prpLine->Vato;
				break;
			case 16:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


//SHA20000112
// Umwandlung von 180 min aus DB 
// immer ins Format 03:00 (hh:mm) der Gui
CString BasisschichtenTableViewer::DBmin2Gui(CString dbmin)
{
	int		ilTmp = 0;
	div_t	divTmp;   
	CString slOut = "";
	

	if (!dbmin.IsEmpty())
	{
		ilTmp = atoi(dbmin);
		divTmp = div( ilTmp, 60 );
		if (divTmp.quot<10)
		{
			if (divTmp.rem<10)
			{
				slOut.Format("0%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("0%d:%d",divTmp.quot,divTmp.rem);
			}
		}
		else
		{
			if (divTmp.rem<10)
			{
				slOut.Format("%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("%d:%d",divTmp.quot,divTmp.rem);
			}
		}
	}
	else
	{
		slOut ="00:00";
	}

	return slOut;
}
