// TimeParametersTableViewer.cpp 
//

#include <stdafx.h>
#include <TimeParametersTableViewer.h>
#include <CCSDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void TimeParametersTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// TimeParametersTableViewer
//

TimeParametersTableViewer::TimeParametersTableViewer(CCSPtrArray<TIPDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomTimeParametersTable = NULL;
    ogDdx.Register(this, TIP_CHANGE, CString("TIMEPARAMETERSTABLEVIEWER"), CString("TimeParameters Update"), TimeParametersTableCf);
    ogDdx.Register(this, TIP_NEW,    CString("TIMEPARAMETERSTABLEVIEWER"), CString("TimeParameters New"),    TimeParametersTableCf);
    ogDdx.Register(this, TIP_DELETE, CString("TIMEPARAMETERSTABLEVIEWER"), CString("TimeParameters Delete"), TimeParametersTableCf);
}

//-----------------------------------------------------------------------------------------------

TimeParametersTableViewer::~TimeParametersTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::Attach(CCSTable *popTable)
{
    pomTimeParametersTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::MakeLines()
{
	int ilTimeParametersCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilTimeParametersCount; ilLc++)
	{
		TIPDATA *prlTimeParametersData = &pomData->GetAt(ilLc);
		MakeLine(prlTimeParametersData);
	}
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::MakeLine(TIPDATA *prpTimeParameters)
{

    //if( !IsPassFilter(prpTimeParameters)) return;

    // Update viewer data for this shift record
    TIMEPARAMETERSTABLE_LINEDATA rlTimeParameters;

	rlTimeParameters.Urno = prpTimeParameters->Urno;
	rlTimeParameters.Tipc = prpTimeParameters->Tipc;
	rlTimeParameters.Tipt = prpTimeParameters->Tipt;
	rlTimeParameters.Tipu = prpTimeParameters->Tipu;
	rlTimeParameters.Tipv = prpTimeParameters->Tipv;
	rlTimeParameters.Vafr = prpTimeParameters->Vafr.Format("%d.%m.%Y %H:%M");
	rlTimeParameters.Vato = prpTimeParameters->Vato.Format("%d.%m.%Y %H:%M");

	CreateLine(&rlTimeParameters);
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::CreateLine(TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareTimeParameters(prpTimeParameters, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	TIMEPARAMETERSTABLE_LINEDATA rlTimeParameters;
	rlTimeParameters = *prpTimeParameters;
    omLines.NewAt(ilLineno, rlTimeParameters);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void TimeParametersTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 5;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomTimeParametersTable->SetShowSelection(TRUE);
	pomTimeParametersTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 664; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING588),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomTimeParametersTable->SetHeaderFields(omHeaderDataArray);

	pomTimeParametersTable->SetDefaultSeparator();
	pomTimeParametersTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Tipc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tipu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Tipv;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Tipt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTimeParametersTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTimeParametersTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString TimeParametersTableViewer::Format(TIMEPARAMETERSTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool TimeParametersTableViewer::FindTimeParameters(char *pcpTimeParametersKeya, char *pcpTimeParametersKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpTimeParametersKeya) &&
			 (omLines[ilItem].Keyd == pcpTimeParametersKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void TimeParametersTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    TimeParametersTableViewer *polViewer = (TimeParametersTableViewer *)popInstance;
    if (ipDDXType == TIP_CHANGE || ipDDXType == TIP_NEW) polViewer->ProcessTimeParametersChange((TIPDATA *)vpDataPointer);
    if (ipDDXType == TIP_DELETE) polViewer->ProcessTimeParametersDelete((TIPDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::ProcessTimeParametersChange(TIPDATA *prpTimeParameters)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpTimeParameters->Tipc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTimeParameters->Tipu;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpTimeParameters->Tipv;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpTimeParameters->Tipt;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTimeParameters->Vafr.Format("%d.%m.%Y %H:%M");
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTimeParameters->Vato.Format("%d.%m.%Y %H:%M"); 
	olLine.NewAt(olLine.GetSize(), rlColumn);


	
	if (FindLine(prpTimeParameters->Urno, ilItem))
	{
        TIMEPARAMETERSTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpTimeParameters->Urno;
		prlLine->Tipc = prpTimeParameters->Tipc;
		prlLine->Tipt = prpTimeParameters->Tipt;
		prlLine->Tipu = prpTimeParameters->Tipu;
		prlLine->Tipv = prpTimeParameters->Tipv;
		prlLine->Vafr = prpTimeParameters->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpTimeParameters->Vato.Format("%d.%m.%Y %H:%M");

		pomTimeParametersTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomTimeParametersTable->DisplayTable();
	}
	else
	{
		MakeLine(prpTimeParameters);
		if (FindLine(prpTimeParameters->Urno, ilItem))
		{
	        TIMEPARAMETERSTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomTimeParametersTable->AddTextLine(olLine, (void *)prlLine);
				pomTimeParametersTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::ProcessTimeParametersDelete(TIPDATA *prpTimeParameters)
{
	int ilItem;
	if (FindLine(prpTimeParameters->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomTimeParametersTable->DeleteTextLine(ilItem);
		pomTimeParametersTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool TimeParametersTableViewer::IsPassFilter(TIPDATA *prpTimeParameters)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int TimeParametersTableViewer::CompareTimeParameters(TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters1, TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool TimeParametersTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void TimeParametersTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING197);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool TimeParametersTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=16)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool TimeParametersTableViewer::PrintTableLine(TIMEPARAMETERSTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=16)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Tipc;
				break;
			case 1:
				rlElement.Text		 = prpLine->Tipu;
				break;
			case 2:
				rlElement.Alignment	 = PRINT_RIGHT;
				rlElement.Text		 = prpLine->Tipv;
				break;
			case 3:
				rlElement.Text		 = prpLine->Tipt;
				break;
			case 4:
				rlElement.Text		 = prpLine->Vafr;
				break;
			case 5:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Vato;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
