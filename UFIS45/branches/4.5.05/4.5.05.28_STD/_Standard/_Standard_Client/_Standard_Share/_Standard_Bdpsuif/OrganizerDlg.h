#ifndef AFX_ORGANIZERDLG_H__A68B2B01_1AFF_11D2_8005_004095434A85__INCLUDED_
#define AFX_ORGANIZERDLG_H__A68B2B01_1AFF_11D2_8005_004095434A85__INCLUDED_

// OrganizerDlg.h : Header-Datei
//
#include <CedaChtData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld OrganizerDlg 

class OrganizerDlg : public CDialog
{
// Konstruktion
public:
	OrganizerDlg(CHTDATA *popCht,CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(OrganizerDlg)
	enum { IDD = IDD_ORGANIZERDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_CHTC;
	CCSEdit	m_CHTN;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(OrganizerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(OrganizerDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	CHTDATA *pomCht;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ORGANIZERDLG_H__A68B2B01_1AFF_11D2_8005_004095434A85__INCLUDED_
