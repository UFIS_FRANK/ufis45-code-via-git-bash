// RunwayTableViewer.cpp 
//

#include <stdafx.h>
#include <RunwayTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RunwayTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// RunwayTableViewer
//

RunwayTableViewer::RunwayTableViewer(CCSPtrArray<RWYDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomRunwayTable = NULL;
    ogDdx.Register(this, RWY_CHANGE, CString("RUNWAYTABLEVIEWER"), CString("Runway Update/new"), RunwayTableCf);
    ogDdx.Register(this, RWY_DELETE, CString("RUNWAYTABLEVIEWER"), CString("Runway Delete"), RunwayTableCf);
}

//-----------------------------------------------------------------------------------------------

RunwayTableViewer::~RunwayTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::Attach(CCSTable *popTable)
{
    pomRunwayTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::MakeLines()
{
	int ilRunwayCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilRunwayCount; ilLc++)
	{
		RWYDATA *prlRunwayData = &pomData->GetAt(ilLc);
		MakeLine(prlRunwayData);
	}
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::MakeLine(RWYDATA *prpRunway)
{

    //if( !IsPassFilter(prpRunway)) return;

    // Update viewer data for this shift record
    RUNWAYTABLE_LINEDATA rlRunway;
	rlRunway.Urno = prpRunway->Urno; 
	rlRunway.Rnam = prpRunway->Rnam; 
	rlRunway.Rnum = prpRunway->Rnum;  
	(prpRunway->Rtyp[0] == 'S' || prpRunway->Rtyp[0] == 'B') ? rlRunway.RtypS = cgYes : rlRunway.RtypS = cgNo;
	(prpRunway->Rtyp[0] == 'L' || prpRunway->Rtyp[0] == 'B') ? rlRunway.RtypL = cgYes : rlRunway.RtypL = cgNo;
	rlRunway.Nafr = prpRunway->Nafr.Format("%d.%m.%Y %H:%M"); 
	rlRunway.Nato = prpRunway->Nato.Format("%d.%m.%Y %H:%M"); 
	rlRunway.Resn = prpRunway->Resn;
	rlRunway.Vafr = prpRunway->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlRunway.Vato = prpRunway->Vato.Format("%d.%m.%Y %H:%M"); 

	rlRunway.Home = prpRunway->Home;

	CreateLine(&rlRunway);
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::CreateLine(RUNWAYTABLE_LINEDATA *prpRunway)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareRunway(prpRunway, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	RUNWAYTABLE_LINEDATA rlRunway;
	rlRunway = *prpRunway;
    omLines.NewAt(ilLineno, rlRunway);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void RunwayTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 9;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomRunwayTable->SetShowSelection(TRUE);
	pomRunwayTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING280),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomRunwayTable->SetHeaderFields(omHeaderDataArray);
	pomRunwayTable->SetDefaultSeparator();
	pomRunwayTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Rnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rnum;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].RtypS;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].RtypL;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Home;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomRunwayTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomRunwayTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString RunwayTableViewer::Format(RUNWAYTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL RunwayTableViewer::FindRunway(char *pcpRunwayKeya, char *pcpRunwayKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpRunwayKeya) &&
			 (omLines[ilItem].Keyd == pcpRunwayKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void RunwayTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RunwayTableViewer *polViewer = (RunwayTableViewer *)popInstance;
    if (ipDDXType == RWY_CHANGE) polViewer->ProcessRunwayChange((RWYDATA *)vpDataPointer);
    if (ipDDXType == RWY_DELETE) polViewer->ProcessRunwayDelete((RWYDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------
void RunwayTableViewer::ProcessRunwayChange(RWYDATA *prpRunway)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpRunway->Rnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Rnum;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(prpRunway->Rtyp[0] == 'S' || prpRunway->Rtyp[0] == 'B') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(prpRunway->Rtyp[0] == 'L' || prpRunway->Rtyp[0] == 'B') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Nafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Nato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Resn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpRunway->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpRunway->Home;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpRunway->Urno, ilItem))
	{
        RUNWAYTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpRunway->Urno;
		prlLine->Rnam = prpRunway->Rnam;
		prlLine->Rnum = prpRunway->Rnum;
		(prpRunway->Rtyp[0] == 'S' || prpRunway->Rtyp[0] == 'B') ? prlLine->RtypS = cgYes : prlLine->RtypS = cgNo;
		(prpRunway->Rtyp[0] == 'L' || prpRunway->Rtyp[0] == 'B') ? prlLine->RtypL = cgYes : prlLine->RtypL = cgNo;
		prlLine->Nafr = prpRunway->Nafr.Format("%d.%m.%Y %H:%M");
		prlLine->Nato = prpRunway->Nato.Format("%d.%m.%Y %H:%M");
		prlLine->Resn = prpRunway->Resn;
		prlLine->Home = prpRunway->Home;
		prlLine->Vafr = prpRunway->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpRunway->Vato.Format("%d.%m.%Y %H:%M");
	
		pomRunwayTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomRunwayTable->DisplayTable();
	}
	else
	{
		MakeLine(prpRunway);
		if (FindLine(prpRunway->Urno, ilItem))
		{
	        RUNWAYTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomRunwayTable->AddTextLine(olLine, (void *)prlLine);
				pomRunwayTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::ProcessRunwayDelete(RWYDATA *prpRunway)
{
	int ilItem;
	if (FindLine(prpRunway->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomRunwayTable->DeleteTextLine(ilItem);
		pomRunwayTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL RunwayTableViewer::IsPassFilter(RWYDATA *prpRunway)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int RunwayTableViewer::CompareRunway(RUNWAYTABLE_LINEDATA *prpRunway1, RUNWAYTABLE_LINEDATA *prpRunway2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpRunway1->Tifd == prpRunway2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpRunway1->Tifd > prpRunway2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL RunwayTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void RunwayTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING190);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool RunwayTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool RunwayTableViewer::PrintTableLine(RUNWAYTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Rnam;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Rnum;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->RtypS;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->RtypL;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Nafr;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Nato;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Resn;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Vato;
				}
				break;
			case 9:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Home;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
