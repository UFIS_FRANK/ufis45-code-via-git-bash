#if !defined(AFX_APPLICATION_H__74D21B55_918C_4A98_9053_643FCB33DBDB__INCLUDED_)
#define AFX_APPLICATION_H__74D21B55_918C_4A98_9053_643FCB33DBDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Application.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CApplication command target

class CApplication : public CCmdTarget
{
	DECLARE_DYNCREATE(CApplication)

	CApplication();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CApplication)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CApplication();

	// Generated message map functions
	//{{AFX_MSG(CApplication)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CApplication)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CApplication)
	afx_msg BSTR GetUsername();
	afx_msg void SetUsername(LPCTSTR lpszNewValue);
	afx_msg BSTR GetPassword();
	afx_msg void SetPassword(LPCTSTR lpszNewValue);
	afx_msg BSTR GetResourceTable();
	afx_msg void SetResourceTable(LPCTSTR lpszNewValue);
	afx_msg long GetResourceUrno();
	afx_msg void SetResourceUrno(long nNewValue);
	afx_msg BSTR GetCaller();
	afx_msg void SetCaller(LPCTSTR lpszNewValue);
	afx_msg BOOL SetCmdLine(LPCTSTR pszCmdLine);
	afx_msg BOOL DisplayResource();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	bool	Initialization();
	CDialog omDummyDlg;	
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_APPLICATION_H__74D21B55_918C_4A98_9053_643FCB33DBDB__INCLUDED_)
