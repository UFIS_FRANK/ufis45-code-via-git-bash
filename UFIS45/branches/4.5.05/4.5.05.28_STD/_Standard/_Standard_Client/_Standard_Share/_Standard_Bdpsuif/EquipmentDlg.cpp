// EquipmentDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <EquipmentDlg.h>
#include <PrivList.h>
#include <NotAvailableDlg.h>
#include <AwDlg.h>
#include <CedaEqtData.h>
#include <CedaPerData.h>
#include <EquipmentAttributDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EquipmentDlg dialog
static int ByNafrTifr(const BLKDATA **pppBlk1, const BLKDATA **pppBlk2)
{
	int ilRes = 0;
	if((**pppBlk1).Nafr > (**pppBlk2).Nafr) 
	{
		ilRes = 1;
	}
	else if((**pppBlk1).Nafr < (**pppBlk2).Nafr) 
	{
		ilRes = -1;
	}
	else
	{
		if((ilRes = strcmp((**pppBlk1).Tifr, (**pppBlk2).Tifr)) == 0)
			ilRes = strcmp((**pppBlk1).Tito, (**pppBlk2).Tito);
	}

	return ilRes;
}


EquipmentDlg::EquipmentDlg(EQUDATA *popEqu, VALDATA *popVal, CWnd* pParent /*=NULL*/) : CDialog(EquipmentDlg::IDD, pParent)
{
	pomEqu = popEqu;
	pomVal = popVal;

	pomTable = NULL;
	pomTable = new CCSTable;

	pomEqaTable = NULL;
	pomEqaTable = new CCSTable;
	
	imBurn = 0;		
	//{{AFX_DATA_INIT(EquipmentDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

EquipmentDlg::~EquipmentDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;
	delete pomEqaTable;

}

void EquipmentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EquipmentDlg)
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_EQANEW, m_EQANEW);
	DDX_Control(pDX, IDC_EQADEL, m_EQADEL);
	DDX_Control(pDX, IDC_EQA,	 m_EQAFRAME);
	DDX_Control(pDX, IDC_GCDE, m_GCDE);
	DDX_Control(pDX, IDC_GKEY,   m_GKEY);
	DDX_Control(pDX, IDC_CRQU,   m_CRQU);
	DDX_Control(pDX, IDC_ENAM, m_ENAM);
	DDX_Control(pDX, IDC_EQPS, m_EQPS);
	DDX_Control(pDX, IDC_ETYP, m_ETYP);
	DDX_Control(pDX, IDC_IVNR, m_IVNR);
	DDX_Control(pDX, IDC_TELE, m_TELE);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDOK,       m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_VAFR_D,	 m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T,	 m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D,	 m_VATOD);
	DDX_Control(pDX, IDC_VATO_T,	 m_VATOT);
	DDX_Control(pDX, IDC_B_EQT_AW,	 m_BEQTAW);
	DDX_Control(pDX, IDC_B_PER_AW,	 m_BPERAW);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EquipmentDlg, CDialog)
	//{{AFX_MSG_MAP(EquipmentDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_EQADEL, OnEqaDel)
	ON_BN_CLICKED(IDC_EQANEW, OnEqaNew)
	ON_BN_CLICKED(IDC_B_EQT_AW, OnBEqtAw)
	ON_BN_CLICKED(IDC_B_PER_AW, OnBPerAw)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EquipmentDlg message handlers
//----------------------------------------------------------------------------------------

BOOL EquipmentDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING867) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("EQUIPMENTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomEqu->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomEqu->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomEqu->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomEqu->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomEqu->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomEqu->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_USEU"));
	
	m_GCDE.SetTextLimit(1,5);
	m_GCDE.SetBKColor(YELLOW);  
	m_GCDE.SetTextErrColor(RED);
	m_GCDE.SetInitText(pomEqu->Gcde);
	m_GCDE.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_GCDE"));
	//------------------------------------
	m_ENAM.SetTextLimit(1,20);
	m_ENAM.SetBKColor(YELLOW);  
	m_ENAM.SetTextErrColor(RED);
	m_ENAM.SetInitText(pomEqu->Enam);
	m_ENAM.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_ENAM"));
	
	m_EQPS.SetTextLimit(0,5);
	m_EQPS.SetTextErrColor(RED);
	m_EQPS.SetInitText(pomEqu->Eqps);
	m_EQPS.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_EQPS"));
	
	m_IVNR.SetTextLimit(0,20);
	m_IVNR.SetTextErrColor(RED);
	m_IVNR.SetInitText(pomEqu->Ivnr);
	m_IVNR.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_IVNR"));

	m_TELE.SetTextLimit(0,10);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomEqu->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_TELE"));

	m_REMA.SetTextLimit(0,60);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomEqu->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_REMA"));

	m_ETYP.SetTextLimit(0,40);  
	m_ETYP.SetTextErrColor(RED);
	m_ETYP.SetInitText(pomEqu->Etyp);
	m_ETYP.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_ETYP"));

	//------------------------------------
	COleDateTime olToday = COleDateTime::GetCurrentTime();
	
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	if(pomVal->Vafr.GetStatus() == COleDateTime::valid)
		m_VAFRD.SetInitText(pomVal->Vafr.Format("%d.%m.%Y"));
	else
		m_VAFRD.SetInitText(olToday.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime();
	m_VAFRT.SetTextErrColor(RED);
	//m_VAFRT.SetBKColor(YELLOW);
	if(pomVal->Vafr.GetStatus() == COleDateTime::valid)
		m_VAFRT.SetInitText(pomVal->Vafr.Format("%H:%M"));
	else
		m_VAFRT.SetInitText("00:00");
	m_VAFRT.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	if(pomVal->Vato.GetStatus() == COleDateTime::valid)
		m_VATOD.SetInitText(pomVal->Vato.Format("%d.%m.%Y"));
	else
		m_VATOD.SetInitText("");
	m_VATOD.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	if(pomVal->Vato.GetStatus() == COleDateTime::valid)
		m_VATOT.SetInitText(pomVal->Vato.Format("%H:%M"));
	else
		m_VATOT.SetInitText("");
	m_VATOT.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_VATO"));
	//------------------------------------
	
	CString olGkey;
	char clWhere[100];

	m_GKEY.SetTextLimit(0,5);
	m_GKEY.SetBKColor(YELLOW);  
	m_GKEY.SetTextErrColor(RED);

	if(pomEqu->Gkey > 0)
	{
		CCSPtrArray<EQTDATA> olEqtCPA;
		olGkey.Format("%i", pomEqu->Gkey);
		sprintf(clWhere,"WHERE URNO='%s'",olGkey);
		if(ogEqtData.ReadSpecialData(&olEqtCPA,clWhere,"URNO,CODE,NAME",false) == false)
			m_GKEY.SetInitText("");
		else
			m_GKEY.SetInitText(olEqtCPA[0].Code);
			

		olEqtCPA.DeleteAll();
	}

	m_GKEY.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_GKEY"));

	CString olCrqu;
	
	m_CRQU.SetTextLimit(0,20);
	m_CRQU.SetTextErrColor(RED);

	if(pomEqu->Crqu > 0)
	{
		CCSPtrArray<PERDATA> olPerCPA;
		olCrqu.Format("%s", pomEqu->Crqu);
		sprintf(clWhere,"WHERE PRMC='%s'",olCrqu);
		if(ogPerData.ReadSpecialData(&olPerCPA,clWhere,"URNO,PRMC",false) == false)
			m_CRQU.SetInitText("");
		else
			m_CRQU.SetInitText(olPerCPA[0].Prmc);
			

		olPerCPA.DeleteAll();
	}

	m_CRQU.SetSecState(ogPrivList.GetStat("EQUIPMENTDLG.m_CRQU"));
	
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}

	CString olUrno;
	olUrno.Format("%d",pomEqu->Urno);
	MakeNoavTable("EQU", olUrno);
	bool bpCopied = false;

	if (pomEqu->Urno == 0)
	{
		olUrno.Format("%d",imBurn);
		bpCopied = true;
	}

	MakeEqaTable(olUrno,bpCopied);
	return TRUE;
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_GCDE.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_GCDE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_ENAM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_ENAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING859) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING859) + ogNotFormat;
		}
	}
	
	if(m_GKEY.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING862) + ogNotFormat;
	}

	if(m_CRQU.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING873) + ogNotFormat;
	}

	if(m_EQPS.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_EQPS.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING860) + ogNotFormat;
		}
	}

	if(m_IVNR.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_IVNR.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING863) + ogNotFormat;
		}
	}

	if(m_TELE.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_TELE.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING864) + ogNotFormat;
		}
	}

	if(m_REMA.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_REMA.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING805) + ogNotFormat;
		}
	}

	if(m_ETYP.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_ETYP.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING871) + ogNotFormat;
		}
	}

	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		
		/*if(m_VAFRT.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{*/
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		//}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olVafrd,olVafrt,olVatod,olVatot;
	COleDateTime olTmpTimeFr,olTmpTimeTo;

	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if(olVafrd.GetLength() > 0 && olVafrt.GetLength() == 0)
	{
		olVafrt = CString("0000");
	}
	if(olVatod.GetLength() > 0 && olVatot.GetLength() == 0)
	{
		olVatot = CString("2359");
	}
	/*if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}*/

	long ilGkey = 0;
	char clWhere[100];
	
	if (ilStatus == true)
	{
		CString olGkey;
		
		if (m_GKEY.GetWindowTextLength() != 0)
		{
			CCSPtrArray<EQTDATA> olEqtCPA;
			m_GKEY.GetWindowText(olGkey);
			sprintf(clWhere,"WHERE CODE='%s'",olGkey);
			if(ogEqtData.ReadSpecialData(&olEqtCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING849);
			}
			else
				ilGkey = olEqtCPA[0].Urno;
			
			olEqtCPA.DeleteAll();
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING849);
		}
	}

	if (ilStatus == true)
	{
		CString olCrqu;
		
		if(m_CRQU.GetWindowTextLength() != 0)
		{
			CCSPtrArray<PERDATA> olPerCPA;
			m_CRQU.GetWindowText(olCrqu);
			sprintf(clWhere,"WHERE PRMC='%s'",olCrqu);
			if(ogPerData.ReadSpecialData(&olPerCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING870);
			}
			
			olPerCPA.DeleteAll();
		}
	}

	if (ilStatus == true)
	{
		CString olGCDE;
		
		if(m_GCDE.GetWindowTextLength() != 0)
		{
			CCSPtrArray<EQUDATA> olEquCPA;
			m_GCDE.GetWindowText(olGCDE);
			sprintf(clWhere,"WHERE GCDE='%s'",olGCDE);
			if(ogEquData.ReadSpecialData(&olEquCPA,clWhere,"URNO",false) == true)
			{
				if(olEquCPA[0].Urno != this->pomEqu->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			
			olEquCPA.DeleteAll();
		}
	}

	if (ilStatus == true)
	{
		CString olENAM;
		
		if(m_ENAM.GetWindowTextLength() != 0)
		{
			CCSPtrArray<EQUDATA> olEquCPA;
			m_ENAM.GetWindowText(olENAM);
			sprintf(clWhere,"WHERE ENAM='%s'",olENAM);
			if(ogEquData.ReadSpecialData(&olEquCPA,clWhere,"URNO",false) == true)
			{
				if(olEquCPA[0].Urno != this->pomEqu->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
				}
			}
			
			olEquCPA.DeleteAll();
		}
	}

	if(ilStatus == true) //m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		//CString olText;
		COleDateTime olDate; 
		CTime        olTime;
		//m_VAFRD.GetWindowText(olText); 
		olDate = OleDateStringToDate(olVafrd);
		//m_VAFRT.GetWindowText(olText); 
		olTime = HourStringToDate(olVafrt);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			olTmpTimeFr = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
		}
		else
		{
			olTmpTimeFr.SetStatus(COleDateTime::null);
		}
		//m_VATOD.GetWindowText(olText); 
		olDate = OleDateStringToDate(olVatod);
		//m_VATOT.GetWindowText(olText); 
		olTime = HourStringToDate(olVatot);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			olTmpTimeTo = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
		}
		else
		{
			olTmpTimeTo.SetStatus(COleDateTime::null);
		}

		if(olTmpTimeTo.GetStatus() == COleDateTime::valid && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr.GetStatus() == COleDateTime::null))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if (ilStatus == true)
	{
		m_GCDE.GetWindowText(pomEqu->Gcde,6);
		m_ENAM.GetWindowText(pomEqu->Enam,21);
		m_EQPS.GetWindowText(pomEqu->Eqps,6);
		m_IVNR.GetWindowText(pomEqu->Ivnr,21);
		m_TELE.GetWindowText(pomEqu->Tele,11);
		m_REMA.GetWindowText(pomEqu->Rema,61);
		m_ETYP.GetWindowText(pomEqu->Etyp,21);
		m_CRQU.GetWindowText(pomEqu->Crqu,21);
		pomEqu->Gkey = ilGkey;
		pomVal->Vafr = olTmpTimeFr;
		pomVal->Vato = olTmpTimeTo;
		CDialog::OnOK();
	}	
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_GCDE.SetFocus();
		m_GCDE.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING140),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void EquipmentDlg::UpdateNoavTable()
{
	int ilLines = omBlkPtrA.GetSize();
	omBlkPtrA.Sort(ByNafrTifr);

	pomTable->ResetContent();
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		CTime olNafr = omBlkPtrA[ilLineNo].Nafr;
		CTime olNato = omBlkPtrA[ilLineNo].Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		rlColumnData.Text = olNafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = olNato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumnData.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumnData.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}
//----------------------------------------------------------------------------------------

void EquipmentDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		if(ipLineNo >= 0 && ipLineNo < omBlkPtrA.GetSize())
			omBlkPtrA[ipLineNo] = *prpBlk;
		UpdateNoavTable();
		
//		TABLE_COLUMN rlColumn;
//		CCSPtrArray <TABLE_COLUMN> olLine;
//
//		rlColumn.Lineno = 0;
//		rlColumn.VerticalSeparator = SEPA_NONE;
//		rlColumn.SeparatorType = SEPA_NONE;
//		rlColumn.BkColor = WHITE;
//		rlColumn.Font = &ogCourier_Regular_10;
//		rlColumn.Alignment = COLALIGN_LEFT;
//
//		CTime olNafr = prpBlk->Nafr;
//		CTime olNato = prpBlk->Nato;
//		if (this->imLastSelection == 2)	// local
//		{
//			ogBasicData.UtcToLocal(olNafr);
//			ogBasicData.UtcToLocal(olNato);
//		}
//
//		int ilColumnNo = 0;
//		rlColumn.Text = olNafr.Format("%d.%m.%Y %H:%M");
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//		rlColumn.Text = olNato.Format("%d.%m.%Y %H:%M"); 
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//		rlColumn.Text = prpBlk->Days;
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//
//		char pclTmp[10]="";
//		rlColumn.Text = "";
//		if(strlen(prpBlk->Tifr) > 0)
//		{
//			if (this->imLastSelection == 2)	// local
//			{
//				CString olText = prpBlk->Tifr;
//				CTime olCurrent = CTime::GetCurrentTime();
//				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);
//
//				ogBasicData.UtcToLocal(olTifr);
//				rlColumn.Text = olTifr.Format("%H:%M");
//			}
//			else
//			{
//				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
//				rlColumn.Text = pclTmp;
//			}
//		}
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//
//		rlColumn.Text = "";
//		if(strlen(prpBlk->Tito) > 0)
//		{
//			if (this->imLastSelection == 2)	// local
//			{
//				CString olText = prpBlk->Tito;
//				CTime olCurrent = CTime::GetCurrentTime();
//				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);
//
//				ogBasicData.UtcToLocal(olTito);
//				rlColumn.Text = olTito.Format("%H:%M");
//			}
//			else
//			{
//				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
//				rlColumn.Text = pclTmp;
//			}
//		}
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//
//		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//		rlColumn.Text = prpBlk->Resn;
//		rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
//
//		if(ipLineNo>-1)
//		{
//			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
//			pomTable->DisplayTable();
//		}
//		else
//		{
//			if(prpBlk != NULL)
//			{
//				pomTable->AddTextLine(olLine, (void *)prpBlk);
//				pomTable->DisplayTable();
//			}
//		}
//		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG EquipmentDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY*)lParam;

	if(polNotify->SourceTable == pomTable) 
	{
		if(ogPrivList.GetStat("EQUIPMENTDLG.m_NOAVCHA") == '1')
		{
			int ilLineNo = pomTable->GetCurSel();
			if (ilLineNo == -1)
			{
				MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
			}
			else
			{
				BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
				if (prlBlk != NULL)
				{
					BLKDATA rlBlk = *prlBlk;
					NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
					m_GCDE.GetWindowText(polDlg->omName);
					if(polDlg->DoModal() == IDOK)
					{
						*prlBlk = rlBlk;
						if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
						ChangeNoavTable(prlBlk, ilLineNo);
					}
					delete polDlg;
				}
			}
		}
	}
	else{
		if(polNotify->SourceTable == pomEqaTable){
			if(ogPrivList.GetStat("EQUIPMENTDLG.m_EQACHA") == '1')
			{
				int ilLineNo = pomEqaTable->GetCurSel();
				if (ilLineNo == -1)
				{
					MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
				}
				else
				{
					EQADATA *prlEqa = &omEqaPtrA[ilLineNo];
					if (prlEqa != NULL)
					{
						EQADATA rlEqa = *prlEqa;
						EquipmentAttributDlg *polDlg = new EquipmentAttributDlg(&rlEqa,this);
						if(polDlg->DoModal() == IDOK)
						{
							*prlEqa = rlEqa;
							if(prlEqa->IsChanged != DATA_NEW) prlEqa->IsChanged = DATA_CHANGED;
							ChangeEqaTable(prlEqa, ilLineNo);
						}
						delete polDlg;
					}
				}
			}
		}
	}

	return 0L;
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnNoavNew() 
{
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_NOAVNEW") != '1')
		return;

	BLKDATA rlBlk;
	NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
	m_GCDE.GetWindowText(polDlg->omName);
	if(polDlg->DoModal() == IDOK)
	{
		BLKDATA *prlBlk = new BLKDATA;
		*prlBlk = rlBlk;
		omBlkPtrA.Add(prlBlk);
		prlBlk->IsChanged = DATA_NEW;
		ChangeNoavTable(prlBlk, -1);
	}
	delete polDlg;
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnNoavDel() 
{
	
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_NOAVDEL") != '1')
		return;

	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if (&omBlkPtrA[ilLineNo] != NULL)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = omBlkPtrA[ilLineNo];
			prlBlk->IsChanged = DATA_DELETED;
			omDeleteBlkPtrA.Add(prlBlk);
			omBlkPtrA.DeleteAt(ilLineNo);
			ChangeNoavTable(NULL, ilLineNo);
		}
	}
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnBEqtAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<EQTDATA> olList;
	
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_BEQTAW") != '1')
		return;

	if(ogEqtData.ReadSpecialData(&olList, "order by CODE", "URNO,CODE,NAME", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Code));
			olCol2.Add(CString(olList[i].Name));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_GKEY.SetInitText(pomDlg->omReturnString);
			m_GKEY.SetFocus();
			
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
}

void EquipmentDlg::OnBPerAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PERDATA> olList;
	
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_BPERAW") != '1')
		return;

	if(ogPerData.ReadSpecialData(&olList, "order by PRMC", "URNO,PRMC,PRMN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Prmc));
			olCol2.Add(CString(olList[i].Prmn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_CRQU.SetInitText(pomDlg->omReturnString);
			m_CRQU.SetFocus();
			
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
}

void EquipmentDlg::MakeEqaTable(CString opBurn,bool bpCopied)
{
	CRect olRectBorder;
	m_EQAFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomEqaTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomEqaTable->SetSelectMode(0);
	pomEqaTable->SetShowSelection(TRUE);
	pomEqaTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 225; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING869),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 225; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING869),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	
	pomEqaTable->SetHeaderFields(olHeaderDataArray);
	pomEqaTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<EQADATA> olEqaCPA;
	sprintf(clWhere,"WHERE UEQU='%s'",opBurn);
	ogEqaData.ReadSpecialData(&omEqaPtrA,clWhere,"",false);
	int ilLines = omEqaPtrA.GetSize();

	pomEqaTable->SetDefaultSeparator();
	pomEqaTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omEqaPtrA[ilLineNo].Name;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omEqaPtrA[ilLineNo].Valu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomEqaTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
		if (bpCopied)
		{
			omEqaPtrA[ilLineNo].IsChanged = DATA_NEW;
		}
	}
	pomEqaTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::ChangeEqaTable(EQADATA *prpEqa, int ipLineNo)
{
	if(prpEqa != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;
		rlColumn.Text = prpEqa->Name;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpEqa->Valu; 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		
		if(ipLineNo>-1)
		{
			pomEqaTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpEqa);
			pomEqaTable->DisplayTable();
		}
		else
		{
			if(prpEqa != NULL)
			{
				pomEqaTable->AddTextLine(olLine, (void *)prpEqa);
				pomEqaTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomEqaTable->DeleteTextLine(ipLineNo);
			pomEqaTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnEqaNew() 
{
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_EQANEW") != '1')
		return;

	EQADATA rlEqa;
	EquipmentAttributDlg *polDlg = new EquipmentAttributDlg(&rlEqa,this);
	if(polDlg->DoModal() == IDOK)
	{
		EQADATA *prlEqa = new EQADATA;
		*prlEqa = rlEqa;
		omEqaPtrA.Add(prlEqa);
		prlEqa->IsChanged = DATA_NEW;
		ChangeEqaTable(prlEqa, -1);
	}
	delete polDlg;
}

//----------------------------------------------------------------------------------------

void EquipmentDlg::OnEqaDel() 
{
	if(ogPrivList.GetStat("EQUIPMENTDLG.m_EQADEL") != '1')
		return;

	int ilLineNo = pomEqaTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if (&omEqaPtrA[ilLineNo] != NULL)
		{
			EQADATA *prlEqa = new EQADATA;
			*prlEqa = omEqaPtrA[ilLineNo];
			prlEqa->IsChanged = DATA_DELETED;
			omDeleteEqaPtrA.Add(prlEqa);
			omEqaPtrA.DeleteAt(ilLineNo);
			ChangeEqaTable(NULL, ilLineNo);
		}
	}
}

//----------------------------------------------------------------------------------------
void EquipmentDlg::OnUTC() 
{
	if (imLastSelection == 1)
		return;

	imLastSelection = 1;

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void EquipmentDlg::OnLocal() 
{
	if (imLastSelection == 2)
		return;

	imLastSelection = 2;

	UpdateNoavTable();
}
