// CedaAwiData.cpp
 
#include <stdafx.h>
#include <CedaAwiData.h>
#include <resource.h>


void ProcessAwiCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAwiData::CedaAwiData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for AWIDataStruct
	BEGIN_CEDARECINFO(AWIDATA,AWIDataRecInfo)
		FIELD_CHAR_TRIM	(Apc3,"APC3")
		FIELD_CHAR_TRIM	(Apc4,"APC4")
		FIELD_CHAR_TRIM	(Humi,"HUMI")
		FIELD_CHAR_TRIM	(Msgt,"MSGT")
		FIELD_CHAR_TRIM	(Temp,"TEMP")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Visi,"VISI")
		FIELD_DATE		(Vafr,"VPFR")
		FIELD_DATE		(Vato,"VPTO")
		FIELD_CHAR_TRIM	(Wcod,"WCOD")
		FIELD_CHAR_TRIM	(Wdir,"WDIR")
		FIELD_CHAR_TRIM	(Wind,"WIND")

	END_CEDARECINFO //(AWIDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(AWIDataRecInfo)/sizeof(AWIDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AWIDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"AWI");
	strcpy(pcmListOfFields,"APC3,APC4,HUMI,MSGT,TEMP,URNO,VISI,VPFR,VPTO,WCOD,WDIR,WIND");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaAwiData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("APC3");
	ropFields.Add("APC4");
	ropFields.Add("HUMI");
	ropFields.Add("MSGT");
	ropFields.Add("TEMP");
	ropFields.Add("URNO");
	ropFields.Add("VISI");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");
	ropFields.Add("WCOD");
	ropFields.Add("WDIR");
	ropFields.Add("WIND");

	ropDesription.Add(LoadStg(IDS_STRING290));
	ropDesription.Add(LoadStg(IDS_STRING300));
	ropDesription.Add(LoadStg(IDS_STRING603));
	ropDesription.Add(LoadStg(IDS_STRING608));
	ropDesription.Add(LoadStg(IDS_STRING604));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING605));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING606));
	ropDesription.Add(LoadStg(IDS_STRING607));
	ropDesription.Add(LoadStg(IDS_STRING611));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAwiData::Register(void)
{
	ogDdx.Register((void *)this,BC_AWI_CHANGE,	CString("AWIDATA"), CString("Awi-changed"),	ProcessAwiCf);
	ogDdx.Register((void *)this,BC_AWI_NEW,		CString("AWIDATA"), CString("Awi-new"),		ProcessAwiCf);
	ogDdx.Register((void *)this,BC_AWI_DELETE,	CString("AWIDATA"), CString("Awi-deleted"),	ProcessAwiCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAwiData::~CedaAwiData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAwiData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAwiData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		AWIDATA *prlAwi = new AWIDATA;
		if ((ilRc = GetFirstBufferRecord(prlAwi)) == true)
		{
			omData.Add(prlAwi);//Update omData
			omUrnoMap.SetAt((void *)prlAwi->Urno,prlAwi);
		}
		else
		{
			delete prlAwi;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAwiData::Insert(AWIDATA *prpAwi)
{
	prpAwi->IsChanged = DATA_NEW;
	if(Save(prpAwi) == false) return false; //Update Database
	InsertInternal(prpAwi);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAwiData::InsertInternal(AWIDATA *prpAwi)
{
	ogDdx.DataChanged((void *)this, AWI_NEW,(void *)prpAwi ); //Update Viewer
	omData.Add(prpAwi);//Update omData
	omUrnoMap.SetAt((void *)prpAwi->Urno,prpAwi);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAwiData::Delete(long lpUrno)
{
	AWIDATA *prlAwi = GetAwiByUrno(lpUrno);
	if (prlAwi != NULL)
	{
		prlAwi->IsChanged = DATA_DELETED;
		if(Save(prlAwi) == false) return false; //Update Database
		DeleteInternal(prlAwi);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAwiData::DeleteInternal(AWIDATA *prpAwi)
{
	ogDdx.DataChanged((void *)this,AWI_DELETE,(void *)prpAwi); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAwi->Urno);
	int ilAwiCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAwiCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAwi->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAwiData::Update(AWIDATA *prpAwi)
{
	if (GetAwiByUrno(prpAwi->Urno) != NULL)
	{
		if (prpAwi->IsChanged == DATA_UNCHANGED)
		{
			prpAwi->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAwi) == false) return false; //Update Database
		UpdateInternal(prpAwi);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAwiData::UpdateInternal(AWIDATA *prpAwi)
{
	AWIDATA *prlAwi = GetAwiByUrno(prpAwi->Urno);
	if (prlAwi != NULL)
	{
		*prlAwi = *prpAwi; //Update omData
		ogDdx.DataChanged((void *)this,AWI_CHANGE,(void *)prlAwi); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

AWIDATA *CedaAwiData::GetAwiByUrno(long lpUrno)
{
	AWIDATA  *prlAwi;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAwi) == TRUE)
	{
		return prlAwi;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAwiData::ReadSpecialData(CCSPtrArray<AWIDATA> *popAwi,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","AWI",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","AWI",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAwi != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			AWIDATA *prpAwi = new AWIDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAwi,CString(pclFieldList))) == true)
			{
				popAwi->Add(prpAwi);
			}
			else
			{
				delete prpAwi;
			}
		}
		if(popAwi->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAwiData::Save(AWIDATA *prpAwi)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAwi->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAwi->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAwi);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAwi->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAwi->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAwi);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAwi->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAwi->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAwiCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAwiData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAwiData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAwiData;
	prlAwiData = (struct BcStruct *) vpDataPointer;
	AWIDATA *prlAwi;
	if(ipDDXType == BC_AWI_NEW)
	{
		prlAwi = new AWIDATA;
		GetRecordFromItemList(prlAwi,prlAwiData->Fields,prlAwiData->Data);
		if(ValidateAwiBcData(prlAwi->Urno)) //Prf: 8795
		{
			InsertInternal(prlAwi);
		}
		else
		{
			delete prlAwi;
		}
	}
	if(ipDDXType == BC_AWI_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlAwiData->Selection);
		prlAwi = GetAwiByUrno(llUrno);
		if(prlAwi != NULL)
		{
			GetRecordFromItemList(prlAwi,prlAwiData->Fields,prlAwiData->Data);
			if(ValidateAwiBcData(prlAwi->Urno)) //Prf: 8795
			{
				UpdateInternal(prlAwi);
			}
			else
			{
				DeleteInternal(prlAwi);
			}
		}
	}
	if(ipDDXType == BC_AWI_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlAwiData->Selection);

		prlAwi = GetAwiByUrno(llUrno);
		if (prlAwi != NULL)
		{
			DeleteInternal(prlAwi);
		}
	}
}

//Prf: 8795
//--ValidateAwiBcData--------------------------------------------------------------------------------------

bool CedaAwiData::ValidateAwiBcData(const long& lrpUrno)
{
	bool blValidateAwiBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<AWIDATA> olAwis;
		if(!ReadSpecialData(&olAwis,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateAwiBcData = false;
		}
	}
	return blValidateAwiBcData;
}

//---------------------------------------------------------------------------------------------------------
