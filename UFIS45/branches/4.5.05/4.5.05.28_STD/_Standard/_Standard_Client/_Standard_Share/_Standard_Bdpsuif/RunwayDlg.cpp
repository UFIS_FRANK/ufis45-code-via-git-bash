// RunwayDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <RunwayDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RunwayDlg dialog


RunwayDlg::RunwayDlg(RWYDATA *popRWY,CWnd* pParent /*=NULL*/) : CDialog(RunwayDlg::IDD, pParent)
{
	pomRWY = popRWY;

	//{{AFX_DATA_INIT(RunwayDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void RunwayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RunwayDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_RTYP_S, m_RTYPS);
	DDX_Control(pDX, IDC_RTYP_L, m_RTYPL);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_RESN,	 m_RESN);
	DDX_Control(pDX, IDC_RNAM,	 m_RNAM);
	DDX_Control(pDX, IDC_RNUM,	 m_RNUM);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_HOME, m_HOME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RunwayDlg, CDialog)
	//{{AFX_MSG_MAP(RunwayDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RunwayDlg message handlers
//----------------------------------------------------------------------------------------

BOOL RunwayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING190) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("RUNWAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomRWY->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomRWY->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomRWY->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomRWY->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomRWY->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomRWY->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_RNAM.SetFormat("x|#x|#x|#x|#");
	m_RNAM.SetTextLimit(1,4);
	m_RNAM.SetBKColor(YELLOW);
	m_RNAM.SetTextErrColor(RED);
	m_RNAM.SetInitText(pomRWY->Rnam);
	m_RNAM.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_RNAM"));
	//------------------------------------
	m_RNUM.SetFormat("x|#");
//	m_RNUM.SetBKColor(YELLOW);  
	m_RNUM.SetTextErrColor(RED);
	m_RNUM.SetInitText(pomRWY->Rnum);
	m_RNUM.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_RNUM"));
	//------------------------------------
	m_NAFRD.SetTypeToDate();
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomRWY->Nafr.Format("%d.%m.%Y"));
	m_NAFRD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_NAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime();
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomRWY->Nafr.Format("%H:%M"));
	m_NAFRT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_NAFR"));
	//------------------------------------
	m_NATOD.SetTypeToDate();
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomRWY->Nato.Format("%d.%m.%Y"));
	m_NATOD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_NATO"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime();
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomRWY->Nato.Format("%H:%M"));
	m_NATOT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_NATO"));
	//------------------------------------
	m_RESN.SetTypeToString("X(40)",40,0);
	m_RESN.SetTextErrColor(RED);
	m_RESN.SetInitText(pomRWY->Resn);
	m_RESN.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_RESN"));
	//------------------------------------
	m_HOME.SetTypeToString("X(5)",5,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomRWY->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_HOME"));
	//------------------------------------
	if (pomRWY->Rtyp[0] == 'S' || pomRWY->Rtyp[0] == 'B') m_RTYPS.SetCheck(1);
	clStat = ogPrivList.GetStat("RUNWAYDLG.m_RTYP");
	SetWndStatAll(clStat,m_RTYPS);
	if (pomRWY->Rtyp[0] == 'L' || pomRWY->Rtyp[0] == 'B') m_RTYPL.SetCheck(1);
	clStat = ogPrivList.GetStat("RUNWAYDLG.m_RTYP");
	SetWndStatAll(clStat,m_RTYPL);
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomRWY->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomRWY->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomRWY->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomRWY->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("RUNWAYDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void RunwayDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_RNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_RNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
/*	if(m_RNUM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_RNUM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING94) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING94) + ogNotFormat;
		}
	}
*/	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_RESN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING366) + ogNotFormat;
	}
	/*if(m_RTYPS.GetCheck() != 1 && m_RTYPL.GetCheck() != 1)
	{
		ilStatus = false;
		olErrorText += "Typ > Es mu� mindestens ein Typ selectiert werden\n";
	}*/
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING291);
	}
	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING292);
	}
	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING293);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olRnum,olRnam;
		char clWhere[100];
		CCSPtrArray<RWYDATA> olRwyCPA;

		m_RNUM.GetWindowText(olRnum);
		sprintf(clWhere,"WHERE RNUM='%s'",olRnum);
		if(ogRWYData.ReadSpecialData(&olRwyCPA,clWhere,"URNO,RNUM",false) == true)
		{
			if(olRwyCPA[0].Urno != pomRWY->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING94) + LoadStg(IDS_EXIST);
			}
		}
		olRwyCPA.DeleteAll();
		m_RNAM.GetWindowText(olRnam);
		sprintf(clWhere,"WHERE RNAM='%s'",olRnam);
		if(ogRWYData.ReadSpecialData(&olRwyCPA,clWhere,"URNO,RNAM",false) == true)
		{
			if(olRwyCPA[0].Urno != pomRWY->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olRwyCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_RNAM.GetWindowText(pomRWY->Rnam,5);
		m_RNUM.GetWindowText(pomRWY->Rnum,2);
		pomRWY->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomRWY->Vato = DateHourStringToDate(olVatod,olVatot);
		pomRWY->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		pomRWY->Nato = DateHourStringToDate(olNatod,olNatot);
		m_RESN.GetWindowText(pomRWY->Resn,41);
		m_HOME.GetWindowText(pomRWY->Home,3);
		if      (m_RTYPS.GetCheck() == 1 && m_RTYPL.GetCheck() == 1) pomRWY->Rtyp[0] = 'B';
		else if (m_RTYPS.GetCheck() == 1 && m_RTYPL.GetCheck() == 0) pomRWY->Rtyp[0] = 'S';
		else if (m_RTYPS.GetCheck() == 0 && m_RTYPL.GetCheck() == 1) pomRWY->Rtyp[0] = 'L';
		else     pomRWY->Rtyp[0] = 'N';
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_RNAM.SetFocus();
		m_RNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
