#if !defined(AFX_EXTLMGATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_)
#define AFX_EXTLMGATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosWaitingroomDlg.h : header file
//
#include <GatPosWaitingroomDlg.h>
#include <ExtLMDatGrid.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosWaitingroomDlg dialog

class ExtLMGatPosWaitingroomDlg : public GatPosWaitingroomDlg
{
// Construction
public:
	ExtLMGatPosWaitingroomDlg(WRODATA *popWRO,long lpOriginalUrno,CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(ExtLMGatPosWaitingroomDlg)
	enum { IDD = IDD_EXTLM_GATPOS_WAITINGROOMDLG };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExtLMGatPosWaitingroomDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExtLMGatPosWaitingroomDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDatNew();
	afx_msg void OnDatDelete();
	afx_msg void OnDatCopy();
	afx_msg void OnDatAssign();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	ExtLMDatGrid	omGrid;
	long			lmOriginalUrno;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTLMGATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_)
