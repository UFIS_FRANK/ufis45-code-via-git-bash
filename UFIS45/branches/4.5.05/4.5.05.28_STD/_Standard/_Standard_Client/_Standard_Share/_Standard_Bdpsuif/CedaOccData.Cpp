// CedaOccData.cpp
 
#include <stdafx.h>
#include <CedaOccData.h>
#include <resource.h>


void ProcessOccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOccData::CedaOccData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(OCCDATA, OccDataRecInfo)
		FIELD_LONG			(Urno,"URNO")
		FIELD_DATE			(Ocfr,"OCFR")
		FIELD_DATE			(Octo,"OCTO")
		FIELD_CHAR_TRIM		(Days,"DAYS")
		FIELD_CHAR_TRIM		(Rema,"REMA")
		FIELD_LONG			(Aloc,"ALOC")
		FIELD_LONG			(Alid,"ALID")
		FIELD_LONG			(Ocby,"OCBY")
		FIELD_CHAR_TRIM		(Octa,"OCTA")
		FIELD_CHAR_TRIM		(Tifr,"TIFR")
		FIELD_CHAR_TRIM		(Tito,"TITO")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(OccDataRecInfo)/sizeof(OccDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OccDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"OCC");



    sprintf(pcmListOfFields,"URNO,OCFR,OCTO,DAYS,REMA,ALOC,ALID,OCBY,OCTA,TIFR,TITO");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaOccData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	//ropFields.Add("");
	//ropDesription.Add(LoadStg());
	//ropType.Add("");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOccData::Register(void)
{
	ogDdx.Register((void *)this,BC_OCC_CHANGE,	CString("OCCDATA"), CString("Occ-changed"),	ProcessOccCf);
	ogDdx.Register((void *)this,BC_OCC_NEW,		CString("OCCDATA"), CString("Occ-new"),		ProcessOccCf);
	ogDdx.Register((void *)this,BC_OCC_DELETE,	CString("OCCDATA"), CString("Occ-deleted"),	ProcessOccCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOccData::~CedaOccData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOccData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOccData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		OCCDATA *prlOcc = new OCCDATA;
		if ((ilRc = GetFirstBufferRecord(prlOcc)) == true)
		{
			omData.Add(prlOcc);//Update omData
			omUrnoMap.SetAt((void *)prlOcc->Urno,prlOcc);
		}
		else
		{
			delete prlOcc;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOccData::Insert(OCCDATA *prpOcc)
{
	prpOcc->IsChanged = DATA_NEW;
	if(Save(prpOcc) == false) return false; //Update Database
	InsertInternal(prpOcc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOccData::InsertInternal(OCCDATA *prpOcc)
{
	ogDdx.DataChanged((void *)this, BLK_NEW,(void *)prpOcc ); //Update Viewer
	omData.Add(prpOcc);//Update omData
	omUrnoMap.SetAt((void *)prpOcc->Urno,prpOcc);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOccData::Delete(long lpUrno)
{
	OCCDATA *prlOcc = GetOccByUrno(lpUrno);
	if (prlOcc != NULL)
	{
		prlOcc->IsChanged = DATA_DELETED;
		if(Save(prlOcc) == false) return false; //Update Database
		DeleteInternal(prlOcc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOccData::DeleteInternal(OCCDATA *prpOcc)
{
	ogDdx.DataChanged((void *)this,BLK_DELETE,(void *)prpOcc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOcc->Urno);
	int ilOccCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOccCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpOcc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOccData::Update(OCCDATA *prpOcc)
{
	if (GetOccByUrno(prpOcc->Urno) != NULL)
	{
		if (prpOcc->IsChanged == DATA_UNCHANGED)
		{
			prpOcc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOcc) == false) return false; //Update Database
		UpdateInternal(prpOcc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOccData::UpdateInternal(OCCDATA *prpOcc)
{
	OCCDATA *prlOcc = GetOccByUrno(prpOcc->Urno);
	if (prlOcc != NULL)
	{
		*prlOcc = *prpOcc; //Update omData
		ogDdx.DataChanged((void *)this,BLK_CHANGE,(void *)prlOcc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

OCCDATA *CedaOccData::GetOccByUrno(long lpUrno)
{
	OCCDATA  *prlOcc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOcc) == TRUE)
	{
		return prlOcc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOccData::ReadSpecialData(CCSPtrArray<OCCDATA> *popOcc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	else
	{
		strcpy(pclFieldList, pcmListOfFields);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","OCC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","OCC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOcc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			OCCDATA *prpOcc = new OCCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpOcc,CString(pclFieldList))) == true)
			{
				popOcc->Add(prpOcc);
			}
			else
			{
				delete prpOcc;
			}
		}
		if(popOcc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOccData::Save(OCCDATA *prpOcc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOcc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOcc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOcc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		if(prpOcc->Urno == 0)
		{
			sprintf(pclSelection, "WHERE ALOC = '%ld' AND ALID = '%ld'", prpOcc->Aloc, prpOcc->Alid);
		}
		else
		{
			sprintf(pclSelection, "WHERE URNO = %ld", prpOcc->Urno);
		}
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOccData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOccData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOccData;
	prlOccData = (struct BcStruct *) vpDataPointer;
	OCCDATA *prlOcc;
	if(ipDDXType == BC_OCC_NEW)
	{
		prlOcc = new OCCDATA;
		GetRecordFromItemList(prlOcc,prlOccData->Fields,prlOccData->Data);
		InsertInternal(prlOcc);
	}
	if(ipDDXType == BC_OCC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlOccData->Selection);
		prlOcc = GetOccByUrno(llUrno);
		if(prlOcc != NULL)
		{
			GetRecordFromItemList(prlOcc,prlOccData->Fields,prlOccData->Data);
			UpdateInternal(prlOcc);
		}
	}
	if(ipDDXType == BC_OCC_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlOccData->Selection);

		prlOcc = GetOccByUrno(llUrno);
		if (prlOcc != NULL)
		{
			DeleteInternal(prlOcc);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
