#if !defined(AFX_LFZREGIDLG_H__56BF0B45_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_LFZREGIDLG_H__56BF0B45_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LFZRegiDlg.h : header file
//
#include <CedaACRData.h>
#include <CCSEdit.h>

//~~~~~ 10.08.99 SHA : 
#include <lfzregiext.h>
#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// LFZRegiDlg dialog

class LFZRegiDlg : public CDialog
{
// Construction
public:
	LFZRegiDlg(ACRDATA *popACR,CWnd* pParent = NULL,bool isnew=false);   // standard constructor

// Dialog Data
	//{{AFX_DATA(LFZRegiDlg)
	enum { IDD = IDD_LFZREGISTRATIONDLG };
	CCSButtonCtrl	m_BLACK_LIST;
	CCSEdit	m_MING;
	CCSEdit	m_REGI;
	CButton	m_OK;
	CString	m_Caption;
	CButton	m_APUI;
	CButton	m_MAIN;
	CCSEdit	m_ACT3;
	CCSEdit	m_ANNX;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_DEBI;
	CCSEdit	m_ENNA;
	CCSEdit	m_LADPD;
	CCSEdit	m_LADPT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_MOWE;
	CCSEdit	m_MTOW;
	CCSEdit	m_NOGA;
	CCSEdit	m_NOSE;
	CCSEdit	m_NOTO;
	CCSEdit	m_OWNE;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_REGN;
	CCSEdit	m_REMA;
	CCSEdit	m_SELC;
	CCSEdit	m_ACT5;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_ENTY;
	CCSEdit	m_GALL;
	CCSEdit	m_FRST;
	CCSEdit	m_REST;
	CCSEdit	m_EFIS;
	CCSEdit	m_ACTI;
	CCSEdit	m_ACRH;
	CCSEdit	m_ACRL;
	CCSEdit	m_ACRW;
	CCSEdit	m_CONF;
	CCSEdit	m_CONT;
	CCSEdit	m_CTRY;
	CCSEdit	m_DELI;
	CCSEdit	m_EXRG;
	CCSEdit	m_FUSE;
	CCSEdit	m_IATA;
	CCSEdit	m_ICAO;
	CCSEdit	m_LCOD;
	CCSEdit	m_NOIS;
	CCSEdit	m_ODAT;
	CCSEdit	m_POWD;
	CCSEdit	m_SCOD;
	CCSEdit	m_SERI;
	CCSEdit	m_STXT;
	CCSEdit	m_TYPE;
	CCSEdit	m_YEAR;
	CCSEdit	m_LSDF;
	CButton	m_CVTD;
	CButton	m_LSDB;
	CButton	m_LSFB;
	CButton	m_LSTB;
	CButton	m_OOPE;
	CButton	m_OOPT;
	CButton	m_OORD;
	CButton	m_OPBB;
	CButton	m_OPBF;
	CButton	m_OPWB;
	CButton	m_REGB;
	CButton	m_STRD;
	CButton	m_WFUB;
	CButton	m_WOBO;
	CButton	m_UFIS;
	CButton m_MTOW_DETAIL;
	CButton m_CODE_DETAIL;
	CCSEdit	m_CNAM;
	CCSEdit	m_PNUM;
	CCSEdit	m_HOM2;
	CCSEdit	m_NACO;
	CCSEdit	m_OADR;
	CCSEdit	m_TYPU;
	CCSEdit	m_CRW1;
	CCSEdit	m_CRW2;
	//}}AFX_DATA

	CLFZRegiExt dlgExt;
	CString	GetFormula();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LFZRegiDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LFZRegiDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
    afx_msg LONG OnSelChange(UINT wParam, LONG lParam);
	afx_msg void OnExtended();
	afx_msg	void OnBSelCode();	
	afx_msg	void OnBMtow();
	afx_msg	void OnBCode();
	afx_msg void OnBlackList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	DWORD	mdCodeDetailProcId;
	DWORD	mdMtowDetailProcId;
	DWORD	mdBlackListProcId;
	


private:
	void	OnAct3Code();
	void	OnAct5Code();
	

private:

	ACRDATA *pomACR;
	bool	bmActSelect;
	CString				omCodeDetailWindowName;
	CString				omMtowDetailWindowName;
	CString				omNtsFormula;
	bool ombNew;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LFZREGIDLG_H__56BF0B45_2049_11D1_B38A_0000C016B067__INCLUDED_)
