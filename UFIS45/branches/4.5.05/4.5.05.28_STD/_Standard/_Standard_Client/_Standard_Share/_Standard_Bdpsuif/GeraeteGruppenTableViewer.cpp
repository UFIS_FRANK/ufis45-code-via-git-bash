// GeraetegruppenTableViewer.cpp 
//

#include <stdafx.h>
#include <GeraetegruppenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void GeraetegruppenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// GeraetegruppenTableViewer
//

GeraetegruppenTableViewer::GeraetegruppenTableViewer(CCSPtrArray<GEGDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomGeraetegruppenTable = NULL;
    ogDdx.Register(this, GEG_CHANGE, CString("GERAETEGRUPPENTABLEVIEWER"), CString("Geraetegruppen Update"), GeraetegruppenTableCf);
    ogDdx.Register(this, GEG_NEW,    CString("GERAETEGRUPPENTABLEVIEWER"), CString("Geraetegruppen New"),    GeraetegruppenTableCf);
    ogDdx.Register(this, GEG_DELETE, CString("GERAETEGRUPPENTABLEVIEWER"), CString("Geraetegruppen Delete"), GeraetegruppenTableCf);
}

//-----------------------------------------------------------------------------------------------

GeraetegruppenTableViewer::~GeraetegruppenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::Attach(CCSTable *popTable)
{
    pomGeraetegruppenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::MakeLines()
{
	int ilGeraetegruppenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilGeraetegruppenCount; ilLc++)
	{
		GEGDATA *prlGeraetegruppenData = &pomData->GetAt(ilLc);
		MakeLine(prlGeraetegruppenData);
	}
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::MakeLine(GEGDATA *prpGeraetegruppen)
{

    //if( !IsPassFilter(prpGeraetegruppen)) return;

    GERAETEGRUPPENTABLE_LINEDATA rlGeraetegruppen;

	rlGeraetegruppen.Urno = prpGeraetegruppen->Urno;
	rlGeraetegruppen.Gcde = prpGeraetegruppen->Gcde;
	rlGeraetegruppen.Gcat = prpGeraetegruppen->Gcat;
	rlGeraetegruppen.Gnam = prpGeraetegruppen->Gnam;
	rlGeraetegruppen.Gsnm = prpGeraetegruppen->Gsnm;
	rlGeraetegruppen.Rema = prpGeraetegruppen->Rema;

	CreateLine(&rlGeraetegruppen);
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::CreateLine(GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareGeraetegruppen(prpGeraetegruppen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	GERAETEGRUPPENTABLE_LINEDATA rlGeraetegruppen;
	rlGeraetegruppen = *prpGeraetegruppen;
    omLines.NewAt(ilLineno, rlGeraetegruppen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void GeraetegruppenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 5;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomGeraetegruppenTable->SetShowSelection(TRUE);
	pomGeraetegruppenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING264),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING264),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING264),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING264),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING264),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomGeraetegruppenTable->SetHeaderFields(omHeaderDataArray);

	pomGeraetegruppenTable->SetDefaultSeparator();
	pomGeraetegruppenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Gcde;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gcat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gsnm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomGeraetegruppenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomGeraetegruppenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString GeraetegruppenTableViewer::Format(GERAETEGRUPPENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool GeraetegruppenTableViewer::FindGeraetegruppen(char *pcpGeraetegruppenKeya, char *pcpGeraetegruppenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpGeraetegruppenKeya) &&
			 (omLines[ilItem].Keyd == pcpGeraetegruppenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void GeraetegruppenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    GeraetegruppenTableViewer *polViewer = (GeraetegruppenTableViewer *)popInstance;
    if (ipDDXType == GEG_CHANGE || ipDDXType == GEG_NEW) polViewer->ProcessGeraetegruppenChange((GEGDATA *)vpDataPointer);
    if (ipDDXType == GEG_DELETE) polViewer->ProcessGeraetegruppenDelete((GEGDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::ProcessGeraetegruppenChange(GEGDATA *prpGeraetegruppen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpGeraetegruppen->Gcde;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGeraetegruppen->Gcat;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGeraetegruppen->Gsnm;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGeraetegruppen->Gnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpGeraetegruppen->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpGeraetegruppen->Urno, ilItem))
	{
        GERAETEGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpGeraetegruppen->Urno;
		prlLine->Gcde = prpGeraetegruppen->Gcde;
		prlLine->Gcat = prpGeraetegruppen->Gcat;
		prlLine->Gnam = prpGeraetegruppen->Gnam;
		prlLine->Gsnm = prpGeraetegruppen->Gsnm;
		prlLine->Rema = prpGeraetegruppen->Rema;


		pomGeraetegruppenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomGeraetegruppenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpGeraetegruppen);
		if (FindLine(prpGeraetegruppen->Urno, ilItem))
		{
	        GERAETEGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomGeraetegruppenTable->AddTextLine(olLine, (void *)prlLine);
				pomGeraetegruppenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::ProcessGeraetegruppenDelete(GEGDATA *prpGeraetegruppen)
{
	int ilItem;
	if (FindLine(prpGeraetegruppen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomGeraetegruppenTable->DeleteTextLine(ilItem);
		pomGeraetegruppenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool GeraetegruppenTableViewer::IsPassFilter(GEGDATA *prpGeraetegruppen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int GeraetegruppenTableViewer::CompareGeraetegruppen(GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen1, GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpGeraetegruppen1->Tifd == prpGeraetegruppen2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpGeraetegruppen1->Tifd > prpGeraetegruppen2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool GeraetegruppenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void GeraetegruppenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING180);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool GeraetegruppenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool GeraetegruppenTableViewer::PrintTableLine(GERAETEGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Gcde;
				break;
			case 1:
				rlElement.Text		= prpLine->Gcat;
				break;
			case 2:
				rlElement.Text		= prpLine->Gsnm;
				break;
			case 3:
				rlElement.Text		= prpLine->Gnam;
				break;
			case 4:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
