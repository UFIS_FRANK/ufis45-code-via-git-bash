// CedaAadData.cpp
 
#include <stdafx.h>
#include <CedaAadData.h>
#include <resource.h>


void ProcessAadCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAadData::CedaAadData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(AADDATA, AadDataRecInfo)
		FIELD_CHAR_TRIM	(Deid,"DEID")
		FIELD_CHAR_TRIM	(Moti,"MOTI")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AadDataRecInfo)/sizeof(AadDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AadDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"AAD");
    sprintf(pcmListOfFields,"DEID,MOTI,CDAT,LSTU,NAME,URNO,USEC,USEU,VAFR,VATO,NAFR,NATO");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//----------------------------------------------------------------------------------------------------

void CedaAadData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("DEID");
	ropFields.Add("MOTI");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAME");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");

	ropDesription.Add(LoadStg(IDS_STRING32807));
	ropDesription.Add(LoadStg(IDS_STRING32809));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING32808));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAadData::Register(void)
{
	ogDdx.Register((void *)this,BC_AAD_CHANGE,	CString("AADDATA"), CString("Aad-changed"),	ProcessAadCf);
	ogDdx.Register((void *)this,BC_AAD_NEW,		CString("AADDATA"), CString("Aad-new"),		ProcessAadCf);
	ogDdx.Register((void *)this,BC_AAD_DELETE,	CString("AADDATA"), CString("Aad-deleted"),	ProcessAadCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAadData::~CedaAadData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAadData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAadData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		AADDATA *prlAad = new AADDATA;
		if ((ilRc = GetFirstBufferRecord(prlAad)) == true)
		{
			omData.Add(prlAad);//Update omData
			omUrnoMap.SetAt((void *)prlAad->Urno,prlAad);
		}
		else
		{
			delete prlAad;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAadData::Insert(AADDATA *prpAad)
{
	prpAad->IsChanged = DATA_NEW;
	if(Save(prpAad) == false) return false; //Update Database
	InsertInternal(prpAad);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAadData::InsertInternal(AADDATA *prpAad)
{
	ogDdx.DataChanged((void *)this, AAD_NEW,(void *)prpAad ); //Update Viewer
	omData.Add(prpAad);//Update omData
	omUrnoMap.SetAt((void *)prpAad->Urno,prpAad);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAadData::Delete(long lpUrno)
{
	AADDATA *prlAad = GetAadByUrno(lpUrno);
	if (prlAad != NULL)
	{
		prlAad->IsChanged = DATA_DELETED;
		if(Save(prlAad) == false) return false; //Update Database
		DeleteInternal(prlAad);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAadData::DeleteInternal(AADDATA *prpAad)
{
	ogDdx.DataChanged((void *)this,AAD_DELETE,(void *)prpAad); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAad->Urno);
	int ilAADCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAADCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAad->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAadData::Update(AADDATA *prpAad)
{
	if (GetAadByUrno(prpAad->Urno) != NULL)
	{
		if (prpAad->IsChanged == DATA_UNCHANGED)
		{
			prpAad->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAad) == false) return false; //Update Database
		UpdateInternal(prpAad);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAadData::UpdateInternal(AADDATA *prpAad)
{
	AADDATA *prlAad = GetAadByUrno(prpAad->Urno);
	if (prlAad != NULL)
	{
		*prlAad = *prpAad; //Update omData
		ogDdx.DataChanged((void *)this,AAD_CHANGE,(void *)prlAad); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

AADDATA *CedaAadData::GetAadByUrno(long lpUrno)
{
	AADDATA  *prlAad;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAad) == TRUE)
	{
		return prlAad;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAadData::ReadSpecialData(CCSPtrArray<AADDATA> *popAAD,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","AAD",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","AAD",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAAD != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			AADDATA *prpAad = new AADDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAad,CString(pclFieldList))) == true)
			{
				popAAD->Add(prpAad);
			}
			else
			{
				delete prpAad;
			}
		}
		if(popAAD->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAadData::Save(AADDATA *prpAad)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAad->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAad->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAad);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAad->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAad->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAad);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAad->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAad->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAadCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAadData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAadData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAadData;
	prlAadData = (struct BcStruct *) vpDataPointer;
	AADDATA *prlAad;
	if(ipDDXType == BC_AAD_NEW)
	{
		prlAad = new AADDATA;
		GetRecordFromItemList(prlAad,prlAadData->Fields,prlAadData->Data);
		if(ValidateAadBcData(prlAad->Urno)) //Prf: 8795
		{
			InsertInternal(prlAad);
		}
		else
		{
			delete prlAad;
		}
	}
	if(ipDDXType == BC_AAD_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlAadData->Selection);
		prlAad = GetAadByUrno(llUrno);
		if(prlAad != NULL)
		{
			GetRecordFromItemList(prlAad,prlAadData->Fields,prlAadData->Data);
			if(ValidateAadBcData(prlAad->Urno)) //Prf: 8795
			{
				UpdateInternal(prlAad);
			}
			else
			{
				DeleteInternal(prlAad);
			}
		}
	}
	if(ipDDXType == BC_AAD_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlAadData->Selection);

		prlAad = GetAadByUrno(llUrno);
		if (prlAad != NULL)
		{
			DeleteInternal(prlAad);
		}
	}
}

//Prf: 8795
//--ValidateAadBcData--------------------------------------------------------------------------------------

bool CedaAadData::ValidateAadBcData(const long& lrpUrno)
{
	bool blValidateAadBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<AADDATA> olAads;
		if(!ReadSpecialData(&olAads,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateAadBcData = false;
		}
	}
	return blValidateAadBcData;
}
//---------------------------------------------------------------------------------------------------------
