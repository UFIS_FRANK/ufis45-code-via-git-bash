// CedaSorData.cpp
 
#include <stdafx.h>
#include <CedaSorData.h>
#include <resource.h>


void ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSorData::CedaSorData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SORDataStruct
	BEGIN_CEDARECINFO(SORDATA,SORDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		FIELD_CHAR_TRIM	(Odgc,"ODGC")
		FIELD_CHAR_TRIM	(Lead,"LEAD")
	END_CEDARECINFO //(SORDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SORDataRecInfo)/sizeof(SORDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SORDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SOR");
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,ODGC,LEAD");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("CODE");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");
	ropFields.Add("ODGC");
	ropFields.Add("LEAD");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropDesription.Add(LoadStg(IDS_STRING746));
	ropDesription.Add(LoadStg(IDS_STRING747));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSorData::Register(void)
{
	ogDdx.Register((void *)this,BC_SOR_CHANGE,	CString("SORDATA"), CString("Sor-changed"),	ProcessSorCf);
	ogDdx.Register((void *)this,BC_SOR_NEW,		CString("SORDATA"), CString("Sor-new"),		ProcessSorCf);
	ogDdx.Register((void *)this,BC_SOR_DELETE,	CString("SORDATA"), CString("Sor-deleted"),	ProcessSorCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSorData::~CedaSorData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSorData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();

	long llKey;
	CString *polValue;
	for (POSITION pos = this->omStaffOrgMap.GetStartPosition(); pos != NULL;)
	{
		this->omStaffOrgMap.GetNextAssoc(pos,(void *&)llKey,(void *&)polValue);
		delete polValue;
	}

	this->omStaffOrgMap.RemoveAll();

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSorData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();
	this->ClearAll(false);
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		// *** SHA 2001-01-15
		// *** Sort by validfrom (swissport!!! prf 82c)
		// *** sprintf nur in SOR ! rest hat gleiches where !
		sprintf(pspWhere,"%s order by vpfr desc,code",pspWhere);
		ilRc = CedaAction("RT", pspWhere);


	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SORDATA *prlSor = new SORDATA;
		if ((ilRc = GetFirstBufferRecord(prlSor)) == true)
		{
			omData.Add(prlSor);//Update omData
			omUrnoMap.SetAt((void *)prlSor->Urno,prlSor);
			bool blVptoIsOk = true;
			if(prlSor->Vpto.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpto < olCurrTime)
				{
					blVptoIsOk = false;
				}
			}

			if((prlSor->Vpfr < olCurrTime ) && blVptoIsOk)
			{
				omStaffUrnoMap.SetAt((void *)prlSor->Surn,prlSor);

				CString *polValue = NULL;
				if (this->omStaffOrgMap.Lookup((void *)prlSor->Surn,(void *&)polValue) == FALSE)
				{
					polValue = new CString();
					this->omStaffOrgMap.SetAt((void *)prlSor->Surn,(void *)polValue);
				}

				if (!(*polValue).IsEmpty())			
					(*polValue) += ogListSeparator;
				(*polValue) += prlSor->Code;
			}

		}
		else
		{
			delete prlSor;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSorData::Insert(SORDATA *prpSor)
{
	prpSor->IsChanged = DATA_NEW;
	if(Save(prpSor) == false) return false; //Update Database
	InsertInternal(prpSor);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSorData::InsertInternal(SORDATA *prpSor)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	omData.Add(prpSor);//Update omData
	omUrnoMap.SetAt((void *)prpSor->Urno,prpSor);
	bool blVptoIsOk = true;
	if(prpSor->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSor->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSor->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.SetAt((void *)prpSor->Surn,prpSor);
		CString *polValue = NULL;
		if (this->omStaffOrgMap.Lookup((void *)prpSor->Surn,(void *&)polValue) == FALSE)
		{
			polValue = new CString();
			this->omStaffOrgMap.SetAt((void *)prpSor->Surn,(void *)polValue);
		}

		if (!(*polValue).IsEmpty())			
			(*polValue) += ogListSeparator;
		(*polValue) += prpSor->Code;

	}
	ogDdx.DataChanged((void *)this, SOR_NEW,(void *)prpSor ); //Update Viewer

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSorData::Delete(long lpUrno)
{
	SORDATA *prlSor = GetSorByUrno(lpUrno);
	if (prlSor != NULL)
	{
		prlSor->IsChanged = DATA_DELETED;
		if(Save(prlSor) == false) return false; //Update Database
		DeleteInternal(prlSor);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSorData::DeleteInternal(SORDATA *prpSor)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	bool blVptoIsOk = true;
	if(prpSor->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSor->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSor->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.RemoveKey((void *)prpSor->Surn);
		omStaffOrgMap.RemoveKey((void *)prpSor->Surn);

		int ilSorCount = omData.GetSize();
		for (int ilLc = 0; ilLc < ilSorCount; ilLc++)
		{
			if (omData[ilLc].Urno == prpSor->Urno)
			{
				continue;
			}
			else
			{
				bool blVptoIsOk = true;
				if (omData[ilLc].Vpto.GetStatus() == COleDateTime::valid)
				{
					if(omData[ilLc].Vpto < olCurrTime)
					{
						blVptoIsOk = false;
					}
				}

				if ((omData[ilLc].Vpfr < olCurrTime ) && blVptoIsOk)
				{
					omStaffUrnoMap.SetAt((void *)omData[ilLc].Surn,&omData[ilLc]);

					CString *polValue = NULL;
					if (this->omStaffOrgMap.Lookup((void *)omData[ilLc].Surn,(void *&)polValue) == FALSE)
					{
						polValue = new CString();
						this->omStaffOrgMap.SetAt((void *)omData[ilLc].Surn,(void *)polValue);
					}

					if (!(*polValue).IsEmpty())			
						(*polValue) += ogListSeparator;
					(*polValue) += omData[ilLc].Code;

				}

			}
		}
	}


	ogDdx.DataChanged((void *)this,SOR_DELETE,(void *)prpSor); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSor->Urno);

	int ilSorCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSorCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSor->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSorData::Update(SORDATA *prpSor)
{
	if (GetSorByUrno(prpSor->Urno) != NULL)
	{
		if (prpSor->IsChanged == DATA_UNCHANGED)
		{
			prpSor->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSor) == false) return false; //Update Database
		UpdateInternal(prpSor);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSorData::UpdateInternal(SORDATA *prpSor)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	SORDATA *prlSor = GetSorByUrno(prpSor->Urno);
	if (prlSor != NULL)
	{
		*prlSor = *prpSor; //Update omData
		bool blVptoIsOk = true;
		if(prlSor->Vpto.GetStatus() == COleDateTime::valid)
		{
			if(prlSor->Vpto < olCurrTime)
			{
				blVptoIsOk = false;
			}
		}

		if((prlSor->Vpfr < olCurrTime ) && blVptoIsOk)
		{
			omStaffUrnoMap.SetAt((void *)prlSor->Surn,prlSor);
		}

		ogDdx.DataChanged((void *)this,SOR_CHANGE,(void *)prlSor); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SORDATA *CedaSorData::GetSorByUrno(long lpUrno)
{
	SORDATA  *prlSor;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSor) == TRUE)
	{
		return prlSor;
	}
	return NULL;
}

//--GET-BY-SURN--------------------------------------------------------------------------------------------

SORDATA *CedaSorData::GetValidSorBySurn(long lpSurn)
{
	SORDATA  *prlSor;
	if (omStaffUrnoMap.Lookup((void *)lpSurn,(void *& )prlSor) == TRUE)
	{
		return prlSor;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

CString CedaSorData::GetValidOrgBySurn(long lpSurn)
{
	CString  *prlValue;
	if (omStaffOrgMap.Lookup((void *)lpSurn,(void *& )prlValue) == TRUE)
	{
		return *prlValue;
	}
	return "";
}

//--GET-ALL BY-SURN--------------------------------------------------------------------------------------------

void CedaSorData::GetAllSorBySurn(long lpSurn,CCSPtrArray<SORDATA> &ropList)
{
	int ilSorCount = omData.GetSize();
	ropList.RemoveAll();
	for (int ilLc = 0; ilLc < ilSorCount; ilLc++)
	{
		if (omData[ilLc].Surn == lpSurn)
		{
			ropList.Add(&omData[ilLc]);
		}
	}
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSorData::ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SOR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SOR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSor != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SORDATA *prpSor = new SORDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSor,CString(pclFieldList))) == true)
			{
				popSor->Add(prpSor);
			}
			else
			{
				delete prpSor;
			}
		}
		if(popSor->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSorData::Save(SORDATA *prpSor)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSor->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSor->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSorData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSorData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSorData;
	prlSorData = (struct BcStruct *) vpDataPointer;
	SORDATA *prlSor;
	if(ipDDXType == BC_SOR_NEW)
	{
		prlSor = new SORDATA;
		GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
		InsertInternal(prlSor);
	}
	if(ipDDXType == BC_SOR_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSorData->Selection);
		prlSor = GetSorByUrno(llUrno);
		if(prlSor != NULL)
		{
			GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
			UpdateInternal(prlSor);
		}
	}
	if(ipDDXType == BC_SOR_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSorData->Selection);

		prlSor = GetSorByUrno(llUrno);
		if (prlSor != NULL)
		{
			DeleteInternal(prlSor);
		}
	}
}

//---------------------------------------------------------------------------------------------------------

