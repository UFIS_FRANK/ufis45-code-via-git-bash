#ifndef __BEWERTUNGSFAKTORENTABLEVIEWER_H__
#define __BEWERTUNGSFAKTORENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaAsfData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct BEWERTUNGSFAKTORENTABLE_LINEDATA
{

	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Bewc; 	// Code
	CString  Bewf; 	// Bewertungsfaktor
	CString  Bewn; 	// Bezeichnung
	CString  Rema; 	// Bemerkung

};

/////////////////////////////////////////////////////////////////////////////
// BewertungsfaktorenTableViewer

	  
class BewertungsfaktorenTableViewer : public CViewer
{
// Constructions
public:
    BewertungsfaktorenTableViewer(CCSPtrArray<ASFDATA> *popData);
    ~BewertungsfaktorenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ASFDATA *prpBewertungsfaktoren);
	int CompareBewertungsfaktoren(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren1, BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren2);
    void MakeLines();
	void MakeLine(ASFDATA *prpBewertungsfaktoren);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpLine);
	void ProcessBewertungsfaktorenChange(ASFDATA *prpBewertungsfaktoren);
	void ProcessBewertungsfaktorenDelete(ASFDATA *prpBewertungsfaktoren);
	bool FindBewertungsfaktoren(char *prpBewertungsfaktorenKeya, char *prpBewertungsfaktorenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomBewertungsfaktorenTable;
	CCSPtrArray<ASFDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<BEWERTUNGSFAKTORENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__BEWERTUNGSFAKTORENTABLEVIEWER_H__
