#if !defined(AFX_FIDSDEVICEDLG_H__E8F15453_4A26_11D7_8007_00010215BFDE__INCLUDED_)
#define AFX_FIDSDEVICEDLG_H__E8F15453_4A26_11D7_8007_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsDeviceDlg.h : header file
//
#include <CedaDEVData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// FidsDeviceDlg dialog

class FidsDeviceDlg : public CDialog
{
// Construction
public:
	FidsDeviceDlg(DEVDATA *popDEV,const CString& ropCaption,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(FidsDeviceDlg)
	enum { IDD = IDD_FIDS_DEVICE };
	CCSEdit	m_DLRD;
	CCSEdit	m_DLDC;
	CCSEdit	m_DLDT;
	CCSEdit	m_DLCT;
	CCSEdit	m_DRNA;
	CCSEdit	m_REMA;
	CCSEdit	m_LOCA;
	CCSEdit	m_DIVN;
	CCSEdit	m_DDGI;
	CCSEdit	m_DTER;
	CCSEdit	m_DFFD;
	CCSEdit	m_DFCO;
	CCSEdit	m_DRNP;
	CCSEdit	m_DARE;
	CCSEdit	m_DALZ;
	CCSEdit	m_ALGC;
	CCSEdit	m_ADID;
	CCSEdit	m_STAT;
	CCSEdit	m_DPID;
	CCSEdit	m_DPRT;
	CCSEdit	m_DEVN;
	CCSEdit	m_GRPN;
	CCSEdit	m_DADR;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CButton	m_CANCEL;
	CButton	m_OK;
	CButton	m_DRGI;
	CCSEdit	m_DBRF;
	CCSEdit m_DCOF;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsDeviceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsDeviceDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelectDisplay();
	afx_msg void OnSelectAlt3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString	m_Caption;
	DEVDATA *pomDEV;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSDEVICEDLG_H__E8F15453_4A26_11D7_8007_00010215BFDE__INCLUDED_)
