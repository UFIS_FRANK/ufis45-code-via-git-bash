// GatPosCheckinCounterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <GatPosCheckinCounterDlg.h>
#include <BasicData.h>
#include <PrivList.h>
#include <GatPosNotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosCheckinCounterDlg dialog


GatPosCheckinCounterDlg::GatPosCheckinCounterDlg(CICDATA *popCIC,CWnd* pParent /*=NULL*/)
	: CheckinCounterDlg(popCIC,GatPosCheckinCounterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

GatPosCheckinCounterDlg::GatPosCheckinCounterDlg(CICDATA *popCIC,int ipDlg,CWnd* pParent /*=NULL*/)
	: CheckinCounterDlg(popCIC,ipDlg, pParent)
{
	//{{AFX_DATA_INIT(GatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosCheckinCounterDlg::DoDataExchange(CDataExchange* pDX)
{
	CheckinCounterDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosCheckinCounterDlg)
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosCheckinCounterDlg, CheckinCounterDlg)
	//{{AFX_MSG_MAP(GatPosCheckinCounterDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosCheckinCounterDlg message handlers

void GatPosCheckinCounterDlg::OnOK() 
{
	// TODO: Add extra validation here
	this->pomCIC->Ibit = m_BlockingBitmap.GetCurSel();
	
	CheckinCounterDlg::OnOK();
}

BOOL GatPosCheckinCounterDlg::OnInitDialog() 
{
	CheckinCounterDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomCIC->Ibit);
	ogBasicData.SetWindowStat("CHECKINCOUNTERDLG.m_IBIT",&m_BlockingBitmap);

	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------

LONG GatPosCheckinCounterDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_CNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GatPosCheckinCounterDlg::OnNoavNew() 
{
	
	BLKDATA rlBlk;
	if(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_CNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}


