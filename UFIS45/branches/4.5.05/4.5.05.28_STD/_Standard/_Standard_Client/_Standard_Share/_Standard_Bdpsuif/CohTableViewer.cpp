// CohTableViewer.cpp 
//

#include <stdafx.h>
#include <CohTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void CohTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// CohTableViewer
//

CohTableViewer::CohTableViewer(CCSPtrArray<COHDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomCohTable = NULL;
    ogDdx.Register(this, COH_CHANGE, CString("COHTABLEVIEWER"), CString("Coh Update"), CohTableCf);
    ogDdx.Register(this, COH_NEW,    CString("COHTABLEVIEWER"), CString("Coh New"),    CohTableCf);
    ogDdx.Register(this, COH_DELETE, CString("COHTABLEVIEWER"), CString("Coh Delete"), CohTableCf);
}

//-----------------------------------------------------------------------------------------------

CohTableViewer::~CohTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::Attach(CCSTable *popTable)
{
    pomCohTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::MakeLines()
{
	int ilCohCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilCohCount; ilLc++)
	{
		COHDATA *prlCohData = &pomData->GetAt(ilLc);
		MakeLine(prlCohData);
	}
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::MakeLine(COHDATA *prpCoh)
{

    //if( !IsPassFilter(prpCoh)) return;

    // Update viewer data for this shift record
    COHTABLE_LINEDATA rlCoh;

	rlCoh.Urno = prpCoh->Urno;
	rlCoh.Cdat = prpCoh->Cdat;
	rlCoh.Lstu = prpCoh->Lstu;
	rlCoh.Ctrc = prpCoh->Ctrc;
	rlCoh.Fage = prpCoh->Fage;
	rlCoh.Hold = prpCoh->Hold;
	rlCoh.Usec = prpCoh->Usec;
	rlCoh.Useu = prpCoh->Useu;

	CreateLine(&rlCoh);
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::CreateLine(COHTABLE_LINEDATA *prpCoh)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCoh(prpCoh, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	COHTABLE_LINEDATA rlCoh;
	rlCoh = *prpCoh;
    omLines.NewAt(ilLineno, rlCoh);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CohTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomCohTable->SetShowSelection(TRUE);
	pomCohTable->ResetContent();

	// rro 08.07.2003:
	// old string: Contract|Kader-Code|Age|Holidays|ZIF Holidays
	// new string: Contract|Age|Holidays

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING760),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//rlHeader.Length = 40; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING760),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING760),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING760),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//rlHeader.Length = 120; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING760),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomCohTable->SetHeaderFields(omHeaderDataArray);
	pomCohTable->SetDefaultSeparator();
	pomCohTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Fage;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Hold;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCohTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomCohTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString CohTableViewer::Format(COHTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool CohTableViewer::FindCoh(char *pcpCohKeya, char *pcpCohKeyd, int& ilItem)
{
	return false;
}

//-----------------------------------------------------------------------------------------------

static void CohTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CohTableViewer *polViewer = (CohTableViewer *)popInstance;
    if (ipDDXType == COH_CHANGE || ipDDXType == COH_NEW) polViewer->ProcessCohChange((COHDATA *)vpDataPointer);
    if (ipDDXType == COH_DELETE) polViewer->ProcessCohDelete((COHDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void CohTableViewer::ProcessCohChange(COHDATA *prpCoh)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	CString olText;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpCoh->Ctrc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCoh->Fage;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCoh->Hold;
	//rlColumn.Columnno = ilColumnNo++;
	//olLine.NewAt(olLine.GetSize(), rlColumn);
	//rlColumn.Columnno = ilColumnNo++;
	//olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpCoh->Urno, ilItem))
	{
        COHTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Ctrc = prpCoh->Ctrc;
		prlLine->Fage = prpCoh->Fage;
		prlLine->Hold = prpCoh->Hold;
		prlLine->Cdat = prpCoh->Cdat;
		prlLine->Lstu = prpCoh->Lstu;
		prlLine->Usec = prpCoh->Usec;
		prlLine->Useu = prpCoh->Useu;
		prlLine->Urno = prpCoh->Urno;

		pomCohTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomCohTable->DisplayTable();
	}
	else
	{
		MakeLine(prpCoh);
		if (FindLine(prpCoh->Urno, ilItem))
		{
	        COHTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCohTable->AddTextLine(olLine, (void *)prlLine);
				pomCohTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::ProcessCohDelete(COHDATA *prpCoh)
{
	int ilItem;
	if (FindLine(prpCoh->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCohTable->DeleteTextLine(ilItem);
		pomCohTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool CohTableViewer::IsPassFilter(COHDATA *prpCoh)
{
	return true;
}

//-----------------------------------------------------------------------------------------------

int CohTableViewer::CompareCoh(COHTABLE_LINEDATA *prpCoh1, COHTABLE_LINEDATA *prpCoh2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void CohTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool CohTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void CohTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void CohTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING133);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for (int i = 0; i < ilLines; i++ ) 
			{
				if (pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if (pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool CohTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 3;
	for (int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if (rlElement.Length < igCCSPrintMinLength)
			rlElement.Length += igCCSPrintMoreLength; 
		rlElement.Text = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}

//-----------------------------------------------------------------------------------------------

bool CohTableViewer::PrintTableLine(COHTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 3;
	for(int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text		= prpLine->Ctrc;
			break;
		case 1:
			rlElement.Text		= prpLine->Fage;
			break;
		case 2:
			rlElement.FrameRight  = PRINT_FRAMETHIN;
			rlElement.Text		= prpLine->Hold;
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
