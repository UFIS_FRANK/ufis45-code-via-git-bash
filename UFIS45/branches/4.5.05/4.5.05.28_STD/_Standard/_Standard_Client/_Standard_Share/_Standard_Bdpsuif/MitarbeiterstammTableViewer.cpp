// MitarbeiterstammTableViewer.cpp 
//

#include <stdafx.h>
#include <MitarbeiterstammTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <CedaOrgData.h>
#include <CedaPfcData.h>
#include <CedaCotData.h>
#include <CedaStsData.h>
#include <CedaSreData.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void MitarbeiterstammTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// MitarbeiterstammTableViewer
//


enum {
		BY_SORCODE_ASC,
		BY_SORCODE_DSC,
		BY_SPFCODE_ASC,
		BY_SPFCODE_DSC,
		//BY_SCOCODE,
		BY_SPECODE_ASC,
		BY_SPECODE_DSC,
		BY_NONE
};



MitarbeiterstammTableViewer::MitarbeiterstammTableViewer(CCSPtrArray<STFDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomMitarbeiterstammTable = NULL;

	ogOrgData.Read();
	ogOrgData.Register();

	ogPfcData.Read();
	ogPfcData.Register();

	ogCotData.Read();
	ogCotData.Register();

	ogDdx.Register(this, STF_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, STF_NEW,    CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, STF_DELETE, CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm Delete"), MitarbeiterstammTableCf);

	ogDdx.Register(this, SOR_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Organisationseinheit Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, SOR_NEW,    CString("MitarbeiterstammTableViewer"), CString("Organisationseinheit New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, SOR_DELETE, CString("MitarbeiterstammTableViewer"), CString("Organisationseinheit Delete"), MitarbeiterstammTableCf);

	ogDdx.Register(this, SPF_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Funktion Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, SPF_NEW,    CString("MitarbeiterstammTableViewer"), CString("Funktion New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, SPF_DELETE, CString("MitarbeiterstammTableViewer"), CString("Funktion Delete"), MitarbeiterstammTableCf);

	ogDdx.Register(this, SPE_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Qualifikation Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, SPE_NEW,    CString("MitarbeiterstammTableViewer"), CString("Qualifikation New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, SPE_DELETE, CString("MitarbeiterstammTableViewer"), CString("Qualifikation Delete"), MitarbeiterstammTableCf);

	ogDdx.Register(this, SWG_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Arbeitsgruppe Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, SWG_NEW,    CString("MitarbeiterstammTableViewer"), CString("Arbeitsgruppe New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, SWG_DELETE, CString("MitarbeiterstammTableViewer"), CString("Arbeitsgruppe Delete"), MitarbeiterstammTableCf);
}

//-----------------------------------------------------------------------------------------------

MitarbeiterstammTableViewer::~MitarbeiterstammTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
	ogSorData.ClearAll();
	ogSpfData.ClearAll();
	ogSpeData.ClearAll();
	ogOrgData.ClearAll();
	ogPfcData.ClearAll();
	ogCotData.ClearAll();
	ogSwgData.ClearAll();
	ogSreData.ClearAll();

    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::Attach(CCSTable *popTable)
{
    pomMitarbeiterstammTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ChangeViewTo(const char *pcpViewName)
{
	CStringArray olPossibleFilters;
	this->GetFilterPage(olPossibleFilters);
	if (olPossibleFilters.GetSize() == 1)
	{
		olPossibleFilters.RemoveAll();
		olPossibleFilters.Add("SORFILTER");
		olPossibleFilters.Add("SPFFILTER");
		this->AddFilterPage(olPossibleFilters);

		CStringArray olAllFilter;
		olAllFilter.Add(LoadStg(IDS_ALL));
		this->SetFilter("SORFILTER",olAllFilter);
		this->SetFilter("SPFFILTER",olAllFilter);
	}

    DeleteAll();
    PrepareFilter();
    PrepareSorter();

    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::MakeLines()
{
	int ilMitarbeiterstammCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilMitarbeiterstammCount; ilLc++)
	{
		STFDATA *prlMitarbeiterstammData = &pomData->GetAt(ilLc);
		MakeLine(prlMitarbeiterstammData);
	}
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::MakeLine(STFDATA *prpMitarbeiterstamm)
{

    if(!IsPassFilter(prpMitarbeiterstamm)) 
		return;

    // Update viewer data for this shift record
    MITARBEITERSTAMMTABLE_LINEDATA rlMitarbeiterstamm;

	rlMitarbeiterstamm.Urno = prpMitarbeiterstamm->Urno;// Eindeutige Datensatz-Nr.
	rlMitarbeiterstamm.Finm = prpMitarbeiterstamm->Finm;// Vorname
	rlMitarbeiterstamm.Gsmn = prpMitarbeiterstamm->Gsmn;// GSM-Number
	rlMitarbeiterstamm.Ktou = prpMitarbeiterstamm->Ktou;// Urlaubsanspr�che
	rlMitarbeiterstamm.Lanm = prpMitarbeiterstamm->Lanm;// Name
	rlMitarbeiterstamm.Lino = prpMitarbeiterstamm->Lino;// License Number
	rlMitarbeiterstamm.Makr = prpMitarbeiterstamm->Makr;// Mitarbeiterkreis
	rlMitarbeiterstamm.Matr = prpMitarbeiterstamm->Matr;// Matricula
	rlMitarbeiterstamm.Peno = prpMitarbeiterstamm->Peno;// Personalnummer
	rlMitarbeiterstamm.Perc = prpMitarbeiterstamm->Perc;// K�rzel
	rlMitarbeiterstamm.Prfl = prpMitarbeiterstamm->Prfl;// Protokollierungskennung
	rlMitarbeiterstamm.Rema = prpMitarbeiterstamm->Rema;// Bemerkungen
	rlMitarbeiterstamm.Shnm = prpMitarbeiterstamm->Shnm;// Kurzname
	rlMitarbeiterstamm.Sken = prpMitarbeiterstamm->Sken;// Schichtkennzeichen
	rlMitarbeiterstamm.Teld = prpMitarbeiterstamm->Teld;// Telefonnr. dienstlich
	rlMitarbeiterstamm.Telh = prpMitarbeiterstamm->Telh;// Telefonnr. Handy
	rlMitarbeiterstamm.Telp = prpMitarbeiterstamm->Telp;// Telefonnr. privat
	rlMitarbeiterstamm.Tohr = prpMitarbeiterstamm->Tohr;// Soll an SAP-HR �bermittelt werden
	rlMitarbeiterstamm.Usec = prpMitarbeiterstamm->Usec;// Anwender (Ersteller)
	rlMitarbeiterstamm.Useu = prpMitarbeiterstamm->Useu;// Anwender (letzte �nderung)
	rlMitarbeiterstamm.Zutb = prpMitarbeiterstamm->Zutb;// Zus�tzliche Urlaubstage Schwerbehinderung
	rlMitarbeiterstamm.Zutf = prpMitarbeiterstamm->Zutf;// Zus�tzliche Urlaubstage Freistellungstage
	rlMitarbeiterstamm.Zutn = prpMitarbeiterstamm->Zutn;// Zus�tzliche Urlaubstage Nachtschicht
	rlMitarbeiterstamm.Zutw = prpMitarbeiterstamm->Zutw;// Zus�tzliche Urlaubstage Wechselschicht
	rlMitarbeiterstamm.Pnof = prpMitarbeiterstamm->Pnof;// fiktive Personalnummer
	rlMitarbeiterstamm.Pdgl = GetHHMMFromMinutes(prpMitarbeiterstamm->Pdgl,true);// pers�nliche durchschnittliche Ganztouenl�nge
	rlMitarbeiterstamm.Pdkl = GetHHMMFromMinutes(prpMitarbeiterstamm->Pdkl,true);// pers�nliche durchschnittliche Kurztouenl�nge
	rlMitarbeiterstamm.Pmag = prpMitarbeiterstamm->Pmag;// pers�nliche mtl Ganztouen anz.
	rlMitarbeiterstamm.Pmak = prpMitarbeiterstamm->Pmak;// pers�nliche mtl Kurztouen anz.
	rlMitarbeiterstamm.Rels = prpMitarbeiterstamm->Rels;// Dienstplan/Mitarbeiter freigeben
	rlMitarbeiterstamm.Kind = prpMitarbeiterstamm->Kind;// Geschlecht
	if (strcmp(prpMitarbeiterstamm->Kids,"1") == 0)		// Schulpflichtige Kinder
		rlMitarbeiterstamm.Kids = "Y";
	else
		rlMitarbeiterstamm.Kids = "N";
	rlMitarbeiterstamm.Nati = prpMitarbeiterstamm->Nati;// Nationalit�t
	rlMitarbeiterstamm.Gebu = prpMitarbeiterstamm->Gebu;// Geburtstag
	rlMitarbeiterstamm.Regi = prpMitarbeiterstamm->Regi;// Regelm�ssigkeitsindikator
	if(prpMitarbeiterstamm->Dodm.GetStatus() == COleDateTime::valid)
	{
		rlMitarbeiterstamm.Dodm = prpMitarbeiterstamm->Dodm.Format("%d.%m.%Y");	// Austrittsdatum
	}
	if(prpMitarbeiterstamm->Doem.GetStatus() == COleDateTime::valid)
	{
		rlMitarbeiterstamm.Doem = prpMitarbeiterstamm->Doem.Format("%d.%m.%Y");	// Eintrittsdatum
	}
	if(prpMitarbeiterstamm->Dobk.GetStatus() == COleDateTime::valid)
	{
		rlMitarbeiterstamm.Dobk = prpMitarbeiterstamm->Dobk.Format("%d.%m.%Y");	// Wiedereintrittsdatum
	}

	rlMitarbeiterstamm.SorCode = "";
	rlMitarbeiterstamm.SpfCode = "";
	rlMitarbeiterstamm.SpeCode = "";
	rlMitarbeiterstamm.SwgCode = "";
	


	SPEDATA *prlSpe = ogSpeData.GetValidSpeBySurn(prpMitarbeiterstamm->Urno);
	if(prlSpe != NULL)
		rlMitarbeiterstamm.SpeCode = prlSpe->Code;
	
	SORDATA *prlSor = ogSorData.GetValidSorBySurn(prpMitarbeiterstamm->Urno);
	if(prlSor != NULL)
	{
		rlMitarbeiterstamm.SorCode = prlSor->Code;
	}
	
	SPFDATA *prlSpf = ogSpfData.GetValidSpfBySurn(prpMitarbeiterstamm->Urno);
	if(prlSpf != NULL)
	{
		rlMitarbeiterstamm.SpfCode = prlSpf->Code;
	}
	
	SWGDATA *prlSwg = ogSwgData.GetValidSwgBySurn(prpMitarbeiterstamm->Urno);
	if(prlSwg != NULL)
	{
		rlMitarbeiterstamm.SwgCode = prlSwg->Code;
	}
	


	CreateLine(&rlMitarbeiterstamm);
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::CreateLine(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareMitarbeiterstamm(prpMitarbeiterstamm, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	MITARBEITERSTAMMTABLE_LINEDATA rlMitarbeiterstamm;
	rlMitarbeiterstamm = *prpMitarbeiterstamm;
    omLines.NewAt(ilLineno, rlMitarbeiterstamm);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void MitarbeiterstammTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 25;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomMitarbeiterstammTable->SetShowSelection(TRUE);
	pomMitarbeiterstammTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Lanm
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Finm
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Peno
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	rlHeader.Length = 75; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//SorCode
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 75; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//SpfCode
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 75; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//SpeCode
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 85; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//SwgCode
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Shnm
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Perc
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Teld
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Telh
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Telp
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 10; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Kind
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Pdgl
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Pdkl
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Pmag
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Pmak
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 10; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Regi
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 24; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Nati
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 24; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Kids
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Doem
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Dodm
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Dobk
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');//Rema
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomMitarbeiterstammTable->SetHeaderFields(omHeaderDataArray);
	pomMitarbeiterstammTable->SetDefaultSeparator();
	pomMitarbeiterstammTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Lanm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Finm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Peno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].SorCode;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].SpfCode;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].SpeCode;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].SwgCode;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Shnm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Perc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Teld;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Telh;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Telp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Kind;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Pdgl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pdkl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pmag;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pmak;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Regi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nati;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Kids;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Doem;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dodm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dobk;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomMitarbeiterstammTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomMitarbeiterstammTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString MitarbeiterstammTableViewer::Format(MITARBEITERSTAMMTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::FindMitarbeiterstamm(char *pcpMitarbeiterstammKeya, char *pcpMitarbeiterstammKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpMitarbeiterstammKeya) &&
			 (omLines[ilItem].Keyd == pcpMitarbeiterstammKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void MitarbeiterstammTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    MitarbeiterstammTableViewer *polViewer = (MitarbeiterstammTableViewer *)popInstance;
	switch(ipDDXType)
	{
	case STF_CHANGE:
	case STF_NEW :
		polViewer->ProcessMitarbeiterstammChange((STFDATA *)vpDataPointer);
	break;
	case SOR_NEW :
	case SOR_CHANGE :
	case SOR_DELETE :
		{
			SORDATA *prlSor = (SORDATA *)vpDataPointer;
			if (prlSor)
			{
				STFDATA *prlStf = ogStfData.GetStfByUrno(prlSor->Surn);
				if (prlStf)
				{
					polViewer->ProcessMitarbeiterstammChange(prlStf);
				}
			}
		}
	break;
	case SPF_NEW :
	case SPF_CHANGE :
	case SPF_DELETE :
		{
			SPFDATA *prlSpf = (SPFDATA *)vpDataPointer;
			if (prlSpf)
			{
				STFDATA *prlStf = ogStfData.GetStfByUrno(prlSpf->Surn);
				if (prlStf)
				{
					polViewer->ProcessMitarbeiterstammChange(prlStf);
				}
			}
		}
	break;
	case SPE_NEW :
	case SPE_CHANGE :
	case SPE_DELETE :
		{
			SPEDATA *prlSpe = (SPEDATA *)vpDataPointer;
			if (prlSpe)
			{
				STFDATA *prlStf = ogStfData.GetStfByUrno(prlSpe->Surn);
				if (prlStf)
				{
					polViewer->ProcessMitarbeiterstammChange(prlStf);
				}
			}
		}
	break;
	case SWG_NEW :
	case SWG_CHANGE :
	case SWG_DELETE :
		{
			SWGDATA *prlSwg = (SWGDATA *)vpDataPointer;
			if (prlSwg)
			{
				STFDATA *prlStf = ogStfData.GetStfByUrno(prlSwg->Surn);
				if (prlStf)
				{
					polViewer->ProcessMitarbeiterstammChange(prlStf);
				}
			}
		}
	break;
	case STF_DELETE :
		polViewer->ProcessMitarbeiterstammDelete((STFDATA *)vpDataPointer);
	break;
	}

} 


//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ProcessMitarbeiterstammChange(STFDATA *prpMitarbeiterstamm)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpMitarbeiterstamm->Lanm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Finm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Peno;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	//uhi 7.9.01
	SORDATA *prlSor = ogSorData.GetValidSorBySurn(prpMitarbeiterstamm->Urno);
	if(prlSor != NULL)
		rlColumn.Text = prlSor->Code;
	else
		rlColumn.Text = "";

	olLine.NewAt(olLine.GetSize(), rlColumn);

	SPFDATA *prlSpf = ogSpfData.GetValidSpfBySurn(prpMitarbeiterstamm->Urno);
	if(prlSpf != NULL)
		rlColumn.Text = prlSpf->Code;
	else
		rlColumn.Text = "";

	olLine.NewAt(olLine.GetSize(), rlColumn);

	SPEDATA *prlSpe = ogSpeData.GetValidSpeBySurn(prpMitarbeiterstamm->Urno);
	if(prlSpe != NULL){
		rlColumn.Text = prlSpe->Code;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	else{
		rlColumn.Text = CString("");	
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	SWGDATA *prlSwg = ogSwgData.GetValidSwgBySurn(prpMitarbeiterstamm->Urno);
	if(prlSwg != NULL){
		rlColumn.Text = prlSwg->Code;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	else{
		rlColumn.Text = CString("");	
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	rlColumn.Text = prpMitarbeiterstamm->Shnm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Perc;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Teld;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Telh;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Telp;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Kind;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = GetHHMMFromMinutes(prpMitarbeiterstamm->Pdgl,true);	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = GetHHMMFromMinutes(prpMitarbeiterstamm->Pdkl,true);	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Pmag;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Pmak;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpMitarbeiterstamm->Regi;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Nati;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if (strcmp(prpMitarbeiterstamm->Kids,"1") == 0)
		rlColumn.Text = "Y";	
	else
		rlColumn.Text = "N";	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = "";
	if(prpMitarbeiterstamm->Doem.GetStatus() == COleDateTime::valid)
	{
		rlColumn.Text = prpMitarbeiterstamm->Doem.Format("%d.%m.%Y");	
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = "";
	if(prpMitarbeiterstamm->Dodm.GetStatus() == COleDateTime::valid)
	{
		rlColumn.Text = prpMitarbeiterstamm->Dodm.Format("%d.%m.%Y");	
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = "";
	if(prpMitarbeiterstamm->Dobk.GetStatus() == COleDateTime::valid)
	{
		rlColumn.Text = prpMitarbeiterstamm->Dobk.Format("%d.%m.%Y");	
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Rema;	
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomMitarbeiterstammTable->DeleteTextLine(ilItem);

		MakeLine(prpMitarbeiterstamm);
		if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
		{
	        MITARBEITERSTAMMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomMitarbeiterstammTable->AddTextLine(olLine, (void *)prlLine);
				pomMitarbeiterstammTable->DisplayTable();
			}
		}
		else
		{
			pomMitarbeiterstammTable->DisplayTable();
		}
	}
	else
	{
		MakeLine(prpMitarbeiterstamm);
		if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
		{
	        MITARBEITERSTAMMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomMitarbeiterstammTable->AddTextLine(olLine, (void *)prlLine);
				pomMitarbeiterstammTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ProcessMitarbeiterstammDelete(STFDATA *prpMitarbeiterstamm)
{
	int ilItem;
	if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomMitarbeiterstammTable->DeleteTextLine(ilItem);
		pomMitarbeiterstammTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::IsPassFilter(STFDATA *prpMitarbeiterstamm)
{

	void *p;

	//bool blIsScoOk	= false;
	bool blIsSpfOk	= false;
	bool blIsSorOk	= false;
	bool blIsSpeOk	= false;

	//SCODATA *prlSco = NULL;
	SPEDATA *prlSpe = NULL;
	SPFDATA *prlSpf = NULL;
	SORDATA *prlSor = NULL;

	if(prpMitarbeiterstamm == NULL)
		return false;
	/*if (bmUseAllSco == false)
	{
		prlSco = ogScoData.GetValidScoBySurn(prpMitarbeiterstamm->Urno);
		if(prlSco != NULL)
		{
			blIsScoOk = (omCMapForSco.Lookup(CString(prlSco->Code),p) == TRUE)?true:false;
		}
	}
	else
	{
		blIsScoOk = true;
	}*/

	if (bmUseAllSpe == false)
	{
		prlSpe = ogSpeData.GetValidSpeBySurn(prpMitarbeiterstamm->Urno);
		if(prlSpe != NULL)
		{
			blIsSpeOk = (omCMapForSpe.Lookup(CString(prlSpe->Code),p) == TRUE)?true:false;
		}
	}
	else
	{
		blIsSpeOk = true;
	}

	if (bmUseAllSpf == false)
	{
		prlSpf = ogSpfData.GetValidSpfBySurn(prpMitarbeiterstamm->Urno);
		if(prlSpf != NULL)
		{
			blIsSpfOk = (omCMapForSpf.Lookup(CString(prlSpf->Code),p) == TRUE)?true:false;
		}
	}
	else
	{
		blIsSpfOk = true;
	}

	if (bmUseAllSor == false)
	{
		prlSor= ogSorData.GetValidSorBySurn(prpMitarbeiterstamm->Urno);
		if(prlSor != NULL)
		{
			blIsSorOk = (omCMapForSor.Lookup(CString(prlSor->Code),p) == TRUE)?true:false;
		}
	}
	else
	{
		blIsSorOk = true;
	}


	return (blIsSpfOk && blIsSpeOk /*blIsScoOk*/ && blIsSorOk);
}


void MitarbeiterstammTableViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	CString olText;
	//int ilLc,ilCount;

	////Variablen f�r PSFlightPage

////Variablen f�r PSSpfPage
	omCMapForSpf.RemoveAll();		// Staff personal Function
	bmUseAllSpf   = false;

////Variablen f�r PSSorPage
	omCMapForSor.RemoveAll();		// Staff organisation unit
	bmUseAllSor   = false;

////Variablen f�r PSScoPage
	/*omCMapForSco.RemoveAll();		// Staff contracts
	bmUseAllSco   = false;*/

	omCMapForSpe.RemoveAll();		// Staff qualification
	bmUseAllSpe   = false;

    GetFilter("SORFILTER", olFilterValues);
		
    int ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_ALL))
		{
			bmUseAllSor = true;
		}
		else
		{
			for (int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForSor[olFilterValues[ilLc]] = NULL;
			}
		}
	}

    GetFilter("SPFFILTER", olFilterValues);
		
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_ALL))
		{
			bmUseAllSpf = true;
		}
		else
		{
			for (int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForSpf[olFilterValues[ilLc]] = NULL;
			}
		}
	}

	bmUseAllSpe = true;

}

void MitarbeiterstammTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort("SPECIALSORT",olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0 ; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "SORCODE+")
            ilSortOrderEnumeratedValue = BY_SORCODE_ASC;
        else if (olSortOrder[i] == "SORCODE-")
            ilSortOrderEnumeratedValue = BY_SORCODE_DSC;
        else if (olSortOrder[i] == "SPFCODE+")
            ilSortOrderEnumeratedValue = BY_SPFCODE_ASC;
        else if (olSortOrder[i] == "SPFCODE-")
            ilSortOrderEnumeratedValue = BY_SPFCODE_DSC;
        /*else if (olSortOrder[i] == "SCOCODE")
            ilSortOrderEnumeratedValue = BY_SCOCODE;*/
		else if (olSortOrder[i] == "SPECODE+")
            ilSortOrderEnumeratedValue = BY_SPECODE_ASC;
		else if (olSortOrder[i] == "SPECODE-")
            ilSortOrderEnumeratedValue = BY_SPECODE_DSC;
        else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

//-----------------------------------------------------------------------------------------------

int MitarbeiterstammTableViewer::CompareMitarbeiterstamm(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm1, MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm2)
{
	int	ilCompareResult = 0;
	if ((prpMitarbeiterstamm1 != NULL) && (prpMitarbeiterstamm2 != NULL))
	{
	    int ilCount = omSortOrder.GetSize();
		CString Code1 = "";
		CString Code2 = "";
		for (int ilLc = 0; ilLc < ilCount; ilLc++)
		{
			ilCompareResult = 0;

			switch (omSortOrder[ilLc])
			{
			case BY_SORCODE_ASC:
				Code1 = prpMitarbeiterstamm1->SorCode;
				Code2 = prpMitarbeiterstamm2->SorCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  > (Code2) ? 1: -1;
				break;
			case BY_SORCODE_DSC:
				Code1 = prpMitarbeiterstamm1->SorCode;
				Code2 = prpMitarbeiterstamm2->SorCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  < (Code2) ? 1: -1;
				break;
			case BY_SPFCODE_ASC:
				Code1 = prpMitarbeiterstamm1->SpfCode;
				Code2 = prpMitarbeiterstamm2->SpfCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  > (Code2) ? 1: -1;
				break;
			case BY_SPFCODE_DSC:
				Code1 = prpMitarbeiterstamm1->SpfCode;
				Code2 = prpMitarbeiterstamm2->SpfCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  < (Code2) ? 1: -1;
				break;
			/*case BY_SCOCODE:
				Code1 = prpMitarbeiterstamm1->ScoCode;
				Code2 = prpMitarbeiterstamm2->ScoCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  > (Code2) ? 1: -1;
				break;*/
			case BY_SPECODE_ASC:
				Code1 = prpMitarbeiterstamm1->SpeCode;
				Code2 = prpMitarbeiterstamm2->SpeCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  > (Code2) ? 1: -1;
				break;
			case BY_SPECODE_DSC:
				Code1 = prpMitarbeiterstamm1->SpeCode;
				Code2 = prpMitarbeiterstamm2->SpeCode;
				ilCompareResult = ((Code1) == (Code2))? 0:
					(Code1)  < (Code2) ? 1: -1;
				break;
			}

			if (ilCompareResult != 0)
				return ilCompareResult;
		}
	}
	return 0;   
}


//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING183);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ ) 
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}



//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,MITARBEITERSTAMMTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Lanm;
				break;
			case 1:
				rlElement.Text = prpLine->Finm;
				break;
			case 2:
				rlElement.Text = prpLine->Peno;
				break;
			case 3:
				rlElement.Text = prpLine->SorCode;
				break;
			case 4:
				rlElement.Text = prpLine->SpfCode;
				break;
			case 5:
				rlElement.Text = prpLine->SpeCode;
				break;
			case 6:
				rlElement.Text = prpLine->SwgCode;
				break;
			case 7:
				rlElement.Text = prpLine->Shnm;
				break;
			case 8:
				rlElement.Text = prpLine->Perc;
				break;
			case 9:
				rlElement.Text = prpLine->Teld;
				break;
			case 10:
				rlElement.Text = prpLine->Telh;
				break;
			case 11:
				rlElement.Text = prpLine->Telp;
				break;
			case 12:
				rlElement.Text = prpLine->Kind;
				break;
			case 13:
				rlElement.Text = prpLine->Pdgl;
				break;
			case 14:
				rlElement.Text = prpLine->Pdkl;
				break;
			case 15:
				rlElement.Text = prpLine->Pmag;
				break;
			case 16:
				rlElement.Text = prpLine->Pmak;
				break;
			case 17:
				rlElement.Text = prpLine->Regi;
				break;
			case 18:
				rlElement.Text = prpLine->Nati;
				break;
			case 19:
				rlElement.Text = prpLine->Kids;
				break;
			case 20:
				rlElement.Text = prpLine->Doem;
				break;
			case 21:
				rlElement.Text = prpLine->Dodm;
				break;
			case 22:
				rlElement.Text = prpLine->Dobk;
				break;
			case 23:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

bool MitarbeiterstammTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "STFTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "STFTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING288) //CString("Last Name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING69)	//CString("First Name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING71)	//CString("Personnel Number");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING883) //CString("Organisational Unit");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING884)	//CString("Functions");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING885) //CString("Qualifications");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING301)	//CString("Abbreviation");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING72)	//CString("Initials");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING73)	//CString("Telephone work");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING74)	//CString("Telephone mobile");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING75)	//CString("Telephone private");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING740)	//CString("Gender Ind");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING735)	//CString("Pdgl");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING736)	//CString("Pdkl");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING737) //CString("Pmag");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING738) //CString("Pmak");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING804) //CString("Regi");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING741) //CString("Nati");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING880) //CString("Kids");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING67) //CString("Doem");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING65) //CString("Dodm");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING749) //CString("Dobk");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING888) //CString("Rema");
		<< endl;


	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		MITARBEITERSTAMMTABLE_LINEDATA rlLine = omLines[i];
		CString olSpeList = ogSpeData.GetValidPermissionBySurn(rlLine.Urno);
		CString olSpfList = ogSpfData.GetValidFctBySurn(rlLine.Urno);
		CString olSorList = ogSorData.GetValidOrgBySurn(rlLine.Urno);

		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Lanm							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Finm							// Vorname
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Peno							// Personal Nummer
		     << setw(0) << opTrenner 
			 << setw(0) << olSorList							// Organisationseinheit
		     << setw(0) << opTrenner 
			 << setw(0) << olSpfList							// Funktionen
		     << setw(0) << opTrenner 
			 << setw(0) << olSpeList							// Qualifikationen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Shnm							// Abk�rzung
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Perc							// Initialien
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Teld							// Telephon B�ro
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Telh							// Telephon mobil
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Telp							// Telephon privat
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Kind							// Gender Ind
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pdgl							// Pdgl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pdkl							// Pdkl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pmag							// Pmag
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pmak;							// Pmak

		   of  << setw(0) << opTrenner 
		     << setw(0) << rlLine.Regi							// Regi 
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Nati
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Kids 
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Doem
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dodm
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dobk
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Rema
		     << endl; 
		
	}

	of.close();
	return true;
}


//-----------------------------------------------------------------------------------------------
