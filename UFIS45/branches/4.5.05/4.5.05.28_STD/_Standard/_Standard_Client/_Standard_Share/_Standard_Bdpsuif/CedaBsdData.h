// CedaBsdData.h

#ifndef __CEDAPBSDDATA__
#define __CEDAPBSDDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct BSDDATA 
{
	char 	 Bewc[7]; 	// Bewertungsfaktor.code
	char 	 Bkd1[6]; 	// Pausenl�nge
	char 	 Bkf1[6]; 	// Pausenlage von
	char 	 Bkr1[3]; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	char 	 Bkt1[6]; 	// Pausenlage bis
	char 	 Bsdc[10]; 	// Code
	char 	 Bsdk[14]; 	// Kurzbezeichnung
	char 	 Bsdn[42]; 	// Bezeichnung
	char 	 Bsds[5]; 	// SAP-Code
	CTime 	 Cdat;		// Erstellungsdatum
	char 	 Ctrc[7]; 	// Vertragsart.code
	char 	 Esbg[6]; 	// Fr�hester Schichtbeginn
	char 	 Lsen[6]; 	// Sp�testes Schichtende
	CTime 	 Lstu;		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennung
	char 	 Rema[62]; 	// Bemerkungen
	char 	 Sdu1[6]; 	// Regul�re Schichtdauer
	char 	 Sex1[6]; 	// M�gliche Arbeitszeitver-l�ngerung
	char 	 Ssh1[6]; 	// M�gliche Arbeitszeitverk�rzung
	char 	 Type[3]; 	// Flag (statisch/dynamisch)
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)


	//*** 09.11.99 SHA ***
	//*** SWISSPORT! ***
	char 	 Fctc[7]; 	// Reg.Funktions.code
	char 	 Dpt1[8+2]; 	// Reg.OrgEinheit.code
	char 	 Rgsb[16]; 	// Fr�hester Schichtbeginn dynamisch
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis
	char	 Fdel[3];	//Abordnungen existieren
	char	 Rgbc[6+2];	// Farbcode der Basisschicht


	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	BSDDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end BSDDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaBsdData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<BSDDATA> omData;

	char pcmListOfFields[2048];

// OBsdations
public:
    CedaBsdData();
	~CedaBsdData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(BSDDATA *prpBsd);
	bool InsertInternal(BSDDATA *prpBsd);
	bool Update(BSDDATA *prpBsd);
	bool UpdateInternal(BSDDATA *prpBsd);
	bool Delete(long lpUrno);
	bool DeleteInternal(BSDDATA *prpBsd);
	bool ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(BSDDATA *prpBsd);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	BSDDATA  *GetBsdByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareBsdData(BSDDATA *prpBsdData);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateBsdBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPBSDDATA__
