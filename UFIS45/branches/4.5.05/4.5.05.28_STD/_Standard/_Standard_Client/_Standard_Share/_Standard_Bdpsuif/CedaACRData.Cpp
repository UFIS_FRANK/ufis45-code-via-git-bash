// CedaACRData.cpp

#include <stdafx.h>
#include <CedaACRData.h>
#include <resource.h>
#include <PrivList.h>
#include <Util.h>


// Local function prototype
static void ProcessACRCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaACRData::CedaACRData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for ACRDATA
	BEGIN_CEDARECINFO(ACRDATA,ACRDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Regn,"REGN")
		FIELD_CHAR_TRIM	(Regi,"REGI")
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_CHAR_TRIM	(Act5,"ACT5")
		FIELD_CHAR_TRIM	(Owne,"OWNE")
		FIELD_CHAR_TRIM	(Selc,"SELC")
		FIELD_CHAR_TRIM	(Mtow,"MTOW")
		FIELD_CHAR_TRIM	(Mowe,"MOWE")
		FIELD_CHAR_TRIM	(Apui,"APUI")
		FIELD_CHAR_TRIM	(Main,"MAIN")
		FIELD_CHAR_TRIM	(Enna,"ENNA")
		FIELD_CHAR_TRIM	(Annx,"ANNX")
		FIELD_CHAR_TRIM	(Nose,"NOSE")
		FIELD_CHAR_TRIM	(Noga,"NOGA")
		FIELD_CHAR_TRIM	(Noto,"NOTO")
		FIELD_CHAR_TRIM	(Debi,"DEBI")
		FIELD_DATE		(Ladp,"LADP")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Enty,"ENTY")
		FIELD_CHAR_TRIM	(Gall,"GALL")
		FIELD_CHAR_TRIM	(Frst,"FRST")
		FIELD_CHAR_TRIM	(Rest,"REST")
		FIELD_CHAR_TRIM	(Efis,"EFIS")
		FIELD_CHAR_TRIM	(Acti,"ACTI")
		FIELD_CHAR_TRIM	(Ache,"ACHE")
		FIELD_CHAR_TRIM	(Acle,"ACLE")
		FIELD_CHAR_TRIM	(Acws,"ACWS")
		FIELD_LONG		(Aurn,"AURN")
		FIELD_CHAR_TRIM	(Conf,"CONF")
		FIELD_CHAR_TRIM	(Cont,"CONT")
		FIELD_CHAR_TRIM	(Ctry,"CTRY")
		FIELD_CHAR_TRIM	(Cvtd,"CVTD")
		FIELD_CHAR_TRIM	(Deli,"DELI")
		FIELD_CHAR_TRIM	(Etxt,"ETXT")
		FIELD_CHAR_TRIM	(Exrg,"EXRG")
		FIELD_CHAR_TRIM	(Fhrs,"FHRS")
		FIELD_CHAR_TRIM	(Fuse,"FUSE")
		FIELD_CHAR_TRIM	(Iata,"IATA")
		FIELD_CHAR_TRIM	(Icao,"ICAO")
		FIELD_CHAR_TRIM	(Lcod,"LCOD")
		FIELD_CHAR_TRIM	(Lcyc,"LCYC")
		FIELD_CHAR_TRIM	(Lsdb,"LSDB")
		FIELD_CHAR_TRIM	(Lsdf,"LSDF")
		FIELD_CHAR_TRIM	(Lsfb,"LSFB")
		FIELD_CHAR_TRIM	(Lstb,"LSTB")
		FIELD_CHAR_TRIM	(Mode,"MODC")
		FIELD_CHAR_TRIM	(Nois,"NOIS")
		FIELD_CHAR_TRIM	(Odat,"ODAT")
		FIELD_CHAR_TRIM	(Oope,"OOPE")
		FIELD_CHAR_TRIM	(Oopt,"OPTB")
		FIELD_CHAR_TRIM	(Oord,"OORD")
		FIELD_CHAR_TRIM	(Opbb,"OPBB")
		FIELD_CHAR_TRIM	(Opbf,"OPBF")
		FIELD_CHAR_TRIM	(Opwb,"OPWB")
		FIELD_CHAR_TRIM	(Powd,"POWD")
		FIELD_CHAR_TRIM	(Regb,"REGB")
		FIELD_CHAR_TRIM	(Scod,"SCOD")
		FIELD_CHAR_TRIM	(Seri,"SERI")
		FIELD_CHAR_TRIM	(Strd,"STRD")
		FIELD_CHAR_TRIM	(Text,"TEXT")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Wfub,"WFUB")
		FIELD_CHAR_TRIM	(Wobo,"WOBO")
		FIELD_CHAR_TRIM	(Year,"YEAR")
		FIELD_CHAR_TRIM	(Ufis,"UFIS")
		FIELD_CHAR_TRIM	(Cnam,"CNAM")
		FIELD_CHAR_TRIM	(Pnum,"PNUM")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Naco,"NACO")
		FIELD_CHAR_TRIM	(Oadr,"OADR")
		FIELD_CHAR_TRIM	(Typu,"TYPU")
		FIELD_CHAR_TRIM	(Crw1,"CRW1")
		FIELD_CHAR_TRIM	(Crw2,"CRW2")
	END_CEDARECINFO //(ACRDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(ACRDataRecInfo)/sizeof(ACRDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ACRDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ACR");
	strcpy(pcmACRFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,REGN,REGI,ACT3,ACT5,OWNE,SELC,MTOW,MOWE,APUI,MAIN,ENNA,ANNX,NOSE,NOGA,NOTO,DEBI,LADP,REMA,VAFR,VATO,ENTY,GALL,FRST,REST,EFIS,ACTI,ACHE,ACLE,ACWS,AURN,CONF,CONT,CTRY,CVTD,DELI,ETXT,EXRG,FHRS,FUSE,IATA,ICAO,LCOD,LCYC,LSDB,LSDF,LSFB,LSTB,MODC,NOIS,ODAT,OOPE,OPTB,OORD,OPBB,OPBF,OPWB,POWD,REGB,SCOD,SERI,STRD,TEXT,TYPE,WFUB,WOBO,YEAR,UFIS,CNAM,PNUM,HOME,NACO,OADR,TYPU,CRW1,CRW2");
	pcmFieldList = pcmACRFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}
void CedaACRData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{


	ropDesription.Add(LoadStg(IDS_STRING440));	//	1
	ropDesription.Add(LoadStg(IDS_STRING441));
	ropDesription.Add(LoadStg(IDS_STRING442));
	ropDesription.Add(LoadStg(IDS_STRING48));
	ropDesription.Add(LoadStg(IDS_STRING506));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING62));
	ropDesription.Add(LoadStg(IDS_STRING61));
	ropDesription.Add(LoadStg(IDS_STRING248));
	ropDesription.Add(LoadStg(IDS_STRING620));

	ropDesription.Add(LoadStg(IDS_STRING621));	//	11
	ropDesription.Add(LoadStg(IDS_STRING622));
	ropDesription.Add(LoadStg(IDS_STRING623));
	ropDesription.Add(LoadStg(IDS_STRING55));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING507));
	ropDesription.Add(LoadStg(IDS_STRING508));
	ropDesription.Add(LoadStg(IDS_STRING509));
	ropDesription.Add(LoadStg(IDS_STRING59));
	ropDesription.Add(LoadStg(IDS_STRING58));

	ropDesription.Add(LoadStg(IDS_STRING60));	//	21
	ropDesription.Add(LoadStg(IDS_STRING53));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING510));
	ropDesription.Add(LoadStg(IDS_STRING182));
	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING54));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropDesription.Add(LoadStg(IDS_STRING230));	//	31
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING642));
	ropDesription.Add(LoadStg(IDS_STRING643));
	ropDesription.Add(LoadStg(IDS_STRING644));
	ropDesription.Add(LoadStg(IDS_STRING645));
	ropDesription.Add(LoadStg(IDS_STRING646));
	ropDesription.Add(LoadStg(IDS_STRING647));
	ropDesription.Add(LoadStg(IDS_STRING648));
	ropDesription.Add(LoadStg(IDS_STRING649));

	ropDesription.Add(LoadStg(IDS_STRING650));	//	41
	ropDesription.Add(LoadStg(IDS_STRING651));
	ropDesription.Add(LoadStg(IDS_STRING652));
	ropDesription.Add(LoadStg(IDS_STRING653));
	ropDesription.Add(LoadStg(IDS_STRING654));
	ropDesription.Add(LoadStg(IDS_STRING655));
	ropDesription.Add(LoadStg(IDS_STRING656));
	ropDesription.Add(LoadStg(IDS_STRING657));
	ropDesription.Add(LoadStg(IDS_STRING658));
	ropDesription.Add(LoadStg(IDS_STRING659));	

	ropDesription.Add(LoadStg(IDS_STRING660));	//	51
	ropDesription.Add(LoadStg(IDS_STRING661));
	ropDesription.Add(LoadStg(IDS_STRING662));
	ropDesription.Add(LoadStg(IDS_STRING663));
	ropDesription.Add(LoadStg(IDS_STRING664));
	ropDesription.Add(LoadStg(IDS_STRING665));
	ropDesription.Add(LoadStg(IDS_STRING666));
	ropDesription.Add(LoadStg(IDS_STRING667));
	ropDesription.Add(LoadStg(IDS_STRING668));
	ropDesription.Add(LoadStg(IDS_STRING669));

	ropDesription.Add(LoadStg(IDS_STRING670));	//	61
	ropDesription.Add(LoadStg(IDS_STRING671));
	ropDesription.Add(LoadStg(IDS_STRING672));
	ropDesription.Add(LoadStg(IDS_STRING673));
	ropDesription.Add(LoadStg(IDS_STRING674));
	ropDesription.Add(LoadStg(IDS_STRING675));
	ropDesription.Add(LoadStg(IDS_STRING676));
	ropDesription.Add(LoadStg(IDS_STRING677));
	ropDesription.Add(LoadStg(IDS_STRING678));
	ropDesription.Add(LoadStg(IDS_STRING679));

	ropDesription.Add(LoadStg(IDS_STRING680));	//	71
	ropDesription.Add(LoadStg(IDS_STRING681));
	ropDesription.Add(LoadStg(IDS_STRING682));
	ropDesription.Add(LoadStg(IDS_STRING683));
	ropDesription.Add(LoadStg(IDS_STRING684));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING1037));
	ropDesription.Add(LoadStg(IDS_STRING1038));
	ropDesription.Add(LoadStg(IDS_STRING1039));
	ropDesription.Add(LoadStg(IDS_STRING1040));

	ropDesription.Add(LoadStg(IDS_STRING1041));	//	81
    ropDesription.Add(LoadStg(IDS_STRING711));	//	82

	ropType.Add("String");	ropFields.Add("ACT3");
	ropType.Add("String");	ropFields.Add("ACT5");
	ropType.Add("String");	ropFields.Add("ACTI");
	ropType.Add("String");	ropFields.Add("ANNX");
	ropType.Add("String");	ropFields.Add("APUI");
	ropType.Add("Date");	ropFields.Add("CDAT");
	ropType.Add("String");	ropFields.Add("DEBI");
	ropType.Add("String");	ropFields.Add("ENNA");
	ropType.Add("String");	ropFields.Add("ENTY");
	ropType.Add("String");	ropFields.Add("GALL");
	ropType.Add("String");	ropFields.Add("FRST");
	ropType.Add("String");	ropFields.Add("REST");
	ropType.Add("String");	ropFields.Add("EFIS");
	ropType.Add("Date");	ropFields.Add("LADP");
	ropType.Add("Date");	ropFields.Add("LSTU");
	ropType.Add("String");	ropFields.Add("MAIN");
	ropType.Add("Double");	ropFields.Add("MOWE");
	ropType.Add("Double");	ropFields.Add("MTOW");
	ropType.Add("Double");	ropFields.Add("NOGA");
	ropType.Add("Double");	ropFields.Add("NOSE");
	ropType.Add("Double");	ropFields.Add("NOTO");
	ropType.Add("String");	ropFields.Add("OWNE");
	ropType.Add("String");	ropFields.Add("PRFL");
	ropType.Add("String");	ropFields.Add("REGI");
	ropType.Add("String");	ropFields.Add("REGN");
	ropType.Add("String");	ropFields.Add("REMA");
	ropType.Add("String");	ropFields.Add("SELC");
	ropType.Add("String");	ropFields.Add("URNO");
	ropType.Add("String");	ropFields.Add("USEC");
	ropType.Add("String");	ropFields.Add("USEU");
	ropType.Add("Date");	ropFields.Add("VAFR");
	ropType.Add("Date");	ropFields.Add("VATO");
	ropType.Add("Double");	ropFields.Add("ACHE");
	ropType.Add("Double");	ropFields.Add("ACLE");
	ropType.Add("Double");	ropFields.Add("ACWS");
	ropType.Add("String");	ropFields.Add("AURN");
	ropType.Add("String");	ropFields.Add("CONF");
	ropType.Add("String");	ropFields.Add("CONT");
	ropType.Add("String");	ropFields.Add("CTRY");
	ropType.Add("String");	ropFields.Add("CVTD");
	ropType.Add("String");	ropFields.Add("DELI");
	ropType.Add("String");	ropFields.Add("ETXT");
	ropType.Add("String");	ropFields.Add("EXRG");
	ropType.Add("String");	ropFields.Add("FHRS");
	ropType.Add("String");	ropFields.Add("FUSE");
	ropType.Add("String");	ropFields.Add("IATA");
	ropType.Add("String");	ropFields.Add("ICAO");
	ropType.Add("String");	ropFields.Add("LCOD");
	ropType.Add("String");	ropFields.Add("LCYC");
	ropType.Add("String");	ropFields.Add("LSDB");
	ropType.Add("String");	ropFields.Add("LSDF");
	ropType.Add("String");	ropFields.Add("LSFB");
	ropType.Add("String");	ropFields.Add("LSTB");
	ropType.Add("String");	ropFields.Add("MODC");
	ropType.Add("String");	ropFields.Add("NOIS");
	ropType.Add("String");	ropFields.Add("ODAT");
	ropType.Add("String");	ropFields.Add("OOPE");
	ropType.Add("String");	ropFields.Add("OPTB");
	ropType.Add("String");	ropFields.Add("OORD");
	ropType.Add("String");	ropFields.Add("OPBB");
	ropType.Add("String");	ropFields.Add("OPBF");
	ropType.Add("String");	ropFields.Add("OPWB");
	ropType.Add("String");	ropFields.Add("POWD");
	ropType.Add("String");	ropFields.Add("REGB");
	ropType.Add("String");	ropFields.Add("SCOD");
	ropType.Add("String");	ropFields.Add("SERI");
	ropType.Add("String");	ropFields.Add("STRD");
	ropType.Add("String");	ropFields.Add("TEXT");
	ropType.Add("String");	ropFields.Add("TYPE");
	ropType.Add("String");	ropFields.Add("WFUB");
	ropType.Add("String");	ropFields.Add("WOBO");
	ropType.Add("String");	ropFields.Add("YEAR");
	ropType.Add("String");	ropFields.Add("UFIS");
	ropType.Add("String");	ropFields.Add("CNAM");
	ropType.Add("String");	ropFields.Add("PNUM");
	ropType.Add("String");	ropFields.Add("HOME");
	ropType.Add("String");	ropFields.Add("NACO");
	ropType.Add("String");	ropFields.Add("OADR");
	ropType.Add("String");	ropFields.Add("TYPU");
	ropType.Add("String");	ropFields.Add("CRW1");
	ropType.Add("String");	ropFields.Add("CRW2");
	ropType.Add("String");  ropFields.Add("HOME");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//----------------------------------------------------------------------------------------------------

void CedaACRData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACR_CHANGE,CString("ACRDATA"), CString("ACR-changed"),ProcessACRCf);
	ogDdx.Register((void *)this,BC_ACR_DELETE,CString("ACRDATA"), CString("ACR-deleted"),ProcessACRCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaACRData::~CedaACRData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaACRData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaACRData::Read(char *pspWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere != NULL)
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pspWhere);
	}
	else
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACRDATA *prpACR = new ACRDATA;
		if ((ilRc = GetFirstBufferRecord(prpACR)) == true)
		{
			prpACR->IsChanged = DATA_UNCHANGED;

			CutZeros(prpACR);

			omData.Add(prpACR);//Update omData
			omUrnoMap.SetAt((void *)prpACR->Urno,prpACR);
		}
		else
		{
			delete prpACR;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaACRData::InsertACR(ACRDATA *prpACR,BOOL bpSendDdx)
{
	prpACR->IsChanged = DATA_NEW;
	AddZeros(prpACR);
	if(SaveACR(prpACR) == false) return false; //Update Database
	InsertACRInternal(prpACR);
    return true;
}

//--INSERT-INTERNAL----------------------------------------------------------------------------------------

bool CedaACRData::InsertACRInternal(ACRDATA *prpACR)
{
	CutZeros(prpACR);
	ogDdx.DataChanged((void *)this, ACR_CHANGE,(void *)prpACR ); //Update Viewer
	omData.Add(prpACR);//Update omData
	omUrnoMap.SetAt((void *)prpACR->Urno,prpACR);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaACRData::DeleteACR(long lpUrno)
{
	ACRDATA *prlACR = GetACRByUrno(lpUrno);
	if (prlACR != NULL)
	{
		prlACR->IsChanged = DATA_DELETED;
		if(SaveACR(prlACR) == false) return false; //Update Database
		DeleteACRInternal(prlACR);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaACRData::DeleteACRInternal(ACRDATA *prpACR)
{
	ogDdx.DataChanged((void *)this,ACR_DELETE,(void *)prpACR); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpACR->Urno);
	int ilACRCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilACRCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpACR->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaACRData::PrepareACRData(ACRDATA *prpACR)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaACRData::UpdateACR(ACRDATA *prpACR,BOOL bpSendDdx)
{
	if (GetACRByUrno(prpACR->Urno) != NULL)
	{
		if (prpACR->IsChanged == DATA_UNCHANGED)
		{
			prpACR->IsChanged = DATA_CHANGED;
		}
		AddZeros(prpACR);
		if(SaveACR(prpACR) == false) return false; //Update Database
		UpdateACRInternal(prpACR);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaACRData::UpdateACRInternal(ACRDATA *prpACR)
{
	ACRDATA *prlACR = GetACRByUrno(prpACR->Urno);
	if (prlACR != NULL)
	{
		CutZeros(prpACR);
		*prlACR = *prpACR; //Update omData
		ogDdx.DataChanged((void *)this,ACR_CHANGE,(void *)prlACR); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACRDATA *CedaACRData::GetACRByUrno(long lpUrno)
{
	ACRDATA  *prlACR;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlACR) == TRUE)
	{
		return prlACR;
	}
	return NULL;
}

//--READSPECIALDATA-----------------------------------------------------------------------------------------

bool CedaACRData::ReadSpecialData(CCSPtrArray<ACRDATA> *popAcr,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ACR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ACR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAcr != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ACRDATA *prpAcr = new ACRDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAcr,CString(pclFieldList))) == true)
			{
				popAcr->Add(prpAcr);
			}
			else
			{
				delete prpAcr;
			}
		}
		if(popAcr->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaACRData::SaveACR(ACRDATA *prpACR)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];
	CString olWhere;
	if (prpACR->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MTOW_DETAIL");
	BOOL regEnh = IsPrivateProfileOn("REG_ENH", "NO" );

	

	switch(prpACR->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpACR);
		strcpy(pclData,olListOfData);
  		ilRc = CedaAction("IRT","","",pclData);
		prpACR->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACR->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpACR);
		strcpy(pclData,olListOfData);
		
		if(regEnh)
		{
			ilRc = CedaAction("NTS",pclSelection,pcmACRFieldList,pclData);
			
		}
		else
		{
 			ilRc = CedaAction("URT",pclSelection,pcmACRFieldList,pclData);
		}
		prpACR->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACR->Urno);
 		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessACRCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ACR_CHANGE :
	case BC_ACR_DELETE :
		((CedaACRData *)popInstance)->ProcessACRBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaACRData::ProcessACRBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlACRData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlACRData->Selection);

	ACRDATA *prlACR = GetACRByUrno(llUrno);
	if(ipDDXType == BC_ACR_CHANGE)
	{
		if (prlACR != NULL)
		{
			GetRecordFromItemList(prlACR,prlACRData->Fields,prlACRData->Data);
			if(ValidateACRBcData(prlACR->Urno)) //Prf: 8795
			{
				UpdateACRInternal(prlACR);
			}
			else
			{
				DeleteACRInternal(prlACR);
			}
		}
		else
		{
			prlACR = new ACRDATA;
			GetRecordFromItemList(prlACR,prlACRData->Fields,prlACRData->Data);
			if(ValidateACRBcData(prlACR->Urno)) //Prf: 8795
			{
				InsertACRInternal(prlACR);
			}
			else
			{
				delete prlACR;
			}
		}
	}
	if(ipDDXType == BC_ACR_DELETE)
	{
		if (prlACR != NULL)
		{
			DeleteACRInternal(prlACR);
		}
	}
}
//Prf: 8795
//--ValidateACRBcData--------------------------------------------------------------------------------------

bool CedaACRData::ValidateACRBcData(const long& lrpUrno)
{
	bool blValidateACRBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ACRDATA> olAcrs;
		if(!ReadSpecialData(&olAcrs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateACRBcData = false;
		}
	}
	return blValidateACRBcData;
}

//---------------------------------------------------------------------------------------------------------

void  CedaACRData::CutZeros(ACRDATA *prpACR)
{
	if(strlen(prpACR->Mtow) != 0) sprintf(prpACR->Mtow,"%.2f",atof(prpACR->Mtow));
	if(strlen(prpACR->Mowe) != 0) sprintf(prpACR->Mowe,"%.2f",atof(prpACR->Mowe));

	if(strlen(prpACR->Noga) != 0) sprintf(prpACR->Noga,"%d",  atoi(prpACR->Noga));
	if(strlen(prpACR->Nose) != 0) sprintf(prpACR->Nose,"%d",  atoi(prpACR->Nose));
	if(strlen(prpACR->Noto) != 0) sprintf(prpACR->Noto,"%d",  atoi(prpACR->Noto));
}

//---------------------------------------------------------------------------------------------------------

void  CedaACRData::AddZeros(ACRDATA *prpACR)
{
	if(strlen(prpACR->Mtow) != 0) sprintf(prpACR->Mtow,"%010.2f",atof(prpACR->Mtow));
	if(strlen(prpACR->Mowe) != 0) sprintf(prpACR->Mowe,"%010.2f",atof(prpACR->Mowe));

	if(strlen(prpACR->Noga) != 0) sprintf(prpACR->Noga,"%02d",   atoi(prpACR->Noga));
	if(strlen(prpACR->Nose) != 0) sprintf(prpACR->Nose,"%03d",   atoi(prpACR->Nose));
	if(strlen(prpACR->Noto) != 0) sprintf(prpACR->Noto,"%02d",   atoi(prpACR->Noto));
}

//---------------------------------------------------------------------------------------------------------
