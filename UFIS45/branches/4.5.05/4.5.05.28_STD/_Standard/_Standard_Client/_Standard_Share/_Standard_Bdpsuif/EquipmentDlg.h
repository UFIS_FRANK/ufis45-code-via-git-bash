#if !defined(AFX_EQUIPMENTDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_EQUIPMENTDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EquipmentDlg.h : header file
//
#include <CedaEquData.h>

#include <CCSEdit.h>
#include <CCSTable.h>
#include <resrc1.h>		// main symbols#
#include <CedaBlkData.h>
#include <CCSTable.h>
#include <CedaValData.h>
#include <CedaEqaData.h>

/////////////////////////////////////////////////////////////////////////////
// EquipmentDlg dialog

class EquipmentDlg : public CDialog
{
// Construction
public:
	EquipmentDlg(EQUDATA *popEqu, VALDATA *popVal, CWnd* pParent = NULL);   // standard constructor
	~EquipmentDlg();

	EQUDATA *pomEqu;
	int		imBurn;

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;

	CCSPtrArray<EQADATA> omEqaPtrA;
	CCSPtrArray<EQADATA> omDeleteEqaPtrA;

// Dialog Data
	//{{AFX_DATA(EquipmentDlg)
	enum { IDD = IDD_EQUIPMENTDLG };
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CButton	m_EQANEW;
	CButton	m_EQADEL;
	CStatic	m_EQAFRAME;
	CCSEdit	m_GCDE	;
	CCSEdit	m_ENAM;
	CCSEdit	m_EQPS;
	CCSEdit	m_ETYP;
	CCSEdit m_CRQU;
	CCSEdit	m_IVNR;
	CCSEdit	m_TELE;
	CCSEdit	m_REMA;
	CCSEdit	m_GKEY;
	CButton	m_OK;
	CButton m_BEQTAW;
	CButton m_BPERAW;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CButton	m_UTC;
	CButton	m_LOCAL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EquipmentDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	
protected:
// Generated message map functions
	//{{AFX_MSG(EquipmentDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	afx_msg void OnEqaDel();
	afx_msg void OnEqaNew();
	afx_msg void OnBEqtAw();
	afx_msg void OnBPerAw();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CCSTable	*pomTable;
	CCSTable	*pomEqaTable;

	VALDATA		*pomVal;
	int			imLastSelection;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
	void UpdateNoavTable();

	void MakeEqaTable(CString opBurn,bool bpCopied = false);
	void ChangeEqaTable(EQADATA *prpEqa, int ipLineNo);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EQUIPMENTDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
