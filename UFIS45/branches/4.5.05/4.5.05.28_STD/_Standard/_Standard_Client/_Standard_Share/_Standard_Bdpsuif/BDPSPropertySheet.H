// flplanps.h : header file
//



#ifndef _BDPSPROPERTYSHEET_H_
#define _BDPSPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSUniFilterPage.h>
#include <PSunisortpage.h>
#include <PsSorFilterPage.h>
#include <PsSpfFilterPage.h>		

/////////////////////////////////////////////////////////////////////////////
// BDPSPropertySheet

class BDPSPropertySheet : public BasePropertySheet
{
// Construction
public:
	BDPSPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	//TestPage m_TestPage;
	CString omCalledFrom;
	CPsUniFilter		m_PSUniFilter;
	CPSUniSortPage		m_PSUniSortPage;
	CPsSorFilterPage	m_PSSorFilterPage;
	CPsSpfFilterPage	m_PSSpfFilterPage;	
private:
	CString				omSorCodeText;
	CString				omSpfCodeText;
// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _BDPSPROPERTYSHEET_H_
