#if !defined(AFX_GATPOSGATEDLG_H__452453F6_BB0D_11D6_8214_00010215BFDE__INCLUDED_)
#define AFX_GATPOSGATEDLG_H__452453F6_BB0D_11D6_8214_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosGateDlg.h : header file
//
#include <GateDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosGateDlg dialog

class GatPosGateDlg : public GateDlg
{
// Construction
public:
	GatPosGateDlg(GATDATA *popGAT,CWnd* pParent = NULL);   // standard constructor
	int GetConnectedBaggageBelts(CStringArray& ropBaggageBelts);
	int GetConnectedWaitingRooms(CStringArray& ropLoungess);
	int GetConnectedGates(CStringArray& ropGates);
	void HandleEquipment();

// Dialog Data
	//{{AFX_DATA(GatPosGateDlg)
	enum { IDD = IDD_GATPOS_GATEDLG };
	CListBox	m_ConnectedGates;
	CButton	m_FCOM;
	AatBitmapComboBox	m_BlockingBitmap;
	CListBox		m_ConnectedLounges;
	CListBox		m_ConnectedBaggageBelts;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosGateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosGateDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnLounge();
	afx_msg void OnBaggageBelt();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavNew();
	afx_msg void OnCommonGate();
	afx_msg void OnGates();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	CStringArray	omAvailableBaggageBelts;
	CStringArray	omConnectedBaggageBelts;
	CStringArray	omAvailableLounges;
	CStringArray	omConnectedLounges;
	CStringArray    omAvailableGates;
	CStringArray    omConnectedGates;

	GatPosGateDlg(GATDATA *popGAT,int ipDlg,CWnd* pParent = NULL);   // standard constructor
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSGATEDLG_H__452453F6_BB0D_11D6_8214_00010215BFDE__INCLUDED_)
