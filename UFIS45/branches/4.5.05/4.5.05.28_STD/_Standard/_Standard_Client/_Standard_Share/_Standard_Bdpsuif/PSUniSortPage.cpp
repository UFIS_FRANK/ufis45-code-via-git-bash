// ccsunisortpage.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <PSUniSortPage.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage property page

IMPLEMENT_DYNCREATE(CPSUniSortPage, CPropertyPage)

CPSUniSortPage::CPSUniSortPage() : CPropertyPage(CPSUniSortPage::IDD)
{
	//{{AFX_DATA_INIT(CPSUniSortPage)
	m_EditSort = _T("");
	//}}AFX_DATA_INIT
	//pomPageBuffer = (CPageBuffer *)pomDataBuffer;
	pomCedaData = NULL;
}

CPSUniSortPage::~CPSUniSortPage()
{

}

void CPSUniSortPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;

	     if(omCalledFrom == LoadStg(IDS_STRING173)) pomCedaData = (CCSCedaData *)&ogALTData;
	else if(omCalledFrom == LoadStg(IDS_STRING176)) pomCedaData = (CCSCedaData *)&ogACTData;
	else if(omCalledFrom == LoadStg(IDS_STRING182)) pomCedaData = (CCSCedaData *)&ogACRData;
	else if(omCalledFrom == LoadStg(IDS_STRING174)) pomCedaData = (CCSCedaData *)&ogAPTData;
	else if(omCalledFrom == LoadStg(IDS_STRING190)) pomCedaData = (CCSCedaData *)&ogRWYData;
	else if(omCalledFrom == LoadStg(IDS_STRING191)) pomCedaData = (CCSCedaData *)&ogTWYData;
	else if(omCalledFrom == LoadStg(IDS_STRING185)) pomCedaData = (CCSCedaData *)&ogPSTData;
	else if(omCalledFrom == LoadStg(IDS_STRING178)) pomCedaData = (CCSCedaData *)&ogGATData;
	else if(omCalledFrom == LoadStg(IDS_STRING166)) pomCedaData = (CCSCedaData *)&ogCICData;
	else if(omCalledFrom == LoadStg(IDS_STRING179)) pomCedaData = (CCSCedaData *)&ogBLTData;
	else if(omCalledFrom == LoadStg(IDS_STRING163)) pomCedaData = (CCSCedaData *)&ogEXTData;
	else if(omCalledFrom == LoadStg(IDS_STRING167)) pomCedaData = (CCSCedaData *)&ogDENData;
	else if(omCalledFrom == LoadStg(IDS_STRING192)) pomCedaData = (CCSCedaData *)&ogMVTData;
	else if(omCalledFrom == LoadStg(IDS_STRING194)) pomCedaData = (CCSCedaData *)&ogNATData;
	else if(omCalledFrom == LoadStg(IDS_STRING160)) pomCedaData = (CCSCedaData *)&ogHAGData;
	else if(omCalledFrom == LoadStg(IDS_STRING195)) pomCedaData = (CCSCedaData *)&ogWROData;
	else if(omCalledFrom == LoadStg(IDS_STRING161)) pomCedaData = (CCSCedaData *)&ogHTYData;
	else if(omCalledFrom == LoadStg(IDS_STRING188)) pomCedaData = (CCSCedaData *)&ogSTYData;
	else if(omCalledFrom == LoadStg(IDS_STRING172)) pomCedaData = (CCSCedaData *)&ogFIDData;
	else if(omCalledFrom == LoadStg(IDS_STRING170)) pomCedaData = (CCSCedaData *)&ogSphData; //the same table SPH
	else if(omCalledFrom == LoadStg(IDS_STRING169)) pomCedaData = (CCSCedaData *)&ogSphData; //the same table SPH
	else if(omCalledFrom == LoadStg(IDS_STRING189)) pomCedaData = (CCSCedaData *)&ogStrData;
	else if(omCalledFrom == LoadStg(IDS_STRING175)) pomCedaData = (CCSCedaData *)&ogSeaData;
	else if(omCalledFrom == LoadStg(IDS_STRING181)) pomCedaData = (CCSCedaData *)&ogGhsData;
	else if(omCalledFrom == LoadStg(IDS_STRING186)) pomCedaData = (CCSCedaData *)&ogPerData;
	else if(omCalledFrom == LoadStg(IDS_STRING184)) pomCedaData = (CCSCedaData *)&ogOrgData;
	else if(omCalledFrom == LoadStg(IDS_STRING162)) pomCedaData = (CCSCedaData *)&ogCotData;
	else if(omCalledFrom == LoadStg(IDS_STRING165)) pomCedaData = (CCSCedaData *)&ogAsfData;
	else if(omCalledFrom == LoadStg(IDS_STRING164)) pomCedaData = (CCSCedaData *)&ogBsdData;
	else if(omCalledFrom == LoadStg(IDS_STRING177)) pomCedaData = (CCSCedaData *)&ogPfcData;
	else if(omCalledFrom == LoadStg(IDS_STRING171)) pomCedaData = (CCSCedaData *)&ogTeaData;
	else if(omCalledFrom == LoadStg(IDS_STRING187)) pomCedaData = (CCSCedaData *)&ogPrcData;
	else if(omCalledFrom == LoadStg(IDS_STRING168)) pomCedaData = (CCSCedaData *)&ogOdaData;
	else if(omCalledFrom == LoadStg(IDS_STRING183)) pomCedaData = (CCSCedaData *)&ogStfData;
	else if(omCalledFrom == LoadStg(IDS_STRING180)) pomCedaData = (CCSCedaData *)&ogGegData;
	else if(omCalledFrom == LoadStg(IDS_STRING196)) pomCedaData = (CCSCedaData *)&ogWayData;
	else if(omCalledFrom == LoadStg(IDS_STRING193)) pomCedaData = (CCSCedaData *)&ogChtData;
	else if(omCalledFrom == LoadStg(IDS_STRING197)) pomCedaData = (CCSCedaData *)&ogTipData;
	else if(omCalledFrom == LoadStg(IDS_STRING198)) pomCedaData = (CCSCedaData *)&ogHolData;
	else if(omCalledFrom == LoadStg(IDS_STRING199)) pomCedaData = (CCSCedaData *)&ogWgpData;
	else if(omCalledFrom == LoadStg(IDS_STRING200)) pomCedaData = (CCSCedaData *)&ogPgpData;
	else if(omCalledFrom == LoadStg(IDS_STRING201)) pomCedaData = (CCSCedaData *)&ogAwiData;
	else if(omCalledFrom == LoadStg(IDS_STRING202)) pomCedaData = (CCSCedaData *)&ogCccData;
	else if(omCalledFrom == LoadStg(IDS_STRING203)) pomCedaData = (CCSCedaData *)&ogVipData;
	else if(omCalledFrom == LoadStg(IDS_STRING618)) pomCedaData = (CCSCedaData *)&ogAFMData;
	else if(omCalledFrom == LoadStg(IDS_STRING619)) pomCedaData = (CCSCedaData *)&ogENTData;
	else if(omCalledFrom == LoadStg(IDS_STRING764)) pomCedaData = (CCSCedaData *)&ogMfmData;
	else if(omCalledFrom == LoadStg(IDS_STRING766)) pomCedaData = (CCSCedaData *)&ogCohData;
	else if(omCalledFrom == LoadStg(IDS_STRING765)) pomCedaData = (CCSCedaData *)&ogStsData;
	else if(omCalledFrom == LoadStg(IDS_STRING763)) pomCedaData = (CCSCedaData *)&ogNwhData;
	else if(omCalledFrom == LoadStg(IDS_STRING730)) pomCedaData = (CCSCedaData *)&ogParData;
	else if(omCalledFrom == LoadStg(IDS_STRING842)) pomCedaData = (CCSCedaData *)&ogPolData;
	else if(omCalledFrom == LoadStg(IDS_STRING132)) pomCedaData = (CCSCedaData *)&ogPmxData;
	else if(omCalledFrom == LoadStg(IDS_STRING131)) pomCedaData = (CCSCedaData *)&ogWisData;
	else if(omCalledFrom == LoadStg(IDS_STRING825)) pomCedaData = (CCSCedaData *)&ogSrcData;
	else if(omCalledFrom == LoadStg(IDS_STRING432)) pomCedaData = (CCSCedaData *)&ogEquData;
	else if(omCalledFrom == LoadStg(IDS_STRING862)) pomCedaData = (CCSCedaData *)&ogEqtData;
	else if(omCalledFrom == LoadStg(IDS_STRING897)) pomCedaData = (CCSCedaData *)&ogMaaData;
	else if(omCalledFrom == LoadStg(IDS_STRING929)) pomCedaData = (CCSCedaData *)&ogDEVData;
	else if(omCalledFrom == LoadStg(IDS_STRING930)) pomCedaData = (CCSCedaData *)&ogDSPData;
	else if(omCalledFrom == LoadStg(IDS_STRING931)) pomCedaData = (CCSCedaData *)&ogPAGData;
	else if(omCalledFrom == LoadStg(IDS_STRING32795)) pomCedaData = (CCSCedaData *)&ogCrcData; 
	else if(omCalledFrom == LoadStg(IDS_STRING32806)) pomCedaData = (CCSCedaData *)&ogAadData; 
	else if(omCalledFrom == LoadStg(IDS_STRING32805)) pomCedaData = (CCSCedaData *)&ogPdaData;	 
	else if(omCalledFrom == LoadStg(IDS_STRING1001)) pomCedaData = (CCSCedaData *)&ogDSCData;
	else if(omCalledFrom == LoadStg(IDS_STRING1147)) pomCedaData = (CCSCedaData *)&ogFLGData;
	else if(omCalledFrom == LoadStg(IDS_STRING1157)) pomCedaData = (CCSCedaData *)&ogCHUData;
}

void CPSUniSortPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSUniSortPage)
	DDX_Control(pDX, IDC_LIST_FIELDS, m_ListFields);
	//DDX_Text(pDX, IDC_EDIT_SORT, m_EditSort);
	DDX_Control(pDX, IDC_EDIT_SORT, E_SortText);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(CPSUniSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPSUniSortPage)
	ON_BN_CLICKED(IDC_BUTTON_ASC, OnButtonAsc)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_DESC, OnButtonDesc)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage message handlers

void CPSUniSortPage::GetData()
{
	omValues.RemoveAll();
	E_SortText.SetWindowText(m_EditSort);
	CString olSubString = m_EditSort;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(' ');
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(' '));
				olSubString = olSubString.Mid(olSubString.Find(' ')+1, olSubString.GetLength( )-olSubString.Find(' ')+1);
			}
			omValues.Add(olText);
		}
	}
}

void CPSUniSortPage::SetData()
{
	m_ListFields.SetCurSel(-1);
	E_SortText.SetWindowText("");
	m_EditSort = "";
	for(int i = 0; i < omValues.GetSize(); i++)
	{
		m_EditSort += omValues[i] + CString(" ");
	}
	E_SortText.SetWindowText(m_EditSort);
}

void CPSUniSortPage::InitMask()
{
	SetFieldAndDescriptionArrays();
	int ilCount = omFieldArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		char pclText[200]=""; 
		sprintf(pclText, "%s...%s", omFieldArray[i], omDescArray[i]);
		m_ListFields.AddString(pclText);
	}
} 

void CPSUniSortPage::OnButtonAsc() 
{
	E_SortText.GetWindowText(m_EditSort);
	CString olFilter;
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
	}
	if(m_EditSort.Find(olFilter) == -1)
	{
		m_EditSort+=olFilter + "+ ";
	}
	E_SortText.SetWindowText(m_EditSort); 
}

void CPSUniSortPage::OnButtonDesc() 
{

	CString olFilter;
	E_SortText.GetWindowText(m_EditSort);
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
	}
	if(m_EditSort.Find(olFilter) == -1)
	{
		m_EditSort+=olFilter + "- ";
	}
	E_SortText.SetWindowText(m_EditSort);
}

void CPSUniSortPage::OnButtonDelete() 
{
	CString olFilter;
	E_SortText.GetWindowText(m_EditSort);
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
		int ilPos = m_EditSort.Find(olFilter);
		if(ilPos != -1)
		{
			CString olP1 = m_EditSort.Left(ilPos);
			CString olP2 = m_EditSort.Right((m_EditSort.GetLength()-1) - (ilPos+olFilter.GetLength()+1));
			m_EditSort = olP1 + olP2;
			E_SortText.SetWindowText(m_EditSort);
		}
	}
}

void CPSUniSortPage::OnButtonNew() 
{
	E_SortText.SetWindowText("");
}

BOOL CPSUniSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	m_ListFields.SetFont(&ogCourier_Regular_8);
	InitMask();	
	return TRUE;
}


void CPSUniSortPage::SetFieldAndDescriptionArrays()
{
	CStringArray olTypeArray;
	omFieldArray.RemoveAll();
	omDescArray.RemoveAll();
	
	if (pomCedaData != NULL)
	{
		pomCedaData->GetDataInfo(omFieldArray, omDescArray, olTypeArray);
	}

	if (omCalledFrom == LoadStg(IDS_STRING183) || omCalledFrom == LoadStg(IDS_STRING164))
	{
		omFieldArray.Add("SPFCODE");
		omDescArray.Add("Function");
		olTypeArray.Add("String");
	}

}