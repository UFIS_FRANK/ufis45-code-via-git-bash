// CedaNwhData.cpp
 
#include <stdafx.h>
#include <CedaNwhData.h>
#include <resource.h>


void ProcessNwhCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaNwhData::CedaNwhData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for NWHDATA
	BEGIN_CEDARECINFO(NWHDATA,NwhDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Hr01,"HR01")
		FIELD_CHAR_TRIM	(Hr02,"HR02")
		FIELD_CHAR_TRIM	(Hr03,"HR03")
		FIELD_CHAR_TRIM	(Hr04,"HR04")
		FIELD_CHAR_TRIM	(Hr05,"HR05")
		FIELD_CHAR_TRIM	(Hr06,"HR06")
		FIELD_CHAR_TRIM	(Hr07,"HR07")
		FIELD_CHAR_TRIM	(Hr08,"HR08")
		FIELD_CHAR_TRIM	(Hr09,"HR09")
		FIELD_CHAR_TRIM	(Hr10,"HR10")
		FIELD_CHAR_TRIM	(Hr11,"HR11")
		FIELD_CHAR_TRIM	(Hr12,"HR12")
		FIELD_CHAR_TRIM	(Year,"YEAR")
		FIELD_CHAR_TRIM	(Regi,"REGI")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")

	END_CEDARECINFO //(NWHDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(NwhDataRecInfo)/sizeof(NwhDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&NwhDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"NWH");
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,HR01,HR02,HR03,HR04,HR05,HR06,HR07,HR08,HR09,HR10,HR11,HR12,YEAR,REGI,REMA");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaNwhData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("HR01");
	ropFields.Add("HR02");
	ropFields.Add("HR03");
	ropFields.Add("HR04");
	ropFields.Add("HR05");
	ropFields.Add("HR06");
	ropFields.Add("HR07");
	ropFields.Add("HR08");
	ropFields.Add("HR09");
	ropFields.Add("HR10");
	ropFields.Add("HR11");
	ropFields.Add("HR12");
	ropFields.Add("YEAR");
	ropFields.Add("REGI");
	ropFields.Add("REMA");
	ropFields.Add("CTRC");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING792));
	ropDesription.Add(LoadStg(IDS_STRING793));
	ropDesription.Add(LoadStg(IDS_STRING794));
	ropDesription.Add(LoadStg(IDS_STRING795));
	ropDesription.Add(LoadStg(IDS_STRING796));
	ropDesription.Add(LoadStg(IDS_STRING797));
	ropDesription.Add(LoadStg(IDS_STRING798));
	ropDesription.Add(LoadStg(IDS_STRING799));
	ropDesription.Add(LoadStg(IDS_STRING800));
	ropDesription.Add(LoadStg(IDS_STRING801));
	ropDesription.Add(LoadStg(IDS_STRING802));
	ropDesription.Add(LoadStg(IDS_STRING803));
	ropDesription.Add(LoadStg(IDS_STRING681));
	ropDesription.Add(LoadStg(IDS_STRING804));
	ropDesription.Add(LoadStg(IDS_STRING805));
	ropDesription.Add(LoadStg(IDS_STRING1014));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaNwhData::Register(void)
{
	ogDdx.Register((void *)this,BC_NWH_CHANGE,	CString("NWHDATA"), CString("Nwh-changed"),	ProcessNwhCf);
	ogDdx.Register((void *)this,BC_NWH_NEW,		CString("NWHDATA"), CString("Nwh-new"),		ProcessNwhCf);
	ogDdx.Register((void *)this,BC_NWH_DELETE,	CString("NWHDATA"), CString("Nwh-deleted"),	ProcessNwhCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaNwhData::~CedaNwhData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaNwhData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaNwhData::Read(char *pspWhere /*NULL*/)
{
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,HR01,HR02,HR03,HR04,HR05,HR06,HR07,HR08,HR09,HR10,HR11,HR12,YEAR,REGI,REMA,CTRC");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		NWHDATA *prlNwh = new NWHDATA;
		if ((ilRc = GetFirstBufferRecord(prlNwh)) == true)
		{
			omData.Add(prlNwh);//Update omData
			omUrnoMap.SetAt((void *)prlNwh->Urno,prlNwh);
		}
		else
		{
			delete prlNwh;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaNwhData::Insert(NWHDATA *prpNwh)
{
	prpNwh->IsChanged = DATA_NEW;
	if(Save(prpNwh) == false) return false; //Update Database
	InsertInternal(prpNwh);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaNwhData::InsertInternal(NWHDATA *prpNwh)
{
	ogDdx.DataChanged((void *)this, NWH_NEW,(void *)prpNwh ); //Update Viewer
	omData.Add(prpNwh);//Update omData
	omUrnoMap.SetAt((void *)prpNwh->Urno,prpNwh);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaNwhData::Delete(long lpUrno)
{
	NWHDATA *prlNwh = GetNwhByUrno(lpUrno);
	if (prlNwh != NULL)
	{
		prlNwh->IsChanged = DATA_DELETED;
		if(Save(prlNwh) == false) return false; //Update Database
		DeleteInternal(prlNwh);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaNwhData::DeleteInternal(NWHDATA *prpNwh)
{
	ogDdx.DataChanged((void *)this,NWH_DELETE,(void *)prpNwh); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpNwh->Urno);
	int ilNwhCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilNwhCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpNwh->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaNwhData::Update(NWHDATA *prpNwh)
{
	if (GetNwhByUrno(prpNwh->Urno) != NULL)
	{
		if (prpNwh->IsChanged == DATA_UNCHANGED)
		{
			prpNwh->IsChanged = DATA_CHANGED;
		}
		if(Save(prpNwh) == false) return false; //Update Database
		UpdateInternal(prpNwh);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaNwhData::UpdateInternal(NWHDATA *prpNwh)
{
	NWHDATA *prlNwh = GetNwhByUrno(prpNwh->Urno);
	if (prlNwh != NULL)
	{
		*prlNwh = *prpNwh; //Update omData
		ogDdx.DataChanged((void *)this,NWH_CHANGE,(void *)prlNwh); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

NWHDATA *CedaNwhData::GetNwhByUrno(long lpUrno)
{
	NWHDATA  *prlNwh;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlNwh) == TRUE)
	{
		return prlNwh;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaNwhData::ReadSpecialData(CCSPtrArray<NWHDATA> *popNwh,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","NWH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","NWH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popNwh != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			NWHDATA *prpNwh = new NWHDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpNwh,CString(pclFieldList))) == true)
			{
				popNwh->Add(prpNwh);
			}
			else
			{
				delete prpNwh;
			}
		}
		if(popNwh->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaNwhData::Save(NWHDATA *prpNwh)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpNwh->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpNwh->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpNwh);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpNwh->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpNwh->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpNwh);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpNwh->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpNwh->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessNwhCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogNwhData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaNwhData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlNwhData;
	prlNwhData = (struct BcStruct *) vpDataPointer;
	NWHDATA *prlNwh;
	if(ipDDXType == BC_NWH_NEW)
	{
		prlNwh = new NWHDATA;
		GetRecordFromItemList(prlNwh,prlNwhData->Fields,prlNwhData->Data);
		if(ValidateNwhBcData(prlNwh->Urno)) //Prf: 8795
		{
			InsertInternal(prlNwh);
		}
		else
		{
			delete prlNwh;
		}
	}
	if(ipDDXType == BC_NWH_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlNwhData->Selection);
		prlNwh = GetNwhByUrno(llUrno);
		if(prlNwh != NULL)
		{
			GetRecordFromItemList(prlNwh,prlNwhData->Fields,prlNwhData->Data);
			if(ValidateNwhBcData(prlNwh->Urno)) //Prf: 8795
			{
				UpdateInternal(prlNwh);
			}
			else
			{
				DeleteInternal(prlNwh);
			}
		}
	}
	if(ipDDXType == BC_NWH_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlNwhData->Selection);

		prlNwh = GetNwhByUrno(llUrno);
		if (prlNwh != NULL)
		{
			DeleteInternal(prlNwh);
		}
	}
}

//Prf: 8795
//--ValidateNwhBcData--------------------------------------------------------------------------------------

bool CedaNwhData::ValidateNwhBcData(const long& lrpUrno)
{
	bool blValidateNwhBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<NWHDATA> olNwhs;
		if(!ReadSpecialData(&olNwhs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateNwhBcData = false;
		}
	}
	return blValidateNwhBcData;
}

//---------------------------------------------------------------------------------------------------------
