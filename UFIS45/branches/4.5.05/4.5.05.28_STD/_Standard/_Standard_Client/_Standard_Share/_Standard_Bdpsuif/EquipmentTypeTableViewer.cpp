// EquipmentTypeTableViewer.cpp 
//

#include <stdafx.h>
#include <EquipmentTypeTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void EquipmentTypeTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// EquipmentTypeTableViewer
//

EquipmentTypeTableViewer::EquipmentTypeTableViewer(CCSPtrArray<EQTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomEquipmentTypeTable = NULL;
    ogDdx.Register(this, EQT_CHANGE, CString("EquipmentTypeTableViewer"), CString("EquipmentType Update"), EquipmentTypeTableCf);
    ogDdx.Register(this, EQT_NEW,    CString("EquipmentTypeTableViewer"), CString("EquipmentType New"),    EquipmentTypeTableCf);
    ogDdx.Register(this, EQT_DELETE, CString("EquipmentTypeTableViewer"), CString("EquipmentType Delete"), EquipmentTypeTableCf);
}

//-----------------------------------------------------------------------------------------------

EquipmentTypeTableViewer::~EquipmentTypeTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::Attach(CCSTable *popTable)
{
    pomEquipmentTypeTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::MakeLines()
{
	int ilEquipmentTypeCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilEquipmentTypeCount; ilLc++)
	{
		EQTDATA *prlEquipmentTypeData = &pomData->GetAt(ilLc);
		MakeLine(prlEquipmentTypeData);
	}
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::MakeLine(EQTDATA *prpEquipmentType)
{

    //if( !IsPassFilter(prpEquipmentType)) return;

    // Update viewer data for this shift record
    EQUIPMENTTYPETABLE_LINEDATA rlEquipmentType;


	rlEquipmentType.Urno = prpEquipmentType->Urno;
	rlEquipmentType.Code = prpEquipmentType->Code;
	rlEquipmentType.Name = prpEquipmentType->Name;
	
	CreateLine(&rlEquipmentType);
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::CreateLine(EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareEquipmentType(prpEquipmentType, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	EQUIPMENTTYPETABLE_LINEDATA rlEquipmentType;
	rlEquipmentType = *prpEquipmentType;
    omLines.NewAt(ilLineno, rlEquipmentType);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void EquipmentTypeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 1;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomEquipmentTypeTable->SetShowSelection(TRUE);
	pomEquipmentTypeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 50;  //CODE 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING865),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332;  //NAME
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING865),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	pomEquipmentTypeTable->SetHeaderFields(omHeaderDataArray);
	pomEquipmentTypeTable->SetDefaultSeparator();
	pomEquipmentTypeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Code;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Name;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomEquipmentTypeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomEquipmentTypeTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString EquipmentTypeTableViewer::Format(EQUIPMENTTYPETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTypeTableViewer::FindEquipmentType(char *pcpEquipmentTypeKeya, char *pcpEquipmentTypeKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpEquipmentTypeKeya) &&
			 (omLines[ilItem].Keyd == pcpEquipmentTypeKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void EquipmentTypeTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    EquipmentTypeTableViewer *polViewer = (EquipmentTypeTableViewer *)popInstance;
    if (ipDDXType == EQT_CHANGE || ipDDXType == EQT_NEW) polViewer->ProcessEquipmentTypeChange((EQTDATA *)vpDataPointer);
    if (ipDDXType == EQT_DELETE) polViewer->ProcessEquipmentTypeDelete((EQTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::ProcessEquipmentTypeChange(EQTDATA *prpEquipmentType)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;
	CString olText;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpEquipmentType->Code;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEquipmentType->Name;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpEquipmentType->Urno, ilItem))
	{
        EQUIPMENTTYPETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpEquipmentType->Urno;
		prlLine->Code = prpEquipmentType->Code;
		prlLine->Name = prpEquipmentType->Name;
		
		pomEquipmentTypeTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomEquipmentTypeTable->DisplayTable();
	}
	else
	{
		MakeLine(prpEquipmentType);
		if (FindLine(prpEquipmentType->Urno, ilItem))
		{
	        EQUIPMENTTYPETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomEquipmentTypeTable->AddTextLine(olLine, (void *)prlLine);
				pomEquipmentTypeTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::ProcessEquipmentTypeDelete(EQTDATA *prpEquipmentType)
{
	int ilItem;
	if (FindLine(prpEquipmentType->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomEquipmentTypeTable->DeleteTextLine(ilItem);
		pomEquipmentTypeTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTypeTableViewer::IsPassFilter(EQTDATA *prpEquipmentType)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int EquipmentTypeTableViewer::CompareEquipmentType(EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType1, EQUIPMENTTYPETABLE_LINEDATA *prpEquipmentType2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpEquipmentType1->Tifd == prpEquipmentType2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpEquipmentType1->Tifd > prpEquipmentType2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTypeTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void EquipmentTypeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING981);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTypeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTypeTableViewer::PrintTableLine(EQUIPMENTTYPETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;


			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Code;
				break;
			case 1:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Name;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
