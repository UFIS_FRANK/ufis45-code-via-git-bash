#if !defined(AFX_CHUTEDLG_H__89BF0F5F_21C7_49f8_A31C_317A9D9D63BE__INCLUDED_)
#define AFX_CHUTEDLG_H__89BF0F5F_21C7_49f8_A31C_317A9D9D63BE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChuteDlg.h : header file
//
#include <CedaCHUData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CedaBlkData.h>
#include <CedaDatData.h>
/////////////////////////////////////////////////////////////////////////////
// CHUTEDlg dialog

class ChuteDlg : public CDialog
{
// Construction
public:
	ChuteDlg(CHUDATA *popCHU,CWnd* pParent = NULL);   // standard constructor
	ChuteDlg(CHUDATA *popCHU,int ipDlg,CWnd* pParent = NULL);   // standard constructor
	~ChuteDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;	

// Dialog Data
	//{{AFX_DATA(ChuteDlg)
	enum { IDD = IDD_CHUTEDLG };
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_CNAM;
	CCSEdit	m_TERM;
	CCSEdit	m_SORT;
	CCSEdit	m_MAXF;

	CButton	m_OK;
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CButton	m_UTC;
	CButton	m_LOCAL;

	CStatic	m_NOAVFRAME;
	CString	m_Caption;
	
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ChuteDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ChuteDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	afx_msg void OnNoavCopy();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

	CHUDATA		*pomCHU;
	CCSTable	*pomTable;
	int			imLastSelection;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
	void UpdateNoavTable();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHUTEDLG_H__89BF0F5F_21C7_49f8_A31C_317A9D9D63BE__INCLUDED_)
