// GatPosNotAvailableDlgEx.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "GatPosNotAvailableDlgEx.h"
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg dialog


GatPosNotAvailableDlgEx::GatPosNotAvailableDlgEx(BLKDATA *popBlk,CWnd* pParent /*=NULL*/)
	: GatPosNotAvailableDlg(popBlk,pParent)
{
	//{{AFX_DATA_INIT(GatPosNotAvailableDlgEx)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosNotAvailableDlgEx::DoDataExchange(CDataExchange* pDX)
{
	GatPosNotAvailableDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosNotAvailableDlgEx)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

int  GatPosNotAvailableDlgEx::GetNames(CStringArray& ropNames)
{
	ropNames.Append(omSelectedNames);
	return ropNames.GetSize();
}

void GatPosNotAvailableDlgEx::SetNames(const CStringArray& ropNames)
{
	omSelectedNames.Append(ropNames);
}

BEGIN_MESSAGE_MAP(GatPosNotAvailableDlgEx, GatPosNotAvailableDlg)
	//{{AFX_MSG_MAP(GatPosNotAvailableDlgEx)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg message handlers

BOOL GatPosNotAvailableDlgEx::OnInitDialog() 
{
	GatPosNotAvailableDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(LoadStg(IDS_STRING910));

	CWnd *polEdit = GetDlgItem(IDC_NAME);
	if (polEdit)
		polEdit->ShowWindow(SW_HIDE);

	for (int i = 0; i < omSelectedNames.GetSize(); i++)
	{
		omNames.AddString(omSelectedNames[i]);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosNotAvailableDlgEx::OnOK() 
{
	// TODO: Add extra validation here
	omSelectedNames.RemoveAll();

	int ilSelCount = omNames.GetSelCount();
	if (ilSelCount > 0)
	{
		int *pilSelItems = new int[ilSelCount];
		omNames.GetSelItems(ilSelCount,pilSelItems);

		for (int i = 0; i < ilSelCount; i++)
		{
			CString olName;
			omNames.GetText(pilSelItems[i],olName);
			omSelectedNames.Add(olName);
		}

		delete[] pilSelItems;
		GatPosNotAvailableDlg::OnOK();
	}
}

int GatPosNotAvailableDlgEx::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (GatPosNotAvailableDlg::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_HSCROLL|WS_VSCROLL|LBS_MULTIPLESEL|LBS_STANDARD;
	CRect olRect(83,5,83+100,5+60);
//	this->ClientToScreen(&olRect);
	omNames.Create(dwStyle,olRect,this,IDC_NAMES);
	
	return 0;
}
