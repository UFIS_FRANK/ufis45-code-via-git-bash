// ExtLMGatPosWaitingroomDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "ExtLMGatPosWaitingroomDlg.h"
#include <PrivList.h>
#include <GatPosNotAvailableDlg.h>
#include <GatPosNotAvailableDlgEx.h>
#include <CedaWroData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosWaitingroomDlg dialog


ExtLMGatPosWaitingroomDlg::ExtLMGatPosWaitingroomDlg(WRODATA *popWRO,long lpOriginalUrno,CWnd* pParent /*=NULL*/)
	: GatPosWaitingroomDlg(popWRO,ExtLMGatPosWaitingroomDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExtLMGatPosWaitingroomDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->lmOriginalUrno = lpOriginalUrno;

}


void ExtLMGatPosWaitingroomDlg::DoDataExchange(CDataExchange* pDX)
{
	GatPosWaitingroomDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExtLMGatPosWaitingroomDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExtLMGatPosWaitingroomDlg, GatPosWaitingroomDlg)
	//{{AFX_MSG_MAP(ExtLMGatPosWaitingroomDlg)
	ON_BN_CLICKED(IDC_DAT_NEW, OnDatNew)
	ON_BN_CLICKED(IDC_DAT_DEL, OnDatDelete)
	ON_BN_CLICKED(IDC_DAT_COPY,OnDatCopy)
	ON_BN_CLICKED(IDC_DAT_ASSIGN, OnDatAssign)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosWaitingroomDlg message handlers

void ExtLMGatPosWaitingroomDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	bool ilStatus = true;
	CString olErrorText;

	if (m_SHGN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING895) + ogNotFormat;
	}

	if (!this->omGrid.FinalCheck(this->omDatPtrA,this->omDeleteDatPtrA,olErrorText))
	{
		ilStatus = false;
	}

	if (ilStatus == true)
	{
		m_SHGN.GetWindowText(pomWRO->Shgn,2);
		this->pomWRO->Ibit = m_BlockingBitmap.GetCurSel();
		WaitingroomDlg::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_SHGN.SetFocus();
		m_SHGN.SetSel(0,-1);
	}
}

BOOL ExtLMGatPosWaitingroomDlg::OnInitDialog() 
{
	GatPosWaitingroomDlg::OnInitDialog();
	
	// TODO: Add extra initialization here

	this->omGrid.SubClassDlgItem(IDC_DAT,this);


	CWnd *polWnd = NULL;
	switch(ogPrivList.GetStat("WAITINGROOMDLG.m_DEFD"))
	{
	case '0' :	// Readonly
		polWnd = GetDlgItem(IDC_DAT_NEW);
		if (polWnd)
			polWnd->EnableWindow(FALSE);
		polWnd = GetDlgItem(IDC_DAT_DEL);
		if (polWnd)
			polWnd->EnableWindow(FALSE);

		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("WRO",this->pomWRO->Urno,false,this->omDatPtrA,true);
		else
			this->omGrid.Initialize("WRO",lmOriginalUrno,true,this->omDatPtrA,true);
	break;
	case '-' :	// Hide
		polWnd = GetDlgItem(IDC_DAT_NEW);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_DAT_DEL);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_HIDE);
		}
		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("WRO",this->pomWRO->Urno,false,this->omDatPtrA,true);
		else
			this->omGrid.Initialize("WRO",lmOriginalUrno,true,this->omDatPtrA,true);
		this->omGrid.ShowWindow(SW_HIDE);
	break;
	default:
		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("WRO",this->pomWRO->Urno,false,this->omDatPtrA,false);
		else
			this->omGrid.Initialize("WRO",lmOriginalUrno,true,this->omDatPtrA,false);

	}



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosWaitingroomDlg::OnDatNew() 
{
	this->omGrid.Insert(this->omDatPtrA);
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosWaitingroomDlg::OnDatDelete() 
{
	this->omGrid.Delete(this->omDatPtrA,this->omDeleteDatPtrA);
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosWaitingroomDlg::OnDatCopy() 
{
	this->omGrid.Copy(this->omDatPtrA);
}

void ExtLMGatPosWaitingroomDlg::OnDatAssign() 
{
	// TODO: Add your control notification handler code here
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<WRODATA> olList;
	CString olOrderBy = "ORDER BY WNAM";
	if (ogWROData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,WNAM", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			if (this->pomWRO->Urno != olList[i].Urno)
			{
				char pclUrno[20]="";
				sprintf(pclUrno, "%ld", olList[i].Urno);
				olCol3.Add(CString(pclUrno));
				olCol1.Add(CString(olList[i].Wnam));
			}
		}

		this->omGrid.Assign(this->omDatPtrA,this->omDeleteDatPtrA,olCol1,olCol3,LoadStg(IDS_STRING195));
	}			
}
