// ADR_Dlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <ADR_Dlg.h>
#include <CedaBasicData.h>
#include <basicdata.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CADR_Dlg 

CADR_Dlg::CADR_Dlg(CWnd* pParent /*=NULL*/, CString STFU)
	: CDialog(CADR_Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CADR_Dlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	m_Stfu = STFU;
	pomGrid= NULL;
}

CADR_Dlg::~CADR_Dlg()
{
	delete pomGrid;
}

void CADR_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CADR_Dlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CADR_Dlg, CDialog)
	//{{AFX_MSG_MAP(CADR_Dlg)
	ON_BN_CLICKED(IDC_Del, OnDel)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_ENDEDIT, OnGridEndEditing)
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------
// Behandlungsroutinen f�r Nachrichten CADR_Dlg 
BOOL CADR_Dlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pomGrid = new CGridControl(this,IDC_ADR_Grid,12,1);

	InitGrid();
	//ReadADR_Data();
	ReadSTF_Data();
	FillGrid();

	SetWindowText(m_Lanm + ", " + m_Finm);

	// TODO: Zus�tzliche Initialisierung hier einf�gen
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
//---------------------------------------------------------------------------
void CADR_Dlg::InitGrid()
{
	
	/*pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	pomGrid->SetColCount(10+1);
	pomGrid->LockUpdate(FALSE);
	pomGrid->GetParam()->EnableUndo(TRUE);
	pomGrid->SetAutoScroll(true);
	pomGrid->ShowScrollBar(0, true);
	pomGrid->ShowScrollBar(1, true);
	pomGrid->GetParam()->EnableTrackRowHeight(FALSE);		//disable rowsizing
	pomGrid->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	pomGrid->GetParam()->EnableSelection(GX_SELNONE);*/


	pomGrid->SetValueRange(CGXRange(0, 0), "Nbr");
	pomGrid->SetValueRange(CGXRange(0, 1), "Zip-Code");
	pomGrid->SetValueRange(CGXRange(0, 2), "City");
	pomGrid->SetValueRange(CGXRange(0, 3), "Street");
	pomGrid->SetValueRange(CGXRange(0, 4), "Adr. c/o");
	pomGrid->SetValueRange(CGXRange(0, 5), "Country");
	pomGrid->SetValueRange(CGXRange(0, 6), "Pre-P1");
	pomGrid->SetValueRange(CGXRange(0, 7), "Phone no.1");
	pomGrid->SetValueRange(CGXRange(0, 8), "Pre-P2");
	pomGrid->SetValueRange(CGXRange(0, 9), "Phone no.2");
	pomGrid->SetValueRange(CGXRange(0,10), "Valid from");
	pomGrid->SetValueRange(CGXRange(0,11), "Valid to");

	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomGrid->GetParam()->EnableTrackColWidth();
	pomGrid->GetParam()->EnableTrackRowHeight(FALSE);
    pomGrid->GetParam()->EnableSelection(GX_SELROW);

	//Scrollbars anzeigen wenn n�tig
	pomGrid->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	pomGrid->SetStyleRange(CGXRange().SetCols(1), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(7));
	pomGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(26));
	pomGrid->SetStyleRange(CGXRange().SetCols(3), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(26));
	pomGrid->SetStyleRange(CGXRange().SetCols(4), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)		
							.SetMaxLength(26));
	pomGrid->SetStyleRange(CGXRange().SetCols(5), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(3));
	pomGrid->SetStyleRange(CGXRange().SetCols(6), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(10));
	pomGrid->SetStyleRange(CGXRange().SetCols(7), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(11));
	pomGrid->SetStyleRange(CGXRange().SetCols(8), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(10));
	pomGrid->SetStyleRange(CGXRange().SetCols(9), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							//.SetFormat(GX_FMT_TEXT)
							.SetMaxLength(11));
		
	pomGrid->SetStyleRange(CGXRange().SetCols(10), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							.SetFormat(GX_FMT_DATE)
							.SetMaxLength(10));
	pomGrid->SetStyleRange(CGXRange().SetCols(11), CGXStyle()
							.SetControl(GX_IDS_CTRL_EDIT)
							.SetFormat(GX_FMT_DATE)
							.SetMaxLength(10));
}
//---------------------------------------------------------------------------
void CADR_Dlg::ReadADR_Data()
{
	CString clWhere;

	clWhere.Format("WHERE STFU='%s'",m_Stfu);
	ogBCD.SetObject("ADR","URNO,STFU,PENO,STRA,ADRC,ZIPA,CITY,LANA,VWT1,TEL1,VWT2,TEL2,VAFR,VATO,CDAT,USEC,USEU,LSTU,HOPO");
	ogBCD.Read("ADR",clWhere);

	im_UrnoPos=ogBCD.GetFieldIndex("ADR","URNO");
	if (im_UrnoPos < 0) 
		im_UrnoPos=0;
	im_StfuPos=ogBCD.GetFieldIndex("ADR","STFU");
	if (im_StfuPos < 0) 
		im_StfuPos=0;
	im_PenoPos=ogBCD.GetFieldIndex("ADR","PENO");
	if (im_PenoPos < 0) 
		im_PenoPos=0;
	im_StraPos=ogBCD.GetFieldIndex("ADR","STRA");
	if (im_StraPos < 0) 
		im_StraPos=0;
	im_AdrcPos=ogBCD.GetFieldIndex("ADR","ADRC");
	if (im_AdrcPos < 0) 
		im_AdrcPos=0;
	im_ZipaPos=ogBCD.GetFieldIndex("ADR","ZIPA");
	if (im_ZipaPos < 0) 
		im_ZipaPos=0;
	im_CityPos=ogBCD.GetFieldIndex("ADR","CITY");
	if (im_CityPos < 0) 
		im_CityPos=0;
	im_LanaPos=ogBCD.GetFieldIndex("ADR","LANA");
	if (im_LanaPos < 0) 
		im_LanaPos=0;
	im_Vwt1Pos=ogBCD.GetFieldIndex("ADR","VWT1");
	if (im_Vwt1Pos < 0) 
		im_Vwt1Pos=0;
	im_Tel1Pos=ogBCD.GetFieldIndex("ADR","TEL1");
	if (im_Tel1Pos < 0) 
		im_Tel1Pos=0;
	im_Vwt2Pos=ogBCD.GetFieldIndex("ADR","VWT2");
	if (im_Vwt2Pos < 0) 
		im_Vwt2Pos=0;
	im_Tel2Pos=ogBCD.GetFieldIndex("ADR","TEL2");
	if (im_Tel2Pos < 0) 
		im_Tel2Pos=0;
	im_VafrPos=ogBCD.GetFieldIndex("ADR","VAFR");
	if (im_VafrPos < 0) 
		im_VafrPos=0;
	im_VatoPos=ogBCD.GetFieldIndex("ADR","VATO");
	if (im_VatoPos < 0) 
		im_VatoPos=0;
	im_CdatPos=ogBCD.GetFieldIndex("ADR","CDAT");
	if (im_CdatPos < 0) 
		im_CdatPos=0;
	im_UsecPos=ogBCD.GetFieldIndex("ADR","USEC");
	if (im_UsecPos < 0) 
		im_UsecPos=0;
	im_UseuPos=ogBCD.GetFieldIndex("ADR","USEU");
	if (im_UseuPos < 0) 
		im_UseuPos=0;
	im_LstuPos=ogBCD.GetFieldIndex("ADR","LSTU");
	if (im_LstuPos < 0) 
		im_LstuPos=0;
	im_HopoPos=ogBCD.GetFieldIndex("ADR","HOPO");
	if (im_HopoPos < 0) 
		im_HopoPos=0;

	//uhi 10.9.01
	if(m_Stfu == "0")
		im_ADRCount = 0;
	else
		im_ADRCount = ogBCD.GetDataCount("ADR"); //number of rows
	
	pomGrid->SetRowCount(im_ADRCount+1);
}
//---------------------------------------------------------------------------
void CADR_Dlg::ReadSTF_Data()
{
	CString clWhere;

	clWhere.Format("WHERE URNO='%s'",m_Stfu);
	ogBCD.SetObject("STF");
	ogBCD.Read("STF",clWhere);
	
	int ilDataCount;
	
	if (m_Stfu == "0")
		ilDataCount = 0;
	else
		ilDataCount = ogBCD.GetDataCount("STF");

	m_Lanm = ogBCD.GetField("STF", "URNO", m_Stfu, "LANM");
	m_Finm = ogBCD.GetField("STF", "URNO", m_Stfu, "FINM");
	m_Peno = ogBCD.GetField("STF", "URNO", m_Stfu, "PENO");
}
//---------------------------------------------------------------------------
void CADR_Dlg::FillGrid()
{
	InitMemberValue();

	//im_ADRCount = ogBCD.GetDataCount("ADR");
	//pomGrid->SetRowCount(im_ADRCount+1);

	// Grid mit Daten f�llen.
	pomGrid->GetParam()->SetLockReadOnly(false);

	//*** FILL THE GRID WITH EXISTING DATA ***
	for(int i = 0; i < im_ADRCount; i++)
	{
		CString VAFR = m_Vafr.GetAt(i);
		COleDateTime olFrom = ogBasicData.DBStringToDateTime(VAFR);
		if (olFrom.GetStatus() == COleDateTime::invalid)
		{
			int err=0;
		}
		else
		{
			VAFR = olFrom.Format("%d.%m.%Y");
		}
		
		CString VATO = m_Vato.GetAt(i);
		COleDateTime olTo = ogBasicData.DBStringToDateTime(VATO);
		if (olTo.GetStatus() == COleDateTime::invalid)
		{
			int err=0;
		}
		else
		{
			VATO = olTo.Format("%d.%m.%Y");
		}
		
		pomGrid->SetValueRange(CGXRange(i+1, 1),m_Zipa.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 2),m_City.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 3),m_Stra.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 4),m_Adrc.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 5),m_Lana.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 6),m_Vwt1.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 7),m_Tel1.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 8),m_Vwt2.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1, 9),m_Tel2.GetAt(i));
		pomGrid->SetValueRange(CGXRange(i+1,10),VAFR);
		pomGrid->SetValueRange(CGXRange(i+1,11),VATO);
		pomGrid->SetValueRange(CGXRange(i+1,12),"");
	}

	pomGrid->HideCols(12,12);

	// Anzeige einstellen
	pomGrid->GetParam()->SetLockReadOnly(true);
}
//---------------------------------------------------------------------------
bool CADR_Dlg::SaveADR_Data()
{
	int       cnt	= ogBCD.GetFieldCount("ADR");// to init recordset	
	bool blOK = true;
	RecordSet olTRecord(cnt);						// init recordset
	CString olTxt;
	CString olText;

	m_Peno = m_Peno.Right(6);
	olTRecord.Values[im_StfuPos] = m_Stfu;
	olTRecord.Values[im_PenoPos] = m_Peno;
	for (int i=0;i<im_ADRCount;i++)
	{
		if ("*" == pomGrid->GetValue(i+1,12))
		{
			CString olDate = m_Vafr.GetAt(i);
			if(olDate.GetLength() >= 8)
			{
				olDate = olDate.Mid(6,2) + CString(".") + olDate.Mid(4,2) + CString(".") + olDate.Left(4);
			}
			COleDateTime olFrom = OleDateStringToDate(olDate);

			if(olFrom.GetStatus() == COleDateTime::valid)
			{
				olTRecord.Values[im_StraPos] = m_Stra.GetAt(i);
				olTRecord.Values[im_AdrcPos] = m_Adrc.GetAt(i);
				olTRecord.Values[im_ZipaPos] = m_Zipa.GetAt(i);
				olTRecord.Values[im_CityPos] = m_City.GetAt(i);
				olTRecord.Values[im_LanaPos] = m_Lana.GetAt(i);
				olTRecord.Values[im_Vwt1Pos] = m_Vwt1.GetAt(i);
				olTRecord.Values[im_Tel1Pos] = m_Tel1.GetAt(i);
				olTRecord.Values[im_Vwt2Pos] = m_Vwt2.GetAt(i);
				olTRecord.Values[im_Tel2Pos] = m_Tel2.GetAt(i);
				olTRecord.Values[im_VafrPos] = m_Vafr.GetAt(i);
				olTRecord.Values[im_VatoPos] = m_Vato.GetAt(i);

				CString Urno = ogBCD.GetFieldExt("ADR", "STFU", "VAFR", m_Stfu, m_Vafr.GetAt(i), "URNO");

				if (!Urno.IsEmpty())
				{
					olTRecord.Values[im_UrnoPos] = Urno;
					ogBCD.SetRecord("ADR","URNO",Urno,olTRecord.Values,true);
				}
				else
				{	//NEW
					ogBCD.InsertRecord("ADR", olTRecord, true);
				}
				pomGrid->SetValue(i+1,12,CString(" "));
			}
			else
			{
				CString olNewText;
				olNewText.Format("Date 'Valid from' in line %d is invalid.\n",i+1);
				olText += olNewText;
				blOK = false;
			}
		}
	}
	if(!blOK)
	{
		Beep(440,70);
		MessageBox(olText,LoadStg(IDS_STRING145),MB_ICONERROR);
	}
	return blOK;
}
//---------------------------------------------------------------------------
void CADR_Dlg::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	
	if(SaveADR_Data())
		CDialog::OnOK();
}
//---------------------------------------------------------------------------
void CADR_Dlg::Test()
{
	CString test = pomGrid->GetValue(0,1);
}
//---------------------------------------------------------------------------
void CADR_Dlg::OnGridEndEditing( WPARAM wparam, LPARAM lparam )
{
	GRIDNOTIFY rlNotify;
	if ( lparam )
	{
		rlNotify = *((GRIDNOTIFY*)lparam);
		if ( rlNotify.row>0)
		{
			FillMember(rlNotify.row,rlNotify.col);
		}
	}
}
//---------------------------------------------------------------------------
void CADR_Dlg::FillMember(UINT row,UINT col)
{
	int colm = 12;
	if(!(int(row) <= im_ADRCount))
	{	//new
		CString Test = pomGrid->GetValue(row,col);
		if (!Test.IsEmpty())
		{
			pomGrid->InsertBottomRow();
			pomGrid->SetValue(row,colm,CString("*"));
			//-----------------
			if(col==1)
				m_Zipa.Add(Test);
			else
				m_Zipa.Add("");
			//-----------------
			if(col==2)
				m_City.Add(Test);
			else
				m_City.Add("");
			//-----------------
			if(col==3)
				m_Stra.Add(Test);
			else
				m_Stra.Add("");
			//-----------------
			if(col==4)
				m_Adrc.Add(Test);
			else
				m_Adrc.Add("");
			//-----------------
			if(col==5)
				m_Lana.Add(Test);
			else
				m_Lana.Add("");
			//-----------------
			if(col==6)
				m_Vwt1.Add(Test);
			else
				m_Vwt1.Add("");
			//-----------------
			if(col==7)
				m_Tel1.Add(Test);
			else
				m_Tel1.Add("");
			//-----------------
			if(col==8)
				m_Vwt2.Add(Test);
			else
				m_Vwt2.Add("");
			//-----------------
			if(col==9)
				m_Tel2.Add(Test);
			else
				m_Tel2.Add("");
			//-----------------
			if(col==10)
				m_Vafr.Add(Test);
			else
				m_Vafr.Add("");
			//-----------------
			if(col==11)
				m_Vato.Add(Test);
			else
				m_Vato.Add("");
			//-----------------
			im_ADRCount++;
		}
	}

	if (int(row) <= im_ADRCount)
	{	//update	
		switch (col)
		{
		case 1:
			{
				if (!(m_Zipa.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Zipa.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 2:
			{
				if (!(m_City.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_City.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 3:
			{
				if (!(m_Stra.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Stra.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 4:
			{
				if (!(m_Adrc.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Adrc.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 5:
			{
				if (!(m_Lana.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Lana.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 6:
			{
				if (!(m_Vwt1.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Vwt1.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 7:
			{
				if (!(m_Tel1.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Tel1.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 8:
			{
				if (!(m_Vwt2.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Vwt2.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 9:
			{
				if (!(m_Tel2.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{
					m_Tel2.SetAt(row-1,pomGrid->GetValue(row,col));
					pomGrid->SetValue(row,colm,CString("*"));
				}
				break;
			}
		case 10:
			{
				if (!(m_Vafr.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{

					CString VAFR = pomGrid->GetValue(row,col);// m_Vafr.GetAt(row-1);
					COleDateTime olFrom = OleDateStringToDate(VAFR);
					if (olFrom.GetStatus() == COleDateTime::invalid)
					{
						break;
					}
					else
					{
						VAFR = ogBasicData.COleDateTimeToDBString(olFrom);
						m_Vafr.SetAt(row-1,VAFR);
						pomGrid->SetValue(row,colm,CString("*"));
					}
				}
				break;
			}
		case 11:
			{
				if (!(m_Vato.GetAt(row-1) == pomGrid->GetValue(row,col)))
				{

					CString VATO = pomGrid->GetValue(row,col);// m_Vato.GetAt(row-1);
					COleDateTime olTo = OleDateStringToDate(VATO);
					if (olTo.GetStatus() == COleDateTime::invalid)
					{
						break;
					}
					else
					{
						VATO = ogBasicData.COleDateTimeToDBString(olTo);
						m_Vato.SetAt(row-1,VATO);
						pomGrid->SetValue(row,colm,CString("*"));
					}
				}
				break;
			}
		default:
			{
				break;
			}
		}
	}
}
//---------------------------------------------------------------------------
void CADR_Dlg::InitMemberValue()
{
	m_Zipa.RemoveAll();
	m_City.RemoveAll();
	m_Stra.RemoveAll();
	m_Adrc.RemoveAll();
	m_Lana.RemoveAll();
	m_Vwt1.RemoveAll();
	m_Tel1.RemoveAll();
	m_Vwt2.RemoveAll();
	m_Tel2.RemoveAll();
	m_Vafr.RemoveAll();
	m_Vato.RemoveAll();

	ReadADR_Data();
	
	for(int i = 0; i < im_ADRCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("ADR", i,  olRecord);
		m_Zipa.Add(olRecord.Values[im_ZipaPos]);
		m_City.Add(olRecord.Values[im_CityPos]);
		m_Stra.Add(olRecord.Values[im_StraPos]);
		m_Adrc.Add(olRecord.Values[im_AdrcPos]);
		m_Lana.Add(olRecord.Values[im_LanaPos]);
		m_Vwt1.Add(olRecord.Values[im_Vwt1Pos]);
		m_Tel1.Add(olRecord.Values[im_Tel1Pos]);
		m_Vwt2.Add(olRecord.Values[im_Vwt2Pos]);
		m_Tel2.Add(olRecord.Values[im_Tel2Pos]);
		m_Vafr.Add(olRecord.Values[im_VafrPos]);
		m_Vato.Add(olRecord.Values[im_VatoPos]);
	}
}
//---------------------------------------------------------------------------
void CADR_Dlg::OnDel() 
{
	CRowColArray awRows;
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	int selRows = pomGrid->GetSelectedRows(awRows,false ,true);
	if (selRows == 1)
	{
		CString vafr = m_Vafr.GetAt(awRows.GetAt(0)-1);
		CString urno = ogBCD.GetFieldExt("ADR", "STFU", "VAFR", m_Stfu, vafr, "URNO");

		if (!urno.IsEmpty())
		{
			if (ogBCD.DeleteRecord("ADR","URNO",urno,true))
			{
				pomGrid->SetRowCount(0);
				FillGrid();
			}
		}
	}

}
//---------------------------------------------------------------------------
CTime COleDateTimeToCTime(COleDateTime opTime)
{
	CTime olTime = -1;
	bool blYear = false;
	int ilDay = 1,ilMonth =1, ilYear;
	if(opTime.GetStatus() == COleDateTime::valid)
	{
		if(opTime.GetYear()>=2038 || opTime.GetYear()<1970)
		{
			blYear = true;
			if(opTime.GetYear()>=2038)
			{
				ilYear = 2038;
				ilMonth = 1;
			}
			else
			{
				ilYear = 1970;
				ilMonth = opTime.GetMonth();
			}
		}
		if(blYear && (ilDay = opTime.GetDay())>18)
		{
			if(ilYear ==2038)
				ilDay = 18;
		}
		if(blYear)
		{
			olTime = CTime(ilYear, ilMonth, ilDay, 1, 0, 0);
		}
		else
		{
			olTime = CTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
		}
	}
	return olTime;
}
//---------------------------------------------------------------------------
COleDateTime CTimeToCOleDateTime(CTime opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}
//---------------------------------------------------------------------------