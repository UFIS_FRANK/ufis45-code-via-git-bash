#if !defined(AFX_PARAMETERDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_PARAMETERDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ParameterDlg.h : header file
//
#include <CedaParData.h>
#include <CedaVAlData.h>
#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// ParameterDlg dialog

class ParameterDlg : public CDialog
{
// Construction
public:
	ParameterDlg(PARDATA *popPar, VALDATA *popVal, CWnd* pParent = NULL);   // standard constructor

	CString omPType;
// Dialog Data
	//{{AFX_DATA(ParameterDlg)
	enum { IDD = IDD_PARAMETERDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_NAME;
	CCSEdit	m_APPL;
	CCSEdit	m_PTYP;
	CCSEdit	m_VALU_TIME;
	CCSEdit	m_VALU_DATE;
	CCSEdit	m_VALU_TEXT;
	CCSEdit	m_PAID;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ParameterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ParameterDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	PARDATA *pomPar;
	VALDATA *pomVal;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETERDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
