#ifndef __BASISSCHICHTENTABLEVIEWER_H__
#define __BASISSCHICHTENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaBsdData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct BASISSCHICHTENTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString	 Bsdc; 	// Code
	CString	 Bsdk; 	// Kurzbezeichnung
	CString	 Bsdn; 	// Bezeichnung
	CString	 Bsds; 	// SAP-Code
	CString	 Ctrc; 	// Vertragsart.code
	CString	 Esbg; 	// Frühester Schichtbeginn
	CString	 Lsen; 	// Spätestes Schichtende
	CString	 Sdu1; 	// Reguläre Schichtdauer
	CString	 Sex1; 	// Mögliche Arbeitszeitver-längerung
	CString	 Ssh1; 	// Mögliche Arbeitszeitverkürzung
	CString	 Bkf1; 	// Pausenlage von
	CString	 Bkt1; 	// Pausenlage bis
	CString	 Bkr1; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	CString	 Bkd1; 	// Pausenlänge
	CString	 Bewc; 	// Springerschicht
	//uhi 16.3.01
	CString	 Vafr;	// valid from
	CString  Vato;  // valid to	
	CString	 Rema; 	// Bemerkungen
	CString	 Type; 	// Flag (statisch/dynamisch)

};

/////////////////////////////////////////////////////////////////////////////
// BasisschichtenTableViewer

	  
class BasisschichtenTableViewer : public CViewer
{
// Constructions
public:
    BasisschichtenTableViewer(CCSPtrArray<BSDDATA> *popData);
    ~BasisschichtenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	void PrepareFilter();
	bool IsPassFilter(BSDDATA *prpBasisschichten);
	int CompareBasisschichten(BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten1, BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten2);
    void MakeLines();
	void MakeLine(BSDDATA *prpBasisschichten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations

	CString DBmin2Gui(CString dbmin);
public:
	void DeleteAll();
	void CreateLine(BASISSCHICHTENTABLE_LINEDATA *prpBasisschichten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(BASISSCHICHTENTABLE_LINEDATA *prpLine);
	void ProcessBasisschichtenChange(BSDDATA *prpBasisschichten);
	void ProcessBasisschichtenDelete(BSDDATA *prpBasisschichten);
	bool FindBasisschichten(char *prpBasisschichtenKeya, char *prpBasisschichtenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomBasisschichtenTable;
	CCSPtrArray<BSDDATA> *pomData;

	CMapStringToPtr omCMapForPfc;		
	bool    bmUseAllPfc;

	CMapStringToPtr omCMapForOrg;		
	bool    bmUseAllOrg;

// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<BASISSCHICHTENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(BASISSCHICHTENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__BASISSCHICHTENTABLEVIEWER_H__
