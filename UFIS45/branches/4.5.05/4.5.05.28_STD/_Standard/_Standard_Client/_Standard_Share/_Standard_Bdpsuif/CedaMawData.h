// CedaMawData.h

#ifndef __CEDAPMAWDATA__
#define __CEDAPMAWDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct MAWDATA 
{
	char 	 Dflt[5+2];  // Default-Value
	char     Hopo[3+2];  // Hopo
	char 	 Name[32+2]; // Name
    long 	 Urno;       // Eindeutige Datensatz-Nr.
	//char 	 Year[4+2];  // Jahr
	
	//DataCreated by this class
	int      IsChanged;
	
	MAWDATA(void)
	{
		memset(this,'\0',sizeof(*this));
	}

}; 

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaMawData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<MAWDATA> omData;

	char pcmListOfFields[2048];


public:
    CedaMawData();
	~CedaMawData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(MAWDATA *prpMaw);
	bool InsertInternal(MAWDATA *prpMaw);
	bool Update(MAWDATA *prpMaw);
	bool UpdateInternal(MAWDATA *prpMaw);
	bool Delete(long lpUrno);
	bool DeleteInternal(MAWDATA *prpMaw);
	bool ReadSpecialData(CCSPtrArray<MAWDATA> *prpMaw,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(MAWDATA *prpMaw);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	MAWDATA  *GetMawByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareMawData(MAWDATA *prpMawData);
	CString omWhere; //Prf: 8795
	bool ValidateMawBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPMAWDATA__

