// CedaOacData.h

#ifndef __CEDAPOACDATA__
#define __CEDAPOACDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct OACDATA 
{
	long 	 Urno;	 	// Eindeutige Datensatz-Nr.
	char 	 Ctrc[8+2]; 	// Code
	char 	 Sdac[8+2]; 	// Bezeichnung

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	OACDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end OACDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOacData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<OACDATA> omData;

	char pcmListOfFields[2048];

// OOacations
public:
    CedaOacData();
	~CedaOacData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(OACDATA *prpOac);
	bool InsertInternal(OACDATA *prpOac);
	bool Update(OACDATA *prpOac);
	bool UpdateInternal(OACDATA *prpOac);
	bool Delete(long lpUrno);
	bool DeleteInternal(OACDATA *prpOac);
	bool ReadSpecialData(CCSPtrArray<OACDATA> *popOac,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(OACDATA *prpOac);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	OACDATA  *GetOacByUrno(long lpUrno);


	// Private methods
private:
    void PrepareOacData(OACDATA *prpOacData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPOACDATA__
