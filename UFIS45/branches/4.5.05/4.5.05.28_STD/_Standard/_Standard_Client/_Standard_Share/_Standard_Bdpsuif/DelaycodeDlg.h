#if !defined(AFX_DELAYCODEDLG_H__526E8E33_33ED_11D1_B39E_0000C016B067__INCLUDED_)
#define AFX_DELAYCODEDLG_H__526E8E33_33ED_11D1_B39E_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DelaycodeDlg.h : header file
//

#include <CedaDENData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// DelaycodeDlg dialog

class DelaycodeDlg : public CDialog
{
// Construction
public:
	DelaycodeDlg(DENDATA *popDEN, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DelaycodeDlg)
	enum { IDD = IDD_DELAYCODEDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_DECN;
	CCSEdit	m_DECA;
	CCSEdit	m_DECS;//AM:20100913 - For MultiDelayCode
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_ALC3;
	CCSEdit	m_DENA;
	CComboBox	m_RCOL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DelaycodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DelaycodeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	DENDATA *pomDEN;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DELAYCODEDLG_H__526E8E33_33ED_11D1_B39E_0000C016B067__INCLUDED_)
