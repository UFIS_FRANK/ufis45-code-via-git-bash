// CedaSteData.cpp
 
#include <stdafx.h>
#include <CedaSteData.h>
#include <resource.h>


void ProcessSteCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSteData::CedaSteData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STEDataStruct
	BEGIN_CEDARECINFO(STEDATA,STEDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(STEDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(STEDataRecInfo)/sizeof(STEDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STEDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STE");
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSteData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("CODE");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSteData::Register(void)
{
	ogDdx.Register((void *)this,BC_STE_CHANGE,	CString("STEDATA"), CString("Ste-changed"),	ProcessSteCf);
	ogDdx.Register((void *)this,BC_STE_NEW,		CString("STEDATA"), CString("Ste-new"),		ProcessSteCf);
	ogDdx.Register((void *)this,BC_STE_DELETE,	CString("STEDATA"), CString("Ste-deleted"),	ProcessSteCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSteData::~CedaSteData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSteData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSteData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STEDATA *prlSte = new STEDATA;
		if ((ilRc = GetFirstBufferRecord(prlSte)) == true)
		{
			omData.Add(prlSte);//Update omData
			omUrnoMap.SetAt((void *)prlSte->Urno,prlSte);
		}
		else
		{
			delete prlSte;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSteData::Insert(STEDATA *prpSte)
{
	prpSte->IsChanged = DATA_NEW;
	if(Save(prpSte) == false) return false; //Update Database
	InsertInternal(prpSte);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSteData::InsertInternal(STEDATA *prpSte)
{
	ogDdx.DataChanged((void *)this, STE_NEW,(void *)prpSte ); //Update Viewer
	omData.Add(prpSte);//Update omData
	omUrnoMap.SetAt((void *)prpSte->Urno,prpSte);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSteData::Delete(long lpUrno)
{
	STEDATA *prlSte = GetSteByUrno(lpUrno);
	if (prlSte != NULL)
	{
		prlSte->IsChanged = DATA_DELETED;
		if(Save(prlSte) == false) return false; //Update Database
		DeleteInternal(prlSte);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSteData::DeleteInternal(STEDATA *prpSte)
{
	ogDdx.DataChanged((void *)this,STE_DELETE,(void *)prpSte); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSte->Urno);
	int ilSteCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSteCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSte->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSteData::Update(STEDATA *prpSte)
{
	if (GetSteByUrno(prpSte->Urno) != NULL)
	{
		if (prpSte->IsChanged == DATA_UNCHANGED)
		{
			prpSte->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSte) == false) return false; //Update Database
		UpdateInternal(prpSte);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSteData::UpdateInternal(STEDATA *prpSte)
{
	STEDATA *prlSte = GetSteByUrno(prpSte->Urno);
	if (prlSte != NULL)
	{
		*prlSte = *prpSte; //Update omData
		ogDdx.DataChanged((void *)this,STE_CHANGE,(void *)prlSte); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STEDATA *CedaSteData::GetSteByUrno(long lpUrno)
{
	STEDATA  *prlSte;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSte) == TRUE)
	{
		return prlSte;
	}
	return NULL;
}

//--READSTECIALDATA-------------------------------------------------------------------------------------

bool CedaSteData::ReadStecialData(CCSPtrArray<STEDATA> *popSte,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSte != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STEDATA *prpSte = new STEDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSte,CString(pclFieldList))) == true)
			{
				popSte->Add(prpSte);
			}
			else
			{
				delete prpSte;
			}
		}
		if(popSte->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSteData::Save(STEDATA *prpSte)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSte->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSte->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSte);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSte->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSte->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSte);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSte->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSte->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSteCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSteData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSteData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSteData;
	prlSteData = (struct BcStruct *) vpDataPointer;
	STEDATA *prlSte;
	if(ipDDXType == BC_STE_NEW)
	{
		prlSte = new STEDATA;
		GetRecordFromItemList(prlSte,prlSteData->Fields,prlSteData->Data);
		InsertInternal(prlSte);
	}
	if(ipDDXType == BC_STE_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSteData->Selection);
		prlSte = GetSteByUrno(llUrno);
		if(prlSte != NULL)
		{
			GetRecordFromItemList(prlSte,prlSteData->Fields,prlSteData->Data);
			UpdateInternal(prlSte);
		}
	}
	if(ipDDXType == BC_STE_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSteData->Selection);

		prlSte = GetSteByUrno(llUrno);
		if (prlSte != NULL)
		{
			DeleteInternal(prlSte);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
