// CedaDSCData.h

#ifndef __CEDADSCDATA__
#define __CEDADSCDATA__

#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct DSCDATA 
{
	long 	 Urno;
	char     Hopo[4];	
	char 	 Usec[34];	
	char 	 Useu[34];
	CTime	 Cdat;
	CTime	 Lstu;
	char 	 Devn[34];
	char 	 Vafr[9];
	char  	 Vato[9];

	//DataCreated by this class
	int		 IsChanged;

	DSCDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}
};


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDSCData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;	
    CCSPtrArray<DSCDATA> omData;

// Operations
public:
    CedaDSCData();
	~CedaDSCData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<DSCDATA> *popDSCDataArray,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertDSC(DSCDATA *prpDSC,BOOL bpSendDdx = TRUE);
	bool InsertDSCInternal(DSCDATA *prpDSC);
	bool UpdateDSC(DSCDATA *prpDSC,BOOL bpSendDdx = TRUE);
	bool UpdateDSCInternal(DSCDATA *prpDSC);
	bool DeleteDSC(long lpUrno);
	bool DeleteDSCInternal(DSCDATA *prpDSC);
	DSCDATA  *GetDSCByUrno(long lpUrno);	
	bool SaveDSC(DSCDATA *prpDSC);
	char pcmDSCFieldList[2048];
	void ProcessDSCBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;}
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;}
	
private:
    void PrepareDSCData(DSCDATA *prpDSCData);	
	CString omWhere;
	bool ValidateDSCBcData(const long& lrpUnro);
};


//---------------------------------------------------------------------------------------------------------

#endif //__CedaDSCData__
