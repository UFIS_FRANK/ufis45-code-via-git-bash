// CedaCICData.cpp
 
#include <stdafx.h>
#include <CedaCICData.h>
#include <resource.h>
#include <BasicData.h>

// Local function prototype
static void ProcessCICCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCICData::CedaCICData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for CICDATA
	BEGIN_CEDARECINFO(CICDATA,CICDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Cnam,"CNAM")
		FIELD_CHAR_TRIM	(Edpe,"EDPE")
		FIELD_DATE		(Lstu,"LSTU")
//		FIELD_DATE		(Nafr,"NAFR")
//		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		//*** 15.12.1999 SHA ***
		FIELD_CHAR_TRIM	(Tele,"TEL2")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Rgbl,"RGBL")
//		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Term,"TERM")
		FIELD_CHAR_TRIM	(Hall,"HALL")
		FIELD_CHAR_TRIM	(Cicr,"CICR")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Cbaz,"CBAZ")
		FIELD_CHAR_TRIM	(Cicl,"CICL")

		FIELD_CHAR_TRIM	(Apis,"APIS")
		FIELD_CHAR_TRIM	(Wesc,"WESC")
		FIELD_CHAR_TRIM	(Oubl,"OUBL")
		FIELD_CHAR_TRIM	(Inbl,"INBL")

		FIELD_LONG		(Ibit,"IBIT")
		FIELD_CHAR_TRIM	(Catr,"CATR")
		FIELD_CHAR_TRIM	(Cseq,"CSEQ")

	END_CEDARECINFO //(CICDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(CICDataRecInfo)/sizeof(CICDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CICDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CIC");
//	strcpy(pcmCICFieldList,"CDAT,CNAM,EDPE,LSTU,NAFR,NATO,PRFL,TELE,TEL2,URNO,USEC,USEU,VAFR,VATO,RGBL,RESN,TERM,HALL,CICR,HOME,CBAZ,CICL");
//	strcpy(pcmCICFieldList,"CDAT,CNAM,EDPE,LSTU,PRFL,TELE,TEL2,URNO,USEC,USEU,VAFR,VATO,RGBL,TERM,HALL,CICR,HOME,CBAZ,CICL");
	//strcpy(pcmCICFieldList,"CDAT,CNAM,EDPE,LSTU,PRFL,TELE,TEL2,URNO,USEC,USEU,VAFR,VATO,RGBL,TERM,HALL,CICR,HOME,CBAZ,CICL,APIS,WESC,OUBL,INBL");//WNY UFIS-1087

	strcpy(pcmCICFieldList,"CDAT,CNAM,EDPE,LSTU,PRFL,TELE,TEL2,URNO,USEC,USEU,VAFR,VATO,RGBL,TERM,HALL,CICR,HOME,CBAZ,CICL");
	
	pcmFieldList = pcmCICFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaCICData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");  ropDesription.Add(LoadStg(IDS_STRING343));  ropType.Add("Date");
	ropFields.Add("CNAM");	ropDesription.Add(LoadStg(IDS_STRING476));	ropType.Add("String");
	ropFields.Add("EDPE");  ropDesription.Add(LoadStg(IDS_STRING477));  ropType.Add("String");
	ropFields.Add("LSTU");  ropDesription.Add(LoadStg(IDS_STRING344));  ropType.Add("Date");
	//ropFields.Add("NAFR");  ropDesription.Add(LoadStg(IDS_STRING364));  ropType.Add("Date"); 
	//ropFields.Add("NATO");  ropDesription.Add(LoadStg(IDS_STRING365));  ropType.Add("Date");
	ropFields.Add("PRFL");  ropDesription.Add(LoadStg(IDS_STRING345));  ropType.Add("String");
	//ropFields.Add("RESN");  ropDesription.Add(LoadStg(IDS_STRING366));  ropType.Add("String");
	//
	ropFields.Add("TELE");  ropDesription.Add(LoadStg(IDS_STRING294));  ropType.Add("String");
	ropFields.Add("TEL2");  ropDesription.Add(LoadStg(IDS_STRING296));  ropType.Add("String");
	ropFields.Add("URNO");  ropDesription.Add(LoadStg(IDS_STRING346));  ropType.Add("String");
	ropFields.Add("USEC");  ropDesription.Add(LoadStg(IDS_STRING347));  ropType.Add("String");
	ropFields.Add("USEU");  ropDesription.Add(LoadStg(IDS_STRING348));  ropType.Add("String");
	ropFields.Add("VAFR");  ropDesription.Add(LoadStg(IDS_STRING230));  ropType.Add("Date");
	ropFields.Add("VATO");  ropDesription.Add(LoadStg(IDS_STRING231));  ropType.Add("Date");
	ropFields.Add("RGBL");  ropDesription.Add(LoadStg(IDS_STRING478));  ropType.Add("String");
	ropFields.Add("TERM");  ropDesription.Add(LoadStg(IDS_STRING296));  ropType.Add("String");
	ropFields.Add("HALL");  ropDesription.Add(LoadStg(IDS_STRING1028)); ropType.Add("String");
	ropFields.Add("CICR");  ropDesription.Add(LoadStg(IDS_STRING1026)); ropType.Add("String");
	ropFields.Add("HOME");  ropDesription.Add(LoadStg(IDS_STRING711));  ropType.Add("String");
	ropFields.Add("CBAZ");  ropDesription.Add(LoadStg(IDS_STRING591));  ropType.Add("String");
	ropFields.Add("CICL");  ropDesription.Add(LoadStg(IDS_STRING1027)); ropType.Add("String");
	
	if(bgShowCICEquip)
	{
		ropFields.Add("APIS");  ropDesription.Add(LoadStg(IDS_STRING32836)); ropType.Add("String");
		ropFields.Add("WESC");  ropDesription.Add(LoadStg(IDS_STRING32837)); ropType.Add("String");
		ropFields.Add("OUBL");  ropDesription.Add(LoadStg(IDS_STRING32838)); ropType.Add("String");
		ropFields.Add("INBL");  ropDesription.Add(LoadStg(IDS_STRING32839)); ropType.Add("String");
	}
	
	if (ogBasicData.IsGatPosEnabled())
		ropFields.Add("IBIT");  ropDesription.Add(LoadStg(IDS_STRING909)); ropType.Add("String");

	if (ogBasicData.IsExtendedLMEnabled())
	{
		ropFields.Add("CATR");  ropDesription.Add(LoadStg(IDS_STRING1090)); ropType.Add("String");
		ropFields.Add("CSEQ");  ropDesription.Add(LoadStg(IDS_STRING1091)); ropType.Add("String");
	}
}

//----------------------------------------------------------------------------------------------------

void CedaCICData::Register(void)
{
	ogDdx.Register((void *)this,BC_CIC_CHANGE,CString("CICDATA"), CString("CIC-changed"),ProcessCICCf);
	ogDdx.Register((void *)this,BC_CIC_DELETE,CString("CICDATA"), CString("CIC-deleted"),ProcessCICCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCICData::~CedaCICData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCICData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCICData::Read(char *pcpWhere)
{
	if (ogBasicData.IsGatPosEnabled())
	{
		if( CString(pcmCICFieldList).Find("IBIT") < 0 )
		{
			strcat(pcmCICFieldList,",IBIT");
		}
	}

	if (ogBasicData.IsExtendedLMEnabled())
	{
		if( CString(pcmCICFieldList).Find("CATR,CSEQ") < 0 )
		{
			strcat(pcmCICFieldList,",CATR,CSEQ");
		}
	}

	if(bgShowCICEquip)
	{
		if( CString(pcmCICFieldList).Find("APIS,WESC,OUBL,INBL") < 0 )
		{
			strcat(pcmCICFieldList,",APIS,WESC,OUBL,INBL");
		
		}
	}

	pcmFieldList = pcmCICFieldList;
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CICDATA *prpCIC = new CICDATA;
		if ((ilRc = GetFirstBufferRecord(prpCIC)) == true)
		{
			prpCIC->IsChanged = DATA_UNCHANGED;
			omData.Add(prpCIC);//Update omData
			omUrnoMap.SetAt((void *)prpCIC->Urno,prpCIC);
		}
		else
		{
			delete prpCIC;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCICData::InsertCIC(CICDATA *prpCIC,BOOL bpSendDdx)
{
	prpCIC->IsChanged = DATA_NEW;
	if(SaveCIC(prpCIC) == false) return false; //Update Database
	InsertCICInternal(prpCIC);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaCICData::InsertCICInternal(CICDATA *prpCIC)
{
	//PrepareCICData(prpCIC);
	ogDdx.DataChanged((void *)this, CIC_CHANGE,(void *)prpCIC ); //Update Viewer
	omData.Add(prpCIC);//Update omData
	omUrnoMap.SetAt((void *)prpCIC->Urno,prpCIC);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCICData::DeleteCIC(long lpUrno)
{
	CICDATA *prlCIC = GetCICByUrno(lpUrno);
	if (prlCIC != NULL)
	{
		prlCIC->IsChanged = DATA_DELETED;
		if(SaveCIC(prlCIC) == false) return false; //Update Database
		DeleteCICInternal(prlCIC);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCICData::DeleteCICInternal(CICDATA *prpCIC)
{
	ogDdx.DataChanged((void *)this,CIC_DELETE,(void *)prpCIC); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCIC->Urno);
	int ilCICCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCICCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCIC->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaCICData::PrepareCICData(CICDATA *prpCIC)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCICData::UpdateCIC(CICDATA *prpCIC,BOOL bpSendDdx)
{
	if (GetCICByUrno(prpCIC->Urno) != NULL)
	{
		if (prpCIC->IsChanged == DATA_UNCHANGED)
		{
			prpCIC->IsChanged = DATA_CHANGED;
		}
		if(SaveCIC(prpCIC) == false) return false; //Update Database
		UpdateCICInternal(prpCIC);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCICData::UpdateCICInternal(CICDATA *prpCIC)
{
	CICDATA *prlCIC = GetCICByUrno(prpCIC->Urno);
	if (prlCIC != NULL)
	{
		*prlCIC = *prpCIC; //Update omData
		ogDdx.DataChanged((void *)this,CIC_CHANGE,(void *)prlCIC); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CICDATA *CedaCICData::GetCICByUrno(long lpUrno)
{
	CICDATA  *prlCIC;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCIC) == TRUE)
	{
		return prlCIC;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCICData::ReadSpecialData(CCSPtrArray<CICDATA> *popCic,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","CIC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","CIC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCic != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CICDATA *prpCic = new CICDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCic,CString(pclFieldList))) == true)
			{
				popCic->Add(prpCic);
			}
			else
			{
				delete prpCic;
			}
		}
		if(popCic->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCICData::SaveCIC(CICDATA *prpCIC)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCIC->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpCIC->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCIC);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCIC->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCIC->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCIC);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCIC->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCIC->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCICCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_CIC_CHANGE :
	case BC_CIC_DELETE :
		((CedaCICData *)popInstance)->ProcessCICBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCICData::ProcessCICBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCICData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlCICData->Selection);

	CICDATA *prlCIC = GetCICByUrno(llUrno);
	if(ipDDXType == BC_CIC_CHANGE)
	{
		if (prlCIC != NULL)
		{
			GetRecordFromItemList(prlCIC,prlCICData->Fields,prlCICData->Data);
			if(ValidateCICBcData(prlCIC->Urno)) //Prf: 8795
			{
				UpdateCICInternal(prlCIC);
			}
			else
			{
				DeleteCICInternal(prlCIC);
			}
		}
		else
		{
			prlCIC = new CICDATA;
			GetRecordFromItemList(prlCIC,prlCICData->Fields,prlCICData->Data);
			if(ValidateCICBcData(prlCIC->Urno)) //Prf: 8795
			{
				InsertCICInternal(prlCIC);
			}
			else
			{
				delete prlCIC;
			}
		}
	}
	if(ipDDXType == BC_CIC_DELETE)
	{
		if (prlCIC != NULL)
		{
			DeleteCICInternal(prlCIC);
		}
	}
}

//Prf: 8795
//--ValidateCICBcData--------------------------------------------------------------------------------------

bool CedaCICData::ValidateCICBcData(const long& lrpUrno)
{
	bool blValidateCICBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<CICDATA> olCics;
		if(!ReadSpecialData(&olCics,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateCICBcData = false;
		}
	}
	return blValidateCICBcData;
}


//---------------------------------------------------------------------------------------------------------

bool CedaCICData::CompareCIC(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
/*	bool ilRc = true;
	CString olListOfData = *pcpListOfData;
	CString olFieldList  = *pcpFieldList;
	ilRc = MakeCedaData(*pcpListOfData, *pcpFieldLis, pvpSaveDataStruct, pvpChangedDataStruct);
*/	
	return MakeCedaData(pcpListOfData, pcpFieldList, pvpSaveDataStruct, pvpChangedDataStruct);
}

bool CedaCICData::MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
	return MakeCedaData(&omRecInfo,pcpListOfData, pcpFieldList, pvpSaveDataStruct, pvpChangedDataStruct);
}

//---------------------------------------------------------------------------------------------------------
bool CedaCICData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
    pcpListOfData = "";
	char pclBuf[128];
    char buf[33];   // ltoa() need buffer 33 bytes
    CString sSave;
    CString sChanged;
    CString sOrg;
    CString pclFieldList;
	int ilFieldListPos = 0;
	int ilFieldListLength = 0;
	int ilTmpPos = 0;
	int ilLc = 0;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();

    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;

        void *pSave    = (void *)((char *)pvpSaveDataStruct + (*pomRecInfo)[fieldno].Offset);
        void *pChanged = (void *)((char *)pvpChangedDataStruct + (*pomRecInfo)[fieldno].Offset);

        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) 
		{
        case CEDACHAR:
			{
				sSave = (length == 0)? CString(" "): CString((char *)pSave).Left(length);
				MakeCedaString(sSave);
				sChanged = (length == 0)? CString(" "): CString((char *)pChanged).Left(length);
				MakeCedaString(sChanged);
			}
            break;
        case CEDAINT:
			{
				sSave	 = CString(itoa(*(int *)pSave, buf, 10));
				sChanged = CString(itoa(*(int *)pChanged, buf, 10));
			}
            break;
        case CEDALONG:
			{
				sSave	 = CString(ltoa(*(long *)pSave, buf, 10));
				sChanged = CString(ltoa(*(long *)pChanged, buf, 10));
			}
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((CTime *)pSave)->Format("%Y%m%d%H%M00");
				sChanged = ((CTime *)pChanged)->Format("%Y%m%d%H%M00");
				sOrg = ((CTime *)pChanged)->Format("%Y%m%d%H%M%S");
			}
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((COleDateTime *)pSave)->Format("%Y%m%d%H%M%S");
				sChanged = ((COleDateTime *)pChanged)->Format("%Y%m%d%H%M%S");
			}
            break;
		case CEDADOUBLE:
			{
				sprintf(pclBuf,"%.2f",(double *)pSave);
				sSave = CString(pclBuf);
				sprintf(pclBuf,"%.2f",(double *)pChanged);
				sChanged = CString(pclBuf);
			}
			break;
        default:    // invalid field type
            return false;
        }

		ilTmpPos = ilFieldListPos;
		ilFieldListLength = pcpFieldList.GetLength();

		for( ilFieldListPos++ ; ((ilFieldListPos < ilFieldListLength)  && (pcpFieldList[ilFieldListPos] != ',')); ilFieldListPos++);

		if(sChanged != sSave)
		{
			if(type == CEDADATE)
				pcpListOfData += sOrg + ",";
			else
				pcpListOfData += sChanged + ",";

// TVO 02.02.2000
			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos - 1) )
//			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos ) )
				pclFieldList += pcpFieldList.Mid(ilTmpPos, ilFieldListPos - ilTmpPos);
		}

    }
	if(!pclFieldList.IsEmpty())
	{
		if(pclFieldList[0] == ',')
			pclFieldList.SetAt(0,' ');
		if(pclFieldList[pclFieldList.GetLength()-1] == ',')
			pclFieldList.SetAt(pclFieldList.GetLength()-1,' ');
		pclFieldList.TrimRight();
		pclFieldList.TrimLeft();
	}
	pcpFieldList = pclFieldList;
	if(!pcpListOfData.IsEmpty())
	{
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);
	}
    return true;
}

//---------------------------------------------------------------------------------------------------------
bool CedaCICData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}
