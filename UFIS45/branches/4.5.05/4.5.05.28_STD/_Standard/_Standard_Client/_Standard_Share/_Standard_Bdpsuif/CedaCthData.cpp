// CedaCthData.cpp
 
#include <stdafx.h>
#include <CedaCthData.h>
#include <resource.h>


void ProcessCthCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCthData::CedaCthData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for CTHDATA
	BEGIN_CEDARECINFO(CTHDATA,CthDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Absf,"ABSF")
		FIELD_CHAR_TRIM	(Abst,"ABST")
		FIELD_CHAR_TRIM	(Cthg,"CTHG")
		FIELD_CHAR_TRIM	(Redu,"REDU")
		FIELD_CHAR_TRIM	(Year,"YEAR")
		FIELD_LONG		(Urno,"URNO")

	END_CEDARECINFO //(CTHDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(CthDataRecInfo)/sizeof(CthDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CthDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CTH");
	strcpy(pcmListOfFields,"CDAT,LSTU,USEC,USEU,ABSF,ABST,CTHG,REDU,YEAR,URNO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaCthData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("ABSF");
	ropFields.Add("ABST");
	ropFields.Add("CTHG");
	ropFields.Add("REDU");
	ropFields.Add("YEAR");
	ropFields.Add("URNO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING775));
	ropDesription.Add(LoadStg(IDS_STRING776));
	ropDesription.Add(LoadStg(IDS_STRING774));
	ropDesription.Add(LoadStg(IDS_STRING187));
	ropDesription.Add(LoadStg(IDS_STRING681));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCthData::Register(void)
{
	ogDdx.Register((void *)this,BC_CTH_CHANGE,	CString("CTHDATA"), CString("Cth-changed"),	ProcessCthCf);
	ogDdx.Register((void *)this,BC_CTH_NEW,		CString("CTHDATA"), CString("Cth-new"),		ProcessCthCf);
	ogDdx.Register((void *)this,BC_CTH_DELETE,	CString("CTHDATA"), CString("Cth-deleted"),	ProcessCthCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCthData::~CedaCthData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCthData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCthData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CTHDATA *prlCth = new CTHDATA;
		if ((ilRc = GetFirstBufferRecord(prlCth)) == true)
		{
			omData.Add(prlCth);//Update omData
			omUrnoMap.SetAt((void *)prlCth->Urno,prlCth);
		}
		else
		{
			delete prlCth;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCthData::Insert(CTHDATA *prpCth)
{
	prpCth->IsChanged = DATA_NEW;
	if(Save(prpCth) == false) return false; //Update Database
	InsertInternal(prpCth);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCthData::InsertInternal(CTHDATA *prpCth)
{
	ogDdx.DataChanged((void *)this, CTH_NEW,(void *)prpCth ); //Update Viewer
	omData.Add(prpCth);//Update omData
	omUrnoMap.SetAt((void *)prpCth->Urno,prpCth);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCthData::Delete(long lpUrno)
{
	CTHDATA *prlCth = GetCthByUrno(lpUrno);
	if (prlCth != NULL)
	{
		prlCth->IsChanged = DATA_DELETED;
		if(Save(prlCth) == false) return false; //Update Database
		DeleteInternal(prlCth);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCthData::DeleteInternal(CTHDATA *prpCth)
{
	ogDdx.DataChanged((void *)this,CTH_DELETE,(void *)prpCth); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCth->Urno);
	int ilCthCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCthCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCth->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCthData::Update(CTHDATA *prpCth)
{
	if (GetCthByUrno(prpCth->Urno) != NULL)
	{
		if (prpCth->IsChanged == DATA_UNCHANGED)
		{
			prpCth->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCth) == false) return false; //Update Database
		UpdateInternal(prpCth);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCthData::UpdateInternal(CTHDATA *prpCth)
{
	CTHDATA *prlCth = GetCthByUrno(prpCth->Urno);
	if (prlCth != NULL)
	{
		*prlCth = *prpCth; //Update omData
		ogDdx.DataChanged((void *)this,CTH_CHANGE,(void *)prlCth); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CTHDATA *CedaCthData::GetCthByUrno(long lpUrno)
{
	CTHDATA  *prlCth;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCth) == TRUE)
	{
		return prlCth;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCthData::ReadSpecialData(CCSPtrArray<CTHDATA> *popCth,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","CTH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","CTH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCth != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CTHDATA *prpCth = new CTHDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCth,CString(pclFieldList))) == true)
			{
				popCth->Add(prpCth);
			}
			else
			{
				delete prpCth;
			}
		}
		if(popCth->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCthData::Save(CTHDATA *prpCth)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCth->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCth->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCth);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCth->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCth->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCth);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCth->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCth->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCthCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogCthData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCthData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCthData;
	prlCthData = (struct BcStruct *) vpDataPointer;
	CTHDATA *prlCth;
	if(ipDDXType == BC_CTH_NEW)
	{
		prlCth = new CTHDATA;
		GetRecordFromItemList(prlCth,prlCthData->Fields,prlCthData->Data);
		InsertInternal(prlCth);
	}
	if(ipDDXType == BC_CTH_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlCthData->Selection);
		prlCth = GetCthByUrno(llUrno);
		if(prlCth != NULL)
		{
			GetRecordFromItemList(prlCth,prlCthData->Fields,prlCthData->Data);
			UpdateInternal(prlCth);
		}
	}
	if(ipDDXType == BC_CTH_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlCthData->Selection);

		prlCth = GetCthByUrno(llUrno);
		if (prlCth != NULL)
		{
			DeleteInternal(prlCth);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
