// CedaACRData.h

#ifndef __CEDAACRDATA__
#define __CEDAACRDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct ACRDATA 
{
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Regn[14]; 	// Flugzeug-Kennung
	char 	 Regi[14]; 	// Flugzeug-Kennung mit Bindestrich
	char 	 Act3[5]; 	// Flugzeug-Typ 3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-Typ 5-Letter Code (ICAO)
	char 	 Owne[12]; 	// Flugzeug-Besitzer
	char 	 Selc[7]; 	// SELCAL
	char 	 Mtow[12]; 	// Max. Startgewicht
	char 	 Mowe[12]; 	// Max. Eigengewicht
	char 	 Apui[3]; 	// APU INOP
	char 	 Main[3]; 	// Maindeck-Flugzeug
	char 	 Enna[12]; 	// Triebwerksbezeichnung
	char 	 Annx[4]; 	// L�rm-Annex
	char 	 Nose[5]; 	// Anzahl Sitze
	char 	 Noga[4]; 	// Anzahl K�chen
	char 	 Noto[4]; 	// Anzahl Toiletten
	char 	 Debi[12]; 	// Debitoren Nummer
	CTime	 Ladp;	 	// Letzte Abflugzeit
	char 	 Rema[82]; 	// Bemerkung
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char 	 Enty[7]; 	// Antriebsart
	char 	 Acti[7]; 	// Modifizierter ICAO Code
	char 	 Gall[34]; 	// Anzahl Toiletten
	char 	 Frst[5]; 	// Anzahl Toiletten
	char 	 Rest[5]; 	// Anzahl Toiletten
	char 	 Efis[5]; 	// Anzahl Toiletten
	char 	 Ache[8];
	char 	 Acle[8];
	char 	 Acws[8];
	char 	 Aurn[12];
	char 	 Conf[18];
	char 	 Cont[4];
	char 	 Ctry[12];
	char 	 Cvtd[4];
	char 	 Deli[6];
	char 	 Etxt[4];
	char 	 Exrg[12];
	char 	 Fhrs[4];
	char 	 Fuse[8];
	char 	 Iata[6];
	char 	 Icao[6];
	char 	 Lcod[6];
	char 	 Lcyc[4];
	char 	 Lsdb[4];
	char 	 Lsdf[24];
	char 	 Lsfb[4];
	char 	 Lstb[4];
	char 	 Mode[4];
	char 	 Nois[5];
	char 	 Odat[6];
	char 	 Oope[4];
	char 	 Oopt[4];
	char 	 Oord[4];
	char 	 Opbb[4];
	char 	 Opbf[4];
	char 	 Opwb[4];
	char 	 Powd[24];
	char 	 Regb[4];
	char 	 Scod[12];
	char 	 Seri[18];
	char 	 Strd[4];
	char 	 Text[24];
	char 	 Type[44];
	char 	 Wfub[4];
	char 	 Wobo[4];
	char 	 Year[6];
	char 	 Ufis[2];
	char 	 Cnam[41];
	char 	 Pnum[12];
	char 	 Home[4];
	char 	 Naco[4];
	char 	 Oadr[258];
	char 	 Typu[4];
	char 	 Crw1[4];
	char 	 Crw2[4];
	char	 Ming[5];	//*** 04.10.99 SHA ***
						//*** Minimum Ground Time (REQ. FROM SHANGHAI) ***

	//DataCreated by this class
	int      IsChanged;

	ACRDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Ladp=-1;
		Vafr=-1;
		Vato=-1;
	}


}; // end ACRDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaACRData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CCSPtrArray<ACRDATA> omData;

// Operations
public:
    CedaACRData();
	~CedaACRData();
	void Register(void);
	void ClearAll(void);

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<ACRDATA> *popAcr,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertACR(ACRDATA *prpACR,BOOL bpSendDdx = TRUE);
	bool InsertACRInternal(ACRDATA *prpACR);
	bool UpdateACR(ACRDATA *prpACR,BOOL bpSendDdx = TRUE);
	bool UpdateACRInternal(ACRDATA *prpACR);
	bool DeleteACR(long lpUrno);
	bool DeleteACRInternal(ACRDATA *prpACR);
	ACRDATA  *GetACRByUrno(long lpUrno);
	bool SaveACR(ACRDATA *prpACR);
	char pcmACRFieldList[2048];
	void ProcessACRBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void  CutZeros(ACRDATA *prpACR);
	void  AddZeros(ACRDATA *prpACR);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareACRData(ACRDATA *prpACRData);	
	CString omWhere; //Prf: 8795
	bool ValidateACRBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAACRDATA__
