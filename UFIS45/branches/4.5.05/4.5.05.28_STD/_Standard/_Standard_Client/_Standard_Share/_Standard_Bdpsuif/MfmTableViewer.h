#ifndef __MFMTABLEVIEWER_H__
#define __MFMTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaMfmData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct MFMTABLE_LINEDATA
{
	long 	Urno;
	CString	Year;
	CString Dpt1;
	CString	Fm01;
	CString	Fm02;
	CString	Fm03;
	CString	Fm04;
	CString	Fm05;
	CString	Fm06;
	CString	Fm07;
	CString	Fm08;
	CString	Fm09;
	CString	Fm10;
	CString	Fm11;
	CString	Fm12;
	CString Ctrc;
};

/////////////////////////////////////////////////////////////////////////////
// MfmTableViewer

class MfmTableViewer : public CViewer
{
// Constructions
public:
    MfmTableViewer(CCSPtrArray<MFMDATA> *popData);
    ~MfmTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(MFMDATA *prpMfm);
	int CompareMfm(MFMTABLE_LINEDATA *prpMfm1, MFMTABLE_LINEDATA *prpMfm2);
    void MakeLines();
	void MakeLine(MFMDATA *prpMfm);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(MFMTABLE_LINEDATA *prpMfm);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(MFMTABLE_LINEDATA *prpLine);
	void ProcessMfmChange(MFMDATA *prpMfm);
	void ProcessMfmDelete(MFMDATA *prpMfm);
	bool FindMfm(char *prpMfmKeya, char *prpMfmKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomMfmTable;
	CCSPtrArray<MFMDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<MFMTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(MFMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__MFMTABLEVIEWER_H__
