// PolTableViewer.cpp 
//

#include <stdafx.h>
#include <PolTableViewer.h>
#include <CcsDdx.h>
#include <resource.h> 
#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void PolTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PolTableViewer
//

PolTableViewer::PolTableViewer(CCSPtrArray<POLDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPolTable = NULL;
    ogDdx.Register(this, POL_CHANGE, CString("POLTABLEVIEWER"), CString("Pol Update"),	 PolTableCf);
    ogDdx.Register(this, POL_DELETE, CString("POLTABLEVIEWER"), CString("Pol Delete"),	 PolTableCf);
    ogDdx.Register(this, POL_NEW,	 CString("POLTABLEVIEWER"), CString("Pol new"),		 PolTableCf);
}
//-----------------------------------------------------------------------------------------------

PolTableViewer::~PolTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::Attach(CCSTable *popTable)
{
    pomPolTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::MakeLines()
{
	int ilPolCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPolCount; ilLc++)
	{
		POLDATA *prlPolData = &pomData->GetAt(ilLc);
		MakeLine(prlPolData);
	}
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::MakeLine(POLDATA *prpPol)
{
    //if( !IsPassFilter(prpPol)) return;

    // Update viewer data for this shift record
    POLTABLE_LINEDATA rlPol;

	rlPol.Urno = prpPol->Urno; 
	rlPol.Name = prpPol->Name; 
	rlPol.Dtel = prpPol->Dtel; 
	rlPol.Pool = prpPol->Pool; 

	CreateLine(&rlPol);
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::CreateLine(POLTABLE_LINEDATA *prpPol)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePol(prpPol, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	POLTABLE_LINEDATA rlPol;
	rlPol = *prpPol;
    omLines.NewAt(ilLineno, rlPol);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PolTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 4;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomPolTable->SetShowSelection(TRUE);
	pomPolTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 125;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING843),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 430; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING843),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 400; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING843),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomPolTable->SetHeaderFields(omHeaderDataArray);

	pomPolTable->SetDefaultSeparator();
	pomPolTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Name;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pool;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dtel;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomPolTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPolTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString PolTableViewer::Format(POLTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL PolTableViewer::FindPol(char *pcpPolKeya, char *pcpPolKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpPolKeya) &&
			 (omLines[ilItem].Keyd == pcpPolKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void PolTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    PolTableViewer *polViewer = (PolTableViewer *)popInstance;
    if (ipDDXType == POL_CHANGE || ipDDXType == POL_NEW) polViewer->ProcessPolChange((POLDATA *)vpDataPointer);
    if (ipDDXType == POL_DELETE) polViewer->ProcessPolDelete((POLDATA *)vpDataPointer);
} 

//-----------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------
void PolTableViewer::ProcessPolChange(POLDATA *prpPol)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;


	rlColumn.Text = prpPol->Name;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPol->Pool;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPol->Dtel;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpPol->Urno, ilItem))
	{
        POLTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpPol->Urno;
		prlLine->Name = prpPol->Name;
		prlLine->Pool = prpPol->Pool;
		prlLine->Dtel = prpPol->Dtel;
	
		pomPolTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomPolTable->DisplayTable();
	}
	else
	{
		MakeLine(prpPol);
		if (FindLine(prpPol->Urno, ilItem))
		{
	        POLTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPolTable->AddTextLine(olLine, (void *)prlLine);
				pomPolTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::ProcessPolDelete(POLDATA *prpPol)
{
	int ilItem;
	if (FindLine(prpPol->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPolTable->DeleteTextLine(ilItem);
		pomPolTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL PolTableViewer::IsPassFilter(POLDATA *prpPol)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int PolTableViewer::ComparePol(POLTABLE_LINEDATA *prpPol1, POLTABLE_LINEDATA *prpPol2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpPol1->Tifd == prpPol2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpPol1->Tifd > prpPol2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void PolTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL PolTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void PolTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PolTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING842);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PolTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool PolTableViewer::PrintTableLine(POLTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=3&&i!=4)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Name;
				}
			break;
			case 1:
				{
					rlElement.Text		= prpLine->Pool;
				}
			break;
			case 2:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Dtel;
				}
			break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
