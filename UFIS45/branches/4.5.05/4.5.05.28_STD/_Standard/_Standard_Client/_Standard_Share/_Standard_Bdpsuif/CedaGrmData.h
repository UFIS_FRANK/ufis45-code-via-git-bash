// CedaGrmData.h

#ifndef __CEDAGRMDATA__
#define __CEDAGRMDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct GRMDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	long 	 Gurn; 		// Datensatz-Nr. des Gruppennamens
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Valu[14]; 	// Wert der Gruppierung


	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	GRMDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end GrmDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaGrmData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<GRMDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaGrmData();
	~CedaGrmData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(GRMDATA *prpGrm);
	bool InsertInternal(GRMDATA *prpGrm);
	bool Update(GRMDATA *prpGrm);
	bool UpdateInternal(GRMDATA *prpGrm);
	bool Delete(long lpUrno);
	bool DeleteInternal(GRMDATA *prpGrm);
	bool ReadSpecialData(CCSPtrArray<GRMDATA> *popGrm,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(GRMDATA *prpGrm);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	GRMDATA  *GetGrmByUrno(long lpUrno);

	// Private methods
private:
    void PrepareGrmData(GRMDATA *prpGrmData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAGRMDATA__
