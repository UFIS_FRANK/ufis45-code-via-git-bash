#if !defined(AFX_AIRPORTWET_H__6FB70433_EE67_11D2_AD3D_004095436A98__INCLUDED_)
#define AFX_AIRPORTWET_H__6FB70433_EE67_11D2_AD3D_004095436A98__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AirPortWet.h : header file
//
#include <PrivList.h>
#include <CCSEdit.h>
#include <CCSComboBox.h>
#include <CedaAwiData.h>

/////////////////////////////////////////////////////////////////////////////
// CAirPortWet dialog

class CAirPortWet : public CDialog
{
// Construction
public:
	CAirPortWet(AWIDATA *prpAwi,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAirPortWet)
	enum { IDD = IDD_AIRPORTWETDLG };
	CButton	m_OK;
	CCSEdit	m_Apc3;
	CCSEdit	m_Apc4;
	CCSEdit	m_Humi;
	CCSEdit	m_Msgt;
	CCSEdit	m_Temp;
	CCSEdit	m_Visi;
	CCSEdit	m_Wcod;
	CCSEdit	m_Wdir;
	CCSEdit	m_Wind;
	CCSEdit	m_VaFrD;
	CCSEdit	m_VaFrT;
	CCSEdit	m_VaToD;
	CCSEdit	m_VaToT;
	CString	m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAirPortWet)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAirPortWet)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
		AWIDATA *pomAwi;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRPORTWET_H__6FB70433_EE67_11D2_AD3D_004095436A98__INCLUDED_)
