#if !defined(AFX_ARBEITSVERTRAGSARTENDLG_H__780A8AF3_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
#define AFX_ARBEITSVERTRAGSARTENDLG_H__780A8AF3_64B4_11D1_B3D1_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ArbeitsvertragsartenDlg.h : header file
//
#include <CedaCotData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenDlg dialog

class ArbeitsvertragsartenDlg : public CDialog
{
// Construction
public:
	ArbeitsvertragsartenDlg(COTDATA *popCot, CWnd* pParent = NULL);   // standard constructor

	CStatic	*pomStatus;

// Dialog Data
	//{{AFX_DATA(ArbeitsvertragsartenDlg)
	enum { IDD = IDD_ARBEITSVERTRAGSARTEN };
	CButton	m_Inbu;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC;
	CCSEdit	m_CTRN;
	CCSEdit	m_DPTC;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_WHPW;
	CString	m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ArbeitsvertragsartenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ArbeitsvertragsartenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBOrgAw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	COTDATA *pomCot;
	CString Gui2DBmin(CString guimin);
	CString DBmin2Gui(CString dbmin);
	CString	omOldCode;
	bool	bmChangeAction;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ARBEITSVERTRAGSARTENDLG_H__780A8AF3_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
