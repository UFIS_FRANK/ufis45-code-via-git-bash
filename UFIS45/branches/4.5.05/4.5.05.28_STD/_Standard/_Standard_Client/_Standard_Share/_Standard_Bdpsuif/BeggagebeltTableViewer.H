#ifndef __BEGGAGEBELTTABLEVIEWER_H__
#define __BEGGAGEBELTTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaBLTData.h>
#include <CedaSGMData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct BEGGAGEBELTTABLE_LINEDATA
{
	long	Urno;
	CString	Bnam;
	CString	Term;
	CString	Tele;
	CString	Vafr;
	CString	Vato;
	CString	Bltt;
	CString	Stat;
	CString	Exits;
};

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltTableViewer

class BeggagebeltTableViewer : public CViewer
{
// Constructions
public:
    BeggagebeltTableViewer(CCSPtrArray<BLTDATA> *popData);
    ~BeggagebeltTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(BLTDATA *prpBeggagebelt);
	int CompareBeggagebelt(BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt1, BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt2);
    void MakeLines();
	void MakeLine(BLTDATA *prpBeggagebelt);
// Operations
public:
	BOOL FindLine(long lpUrno, int &rilLineno);
	void DeleteAll();
	void CreateLine(BEGGAGEBELTTABLE_LINEDATA *prpBeggagebelt);
	void DeleteLine(int ipLineno);
	bool CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(BEGGAGEBELTTABLE_LINEDATA *prpLine);
	void ProcessBeggagebeltChange(BLTDATA *prpBeggagebelt);
	void ProcessBeggagebeltDelete(BLTDATA *prpBeggagebelt);
	void ProcessBeggagebeltChange(SGMDATA *prpSgm);
	BOOL FindBeggagebelt(char *prpBeggagebeltKeya, char *prpBeggagebeltKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomBeggagebeltTable;
	CCSPtrArray<BLTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<BEGGAGEBELTTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(BEGGAGEBELTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__BEGGAGEBELTTABLEVIEWER_H__
