﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Input;

namespace Ufis.MVVM.ViewModel
{
    public class MenuViewModel : CommandViewModel
    {
        public class MenuGroup
        {
            /// <summary>
            /// Gets or sets the menu group caption.
            /// </summary>
            public string Caption { get; set; }
            /// <summary>
            /// Gets or sets the image source for menu group.
            /// </summary>
            public ImageSource Glyph { get; set; }
        }

        public MenuViewModel(string displayName, ICommand command)
            : base(displayName, command)
        {
            
        }

        /// Gets or sets the menu group.
        /// </summary>
        public MenuGroup Group { get; set; }
    }
}
