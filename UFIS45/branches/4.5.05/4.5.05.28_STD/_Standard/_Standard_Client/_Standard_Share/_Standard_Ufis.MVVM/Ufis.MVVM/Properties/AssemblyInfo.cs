﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Ufis.MVVM")]
[assembly: AssemblyDescription("Last Changes On - 29.Mar.2012")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UFIS Airport Solutions GmbH")]
[assembly: AssemblyProduct("Ufis.MVVM")]
[assembly: AssemblyCopyright("Copyright ©UFIS Airport Solutions GmbH")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5695c0f7-fa60-4a18-928c-3bb9baaa93a3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.5.0.0")]
[assembly: AssemblyFileVersion("4.5.0.4")]
