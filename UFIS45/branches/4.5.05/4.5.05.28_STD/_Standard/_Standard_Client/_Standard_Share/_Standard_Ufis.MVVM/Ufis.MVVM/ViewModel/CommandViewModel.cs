﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Ufis.MVVM.ViewModel
{
    /// <summary>
    /// Represents an actionable item displayed by a View.
    /// </summary>
    public class CommandViewModel : ViewModelBase
    {
        public CommandViewModel(string displayName, ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            base.DisplayName = displayName;
            this.Command = command;
        }

        /// <summary>
        /// Gets the command for this view model.
        /// </summary>
        public ICommand Command { get; private set; }
    }
}