﻿using System;
using System.Globalization;

namespace Ufis.Entities
{
    public abstract class BaseEntity : NoIdBaseEntity, IIdentityEntity
    {

        // <ADD>
        // <2012-05-29>
        public static readonly string UI_DATETIME_FORMAT = "dd.MM.yyyy HH:mm";
        public static readonly string DB_DATETIME_FORMAT = "yyyyMMddHHmmss";
        public static readonly string UI_TIME_FORMAT = "HH:mm";
        public static readonly string DB_TIME_FORMAT = "HHmm";
        public static IFormatProvider cultureInfo = new CultureInfo("en-US");
        // </2012-05-29>
        // </ADD>

        private int _urno;

        [EntityAttribute(SerializedName = "URNO", IsPrimaryKey = true, IsReadOnly = true)]
        public int Urno 
        {
            get { return _urno; }
            set
            {
                if (_urno != value)
                {
                    _urno = value;
                    OnPropertyChanged("Urno");
                }
            }
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;

            BaseEntity entity = (BaseEntity)obj;
            return (Urno == entity.Urno);
        }

        public override int GetHashCode()
        {
            return Urno.GetHashCode();
        }
    }
}
