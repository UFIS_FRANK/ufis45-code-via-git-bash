﻿using System;

namespace Ufis.Entities
{
    public interface IBaseEntity
    {
        string HomeAirport { get; set; }
    }
}
