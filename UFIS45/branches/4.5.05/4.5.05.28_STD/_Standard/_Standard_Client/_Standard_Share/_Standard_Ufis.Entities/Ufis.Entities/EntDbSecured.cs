﻿using System;

namespace Ufis.Entities
{
    [EntityAttribute(SerializedName = "SECTAB")]
    public class EntDbSecured : BaseEntity
    {
        //private string _languange;
        private string _name;
        private string _password;
        private string _remark;
        private string _status;
        private string _type;
        private string _userId;

        //[Entity(SerializedName = "LANG", MaxLength = 4)]
        //public string Languange
        //{
        //    get { return _languange; }
        //    set
        //    {
        //        if (_languange != value)
        //        {
        //            _languange = value;
        //            OnPropertyChanged("Languange");
        //        }
        //    }
        //}

        [Entity(SerializedName = "NAME", MaxLength = 32)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "PASS", MaxLength = 32)]
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        [Entity(SerializedName = "REMA", MaxLength = 80)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        [Entity(SerializedName = "STAT", MaxLength = 1)]
        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        [Entity(SerializedName = "TYPE", MaxLength = 1)]
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("Type");
                }
            }
        }

        [Entity(SerializedName = "USID", MaxLength = 32)]
        public string UserId
        {
            get { return _userId; }
            set
            {
                if (_userId != value)
                {
                    _userId = value;
                    OnPropertyChanged("UserId");
                }
            }
        }
    }
}
