﻿using System;

namespace Ufis.Entities
{    
    public abstract class NoIdBaseEntity : Entity, IBaseEntity, ILogHistory
    {
        private string _homeAirport;
        private string _createdBy;
        private DateTime? _creationDate;
        private string _modifiedBy;
        private DateTime? _modificationDate;

        [EntityAttribute(SerializedName = "HOPO", IsReadOnly = true)]
        public string HomeAirport
        {
            get { return _homeAirport; }
            set
            {
                if (_homeAirport != value)
                {
                    _homeAirport = value;
                    OnPropertyChanged("HomeAirport");
                }
            }
        }

        [EntityAttribute(SerializedName = "USEC", IsReadOnly = true)]
        public string CreatedBy
        {
            get { return _createdBy; }
            set
            {
                if (_createdBy != value)
                {
                    _createdBy = value;
                    OnPropertyChanged("CreatedBy");
                }
            }
        }

        [EntityAttribute(SerializedName = "CDAT", SourceDataType = typeof(String), IsReadOnly = true)]
        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            {
                if (_creationDate != value)
                {
                    _creationDate = value;
                    OnPropertyChanged("CreationDate");
                }
            }
        }

        [EntityAttribute(SerializedName = "USEU", IsReadOnly = true)]
        public string ModifiedBy
        {
            get { return _modifiedBy; }
            set
            {
                if (_modifiedBy != value)
                {
                    _modifiedBy = value;
                    OnPropertyChanged("ModifiedBy");
                }
            }
        }

        [EntityAttribute(SerializedName = "LSTU", SourceDataType = typeof(String), IsReadOnly = true)]
        public DateTime? ModificationDate
        {
            get { return _modificationDate; }
            set
            {
                if (_modificationDate != value)
                {
                    _modificationDate = value;
                    OnPropertyChanged("ModificationDate");
                }
            }
        }

    }
}
