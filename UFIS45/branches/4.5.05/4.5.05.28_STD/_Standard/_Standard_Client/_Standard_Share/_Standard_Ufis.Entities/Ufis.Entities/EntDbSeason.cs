﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "SEATAB")]
    public class EntDbSeason : BaseEntity
    {
        private string _remark;
        //private string _loggingCode; 
        private string _name; 
        private DateTime? _validFrom;
        private DateTime? _validTo;

        [Entity(SerializedName = "BEME", MaxLength = 20)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        //[Entity(SerializedName = "PRFL", MaxLength = 1)]
        //public string LoggingCode
        //{
        //    get { return _loggingCode; }
        //    set
        //    {
        //        if (_loggingCode != value)
        //        {
        //            _loggingCode = value;
        //            OnPropertyChanged("LoggingCode");
        //        }
        //    }
        //}

        [Entity(SerializedName = "SEAS", MaxLength = 6)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "VPFR", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ValidFrom
        {
            get { return _validFrom; }
            set
            {
                if (_validFrom != value)
                {
                    _validFrom = value;
                    OnPropertyChanged("ValidFrom");
                }
            }
        }

        [Entity(SerializedName = "VPTO", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ValidTo
        {
            get { return _validTo; }
            set
            {
                if (_validTo != value)
                {
                    _validTo = value;
                    OnPropertyChanged("ValidTo");
                }
            }
        }
    }
}
