﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "ENTTAB")]
    public class EntDbEngineType : BaseEntity
    {
        private string _code;
        private string _name;

        [Entity(SerializedName = "ENTC", MaxLength = 5, IsMandatory = true, IsUnique = true)]
        public string Code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "ENAM", MaxLength = 32)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
    }
}
