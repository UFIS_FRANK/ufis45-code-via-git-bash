﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "APTTAB")]
    public class EntDbAirport : ValidityBaseEntity
    {
        private string _IATACode;
        private string _ICAOCode;
        private string _name;
        private string _shortName;
        private string _shortName2;
        private string _shortName3;
        private string _shortName4;
        private int? _standardFlightTime;
        private string _countryCode;
        private string _type;
        private int? _timeDifferenceBeforeChanges;
        private int? _timeDifferenceAfterChanges;
        private DateTime? _timeChange;
        private int? _timeDifferenceInSummer;
        private int? _timeDifferenceInWinter;
        private string _continentCode;

        [Entity(SerializedName="APC3", MaxLength = 3)]
        public string IATACode
        {
            get { return _IATACode; }
            set
            {
                if (_IATACode != value)
                {
                    _IATACode = value;
                    OnPropertyChanged("IATACode");
                }
            }
        }
        
        [Entity(SerializedName="APC4", MaxLength = 4, IsMandatory = true, IsUnique = true)]
        public string ICAOCode
        {
            get { return _ICAOCode; }
            set
            {
                if (_ICAOCode != value)
                {
                    _ICAOCode = value;
                    OnPropertyChanged("ICAOCode");
                }
            }
        }

        [Entity(SerializedName = "APFN", MaxLength = 32)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "APSN", MaxLength = 20)]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                if (_shortName != value)
                {
                    _shortName = value;
                    OnPropertyChanged("ShortName");
                }
            }
        }

        [Entity(SerializedName = "APN2", MaxLength = 20)]
        public string ShortName2
        {
            get { return _shortName2; }
            set
            {
                if (_shortName2 != value)
                {
                    _shortName2 = value;
                    OnPropertyChanged("ShortName2");
                }
            }
        }

        [Entity(SerializedName = "APN3", MaxLength = 20)]
        public string ShortName3
        {
            get { return _shortName3; }
            set
            {
                if (_shortName3 != value)
                {
                    _shortName3 = value;
                    OnPropertyChanged("ShortName3");
                }
            }
        }

        [Entity(SerializedName = "APN4", MaxLength = 20)]
        public string ShortName4
        {
            get { return _shortName4; }
            set
            {
                if (_shortName4 != value)
                {
                    _shortName4 = value;
                    OnPropertyChanged("ShortName4");
                }
            }
        }

        [Entity(SerializedName = "ETOF", SourceDataType=typeof(String), MaxLength = 4)]
        public int? StandardFlightTime
        {
            get { return _standardFlightTime; }
            set
            {
                if (_standardFlightTime != value)
                {
                    _standardFlightTime = value;
                    OnPropertyChanged("StandardFlightTime");
                }
            }
        }

        [Entity(SerializedName = "LAND", MaxLength = 2, IsMandatory = true)]
        public string CountryCode
        {
            get { return _countryCode; }
            set
            {
                if (_countryCode != value)
                {
                    _countryCode = value;
                    OnPropertyChanged("CountryCode");
                }
            }
        }

        [Entity(SerializedName = "APTT", MaxLength = 1)]
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("Type");
                }
            }
        }

        [Entity(SerializedName = "TDI1", SourceDataType = typeof(String), MaxLength = 3)]
        public int? TimeDifferenceBeforeChanges
        {
            get { return _timeDifferenceBeforeChanges; }
            set
            {
                if (_timeDifferenceBeforeChanges != value)
                {
                    _timeDifferenceBeforeChanges = value;
                    OnPropertyChanged("TimeDifferenceBeforeChanges");
                }
            }
        }

        [Entity(SerializedName = "TDI2", SourceDataType = typeof(String), MaxLength = 3)]
        public int? TimeDifferenceAfterChanges
        {
            get { return _timeDifferenceAfterChanges; }
            set
            {
                if (_timeDifferenceAfterChanges != value)
                {
                    _timeDifferenceAfterChanges = value;
                    OnPropertyChanged("TimeDifferenceAfterChanges");
                }
            }
        }

        [Entity(SerializedName = "TICH", SourceDataType = typeof(String))]
        public DateTime? TimeChange
        {
            get { return _timeChange; }
            set
            {
                if (_timeChange != value)
                {
                    _timeChange = value;
                    OnPropertyChanged("TimeChange");
                }
            }
        }

        [Entity(SerializedName = "TDIS", SourceDataType = typeof(String), MaxLength = 3)]
        public int? TimeDifferenceInSummer
        {
            get { return _timeDifferenceInSummer; }
            set
            {
                if (_timeDifferenceInSummer != value)
                {
                    _timeDifferenceInSummer = value;
                    OnPropertyChanged("TimeDifferenceInSummer");
                }
            }
        }

        [Entity(SerializedName = "TDIW", SourceDataType = typeof(String), MaxLength = 3)]
        public int? TimeDifferenceInWinter
        {
            get { return _timeDifferenceInWinter; }
            set
            {
                if (_timeDifferenceInWinter != value)
                {
                    _timeDifferenceInWinter = value;
                    OnPropertyChanged("TimeDifferenceInWinter");
                }
            }
        }

        [Entity(SerializedName = "CONT", MaxLength = 3)]
        public string ContinentCode
        {
            get { return _continentCode; }
            set
            {
                if (_continentCode != value)
                {
                    _continentCode = value;
                    OnPropertyChanged("ContinentCode");
                }
            }
        }

        //[Field(SerializedName = "PRFL")]
        //public string Protocol { get; set; }

        //[Field(SerializedName = HOME")]
        //public string Home { get; set; }
    }
}
