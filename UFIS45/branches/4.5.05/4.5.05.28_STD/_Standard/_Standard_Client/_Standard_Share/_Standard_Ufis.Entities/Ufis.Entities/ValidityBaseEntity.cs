﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Entities
{
    public abstract class ValidityBaseEntity : BaseEntity, IValidityEntity
    {
        private DateTime? _validFrom;
        private DateTime? _validTo;

        [EntityAttribute(SerializedName = "VAFR", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ValidFrom
        {
            get { return _validFrom; }
            set
            {
                if (_validFrom != value)
                {
                    _validFrom = value;
                    OnPropertyChanged("ValidFrom");
                }
            }
        }

        [EntityAttribute(SerializedName = "VATO", SourceDataType = typeof(String))]
        public DateTime? ValidTo
        {
            get { return _validTo; }
            set
            {
                if (_validTo != value)
                {
                    _validTo = value;
                    OnPropertyChanged("ValidTo");
                }
            }
        }
    }
}
