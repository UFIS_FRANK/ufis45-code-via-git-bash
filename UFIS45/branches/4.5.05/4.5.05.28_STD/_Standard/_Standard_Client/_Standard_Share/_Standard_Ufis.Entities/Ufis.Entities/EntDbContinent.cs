﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Entities
{
    [Entity(SerializedName = "CNTTAB")] 
    public class EntDbContinent : BaseEntity
    {
        private string _code;
        private string _name;

        [Entity(SerializedName = "CODE", MaxLength = 2, IsMandatory = true, IsUnique = true)]
        public string Code 
        { 
            get { return _code; }         
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "DESP", MaxLength = 20)]
        public string Name 
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        } 
    }
}
