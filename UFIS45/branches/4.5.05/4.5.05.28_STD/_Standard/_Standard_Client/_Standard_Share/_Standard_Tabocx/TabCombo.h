#if !defined(AFX_TABCOMBO_H__A464C333_9944_11D6_8090_00D0B7E2A467__INCLUDED_)
#define AFX_TABCOMBO_H__A464C333_9944_11D6_8090_00D0B7E2A467__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabCombo.h : header file
//
#include "CCSPtrArray.h"
/////////////////////////////////////////////////////////////////////////////
// CTabCombo window
//===================================================
// Describes the combobox attributes, data and styles
//
#define USR_SELCHANGED	(WM_USER+5001)

class COMBO_LINE_DEF : public CObject
{
public:
	COMBO_LINE_DEF()
	{
		TextColor = COLORREF(0);
		BackColor = COLORREF(RGB(255,255,255));
	}
	long		 TextColor;
	long		 BackColor;
	CStringArray Values;
};
class CTabCombo;
class COMBO_OBJECT_PROP : public CObject
{
public:
	CString			ObjID;
	CTabCombo		*pCombo;
	long			Columns;
	CString			Style;
	long			ListBoxWidth;
	int				GridPixelWidth;
	long			currLine;
	long			currColumn;
	long			GridColor;
	long			ResultColumn;
	CUIntArray		omColumnLen;
	bool			bmActive;
	CCSPtrArray<COMBO_LINE_DEF>			omLines;
	bool			bmVisible;
};

class CTabCombo : public CComboBox
{
// Construction
public:
	CTabCombo();
	CTabCombo(COMBO_OBJECT_PROP *popProperties, CWnd* popParent);
// Attributes
public:
	COMBO_OBJECT_PROP *pomProperties;
	CWnd *pomParent;
// Operations
public:

	int imCurrentTabLine;
	int imLineHeight;
	BOOL OwnerDraw;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabCombo)
	public:
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTabCombo();

	// Generated message map functions
protected:
	//{{AFX_MSG(CTabCombo)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSelchange();
	afx_msg void OnSelendok();
	afx_msg void OnKillfocus();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_MSG
//	virtual void DrawItem( LPDRAWITEMSTRUCT lpDrawItemStruct );
	void CTabCombo::DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
		BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABCOMBO_H__A464C333_9944_11D6_8090_00D0B7E2A467__INCLUDED_)
