﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Ufis.MVVM.ViewModel;

namespace Ufis.Resources
{

    /// <summary>
    /// Converts multi values to an array of object.
    /// </summary>
    public class MultiValuesToArrayConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        public object Convert(object[] values, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return values.ToArray();
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return ((object[])value).ToArray();
        }

        #endregion
    }
}
