﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Ufis.Resources
{
    public class SharedImages
    {
        public static ComponentResourceKey UFIS48
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "UFIS48"); }
        }
        public static ComponentResourceKey AppLogo
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "AppLogo"); }
        }

        public static ComponentResourceKey Add16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Add16"); }
        }
        public static ComponentResourceKey Add32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Add32"); }
        }
        public static ComponentResourceKey Administrator16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Administrator16"); }
        }
        public static ComponentResourceKey Administrator32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Administrator32"); }
        }
        public static ComponentResourceKey Alarm16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Alarm16"); }
        }
        public static ComponentResourceKey Alarm32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Alarm32"); }
        }
        public static ComponentResourceKey Application16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Application16"); }
        }
        public static ComponentResourceKey Application32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Application32"); }
        }
        public static ComponentResourceKey AutoFilterRow16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "AutoFilterRow16"); }
        }
        public static ComponentResourceKey AutoFilterRow32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "AutoFilterRow32"); }
        }
        public static ComponentResourceKey Back16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Back16"); }
        }
        public static ComponentResourceKey Back32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Back32"); }
        }
        public static ComponentResourceKey Bag16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Bag16"); }
        }
        public static ComponentResourceKey Bag32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Bag32"); }
        }
        public static ComponentResourceKey Box16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Box16"); }
        }
        public static ComponentResourceKey Box32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Box32"); }
        }
        public static ComponentResourceKey Clock16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Clock16"); }
        }
        public static ComponentResourceKey Clock32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Clock32"); }
        }
        public static ComponentResourceKey Close16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Close16"); }
        }
        public static ComponentResourceKey Close32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Close32"); }
        }
        public static ComponentResourceKey Comment16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Comment16"); }
        }
        public static ComponentResourceKey Comment32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Comment32"); }
        }
        public static ComponentResourceKey Conveyor16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Conveyor16"); }
        }
        public static ComponentResourceKey Copy16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Copy16"); }
        }
        public static ComponentResourceKey Copy32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Copy32"); }
        }
        public static ComponentResourceKey Delete16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Delete16"); }
        }
        public static ComponentResourceKey Delete32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Delete32"); }
        }
        public static ComponentResourceKey Document16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Document16"); }
        }
        public static ComponentResourceKey Document32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Document32"); }
        }
        public static ComponentResourceKey Edit16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Edit16"); }
        }
        public static ComponentResourceKey Edit32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Edit32"); }
        }
        public static ComponentResourceKey Error16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Error16"); }
        }
        public static ComponentResourceKey Error32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Error32"); }
        }
        public static ComponentResourceKey Filter16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Filter16"); }
        }
        public static ComponentResourceKey Filter32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Filter32"); }
        }
        public static ComponentResourceKey Find16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Find16"); }
        }
        public static ComponentResourceKey Find32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Find32"); }
        }
        public static ComponentResourceKey FlightArrival16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightArrival16"); }
        }
        public static ComponentResourceKey FlightArrival32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightArrival32"); }
        }
        public static ComponentResourceKey FlightConnection16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightConnection16"); }
        }
        public static ComponentResourceKey FlightConnection32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightConnection32"); }
        }
        public static ComponentResourceKey FlightDeparture16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightDeparture16"); }
        }
        public static ComponentResourceKey FlightDeparture32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightDeparture32"); }
        }
        public static ComponentResourceKey FlightInfo16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightInfo16"); }
        }
        public static ComponentResourceKey FlightInfo32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightInfo32"); }
        }
        public static ComponentResourceKey FlightMonitor16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightMonitor16"); }
        }
        public static ComponentResourceKey FlightMonitor32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightMonitor32"); }
        }
        public static ComponentResourceKey FlightRotation16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightRotation16"); }
        }
        public static ComponentResourceKey FlightRotation32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "FlightRotation32"); }
        }
        public static ComponentResourceKey Gantt16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Gantt16"); }
        }
        public static ComponentResourceKey Gantt32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Gantt32"); }
        }
        public static ComponentResourceKey Gear16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Gear16"); }
        }
        public static ComponentResourceKey Gear32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Gear32"); }
        }
        public static ComponentResourceKey Grid16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Grid16"); }
        }
        public static ComponentResourceKey Grid32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Grid32"); }
        }
        public static ComponentResourceKey Help16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Help16"); }
        }
        public static ComponentResourceKey Help32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Help32"); }
        }
        public static ComponentResourceKey Info16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Info16"); }
        }
        public static ComponentResourceKey Info32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Info32"); }
        }
        public static ComponentResourceKey Keys16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Keys16"); }
        }
        public static ComponentResourceKey Keys32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Keys32"); }
        }
        public static ComponentResourceKey Mail16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Mail16"); }
        }
        public static ComponentResourceKey Mail32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Mail32"); }
        }
        public static ComponentResourceKey Map16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Map16"); }
        }
        public static ComponentResourceKey Map32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Map32"); }
        }
        public static ComponentResourceKey Note16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Note16"); }
        }
        public static ComponentResourceKey Note32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Note32"); }
        }
        public static ComponentResourceKey Next16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Next16"); }
        }
        public static ComponentResourceKey Next32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Next32"); }
        }
        public static ComponentResourceKey OK16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "OK16"); }
        }
        public static ComponentResourceKey OK32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "OK32"); }
        }
        public static ComponentResourceKey Person16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Person16"); }
        }
        public static ComponentResourceKey Person32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Person32"); }
        }
        public static ComponentResourceKey PersonConnect16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonConnect16"); }
        }
        public static ComponentResourceKey PersonConnect32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonConnect32"); }
        }
        public static ComponentResourceKey PersonGroup16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonGroup16"); }
        }
        public static ComponentResourceKey PersonGroup32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonGroup32"); }
        }
        public static ComponentResourceKey PersonProfile16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonProfile16"); }
        }
        public static ComponentResourceKey PersonProfile32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "PersonProfile32"); }
        }
        public static ComponentResourceKey Preview16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Preview16"); }
        }
        public static ComponentResourceKey Preview32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Preview32"); }
        }
        public static ComponentResourceKey Print16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Print16"); }
        }
        public static ComponentResourceKey Print32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Print32"); }
        }
        public static ComponentResourceKey RegisterModule16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "RegisterModule16"); }
        }
        public static ComponentResourceKey RegisterModule32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "RegisterModule32"); }
        }
        public static ComponentResourceKey Save16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Save16"); }
        }
        public static ComponentResourceKey Save32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Save32"); }
        }
        public static ComponentResourceKey SaveAs16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "SaveAs16"); }
        }
        public static ComponentResourceKey SaveAs32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "SaveAs32"); }
        }
        public static ComponentResourceKey Task16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Task16"); }
        }
        public static ComponentResourceKey Task32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Task32"); }
        }
        public static ComponentResourceKey TowTruck16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "TowTruck16"); }
        }
        public static ComponentResourceKey View16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "View16"); }
        }
        public static ComponentResourceKey View32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "View32"); }
        }
        public static ComponentResourceKey ViewAdd16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewAdd16"); }
        }
        public static ComponentResourceKey ViewAdd32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewAdd32"); }
        }
        public static ComponentResourceKey ViewDelete16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewDelete16"); }
        }
        public static ComponentResourceKey ViewDelete32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewDelete32"); }
        }
        public static ComponentResourceKey ViewLocked16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewLocked16"); }
        }
        public static ComponentResourceKey ViewLocked32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewLocked32"); }
        }
        public static ComponentResourceKey ViewSave16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewSave16"); }
        }
        public static ComponentResourceKey ViewSave32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewSave32"); }
        }
        public static ComponentResourceKey ViewSaveAs16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewSaveAs16"); }
        }
        public static ComponentResourceKey ViewSaveAs32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "ViewSaveAs32"); }
        }
        public static ComponentResourceKey Warning16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Warning16"); }
        }
        public static ComponentResourceKey Warning32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Warning32"); }
        }
        public static ComponentResourceKey Wheelchair16
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Wheelchair16"); }
        }
        public static ComponentResourceKey Wheelchair32
        {
            get { return new ComponentResourceKey(typeof(SharedImages), "Wheelchair32"); }
        }
    }
}
