// AddIntoGrid.cpp : implementation file
//

#include <stdafx.h>

#include <CCSglobl.h>
#include <CoCo3.h>
#include <CoCo3dlg.h>
#include <InputGrid.h>
#include <AddIntoGrid.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddIntoGrid dialog

CAddIntoGrid::CAddIntoGrid(CWnd* pParent /*=NULL*/)
	: CDialog(CAddIntoGrid::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddIntoGrid)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomGrid = 0;
}

CAddIntoGrid::~CAddIntoGrid()
{
	if ( pomGrid )
		delete pomGrid ;
}

void CAddIntoGrid::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddIntoGrid)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddIntoGrid, CDialog)
	//{{AFX_MSG_MAP(CAddIntoGrid)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SAVETODB, OnSavetodb)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddIntoGrid message handlers

void CAddIntoGrid::OnOK() 
{
	// TODO: Add extra validation here
	pomGrid->LockUpdate (TRUE );
	if ( StoreEntries () )	
		CDialog::OnOK();
	pomGrid->LockUpdate (FALSE );
	pomGrid->Redraw ();
}

BOOL CAddIntoGrid::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	pomGrid	= new CInputGrid ( this );
	pomGrid->SubclassDlgItem ( IDC_ADD_GRID, this );
	pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	
	pomGrid->SetRowCount ( 100 );
	pomGrid->SetColCount(INPUTCOLS);

	pomGrid->SetValueRange(CGXRange(0, 0), "Nr");	
	pomGrid->SetValueRange(CGXRange(0, 1), "App");		
	pomGrid->SetValueRange(CGXRange(0, 2), "CoCo");		
	pomGrid->SetValueRange(CGXRange(0, 3), "Status");		
	pomGrid->SetValueRange(CGXRange(0, 4), "String");		
	pomGrid->SetValueRange(CGXRange(0, 5), "TextID");		
	pomGrid->SetValueRange(CGXRange(0, 6), "StringID");	
	pomGrid->GetParam()->EnableUndo(TRUE);
	
	SetColWidths ();

	pomFields[0] = "APPL";
	pomFields[1] = "COCO";
	pomFields[2] = "STAT";
	pomFields[3] = "STRG";
	pomFields[4] = "TXID";
	pomFields[5] = "STID";

	for ( int i=0; i<INPUTCOLS; i++ )
	{
		pimFieldLengths[i] = ogBCD.GetMaxFieldLength( "TXT", pomFields[i] );
		pimFieldIdx[i] = ogBCD.GetFieldIndex( "TXT", pomFields[i] );
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAddIntoGrid::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect olRect;
	if ( pomGrid && pomGrid->m_hWnd )
	{
		GetClientRect(&olRect);
		olRect.bottom -=50;
		if ( !olRect.IsRectEmpty () )
		{
			pomGrid->MoveWindow(&olRect); 
			SetColWidths ();
		}
	}
	
}

void CAddIntoGrid::OnSavetodb() 
{
	// TODO: Add your control notification handler code here
	pomGrid->LockUpdate (TRUE );
	bool blOk = StoreEntries ();			
	pomGrid->LockUpdate (FALSE );
	pomGrid->Redraw ();
}

bool CAddIntoGrid::StoreEntries ()
{
	bool		blOk, blRet = true;
	CString		polEntries[INPUTCOLS], olUrno;

	ROWCOL i, ilLength, j, ilErr;
	ilLength = pomGrid->GetRowCount();

	pomGrid->GetParam()->EnableUndo(FALSE);

	for ( i=ilLength; i>=1; i-- )
	{
		for ( j=1; j<=INPUTCOLS; j++ )
		{	
			polEntries[j-1] = pomGrid->GetValueRowCol ( i, j );
		}
		if ( polEntries[0].IsEmpty() && polEntries[1].IsEmpty() && 
			 polEntries[2].IsEmpty() && polEntries[3].IsEmpty() && 
			 polEntries[4].IsEmpty() && polEntries[5].IsEmpty() )
		{	//  komplette Zeile leer
			pomGrid->DeleteRow(i);
			continue;
		}
		ilErr = 0;
		blOk = true;
		Grid2Internal( polEntries[3] );
		olUrno.Empty();

		for ( j=0; blOk&&(j<INPUTCOLS); j++ )		
			blOk &= ( polEntries[j].GetLength() <= pimFieldLengths[j] );
		if ( blOk )
		{
			if ( !theApp.UseExistingDuplicateString ( this, polEntries[3], 
											polEntries[1], polEntries[0]) )
				ilErr = theApp.AddTxtRecord( polEntries[0], polEntries[1], 
											 polEntries[2], polEntries[3], 
											 polEntries[4], polEntries[5], olUrno );
		}
		else
			ilErr |= 32;
		if ( ilErr )
		{
			CString olErrMsg = GetInsertTXTErrorMsg ( ilErr );
			if ( !olErrMsg.IsEmpty () )
				((CGXGridWnd*)pomGrid)->SetStyleRange( CGXRange().SetRows( i, i), 
								CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olErrMsg) );
			blRet = false;
		}
		else
			pomGrid->DeleteRow(i);
	}
	pomGrid->SetRowCount (100);
	pomGrid->GetParam()->EnableUndo(TRUE);
	CWnd *polParent = GetParent();
	if ( polParent )
		((CCoCo3Dlg*)polParent)->UpdateDisplay();
	return blRet;
}

void CAddIntoGrid::SetColWidths ()
{
	CRect olRect = pomGrid->GetGridRect();
	int ilAll = olRect.Width() - pomGrid->GetColWidth( 0 );
	
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->SetColWidth ( 1, 1, (int)((float)ilAll*0.08) );
	pomGrid->SetColWidth ( 2, 2, (int)((float)ilAll*0.04) );
	pomGrid->SetColWidth ( 3, 3, (int)((float)ilAll*0.1) );
	pomGrid->SetColWidth ( 4, 4, (int)((float)ilAll*0.50) );
	pomGrid->SetColWidth ( 5, 5, (int)((float)ilAll*0.21) );
	pomGrid->SetColWidth ( 6, 6, (int)((float)ilAll*0.07) );
	pomGrid->GetParam()->EnableUndo(TRUE);
}		 


