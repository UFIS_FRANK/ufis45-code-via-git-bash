// CoCo3.cpp : Defines the class behaviors for the application.
//
//
// CoCo3 is part of the CoCo project
//
// CoCo3 is the main file of the project.
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <CoCo3.h>
#include <CoCo3Dlg.h>

//#include "BasicData.h"
//#include "RegisterDlg.h"
#include <CedaBasicData.h>
#include <CCSGlobl.h>
//#include "PrivList.h"
#include <gxall.h>
//#include <CCSParam.h>
#include <AatLogin.h>
#include <AatHelp.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoCo3App

BEGIN_MESSAGE_MAP(CCoCo3App, CWinApp)
	//{{AFX_MSG_MAP(CCoCo3App)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoCo3App construction

CCoCo3App::CCoCo3App()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCoCo3App object

CCoCo3App theApp;

/////////////////////////////////////////////////////////////////////////////
// CCoCo3App initialization

BOOL CCoCo3App::InitInstance()
{
//	CCSParamEntry *myparamentry;		// only for testing CCSParam !!!

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox("IDP_OLE_INIT_FAILED");
		return FALSE;
	}

	AfxEnableControlContainer();

	GXInit();

	// INIT Tablenames and Homeairport
	char pclConfigPath[256];
//	char pclUser[256];
//	char pclPassword[256];
//	char pclDebug[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

    showRCButton=GetPrivateProfileInt(ogAppName, "IMPORTRC", 0, pclConfigPath);
    showUrnoCol=GetPrivateProfileInt(ogAppName, "SHOWURNO", 0, pclConfigPath);


	// set some initializazion values for CedaBasicData-object
	//
	// NOTE: it's possible that the given values are not enough for CEDA
	//
	//strcpy(ogBCD.pcmTableExt, "");			// wes: hard wired!!
	//strcpy(ogBCD.pcmHomeAirport, pcgHome);
	//strcpy(ogBCD.pcmApplName, "");

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		AfxAbort();
   		return FALSE;
	}
	ogBCD.SetTableExtension(pcgTableExt);

	if (!AatHelp::Initialize("COCO3"))
	{
		CString olError;
		AatHelp::GetLastError(olError);
		MessageBox(NULL,olError,"Error",MB_OK);
	}

//###
	CAatLogin olLoginCtrl;
	CWnd *pWnd = CWnd::GetDesktopWindow();
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),pWnd,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(ogAppName);

	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		return FALSE;
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

//###

	ogBCD.SetObject("TXT","URNO,APPL,COCO,STAT,STRG,TXID,STID");
	ogBCD.Read("TXT");
	ogBCD.SetDdxType("TXT", "IRT", TXT_NEW);
	ogBCD.SetDdxType("TXT", "URT", TXT_CHANGE);
	ogBCD.SetDdxType("TXT", "DRT", TXT_DELETE);


	// some testparameters - this is only for testing the CCSParam class and has no purpose in CoCo
	//
//	ogCCSParam.BufferParams("COCO");
//	myparamentry=ogCCSParam.GetParam("COCO", "PID_TEST1", "19991123103914", "not set",
//								"CoCo-Internal", "ID_TEST1", "TEXT");

//	myparamentry=ogCCSParam.GetParam("COCO", "PID_TEST4", "19991124161000", "still not set",
//								"CoCo-Interna4", "ID_TEST4", "TEXT");
//	myparamentry=ogCCSParam.GetParam("COCO", "PID_TEST5", "19991124161201", "upset",
//								"CoCo-Internal5", "ID_TEST5", "TEXT",
//								"19991124161000","19991125161200","1000","1200","1101111");


	
	
	
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

 	CCoCo3Dlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


int CCoCo3App::AddTxtRecord ( CString &ropAppl, CString &ropCoco, 
							   CString &ropStat, CString &ropStrg, 
							   CString &ropTxid, CString &ropStid, 
							   CString &ropUrno, bool bpSave/*=AutoCommitChanges*/ )
{
	RecordSet olRecord( ogBCD.GetFieldCount("TXT") );
	CCoCo3Dlg *polMainDlg;

	int ilEcd=0;     //Error-Code ^= Return-Wert
	//  Sicherheitschecks
	if ( ropAppl.IsEmpty() )
		ilEcd |= 1;
	if ( ropCoco.IsEmpty() )
		ilEcd |= 2;
	if ( ropTxid.IsEmpty() && ropStid.IsEmpty() )
		ilEcd |= 4;
	if ( !ilEcd && !IsNewRecordAllowed ( ropAppl, ropCoco, ropStid, ropTxid, false ) )
		ilEcd |= 8;
	
	if ( ilEcd  )
		return ilEcd;

	//  status defaultm��ig auf "NEW" 
	if ( ropStat.IsEmpty () )
		ropStat = "NEW";
	
	if ( ropUrno.IsEmpty () )
		ropUrno.Format( "%lu", ogBCD.GetNextUrno() );

	olRecord[POS_URNO] = ropUrno;	
	olRecord[POS_APPL] = ropAppl;	
	olRecord[POS_COCO] = ropCoco;
	olRecord[POS_STAT] = ropStat;
	olRecord[POS_STRN] = ropStrg;
	olRecord[POS_TXID] = ropTxid;
	olRecord[POS_STID] = ropStid;

	if ( !ogBCD.InsertRecord ( "TXT", olRecord, bpSave ) )
	{
		ilEcd |= 16;
		TRACE("[CCoCo3App::AddTxtRecord] Failed appending a record into TXTTAB.\n");
	}
	else
	{
		isModified = !bpSave;
		polMainDlg = (CCoCo3Dlg*)m_pMainWnd;
		if ( polMainDlg )
		{	
			polMainDlg->AddApplIfNew(ropAppl);
			polMainDlg->AddCocoIfNew(ropCoco);
			/*
			if ( polMainDlg->m_Applications.FindString(0,ropAppl) == CB_ERR )
			{
				polMainDlg->m_Applications.AddString(ropAppl);
				globAppList.Add(ropAppl) ;
			}
			if ( polMainDlg->m_CoCos.FindString(0,ropCoco) == CB_ERR )
			{
				polMainDlg->m_CoCos.AddString(ropCoco); 
				globCoCoList.Add(ropCoco);
			}*/
		}
		//globStrList.Add(ropStrg);				// keep track on strings
		globUrnoList.Add(ropUrno);				// and also remember urnos
	}
	return ilEcd;
}


CString GetInsertTXTErrorMsg ( int ipEcd )
{
	CString olRet;

	if ( ipEcd & 1 )
		olRet = "Field <APPL> is empty.\n";
	if ( ipEcd & 2 )
		olRet += "Field <COCO> is empty.\n";
	if ( ipEcd & 4 )
		olRet += "Fields <TXID> and <STID> are empty.\n";
	if ( ipEcd & 8 )
		olRet += "Either the combination <APPL,COCO,TXID> or the combination <APPL,COCO,STID> already exists.\n" ;
	if ( ipEcd & 16 )
		olRet += "InsertRecord failed.\n" ;
	if ( ipEcd & 32 )
		olRet += "At least one entry is too long.\n" ;
	return olRet.Left( olRet.GetLength()-1 ) ;
}

//  Check, ob der String ropStrg bereits in der Datenbank
//  Wenn gefunden, frage, ob dieser	verwendet wird
//  Wenn in genau einer APPL mit mit APPL!="GENERAL" und APPL!=ropAppl gefunden,
//  setze APPL auf "GENERAL", wenn dieser String verwendet werden soll
//  Return:
//	true:	Benutzer m�chte einen bereits vorhandenen String benutzen, d.h.
//			neuer String soll nicht eingef�gt werden
//  false:	String ist neu bzw. vorhandener soll nicht verwendet werden, d.h.
//			String mu� eingef�gt werden
bool CCoCo3App::UseExistingDuplicateString ( CWnd *popParent, CString &ropStrg,
											 CString &ropCoco, CString &ropAppl )
{
	// find out if string already exists
	CCoCo3Dlg *polMainDlg;
	RecordSet	olRecord;
	CCSPtrArray<RecordSet>  olFoundRecs;
	bool		blExistsInAppl = false, blExistsInGeneral = false;
	bool		blHaveToChangeAppl =false ;
	CStringList	olFoundInAppls;
	int			ilApplCount, i;
	CString		olStid, olTxid, olAppl, olMsg, olUrno;

	int ilAnz = ogBCD.GetRecordsExt("TXT", "COCO", "STRG", ropCoco, ropStrg, &olFoundRecs );
	if ( ilAnz <= 0 )
		return false;

	for ( i=0; i<ilAnz; i++ )
	{
		olAppl = olFoundRecs[i].Values[POS_APPL];
		if ( olAppl == ropAppl )
		{
			blExistsInAppl = true;
			break;	//  wenn String in gesuchter Appl. schon vorhanden, fertig
		}
		if ( olAppl == "GENERAL" )
			blExistsInGeneral = true;
		else	
			if ( !olFoundInAppls.Find ( olAppl ) )
			{
				olFoundInAppls.AddTail ( olAppl );
				olStid = olFoundRecs[i].Values[POS_STID]; 
				olTxid = olFoundRecs[i].Values[POS_TXID];
			}
	}
	olFoundRecs.DeleteAll ();
	polMainDlg = (CCoCo3Dlg*)m_pMainWnd;
	ilApplCount = olFoundInAppls.GetCount();

	if ( blExistsInAppl )
		olMsg = "The string\n\"%s\"\n already exists for the given application. Do you want to use the already existing entry (recommended)?" ;
	else
		if ( blExistsInGeneral )
			olMsg = "The string\n\"%s\"\n already exists with APPL=\"GENERAL\". Do you want to use the already existing entry (recommended)?" ;
		else
		{
			if ( ilApplCount>1 )
				olMsg = "The string\n\"%s\"\n already exists for other applications. Do you want to use one of them (Please change APPL to \"GENERAL\" for this string in all languages manually)?" ;
			else
			{
				olMsg = "The string\n\"%s\"\n already exists for an other application. Do you want to use the already existing entry (APPL will be changed to \"GENERAL\")?" ;
				blHaveToChangeAppl = true;
			}
		}
	olMsg.Replace ( "%s", ropStrg );

	//  Show Duplicate strings
	if ( polMainDlg )
	{
		bool blMustUpdate = false;
		int ilCocoToSel = polMainDlg->m_CoCos.FindStringExact( -1, ropCoco );
		int ilActSelCoco = polMainDlg->m_CoCos.GetCurSel();
		int ilActSelAppl = polMainDlg->m_Applications.GetCurSel();
		int ilApplToSel, ilLineToSel=0;

		if ( (ilActSelCoco>0) && (ilCocoToSel != ilActSelCoco ) )
		{	// falsche Sprache im Hauptdialog ausgew�hlt.
			polMainDlg->m_CoCos.SetCurSel(ilCocoToSel);
			blMustUpdate = true;
		}
		if ( blExistsInGeneral )
		{	// ist GENERAL oder all applications
			if ( !polMainDlg->m_ShowGeneral.GetCheck() && (ilActSelAppl>0) )
			{
				polMainDlg->m_Applications.GetLBText(ilActSelAppl,olAppl);
				if ( olAppl != "GENERAL" )
				{
					polMainDlg->m_ShowGeneral.SetCheck(1);
					blMustUpdate = true;
				}
			}		
		}
		else
		{
			if ( blExistsInAppl )
				ilApplToSel = polMainDlg->m_Applications.FindStringExact( -1, ropAppl );
			else
				if ( ilApplCount > 1 )		//  alle appl. anzeigen
					ilApplToSel = 0;
				else
					ilApplToSel = polMainDlg->m_Applications.FindStringExact( -1, olFoundInAppls.GetHead() );
			if  ( (ilActSelAppl>0) && (ilApplToSel>=0) && (ilApplToSel!=ilActSelAppl) )
			{
				polMainDlg->m_Applications.SetCurSel ( ilApplToSel );
				blMustUpdate = true;
			}
		}
		if ( blMustUpdate )
			polMainDlg->UpdateDisplay();
		polMainDlg->omCoCoGrid.SortTable ( 5 );
		for ( ROWCOL i=1; (i<=polMainDlg->omCoCoGrid.GetRowCount()) && !ilLineToSel; i++ )
			if ( ( polMainDlg->omCoCoGrid.GetValueRowCol ( i, 5 ) == ropStrg ) &&
				 ( polMainDlg->omCoCoGrid.GetValueRowCol ( i, 3 ) == ropCoco ) )
				ilLineToSel = i;

		if ( ilLineToSel )
		{
			polMainDlg->omCoCoGrid.SelectRange(CGXRange().SetRows(ilLineToSel) );				
			polMainDlg->omCoCoGrid.SetTopRow(ilLineToSel);		// make it visible
		}
	}

	if ( popParent )
		popParent->ShowWindow ( SW_HIDE );
	int ilRet = MessageBox( GetActiveWindow(), olMsg, "please confirm:", 
							MB_YESNO | MB_DEFBUTTON1 | MB_ICONQUESTION);
	if ( popParent )
		popParent->ShowWindow ( SW_SHOW );
	if ( ilRet == IDYES )
	{
		if ( blHaveToChangeAppl )
		{	//  Setze Appl auf "GENERAL" f�r alle Sprachen, wo APPL=olFoundInAppls.GetHead()
			//	STID=olStid und TXID=olTxid
			ilAnz = ogBCD.GetRecordsExt("TXT", "TXID", "STID", olTxid, olStid, &olFoundRecs );
			for ( i=0; i<ilAnz; i++ )
			{
				olUrno = olFoundRecs[i].Values[POS_URNO];
				if ( ogBCD.SetField( "TXT", "URNO", olUrno, "APPL", "GENERAL",AutoCommitChanges) )
					isModified=true;
			}
			olFoundRecs.DeleteAll ();
		}
	}
	return (ilRet==IDYES);
}

int CCoCo3App::TextChanged(CString &ropCoco, CString &ropOldText, 
						   CString &ropNewText)
{
	CCSPtrArray<RecordSet>  olEqualStrings;
	int						ilChanged = 0, ilRet, i;
	CString					olMsg = "Do you also want to change the following string from \"%s\" to \"%s\"?\nAPPL = %s\nSTID = %s\nTXID = %s";
	CString					olText, olUrno, olAppl, olTxid, olStid;

	if ( igAutoTranslationMode == NO_AUTO_TRANS )
		return 0;
	ogBCD.GetRecordsExt ( "TXT", "COCO", "STRG", ropCoco, ropOldText, &olEqualStrings );
	for ( i=0; i<olEqualStrings.GetSize(); i++ )
	{
		olUrno = olEqualStrings[i].Values[POS_URNO];
		if ( igAutoTranslationMode == AUTO_WITH_CONFIRM )
		{
			olAppl = olEqualStrings[i].Values[POS_APPL].IsEmpty() ? "<empty>" : 
								olEqualStrings[i].Values[POS_APPL];
			olTxid = olEqualStrings[i].Values[POS_TXID].IsEmpty() ? "<empty>" : 
								olEqualStrings[i].Values[POS_TXID];
			olStid = olEqualStrings[i].Values[POS_STID].IsEmpty() ? "<empty>" : 
								olEqualStrings[i].Values[POS_STID];
			olText.Format ( olMsg, ropOldText, ropNewText, olAppl, olStid, olTxid );
			ilRet = AfxMessageBox ( olText, MB_ICONQUESTION | MB_YESNO );
		}
		else
			ilRet = IDYES;		// igAutoTranslationMode == AUTO_NO_CONFIRM
		
		if ( ( ilRet == IDYES ) &&
			 ogBCD.SetField("TXT", "URNO",olUrno, "STRG", ropNewText, AutoCommitChanges) )
		{
 			ogBCD.SetField("TXT", "URNO",olUrno, "STAT", "TRANSLATED",AutoCommitChanges);//#hier
			ilChanged++;	
		}
	}
	return ilChanged;
}

int CCoCo3App::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	AatHelp::ExitApp();

	return CWinApp::ExitInstance();
}
