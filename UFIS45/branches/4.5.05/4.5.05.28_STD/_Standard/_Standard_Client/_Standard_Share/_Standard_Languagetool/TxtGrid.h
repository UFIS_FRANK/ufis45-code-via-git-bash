// TxtGrid.h: interface for the CTxtGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TXTGRID_H__14F4FB61_E85F_11D3_93E6_00001C033B5D__INCLUDED_)
#define AFX_TXTGRID_H__14F4FB61_E85F_11D3_93E6_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

template <class>class CCSPtrArray;
class RecordSet;

#include <GxGridTab.h>

struct GRIDDATA
{	
	CString		urno;
	RecordSet	*ptr;
	GRIDDATA() {ptr=0;}
};

class CTxtGrid : public GxGridTab  
{
public:
	CTxtGrid();
	virtual ~CTxtGrid();
public:
	int imCols;
	int *imDataIdx;
	int	imCompCol;
	//hag000809 CCSPtrArray<RecordSet*> omGridData;
	CCSPtrArray<GRIDDATA> omGridData;
	CStringArray omGridFields;
	CObList		omSortOrder;
public:
	virtual void SortTable ( ROWCOL ipCol );
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, const CGXStyle* pStyle, GXModifyType mt, int nType);
	ROWCOL GetRowCount();
	void LoadData ( char *pcpCocoFilter, char *pcpApplFilter, bool bpGeneral );
 
};

//static int CompareData(RecordSet* const **popItem1, RecordSet* const  **popItem2);
static int CompareData(GRIDDATA const **popItem1, GRIDDATA const  **popItem2);
static int CompareUrnos(CString const **popItem1, CString const  **popItem2);

RecordSet *GetAdressOfTxtRec ( CString& ropUrno );
	
#endif // !defined(AFX_TXTGRID_H__14F4FB61_E85F_11D3_93E6_00001C033B5D__INCLUDED_)
