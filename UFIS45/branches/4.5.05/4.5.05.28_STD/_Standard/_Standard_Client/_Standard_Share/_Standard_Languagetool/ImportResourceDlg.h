#if !defined(AFX_IMPORTRESOURCEDLG_H__DAA1E0B1_9C01_11D3_8FBA_0050DA1CAD13__INCLUDED_)
#define AFX_IMPORTRESOURCEDLG_H__DAA1E0B1_9C01_11D3_8FBA_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportResourceDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImportResourceDlg dialog

class CImportResourceDlg : public CDialog
{
// Construction
public:
	CImportResourceDlg(CWnd* pParent = NULL);   // standard constructor

	void LoadResource(CString myfullname, CString myfilename);
// Dialog Data
	//{{AFX_DATA(CImportResourceDlg)
	enum { IDD = IDD_IMPORT_DLG };
	CButton	m_Create;
	CButton	m_CreateAll;
	CListBox	m_output;
	CListBox	m_rc;
	CListBox	m_resource;
	CButton	m_Confirm;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportResourceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CImportResourceDlg)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDblclkList2();
	afx_msg void OnButton1();
	afx_msg void OnDblclkList3();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTRESOURCEDLG_H__DAA1E0B1_9C01_11D3_8FBA_0050DA1CAD13__INCLUDED_)
