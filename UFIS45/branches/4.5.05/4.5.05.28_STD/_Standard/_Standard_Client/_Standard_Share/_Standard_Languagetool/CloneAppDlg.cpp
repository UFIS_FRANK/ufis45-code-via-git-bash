// CloneAppDlg.cpp : implementation file
//
//
// CloneAppDlg is part of the CoCo project
//
// CloneAppDlg is a little dialog class that receives user input (application name) to be processed by the CoCo tool.
//	Its only function is to receive a text string.
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <CoCo3.h>
#include <CCSglobl.h>
#include <CloneAppDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCloneAppDlg dialog


CCloneAppDlg::CCloneAppDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCloneAppDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCloneAppDlg)
	m_CloneCoCo = _T("");
	bmNewLanguage = FALSE;
	bmOldLanguages = FALSE;
	//}}AFX_DATA_INIT
	olNewCocos.RemoveAll();
}


void CCloneAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCloneAppDlg)
	DDX_Control(pDX, IDC_COCO_LIST, omOldLangLB);
	DDX_Text(pDX, IDC_EDIT1, m_CloneCoCo);
	DDX_Check(pDX, IDC_NEW_LANG_CHK, bmNewLanguage);
	DDX_Check(pDX, IDC_OLD_LANG_CHK, bmOldLanguages);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCloneAppDlg, CDialog)
	//{{AFX_MSG_MAP(CCloneAppDlg)
	ON_BN_CLICKED(IDC_NEW_LANG_CHK, OnCheckBox)
	ON_BN_CLICKED(IDC_OLD_LANG_CHK, OnCheckBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCloneAppDlg message handlers

void CCloneAppDlg::OnCheckBox() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	CWnd *polCtrl = GetDlgItem ( IDC_EDIT1 );
	if ( polCtrl )
		polCtrl->EnableWindow ( bmNewLanguage );
	omOldLangLB.EnableWindow ( bmOldLanguages );
}

BOOL CCloneAppDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	for ( int i=0; i<globCoCoList.GetSize(); i++ )
		if ( globCoCoList[i] != m_OriginCoCo )
			omOldLangLB.AddString ( globCoCoList[i] );
	OnCheckBox ();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCloneAppDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString olItem;
	UpdateData();
	if ( bmNewLanguage && ( m_CloneCoCo.GetLength()==2 ) && 
		 ( m_CloneCoCo != m_OriginCoCo ) )
		olNewCocos.Add ( m_CloneCoCo );
	else
		m_CloneCoCo.Empty();
	int ilSelCount = 0;
	if ( bmOldLanguages )
		ilSelCount = omOldLangLB.GetSelCount( );
	if ( ilSelCount > 0 )
	{
		int *pilItems = new int[ilSelCount];

		ilSelCount = omOldLangLB.GetSelItems( ilSelCount, pilItems );
		for ( int i=0; i<ilSelCount; i++ )
		{
			omOldLangLB.GetText ( pilItems[i], olItem );
			olNewCocos.Add ( olItem );
		}
		if ( pilItems )
			delete pilItems;
	}
	if ( olNewCocos.GetSize () > 0 )
		CDialog::OnOK();
	else
	{
		UpdateData ( FALSE );
		MessageBox ( "No correct target language(s) selected !" );
	}
}
