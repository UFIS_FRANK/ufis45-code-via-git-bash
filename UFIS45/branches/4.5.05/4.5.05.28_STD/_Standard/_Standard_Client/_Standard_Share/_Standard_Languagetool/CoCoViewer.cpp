// CoCoViewer.cpp : implementation file
//
//
// CoCoViewer is part of the CoCo project
//
// CoCoViewer is a little dialog class that holds two grids and a language selection combo box.
//	Please see the comments in the functions headers for further details.
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <CoCo3.h>
#include <CoCoViewer.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCoCoViewer dialog


CCoCoViewer::CCoCoViewer(CWnd* pParent /*=NULL*/)
	: CDialog(CCoCoViewer::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoCoViewer)
	m_RefCoCo = _T("");
	//}}AFX_DATA_INIT
}


void CCoCoViewer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoCoViewer)
	DDX_Control(pDX, IDC_COMBO2, m_CmpCoCo);
	DDX_Text(pDX, IDC_EDIT1, m_RefCoCo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoCoViewer, CDialog)
	//{{AFX_MSG_MAP(CCoCoViewer)
	ON_WM_DRAWITEM()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnSelchangeCombo2)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoCoViewer message handlers

void CCoCoViewer::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default


	
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

//
// OnInitDialog initializes the two grids and the combobox for language selection. The grids headers
//	are initialized and the language selection combobox updated (with all known country codes). The
//	reference (left) grid is filled with values of the english ("US") language code.
//
//
BOOL CCoCoViewer::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString s;

	// initialize grids here
	omSrcGrid.SetParent(this);
	omSrcGrid.SubclassDlgItem(IDC_SRCGRID, this);
	omSrcGrid.Initialize();


	omDstGrid.SetParent(this);
	omDstGrid.SubclassDlgItem(IDC_DSTGRID, this);
	omDstGrid.Initialize();


	// set headers

	omSrcGrid.SetColCount(7);
	omSrcGrid.SetValueRange(CGXRange(0, 0), "Nr");		
	omSrcGrid.SetValueRange(CGXRange(0, 1), "Urno");		
	omSrcGrid.SetValueRange(CGXRange(0, 2), "App");		
	omSrcGrid.SetValueRange(CGXRange(0, 3), "CoCo");		
	omSrcGrid.SetValueRange(CGXRange(0, 4), "Status");		
	omSrcGrid.SetValueRange(CGXRange(0, 5), "String");		
	omSrcGrid.SetValueRange(CGXRange(0, 6), "TextID");		
	omSrcGrid.SetValueRange(CGXRange(0, 7), "StringID");		

	omDstGrid.SetColCount(7);
	omDstGrid.SetValueRange(CGXRange(0, 0), "Nr");		
	omDstGrid.SetValueRange(CGXRange(0, 1), "Urno");		
	omDstGrid.SetValueRange(CGXRange(0, 2), "App");		
	omDstGrid.SetValueRange(CGXRange(0, 3), "CoCo");		
	omDstGrid.SetValueRange(CGXRange(0, 4), "Status");		
	omDstGrid.SetValueRange(CGXRange(0, 5), "String");		
	omDstGrid.SetValueRange(CGXRange(0, 6), "TextID");		
	omDstGrid.SetValueRange(CGXRange(0, 7), "StringID");		


	for (int t=0; t<8; t++)	omSrcGrid.SetColWidth(t, t, StdColWidthArray[t]);
	for (t=0; t<8; t++)	omDstGrid.SetColWidth(t, t, StdColWidthArray[t]);

	// hide urnos
	omSrcGrid.HideCols(1,1,TRUE);	
	omDstGrid.HideCols(1,1,TRUE);

	//CString olRet;
	//olRet = ogBCD.GetValueList( "TXT", "APPL", refApp, "COCO" );

	// duplicate the strings from the CoCoList to the dialogs 
	// language selection combobox
	//
	m_CmpCoCo.Clear();

	for (t=(globCoCoList.GetSize()-1); t>=0; t--)
	{
		CString s=globCoCoList.GetAt(t);
		if (strcmp(s,refCoCo) != 0) { m_CmpCoCo.AddString(s); }
	}
	m_CmpCoCo.SetCurSel(0);


	int cnt=ogBCD.GetFieldCount("TXT");
	RecordSet olRecord(cnt);

	int fcnt=ogBCD.GetDataCount("TXT");
	omSrcGrid.SetRowCount(fcnt);
	omDstGrid.SetRowCount(fcnt);


	
	int lcnt=0;		// count entries in src-grid
	int rcnt=1;		// count entries in dst-grid

	CedaObject *prlObject;
	CCSPtrArray<RecordSet>  *polObjectData = 0;

	if( ogBCD.omObjectMap.Lookup("TXT",(void *& )prlObject) )
		polObjectData = prlObject->GetDataBuffer();

	// fill left grid as reference
	for (t=0; t<fcnt; t++)
	{
		if ( polObjectData && ( (*polObjectData)[t].GetStatus() == DATA_DELETED ) )
			continue;
		ogBCD.GetRecord("TXT", t, olRecord);

		s=olRecord.Values[POS_APPL];			// application name
		if (strcmp(s,refApp) == 0)
		{
			s=olRecord.Values[POS_COCO]; 		// coco
			if (strcmp(s,refCoCo) == 0)
			{	
				// add entry in left grid = reference grid
				int status = olRecord.GetStatus();
				s.Format("%i",rcnt); 
				omSrcGrid.SetValueRange(CGXRange(rcnt,0),s);
				s=olRecord.Values[POS_URNO]; 
				omSrcGrid.SetValueRange(CGXRange(rcnt,1),s);		// urno
				s=olRecord.Values[POS_APPL];  
				omSrcGrid.SetValueRange(CGXRange(rcnt,2),s);		// application
				s=olRecord.Values[POS_COCO];  
				omSrcGrid.SetValueRange(CGXRange(rcnt,3),s);		// coco
				s=olRecord.Values[POS_STAT];  
				omSrcGrid.SetValueRange(CGXRange(rcnt,4),s);		// status

				s=olRecord.Values[POS_STRN]; 
				MakeClientString(s); 
				Internal2Grid(s); 
				omSrcGrid.SetValueRange(CGXRange(rcnt,5),s);		// strings

				s=olRecord.Values[POS_TXID]; 
				omSrcGrid.SetValueRange(CGXRange(rcnt,6),s);		// textID
				s=olRecord.Values[POS_STID];  
				omSrcGrid.SetValueRange(CGXRange(rcnt,7),s);		// stringID
				rcnt++;
			}
		}
	}
	omSrcGrid.SetRowCount(rcnt-1);
	omDstGrid.SetRowCount(rcnt-1);

	UpdateLangDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//
// UpdateLangDisplay clears the right grids entries and fills it with the records of the selected
//	language code at the position of the reference records on the left grid. Record matching is done
//	by comparing the StringID's of both records.
//
//
void CCoCoViewer::UpdateLangDisplay()
{
	CString ThisCoCo,s;

	int fcnt=ogBCD.GetDataCount("TXT");
	int cnt=ogBCD.GetFieldCount("TXT");
	RecordSet olRecord(cnt);

	// clear right grid
	//
	for (int t=omSrcGrid.GetRowCount(); t>0; t--)
	{
		omDstGrid.SetValueRange(CGXRange(t,0),"");
		omDstGrid.SetValueRange(CGXRange(t,1),"");
		omDstGrid.SetValueRange(CGXRange(t,2),"");
		omDstGrid.SetValueRange(CGXRange(t,3),"");
		omDstGrid.SetValueRange(CGXRange(t,4),"");
		omDstGrid.SetValueRange(CGXRange(t,5),"");
		omDstGrid.SetValueRange(CGXRange(t,6),"");
		omDstGrid.SetValueRange(CGXRange(t,7),"");
	}

	// fill right grid as reference
	//
	int lpos;
	int cmpcoco = m_CmpCoCo.GetCurSel();
	if (cmpcoco>=0)
	{
		m_CmpCoCo.GetLBText(cmpcoco,ThisCoCo);

		for (int t=0; t<fcnt; t++)
		{
			ogBCD.GetRecord("TXT", t, olRecord);

			s=olRecord.Values[POS_APPL]; /*MakeClientString(s);*/			// application name
			if (strcmp(s,refApp) == 0)
			{
				s=olRecord.Values[POS_COCO]; /*MakeClientString(s);*/		// coco
				if (strcmp(s,ThisCoCo) == 0)
				{	
					// find corresponding part on left side
					s=olRecord.Values[POS_STID]; /*MakeClientString(s);*/		// StringID
					lpos=omSrcGrid.GetRowCount();
					while ((lpos>0) && (strcmp(omSrcGrid.GetValueRowCol(lpos,7),s)))
					{
						lpos--;
					}
					if (lpos>0)
					{
						// add entry in left grid = reference grid
						s=omSrcGrid.GetValueRowCol(lpos,0); omDstGrid.SetValueRange(CGXRange(lpos,0),s);	// lfd. Nr
						s=olRecord.Values[POS_URNO]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,1),s);		// urno
						s=olRecord.Values[POS_APPL]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,2),s);		// application
						s=olRecord.Values[POS_COCO]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,3),s);		// coco
						s=olRecord.Values[POS_STAT]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,4),s);		// status
						s=olRecord.Values[POS_STRN]; MakeClientString(s); Internal2Grid(s); omDstGrid.SetValueRange(CGXRange(lpos,5),s);		// strings
						s=olRecord.Values[POS_TXID]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,6),s);		// textID
						s=olRecord.Values[POS_STID]; /*MakeClientString(s);*/ omDstGrid.SetValueRange(CGXRange(lpos,7),s);		// stringID
					}
				}
			}
		}
	}
}

//
// resize the grids to maximum
//
//
void CCoCoViewer::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// resize grids
	RECT myrect, myrect2;

	if (omSrcGrid)
	{
		GetClientRect(&myrect);
		myrect2.top=50;
		myrect2.left=myrect.left;
		myrect2.bottom=myrect.bottom;
		myrect2.right=(myrect.right-myrect.left)/2;
		omSrcGrid.MoveWindow(&myrect2,TRUE); 

		myrect2.top=50;
		myrect2.left=(myrect.right-myrect.left)/2;
		myrect2.bottom=myrect.bottom;
		myrect2.right=myrect.right; 
		omDstGrid.MoveWindow(&myrect2,TRUE); 
	}
		
}

void CCoCoViewer::OnSelchangeCombo1() 
{
	UpdateLangDisplay();	
}

//
// copy reference entry to other language
//
//
void CCoCoViewer::OnButton1() 
{
	int cnt=ogBCD.GetFieldCount("TXT");

	RecordSet olNewRecord(cnt);
	CRowColArray MyRows;
				
	int selcnt=omSrcGrid.GetSelectedRows(MyRows, TRUE, TRUE);	
	if (selcnt>0)
	{
		CString s, newurno, newstring;

		int i=m_CmpCoCo.GetCurSel();
		m_CmpCoCo.GetLBText(i, s); 

			
		for (int t=(selcnt-1); t>=0; t--) 
		{
			olNewRecord.Values[POS_STID]=omSrcGrid.GetValueRowCol(MyRows[t],7);	// stid		
			newstring=omSrcGrid.GetValueRowCol(MyRows[t],5); olNewRecord.Values[POS_STRN]=newstring;	// strn
			olNewRecord.Values[POS_APPL]=omSrcGrid.GetValueRowCol(MyRows[t],2);	// appl
			newurno.Format("%ld",ogBCD.GetNextUrno());
			olNewRecord.Values[POS_URNO]=newurno;					// reset urno		
			olNewRecord.Values[POS_TXID]=omSrcGrid.GetValueRowCol(MyRows[t],6);	// txid
			olNewRecord.Values[POS_STAT]="NEW";
			olNewRecord.Values[POS_COCO]=s;

			if (ogBCD.InsertRecord("TXT",olNewRecord,AutoCommitChanges) == false)
			{
				ogLog.Trace("DEBUG", "[LanguageTool::CopyReference] Failed copying a record into TXTTAB.");
			}
			else 
			{
				//globStrList.Add(newstring);				// keep track on strings
				globUrnoList.Add(newurno);												// and also remember urnos
			}
			isModified=true;
		}
	}
	else { MessageBox("Please select at least one record before trying to copy it.", "sorry ...", MB_OK | MB_ICONEXCLAMATION); }

	UpdateLangDisplay();
}

void CCoCoViewer::OnSelchangeCombo2() 
{
	UpdateLangDisplay();
}

void CCoCoViewer::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	RECT myrect, myrect2;

	GetClientRect(&myrect);
	myrect2.top=50;
	myrect2.left=myrect.left;
	myrect2.bottom=myrect.bottom;
	myrect2.right=(myrect.right-myrect.left)/2;
	omSrcGrid.MoveWindow(&myrect2,TRUE); 

	myrect2.top=50;
	myrect2.left=(myrect.right-myrect.left)/2;
	myrect2.bottom=myrect.bottom;
	myrect2.right=myrect.right; 
	omDstGrid.MoveWindow(&myrect2,TRUE); 
}
