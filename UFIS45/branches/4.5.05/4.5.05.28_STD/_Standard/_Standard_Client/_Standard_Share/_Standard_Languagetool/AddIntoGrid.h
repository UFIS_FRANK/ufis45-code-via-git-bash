#if !defined(AFX_ADDINTOGRID_H__26B83E41_EE83_11D3_93ED_00001C033B5D__INCLUDED_)
#define AFX_ADDINTOGRID_H__26B83E41_EE83_11D3_93ED_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddIntoGrid.h : header file
//

class CInputGrid;

#define INPUTCOLS 6

/////////////////////////////////////////////////////////////////////////////
// CAddIntoGrid dialog

class CAddIntoGrid : public CDialog
{
// Construction
public:
	CAddIntoGrid(CWnd* pParent = NULL);   // standard constructor
	~CAddIntoGrid();

// Dialog Data
	//{{AFX_DATA(CAddIntoGrid)
	enum { IDD = IDD_ADD_INTO_GRID };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CInputGrid *pomGrid;
	CString		pomFields[INPUTCOLS];
	int			pimFieldLengths[INPUTCOLS];
	int			pimFieldIdx[INPUTCOLS];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddIntoGrid)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool StoreEntries ();
	void SetColWidths ();
	CString GetErrorMessage ( int ipEcd );


	// Generated message map functions
	//{{AFX_MSG(CAddIntoGrid)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSavetodb();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDINTOGRID_H__26B83E41_EE83_11D3_93ED_00001C033B5D__INCLUDED_)
