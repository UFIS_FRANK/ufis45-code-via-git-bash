// WDayChk.cpp: implementation of the CWDayCheckListComboBox class.
// The control was not derived, it was completely copied and changed.
// Only some of the functions bellow were changed considerably:
// EncodeValue(), EncodeValueSupportRoutine(), 
// Decodevalue(), DecodeValueSupportRoutine(),
// KeyPressed(), ConvertControlTextToValue()
// these were also marked with the heads
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <DBDPS.h>
#include <WDayChk.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const TCHAR WDAY_UNSELECTED = _T('-');


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#ifdef _GXDLL
#undef AFXAPI_DATA
#define AFXAPI_DATA __based(__segname("_DATA"))
#endif

// Headers

#include <gxresrc.h>

#ifndef _GXCTRLI_H_
#include <gxctrli.h>
#endif

#ifndef _GXMSG_H_
#include <gxmsg.h>
#endif

#ifdef _GXDLL
#undef AFXAPP_DATA
#define AFXAPP_DATA AFXAPI_DATA
#endif

#ifdef GX_USE_SEGMENTS
#pragma code_seg("GX_SEG_GXCCBCHK")
#endif

IMPLEMENT_CONTROL(CWDayCheckListComboBox, CGXComboBox);
IMPLEMENT_DYNAMIC(CWDayCheckListComboBox, CGXComboBox)
IMPLEMENT_DYNAMIC(CWDayCheckListComboLBox, CCheckListBox)

#define new DEBUG_NEW

LPCTSTR szDelim = _T(";");

// CWDayCheckListComboLBox

CWDayCheckListComboLBox::CWDayCheckListComboLBox(CWnd* pMsgWnd)
{
	m_bKeyDown = FALSE;
	m_bLButtonDown = FALSE;
	m_pMsgWnd = pMsgWnd;
	m_bColor = FALSE;
}

void CWDayCheckListComboLBox::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	VERIFY(CCheckListBox::Create(
		dwStyle | WS_VISIBLE | WS_BORDER | LBS_HASSTRINGS,
		rect, pParentWnd, nID));
}

CWDayCheckListComboLBox::~CWDayCheckListComboLBox()
{
}

BEGIN_MESSAGE_MAP(CWDayCheckListComboLBox, CCheckListBox)
	//{{AFX_MSG_MAP(CWDayCheckListComboLBox)
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_KEYUP()
	ON_WM_KILLFOCUS()
	ON_WM_SYSKEYUP()
	ON_WM_SYSKEYDOWN()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// ============ CWDayCheckListComboLBox message handlers

void CWDayCheckListComboLBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	m_bKeyDown =TRUE;
	m_nOldSel = GetCurSel();
	m_bAlNum = _istalnum((_TXCHAR) nChar);

	CCheckListBox::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CWDayCheckListComboLBox::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bLButtonDown = TRUE;
	m_nOldSel = GetCurSel();

	CCheckListBox::OnLButtonDown(nFlags, point);
}

void CWDayCheckListComboLBox::OnLButtonUp(UINT nFlags, CPoint point)
{
	CCheckListBox::OnLButtonUp(nFlags, point);

	m_bLButtonDown = FALSE;
}

BOOL CWDayCheckListComboLBox::OnEraseBkgnd(CDC* pDC)
{
#if _MFC_VER >= 0x0300
	HBRUSH hBrush = (HBRUSH) GetParent()->SendMessage(
		WM_CTLCOLORLISTBOX,
		(WPARAM) pDC->GetSafeHdc(),
		(LPARAM) m_hWnd);
#else
	HBRUSH hBrush = (HBRUSH) GetParent()->SendMessage(
		WM_CTLCOLOR,
		(WPARAM) pDC->GetSafeHdc(),
		MAKELONG((WORD) m_hWnd, (WORD) CTLCOLOR_LISTBOX));
#endif

	if (hBrush)
	{
		CRect rect;
		GetClientRect(rect);
		::FillRect(pDC->GetSafeHdc(), rect, hBrush);
		return TRUE;
	}

	return CCheckListBox::OnEraseBkgnd(pDC);
}

void CWDayCheckListComboLBox::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CCheckListBox::OnKeyUp(nChar, nRepCnt, nFlags);

	if (m_bKeyDown)
	{
		if (nChar == 9)
		{
			m_pMsgWnd->PostMessage(WM_KEYDOWN, 9);
			m_pMsgWnd->PostMessage(WM_KEYUP, 9);
			m_pMsgWnd->SendMessage(WM_GX_LBOXEND, 0, 0);
		}
		else if (nChar == 13)
			m_pMsgWnd->SendMessage(WM_GX_LBOXEND, 0, 0);
		else if (nChar == 27)
			m_pMsgWnd->SendMessage(WM_GX_LBOXEND, 0, 0);
	}
	m_bKeyDown = FALSE;
}

void CWDayCheckListComboLBox::OnKillFocus(CWnd* pNewWnd)
{
	CCheckListBox::OnKillFocus(pNewWnd);

	if (m_pMsgWnd)
		m_pMsgWnd->PostMessage(WM_GX_LBOXEND, 0, 0);
}

void CWDayCheckListComboLBox::OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (m_bKeyDown && nChar == VK_DOWN)
		m_pMsgWnd->SendMessage(WM_GX_LBOXEND, 0, 0);
	else
		CCheckListBox::OnSysKeyUp(nChar, nRepCnt, nFlags);
}

void CWDayCheckListComboLBox::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	m_bKeyDown =TRUE;

	CCheckListBox::OnSysKeyDown(nChar, nRepCnt, nFlags);
}


// CWDayCheckListComboBox

const int nComboBtnWidth = 15;
const int nComboBtnHeight = 18;

CWDayCheckListComboBox::CWDayCheckListComboBox(CGXGridCore* pGrid, UINT nEditID, UINT nListBoxID)
	: CGXComboBox(pGrid, nEditID, nListBoxID, GXCOMBO_NOTEXTFIT)
{
	//m_Active = FALSE;
}

CWDayCheckListComboBox::~CWDayCheckListComboBox()
{
}



class BadFormatEncodeValueFailed {} ; //exception

//-----------------------------------------------------------------------
// EncodeValueSupportRoutine
//	
//	This function serves for the EncodeValue
//-----------------------------------------------------------------------
void CWDayCheckListComboBox::EncodeValueSupportRoutine(const CString &value,
		const Attributes *attr,
		const CStringArray& LeadingAbbrev, const char LeadingCode, CString &code)
{
	LPCTSTR pValue = value;
//	const char* pValue = value1;
	const char NoLeadingCode = (LeadingCode == '1') ? '0' : '1';

	while( *pValue)
	{
		if(code.GetLength() >= attr->Items)
			throw BadFormatEncodeValueFailed();

		if(code.GetLength() && attr->Separator.GetLength())
		{
			if( _tcsncmp( attr->Separator, pValue, attr->Separator.GetLength()))
			{
				if(!(attr->SmartSeparator))
					throw BadFormatEncodeValueFailed();
			}
			else
				pValue += attr->Separator.GetLength();

			if( attr->SmartSeparator)
			{
				while(! _tcsncmp( attr->Separator, pValue, attr->Separator.GetLength()))
				{
					pValue += attr->Separator.GetLength();
					code += NoLeadingCode;
				}
			}
		}
		if( !_tcsncmp( LeadingAbbrev[code.GetLength()], pValue,
			LeadingAbbrev[code.GetLength()].GetLength()))
		{
			pValue += min((size_t)(LeadingAbbrev[code.GetLength()].GetLength())
							,_tcslen(pValue));
			code += LeadingCode;
			continue;
		}
		else
		{
			code += NoLeadingCode;
			continue;
		}
	}
	if( attr->Separator.IsEmpty() || attr->SmartSeparator)
		while(code.GetLength() < attr->Items)
			code += NoLeadingCode;

	if(code.GetLength() != attr->Items)
		throw BadFormatEncodeValueFailed();
}

//-----------------------------------------------------------------------
// EncodeValue
//	
//	This function creates the bit code out of string value
//-----------------------------------------------------------------------
bool CWDayCheckListComboBox::EncodeValue( const Attributes* attr, CString& code)
{
	try
	{
		const CString value = code;
		code.Empty();

		ASSERT(attr->Items > 0);
		ASSERT(attr->Name.GetSize() == attr->Items);
		ASSERT(attr->Checked.GetSize() == attr->Items || !attr->Checked.GetSize());
		ASSERT(attr->Unchecked.GetSize() == attr->Items || !attr->Unchecked.GetSize());
		ASSERT(attr->VirtKey.GetSize() == attr->Items || !attr->VirtKey.GetSize());
		ASSERT(attr->DefaultCode.GetLength() == attr->Items || !attr->DefaultCode.GetLength());
		ASSERT(attr->Checked.GetSize() || attr->Unchecked.GetSize());


		if(attr->Checked.GetSize() && attr->Unchecked.GetSize())
		{
			LPCTSTR pValue = value;

			while( *pValue)
			{
				if(code.GetLength() >= attr->Items)
					throw BadFormatEncodeValueFailed();

				if(code.GetLength() && attr->Separator.GetLength())
				{
					if( _tcsncmp( attr->Separator, pValue, attr->Separator.GetLength()))
					{
						if(!(attr->SmartSeparator))
							throw BadFormatEncodeValueFailed();
					}
					else
						pValue += attr->Separator.GetLength();

					if( attr->SmartSeparator)
					{
						while( !_tcsncmp( attr->Separator, pValue, attr->Separator.GetLength()))
						{
							pValue += attr->Separator.GetLength();
							code += (attr->Checked[code.GetLength()].IsEmpty()) 
										? '1' : '0';
						}
					}
				}

				if( attr->Checked[code.GetLength()].GetLength() 
					&& !_tcsncmp( attr->Checked[code.GetLength()], pValue,
					attr->Checked[code.GetLength()].GetLength()))
				{
					pValue += min((size_t)(attr->Checked[code.GetLength()].GetLength())
									,_tcslen(pValue));
					code += '1';
					continue;
				}
				if( attr->Unchecked[code.GetLength()].GetLength()
					&& !_tcsncmp( attr->Unchecked[code.GetLength()], pValue,
					attr->Unchecked[code.GetLength()].GetLength()))
				{
					pValue += min((size_t)(attr->Unchecked[code.GetLength()].GetLength())
									,_tcslen(pValue));
					code += '0';
					continue;
				}
				if(!attr->Checked[code.GetLength()].GetLength())
				{
					code += '1';
					continue;
				}
				if(!attr->Unchecked[code.GetLength()].GetLength())
				{
					code += '0';
					continue;
				}
				throw BadFormatEncodeValueFailed();
			}
			if( attr->Separator.IsEmpty() || attr->SmartSeparator)
				while(code.GetLength() < attr->Items)
					code += (attr->Checked[code.GetLength()].IsEmpty()) ? '1' : '0';

			if(code.GetLength() != attr->Items)
				throw BadFormatEncodeValueFailed();

			return true;
		}

		if(attr->Checked.GetSize())
		{

			EncodeValueSupportRoutine( value, attr, attr->Checked, '1', code);
			return true;
		}

		if(attr->Unchecked.GetSize())
		{


			EncodeValueSupportRoutine( value, attr, attr->Unchecked, '0', code);
			return true;
		}
		ASSERT(0);
	}
	catch(BadFormatEncodeValueFailed )
	{
		if(attr->DefaultCode.GetLength())
			code = attr->DefaultCode;
		else
		{
			code.Empty();
			for(int i = attr->Items; i--;)
				code += '0';
		}
		return false;
	}
	catch(...)
	{
		throw;
	}
	return true;
}


//-----------------------------------------------------------------------
// DecodeValueSupportRoutine
//	
//	This function serves for DecodeValue
//-----------------------------------------------------------------------
void CWDayCheckListComboBox::DecodeValueSupportRoutine(const CString &code,
		const Attributes *attr,
		const CStringArray& LeadingAbbrev, const char LeadingCode, CString &value)
{
	const char NoLeadingCode = (LeadingCode == '1') ? '0' : '1';

	bool	dummyLastValue = false;
	bool	insertSmartSeparator = false;

	for(int i=0; i < attr->Items; i++)
	{
		if(i && attr->Separator.GetLength())
		{
			if(attr->SmartSeparator == false)
				value += attr->Separator;
			else
			{
				if(!dummyLastValue)
					insertSmartSeparator = true;
			}
		}

		dummyLastValue = true;	//assumption

		if( code[i] == LeadingCode)
		{
			if(!(LeadingAbbrev[i].IsEmpty()))
			{
				dummyLastValue = false;
				if(insertSmartSeparator)
				{
					value += attr->Separator;
					insertSmartSeparator = false;
				}
				value += LeadingAbbrev[i];
			}
		}
		else
			ASSERT(code[i] == NoLeadingCode);
	}
	return;
}


//-----------------------------------------------------------------------
// DecodeValue
//	
//	This function creates the string value out of the bit code 
//-----------------------------------------------------------------------
void CWDayCheckListComboBox::DecodeValue( const Attributes* attr, CString& value)
{
	const CString code = value;
	value.Empty();

	ASSERT(attr->Items > 0);
	ASSERT(attr->Name.GetSize() == attr->Items);
	ASSERT(attr->Checked.GetSize() == attr->Items || !attr->Checked.GetSize());
	ASSERT(attr->Unchecked.GetSize() == attr->Items || !attr->Unchecked.GetSize());
	ASSERT(attr->VirtKey.GetSize() == attr->Items || !attr->VirtKey.GetSize());
	ASSERT(attr->DefaultCode.GetLength() == attr->Items || !attr->DefaultCode.GetLength());
	ASSERT(attr->Checked.GetSize() || attr->Unchecked.GetSize());

	bool	dummyLastValue = false;
	bool	insertSmartSeparator = false;

	if(attr->Checked.GetSize() && attr->Unchecked.GetSize())
	{
		for(int i=0; i < attr->Items; i++)
		{
			if(i && attr->Separator.GetLength())
			{
				if(attr->SmartSeparator == false)
					value += attr->Separator;
				else
				{
					if(!dummyLastValue)
					{
						insertSmartSeparator = true;
					}
				}
			}

			dummyLastValue = true;	//assumption

			if( code[i] == '0')
			{
				if( !(attr->Unchecked[i].IsEmpty()))
				{
					dummyLastValue = false;
					if(insertSmartSeparator)
					{
						value += attr->Separator;
						insertSmartSeparator = false;
					}
					value += attr->Unchecked[i];
				}
			}
			else
			{
				ASSERT(code [i] == '1');
				if(!(attr->Checked[i].IsEmpty()))
				{
					dummyLastValue = false;
					if(insertSmartSeparator)
					{
						value += attr->Separator;
						insertSmartSeparator = false;
					}
					value += attr->Checked[i];
				}
			}
		}
		return;
	}
	if(attr->Checked.GetSize())
	{

		DecodeValueSupportRoutine( code, attr, attr->Checked, '1', value);
		return;
	}
	if(attr->Unchecked.GetSize())
	{


		DecodeValueSupportRoutine( code, attr, attr->Unchecked, '0', value);
		return;
	}
	ASSERT(0);
}


/*
BOOL CWDayCheckListComboBox::OnProcessChar(UINT , UINT , UINT )
{
	m_sLastChar.Empty();
	return FALSE;
}
*/

DWORD CWDayCheckListComboBox::CalcEditStyle(LPRECT rectNP, int& dyHeight)
{
	DWORD dwStyle = CGXEditControl::CalcEditStyle(rectNP, dyHeight);

	// Add ES_PASSWORD to the edit control style settings and
	// remove ES_MULTILINE bit becuase ES_PASSWORD does not work
	// together with ES_MULTILINE.

	return dwStyle | ES_READONLY;
}


BEGIN_MESSAGE_MAP(CWDayCheckListComboBox, CGXComboBox)
	//{{AFX_MSG_MAP(CWDayCheckListComboBox)
	ON_MESSAGE(WM_GX_LBOXEND, OnListBoxEnd)
	ON_MESSAGE(WM_GX_LBOXCANCEL, OnListBoxCancel)
	ON_MESSAGE(WM_GX_LBOXCHANGED, OnListBoxChanged)
	ON_MESSAGE(WM_GX_LBOXKILLFOCUS, OnListBoxKillFocus)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/*
BOOL CWDayCheckListComboBox::GetControlText(CString& strResult, ROWCOL nRow, ROWCOL nCol, LPCTSTR pszRawValue, const CGXStyle& style)
{
	// Unused:
	pszRawValue, nRow, nCol;

	strResult = style.GetValue();

	return TRUE;
}
*/

CWnd* CWDayCheckListComboBox::CreateListBox(CWnd* pParentWnd, UINT nID)
{
	// Creates the listbox

	// if you want to create an owner-drawn listbox, you should
	// derive a class from CWDayCheckListComboLBox, override CreateListBox
	// and return a pointer to it.

	CWDayCheckListComboLBox* pListBox = new CWDayCheckListComboLBox(this);

	CRect rect;
	pParentWnd->GetClientRect(&rect);
	rect.left++;
	rect.right--;

	if (m_dwListBoxStyle == 0)
		m_dwListBoxStyle = WS_VSCROLL|LBS_NOTIFY;

	pListBox->Create(m_dwListBoxStyle, rect, pParentWnd, nID);

	// CWDayCheckListComboBox will delete the listbox object.

	return pListBox;
}

void CWDayCheckListComboBox::OnFillDroppedList(CListBox* lbox)
{
	const CGXStyle& style = Grid()->LookupStyleRowCol(m_nRow, m_nCol);
	CWDayCheckListComboLBox* checklbox = (CWDayCheckListComboLBox*) lbox;
	ASSERT(checklbox->IsKindOf(RUNTIME_CLASS(CWDayCheckListComboLBox)));

	// fill with Choices
	ASSERT(style.GetIncludeItemDataPtr());
	if (style.GetIncludeItemDataPtr())
	{
		const Attributes* att = (const Attributes *) style.GetItemDataPtr();

		ASSERT(att);
		if(att)
		{
			m_nCount = att->Items;

			for (int i = 0; i < m_nCount; i++)
			{
				lbox->AddString(att->Name[i]);
			}
		}
	}

	// enable choices
	if (style.GetIncludeValue())
	{
		CString Value = style.GetValue();

		ASSERT(style.GetIncludeItemDataPtr());
		if (style.GetIncludeItemDataPtr())
		{
			const Attributes* att = (const Attributes *) style.GetItemDataPtr();

			ASSERT(att);
			if(att)
			{
				CString code = Value;
					
				if( !EncodeValue( att, Value))	
				{
					MessageBeep(-1);		//unrecognized value set to default

					CString tmpValue = Value;
					DecodeValue( att, tmpValue);
					SetControlText(m_nRow,m_nCol,tmpValue);
				}

				for (int n = 0; n < __min(m_nCount,Value.GetLength()); n++)
				{
					checklbox->SetCheck(n, Value[n] == '1');
				}
			}
		}
	}
}

void CWDayCheckListComboBox::OnStoreDroppedList(CListBox* lbox)
{
	CWDayCheckListComboLBox* checklbox = (CWDayCheckListComboLBox*) lbox;
	ASSERT(checklbox->IsKindOf(RUNTIME_CLASS(CWDayCheckListComboLBox)));

	// Save the selection
	// enable choices
	
	CGXStyle style;
	Grid()->ComposeStyleRowCol(m_nRow, m_nCol, &style);
	CString Value;
	
	for (int n = 0; n < m_nCount; n++)
	{
		Value += (checklbox->GetCheck(n) == 1) ? '1' : '0';
	}

	ASSERT(style.GetIncludeItemDataPtr());
	if (style.GetIncludeItemDataPtr())
	{
		const Attributes* att = (const Attributes *) style.GetItemDataPtr();

		ASSERT(att);
		if(att)
		{
			ASSERT(att->Items == m_nCount);

			DecodeValue( att, Value);

			// Selection has changed
			if(Value != style.GetValue()) 
			{
				SetModify(TRUE);

				Grid()->SetValueRange(CGXRange(m_nRow, m_nCol), Value);
			}
		}
	}
}

LRESULT CWDayCheckListComboBox::OnListBoxEnd(WPARAM, LPARAM )
{
	Grid()->SetIgnoreFocus(TRUE);
	Grid()->SetDropDownCell(FALSE);

	// User has selected an item
	if (m_pDropDownWnd)
	{
		CGXGridCombo::SetComboBoxDropDown(TRUE);

		NeedStyle();

		// empty cell, when user pressed alpahnumeric key
		if (!IsReadOnly())
			OnStoreDroppedList(&m_pDropDownWnd->GetLBox());

		// I need to set m_pDropDownWnd = NULL before I destroy the window
		// This avoids problems when killing the focus results in a call to ListBoxCancel
		CWnd* pDropDownWnd = m_pDropDownWnd;
		m_pDropDownWnd  = NULL;

		if (pDropDownWnd)
			DestroyDropDownWnd(pDropDownWnd);

		CGXGridCombo::SetComboBoxDropDown(FALSE);

		// Fire OnModifyCell event after Dropdown-window has been destroyed

		OnModifyCell();

#ifdef _WIN32
		if (GetFocus() == GridWnd())
			SetFocus();
#else
		if (::IsChild(AfxGetMainWnd()->m_hWnd,::GetFocus()));
			SetFocus();
#endif

		Grid()->SetIgnoreFocus(FALSE);

		SetModify(FALSE);
		SetActive(FALSE);
		Refresh();
	}

	return 0;
}

LRESULT CWDayCheckListComboBox::OnListBoxCancel(WPARAM wParam, LPARAM lParam)
{
	return OnListBoxEnd(wParam, lParam);
}

LRESULT CWDayCheckListComboBox::OnListBoxChanged(WPARAM, LPARAM)
{
	// User has changed selection
	return 0;
}

LRESULT CWDayCheckListComboBox::OnListBoxKillFocus(WPARAM, LPARAM)
{
	// User has changed selection

	CWnd* pWnd = GetFocus();

	if (!(GridWnd()->IsChild(pWnd) || GridWnd() == pWnd))
		SendMessage(WM_GX_LBOXCANCEL, 0, 0);

	return 0;
}

//-----------------------------------------------------------------------
// KeyPressed
//
// This function handles the press key event
//-----------------------------------------------------------------------
BOOL CWDayCheckListComboBox::KeyPressed(UINT nMessage, UINT nChar, UINT nRepCnt, UINT flags)
{
	if (nMessage == WM_SYSKEYDOWN)
		return CGXComboBox::KeyPressed(nMessage, nChar, nRepCnt, flags);

	if( nMessage == WM_CHAR)
	{
		CGXStyle style;

		Grid()->ComposeStyleRowCol(m_nRow, m_nCol, &style);

		ASSERT(style.GetIncludeItemDataPtr());
		if (style.GetIncludeItemDataPtr())
		{
			const Attributes* att = (const Attributes *) style.GetItemDataPtr();

			ASSERT(att);
			if(att)
			{
				if ((nChar | 32) == _T('a'))		// on pressing 'a' set all the values
				{
					CString Value(_T('1'), att->Items);
					DecodeValue( att, Value);
					SetControlText(m_nRow,m_nCol,Value);
					return TRUE;
				}
				
				for(int idx = 0; idx < att->Items; idx++)
				{
					if( nChar == att->VirtKey[idx])
						break;
				}

				if( idx < att->Items)
				{
					CString Value = style.GetValue();
					
					if( !EncodeValue( att, Value))
					{
						MessageBeep(-1);
					}
					Value.SetAt(idx, (Value[idx] == '0') ? '1' : '0');
					DecodeValue( att, Value);
					SetControlText(m_nRow,m_nCol,Value);
					return TRUE;
				}
			}
		}
		if( nChar < ' ')
		{
			return CGXComboBox::KeyPressed(nMessage, nChar, nRepCnt, flags);
		}

		MessageBeep(-1);
	}
	return FALSE;
}


void CWDayCheckListComboBox::OnSetFocus(CWnd* pOldWnd)
{
	CGXComboBox::OnSetFocus(pOldWnd);

	GetParent()->SetFocus();
}


//-----------------------------------------------------------------------
// ConvertControlTextToValue
//
// This function handles the insert action and setts the "empty" value 
//	on the delete action
//-----------------------------------------------------------------------
BOOL CWDayCheckListComboBox::ConvertControlTextToValue(CString& str, ROWCOL nRow, ROWCOL nCol, const CGXStyle* OldStyle)
{
	ASSERT(OldStyle);

	if(OldStyle)
	{
		const CGXStyle& style = *OldStyle;

		ASSERT(style.GetIncludeItemDataPtr());
		if (style.GetIncludeItemDataPtr())
		{
			const Attributes* att = (const Attributes *) style.GetItemDataPtr();

			ASSERT(att);
			if(att)
			{
				if (str.IsEmpty()) 
					for (int i = 0; i < att->Unchecked.GetSize(); i++)
					{
						if (att->SmartSeparator && 
							!att->Unchecked[i].IsEmpty() && 
							!str.IsEmpty()) str += att->Separator;
						else if (!att->SmartSeparator && i != 0) 
							str += att->Separator;

						str += att->Unchecked[i];
					}
				CString Value = str;
				if( !EncodeValue( att, Value))
				{
					DecodeValue(att, Value);
					MessageBeep(-1);
					return FALSE;
				}
			}
		}
	}
	return TRUE;
}


void CWDayCheckListComboBox::Draw(CDC* pDC, CRect rect, ROWCOL nRow, ROWCOL nCol, const CGXStyle& style, const CGXStyle* pStandardStyle)
{
	ASSERT(pDC != NULL && pDC->IsKindOf(RUNTIME_CLASS(CDC)));
	// ASSERTION-> Invalid Device Context ->END
	ASSERT(nRow <= Grid()->GetRowCount() && nCol <= Grid()->GetColCount());
	// ASSERTION-> Cell coordinates out of range ->END

	ASSERT_VALID(pDC);
	/*
	// if (nRow == m_nRow && nCol == m_nCol && m_bIsActive)
	//     initialize CWnd, make it visible and set focus
	if (nRow == m_nRow && nCol == m_nCol
		&& IsActive() && !Grid()->IsPrinting() )
	{
		SetSel(0,-1);
		CGXEditControl::Draw(pDC, rect, nRow, nCol, style, pStandardStyle);
	}
	else
	*/
		CGXStatic::Draw(pDC, rect, nRow, nCol, style, pStandardStyle);
}

/*
BOOL CWDayCheckListComboBox::LButtonUp(UINT nFlags, CPoint pt, UINT nHitState)
{
	return CGXControl::LButtonUp(nFlags, pt, nHitState);
}

*/

void CWDayCheckListComboBox::SetActive(BOOL bActive)
{
	//m_Active = bActive;
	//CGXComboBox::SetActive(bActive);
}



