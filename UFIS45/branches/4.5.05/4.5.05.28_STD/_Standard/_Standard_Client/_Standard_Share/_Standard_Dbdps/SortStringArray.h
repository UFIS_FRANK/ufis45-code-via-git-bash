// SortStringArray.h: interface for the CSortStringArray class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SORTSTRINGARRAY_H__677A6AC4_5C52_11D3_9823_00001C0411B3__INCLUDED_)
#define AFX_SORTSTRINGARRAY_H__677A6AC4_5C52_11D3_9823_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSortStringArray: public CStringArray 
{
public:
	CSortStringArray();
	virtual ~CSortStringArray();

	void Sort();
private:

	
   BOOL CompareAndSwap(int pos);


};

#endif // !defined(AFX_SORTSTRINGARRAY_H__677A6AC4_5C52_11D3_9823_00001C0411B3__INCLUDED_)
