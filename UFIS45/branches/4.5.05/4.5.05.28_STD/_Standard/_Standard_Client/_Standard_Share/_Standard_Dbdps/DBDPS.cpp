// DBDPS.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <DBDPS.h>
#include <MainFrm.h>
#include <DBDPSDoc.h>
#include <DBDPSView.h>
#include <SetAppDefault.h>
#include <locale.h>

//*** 27.08.99 SHA ***
#include <SortStringArray.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBDPSApp

BEGIN_MESSAGE_MAP(CDBDPSApp, CWinApp)
	//{{AFX_MSG_MAP(CDBDPSApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDBDPSApp construction

CDBDPSApp::CDBDPSApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDBDPSApp object

CDBDPSApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDBDPSApp initialization

BOOL CDBDPSApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	setlocale( LC_ALL, "" ); 


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	if(!SetAppDefaultValues())
		return FALSE;
	
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CDBDPSDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CDBDPSView));
	AddDocTemplate(pDocTemplate);

	// Initialization of the grid library
	GXInit();
	GXSetNewGridLineMode(TRUE);



	//*** SHA2000-03-02
	//*** CHECK THE COMMAND LINE ***
	//*** IF THE APPLICATION IS CALLED BY ANOTHER, KICK THE LOGIN DIALOG TO HELL ***

	CStringArray ogCmdLineArgsArray;
	ogCmdLineArgsArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	CString olDmy;

	//AfxMessageBox (olCmdLine);
		gParameterUser = "";
		gParameterPassword = "";

		if(olCmdLine.GetLength() != 0)
		{

			int ilParamCount = ExtractItemList(olCmdLine,&ogCmdLineArgsArray, ',');

			//*** THE ORDER SHOULD BE APPNAME USER PASSWORD ***
			
			gParameterUser = ogCmdLineArgsArray.GetAt(1);
			gParameterPassword = ogCmdLineArgsArray.GetAt(2);

		}


	// Parse command line for standard shell commands, DDE, file open
	//*** 02.12.1999 SHA ***
	//*** KICKED <PARSE> BECAUSE OF PARAMETERS OF FIPS-MODULE THAT CAN'T BE EVALUATED ***
	CCommandLineInfo cmdInfo;
	//ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// get the active document
	POSITION myPosition = pDocTemplate->GetFirstDocPosition();
	if(myPosition == NULL)
	{
		AfxMessageBox("Application-InitInstance(). No document found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}
	CDocument* myDocument = pDocTemplate->GetNextDoc(myPosition);
	if(myPosition != NULL)
	{
		AfxMessageBox("Application-InitInstance(). Too many documents found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}

	// refreshing of the menu item Open
	if(!RefreshMenu(myDocument))
		return FALSE;

	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

BOOL CDBDPSApp::ExitInstance()
{
	return CWinApp::ExitInstance();
}
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CDBDPSApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSApp commands

//-----------------------------------------------------------------------
// RefreshMenu
//	
//	The function sets the menu items for openning tables in application menu
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 30.06.1999 | Martin Danihel    | Added security functionality
//-----------------------------------------------------------------------
bool CDBDPSApp::RefreshMenu(CDocument* pDocument)
{
	// locate the open submenu
	CMenu* pOpenSubmenu = NULL;
	CMenu* pTopMenu = AfxGetMainWnd()->GetMenu();
	CMenu* pMenu;
	int iPos;
	int iPos2;
	for (iPos = pTopMenu->GetMenuItemCount() - 1; iPos >= 0; iPos--)
	{
		pMenu = pTopMenu->GetSubMenu(iPos);
		if (pMenu && pMenu->GetMenuItemID(0) == -1)	//-1 = submenu
		{
			CMenu* pSubMenu = pMenu->GetSubMenu(0);
			if (pSubMenu && pSubMenu->GetMenuItemID(0) == ID_REFRESH)
			{
				pOpenSubmenu = pSubMenu;
				break;
			}
		}
	}
	if(pOpenSubmenu == NULL)
		return FALSE;

	// first, delete all items
	for (iPos2 = pOpenSubmenu->GetMenuItemCount() - 1; iPos2 >= 0; iPos2--)
		pOpenSubmenu->DeleteMenu(iPos2, MF_BYPOSITION);

	CString myStatus;	// security status

	myStatus = GetSecurityStatus(_T("MAIN"), _T("FILE_OPEN"));
	if(myStatus != SEC_STAT_ACT)	// no law to open any table
	{
		pMenu->EnableMenuItem(0, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
		return TRUE;
	}

	if(MAX_OF_MENU_ITEMS_VIEWS < 1)
	{
		// add only item more
		if(!(pOpenSubmenu->AppendMenu(MF_ENABLED + MF_STRING, ID_VIEW_MORE, "Tables...")))
			return FALSE;

		return TRUE;
	}

	//*** 27.08.99 SHA ***
/*	CSortStringArray	olMenu,olMenuLong;
	CString				olMenuEntry;
	CString				olLongMenu;
	CString				olDmy;
	for (int iMenu = 0; iMenu < gTables.GetSize(); iMenu++)
	{
		olMenuEntry = gTables.GetAt(iMenu);
		gLongTableNames.Lookup(olMenuEntry + gTableExt, olLongMenu);
		olDmy = olLongMenu + olMenuEntry;
		olMenu.Add(olDmy);
		olMenu.Sort();
	}
*/	
	// add menu items from intern structure
	CString myTableName;
	CString *myLongTableName;
	int j = 0;
	for (int i = 0; i < gTables.GetSize(); i++)
	{
		myTableName = gTables.GetAt(i);
		//*** 27.08.99 SHA ***
		//myTableName = olMenu.GetAt(i);

		myStatus = GetSecurityStatus(myTableName + gTableExt, _T("FILE_OPEN"));
		if(myStatus == SEC_STAT_ACT)
		{
			if (!gLongTableNames.Lookup(myTableName + gTableExt, myLongTableName))	// if a long name exists set this
			{
				AfxMessageBox("RefreshMenu(). Long table name not found.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}
			if(!(pOpenSubmenu->AppendMenu(MF_ENABLED + MF_STRING, ID_VIEW_MIN + j,
				*myLongTableName)))
				return FALSE;

			j++;	// added new menu item
			if(j > (ID_VIEW_MAX - ID_VIEW_MIN + 1))
			{
				// amount of possible object id's too few
				AfxMessageBox("RefreshMenu(). Too few number of view item id.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}
			if(j > (MAX_OF_MENU_ITEMS_VIEWS))	// amount of items is more then max amount
			{
				if(!pOpenSubmenu->DeleteMenu(j - 1, MF_BYPOSITION))	// delete last item
					return FALSE;
				if(!pOpenSubmenu->DeleteMenu(j - 2, MF_BYPOSITION))	// delete semi-final item
					return FALSE;
				// add item more
				if(!(pOpenSubmenu->AppendMenu(MF_ENABLED + MF_STRING, ID_VIEW_MORE, "Tables...")))
					return FALSE;

				return TRUE;
			}
		}
	}

	return TRUE;
}


int CDBDPSApp::ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


