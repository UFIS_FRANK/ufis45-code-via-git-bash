//***********************************************************************
// DBDPSDoc.h : interface of the CDBDPSDoc class
//***********************************************************************
#include <Globals.h>
#include <gxall.h>

class CDBDPSDoc : public CDocument
{
protected: // create from serialization only
	CDBDPSDoc();
	DECLARE_DYNCREATE(CDBDPSDoc)
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBDPSDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL


// Implementation
private:
	void SetDefaultPriority();
	void SetSecurityStatus
		(
			LPCTSTR pObjectId,
			LPCTSTR pControlId,
			LPCTSTR pStatus
		);

public:
	virtual ~CDBDPSDoc();
	CIndexMap mIndexMap;					// the index map for sorting
	CGRRecordset mChangingRecord;		// the temporary record
public:
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//*** 15.11.99 SHA ***
	bool CreateLogFile(CString olFile);
	bool CloseLogFile();
	bool WriteLogFile(CString olText);
	
	CStdioFile	outFile;
	bool		outFileOK;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//*** 02.12.1999 SHA ***
	CString GetTokenFromString(CString Delimiter, long TokenNo, CString &FromString);
	void ConvertToArray(CString &olIn, CStringArray &olOut, CString Delimiter, bool &Valid);
	CString GetValue(CString &Fields, CString &Values, CString Delimiter, CString FieldName);
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	bool LoadSysInfo(LPCTSTR pExtension);
	bool LoadTabInfo();
	bool LoadSecFieldInfo();
	bool CheckAndTrim
		(
			LPTSTR &pValue,				
			int pLength,	
			bool pRequired = true,		
			bool pCanBeShorter = true
		);	
	bool CheckAndTrim
		(	
			LPCTSTR TableName,			
			LPCTSTR FieldName,			
			LPCTSTR URNOName,				
			LPCTSTR URNOValue,			
			bool URNOIsCurrent,			
			LPTSTR &pValue,				
			int pLength,					
			bool pRequired = true,		
			bool pCanBeShorter = true,	
			bool pNoMessage = false
		);
	bool FillDataStructure
		(	
			LPCTSTR pTableName,
			LPCTSTR pExtension,
			LPCTSTR pWhere				// the where clause
		);
	void DoSort();
	void ClearDataStructure();
	void ClearIndexMap();
	void ClearRecordset(CGRRecordset *pGRRecordset);
	void ClearChangingRecord();
	void ClearSysInfo();

	bool UpdateRecord(ROWCOL pRow);	// update record in the gGRTable and the database
	bool InsertRecord();					// inserts record into the gGRTable and the database
	bool DeleteRecord(ROWCOL pRow);	// delete record from the gGRTable and the database

	EBoolType BoolTypeToEnum(const CString &pBoolType);
	//h - start
	EBol1Type Bol1TypeToEnum(const CString &pBol1Type);
	//h - end
	//*** 18.11.1999 SHA ***
	EBol2Type Bol2TypeToEnum(const CString &pBol2Type);

	EDBType DBTypeToEnum(const CString &pDBType);
	ELGType LGTypeToEnum(const CString &pLGType);
	ETimeType TimeTypeToEnum(const CString &pTimeType);

	bool TransDBToCell 
		(
			CGRCell *pGRCell,
			CFieldInfo *pFieldInfo,
			LPTSTR pCellValue
		);
	void GetGRCellValue
		(	
			ROWCOL pRow,
			ROWCOL pCol,
			CGXStyle &pStyle
		);
	void CheckValueSetColor
		(
			ROWCOL pCol,
			const CString& pGRValue, 
			CGXStyle &pStyle
		);
	bool CheckValueSetValue
		(
			ROWCOL pCol,
			const CString& pGRValue, 
			CGXStyle &pStyle
		);
	bool ValidGRValue
		(
			ROWCOL pCol,
			const CString& pGRValue, 
			CString& pNewValue
		);
	bool TimeDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue,
			int pTimeZoneDif
		);
	bool DateTimeDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue,
			int pTimeZoneDif
		);
	bool DateDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue
		);
	bool CheckDateTime
		(
			int pYear, 
			int pMonth, 
			int pDay, 
			int pHour, 
			int pMinute
		);
	void WeekCellToDB
		(
			LPCTSTR pCellValue,
			CString &pDBValue
		);
	void MlstCellToDB
		(
			LPCTSTR pCellValue,
			CString &pDBValue,
			CWDayCheckListComboBox::Attributes *pAttr
		);
	bool WeekDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue
		);
	bool MlstDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue,
			CWDayCheckListComboBox::Attributes *pAttr
		);
	bool NumberDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellStrValue,
			double &pCellNumValue,
			int pAfterPoint
		);
	void CopyString
		(	LPTSTR dest,	// destination string
			LPTSTR src,		// source string
			int bts			// copy at most bts bytes
		);		
	bool TimeGRToCell
		(
			const CString &pGRValue, 
			CString &pCellValue
		);
	bool CurrentDateTimeToCell(CString &pCellValue);
	bool DateTimeGRToCell
		(
			const CString &pGRValue, 
			CString &pCellValue
		);
	bool DateGRToCell
		(
			const CString &pGRValue, 
			CString &pCellValue
		);
	bool NumberGRToCell
		(
			const CString &pGRValue, 
			CString &pCellStrValue,
			double &pCellNumValue,
			int pAfterPoint
		);
	bool TimeCellToGR
		(
			const CString &pCellValue, 
			CString &pGRValue
		);
	bool DateTimeCellToGR
		(
			const CString &pCellValue, 
			CString &pGRValue
		);
	void TimeCellToDB
		(
			const CString &pCellValue, 
			CString &pDBValue,
			int pTimeZoneDif
		);
	void DateTimeCellToDB
		(
			const CString &pCellValue, 
			CString &pDBValue,
			int pTimeZoneDif
		);
	bool DateCellToGR
		(
			const CString &pCellValue, 
			CString &pGRValue
		);
	void DateCellToDB
		(
			const CString &pCellValue, 
			CString &pDBValue
		);
	bool ComboDBToCell
		(
			LPCTSTR pDBValue, 
			CString &pCellValue,
			CFieldInfo *pFieldInfo
		);
	bool BoolDBToCell 
		(
			LPCTSTR myDBValue, 
			CString &pCellValue
		);
//h - start
	bool Bol1DBToCell 
		(
			LPCTSTR myDBValue, 
			CString &pCellValue
		);
//h - end
	//*** 18.11.1999 SHA ***
	bool Bol2DBToCell 
	(
		LPCTSTR myDBValue, 
		CString &pCellValue
	);


	void SetStyleColor
		(
			CGXStyle &pStyle,
			ECLType pColor
		);
	void TransCellToDB
		(	
			ROWCOL pCol,
			const CString &pCellValue,
			CString &pDBValue
		);
	bool TransGRToCell
		(	
			ROWCOL pCol,
			const CString &pGRValue,
			CString &pCellStrValue,
			double &pCellNumValue
		);
	void CDBDPSDoc::TransCellToGR
		(	
			CFieldInfo *pFieldInfo,
			CGRCell *pGRCell,
			CGXStyle &pStyle
		);
	int UpdateRecordDB(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	int InsertRecordDB(LPCTSTR, LPCTSTR, LPCTSTR);
	int DeleteRecordDB(LPCTSTR, LPCTSTR);
	int InsertRecordBc
		(
			LPCTSTR pTableFields,
			LPCTSTR pFieldsValue
		);
	int UpdateRecordBc
		(
			LPCTSTR pTableFields,
			LPCTSTR pFieldsValue,
			LPCTSTR pWhereClause
		);
	int DeleteRecordBc
		(
			LPCTSTR pWhereClause
		);
	bool GetLocaleSettings();
	void ReplaceCellByDefault
		(
			CGRCell *pGRCell,
			CFieldInfo *pFieldInfo,
			bool pInvalid
		);
	void CDBDPSDoc::EmptyDBToCell
		(
			CFieldInfo *pFieldInfo,
			CGRCell *pGRCell
		);
	bool CDBDPSDoc::SetDefaultValue
		(
			CFieldInfo *pFieldInfo, 
			bool &pDefaultValueProblem
		);
	bool GetChoiceList(CFieldInfo *pFieldInfo);
	bool GetMultichoiceAtt
		(
			const CString &pComboTabField,
			CWDayCheckListComboBox::Attributes *&pMultichoiceAtt
		);
	void ProcessBc
		(
			int pDdxType,
			void * pDataPointer,
			CString &pInstanceName
		);
	BOOL LoadSecurityInfo();
	void ClearSecurityInfo();
	void AddSecInfo
		(
			CSecurityObjectCollection *pObjectCollection,
			CString pLabel,
			CString pType,
			CString pStatus,
			CString pControlId
		);
	BOOL RegisterApp();
	BOOL RegisterModules
		(
			LPCTSTR pRegisterText
		);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDBDPSDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
