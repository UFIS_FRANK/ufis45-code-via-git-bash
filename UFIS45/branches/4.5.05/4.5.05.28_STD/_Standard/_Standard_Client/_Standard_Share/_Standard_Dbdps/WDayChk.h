// WDayCheckListComboBox.h: interface for the CWDayCheckListComboBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WDAYCHECKLISTCOMBOBOX_H__79B3A471_8544_11D2_B470_00A024F0CD27__INCLUDED_)
#define AFX_WDAYCHECKLISTCOMBOBOX_H__79B3A471_8544_11D2_B470_00A024F0CD27__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/////////////////////////////////////////////////////////////////////////////
// CWDayCheckListComboBox

class CWDayCheckListComboLBox : public CCheckListBox
{
	DECLARE_DYNAMIC(CWDayCheckListComboLBox)

// Construction
public:
	CWDayCheckListComboLBox(CWnd* pMsgWnd);
	virtual ~CWDayCheckListComboLBox();

	void Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

// Operations
public:
	void SetBackColor(COLORREF rgb);

// Implementation
protected:
	// Attributes
	BOOL    m_bKeyDown;
	BOOL    m_bLButtonDown;
	CWnd*   m_pMsgWnd;
	BOOL    m_bColor;
	COLORREF m_rgbBack;
	int     m_nOldSel;
	BOOL    m_bAlNum;

protected:
	// Generated message map functions
	//{{AFX_MSG(CWDayCheckListComboLBox)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

// CGXComboBox is the control which encapsulates
// the combobox behaviour.

class CWDayCheckListComboBox: public CGXComboBox
{
	DECLARE_CONTROL(CWDayCheckListComboBox)
	DECLARE_DYNAMIC(CWDayCheckListComboBox)

public:
	// the completely new structure determinig the control behaviour
	typedef struct Attributes
	{
		Attributes(const int i, const CStringArray& n, const CString &s,
				const bool ss,
				const CStringArray& ch, const CStringArray& uch,
				const CUIntArray& vk, const CString& d)
			:Items(i),
			 SmartSeparator(ss),
			 Name(n),
			 Separator(s),
			 Checked(ch),
			 Unchecked(uch),
			 VirtKey(vk),
			 DefaultCode(d)
		{
#ifdef _DEBUG
			ASSERT(Items > 0);
			ASSERT(Name.GetSize() == Items);
			ASSERT(Checked.GetSize() == Items || !Checked.GetSize());
			ASSERT(Unchecked.GetSize() == Items || !Unchecked.GetSize());
			ASSERT(VirtKey.GetSize() == Items || !VirtKey.GetSize());
			ASSERT(DefaultCode.GetLength() == Items || !DefaultCode.GetLength());
			ASSERT(Checked.GetSize() || Unchecked.GetSize());

			if(Checked.GetSize() && Unchecked.GetSize())
			{
				for( int i=0; i < Checked.GetSize(); i++)
					if( Checked[i] == Unchecked[i])
						ASSERT(0);
			}

			if(!Checked.GetSize())
			{
				for( int i=0; i < Unchecked.GetSize(); i++)
				{
					for( int j = i + 1; j < Unchecked.GetSize(); j++)
						if( Unchecked[i] == Unchecked[j])
							ASSERT(0);
					ASSERT(!(Unchecked[i].IsEmpty()));
				}
			}

			if(!Unchecked.GetSize())
			{
				for( int i=0; i < Checked.GetSize(); i++)
				{
					for( int j = i + 1; j < Checked.GetSize(); j++)
					{
						if( Checked[i] == Checked[j])
							ASSERT(0);
					}
					ASSERT(!(Checked[i].IsEmpty()));
				}
			}
#endif
		};

		const int Items;//count of check list box items, must be greater than 0
						// (this is the dimension of arrays in this structure too)

		const CStringArray&	Name;	//mandatory: array of names of check list box items

						// control's formating for unselected or folded state
						// (in the grid appearance)
		const CString&	Separator;		//items separator or empty
		const bool SmartSeparator ;
//		{	False=0,			//immediate separator repeating will not be avoided
//			CheckLead,			//array Checked will be used as guide to interpreting control's value
//			UncheckLead			//array Unchecked will be used as guide to interpreting control's value
//		} 	

		const CStringArray& Checked;	//array of abbreviations for checked check list box items or empty array
		const CStringArray& Unchecked;	//array of abbreviations for unchecked check list box items or empty array

						// control's hot keys (change check state of appropriate item when
						// key is pressed on selected folded control)
		const CUIntArray& VirtKey;	// array of virtual key codes or empty array

		const CString& DefaultCode;
						// Encoded default value of control 
						// String of characters '0' (unchecked) or '1' (checked).
						// Array "DefaultCode" must be either "Items" character length or empty.

		// more structure constraints (note: not all these ones are checked at debug version only):
		//
		//	1) At least one array "Checked" or "Unchecked" must not be empty.
		//
		//	2) If both the arrays "Checked" and "Unchecked" are not empty, then the appropriate
		//		items of these ones must not be identical.
		//
		//  3) If array "Checked" is undefined:
		//
		//		3.1) then the items of array "Unchecked" must be unique.
		//
		//      3.3) In the array "Unchecked" must not exist empty item.
		//
		//  4) If array "Unchecked" is undefined:
		//
		//		4.1) then the items of array "Checked" must be unique.
		//
		//      4.3) In the array "Checked" must not exist empty item.
		//
		//	5) All this structure arrays must has either "Items" elements or must be empty
		//
	};

public:
	// Constructor & Destructor
	CWDayCheckListComboBox(CGXGridCore* pGrid, UINT nEditID = 0, UINT nListBoxID = 0);
	virtual ~CWDayCheckListComboBox();

	// Encode control's formating for unselected or folded state into array
	// of character '0' or '1'.
	// In case of error in control's format the function EncodeValue returns FALSE
	// and the value defined by attr->DefaultCode will be used.
	bool EncodeValue( const Attributes* attr, CString& code);
	void DecodeValue( const Attributes* attr, CString& value);


//jeje	virtual BOOL OnProcessChar(UINT nChar, UINT nRepCnt, UINT flags);

	virtual CWnd* CreateListBox(CWnd* pParentWnd, UINT nID);

//jeje	virtual BOOL GetControlText(CString& strResult, ROWCOL nRow, ROWCOL nCol, LPCTSTR pszRawValue, const CGXStyle& style);

virtual BOOL ConvertControlTextToValue(CString& str, ROWCOL nRow, ROWCOL nCol, const CGXStyle* pOldStyle);

	virtual void Draw(CDC* pDC, CRect rect, ROWCOL nRow, ROWCOL nCol, const CGXStyle& style, const CGXStyle* pStandardStyle);
//jeje  virtual BOOL GetValue(CString& strResult);

	// Override these methods if you don't want to fill the list from the coice-list
	virtual void OnStoreDroppedList(CListBox* lbox);
	virtual void OnFillDroppedList(CListBox* lbox);

	virtual BOOL KeyPressed(UINT nMessage, UINT nChar, UINT nRepCnt = 1, UINT flags = 0);

	virtual DWORD CalcEditStyle(LPRECT rectNP, int& dyHeight);

//	virtual BOOL LButtonUp(UINT nFlags, CPoint pt, UINT nHitState);
	virtual void SetActive(BOOL bActive);

	int m_nCount;

	// Generated message map functions
protected:

	//BOOL m_Active;

	//{{AFX_MSG(CWDayCheckListComboBox)
	afx_msg LRESULT OnListBoxEnd(WPARAM, LPARAM);
	afx_msg LRESULT OnListBoxCancel(WPARAM, LPARAM);
	afx_msg LRESULT OnListBoxChanged(WPARAM, LPARAM);
	afx_msg LRESULT OnListBoxKillFocus(WPARAM, LPARAM);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void EncodeValueSupportRoutine(const CString &value,
		const Attributes *attr,
		const CStringArray& LeadingAbbrev, const char LeadingCode, CString &code);

	void DecodeValueSupportRoutine(const CString &code,
		const Attributes *attr,
		const CStringArray& LeadingAbbrev, const char LeadingCode, CString &value);
};

#endif // !defined(AFX_WDAYCHECKLISTCOMBOBOX_H__79B3A471_8544_11D2_B470_00A024F0CD27__INCLUDED_)
