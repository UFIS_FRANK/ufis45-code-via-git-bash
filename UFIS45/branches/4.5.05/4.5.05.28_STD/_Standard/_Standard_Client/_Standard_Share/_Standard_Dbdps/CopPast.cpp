//***********************************************************************
// CopPast.cpp 
//***********************************************************************
#include <stdafx.h>
#include <CopPast.h>

//-----------------------------------------------------------------------
// CCopPast()
//	
//	Constructor
//-----------------------------------------------------------------------
CCopPast::CCopPast()
{
	mCanPastRow = mCanPastRegion = false;
}

//-----------------------------------------------------------------------
// ~CCopPast()
//	
//	Destructor
//-----------------------------------------------------------------------
CCopPast::~CCopPast()
{
	Clear();
}

//-----------------------------------------------------------------------
// Clear()
//	
//	The function clears the mCells 
//-----------------------------------------------------------------------
void CCopPast::Clear()
{
	mCanPastRow = false;
	mCanPastRegion = false;

	for (int i = 0; i < mStyles.GetSize(); i++) 
		delete mStyles.GetAt(i);				// delete the style 
	mStyles.RemoveAll();						// remove all the items

	ClearUndo();
}

//-----------------------------------------------------------------------
// ClearUndo()
//	
//	The function clears the mCells 
//-----------------------------------------------------------------------
void CCopPast::ClearUndo()
{
	mCanUndoPastRegion = false;
	mIsInUndoTransaction = false;
	mNowUndoUpdate = false;

	for (int i = 0; i < mUndoStyles.GetSize(); i++) 
		delete mUndoStyles.GetAt(i);				// delete the undo style 
	mUndoStyles.RemoveAll();						// remove all the items

	mUndoRows.RemoveAll();
}
	