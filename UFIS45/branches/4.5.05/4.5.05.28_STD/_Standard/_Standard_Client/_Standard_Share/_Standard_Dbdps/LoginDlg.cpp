// LoginDlg.cpp : implementation file
//

#include <stdafx.h>
#include <dbdps.h>
#include <LoginDlg.h>
#include <Globals.h>

#ifdef ABBCCS_RELEASE
#include <UfisCEDA.h>
#else 
#include <UfisSWH.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define YELLOW  RGB(255, 255,   0)
#define RED     RGB(255,   0,   0)

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog


CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoginDlg)
	m_EditUserName = _T("");
	m_EditPassword = _T("");
	//}}AFX_DATA_INIT
}


void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginDlg)
	DDX_Text(pDX, IDC_EDIT_USER_NAME, m_EditUserName);
	DDX_Text(pDX, IDC_EDIT_PASSWORD, m_EditPassword);
	//}}AFX_DATA_MAP

	//*** 17.11.99 SHA ***
	DDX_Control(pDX, IDC_EDIT_USER_NAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_PasswordCtrl);


}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginDlg)
	ON_BN_CLICKED(ID_MY_OK, OnMyOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	//*** 17.11.99 SHA ***
	CString olLoginText;
	olLoginText.Format("Login DBDPS - Server %s / %s",gHostName,gHostType);
	SetWindowText(olLoginText);

	m_LoginCount = 0;

	
	//m_UsernameCtrl.SetTypeToString("x(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	//m_PasswordCtrl.SetTypeToString("x(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);


	m_PasswordCtrl.SetTextErrColor(RED);

	m_UsernameCtrl.SetFocus();

	//*** 17.11.99 SHA ***
	//*** WRITE FILEVERSION ***
	CString olVersion;
	olVersion = "Version " + CString(VERSION_NO) + " (" + VERSION_DATE +")";
	GetDlgItem(IDC_VERSION)->SetWindowText(olVersion);

	//SHA2000-03-02
	if (gParameterPassword!="" && gParameterUser!="")
	{
		m_PasswordCtrl.SetWindowText(gParameterPassword);
		m_UsernameCtrl.SetWindowText(gParameterUser);
		OnMyOk();
	}
	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnMyOk() 
{
	UpdateData(TRUE);
	int ret = Login(m_EditUserName, m_EditPassword);

	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("Login(). ") + gErrorMessage + POINT_CHAR);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		if(!_tcscmp(gErrorMessage, _T("LOGINERROR INVALID_APPLICATION")) || 
			!_tcscmp(gErrorMessage, _T("LOGINERROR DISABLED_APPLICATION")) ||
			!_tcscmp(gErrorMessage, _T("LOGINERROR EXPIRED_APPLICATION")))
					EndDialog(IDABORT);
		if(++m_LoginCount >= MAX_LOGIN)
			EndDialog(IDABORT);
	}
	else
		EndDialog(IDOK); // This value is returned by DoModal!

	return;
}

//-----------------------------------------------------------------------
// Login
//	(
//		LPCTSTR pUserName,
//		LPCTSTR pPassword
//	)
//	
//	The function checks the user's autorization and returns a profile map.
//-----------------------------------------------------------------------
int CLoginDlg::Login
	(
		LPCTSTR pUserName,
		LPCTSTR pPassword
	)
{
	gErrorMessage.Empty();

	TCHAR myData[1000];

	ASSERT(_tcslen(pUserName) + _tcslen(pPassword) + _tcslen(APP_NAME) <= (sizeof(myData) - 2));
	_stprintf(myData, "%s,%s,%s", pUserName, pPassword, APP_NAME);

	int ret = DBDPSCallCeda
					(
						UFIS_GPR,
						gTableExt,
						NULL,
						NULL,
						_T("USID,PASS,APPL"),
						myData
					);
	if(ret)
		return ret;

	gUserName = pUserName;

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR tempDataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the SYSTAB
	int myBufLen = _tcslen(tempDataPtr);

	gSecPtr = (TCHAR*)GlobalAlloc(GMEM_FIXED, (myBufLen + 1) * sizeof(TCHAR));
	if (!gSecPtr)
	{
		AfxMessageBox(_T("Login(). Error on memory allocation. "), MB_OK + MB_ICONSTOP);
		return false;
	}
	_tcscpy(gSecPtr, tempDataPtr);
	gSecRowCount = GetNumberOfLines(UFIS_BUF);

	return ret;
}
