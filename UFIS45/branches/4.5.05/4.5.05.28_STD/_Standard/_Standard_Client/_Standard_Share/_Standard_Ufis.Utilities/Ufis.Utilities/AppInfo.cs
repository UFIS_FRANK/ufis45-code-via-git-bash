﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;

namespace Ufis.Utilities
{
    /// <summary>
    /// Represents the class for information on the application.
    /// </summary>
    public class AppInfo
    {
        /// <summary>
        /// Gets or sets the product code.
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// Gets or sets the product title.
        /// </summary>
        public string ProductTitle { get; set; }
        /// <summary>
        /// Gets or sets the application version.
        /// </summary>        
        public string Version { get; set; }
        /// <summary>
        /// Gets or sets the application registration string.
        /// </summary>
        public string RegistrationString { get; set; }
        /// <summary>
        /// Gets or sets the application image.
        /// </summary>
        public BitmapImage Image { get; set; }
        /// <summary>
        /// Gets or sets the application description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of Ufis.Utilities.AppInfo class.
        /// </summary>
        public AppInfo() { }
    }
}
