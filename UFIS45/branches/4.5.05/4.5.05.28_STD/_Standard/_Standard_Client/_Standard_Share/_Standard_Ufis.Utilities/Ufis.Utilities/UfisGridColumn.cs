﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Utils;
using System.Windows;
using DevExpress.Xpf.Editors.Settings;

namespace Ufis.Utilities
{
    /// <summary>
    /// Enumerates possible types of in-place editors used to edit cell values.
    /// </summary>
    public enum UfisGridColumnSettingsType { Default, DateTime, Check, Combo, CellTemplated }

    /// <summary>
    /// Represents the object which provides the information on how to render the DevExpress.Xpf.Grid.GridColumn
    /// in DevExpress.Xpf.Grid.GridControl
    /// </summary>
    public class UfisGridColumn
    {
        private string _name;
        private string _fieldName;
        private string _header;
        private int _width = 100;
        private bool _visible = true;
        private UfisGridColumnSettingsType _settings = UfisGridColumnSettingsType.Default;
        private string _displayFormat;
        private DefaultBoolean _allowEditing = DefaultBoolean.Default;
        private HorizontalAlignment _horizontalHeaderContentAlignment = HorizontalAlignment.Center;
        private EditSettingsHorizontalAlignment _horizontalContentAlignment = EditSettingsHorizontalAlignment.Default;
        
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                    return _fieldName;
                else
                    return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string FieldName 
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }
        
        public string Header 
        {
            get
            {
                if (_header == null)
                    return Name;
                else
                    return _header;
            }
            set
            {
                _header = value;
            }
        }
        
        public int Width 
        {
            get { return _width; }
            set { _width = value; } 
        }

        public bool Visible 
        {
            get { return _visible; }
            set { _visible = value; }        
        }

        public UfisGridColumnSettingsType Settings 
        {
            get { return _settings; }
            set { _settings = value; }
        }

        public string DisplayFormat
        {
            get { return _displayFormat; }
            set { _displayFormat = value; }
        }

        public DefaultBoolean AllowEditing
        {
            get { return _allowEditing; }
            set { _allowEditing = value; }
        }

        public HorizontalAlignment HorizontalHeaderContentAlignment
        {
            get { return _horizontalHeaderContentAlignment; }
            set { _horizontalHeaderContentAlignment = value; }
        }

        public EditSettingsHorizontalAlignment HorizontalContentAlignment
        {
            get { return _horizontalContentAlignment; }
            set { _horizontalContentAlignment = value; }
        }

        public object ListSource { get; set; }
        
        public string ListDisplayMember { get; set; }
        
        public string ListValueMember { get; set; }

    }

    /// <summary>
    /// Represents the object which the object which uses date editor in the DevExpress.Xpf.Grid.GridColumn
    /// in DevExpress.Xpf.Grid.GridControl
    /// </summary>
    public class UfisGridDateTimeColumn : UfisGridColumn
    {
        public new UfisGridColumnSettingsType Settings
        {
            get { return base.Settings; }
        }

        public UfisGridDateTimeColumn()
        {
            base.Settings = UfisGridColumnSettingsType.DateTime;
            DisplayFormat = "dd.MM.yyyy HH:mm";
        }
    }

    /// <summary>
    /// Represents the object which uses combo editor in the DevExpress.Xpf.Grid.GridColumn
    /// in DevExpress.Xpf.Grid.GridControl
    /// </summary>
    public class UfisGridComboColumn : UfisGridColumn
    {
        public new UfisGridColumnSettingsType Settings
        {
            get { return base.Settings; }
        }

        public UfisGridComboColumn()
        {
            base.Settings = UfisGridColumnSettingsType.Combo;
        }
    }
}
