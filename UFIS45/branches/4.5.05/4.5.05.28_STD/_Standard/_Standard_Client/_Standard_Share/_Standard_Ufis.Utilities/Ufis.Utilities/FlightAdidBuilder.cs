﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Data;

namespace Ufis.Utilities
{
    /// <summary>
    /// Provides functionality to create the flight ADID list.
    /// </summary>
    public class FlightAdidBuilder
    {
        private static IList<EntCodeName> _defaultFlightAdidList;

        /// <summary>
        /// Gets the default flight ADID list.
        /// </summary>
        /// <returns>List of flight ADID.</returns>
        public static IList<EntCodeName> GetDefaultFlightAdidList()
        {
            if (_defaultFlightAdidList == null)
                _defaultFlightAdidList = GetFlightAdidList(FlightAdid.Unspecified);

            return _defaultFlightAdidList;
        }

        /// <summary>
        /// Gets the flight ADID list from flight ADID enumeration values.
        /// </summary>
        /// <param name="flightAdids">Flight ADID enumeration values.</param>
        /// <returns>List of flight ADID.</returns>
        public static IList<EntCodeName> GetFlightAdidList(FlightAdid flightAdids)
        {
            List<EntCodeName> _flightAdidList = new List<EntCodeName>();

            foreach (FlightAdid adid in Enum.GetValues(typeof(FlightAdid)))
            {
                if (adid != FlightAdid.Unspecified)
                {
                    if (flightAdids == FlightAdid.Unspecified || ((flightAdids & adid) == adid))
                    {
                        _flightAdidList.Add(CreateFlightAdidEntity(adid));
                    }
                }
            }

            return _flightAdidList;
        }

        /// <summary>
        /// Create flight ADID entity based on flight ADID enumeration value.
        /// </summary>
        /// <param name="flightAdid">Flight ADID enumeration value.</param>
        /// <returns>Flight ADID entity.</returns>
        public static EntCodeName CreateFlightAdidEntity(FlightAdid flightAdid)
        {
            string strName = Enum.GetName(typeof(FlightAdid), flightAdid);

            return new EntCodeName()
                {
                    Code = strName.Substring(0, 1),
                    Name = strName
                };
        }
    }
}
