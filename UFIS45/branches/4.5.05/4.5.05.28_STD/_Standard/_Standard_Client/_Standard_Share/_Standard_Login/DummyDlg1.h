//{{AFX_INCLUDES()
#include "ufiscom.h"
//}}AFX_INCLUDES
#if !defined(AFX_DUMMYDLG1_H__8CE156DA_0B58_45B7_B783_13BC6F990A24__INCLUDED_)
#define AFX_DUMMYDLG1_H__8CE156DA_0B58_45B7_B783_13BC6F990A24__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DummyDlg1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDummyDlg dialog

class CDummyDlg : public CDialog
{
// Construction
public:
	CDummyDlg(CWnd* pParent = NULL);   // standard constructor

	IDispatch *GetUfisComCtrl();
// Dialog Data
	//{{AFX_DATA(CDummyDlg)
	enum { IDD = IDD_DUMMY };
	CUfisCom	m_UfisCom;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDummyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDummyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUMMYDLG1_H__8CE156DA_0B58_45B7_B783_13BC6F990A24__INCLUDED_)
