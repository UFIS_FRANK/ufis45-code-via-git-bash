// Aatstatic.cpp : implementation file
//
// Written By:
// Pichet on some day in the beginning time of the project -- Damkerng
//
// Modification History:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	Fix the painting routine which paints outside the window.
//	The fixed places will be commented as "-- this will paint outside the window".
#include "StdAfx.h"
#include "Aatstatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// AatStatic
 



AatStatic::AatStatic()
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	pomFont = new CFont;
    lolFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(lolFont.lfFaceName, "MS Sans Serif");
    pomFont->CreateFontIndirect(&lolFont);

	lmBkColor = RGB(192, 192, 192);
	lmTextColor = RGB(0,0,0);
	lmHilightColor = RGB(255,0,0);
}

AatStatic::~AatStatic()
{
	pomFont->DeleteObject();
	delete pomFont;
}


BEGIN_MESSAGE_MAP(AatStatic, CStatic)
    //{{AFX_MSG_MAP(AatStatic)
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// AatStatic message handlers


bool AatStatic::OnEraseBkgnd(CDC* pDC) 
{
#if	0
    CRect olClipRect;
    GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
#endif    
    return true;
}

void AatStatic::OnPaint() 
{
    CPaintDC dc(this); // device context for painting

	dc.SetBkMode(TRANSPARENT);
    
    // TODO: Add your message handler code here
    // Do not call CStatic::OnPaint() for painting messages

    CFont *polOldFont = (CFont *) dc.SelectObject(pomFont);
    CRect olClientRect; GetClientRect(&olClientRect);

   
    CString olWindowText;
    GetWindowText(olWindowText);
    
    dc.SetBkColor(lmBkColor);
    dc.SetTextColor(lmTextColor);
    
    int ilLeftPos;
    DWORD llStyle = GetStyle();
    
    // all static have SS_LEFT so we check SS_LEFT last
    if ((llStyle & SS_RIGHT) == SS_RIGHT)
    {
        ilLeftPos = olClientRect.right - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx - 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }
    else if ((llStyle & SS_CENTER) == SS_CENTER)
    {
        ilLeftPos = (olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2;
        
        int ilSlashPosition = olWindowText.Find('/');
        if (ilSlashPosition != -1)
        {                                          
            ilSlashPosition++;
        
            CString olLeftText = olWindowText.Left(ilSlashPosition);
            CString olRightText = olWindowText.Mid(ilSlashPosition);
        
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cy) / 2, olLeftText);

            dc.SetTextColor(lmHilightColor);
        
            ilLeftPos = ((olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2) +
                dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cx;
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olRightText);
        }
        else
        {
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
        }
    }
    else if ((llStyle & SS_LEFT) == SS_LEFT)
    {
        ilLeftPos = 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }

    dc.SelectObject(polOldFont);
}
