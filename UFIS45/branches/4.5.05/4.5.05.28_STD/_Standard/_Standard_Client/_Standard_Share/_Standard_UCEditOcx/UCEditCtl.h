#if !defined(AFX_UCEDITCTL_H__B4194632_6A3E_4E4A_AA30_6EAF1C196C2C__INCLUDED_)
#define AFX_UCEDITCTL_H__B4194632_6A3E_4E4A_AA30_6EAF1C196C2C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UCEditCtl.h : Declaration of the CUCEditCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl : See UCEditCtl.cpp for implementation.

class CUCEditCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUCEditCtrl)

// Constructor
public:
	CUCEditCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUCEditCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual BOOL OnGetPredefinedValue(DISPID dispid, DWORD dwCookie, VARIANT* lpvarOut);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CUCEditCtrl();

	DECLARE_OLECREATE_EX(CUCEditCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUCEditCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUCEditCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUCEditCtrl)		// Type name and misc status

	// Subclassed control support
	BOOL IsSubclassedControl();
	LRESULT OnOcmCommand(WPARAM wParam, LPARAM lParam);

// Message maps
	//{{AFX_MSG(CUCEditCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUCEditCtrl)
	afx_msg LPUNKNOWN RetrieveUCText();
	afx_msg void AssignUCText(LPUNKNOWN pupByteArr);
	afx_msg void SetUCText(LPUNKNOWN pupByteArr, short spSize);
	afx_msg short GetUCText(LPUNKNOWN pupByteArr);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CUCEditCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

private:
	BYTE* plByteArr;
	CString slText;
	//LPCWSTR slText;
// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CUCEditCtrl)
	dispidRetrieveUCText = 1L,
	dispidAssignUCText = 2L,
	dispidSetUCText = 3L,
	dispidGetUCText = 4L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCEDITCTL_H__B4194632_6A3E_4E4A_AA30_6EAF1C196C2C__INCLUDED)
