//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BCComServer.rc
//
#define IDS_PROJNAME                    100
#define IDR_BcComServer                 100
#define IDR_BCCOMATL                    101
#define IDD_MAINDLG                     201
#define IDC_CONNECTION                  201
#define IDC_BCLIST                      202
#define IDC_BROADCASTS                  203
#define IDD_INFODLG                     203
#define IDC_SAVELISTAS                  204
#define IDD_CLIENTINFODLG               204
#define IDC_BUTTON_INFO                 205
#define IDC_LIST1                       206
#define IDC_BUTTON_STOPTHREAD           207
#define IDC_CLIENT_INFO                 208

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        203
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         209
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
