#if !defined(AFX_MONITORDLG_H__2C13D3C9_3131_4C1D_AA76_F6F9F3BABD6D__INCLUDED_)
#define AFX_MONITORDLG_H__2C13D3C9_3131_4C1D_AA76_F6F9F3BABD6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MonitorDlg.h : header file
//
#include "resource.h"

#define WM_THREADFIREEVENT WM_USER+101

/////////////////////////////////////////////////////////////////////////////
// MonitorDlg dialog

class CBcComAtl;
typedef struct _StringDescriptor STR_DESC;

class MonitorDlg : public CDialog
{
// Construction
public:
	MonitorDlg(CWnd* pParent = NULL);   // standard constructor

	void TransmitFilter();
	bool IsMultiThreaded() { return this->bmMultithreaded;}
	bool AddToSpooler(const STR_DESC& ropBc);
	bool Fire_OnLostBc(int ilErr);
	bool Fire_OnLostBcEx(const CComBSTR& ropMsg);

// Dialog Data
	//{{AFX_DATA(MonitorDlg)
	enum { IDD = IDD_MAINDLG };
	CStatic	m_Connection;
	CListBox	m_BcList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MonitorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MonitorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnSavelistas();
	afx_msg void OnClose();
	afx_msg	LRESULT OnFireEventForThread(WPARAM wParam,LPARAM lParam);
	afx_msg void OnButtonInfo();
	afx_msg void OnClientInfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void		BC_To_Client();
	bool		bmMultithreaded;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MONITORDLG_H__2C13D3C9_3131_4C1D_AA76_F6F9F3BABD6D__INCLUDED_)
