VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form LookupForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lookup"
   ClientHeight    =   7695
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8205
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "LookupForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7695
   ScaleWidth      =   8205
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lvwLookup 
      Height          =   6975
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   12303
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ColHdrIcons     =   "imlList"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.TabStrip tbsLookup 
      Height          =   7335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   12938
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlList 
      Left            =   1680
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "LookupForm.frx":038A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "LookupForm.frx":06DC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlLookup 
      Left            =   4920
      Top             =   2520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "LookupForm.frx":0A2E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "LookupForm.frx":0DC8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tbrLookup 
      Align           =   2  'Align Bottom
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   7335
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   635
      ButtonWidth     =   1561
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "imlLookup"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Select"
            Key             =   "OK"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Cancel"
            Key             =   "Cancel"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
End
Attribute VB_Name = "LookupForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public DataSource As Variant
Public ColumnHeaders As String
Public Columns As String
Public ColumnWidths As String
Public KeyColumns As String
Public WindowWidth As Single
Public WindowHeight As Single
Public Filters As String
Public FilterIndex As Integer
Public SelectedData As String
Public AllowMultiSelection As Boolean
Public IsOnTop As Boolean
Public LookupFont As StdFont

Private IsActivated As Boolean

Private Sub OKSelect(ByVal Index As Integer)
    Dim lsi As ListItem
    
    If AllowMultiSelection Then
        SelectedData = ""
        For Each lsi In lvwLookup(Index).ListItems
            If lsi.Checked Then
                'SelectedData = SelectedData & "," & Trim(Replace(lsi.Key, Chr(0), ""))
                SelectedData = SelectedData & "," & Trim(lsi.Tag)
            End If
        Next lsi
        If SelectedData = "" Then
            Set lsi = lvwLookup(Index).SelectedItem
            If lsi Is Nothing Then
                SelectedData = ""
            Else
                'SelectedData = Trim(Replace(lsi.Key, Chr(0), ""))
                SelectedData = Trim(lsi.Tag)
            End If
        Else
            SelectedData = Mid(SelectedData, 2)
        End If
    Else
        Set lsi = lvwLookup(Index).SelectedItem
        If lsi Is Nothing Then
            SelectedData = ""
        Else
            'SelectedData = Trim(Replace(lsi.Key, Chr(0), ""))
            SelectedData = Trim(lsi.Tag)
        End If
    End If
    
    Me.Hide
    IsActivated = False
End Sub

Private Sub CancelSelect()
    SelectedData = ""
    
    Me.Hide
    IsActivated = False
End Sub

Private Sub FillInGrid()
    Dim ColHdr As ColumnHeader
    Dim lsi As ListItem
    Dim aColHdrs As Variant
    Dim aColWidths As Variant
    Dim i As Integer
    Dim aKeys As Variant
    Dim vKey As Variant
    Dim sKey As String
    Dim fld As ADODB.Field
    Dim sColumns As String
    Dim aColumns As Variant
    Dim oURecordset As UFISRecordset
    Dim rsBrowse As ADODB.Recordset
    Dim Index As Integer

    Index = 0
    
    lvwLookup(Index).CheckBoxes = AllowMultiSelection
    lvwLookup(Index).ColumnHeaders.Clear
    lvwLookup(Index).ListItems.Clear
    
    aColHdrs = Split(ColumnHeaders, ",")
    aColWidths = Split(ColumnWidths, ",")
    For i = 0 To UBound(aColHdrs)
        Set ColHdr = lvwLookup(Index).ColumnHeaders.Add(, , aColHdrs(i))
        If i <= UBound(aColWidths) Then
            ColHdr.Width = aColWidths(i)
            
            If i = 0 And AllowMultiSelection Then
                ColHdr.Width = ColHdr.Width + Me.TextWidth("WWW")
            End If
        End If
    Next i
    
    Select Case VarType(DataSource)
        Case vbString
            If DataSource <> "" Then
                Set oURecordset = New UFISRecordset
                oURecordset.ColumnList = ColumnHeaders
                oURecordset.ConvertSpecialChars = True
                oURecordset.CheckOldVersionWhenConverting = True
                Set rsBrowse = oURecordset.CreateRecordset(, DataSource)
                Set oURecordset = Nothing
            End If
        Case vbObject
            If TypeOf DataSource Is ADODB.Recordset Then
                Set rsBrowse = DataSource
            End If
    End Select
    
    If Not (rsBrowse Is Nothing) Then
        
        Dim vntFilters As Variant
        Dim vntFilter As Variant
        Dim vntFilterDetails As Variant
        Dim strFilterName As String
        Dim strFilterCriteria As String
        
        vntFilters = Split(Filters, "|")
        
        If FilterIndex < 0 Or FilterIndex > UBound(vntFilters) Then
            FilterIndex = 0
        End If
        
        If UBound(vntFilters) = -1 Then vntFilters = Split(",", "|")
        
        For Each vntFilter In vntFilters
            vntFilterDetails = Split(vntFilter, ",")
            
            strFilterName = vntFilterDetails(0)
            strFilterCriteria = vntFilterDetails(1)
            
            If lvwLookup.UBound >= Index Then
                tbsLookup.Tabs.Item(Index + 1).Caption = strFilterName
            Else
                tbsLookup.Tabs.Add , , strFilterName

                Load lvwLookup(Index)
                lvwLookup(Index).ZOrder
            End If
            lvwLookup(Index).Visible = (FilterIndex = Index)
            lvwLookup(Index).ListItems.Clear
            
            If strFilterCriteria <> "" Then
                rsBrowse.Filter = strFilterCriteria
            End If
            
            If Not (rsBrowse.BOF And rsBrowse.EOF) Then rsBrowse.MoveFirst
            Do While Not rsBrowse.EOF
                sKey = ""
                aKeys = Split(KeyColumns, ",")
                For Each vKey In aKeys
                    sKey = sKey & "," & rsBrowse.Fields(vKey).Value
                Next vKey
                If sKey <> "" Then sKey = Mid(sKey, 2)
                
                'If IsNumeric(sKey) Then sKey = Chr(0) & sKey
    
                sColumns = Columns
                If sColumns = "" Then
                    For Each fld In rsBrowse.Fields
                        sColumns = sColumns & "," & fld.Name
                    Next fld
                    If sColumns <> "" Then sColumns = Mid(sColumns, 2)
                End If
                aColumns = Split(sColumns, ",")
            
                Set lsi = lvwLookup(Index).ListItems.Add(, , rsBrowse.Fields(aColumns(0)).Value)
                lsi.Tag = sKey
                For i = 1 To UBound(aColumns)
                    If i <= UBound(aColHdrs) + 1 Then
                        lsi.SubItems(i) = rsBrowse.Fields(aColumns(i)).Value
                    End If
                Next i
                
                rsBrowse.MoveNext
            Loop
            rsBrowse.Filter = adFilterNone
            
            Index = Index + 1
        Next vntFilter
        
        For i = Index To lvwLookup.UBound
            tbsLookup.Tabs.Remove i + 1

            Unload lvwLookup(i)
        Next i
    End If
End Sub

Private Sub SetFont()
    Dim lvw As ListView
    
    If Not LookupFont Is Nothing Then
        Set Me.Font = LookupFont
        Set tbsLookup.Font = LookupFont
        For Each lvw In lvwLookup
            Set lvw.Font = LookupFont
        Next lvw
    End If
End Sub

Private Sub Form_Activate()
    If Not IsActivated Then
        If IsOnTop Then
            SetFormOnTop Me, IsOnTop
        End If
    
        Call SetFont
        Call FillInGrid
        Set tbsLookup.SelectedItem = tbsLookup.Tabs(FilterIndex + 1)
        lvwLookup(FilterIndex).SetFocus
        
        If WindowWidth > 0 Then Me.Width = WindowWidth
        If WindowHeight > 0 Then Me.Height = WindowHeight
        
        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    IsActivated = False
    AllowMultiSelection = False
    IsOnTop = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Call CancelSelect
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim sngWidth As Single
    Dim sngHeight As Single
    Dim sngTop As Single
    Dim lvw As ListView
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        
        sngWidth = Me.ScaleWidth
        If sngWidth < 0 Then sngWidth = 0
        
        sngHeight = Me.ScaleHeight - tbrLookup.Height
        If sngHeight < 0 Then sngHeight = 0
        
        tbsLookup.Visible = (Filters <> "")
        If tbsLookup.Visible Then
            tbsLookup.Move 0, 0, sngWidth, sngHeight
        
            sngTop = 360
            sngHeight = sngHeight - sngTop
            If sngHeight < 0 Then sngHeight = 0
        End If
        
        For Each lvw In lvwLookup
            lvw.Move 0, sngTop, sngWidth, sngHeight
        Next lvw
    End If
End Sub

Private Sub lvwLookup_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Const ICON_WIDTH = 400
    
    Dim CurrentSortKey As Integer
    Dim ColumnHeaderCaptionLength As Single
    
    If lvwLookup(Index).Sorted Then
        CurrentSortKey = lvwLookup(Index).SortKey + 1
        If ColumnHeader.Index = CurrentSortKey Then
            If lvwLookup(Index).SortOrder = lvwAscending Then
                lvwLookup(Index).SortOrder = lvwDescending
            Else
                lvwLookup(Index).SortOrder = lvwAscending
            End If
        Else
            lvwLookup(Index).ColumnHeaders(CurrentSortKey).Icon = 0
            lvwLookup(Index).ColumnHeaders(CurrentSortKey).Width = _
                lvwLookup(Index).ColumnHeaders(CurrentSortKey).Width - ICON_WIDTH
            
            lvwLookup(Index).SortKey = ColumnHeader.Index - 1
            lvwLookup(Index).SortOrder = lvwAscending
        End If
    Else
        lvwLookup(Index).Sorted = True
        lvwLookup(Index).SortKey = ColumnHeader.Index - 1
        lvwLookup(Index).SortOrder = lvwAscending
    End If
    ColumnHeader.Icon = lvwLookup(Index).SortOrder + 1
    If ColumnHeader.Index <> CurrentSortKey Then
        ColumnHeader.Width = ColumnHeader.Width + ICON_WIDTH
    End If
End Sub

Private Sub lvwLookup_DblClick(Index As Integer)
    Call OKSelect(Index)
End Sub

Private Sub lvwLookup_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            Call OKSelect(Index)
        Case vbKeyEscape
            Call CancelSelect
    End Select
End Sub

Private Sub tbrLookup_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim Index As Integer

    Select Case Button.Key
        Case "OK"
            Index = 0
            If tbsLookup.Visible Then Index = tbsLookup.SelectedItem.Index - 1
            Call OKSelect(Index)
        Case "Cancel"
            Call CancelSelect
    End Select
End Sub

Private Sub tbsLookup_Click()
    Dim lvw As ListView

    For Each lvw In lvwLookup
        lvw.Visible = (lvw.Index = tbsLookup.SelectedItem.Index - 1)
        If lvw.Visible Then
            lvw.SetFocus
        End If
    Next lvw
End Sub
