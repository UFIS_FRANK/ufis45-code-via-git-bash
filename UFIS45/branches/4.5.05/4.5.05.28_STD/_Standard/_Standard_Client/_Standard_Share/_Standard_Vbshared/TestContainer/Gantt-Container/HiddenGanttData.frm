VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form HiddenGanttData 
   Caption         =   "HiddenGanttData"
   ClientHeight    =   8295
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13695
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   8295
   ScaleWidth      =   13695
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Refresh 
      Caption         =   "Refresh"
      Height          =   255
      Left            =   12120
      TabIndex        =   3
      Top             =   480
      Width           =   1215
   End
   Begin TABLib.TAB TabGanttBkBars 
      Height          =   2775
      Left            =   240
      TabIndex        =   2
      Top             =   5280
      Width           =   13095
      _Version        =   65536
      _ExtentX        =   23098
      _ExtentY        =   4895
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGanttBars 
      Height          =   2775
      Left            =   240
      TabIndex        =   1
      Top             =   2400
      Width           =   13095
      _Version        =   65536
      _ExtentX        =   23098
      _ExtentY        =   4895
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGanttLines 
      Height          =   1695
      Left            =   3720
      TabIndex        =   0
      Top             =   120
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   2990
      _StockProps     =   64
   End
End
Attribute VB_Name = "HiddenGanttData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function InitGanttDataTab()

    Dim strHeader, strHeaderLen As String
    Dim strArr() As String
    Dim i As Integer

    TabGanttBkBars.ResetContent
    TabGanttLines.ResetContent
    TabGanttBars.ResetContent

    'init TabGanttLines
    strHeader = "EURN, SecondName, FirstName, TabBackColor, TabTextColor"
    strHeaderLen = ""
    strArr = Split(strHeader, ",")
    For i = 0 To UBound(strArr)
        If i + 1 > UBound(strArr) Then
            strHeaderLen = strHeaderLen & "80"
        Else
            strHeaderLen = strHeaderLen & "80,"
        End If
    Next i
    TabGanttLines.HeaderLengthString = strHeaderLen
    TabGanttLines.HeaderString = strHeader

    'init TabGanttBars and TabGanttBkBars
    strHeader = "EURN, Key, Begin, End, BarText, Shape, BackColor, TextColor, SplitColor" & _
        ",LeftColor, RightColor, Visible, LeftText, RightText, LeftTextColor, RightTextColor"
    strHeaderLen = ""
    strArr = Split(strHeader, ",")
    For i = 0 To UBound(strArr)
        If i + 1 > UBound(strArr) Then
            strHeaderLen = strHeaderLen & "80"
        Else
            strHeaderLen = strHeaderLen & "80,"
        End If
    Next i
    TabGanttBars.HeaderLengthString = strHeaderLen
    TabGanttBars.HeaderString = strHeader
    TabGanttBars.ShowHorzScroller True

    TabGanttBkBars.HeaderLengthString = strHeaderLen
    TabGanttBkBars.HeaderString = strHeader
    TabGanttBkBars.ShowHorzScroller True

End Function

Public Sub MakeGanttData()

    Dim i, j As Long
    Dim strEURN As String
    Dim strRet As String
    Dim strTextLine As String
    Dim strFlno As String
    Dim strFkey As String
    Dim strBarBkColor As String
    Dim strDrti As String

    Dim olLines() As String

    InitGanttDataTab

    'look for the workers - every worker gets only one line!
    With HiddenDataTab
        For i = 0 To .TabGHD.GetLineCount - 1
            strEURN = Trim(.TabGHD.GetColumnValue(i, 2))

            If TabGanttLines.GetLinesByColumnValue(0, strEURN, 0) = "" And _
            HiddenDataTab.TabDSR.GetLinesByColumnValue(0, strEURN, 0) <> "" Then 'the worker has no line
                strTextLine = strEURN + "," + _
                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 1)) + "," + _
                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 2)) + "," + _
                    "16777215,0"
                TabGanttLines.InsertTextLine strTextLine, False
            End If
        Next i

        For i = 0 To .TabDSR.GetLineCount - 1
            strEURN = Trim(.TabDSR.GetColumnValue(i, 0))

            If TabGanttLines.GetLinesByColumnValue(0, strEURN, 0) = "" Then  'the worker still has no line
                strTextLine = strEURN + "," + _
                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 1)) + "," + _
                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 2)) + "," + _
                    "16777215,0"
                TabGanttLines.InsertTextLine strTextLine, False
            End If
        Next i

        'look for the bars and the background bars of the worker
        For i = 0 To TabGanttLines.GetLineCount - 1
            strEURN = TabGanttLines.GetColumnValue(i, 0)

            'first do the bars
            strRet = .TabGHD.GetLinesByColumnValue(2, strEURN, 1)
            If strRet <> "" Then
                olLines = Split(strRet, ",")
                For j = 0 To UBound(olLines)
                    strFlno = GetFlnoByFkey(Trim(.TabGHD.GetColumnValue(olLines(j), 1)))
                    If strFlno = "" Then strFlno = "No data!"
                    strDrti = .TabGHD.GetColumnValue(olLines(j), 5)
                    strBarBkColor = vbRed
                    If strDrti = "A" Then
                        strBarBkColor = vbYellow
                    End If
                    If strDrti = "B" Then
                        strBarBkColor = 12632064
                    End If
                    If strDrti = "D" Then
                        strBarBkColor = vbGreen
                        strTextLine = strEURN + "," + _
                            Trim(.TabGHD.GetColumnValue(olLines(j), 0)) + "," + _
                            CStr(Tools.CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 3)))) + "," + _
                            CStr(Tools.CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 4)))) + "," + _
                            strFlno + "," + _
                            "1," + strBarBkColor + ",0,1," + CStr(vbGreen) + "," + CStr(vbWhite) + ",1," + _
                            " ," + " ," + "0,0"
                    Else
                        strTextLine = strEURN + "," + _
                            Trim(.TabGHD.GetColumnValue(olLines(j), 0)) + "," + _
                            CStr(Tools.CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 3)))) + "," + _
                            CStr(Tools.CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 4)))) + "," + _
                            strFlno + "," + _
                            "1," + strBarBkColor + ",0,0,0,0,1," + _
                            " ," + " ," + "0,0"
                    End If
                    TabGanttBars.InsertTextLine strTextLine, False
                Next j
            End If

            'now do the background bars
            strRet = .TabDSR.GetLinesByColumnValue(0, strEURN, 1)
            If strRet <> "" Then
                olLines = Split(strRet, ",")
                For j = 0 To UBound(olLines)
                    strTextLine = strEURN + "," + _
                        Trim(.TabDSR.GetColumnValue(olLines(j), 0)) + "," + _
                        CStr(Tools.CedaFullDateToVb(Trim(.TabDSR.GetColumnValue(olLines(j), 3)))) + "," + _
                        CStr(Tools.CedaFullDateToVb(Trim(.TabDSR.GetColumnValue(olLines(j), 4)))) + "," + _
                        " " + ",0,16711680,0,1"
                    TabGanttBkBars.InsertTextLine strTextLine, False
                Next j
            End If
        Next i
    End With
End Sub

Public Sub SendGanttData()
    Dim i, j As Long
    Dim strRet, strEURN As String
    Dim olLines() As String

    'init the line
    For i = 0 To TabGanttLines.GetLineCount - 1
        strEURN = TabGanttLines.GetColumnValue(i, 0)
        Form1.Gantt.AddLineAt i, _
            strEURN, _
            TabGanttLines.GetColumnValue(i, 1) + "," + _
            TabGanttLines.GetColumnValue(i, 2)

    'send the BackBars
        strRet = TabGanttBkBars.GetLinesByColumnValue(0, strEURN, 0)
        If strRet <> "" Then
            olLines = Split(strRet, ",")
            For j = 0 To UBound(olLines)
                Form1.Gantt.AddBkBarToLine i, _
                    CStr(TabGanttBkBars.GetColumnValue(olLines(j), 1)), _
                    TabGanttBkBars.GetColumnValue(olLines(j), 2), _
                    TabGanttBkBars.GetColumnValue(olLines(j), 3), _
                    TabGanttBkBars.GetColumnValue(olLines(j), 6), _
                    TabGanttBkBars.GetColumnValue(olLines(j), 7)
            Next j
        End If
    
    'send the Bars
        strRet = TabGanttBars.GetLinesByColumnValue(0, strEURN, 0)
        If strRet <> "" Then
            olLines = Split(strRet, ",")
            For j = 0 To UBound(olLines)
                Form1.Gantt.AddBarToLine i, _
                    TabGanttBars.GetColumnValue(olLines(j), 1), _
                    TabGanttBars.GetColumnValue(olLines(j), 2), _
                    TabGanttBars.GetColumnValue(olLines(j), 3), _
                    TabGanttBars.GetColumnValue(olLines(j), 4), _
                    TabGanttBars.GetColumnValue(olLines(j), 5), _
                    TabGanttBars.GetColumnValue(olLines(j), 6), _
                    TabGanttBars.GetColumnValue(olLines(j), 7), _
                    CBool(TabGanttBars.GetColumnValue(olLines(j), 8)), _
                    TabGanttBars.GetColumnValue(olLines(j), 9), _
                    TabGanttBars.GetColumnValue(olLines(j), 10), _
                    TabGanttBars.GetColumnValue(olLines(j), 12), _
                    TabGanttBars.GetColumnValue(olLines(j), 13), _
                    TabGanttBars.GetColumnValue(olLines(j), 14), _
                    TabGanttBars.GetColumnValue(olLines(j), 15)
            Next j
        End If
    Next i
End Sub

Private Sub Refresh_Click()
    TabGanttBars.RedrawTab
    TabGanttBkBars.RedrawTab
    TabGanttLines.RedrawTab
End Sub

Private Function GetFlnoByFkey(strFkey As String) As String
    Dim strLineNo As String
    Dim llLineNo As Long

    With HiddenDataTab.TabAFT
        strLineNo = .GetLinesByColumnValue(0, strFkey, 0)
        If strLineNo <> "" Then
            llLineNo = strLineNo
        End If
        GetFlnoByFkey = .GetColumnValue(llLineNo, 1)
    End With
End Function
