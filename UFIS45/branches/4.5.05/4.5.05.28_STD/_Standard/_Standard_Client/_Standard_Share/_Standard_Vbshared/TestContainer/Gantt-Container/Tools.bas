Attribute VB_Name = "Tools"
Option Explicit

Public Function CedaDateToVb(cpDate As String)
    If Trim(cpDate) <> "" Then
        CedaDateToVb = DateSerial(val(Mid(cpDate, 1, 4)), val(Mid(cpDate, 5, 2)), val(Mid(cpDate, 7, 2)))
    Else
        CedaDateToVb = ""
    End If
End Function

Public Function CedaTimeToVb(cpDate As String)
    Dim ilHH As Integer
    Dim ilMM As Integer
    Dim ilSS As Integer

    If Trim(cpDate) <> "" Then
        If Len(cpDate) > 8 Then
            CedaTimeToVb = TimeSerial(val(Mid(cpDate, 9, 2)), val(Mid(cpDate, 11, 2)), val(Mid(cpDate, 13, 2)))
        Else
            If InStr(cpDate, ":") > 0 Then
                ilHH = val(GetItem(cpDate, 1, ":"))
                ilMM = val(GetItem(cpDate, 2, ":"))
                ilSS = val(GetItem(cpDate, 3, ":"))
                CedaTimeToVb = TimeSerial(ilHH, ilMM, ilSS)
            Else
                CedaTimeToVb = TimeSerial(val(Mid(cpDate, 1, 2)), val(Mid(cpDate, 3, 2)), val(Mid(cpDate, 5, 2)))
            End If
        End If
    Else
        CedaTimeToVb = ""
    End If
End Function

Public Function CedaFullDateToVb(cpDate As String)
    Dim myDate
    Dim myTime

    myDate = CedaDateToVb(cpDate)
    myTime = CedaTimeToVb(cpDate)
    If (myDate <> "") And (myTime <> "") Then
        CedaFullDateToVb = myDate + myTime
    Else
        CedaFullDateToVb = ""
    End If
End Function

Public Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
    Dim Result
    Dim ilSepLen As Integer
    Dim ilFirstPos As Integer
    Dim ilLastPos As Integer
    Dim ilItmLen As Integer
    Dim ilItmNbr As Integer

    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function

Public Function GetBarLineInTab(Key As String) As Long
    Dim strBarLine As String
    Dim llBarLine As Long

    With HiddenGanttData.TabGanttBars
        strBarLine = .GetLinesByColumnValue(1, Key, 1)
        If strBarLine <> "" Then
            llBarLine = CLng(strBarLine)
        End If
    End With
    GetBarLineInTab = llBarLine
End Function
Public Function GetBkBarLineInTab(Key As String) As Long
    Dim strBkBarLine As String
    Dim llBkBarLine As Long

    With HiddenGanttData.TabGanttBkBars
        strBkBarLine = .GetLinesByColumnValue(1, Key, 1)
        If strBkBarLine <> "" Then
            llBkBarLine = CLng(strBkBarLine)
        End If
    End With
    GetBkBarLineInTab = llBkBarLine
End Function

Public Function GetBarLineByKey(Key As String) As Long
    Dim strBarLine, strTmpLine As String
    Dim llBarLine, llTmpLine As Long
    Dim strEURN As String

    'look for the bar in the TabGanttBars-table
    With HiddenGanttData.TabGanttBars
        strTmpLine = .GetLinesByColumnValue(1, Key, 0)
        If strTmpLine <> "" Then
            llTmpLine = CLng(strTmpLine)
            strEURN = .GetColumnValue(llTmpLine, 0)
        End If
    End With

    'look for the line the bar is in
    With HiddenGanttData.TabGanttLines
        strBarLine = .GetLinesByColumnValue(0, strEURN, 0)
        If strBarLine <> "" Then
            llBarLine = CLng(strBarLine)
        End If
    End With

    GetBarLineByKey = llBarLine
End Function

Public Function GetBkBarLineByKey(Key As String) As Long
    Dim strBarLine, strTmpLine As String
    Dim llBarLine, llTmpLine As Long
    Dim strEURN As String

    'look for the bar in the TabGanttBars-table
    With HiddenGanttData.TabGanttBkBars
        strTmpLine = .GetLinesByColumnValue(1, Key, 1)
        If strTmpLine <> "" Then
            llTmpLine = CLng(strTmpLine)
            strEURN = .GetColumnValue(llTmpLine, 0)
        End If
    End With

    'look for the line the bar is in
    With HiddenGanttData.TabGanttLines
        strBarLine = .GetLinesByColumnValue(0, strEURN, 1)
        If strBarLine <> "" Then
            llBarLine = CLng(strBarLine)
        End If
    End With

    GetBkBarLineByKey = llBarLine
End Function



'Public Function CleanString(cpText As String) As String
'    Static ilPatchValue(20)
'    Dim ilPos As Integer
'    Dim clLook As String
'    Dim clPatch As String
'
'    clLook = Chr(179)
'    clPatch = Chr(10)
'    If clLook <> clPatch Then
'        ilPos = 1
'        While (ilPos > 0)
'            ilPos = InStr(ilPos, cpText, clLook)
'            If ilPos > 0 Then
'                If clLook = Chr(179) Then
'                    cpText = Left(cpText, ilPos - 1) & Chr(13) & Chr(10) & Mid(cpText, ilPos + 1)
'                Else
'                    Mid(cpText, ilPos, 1) = clPatch
'                End If
'            End If
'        Wend
'    End If
'    CleanString = cpText
'End Function

'Public Function StringClientToServer(opText As String)
''    Dim omClientChars As String
''    Dim omServerChars As String
'    Dim olRet As String
'    Dim ilIndex, i As Integer
'
'    Dim olClientChars() As String
'    Dim olServerChars() As String
'
'    olClientChars = Array(Chr(34), Chr(39), Chr(44), Chr(2), Chr(13), Chr(58), _
'        Chr(123), Chr(125), Chr(91), Chr(93), Chr(124), Chr(94), Chr(63), Chr(36), _
'        Chr(40), Chr(41), Chr(59), Chr(64))
'
'    'omClientChars = "\042\047\54\002\015\072"       ' 34,39,44,10,13,58
'                                    '  {   }   [   ]   |   ^   ?  $  (  )  ;  @
'    'omClientChars = omClientChars + "\173\175\133\135\174\136\77\44\50\51\73\100"
'
'    olServerChars = Array(Chr(176), Chr(177), Chr(178), Chr(180), Chr(179), Chr(191), _
'        Chr(147), Chr(148), Chr(149), Chr(150), Chr(151), Chr(152), Chr(153), Chr(154), _
'        Chr(155), Chr(156), Chr(157), Chr(158))
'
'    'omServerChars = "\260\261\262\264\263\277"     '176,177,178,180,179,191
'                                    ' {   }   [   ]   |   ^   ?   $   (   )   ;   @
'    'omServerChars = omServerChars + "\223\224\225\226\227\230\231\232\233\234\235\236"
'
'    olRet = opText
'    For i = 0 To Len(olRet)
'        ilIndex = InStr(0, olRet, (Mid(olRet, i, 1)))
'        If ilIndex > -1 Then
'            olRet = Left(olRet, i) + olServerChars(ilIndex) + Mid(olRet, i + 2)
'        End If
'    Next i
'
'    StringClientToServer = olRet
'
'End Function


'Public Function StringServerToClient(opText As String)
'
'    Dim olClientChars() As String
'    Dim olServerChars() As String
'    Dim ilIndex, i As Integer
'    Dim olRet As String
'
'    olClientChars = Array(Chr(34), Chr(39), Chr(44), Chr(2), Chr(13), Chr(58), _
'        Chr(123), Chr(125), Chr(91), Chr(93), Chr(124), Chr(94), Chr(63), Chr(36), _
'        Chr(40), Chr(41), Chr(59), Chr(64))
'
'    olServerChars = Array(Chr(176), Chr(177), Chr(178), Chr(180), Chr(179), Chr(191), _
'        Chr(147), Chr(148), Chr(149), Chr(150), Chr(151), Chr(152), Chr(153), Chr(154), _
'        Chr(155), Chr(156), Chr(157), Chr(158))
'
'    olRet = opText
'    For i = 0 To Len(olRet)
'        ilIndex = InStr(0, olRet, (Mid(olRet, i, 1)))
'        If ilIndex > -1 Then
'            olRet = Left(olRet, i) + olServerChars(ilIndex) + Mid(olRet, i + 2)
'        End If
'    Next i
'
'    StringServerToClient = olRet
'
'End Function

'Public Function CutServerStrings(opText As String)
'    Dim olServerChars As String
'    Dim ilIndex, i As Integer
'    Dim olRet As String
'
'    olServerChars = Chr(176) + Chr(177) + Chr(178) + Chr(180) + Chr(179) + Chr(191) + Chr(147) + Chr(148) + Chr(149) + Chr(150) + Chr(151) + Chr(152) + Chr(153) + Chr(154) + Chr(155) + Chr(156) + Chr(157) + Chr(158) + Chr(253)
'
'    olRet = opText
'    For i = 1 To Len(olRet)
'        ilIndex = InStr(1, olServerChars, (Mid(olRet, i, 1)))
'        If ilIndex > 0 Then
'            olRet = Left(olRet, i - 1) '+ olServerChars(ilIndex) + Mid(olRet, i + 2)
'            CutServerStrings = olRet
'            Exit For
'        End If
'    Next i
'
'    CutServerStrings = olRet
'
'End Function
'{
'    CString omClientChars("\042\047\54\002\015\072");  // 34,39,44,10,13,58
'                            //  {   }   [   ]   |   ^   ?  $  (  )  ;  @
'    omClientChars += CString("\173\175\133\135\174\136\77\44\50\51\73\100");
'    CString omServerChars("\260\261\262\264\263\277");  // 176,177,178,180,179,191
'                            //  {   }   [   ]   |   ^   ?   $   (   )   ;   @
'    omServerChars += CString("\223\224\225\226\227\230\231\232\233\234\235\236");
'    CString olRet = opText;
'    for(int i = 0; i < omServerChars.GetLength(); i++)
'    {
'        olRet.Replace(omServerChars[i], omClientChars[i]);
'    }
'    return olRet;
'
'}
