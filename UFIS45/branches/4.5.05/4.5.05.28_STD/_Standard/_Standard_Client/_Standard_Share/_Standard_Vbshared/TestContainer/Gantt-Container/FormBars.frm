VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormBars 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormBars"
   ClientHeight    =   7350
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8490
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7350
   ScaleWidth      =   8490
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkAutoSizeLineHeightToFit 
      Caption         =   "AutoSizeLineHeightToFit"
      Height          =   240
      Left            =   5490
      TabIndex        =   78
      Top             =   3510
      Width           =   2895
   End
   Begin VB.Frame MainBarKey 
      Height          =   2400
      Left            =   0
      TabIndex        =   53
      Top             =   4905
      Width           =   8385
      Begin VB.CheckBox chkAutoMoveSubBarsWithBar 
         Caption         =   "AutoMoveSubBarsWithBar"
         Height          =   195
         Left            =   2745
         TabIndex        =   77
         Top             =   225
         Width           =   2715
      End
      Begin VB.TextBox txtSubBarStackCount 
         Height          =   285
         Left            =   1635
         TabIndex        =   76
         Text            =   "1"
         ToolTipText     =   "enter the MainBarKey"
         Top             =   180
         Width           =   945
      End
      Begin VB.CommandButton btnSubBarLevels 
         Caption         =   "SubBarLevels"
         Height          =   285
         Left            =   90
         TabIndex        =   75
         Top             =   180
         Width           =   1425
      End
      Begin VB.CommandButton btnRemoveSubBar 
         Caption         =   "RemoveSubBar"
         Height          =   285
         Left            =   90
         TabIndex        =   70
         Top             =   1530
         Width           =   1425
      End
      Begin VB.TextBox txtRemoveSubBarKey 
         Height          =   285
         Left            =   2700
         TabIndex        =   72
         Text            =   "SubBarKey"
         ToolTipText     =   "Key for the Bar"
         Top             =   1530
         Width           =   885
      End
      Begin VB.TextBox txtRemoveSubBarMainKey 
         Height          =   285
         Left            =   1635
         TabIndex        =   71
         Text            =   "MainBarKey"
         ToolTipText     =   "enter the MainBarKey"
         Top             =   1530
         Width           =   945
      End
      Begin VB.TextBox txtSubBarRightOutsideTextColor 
         Height          =   285
         Left            =   5175
         TabIndex        =   69
         Text            =   "123465"
         ToolTipText     =   "SubBarRightOutsideTextColor"
         Top             =   1170
         Width           =   900
      End
      Begin VB.TextBox txtSubBarLeftOutsideTextColor 
         Height          =   285
         Left            =   4185
         TabIndex        =   68
         Text            =   "255"
         ToolTipText     =   "SubBarLeftOutsideTextColor"
         Top             =   1170
         Width           =   945
      End
      Begin VB.TextBox txtSubBarRightOutsideText 
         Height          =   285
         Left            =   2700
         TabIndex        =   67
         Text            =   "right text"
         ToolTipText     =   "SubBarRightOutsideText"
         Top             =   1170
         Width           =   900
      End
      Begin VB.TextBox txtSubBarLeftOutsideText 
         Height          =   285
         Left            =   1635
         TabIndex        =   66
         Text            =   "left text"
         ToolTipText     =   "SubBarLeftOutsideText"
         Top             =   1170
         Width           =   945
      End
      Begin VB.TextBox txtSubBarRightSplitColor 
         Height          =   285
         Left            =   5175
         TabIndex        =   65
         Text            =   "123465"
         ToolTipText     =   "SubBarRightSplitColor"
         Top             =   855
         Width           =   900
      End
      Begin VB.TextBox txtSubBarLeftSplitColor 
         Height          =   285
         Left            =   4185
         TabIndex        =   64
         Text            =   "255"
         ToolTipText     =   "SubBarLeftSplitColor"
         Top             =   855
         Width           =   945
      End
      Begin VB.TextBox txtSplitColor 
         Height          =   285
         Left            =   3690
         TabIndex        =   63
         Text            =   "0"
         ToolTipText     =   "SubBarSplittColor"
         Top             =   855
         Width           =   405
      End
      Begin VB.TextBox txtSubBarTextColor 
         Height          =   285
         Left            =   2700
         TabIndex        =   62
         Text            =   "0"
         ToolTipText     =   "SubBarTextColor"
         Top             =   855
         Width           =   900
      End
      Begin VB.TextBox txtSubBarColor 
         Height          =   285
         Left            =   1635
         TabIndex        =   61
         Text            =   "255"
         ToolTipText     =   "SubBarColor"
         Top             =   855
         Width           =   945
      End
      Begin VB.TextBox txtSubBarShape 
         Height          =   285
         Left            =   7425
         TabIndex        =   60
         Text            =   "1"
         ToolTipText     =   "The drawing style of the bar (0, 1 or 2))"
         Top             =   540
         Width           =   810
      End
      Begin VB.TextBox txtSubBarText 
         Height          =   285
         Left            =   6705
         TabIndex        =   59
         Text            =   "Caption"
         ToolTipText     =   "Caption of the bar"
         Top             =   540
         Width           =   660
      End
      Begin VB.TextBox txtSubBarEndTime 
         Height          =   285
         Left            =   5175
         TabIndex        =   58
         Text            =   "29.09.00 23:00:00"
         ToolTipText     =   "Endtime of Bar, e.g. 01.01.2000 10:15"
         Top             =   540
         Width           =   1485
      End
      Begin VB.TextBox txtMainBarKey 
         Height          =   285
         Left            =   1635
         TabIndex        =   55
         Text            =   "MainBarKey"
         ToolTipText     =   "enter the MainBarKey"
         Top             =   540
         Width           =   945
      End
      Begin VB.TextBox txtSubBarKey 
         Height          =   285
         Left            =   2700
         TabIndex        =   56
         Text            =   "SubBarKey"
         ToolTipText     =   "Key for the Bar"
         Top             =   540
         Width           =   885
      End
      Begin VB.TextBox txtSubBarStartTime 
         Height          =   285
         Left            =   3690
         TabIndex        =   57
         Text            =   "29.09.00 22:00:00"
         ToolTipText     =   "Start of bar, e.g. 01.01.2000 10:15"
         Top             =   540
         Width           =   1440
      End
      Begin VB.CommandButton btnAddSubBar 
         Caption         =   "AddSubBar"
         Height          =   285
         Left            =   90
         TabIndex        =   54
         Top             =   540
         Width           =   1425
      End
   End
   Begin VB.CommandButton SetBarStyleByKey 
      Caption         =   "SetBarStyleByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   52
      Top             =   1702
      Width           =   1800
   End
   Begin VB.TextBox txtSetBarStyleByKey 
      Height          =   285
      Left            =   2010
      TabIndex        =   51
      Text            =   "Key"
      ToolTipText     =   "BarKey"
      Top             =   1702
      Width           =   1320
   End
   Begin VB.TextBox txtSetBarStyleByKeyStyle 
      Height          =   285
      Left            =   3465
      TabIndex        =   50
      Text            =   "1"
      ToolTipText     =   "The drawing style of the bar (0, 1 or 2))"
      Top             =   1702
      Width           =   810
   End
   Begin VB.TextBox txtSetBarSplitColorByKeyRightColor 
      Height          =   285
      Left            =   4410
      TabIndex        =   13
      Text            =   "255"
      ToolTipText     =   "Right bar color"
      Top             =   2355
      Width           =   810
   End
   Begin VB.CommandButton SetBarSplitColorByKey 
      Caption         =   "SetBarSplitColorByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   49
      Top             =   2355
      Width           =   1800
   End
   Begin VB.TextBox txtSetBarSplitColorByKey 
      Height          =   285
      Left            =   2010
      TabIndex        =   11
      Text            =   "Key"
      ToolTipText     =   "BarKey"
      Top             =   2355
      Width           =   1320
   End
   Begin VB.TextBox txtSetBarSplitColorByKeyLeftColor 
      Height          =   285
      Left            =   3465
      TabIndex        =   12
      Text            =   "255"
      ToolTipText     =   "Left bar color"
      Top             =   2355
      Width           =   810
   End
   Begin VB.TextBox txtSetBarTextColorByKeyColor 
      Height          =   285
      Left            =   3465
      TabIndex        =   10
      Text            =   "255"
      ToolTipText     =   "BarColor"
      Top             =   1376
      Width           =   810
   End
   Begin VB.TextBox txtSetBarTextColorByKey 
      Height          =   285
      Left            =   2010
      TabIndex        =   9
      Text            =   "Key"
      ToolTipText     =   "BarKey"
      Top             =   1376
      Width           =   1320
   End
   Begin VB.CommandButton SetBarTextColorByKey 
      Caption         =   "SetBarTextColorByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   48
      Top             =   1376
      Width           =   1800
   End
   Begin VB.CommandButton GetBarsByLine 
      Caption         =   "GetBarsByLine"
      Height          =   285
      Left            =   5565
      TabIndex        =   36
      Top             =   4035
      Width           =   1455
   End
   Begin VB.TextBox txtGetBarsByLine 
      Height          =   285
      Left            =   7125
      TabIndex        =   35
      Text            =   "1"
      ToolTipText     =   "Insert LineNo"
      Top             =   4035
      Width           =   930
   End
   Begin VB.CommandButton GetBarsByBkBar 
      Caption         =   "GetBarsByBkBar"
      Height          =   285
      Left            =   5565
      TabIndex        =   34
      Top             =   4350
      Width           =   1455
   End
   Begin VB.TextBox txtGetBarsByBkBar 
      Height          =   285
      Left            =   7125
      TabIndex        =   33
      Top             =   4350
      Width           =   930
   End
   Begin VB.CommandButton btnDeleteBar 
      Caption         =   "DeleteBarByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   32
      Top             =   420
      Width           =   1425
   End
   Begin VB.TextBox txtDeleteBar 
      Height          =   285
      Left            =   1650
      TabIndex        =   6
      Text            =   "Key"
      ToolTipText     =   "Key of the bar to delete"
      Top             =   420
      Width           =   975
   End
   Begin VB.CommandButton btnInsertBar 
      Caption         =   "AddBarToLine"
      Height          =   285
      Left            =   105
      TabIndex        =   31
      Top             =   105
      Width           =   1425
   End
   Begin VB.TextBox txtBarStartTime 
      Height          =   285
      Left            =   2760
      TabIndex        =   3
      Text            =   "29.09.00 22:00:00"
      ToolTipText     =   "Start of bar, e.g. 01.01.2000 10:15"
      Top             =   105
      Width           =   1440
   End
   Begin VB.TextBox txtBarKey 
      Height          =   285
      Left            =   2100
      TabIndex        =   2
      Text            =   "TestKey"
      ToolTipText     =   "Key for the Bar"
      Top             =   105
      Width           =   705
   End
   Begin VB.TextBox btnBarLineNo 
      Height          =   285
      Left            =   1650
      TabIndex        =   1
      Text            =   "5"
      ToolTipText     =   "LineNo where Backgroundbar shall be inserted"
      Top             =   105
      Width           =   405
   End
   Begin VB.TextBox txtBarEndTime 
      Height          =   285
      Left            =   4200
      TabIndex        =   4
      Text            =   "29.09.00 23:00:00"
      ToolTipText     =   "Endtime of Bar, e.g. 01.01.2000 10:15"
      Top             =   105
      Width           =   1485
   End
   Begin VB.TextBox txtBarCaption 
      Height          =   285
      Left            =   5685
      TabIndex        =   5
      Text            =   "Caption"
      ToolTipText     =   "Caption of the bar"
      Top             =   105
      Width           =   660
   End
   Begin VB.CommandButton SetBarTimeByKey 
      Caption         =   "SetBarTimeByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   30
      Top             =   2028
      Width           =   1800
   End
   Begin VB.TextBox txtSetBarTimeByKey_Key 
      Height          =   285
      Left            =   2010
      TabIndex        =   14
      Text            =   "Key"
      ToolTipText     =   "BarKey"
      Top             =   2028
      Width           =   1320
   End
   Begin VB.TextBox txtSetBarTimeByKeyBegin 
      Height          =   285
      Left            =   3465
      TabIndex        =   15
      Text            =   "29.09.00 22:00:00"
      ToolTipText     =   "Begin"
      Top             =   2028
      Width           =   1440
   End
   Begin VB.TextBox txtSetBarTimeByKeyEnde 
      Height          =   285
      Left            =   4935
      TabIndex        =   16
      Text            =   "29.09.00 23:00:00"
      ToolTipText     =   "End"
      Top             =   2028
      Width           =   1335
   End
   Begin VB.CommandButton SetBarColorByKey 
      Caption         =   "SetBarColorByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   28
      Top             =   1050
      Width           =   1800
   End
   Begin VB.TextBox txtSetBarColorByKey 
      Height          =   285
      Left            =   2010
      TabIndex        =   7
      Text            =   "Key"
      ToolTipText     =   "BarKey"
      Top             =   1050
      Width           =   1320
   End
   Begin VB.TextBox txtBarColor 
      Height          =   285
      Left            =   3465
      TabIndex        =   8
      Text            =   "255"
      ToolTipText     =   "BarColor"
      Top             =   1050
      Width           =   810
   End
   Begin VB.TextBox txtSetBarLeftOutsideTextColor 
      Height          =   330
      Left            =   3885
      TabIndex        =   27
      Text            =   "color"
      ToolTipText     =   "Insert the text color"
      Top             =   4350
      Width           =   1380
   End
   Begin VB.TextBox txtSetBarLeftOutsideTextColorKey 
      Height          =   330
      Left            =   2415
      TabIndex        =   26
      Text            =   "key"
      ToolTipText     =   "Insert the bar key"
      Top             =   4350
      Width           =   1275
   End
   Begin VB.TextBox txtSetBarLeftOutsideText 
      Height          =   330
      Left            =   3885
      TabIndex        =   25
      Text            =   "text"
      ToolTipText     =   "insert the text"
      Top             =   4035
      Width           =   1380
   End
   Begin VB.TextBox txtSetBarLeftOutsideTextKey 
      Height          =   330
      Left            =   2415
      TabIndex        =   24
      Text            =   "key"
      ToolTipText     =   "Insert the bar key"
      Top             =   4035
      Width           =   1275
   End
   Begin VB.CommandButton SetBarLeftOutsideTextColor 
      Caption         =   "SetBarLeftOutsideTextColor"
      Height          =   330
      Left            =   105
      TabIndex        =   23
      Top             =   4350
      Width           =   2220
   End
   Begin VB.CommandButton SetBarLeftOutsideText 
      Caption         =   "SetBarLeftOutsideText"
      Height          =   330
      Left            =   105
      TabIndex        =   22
      Top             =   4035
      Width           =   2220
   End
   Begin VB.TextBox txtSetBarRightOutsideTextColor 
      Height          =   330
      Left            =   3885
      TabIndex        =   21
      Text            =   "color"
      ToolTipText     =   "Insert the text color"
      Top             =   3720
      Width           =   1380
   End
   Begin VB.TextBox txtSetBarRightOutsideTextColorKey 
      Height          =   330
      Left            =   2415
      TabIndex        =   20
      Text            =   "key"
      ToolTipText     =   "Insert the bar key"
      Top             =   3720
      Width           =   1275
   End
   Begin VB.TextBox txtSetBarRightOutsideText 
      Height          =   330
      Left            =   3885
      TabIndex        =   19
      Text            =   "text"
      ToolTipText     =   "insert the text"
      Top             =   3405
      Width           =   1380
   End
   Begin VB.TextBox txtSetBarRightOutsideTextKey 
      Height          =   330
      Left            =   2415
      TabIndex        =   18
      Text            =   "key"
      ToolTipText     =   "Insert the bar key"
      Top             =   3405
      Width           =   1275
   End
   Begin VB.CommandButton SetBarRightOutsideTextColor 
      Caption         =   "SetBarRightOutsideTextColor"
      Height          =   330
      Left            =   105
      TabIndex        =   17
      Top             =   3720
      Width           =   2220
   End
   Begin VB.CommandButton SetBarRightOutsideText 
      Caption         =   "SetBarRightOutsideText"
      Height          =   330
      Left            =   105
      TabIndex        =   0
      Top             =   3405
      Width           =   2220
   End
   Begin VB.Frame Frame1 
      Height          =   1590
      Left            =   0
      TabIndex        =   29
      Top             =   3195
      Width           =   5370
   End
   Begin VB.Frame Frame2 
      Height          =   855
      Left            =   0
      TabIndex        =   37
      Top             =   -75
      Width           =   6420
   End
   Begin VB.Frame Frame3 
      Height          =   2250
      Left            =   0
      TabIndex        =   38
      Top             =   840
      Width           =   6420
      Begin VB.TextBox txtDeleteSplitColor 
         Height          =   285
         Left            =   1995
         TabIndex        =   74
         Text            =   "Key"
         ToolTipText     =   "BarKey"
         Top             =   1845
         Width           =   1320
      End
      Begin VB.CommandButton btnDeleteSplitColor 
         Caption         =   "DeleteSplitColor"
         Height          =   285
         Left            =   90
         TabIndex        =   73
         Top             =   1845
         Width           =   1800
      End
   End
   Begin VB.Frame Frame4 
      Height          =   960
      Left            =   5460
      TabIndex        =   39
      Top             =   3825
      Width           =   2955
   End
   Begin MSComctlLib.Slider Slider5 
      Height          =   255
      Left            =   6720
      TabIndex        =   40
      Top             =   2205
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   450
      _Version        =   393216
      Min             =   1
      SelStart        =   3
      Value           =   3
   End
   Begin MSComctlLib.Slider Slider4 
      Height          =   255
      Left            =   6720
      TabIndex        =   41
      Top             =   1575
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   450
      _Version        =   393216
      Max             =   30
      SelStart        =   4
      Value           =   4
   End
   Begin MSComctlLib.Slider Slider3 
      Height          =   255
      Left            =   6720
      TabIndex        =   44
      Top             =   945
      Width           =   1710
      _ExtentX        =   3016
      _ExtentY        =   450
      _Version        =   393216
      Min             =   9
      Max             =   50
      SelStart        =   30
      Value           =   30
   End
   Begin MSComctlLib.Slider Slider2 
      Height          =   255
      Left            =   6720
      TabIndex        =   45
      Top             =   315
      Width           =   1710
      _ExtentX        =   3016
      _ExtentY        =   450
      _Version        =   393216
      Min             =   3
      Max             =   15
      SelStart        =   5
      Value           =   5
   End
   Begin VB.Label ResizeAreaWidth 
      Caption         =   "ResizeAreaWidth:"
      Height          =   255
      Left            =   6615
      TabIndex        =   47
      Top             =   105
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "ResizeMinimalWidth:"
      Height          =   255
      Left            =   6615
      TabIndex        =   46
      Top             =   735
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "BarOverlapOffset:"
      Height          =   255
      Left            =   6615
      TabIndex        =   43
      Top             =   1365
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "BarNumberExpandLine:"
      Height          =   255
      Left            =   6615
      TabIndex        =   42
      Top             =   1995
      Width           =   1815
   End
End
Attribute VB_Name = "FormBars"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnDeleteSplitColor_Click()
    Form1.Gantt.DeleteSplitColor txtDeleteSplitColor.Text
End Sub

Private Sub btnSubBarLevels_Click()
    Form1.Gantt.SubbarStackCount = txtSubBarStackCount.Text
End Sub

Private Sub chkAutoMoveSubBarsWithBar_Click()
    If chkAutoMoveSubBarsWithBar.Value = 1 Then
        Form1.Gantt.AutoMoveSubBarsWithBar = True
    Else
        Form1.Gantt.AutoMoveSubBarsWithBar = False
    End If
End Sub

Private Sub chkAutoSizeLineHeightToFit_Click()
    If chkAutoSizeLineHeightToFit.Value = 1 Then
        Form1.Gantt.AutoSizeLineHeightToFit = True
    Else
        Form1.Gantt.AutoSizeLineHeightToFit = False
    End If
End Sub

Private Sub SetBarLeftOutsideText_Click()
    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(txtSetBarLeftOutsideTextKey.Text)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 12, txtSetBarLeftOutsideText.Text

    'send the new text to the gantt
    Form1.Gantt.SetBarLeftOutsideText txtSetBarLeftOutsideTextKey.Text, txtSetBarLeftOutsideText.Text

    'Refresh the gantt line
    Dim omBarLine As Long
    omBarLine = Tools.GetBarLineByKey(txtSetBarLeftOutsideTextKey.Text)
    Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub SetBarLeftOutsideTextColor_Click()
    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(txtSetBarLeftOutsideTextColorKey.Text)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 14, txtSetBarLeftOutsideTextColor.Text

    'send the new text to the gantt
    Form1.Gantt.SetBarLeftOutsideTextColor txtSetBarLeftOutsideTextColorKey.Text, txtSetBarLeftOutsideTextColor.Text

    'Refresh the gantt line
    Dim omBarLine As Long
    omBarLine = Tools.GetBarLineByKey(txtSetBarLeftOutsideTextColorKey.Text)
    Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub SetBarRightOutsideText_Click()
    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(txtSetBarRightOutsideTextKey.Text)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 13, txtSetBarRightOutsideText.Text

    'send the new text to the gantt
    Form1.Gantt.SetBarRightOutsideText txtSetBarRightOutsideTextKey.Text, txtSetBarRightOutsideText.Text

    'Refresh the gantt line
    Dim omBarLine As Long
    omBarLine = Tools.GetBarLineByKey(txtSetBarRightOutsideTextKey.Text)
    Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub SetBarRightOutsideTextColor_Click()
    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(txtSetBarRightOutsideTextColorKey.Text)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 15, txtSetBarRightOutsideTextColor.Text

    'send the new text to the gantt
    Form1.Gantt.SetBarRightOutsideTextColor txtSetBarRightOutsideTextColorKey.Text, txtSetBarRightOutsideTextColor.Text

    'Refresh the gantt line
    Dim omBarLine As Long
    omBarLine = Tools.GetBarLineByKey(txtSetBarRightOutsideTextColorKey.Text)
    Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub GetBarsByBkBar_Click()
    Dim BkBar As String
    BkBar = txtGetBarsByBkBar.Text
    If (BkBar <> "") Then
        MsgBox Form1.Gantt.GetBarsByBkBar(BkBar), vbOKOnly, "GetBarsByBkBar"
    End If
End Sub

Private Sub GetBarsByLine_Click()
    Dim LineNr As String
    LineNr = txtGetBarsByLine.Text
    If (IsNumeric(LineNr) = True) Then
        If LineNr > -1 Then
            MsgBox Form1.Gantt.GetBarsByLine(LineNr), vbOKOnly, "GetBarsByLine"
        End If
    End If
End Sub
Private Sub SetBarColorByKey_Click()
    Dim Key As String
    Dim Color As Long
    Key = txtSetBarColorByKey.Text
    Color = txtBarColor.Text

    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 6, Color  'set the new color
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 8, 0      'remind that there's no split-color

    'send the new color to the gantt
    Form1.Gantt.SetBarColor Key, Color

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"
    'Dim omBarLine As Long
    'omBarLine = Tools.GetBarLineByKey(Key)
    'Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub SetBarSplitColorByKey_Click()
    Dim Key As String
    Dim LeftColor, RightColor As Long
    Key = txtSetBarSplitColorByKey.Text
    LeftColor = txtSetBarSplitColorByKeyLeftColor.Text
    RightColor = txtSetBarSplitColorByKeyRightColor.Text

    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 8, 1          'remind that there's a split-color
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 9, LeftColor
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 10, RightColor

    'send the new color to the gantt
    Form1.Gantt.SetBarSplitColor Key, LeftColor, RightColor

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"
    'Dim omBarLine As Long
    'omBarLine = Tools.GetBarLineByKey(Key)
    'Form1.Gantt.RefreshGanttLine omBarLine

End Sub

Private Sub SetBarStyleByKey_Click()
    Dim Key As String
    Dim BarStyle As Integer
    Key = txtSetBarStyleByKey.Text
    BarStyle = txtSetBarStyleByKeyStyle.Text

    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 5, BarStyle

    'send the new color to the gantt
    Form1.Gantt.SetBarStyle Key, BarStyle

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"

End Sub

Private Sub SetBarTextColorByKey_Click()
    Dim Key As String
    Dim Color As Long
    Key = txtSetBarTextColorByKey.Text
    Color = txtSetBarTextColorByKeyColor.Text

    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 7, Color

    'send the new color to the gantt
    Form1.Gantt.SetBarTextColor Key, Color

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub SetBarTimeByKey_Click()
    Dim Key As String
    Dim Begin, Ende As Date
    Key = txtSetBarTimeByKey_Key.Text
    Begin = txtSetBarTimeByKeyBegin.Text
    Ende = txtSetBarTimeByKeyEnde.Text

    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 2, Begin
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 3, Ende

    'send the new date to the gantt
    Form1.Gantt.SetBarDate Key, Begin, Ende

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"
    'Dim omBarLine As Long
    'omBarLine = Tools.GetBarLineByKey(txtSetBarTimeByKey_Key.Text)
    'Form1.Gantt.RefreshGanttLine omBarLine
End Sub

Private Sub btnDeleteBar_Click()
    Dim Key As String
    Key = txtDeleteBar.Text
    
    'first set the data in the container
    Dim omBarLineTab As Long
    omBarLineTab = Tools.GetBarLineInTab(Key)
    HiddenGanttData.TabGanttBars.SetColumnValue omBarLineTab, 11, 0 'make it invisible

    'send the new color to the gantt
    Form1.Gantt.DeleteBarByKey (Key)

    'Refresh the gantt line
    Form1.Gantt.RefreshArea "GANTT"
    'Dim omBarLine As Long
    'omBarLine = Tools.GetBarLineByKey(txtDeleteBar.Text)
    'Form1.Gantt.RefreshGanttLine omBarLine
End Sub
Private Sub btnInsertBar_Click()
    Dim Start As Date, Ende As Date
    Dim Color As Long
    Dim strEURN, strTextLine As String
    strEURN = CStr(HiddenGanttData.TabGanttLines.GetColumnValue(btnBarLineNo.Text, 0))
    
    Form1.CommonDialog1.ShowColor

    Color = Form1.CommonDialog1.Color
    Start = txtBarStartTime.Text
    Ende = txtBarEndTime.Text

    'first set the data in the container
    strTextLine = strEURN + "," + _
        CStr(txtBarKey.Text) + "," + _
        CStr(Start) + "," + _
        CStr(Ende) + "," + _
        txtBarCaption.Text + "," + _
        "1," + CStr(Color) + ",0,0,0,0,1," + _
        " ," + " ," + "0,0"
    HiddenGanttData.TabGanttBars.InsertTextLine strTextLine, False

    'send the new bar to the gantt
    Form1.Gantt.AddBarToLine btnBarLineNo.Text, txtBarKey.Text, _
                         Start, Ende, txtBarCaption.Text, 1, _
                         Color, 0, 0, 0, 0, "", "", 0, 0
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Slider2_Change()
    Slider2_Click
End Sub

Private Sub Slider2_Click()
    Form1.Gantt.ResizeAreaWidth = Slider2.Value
End Sub

Private Sub Slider3_Change()
    Slider3_Click
End Sub

Private Sub Slider3_Click()
    Form1.Gantt.ResizeMinimalWidth = Slider3.Value
End Sub

Private Sub Slider4_Change()
    Slider4_Click
End Sub

Private Sub Slider4_Click()
    Form1.Gantt.BarOverlapOffset = Slider4.Value
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Slider5_Change()
    Slider5_Click
End Sub

Private Sub Slider5_Click()
    Form1.Gantt.BarNumberExpandLine = Slider5.Value
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub btnAddSubBar_Click()
    Dim Start As Date
    Dim Ende As Date

    Start = CDate(txtSubBarStartTime.Text)
    Ende = CDate(txtSubBarEndTime.Text)

    Form1.Gantt.AddSubBar txtMainBarKey.Text, txtSubBarKey.Text, Start, Ende, txtSubBarText.Text, _
        CLng(txtSubBarShape.Text), CLng(txtSubBarColor.Text), CLng(txtSubBarTextColor.Text), _
        CLng(txtSplitColor.Text), CLng(txtSubBarLeftSplitColor.Text), CLng(txtSubBarRightSplitColor.Text), _
        txtSubBarLeftOutsideText.Text, txtSubBarRightOutsideText, _
        CLng(txtSubBarLeftOutsideTextColor.Text), CLng(txtSubBarRightOutsideTextColor.Text)
End Sub

Private Sub btnRemoveSubBar_Click()
    Form1.Gantt.RemoveSubBar txtRemoveSubBarMainKey.Text, txtRemoveSubBarKey.Text
End Sub

