VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormArrows 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormArrows"
   ClientHeight    =   2265
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4395
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2265
   ScaleWidth      =   4395
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Finish2Start 
      Caption         =   "Finish2Start"
      Height          =   285
      Left            =   0
      TabIndex        =   18
      Top             =   630
      Width           =   2085
   End
   Begin VB.TextBox txtFinish2Start_StartBar 
      Height          =   285
      Left            =   2310
      TabIndex        =   17
      Text            =   "51760088"
      ToolTipText     =   "Key of the StartBar"
      Top             =   630
      Width           =   960
   End
   Begin VB.TextBox txtFinish2Start_FinishBar 
      Height          =   285
      Left            =   3465
      TabIndex        =   16
      Text            =   "51760130"
      ToolTipText     =   "Key of the FinishBar"
      Top             =   630
      Width           =   855
   End
   Begin VB.TextBox txtFinish2Finish_FinishBar 
      Height          =   285
      Left            =   3465
      TabIndex        =   15
      Text            =   "51760130"
      ToolTipText     =   "Key of the FinishBar"
      Top             =   945
      Width           =   855
   End
   Begin VB.TextBox txtFinish2Finish_StartBar 
      Height          =   285
      Left            =   2310
      TabIndex        =   14
      Text            =   "51760088"
      ToolTipText     =   "Key of the StartBar"
      Top             =   945
      Width           =   960
   End
   Begin VB.CommandButton Finish2Finish 
      Caption         =   "Finish2Finish"
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   945
      Width           =   2085
   End
   Begin VB.TextBox txtStart2Finish_FinishBar 
      Height          =   285
      Left            =   3465
      TabIndex        =   12
      Text            =   "51760130"
      ToolTipText     =   "Key of the FinishBar"
      Top             =   1260
      Width           =   855
   End
   Begin VB.TextBox txtStart2Finish_StartBar 
      Height          =   285
      Left            =   2310
      TabIndex        =   11
      Text            =   "51760088"
      ToolTipText     =   "Key of the StartBar"
      Top             =   1260
      Width           =   960
   End
   Begin VB.CommandButton Start2Finish 
      Caption         =   "Start2Finish"
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   1260
      Width           =   2085
   End
   Begin VB.TextBox txtStart2Start_FinishBar 
      Height          =   285
      Left            =   3465
      TabIndex        =   9
      Text            =   "51760130"
      ToolTipText     =   "Key of the FinishBar"
      Top             =   1575
      Width           =   855
   End
   Begin VB.TextBox txtStart2Start_StartBar 
      Height          =   285
      Left            =   2310
      TabIndex        =   8
      Text            =   "51760088"
      ToolTipText     =   "Key of the StartBar"
      Top             =   1575
      Width           =   960
   End
   Begin VB.CommandButton Start2Start 
      Caption         =   "Start2Start"
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   1575
      Width           =   2085
   End
   Begin VB.TextBox txtArrowColNormal 
      Height          =   285
      Left            =   2310
      TabIndex        =   6
      Text            =   "0"
      ToolTipText     =   "Color (e.g. long-typ)"
      Top             =   315
      Width           =   960
   End
   Begin VB.CommandButton ArrowColNormal 
      Caption         =   "ArrowColNormal"
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   315
      Width           =   2085
   End
   Begin VB.TextBox txtArrowColSelected 
      Height          =   285
      Left            =   2310
      TabIndex        =   4
      Text            =   "255"
      ToolTipText     =   "Color (e.g. long-typ)"
      Top             =   0
      Width           =   960
   End
   Begin VB.CommandButton ArrowColSelected 
      Caption         =   "ArrowColSelected"
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   2085
   End
   Begin VB.CommandButton DeleteX2X 
      Caption         =   "DeleteX2X"
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   1890
      Width           =   2085
   End
   Begin VB.TextBox txtDeleteX2X_Key1 
      Height          =   285
      Left            =   2310
      TabIndex        =   1
      Text            =   "51760088"
      ToolTipText     =   "Key of the StartBar"
      Top             =   1890
      Width           =   960
   End
   Begin VB.TextBox txtDeleteX2X_Key2 
      Height          =   285
      Left            =   3465
      TabIndex        =   0
      Text            =   "51760130"
      ToolTipText     =   "Key of the FinishBar"
      Top             =   1890
      Width           =   855
   End
   Begin MSComctlLib.Slider Slider6 
      Height          =   225
      Left            =   3465
      TabIndex        =   19
      Top             =   315
      Width           =   960
      _ExtentX        =   1693
      _ExtentY        =   397
      _Version        =   393216
      Min             =   1
      Max             =   5
      SelStart        =   2
      Value           =   2
   End
   Begin VB.Label Label5 
      Caption         =   "ArrowWidth"
      Height          =   225
      Left            =   3465
      TabIndex        =   20
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "FormArrows"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ArrowColNormal_Click()
    Form1.Gantt.ArrowColorNormal = txtArrowColNormal.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub ArrowColSelected_Click()
    Form1.Gantt.ArrowColorSelected = txtArrowColSelected.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Finish2Finish_Click()
    Form1.Gantt.SetFinish2Finish txtFinish2Finish_StartBar.Text, txtFinish2Finish_FinishBar.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Finish2Start_Click()
    Form1.Gantt.SetFinish2Start txtFinish2Start_StartBar.Text, txtFinish2Start_FinishBar.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Start2Finish_Click()
    Form1.Gantt.SetStart2Finish txtStart2Finish_StartBar.Text, txtStart2Finish_FinishBar.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Start2Start_Click()
    Form1.Gantt.SetStart2Start txtStart2Start_StartBar.Text, txtStart2Start_FinishBar.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub DeleteX2X_Click()
    Form1.Gantt.DeleteX2X txtDeleteX2X_Key1.Text, txtDeleteX2X_Key2.Text
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub Slider6_Click()
    Form1.Gantt.ArrowWidth = Slider6.Value
    Form1.Gantt.RefreshArea "GANTT"
End Sub
