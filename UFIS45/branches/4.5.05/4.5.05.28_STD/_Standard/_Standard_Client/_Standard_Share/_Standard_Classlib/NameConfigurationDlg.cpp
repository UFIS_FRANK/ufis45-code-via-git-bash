// NameConfigurationDlg.cpp: Implementierung der Klasse NameConfigurationDlg.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "NameConfigurationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

NameConfigurationDlg::NameConfigurationDlg(CStringArray* prpStrings, CString opConfigString, CWnd* pParent /*=NULL*/)
	: CDialog(NameConfigurationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(NameConfigurationDlg)
		// HINWEIS: Der Klassenassistent f�gt hier Member-Initialisierung ein
	//}}AFX_DATA_INIT

	for (int i = 0; i < prpStrings->GetSize(); i++)
	{
		omStringArr.Add(prpStrings->GetAt(i));
	}

	omConfigString = opConfigString;
	omConfigSeparator = "<+>";

	// holding the names of the database fields
	omFieldArray.Add("");
	omFieldArray.Add("<FINM>");
	omFieldArray.Add("<LANM>");
	omFieldArray.Add("<SHNM>");
	omFieldArray.Add("<PERC>");
	omFieldArray.Add("<PENO>");

	imMaxNameLen = -1; // -1 means no restriction on name length
}

BEGIN_MESSAGE_MAP(NameConfigurationDlg, CDialog)
	//{{AFX_MSG_MAP(NameConfigurationDlg)
	ON_CBN_SELCHANGE(IDC_C_SEL1_NAMECONFIG, OnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_C_SEL2_NAMECONFIG, OnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_C_SEL3_NAMECONFIG, OnSelchangeCombo3)
	ON_CBN_SELCHANGE(IDC_C_SEL4_NAMECONFIG, OnSelchangeCombo4)
	ON_CBN_SELCHANGE(IDC_C_SEL5_NAMECONFIG, OnSelchangeCombo5)
	ON_EN_CHANGE(IDC_E_FINM_NAMECONFIG, OnChangeFinm)
	ON_EN_CHANGE(IDC_E_LANM_NAMECONFIG, OnChangeLanm)
	ON_EN_CHANGE(IDC_E_PERC_NAMECONFIG, OnChangePerc)
	ON_EN_CHANGE(IDC_E_SHNM_NAMECONFIG, OnChangeShnm)
	ON_EN_CHANGE(IDC_E_PENO_NAMECONFIG, OnChangeEPeno)
	ON_EN_CHANGE(IDC_E_CHAR1_NAMECONFIG, OnChangeEChar1)
	ON_EN_CHANGE(IDC_E_CHAR2_NAMECONFIG, OnChangeEChar2)
	ON_EN_CHANGE(IDC_E_CHAR3_NAMECONFIG, OnChangeEChar3)
	ON_EN_CHANGE(IDC_E_CHAR4_NAMECONFIG, OnChangeEChar4)
	ON_EN_CHANGE(IDC_E_CHAR5_NAMECONFIG, OnChangeEChar5)
	ON_EN_CHANGE(IDC_E_SUFFIX1_NAMECONFIG, OnChangeESuffix1)
	ON_EN_CHANGE(IDC_E_SUFFIX2_NAMECONFIG, OnChangeESuffix2)
	ON_EN_CHANGE(IDC_E_SUFFIX3_NAMECONFIG, OnChangeESuffix3)
	ON_EN_CHANGE(IDC_E_SUFFIX4_NAMECONFIG, OnChangeESuffix4)
	ON_EN_CHANGE(IDC_E_SUFFIX5_NAMECONFIG, OnChangeESuffix5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void NameConfigurationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NameConfigurationDlg)
	DDX_Control(pDX, IDC_C_SEL5_NAMECONFIG, m_Combo5);
	DDX_Control(pDX, IDC_C_SEL4_NAMECONFIG, m_Combo4);
	DDX_Control(pDX, IDC_C_SEL3_NAMECONFIG, m_Combo3);
	DDX_Control(pDX, IDC_C_SEL2_NAMECONFIG, m_Combo2);
	DDX_Control(pDX, IDC_C_SEL1_NAMECONFIG, m_Combo1);
	//}}AFX_DATA_MAP
}

NameConfigurationDlg::~NameConfigurationDlg()
{

}

int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa)
{
	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}

BOOL NameConfigurationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText (IDC_E_LANM_NAMECONFIG, "Miller");
	SetDlgItemText (IDC_E_FINM_NAMECONFIG, "John");
	SetDlgItemText (IDC_E_SHNM_NAMECONFIG, "Johnny");
	SetDlgItemText (IDC_E_PERC_NAMECONFIG, "JMI");
	SetDlgItemText (IDC_E_PENO_NAMECONFIG, "06120078");

	InitLabels(&omStringArr);
	InitCombos();
	InitConfigString(omConfigString);

	return TRUE;
}

void NameConfigurationDlg::OnOK() 
{
	BuildConfigString();

	CDialog::OnOK();
}

//
// Building a new config strig if the user pressed OK
//
void NameConfigurationDlg::BuildConfigString()
{
	int ilSel;
	bool blContinue = true;
	CString olConfigString = "";
	CString olTmp;

	for (int i = 0; i < 5 && blContinue; i++)
	{
		switch (i)
		{
		case 0:
			ilSel = m_Combo1.GetCurSel();
			if (ilSel > -1 && ilSel < omFieldArray.GetSize())
			{
				olConfigString += omConfigSeparator + omFieldArray[ilSel];
				GetDlgItemText(IDC_E_CHAR1_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
				GetDlgItemText(IDC_E_SUFFIX1_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
			}
			else
			{
				blContinue = false;
			}
			break;
		case 1:
			ilSel = m_Combo2.GetCurSel();
			if (ilSel > -1 && ilSel < omFieldArray.GetSize())
			{
				olConfigString += omConfigSeparator + omFieldArray[ilSel];
				GetDlgItemText(IDC_E_CHAR2_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
				GetDlgItemText(IDC_E_SUFFIX2_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
			}
			else
			{
				blContinue = false;
			}
			break;
		case 2:
			ilSel = m_Combo3.GetCurSel();
			if (ilSel > -1 && ilSel < omFieldArray.GetSize())
			{
				olConfigString += omConfigSeparator + omFieldArray[ilSel];
				GetDlgItemText(IDC_E_CHAR3_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
				GetDlgItemText(IDC_E_SUFFIX3_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
			}
			else
			{
				blContinue = false;
			}
			break;
		case 3:
			ilSel = m_Combo4.GetCurSel();
			if (ilSel > -1 && ilSel < omFieldArray.GetSize())
			{
				olConfigString += omConfigSeparator + omFieldArray[ilSel];
				GetDlgItemText(IDC_E_CHAR4_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
				GetDlgItemText(IDC_E_SUFFIX4_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
			}
			else
			{
				blContinue = false;
			}
			break;
		case 4:
			ilSel = m_Combo5.GetCurSel();
			if (ilSel > -1 && ilSel < omFieldArray.GetSize())
			{
				olConfigString += omConfigSeparator + omFieldArray[ilSel];
				GetDlgItemText(IDC_E_CHAR5_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
				GetDlgItemText(IDC_E_SUFFIX5_NAMECONFIG, olTmp);
				olConfigString += omConfigSeparator + olTmp;
			}
			else
			{
				blContinue = false;
			}
			break;
		}
	}
	if (olConfigString.GetLength())
	{
		omConfigString = olConfigString.Mid(omConfigSeparator.GetLength());
		omConfigStringArr.RemoveAll();
		ExtractTextLineFast(omConfigStringArr, olConfigString.Mid(1).GetBuffer(0), omConfigSeparator.GetBuffer(0));
		while (omConfigStringArr.GetSize() % 3 != 0)
		{
			omConfigStringArr.Add ("");
		}
	}
}

void NameConfigurationDlg::OnCancel() 
{
	CDialog::OnCancel();
}

//
// building the preview depending on the user input
//
void NameConfigurationDlg::RebuildPreview()
{
	CStringArray olFieldNameArray;
	CString olTmp;
	CString olResult;
	CString olText;
	CString olSuffix;
	CString olLength;
	int ilSel;

	olTmp = "";
	olFieldNameArray.Add(olTmp);
	GetDlgItemText(IDC_E_FINM_NAMECONFIG, olTmp);
	olFieldNameArray.Add(olTmp);
	GetDlgItemText(IDC_E_LANM_NAMECONFIG, olTmp);
	olFieldNameArray.Add(olTmp);
	GetDlgItemText(IDC_E_SHNM_NAMECONFIG, olTmp);
	olFieldNameArray.Add(olTmp);
	GetDlgItemText(IDC_E_PERC_NAMECONFIG, olTmp);
	olFieldNameArray.Add(olTmp);
	GetDlgItemText(IDC_E_PENO_NAMECONFIG, olTmp);
	olFieldNameArray.Add(olTmp);

	// building the preview
	for (int i = 0; i < 5; i++)
	{
		switch (i)
		{
		case 0:
			ilSel = m_Combo1.GetCurSel();
			if (ilSel > -1 && ilSel < olFieldNameArray.GetSize())
			{
				olText = olFieldNameArray[ilSel];
			}
			GetDlgItemText(IDC_E_CHAR1_NAMECONFIG, olLength);
			GetDlgItemText(IDC_E_SUFFIX1_NAMECONFIG, olSuffix);
			break;
		case 1:
			ilSel = m_Combo2.GetCurSel();
			if (ilSel > -1 && ilSel < olFieldNameArray.GetSize())
			{
				olText = olFieldNameArray[ilSel];
			}
			GetDlgItemText(IDC_E_CHAR2_NAMECONFIG, olLength);
			GetDlgItemText(IDC_E_SUFFIX2_NAMECONFIG, olSuffix);
			break;
		case 2:
			ilSel = m_Combo3.GetCurSel();
			if (ilSel > -1 && ilSel < olFieldNameArray.GetSize())
			{
				olText = olFieldNameArray[ilSel];
			}
			GetDlgItemText(IDC_E_CHAR3_NAMECONFIG, olLength);
			GetDlgItemText(IDC_E_SUFFIX3_NAMECONFIG, olSuffix);
			break;
		case 3:
			ilSel = m_Combo4.GetCurSel();
			if (ilSel > -1 && ilSel < olFieldNameArray.GetSize())
			{
				olText = olFieldNameArray[ilSel];
			}
			GetDlgItemText(IDC_E_CHAR4_NAMECONFIG, olLength);
			GetDlgItemText(IDC_E_SUFFIX4_NAMECONFIG, olSuffix);
			break;
		case 4:
			ilSel = m_Combo5.GetCurSel();
			if (ilSel > -1 && ilSel < olFieldNameArray.GetSize())
			{
				olText = olFieldNameArray[ilSel];
			}
			GetDlgItemText(IDC_E_CHAR5_NAMECONFIG, olLength);
			GetDlgItemText(IDC_E_SUFFIX5_NAMECONFIG, olSuffix);
			break;
		}
		olResult += GetFormatedString (olText.GetBuffer(0), olLength.GetBuffer(0), olSuffix.GetBuffer(0));
	}

	if(imMaxNameLen > 0)
	{
		olResult = olResult.Left(imMaxNameLen);
	}

	SetDlgItemText (IDC_E_PREVIEW_NAMECONFIG, olResult);
}

CString NameConfigurationDlg::GetFormatedString(char *pcpText, char *pcpLength, char *pcpSuffix)
{
	CString olRet = "";
	int ilLength = atoi(pcpLength);
	if (ilLength < 1)
		return olRet;

	int ilRealLength = strlen(pcpText);
	int ilSuffixLength = strlen(pcpSuffix);

	char *cpRet = (char*) malloc(ilRealLength + ilSuffixLength + 2);
	memset(cpRet,'\0',sizeof(*cpRet));

	int ilCopy = min(ilLength, ilRealLength);

	strncpy(cpRet, pcpText, ilCopy);
	cpRet[ilCopy] = '\0';
	strcat(cpRet, pcpSuffix);
	olRet = CString (cpRet); 
	delete cpRet;
	return olRet;
}

//
// Changing the selection of a combobox
//
void NameConfigurationDlg::OnSelchangeCombo1() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnSelchangeCombo2() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnSelchangeCombo3() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnSelchangeCombo4() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnSelchangeCombo5() 
{
	RebuildPreview();
}

//
// Changing the example data
//
void NameConfigurationDlg::OnChangeFinm() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeLanm() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangePerc() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeShnm() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeEPeno() 
{
	RebuildPreview();
}

//
// setting the text of the comboboxes
//
void NameConfigurationDlg::InitCombos()
{
	GetDlgItemText(IDC_S_LANM_NAMECONFIG, olLANM);
	GetDlgItemText(IDC_S_FINM_NAMECONFIG, olFINM);
	GetDlgItemText(IDC_S_PERC_NAMECONFIG, olPERC);
	GetDlgItemText(IDC_S_SHNM_NAMECONFIG, olSHNM);
	GetDlgItemText(IDC_S_PENO_NAMECONFIG, olPENO);

	olFINM = olFINM.Left(olFINM.GetLength() - 1);
	olLANM = olLANM.Left(olLANM.GetLength() - 1);
	olSHNM = olSHNM.Left(olSHNM.GetLength() - 1);
	olPERC = olPERC.Left(olPERC.GetLength() - 1);
	olPENO = olPENO.Left(olPENO.GetLength() - 1);

	m_Combo1.ResetContent();
	m_Combo2.ResetContent();
	m_Combo3.ResetContent();
	m_Combo4.ResetContent();
	m_Combo5.ResetContent();

	m_Combo1.AddString("");
	m_Combo2.AddString("");
	m_Combo3.AddString("");
	m_Combo4.AddString("");
	m_Combo5.AddString("");

	m_Combo1.AddString(olFINM);
	m_Combo2.AddString(olFINM);
	m_Combo3.AddString(olFINM);
	m_Combo4.AddString(olFINM);
	m_Combo5.AddString(olFINM);

	m_Combo1.AddString(olLANM);
	m_Combo2.AddString(olLANM);
	m_Combo3.AddString(olLANM);
	m_Combo4.AddString(olLANM);
	m_Combo5.AddString(olLANM);

	m_Combo1.AddString(olSHNM);
	m_Combo2.AddString(olSHNM);
	m_Combo3.AddString(olSHNM);
	m_Combo4.AddString(olSHNM);
	m_Combo5.AddString(olSHNM);

	m_Combo1.AddString(olPERC);
	m_Combo2.AddString(olPERC);
	m_Combo3.AddString(olPERC);
	m_Combo4.AddString(olPERC);
	m_Combo5.AddString(olPERC);

	m_Combo1.AddString(olPENO);
	m_Combo2.AddString(olPENO);
	m_Combo3.AddString(olPENO);
	m_Combo4.AddString(olPENO);
	m_Combo5.AddString(olPENO);
}

bool NameConfigurationDlg::CheckInput()
{
	bool blRet = false;

	return blRet;
}

//
// changing any number of characters
//
void NameConfigurationDlg::OnChangeEChar1() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeEChar2() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeEChar3() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeEChar4() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeEChar5() 
{
	RebuildPreview();
}

//
// changing any suffix
//
void NameConfigurationDlg::OnChangeESuffix1() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeESuffix2() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeESuffix3() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeESuffix4() 
{
	RebuildPreview();
}

void NameConfigurationDlg::OnChangeESuffix5() 
{
	RebuildPreview();
}

//
// setting the configuration string for getting formatted strings
//
void NameConfigurationDlg::SetConfigString(CString opConfigString)
{
	omConfigString = opConfigString;
	omConfigStringArr.RemoveAll();
	ExtractTextLineFast(omConfigStringArr, opConfigString.GetBuffer(0), omConfigSeparator.GetBuffer(0));
	while (omConfigStringArr.GetSize() % 3 != 0)
	{
		omConfigStringArr.Add ("");
	}
}

//
// returning the configuration string so that the container application can save it
//
CString NameConfigurationDlg::GetConfigString()
{
	return omConfigString;
}

//
// setting the initial selection of the combo boxes if there is already a config string
//
void NameConfigurationDlg::SetComboSelection(CComboBox *prpCombo, CString opFieldName)
{
	CString olSearch;
	int ilRet;

	if (opFieldName == omFieldArray[1]) //FINM
	{
		GetDlgItemText(IDC_S_FINM_NAMECONFIG, olSearch);
		if (olSearch.GetLength())
		{
			olSearch = olSearch.Left(olSearch.GetLength() - 1);
			ilRet = prpCombo->FindStringExact(-1, olSearch);
			if (ilRet != CB_ERR)
			{
				prpCombo->SetCurSel(ilRet);
			}
		}
	}
	else if (opFieldName == omFieldArray[2]) //LANM
	{
		GetDlgItemText(IDC_S_LANM_NAMECONFIG, olSearch);
		if (olSearch.GetLength())
		{
			olSearch = olSearch.Left(olSearch.GetLength() - 1);
			ilRet = prpCombo->FindStringExact(-1, olSearch);
			if (ilRet != CB_ERR)
			{
				prpCombo->SetCurSel(ilRet);
			}
		}
	}
	else if (opFieldName == omFieldArray[3]) //SHNM
	{
		GetDlgItemText(IDC_S_SHNM_NAMECONFIG, olSearch);
		olSearch = olSearch.Left(olSearch.GetLength() - 1);
		ilRet = prpCombo->FindStringExact(-1, olSearch);
		if (ilRet != CB_ERR)
		{
			prpCombo->SetCurSel(ilRet);
		}
	}
	else if (opFieldName == omFieldArray[4]) //PERC
	{
		GetDlgItemText(IDC_S_PERC_NAMECONFIG, olSearch);
		olSearch = olSearch.Left(olSearch.GetLength() - 1);
		ilRet = prpCombo->FindStringExact(-1, olSearch);
		if (ilRet != CB_ERR)
		{
			prpCombo->SetCurSel(ilRet);
		}
	}
	else if (opFieldName == omFieldArray[5]) //PENO
	{
		GetDlgItemText(IDC_S_PENO_NAMECONFIG, olSearch);
		olSearch = olSearch.Left(olSearch.GetLength() - 1);
		ilRet = prpCombo->FindStringExact(-1, olSearch);
		if (ilRet != CB_ERR)
		{
			prpCombo->SetCurSel(ilRet);
		}
	}
}

//
// order of the strings in the CStringArray:
//
// IDD_NAME_CONFIGURATION - dialog title
// IDC_S_FINM - first name
// IDC_S_LANM - last name
// IDC_S_SHNM - shortname/nickname
// IDC_S_PERC - initials
// IDC_S_PENO - personnal no.
// IDC_S_FIELD_NAME - "field:"
// IDC_S_NO_CHARS - "No. of chars:"
// IDC_S_SUFFIX - "Suffix:"
// IDC_S_PREVIEW - "Preview:"
//
void NameConfigurationDlg::InitLabels(CStringArray *ropStrings)
{
	for (int i = 0; i < ropStrings->GetSize(); i++)
	{
		switch (i)
		{
		case 0:
			this->SetWindowText(ropStrings->GetAt(i));
			break;
		case 1:
			SetDlgItemText (IDC_S_FINM_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 2:
			SetDlgItemText (IDC_S_LANM_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 3:
			SetDlgItemText (IDC_S_SHNM_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 4:
			SetDlgItemText (IDC_S_PERC_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 5:
			SetDlgItemText (IDC_S_PENO_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 6:
			SetDlgItemText (IDC_S_FIELD_NAME_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 7:
			SetDlgItemText (IDC_S_NO_CHARS_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 8:
			SetDlgItemText (IDC_S_SUFFIX_NAMECONFIG, ropStrings->GetAt(i));
			break;
		case 9:
			SetDlgItemText (IDC_S_PREVIEW_NAMECONFIG, ropStrings->GetAt(i));
			break;
		}
	}
}

void NameConfigurationDlg::InitConfigString(CString opConfigString)
{
	CStringArray olArr;
	int ilCnt = ExtractTextLineFast(olArr, opConfigString.GetBuffer(0), omConfigSeparator.GetBuffer(0));
	int ilRest;
	int ilRow;

	for (int i = 0; i < ilCnt; i++)
	{
		ilRest = i % 3;
		ilRow  = (int)(i / 3);
		switch (ilRest)
		{
		case 0:
			if (ilRow == 0)
			{
				SetComboSelection(&m_Combo1, olArr[i]);
			}
			else if (ilRow == 1)
			{
				SetComboSelection(&m_Combo2, olArr[i]);
			}
			else if (ilRow == 2)
			{
				SetComboSelection(&m_Combo3, olArr[i]);
			}
			else if (ilRow == 3)
			{
				SetComboSelection(&m_Combo4, olArr[i]);
			}
			else if (ilRow == 4)
			{
				SetComboSelection(&m_Combo5, olArr[i]);
			}
			break;
		case 1:
			if (ilRow == 0)
			{
				SetDlgItemText(IDC_E_CHAR1_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 1)
			{
				SetDlgItemText(IDC_E_CHAR2_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 2)
			{
				SetDlgItemText(IDC_E_CHAR3_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 3)
			{
				SetDlgItemText(IDC_E_CHAR4_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 4)
			{
				SetDlgItemText(IDC_E_CHAR5_NAMECONFIG, olArr[i]);
			}
			break;
		case 2:
			if (ilRow == 0)
			{
				SetDlgItemText(IDC_E_SUFFIX1_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 1)
			{
				SetDlgItemText(IDC_E_SUFFIX2_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 2)
			{
				SetDlgItemText(IDC_E_SUFFIX3_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 3)
			{
				SetDlgItemText(IDC_E_SUFFIX4_NAMECONFIG, olArr[i]);
			}
			else if (ilRow == 4)
			{
				SetDlgItemText(IDC_E_SUFFIX5_NAMECONFIG, olArr[i]);
			}
			break;
		}
	}
}

CString NameConfigurationDlg::GetNameString(char *cpFirstName, char *cpLastName, char *cpNickName, char *cpInitial, char* cpPeno)
{
	CString olRet = "";
	CString olTmp;
	int ilCnt = omConfigStringArr.GetSize() / 3;

	for (int i = 0; i < ilCnt; i++)
	{
		olTmp = omConfigStringArr[i*3];
		if (strstr(olTmp.GetBuffer(0), omFieldArray[1]) != NULL) //FINM
		{
			olRet += GetFormatedString (cpFirstName, omConfigStringArr[i*3+1].GetBuffer(0), omConfigStringArr[i*3+2].GetBuffer(0));
		}
		else if (strstr(olTmp.GetBuffer(0), omFieldArray[2]) != NULL) //LANM
		{
			olRet += GetFormatedString (cpLastName, omConfigStringArr[i*3+1].GetBuffer(0), omConfigStringArr[i*3+2].GetBuffer(0));
		}
		else if (strstr(olTmp.GetBuffer(0), omFieldArray[3]) != NULL) //SHNM
		{
			olRet += GetFormatedString (cpNickName, omConfigStringArr[i*3+1].GetBuffer(0), omConfigStringArr[i*3+2].GetBuffer(0));
		}
		else if (strstr(olTmp.GetBuffer(0), omFieldArray[4]) != NULL) //PERC
		{
			olRet += GetFormatedString (cpInitial, omConfigStringArr[i*3+1].GetBuffer(0), omConfigStringArr[i*3+2].GetBuffer(0));
		}
		else if (strstr(olTmp.GetBuffer(0), omFieldArray[5]) != NULL) //PENO
		{
			olRet += GetFormatedString (cpPeno, omConfigStringArr[i*3+1].GetBuffer(0), omConfigStringArr[i*3+2].GetBuffer(0));
		}
	}
	return olRet;
}

void NameConfigurationDlg::SetConfigStringSeparator(CString opSeparator)
{
	omConfigSeparator = opSeparator;
}

CString NameConfigurationDlg::GetConfigStringSeparator()
{
	return omConfigSeparator;
}

void NameConfigurationDlg::SetDatabaseFieldNames(CStringArray opArr)
{
	// holding the names of the database fields
	omFieldArray.RemoveAll();
	omFieldArray.Add("");

	for (int i = 0; i < opArr.GetSize(); i++)
	{
		omFieldArray.Add(opArr[i]);
	}
}

void NameConfigurationDlg::SetMaxNameLength(int ipMaxNameLen)
{
	imMaxNameLen = ipMaxNameLen;
}