// CCSTree.cpp : implementation file
//

#include <stdafx.h>
#include <CCSTree.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCSTree


CCSTree::CCSTree(CWnd *popParentWindow)
{
	bmHasButtons = false;
	bmDeleteMode = false;
	bmMultiSelection = false;
	pomParentWindow = popParentWindow;
	omBKColor = RGB(255,255,255);
	omTextColor = RGB(0,0,0);
}


CCSTree::CCSTree(CWnd *popParentWindow, int ipXStart, int ipXEnd, int ipYStart, int ipYEnd, int ipCXBitmap,  int ipCYBitmap, bool bpMultiSelection)
{	
	omBKColor = RGB(255,255,255);
	omTextColor = RGB(0,0,0);
	bmHasButtons = false;
	bmDeleteMode = false;
	SetTreeData(popParentWindow, ipXStart, ipXEnd, ipYStart, ipYEnd, ipCXBitmap, ipCYBitmap, bpMultiSelection);
}


bool CCSTree::SetTreeData(CWnd *popParentWindow, int ipXStart, int ipXEnd, int ipYStart, int ipYEnd, int ipCXBitmap,  int ipCYBitmap, bool bpMultiSelection)
{	
    // destroy the window if parent window changed
    if (popParentWindow != pomParentWindow)
        DestroyWindow();    // this works even if m_hWnd == NULL.

    // initialize all data members
	bmMultiSelection = bpMultiSelection;
    pomParentWindow = popParentWindow;
    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);
	imCXBitmap = ipCXBitmap;
	imCYBitmap = ipCYBitmap;

    if (m_hWnd == NULL) // window was not created yet?
    {
        // create the table window

		DWORD lWndStyle =	WS_VISIBLE		|	WS_CHILD	| WS_DLGFRAME		| 
							WS_VSCROLL		|	WS_HSCROLL  | TVS_SHOWSELALWAYS | 
							TVS_HASBUTTONS	|	TVS_HASLINES |TVS_LINESATROOT ;
							//| TVS_EDITLABELS;
			
		CRect rectWnd(imXStart, imYStart, imXEnd, imYEnd);
		if (Create(lWndStyle, rectWnd, pomParentWindow, 0) == 0)
		{
			return false;
		}
		if((imCXBitmap > 0) && (imCYBitmap > 0))
		{
			bmHasButtons = true;
			omImageList.Create( imCXBitmap, imCYBitmap, false, 0, 2 );
			omImageList.SetBkColor(GetSysColor(COLOR_WINDOW));
		}
	}
	else
	{
	    SetPosition(imXStart, imXEnd, imYStart, imYEnd);
	}
    return true;
}


bool CCSTree::SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd)
{	
	bool blRepaint = true;

	if (abs(ipXEnd-ipXStart+1) == imXEnd-imXStart+1 &&
        abs(ipYEnd-ipYStart+1) == imYEnd-imYStart+1)
        blRepaint = false;

    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);

    // reposition the window, including the owner-drawn list box controls
    if (m_hWnd != NULL)
    {
        MoveWindow(imXStart, imYStart, imXEnd, imYEnd, blRepaint);
    }
	return true;
}


CCSTree::~CCSTree()
{
}


BEGIN_MESSAGE_MAP(CCSTree, CTreeCtrl)
	//{{AFX_MSG_MAP(CCSTree)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(TVN_BEGINLABELEDIT, OnBeginlabeledit)
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnSelchanging)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDED, OnItemexpanded)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSTree message handlers


bool CCSTree::SetTreeItemDef(int ipItemType, UINT ipBitmapID, UINT ipSelBitmapID, int ipTextLimit, bool bpEdit, bool bpMultiSel, bool bpBold)
{
	TREEITEMDEF rlTIDef;
	CBitmap olBM;
	CBitmap olBMSel;
	int ilSize = 0;
	
	if(bmHasButtons)
	{
		if(olBM.LoadBitmap(ipBitmapID) == 0) return false;
		if(olBMSel.LoadBitmap(ipSelBitmapID) == 0) olBMSel.LoadBitmap(ipBitmapID);
	}
	for(int ilLc = omTreeItemDefs.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		if(omTreeItemDefs[ilLc].ItemType == ipItemType)
		{
			omTreeItemDefs[ilLc].Edit = bpEdit;
			omTreeItemDefs[ilLc].TextLimit = ipTextLimit;
			omTreeItemDefs[ilLc].Bold = bpBold;
			omTreeItemDefs[ilLc].MultiSel = bpMultiSel;
			if(bmHasButtons)
			{
				omImageList.Replace(omTreeItemDefs[ilLc].Image, &olBM, NULL);
				omImageList.Replace(omTreeItemDefs[ilLc].SelImage, &olBMSel, NULL);
			}
			return true;
		}
	}
	if(bmHasButtons)
	{
		ilSize = omImageList.GetImageCount();
		omImageList.Add(&olBM,RGB(255,0,255));
		omImageList.Add(&olBMSel,RGB(255,0,255));
		/// Attach image list to Tree
		SetImageList(&omImageList,TVSIL_NORMAL);
	}

	rlTIDef.ItemType = ipItemType;
	rlTIDef.Image = ilSize ;
	rlTIDef.SelImage = ilSize + 1;
	rlTIDef.Edit = bpEdit;
	rlTIDef.MultiSel = bpMultiSel;
	rlTIDef.TextLimit = ipTextLimit;
	rlTIDef.Bold = bpBold;

	omTreeItemDefs.NewAt(0,rlTIDef);
	return true;
}


void CCSTree::SetMultiSelect(bool bpMultiSelect)
{
	if(bpMultiSelect)
		bmMultiSelection = true;
	else
		bmMultiSelection = false;
}

void CCSTree::SetLabelEdit(bool bpLabelEdit)
{
	if(bpLabelEdit)
		ModifyStyle(NULL, TVS_EDITLABELS, 0);
	else
		ModifyStyle(TVS_EDITLABELS,NULL, 0);
}


int CCSTree::GetImageByDef(int ipItemType)
{
	for(int ilLc = omTreeItemDefs.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		if(omTreeItemDefs[ilLc].ItemType == ipItemType)
			return omTreeItemDefs[ilLc].Image;
	}
	return -1;
}


int CCSTree::GetSelImageByDef(int ipItemType)
{
	for(int ilLc = omTreeItemDefs.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		if(omTreeItemDefs[ilLc].ItemType == ipItemType)
			return omTreeItemDefs[ilLc].SelImage;
	}
	return -1;
}


TREEITEMDEF *CCSTree::GetTreeItemDef(int ipItemType)
{
	for(int ilLc = omTreeItemDefs.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		if(omTreeItemDefs[ilLc].ItemType == ipItemType)
			return &omTreeItemDefs[ilLc];
	}
	return NULL;
}


bool CCSTree::TestItemParent(TREEITEM *prpTI)
{
	CCSTREEITEM *prlCCSTI;
	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) != 0)
		return false;
	if(prpTI->Parent != prlCCSTI->Parent->pData)
		return false;
	return true;
}


bool CCSTree::TestItemPos(TREEITEM *prpTI, TREEITEM *prpTIAfter)
{
	CCSTREEITEM *prlCCSTI;
	int ilLc;
	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) == 0)
		return false;
	if(prlCCSTI->ChildItems.GetSize() == 0)
		return false;

	if(prpTIAfter == NULL)
	{
		if(prlCCSTI->ChildItems[0].pData != prpTI)
			return false;
	}
	else
	{
		for(ilLc = 1; ilLc < prlCCSTI->ChildItems.GetSize(); ilLc++)
		{
			if(prlCCSTI->ChildItems[ilLc].pData == prpTI)
			{
				if(prlCCSTI->ChildItems[ilLc - 1].pData == prpTIAfter)
				{
					return true;
				}
			}
		}
		return false;
	}
	return true;
}


bool CCSTree::InsertItem(TREEITEM *prpTI, TREEITEM *prpTIAfter, bool bpSelect)
{
	return InsertItem(prpTI, GetTreeItemDef(prpTI->ItemType), prpTIAfter, bpSelect);
}


bool CCSTree::InsertItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef, TREEITEM *prpTIAfter, bool bpSelect)
{
	CCSTREEITEM		*prlCCSTI;
	CCSTREEITEM		*prlCCSTINew;
	TV_ITEM			tvItem;
	TV_INSERTSTRUCT tvInsert;
	tvItem.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_STATE | TVIF_PARAM;

	CString			olStr;
	int				ilLc = 0;
	TREEITEMDEF		*prlTIDef = prpTIDef;

	
	//TRACE	///////////////////////////////////////////////////////////////////////////////
	//TraceTree();
	//CString olAfter;
	
	//if(prpTI->Text == "WTYP")
	//		prpTI->Text = prpTI->Text;

	//if(prpTIAfter != NULL)
	//	olAfter = prpTIAfter->Text;
	//TRACE("\n Insert: %s   after: %s\n",prpTI->Text,olAfter); 
	//TRACE("\n %d\n",omPtrMap.GetCount()); 
	///////////////////////////////////////////////////////////////////////////////////////


	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) != 0)
	{
		if((TestItemPos(prpTI,prpTIAfter) == false) || (TestItemParent(prpTI) == false))
		{
			DeleteItem(prpTI);
		}
	}

	if(prlTIDef == NULL)
		prlTIDef = GetTreeItemDef(prpTI->ItemType);
	if(prlTIDef == NULL)
		return false;

	prlCCSTINew = new CCSTREEITEM;
	
	if(prpTI->Parent != NULL) 
	{
		// Search the parent item
		if(omPtrMap.Lookup((void *&)prpTI->Parent, (void *&)prlCCSTI) == 0)
		{
			delete prlCCSTINew;
			return false;
		}
		prlCCSTINew->Parent = prlCCSTI;
		// insert after ?
		if((prpTIAfter != NULL) && (prlCCSTI->ChildItems.GetSize() > 0))
		{
			for(ilLc = 0; ilLc < prlCCSTI->ChildItems.GetSize() ; ilLc++)
			{
				if((prlCCSTI->pData == prpTIAfter) || (ilLc == prlCCSTI->ChildItems.GetSize() - 1))
					break;
			}	
		}
		else
			ilLc = -1;
	}
	else
	{
		prlCCSTINew->Parent = NULL;
		// insert after ?
		if(prpTIAfter != NULL)
		{
			for(ilLc = 0; ilLc < omRootItems.GetSize() ; ilLc++)
			{
				if((omRootItems[ilLc].pData == prpTIAfter) || (ilLc == omRootItems.GetSize() - 1))
					break;
			}	
		}
		else
			ilLc = -1;
	}

	prlCCSTINew->Edit			= prpTIDef->Edit;
	prlCCSTINew->Bold			= prpTIDef->Bold;
	prlCCSTINew->ItemType		= prpTIDef->ItemType;
	prlCCSTINew->MultiSel		= prpTIDef->MultiSel;
	prlCCSTINew->TextLimit		= prpTIDef->TextLimit;
	prlCCSTINew->pData			= prpTI;

	//tvItem.iSelectedImage = GetSelImageByDef(prpTIDef->ItemType);
	//tvItem.iImage = GetImageByDef(prpTIDef->ItemType);
	tvItem.iSelectedImage = prlTIDef->SelImage;
	tvItem.iImage = prlTIDef->Image;
	
	
	tvItem.cchTextMax = prpTIDef->TextLimit;
	olStr = prpTI->Text.Left(prpTIDef->TextLimit);
	tvItem.pszText = olStr.GetBuffer(prpTIDef->TextLimit);
	olStr.ReleaseBuffer();
	tvItem.lParam = (LPARAM)prlCCSTINew;
	
	prlCCSTINew->Image =  tvItem.iImage;
	prlCCSTINew->SelImage =  tvItem.iSelectedImage;


	tvItem.stateMask = TVIS_OVERLAYMASK | TVIS_BOLD | TVIS_EXPANDED | TVIS_SELECTED;
	tvItem.state = 0;
	if(prpTIDef->Bold) tvItem.state |= TVIS_BOLD;  

	if(prlCCSTINew->Parent == NULL)
		tvInsert.hParent = TVI_ROOT;	
	else
		tvInsert.hParent = prlCCSTI->hItem;	

	if(ilLc < 0)
		tvInsert.hInsertAfter = TVI_FIRST;	
	else
	{
		if(prpTI->Parent != NULL) 
			tvInsert.hInsertAfter = prlCCSTI->ChildItems[ilLc].hItem;
		else
			tvInsert.hInsertAfter = omRootItems[ilLc].hItem;
	}
	tvInsert.item = tvItem;
	prlCCSTINew->hItem = CTreeCtrl::InsertItem(&tvInsert);
	TRACE("\n %s  %p    Parent: %p  hItem: %p",prpTI->Text, prlCCSTINew, prlCCSTI,prlCCSTINew->hItem);

	
	if(prpTI->Parent != NULL) 
		prlCCSTI->ChildItems.InsertAt(ilLc + 1, prlCCSTINew);
	else
		omRootItems.InsertAt(ilLc + 1, prlCCSTINew);
	omPtrMap.SetAt((void *&)prlCCSTINew->pData,(void *&) prlCCSTINew);
	//TRACE("\n Insert: %s   after: %s     %p MultiSel %d\n",prpTI->Text,olAfter,prlCCSTINew->hItem, prlCCSTINew->MultiSel); 

	if(bpSelect) CTreeCtrl::Select(prlCCSTINew->hItem ,TVGN_CARET);
/*
	int ilTmp = prlCCSTINew->Image;
	prlCCSTINew->Image = prlCCSTINew->SelImage;
	prlCCSTINew->SelImage = ilTmp;
	CTreeCtrl::SetItemImage(prlCCSTINew->hItem, prlCCSTINew->Image, prlCCSTINew->SelImage);
*/		
	
	return true;
}


bool CCSTree::ResetContent()
{
	CCSTREEITEM *prlCCSTI;
	void *pVoid;
	POSITION pos;
	bmDeleteMode = true;
	CTreeCtrl::DeleteAllItems();
	//is al litle bit faster
	for( pos = omPtrMap.GetStartPosition(); pos != NULL; )
	{
		omPtrMap.GetNextAssoc( pos, pVoid , (void *&)prlCCSTI );
		delete prlCCSTI;
	}
	omRootItems.RemoveAll();
	omSelItems.RemoveAll();
	omPtrMap.RemoveAll();
	bmDeleteMode = false;
	return true;;
/*	
	int ilLc;
	int ilLc2;
	CPtrArray olItems;	
	CPtrArray olItemsUp;	
	CPtrArray olAllItems;	
	for( ilLc = omRootItems.GetSize() -1; ilLc >= 0; ilLc--)
	{
		olAllItems.Add(&omRootItems[ilLc]);
		olItems.Add(&omRootItems[ilLc]);
	}

	while(olItems.GetSize() > 0)
	{
		for(ilLc = 0; ilLc < olItems.GetSize(); ilLc++)
		{	
			prlCCSTI = (CCSTREEITEM *)olItems[ilLc];
			for(ilLc2 = 0; ilLc2 < prlCCSTI->ChildItems.GetSize(); ilLc2++)
			{
				olAllItems.Add(&prlCCSTI->ChildItems[ilLc2]);
				olItemsUp.Add(&prlCCSTI->ChildItems[ilLc2]);
			}
		}
		olItems.RemoveAll();
		for(ilLc = 0; ilLc < olItemsUp.GetSize(); ilLc++)
			olItems.Add(olItemsUp[ilLc]);
		olItemsUp.RemoveAll();
	}
	TRACE("ResetContent: loesche %d Items\n",olAllItems.GetSize());
	TRACE("Items   %d \n",omPtrMap.GetCount());
	for(ilLc = olAllItems.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		delete (CCSTREEITEM *)olAllItems[ilLc];
	}
*/
}




bool CCSTree::DeleteItem(TREEITEM *prpTI)
{
	CPtrArray olItems;	
	CPtrArray olItemsUp;	
	CPtrArray olAllItems;	
	CCSTREEITEM *prlCCSTI = 0;
	CCSTREEITEM *prlCCSTIParent = 0;
	CCSPtrArray<HTREEITEM> olHItems;
	int ilLc;
	int ilLc2;

	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTIParent) == 0) 
		return false;

	bmDeleteMode = true;
		
	for(ilLc2 = 0; ilLc2 < prlCCSTIParent->ChildItems.GetSize(); ilLc2++)
	{
		olAllItems.Add(&prlCCSTIParent->ChildItems[ilLc2]);
		olItems.Add(&prlCCSTIParent->ChildItems[ilLc2]);
	}

	while(olItems.GetSize() > 0)
	{
		for(ilLc = 0; ilLc < olItems.GetSize(); ilLc++)
		{	
			prlCCSTI = (CCSTREEITEM *)olItems[ilLc];
			for(ilLc2 = 0; ilLc2 < prlCCSTI->ChildItems.GetSize(); ilLc2++)
			{
				olAllItems.Add(&prlCCSTI->ChildItems[ilLc2]);
				olItemsUp.Add(&prlCCSTI->ChildItems[ilLc2]);
			}
		}
		olItems.RemoveAll();
		for(ilLc = 0; ilLc < olItemsUp.GetSize(); ilLc++)
			olItems.Add(olItemsUp[ilLc]);
		olItemsUp.RemoveAll();
	}
	for(ilLc = olAllItems.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		prlCCSTI = (CCSTREEITEM *)olAllItems[ilLc];
		CTreeCtrl::DeleteItem(prlCCSTI->hItem);
	}

	for(ilLc = olAllItems.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		prlCCSTI = (CCSTREEITEM *)olAllItems[ilLc];
		omPtrMap.RemoveKey((void *&)prlCCSTI->pData);
		delete prlCCSTI;
	}


	if(prlCCSTIParent->Parent != NULL)
	{
		for(ilLc = 0; ilLc < prlCCSTIParent->Parent->ChildItems.GetSize() ; ilLc++)
		{
			if(&prlCCSTIParent->Parent->ChildItems[ilLc] == prlCCSTI)
			{
				omPtrMap.RemoveKey((void *&)prlCCSTIParent->pData);
				CTreeCtrl::DeleteItem(prlCCSTIParent->hItem);
				prlCCSTIParent->Parent->ChildItems.DeleteAt(ilLc);
				break;
			}
		}	
	}
	else
	{
		for(ilLc = 0; ilLc < omRootItems.GetSize() ; ilLc++)
		{
			if(&omRootItems[ilLc] == prlCCSTIParent)
			{
				omPtrMap.RemoveKey((void *&)prlCCSTIParent->pData);
				CTreeCtrl::DeleteItem(prlCCSTIParent->hItem);
				omRootItems.DeleteAt(ilLc);
				break;
			}
		}	
	}
	olHItems.DeleteAll();
	bmDeleteMode = false;
	CTreeCtrl::SelectItem(CTreeCtrl::GetSelectedItem());
	return true;
}



bool CCSTree::ChangeItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef,TREEITEM *prpTIAfter, bool bpSelect)
{
	if((TestItemPos(prpTI,prpTIAfter) == false) || (TestItemParent(prpTI) == false))
	{
		DeleteItem(prpTI);
		return InsertItem(prpTI, prpTIDef, prpTIAfter, bpSelect);
	}
	else
	{
		return ChangeItem(prpTI, prpTIDef, bpSelect);
	}
}

bool CCSTree::ChangeItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef, bool bpSelect)
{
	CCSTREEITEM		*prlCCSTI;
	TV_ITEM			tvItem;
	TREEITEMDEF		*prlTIDef = prpTIDef;
	CString			olStr;

	// Search the item
	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) == 0)
		return false;


	if(TestItemParent(prpTI) == false)
	{
		return false;
	}

	if(prlTIDef == NULL)
		prlTIDef = GetTreeItemDef(prpTI->ItemType);
	if(prlTIDef == NULL)
		return false;
	if(prpTI->Parent != prlCCSTI->Parent->pData)
		return false;


	CTreeCtrl::SelectItem(prlCCSTI->hItem);
	GetItem(&tvItem);

	
	olStr = prpTI->Text.Left(prlTIDef->TextLimit);
	tvItem.pszText = olStr.GetBuffer(prlTIDef->TextLimit);
	olStr.ReleaseBuffer();

	tvItem.cchTextMax = prlTIDef->TextLimit;

	prlCCSTI->Edit = prlTIDef->Edit;
	prlCCSTI->MultiSel = prlTIDef->MultiSel;

	//if(prlTIDef->Edit)
	//	ModifyStyle(NULL, TVS_EDITLABELS, 0);

	if(prlTIDef->Bold)
		tvItem.state |= TVIS_BOLD;  
	else
		tvItem.state &= TVIS_BOLD;  

	SetItem(&tvItem);

	if(bpSelect) CTreeCtrl::Select(prlCCSTI->hItem ,TVGN_CARET);
	return true;
}


bool CCSTree::InsertTree(CCSPtrArray<TREEITEM> prpTItems, TREEITEM *prpTIAfter)
{
	int ilLc;
	for(ilLc = 0; ilLc < prpTItems.GetSize(); ilLc++)
	{	
		if(ilLc == 0)
		{
			if(InsertTree(&prpTItems[ilLc], prpTIAfter) == false)
				return false;
		}
		else
		{
			if(InsertTree(&prpTItems[ilLc], &prpTItems[ilLc - 1]) == false)
				return false;
		}
	}
	//TRACE("--------------------------------------------------\n");
	//TRACE("\n %d\n",omPtrMap.GetCount()); 
	return true;
}


bool CCSTree::InsertTree(TREEITEM *prpTI, TREEITEM *prpTIAfter)
{
	CPtrArray olItems;	
	CPtrArray olItemsUp;	
	TREEITEM *prlTITmp;
	int ilLc;
	int ilLc2;
	//int blRoot = true;
	
	olItems.Add(prpTI);
	while(olItems.GetSize() > 0)
	{
		for(ilLc = 0; ilLc < olItems.GetSize(); ilLc++)
		{	
			prlTITmp = (TREEITEM *)olItems[ilLc];
			if(prlTITmp == prpTI)
			{
				if(InsertItem(prlTITmp, prpTIAfter) == false) return false;
			}
			else
			{
				if(ilLc == 0)
				{
					if(InsertItem(prlTITmp) == false) return false;
				}
				else
				{
					if(InsertItem(prlTITmp,(TREEITEM *)olItems[ilLc - 1]) == false) return false;
				}
			}
			for(ilLc2 = 0; ilLc2 < prlTITmp->ChildItems.GetSize(); ilLc2++)
				olItemsUp.Add(&prlTITmp->ChildItems[ilLc2]);
		}
		olItems.RemoveAll();
		for(ilLc = 0; ilLc < olItemsUp.GetSize(); ilLc++)
			olItems.Add(olItemsUp[ilLc]);
		olItemsUp.RemoveAll();
	}
	//TRACE("--------------------------------------------------\n");
	//TRACE("\n %d\n",omPtrMap.GetCount()); 
	return true;
}




int CCSTree::GetLevel(TREEITEM *prpTI)
{
	HTREEITEM hItem;
	CCSTREEITEM *prlCCSTI;
	int ilLevel = 0;
	if(prpTI != NULL)
	{
		if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) == 0)
			return -1;
	}
	else
	{
		if((hItem = CTreeCtrl::GetSelectedItem()) == NULL)
			return -1;
		prlCCSTI = (CCSTREEITEM *)CTreeCtrl::GetItemData(hItem);
		if(prlCCSTI == NULL)
			return -1;
	}
	while(prlCCSTI->Parent != NULL)
	{
		ilLevel++;
		prlCCSTI = prlCCSTI->Parent;
	}
	return ilLevel;
}


TREEITEM *CCSTree::GetFocusItem()
{
	HTREEITEM hItem;
	if((hItem = CTreeCtrl::GetSelectedItem()) == NULL)
		return NULL;
	return (TREEITEM *)((CCSTREEITEM *)CTreeCtrl::GetItemData(hItem))->pData;
}


void CCSTree::TraceTree()
{
	CPtrArray olItems;	
	CPtrArray olItemsUp;	
	CCSTREEITEM *prlCCSTI;
	int ilLc;
	int ilLc2;
	int ilCount = 0;	

	for(ilLc2 = 0; ilLc2 < omRootItems.GetSize(); ilLc2++)
		olItems.Add(&omRootItems[ilLc2]);

	while(olItems.GetSize() > 0)
	{
		for(ilLc = 0; ilLc < olItems.GetSize(); ilLc++)
		{	
			prlCCSTI = (CCSTREEITEM *)olItems[ilLc];
			///////////////TRACE//////////////////////////////////////////////////////////////////////////////
			
			TRACE("\n-CCSTree--->  %s   %p", GetItemText(prlCCSTI->hItem), prlCCSTI->hItem);
			ilCount++;
			
			///////////////TRACE//////////////////////////////////////////////////////////////////////////////
			for(ilLc2 = 0; ilLc2 < prlCCSTI->ChildItems.GetSize(); ilLc2++)
				olItemsUp.Add(&prlCCSTI->ChildItems[ilLc2]);
		}
		olItems.RemoveAll();
		for(ilLc = 0; ilLc < olItemsUp.GetSize(); ilLc++)
			olItems.Add(olItemsUp[ilLc]);
		olItemsUp.RemoveAll();
	}
	TRACE("\n Anzahl Items: %d \n\n",ilCount);
}



void CCSTree::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	HTREEITEM hItem;
	CCSTREENOTIFY	olNotify;

	omPoint = point;
	olNotify.SourceTree = this;	
	olNotify.Focus = true;	
	olNotify.Selected = false;	
	olNotify.Point = omPoint;

	if((hItem = CTreeCtrl::GetSelectedItem()) != NULL) 
	{
		olNotify.Item = (TREEITEM *)((CCSTREEITEM *)CTreeCtrl::GetItemData(hItem))->pData;
		if(olNotify.Item == NULL)
			olNotify.Data = NULL;
		else
			olNotify.Data = olNotify.Item->pVData; 

		pomParentWindow->SendMessage(WM_TREE_LBUTTONDBLCLK, 0, (LPARAM)&olNotify);
	}
	CTreeCtrl::OnLButtonDblClk(nFlags, point);
}


void CCSTree::OnRButtonDown(UINT nFlags, CPoint point) 
{
	HTREEITEM hItem;
	CCSTREENOTIFY	olNotify;

	omPoint = point;
	olNotify.SourceTree = this;	
	olNotify.Focus = false;	
	olNotify.Selected = false;	
	olNotify.Point = omPoint;

	hItem = CTreeCtrl::GetSelectedItem();
	olNotify.Item = (TREEITEM *)((CCSTREEITEM *)CTreeCtrl::GetItemData(hItem))->pData;
	if(olNotify.Item == NULL)
		olNotify.Data = NULL;
	else
		olNotify.Data = olNotify.Item->pVData; 

	pomParentWindow->SendMessage(WM_TREE_RBUTTONDOWN, 0, (LPARAM)&olNotify);
	CTreeCtrl::OnRButtonDown(nFlags, point);
}


void CCSTree::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	pNMHDR;

	*pResult = 0;
}


void CCSTree::OnDestroy() 
{
	ResetContent();
	omTreeItemDefs.DeleteAll();
	CTreeCtrl::OnDestroy();
}


void CCSTree::OnBeginlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	pNMHDR;
	HTREEITEM hItem;
	CCSTREEITEM *prlCCSTI;
	void *p;

	if( (hItem = CTreeCtrl::GetSelectedItem()) != NULL)
	{
		p = (void*)GetItemData(hItem);
		prlCCSTI = (CCSTREEITEM*)p;

		if(prlCCSTI->Edit == false)
		{

				ModifyStyle(TVS_EDITLABELS,NULL, 0);
				ModifyStyle(NULL, TVS_EDITLABELS, 0);
		}
	}
	*pResult = 0;
}











///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
////OLD METHODS////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
bool CCSTree::DeleteItem(TREEITEM *prpTI)
{

	CPtrArray olItems;	
	CPtrArray olItemsUp;	
	CPtrArray olAllItems;	
	TREEITEM *prlTI;
	CCSTREEITEM *prlCCSTI;
	int ilLc;
	int ilLc2;
	bmDeleteMode = true;
	for(ilLc2 = 0; ilLc2 < prpTI->ChildItems.GetSize(); ilLc2++)
	{
		olAllItems.Add(&prpTI->ChildItems[ilLc2]);
		olItems.Add(&prpTI->ChildItems[ilLc2]);
	}

	while(olItems.GetSize() > 0)
	{
		for(ilLc = 0; ilLc < olItems.GetSize(); ilLc++)
		{	
			prlTI = (TREEITEM *)olItems[ilLc];
			for(ilLc2 = 0; ilLc2 < prlTI->ChildItems.GetSize(); ilLc2++)
			{
				olAllItems.Add(&prlTI->ChildItems[ilLc2]);
				olItemsUp.Add(&prlTI->ChildItems[ilLc2]);
			}
		}
		olItems.RemoveAll();
		for(ilLc = 0; ilLc < olItemsUp.GetSize(); ilLc++)
			olItems.Add(olItemsUp[ilLc]);
		olItemsUp.RemoveAll();
	}
	TRACE("loesche %d Items\n",olAllItems.GetSize());
	TRACE("Items   %d \n",omPtrMap.GetCount());
	for(ilLc = olAllItems.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		prlTI = (TREEITEM *)olAllItems[ilLc];
		TRACE("loesche: %s   %p  %p\n",prlTI->Text, prlTI, prlCCSTI->hItem);
		
		if(omPtrMap.Lookup((void *&)prlTI, (void *&)prlCCSTI) != 0)
		{	
			omPtrMap.RemoveKey((void *&)prlTI);
			CTreeCtrl::DeleteItem(prlCCSTI->hItem);
			delete prlCCSTI;
		}
	}
	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) != 0)
	{
		if(prlCCSTI->Parent != NULL)
		{
			for(ilLc = 0; ilLc < prlCCSTI->Parent->ChildItems.GetSize() ; ilLc++)
			{
				if(&prlCCSTI->Parent->ChildItems[ilLc] == prlCCSTI)
				{
					omPtrMap.RemoveKey((void *&)prpTI);
					CTreeCtrl::DeleteItem(prlCCSTI->hItem);
					prlCCSTI->Parent->ChildItems.DeleteAt(ilLc);
					break;
				}
			}	
		}
		else
		{
			for(ilLc = 0; ilLc < omRootItems.GetSize() ; ilLc++)
			{
				if(&omRootItems[ilLc] == prlCCSTI)
				{
					omPtrMap.RemoveKey((void *&)prpTI);
					CTreeCtrl::DeleteItem(prlCCSTI->hItem);
					omRootItems.DeleteAt(ilLc);
					break;
				}
			}	
		}
	
	}
	bmDeleteMode = false;
	CTreeCtrl::SelectItem(CTreeCtrl::GetSelectedItem());
	return true;
}







void CCSTree::TraceTree()
{
	CCSTREEITEM	*prlCCSTI;
	CCSTREEITEM	*prlCCSTITmp;

	#define FORWARD 0
	#define BACK	1
	int i[MAXLEVEL];

	int ilAnz[MAXLEVEL];
	int ilLevel = 0;		// from the childitems
	int ilCount = 0;
	int ilUpDown = FORWARD;

	TRACE("\n\n--- Listing aller TreeItems ---------------------------------------------------------------- ");
	TRACE("\n-------------------------------------------------------------------------------------------- ");

	prlCCSTI = &omRoot; 

	ilAnz[0] = prlCCSTI->ChildItems.GetSize();

	while(ilAnz[0] > 0)
	{
		if(ilUpDown == FORWARD)
		{
			if(ilAnz[ilLevel] > 0)
			{
				prlCCSTI = &prlCCSTI->ChildItems[ilAnz[ilLevel] - 1];
				ilLevel++;
				ilAnz[ilLevel] = prlCCSTI->ChildItems.GetSize();
			}
			else
				ilUpDown = BACK;
		}
		if(ilUpDown == BACK)
		{
			prlCCSTITmp = prlCCSTI;
			for(int ilLc = 0; ilLc < MAXLEVEL; i[ilLc] = -1, ilLc++);
			while(prlCCSTITmp != &omRoot)
			{
				i[prlCCSTITmp->Level] = prlCCSTITmp->Index; 
				prlCCSTITmp = prlCCSTITmp->Parent; 
			}

			TRACE("\n (%d) (%d) (%d) (I %d) %s %p",i[0], i[1], i[2], prlCCSTI->Index, GetItemText(prlCCSTI->hItem), prlCCSTI->hItem);
			ilCount++;
			ilAnz[ilLevel - 1]--;
			ilUpDown = FORWARD;
			if(ilAnz[ilLevel - 1] > 0)
			{
				prlCCSTI = &(prlCCSTI->Parent)->ChildItems[ilAnz[ilLevel - 1] - 1];
				ilAnz[ilLevel] = prlCCSTI->ChildItems.GetSize();
			}
			else
			{
				prlCCSTI = prlCCSTI->Parent;
				ilLevel--;
			}
		}
	}
	TRACE("\n Anzahl Items: %d \n\n",ilCount);
}




*/


int CCSTree::GetCountSelItems()
{
	return omSelItems.GetCount();
}

TREEITEM *CCSTree::GetSelectedItem(int ipIndex)
{ 
	if((ipIndex >= omSelItems.GetCount()) || (ipIndex < 0))
		return NULL;

	POSITION pos;
	CCSTREEITEM *prlCCSTI;
	CCSTREEITEM *prlCCSTITmp;
	int ilLc = 0;
	for( pos = omSelItems.GetStartPosition(); pos != NULL; )
	{
		omSelItems.GetNextAssoc( pos, (void *&)prlCCSTITmp , (void *&)prlCCSTI );
		if(ilLc == ipIndex)
			return	(TREEITEM *)prlCCSTI->pData;
		ilLc++;
		//TRACE("\n   %s ",GetItemText(prlCCSTI->hItem));
	}
	return NULL;

}				


bool CCSTree::SelectItem(TREEITEM *prpTI, bool bpFocus)
{
	CCSTREEITEM *prlCCSTI;
	CCSTREENOTIFY	olNotify;
	int ilI;	
	int ilSI;	

	if(omPtrMap.Lookup((void *&)prpTI, (void *&)prlCCSTI) == 0)
		return false;

	olNotify.SourceTree = this;	
	olNotify.Focus = true;	
	olNotify.Selected = true;	
	olNotify.Point = CPoint(-1,-1);
	olNotify.Item = (TREEITEM *)prlCCSTI->pData;

	
	if(!bmMultiSelection)
	{
		CTreeCtrl::Select(prlCCSTI->hItem ,TVGN_CARET);
		pomParentWindow->SendMessage(WM_TREE_SELCHANGE, 0, (LPARAM)&olNotify);
	}
	else
	{
		if(bpFocus)
		{
			DeSelectAll();
			CTreeCtrl::Select(prlCCSTI->hItem ,TVGN_CARET);
			omSelItems.RemoveAll();
			omSelItems.SetAt((void*&)prlCCSTI, (void*&)prlCCSTI);
			pomParentWindow->SendMessage(WM_TREE_SELCHANGE, 0, (LPARAM)&olNotify);
			TRACE("\nITEMS: %d",omSelItems.GetCount());
		}
		else
		{
			if(prlCCSTI != NULL  && prlCCSTI->MultiSel)
			{
				if(CTreeCtrl::GetItemImage(prlCCSTI->hItem, ilI, ilSI) != 0)
				{
					CTreeCtrl::SetItemImage(prlCCSTI->hItem, ilSI, ilI);

					if(ilI == prlCCSTI->Image)
						omSelItems.SetAt((void*&)prlCCSTI, (void*&)prlCCSTI);
					else
						omSelItems.RemoveKey((void*&)prlCCSTI);

					olNotify.Focus = false;	
					pomParentWindow->SendMessage(WM_TREE_SELCHANGE, 0, (LPARAM)&olNotify);
					TRACE("\nITEMS: %d",omSelItems.GetCount());
				}
			}
		}
	}
	return true;
}


void CCSTree::DeSelectAll() 
{
	CCSTREEITEM *prlCCSTI;
	HTREEITEM hItem;
	void *pVoid;
	POSITION pos;
	for( pos = omSelItems.GetStartPosition(); pos != NULL; )
	{
		omSelItems.GetNextAssoc( pos, pVoid , (void *&)prlCCSTI );
		CTreeCtrl::SetItemImage(prlCCSTI->hItem, prlCCSTI->Image, prlCCSTI->SelImage);
	}
	omSelItems.RemoveAll();
	hItem = CTreeCtrl::GetSelectedItem();
	prlCCSTI = (CCSTREEITEM *)CTreeCtrl::GetItemData(hItem);
	if(prlCCSTI != NULL)
	{
		omSelItems.SetAt((void*&)prlCCSTI, (void*&)prlCCSTI);
		//CTreeCtrl::SetItemImage(prlCCSTI->hItem, prlCCSTI->SelImage, prlCCSTI->Image);
	}
}


void CCSTree::OnLButtonDown(UINT nFlags, CPoint point) 
{
	HTREEITEM hItem;
	CCSTREENOTIFY	olNotify;
	CCSTREEITEM *prlCCSTI;
	int ilI, ilSI;
	omPoint = point;

	if(!((bmMultiSelection) && (nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL)))
	{

		CTreeCtrl::OnLButtonDown(nFlags, point);
		UINT ilFlags = 0;
		hItem = HitTest(point, &ilFlags);
		if(!(ilFlags &= TVHT_ONITEM) || (hItem == NULL))
		{
			return;
		}		

	}
	else
	{
		UINT ilFlags = 0;
		hItem = HitTest(point, &ilFlags);
		if((ilFlags &= TVHT_ONITEM) && (hItem != NULL))
		{
			prlCCSTI = (CCSTREEITEM *)CTreeCtrl::GetItemData(hItem);
			if(prlCCSTI->MultiSel)
			{
				if((CTreeCtrl::GetItemImage(hItem, ilI, ilSI) != 0))
				{
					CTreeCtrl::SetItemImage(hItem, ilSI, ilI);

					if(ilI == prlCCSTI->Image)
						omSelItems.SetAt((void*&)prlCCSTI, (void*&)prlCCSTI);
					else
						omSelItems.RemoveKey((void*&)prlCCSTI);

					olNotify.SourceTree = this;	
					olNotify.Focus = false;	
					olNotify.Selected = true;	
					olNotify.Point = point;
					olNotify.Item = (TREEITEM *)prlCCSTI->pData;
					pomParentWindow->SendMessage(WM_TREE_SELCHANGE, 0, (LPARAM)&olNotify);
					TRACE("\nITEMS: %d",omSelItems.GetCount());
					return;
				}
			}
		}
	}
}







void CCSTree::OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	pNMHDR;
	*pResult = 0;
}


void CCSTree::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HTREEITEM hItem;
	CCSTREENOTIFY	olNotify;
	CCSTREEITEM *prlCCSTI;

	hItem = CTreeCtrl::GetSelectedItem();
	if(hItem == NULL)
		return;

	prlCCSTI = (CCSTREEITEM *)CTreeCtrl::GetItemData(hItem);
	if(prlCCSTI != NULL)
	{
		DeSelectAll();
		omSelItems.SetAt((void*&)prlCCSTI, (void*&)prlCCSTI);

		olNotify.SourceTree = this;	
		olNotify.Focus = true;	
		olNotify.Selected = true;	
		olNotify.Point = omPoint;
		olNotify.Item = (TREEITEM *)prlCCSTI->pData;
		pomParentWindow->SendMessage(WM_TREE_SELCHANGE, 0, (LPARAM)&olNotify);
		TRACE("\nITEMS: %d",omSelItems.GetCount());
	}
	
	*pResult = 0;

}

void CCSTree::OnItemexpanded(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	pNMHDR;
/*
	CCSTREEITEM *prlCCSTI;
	HTREEITEM hItem;
	void *pVoid;
	POSITION pos;
	int ilTmp;
	for( pos = omSelItems.GetStartPosition(); pos != NULL; )
	{
		omSelItems.GetNextAssoc( pos, pVoid , (void *&)prlCCSTI );
		//ilTmp = prlCCSTI->Image;
		//prlCCSTI->Image = prlCCSTI->SelImage;
		//prlCCSTI->SelImage = ilTmp; 

		CTreeCtrl::SetItemImage(prlCCSTI->hItem, prlCCSTI->SelImage, prlCCSTI->Image);
	}
	//hItem = CTreeCtrl::GetSelectedItem();

  */
	*pResult = 0;
}

void CCSTree::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	pNMHDR;
	
	*pResult = 0;
}

HBRUSH CCSTree::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	long llRc;

	hbr = CreateSolidBrush(omBKColor);
	llRc = pDC->SetBkColor(omBKColor);
	llRc = pDC->SetTextColor(omTextColor);
	return hbr;

}

void CCSTree::SetBKColor(COLORREF opColor)
{
	omBKColor = opColor;
	InvalidateRect(NULL,true);
}

void CCSTree::SetTextColor(COLORREF opColor)
{
	omTextColor = opColor;
	InvalidateRect(NULL,true);
}


void CCSTree::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	TRACE("\nCOLOR %d\n",nIDCtl);
	
	CTreeCtrl::OnDrawItem(nIDCtl, lpDrawItemStruct);
}


