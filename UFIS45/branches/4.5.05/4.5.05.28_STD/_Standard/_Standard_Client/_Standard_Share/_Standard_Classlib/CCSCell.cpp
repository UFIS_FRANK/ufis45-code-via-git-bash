// CCSCell.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <CCSCell.h>
#include <CCSDefines.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSCell

CCSCell::CCSCell() : CWnd()
{

	omBKColor = RGB(255,255,255); /*White*/
	omTextColor = RGB(0,0,0);	/*Black*/
	omSelTextColor = RGB(255,255,255);	
	omSelBKColor = RGB(0,0,255);	
//	omDisabledBKColor = RGB(192,192,192);	
//	omBrush.CreateSolidBrush(RGB(192,192,192));
	omDisabledBKColor = ::GetSysColor(COLOR_BTNFACE);	
	omBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
	bmSelected = false;
	bmDisabled = false;
	bmToggleSel = true;
	bmMouseMove = false;
	bmIsMultiSelected = false;
	bmMultiSelected = false;
	imLine = -1;
	imColumn = -1;
	bmHasFocusRect = false;
	imFocusRectOffset = 0;

    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(lolFont.lfFaceName, "Courier New");
    omDefaultFont.CreateFontIndirect(&lolFont);

	pomFont = &omDefaultFont;

}

CCSCell::~CCSCell()
{
	omDefaultFont.DeleteObject();
	omBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CCSCell, CWnd)
	//{{AFX_MSG_MAP(CCSCell)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
    ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCSCell 


void CCSCell::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	CCSCELLNOTIFY olNotify;	

	CWnd *prlParent;
	prlParent = GetParent();

	CPoint olPoint = point;
	ClientToScreen(&olPoint);

	olNotify.Point = olPoint;
	olNotify.Column = imColumn;
	olNotify.Line = imLine;
	olNotify.SourceControl = this;
	olNotify.Text = omText;
	olNotify.Name = omName;
	
	prlParent->SendMessage(WM_CELL_LBUTTONDBLCLK, nFlags, (LPARAM)&olNotify);

}

bool CCSCell::SetFocusRect(int ipOffset)
{
	if (ipOffset < 0)
		ipOffset = 0;

	CPaintDC dc(this);
	CRect olRect;
	GetClientRect(olRect);
	if ( (olRect.Height() - 2*ipOffset) < 2 )
		return false;

	bmHasFocusRect = true;
	imFocusRectOffset = ipOffset;

	InvalidateRect(NULL);
	UpdateWindow();

	return true;
}

bool CCSCell::HasFocusRect(int &ipOffset)
{
	if (bmHasFocusRect)
		ipOffset = imFocusRectOffset;
	else
		ipOffset = 0;

	return bmHasFocusRect;
}

	
void CCSCell::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	CRect olRect;
	GetClientRect(olRect);

	omBrush.DeleteObject();

	bool blSel = false;

	if(bmIsMultiSelected)
	{
		if(bmMultiSelected)
		{
			blSel = true;
		}
	}
	else
	{
		if(bmSelected)
		{
			blSel = true;
		}
	}

	CFont *polOldFont = dc.SelectObject(pomFont);

	CPen olPen;
	//CPen *pomOldPen;

	if(blSel)
	{


		// Background

		dc.SetBkMode( TRANSPARENT );
		omBrush.CreateSolidBrush(omSelBKColor);
		dc.FillRect(olRect,&omBrush);

		

		//olPen.CreatePen( PS_SOLID, 2, omSelTextColor );	

		//pomOldPen =  dc.SelectObject(&olPen);

		dc.SetTextColor(omSelTextColor);

		dc.DrawText((LPCSTR) omText, omText.GetLength(), olRect, DT_VCENTER | DT_CENTER | DT_SINGLELINE );



	}
	else
	{

		// Background
		dc.SetBkMode( TRANSPARENT );

		if (IsDisabled())
			omBrush.CreateSolidBrush(omDisabledBKColor);
		else
			omBrush.CreateSolidBrush(omBKColor);

		dc.FillRect(olRect,&omBrush);
		
		//Text
	
		//olPen.CreatePen( PS_SOLID, 2, omTextColor );	

		//pomOldPen =  dc.SelectObject(&olPen);

		dc.SetTextColor(omTextColor);

		dc.DrawText((LPCSTR) omText, omText.GetLength(), olRect, DT_VCENTER | DT_CENTER | DT_SINGLELINE );

	}

	if (bmHasFocusRect)
	{
		CPoint olTL = olRect.TopLeft();
		CPoint olBR = olRect.BottomRight();
		CPoint olOF(imFocusRectOffset, imFocusRectOffset);
		olTL += olOF;
		olBR -= olOF;
		CRect olFocus (olTL,olBR);
		dc.DrawFocusRect(olFocus);
/*
		CPoint olOF(imFocusRectOffset, imFocusRectOffset);
		CRect olFocus (olRect.TopLeft()+olOF, olRect.BottomRight()-olOF);
		dc.DrawFocusRect(olFocus);
*/
	}

	//dc.SelectObject(pomOldPen);
	dc.SelectObject(polOldFont);

	olPen.DeleteObject();

}


COLORREF CCSCell::SetBKColor(COLORREF opColor)
{ 
	COLORREF olRet = omBKColor;
	omBKColor = opColor;
	InvalidateRect(NULL);
	UpdateWindow();
	return olRet;
}



COLORREF CCSCell::SetSelBKColor(COLORREF opColor)
{ 
	COLORREF olRet = omSelBKColor;
	omSelBKColor = opColor;
	InvalidateRect(NULL);
	UpdateWindow();
	return olRet;
}


COLORREF CCSCell::SetDisabledBKColor(COLORREF opColor)
{ 
	COLORREF olRet = omDisabledBKColor;
	omDisabledBKColor = opColor;
	InvalidateRect(NULL);
	UpdateWindow();
	return olRet;
}


void CCSCell::SetText(CString opText)
{
	omText = opText;
	InvalidateRect(NULL);
	UpdateWindow();
}



void CCSCell::ReleaseMultiSel()
{
	if(bmIsMultiSelected)
	{
		bmIsMultiSelected = false;
		bmSelected = bmMultiSelected;
	}
}


void CCSCell::SetMultiSel(bool bpSet)
{
	if (IsDisabled())
		return;

	if(	bpSet == bmIsMultiSelected && bpSet == bmMultiSelected)
		return;
	
	if(bpSet)
	{
		bmIsMultiSelected = true;
		bmMultiSelected = bpSet;
	}
	else
	{
		bmIsMultiSelected = false;
		bmMultiSelected = false;
	}
	InvalidateRect(NULL);
	UpdateWindow();
}


void CCSCell::OnMouseMove(UINT nFlags, CPoint point) 
{
	CCSCELLNOTIFY olNotify;	


	if(nFlags == MK_LBUTTON)
	{
		CWnd *prlParent;
		prlParent = GetParent();

		CPoint olPoint = point;
		ClientToScreen(&olPoint);

		olNotify.Point = olPoint;
		olNotify.Column = imColumn;
		olNotify.Line = imLine;
		olNotify.SourceControl = this;
		olNotify.Text = omText;
		olNotify.Name = omName;

		prlParent->SendMessage(WM_CELL_MOUSEMOVE, nFlags, (LPARAM)&olNotify);

		ReleaseCapture();

	}
	CWnd::OnMouseMove(nFlags, point);
}




void CCSCell::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (IsDisabled())
		return;

	CCSCELLNOTIFY olNotify;	
	CWnd *prlParent;
	prlParent = GetParent();

	CPoint olPoint = point;
	ClientToScreen(&olPoint);

	olNotify.Point = olPoint;
	olNotify.Column = imColumn;
	olNotify.Line = imLine;
	olNotify.SourceControl = this;
	olNotify.Text = omText;
	olNotify.Name = omName;

	prlParent->SendMessage(WM_CELL_LBUTTONDOWN, nFlags, (LPARAM)&olNotify);

	bmSelected = !bmSelected;
	InvalidateRect(NULL);
	UpdateWindow();

	CWnd::OnLButtonDown(nFlags, point);
}


void CCSCell::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CCSCELLNOTIFY olNotify;	
	CWnd *prlParent;
	prlParent = GetParent();
	CPoint olPoint = point;
	ClientToScreen(&olPoint);
	olNotify.Point = olPoint;
	olNotify.Column = imColumn;
	olNotify.Line = imLine;
	olNotify.SourceControl = this;
	olNotify.Text = omText;
	olNotify.Name = omName;

	prlParent->SendMessage(WM_CELL_LBUTTONUP, nFlags, (LPARAM)&olNotify); 
	CWnd::OnLButtonUp(nFlags, point);
}




void CCSCell::Select(bool bpSet) 
{
	if (IsDisabled())
		return;

	bmIsMultiSelected = false;
	bmMultiSelected = bpSet;
	bmSelected = bpSet;
	InvalidateRect(NULL);
	UpdateWindow();
}


CCSCell::CCSCell(const CCSCell& s)
{

}


const CCSCell& CCSCell::operator= ( const CCSCell& s)
{
	if(this == &s)
	{
		return *this;
	}
	return *this;
}

void CCSCell::Disable(bool bpSet)
{
	if (bmDisabled == bpSet)
		return;

	bmDisabled = bpSet;

	if (bmDisabled)
	{
		bmSelected = false;
		bmToggleSel = false;
		bmMouseMove = false;
		bmIsMultiSelected = false;
		bmMultiSelected = false;
	}
	else
	{
		bmSelected = false;
		bmToggleSel = true;
		bmMouseMove = false;
		bmIsMultiSelected = false;
		bmMultiSelected = false;
	}
	InvalidateRect(NULL);
	UpdateWindow();
}

bool CCSCell::IsDisabled()
{
	return bmDisabled;
}

void CCSCell::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (IsDisabled())
		return;

	CCSCELLNOTIFY olNotify;	
	CWnd *prlParent;
	prlParent = GetParent();

	CPoint olPoint = point;
	ClientToScreen(&olPoint);

	olNotify.Point = olPoint;
	olNotify.Column = imColumn;
	olNotify.Line = imLine;
	olNotify.SourceControl = this;
	olNotify.Text = omText;
	olNotify.Name = omName;

	prlParent->SendMessage(WM_CELL_RBUTTONDOWN, nFlags, (LPARAM)&olNotify);

	InvalidateRect(NULL);
	UpdateWindow();

	CWnd::OnRButtonDown(nFlags, point);
}
