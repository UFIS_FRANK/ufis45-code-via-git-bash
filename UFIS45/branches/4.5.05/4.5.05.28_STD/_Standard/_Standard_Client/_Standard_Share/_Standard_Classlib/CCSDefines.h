// CCSDefines.h: interface for the CCSDefines class.
//
// RKR: 19.04.2001		#define WM_TABLE_RETURN_PRESSED	(WM_USER + 321) added
//						PRF 1358 PRODUCT4.4

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSDEFINES_H__B86450F0_3A21_11D1_82ED_0080AD1DC701__INCLUDED_)
#define AFX_CCSDEFINES_H__B86450F0_3A21_11D1_82ED_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000



#include <afxwin.h>

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif


#define NOTUSED -1
#define BC_UNI_CHANGE		0x1111
#define BC_NFLIGHT_CHANGE	0x1112
#define BC_NFLIGHT_DELETE	0x1113

enum //enumRecordState 
{ 
    DATA_UNCHANGED, DATA_NEW, DATA_CHANGED, DATA_DELETED, DATA_NEW_INTERNAL, DATA_DEL_INTERNAL, DATA_UNCHANGED_INTERNAL
};

/////////////////////////////////////////////////////////////////////////////
// the range WM_USER (0x0400) to  WM_USER + 0x0400 are reserved for ccsglobl
// use for your application messages WM_PRJ + n


#define WM_PRJ  WM_USER + 0x0400


// Messages sent by UFIS32.DLL to notify application for pending broadcasts
#define WM_BCADD						(WM_USER + 16)
// Messages sent from TimeScale to surrounding window (eg. GateDiagram)
#define WM_TIMEUPDATE					(WM_USER + 101)
#define WM_DRAGENTER					(WM_USER + 102) 
#define WM_DRAGOVER						(WM_USER + 103)
#define WM_DROP							(WM_USER + 104)
#define WM_DRAGLEAVE					(WM_USER + 105)

// Messages for handling keystrokes in various diagrams
#define WM_USERKEYDOWN					(WM_USER + 151)
#define WM_USERKEYUP					(WM_USER + 152)

// Messages sent from CTable to surrounding windows (eg. PrePlanDiagram)
#define WM_TABLE_LBUTTONDBLCLK          (WM_USER + 310)     // wParam is the selected item
#define WM_TABLE_LBUTTONDOWN			(WM_USER + 311)     // wParam is the selected item
#define WM_TABLE_RBUTTONDOWN			(WM_USER + 312)     // wParam is the selected item
#define WM_TABLE_RBUTTONDBLCLK          (WM_USER + 313)     // wParam is the selected item
#define WM_TABLE_DRAGBEGIN              (WM_USER + 314)     // wParam is the selected item
#define WM_CHART_RBUTTONDOWN			(WM_USER + 315)		//Owner Drawn Button Clicked
#define WM_TABLE_CTRL_BUTTONDOWN		(WM_USER + 316)
#define WM_TABLE_SELCHANGE		        (WM_USER + 317)
#define WM_TABLE_IPEDIT_KILLFOCUS		(WM_USER + 318)     
#define WM_TABLE_IPEDIT					(WM_USER + 319)
#define WM_TABLE_MENU_SELECT			(WM_USER + 320)
#define WM_TABLE_RETURN_PRESSED			(WM_USER + 321)

#define WM_BUTTON_RBUTTONDOWN			(WM_USER + 350) 
#define WM_BUTTON_CLICKED				(WM_USER + 351) 
#define WM_TBL_INLINE_UPDATE			(WM_USER + 352)	//Table cell changed parameters LineNo, CellNo

// Messages sent from CTree to surrounding windows 
#define WM_TREE_LBUTTONDOWN				(WM_USER + 410)     
#define WM_TREE_LBUTTONDBLCLK           (WM_USER + 411)     
#define WM_TREE_RBUTTONDOWN				(WM_USER + 412)     
#define WM_TREE_RBUTTONDBLCLK			(WM_USER + 413)     // wParam is the selected item
#define WM_TREE_DRAGBEGIN				(WM_USER + 414)     // wParam is the selected item
#define WM_TREE_NEWITEM					(WM_USER + 417)     // wParam is the selected item
#define WM_TREE_SELCHANGE				(WM_USER + 418)     // wParam is the selected item
#define WM_TABLE_SORT					(WM_USER + 419)

#define WM_TABLE_CTRL_DOWN              (WM_USER + 420) 
#define WM_TABLE_CTRL_GROUP             (WM_USER + 421) 

// Messages sent from CCSEdit to surrounding windows 
#define WM_EDIT_KILLFOCUS				(WM_USER + 510)     
#define WM_EDIT_MOVE_IP_LEFT			(WM_USER + 511)     
#define WM_EDIT_MOVE_IP_RIGHT			(WM_USER + 512)     
#define WM_EDIT_MOVE_IP_UP				(WM_USER + 513)     
#define WM_EDIT_MOVE_IP_DOWN			(WM_USER + 514)     
#define WM_EDIT_IP_END					(WM_USER + 515)     
#define WM_EDIT_TAB_BUTTONDOWN			(WM_USER + 516)
#define WM_EDIT_SHIFTTAB_BUTTONDOWN		(WM_USER + 517)
#define WM_EDIT_CHANGED					(WM_USER + 518)
#define WM_EDIT_IP_RETURN				(WM_USER + 519)     
#define WM_EDIT_LBUTTONDOWN			    (WM_USER + 520)     // wParam is the selected item
#define WM_EDIT_DRAGBEGIN               (WM_USER + 521)     // wParam is the selected item
#define WM_EDIT_LBUTTONDBLCLK          (WM_USER + 522)     // wParam is the selected item
#define WM_EDIT_MENU_SELECT				(WM_USER + 523)
#define WM_EDIT_RBUTTONDOWN				(WM_USER + 524)
#define WM_EDIT_RETURN					(WM_USER + 525)

#define WM_CELL_LBUTTONDOWN				(WM_USER + 540)     
#define WM_CELL_LBUTTONUP				(WM_USER + 541)     
#define WM_CELL_MOUSEMOVE				(WM_USER + 542)     
#define WM_CELL_LBUTTONDBLCLK           (WM_USER + 543)     
#define WM_CELL_RBUTTONDOWN				(WM_USER + 544)     


// Messages sent from CCSButtonCtrl to surrounding windows 
#define WM_BUTTON_CLICK					(WM_USER + 610)     



enum
{
	CCSTBL_HEADER_COLUMN = 100,
	CCSTBL_ROW_COLUMN,
};





struct MENUITEM
{
	UINT	ID;
	CString Text;
	UINT	Flags;

	MENUITEM(void)
	{
		ID    = 0;
		Flags = 0;
	}

};



#endif // !defined(AFX_CCSDEFINES_H__B86450F0_3A21_11D1_82ED_0080AD1DC701__INCLUDED_)
