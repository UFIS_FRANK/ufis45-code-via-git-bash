// CedaSEAData.h: interface for the CedaSEAData class.

#ifndef _CEDASEADATA_H_
#define _CEDASEADATA_H_
 
#include <afxtempl.h>
#include <CCSCedaData.h>
#include <CcsDefines.h>


struct SEADATA
{
	//values from seatab
	long	 Urno;
	char 	 Seas[10];
	CTime 	 Vpfr;
	CTime 	 Vpto;

	//indicates if it is a winter or summer seaason
	bool IsWinter;

	SEADATA(void) 
	{
		memset(this,'\0',sizeof(*this));
		Urno = 0;
		Vpfr = TIMENULL;
		Vpto = TIMENULL;
		IsWinter = true;
	}

};


/////////////////////////////////////////////////////////////////////////////
// Class declaration


class CedaSEADataUtcLoc: public CCSCedaData
{
// Attributes
private:
    CCSPtrArray<SEADATA> omDataUTC;
	CMapStringToPtr omSeasMapUTC;	//"SEAS"->SEADATA
    CCSPtrArray<SEADATA> omDataLocal;
	CMapStringToPtr omSeasMapLocal;	//"SEAS"->SEADATA
	char pcmFlist[1000];

// Operations
public:
    CedaSEADataUtcLoc();
 	~CedaSEADataUtcLoc();

	// default TAB is used
	void SetTableExtension(CString opExtension);

	//pspWhere "" reads the whole SEATAB.
	bool Read(char *pspWhere, char *pcpFieldList = NULL);

	//gets a struct of utc or local for opSea("SEAS").
	//if bpLocal=true the seadata for local will be used
	//(note that Apt4ConvertSeaToLocal() must be called one time before.
	SEADATA* FindSea(CString opSea, bool bpLocal = false);

	//if opTime is in a winterseason the return is true.
	//if bpLocal=false the seadata for utc will be used.
	//if bpLocal=true the seadata for local will be used
	//(note that Apt4ConvertSeaToLocal() must be called one time before.
	bool IsWinterSeason(CTime &opTime, bool bpLocal = false);

	//converts the seadata from uct to local with 
	//timedifferenz summer(opTdis) and winter(opTdiw)
	bool Apt4ConvertSeaToLocal(CTimeSpan &opTdis, CTimeSpan &opTdiw);

private:
	void ClearAll(void);

};


#endif
