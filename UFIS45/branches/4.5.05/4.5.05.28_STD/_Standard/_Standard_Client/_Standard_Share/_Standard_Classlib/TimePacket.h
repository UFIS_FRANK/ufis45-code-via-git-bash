// TimePacket.h - packet for updating time band in StaffDiagram (via DDX)
//
// Description:
// There is a feature in the PEPPER project which allow the user to see the
// period of time of the bar which he or she clicks on. The time band will be
// displayed only in the StaffDiagram but the bar which the user click may be
// a job bar or a demand bar from anywhere, including various detail windows.
// The time band will be displayed to the users as two yellow vertical lines.
// Every charts in StaffDiagram will show this time band.
//
// This time band will be removed if the user click outside every bars in
// the StaffDiagram.
//
//
// Written by:
// Damkerng Thammathakerngkit	Sep 25, 1996
//
// RST 21.08.1997
// this struct can be used everywhere


#ifndef _TIMEPACKET_H_
#define _TIMEPACKET_H_

struct TIMEPACKET
{
	CTime StartTime;
	CTime EndTime;
};

#endif // _TIMEPACKET_H_