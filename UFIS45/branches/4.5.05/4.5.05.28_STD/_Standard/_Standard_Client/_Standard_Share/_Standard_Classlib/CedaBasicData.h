// CedaBasicData.h: interface for the CedaBasicData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDABASICDATA_H__28E63D61_971E_11D1_8371_0080AD1DC701__INCLUDED_)
#define AFX_CEDABASICDATA_H__28E63D61_971E_11D1_8371_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// Lib  includes
#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CcsLog.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBasic.h>
#include <CCSBcHandle.h>
#include <FieldSet.h>
#include <UfisOdbc.h>

//
#include <CedaObject.h>


class CedaBasicData  : public CCSCedaData
{
public:
	CedaBasicData(CString opTableExt, CCSDdx *popDdx, CCSBcHandle *popBcHandle, CCSCedaCom *popCommHandler, CCSBasic *popBasic);

	virtual ~CedaBasicData();

// mu� vor dem ersten SetObject() aufgerufen werden
	void SetTableExtension(CString opTableExt) {omTableExt = opTableExt ;};

// Selecting the coonection condition
// ODBC or CEDA ==> "" also for CEDA
	void SetAccessMethod(CString opAccessMethod)
	{
		omAccessMethod = opAccessMethod;
	}
	CString omODBCConnect;
	void SetODBCConnect(CString opConnect)
	{
		omODBCConnect = opConnect;
		pomOdbc->SetConnect(omODBCConnect);
	}
//Setzt den Tabellenname

//MWO 23.12.98
	bool SetObject(CString opObject, CCSPtrArray<FieldSet> opFieldSet,bool bpUseOwnBc = true,CALLBACKFUNC pfpFunction = NULL);


	bool RemoveObject( CString opObject);

//Feldbeschreibung holen
	bool GetFieldSet(CString opObject, CCSPtrArray<FieldSet> &ropFieldSet);
//Holt aus der Feldbeschreibung f�r das Object/Feld die Lookupvalues f�r das Feld, entweder aus dem Referenzobject
	// oder aus den von au�en parametrierten Values z.B.: Kunde, Barzahler
	bool GetPossibleLookupValues(CString opObject, CString opField, CStringArray &ropValues);

//Initialisiert die Referenzen, aus dem Feld REFE der Systab
	bool InitReferences();
//END MWO

//MWO 21.05.1999
	bool SetLogicalFields(CString opObject, CCSPtrArray<FieldSet> opFields);
//END MWO

//MWO 27.05.1999
	bool CloneFieldSet(CString opObject, CCSPtrArray<FieldSet> &ropFields);

//END MWO
	bool SetObject(CString opObject, CString opFieldList = "",bool bpUseOwnBc = true,CALLBACKFUNC pfpFunction = NULL);

	bool SetObject(CString opAlias, CString opObject, CString opFieldList,bool bpUseOwnBc = true,CALLBACKFUNC pfpFunction = NULL);


//Freitext f�r Tabelle setzen
	void SetObjectDesc(CString opObject, CString opDesc);	
//Holt Freitext f�r Tabelle setzen
	CString GetObjectDesc(CString opObject); 
//Baut f�r das entspr, Feld eine Map auf
	bool AddKeyMap(CString opObject, CString opField);
//Liefert einen Pointer auf keymap des betr. Objects
	CMapStringToPtr * GetMapPointer(CString opObject, CString opField);
//Baut einen Header f�r jedes Feld auf
	void  SetTableHeader(CString opObject, CString opField, CString opHeader);
//Holt den Header f�r das betr. Feld
	CString  GetTableHeader(CString opObject, CString opFields, CString opTrenner);
// Buffersize == 0 ==> BC sofort einarbeiten
//            <  0 ==> wegschmei�en
//			  >  0 ==> BCs buffern und bei der angegebenen Zahl siehe n�chste Funtkion
	void SetBcMode(CString opObject, int ipBufferSize = 0);
// Wenn vorige Funktion �berl�uft, >> DDX wird geschickt
	void SetSentBcBufferOverflow(CString opObject, int ipDdxType);
//Setzt den DDX_Typ f�r Tabelle und Command, Trifft das ein wird DDX gesendet
	void SetDdxType(CString opObject, CString opCmd, int ipDdxType);

//Lesen mit Where, falls pomBuffer != NULL, dann werden die Daten in pomBuffer gesetzt
//									  sonst ==> einarbeiten in die lokale Datenhaltung
	int Read(CString opObject, CString opWhere = "", CCSPtrArray<RecordSet> *pomBuffer = NULL, bool bpDeleteBuffer = true);

//MWO 13.04.200
//	Select count(<x>) from <Tab> [<Where ...>]
// return is the result
	int SelectCount(CString opObject, CString opWhere = "");
	// returns the last ErrorMessage
	CString GetLastError();

//END MWO 13.04.200

//Liest mit where die angegebenen Felder, in den buffer, unabh�ngig von SetObject
	int ReadSpecial(CString opObject, CString opFields, CString opWhere, CCSPtrArray<RecordSet> &opBuffer);
//L�schen der Daten in DB und lok. Datenhaltung,
	bool DeleteByWhere(CString opObject, CString opWhere);
//Update der angegebenen Felder, unagbh�ngig der in SetObject angegebenen Felder
	bool UpdateSpecial(CString opObject, CString opFields, CString opWhere, CString opData);

	//Data access 

//
//  Wichtig: bei allen GetField oder Record wird bei mehrfachauftreten von RefValue 
//	der erste Treffer zur�ckgeliefert. ( Unsicheres Ergebnis )
//			 
//
//
//

// F�r den Index die angegebenen Felder mit dem angegebenen Trenner
// Flags:  
//	FILLBLANK
//	TOTRIMLEFT
//  TOTRIMRIGHT
	CString  GetFields(CString opObject, int ipIndex, CString opFields, CString opTrenner = ",", int ipFlags = 0);
/*------------------------------------------------
 CString opObject    = TAbelle
 CString opRefField  = Referenzfeldname 
 CString opRefValue  = Wert des Ref.-Feldes
 CString opFields    = Gew�nschte Felder
 z.B.: "APT", "URNO", "4711", "APC3,APC4,APFN", ","

  return = "HAJ,EDDV,Hannover"
*/
	CString  GetValueList(CString opObject, CString opRefField, CString opRefValue, CString opFields, CString opTrenner = ",");

// Holt den Record nummer ipIndex mit dem Feld opField
//if the field is not valid ==> function returns CString("NULL")

	CString  GetField(CString opObject, int ipIndex, CString opField);

// Liefert im Stringarray alle Records f�r angegebene Felder ==> 
// Ein Datensatz wird in einen String des Arrays mit dem angegebenen Trenner geschrieben
	void GetAllFields(CString opObject, CString opFields, CString opTrenner, int ipFlag, CStringArray &olData);

// L�dt eine Listbox
	int ShowAllFields(CString opObject, CString opFields, CListBox *polList, CString opSelString = "", bool bpTrimRight = false, bool bpAddBlankLine  = false);

//--------------------------------------------------------------------------
// z.B.: "APT", "URNO", "4711", "APC3", "APC4"
//	opReturn1 = "HAJ"
//  opReturn2 = "EDDV"
	bool  GetFields(CString opObject, CString opRefField, CString opRefValue, 
		            CString opField1, CString opField2, CString &opReturn1, CString &opReturn2);

//--------------------------------------------------------------------------
// z.B.: "APT", "APC3", "HAJ", "APC4" 
//	ReturnWert = "EDDV"
	CString  GetField(CString opObject, CString opRefField, CString opRefValue, CString opField);


//--------------------------------------------------------------------------
// z.B.: "SEA", "VPFR", "VPTO", "19980907000000" , "SEAS"
//	opReturn = "SS98"
	bool GetFieldBetween(CString opObject, CString opRefFieldFrom, CString opRefFieldTo, CString opRefValue, 
		                 CString opField,  CString &opReturn );


//--------------------------------------------------------------------------
// z.B.: "APT", "APC3", "APC4", "HAJ" oder "EDDV"
//	opReturn1 = "HAJ"
//  opReturn2 = "EDDV"
	bool GetField(CString opObject,   CString opRefField1, CString opRefField2, CString opRefValue, 
		          CString &opReturn1, CString &opReturn2 , CString opValDate = "" );

//--------------------------------------------------------------------------
// z.B.: "APT", "APC3", "APC4", "HAJ" oder "EDDV", "URNO"
//	Return = "4711"
	CString GetField2(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue, CString opField);

//--------------------------------------------------------------------------
// falls ein Wert alleine nicht eindeutig ist
// z.B.: "APT", "APC3", "APC4", "HAJ" , "EDDV", "URNO"
//	Return = "4711"
	CString GetFieldExt(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CString opField);


//--------------------------------------------------------------------------
// z.B.: "APT", "APC3", "HAJ" , "APC4"
//	Return = "EDDV"
	bool GetField(CString opObject, CString opRefField, CString opRefValue, CString opField, CString &opReturn);


//--------------------------------------------------------------------------
// liefert den kompl. Record;  z.B.: "APT", "APC3", "HAJ"  
	bool GetRecord(CString opObject, CString opRefField, CString opRefValue, RecordSet &rpRecord);

//--------------------------------------------------------------------------
// liefert den kompl. Record an dem angegebenen Index
	bool GetRecord(CString opObject, int ilIndex,  RecordSet &rpRecord);



	void GetRecords(CString opObject, CString opRefField, CString opRefValue, CCSPtrArray<RecordSet> *prpRecords);

	int GetAllRecords(CString opObject, CCSPtrArray <RecordSet> &ropRecords);

	int GetRecordsExt(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CCSPtrArray<RecordSet> *prpRecords);


//--------------------------------------------------------------------------
// liefert die Anzahl der Felder des Objekts - WICHTIG: Bezieht sich auf SetObject()
// z.B.: 	SetObject("APT", "URNO,APC3,APC4")  -->   GetFieldCount("APT") liefert 3 zur�ck
	int GetFieldCount(CString opObject);

// liefert Anzahl der Records
	int GetDataCount(CString opObject);

// liefert den Index des Feldes im Record
//if the field is not valid ==> function returns -1
	int GetFieldIndex(CString opObject, CString opField);


	// Data manipulation


// opObject   --> Tabelle 
// opSortList --> Feldliste z.B. "APC3+,APC4-" 
// bpDo       --> true => sofort soriteren  ( siehe auch Fkt. Sort() ) 
	void SetSort(CString opObject, CString opSortList, bool bpDo, bool bpNumeric = false);
	
// Wendet mit SetSort() angegebene Sortierung an
	void Sort(CString opObject);
	

// Verfahren wie bei GetField
// USEC,USEU,URNO werden ggf. intern gesetzt
// bpDbSave -->  true   => Sofort speichern 
//          -->  false  => �nderungen  sammeln ( siehe Fkt. Save() )

	bool SetRecord(CString opObject, CString opRefField, CString opRefValue, CStringArray &opRecord, bool bpDbSave = false);

	bool SetField(CString opObject, CString opRefField, CString opRefValue, CString opField, CString opValue, bool bpDbSave = false);

	bool InsertRecord(CString opObject, RecordSet &rpRecord, bool bpDbSave = false);

	bool DeleteRecord(CString opObject, CString opRefField, CString opRefValue, bool bpDbSave = false);

// Sichert die gesammelten �nderungen -- aber einzeln !!!
	bool Save(CString opObject);

// Released die gesammelten �nderungen und sendet den angegebenen Broadcast, wenn gew�nscht	
	bool Release(const CString& opObject,int ipPackages = 500,char *ropRelCmd = "LATE,NOACTION,NOLOG",char *pcpSbc = NULL);


	void SetSystabErrorMsg(CString opSystabErrorMsg, CString opSystabErrorCaptionMsg);


// sch�ttet Broadcastbuffer aus ( siehe  Fkt. SetBcMode() )	
	void ReleaseBcBuffer(CString opObject);

// liefert den logischen Datantyp ( nicht DB ):
// DATE
// LONG
// TRIM
// URNO
	CString GetFieldType(CString opObject, CString opField);

// liefert die max. Feldl�nge
// z.B.: "APT", "URNO"   -->  10
// liefert die max. Feldl�nge des angegebenen Feldes von allen Records
	int GetMaxFieldLength(CString opObject, CString opField);


//INTERNAL///INTERNAL///INTERNAL////INTERNAL////INTERNAL///INTERNAL////INTERNAL///INTERNAL///INTERNAL/////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
// internal Broadcast handler
	
	void  ProcessUniDataBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	void  ProcessUniDataBcInternal(void *vpDataPointer, CString &ropInstanceName);

	// debug

	void TraceData(CString opObject);

public:

	int ReadInternal(CString opObject, CString opWhere, CCSPtrArray<RecordSet> *pomBuffer, bool bpSetSel, bool bpDeleteBuffer = true);

	bool GetManyUrnos(int ipAnz, CUIntArray &opUrnos);
	long GetNextUrno(void);

	bool CheckSMWhere(CString opWhere);


	CUIntArray omNewUrnos;
	UfisOdbc	*pomOdbc;
	CString		omAccessMethod; //ODBC ==> "ODBC" or CEDA ==> "CEDA" or ""
	CCSBasic *pomBasic;	
	CCSCedaCom *pomCommHandler;
	CMapStringToPtr omObjectMap;
	CCSPtrArray<CedaObject> CedaObjects;
	CString omTableExt;
	CCSDdx *pomDdx;
	CCSBcHandle *pomBcHandle;

private:
    bool CedaAction2(char *pcpAction,
        char *pcpTableName, 
		char *pcpFieldList,
        char *pcpSelection, 
		char *pcpSort,
        char *pcpData, 
		char *pcpDest = "BUF1",
		bool bpIsWriteAction = false, 
		long lpID = 0, 
		CCSPtrArray<RecordSet> *pomData = NULL, 
		int ipFieldCount = 0, 
		bool bpDelDataArray = true
		);
};

#endif // !defined(AFX_CEDABASICDATA_H__28E63D61_971E_11D1_8371_0080AD1DC701__INCLUDED_)
