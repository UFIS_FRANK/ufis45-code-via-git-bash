// FieldSet.h: interface for the FieldSet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIELDSET_H__28E63D64_971E_11D1_8371_0080AD1DC701__INCLUDED_)
#define AFX_FIELDSET_H__28E63D64_971E_11D1_8371_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CCSPtrArray.h>
class CedaObject;

class ValueSet //for future use
{
public:
	//For hardcoding without reference table
	ValueSet()
	{
		pomRefObject = NULL;
	}
	CStringArray GUIValues;
	CStringArray DBValues;

	CedaObject *pomRefObject;
	CString omFieldName;

	const ValueSet& operator= ( const ValueSet& s)
	{
		if(this == &s)
		{
			return *this;
		}
		for(int i = 0; i < s.GUIValues.GetSize(); i++)
		{
			GUIValues.Add(s.GUIValues[i]);
		}
		for( i = 0; i < s.DBValues.GetSize(); i++)
		{
			DBValues.Add(s.DBValues[i]);
		}

		pomRefObject = s.pomRefObject;
		omFieldName  = s.omFieldName;
		return *this;

	}

};

class FieldSet  
{
public:
	FieldSet();
	virtual ~FieldSet();

	CedaObject *pomRefObject;  //for future use
	CString Field;
	CString Type;
	CString GUIType;
	ValueSet omPossibleValues;	//for future use
	CString DBType;
	int Length;
};

#endif // !defined(AFX_FIELDSET_H__28E63D64_971E_11D1_8371_0080AD1DC701__INCLUDED_)
