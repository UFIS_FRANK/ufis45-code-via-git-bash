// CedaAptLocalUtc.h: interface for the CedaAptLocalUtc class.
//
/*	Use of this class:
	First call of one these functions AptUtcToLocal(),AptLocalToUtc(),
	InitCedaAptLocalUtc() inits this class with all data.
	Only one instance will be ceated.
	You must call FreeCedaAptLocalUtc() by leaving your application,
	otherwise memory leaks will be dedected.

	If you call InitCedaAptLocalUtc(hopo4) one time the hopo4 is used as default
	by calling AptUtcToLocal(opTime,"") or AptLocalToUtc(opTime,"")
	or APC4 is not found.

	This class use:	class CedaAPTDataUtcLoc, class CedaSEADataUtcLoc.
	!!!ONLY APC4 IS USED!!!
*/
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDAAPTLOCALUTC_H__D7E249D3_9E17_11D6_804E_0001022205EE__INCLUDED_)
#define AFX_CEDAAPTLOCALUTC_H__D7E249D3_9E17_11D6_804E_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CedaAPTDataUtcLoc;
class CedaSEADataUtcLoc;


class CedaAptLocalUtc: public CObject  
{
private:
	CedaAptLocalUtc ();
	CedaAptLocalUtc (const CedaAptLocalUtc &src);
	virtual ~CedaAptLocalUtc ();

	bool Apt4UtcToLocal (CTime &opTime, const CString &opAPC4);
	bool Apt4LocalToUtc (CTime &opTime, const CString &opAPC4);
	bool InitAptLocalUtc ();	
	bool InitHopo4 (const CString &opHopo4);	


private:
	CedaAPTDataUtcLoc* pomAptData;
	CedaSEADataUtcLoc* pomSeaData;
	CString omHopo4;
	CString omLastApt4LocalToUtc;

public:
	static bool AptUtcToLocal (CTime &opTime, const CString &opAPC4);
	static bool AptLocalToUtc (CTime &opTime, const CString &opAPC4);
	static bool InitCedaAptLocalUtc (const CString &opHopo4);
	static void FreeCedaAptLocalUtc ();

};

#endif // !defined(AFX_CEDAAPTLOCALUTC_H__D7E249D3_9E17_11D6_804E_0001022205EE__INCLUDED_)
