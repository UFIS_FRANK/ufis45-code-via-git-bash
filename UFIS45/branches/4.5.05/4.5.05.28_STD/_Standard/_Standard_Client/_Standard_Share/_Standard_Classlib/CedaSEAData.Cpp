// CedaSEAData.cpp: implementation of the CedaSEADataUtcLoc class.

#include <stdafx.h>
#include <CedaSEAData.h>
#include <ccsddx.h>



CedaSEADataUtcLoc::CedaSEADataUtcLoc()
{
    // Create an array of CEDARECINFO for SEADATA
    BEGIN_CEDARECINFO(SEADATA, SEADATARecInfo)
		CCS_FIELD_LONG		(Urno,"URNO","",1)
		CCS_FIELD_CHAR_TRIM (Seas,"SEAS","",1)
		CCS_FIELD_DATE		(Vpfr,"VPFR","",1)
		CCS_FIELD_DATE		(Vpto,"VPTO","",1)
	END_CEDARECINFO

	// Copy the record structure
    for (int i = 0; i < sizeof(SEADATARecInfo)/sizeof(SEADATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SEADATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName, "SEATAB");
	strcpy(pcmFlist,"URNO,SEAS,VPFR,VPTO");
	pcmFieldList = pcmFlist;
}

CedaSEADataUtcLoc::~CedaSEADataUtcLoc(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

void CedaSEADataUtcLoc::SetTableExtension(CString opExtension)
{
	strcpy(pcmTableName, "SEA");
	strcat(pcmTableName, opExtension);
}

void CedaSEADataUtcLoc::ClearAll(void)
{
	omSeasMapUTC.RemoveAll();
	omDataUTC.DeleteAll();
	omSeasMapLocal.RemoveAll();
	omDataLocal.DeleteAll();
}

bool CedaSEADataUtcLoc::Read(char *pspWhere, char *pcpFieldList)
{
	bool ilRc = true;
	bool blRet = true;
	char pclFieldList[2000];

	ClearAll();
	
	if(pcpFieldList != NULL)
		strcpy(pclFieldList, pcmFieldList/*pcpFieldList*/);
	else
		strcpy(pclFieldList, pcmFieldList);

	if (CedaAction("RT", pcmTableName/*"SEA"*/, pclFieldList, pspWhere, "", pcgDataBuf)  == false)
		blRet = false;

	if(blRet)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SEADATA *prlSeaUTC = new SEADATA;
			if ((ilRc = GetBufferRecord(ilLc, prlSeaUTC, CString(pclFieldList))) == true)
			{
				if (prlSeaUTC->Vpfr != TIMENULL && prlSeaUTC->Vpto != TIMENULL)
				{
					if (prlSeaUTC->Vpfr.GetYear() == prlSeaUTC->Vpto.GetYear())
						prlSeaUTC->IsWinter = false;
					else
						prlSeaUTC->IsWinter = true;

					omDataUTC.Add(prlSeaUTC);
					omSeasMapUTC.SetAt(CString(prlSeaUTC->Seas), prlSeaUTC);
				}
				else
					delete prlSeaUTC;
			}
			else
			{
				delete prlSeaUTC;
			}
		}

		if(omDataUTC.GetSize() == 0)
			blRet = false;
		else
			blRet = true;
	}

//	SEADATA *prlSeaW02 = FindSea("W02");
	
	return blRet;
}

bool CedaSEADataUtcLoc::Apt4ConvertSeaToLocal(CTimeSpan &opTdis, CTimeSpan &opTdiw)
{
	omDataLocal.DeleteAll();
	omSeasMapLocal.RemoveAll();

	for ( int i = 0; i < omDataUTC.GetSize(); i++)
	{
		SEADATA *prlSea = &omDataUTC[i];
		if (!prlSea)
			continue;

		SEADATA *prlSeaLoc = new SEADATA;
		strcpy (prlSeaLoc->Seas, prlSea->Seas);
		prlSeaLoc->IsWinter = prlSea->IsWinter;
		prlSeaLoc->Urno = prlSea->Urno;
		if (prlSea->IsWinter)
		{
			prlSeaLoc->Vpfr = prlSea->Vpfr + opTdiw;
			prlSeaLoc->Vpto = prlSea->Vpto + opTdiw;
		}
		else
		{
			prlSeaLoc->Vpfr = prlSea->Vpfr + opTdis;
			prlSeaLoc->Vpto = prlSea->Vpto + opTdis;
		}
		omDataLocal.Add(prlSeaLoc);
		omSeasMapLocal.SetAt(CString(prlSeaLoc->Seas), prlSeaLoc);
	}

	if (omDataLocal.GetSize() >0 )
		return true;
	else
		return false;
}


SEADATA *CedaSEADataUtcLoc::FindSea(CString opSea, bool bpLocal)
{
	SEADATA *prlSea = NULL;
	if (bpLocal)
	{
		if(omSeasMapLocal.Lookup(opSea,(void *&)prlSea) == TRUE)
			return prlSea;
	}
	else
	{
		if(omSeasMapUTC.Lookup(opSea,(void *&)prlSea) == TRUE)
			return prlSea;
	}

	return NULL;
}

bool CedaSEADataUtcLoc::IsWinterSeason(CTime &opTime, bool bpLocal)
{
	if (bpLocal)
	{
		for ( int i = 0; i < omDataLocal.GetSize(); i++)
		{
			if (opTime >= omDataLocal[i].Vpfr && opTime <= omDataLocal[i].Vpto)
			{
				return omDataLocal[i].IsWinter;
			}
		}
	}
	else
	{
		for ( int i = 0; i < omDataUTC.GetSize(); i++)
		{
			if (opTime >= omDataUTC[i].Vpfr && opTime <= omDataUTC[i].Vpto)
			{
				return omDataUTC[i].IsWinter;
			}
		}
	}

	return true;
}





