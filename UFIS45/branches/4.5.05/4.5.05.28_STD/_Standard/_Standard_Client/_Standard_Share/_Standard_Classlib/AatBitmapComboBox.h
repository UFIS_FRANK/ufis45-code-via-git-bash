#if !defined(AFX_BITMAPCOMBOBOX_H__7B7C3CE6_BF13_11D6_8216_00010215BFDE__INCLUDED_)
#define AFX_BITMAPCOMBOBOX_H__7B7C3CE6_BF13_11D6_8216_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AatBitmapComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AatBitmapComboBox window

class AatBitmapComboBox : public CComboBox
{
// Construction
public:
	AatBitmapComboBox();

// Attributes
public:
	int AddBitmap(const CString& ropFileName,const CString& ropString="");
	int	AddBitmap(const CBitmap& ropBitmap,const CString& ropString="");

	int InsertBitmap(int ipIndex,const CString& ropFileName,const CString& ropString="");
	int	InsertBitmap(int ipIndex,const CBitmap& ropBitmap,const CString& ropString="");

	int DeleteBitmap(int ipIndex);

	CBitmap	GetSelectedBitmap();	

// Operations
public:
	void ResetContent();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AatBitmapComboBox)
	protected:	
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

// Implementation
public:
	virtual ~AatBitmapComboBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(AatBitmapComboBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:	// helpers
	BOOL	LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette );
	void	Initialize();

private:	// data
	CPtrArray	omBitmaps;
	bool		blDrawStrings;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPCOMBOBOX_H__7B7C3CE6_BF13_11D6_8216_00010215BFDE__INCLUDED_)
