/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/14/98 11:38:00 AM
  Comments: NColumn.h: interface for the CNColumn class.
 ************************************/

#if !defined(AFX_NCOLUMN_H__B3362D57_9309_11D2_8729_0040055C08D9__INCLUDED_)
#define AFX_NCOLUMN_H__B3362D57_9309_11D2_8729_0040055C08D9__INCLUDED_

#include "NEdit.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afx.h>

class CNColumn : public CObject  
{
public:
	virtual void SetName(LPCTSTR lpszColumnName);
	virtual CString GetName();
	virtual CEdit* GetEdit();
	virtual void SetParent(CNWindow* pParent);
	CNColumn(CNWindow* pParent, int nCol, LPCTSTR lpszColumnName);
	virtual ~CNColumn();

protected:
	CString m_sName;
	virtual UINT GetEditStyle(int nCol);
	CNEdit m_edit;
	CNWindow* m_pParent;
private:
};

#endif // !defined(AFX_NCOLUMN_H__B3362D57_9309_11D2_8729_0040055C08D9__INCLUDED_)
