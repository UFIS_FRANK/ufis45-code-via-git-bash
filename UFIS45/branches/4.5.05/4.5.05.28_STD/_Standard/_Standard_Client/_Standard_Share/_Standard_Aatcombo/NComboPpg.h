#if !defined(AFX_NCOMBOPPG_H__DE201798_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NCOMBOPPG_H__DE201798_9000_11D2_8726_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// NComboPpg.h : Declaration of the CNComboPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CNComboPropPage : See NComboPpg.cpp.cpp for implementation.

class CNComboPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CNComboPropPage)
	DECLARE_OLECREATE_EX(CNComboPropPage)

// Constructor
public:
	CNComboPropPage();

// Dialog Data
	//{{AFX_DATA(CNComboPropPage)
	enum { IDD = IDD_PROPPAGE_NCOMBO };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CNComboPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NCOMBOPPG_H__DE201798_9000_11D2_8726_0040055C08D9__INCLUDED)
