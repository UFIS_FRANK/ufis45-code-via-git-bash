// SmartInvalidate.h: interface for the CSmartInvalidate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMARTINVALIDATE_H__7B0D5858_9494_11D2_872B_0040055C08D9__INCLUDED_)
#define AFX_SMARTINVALIDATE_H__7B0D5858_9494_11D2_872B_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "gdi.h"

class CSmartInvalidate
{
protected:
	virtual void Reset();
	struct _WTStruct
	{
	public:
		_WTStruct(CWnd* pWnd)
		{
			ASSERT (pWnd && ::IsWindow(pWnd->m_hWnd));
			m_pBitmap = NULL;
			m_pDC = NULL;
			m_pWnd = pWnd;
			m_bVisible = m_pWnd->IsWindowVisible();
			if (m_bVisible)
				m_pWnd->ModifyStyle(WS_VISIBLE, 0, SWP_NOREDRAW);
		}

		virtual ~_WTStruct()
		{
		}

		virtual void OnUpdate()
		{
			if (m_bVisible)
				m_pWnd->ModifyStyle(0, WS_VISIBLE, SWP_NOREDRAW);
		}

		virtual void Prepare()
		{
			if(m_bVisible)
			{
				m_pWnd->GetWindowRect(m_rRect);
				m_pDC = m_pWnd->GetWindowDC();
				m_dcDraw.CreateCompatibleDC(m_pDC);
				m_pBitmap = CSmartInvalidate::Preview(m_pWnd);
				m_stack.Push(&m_dcDraw, m_pBitmap);
			}
		}

		virtual void Restore()
		{
			if (m_bVisible)
			{
				m_stack.Pop();
				m_pBitmap->DeleteObject();
				delete m_pBitmap;
				m_pWnd->ReleaseDC(m_pDC);
				m_pBitmap = NULL;
				m_pDC = NULL;
			}
		}

		virtual void Draw()
		{
			if (m_bVisible)
			{
				m_pDC->BitBlt(0,0,m_rRect.Width(),m_rRect.Height(),&m_dcDraw, 0,0, SRCCOPY);
			}
		}

	protected:
		BOOL m_bVisible;
		CWnd* m_pWnd;
		CBitmap* m_pBitmap;
		CDC* m_pDC;
		CRect m_rRect;
		CDC m_dcDraw;
		CGdiStack m_stack;
	};

	CArray<_WTStruct*, _WTStruct*> m_arWindows;
public:
	virtual void Update();
	virtual void Add(CWnd* pWindow);
	static CBitmap* Preview(CWnd* pWnd, COLORREF colorBkGnd = RGB(255,255,255));
	CSmartInvalidate();
	virtual ~CSmartInvalidate();
};

#endif // !defined(AFX_SMARTINVALIDATE_H__7B0D5858_9494_11D2_872B_0040055C08D9__INCLUDED_)
