// NComboPpg.cpp : Implementation of the CNComboPropPage property page class.

#include "stdafx.h"
#include "NCombo.h"
#include "NComboPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CNComboPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CNComboPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CNComboPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CNComboPropPage, "NCOMBO.NComboPropPage.1",
	0xde201789, 0x9000, 0x11d2, 0x87, 0x26, 0, 0x40, 0x5, 0x5c, 0x8, 0xd9)


/////////////////////////////////////////////////////////////////////////////
// CNComboPropPage::CNComboPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CNComboPropPage

BOOL CNComboPropPage::CNComboPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_NCOMBO_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CNComboPropPage::CNComboPropPage - Constructor

CNComboPropPage::CNComboPropPage() :
	COlePropertyPage(IDD, IDS_NCOMBO_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CNComboPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CNComboPropPage::DoDataExchange - Moves data between page and properties

void CNComboPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CNComboPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CNComboPropPage message handlers
