#if !defined(AFX_PRINTOPTION_H__16D84D61_21C9_4D3C_B0E6_FACBF87F658D__INCLUDED_)
#define AFX_PRINTOPTION_H__16D84D61_21C9_4D3C_B0E6_FACBF87F658D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintOption.h : header file
//
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////
// CPrintOption dialog

class CPrintOption : public CDialog
{
// Construction
public:
	CPrintOption(CWnd* pParent = NULL);   // standard constructor
	virtual BOOL OnInitDialog();
	inline int GetSelection(){return m_Sel;}

// Dialog Data
	//{{AFX_DATA(CPrintOption)
	enum { IDD = IDD_PRINT_OPTION };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

private:
	int	m_Sel;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintOption)
	protected:		
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:	

	// Generated message map functions
	//{{AFX_MSG(CPrintOption)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTOPTION_H__16D84D61_21C9_4D3C_B0E6_FACBF87F658D__INCLUDED_)
