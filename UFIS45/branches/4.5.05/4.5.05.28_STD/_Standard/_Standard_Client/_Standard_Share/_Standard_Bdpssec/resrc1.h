//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BDPS_SEC.rc
//
#define IDS_OK                          115
#define IDS_CANCEL                      116
#define IDS_PRD_ALL                     117
#define IDS_PRD_APPL                    118
#define IDS_PRD_SUBD                    119
#define IDS_PRINTRIGHTSCAPTION          120
#define IDS_INITLOADDLG                 121
#define IDS_CANONLYVIEWNONSUPERUSERS    122
#define BC_INSERT_SEC                   123
#define BC_INSERT_CAL                   124
#define BC_INSERT_GRP                   125
#define BC_INSERT_PRV                   126
#define BC_INSERT_FKT                   127
#define BC_UPDATE_SEC                   128
#define BC_UPDATE_CAL                   129
#define BC_UPDATE_GRP                   130
#define BC_UPDATE_PRV                   131
#define BC_UPDATE_FKT                   132
#define BC_DELETE_SEC                   133
#define BC_DELETE_CAL                   134
#define BC_DELETE_GRP                   135
#define BC_DELETE_PRV                   136
#define BC_DELETE_FKT                   137
#define INSERT_SEC                      138
#define INSERT_CAL                      139
#define INSERT_PRV                      141
#define INSERT_FKT                      142
#define IDD_WAIT_DIALOG                 143
#define UPDATE_SEC                      143
#define UPDATE_CAL                      144
#define UPDATE_GRP                      145
#define IDD_PRINT_OPTION                145
#define UPDATE_PRV                      146
#define UPDATE_FKT                      147
#define DELETE_SEC                      148
#define DELETE_CAL                      149
#define DELETE_GRP                      150
#define DELETE_PRV                      151
#define IDS_NOBROADCASTS                151
#define DELETE_FKT                      152
#define INSERT_GRP                      153
#define BC_SBC_REMAPP                   154
#define BC_SBC_NEWAPP                   155
#define BC_SBC_SETPRV                   156
#define BC_SBC_SETFKT                   157
#define IDR_FILECOPY                    158
#define BC_SBC_ADDAPP                   158
#define BC_SBC_DELAPP                   159
#define BC_SBC_UPDAPP                   160
#define BC_SBC_SMI                      161
#define BC_SBC_NEWREL                   162
#define BC_SBC_UPDREL                   163
#define BC_SBC_DELREL                   164
#define BC_SBC_DELSEC                   165
#define BROADCAST_CHECK                 166
#define IDS_OBJDLG_BC1                  183
#define IDS_OBJDLG_BC2                  184
#define IDS_OBJDLG_BC3                  185
#define IDS_OBJDLG_BC4                  186
#define IDS_OBJDLG_BC5                  187
#define IDS_OBJDLG_BC6                  188
#define IDS_OBJDLG_BC7                  189
#define IDS_OBJDLG_BC8                  190
#define IDS_OBJDLG_BC9                  191
#define IDS_OBJDLG_BC10                 193
#define IDS_OBJDLG_BC11                 194
#define IDS_OBJDLG_BC12                 195
#define IDS_REFRESHSCREEN1              196
#define IDS_REFRESHSCREEN2              197
#define IDS_BUTTONLIST_NOBC             234
#define IDS_ALLOCATEDDLG_BC1            343
#define IDS_ALLOCATEDDLG_BC2            344
#define IDS_ALLOCATEDDLG_BC3            345
#define IDS_ALLOCATEDDLG_BC4            346
#define IDS_ALLOCATEDDLG_BC5            347
#define IDS_WAITDLG_LOADING             385
#define IDS_ALLOCDLG_CAPTION            386
#define IDS_ALLOCDLG_USER               387
#define IDS_ALLOCDLG_ALLOCONLY          388
#define IDS_ALLOCDLG_VAFR               389
#define IDS_ALLOCDLG_VATO               390
#define IDS_ALLOCDLG_FREQ               391
#define IDS_ALLOCDLG_CANCEL             393
#define IDS_BL_MODULE                   394
#define IDS_BL_PROFILE                  395
#define IDS_BL_USER                     396
#define IDS_BL_GROUP                    397
#define IDS_BL_WKS                      398
#define IDS_BL_WKSGROUP                 399
#define IDS_BL_INSERT                   400
#define IDS_BL_UPDATE                   401
#define IDS_BL_DELETE                   402
#define IDS_BL_RIGHTS                   403
#define IDS_BL_ALLOCATE                 404
#define IDS_BL_PASSWORD                 405
#define IDS_BL_PRINT                    406
#define IDS_BL_EXIT                     407
#define IDS_EUD_USID_HEAD               408
#define IDS_EUD_NAME_HEAD               409
#define IDS_EUD_INITIALRIGHTSGROUP      410
#define IDS_EUD_NONE                    411
#define IDS_EUD_STANDARD                412
#define IDS_EUD_DISABLEALL              413
#define IDS_EUD_ENABLEALL               414
#define IDS_EUD_CAN_USE_BDSPSEC         415
#define IDS_EUD_REMA_HEAD               416
#define IDS_EUD_VAFR_HEAD               417
#define IDS_EUD_VAFR_HEAD2              418
#define IDS_EUD_FREQ_HEAD               419
#define IDS_LOGIN_USIDCAPTION           422
#define IDS_LOGIN_PASSCAPTION           423
#define IDS_OD_FREQ_HEAD                426
#define IDS_HELPCONTENTS                427
#define IDS_GENERAL                     428
#define IDS_ABOUT                       429
#define IDS_FUNCTIONS                   430
#define IDS_HELP                        431
#define IDS_EUD_COPY                    432
#define IDS_OLDPROFILE                  433
#define IDC_COPY                        1028
#define IDC_UFIS_VERSION                1056
#define IDC_BDPSSEC_VERSION             1057
#define IDC_COPYRIGHT                   1058
#define IDC_COPYRIGHT2                  1059
#define IDC_STATIC_VERSION              1060
#define IDC_COPYPROFILE                 1062
#define IDC_CLASSLIBVERSION             1063
#define IDC_BC_STATUS                   1064
#define IDC_PLACEHOLDER1                1065
#define IDC_PLACEHOLDER2                1066
#define IDC_PLACEHOLDER3                1067
#define IDC_CHECK_EXCEL                 1068
#define IDC_RADIO_PRINT                 1069
#define IDC_RADIO_EXCEL                 1070
#define IDC_ANIMATE1                    1262

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1073
#define _APS_NEXT_SYMED_VALUE           167
#endif
#endif
