// PrintRightsDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdps_sec.h>
#include <PrintRightsDlg.h>
#include <ResRc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString CONFIG_PATH = "C:\\UFIS\\SYSTEM\\CEDA.INI";
const CString CEDA = "CEDA";
const CString EXCEL = "EXCEL";
const CString DEFAULT = "DEFAULT";
const CString EXCELSEPARATOR = "EXCELSEPARATOR";
const char    SEPARATOR = ',';

/////////////////////////////////////////////////////////////////////////////
// CPrintRightsDlg dialog


CPrintRightsDlg::CPrintRightsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintRightsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintRightsDlg)
	m_Sel = 1;
	m_export = 0;
	//}}AFX_DATA_INIT
}


void CPrintRightsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintRightsDlg)
	DDX_Radio(pDX, IDC_ALL, m_Sel);
	DDX_Check(pDX, IDC_CHECK_EXCEL, m_export);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintRightsDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintRightsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintRightsDlg message handlers

BOOL CPrintRightsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_PRINTRIGHTSCAPTION));
	GetDlgItem(IDC_ALL)->SetWindowText(GetString(IDS_PRD_ALL));
	GetDlgItem(IDC_ALL2)->SetWindowText(GetString(IDS_PRD_APPL));
	GetDlgItem(IDC_ALL4)->SetWindowText(GetString(IDS_PRD_SUBD));
	if(IsExcelConfigured() == FALSE)
		ResizeDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CPrintRightsDlg::IsExcelConfigured()
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclSeparator[64];

	CString olTempPath = CCSLog::GetTmpPath();

	if (getenv(CEDA) == NULL)
		strcpy(pclConfigPath,CONFIG_PATH ); 
	else
		strcpy(pclConfigPath, getenv(CEDA));

	GetPrivateProfileString(pcgAppName, EXCEL, DEFAULT,pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	GetPrivateProfileString(pcgAppName, EXCELSEPARATOR, CString(SEPARATOR),pclSeparator, sizeof pclSeparator, pclConfigPath);
	if (strcmp(pclExcelPath, "DEFAULT") == 0)
	{
		return FALSE;
	}
	m_ExcelPath = pclExcelPath;
	m_ExcelSeparator = pclSeparator;

	return TRUE;
}

void CPrintRightsDlg::ResizeDialog()
{
	CRect olWindowRect;
	CRect olPrintRect;
	int olOffset = 0;

	((CWnd*)GetDlgItem(IDC_ALL4))->GetWindowRect(olPrintRect);
	ScreenToClient(olPrintRect);

	((CWnd*)GetDlgItem(IDC_CHECK_EXCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	((CWnd*)GetDlgItem(IDC_CHECK_EXCEL))->ShowWindow(SW_HIDE);
	

	olOffset = olWindowRect.bottom - olPrintRect.bottom -10;

	CRect olDeflateRect(0,-(olOffset),0,olOffset);
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= olOffset;
	this->MoveWindow(olWindowRect);
	
	((CWnd*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);	

	((CWnd*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);		
}