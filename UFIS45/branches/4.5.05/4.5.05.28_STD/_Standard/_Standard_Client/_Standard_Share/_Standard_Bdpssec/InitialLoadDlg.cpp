/////////////////////////////////////////////////////////////////////////////
// InitialLoadDlg dialog
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <CCSGlobl.h>
#include <InitialLoadDlg.h>
#include <ResRc1.h>


InitialLoadDlg::InitialLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InitialLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InitialLoadDlg)
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	ShowWindow(SW_SHOWNORMAL);

	m_Progress.SetRange(0,130);

}

InitialLoadDlg::~InitialLoadDlg(void)
{
	pogInitialLoad = NULL;
}

void InitialLoadDlg::SetProgress(int ipProgress)
{
	m_Progress.OffsetPos(ipProgress);
}

void InitialLoadDlg::SetMessage(CString opMessage)
{
	m_MsgList.AddString(opMessage);
	if ((m_MsgList.GetCount()-12) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-12);
	pogInitialLoad->UpdateWindow();
}

BOOL InitialLoadDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

void InitialLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InitialLoadDlg)
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InitialLoadDlg, CDialog)
	//{{AFX_MSG_MAP(InitialLoadDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InitialLoadDlg message handlers

BOOL InitialLoadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_INITLOADDLG));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
