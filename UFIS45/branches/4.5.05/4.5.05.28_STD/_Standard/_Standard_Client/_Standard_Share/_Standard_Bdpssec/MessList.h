#ifndef __MESSLIST_H__
#define __MESSLIST_H__

#define MESSLIST_KEYWORD "#MESSLIST"

#include <CCSPtrArray.h>

struct MESSLIST {

	char	*key;
	char	*mess;
};


class MessList
{
private:

    CCSPtrArray <MESSLIST> omMessList;
	char	mDescNotFound[250];
	BOOL	bmValidLine;

	void	ProcessLine(const char *pcpLine);
	void	CheckForError(const char *pcpItem);
	char	*ProcessItem(char *pcpLine, char **pppItem);

public:

	char	pcmLastError[250];
	
	MessList();
	~MessList();
	BOOL	Load(const char *pcpFilename);
	void	RemoveAll();
	char	*Get(const char *pcpKey);
	void	Add(const char *pcpKey,const char *pcpMess);

};



extern MessList ogMessList;



#endif // __MESSLIST_H__