#if !defined(AFX_WAITDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_)
#define AFX_WAITDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaitDlg.h : header file
//
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// WaitDlg dialog

class WaitDlg : public CDialog
{
// Construction
public:
	WaitDlg(CWnd* pParent = NULL, CString csText = "");   // standard constructor

// Dialog Data
	//{{AFX_DATA(WaitDlg)
	enum { IDD = IDD_WAIT_DIALOG };
	CAnimateCtrl	m_oAnimate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WaitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString		m_csText;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(WaitDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAITDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_)
