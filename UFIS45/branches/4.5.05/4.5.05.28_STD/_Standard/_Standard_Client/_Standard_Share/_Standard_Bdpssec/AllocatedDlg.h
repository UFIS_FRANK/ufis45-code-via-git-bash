// AllocatedDlg.h : header file
//

#ifndef __CALLOCATEDDLG__
#define __CALLOCATEDDLG__


/////////////////////////////////////////////////////////////////////////////
// CAllocatedDlg dialog
#include <GrpTableViewer.h>
#include <ButtonListDlg.h>
#include <CCSTable.h>
#include <CalTableViewer.h>
#include <ObjectListBox.h>

//#define WM_CLOSE_ALLOCDLG WM_USER + 120

typedef struct
{
	bool singleSel;			// true if the listBox is single-selection else multi-selection
	SEC_REC_TYPE type;		// type of list displayed eg. profiles,groups,user etc
	SEC_REC_TYPE allocType;	// type of allocation in GRPTAB: either 'P'=profile or 'G'=group
} SELDATA;


class CAllocatedDlg : public CDialog
{
// Construction
public:

	CAllocatedDlg(CWnd* pParent = NULL);   // standard constructor
	~CAllocatedDlg(void);
	BOOL Create(void);

// Dialog Data
	//{{AFX_DATA(CAllocatedDlg)
	enum { IDD = IDD_ALLOCATED };
	CButton	m_FreqAll;
	CButton	m_CalGroup;
	CButton	m_Freq7;
	CButton	m_Freq6;
	CButton	m_Freq5;
	CButton	m_Freq4;
	CButton	m_Freq3;
	CButton	m_Freq2;
	CButton	m_Freq1;
	CComboBox	m_ListSelectionCtrl;
	CStatic	m_Title2;
	CStatic	m_Title1;
	CButton	m_UpdateButton;
	CButton		m_OkButton;
	CButton		m_CancelButton;
	CButton		m_ShowAllocatedOnly;
	CObjectListBox *m_AllocatedProfilesList;
	//CListBox	m_AllocatedProfilesList;
	CString	m_Name;
	CCSEdit	m_VAFRdateCtrl;
	CCSEdit	m_VAFRtimeCtrl;
	CCSEdit	m_VATOdateCtrl;
	CCSEdit	m_VATOtimeCtrl;
	//}}AFX_DATA

	virtual BOOL OnInitDialog(const char *pcpUrno);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllocatedDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAllocatedDlg)
	virtual void OnOK();
	afx_msg void OnSelchangeAllocatedProfilesList();
	afx_msg void OnShowAllocatedOnly();
	virtual void OnCancel();
	afx_msg void OnSelchangeListSelection();
	afx_msg void OnFreqAll();
	afx_msg void OnFreq1();
	afx_msg void OnFreq2();
	afx_msg void OnFreq3();
	afx_msg void OnFreq4();
	afx_msg void OnFreq5();
	afx_msg void OnFreq6();
	afx_msg void OnFreq7();
	afx_msg LONG OnTableLButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableRButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableSelChange(UINT ipParam, LONG lpParam);
	afx_msg LONG OnEditKillFocus(UINT ipParam, LONG lpParam);
	afx_msg void OnInsertDate();
	afx_msg void OnDeleteDate();
	afx_msg void OnUpdateCal(LONG);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	CWnd*	m_pParent;
	int		m_nID;
	char	pcmUrno[URNOLEN]; // URNO of the line selected on the main screen
	char	pcmPrevUrno[URNOLEN]; // When selection on main screen changes, ask user if they want to update the DB, if YES then prev URNO required for update

public:
	CString omDelRel;
	CCSPtrArray <GRPDATA> omNewRelations;
	void SendCancel(void);
	void SelectDate(int ipLine);
	void UpdateDates(void);


private:
	void UpdateView(SEC_REC_TYPE rpType);
	GrpTableViewer		omGrpTableViewer;
	void				UpdateUsidList(); // function to mirror prmUsidList with the listBox
	void				SelectAllocated(); // select allocated profiles in the list box
	int					imSelectedLine;	// last selected line (for single selection only)
	int					imNumUsids;		// num of usids/profiles in the list box
	void UpdateSelFlag();
	CCSPtrArray <SELDATA> omSelData; // data for values in the comboBox
	void AddSel(bool bpSingleSel, SEC_REC_TYPE rpType, SEC_REC_TYPE rpAllocType);
	void CheckChange();
	bool ChangesFound();
	void InitList();
	int imCurrSel; // index into omSelData --> which value is selected in the comboBox
	SEC_REC_TYPE rmLastType;
	CCSTable *pomCalTable;
	CalTableViewer omCalTableViewer;
	CString omUrno; // URNO of the record last selected --> for calendar dates
	void EnableEditFields(bool bpEnable);
	bool bmEditFieldsEnabled;
	void LoadCalData(void);
	void DisplayCalData(const int ipLine = -1);
	void InsertDate(const char *pcpUrno);
	bool BroadcastRequiresUpdate(const char *pcpFieldList, const char *pcpDataList);
	void HandleReload(CString opMessage);
public:
	void ProcessDelSec(const char *pcpFieldList, const char *pcpDataList);
	void ProcessNewRel(const char *pcpFieldList, const char *pcpDataList);
	void ProcessDelRel(const char *pcpFieldList, const char *pcpDataList);
	void HandleSecDelete(SECDATA *prpSec);
};

extern CAllocatedDlg *pogAllocDlg;

#endif // __CALLOCATEDDLG__
