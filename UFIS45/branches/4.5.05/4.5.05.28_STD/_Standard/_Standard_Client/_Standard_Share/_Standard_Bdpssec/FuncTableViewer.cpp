// FuncTableViewer.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <CCSGlobl.h>

#include <BDPS_SEC.h>
#include <FuncTableViewer.h>
#include <ProfileListViewer.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>
#include <ObjList.h>
#include <resrc1.h>


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



//*************************************************************************************
//
// FuncTableViewer() - constructor
//
//*************************************************************************************

FuncTableViewer::FuncTableViewer()
{
	strcpy(rmStatusColours[0].STAT,HIDDEN_STAT);
	rmStatusColours[0].COLOUR = BLUE;
	strcpy(rmStatusColours[1].STAT,DISABLED_STAT);
	rmStatusColours[1].COLOUR = RED;
	strcpy(rmStatusColours[2].STAT,ENABLED_STAT);
	rmStatusColours[2].COLOUR = GREEN;
}


//*************************************************************************************
//
// FuncTableViewer() - destructor
//
//*************************************************************************************

FuncTableViewer::~FuncTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void FuncTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);

	omIndicies.DeleteAll(); // list of indicies into omLines of the lines displayed
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int FuncTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void FuncTableViewer::DeleteLine(int ipLineno)
{
	
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void FuncTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void FuncTableViewer::ChangeViewTo(CCSPtrArray <ProfileListObject> &ropPrvData)
{
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakePrvLines(ropPrvData);

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}


void FuncTableViewer::ChangeViewTo()
{
	// delete all lines defined
	DeleteAll();

	MakeFktLines();

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - select for user/profile only
//
//*************************************************************************************

bool FuncTableViewer::IsPassFilter(const char *pcpFsec)
{
//	return strcmp(pcpFsec,pcmProf) ? false : true;
	return true;
}

//*************************************************************************************
//
// IsThisFappSubd() - display for this application/subdivision only
//
//*************************************************************************************

bool FuncTableViewer::IsThisFappSubd(const char *pcpFapp,const char *pcpSubd)
{
	return (strcmp(pcpFapp,pcmFapp)||strcmp(pcpSubd,pcmSubd)) ? false : true;
}



//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int FuncTableViewer::CompareLine(FUNCTAB_LINEDATA *prpLine1, FUNCTAB_LINEDATA *prpLine2)
{
	return strcmp(prpLine1->FUAL,prpLine2->FUAL);
}


/////////////////////////////////////////////////////////////////////////////
// FuncTableViewer -- code specific to this class

COLORREF FuncTableViewer::GetStatusColour(char *pcpStat)
{
	for(int ilLoop=0; ilLoop<NUMSTATS; ilLoop++)
		if( !strcmp(rmStatusColours[ilLoop].STAT,pcpStat) )
			return rmStatusColours[ilLoop].COLOUR;

	return BLACK;
}


//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void FuncTableViewer::MakePrvLines(CCSPtrArray <ProfileListObject> &ropPrvData)
{
	int ilNumPrvs = ropPrvData.GetSize();
	for(int ilPrv = 0; ilPrv < ilNumPrvs; ilPrv++)
	{
		MakePrvLine(&ropPrvData[ilPrv]);
	}
}


void FuncTableViewer::MakeFktLines()
{
	int ilCount;
	
	ilCount = ogCedaFktData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeFktLine(&ogCedaFktData.omData[i]);
}



//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void FuncTableViewer::MakePrvLine(ProfileListObject *prpObj)
{

//	// filter out either profiles or users or modul
//	if( !IsPassFilter(prpPrv->FSEC)) 
//		return;

	// get the corresponding FKT record containing SUBD,FUNC,TYPE etc
	FKTDATA *prlFkt = prpObj->Fkt;
	PRVDATA *prlPrv = prpObj->Prv;
	
	FUNCTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prlPrv->URNO);
	strcpy(rlLine.FAPP,prlPrv->FAPP);
	strcpy(rlLine.SUBD,prlFkt->SUBD);
	strcpy(rlLine.FUNC,prlFkt->FUNC);
	strcpy(rlLine.FUAL,prlFkt->FUAL);

	// if FUAL is defined then use it instead of FUNC
	if( !strcmp(rlLine.FUAL," ") || strlen(rlLine.FUAL) <= 0 )
		strcpy(rlLine.FUAL,rlLine.FUNC);

	
	strcpy(rlLine.TYPE,prlFkt->TYPE);
	strcpy(rlLine.oldSTAT,prlPrv->tmpSTAT);
	strcpy(rlLine.newSTAT,prlPrv->tmpSTAT);

	// given "B" and "1" return "Button" and "Enabled"
	ogObjList.GetDesc(prlFkt->TYPE,prlPrv->tmpSTAT,rlLine.TYPEdesc,rlLine.STATdesc);

	CreateLine(&rlLine);
}


void FuncTableViewer::MakeFktLine(FKTDATA  *prpFkt)
{

	// filter out either profiles or users or modul
//	if( !IsPassFilter(prpPrv->FSEC)) 
//		return;

	
	FUNCTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpFkt->URNO);
	strcpy(rlLine.FAPP,prpFkt->FAPP);
	strcpy(rlLine.SUBD,prpFkt->SUBD);
	strcpy(rlLine.FUNC,prpFkt->FUNC);
	strcpy(rlLine.FUAL,prpFkt->FUAL);
	strcpy(rlLine.TYPE,prpFkt->TYPE);
	strcpy(rlLine.oldSTAT,prpFkt->STAT);
	strcpy(rlLine.newSTAT,prpFkt->STAT);

	if(prpFkt->SDAL[0] == '1')
		rlLine.UsedOnlyInOldProfiles = true;

	// given "B" and "1" return "Button" and "Enabled"
	ogObjList.GetDesc(prpFkt->TYPE,prpFkt->STAT,rlLine.TYPEdesc,rlLine.STATdesc);


	CreateLine(&rlLine);
}


//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int FuncTableViewer::CreateLine(FUNCTAB_LINEDATA *prpNewLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpNewLine, &omLines[ilLineno]) <= 0)
            break;  

	FUNCTAB_LINEDATA  rlLine;
    rlLine = *prpNewLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// FuncTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
//
//*************************************************************************************

void FuncTableViewer::UpdateDisplay()
{
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
//	pomTable->SetLeftDockingRange(1);
//	pomTable->SetRightDockingRange(1);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Length = 110; // ???
	rlHeader.Text = GetString(IDS_FUNCTABLEVIEWER_TYPE);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 263; // ???
	rlHeader.Text = GetString(IDS_FUNCTABLEVIEWER_FUNCTION);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 90; // ???
	rlHeader.Text = GetString(IDS_FUNCTABLEVIEWER_STATUS);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	
//	pomTable->SetDefaultSeparator();
	
   // step 2 set linedata
	CCSPtrArray <TABLE_COLUMN> olColList;

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;


	int ilNumLines = omLines.GetSize();
	int ilLinesAdded = 0, ilLc;
	omIndicies.DeleteAll(); // list of indicies into omLines of the lines displayed
	for (ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		FUNCTAB_LINEDATA *prlLine = &omLines[ilLc];

		if( IsThisFappSubd(prlLine->FAPP,prlLine->SUBD) )
		{
			rlColumnData.Text = prlLine->TYPEdesc;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = prlLine->FUAL;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = prlLine->STATdesc;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)prlLine);

			// disable drag&drop for each line
			pomTable->SetTextLineDragEnable(ilLinesAdded,FALSE);

			olColList.DeleteAll();


			// remember the index of this line for updating
			int *pilIndex = new int;
			*pilIndex = ilLc;
			omIndicies.Add(pilIndex);


			ilLinesAdded++;
		}
	}

	pomTable->DisplayTable();


	// set the line colours depending on the STAT
	int ilNumLinesDisplayed = omIndicies.GetSize();
	for( ilLc=0; ilLc < ilNumLinesDisplayed; ilLc++)
	{
		FUNCTAB_LINEDATA *prlLine = &omLines[(int) omIndicies[ilLc]];
		pomTable->SetTextLineColor(ilLc,GetStatusColour(prlLine->newSTAT),prlLine->UsedOnlyInOldProfiles ? SILVER : WHITE );
	}

}

void FuncTableViewer::SelectLine(const int ipLine)
{
	if( ipLine < omIndicies.GetSize() )
		pomTable->SelectLine(ipLine);
}

// given the line number selected in the Functions CCSTable, update newSTAT
void FuncTableViewer::UpdateStatus(const int ipLine, CWnd *popParentWnd)
{
	if( ipLine >= 0 && ipLine < omIndicies.GetSize() )
	{
		int ilLine = omIndicies[ipLine];

		if(omLines[ilLine].UsedOnlyInOldProfiles)
		{
			if(popParentWnd)
			{
				CString olMsg = GetString(IDS_OLDPROFILE) + CString("\n") + CString(omLines[ilLine].FUAL);
				popParentWnd->MessageBox(olMsg,"",MB_ICONINFORMATION);
			}
		}
		else
		{
			// given "B" and "-" return "0" and "Disabled"
			ogObjList.NextStat(omLines[ilLine].TYPE,omLines[ilLine].newSTAT,omLines[ilLine].newSTAT,omLines[ilLine].STATdesc);

			// update the current line characteristics
			CCSPtrArray <TABLE_COLUMN> rlLine;
			pomTable->GetTextLineColumns(&rlLine, ipLine);
			rlLine[2].Text = omLines[ilLine].STATdesc;

			pomTable->SetTextLineColor(ipLine,GetStatusColour(omLines[ilLine].newSTAT),WHITE);
		}
	}

}

// given the an array of line numbers selected in the Functions CCSTable, set STAT to pcpStat
void FuncTableViewer::UpdateStatus(int *pipLines,int ipNumLines,char *pcpStat, CWnd *popParentWnd)
{
	char clTypeDesc[100],clStatDesc[100];
	int ilLc;

	CString olEntriesNotChanged = "";
	for(ilLc=0; ilLc<ipNumLines; ilLc++)
	{
		if( pipLines[ilLc] >= 0 && pipLines[ilLc] < omIndicies.GetSize() )
		{
			int ilLine = omIndicies[pipLines[ilLc]];

			if(omLines[ilLine].UsedOnlyInOldProfiles)
			{
				olEntriesNotChanged += CString("\n") + CString(omLines[ilLine].FUAL);
			}
			else
			{
				// given "B" and "1" return "Button" and "Enabled"
				if(ogObjList.GetDesc(omLines[ilLine].TYPE,pcpStat,clTypeDesc,clStatDesc))
				{
					// only update the line if the TYPE and STAT were found in the object list
					strcpy(omLines[ilLine].newSTAT,pcpStat);
					strcpy(omLines[ilLine].TYPEdesc,clTypeDesc);
					strcpy(omLines[ilLine].STATdesc,clStatDesc);

					// update the current line characteristics
					CCSPtrArray <TABLE_COLUMN> rlLine;
					pomTable->GetTextLineColumns(&rlLine, pipLines[ilLc]);
					rlLine[2].Text = omLines[ilLine].STATdesc;

					pomTable->SetTextLineColor(pipLines[ilLc],GetStatusColour(omLines[ilLine].newSTAT),WHITE);
				}
			}
		}
	}

	if(popParentWnd && !olEntriesNotChanged.IsEmpty())
	{
		CString olMsg = GetString(IDS_OLDPROFILE) + olEntriesNotChanged;
		popParentWnd->MessageBox(olMsg,"",MB_ICONINFORMATION);
	}
}

