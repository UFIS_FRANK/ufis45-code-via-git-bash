// BcPrxy.h : Declaration of the CBcPrxy

#ifndef __BCPRXY_H_
#define __BCPRXY_H_

#include <resource.h>       // main symbols
#include <BcProxyCP.h>
//#include "CCSPtrArray.h"
#include <CCSGlobl.h>
#include <atlbase.h>

/////////////////////////////////////////////////////////////////////////////
// CBcPrxy
class CBcPrxy;
//extern CBcPrxy *pogPrxy;
//extern CCSPtrArray<CBcPrxy> omProxyConnections;

class ATL_NO_VTABLE CBcPrxy : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CBcPrxy, &CLSID_BcPrxy>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CBcPrxy>,
	public IDispatchImpl<IBcPrxy, &IID_IBcPrxy, &LIBID_BcProxyLib>,
	public CProxy_IBcPrxyEvents< CBcPrxy >
{
public:

	CBcPrxy();

	~CBcPrxy();

DECLARE_REGISTRY_RESOURCEID(IDR_BCPRXY)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CBcPrxy)
	COM_INTERFACE_ENTRY(IBcPrxy)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CBcPrxy)
CONNECTION_POINT_ENTRY(DIID__IBcPrxyEvents)
END_CONNECTION_POINT_MAP()


// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IBcPrxy
public:
	STDMETHOD(get_BuildDate)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(SetFilterRange)(BSTR table, BSTR sFromField, BSTR sToField, BSTR sFromValue, BSTR sToValue);
	CCSPtrArray<BcStruct> olBcArray;
	bool bmSpool;
	STDMETHOD(GetNextBufferdBC)(/*[out, retval]*/ BSTR *pReqId, /*[out, retval]*/ BSTR *pDest1,/*[out, retval]*/ BSTR *pDest2,/*[out, retval]*/ BSTR *pCmd,/*[out, retval]*/ BSTR *pObject,/*[out, retval]*/ BSTR *pSeq,/*[out, retval]*/ BSTR *pTws,/*[out, retval]*/ BSTR *pTwe,/*[out, retval]*/ BSTR *pSelection,/*[out, retval]*/ BSTR *pFields,/*[out, retval]*/ BSTR *pData,/*[out, retval]*/ BSTR *pBcNum);
	STDMETHOD(SetSpoolOff)();
	STDMETHOD(SetSpoolOn)();
	STDMETHOD(get_Version)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(Unadvise)(DWORD dwCookie);
	STDMETHOD(UnregisterObject)(BSTR table, BSTR command);
	STDMETHOD(RegisterObject)(/*[out]*/ BSTR table, BSTR command,BOOL ownBc,BSTR appl);
};
#endif //__BCPRXY_H_
