// BcProxyDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BcProxy.h>
#include <CCSGlobl.h >
#include <BcProxyDlg.h>
#include <atlbase.h>
//#include <afxpriv.h>              // For WM_KICKIDLE

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

extern "C" int WINAPI GetWorkstationName (LPSTR ws_name);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBcProxyDlg dialog

CBcProxyDlg::CBcProxyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBcProxyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBcProxyDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	char wks[256];
	::GetWorkstationName(wks);
	omWksName = CString(wks);

	bmCheckApplication = false;
	char pclConfigPath[256];
	char pclTmpBuf[256];
	
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	::GetPrivateProfileString("GLOBAL","BCCHECKAPPLICATION","NO",pclTmpBuf,sizeof(pclTmpBuf),pclConfigPath);
	if (stricmp(pclTmpBuf,"YES") == 0)
	{
		bmCheckApplication = true;
	}

}

void CBcProxyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBcProxyDlg)
	DDX_Control(pDX, IDC_EDIT_COMMAND, m_EditCommand);
	DDX_Control(pDX, IDC_EDIT_TABLE, m_EditTable);
	DDX_Control(pDX, IDC_BCLIST, m_BcList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBcProxyDlg, CDialog)
	//{{AFX_MSG_MAP(CBcProxyDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_FIRE, OnButtonFire)
	ON_BN_CLICKED(IDC_BUTTON_TESTFIRE, OnButtonTestfire)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBcProxyDlg message handlers

BOOL CBcProxyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	if(bgApplicationHidden == true)
	{
		ShowWindow(SW_HIDE);
	}

	// Add "About..." menu item to system menu.
	ogCommHandler.RegisterBcWindow(this);

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	COleMessageFilter *polFilter = AfxOleGetMessageFilter();
	if (polFilter)
	{
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));


		char pclRetryReply[128];
		GetPrivateProfileString("BcProxy", "RetryReply", "10000", pclRetryReply, sizeof pclRetryReply, pclConfigPath);
		
		DWORD dwRetryReply = atol(pclRetryReply);
		polFilter->SetRetryReply(dwRetryReply);

		char pclPendingDelay[128];
		GetPrivateProfileString("BcProxy", "PendingDelay", "8000", pclPendingDelay, sizeof pclPendingDelay, pclConfigPath);

		DWORD dwPendingDelay = atol(pclPendingDelay);
		polFilter->SetMessagePendingDelay(dwPendingDelay);

		char pclBusyDialog[128];
		GetPrivateProfileString("BcProxy", "BusyDialog", "YES", pclBusyDialog, sizeof pclBusyDialog, pclConfigPath);
		if (stricmp(pclBusyDialog,"NO") == 0)
		{
			polFilter->EnableBusyDialog(FALSE);
		}
		else
		{
			polFilter->EnableBusyDialog(TRUE);
		}

		char pclNotRespondingDialog[128];
		GetPrivateProfileString("BcProxy", "NotRespondingDialog", "YES", pclNotRespondingDialog, sizeof pclNotRespondingDialog, pclConfigPath);
		if (stricmp(pclNotRespondingDialog,"NO") == 0)
		{
			polFilter->EnableNotRespondingDialog(FALSE);
		}
		else
		{
			polFilter->EnableNotRespondingDialog(TRUE);
		}

	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CBcProxyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBcProxyDlg::OnPaint() 
{
	if(bgApplicationHidden == true)
	{
		ShowWindow(SW_HIDE);
	}
	else
	{
		if (IsIconic())
		{
			CPaintDC dc(this); // device context for painting

			SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

			// Center icon in client rectangle
			int cxIcon = GetSystemMetrics(SM_CXICON);
			int cyIcon = GetSystemMetrics(SM_CYICON);
			CRect rect;
			GetClientRect(&rect);
			int x = (rect.Width() - cxIcon + 1) / 2;
			int y = (rect.Height() - cyIcon + 1) / 2;

			// Draw the icon
			dc.DrawIcon(x, y, m_hIcon);
		}
		else
		{
			CDialog::OnPaint();
		}
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBcProxyDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LONG CBcProxyDlg::OnBcAdd(UINT wParam, LONG lParam)
{
	CString		ReqId,
				Dest1,
				Dest2,
				Cmd,
				Object,
				Seq,
				Tws,
				Twe,
				Selection,
				Fields,
				Data,
				BcNum;

	CComBSTR	strReqId,
				strDest1,
				strDest2,
				strCmd,
				strObject,
				strSeq,
				strTws,
				strTwe,
				strSelection,
				strFields,
				strData,
				strBcNum;

	CCSPtrArray<BcStruct> olBcArray;
	CString *p = NULL;

	ogBcHandle.GetAtlBc (olBcArray, wParam);

	for (int i = 0; i < olBcArray.GetSize(); i++)
	{
		CString olListString;
		BcStruct	rlBc;
		rlBc = olBcArray[i];
		ReqId		= olBcArray[i].ReqId;		
		Dest1		= olBcArray[i].Dest1;		
		Dest2		= olBcArray[i].Dest2;		
		Cmd			= olBcArray[i].Cmd;		
		Object		= olBcArray[i].Object;		
		Seq			= olBcArray[i].Seq;		
		Tws			= olBcArray[i].Tws;		
		Twe			= olBcArray[i].Twe;		
		Selection	= olBcArray[i].Selection;
		Fields		= olBcArray[i].Fields;		
		Data		= olBcArray[i].Data;		
		BcNum		= olBcArray[i].BcNum;

		strReqId	 = ReqId.GetBuffer(0);
		strDest1	 = Dest1.GetBuffer(0);
		strDest2	 = Dest2.GetBuffer(0);
		strCmd		 = Cmd.GetBuffer(0);
		strObject	 = Object.GetBuffer(0);
		strSeq		 = Seq.GetBuffer(0);
		strTws		 = Tws.GetBuffer(0);
		strTwe		 = Twe.GetBuffer(0);
		strSelection = Selection.GetBuffer(0);
		strFields	 = Fields.GetBuffer(0);
		strData		 = Data.GetBuffer(0);
		strBcNum	 = BcNum.GetBuffer(0);
 
		olListString += "BcNum"		+ BcNum		+ " ";
		olListString += "Cmd"		+ Cmd		+ " ";
		olListString += "Object"	+ Object	+ " ";		
		olListString += "Selection" + Selection + " ";
 		olListString += "ReqId: "	+ ReqId		+ " ";
		olListString += "Dest1"		+ Dest1		+ " ";
		olListString += "Dest2"		+ Dest2		+ " ";
		olListString += "Seq"		+ Seq		+ " ";
		olListString += "Tws"		+ Tws		+ " ";	
		olListString += "Twe"		+ Twe		+ " ";	
		olListString += "Fields"	+ Fields	+ " ";
		olListString += "Data"		+ Data		+ " ";

		//just fill the listbox		
		m_BcList.InsertString (0, olListString);
		int ilCount = m_BcList.GetCount();
 		if (ilCount >= 50)
		{
			m_BcList.DeleteString (ilCount - 1);
		}

		
		//send to all interested clients
		for (int i = 0; i < ogConnections.GetSize(); i++)
		{
			PROXY_INSTANCES *prlProxyInstance = &ogConnections[i];
			CBcPrxy *prlProx = prlProxyInstance->pProxy;

			if(prlProxyInstance->mapTabCmd.Lookup(CString ("@;ALL;@"),(void *&) p) == TRUE)
			{
				if (prlProx->bmSpool == true)
				{
					prlProx->olBcArray.NewAt(prlProx->olBcArray.GetSize(), rlBc);
				}
				else
				{
					for (int k = 0; k < prlProx->olBcArray.GetSize(); k++)
					{
						strReqId	 = prlProx->olBcArray[k].ReqId;
						strDest1	 = prlProx->olBcArray[k].Dest1;
						strDest2	 = prlProx->olBcArray[k].Dest2;
						strCmd		 = prlProx->olBcArray[k].Cmd;
						strObject	 = prlProx->olBcArray[k].Object;
						strSeq		 = prlProx->olBcArray[k].Seq;
						strTws		 = prlProx->olBcArray[k].Tws;
						strTwe		 = prlProx->olBcArray[k].Twe;
						strSelection = prlProx->olBcArray[k].Selection;
						strFields	 = prlProx->olBcArray[k].Fields;
						strData		 = prlProx->olBcArray[k].Data;
						strBcNum	 = prlProx->olBcArray[k].BcNum;
 
						if (CheckFilter(prlProx, Object, Fields, Data) == true)
						{
							prlProx->Fire_OnBcReceive (BSTR(strReqId),
														BSTR(strDest1),
														BSTR(strDest2),
														BSTR(strCmd),
														BSTR(strObject),
														BSTR(strSeq),
														BSTR(strTws),
														BSTR(strTwe),
														BSTR(strSelection),
														BSTR(strFields),
														BSTR(strData),
														BSTR(strBcNum));
						}
					}
					prlProx->olBcArray.DeleteAll();

					strReqId	 = ReqId.GetBuffer(0);
					strDest1	 = Dest1.GetBuffer(0);
					strDest2	 = Dest2.GetBuffer(0);
					strCmd		 = Cmd.GetBuffer(0);
					strObject	 = Object.GetBuffer(0);
					strSeq		 = Seq.GetBuffer(0);
					strTws		 = Tws.GetBuffer(0);
					strTwe		 = Twe.GetBuffer(0);
					strSelection = Selection.GetBuffer(0);
					strFields	 = Fields.GetBuffer(0);
					strData		 = Data.GetBuffer(0);
					strBcNum	 = BcNum.GetBuffer(0);
 
					if (CheckFilter(prlProx, Object, Fields, Data) == true)
					{
						prlProx->Fire_OnBcReceive (BSTR(strReqId),
													BSTR(strDest1),
													BSTR(strDest2),
													BSTR(strCmd),
													BSTR(strObject),
													BSTR(strSeq),
													BSTR(strTws),
													BSTR(strTwe),
													BSTR(strSelection),
													BSTR(strFields),
													BSTR(strData),
													BSTR(strBcNum));
					}
				}
			}
			else
			{
				CString olKeyString = Object + Cmd;
				if(prlProxyInstance->mapTabCmd.Lookup(olKeyString,(void *&) p) == TRUE)
				{
					int ilIndex = p->Find("|0|");
					if (ilIndex >= 0)
					{
						if (this->omWksName == CString(strReqId))
						{
							if (bmCheckApplication)
							{
								CString olAppl = p->Mid(ilIndex + 3);
								if (olAppl.GetLength() == 0 || Twe.Find(olAppl) == 0)							
									continue;
							}
							else
								continue;
						}
					}

					if (prlProx->bmSpool == true)
					{
						prlProx->olBcArray.NewAt(prlProx->olBcArray.GetSize(), rlBc);
					}
					else
					{
						for (int k = 0; k < prlProx->olBcArray.GetSize(); k++)
						{
							strReqId	 = prlProx->olBcArray[k].ReqId;
							strDest1	 = prlProx->olBcArray[k].Dest1;
							strDest2	 = prlProx->olBcArray[k].Dest2;
							strCmd		 = prlProx->olBcArray[k].Cmd;
							strObject	 = prlProx->olBcArray[k].Object;
							strSeq		 = prlProx->olBcArray[k].Seq;
							strTws		 = prlProx->olBcArray[k].Tws;
							strTwe		 = prlProx->olBcArray[k].Twe;
							strSelection = prlProx->olBcArray[k].Selection;
							strFields	 = prlProx->olBcArray[k].Fields;
							strData		 = prlProx->olBcArray[k].Data;
							strBcNum	 = prlProx->olBcArray[k].BcNum;

							if (CheckFilter(prlProx, Object, Fields, Data) == true)
							{
								prlProx->Fire_OnBcReceive (BSTR(strReqId),
															BSTR(strDest1),
															BSTR(strDest2),
															BSTR(strCmd),
															BSTR(strObject),
															BSTR(strSeq),
															BSTR(strTws),
															BSTR(strTwe),
															BSTR(strSelection),
															BSTR(strFields),
															BSTR(strData),
															BSTR(strBcNum));
							}
						}
						prlProx->olBcArray.DeleteAll();

						strReqId	 = ReqId.GetBuffer(0);
						strDest1	 = Dest1.GetBuffer(0);
						strDest2	 = Dest2.GetBuffer(0);
						strCmd		 = Cmd.GetBuffer(0);
						strObject	 = Object.GetBuffer(0);
						strSeq		 = Seq.GetBuffer(0);
						strTws		 = Tws.GetBuffer(0);
						strTwe		 = Twe.GetBuffer(0);
						strSelection = Selection.GetBuffer(0);
						strFields	 = Fields.GetBuffer(0);
						strData		 = Data.GetBuffer(0);
						strBcNum	 = BcNum.GetBuffer(0);

						if (CheckFilter(prlProx, Object, Fields, Data) == true)
						{
							prlProx->Fire_OnBcReceive (BSTR(strReqId),
														BSTR(strDest1),
														BSTR(strDest2),
														BSTR(strCmd),
														BSTR(strObject),
														BSTR(strSeq),
														BSTR(strTws),
														BSTR(strTwe),
														BSTR(strSelection),
														BSTR(strFields),
														BSTR(strData),
														BSTR(strBcNum));
						}
					}
				}
			}
		}
	}

	//tidy up (this was the problem with the increasing memory usage....)
	olBcArray.DeleteAll();
	return TRUE;

} // END CBcProxyDlg::OnBcAdd


void CBcProxyDlg::OnDestroy() 
{
	ogCommHandler.UnRegisterBcWindow(this);
	CDialog::OnDestroy();
}

void CBcProxyDlg::OnButtonFire() 
{
	CComBSTR testStr(_T("Hallo"));
	CString olKeyString, olTableString, olCommandString;
	void *p=NULL;

	m_EditTable.GetWindowText   (olTableString);
	m_EditCommand.GetWindowText (olCommandString);
	olKeyString = olTableString + olCommandString;

	for (int i = 0; i < ogConnections.GetSize(); i++)
	{
		PROXY_INSTANCES *prlProxyInstance = &ogConnections[i];
		CBcPrxy *prlProx = prlProxyInstance->pProxy;

		if(prlProxyInstance->mapTabCmd.Lookup(CString ("@;ALL;@"), p) == TRUE)
		{
			if (CheckFilter(prlProx, olTableString, "", "") == true)
			{
				prlProx->Fire_OnBcReceive (BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr),
											BSTR(testStr));
			}
		}
		else
		{
			if(prlProxyInstance->mapTabCmd.Lookup(olKeyString, p) == TRUE)
			{
				if (CheckFilter(prlProx, olTableString, "", "") == true)
				{
					prlProx->Fire_OnBcReceive (BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr),
												BSTR(testStr));
				}
			}
		}
	}
}

void CBcProxyDlg::OnButtonTestfire() 
{
	CComBSTR testStr(_T("Hallo"));

	/*	
	for (int i = 0; i < omProxyConnections.GetSize(); i++)
	{
		omProxyConnections[i].Fire_OnBcReceive(BSTR(testStr), BSTR(testStr),BSTR(testStr),BSTR(testStr),
			BSTR(testStr), BSTR(testStr),BSTR(testStr),
			BSTR(testStr),BSTR(testStr),BSTR(testStr),BSTR(testStr),BSTR(testStr));
	}*/
}

int GetItemNo(char* pcpLine, char* pcpItem, char* pcpSepa)
{
	char *ptr;
	long i=0;
	int  count = -1;
	char c;

	ptr = strstr(pcpLine, pcpItem);
	if(ptr != NULL)
	{
		c = *ptr;
		*ptr = 0x00;
		if(ptr == pcpLine)
		{
			count = 0;
		}
		while(pcpLine[i] != 0x00)
		{
			if(pcpLine[i] == pcpSepa[0])
			{
				if(count == -1) 
				{
					count=0;
				}
				count++;
			}
			i++;
		}
		*ptr = c;
	}
	return count;
}

bool GetItem(int ipItemNo,char *pcpItemList,char *pcpItemData)
{
	int ilActualItem = 1;
	int ilCount = 0;

	bool blFound = false;

	for(ilActualItem = 1, ilCount = 0; pcpItemList[ilCount] != '\0' && ilActualItem <= ipItemNo; ilCount++)
	{
		if (ilActualItem == ipItemNo)
		{
			blFound = true;
			*pcpItemData = pcpItemList[ilCount];
			if (*pcpItemData == ',')
			{
				break;
			}
			pcpItemData++;
		}
		if (pcpItemList[ilCount] == ',')
		{
			ilActualItem++;
		}
	}

	*pcpItemData = '\0';

	return blFound;
}


long CT_CountPattern(char *Buffer, char *Pattern)
{
	long Count = 0;
	long PatLen = 0;
	char *Ptr = NULL;
	PatLen = strlen(Pattern);
	Ptr = Buffer;
	while (Ptr != NULL)
	{
		Ptr = strstr(Ptr, Pattern);
		if (Ptr != NULL)
		{
			Count++;
			Ptr += PatLen;
		} /* end if */
	} /* end while */
	return Count;
} /* End */


/* ============================================================= 
MWO: 25.03.2003
copies all items from item position to item position into
pcpResult
================================================================ */
int CT_GetItemsFromTo(char* pcpResult, char* pcpLine, long lpFrom, long lpTo, char* pcpSepa)
{
	long	Count = 0;
	long	PatLen = 0;
	char	*Ptr = NULL;
	char	*NextPtr = NULL;
	char	*FromPtr = NULL;
	char	*ToPtr = NULL;
	PatLen = strlen(pcpSepa);

	Ptr = pcpLine;
	if(lpFrom == 0)
	{
		FromPtr = Ptr;
	}
	while (Ptr != NULL && ToPtr == NULL)
	{
		Ptr = strstr(Ptr, pcpSepa);
		if (Ptr != NULL)
		{
			Ptr += PatLen;
			Count++;
			if(Count == lpFrom)
			{
				FromPtr = Ptr;
			}
			if(Count >= lpTo)
			{
				ToPtr = Ptr;
				NextPtr = Ptr + PatLen;
				NextPtr = strstr(Ptr, pcpSepa);
				if (NextPtr != NULL)
				{
					ToPtr = NextPtr;
				}
			}
		} /* end if */
		else
		{
			if(Count >= lpTo)
			{
				ToPtr = Ptr;
			}
		}
	} /* end while */
	if(lpTo == CT_CountPattern(pcpLine, ",") )
	{
		ToPtr = pcpLine + strlen(pcpLine) + 1;
	}
	if (FromPtr != NULL && ToPtr != NULL)
	{
		strncpy(pcpResult, FromPtr, (ToPtr-FromPtr));
		pcpResult[ToPtr-FromPtr]=0x00;
	}
	return -1;
}

bool CBcProxyDlg::CheckFilter(CBcPrxy *prpClient, CString opTable, CString opFields, CString opData)
{
	bool blRet = true;
	int idxFrom, idxTo;
	CString olValFrom, olValTo;
	char pclFrom[1024], pclTo[1024];
	pclFrom[0] = '\0';
	pclTo[0] = '\0';

	for (int i = 0; i < ogClientFilterArray.GetSize(); i++)
	{
		if (ogClientFilterArray[i].pClient == prpClient)
		{
			if (ogClientFilterArray[i].strTable == opTable)
			{
				idxFrom = GetItemNo(opFields.GetBuffer(0), ogClientFilterArray[i].strFromField.GetBuffer(0), (char*)",");
				idxTo =   GetItemNo(opFields.GetBuffer(0), ogClientFilterArray[i].strToField.GetBuffer(0), (char*)",");
				if(idxFrom > -1 && idxTo > -1)
				{
					CT_GetItemsFromTo(pclFrom, opData.GetBuffer(0), idxFrom, idxFrom, (char*)",");
					CT_GetItemsFromTo(pclTo, opData.GetBuffer(0), idxTo, idxTo, (char*)",");
					if(strlen(pclFrom)>0 && strlen(pclTo)>0)
					{
						olValFrom = CString(pclFrom);
						olValTo   = CString(pclTo); 
						if (olValTo.GetLength() < 8)
						{
							olValTo = "99999999999999"; //maximum
						}

						if (IsOverlapped(olValFrom, olValTo, ogClientFilterArray[i].strFromValue, ogClientFilterArray[i].strToValue) == false)
						{
							blRet = false;
						}
					}
				}
				break;
			}
		}
	}

	return blRet;
}
