// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
//#include "BasicData.h"
//#include "CedaCfgData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool bgApplicationHidden;

//-------------------------------
//			global variables
//-------------------------------

//--- error handling
ofstream of_catch;


//--- general global variables
CString ogAppName = "BcProxy";	// This is the only string that is to be hardcoded here !!!
									// The Xs are only to recognize failures in SQLHDL.log
CString ogCallingApp = "";

char pcgUser[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgPasswd[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgConfigPath[142] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgHome[4] = "XXX";
char pcgTableExt[10] = "XXX";

//--- own classes' global objects
CCSLog			ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, "BcProxy");
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CCSBasic		ogCCSBasic;		// lib
//CBasicData	ogBasicData;	// local
//CedaCfgData	ogCfgData;
//PrivList		ogPrivList;
//DataSet		ogDataSet;
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

//the map where the key built of table + command are stored in
CCSPtrArray<CLIENT_FILTER> ogClientFilterArray;
//CBcPrxy *pogPrxy;
//CCSPtrArray<CBcPrxy> omProxyConnections;
CCSPtrArray<PROXY_INSTANCES> ogConnections;

bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return false;
	polCtrl->EnableWindow ( bpEnable );
	polCtrl->ShowWindow ( bpShow ? SW_SHOW : SW_HIDE );
	return true;
}

BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER  );
}

BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER  );
}