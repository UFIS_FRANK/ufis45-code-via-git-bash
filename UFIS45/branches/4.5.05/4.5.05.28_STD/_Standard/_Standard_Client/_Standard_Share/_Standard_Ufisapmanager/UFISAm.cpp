/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla				06/09/2000		Initial version
 *		cla				11/09/2000		isTlxPending added to avoid 
 *										multiple startup
 *		cla				18/10/2000		FipsCUTE support added
 *		cla				24/10/2000		StartFipsCute corrected and holdData
 *										changed to avoid multiple detach ops
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// UFISAm.cpp : Implementierung von CUFISAm
#include <stdafx.h>
#include <UFISAppMng.h>
#include <UFISAm.h>

/////////////////////////////////////////////////////////////////////////////
// CUFISAm

STDMETHODIMP CUFISAm::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IUFISAm
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

// ==================================================================
// 
// FUNCTION :  CUFISAm::TransferData()
// 
// * Description : Sends the data to the destination. Sorry hardcoded
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 12:17:58]
// 
// * Returns : [STDMETHODIMP] -
// 
// * Function parameters : 
// [Dest] - Source app
// [Orig] - Dest app
// [Data] - The data
// 
// ==================================================================
STDMETHODIMP CUFISAm::TransferData(long Dest, long Orig, BSTR Data)
{

	// ZU ERLEDIGEN: Implementierungscode hier hinzufügen


	switch(Dest)
	{
		case TlxPool:

			if((isTlxStarted == false) && (isTlxPending == false))
			{
				isTlxPending = true;
				StartTlxPool();
				m_holdTlxData.Attach(Data);
			}
			else
			{
				Fire_ForwardData(Orig,Data);
			}
			break;

		case FipsCUTE:

			if((isfCUTEStarted == false) && (isfCUTEPending == false))
			{
				isfCUTEPending = true;
				StartFipsCute();
				m_holdfCUTEData.Attach(Data);
			}
			else
			{
				Fire_ForwardData(Orig,Data);
			}
			break;
		case Fips:
			Fire_ForwardData(Orig,Data);
			break;

		case UCDialog:
			Fire_ForwardData(Orig,Data);
			break;
		
		case Bdps:
			Fire_ForwardData(Orig,Data);
			break;
	}
	return S_OK;
}

// ==================================================================
// 
// FUNCTION :  CUFISAm::AssignAppTag()
// 
// * Description : An AppId signals that we are now ready to proceed
// *			   messages
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 12:17:05]
// 
// * Returns : [STDMETHODIMP] -
// 
// * Function parameters : 
// [AppID] - the attached application
// 
// ==================================================================
STDMETHODIMP CUFISAm::AssignAppTag(long AppID)
{

	// TODO: Add your implementation code here
	/*
	 * We are going to assign the AppID
	 */
	CClntCon nCon(m_pActUnk,m_cookie,static_cast<ClntTags>(AppID));
	m_clntCon.push_back(nCon);
	if(AppID == TlxPool)
	{
		isTlxStarted = true;
		isTlxPending = false;
		Fire_ForwardData(Fips,BSTR(m_holdTlxData));
	}
	else if(AppID == FipsCUTE)
	{
		isfCUTEStarted = true;
		isfCUTEPending = false;
		Fire_ForwardData(Fips,BSTR(m_holdfCUTEData));
	}
		return S_OK;
}

STDMETHODIMP CUFISAm::Unadvise(DWORD pdwCookie)
{
	// TODO: Add your implementation code here
	/*
	 * First we have to find the right element in list
	 * delete it
	 */

	ClntList::iterator pos;

	for(pos =m_clntCon.begin();pos != m_clntCon.end();++pos)
	{
		if(*pos == pdwCookie)
		{
			m_clntCon.remove(*pos);
			break;
		}
	}
	CProxy_IUFISAmEvents< CUFISAm >::Unadvise(pdwCookie);
	return S_OK;
}

// ==================================================================
// 
// FUNCTION :  CUFISAm::ReadProfileInfo()
// 
// * Description : Reads profile information
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 09:49:51]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// 
// ==================================================================
void CUFISAm::ReadProfileInfo()
{

	char pclConfigPath[512];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(_T("UFISAPPMAN"), _T("TLX_LAUNCH_STR"), _T("Nothing"),
							m_tlxLaunchStr,sizeof m_tlxLaunchStr, pclConfigPath);

    GetPrivateProfileString(_T("UFISAPPMAN"), _T("FCUTE_LAUNCH_STR"), _T("Nothing"),
							m_fCUTELaunchStr,sizeof m_fCUTELaunchStr, pclConfigPath);
	
}

// ==================================================================
// 
// FUNCTION :  CUFISAm::StartTlxPool()
// 
// * Description : Starts the Telexpool application
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 09:52:37]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// 
// ==================================================================
void CUFISAm::StartTlxPool()
{

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si,sizeof(si));
	si.cb = sizeof(si);
	si.wShowWindow = SW_SHOWNORMAL;
	si.dwFlags = STARTF_USESHOWWINDOW;

	CreateProcess(NULL,m_tlxLaunchStr,
				  NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);
}

// ==================================================================
// 
// FUNCTION :  CUFISAm::StartFipsCute()
// 
// * Description :
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 10:03:48]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// 
// ==================================================================
void CUFISAm::StartFipsCute()
{


	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si,sizeof(si));
	si.cb = sizeof(si);
	si.wShowWindow = SW_SHOWNORMAL;
	si.dwFlags = STARTF_USESHOWWINDOW;

	CreateProcess(NULL,m_fCUTELaunchStr,
				  NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);
}
// ==================================================================
// 
// FUNCTION :  CUFISAm::DetachApp()
// 
// * Description : Is responsible for cleanup
// 
// 
// * Author : [cla AAT/IR], Created : [19.10.00 09:53:24]
// 
// * Returns : [STDMETHODIMP] - COM defauls
// 
// * Function parameters : 
// [AppID] - the detached application
// 
// ==================================================================
STDMETHODIMP CUFISAm::DetachApp(long AppID)
{

	// TODO: Add your implementation code here

	switch(AppID)
	{
		case TlxPool: 
			isTlxStarted = false;
			m_holdTlxData.Detach();
		break;
		case FipsCUTE:
			isfCUTEStarted = false;
			m_holdfCUTEData.Detach();
		break;
	}
	return S_OK;
}

STDMETHODIMP CUFISAm::get_Version(BSTR *pVal)
{
	CComBSTR bstrVersion("4.5.1.2");
	*pVal = bstrVersion.Detach();
	return S_OK;
}

STDMETHODIMP CUFISAm::get_BuildDate(BSTR *pVal)
{
	char pclDate[100]="";
	char pclTime[100]="";
	char pclResult[100]="";

	strcpy(pclDate, __DATE__);
	strcpy(pclTime, __TIME__);
	sprintf(pclResult, "%s - %s", pclDate, pclTime);
	CComBSTR bstrBuildDate(pclResult);
	*pVal = bstrBuildDate.Detach();

	return S_OK;
}

