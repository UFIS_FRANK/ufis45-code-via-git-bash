﻿namespace _Standard_Bdpsuif_Addon.UI
{
    partial class frmBlockParkingStand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBlockParkingStand));
            this.pnlTop = new System.Windows.Forms.Panel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.cboMultiBlockName = new System.Windows.Forms.ComboBox();
            this.lbHeader = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlImageCombo = new System.Windows.Forms.Panel();
            this.lbBitmap = new System.Windows.Forms.Label();
            this.lbReason = new System.Windows.Forms.Label();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.gpPst = new System.Windows.Forms.GroupBox();
            this.lblPositions = new System.Windows.Forms.Label();
            this.lsRight = new System.Windows.Forms.ListBox();
            this.lsLeft = new System.Windows.Forms.ListBox();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.gpFrequency = new System.Windows.Forms.GroupBox();
            this.pnlWeekdays = new System.Windows.Forms.Panel();
            this.ch5 = new System.Windows.Forms.CheckBox();
            this.ch6 = new System.Windows.Forms.CheckBox();
            this.ch7 = new System.Windows.Forms.CheckBox();
            this.ch4 = new System.Windows.Forms.CheckBox();
            this.ch1 = new System.Windows.Forms.CheckBox();
            this.ch3 = new System.Windows.Forms.CheckBox();
            this.ch2 = new System.Windows.Forms.CheckBox();
            this.chDaily = new System.Windows.Forms.CheckBox();
            this.gpValidity = new System.Windows.Forms.GroupBox();
            this.txtVToTime = new System.Windows.Forms.TextBox();
            this.txtVFromTime = new System.Windows.Forms.TextBox();
            this.lbValidityTo = new System.Windows.Forms.Label();
            this.lbValidityFrom = new System.Windows.Forms.Label();
            this.txtVToDate = new System.Windows.Forms.TextBox();
            this.txtVFromDate = new System.Windows.Forms.TextBox();
            this.gpTime = new System.Windows.Forms.GroupBox();
            this.lbTimeTo = new System.Windows.Forms.Label();
            this.lbTimeFrom = new System.Windows.Forms.Label();
            this.txtTimeTo = new System.Windows.Forms.TextBox();
            this.txtTimeFrom = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTop.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.gpPst.SuspendLayout();
            this.gpFrequency.SuspendLayout();
            this.pnlWeekdays.SuspendLayout();
            this.gpValidity.SuspendLayout();
            this.gpTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.GhostWhite;
            this.pnlTop.Controls.Add(this.btnRemove);
            this.pnlTop.Controls.Add(this.btnCopy);
            this.pnlTop.Controls.Add(this.cboMultiBlockName);
            this.pnlTop.Controls.Add(this.lbHeader);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(565, 32);
            this.pnlTop.TabIndex = 0;
            // 
            // btnRemove
            // 
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.Location = new System.Drawing.Point(538, 4);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(25, 22);
            this.btnRemove.TabIndex = 3;
            this.toolTip.SetToolTip(this.btnRemove, "Remove from list");
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Visible = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
            this.btnCopy.Location = new System.Drawing.Point(509, 4);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(25, 22);
            this.btnCopy.TabIndex = 2;
            this.toolTip.SetToolTip(this.btnCopy, "Copy");
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Visible = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // cboMultiBlockName
            // 
            this.cboMultiBlockName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMultiBlockName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMultiBlockName.BackColor = System.Drawing.Color.GhostWhite;
            this.cboMultiBlockName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMultiBlockName.FormattingEnabled = true;
            this.cboMultiBlockName.Location = new System.Drawing.Point(148, 4);
            this.cboMultiBlockName.Name = "cboMultiBlockName";
            this.cboMultiBlockName.Size = new System.Drawing.Size(355, 23);
            this.cboMultiBlockName.TabIndex = 1;
            this.cboMultiBlockName.Validating += new System.ComponentModel.CancelEventHandler(this.cboMultiBlockName_Validating);
            this.cboMultiBlockName.SelectedIndexChanged += new System.EventHandler(this.cboMultiBlockName_SelectedIndexChanged);
            // 
            // lbHeader
            // 
            this.lbHeader.AutoSize = true;
            this.lbHeader.BackColor = System.Drawing.Color.GhostWhite;
            this.lbHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbHeader.Location = new System.Drawing.Point(3, 5);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(139, 15);
            this.lbHeader.TabIndex = 0;
            this.lbHeader.Text = "Multiple Blocking for";
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnDelete);
            this.pnlBottom.Controls.Add(this.txtName);
            this.pnlBottom.Controls.Add(this.lblName);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.pnlImageCombo);
            this.pnlBottom.Controls.Add(this.lbBitmap);
            this.pnlBottom.Controls.Add(this.lbReason);
            this.pnlBottom.Controls.Add(this.txtReason);
            this.pnlBottom.Controls.Add(this.gpPst);
            this.pnlBottom.Controls.Add(this.gpFrequency);
            this.pnlBottom.Controls.Add(this.gpValidity);
            this.pnlBottom.Controls.Add(this.gpTime);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottom.Location = new System.Drawing.Point(0, 32);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(565, 591);
            this.pnlBottom.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(23, 586);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtName.Location = new System.Drawing.Point(75, 19);
            this.txtName.MaxLength = 25;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(18, 22);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name:";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(315, 586);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(187, 586);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlImageCombo
            // 
            this.pnlImageCombo.Location = new System.Drawing.Point(442, 14);
            this.pnlImageCombo.Name = "pnlImageCombo";
            this.pnlImageCombo.Size = new System.Drawing.Size(99, 28);
            this.pnlImageCombo.TabIndex = 3;
            // 
            // lbBitmap
            // 
            this.lbBitmap.AutoSize = true;
            this.lbBitmap.Location = new System.Drawing.Point(312, 22);
            this.lbBitmap.Name = "lbBitmap";
            this.lbBitmap.Size = new System.Drawing.Size(124, 13);
            this.lbBitmap.TabIndex = 2;
            this.lbBitmap.Text = "Bitmap for blocking times";
            // 
            // lbReason
            // 
            this.lbReason.AutoSize = true;
            this.lbReason.Location = new System.Drawing.Point(23, 228);
            this.lbReason.Name = "lbReason";
            this.lbReason.Size = new System.Drawing.Size(47, 13);
            this.lbReason.TabIndex = 6;
            this.lbReason.Text = "Reason:";
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(75, 225);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(470, 20);
            this.txtReason.TabIndex = 7;
            // 
            // gpPst
            // 
            this.gpPst.Controls.Add(this.lblPositions);
            this.gpPst.Controls.Add(this.lsRight);
            this.gpPst.Controls.Add(this.lsLeft);
            this.gpPst.Controls.Add(this.btnRight);
            this.gpPst.Controls.Add(this.btnLeft);
            this.gpPst.Location = new System.Drawing.Point(21, 318);
            this.gpPst.Name = "gpPst";
            this.gpPst.Size = new System.Drawing.Size(524, 253);
            this.gpPst.TabIndex = 9;
            this.gpPst.TabStop = false;
            // 
            // lblPositions
            // 
            this.lblPositions.AutoSize = true;
            this.lblPositions.Location = new System.Drawing.Point(50, 16);
            this.lblPositions.Name = "lblPositions";
            this.lblPositions.Size = new System.Drawing.Size(82, 13);
            this.lblPositions.TabIndex = 0;
            this.lblPositions.Text = "Parking Stands:";
            // 
            // lsRight
            // 
            this.lsRight.BackColor = System.Drawing.Color.LemonChiffon;
            this.lsRight.FormattingEnabled = true;
            this.lsRight.Location = new System.Drawing.Point(339, 33);
            this.lsRight.Name = "lsRight";
            this.lsRight.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lsRight.Size = new System.Drawing.Size(124, 212);
            this.lsRight.Sorted = true;
            this.lsRight.TabIndex = 5;
            // 
            // lsLeft
            // 
            this.lsLeft.FormattingEnabled = true;
            this.lsLeft.Location = new System.Drawing.Point(53, 32);
            this.lsLeft.Name = "lsLeft";
            this.lsLeft.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lsLeft.Size = new System.Drawing.Size(124, 212);
            this.lsLeft.Sorted = true;
            this.lsLeft.TabIndex = 2;
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(213, 80);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 3;
            this.btnRight.Text = "--->";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(213, 147);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 4;
            this.btnLeft.Text = "<---";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // gpFrequency
            // 
            this.gpFrequency.Controls.Add(this.pnlWeekdays);
            this.gpFrequency.Controls.Add(this.chDaily);
            this.gpFrequency.Location = new System.Drawing.Point(21, 120);
            this.gpFrequency.Name = "gpFrequency";
            this.gpFrequency.Size = new System.Drawing.Size(363, 86);
            this.gpFrequency.TabIndex = 5;
            this.gpFrequency.TabStop = false;
            this.gpFrequency.Text = "Weekdays";
            // 
            // pnlWeekdays
            // 
            this.pnlWeekdays.Controls.Add(this.ch5);
            this.pnlWeekdays.Controls.Add(this.ch6);
            this.pnlWeekdays.Controls.Add(this.ch7);
            this.pnlWeekdays.Controls.Add(this.ch4);
            this.pnlWeekdays.Controls.Add(this.ch1);
            this.pnlWeekdays.Controls.Add(this.ch3);
            this.pnlWeekdays.Controls.Add(this.ch2);
            this.pnlWeekdays.Location = new System.Drawing.Point(17, 27);
            this.pnlWeekdays.Name = "pnlWeekdays";
            this.pnlWeekdays.Size = new System.Drawing.Size(246, 43);
            this.pnlWeekdays.TabIndex = 0;
            // 
            // ch5
            // 
            this.ch5.AutoSize = true;
            this.ch5.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch5.Location = new System.Drawing.Point(148, 0);
            this.ch5.Name = "ch5";
            this.ch5.Size = new System.Drawing.Size(17, 31);
            this.ch5.TabIndex = 4;
            this.ch5.Text = "5";
            this.ch5.UseVisualStyleBackColor = true;
            this.ch5.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch6
            // 
            this.ch6.AutoSize = true;
            this.ch6.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch6.Location = new System.Drawing.Point(183, 0);
            this.ch6.Name = "ch6";
            this.ch6.Size = new System.Drawing.Size(17, 31);
            this.ch6.TabIndex = 5;
            this.ch6.Text = "6";
            this.ch6.UseVisualStyleBackColor = true;
            this.ch6.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch7
            // 
            this.ch7.AutoSize = true;
            this.ch7.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch7.Location = new System.Drawing.Point(218, 0);
            this.ch7.Name = "ch7";
            this.ch7.Size = new System.Drawing.Size(17, 31);
            this.ch7.TabIndex = 6;
            this.ch7.Text = "7";
            this.ch7.UseVisualStyleBackColor = true;
            this.ch7.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch4
            // 
            this.ch4.AutoSize = true;
            this.ch4.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch4.Location = new System.Drawing.Point(113, 0);
            this.ch4.Name = "ch4";
            this.ch4.Size = new System.Drawing.Size(17, 31);
            this.ch4.TabIndex = 3;
            this.ch4.Text = "4";
            this.ch4.UseVisualStyleBackColor = true;
            this.ch4.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch1
            // 
            this.ch1.AutoSize = true;
            this.ch1.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch1.Location = new System.Drawing.Point(8, 0);
            this.ch1.Name = "ch1";
            this.ch1.Size = new System.Drawing.Size(17, 31);
            this.ch1.TabIndex = 0;
            this.ch1.Text = "1";
            this.ch1.UseVisualStyleBackColor = true;
            this.ch1.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch3
            // 
            this.ch3.AutoSize = true;
            this.ch3.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch3.Location = new System.Drawing.Point(78, 0);
            this.ch3.Name = "ch3";
            this.ch3.Size = new System.Drawing.Size(17, 31);
            this.ch3.TabIndex = 2;
            this.ch3.Text = "3";
            this.ch3.UseVisualStyleBackColor = true;
            this.ch3.Click += new System.EventHandler(this.ch_Click);
            // 
            // ch2
            // 
            this.ch2.AutoSize = true;
            this.ch2.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ch2.Location = new System.Drawing.Point(43, 0);
            this.ch2.Name = "ch2";
            this.ch2.Size = new System.Drawing.Size(17, 31);
            this.ch2.TabIndex = 1;
            this.ch2.Text = "2";
            this.ch2.UseVisualStyleBackColor = true;
            this.ch2.Click += new System.EventHandler(this.ch_Click);
            // 
            // chDaily
            // 
            this.chDaily.AutoSize = true;
            this.chDaily.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.chDaily.Location = new System.Drawing.Point(300, 27);
            this.chDaily.Name = "chDaily";
            this.chDaily.Size = new System.Drawing.Size(34, 31);
            this.chDaily.TabIndex = 1;
            this.chDaily.Text = "Daily";
            this.chDaily.UseVisualStyleBackColor = true;
            this.chDaily.Click += new System.EventHandler(this.chDaily_Click);
            // 
            // gpValidity
            // 
            this.gpValidity.Controls.Add(this.txtVToTime);
            this.gpValidity.Controls.Add(this.txtVFromTime);
            this.gpValidity.Controls.Add(this.lbValidityTo);
            this.gpValidity.Controls.Add(this.lbValidityFrom);
            this.gpValidity.Controls.Add(this.txtVToDate);
            this.gpValidity.Controls.Add(this.txtVFromDate);
            this.gpValidity.Location = new System.Drawing.Point(21, 260);
            this.gpValidity.Name = "gpValidity";
            this.gpValidity.Size = new System.Drawing.Size(524, 52);
            this.gpValidity.TabIndex = 8;
            this.gpValidity.TabStop = false;
            this.gpValidity.Text = "Validity";
            // 
            // txtVToTime
            // 
            this.txtVToTime.Location = new System.Drawing.Point(329, 19);
            this.txtVToTime.Name = "txtVToTime";
            this.txtVToTime.Size = new System.Drawing.Size(40, 20);
            this.txtVToTime.TabIndex = 5;
            // 
            // txtVFromTime
            // 
            this.txtVFromTime.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtVFromTime.Location = new System.Drawing.Point(137, 19);
            this.txtVFromTime.Name = "txtVFromTime";
            this.txtVFromTime.Size = new System.Drawing.Size(40, 20);
            this.txtVFromTime.TabIndex = 2;
            // 
            // lbValidityTo
            // 
            this.lbValidityTo.AutoSize = true;
            this.lbValidityTo.Location = new System.Drawing.Point(192, 26);
            this.lbValidityTo.Name = "lbValidityTo";
            this.lbValidityTo.Size = new System.Drawing.Size(16, 13);
            this.lbValidityTo.TabIndex = 3;
            this.lbValidityTo.Text = "to";
            // 
            // lbValidityFrom
            // 
            this.lbValidityFrom.AutoSize = true;
            this.lbValidityFrom.Location = new System.Drawing.Point(14, 26);
            this.lbValidityFrom.Name = "lbValidityFrom";
            this.lbValidityFrom.Size = new System.Drawing.Size(27, 13);
            this.lbValidityFrom.TabIndex = 0;
            this.lbValidityFrom.Text = "from";
            // 
            // txtVToDate
            // 
            this.txtVToDate.Location = new System.Drawing.Point(243, 19);
            this.txtVToDate.Name = "txtVToDate";
            this.txtVToDate.Size = new System.Drawing.Size(71, 20);
            this.txtVToDate.TabIndex = 4;
            // 
            // txtVFromDate
            // 
            this.txtVFromDate.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtVFromDate.Location = new System.Drawing.Point(54, 19);
            this.txtVFromDate.Name = "txtVFromDate";
            this.txtVFromDate.Size = new System.Drawing.Size(71, 20);
            this.txtVFromDate.TabIndex = 1;
            // 
            // gpTime
            // 
            this.gpTime.Controls.Add(this.lbTimeTo);
            this.gpTime.Controls.Add(this.lbTimeFrom);
            this.gpTime.Controls.Add(this.txtTimeTo);
            this.gpTime.Controls.Add(this.txtTimeFrom);
            this.gpTime.Location = new System.Drawing.Point(21, 48);
            this.gpTime.Name = "gpTime";
            this.gpTime.Size = new System.Drawing.Size(524, 62);
            this.gpTime.TabIndex = 4;
            this.gpTime.TabStop = false;
            this.gpTime.Text = "Time";
            // 
            // lbTimeTo
            // 
            this.lbTimeTo.AutoSize = true;
            this.lbTimeTo.Location = new System.Drawing.Point(136, 26);
            this.lbTimeTo.Name = "lbTimeTo";
            this.lbTimeTo.Size = new System.Drawing.Size(16, 13);
            this.lbTimeTo.TabIndex = 2;
            this.lbTimeTo.Text = "to";
            // 
            // lbTimeFrom
            // 
            this.lbTimeFrom.AutoSize = true;
            this.lbTimeFrom.Location = new System.Drawing.Point(14, 26);
            this.lbTimeFrom.Name = "lbTimeFrom";
            this.lbTimeFrom.Size = new System.Drawing.Size(27, 13);
            this.lbTimeFrom.TabIndex = 0;
            this.lbTimeFrom.Text = "from";
            // 
            // txtTimeTo
            // 
            this.txtTimeTo.Location = new System.Drawing.Point(187, 23);
            this.txtTimeTo.Name = "txtTimeTo";
            this.txtTimeTo.Size = new System.Drawing.Size(54, 20);
            this.txtTimeTo.TabIndex = 3;
            // 
            // txtTimeFrom
            // 
            this.txtTimeFrom.Location = new System.Drawing.Point(54, 23);
            this.txtTimeFrom.Name = "txtTimeFrom";
            this.txtTimeFrom.Size = new System.Drawing.Size(54, 20);
            this.txtTimeFrom.TabIndex = 1;
            // 
            // frmBlockParkingStand
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(565, 623);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBlockParkingStand";
            this.Text = "Multiple-Blocking";
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.gpPst.ResumeLayout(false);
            this.gpPst.PerformLayout();
            this.gpFrequency.ResumeLayout(false);
            this.gpFrequency.PerformLayout();
            this.pnlWeekdays.ResumeLayout(false);
            this.pnlWeekdays.PerformLayout();
            this.gpValidity.ResumeLayout(false);
            this.gpValidity.PerformLayout();
            this.gpTime.ResumeLayout(false);
            this.gpTime.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel pnlImageCombo;
        private System.Windows.Forms.Label lbBitmap;
        private System.Windows.Forms.Label lbReason;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.GroupBox gpPst;
        private System.Windows.Forms.Label lblPositions;
        private System.Windows.Forms.ListBox lsRight;
        private System.Windows.Forms.ListBox lsLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.GroupBox gpFrequency;
        private System.Windows.Forms.Panel pnlWeekdays;
        private System.Windows.Forms.CheckBox ch5;
        private System.Windows.Forms.CheckBox ch6;
        private System.Windows.Forms.CheckBox ch7;
        private System.Windows.Forms.CheckBox ch4;
        private System.Windows.Forms.CheckBox ch1;
        private System.Windows.Forms.CheckBox ch3;
        private System.Windows.Forms.CheckBox ch2;
        private System.Windows.Forms.CheckBox chDaily;
        private System.Windows.Forms.GroupBox gpValidity;
        private System.Windows.Forms.TextBox txtVToTime;
        private System.Windows.Forms.TextBox txtVFromTime;
        private System.Windows.Forms.Label lbValidityTo;
        private System.Windows.Forms.Label lbValidityFrom;
        private System.Windows.Forms.TextBox txtVToDate;
        private System.Windows.Forms.TextBox txtVFromDate;
        private System.Windows.Forms.GroupBox gpTime;
        private System.Windows.Forms.Label lbTimeTo;
        private System.Windows.Forms.Label lbTimeFrom;
        private System.Windows.Forms.TextBox txtTimeTo;
        private System.Windows.Forms.TextBox txtTimeFrom;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cboMultiBlockName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.ToolTip toolTip;

    }
}