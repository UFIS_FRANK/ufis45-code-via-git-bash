﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _Standard_Bdpsuif_Addon.UserControl
{
    public class MyDataGridView : DataGridView
    {
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.FindForm().AcceptButton.PerformClick();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.FindForm().AcceptButton.PerformClick();
                return true;
            }
            return base.ProcessDataGridViewKey(e);
        }
    }
}
