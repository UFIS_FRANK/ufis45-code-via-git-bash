// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// _IBcComAtlEvents wrapper class

class _IBcComAtlEvents : public COleDispatchDriver
{
public:
	_IBcComAtlEvents() {}		// Calls COleDispatchDriver default constructor
	_IBcComAtlEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_IBcComAtlEvents(const _IBcComAtlEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	// method 'OnLostBc' not emitted because of invalid return type or parameter type
	// method 'OnLostBcEx' not emitted because of invalid return type or parameter type
	// method 'OnBc' not emitted because of invalid return type or parameter type
};
/////////////////////////////////////////////////////////////////////////////
// IBcComAtl wrapper class

class IBcComAtl : public COleDispatchDriver
{
public:
	IBcComAtl() {}		// Calls COleDispatchDriver default constructor
	IBcComAtl(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBcComAtl(const IBcComAtl& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void SetPID(LPCTSTR strAppl, LPCTSTR strPID);
	void SetFilter(LPCTSTR strTable, LPCTSTR strCommand, long bOwnBC, LPCTSTR application);
	void TransmitFilter();
	void StartBroadcasting();
	void StopBroadcasting();
};
