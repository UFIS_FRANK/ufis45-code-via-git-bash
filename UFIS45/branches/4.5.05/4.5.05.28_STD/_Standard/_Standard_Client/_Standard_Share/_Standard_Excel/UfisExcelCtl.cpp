// UfisExcelCtl.cpp : Implementation of the CUfisExcelCtrl ActiveX Control class.

#include "stdafx.h"
#include "UfisExcel.h"
#include "UfisExcelCtl.h"
#include "UfisExcelPpg.h"
#include "Excel9.h"
#include "DTab.h"
#include "CCSBasicFunc.h"
#include "ExcelEventSink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUfisExcelCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisExcelCtrl, COleControl)
	//{{AFX_MSG_MAP(CUfisExcelCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUfisExcelCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUfisExcelCtrl)
	DISP_FUNCTION(CUfisExcelCtrl, "GenerateFromCSV", GenerateFromCSV, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CUfisExcelCtrl, "GenerateFromXSL", GenerateFromXSL, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CUfisExcelCtrl, "GenerateFromTabCtrl", GenerateFromTabCtrl, VT_BOOL, VTS_DISPATCH)
	DISP_FUNCTION(CUfisExcelCtrl, "GenerateFromTab", GenerateFromTab, VT_BOOL, VTS_HANDLE)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CUfisExcelCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUfisExcelCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUfisExcelCtrl)
	EVENT_CUSTOM("NewWorkBook", FireNewWorkBook, VTS_DISPATCH)
	EVENT_CUSTOM("SheetSelectionChange", FireSheetSelectionChange, VTS_DISPATCH  VTS_DISPATCH)
	EVENT_CUSTOM("SheetBeforeDoubleClick", FireSheetBeforeDoubleClick, VTS_DISPATCH  VTS_DISPATCH  VTS_PBOOL)
	EVENT_CUSTOM("SheetBeforeRightClick", FireSheetBeforeRightClick, VTS_DISPATCH  VTS_DISPATCH  VTS_PBOOL)
	EVENT_CUSTOM("SheetActivate", FireSheetActivate, VTS_DISPATCH)
	EVENT_CUSTOM("SheetDeactivate", FireSheetDeactivate, VTS_DISPATCH)
	EVENT_CUSTOM("SheetCalculate", FireSheetCalculate, VTS_DISPATCH)
	EVENT_CUSTOM("SheetChange", FireSheetChange, VTS_DISPATCH  VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookOpen", FireWorkbookOpen, VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookActivate", FireWorkbookActivate, VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookDeactivate", FireWorkbookDeactivate, VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookBeforeClose", FireWorkbookBeforeClose, VTS_DISPATCH  VTS_PBOOL)
	EVENT_CUSTOM("WorkbookBeforeSave", FireWorkbookBeforeSave, VTS_DISPATCH  VTS_BOOL  VTS_PBOOL)
	EVENT_CUSTOM("WorkbookBeforePrint", FireWorkbookBeforePrint, VTS_DISPATCH  VTS_PBOOL)
	EVENT_CUSTOM("WorkbookNewSheet", FireWorkbookNewSheet, VTS_DISPATCH  VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookAddinInstall", FireWorkbookAddinInstall, VTS_DISPATCH)
	EVENT_CUSTOM("WorkbookAddinUninstall", FireWorkbookAddinUninstall, VTS_DISPATCH)
	EVENT_CUSTOM("WindowResize", FireWindowResize, VTS_DISPATCH  VTS_DISPATCH)
	EVENT_CUSTOM("WindowActivate", FireWindowActivate, VTS_DISPATCH  VTS_DISPATCH)
	EVENT_CUSTOM("WindowDeactivate", FireWindowDeactivate, VTS_DISPATCH  VTS_DISPATCH)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUfisExcelCtrl, 1)
	PROPPAGEID(CUfisExcelPropPage::guid)
END_PROPPAGEIDS(CUfisExcelCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisExcelCtrl, "UFISEXCEL.UfisExcelCtrl.1",
	0xd8a28baa, 0x524f, 0x4098, 0x95, 0x58, 0xcd, 0xcc, 0x53, 0xc4, 0x83, 0x4)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUfisExcelCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUfisExcel =
		{ 0xc2069371, 0x249, 0x46c9, { 0x9c, 0xa5, 0, 0x2b, 0xa, 0xd8, 0x75, 0x3 } };
const IID BASED_CODE IID_DUfisExcelEvents =
		{ 0x51326833, 0x71ea, 0x4e8d, { 0x87, 0x17, 0xc8, 0xd1, 0x40, 0x9, 0xea, 0xd1 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUfisExcelOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUfisExcelCtrl, IDS_UFISEXCEL, _dwUfisExcelOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::CUfisExcelCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisExcelCtrl

BOOL CUfisExcelCtrl::CUfisExcelCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UFISEXCEL,
			IDB_UFISEXCEL,
			afxRegApartmentThreading,
			_dwUfisExcelOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::CUfisExcelCtrl - Constructor

CUfisExcelCtrl::CUfisExcelCtrl()
{
	InitializeIIDs(&IID_DUfisExcel, &IID_DUfisExcelEvents);

	// TODO: Initialize your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::~CUfisExcelCtrl - Destructor

CUfisExcelCtrl::~CUfisExcelCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::OnDraw - Drawing function

void CUfisExcelCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::DoPropExchange - Persistence support

void CUfisExcelCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::OnResetState - Reset control to default state

void CUfisExcelCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl::AboutBox - Display an "About" box to the user

void CUfisExcelCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UFISEXCEL);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl message handlers

BOOL CUfisExcelCtrl::GenerateFromCSV(LPCTSTR FileName) 
{
	// TODO: Add your dispatch handler code here
	try
	{
		_Application olExcel;
		if (olExcel.CreateDispatch("Excel.Application"))
		{
			CExcelEventSink olEventSink(this);
			olEventSink.Advise(olExcel.m_lpDispatch,__uuidof(Excel::AppEvents));

			olExcel.SetVisible(TRUE);
			olExcel.SetWindowState(0xFFFFEFD1);

			LPDISPATCH pIDispatch = olExcel.GetWorkbooks();
			Workbooks olWorkbooks;
			olWorkbooks.AttachDispatch(pIDispatch);

			COleVariant blUpdateLinks((short)0);

			COleVariant blReadOnly((short)FALSE);
			blReadOnly.ChangeType(VT_BOOL);

			COleVariant ilFormat((short)4);	

			COleVariant olPassword;
			olPassword.ChangeType(VT_NULL);

			COleVariant olWriteResPassword;
			olWriteResPassword.ChangeType(VT_NULL);

			COleVariant	blIgnoreReadOnlyRecommended((short)TRUE);
			blIgnoreReadOnlyRecommended.ChangeType(VT_BOOL);

			COleVariant	olOrigin((short)2);

			COleVariant	olDelimiter;
			olDelimiter.ChangeType(VT_NULL);

			COleVariant	blEditable((short)TRUE);
			blEditable.ChangeType(VT_BOOL);

			COleVariant	blNotify((short)TRUE);
			blNotify.ChangeType(VT_BOOL);

			COleVariant	olConverter((short)0);

			COleVariant	blAddToMru((short)TRUE);
			blAddToMru.ChangeType(VT_BOOL);
			
//			olWorkbooks.Open(FileName,blUpdateLinks,blReadOnly,ilFormat,olPassword,olWriteResPassword,blIgnoreReadOnlyRecommended,olOrigin,olDelimiter,blEditable,blNotify,olConverter,blAddToMru);
			COleVariant olMissing;
			olMissing.ChangeType(VT_NULL);
			olWorkbooks.Open(FileName,olMissing,olMissing,ilFormat,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing);

			olEventSink.Unadvise(__uuidof(Excel::AppEvents));

		}
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CUfisExcelCtrl::GenerateFromXSL(LPCTSTR FileName) 
{
	// TODO: Add your dispatch handler code here
	try
	{
		_Application olExcel;
		if (olExcel.CreateDispatch("Excel.Application"))
		{
			CExcelEventSink olEventSink(this);
			olEventSink.Advise(olExcel.m_lpDispatch,__uuidof(Excel::AppEvents));

			olExcel.SetVisible(TRUE);
			olExcel.SetWindowState(0xFFFFEFD1);

			LPDISPATCH pIDispatch = olExcel.GetWorkbooks();
			Workbooks olWorkbooks;
			olWorkbooks.AttachDispatch(pIDispatch);

			COleVariant blUpdateLinks((short)0);

			COleVariant blReadOnly((short)FALSE);
			blReadOnly.ChangeType(VT_BOOL);

			COleVariant ilFormat((short)0);	

			COleVariant olPassword;
			olPassword.ChangeType(VT_NULL);

			COleVariant olWriteResPassword;
			olWriteResPassword.ChangeType(VT_NULL);

			COleVariant	blIgnoreReadOnlyRecommended((short)TRUE);
			blIgnoreReadOnlyRecommended.ChangeType(VT_BOOL);

			COleVariant	olOrigin((short)2);

			COleVariant	olDelimiter;
			olDelimiter.ChangeType(VT_NULL);

			COleVariant	blEditable((short)TRUE);
			blEditable.ChangeType(VT_BOOL);

			COleVariant	blNotify((short)TRUE);
			blNotify.ChangeType(VT_BOOL);

			COleVariant	olConverter((short)0);

			COleVariant	blAddToMru((short)TRUE);
			blAddToMru.ChangeType(VT_BOOL);
			
//			olWorkbooks.Open(FileName,blUpdateLinks,blReadOnly,ilFormat,olPassword,olWriteResPassword,blIgnoreReadOnlyRecommended,olOrigin,olDelimiter,blEditable,blNotify,olConverter,blAddToMru);
			COleVariant olMissing;
			olMissing.ChangeType(VT_NULL);
			olWorkbooks.Open(FileName,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing,olMissing);

			olEventSink.Unadvise(__uuidof(Excel::AppEvents));

		}
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CUfisExcelCtrl::GenerateFromTabCtrl(LPDISPATCH ITabCtrl) 
{
	// TODO: Add your dispatch handler code here
	_DTAB myTab;
	try
	{
		myTab.AttachDispatch(ITabCtrl,FALSE);

		_Application olExcel;
		if (olExcel.CreateDispatch("Excel.Application"))
		{
			CExcelEventSink olEventSink(this);
			olEventSink.Advise(olExcel.m_lpDispatch,__uuidof(Excel::AppEvents));

			olExcel.SetVisible(TRUE);
			olExcel.SetWindowState(0xFFFFEFD1);

			LPDISPATCH pIDispatch = olExcel.GetWorkbooks();
			Workbooks olWorkbooks;
			olWorkbooks.AttachDispatch(pIDispatch);

			COleVariant olTemplate;
			olTemplate.ChangeType(VT_NULL);
			pIDispatch = olWorkbooks.Add(olTemplate);
			_Workbook olWorkbook;
			olWorkbook.AttachDispatch(pIDispatch);

			pIDispatch = olWorkbook.GetActiveSheet();
			_Worksheet olWorksheet;
			olWorksheet.AttachDispatch(pIDispatch);

			CString olName = myTab.GetMyName();
			if (olName.GetLength() > 0)
			{
				olWorksheet.SetName(myTab.GetMyName());
			}

			pIDispatch = olWorksheet.GetCells();
			Range olCells;
			olCells.AttachDispatch(pIDispatch);
			COleVariant olFormat("@");
			olCells.SetNumberFormat(olFormat);

			CString olHeaderString = myTab.GetHeaderString();
			CStringArray olFields;
			::ExtractItemList(olHeaderString,&olFields);

			COleVariant olRowIndex((long)1);
			for (int i = 0; i < olFields.GetSize(); i++)
			{
				COleVariant olColumnIndex((long)(i+1));
				COleVariant olValue(olFields[i]);
				olCells.SetItem(olRowIndex,olColumnIndex,olValue);
			}
		

			CString olFieldsString = myTab.GetLogicalFieldList();
			olFields.RemoveAll();
			::ExtractItemList(olFieldsString,&olFields);
			
			for (i = 0; i < myTab.GetLineCount(); i++)
			{
				for (int j = 0; j < myTab.GetColumnCount(); j++)
				{
					CString olValue = myTab.GetFieldValue(i,olFields[j]);
					
					COleVariant olRowIndex((long)(i+2));
					COleVariant olColumnIndex((long)(j+1));
					COleVariant olCellValue(olValue);
					olCells.SetItem(olRowIndex,olColumnIndex,olCellValue);
				}
		
			}

			pIDispatch = olWorksheet.GetColumns();
			Range olColumns;
			olColumns.AttachDispatch(pIDispatch);

			olColumns.AutoFit();

			olWorkbook.Save();

			olEventSink.Unadvise(__uuidof(Excel::AppEvents));

		}

		myTab.DetachDispatch();
	}
	catch(...)
	{
		myTab.DetachDispatch();
		return FALSE;
	}
	
	return TRUE;
}



BOOL CUfisExcelCtrl::GenerateFromTab(OLE_HANDLE HTabCtrl) 
{
	return TRUE;
}
