#if !defined(AFX_UFISEXCEL_H__38035CA9_E298_4199_9CC8_A8E4F35D2E22__INCLUDED_)
#define AFX_UFISEXCEL_H__38035CA9_E298_4199_9CC8_A8E4F35D2E22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisExcel.h : main header file for UFISEXCEL.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelApp : See UfisExcel.cpp for implementation.

class CUfisExcelApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCEL_H__38035CA9_E298_4199_9CC8_A8E4F35D2E22__INCLUDED)
