// lb_containerDlg.h : header file
//

#if !defined(AFX_LB_CONTAINERDLG_H__8942EEE0_9796_11D5_9970_0000865098D4__INCLUDED_)
#define AFX_LB_CONTAINERDLG_H__8942EEE0_9796_11D5_9970_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ufiscom.h>
#include <bcproxy.h>
#include <amp_test.h>
class CLb_containerDlgAutoProxy;

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlg dialog

class CLb_containerDlg : public CDialog
{
	DECLARE_DYNAMIC(CLb_containerDlg);
	friend class CLb_containerDlgAutoProxy;

// Construction
public:
	CLb_containerDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CLb_containerDlg();

// Dialog Data
	//{{AFX_DATA(CLb_containerDlg)
	enum { IDD = IDD_LB_CONTAINER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLb_containerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool StartUfisCom();
	bool StartBc();
	int m_iMaxTime;
	bool something_wrong;
	CLb_containerDlgAutoProxy* m_pAutoProxy;
	CUfisCom* pUfisCom;
	CAmp_test* m_pAmpel;
	_IBcPrxyEvents omBcPrxyEvents;
	IDispatch* pDispBcPrxyEvents;
	IDispatch* pDispUfisCom;
	HWND m_hWnd;
	
	
	
	HICON m_hIcon;

	BOOL CanExit();

	// Generated message map functions
	//{{AFX_MSG(CLb_containerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnTlc();
	afx_msg void OntimeAmp(long ltime);
	afx_msg void OnPing();
	afx_msg void OnRed();
	afx_msg void OnRelease();
	afx_msg void OnYellow();
	afx_msg void OnGreen();
	afx_msg void OnProp();
	afx_msg void OnAbout();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LB_CONTAINERDLG_H__8942EEE0_9796_11D5_9970_0000865098D4__INCLUDED_)
