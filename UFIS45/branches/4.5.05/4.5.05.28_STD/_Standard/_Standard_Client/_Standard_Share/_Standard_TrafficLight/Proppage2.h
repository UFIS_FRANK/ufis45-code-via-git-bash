#if !defined(AFX_PROPPAGE2_H__AFBD4FA6_AC03_11D5_BFF8_0004769155FB__INCLUDED_)
#define AFX_PROPPAGE2_H__AFBD4FA6_AC03_11D5_BFF8_0004769155FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Proppage2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProppage2 : Property page dialog

class CProppage2 : public COlePropertyPage
{
	DECLARE_DYNCREATE(CProppage2)
	DECLARE_OLECREATE_EX(CProppage2)

// Constructors
public:
	CProppage2();

// Dialog Data
	//{{AFX_DATA(CProppage2)
	enum { IDD = IDD_PROPPAGE_2 };
	int		m_iRadio1;
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);        // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CProppage2)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPPAGE2_H__AFBD4FA6_AC03_11D5_BFF8_0004769155FB__INCLUDED_)
