//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TestContainer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDS_ABOUTBOX                    101
#define IDD_LB_CONTAINER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDC_TLC                         1001
#define IDC_TIME                        1003
#define IDC_PING                        1005
#define IDC_MAX_TIME                    1006
#define IDC_RED                         1007
#define IDC_YELLOW                      1008
#define IDC_GREEN                       1009
#define IDC_RELEASE                     1010
#define IDC_PROP                        1011
#define IDC_ABOUT                       1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
