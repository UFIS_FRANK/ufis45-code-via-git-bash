// Amp_testPpg.cpp : Implementation of the CAmp_testPropPage property page class.

#include <stdafx.h>
#include <TrafficLight.h>
#include <Amp_testPpg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAmp_testPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAmp_testPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CAmp_testPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAmp_testPropPage, "AMPTEST.AmptestPropPage.1",
	0x59bec159, 0x923c, 0x11d5, 0x99, 0x69, 0, 0, 0x86, 0x50, 0x98, 0xd4)


/////////////////////////////////////////////////////////////////////////////
// CAmp_testPropPage::CAmp_testPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAmp_testPropPage

BOOL CAmp_testPropPage::CAmp_testPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AMP_TEST_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testPropPage::CAmp_testPropPage - Constructor

CAmp_testPropPage::CAmp_testPropPage() :
	COlePropertyPage(IDD, IDS_AMP_TEST_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CAmp_testPropPage)
	m_iTimeRed = 500;
	m_iTimeYellow = 50;
	m_bNotify = FALSE;
	m_Interval = 10;
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testPropPage::DoDataExchange - Moves data between page and properties

void CAmp_testPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CAmp_testPropPage)
	DDP_Text(pDX, IDC_TIME_RED, m_iTimeRed, _T("RTime") );
	DDX_Text(pDX, IDC_TIME_RED, m_iTimeRed);
	DDV_MinMaxInt(pDX, m_iTimeRed, 3, 9000);
	DDP_Text(pDX, IDC_TIME_YELLOW, m_iTimeYellow, _T("YTime") );
	DDX_Text(pDX, IDC_TIME_YELLOW, m_iTimeYellow);
	DDV_MinMaxInt(pDX, m_iTimeYellow, 2, 8000);
	DDP_Check(pDX, IDC_CHECK_MESSAGE, m_bNotify, _T("Note") );
	DDX_Check(pDX, IDC_CHECK_MESSAGE, m_bNotify);
	DDP_Text(pDX, IDC_INTERVAL, m_Interval, _T("Interval") );
	DDX_Text(pDX, IDC_INTERVAL, m_Interval);
	DDV_MinMaxLong(pDX, m_Interval, 10, 3600);
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testPropPage message handlers

void CAmp_testPropPage::OnDefaults()
{

}
