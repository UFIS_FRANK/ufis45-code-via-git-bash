#if !defined(AFX_AMP_TEST_H__59BEC15E_923C_11D5_9969_0000865098D4__INCLUDED_)
#define AFX_AMP_TEST_H__59BEC15E_923C_11D5_9969_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// amp_test.h : main header file for AMP_TEST.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include <resource.h>       // main symbols
#include <Amp_test_i.h>

/////////////////////////////////////////////////////////////////////////////
// CAmp_testApp : See amp_test.cpp for implementation.

class CAmp_testApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
private:
	BOOL InitATL();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMP_TEST_H__59BEC15E_923C_11D5_9969_0000865098D4__INCLUDED)
