// AlerterDlg.h : header file
//
//{{AFX_INCLUDES()
#include "aatlogin.h"
#include "trafficlight.h"
#include "tab.h"
#include "ufiscom.h"
//}}AFX_INCLUDES
#include <bcproxy.h>
#include <STabCtrl.h>
#include <DlgResizeHelper.h>

#if !defined(AFX_ALERTERDLG_H__409F0E5A_5608_11D7_8009_00010215BFDE__INCLUDED_)
#define AFX_ALERTERDLG_H__409F0E5A_5608_11D7_8009_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAlerterDlgAutoProxy;

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlg dialog

class CAlerterDlg : public CDialog
{
	DECLARE_DYNAMIC(CAlerterDlg);
	friend class CAlerterDlgAutoProxy;

// Construction
public:
	CAlerterDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CAlerterDlg();

// Dialog Data
	//{{AFX_DATA(CAlerterDlg)
	enum { IDD = IDD_ALERTER_DIALOG };
	CStatic	m_DetailHeader;
	CAatLogin			m_LoginControl;
	CTAB				m_DetailList;
	CTAB				m_OverviewList;
	CUfisCom			m_UfisComControl;
	CSTabCtrl			m_Pages;
	int					m_TimeFormat;
	CTrafficLight		m_TrafficLightControl;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAlerterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CAlerterDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();
	bool StartBc();
	bool InitTabs();
	bool FillTabs();

	// Generated message map functions
	//{{AFX_MSG(CAlerterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnHelp();
	afx_msg void OnConfirm();
	afx_msg void OnReopen();
	afx_msg void OnRadioLocal();
	afx_msg void OnRadioUcs();
	afx_msg void OnCloseStatus();
	afx_msg void OnMenuConfirm();
	afx_msg void OnMenuClose();
	afx_msg void OnMenuReopen();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnActivate( UINT nState, CWnd* pWndOther, BOOL bMinimized );
	afx_msg void OnSelchangingTabPages(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeTabPages(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG

	afx_msg BOOL OnLoginResult(const char *pszResult);
	afx_msg void OnRowSelectionChangedTabctrl1(long LineNo, BOOL Selected);
	afx_msg BOOL OnRowSelectionChanged(UINT nID,long LineNo, BOOL Selected); 
	afx_msg void OnSendRButtonClickTabctrl1(long LineNo, long ColNo);
	afx_msg BOOL OnSendRButtonClick(UINT nID,long LineNo, long ColNo); 
	afx_msg void OnOnHScroll(UINT nID,long ColNo);

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CAlerterDlg)
		afx_msg BOOL OnBroadcast(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	DECLARE_MESSAGE_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CString	omHostName;
	CString	omHomeAirport;
	CString	omTableExtension;
	BOOL	bmLoggedIn;
	_IBcPrxyEvents omBcPrxyEvents;
	IDispatch* pDispBcPrxyEvents;
	IDispatch* pDispUfisCom;
	DWORD m_dwCookie;// used for AfxConnectionAdvise(..)
	IUnknown* pIUnknown_Auto;
	CTime	omLoginTime;
	CTime	omCurrTime;
	int		imTimeSpan;
	int		imLoadTime;
	int     imTabText;

	CTAB*	pomSelTab;
	long	lmSelLine;
	long	lmSelColumn;
	UINT	imTimer;

	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	CStringArray	omTabNames;
	DlgResizeHelper m_resizeHelper;

	BOOL	bmDetailListIndexesOK;

private:
	typedef struct ERROR_PAGE
	{
		UINT		m_HeaderId;
		CStatic		m_DetailHeader;
		CTAB		m_DetailList;
		UINT		m_ItemId;
		long		m_OpenItems;
		long		m_ConfirmedItems;
		long		m_ClosedItems;

		ERROR_PAGE()
		{
			m_HeaderId  = -1;
			m_ItemId	= -1;
			m_OpenItems = 0;
			m_ConfirmedItems = 0;
			m_ClosedItems = 0;
		}

	}	ERROR_PAGE;

private:
	ERROR_PAGE*	FindErrorPage(const CString& ropIntr);
	long		FindLine(CTAB& ropTab,const CString& ropUrno);
	CString		GetConnectionState(const CString& ropCons);
	CString		GetErrorState(const CString& ropStat);
	bool		CreateOrUpdateDetailLine(long lpLine,ERROR_PAGE *popPage = NULL,bool bpRefresh = false,bool bpCreate=false);
	bool		CreateOrUpdateOverviewLine(long lpLine,bool bpRefresh = false,bool bpCreate=false);
	CString		GetUrnoFromSelection(CString opSelection);
	bool		ChangeStatus(const CString& ropOldStatus,const CString& ropOldStatus2,const CString& ropNewStatus);
	bool		ChangeStatus(CTAB *popTab,long lpLineno,long lpColNo,const CString& ropOldStatus,const CString& ropOldStatus2,const CString& ropNewStatus);
	bool		WriteStatusChanges(long lpLine,const CString& ropStatus);
	void		GetItemInfo(const CString& ropIntr,long& rlpOpenItems,long& rlpConfirmedItems,long& rlpClosedItems);

	void		PlaySound();

	BOOL		SetLocalDiff();
	void		LocalToUtc(CTime& ropTime);
	void		UtcToLocal(CTime& ropTime);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALERTERDLG_H__409F0E5A_5608_11D7_8009_00010215BFDE__INCLUDED_)
