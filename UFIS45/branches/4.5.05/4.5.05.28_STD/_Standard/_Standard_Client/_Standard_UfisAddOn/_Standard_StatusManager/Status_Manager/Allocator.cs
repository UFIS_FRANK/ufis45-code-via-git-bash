using System;
using System.Collections;

namespace Ufis.Status_Manager.UI
{
	public class TimeFrame
	{
		public DateTime start;
		public DateTime end;
		public TimeFrame()
		{
		}
	}

	public class GanttLine
	{
		public DateTime datStart;
		public DateTime datEnd;
		public ArrayList entries = new ArrayList(100);
		public GanttLine()
		{
		}
		public void Add(TimeFrame pTF)
		{
			entries.Add(pTF);
		}
	}
	/// <summary>
	/// Summary description for Allocator.
	/// </summary>
	public class Allocator
	{
		ArrayList Lines = new ArrayList(200);
		public Allocator(int pLines, DateTime TimeframeFrom, DateTime TimeframeTo)
		{
			for(int i = 0; i < pLines; i++)
			{
				GanttLine line = new GanttLine();
				line.datStart = TimeframeFrom;
				line.datEnd = TimeframeTo;
				TimeFrame first = new TimeFrame();
				TimeFrame last = new TimeFrame();
				first.start = line.datStart.Subtract(new TimeSpan(0, 1, 0, 0, 0));
				first.end = line.datStart;
				last.start = line.datEnd;
				last.end = line.datEnd.AddHours(1);
				line.Add(first);
				line.Add(last);
				Lines.Add(line);
				
			}
		}
		public void Clear()
		{
			for(int i = 0; i < Lines.Count; i++)
			{
				GanttLine myLine = (GanttLine)Lines[i];
				for(int j = 0; j < myLine.entries.Count; j++)
				{
					myLine.entries.Clear();
				}
			}
			Lines.Clear();
		}
		public int GetLineForTimeframe(DateTime TimeframeFrom, DateTime TimeframeTo)
		{
			int ilRet = -1;
			bool blFound = false; 
			for(int i = 0; (i < Lines.Count && blFound == false); i++)
			{
				GanttLine myLine = (GanttLine)Lines[i];
				for(int j = 0; (j < myLine.entries.Count-1 && blFound == false); j++)
				{
					TimeFrame tfF = (TimeFrame)myLine.entries[j];
					TimeFrame tfT = (TimeFrame)myLine.entries[j+1];
					if(TimeframeFrom >= tfF.end && TimeframeTo <= tfT.start)
					{
						TimeFrame tfNew = new TimeFrame();
						tfNew.start = TimeframeFrom;
						tfNew.end = TimeframeTo;
						myLine.entries.Insert(j+1, tfNew);
						ilRet = i;
						blFound = true;
					}
				}
			}
			return ilRet;
		}
	}
}
