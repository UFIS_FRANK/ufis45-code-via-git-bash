﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntObjectGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EntObjectType Type { get; set; }
        public Color SymbolColor { get; set; }
        public int OptNumbers { get; set; }

        public EntObjectGroup(): this(0, null, null, Color.White, 1)
        {
        }

        public EntObjectGroup(int id, string name, EntObjectType type, Color color,
            int optNumbers)
        {
            this.Id = id;
            this.Name = name;
            this.Type = type;
            this.SymbolColor = color;
            this.OptNumbers = optNumbers;
        }

        public override string ToString()
        {
            return base.ToString() + ";" + this.Id.ToString();
        }
    }

    public class EntObjectGroupCollection : List<EntObjectGroup>
    {
        public EntObjectGroup FindByName(string name)
        {
            foreach (EntObjectGroup objGroup in this)
            {
                if (objGroup.Name == name)
                    return objGroup;
            }
            return null;
        }

        public void GenerateObjectGroups(EntObjectTypeCollection types)
        {
            GenerateObjectGroups(types, null, null);
        }

        public void GenerateObjectGroups(EntObjectTypeCollection types, 
            string[] staffTypes, string[] equTypes)
        {
            Color[] colors = GetColorList();

            if (staffTypes == null)
                staffTypes = new string[] 
                    { "RLO", "FC", "FDM" };

            if (equTypes == null)
                equTypes = new string[]
                    {"JCPL",
                    "Transporter",
                    "Tractor",
                    "Skyloader"};

            int groupId = 0;
            Random random = new Random();
            int optNumbers = 1;

            //staff
            foreach (string staffType in staffTypes)
            {
                optNumbers = (groupId <= 1 ? 30 : 2);
                this.Add(new EntObjectGroup(groupId, staffType, types[0], colors[groupId], optNumbers));

                groupId++;
            }

            //equipment
            foreach (string equType in equTypes)
            {
                Color color;
                if (groupId < colors.Length)
                {
                    color = colors[groupId];
                }
                else
                {
                    color = Color.FromArgb(random.Next(0, 214), random.Next(0, 214), random.Next(0, 214));                    
                }

                optNumbers = (groupId <= 4 ? 2 : 1);
                this.Add(new EntObjectGroup(groupId, equType, types[1], color, optNumbers));

                groupId++;
            }
        }

        private Color[] GetColorList()
        {
            List<Color> colors = new List<Color>();

            KnownColor[] knownColors = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            int colorsCount = knownColors.Length;
            int j = Convert.ToInt32(colorsCount / 2);

            for (int i = 0; i < j; i++)
            {
                AddColor(colors, knownColors[i]);
                AddColor(colors, knownColors[colorsCount - 1 - i]);
            }

            if (colorsCount % 2 != 0)
            {
                AddColor(colors, knownColors[j]);
            }

            return colors.ToArray();
        }

        private void AddColor(List<Color> colors, KnownColor knownColor)
        {

            Color color = Color.FromKnownColor(knownColor);
            if ((!color.IsSystemColor) && (!IsLightColor(color)))
                colors.Add(color);
        }

        private bool IsLightColor(Color color)
        {
            return (color.R >= 215 && color.G >= 215 && color.B >= 215);
        }

        public EntObjectGroupCollection(): this(false, null)
        {
        }

        public EntObjectGroupCollection(bool autoGenerate, EntObjectTypeCollection types)
        {
            GenerateObjectGroups(types, null, null);
        }

        public EntObjectGroupCollection(bool autoGenerate, EntObjectTypeCollection types,
            string[] staffTypes, string[] equTypes)
        {
            if (autoGenerate)
            {
                if (types == null)
                {
                    types = new EntObjectTypeCollection(true);
                }
                GenerateObjectGroups(types, staffTypes, equTypes);
            }
        }
    }


}
