namespace Ufis.Status_Manager.UI
{
    partial class UCJobTemplate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbJobTemplate = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbJobTemplate
            // 
            this.lbJobTemplate.FormattingEnabled = true;
            this.lbJobTemplate.Location = new System.Drawing.Point(6, 19);
            this.lbJobTemplate.Name = "lbJobTemplate";
            this.lbJobTemplate.Size = new System.Drawing.Size(216, 238);
            this.lbJobTemplate.TabIndex = 0;
            this.lbJobTemplate.SelectedIndexChanged += new System.EventHandler(this.lbJobTemplate_SelectedIndexChanged);
            this.lbJobTemplate.DoubleClick += new System.EventHandler(this.lbJobTemplate_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbJobTemplate);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 264);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Job Template";
            // 
            // UCJobTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCJobTemplate";
            this.Size = new System.Drawing.Size(234, 270);
            this.Load += new System.EventHandler(this.UCJobTemplate_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbJobTemplate;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
