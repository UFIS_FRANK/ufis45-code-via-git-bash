using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;
using System.Collections;

using Ufis.Utils;
using Ufis.Data;

using Ufis.Status_Manager.DS;

namespace Ufis.Status_Manager.Ctrl
{
    public class CtrlCfl : IDisposable
    {
        private static CtrlCfl _this = null;
        private static DSFlightCflStat _dsFlightCfl = null;
        //private static int TimerIntervalConflicts = 1;
        private static IDatabase myDB = null;
        private static ITable myOST = null;
        private static ITable myAFT = null;
        private static ITable mySDE = null;
        private static ITable myALT = null;
        private static ITable myNAT = null;
        private static ITable myHSS = null;
        private static ITable mySRH = null;

        public static string CurrentViewName
        {
            get { return CFC.CurrentViewName; }
        }

        public static int CurrentViewIdx
        {
            get { return CFC.CurrentViewIdx; }
        }

        public static DSFlightCflStat MyDsFlightCfl
        {
            get
            {
                if (_dsFlightCfl == null)
                {
                    _dsFlightCfl = new DSFlightCflStat();
                }
                return CtrlCfl._dsFlightCfl;
            }
            set
            {
                _dsFlightCfl = value;
            }
        }

        //private DE.OST_Changed delegateOstChanged = null;
        private DE.UpdateFlightData delegateUpdateFlightData = null;
        private DE.OST_ChangedForAFlight delegateOstChangedForAFlight = null;
        
        private bool _disposed = false;

        private CtrlCfl()
        {
            Init();
        }


        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!_disposed)
                {
                    UT.LogMsg("CtrlCfl: Closing");
                    //if(delegateOstChanged!=null) DE.OnOST_Changed -= delegateOstChanged;
                    if(delegateUpdateFlightData!=null) DE.OnUpdateFlightData -= delegateUpdateFlightData;
                    if (delegateOstChangedForAFlight != null) DE.OnOST_ChangedForAFlight -= delegateOstChangedForAFlight;
                    UT.LogMsg("CtrlCfl: Closed");
                    _disposed = true;
                }
            }
        }

        public static CtrlCfl GetInstance()
        {
            if (_this == null) _this = new CtrlCfl();
            return _this;
        }

        private void DE_OnOST_Changed(object sender, string strOstUrno, State state)
        {
            string strAftUrno = "";
            try
            {
                UT.LogMsg("CtrlCfl:DE_OnOST_Changed: b4 OST acq Reader Lock");
                myOST.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                UT.LogMsg("CtrlCfl:DE_OnOST_Changed: af OST acq Reader Lock");
                IRow[] ostRows = myOST.RowsByIndexValue("URNO", strOstUrno);
                if (ostRows.Length > 0)
                {
                    strAftUrno = ostRows[0]["UAFT"].Trim();
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "");
            }
            finally
            {
                UT.LogMsg("CtrlCfl:DE_OnOST_Changed: b4 OST rel Reader Lock");
                if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock();
                UT.LogMsg("CtrlCfl:DE_OnOST_Changed: af OST rel Reader Lock");
            }

            try
            {
                UpdateStatusConflictCount(strAftUrno);
                UpdateStatusConflictInfo(strAftUrno);

            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    DE.Call_FlightStatus_Changed(this, strAftUrno);
                }
                catch (Exception)
                {
                }
            }
        }

        private static ReaderWriterLock _rwLock = new ReaderWriterLock();
        private ReaderWriterLock Lock
        {
            get
            {
                if (_rwLock == null)
                {
                    _rwLock = new ReaderWriterLock();
                    _rwLock.ReleaseLock();
                }
                return _rwLock;
            }
        }

        private static bool _hasWriterLockAcquring = false;

        private void GetReaderLock()
        {
            for (int i = 0; i < 20; i++)
            {
                if (!_hasWriterLockAcquring) break;
                Thread.Sleep(100);
            }
            Lock.AcquireReaderLock(Timeout.Infinite);
        }

        private void ReleaseReaderLock()
        {
            if (Lock.IsReaderLockHeld) Lock.ReleaseReaderLock();
        }

        private void GetWriterLock()
        {
            Lock.AcquireWriterLock(Timeout.Infinite);
        }

        private void ReleaseWriterLock()
        {
            _hasWriterLockAcquring = true;
            if (Lock.IsWriterLockHeld) Lock.ReleaseWriterLock();
            _hasWriterLockAcquring = false;
        }

        public void GetConflictStatus( string strAftUrno, int maxNoOfCntToReturn, string seperator, 
            ref string lastStat, ref string allCfl )
        {
            lastStat = "";
            allCfl = "";

            if ((strAftUrno != null) && (strAftUrno != ""))
            {
                DSFlightCflStat.TBL_CFL_STATRow[] rows = (DSFlightCflStat.TBL_CFL_STATRow[])MyDsFlightCfl.TBL_CFL_STAT.Select("UAFT='" + strAftUrno + "'");
                 if (rows.Length == 0)
                {//Not in current info. Try to update it
                    UpdateStatusConflictInfo(strAftUrno);
                    rows = (DSFlightCflStat.TBL_CFL_STATRow[])MyDsFlightCfl.TBL_CFL_STAT.Select("UAFT='" + strAftUrno + "'");
                }


                if (rows.Length > 0)
                {
                    lastStat = rows[0].LAST_STAT;
                    allCfl = "";

                    String[] del = { CFL_SEPERATOR };
                    string[] stAllStat = rows[0].ALL_STAT.Split(del, StringSplitOptions.None);
                    int cnt = stAllStat.Length;

                    for (int i = 0; i < cnt; i++)
                    {
                        if (i < maxNoOfCntToReturn)
                        {
                            allCfl += (string)stAllStat[i] + seperator;
                        }
                        else
                        {
                            allCfl += " more conflicts";
                            break;
                        }
                    }
                }
            }
            return;
        }

        public string GetConflictStatus( string strAftUrno, int maxNoOfCntToReturn, string seperator )
        {
            string lastStat = "";
            string allCfl = "";
            GetConflictStatus(strAftUrno, maxNoOfCntToReturn, seperator, ref lastStat, ref allCfl);

            return allCfl;
        }

        public string GetLastStatus(string strAftUrno)
        {
            string lastStat = "";
            string allCfl = "";
            GetConflictStatus(strAftUrno, 5, "\r\n", ref lastStat, ref allCfl);

            return lastStat;
        }

        public bool GetConflictCount(string strAftUrno,
            ref int cflCount, ref int ackCount, ref int totalCount,
            ref int ostCount)
        {
            bool inView = false;
            return HasConflict(strAftUrno,
                ref cflCount, ref ackCount, ref totalCount, ref ostCount, ref inView);
        }

        public bool GetConflictCount(string strAftUrno,
            ref int cflCount, ref int ackCount, ref int totalCount,
            ref int ostCount,ref bool inView)
        {

            return HasConflict(strAftUrno,
                ref cflCount, ref ackCount, ref totalCount, ref ostCount, ref inView);
        }

        public bool HasConflict(string strAftUrno,
            ref int cflCount, ref int ackCount, ref int totalCount,
            ref int ostCount, ref bool inView)
        {
            bool hasCfl = false;
            DSFlightCflStat ds = null;
            try
            {
                GetReaderLock();
                //ds = (DSFlightCflStat) MyDsFlightCfl.Copy();
                ds = MyDsFlightCfl;
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, ex.Message);
            }
            finally
            {
                ReleaseReaderLock();
            }
            try
            {
                cflCount = 0;
                ackCount = 0;
                totalCount = 0;
                ostCount = 0;

                DSFlightCflStat.TBL_CFLRow[] rows = (DSFlightCflStat.TBL_CFLRow[])ds.TBL_CFL.Select("UAFT='" + strAftUrno + "'");
                if (rows.Length == 0)
                {//Not in current info. Try to update it
                    UpdateStatusConflictCount(strAftUrno);
                    rows = (DSFlightCflStat.TBL_CFLRow[])MyDsFlightCfl.TBL_CFL.Select("UAFT='" + strAftUrno + "'");
                }

                if (rows.Length == 1)
                {
                    DSFlightCflStat.TBL_CFLRow row = rows[0];
                    cflCount = row.NCFL;
                    ackCount = row.NACK;
                    totalCount = row.NTOT;
                    ostCount = row.NOST;
                    inView = row.IN_VIEW;
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, ex.Message);
            }
            finally
            {
            }
            return hasCfl;
        }

        public void InitConflictStatusCount()
        {
            try
            {
                GetWriterLock();
                MyDsFlightCfl.TBL_CFL.Clear();
                //int rowCnt = myAFT.Count;
                //for (int i = 0; i < rowCnt; i++)
                //{
                //    IRow row = myAFT[i];
                //    UpdateStatusConflicts((string)row["URNO"]);
                //}
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "");
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        private string _curUpdUaft = "";

        private void UpdateStatusConflictInfo(string strAftUrno)
        {
            if (string.IsNullOrEmpty(strAftUrno)) return;
            if (strAftUrno == _curUpdUaft) return;
            try
            {
                _curUpdUaft = strAftUrno;
                GetWriterLock();
                if ((strAftUrno != null) && (strAftUrno != ""))
                {
                    DSFlightCflStat.TBL_CFL_STATRow[] rows = (DSFlightCflStat.TBL_CFL_STATRow[])MyDsFlightCfl.TBL_CFL_STAT.Select("UAFT='" + strAftUrno + "'");
                    DSFlightCflStat.TBL_CFL_STATRow row = null;
                    if (rows.Length < 1)
                    {
                        row = MyDsFlightCfl.TBL_CFL_STAT.NewTBL_CFL_STATRow();
                        row.UAFT = strAftUrno;
                        MyDsFlightCfl.TBL_CFL_STAT.AddTBL_CFL_STATRow(row);
                        MyDsFlightCfl.TBL_CFL_STAT.AcceptChanges();
                    }
                    else
                    {
                        row = rows[0];
                    }

                    if (row != null)
                    {
                        string lastStat = "";
                        string allCfl = "";
                        try
                        {
                            UT.LogMsg("CtrlCfl:UpdateStatusConflictInfo: b4 AFT,OST Acq Reader Lock. UAFT<" + strAftUrno + ">");
                            myOST.Lock.AcquireReaderLock(Timeout.Infinite);
                            UT.LogMsg("CtrlCfl:UpdateStatusConflictInfo: af AFT,OST Acq Reader Lock");

                            IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);

                            if (ostRows.Length > -1)
                            {
                                try
                                {
                                    lastStat = GetStatusForFlight(ostRows, ref allCfl);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogExceptionErr(ex, "");
                        }
                        finally
                        {
                            UT.LogMsg("CtrlCfl:UpdateStatusConflictInfo: b4 AFT,OST Rel Reader Lock");
                            if ((myOST != null) && (myOST.Lock.IsReaderLockHeld)) myOST.Lock.ReleaseReaderLock();
                            UT.LogMsg("CtrlCfl:UpdateStatusConflictInfo: af AFT,OST Rel Reader Lock");
                        }

                        row.LAST_STAT = lastStat;
                        row.ALL_STAT = allCfl;
                        MyDsFlightCfl.TBL_CFL_STAT.AcceptChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "Fail to Update Conflict Status.");
            }
            finally
            {
                ReleaseWriterLock();
                _curUpdUaft = "";
            }
            UT.LogMsg("CtrlCfl:UpdateStatusConflictInfo:Finish");
        }

        private void UpdateStatusConflictCount(string strAftUrno)
        {
            try
            {
                GetWriterLock();
                if ((strAftUrno != null) && (strAftUrno != ""))
                {
                    DSFlightCflStat.TBL_CFLRow[] rows = (DSFlightCflStat.TBL_CFLRow[])MyDsFlightCfl.TBL_CFL.Select("UAFT='" + strAftUrno + "'");
                    DSFlightCflStat.TBL_CFLRow row = null;
                    if (rows.Length < 1)
                    {
                        row = MyDsFlightCfl.TBL_CFL.NewTBL_CFLRow();
                        row.UAFT = strAftUrno;
                        row.HAS_CFL = false;
                        MyDsFlightCfl.TBL_CFL.AddTBL_CFLRow(row);
                        MyDsFlightCfl.TBL_CFL.AcceptChanges();
                    }
                    else
                    {
                        row = rows[0];
                    }

                    if (row != null)
                    {
                        int cflCnt = 0, ackCnt = 0, totCnt = 0, ostCnt = 0;
                        bool inView = false;
                        row.HAS_CFL = HasFlightStatusConflicts(strAftUrno, ref cflCnt,
                            ref ackCnt, ref totCnt, ref ostCnt, ref inView);
                        row.NACK = ackCnt;
                        row.NCFL = cflCnt;
                        row.NOST = ostCnt;
                        row.NTOT = totCnt;
                        row.IN_VIEW = inView;
                     }
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "Fail to Update Conflict Status.");
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        const string CFL_SEPERATOR = "\n";

        private void Init()
        {
            myDB = UT.GetMemDB();
            myOST = myDB["OST"];
            myAFT = myDB["AFT"];
            mySDE = myDB["SDE"];
            myALT = myDB["ALT"];
            myNAT = myDB["NAT"];
            myHSS = myDB["HSS"];
            mySRH = myDB["SRH"];

            //delegateOstChanged = new Status_Manager.DE.OST_Changed(DE_OnOST_Changed);
            delegateOstChangedForAFlight = new DE.OST_ChangedForAFlight(DE_OnOstChangedForAFlight);
            delegateUpdateFlightData = new DE.UpdateFlightData(DE_OnUpdateFlightData);
            //DE.OnOST_Changed += delegateOstChanged;
            DE.OnOST_ChangedForAFlight += delegateOstChangedForAFlight;
            DE.OnUpdateFlightData += delegateUpdateFlightData;
        }

        private ArrayList GetStatusConflicsForFlight(string strAftUrno)
        {
            ArrayList arr = new ArrayList();
            try
            {
                UT.LogMsg("CFC:GetStatusConflicsForFlight: b4 OST acq Reader Lock");
                myOST.Lock.AcquireReaderLock(Timeout.Infinite);
                mySDE.Lock.AcquireReaderLock(Timeout.Infinite);
                mySRH.Lock.AcquireReaderLock(Timeout.Infinite);
                UT.LogMsg("CFC:GetStatusConflicsForFlight: af OST acq Reader Lock");
                IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
                string strCOTY = "";
                string strValues = "";
                DateTime dat;
                string strUSRH = "";


                if (UT.IsTimeInUtc == true)
                    dat = CFC.GetUTC();// DateTime.UtcNow;
                else
                    dat = DateTime.Now;
                if (ostRows.Length > -1)
                {
                    for (int i = 0; i < ostRows.Length; i++)
                    {
                        strCOTY = ostRows[i]["COTY"];
                        if (strCOTY == "N" || strCOTY == "A")
                        {
                            IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRows[i]["USDE"]);
                            if (sdeRow.Length > 0)
                            {
                                strUSRH = ostRows[i]["USRH"];
                                IRow[] srhRows = mySRH.RowsByIndexValue("URNO", strUSRH);
                                if (srhRows.Length > 0)
                                {
                                    strValues = srhRows[0]["NAME"] + " => " + sdeRow[0]["NAME"];
                                }
                                arr.Add(strValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "GetStatusConflicsForFlight");
            }
            finally
            {
                UT.LogMsg("CFC:GetStatusConflicsForFlight: b4 OST rel Reader Lock");
                try { if (mySRH.Lock.IsReaderLockHeld) mySRH.Lock.ReleaseReaderLock(); }
                catch { }

                try { if (mySDE.Lock.IsReaderLockHeld) mySDE.Lock.ReleaseReaderLock(); }
                catch { }

                try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
                catch { }
                UT.LogMsg("CFC:GetStatusConflicsForFlight: af OST acq Reader Lock");
            }
            return arr;
        }


        private bool HasFlightStatusConflicts(string strAftUrno, ref int CflCount,
            ref int AckCount, ref int TotalCount, ref int OSTCount, ref bool InView)
        {
            bool blRet = false;
            CflCount = 0;
            AckCount = 0;
            TotalCount = 0;
            OSTCount = 0;
            try
            {
                UT.LogMsg("CtrlCfl:HasFlightStatusConflicts: b4 AFT,OST Acq Reader Lock. UAFT<" + strAftUrno + ">" );
                myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                myOST.Lock.AcquireReaderLock(Timeout.Infinite);
                UT.LogMsg("CtrlCfl:HasFlightStatusConflicts: af AFT,OST Acq Reader Lock");

                IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
                IRow[] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);

                OSTCount = ostRows.Length;
                if (ostRows.Length > -1)
                {
                    if (CurrentViewName  == "<DEFAULT>" || CurrentViewIdx < 0)
                    {
                        InView = true;
                        for (int i = 0; i < ostRows.Length; i++)
                        {
                            TotalCount++;
                            if (ostRows[i]["COTY"] == "N")
                            {
                                CflCount++;
                            }
                            if (ostRows[i]["COTY"] == "A")
                            {
                                AckCount++;
                            }
                        }
                    }
                    else
                    {
                        if (aftRows.Length > 0)
                        {
                            IRow[] altRows;
                            IRow[] natRows;
                            bool altCheck = false;
                            bool natCheck = false;
                            
                            string strViewHSS = CFC.GetHssStr();
                            string strViewALT = CFC.GetAltStr();
                            string strViewNAT = CFC.GetNatStr();

                            string strAftAlc = aftRows[0]["ALC2"];
                            string strAftTTYP = aftRows[0]["TTYP"];
                            //if (strAftAlc == "SQ")
                            //{
                            //    strAftAlc = "SQ";
                            //}
                            if (strViewALT != "")
                            {
                                altRows = myALT.RowsByIndexValue("ALC2", strAftAlc);
                                if (altRows.Length == 0)
                                {
                                    strAftAlc = aftRows[0]["ALC3"];
                                    altRows = myALT.RowsByIndexValue("ALC3", strAftAlc);
                                }
                                if (altRows.Length > 0)
                                {
                                    if (strViewALT.IndexOf(altRows[0]["URNO"], 0, strViewALT.Length) > -1)
                                    {
                                        altCheck = true;
                                        InView = true;
                                    }
                                }
                            }
                            else { altCheck = true; }
                            if (strViewNAT != "")
                            {
                                natRows = myNAT.RowsByIndexValue("TTYP", strAftTTYP.Trim());
                                if (natRows.Length > 0)
                                {
                                    if (strViewNAT.IndexOf(natRows[0]["URNO"].Trim(), 0, strViewNAT.Length) > -1)
                                    {
                                        natCheck = true;
                                        InView = true;
                                    }
                                }
                            }
                            else { natCheck = true; }
                            //if flight must be checke iterate through the statuses for this flight
                            //otherwise no conflict is to be detected
                            if (natCheck == true && altCheck == true)
                            {
                                if (strViewHSS != "")// check only for the specified status sections
                                {
                                    //Hier weiter machen
                                    for (int i = 0; i < ostRows.Length; i++)
                                    {
                                        if (strViewHSS.IndexOf(ostRows[i]["UHSS"], 0, strViewHSS.Length) > -1)
                                        {
                                            InView = true;
                                            TotalCount++;
                                            if (ostRows[i]["COTY"] == "N")
                                            {
                                                CflCount++;
                                            }
                                            if (ostRows[i]["COTY"] == "A")
                                            {
                                                AckCount++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < ostRows.Length; i++)
                                    {
                                        TotalCount++;
                                        if (ostRows[i]["COTY"] == "N")
                                        {
                                            CflCount++;
                                        }
                                        if (ostRows[i]["COTY"] == "A")
                                        {
                                            AckCount++;
                                        }
                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(strViewALT) && string.IsNullOrEmpty(strViewNAT) && string.IsNullOrEmpty(strViewHSS))
                            {
                                InView = true;
                            }
                        }//if(aftRows.Length > 0)
                    }// else of if(currentViewName == "<DEFAULT>" || myViewIdx < 0)
                }
            }
            catch (Exception ex)
            {
                UT.LogMsg("CtrlCfl:HasFlightStatusConflicts:Err:" + ex.Message + Environment.NewLine + ex.StackTrace);
                throw ex;
            }
            finally
            {
                UT.LogMsg("CtrlCfl:HasFlightStatusConflicts: b4 AFT,OST Rel Reader Lock");
                try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
                catch { }
                try { if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock(); }
                catch { }
                UT.LogMsg("CtrlCfl:HasFlightStatusConflicts: af AFT,OST Rel Reader Lock");
            }
            if (AckCount > 0 || CflCount > 0)
                blRet = true;
            return blRet;
        }

        private string GetLastActionStatusForFlight(string strAftUrno)
        {
            string strRet = "";
            try
            {
                UT.LogMsg("CFC:GetLastActionStatusForFlight: b4 OST acq Reader Lock");
                myOST.Lock.AcquireReaderLock(Timeout.Infinite);
                mySDE.Lock.AcquireReaderLock(Timeout.Infinite);
                UT.LogMsg("CFC:GetLastActionStatusForFlight: af OST acq Reader Lock");

                IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "GetLastActionStatusForFlight");
            }
            finally
            {
                UT.LogMsg("CFC:GetLastActionStatusForFlight: b4 OST rel Reader Lock");
                try { if (mySDE.Lock.IsReaderLockHeld) mySDE.Lock.ReleaseReaderLock(); }
                catch { }
                try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
                catch { }
                UT.LogMsg("CFC:GetLastActionStatusForFlight: af OST rel Reader Lock");

            }
            return strRet;
        }

        private string GetCflStatus(IRow ostRow)
        {
            string strRet = "";
            string strCOTY = ostRow["COTY"];
            if (strCOTY == "N" || strCOTY == "A")
            {
                IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRow["USDE"]);
                if (sdeRow.Length > 0)
                {
                    string strUSRH = ostRow["USRH"];
                    IRow[] srhRows = mySRH.RowsByIndexValue("URNO", strUSRH);
                    if (srhRows.Length > 0)
                    {
                        strRet = srhRows[0]["NAME"] + " => " + sdeRow[0]["NAME"];
                    }
                }
            }
            return strRet;
        }

        private string GetStatusForFlight(IRow[] ostRows, ref string allCflStat)
        {
            string strRet = "";
            allCflStat = "";
            if (ostRows != null)
            {
                int idx = -1;
                DateTime datBest = new DateTime(1900, 1, 1, 1, 1, 1, 1);
                DateTime datConu = new DateTime(1900, 1, 1, 1, 1, 1, 1);
                DateTime datCona = new DateTime(1900, 1, 1, 1, 1, 1, 1);
                DateTime datLast = new DateTime(1900, 1, 1, 1, 1, 1, 1);
                string del = "";
                for (int i = 0; i < ostRows.Length; i++)
                {
                    try
                    {
                        string cfl = GetCflStatus(ostRows[i]).Trim();
                        if (cfl != "")
                        {
                            allCflStat += del + cfl;
                            if (del == "")
                            {
                                del = CFL_SEPERATOR;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogExceptionErr(ex, "");
                    }
                    
                    if (ostRows[i]["CONU"].Trim() != "" || ostRows[i]["CONA"].Trim() != "")
                    {
                        if (ostRows[i]["CONU"].Trim() != "" && ostRows[i]["CONA"].Trim() != "")
                        {
                            datConu = UT.CedaFullDateToDateTime(ostRows[i]["CONU"].Trim());
                            datCona = UT.CedaFullDateToDateTime(ostRows[i]["CONA"].Trim());
                            if (datConu >= datCona)
                                datBest = datConu;
                            else
                                datBest = datCona;
                        }
                        else if (ostRows[i]["CONU"].Trim() != "" && ostRows[i]["CONA"].Trim() == "")
                        {
                            datBest = UT.CedaFullDateToDateTime(ostRows[i]["CONU"].Trim());
                        }
                        else if (ostRows[i]["CONU"].Trim() == "" && ostRows[i]["CONA"].Trim() != "")
                        {
                            datBest = UT.CedaFullDateToDateTime(ostRows[i]["CONA"].Trim());
                        }
                        if (datBest > datLast)
                        {
                            idx = i;
                            datLast = datBest;
                        }
                    }
                }

                if (idx > -1)
                {
                    strRet = GetRuleNameForId(ostRows[idx]["USRH"], ostRows[idx]["USDE"]);
                }
            }
            return strRet;
        }

        private string GetRuleNameForId(string srhUrno, string sdeUrno)
        {
            string strRet = "";
            try
            {
                if ((srhUrno != null) && (srhUrno != ""))
                {
                    IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", sdeUrno);
                    if (sdeRow.Length > 0)
                    {
                        IRow[] srhRows = mySRH.RowsByIndexValue("URNO", srhUrno);
                        if (srhRows.Length > 0)
                        {
                            strRet = srhRows[0]["NAME"] + " => " + sdeRow[0]["NAME"];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "");
            }
            return strRet;
        }

        private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
        {
            try
            {
                UpdateStatusConflictCount(strAftUrno);
                //UpdateStatusConflictInfo(strAftUrno);                
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    DE.Call_FlightStatus_Changed(this, strAftUrno);
                }
                catch (Exception)
                {
                }
            }
        }

        private void DE_OnOstChangedForAFlight(object sender, string strAftUrno)
        {
            UT.LogMsg("CtrlCfl:DE_OnOstChangedForAFlight:Start");
            try
            {
                UpdateStatusConflictCount(strAftUrno);
                UpdateStatusConflictInfo(strAftUrno);
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    DE.Call_FlightStatus_Changed(this, strAftUrno);
                }
                catch (Exception)
                {
                }
            }
            UT.LogMsg("CtrlCfl:DE_OnOstChangedForAFlight:Finish");
        }


        private static void LogExceptionErr(Exception ex, string msgToDisplay)
        {
            UT.LogMsg("CtrlCfl:Err:" + ex.Message + Environment.NewLine + ex.StackTrace);
            if (msgToDisplay.Trim() != "")
            {
                throw new ApplicationException(msgToDisplay);
            }
        }
    }


}