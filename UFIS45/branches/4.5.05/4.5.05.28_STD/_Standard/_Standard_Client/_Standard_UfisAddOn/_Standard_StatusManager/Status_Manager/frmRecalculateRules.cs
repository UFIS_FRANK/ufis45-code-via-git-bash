using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmRecalculateRules.
	/// </summary>
	public class frmRecalculateRules : System.Windows.Forms.Form
	{
		public System.Windows.Forms.DateTimePicker dtTo;
		public System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox txtALC;
		private System.Windows.Forms.TextBox txtFLNO;
		private System.Windows.Forms.TextBox txtORIG;
		private System.Windows.Forms.TextBox txtDEST;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblFrom;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.Label lblFlno;
		private System.Windows.Forms.Label lblOrig;
		private System.Windows.Forms.Label lblDest;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtSection;
		private System.Windows.Forms.Button btnALT;
		private System.Windows.Forms.TextBox txtHSSU;
		private System.Windows.Forms.Label lblSection;
		private System.Windows.Forms.TextBox txtSuffix;
		private System.Windows.Forms.ImageList imageList1;
		private System.ComponentModel.IContainer components;

		public frmRecalculateRules()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmRecalculateRules));
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.txtALC = new System.Windows.Forms.TextBox();
			this.txtFLNO = new System.Windows.Forms.TextBox();
			this.txtORIG = new System.Windows.Forms.TextBox();
			this.txtDEST = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lblFrom = new System.Windows.Forms.Label();
			this.lblTo = new System.Windows.Forms.Label();
			this.lblFlno = new System.Windows.Forms.Label();
			this.lblOrig = new System.Windows.Forms.Label();
			this.lblDest = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.txtSection = new System.Windows.Forms.TextBox();
			this.btnALT = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.txtHSSU = new System.Windows.Forms.TextBox();
			this.lblSection = new System.Windows.Forms.Label();
			this.txtSuffix = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// dtTo
			// 
			this.dtTo.AccessibleDescription = resources.GetString("dtTo.AccessibleDescription");
			this.dtTo.AccessibleName = resources.GetString("dtTo.AccessibleName");
			this.dtTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtTo.Anchor")));
			this.dtTo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtTo.BackgroundImage")));
			this.dtTo.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtTo.CalendarFont")));
			this.dtTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtTo.Dock")));
			this.dtTo.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtTo.DropDownAlign")));
			this.dtTo.Enabled = ((bool)(resources.GetObject("dtTo.Enabled")));
			this.dtTo.Font = ((System.Drawing.Font)(resources.GetObject("dtTo.Font")));
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtTo.ImeMode")));
			this.dtTo.Location = ((System.Drawing.Point)(resources.GetObject("dtTo.Location")));
			this.dtTo.Name = "dtTo";
			this.dtTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtTo.RightToLeft")));
			this.dtTo.Size = ((System.Drawing.Size)(resources.GetObject("dtTo.Size")));
			this.dtTo.TabIndex = ((int)(resources.GetObject("dtTo.TabIndex")));
			this.dtTo.Visible = ((bool)(resources.GetObject("dtTo.Visible")));
			// 
			// dtFrom
			// 
			this.dtFrom.AccessibleDescription = resources.GetString("dtFrom.AccessibleDescription");
			this.dtFrom.AccessibleName = resources.GetString("dtFrom.AccessibleName");
			this.dtFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtFrom.Anchor")));
			this.dtFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtFrom.BackgroundImage")));
			this.dtFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtFrom.CalendarFont")));
			this.dtFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtFrom.Dock")));
			this.dtFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtFrom.DropDownAlign")));
			this.dtFrom.Enabled = ((bool)(resources.GetObject("dtFrom.Enabled")));
			this.dtFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtFrom.Font")));
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtFrom.ImeMode")));
			this.dtFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtFrom.Location")));
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtFrom.RightToLeft")));
			this.dtFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtFrom.Size")));
			this.dtFrom.TabIndex = ((int)(resources.GetObject("dtFrom.TabIndex")));
			this.dtFrom.Visible = ((bool)(resources.GetObject("dtFrom.Visible")));
			// 
			// pictureBox1
			// 
			this.pictureBox1.AccessibleDescription = resources.GetString("pictureBox1.AccessibleDescription");
			this.pictureBox1.AccessibleName = resources.GetString("pictureBox1.AccessibleName");
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox1.Anchor")));
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox1.Dock")));
			this.pictureBox1.Enabled = ((bool)(resources.GetObject("pictureBox1.Enabled")));
			this.pictureBox1.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox1.Font")));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox1.ImeMode")));
			this.pictureBox1.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox1.Location")));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox1.RightToLeft")));
			this.pictureBox1.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox1.Size")));
			this.pictureBox1.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox1.SizeMode")));
			this.pictureBox1.TabIndex = ((int)(resources.GetObject("pictureBox1.TabIndex")));
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Text = resources.GetString("pictureBox1.Text");
			this.pictureBox1.Visible = ((bool)(resources.GetObject("pictureBox1.Visible")));
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// txtALC
			// 
			this.txtALC.AccessibleDescription = resources.GetString("txtALC.AccessibleDescription");
			this.txtALC.AccessibleName = resources.GetString("txtALC.AccessibleName");
			this.txtALC.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtALC.Anchor")));
			this.txtALC.AutoSize = ((bool)(resources.GetObject("txtALC.AutoSize")));
			this.txtALC.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtALC.BackgroundImage")));
			this.txtALC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtALC.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtALC.Dock")));
			this.txtALC.Enabled = ((bool)(resources.GetObject("txtALC.Enabled")));
			this.txtALC.Font = ((System.Drawing.Font)(resources.GetObject("txtALC.Font")));
			this.txtALC.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtALC.ImeMode")));
			this.txtALC.Location = ((System.Drawing.Point)(resources.GetObject("txtALC.Location")));
			this.txtALC.MaxLength = ((int)(resources.GetObject("txtALC.MaxLength")));
			this.txtALC.Multiline = ((bool)(resources.GetObject("txtALC.Multiline")));
			this.txtALC.Name = "txtALC";
			this.txtALC.PasswordChar = ((char)(resources.GetObject("txtALC.PasswordChar")));
			this.txtALC.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtALC.RightToLeft")));
			this.txtALC.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtALC.ScrollBars")));
			this.txtALC.Size = ((System.Drawing.Size)(resources.GetObject("txtALC.Size")));
			this.txtALC.TabIndex = ((int)(resources.GetObject("txtALC.TabIndex")));
			this.txtALC.Text = resources.GetString("txtALC.Text");
			this.txtALC.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtALC.TextAlign")));
			this.txtALC.Visible = ((bool)(resources.GetObject("txtALC.Visible")));
			this.txtALC.WordWrap = ((bool)(resources.GetObject("txtALC.WordWrap")));
			// 
			// txtFLNO
			// 
			this.txtFLNO.AccessibleDescription = resources.GetString("txtFLNO.AccessibleDescription");
			this.txtFLNO.AccessibleName = resources.GetString("txtFLNO.AccessibleName");
			this.txtFLNO.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtFLNO.Anchor")));
			this.txtFLNO.AutoSize = ((bool)(resources.GetObject("txtFLNO.AutoSize")));
			this.txtFLNO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFLNO.BackgroundImage")));
			this.txtFLNO.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtFLNO.Dock")));
			this.txtFLNO.Enabled = ((bool)(resources.GetObject("txtFLNO.Enabled")));
			this.txtFLNO.Font = ((System.Drawing.Font)(resources.GetObject("txtFLNO.Font")));
			this.txtFLNO.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtFLNO.ImeMode")));
			this.txtFLNO.Location = ((System.Drawing.Point)(resources.GetObject("txtFLNO.Location")));
			this.txtFLNO.MaxLength = ((int)(resources.GetObject("txtFLNO.MaxLength")));
			this.txtFLNO.Multiline = ((bool)(resources.GetObject("txtFLNO.Multiline")));
			this.txtFLNO.Name = "txtFLNO";
			this.txtFLNO.PasswordChar = ((char)(resources.GetObject("txtFLNO.PasswordChar")));
			this.txtFLNO.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtFLNO.RightToLeft")));
			this.txtFLNO.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtFLNO.ScrollBars")));
			this.txtFLNO.Size = ((System.Drawing.Size)(resources.GetObject("txtFLNO.Size")));
			this.txtFLNO.TabIndex = ((int)(resources.GetObject("txtFLNO.TabIndex")));
			this.txtFLNO.Text = resources.GetString("txtFLNO.Text");
			this.txtFLNO.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtFLNO.TextAlign")));
			this.txtFLNO.Visible = ((bool)(resources.GetObject("txtFLNO.Visible")));
			this.txtFLNO.WordWrap = ((bool)(resources.GetObject("txtFLNO.WordWrap")));
			// 
			// txtORIG
			// 
			this.txtORIG.AccessibleDescription = resources.GetString("txtORIG.AccessibleDescription");
			this.txtORIG.AccessibleName = resources.GetString("txtORIG.AccessibleName");
			this.txtORIG.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtORIG.Anchor")));
			this.txtORIG.AutoSize = ((bool)(resources.GetObject("txtORIG.AutoSize")));
			this.txtORIG.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtORIG.BackgroundImage")));
			this.txtORIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtORIG.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtORIG.Dock")));
			this.txtORIG.Enabled = ((bool)(resources.GetObject("txtORIG.Enabled")));
			this.txtORIG.Font = ((System.Drawing.Font)(resources.GetObject("txtORIG.Font")));
			this.txtORIG.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtORIG.ImeMode")));
			this.txtORIG.Location = ((System.Drawing.Point)(resources.GetObject("txtORIG.Location")));
			this.txtORIG.MaxLength = ((int)(resources.GetObject("txtORIG.MaxLength")));
			this.txtORIG.Multiline = ((bool)(resources.GetObject("txtORIG.Multiline")));
			this.txtORIG.Name = "txtORIG";
			this.txtORIG.PasswordChar = ((char)(resources.GetObject("txtORIG.PasswordChar")));
			this.txtORIG.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtORIG.RightToLeft")));
			this.txtORIG.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtORIG.ScrollBars")));
			this.txtORIG.Size = ((System.Drawing.Size)(resources.GetObject("txtORIG.Size")));
			this.txtORIG.TabIndex = ((int)(resources.GetObject("txtORIG.TabIndex")));
			this.txtORIG.Text = resources.GetString("txtORIG.Text");
			this.txtORIG.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtORIG.TextAlign")));
			this.txtORIG.Visible = ((bool)(resources.GetObject("txtORIG.Visible")));
			this.txtORIG.WordWrap = ((bool)(resources.GetObject("txtORIG.WordWrap")));
			// 
			// txtDEST
			// 
			this.txtDEST.AccessibleDescription = resources.GetString("txtDEST.AccessibleDescription");
			this.txtDEST.AccessibleName = resources.GetString("txtDEST.AccessibleName");
			this.txtDEST.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDEST.Anchor")));
			this.txtDEST.AutoSize = ((bool)(resources.GetObject("txtDEST.AutoSize")));
			this.txtDEST.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDEST.BackgroundImage")));
			this.txtDEST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtDEST.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDEST.Dock")));
			this.txtDEST.Enabled = ((bool)(resources.GetObject("txtDEST.Enabled")));
			this.txtDEST.Font = ((System.Drawing.Font)(resources.GetObject("txtDEST.Font")));
			this.txtDEST.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDEST.ImeMode")));
			this.txtDEST.Location = ((System.Drawing.Point)(resources.GetObject("txtDEST.Location")));
			this.txtDEST.MaxLength = ((int)(resources.GetObject("txtDEST.MaxLength")));
			this.txtDEST.Multiline = ((bool)(resources.GetObject("txtDEST.Multiline")));
			this.txtDEST.Name = "txtDEST";
			this.txtDEST.PasswordChar = ((char)(resources.GetObject("txtDEST.PasswordChar")));
			this.txtDEST.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDEST.RightToLeft")));
			this.txtDEST.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDEST.ScrollBars")));
			this.txtDEST.Size = ((System.Drawing.Size)(resources.GetObject("txtDEST.Size")));
			this.txtDEST.TabIndex = ((int)(resources.GetObject("txtDEST.TabIndex")));
			this.txtDEST.Text = resources.GetString("txtDEST.Text");
			this.txtDEST.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDEST.TextAlign")));
			this.txtDEST.Visible = ((bool)(resources.GetObject("txtDEST.Visible")));
			this.txtDEST.WordWrap = ((bool)(resources.GetObject("txtDEST.WordWrap")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// lblFrom
			// 
			this.lblFrom.AccessibleDescription = resources.GetString("lblFrom.AccessibleDescription");
			this.lblFrom.AccessibleName = resources.GetString("lblFrom.AccessibleName");
			this.lblFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFrom.Anchor")));
			this.lblFrom.AutoSize = ((bool)(resources.GetObject("lblFrom.AutoSize")));
			this.lblFrom.BackColor = System.Drawing.Color.Transparent;
			this.lblFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFrom.Dock")));
			this.lblFrom.Enabled = ((bool)(resources.GetObject("lblFrom.Enabled")));
			this.lblFrom.Font = ((System.Drawing.Font)(resources.GetObject("lblFrom.Font")));
			this.lblFrom.Image = ((System.Drawing.Image)(resources.GetObject("lblFrom.Image")));
			this.lblFrom.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFrom.ImageAlign")));
			this.lblFrom.ImageIndex = ((int)(resources.GetObject("lblFrom.ImageIndex")));
			this.lblFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFrom.ImeMode")));
			this.lblFrom.Location = ((System.Drawing.Point)(resources.GetObject("lblFrom.Location")));
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFrom.RightToLeft")));
			this.lblFrom.Size = ((System.Drawing.Size)(resources.GetObject("lblFrom.Size")));
			this.lblFrom.TabIndex = ((int)(resources.GetObject("lblFrom.TabIndex")));
			this.lblFrom.Text = resources.GetString("lblFrom.Text");
			this.lblFrom.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFrom.TextAlign")));
			this.lblFrom.Visible = ((bool)(resources.GetObject("lblFrom.Visible")));
			// 
			// lblTo
			// 
			this.lblTo.AccessibleDescription = resources.GetString("lblTo.AccessibleDescription");
			this.lblTo.AccessibleName = resources.GetString("lblTo.AccessibleName");
			this.lblTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblTo.Anchor")));
			this.lblTo.AutoSize = ((bool)(resources.GetObject("lblTo.AutoSize")));
			this.lblTo.BackColor = System.Drawing.Color.Transparent;
			this.lblTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblTo.Dock")));
			this.lblTo.Enabled = ((bool)(resources.GetObject("lblTo.Enabled")));
			this.lblTo.Font = ((System.Drawing.Font)(resources.GetObject("lblTo.Font")));
			this.lblTo.Image = ((System.Drawing.Image)(resources.GetObject("lblTo.Image")));
			this.lblTo.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblTo.ImageAlign")));
			this.lblTo.ImageIndex = ((int)(resources.GetObject("lblTo.ImageIndex")));
			this.lblTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblTo.ImeMode")));
			this.lblTo.Location = ((System.Drawing.Point)(resources.GetObject("lblTo.Location")));
			this.lblTo.Name = "lblTo";
			this.lblTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblTo.RightToLeft")));
			this.lblTo.Size = ((System.Drawing.Size)(resources.GetObject("lblTo.Size")));
			this.lblTo.TabIndex = ((int)(resources.GetObject("lblTo.TabIndex")));
			this.lblTo.Text = resources.GetString("lblTo.Text");
			this.lblTo.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblTo.TextAlign")));
			this.lblTo.Visible = ((bool)(resources.GetObject("lblTo.Visible")));
			// 
			// lblFlno
			// 
			this.lblFlno.AccessibleDescription = resources.GetString("lblFlno.AccessibleDescription");
			this.lblFlno.AccessibleName = resources.GetString("lblFlno.AccessibleName");
			this.lblFlno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFlno.Anchor")));
			this.lblFlno.AutoSize = ((bool)(resources.GetObject("lblFlno.AutoSize")));
			this.lblFlno.BackColor = System.Drawing.Color.Transparent;
			this.lblFlno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFlno.Dock")));
			this.lblFlno.Enabled = ((bool)(resources.GetObject("lblFlno.Enabled")));
			this.lblFlno.Font = ((System.Drawing.Font)(resources.GetObject("lblFlno.Font")));
			this.lblFlno.Image = ((System.Drawing.Image)(resources.GetObject("lblFlno.Image")));
			this.lblFlno.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlno.ImageAlign")));
			this.lblFlno.ImageIndex = ((int)(resources.GetObject("lblFlno.ImageIndex")));
			this.lblFlno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFlno.ImeMode")));
			this.lblFlno.Location = ((System.Drawing.Point)(resources.GetObject("lblFlno.Location")));
			this.lblFlno.Name = "lblFlno";
			this.lblFlno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFlno.RightToLeft")));
			this.lblFlno.Size = ((System.Drawing.Size)(resources.GetObject("lblFlno.Size")));
			this.lblFlno.TabIndex = ((int)(resources.GetObject("lblFlno.TabIndex")));
			this.lblFlno.Text = resources.GetString("lblFlno.Text");
			this.lblFlno.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlno.TextAlign")));
			this.lblFlno.Visible = ((bool)(resources.GetObject("lblFlno.Visible")));
			// 
			// lblOrig
			// 
			this.lblOrig.AccessibleDescription = resources.GetString("lblOrig.AccessibleDescription");
			this.lblOrig.AccessibleName = resources.GetString("lblOrig.AccessibleName");
			this.lblOrig.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblOrig.Anchor")));
			this.lblOrig.AutoSize = ((bool)(resources.GetObject("lblOrig.AutoSize")));
			this.lblOrig.BackColor = System.Drawing.Color.Transparent;
			this.lblOrig.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblOrig.Dock")));
			this.lblOrig.Enabled = ((bool)(resources.GetObject("lblOrig.Enabled")));
			this.lblOrig.Font = ((System.Drawing.Font)(resources.GetObject("lblOrig.Font")));
			this.lblOrig.Image = ((System.Drawing.Image)(resources.GetObject("lblOrig.Image")));
			this.lblOrig.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOrig.ImageAlign")));
			this.lblOrig.ImageIndex = ((int)(resources.GetObject("lblOrig.ImageIndex")));
			this.lblOrig.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblOrig.ImeMode")));
			this.lblOrig.Location = ((System.Drawing.Point)(resources.GetObject("lblOrig.Location")));
			this.lblOrig.Name = "lblOrig";
			this.lblOrig.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblOrig.RightToLeft")));
			this.lblOrig.Size = ((System.Drawing.Size)(resources.GetObject("lblOrig.Size")));
			this.lblOrig.TabIndex = ((int)(resources.GetObject("lblOrig.TabIndex")));
			this.lblOrig.Text = resources.GetString("lblOrig.Text");
			this.lblOrig.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOrig.TextAlign")));
			this.lblOrig.Visible = ((bool)(resources.GetObject("lblOrig.Visible")));
			// 
			// lblDest
			// 
			this.lblDest.AccessibleDescription = resources.GetString("lblDest.AccessibleDescription");
			this.lblDest.AccessibleName = resources.GetString("lblDest.AccessibleName");
			this.lblDest.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblDest.Anchor")));
			this.lblDest.AutoSize = ((bool)(resources.GetObject("lblDest.AutoSize")));
			this.lblDest.BackColor = System.Drawing.Color.Transparent;
			this.lblDest.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblDest.Dock")));
			this.lblDest.Enabled = ((bool)(resources.GetObject("lblDest.Enabled")));
			this.lblDest.Font = ((System.Drawing.Font)(resources.GetObject("lblDest.Font")));
			this.lblDest.Image = ((System.Drawing.Image)(resources.GetObject("lblDest.Image")));
			this.lblDest.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDest.ImageAlign")));
			this.lblDest.ImageIndex = ((int)(resources.GetObject("lblDest.ImageIndex")));
			this.lblDest.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblDest.ImeMode")));
			this.lblDest.Location = ((System.Drawing.Point)(resources.GetObject("lblDest.Location")));
			this.lblDest.Name = "lblDest";
			this.lblDest.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblDest.RightToLeft")));
			this.lblDest.Size = ((System.Drawing.Size)(resources.GetObject("lblDest.Size")));
			this.lblDest.TabIndex = ((int)(resources.GetObject("lblDest.TabIndex")));
			this.lblDest.Text = resources.GetString("lblDest.Text");
			this.lblDest.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDest.TextAlign")));
			this.lblDest.Visible = ((bool)(resources.GetObject("lblDest.Visible")));
			// 
			// btnOK
			// 
			this.btnOK.AccessibleDescription = resources.GetString("btnOK.AccessibleDescription");
			this.btnOK.AccessibleName = resources.GetString("btnOK.AccessibleName");
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOK.Anchor")));
			this.btnOK.BackColor = System.Drawing.Color.Transparent;
			this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOK.Dock")));
			this.btnOK.Enabled = ((bool)(resources.GetObject("btnOK.Enabled")));
			this.btnOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOK.FlatStyle")));
			this.btnOK.Font = ((System.Drawing.Font)(resources.GetObject("btnOK.Font")));
			this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
			this.btnOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.ImageAlign")));
			this.btnOK.ImageIndex = ((int)(resources.GetObject("btnOK.ImageIndex")));
			this.btnOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOK.ImeMode")));
			this.btnOK.Location = ((System.Drawing.Point)(resources.GetObject("btnOK.Location")));
			this.btnOK.Name = "btnOK";
			this.btnOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOK.RightToLeft")));
			this.btnOK.Size = ((System.Drawing.Size)(resources.GetObject("btnOK.Size")));
			this.btnOK.TabIndex = ((int)(resources.GetObject("btnOK.TabIndex")));
			this.btnOK.Text = resources.GetString("btnOK.Text");
			this.btnOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.TextAlign")));
			this.btnOK.Visible = ((bool)(resources.GetObject("btnOK.Visible")));
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// txtSection
			// 
			this.txtSection.AccessibleDescription = resources.GetString("txtSection.AccessibleDescription");
			this.txtSection.AccessibleName = resources.GetString("txtSection.AccessibleName");
			this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSection.Anchor")));
			this.txtSection.AutoSize = ((bool)(resources.GetObject("txtSection.AutoSize")));
			this.txtSection.BackColor = System.Drawing.Color.White;
			this.txtSection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSection.BackgroundImage")));
			this.txtSection.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSection.Dock")));
			this.txtSection.Enabled = ((bool)(resources.GetObject("txtSection.Enabled")));
			this.txtSection.Font = ((System.Drawing.Font)(resources.GetObject("txtSection.Font")));
			this.txtSection.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSection.ImeMode")));
			this.txtSection.Location = ((System.Drawing.Point)(resources.GetObject("txtSection.Location")));
			this.txtSection.MaxLength = ((int)(resources.GetObject("txtSection.MaxLength")));
			this.txtSection.Multiline = ((bool)(resources.GetObject("txtSection.Multiline")));
			this.txtSection.Name = "txtSection";
			this.txtSection.PasswordChar = ((char)(resources.GetObject("txtSection.PasswordChar")));
			this.txtSection.ReadOnly = true;
			this.txtSection.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSection.RightToLeft")));
			this.txtSection.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSection.ScrollBars")));
			this.txtSection.Size = ((System.Drawing.Size)(resources.GetObject("txtSection.Size")));
			this.txtSection.TabIndex = ((int)(resources.GetObject("txtSection.TabIndex")));
			this.txtSection.Text = resources.GetString("txtSection.Text");
			this.txtSection.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSection.TextAlign")));
			this.txtSection.Visible = ((bool)(resources.GetObject("txtSection.Visible")));
			this.txtSection.WordWrap = ((bool)(resources.GetObject("txtSection.WordWrap")));
			// 
			// btnALT
			// 
			this.btnALT.AccessibleDescription = resources.GetString("btnALT.AccessibleDescription");
			this.btnALT.AccessibleName = resources.GetString("btnALT.AccessibleName");
			this.btnALT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnALT.Anchor")));
			this.btnALT.BackColor = System.Drawing.SystemColors.Control;
			this.btnALT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnALT.BackgroundImage")));
			this.btnALT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnALT.Dock")));
			this.btnALT.Enabled = ((bool)(resources.GetObject("btnALT.Enabled")));
			this.btnALT.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnALT.FlatStyle")));
			this.btnALT.Font = ((System.Drawing.Font)(resources.GetObject("btnALT.Font")));
			this.btnALT.Image = ((System.Drawing.Image)(resources.GetObject("btnALT.Image")));
			this.btnALT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALT.ImageAlign")));
			this.btnALT.ImageIndex = ((int)(resources.GetObject("btnALT.ImageIndex")));
			this.btnALT.ImageList = this.imageList1;
			this.btnALT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnALT.ImeMode")));
			this.btnALT.Location = ((System.Drawing.Point)(resources.GetObject("btnALT.Location")));
			this.btnALT.Name = "btnALT";
			this.btnALT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnALT.RightToLeft")));
			this.btnALT.Size = ((System.Drawing.Size)(resources.GetObject("btnALT.Size")));
			this.btnALT.TabIndex = ((int)(resources.GetObject("btnALT.TabIndex")));
			this.btnALT.Text = resources.GetString("btnALT.Text");
			this.btnALT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALT.TextAlign")));
			this.btnALT.Visible = ((bool)(resources.GetObject("btnALT.Visible")));
			this.btnALT.Click += new System.EventHandler(this.btnALT_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageList1.ImageSize")));
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// txtHSSU
			// 
			this.txtHSSU.AccessibleDescription = resources.GetString("txtHSSU.AccessibleDescription");
			this.txtHSSU.AccessibleName = resources.GetString("txtHSSU.AccessibleName");
			this.txtHSSU.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtHSSU.Anchor")));
			this.txtHSSU.AutoSize = ((bool)(resources.GetObject("txtHSSU.AutoSize")));
			this.txtHSSU.BackColor = System.Drawing.Color.White;
			this.txtHSSU.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtHSSU.BackgroundImage")));
			this.txtHSSU.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtHSSU.Dock")));
			this.txtHSSU.Enabled = ((bool)(resources.GetObject("txtHSSU.Enabled")));
			this.txtHSSU.Font = ((System.Drawing.Font)(resources.GetObject("txtHSSU.Font")));
			this.txtHSSU.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtHSSU.ImeMode")));
			this.txtHSSU.Location = ((System.Drawing.Point)(resources.GetObject("txtHSSU.Location")));
			this.txtHSSU.MaxLength = ((int)(resources.GetObject("txtHSSU.MaxLength")));
			this.txtHSSU.Multiline = ((bool)(resources.GetObject("txtHSSU.Multiline")));
			this.txtHSSU.Name = "txtHSSU";
			this.txtHSSU.PasswordChar = ((char)(resources.GetObject("txtHSSU.PasswordChar")));
			this.txtHSSU.ReadOnly = true;
			this.txtHSSU.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtHSSU.RightToLeft")));
			this.txtHSSU.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtHSSU.ScrollBars")));
			this.txtHSSU.Size = ((System.Drawing.Size)(resources.GetObject("txtHSSU.Size")));
			this.txtHSSU.TabIndex = ((int)(resources.GetObject("txtHSSU.TabIndex")));
			this.txtHSSU.Text = resources.GetString("txtHSSU.Text");
			this.txtHSSU.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtHSSU.TextAlign")));
			this.txtHSSU.Visible = ((bool)(resources.GetObject("txtHSSU.Visible")));
			this.txtHSSU.WordWrap = ((bool)(resources.GetObject("txtHSSU.WordWrap")));
			// 
			// lblSection
			// 
			this.lblSection.AccessibleDescription = resources.GetString("lblSection.AccessibleDescription");
			this.lblSection.AccessibleName = resources.GetString("lblSection.AccessibleName");
			this.lblSection.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblSection.Anchor")));
			this.lblSection.AutoSize = ((bool)(resources.GetObject("lblSection.AutoSize")));
			this.lblSection.BackColor = System.Drawing.Color.Transparent;
			this.lblSection.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblSection.Dock")));
			this.lblSection.Enabled = ((bool)(resources.GetObject("lblSection.Enabled")));
			this.lblSection.Font = ((System.Drawing.Font)(resources.GetObject("lblSection.Font")));
			this.lblSection.Image = ((System.Drawing.Image)(resources.GetObject("lblSection.Image")));
			this.lblSection.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblSection.ImageAlign")));
			this.lblSection.ImageIndex = ((int)(resources.GetObject("lblSection.ImageIndex")));
			this.lblSection.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblSection.ImeMode")));
			this.lblSection.Location = ((System.Drawing.Point)(resources.GetObject("lblSection.Location")));
			this.lblSection.Name = "lblSection";
			this.lblSection.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblSection.RightToLeft")));
			this.lblSection.Size = ((System.Drawing.Size)(resources.GetObject("lblSection.Size")));
			this.lblSection.TabIndex = ((int)(resources.GetObject("lblSection.TabIndex")));
			this.lblSection.Text = resources.GetString("lblSection.Text");
			this.lblSection.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblSection.TextAlign")));
			this.lblSection.Visible = ((bool)(resources.GetObject("lblSection.Visible")));
			// 
			// txtSuffix
			// 
			this.txtSuffix.AccessibleDescription = resources.GetString("txtSuffix.AccessibleDescription");
			this.txtSuffix.AccessibleName = resources.GetString("txtSuffix.AccessibleName");
			this.txtSuffix.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSuffix.Anchor")));
			this.txtSuffix.AutoSize = ((bool)(resources.GetObject("txtSuffix.AutoSize")));
			this.txtSuffix.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSuffix.BackgroundImage")));
			this.txtSuffix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtSuffix.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSuffix.Dock")));
			this.txtSuffix.Enabled = ((bool)(resources.GetObject("txtSuffix.Enabled")));
			this.txtSuffix.Font = ((System.Drawing.Font)(resources.GetObject("txtSuffix.Font")));
			this.txtSuffix.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSuffix.ImeMode")));
			this.txtSuffix.Location = ((System.Drawing.Point)(resources.GetObject("txtSuffix.Location")));
			this.txtSuffix.MaxLength = ((int)(resources.GetObject("txtSuffix.MaxLength")));
			this.txtSuffix.Multiline = ((bool)(resources.GetObject("txtSuffix.Multiline")));
			this.txtSuffix.Name = "txtSuffix";
			this.txtSuffix.PasswordChar = ((char)(resources.GetObject("txtSuffix.PasswordChar")));
			this.txtSuffix.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSuffix.RightToLeft")));
			this.txtSuffix.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSuffix.ScrollBars")));
			this.txtSuffix.Size = ((System.Drawing.Size)(resources.GetObject("txtSuffix.Size")));
			this.txtSuffix.TabIndex = ((int)(resources.GetObject("txtSuffix.TabIndex")));
			this.txtSuffix.Text = resources.GetString("txtSuffix.Text");
			this.txtSuffix.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSuffix.TextAlign")));
			this.txtSuffix.Visible = ((bool)(resources.GetObject("txtSuffix.Visible")));
			this.txtSuffix.WordWrap = ((bool)(resources.GetObject("txtSuffix.WordWrap")));
			// 
			// frmRecalculateRules
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.txtSuffix);
			this.Controls.Add(this.txtHSSU);
			this.Controls.Add(this.txtSection);
			this.Controls.Add(this.txtDEST);
			this.Controls.Add(this.txtORIG);
			this.Controls.Add(this.txtFLNO);
			this.Controls.Add(this.txtALC);
			this.Controls.Add(this.lblSection);
			this.Controls.Add(this.btnALT);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.lblDest);
			this.Controls.Add(this.lblOrig);
			this.Controls.Add(this.lblFlno);
			this.Controls.Add(this.lblTo);
			this.Controls.Add(this.lblFrom);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dtTo);
			this.Controls.Add(this.dtFrom);
			this.Controls.Add(this.pictureBox1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmRecalculateRules";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmRecalculateRules_Closing);
			this.Load += new System.EventHandler(this.frmRecalculateRules_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void frmRecalculateRules_Load(object sender, System.EventArgs e)
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy";// - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			lblFrom.Parent = pictureBox1;
			lblTo.Parent = pictureBox1;
			lblFlno.Parent = pictureBox1;
			lblOrig.Parent = pictureBox1;
			lblDest.Parent = pictureBox1;
			lblSection.Parent = pictureBox1;

		}

		private void btnALT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Status Sections";
			olDlg.currTable = "HSS";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtHSSU.Text = strUrno;
				txtSection.Text = olDlg.tabList.GetColumnValue(sel,1);
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strMessage="";
			strMessage = Check();
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}
		private string  Check()
		{
			string strMessage="";
			string strValue = txtALC.Text;
			IDatabase myDB = UT.GetMemDB();
			if(strValue != "")
			{
				ITable myTab = myDB["ALT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["ALC2"] || strValue == myTab[i]["ALC3"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Airline code <" + strValue + "> does not exist\n";
				}
			}
			strValue = txtORIG.Text;
			if(strValue != "")
			{
				ITable myTab = myDB["APT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Origin <" + strValue + "> does not exist\n";
				}
			}
			strValue = txtDEST.Text;
			if(strValue != "")
			{
				ITable myTab = myDB["APT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Destination <" + strValue + "> does not exist\n";
				}
			}
//			if(dtFrom.Value >= dtTo.Value)
//			{
//				strMessage += "Date From > Date To\n";
//			}
			return strMessage;
		}

		private void frmRecalculateRules_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strMessage = "";
			string strSQL = "";
			strMessage = Check();
			if(this.DialogResult == DialogResult.OK)
			{
				if(strMessage != "")
				{
					e.Cancel = true;
				}
				else
				{
					//Build SQL string
					strSQL = BuildSQLString();
					IUfisComWriter aUfis = UT.GetUfisCom();
					int ilRet = aUfis.CallServer("CFS", "AFTTAB", "", "", strSQL, "230");
					if(ilRet != 0)
					{
						string strERR = aUfis.LastErrorMessage;
						MessageBox.Show(this, strERR);
						//e.Cancel = true;//TO DO remove this
					}
				}
			}
		}
		private string BuildSQLString()
		{
			string strSQL = "";
			string strDateFrom = "";
			string strDateTo = "";
			string strALC = "";
			string strORIG = "";
			string strFLNO = "";
			string strDEST = "";
			string strTmpFlno = "";

			DateTime datFrom = dtFrom.Value;
			DateTime datTo;
			datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 0, 0, 0);
			datTo = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 23, 59, 0);
			strDateFrom = UT.DateTimeToCeda(datFrom);
			strDateTo = UT.DateTimeToCeda(datTo);

			strSQL  = "WHERE ((TIFA BETWEEN '" + strDateFrom +"' AND '" + strDateTo + "') OR";
			strSQL += " (TIFD BETWEEN '" + strDateFrom +"' AND '" + strDateTo + "')) ";

			if(txtALC.Text != "" && txtFLNO.Text != "") //Build the flight number
			{
				if(txtALC.Text.Length == 2)
				{
					strALC = txtALC.Text + " ";
				}
				if(txtALC.Text.Length == 3)
				{
					strALC = txtALC.Text;
				}
				strTmpFlno = txtFLNO.Text;
				if(strTmpFlno.Length < 3)
				{
					strTmpFlno = strTmpFlno.PadLeft(3, '0');
				}
				if(txtSuffix.Text!= "")
				{
					strTmpFlno = strTmpFlno.PadRight(5, ' ') + txtSuffix.Text;
					strFLNO = " FLNO='" + strALC + strTmpFlno + "'";
					strSQL += "AND " + strFLNO;
				}
				else
				{
					strFLNO = " FLNO='" + strALC + strTmpFlno + "'";
					strSQL += "AND " + strFLNO;
				}
			}
			else if(txtALC.Text != "")
			{
				if(txtALC.Text.Length == 2)
				{
					strFLNO = " ALC2='"  + txtALC.Text + "' ";
					strSQL += "AND " + strFLNO;
				}
				if(txtALC.Text.Length == 3)
				{
					strFLNO = " ALC3='" + txtALC.Text + "' ";
					strSQL += "AND " + strFLNO;
				}
			}
			if(txtDEST.Text != "")
			{
				strDEST = " DES3='" + txtDEST.Text + "' ";
				strSQL += "AND " + strDEST;
			}
			if(txtORIG.Text != "")
			{
				strORIG = " ORG3='" + txtORIG.Text + "' ";
				strSQL += "AND " + strORIG;
			}
			if(strSQL != "" && txtHSSU.Text != "")
			{
				strSQL += "|" + txtHSSU.Text;
			}
			return strSQL;
		}
	}
}
