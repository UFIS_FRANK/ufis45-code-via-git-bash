﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Status_Manager.DB;
using Ufis.Status_Manager.Util;

namespace Ufis.Status_Manager.Ctrl
{
    public class CtrlView
    {
        public const string CFC_DEFAULT_VIEW = "<DEFAULT>";

        private DbVcd _dbVcd = null;

        #region Singleton
        private static CtrlView _this = null;
        private CtrlView()
        {
        }

        public static CtrlView GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlView();
                _this._dbVcd = DbVcd.GetInstance();
                _this._dbVcd.LoadDataForUser(CtrlMain.APPLN_ID, CtrlMain.GetUserId());
            }
            return _this;
        }
        #endregion

        /// <summary>
        /// Save View for CFC Filter
        /// </summary>
        /// <param name="setAsDefaultView">Set this view as default view</param>
        /// <param name="viewName"></param>
        /// <param name="stValidFromDt"></param>
        /// <param name="stValidToDt"></param>
        /// <param name="stHSS"></param>
        /// <param name="stALT"></param>
        /// <param name="stNAT"></param>
        /// <returns></returns>
        public bool SaveView(
            bool setAsDefaultView,
            string viewName,
            string stValidFromDt,
            string stValidToDt,
            string stHSS,
            string stALT,
            string stNAT
            )
        {
            DbVcd db = DbVcd.GetInstance();
            bool success = db.SaveViewInfo(viewName,
                string.Format("{0};{1};{2};{3};{4};{5}",
                viewName, 
                stValidFromDt, stValidToDt, 
                stHSS, stALT, stNAT));
            if (setAsDefaultView && success)
            {
                db.SaveDefaultView(viewName);
            }

            return success;
        }

        public string GetDefaultViewName()
        {
            DbVcd db = DbVcd.GetInstance();
            string stDefaultViewName = db.GetDefaultView();
            if (string.IsNullOrEmpty(stDefaultViewName)) stDefaultViewName = CFC_DEFAULT_VIEW;
            return stDefaultViewName;
        }

        /// <summary>
        /// Get All View previously saved by this user
        /// </summary>
        /// <param name="stInfoSeparator">Separator for fields</param>
        /// <returns></returns>
        public List<string> GetAllView(string stInfoSeparator)
        {
            Dictionary<string,string> mapView = DbVcd.GetInstance().GetViewInfo();
            List<string> lsViewInfo = new List<string>();
            foreach (string st in mapView.Values)
            {
                lsViewInfo.Add(st.Replace(";", stInfoSeparator));
            }
            return lsViewInfo;
        }


    }
}
