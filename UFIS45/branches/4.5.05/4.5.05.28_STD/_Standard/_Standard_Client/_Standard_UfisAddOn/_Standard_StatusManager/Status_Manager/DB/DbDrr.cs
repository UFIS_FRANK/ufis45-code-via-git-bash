// DAILY ROSTER RECORD Information 
//   Related to Database
// AUNG MOE : 20080721
//

using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

using Ufis.Status_Manager.Ent;
using Ufis.Status_Manager.DS;

namespace Ufis.Status_Manager.DB
{
    public class DbDrr
    {
        #region data member
        private static DbDrr _this = null;
        private static object _lockDrrTableProp = new object();

        private IDatabase _myDB = null;
        private ITable _drrTable = null;
        #endregion

        #region property member
        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable DrrTable
        {
            get
            {
                _drrTable = MyDb["DRR"];
                if (_drrTable == null)
                {
                    lock (_lockDrrTableProp)
                    {
                        _drrTable = MyDb["DRR"];//Get and check again
                        if (_drrTable == null)
                        {
                            _drrTable = MyDb.Bind("DRR", "DRR",
                                "URNO,FCTC",
                                "10,8",
                                "URNO,FCTC");
                            //_drrTable.UseTrackChg = true;
                            //_drrTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _drrTable;
            }
        }
        #endregion

        public ITable GetDrrTable()
        {
            return DrrTable;
        }

        #region singleton
        private DbDrr()
        {
        }

        public static DbDrr GetInstance()
        {
            if (_this == null) _this = new DbDrr();
            return _this;
        }
        #endregion

        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbDrr:" + msg);
        }

        #region lock table
        public void LockDrrTableForWrite()
        {
            LogMsg("B4 Lock Drr Write");
            DrrTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Drr Write");
        }

        public void ReleaseDrrTableWrite()
        {
            if (DrrTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Drr Write");
                DrrTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Drr Write");
            }
        }

        public void LockDrrTableForRead()
        {
            LogMsg("B4 Lock Drr Read");
            DrrTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Drr Read");
        }

        public void ReleaseDrrTableRead()
        {
            if (DrrTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Drr Read");
                DrrTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Drr Read");
            }
        }
        #endregion

        private void AddDataToDsJob(IRow row, DSJob dsJob)
        {
            DSJob.DRRRow dtRow = dsJob.DRR.NewDRRRow();

            dtRow.URNO = row["URNO"].ToString();
            dtRow.FCTC = row["FCTC"].ToString();
            dsJob.DRR.AddDRRRow(dtRow);
        }

        /// <summary>
        /// Get Drripment Information and load them into passing dsJob
        /// </summary>
        /// <param name="dsJob">load the relevent Drripment info into this dataset</param>
        /// <param name="udrrArr">UDRR array to load</param>
        public void GetDrrInfo(DSJob dsJob, string[] udrrArr)
        {
            dsJob.DRR.Clear();
            int cntArr = udrrArr.Length;
            if (cntArr > 0)
            {
                try
                {
                    LockDrrTableForRead();

                    string[] missingUdrrArr = new string[udrrArr.Length];
                    int cntMissing = 0;
                    for (int idxUdrr = 0; idxUdrr < cntArr; idxUdrr++)
                    {
                        string udrr = udrrArr[idxUdrr];
                        IRow[] rows = DrrTable.RowsByIndexValue("URNO", udrr);
                        if ((rows == null) || (rows.Length < 1))
                        {
                            missingUdrrArr[cntMissing] = udrr;
                            cntMissing++;
                        }
                        else
                        {
                            AddDataToDsJob(rows[0], dsJob);
                        }
                    }

                    if (cntMissing > 0)
                    {
                        AddDrrData("'" + string.Join("','", missingUdrrArr) + "'");
                        for (int i = 0; i < cntMissing; i++)
                        {
                            string udrr = missingUdrrArr[i];
                            IRow[] rows = DrrTable.RowsByIndexValue("URNO", udrr);
                            if ((rows == null) || (rows.Length < 1))
                            {
                            }
                            else
                            {
                                AddDataToDsJob(rows[0], dsJob);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ReleaseDrrTableRead();
                }
            }
        }


        private void AddDrrData(string udrrSt)
        {
            DrrTable.Load(string.Format(" WHERE URNO IN ({0})", udrrSt));
        }

        /// <summary>
        /// Load DRRTAB Data
        /// Note: Existing In memory Data in DRRTAB will be cleared
        /// </summary>
        /// <param name="udrrArrStForSelection">Comma seperated UDRR string array (e.g. Arr[0]==>65132523,65132556,65132745 Arr[0]==>6554324523,651345256,651387685 </param>
        /// <returns>result message</returns>
        public string LoadDrrData(ArrayList udrrArrStForSelection)
        {
            string result = "";

            //Select the DRRTAB records according to the flight

            try
            {
                LockDrrTableForWrite();
                DeleteDrrIndex();
                DrrTable.Clear();

                LogMsg("LoadDRRData: Start");
                DrrTable.TimeFieldsInitiallyInUtc = true;
                DrrTable.TimeFieldsCurrentlyInUtc = true;

                LogMsg("LoadDRRData: b4 load");
                int cntArr = udrrArrStForSelection.Count;
                for (int i = 0; i < cntArr; i++)
                {
                    try
                    {
                        if (udrrArrStForSelection[i].ToString() != "")
                        {
                            AddDrrData(udrrArrStForSelection[i].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDrrData:Err:" + ex.Message);
                    }
                }
                DrrTable.Sort("URNO", true);
                CreateDrrIndex();

                DrrTable.Command("insert", ",IRT,");
                DrrTable.Command("update", ",URT,");
                DrrTable.Command("delete", ",DRT,");
                DrrTable.Command("read", ",GFR,");

                result = "DRRTAB " + DrrTable.Count + " Records loaded";

                LogMsg("DRRTAB <" + DrrTable.Count + ">");
                return result;
            }
            catch (Exception ex)
            {
                LogMsg("LoadDRRData:Err:" + ex.Message);
            }
            finally
            {
                ReleaseDrrTableWrite();
            }
            return result;
        }

        #region table index
        private void CreateDrrIndex()
        {
            DrrTable.CreateIndex("URNO", "URNO");
        }

        private void DeleteDrrIndex()
        {
            try
            {
                DrrTable.DeleteIndex("URNO");
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
