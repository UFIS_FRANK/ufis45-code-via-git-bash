using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using Microsoft.Win32;

using System.Threading;

using Ufis.Status_Manager.Ctrl;
using Ufis.Status_Manager.DS;
using Ufis.Status_Manager.Ent;
using Ufis.Status_Manager.UI;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmRunViewPoint.
	/// </summary>
	public class frmRunViewPoint : System.Windows.Forms.Form
	{
		#region OWN Variabels

		private IDatabase myDB = null;
		private System.Windows.Forms.ImageList imageToolbar;
		private ITable myAFT = null;
		private ITable myTmo = null;
		private ITable myLand = null;
		private ITable myOffbl = null;
		private int currX = -1;
		private frmData omHiddenData;
		private frmMain omParent;
		private int imDefaultTimeFrame = 60;
		private int imImageSize = 56;
		private String strDrawMode = "";
		private String FileName = "";
		private String omViewPoint;
		private string myCurrentPOS = ""; //to optimize the flying infor window ==> for the case it's equal to
		//the predecessor position it will not be updated
		private string strCurrentSelected = "";
		private DE.NavigateToFlight delegateNavigateToFlight = null;
		private DE.ConflicTimerElapsed delegateConflicTimerElapsed = null;
		private DE.UpdateFlightData delegateUpdateFlightData = null;
		private DE.ViewNameChanged	delegateViewNameChanged = null;
		private DE.UTC_LocalTimeChanged delegateUTC_LocalTimeChanged = null;

		private int currY = -1;
		private int currTmoLand_X	= 0;
		private int currTmoLand_Y	= 0;
		private int currOFBL_X		= 0;
		private int currOFBL_Y		= 0;

		#endregion OWN Variabels

        #region Controls

        private System.Windows.Forms.Panel panelToolbar;
		private System.Windows.Forms.PictureBox picToolbar;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnNow;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel panelProperties;
		private System.ComponentModel.IContainer components;

		private frmInfoTable omInfoDlg;
        //private frmInfoTable omInfoRotationDlg;
		private DateTime currTimePoint;
		private System.Windows.Forms.CheckBox cbShowToolWindow;
		private System.Windows.Forms.PictureBox picProperties;
		private System.Windows.Forms.Panel panelTimeBar;
		private System.Windows.Forms.PictureBox picTimeBar;
		private AxTABLib.AxTAB myPosDefs;
		private System.Windows.Forms.ToolTip toolTip2;
		private System.Windows.Forms.Label lblTime;
		private System.Windows.Forms.TextBox txtTime;
		private System.Timers.Timer timerMinutes;
		private System.Windows.Forms.CheckBox cbShowDirectInfo;
		private AxTABLib.AxTAB tabCurrFlights;
		private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.CheckBox cbShowPositionCircle;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ImageList imageSmallAC;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Panel panelTmoLand;
		private System.Windows.Forms.Panel panelOffblocks;
		private System.Windows.Forms.Panel panelWork;
		private System.Windows.Forms.Panel panelArea;
		private System.Windows.Forms.PictureBox picArea;
		private System.Windows.Forms.Label lblToolTip;
		private System.Windows.Forms.CheckBox cbTmoLandView;
		private System.Windows.Forms.CheckBox cbOffblocksView;
		private System.Windows.Forms.ImageList imageDayNight;
		private System.Windows.Forms.ImageList imagePlanets;
		private System.Windows.Forms.PictureBox picTmoLand;
		private System.Windows.Forms.PictureBox picOffblocks;
		private System.Windows.Forms.PictureBox imgDay;
		private System.Windows.Forms.PictureBox imgNight;
		private System.Windows.Forms.CheckBox cbOneChain;
		private System.Windows.Forms.CheckBox cbCyclicUpdate;
		private System.Windows.Forms.CheckBox cbShowRealWord;
        private CheckBox cbxShowJob;
		private System.Windows.Forms.ImageList imageTmoLandOffbl;

        #endregion
        
        public frmRunViewPoint(frmMain opParent, frmData opData, String opViewPoint)
		{
			omParent = opParent; 
			omHiddenData = opData;
			omViewPoint = opViewPoint;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

        delegate void CallBackDispose(bool disposing);


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
            if (this.InvokeRequired)
            {
                CallBackDispose d = new CallBackDispose(Dispose);
                object[] arrObj = new object[1];
                arrObj[0] = disposing;
                this.Invoke(d, arrObj);
            }
            else
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
                try
                {
                    base.Dispose(disposing);
                }
                catch (Exception)
                {
                }
                if (picArea.Image != null)
                {
                    picArea.Image.Dispose();
                }
            }
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRunViewPoint));
            this.panelToolbar = new System.Windows.Forms.Panel();
            this.cbxShowJob = new System.Windows.Forms.CheckBox();
            this.cbShowRealWord = new System.Windows.Forms.CheckBox();
            this.cbOneChain = new System.Windows.Forms.CheckBox();
            this.cbOffblocksView = new System.Windows.Forms.CheckBox();
            this.cbTmoLandView = new System.Windows.Forms.CheckBox();
            this.cbShowPositionCircle = new System.Windows.Forms.CheckBox();
            this.imageToolbar = new System.Windows.Forms.ImageList(this.components);
            this.cbShowDirectInfo = new System.Windows.Forms.CheckBox();
            this.cbShowToolWindow = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.picToolbar = new System.Windows.Forms.PictureBox();
            this.panelTimeBar = new System.Windows.Forms.Panel();
            this.cbCyclicUpdate = new System.Windows.Forms.CheckBox();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.btnNow = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.picTimeBar = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelProperties = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.imageSmallAC = new System.Windows.Forms.ImageList(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabCurrFlights = new AxTABLib.AxTAB();
            this.myPosDefs = new AxTABLib.AxTAB();
            this.picProperties = new System.Windows.Forms.PictureBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.timerMinutes = new System.Timers.Timer();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.panelTmoLand = new System.Windows.Forms.Panel();
            this.imgDay = new System.Windows.Forms.PictureBox();
            this.imgNight = new System.Windows.Forms.PictureBox();
            this.picTmoLand = new System.Windows.Forms.PictureBox();
            this.panelOffblocks = new System.Windows.Forms.Panel();
            this.picOffblocks = new System.Windows.Forms.PictureBox();
            this.panelWork = new System.Windows.Forms.Panel();
            this.panelArea = new System.Windows.Forms.Panel();
            this.lblToolTip = new System.Windows.Forms.Label();
            this.picArea = new System.Windows.Forms.PictureBox();
            this.imageDayNight = new System.Windows.Forms.ImageList(this.components);
            this.imagePlanets = new System.Windows.Forms.ImageList(this.components);
            this.imageTmoLandOffbl = new System.Windows.Forms.ImageList(this.components);
            this.panelToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).BeginInit();
            this.panelTimeBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTimeBar)).BeginInit();
            this.panelProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabCurrFlights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myPosDefs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerMinutes)).BeginInit();
            this.panelTmoLand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTmoLand)).BeginInit();
            this.panelOffblocks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOffblocks)).BeginInit();
            this.panelWork.SuspendLayout();
            this.panelArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picArea)).BeginInit();
            this.SuspendLayout();
            // 
            // panelToolbar
            // 
            this.panelToolbar.Controls.Add(this.cbxShowJob);
            this.panelToolbar.Controls.Add(this.cbShowRealWord);
            this.panelToolbar.Controls.Add(this.cbOneChain);
            this.panelToolbar.Controls.Add(this.cbOffblocksView);
            this.panelToolbar.Controls.Add(this.cbTmoLandView);
            this.panelToolbar.Controls.Add(this.cbShowPositionCircle);
            this.panelToolbar.Controls.Add(this.cbShowDirectInfo);
            this.panelToolbar.Controls.Add(this.cbShowToolWindow);
            this.panelToolbar.Controls.Add(this.btnClose);
            this.panelToolbar.Controls.Add(this.picToolbar);
            this.panelToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelToolbar.Location = new System.Drawing.Point(0, 0);
            this.panelToolbar.Name = "panelToolbar";
            this.panelToolbar.Size = new System.Drawing.Size(912, 28);
            this.panelToolbar.TabIndex = 1;
            // 
            // cbxShowJob
            // 
            this.cbxShowJob.BackColor = System.Drawing.Color.Transparent;
            this.cbxShowJob.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxShowJob.Location = new System.Drawing.Point(753, 8);
            this.cbxShowJob.Name = "cbxShowJob";
            this.cbxShowJob.Size = new System.Drawing.Size(81, 17);
            this.cbxShowJob.TabIndex = 14;
            this.cbxShowJob.Text = "Show Job";
            this.cbxShowJob.UseVisualStyleBackColor = false;
            this.cbxShowJob.CheckedChanged += new System.EventHandler(this.cbxShowJob_CheckedChanged);
            // 
            // cbShowRealWord
            // 
            this.cbShowRealWord.BackColor = System.Drawing.Color.Transparent;
            this.cbShowRealWord.Checked = true;
            this.cbShowRealWord.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowRealWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowRealWord.Location = new System.Drawing.Point(628, 4);
            this.cbShowRealWord.Name = "cbShowRealWord";
            this.cbShowRealWord.Size = new System.Drawing.Size(128, 24);
            this.cbShowRealWord.TabIndex = 13;
            this.cbShowRealWord.Text = "Show Real &World";
            this.cbShowRealWord.UseVisualStyleBackColor = false;
            this.cbShowRealWord.CheckedChanged += new System.EventHandler(this.cbShowRealWord_CheckedChanged);
            // 
            // cbOneChain
            // 
            this.cbOneChain.BackColor = System.Drawing.Color.Transparent;
            this.cbOneChain.Checked = true;
            this.cbOneChain.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOneChain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOneChain.Location = new System.Drawing.Point(540, 4);
            this.cbOneChain.Name = "cbOneChain";
            this.cbOneChain.Size = new System.Drawing.Size(88, 24);
            this.cbOneChain.TabIndex = 12;
            this.cbOneChain.Text = "One Ch&ain";
            this.cbOneChain.UseVisualStyleBackColor = false;
            this.cbOneChain.CheckedChanged += new System.EventHandler(this.cbOneChain_CheckedChanged);
            // 
            // cbOffblocksView
            // 
            this.cbOffblocksView.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbOffblocksView.BackColor = System.Drawing.Color.Transparent;
            this.cbOffblocksView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOffblocksView.Location = new System.Drawing.Point(456, 0);
            this.cbOffblocksView.Name = "cbOffblocksView";
            this.cbOffblocksView.Size = new System.Drawing.Size(76, 28);
            this.cbOffblocksView.TabIndex = 11;
            this.cbOffblocksView.Text = "&Off Blocks";
            this.cbOffblocksView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbOffblocksView.UseVisualStyleBackColor = false;
            this.cbOffblocksView.CheckedChanged += new System.EventHandler(this.cbOffblocksView_CheckedChanged);
            // 
            // cbTmoLandView
            // 
            this.cbTmoLandView.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTmoLandView.BackColor = System.Drawing.Color.Transparent;
            this.cbTmoLandView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTmoLandView.Location = new System.Drawing.Point(380, 0);
            this.cbTmoLandView.Name = "cbTmoLandView";
            this.cbTmoLandView.Size = new System.Drawing.Size(76, 28);
            this.cbTmoLandView.TabIndex = 10;
            this.cbTmoLandView.Text = "&TMO/LAND";
            this.cbTmoLandView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTmoLandView.UseVisualStyleBackColor = false;
            this.cbTmoLandView.CheckedChanged += new System.EventHandler(this.cbTmoLandView_CheckedChanged);
            // 
            // cbShowPositionCircle
            // 
            this.cbShowPositionCircle.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowPositionCircle.BackColor = System.Drawing.Color.Transparent;
            this.cbShowPositionCircle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowPositionCircle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbShowPositionCircle.ImageList = this.imageToolbar;
            this.cbShowPositionCircle.Location = new System.Drawing.Point(276, 0);
            this.cbShowPositionCircle.Name = "cbShowPositionCircle";
            this.cbShowPositionCircle.Size = new System.Drawing.Size(104, 28);
            this.cbShowPositionCircle.TabIndex = 9;
            this.cbShowPositionCircle.Text = "&Position Circles";
            this.cbShowPositionCircle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbShowPositionCircle.UseVisualStyleBackColor = false;
            this.cbShowPositionCircle.CheckedChanged += new System.EventHandler(this.cbShowPositionCircle_CheckedChanged);
            // 
            // imageToolbar
            // 
            this.imageToolbar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageToolbar.ImageStream")));
            this.imageToolbar.TransparentColor = System.Drawing.Color.White;
            this.imageToolbar.Images.SetKeyName(0, "");
            this.imageToolbar.Images.SetKeyName(1, "");
            this.imageToolbar.Images.SetKeyName(2, "");
            this.imageToolbar.Images.SetKeyName(3, "");
            // 
            // cbShowDirectInfo
            // 
            this.cbShowDirectInfo.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowDirectInfo.BackColor = System.Drawing.Color.LightGreen;
            this.cbShowDirectInfo.Checked = true;
            this.cbShowDirectInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowDirectInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowDirectInfo.Location = new System.Drawing.Point(200, 0);
            this.cbShowDirectInfo.Name = "cbShowDirectInfo";
            this.cbShowDirectInfo.Size = new System.Drawing.Size(76, 28);
            this.cbShowDirectInfo.TabIndex = 8;
            this.cbShowDirectInfo.Text = "&Direct Info";
            this.cbShowDirectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbShowDirectInfo.UseVisualStyleBackColor = false;
            this.cbShowDirectInfo.CheckedChanged += new System.EventHandler(this.cbShowDirectInfo_CheckedChanged);
            // 
            // cbShowToolWindow
            // 
            this.cbShowToolWindow.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowToolWindow.BackColor = System.Drawing.Color.Transparent;
            this.cbShowToolWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowToolWindow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbShowToolWindow.ImageIndex = 3;
            this.cbShowToolWindow.ImageList = this.imageToolbar;
            this.cbShowToolWindow.Location = new System.Drawing.Point(84, 0);
            this.cbShowToolWindow.Name = "cbShowToolWindow";
            this.cbShowToolWindow.Size = new System.Drawing.Size(116, 28);
            this.cbShowToolWindow.TabIndex = 7;
            this.cbShowToolWindow.Text = "       &Show Legend";
            this.cbShowToolWindow.UseVisualStyleBackColor = false;
            this.cbShowToolWindow.CheckedChanged += new System.EventHandler(this.cbShowToolWindow_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 1;
            this.btnClose.ImageList = this.imageToolbar;
            this.btnClose.Location = new System.Drawing.Point(0, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "  &Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picToolbar
            // 
            this.picToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picToolbar.Location = new System.Drawing.Point(0, 0);
            this.picToolbar.Name = "picToolbar";
            this.picToolbar.Size = new System.Drawing.Size(912, 28);
            this.picToolbar.TabIndex = 5;
            this.picToolbar.TabStop = false;
            this.picToolbar.Click += new System.EventHandler(this.picToolbar_Click);
            this.picToolbar.Paint += new System.Windows.Forms.PaintEventHandler(this.picToolbar_Paint);
            // 
            // panelTimeBar
            // 
            this.panelTimeBar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTimeBar.Controls.Add(this.cbCyclicUpdate);
            this.panelTimeBar.Controls.Add(this.txtTime);
            this.panelTimeBar.Controls.Add(this.lblTime);
            this.panelTimeBar.Controls.Add(this.btnNow);
            this.panelTimeBar.Controls.Add(this.trackBar1);
            this.panelTimeBar.Controls.Add(this.picTimeBar);
            this.panelTimeBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimeBar.Location = new System.Drawing.Point(0, 587);
            this.panelTimeBar.Name = "panelTimeBar";
            this.panelTimeBar.Size = new System.Drawing.Size(912, 36);
            this.panelTimeBar.TabIndex = 2;
            // 
            // cbCyclicUpdate
            // 
            this.cbCyclicUpdate.BackColor = System.Drawing.Color.Transparent;
            this.cbCyclicUpdate.Checked = true;
            this.cbCyclicUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCyclicUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCyclicUpdate.Location = new System.Drawing.Point(796, 10);
            this.cbCyclicUpdate.Name = "cbCyclicUpdate";
            this.cbCyclicUpdate.Size = new System.Drawing.Size(104, 20);
            this.cbCyclicUpdate.TabIndex = 5;
            this.cbCyclicUpdate.Text = "C&yclic Update";
            this.cbCyclicUpdate.UseVisualStyleBackColor = false;
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(732, 10);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(56, 20);
            this.txtTime.TabIndex = 4;
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Location = new System.Drawing.Point(696, 12);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(36, 16);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Time:";
            // 
            // btnNow
            // 
            this.btnNow.BackColor = System.Drawing.Color.Transparent;
            this.btnNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNow.Location = new System.Drawing.Point(612, 8);
            this.btnNow.Name = "btnNow";
            this.btnNow.Size = new System.Drawing.Size(75, 23);
            this.btnNow.TabIndex = 1;
            this.btnNow.Text = "&Now";
            this.btnNow.UseVisualStyleBackColor = false;
            this.btnNow.Click += new System.EventHandler(this.btnNow_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.trackBar1.LargeChange = 10;
            this.trackBar1.Location = new System.Drawing.Point(4, 8);
            this.trackBar1.Maximum = 1440;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(600, 20);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 120;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseUp);
            // 
            // picTimeBar
            // 
            this.picTimeBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picTimeBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picTimeBar.Location = new System.Drawing.Point(0, 0);
            this.picTimeBar.Name = "picTimeBar";
            this.picTimeBar.Size = new System.Drawing.Size(908, 32);
            this.picTimeBar.TabIndex = 2;
            this.picTimeBar.TabStop = false;
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 100;
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 1;
            this.toolTip1.ReshowDelay = 5000;
            // 
            // panelProperties
            // 
            this.panelProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelProperties.Controls.Add(this.label22);
            this.panelProperties.Controls.Add(this.label20);
            this.panelProperties.Controls.Add(this.label21);
            this.panelProperties.Controls.Add(this.label18);
            this.panelProperties.Controls.Add(this.label19);
            this.panelProperties.Controls.Add(this.label16);
            this.panelProperties.Controls.Add(this.label17);
            this.panelProperties.Controls.Add(this.label15);
            this.panelProperties.Controls.Add(this.label14);
            this.panelProperties.Controls.Add(this.label13);
            this.panelProperties.Controls.Add(this.label12);
            this.panelProperties.Controls.Add(this.label11);
            this.panelProperties.Controls.Add(this.label10);
            this.panelProperties.Controls.Add(this.label9);
            this.panelProperties.Controls.Add(this.label8);
            this.panelProperties.Controls.Add(this.label7);
            this.panelProperties.Controls.Add(this.label6);
            this.panelProperties.Controls.Add(this.label5);
            this.panelProperties.Controls.Add(this.label4);
            this.panelProperties.Controls.Add(this.label3);
            this.panelProperties.Controls.Add(this.label2);
            this.panelProperties.Controls.Add(this.label1);
            this.panelProperties.Controls.Add(this.tabCurrFlights);
            this.panelProperties.Controls.Add(this.myPosDefs);
            this.panelProperties.Controls.Add(this.picProperties);
            this.panelProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelProperties.Location = new System.Drawing.Point(724, 28);
            this.panelProperties.Name = "panelProperties";
            this.panelProperties.Size = new System.Drawing.Size(188, 559);
            this.panelProperties.TabIndex = 3;
            this.panelProperties.Visible = false;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Black;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Location = new System.Drawing.Point(88, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(2, 180);
            this.label22.TabIndex = 24;
            this.label22.Visible = false;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(140, 136);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 12);
            this.label20.TabIndex = 23;
            this.label20.Text = "AIRB";
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ImageIndex = 8;
            this.label21.ImageList = this.imageSmallAC;
            this.label21.Location = new System.Drawing.Point(104, 128);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 32);
            this.label21.TabIndex = 22;
            this.label21.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // imageSmallAC
            // 
            this.imageSmallAC.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageSmallAC.ImageStream")));
            this.imageSmallAC.TransparentColor = System.Drawing.Color.Transparent;
            this.imageSmallAC.Images.SetKeyName(0, "");
            this.imageSmallAC.Images.SetKeyName(1, "");
            this.imageSmallAC.Images.SetKeyName(2, "");
            this.imageSmallAC.Images.SetKeyName(3, "");
            this.imageSmallAC.Images.SetKeyName(4, "");
            this.imageSmallAC.Images.SetKeyName(5, "");
            this.imageSmallAC.Images.SetKeyName(6, "");
            this.imageSmallAC.Images.SetKeyName(7, "");
            this.imageSmallAC.Images.SetKeyName(8, "");
            this.imageSmallAC.Images.SetKeyName(9, "");
            this.imageSmallAC.Images.SetKeyName(10, "");
            this.imageSmallAC.Images.SetKeyName(11, "");
            this.imageSmallAC.Images.SetKeyName(12, "");
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(140, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 12);
            this.label18.TabIndex = 21;
            this.label18.Text = "OffBL";
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ImageIndex = 5;
            this.label19.ImageList = this.imageSmallAC;
            this.label19.Location = new System.Drawing.Point(104, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 32);
            this.label19.TabIndex = 20;
            this.label19.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(140, 72);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 12);
            this.label16.TabIndex = 19;
            this.label16.Text = "ETD";
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ImageIndex = 11;
            this.label17.ImageList = this.imageSmallAC;
            this.label17.Location = new System.Drawing.Point(104, 64);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 32);
            this.label17.TabIndex = 18;
            this.label17.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(140, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 12);
            this.label15.TabIndex = 17;
            this.label15.Text = "STD";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(40, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 12);
            this.label14.TabIndex = 16;
            this.label14.Text = "ONBL";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(40, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 12);
            this.label13.TabIndex = 15;
            this.label13.Text = "LAND";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(40, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 12);
            this.label12.TabIndex = 14;
            this.label12.Text = "TMO";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(40, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "ETA";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(40, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "STA";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(103, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "Departure:";
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ImageIndex = 1;
            this.label8.ImageList = this.imageSmallAC;
            this.label8.Location = new System.Drawing.Point(104, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 32);
            this.label8.TabIndex = 10;
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImageIndex = 5;
            this.label7.ImageList = this.imageSmallAC;
            this.label7.Location = new System.Drawing.Point(2, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 32);
            this.label7.TabIndex = 9;
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImageIndex = 8;
            this.label6.ImageList = this.imageSmallAC;
            this.label6.Location = new System.Drawing.Point(2, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 32);
            this.label6.TabIndex = 8;
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageIndex = 2;
            this.label5.ImageList = this.imageSmallAC;
            this.label5.Location = new System.Drawing.Point(2, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 32);
            this.label5.TabIndex = 7;
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Arrival:";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageIndex = 3;
            this.label3.ImageList = this.imageSmallAC;
            this.label3.Location = new System.Drawing.Point(2, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 32);
            this.label3.TabIndex = 5;
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImageIndex = 2;
            this.label2.ImageList = this.imageSmallAC;
            this.label2.Location = new System.Drawing.Point(2, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 32);
            this.label2.TabIndex = 4;
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Colour Legend:";
            // 
            // tabCurrFlights
            // 
            this.tabCurrFlights.Location = new System.Drawing.Point(0, 416);
            this.tabCurrFlights.Name = "tabCurrFlights";
            this.tabCurrFlights.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabCurrFlights.OcxState")));
            this.tabCurrFlights.Size = new System.Drawing.Size(184, 80);
            this.tabCurrFlights.TabIndex = 2;
            this.tabCurrFlights.Visible = false;
            // 
            // myPosDefs
            // 
            this.myPosDefs.Location = new System.Drawing.Point(0, 324);
            this.myPosDefs.Name = "myPosDefs";
            this.myPosDefs.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myPosDefs.OcxState")));
            this.myPosDefs.Size = new System.Drawing.Size(184, 80);
            this.myPosDefs.TabIndex = 1;
            this.myPosDefs.Visible = false;
            // 
            // picProperties
            // 
            this.picProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picProperties.Location = new System.Drawing.Point(0, 0);
            this.picProperties.Name = "picProperties";
            this.picProperties.Size = new System.Drawing.Size(184, 555);
            this.picProperties.TabIndex = 0;
            this.picProperties.TabStop = false;
            this.picProperties.Paint += new System.Windows.Forms.PaintEventHandler(this.picProperties_Paint);
            // 
            // toolTip2
            // 
            this.toolTip2.ShowAlways = true;
            // 
            // timerMinutes
            // 
            this.timerMinutes.Enabled = true;
            this.timerMinutes.Interval = 60000;
            this.timerMinutes.SynchronizingObject = this;
            this.timerMinutes.Elapsed += new System.Timers.ElapsedEventHandler(this.timerMinutes_Elapsed);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "");
            this.imageList.Images.SetKeyName(1, "");
            this.imageList.Images.SetKeyName(2, "");
            this.imageList.Images.SetKeyName(3, "");
            this.imageList.Images.SetKeyName(4, "");
            this.imageList.Images.SetKeyName(5, "");
            this.imageList.Images.SetKeyName(6, "");
            this.imageList.Images.SetKeyName(7, "");
            this.imageList.Images.SetKeyName(8, "");
            this.imageList.Images.SetKeyName(9, "");
            this.imageList.Images.SetKeyName(10, "");
            this.imageList.Images.SetKeyName(11, "");
            this.imageList.Images.SetKeyName(12, "");
            // 
            // panelTmoLand
            // 
            this.panelTmoLand.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTmoLand.Controls.Add(this.imgDay);
            this.panelTmoLand.Controls.Add(this.imgNight);
            this.panelTmoLand.Controls.Add(this.picTmoLand);
            this.panelTmoLand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTmoLand.Location = new System.Drawing.Point(0, 0);
            this.panelTmoLand.Name = "panelTmoLand";
            this.panelTmoLand.Size = new System.Drawing.Size(724, 62);
            this.panelTmoLand.TabIndex = 6;
            this.panelTmoLand.SizeChanged += new System.EventHandler(this.panelTmoLand_SizeChanged);
            // 
            // imgDay
            // 
            this.imgDay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgDay.BackgroundImage")));
            this.imgDay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgDay.Location = new System.Drawing.Point(484, 0);
            this.imgDay.Name = "imgDay";
            this.imgDay.Size = new System.Drawing.Size(212, 64);
            this.imgDay.TabIndex = 2;
            this.imgDay.TabStop = false;
            this.imgDay.Visible = false;
            // 
            // imgNight
            // 
            this.imgNight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgNight.BackgroundImage")));
            this.imgNight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgNight.Location = new System.Drawing.Point(264, 0);
            this.imgNight.Name = "imgNight";
            this.imgNight.Size = new System.Drawing.Size(212, 64);
            this.imgNight.TabIndex = 3;
            this.imgNight.TabStop = false;
            this.imgNight.Visible = false;
            // 
            // picTmoLand
            // 
            this.picTmoLand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picTmoLand.Location = new System.Drawing.Point(0, 0);
            this.picTmoLand.Name = "picTmoLand";
            this.picTmoLand.Size = new System.Drawing.Size(720, 58);
            this.picTmoLand.TabIndex = 0;
            this.picTmoLand.TabStop = false;
            this.picTmoLand.DoubleClick += new System.EventHandler(this.picTmoLand_DoubleClick);
            this.picTmoLand.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picTmoLand_MouseMove);
            this.picTmoLand.Paint += new System.Windows.Forms.PaintEventHandler(this.picTmoLand_Paint);
            // 
            // panelOffblocks
            // 
            this.panelOffblocks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelOffblocks.Controls.Add(this.picOffblocks);
            this.panelOffblocks.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelOffblocks.Location = new System.Drawing.Point(0, 497);
            this.panelOffblocks.Name = "panelOffblocks";
            this.panelOffblocks.Size = new System.Drawing.Size(724, 62);
            this.panelOffblocks.TabIndex = 7;
            // 
            // picOffblocks
            // 
            this.picOffblocks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picOffblocks.Location = new System.Drawing.Point(0, 0);
            this.picOffblocks.Name = "picOffblocks";
            this.picOffblocks.Size = new System.Drawing.Size(720, 58);
            this.picOffblocks.TabIndex = 0;
            this.picOffblocks.TabStop = false;
            this.picOffblocks.DoubleClick += new System.EventHandler(this.picOffblocks_DoubleClick);
            this.picOffblocks.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picOffblocks_MouseMove);
            this.picOffblocks.Paint += new System.Windows.Forms.PaintEventHandler(this.picOffblocks_Paint);
            this.picOffblocks.SizeChanged += new System.EventHandler(this.picOffblocks_SizeChanged);
            // 
            // panelWork
            // 
            this.panelWork.Controls.Add(this.panelArea);
            this.panelWork.Controls.Add(this.panelTmoLand);
            this.panelWork.Controls.Add(this.panelOffblocks);
            this.panelWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWork.Location = new System.Drawing.Point(0, 28);
            this.panelWork.Name = "panelWork";
            this.panelWork.Size = new System.Drawing.Size(724, 559);
            this.panelWork.TabIndex = 5;
            // 
            // panelArea
            // 
            this.panelArea.AutoScroll = true;
            this.panelArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelArea.Controls.Add(this.lblToolTip);
            this.panelArea.Controls.Add(this.picArea);
            this.panelArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelArea.Location = new System.Drawing.Point(0, 62);
            this.panelArea.Name = "panelArea";
            this.panelArea.Size = new System.Drawing.Size(724, 435);
            this.panelArea.TabIndex = 8;
            // 
            // lblToolTip
            // 
            this.lblToolTip.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.lblToolTip.Location = new System.Drawing.Point(252, 36);
            this.lblToolTip.Name = "lblToolTip";
            this.lblToolTip.Size = new System.Drawing.Size(100, 23);
            this.lblToolTip.TabIndex = 1;
            this.lblToolTip.Text = "Tooltip";
            this.lblToolTip.Visible = false;
            // 
            // picArea
            // 
            this.picArea.Location = new System.Drawing.Point(0, 0);
            this.picArea.Name = "picArea";
            this.picArea.Size = new System.Drawing.Size(100, 50);
            this.picArea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picArea.TabIndex = 0;
            this.picArea.TabStop = false;
            this.picArea.DoubleClick += new System.EventHandler(this.picArea_DoubleClick);
            this.picArea.MouseLeave += new System.EventHandler(this.picArea_MouseLeave);
            this.picArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picArea_MouseMove);
            this.picArea.Click += new System.EventHandler(this.picArea_Click);
            this.picArea.Paint += new System.Windows.Forms.PaintEventHandler(this.picArea_Paint);
            // 
            // imageDayNight
            // 
            this.imageDayNight.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageDayNight.ImageStream")));
            this.imageDayNight.TransparentColor = System.Drawing.Color.White;
            this.imageDayNight.Images.SetKeyName(0, "");
            this.imageDayNight.Images.SetKeyName(1, "");
            // 
            // imagePlanets
            // 
            this.imagePlanets.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imagePlanets.ImageStream")));
            this.imagePlanets.TransparentColor = System.Drawing.Color.White;
            this.imagePlanets.Images.SetKeyName(0, "");
            this.imagePlanets.Images.SetKeyName(1, "");
            // 
            // imageTmoLandOffbl
            // 
            this.imageTmoLandOffbl.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageTmoLandOffbl.ImageStream")));
            this.imageTmoLandOffbl.TransparentColor = System.Drawing.Color.White;
            this.imageTmoLandOffbl.Images.SetKeyName(0, "");
            this.imageTmoLandOffbl.Images.SetKeyName(1, "");
            // 
            // frmRunViewPoint
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(912, 623);
            this.Controls.Add(this.panelWork);
            this.Controls.Add(this.panelProperties);
            this.Controls.Add(this.panelTimeBar);
            this.Controls.Add(this.panelToolbar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRunViewPoint";
            this.Text = "Viewpoint: <x>";
            this.Load += new System.EventHandler(this.frmRunViewPoint_Load);
            this.Closed += new System.EventHandler(this.frmRunViewPoint_Closed);
            this.Shown += new System.EventHandler(this.frmRunViewPoint_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRunViewPoint_FormClosed);
            this.panelToolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).EndInit();
            this.panelTimeBar.ResumeLayout(false);
            this.panelTimeBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTimeBar)).EndInit();
            this.panelProperties.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabCurrFlights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myPosDefs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerMinutes)).EndInit();
            this.panelTmoLand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTmoLand)).EndInit();
            this.panelOffblocks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picOffblocks)).EndInit();
            this.panelWork.ResumeLayout(false);
            this.panelArea.ResumeLayout(false);
            this.panelArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picArea)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void frmRunViewPoint_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			myAFT = myDB["AFT"];

			myTmo =  myDB.Bind("TMO", "", "KEY,FLNO,TIME,TIS,X,Y", "10,10,1,10,10", "");
			myLand =  myDB.Bind("LAND", "", "KEY,FLNO,TIME,TIS,X,Y", "10,10,1,10,10", "");
			myOffbl =  myDB.Bind("OFFBL", "", "KEY,FLNO,TIME,TIS,X,Y", "10,10,1,10,10", "");

			//			myTmo.CreateIndex("KEY", "KEY");
			//			myLand.CreateIndex("KEY", "KEY");
			//			myOffbl.CreateIndex("KEY", "KEY");
			int i=0;
			this.Text = "Viewpoint: <" + omViewPoint + ">";
//			this.Top = omParent.Bottom;
//			this.Left = 0;
//			this.Width = Screen.PrimaryScreen.WorkingArea.Width;
//			this.Height = Screen.PrimaryScreen.WorkingArea.Height - omParent.Bottom;
			myPosDefs.ResetContent();
			myPosDefs.HeaderString = "POS,X,Y,ALIGN,ROTATE,TYPE";
			myPosDefs.LogicalFieldList = "POS,X,Y,ALIGN,ROTATE,TYPE";
			myPosDefs.HeaderLengthString = "60,60,60,60,60,60";
			myPosDefs.EnableHeaderSizing(true);
			myPosDefs.AutoSizeByHeader = true;
			myPosDefs.ShowHorzScroller(true);

			ReadRegistry();

			delegateNavigateToFlight = new DE.NavigateToFlight(DE_OnNavigateToFlight);
			DE.OnNavigateToFlight += delegateNavigateToFlight;
			delegateConflicTimerElapsed = new DE.ConflicTimerElapsed(DE_OnConflicTimerElapsed);
			DE.OnConflicTimerElapsed += delegateConflicTimerElapsed;
			delegateUpdateFlightData = new DE.UpdateFlightData(DE_OnUpdateFlightData);
			DE.OnUpdateFlightData += delegateUpdateFlightData;
			delegateViewNameChanged = new DE.ViewNameChanged(DE_OnViewNameChanged);
			DE.OnViewNameChanged += delegateViewNameChanged;
			delegateUTC_LocalTimeChanged = new DE.UTC_LocalTimeChanged (DE_OnUTC_LocalTimeChanged);
			DE.OnUTC_LocalTimeChanged += delegateUTC_LocalTimeChanged;

			btnClose.Parent = picToolbar;
			cbShowPositionCircle.Parent = picToolbar;
			cbShowToolWindow.Parent = picToolbar;
			cbShowDirectInfo.Parent = picToolbar;
			cbTmoLandView.Parent = picToolbar;
			cbOffblocksView.Parent = picToolbar;
			cbShowRealWord.Parent = picToolbar;
            cbxShowJob.Parent = picToolbar;
			cbOneChain.Parent = picToolbar;
			panelOffblocks.Visible = false;
			panelTmoLand.Visible = false;

			omInfoDlg = new frmInfoTable();
			omInfoDlg.TopMost=true;
			omInfoDlg.HideMe();


            //omInfoRotationDlg = new frmInfoTable();
            //omInfoRotationDlg.TopMost=true;
            //omInfoRotationDlg.Hide();

			lblTime.Parent = picTimeBar;
			cbCyclicUpdate.Parent = picTimeBar;

			label1.Parent = picProperties;
			label4.Parent = picProperties;
			label9.Parent = picProperties;
			label10.Parent = picProperties;
			label11.Parent = picProperties;
			label12.Parent = picProperties;
			label13.Parent = picProperties;
			label14.Parent = picProperties;
			label15.Parent = picProperties;
			label16.Parent = picProperties;
			label18.Parent = picProperties;
			label20.Parent = picProperties;

			String strFile="";
			String strGrid="";
			for(i = 0; i < omHiddenData.tabVPD.GetLineCount(); i++)
			{
				// 0    1    2    3     4         5        6         7
				//URNO,VIEW,FILE,GRID,DRAWMODE,IMAGESIZE,DEFTIMEFR,HOPO
				if(omHiddenData.tabVPD.GetFieldValue(i, "VIEW") == omViewPoint)
				{
					strFile = omHiddenData.tabVPD.GetFieldValue(i, "FILE");
					strGrid = omHiddenData.tabVPD.GetFieldValue(i, "GRID");
					String strDefTimeframe = omHiddenData.tabVPD.GetFieldValue(i, "DEFTIMEFR");
					String strImageSize = omHiddenData.tabVPD.GetFieldValue(i, "IMAGESIZE");

					imImageSize = Convert.ToInt32(strImageSize);
					imDefaultTimeFrame = Convert.ToInt32(strDefTimeframe);
					strDrawMode = omHiddenData.tabVPD.GetFieldValue(i, "DRAWMODE");
					i = omHiddenData.tabVPD.GetLineCount();
				}
			}
			if(strFile != "")
			{
				try
				{
					picArea.Image = new Bitmap(strFile);
					FileName = strFile;
				}
				catch(Exception err)
				{
					string strMessage = "";
					strMessage += err.Message.ToString() + "\n\n";
					strMessage += "Bitmap file cannot be opened!!\n";
					strMessage += strFile + "\n\n";
					strMessage += "Please copy the airport bitmaps to the correct location!!!\n";
					MessageBox.Show(this.Owner, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //AM 20080422 - Use 'this.Owner' instead of 'this' - To show the message in Owner Screen
					this.Close();
					return;
				}
			}
			for( i = 0; i < omHiddenData.tabVPL.GetLineCount(); i++)
			{
				// 0    1    2  3 4   5     6      7   8
				//URNO,VIEW,POS,X,Y,ALIGN,ROTATE,TYPE,HOPO
				if(omHiddenData.tabVPL.GetFieldValue(i, "VIEW") == omViewPoint)
				{
					String strValues = omHiddenData.tabVPL.GetFieldValues(i, "POS,X,Y,ALIGN,ROTATE,TYPE");
					myPosDefs.InsertTextLine(strValues, true);
				}
			}
			//Load the position list box
			picArea.Invalidate();
			myPosDefs.IndexCreate("POS", 0);
			myPosDefs.SetInternalLineBuffer(true);

			ScrollToNow();
            CtrlJob ctrlJob = CtrlJob.GetInstance();
            if (ctrlJob.AllowShowJob)
            {
                cbxShowJob.Checked = ctrlJob.ShowJob;
                cbxShowJob.Visible = true;
            }
            else
            {
                cbxShowJob.Visible = false;
            }

		}

        private void PrepareJobInfoButton()
        {
            CtrlJob ctrlJob = CtrlJob.GetInstance();
            if (ctrlJob.AllowShowJob)
            {
                cbxShowJob.Visible = true;
                cbxShowJob.Checked = CtrlJob.GetInstance().ShowJob;
            }
            else
            {
                cbxShowJob.Visible = false;
            }
        }

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\STAT_MGR_VIEWPOINT_" + omViewPoint;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}

		}
		private void WriteRegistry()
		{
			string theKey = "Software\\StatusManager\\STAT_MGR_VIEWPOINT_" + omViewPoint;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);

			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				theKey = "STAT_MGR_VIEWPOINT_" + omViewPoint;
				rk.CreateSubKey(theKey);
				rk.Close();
				theKey = "Software\\StatusManager\\STAT_MGR_VIEWPOINT_" + omViewPoint;
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

		private void cbShowToolWindow_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbShowToolWindow.Checked == true)
			{
				panelProperties.Visible = true;
				cbShowToolWindow.BackColor = Color.LightGreen;
			}
			else
			{
				panelProperties.Visible = false;
				cbShowToolWindow.BackColor = Color.Transparent;
			}
		}

		private void picToolbar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picToolbar, Color.WhiteSmoke, Color.Gray);		
		}

		private void picProperties_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picProperties, Color.WhiteSmoke, Color.Gray);		
		}

		private void picArea_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{            
			String strPos="";
			int x=0;
			int y=0;
			Point p;
			int i = 0;
			bool blFound = false;
			bool isGate = false;
			Rectangle myR;
			//frmInfoTable omInfoDlg = null;

			currX = -1;
			currY = -1;
			myR = new Rectangle(e.X - (imImageSize/2), e.Y - (imImageSize/2), imImageSize, imImageSize);
			for( i = 0; i < myPosDefs.GetLineCount(); i++)
			{
				isGate = false;
				x = Convert.ToInt32(myPosDefs.GetFieldValue(i, "X"));
				y = Convert.ToInt32(myPosDefs.GetFieldValue(i, "Y"));
				p = new Point(x, y);
				if( myR.Contains(p) == true)
				{
					if(myPosDefs.GetFieldValue(i, "TYPE") == "GATE")
					{
						isGate = true;
					}
					strPos = myPosDefs.GetFieldValue(i, "POS");
					i = myPosDefs.GetLineCount();
					blFound = true;
					currX = e.X;
					currY = e.Y;
				}
			}
			x = this.Left + this.Width;
			y = this.Top + this.Height;
			if((strPos == myCurrentPOS) && (myCurrentPOS != ""))
			{
				if(MousePosition.Y - (omInfoDlg.Height/2) < 0)
				{
					omInfoDlg.Top = 0;
				}
				else
				{
					omInfoDlg.Top = MousePosition.Y - (omInfoDlg.Height/2);
				}
				
				if((MousePosition.X + 10 + omInfoDlg.Width) <= x)
				{
					omInfoDlg.Left = MousePosition.X + 10;
				}
				else
				{
					omInfoDlg.Left = MousePosition.X - 10 - omInfoDlg.Width;
				}
				omInfoDlg.Show();
				//!!!!! leave the function
				return; //Do nothing because the position
			}
			if(blFound == false)
			{
				lblToolTip.Visible = false;
			}
			if(cbShowDirectInfo.Checked == true)
			{
			
				int llLine=-1;
                if (strPos != "")
                {
                    for (int j = 0; j < tabCurrFlights.GetLineCount(); j++)
                    {
                        if (isGate == false)
                        {
                            if (tabCurrFlights.GetFieldValue(j, "POS") == strPos)
                            {
                                llLine = j;
                                myCurrentPOS = strPos;
                            }
                        }
                        else
                        {
                            if (tabCurrFlights.GetFieldValue(j, "GAT") == strPos)
                            {
                                llLine = j;
                                myCurrentPOS = strPos;
                            }
                        }
                    }
                }
                else
                {
                    omInfoDlg.HideMe();
                    //omInfoRotationDlg.HideMe();
                    myCurrentPOS = "";
                }
				if(llLine > -1)
                {

                    try
                    {
                        //myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                        #region Get Aft Info
                        //Locate the info dialog
                        omInfoDlg.Top = MousePosition.Y - (omInfoDlg.Height / 2);
                        if ((MousePosition.X + 10 + omInfoDlg.Width) <= x)
                        {
                            omInfoDlg.Left = MousePosition.X + 10;
                        }
                        else
                        {
                            omInfoDlg.Left = MousePosition.X - 10 - omInfoDlg.Width;
                        }
                        bool hasFlight = omInfoDlg.DisplayFlightInfoScreen(tabCurrFlights.GetFieldValue(llLine, "RKEY"));//AM 20080319
                        if (!hasFlight) myCurrentPOS = "";//AM 20080319
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LogExceptionErr(ex, "Error Displaying Flight Information.");
                    }
                    finally
                    {
                        //if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                    }
                }
				else
				{
                    omInfoDlg.HideMe();
					myCurrentPOS = "";
				}
			}
			else 
			{
                omInfoDlg.HideMe();
				myCurrentPOS = "";
			}

		}

		private void trackBar1_Scroll(object sender, System.EventArgs e)
		{
			int ilPos = trackBar1.Value;
			DateTime olNow = CFC.GetUTC();
			DateTime olStart = new DateTime(olNow.Year, olNow.Month, olNow.Day, 0, 0, 0);
			olStart = olStart + new TimeSpan(0, ilPos, 0);
			toolTip2.SetToolTip(trackBar1, olStart.ToShortTimeString());
			txtTime.Text = olStart.ToShortTimeString();
			currTimePoint = olStart;
		}

		private void btnNow_Click(object sender, System.EventArgs e)
		{
			ScrollToNow();
		}

        delegate void CallbackMethod0();
 

		private void ScrollToNow()
		{
            if (this.InvokeRequired)
            {//AM 20080728 - Make as thread safe
                CallbackMethod0 d = new CallbackMethod0(ScrollToNow);
                this.Invoke(d);
            }
            else
            {
                DateTime olNow;
                if (UT.IsTimeInUtc == true)
                {
                    olNow = CFC.GetUTC();
                }
                else
                {
                    olNow = DateTime.Now;
                }
                txtTime.Text = olNow.ToShortTimeString();
                DateTime datStart;
                datStart = new DateTime(olNow.Year, olNow.Month, olNow.Day, 0, 0, 0);
                TimeSpan TS = olNow.Subtract(datStart);
                trackBar1.Value = (int)TS.TotalMinutes;
                currTimePoint = olNow;
                RefillFlights();
            }
		}

		private void timerMinutes_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			//ScrollToNow();
		}

		private void picArea_MouseLeave(object sender, System.EventArgs e)
		{
            omInfoDlg.HideMe();
            myCurrentPOS = "";
		}

		private void picToolbar_Click(object sender, System.EventArgs e)
		{
		
		}

		private void cbShowDirectInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbShowDirectInfo.Checked == true)
			{
				cbShowDirectInfo.BackColor = Color.LightGreen;
			}
			else
			{
				cbShowDirectInfo.BackColor = Color.Transparent;
			}
		}

		private void picArea_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			int ilCircleSize = (int)(imImageSize*.66);
			Pen penAqua = new Pen(Brushes.LightGray, 2);
			Pen penRed = new Pen(Brushes.Red, 4);
			int i=0;
			if(cbShowPositionCircle.Checked == true)
			{
				for(i = 0; i < myPosDefs.GetLineCount(); i++)
				{
					String olPos = myPosDefs.GetFieldValue(i, "POS");
					int x = -1;
					int y = -1;
					String strX = myPosDefs.GetFieldValue(i, "X");
					String strY = myPosDefs.GetFieldValue(i, "Y");
					String strAlign = myPosDefs.GetFieldValue(i, "ALIGN");
					if(strX != "" && strY != "")
					{
						x = Convert.ToInt32(strX);
						y = Convert.ToInt32(strY);
						g.DrawEllipse(penAqua, x-(ilCircleSize/2), y-(ilCircleSize/2), ilCircleSize, ilCircleSize);
					}
				}
			}
			for( i = 0; i < myPosDefs.GetLineCount(); i++)
			{
				int llLine=0;
				PointF pointText=new PointF(-100,-100);
				int myFontSize = 10;
				Font olFont;
                int flCnt = 0;
                int lineSpace = myFontSize + 2;
                olFont = new Font("Arial", imImageSize / 6, FontStyle.Bold);
                //olFont = new Font("Courier New", imImageSize/5, FontStyle.Bold);
                ArrayList alArr = new ArrayList();
                ArrayList alDep = new ArrayList();
                ArrayList alOth = new ArrayList();
                
				//myPosDefs.HeaderString = "POS,X,Y,ALIGN,ROTATE,TYPE";
				string strGate = "";
				bool isDouble = false;
				if(myPosDefs.GetFieldValue(i, "TYPE") == "GATE")
				{
					strGate = myPosDefs.GetFieldValue(i, "POS");
					if(strGate != "")
					{
						tabCurrFlights.GetLinesByIndexValue("GAT", strGate, 0);
						llLine = tabCurrFlights.GetNextResultLine();
					}
				}
				else
				{
					string strCPos = myPosDefs.GetFieldValue(i, "POS");
					if(strCPos != "")
					{
						tabCurrFlights.GetLinesByIndexValue("POS", strCPos, 0);
						llLine = tabCurrFlights.GetNextResultLine();
					}
                }

                int x = -1;
                int y = -1;
                String strX = myPosDefs.GetFieldValue(i, "X");
                String strY = myPosDefs.GetFieldValue(i, "Y");
                string strRotate = myPosDefs.GetFieldValue(i, "ROTATE");
                String strAlign = myPosDefs.GetFieldValue(i, "ALIGN");
                x = Convert.ToInt32(strX);
                y = Convert.ToInt32(strY);

                int iw = imImageSize;
                float iTextSize = 0;
                int ih = iw;
                int ix = x - (iw / 2);
                int iy = y - (ih / 2);
                int cx = x + (iw / 2);
                int cy = y + (ih / 2);
                int nx = (iw / 2) + ix;
                int ny = (ih / 2) + iy;

                #region Process for a position/gate
                while (llLine > -1)
				{
                    flCnt++;
					//tabCurrFlights
					//myPosDefs.GetLinesByIndexValue("POS", tabCurrFlights.GetFieldValue(i, "POS"), 0);
					if(tabCurrFlights.GetFieldValue(llLine, "DOUBLE") == "TRUE")
						isDouble = true;
                    
					string strFlightUrno = tabCurrFlights.GetFieldValue(llLine, "URNO");
					String strAdid  = tabCurrFlights.GetFieldValue(llLine, "ADID");
					String strFlno  = tabCurrFlights.GetFieldValue(llLine, "FLNO");
					String strTisa  = tabCurrFlights.GetFieldValue(llLine, "TISA");
					String strTisd  = tabCurrFlights.GetFieldValue(llLine, "TISD");

                    //AM 20080303 - To Sort the Flight No. :Start 
                    String strTifa = tabCurrFlights.GetFieldValue(llLine, "TIFA");
                    String strTifd = tabCurrFlights.GetFieldValue(llLine, "TIFD");
                    string strVal = strAdid + "-" + strFlno;
                    if (strAdid == "A")
                    {
                        //MessageBox.Show("Flight " + strVal + ": " + strTifa);
                        alArr.Add(new ShowString(strTifa, strVal));
                    }
                    else if (strAdid == "B")
                    {
                        //MessageBox.Show("Flight " + strVal + ": " + strTifd);
                        alDep.Add(new ShowString(strTifd, strVal));
                    }
                    else
                    {
                        //MessageBox.Show("Flight " + strVal + ": " + strTifd);
                        alOth.Add(new ShowString(strTifd, strVal));
                    }
                    //AM 20080303 - To Sort the Flight No. :Finish

					llLine = myPosDefs.GetNextResultLine();
					try
					{
                        //int x = -1;
                        //int y = -1;
                        //String strX = myPosDefs.GetFieldValue(i, "X");
                        //String strY = myPosDefs.GetFieldValue(i, "Y");
                        //string strRotate = myPosDefs.GetFieldValue(i, "ROTATE");
                        //String strAlign = myPosDefs.GetFieldValue(i, "ALIGN");
                        //x = Convert.ToInt32(strX);
                        //y = Convert.ToInt32(strY);

                        //int iw = imImageSize;
                        //float iTextSize = 0;
                        //int ih = iw;
                        //int ix = x - (iw/2);
                        //int iy = y - (ih/2);
                        //int cx = x + (iw/2);
                        //int cy = y + (ih/2);
                        //int nx = (iw/2)+ix;
                        //int ny = (ih/2)+iy;
						Point [] myPoints;
						
						Rectangle myR = new Rectangle(x - (iw/2), y - (ih/2), iw, ih);
						int idxImage=0;
						if(strFlightUrno == strCurrentSelected)
						{
							idxImage = 11;
						} 
						else if(strAdid == "A")
						{
							idxImage = GetTisaColor(strTisa);
						}
						else
						{
							idxImage = GetTisdColor(strTisd);
						}
						Image im = (Image)imageList.Images[idxImage];
									
                        //pointText = new PointF(-100, -100);
						iTextSize = g.MeasureString(strVal + " ", olFont).Width;
                        
						switch(strAlign)
						{
                            case "0"://225�
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x - (iw / 2) - (iTextSize), (float)y - iw / 2);//-(myFontSize/2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(nx,iy+ih+(ih/6)),
																   new Point(ix-(iw/6), ny),
																   new Point(ix+iw+(iw/6), ny)};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(nx,           iy-(ih/6)),
																   new Point(ix+iw+(iw/6), ny),
																   new Point(ix-(iw/6),    ny)};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "1"://270�  |
                                //           \/
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x - (iTextSize / 2) - (myFontSize), (float)y - (iw / 2) - myFontSize - (myFontSize / 2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(myR.Left, myR.Bottom),
																   new Point(myR.Left, myR.Top),
																   new Point(myR.Right,  myR.Bottom)};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(myR.Right, myR.Top),
																   new Point(myR.Right, myR.Bottom),
																   new Point(myR.Left,  myR.Top)};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "2":// 315�
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x + (iw / 2) + (myFontSize / 2), (float)y - (iw / 2) - (myFontSize / 2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(ix-(iw/6), ny),
																   new Point(nx,        iy-(ih/6)),
																   new Point(nx,        iy+ih+(ih/6))};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(ix+iw+(iw/6), ny),
																   new Point(nx,           iy+ih+(ih/6)),
																   new Point(nx,           iy-(ih/6))};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "3"://0� <---
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x + (iw / 2), (float)y - (myFontSize / 2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        g.DrawImage(im, myR);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(myR.Right, myR.Bottom),
																   new Point(myR.Left, myR.Bottom),
																   new Point(myR.Right,  myR.Top)};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "4": //45�
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x + (iw/2) + (myFontSize/2), (float)y-iw/2);//-(myFontSize/2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    pointText = new PointF((float)x + (iw / 2) - (iTextSize / 2) + myFontSize, (float)y + (iw / 2) - (myFontSize / 2));//-15);
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(nx,           iy-(ih/6)),
																   new Point(ix+iw+(iw/6), ny),
																   new Point(ix-(iw/6),    ny)};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(nx,iy+ih+(ih/6)),
																   new Point(ix-(iw/6), ny),
																   new Point(ix+iw+(iw/6), ny)};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "5"://90�
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x - (iTextSize / 2), (float)y + (iw / 2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(myR.Right, myR.Top),
																   new Point(myR.Right, myR.Bottom),
																   new Point(myR.Left,  myR.Top)};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(myR.Left, myR.Bottom),
																   new Point(myR.Left, myR.Top),
																   new Point(myR.Right,  myR.Bottom)};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "6"://135�
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x - (iw / 2) - (iTextSize), (float)y + (iw / 2) - myFontSize);//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(ix+iw+(iw/6), ny),
																   new Point(nx,           iy+ih+(ih/6)),
																   new Point(nx,           iy-(ih/6))};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        myPoints = new Point[] {new Point(ix-(iw/6), ny),
																   new Point(nx,        iy-(ih/6)),
																   new Point(nx,        iy+ih+(ih/6))};
                                        g.DrawImage(im, myPoints);
                                    }
                                }
                                break;
                            case "7"://180�  --->
                                if (flCnt<2)
                                {
                                    pointText = new PointF((float)x - (iw / 2) - (iTextSize), (float)y - (myFontSize / 2));//-15);
                                }
                                if (isDouble == false)
                                {
                                    if (strAdid == "A" || strRotate != "J")
                                    {
                                        myPoints = new Point[] {new Point(myR.Right, myR.Bottom),
																   new Point(myR.Left, myR.Bottom),
																   new Point(myR.Right,  myR.Top)};
                                        g.DrawImage(im, myPoints);
                                    }
                                    else
                                    {
                                        g.DrawImage(im, myR);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
						if(isDouble == true)
						{
							g.FillRectangle(Brushes.Blue, myR);
						}
                        
                        
                        //g.DrawString(strFlno, olFont, Brushes.Blue, pointText);
						//Random olR = new Random();
						//int iR = olR.Next(5);
						//if(iR == 1)
						int CflCount = 0;
						int AckCount = 0;
						int TotalCount = 0;
						int OSTCount = 0;
                        //CFC.HasFlightStatusConflicts(strFlightUrno, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
                        CtrlCfl.GetInstance().GetConflictCount(strFlightUrno, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
						if(CflCount > 0)
						{
							g.FillEllipse(Brushes.Red, x-5, y-5, 10,10);
						}
						else if(AckCount > 0)
						{
							g.FillEllipse(Brushes.Orange, x-5, y-5, 10,10);
						} 
					}
					catch(System.Exception err)
					{
						MessageBox.Show(err.Message);
					}
					llLine = tabCurrFlights.GetNextResultLine();
				}//While End. Processing for a Position/Gate
                #endregion
                //if (alArr.Count > 0 || alDep.Count > 0 || alOth.Count > 0)
                //{
                //    MessageBox.Show(alArr.Count + "," + alDep.Count + "," + alOth.Count);
                //}
                int totCntToShow = alArr.Count + alDep.Count + alOth.Count;

                //MessageBox.Show("AlArr Count : " + cntToShow);
                PointF pointTextToShow;
                if (strAlign == "1")//270Degree
                {
                    pointTextToShow = new PointF(pointText.X, pointText.Y - (flCnt * lineSpace));
                }
                else if (strAlign == "5")//90Degree
                {
                    pointTextToShow = new PointF(pointText.X, pointText.Y - (lineSpace));
                }
                else
                {
                    pointTextToShow = new PointF(pointText.X, pointText.Y - (((((float)flCnt)/2) + 1) * lineSpace));
                }
                
                alArr.Sort(new AscSortShowString());
                int cntToShow = alArr.Count; 
                for (int iToShow = 0; iToShow < cntToShow; iToShow++)
                {
                    pointTextToShow.Y += lineSpace;
                    g.DrawString(((ShowString)alArr[iToShow]).StToShow, olFont, Brushes.Blue, pointTextToShow);
                    //MessageBox.Show("ArrShow: " + ((ShowString)alArr[iToShow]).StToShow + " : " + pointTextToShow.X + ", " + pointTextToShow.Y);
                }

                cntToShow = alDep.Count;
                //MessageBox.Show("AlDep Count : " + cntToShow);
                //pointTextToShow = new PointF(ix, iy + imImageSize - lineSpace);
                alDep.Sort(new AscSortShowString());
                for (int iToShow = 0; iToShow < cntToShow; iToShow++)
                {
                    pointTextToShow.Y += lineSpace;
                    g.DrawString(((ShowString)alDep[iToShow]).StToShow, olFont, Brushes.Blue, pointTextToShow);
                    //MessageBox.Show("DepShow: " + ((ShowString)alDep[iToShow]).StToShow + " : " + pointTextToShow.X + ", " + pointTextToShow.Y);
                }

                cntToShow = alOth.Count;
                //MessageBox.Show("AlOth Count : " + cntToShow);
                alOth.Sort(new AscSortShowString());
                for (int iToShow = 0; iToShow < cntToShow; iToShow++)
                {
                    pointTextToShow.Y += lineSpace;
                    g.DrawString(((ShowString)alOth[iToShow]).StToShow, olFont, Brushes.Blue, pointTextToShow);
                    //MessageBox.Show("OthShow: " + ((ShowString)alOth[iToShow]).StToShow + " : " + pointTextToShow.X + ", " + pointTextToShow.Y);
                }
			}//For End. Processing for all positions and gates
			penAqua.Dispose();
			penRed.Dispose();
		}//END void picArea_Paint(..)

        private void RefillFlights()
        {
            if (this.InvokeRequired)
            {//AM 20080728 - Make as thread safe
                CallbackMethod0 d = new CallbackMethod0(RefillFlights);
                this.Invoke(d);
            }
            else
            {
                //Schedule position fields
                DateTime refTimeArr;
                DateTime refTimeDep;
                DateTime PABS;
                DateTime PAES;
                DateTime PDBS;
                DateTime PDES;
                //Actual position fields
                DateTime PABA;
                DateTime PAEA;
                DateTime PDBA;
                DateTime PDEA;

                DateTime dtDummy = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                DateTime G1Start = dtDummy;
                DateTime G1End = dtDummy;
                DateTime G2Start = dtDummy;
                DateTime G2End = dtDummy;
                if (UT.IsTimeInUtc == true)
                {
                    refTimeArr = CFC.GetUTC();
                    refTimeDep = CFC.GetUTC();
                    PABS = CFC.GetUTC(); PAES = CFC.GetUTC(); PDBS = CFC.GetUTC(); PDES = CFC.GetUTC();
                    PABA = CFC.GetUTC(); PAEA = CFC.GetUTC(); PDBA = CFC.GetUTC(); PDEA = CFC.GetUTC();
                }
                else
                {
                    refTimeArr = DateTime.Now;
                    refTimeDep = DateTime.Now;
                    PABS = DateTime.Now; PAES = DateTime.Now; PDBS = DateTime.Now; PDES = DateTime.Now;
                    PABA = DateTime.Now; PAEA = DateTime.Now; PDBA = DateTime.Now; PDEA = DateTime.Now;
                }
                DateTime olFltTime;
                String strAdid = "";
                tabCurrFlights.ResetContent();
                //tabCurrFlights.HeaderString = "URNO,RKEY,ADID,POS,GAT,FLNO,ORIG,DEST,TISA,TISD,DOUBLE";
                tabCurrFlights.HeaderString = "URNO,RKEY,ADID,POS,GAT,FLNO,ORIG,DEST,TISA,TISD,TIFA,TIFD,DOUBLE";
                tabCurrFlights.ColumnWidthString = "10,10,2,5,5,10,5,5,1,1,5";
                tabCurrFlights.LogicalFieldList = tabCurrFlights.HeaderString;
                tabCurrFlights.EnableHeaderSizing(true);
                tabCurrFlights.ShowHorzScroller(true);
                tabCurrFlights.SetInternalLineBuffer(true);

                if ((myAFT != null) && (myTmo != null) && (myLand != null) && (myOffbl != null))
                {
                    try
                    {
                        myTmo.Lock.AcquireWriterLock(Timeout.Infinite);
                        myLand.Lock.AcquireWriterLock(Timeout.Infinite);
                        myOffbl.Lock.AcquireWriterLock(Timeout.Infinite);
                        myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                        #region Get Flight Info and Fill Tmo, Land, Offbl
                        //The Tmo,Land and Offblock Area data
                        myTmo.Clear();
                        myLand.Clear();
                        myOffbl.Clear();

                        refTimeDep = currTimePoint.Subtract(new TimeSpan(0, 30, 0));
                        refTimeArr = currTimePoint.Add(new TimeSpan(0, 30, 0));
                        myAFT.Sort("TIFA,TIFD", true);
                        for (int i = 0; i < myAFT.Count; i++)
                        {
                            String strFlno = myAFT[i]["FLNO"];
                            strAdid = myAFT[i]["ADID"];
                            if (strAdid == "A")
                            {
                                PABS = UT.CedaFullDateToDateTime(myAFT[i]["PABS"]);
                                PAES = UT.CedaFullDateToDateTime(myAFT[i]["PAES"]);
                                olFltTime = UT.CedaFullDateToDateTime(myAFT[i]["TIFA"]);
                                int gCnt = GateInfoForFlight(myAFT[i], ref G1Start, ref G1End, ref G2Start, ref G2End);
                                
                                if (PAES >= currTimePoint && PABS <= currTimePoint)
                                {
                                    if ((cbShowRealWord.Checked == false) || (ShowArrivalInRealWorld(myAFT[i]) == true))
                                    {
                                        string strV = "";
                                        string strGate = "";
                                        string strPos = "";
                                        strPos = myAFT[i]["PSTA"].Trim();
                                        strGate = myAFT[i]["GTA1"].Trim();
                                        if (strGate == "")//G2
                                            strGate = myAFT[i]["GTA2"].Trim();
                                        if (strPos != "" || strGate != "")
                                        {
                                            strV = myAFT[i].FieldValues("URNO,RKEY,ADID") + ",";
                                            strV += strPos + "," + strGate + ",";
                                            //strV += myAFT[i].FieldValues("FLNO,ORG3,DES3,TISA,TISD")+",";
                                            strV += myAFT[i].FieldValues("FLNO,ORG3,DES3,TISA,TISD,TIFA,TIFD") + ",";
                                            tabCurrFlights.InsertTextLine(strV, true);
                                        }
                                    }//END if(ShowArrivalInRealWorld(myAFT[i]) == true || cbShowRealWord.Checked == false)
                                }
                                if (myAFT[i]["TISA"] == "T")
                                {
                                    IRow row = myTmo.CreateEmptyRow();
                                    row["KEY"] = myAFT[i]["URNO"];
                                    row["FLNO"] = myAFT[i]["FLNO"];
                                    row["TIME"] = myAFT[i]["TIFA"];
                                    row["TIS"] = myAFT[i]["TIDA"];
                                    row["X"] = "0";
                                    row["Y"] = "0";
                                    myTmo.Add(row);
                                }
                                if (myAFT[i]["TISA"] == "L")
                                {
                                    IRow row = myLand.CreateEmptyRow();
                                    row["KEY"] = myAFT[i]["URNO"];
                                    row["FLNO"] = myAFT[i]["FLNO"];
                                    row["TIME"] = myAFT[i]["TIFA"];
                                    row["TIS"] = myAFT[i]["TIDA"];
                                    row["X"] = "0";
                                    row["Y"] = "0";
                                    myLand.Add(row);
                                }
                            }
                            else if (strAdid == "D")
                            {
                                PDBS = UT.CedaFullDateToDateTime(myAFT[i]["PDBS"]);
                                PDES = UT.CedaFullDateToDateTime(myAFT[i]["PDES"]);
                                olFltTime = UT.CedaFullDateToDateTime(myAFT[i]["TIFD"]);
                                bool blPosOK = PDES >= currTimePoint && PDBS <= currTimePoint;
                                if (blPosOK == true)
                                {
                                    if ((cbShowRealWord.Checked == false) || (ShowDepartureInRealWorld(myAFT[i]) == true))
                                    {
                                        string strV = "";
                                        string strGate = "";
                                        string strPos = "";
                                        strPos = myAFT[i]["PSTD"].Trim();
                                        strGate = myAFT[i]["GTD1"].Trim();
                                        if (strGate == "")
                                            strGate = myAFT[i]["GTD2"].Trim();

                                        if (strPos != "" || strGate != "")
                                        {
                                            strV = myAFT[i].FieldValues("URNO,RKEY,ADID") + ",";
                                            strV += strPos + "," + strGate + ",";
                                            //strV += myAFT[i].FieldValues("FLNO,ORG3,DES3,TISA,TISD") + ",";
                                            strV += myAFT[i].FieldValues("FLNO,ORG3,DES3,TISA,TISD,TIFA,TIFD") + ",";
                                            tabCurrFlights.InsertTextLine(strV, true);
                                        }
                                    }//END if(ShowDepartureInRealWorld(myAFT[i]) == true || cbShowRealWord.Checked == false)
                                }
                                if (myAFT[i]["TISD"] == "O")
                                {
                                    IRow row = myOffbl.CreateEmptyRow();
                                    row["KEY"] = myAFT[i]["URNO"];
                                    row["FLNO"] = myAFT[i]["FLNO"];
                                    row["TIME"] = myAFT[i]["TIFD"];
                                    row["TIS"] = myAFT[i]["TIDS"];
                                    row["X"] = "0";
                                    row["Y"] = "0";
                                    myOffbl.Add(row);
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LogExceptionErr(ex, "Flight ReDisplay Error.");
                    }
                    finally
                    {
                        try { if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock(); }
                        catch { }
                        try { if (myOffbl.Lock.IsWriterLockHeld) myOffbl.Lock.ReleaseWriterLock(); }
                        catch { }
                        try { if (myLand.Lock.IsWriterLockHeld) myLand.Lock.ReleaseWriterLock(); }
                        catch { }
                        try { if (myTmo.Lock.IsWriterLockHeld) myTmo.Lock.ReleaseWriterLock(); }
                        catch { }
                    }
                }
                tabCurrFlights.IndexCreate("POS", 3);
                tabCurrFlights.IndexCreate("GAT", 4);
                tabCurrFlights.Sort("2", true, true);
                picArea.Invalidate();
                if (panelTmoLand.Visible == true)
                {
                    picTmoLand.Invalidate();
                }
                if (panelOffblocks.Visible == true)
                {
                    picOffblocks.Invalidate();
                }
            }
        }
        private void LogExceptionErr(Exception ex, string msgToDisplay)
        {
            UT.LogMsg("frmRunViewPoint:Err:" + ex.Message + Environment.NewLine + ex.StackTrace);
            if (msgToDisplay.Trim() != "")
            {
                MessageBox.Show(msgToDisplay, "Error");
            }
        }
	
		/// <summary>
		///Detects hwo many gate are allocated for flight and which ones
		///Moreover it returns the relevant timeField
		/// </summary>
		/// <param name="aftRow"></param>
		/// <param name="dtGateStart">start time for gate allocation</param>
		/// <param name="dtGateEnd">end time for gate allocation</param>
		/// <returns>0 = none, 1 = GATE1, 2=GATE2, 3=GATE1 and GATE2</returns>
		private int GateInfoForFlight(IRow aftRow, ref DateTime dtGateStart1, ref DateTime dtGateEnd1,
			ref DateTime dtGateStart2, ref DateTime dtGateEnd2)
		{
			int ilRet = 0;
			DateTime dtDummy = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			if(aftRow["ADID"] == "A")
			{
				if(aftRow["GTA1"] != "" &&  aftRow["GTA2"] != "")
					ilRet = 3;
				if(aftRow["GTA1"] != "" &&  aftRow["GTA2"] == "")
					ilRet = 1;
				if(aftRow["GTA1"] == "" &&  aftRow["GTA2"] != "")
					ilRet = 2;
				dtGateStart1 = GetGateTime(aftRow["GA1B"], aftRow["GA1X"]);
				dtGateEnd1   = GetGateTime(aftRow["GA1E"], aftRow["GA1Y"]);
				dtGateStart2 = GetGateTime(aftRow["GA2B"], aftRow["GA2X"]);
				dtGateEnd2   = GetGateTime(aftRow["GA2E"], aftRow["GA2Y"]);
			}
			if(aftRow["ADID"] == "D")
			{
				if(aftRow["GTD1"] != "" &&  aftRow["GTD2"] != "")
					ilRet = 3; 
				if(aftRow["GTD1"] != "" &&  aftRow["GTD2"] == "")
					ilRet = 1;
				if(aftRow["GTD1"] == "" &&  aftRow["GTD2"] != "")
					ilRet = 2;
				dtGateStart1 = GetGateTime(aftRow["GD1B"], aftRow["GD1X"]);
				dtGateEnd1   = GetGateTime(aftRow["GD1E"], aftRow["GD1Y"]);
				dtGateStart2 = GetGateTime(aftRow["GD2B"], aftRow["GD2X"]);
				dtGateEnd2   = GetGateTime(aftRow["GD2E"], aftRow["GD2Y"]);
			}
			return ilRet;
		}
		private DateTime GetGateTime(string strSched, string strActual)
		{
			DateTime dtDummy = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			DateTime dtRet = dtDummy;
			if(strActual != "")
			{
				dtRet = UT.CedaFullDateToDateTime(strActual);
			}
			else
			{
				if(strSched != "")
				{
					dtRet = UT.CedaFullDateToDateTime(strSched);
				}
			}
			return dtRet;
		}

		private bool ShowArrivalInRealWorld(IRow row)
		{
			bool blRet = false;
			if(row["TISA"] == "O")
				blRet = true;

			return blRet;
		}

		private bool ShowDepartureInRealWorld(IRow row)
		{
			bool blRet = true;
			if(row["TISD"] == "O")
				blRet = false;
			if(row["TISD"] == "A")
				blRet = false;
            if (blRet == true)
            {
                //Now checking if arrival has onblock ==> then Departure is ok so far
                IRow rowArrival = null;
                try
                {
                    myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                    IRow[] rows = myAFT.RowsByIndexValue("RKEY", row["RKEY"]);
                    for (int i = 0; i < rows.Length; i++)
                    {
                        if (rows[i]["ADID"] == "A")
                        {
                            rowArrival = rows[i];
                            if (rowArrival["TISA"] != "O")
                                blRet = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogExceptionErr(ex, "Error in Retrieving Departure Flight Information.");
                }
                finally
                {
                    if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                }
                if (rowArrival == null)
                    blRet = false;
            }

			return blRet;
		}
		private void trackBar1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			RefillFlights();
		}//END void RefillFlights()

		/// <summary>
		/// returns the index in the imagelists collection
		/// for the time status of arrival
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns></returns>
		private int GetTisaColor(String strTisa)
		{
			int retIDX=0;
			switch(strTisa)
			{
				case "S": //Sched
					retIDX = 8;//colGray;
					break;
				case "E": //Est.
					retIDX = 7;//colWhite
					break;
				case "T": //TMO
					retIDX = 12;//colYellow
					break;
				case "L": //Landed
					retIDX = 9;//colGreen
					break;
				case "O": //Onblock
					retIDX = 6;//colLime
					break;
			}
			return retIDX;
		}//END int GetTisaColor(String strTisa)
		/// <summary>
		/// returns the index in the imagelists collection
		/// for the time status of departure
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns></returns>
		private int GetTisdColor(String strTisa)
		{
			int retIDX=0;
			switch(strTisa)
			{
				case "O": //Offblock
					retIDX = 6;//colGray;
					break;
				case "A": //Est.
					retIDX = 9;//colWhite
					break;
				case "S": //TMO
					retIDX = 8;//colYellow
					break;
				case "E": //Landed
					retIDX = 7;//colGreen
					break;
			}
			return retIDX;
		}

		private void cbShowPositionCircle_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbShowPositionCircle.Checked == true)
			{
				cbShowPositionCircle.BackColor = Color.LightGreen;
			}
			else
			{
				cbShowPositionCircle.BackColor = Color.Transparent;
			}
			picArea.Invalidate();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void picArea_DoubleClick(object sender, System.EventArgs e)
		{
			String strPos="";
			int x=0;
			int y=0;
			Point p;
			int i = 0;
			bool blFound = false;
			Rectangle myR;
			//			rptViewPointRun r = new rptViewPointRun(picArea, myPosDefs, tabCurrFlights, imageList, imImageSize);
			//			frmPreview dlg = new frmPreview(r);
			//			dlg.Show();
			myR = new Rectangle(currX - (imImageSize/2), currY - (imImageSize/2), imImageSize, imImageSize);
			for( i = 0; i < myPosDefs.GetLineCount(); i++)
			{
				x = Convert.ToInt32(myPosDefs.GetFieldValue(i, "X"));
				y = Convert.ToInt32(myPosDefs.GetFieldValue(i, "Y"));
				p = new Point(x, y);
				if( myR.Contains(p) == true) 
				{
					strPos = myPosDefs.GetFieldValue(i, "POS");
					i = myPosDefs.GetLineCount();
					blFound = true;
				}
			}
			if(blFound == false)
			{
				lblToolTip.Visible = false;
			}
			//if(cbShowDirectInfo.Checked == true)
			{
				int llLine=-1;
				if(strPos != "")
				{
					for(int j = 0; j < tabCurrFlights.GetLineCount(); j++)
					{
						if(tabCurrFlights.GetFieldValue(j, "POS") == strPos || tabCurrFlights.GetFieldValue(j, "GAT") == strPos)
						{
							llLine = j;
						}
					}
				}
				if(llLine > -1)
				{
                    ShowStatusDetail(tabCurrFlights.GetFieldValue(llLine, "URNO"));
				}
			}
		}

		private void DE_OnNavigateToFlight(object sender, string strAftUrno)
		{
			if(sender == (object)this)
			{
				//return; //No update if "this" is the sender
			}
			if(strAftUrno != strCurrentSelected)
			{
				strCurrentSelected = strAftUrno;
				picArea.Refresh();
				//picArea.Invalidate();
			}
		}

		private void frmRunViewPoint_Closed(object sender, System.EventArgs e)
		{
            UT.LogMsg("RunViewPoint: Closing");
            try
            {
                if ((omInfoDlg != null) && (!omInfoDlg.IsDisposed)) omInfoDlg.Close();
            }
            catch { }

			DE.OnNavigateToFlight -= delegateNavigateToFlight;
			DE.OnConflicTimerElapsed -= delegateConflicTimerElapsed;
			DE.OnUpdateFlightData -= delegateUpdateFlightData;
			DE.OnViewNameChanged -= delegateViewNameChanged;
			DE.OnUTC_LocalTimeChanged -= delegateUTC_LocalTimeChanged;
            UT.LogMsg("RunViewPoint: Closed");
			WriteRegistry();

		}

		private void picArea_Click(object sender, System.EventArgs e)
		{
		}

		private void cbOffblocksView_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbOffblocksView.Checked == true)
			{
				cbOffblocksView.BackColor = Color.LightGreen;
				panelOffblocks.Visible = true;
			}
			else
			{
				cbOffblocksView.BackColor = Color.Transparent;
				panelOffblocks.Visible = false;
			}
		}

		private void cbTmoLandView_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbTmoLandView.Checked == true)
			{
				cbTmoLandView.BackColor = Color.LightGreen;
				PrepareTmoLandOffbl();
				panelTmoLand.Visible = true;
			}
			else
			{
				cbTmoLandView.BackColor = Color.Transparent;
				panelTmoLand.Visible = false;
			}
		}

		private void picTmoLand_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			try
			{
				int i = 0;
				int currTmoX = 0;
				Graphics g = e.Graphics;
				int imgHalfWidth = (int)imageTmoLandOffbl.Images[0].Width/2;
				int imgHalfHeight = (int)imageTmoLandOffbl.Images[0].Height/2;
				int imgWidth = imageTmoLandOffbl.Images[0].Width;
				int imgHeight = imageTmoLandOffbl.Images[0].Height;
				bool isDay = true;


				PaintDayAndNight(picTmoLand, e.Graphics, ref isDay);
				Font olFont = new Font("Arial", 8, FontStyle.Bold);
                try
                {
                    myTmo.Lock.AcquireReaderLock(Timeout.Infinite);
                    #region Process TMO
                    for (i = 0; (i < myTmo.Count && i < 10); i++)
                    {
                        string strFlno = myTmo[i]["FLNO"];
                        float iTextSize;
                        iTextSize = g.MeasureString(strFlno, olFont).Width;
                        PointF pointText = new PointF(currTmoX + ((imgWidth / 2) - (iTextSize / 2)), 1);
                        Image img = imageTmoLandOffbl.Images[0];
                        g.DrawImage(img, currTmoX, -6, imgWidth, imgHeight);
                        Rectangle rect = new Rectangle((int)(currTmoX + ((imgWidth / 2) - (iTextSize / 2))), 1, (int)iTextSize, 11);
                        g.FillRectangle(Brushes.White, rect);
                        g.DrawString(strFlno, olFont, Brushes.Blue, pointText);
                        myTmo[i]["X"] = ((int)(currTmoX + (imgWidth / 2))).ToString();
                        myTmo[i]["Y"] = ((int)(imgHalfHeight + 1)).ToString();
                        currTmoX += imgWidth + 10;
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    LogExceptionErr(ex, "TMO Display Error.");
                }
                finally
                {
                    if (myTmo.Lock.IsReaderLockHeld) myTmo.Lock.ReleaseReaderLock();
                }

				if(cbOneChain.Checked == false)
				{
					currTmoX = 0;
				}
                try
                {
                    myLand.Lock.AcquireReaderLock(Timeout.Infinite);
                    #region Process LANDED FLIGHT
                    for (i = 0; (i < myLand.Count && i < 10); i++)
                    {
                        string strFlno = myLand[i]["FLNO"];
                        float iTextSize;
                        iTextSize = g.MeasureString(strFlno, olFont).Width;
                        PointF pointText = new PointF(currTmoX + ((imgWidth / 2) - (iTextSize / 2)), 25);
                        Image img = imageTmoLandOffbl.Images[1];
                        g.DrawImage(img, currTmoX, 18, imgWidth, imgHeight);
                        Rectangle rect = new Rectangle((int)(currTmoX + ((imgWidth / 2) - (iTextSize / 2))), 25, (int)iTextSize, 11);
                        g.FillRectangle(Brushes.White, rect);
                        g.DrawString(strFlno, olFont, Brushes.Blue, pointText);
                        myLand[i]["X"] = ((int)(currTmoX + (imgWidth / 2))).ToString();
                        myLand[i]["Y"] = ((int)(imgHalfHeight + 25)).ToString();
                        currTmoX += imgWidth + 10;
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    LogExceptionErr(ex, "Landed Flight Display Error.");
                }
                finally
                {
                    if (myLand.Lock.IsReaderLockHeld) myLand.Lock.ReleaseReaderLock();
                }
			}
			catch(Exception err)
			{
				MessageBox.Show(err.Message);
			}
		}

		private void picOffblocks_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
            try
            {
                myOffbl.Lock.AcquireReaderLock(Timeout.Infinite);
                int i = 0;
                int currX = 0;
                Graphics g = e.Graphics;
                int imgHalfWidth = (int)imageTmoLandOffbl.Images[1].Width / 2;
                int imgHalfHeight = (int)imageTmoLandOffbl.Images[1].Height / 2;
                int imgWidth = imageTmoLandOffbl.Images[0].Width;
                int imgHeight = imageTmoLandOffbl.Images[0].Height;
                bool isDay = true;

                PaintDayAndNight(picOffblocks, e.Graphics, ref isDay);
                Font olFont = new Font("Arial", 8, FontStyle.Bold);
                for (i = 0; (i < myOffbl.Count && i < 10); i++)
                {
                    string strFlno = myOffbl[i]["FLNO"];
                    float iTextSize;
                    iTextSize = g.MeasureString(strFlno, olFont).Width;
                    PointF pointText = new PointF(currX + ((imgWidth / 2) - (iTextSize / 2)), 25);
                    Image img = imageTmoLandOffbl.Images[1];
                    g.DrawImage(img, currX, 18, imgWidth, imgHeight);
                    Rectangle rect = new Rectangle((int)(currX + ((imgWidth / 2) - (iTextSize / 2))), 25, (int)iTextSize, 11);
                    g.FillRectangle(Brushes.White, rect);
                    g.DrawString(strFlno, olFont, Brushes.Blue, pointText);
                    myOffbl[i]["X"] = ((int)(currX + (imgWidth / 2))).ToString();
                    myOffbl[i]["Y"] = ((int)(imgHalfHeight + 25)).ToString();
                    currX += imgWidth + 10;
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "OffBlock Flight Display Error.");
            }
            finally
            {
                if (myOffbl.Lock.IsReaderLockHeld) myOffbl.Lock.ReleaseReaderLock();
            }
		}

		private void PaintDayAndNight(PictureBox myPic, Graphics g, ref bool isDay)
		{
			try
			{
				int ilTimeX;
				double oneMinute;
				DateTime now = DateTime.Now;
				DateTime beginOfToday = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
				DateTime startOfDay = new DateTime(now.Year, now.Month, now.Day, 6, 0, 0);
				DateTime midOfDay = new DateTime(now.Year, now.Month, now.Day, 18, 0, 0);
				TimeSpan olTS = now - beginOfToday;

				Image myPlanet = null;
				Font olFont = new Font("Arial", 8, FontStyle.Bold);
				if(now > startOfDay && now <= midOfDay)
				{
					myPlanet = imagePlanets.Images[0];
					myPic.BackgroundImage = imgDay.BackgroundImage;
					olTS = now - startOfDay;
					isDay = true;
				}
				if(now <= startOfDay || now > midOfDay)
				{
					myPlanet = imagePlanets.Images[1];
					myPic.BackgroundImage = imgNight.BackgroundImage;
					olTS = now - midOfDay;
					isDay = false;
				}
				TimeSpan TStmp = (midOfDay - startOfDay);
				oneMinute = panelTmoLand.Width / TStmp.TotalMinutes;
				ilTimeX = (int)(oneMinute * olTS.TotalMinutes);
				g.DrawImage(myPlanet, ilTimeX, 1, myPlanet.Width, myPlanet.Height);
			}
			catch(Exception err)
			{
				MessageBox.Show(err.Message);
			}
		}

		private void DE_OnConflicTimerElapsed()
		{
			if(cbCyclicUpdate.Checked == true)
			{
				ScrollToNow();
			}
		}

		private void PrepareTmoLandOffbl()
		{
			int i = 0;

            try
            {
                myTmo.Lock.AcquireWriterLock(Timeout.Infinite);
                myLand.Lock.AcquireWriterLock(Timeout.Infinite);
                myOffbl.Lock.AcquireWriterLock(Timeout.Infinite);
                myAFT.Lock.AcquireReaderLock(Timeout.Infinite);

                #region Get Flight Infor and Fill Tmo,Land and Offbl
                myTmo.Clear();
                myLand.Clear();
                myOffbl.Clear();
                for (i = 0; i < myAFT.Count; i++)
                {
                    if (myAFT[i]["ADID"] == "A")
                    {
                        if (myAFT[i]["TISA"] == "T")
                        {
                            IRow row = myTmo.CreateEmptyRow();
                            row["KEY"] = myAFT[i]["URNO"];
                            row["FLNO"] = myAFT[i]["FLNO"];
                            row["TIME"] = myAFT[i]["TIFA"];
                            row["TIS"] = myAFT[i]["TIDA"];
                            row["X"] = "0";
                            row["Y"] = "0";
                            myTmo.Add(row);
                        }
                        if (myAFT[i]["TISA"] == "L")
                        {
                            IRow row = myLand.CreateEmptyRow();
                            row["KEY"] = myAFT[i]["URNO"];
                            row["FLNO"] = myAFT[i]["FLNO"];
                            row["TIME"] = myAFT[i]["TIFA"];
                            row["TIS"] = myAFT[i]["TIDA"];
                            row["X"] = "0";
                            row["Y"] = "0";
                            myLand.Add(row);
                        }
                    }
                    if (myAFT[i]["ADID"] == "D")
                    {
                        if (myAFT[i]["TISD"] == "O")
                        {
                            IRow row = myOffbl.CreateEmptyRow();
                            row["KEY"] = myAFT[i]["URNO"];
                            row["FLNO"] = myAFT[i]["FLNO"];
                            row["TIME"] = myAFT[i]["TIFD"];
                            row["TIS"] = myAFT[i]["TIDS"];
                            row["X"] = "0";
                            row["Y"] = "0";
                            myOffbl.Add(row);
                        }
                    }
                }
                #endregion
            }
            catch (Exception err)
            {
                LogExceptionErr(err, err.Message);
            }
            finally
            {
                try { if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock(); }
                catch { }
                try { if (myOffbl.Lock.IsWriterLockHeld) myOffbl.Lock.ReleaseWriterLock(); }
                catch { }
                try { if (myLand.Lock.IsWriterLockHeld) myLand.Lock.ReleaseWriterLock(); }
                catch { }
                try { if (myTmo.Lock.IsWriterLockHeld) myTmo.Lock.ReleaseWriterLock(); }
                catch { }
            }
		}

		private void panelTmoLand_SizeChanged(object sender, System.EventArgs e)
		{
			picTmoLand.Invalidate();
		}

		private void picOffblocks_SizeChanged(object sender, System.EventArgs e)
		{
			picOffblocks.Invalidate();
		}

		private void picTmoLand_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Rectangle myR;
			string strUrno = "";

			currTmoLand_X = e.X;
			currTmoLand_Y = e.Y;

			if(cbShowDirectInfo.Checked == true)
			{
				myR = new Rectangle(e.X - 13, e.Y - 13,	26, 26);
                strUrno = this.GetTmoLandAftUrno(myR);

				if(strUrno != "")
				{
                    omInfoDlg.Top = MousePosition.Y + 10;// - (omInfoDlg.Height/2);
                    omInfoDlg.Left = MousePosition.X + 10;
                    bool hasFlight = omInfoDlg.DisplayFlightInfoScreen(strUrno);
                    if (!hasFlight) myCurrentPOS = "";
                }//END if(strUrno != "")
				else
				{
					omInfoDlg.HideMe();
                    myCurrentPOS = "";
				}
			}
		}

		private void cbOneChain_CheckedChanged(object sender, System.EventArgs e)
		{
			picTmoLand.Invalidate();
		}

		private void picOffblocks_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{            
			Rectangle myR;
			string strUrno = "";

			currOFBL_X = e.X;		
			currOFBL_Y = e.Y;

			if(cbShowDirectInfo.Checked == true)
			{
				myR = new Rectangle(e.X - 13, e.Y - 13,	26, 26);
                strUrno = this.GetOffBlkAftUrno(myR);
				if(strUrno != "")
				{
                    omInfoDlg.Top = MousePosition.Y - omInfoDlg.Height - 10;
                    omInfoDlg.Left = MousePosition.X + 10;

                    bool hasFlight = omInfoDlg.DisplayFlightInfoScreen(strUrno);
                    if (!hasFlight) myCurrentPOS = "";
				}//END if(strUrno != "")
				else
				{
					omInfoDlg.HideMe();
                    myCurrentPOS = "";
				}
			}
		}

        private DateTime _dtLastFlightDataUpdate = new DateTime(1, 1, 1);
        private bool _waitingToRefresh = false;
        private object _lockObjectToRefresh = new object();

        private void UpdateOstChangesForAFlight(object sender, string strAftUrno, State state)
		{
            if (this._waitingToRefresh) return;
            UT.LogMsg("frmRunViewPoint:UpdateFlightData:Start");
            lock (_lockObjectToRefresh)
            {
                if (!this._waitingToRefresh)
                {
                    try
                    {
                        this._waitingToRefresh = true;
                        TimeSpan tsDiff = DateTime.Now.Subtract(_dtLastFlightDataUpdate);
                        if (tsDiff.TotalSeconds < 5.0)
                        {
                            System.Threading.Thread.Sleep(3000);//wait for 3 seconds
                        }
                        //ScrollToNow();//AM:20080729
                        RefillFlights();//AM:20080729
                        this._dtLastFlightDataUpdate = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        LogExceptionErr(ex, "");
                    }
                    finally
                    {
                        this._waitingToRefresh = false;
                    }
                }
            }
            UT.LogMsg("frmRunViewPoint:UpdateFlightData:Finish");
		}

		private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
		{
            if (this._waitingToRefresh) return;
            UT.LogMsg("frmRunViewPoint:DE_OnUpdateFlightData:Start");
            Thread thUpdFlightData = new Thread(delegate() { this.UpdateOstChangesForAFlight(sender, strAftUrno, state); });
            thUpdFlightData.IsBackground = true;
            thUpdFlightData.Start();
            UT.LogMsg("frmRunViewPoint:DE_OnUpdateFlightData:Finish");
            //lock (_lockObjectToRefresh)
            //{
            //    if (!this._waitingToRefresh)
            //    {
            //        try
            //        {
            //            this._waitingToRefresh = true;
            //            TimeSpan tsDiff = DateTime.Now.Subtract(_dtLastFlightDataUpdate);
            //            if (tsDiff.TotalSeconds < 5.0)
            //            {
            //                System.Threading.Thread.Sleep(3000);//wait for 3 seconds
            //            }
            //            ScrollToNow();
            //            this._dtLastFlightDataUpdate = DateTime.Now;
            //        }
            //        catch (Exception ex)
            //        {
            //            LogExceptionErr(ex, "");
            //        }
            //        finally
            //        {
            //            this._waitingToRefresh = false;
            //        }
            //    }
            //}
		}

		private void DE_OnViewNameChanged()
		{
			ScrollToNow();
		}

		private void DE_OnUTC_LocalTimeChanged()
		{
			ScrollToNow();
		}

		private void cbShowRealWord_CheckedChanged(object sender, System.EventArgs e)
		{
			RefillFlights();
		}

		private void picTmoLand_DoubleClick(object sender, System.EventArgs e)
		{
			Rectangle myR;

			string strUrno = "";

			myR = new Rectangle(currTmoLand_X - 13, currTmoLand_Y - 13,	26, 26);
            strUrno = this.GetTmoLandAftUrno(myR);

			if(strUrno != "")
			{
                ShowStatusDetail(strUrno);
			}
		}

		private void picOffblocks_DoubleClick(object sender, System.EventArgs e)
		{
			Rectangle myR;
			string strUrno = "";

			if(cbShowDirectInfo.Checked == true)
			{
				myR = new Rectangle(currOFBL_X - 13, currOFBL_Y - 13,	26, 26);
                strUrno = this.GetOffBlkAftUrno(myR);
				if(strUrno != "")
				{
                    ShowStatusDetail(strUrno);
				}
			}
		}

        private void ShowStatusDetail( string strAftUrno)
        {
            if ((strAftUrno!=null) && (strAftUrno != ""))
            {
                try
                {
                    myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                    IRow[] rows = myAFT.RowsByIndexValue("URNO", strAftUrno);
                    if (rows.Length > 0)
                    {
                        DE.Call_DetailStatusDlg(rows[0]["URNO"], rows[0]["RKEY"], "");
                        DE.Call_NavigateToFlight(this, rows[0]["URNO"]);
                    }
                }
                catch (Exception ex)
                {
                    LogExceptionErr(ex, "Error To Display the Flight Status Detail.");
                }
                finally
                {
                    if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                }
            }
        }


        /// <summary>
        /// Get the Aft Urno for Tmo and Land
        /// </summary>
        /// <param name="myR">Current Position Point of the flight</param>
        /// <returns>flightUrno</returns>
        private string GetTmoLandAftUrno(Rectangle myR)
        {
            int i = 0;

            string strUrno = "";
            bool blFound = false;
            int x = 0;
            int y = 0;

            try
            {
                myTmo.Lock.AcquireReaderLock(Timeout.Infinite);
                #region Get TMO Flight No.
                for (i = 0; i < myTmo.Count; i++)
                {
                    x = Convert.ToInt32(myTmo[i]["X"]);
                    y = Convert.ToInt32(myTmo[i]["Y"]);
                    Point p = new Point(x, y);
                    if (myR.Contains(p) == true)
                    {
                        strUrno = myTmo[i]["KEY"];
                        i = myTmo.Count;
                        blFound = true;
                        break;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this.LogExceptionErr(ex, "Error Checking Tmo Flight.");
            }
            finally
            {
                if (myTmo.Lock.IsReaderLockHeld) myTmo.Lock.ReleaseReaderLock();
            }

            if (blFound == false)
            {
                try
                {
                    myLand.Lock.AcquireReaderLock(Timeout.Infinite);
                    #region Get Landing Flight No
                    for (i = 0; i < myLand.Count; i++)
                    {
                        x = Convert.ToInt32(myLand[i]["X"]);
                        y = Convert.ToInt32(myLand[i]["Y"]);
                        Point p = new Point(x, y);
                        if (myR.Contains(p) == true)
                        {
                            strUrno = myLand[i]["KEY"];
                            i = myLand.Count;
                            blFound = true;
                            break;
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    this.LogExceptionErr(ex, "Error Checking Landing Flight.");
                }
                finally
                {
                    if (myLand.Lock.IsReaderLockHeld) myLand.Lock.ReleaseReaderLock();
                }
            }
            return strUrno;
        }

        private string GetOffBlkAftUrno(Rectangle myR)
        {
            int x = 0, y = 0;
            string strUrno = "";
            try
            {
                myOffbl.Lock.AcquireReaderLock(Timeout.Infinite);
                for (int i = 0; i < myOffbl.Count; i++)
                {
                    x = Convert.ToInt32(myOffbl[i]["X"]);
                    y = Convert.ToInt32(myOffbl[i]["Y"]);
                    Point p = new Point(x, y);
                    if (myR.Contains(p) == true)
                    {
                        strUrno = myOffbl[i]["KEY"];
                        i = myOffbl.Count;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "Checking Offblock Flight Error.");
            }
            finally
            {
                if (myOffbl.Lock.IsReaderLockHeld) myOffbl.Lock.ReleaseReaderLock();
            }
            return strUrno;
        }

        private void frmRunViewPoint_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if ((omInfoDlg != null) && (!omInfoDlg.IsDisposed)) omInfoDlg.Close();
            }
            catch{}
        }

        private void cbxShowJob_CheckedChanged(object sender, EventArgs e)
        {
            if (CtrlJob.GetInstance().ShowJob != cbxShowJob.Checked)
            {
                CtrlJob.GetInstance().ShowJob = cbxShowJob.Checked;
                if (cbxShowJob.Checked)
                {
                    CtrlJob.GetInstance().SelectJobTemplate(
                        this,
                        cbxShowJob.Left + this.Left, 
                        cbxShowJob.Bottom + this.Top);
                }
            }
        }

        private void frmRunViewPoint_Shown(object sender, EventArgs e)
        {

        }
	}

    class ShowString
    {
        private string _KeyToSort = "";
        private string _StToShow = "";
        public ShowString(string keyToSort, string stToShow)
        {
            _KeyToSort = keyToSort;
            _StToShow = stToShow;
        }

        public string KeyToSort
        {
            get
            {
                return _KeyToSort;
            }
        }

        public string StToShow
        {
            get
            {
                return _StToShow;
            }
        }
    }

    class AscSortShowString : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((ShowString)x).KeyToSort.CompareTo(((ShowString)y).KeyToSort);
        }
    }

    class DscSortShowString : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((ShowString)y).KeyToSort.CompareTo(((ShowString)x).KeyToSort);
        }
    }
}




