﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ufis.Status_Manager.Ent
{
    public class EntTime
    {
        public DateTime ExpectedTime { get; set; }
        public DateTime SystemTime { get; set; }
        public DateTime UserTime { get; set; }
        public string Description { get; set; }
        public bool Conflicted { get; set; }
    }

    public class EntTimeCollection : List<EntTime>
    {
        
    }
}
