﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntStatus
    {
        public enum EntStatusTypeEnum
        {
            ManAtBay,
            Equipment,
            Unknown
        }

        private string _pictureFile = string.Empty;
        private Image _image = null;
        private Icon _icon = null;        
        
        public string Name { get; set; }
        public string Description { get; set; }

        public string PictureFile
        {
            get { return _pictureFile; }
            set
            {
                if (_pictureFile != value)
                {
                    try
                    {
                        _icon = null;
                        _image = Image.FromFile(value);
                    }
                    catch
                    {
                        try
                        {
                            _image = null;
                            _icon = Icon.ExtractAssociatedIcon(value);
                        }
                        catch
                        {
                            _image = null;
                        }
                    }
                }
                _pictureFile = value;
            }
        }

        public EntStatusTypeEnum Type { get; set; }
        public string OnlineStatusName { get; set; }
        public string OnlineStatusSection { get; set; }

        public Image GetImage()
        {
            return _image;
        }

        public Icon GetIcon()
        {
            return _icon;
        }

        public bool IsImageLoaded()
        {
            return (_image != null);
        }

        public bool IsIconLoaded()
        {
            return (_icon != null);
        }

        public static EntStatusTypeEnum GetEntStatusTypeFromString(string type)
        {
            EntStatusTypeEnum entStatusTypeEnum;

            switch (type.ToUpper())
            {
                case "0":
                case "MAB":
                case "MAN_AT_BAY":
                    entStatusTypeEnum = EntStatusTypeEnum.ManAtBay;
                    break;
                case "1":
                case "EQU":
                case "EQUIPMENT":
                    entStatusTypeEnum = EntStatusTypeEnum.Equipment;
                    break;
                default:
                    entStatusTypeEnum = EntStatusTypeEnum.Unknown;
                    break;
            }

            return entStatusTypeEnum;
        }
    }

    public class EntStatusList : List<EntStatus>
    {
        public EntStatus FindByType(EntStatus.EntStatusTypeEnum type)
        {
            foreach (EntStatus entStatus in this)
            {
                if (entStatus.Type == type)
                    return entStatus;
            }
            return null;
        }
    }

    public class EntStatusImage
    {
        PointF[] _imPoints;

        int polySides;
        float[] polyX;
        float[] polyY;

        public PointF[] ImagePoints 
        { 
            get
            {
                return _imPoints;
            } 

            set
            {
                polySides = value.Length;
                polyX = new float[polySides];
                polyY = new float[polySides];

                for (int i = 0; i < polySides; i++)
                {
                    polyX[i] = value[i].X;
                    polyY[i] = value[i].Y;
                }

                _imPoints = value;
            }
        }
        public string Description { get; set; }

        public bool Contains(Point point)
        {
            return Contains(new PointF(point.X, point.Y));
        }

        public bool Contains(PointF pointF)
        {
            int j = polySides - 1;
            bool oddNodes = false;

            float x = pointF.X, y = pointF.Y;

            for (int i = 0; i < polySides; i++) 
            {
                if (polyY[i] < y && polyY[j] >= y
                ||  polyY[j] < y && polyY[i] >= y) 
                {
                    if (polyX[i] + (y - polyY[i]) / (polyY[j] - polyY[i]) * (polyX[j] - polyX[i]) < x) 
                    {
                        oddNodes=!oddNodes; 
                    }
                }
                j=i; 
            }

            return oddNodes;
        }

        public EntStatusImage(PointF[] imagePoints, string description)
        {
            this.ImagePoints = imagePoints;
            this.Description = description;
        }

        public EntStatusImage() : this(null, string.Empty) { }
    }
}
