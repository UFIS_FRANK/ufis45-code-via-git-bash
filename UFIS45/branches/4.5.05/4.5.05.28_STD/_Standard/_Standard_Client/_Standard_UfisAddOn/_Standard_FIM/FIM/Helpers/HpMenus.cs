﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Utilities;
using System.Windows.Media.Imaging;

namespace BDPSUIF.Helpers
{
    public class HpMenus
    {
        private static List<AppMenu> _lstAppMenus;

        private static List<AppMenu> CreateMenus()
        {
            BitmapImage smallImage = ImageGetter.GetImage("application_16x16.png");
            BitmapImage largeImage = ImageGetter.GetImage("application_32x32.png");

            IList<AppMenu> lstAppMenus = new List<AppMenu>();

            lstAppMenus.Add(
                new AppMenu() 
                { Name = "Flight identifier mappings", ImageSmall = smallImage, ImageLarge = largeImage } );

            return lstAppMenus.ToList();
        }

        public static List<AppMenu> MenuList
        {
            get 
            { 
                if (_lstAppMenus == null)
                    _lstAppMenus = CreateMenus();

                return _lstAppMenus; 
            }
        }
    }
}
