﻿using System;
using System.Collections.Generic;
using System.Linq;
using BDPSUIF.BasicData;
using System.Windows;
using Ufis.MVVM.Helpers;
using System.Windows.Controls;

namespace BDPSUIF.Helpers
{
    public class HpBasicData
    {
        static string _strLastBasicDataName;

        public static BasicDataViewModel OpenBasicDataViewModel(string basicDataName)
        {              
            BasicDataViewModel viewModel = null;

            if (_strLastBasicDataName != basicDataName)
            {
                _strLastBasicDataName = basicDataName;

                switch (basicDataName)
                {
                    case "Flight identifier mappings":
                        viewModel = new FlightIdMapViewModel(basicDataName) { LoadAllData = true };
                        viewModel.LoadLayout(viewModel.UserDefinedLayouts.ElementAt(0));
                        break;
                }
            }

            return viewModel;
        }

        public static bool IsAnEntryElement(IInputElement inputElement)
        {
            bool bReturn = false;

            UIElement uiElement = (UIElement)inputElement;
            if (uiElement != null)
            {
                Grid gridEntry = uiElement.TryFindParent<Grid>();
                if (gridEntry != null && gridEntry.Name == "gridEntry")
                {
                    bReturn = true;
                }
                else
                {
                    //find one more level up
                    gridEntry = gridEntry.TryFindParent<Grid>();
                    if (gridEntry != null && gridEntry.Name == "gridEntry")
                    {
                        bReturn = true;
                    }
                }
            }

            return bReturn;
        }
    }
}
