﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ufis.AllocationPlan.Helpers;
using DevExpress.Xpf.Printing;
using DevExpress.Xpf.Grid;
using System.Threading;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Ufis.AllocationPlan
{
    /// <summary>
    /// Interaction logic for AllocationPlanView.xaml
    /// </summary>
    public partial class AllocationPlanView : UserControl, IDisposable
    {
        public HpParameters param;
        public AllocationPlanViewModel objViewModel;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                if (objViewModel != null)
                {
                    objViewModel.Dispose();
                    objViewModel = null;
                }
        }
        ~AllocationPlanView()
        {
            Dispose(false);
        }
        public AllocationPlanView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture = ci;

            InitializeComponent();
        }
        
        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            param = new HpParameters();
            DateTime tDate = dtpDate.SelectedDate.Value;
            string sDate = tDate.ToString("dd-MM-yyyy");

            param.MyParameter = String.Format("Allocation Plan for {0} from {1}-{2} LT ", sDate, txtFromTime.Text, txtToTime.Text);
            
            ((HpParameters)Resources["paramet"]).MyParameter = this.param.MyParameter;

            if (gridAllocationPlan.VisibleRowCount > 0)
            {
                PrintableControlLink link = new PrintableControlLink((TableView)gridAllocationPlan.View);
                link.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
                link.Landscape = true;
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }       

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            objViewModel = new AllocationPlanViewModel();

            DateTime tDate = dtpDate.SelectedDate.Value;
            string sDate = tDate.ToString("yyyy/MM/dd").Replace("/", "");
            string sFromDate = String.Format("{0}{1}00", sDate, txtFromTime.Text);
            string sTodate = String.Format("{0}{1}59", sDate, txtToTime.Text);

            objViewModel.GetAllocationPlanList(sFromDate, sTodate);

            gridAllocationPlan.ItemsSource = objViewModel.ItemSource;

        }

        private void dtpDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            txtFromTime.Text = "0000";
            string sTime = DateTime.Now.ToString("HH:MM", System.Globalization.DateTimeFormatInfo.InvariantInfo).Replace(":", "");
            txtToTime.Text = sTime;            
        }

        public int MyInt
        {
            get { return (int)GetValue(MyIntProperty); }
            set { SetValue(MyIntProperty, value); }
        }
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
            DependencyProperty.Register("MyInt", typeof(int), typeof(AllocationPlanView), new UIPropertyMetadata(1));
    }
}