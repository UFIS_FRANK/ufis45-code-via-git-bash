VERSION 5.00
Begin VB.Form frmLoad 
   Caption         =   "Load Data for time frame ..."
   ClientHeight    =   1470
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5760
   Icon            =   "frmLoad.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1470
   ScaleWidth      =   5760
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picFrame 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2595
      Left            =   0
      ScaleHeight     =   2595
      ScaleWidth      =   5775
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      Begin VB.TextBox txtDateFrom 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1140
         TabIndex        =   7
         ToolTipText     =   "Date from"
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox txtTimeFrom 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   6
         ToolTipText     =   "Time from"
         Top             =   240
         Width           =   675
      End
      Begin VB.TextBox txtDateTo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3060
         TabIndex        =   5
         ToolTipText     =   "Date from"
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox txtTimeTo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4200
         TabIndex        =   4
         ToolTipText     =   "Time from"
         Top             =   240
         Width           =   675
      End
      Begin VB.CommandButton cmdLoad 
         Caption         =   "&Load Data"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1380
         TabIndex        =   3
         Top             =   840
         Width           =   1155
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2580
         TabIndex        =   2
         Top             =   840
         Width           =   1155
      End
      Begin VB.CommandButton cmdToday 
         Caption         =   "&Today"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4920
         TabIndex        =   1
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Time frame:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   300
         Width           =   1035
      End
   End
End
Attribute VB_Name = "frmLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public wasCancel As Boolean

Private Sub cmdLoad_Click()
    Dim fromDat As Date
    Dim toDat As Date
    Dim fDat As Date, tDat As Date
    Dim strFrom As String
    Dim strTo As String
    
    Me.Hide
    
    
    CheckDateField txtDateFrom, "dd.mm.yyyy", False
    CheckDateField txtDateTo, "dd.mm.yyyy", False
    strFrom = txtDateFrom.Tag + txtTimeFrom
    strTo = txtDateTo.Tag + txtTimeTo
    strReadLoadFrom = strFrom
    strReadLoadTo = strTo
    If TimesInUTC = False Then
        fDat = CedaFullDateToVb(strReadLoadFrom)
        tDat = CedaFullDateToVb(strReadLoadTo)
        fDat = DateAdd("h", -UTCOffset, fDat)
        tDat = DateAdd("h", -UTCOffset, tDat)
        strReadLoadFrom = Format(fDat, "YYYYMMDDhhmmss")
        strReadLoadTo = Format(tDat, "YYYYMMDDhhmmss")
    End If
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    fromDat = DateAdd("h", -12, fromDat)
    toDat = DateAdd("h", 12, toDat)
    
    If TimesInUTC = False Then
        fromDat = DateAdd("h", -UTCOffset, fromDat)
        toDat = DateAdd("h", -UTCOffset, toDat)
    End If
    strFrom = Format(fromDat, "YYYYMMDDhhmmss")
    strTo = Format(toDat, "YYYYMMDDhhmmss")
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    fDat = fromDat
    tDat = toDat
    
    strTimeFrameFrom = strFrom
    strTimeFrameTo = strTo

    If strShowHiddenData = "" Then
        frmData.Hide
    Else
        frmData.Show
    End If

    frmStartup.Show
    'SetFormOnTop frmStartup, True
    frmStartup.Refresh
    
    'frmStartup.top = CInt(frmStartup.Tag)
'    SetFormOnTop frmStartup, True
'    frmData.LoadData
    Unload Me
End Sub

Private Sub cmdToday_Click()
    Dim today As Date
    Dim strDate As String
    Dim strTime As String
    today = Now
    strDate = Format(today, "dd.mm.YYYY")
    txtDateFrom = strDate
    txtDateTo = strDate
    txtTimeFrom = "0000"
    txtTimeTo = "2359"
    CheckDateField txtDateFrom, "dd.mm.yyyy", False
    CheckDateField txtDateTo, "dd.mm.yyyy", False
    DrawBackGround picFrame, 7, True, True, 100
End Sub

Private Sub Command1_Click()
    wasCancel = True
    Unload Me
End Sub

Private Sub Form_Load()
    wasCancel = False
    cmdToday_Click
End Sub

Private Sub txtDateFrom_Change()
    CheckDateField txtDateFrom, "dd.mm.yyyy", False
    HandleLoadButton
End Sub

Private Sub txtDateTo_Change()
    CheckDateField txtDateTo, "dd.mm.yyyy", False
    HandleLoadButton
End Sub


Private Sub txtTimeFrom_Change()
    Dim strDate As String
    strDate = txtTimeFrom.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtTimeFrom.BackColor = vbRed
        txtTimeFrom.ForeColor = vbWhite
    Else
        txtTimeFrom.BackColor = vbWhite
        txtTimeFrom.ForeColor = vbBlack
    End If
    HandleLoadButton
End Sub

Private Sub txtTimeTo_Change()
    Dim strDate As String
    strDate = txtTimeTo.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtTimeTo.BackColor = vbRed
        txtTimeTo.ForeColor = vbWhite
    Else
        txtTimeTo.BackColor = vbWhite
        txtTimeTo.ForeColor = vbBlack
    End If
    HandleLoadButton
End Sub

Private Sub HandleLoadButton()
    If txtTimeTo.BackColor = vbRed Or _
       txtTimeFrom.BackColor = vbRed Or _
       txtDateFrom.BackColor = vbRed Or _
       txtDateTo.BackColor = vbRed Then
       cmdLoad.Enabled = False
    Else
        cmdLoad.Enabled = True
    End If
End Sub
