VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "Flight Schedule ..."
   ClientHeight    =   10425
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12705
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10425
   ScaleWidth      =   12705
   StartUpPosition =   3  'Windows Default
   WindowState     =   1  'Minimized
   Begin TABLib.TAB tabTmp 
      Height          =   1095
      Left            =   2700
      TabIndex        =   60
      Top             =   8550
      Visible         =   0   'False
      Width           =   2130
      _Version        =   65536
      _ExtentX        =   3757
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin VB.Timer timerConflicCheck 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   90
      Top             =   8190
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   4
      Top             =   10080
      Width           =   12705
      _ExtentX        =   22410
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picBackground 
      AutoRedraw      =   -1  'True
      Height          =   9165
      Index           =   2
      Left            =   9000
      ScaleHeight     =   9105
      ScaleWidth      =   3675
      TabIndex        =   3
      Top             =   930
      Width           =   3735
      Begin VB.CheckBox cbShowDepartureGrid 
         Height          =   195
         Left            =   330
         TabIndex        =   44
         Top             =   8760
         Value           =   1  'Checked
         Width           =   195
      End
      Begin VB.CheckBox cbShowArrivalGrid 
         Height          =   195
         Left            =   330
         TabIndex        =   43
         Top             =   8430
         Value           =   1  'Checked
         Width           =   195
      End
      Begin VB.PictureBox picBackground 
         AutoRedraw      =   -1  'True
         Height          =   3705
         Index           =   4
         Left            =   90
         ScaleHeight     =   3645
         ScaleWidth      =   3435
         TabIndex        =   8
         Top             =   4500
         Width           =   3495
         Begin VB.CheckBox cmdDepartureFilter 
            Caption         =   "Do &Departure Filter"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   55
            Top             =   3330
            Width           =   3315
         End
         Begin VB.CommandButton cmdAlcDepNone 
            Caption         =   "None"
            Height          =   255
            Left            =   60
            TabIndex        =   39
            Top             =   1830
            Width           =   585
         End
         Begin VB.CommandButton cmdAlcDepAll 
            Caption         =   "All"
            Height          =   255
            Left            =   60
            TabIndex        =   38
            Top             =   1500
            Width           =   585
         End
         Begin VB.TextBox txtDepDateFrom 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   30
            Text            =   "01.06.2003"
            Top             =   90
            Width           =   1305
         End
         Begin VB.TextBox txtDepTimeFrom 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   29
            Text            =   "00:00"
            Top             =   90
            Width           =   705
         End
         Begin VB.TextBox txtDepDateTo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   28
            Text            =   "01.06.2003"
            Top             =   420
            Width           =   1305
         End
         Begin VB.TextBox txtDepTimeTo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   27
            Text            =   "00:00"
            Top             =   420
            Width           =   705
         End
         Begin VB.CommandButton cmdDepToday 
            Caption         =   "Today"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   26
            Top             =   720
            Width           =   2685
         End
         Begin VB.CheckBox cbDEEntry 
            Height          =   195
            Left            =   660
            TabIndex        =   24
            Top             =   2700
            Width           =   195
         End
         Begin VB.CheckBox cbNoDEEntry 
            Height          =   225
            Left            =   660
            TabIndex        =   23
            Top             =   3000
            Width           =   195
         End
         Begin TABLib.TAB tabDepAirlines 
            Height          =   1545
            Index           =   0
            Left            =   660
            TabIndex        =   25
            Top             =   1020
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   2725
            _StockProps     =   64
         End
         Begin TABLib.TAB tabDepAirlines 
            Height          =   1545
            Index           =   1
            Left            =   2040
            TabIndex        =   31
            Top             =   1020
            Width           =   1305
            _Version        =   65536
            _ExtentX        =   2302
            _ExtentY        =   2725
            _StockProps     =   64
         End
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            Caption         =   "From:"
            Height          =   195
            Left            =   60
            TabIndex        =   35
            Top             =   150
            Width           =   465
         End
         Begin VB.Label Label8 
            BackStyle       =   0  'Transparent
            Caption         =   "To:"
            Height          =   195
            Left            =   60
            TabIndex        =   34
            Top             =   450
            Width           =   465
         End
         Begin VB.Label Label7 
            BackStyle       =   0  'Transparent
            Caption         =   "Flights with DE-Entry"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   900
            TabIndex        =   33
            Top             =   2700
            Width           =   2235
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Flights without DE-Entry"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   900
            TabIndex        =   32
            Top             =   3000
            Width           =   2235
         End
      End
      Begin VB.PictureBox picBackground 
         AutoRedraw      =   -1  'True
         Height          =   3855
         Index           =   3
         Left            =   90
         ScaleHeight     =   3795
         ScaleWidth      =   3435
         TabIndex        =   6
         Top             =   270
         Width           =   3495
         Begin VB.CheckBox cmdArrivalFilter 
            Caption         =   "Do &Arrival Filter"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   3480
            Width           =   3315
         End
         Begin VB.CommandButton cmdAlcArrNone 
            Caption         =   "None"
            Height          =   255
            Left            =   60
            TabIndex        =   37
            Top             =   1800
            Width           =   585
         End
         Begin VB.CommandButton cmdAlcArrAll 
            Caption         =   "All"
            Height          =   255
            Left            =   60
            TabIndex        =   36
            Top             =   1470
            Width           =   585
         End
         Begin VB.CheckBox cbNoCPMMessage 
            Height          =   225
            Left            =   660
            TabIndex        =   20
            Top             =   3060
            Width           =   195
         End
         Begin VB.CheckBox cbCPMMessage 
            Height          =   195
            Left            =   660
            TabIndex        =   19
            Top             =   2730
            Width           =   195
         End
         Begin TABLib.TAB tabArrAirlines 
            Height          =   1545
            Index           =   0
            Left            =   660
            TabIndex        =   17
            Top             =   1020
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   2725
            _StockProps     =   64
         End
         Begin VB.CommandButton cmdArrToday 
            Caption         =   "Today"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   14
            Top             =   720
            Width           =   2685
         End
         Begin VB.TextBox txtArrTimeTo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   13
            Text            =   "00:00"
            Top             =   420
            Width           =   705
         End
         Begin VB.TextBox txtArrDateTo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   12
            Text            =   "01.06.2003"
            Top             =   420
            Width           =   1305
         End
         Begin VB.TextBox txtArrTimeFrom 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   11
            Text            =   "00:00"
            Top             =   120
            Width           =   705
         End
         Begin VB.TextBox txtArrDateFrom 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   10
            Text            =   "01.06.2003"
            Top             =   120
            Width           =   1305
         End
         Begin TABLib.TAB tabArrAirlines 
            Height          =   1545
            Index           =   1
            Left            =   2040
            TabIndex        =   18
            Top             =   1020
            Width           =   1305
            _Version        =   65536
            _ExtentX        =   2302
            _ExtentY        =   2725
            _StockProps     =   64
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "Only Flights with ETA and no CPM-Message"
            ForeColor       =   &H8000000E&
            Height          =   435
            Left            =   900
            TabIndex        =   22
            Top             =   3030
            Width           =   2235
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Only Flights with CPM-Message"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   900
            TabIndex        =   21
            Top             =   2700
            Width           =   2235
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "To:"
            Height          =   195
            Left            =   60
            TabIndex        =   16
            Top             =   450
            Width           =   465
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "From:"
            Height          =   195
            Left            =   60
            TabIndex        =   15
            Top             =   150
            Width           =   465
         End
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "&2 Departure Schedule"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   570
         TabIndex        =   46
         Top             =   8760
         Width           =   2175
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "&1 Arrival Schedule"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   570
         TabIndex        =   45
         Top             =   8430
         Width           =   2025
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Departure Filter:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   285
         Index           =   1
         Left            =   90
         TabIndex        =   9
         Top             =   4230
         Width           =   3135
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Arrival Filter:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   7
         Top             =   30
         Width           =   3135
      End
   End
   Begin VB.PictureBox picBackground 
      AutoRedraw      =   -1  'True
      Height          =   7050
      Index           =   1
      Left            =   0
      ScaleHeight     =   6990
      ScaleWidth      =   8835
      TabIndex        =   2
      Top             =   930
      Width           =   8895
      Begin VB.PictureBox picSplitter 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   135
         Left            =   0
         ScaleHeight     =   135
         ScaleWidth      =   9255
         TabIndex        =   51
         Top             =   2160
         Width           =   9255
      End
      Begin TABLib.TAB tabFlights 
         Height          =   1635
         Index           =   0
         Left            =   0
         TabIndex        =   47
         Top             =   270
         Width           =   4065
         _Version        =   65536
         _ExtentX        =   7170
         _ExtentY        =   2884
         _StockProps     =   64
      End
      Begin TABLib.TAB tabFlights 
         Height          =   1635
         Index           =   1
         Left            =   0
         TabIndex        =   49
         Top             =   2640
         Width           =   4065
         _Version        =   65536
         _ExtentX        =   7170
         _ExtentY        =   2884
         _StockProps     =   64
      End
      Begin VB.Label lblEmptyText 
         BackStyle       =   0  'Transparent
         Caption         =   "No Schedule selected ..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Index           =   0
         Left            =   570
         TabIndex        =   52
         Top             =   5070
         Width           =   6375
      End
      Begin VB.Label lblGridCaption 
         Alignment       =   2  'Center
         BackColor       =   &H00000000&
         Caption         =   "Departure Schedule (111 Flights)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   50
         Top             =   2370
         Width           =   9255
      End
      Begin VB.Label lblGridCaption 
         Alignment       =   2  'Center
         BackColor       =   &H00000000&
         Caption         =   "Arrival Schedule (222 Flights)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   48
         Top             =   0
         Width           =   9255
      End
      Begin VB.Label lblEmptyText 
         BackStyle       =   0  'Transparent
         Caption         =   "No Schedule selected ..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   645
         Index           =   1
         Left            =   600
         TabIndex        =   53
         Top             =   5100
         Width           =   6375
      End
   End
   Begin VB.PictureBox picBackground 
      AutoRedraw      =   -1  'True
      FillColor       =   &H8000000F&
      Height          =   855
      Index           =   0
      Left            =   0
      ScaleHeight     =   795
      ScaleWidth      =   12675
      TabIndex        =   0
      Top             =   0
      Width           =   12735
      Begin VB.CommandButton cmdMakeTestData 
         Caption         =   "Make Test data"
         Height          =   285
         Left            =   8865
         TabIndex        =   64
         Top             =   90
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "&About"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   7
         Left            =   6165
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   420
         Width           =   825
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "S&etup"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   6
         Left            =   5265
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   420
         Width           =   825
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "&UTC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   5
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   30
         Width           =   825
      End
      Begin MSComctlLib.Slider slFonts 
         Height          =   320
         Left            =   30
         TabIndex        =   56
         ToolTipText     =   "Change the font sizes"
         Top             =   420
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         _Version        =   393216
         Min             =   8
         Max             =   72
         SelStart        =   8
         TickFrequency   =   4
         Value           =   8
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "Show Dep. &Telex"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   4
         Left            =   3510
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   420
         Width           =   1695
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "Print Dep. &Report"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   1770
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   420
         Width           =   1695
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "&Show Arr. Telex"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   3510
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   30
         Width           =   1695
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "&Print Arr. Report"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   1770
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   30
         Width           =   1695
      End
      Begin VB.CheckBox cbToolbar 
         Caption         =   "&Load"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   30
         Width           =   825
      End
      Begin VB.CommandButton cmdGetUldInfo 
         Caption         =   "Get-ULD-Info"
         Height          =   285
         Left            =   10260
         TabIndex        =   59
         Top             =   90
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.Label lblTime 
         BackStyle       =   0  'Transparent
         Caption         =   "27.07.2003 -10:23z"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   0
         Left            =   8550
         TabIndex        =   62
         Top             =   450
         Width           =   2025
      End
      Begin VB.Label lblTime 
         BackStyle       =   0  'Transparent
         Caption         =   "27.07.2003 -10:23"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   10680
         TabIndex        =   61
         Top             =   450
         Width           =   1935
      End
      Begin VB.Shape ServerSignal 
         FillColor       =   &H00FF0000&
         Height          =   195
         Index           =   2
         Left            =   12300
         Shape           =   3  'Circle
         Top             =   210
         Width           =   195
      End
      Begin VB.Shape ServerSignal 
         FillColor       =   &H00FF0000&
         Height          =   195
         Index           =   0
         Left            =   11820
         Shape           =   3  'Circle
         Top             =   210
         Width           =   195
      End
      Begin VB.Shape ServerSignal 
         FillColor       =   &H0000FF00&
         Height          =   195
         Index           =   1
         Left            =   12060
         Shape           =   3  'Circle
         Top             =   210
         Width           =   195
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private WithEvents MyBC As BcProxyLib.BcPrxy
Attribute MyBC.VB_VarHelpID = -1
Public omCurrentSelectedUrno As String
Public isInit As Boolean
Public omTrueValue As String
Public omFalseValue As String
Private bmFirstUTC As Boolean

Private Sub cbCPMMessage_Click()
    If cbCPMMessage.Value = 1 Then
        cbNoCPMMessage.Value = 0
    End If
    
End Sub

Private Sub cbDEEntry_Click()
    If cbDEEntry.Value = 1 Then
        cbNoDEEntry.Value = 0
    End If
End Sub

Private Sub cbNoCPMMessage_Click()
    If cbNoCPMMessage.Value = 1 Then
        cbCPMMessage.Value = 0
    End If
End Sub

Private Sub cbNoDEEntry_Click()
    If cbNoDEEntry.Value = 1 Then
        cbDEEntry.Value = 0
    End If
End Sub

Public Function CallServer(command As String, Object As String, Fields As String, _
                           Data As String, where As String, timeout As String) As Boolean
                           
    Dim blRet As Boolean
    
    blRet = True
    frmMain.ServerSignal(2).FillStyle = vbSolid
    frmMain.ServerSignal(2).FillColor = vbYellow
    frmMain.ServerSignal(2).Refresh
    frmData.SetServerParameters
    If frmData.aUfis.CallServer(command, Object, Fields, Data, where, timeout) <> 0 Then
        blRet = True
    End If
    frmMain.ServerSignal(2).FillColor = vbBlue
    frmMain.ServerSignal(2).Refresh
    CallServer = blRet
End Function


Private Sub cmdGetUldInfo_Click()
    Dim i As Long
    Dim strUrno As String
    Dim strRet As String
    Dim strFlno As String
    Dim strValues As String
    Dim strTifd As String
    Dim llLine As Long
    Dim strLoa As String
    
    For i = 0 To tabFlights(1).GetLineCount
        strUrno = tabFlights(1).GetFieldValue(i, "URNO")
'        strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strUrno, 0)
'        llLine = frmData.tabData(3).GetNextResultLine
'        If llLine > -1 Then
         strLoa = HasFlightLoaInfo(strUrno, "CPM,KRI,UCM,DLS")
         If InStr(1, strLoa, "Y") > 0 Then
            strFlno = tabFlights(1).GetFieldValue(i, "FLNO")
            strValues = strValues + strFlno + "   Time: " + tabFlights(1).GetFieldValue(i, "STOD") + vbLf
        End If
    Next i
    MsgBox strValues
End Sub

Private Sub MyBC_OnBcReceive(ByVal ReqId As String, ByVal Dest1 As String, _
                             ByVal Dest2 As String, ByVal Cmd As String, ByVal Object As String, _
                             ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, _
                             ByVal Selection As String, ByVal Fields As String, ByVal Data As String, _
                             ByVal BcNum As String)
    Dim strRet As String
    Dim strUrno As String
    Dim llItem As Long
    Dim llTabLine As Long
    Dim ItemNo As Long
    Dim strVal As String
    Dim strValues As String
    Dim strTifa As String
    Dim strTifd As String
    Dim strOldTIFA As String
    Dim strOldTIFD As String
    Dim strLineValues As String
    Dim ttyp As String
    Dim styp As String
    Dim strSkey As String
    Dim strFlnu As String
    
    llItem = GetRealItemNo(Fields, "URNO")
    strUrno = GetRealItem(Data, llItem, ",")
    If ServerSignal(0).FillColor = vbBlue Then
        ServerSignal(0).FillStyle = vbSolid
        ServerSignal(0).FillColor = vbGreen
        ServerSignal(1).FillStyle = vbSolid
        ServerSignal(1).FillColor = vbBlue
    Else
        ServerSignal(0).FillStyle = vbSolid
        ServerSignal(0).FillColor = vbBlue
        ServerSignal(1).FillStyle = vbSolid
        ServerSignal(1).FillColor = vbGreen
    End If
    ServerSignal(0).Refresh
    ServerSignal(1).Refresh
    Select Case (Cmd)
        Case "CLO"
            MsgBox "The server has switched or was shut down." & vbLf & "Please restart the application!!"
            End
        Case "IRT"
            Select Case (Object)
                Case "REMTAB"
                    strUrno = GetFieldValue("RURN", Data, Fields)
                    llTabLine = frmData.InsertEmptyLineForTab(6)
                    frmData.tabData(6).SetFieldValues llTabLine, Fields, Data
                    frmData.tabData(6).Sort "0,1", True, True
                    If frmULDHandling.omAftUrno = strUrno Then
                        frmULDHandling.SetFlightRemark CleanString(frmData.tabData(6).GetFieldValue(llTabLine, "TEXT"), FOR_CLIENT, True)
                    End If
                Case "TLXTAB"
                    ttyp = GetFieldValue("TTYP", Data, Fields)
                    styp = GetFieldValue("STYP", Data, Fields)
                    If ttyp = "CPM" Or ttyp = "UCM" Or ttyp = "DLS" Or ttyp = "KRIS" Then
                        ItemNo = GetRealItemNo(Fields, "FLNU")
                        If ItemNo > -1 Then
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            strUrno = Trim(strUrno)
                            If strUrno <> "" And strUrno <> "0" Then
                                HandleTlxBC strUrno
                            End If
                        End If
                    End If
                    ttyp = ""
                    styp = ""
                Case "BDITAB"
                    llTabLine = frmData.InsertEmptyLineForTab(7)
                    frmData.tabData(7).SetFieldValues llTabLine, Fields, Data
                    frmData.tabData(7).Sort "1", True, True
                    strFlnu = GetFieldValue("FLNU", Data, Fields)
                    strUrno = GetFieldValue("URNO", Data, Fields)
                    If frmULDHandling.omAftUrno = strFlnu Then
                        frmULDHandling.InitBDI
                    End If
            End Select
        Case "IBT"
            Select Case (Object)
                Case "REMTAB"
                    strUrno = GetFieldValue("RURN", Data, Fields)
                    llTabLine = frmData.InsertEmptyLineForTab(6)
                    frmData.tabData(6).SetFieldValues llTabLine, Fields, Data
                    frmData.tabData(6).Sort "0,1", True, True
                    If frmULDHandling.omAftUrno = strUrno Then
                        frmULDHandling.SetFlightRemark CleanString(frmData.tabData(6).GetFieldValue(llTabLine, "TEXT"), FOR_CLIENT, True)
                    End If
                Case "TLXTAB"
                    ttyp = GetFieldValue("TTYP", Data, Fields)
                    styp = GetFieldValue("STYP", Data, Fields)
                    If ttyp = "CPM" Or ttyp = "UCM" Or ttyp = "DLS" Or ttyp = "KRIS" Then
                        ItemNo = GetRealItemNo(Fields, "FLNU")
                        If ItemNo > -1 Then
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            strUrno = Trim(strUrno)
                            If strUrno <> "" And strUrno <> "0" Then
                                HandleTlxBC strUrno
                            End If
                        End If
                    End If
                    ttyp = ""
                    styp = ""
            End Select
        Case "URT"
            Select Case (Object)
                Case "REMTAB"
                    strUrno = GetFieldValue("URNO", Data, Fields)
                    strRet = frmData.tabData(6).GetLinesByIndexValue("URNO", strUrno, 0)
                    llTabLine = frmData.tabData(6).GetNextResultLine
                    If llTabLine > -1 Then
                        frmData.tabData(6).SetFieldValues llTabLine, Fields, Data
                        frmData.tabData(6).Sort "0,1", True, True
                        strUrno = GetFieldValue("RURN", Data, Fields)
                        If frmULDHandling.omAftUrno = strUrno Then
                            frmULDHandling.SetFlightRemark CleanString(frmData.tabData(6).GetFieldValue(llTabLine, "TEXT"), FOR_CLIENT, True)
                        End If
                    End If
                Case "TLXTAB"
                    ttyp = GetFieldValue("TTYP", Data, Fields)
                    styp = GetFieldValue("STYP", Data, Fields)
                    If ttyp = "CPM" Or ttyp = "UCM" Or ttyp = "DLS" Or ttyp = "KRIS" Then
                        ItemNo = GetRealItemNo(Fields, "FLNU")
                        If ItemNo > -1 Then
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            strUrno = Trim(strUrno)
                            If strUrno <> "" And strUrno <> "0" Then
                                HandleTlxBC strUrno
                            End If
                        End If
                    End If
                    ttyp = ""
                    styp = ""
            End Select
        Case "SBC"
            If Fields = "RELULD" Then
                HandleULDUpdate Selection
            End If
        Case "RAC"
            strSkey = ""
        Case "DFR"
            strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
            llTabLine = frmData.tabData(0).GetNextResultLine
            If llTabLine <> -1 Then
                'TO DO Handling of DFR
            End If
        Case "UFR", "UPS", "UPJ"
            HandleLoacUTC Fields, Data
            strUrno = GetFieldValue("URNO", Data, Fields)
            HandleUFR strUrno, Fields, Data
        Case "ISF"
            HandleLoacUTC Fields, Data
            ItemNo = GetRealItemNo(Fields, "FTYP")
            If ItemNo > -1 Then
                strVal = GetRealItem(Data, ItemNo, ",")
                If InStr(strAllFtyps, strVal) > 0 Then 'Check if relevant FTYP
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTifa = GetRealItem(Data, ItemNo, ",")
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTifd = GetRealItem(Data, ItemNo, ",")
                    If (strTifa >= strTimeFrameFrom And strTifa <= strTimeFrameTo) Or _
                       (strTifd >= strTimeFrameFrom And strTifd <= strTimeFrameTo) Then
                       ItemNo = GetRealItemNo(Fields, "SKEY")
                        If ItemNo > -1 Then
                            strSkey = GetRealItem(Data, ItemNo, ",")
                            If strSkey <> "" Then
                                HandleISF strSkey
                            End If
                        End If
                    End If
                End If
            End If
    End Select
'    ServerSignal(1).FillStyle = vbTransparent
'    ServerSignal(1).FillColor = vbGreen
'    ServerSignal(1).Refresh
End Sub
Public Sub ReloadAfttab(strUrno)
    Dim arr() As String
    Dim arrFields() As String
    Dim strWhere As String
    
    arr = Split(frmData.tabData(0).Tag, ";")
    If UBound(arr) = 2 Then
        arrFields = Split(arr(1), ",")
        strWhere = " WHERE URNO=" & strUrno
        frmData.tabData(0).CedaAction "RT", "AFTTAB", arr(1), "", strWhere
        frmData.tabData(0).Sort "1,3", True, True
        frmData.tabData(0).IndexCreate "URNO", 0
        frmData.tabData(0).IndexCreate "RKEY", 1
        frmData.tabData(0).IndexCreate "FLNO", 4
        frmData.tabData(0).IndexCreate "DES3", 20
        frmData.tabData(0).SetInternalLineBuffer True
    End If

End Sub
'------------------------------------------------------------------------
' For flights updates by broadcasts Commands "UFR", "UPS", "UPJ"
'------------------------------------------------------------------------
Public Sub HandleUFR(strUrno As String, strFields As String, strData As String)
    Dim llItemNo As Long
    Dim strAdid As String
    Dim strFtyp As String
    Dim strRet As String
    Dim llTabLine As Long
    Dim llAftLine As Long
    Dim strOldTIFA As String
    Dim strOldTIFD As String
    Dim strTifa As String
    Dim strTifd As String
    Dim selCol As String
    Dim IDXtab As Integer
    Dim strValues As String
    Dim strFLDs As String
    Dim strWhere As String
    Dim strLoa As String
    Dim strOnbl As String
    Dim strOfbl As String
    Dim strLand As String
    Dim strAirb As String
    
    IDXtab = -1
    llItemNo = GetRealItemNo(strFields, "ADID")
    If llItemNo > -1 Then
        strAdid = GetRealItem(strData, llItemNo, ",")
    End If
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
    llAftLine = frmData.tabData(0).GetNextResultLine
    'Not found ==> reload with urno directly into frmData.tabData(0)
    If llAftLine = -1 Then
        ReloadAfttab strUrno
    End If
        
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
    llAftLine = frmData.tabData(0).GetNextResultLine
    If llAftLine <> -1 Then
        strOldTIFA = frmData.tabData(0).GetFieldValue(llAftLine, "TIFA")
        strOldTIFD = frmData.tabData(0).GetFieldValue(llAftLine, "TIFD")
        frmData.tabData(0).SetFieldValues llAftLine, strFields, strData
        frmData.tabData(0).Refresh
        strTifa = frmData.tabData(0).GetFieldValue(llAftLine, "TIFA")
        strTifd = frmData.tabData(0).GetFieldValue(llAftLine, "TIFD")
        strFtyp = frmData.tabData(0).GetFieldValue(llAftLine, "FTYP")
        strFtyp = Trim(strFtyp)
        If strAdid = "A" Then IDXtab = 0
        If strAdid = "D" Then IDXtab = 1
        If strFtyp = "D" Or strFtyp = "" Or strFtyp = "N" Then ' must be deleted everywhere
            frmData.tabData(0).DeleteLine llAftLine
            frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
            If IDXtab > -1 Then
                strRet = tabFlights(IDXtab).GetLinesByIndexValue("URNO", strUrno, 0)
                llTabLine = tabFlights(IDXtab).GetNextResultLine
                If llTabLine > -1 Then
                    tabFlights(IDXtab).DeleteLine llTabLine
                    selCol = tabFlights(IDXtab).GetSelectedColumns
                    If tabFlights(IDXtab).SortOrderASC = True Then
                        tabFlights(IDXtab).Sort selCol, True, True
                    Else
                        tabFlights(IDXtab).Sort selCol, False, True
                    End If
                    'tabFlights(IDXtab).IndexDestroy "URNO"
                    tabFlights(IDXtab).IndexCreate "URNO", 9
                    tabFlights(IDXtab).SetInternalLineBuffer True
                End If
            End If
            If omCurrentSelectedUrno = strUrno Then
                ' to avoid update of frmULDHandling, cause the flight does not longer
                ' exist, just close the dialog
                frmULDHandling.Hide
            End If
        Else
            'TO Do update the tabFlights(x)
            If strAdid = "A" Then 'Handle the tabFlights(0) = Arrival
                strRet = tabFlights(0).GetLinesByIndexValue("URNO", strUrno, 0)
                llTabLine = tabFlights(0).GetNextResultLine
                If llTabLine > -1 Then
                    strFLDs = "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM',ULD,URNO"
                    strValues = frmData.tabData(0).GetFieldValues(llAftLine, "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM'") + ",," + strUrno
                    tabFlights(0).SetFieldValues llTabLine, strFLDs, strValues
                    strLand = frmData.tabData(0).GetFieldValue(llAftLine, "LAND")
                    strOnbl = frmData.tabData(0).GetFieldValue(llAftLine, "ONBL")
                    strLand = Trim(strLand)
                    strOnbl = Trim(strOnbl)
                    If strLand = "" And strOnbl = "" Then
                        tabFlights(0).SetFieldValues llTabLine, "TIFA", ""
                    Else
                        If strOnbl <> "" Then
                            tabFlights(0).SetFieldValues llTabLine, "TIFA", strOnbl
                        Else
                            tabFlights(0).SetFieldValues llTabLine, "TIFA", strLand
                        End If
                    End If
                    If HasFlightCPM(strUrno) = True Then
                        tabFlights(0).SetFieldValues llTabLine, "'CPM'", "Y"
                    End If
                    If HasFlightULD(strUrno) = True Then
                        tabFlights(0).SetFieldValues llTabLine, "ULD", "Y"
                    Else
                        tabFlights(0).SetFieldValues llTabLine, "ULD", ""
                    End If
                    'Check for unconfirmed conflicts due to new times of flight
                    If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                        tabFlights(0).SetLineColor llTabLine, vbBlack, vbRed
                    Else
                        tabFlights(0).SetLineColor llTabLine, vbBlack, vbWhite
                    End If
                Else
                    strValues = frmData.tabData(0).GetFieldValues(llAftLine, "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM'") + ",," + frmData.tabData(0).GetFieldValues(llAftLine, "URNO")
                    If IsPassArrivalFilter(llAftLine) = True Then
                        tabFlights(0).InsertTextLine strValues, False
                        strLand = frmData.tabData(0).GetFieldValue(llAftLine, "LAND")
                        strOnbl = frmData.tabData(0).GetFieldValue(llAftLine, "ONBL")
                        strLand = Trim(strLand)
                        strOnbl = Trim(strOnbl)
                        If strLand = "" And strOnbl = "" Then
                            tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "TIFA", ""
                        Else
                            If strOnbl <> "" Then
                                tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "TIFA", strOnbl
                            Else
                                tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "TIFA", strLand
                            End If
                        End If
                        If HasFlightCPM(strUrno) = True Then
                            tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "'CPM'", "Y"
                        End If
                        If HasFlightULD(strUrno) = True Then
                            tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "ULD", "Y"
                        Else
                            tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "ULD", ""
                        End If
                        If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                            tabFlights(0).SetLineColor tabFlights(0).GetLineCount - 1, vbBlack, vbRed
                        End If
                        'And resort it
                        If tabFlights(0).SortOrderASC = True Then
                            tabFlights(0).Sort CStr(tabFlights(0).CurrentSortColumn), True, True
                        Else
                            tabFlights(0).Sort CStr(tabFlights(0).CurrentSortColumn), False, True
                        End If
                    End If
                End If 'if found in arrival tab
            End If
            If strAdid = "D" Then 'Handle the tabFlights(1) = Departure
                strRet = tabFlights(1).GetLinesByIndexValue("URNO", strUrno, 0)
                llTabLine = tabFlights(1).GetNextResultLine
                If llTabLine > -1 Then
                    strFLDs = "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM',ULD,URNO"
                    strValues = frmData.tabData(0).GetFieldValues(llAftLine, "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM'") + ",," + strUrno
                    tabFlights(1).SetFieldValues llTabLine, strFLDs, strValues
                    strOfbl = frmData.tabData(0).GetFieldValue(llAftLine, "OFBL")
                    strAirb = frmData.tabData(0).GetFieldValue(llAftLine, "AIRB")
                    strOfbl = Trim(strOfbl)
                    strAirb = Trim(strAirb)
                    If strAirb = "" And strOfbl = "" Then
                        tabFlights(1).SetFieldValues llTabLine, "TIFD", ""
                    Else
                        If strAirb <> "" Then
                            tabFlights(1).SetFieldValues llTabLine, "TIFD", strAirb
                        Else
                            tabFlights(1).SetFieldValues llTabLine, "TIFD", strOfbl
                        End If
                    End If
                    If HasFlightCPM(strUrno) = True Then
                        tabFlights(1).SetFieldValues llTabLine, "'CPM'", "Y"
                    End If
                    If HasFlightULD(strUrno) = True Then
                        tabFlights(1).SetFieldValues llTabLine, "ULD", "Y"
                    Else
                        tabFlights(1).SetFieldValues llTabLine, "ULD", ""
                    End If
                    'Check for unconfirmed conflicts due to new times of flight
                    If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                        tabFlights(1).SetLineColor llTabLine, vbBlack, vbRed
                    Else
                        If HasOffloads(strUrno) = True Then
                            tabFlights(1).SetLineColor llTabLine, vbBlack, vbCyan
                        Else
                            tabFlights(1).SetLineColor llTabLine, vbBlack, vbWhite
                        End If
                    End If
                Else
                    If IsPassDepartureFilter(llAftLine) = True Then
                        strValues = frmData.tabData(0).GetFieldValues(llAftLine, "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM'") + ",,," + frmData.tabData(0).GetFieldValues(llAftLine, "URNO")
                        tabFlights(1).InsertTextLine strValues, False
                        strOfbl = frmData.tabData(0).GetFieldValue(llAftLine, "OFBL")
                        strAirb = frmData.tabData(0).GetFieldValue(llAftLine, "AIRB")
                        strOfbl = Trim(strOfbl)
                        strAirb = Trim(strAirb)
                        If strAirb = "" And strOfbl = "" Then
                            tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", ""
                        Else
                            If strAirb <> "" Then
                                tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strAirb
                            Else
                                tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strOfbl
                            End If
                        End If
                        strLoa = HasFlightLoaInfo(strUrno, "CPM,KRI")
                        tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "'CPM',KRISCOM", strLoa
                        If HasFlightULD(strUrno) = True Then
                            tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "ULD", "Y"
                        End If
                        If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                            tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbRed
                        Else
                            If HasOffloads(strUrno) = True Then
                                tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbCyan
                            Else
                                tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbWhite
                            End If
                        End If
                        'And resort it
                        If tabFlights(1).SortOrderASC = True Then
                            tabFlights(1).Sort CStr(tabFlights(1).CurrentSortColumn), True, True
                        Else
                            tabFlights(1).Sort CStr(tabFlights(1).CurrentSortColumn), False, True
                        End If
                    End If
                End If 'if flight found in departure tab
            End If
            If omCurrentSelectedUrno = strUrno Then
                frmULDHandling.SetData strUrno
            End If
        End If
    End If
    tabFlights(0).Refresh
    tabFlights(1).Refresh
End Sub

'-----------------------------------------------------
' 1. Reloads the AFTTAB records for this SKEY, use the
'    frmData.tabReread for tmepory storage and
'    reorgs the AFTdata in frmData.tabData(0)
' 2. checks the flight with FLNU for connection conflicts
'    and redraws the bar in the charts
'-----------------------------------------------------
Public Sub HandleISF(strSkey As String)
    Dim strWhere As String
    Dim strAdid As String
    Dim llLineNo As Long
    Dim llCurrLine As Long
    Dim strRet As String
    Dim cnt As Long
    Dim strUrno As String
    Dim i As Long
    Dim llTabLine As Long
    Dim strFieldList As String
    Dim strLineValues As String
    Dim selCol As String
    Dim strOnbl As String
    Dim strOfbl As String
    Dim strLand As String
    Dim strAirb As String
    
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(0).HeaderString
    frmData.tabReread.HeaderLengthString = frmData.tabData(0).HeaderLengthString
    frmData.tabReread.LogicalFieldList = frmData.tabData(0).LogicalFieldList
    strFieldList = frmData.tabReread.LogicalFieldList
    frmData.tabReread.EnableHeaderSizing True
    
    frmData.tabReread.CedaServerName = strServer
    frmData.tabReread.CedaPort = "3357"
    frmData.tabReread.CedaHopo = strHopo
    frmData.tabReread.CedaCurrentApplication = "ULD-Manager" & "," & frmData.GetApplVersion(True)
    frmData.tabReread.CedaTabext = "TAB"
    frmData.tabReread.CedaUser = strCurrentUser
    frmData.tabReread.CedaWorkstation = frmData.ULogin.GetWorkStationName 'frmData.aUfis.GetWorkStationName
    frmData.tabReread.CedaSendTimeout = "3"
    frmData.tabReread.CedaReceiveTimeout = "240"
    frmData.tabReread.CedaRecordSeparator = Chr(10)
    frmData.tabReread.CedaIdentifier = "ULDMGR"
    '" (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R')"
    'strWhere = "WHERE SKEY=" + strSkey + " AND (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R')"
    'amended by kkh on 11/04/2008
    strWhere = "WHERE SKEY=" + strSkey + " AND (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S','O','OO','OS','SO') AND ADID IN ('A','B') OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S','O','OO','OS','SO') AND ADID IN ('D','B')"
    
    frmData.tabReread.CedaAction "RT", "AFTTAB", frmData.tabReread.HeaderString, "", strWhere
    
    cnt = frmData.tabReread.GetLineCount - 1
    For i = 0 To cnt
        strUrno = frmData.tabReread.GetFieldValue(i, "URNO")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
        llLineNo = frmData.tabData(0).GetNextResultLine
        strLineValues = frmData.tabReread.GetLineValues(i)
        If llLineNo > -1 Then
            frmData.tabData(0).SetFieldValues llLineNo, strFieldList, strLineValues
        Else
            frmData.tabData(0).InsertTextLine strLineValues, False
        End If
        frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
        
        'TO DO: Check for tabFlights
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
        llLineNo = frmData.tabData(0).GetNextResultLine
        If llLineNo > -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llLineNo, "ADID")
            If strAdid = "A" Then 'Handle the tabFlights(0) = Arrival
                strLineValues = frmData.tabData(0).GetFieldValues(llLineNo, "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM'") + ",," + strUrno
                tabFlights(0).InsertTextLine strLineValues, False
                llTabLine = tabFlights(0).GetLineCount - 1
                strLand = frmData.tabData(0).GetFieldValue(llLineNo, "LAND")
                strOnbl = frmData.tabData(0).GetFieldValue(llLineNo, "ONBL")
                strLand = Trim(strLand)
                strOnbl = Trim(strOnbl)
                If strLand = "" And strOnbl = "" Then
                    tabFlights(0).SetFieldValues llTabLine, "TIFA", ""
                Else
                    If strOnbl <> "" Then
                        tabFlights(0).SetFieldValues llTabLine, "TIFA", strOnbl
                    Else
                        tabFlights(0).SetFieldValues llTabLine, "TIFA", strLand
                    End If
                End If
                If HasFlightCPM(strUrno) = True Then
                    tabFlights(0).SetFieldValues llTabLine, "'CPM'", "Y"
                End If
                If HasFlightULD(strUrno) = True Then
                    tabFlights(0).SetFieldValues llTabLine, "ULD", "Y"
                Else
                    tabFlights(0).SetFieldValues llTabLine, "ULD", ""
                End If
            End If
            If strAdid = "D" Then 'Handle the tabFlights(1) = Departure
                strLineValues = frmData.tabData(0).GetFieldValues(llLineNo, "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM'") + ",," + strUrno
                tabFlights(1).InsertTextLine strLineValues, False
                llTabLine = tabFlights(0).GetLineCount - 1
                strOfbl = frmData.tabData(0).GetFieldValue(llLineNo, "OFBL")
                strAirb = frmData.tabData(0).GetFieldValue(llLineNo, "AIRB")
                strOfbl = Trim(strOfbl)
                strAirb = Trim(strAirb)
                If strAirb = "" And strOfbl = "" Then
                    tabFlights(1).SetFieldValues llTabLine, "TIFD", ""
                Else
                    If strAirb <> "" Then
                        tabFlights(1).SetFieldValues llTabLine, "TIFD", strAirb
                    Else
                        tabFlights(1).SetFieldValues llTabLine, "TIFD", strOfbl
                    End If
                End If
                If HasFlightCPM(strUrno) = True Then
                    tabFlights(1).SetFieldValues llTabLine, "'CPM'", "Y"
                End If
                If HasFlightULD(strUrno) = True Then
                    tabFlights(1).SetFieldValues llTabLine, "ULD", "Y"
                Else
                    tabFlights(1).SetFieldValues llTabLine, "ULD", ""
                End If
            End If
        End If
    Next i
    'resort and refresh the tabFlights(x)
    For i = 0 To 1
        selCol = tabFlights(i).GetSelectedColumns
        If tabFlights(i).SortOrderASC = True Then
            tabFlights(i).Sort selCol, True, True
        Else
            tabFlights(i).Sort selCol, False, True
        End If
        'tabFlights(i).IndexDestroy "URNO"
        tabFlights(i).IndexCreate "URNO", 9
        tabFlights(i).SetInternalLineBuffer True
        tabFlights(i).Refresh
    Next i
End Sub

Public Sub HandleLoacUTC(Fields As String, Data As String)
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Integer
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    Dim idxFld As Long
    Dim OldData As String
    
    OldData = Data
    
    If TimesInUTC = False Then
        fldCnt = ItemCount(strAftTimeFields, ",")
        For i = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(i), ",")
            idxFld = GetRealItemNo(Fields, strField)
            If idxFld > -1 Then
                strVal = GetRealItem(Data, idxFld, ",")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    myDat = DateAdd("h", UTCOffset, myDat)
                    strVal = Format(myDat, "YYYYMMDDhhmmss")
                    SetItem Data, idxFld, ",", strVal
                End If
            End If
        Next i
    End If
End Sub
Public Sub HandleULDUpdate(strFlnu As String)
    Dim strRet As String
    Dim llLine As String
    Dim i As Long
    Dim strValues As String
    Dim strWhere As String
    Dim cnt As Long
    Dim cntNewUldData As Long
    Dim strMyLines As String
    Dim strUldCheckValue As String
    Dim strAdid As String
    Dim strTifa As String
    Dim strTifd As String
    Dim llAftLine As Long
    
    ' Read all ULDs for strFlnu into frmData.tabReread
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(5).HeaderString
    frmData.tabReread.LogicalFieldList = frmData.tabData(5).LogicalFieldList
    frmData.tabReread.HeaderLengthString = frmData.tabData(5).HeaderLengthString
    frmData.tabReread.EnableHeaderSizing True
    strWhere = "WHERE FLNU=" & strFlnu
    frmData.tabReread.CedaAction "RT", "ULDTAB", frmData.tabReread.HeaderString, "", strWhere
    
    cntNewUldData = frmData.tabReread.GetLineCount
    ' Delete all existing from frmData.tabData(5) = ULDTAB entries
    llLine = -1
    strRet = frmData.tabData(5).GetLinesByIndexValue("FLNU", strFlnu, 0)
    llLine = frmData.tabData(5).GetNextResultLine
    While llLine > -1
        strMyLines = strMyLines & CStr(llLine) & ","
        llLine = frmData.tabData(5).GetNextResultLine
    Wend
    If Len(strMyLines) > 0 Then
        strMyLines = left(strMyLines, Len(strMyLines) - 1)
    End If
    
    'Just get the original Data
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
    llAftLine = frmData.tabData(0).GetNextResultLine
    If llAftLine > -1 Then
        strTifd = frmData.tabData(0).GetFieldValue(llAftLine, "TIFD")
        strTifa = frmData.tabData(0).GetFieldValue(llAftLine, "TIFA")
    End If

    For i = ItemCount(strMyLines, ",") - 1 To 0 Step -1
        strRet = GetRealItem(strMyLines, CInt(i), ",")
        If strRet <> "" Then
            llLine = Val(strRet)
            frmData.tabData(5).DeleteLine llLine
        End If
    Next i
    ' after delete insert the new values into frmData.tabData(5)
    For i = 0 To frmData.tabReread.GetLineCount - 1
        strValues = frmData.tabReread.GetLineValues(i)
        frmData.tabData(5).InsertTextLine strValues, False
    Next i
    frmData.tabData(5).Sort "0,1", True, True
    frmData.tabData(5).Refresh

    ' Update the arrival/departure list in the frmMain
    ' with checkbox for ULD-flag
    If cntNewUldData > 0 Then
        strUldCheckValue = "Y"
    Else
        strUldCheckValue = ""
    End If
    llLine = -1
    strRet = tabFlights(0).GetLinesByIndexValue("URNO", strFlnu, 0)
    llLine = tabFlights(0).GetNextResultLine
    If llLine > -1 Then
        tabFlights(0).SetFieldValues llLine, "ULD", strUldCheckValue
        strAdid = "A"
        'strTifa = tabFlights(0).GetFieldValue(llLine, "TIFA")
        If HasFlightUnconfirmedULDs(strFlnu, strTifa, "", strAdid) = True Then
            tabFlights(0).SetLineColor llLine, vbBlack, vbRed
        Else
            tabFlights(0).SetLineColor llLine, vbBlack, vbWhite
        End If
        tabFlights(0).Refresh
    End If
    strRet = tabFlights(1).GetLinesByIndexValue("URNO", strFlnu, 0)
    llLine = tabFlights(1).GetNextResultLine
    If llLine > -1 Then
        tabFlights(1).SetFieldValues llLine, "ULD", strUldCheckValue
        strAdid = "D"
        'strTifd = tabFlights(1).GetFieldValue(llLine, "TIFD")
        If HasFlightUnconfirmedULDs(strFlnu, "", strTifd, strAdid) = True Then
            tabFlights(1).SetLineColor llLine, vbBlack, vbRed
        Else
            If HasOffloads(strFlnu) = True Then
                tabFlights(1).SetLineColor llLine, vbBlack, vbCyan
            Else
                tabFlights(1).SetLineColor llLine, vbBlack, vbWhite
            End If
        End If
        tabFlights(1).Refresh
    End If
    ' examine if the current open ULD-Dialog is open with the strFlnu
    ' and update it
    If frmULDHandling.omAftUrno = strFlnu Then
        frmULDHandling.tabUld.ResetContent
        frmULDHandling.SetData strFlnu
        frmULDHandling.Show , Me
    End If
End Sub

'-----------------------------------------------------
' Reloads the LOATAB records for this FLNU and
' reorgs the LOATAB in frmData.tabData(3)
' checks the flight with FLNU for connection conflicts
' and redraws the bar in the charts
'-----------------------------------------------------
Sub HandleTlxBC(strFlnu As String)
    Dim strRet As String
    Dim llLine As Long
    Dim i As Integer
    
    ReloadLOATABForFlnu strFlnu
    For i = 0 To 1
        strRet = tabFlights(i).GetLinesByIndexValue("URNO", strFlnu, 0)
        llLine = tabFlights(i).GetNextResultLine
        If llLine <> -1 Then
            If HasFlightCPM(strFlnu) Then
                tabFlights(i).SetFieldValues llLine, "'CPM'", "Y"
            Else
                tabFlights(i).SetFieldValues llLine, "'CPM'", ""
            End If
            If HasFlightULD(strFlnu) Then
                tabFlights(i).SetFieldValues llLine, "ULD", "Y"
            Else
                tabFlights(i).SetFieldValues llLine, "ULD", ""
            End If
            tabFlights(i).Refresh
        End If
    Next i
End Sub

'------------------------------------------------
' Reorgs the loatab for a AFT.URNO or a TLX.FLNU
'------------------------------------------------
Public Sub ReloadLOATABForFlnu(strFlnu As String)
    Dim strRet As String
    Dim cnt As Long
    Dim llLine As Long
    Dim i As Long
    Dim strWhere As String
    Dim strMyLines As String
    
'*** delete all data concerning the AFT.URNO=LOATAB.FLNU
    llLine = -1
    strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strFlnu, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine > -1
        strMyLines = strMyLines & CStr(llLine) & ","
        llLine = frmData.tabData(3).GetNextResultLine
    Wend
    If Len(strMyLines) > 0 Then
        strMyLines = left(strMyLines, Len(strMyLines) - 1)
    End If
    For i = ItemCount(strMyLines, ",") - 1 To 0 Step -1
        strRet = GetRealItem(strMyLines, CInt(i), ",")
        If strRet <> "" Then
            llLine = Val(strRet)
            frmData.tabData(3).DeleteLine llLine
        End If
    Next i
    
'*** Read the data from LOATAB and insert it into the tabReread
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(3).HeaderString
    frmData.tabReread.LogicalFieldList = frmData.tabData(3).LogicalFieldList
    frmData.tabReread.HeaderLengthString = frmData.tabData(3).HeaderLengthString
    frmData.tabReread.EnableHeaderSizing True
    
    strWhere = "WHERE FLNU=" + strFlnu + " AND TYPE='ULD'"
    frmData.tabReread.CedaAction "RT", "LOATAB", frmData.tabReread.HeaderString, "", strWhere
    frmData.tabReread.Sort "1,2", True, True ' This reorgs the indexes as well!
    cnt = frmData.tabReread.GetLineCount
    If cnt > 0 Then
        frmData.tabData(3).InsertBuffer frmData.tabReread.GetBuffer(0, frmData.tabReread.GetLineCount - 1, vbLf), vbLf
    End If
    frmData.tabData(3).Sort "1,2", True, True  ' This reorgs the indexes as well!
    cnt = frmData.tabData(3).GetLineCount
    frmData.lblRecCount(3) = "Table: " + "LOATAB" + "  Records: " + CStr(cnt)

    frmData.tabReread.ResetContent
    
End Sub



Public Sub InitConfigData()
    Dim i As Integer
End Sub

Private Sub cbShowArrivalGrid_Click()
    Form_Resize
End Sub

Private Sub cbShowDepartureGrid_Click()
    Form_Resize
End Sub

Private Sub Check1_Click()

End Sub

Private Sub cbToolbar_Click(Index As Integer)
    Dim rpt As New rptFlights
    Dim myDate As Date
    Dim fDat As Date, tDat As Date
    Dim strCtlNames As String
    Dim myControls() As Control
    
    
    Select Case (Index)
        Case 0 '*** Load was pressed
            If cbToolbar(Index).Value = 1 Then
                cbToolbar(Index).BackColor = vbGreen
                frmStartup.Show
                frmLoad.Show vbModal, frmStartup
                If frmLoad.wasCancel = True Then
                    cbToolbar(Index).Value = 0
                    cbToolbar(Index).BackColor = vbButtonFace
                    Unload frmStartup
                    Exit Sub
                End If
                frmData.LoadData
                Unload frmStartup
                'frmMain.Show
                myDate = CedaFullDateToVb(strReadLoadFrom)
                txtArrDateFrom.Text = Format(myDate, "dd.mm.yyyy")
                txtDepDateFrom.Text = Format(myDate, "dd.mm.yyyy")
                myDate = CedaFullDateToVb(strReadLoadTo)
                txtArrDateTo.Text = Format(myDate, "dd.mm.yyyy")
                txtDepDateTo.Text = Format(myDate, "dd.mm.yyyy")
                frmMain.ZOrder
                cbToolbar(Index).Value = 0
                cbToolbar(Index).BackColor = vbButtonFace
'                If cbToolbar(5).Value = 0 Then
'                    frmData.ChangeAftToLocal
'                End If
                If cmdArrivalFilter.Value = 1 Then
                    DoArrivalFilter
                Else
                    ShowAllArrivalFlights
                End If
                If cmdDepartureFilter.Value = 1 Then
                    DoDepartureFilter
                Else
                    ShowAllDepartureFligths
                End If
            End If
        Case 1 '*** Print Arr. Report was pressed
            If cbToolbar(Index).Value = 1 Then
                rpt.IDXtab = 0
                rpt.Show , Me
                cbToolbar(Index).Value = 0
            End If
        Case 2 '*** Show Arr. Telex was pressed
            If cbToolbar(Index).Value = 1 Then
                'frmTelex.TabReadTelex strRurn
                ShowTelex 0
                cbToolbar(Index).Value = 0
            End If
        Case 3 '*** Print Dep. Report was pressed
            If cbToolbar(Index).Value = 1 Then
                rpt.IDXtab = 1
                rpt.Show , Me
                cbToolbar(Index).Value = 0
            End If
        Case 4 '*** Show Dep. Telex was pressed
            If cbToolbar(Index).Value = 1 Then
                'frmTelex.TabReadTelex strRurn
                ShowTelex 1
                cbToolbar(Index).Value = 0
            End If
        Case 5 '*** UTC was pressed
            Screen.MousePointer = vbHourglass
            If cbToolbar(Index).Value = 1 Then
                cbToolbar(Index).BackColor = vbGreen
                If isInit = True Then
                    TimesInUTC = True
                    frmData.ChangeAftToUTC
                End If
            Else
                TimesInUTC = False
                cbToolbar(Index).BackColor = vbButtonFace
                frmData.ChangeAftToLocal
            End If
            If cmdArrivalFilter.Value = 1 Then
                DoArrivalFilter
            Else
                ShowAllArrivalFlights
            End If
            If cmdDepartureFilter.Value = 1 Then
                DoDepartureFilter
            Else
                ShowAllDepartureFligths
            End If
            Screen.MousePointer = 0
        Case 6 '*** Setup was selected
            If cbToolbar(Index).Value = 1 Then
                cbToolbar(Index).BackColor = vbGreen
                frmSetup.Show vbModal, Me
                cbToolbar(Index).Value = 0
                cbToolbar(Index).BackColor = vbButtonFace
            End If
        Case 7
            If cbToolbar(Index).Value = 1 Then
                cbToolbar(Index).BackColor = vbGreen
                strCtlNames = "BcProxy.exe," & Replace(MyBC.Version, ",", ".", 1, -1, vbBinaryCompare) & "," & MyBC.BuildDate & ",Out-process server for broadcasts" & vbLf
                MyAboutBox.SetLifeStyle = True
                MyAboutBox.SetComponents myControls, strCtlNames
                MyAboutBox.Show vbModal, Me
                cbToolbar(Index).Value = 0
                cbToolbar(Index).BackColor = vbButtonFace
            End If
        Case Else
            'Do nothing
    End Select
    If TimesInUTC = True Then
        fDat = CedaFullDateToVb(strReadLoadFrom)
        tDat = CedaFullDateToVb(strReadLoadTo)
    Else
        fDat = CedaFullDateToVb(strReadLoadFrom)
        tDat = CedaFullDateToVb(strReadLoadTo)
        fDat = DateAdd("h", UTCOffset, fDat)
        tDat = DateAdd("h", UTCOffset, tDat)
    End If
    Me.Caption = "ULD-Manager: Loaded [" & fDat & "] - [" & tDat & "]" & "    Server: " & strServer
End Sub

Public Sub ShowTelex(FltTabIdx As Integer)
    Dim llCurrLine As Long
    Dim llLine As Long
    Dim strRet As String
    Dim strAftUrno As String
    Dim strUrnoList As String
    
    tabTmp.ResetContent
    tabTmp.HeaderString = "URNO"
    tabTmp.HeaderLengthString = "100"
    tabTmp.LogicalFieldList = "URNO"
    tabTmp.SetUniqueFields "URNO"
    
    llCurrLine = tabFlights(FltTabIdx).GetCurrentSelected
    If llCurrLine > -1 Then
        strAftUrno = tabFlights(FltTabIdx).GetFieldValue(llCurrLine, "URNO")
        strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strAftUrno, 0)
        llLine = frmData.tabData(3).GetNextResultLine
        strUrnoList = ""
        While llLine > -1
            tabTmp.InsertTextLine frmData.tabData(3).GetFieldValue(llLine, "RURN"), False
            llLine = frmData.tabData(3).GetNextResultLine
        Wend
        strUrnoList = tabTmp.GetBuffer(0, tabTmp.GetLineCount - 1, ",")
        frmTelex.ReadTelexes strUrnoList
        frmTelex.Show , Me
    End If
End Sub

Private Sub cmdAlcArrAll_Click()
    tabArrAirlines(1).ResetContent
    tabArrAirlines(1).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    'tabArrAirlines(1).Sort "0,1", True, True
    tabArrAirlines(0).ResetContent
    tabArrAirlines(0).Refresh
    tabArrAirlines(1).Refresh
End Sub

Private Sub cmdAlcArrNone_Click()
    tabArrAirlines(0).ResetContent
    tabArrAirlines(0).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    'tabArrAirlines(0).Sort "0,1", True, True
    tabArrAirlines(1).ResetContent
    tabArrAirlines(0).Refresh
    tabArrAirlines(1).Refresh
End Sub

Private Sub cmdAlcDepAll_Click()
    tabDepAirlines(1).ResetContent
    tabDepAirlines(1).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    'tabDepAirlines(1).Sort "0,1", True, True
    tabDepAirlines(0).ResetContent
    tabDepAirlines(0).Refresh
    tabDepAirlines(1).Refresh
End Sub

Private Sub cmdAlcDepNone_Click()
    tabDepAirlines(0).ResetContent
    tabDepAirlines(0).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    'tabDepAirlines(0).Sort "0,1", True, True
    tabDepAirlines(1).ResetContent
    tabDepAirlines(0).Refresh
    tabDepAirlines(1).Refresh
End Sub

Private Sub cmdArrivalFilter_Click()
    If cmdArrivalFilter.Value = 1 Then
        cmdArrivalFilter.BackColor = vbGreen 'colGreen
        DoArrivalFilter
    Else
        ShowAllArrivalFlights
        cmdArrivalFilter.BackColor = vbButtonFace
    End If
End Sub

Public Sub ShowAllArrivalFlights()
    Dim strAdid As String
    Dim strValues As String
    Dim i As Long
    Dim selCols As String
    Dim strUrno As String
    Dim strTifa As String
    Dim strTifd As String
    Dim llTabLine As Long
    Dim strLand As String
    Dim strOnbl As String
    
    tabFlights(0).ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        strValues = ""
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If strAdid = "A" Then
            strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
            strTifa = frmData.tabData(0).GetFieldValue(i, "TIFA")
            strValues = frmData.tabData(0).GetFieldValues(i, "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM'") + ",," + frmData.tabData(0).GetFieldValues(i, "URNO")
            tabFlights(0).InsertTextLine strValues, False
            llTabLine = tabFlights(0).GetLineCount - 1
            strLand = frmData.tabData(0).GetFieldValue(i, "LAND")
            strOnbl = frmData.tabData(0).GetFieldValue(i, "ONBL")
            strLand = Trim(strLand)
            strOnbl = Trim(strOnbl)
            If strLand = "" And strOnbl = "" Then
                tabFlights(0).SetFieldValues llTabLine, "TIFA", ""
            Else
                If strOnbl <> "" Then
                    tabFlights(0).SetFieldValues llTabLine, "TIFA", strOnbl
                Else
                    tabFlights(0).SetFieldValues llTabLine, "TIFA", strLand
                End If
            End If
            If HasFlightCPM(strUrno) = True Then
                tabFlights(0).SetFieldValues llTabLine, "'CPM'", "Y"
            End If
            If HasFlightULD(strUrno) = True Then
                tabFlights(0).SetFieldValues llTabLine, "ULD", "Y"
            Else
                tabFlights(0).SetFieldValues llTabLine, "ULD", ""
            End If
            If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                tabFlights(0).SetLineColor llTabLine, vbBlack, vbRed
            Else
                If HasOffloads(strUrno) = True Then
                    tabFlights(1).SetLineColor llTabLine, vbBlack, vbCyan
                Else
                    tabFlights(1).SetLineColor llTabLine, vbBlack, vbWhite
                End If
            End If
        End If
    Next i
    tabFlights(0).AutoSizeColumns
    selCols = tabFlights(0).GetSelectedColumns
    If selCols <> "" Then
        tabFlights(0).Sort selCols, True, True
    Else
        selCols = ""
    End If
    'tabFlights(0).IndexDestroy "URNO"
    tabFlights(0).IndexCreate "URNO", 9
    tabFlights(0).SetInternalLineBuffer True
    
    tabFlights(0).AutoSizeColumns
    tabFlights(0).Refresh
    lblGridCaption(0).Caption = "Arrival Schedule (" & tabFlights(0).GetLineCount & ")"
End Sub

Public Sub ShowAllDepartureFligths()
    Dim strAdid As String
    Dim strValues As String
    Dim i As Long
    Dim selCols As String
    Dim strUrno As String
    Dim strLoa As String
    Dim strTifa As String
    Dim strTifd As String
    Dim strFlno As String
    Dim strOnbl As String
    Dim strOfbl As String
    Dim strLand As String
    Dim strAirb As String
    
    
    frmData.tabData(3).Sort "1,2", True, True

    tabFlights(1).ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        strValues = ""
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If strAdid = "D" Then
            strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
            strTifd = frmData.tabData(0).GetFieldValue(i, "TIFD")
            strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
            If strFlno = "SQ 064" Then
                strFlno = strFlno
            End If
            strValues = frmData.tabData(0).GetFieldValues(i, "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM'") + ",,," + frmData.tabData(0).GetFieldValues(i, "URNO")
            tabFlights(1).InsertTextLine strValues, False
            strOfbl = frmData.tabData(0).GetFieldValue(i, "OFBL")
            strAirb = frmData.tabData(0).GetFieldValue(i, "AIRB")
            strOfbl = Trim(strOfbl)
            strAirb = Trim(strAirb)
            If strAirb = "" And strOfbl = "" Then
                tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", ""
            Else
                If strAirb <> "" Then
                    tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strAirb
                Else
                    tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strOfbl
                End If
            End If
            strLoa = HasFlightLoaInfo(strUrno, "CPM,KRI")
            tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "'CPM',KRISCOM", strLoa
            If HasFlightULD(strUrno) = True Then
                tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "ULD", "Y"
            End If
            If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbRed
            Else
                If HasOffloads(strUrno) = True Then
                    tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbCyan
                Else
                    tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbWhite
                End If
            End If
        End If
    Next i
    tabFlights(1).AutoSizeColumns
    selCols = tabFlights(1).GetSelectedColumns
    If selCols <> "" Then
        tabFlights(1).Sort selCols, True, True
    Else
        selCols = ""
    End If
    tabFlights(1).IndexDestroy "URNO"
    tabFlights(1).IndexCreate "URNO", 10
    tabFlights(1).SetInternalLineBuffer True

    tabFlights(1).AutoSizeColumns
    tabFlights(1).Refresh
    lblGridCaption(1).Caption = "Departure Schedule (" & tabFlights(1).GetLineCount & ")"
End Sub

Private Sub cmdArrToday_Click()
    Dim today As Date
    Dim strDate As String
    Dim strTime As String
    today = Now
    strDate = Format(today, "dd.mm.YYYY")
    txtArrDateFrom = strDate
    txtArrDateTo = strDate
    txtArrTimeFrom = "0000"
    txtArrTimeTo = "2359"
    CheckDateField txtArrDateFrom, "dd.mm.yyyy", False
    CheckDateField txtArrDateTo, "dd.mm.yyyy", False
End Sub

Private Sub cmdDepartureFilter_Click()
    If cmdDepartureFilter.Value = 1 Then
        cmdDepartureFilter.BackColor = vbGreen 'colGreen
        DoDepartureFilter
    Else
        cmdDepartureFilter.BackColor = vbButtonFace
        ShowAllDepartureFligths
    End If
End Sub

Private Sub cmdDepToday_Click()
    Dim today As Date
    Dim strDate As String
    Dim strTime As String
    today = Now
    strDate = Format(today, "dd.mm.YYYY")
    txtDepDateFrom = strDate
    txtDepDateTo = strDate
    txtDepTimeFrom = "0000"
    txtDepTimeTo = "2359"
    CheckDateField txtDepDateFrom, "dd.mm.yyyy", False
    CheckDateField txtDepDateTo, "dd.mm.yyyy", False
End Sub

Private Sub Form_Activate()
    Static allreadyshown As Boolean
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilWidth As Long
    Dim ilHeight As Long
    
    If Not allreadyshown Then
        Me.WindowState = vbNormal
        Me.Height = Screen.Height + 180
        Me.Width = Screen.Width + 180
        DrawObjBackgrounds
        Me.Show
        ilLeft = GetSetting(AppName:="ULD_Manager_Main", _
                            section:="Startup", _
                            Key:="myLeft", _
                            Default:=-1)
        ilTop = GetSetting(AppName:="ULD_Manager_Main", _
                            section:="Startup", _
                            Key:="myTop", _
                            Default:=-1)
        ilWidth = GetSetting(AppName:="ULD_Manager_Main", _
                            section:="Startup", _
                            Key:="myWidth", _
                            Default:=-1)
        ilHeight = GetSetting(AppName:="ULD_Manager_Main", _
                            section:="Startup", _
                            Key:="myHeight", _
                            Default:=-1)
        
        If ilTop = -1 Or ilLeft = -1 Or ilWidth = -1 Or ilHeight = -1 Then
            Me.top = 0
            Me.left = 0
            Me.Height = Screen.Height
            Me.Width = Screen.Width
        Else
            Me.Move ilLeft, ilTop, ilWidth, ilHeight
        End If
        
        Me.Refresh
        allreadyshown = True
        Me.ZOrder
    End If
    timerConflicCheck.Enabled = True
    Form_Resize
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim datFrom As Date
    Dim datTo As Date
    
    
    Dim tmLocal As Date
    Dim tmUTC As Date
    Dim strTime As String
    
    bmFirstUTC = True
    tmLocal = Now
    tmUTC = DateAdd("h", -UTCOffset, tmLocal)
    strTime = Format(tmUTC, "DD.MM.YY-")
    strTime = strTime + Format(tmUTC, "hh:mm") + "z"
    lblTime(0).Caption = strTime
    strTime = Format(tmLocal, "DD.MM.YY-")
    strTime = strTime + Format(tmLocal, "hh:mm")
    lblTime(1).Caption = strTime
    
    isInit = False

    omCurrentSelectedUrno = ""
    Me.top = -Screen.Height - 1000
    Me.left = 0
    omTrueValue = "Y"
    omFalseValue = ""
    
'    datFrom = CedaFullDateToVb(strTimeFrameFrom)
'    datTo = CedaFullDateToVb(strTimeFrameTo)
    datFrom = CedaFullDateToVb(strReadLoadFrom)
    datTo = CedaFullDateToVb(strReadLoadTo)
    Me.Caption = "ULD-Manager: Loaded [" & datFrom & "] - [" & datTo & "]" & "    Server: " & strServer
    MyInit
    cbToolbar(5).Value = 1
    cbToolbar(5).BackColor = vbGreen
    cmdArrToday_Click
    cmdDepToday_Click
    isInit = True
End Sub

Public Sub LoadBCProxy()
    Set MyBC = New BcPrxy
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilHeight As Long
    Dim ilWidth As Long
    
    
    ilLeft = Me.left
    ilTop = Me.top
    ilWidth = Me.Width
    ilHeight = Me.Height
    SaveSetting "ULD_Manager_Main", _
                "Startup", _
                "myLeft", _
                ilLeft
    SaveSetting "ULD_Manager_Main", _
                "Startup", _
                "myTop", ilTop
    SaveSetting "ULD_Manager_Main", _
                "Startup", _
                "myWidth", ilWidth
    SaveSetting "ULD_Manager_Main", _
                "Startup", _
                "myHeight", ilHeight

    If MsgBox("Do you really want to exit the application??", vbYesNo Or vbExclamation) <> vbYes Then
        Cancel = True
    Else
        Cancel = False
        Unload frmData
        End
    End If
End Sub

Private Sub Form_Resize()
    Dim ilW As Long
    Dim ilH As Long
    
    If picBackground(0).Width - 700 > 0 Then
        ServerSignal(0).left = picBackground(0).Width - 700
    End If
    If picBackground(0).Width - 950 > 0 Then
        ServerSignal(1).left = picBackground(0).Width - 950
    End If
    If picBackground(0).Width - 1200 > 0 Then
        ServerSignal(2).left = picBackground(0).Width - 1200
    End If
    
    If Me.ScaleWidth - 30 > 0 Then
        picBackground(0).Width = Me.ScaleWidth - 30
    End If
    If Me.ScaleWidth - 30 - picBackground(2).Width > 0 Then
        picBackground(2).left = Me.ScaleWidth - 30 - picBackground(2).Width
    End If
    If StatusBar1.top - 50 - picBackground(2).top > 0 Then
        picBackground(2).Height = StatusBar1.top - 50 - picBackground(2).top 'picBackground(3).top - 60 - picBackground(2).top
    End If
    If picBackground(2).left - 60 > 0 Then
        picBackground(1).Width = picBackground(2).left - 60
        picSplitter.Width = picBackground(1).Width
    End If
    If StatusBar1.top - 50 - picBackground(1).top > 0 Then
        picBackground(1).Height = StatusBar1.top - 50 - picBackground(1).top   'picBackground(3).top - 60 - picBackground(1).top
    End If
    If (picBackground(1).Height / 2) - (lblEmptyText(0).Height / 2) > 0 Then
        lblEmptyText(0).top = (picBackground(1).Height / 2) - (lblEmptyText(0).Height / 2)
        lblEmptyText(1).top = lblEmptyText(0).top + 30
    End If
    If picBackground(1).Width - 30 > 0 Then
        tabFlights(0).Width = picBackground(1).Width - 60
        tabFlights(1).Width = picBackground(1).Width - 60
    End If
    lblGridCaption(0).Width = picBackground(1).Width
    lblGridCaption(1).Width = picBackground(1).Width
    lblEmptyText(0).Visible = False
    lblEmptyText(1).Visible = False
    If cbShowArrivalGrid.Value = 1 And cbShowDepartureGrid.Value = 1 Then
        picSplitter.Visible = True
        lblGridCaption(1).Visible = True
        tabFlights(1).Visible = True
        picSplitter.Visible = True
        lblGridCaption(0).Visible = True
        tabFlights(0).Visible = True
        If (picBackground(1).Height / 2) - (picSplitter.Height / 2) > 0 Then
            picSplitter.top = (picBackground(1).Height / 2) - (picSplitter.Height / 2)
        End If
        lblGridCaption(0).top = 0
        If picSplitter.top + picSplitter.Height > 0 Then
            lblGridCaption(1).top = picSplitter.top + picSplitter.Height
        End If
        If picSplitter.top - (lblGridCaption(0).Height - lblGridCaption(0).top) - 50 > 0 Then
            tabFlights(0).Height = picSplitter.top - (lblGridCaption(0).Height - lblGridCaption(0).top) - 50
        End If
        If lblGridCaption(1).top + lblGridCaption(1).Height > 0 Then
            tabFlights(1).top = lblGridCaption(1).top + lblGridCaption(1).Height
        End If
        If picBackground(1).Height - (lblGridCaption(1).top + lblGridCaption(1).Height) - 50 > 0 Then
            tabFlights(1).Height = picBackground(1).Height - (lblGridCaption(1).top + lblGridCaption(1).Height) - 50
        End If
    Else
        If cbShowArrivalGrid.Value = 1 Then
            picSplitter.Visible = False
            lblGridCaption(1).Visible = False
            tabFlights(1).Visible = False
            lblGridCaption(0).Visible = True
            tabFlights(0).Visible = True
            lblGridCaption(0).top = 0
            If lblGridCaption(0).top + lblGridCaption(0).Height > 0 Then
                tabFlights(0).top = lblGridCaption(0).top + lblGridCaption(0).Height
            End If
            If picBackground(0).Height - (lblGridCaption(0).top + lblGridCaption(0).Height) > 0 Then
                tabFlights(0).Height = picBackground(1).Height - (lblGridCaption(0).top + lblGridCaption(0).Height)
            End If
            tabFlights(0).Width = picBackground(0).Width
        End If
        If cbShowDepartureGrid.Value = 1 Then
            picSplitter.Visible = False
            lblGridCaption(0).Visible = False
            tabFlights(0).Visible = False
            lblGridCaption(1).Visible = True
            tabFlights(1).Visible = True
            lblGridCaption(1).top = 0
            If lblGridCaption(1).top + lblGridCaption(1).Height > 0 Then
                tabFlights(1).top = lblGridCaption(1).top + lblGridCaption(1).Height
            End If
            If picBackground(1).Height - (lblGridCaption(1).top + lblGridCaption(1).Height) > 0 Then
                tabFlights(1).Height = picBackground(1).Height - (lblGridCaption(1).top + lblGridCaption(1).Height)
            End If
            tabFlights(1).Width = picBackground(1).Width
        End If
        If cbShowArrivalGrid.Value = 0 And cbShowDepartureGrid.Value = 0 Then
            picSplitter.Visible = False
            lblGridCaption(1).Visible = False
            tabFlights(1).Visible = False
            picSplitter.Visible = False
            lblGridCaption(0).Visible = False
            tabFlights(0).Visible = False
            lblEmptyText(0).Visible = True
            lblEmptyText(1).Visible = True
        End If
    End If
    If ((Me.Width - 200) - (lblTime(1).Width)) > 0 Then
        lblTime(1).left = ((Me.Width - 200) - (lblTime(1).Width)) ' - lblTime(1).left))
    End If
    If lblTime(1).left - 32 - lblTime(0).Width > 0 Then
        lblTime(0).left = lblTime(1).left - 32 - lblTime(0).Width
    End If
    
    'DrawObjBackgrounds
End Sub

Public Sub DrawObjBackgrounds()
    DrawBackGround picSplitter, 7, True, True, 100
    DrawBackGround picBackground(0), 7, True, True, 70
    DrawBackGround picBackground(1), 7, True, True, 70
    DrawBackGround picBackground(2), 7, False, True, 40
    DrawBackGround picBackground(3), 7, True, True, 70
    DrawBackGround picBackground(4), 7, True, True, 70
End Sub

Private Sub Label10_Click()
    If cbShowArrivalGrid.Value = 0 Then
        cbShowArrivalGrid.Value = 1
    Else
        cbShowArrivalGrid.Value = 0
    End If
    Form_Resize
End Sub

Private Sub Label11_Click()
    If cbShowDepartureGrid.Value = 0 Then
        cbShowDepartureGrid.Value = 1
    Else
        cbShowDepartureGrid.Value = 0
    End If
    Form_Resize
End Sub

Public Sub MyInit()
    Dim i As Long
    Dim strValues As String
    Dim strAdid As String
    Dim strColor As String
    
    strColor = vbBlue & "," & vbBlue
    
    tabFlights(0).ResetContent
    tabFlights(0).HeaderString = "FLNO,ACT,STA,ETA,Actual,Orig.,POS,CPM-Message,ULDs,URNO"
    tabFlights(0).LogicalFieldList = "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM',ULD,URNO"
    tabFlights(0).CursorDecoration "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM',ULD,URNO", "T,B", "2,2", strColor
    tabFlights(0).ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L"
    tabFlights(0).HeaderLengthString = "100,100,100,100,100,100,100,100,30,100"
    tabFlights(0).ColumnWidthString = "20,5,15,15,15,5,5,5,5,15"
    tabFlights(0).IndexCreate "URNO", 9
    tabFlights(0).LifeStyle = True
    tabFlights(0).SetInternalLineBuffer True
    tabFlights(0).SelectColumnBackColor = RGB(235, 235, 255)
    tabFlights(0).SelectColumnTextColor = vbBlack
    'tabFlights(0).ColumnSelectionLifeStyle = True
    'tabFlights(0).CursorLifeStyle = True

    tabFlights(0).DefaultCursor = False

    tabFlights(0).SetColumnBoolProperty 7, "Y", ""
    tabFlights(0).SetBoolPropertyReadOnly 7, True
    tabFlights(0).SetColumnBoolProperty 8, "Y", ""
    tabFlights(0).SetBoolPropertyReadOnly 8, True
    tabFlights(0).ShowHorzScroller True

    tabFlights(1).ResetContent
    tabFlights(1).HeaderString = "FLNO,ACT,STD,ETD,Actual,Dest.,POS,CPM-Message,KRISCOM,ULDs,Urno"
    tabFlights(1).LogicalFieldList = "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM',KRISCOM,ULD,URNO"
    tabFlights(1).CursorDecoration "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM',KRISCOM,ULD,URNO", "T,B", "2,2", strColor
    tabFlights(1).ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L"
    tabFlights(1).HeaderLengthString = "100,100,100,100,100,100,100,100,80,30,100"
    tabFlights(1).ColumnWidthString = "20,5,15,15,15,5,5,5,5,5,15"
    tabFlights(1).IndexCreate "URNO", 10
    tabFlights(1).LifeStyle = True
    tabFlights(1).SetInternalLineBuffer True
    tabFlights(1).SelectColumnBackColor = RGB(235, 235, 255)
    tabFlights(1).SelectColumnTextColor = vbBlack
    'tabFlights(1).ColumnSelectionLifeStyle = True
    'tabFlights(1).CursorLifeStyle = True
    tabFlights(1).DefaultCursor = False
    tabFlights(1).SetColumnBoolProperty 7, "Y", ""
    tabFlights(1).SetBoolPropertyReadOnly 7, True
    tabFlights(1).SetColumnBoolProperty 8, "Y", ""
    tabFlights(1).SetBoolPropertyReadOnly 8, True
    tabFlights(1).SetColumnBoolProperty 9, "Y", ""
    tabFlights(1).SetBoolPropertyReadOnly 9, True
    tabFlights(1).ShowHorzScroller True
    
    slFonts.Value = 17
    For i = 0 To 1
        tabFlights(i).SetTabFontBold True
        tabFlights(i).FontName = "Courier New"
        tabFlights(i).LineHeight = 17
        tabFlights(i).FontSize = 17
        tabFlights(i).EnableHeaderSizing True
        tabFlights(i).LifeStyle = True
        tabFlights(i).HeaderFontSize = 17
        tabFlights(i).DateTimeSetColumn 3
        tabFlights(i).DateTimeSetColumn 4
        tabFlights(i).AutoSizeByHeader = True

        
        tabFlights(i).DateTimeSetColumnFormat 2, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
        tabFlights(i).DateTimeSetColumnFormat 3, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
        tabFlights(i).DateTimeSetColumnFormat 4, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
    Next i
    For i = 0 To 1
        tabArrAirlines(i).ResetContent
        tabArrAirlines(i).HeaderString = "ALC2,ALC3"
        tabArrAirlines(i).LogicalFieldList = "ALC2,ALC3"
        tabArrAirlines(i).HeaderLengthString = "40,40"
        tabArrAirlines(i).SetInternalLineBuffer True
        tabArrAirlines(i).FontName = "Arial"
        tabArrAirlines(i).FontSize = 14
        tabArrAirlines(i).HeaderFontSize = 14
        tabArrAirlines(i).SetTabFontBold True
        tabArrAirlines(i).CursorLifeStyle = True
        tabArrAirlines(i).LifeStyle = True
        
        tabDepAirlines(i).ResetContent
        tabDepAirlines(i).HeaderString = "ALC2,ALC3"
        tabDepAirlines(i).LogicalFieldList = "ALC2,ALC3"
        tabDepAirlines(i).HeaderLengthString = "40,40"
        tabDepAirlines(i).SetInternalLineBuffer True
        tabDepAirlines(i).FontName = "Arial"
        tabDepAirlines(i).FontSize = 14
        tabDepAirlines(i).HeaderFontSize = 14
        tabDepAirlines(i).SetTabFontBold True
        tabDepAirlines(i).CursorLifeStyle = True
        tabDepAirlines(i).LifeStyle = True
    Next i
    
    For i = 0 To 1
'        tabFlights(i).Sort "2", True, True
        tabFlights(i).ColSelectionAdd 2
    Next i
    
    ShowAllArrivalFlights
    ShowAllDepartureFligths
    
    tabArrAirlines(0).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    tabDepAirlines(0).InsertBuffer frmData.tabData(4).GetBufferByFieldList(0, frmData.tabData(4).GetLineCount - 1, "ALC2,ALC3", vbLf), vbLf
    
    tabFlights(0).Refresh
    tabFlights(1).Refresh
    tabDepAirlines(0).Refresh
    tabArrAirlines(0).Refresh
    tabDepAirlines(1).Refresh
    tabArrAirlines(1).Refresh
    lblGridCaption(0).Caption = "Arrival Schedule (" & tabFlights(0).GetLineCount & ")"
    lblGridCaption(1).Caption = "Departure Schedule (" & tabFlights(1).GetLineCount & ")"
End Sub

Private Sub slFonts_Scroll()
    Dim ilSize As Integer
    Dim i As Integer
    
    ilSize = slFonts.Value
    For i = 0 To 1
        tabFlights(i).LineHeight = ilSize
        tabFlights(i).FontSize = ilSize
        tabFlights(i).HeaderFontSize = ilSize
        tabFlights(i).AutoSizeColumns
    Next i
End Sub

Private Sub tabArrAirlines_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim i As Long
    Dim strVal As String
    Dim idx  As Long
    Dim theKey As String
    
    theKey = Chr(Key)
    theKey = UCase(theKey)
    idx = tabArrAirlines(Index).GetCurrentSelected
    For i = idx + 1 To tabArrAirlines(Index).GetLineCount - 1
        strVal = tabArrAirlines(Index).GetColumnValue(i, 0)
        strVal = Mid(strVal, 1, 1)
        If strVal = theKey Then
            tabArrAirlines(Index).SetCurrentSelection i
            tabArrAirlines(Index).OnVScrollTo i
            i = tabArrAirlines(Index).GetLineCount
        End If
    Next i
End Sub

Private Sub tabArrAirlines_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strValues As String
    
    If LineNo > -1 Then
        If Index = 0 Then
            strValues = tabArrAirlines(0).GetFieldValues(LineNo, "ALC2,ALC3")
            tabArrAirlines(1).InsertTextLine strValues, True
            tabArrAirlines(0).DeleteLine LineNo
        Else
            strValues = tabArrAirlines(1).GetFieldValues(LineNo, "ALC2,ALC3")
            tabArrAirlines(0).InsertTextLine strValues, True
            tabArrAirlines(0).Sort "0,1", True, True
            tabArrAirlines(1).DeleteLine LineNo
        End If
    End If
End Sub

Private Sub tabDepAirlines_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim i As Long
    Dim strVal As String
    Dim idx  As Long
    Dim theKey As String
    
    theKey = Chr(Key)
    theKey = UCase(theKey)
    idx = tabDepAirlines(Index).GetCurrentSelected
    For i = idx + 1 To tabDepAirlines(Index).GetLineCount - 1
        strVal = tabDepAirlines(Index).GetColumnValue(i, 0)
        strVal = Mid(strVal, 1, 1)
        If strVal = theKey Then
            tabDepAirlines(Index).SetCurrentSelection i
            tabDepAirlines(Index).OnVScrollTo i
            i = tabDepAirlines(Index).GetLineCount
        End If
    Next i
End Sub

Private Sub tabDepAirlines_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strValues As String
    
    If LineNo > -1 Then
        If Index = 0 Then
            strValues = tabDepAirlines(0).GetFieldValues(LineNo, "ALC2,ALC3")
            tabDepAirlines(1).InsertTextLine strValues, True
            tabDepAirlines(0).DeleteLine LineNo
        Else
            strValues = tabDepAirlines(1).GetFieldValues(LineNo, "ALC2,ALC3")
            tabDepAirlines(0).InsertTextLine strValues, True
            tabDepAirlines(0).Sort "0,1", True, True
            tabDepAirlines(1).DeleteLine LineNo
        End If
    End If
End Sub

Private Sub tabFlights_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        If ColNo = tabFlights(Index).CurrentSortColumn Then
            If tabFlights(Index).SortOrderASC = True Then
                tabFlights(Index).Sort CStr(ColNo), False, True
            Else
                tabFlights(Index).Sort CStr(ColNo), True, True
            End If
        Else
            tabFlights(Index).Sort CStr(ColNo), True, True
        End If
        cnt = ItemCount(tabFlights(Index).HeaderString, ",")
        For i = 0 To cnt
            tabFlights(Index).ColSelectionRemove i
        Next i
        tabFlights(Index).ColSelectionAdd ColNo
        tabFlights(Index).AutoSizeColumns
        tabFlights(Index).Refresh
    Else
        omCurrentSelectedUrno = tabFlights(Index).GetFieldValue(LineNo, "URNO")
        frmULDHandling.SetData omCurrentSelectedUrno
        frmULDHandling.Show , Me
    End If
End Sub
'--------------------------------------------------------------------------------------------------
    'Check every minute if a not confirmed ULD has exeeded the
    'configured timeframe -x min for departure and +x min for arrival
    'iterate through all flights and check if strBaggageDefault, strMailDefault, strCargoDefault
    'exeeded for this flight. YES => check the related ULDs if there are any and
    'draw the background color of the flight in the grid red
'--------------------------------------------------------------------------------------------------
Private Sub timerConflicCheck_Timer()
    Dim i As Long
    Dim strAdid As String
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strTifa As String
    Dim strTifd As String
    Dim strFlno As String
    Dim blIsCritical As Boolean
    Dim datNow As Date
    Dim strNow As String
    Dim strTisa As String
    Dim strUrno As String
    Dim llLine As Long
    Dim tabIDX As Integer
    
' FOR THE TIME FIELDS
    Dim tmLocal As Date
    Dim tmUTC As Date
    Dim strTime As String
    
    tmLocal = Now
    tmUTC = DateAdd("h", -UTCOffset, tmLocal)
    strTime = Format(tmUTC, "DD.MM.YY-")
    strTime = strTime + Format(tmUTC, "hh:mm") + "z"
    lblTime(0).Caption = strTime
    strTime = Format(tmLocal, "DD.MM.YY-")
    strTime = strTime + Format(tmLocal, "hh:mm")
    lblTime(1).Caption = strTime
' END TIME FIELDS
    
    
    datNow = Now
    If cbToolbar(5).Value = 1 Then
        'We calculate in UTC so now must be UTC
        datNow = DateAdd("h", -UTCOffset, datNow)
    End If
    'tabFlights(0).IndexDestroy "URNO"
    tabFlights(0).IndexCreate "URNO", 9
    'tabFlights(1).IndexDestroy "URNO"
    tabFlights(1).IndexCreate "URNO", 10
    
    'tabFlights(0).Sort
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        blIsCritical = False
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        strTifa = frmData.tabData(0).GetFieldValue(i, "TIFA")
        strTisa = frmData.tabData(0).GetFieldValue(i, "TISA")
        strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
        strTifd = frmData.tabData(0).GetFieldValue(i, "TIFD")
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        If Trim(strFlno) = "SQ 321" Or Trim(strFlno) = "SQ 997" Or Trim(strFlno) = "SQ 973" Then
            strFlno = strFlno
        End If
        If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
            If strAdid = "A" Then
                tabIDX = 0
            Else
                tabIDX = 1
            End If
            tabFlights(tabIDX).GetLinesByIndexValue "URNO", strUrno, 0
            llLine = tabFlights(tabIDX).GetNextResultLine
            If llLine > -1 Then
                tabFlights(tabIDX).SetLineColor llLine, vbBlack, vbRed
            End If
        Else
            tabFlights(tabIDX).GetLinesByIndexValue "URNO", strUrno, 0
            llLine = tabFlights(tabIDX).GetNextResultLine
            If llLine > -1 Then
                tabFlights(tabIDX).SetLineColor llLine, vbBlack, vbWhite
            End If
            If HasOffloads(strUrno) = True Then
                tabFlights(1).SetLineColor llLine, vbBlack, vbCyan
            Else
                tabFlights(1).SetLineColor llLine, vbBlack, vbWhite
            End If
        End If
    Next i
    tabFlights(0).Refresh
    tabFlights(1).Refresh
End Sub

Private Sub txtArrDateFrom_Change()
    CheckDateField txtArrDateFrom, "dd.mm.yyyy", False
    HandleArrivalFilterButton
End Sub

Private Sub txtArrDateTo_Change()
    CheckDateField txtArrDateTo, "dd.mm.yyyy", False
    HandleArrivalFilterButton
End Sub

Private Sub txtArrTimeFrom_Change()
    Dim strDate As String
    strDate = txtArrTimeFrom.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtArrTimeFrom.BackColor = vbRed
        txtArrTimeFrom.ForeColor = vbWhite
    Else
        txtArrTimeFrom.BackColor = vbWhite
        txtArrTimeFrom.ForeColor = vbBlack
    End If
    HandleArrivalFilterButton
End Sub

Private Sub txtArrTimeTo_Change()
    Dim strDate As String
    strDate = txtArrTimeTo.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtArrTimeTo.BackColor = vbRed
        txtArrTimeTo.ForeColor = vbWhite
    Else
        txtArrTimeTo.BackColor = vbWhite
        txtArrTimeTo.ForeColor = vbBlack
    End If
    HandleArrivalFilterButton
End Sub

Private Sub HandleArrivalFilterButton()
    If txtArrDateFrom.BackColor = vbRed Or _
       txtArrTimeFrom.BackColor = vbRed Or _
       txtArrDateTo.BackColor = vbRed Or _
       txtArrTimeTo.BackColor = vbRed Then
       cmdArrivalFilter.Enabled = False
    Else
       cmdArrivalFilter.Enabled = True
    End If
End Sub
Private Sub HandleDepartureFilterButton()
    If txtDepDateFrom.BackColor = vbRed Or _
       txtDepTimeFrom.BackColor = vbRed Or _
       txtDepDateTo.BackColor = vbRed Or _
       txtDepTimeTo.BackColor = vbRed Then
       cmdDepartureFilter.Enabled = False
    Else
       cmdDepartureFilter.Enabled = True
    End If
End Sub
Private Sub txtDepDateFrom_Change()
    CheckDateField txtDepDateFrom, "dd.mm.yyyy", False
    HandleDepartureFilterButton
End Sub

Private Sub txtDepDateTo_Change()
    CheckDateField txtDepDateTo, "dd.mm.yyyy", False
    HandleDepartureFilterButton
End Sub

Public Sub DoArrivalFilter()
    Dim i As Long
    Dim strAdid As String
    Dim strFlno As String
    Dim selCols As String
    Dim strUrno As String
    Dim strTifa As String
    Dim strTifd As String
    Dim blInsertIt As Boolean
    Dim strValues As String
    Dim llLine As Long
    Dim llTabLine As Long
    Dim strLand As String
    Dim strOnbl As String
    
    
    tabFlights(0).ResetContent
    
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        blInsertIt = False
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If strAdid = "A" Then
            strTifa = frmData.tabData(0).GetFieldValue(i, "TIFA")
            strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
            strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
            If strFlno = "SQ 025" Then
                strFlno = "SQ 025"
            End If
            blInsertIt = IsPassArrivalFilter(i)
            If blInsertIt = True Then
                strValues = frmData.tabData(0).GetFieldValues(i, "FLNO,ACT3,STOA,ETAI,TIFA,ORG3,PSTA,'CPM'") + ",," + frmData.tabData(0).GetFieldValues(i, "URNO")
                tabFlights(0).InsertTextLine strValues, False
                llTabLine = tabFlights(0).GetLineCount - 1
                strLand = frmData.tabData(0).GetFieldValue(i, "LAND")
                strOnbl = frmData.tabData(0).GetFieldValue(i, "ONBL")
                strLand = Trim(strLand)
                strOnbl = Trim(strOnbl)
                If strLand = "" And strOnbl = "" Then
                    tabFlights(0).SetFieldValues llTabLine, "TIFA", ""
                Else
                    If strOnbl <> "" Then
                        tabFlights(0).SetFieldValues llTabLine, "TIFA", strOnbl
                    Else
                        tabFlights(0).SetFieldValues llTabLine, "TIFA", strLand
                    End If
                End If

                llLine = tabFlights(0).GetLineCount - 1
                If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                    tabFlights(0).SetLineColor tabFlights(0).GetLineCount - 1, vbBlack, vbRed
                End If
                If HasFlightCPM(strUrno) = True Then
                    tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "'CPM'", "Y"
                Else
                    tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "'CPM'", ""
                End If
                If HasFlightULD(strUrno) = True Then
                    tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "ULD", "Y"
                Else
                    tabFlights(0).SetFieldValues tabFlights(0).GetLineCount - 1, "ULD", ""
                End If
            End If
        End If
    Next i
    selCols = tabFlights(0).GetSelectedColumns
    If selCols <> "" Then
        tabFlights(0).Sort selCols, True, True
    End If
    tabFlights(0).IndexCreate "URNO", 9
    tabFlights(0).SetInternalLineBuffer True
    
    tabFlights(0).AutoSizeColumns
    tabFlights(0).Refresh
    lblGridCaption(0).Caption = "Arrival Schedule (" & tabFlights(0).GetLineCount & ")   Filter active"
End Sub
Public Sub DoDepartureFilter()
    Dim i As Long
    Dim strFrom As String
    Dim strTo As String
    Dim strAlcFilter As String
    Dim fromDat As Date
    Dim toDat As Date
    Dim strAdid As String
    Dim strTifd As String
    Dim strTifa As String
    Dim strAlc2 As String, strAlc3 As String
    Dim strValues As String
    Dim strFlno As String
    Dim selCols As String
    Dim strUrno As String
    Dim strLoa As String
    Dim blInsertIt As Boolean
    Dim strOfbl As String
    Dim strAirb As String
    
    strFrom = txtDepDateFrom.Tag + txtDepTimeFrom
    strTo = txtDepDateTo.Tag + txtDepTimeTo
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    strFrom = Format(fromDat, "YYYYMMDDhhmmss")
    strTo = Format(toDat, "YYYYMMDDhhmmss")
        
    strAlcFilter = tabDepAirlines(1).GetBuffer(0, tabDepAirlines(1).GetLineCount - 1, ",")
    strAlcFilter = "," & strAlcFilter & ","
    tabFlights(1).ResetContent
    
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        blInsertIt = True
        If strAdid = "D" Then
            strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
            strTifd = frmData.tabData(0).GetFieldValue(i, "TIFD")
            strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
            blInsertIt = IsPassDepartureFilter(i)
            If (blInsertIt = True) Then
                strValues = frmData.tabData(0).GetFieldValues(i, "FLNO,ACT3,STOD,ETDI,TIFD,DES3,PSTD,'CPM'") + ",,," + frmData.tabData(0).GetFieldValues(i, "URNO")
                tabFlights(1).InsertTextLine strValues, False
                strOfbl = frmData.tabData(0).GetFieldValue(i, "OFBL")
                strAirb = frmData.tabData(0).GetFieldValue(i, "AIRB")
                strOfbl = Trim(strOfbl)
                strAirb = Trim(strAirb)
                If strAirb = "" And strOfbl = "" Then
                    tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", ""
                Else
                    If strAirb <> "" Then
                        tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strAirb
                    Else
                        tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "TIFD", strOfbl
                    End If
                End If
                If HasFlightUnconfirmedULDs(strUrno, strTifa, strTifd, strAdid) = True Then
                    tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbRed
                Else
                    If HasOffloads(strUrno) = True Then
                        tabFlights(1).SetLineColor tabFlights(1).GetLineCount - 1, vbBlack, vbCyan
                    End If
                End If
                strLoa = HasFlightLoaInfo(strUrno, "CPM,KRI")
                tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "'CPM',KRISCOM", strLoa
                If HasFlightULD(strUrno) = True Then
                    tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "ULD", "Y"
                Else
                    tabFlights(1).SetFieldValues tabFlights(1).GetLineCount - 1, "ULD", ""
                End If
            End If
        End If
    Next i
    selCols = tabFlights(1).GetSelectedColumns
    If selCols <> "" Then
        tabFlights(1).Sort selCols, True, True
    End If
    'tabFlights(1).IndexDestroy "URNO"
    tabFlights(1).IndexCreate "URNO", 10
    tabFlights(1).SetInternalLineBuffer True
    
    tabFlights(1).AutoSizeColumns
    tabFlights(1).Refresh
    lblGridCaption(1).Caption = "Departure Schedule (" & tabFlights(1).GetLineCount & ")   Filter active"
End Sub

' checks for a flights, if CPM,UCM,KRI,DLS etc. is available
' and returns it in a true/false Value list, which is able to
' be set with field
Public Function HasFlightLoaInfo(strAftUrno As String, strToFindItems As String) As String
    Dim strRet As String
    Dim strCmpRet As String
    Dim strDSSN As String
    Dim llLine As Long
    Dim llCnt As Long
    Dim strCurrFind As String
    Dim i As Long
    Dim foundItems As Long
    Dim nextI As Long
    Dim blFound As Boolean
    
    
    llCnt = ItemCount(strToFindItems, ",")
    
    For i = 0 To llCnt - 1
        blFound = False
        strCurrFind = GetRealItem(strToFindItems, i, ",")
        frmData.tabData(3).GetLinesByIndexValue "FLNU", strAftUrno, 0
        llLine = frmData.tabData(3).GetNextResultLine
        While llLine > -1 And blFound = False
            strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
            If strDSSN = strCurrFind Then
                blFound = True
            End If
            llLine = frmData.tabData(3).GetNextResultLine
        Wend
        If blFound = True Then
            strRet = strRet + "Y,"
        Else
            strRet = strRet + ","
        End If
    Next i
    strRet = left(strRet, Len(strRet) - 1)
    
'    nextI = 0
'    llCnt = ItemCount(strToFindItems, ",")
'    strCmpRet = String(llCnt, "Y,")
'    strRet = String(llCnt, ",")
'    frmData.tabData(3).GetLinesByIndexValue "FLNU", strAftUrno, 0
'    llLine = frmData.tabData(3).GetNextResultLine
'    While (llLine > -1 And foundItems < llCnt And (strRet <> strCmpRet))
'        For i = nextI To llCnt - 1
'            If llLine > -1 Then
'                strCurrFind = GetRealItem(strToFindItems, i, ",")
'                strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
'                If strDSSN = strCurrFind Then
'                    llLine = -1
'                    foundItems = foundItems + 1
'                    nextI = nextI + 1
'                    SetItem strRet, CInt(i), ",", "Y"
'                End If
'            Else
'             i = llCnt
'            End If
'        Next i
'        llLine = frmData.tabData(3).GetNextResultLine
'    Wend
    
    HasFlightLoaInfo = strRet
End Function

Public Function HasFlightCPM(strAftUrno As String) As Boolean
    Dim blRet As Boolean
    Dim llLine As Long
    Dim strDSSN As String
    
    blRet = False
    frmData.tabData(3).GetLinesByIndexValue "FLNU", strAftUrno, 0
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine > -1
        strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
        If strDSSN = "CPM" Then
            llLine = -1
            blRet = True
        Else
            llLine = frmData.tabData(3).GetNextResultLine
        End If
    Wend
    HasFlightCPM = blRet
End Function

Public Function HasFlightULD(strAftUrno As String) As Boolean
    Dim blRet As Boolean
    Dim llLine As Long
    
    blRet = False
    frmData.tabData(5).GetLinesByIndexValue "FLNU", strAftUrno, 0
    llLine = frmData.tabData(5).GetNextResultLine
    If llLine > -1 Then
        blRet = True
    End If
    HasFlightULD = blRet
End Function
'-------------------------------------------------------------------------------------
'Checks if at least one of the ULDs has one Offld flag then return true
' if the flight has Offld flags but ALL TIMH are set the return false
'-------------------------------------------------------------------------------------
Public Function HasOffloads(strAftUrno As String) As Boolean
    Dim blRet As Boolean
    Dim llLine As Long
    Dim cntOffld As Long
    Dim cntTimh As Long
    Dim cnt As Long
    Dim strTimh As String
    Dim strOffld As String
    
    cntOffld = 0
    cntTimh = 0
    cnt = 0
    blRet = False
    frmData.tabData(5).GetLinesByIndexValue "FLNU", strAftUrno, 0
    llLine = frmData.tabData(5).GetNextResultLine
    While llLine > -1
        cnt = cnt + 1
        strOffld = frmData.tabData(5).GetFieldValue(llLine, "OFLD")
        If strOffld = "Y" Then cntOffld = cntOffld + 1
        strTimh = frmData.tabData(5).GetFieldValue(llLine, "TIMH")
        If Trim(strTimh) <> "" Then cntTimh = cntTimh + 1
        llLine = frmData.tabData(5).GetNextResultLine
    Wend
    If cntTimh < cnt And cntOffld > 0 Then
        blRet = True
    End If

    HasOffloads = blRet
End Function
'-------------------------------------------------------------------------------------
' Checks if a flight has unconfirmed ULDs. If the flight has no ULDs the
' the function looks for existing LOATAB entries to ensure, that the user
' does not forget to produce the ULD entries. For this purpose the minimum
' confirmation time wil be used (min of (strARR_BaggageDefault, strARR_MailDefault and strARR_CargoDefault))
' Return
'           True if the flight has unconfirmed ULDs with mail/bag/cargo
' Parameters:
'           strAftUrno = URNO of flight
'           strTifa    = Reference time for arrival flight
'           strTifd    = Reference time for departure flight
'           strAdid    = Arrival or Departure
'-------------------------------------------------------------------------------------
Public Function HasFlightUnconfirmedULDs(strAftUrno As String, strTifa As String, _
                                         strTifd As String, strAdid As String) As Boolean
    Dim blRet As Boolean
    Dim llLine As Long
    Dim strCont As String
    Dim currTimeDiff As Long
    Dim datNow As Date
    Dim datRef As Date
    Dim llULDCount As Long
    Dim llAftLine As Long
    Dim strTisX As String
    Dim strFlno As String
    
    datNow = Now
    If cbToolbar(5).Value = 1 Then
        'We calculate in UTC so now must be UTC
        datNow = DateAdd("h", -UTCOffset, datNow)
    End If
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", strAftUrno, 0
    llAftLine = frmData.tabData(0).GetNextResultLine
    If llAftLine > -1 Then
        If strAdid = "A" Then strTisX = frmData.tabData(0).GetFieldValue(llAftLine, "TISA")
        If strAdid = "D" Then strTisX = frmData.tabData(0).GetFieldValue(llAftLine, "TISD")
        strFlno = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
        If strFlno = "AF 022" Then
            strFlno = strFlno
        End If
    End If
    
    blRet = False
    frmData.tabData(5).GetLinesByIndexValue "FLNU", strAftUrno, 0
    llLine = frmData.tabData(5).GetNextResultLine
    While (llLine > -1 And blRet = False)
        llULDCount = llULDCount + 1
        currTimeDiff = 0
        If frmData.tabData(5).GetFieldValue(llLine, "CONF") <> "Y" Then
            strCont = frmData.tabData(5).GetFieldValue(llLine, "CONT")
            If strAdid = "A" Then
                If strTisX = "O" Then
                    If strTifa <> "" Then
                        datRef = CedaFullDateToVb(strTifa)
                        currTimeDiff = DateDiff("n", datRef, datNow)
                    End If
                    Select Case (strCont)
                        Case "M"
                            If currTimeDiff > Val(strARR_MailDefault) Then
                                blRet = True
                            End If
                        Case "B"
                            If currTimeDiff > Val(strARR_BaggageDefault) Then
                                blRet = True
                            End If
                        Case "C"
                            If currTimeDiff > Val(strARR_CargoDefault) Then
                                blRet = True
                            End If
                    End Select
                End If
            End If
            If strAdid = "D" Then
                If strTifd <> "" Then
                    datRef = CedaFullDateToVb(strTifd)
                    currTimeDiff = DateDiff("n", datNow, datRef)
                End If
                Select Case (strCont)
                    Case "M"
                        If currTimeDiff < Val(strDEP_MailDefault) Then
                            blRet = True
                        End If
                    Case "B"
                        If currTimeDiff < Val(strDEP_BaggageDefault) Then
                            blRet = True
                        End If
                    Case "C"
                        If currTimeDiff < Val(strDEP_CargoDefault) Then
                            blRet = True
                        End If
                End Select
            End If
        End If
        llLine = frmData.tabData(5).GetNextResultLine
    Wend
    If blRet = False And llULDCount = 0 Then
        If strAdid = "A" And strTisX = "O" Then
            blRet = HasFlightUnconfirmedLOA(strAftUrno, strTifa, strTifd, strAdid, strTisX)
        Else
            If strAdid = "D" Then
                blRet = HasFlightUnconfirmedLOA(strAftUrno, strTifa, strTifd, strAdid, strTisX)
            End If
        End If
            
    End If
    HasFlightUnconfirmedULDs = blRet
End Function
Public Function HasFlightUnconfirmedLOA(strAftUrno As String, strTifa As String, _
                                         strTifd As String, strAdid As String, strTisX As String) As Boolean
    Dim blRet As Boolean
    Dim llLine As Long
    Dim minArrValue As Long
    Dim minDepValue As Long
    Dim datNow As Date
    Dim datRef As Date
    Dim currTimeDiff As Long
    Dim strCont As String
    Dim strULDT As String
    Dim strDSSN As String
    
    datNow = Now
    If cbToolbar(5).Value = 1 Then
        'We calculate in UTC so now must be UTC
        datNow = DateAdd("h", -UTCOffset, datNow)
    End If
'    minArrValue = 99999
'    minDepValue = 99999
'    If minArrValue > Val(strARR_BaggageDefault) Then minArrValue = Val(strARR_BaggageDefault)
'    If minArrValue > Val(strARR_MailDefault) Then minArrValue = Val(strARR_MailDefault)
'    If minArrValue > Val(strARR_CargoDefault) Then minArrValue = Val(strARR_CargoDefault)
'    If minDepValue > Val(strDEP_BaggageDefault) Then minDepValue = Val(strDEP_BaggageDefault)
'    If minDepValue > Val(strDEP_MailDefault) Then minDepValue = Val(strDEP_MailDefault)
'    If minDepValue > Val(strDEP_CargoDefault) Then minDepValue = Val(strDEP_CargoDefault)
    
    blRet = False
    frmData.tabData(3).GetLinesByIndexValue "FLNU", strAftUrno, 0
    llLine = frmData.tabData(3).GetNextResultLine
    While ((llLine > -1) And (blRet = False))
        strCont = frmData.tabData(3).GetFieldValue(llLine, "SSTP")
        strULDT = frmData.tabData(3).GetFieldValue(llLine, "ULDT")
        strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
        If strULDT <> "" And (strDSSN = "KRI" Or strDSSN = "DLS" Or strDSSN = "CPM") Then
            If strAdid = "A" Then
                If strTifa <> "" Then
                    datRef = CedaFullDateToVb(strTifa)
                    currTimeDiff = DateDiff("n", datRef, datNow)
                    strCont = GetContent(strCont)
                    If strAdid = "A" Then
                        If strTisX = "O" Then
                            If strTifa <> "" Then
                                datRef = CedaFullDateToVb(strTifa)
                                currTimeDiff = DateDiff("n", datRef, datNow)
                            End If
                            Select Case (strCont)
                                Case "M"
                                    If currTimeDiff > Val(strARR_MailDefault) Then
                                        blRet = True
                                    End If
                                Case "B"
                                    If currTimeDiff > Val(strARR_BaggageDefault) Then
                                        blRet = True
                                    End If
                                Case "C"
                                    If currTimeDiff > Val(strARR_CargoDefault) Then
                                        blRet = True
                                    End If
                            End Select
                        End If
                    End If
    '                If minArrValue < currTimeDiff Then
    '                    blRet = True
    '                End If
                End If
            End If
            If strAdid = "D" Then
                If strTifd <> "" Then
                    If strTifd <> "" Then
                        datRef = CedaFullDateToVb(strTifd)
                        currTimeDiff = DateDiff("n", datNow, datRef)
                    End If
                    strCont = GetContent(strCont)
                    Select Case (strCont)
                        Case "M"
                            If currTimeDiff < Val(strDEP_MailDefault) Then
                                blRet = True
                            End If
                        Case "B"
                            If currTimeDiff < Val(strDEP_BaggageDefault) Then
                                blRet = True
                            End If
                        Case "C"
                            If currTimeDiff < Val(strDEP_CargoDefault) Then
                                blRet = True
                            End If
                    End Select
    '                datRef = CedaFullDateToVb(strTifd)
    '                currTimeDiff = DateDiff("n", datRef, datNow) 'datNow, datRef)
    '                If minDepValue < currTimeDiff Then
    '                    blRet = True
    '                End If
                End If
            End If
        End If
        llLine = frmData.tabData(3).GetNextResultLine
    Wend
    HasFlightUnconfirmedLOA = blRet
End Function
Public Function GetContent(strSSTP As String) As String
    Dim strRet As String
    
    'check for BAGGAGE content
'    If strSSTP = "B" Or strSSTP = "TB" Or strSSTP = "FB" Or _
'                strSSTP = "Q" Or strSSTP = "D" Or strSSTP = "F" Or _
'                InStr(1, strSSTP, "B") = 1 Then
'        strRet = "BAGGAGE"
'    End If
'
'    'check for MAIL content
'    If strRet = "" Then
'        If strSSTP = "M" Then
'            strRet = "MAIL"
'        End If
'    End If
'    'check for CARGO content
'    If strRet = "" Then
'        If strSSTP = "Z" Or strSSTP = "H" Or strSSTP = "TC" Or _
'           strSSTP = "X" Or strSSTP = "W" Or strSSTP = "U" Or _
'           strSSTP = "E" Or strSSTP = "C" Or strSSTP = "LC" _
'           Then
'            strRet = "CARGO"
'        End If
'    End If
'    'check empty
'    If strSSTP = "N" Then
'        strRet = "EMPTY"
'    End If
    GetContent = strSSTP
End Function

Public Function IsPassArrivalFilter(llAftLine As Long) As Boolean
    Dim i As Long
    Dim strFrom As String
    Dim strTo As String
    Dim strAlcFilter As String
    Dim fromDat As Date
    Dim toDat As Date
    Dim strAdid As String
    Dim strTifa As String
    Dim strTifd As String
    Dim strETDA As String
    Dim strAlc2 As String, strAlc3 As String
    Dim strValues As String
    Dim strFlno As String
    Dim selCols As String
    Dim strUrno As String
    Dim filterCPMs As Boolean
    Dim filterNoCPMs As Boolean
    Dim blInsertIt As Boolean
    
    filterCPMs = False
    filterNoCPMs = False
    If cbCPMMessage.Value = 1 Then
        filterCPMs = True
    End If
    If cbNoCPMMessage.Value = 1 Then
        filterNoCPMs = True
    End If
    strFrom = txtArrDateFrom.Tag + txtArrTimeFrom
    strTo = txtArrDateTo.Tag + txtArrTimeTo
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    strFrom = Format(fromDat, "YYYYMMDDhhmmss")
    strTo = Format(toDat, "YYYYMMDDhhmmss")
        
    strAlcFilter = tabArrAirlines(1).GetBuffer(0, tabArrAirlines(1).GetLineCount - 1, ",")
    strAlcFilter = "," & strAlcFilter & ","
    
    i = llAftLine
    blInsertIt = False
    strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
    If strAdid = "A" Then
        strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
        strTifa = frmData.tabData(0).GetFieldValue(i, "TIFA")
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        If strFlno = "SQ 105" Then
            'strFlno = strFlno
        End If
        If strTifa >= strFrom And strTifa <= strTo Then
            strAlc2 = "," & frmData.tabData(0).GetFieldValue(i, "ALC2") & ","
            strAlc3 = "," & frmData.tabData(0).GetFieldValue(i, "ALC3") & ","
            If strAlcFilter = ",," Then
                If filterCPMs = False And filterNoCPMs = False Then
                    blInsertIt = True
                Else
                    If filterCPMs = True Then
                        If HasFlightCPM(strUrno) = True Then
                            blInsertIt = True
                        End If
                    End If
                    If filterNoCPMs = True Then
                        strETDA = frmData.tabData(0).GetFieldValue(i, "ETAI")
                        If Trim(strETDA) <> "" Then
                            If HasFlightCPM(strUrno) = False Then
                                blInsertIt = True
                            End If
                        End If
                    End If
                End If
            Else
                If (InStr(1, strAlcFilter, strAlc2) > 0) Or (InStr(1, strAlcFilter, strAlc3) > 0) Then
                    If filterCPMs = False And filterNoCPMs = False Then
                        blInsertIt = True
                    Else
                        If filterCPMs = True Then
                            If HasFlightCPM(strUrno) = True Then
                                blInsertIt = True
                            End If
                        End If
                        If filterNoCPMs = True Then
                            strETDA = frmData.tabData(0).GetFieldValue(i, "ETAI")
                            If Trim(strETDA) <> "" Then
                                If HasFlightCPM(strUrno) = False Then
                                    blInsertIt = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
    IsPassArrivalFilter = blInsertIt
End Function

Function IsPassDepartureFilter(llAftLine As Long) As Boolean
    Dim i As Long
    Dim strFrom As String
    Dim strTo As String
    Dim strAlcFilter As String
    Dim fromDat As Date
    Dim toDat As Date
    Dim strAdid As String
    Dim strTifd As String
    Dim strTifa As String
    Dim strAlc2 As String, strAlc3 As String
    Dim strValues As String
    Dim strFlno As String
    Dim selCols As String
    Dim strUrno As String
    Dim strLoa As String
    Dim blInsertIt As Boolean
    
    strFrom = txtDepDateFrom.Tag + txtDepTimeFrom
    strTo = txtDepDateTo.Tag + txtDepTimeTo
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    strFrom = Format(fromDat, "YYYYMMDDhhmmss")
    strTo = Format(toDat, "YYYYMMDDhhmmss")
        
    strAlcFilter = tabDepAirlines(1).GetBuffer(0, tabDepAirlines(1).GetLineCount - 1, ",")
    strAlcFilter = "," & strAlcFilter & ","
    
    i = llAftLine
    strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
    blInsertIt = True
    If strAdid = "D" Then
        strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
        strTifd = frmData.tabData(0).GetFieldValue(i, "TIFD")
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        If strFlno = "UA 805" Then
            strFlno = "UA 805"
        End If
        If strTifd >= strFrom And strTifd <= strTo Then
            strAlc2 = "," & frmData.tabData(0).GetFieldValue(i, "ALC2") & ","
            strAlc3 = "," & frmData.tabData(0).GetFieldValue(i, "ALC3") & ","
            strLoa = HasFlightLoaInfo(strUrno, "CPM,KRI")
            If cbDEEntry.Value = 1 Then
                If GetRealItem(strLoa, 1, ",") <> "Y" Then
                    blInsertIt = False
                End If
            End If
            If cbNoDEEntry.Value = 1 Then
                If GetRealItem(strLoa, 1, ",") = "Y" Then
                    blInsertIt = False
                End If
            End If
            If blInsertIt = True Then
                If strAlcFilter = ",," Then
                    blInsertIt = True
                Else
                    If (InStr(1, strAlcFilter, strAlc2) > 0) Or (InStr(1, strAlcFilter, strAlc3) > 0) Then
                        blInsertIt = True
                    Else
                        blInsertIt = False
                    End If
                End If
            End If
        Else
            blInsertIt = False
        End If
    End If
    IsPassDepartureFilter = blInsertIt
End Function

Private Sub txtDepTimeFrom_Change()
    Dim strDate As String
    strDate = txtDepTimeFrom.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtDepTimeFrom.BackColor = vbRed
        txtDepTimeFrom.ForeColor = vbWhite
    Else
        txtDepTimeFrom.BackColor = vbWhite
        txtDepTimeFrom.ForeColor = vbBlack
    End If
    HandleDepartureFilterButton
End Sub

Private Sub txtDepTimeTo_Change()
    Dim strDate As String
    strDate = txtDepTimeTo.Text
    If (CheckTimeValues(strDate, True) = False) Then
        txtDepTimeTo.BackColor = vbRed
        txtDepTimeTo.ForeColor = vbWhite
    Else
        txtDepTimeTo.BackColor = vbWhite
        txtDepTimeTo.ForeColor = vbBlack
    End If
    HandleDepartureFilterButton
End Sub
