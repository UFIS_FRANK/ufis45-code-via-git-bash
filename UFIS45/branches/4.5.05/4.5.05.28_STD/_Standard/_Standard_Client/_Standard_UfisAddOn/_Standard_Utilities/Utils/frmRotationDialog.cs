using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Ufis.Utils
{
	/// <summary>
	/// This class is currently under construction and may not be used.
	/// </summary>
	/// <remarks>
	/// This is the start of a common rotation dialog definition and has to be continued.
	/// </remarks>
	public class frmRotationDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public System.Windows.Forms.Button btnOK;
		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.GroupBox groupBoxRotation;
		private System.Windows.Forms.Label lblRegistration;
		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public System.Windows.Forms.TextBox txtREGN;
		private System.Windows.Forms.Label lblACType;
		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public System.Windows.Forms.TextBox txtACT3;
		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public System.Windows.Forms.TextBox txtACT5;
		private System.Windows.Forms.GroupBox groupBoxArrival;
		private System.Windows.Forms.Label lblFLNO_A;
		private System.Windows.Forms.TextBox txtFLNO_A;
		private System.Windows.Forms.Label lblCSGN_A;
		private System.Windows.Forms.TextBox txtCSGN_A;
		private System.Windows.Forms.GroupBox groupBoxDeparture;
		private System.Windows.Forms.TextBox txtCSGN_D;
		private System.Windows.Forms.Label lblCSGN_D;
		private System.Windows.Forms.TextBox txtFLNO_D;
		private System.Windows.Forms.Label lblFLNO_D;
		private AxTABLib.AxTAB tabArrival;
		private AxTABLib.AxTAB tabDeparture;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// under construction
		/// </summary>
		/// <remarks>
		/// under construction
		/// </remarks>
		public frmRotationDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <remarks>
		/// none
		/// </remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmRotationDialog));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.groupBoxRotation = new System.Windows.Forms.GroupBox();
			this.txtACT5 = new System.Windows.Forms.TextBox();
			this.txtACT3 = new System.Windows.Forms.TextBox();
			this.lblACType = new System.Windows.Forms.Label();
			this.txtREGN = new System.Windows.Forms.TextBox();
			this.lblRegistration = new System.Windows.Forms.Label();
			this.groupBoxArrival = new System.Windows.Forms.GroupBox();
			this.txtCSGN_A = new System.Windows.Forms.TextBox();
			this.lblCSGN_A = new System.Windows.Forms.Label();
			this.txtFLNO_A = new System.Windows.Forms.TextBox();
			this.lblFLNO_A = new System.Windows.Forms.Label();
			this.groupBoxDeparture = new System.Windows.Forms.GroupBox();
			this.txtCSGN_D = new System.Windows.Forms.TextBox();
			this.lblCSGN_D = new System.Windows.Forms.Label();
			this.txtFLNO_D = new System.Windows.Forms.TextBox();
			this.lblFLNO_D = new System.Windows.Forms.Label();
			this.tabArrival = new AxTABLib.AxTAB();
			this.tabDeparture = new AxTABLib.AxTAB();
			this.panel1.SuspendLayout();
			this.groupBoxRotation.SuspendLayout();
			this.groupBoxArrival.SuspendLayout();
			this.groupBoxDeparture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabArrival)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnOK);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(704, 32);
			this.panel1.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCancel.Location = new System.Drawing.Point(84, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnOK.Location = new System.Drawing.Point(4, 4);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "&OK";
			// 
			// groupBoxRotation
			// 
			this.groupBoxRotation.BackColor = System.Drawing.SystemColors.Control;
			this.groupBoxRotation.Controls.Add(this.txtACT5);
			this.groupBoxRotation.Controls.Add(this.txtACT3);
			this.groupBoxRotation.Controls.Add(this.lblACType);
			this.groupBoxRotation.Controls.Add(this.txtREGN);
			this.groupBoxRotation.Controls.Add(this.lblRegistration);
			this.groupBoxRotation.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBoxRotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBoxRotation.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.groupBoxRotation.Location = new System.Drawing.Point(0, 32);
			this.groupBoxRotation.Name = "groupBoxRotation";
			this.groupBoxRotation.Size = new System.Drawing.Size(704, 52);
			this.groupBoxRotation.TabIndex = 1;
			this.groupBoxRotation.TabStop = false;
			this.groupBoxRotation.Text = "Rotation:";
			// 
			// txtACT5
			// 
			this.txtACT5.Location = new System.Drawing.Point(380, 20);
			this.txtACT5.Name = "txtACT5";
			this.txtACT5.Size = new System.Drawing.Size(44, 20);
			this.txtACT5.TabIndex = 4;
			this.txtACT5.Tag = "ACT5";
			this.txtACT5.Text = "";
			// 
			// txtACT3
			// 
			this.txtACT3.Location = new System.Drawing.Point(332, 20);
			this.txtACT3.Name = "txtACT3";
			this.txtACT3.Size = new System.Drawing.Size(44, 20);
			this.txtACT3.TabIndex = 3;
			this.txtACT3.Tag = "ACT3";
			this.txtACT3.Text = "";
			// 
			// lblACType
			// 
			this.lblACType.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblACType.Location = new System.Drawing.Point(208, 24);
			this.lblACType.Name = "lblACType";
			this.lblACType.Size = new System.Drawing.Size(124, 12);
			this.lblACType.TabIndex = 2;
			this.lblACType.Text = "A/C tyoe (IATA/ICAO):";
			// 
			// txtREGN
			// 
			this.txtREGN.Location = new System.Drawing.Point(88, 20);
			this.txtREGN.Name = "txtREGN";
			this.txtREGN.Size = new System.Drawing.Size(104, 20);
			this.txtREGN.TabIndex = 1;
			this.txtREGN.Tag = "REGN";
			this.txtREGN.Text = "";
			// 
			// lblRegistration
			// 
			this.lblRegistration.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblRegistration.Location = new System.Drawing.Point(8, 24);
			this.lblRegistration.Name = "lblRegistration";
			this.lblRegistration.Size = new System.Drawing.Size(76, 12);
			this.lblRegistration.TabIndex = 0;
			this.lblRegistration.Text = "Registration:";
			// 
			// groupBoxArrival
			// 
			this.groupBoxArrival.BackColor = System.Drawing.SystemColors.Control;
			this.groupBoxArrival.Controls.Add(this.tabArrival);
			this.groupBoxArrival.Controls.Add(this.txtCSGN_A);
			this.groupBoxArrival.Controls.Add(this.lblCSGN_A);
			this.groupBoxArrival.Controls.Add(this.txtFLNO_A);
			this.groupBoxArrival.Controls.Add(this.lblFLNO_A);
			this.groupBoxArrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBoxArrival.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.groupBoxArrival.Location = new System.Drawing.Point(0, 88);
			this.groupBoxArrival.Name = "groupBoxArrival";
			this.groupBoxArrival.Size = new System.Drawing.Size(344, 424);
			this.groupBoxArrival.TabIndex = 2;
			this.groupBoxArrival.TabStop = false;
			this.groupBoxArrival.Text = "Arrival:";
			// 
			// txtCSGN_A
			// 
			this.txtCSGN_A.Location = new System.Drawing.Point(224, 20);
			this.txtCSGN_A.Name = "txtCSGN_A";
			this.txtCSGN_A.Size = new System.Drawing.Size(108, 20);
			this.txtCSGN_A.TabIndex = 3;
			this.txtCSGN_A.Text = "";
			// 
			// lblCSGN_A
			// 
			this.lblCSGN_A.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCSGN_A.Location = new System.Drawing.Point(160, 24);
			this.lblCSGN_A.Name = "lblCSGN_A";
			this.lblCSGN_A.Size = new System.Drawing.Size(56, 16);
			this.lblCSGN_A.TabIndex = 2;
			this.lblCSGN_A.Text = "Call sign:";
			// 
			// txtFLNO_A
			// 
			this.txtFLNO_A.Location = new System.Drawing.Point(52, 20);
			this.txtFLNO_A.Name = "txtFLNO_A";
			this.txtFLNO_A.TabIndex = 1;
			this.txtFLNO_A.Text = "";
			// 
			// lblFLNO_A
			// 
			this.lblFLNO_A.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblFLNO_A.Location = new System.Drawing.Point(12, 24);
			this.lblFLNO_A.Name = "lblFLNO_A";
			this.lblFLNO_A.Size = new System.Drawing.Size(40, 16);
			this.lblFLNO_A.TabIndex = 0;
			this.lblFLNO_A.Text = "Flight:";
			// 
			// groupBoxDeparture
			// 
			this.groupBoxDeparture.BackColor = System.Drawing.SystemColors.Control;
			this.groupBoxDeparture.Controls.Add(this.tabDeparture);
			this.groupBoxDeparture.Controls.Add(this.txtCSGN_D);
			this.groupBoxDeparture.Controls.Add(this.lblCSGN_D);
			this.groupBoxDeparture.Controls.Add(this.txtFLNO_D);
			this.groupBoxDeparture.Controls.Add(this.lblFLNO_D);
			this.groupBoxDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBoxDeparture.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.groupBoxDeparture.Location = new System.Drawing.Point(356, 88);
			this.groupBoxDeparture.Name = "groupBoxDeparture";
			this.groupBoxDeparture.Size = new System.Drawing.Size(344, 424);
			this.groupBoxDeparture.TabIndex = 3;
			this.groupBoxDeparture.TabStop = false;
			this.groupBoxDeparture.Text = "Departure:";
			// 
			// txtCSGN_D
			// 
			this.txtCSGN_D.Location = new System.Drawing.Point(224, 20);
			this.txtCSGN_D.Name = "txtCSGN_D";
			this.txtCSGN_D.Size = new System.Drawing.Size(108, 20);
			this.txtCSGN_D.TabIndex = 7;
			this.txtCSGN_D.Tag = "CSGN_D";
			this.txtCSGN_D.Text = "";
			// 
			// lblCSGN_D
			// 
			this.lblCSGN_D.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCSGN_D.Location = new System.Drawing.Point(160, 24);
			this.lblCSGN_D.Name = "lblCSGN_D";
			this.lblCSGN_D.Size = new System.Drawing.Size(56, 16);
			this.lblCSGN_D.TabIndex = 6;
			this.lblCSGN_D.Text = "Call sign:";
			// 
			// txtFLNO_D
			// 
			this.txtFLNO_D.Location = new System.Drawing.Point(52, 20);
			this.txtFLNO_D.Name = "txtFLNO_D";
			this.txtFLNO_D.TabIndex = 5;
			this.txtFLNO_D.Tag = "FLNO_D";
			this.txtFLNO_D.Text = "";
			// 
			// lblFLNO_D
			// 
			this.lblFLNO_D.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblFLNO_D.Location = new System.Drawing.Point(12, 24);
			this.lblFLNO_D.Name = "lblFLNO_D";
			this.lblFLNO_D.Size = new System.Drawing.Size(40, 16);
			this.lblFLNO_D.TabIndex = 4;
			this.lblFLNO_D.Text = "Flight:";
			// 
			// tabArrival
			// 
			this.tabArrival.ContainingControl = this;
			this.tabArrival.Location = new System.Drawing.Point(8, 52);
			this.tabArrival.Name = "tabArrival";
			this.tabArrival.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabArrival.OcxState")));
			this.tabArrival.Size = new System.Drawing.Size(324, 356);
			this.tabArrival.TabIndex = 4;
			// 
			// tabDeparture
			// 
			this.tabDeparture.ContainingControl = this;
			this.tabDeparture.Location = new System.Drawing.Point(8, 52);
			this.tabDeparture.Name = "tabDeparture";
			this.tabDeparture.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDeparture.OcxState")));
			this.tabDeparture.Size = new System.Drawing.Size(324, 356);
			this.tabDeparture.TabIndex = 8;
			// 
			// frmRotationDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(704, 534);
			this.Controls.Add(this.groupBoxDeparture);
			this.Controls.Add(this.groupBoxArrival);
			this.Controls.Add(this.groupBoxRotation);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmRotationDialog";
			this.Text = "Daily Rotation Data [LOCAL/UTC TIMES]";
			this.Load += new System.EventHandler(this.frmRotationDialog_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmRotationDialog_Paint);
			this.panel1.ResumeLayout(false);
			this.groupBoxRotation.ResumeLayout(false);
			this.groupBoxArrival.ResumeLayout(false);
			this.groupBoxDeparture.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabArrival)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmRotationDialog_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{ 
			//UT.DrawGradient(e.Graphics, 90f, this, Color.WhiteSmoke, Color.LightGray);
		}

		private void frmRotationDialog_Load(object sender, System.EventArgs e)
		{
			tabArrival.ResetContent();
			tabArrival.HeaderString = "Field,Data";
			tabArrival.LogicalFieldList = tabArrival.HeaderString;
			tabArrival.HeaderLengthString = "150,300";
			tabArrival.EnableHeaderSizing(true);

			tabDeparture.ResetContent();
			tabDeparture.HeaderString = "Field,Data";
			tabDeparture.LogicalFieldList = tabArrival.HeaderString;
			tabDeparture.HeaderLengthString = "150,300";
			tabDeparture.EnableHeaderSizing(true);
		}
	}
}

