﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;

namespace Data_Changes.Ctrl
{
    public class LinkInfo
    {
        //private string _tableName = null;//e.g. ICE (ICETAB)
        //private string _tableNameToLink = null;//e.g. AFT (to lik to AFTTAB)
        private string _linkColumn = null;//e.g. RURN (URNO of AFTTAB store in ICETAB, RURN column)
        List<string> _lsColumnToCheck = null;

        public LinkInfo( string linkColumn, List< string> lsColToCheck)
        {
            _linkColumn = linkColumn;
            _lsColumnToCheck = lsColToCheck;

            if (_linkColumn == null) _linkColumn = "";

        }

        public void AddColumnToCheck(List<string> lsColToCheck)
        {
            foreach (string col in lsColToCheck)
            {
                if (!_lsColumnToCheck.Contains(col)) _lsColumnToCheck.Add(col);
            }
        }

        public string LinkColumn
        {
            get { return _linkColumn; }
        }

        public List<string> ListColumnToCheck
        {
            get { return _lsColumnToCheck; }
        }
    }

    public class LinkInfoToATAble
    {
        private string _refTableName = null;
        Dictionary<string, LinkInfo> _mapLinkInfo = new Dictionary<string, LinkInfo>();
        

        public LinkInfoToATAble(string refTableName)
        {
            if (string.IsNullOrEmpty(refTableName)) throw new ApplicationException("No Ref. Table Name");
            _refTableName = refTableName;
        }

        public void AddLinkInfo(string tableName, string linkColumn, List<string> lsColToCheck)
        {
            LinkInfo linkInfo = new LinkInfo(linkColumn, lsColToCheck);
            if (_mapLinkInfo.ContainsKey(tableName))
            {
                //throw new ApplicationException("Duplicate Link Info for " + tableName + " TO " + _refTableName);
            }
            else
            {
                _mapLinkInfo.Add(tableName, linkInfo);
            }
        }

        public List<string> GetLinkTableList()
        {
            List<string> lsTableNames = new List<string>();
            foreach (string tableName in _mapLinkInfo.Keys)
            {
                lsTableNames.Add(tableName);
            }

            return lsTableNames;
        }

        public LinkInfo GetLinkInfo(string tableName)
        {
            LinkInfo linkInfo = null;
            if (_mapLinkInfo.ContainsKey(tableName))
            {
                linkInfo = _mapLinkInfo[tableName];
            }
            return linkInfo;
        }
    }

    public class AllLinkInfo
    {
        private Dictionary<string, LinkInfoToATAble> _mapLinkInfo = new Dictionary<string, LinkInfoToATAble>();

        public bool AddLinkInfo(string refTableName, string tableName, string linkColumn, List<string> lsColToCheck)
        {
            bool added = false;
            if (string.IsNullOrEmpty(tableName) ||
                string.IsNullOrEmpty(refTableName) ||
                string.IsNullOrEmpty(linkColumn)
                )
            {
                added = false;
            }
            else
            {
                LinkInfoToATAble linkInfo = GetLinkInfoToATable(refTableName);      
                linkInfo.AddLinkInfo(tableName, linkColumn, lsColToCheck);
                added = true;
            }
            return added;
        }

        public void AddLinkInfo(string refTableName, string tableName, string linkColumn, string[] arrColToCheck)
        {
            List<string> lsColToCheck = new List<string>();
            lsColToCheck.AddRange(arrColToCheck);
            AddLinkInfo(refTableName, tableName, linkColumn, lsColToCheck);
        }
        public void AddLinkInfo(string refTableName, string tableName, string linkColumn, string stColToCheck)
        {
            string[] arr = stColToCheck.Split(',');
            AddLinkInfo(refTableName, tableName, linkColumn, arr);
        }

        public LinkInfoToATAble GetLinkInfoToATable(string refTableName)
        {
            LinkInfoToATAble info = null;
            if (_mapLinkInfo.ContainsKey(refTableName))
            {
                info = _mapLinkInfo[refTableName];
            }
            else
            {
                info = new LinkInfoToATAble(refTableName);
                _mapLinkInfo.Add(refTableName, info);
            }
            return info;
        }

        public LinkInfo GetLinkInfo(string refTableName, string tableName)
        {
            LinkInfoToATAble refTableLinkInfo = GetLinkInfoToATable(refTableName);
            return refTableLinkInfo.GetLinkInfo(tableName);
        }

        private string GetLinkKey(string refTableName, string tableName)
        {
            return  refTableName  + "_$$$_" + tableName;
        }
    }

    public class CtrlConfig
    {
        private const string LINK_TO_PREFIX = "LINK_TO$_";
        private const string LINK_FR_PREFIX = "_FR#_";
        private const string AFT_LINK = LINK_TO_PREFIX + "AFT";//Config Section for AFT_LINK
        private const string LINK_TABLES_KEY = "TABLES";//Table Name to link to AFTTAB
        
        
        Dictionary<string, string> _mapCfg = null;//Map for Config Information
        private const string MAP_KEY_SEPARATOR = "_$$$_";
        //seperator between SECTION value and KEY value as mapKey to be used in mapAftLink

        AllLinkInfo _allLinkInfo = new AllLinkInfo();
        //Table Name as key and Column name (in that table to link to AFTTAB) as value
        //e.g ICE, RURN

        static bool _configInfoLoaded = false;
        static object _configLocker = new object();

        #region Singleton
        private static CtrlConfig _this = null;

        private CtrlConfig()
        {
            _mapCfg = new Dictionary<string, string>();
        }

        public static CtrlConfig GetInstance()
        {
            if (_this == null) _this = new CtrlConfig();
            return _this;
        }
        #endregion

        IniFile _myIni = new IniFile("C:\\Ufis\\System\\DATACHANGE.ini");
        private IniFile MyIni
        {
            get { return _myIni; }
        }

        public void LoadConfigInfo()
        {
            if (!_configInfoLoaded)
            {
                lock (_configLocker)
                {
                    if (!_configInfoLoaded)
                    {
                        LoadLinkInfo("AFT");
                        _configInfoLoaded = true;
                    }
                }
            }
        }

        public LinkInfoToATAble GetLinkInfoFor(string refTableName)
        {
            return _allLinkInfo.GetLinkInfoToATable(refTableName);
        }

        private void LoadLinkInfo( string refTableName)
        {
            string linkCfgSectionName = LINK_TO_PREFIX + refTableName;
            string linkTables = GetConfigInfo(linkCfgSectionName, LINK_TABLES_KEY);
            
            string[] arrTables = linkTables.Split(',');
            int tabcnt = arrTables.Length;

            for (int i = 0; i < tabcnt; i++)
            {
                string table = arrTables[i];
                if (string.IsNullOrEmpty(table)) continue;
                if (table.Trim() == "") continue;

                string linkTabRefSectionName = linkCfgSectionName + LINK_FR_PREFIX + table;
                string linkCol = GetConfigInfo(linkTabRefSectionName, "RURN");
                string fieldsToCheck = GetConfigInfo(linkTabRefSectionName, "FIELDS");

                _allLinkInfo.AddLinkInfo(refTableName, table, linkCol, fieldsToCheck);
            }
        }

        private string GetConfigInfo(string section, string key)
        {
            string val = "";
            string mapKey = section + MAP_KEY_SEPARATOR + key;
            if (_mapCfg.ContainsKey(mapKey))
            {
                val = _mapCfg[mapKey];
            }
            else
            {
                val = MyIni.IniReadValue(section, key);
                try
                {
                    _mapCfg.Add(mapKey, val);
                }
                catch { }
            }
            return val;
        }

    }
}
