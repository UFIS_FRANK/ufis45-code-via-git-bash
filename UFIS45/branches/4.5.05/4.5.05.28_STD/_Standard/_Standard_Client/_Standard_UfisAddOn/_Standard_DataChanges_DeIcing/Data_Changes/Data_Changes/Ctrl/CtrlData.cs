﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;
using Ufis.Data;

namespace Data_Changes.Ctrl
{
    public class MapRelation : Dictionary<string, string>
    {
    }

    public class CtrlData
    {
        private IDatabase _myDB = null;

        public IDatabase MyDB
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private static CtrlData _this = null;

        public static CtrlData GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlData();
            }
            return _this;
        }

        public static List<string> GetIDsForSelection(
           List<string> lsIds,
           int noOfIdInASelection, string separator, string quote
           )
        {
            List<string> ls = new List<string>();
            int cnt = lsIds.Count;
            string delimiter = "";
            if (lsIds != null && cnt > 0)
            {
                
                StringBuilder sb = new StringBuilder(1000);
                int cntUrno = 0;
                for (int i = 0; i < cnt; i++)
                {
                    string urno = lsIds[i];
                    if (string.IsNullOrEmpty(urno)) continue;
                    urno = urno.Trim();
                    if (urno == "") continue;

                    sb.Append(string.Format("{2}{0}{1}{0}", quote, urno, delimiter));

                    cntUrno++;
                    if (cntUrno == noOfIdInASelection)
                    {
                        ls.Add(sb.ToString());
                        delimiter = "";
                        cntUrno = 0;
                    }
                    else
                    {
                        delimiter = separator;
                    }
                }
                if (sb.Length > 0) ls.Add(sb.ToString());
            }
            
            return ls;
        }

        public List<string> GetIDListForSelection(
            string tableName, string fieldName,
            int noOfIdInASelection, string separator, string quote
            )
        {
            if (noOfIdInASelection < 20) noOfIdInASelection = 20;
            List<string> ls = new List<string>();
            ITable myTable = MyDB[tableName];
            if (myTable != null)
            {
                int cnt = myTable.Count;
                StringBuilder sb = new StringBuilder(1000);
                int cntUrno = 0;
                string delimiter = "";

                for (int i = 0; i < cnt; i++)
                {
                    IRow row = myTable[i];
                    string urno = myTable[i][fieldName];

                    if (string.IsNullOrEmpty(urno)) continue;
                    urno = urno.Trim();
                    if (urno == "") continue;

                    sb.Append(string.Format("{2}{0}{1}{0}", quote, urno, delimiter));

                    cntUrno++;
                    if (cntUrno == noOfIdInASelection)
                    {
                        ls.Add(sb.ToString());
                        delimiter = "";
                        cntUrno = 0;
                    }
                    else
                    {
                        delimiter = separator;
                    }
                }
                if (sb.Length > 0) ls.Add(sb.ToString());
            }

            return ls;
        }



        public MapRelation GetMap(
           string tableName, string urnoFieldName, string refFieldName,
           int noOfIdInASelection, string separator, string quote,
           out List<string> lsUrnos)
        {
            MapRelation map = new MapRelation();
            lsUrnos = new List<string>();
            IDatabase myDB = UT.GetMemDB();
            string fieldNames = string.Format("{0},{1}", urnoFieldName, refFieldName);

            ITable myTable = MyDB[tableName];

            int cnt = myTable.Count;
            StringBuilder sb = new StringBuilder(1000);
            int cntUrno = 0;
            string delimiter = "";

            for (int i = 0; i < cnt; i++)
            {
                IRow row = myTable[i];
                string urno = row[urnoFieldName];
                if (map.ContainsKey(urno))
                {
                }
                else
                {                 
                    if (string.IsNullOrEmpty(urno)) continue;
                    urno = urno.Trim();
                    if (urno == "") continue;

                    map.Add(urno, row[refFieldName]);

                    sb.Append(string.Format("{2}{0}{1}{0}", quote, urno, delimiter));

                    cntUrno++;
                    if (cntUrno == noOfIdInASelection)
                    {
                        lsUrnos.Add(sb.ToString());
                        delimiter = "";
                        cntUrno = 0;
                    }
                    else
                    {
                        delimiter = separator;
                    }
                }
            }
            if (sb.Length > 0) lsUrnos.Add(sb.ToString());
            return map;
        }


    }
}
