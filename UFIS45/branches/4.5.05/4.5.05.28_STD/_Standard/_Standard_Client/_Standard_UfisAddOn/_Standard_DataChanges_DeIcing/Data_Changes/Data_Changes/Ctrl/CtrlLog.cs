﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Data;
using Ufis.Utils;

namespace Data_Changes.Ctrl
{
    public class CtrlLog
    {
        static IDatabase _myDB = null;
        static CtrlLog _this = null;

        private CtrlLog() { }

        public static CtrlLog GetInstance()
        {
            if (_this == null) _this = new CtrlLog();
            return _this;
        }


        public const string LOGTAB_LOGICAL_FIELDS = 
            "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX,TANA";
        public const string LOGTAB_FIELD_LENGTHS = 
            "10,10,10,10,10,14,10,10,10,14,10";
        public const string LOGTAB_DB_FIELDS = 
            "UREF,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,TANA";
        public const string LOGTAB_EXCLUDE_FIELDS = ",URNO,USEC,CDAT,USEU,LSTU,";

        public static bool IsFieldToExcludeForLog(string fldName)
        {
            return LOGTAB_EXCLUDE_FIELDS.Contains("," + fldName + ",");
        }

        public List<string> GetLogFieldsWithoutDefaultFields(List<string> lsFieldList)
        {
            List<string> lsNew = new List<string>();
            foreach (string fld in lsFieldList)
            {
                if (IsFieldToExcludeForLog(fld)) continue;
                lsNew.Add(fld);
            }
            return lsNew;
        }


        public bool LoadAFTRelatedLog(string stWhereTimeframe, ITable logTab, out List<string> lsAdditionalTables)
        {
            bool result = true;
            IDatabase myDB = UT.GetMemDB();
            ITable myAft = myDB["AFT"];
            lsAdditionalTables = null;

            List<string> lsAftUrnosForSelection = CtrlData.GetInstance().GetIDListForSelection("AFT", "URNO", 300, ",", "'");

            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("AFT");
            List<string> lsTables = aftLinkInfo.GetLinkTableList();
            lsAdditionalTables = lsTables;
            Dictionary<string, MapRelation> mapTableAndKeyMap = new Dictionary<string, MapRelation>();

            foreach (string tableName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tableName);

                List<string> lsUrnosForSelection = null;
                MapRelation map = GetMap(lsAftUrnosForSelection, lInfo.LinkColumn, tableName, "URNO", out lsUrnosForSelection);
                if (map != null && map.Count > 0)
                {
                    mapTableAndKeyMap.Add(tableName, map);
                    string logTableName = "";
                    List<string> lsLogFields;
                    CtrlSYSTabData.GetInstance().GetLogInfoFor(tableName, out logTableName, out lsLogFields);

                    List<string> lsFieldsToCheck = CtrlData.GetIDsForSelection(lInfo.ListColumnToCheck, 200, ",", "'");
                    if (lsFieldsToCheck == null || lsFieldsToCheck.Count == 0)
                    {
                        lsFieldsToCheck = CtrlData.GetIDsForSelection(lsLogFields, 200, ",", "'");
                    }


                    if (lsFieldsToCheck != null && lsFieldsToCheck.Count > 0)
                    {
                        foreach (string columnsToCheck in lsFieldsToCheck)
                        {
                            LoadLinkData(
                                stWhereTimeframe,
                                lsUrnosForSelection, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap);
                        }
                    }
                }
            }

            //int cntLog = logTab.Count;
            //for (int i = 0; i < cntLog; i++)
            //{
            //    IRow row = logTab[i];

            //    string table = row["TANA"];
            //    if (mapTableAndKeyMap.ContainsKey(table))
            //    {
            //        try
            //        {
            //            string id = row["Flight"];
            //            row["KeyF"] = mapTableAndKeyMap[table][id];
            //        }
            //        catch (Exception ex)
            //        {
            //            row["KeyF"] = " ";
            //        }
            //    }
            //}

            return result;
        }

        public void LoadLinkData(
            string stWhereTimeframe,
            List<string> lsUrnos,
            ITable logTable, string logTableName,
            string tableName, string tableLoadingMsg,
            string fieldNamesForLog,
            Dictionary<string, MapRelation> mapTableAndKeyMap
             )
        {
            string fieldFilter = "";
            if (string.IsNullOrEmpty(fieldNamesForLog))
            {
            }
            else
            {
                if (fieldNamesForLog.Trim() != "")
                {
                    fieldFilter = " AND FINA IN (" + fieldNamesForLog + ")";
                }
            }

            IDatabase myDB = UT.GetMemDB();
            myDB.Unbind("TEMP");

            //myDB.Unbind("L0G");
            ITable tempLogTable = myDB.Bind("TEMP",
                logTableName,
                CtrlLog.LOGTAB_LOGICAL_FIELDS,
                CtrlLog.LOGTAB_FIELD_LENGTHS,
                CtrlLog.LOGTAB_DB_FIELDS);

            foreach (string urnos in lsUrnos)
            {
                if (string.IsNullOrEmpty(urnos) || urnos.Trim() == "") continue;
                string st = string.Format("{0} AND TANA='{1}' {2} AND UREF IN ({3})", stWhereTimeframe, tableName, fieldFilter, urnos);
                tempLogTable.Load(st);
            }

            int cntLog = tempLogTable.Count;
            for (int i = 0; i < cntLog; i++)
            {
                IRow tempRow = tempLogTable[i];

                string table = tempRow["TANA"];
                if (mapTableAndKeyMap.ContainsKey(table))
                {
                    try
                    {
                        string id = tempRow["Flight"];
                        tempRow["Keyf"] = mapTableAndKeyMap[table][id];
                    }
                    catch (Exception)
                    {
                        tempRow["Keyf"] = " ";
                    }
                }

                IRow rowNew = logTable.CreateEmptyRow();
                rowNew.SetFieldValues(tempRow.FieldValues());
                logTable.Add(rowNew);
            }
        }

        public MapRelation GetMap(List<string> lsRefTableUrno, string refFieldName,
            string tableName, string urnoFieldName,
            out List<string> lsUrnos)
        {
            MapRelation map = new MapRelation();
            lsUrnos = new List<string>();
            IDatabase myDB = UT.GetMemDB();
            string fieldNames = string.Format("{0},{1}", urnoFieldName, refFieldName);
            myDB.Unbind("TEMP");

            ITable myTable = myDB.Bind("TEMP",
                tableName,
                fieldNames,
                "14,14",
                fieldNames);
            myTable.Clear();

            foreach (string urnos in lsRefTableUrno)
            {
                string st = string.Format("WHERE {0} IN ({1})", refFieldName, urnos);
                myTable.Load(string.Format("WHERE {0} IN ({1})", refFieldName, urnos));
            }

            return CtrlData.GetInstance().GetMap("TEMP", urnoFieldName, refFieldName, 300, ",", "'", out lsUrnos);
        }
    }
}
