using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Microsoft.Office.Interop;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace ExportConfigData
{	
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ReadConfigWriteXls : System.Windows.Forms.Form
	{
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section,
			string key,string def, StringBuilder retVal,
			int size,string filePath);

		private string strExcelFileName       = "";// = "C:\\Ufis\\Appl\\Ufis_Client_Config.xls";
		private string strIniFileName         = "";// = "C:\\Ufis\\Appl\\Ceda.ini";		
		private int    imIncrementStep        = 0;		
		private int    imIncrementCompleted   = 0;
		private int    imExcelEndLineNo       = 0;
		private bool   bmExcelChanged         = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmb_Customer;
		private System.Windows.Forms.Button btnOK;
		private Microsoft.Office.Interop.Excel.Application xlApp               = null;
		private Microsoft.Office.Interop.Excel.Workbook workBook               = null;
		private System.Collections.Hashtable hashTabCustomerToColumnRef        = new System.Collections.Hashtable();
		private System.Collections.Hashtable hashTabExcelAppNameToEndLine      = new Hashtable();
		private System.Collections.Hashtable hashTabIniAppNameToParamHashTab   = new Hashtable();
		private System.Collections.ArrayList arrayListExcelAppNames            = new ArrayList();
		private System.Collections.ArrayList arrayListIniAppNames              = new ArrayList();
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtBoxIniFile;
		private System.Windows.Forms.Button btnSelectIniFile;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtBoxXlsFile;
		private System.Windows.Forms.Button btnSelectXlsFile;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ProgressBar progressBar;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ReadConfigWriteXls()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);			
		}		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.cmb_Customer = new System.Windows.Forms.ComboBox();
			this.btnOK = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtBoxIniFile = new System.Windows.Forms.TextBox();
			this.btnSelectIniFile = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtBoxXlsFile = new System.Windows.Forms.TextBox();
			this.btnSelectXlsFile = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 83);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Airport:";
			// 
			// cmb_Customer
			// 
			this.cmb_Customer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_Customer.Location = new System.Drawing.Point(72, 80);
			this.cmb_Customer.Name = "cmb_Customer";
			this.cmb_Customer.Size = new System.Drawing.Size(121, 21);
			this.cmb_Customer.TabIndex = 2;
			this.cmb_Customer.SelectedIndexChanged += new System.EventHandler(this.cmb_Customer_SelectedIndexChanged);
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(136, 120);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 3;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 19);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Ini File:";
			// 
			// txtBoxIniFile
			// 
			this.txtBoxIniFile.BackColor = System.Drawing.SystemColors.Info;
			this.txtBoxIniFile.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtBoxIniFile.Location = new System.Drawing.Point(72, 16);
			this.txtBoxIniFile.Name = "txtBoxIniFile";
			this.txtBoxIniFile.ReadOnly = true;
			this.txtBoxIniFile.Size = new System.Drawing.Size(328, 20);
			this.txtBoxIniFile.TabIndex = 4;
			this.txtBoxIniFile.TabStop = false;
			this.txtBoxIniFile.Text = "";
			// 
			// btnSelectIniFile
			// 
			this.btnSelectIniFile.Location = new System.Drawing.Point(405, 15);
			this.btnSelectIniFile.Name = "btnSelectIniFile";
			this.btnSelectIniFile.Size = new System.Drawing.Size(24, 23);
			this.btnSelectIniFile.TabIndex = 0;
			this.btnSelectIniFile.Text = "...";
			this.btnSelectIniFile.Click += new System.EventHandler(this.btnSelectIniFile_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 53);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Xls File:";
			// 
			// txtBoxXlsFile
			// 
			this.txtBoxXlsFile.BackColor = System.Drawing.SystemColors.Info;
			this.txtBoxXlsFile.Location = new System.Drawing.Point(72, 48);
			this.txtBoxXlsFile.Name = "txtBoxXlsFile";
			this.txtBoxXlsFile.ReadOnly = true;
			this.txtBoxXlsFile.Size = new System.Drawing.Size(328, 20);
			this.txtBoxXlsFile.TabIndex = 7;
			this.txtBoxXlsFile.TabStop = false;
			this.txtBoxXlsFile.Text = "";
			// 
			// btnSelectXlsFile
			// 
			this.btnSelectXlsFile.Location = new System.Drawing.Point(405, 48);
			this.btnSelectXlsFile.Name = "btnSelectXlsFile";
			this.btnSelectXlsFile.Size = new System.Drawing.Size(24, 23);
			this.btnSelectXlsFile.TabIndex = 1;
			this.btnSelectXlsFile.Text = "...";
			this.btnSelectXlsFile.Click += new System.EventHandler(this.btnSelectXlsFile_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(224, 120);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnCancel.Enter += new System.EventHandler(this.btnCancel_Enter);
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(205, 80);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(224, 21);
			this.progressBar.TabIndex = 10;
			this.progressBar.Visible = false;
			// 
			// ReadConfigWriteXls
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(432, 158);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSelectXlsFile);
			this.Controls.Add(this.txtBoxXlsFile);
			this.Controls.Add(this.txtBoxIniFile);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnSelectIniFile);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.cmb_Customer);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(440, 192);
			this.MinimumSize = new System.Drawing.Size(440, 192);
			this.Name = "ReadConfigWriteXls";
			this.Text = "Export Config Data";
			this.Load += new System.EventHandler(this.ReadConfigWriteXls_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ReadConfigWriteXls());
		}

		private void cmb_Customer_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strCustomerName = cmb_Customer.GetItemText(cmb_Customer.SelectedItem);
		}

		private void InitializeDataContainer()
		{			
			arrayListIniAppNames.Clear();
			arrayListExcelAppNames.Clear();
			hashTabIniAppNameToParamHashTab.Clear();
			hashTabExcelAppNameToEndLine.Clear();
		}

		private void FillupCustomerComboBox()
		{
			System.Globalization.CultureInfo oldCulterInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

			try
			{
				if(xlApp == null)
				{
					xlApp = new Microsoft.Office.Interop.Excel.Application();
				}
				if (xlApp == null)
				{
					MessageBox.Show(this,"EXCEL could not be started","Unknown Error",MessageBoxButtons.OK,MessageBoxIcon.Stop);
					return;
				}

				hashTabCustomerToColumnRef.Clear();
				xlApp.Visible = false;
				cmb_Customer.Items.Clear();
				//workBook = xlApp.Workbooks.Open(strExcelFileName,0, false, 5,"", "", true,Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",true, false, 0, true,false,false);
				workBook = xlApp.Workbooks.Open(strExcelFileName,0, true, 5,"", "", false,Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",false, false, 0, true,true,false);
				Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
				workSheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoRestrictions;
			
				Microsoft.Office.Interop.Excel.Range range = workSheet.get_Range("H1","BU1");			
				if(range != null)
				{					
					for(int i = 1 ; i <= range.Count; i++)
					{
						Microsoft.Office.Interop.Excel.Range rangeCell = (Microsoft.Office.Interop.Excel.Range)range.get_Item(1,i);
						if(rangeCell != null && rangeCell.Value2 != null)
						{						
							string strColumnData = rangeCell.Value2.ToString();							
							cmb_Customer.Items.Add(strColumnData);
							hashTabCustomerToColumnRef[strColumnData] = rangeCell.Column;							
						}
					}					
				}				
				workBook.Close(false,"",false);				
				xlApp.Quit();
				workBook = null;
				xlApp = null;
			}
			catch(Exception e)
			{
				if(workBook != null)
				{
					workBook.Close(false,"",false);
					workBook = null;
				}
				if(xlApp != null)
				{
					xlApp.Quit();
					xlApp = null;
				}
				MessageBox.Show(this,e.Message + "\n" + e.StackTrace,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			System.Threading.Thread.CurrentThread.CurrentCulture = oldCulterInfo;
		}

		private void WriteToXls(string strCustomer)
		{
			System.Globalization.CultureInfo oldCulterInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

			try
			{				
				if(xlApp == null)
				{
					xlApp = new Microsoft.Office.Interop.Excel.Application();
				}
				if (xlApp == null)
				{
					MessageBox.Show(this,"Error in excel application.\nPlease check configuration","Excel error",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return;
				}
				int ilLineno = 0;
				int ilBlankRows = 0;
				string strLastReadAppName = "";
				int ilCustomerColumn = Convert.ToInt32(hashTabCustomerToColumnRef[strCustomer]);
				xlApp.Visible = false;			
				workBook = xlApp.Workbooks.Open(strExcelFileName,0, false, 5,"", "", true,Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",true, false, 0, false,true,false);
				Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
				Microsoft.Office.Interop.Excel.Range range = workSheet.get_Range("A2","B2");
				Microsoft.Office.Interop.Excel.Range rangeCustomerCell = workSheet.get_Range("A1","A1").get_Offset(1,ilCustomerColumn - 1);
				
				while(true)
				{					
					ilLineno++;
					if(range != null)
					{
						Microsoft.Office.Interop.Excel.Range rangeAppCell = (Microsoft.Office.Interop.Excel.Range)range.get_Item(1,1);
						Microsoft.Office.Interop.Excel.Range rangeParamCell = (Microsoft.Office.Interop.Excel.Range)range.get_Item(1,2);
						if(rangeAppCell != null && rangeAppCell.Value2 != null
							&& rangeParamCell != null && rangeParamCell.Value2 != null)
						{							
							ilBlankRows = 0;
							string strAppName = rangeAppCell.Value2.ToString().ToUpper();
							string strParamName = rangeParamCell.Value2.ToString();

							if(strLastReadAppName.Length != 0 && strLastReadAppName.CompareTo(strAppName) != 0
								&& hashTabExcelAppNameToEndLine.ContainsKey(strLastReadAppName) == false)
							{
								hashTabExcelAppNameToEndLine[strLastReadAppName] = ilLineno.ToString();;
							}
							if(arrayListExcelAppNames.IndexOf(strAppName) == -1)
							{
								arrayListExcelAppNames.Add(strAppName);
							}

							Hashtable hashTabIniParamNameToData = (Hashtable)hashTabIniAppNameToParamHashTab[strAppName];
							if(hashTabIniParamNameToData != null)
							{
								string strCustomerCellText = rangeCustomerCell.PrefixCharacter.ToString()+ rangeCustomerCell.Text.ToString();
								if(hashTabIniParamNameToData[strParamName] != null)
								{									
									string strHashTabIniParamData = hashTabIniParamNameToData[strParamName].ToString();									
									strCustomerCellText = strCustomerCellText.Trim();
									if(!(rangeCustomerCell.Value2 != null && strCustomerCellText.Length >= 0
										&& ((strCustomerCellText.CompareTo(strHashTabIniParamData) == 0)
										||(strCustomerCellText.Length == 0 && strHashTabIniParamData.Length == 0))))
									{
										if(strHashTabIniParamData.Length == 1)
										{
											strHashTabIniParamData = " " + strHashTabIniParamData;
										}
										bmExcelChanged = true;
										rangeCustomerCell.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault,strHashTabIniParamData);
									}
								}
								else if(strCustomerCellText.Length > 0)
								{
									bmExcelChanged = true;
									rangeCustomerCell.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault,"");
								}

								hashTabIniParamNameToData.Remove(strParamName);
								progressBar.Increment(imIncrementStep);
								imIncrementCompleted += imIncrementStep;
							}
							strLastReadAppName = strAppName;
						}
						else if(rangeAppCell != null && rangeAppCell.Value2 == null
							&& rangeParamCell != null && rangeParamCell.Value2 == null)
						{
							ilBlankRows++;
							if(ilBlankRows > 100)
							{
								imExcelEndLineNo = rangeAppCell.Row - 100;
								break;
							}
						}
						else
						{
							ilBlankRows = 0;
						}
						range = range.get_Offset(1,0);						
					}
					if(rangeCustomerCell != null)
					{
						rangeCustomerCell = rangeCustomerCell.get_Offset(1,0);						
					}
				}				
				WriteExtraIniEntriesToXls(ref workSheet,strCustomer);

				if(bmExcelChanged == true)
				{
					workBook.Save();
				}
				workBook.Close(false,"",false);
				xlApp.Quit();
				workBook = null;
				xlApp = null;
				MessageBox.Show(this,"Completed",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Information);
			}
			catch(Exception e)
			{
				bmExcelChanged = false;
				if(workBook != null)
				{
					workBook.Close(false,"",false);
					workBook = null;
				}
				if(xlApp != null)
				{					
					xlApp.Quit();
					xlApp = null;
				}
				MessageBox.Show(this,e.Message + "\n" + e.StackTrace,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			System.Threading.Thread.CurrentThread.CurrentCulture = oldCulterInfo;
		}

		private void WriteExtraIniEntriesToXls(ref Microsoft.Office.Interop.Excel.Worksheet workSheet, string strCustomer)
		{			
			int ilLineno = 0;
			int ilLineAdded = 0;
			int ilCustomerColumn = Convert.ToInt32(hashTabCustomerToColumnRef[strCustomer]);
			for(int i = 0 ; i < arrayListExcelAppNames.Count ; i++)
			{
				Hashtable hashTabIniParamNameToData = (Hashtable)hashTabIniAppNameToParamHashTab[arrayListExcelAppNames[i].ToString()];
				if(hashTabIniParamNameToData != null && hashTabIniParamNameToData.Count > 0)
				{
					IDictionaryEnumerator iDictEnumerator = hashTabIniParamNameToData .GetEnumerator();
					
					while(iDictEnumerator.MoveNext())
					{
						string strLineno = hashTabExcelAppNameToEndLine[arrayListExcelAppNames[i]].ToString();
						ilLineno = Convert.ToInt32(strLineno) + ilLineAdded++;
						imExcelEndLineNo++;

						Microsoft.Office.Interop.Excel.Range range = workSheet.get_Range("A2","B2");
						System.Boolean blRowInserted = (System.Boolean)workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0).Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown,System.Type.Missing);
						if(blRowInserted)
						{
							Microsoft.Office.Interop.Excel.Range rangeNewRow = workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0);
							Microsoft.Office.Interop.Excel.Range rangeCustomerCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,ilCustomerColumn);
							Microsoft.Office.Interop.Excel.Range rangeAppCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,1);
							Microsoft.Office.Interop.Excel.Range rangeParamCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,2);				
											
							if(rangeAppCell != null && rangeParamCell != null)
							{
								bmExcelChanged = true;
								rangeAppCell.Value2 = arrayListExcelAppNames[i].ToString();// + "_NEW";
								rangeParamCell.Value2 = iDictEnumerator.Key;// + "_NEW";
								rangeCustomerCell.Value2 = iDictEnumerator.Value;
								progressBar.Increment(imIncrementStep);
								imIncrementCompleted += imIncrementStep;								
							}
						}
					}					
					ilLineno ++;ilLineAdded++;imExcelEndLineNo++;
					System.Boolean blRowBreak = (System.Boolean)workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0).Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown,System.Type.Missing);
					hashTabIniParamNameToData.Clear();
					hashTabIniAppNameToParamHashTab.Remove(arrayListExcelAppNames[i].ToString());
				}
			}

			if(hashTabIniAppNameToParamHashTab.Count > 0)
			{
				IDictionaryEnumerator iDictEnumerator = hashTabIniAppNameToParamHashTab.GetEnumerator();
				while(iDictEnumerator.MoveNext())
				{
					Hashtable hashTabIniParamNameToData = (Hashtable)iDictEnumerator.Value;
					IDictionaryEnumerator iDictParamNameToDataEnumerator = hashTabIniParamNameToData.GetEnumerator();
					while(iDictParamNameToDataEnumerator.MoveNext())
					{
						ilLineno = imExcelEndLineNo++;							
						System.Boolean blRowInserted = (System.Boolean)workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0).Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown,System.Type.Missing);
						if(blRowInserted)
						{
							Microsoft.Office.Interop.Excel.Range rangeNewRow = workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0);
							Microsoft.Office.Interop.Excel.Range rangeCustomerCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,ilCustomerColumn);
							Microsoft.Office.Interop.Excel.Range rangeAppCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,1);
							Microsoft.Office.Interop.Excel.Range rangeParamCell = (Microsoft.Office.Interop.Excel.Range)rangeNewRow.get_Item(1,2);				
										
							if(rangeAppCell != null && rangeParamCell != null)
							{
								rangeAppCell.Value2 = iDictEnumerator.Key;// + "_NEW";
								rangeParamCell.Value2 = iDictParamNameToDataEnumerator.Key;// + "_NEW";
								rangeCustomerCell.Value2 = iDictParamNameToDataEnumerator.Value;
								progressBar.Increment(imIncrementStep);
								imIncrementCompleted += imIncrementStep;								
							}
						}
					}
					ilLineno = imExcelEndLineNo++;
					System.Boolean blRowBreak = (System.Boolean)workSheet.get_Range("A1","IV1").get_Offset(ilLineno,0).Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown,System.Type.Missing);
				}
			}
		}

		private void ReadIniFile()
		{
			try
			{
				string strAppName = "";
				string strParamName = "";
				string strParamData = "";

				InitializeDataContainer();
				StreamReader streamIniReader = new StreamReader(strIniFileName);
				string strLineData;
				while((strLineData = streamIniReader.ReadLine()) != null)
				{			
					strLineData = strLineData.Trim();
					if(strLineData.Length > 0 && strLineData[0] != '#')
					{
						if(strLineData[0] == '[' && strLineData[strLineData.Length - 1] == ']')
						{
							strAppName = strLineData.Substring(1,strLineData.Length - 2).ToUpper();
							arrayListIniAppNames.Add(strAppName);
						}
						else if(strLineData.IndexOf("=") != -1)
						{
							strParamName = strLineData.Substring(0,strLineData.IndexOf("="));
							strParamData = strLineData.Substring(strLineData.IndexOf("=")+1);
							//if(strParamData.Length != 0)							
							{
								strParamData = strParamData.Trim();
							}
							/*if(strParamData.Length == 0)
							{
								strParamData = " ";
							}
							else
							{
								strParamData = strParamData.Trim();
							}*/
							strParamName = strParamName.Trim();
							if(strAppName.Length != 0 && strParamName.Length != 0 && strParamData.Length != 0)
							{
								if(hashTabIniAppNameToParamHashTab.ContainsKey(strAppName) == false)
								{
									Hashtable hashTabIniParamNameToData = new Hashtable();							
									hashTabIniParamNameToData[strParamName] = strParamData;
									hashTabIniAppNameToParamHashTab[strAppName] = hashTabIniParamNameToData;
								}
								else
								{
									Hashtable hashTabIniParamNameToData = (Hashtable)hashTabIniAppNameToParamHashTab[strAppName];
									if(hashTabIniParamNameToData != null)
									{
										hashTabIniParamNameToData[strParamName] = strParamData;
									}
								}
							}
						}
					}
				}
				streamIniReader.Close();
			}
			catch(Exception e)
			{
				MessageBox.Show(this,e.Message + "\n" + e.StackTrace,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void btnSelectIniFile_Click(object sender, System.EventArgs e)
		{
			try
			{
				OpenFileDialog openFileDialog1 = new OpenFileDialog();
				openFileDialog1.InitialDirectory = "c:\\" ;
				openFileDialog1.Filter = "ini files (*.ini)|*.ini||" ;
				openFileDialog1.FilterIndex = 2 ;
				openFileDialog1.RestoreDirectory = true ;
				openFileDialog1.Title = "Select Ini File";

				if(openFileDialog1.ShowDialog() == DialogResult.OK)
				{
					strIniFileName = openFileDialog1.FileName;
					txtBoxIniFile.Text = strIniFileName;
					StringBuilder strData = new StringBuilder(255,255);					
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this,ex.Message + "\n" + ex.StackTrace,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void btnSelectXlsFile_Click(object sender, System.EventArgs e)
		{
			try
			{
				OpenFileDialog openFileDialog1 = new OpenFileDialog();
				openFileDialog1.InitialDirectory = "c:\\" ;
				openFileDialog1.Filter = "xls files (*.xls)|*.xls||" ;
				openFileDialog1.FilterIndex = 2 ;
				openFileDialog1.RestoreDirectory = true ;
				openFileDialog1.Title = "Select Xls File";
		
				if(openFileDialog1.ShowDialog() == DialogResult.OK)
				{
					strExcelFileName = openFileDialog1.FileName;
					txtBoxXlsFile.Text = strExcelFileName;
					FillupCustomerComboBox();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this,ex.Message + "\n" + ex.StackTrace,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void btnOK_Enter(object sender, System.EventArgs e)
		{			
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{						
			OnOK();
		}

		private void OnOK()
		{
			if(cmb_Customer.SelectedIndex != -1)
			{
				if(strIniFileName.Length == 0)
				{
					MessageBox.Show(this,"Please enter Ini file name","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
				else if(strIniFileName.ToUpper().IndexOf("\\CEDA.INI") == -1)
				{
					MessageBox.Show(this,"Except Ceda.ini file the software has not been developed","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
				else
				{			
					ReadIniFile();
					progressBar.Visible = true;
					CalculateIncrementStep();
					WriteToXls(cmb_Customer.SelectedItem.ToString());
					progressBar.Visible = false;
					bmExcelChanged = false;
				}
			}
		}

		private void CalculateIncrementStep()
		{	
			int ilIniEntryCount = 0;			
			IDictionaryEnumerator iDictEnumerator = hashTabIniAppNameToParamHashTab .GetEnumerator();
			while(iDictEnumerator.MoveNext())
			{						
				ilIniEntryCount += ((Hashtable)iDictEnumerator.Value).Count;
			}
			progressBar.Maximum = ilIniEntryCount;
			progressBar.Minimum = 0;
			progressBar.Value = 0;
			imIncrementStep = 1;			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			if(xlApp != null)
			{
				xlApp.Quit();				
			}			
			Application.Exit();
		}

		private void btnCancel_Enter(object sender, System.EventArgs e)
		{			
		}		

		private void ReadConfigWriteXls_Load(object sender, System.EventArgs e)
		{
		
		}		
	}
}