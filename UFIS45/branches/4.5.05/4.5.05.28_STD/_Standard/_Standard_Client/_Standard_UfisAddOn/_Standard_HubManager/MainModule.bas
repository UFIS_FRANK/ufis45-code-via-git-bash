Attribute VB_Name = "MainModule"
Option Explicit

Public strTimeFrameFrom As String
Public strTimeFrameTo As String
'Public datTimeMarkerStart As Date
'Public datTimeMarkerEnd As Date
Public strServer As String
Public strHopo As String
Public strTableExt  As String
Public strShowHiddenData As String
Public strAppl As String
Public strAddiFeature As String
Public strAllFtyps As String
Public strAftTimeFields As String ' to be used for UTC to local convertation
Public bgAutoLogin As Boolean
Public strCurrentUser As String
Public strDecisionOption As String
Public pstrShowYellowLines As String 'igu on 25 Jan 2010
Public pblnCopyToSpecialRequirements As Boolean 'igu on 27/06/2011
Public intBcRefreshInterval As Integer 'igu on 05/01/2012
'***Default connection times
Public strPaxDefault As String
Public TimesInUTC As Boolean       ' True if UTC, false if local time
                                 ' shall be displayed
Public UTCOffset As Integer

'********
'Colors in FIPS
Public colGray As Long
Public colLightGray As Long
Public colWhite As Long
Public colYellow As Long
Public colGreen As Long
Public colOrange As Long
Public colLime As Long
Public colBlue As Long
Public colLightBlue As Long
Public colorCorrespondingBar As Long
Public colLightYellow As Long
Public colBrightOrange As Long
Public MySitaOutType As String

Public pstrTelexOriginAddress As String 'igu on 09/02/2012
Public pstrTelexOriginDescription As String 'igu on 09/02/2012

Public Sub Main()
    Dim strAutoLogin As String
    
    strServer = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HOSTNAME", "")
    strHopo = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HOMEAIRPORT", "")
    strTableExt = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TABLEEXTENSION", "")
    strShowHiddenData = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HIDDENDATA", "")
    strAutoLogin = GetIniEntry("", "HUBMANAGER", "GLOBAL", "AUTOLOGIN", "")
    strAppl = "HUB_MGR"
    strAddiFeature = GetIniEntry("", "HUBMANAGER", "GLOBAL", "ADDI_FEATURE", "")
    strDecisionOption = GetIniEntry("", "HUBMANAGER", "GLOBAL", "DECISION", "")
    
    pstrShowYellowLines = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SHOW_YELLOW_LINES", "YES")  'igu on 25 Jan 2010
    pblnCopyToSpecialRequirements = (GetIniEntry("", "HUBMANAGER", "GLOBAL", _
        "COPY_TO_SPECIAL_REQUIREMENTS", "NO") = "YES") 'igu on 27/06/2011
        
    'igu on 09/02/2012
    '-----------------
    pstrTelexOriginAddress = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TELEX_ORIGIN_ADDRESS", "SINKDXH")
    pstrTelexOriginDescription = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TELEX_ORIGIN_DESCRIPTION", "SATS Operations Control Centre SOCC for T2 airlines")
    '-----------------
        
    'igu on 05/01/2012
    '-----------------
    Dim strBcRefreshInterval As String
    strBcRefreshInterval = GetIniEntry("", "HUBMANAGER", "GLOBAL", "REFRESH_INTERVAL_IN_MINUTES", "1")
    intBcRefreshInterval = Val(strBcRefreshInterval)
    If intBcRefreshInterval <= 0 Then intBcRefreshInterval = 1
    '-----------------
    
    strAllFtyps = "S,O,X,N,D,R,OO,OS,SO"
    strAftTimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,AIRB"
    
    'Login procedure
    If strAutoLogin = "TRUE" Then
        bgAutoLogin = True
    Else
        bgAutoLogin = False
    End If
    If bgAutoLogin = False And InStr(1, Command, "/CONNECTED") <> 1 Then
'    If bgAutoLogin = False Or InStr(1, Command, "/CONNECTED") = 0 Then
        If frmData.LoginProcedure = False Then
            Unload frmData
            Exit Sub
        End If
    End If

    strCurrentUser = frmData.ULogin.GetUserName
    If strCurrentUser = "" Then strCurrentUser = GetWindowsUserName()
    
    colGray = RGB(128, 128, 128)
    colWhite = RGB(255, 255, 255)
    colYellow = RGB(255, 255, 0)
    colOrange = RGB(255, 193, 164)
    colGreen = RGB(0, 128, 0)
    colLime = RGB(0, 255, 0)
    colBlue = RGB(0, 0, 255)
    colLightBlue = 16777152
    colLightGray = RGB(235, 235, 235) '14737632
    colLightYellow = 12648447
    colBrightOrange = 33023
    strPaxDefault = "60"
    
    TimesInUTC = False
    
    StartUpApplication
End Sub

Public Sub StartUpApplication()
    Dim wasCancel As Boolean
    
    frmLoad.Show vbModal
    wasCancel = frmLoad.wasCancel
    Unload frmLoad
    If wasCancel = True Then
        Unload frmData
        Exit Sub
    End If
    
    frmMain.Show
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    '
End Sub

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim prntText As String
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function
