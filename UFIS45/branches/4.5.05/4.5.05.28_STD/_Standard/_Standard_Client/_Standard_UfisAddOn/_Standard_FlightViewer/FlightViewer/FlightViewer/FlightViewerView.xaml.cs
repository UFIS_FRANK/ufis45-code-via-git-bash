﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ufis.Entities;
using Ufis.Utilities;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using FlightViewer.Helpers;


namespace FlightViewer.FlightViewer
{
    
    /// <summary>
    /// Interaction logic for FlightViewerView.xaml
    /// </summary>
    public partial class FlightViewerView : UserControl
    {
        char cFlight;
        char cAirport;
        public HpParameters param;
        public FlightViewerView()
        {            
            InitializeComponent();
            
            radAFBoth.IsChecked = true;
            radALBth.IsChecked = true;
            if (txtAirport.Text == "")
            {
                radAFBoth.IsEnabled = false;
                radAFOrg.IsEnabled = false;
                radAFDes.IsEnabled = false;
            }
            if (txtAirline.Text == "")
            {
                radALArr.IsEnabled = false;
                radALDep.IsEnabled = false;
                radALBth.IsEnabled = false;
            }
            
        }
        /// <summary>
        /// Get Season date 
        /// </summary>        
        private void dxpcbeSeason_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {          
            EntDbSeason objSeason = (EntDbSeason)e.NewValue;
            txtValidFrom.EditValue = objSeason.ValidFrom;
            txtValidTo.EditValue = objSeason.ValidTo;
        }
        /// <summary>
        /// Change column value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dxpgrcSeasonFlightLists_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value != null) 
            {
                switch (e.Column.FieldName)
                {
                    case "DaysOfOperation":
                        e.DisplayText = e.Value.ToString().Replace("0", "-");
                        break;
                    case "StandardTimeOfArrival":
                    case "StandardTimeOfDeparture":
                        e.DisplayText = string.Format("{0:hh\\:mm}", e.Value);
                        break;
                }
            }
        }

        public class MyGridControl : GridControl
        {
            protected override void OnItemsSourceChanged(object oldValue, object newValue)
            {
                base.OnItemsSourceChanged(oldValue, newValue);
                PopulateColumns();
            }
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button objbtn = (Button)sender;
            switch (objbtn.Content.ToString())
            {
                case "+":
                    objbtn.Content = "-";
                    break;
                default:
                    objbtn.Content = "+";
                    break;
            }
        }


        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                //Search Result....
                FlightViewerViewModel viewModel = (FlightViewerViewModel)DataContext;
                Boolean bAirportEI, bAirlineEI;
                if (btnAPei.Content.ToString() == "+")
                    bAirportEI = true;
                else
                    bAirportEI = false;

                if (btnAFei.Content.ToString() == "+")
                    bAirlineEI = true;
                else
                    bAirlineEI = false;
                
                //Get Data
                if (txtValidFrom.EditValue == null || txtValidTo.EditValue == null)
                {
                    MessageBox.Show("Please specify valid period of season to load!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    DateTime validFrom = (DateTime)txtValidFrom.EditValue;
                    DateTime validTo = (DateTime)txtValidTo.EditValue;

                    viewModel.SearchSeason(dxpcbeSeason.Text, validFrom, validTo, txtAirport.Text, bAirportEI, txtAirline.Text, bAirlineEI, cAirport, cFlight);
                }
                
            }       
                
            catch (Exception ex)
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                MessageBox.Show(ex.Message);
            }

        }
        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            param = new HpParameters();

            string strValidFrom = string.Empty;
            if (txtValidFrom.EditValue != null)
                strValidFrom = ((DateTime)txtValidFrom.EditValue).ToString("dd.MM.yyyy");

            string strValidTo = string.Empty;
            if (txtValidTo.EditValue != null)
                strValidTo = ((DateTime)txtValidTo.EditValue).ToString("dd.MM.yyyy");
                
            param.MyParameter = String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo);

            if (txtAirline.Text != "")
            {
                if (btnAFei.Content.ToString() == "+")
                {
                    param.MyParameter = param.MyParameter + String.Format(" , Airline(s)  in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ","));
                }
                else
                {
                    param.MyParameter = param.MyParameter + String.Format(" , Airline(s) not in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ","));
                }
            }
            if (txtAirport.Text != "")
            {
                if (btnAPei.Content.ToString() == "+")
                {
                    param.MyParameter = param.MyParameter + String.Format(" , Airport in ( {0} )", txtAirport.Text.ToUpper().Replace(" ", ","));
                }
                else
                {
                    param.MyParameter = param.MyParameter + String.Format(" , Airport not in ( {0} )", txtAirport.Text.ToUpper().Replace(" ", ","));
                }
            }
            ((HpParameters)Resources["paramet"]).MyParameter = this.param.MyParameter;
            if (dxpgrcSeasonFlightLists.VisibleRowCount > 0)
            {
                PrintableControlLink link = new PrintableControlLink((TableView)dxpgrcSeasonFlightLists.View);                
                link.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
                link.Landscape = true;
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];                  
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }

        private void Radio_Click(object sender, RoutedEventArgs e)
        {
            RadioButton objRdb = (RadioButton)sender;
            switch (objRdb.Name)
            {
                case "radAFBoth":
                    cAirport = 'B';
                    break;
                case "radAFOrg":
                    cAirport = 'O';
                    break;
                case "radAFDes":
                    cAirport = 'D';
                    break;
                case "radALArr":
                    cFlight = 'A';
                    break;
                case "radALBth":
                    cFlight = 'C';
                    break;
                default:
                    cFlight = 'D';
                    break;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox objTextBox = (TextBox)sender;
            switch (objTextBox.Name)
            {
                case "txtAirline":
                    if (txtAirline.Text.Length > 0)
                    {                        
                        radALBth.IsChecked = true;
                        radALBth.IsEnabled = true;
                        radALArr.IsEnabled = true;
                        radALDep.IsEnabled = true;
                        cFlight = 'C';
                    }
                    else
                    {
                        radALBth.IsEnabled = false;
                        radALArr.IsEnabled = false;
                        radALDep.IsEnabled = false;                        
                    }
                    break;
                default:
                    if (txtAirport.Text.Length > 0)
                    {
                        radAFBoth.IsChecked = true;
                        radAFBoth.IsEnabled = true;
                        radAFOrg.IsEnabled = true;
                        radAFDes.IsEnabled = true;
                        cAirport = 'B';
                    }
                    else
                    {
                        radAFBoth.IsEnabled = false;
                        radAFOrg.IsEnabled = false;
                        radAFDes.IsEnabled = false;
                    }
                    break;

            }
        }        
    }
    public class HeaderFormattingOptions
    {
        public double FontSize { get; set; }
        // other options ...
    }
}
