VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmFilter 
   Caption         =   "Flights (Filter)"
   ClientHeight    =   9795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7440
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9795
   ScaleWidth      =   7440
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picBackground 
      AutoRedraw      =   -1  'True
      Height          =   9780
      Left            =   0
      ScaleHeight     =   9720
      ScaleWidth      =   7380
      TabIndex        =   0
      Top             =   0
      Width           =   7440
      Begin VB.TextBox txtFlno 
         Height          =   315
         Left            =   3825
         TabIndex        =   3
         Top             =   90
         Width           =   1335
      End
      Begin VB.CommandButton btnDoFilter 
         Caption         =   "&Do Filter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5355
         TabIndex        =   10
         Top             =   900
         Width           =   1725
      End
      Begin VB.CommandButton btnClose 
         Caption         =   "&Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3060
         TabIndex        =   12
         Top             =   9180
         Width           =   1320
      End
      Begin VB.TextBox txtKeycode 
         Height          =   315
         Left            =   3825
         TabIndex        =   9
         Top             =   900
         Width           =   1335
      End
      Begin VB.TextBox txtNature 
         Height          =   315
         Left            =   1215
         TabIndex        =   7
         Top             =   900
         Width           =   1335
      End
      Begin VB.TextBox txtOrigin 
         Height          =   315
         Left            =   1215
         TabIndex        =   5
         Top             =   495
         Width           =   1335
      End
      Begin VB.TextBox txtAirline 
         Height          =   315
         Left            =   1215
         TabIndex        =   1
         Top             =   90
         Width           =   1335
      End
      Begin TABLib.TAB tabFlights 
         Height          =   7575
         Left            =   225
         TabIndex        =   11
         Top             =   1350
         Width           =   6990
         _Version        =   65536
         _ExtentX        =   12330
         _ExtentY        =   13361
         _StockProps     =   64
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "FLNO:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3060
         TabIndex        =   2
         Top             =   135
         Width           =   735
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Key Code:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2745
         TabIndex        =   8
         Top             =   945
         Width           =   1005
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Nature:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   6
         Top             =   945
         Width           =   735
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Orig/Dest:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   4
         Top             =   540
         Width           =   870
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Airline:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   300
         TabIndex        =   13
         Top             =   135
         Width           =   735
      End
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strMode As String ' "" is just for filter,
                         ' "CHOICE" is as choice list

Private Sub btnClose_Click()
    Dim selLine As Long
    Dim strUrno As String
    Dim strAdid As String
    
    If strMode = "CHOICE" Then
        selLine = tabFlights.GetCurrentSelected
        If selLine > -1 Then
            strUrno = tabFlights.GetFieldValue(selLine, "URNO")
            strAdid = tabFlights.GetFieldValue(selLine, "ADID")
            If strUrno <> "" And strAdid = "D" Then
                frmMain.InsertConnection strUrno
            End If
        End If
    End If
    If strMode <> "CHOICE" Then
        Unload Me
    End If
End Sub

Private Sub btnDoFilter_Click()
    Dim strFAlc As String
    Dim strFFlno As String
    Dim strFOrig As String
    Dim strFDest As String
    Dim strFNat As String
    Dim strFKeycode As String
    Dim strAlc2 As String
    Dim strFlno As String
    Dim strAlc3 As String
    Dim strOrig As String
    Dim strDest As String
    Dim strNat As String
    Dim strKeycode As String
    Dim strAdid As String
    Dim blInsert As Boolean
    Dim strValues As String
    Dim i As Long
    
    strFAlc = txtAirline.Text
    strFFlno = txtFlno.Text
    strFOrig = txtOrigin.Text
    strFNat = txtNature.Text
    strFKeycode = txtKeycode.Text
    
    tabFlights.ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        blInsert = True
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        strAlc2 = frmData.tabData(0).GetFieldValue(i, "ALC2")
        strAlc3 = frmData.tabData(0).GetFieldValue(i, "ALC3")
        strOrig = frmData.tabData(0).GetFieldValue(i, "ORG3")
        strDest = frmData.tabData(0).GetFieldValue(i, "DES3")
        strNat = frmData.tabData(0).GetFieldValue(i, "TTYP")
        strKeycode = frmData.tabData(0).GetFieldValue(i, "STEV")
        If strAdid = "A" And strMode <> "CHOICE" Then
            If (strFAlc <> "") Then
                If strFAlc <> strAlc2 Then
                    If strFAlc <> strAlc3 Then
                        blInsert = False
                    End If
                End If
            End If
            If strFFlno <> "" Then
                If strFFlno <> strFlno Then
                    blInsert = False
                End If
            End If
            If strFOrig <> "" Then
                If strFOrig <> strOrig Then
                    blInsert = False
                End If
            End If
            If strFNat <> "" Then
                If strFNat <> strNat Then
                    blInsert = False
                End If
            End If
            If strFKeycode <> "" Then
                If strFKeycode <> strKeycode Then
                    blInsert = False
                End If
            End If
            If blInsert = True Then ' insert the record
                strValues = frmData.tabData(0).GetFieldValues(i, "URNO,ADID,FLNO,STOA,ETAI,TIFA,ORG3")   '"URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
                tabFlights.InsertTextLine strValues, False
            End If
        End If
        If strAdid = "D" Then
            If (strFAlc <> "") Then
                If strFAlc <> strAlc2 Then
                    If strFAlc <> strAlc3 Then
                        blInsert = False
                    End If
                End If
            End If
            If strFFlno <> "" Then
                If strFFlno <> strFlno Then
                    blInsert = False
                End If
            End If
            If strFOrig <> "" Then
                If strFOrig <> strDest Then
                    blInsert = False
                End If
            End If
            If strFNat <> "" Then
                If strFNat <> strNat Then
                    blInsert = False
                End If
            End If
            If strFKeycode <> "" Then
                If strFKeycode <> strKeycode Then
                    blInsert = False
                End If
            End If
            If blInsert = True Then ' insert the record
                strValues = frmData.tabData(0).GetFieldValues(i, "URNO,ADID,FLNO,STOD,ETDI,TIFD,DES3")   '"URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
                tabFlights.InsertTextLine strValues, False
            End If
        End If
    Next i
    tabFlights.Sort "5", True, True
    tabFlights.Refresh
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    DrawBackGround picBackground, 7, True, True
    tabFlights.ResetContent
    tabFlights.HeaderString = "URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
    tabFlights.LogicalFieldList = "URNO,ADID,FLNO,SCHED,EST,ACTUAL,ORGDES"
    tabFlights.HeaderLengthString = "0,40,80,80,80,80,80"
    tabFlights.ColumnWidthString = "10,2,8,14,14,14,5"
    tabFlights.LifeStyle = True
    
    If strMode = "CHOICE" Then
        Me.Caption = "Insert flight to connections ..."
        btnClose.Caption = "&Insert Flight"
        frmMain.cbToolButton(9).Enabled = False
    Else
        frmMain.cmdInsert.Enabled = False
    End If
    For i = 3 To 5
        tabFlights.DateTimeSetColumn i
        tabFlights.DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
    Next i
    
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    frmMain.cbToolButton(9).Enabled = True
    frmMain.cmdInsert.Enabled = True
End Sub

Private Sub tabFlights_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strUrno As String
    Dim strTif As String
    Dim strAdid As String
    Dim datTif As Date
    
    If strMode <> "CHOICE" And LineNo > -1 Then
        strUrno = tabFlights.GetFieldValue(LineNo, "URNO")
        strTif = tabFlights.GetFieldValue(LineNo, "ACTUAL")
        If strTif <> "" Then
            datTif = CedaFullDateToVb(strTif)
            datTif = DateAdd("h", -1, datTif)
        End If
        frmMain.SelectMainGanttBar strUrno, 0
        frmCompressedFligths.gantt.ScrollTo datTif
        frmCompressedFligths.gantt.Refresh
        frmCompressedFligths.lblAboveText(0) = Format(datTif, "dd.mm.YY")
        frmCompressedFligths.lblAboveText(0).Refresh
        
        frmMain.gantt(0).ScrollTo datTif
        frmMain.gantt(0).Refresh
        frmMain.lblAboveText(0) = Format(datTif, "dd.mm.YY")
        frmMain.lblAboveText(0).Refresh
    End If
End Sub

Private Sub tabFlights_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        If ColNo = tabFlights.CurrentSortColumn Then
            If tabFlights.SortOrderASC = True Then
                tabFlights.Sort CStr(ColNo), False, True
            Else
                tabFlights.Sort CStr(ColNo), True, True
            End If
        Else
            tabFlights.Sort CStr(ColNo), True, True
        End If
        tabFlights.Refresh
    Else
        btnClose_Click
    End If
End Sub

Private Sub txtAirline_GotFocus()
    SetTextSelected
End Sub

Private Sub txtAirline_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFlno_GotFocus()
    SetTextSelected
End Sub

Private Sub txtFlno_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtKeycode_GotFocus()
    SetTextSelected
End Sub

Private Sub txtNature_GotFocus()
    SetTextSelected
End Sub

Private Sub txtOrigin_GotFocus()
    SetTextSelected
End Sub

Private Sub txtOrigin_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
