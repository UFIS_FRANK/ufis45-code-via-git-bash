VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmViewLog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Log"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7380
   Icon            =   "frmViewLog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtViewLog 
      Height          =   1935
      Left            =   0
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   3000
      Width           =   7335
   End
   Begin TABLib.TAB tabL04 
      Height          =   2925
      Left            =   0
      TabIndex        =   0
      Tag             =   "L04TAB;FLST,FVAL,HOPO,MORE,ORNO,SFKT,STAB,TIME,URNO,USEC;"
      Top             =   0
      Width           =   7335
      _Version        =   65536
      _ExtentX        =   12938
      _ExtentY        =   5159
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmViewLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'kkh on 25/07/2008 P2 View Decision Log
Private Sub Form_Load()
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
    tabL04.FontSize = 8
    'tabL04.FontBold = True 'MyFontBold
    tabL04.ResetContent
    tabL04.Refresh
    
    'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_ViewLog", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_ViewLog", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_ViewLog", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_ViewLog", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
                        
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
End Sub

Public Sub TabReadLog(strCfiUrno As String)
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpSqlKey As String
    Dim LineNo As Long
    Dim i, k As Integer
    Dim j, m As Integer
    Dim Data() As String
    Dim strDecisionHex As String
    Dim strTransferHex As String
    Dim strDecision As String
    Dim strTransfer As String
    Dim KeyVal As String
    Dim keyVal2 As String
    
    'tabL04.ResetContent
    tabL04.CedaServerName = strServer
    tabL04.CedaPort = "3357"
    tabL04.CedaHopo = strHopo
    tabL04.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
    tabL04.CedaTabext = "TAB"
    tabL04.CedaUser = strCurrentUser 'GetWindowsUserName()
    tabL04.CedaWorkstation = frmData.aUfis.GetWorkStationName   'GetWorkstationName()
    tabL04.CedaSendTimeout = "3"
    tabL04.CedaReceiveTimeout = "240"
    tabL04.CedaRecordSeparator = Chr(10)
    tabL04.CedaIdentifier = "CfiTab"
    tabL04.HeaderString = "FLST,FVAL,ORNO,SFKT,TIME,URNO,USEC,DECISION,TRANSFER"
    tabL04.HeaderLengthString = "0,0,0,60,160,0,80,100,110"
    'tabL04.ColumnWidthString = "80,80,80,80,80,80,80"
    tabL04.EnableHeaderSizing True
    'tabL04.IndexCreate "TIME", 4
    
    tmpCmd = "RT"
    tmpTable = "L04TAB"
    tmpFields = "FLST,FVAL,ORNO,SFKT,TIME,URNO,USEC"
    tmpData = ""
    tmpSqlKey = "WHERE ORNO='" & strCfiUrno & "' ORDER BY TIME"
        
    If strServer <> "LOCAL" Then
        Screen.MousePointer = 11
        tabL04.CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey
        frmViewLog.Show
        'kkh on 20/01/2009 check UTC or local before insert into CFITAB and L04TAB
        ChangetabL04ToLocal

        For i = 0 To tabL04.GetLineCount - 1
            strTransferHex = ""
            strDecisionHex = ""
            KeyVal = ""
            For k = 1 To Len(tabL04.GetColumnValue(i, 1))
                KeyVal = KeyVal + Hex(Asc(Mid(tabL04.GetColumnValue(i, 1), k, 1)))   ' Build Value Char. By Char.
            Next k
            Data = Split(KeyVal, "1E")
            For j = 0 To UBound(Data)
                If tabL04.GetColumnValue(i, 3) = "IRT" Then
                    If j = 19 Then
                        strTransferHex = Data(19)
                        strDecisionHex = Data(20)
                    End If
                Else
                    If j = 17 Then
                        strTransferHex = Data(17)
                        strDecisionHex = Data(18)
                    End If
                End If
            Next j
            keyVal2 = ""
            strDecision = ""
            'get decision FVAL
            For m = 1 To Len(strDecisionHex)
                If Len(keyVal2) = 1 Then
                    keyVal2 = keyVal2 + Mid(strDecisionHex, m, 1)
                    strDecision = strDecision + ChrW("&h" & keyVal2)
                Else
                    If Len(keyVal2) >= 2 Then
                        keyVal2 = ""
                        keyVal2 = Mid(strDecisionHex, m, 1)
                    Else
                        keyVal2 = Mid(strDecisionHex, m, 1)
                    End If
                End If
            Next
            keyVal2 = ""
            strTransfer = ""
            'get Transfer time FVAL
            For m = 1 To Len(strTransferHex)
                If Len(keyVal2) = 1 Then
                    keyVal2 = keyVal2 + Mid(strTransferHex, m, 1)
                    strTransfer = strTransfer + ChrW("&h" & keyVal2)
                Else
                    If Len(keyVal2) >= 2 Then
                        keyVal2 = ""
                        keyVal2 = Mid(strTransferHex, m, 1)
                    Else
                        keyVal2 = Mid(strTransferHex, m, 1)
                    End If
                End If
            Next
            tabL04.SetColumnValue i, 7, strDecision
            tabL04.SetColumnValue i, 8, strTransfer
        Next i
        If tabL04.GetLineCount < 1 Then
            txtViewLog.Text = "No Log details found."
        Else
            txtViewLog.Text = "Please click row to see log details."
        End If
        Screen.MousePointer = 0
    End If
    
    LineNo = frmMain.tabTab(1).GetCurrentSelected
    If LineNo <> -1 Then
        frmViewLog.Caption = "Decision Log - Dep Flight " + frmMain.tabTab(1).GetFieldValue(LineNo, "FLNO") + " " + frmMain.tabTab(1).GetFieldValue(LineNo, "DES")
    Else
        frmViewLog.Caption = "Decision Log for Dep Flight "
    End If
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_ViewLog", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_ViewLog", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_ViewLog", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_ViewLog", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
End Sub

'display log details
Private Sub tabL04_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strLine As Long
    Dim delimiterCnt As Integer
    Dim delimiterContent() As String
    Dim strDisplay As String
    Dim KeyVal As String
    Dim keyValFinal As String
    Dim delimiterCnt2 As Integer
    
    strLine = tabL04.GetCurrentSelected
    txtViewLog.Text = ""
    
    For i = 1 To Len(tabL04.GetColumnValue(strLine, 1))   ' Convert Each Bit
        KeyVal = Hex(Asc(Mid(tabL04.GetColumnValue(strLine, 1), i, 1)))   ' Build Value Char. By Char.
        If KeyVal = "1E" Then
            keyValFinal = keyValFinal + "|"
        Else
            keyValFinal = keyValFinal + ChrW("&h" & KeyVal)
        End If
    Next
    delimiterContent = Split(keyValFinal, "|")
    If tabL04.GetColumnValue(strLine, 3) = "URT" Then
        delimiterCnt2 = 16
    Else
        delimiterCnt2 = 18
    End If
    For delimiterCnt = 0 To UBound(delimiterContent)
        'kkh on 20/01/2009 hide details of CDAT and LSTU
        If delimiterCnt <> 1 And delimiterCnt <> 3 Then
            If delimiterCnt < delimiterCnt2 Then
                strDisplay = strDisplay & "|" & delimiterContent(delimiterCnt)
            Else
                If delimiterCnt = delimiterCnt2 Then
                    strDisplay = strDisplay & "|" & delimiterContent(delimiterCnt) & vbNewLine
                Else
                    strDisplay = strDisplay & "|" & delimiterContent(delimiterCnt)
                End If
            End If
        End If
    Next delimiterCnt
    
    txtViewLog.Text = strDisplay
End Sub
Private Sub tabL04_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    tabL04_SendLButtonClick LineNo, Selected
End Sub
Private Sub Form_Unload(Cancel As Integer)
    frmMain.cbToolButton(11).backColor = vbButtonFace
    frmMain.cbToolButton(11).value = 0
    Unload Me
End Sub

Public Sub ChangetabL04ToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    Dim strL04TimeFields As String
    
    strL04TimeFields = "TIME"
    cnt = tabL04.GetLineCount
    fldCnt = ItemCount(strL04TimeFields, ",")
    For i = 0 To cnt
        For j = 1 To fldCnt
            strField = GetRealItem(strL04TimeFields, CInt(j), ",")
            'strVal = tabL04.GetFieldValue(i, strField)
            strVal = tabL04.GetColumnValues(i, 4) ' TIME
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = False Then
                    myDat = DateAdd("h", UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                'tabL04.SetFieldValues i, strField, strVal
                tabL04.SetColumnValue i, 4, strVal
                tabL04.Refresh
            End If
        Next j
    Next i
    tabL04.Refresh
End Sub
