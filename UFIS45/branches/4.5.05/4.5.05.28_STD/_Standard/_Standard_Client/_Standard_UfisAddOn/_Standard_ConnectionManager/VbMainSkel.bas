Attribute VB_Name = "MainModule"
Option Explicit
Public strTimeFrameFrom As String
Public strTimeFrameTo As String
Public datTimeMarkerStart As Date
Public datTimeMarkerEnd As Date
Public strTableExt  As String
Public strAppl As String
Public strHopo As String
Public strServer As String
Public strTestServer As String
Public strTestHopo As String
Public strAllFtyps As String
Public strShowHiddenData As String
Public readFromFile As Boolean
Public MainGantt As UGantt
Public DetailGantt As UGantt

Public strCurrentUser As String

Public igTmpIDX As Integer
Public igAPP_IDX As Integer

Public UTCOffset As Integer

'********
'Colors in FIPS
Public colGray As Long
Public colLightGray As Long
Public colWhite As Long
Public colYellow As Long
Public colGreen As Long
Public colOrange As Long
Public colLime As Long
Public colBlue As Long
Public colLightBlue As Long
Public colorCorrespondingBar As Long
Public colLightYellow As Long
Public colBrightOrange As Long

'***CFG DATA
Public colorArrivalBar As Long
Public colorDepartureBar As Long
Public colorCurrentBar As Long
Public DetailGanttTimeFrame As Long
Public MinDepartureBarLen As Long 'Minumum length of the departure bar
Public MinArrivalBarLen As Long   'Minumum length of the arrival bar
Public ShowHints As Boolean       ' shows hints or not
Public TimesInUTC As Boolean       ' True if UTC, false if local time
                                 ' shall be displayed
'***END CFG DATA
Public strScriptSourceCode As String
Public dataFromFile As Boolean
'***Default connectio times
Public strPaxDefault As String
Public strMailDefault As String
Public strCargoDefault As String
Public strAftTimeFields As String ' to be used for UTC to local convertation
Public bgAutoLogin As Boolean

Public bgfrmCompressedFlightsOpen As Boolean
'Reset frmMain DECISION dropdown
Public bgfrmMainOpen As Boolean
'warning colour orange
'Public compressedFlightBarWarnColour As Boolean
'Public compressedFlightBarCritColour As Boolean
'sending telex
Public MySitaOutType As String
Public strAddiFeature As String
Public strDecisionOption As String
Public pstrShowYellowLines As String 'igu on 25 Jan 2010
Public pblnCopyToSpecialRequirements As Boolean 'igu on 27/06/2011

Public Sub Main()
    Dim strAutoLogin As String
    
    dataFromFile = True
    'TO DO: Read setup values from ceda.ini or registry
    MinDepartureBarLen = 15
    MinArrivalBarLen = 15
    ShowHints = True
    colorCurrentBar = 16711935
    colorCorrespondingBar = vbYellow
    colorArrivalBar = 16777215
    colorDepartureBar = 16777152
    DetailGanttTimeFrame = 2 'hours
    strServer = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HOSTNAME", "")
    'TO DO: Remove all strTestServer and strTestHopo
    strTestServer = "Tonga"
    strTestHopo = "ATH"
    strHopo = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HOMEAIRPORT", "")
    strTableExt = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TABLEEXTENSION", "")
    strShowHiddenData = GetIniEntry("", "HUBMANAGER", "GLOBAL", "HIDDENDATA", "")
    strAutoLogin = GetIniEntry("", "HUBMANAGER", "GLOBAL", "AUTOLOGIN", "")
    readFromFile = False
    strAppl = "HUB_MGR"
    strAddiFeature = GetIniEntry("", "HUBMANAGER", "GLOBAL", "ADDI_FEATURE", "")
    strDecisionOption = GetIniEntry("", "HUBMANAGER", "GLOBAL", "DECISION", "")
    
    pstrShowYellowLines = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SHOW_YELLOW_LINES", "YES")  'igu on 25 Jan 2010
    pblnCopyToSpecialRequirements = (GetIniEntry("", "HUBMANAGER", "GLOBAL", _
        "COPY_TO_SPECIAL_REQUIREMENTS", "NO") = "YES") 'igu on 27/06/2011
    
    strAllFtyps = "S,O,X,N,D,R,OO,OS,SO"
    strAftTimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,AIRB"
    
    'Login procedure
    If strAutoLogin = "TRUE" Then
        bgAutoLogin = True
    Else
        bgAutoLogin = False
    End If
    If bgAutoLogin = False And InStr(1, Command, "/CONNECTED") <> 1 Then
'    If bgAutoLogin = False Or InStr(1, Command, "/CONNECTED") = 0 Then
        If frmData.LoginProcedure = False Then
            End
        End If
    End If

    strCurrentUser = frmData.ULogin.GetUserName
    If strCurrentUser = "" Then strCurrentUser = GetWindowsUserName()
    colGray = RGB(128, 128, 128)
    colWhite = RGB(255, 255, 255)
    colYellow = RGB(255, 255, 0)
    colOrange = RGB(255, 193, 164)
    colGreen = RGB(0, 128, 0)
    colLime = RGB(0, 255, 0)
    colBlue = RGB(0, 0, 255)
    colLightBlue = 16777152
    colLightGray = RGB(235, 235, 235) '14737632
    colLightYellow = 12648447
    colBrightOrange = 33023
    strPaxDefault = "60"
    strMailDefault = "90"
    strCargoDefault = "90"
    TimesInUTC = False
    
    StartUpApplication
    bgfrmCompressedFlightsOpen = False
    
    
End Sub

Public Sub StartUpApplication()
    frmStartup.Show
    frmLoad.Show vbModal, frmStartup
    If frmLoad.wasCancel = True Then
        End
    End If
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    '
End Sub

Public Sub ShutDownApplication(AskBack As Boolean)
    Dim i As Integer
    Dim cnt As Integer
    Dim RetVal As Integer
    RetVal = 1
    If AskBack Then RetVal = 1 'MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer)
    If RetVal = 1 Then
        On Error Resume Next
        ShutDownRequested = True
        cnt = Forms.count - 1
        For i = cnt To 0 Step -1
            Unload Forms(i)
        Next
        'Here we shut down
        End
    End If
    'If not, we come back
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    '
End Sub
Private Sub HandleShutDown(Abort As Boolean)
    If Not Abort Then ShutDownApplication False
    End
End Sub

'********************************
' HELPER FUNCTIONS

'------------------------------------------------------------
' Returns the len for this flight.
' TO DO: returns now the default value, ==> must calculate
' the time in respect to other criteria
Public Function GetArrivalBarLenInMinutes(Urno As String, Tifa As String) As Integer

    GetArrivalBarLenInMinutes = MinArrivalBarLen
End Function
'------------------------------------------------------------
' Returns the len for this flight.
' TO DO: returns now the default value, ==> must calculate
' the time in respect to other criteria
Public Function GetDepartureBarLenInMinutes(Urno As String, Tifa As String) As Integer
    GetDepartureBarLenInMinutes = MinDepartureBarLen
End Function
'TO DO: If necessary for both arr and dep implement this
Public Function HasConnectionConflicts(strAftUrno As String) As Boolean
    HasConnectionConflicts = False
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim prntText As String
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function

Public Function Min(ParamArray values() As Variant) As Variant
    Dim Value As Variant
    Dim minValue As Variant
    Dim i As Integer
    
    i = 0
    For Each Value In values
        If i = 0 Then
            minValue = Value
        Else
            If minValue > Value Then
                minValue = Value
            End If
        End If
    
        i = i + 1
    Next Value
    
    Min = minValue
End Function

Public Function Max(ParamArray values() As Variant) As Variant
    Dim Value As Variant
    Dim maxValue As Variant
    Dim i As Integer
    
    i = 0
    For Each Value In values
        If i = 0 Then
            maxValue = Value
        Else
            If maxValue < Value Then
                maxValue = Value
            End If
        End If
    
        i = i + 1
    Next Value
    
    Max = maxValue
End Function

