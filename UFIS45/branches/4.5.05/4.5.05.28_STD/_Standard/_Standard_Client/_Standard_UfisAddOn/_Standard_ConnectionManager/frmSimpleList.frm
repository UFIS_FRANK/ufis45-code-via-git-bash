VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmSimpleList 
   Caption         =   "List ..."
   ClientHeight    =   8175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3930
   Icon            =   "frmSimpleList.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8175
   ScaleWidth      =   3930
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabList 
      Height          =   7935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   13996
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmSimpleList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public myHour As Integer
Public omAdid As String
Public omListMode As String

Private Sub Form_Load()
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_SimpleList", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_SimpleList", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_SimpleList", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_SimpleList", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
                        
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
'END: Read position from registry and move the window
    
    ShowFlights myHour, omAdid
    tabList.Top = 30
    tabList.Width = Me.ScaleWidth - 30
    tabList.Height = Me.ScaleHeight - 30
End Sub
Public Sub ShowAlternateFlights(strAftLines As String, strDestOrVIA As String)
    Dim cnt As Long
    Dim i As Long
    Dim llLine As Long
    Dim strVal As String
    Dim strAftUrno As String
    Dim curSel As Integer
    Dim strVIAL As String
    
    tabList.ResetContent
    If strHopo = "BKK" Then
        tabList.HeaderString = "Urno,Flno,Time,Des/Via,F-Free,C-Free,U-Free,Y-Free"
        tabList.HeaderLengthString = "0,80,80,60,60,60,60,60"
    Else
        tabList.HeaderString = "Urno,Flno,Time,Dest/Via,F-Free,C-Free,Y-Free"
        tabList.HeaderLengthString = "0,80,80,60,60,60,60"
    End If
    tabList.DateTimeSetColumn 2
    tabList.DateTimeSetColumnFormat 2, "YYYYMMDDhhmmss", "DD'/'hh':'mm"
    cnt = ItemCount(strAftLines, ",")
    For i = 0 To cnt - 1
        llLine = Val(GetRealItem(strAftLines, CInt(i), ","))
        'kkh 31/07/2008 VIAL
        'strVal = frmData.tabData(0).GetFieldValues(llLine, "URNO,FLNO,TIFD,DES3")
        strVIAL = ""
        strVal = frmData.tabData(0).GetFieldValues(llLine, "URNO,FLNO,TIFD")
        strVIAL = frmData.tabData(0).GetFieldValues(llLine, "VIAL")
        
        curSel = frmMain.tabTab(1).GetCurrentSelected
        If curSel > -1 Then
            strVal = strVal + "," + frmMain.tabTab(1).GetFieldValue(curSel, "DES")
        End If
        
        strAftUrno = frmData.tabData(0).GetFieldValue(llLine, "URNO")
        If strHopo = "BKK" Then
            strVal = strVal + "," + GetBookingClassStringBKK(strAftUrno, strVIAL, strDestOrVIA)
        Else
            strVal = strVal + "," + GetBookingClassString(strAftUrno)
        End If
        tabList.InsertTextLine strVal, False
    Next i
    tabList.Sort "2", True, True
    tabList.Refresh
    Me.Caption = "Altern. Connections"
End Sub
Public Sub ShowFlights(Hour As Integer, strAdid As String)
    Dim i As Long
    Dim strVal As String
    Dim strTif As String
    Dim strV2 As String
    Dim strCurrDate As String
    Dim strTifDate As String
    
    tabList.ResetContent
    If strAdid = "A" Then
        tabList.HeaderString = "Urno,Flno,Time,Orig."
    Else
        tabList.HeaderString = "Urno,Flno,Time,Dest."
    End If
    tabList.HeaderLengthString = "0,80,80,80"
    tabList.DateTimeSetColumn 2
    tabList.DateTimeSetColumnFormat 2, "YYYYMMDDhhmmss", "DD'/'hh':'mm"
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        If frmData.tabData(0).GetFieldValue(i, "ADID") = strAdid Then
            If strAdid = "A" Then
                strTif = frmData.tabData(0).GetFieldValue(i, "TIFA")
                strTifDate = Mid(strTif, 1, 8)
                strCurrDate = Format(frmHourlyDistribution.omCurrentDate, "YYYYMMDD")
                If strCurrDate = strTifDate Then
                    strV2 = Mid(strTif, 9, 2)
                    If CInt(strV2) = Hour Then
                        strVal = frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,TIFA,ORG3")
                        tabList.InsertTextLine strVal, False
                    End If
                End If
            Else
                strTif = frmData.tabData(0).GetFieldValue(i, "TIFD")
                strV2 = Mid(strTif, 9, 2)
                strTifDate = Mid(strTif, 1, 8)
                strCurrDate = Format(frmHourlyDistribution.omCurrentDate, "YYYYMMDD")
                If strCurrDate = strTifDate Then
                    If CInt(strV2) = Hour Then
                        strVal = frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,TIFD,DES3")
                        tabList.InsertTextLine strVal, False
                    End If
                End If
            End If
        End If
    Next i
    tabList.Sort "1", True, True
    If omListMode = "HOURLYFLIGHTS" Then
        If strAdid = "A" Then
            Me.Caption = "Arr./Hour: " & myHour & " [" & tabList.GetLineCount & "]"
        End If
        If strAdid = "D" Then
            Me.Caption = "Dep./Hour: " & myHour & " [" & tabList.GetLineCount & "]"
        End If
    Else
        Me.Caption = ""
    End If
    For i = 0 To tabList.GetLineCount - 1
        If i Mod 2 <> 0 Then
            tabList.SetLineColor i, vbBlack, colLightGray
        End If
    Next i
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_SimpleList", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_SimpleList", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_SimpleList", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_SimpleList", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
End Sub

'Private Sub Form_Resize()
'    tabList.top = 30
'    tabList.Width = Me.ScaleWidth - 30
'    tabList.Height = Me.ScaleHeight - 30
'End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmHourlyDistribution.SimpleListShown = False
    frmMain.RefreshAllControls
End Sub

Private Function GetBookingClassStringBKK(strAftUrno As String, strVIAL As String, strDestOrVIA As String) As String
    Dim strRet As String
    Dim strResult As String
    Dim strDIFF As String
    Dim strDIFC As String
    Dim strDIFU As String
    Dim strDIFY As String
    
    strResult = frmData.tabData(12).GetLinesByIndexValue("RURN", strAftUrno, 0)
    'llLineAVL = frmData.tabData(12).GetLinesByColumnValue("RURN", strAftUrno, 0)
    
    If strResult <> "" Then
        If strVIAL = "" Then
        strDIFF = frmData.tabData(12).GetFieldValue(strResult, "DIFF")
        strDIFC = frmData.tabData(12).GetFieldValue(strResult, "DIFC")
        strDIFU = frmData.tabData(12).GetFieldValue(strResult, "DIFU")
        strDIFY = frmData.tabData(12).GetFieldValue(strResult, "DIFY")
        Else
            If InStr(strVIAL, strDestOrVIA) = 0 Then
                strDIFF = "0"
                strDIFC = "0"
                strDIFU = "0"
                strDIFY = "0"
            Else
                strDIFF = frmData.tabData(12).GetFieldValue(strResult, "DIFF")
                strDIFC = frmData.tabData(12).GetFieldValue(strResult, "DIFC")
                strDIFU = frmData.tabData(12).GetFieldValue(strResult, "DIFU")
                strDIFY = frmData.tabData(12).GetFieldValue(strResult, "DIFY")
            End If
        End If
    End If
    
    strRet = strDIFF + "," + strDIFC + "," + strDIFU + "," + strDIFY
    GetBookingClassStringBKK = strRet
    strResult = ""
End Function


Private Function GetBookingClassString(strAftUrno As String) As String
    Dim strRet As String
    Dim llLine As Long
    Dim strResult As String
    Dim strSTYP As String
    Dim strVALU As String
    Dim bookedFClass As Long
    Dim bookedBClass As Long
    Dim bookedEClass As Long
    Dim bookedUClass As Long
    Dim seatsFClass As Long
    Dim seatsBClass As Long
    Dim seatsEClass As Long
    Dim strRegn As String
    Dim strAct3 As String
    
    bookedFClass = 0
    bookedBClass = 0
    bookedEClass = 0
    bookedUClass = 0
    seatsFClass = 0
    seatsBClass = 0
    seatsEClass = 0
    
    strResult = frmData.tabData(3).GetLinesByIndexValue("FLNU", strAftUrno, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine > -1
        strSTYP = frmData.tabData(3).GetFieldValue(llLine, "STYP")
        strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU")
        Select Case (strSTYP)
            Case "E"
                bookedEClass = Val(strVALU)
            Case "F"
                bookedFClass = Val(strVALU)
            Case "B"
                bookedBClass = Val(strVALU)
            'Customize for GOCC 25/02/2008
            Case "U"
                bookedUClass = Val(strVALU)
        End Select
        llLine = frmData.tabData(3).GetNextResultLine
    Wend
    
    strResult = frmData.tabData(0).GetLinesByIndexValue("URNO", strAftUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strRegn = frmData.tabData(0).GetFieldValue(llLine, "REGN")
        strAct3 = frmData.tabData(0).GetFieldValue(llLine, "ACT3")
        If strRegn <> "" Then
            strResult = frmData.tabData(9).GetLinesByIndexValue("REGN", strRegn, 0)
            llLine = frmData.tabData(9).GetNextResultLine
            If llLine > -1 Then
                seatsFClass = Val(frmData.tabData(9).GetFieldValue(llLine, "SEAF"))
                seatsBClass = Val(frmData.tabData(9).GetFieldValue(llLine, "SEAB"))
                seatsEClass = Val(frmData.tabData(9).GetFieldValue(llLine, "SEAE"))
            End If
        Else
            'Try the ACT3
            If strAct3 <> "" Then
                strResult = frmData.tabData(8).GetLinesByIndexValue("ACT3", strAct3, 0)
                llLine = frmData.tabData(8).GetNextResultLine
                If llLine > -1 Then
                    seatsFClass = Val(frmData.tabData(8).GetFieldValue(llLine, "SEAF"))
                    seatsBClass = Val(frmData.tabData(8).GetFieldValue(llLine, "SEAB"))
                    seatsEClass = Val(frmData.tabData(8).GetFieldValue(llLine, "SEAE"))
                End If
            End If
        End If
    End If
    'OK nows it's time to build the return string
    strRet = CStr(seatsFClass - bookedFClass) + ","
    strRet = strRet + CStr(seatsBClass - bookedBClass) + ","
    strRet = strRet + CStr(seatsEClass - bookedEClass)
    
    GetBookingClassString = strRet
End Function
