﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;
using Ufis.Data;

namespace Data_Changes.Ctrl
{
    public class CtrlSYSTabData
    {
        class LogInfoRelation
        {
            internal string _logTableName = "";
            internal List<string> _lsLogFields = new List<string>();
            internal MapRelation _mapRelation = new MapRelation();
            internal MapRelation _mapRelationType = new MapRelation();

            internal void AddRelation(string fieldName, string fieldDesc,string fieldType)
            {
                if (string.IsNullOrEmpty(fieldName)) return;
                fieldName = fieldName.Trim();
                if (fieldName == "") return;

                if (CtrlLog.IsFieldToExcludeForLog(fieldName)) return;

                if (!_mapRelation.ContainsKey(fieldName))
                {
                    _mapRelation.Add(fieldName, fieldDesc);
                    _lsLogFields.Add(fieldName);
                }

                if (!_mapRelationType.ContainsKey(fieldName))
                {
                    _mapRelationType.Add(fieldName, fieldType);
                }
            }
            

            internal string GetFieldDesc(string fieldName)
            {
                string desc = "";
                if (_mapRelation.ContainsKey(fieldName))
                {
                    desc = _mapRelation[fieldName];
                }
                return desc;
            }

            internal string GetFieldType(string fieldName)
            {
                string desc = "";
                if (_mapRelationType.ContainsKey(fieldName))
                {
                    desc = _mapRelationType[fieldName];
                }
                return desc;
            }
        }

        public const string SYSTAB_DB_FIELDS = "URNO,FINA,TANA,LOGD,ADDI,TYPE";
        public const string SYSTAB_FIELD_LENGTHS = "10,4,3,6,50,4";
        public const string SYSTAB_LOGICAL_FIELDS = "URNO,FINA,TANA,LOGD,ADDI,TYPE";
        public const string SYSTAB_DB_TANA = "SYS";

        private static CtrlSYSTabData _this = null;
        private ITable _sysTab = null;
        private Dictionary<string, LogInfoRelation> _mapLoadedSysTabInfo = new Dictionary<string, LogInfoRelation>();

        private const string LOGICAL_SYS_TAB_FOR_LOG = "LOGICAL_SYS_TAB_FOR_LOG";

        public static CtrlSYSTabData GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlSYSTabData();
            }
            return _this;
        }

        private ITable SYSTab
        {
            get
            {
                if (_sysTab == null)
                {
                    _sysTab = CtrlData.GetInstance().MyDB.Bind(LOGICAL_SYS_TAB_FOR_LOG, SYSTAB_DB_TANA,
                        SYSTAB_LOGICAL_FIELDS,
                        SYSTAB_FIELD_LENGTHS,
                        SYSTAB_DB_FIELDS);
                    _sysTab.Clear();
                    _sysTab.CreateIndex("TANA", "TANA");
                }
                return _sysTab;
            }
        }



        public string GetFieldDesc(string dataTableName, string dataFieldName)
        {
            string fieldDesc = "";
            if (!_mapLoadedSysTabInfo.ContainsKey(dataTableName))
            {
                List<string> lsLogFields = null;
                string logTableName;
                GetLogInfoFor(dataTableName, out logTableName, out lsLogFields);
            }

            if (_mapLoadedSysTabInfo.ContainsKey(dataTableName))
            {
                try
                {
                    fieldDesc = _mapLoadedSysTabInfo[dataTableName].GetFieldDesc(dataFieldName);
                }
                catch (Exception) { }
            }

            return fieldDesc;
        }

        public string GetFieldType(string dataTableName, string dataFieldName)
        {
            string fieldDesc = "";
            if (!_mapLoadedSysTabInfo.ContainsKey(dataTableName))
            {
                List<string> lsLogFields = null;
                string logTableName;
                GetLogInfoFor(dataTableName, out logTableName, out lsLogFields);
            }

            if (_mapLoadedSysTabInfo.ContainsKey(dataTableName))
            {
                try
                {
                    fieldDesc = _mapLoadedSysTabInfo[dataTableName].GetFieldType(dataFieldName);
                }
                catch (Exception) { }
            }

            return fieldDesc;
        }

        public bool GetLogInfoFor(string dataTableName, out string logTableName, out List<string> lsLogFields)
        {
            bool hasData = false;
            logTableName = "";
            lsLogFields = new List<string>();

            LogInfoRelation mapRelation = null;

            IRow[] rows = null;
            if (!_mapLoadedSysTabInfo.ContainsKey(dataTableName))
            {
                //SYSTab.Load("WHERE TANA='" + dataTableName + "' AND LOGD != ' '");
                SYSTab.Load("WHERE TANA='" + dataTableName + "'");
                _sysTab.CreateIndex("TANA", "TANA");
                mapRelation = new LogInfoRelation();
                _mapLoadedSysTabInfo.Add(dataTableName, mapRelation);
                //SYSTab.ReorganizeIndexes();

                rows = SYSTab.RowsByIndexValue("TANA", dataTableName);
                if ((rows != null) && rows.Length > 0)
                {
                    logTableName = rows[0]["LOGD"];
                    if (logTableName.Length > 3)
                    {
                        if (logTableName.EndsWith("TAB"))
                        {
                            logTableName = logTableName.Substring(0, logTableName.Length - 3);
                        }
                    }
                    mapRelation._logTableName = logTableName;

                    foreach (IRow row in rows)
                    {
                        string fieldName = row["FINA"];
                        //    if (string.IsNullOrEmpty(fieldName)) continue;
                        //    fieldName = fieldName.Trim();
                        //    if (fieldName == "") continue;

                        //    if (CtrlLogTab.IsFieldToExcludeForLog(fieldName)) continue;

                        //    if (!mapRelation.ContainsKey(fieldName))
                        //    {
                        //        mapRelation.Add(fieldName, row["ADID"]);
                        //        lsLogFields.Add(fieldName);
                        //    }
                        mapRelation.AddRelation(fieldName, row["ADDI"], row["TYPE"]);
                    }

                }
            }
            else
            {
                mapRelation = _mapLoadedSysTabInfo[dataTableName];
                //foreach (string st in mapRelation.Keys)
                //{
                //    lsLogFields.Add(st);
                //}
            }

            if (mapRelation != null)
            {
                logTableName = mapRelation._logTableName;
                lsLogFields = mapRelation._lsLogFields;
                hasData = true;
            }



            return hasData;
        }

        
        }
    
}
