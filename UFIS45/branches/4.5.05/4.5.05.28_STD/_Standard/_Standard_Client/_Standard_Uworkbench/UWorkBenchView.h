// UWorkBenchView.h : interface of the CUWorkBenchView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_UWORKBENCHVIEW_H__8EF7589E_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_UWORKBENCHVIEW_H__8EF7589E_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UWorkBenchDoc.h"

class CUWorkBenchView : public CScrollView
{
protected: // create from serialization only
	CUWorkBenchView();
	DECLARE_DYNCREATE(CUWorkBenchView)

// Attributes
public:
	CUWorkBenchDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUWorkBenchView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUWorkBenchView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CUWorkBenchView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in UWorkBenchView.cpp
inline CUWorkBenchDoc* CUWorkBenchView::GetDocument()
   { return (CUWorkBenchDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UWORKBENCHVIEW_H__8EF7589E_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
