// GridView.h : interface of the CQueryDesignerGridView class
//

#ifndef __CQueryDesignerGridView__
#define __CQueryDesignerGridView__
/////////////////////////////////////////////////////////////////////////////
#include "UQueryDesingerDoc.h"

//extern const int gxnFirstCtrl = 101;

class CQueryDesignerGridView : public CGXGridView
{
protected: // create from serialization only
	CQueryDesignerGridView();
	DECLARE_DYNCREATE(CQueryDesignerGridView)

// Attributes
public:
	UQueryDesingerDoc* GetDocument();

	CGXStyle *pomLineStyle1,
			 *pomLineStyle2,
			 *pomLineStyle3,
			 *pomLineStyle4,
			 *pomLineStyle5,
			 *pomLineStyle6,
			 *pomLineStyle7,
			 *pomLineStyle8,
			 *pomLineStyle9,
			 *pomLineStyle10;
	CString omQueryName;
	bool bmInit;
	bool bmIsInInitialUpdate;
// Operations
public:
//added for initialization 
	void SetupUserAttributes();
#if _MFC_VER >= 0x0400
	CGXGridDropTarget m_objDndDropTarget;
#endif


	void SaveQuery();
	void SetName(CString opName){omQueryName = opName;}
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CQueryDesignerGridView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint );
	void OnModifyCell(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, 
								  const CGXStyle* pStyle, 
								  GXModifyType mt = gxOverride, int nType = 0);
	virtual BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	virtual void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	virtual void UpdateAllViews(CWnd* pSender, LPARAM lHint, CObject* pHint);


	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CQueryDesignerGridView();
//#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
//#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CQueryDesignerGridView)
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//#ifndef _DEBUG  // debug version in GridView.cpp
//inline CGridDoc* CQueryDesignerGridView::GetDocument()
//   { return (CGridDoc*)m_pDocument; }
//#endif

/////////////////////////////////////////////////////////////////////////////
#endif //__CQueryDesignerGridView__