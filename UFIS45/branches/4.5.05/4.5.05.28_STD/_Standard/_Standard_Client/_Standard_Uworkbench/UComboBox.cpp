// UComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "UComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UComboBox

UComboBox::UComboBox(CWnd *popParent, COLORREF opBkColor, COLORREF opTextColor)
{
	pomParent = popParent;
	omBKColor = opBkColor;
	omTextColor = opTextColor;
}

UComboBox::~UComboBox()
{
}


BEGIN_MESSAGE_MAP(UComboBox, CComboBox)
	//{{AFX_MSG_MAP(UComboBox)
	ON_WM_KILLFOCUS()
	ON_WM_CTLCOLOR_REFLECT()
	ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelchange)
	ON_CONTROL_REFLECT(CBN_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UComboBox message handlers

void UComboBox::OnKillFocus(CWnd* pNewWnd) 
{
	CComboBox::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
//	pomParent->SendMessage(WM_UCOMBOBOX_KILLFOCUS, (UINT)this, 0L);
	
}

HBRUSH UComboBox::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omBKColor);
	hbr = omBrush;	

	pDC->SetBkColor(omBKColor);
	pDC->SetTextColor(omTextColor);

	return hbr;
}

void UComboBox::OnSelchange() 
{
	pomParent->SendMessage(WM_UCOMBOBOX_SELCHANGE, (UINT)this, 0L);
	
}
void UComboBox::SetBkColor(COLORREF opColor)
{
	omBKColor = opColor;
}

void UComboBox::SetTextColor(COLORREF opColor)
{
	omTextColor = opColor;
}


void UComboBox::OnKillfocus() 
{
	pomParent->SendMessage(WM_UCOMBOBOX_KILLFOCUS, (UINT)this, 0L);
}	
