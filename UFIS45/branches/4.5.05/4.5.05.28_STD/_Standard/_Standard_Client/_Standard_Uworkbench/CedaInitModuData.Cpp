// CedaInitModuData.cpp
 
#include "stdafx.h"
#include "CedaInitModuData.h"
const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = ogPseudoAppName + ",InitModu,InitModu,Initialize (InitModu),B,-";
			//olInitModuData += "Anzeige Button,m_ANSICHT,Ansicht,B,1";

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	//olInitModuData += "Desktop,DESKTOP_CB_Warteraeume,Warteräume,B,1,";
	olInitModuData += GetInitModuTxtG();

	
	//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	/*
	FILE *prlFp = fopen("c:\\Personal\\Test.txt","w");
	if(prlFp!=NULL)
	{
		fprintf(prlFp,"%s",pclInitModuData);
		fclose(prlFp);
	}
	*/
	delete pclInitModuData;

	return ilRc;
}


//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olTmpStr="";
	for(int i = 0; i < ogGridDefs.GetSize(); i++)
	{
		CString olTmp;
		//olTmpStr += ",Diposition details gantt,DDG_ONDROP," + LoadStg(IDS_STRING1561) + ",D,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_OPEN," +    LoadStg(TABLE_SHOW) + ",N,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_INSERT," +  LoadStg(RECORD_INSERT) + ",Z,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_UPDATE," +  LoadStg(RECORD_UPDATE) + ",Z,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_DELETE," +  LoadStg(RECORD_DELETE) + ",Z,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_COPY," +    LoadStg(RECORD_COPY) + ",Z,1";
		olTmp += "," + ogGridDefs[i].GridName + "," + ogGridDefs[i].GridName + "_VIEWDEF," + LoadStg(TABLE_VIEWDEF) + ",Z,1";
		olTmpStr += olTmp;
	}

	return olTmpStr;

/*	for(int ilIndex =0;ilIndex<10;ilIndex++)
	{
		switch(ilIndex)
		{
		case 0:
			//Diposition details gantt
			//olTmpStr += ",Diposition details gantt,DDG_ONDROP," + LoadStg(IDS_STRING1561) + ",D,1";
			break;
			
		case 1:
			//DSR_DLG
			//olTmpStr += ",Duty shift roster dialog,DSR_DLG_CDAT," + LoadStg(IDS_STRING646) + ",E,1";
			break;
		case 4:
			break;
		default:
			break;
		}
	}
*/
}
