#ifndef __RUNTIME_CTRL__
#define __RUNTIME_CTRL__
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Encapsulates the the Control and related issues
 *		like Sinks and so on
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo AAT/IR		Initial version
 *		01/11/00	cla AAT/IR		Sink class support (CCmdTarget) added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "CCSGlobl.h"

class RunTimeCtrl : public CObject
{
public:
	RunTimeCtrl():CObject()
	{
		pomControl	  = NULL;
		pomProperties = NULL;
		pomFieldInfo  = NULL;
		pomSink = NULL;
	}
	~RunTimeCtrl()
	{
		omCurrentRecords.DeleteAll();
		omNewRecords.DeleteAll();
		omDeletedRecords.DeleteAll();
		omChangedRecords.DeleteAll();
	}
	RunTimeCtrl(const RunTimeCtrl &opO)
	{
		omType = opO.omType;		
		pomControl = opO.pomControl;
		omCurrentRecords.DeleteAll();
		omNewRecords.DeleteAll();	
		omDeletedRecords.DeleteAll();
		omChangedRecords.DeleteAll();
		for( int i = 0; i < opO.omNewRecords.GetSize(); i++ )
			omNewRecords.NewAt(omNewRecords.GetSize(), opO.omNewRecords[i]);
		for( i = 0; i < opO.omDeletedRecords.GetSize(); i++)
			omDeletedRecords.NewAt(omDeletedRecords.GetSize(), opO.omDeletedRecords[i]);
		for( i = 0; i < opO.omChangedRecords.GetSize(); i++ )
			omChangedRecords.NewAt(omChangedRecords.GetSize(), opO.omChangedRecords[i]);
		pomProperties = opO.pomProperties;
		pomFieldInfo  = opO.pomFieldInfo;
		pomSink = opO.pomSink;
	}
	const RunTimeCtrl &operator =(const RunTimeCtrl &opO) 
	{
		omType = opO.omType;		
		pomControl = opO.pomControl;
		omCurrentRecords.DeleteAll();
		omNewRecords.DeleteAll();	
		omDeletedRecords.DeleteAll();
		omChangedRecords.DeleteAll();
		for( int i = 0; i < opO.omNewRecords.GetSize(); i++ )
			omNewRecords.NewAt(omNewRecords.GetSize(), opO.omNewRecords[i]);
		for( i = 0; i < opO.omDeletedRecords.GetSize(); i++)
			omDeletedRecords.NewAt(omDeletedRecords.GetSize(), opO.omDeletedRecords[i]);
		for( i = 0; i < opO.omChangedRecords.GetSize(); i++ )
			omChangedRecords.NewAt(omChangedRecords.GetSize(), opO.omChangedRecords[i]);
		pomProperties = opO.pomProperties;
		pomFieldInfo  = opO.pomFieldInfo;
		pomSink = opO.pomSink;
		return *this;
	}


	CString omType;		// "button", "edit", "checkbox", "combobox", "static", 
						// "line", "rectangle", "radio", "group", "listbox"
	CWnd	*pomControl;
	CCmdTarget *pomSink; // Pointer to the sink class
	CCSPtrArray<RecordSet> omCurrentRecords;	//only used if the control is a grid
	CCSPtrArray<RecordSet> omNewRecords;		//only used if the control is a grid
	CCSPtrArray<RecordSet> omDeletedRecords;	//only used if the control is a grid
	CCSPtrArray<RecordSet> omChangedRecords;	//only used if the control is a grid

	UCtrlProperties *pomProperties;
	UFormField *pomFieldInfo;
};


#endif //__RUNTIME_CTRL__