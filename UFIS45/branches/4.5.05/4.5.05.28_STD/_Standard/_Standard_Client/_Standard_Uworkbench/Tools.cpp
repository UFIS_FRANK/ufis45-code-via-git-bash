#include <stdafx.h>
#include "CCSGlobl.h"
#include "Tools.h"
#include "UWorkBench.h"
#include "MainFrm.h"
//CMapStringToPtr		omFieldMap; // List of field in format: TAB.FILD


Tools::~Tools()
{
	omCurrentTables.DeleteAll();
}

bool Tools::FieldExists(CString opTabField)
{
	void *p;
	if(omFieldMap.Lookup(opTabField,(void *&)p) == TRUE)
	{
		return true;
	}
	return false;
}


bool Tools::IsTableReaded(CString opTable)
{
	void *p;
	if(omReadedTables.Lookup(opTable,(void *&)p) == TRUE)
	{
		return true;
	}
	return false;
}

void Tools::AddFieldToDefaultSet(BDLoadTable *popTab, CString opTabField)
{
	CString olTabName;
	olTabName = GetTableName(opTabField);
	BDLoadTable *polTab = NULL;
	CString olField = GetFieldName(opTabField);
	UDBLoadSet *polLoadSet = GetDefaultLoadSet();
	if(popTab == NULL)
	{ 
		polTab = GetLoadTabObjFromDefaultSet(opTabField);
		if( polTab == NULL)
		{
			if(polLoadSet != NULL)
			{
				polTab = new BDLoadTable();
				polLoadSet->TableArray.Add(polTab);
				polTab->Name = olTabName;
				if(!olField.IsEmpty())
				{
					polTab->FieldList.Add(olField);
				}
				omTabMap.SetAt(olTabName, (void *&)polTab);
				void *p = NULL;
				omFieldMap.SetAt(opTabField, (void *)p);
			}
		}
		else
		{
			bool blFound = false;
			for(int i = 0; i < polTab->FieldList.GetSize(); i++)
			{
				if(olField == polTab->FieldList[i])
				{
					blFound = true;
					break;
				}
			}
			if(blFound == false)
			{
				polTab->FieldList.Add(olField);
			}
			if(FieldExists(opTabField) == false)
			{
				//omTabMap.SetAt(olTabName, (void *&)popTab);
				void *p = NULL;
				omFieldMap.SetAt(opTabField, (void *)p);
			}
		}
	}
	else
	{
		bool blFound = false;
		for(int i = 0; i < popTab->FieldList.GetSize(); i++)
		{
			if(olField == popTab->FieldList[i])
			{
				blFound = true;
				break;
			}
		}
		if(blFound == false)
		{
			popTab->FieldList.Add(olField);
		}
		if(FieldExists(opTabField) == false)
		{
			omTabMap.SetAt(olTabName, (void *&)popTab);
			void *p = NULL;
			omFieldMap.SetAt(opTabField, (void *)p);
		}

	}
}

void Tools::RemoveFieldFromDefaultSet(CString opTabField)
{
	CString olTabName, olField;
	olTabName = GetTableName(opTabField);
	olField   = GetFieldName(opTabField);
	if(FieldExists(opTabField) == true)
	{
		omFieldMap.RemoveKey(opTabField);
		if(!olTabName.IsEmpty())
		{
			POSITION olPos;
			bool blFound = false;
			for( olPos = omFieldMap.GetStartPosition(); olPos != NULL; )
			{
				CString olKey;
				void *p = NULL;
				omFieldMap.GetNextAssoc(olPos, olKey , (void *&)p );
				if(olKey.Find(olTabName) != -1)
				{
					blFound = true;
				}
			}
			if(blFound == false)
			{
				omTabMap.RemoveKey(olTabName);
			}
		}
	}
	BDLoadTable *polTab = GetLoadTabObjFromDefaultSet(opTabField);
	if(polTab != NULL)
	{
		bool blFound = false;
		int ilDeleteIdx = -1;
		for(int i = 0; i < polTab->FieldList.GetSize(); i++)
		{
			if(olField == polTab->FieldList[i])
			{
				blFound = true;
				ilDeleteIdx = i;
				break;
			}
		}
		if((blFound == true) && (ilDeleteIdx > -1))
		{
			polTab->FieldList.RemoveAt(ilDeleteIdx);
		}
	}
}

BDLoadTable * Tools::GetLoadTabObjFromDefaultSet(CString opTabField)
{
	CString olTab = opTabField;
	if(opTabField.GetLength() > 3)
	{
		olTab = GetTableName(opTabField);
	}
	BDLoadTable *polTab;
	if(omTabMap.Lookup(olTab,(void *&)polTab) == TRUE)
	{
		return polTab;
	}
	return NULL;
}

void Tools::InitDefaultLoadSetInfo()
{
	omTabMap.RemoveAll();
	omFieldMap.RemoveAll();
	for(int i = 0; i < ogLoadSets.GetSize(); i++)
	{
		if(ogLoadSets[i].Name == "Default System Set")
		{
			for(int j = 0; j < ogLoadSets[i].TableArray.GetSize(); j++)
			{
				omTabMap.SetAt(ogLoadSets[i].TableArray[j].Name, (void *)&ogLoadSets[i].TableArray[j]);
				for(int k = 0; k < ogLoadSets[i].TableArray[j].FieldList.GetSize(); k++)
				{
					void *p = NULL;
					CString olTabField = ogLoadSets[i].TableArray[j].Name + CString(".") + ogLoadSets[i].TableArray[j].FieldList[k];
					omFieldMap.SetAt(olTabField, (void *&)p);
				}
			}
		}
	}
}

CString Tools::GetTableName(CString opObject)
{
	CString olRet;
	if(opObject.GetLength() != 8)
	{
		return CString();
	}
	int ilIdx = opObject.Find('.');
	if(ilIdx != -1)
	{
		olRet = opObject.Left(3);
	}
	return olRet;
}

CString Tools::GetFieldName(CString opObject)
{
	CString olRet;
	if(opObject.GetLength() != 8)
	{
		return CString();
	}
	int ilIdx = opObject.Find('.');
	if(ilIdx != -1)
	{
		olRet = opObject.Right(4);
	}
	return olRet;
}

UDBLoadSet *Tools::GetDefaultLoadSet()
{
	for(int i = 0; i < ogLoadSets.GetSize(); i++)
	{
		if(ogLoadSets[i].Name == "Default System Set")
		{
			return &ogLoadSets[i];
		}
	}
	return NULL;
}

void Tools::ReloadDataForTable(CString opTable, CListBox *popLB /*= NULL*/, CString opWhere /*= CString("")*/)
{
	UDBLoadSet *polLoadSet = GetDefaultLoadSet();
	if(polLoadSet != NULL)
	{
		for(int i = 0; i < polLoadSet->TableArray.GetSize(); i++)
		{
			if(polLoadSet->TableArray[i].Name == opTable)
			{
				CString olTable = polLoadSet->TableArray[i].Name;
				CString olFields;
				CString olWhere;
				if(opWhere != CString(""))
				{
					olWhere = opWhere;
				}
				else
				{
					if(!polLoadSet->TableArray[i].SqlPart.IsEmpty())
					{
						CString olTmpWhere = polLoadSet->TableArray[i].SqlPart;
						SubstituteWhere(olTmpWhere);
						if(!olTmpWhere.IsEmpty())
						{
							olWhere = CString("WHERE ") + olTmpWhere;
						}
					}
				}
				for(int j = 0; j < polLoadSet->TableArray[i].FieldList.GetSize(); j++)
				{
					if((j+1) == polLoadSet->TableArray[i].FieldList.GetSize())
					{
						olFields += polLoadSet->TableArray[i].FieldList[j];
					}
					else
					{
						olFields += polLoadSet->TableArray[i].FieldList[j] + CString(",");
					} 
				}
				ogBCD.RemoveObject(olTable);
				ogBCD.SetObject(olTable, olFields);
				ogBCD.SetObjectDesc(olTable, olTable);
				if(olWhere.IsEmpty())
				{
  					ogBCD.Read(olTable);
				}
				else
				{
					ogBCD.Read(olTable, olWhere);
				}
				TRACE("%s: %d Records loaded\n", olTable, ogBCD.GetDataCount(olTable));
				if(popLB != NULL)
				{
					CString olText;
					olText.Format("%s: %d Records loaded", olTable, ogBCD.GetDataCount(olTable));
					popLB->AddString(olText);
					if(popLB->GetCount()>12)
					{
						popLB->SetTopIndex(popLB->GetCount()-12);
					}
					popLB->UpdateWindow();
				}
				void *p = NULL;
				omReadedTables.SetAt(olTable, (void *)p);
			}
		}
	}
}

void Tools::LoadDataWithDefaultSet(CListBox *popLB)
{
	UDBLoadSet *polLoadSet = GetDefaultLoadSet();
	if(polLoadSet != NULL)
	{
		for(int i = 0; i < polLoadSet->TableArray.GetSize(); i++)
		{
			CString olTable = polLoadSet->TableArray[i].Name;
			CString olFields;
			CString olWhere;
			if(!polLoadSet->TableArray[i].SqlPart.IsEmpty())
			{
				CString olTmpWhere = polLoadSet->TableArray[i].SqlPart;
				SubstituteWhere(olTmpWhere);
				if(!olTmpWhere.IsEmpty())
				{
					olWhere = CString("WHERE ") + olTmpWhere;
				}
			}
			for(int j = 0; j < polLoadSet->TableArray[i].FieldList.GetSize(); j++)
			{
				if((j+1) == polLoadSet->TableArray[i].FieldList.GetSize())
				{
					olFields += polLoadSet->TableArray[i].FieldList[j];
				}
				else
				{
					olFields += polLoadSet->TableArray[i].FieldList[j] + CString(",");
				} 
			}
			ogBCD.RemoveObject(olTable);
			ogBCD.SetObject(olTable, olFields);
			ogBCD.SetObjectDesc(olTable, olTable);
			if(olWhere.IsEmpty())
			{
  				ogBCD.Read(olTable);
			}
			else
			{
				ogBCD.Read(olTable, olWhere);
			}
			TRACE("%s: %d Records loaded\n", olTable, ogBCD.GetDataCount(olTable));
			if(popLB != NULL)
			{
				CString olText;
				olText.Format("%s: %d Records loaded", olTable, ogBCD.GetDataCount(olTable));
				popLB->AddString(olText);
				if(popLB->GetCount()>12)
				{
					popLB->SetTopIndex(popLB->GetCount()-12);
				}
				popLB->UpdateWindow();
			}
			void *p = NULL;
			omReadedTables.SetAt(olTable, (void *)p);
		}
	}
}


void Tools::SubstituteWhere(CString &opWhere)
{
	int ilIdx1;
	CString olPart1, olPart2, olNewValue;
	while ((ilIdx1 = opWhere.Find("[TIMETO]")) != -1)
	{
		olPart1 = opWhere.Left(ilIdx1-1);
		olNewValue = CString("'") + ogTimeFrameTo.Format("%Y%m%d%H%M00") + CString("' ");
		olPart2 = opWhere.Right(opWhere.GetLength() - (ilIdx1+8));
		opWhere = olPart1 + olNewValue + olPart2;
	}
	olPart1 = CString("");
	olPart2 = CString("");
	olNewValue = CString("");
	while ((ilIdx1 = opWhere.Find("[TIMEFROM]")) != -1)
	{
		olPart1 = opWhere.Left(ilIdx1-1);
		olNewValue = CString("'") + ogTimeFrameFrom.Format("%Y%m%d%H%M00") + CString("' ");
		olPart2 = opWhere.Right(opWhere.GetLength() - (ilIdx1+10));
		opWhere = olPart1 + olNewValue + olPart2;
	}
}

//--------------------------------------------------------------------------------------
//1. Check for the Grid Maintable
//2. Check for the Form and all childs of popGridDef->FormName
// return Parameter ==> all used tables concerning the grid/form-combination
//--------------------------------------------------------------------------------------
int Tools::GetTablesForGridAndForm(UGridDefinition *popGridDef, CStringArray &ropTables)
{
	int Count = 0;
	return Count;
}

//--------------------------------------------------------------------------------------
// Sets all needed tables for grid/form combination
//--------------------------------------------------------------------------------------
void Tools::SetTablesForGridAndForm(UGridDefinition *popGridDef, bool blSetOrRemove /* = true => SET false = REMOVE*/)
{
	if(popGridDef->AsBasicData == TRUE)
	{
		if(blSetOrRemove == true)
			SetUsedTable(popGridDef->omTables[0], popGridDef->Where);
		else
			RemoveUsedTable(popGridDef->omTables[0]);

		if(popGridDef->FormName != CString(""))
		{
			UFormProperties *polFormDef = GetFormDefByName(popGridDef->FormName);
			if(polFormDef != NULL)
			{
				SetTablesForForm(polFormDef, blSetOrRemove);
			}
		}
	}
	else
	{
		for(int i = 0; i < popGridDef->omTables.GetSize(); i++)
		{
			if(blSetOrRemove == true)
				SetUsedTable(popGridDef->omTables[i]);
			else
				RemoveUsedTable(popGridDef->omTables[i]);
		}
	}
}


//--------------------------------------------------------------------------------------
// Set a table as used
//--------------------------------------------------------------------------------------
void Tools::SetUsedTable(CString opTable, CString opWhere /*= CString("")*/)
{
	CurrentTableCounter *polCurrTabCounter = NULL;
	polCurrTabCounter = GetTableCounter(opTable);
	if(polCurrTabCounter != NULL)
	{
		polCurrTabCounter->ReferenceCounter++;
	}
	else
	{
		polCurrTabCounter = new CurrentTableCounter;
		polCurrTabCounter->ReferenceCounter = 1;
		polCurrTabCounter->TableName = opTable;
		omCurrentTables.Add(polCurrTabCounter);
		CString olText;
		olText.Format("Loading Table: %s", opTable.GetBuffer(0));
		((CUWorkBenchApp*)AfxGetApp())->pMainFrame->SetMessageText(olText);
		ReloadDataForTable(opTable, NULL, opWhere);
		((CUWorkBenchApp*)AfxGetApp())->pMainFrame->SetMessageText("");
	}
}

//--------------------------------------------------------------------------------------
//removes a table as used
// if this reference is the last ==> the table will be removed
// otherwise the reference-counter will be decremented
//--------------------------------------------------------------------------------------
void Tools::RemoveUsedTable(CString opTable)
{
	CurrentTableCounter *polCurrTabCounter = NULL;
	polCurrTabCounter = GetTableCounter(opTable);
	if(polCurrTabCounter != NULL)
	{
		polCurrTabCounter->ReferenceCounter--;
		if(polCurrTabCounter->ReferenceCounter == 0)
		{
			for(int i = omCurrentTables.GetSize()-1; i >= 0; i--)
			{
				if(omCurrentTables[i].TableName == opTable)
				{
					omCurrentTables.DeleteAt(i);
					ogBCD.RemoveObject(opTable);
				}
			}
		}
	}
	else
	{
		//Should never happen but however ==> do nothing
		;
	}
}

//--------------------------------------------------------------------------------------
// retrives the currnt tabel counter for the parameter opTable
//--------------------------------------------------------------------------------------
CurrentTableCounter* Tools::GetTableCounter(CString opTable)
{
	CurrentTableCounter *polCurrTabCounter = NULL;
	for(int i = 0; i < omCurrentTables.GetSize(); i++)
	{
		if(omCurrentTables[i].TableName == opTable)
		{
			polCurrTabCounter = &omCurrentTables[i];
			return polCurrTabCounter;
		}
	}
	return polCurrTabCounter;
}

//--------------------------------------------------------------------------------------
// returns the FormDef by Name
//--------------------------------------------------------------------------------------
UFormProperties *Tools::GetFormDefByName(CString opName)
{
	UFormProperties *polFormDef = NULL;
	for(int i = 0; i < ogForms.GetSize(); i++)
	{
		if(ogForms[i].omName == opName)
		{
			polFormDef = &ogForms[i];
			return polFormDef;
		}
	}
	return polFormDef;
}

//--------------------------------------------------------------------------------------
// Sets all needed tables for formdefinition
//--------------------------------------------------------------------------------------
void Tools::SetTablesForForm(UFormProperties *popFormDef, bool blSetOrRemove /* = true => SET false = REMOVE*/)
{
	if(popFormDef == NULL)
		return;

	for(int i = 0; i < popFormDef->omProperties.GetSize(); i++)
	{
		if(popFormDef->omProperties[i].omCtrlType == "grid")
		{
			CString olDetailTable, olDetailField;
			UGridCtrlProperties *polGridProp = (UGridCtrlProperties *)&(popFormDef->omProperties[i]);
			if(!polGridProp->omMasterDetail.IsEmpty())
			{
				CStringArray olRelParts; //Relationparts STF.URNO=SPE.SURN ==> Master [0] = STF.URNO Detail [1] = SPE.SURN
				ExtractItemList(polGridProp->omMasterDetail, &olRelParts, '=');
				if(olRelParts.GetSize() == 2)
				{
					olDetailTable = GetTableName(olRelParts[1]);
					olDetailField = GetFieldName(olRelParts[1]);
					if(!olDetailTable.IsEmpty() && !olDetailField.IsEmpty())
					{
						if(blSetOrRemove == true)
							SetUsedTable(olDetailTable);
						else
							RemoveUsedTable(olDetailTable);
						int ilFC = polGridProp->omFieldlist.GetSize();
						for(int j = 0; j < ilFC; j++)
						{
							if(!polGridProp->omLookupfieldList[j].IsEmpty())
							{
								CStringArray olLookups;
								CString olTable;
								CStringArray olFldLookupList;
								ExtractItemList(polGridProp->omLookupfieldList[j], &olLookups, '+');
								for(int k = 0; k < olLookups.GetSize(); k++)
								{
 									olTable = ogTools.GetTableName(olLookups[k]);
									if(blSetOrRemove == true)
										SetUsedTable(olTable);
									else
										RemoveUsedTable(olTable);
								}
							}
						}
					}
				}
			}

		}
		if(popFormDef->omProperties[i].omCtrlType == "combobox")
		{
			UFormField *polFieldInfo = popFormDef->GetFieldInfo(popFormDef->omProperties[i].omDBField, popFormDef->omTable);
			if( polFieldInfo != NULL )
			{
				if(!polFieldInfo->Refe.IsEmpty())
				{
					CString olTable = GetTableName(polFieldInfo->Refe);
					if(blSetOrRemove == true)
						SetUsedTable(olTable);
					else
						RemoveUsedTable(olTable);
				}
			}
		}
	}
}
/*-------------------------------------------------------------------------------------------
Creator: MWO

 Sets all DDX types for specified table (IRT,URT,DRT)
-------------------------------------------------------------------------------------------*/
void Tools::SetDDXTypesForTable(CString opTable)
{
	bool blIrtFound = false;
	bool blUrtFound = false;
	bool blDrtFound = false;
	bool blAllFound = false;
	int i;
	for(i = 0; ((i < ogDDXMapper.GetSize()) && (blAllFound == false)); i++)
	{
		if(ogDDXMapper[i].Table == opTable)
		{
			if(ogDDXMapper[i].Command == "IRT")
			{
				blIrtFound = true;
			}
			if(ogDDXMapper[i].Command == "URT")
			{
				blUrtFound = true;
			}
			if(ogDDXMapper[i].Command == "DRT")
			{
				blDrtFound = true;
			}
			if(blIrtFound == true && blUrtFound == true && blDrtFound == true)
			{
				blAllFound = true;
			}
		}
	}//end for
	if(blAllFound == false)
	{
		if(blIrtFound == false)
		{
			DDXMapper *prlMapper = new DDXMapper;
			prlMapper->Command = "IRT";
			prlMapper->Table   = opTable;
			prlMapper->DDXType = ogCurrentDDXType++;
			ogDDXMapper.Add(prlMapper);
		}
		if(blUrtFound == false)
		{
			DDXMapper *prlMapper = new DDXMapper;
			prlMapper->Command = "URT";
			prlMapper->Table   = opTable;
			prlMapper->DDXType = ogCurrentDDXType++;
			ogDDXMapper.Add(prlMapper);
		}
		if(blDrtFound == false)
		{
			DDXMapper *prlMapper = new DDXMapper;
			prlMapper->Command = "DRT";
			prlMapper->Table   = opTable;
			prlMapper->DDXType = ogCurrentDDXType++;
			ogDDXMapper.Add(prlMapper);
		}
	}//end if(blAllFound == false)
}

/*--------------------------------------------------------------------------------------------
Creator: MWO

 Get  all DDX types for specified table (IRT,URT,DRT)
 This enables a grid to handle broadcasts => but therefore it needs the
 correct DDXTypes for registering at DDX for correct callback for changes
 return params: the referece parameters for all Commands as DDXType
 if one or more DDXTypes for IRT,URT,DRT are missing 
 ==> return false
---------------------------------------------------------------------------------------------*/
bool Tools::GetDDXTypesForTable(CString opTable, int &ripURTType, int &ripDRTType, int &ripIRTType)
{
	bool blRet = false;

	bool blIrtFound = false;
	bool blUrtFound = false;
	bool blDrtFound = false;
	bool blAllFound = false;

	ripURTType = -1;
	ripDRTType = -1;
	ripIRTType = -1;
	
	for(int i = 0; ((i < ogDDXMapper.GetSize()) && (blAllFound == false)); i++)
	{
		if(ogDDXMapper[i].Table == opTable)
		{
			if(ogDDXMapper[i].Command == "IRT")
			{
				ripIRTType = ogDDXMapper[i].DDXType;
				blIrtFound = true;
			}
			if(ogDDXMapper[i].Command == "URT")
			{
				ripURTType = ogDDXMapper[i].DDXType;
				blUrtFound = true;
			}
			if(ogDDXMapper[i].Command == "DRT")
			{
				ripDRTType = ogDDXMapper[i].DDXType;
				blDrtFound = true;
			}
			if(blIrtFound == true && blUrtFound == true && blDrtFound == true)
			{
				blAllFound = true;
			}
		}
	}//end for(int i = 0; ((i < ogDDXMapper.GetSize()) && (blAllFound == false)); i++)
	if(blAllFound == true)
	{
		blRet = true;
	}

	return blRet;
}
