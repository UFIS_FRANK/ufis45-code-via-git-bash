#if !defined(AFX_UDRAGDROPLISTBOX_H__B2E12D14_2DDF_11D3_B07C_00001C019205__INCLUDED_)
#define AFX_UDRAGDROPLISTBOX_H__B2E12D14_2DDF_11D3_B07C_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDragDropListBox.h : header file
//
#include "CCSDragDropCtrl.h"
/////////////////////////////////////////////////////////////////////////////
// UDragDropListBox window

#define DND_LISTBOX 500
#define WM_DND_LBOX_BEGIN_DRAG	(WM_USER + 1000)
#define WM_DND_LBOX_DRAG_OVER	(WM_USER + 1001)
#define WM_DND_LBOX_DROP		(WM_USER + 1002)

class UDragDropListBox : public CListBox
{
// Construction
public:
	UDragDropListBox(CWnd *popParent);

	CWnd *pomParent;
// Attributes
public:

// Operations
public:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	CPoint omDropPosition;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UDragDropListBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UDragDropListBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(UDragDropListBox)
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg LONG OnDrop(UINT wParam, LONG lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDRAGDROPLISTBOX_H__B2E12D14_2DDF_11D3_B07C_00001C019205__INCLUDED_)
