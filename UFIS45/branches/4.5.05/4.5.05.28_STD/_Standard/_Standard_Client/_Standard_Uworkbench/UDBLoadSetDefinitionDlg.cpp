// UDBLoadSetDefinitionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UDBLoadSetDefinitionDlg.h"
#include "Tools.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UDBLoadSetDefinitionDlg dialog


UDBLoadSetDefinitionDlg::UDBLoadSetDefinitionDlg(CWnd* pParent, UDBLoadSet *popLoadSet)
	: CDialog(UDBLoadSetDefinitionDlg::IDD, pParent)
{
	pomLoadSet = popLoadSet;
	//{{AFX_DATA_INIT(UDBLoadSetDefinitionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void UDBLoadSetDefinitionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
 	//{{AFX_DATA_MAP(UDBLoadSetDefinitionDlg)
	DDX_Control(pDX, IDC_RELOAD_LIST, m_ReloadList);
	DDX_Control(pDX, IDC_SQL_FUNCTIONS, m_SqlFunctions);
	DDX_Control(pDX, IDC_INDIRECTLOAD, m_IndirectLoad);
	DDX_Control(pDX, IDC_AFTOBJECT, m_FlightObject);
	DDX_Control(pDX, IDC_TIMEFRAME, m_TimeFrame);
	DDX_Control(pDX, IDC_SQL_PART, m_SQL_Part);
	DDX_Control(pDX, IDC_SET_NAME, m_SetName);
	DDX_Control(pDX, IDC_SOURCE_TABLE_LIST, m_SourceTables);
	DDX_Control(pDX, IDC_SOURCE_FIELD_LIST, m_SorceFields);
	DDX_Control(pDX, IDC_DEST_TABLE_LIST, m_DestTableList);
	DDX_Control(pDX, IDC_DEST_FIELD_LIST, m_DestFields);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UDBLoadSetDefinitionDlg, CDialog)
	//{{AFX_MSG_MAP(UDBLoadSetDefinitionDlg)
	ON_BN_CLICKED(IDC_ADDFIELD, OnAddfield)
	ON_BN_CLICKED(IDC_ADDTABLE, OnAddtable)
	ON_BN_CLICKED(IDC_REMOVEFIELD, OnRemovefield)
	ON_BN_CLICKED(IDC_REMOVETABLE, OnRemovetable)
	ON_LBN_SELCHANGE(IDC_SOURCE_FIELD_LIST, OnSelchangeSourceFieldList)
	ON_LBN_SELCHANGE(IDC_SOURCE_TABLE_LIST, OnSelchangeSourceTableList)
	ON_LBN_SELCHANGE(IDC_DEST_FIELD_LIST, OnSelchangeDestFieldList)
	ON_LBN_SELCHANGE(IDC_DEST_TABLE_LIST, OnSelchangeDestTableList)
	ON_EN_KILLFOCUS(IDC_SQL_PART, OnKillfocusSqlPart)
	ON_BN_CLICKED(IDC_AFTOBJECT, OnAftobject)
	ON_BN_CLICKED(IDC_TIMEFRAME, OnTimeframe)
	ON_CBN_SELCHANGE(IDC_SQL_FUNCTIONS, OnSelchangeSqlFunctions)
	ON_BN_CLICKED(IDC_OR_BTN, OnOrBtn)
	ON_BN_CLICKED(IDC_LESS_BTN, OnLessBtn)
	ON_BN_CLICKED(IDC_GT_BTN, OnGtBtn)
	ON_BN_CLICKED(IDC_EQU_BTN, OnEquBtn)
	ON_BN_CLICKED(IDC_INDIRECTLOAD, OnIndirectload)
	ON_BN_CLICKED(IDC_AND_BTN, OnAndBtn)
	ON_BN_CLICKED(IDC_RELOAD, OnReload)
	ON_BN_CLICKED(IDC_RELOAD_TABLE, OnReloadTable)
	ON_BN_CLICKED(IDC_UPDATE_LOADSET, OnUpdateLoadset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UDBLoadSetDefinitionDlg message handlers

void UDBLoadSetDefinitionDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString olText;
	m_SetName.GetWindowText(olText);
	if(olText.IsEmpty())
	{
		MessageBox("Please enter a name for the set!!", "Info..", MB_OK);
		return;
	}
	pomLoadSet->Name = olText;
	CDialog::OnOK();
}

void UDBLoadSetDefinitionDlg::OnAddfield() 
{
	m_SourceTables.LockWindowUpdate();
	m_SorceFields.LockWindowUpdate();
	m_DestTableList.LockWindowUpdate();
	m_DestFields.LockWindowUpdate();
	BDLoadTable *polLoadTab = NULL;
	int ilSelCount = m_SorceFields.GetSelCount();
	int *ipSels;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_SorceFields.GetSelItems(ilSelCount, ipSels);

	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olField;
		m_SorceFields.GetText(ipSels[ili], olField);
		m_DestFields.AddString(olField);
		
		int ilIdx = m_DestTableList.GetCurSel();
		if(ilIdx != LB_ERR)
		{
			CString olTab;
			m_DestTableList.GetText(ilIdx, olTab);
			polLoadTab = GetCurrentTabLoad(olTab);
			if(polLoadTab != NULL)
			{
				CString olTabField;
				CString olAddField = olField.Left(4);
				polLoadTab->FieldList.Add(olAddField);
				olTabField = olTab + "." + olAddField;
				ogTools.AddFieldToDefaultSet(NULL, olTabField);
			}
		}
		
	}
	if(polLoadTab != NULL)
	{
		MakeFieldDiffList(polLoadTab);
	}
	m_SourceTables.UnlockWindowUpdate();
	m_SorceFields.UnlockWindowUpdate();
	m_DestTableList.UnlockWindowUpdate();
	m_DestFields.UnlockWindowUpdate();
	free(ipSels);
}

void UDBLoadSetDefinitionDlg::OnAddtable() 
{
	m_SourceTables.LockWindowUpdate();
	m_SorceFields.LockWindowUpdate();
	m_DestTableList.LockWindowUpdate();
	m_DestFields.LockWindowUpdate();

	int ilIdx = m_SourceTables.GetCurSel();
	CString olTab;
	m_SourceTables.GetText(ilIdx, olTab);
	int ilNewIdx = m_DestTableList.AddString(olTab);
	m_SourceTables.DeleteString(ilIdx);
	BDLoadTable *polLoadTab = new BDLoadTable();
	polLoadTab->Name = olTab;
	pomLoadSet->TableArray.Add(polLoadTab);
	m_DestTableList.SetCurSel(ilNewIdx);
	m_DestTableList.SetSel(ilNewIdx);
	
	OnSelchangeDestTableList();
	m_SourceTables.UnlockWindowUpdate();
	m_SorceFields.UnlockWindowUpdate();
	m_DestTableList.UnlockWindowUpdate();
	m_DestFields.UnlockWindowUpdate();
}

void UDBLoadSetDefinitionDlg::OnRemovefield() 
{
	m_SourceTables.LockWindowUpdate();
	m_SorceFields.LockWindowUpdate();
	m_DestTableList.LockWindowUpdate();
	m_DestFields.LockWindowUpdate();
	BDLoadTable *polLoadTab = NULL;
	int ilSelCount = m_DestFields.GetSelCount();
	int *ipSels;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_DestFields.GetSelItems(ilSelCount, ipSels);

	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olField;
		m_DestFields.GetText(ipSels[ili], olField);
		m_SorceFields.AddString(olField);
		
		int ilIdx = m_DestTableList.GetCurSel();
		if(ilIdx != LB_ERR)
		{
			CString olTab;
			m_DestTableList.GetText(ilIdx, olTab);
			polLoadTab = GetCurrentTabLoad(olTab);
			if(polLoadTab != NULL)
			{
				CString olRemoveField = olField.Left(4);
				CString olTabField;
				olTabField = olTab + "." + olRemoveField;
				ogTools.RemoveFieldFromDefaultSet(olTabField);
				for(int j = polLoadTab->FieldList.GetSize()-1; j >= 0; j--)
				{
					if(polLoadTab->FieldList[j] == olRemoveField)
					{
						polLoadTab->FieldList.RemoveAt(j);
					}
				}
			}
		}
		
	}
	if(polLoadTab != NULL)
	{
		MakeFieldDiffList(polLoadTab);
	}
	m_SourceTables.UnlockWindowUpdate();
	m_SorceFields.UnlockWindowUpdate();
	m_DestTableList.UnlockWindowUpdate();
	m_DestFields.UnlockWindowUpdate();
	free(ipSels);
}

void UDBLoadSetDefinitionDlg::OnRemovetable() 
{
	int ilIdx = m_DestTableList.GetCurSel();
	m_DestFields.SelItemRange(TRUE, 0, m_DestFields.GetCount());
	OnRemovefield();
	CString olTab;
	m_DestTableList.GetText(ilIdx, olTab);
	int ilNewIdx = m_SourceTables.AddString(olTab);
	m_DestTableList.DeleteString(ilIdx);
	for(int i = pomLoadSet->TableArray.GetSize()-1; i >= 0; i--)
	{
		if(pomLoadSet->TableArray[i].Name == olTab)
		{
			pomLoadSet->TableArray.DeleteAt(i);
		}
	}
	m_SorceFields.ResetContent();
	m_DestFields.ResetContent();
}



void UDBLoadSetDefinitionDlg::OnSelchangeSourceFieldList() 
{
	// TODO: Add your control notification handler code here
	
}

void UDBLoadSetDefinitionDlg::OnSelchangeSourceTableList() 
{
}

void UDBLoadSetDefinitionDlg::OnSelchangeDestFieldList() 
{
	// TODO: Add your control notification handler code here
	
}

void UDBLoadSetDefinitionDlg::OnSelchangeDestTableList() 
{
	int ilIdx = m_DestTableList.GetCurSel();
	CString olTab;
	m_DestTableList.GetText(ilIdx, olTab);
	BDLoadTable *popLoadTab = GetCurrentTabLoad(olTab);
	MakeFieldDiffList(popLoadTab);
	BDLoadTable *polLoad =  GetCurrentTabLoad(olTab);
	if(polLoad != NULL)
	{
		m_SQL_Part.SetWindowText(polLoad->SqlPart);
		if(polLoad->IsAFTObject == true)
			m_FlightObject.SetCheck(1);
		else
			m_FlightObject.SetCheck(0);
		if(polLoad->IsTimeFrameObject == true)
			m_TimeFrame.SetCheck(1);
		else
			m_TimeFrame.SetCheck(0);
		if(polLoad->IsIndirectLoad == true)
			m_IndirectLoad.SetCheck(1);
		else
			m_IndirectLoad.SetCheck(0);
		
	}
}

BOOL UDBLoadSetDefinitionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	POSITION pos;

	m_SourceTables.SetFont(&ogCourier_Regular_8);
	m_SorceFields.SetFont(&ogCourier_Regular_8);
	m_DestTableList.SetFont(&ogCourier_Regular_8);
	m_DestFields.SetFont(&ogCourier_Regular_8);
	m_SetName.SetWindowText(pomLoadSet->Name);
	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		bool blFound = false;
		for(int i = 0; i < pomLoadSet->TableArray.GetSize(); i++)
		{
			if(olKey == pomLoadSet->TableArray[i].Name)
			{
				blFound = true;
				i = pomLoadSet->TableArray.GetSize();
			}
		}
		if(blFound == false)
		{
			m_SourceTables.AddString(olKey);
		}
		else
		{
			m_DestTableList.AddString(olKey);
		}
	}
	if(pomLoadSet->Name == "Default System Set")
	{
		m_SetName.EnableWindow(FALSE);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
}

BDLoadTable *UDBLoadSetDefinitionDlg::GetCurrentTabLoad(CString opTab)
{
	BDLoadTable *polLoadTab = NULL;
	if(pomLoadSet != NULL)
	{
		int ilCount = pomLoadSet->TableArray.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			if(pomLoadSet->TableArray[i].Name == opTab)
			{
				polLoadTab = &pomLoadSet->TableArray[i];
				i = ilCount;
			}
		}
	}
	return polLoadTab;
}

void UDBLoadSetDefinitionDlg::MakeFieldDiffList(BDLoadTable *popLoadTab)
{
	if(popLoadTab != NULL)
	{
		m_SorceFields.ResetContent();
		m_DestFields.ResetContent();
		CCSPtrArray<RecordSet> olRecords;

		ogBCD.GetRecords("SYS", "TANA", popLoadTab->Name, &olRecords);
		for(int i = 0; i < olRecords.GetSize(); i++)
		{
			bool blFound = false;
			for(int j = 0; j < popLoadTab->FieldList.GetSize(); j++)
			{
				if(popLoadTab->FieldList[j] == olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")])
				{
					blFound = true;
				}
			}
			if(blFound == false)
			{
				CString olSrcStr;
				olSrcStr = olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
				olSrcStr += CString(": ") + olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];;
				m_SorceFields.AddString(olSrcStr );
			}
			else
			{
				CString olSrcStr;
				olSrcStr = olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
				olSrcStr += CString(": ") + olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];;
				m_DestFields.AddString(olSrcStr );
			}
		}
		olRecords.DeleteAll();
	}
	else
	{
		m_SorceFields.ResetContent();
		m_DestFields.ResetContent();
		CCSPtrArray<RecordSet> olRecords;

		ogBCD.GetRecords("SYS", "TANA", popLoadTab->Name, &olRecords);
		for(int i = 0; i < olRecords.GetSize(); i++)
		{
			CString olSrcStr;
			olSrcStr = olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
			olSrcStr += CString(": ") + olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];;
			m_SorceFields.AddString(olSrcStr );
		}
		olRecords.DeleteAll();
	}
}

void UDBLoadSetDefinitionDlg::OnKillfocusSqlPart() 
{
	CString olText;

	m_SQL_Part.GetWindowText(olText);

	int ilIdx = m_DestTableList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olTab;
		m_DestTableList.GetText(ilIdx, olTab);

		BDLoadTable *polLoad = GetCurrentTabLoad(olTab);
		if(polLoad != NULL)
		{
			polLoad->SqlPart = olText;
		}
	}
}


void UDBLoadSetDefinitionDlg::OnAftobject() 
{
	
	int ilIdx = m_DestTableList.GetCurSel();
	CString olTab;
	m_DestTableList.GetText(ilIdx, olTab);
	int ilCheck = m_FlightObject.GetCheck();
	BDLoadTable *polLoad = GetCurrentTabLoad(olTab);
	if(polLoad != NULL)
	{
		if(ilCheck == 0)
		{
			polLoad->IsAFTObject = false;
		}
		else
		{
			polLoad->IsAFTObject = true;
		}
		
	}
}

void UDBLoadSetDefinitionDlg::OnTimeframe() 
{
		
	int ilIdx = m_DestTableList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olTab;
		m_DestTableList.GetText(ilIdx, olTab);
		int ilCheck = m_TimeFrame.GetCheck();
		BDLoadTable *polLoad = GetCurrentTabLoad(olTab);
		if(polLoad != NULL)
		{
			if(ilCheck == 0)
			{
				polLoad->IsTimeFrameObject = false;
			}
			else
			{
				polLoad->IsTimeFrameObject = true;
			}
			
		}
	}
}

void UDBLoadSetDefinitionDlg::OnSelchangeSqlFunctions() 
{
	int ilIdx = m_SqlFunctions.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olWhere;
		m_SQL_Part.GetWindowText(olWhere);
		CString olCBText;
		m_SqlFunctions.GetWindowText(olCBText);
		olWhere += CString(" ") + olCBText;
		m_SQL_Part.SetWindowText(olWhere);
	}
}

void UDBLoadSetDefinitionDlg::OnOrBtn() 
{
	CString olWhere;
	m_SQL_Part.GetWindowText(olWhere);
	olWhere += CString(" OR");
	m_SQL_Part.SetWindowText(olWhere);
}

void UDBLoadSetDefinitionDlg::OnLessBtn() 
{
	CString olWhere;
	m_SQL_Part.GetWindowText(olWhere);
	olWhere += CString(" <");
	m_SQL_Part.SetWindowText(olWhere);
}

void UDBLoadSetDefinitionDlg::OnGtBtn() 
{
	CString olWhere;
	m_SQL_Part.GetWindowText(olWhere);
	olWhere += CString(" >");
	m_SQL_Part.SetWindowText(olWhere);
}

void UDBLoadSetDefinitionDlg::OnEquBtn() 
{
	CString olWhere;
	m_SQL_Part.GetWindowText(olWhere);
	olWhere += CString(" =");
	m_SQL_Part.SetWindowText(olWhere);
}

void UDBLoadSetDefinitionDlg::OnIndirectload() 
{
	int ilIdx = m_DestTableList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olTab;
		m_DestTableList.GetText(ilIdx, olTab);
		int ilCheck = m_IndirectLoad.GetCheck();
		BDLoadTable *polLoad = GetCurrentTabLoad(olTab);
		if(polLoad != NULL)
		{
			if(ilCheck == 0)
			{
				polLoad->IsIndirectLoad = false;
			}
			else
			{
				polLoad->IsIndirectLoad = true;
			}
			
		}
	}
}

void UDBLoadSetDefinitionDlg::OnAndBtn() 
{
	CString olWhere;
	m_SQL_Part.GetWindowText(olWhere);
	olWhere += CString(" AND");
	m_SQL_Part.SetWindowText(olWhere);
}

void UDBLoadSetDefinitionDlg::OnReload() 
{
	m_ReloadList.ResetContent();
	m_ReloadList.SetFont(&ogSmallFonts_Regular_6);
	ogTools.LoadDataWithDefaultSet(&m_ReloadList);
}

void UDBLoadSetDefinitionDlg::OnReloadTable() 
{
	int ilIdx = m_DestTableList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olTab;
		m_DestTableList.GetText(ilIdx, olTab);
		ogTools.ReloadDataForTable(olTab, &m_ReloadList);
	}
}

void UDBLoadSetDefinitionDlg::OnUpdateLoadset() 
{
	OnKillfocusSqlPart();
}

void UDBLoadSetDefinitionDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
