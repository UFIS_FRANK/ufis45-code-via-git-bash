// FormExecuteView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormExecuteDoc.h"
#include "FormExecuteView.h"
#include "UCheckBox.h"
#include "UComboBox.h"
#include "CCSTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormExecuteView

IMPLEMENT_DYNCREATE(FormExecuteView, CFormView)

FormExecuteView::FormExecuteView()
	: CFormView(FormExecuteView::IDD)
{
	//{{AFX_DATA_INIT(FormExecuteView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	imID = WM_USER + 5000;
	omBrush.CreateSolidBrush(RGB(192,192,192));
	omMode = "new";
}

FormExecuteView::~FormExecuteView()
{
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		delete polCtrl->pomControl;
		delete polCtrl;
	}
	omBrush.DeleteObject();
}

void FormExecuteView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FormExecuteView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

 
BEGIN_MESSAGE_MAP(FormExecuteView, CFormView)
	//{{AFX_MSG_MAP(FormExecuteView)
//	ON_WM_CTLCOLOR()
	ON_COMMAND(ID_FIRST,	OnFirst)
	ON_COMMAND(ID_PROVIOUS, OnPrev)
	ON_COMMAND(ID_NEXT,		OnNext)
	ON_COMMAND(ID_LAST,		OnLast)
	ON_COMMAND(ID_NEW_RECORD,		OnNew)
	ON_COMMAND(ID_SAVE,		OnSave)
	ON_COMMAND(ID_DELETE,	OnDelete)
	ON_MESSAGE(WM_UCHECKBOX_KILLFOCUS,	OnCheckBoxKillFocus)
	ON_MESSAGE(WM_UCHECKBOX_CLICKED,	OnCheckBoxClicked)	
	ON_MESSAGE(WM_UCOMBOBOX_SELCHANGE,	OnComboboxSelChanged)	
	ON_MESSAGE(WM_UCOMBOBOX_KILLFOCUS,	OnComboboxKillFocus)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormExecuteView diagnostics

#ifdef _DEBUG
void FormExecuteView::AssertValid() const
{
	CFormView::AssertValid();
}

void FormExecuteView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// FormExecuteView message handlers

void FormExecuteView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	
}

void FormExecuteView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	CSize olSize;
	FormExecuteDoc* pD = GetDocument( );
	UFormProperties *polForm = pD->pomForm;
	olSize = CSize(polForm->omRect.right - polForm->omRect.left, 
				   polForm->omRect.bottom - polForm->omRect.top);
	SetScrollSizes( MM_LOENGLISH, olSize );
	ResizeParentToFit( );   // Default bShrinkOnly argument	

	bool bmFirstFocus = false;
	CWnd *polFirstFocusObject = NULL;
	pD->SetTitle(polForm->omName);
	for(int i = 0; i < polForm->omProperties.GetSize(); i++)
	{
		if(polForm->omProperties[i].omCtrlType == "button")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			CButton *polObj = new CButton();
			polObj->Create(polForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_PUSHBUTTON) , polForm->omProperties[i].omRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &polForm->omProperties[i];
			polCtrl->pomFieldInfo = polForm->GetFieldInfo(polForm->omProperties[i].omDBField, polForm->omTable);
			if(bmFirstFocus == false && polForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(polForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
		}
		if(polForm->omProperties[i].omCtrlType == "edit")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
//			CEdit *polObj = new CEdit();
			CCSEdit *polObj = new CCSEdit();
			CRect olRect = polForm->omProperties[i].omRect;
//			polObj->CreateEx( WS_EX_CLIENTEDGE,_T("EDIT"), "", WS_CHILD|WS_VISIBLE|WS_TABSTOP, polForm->omProperties[i].omRect, this, ++imID);
			polObj->CreateEx( WS_EX_CLIENTEDGE,_T("EDIT"), "", WS_CHILD|WS_VISIBLE|WS_TABSTOP, polForm->omProperties[i].omRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &polForm->omProperties[i];

			SetCCSEditTpye(polObj, polCtrl->pomProperties);

			polCtrl->pomFieldInfo = polForm->GetFieldInfo(polForm->omProperties[i].omDBField, polForm->omTable);
			if(bmFirstFocus == false && polForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(polForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
		}
		if(polForm->omProperties[i].omCtrlType == "checkbox")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
//			CButton *polObj = new CButton();
			UCheckBox *polObj = new UCheckBox(this, polForm->omProperties[i].omBackColor, polForm->omProperties[i].omFontColor);
			polObj->Create(polForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_AUTOCHECKBOX ) , polForm->omProperties[i].omRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &polForm->omProperties[i];
			polCtrl->pomFieldInfo = polForm->GetFieldInfo(polForm->omProperties[i].omDBField, polForm->omTable);
			if(bmFirstFocus == false && polForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(polForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
		}  
		if(polForm->omProperties[i].omCtrlType == "combobox")
		{
			//CComboBox o; o.Create((WS_CHILD|WS_VISIBLE|WS_TABSTOP|CBS_DROPDOWN), polForm->omProperties[i].omRect, this, ++imID);
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
//			CComboBox *polObj = new CComboBox();
			UComboBox *polObj = new UComboBox(this, polForm->omProperties[i].omBackColor, polForm->omProperties[i].omFontColor);
			CRect olCBRect = polForm->omProperties[i].omRect;
			olCBRect.bottom = olCBRect.top + 150;
			polObj->Create((/*WS_CHILD|*/WS_VISIBLE|WS_TABSTOP|CBS_DROPDOWN|CBS_AUTOHSCROLL|WS_VSCROLL|CBS_SORT), olCBRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &polForm->omProperties[i];
			polCtrl->pomFieldInfo = polForm->GetFieldInfo(polForm->omProperties[i].omDBField, polForm->omTable);
			if(bmFirstFocus == false && polForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(polForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(polCtrl->pomFieldInfo != NULL)
			{
				if(!polCtrl->pomFieldInfo->Refe.IsEmpty())
				{
					CString olTable = ogTools.GetTableName(polCtrl->pomFieldInfo->Refe);
					CString olField = ogTools.GetFieldName(polCtrl->pomFieldInfo->Refe);
					if(!olField.IsEmpty() && !olTable.IsEmpty())
					{
//						ogBCD.ShowAllFields(olTable, olField, (CListBox *)polObj);
						int ilDataCount = ogBCD.GetDataCount(olTable);
						for(int k = 0; k < ilDataCount; k++)
						{
							RecordSet olRecord;
							CString   olValue;
							ogBCD.GetRecord(olTable, k,  olRecord);
							int ilIdx = -1;
							ilIdx = ogBCD.GetFieldIndex(olTable, olField);
							if(ilIdx != -1)
							{
								olValue = olRecord[ilIdx];
								polObj->AddString(olValue);
							}

						}

					}
				}
			}
		}
		if(polForm->omProperties[i].omCtrlType == "static")
		{
			//CStatic o;o.Create(polForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE), this, ++imID);
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			CStatic *polObj = new CStatic();
			polObj->Create(polForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE), polForm->omProperties[i].omRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &polForm->omProperties[i];
			polCtrl->pomFieldInfo = polForm->GetFieldInfo(polForm->omProperties[i].omDBField, polForm->omTable);
			omControls.AddTail(polCtrl);
			if(polForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
		}
		if(polForm->omProperties[i].omCtrlType == "grid")
		{
			UGridCtrlProperties *polGridProperty = (UGridCtrlProperties *)&(polForm->omProperties[i]);
			//GxGridTab
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			GxGridTab *polObj = new GxGridTab();
//			GxBrowseTab *polObj = new GxBrowseTab();
			polObj->Create((WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL|WS_HSCROLL), polGridProperty->omRect, this, ++imID);
			polObj->Initialize();
//			polObj->InitBrowserSettings();


/*			CGXDateTimeCtrl *polDateTimeCtrl = new CGXDateTimeCtrl(polObj, ++imID);
			CGXTabbedComboBox *polTabbedCombo = new CGXTabbedComboBox(polObj, ++imID, ++imID);
			polTabbedCombo->m_bFillWithChoiceList = TRUE;
			polObj->RegisterControl(GX_IDS_CTRL_DATETIME, polDateTimeCtrl);
			polObj->RegisterControl(GX_IDS_CTRL_TABBED_COMBOBOX, polTabbedCombo);
*/
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = (UCtrlProperties *)polGridProperty;
			omControls.AddTail(polCtrl);
			if(polGridProperty->bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			CGXStyle olHeaderStyle;
			polObj->SetRowCount(100);
			polObj->SetColCount(polGridProperty->omHaederlist.GetSize());

			//Column Sizes
			for(int j = 0; j < polGridProperty->omHeadersizes.GetSize(); j++)
			{
				polObj->SetColWidth(j+1,j+1,atoi(polGridProperty->omHeadersizes[j].GetBuffer(0)));
			}
			// Set the grid header text
			for( j = 0; j < polGridProperty->omHaederlist.GetSize(); j++)
			{
				olHeaderStyle.SetValue(polGridProperty->omHaederlist[j]);
				polObj->SetStyleRange(CGXRange(0, j+1, 0, j+1 ), olHeaderStyle);
			}

		}
		if(polForm->omProperties[i].omCtrlType == "line")
		{
		}
		if(polForm->omProperties[i].omCtrlType == "rectangle")
		{
		}
		if(polForm->omProperties[i].omCtrlType == "radio")
		{
			//TO DO
		}
		if(polForm->omProperties[i].omCtrlType == "group")
		{
			//TO DO
		}
		if(polForm->omProperties[i].omCtrlType == "listbox")
		{
			//TO DO
		}
	}
	if(polFirstFocusObject != NULL)
	{
		polFirstFocusObject->SetFocus();
	}
	ResizeParentToFit( );   // Default bShrinkOnly argument	
	OnFirst();
}

void FormExecuteView::SetCCSEditTpye(CCSEdit *popEdit, UCtrlProperties *popProperty)
{
	bool blMandatory = false;
	popEdit->SetBKColor(popProperty->omBackColor);
	popEdit->SetTextColor(popProperty->omFontColor);
	if(popProperty->omFormatString.IsEmpty())
	{
		if(popProperty->omFieldType == "TRIM")
		{
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE)
					popEdit->SetTypeToString("", popProperty->imTextLimit, 1);
				else
					popEdit->SetTypeToString("", popProperty->imTextLimit, 0);
			}
			else
			{
				popEdit->SetTypeToString();
			}
		}
		else if(popProperty->omFieldType == "DATE")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToDate(blMandatory);
		}
		else if(popProperty->omFieldType == "TIME")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToTime(blMandatory, false);
		}
		else if(popProperty->omFieldType == "INT")
		{
			popEdit->SetTypeToInt();
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE )
					popEdit->SetTextLimit(0, popProperty->imTextLimit, false);
				else
					popEdit->SetTextLimit(0, popProperty->imTextLimit, true);
			}
		}
		else if(popProperty->omFieldType == "LONG")
		{
			popEdit->SetTypeToInt();
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE )
					popEdit->SetTextLimit(0, popProperty->imTextLimit, false);
				else
					popEdit->SetTextLimit(0, popProperty->imTextLimit, true);
			}
		}
		else if(popProperty->omFieldType == "DOUBLE")
		{
			popEdit->SetTypeToDouble();
		}
	}
	else
	{
		if(popProperty->omFieldType == "TRIM")
		{
			if(popProperty->imTextLimit != 0)
			{
				popEdit->SetTypeToString("", popProperty->imTextLimit, 0);
			}
			else
			{
				popEdit->SetTypeToString();
			}
		}
		popEdit->SetFormat(popProperty->omFormatString);
	}
	popEdit->SetTextErrColor(RED);
}

void FormExecuteView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnPrepareDC(pDC, pInfo);
}

void FormExecuteView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnPrint(pDC, pInfo);
}

void FormExecuteView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}


void FormExecuteView::FillControl(RunTimeCtrl *popCtrl, RecordSet &ropRecord)
{
	if(popCtrl->pomControl == NULL)
	{
		return;
	}
	if(popCtrl->pomProperties != NULL)
	{
		if(popCtrl->pomProperties->omCtrlType == "grid")
		{
			FillGrid(popCtrl, (UGridCtrlProperties *)popCtrl->pomProperties, ropRecord);
		}
		else
		{
			if(!popCtrl->pomProperties->omDBField.IsEmpty())
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, popCtrl->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					CString olValue = ropRecord[ilIdx];
					if(popCtrl->pomProperties->omCtrlType == "checkbox")
					{
						CButton *polButton = (CButton *)popCtrl->pomControl;
						if(olValue == popCtrl->pomProperties->omCheckedValue)
						{
							polButton->SetCheck(1);
						}
						else
						{
							polButton->SetCheck(0);
						}
					}
					else if(popCtrl->pomProperties->omCtrlType == "edit" )
					{
						((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
					}
					else if(popCtrl->pomProperties->omCtrlType == "combobox")
					{
						popCtrl->pomControl->SetWindowText(olValue);
					}
				}
			}
			else
			{
				if(!popCtrl->pomProperties->omMasterName.IsEmpty())
				{
					MakeMasterChild(popCtrl, ropRecord);
				}
			}
		}
	}// end if(polCtrl->pomProperties != NULL)
}

void FormExecuteView::OnFirst()
{
	RecordSet olRecord;
	if(GetDocument()->GetFirstRecord(olRecord) == true)
	{
		omCurrentRecord = olRecord;
		omMode = "update";
		POSITION pos = omControls.GetHeadPosition();
		while (pos != NULL)
		{
			RunTimeCtrl *polCtrl = omControls.GetNext(pos);
			FillControl(polCtrl, olRecord);
		}
	}
}
void FormExecuteView::OnPrev()
{
	RecordSet olRecord;
	if(GetDocument()->GetPrevRecord(olRecord) == true)
	{
		omCurrentRecord = olRecord;
		omMode = "update";
		POSITION pos = omControls.GetHeadPosition();
		while (pos != NULL)
		{
			RunTimeCtrl *polCtrl = omControls.GetNext(pos);
			FillControl(polCtrl, olRecord);
		}
	}
}
void FormExecuteView::OnNext()
{
	RecordSet olRecord;
	if(GetDocument()->GetNextRecord(olRecord) == true)
	{
		omCurrentRecord = olRecord;
		omMode = "update";
		POSITION pos = omControls.GetHeadPosition();
		while (pos != NULL)
		{
			RunTimeCtrl *polCtrl = omControls.GetNext(pos);
			FillControl(polCtrl, olRecord);
		}// end while (pos != NULL)
	}
}
void FormExecuteView::OnLast()
{
	RecordSet olRecord;
	if(GetDocument()->GetLastRecord(olRecord) == true)
	{
		omCurrentRecord = olRecord;
		omMode = "update";
		POSITION pos = omControls.GetHeadPosition();
		while (pos != NULL)
		{ 
			RunTimeCtrl *polCtrl = omControls.GetNext(pos);
			FillControl(polCtrl, olRecord);
		}
	}
}
void FormExecuteView::OnNew()
{
	//Clear the form
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		omMode = "new";
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(polCtrl->pomProperties->omCtrlType == "checkbox")
				{
					CButton *polButton = (CButton *)polCtrl->pomControl;
					polButton->SetCheck(0);
				}
				else if(polCtrl->pomProperties->omCtrlType == "edit" || polCtrl->pomProperties->omCtrlType == "combobox")
				{
					polCtrl->pomControl->SetWindowText("");
				}
			}
		}
	}
	RecordSet olEmptyRecord(ogBCD.GetFieldCount(GetDocument()->omMainTable));
	omCurrentRecord = olEmptyRecord;

}
void FormExecuteView::OnSave()
{
	if(omMode == "new")
	{
		InsertFormData();
	}
	if(omMode == "update")
	{
		UpdateFormData();
	}
}

void FormExecuteView::InsertFormData()
{
	CString olErrorText;
	RecordSet olCurrentRecord(ogBCD.GetFieldCount(GetDocument()->omMainTable));
	bool olFormOk = true;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(!polCtrl->pomProperties->omDBField.IsEmpty())
				{
					if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
					{
						int ilIdx=-1;
						ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polCtrl->pomProperties->omDBField);
						if(ilIdx != -1)
						{
							CString olValue;
	//********** checkbox
							if(polCtrl->pomProperties->omCtrlType == "checkbox")
							{
								CButton *polButton = (CButton *)polCtrl->pomControl;
								if(polButton->GetCheck() == 1)
								{
									olValue = polCtrl->pomProperties->omCheckedValue;
								}
								else
								{
									olValue = "0"; //DO DO provide a omFalseValue
								}
								olCurrentRecord.Values[ilIdx] = olValue;
							}
	//********** edit
							else if(polCtrl->pomProperties->omCtrlType == "edit" )
							{
								CCSEdit *polEdit = (CCSEdit *)polCtrl->pomControl;
								polEdit->GetWindowText(olValue);
								if(polEdit->GetStatus() == true)
								{
									olCurrentRecord.Values[ilIdx] = olValue;
								}
								else
								{
									if(polCtrl->pomProperties->bmMandatory == FALSE)
									{
										olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
										olFormOk = false;
									}
									else
									{
										if(olValue.IsEmpty())
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
											olFormOk = false;
										}
										else
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
											olFormOk = false;
										}
									}
								}
							}
	//********** combobox
							else if(polCtrl->pomProperties->omCtrlType == "combobox")
							{
								((UComboBox *)polCtrl->pomControl)->GetWindowText(olValue);
								olCurrentRecord.Values[ilIdx] = olValue;

								if(polCtrl->pomProperties->bmMandatory == TRUE)
								{
									if(olValue.IsEmpty())
									{
										olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
										olFormOk = false;
									}
								}
							}
						}//if(ilIdx != -1)
					}//if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
				}//if(!polCtrl->pomProperties->omDBField.IsEmpty())
//********** Child Fields
				else
				{
					if(!polCtrl->pomProperties->omMasterName.IsEmpty())
					{
						if(MakeMasterValue(polCtrl, olCurrentRecord, olErrorText) == false)
						{
							olFormOk = false;
						}
					}
				}//else of if(!polCtrl->pomProperties->omDBField.IsEmpty())
			}
		}
	}
	if(olFormOk == true)
	{
		ogBCD.InsertRecord(GetDocument()->omMainTable, olCurrentRecord, true);
		OnNew();
	}
	else
	{
		MessageBox(olErrorText, "Error", MB_OK);
	}
}
void FormExecuteView::UpdateFormData()
{
	CString olErrorText;
//	RecordSet olCurrentRecord(ogBCD.GetFieldCount(GetDocument()->omMainTable));
	bool olFormOk = true;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(!polCtrl->pomProperties->omDBField.IsEmpty())
				{
					if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
					{
						int ilIdx=-1;
						ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polCtrl->pomProperties->omDBField);
						if(ilIdx != -1)
						{
							CString olValue;
	//********** checkbox
							if(polCtrl->pomProperties->omCtrlType == "checkbox")
							{
								CButton *polButton = (CButton *)polCtrl->pomControl;
								if(polButton->GetCheck() == 1)
								{
									olValue = polCtrl->pomProperties->omCheckedValue;
								}
								else
								{
									olValue = "0"; //DO DO provide a omFalseValue
								}
								GetDocument()->omCurrentRecord.Values[ilIdx] = olValue;
							}
	//********** edit
							else if(polCtrl->pomProperties->omCtrlType == "edit" )
							{
								CCSEdit *polEdit = (CCSEdit *)polCtrl->pomControl;
								polEdit->GetWindowText(olValue);
								if(polEdit->GetStatus() == true)
								{
									GetDocument()->omCurrentRecord.Values[ilIdx] = olValue;
								}
								else
								{
									if(polCtrl->pomProperties->bmMandatory == FALSE)
									{
										olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
										olFormOk = false;
									}
									else
									{
										if(olValue.IsEmpty())
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
											olFormOk = false;
										}
										else
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
											olFormOk = false;
										}
									}
								}
							}
	//********** combobox
							else if(polCtrl->pomProperties->omCtrlType == "combobox")
							{
								((UComboBox *)polCtrl->pomControl)->GetWindowText(olValue);
								GetDocument()->omCurrentRecord.Values[ilIdx] = olValue;

								if(polCtrl->pomProperties->bmMandatory == TRUE)
								{
									if(olValue.IsEmpty())
									{
										olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
										olFormOk = false;
									}
								}
							}
						}//if(ilIdx != -1)
					}//if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
				}//if(!polCtrl->pomProperties->omDBField.IsEmpty())
//********** Child Fields
				else
				{
					if(!polCtrl->pomProperties->omMasterName.IsEmpty())
					{
						if(MakeMasterValue(polCtrl, GetDocument()->omCurrentRecord, olErrorText) == false)
						{
							olFormOk = false;
						}
					}
				}//else of if(!polCtrl->pomProperties->omDBField.IsEmpty())
			}
		}
	}
	if(olFormOk == true)
	{
		CString olUrno;
		int ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, "URNO");
		if(ilIdx != -1)
		{
			olUrno = GetDocument()->omCurrentRecord[ilIdx];

			ogBCD.SetRecord(GetDocument()->omMainTable, "URNO", olUrno, GetDocument()->omCurrentRecord.Values, true );
			POSITION pos = omControls.GetHeadPosition();
			while (pos != NULL)
			{
				RunTimeCtrl *polCtrl = omControls.GetNext(pos);
				FillControl(polCtrl, GetDocument()->omCurrentRecord);
			}// end while (pos != NULL)
		}
	}
	else
	{
		MessageBox(olErrorText, "Error", MB_OK);
	}
}

void FormExecuteView::OnDelete()
{
}


HBRUSH FormExecuteView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
 	HBRUSH hbr = 0;


	POSITION pos = omControls.GetHeadPosition();
	if(nCtlColor == CTLCOLOR_LISTBOX || nCtlColor == CTLCOLOR_EDIT  )
	{
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(COLORREF(RGB(255,255,255)));
		hbr = omBrush;
	}
	else
	{
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(COLORREF(RGB(192,192,192)));
		hbr = omBrush;
	}
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl->pomControl != NULL)
		{
			long llRc;

			if (polCtrl->pomControl == pWnd)
			{
				llRc = pDC->SetBkColor(polCtrl->pomProperties->omBackColor);
				llRc = pDC->SetTextColor(polCtrl->pomProperties->omFontColor);
				omBrush.DeleteObject();
				omBrush.CreateSolidBrush(polCtrl->pomProperties->omBackColor);
				hbr = omBrush;
			}
		}
	}
	return hbr;
}

RunTimeCtrl *FormExecuteView::GetCtrlByName(CString opName)
{
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties->omName == opName)
			{
				return polCtrl; 
			}
		}
	}
	return NULL;

}

bool FormExecuteView::MakeMasterValue(RunTimeCtrl *popChild, RecordSet &ropRecord, CString &ropErrorText)
{
	RunTimeCtrl *polMaster = GetCtrlByName(popChild->pomProperties->omMasterName);
	bool blChildOk = true;
	CString olValue;
	if(polMaster != NULL)
	{
		// Get current value of master field
		CString olMasterValue;
		polMaster->pomControl->GetWindowText(olMasterValue);
		// Check for valid entry
		if(popChild->pomProperties->omCtrlType == "edit" )
		{
			CCSEdit *polEdit = (CCSEdit *)popChild->pomControl;
			polEdit->GetWindowText(olValue);
			if(polEdit->GetStatus() == true)
			{
			}
			else
			{
				if(popChild->pomProperties->bmMandatory == FALSE)
				{
					ropErrorText+= popChild->pomProperties->omLabel + CString(":   Incorrect Format!\n");
					blChildOk = false;
				}
				else
				{
					if(olValue.IsEmpty())
					{
						ropErrorText+= popChild->pomProperties->omLabel + CString(":   Field is mandatory!\n");
						blChildOk = false;
					}
					else
					{
						ropErrorText+= popChild->pomProperties->omLabel + CString(":   Incorrect Format!\n");
						blChildOk = false;
					}
				}
			}
		}
		if( blChildOk == true && !olValue.IsEmpty())
		{
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "DATE")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					CString olCurrVal = ropRecord[ilIdx];
					COleDateTime olDate;
					olDate = OleDateStringToDate(olValue);
					if(olDate.GetStatus() == COleDateTime::valid)
					{
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olDate.Format("%Y%m%d") + olCurrVal.Right(6);
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olDate.Format("%Y%m%d      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
					}
				}
			}
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "TIME")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					CString olCurrVal = ropRecord[ilIdx];
					CTime olDate;
					olDate = HourStringToDate(olValue,true);
					if(olDate != TIMENULL)
					{
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olCurrVal.Left(8) + olDate.Format("%H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olDate.Format("        %H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
					}
				}
			}
		}
	}
	return blChildOk;
}

void FormExecuteView::MakeMasterChild(RunTimeCtrl *popChild, RecordSet &ropRecord)
{
	RunTimeCtrl *polMaster = GetCtrlByName(popChild->pomProperties->omMasterName);
	if(polMaster != NULL)
	{
		if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "DATE")
		{
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polMaster->pomProperties->omDBField);
			if(ilIdx != -1)
			{
				CString olValue = ropRecord[ilIdx];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != -1)
					olValue = olTime.Format("%d.%m.%Y");
				else
					olValue = CString("");
				((CCSEdit *)(popChild->pomControl))->SetInitText(olValue, false);
//				popChild->pomControl->SetWindowText(olValue);
			}
		}
		if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "TIME")
		{
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(GetDocument()->omMainTable, polMaster->pomProperties->omDBField);
			if(ilIdx != -1)
			{
				CString olValue = ropRecord[ilIdx];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != -1)
					olValue = olTime.Format("%H:%M");
				else
					olValue = CString("");
				((CCSEdit *)(popChild->pomControl))->SetInitText(olValue, false);
//				popChild->pomControl->SetWindowText(olValue);
			}
		}
	}
}

void FormExecuteView::FillGrid(RunTimeCtrl *polCtrl, 
							   UGridCtrlProperties *popGridProperties, 
							   RecordSet &ropRecord)
{
//	GxBrowseTab *polGrid = (GxBrowseTab *)polCtrl->pomControl;
	GxGridTab *polGrid = (GxGridTab *)polCtrl->pomControl;
	polGrid->GetParam()->EnableUndo(FALSE);
	BOOL bLockOld = polGrid->LockUpdate(TRUE);

	int ilRowCount = polGrid->GetRowCount();
	if(ilRowCount > 1)
	{
		polGrid->RemoveRows(1, ilRowCount);
	}

	CCSPtrArray<RecordSet> olGridRecords;
	if(!popGridProperties->omGridTable.IsEmpty())
	{
		if(!popGridProperties->omMasterDetail.IsEmpty())
		{
			CStringArray olRelParts; //Relationparts STF.URNO=SPE.SURN ==> Master [0] = STF.URNO Detail [1] = SPE.SURN
			ExtractItemList(popGridProperties->omMasterDetail, &olRelParts, '=');
			if(olRelParts.GetSize() == 2)
			{
				CString olMasterTable = ogTools.GetTableName(olRelParts[0]);
				CString olMasterField = ogTools.GetFieldName(olRelParts[0]);
				CString olDetailTable = ogTools.GetTableName(olRelParts[1]);
				CString olDetailField = ogTools.GetFieldName(olRelParts[1]);
				if(!olMasterTable.IsEmpty() && !olMasterField.IsEmpty() && !olDetailTable.IsEmpty() && !olDetailField.IsEmpty())
				{
					int ilMasterFieldIdx = ogBCD.GetFieldIndex(olMasterTable, olMasterField);
					if(ilMasterFieldIdx >= 0)
					{
						CString olKeyValue = ropRecord[ogBCD.GetFieldIndex(olMasterTable, olMasterField)];
						ogBCD.GetRecords(olDetailTable, olDetailField, olKeyValue, &olGridRecords);
						if(olGridRecords.GetSize() > 0)
						{
							polGrid->InsertRows(polGrid->GetRowCount( )+1, olGridRecords.GetSize());
						}
						for(int i = 0; i < olGridRecords.GetSize(); i++)
						{

							for(int j = 0; j < popGridProperties->omFieldlist.GetSize(); j++)
							{
								CString olValue;
								int ilIdx = ogBCD.GetFieldIndex(olDetailTable, popGridProperties->omFieldlist[j]);
								if(ilIdx >= 0)
								{
									olValue = olGridRecords[i][ilIdx];
									if(j < popGridProperties->omTypelist.GetSize())
									{
										if(popGridProperties->omTypelist[j] == "DATE")
										{
											CTime olTime;
											olTime = DBStringToDateTime(olValue);
											if(olTime != -1)
												olValue = olTime.Format("%d.%m.%Y");
											else
												olValue = CString("");
/*											polGrid->SetStyleRange(CGXRange(i+1, j+1),
												CGXStyle()
													.SetControl(GX_IDS_CTRL_DATETIME)
//													.SetUserAttribute(GX_IDS_UA_DATEMAX, MAX_DATE) 
//													.SetUserAttribute(GX_IDS_UA_DATEMIN, MIN_DATE) 
													.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("2"))
													.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("dd.MM.yyyy"))
													.SetValue(olValue));
*/
											polGrid->SetStyleRange(CGXRange(i+1, j+1),
																		CGXStyle()
																		.SetControl(GX_IDS_CTRL_DATETIME)
																		.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT,_T("dd.MM.yyyy"))
																		.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("1"))
																		.SetValue(olValue));


										}
										if(popGridProperties->omTypelist[j] == "DTIM")
										{
											CTime olTime;
											olTime = DBStringToDateTime(olValue);
											if(olTime != -1)
												olValue = olTime.Format("%d.%m.%Y - %H:%M");
											else
												olValue = CString("");
											polGrid->SetValueRange(CGXRange(i+1, j+1), olValue);
										}
										if(popGridProperties->omTypelist[j] == "TIME")
										{
											polGrid->SetValueRange(CGXRange(i+1, j+1), olValue);
										}
										if(popGridProperties->omTypelist[j] == "TRIM")
										{
											if(!popGridProperties->omLookupfieldList[j].IsEmpty())
											{
												CStringArray olLookups;
												CString olChoiceList;
												CString olStrTab;
												CString olStrFld;
												CStringArray olFldLookupList;
												ExtractItemList(popGridProperties->omLookupfieldList[j], &olLookups, '+');
												for(int k = 0; k < olLookups.GetSize(); k++)
												{
 													olStrTab = ogTools.GetTableName(olLookups[k]);
													if((k+1) == olLookups.GetSize())
													{
														olStrFld += ogTools.GetFieldName(olLookups[k]);
														olFldLookupList.Add(ogTools.GetFieldName(olLookups[k]));
													}
													else
													{
														olStrFld += ogTools.GetFieldName(olLookups[k])+CString(",");
														olFldLookupList.Add(ogTools.GetFieldName(olLookups[k]));
													}
													
												}
												int ilC3 = ogBCD.GetDataCount(olStrTab);
												for(k = 0; k < ilC3; k++)
												{
													RecordSet olTmpRec;
													CString olTmpChoiceList;
													for(int l = 0; l < olFldLookupList.GetSize(); l++)
													{
														ogBCD.GetRecord(olStrTab, k, olTmpRec);
														int ilIdx3 = ogBCD.GetFieldIndex(olStrTab, olFldLookupList[l]);
														if(ilIdx3 >= 0)
														{
															olTmpChoiceList+=olTmpRec[ilIdx3]+CString("\t");
														}
													}
													if(!olTmpChoiceList.IsEmpty())
														olChoiceList+=olTmpChoiceList+CString("\n");
												}
/*												CStringArray olData;
												ogBCD.GetAllFields(olStrTab, olStrFld, CString("\t"), TOTRIMLEFT, olData);
												for(k = 0; k < olData.GetSize(); k++)
												{
													olChoiceList+=olData[k]+CString("\n");
												}
*/
												polGrid->SetStyleRange(CGXRange(i+1, j+1),
																			CGXStyle()
																			.SetHorizontalAlignment(DT_LEFT)
																			.SetVerticalAlignment(DT_BOTTOM)
																			.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
																			.SetChoiceList(olChoiceList)
																			.SetValue(olValue));

											}
											else
											{
												polGrid->SetValueRange(CGXRange(i+1, j+1), olValue);
											}

										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	polGrid->LockUpdate(bLockOld);
	polGrid->Redraw();
	polGrid->Invalidate();
	olGridRecords.DeleteAll();
}

bool FormExecuteView::HasChildFields(RunTimeCtrl *popCtrl)
{
	CString olFieldName = popCtrl->pomProperties->omName;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(olFieldName == polCtrl->pomProperties->omMasterName)
				{
					return true;
				}
			}
		}
	}
	return false;
}

LONG FormExecuteView::OnCheckBoxKillFocus(UINT wParam, LONG lParam)
{
	UCheckBox *polCtrl = (UCheckBox *)wParam;
	return 0L;
}

LONG FormExecuteView::OnCheckBoxClicked(UINT wParam, LONG lParam)	
{
	UCheckBox *polCtrl = (UCheckBox *)wParam;
	return 0L;
}

LONG FormExecuteView::OnComboboxSelChanged(UINT wParam, LONG lParam)
{
	UComboBox *polCtrl = (UComboBox *)wParam;
	return 0L;
}

LONG FormExecuteView::OnComboboxKillFocus(UINT wParam, LONG lParam)
{
	UComboBox *polCtrl = (UComboBox *)wParam;
	return 0L;
}

