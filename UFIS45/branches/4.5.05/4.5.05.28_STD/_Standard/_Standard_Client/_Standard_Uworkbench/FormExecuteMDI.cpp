// FormExecuteMDI.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormExecuteMDI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormExecuteMDI
#include "resource.h"
IMPLEMENT_DYNCREATE(FormExecuteMDI, CMDIChildWnd)

FormExecuteMDI::FormExecuteMDI()
{
}

FormExecuteMDI::~FormExecuteMDI()
{
}


BEGIN_MESSAGE_MAP(FormExecuteMDI, CMDIChildWnd)
	//{{AFX_MSG_MAP(FormExecuteMDI)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormExecuteMDI message handlers

int FormExecuteMDI::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_NAVIGATOR_BAR))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	
	return 0;
}
