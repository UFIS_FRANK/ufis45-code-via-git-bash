#ifndef __U_OBJECT_DATA_DEFINITIONS__
#define __U_OBJECT_DATA_DEFINITIONS__


#include "CCSPtrArray.h"
//-****************************************************************************************
//
//-****************************************************************************************
/*class UObjectDataSourceDefinition : public CObject
{
public:
	UObjectDataSourceDefinition();
	UObjectDataSourceDefinition(CString opName);
	~UObjectDataSourceDefinition();

	CString omName;				//give this object a name

	CString omDataObject;			//which object is represented e.g. a table
								//could also be a query
	bool bmIsQuery;				//if omBaseData is a query ==> set to true

	CStringArray omFields;		//which field(s) are/is used to specifiy a object ==>
								//e.g group or line or leftscale

	void SetName(CString opName){omName = opName;}
	void SetDataObject(CString opName){omName= opName;}
};

//-****************************************************************************************
//
//-****************************************************************************************
class ULeftScaleDataSource : public UObjectDataSourceDefinition
{
public:
	ULeftScaleDataSource();
	~ULeftScaleDataSource();

};

//-****************************************************************************************
//
//-****************************************************************************************
class UBarDataSource : public UObjectDataSourceDefinition
{
public:
	UBarDataSource();
	UBarDataSource(CString opName);
	~UBarDataSource();
};

//-****************************************************************************************
//
//-****************************************************************************************
class UBkBarDataSource : public UObjectDataSourceDefinition
{
public:
	UBkBarDataSource();
	UBkBarDataSource(CString opName);
	~UBkBarDataSource();
};

//-****************************************************************************************
//
//-****************************************************************************************
class ULineDataSource : public UObjectDataSourceDefinition
{
public:
	ULineDataSource();
	ULineDataSource(CString opName);
	~ULineDataSource();
};

//-****************************************************************************************
//
//-****************************************************************************************
class UGroupDataSource : public UObjectDataSourceDefinition
{
public:
	UGroupDataSource();
	UGroupDataSource(CString opName);
	~UGroupDataSource();

	CString omGroupTextField;		//Field, which contains the text of the groupbutton/grouptext
								//shown in GUI
	void SetGroupText(CString opTextField){omGroupTextField = opTextField;}
};

//-****************************************************************************************
//
//-****************************************************************************************
class UObjectSet : public CObject
{
public:
	UObjectSet ();
	~UObjectSet ();
	UGroupDataSource omGroupDataSource;

};
*/
//-****************************************************************************************
//
//-****************************************************************************************
class UQueryDetail //: public CObject
{
public:
	UQueryDetail(){;}//:CObject(){;}
	~UQueryDetail(){;}
	UQueryDetail(const UQueryDetail &opO)
	{
		this->imColNo = opO.imColNo;
		this->Object = opO.Object;
		this->Field = opO.Field;
		this->Sort = opO.Sort;
		this->Operator = opO.Operator;
		this->Criteria = opO.Criteria;
		this->omShowAsResult = opO.omShowAsResult;
	}

	const UQueryDetail &operator =(const UQueryDetail &opO) 
	{
		this->imColNo = opO.imColNo;
		this->Object = opO.Object;
		this->Field = opO.Field;
		this->Sort = opO.Sort;
		this->Operator = opO.Operator;
		this->Criteria = opO.Criteria;
		this->omShowAsResult = opO.omShowAsResult;

		return *this;
	}


	int     imColNo;	//Column Number 0 - 20
	CString Object;		//Table e.g. AFT
	CString Field;		//Field e.g. ADID

	CString Sort;		//ASC,DSC,NO

	CString Operator;	// =,<,>,#,~(Between)    | > 30
	CString Criteria;	// Value		  | <> GF
	
	CString omShowAsResult; // Show => 1, Notshow => 0

};

//-****************************************************************************************
//
//-****************************************************************************************
class UQuery : public CObject
{
public:
	UQuery():CObject()
	{
		;
	}
	~UQuery(){omQueryDetails.DeleteAll();}
	const UQuery &operator =(const UQuery &opO) 
	{
		this->omName = opO.omName;
		for(int i = 0; i < opO.omQueryDetails.GetSize(); i++)
		{
			this->omQueryDetails.NewAt(omQueryDetails.GetSize(), opO.omQueryDetails[i]);
		}
		for(i = 0; i < opO.omObjRelation.GetSize(); i++)
		{
			this->omObjRelation.Add(opO.omObjRelation[i]);
		}
		for(i = 0; i < opO.omUsedTables.GetSize(); i++)
		{
			this->omUsedTables.Add(opO.omUsedTables[i]);
		}
		return *this;
	}
	UQueryDetail * GetQueryDetailByColumn(int ipCol)
	{
		bool blFound = false;
		int ilCount = omQueryDetails.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			if(omQueryDetails[i].imColNo == ipCol)
			{
				return &omQueryDetails[i];
			}
		}
		if(blFound == false)
		{
			return NULL;
		}
		return NULL;
	}

	CStringArray omObjRelation; //e.g. CCA.FLNU=AFT.URNO
	CStringArray omUsedTables;

	CString omName;		//Name of the Query
	CCSPtrArray<UQueryDetail> omQueryDetails;

	CString GetName(){return omName;} 
};


//-****************************************************************************************
//
//-****************************************************************************************
class BDLoadTable  : public CObject
{
public:
	BDLoadTable():CObject()
	{
		IsTimeFrameObject = false;
		IsAFTObject = false;
	}
	BDLoadTable(const BDLoadTable &opO)
	{
		this->FieldList.RemoveAll();
		for(int i = 0; i < opO.FieldList.GetSize(); i++)
		{
			this->FieldList.Add(opO.FieldList[i]);
		}
		this->Name = opO.Name;
		this->IsTimeFrameObject = opO.IsTimeFrameObject;
		this->IsAFTObject = opO.IsAFTObject;
		this->IsIndirectLoad = opO.IsIndirectLoad;
		this->SqlPart = opO.SqlPart;
	}
	~BDLoadTable()
	{
		FieldList.RemoveAll();
	}
	const BDLoadTable &operator =(const BDLoadTable &opO) 
	{
		this->FieldList.RemoveAll();
		for(int i = 0; i < opO.FieldList.GetSize(); i++)
		{
			this->FieldList.Add(opO.FieldList[i]);
		}
		this->Name = opO.Name;
		this->IsTimeFrameObject = opO.IsTimeFrameObject;
		this->IsAFTObject = opO.IsAFTObject;
		this->IsIndirectLoad = opO.IsIndirectLoad;
		this->SqlPart = opO.SqlPart;
		return *this;
	}
	CStringArray FieldList;
	CString Name;
	CString SqlPart;
	bool IsTimeFrameObject; //Should produce an Timeframedialog per Default ==>
							//otherwise should use  global variables called 
							// CTime ogTimeFramFrom and ogTimeFramTo if not TIMENULL

	bool IsIndirectLoad;	//For selecting data, that depends on other loaded data, e.g. used for select with
							// "IN" - clause

	bool IsAFTObject;       //Special for AFT because of 2 records that have to be concated
};
//-****************************************************************************************
//
//-****************************************************************************************
class UDBLoadSet : public CObject
{
public:
	UDBLoadSet():CObject()
	{
	}
	~UDBLoadSet()
	{
		TableArray.DeleteAll();
	}
	const UDBLoadSet &operator =(const UDBLoadSet &opO) 
	{
		this->TableArray.DeleteAll();
		for(int i = 0; i < opO.TableArray.GetSize(); i++)
		{
			this->TableArray.NewAt(this->TableArray.GetSize(), opO.TableArray[i]);
		}
		this->Name = opO.Name;
		return *this;
	}
	CCSPtrArray<BDLoadTable> TableArray;
	CString Name;
};
//-****************************************************************************************
//
//-****************************************************************************************
class UGridFields : public CObject
{
public:
	UGridFields() : CObject()
	{;}
	~UGridFields(){;}
	CString Table,
		    Field,
			Type,
			Header,
			Visible,
			Key,	// For the moment REFE from SYSTAB ==> may be changed later
			Unique,		
			QueryName;

	UGridFields(const UGridFields &opO)
	{
		this->Table     = opO.Table;
		this->Field		= opO.Field	;
		this->Type		= opO.Type	;
		this->Header	= opO.Header;
		this->Visible   = opO.Visible;
		this->Key		= opO.Key	;
		this->Unique    = opO.Unique;
		this->QueryName = opO.QueryName;
	}
	const UGridFields &operator =(const UGridFields &opO) 
	{
 		this->Table     = opO.Table;
		this->Field		= opO.Field	;
		this->Type		= opO.Type	;
		this->Header	= opO.Header;
		this->Visible   = opO.Visible;
		this->Key		= opO.Key	;
		this->Unique    = opO.Unique;
		this->QueryName = opO.QueryName;
		return *this;
	}

};
//-****************************************************************************************
//
//-****************************************************************************************
class UGridDefinition : public CObject
{
public:
	UGridDefinition():CObject()
	{
		AsFrameWindow = true;
		AsChoiceList  = false;
		HasViewButton = true;
		HeaderSize    = true;
		IsEditable    = false;
		MultiSelect   = false;
		Printable     = true;
		SensitiveForChanges = true;
		SetDefaultTimeFrame = true;
		ShowTimeFrameDialog = false;
		Today               = 0;// 0 = today, 1 = tomorrow
		left = -1;
		top = -1;
		right = -1;
		bottom = -1;
		AsBasicData = FALSE;
		Where		= CString("");

	}
	~UGridDefinition()
	{
		omGridFields.DeleteAll();
	}
	UGridDefinition(const UGridDefinition &opO)
	{
		this->AsFrameWindow       = opO.AsFrameWindow      ; 
		this->AsChoiceList        = opO.AsChoiceList       ;
		this->DataSource          = opO.DataSource         ;
		this->Filter              = opO.Filter             ;
		this->GridName            = opO.GridName           ;
		this->HasViewButton       = opO.HasViewButton      ;
		this->HeaderSize          = opO.HeaderSize         ;
		this->IsEditable          = opO.IsEditable         ;
		this->MultiSelect         = opO.MultiSelect        ;
		this->Printable           = opO.Printable          ;
		this->ReturnField         = opO.ReturnField        ;
		this->SensitiveForChanges = opO.SensitiveForChanges;
		this->SetDefaultTimeFrame = opO.SetDefaultTimeFrame;
		this->ShowTimeFrameDialog = opO.ShowTimeFrameDialog;
		this->Today               = opO.Today              ;
		this->TodayFrom           = opO.TodayFrom          ;  
		this->TodayTo             = opO.TodayTo            ;
		this->TomorrowFrom        = opO.TomorrowFrom       ;
		this->TomorrowTo          = opO.TomorrowTo         ;
		this->AsBasicData		  = opO.AsBasicData		;
		this->FormName			  = opO.FormName;
		this->omGridFields.DeleteAll();
		for(int i = 0; i < opO.omGridFields.GetSize(); i++)
		{
			this->omGridFields.NewAt(this->omGridFields.GetSize(),opO.omGridFields[i]);
		}
		this->omTables.RemoveAll();
		for(i = 0; i < opO.omTables.GetSize(); i++)
		{
			this->omTables.Add(opO.omTables[i]); 
		}
		this->omColWidths.RemoveAll();
		for( i = 0; i < opO.omColWidths.GetSize(); i++)
		{
			this->omColWidths.Add(opO.omColWidths[i]);
		}
		this->omTableRelations = opO.omTableRelations;	
		this->omRowConnection  = opO.omRowConnection;	
		this->omSortColumns.RemoveAll();
		for( i = 0; i < opO.omSortColumns.GetSize(); i++)
		{
			this->omSortColumns.Add(opO.omSortColumns[i]);
		}
		this->omSortDirection = opO.omSortDirection;		//ASC or DESC
		this->left		= opO.left;					//Windowposition
		this->top		= opO.top; 
		this->right		= opO.right;
		this->bottom	= opO.bottom;
		this->Where		= opO.Where;
	}
	const UGridDefinition &operator =(const UGridDefinition &opO) 
	{
		this->AsFrameWindow       = opO.AsFrameWindow      ; 
		this->AsChoiceList        = opO.AsChoiceList       ;
		this->DataSource          = opO.DataSource         ;
		this->Filter              = opO.Filter             ;
		this->GridName            = opO.GridName           ;
		this->HasViewButton       = opO.HasViewButton      ;
		this->HeaderSize          = opO.HeaderSize         ;
		this->IsEditable          = opO.IsEditable         ;
		this->MultiSelect         = opO.MultiSelect        ;
		this->Printable           = opO.Printable          ;
		this->ReturnField         = opO.ReturnField        ;
		this->SensitiveForChanges = opO.SensitiveForChanges;
		this->SetDefaultTimeFrame = opO.SetDefaultTimeFrame;
		this->ShowTimeFrameDialog = opO.ShowTimeFrameDialog;
		this->Today               = opO.Today              ;
		this->TodayFrom           = opO.TodayFrom          ;  
		this->TodayTo             = opO.TodayTo            ;
		this->TomorrowFrom        = opO.TomorrowFrom       ;
		this->TomorrowTo          = opO.TomorrowTo         ;
		this->AsBasicData		  = opO.AsBasicData		;
		this->FormName			  = opO.FormName;
		this->omGridFields.DeleteAll();
		for(int i = 0; i < opO.omGridFields.GetSize(); i++)
		{
			this->omGridFields.NewAt(this->omGridFields.GetSize(),opO.omGridFields[i]);
		}
		this->omTables.RemoveAll();
		for(i = 0; i < opO.omTables.GetSize(); i++)
		{
			this->omTables.Add(opO.omTables[i]); 
		}
		this->omColWidths.RemoveAll();
		for( i = 0; i < opO.omColWidths.GetSize(); i++)
		{
			this->omColWidths.Add(opO.omColWidths[i]);
		}
		this->omTableRelations = opO.omTableRelations;	
		this->omRowConnection  = opO.omRowConnection;	
		this->omSortColumns.RemoveAll();
		for( i = 0; i < opO.omSortColumns.GetSize(); i++)
		{
			this->omSortColumns.Add(opO.omSortColumns[i]);
		}
		this->omSortDirection = opO.omSortDirection;		//ASC or DESC
		this->left		= opO.left;					//Windowposition
		this->top		= opO.top; 
		this->right		= opO.right;
		this->bottom	= opO.bottom;
		
		this->Where		= opO.Where;
		return *this;
	}

	CString	GridName;
	CString FormName;
	BOOL	AsBasicData;
	BOOL	AsFrameWindow;
	BOOL	AsChoiceList;
	CString	DataSource; //For Queries
	CString	Filter;
	BOOL	HasViewButton;
	BOOL	HeaderSize;
	BOOL	IsEditable;
	BOOL	MultiSelect;
	BOOL	Printable;
	CString	ReturnField;
	BOOL	SensitiveForChanges;
	BOOL	SetDefaultTimeFrame;
	BOOL	ShowTimeFrameDialog;
	int		Today;
	CString	TodayFrom;
	CString	TodayTo;
	CString	TomorrowFrom;
	CString	TomorrowTo;
	
	CStringArray omTables;			//All Tables, which are used by the grid (Master/Detail)
	CString		 omTableRelations;	// e.g. [GHD.FKEY=AFT.URNO]
	CString	     omRowConnection;	// [ AFT.RKEY > 1 Line] Show a complete Rotation in 1 Line (2 Records)

	CCSPtrArray<UGridFields> omGridFields;
	CUIntArray omColWidths;
	CUIntArray omSortColumns;
	CString    omSortDirection;		//ASC or DESC
	int		   left,				//Windowposition
			   top, 
			   right, 
			   bottom;
// members decalred below will not be stored in cfg-file
// they are for temporary information only
	CString	   Where;	// Condition of the view
};




#endif //__U_OBJECT_DATA_DEFINITIONS__