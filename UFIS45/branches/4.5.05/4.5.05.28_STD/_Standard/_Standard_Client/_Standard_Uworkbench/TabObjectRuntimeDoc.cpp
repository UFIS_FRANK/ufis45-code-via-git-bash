// TabObjectRuntimeDoc.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "TabObjectRuntimeDoc.h"
#include "TabObjectRuntimeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeDoc

IMPLEMENT_DYNCREATE(TabObjectRuntimeDoc, CDocument)

TabObjectRuntimeDoc::TabObjectRuntimeDoc()
{
}

BOOL TabObjectRuntimeDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

TabObjectRuntimeDoc::~TabObjectRuntimeDoc()
{
}


BEGIN_MESSAGE_MAP(TabObjectRuntimeDoc, CDocument)
	//{{AFX_MSG_MAP(TabObjectRuntimeDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeDoc diagnostics

#ifdef _DEBUG
void TabObjectRuntimeDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void TabObjectRuntimeDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeDoc serialization

void TabObjectRuntimeDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}
void TabObjectRuntimeDoc::UpdateTabView()
{
	if(!m_viewList.IsEmpty()) {
		((TabObjectRuntimeView*)m_viewList.GetHead())->UpdateView();
	}
}

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeDoc commands
