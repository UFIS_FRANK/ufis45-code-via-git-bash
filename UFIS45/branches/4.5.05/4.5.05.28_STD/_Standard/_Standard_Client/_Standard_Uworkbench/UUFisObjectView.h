#if !defined(AFX_UUFISOBJECTVIEW_H__0408F143_2E00_11D3_B07D_00001C019205__INCLUDED_)
#define AFX_UUFISOBJECTVIEW_H__0408F143_2E00_11D3_B07D_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUFisObjectView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UUFisObjectView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "CCSGlobl.h"
#include "UQueryDesingerDoc.h"

enum
{
	PAGE_DB_TABLES, PAGE_GRIDS, PAGE_GANTTS, PAGE_QUERIES, PAGE_FORMS, PAGE_DBLOADSET
};

class UUFisObjectView : public CFormView
{
protected:
	UUFisObjectView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UUFisObjectView)

// Form Data
public:
	//{{AFX_DATA(UUFisObjectView)
	enum { IDD = IDD_UFIS_OBJECTS };
	CButton	m_DeleteB;
	CButton	m_OpenB;
	CButton	m_NewB;
	CButton	m_DesignB;
	CListBox	m_ObjectList;
	CTabCtrl	m_ObjectsTab;
	//}}AFX_DATA

	bool bmIsInit;
// Attributes
public:
	int imCurrentPage;
	void InitTablePage();
	void InitGridsPage();
	void InitGanttsPage();
	void InitQueriesPage();
	void InitFormsPage();
	void InitDBLoadSetPage();
	void PerFormOpenQuery();
	void PerFormOpenLoadSet();
	void PerformOpenGird();
	void PerformOpenForm();
	void PerformExecuteForm();

	void PerFormRunGrid();

	void PerformDeleteGrid();
	void PerformDeleteQuery();
	void PerformDeleteForm();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUFisObjectView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UUFisObjectView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(UUFisObjectView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnClose();
	afx_msg void OnSelchangeObjects(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNew();
	afx_msg void OnDblclkObjectList();
	afx_msg void OnDesign();
	afx_msg void OnDelete();
	afx_msg void OnOpen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUFISOBJECTVIEW_H__0408F143_2E00_11D3_B07D_00001C019205__INCLUDED_)
