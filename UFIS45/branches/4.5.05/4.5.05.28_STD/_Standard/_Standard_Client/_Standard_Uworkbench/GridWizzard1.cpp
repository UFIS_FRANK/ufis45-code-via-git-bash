// GridWizzard1.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "GridWizzard1.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GridWizzard1 dialog


GridWizzard1::GridWizzard1(CWnd* pParent, UGridDefinition *popGridDef)
	: CDialog(GridWizzard1::IDD, pParent)
{
	pomGridDef = popGridDef;
	//{{AFX_DATA_INIT(GridWizzard1)
	v_AsFrameWindow = FALSE;
	v_AsChoiceList = FALSE;
	v_DataSource = _T("");
	v_Filter = _T("");
	v_GridName = _T("");
	v_GroupBy = _T("");
	v_HasViewButton = FALSE;
	v_HeaderSize = FALSE;
	v_IsEditable = FALSE;
	v_MultiSelect = FALSE;
	v_Printable = FALSE;
	v_ReturnField = _T("");
	v_SaveUserLayout = FALSE;
	v_SensitiveForChanges = FALSE;
	v_SetDefaultTimeFrame = FALSE;
	v_ShowTimeFrameDialog = FALSE;
	v_Today = -1;
	v_TodayFrom = _T("");
	v_TodayTo = _T("");
	v_TomorrowFrom = _T("");
	v_TomorrowTo = _T("");
	v_UrnoVisible = FALSE;
	v_MasterWhere = _T("");
	v_MasterTable = _T("");
	m_BasicData = FALSE;
	m_FormCb = _T("");
	//}}AFX_DATA_INIT
	v_AsFrameWindow         = pomGridDef->AsFrameWindow      ;
	v_AsChoiceList          = pomGridDef->AsChoiceList       ;
	v_DataSource            = pomGridDef->DataSource         ;
	v_Filter                = pomGridDef->Filter             ;
	v_GridName              = pomGridDef->GridName           ;
	v_HasViewButton         = pomGridDef->HasViewButton      ;
	v_HeaderSize            = pomGridDef->HeaderSize         ;
	v_IsEditable            = pomGridDef->IsEditable         ;
	v_MultiSelect           = pomGridDef->MultiSelect        ;
	v_Printable             = pomGridDef->Printable          ;
	v_ReturnField           = pomGridDef->ReturnField        ;
	v_SensitiveForChanges   = pomGridDef->SensitiveForChanges;
	v_SetDefaultTimeFrame   = pomGridDef->SetDefaultTimeFrame;
	v_ShowTimeFrameDialog   = pomGridDef->ShowTimeFrameDialog;
	v_Today                 = pomGridDef->Today              ;
	v_TodayFrom             = pomGridDef->TodayFrom          ;
	v_TodayTo               = pomGridDef->TodayTo            ;
	v_TomorrowFrom          = pomGridDef->TomorrowFrom       ;
	v_TomorrowTo            = pomGridDef->TomorrowTo         ;


	for(int i = 0; i < pomGridDef->omTables.GetSize(); i++)
	{
		omTableMap.SetAt(pomGridDef->omTables[i], (void*&)pomGridDef->omTables[i]);
	}
	
	imColCount = 0;
	omTabPropertyGrid.EnableAutoGrow(false);
	omPropertyGrid.EnableAutoGrow(false);
}


void GridWizzard1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GridWizzard1)
	DDX_Control(pDX, IDC_BASICDATA, m_BasicDataCheck);
	DDX_Control(pDX, IDC_FORM_CB, m_FormsCombo);
	DDX_Control(pDX, IDC_MASTERTAB_STATIC, m_MasterTabStatic);
	DDX_Control(pDX, IDC_MASTER_TABLE, m_MasterTable);
	DDX_Control(pDX, IDC_MASTER_WHERE, m_MasterWhere);
	DDX_Control(pDX, IDC_SOURCEFIELDS, m_SourceFields);
	DDX_Control(pDX, IDC_TABLES, m_Tables);
	DDX_Control(pDX, IDC_RETURNSTATIC, m_ReturnLabel);
	DDX_Control(pDX, IDC_URNO_VISIBLE, m_UrnoVisible);
	DDX_Control(pDX, IDC_TOMORROW_TO, m_TomorrowTo);
	DDX_Control(pDX, IDC_TOMORROW_FROM, m_TomorrowFrom);
	DDX_Control(pDX, IDC_TODAY_TO, m_TodayTo);
	DDX_Control(pDX, IDC_TODAY_FROM, m_TodayFrom);
	DDX_Control(pDX, IDC_TODAY, m_Today);
	DDX_Control(pDX, IDC_SHOW_TIMEFRAME_DIALOG, m_ShowTimeFrameDialog);
	DDX_Control(pDX, IDC_SET_DEFAULT_TIMEFRAM, m_SetDefaultTimeFrame);
	DDX_Control(pDX, IDC_SENSITIV_FOR_DATACHANGES, m_SensitiveForChanges);
	DDX_Control(pDX, IDC_SAVE_USERLAYOUT, m_SaveUserLayout);
	DDX_Control(pDX, IDC_RETURN_FIELD_CB, m_ReturnField);
	DDX_Control(pDX, IDC_PRINTABLE, m_IsPrintable);
	DDX_Control(pDX, IDC_MULTISELECT, m_MultiSelect);
	DDX_Control(pDX, IDC_IS_EDITABLE, m_IsEditable);
	DDX_Control(pDX, IDC_HEADER_SIZE, m_HeaderSize);
	DDX_Control(pDX, IDC_HAS_VIEW_BUTTON, m_HasViewButton);
	DDX_Control(pDX, IDC_GRID_NAME, m_GridName);
	DDX_Control(pDX, IDC_ASCHOICELIST, m_AsChoiceList);
	DDX_Control(pDX, IDC_AS_FRAMEWINDOW, m_AsFrameWindow);
	DDX_Check(pDX, IDC_AS_FRAMEWINDOW, v_AsFrameWindow);
	DDX_Check(pDX, IDC_ASCHOICELIST, v_AsChoiceList);
	DDX_Text(pDX, IDC_GRID_NAME, v_GridName);
	DDX_Check(pDX, IDC_HAS_VIEW_BUTTON, v_HasViewButton);
	DDX_Check(pDX, IDC_HEADER_SIZE, v_HeaderSize);
	DDX_Check(pDX, IDC_IS_EDITABLE, v_IsEditable);
	DDX_Check(pDX, IDC_MULTISELECT, v_MultiSelect);
	DDX_Check(pDX, IDC_PRINTABLE, v_Printable);
	DDX_CBString(pDX, IDC_RETURN_FIELD_CB, v_ReturnField);
	DDX_Check(pDX, IDC_SAVE_USERLAYOUT, v_SaveUserLayout);
	DDX_Check(pDX, IDC_SENSITIV_FOR_DATACHANGES, v_SensitiveForChanges);
	DDX_Check(pDX, IDC_SET_DEFAULT_TIMEFRAM, v_SetDefaultTimeFrame);
	DDX_Check(pDX, IDC_SHOW_TIMEFRAME_DIALOG, v_ShowTimeFrameDialog);
	DDX_Radio(pDX, IDC_TODAY, v_Today);
	DDX_Text(pDX, IDC_TODAY_FROM, v_TodayFrom);
	DDX_Text(pDX, IDC_TODAY_TO, v_TodayTo);
	DDX_Text(pDX, IDC_TOMORROW_FROM, v_TomorrowFrom);
	DDX_Text(pDX, IDC_TOMORROW_TO, v_TomorrowTo);
	DDX_Check(pDX, IDC_URNO_VISIBLE, v_UrnoVisible);
	DDX_Text(pDX, IDC_MASTER_WHERE, v_MasterWhere);
	DDX_CBString(pDX, IDC_MASTER_TABLE, v_MasterTable);
	DDX_Check(pDX, IDC_BASICDATA, m_BasicData);
	DDX_CBString(pDX, IDC_FORM_CB, m_FormCb);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GridWizzard1, CDialog)
	//{{AFX_MSG_MAP(GridWizzard1)
	ON_BN_CLICKED(IDC_CREATE, OnCreate)
	ON_BN_CLICKED(IDC_CREATE_AND_START, OnCreateAndStart)
	ON_BN_CLICKED(IDC_MULTITOLEFT, OnMultitoleft)
	ON_BN_CLICKED(IDC_MULTITORIGHT, OnMultitoright)
	ON_BN_CLICKED(IDC_SINGLETOLEFT, OnSingletoleft)
	ON_BN_CLICKED(IDC_SINGLETORIGHT, OnSingletoright)
	ON_CBN_SELCHANGE(IDC_TABLES, OnSelchangeTables)
	ON_BN_CLICKED(IDC_SET_DEFAULT_TIMEFRAM, OnSetDefaultTimefram)
	ON_BN_CLICKED(IDC_SHOW_TIMEFRAME_DIALOG, OnShowTimeframeDialog)
	ON_BN_CLICKED(IDC_TODAY, OnToday)
	ON_BN_CLICKED(IDC_TOMORROW, OnTomorrow)
	ON_CBN_SELCHANGE(IDC_MASTER_TABLE, OnSelchangeMasterTable)
	ON_BN_CLICKED(IDC_BASICDATA, OnBasicdata)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GridWizzard1 message handlers

BOOL GridWizzard1::OnInitDialog() 
{
	CDialog::OnInitDialog();

	


	m_SourceFields.SetFont(&ogCourier_Regular_8);
	POSITION pos;

	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		m_Tables.AddString(olKey);
	}

	m_FormsCombo.ResetContent();
	for(int i = 0; i < ogForms.GetSize(); i++)
	{
		m_FormsCombo.AddString(ogForms[i].omName);
	}
	m_FormsCombo.SetWindowText(pomGridDef->FormName);
	if(pomGridDef->AsBasicData == TRUE)
	{
		m_BasicDataCheck.SetCheck(1);
	}
	else
	{
		m_BasicDataCheck.SetCheck(0);
	}

//Fields Grid
	CString olHeader;
	int ilRowCount = pomGridDef->omGridFields.GetSize();
	olHeader = LoadStg(IDS_STRING61454);
	omPropertyGrid.SubclassDlgItem(IDC_FIELD_PROPERTIES, this);
	omPropertyGrid.Initialize();
	omPropertyGrid.GetParam()->EnableUndo(FALSE);
	omPropertyGrid.LockUpdate(TRUE);
	CGXStyle olHeaderStyle;
	CStringArray olHeaderList;
	ExtractItemList(olHeader, &olHeaderList, ',');
	omPropertyGrid.SetRowCount(ilRowCount);
	imColCount = olHeaderList.GetSize();
	omPropertyGrid.SetColCount(imColCount);

	CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(&omPropertyGrid);
	pWnd->Create(WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL | CBS_SORT, 0);
	pWnd->m_bFillWithChoiceList = TRUE;
	pWnd->m_bWantArrowKeys = TRUE;
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);

	CGXCheckBox *polCeckBox = new CGXCheckBox((CGXGridCore*)(&omPropertyGrid), TRUE, GX_IDB_CHECK_95);
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CHECKBOX3D, polCeckBox);

//Table,Field,Type,Header,Visible,Key,Query
	omPropertyGrid.SetColWidth(1,1,40);
	omPropertyGrid.SetColWidth(2,2,40);
	omPropertyGrid.SetColWidth(3,3,40);
	omPropertyGrid.SetColWidth(4,4,160);
	omPropertyGrid.SetColWidth(5,5,40);
	omPropertyGrid.SetColWidth(6,6,60);
	omPropertyGrid.SetColWidth(7,7,60);
	omPropertyGrid.SetColWidth(8,8,160);

	for( i = 0; i < olHeaderList.GetSize(); i++)
	{
		olHeaderStyle.SetValue(olHeaderList[i]);
		omPropertyGrid.SetStyleRange(CGXRange(0, i+1, 0, i+1 ), olHeaderStyle);
	}

	CString olChoiceList;
	for( i = 0; i < ogQueries.GetSize(); i++)
	{
		olChoiceList += ogQueries[i].omName + CString("\n");
	}
	for(i = 0; i < pomGridDef->omGridFields.GetSize(); i++)
	{
		omPropertyGrid.SetValueRange(CGXRange(i+1, 1), pomGridDef->omGridFields[i].Table);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 2), pomGridDef->omGridFields[i].Field);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 3), pomGridDef->omGridFields[i].Type);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 4), pomGridDef->omGridFields[i].Header);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 5), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 7), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		CString olCheck = pomGridDef->omGridFields[i].Visible;
		omPropertyGrid.SetValueRange(CGXRange(i+1, 5), olCheck);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 6), pomGridDef->omGridFields[i].Key);
		olCheck = pomGridDef->omGridFields[i].Unique;
		omPropertyGrid.SetValueRange(CGXRange(i+1, 7), olCheck);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 8), CGXStyle().SetHorizontalAlignment(DT_LEFT)
												.SetVerticalAlignment(DT_BOTTOM)
												.SetControl(GX_IDS_CTRL_COMBOBOX)
												.SetChoiceList(olChoiceList));
		omPropertyGrid.SetValueRange(CGXRange(i+1, 8), pomGridDef->omGridFields[i].QueryName);
	}
	CString olTypes;
	olTypes = LoadStg(IDS_FIELD_TYPES);


	omPropertyGrid.LockUpdate(FALSE);
	omPropertyGrid.GetParam()->EnableUndo(TRUE);


	if(v_AsChoiceList == TRUE)
	{
		m_ReturnField.ShowWindow(SW_SHOWNORMAL);
		m_ReturnLabel.ShowWindow(SW_SHOWNORMAL);
	}
	else
	{
		m_ReturnField.ShowWindow(SW_HIDE);
		m_ReturnLabel.ShowWindow(SW_HIDE);
		m_ReturnField.SetWindowText("");
	}

// Table Property Grid
	omTabPropertyGrid.SubclassDlgItem(IDC_TABLE_PROPERTIES, this);

	omTabPropertyGrid.Initialize();
	omTabPropertyGrid.GetParam()->EnableUndo(FALSE);
	omTabPropertyGrid.LockUpdate(TRUE);

	ilRowCount = pomGridDef->omTables.GetSize();
	olHeaderList.RemoveAll();
	olHeader = LoadStg(IDS_STRING61456);
	ExtractItemList(olHeader, &olHeaderList, ',');
	omTabPropertyGrid.SetRowCount(ilRowCount);
	imColCount = olHeaderList.GetSize();
	omTabPropertyGrid.SetColCount(imColCount);


	//Table,Field,Type,Header,Visible,Key,Query
	omTabPropertyGrid.SetColWidth(0,1,20);
	omTabPropertyGrid.SetColWidth(1,1,70);
	omTabPropertyGrid.SetColWidth(2,2,60);

	for( i = 0; i < olHeaderList.GetSize(); i++)
	{
		olHeaderStyle.SetValue(olHeaderList[i]);
		omTabPropertyGrid.SetStyleRange(CGXRange(0, i+1, 0, i+1 ), olHeaderStyle);
	}

	for(i = 0; i < pomGridDef->omTables.GetSize(); i++)
	{
		omTabPropertyGrid.SetValueRange(CGXRange(i+1, 1), pomGridDef->omTables[i]);
		omTabPropertyGrid.SetValueRange(CGXRange(i+1, 2), ""); //For the moment	
	}

	omTabPropertyGrid.LockUpdate(FALSE);
	m_FormCb = pomGridDef->FormName;
	m_BasicData = pomGridDef->AsBasicData;


	UpdateData(FALSE);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GridWizzard1::OnCreate() 
{
	if(GetValues() == true)
	{
		EndDialog(10);
	}
}

bool GridWizzard1::GetValues()
{
	if(UpdateData() == FALSE)
		return false;

	if(v_GridName.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_GridName.SetFocus();	
		return false;
	}

	pomGridDef->FormName = m_FormCb;
	pomGridDef->AsBasicData = m_BasicData;

 	pomGridDef->AsFrameWindow      = v_AsFrameWindow         ;
	pomGridDef->AsChoiceList       = v_AsChoiceList          ;
	pomGridDef->DataSource         = v_DataSource            ;
	pomGridDef->Filter             = v_Filter                ;
	pomGridDef->GridName           = v_GridName              ;
	pomGridDef->HasViewButton      = v_HasViewButton         ;
	pomGridDef->HeaderSize         = v_HeaderSize            ;
	pomGridDef->IsEditable         = v_IsEditable            ;
	pomGridDef->MultiSelect        = v_MultiSelect           ;
	pomGridDef->Printable          = v_Printable             ;
	pomGridDef->ReturnField        = v_ReturnField           ;
	pomGridDef->SensitiveForChanges= v_SensitiveForChanges   ;
	pomGridDef->SetDefaultTimeFrame= v_SetDefaultTimeFrame   ;
	pomGridDef->ShowTimeFrameDialog= v_ShowTimeFrameDialog   ;
	pomGridDef->Today              = v_Today                 ;
	pomGridDef->TodayFrom          = v_TodayFrom             ;
	pomGridDef->TodayTo            = v_TodayTo               ;
	pomGridDef->TomorrowFrom       = v_TomorrowFrom          ;
	pomGridDef->TomorrowTo         = v_TomorrowTo            ;


	int ilRows = omPropertyGrid.GetRowCount();
	pomGridDef->omGridFields.DeleteAll();
	CString olTabField;
	int ilRealToReadRows = 0;
	for(int i = 0; i < ilRows; i++)
	{
		UGridFields *polGridFields = new UGridFields();
		polGridFields->Table =		omPropertyGrid.GetValueRowCol(i+1, 1);
		polGridFields->Field =		omPropertyGrid.GetValueRowCol(i+1, 2);
		polGridFields->Type =		omPropertyGrid.GetValueRowCol(i+1, 3);
		polGridFields->Header =		omPropertyGrid.GetValueRowCol(i+1, 4);
		polGridFields->Visible = omPropertyGrid.GetValueRowCol(i+1, 5);
		polGridFields->Key =		omPropertyGrid.GetValueRowCol(i+1, 6);
		polGridFields->Unique = omPropertyGrid.GetValueRowCol(i+1, 7);
		polGridFields->QueryName =	omPropertyGrid.GetValueRowCol(i+1, 8);
		pomGridDef->omGridFields.Add(polGridFields);
		olTabField = polGridFields->Table + CString(".") + polGridFields->Field;
		ogTools.AddFieldToDefaultSet(NULL, olTabField );
		ilRealToReadRows++;
		if(!polGridFields->Key.IsEmpty())
		{
			ilRealToReadRows++;
			ogTools.AddFieldToDefaultSet(NULL, polGridFields->Key );
		}
	}
	for(i = pomGridDef->omSortColumns.GetSize()-1; i >= 0; i--)
	{
		pomGridDef->omSortColumns.RemoveAll();
	}
	int ilTabRows = omTabPropertyGrid.GetRowCount();
	pomGridDef->omTables.RemoveAll();
	for(i = 0; i < ilTabRows; i++)
	{
		CString olV = omTabPropertyGrid.GetValueRowCol(i+1, 1); 
		if(!olV.IsEmpty())
		{
			pomGridDef->omTables.Add(olV);
		}
	}

	return true;
}

void GridWizzard1::OnCreateAndStart() 
{
	if(GetValues() == true)
	{
		EndDialog(11);
	}
}

void GridWizzard1::OnMultitoleft() 
{
}

void GridWizzard1::OnMultitoright() 
{
	BOOL bOld = omPropertyGrid.LockUpdate();
	

	int ilCount = m_SourceFields.GetCount();
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		for(int i = 0; i < ilCount; i++)
		{
			CString olField;
			m_SourceFields.GetText(i, olField);
			int ilRowNo = omPropertyGrid.GetRowCount( )+1;
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("0"));

			//Check for existing table
			int ilTabRows = omTabPropertyGrid.GetRowCount();
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < ilTabRows); j++)
			{
				if( olTable == omTabPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if( blFound == false)
			{
				//omTabPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
				omTabPropertyGrid.InsertRows(omTabPropertyGrid.GetRowCount( )+1, 1);
				omTabPropertyGrid.SetColCount(2);

				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 1), olTable);
				//omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 2), "");
			}
		}
	}
	CString olChoiceList;
	for(int i = 0; i < ogQueries.GetSize(); i++)
	{
		olChoiceList += ogQueries[i].omName + CString("\n");
	}
	omPropertyGrid.SetStyleRange(CGXRange(1, 8, omPropertyGrid.GetRowCount( ), 8), CGXStyle().SetHorizontalAlignment(DT_LEFT)
											.SetVerticalAlignment(DT_BOTTOM)
											.SetControl(GX_IDS_CTRL_COMBOBOX)
											.SetChoiceList(olChoiceList));

	m_SourceFields.ResetContent();
	omPropertyGrid.LockUpdate(bOld);
	omPropertyGrid.Redraw();
}

void GridWizzard1::OnSingletoleft() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	CRowColArray olRows;

	omPropertyGrid.GetSelectedRows(olRows);
	for(int i = olRows.GetSize()-1; i >= 0; i--)
	//while(olRows.GetSize() > 0)
	{
		if(olRows[i] != 0)
		{
			CString olTab, olField;
			olTab = omPropertyGrid.GetValueRowCol(olRows[i], 1);
			olField = omPropertyGrid.GetValueRowCol(olRows[i], 2);
			if(olTable == olTab)
			{
				m_SourceFields.AddString(olField);
			}
			omPropertyGrid.RemoveRows(olRows[i], olRows[i]);
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < (int)omPropertyGrid.GetRowCount()); j++)
			{
				CString olT = omPropertyGrid.GetValueRowCol(j+1, 1);
				if(olTab == omPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if(blFound == false)
			{
				int ilTabRows = omTabPropertyGrid.GetRowCount();
				bool blTabFound = false;
				int  ilLine;
				for(int j = 0; (blTabFound == false) && (j < ilTabRows); j++)
				{
					CString olV = omTabPropertyGrid.GetValueRowCol(j+1, 1); 
					if( olTab == omTabPropertyGrid.GetValueRowCol(j+1, 1))
					{
						ilLine = j+1;
						blTabFound = true;
					}
				}
				if(blTabFound == true)
				{
					omTabPropertyGrid.RemoveRows(ilLine, ilLine);

				}
			}
		}
	}

}

void GridWizzard1::OnSingletoright() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		int ilIdx = m_SourceFields.GetCurSel();	
		if(ilIdx != LB_ERR)
		{
			CString olField;
			m_SourceFields.GetText(ilIdx, olField);
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			CString olChoiceList;
			for(int i = 0; i < ogQueries.GetSize(); i++)
			{
				olChoiceList += ogQueries[i].omName + CString("\n");
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);

			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("0"));
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), CGXStyle().SetHorizontalAlignment(DT_LEFT)
													.SetVerticalAlignment(DT_BOTTOM)
													.SetControl(GX_IDS_CTRL_COMBOBOX)
													.SetChoiceList(olChoiceList)/*olOldStyle*/);
			m_SourceFields.DeleteString(ilIdx);
			int ilTabRows = omTabPropertyGrid.GetRowCount();
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < ilTabRows); j++)
			{
				if( olTable == omTabPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if( blFound == false)
			{
				omTabPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 1), olTable);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 2), "");
			}
		}
	}
}

void GridWizzard1::OnSelchangeTables() 
{
	int ilIdx = m_Tables.GetCurSel();	
	m_SourceFields.ResetContent();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_Tables.GetLBText(ilIdx, olText);
		CCSPtrArray<RecordSet> olRecords;

		ogBCD.GetRecords("SYS", "TANA", olText, &olRecords);
		for(int i = 0; i < olRecords.GetSize(); i++)
		{
			m_SourceFields.AddString(olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")]);
		}
		olRecords.DeleteAll();
	}
}

void GridWizzard1::OnSetDefaultTimefram()  
{
	// TODO: Add your control notification handler code here
	
}

void GridWizzard1::OnShowTimeframeDialog() 
{
	// TODO: Add your control notification handler code here
	
}

void GridWizzard1::OnToday() 
{
	// TODO: Add your control notification handler code here
	
}

void GridWizzard1::OnTomorrow() 
{
	// TODO: Add your control notification handler code here
	
}

void GridWizzard1::OnSelchangeMasterTable() 
{
	// TODO: Add your control notification handler code here
	
}

void GridWizzard1::OnBasicdata() 
{
	// TODO: Add your control notification handler code here
	
}
