//{{AFX_INCLUDES()
#include "aatedit5.h"
//}}AFX_INCLUDES
#if !defined(AFX_FORMDIALOGDLG_H__1042F831_E840_11D3_A26E_00500437F607__INCLUDED_)
#define AFX_FORMDIALOGDLG_H__1042F831_E840_11D3_A26E_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormDialogDlg.h : header file
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Runtime Form dialog
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo AAT/ID		Initial version
 *		15/11/00	cla AAT/ID		Insertion of code into script eng. added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "CCSGlobl.h"
#include "CtrlProperties.h"
#include "RunTimeCtrl.h"
#include "CCSEdit.h"
#include "GxGridTab.h"
#include "UFISGridViewerDoc.h"
#include "CedaBasicData.h"
#include "MiniGridInformation.h"

/////////////////////////////////////////////////////////////////////////////
// FormDialogDlg dialog

class MiniGridInformation;
class FormDialogDlg : public CDialog
{
// Construction
public:
	FormDialogDlg(CWnd* pParent, CString opMode, UFormProperties *popForm, CString opKey, UGridDefinition *popGridDef);   // standard constructor

	~FormDialogDlg();
	UGridDefinition *pomGridDef;
// Dialog Data
	//{{AFX_DATA(FormDialogDlg)
	enum { IDD = IDD_DLG_EXECUTE_DLG };
	//}}AFX_DATA

	CString omMode;
	int imCurrentRow; // for selection of row RButton ==> popup menu and preparation for deletion
	UFormProperties *pomForm;
	CUFISGridViewerDoc *pomDoc;
	RecordSet	omCurrentRecord;
	typedef CTypedPtrList<CObList, RunTimeCtrl*> URuntimeControls;

	CCSPtrArray<MiniGridInformation> omMiniGrids;
	URuntimeControls omControls;

	UINT imID;

	CString omKey;		// Should be Unique Record Number
	CToolBar    m_wndToolBar; //IDR_NAVIGATOR_BAR

	CBrush omBrush;

//	void SetCCSEditTpye(CCSEdit *popEdit, UCtrlProperties *popProperty);
	void SetCCSEditTpye(CAatedit5 *popEdit, UCtrlProperties *popProperty);
	bool InsertFormData();
	bool UpdateFormData();
	void DeleteFormData();
	void FillGrid(RunTimeCtrl *polCtrl, UGridCtrlProperties *popGridProperties, RecordSet &ropRecord);
	void FillControl(RunTimeCtrl *popCtrl, RecordSet &ropRecord);
	void ShowData();
	bool HasChildFields(RunTimeCtrl *popCtrl);
	bool MakeMasterValue(RunTimeCtrl *popChild, RecordSet &ropRecord, CString &ropErrorText);
	void MakeMasterChild(RunTimeCtrl *popChild, RecordSet &ropRecord);

	bool MakeDBValue(UCtrlProperties *popProperty, CString &ropValue, CString &ropErrorText);
	void MakeEditValue(RunTimeCtrl *popCtrl, RecordSet &ropRecord);
	bool InjectCodeIntoScriptHost(CString formName);

	// for inserts and updates:
	// if an DB-error occurs the user needs an understandable message
	CString PrepareUniqueConstraintMessage(CString opDBMessage);
	// searches all child which are connected to opMasterName
	// return: amount of hits
	int  GetAllChildsForMaster(CString opMasterName, CCSPtrArray<RunTimeCtrl> &ropChilds); 

	RunTimeCtrl *GetCtrlByName(CString opName);
	UGridCtrlProperties *GetGrid(GxGridTab *popGrid);

	bool IsLineOk(int opRow);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormDialogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FormDialogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void DeleteGridLine();
	afx_msg LONG ChangedGridCell(UINT wParam, LONG lParam);
	afx_msg LONG ClickedGridRButton(UINT wParam, LONG lParam);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMDIALOGDLG_H__1042F831_E840_11D3_A26E_00500437F607__INCLUDED_)
