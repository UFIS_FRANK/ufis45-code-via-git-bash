// UFISGridViewerDoc.cpp : implementation of the CUFISGridViewerDoc class
//

#include "stdafx.h"
#include "UWorkBench.h"

#include "UFISGridViewerDoc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//Selected columns for sorting
CUIntArray	CUFISGridViewerDoc::omSelectedColumns;

//Broadcast processing functionality
static void ProcessBroadCastCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

void  ProcessBroadCastCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CUFISGridViewerDoc *)popInstance)->ProcessBroacast(ipDDXType, vpDataPointer,ropInstanceName);
}

void  CUFISGridViewerDoc::ProcessBroacast(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	int ilI = 0;
	int ilUrnoIdx;
	CString olUrno;
	RecordSet olRecord;
	olRecord = *((RecordSet*)vpDataPointer);
	int ilURT, ilDRT, ilIRT;
	if(pomGridDef != NULL)
	{
		if(pomGridDef->omTables.GetSize() > 0)
		{
			if(ogTools.GetDDXTypesForTable(pomGridDef->omTables[0], ilURT, ilDRT, ilIRT) == true)
			{
				if(ipDDXType == ilIRT)
				{
					ilUrnoIdx = ogBCD.GetFieldIndex(pomGridDef->omTables[0], "URNO");
					if(ilUrnoIdx > -1)
					{
						int ilLineNo;
						olUrno = olRecord.Values[ilUrnoIdx];
						ilLineNo = GetLineNoByUrno(olUrno);
						if(ilLineNo != -1)
						{
							UpdateLine(ilLineNo, olUrno);
							UpdateAllViews(NULL);
						}
						else 
						{
							InsertLine(olUrno);
						}
					}
				}
				if(ipDDXType == ilURT)
				{
					ilUrnoIdx = ogBCD.GetFieldIndex(pomGridDef->omTables[0], "URNO");
					if(ilUrnoIdx > -1)
					{
						int ilLineNo;
						olUrno = olRecord.Values[ilUrnoIdx];
						ilLineNo = GetLineNoByUrno(olUrno);
						if(ilLineNo != -1)
						{
							UpdateLine(ilLineNo, olUrno);
							UpdateAllViews(NULL);
						}
					}
				}
				if(ipDDXType == ilDRT)
				{
					ilUrnoIdx = ogBCD.GetFieldIndex(pomGridDef->omTables[0], "URNO");
					if(ilUrnoIdx > -1)
					{
						int ilLineNo;
						olUrno = olRecord.Values[ilUrnoIdx];
						ilLineNo = GetLineNoByUrno(olUrno);
						if(ilLineNo != -1)
						{
							omData.DeleteAt(ilLineNo);
							UpdateAllViews(NULL);
						}
					}
				}
			}
		}
	}
}



static int CompareLinesAsc(const GRID_LINE_DATA **e1, const GRID_LINE_DATA **e2)
{
	for(int i = 0; i < CUFISGridViewerDoc::omSelectedColumns.GetSize(); i++)
	{
		int ilCompareResult = 0;

		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] == (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = 0;
		}
		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] > (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = 1;
		}
		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] < (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = -1;
		}
		if (ilCompareResult != 0)
			return ilCompareResult;
	}
	return 0;
}


static int CompareLinesDsc(const GRID_LINE_DATA **e1, const GRID_LINE_DATA **e2)
{
	for(int i = 0; i < CUFISGridViewerDoc::omSelectedColumns.GetSize(); i++)
	{
		int ilCompareResult = 0;

		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] == (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = 0;
		}
		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] < (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = 1;
		}
		if( (**e1).Values[CUFISGridViewerDoc::omSelectedColumns[i]] > (**e2).Values[CUFISGridViewerDoc::omSelectedColumns[i]])
		{
			ilCompareResult = -1;
		}
		if (ilCompareResult != 0)
			return ilCompareResult;
	}
	return 0;
}


/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerDoc

IMPLEMENT_DYNCREATE(CUFISGridViewerDoc, CDocument)

BEGIN_MESSAGE_MAP(CUFISGridViewerDoc, CDocument)
	//{{AFX_MSG_MAP(CUFISGridViewerDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerDoc construction/destruction

CUFISGridViewerDoc::CUFISGridViewerDoc()
{
	pomGridDef= NULL;
	imColumnCount = 0;
}

CUFISGridViewerDoc::~CUFISGridViewerDoc()
{
	for(int i = omFieldInfo.GetSize() - 1; i >= 0; i--)
	{
		omFieldInfo[i].Fields.DeleteAll();
	}
	omData.DeleteAll();
	omFieldInfo.DeleteAll();
	int ilURT, ilDRT, ilIRT;
	if(ogTools.GetDDXTypesForTable(pomGridDef->omTables[0], ilURT, ilDRT, ilIRT) == true)
	{
		ogDdx.UnRegister((void *)this,NOTUSED);
	}

//	omFieldInfo.DeleteAll();
}


BOOL CUFISGridViewerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)


	return TRUE;
}

BOOL CUFISGridViewerDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
	pomGridDef = (UGridDefinition *)lpszPathName;
	SetDefinitionInfo(pomGridDef);
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

void CUFISGridViewerDoc::SetGridDef(UGridDefinition *popGridDef)
{
	pomGridDef = popGridDef; 
//	UpdateAllViews(NULL);
	if(pomGridDef != NULL)
	{
		SetTitle(pomGridDef->GridName);
		//TO DO unterst�zung mehrerer Tabellen
		CString olTable;
		CString olFields;
		for(int i = 0; i < pomGridDef->omGridFields.GetSize(); i++)
		{
			if(i == 0)
			{
				olTable = pomGridDef->omGridFields[i].Table;
				olFields += pomGridDef->omGridFields[i].Field;
			}
			else
			{
				olFields += CString(",") + pomGridDef->omGridFields[i].Field;
			}

			// wiel das feld, dem ein Query zugeordnet ist derzeit nicht angezeigt wird
			if(pomGridDef->omGridFields[i].QueryName.IsEmpty())
			{
				omFieldsList.Add(olTable + CString(".") + pomGridDef->omGridFields[i].Field);
			}
			if(!pomGridDef->omGridFields[i].Key.IsEmpty())
			{
				omFieldsList.Add(pomGridDef->omGridFields[i].Key);
			}
			omHeaderList.Add(pomGridDef->omGridFields[i].Header);
		}
		if(!olTable.IsEmpty() && !olFields.IsEmpty())
		{
			if(olTable != "SYS")
			{
				omTable = olTable;
				ogBCD.SetObject(omTable, olFields);
				ogBCD.SetObjectDesc(omTable, omTable);
				//ogBCD.AddKeyMap("SYS", "TANA");
  				ogBCD.Read(omTable);
			}
		}
		UpdateAllViews(NULL);
	}
}

void CUFISGridViewerDoc::SetDefinitionInfo(UGridDefinition *popGridDef, bool bpWithRead /*= true*/)
{
	pomGridDef = popGridDef;
	omData.DeleteAll();

	if(bpWithRead == true)
	{
		omColumnTypeList.RemoveAll();
		for(int i = omFieldInfo.GetSize() - 1; i >= 0; i--)
		{
			omFieldInfo[i].Fields.DeleteAll();
		}
		omData.DeleteAll();
		omFieldInfo.DeleteAll();

		if(pomGridDef != NULL)
		{
			SetTitle(pomGridDef->GridName);
			for(int i = 0; i < pomGridDef->omGridFields.GetSize(); i++)
			{
				GRID_FIELD_INFO *polInfo = GetFieldInfo(pomGridDef->omGridFields[i].Table);
				CString olTabField;
				olTabField = pomGridDef->omGridFields[i].Table;
				if(polInfo == NULL)
				{
					polInfo = new GRID_FIELD_INFO;
					omFieldInfo.Add(polInfo);

				}
				olTabField += CString(".") + pomGridDef->omGridFields[i].Field;
				void *p;
				if(omFieldMap.Lookup(olTabField,(void *&)p) == FALSE)
				{
					GRID_FIELD_DETAIL_INFO *polFldDetail = new GRID_FIELD_DETAIL_INFO;
					polFldDetail->Table = pomGridDef->omGridFields[i].Table;
					polFldDetail->Field = pomGridDef->omGridFields[i].Field;
					polFldDetail->Reference =pomGridDef->omGridFields[i].Key;
					polInfo->Table = pomGridDef->omGridFields[i].Table;

					if(!pomGridDef->omGridFields[i].QueryName.IsEmpty())
					{
						polInfo->Show.Add("N");
						polInfo->Query.Add(pomGridDef->omGridFields[i].QueryName);
						polInfo->Type = pomGridDef->omGridFields[i].Type;
						CreateFieldDetailByQuery(polFldDetail, pomGridDef->omGridFields[i].QueryName);
					} 
					else
					{
						omFieldsList.Add(olTabField);
						omHeaderList.Add(pomGridDef->omGridFields[i].Header);
						omVisibleList.Add(pomGridDef->omGridFields[i].Visible);
						polInfo->Show.Add("Y");
						polInfo->Query.Add(CString(" "));
						polInfo->Type = pomGridDef->omGridFields[i].Type;
						polFldDetail->FieldTypes.Add(polInfo->Type);
						omColumnTypeList.Add(polInfo->Type);
						CString dbgHeader = pomGridDef->omGridFields[i].Header;
						polFldDetail->HeaderToShow.Add(pomGridDef->omGridFields[i].Header);
					}
					polInfo->Fields.Add(polFldDetail);
					omFieldMap.SetAt(olTabField, (void *&)polFldDetail);
					if(!pomGridDef->omGridFields[i].Key.IsEmpty())
					{
						MakeReferenceTabInfo(pomGridDef->omGridFields[i].Key, pomGridDef->omGridFields[i].Visible);
					}
				}
				omFieldMap.SetAt(olTabField, (void *&)olTabField);

	//Den evtl. angegebenen Query mitber�cksichtigen
				if(!pomGridDef->omGridFields[i].QueryName.IsEmpty())
				{
					for(int j = 0; j < ogQueries.GetSize(); j++)
					{
						if(ogQueries[j].omName == pomGridDef->omGridFields[i].QueryName)
						{
							CString olRelTab, olRelField;
							MakeTabAndFieldFromQuery(&ogQueries[j]);
						}
					}
				}
			}
			if(bpWithRead == true)
			{
 				ReadAllData();
			}
			//MakeLines();
			if(pomGridDef->omTables.GetSize() > 0)
			{
				CString olT = pomGridDef->omTables[0];
				GRID_FIELD_INFO *polTab = GetFieldInfo(pomGridDef->omTables[0]);
				if(polTab != NULL)
				{
					CStringArray olFieldList; 
					GetMakeLines(polTab, olFieldList, omData, CString(), CString(), NULL, false);
				}
				//Set the ddxtypes for the current table in the main grid
				int ilURT, ilDRT, ilIRT;
				if(ogTools.GetDDXTypesForTable(polTab->Table, ilURT, ilDRT, ilIRT) == false)
				{
					ogTools.SetDDXTypesForTable(polTab->Table);
					//this ensures that the ddxtypes are intialized
					ogTools.GetDDXTypesForTable(polTab->Table, ilURT, ilDRT, ilIRT);

				}
				ogBCD.SetDdxType(polTab->Table, "IRT", ilIRT);
				ogBCD.SetDdxType(polTab->Table, "URT", ilURT);
				ogBCD.SetDdxType(polTab->Table, "DRT", ilDRT);
				ogDdx.Register((void *)this,ilIRT, CString("IRT"), CString("Table irt"),ProcessBroadCastCf);
				ogDdx.Register((void *)this,ilURT, CString("URT"), CString("Table urt"),ProcessBroadCastCf);
				ogDdx.Register((void *)this,ilDRT, CString("DRT"), CString("Table drt"),ProcessBroadCastCf);

			}
			omSelectedColumns.RemoveAll();
			for( i = 0; i < pomGridDef->omSortColumns.GetSize(); i++)
			{
				omSelectedColumns.Add(pomGridDef->omSortColumns[i]);
			}
			SortImmediate(pomGridDef->omSortDirection);

	//		UpdateAllViews(NULL);
		}
	}
	else
	{
		if(pomGridDef->omTables.GetSize() > 0)
		{
			CString olT = pomGridDef->omTables[0];
			GRID_FIELD_INFO *polTab = GetFieldInfo(pomGridDef->omTables[0]);
			if(polTab != NULL)
			{
				CStringArray olFieldList; 
				GetMakeLines(polTab, olFieldList, omData, CString(), CString(), NULL, false);
			}

		}
		omSelectedColumns.RemoveAll();
		for( int i = 0; i < pomGridDef->omSortColumns.GetSize(); i++)
		{
			omSelectedColumns.Add(pomGridDef->omSortColumns[i]);
		}
		SortImmediate(pomGridDef->omSortDirection);
	}
}	


void CUFISGridViewerDoc::UpdateLine(int ipLineNo, CString opUrno)
{
	if(pomGridDef->AsBasicData == TRUE)
	{
		if(omData.GetSize() > ipLineNo && ipLineNo >= 0)
		{
			RecordSet olRec;
			if(ogBCD.GetRecord(pomGridDef->omTables[0], "URNO", opUrno, olRec))
			{
				GRID_FIELD_INFO *polTab = GetFieldInfo(pomGridDef->omTables[0]);
				if(polTab != NULL)
				{
					for(int i = 0; i < polTab->Fields.GetSize(); i++)
					{
						CString olTable = polTab->Table;
						CString olField = polTab->Fields[i].Field;
						CString olTabField = olTable + CString(".") + olField;
						CString olValue;
						int ilDBIdx = ogBCD.GetFieldIndex(olTable, olField);
						if(ilDBIdx != -1)
						{
							int ilGridCol = GetFieldIndex(olTabField);
							if(ilGridCol != -1)
							{
								CString olValue = olRec.Values[ilDBIdx];
								int ilIdx2 = GetFieldIndex(olTabField);
								if(ilIdx2 != -1)
								{
									omData[ipLineNo].Values[ilIdx2] = olValue;
								}
							}
						}
					}
				}
				UpdateAllViews(NULL);
			}
		}
	}
	else
	{
		// TO DO for complex tables
	}
}


void CUFISGridViewerDoc::InsertLine(CString opUrno)
{
	if(pomGridDef->AsBasicData == TRUE)
	{
		RecordSet olRec;
		if(ogBCD.GetRecord(pomGridDef->omTables[0], "URNO", opUrno, olRec))
		{
			GRID_FIELD_INFO *polTab = GetFieldInfo(pomGridDef->omTables[0]);
			if(polTab != NULL)
			{
				GRID_LINE_DATA *polNewLine = new GRID_LINE_DATA();
				CString olEmptyString = CString("");
				int ilSize = omFieldsList.GetSize();
				for(int i = 0; i < ilSize; i++)
				{
					polNewLine->Values.Add(olEmptyString);
				}
				for(i = 0; i < polTab->Fields.GetSize(); i++)
				{
					CString olTable = polTab->Table;
					CString olField = polTab->Fields[i].Field;
					CString olTabField = olTable + CString(".") + olField;
					CString olValue;
					int ilDBIdx = ogBCD.GetFieldIndex(olTable, olField);
					if(ilDBIdx != -1)
					{
						int ilGridCol = GetFieldIndex(olTabField);
						if(ilGridCol != -1)
						{
							CString olValue = olRec.Values[ilDBIdx];
							int ilIdx2 = GetFieldIndex(olTabField);
							if(ilIdx2 != -1)
							{
								polNewLine->Values[ilIdx2] = olValue;
							}
						}
					}
				}
				omData.Add(polNewLine);
				UpdateAllViews(NULL);
			}
		}
	}
	else
	{
		// TO DO for complex tables
	}
}

void CUFISGridViewerDoc::DeleteLine(int ipLineNo, CString opUrno)
{
	if(!opUrno.IsEmpty())
	{
		if(omData.GetSize() > ipLineNo && ipLineNo >= 0)
		{
			CString olMessage;
			//TO DO Language
			olMessage = CString("Do you really want to delete:\n\n");
			for(int i = 0; (i < omData[ipLineNo].Values.GetSize() && i < 5); i++)
			{
				olMessage += omData[ipLineNo].Values[i] + CString("\n");
			}
			if(AfxMessageBox(olMessage, (MB_YESNOCANCEL|MB_ICONQUESTION)) == IDYES)
			{
				if(ogBCD.DeleteRecord(pomGridDef->omTables[0], "URNO", opUrno, true) == true)
				{
					omData.DeleteAt(ipLineNo);
					UpdateAllViews(NULL);
				}
			}
		}
	}
}

void CUFISGridViewerDoc::MakeFieldInfo()
{
	if(pomGridDef != NULL)
	{
	}
}
void CUFISGridViewerDoc::ReadAllData()
{
	for(int i = 0; i < omFieldInfo.GetSize(); i++)
	{
		CString olTable, olFields;
		olTable = omFieldInfo[i].Table;
		if(ogTools.IsTableReaded(olTable) == false)
		{
			if(imColumnCount < omFieldInfo[i].Fields.GetSize())
			{
				imColumnCount = omFieldInfo[i].Fields.GetSize();
			}
			for(int j = 0; j < omFieldInfo[i].Fields.GetSize(); j++)
			{
				if(j == 0)
				{
					olFields += omFieldInfo[i].Fields[j].Field;
				}
				else
				{
					olFields += CString(",") + omFieldInfo[i].Fields[j].Field;
				}
			}
			if(!olTable.IsEmpty() && !olFields.IsEmpty())
			{
				if(olTable != "SYS")
				{
					ogBCD.SetObject(olTable, olFields);
					ogBCD.SetObjectDesc(olTable, olTable);
  					ogBCD.Read(olTable);
				}
			}
		}
	}
}
int CUFISGridViewerDoc::MakeLines(CStringArray opTables)
{
	
	return omData.GetSize();
}

//********************** Hammerharter Versuch

int CUFISGridViewerDoc::GetMakeLines(GRID_FIELD_INFO *popTab, 
				 CStringArray &ropFieldList, 
				 CCSPtrArray<GRID_LINE_DATA> &opData, 
				 CString opRefField, 
				 CString opRefValue,
				 GRID_LINE_DATA *popLine, bool bpIsInRecursion)
{
	int ilLineCount = 0;
	int ilRecursionReturn = -1;
	bool blReturn = false;
	bool blIsInRecursion = false;
	CCSPtrArray<GRID_LINE_DATA> olCurrentGridLines;
	CCSPtrArray<GRID_LINE_DATA> olData;
	CCSPtrArray<RecordSet> olRecords;
	bool blWasInRecursion = false;

	if(opRefField.IsEmpty() && opRefField.IsEmpty())
	{ 
		int ilDataCount = ogBCD.GetDataCount(popTab->Table);
		for(int i = 0; i < ilDataCount; i++)
		{ 
			RecordSet olRecord;
			ogBCD.GetRecord(popTab->Table, i,  olRecord);
			olRecords.NewAt(olRecords.GetSize(), olRecord);
		}
	}
	else
	{
		ogBCD.GetRecords(popTab->Table, opRefField, opRefValue, &olRecords);
	}

	for(int ilRecCount = 0; ilRecCount < olRecords.GetSize(); ilRecCount++)
	{
		ilRecursionReturn = -1;
		GRID_LINE_DATA *polCurrentLine;// = new GRID_LINE_DATA;
		if(bpIsInRecursion == true)
		{
			if(ilRecCount == 0)
			{
				polCurrentLine = popLine;
			}
			else
			{
				polCurrentLine = new GRID_LINE_DATA;
				CString olEmptyString = CString("");
				int ilSize = omFieldsList.GetSize();
				for(int i = 0; i < ilSize; i++)
				{ 
					polCurrentLine->Values.Add(olEmptyString);
				}
				*polCurrentLine = *popLine;
				opData.Add(polCurrentLine);
			}
		}
		else
		{
			polCurrentLine = new GRID_LINE_DATA;
			CString olEmptyString = CString("");
			int ilSize = omFieldsList.GetSize();
			for(int i = 0; i < ilSize; i++)
			{
				polCurrentLine->Values.Add(olEmptyString);
			}
			olCurrentGridLines.Add(polCurrentLine);
		}
		for(int i = 0; i < popTab->Fields.GetSize(); i++)
		{
			//...Felder in "polCurrentLine" f�llen 
				
			if((i+1) == popTab->Fields.GetSize())
			{
				//blWasLastField = true;
			}
			CString olTable = popTab->Table;
			CString olField = popTab->Fields[i].Field;
			CString olTabField = olTable + CString(".") + olField;
			CString olValue;
			int ilDBIdx = ogBCD.GetFieldIndex(olTable, olField);
			if(ilDBIdx != -1)
			{
				if(ilRecCount == 0)
				{
					ropFieldList.Add(olTabField);
				}
				olValue = olRecords[ilRecCount][ilDBIdx];
				int ilIdx2 = GetFieldIndex(olTabField);
				if(ilIdx2 != -1)
				{
					polCurrentLine->Values[ilIdx2] = olValue;
					blReturn = true;
				}
			}//if(ilIdx2 != -1)
			if(!popTab->Fields[i].Reference.IsEmpty())
			{
				CString olRefTab = GetTableName(popTab->Fields[i].Reference);
				CString olRefField = GetFieldName(popTab->Fields[i].Reference);
				GRID_FIELD_INFO *polRefTab = GetFieldInfo(olRefTab);
				
				if(polRefTab != NULL)
				{
					CStringArray olFieldList;
					ilRecursionReturn = GetMakeLines(polRefTab, olFieldList, opData, olRefField, olValue, polCurrentLine, true/*blIsInRecursion*/);
					blWasInRecursion = true;
				}
			}
		}
		if(/*blWasInRecursion */bpIsInRecursion == false)
		{
			if(olCurrentGridLines.GetSize() > 1)
			{
				for(int j = 0; j < olCurrentGridLines.GetSize(); j++)
				{
					opData.NewAt(opData.GetSize(), olCurrentGridLines[j]);
				}
				olCurrentGridLines.DeleteAll();
			}

			else
			{
				//if(bpIsInRecursion == false)
				{
					opData.NewAt(opData.GetSize(), *polCurrentLine);
					olCurrentGridLines.DeleteAll();
				}
			}
		}
	}
	olCurrentGridLines.DeleteAll();
	olRecords.DeleteAll();
	return opData.GetSize();
}

void CUFISGridViewerDoc::AppendLineArray(int ipAddLineCount, 
										 GRID_LINE_DATA *polCurrentLine, 
										 CCSPtrArray<GRID_LINE_DATA> &ropCurrentGridLines)
{
	if(ipAddLineCount <= 0)
	{
		return;
	}
	for(int i = 0; i < ipAddLineCount; i++)
	{
		GRID_LINE_DATA *polNewLine = new GRID_LINE_DATA;
		int ilSize = omFieldsList.GetSize();
		//polNewLine->Values.SetSize(ilSize);
		CString olEmptyString = CString("");
		for(int j = 0; j < ilSize; j++)
		{
			polCurrentLine->Values.Add(olEmptyString);
		}
		for( j = 0; j < polCurrentLine->Values.GetSize(); j++)
		{
			polNewLine->Values.Add(polCurrentLine->Values[j]);
		}
		ropCurrentGridLines.Add(polNewLine);
	}
}
//********************** Hammerharter Versuch Ende



void CUFISGridViewerDoc::MakeLines()
{
	omFieldsInVisibleOrder.RemoveAll();
	imColumnCount = 0;
	omHeaderList.RemoveAll();
	int ilColumnCount = 0;
	for(int i = 0; i < omFieldInfo.GetSize(); i++)
	{
		if(omFieldInfo[i].IsMasterTable == true)
		{
			//GRID_LINE_DATA *polLineData = new GRID_LINE_DATA;
			int ilDataCount = ogBCD.GetDataCount(omFieldInfo[i].Table);
			for(int j = 0; j < ilDataCount; j++)
			{
				RecordSet olRecord;
				GRID_LINE_DATA *polLineData = new GRID_LINE_DATA;
				ogBCD.GetRecord(omFieldInfo[i].Table, j,  olRecord);
				ilColumnCount=0;
				for(int k = 0; k < omFieldInfo[i].Fields.GetSize(); k++)
				{
					CString olValue;
					CString olType;
					CString dgbTab = omFieldInfo[i].Table;
					CString dbgField = omFieldInfo[i].Fields[k].Field;
					if(omFieldInfo[i].Fields[k].Relations.GetSize() > 0)
					{
						CString olLastRefValue;
						CString olNewRefValue;
						CString olOrgFieldValue;
						int ilOrgIdx= ogBCD.GetFieldIndex(omFieldInfo[i].Table, omFieldInfo[i].Fields[k].Field);
						if(ilOrgIdx != -1)
						{
							olOrgFieldValue = olRecord[ilOrgIdx];
							olNewRefValue = olOrgFieldValue; 
						}
						int ilRelationCount = omFieldInfo[i].Fields[k].Relations.GetSize();
						for(int l = 0; l < ilRelationCount; l++)
						{
							CString olCurrLeftTable;
							CString olCurrLeftField;
							CString olCurrRightTable;
							CString olCurrRightField;
							CString olNextLeftTable;
							CString olNextLeftField;
							CString olNextRightTable;
							CString olNextRightField;
							CString olResultRelValue;
							CString olCurrLeftValue = olNewRefValue;
							//For next relation
							if( l+1 < ilRelationCount)
							{
								CString olTmpRel = GetLeftRelationPart(omFieldInfo[i].Fields[k].Relations[l+1]);
								olNextLeftTable  = GetTableName(olTmpRel);
								olNextLeftField  = GetFieldName(olTmpRel);
								olTmpRel = GetRightRelationPart(omFieldInfo[i].Fields[k].Relations[l+1]);
								olNextRightTable  = GetTableName(olTmpRel);
								olNextRightField  = GetFieldName(olTmpRel);
							}
							//For current relation
							CString olTmpRel = GetLeftRelationPart(omFieldInfo[i].Fields[k].Relations[l]);
							olCurrLeftTable  = GetTableName(olTmpRel);
							olCurrLeftField  = GetFieldName(olTmpRel);
							olTmpRel = GetRightRelationPart(omFieldInfo[i].Fields[k].Relations[l]);
							olCurrRightTable  = GetTableName(olTmpRel);
							olCurrRightField  = GetFieldName(olTmpRel);

							if(!olCurrRightTable.IsEmpty() && !olCurrRightField.IsEmpty())
							{
								if(!olNewRefValue.IsEmpty())
								{
									CCSPtrArray<RecordSet> olRefRecords;
									ogBCD.GetRecords(olCurrRightTable, olCurrRightField, olNewRefValue, &olRefRecords);
//										ogBCD.GetRecords(olCurrRightTable, olCurrRightField, olNewRefValue, &olRefRecords);
									if(!olNextLeftTable.IsEmpty() && !olNextRightField.IsEmpty())
									{
										int ilRelIdx= ogBCD.GetFieldIndex(olNextLeftTable, olNextLeftField);
										olLastRefValue = olNewRefValue;
										if((ilRelIdx != -1) && (olRefRecords.GetSize() > 0))
										{
											olNewRefValue = olRefRecords[0][ilRelIdx];
										}
									}
									else
									{
										for(int m = 0; m < omFieldInfo[i].Fields[k].FieldsToShow.GetSize(); m++)
										{
											if((olRefRecords.GetSize() > 0) && (omFieldInfo[i].Fields[k].FieldsToShow[m].Find(olCurrRightTable) != -1))
											{
												omFieldsInVisibleOrder.Add(omFieldInfo[i].Fields[k].FieldsToShow[m]);	
//													omColumnTypeList.Add(omFieldInfo[i].Fields[k].FieldTypes[m]);
												CString olTmpFld = GetFieldName(omFieldInfo[i].Fields[k].FieldsToShow[m]);
												int ilFldIdx = ogBCD.GetFieldIndex(olCurrRightTable, olTmpFld);
												if(ilFldIdx != -1)
												{
													polLineData->Values.Add(olRefRecords[0][ilFldIdx]);
													ilColumnCount++;
													if(imColumnCount < ilColumnCount)
													{
														imColumnCount = ilColumnCount;
													}
													if(m < omFieldInfo[i].Fields[k].HeaderToShow.GetSize())
													{
														omHeaderList.Add(omFieldInfo[i].Fields[k].HeaderToShow[m]);
													}
													else
													{
														omHeaderList.Add("???");
													}
												}
											}
											else
											{
												//No data has been found in referece
												polLineData->Values.Add(CString(""));
//													omColumnTypeList.Add("TRIM");
												ilColumnCount++;
												if(imColumnCount < ilColumnCount)
												{
													imColumnCount = ilColumnCount;
												}
												if(m < omFieldInfo[i].Fields[k].HeaderToShow.GetSize())
												{
													omHeaderList.Add(omFieldInfo[i].Fields[k].HeaderToShow[m]);
												}
												else
												{
													omHeaderList.Add("???");
												}
											}
										}
									}
									olRefRecords.DeleteAll();
								}//if(!olNewRefValue.IsEmpty())
							}//if(!olRelTab.IsEmpty() && !olRelField.IsEmpty())
						}//for(int l = 0; l < omFieldInfo[i].Fields[k].Relations.GetSize(); l++)
					}//if(omFieldInfo[i].Fields[k].Relations.GetSize() > 0)
					else
					{
						int ilIdx= ogBCD.GetFieldIndex(omFieldInfo[i].Table, omFieldInfo[i].Fields[k].Field);
						if(ilIdx != -1)
						{
							omFieldsInVisibleOrder.Add(omFieldInfo[i].Table+CString(".")+omFieldInfo[i].Fields[k].Field);
							if(omFieldInfo[i].Fields[k].FieldTypes.GetSize() > 0)
							{
//								omColumnTypeList.Add(omFieldInfo[i].Fields[k].FieldTypes[0]);
							}
							else
							{
//								omColumnTypeList.Add("TRIM");
							}
							CString Tab = omFieldInfo[i].Table;
							CString Field = omFieldInfo[i].Fields[k].Field;
							olValue = olRecord[ilIdx];
							polLineData->Values.Add(olValue);
							ilColumnCount++;
							if(imColumnCount < ilColumnCount)
							{
								imColumnCount = ilColumnCount;
							}
							if(omHeaderList.GetSize() <= imColumnCount )
							{
								if(omFieldInfo[i].Fields[k].HeaderToShow.GetSize() > 0)
								{
									CString olHeader  = omFieldInfo[i].Fields[k].HeaderToShow[0];
									omHeaderList.Add(omFieldInfo[i].Fields[k].HeaderToShow[0]);
								}
								else
								{
									omHeaderList.Add("???");
								}
							}
						}
					}// else of if(omFieldInfo[i].Fields[k].Relations.GetSize() > 0)
				}//for( k = 0; K < omFieldInfo[i].Fields.GetSize()-1; k++)
				omData.Add(polLineData);
			}//for(int j = 0; j < ilDataCount; j++)
		}//if(omFieldInfo[i].IsMasterTable == true)
	}//for(int i = 0; i < omFieldInfo.GetSize(); i++)
}
void CUFISGridViewerDoc::MakeLine()
{
}


GRID_FIELD_INFO *CUFISGridViewerDoc::GetFieldInfo(CString opTable)
{
	for(int i = 0; i < omFieldInfo.GetSize(); i++)
	{
		CString olT = omFieldInfo[i].Table;
		if(omFieldInfo[i].Table == opTable)
		{
			return &omFieldInfo[i];
		}
	}
	return NULL;
}

void CUFISGridViewerDoc::CreateFieldDetailByQuery(GRID_FIELD_DETAIL_INFO* polFieldDetail, CString opQueryName)
{
//	CStringArray Relations;		// 1. DEM.URUD=RUD.URNO 2.RUD.URUE=RUE.URNO
//	CStringArray FieldsToShow;	// 1. RUE.RUSN (= Rule Shortname) 2. RUE.RUNA (= Rulename) 3.RUD.RETY (= Rule demand resource type)
//	CStringArray HeaderToShow;  // Read from systab RUE.RUSN ==> Addi
	for(int i = 0; i < ogQueries.GetSize(); i++)
	{
		if(ogQueries[i].omName == opQueryName)
		{
			// 1. Relations
			for(int j = 0; j < ogQueries[i].omObjRelation.GetSize(); j++)
			{
				CString olRel = ogQueries[i].omObjRelation[j];
				polFieldDetail->Relations.Add(ogQueries[i].omObjRelation[j]);
			}

			// 2. Fields to show und tableheader
			for(j = 0; j < ogQueries[i].omQueryDetails.GetSize(); j++)
			{
				CString olTab, olFld;
				olTab = ogQueries[i].omQueryDetails[j].Object;
				olFld = ogQueries[i].omQueryDetails[j].Field;
				if(!olTab.IsEmpty() && !olFld.IsEmpty())
				{
					omFieldsList.Add(olTab+CString(".")+olFld);
					CString olHeader = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTab, olFld, "ADDI");
					polFieldDetail->HeaderToShow.Add(olHeader);
					omHeaderList.Add(olHeader);

					CString olType = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTab, olFld, "TYPE");
					polFieldDetail->FieldTypes.Add(olType);
					omColumnTypeList.Add(olType);

					polFieldDetail->FieldsToShow.Add(olTab+CString(".")+olFld);
				}
			}//for(j = 0; j < ogQueries[i].omQueryDetails.GetSize(); j++)	
		}//if(ogQueries[i].omName == opQueryName)
	}//for(int i = 0; i < ogQueries.GetSize(); i++)
}

void CUFISGridViewerDoc::MakeReferenceTabInfo(CString opRef, CString opVisible)
{
	CString olTable = GetTableName(opRef);
	CString olField = GetFieldName(opRef);
	if(olTable.IsEmpty() || olField.IsEmpty())
	{
		return;
	}
	GRID_FIELD_INFO *polInfo = GetFieldInfo(olTable);


	if(polInfo == NULL)
	{
		polInfo = new GRID_FIELD_INFO;
		omFieldInfo.Add(polInfo);
	}

	CString olTabField = olTable + CString(".") + olField;
	void *p;
	if(omFieldMap.Lookup(olTabField,(void *&)p) == FALSE)
	{
		//polInfo->IsMasterTable=true;

		GRID_FIELD_DETAIL_INFO *polFldDetail = new GRID_FIELD_DETAIL_INFO;
		polInfo->Table = olTable;
		polFldDetail->Table = olTable;
		polFldDetail->Field = olField;
		omFieldsList.Add(olTabField);


		polInfo->Show.Add("Y");
		polInfo->Query.Add(CString(""));
		polInfo->Type = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "TYPE");
		polFldDetail->FieldTypes.Add(polInfo->Type);

		omColumnTypeList.Add(polInfo->Type);

		CString olHeader = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "ADDI");

		polFldDetail->HeaderToShow.Add(olHeader);
		omHeaderList.Add(olHeader);
		omVisibleList.Add(opVisible);

		polInfo->Fields.Add(polFldDetail);
		
		omFieldMap.SetAt(olTabField, (void *&)olTabField);
//		omFieldInfo.Add(polInfo);
	}
}

void CUFISGridViewerDoc::MakeTabAndFieldFromQuery(UQuery *popQuery)
{
	if(popQuery != NULL)
	{
		// 1st part is for relation
		for(int i = 0; i < popQuery->omObjRelation.GetSize(); i++)
		{
			CStringArray olItems;
			ExtractItemList(popQuery->omObjRelation[i], &olItems, '=');
			if(olItems.GetSize() == 2)
			{
				for(int j = 0; j < olItems.GetSize(); j++)
				{
					CStringArray olSubItems;
					ExtractItemList(olItems[j], &olSubItems, '.');
					CString olTab, olFld;
					olTab = olSubItems[0];
					olFld = olSubItems[1];
					CString olTabFld = olTab + CString(".") + olFld;
					void *p;
					if(omFieldMap.Lookup(olTabFld,(void *&)p) == FALSE)
					{
						GRID_FIELD_INFO *polInfo = GetFieldInfo(olTab);
						GRID_FIELD_DETAIL_INFO *polFldDetail = new GRID_FIELD_DETAIL_INFO;
						

						polFldDetail->Table = olTab;
						polFldDetail->Field = olFld;
						if(polInfo != NULL)
						{
							omFieldMap.SetAt(olTabFld, (void *&)olTabFld);
							polInfo->Fields.Add(polFldDetail);
						}
						else
						{
							polInfo = new GRID_FIELD_INFO;
							polInfo->IsMasterTable=false;
							polInfo->Table = olTab;
							polInfo->Fields.Add(polFldDetail);
							
							omFieldMap.SetAt(olTabFld, (void *&)olTabFld);
							omFieldInfo.Add(polInfo);
						}
					}//if(omFieldMap.Lookup(olTabFld,(void *&)p) == FALSE)				
				}//for(int j = 0; j < olItems.GetSize(); j++)
			}//if(olItems.GetSize() == 2)
		}//for(int i = 0; i < popQuery->omObjRelation.GetSize(); i++)

		//2nd part is for querydetails
		for(i = 0; i < popQuery->omQueryDetails.GetSize(); i++)
		{
			CString olTab, olFld;
			olTab = popQuery->omQueryDetails[i].Object;
			olFld = popQuery->omQueryDetails[i].Field;
			if(!olTab.IsEmpty() && !olFld.IsEmpty())
			{
				CString olTabFld = olTab + CString(".") + olFld;
				void *p;
				if(omFieldMap.Lookup(olTabFld,(void *&)p) == FALSE)
				{
					GRID_FIELD_INFO *polInfo = GetFieldInfo(olTab);
					GRID_FIELD_DETAIL_INFO *polFldDetail = new GRID_FIELD_DETAIL_INFO;
					polInfo->Table = olTab;

					polFldDetail->Table = olTab;
					polFldDetail->Field = olFld;
					if(polInfo != NULL)
					{
						omFieldMap.SetAt(olTabFld, (void *&)olTabFld);
						polInfo->Fields.Add(polFldDetail);
					}
					else
					{
						polInfo = new GRID_FIELD_INFO;
						polInfo->IsMasterTable=false;
						polInfo->Table = olTab;
						polInfo->Fields.Add(polFldDetail);
						
						omFieldMap.SetAt(olTabFld, (void *&)olTabFld);
						omFieldInfo.Add(polInfo);
					}
				}				
			}
		}
	}//if(popQuery != NULL)
}

/**********************************************
Helper functions
***********************************************/
 
//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns AFT
CString CUFISGridViewerDoc::GetTableName(CString opObject)
{
	CString olRet;
	if(opObject.GetLength() != 8)
	{
		return CString();
	}
	int ilIdx = opObject.Find('.');
	if(ilIdx != -1)
	{
		olRet = opObject.Left(3);
	}
	return olRet;
}

//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns STOA
CString CUFISGridViewerDoc::GetFieldName(CString opObject)
{
	CString olRet;
	if(opObject.GetLength() != 8)
	{
		return CString();
	}
	int ilIdx = opObject.Find('.');
	if(ilIdx != -1)
	{
		olRet = opObject.Right(4);
	}
	return olRet;
}

CString CUFISGridViewerDoc::GetLeftRelationPart(CString opRelation)
{
	CString olRet;
	int ilIdx = opRelation.Find("=");
	if(ilIdx != -1)
	{
		olRet = opRelation.Left(ilIdx);
	}
	return olRet;
}

CString CUFISGridViewerDoc::GetRightRelationPart(CString opRelation)
{
	CString olRet;
	int ilIdx = opRelation.Find("=");
	if(ilIdx != -1)
	{
		olRet = opRelation.Right(opRelation.GetLength() - (ilIdx+1));
	}
	return olRet;
}


void CUFISGridViewerDoc::SortImmediate(CString opSortOrder)
{
	if(opSortOrder == "ASC")
	{
		omData.Sort(CompareLinesAsc);
	}
	else
	{
		omData.Sort(CompareLinesDsc);
	}
	UpdateAllViews(NULL);
}

int CUFISGridViewerDoc::GetFieldIndex(CString opField)
{
	for(int i = 0; i < omFieldsList.GetSize(); i++)
	{
		if(opField == omFieldsList[i])
		{
			return i;
		}
	}
	return -1;
}
/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerDoc serialization

void CUFISGridViewerDoc::Serialize(CArchive& ar)
{

}


/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerDoc diagnostics

#ifdef _DEBUG
void CUFISGridViewerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUFISGridViewerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerDoc commands
BOOL CUFISGridViewerDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	SetModifiedFlag(FALSE);
	return CDocument::CanCloseFrame(pFrame);
}

CString CUFISGridViewerDoc::GetMainTableUrnoFromLineNo(int ipLineNo)
{
	CString olUrno;
	if(ipLineNo >= 0)
	{
		if(omFieldInfo.GetSize() > 0)
		{
			if(omFieldInfo[0].Fields.GetSize() > 0)
			{
				CString olTmp;
				int ilIdx = -1;
				olTmp = omFieldInfo[0].Fields[0].Table + CString(".URNO");
				ilIdx = GetFieldIndex(olTmp);
				if(ilIdx > -1)
				{
					if(omData.GetSize() > ipLineNo)
					{
						if(omData[ipLineNo].Values.GetSize() > ilIdx)
						{
							return omData[ipLineNo].Values[ilIdx];
						}
					}
				}
			}
		}
	}
	return olUrno;
}

long CUFISGridViewerDoc::GetLineNoByUrno(CString opUrno)
{
	CString olUrno;
	int ilLineNo = -1;
	if(omFieldInfo.GetSize() > 0)
	{
		if(omFieldInfo[0].Fields.GetSize() > 0)
		{
			CString olTmp;
			int ilIdx = -1;
			olTmp = omFieldInfo[0].Fields[0].Table + CString(".URNO");
			ilIdx = GetFieldIndex(olTmp);
			if(ilIdx > -1)
			{
				for(int i = 0; i < omData.GetSize(); i++)
				{
					if(omData[i].Values.GetSize() > ilIdx)
					{
						if (omData[i].Values[ilIdx] == opUrno)
						{
							ilLineNo = i;
							i = omData.GetSize();
						}
					}
				}
			}
		}
	}
	return ilLineNo;
}

