// ComButtonSink.cpp : implementation file
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Button Sink class
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		02/11/00	cla AAT/IR		Initial version
 *		14/11/00	cla AAT/ID		adapted version for Workbench
 *									Scripthost, RunTimeCtrl ...
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
//#include "Script2.h"
//#include "Script2View.h"
//#include "Script2Doc.h"
#include "ComButtonSink.h"
#include <ATLCONV.H>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CComButtonSink

IMPLEMENT_DYNCREATE(CComButtonSink, CCmdTarget)

CComButtonSink::CComButtonSink() : m_ComButtonEventsAdvisor(IID_IComButtonSink)
{
//	m_pScript2View = NULL;
	EnableAutomation();
}

CComButtonSink::~CComButtonSink()
{
	this->Unadvise(IID_IComButtonSink);
	CCmdTarget::~CCmdTarget();
}


BEGIN_MESSAGE_MAP(CComButtonSink, CCmdTarget)
	//{{AFX_MSG_MAP(CComButtonSink)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/*----------------------------------------------------------------------------*/

BEGIN_DISPATCH_MAP(CComButtonSink, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CUFISAmSink)
	//DISP_FUNCTION(CComButtonSink, "Click", OnClick,VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CComButtonSink, "Click", 0xfffffda8, OnClick, VT_EMPTY, VTS_NONE)
  //}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

/*----------------------------------------------------------------------------*/

BEGIN_INTERFACE_MAP(CComButtonSink, CCmdTarget)
	INTERFACE_PART(CComButtonSink,IID_IComButtonSink,Dispatch)
END_INTERFACE_MAP()

/*----------------------------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////
// CComButtonSink message handlers

// ==================================================================
// 
// FUNCTION :  CComButtonSink::Advise()
// 
// * Description : Advise wrapper function
// 
// 
// * Author : [cla AAT/IR], Created : [02.11.00 10:39:42]
// 
// * Returns : [BOOL] - success or not
// 
// * Function parameters : 
// [pSource] - Pointer to the control
// [iid] - IID of the Control
// 
// ==================================================================
BOOL CComButtonSink::Advise(IUnknown* pSource,REFIID iid)
{
	// This GetInterface does not AddRef
	IUnknown* pUnknownSink = GetInterface(&IID_IUnknown);
	if (pUnknownSink == NULL)
	{
		return FALSE;
	}

	if (iid == IID_IComButtonSink)
	{
		return m_ComButtonEventsAdvisor.Advise(pUnknownSink, pSource);
	}
	else 
	{
		return FALSE;
	}
}

// ==================================================================
// 
// FUNCTION :  CComButtonSink::Unadvise()
// 
// * Description :
// 
// 
// * Author : [cla AAT/IR], Created : [02.11.00 10:42:01]
// 
// * Returns : [BOOL] - success or not
// 
// * Function parameters : 
// [iid] - IID of the interface
// 
// ==================================================================	
BOOL CComButtonSink::Unadvise(REFIID iid)
{
	if (iid == IID_IComButtonSink)
	{
		return m_ComButtonEventsAdvisor.Unadvise();
	}
	else 
	{
		return FALSE;
	}
}

/*----------------------------------------------------------------------------*/
/*
void CComButtonSink::SetLauncher(CScript2View* pScript2View)
{
	m_pScript2View = pScript2View;
}
*/

// ==================================================================
// 
// FUNCTION :  CComButtonSink::OnClick()
// 
// * Description :
// 
// 
// * Author : [cla AAT/IR], Created : [02.11.00 10:42:57]
// 
// * Returns : [void] - 
// 
// * Function parameters : 
// 
// ==================================================================
void CComButtonSink::OnClick()
{
//	_bstr_t vbMethod;
//	CString tmpMeth;

//	tmpMeth = m_cntlAlias;
//	tmpMeth += CLICK_EVENT_EXTENSION;
//	vbMethod = tmpMeth.GetBuffer(tmpMeth.GetLength());
	
//  Skip any functionality up to now
//	AfxMessageBox("OnClick received");
	DispatchEvent2VB(0xfffffda8);
//	m_scriptHost->pScriptControl->ExecuteStatement("MyButton.Caption = \"Clicked\"");
//	m_scriptHost->pScriptControl->ExecuteStatement("MyButton.Caption = Edit1.Text");
/*	try
	{ 
		m_scriptHost->pScriptControl->ExecuteStatement(vbMethod);
	}
	catch(_com_error & error)
	{
		AfxMessageBox(error.ErrorMessage());
	}
*/
//	m_scriptHost->pScriptControl->ExecuteStatement("Edit3.Text = CInt(Edit1.Text) + CInt(Edit2.Text)");
}

// ==================================================================
// 
// FUNCTION :  CComButtonSink::SetScriptHost()
// 
// * Description : Stores a pointer to the scriphost instance
// 
// 
// * Author : [cla AAT/ID], Created : [14.11.00 13:41:40]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [pScriptHost] -	pointer to script host
// 
// ==================================================================
void CComButtonSink::SetScriptHost(CScriptHost* pScriptHost)
{
	m_scriptHost = pScriptHost;
}

/*----------------------------------------------------------------------------*/

void CComButtonSink::SetAlias(CString& alias)
{
	m_cntlAlias = _T("On");
	m_cntlAlias += alias;
}

// ==================================================================
// 
// FUNCTION :  CComButtonSink::SetCtrl()
// 
// * Description : Stores a pointer to the associated control wrapper
// 
// 
// * Author : [cla AAT/ID], Created : [14.11.00 13:49:05]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [pRuntCtrl] -	pointer to runtime control
// 
// ==================================================================
void CComButtonSink::SetCtrl(RunTimeCtrl* pRuntCtrl)
{
	pomRunCtrl = pRuntCtrl;
}

// ==================================================================
// 
// FUNCTION :  CComButtonSink::DispatchEvent2VB()
// 
// * Description : Central dispatcher for all Events
// 
// 
// * Author : [cla AAT/ID], Created : [14.11.00 15:53:43]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [dispId] - dipid according to the handler
// 
// ==================================================================
void CComButtonSink::DispatchEvent2VB(DISPID dispId)
{
	int indx;
	_bstr_t olVbMethod;

	for(indx=0;indx<pomRunCtrl->pomProperties->omEventList.size();indx++)
	{
		if(pomRunCtrl->pomProperties->omEventList[indx] == dispId)
		{
			olVbMethod = 
			pomRunCtrl->pomProperties->omEventList[indx].m_vbMethodName.GetBuffer(
			pomRunCtrl->pomProperties->omEventList[indx].m_vbMethodName.GetLength());
			try
			{ 
				m_scriptHost->pScriptControl->ExecuteStatement(olVbMethod);
			}
			catch(_com_error & error)
			{
				AfxMessageBox(error.ErrorMessage());
			}
		}
	}
}