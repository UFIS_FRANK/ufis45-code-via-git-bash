// UCheckBox.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "UCheckBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UCheckBox

UCheckBox::UCheckBox(CWnd *popParent, COLORREF opBkColor, COLORREF opTextColor)
{
	pomParent = popParent;
	omBKColor = opBkColor;
	omTextColor = opTextColor;
}

UCheckBox::~UCheckBox()
{
}


BEGIN_MESSAGE_MAP(UCheckBox, CButton)
	//{{AFX_MSG_MAP(UCheckBox)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UCheckBox message handlers

void UCheckBox::OnClicked() 
{
	// TODO: Add your control notification handler code here
	pomParent->SendMessage(WM_UCHECKBOX_CLICKED, (UINT)this, 0L);
	
}

HBRUSH UCheckBox::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omBKColor);
	hbr = omBrush;	

	pDC->SetBkColor(omBKColor);
	pDC->SetTextColor(omTextColor);

	return hbr;
}

void UCheckBox::OnKillFocus(CWnd* pNewWnd) 
{
	CButton::OnKillFocus(pNewWnd);

	pomParent->SendMessage(WM_UCHECKBOX_KILLFOCUS, (UINT)this, 0L);
	// TODO: Add your message handler code here
	
}
void UCheckBox::SetBackColor(COLORREF opColor)
{
	omBKColor = opColor;
}

void UCheckBox::SetTxtColor(COLORREF opColor)
{
	omTextColor = opColor;
}


//********************************************************************************************
//********************************************************************************************
// RADIO BUTTON 
//********************************************************************************************
//********************************************************************************************

URadioButton::URadioButton(CWnd *popParent, COLORREF opBkColor, COLORREF opTextColor)
{
	pomParent = popParent;
	omBKColor = opBkColor;
	omTextColor = opTextColor;
}

URadioButton::~URadioButton()
{
}


BEGIN_MESSAGE_MAP(URadioButton, CButton)
	//{{AFX_MSG_MAP(URadioButton)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// URadioButton message handlers

void URadioButton::OnClicked() 
{
	// TODO: Add your control notification handler code here
	pomParent->SendMessage(WM_URADIOBUTTON_CLICKED, (UINT)this, 0L);
	
}

HBRUSH URadioButton::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omBKColor);
	hbr = omBrush;	

	pDC->SetBkColor(omBKColor);
	pDC->SetTextColor(omTextColor);

	return hbr;
}

void URadioButton::OnKillFocus(CWnd* pNewWnd) 
{
	CButton::OnKillFocus(pNewWnd);

	pomParent->SendMessage(WM_URADIOBUTTON_KILLFOCUS, (UINT)this, 0L);
	// TODO: Add your message handler code here
	
}
void URadioButton::SetBackColor(COLORREF opColor)
{
	omBKColor = opColor;
}

void URadioButton::SetTxtColor(COLORREF opColor)
{
	omTextColor = opColor;
}


//********************************************************************************************
//********************************************************************************************
// STATIC TEXT
//********************************************************************************************
//********************************************************************************************

UStatic::UStatic(CWnd *popParent, COLORREF opBkColor, COLORREF opTextColor)
{
	pomParent = popParent;
	omBKColor = opBkColor;
	omTextColor = opTextColor;
}

UStatic::~UStatic()
{
}


BEGIN_MESSAGE_MAP(UStatic, CStatic)
	//{{AFX_MSG_MAP(URadioButton)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// URadioButton message handlers


HBRUSH UStatic::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omBKColor);
	hbr = omBrush;	

	pDC->SetBkColor(omBKColor);
	pDC->SetTextColor(omTextColor);

	return hbr;
}

void UStatic::SetBackColor(COLORREF opColor)
{
	omBKColor = opColor;
}

void UStatic::SetTxtColor(COLORREF opColor)
{
	omTextColor = opColor;
}
