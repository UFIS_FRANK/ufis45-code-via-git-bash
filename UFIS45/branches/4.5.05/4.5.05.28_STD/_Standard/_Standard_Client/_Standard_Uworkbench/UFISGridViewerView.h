// UFISGridViewerView.h : interface of the CUFISGridViewerView class
//
/////////////////////////////////////////////////////////////////////////////
//const int gxnFirstCtrl = 101;
#ifndef __CUFISGridViewerView_H__
#define __CUFISGridViewerView_H__

class CUFISGridViewerView : public CGXGridView
{
protected: // create from serialization only
	CUFISGridViewerView();
	DECLARE_DYNCREATE(CUFISGridViewerView)

// Attributes
public:
	CUFISGridViewerDoc* GetDocument();

	int imCurrentSelected;
// Operations
public:
//added for initialization 
	void SetupUserAttributes();
#if _MFC_VER >= 0x0400
	CGXGridDropTarget m_objDndDropTarget;
#endif

	void SortAscending();
	void SortDescending();
	void SearchInGrid();
	void OnNew();
	void OnCopyRecord();
	void OnUpdate();
	void OnDelete();
	void OnViewFilter();

	BOOL OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	
	void OnGridClick(WPARAM wParam, LPARAM lParam);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUFISGridViewerView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, const CGXStyle* pStyle, GXModifyType mt, int nType);
	ROWCOL GetRowCount();
	virtual BOOL OnTrackColWidthEnd(ROWCOL nCol, int nWidth);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual void OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint );
	virtual BOOL OnTrackColWidth(ROWCOL nCol);
	virtual void OnTextNotFound(LPCTSTR);

	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUFISGridViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CUFISGridViewerView)
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in UFISGridViewerView.cpp
inline CUFISGridViewerDoc* CUFISGridViewerView::GetDocument()
   { return (CUFISGridViewerDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
#endif //__CUFISGridViewerView_H__
