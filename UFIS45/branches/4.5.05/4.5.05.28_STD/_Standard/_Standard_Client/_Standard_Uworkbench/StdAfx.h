// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__8EF75896_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_STDAFX_H__8EF75896_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls

#include <afxcview.h>       // MFC Common controls
#include "CJLibrary.h"

#include <afxrich.h>		// MFC rich edit classes
/*
 * cla: pragma added to avoid "identifier string 
 * exceeded the maximum allowable length and was truncated"
 * problem for the debugger
 */
#pragma warning( disable : 4786 )
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
#include <gxall.h>

#endif // !defined(AFX_STDAFX_H__8EF75896_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
