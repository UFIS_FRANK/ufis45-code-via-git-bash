// UWorkBenchDoc.cpp : implementation of the CUWorkBenchDoc class
//

#include "stdafx.h"
#include "UWorkBench.h"

#include "UWorkBenchDoc.h"
#include "QueryDesignerGridView.h"
#include "QueryChildFrame.h"
#include "UObjectDataDefinitions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchDoc

IMPLEMENT_DYNCREATE(CUWorkBenchDoc, CDocument)

BEGIN_MESSAGE_MAP(CUWorkBenchDoc, CDocument)
	//{{AFX_MSG_MAP(CUWorkBenchDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchDoc construction/destruction

CUWorkBenchDoc::CUWorkBenchDoc()
{
	// TODO: add one-time construction code here
	m_pParam = NULL;

}

CUWorkBenchDoc::~CUWorkBenchDoc()
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
}

void CUWorkBenchDoc::DeleteContents()
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
}


BOOL CUWorkBenchDoc::OnOpenDocument(LPCTSTR pszPathName)
{
	CGXGridParam* pParam = m_pParam;

	m_pParam = NULL;

	if (!CDocument::OnOpenDocument(pszPathName))
	{
		SetModifiedFlag(FALSE);
		m_pParam = pParam;
		return FALSE;
	}

	delete pParam;

	return TRUE;
}


void CUWorkBenchDoc::SetCurrentQueryDesignerTables(CStringArray &opTabs)
{
/*	omCurrentQueryDesingerTables.RemoveAll();
	int ilCount = opTabs.GetSize();
	if(pomCurrentQuery != NULL)
	{
		pomCurrentQuery->omUsedTables.RemoveAll();
		for(int i = 0; i < ilCount; i++)
		{
			omCurrentQueryDesingerTables.Add(opTabs[i]);
			pomCurrentQuery->omUsedTables.Add(opTabs[i]);
		}
	}
	SetModifiedFlag( TRUE );

	UpdateAllViews(NULL);
*/
}

BOOL CUWorkBenchDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	DeleteContents();
	SetModifiedFlag();
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchDoc serialization

void CUWorkBenchDoc::Serialize(CArchive& ar)
{
	static const WORD wVersion = 1;
	WORD wActualVersion = wVersion;

	ASSERT_VALID( this );

	if (ar.IsStoring())
	{
		ar << wVersion;
	}
	else
	{
		// Check for version first
		ar >> wActualVersion;
		if( wActualVersion > wVersion )
		{
#ifdef _DEBUG
			TRACE0( "Incompatible format while reading CGridDoc" );
			TRACE2("in %s at line %d\n", __FILE__, __LINE__);
			ASSERT(0);
			// ASSERTION-> Wrong version detected while reading object ->END
#endif
			AfxThrowArchiveException(CArchiveException::badSchema);
			return;
		}
	}

	if (ar.IsStoring())
	{
		// column settings, base styles
		ar << m_pParam;

	}
	else
	{
		// column settings, base styles
		ar >> m_pParam;

	}

}

BOOL CUWorkBenchDoc::OnSaveDocument(LPCTSTR pszPathName)
{
	// Ensure that each views current cell is stored
	CGXGridHint hint(gxHintTransferCurrentCell);
	hint.lParam = TRUE;

	UpdateAllViews(NULL, 0, &hint);

	// Now, you can save the document
	if (!CDocument::OnSaveDocument(pszPathName))
		return FALSE;

	SetModifiedFlag(FALSE);

	return TRUE;

}

BOOL CUWorkBenchDoc::CanCloseFrame(CFrameWnd* pFrame)
{

	// Ensure that views can be deactivated
	CView* pView = pFrame->GetActiveView();
	if (pView && pView->SendMessage(WM_GX_CANACTIVATE, 0, 0))
		return FALSE;

	// Is it a grid?
	CGXGridCore* pGrid = NULL;
	if (pView->IsKindOf(RUNTIME_CLASS(CGXGridView)))
		pGrid = (CGXGridCore*) ((CGXGridView*) pView);

	else if (pView->IsKindOf(RUNTIME_CLASS(CGXGridHandleView)))
		pGrid = ((CGXGridHandleView*) pView)->GetGrid();

	if (pGrid)
	{
		// Ensure that the current cell can be stored
		if (!pGrid->TransferCurrentCell(TRUE, GX_UPDATENOW, FALSE))
			// grid state is invalid, don't close the frame
			return FALSE;
	}

	// Now, we can close the view

//	return TRUE;
//	SetModifiedFlag(FALSE);
	return CDocument::CanCloseFrame(pFrame);
}

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchDoc diagnostics

#ifdef _DEBUG
void CUWorkBenchDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUWorkBenchDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchDoc commands
