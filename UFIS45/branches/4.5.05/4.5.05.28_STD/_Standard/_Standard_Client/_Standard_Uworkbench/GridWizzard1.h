#if !defined(AFX_GRIDWIZZARD1_H__47739061_5F7A_11D3_A1E2_0000B4984BBE__INCLUDED_)
#define AFX_GRIDWIZZARD1_H__47739061_5F7A_11D3_A1E2_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridWizzard1.h : header file
//

#include "GxGridTab.h"
/////////////////////////////////////////////////////////////////////////////
// GridWizzard1 dialog

class GridWizzard1 : public CDialog
{
// Construction
public:
	GridWizzard1(CWnd* pParent, UGridDefinition *popGridDef);   // standard constructor

	UGridDefinition *pomGridDef;

// Dialog Data
	//{{AFX_DATA(GridWizzard1)
	enum { IDD = IDD_GRIDWIZARD1 };
	CButton	m_BasicDataCheck;
	CComboBox	m_FormsCombo;
	CStatic	m_MasterTabStatic;
	CComboBox	m_MasterTable;
	CEdit	m_MasterWhere;
	CEdit	m_Name;
	CListBox	m_SourceFields;
	CComboBox	m_Tables;
	CStatic	m_ReturnLabel;
	CButton	m_UrnoVisible;
	CEdit	m_TomorrowTo;
	CEdit	m_TomorrowFrom;
	CEdit	m_TodayTo;
	CEdit	m_TodayFrom;
	CButton	m_Today;
	CButton	m_ShowTimeFrameDialog;
	CButton	m_SetDefaultTimeFrame;
	CStatic	m_SepaBmp2;
	CStatic	m_SepaBmp;
	CButton	m_SensitiveForChanges;
	CButton	m_SaveUserLayout;
	CComboBox	m_ReturnField;
	CButton	m_IsPrintable;
	CButton	m_MultiSelect;
	CButton	m_IsEditable;
	CButton	m_HeaderSize;
	CButton	m_HasViewButton;
	CComboBox	m_GroupBy;
	CEdit	m_GridName;
	CComboBox	m_Filter;
	CComboBox	m_DataSource;
	CButton	m_AsChoiceList;
	CButton	m_AsFrameWindow;
	BOOL	v_AsFrameWindow;
	BOOL	v_AsChoiceList;
	CString	v_DataSource;
	CString	v_Filter;
	CString	v_GridName;
	CString	v_GroupBy;
	BOOL	v_HasViewButton;
	BOOL	v_HeaderSize;
	BOOL	v_IsEditable;
	BOOL	v_MultiSelect;
	BOOL	v_Printable;
	CString	v_ReturnField;
	BOOL	v_SaveUserLayout;
	BOOL	v_SensitiveForChanges;
	BOOL	v_SetDefaultTimeFrame;
	BOOL	v_ShowTimeFrameDialog;
	int		v_Today;
	CString	v_TodayFrom;
	CString	v_TodayTo;
	CString	v_TomorrowFrom;
	CString	v_TomorrowTo;
	BOOL	v_UrnoVisible;
	CString	v_MasterWhere;
	CString	v_MasterTable;
	CString	v_TableRelation;
	BOOL	m_BasicData;
	CString	m_FormCb;
	//}}AFX_DATA


	GxGridTab omPropertyGrid;
	GxGridTab omTabPropertyGrid;
	int imColCount;


	CMapStringToPtr omTableMap;

	bool GetValues();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GridWizzard1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GridWizzard1)
	virtual BOOL OnInitDialog();
	afx_msg void OnCreate();
	afx_msg void OnCreateAndStart();
	afx_msg void OnMultitoleft();
	afx_msg void OnMultitoright();
	afx_msg void OnSingletoleft();
	afx_msg void OnSingletoright();
	afx_msg void OnSelchangeTables();
	afx_msg void OnSetDefaultTimefram();
	afx_msg void OnShowTimeframeDialog();
	afx_msg void OnToday();
	afx_msg void OnTomorrow();
	afx_msg void OnSelchangeMasterTable();
	afx_msg void OnBasicdata();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDWIZZARD1_H__47739061_5F7A_11D3_A1E2_0000B4984BBE__INCLUDED_)
