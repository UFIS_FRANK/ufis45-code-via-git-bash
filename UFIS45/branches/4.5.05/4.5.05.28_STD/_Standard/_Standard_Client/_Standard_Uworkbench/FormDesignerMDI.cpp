// FormDesignerMDI.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormDesignerMDI.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormDesignerMDI



IMPLEMENT_DYNCREATE(FormDesignerMDI, CMDIChildWnd)

FormDesignerMDI::FormDesignerMDI()
{
}

FormDesignerMDI::~FormDesignerMDI()
{
}


BEGIN_MESSAGE_MAP(FormDesignerMDI, CMDIChildWnd)
	//{{AFX_MSG_MAP(FormDesignerMDI)
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_COMMAND(ID_ARROW_CTRL,    OnArrow    )
	ON_COMMAND(ID_LABEL_CTRL,    OnLabel    )
	ON_COMMAND(ID_EDIT_CTRL,     OnEdit     )
	ON_COMMAND(ID_GROUP_CTRL,    OnGroup    )
	ON_COMMAND(ID_BUTTON_CTRL,   OnButton   )
	ON_COMMAND(ID_COMBOBOX_CTRL, OnCombobox )
	ON_COMMAND(ID_LISTBOX_CTRL,  OnListbox  )
	ON_COMMAND(ID_RADIO_CTRL,    OnRadio    )
	ON_COMMAND(ID_CHECKBOX_CTRL, OnCheckbox )
	ON_COMMAND(ID_LINE_CTRL,		OnLine		)
	ON_COMMAND(ID_RECT_CTRL,		OnRect)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT BASED_CODE ResourceButtons[] =
{
	// same order as in the bitmap 'toolbar.bmp'
	ID_ARROW_CTRL,  
	ID_LABEL_CTRL,  
	ID_EDIT_CTRL,   
	ID_GROUP_CTRL,  
	ID_RADIO_CTRL,  
	ID_CHECKBOX_CTRL,
	ID_COMBOBOX_CTRL,
	ID_LISTBOX_CTRL,
	ID_BUTTON_CTRL, 
	ID_LINE_CTRL,
	ID_RECT_CTRL,
	ID_GRID_CTRL,
};

/////////////////////////////////////////////////////////////////////////////
// FormDesignerMDI message handlers

BOOL FormDesignerMDI::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIChildWnd::OnCreateClient(lpcs, pContext);
}

void FormDesignerMDI::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

int FormDesignerMDI::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	

	if (!m_wndToolBar.Create(this, WS_CHILD | WS_VISIBLE | CBRS_SIZE_DYNAMIC | CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY, IDW_RES_BAR_TEXT) ||
		!m_wndToolBar.LoadBitmap(IDB_RES_TOOLB) || 
		!m_wndToolBar.SetButtons(ResourceButtons, sizeof(ResourceButtons)/sizeof(UINT)))
	{
		TRACE0("Failed to create resourcebar\n");	
		return -1;      // fail to create
	}
	m_wndToolBar.SetSizes( CSize(27,27), CSize(20,20) );
	m_wndToolBar.SetWindowText(_T("Controls"));
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	for(int i = 0; i < sizeof(ResourceButtons)/sizeof(UINT); i++)
	{
		m_wndToolBar.SetButtonStyle( i, TBBS_CHECKGROUP      /*TBBS_CHECKBOX */);

	}

	DockControlBar(&m_wndToolBar,AFX_IDW_DOCKBAR_LEFT);

	LoadAccelTable( MAKEINTRESOURCE(IDR_FORMDESIGNER) );

	return 0;
}
void FormDesignerMDI::OnArrow()
{
}
void FormDesignerMDI::OnLabel()
{
}
void FormDesignerMDI::OnEdit()  
{
}
void FormDesignerMDI::OnGroup()    
{
}
void FormDesignerMDI::OnButton()   
{
}
void FormDesignerMDI::OnCombobox() 
{
}
void FormDesignerMDI::OnListbox()  
{
}
void FormDesignerMDI::OnRadio()    
{
}
void FormDesignerMDI::OnCheckbox() 
{
}
void FormDesignerMDI::OnLine()
{
}
void FormDesignerMDI::OnRect()
{
}

void FormDesignerMDI::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
	
	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

BOOL FormDesignerMDI::DestroyWindow() 
{
	int ilDocCount=-1;
	POSITION pos = ((CUWorkBenchApp*)AfxGetApp())->pFormDesignerTpl->GetFirstDocPosition();
	while (pos != NULL)
	{
		FormDesignerDoc* pDoc = (FormDesignerDoc*)((CUWorkBenchApp*)AfxGetApp())->pFormDesignerTpl->GetNextDoc(pos );
		if(pDoc != NULL)
		{
			ilDocCount++;
		}
	}

	if(ilDocCount == 0)
	{
		if(pogPropertiesDlg != NULL)
		{
			pogPropertiesDlg->GetWindowRect(&ogPropertiesDlgRect);
			pogPropertiesDlg->EndDialog(0);
			delete pogPropertiesDlg;
			pogPropertiesDlg = NULL;
		}
	}
	return CMDIChildWnd::DestroyWindow();
}

void FormDesignerMDI::OnClose() 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CMDIChildWnd::OnClose();
}
