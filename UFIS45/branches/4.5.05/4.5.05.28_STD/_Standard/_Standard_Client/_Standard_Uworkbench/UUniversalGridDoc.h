#if !defined(AFX_UUNIVERSALGRIDDOC_H__EE6B3C53_47E4_11D3_B093_00001C019205__INCLUDED_)
#define AFX_UUNIVERSALGRIDDOC_H__EE6B3C53_47E4_11D3_B093_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUniversalGridDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridDoc document

#include "CCSGlobl.h"
#include "UObjectDataDefinitions.h"


class UUniversalGridDoc : public CDocument
{
protected:
	UUniversalGridDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UUniversalGridDoc)

// Attributes
public:
	CGXGridParam* m_pParam;

// Operations
public:

	UGridDefinition *pomGridDef;
	void SetCurrentGridDef(UGridDefinition *popGrodDef);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUniversalGridDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual void SetTitle(LPCTSTR lpszTitle);
	virtual void DeleteContents();
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UUniversalGridDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(UUniversalGridDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUNIVERSALGRIDDOC_H__EE6B3C53_47E4_11D3_B093_00001C019205__INCLUDED_)
