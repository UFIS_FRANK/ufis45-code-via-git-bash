#if !defined(AFX_DPLDLG_H__99242CA4_5710_11D4_8FFC_0050DADD7302__INCLUDED_)
#define AFX_DPLDLG_H__99242CA4_5710_11D4_8FFC_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DPLDlg.h : header file
//
#include <GridControl.h>
#include <CCSEdit.h>

#define MINROWS 1

class CGridControl;
/////////////////////////////////////////////////////////////////////////////
// CDPLDlg dialog

class CDPLDlg : public CDialog
{
// Construction
public:
	CString Name;
	long StfUrno;
	CDPLDlg(CWnd* pParent = NULL);   // standard constructor
	~CDPLDlg();

// Dialog Data
	//{{AFX_DATA(CDPLDlg)
	enum { IDD = IDD_DPL_DIALOG };
	CSpinButtonCtrl	m_Spin_Year;
	CSpinButtonCtrl	m_Spin_Month;
	CCSEdit	m_Edit_Year;
	CCSEdit	m_Edit_Month;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDPLDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CGridControl *pomDPL;
	void NewMonth();
	void IniGrid();
	void Register();

	// Generated message map functions
	//{{AFX_MSG(CDPLDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int GetDaysOfMonth(COleDateTime opDay);
	CString GetDayStrg(COleDateTime opDay);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DPLDLG_H__99242CA4_5710_11D4_8FFC_0050DADD7302__INCLUDED_)
