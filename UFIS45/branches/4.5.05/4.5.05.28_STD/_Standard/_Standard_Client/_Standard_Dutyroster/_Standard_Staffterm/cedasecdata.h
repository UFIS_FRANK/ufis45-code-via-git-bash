#ifndef _CedaSecData_H_
#define _CedaSecData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define SEC_SEND_DDX	(true)
#define SEC_NO_SEND_DDX	(false)

// Felddimensionen
#define SEC_LANG_LEN	(4)
#define SEC_REMA_LEN	(80)
#define SEC_HOPO_LEN	(3)
#define SEC_NAME_LEN	(32)
#define SEC_STAT_LEN	(1)

// Struktur eines Sec-Datensatzes
struct SECDATA {
	char			Hopo[SEC_HOPO_LEN+2];	//Hopo
	char			Lang[SEC_LANG_LEN+2];	//Language
	char			Name[SEC_NAME_LEN+2];	//Name
	char			Pass[SEC_NAME_LEN+2];	//Password
	char			Rema[SEC_REMA_LEN+2];	//Remarks
	char			Stat[SEC_STAT_LEN+2];	//Status
	char			Type[SEC_STAT_LEN+2];	//Datensatz Typ
	long			Urno;					//Urno
	char			Usid[SEC_NAME_LEN+2];	//User ID

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	SECDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaSecData class
void ProcessSecCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaSecData: kapselt den Zugriff auf die Tabelle Sec (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaSecData: public CCSCedaData
{
// Funktionen
public:
	bool ReadSecData();
    // Konstruktor/Destruktor
	CedaSecData(CString opTableName = "SEC", CString opExtName = "TAB");
	~CedaSecData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<SECDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_SEC_CHANGE,BC_SEC_DELETE und BC_SEC_NEW
	void ProcessSecBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadSecByUrno(long lpUrno, SECDATA *prpSec);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(SECDATA *prpSec, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(SECDATA *prpSec, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(SECDATA *prpSec, bool bpSave = true);

	// Datens�tze suchen
	// Sec nach Urno suchen
	SECDATA* GetSecByUrno(long lpUrno);

	// kopiert die Feldwerte eines Sec-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopySec(SECDATA* popSecDataSource,SECDATA* popSecDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(SECDATA *prpSec);
	// einen Broadcast BUD_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(SECDATA *prpSec, bool bpSendDdx);
	// einen Broadcast BUD_CHANGE versenden
	bool UpdateInternal(SECDATA *prpSec, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(SECDATA *prpSec, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Secs
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaSecData_H_
