#if !defined(AFX_CHOICEDLG_H__99242CA1_5710_11D4_8FFC_0050DADD7302__INCLUDED_)
#define AFX_CHOICEDLG_H__99242CA1_5710_11D4_8FFC_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChoiceDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg dialog

class CChoiceDlg : public CDialog
{
// Construction
public:
	CMapStringToString omVertragStunden;
	CMapStringToString omVertragFest;
	long StfUrno;
	CChoiceDlg(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CChoiceDlg)
	enum { IDD = IDD_CHOICE_DIALOG };
	int		m_radio_wish;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChoiceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CString Name;

	// Generated message map functions
	//{{AFX_MSG(CChoiceDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonShow();
	afx_msg void OnRadioWish();
	afx_msg void OnRadioDaySchedule();
	afx_msg void OnRadioScheduleOrg();
	afx_msg void OnButtonInfo();
	afx_msg void OnRadioAbsplan();
	//}}AFX_MSG

	// Hier gehen alle BC f�r diese Anwendung rein
	afx_msg LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/);  

	DECLARE_MESSAGE_MAP()
private:
	void FillContractMap(CString omString, CString omContract);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHOICEDLG_H__99242CA1_5710_11D4_8FFC_0050DADD7302__INCLUDED_)
