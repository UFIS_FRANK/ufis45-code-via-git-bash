// StaffTermDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSParam.h>
#include <StaffTerm.h>
#include <StaffTermDlg.h>
#include <CedaWisData.h>
#include <CedaSecData.h>
#include <CedaStfData.h>
#include <CedaBasicData.h>

#include <CedaScoData.h>
//uhi 15.9.00 Eins�tze und Stunden jetzt aus ACCTAB
//#include "CedaSmdData.h"
#include <CedaAccData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//**************************************************************************************
// Proce.s.sBC: globale Funktion als Callback f�r den Broadcast-Handler
//**************************************************************************************

static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CStaffTermDlg *polStaffTerm = (CStaffTermDlg *)popInstance;

	if(ipDDXType == DRR_NEW)          polStaffTerm->ProcessDrrChange((DRRDATA*)vpDataPointer);
	if(ipDDXType == DRR_CHANGE)       polStaffTerm->ProcessDrrChange((DRRDATA*)vpDataPointer);
	if(ipDDXType == DRR_DELETE)       polStaffTerm->ProcessDrrDelete((DRRDATA*)vpDataPointer);
	
	if(ipDDXType == DRW_NEW)          polStaffTerm->ProcessDrwChange((DRWDATA*)vpDataPointer);
	if(ipDDXType == DRW_DELETE)       polStaffTerm->ProcessDrwDelete((DRWDATA*)vpDataPointer);
	if(ipDDXType == DRW_CHANGE)       polStaffTerm->ProcessDrwChange((DRWDATA*)vpDataPointer);
	
}
/////////////////////////////////////////////////////////////////////////////
// CStaffTermDlg dialog

CStaffTermDlg::CStaffTermDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStaffTermDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStaffTermDlg)
	m_Static_Einsatz = _T("");
	m_Static_Hours = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	StfUrno = 0;
	Name = CString("");
	//uhi 19.9.00
	//bGav = false;

	pomWish = NULL;

	//Insert all Hollidays from HOLTAB in omHollidayKeyMap to use for header colors 
    omHolidayKeyMap.RemoveAll();
	CString olTmp;
	RecordSet *polRecord = new RecordSet(ogBCD.GetFieldCount("HOL"));
	for(int i = 0; i < ogBCD.GetDataCount("HOL"); i++)
	{
		ogBCD.GetRecord("HOL",i, *polRecord);
		olTmp =  polRecord->Values[ogBCD.GetFieldIndex("HOL","HDAY")];
		omHolidayKeyMap.SetAt(olTmp,NULL);
	}
	delete polRecord;

	// beim Broadcast-Handler anmelden
	Register();
}

CStaffTermDlg::~CStaffTermDlg()
{
	omHolidayKeyMap.RemoveAll();

	if(pomWish != NULL)
		delete pomWish;
}

void CStaffTermDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStaffTermDlg)
	DDX_Control(pDX, IDMULTIPLEINPUT, m_MultipleInput);
	DDX_Control(pDX, IDC_STATIC_INPUT, m_Static_Input);
	DDX_Control(pDX, IDC_EDIT_HOURS, m_Edit_Hours);
	DDX_Control(pDX, IDC_EDIT_EINSATZ, m_Edit_Einsatz);
	DDX_Control(pDX, IDC_STATIC_NAME, m_Static_Name);
	DDX_Control(pDX, IDC_EDIT_YEAR, m_Edit_Year);
	DDX_Control(pDX, IDC_SPIN_YEAR, m_Spin_Year);
	DDX_Control(pDX, IDC_EDIT_MONTH, m_Edit_Month);
	DDX_Control(pDX, IDC_SPIN_MONTH, m_Spin_Month);
	DDX_Text(pDX, IDC_STATIC_EINSATZ, m_Static_Einsatz);
	DDX_Text(pDX, IDC_STATIC_HOURS, m_Static_Hours);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CStaffTermDlg, CDialog)
	//{{AFX_MSG_MAP(CStaffTermDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_VSCROLL()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDMULTIPLEINPUT, OnMultipleInput)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaffTermDlg message handlers

BOOL CStaffTermDlg::OnInitDialog()
{
	CString csPersnr, csUsid,csMonth, csYear, csTitle; //csCot 
	COleDateTime ctToday = COleDateTime::GetCurrentTime();
	//uhi 19.9.00
	//CCSPtrArray<SCODATA> opScoData;
	//SCODATA olSco;

	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
		pSysMenu->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);
		pSysMenu->DeleteMenu(SC_MAXIMIZE, MF_BYCOMMAND);
	}

	SetDlgItemText(IDC_STATIC_YEAR, LoadStg(IDS_STRING1009));
	SetDlgItemText(IDC_STATIC_MONTH, LoadStg(IDS_STRING1010));
	SetDlgItemText(IDC_STATIC_EINSATZ, LoadStg(IDS_STRING1016));
	SetDlgItemText(IDC_STATIC_HOURS, LoadStg(IDS_STRING1017));
	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));
	//uhi 9.01.01 Mehrfacheingabe
	SetDlgItemText(IDMULTIPLEINPUT, CString("&") + LoadStg(IDS_MULTIPLE_INPUT));

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
		
	csMonth.Format("%.2d", ctToday.GetMonth());
	csYear.Format("%i", ctToday.GetYear());
	
	m_Edit_Einsatz.SetTextLimit(0,2);
	m_Edit_Einsatz.SetTextErrColor(RED);	
	m_Edit_Einsatz.SetTypeToInt(-1,99);

	//uhi 11.5.01
	m_Edit_Hours.SetTextLimit(0,3);
	m_Edit_Hours.SetTextErrColor(RED);
	m_Edit_Hours.SetTypeToInt(-1,999);

	m_Edit_Month.SetTextLimit(1,2);
	m_Edit_Month.SetTextErrColor(RED);
	m_Edit_Month.SetInitText(csMonth);
	m_Edit_Month.SetTypeToInt(0,12);

	m_Edit_Year.SetTextLimit(1,4);
	m_Edit_Year.SetTextErrColor(RED);
	m_Edit_Year.SetInitText(csYear);
	m_Edit_Year.SetTypeToInt(2000,2050);

	// Spins initialisieren
	m_Spin_Month.SetRange(1,12);
	m_Spin_Month.SetPos(ctToday.GetMonth());

	m_Spin_Year.SetRange(2000,2050);
	m_Spin_Year.SetPos(ctToday.GetYear());

	csTitle = LoadStg(IDS_WISH);
	if(!Name.IsEmpty())
		csTitle = csTitle + CString(" - ") + Name;
	SetWindowText(csTitle);

	//uhi 19.9.00
	//Vertragsart ermitteln
	/*if(StfUrno !=0){
		ogScoData.GetScoBySurnWithTime(StfUrno, ctToday, ctToday, &opScoData);
		if(opScoData.GetSize()>0){
			olSco = opScoData.GetAt(0);
			csCot = olSco.Code;
			if(csCot == CString("103") || csCot == CString("109"))
				bGav = true;
		}
	}*/


	//Parameter lesen
	CTime	olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	iMaxWish = atoi(ogCCSParam.GetParamValue(ogAppl,"ID_MAX_WISH",olStringNow));
	//uhi 11.5.01
	//iLastInput = atoi(ogCCSParam.GetParamValue(ogGlobal,"ID_LAST_INPUT",olStringNow));
	iLastInput = atoi(ogCCSParam.GetParamValue(ogAppl,"ID_INPUT_BY",olStringNow));

	int ilYear, ilMonth;

	//uhi 24.4.01	
	if(iLastInput != -1){
		//uhi 15.01.00 Stichtagberechnung neu
		//COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
		//CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	
		ilYear = olTimeNow.GetYear();
		//uhi 31.7.01 PRF 255
		if(olTimeNow.GetDay() <= iLastInput)
			ilMonth = olTimeNow.GetMonth() + 2;
		else
			if(olTimeNow.GetDay() > iLastInput)
				ilMonth = olTimeNow.GetMonth() + 3;	

		if (ilMonth > 12){
			ilMonth = ilMonth - 12;
			ilYear = ilYear + 1;
		}
						
		ctValidInput = COleDateTime(ilYear, ilMonth, 1, 0, 0, 0);
	}
	else{
		ilYear = olTimeNow.GetYear();
		ilMonth = olTimeNow.GetMonth();
	}

	ctValidInput = COleDateTime(ilYear, ilMonth, 1, 0, 0, 0);

	//Grid initialisieren
	pomWish = new CGridControl(this, IDC_WISH_GRID, 6, MINROWS);
	IniGrid();
	NewMonth();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CStaffTermDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CStaffTermDlg::OnDestroy()
{
	ogDdx.UnRegister(this, NOTUSED);
	//ogCommHandler.CleanUpComm();

	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CStaffTermDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CStaffTermDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CStaffTermDlg::IniGrid()
{
CCS_TRY
	CString csWish, csPrio, csCode, csBesch;
	
	for (int n=0; n<ogWisData.omData.GetSize(); n++){
		csCode = ogWisData.omData[n].Wisc;
		csBesch = ogWisData.omData[n].Wisd;
		if (!csCode.IsEmpty())
			csWish = csWish + csCode + "\t" + csBesch + "\n";
	}

	//uhi 23.7.01 Prio von 1 - 3
	for (n=1; n<4; n++){
		csCode.Format("%i", n);
		csPrio = csPrio + csCode + "\n";
	}
	
	//�berschriften setzten
	pomWish->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1002));
	pomWish->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1003));
	pomWish->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1004));
	pomWish->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1005));
	pomWish->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1006));
	pomWish->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING1007));

	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomWish->GetParam()->EnableTrackColWidth(FALSE);
	pomWish->GetParam()->EnableTrackRowHeight(FALSE);
    pomWish->GetParam()->EnableSelection(FALSE);

	//Scrollbars anzeigen wenn n�tig
	pomWish->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	//Monat (read-only)
	pomWish->SetStyleRange( CGXRange().SetCols(1), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));

	//Wunsch (Combo)
	pomWish->SetStyleRange( CGXRange().SetCols(2), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX) //GX_IDS_CTRL_COMBOBOX)
				.SetChoiceList(csWish.GetBuffer(0))
				.SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
				.SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("55,200"))); 
	
	//Priorit�t (Combo)
	pomWish->SetStyleRange( CGXRange().SetCols(3), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(csPrio.GetBuffer(0))); 
	
	//Bemerkung (editierbar)
	pomWish->SetStyleRange( CGXRange().SetCols(4), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_EDIT));

	//Bemerkung f�r Mitarbeiter, Absenzen (read-only)
	pomWish->SetStyleRange( CGXRange().SetCols(5, 6), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));

	//Keine Zeilennummerierung
	pomWish->HideCols(0, 0);
	pomWish->SetColWidth(1, 1, pomWish->Width_LPtoDP(4 * GX_NXAVGWIDTH));
	pomWish->SetColWidth(2, 2, pomWish->Width_LPtoDP(10 * GX_NXAVGWIDTH));
	pomWish->SetColWidth(3, 3, pomWish->Width_LPtoDP(4 * GX_NXAVGWIDTH));
	pomWish->SetColWidth(4, 5, pomWish->Width_LPtoDP(30 * GX_NXAVGWIDTH));
	pomWish->SetColWidth(6, 6, pomWish->Width_LPtoDP(7 * GX_NXAVGWIDTH));
	
	pomWish->EnableAutoGrow(false);
	pomWish->EnableSorting(false);

CCS_CATCH_ALL
}

void CStaffTermDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
CCS_TRY
	// TODO: Add your message handler code here and/or call default
	CString csValue;
	
	//CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
	if (nSBCode == SB_ENDSCROLL)
		return;

	CString csYear, csMonth;

	//uhi 12.3.01 Keine Pr�fung in der Vergangenheit
	m_Edit_Year.GetWindowText(csYear);
	m_Edit_Month.GetWindowText(csMonth);

	COleDateTime ctNewMonth(atoi(csYear), atoi(csMonth), 1, 0, 0, 0);
	
	CString olValidInput = ctValidInput.Format("%d.%m.%Y");

	if(ctNewMonth>=ctValidInput){
		if(!SaveMonth())
			return;
	}
	
	
	if(pScrollBar->GetDlgCtrlID() == IDC_SPIN_MONTH){
		csValue.Format("%.2d", nPos);
		m_Edit_Month.SetInitText(csValue);
	}

	if(pScrollBar->GetDlgCtrlID() == IDC_SPIN_YEAR){
		csValue.Format("%i", nPos);
		m_Edit_Year.SetInitText(csValue);
	}

	NewMonth();
	
CCS_CATCH_ALL
}

int CStaffTermDlg::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL
	return 0;
}

void CStaffTermDlg::NewMonth()
{
CCS_TRY
	ROWCOL iRows;
	DRWDATA *pDrw;
	DRRDATA *pDrr;
	//uhi 15.9.00 Eins�tze und Stunden jetzt aus ACCTAB
	//SMDDATA	*pSmd;
	ACCDATA	*pAcc;

	CString csMonth, csYear, csDay, csStfUrno, csMtYr, csRosl, csChoice;
	COleDateTime ctSelectedMonth;
	int iDays, iPos;
	long ilUrno;

	CGXStyle style;

	csRosl = CString("U");

	iRows = pomWish->GetRowCount();
	if (iRows >= 1)
		pomWish->RemoveRows(1, iRows);

	m_Edit_Month.GetWindowText(csMonth);
	m_Edit_Year.GetWindowText(csYear);

	m_Edit_Einsatz.SetInitText("");
	m_Edit_Hours.SetInitText("");

	csStfUrno.Format("%d", StfUrno);

	//Max.Eins�tze, max.Stunden aus SMDTAB lesen
	/*if(StfUrno != 0){
		csStfUrno.Format("%d", StfUrno);	
		csMtYr.Format("%.4s%.2s", csYear, csMonth);
		pSmd = ogSmdData.GetSmdByStfuWithTime(csStfUrno, csMtYr);
		if (pSmd != NULL){
			m_Edit_Einsatz.SetInitText(pSmd->Maxd);
			m_Edit_Hours.SetInitText(pSmd->Maxt);
		}
	}*/

	CString csType;
	CString csValue;
	if(StfUrno != 0){
		//Maximale Eins�tze
		csType = CString("41");
		pAcc = ogAccData.GetAccByKey(StfUrno, csYear, csType);
		if (pAcc != NULL){
			csValue = GetAccValuePerMonth(pAcc, csMonth);
			m_Edit_Einsatz.SetInitText(csValue);
		}
		//Maxiamle Stunden
		csType = CString("29");
		pAcc = NULL;
		pAcc = ogAccData.GetAccByKey(StfUrno, csYear, csType);
		if (pAcc != NULL){
			csValue = GetAccValuePerMonth(pAcc, csMonth);
			m_Edit_Hours.SetInitText(csValue);
		}
	}

	ctSelectedMonth.SetDate(atoi(csYear), atoi(csMonth), 1);  	
	iDays = GetDaysOfMonth(ctSelectedMonth);
	
	pomWish->SetRowCount(iDays);
	
	// Grid mit Daten f�llen.
	pomWish->GetParam()->SetLockReadOnly(false);
	
	for ( int i=0; i<iDays; i++ )
	{
		//Tag
		csDay.Format("%.2d", i+1);
		pomWish->SetValueRange(CGXRange(i+1,1), csDay);
		//Kein Mitarbeiter dem User zugeordnet. Es werden keine Daten angezeigt.
		if (StfUrno != 0){
			csDay.Format("%4s%.2s%.2i", csYear, csMonth, i+1);
			
			//uhi 12.9.01
			if(IsWeekend(csDay) == true)
				//Wochenende
				pomWish->SetStyleRange(CGXRange(i+1,1), CGXStyle()
						.SetInterior(RGB(233, 195, 195)));
						//.SetBorders(gxBorderAll, CGXPen().SetColor(RGB(255,   0, 0))));
				
			else{
				//kein Wochenende
				CString olTmp;
				olTmp.Format("%4s%2s%2i000000", csYear, csMonth, i+1);
				void  *prlVoid = NULL;
				if(omHolidayKeyMap.Lookup(olTmp,(void *&)prlVoid) == TRUE)
					//Feiertag
					pomWish->SetStyleRange(CGXRange(i+1,1), CGXStyle()
						.SetInterior(RGB(233, 195, 195)));
				else
					//normaler Arbeitstag
					pomWish->SetStyleRange(CGXRange(i+1,1), CGXStyle()
							.SetInterior(RGB(255, 255, 255)));
						//.SetBorders(gxBorderAll, CGXPen().SetColor(RGB(  0,   0,   0))));
			}

			pDrw = ogDrwData.GetDrwByStfuWithTime(csStfUrno, csDay);
			if(pDrw != NULL){
				// Urno auslesen
				ilUrno = pDrw->Urno;
				// Urno mit erstem Feld koppeln.						
				pomWish->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
				//Wunsch
				//uhi 12.3.01 Code nicht vorhanden.
				pomWish->ComposeStyleRowCol(i+1, 2, &style );
				csChoice = style.GetChoiceList();
				iPos = csChoice.Find(pDrw->Wisc);
				if(iPos != -1){
					//csChoice = csChoice + CString(pDrw->Wisc) + "\t" + CString("?") + "\n";
					//pomWish->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetChoiceList(csChoice.GetBuffer(0)));
					pomWish->SetValueRange(CGXRange(i+1,2), pDrw->Wisc);
				}
				pomWish->SetValueRange(CGXRange(i+1,2), pDrw->Wisc);
				//Priorit�t
				pomWish->SetValueRange(CGXRange(i+1,3), pDrw->Prio);
				//Bemerkung Mitarbeiter
				pomWish->SetValueRange(CGXRange(i+1,4), pDrw->Rems);
				//Bemerkung Disponent
				pomWish->SetValueRange(CGXRange(i+1,5), pDrw->Reme);
				//Keine Eingabe wenn Satz vom Disponenten gelockt wurde.
				if (pDrw->Clos == CString("B"))
					pomWish->SetStyleRange( CGXRange().SetRows(i+1), CGXStyle().SetEnabled(false));
			}

			pDrr = ogDrrData.GetDrrByStfuWithTimeAndRosl(csStfUrno, csDay, csRosl);
			if(pDrr != NULL){
				//Absences
				pomWish->SetValueRange(CGXRange(i+1,6), pDrr->Scod);
				//Keine Eingabe
				pomWish->SetStyleRange( CGXRange().SetRows(i+1), CGXStyle().SetEnabled(false));
			}
		}
	}

	if(bGav == false){
		pomWish->HideCols(3, 3, true);
		//pomWish->HideCols(5, 5, true);
		pomWish->SetColWidth(4, 5, pomWish->Width_LPtoDP(32 * GX_NXAVGWIDTH));
		m_Edit_Hours.ShowWindow(SW_SHOWNORMAL);
		m_Edit_Einsatz.ShowWindow(SW_SHOWNORMAL);
		SetDlgItemText(IDC_STATIC_EINSATZ, LoadStg(IDS_STRING1016));
		SetDlgItemText(IDC_STATIC_HOURS, LoadStg(IDS_STRING1017));
	}
	else{
		pomWish->HideCols(3, 3, false);
		//pomWish->HideCols(5, 5, false);
		pomWish->SetColWidth(4, 5, pomWish->Width_LPtoDP(30 * GX_NXAVGWIDTH));
		m_Edit_Hours.ShowWindow(SW_HIDE);
		m_Edit_Einsatz.ShowWindow(SW_HIDE);
		SetDlgItemText(IDC_STATIC_EINSATZ, CString(""));
		SetDlgItemText(IDC_STATIC_HOURS, CString(""));
	}

	//Eingabe erlaubt?
	/*COleDateTime ctToday = COleDateTime::GetCurrentTime();
	if(ctToday.GetMonth() >= atoi(csMonth) && ctToday.GetYear() >= atoi(csYear)){
		//Keine Eingabe in der Vergangenheit/Gegenwart
		pomWish->SetStyleRange( CGXRange().SetTable(), CGXStyle().SetEnabled(false));
		SetDlgItemText(IDC_STATIC_INPUT, LoadStg(IDS_STRING1020));
	}
	else{
		if(ctToday.GetDay() > iLastInput && ctToday.GetMonth() == atoi(csMonth) - 1){
			//Keine Eingabe f�r Folgemonat nach Stichtag
			pomWish->SetStyleRange( CGXRange().SetTable(), CGXStyle().SetEnabled(false));
			SetDlgItemText(IDC_STATIC_INPUT, LoadStg(IDS_STRING1020));
		}
		else{
			pomWish->SetStyleRange( CGXRange().SetTable(), CGXStyle().SetEnabled(true));
			SetDlgItemText(IDC_STATIC_INPUT, CString(""));
		}

	}*/

	//uhi 15.01.00 St�chtagberechnung neu
	COleDateTime ctNewMonth(atoi(csYear), atoi(csMonth), 1, 0, 0, 0);
	CString olValidInput = ctValidInput.Format("%d.%m.%Y");
	if(ctNewMonth>=ctValidInput){
		pomWish->SetStyleRange( CGXRange().SetTable(), CGXStyle().SetEnabled(true));
		SetDlgItemText(IDC_STATIC_INPUT, CString(""));
		//uhi 10.4.01
		m_MultipleInput.EnableWindow(true);
	}
	else{
		//Keine Eingabe
		pomWish->SetStyleRange( CGXRange().SetTable(), CGXStyle().SetEnabled(false));
		SetDlgItemText(IDC_STATIC_INPUT, LoadStg(IDS_STRING1020) + CString(" ") + olValidInput);
		//uhi 10.4.01
		m_MultipleInput.EnableWindow(false);	
	}
		
	// Anzeige einstellen
	pomWish->GetParam()->SetLockReadOnly(true);
	//pomWish->Redraw();

CCS_CATCH_ALL
}

void CStaffTermDlg::OnOK() 
{
	// TODO: Add extra validation here
	bool bRet = true;
	CString csYear, csMonth;

	//uhi 12.3.01 Keine Pr�fung in der Vergangenheit
	m_Edit_Year.GetWindowText(csYear);
	m_Edit_Month.GetWindowText(csMonth);

	COleDateTime ctNewMonth(atoi(csYear), atoi(csMonth), 1, 0, 0, 0);
	
	CString olValidInput = ctValidInput.Format("%d.%m.%Y");

	if(StfUrno != 0){
		if(ctNewMonth>=ctValidInput)
			bRet = SaveMonth();
	}
	if(bRet == true)
		CDialog::OnOK();
}

bool CStaffTermDlg::SaveMonth()
{
	long ilUrno;
	ROWCOL ilRowCount;
	CGXStyle style;
	CString csYear, csMonth, csDay, csWish, csPrio, csRems, csSday, csStfu;
	CString csPrioAlt, csWishAlt, csRemsAlt, csEinsatz, csHours, csMtYr;
	int iWish;

	//WISDATA *pWis;
	
	// Sanduhr
	AfxGetApp()->DoWaitCursor(1);

	ilRowCount = pomWish->GetRowCount();
	
	m_Edit_Year.GetWindowText(csYear);
	m_Edit_Month.GetWindowText(csMonth);
	m_Edit_Einsatz.GetWindowText(csEinsatz);
	m_Edit_Hours.GetWindowText(csHours);

	//Anzahl W�nsche ermitteln
	iWish = 0;
	for (ROWCOL i=0; i<ilRowCount; i++){
		csWish = pomWish->GetValueRowCol(i+1,2);
		if(!csWish.IsEmpty())
			iWish = iWish + 1;
	}
	
	//uhi 24.4.01/4.7.01 Keine Pr�fung f�r Aushilfen
	if(iWish > iMaxWish && iMaxWish != -1 && bGav == true){
		// Sanduhr
		AfxGetApp()->DoWaitCursor(-1);
		CString olErrorTxt;
		olErrorTxt.Format("%s",LoadStg(IDS_STRING1018));
		MessageBox(olErrorTxt, LoadStg(IDS_ERROR), MB_ICONEXCLAMATION);
		return false;
	}

	//Pr�fen
	/*for (i=0; i<ilRowCount; i++){
		csWish = pomWish->GetValueRowCol(i+1,2);
		csPrio = pomWish->GetValueRowCol(i+1,3);
		//Wunschcode pr�fen
		if (!csWish.IsEmpty()){
			pWis = ogWisData.GetWisByKey(csWish);
			if (pWis == NULL){
				pomWish->SetStyleRange(CGXRange(i+1, 2), CGXStyle( ).SetTextColor(RGB(255, 0, 0)));
				Beep(440,70);
				// Sanduhr
				AfxGetApp()->DoWaitCursor(-1);
				return false;
			}
			else
				pomWish->SetStyleRange(CGXRange(i+1, 2), CGXStyle( ).SetTextColor(RGB(0, 0, 0)));
		}
		//Priorit�t pr�fen
		if (!csPrio.IsEmpty()){
			if (csPrio != CString("0") && csPrio != CString("1") && csPrio != CString("2") && csPrio != CString("3")){
				pomWish->SetStyleRange(CGXRange(i+1, 3), CGXStyle( ).SetTextColor(RGB(255, 0, 0)));
				Beep(440,70);
				// Sanduhr
				AfxGetApp()->DoWaitCursor(-1);
				return false;
			}
			else
				pomWish->SetStyleRange(CGXRange(i+1, 2), CGXStyle( ).SetTextColor(RGB(0, 0, 0)));
		}
	}*/

	csStfu.Format("%d", StfUrno);

	//Speichern DRWTAB
	for (i=0; i<ilRowCount; i++){
		csDay = pomWish->GetValueRowCol(i+1,1);
		csWish = pomWish->GetValueRowCol(i+1,2);
		csPrio = pomWish->GetValueRowCol(i+1,3);
		csRems = pomWish->GetValueRowCol(i+1,4);
		csSday.Format("%.4s%.2s%.2s", csYear, csMonth, csDay);
		if (!csWish.IsEmpty()){
			//if(csPrio.IsEmpty())
			//	csPrio = CString("0");
			pomWish->ComposeStyleRowCol(i+1, 1, &style );
			//Urno erhalten
			ilUrno = (long)style.GetItemDataPtr();
			if (ilUrno){
				//Update Satz
				DRWDATA *pDrw = NULL;
				pDrw = ogDrwData.GetDrwByUrno(ilUrno);
				if (pDrw != NULL){
					csPrioAlt = pDrw->Prio;
					csWishAlt = pDrw->Wisc;
					csRemsAlt = pDrw->Rems;
					if(csPrio != csPrioAlt || csWish != csWishAlt || csRems != csRemsAlt){
						// �nderungsflag setzen
						pDrw->IsChanged = DATA_CHANGED;
						// �nderungsdatum einstellen
						pDrw->Lstu = COleDateTime::GetCurrentTime();
						// Anwender einstellen
						strcpy(pDrw->Useu,pcgUser);
						//Priorit�t
						strcpy(pDrw->Prio, csPrio.GetBuffer(0));
						//Remarks Employee
						//if (!csRems.IsEmpty())
							strcpy(pDrw->Rems, csRems.GetBuffer(0));
						//Wunsch
						strcpy(pDrw->Wisc, csWish.GetBuffer(0));
						ogDrwData.Update(pDrw);
					}
				}
			}
			else{
				//Neuer Satz
				DRWDATA *pDrwNew = new DRWDATA;
				// �nderungsflag setzen
				pDrwNew->IsChanged = DATA_NEW;
				// Erzeugungsdatum einstellen
				pDrwNew->Cdat = COleDateTime::GetCurrentTime();
				// Anwender (Ersteller) einstellen
				strcpy(pDrwNew->Usec,pcgUser);
				//n�chste freie Datensatz-Urno ermitteln und speichern
				pDrwNew->Urno = ogBasicData.GetNextUrno();
				//Home Airport
				strcpy(pDrwNew->Hopo,pcgHome);
				//Priorit�t
				strcpy(pDrwNew->Prio, csPrio.GetBuffer(0));
				//Remarks Employee
				//if (!csRems.IsEmpty())
					strcpy(pDrwNew->Rems, csRems.GetBuffer(0));
				//Schichttag
				strcpy(pDrwNew->Sday, csSday.GetBuffer(0));
				//Urno Employee
				strcpy(pDrwNew->Stfu, csStfu.GetBuffer(0));
				//Wunsch
				strcpy(pDrwNew->Wisc, csWish.GetBuffer(0));
				ogDrwData.Insert(pDrwNew);
			}
		}
		else{
			pomWish->ComposeStyleRowCol(i+1, 1, &style );
			//Urno erhalten
			ilUrno = (long)style.GetItemDataPtr();
			if (ilUrno){
				//Update Satz
				DRWDATA *pDrwOld = NULL;
				pDrwOld = ogDrwData.GetDrwByUrno(ilUrno);
				if (pDrwOld != NULL)
					ogDrwData.Delete(pDrwOld);
			}
		}

	}

	//Speichern SMDTAB
	/*if(bGav == false){
		if(!csEinsatz.IsEmpty() || !csHours.IsEmpty()){
			csMtYr.Format("%.4s%.2s", csYear, csMonth);
			SMDDATA *pSmd = NULL;
			pSmd = ogSmdData.GetSmdByStfuWithTime(csStfu, csMtYr);
			//Update Satz
			if (pSmd != NULL){
				// �nderungsflag setzen
				pSmd->IsChanged = DATA_CHANGED;
				// �nderungsdatum einstellen
				pSmd->Lstu = COleDateTime::GetCurrentTime();
				// Anwender einstellen
				strcpy(pSmd->Useu,pcgUser);
				//Max.Eins�tze
				strcpy(pSmd->Maxd, csEinsatz.GetBuffer(0));
				//Max.Stunden
				strcpy(pSmd->Maxt, csHours.GetBuffer(0));
				ogSmdData.Update(pSmd);			
			}
			//Neuer Satz
			else{
				pSmd = new SMDDATA;
				// �nderungsflag setzen
				pSmd->IsChanged = DATA_NEW;
				// Erzeugungsdatum einstellen
				pSmd->Cdat = COleDateTime::GetCurrentTime();
				// Anwender (Ersteller) einstellen
				strcpy(pSmd->Usec,pcgUser);
				//n�chste freie Datensatz-Urno ermitteln und speichern
				pSmd->Urno = ogBasicData.GetNextUrno();
				//Home Airport
				strcpy(pSmd->Hopo,pcgHome);
				//Jahr/Monat
				strcpy(pSmd->Mtyr, csMtYr.GetBuffer(0));
				//Urno Employee
				strcpy(pSmd->Stfu, csStfu.GetBuffer(0));
				//Max.Eins�tze
				strcpy(pSmd->Maxd, csEinsatz.GetBuffer(0));
				//Max.Stunden
				strcpy(pSmd->Maxt, csHours.GetBuffer(0));
				ogSmdData.Insert(pSmd);
			}
		}
	}*/

	//uhi 15.9.00 Eins�tze und Stunden jetzt aus ACCTAB
	if(bGav == false){
		// Maximale Eins�tze
		if(!csEinsatz.IsEmpty())
			SaveAcc(CString("41"), csYear, csMonth, csEinsatz);
	    //Maximale Stunden
		if(!csHours.IsEmpty())
			SaveAcc(CString("29"), csYear, csMonth, csHours);
			
	}

	// Sanduhr
	AfxGetApp()->DoWaitCursor(-1);
	
	return true;
}

void CStaffTermDlg::Register()
{
	ogDdx.Register(this, DRR_CHANGE, CString("CStaffTermDlg"), CString("DRR_CHANGE"), ProcessBC);
	ogDdx.Register(this, DRR_NEW,	 CString("CStaffTermDlg"), CString("DRR_NEW"),	  ProcessBC);
	ogDdx.Register(this, DRR_DELETE, CString("CStaffTermDlg"), CString("DRR_DELETE"), ProcessBC);
				  	
	ogDdx.Register(this, DRW_NEW,    CString("CStaffTermDlg"), CString("DRW_NEW"),    ProcessBC);
    ogDdx.Register(this, DRW_CHANGE, CString("CStaffTermDlg"), CString("DRW_CHANGE"), ProcessBC);
	ogDdx.Register(this, DRW_DELETE, CString("CStaffTermDlg"), CString("DRW_DELETE"), ProcessBC);

}

void CStaffTermDlg::ProcessDrrChange(DRRDATA *prpDrr)
{
CCS_TRY
	CString csStfu, csYear, csMonth, csDay, csSday, csSelYear, csSelMonth;
	ROWCOL ilRowCount;
	int iDay;

	if(prpDrr == NULL)
		return;

	csStfu.Format("%d", StfUrno);

	if(prpDrr->Stfu != csStfu)
		return;

	if(prpDrr->Rosl != CString("U"))
		return;

	csSday = prpDrr->Sday;
	csYear = csSday.Left(4);
	csMonth = csSday.Mid(4, 2);
	csDay = csSday.Right(2);

	m_Edit_Year.GetWindowText(csSelYear);
	m_Edit_Month.GetWindowText(csSelMonth);

	if(csYear != csSelYear || csMonth != csSelMonth)
		return;

	ilRowCount = pomWish->GetRowCount();
	iDay = atoi(csDay);
	if (iDay > (int)ilRowCount)
		return;
	
	//Grid mit Daten f�llen.
	pomWish->GetParam()->SetLockReadOnly(false);
	//Absences
	pomWish->SetValueRange(CGXRange(iDay,6), prpDrr->Scod);
	//Keine Eingabe
	pomWish->SetStyleRange(CGXRange().SetRows(iDay), CGXStyle().SetEnabled(false));
	// Anzeige einstellen
	pomWish->GetParam()->SetLockReadOnly(true);
	//pomWish->Redraw();

	return;
CCS_CATCH_ALL
}

void CStaffTermDlg::ProcessDrwChange(DRWDATA *prpDrw)
{
CCS_TRY
	CString csStfu, csYear, csMonth, csDay, csSday, csSelYear, csSelMonth, csChoice;
	ROWCOL ilRowCount;
	int iDay, iPos;

	CGXStyle style;

	if(prpDrw == NULL)
		return;

	csStfu.Format("%d", StfUrno);

	if(prpDrw->Stfu != csStfu)
		return;

	csSday = prpDrw->Sday;
	csYear = csSday.Left(4);
	csMonth = csSday.Mid(4, 2);
	csDay = csSday.Right(2);

	m_Edit_Year.GetWindowText(csSelYear);
	m_Edit_Month.GetWindowText(csSelMonth);

	if(csYear != csSelYear || csMonth != csSelMonth)
		return;

	ilRowCount = pomWish->GetRowCount();
	iDay = atoi(csDay);
	if (iDay > (int)ilRowCount)
		return;

	//Grid mit Daten f�llen.
	pomWish->GetParam()->SetLockReadOnly(false);
	// Urno auslesen
	long ilUrno = prpDrw->Urno;
	// Urno mit erstem Feld koppeln.						
	pomWish->SetStyleRange (CGXRange(iDay,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
	//Wunsch
	//uhi 12.3.01 Code nicht vorhanden
	pomWish->ComposeStyleRowCol(iDay, 2, &style );
	csChoice = style.GetChoiceList();
	iPos = csChoice.Find(prpDrw->Wisc);
	if(iPos != -1){
		//csChoice = csChoice + CString(prpDrw->Wisc) + "\t" + CString("?") + "\n";
		//pomWish->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetChoiceList(csChoice.GetBuffer(0)));
		pomWish->SetValueRange(CGXRange(iDay,2), prpDrw->Wisc);
	}
	//Priorit�t
	pomWish->SetValueRange(CGXRange(iDay,3), prpDrw->Prio);
	//Bemerkung Mitarbeiter
	pomWish->SetValueRange(CGXRange(iDay,4), prpDrw->Rems);
	//Bemerkung Disponent
	pomWish->SetValueRange(CGXRange(iDay,5), prpDrw->Reme);
	// Anzeige einstellen
	pomWish->GetParam()->SetLockReadOnly(true);
	//pomWish->Redraw();

	return;
CCS_CATCH_ALL
}


void CStaffTermDlg::ProcessDrwDelete(DRWDATA *prpDrw)
{
CCS_TRY
	CString csStfu, csYear, csMonth, csDay, csSday, csSelYear, csSelMonth;
	ROWCOL ilRowCount;
	int iDay;

	CString csLeer = "";

	if(prpDrw == NULL)
		return;

	csStfu.Format("%d", StfUrno);

	if(prpDrw->Stfu != csStfu)
		return;

	csSday = prpDrw->Sday;
	csYear = csSday.Left(4);
	csMonth = csSday.Mid(4, 2);
	csDay = csSday.Right(2);

	m_Edit_Year.GetWindowText(csSelYear);
	m_Edit_Month.GetWindowText(csSelMonth);

	if(csYear != csSelYear || csMonth != csSelMonth)
		return;

	ilRowCount = pomWish->GetRowCount();
	iDay = atoi(csDay);
	if (iDay > (int)ilRowCount)
		return;

	//Grid mit Daten f�llen.
	pomWish->GetParam()->SetLockReadOnly(false);
	//Wunsch
	pomWish->SetValueRange(CGXRange(iDay,2), csLeer);
	//Priorit�t
	pomWish->SetValueRange(CGXRange(iDay,3), csLeer);
	//Bemerkung Mitarbeiter
	pomWish->SetValueRange(CGXRange(iDay,4), csLeer);
	//Bemerkung Disponent
	pomWish->SetValueRange(CGXRange(iDay,5), csLeer);
	// Anzeige einstellen
	pomWish->GetParam()->SetLockReadOnly(true);
	//pomWish->Redraw();

	return;
CCS_CATCH_ALL
}

void CStaffTermDlg::ProcessDrrDelete(DRRDATA *prpDrr)
{
	CString csStfu, csYear, csMonth, csDay, csSday, csSelYear, csSelMonth;
	ROWCOL ilRowCount;
	int iDay;

	CString csLeer = "";

	if(prpDrr == NULL)
		return;

	csStfu.Format("%d", StfUrno);

	if(prpDrr->Stfu != csStfu)
		return;

	if(prpDrr->Rosl != CString("U"))
		return;

	csSday = prpDrr->Sday;
	csYear = csSday.Left(4);
	csMonth = csSday.Mid(4, 2);
	csDay = csSday.Right(2);

	m_Edit_Year.GetWindowText(csSelYear);
	m_Edit_Month.GetWindowText(csSelMonth);

	if(csYear != csSelYear || csMonth != csSelMonth)
		return;

	ilRowCount = pomWish->GetRowCount();
	iDay = atoi(csDay);
	if (iDay > (int)ilRowCount)
		return;
	
	//Grid mit Daten f�llen.
	pomWish->GetParam()->SetLockReadOnly(false);
	//Absences
	pomWish->SetValueRange(CGXRange(iDay,6), csLeer);
	//Keine Eingabe
	pomWish->SetStyleRange( CGXRange().SetRows(iDay), CGXStyle().SetEnabled(true));
	// Anzeige einstellen
	pomWish->GetParam()->SetLockReadOnly(true);
	//pomWish->Redraw();

	return;
}

CString CStaffTermDlg::GetAccValuePerMonth(ACCDATA *opAcc, CString opMonth)
{
	CString olValue;
	switch (atoi(opMonth)) {
		case 1:
			olValue = opAcc->Co01;
			break;
		case 2:
			olValue = opAcc->Co02;
			break;
		case 3:
			olValue = opAcc->Co03;
			break;
		case 4:
			olValue = opAcc->Co04;
			break;
		case 5:
			olValue = opAcc->Co05;
			break;
		case 6:
			olValue = opAcc->Co06;
			break;
		case 7:
			olValue = opAcc->Co07;
			break;
		case 8:
			olValue = opAcc->Co08;
			break;
		case 9:
			olValue = opAcc->Co09;
			break;
		case 10:
			olValue = opAcc->Co10;
			break;
		case 11:
			olValue = opAcc->Co11;
			break;
		case 12:
			olValue = opAcc->Co12;
			break;
		default:
			olValue = CString("");
			break;
	}
	return olValue;
}

void CStaffTermDlg::SaveAcc(CString opType, CString opYear, CString opMonth, CString opValue)
{
	ACCDATA *polAcc;
	STFDATA *polStf;
	polAcc = ogAccData.GetAccByKey(StfUrno, opYear, opType);
	//Update
	if (polAcc != NULL){
		// �nderungsflag setzen
		polAcc->IsChanged = DATA_CHANGED;
		// �nderungsdatum einstellen
		polAcc->Lstu = CTime::GetCurrentTime();
		// Anwender einstellen
		strcpy(polAcc->Useu,pcgUser);
		//Max.Eins�tze/Stunden
		switch (atoi(opMonth)) {
			case 1:
				strcpy(polAcc->Co01, opValue.GetBuffer(0));
				break;
			case 2:
				strcpy(polAcc->Co02, opValue.GetBuffer(0));
				break;
			case 3:
				strcpy(polAcc->Co03, opValue.GetBuffer(0));
				break;
			case 4:
				strcpy(polAcc->Co04, opValue.GetBuffer(0));
				break;
			case 5:
				strcpy(polAcc->Co05, opValue.GetBuffer(0));
				break;
			case 6:
				strcpy(polAcc->Co06, opValue.GetBuffer(0));
				break;
			case 7:
				strcpy(polAcc->Co07, opValue.GetBuffer(0));
				break;
			case 8:
				strcpy(polAcc->Co08, opValue.GetBuffer(0));
				break;
			case 9:
				strcpy(polAcc->Co09, opValue.GetBuffer(0));
				break;
			case 10:
				strcpy(polAcc->Co10, opValue.GetBuffer(0));
				break;
			case 11:
				strcpy(polAcc->Co11, opValue.GetBuffer(0));
				break;
			case 12:
				strcpy(polAcc->Co12, opValue.GetBuffer(0));
				break;
		}				
		ogAccData.Update(polAcc);
	}
	else{
		polAcc = new ACCDATA;
		// �nderungsflag setzen
		polAcc->IsChanged = DATA_NEW;
		// Erstellungsdatum einstellen
		polAcc->Cdat = CTime::GetCurrentTime();
		// Anwender einstellen
		strcpy(polAcc->Usec,pcgUser);
		// �nderungsdatum einstellen (wegen Handler)
		polAcc->Lstu = CTime::GetCurrentTime();
		// Anwender einstellen (wegen Handler)
		strcpy(polAcc->Useu, pcgUser);
		//n�chste freie Datensatz-Urno ermitteln und speichern
		polAcc->Urno = ogBasicData.GetNextUrno();
		//Home Airport
		strcpy(polAcc->Hopo,pcgHome);
		//Mitarbeiter Urno
		polAcc->Stfu = StfUrno;
		//Mitarbeiter Personalnummer
		polStf = ogStfData.GetStfByUrno(StfUrno);
		if(polStf != NULL)
			strcpy(polAcc->Peno, polStf->Peno);
		//Type
		strcpy(polAcc->Type, opType.GetBuffer(0));
		//Jahr
		strcpy(polAcc->Year, opYear.GetBuffer(0));
		//Max.Eins�tze/Stunden
		switch (atoi(opMonth)) {
			case 1:
				strcpy(polAcc->Co01, opValue.GetBuffer(0));
				break;
			case 2:
				strcpy(polAcc->Co02, opValue.GetBuffer(0));
				break;
			case 3:
				strcpy(polAcc->Co03, opValue.GetBuffer(0));
				break;
			case 4:
				strcpy(polAcc->Co04, opValue.GetBuffer(0));
				break;
			case 5:
				strcpy(polAcc->Co05, opValue.GetBuffer(0));
				break;
			case 6:
				strcpy(polAcc->Co06, opValue.GetBuffer(0));
				break;
			case 7:
				strcpy(polAcc->Co07, opValue.GetBuffer(0));
				break;
			case 8:
				strcpy(polAcc->Co08, opValue.GetBuffer(0));
				break;
			case 9:
				strcpy(polAcc->Co09, opValue.GetBuffer(0));
				break;
			case 10:
				strcpy(polAcc->Co10, opValue.GetBuffer(0));
				break;
			case 11:
				strcpy(polAcc->Co11, opValue.GetBuffer(0));
				break;
			case 12:
				strcpy(polAcc->Co12, opValue.GetBuffer(0));
				break;
		}		
		ogAccData.Insert(polAcc);
	}
	return;
}

void CStaffTermDlg::OnMultipleInput() 
{
	// TODO: Add your control notification handler code here
	CMultipleInput dlgMultipleInput;
	
	//SaveMonth();
	
	//dlgMultipleInput.iLastInput = iLastInput;
	dlgMultipleInput.ctValidInput = ctValidInput;
	dlgMultipleInput.StfUrno = StfUrno;
	dlgMultipleInput.bGav = bGav;
	int response = dlgMultipleInput.DoModal();
	/*if (response == IDOK){
		//SaveMonth();
		NewMonth();
	}*/

}

bool CStaffTermDlg::IsWeekend(COleDateTime opDate)
{
CCS_TRY;
	// Wochentag pr�fen
	switch(opDate.GetDayOfWeek())
	{
	case 1: // Sonntag
	case 7: // Sonnabend
		return true;
	default: // anderer Tag
		return false;
	}
CCS_CATCH_ALL;
	return false;
}

bool CStaffTermDlg::IsWeekend(CString opDateString)
{
CCS_TRY;
	// f�r Datumskonvertierung
	COleDateTime opDate;
	if (!DateStringToOleDateTime(opDateString,opDate)) return false; // ung�ltiger String
	return IsWeekend(opDate);
CCS_CATCH_ALL;
	return false;
}

bool CStaffTermDlg::DateStringToOleDateTime(CString opDate, COleDateTime &opTime)
{
CCS_TRY;
	if ((opDate == "") || (opDate.GetLength() < 8) || (opDate.SpanExcluding("0123456789") != "")){
		// <opDate> ung�ltig -> terminieren
		return false;
	}
	
	// COleDateTime-Objekt aus <opDate> erzeugen...
	COleDateTime olTime(atoi(opDate.Left(4).GetBuffer(0)), // Jahr (YYYY)
						atoi(opDate.Mid(4,2).GetBuffer(0)), // Monat (MM)
						atoi(opDate.Mid(6,2).GetBuffer(0)), // Tag (DD)
						0,0,0);	// Stunde, Minute, Sekunde
	// ...und kopieren
	opTime = olTime;
	return true;
CCS_CATCH_ALL;
	return false;
}
