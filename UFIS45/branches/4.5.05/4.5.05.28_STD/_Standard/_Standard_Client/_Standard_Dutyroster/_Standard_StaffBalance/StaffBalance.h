// StaffBalance.h : main header file for the STAFFBALANCE application
//

#if !defined(AFX_TESTAPPL_H__EA4946E5_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
#define AFX_TESTAPPL_H__EA4946E5_1ACF_11D4_8FA9_00010204A53B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols
#include <BasicData.h>

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceApp:
// See StaffBalance.cpp for the implementation of this class
//

class CStaffBalanceApp : public CWinApp
{
public:
	CStaffBalanceApp();

private:
	void CheckParameters();
	void InitialLoad();
	void LoadParameters();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaffBalanceApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CStaffBalanceApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//***************************************************************************
// About Dialog
//***************************************************************************

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_StcUser;
	CStatic	m_StcServer;
	CStatic	m_StcLogintime;
	CStatic	m_Copyright4;
	CStatic	m_Copyright3;
	CStatic	m_Copyright2;
	CStatic	m_Copyright1;
	CStatic	m_Server;
	CStatic	m_User;
	CStatic	m_Logintime;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

extern CStringArray  ogCmdLineStghArray;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTAPPL_H__EA4946E5_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
