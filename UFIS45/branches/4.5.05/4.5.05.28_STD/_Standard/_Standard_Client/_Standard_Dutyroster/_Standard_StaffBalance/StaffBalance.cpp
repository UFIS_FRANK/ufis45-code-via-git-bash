// StaffBalance.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <StaffBalance.h>

#include <CCSParam.h>		// Parameterklasse
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CViewer.h>
#include <PrivList.h>
#include <Ufis.h>
#include <CedaBasicData.h>
#include <CedaSystabData.h>
#include <GUILng.h>

#include <LoginDlg.h>		// Login Dialog
#include <ListBoxDlg.h>		// Ausgabe neuer Parameter in diesem Dielog
#include <RegisterDlg.h>	// Dialog zum Registrieren der Anwendung
#include <InitialLoadDlg.h>	// Dialog zum auflisten der der geladenen Stammdeten
#include <ChoiceDlg.h>

#include <CedaStfData.h>
#include <CedaSorData.h>
#include <CedaAccData.h>
#include <CedaTdaData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStringArray  ogCmdLineStghArray;

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceApp

BEGIN_MESSAGE_MAP(CStaffBalanceApp, CWinApp)
	//{{AFX_MSG_MAP(CStaffBalanceApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceApp construction

CStaffBalanceApp::CStaffBalanceApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CStaffBalanceApp object

CStaffBalanceApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceApp initialization

BOOL CStaffBalanceApp::InitInstance()
{
	AfxEnableControlContainer();
	if (!AfxSocketInit())
	{
		AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		return FALSE;
	}

	GXInit();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//***************************************************************************
	// Parameter�bergabe durch Aufruf aus einem anderen Programm
	//***************************************************************************
	ogCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password"
	if(olCmdLine.GetLength() == 0)
	{
		ogCmdLineStghArray.Add(ogAppName);
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
	}
	else
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING31),"StaffBalance",MB_ICONERROR);
			return FALSE;
		}
	}
	//---------------------------------------------------------------------------
	/*Beispiel zum aufrufen der Applikation mit Parameter
	  f�r das Programm aus dem heraus aufgerufen wird
	
		char *args[4];
		char slRunTxt[256];
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s,%s,%s",Applikationsname,Username,Passwort,Free);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,"StaffBalance.exe",args);
	*/
	//---------------------------------------------------------------------------
	//***************************************************************************
	// Parameter�bergabe Ende
	//***************************************************************************

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Ufis"));

	//LoadStdProfileSettings();  // Load standard INI file options (including MRU)


	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

	GetPrivateProfileString(pcgAppName, "SCRIPT", "DEFAULT", pcgScript, sizeof pcgScript, pclConfigPath);
	
	// CedaBasicData Objekt initialisieren
	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetHomeAirport(CString(pcgHome));

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);


	// Standard Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	// INIT Tablenames and Homeairport
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	ogCommHandler.SetAppName(ogAppName);

    if(ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom ") + ogCommHandler.LastError());
        return FALSE;
    }

	//***********************************************************************************************
	// Initialisierung der GUI-Sprache
	//***********************************************************************************************
	// in ceda.ini einf�gen !!!!!!!!
	// ;'DE','US','IT' oder 'DE,Test' etc.
	// LANGUAGE=DE
	char lng[128];
	char dbparam[128];

	CGUILng* ogGUILng = CGUILng::TheOne();

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", lng, sizeof lng, pclConfigPath);

    int ret = GetPrivateProfileString(ogAppName, "DBParam", "", dbparam, sizeof dbparam, pclConfigPath);
	CString Param = dbparam;
	if (Param.IsEmpty())
	{
		Param = "0";
	}

	CString Error	= "";
	CStringArray olApplArray;
	olApplArray.Add(ogAppl);
	bool rdr = ogGUILng->MemberInit(&Error, CString(pcgUser), &olApplArray, CString(pcgHome), CString(lng), Param);

	if (!rdr)
	{
		CString tmp = "Languagesupport failed!\n"+Error+"parameter empty!\n";
        AfxMessageBox(tmp);
		TRACE("\nMultiLng-Support failed!\n %sparameter empty!!!\n",Error);
	}
	//***********************************************************************************************
	// ENDE der Initialisierung der GUI-Sprache
	//***********************************************************************************************

	//--------------------------------------------------------------------------------------
	// Register your broadcasts here
	CString olTableName;
	CString olTableExt = CString( pcgTableExt);
	// RT : Read Table
	// IRT: Insert Record Table
	// URT: Update Record Table
	// DRT: Delete Record Table

	olTableName = CString("TDA") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_TDA_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_TDA_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_TDA_DELETE, true);
	
	// DEMO
	//olTableName = CString("STF") + olTableExt;
	//ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_STF_NEW,    true);
	//ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_STF_CHANGE, true);
	//ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_STF_DELETE, true);
	// DEMO ENDE
	
	// Login Dialog aufrufen
	// pcgHome,ogAppName und ogAppl sind global definiert und hardcodiert.

	CLoginDialog olLoginDlg(pcgHome,ogAppName,NULL);
	if(ogCmdLineStghArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) == false)
		{
			return FALSE;
		}
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK; 
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		} 
	}

	// Login Zeit setzen
	ogLoginTime = CTime::GetCurrentTime();

	// Ruft offensichtlich den "hochlade" Dialog auf
    InitialLoad();

	// Pr�ft die geladenen Parameter
	CheckParameters();

	ogBasicData.SetLocalDiff();
	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich(); 

	CChoiceDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.

	return FALSE;
}


//****************************************************************************
// Hier werden die Daten von der Datenbank in die Application geladen
//****************************************************************************


void CStaffBalanceApp::InitialLoad()
{
	pogInitialLoad = new CInitialLoadDlg();
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1362));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;

	/////////////////////////////////////////////////////////////////////////////////////
	// add here your cedaXXXdata read methods
	/////////////////////////////////////////////////////////////////////////////////////
	//------------------------------------------------------------------------------------
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogStfData.SetTableName(CString(CString("STF") + CString(pcgTableExt)));
	ogSorData.SetTableName(CString(CString("SOR") + CString(pcgTableExt)));
	ogTdaData.SetTableName(CString(CString("TDA") + CString(pcgTableExt)));
	ogAccData.SetTableName(CString(CString("ACC") + CString(pcgTableExt)));

	//--------------------------------------------------------------------------------------
	//Update Sync because a crashed apllication could have set this state
	CTime olCurrentTime; 
	CTime olToTime;
	olCurrentTime = CTime::GetCurrentTime();
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";

	//SYNC END------------------------------------------------------------------------

	ogCfgData.ReadCfgData();
	ogCfgData.ReadMonitorSetup();

	//---------------------------------------------------------------------------------
	// Start Reading
	
	CString olSizeText;
	int ilTmpLoadSize = 0;
	int ilPercent = 100 / 5; //Teiler ist gleich Anzahl der zu lesenden Tabellen
	

	//Parameter mittels CCSParam Klasse einlesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1844));
	olSizeText.Format(" ........ %d",ogCCSParam.BufferParams(ogRoster));
	
	// Parameter testen
	ogCCSParam.BufferParams(ogRoster);
	LoadParameters();
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	ogStfData.Read();
	olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	pogInitialLoad->SetMessage(LoadStg(IDS_ORGEINHEIT));
	ogBCD.SetObject("ORG");
	ogBCD.SetObjectDesc("ORG", LoadStg(IDS_ORGEINHEIT));
	ogBCD.Read(CString("ORG"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("ORG"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	pogInitialLoad->SetMessage(LoadStg(IDS_ZUORDNUNGEN));
	ogSorData.Read();
	ilTmpLoadSize += ogSorData.omData.GetSize();
	olSizeText.Format(" ........ %d",ilTmpLoadSize);
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	//pogInitialLoad->SetMessage(LoadStg(IDS_ZUORDNUNGEN));
	ogTdaData.Read();
	ilTmpLoadSize += ogTdaData.omData.GetSize();
	olSizeText.Format(" ........ %d",ilTmpLoadSize);
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// DEMO CedaXxxData
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	//ogStfData.Read();
	//olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	//pogInitialLoad->SetProgress(ilPercent);

	// DEMO ogBCD
	//pogInitialLoad->SetMessage("Test");
	//ogBCD.SetObject("GAT");
	//ogBCD.SetObjectDesc("GAT", LoadStg(IDS_STRING1796));
	//ogBCD.AddKeyMap("GAT", "GNAM");
	//ogBCD.Read(CString("GAT"));
	//olSizeText.Format(" ........ %d",ogBCD.GetDataCount("GAT"));
	//pogInitialLoad->SetMessage(olSizeText, false);
	//pogInitialLoad->SetProgress(ilPercent);
	// DEMO ENDE

	Sleep(1000);
	//-- End Reading
	pogInitialLoad->SetProgress(100);
	Sleep(1500);
	// globale Funktion ?

	// Hochlade Dialog beenden
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}
}


//****************************************************************************
// Parameter Werte pr�fen und ggf. Defaultwerte setzen
//****************************************************************************

void CStaffBalanceApp::LoadParameters()
{
	CString olStringNow = ("19501010101010");

	CString olDefaultValidFrom	= olStringNow;
	CString olDefaultValidTo	= "";
	
	//uhi 19.3.01
	ogCCSParam.GetParam(ogRoster,"ID_YEARFREE",olStringNow,"60","Minimale Anzahl der freien Tage pro Jahr","Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	
	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.

	// DEMO
	//ogCCSParam.GetParam(ogAppl,"ID_BLANK_CHAR",olStringNow,"-","Default Zeichen","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	// DEMO ENDE
}




//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

// App command to run the dialog
void CStaffBalanceApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Statics einstellen
	m_Copyright1.SetWindowText(LoadStg(IDS_COPYRIGHT1));
	m_Copyright2.SetWindowText(LoadStg(IDS_COPYRIGHT2));
	m_Copyright3.SetWindowText(LoadStg(IDS_COPYRIGHT3));
	m_Copyright4.SetWindowText(LoadStg(IDS_COPYRIGHT4));
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));

	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);

	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));

	SetWindowText(LoadStg(IDS_APPL_ABOUTBOX));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CStaffBalanceApp::CheckParameters()
{
	//--------------------------------------------------------------------------------
	// Sollten neue Parameter eingef�gt worden sein, Meldung ausgeben.
	CStringList* polMessageList = ogCCSParam.GetMessageList();
	if (polMessageList->GetCount() != 0)
	{
		CListBoxDlg olDlg(polMessageList, LoadStg(IDS_STRING1846),LoadStg(IDS_STRING1847));   
		olDlg.DoModal();
	}
	//--------------------------------------------------------------------------------
}
