// cedadeldata.cpp - Class for handling MSD data
//

#include <stdafx.h>

// Das globale Objekt
CedaDelData ogDelData;

/*************************************************************************************
sorting functions
*************************************************************************************/
static int CompareBsdu(const DELDATA **e1, const DELDATA **e2)
{
	if((**e1).Bsdu>(**e2).Bsdu) 
		return 1;
	else if((**e1).Bsdu<(**e2).Bsdu) 
		return -1;

	return 0;
}

static int CompareDelf(const DELDATA **e1, const DELDATA **e2)
{
	int iCmp = strcmp((**e1).Delf,(**e2).Delf);
	if(iCmp > 0)
		return 1;
	else if(iCmp < 0)
		return -1;

	return 0;
}

//************************************************************************************ 
//
//************************************************************************************

CedaDelData::CedaDelData()
{                  
    // Create an array of CEDARECINFO 
    BEGIN_CEDARECINFO(DELDATA, DelDataRecInfo)
		FIELD_LONG		(Urno, "URNO")
		FIELD_LONG		(Bsdu, "BSDU")	
		FIELD_CHAR_TRIM	(Dpt1, "DPT1")	
		FIELD_CHAR_TRIM	(Fctc, "FCTC")	
		FIELD_CHAR_TRIM	(Delf, "DELF")	
		FIELD_CHAR_TRIM	(Delt, "DELT")	
		FIELD_CHAR_TRIM	(Sdac, "SDAC")	
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DelDataRecInfo)/sizeof(DelDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DelDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"DEL");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"URNO,BSDU,DPT1,FCTC,DELF,DELT,SDAC");

	 pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	ClearAll();
}

//************************************************************************************
//
//************************************************************************************

void CedaDelData::Register(void)
{
}

//************************************************************************************
//
//************************************************************************************

CedaDelData::~CedaDelData()
{
	//ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	omRecInfo.DeleteAll();
}	

//************************************************************************************
// Einlesen der gesamten Informationen (???) 
//************************************************************************************

bool CedaDelData::Read(char *pspWhere /*NULL*/)
{
	bool blRc = true;
	//char sDay[3]="";
	//long Count = 0;

	// alle Daten l�schen
	ClearAll();
    // Select data from the database
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
			return false;
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
			return false;
	}

	for (int ilCountRecord = 0; blRc == true; ilCountRecord++)
    {
		DELDATA *prlDel = new DELDATA;

		if ((blRc = GetFirstBufferRecord2(prlDel))==true) 
		{
			if(IsValidDel(prlDel))
			{
				prlDel->IsChanged = DATA_UNCHANGED;
				if(InsertInternal(prlDel)==false)
				{
					if(IsTraceLoggingEnabled())
					{
						GetDefectDataString(ogRosteringLogText, (void*)prlDel);
						WriteInRosteringLog(LOGFILE_TRACE);
					}
					delete prlDel;
					continue;
				}
#ifdef TRACE_FULL_DATA
				else
				{
					// Datensatz OK, loggen if FULL
					ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
					GetDataFormatted(ogRosteringLogText, prlDel);
					WriteLogFull("");
				}
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlDel;		// defect
			}
		}
		else
		{
			delete prlDel;
			//TRACE("Deleted extra prl\n");
		}
	}
	ClearFastSocketBuffer();	

	// wir sortieren alles nach Bsdu - das beschleunigt das Lesen zigfach
	omData.Sort(CompareBsdu);
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//************************************************************************************
// l�schen aller Daten diese Klasse
//************************************************************************************

bool CedaDelData::ClearAll()
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
    return true;
}

//************************************************************************************
//
//************************************************************************************

bool CedaDelData::InsertInternal(DELDATA *prpDel, bool bpWithDDX /*= false*/)
{
	DELDATA *prlDel = NULL;
	if(omUrnoMap.Lookup((void *)prpDel->Urno,(void *&)prlDel)==FALSE)
	{
		omData.Add(prpDel);
		omUrnoMap.SetAt((void *)prpDel->Urno,prpDel);

		if(bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,DEL_NEW,(void *)prpDel);
		}
		return true;
	}
	else
	{
		return false;
	}
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaDelData::InsertDel(DELDATA *prpDELDATA, BOOL bpWithSave)
{
	bool olRc = true;

	//prpMSDDATA->IsChanged = DATA_NEW;
	if(prpDELDATA->IsChanged == DATA_UNCHANGED)
	{
		prpDELDATA->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		SaveDel(prpDELDATA);
	}
	ogDdx.DataChanged((void *)this,DEL_CHANGE,(void *)prpDELDATA);
	return olRc;
}

bool CedaDelData::DelExist(long Urno)
{
	// don't read from database anymore, just check internal array
	DELDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
}

//**********************************************************************************
// 
//**********************************************************************************

void  ProcessDelCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
//	ogDelData.ProcessDelBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//**********************************************************************************
// Hier werden die Broadcasts behandelt
//**********************************************************************************

void  CedaDelData::ProcessDelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

}

//**********************************************************************************
// 
//**********************************************************************************

DELDATA * CedaDelData::GetDelByUrno(char pcpUrno)
{
	DELDATA *prpDel;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpDel) == TRUE)
	{
		return prpDel;
	}
	return NULL;
}

/*********************************************************************************************
GetDelListByBsdu: sucht alle Datens�tze mit Bsdu
bpGetAbscenses - true - Abwesenheiten, - false - Abweichungen (Funktions-)
wenn popDrrList == 0, wird keine Liste ausgef�llt, nur die Anzahl Datens�tze zur�ckgegeben
popDrrList ist nach Delf sortiert
R�ckgabe:	Anzahl der gefundenen Datens�tze
*********************************************************************************************/

int CedaDelData::GetDelListByBsdu(long lpBsdu, bool bpGetAbscenses, CCSPtrArray<DELDATA> *popDelList/*=0*/)
{
	DELDATA *polDel;

	int ilCount = FindFirstOfBsdu(lpBsdu);
	if(ilCount == -1) return 0;	// lpBsdu nicht gefunden

	int iNumber=0;
	// Alle passende Dels auslesen
	for(; ilCount<omData.GetSize(); ilCount++)
	{
		polDel = &omData[ilCount];
		if(polDel->Bsdu != lpBsdu)
			break;	// omData ist sortiert

		// alle Datens�tze mit Vorgabe vergleichen und wenn gleich - hinzuf�gen
		if(bpGetAbscenses)
		{
			if(strlen(polDel->Sdac) > 0 && ogOdaData.GetOdaBySdac(CString(polDel->Sdac)) > 0)
			{
				iNumber++;
				if(popDelList != 0)
				{
					popDelList->Add((void*)polDel);
				}
			}
		}
		else
		{
			if (strlen(polDel->Fctc) > 0 && ogPfcData.GetPfcByFctc(CString(polDel->Fctc)) > 0)
			{
				iNumber++;
				if(popDelList != 0)
				{
					popDelList->Add((void*)polDel);
				}
			}
		}
	}

	if(popDelList != 0)
	{
		popDelList->Sort(CompareDelf);
	}

	return iNumber;
}

/*********************************************************************************************
HasDel: Pr�ft, ob Satz mit Bsdu und Sdac vorhanden 
R�ckgabe:	true -> Satz gefunden
			false -> kein Satz
*********************************************************************************************/
bool CedaDelData::HasDel(long lpBsdu, CString opSdac, CString opAbfr, CString opAbto)
{
	DELDATA *prpDel;
	long llUrno;
	
	for(POSITION olPos = omUrnoMap.GetStartPosition(); olPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(olPos, (void *&)llUrno, (void*&)prpDel);
		if(!olPos) break;
		if(!prpDel) continue;

		// alle Datens�tze mit Vorgabe vergleichen und wenn gleich - hinzuf�gen
		if(prpDel->Bsdu == lpBsdu && (strcmp(prpDel->Sdac, opSdac) == 0) && (strcmp(prpDel->Delf, opAbfr) == 0) && (strcmp(prpDel->Delt, opAbto) == 0)){
				// einen Eintrag gefunden, ob er einer bekannten Abwesenheit entspricht?
				if(!ogOdaData.GetOdaBySdac(CString(prpDel->Sdac))) continue;
					return true;
		}
	}
	
	
	return false;
}

/*********************************************************************************************
HasDel: Pr�ft, ob Satz mit Bsdu und Org und Fctc vorhanden 
R�ckgabe:	true -> Satz gefunden
			false -> kein Satz
*********************************************************************************************/
bool CedaDelData::HasDel(long lpBsdu, const CString& ropOrg,const CString& ropFctc,const CString& ropAbfr,const CString& ropAbto)
{
	DELDATA *prpDel;
	long llUrno;
	
	for(POSITION olPos = omUrnoMap.GetStartPosition(); olPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(olPos, (void *&)llUrno, (void*&)prpDel);
		if(!olPos) break;
		if(!prpDel) continue;

		// alle Datens�tze mit Vorgabe vergleichen und wenn gleich - hinzuf�gen
		if(prpDel->Bsdu == lpBsdu && (strcmp(prpDel->Dpt1, ropOrg) == 0) && (strcmp(prpDel->Fctc,ropFctc) == 0) && (strcmp(prpDel->Delf, ropAbfr) == 0) && (strcmp(prpDel->Delt, ropAbto) == 0))
		{
			return true;
		}
	}
	
	
	return false;
}

//**********************************************************************************
// 
//**********************************************************************************
/*

bool CedaDelData::DeleteMsdInternal(DELDATA *prpDel)
{
	omUrnoMap.RemoveKey((void *)prpDel->Urno);

	int ilSdtCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSdtCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpDel->Urno)
		{
			omData.DeleteAt(ilCountRecord);
			break;
		}
	}
    return true;
}*/

//**********************************************************************************
// 
//**********************************************************************************

bool CedaDelData::SaveDel(DELDATA *prpDel)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpDel->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDel->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDel);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDel->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDel->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDel);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDel->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDel->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
		break;
	}
   return olRc;
}
 
//**********************************************************************************
// This function has two different behaviors:
// 1. If the parameter popGhdList is set and has data ==> only these Data are released
// 2. No Parameter ==> All DEL-Data are released
//**********************************************************************************

void CedaDelData::Release(CCSPtrArray<DELDATA> *popList)
{
	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;
	//struct _timeb timebuffer;
	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(ogDelData.pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;
	if(popList == NULL) //==> Dann alle Speichern
	{
		olOrigFieldList = CString(ogDelData.pcmListOfFields);
		olInsertString = olIRT + CString(ogDelData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogDelData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		POSITION rlPos;
		for ( rlPos = ogDelData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DELDATA *prlDel;
			ogDelData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDel);
			if(prlDel->IsChanged != DATA_UNCHANGED )
			{
				CString olListOfData;
				CString olCurrentUrno;
				olCurrentUrno.Format("%d",prlDel->Urno);
				MakeCedaData(&omRecInfo,olListOfData,prlDel);
				switch(prlDel->IsChanged)
				{
				case DATA_NEW:
					olInsertString += olListOfData + CString("\n");
					ilCurrentCount++;
					ilNoInserts++;
					break;
				case DATA_CHANGED:
					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoUpdates++;
					ilCurrentCount++;
					break;
				case DATA_DELETED:
					olDeleteString += olCurrentUrno + CString("\n");
					ilCurrentCount++;
					ilNoDeletes++;
					break;
				}
				prlDel->IsChanged = DATA_UNCHANGED;
			}
			if(ilCurrentCount == 50)
			{
				if(ilNoInserts > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","QUICK"/*"LATE"*/,"",olInsertString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoUpdates > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","QUICK"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoDeletes > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","QUICK"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				ilCurrentCount = 0;
				ilNoInserts = 0;
				ilNoUpdates = 0;
				ilNoDeletes = 0;
				olInsertString = olIRT + CString(ogDelData.pcmListOfFields) + CString("\n");
				olUpdateString = olURT + CString(ogDelData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
				olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			}
		}//for(rlPos = ......
		if(ilNoInserts > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","QUICK"/*"LATE"*/,"",olInsertString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoUpdates > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","QUICK"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoDeletes > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","QUICK"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
	}
	else
	{
		olOrigFieldList = CString(ogDelData.pcmListOfFields);
		olInsertString = olIRT + CString(ogDelData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogDelData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		for ( int i = 0; i < popList->GetSize(); i++)
		{
			//DELDATA *prlMsd = GetMsdByUrno(popList->GetAt(i).Urno);
			DELDATA rlDEL = popList->GetAt(i);
			//if(prlMsd != NULL)
			{
				if(rlDEL.IsChanged != DATA_UNCHANGED)
				{
					CString olListOfData;
					CString olCurrentUrno;
					olCurrentUrno.Format("%d",rlDEL.Urno);
					MakeCedaData(&omRecInfo,olListOfData,&rlDEL);
					switch(rlDEL.IsChanged)
					{
					case DATA_NEW:
						olInsertString += olListOfData + CString("\n");
						ilNoInserts++;
						ilCurrentCount++;
						break;
					case DATA_CHANGED:
						olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
						ilNoUpdates++;
						ilCurrentCount++;
						break;
					case DATA_DELETED:
						olDeleteString += olCurrentUrno + CString("\n");
						ilNoDeletes++;
						ilCurrentCount++;
						break;
					}
					rlDEL.IsChanged = DATA_UNCHANGED;
					if(ilCurrentCount == 50)
					{
						if(ilNoInserts > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","QUICK"/*"LATE"*/,"",olInsertString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoUpdates > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","QUICK"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoDeletes > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","QUICK"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						ilCurrentCount = 0;
						ilNoInserts = 0;
						ilNoUpdates = 0;
						ilNoDeletes = 0;
						olInsertString = olIRT + CString(ogDelData.pcmListOfFields) + CString("\n");
						olUpdateString = olURT + CString(ogDelData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
						olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			
					}
				}
			}
		}//for(rlPos = ......
		if(ilCurrentCount > 0)
		{
			if(ilNoInserts > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","QUICK"/*"LATE"*/,"",olInsertString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","QUICK"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","QUICK"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogDelData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
		}
	}
}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaDelData::UpdateDelInternal(DELDATA *prpDel)
{
	
	bool blRet = true;
	/*
	if(prpDel!=NULL)
	{
		DELDATA* prlDel= GetDelByUrno(prpDel->Urno);
		if(prlDel==NULL)
		{
			blRet = false;
		}
		else
		{
			*prlDel = *prpDel;
		}
	}
	else
	{
		blRet = false;
	}
	*/
    return blRet;
}

//**********************************************************************************
// 
//**********************************************************************************
/*
bool CedaDelData::GetSelectedMsd(CCSPtrArray<DELDATA> *popSelectedMsd)
{
	int ilCount = omSelectedData.GetUpperBound();
	
	for(int k=0;k<= ilCount;k++)
	{
		long llUrno = omSelectedData.GetAt(k);
		DELDATA *prlMsd = GetMsdByUrno(llUrno);
		if(prlMsd!=NULL)
		{
			popSelectedMsd->NewAt(0,*prlMsd);
		}
	}
	return (popSelectedMsd->GetSize()>0);
}*/

//******************************************************************************************
// Es werden nur bestimmte Datens�tze eingelesen
//******************************************************************************************

bool CedaDelData::ReadSpecialData(CCSPtrArray<DELDATA> *popDel, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	// Ist die �bergebende Feldliste g�ltig
	if(strlen(pspFieldList) > 0)
	{
		// Hier wird angegeben welche Felder gelesen werden sollen
		// Bei * werden alle genommen
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			// �bergebene Feldliste wird benutzt
			strcpy(pclFieldList, pspFieldList);
		}
	}

	// Hier finded offensichtlich das Lesen der Daten aus der Datenbank statt..
	// Wohin gehen die Daten ?
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}

	// Wenn der Pointer Array g�ltig ist,
	if(popDel != NULL)
	{
		// Solange wie der R�ckgabewert TRUE ist weitermachen
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			// Einen SDT Datensatz erzeugen.
			DELDATA *prpDel = new DELDATA;
			// Datensatz f�llen
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpDel,CString(pclFieldList))) == true)
			{
				// Datensatz ins Array einf�gen
				popDel->Add(prpDel);
			}
			else
			{
				// letzten Datensatz l�schen, falls ung�ltig
				delete prpDel;
			}
		}

	// Wenn nichts eingetragen
		if(popDel->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}
	
/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit der angegebenen Bsdu
! wir nutzen die Tatsache, das omData nach Bsdu & Fctc sortiert ist !
*****************************************************************************/
int CedaDelData::FindFirstOfBsdu(long lpBsdu)
{
	int ilSize = omData.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;
	if(omData[ilSize-1].Bsdu != omData[0].Bsdu)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpBsdu == omData[ilSearch].Bsdu)
				{
					if(!ilSearch || lpBsdu != omData[ilSearch-1].Bsdu)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(lpBsdu != omData[ilSearch-1].Bsdu)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Bsdu in der Liste vorhanden!
						return -1;
					}

					if(lpBsdu == omData[ilUp].Bsdu)
						return ilUp;
					else if(lpBsdu == omData[ilDown].Bsdu)
						return ilDown;
					else 
						return -1;
				}

				if(lpBsdu < omData[ilSearch].Bsdu)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpBsdu > omData[ilSearch].Bsdu)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omData[ilSearch].Bsdu == lpBsdu)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

/**************************************************************
**************************************************************/
bool CedaDelData::IsValidDel(DELDATA *prpDel)
{
	// long	Bsdu;		 	 	 	// Datensatznummer des BSD
	if(!ogBsdData.GetBsdByUrno(prpDel->Bsdu))
	{
		CString olErr;
		olErr.Format("Basic shift deviation (delegation or absence) Connection Table (DELTAB) URNO '%d' is defect, Shift with URNO '%d' in BSDTAB not found\n",
			prpDel->Urno, prpDel->Bsdu);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.BSDU in BSDTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// char	Sdac[SDAC_LEN+2]; 		// Abwesenheitscode (Code aus ODA)
	if(	strlen(prpDel->Sdac) > 0)
	{
		if (!ogOdaData.GetOdaBySdac(CString(prpDel->Sdac)))
		{
			CString olErr;
			olErr.Format("Absence code '%s' in Absences Table not found, DELTAB data corrupt\n",
				prpDel->Sdac);
			ogErrlog += olErr;
			
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.SDAC in ODATAB not found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}
			return false;	// 1. Defekter Datensatz
		}

		// absences neither contain org - unit nor fct 
		if (strlen(prpDel->Dpt1) > 0)
		{
			CString olErr;
			olErr.Format("DELTAB entry with Absence code '%s' must not contain organisational unit '%s'\n",
				prpDel->Sdac,prpDel->Dpt1);
			ogErrlog += olErr;
			
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.DPT1 in absence record of DELTAB found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}
		}

		// absences neither contain org - unit nor fct 
		if (strlen(prpDel->Fctc) > 0)
		{
			CString olErr;
			olErr.Format("DELTAB entry with Absence code '%s' must not contain function '%s'\n",
				prpDel->Sdac,prpDel->Fctc);
			ogErrlog += olErr;
			
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.FCTC in absence record of DELTAB found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}
		}
	}
	else
	{
		// char	Dpt1[8+2]; 				// Code der Organisationseinheit aus ORGTAB
		if(strlen(prpDel->Dpt1) > 0 && !ogOrgData.GetOrgByDpt1(prpDel->Dpt1))
		{
			CString olErr;
			olErr.Format("Organisation code '%s' in Organisational Units Table not found, DELTAB data corrupt\n",
				prpDel->Dpt1);
			ogErrlog += olErr;
			
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.DPT1 in ORGTAB not found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}
			return false;	// 1. Defekter Datensatz
		}


		// char	Fctc[5+2]; 				// Code der Funktion aus PFCTAB
		if(	strlen(prpDel->Fctc) > 0 && !ogPfcData.GetPfcByFctc(CString(prpDel->Fctc)))
		{
			CString olErr;
			olErr.Format("Function '%s' in Function Table not found, DELTAB data corrupt\n",
				prpDel->Fctc);
			ogErrlog += olErr;
			
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prpDel, "DELTAB.FCTC in PRFTAB not found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}
			return false;	// 1. Defekter Datensatz
		}
	}

	// char	Delf[4+2]; 				// Abordnung von	HHMM
	// char	Delt[4+2]; 				// Abordung bis		HHMM


	return true;
}











