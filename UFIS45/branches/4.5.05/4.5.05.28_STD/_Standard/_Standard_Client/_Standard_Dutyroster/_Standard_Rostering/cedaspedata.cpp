// CedaSpeData.cpp
 
#include <stdafx.h>

CedaSpeData ogSpeData;

void ProcessSpeCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSpeData::CedaSpeData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SPEDataStruct
	BEGIN_CEDARECINFO(SPEDATA,SPEDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SPEDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SPEDataRecInfo)/sizeof(SPEDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SPEDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SPE");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
	Register();
}

//---------------------------------------------------------------------------------------------------------

void CedaSpeData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSpeData::Register(void)
{
	DdxRegister((void *)this,BC_SPE_CHANGE,	"SPEDATA", "Spe-changed",	ProcessSpeCf);
	DdxRegister((void *)this,BC_SPE_NEW,	"SPEDATA", "Spe-new",		ProcessSpeCf);
	DdxRegister((void *)this,BC_SPE_DELETE,	"SPEDATA", "Spe-deleted",	ProcessSpeCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSpeData::~CedaSpeData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSpeData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSpeData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SPEDATA *prlSpe = new SPEDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSpe)) == true)
		{
			if(IsValidSpe(prlSpe))
			{
				omData.Add(prlSpe);//Update omData
				omUrnoMap.SetAt((void *)prlSpe->Urno,prlSpe);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSpe);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlSpe;
			}
		}
		else
		{
			delete prlSpe;
		}
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	ClearFastSocketBuffer();	
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSpeData::Insert(SPEDATA *prpSpe)
{
	prpSpe->IsChanged = DATA_NEW;
	if(Save(prpSpe) == false) return false; //Update Database
	InsertInternal(prpSpe);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSpeData::InsertInternal(SPEDATA *prpSpe)
{
	ogDdx.DataChanged((void *)this, SPE_NEW,(void *)prpSpe ); //Update Viewer
	omData.Add(prpSpe);//Update omData
	omUrnoMap.SetAt((void *)prpSpe->Urno,prpSpe);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSpeData::Delete(long lpUrno)
{
	SPEDATA *prlSpe = GetSpeByUrno(lpUrno);
	if (prlSpe != NULL)
	{
		prlSpe->IsChanged = DATA_DELETED;
		if(Save(prlSpe) == false) return false; //Update Database
		DeleteInternal(prlSpe);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSpeData::DeleteInternal(SPEDATA *prpSpe)
{
	ogDdx.DataChanged((void *)this,SPE_DELETE,(void *)prpSpe); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSpe->Urno);
	int ilSpeCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSpeCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSpe->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSpeData::Update(SPEDATA *prpSpe)
{
	if (GetSpeByUrno(prpSpe->Urno) != NULL)
	{
		if (prpSpe->IsChanged == DATA_UNCHANGED)
		{
			prpSpe->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSpe) == false) return false; //Update Database
		UpdateInternal(prpSpe);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSpeData::UpdateInternal(SPEDATA *prpSpe)
{
	SPEDATA *prlSpe = GetSpeByUrno(prpSpe->Urno);
	if (prlSpe != NULL)
	{
		*prlSpe = *prpSpe; //Update omData
		ogDdx.DataChanged((void *)this,SPE_CHANGE,(void *)prlSpe); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPEDATA *CedaSpeData::GetSpeByUrno(long lpUrno)
{
	SPEDATA  *prlSpe;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpe) == TRUE)
	{
		return prlSpe;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSpeData::ReadSpecialData(CCSPtrArray<SPEDATA> *popSpe,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSpe != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SPEDATA *prpSpe = new SPEDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSpe,CString(pclFieldList))) == true)
			{
				// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
				// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
				if(prpSpe->Vpfr.GetStatus() != COleDateTime::valid || prpSpe->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Spe: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSpe->Surn);
					delete prpSpe;
					continue;
				}
				//else
					//TRACE("Read-Spe: %d gelesen, KEIN FEHLER\n",ilCountRecord-1);
				
				
				popSpe->Add(prpSpe);
			}
			else
			{
				delete prpSpe;
			}
		}
		ClearFastSocketBuffer();	
		if(popSpe->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
		
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSpeData::Save(SPEDATA *prpSpe)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSpe->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSpe->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpe->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpe->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSpeCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSpeData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSpeData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSpeData;
	prlSpeData = (struct BcStruct *) vpDataPointer;
	SPEDATA *prlSpe;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSpeData->Cmd, prlSpeData->Object, prlSpeData->Twe, prlSpeData->Selection, prlSpeData->Fields, prlSpeData->Data,prlSpeData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_SPE_CHANGE:
		prlSpe = GetSpeByUrno(GetUrnoFromSelection(prlSpeData->Selection));
		if(prlSpe != NULL)
		{
			GetRecordFromItemList(prlSpe,prlSpeData->Fields,prlSpeData->Data);
			UpdateInternal(prlSpe);
			break;
		}
	case BC_SPE_NEW:
		prlSpe = new SPEDATA;
		GetRecordFromItemList(prlSpe,prlSpeData->Fields,prlSpeData->Data);
		InsertInternal(prlSpe);
		break;
	case BC_SPE_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSpeData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSpeData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSpe = GetSpeByUrno(llUrno);
			if (prlSpe != NULL)
			{
				DeleteInternal(prlSpe);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
//uhi 26.4.01

void CedaSpeData::GetSpeBySurn(long lpSurn,CCSPtrArray<SPEDATA> *popSpeData)
{
	SPEDATA  *prlSpe;

	popSpeData->DeleteAll();
	
	CCSPtrArray<SPEDATA> olSpeData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSpe);
		if((prlSpe->Surn == lpSurn))
			popSpeData->Add(prlSpe); 
	}
}

/******************************************************************
******************************************************************/
bool CedaSpeData::IsValidSpe(SPEDATA *popSpe)
{
	// long			Surn;			// Mitarbeiter-Urno
	if(!ogStfData.GetStfByUrno(popSpe->Surn))
	{
		CString olErr;
		olErr.Format("Qualification Connection Table (SPETAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
			popSpe->Urno, popSpe->Surn);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popSpe, "SPETAB.SURN in STFTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	// char			Code[7];		// Qualification Code

	if(popSpe->Vpfr.GetStatus() != COleDateTime::valid || popSpe->Vpto.GetStatus() == COleDateTime::invalid)
	{
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popSpe, "invalid time");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}
	return true;			
}