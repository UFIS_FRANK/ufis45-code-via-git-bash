#if !defined(AFX_INFODLG_H__BC0364D5_C67C_11D3_8FDE_00500454BF3F__INCLUDED_)
#define AFX_INFODLG_H__BC0364D5_C67C_11D3_8FDE_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : Header-Datei
//

#include <cedadrrdata.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CInfoDlg 

class CInfoDlg : public CDialog
{
// Konstruktion
public:
	CInfoDlg(CWnd* pParent, CStringArray* popStringArray);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CInfoDlg)
	enum { IDD = IDD_INFO_DLG };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	CStringArray* pomStringArray;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CInfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_INFODLG_H__BC0364D5_C67C_11D3_8FDE_00500454BF3F__INCLUDED_
