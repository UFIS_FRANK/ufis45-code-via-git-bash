// DutyAccountDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyAccountDlg 
//----------------------------------------------------------------------------------------

DutyAccountDlg::DutyAccountDlg(long lpStfUrno, CString opYear, CWnd* pParent /*=NULL*/) : CDialog(DutyAccountDlg::IDD, pParent)
{
	CCS_TRY;
	pomParent = (DutyRoster_View*)pParent;

	prmViewInfo = &pomParent->rmViewInfo;

	imSelectAccountLine = -1;


	lmStfUrno = lpStfUrno;
	omYear    = opYear;
	prmStf = ogStfData.GetStfByUrno(lmStfUrno);

	prmAccTemp = new ACCDATA;
	prmAccOrig = NULL;

	for (int i=0;i < ogBCD.GetDataCount("ADE"); i++)
	{
		if(ogBCD.GetField("ADE", i, "SHOW") == "x" &&
			 (ogBCD.GetField("ADE", i,"KTYP") == "D" ||
			  ogBCD.GetField("ADE", i,"KTYP") == "S"))
		{
			bool blAccountIsSelect = false;
			long llAccType = atol(ogBCD.GetField("ADE", i, "TYPE"));
			// Wurde das Konto in der Ansicht ausgew�hlt ?
			for (int j=0; j<prmViewInfo->oAccounts.GetSize(); j++)
			{
				if(llAccType == (long)prmViewInfo->oAccounts[j])
				{
					blAccountIsSelect = true;
					break;
				}
			}
			if (blAccountIsSelect)
			{
				ACCOUNTS* prlAccounts = new ACCOUNTS;

				prlAccounts->lTYPE = llAccType;
				prlAccounts->oNAME = ogBCD.GetField("ADE",i,"NAME");
				prlAccounts->oSNAM = ogBCD.GetField("ADE",i,"SNAM");
				prlAccounts->oKTYP = ogBCD.GetField("ADE",i,"KTYP");
				prlAccounts->oFTYP = ogBCD.GetField("ADE",i,"FTYP");
				prlAccounts->iForm = atoi(ogBCD.GetField("ADE",i,"FORM"));
				prlAccounts->oDPER = ogBCD.GetField("ADE",i,"DPER");
				if(ogBCD.GetField("ADE", i, "EDIT") == "x")
					prlAccounts->bEDIT = true;
				prlAccounts->oMaskType = ogBCD.GetField("ADE",i,"MTYP");
				omAccountList.Add(prlAccounts);
			}
		}
	}
	ipCountMakeAccountMask = 0;
	//{{AFX_DATA_INIT(DutyAccountDlg)
	//}}AFX_DATA_INIT
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

DutyAccountDlg::~DutyAccountDlg()
{
	CCS_TRY;
	omAccountList.DeleteAll();
	delete prmAccTemp;
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::DoDataExchange(CDataExchange* pDX)
{
	CCS_TRY;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyAccountDlg)
	DDX_Control(pDX, IDC_C01, m_C01);
	DDX_Control(pDX, IDC_C02, m_C02);
	DDX_Control(pDX, IDC_C03, m_C03);
	DDX_Control(pDX, IDC_C04, m_C04);
	DDX_Control(pDX, IDC_C05, m_C05);
	DDX_Control(pDX, IDC_C06, m_C06);
	DDX_Control(pDX, IDC_C07, m_C07);
	DDX_Control(pDX, IDC_C08, m_C08);
	DDX_Control(pDX, IDC_C09, m_C09);
	DDX_Control(pDX, IDC_C10, m_C10);
	DDX_Control(pDX, IDC_C11, m_C11);
	DDX_Control(pDX, IDC_C12, m_C12);
	DDX_Control(pDX, IDC_CO01_TEXT, m_Co01Text);
	DDX_Control(pDX, IDC_S_CL01,    m_SCl01);
	DDX_Control(pDX, IDC_S_CL02,	m_SCl02);
	DDX_Control(pDX, IDC_S_CL03,	m_SCl03);
	DDX_Control(pDX, IDC_S_CL04,	m_SCl04);
	DDX_Control(pDX, IDC_S_CL05,	m_SCl05);
	DDX_Control(pDX, IDC_S_CL06,	m_SCl06);
	DDX_Control(pDX, IDC_S_CL07,	m_SCl07);
	DDX_Control(pDX, IDC_S_CL08,	m_SCl08);
	DDX_Control(pDX, IDC_S_CL09,	m_SCl09);
	DDX_Control(pDX, IDC_S_CL10,	m_SCl10);
	DDX_Control(pDX, IDC_S_CL11,	m_SCl11);
	DDX_Control(pDX, IDC_S_CL12,	m_SCl12);
	DDX_Control(pDX, IDC_S_CO01,	m_SCo01);
	DDX_Control(pDX, IDC_S_CO02,	m_SCo02);
	DDX_Control(pDX, IDC_S_CO03,	m_SCo03);
	DDX_Control(pDX, IDC_S_CO04,	m_SCo04);
	DDX_Control(pDX, IDC_S_CO05,	m_SCo05);
	DDX_Control(pDX, IDC_S_CO06,	m_SCo06);
	DDX_Control(pDX, IDC_S_CO07,	m_SCo07);
	DDX_Control(pDX, IDC_S_CO08,	m_SCo08);
	DDX_Control(pDX, IDC_S_CO09,	m_SCo09);
	DDX_Control(pDX, IDC_S_CO10,	m_SCo10);
	DDX_Control(pDX, IDC_S_CO11,	m_SCo11);
	DDX_Control(pDX, IDC_S_CO12,	m_SCo12);
	DDX_Control(pDX, IDC_S_OP01,	m_SOp01);
	DDX_Control(pDX, IDC_S_OP02,	m_SOp02);
	DDX_Control(pDX, IDC_S_OP03,	m_SOp03);
	DDX_Control(pDX, IDC_S_OP04,	m_SOp04);
	DDX_Control(pDX, IDC_S_OP05,	m_SOp05);
	DDX_Control(pDX, IDC_S_OP06,	m_SOp06);
	DDX_Control(pDX, IDC_S_OP07,	m_SOp07);
	DDX_Control(pDX, IDC_S_OP08,	m_SOp08);
	DDX_Control(pDX, IDC_S_OP09,	m_SOp09);
	DDX_Control(pDX, IDC_S_OP10,	m_SOp10);
	DDX_Control(pDX, IDC_S_OP11,	m_SOp11);
	DDX_Control(pDX, IDC_S_OP12,	m_SOp12);
	DDX_Control(pDX, IDC_CHECK_EDIT,	m_CheckEdit);
	DDX_Control(pDX, IDC_C_ACCOUNT, m_C_Account);
	DDX_Control(pDX, IDC_EDITSALDO, m_Saldo);
	DDX_Control(pDX, IDC_EDITSTART, m_Start);
	DDX_Control(pDX, IDC_AUBY,	m_Auby);
	DDX_Control(pDX, IDC_NAME,	m_Name);
	DDX_Control(pDX, IDC_PENO,	m_Peno);
	DDX_Control(pDX, IDC_AATO_D, m_AatoD);
	DDX_Control(pDX, IDC_AATO_T, m_AatoT);
	DDX_Control(pDX, IDC_AUDA_D, m_AudaD);
	DDX_Control(pDX, IDC_AUDA_T, m_AudaT);
	DDX_Control(pDX, IDC_CL01,	m_Cl01);
	DDX_Control(pDX, IDC_CL02,	m_Cl02);
	DDX_Control(pDX, IDC_CL03,	m_Cl03);
	DDX_Control(pDX, IDC_CL04,	m_Cl04);
	DDX_Control(pDX, IDC_CL05,	m_Cl05);
	DDX_Control(pDX, IDC_CL06,	m_Cl06);
	DDX_Control(pDX, IDC_CL07,	m_Cl07);
	DDX_Control(pDX, IDC_CL08,	m_Cl08);
	DDX_Control(pDX, IDC_CL09,	m_Cl09);
	DDX_Control(pDX, IDC_CL10,	m_Cl10);
	DDX_Control(pDX, IDC_CL11,	m_Cl11);
	DDX_Control(pDX, IDC_CL12,	m_Cl12);
	DDX_Control(pDX, IDC_CO01,	m_Co01);
	DDX_Control(pDX, IDC_CO02,	m_Co02);
	DDX_Control(pDX, IDC_CO03,	m_Co03);
	DDX_Control(pDX, IDC_CO04,	m_Co04);
	DDX_Control(pDX, IDC_CO05,	m_Co05);
	DDX_Control(pDX, IDC_CO06,	m_Co06);
	DDX_Control(pDX, IDC_CO07,	m_Co07);
	DDX_Control(pDX, IDC_CO08,	m_Co08);
	DDX_Control(pDX, IDC_CO09,	m_Co09);
	DDX_Control(pDX, IDC_CO10,	m_Co10);
	DDX_Control(pDX, IDC_CO11,	m_Co11);
	DDX_Control(pDX, IDC_CO12,	m_Co12);
	DDX_Control(pDX, IDC_OP01,	m_Op01);
	DDX_Control(pDX, IDC_OP02,	m_Op02);
	DDX_Control(pDX, IDC_OP03,	m_Op03);
	DDX_Control(pDX, IDC_OP04,	m_Op04);
	DDX_Control(pDX, IDC_OP05,	m_Op05);
	DDX_Control(pDX, IDC_OP06,	m_Op06);
	DDX_Control(pDX, IDC_OP07,	m_Op07);
	DDX_Control(pDX, IDC_OP08,	m_Op08);
	DDX_Control(pDX, IDC_OP09,	m_Op09);
	DDX_Control(pDX, IDC_OP10,	m_Op10);
	DDX_Control(pDX, IDC_OP11,	m_Op11);
	DDX_Control(pDX, IDC_OP12,	m_Op12);
	//}}AFX_DATA_MAP
	CCS_CATCH_ALL;
}


BEGIN_MESSAGE_MAP(DutyAccountDlg, CDialog)
	//{{AFX_MSG_MAP(DutyAccountDlg)
	ON_BN_CLICKED(IDC_BUTTONINFO, OnButtoninfo)
	ON_CBN_SELCHANGE(IDC_C_ACCOUNT, OnSelchangeCAccount)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyAccountDlg 
//----------------------------------------------------------------------------------------

BOOL DutyAccountDlg::OnInitDialog() 
{
	CCS_TRY;
	CDialog::OnInitDialog();

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);
	
	//------------------------------------------------------------------------
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING1705));
	//------------------------------------------------------------------------
	CString olTmp;
	if(prmStf != NULL)
	{
		olTmp = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, prmStf, "", "");
		m_Name.SetInitText(olTmp);
		m_Peno.SetInitText(prmStf->Peno);
	}

	m_Name.SetBKColor(SILVER);	
	m_Peno.SetBKColor(SILVER);	
	m_AudaD.SetBKColor(SILVER);	
	m_AudaT.SetBKColor(SILVER);	
	m_Auby.SetBKColor(SILVER);	
	m_AatoD.SetBKColor(SILVER);	
	m_AatoT.SetBKColor(SILVER);	
	m_Saldo.SetBKColor(SILVER);
	m_Start.SetBKColor(SILVER);
	m_Co01Text.SetReadOnly(TRUE);
	//------------------------------------
	// Statics Fields
	SetDlgItemText(IDC_S_NAME, LoadStg(IDS_STRING60)+CString(":"));
	SetDlgItemText(IDC_S_PENO, LoadStg(SHIFT_STF_PENO)+CString(":"));
	SetDlgItemText(IDC_S_ACCOUNT, LoadStg(IDC_S_Konto2)+CString(":"));
	SetDlgItemText(IDC_S_AUDA, LoadStg(IDS_S_Aktualisiert_am)+CString(":"));
	SetDlgItemText(IDC_S_AUBY, LoadStg(IDS_S_Aktualisiert_von)+CString(":"));
	SetDlgItemText(IDC_S_AATO, LoadStg(IDS_S_Konto_ermittelt_bis)+CString(":"));
	SetDlgItemText(IDC_S_CO01_TEXT, LoadStg(IDS_S_FreeText));
	SetDlgItemText(IDC_S_MONTH01, LoadStg(IDS_STRING61));
	SetDlgItemText(IDC_S_MONTH02, LoadStg(IDS_STRING62));
	SetDlgItemText(IDC_S_MONTH03, LoadStg(IDS_STRING63));
	SetDlgItemText(IDC_S_MONTH04, LoadStg(IDS_STRING64));
	SetDlgItemText(IDC_S_MONTH05, LoadStg(IDS_STRING65));
	SetDlgItemText(IDC_S_MONTH06, LoadStg(IDS_STRING66));
	SetDlgItemText(IDC_S_MONTH07, LoadStg(IDS_STRING67));
	SetDlgItemText(IDC_S_MONTH08, LoadStg(IDS_STRING68));
	SetDlgItemText(IDC_S_MONTH09, LoadStg(IDS_STRING69));
	SetDlgItemText(IDC_S_MONTH10, LoadStg(IDS_STRING70));
	SetDlgItemText(IDC_S_MONTH11, LoadStg(IDS_STRING71));
	SetDlgItemText(IDC_S_MONTH12, LoadStg(IDS_STRING72));
	SetDlgItemText(IDC_BUTTONINFO, LoadStg(IDC_BUTTONINFO));
	SetDlgItemText(IDOK,LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));

	//------------------------------------
	// Check-Feld Setzen Default
	// aber Abh�ngig von ADE.FTYP
	m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	m_Co01Text.SetLimitText(40);

	//------------------------------------
	//Combobox f�llen
	m_C_Account.SetFont(&ogCourier_Regular_10);
	CString olLine;
	for(int i=0; i<omAccountList.GetSize(); i++)
	{
		olLine.Format("%-8s  %-40s", omAccountList[i].oSNAM, omAccountList[i].oNAME);
		m_C_Account.AddString(olLine);
	}


	MakeAccountMask(-1);


	return TRUE;
CCS_CATCH_ALL;
return 0;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::OnOK() 
{
	CCS_TRY;
	if(CheckData(true))
		CDialog::OnOK();
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::InternalCancel()
{
	CCS_TRY;
	CDialog::OnCancel();
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::OnCancel() 
{
	CCS_TRY;
	CDialog::OnCancel();
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::OnButtoninfo() 
{
CCS_TRY;
	CStringArray olStringArray;

	olStringArray.Add(prmAccTemp->Cdat.Format("%d.%m.%Y"));
	olStringArray.Add(prmAccTemp->Cdat.Format("%H:%M"));
	olStringArray.Add(prmAccTemp->Lstu.Format("%d.%m.%Y"));
	olStringArray.Add(prmAccTemp->Lstu.Format("%H:%M"));
	olStringArray.Add(prmAccTemp->Usec);
	olStringArray.Add(prmAccTemp->Useu);

	CInfoDlg olDlg(this,&olStringArray);
	olDlg.DoModal();
CCS_CATCH_ALL;		
}
//*********************************************************************************
// 
//*********************************************************************************

void DutyAccountDlg::OnSelchangeCAccount() 
{
CCS_TRY;
	int ilSelAcc = m_C_Account.GetCurSel();
	if(ilSelAcc >= omAccountList.GetSize() || ilSelAcc == -1)
		return;

	long llAccountTypeIntern = omAccountList[ilSelAcc].lTYPE;

	if(CheckData(false))
	{
		imSelectAccountLine = ilSelAcc;
		GetAcc(llAccountTypeIntern);
		MakeAccountMask(llAccountTypeIntern);
	}
	else
	{
		imSelectAccountLine = m_C_Account.SetCurSel(imSelectAccountLine);

	}

CCS_CATCH_ALL;
}


//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::GetAcc(long lpAccount)
{
CCS_TRY;
	for (int i=0; i<omAccountList.GetSize() ; i++)
	{
		if(omAccountList[i].lTYPE == lpAccount)
		{
			rmAccount.lTYPE		= omAccountList[i].lTYPE;
			rmAccount.oNAME		= omAccountList[i].oNAME;
			rmAccount.oSNAM		= omAccountList[i].oSNAM;
			rmAccount.oKTYP		= omAccountList[i].oKTYP;
			rmAccount.oFTYP		= omAccountList[i].oFTYP;
			rmAccount.oMaskType	= omAccountList[i].oMaskType;
			rmAccount.iForm		= omAccountList[i].iForm;
			rmAccount.bEDIT		= omAccountList[i].bEDIT;
			rmAccount.oDPER		= omAccountList[i].oDPER;
			rmAccount.oBCDTable	= omAccountList[i].oBCDTable;
			rmAccount.oBCDField	= omAccountList[i].oBCDField;
			break;
		}
	}

	omType.Format("%ld",lpAccount); 

	prmAccOrig = ogAccData.GetAccByKey(lmStfUrno, omYear, omType);

	if(prmStf != NULL)
	{
		if(prmAccTemp != NULL)
		{
			ACCDATA rlAccTemp;
			*prmAccTemp = rlAccTemp;
		}

		if(prmAccOrig != NULL)
		{
			if(rmAccount.oDPER != "S")
			{
				strcpy(prmAccOrig->Op01, TrimValue(prmAccOrig->Op01, rmAccount.iForm));
				strcpy(prmAccOrig->Op02, TrimValue(prmAccOrig->Op02, rmAccount.iForm));
				strcpy(prmAccOrig->Op03, TrimValue(prmAccOrig->Op03, rmAccount.iForm));
				strcpy(prmAccOrig->Op04, TrimValue(prmAccOrig->Op04, rmAccount.iForm));
				strcpy(prmAccOrig->Op05, TrimValue(prmAccOrig->Op05, rmAccount.iForm));
				strcpy(prmAccOrig->Op06, TrimValue(prmAccOrig->Op06, rmAccount.iForm));
				strcpy(prmAccOrig->Op07, TrimValue(prmAccOrig->Op07, rmAccount.iForm));
				strcpy(prmAccOrig->Op08, TrimValue(prmAccOrig->Op08, rmAccount.iForm));
				strcpy(prmAccOrig->Op09, TrimValue(prmAccOrig->Op09, rmAccount.iForm));
				strcpy(prmAccOrig->Op10, TrimValue(prmAccOrig->Op10, rmAccount.iForm));
				strcpy(prmAccOrig->Op11, TrimValue(prmAccOrig->Op11, rmAccount.iForm));
				strcpy(prmAccOrig->Op12, TrimValue(prmAccOrig->Op12, rmAccount.iForm));

				strcpy(prmAccOrig->Co01, TrimValue(prmAccOrig->Co01, rmAccount.iForm));
				strcpy(prmAccOrig->Co02, TrimValue(prmAccOrig->Co02, rmAccount.iForm));
				strcpy(prmAccOrig->Co03, TrimValue(prmAccOrig->Co03, rmAccount.iForm));
				strcpy(prmAccOrig->Co04, TrimValue(prmAccOrig->Co04, rmAccount.iForm));
				strcpy(prmAccOrig->Co05, TrimValue(prmAccOrig->Co05, rmAccount.iForm));
				strcpy(prmAccOrig->Co06, TrimValue(prmAccOrig->Co06, rmAccount.iForm));
				strcpy(prmAccOrig->Co07, TrimValue(prmAccOrig->Co07, rmAccount.iForm));
				strcpy(prmAccOrig->Co08, TrimValue(prmAccOrig->Co08, rmAccount.iForm));
				strcpy(prmAccOrig->Co09, TrimValue(prmAccOrig->Co09, rmAccount.iForm));
				strcpy(prmAccOrig->Co10, TrimValue(prmAccOrig->Co10, rmAccount.iForm));
				strcpy(prmAccOrig->Co11, TrimValue(prmAccOrig->Co11, rmAccount.iForm));
				strcpy(prmAccOrig->Co12, TrimValue(prmAccOrig->Co12, rmAccount.iForm));

				strcpy(prmAccOrig->Cl01, TrimValue(prmAccOrig->Cl01, rmAccount.iForm));
				strcpy(prmAccOrig->Cl02, TrimValue(prmAccOrig->Cl02, rmAccount.iForm));
				strcpy(prmAccOrig->Cl03, TrimValue(prmAccOrig->Cl03, rmAccount.iForm));
				strcpy(prmAccOrig->Cl04, TrimValue(prmAccOrig->Cl04, rmAccount.iForm));
				strcpy(prmAccOrig->Cl05, TrimValue(prmAccOrig->Cl05, rmAccount.iForm));
				strcpy(prmAccOrig->Cl06, TrimValue(prmAccOrig->Cl06, rmAccount.iForm));
				strcpy(prmAccOrig->Cl07, TrimValue(prmAccOrig->Cl07, rmAccount.iForm));
				strcpy(prmAccOrig->Cl08, TrimValue(prmAccOrig->Cl08, rmAccount.iForm));
				strcpy(prmAccOrig->Cl09, TrimValue(prmAccOrig->Cl09, rmAccount.iForm));
				strcpy(prmAccOrig->Cl10, TrimValue(prmAccOrig->Cl10, rmAccount.iForm));
				strcpy(prmAccOrig->Cl11, TrimValue(prmAccOrig->Cl11, rmAccount.iForm));
				strcpy(prmAccOrig->Cl12, TrimValue(prmAccOrig->Cl12, rmAccount.iForm));
			}

			*prmAccTemp = *prmAccOrig;
		}
		else
		{
			prmAccTemp->Stfu = lmStfUrno;
			strcpy(prmAccTemp->Type, omType);
			strcpy(prmAccTemp->Year, omYear);
			strcpy(prmAccTemp->Peno, prmStf->Peno);
		}
	}
CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

void DutyAccountDlg::MakeAccountMask(long lpAccount /*=0*/)
{
CCS_TRY;
	ipCountMakeAccountMask++;
	for (int i=0; i<omAccountList.GetSize() ; i++)
	{
		if(omAccountList[i].lTYPE == lpAccount)
		{
			rmAccount.lTYPE		= omAccountList[i].lTYPE;
			rmAccount.oNAME		= omAccountList[i].oNAME;
			rmAccount.oSNAM		= omAccountList[i].oSNAM;
			rmAccount.oKTYP		= omAccountList[i].oKTYP;
			rmAccount.oFTYP		= omAccountList[i].oFTYP;
			rmAccount.oMaskType	= omAccountList[i].oMaskType;
			rmAccount.iForm		= omAccountList[i].iForm;
			rmAccount.bEDIT		= omAccountList[i].bEDIT;
			rmAccount.oDPER		= omAccountList[i].oDPER;
			rmAccount.oBCDTable	= omAccountList[i].oBCDTable;
			rmAccount.oBCDField	= omAccountList[i].oBCDField;
			break;
		}
	}
	omMaskPeriod = rmAccount.oDPER;
	m_Auby.SetInitText(prmAccTemp->Auby);
	m_AatoD.SetInitText(prmAccTemp->Aato.Format("%d.%m.%Y"));
	m_AatoT.SetInitText(prmAccTemp->Aato.Format("%H:%M"));
	m_AudaD.SetInitText(prmAccTemp->Auda.Format("%d.%m.%Y"));
	m_AudaT.SetInitText(prmAccTemp->Auda.Format("%H:%M"));

	// Feststellen und anzeigen ob Daten von Hand ge�ndert wurden
	if((prmAccTemp->Auda != prmAccTemp->Cdat && prmAccTemp->Lstu == TIMENULL) ||
		(prmAccTemp->Auda != prmAccTemp->Lstu && prmAccTemp->Lstu != TIMENULL))
	{
		SetDlgItemText(IDC_S_VONHAND, LoadStg(IDS_S_DatenVonHandGeaendert));
	}
	else
	{
		SetDlgItemText(IDC_S_VONHAND, CString(""));
	}

	CString olEroeffnungstext = "...";
	CString olKorrekturtext  = "...";
	CString olAbschlusstext  = "...";
	int ilCmdShowOP = SW_SHOW;
	int ilCmdShowCO = SW_SHOW;
	int ilCmdShowCL = SW_SHOW;
	BOOL blReadOnlyOP = TRUE;
	BOOL blReadOnlyCO = TRUE;
	BOOL blReadOnlyCL = TRUE;
	int ilShowFlag = SW_SHOW;

	if (rmAccount.oMaskType == "1" || rmAccount.oMaskType == "6")
	{
		// Aussehen f�r fast alle statische Konten 
		// mit nur einem festen Monatswert
		// 6 wie 1 aber mit "wurde vom Planer ver�ndert" Flag
		ShowStart(false);
		ShowSaldo(false);
		ShowFreeText(false);
		olEroeffnungstext = "...";
		olAbschlusstext  = "...";
		ilCmdShowOP = SW_HIDE;
		ilCmdShowCO = SW_SHOW;
		ilCmdShowCL = SW_HIDE;

		blReadOnlyOP = TRUE;
		blReadOnlyCL = TRUE;

		if(rmAccount.oDPER == "Y")
			olKorrekturtext  = LoadStg(IDS_S_Jahreswert);
		else if(rmAccount.oDPER == "Q")
			olKorrekturtext  = LoadStg(IDS_S_Quartalswert);
		else if(rmAccount.oDPER == "M")
			olKorrekturtext  = LoadStg(IDS_S_Monatswert);
		else
			olKorrekturtext  = "...";

		if(rmAccount.bEDIT)
			blReadOnlyCO = FALSE;
		else
			blReadOnlyCO = TRUE;

		if (rmAccount.oMaskType == "1")
			ilShowFlag = SW_HIDE;

	}
	else if (rmAccount.oMaskType == "2" || rmAccount.oMaskType == "7")
	{
		// Aussehen f�r fast alle dynamischen Konten 
		// mit Er�ffnung, Korrektur (editierbar) und Abschlu�
		// 7 wie 2 aber mit "wurde vom Planer ver�ndert" Flag
		ShowStart(true);
		ShowSaldo(true);
		ShowFreeText(false);

		olEroeffnungstext = LoadStg(IDS_S_Eroeffnung);
		olKorrekturtext  =  LoadStg(IDS_S_Korrektur);
		olAbschlusstext  =  LoadStg(IDS_S_Abschluss);

		ilCmdShowOP = SW_SHOW;
		ilCmdShowCO = SW_SHOW;
		ilCmdShowCL = SW_SHOW;

		blReadOnlyOP = TRUE;
		//blReadOnlyCO = FALSE;
		if(rmAccount.bEDIT)
			blReadOnlyCO = FALSE;
		else
			blReadOnlyCO = TRUE;


		blReadOnlyCL = TRUE;
	
		if (rmAccount.oMaskType == "2")
			ilShowFlag = SW_HIDE;

	}
	else if (rmAccount.oMaskType == "3" || rmAccount.oMaskType == "8")
	{
		// Aussehen f�r fast alle Singel-Wert Konten 
		// 8 wie 3 aber mit "wurde vom Planer ver�ndert" Flag
		ShowStart(false);
		ShowSaldo(false);
		ShowFreeText(true);

		olEroeffnungstext = "...";
		olKorrekturtext   = "...";
		olAbschlusstext   = "...";

		ilCmdShowOP = SW_HIDE;
		ilCmdShowCO = SW_HIDE;
		ilCmdShowCL = SW_HIDE;

		blReadOnlyOP = TRUE;
		blReadOnlyCO = TRUE;
		blReadOnlyCL = TRUE;
	
		if (rmAccount.oMaskType == "3")
			ilShowFlag = SW_HIDE;

	}
	else //if (rmAccount.oMaskType == "0")
	{
		// Default (alles lehr)
		ShowStart(true);
		ShowSaldo(true);

		olEroeffnungstext = LoadStg(IDS_S_Eroeffnung);
		olKorrekturtext  =  LoadStg(IDS_S_Korrektur);
		olAbschlusstext  =  LoadStg(IDS_S_Abschluss);

		ilCmdShowOP = SW_SHOW;
		ilCmdShowCO = SW_SHOW;
		ilCmdShowCL = SW_SHOW;

		blReadOnlyOP = TRUE;
		blReadOnlyCO = TRUE;
		blReadOnlyCL = TRUE;

		if(ipCountMakeAccountMask == 1)
			ilShowFlag = SW_SHOW;
		else
			ilShowFlag = SW_HIDE;

	}

	//----------------------------------------
	// Check-Feld abh�ngig von ADE.FTYP setzen
	m_Co01Text.SetLimitText(40); // immer
	if(rmAccount.oFTYP == "D")
	{
		m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	}
	else if(rmAccount.oFTYP == "H")
	{
		m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	}
	else if(rmAccount.oFTYP == "M")
	{
		m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	}
	else if(rmAccount.oFTYP == "T")
	{
		m_CheckEdit.SetTypeToString("X(8)",8,0);
	}
	else if(rmAccount.oFTYP == "N")
	{
		m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	}
	else
	{
		m_CheckEdit.SetTypeToDouble(3,2, -999.99, 999.99);
	}


	SetMaskColumn_Op(rmAccount, olEroeffnungstext, ilCmdShowOP, blReadOnlyOP, ilShowFlag);
	SetMaskColumn_Co(rmAccount, olKorrekturtext,   ilCmdShowCO, blReadOnlyCO, ilShowFlag);
	SetMaskColumn_Cl(rmAccount, olAbschlusstext,   ilCmdShowCL, blReadOnlyCL, ilShowFlag);

	SetDummyFields(lpAccount);
CCS_CATCH_ALL;
}

//*********************************************************************************
// Die 1.Spalte (OPEN) laut Einstellungen anzeigen
//*********************************************************************************

void DutyAccountDlg::SetMaskColumn_Op(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow /*=SW_SHOW*/, BOOL bpReadOnly /*=TRUE*/, int ipShowFlag /*= SW_SHOW*/)
{
CCS_TRY;
	CString olMonthText;
	SetDlgItemText(IDC_S_OP1, opHeaderText);
	SetDlgItemText(IDC_S_OP2, opHeaderText);

	if (rpAccount.oDPER == "S")
	{
		SetDlgItemText(IDC_S_MONTH01, "...");
		SetDlgItemText(IDC_S_MONTH02, "...");
		SetDlgItemText(IDC_S_MONTH03, "...");
		SetDlgItemText(IDC_S_MONTH04, "...");
		SetDlgItemText(IDC_S_MONTH05, "...");
		SetDlgItemText(IDC_S_MONTH06, "...");
		SetDlgItemText(IDC_S_MONTH07, "...");
		SetDlgItemText(IDC_S_MONTH08, "...");
		SetDlgItemText(IDC_S_MONTH09, "...");
		SetDlgItemText(IDC_S_MONTH10, "...");
		SetDlgItemText(IDC_S_MONTH11, "...");
		SetDlgItemText(IDC_S_MONTH12, "...");

		m_Op01.ShowWindow(SW_HIDE); m_Op01.SetReadOnly(bpReadOnly); m_Op01.SetWindowText(prmAccTemp->Op01);
		m_Op02.ShowWindow(SW_HIDE); m_Op02.SetReadOnly(bpReadOnly); m_Op02.SetWindowText(prmAccTemp->Op02);
		m_Op03.ShowWindow(SW_HIDE); m_Op03.SetReadOnly(bpReadOnly); m_Op03.SetWindowText(prmAccTemp->Op03);
		m_Op04.ShowWindow(SW_HIDE); m_Op04.SetReadOnly(bpReadOnly); m_Op04.SetWindowText(prmAccTemp->Op04);
		m_Op05.ShowWindow(SW_HIDE); m_Op05.SetReadOnly(bpReadOnly); m_Op05.SetWindowText(prmAccTemp->Op05);
		m_Op06.ShowWindow(SW_HIDE); m_Op06.SetReadOnly(bpReadOnly); m_Op06.SetWindowText(prmAccTemp->Op06);
		m_Op07.ShowWindow(SW_HIDE); m_Op07.SetReadOnly(bpReadOnly); m_Op07.SetWindowText(prmAccTemp->Op07);
		m_Op08.ShowWindow(SW_HIDE); m_Op08.SetReadOnly(bpReadOnly); m_Op08.SetWindowText(prmAccTemp->Op08);
		m_Op09.ShowWindow(SW_HIDE); m_Op09.SetReadOnly(bpReadOnly); m_Op09.SetWindowText(prmAccTemp->Op09);
		m_Op10.ShowWindow(SW_HIDE); m_Op10.SetReadOnly(bpReadOnly); m_Op10.SetWindowText(prmAccTemp->Op10);
		m_Op11.ShowWindow(SW_HIDE); m_Op11.SetReadOnly(bpReadOnly); m_Op11.SetWindowText(prmAccTemp->Op11);
		m_Op12.ShowWindow(SW_HIDE); m_Op12.SetReadOnly(bpReadOnly); m_Op12.SetWindowText(prmAccTemp->Op12);
	}
	else if (rpAccount.oDPER == "Y")
	{
		SetDlgItemText(IDC_S_MONTH01, "...");
		SetDlgItemText(IDC_S_MONTH02, "...");
		SetDlgItemText(IDC_S_MONTH03, "...");
		SetDlgItemText(IDC_S_MONTH04, "...");
		SetDlgItemText(IDC_S_MONTH05, "...");
		SetDlgItemText(IDC_S_MONTH06, "...");
		SetDlgItemText(IDC_S_MONTH07, "...");
		SetDlgItemText(IDC_S_MONTH08, "...");
		SetDlgItemText(IDC_S_MONTH09, "...");
		SetDlgItemText(IDC_S_MONTH10, "...");
		SetDlgItemText(IDC_S_MONTH11, "...");
		olMonthText = LoadStg(IDS_STRING61) + CString(" - ") + LoadStg(IDS_STRING72);
		SetDlgItemText(IDC_S_MONTH12, olMonthText);

		m_Op01.ShowWindow(SW_HIDE); m_Op01.SetReadOnly(bpReadOnly); m_Op01.SetWindowText(prmAccTemp->Op01);
		m_Op02.ShowWindow(SW_HIDE); m_Op02.SetReadOnly(bpReadOnly); m_Op02.SetWindowText(prmAccTemp->Op02);
		m_Op03.ShowWindow(SW_HIDE); m_Op03.SetReadOnly(bpReadOnly); m_Op03.SetWindowText(prmAccTemp->Op03);
		m_Op04.ShowWindow(SW_HIDE); m_Op04.SetReadOnly(bpReadOnly); m_Op04.SetWindowText(prmAccTemp->Op04);
		m_Op05.ShowWindow(SW_HIDE); m_Op05.SetReadOnly(bpReadOnly); m_Op05.SetWindowText(prmAccTemp->Op05);
		m_Op06.ShowWindow(SW_HIDE); m_Op06.SetReadOnly(bpReadOnly); m_Op06.SetWindowText(prmAccTemp->Op06);
		m_Op07.ShowWindow(SW_HIDE); m_Op07.SetReadOnly(bpReadOnly); m_Op07.SetWindowText(prmAccTemp->Op07);
		m_Op08.ShowWindow(SW_HIDE); m_Op08.SetReadOnly(bpReadOnly); m_Op08.SetWindowText(prmAccTemp->Op08);
		m_Op09.ShowWindow(SW_HIDE); m_Op09.SetReadOnly(bpReadOnly); m_Op09.SetWindowText(prmAccTemp->Op09);
		m_Op10.ShowWindow(SW_HIDE); m_Op10.SetReadOnly(bpReadOnly); m_Op10.SetWindowText(prmAccTemp->Op10);
		m_Op11.ShowWindow(SW_HIDE); m_Op11.SetReadOnly(bpReadOnly); m_Op11.SetWindowText(prmAccTemp->Op11);
		m_Op12.ShowWindow(ipCmdShow); m_Op12.SetReadOnly(bpReadOnly); m_Op12.SetWindowText(prmAccTemp->Op12);
	}
	else if (rpAccount.oDPER == "Q")
	{
		olMonthText = LoadStg(IDS_STRING61) + CString(" - ") + LoadStg(IDS_STRING63);
		SetDlgItemText(IDC_S_MONTH01, olMonthText);
		SetDlgItemText(IDC_S_MONTH02, "...");
		SetDlgItemText(IDC_S_MONTH03, "...");
		olMonthText = LoadStg(IDS_STRING64) + CString(" - ") + LoadStg(IDS_STRING66);
		SetDlgItemText(IDC_S_MONTH04, olMonthText);
		SetDlgItemText(IDC_S_MONTH05, "...");
		SetDlgItemText(IDC_S_MONTH06, "...");
		olMonthText = LoadStg(IDS_STRING67) + CString(" - ") + LoadStg(IDS_STRING69);
		SetDlgItemText(IDC_S_MONTH07, olMonthText);
		SetDlgItemText(IDC_S_MONTH08, "...");
		SetDlgItemText(IDC_S_MONTH09, "...");
		olMonthText = LoadStg(IDS_STRING70) + CString(" - ") + LoadStg(IDS_STRING72);
		SetDlgItemText(IDC_S_MONTH10, olMonthText);
		SetDlgItemText(IDC_S_MONTH11, "...");
		SetDlgItemText(IDC_S_MONTH12, "...");

		m_Op01.ShowWindow(ipCmdShow); m_Op01.SetReadOnly(bpReadOnly); m_Op01.SetWindowText(prmAccTemp->Op01);
		m_Op02.ShowWindow(SW_HIDE);   m_Op02.SetReadOnly(bpReadOnly); m_Op02.SetWindowText(prmAccTemp->Op02);
		m_Op03.ShowWindow(SW_HIDE);   m_Op03.SetReadOnly(bpReadOnly); m_Op03.SetWindowText(prmAccTemp->Op03);
		m_Op04.ShowWindow(ipCmdShow); m_Op04.SetReadOnly(bpReadOnly); m_Op04.SetWindowText(prmAccTemp->Op04);
		m_Op05.ShowWindow(SW_HIDE);   m_Op05.SetReadOnly(bpReadOnly); m_Op05.SetWindowText(prmAccTemp->Op05);
		m_Op06.ShowWindow(SW_HIDE);   m_Op06.SetReadOnly(bpReadOnly); m_Op06.SetWindowText(prmAccTemp->Op06);
		m_Op07.ShowWindow(ipCmdShow); m_Op07.SetReadOnly(bpReadOnly); m_Op07.SetWindowText(prmAccTemp->Op07);
		m_Op08.ShowWindow(SW_HIDE);   m_Op08.SetReadOnly(bpReadOnly); m_Op08.SetWindowText(prmAccTemp->Op08);
		m_Op09.ShowWindow(SW_HIDE);   m_Op09.SetReadOnly(bpReadOnly); m_Op09.SetWindowText(prmAccTemp->Op09);
		m_Op10.ShowWindow(ipCmdShow); m_Op10.SetReadOnly(bpReadOnly); m_Op10.SetWindowText(prmAccTemp->Op10);
		m_Op11.ShowWindow(SW_HIDE);   m_Op11.SetReadOnly(bpReadOnly); m_Op11.SetWindowText(prmAccTemp->Op11);
		m_Op12.ShowWindow(SW_HIDE);   m_Op12.SetReadOnly(bpReadOnly); m_Op12.SetWindowText(prmAccTemp->Op12);
	}
	else // rpAccount.oDPER == "M"
	{
		SetDlgItemText(IDC_S_MONTH01, LoadStg(IDS_STRING61));
		SetDlgItemText(IDC_S_MONTH02, LoadStg(IDS_STRING62));
		SetDlgItemText(IDC_S_MONTH03, LoadStg(IDS_STRING63));
		SetDlgItemText(IDC_S_MONTH04, LoadStg(IDS_STRING64));
		SetDlgItemText(IDC_S_MONTH05, LoadStg(IDS_STRING65));
		SetDlgItemText(IDC_S_MONTH06, LoadStg(IDS_STRING66));
		SetDlgItemText(IDC_S_MONTH07, LoadStg(IDS_STRING67));
		SetDlgItemText(IDC_S_MONTH08, LoadStg(IDS_STRING68));
		SetDlgItemText(IDC_S_MONTH09, LoadStg(IDS_STRING69));
		SetDlgItemText(IDC_S_MONTH10, LoadStg(IDS_STRING70));
		SetDlgItemText(IDC_S_MONTH11, LoadStg(IDS_STRING71));
		SetDlgItemText(IDC_S_MONTH12, LoadStg(IDS_STRING72));

		m_Op01.ShowWindow(ipCmdShow); m_Op01.SetReadOnly(bpReadOnly); m_Op01.SetWindowText(prmAccTemp->Op01);
		m_Op02.ShowWindow(ipCmdShow); m_Op02.SetReadOnly(bpReadOnly); m_Op02.SetWindowText(prmAccTemp->Op02);
		m_Op03.ShowWindow(ipCmdShow); m_Op03.SetReadOnly(bpReadOnly); m_Op03.SetWindowText(prmAccTemp->Op03);
		m_Op04.ShowWindow(ipCmdShow); m_Op04.SetReadOnly(bpReadOnly); m_Op04.SetWindowText(prmAccTemp->Op04);
		m_Op05.ShowWindow(ipCmdShow); m_Op05.SetReadOnly(bpReadOnly); m_Op05.SetWindowText(prmAccTemp->Op05);
		m_Op06.ShowWindow(ipCmdShow); m_Op06.SetReadOnly(bpReadOnly); m_Op06.SetWindowText(prmAccTemp->Op06);
		m_Op07.ShowWindow(ipCmdShow); m_Op07.SetReadOnly(bpReadOnly); m_Op07.SetWindowText(prmAccTemp->Op07);
		m_Op08.ShowWindow(ipCmdShow); m_Op08.SetReadOnly(bpReadOnly); m_Op08.SetWindowText(prmAccTemp->Op08);
		m_Op09.ShowWindow(ipCmdShow); m_Op09.SetReadOnly(bpReadOnly); m_Op09.SetWindowText(prmAccTemp->Op09);
		m_Op10.ShowWindow(ipCmdShow); m_Op10.SetReadOnly(bpReadOnly); m_Op10.SetWindowText(prmAccTemp->Op10);
		m_Op11.ShowWindow(ipCmdShow); m_Op11.SetReadOnly(bpReadOnly); m_Op11.SetWindowText(prmAccTemp->Op11);
		m_Op12.ShowWindow(ipCmdShow); m_Op12.SetReadOnly(bpReadOnly); m_Op12.SetWindowText(prmAccTemp->Op12);
	}
	m_C01.ShowWindow(ipShowFlag);
	m_C02.ShowWindow(ipShowFlag);
	m_C03.ShowWindow(ipShowFlag);
	m_C04.ShowWindow(ipShowFlag);
	m_C05.ShowWindow(ipShowFlag);
	m_C06.ShowWindow(ipShowFlag);
	m_C07.ShowWindow(ipShowFlag);
	m_C08.ShowWindow(ipShowFlag);
	m_C09.ShowWindow(ipShowFlag);
	m_C10.ShowWindow(ipShowFlag);
	m_C11.ShowWindow(ipShowFlag);
	m_C12.ShowWindow(ipShowFlag);

	m_C01.SetCheck(0);
	m_C02.SetCheck(0);
	m_C03.SetCheck(0);
	m_C04.SetCheck(0);
	m_C05.SetCheck(0);
	m_C06.SetCheck(0);
	m_C07.SetCheck(0);
	m_C08.SetCheck(0);
	m_C09.SetCheck(0);
	m_C10.SetCheck(0);
	m_C11.SetCheck(0);
	m_C12.SetCheck(0);

	ogOldFumo = prmAccTemp->Fumo;
	if(ogOldFumo.GetLength() == 12)
	{
		if(ogOldFumo[0] == '1')
			m_C01.SetCheck(1);
		if(ogOldFumo[1] == '1')
			m_C02.SetCheck(1);
		if(ogOldFumo[2] == '1')
			m_C03.SetCheck(1);
		if(ogOldFumo[3] == '1')
			m_C04.SetCheck(1);
		if(ogOldFumo[4] == '1')
			m_C05.SetCheck(1);
		if(ogOldFumo[5] == '1')
			m_C06.SetCheck(1);
		if(ogOldFumo[6] == '1')
			m_C07.SetCheck(1);
		if(ogOldFumo[7] == '1')
			m_C08.SetCheck(1);
		if(ogOldFumo[8] == '1')
			m_C09.SetCheck(1);
		if(ogOldFumo[9] == '1')
			m_C10.SetCheck(1);
		if(ogOldFumo[10] == '1')
			m_C11.SetCheck(1);
		if(ogOldFumo[11] == '1')
			m_C12.SetCheck(1);
	}
	else
	{
		ogOldFumo = "000000000000";
	}

CCS_CATCH_ALL;
}

//*********************************************************************************
// Die 2.Spalte (CORRECTION) laut Einstellungen anzeigen
//*********************************************************************************

void DutyAccountDlg::SetMaskColumn_Co(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow /*=SW_SHOW*/, BOOL bpReadOnly /*=TRUE*/, int ipShowFlag /*= 1*/)
{
CCS_TRY;
	SetDlgItemText(IDC_S_CO1, opHeaderText);
	SetDlgItemText(IDC_S_CO2, opHeaderText);

	if (rpAccount.oDPER == "S")
	{
		m_Co01.ShowWindow(SW_HIDE); m_Co01.SetReadOnly(bpReadOnly); m_Co01.SetWindowText(prmAccTemp->Co01);
		m_Co02.ShowWindow(SW_HIDE); m_Co02.SetReadOnly(bpReadOnly); m_Co02.SetWindowText(prmAccTemp->Co02);
		m_Co03.ShowWindow(SW_HIDE); m_Co03.SetReadOnly(bpReadOnly); m_Co03.SetWindowText(prmAccTemp->Co03);
		m_Co04.ShowWindow(SW_HIDE); m_Co04.SetReadOnly(bpReadOnly); m_Co04.SetWindowText(prmAccTemp->Co04); 
		m_Co05.ShowWindow(SW_HIDE); m_Co05.SetReadOnly(bpReadOnly); m_Co05.SetWindowText(prmAccTemp->Co05);
		m_Co06.ShowWindow(SW_HIDE); m_Co06.SetReadOnly(bpReadOnly); m_Co06.SetWindowText(prmAccTemp->Co06);
		m_Co07.ShowWindow(SW_HIDE); m_Co07.SetReadOnly(bpReadOnly); m_Co07.SetWindowText(prmAccTemp->Co07);
		m_Co08.ShowWindow(SW_HIDE); m_Co08.SetReadOnly(bpReadOnly); m_Co08.SetWindowText(prmAccTemp->Co08);
		m_Co09.ShowWindow(SW_HIDE); m_Co09.SetReadOnly(bpReadOnly); m_Co09.SetWindowText(prmAccTemp->Co09);
		m_Co10.ShowWindow(SW_HIDE); m_Co10.SetReadOnly(bpReadOnly); m_Co10.SetWindowText(prmAccTemp->Co10); 
		m_Co11.ShowWindow(SW_HIDE); m_Co11.SetReadOnly(bpReadOnly); m_Co11.SetWindowText(prmAccTemp->Co11);
		m_Co12.ShowWindow(SW_HIDE); m_Co12.SetReadOnly(bpReadOnly); m_Co12.SetWindowText(prmAccTemp->Co12);
	}
	else if (rpAccount.oDPER == "Y")
	{
		m_Co01.ShowWindow(SW_HIDE); m_Co01.SetReadOnly(bpReadOnly); m_Co01.SetWindowText(prmAccTemp->Co01);
		m_Co02.ShowWindow(SW_HIDE); m_Co02.SetReadOnly(bpReadOnly); m_Co02.SetWindowText(prmAccTemp->Co02);
		m_Co03.ShowWindow(SW_HIDE); m_Co03.SetReadOnly(bpReadOnly); m_Co03.SetWindowText(prmAccTemp->Co03);
		m_Co04.ShowWindow(SW_HIDE); m_Co04.SetReadOnly(bpReadOnly); m_Co04.SetWindowText(prmAccTemp->Co04); 
		m_Co05.ShowWindow(SW_HIDE); m_Co05.SetReadOnly(bpReadOnly); m_Co05.SetWindowText(prmAccTemp->Co05);
		m_Co06.ShowWindow(SW_HIDE); m_Co06.SetReadOnly(bpReadOnly); m_Co06.SetWindowText(prmAccTemp->Co06);
		m_Co07.ShowWindow(SW_HIDE); m_Co07.SetReadOnly(bpReadOnly); m_Co07.SetWindowText(prmAccTemp->Co07);
		m_Co08.ShowWindow(SW_HIDE); m_Co08.SetReadOnly(bpReadOnly); m_Co08.SetWindowText(prmAccTemp->Co08);
		m_Co09.ShowWindow(SW_HIDE); m_Co09.SetReadOnly(bpReadOnly); m_Co09.SetWindowText(prmAccTemp->Co09);
		m_Co10.ShowWindow(SW_HIDE); m_Co10.SetReadOnly(bpReadOnly); m_Co10.SetWindowText(prmAccTemp->Co10); 
		m_Co11.ShowWindow(SW_HIDE); m_Co11.SetReadOnly(bpReadOnly); m_Co11.SetWindowText(prmAccTemp->Co11);
		m_Co12.ShowWindow(ipCmdShow); m_Co12.SetReadOnly(bpReadOnly); m_Co12.SetWindowText(prmAccTemp->Co12);
	}
	else if (rpAccount.oDPER == "Q")
	{
		m_Co01.ShowWindow(ipCmdShow); m_Co01.SetReadOnly(bpReadOnly); m_Co01.SetWindowText(prmAccTemp->Co01);
		m_Co02.ShowWindow(SW_HIDE);   m_Co02.SetReadOnly(bpReadOnly); m_Co02.SetWindowText(prmAccTemp->Co02);
		m_Co03.ShowWindow(SW_HIDE);   m_Co03.SetReadOnly(bpReadOnly); m_Co03.SetWindowText(prmAccTemp->Co03);
		m_Co04.ShowWindow(ipCmdShow); m_Co04.SetReadOnly(bpReadOnly); m_Co04.SetWindowText(prmAccTemp->Co04); 
		m_Co05.ShowWindow(SW_HIDE);   m_Co05.SetReadOnly(bpReadOnly); m_Co05.SetWindowText(prmAccTemp->Co05);
		m_Co06.ShowWindow(SW_HIDE);   m_Co06.SetReadOnly(bpReadOnly); m_Co06.SetWindowText(prmAccTemp->Co06);
		m_Co07.ShowWindow(ipCmdShow); m_Co07.SetReadOnly(bpReadOnly); m_Co07.SetWindowText(prmAccTemp->Co07);
		m_Co08.ShowWindow(SW_HIDE);   m_Co08.SetReadOnly(bpReadOnly); m_Co08.SetWindowText(prmAccTemp->Co08);
		m_Co09.ShowWindow(SW_HIDE);   m_Co09.SetReadOnly(bpReadOnly); m_Co09.SetWindowText(prmAccTemp->Co09);
		m_Co10.ShowWindow(ipCmdShow); m_Co10.SetReadOnly(bpReadOnly); m_Co10.SetWindowText(prmAccTemp->Co10); 
		m_Co11.ShowWindow(SW_HIDE);   m_Co11.SetReadOnly(bpReadOnly); m_Co11.SetWindowText(prmAccTemp->Co11);
		m_Co12.ShowWindow(SW_HIDE);   m_Co12.SetReadOnly(bpReadOnly); m_Co12.SetWindowText(prmAccTemp->Co12);
	}
	else // rpAccount.oDPER == "M"  rpAccount.oDPER == "S" mit Maske Typ 3
	{
		m_Co01.ShowWindow(ipCmdShow); m_Co01.SetReadOnly(bpReadOnly); m_Co01.SetWindowText(prmAccTemp->Co01);
		m_Co02.ShowWindow(ipCmdShow); m_Co02.SetReadOnly(bpReadOnly); m_Co02.SetWindowText(prmAccTemp->Co02);
		m_Co03.ShowWindow(ipCmdShow); m_Co03.SetReadOnly(bpReadOnly); m_Co03.SetWindowText(prmAccTemp->Co03);
		m_Co04.ShowWindow(ipCmdShow); m_Co04.SetReadOnly(bpReadOnly); m_Co04.SetWindowText(prmAccTemp->Co04); 
		m_Co05.ShowWindow(ipCmdShow); m_Co05.SetReadOnly(bpReadOnly); m_Co05.SetWindowText(prmAccTemp->Co05);
		m_Co06.ShowWindow(ipCmdShow); m_Co06.SetReadOnly(bpReadOnly); m_Co06.SetWindowText(prmAccTemp->Co06);
		m_Co07.ShowWindow(ipCmdShow); m_Co07.SetReadOnly(bpReadOnly); m_Co07.SetWindowText(prmAccTemp->Co07);
		m_Co08.ShowWindow(ipCmdShow); m_Co08.SetReadOnly(bpReadOnly); m_Co08.SetWindowText(prmAccTemp->Co08);
		m_Co09.ShowWindow(ipCmdShow); m_Co09.SetReadOnly(bpReadOnly); m_Co09.SetWindowText(prmAccTemp->Co09);
		m_Co10.ShowWindow(ipCmdShow); m_Co10.SetReadOnly(bpReadOnly); m_Co10.SetWindowText(prmAccTemp->Co10); 
		m_Co11.ShowWindow(ipCmdShow); m_Co11.SetReadOnly(bpReadOnly); m_Co11.SetWindowText(prmAccTemp->Co11);
		m_Co12.ShowWindow(ipCmdShow); m_Co12.SetReadOnly(bpReadOnly); m_Co12.SetWindowText(prmAccTemp->Co12);
	}
CCS_CATCH_ALL;
}

//*********************************************************************************
// Die 3.Spalte (CLOSE) laut Einstellungen anzeigen
//*********************************************************************************

void DutyAccountDlg::SetMaskColumn_Cl(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow /*=SW_SHOW*/, BOOL bpReadOnly /*=TRUE*/, int ipShowFlag /*= 1*/)
{
CCS_TRY;
	SetDlgItemText(IDC_S_CL1, opHeaderText);
	SetDlgItemText(IDC_S_CL2, opHeaderText);

	if (rpAccount.oDPER == "S")
	{
		m_Cl01.ShowWindow(SW_HIDE); m_Cl01.SetReadOnly(bpReadOnly); m_Cl01.SetWindowText(prmAccTemp->Cl01);
		m_Cl02.ShowWindow(SW_HIDE); m_Cl02.SetReadOnly(bpReadOnly); m_Cl02.SetWindowText(prmAccTemp->Cl02);
		m_Cl03.ShowWindow(SW_HIDE); m_Cl03.SetReadOnly(bpReadOnly); m_Cl03.SetWindowText(prmAccTemp->Cl03);
		m_Cl04.ShowWindow(SW_HIDE); m_Cl04.SetReadOnly(bpReadOnly); m_Cl04.SetWindowText(prmAccTemp->Cl04);
		m_Cl05.ShowWindow(SW_HIDE); m_Cl05.SetReadOnly(bpReadOnly); m_Cl05.SetWindowText(prmAccTemp->Cl05);
		m_Cl06.ShowWindow(SW_HIDE); m_Cl06.SetReadOnly(bpReadOnly); m_Cl06.SetWindowText(prmAccTemp->Cl06);
		m_Cl07.ShowWindow(SW_HIDE); m_Cl07.SetReadOnly(bpReadOnly); m_Cl07.SetWindowText(prmAccTemp->Cl07);
		m_Cl08.ShowWindow(SW_HIDE); m_Cl08.SetReadOnly(bpReadOnly); m_Cl08.SetWindowText(prmAccTemp->Cl08);
		m_Cl09.ShowWindow(SW_HIDE); m_Cl09.SetReadOnly(bpReadOnly); m_Cl09.SetWindowText(prmAccTemp->Cl09);
		m_Cl10.ShowWindow(SW_HIDE); m_Cl10.SetReadOnly(bpReadOnly); m_Cl10.SetWindowText(prmAccTemp->Cl10);
		m_Cl11.ShowWindow(SW_HIDE); m_Cl11.SetReadOnly(bpReadOnly); m_Cl11.SetWindowText(prmAccTemp->Cl11);
		m_Cl12.ShowWindow(SW_HIDE); m_Cl12.SetReadOnly(bpReadOnly); m_Cl12.SetWindowText(prmAccTemp->Cl12);
	}
	else if (rpAccount.oDPER == "Y")
	{
		m_Cl01.ShowWindow(SW_HIDE); m_Cl01.SetReadOnly(bpReadOnly); m_Cl01.SetWindowText(prmAccTemp->Cl01);
		m_Cl02.ShowWindow(SW_HIDE); m_Cl02.SetReadOnly(bpReadOnly); m_Cl02.SetWindowText(prmAccTemp->Cl02);
		m_Cl03.ShowWindow(SW_HIDE); m_Cl03.SetReadOnly(bpReadOnly); m_Cl03.SetWindowText(prmAccTemp->Cl03);
		m_Cl04.ShowWindow(SW_HIDE); m_Cl04.SetReadOnly(bpReadOnly); m_Cl04.SetWindowText(prmAccTemp->Cl04);
		m_Cl05.ShowWindow(SW_HIDE); m_Cl05.SetReadOnly(bpReadOnly); m_Cl05.SetWindowText(prmAccTemp->Cl05);
		m_Cl06.ShowWindow(SW_HIDE); m_Cl06.SetReadOnly(bpReadOnly); m_Cl06.SetWindowText(prmAccTemp->Cl06);
		m_Cl07.ShowWindow(SW_HIDE); m_Cl07.SetReadOnly(bpReadOnly); m_Cl07.SetWindowText(prmAccTemp->Cl07);
		m_Cl08.ShowWindow(SW_HIDE); m_Cl08.SetReadOnly(bpReadOnly); m_Cl08.SetWindowText(prmAccTemp->Cl08);
		m_Cl09.ShowWindow(SW_HIDE); m_Cl09.SetReadOnly(bpReadOnly); m_Cl09.SetWindowText(prmAccTemp->Cl09);
		m_Cl10.ShowWindow(SW_HIDE); m_Cl10.SetReadOnly(bpReadOnly); m_Cl10.SetWindowText(prmAccTemp->Cl10);
		m_Cl11.ShowWindow(SW_HIDE); m_Cl11.SetReadOnly(bpReadOnly); m_Cl11.SetWindowText(prmAccTemp->Cl11);
		m_Cl12.ShowWindow(ipCmdShow); m_Cl12.SetReadOnly(bpReadOnly); m_Cl12.SetWindowText(prmAccTemp->Cl12);
	}
	else if (rpAccount.oDPER == "Q")
	{
		m_Cl01.ShowWindow(ipCmdShow); m_Cl01.SetReadOnly(bpReadOnly); m_Cl01.SetWindowText(prmAccTemp->Cl01);
		m_Cl02.ShowWindow(SW_HIDE);   m_Cl02.SetReadOnly(bpReadOnly); m_Cl02.SetWindowText(prmAccTemp->Cl02);
		m_Cl03.ShowWindow(SW_HIDE);   m_Cl03.SetReadOnly(bpReadOnly); m_Cl03.SetWindowText(prmAccTemp->Cl03);
		m_Cl04.ShowWindow(ipCmdShow); m_Cl04.SetReadOnly(bpReadOnly); m_Cl04.SetWindowText(prmAccTemp->Cl04);
		m_Cl05.ShowWindow(SW_HIDE);   m_Cl05.SetReadOnly(bpReadOnly); m_Cl05.SetWindowText(prmAccTemp->Cl05);
		m_Cl06.ShowWindow(SW_HIDE);   m_Cl06.SetReadOnly(bpReadOnly); m_Cl06.SetWindowText(prmAccTemp->Cl06);
		m_Cl07.ShowWindow(ipCmdShow); m_Cl07.SetReadOnly(bpReadOnly); m_Cl07.SetWindowText(prmAccTemp->Cl07);
		m_Cl08.ShowWindow(SW_HIDE);   m_Cl08.SetReadOnly(bpReadOnly); m_Cl08.SetWindowText(prmAccTemp->Cl08);
		m_Cl09.ShowWindow(SW_HIDE);   m_Cl09.SetReadOnly(bpReadOnly); m_Cl09.SetWindowText(prmAccTemp->Cl09);
		m_Cl10.ShowWindow(ipCmdShow); m_Cl10.SetReadOnly(bpReadOnly); m_Cl10.SetWindowText(prmAccTemp->Cl10);
		m_Cl11.ShowWindow(SW_HIDE);   m_Cl11.SetReadOnly(bpReadOnly); m_Cl11.SetWindowText(prmAccTemp->Cl11);
		m_Cl12.ShowWindow(SW_HIDE);   m_Cl12.SetReadOnly(bpReadOnly); m_Cl12.SetWindowText(prmAccTemp->Cl12);
	}
	else // rpAccount.oDPER == "M"  rpAccount.oDPER == "S" mit Maske Typ 3
	{
		m_Cl01.ShowWindow(ipCmdShow); m_Cl01.SetReadOnly(bpReadOnly); m_Cl01.SetWindowText(prmAccTemp->Cl01);
		m_Cl02.ShowWindow(ipCmdShow); m_Cl02.SetReadOnly(bpReadOnly); m_Cl02.SetWindowText(prmAccTemp->Cl02);
		m_Cl03.ShowWindow(ipCmdShow); m_Cl03.SetReadOnly(bpReadOnly); m_Cl03.SetWindowText(prmAccTemp->Cl03);
		m_Cl04.ShowWindow(ipCmdShow); m_Cl04.SetReadOnly(bpReadOnly); m_Cl04.SetWindowText(prmAccTemp->Cl04);
		m_Cl05.ShowWindow(ipCmdShow); m_Cl05.SetReadOnly(bpReadOnly); m_Cl05.SetWindowText(prmAccTemp->Cl05);
		m_Cl06.ShowWindow(ipCmdShow); m_Cl06.SetReadOnly(bpReadOnly); m_Cl06.SetWindowText(prmAccTemp->Cl06);
		m_Cl07.ShowWindow(ipCmdShow); m_Cl07.SetReadOnly(bpReadOnly); m_Cl07.SetWindowText(prmAccTemp->Cl07);
		m_Cl08.ShowWindow(ipCmdShow); m_Cl08.SetReadOnly(bpReadOnly); m_Cl08.SetWindowText(prmAccTemp->Cl08);
		m_Cl09.ShowWindow(ipCmdShow); m_Cl09.SetReadOnly(bpReadOnly); m_Cl09.SetWindowText(prmAccTemp->Cl09);
		m_Cl10.ShowWindow(ipCmdShow); m_Cl10.SetReadOnly(bpReadOnly); m_Cl10.SetWindowText(prmAccTemp->Cl10);
		m_Cl11.ShowWindow(ipCmdShow); m_Cl11.SetReadOnly(bpReadOnly); m_Cl11.SetWindowText(prmAccTemp->Cl11);
		m_Cl12.ShowWindow(ipCmdShow); m_Cl12.SetReadOnly(bpReadOnly); m_Cl12.SetWindowText(prmAccTemp->Cl12);
	}

CCS_CATCH_ALL;
}

//*********************************************************************************
// Startwert laut Einstellungen anzeigen 
//*********************************************************************************

void DutyAccountDlg::ShowStart(bool bpShow /*=true*/)
{
CCS_TRY;
	if(bpShow)
	{
		SetDlgItemText(IDS_STATICSTART, LoadStg(IDS_STRING1179));
		m_Start.ShowWindow(SW_SHOW);
		m_Start.SetInitText(TrimValue(prmAccTemp->Yecu, rmAccount.iForm));
	}
	else
	{
		SetDlgItemText(IDS_STATICSTART, CString(""));
		m_Start.ShowWindow(SW_HIDE);
		m_Start.SetInitText(CString(""));
	}
CCS_CATCH_ALL;
}

//*********************************************************************************
// Saldowert laut Einstellungen anzeigen 
//*********************************************************************************

void DutyAccountDlg::ShowSaldo(bool bpShow /*=true*/)
{
CCS_TRY;
	if(bpShow)
	{
		SetDlgItemText(IDS_STATICSALDO, LoadStg(IDS_STRING1180));
		m_Saldo.ShowWindow(SW_SHOW);
		m_Saldo.SetInitText(prmAccTemp->Yela);
	}
	else
	{
		SetDlgItemText(IDS_STATICSALDO, CString(""));
		m_Saldo.ShowWindow(SW_HIDE);
		m_Saldo.SetInitText(CString(""));
	}
CCS_CATCH_ALL;
}
//*********************************************************************************
// Freier Eingabetext laut Einstellungen anzeigen 
//*********************************************************************************

void DutyAccountDlg::ShowFreeText(bool bpShow /*=true*/)
{
CCS_TRY;
	if(bpShow)
	{
		SetDlgItemText(IDC_S_CO01_TEXT, LoadStg(IDS_S_FreeText));
		m_Co01Text.ShowWindow(SW_SHOW);
		m_Co01Text.SetReadOnly(FALSE);
		m_Co01Text.SetWindowText(prmAccTemp->Co01);
	}
	else
	{
		SetDlgItemText(IDC_S_CO01_TEXT, CString(""));
		m_Co01Text.ShowWindow(SW_HIDE);
		m_Co01Text.SetWindowText(CString(""));
	}
CCS_CATCH_ALL;
}

//*********************************************************************************
// Pr�ft ob sich die daten ver�ndert haben bei OnOK oder OnSelchangeCAccount.
// Wenn ver�nderungen bei OnSelchangeCAccount vorliegen > fragen ob gespeichert werden soll
//*********************************************************************************

bool DutyAccountDlg::CheckData(bool blOnOK)
{
CCS_TRY;

	CString olErrorText;
	CString olTmp;
	BOOL blIsModify = FALSE; 

	//-------------------------------------------
	// Hat sich etwas ver�ndert 
	if(m_Co01.GetModify()) blIsModify = TRUE;
	if(m_Co02.GetModify()) blIsModify = TRUE;
	if(m_Co03.GetModify()) blIsModify = TRUE;
	if(m_Co04.GetModify()) blIsModify = TRUE;
	if(m_Co05.GetModify()) blIsModify = TRUE;
	if(m_Co06.GetModify()) blIsModify = TRUE;
	if(m_Co07.GetModify()) blIsModify = TRUE;
	if(m_Co08.GetModify()) blIsModify = TRUE;
	if(m_Co09.GetModify()) blIsModify = TRUE;
	if(m_Co10.GetModify()) blIsModify = TRUE;
	if(m_Co11.GetModify()) blIsModify = TRUE;
	if(m_Co12.GetModify()) blIsModify = TRUE;
	if(m_Co01Text.GetModify()) blIsModify = TRUE;

	if(blIsModify == FALSE)
	{
		// keine ver�nderung > ohne Speichern raus 
		return true;
	}

	if(blIsModify && blOnOK == false)
	{
		int ilMBoxReturn = IDYES;
		ilMBoxReturn = MessageBox(LoadStg(IDS_S_SpeicherAenderung),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		if(ilMBoxReturn == IDNO)
		{
			// Ver�nderungen sollen nicht gespeichert werden > raus
			return true;
		}
		else if(ilMBoxReturn == IDCANCEL)
		{
			// Abbruch > zur�ck 
			return false;
		}
	}

	//-------------------------------------------
	// Pr�fe ob in den Korreckturfelden die Werte richtig sind
	CString olTempFumo = ""; // ist zum f�llen des Fumo Feldes bei benutzer ver�nderungen

	if(m_Co01.GetModify())
	{
		m_Co01.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co01.SetFocus();
			m_Co01.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co01.SetFocus();
				m_Co01.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[0] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co02.GetModify())
	{
		m_Co02.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co02.SetFocus();
			m_Co02.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co02.SetFocus();
				m_Co02.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[1] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co03.GetModify())
	{
		m_Co03.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co03.SetFocus();
			m_Co03.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co03.SetFocus();
				m_Co03.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[2] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co04.GetModify())
	{
		m_Co04.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co04.SetFocus();
			m_Co04.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co04.SetFocus();
				m_Co04.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[3] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co05.GetModify())
	{
		m_Co05.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co05.SetFocus();
			m_Co05.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co05.SetFocus();
				m_Co05.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[4] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co06.GetModify())
	{
		m_Co06.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co06.SetFocus();
			m_Co06.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co06.SetFocus();
				m_Co06.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[5] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co07.GetModify())
	{
		m_Co07.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co07.SetFocus();
			m_Co07.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co07.SetFocus();
				m_Co07.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[6] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co08.GetModify())
	{
		m_Co08.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co08.SetFocus();
			m_Co08.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co08.SetFocus();
				m_Co08.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[7] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co09.GetModify())
	{
		m_Co09.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co09.SetFocus();
			m_Co09.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co09.SetFocus();
				m_Co09.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[8] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co10.GetModify())
	{
		m_Co10.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co10.SetFocus();
			m_Co10.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co10.SetFocus();
				m_Co10.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[9] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co11.GetModify())
	{
		m_Co11.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co11.SetFocus();
			m_Co11.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co11.SetFocus();
				m_Co11.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[10] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	if(m_Co12.GetModify())
	{
		m_Co12.GetWindowText(olTmp);
		m_CheckEdit.SetWindowText(olTmp);
		if(m_CheckEdit.GetStatus() == false)
		{
			MessageBox(LoadStg(ST_BADFORMAT),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			m_Co12.SetFocus();
			m_Co12.SetSel(0,-1);
			return false;
		}
		else
		{
			if(!CheckToFtyp_T())
			{
				MessageBox(LoadStg(IDS_STRING151),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				m_Co12.SetFocus();
				m_Co12.SetSel(0,-1);
				return false;
			}
		}
		olTempFumo += "1";
	}
	else 
	{
		if(ogOldFumo[11] == '1')
			olTempFumo += "1";
		else
			olTempFumo += "0";

	}
	//-------------------------------------------
	// Kommas in Punkte wandel und Wert in prmAccTemp zur�ckschreiben
	m_Co01.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co01, olTmp);

	m_Co02.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co02, olTmp);
	
	m_Co03.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co03, olTmp);
	
	m_Co04.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co04, olTmp);
	
	m_Co05.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co05, olTmp);
	
	m_Co06.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co06, olTmp);
	
	m_Co07.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co07, olTmp);
	
	m_Co08.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co08, olTmp);
	
	m_Co09.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co09, olTmp);
	
	m_Co10.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co10, olTmp);
	
	m_Co11.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co11, olTmp);
	
	m_Co12.GetWindowText(olTmp);
	if(olTmp.Find(',') != -1)
		olTmp.SetAt(olTmp.Find(','), '.');
	strcpy(prmAccTemp->Co12, olTmp);

	// Fumo - Werte f�r Ver�nderungen
	strcpy(prmAccTemp->Fumo, olTempFumo);
	
	if(omMaskPeriod == "S")
	{
		m_Co01Text.GetWindowText(olTmp);
		strcpy(prmAccTemp->Co01, olTmp);
	}
	else if(omMaskPeriod == "Y")
	{
		strcpy(prmAccTemp->Co01, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co02, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co03, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co04, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co05, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co06, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co07, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co08, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co09, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co10, prmAccTemp->Co12);
		strcpy(prmAccTemp->Co11, prmAccTemp->Co12);
	}
	else if(omMaskPeriod == "Q")
	{
		strcpy(prmAccTemp->Co02, prmAccTemp->Co01);
		strcpy(prmAccTemp->Co03, prmAccTemp->Co01);
		strcpy(prmAccTemp->Co05, prmAccTemp->Co04);
		strcpy(prmAccTemp->Co06, prmAccTemp->Co04);
		strcpy(prmAccTemp->Co08, prmAccTemp->Co07);
		strcpy(prmAccTemp->Co09, prmAccTemp->Co07);
		strcpy(prmAccTemp->Co11, prmAccTemp->Co10);
		strcpy(prmAccTemp->Co12, prmAccTemp->Co10);
	}

	ACCDATA rlAccTemp = *prmAccTemp;
	ACCDATA rlAccOrig;
	if(prmAccOrig != NULL)
	{
		rlAccOrig = *prmAccOrig;
	}
	else
	{
		rlAccOrig.Stfu = prmAccTemp->Stfu;
		strcpy(rlAccOrig.Type, prmAccTemp->Type);
		strcpy(rlAccOrig.Year, prmAccTemp->Year);
		strcpy(rlAccOrig.Peno, prmAccTemp->Peno);
	}


	bool blChanged = ogAccData.IsDataChanged(&rlAccTemp, &rlAccOrig);
	if(blChanged)
	{
		if(prmAccOrig != NULL)
		{
			*prmAccOrig = *prmAccTemp;

			prmAccOrig->Lstu = CTime::GetCurrentTime();
			strcpy(prmAccOrig->Useu,pcgUser);
			if(ogAccData.Update(prmAccOrig) == false)
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_UPDATEERR), ogAccData.imLastReturnCode, ogAccData.omLastErrorMessage);
				::MessageBox(NULL, olErrorTxt,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			}	
		}
		else
		{
			prmAccOrig = new ACCDATA;
			*prmAccOrig = *prmAccTemp;

			prmAccOrig->Urno = ogBasicData.GetNextUrno();
			prmAccOrig->Cdat = CTime::GetCurrentTime();
			prmAccOrig->Lstu = prmAccOrig->Cdat;
			strcpy(prmAccOrig->Usec,pcgUser);
			strcpy(prmAccOrig->Useu,pcgUser);
			if(ogAccData.Insert(prmAccOrig) == false)
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_INSERTERR), ogAccData.imLastReturnCode, ogAccData.omLastErrorMessage);
				::MessageBox(NULL, olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				delete prmAccOrig;
				prmAccOrig = NULL;
			}
		}
	}
	return true;      
CCS_CATCH_ALL;
return false;
}
//*********************************************************************************
// Freie Text eingabe �berpr�fen, wenn FTYP = T (Text)
// FutureMusic-> Was zu pr�fen ist, in ADE aufnemen (Tabelle und Feld) f�r ogBSD
//*********************************************************************************
bool DutyAccountDlg::CheckToFtyp_T()
{
CCS_TRY;
	bool blOK = true;
	if(rmAccount.oFTYP == "T" && !rmAccount.oBCDTable.IsEmpty() && !rmAccount.oBCDField.IsEmpty())
	{
		CString olTmp;
		m_CheckEdit.GetWindowText(olTmp);
		CString clWhere;
		clWhere.Format("WHERE %s = '%s'",rmAccount.oBCDField, olTmp);
		ogBCD.SetObject(rmAccount.oBCDTable);
		ogBCD.Read(rmAccount.oBCDTable,clWhere);
		int ilDataCount = ogBCD.GetDataCount(rmAccount.oBCDTable); //number of rows
		if(ilDataCount<1)
			blOK = false;
	}
	return blOK;      
CCS_CATCH_ALL;
return false;
}
//*********************************************************************************
// Zeigt die CStatic Felder statt der CEdit Felder als Platzhalter an
//*********************************************************************************
void DutyAccountDlg::SetDummyFields(long lpAccount) 
{
	if(m_Cl01.IsWindowVisible() || lpAccount<=0) m_SCl01.ShowWindow(SW_HIDE); else m_SCl01.ShowWindow(SW_SHOW);
	if(m_Cl02.IsWindowVisible() || lpAccount<=0) m_SCl02.ShowWindow(SW_HIDE); else m_SCl02.ShowWindow(SW_SHOW);
	if(m_Cl03.IsWindowVisible() || lpAccount<=0) m_SCl03.ShowWindow(SW_HIDE); else m_SCl03.ShowWindow(SW_SHOW);
	if(m_Cl04.IsWindowVisible() || lpAccount<=0) m_SCl04.ShowWindow(SW_HIDE); else m_SCl04.ShowWindow(SW_SHOW);
	if(m_Cl05.IsWindowVisible() || lpAccount<=0) m_SCl05.ShowWindow(SW_HIDE); else m_SCl05.ShowWindow(SW_SHOW);
	if(m_Cl06.IsWindowVisible() || lpAccount<=0) m_SCl06.ShowWindow(SW_HIDE); else m_SCl06.ShowWindow(SW_SHOW);
	if(m_Cl07.IsWindowVisible() || lpAccount<=0) m_SCl07.ShowWindow(SW_HIDE); else m_SCl07.ShowWindow(SW_SHOW);
	if(m_Cl08.IsWindowVisible() || lpAccount<=0) m_SCl08.ShowWindow(SW_HIDE); else m_SCl08.ShowWindow(SW_SHOW);
	if(m_Cl09.IsWindowVisible() || lpAccount<=0) m_SCl09.ShowWindow(SW_HIDE); else m_SCl09.ShowWindow(SW_SHOW);
	if(m_Cl10.IsWindowVisible() || lpAccount<=0) m_SCl10.ShowWindow(SW_HIDE); else m_SCl10.ShowWindow(SW_SHOW);
	if(m_Cl11.IsWindowVisible() || lpAccount<=0) m_SCl11.ShowWindow(SW_HIDE); else m_SCl11.ShowWindow(SW_SHOW);
	if(m_Cl12.IsWindowVisible() || lpAccount<=0) m_SCl12.ShowWindow(SW_HIDE); else m_SCl12.ShowWindow(SW_SHOW);
	if(m_Co01.IsWindowVisible() || lpAccount<=0) m_SCo01.ShowWindow(SW_HIDE); else m_SCo01.ShowWindow(SW_SHOW);
	if(m_Co02.IsWindowVisible() || lpAccount<=0) m_SCo02.ShowWindow(SW_HIDE); else m_SCo02.ShowWindow(SW_SHOW);
	if(m_Co03.IsWindowVisible() || lpAccount<=0) m_SCo03.ShowWindow(SW_HIDE); else m_SCo03.ShowWindow(SW_SHOW);
	if(m_Co04.IsWindowVisible() || lpAccount<=0) m_SCo04.ShowWindow(SW_HIDE); else m_SCo04.ShowWindow(SW_SHOW);
	if(m_Co05.IsWindowVisible() || lpAccount<=0) m_SCo05.ShowWindow(SW_HIDE); else m_SCo05.ShowWindow(SW_SHOW);
	if(m_Co06.IsWindowVisible() || lpAccount<=0) m_SCo06.ShowWindow(SW_HIDE); else m_SCo06.ShowWindow(SW_SHOW);
	if(m_Co07.IsWindowVisible() || lpAccount<=0) m_SCo07.ShowWindow(SW_HIDE); else m_SCo07.ShowWindow(SW_SHOW);
	if(m_Co08.IsWindowVisible() || lpAccount<=0) m_SCo08.ShowWindow(SW_HIDE); else m_SCo08.ShowWindow(SW_SHOW);
	if(m_Co09.IsWindowVisible() || lpAccount<=0) m_SCo09.ShowWindow(SW_HIDE); else m_SCo09.ShowWindow(SW_SHOW);
	if(m_Co10.IsWindowVisible() || lpAccount<=0) m_SCo10.ShowWindow(SW_HIDE); else m_SCo10.ShowWindow(SW_SHOW);
	if(m_Co11.IsWindowVisible() || lpAccount<=0) m_SCo11.ShowWindow(SW_HIDE); else m_SCo11.ShowWindow(SW_SHOW);
	if(m_Co12.IsWindowVisible() || lpAccount<=0) m_SCo12.ShowWindow(SW_HIDE); else m_SCo12.ShowWindow(SW_SHOW);
	if(m_Op01.IsWindowVisible() || lpAccount<=0) m_SOp01.ShowWindow(SW_HIDE); else m_SOp01.ShowWindow(SW_SHOW);
	if(m_Op02.IsWindowVisible() || lpAccount<=0) m_SOp02.ShowWindow(SW_HIDE); else m_SOp02.ShowWindow(SW_SHOW);
	if(m_Op03.IsWindowVisible() || lpAccount<=0) m_SOp03.ShowWindow(SW_HIDE); else m_SOp03.ShowWindow(SW_SHOW);
	if(m_Op04.IsWindowVisible() || lpAccount<=0) m_SOp04.ShowWindow(SW_HIDE); else m_SOp04.ShowWindow(SW_SHOW);
	if(m_Op05.IsWindowVisible() || lpAccount<=0) m_SOp05.ShowWindow(SW_HIDE); else m_SOp05.ShowWindow(SW_SHOW);
	if(m_Op06.IsWindowVisible() || lpAccount<=0) m_SOp06.ShowWindow(SW_HIDE); else m_SOp06.ShowWindow(SW_SHOW);
	if(m_Op07.IsWindowVisible() || lpAccount<=0) m_SOp07.ShowWindow(SW_HIDE); else m_SOp07.ShowWindow(SW_SHOW);
	if(m_Op08.IsWindowVisible() || lpAccount<=0) m_SOp08.ShowWindow(SW_HIDE); else m_SOp08.ShowWindow(SW_SHOW);
	if(m_Op09.IsWindowVisible() || lpAccount<=0) m_SOp09.ShowWindow(SW_HIDE); else m_SOp09.ShowWindow(SW_SHOW);
	if(m_Op10.IsWindowVisible() || lpAccount<=0) m_SOp10.ShowWindow(SW_HIDE); else m_SOp10.ShowWindow(SW_SHOW);
	if(m_Op11.IsWindowVisible() || lpAccount<=0) m_SOp11.ShowWindow(SW_HIDE); else m_SOp11.ShowWindow(SW_SHOW);
	if(m_Op12.IsWindowVisible() || lpAccount<=0) m_SOp12.ShowWindow(SW_HIDE); else m_SOp12.ShowWindow(SW_SHOW);
}
//*********************************************************************************
// Trimmt CString opValue - der eine Zahl enth�lt - auf die Anzahl der festgelegten 
// Nachkommastellen aus ipForm (ADE.FORM) und gibt ihn als CString zur�ck
//*********************************************************************************
CString DutyAccountDlg::TrimValue(CString opValue, int ipForm /*=0*/) 
{
	CString olNewValue;
	if(rmAccount.oFTYP != "T")
	{
		double ldValue = 0;
		ldValue = (atof(opValue));
		if(opValue.GetLength() > 0)
		{
			if(ipForm == 0)
				olNewValue.Format("%01.0f",ldValue);
			else if (ipForm == 1)
				olNewValue.Format("%01.1f",ldValue);
			else if (ipForm == 2)
				olNewValue.Format("%01.2f",ldValue);
			else
				olNewValue = opValue;
		}
		else
		{
			olNewValue = opValue;
		}
	}
	else
	{
		olNewValue = opValue;
	}
	
	return olNewValue;
}
//*********************************************************************************
//
//*********************************************************************************
