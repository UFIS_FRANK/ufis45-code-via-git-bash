#if !defined(AFX_PRINTDAILYROSTERB_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_)
#define AFX_PRINTDAILYROSTERB_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintDailyRoster1.h : header file
//
#include <PrintDailyRoster.h>

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRoster command target

class CPrintDailyRosterB : public CPrintDailyRoster
{
	DECLARE_DYNCREATE(CPrintDailyRosterB)

	CPrintDailyRosterB();									// protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	CPrintDailyRosterB(const COleDateTime& ropDate,const CString& ropViewName,const CStringArray& ropStfUrnos,const CString& ropLevel,const CString& ropKeyItems);
	virtual ~CPrintDailyRosterB();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintDailyRosterB)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void DeleteAll();
	virtual BOOL BuildGroups();
	virtual void GenerateSpecific(std::ofstream& of);

	// Generated message map functions
	//{{AFX_MSG(CPrintDailyRosterB)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:

	struct Team		// teams
	{
		CString					omName;
		CCSPtrArray<DRRDATA>	omDailyRosterRecords;
		CCSPtrArray<Absence>	omDailyRosterAbsences;
		COleDateTime			omTeamLeaderStartTime;
		COleDateTime			omTeamLeaderEndTime;

		~Team()
		{
			omDailyRosterAbsences.DeleteAll();
		}
	};

	struct Group		// shift groups
	{
		CString	omName;
		CMapStringToPtr			omBasicShiftMap;
		CMapStringToPtr			omTeams;			// team	
		CMapStringToPtr			omFunctions;		// without team

		~Group()
		{
			CString olName;
			Team *polTeam;
			POSITION pos = omTeams.GetStartPosition();
			while (pos != NULL)
			{
				omTeams.GetNextAssoc(pos,olName,(void *&)polTeam);
				delete polTeam;
			}

			pos = omFunctions.GetStartPosition();
			while (pos != NULL)
			{
				omFunctions.GetNextAssoc(pos,olName,(void *&)polTeam);
				delete polTeam;
			}
		}
	};

	struct	OrgUnit		// organisational units
	{
		CCSPtrArray<Group>		omGroups;
		~OrgUnit()
		{
			omGroups.DeleteAll();
		}
	};

	BOOL BuildOrgUnit(OrgUnit *popOrgUnit,const CString& ropOrgUnit);
	BOOL BuildGroup(Group *popGroup,SGRDATA& ropSgr,const CString& ropOrgUnit);

	void GenerateOrgUnit(std::ofstream& of,const CString& ropOrgUnit,OrgUnit *popOrgUnit);
	void GenerateGroup(std::ofstream& of,const CString& ropOrgUnit,Group *polGroup);
	void GenerateTeam(std::ofstream& of,Team *polTeam);

	static int CompareDrrByStfuAndTime(const DRRDATA **e1, const DRRDATA **e2);
	static int CompareTeamsByTime (const Team **e1, const Team **e2);
	static int CompareShiftGroupsByGrpn (const SGRDATA **e1, const SGRDATA **e2);
	static BOOL IsTeamLeader(const DRRDATA *popDrr);

	CMapStringToPtr				omOrgUnits;
	static long					lmViewGroupUrno;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTDAILYROSTERB_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_)
