#ifndef _CMSDSD_H_
#define _CMSDSD_H_
 
#include <stdafx.h>

/////////////////////////////////////////////////////////////////////////////

struct MSDDATA {
	char 	 Bsdc[8+2]; 	// Code
	char 	 Days[9]; 	// Verkehrstage
	char 	 Dnam[34]; 	// Bedarfsname
	char 	 Rema[62]; 	// Bemerkungen
	char 	 Type[3]; 	// Flag (statisch/dynamisch)
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	int 	 Sdu1/*[6]*/; 	// Regul�re Schichtdauer
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	long	 Bsdu;
	CTime	 Brkf;	//Pause von
	CTime	 Brkt;	//Pause bis
	CTime 	 Bkf1/*[6]*/; 	// Pausenlage von			====> char 	 Bkf1[6]
	CTime 	 Bkt1/*[6]*/; 	// Pausenlage bis			====> char 	 Bkt1
	CTime	 Sbgi;			//Schichtbeginn
	CTime	 Seni;			//Schichtende
	CTime 	 Cdat; 	// Erstellungsdatum
	CTime 	 Esbg; 	// Fr�hester Schichtbeginn		===> char 	 Esbg[6]
	CTime 	 Lsen; 	// Sp�testes Schichtende		===> char 	 Lsen[6]
	CTime 	 Lstu; 	// Datum letzte �nderung
	long	 Sdgu;
	char     Fctc[FCTC_LEN+2]; // Function

	int  IsChanged;	// Check whether Data has Changed f�r Relaese
	BOOL IsSelected;

	MSDDATA(void)
	{
		memset(this,'\0',sizeof(*this));

		IsSelected = FALSE;

		Bkf1 = TIMENULL;
		Bkt1 = TIMENULL;
		Cdat = TIMENULL;
		Esbg = TIMENULL;
		Lsen = TIMENULL;
		Lstu = TIMENULL;
		Brkf = TIMENULL;
		Brkt = TIMENULL;
	}
 };	
//typedef struct MSDDATAStruct MSDDATA;


// the broadcast CallBack function, has to be outside the CedaMsdData class
void ProcessMsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

class CedaMsdData: public CedaData
{
// Attributes
public:
    CCSPtrArray<MSDDATA> omData;

    CMapPtrToPtr omUrnoMap;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaMsdData();
	~CedaMsdData();
	void Register(void);
	bool Read(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<MSDDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS);

	void ProcessMsdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	bool AddMsdInternal(MSDDATA *prpMsd, bool bpWithDDX = false);
	bool UpdateMsdInternal(MSDDATA *prpMsd, bool bpWithDDX = false);
	bool DeleteMsdInternal(MSDDATA *prpMsd, bool bpWithDDX = false);

	bool InsertMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave = FALSE);
	bool UpdateMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave = FALSE);
	bool DeleteMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave = FALSE);

	bool ClearAll();

	MSDDATA * GetMsdByUrno(long pcpUrno);

	bool MsdExist(long Urno);

	bool SaveMsd(MSDDATA *prpMsd);

	void DeleteBySdgu(long lpSdgUrno);
	void GetMsdBySdgu(long lpSdgu,CCSPtrArray<MSDDATA> *popMSDDATA);

private:
	long GetUrnoFromSelectionString(CString opSelection);
	void CopyMsdValues(MSDDATA* popMsdDataSource, MSDDATA* popMsdDataTarget);
};

extern CedaMsdData ogMsdData;

#endif
