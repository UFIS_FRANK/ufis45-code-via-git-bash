// DutyDragDropDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyDragDropDlg 
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

DutyDragDropDlg::DutyDragDropDlg(CWnd* pParent /*=NULL*/) : CDialog(DutyDragDropDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DutyDragDropDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	pomParent = (DutyRoster_View*)pParent;
	pomStfData  = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;
	pomGroups   = &pomParent->omGroups;

	pomDragDropTable		= NULL;
	prmDragDropIPEAttrib	= NULL;
	pomDragDropTabViewer	= NULL;

	CDialog::Create(DutyDragDropDlg::IDD, pParent);
}

//---------------------------------------------------------------------------

DutyDragDropDlg::~DutyDragDropDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	pomDragDropTable->DragDropRevoke();
	pomDragDropTable->DeleteTable();
	delete prmDragDropIPEAttrib;
	delete pomDragDropTabViewer;
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyDragDropDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyDragDropDlg, CDialog)
	//{{AFX_MSG_MAP(DutyDragDropDlg)
    ON_WM_GETMINMAXINFO()
	ON_WM_SIZING()
	ON_MESSAGE(WM_DYNTABLE_VMOVE,		 VMoveTable)
	ON_MESSAGE(WM_DYNTABLE_VSCROLL,		 VertTableScroll)
	ON_MESSAGE(WM_DYNTABLE_LBUTTONDOWN,	 TableLButtonDown)
	ON_MESSAGE(WM_TABLE_DRAGBEGIN,		 OnDragBegin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyDragDropDlg 
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

BOOL DutyDragDropDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	CString olCaption;
	PGPDATA *prlPgp = NULL;
	prlPgp = ogPgpData.GetPgpByUrno(prmViewInfo->lPlanGroupUrno);
	if(prlPgp != NULL)
	{
		olCaption.Format("%s  %s",prlPgp->Pgpc, prmViewInfo->oSelectDate.Format("%d.%m.%Y"));
		SetWindowText(olCaption);
	}

	//>>>>>>Dialogsetup
	int ilCYCaption = GetSystemMetrics(SM_CYCAPTION);
	int ilCXEdge	= GetSystemMetrics(SM_CXSIZEFRAME);
	int ilCYEdge	= GetSystemMetrics(SM_CYSIZEFRAME);

	CRect olStartRect = pomParent->GetStartWindowRect();

	omWindowRect.left	= (olStartRect.right - olStartRect.left)/2;
	omWindowRect.top	= olStartRect.top + 100;
	omWindowRect.bottom	= olStartRect.bottom - 100;


	int ilScrollBar		 = 16; //is alway fix in CCSDYNTABLE
	int ilTopDistance	 = 10;
	int ilLeftDistance	 = 10;
	int ilBottomDistance = 10;
	int ilRightDistance  = 10;

	int ilDragDropTabColumns	= 2;
	int ilDragDropTabWidth		= 93;
	int ilDragDropTabSolidWidth	= 58;

	int ilTabFixWidth = ilDragDropTabWidth*ilDragDropTabColumns + ilDragDropTabSolidWidth
						+ ilScrollBar + 2*ilCXEdge + ilLeftDistance + ilRightDistance + 1;
	omWindowRect.right = omWindowRect.left + ilTabFixWidth + ilLeftDistance + ilRightDistance;

	//////////////////////////////

	int ilDragDropTabHight			= 18;
	int ilDragDropTabHeaderHight	= 18;

	int ilTabFixHight = ilDragDropTabHight + ilDragDropTabHeaderHight + ilCYCaption;
	int ilWndHight = omWindowRect.bottom - omWindowRect.top;
	int ilDragDropTabRows = (int)((ilWndHight - (ilDragDropTabHeaderHight + ilCYCaption + 2*ilCYEdge)) / ilDragDropTabHight);
	omWindowRect.bottom	= omWindowRect.top + (ilDragDropTabHight*ilDragDropTabRows + ilDragDropTabHeaderHight + ilCYCaption + 2*ilCYEdge) + 1 + ilTopDistance + ilBottomDistance;

	//////////////////////////////

	omMinTrackSize = CPoint(ilTabFixWidth, ilTabFixHight+9);
	omMaxTrackSize = CPoint(ilTabFixWidth, olStartRect.bottom);


	CRect olParentRect, olSystemRect;
	olSystemRect.left	= 0;
	olSystemRect.top	= 0;
	olSystemRect.right  = ::GetSystemMetrics(SM_CXSCREEN);
	olSystemRect.bottom = ::GetSystemMetrics(SM_CYSCREEN);
	
	pomParent->GetWindowRect(&olParentRect);

	int ilLeft = (olParentRect.left + 400) - omWindowRect.left;
	omWindowRect.left += ilLeft;
	omWindowRect.right += ilLeft;
	if(omWindowRect.right>olSystemRect.right)
	{
		ilLeft = omWindowRect.right - olSystemRect.right;
		omWindowRect.left -= ilLeft;
		omWindowRect.right -= ilLeft;
	}

	omWindowRect.top -= 20;
	omWindowRect.bottom -= 20;
	if(omWindowRect.bottom>olSystemRect.bottom)
	{
		int ilTop = omWindowRect.bottom - olSystemRect.bottom;
		omWindowRect.top -= ilTop;
		omWindowRect.bottom -= ilTop;
	}

	MoveWindow(&omWindowRect);


	//>> DragDrop Table *****
	CRect olDragDropTableRect = CRect(ilLeftDistance,ilTopDistance,0,0);
	imDragDropTableID = 424587;
	pomDragDropTable	= new CCSDynTable(this,imDragDropTableID);

	pomDragDropTable->SetTableData(olDragDropTableRect,ilDragDropTabColumns,ilDragDropTabWidth,ilDragDropTabHeaderHight,ilDragDropTabRows,
							 ilDragDropTabHight,ilDragDropTabSolidWidth,false,false,false,true,false,true);
	pomDragDropTable->SetDefaultColors(COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2),
								 COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER1), COLORREF(DT_BLACK));
	pomDragDropTable->SetDefaultColors(9,COLORREF(DT_LIGHTYELLOW1));
	pomDragDropTable->SetDifferenceToParentWndRect(0,-24);
	prmDragDropIPEAttrib = new CCSEDIT_ATTRIB;
	pomDragDropTable->SetDefaultTextForm(&ogMSSansSerif_Regular_8, &ogMSSansSerif_Italic_8, &ogMSSansSerif_Regular_8, &ogMSSansSerif_Regular_8,
								   DT_SINGLELINE|DT_VCENTER, DT_SINGLELINE|DT_VCENTER,
								   DT_SINGLELINE|DT_CENTER|DT_VCENTER, prmDragDropIPEAttrib);
	pomDragDropTable->Create();
	pomDragDropTable->DragRegister(DIT_FROMGROUP_TODUTYROSTER);//DIT_FROMDUTYROSTER_TOGROUP
	pomDragDropTabViewer  =  new DutyDragDropTabViewer(this);
	pomCurrentDragDropViewer = (CViewer *)pomDragDropTabViewer;
	pomCurrentDragDropViewer->SetViewerKey(CString("DRAGDROP_TABLE"));
	pomDragDropTabViewer->Attach(pomDragDropTable);
	//<< DragDrop Tab
	//<<<<<< Create Tables

	pomDragDropTabViewer->ChangeView();

	ShowWindow(SW_SHOWNORMAL);
	return TRUE;
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::InternalCancel()
{
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::OnCancel()
{
	//do nothing more
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::OnOK() 
{
	//do nothing more
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    CDialog::OnGetMinMaxInfo(lpMMI);
    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::OnSizing(UINT nSide, LPRECT lpRect ) 
{
	CDialog::OnSizing(nSide, lpRect);
	pomDragDropTabViewer->TableSizing(/*nSide,*/lpRect);
}

//---------------------------------------------------------------------------

LONG DutyDragDropDlg::VMoveTable(UINT wParam, LONG lParam) 
{
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;

	if(wParam == imDragDropTableID)
	{
		pomDragDropTabViewer->MoveTable(prlModiTab);
	}

	return 0L;
}

//---------------------------------------------------------------------------

LONG DutyDragDropDlg::VertTableScroll(UINT wParam, LONG lParam) 
{
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;

	if(wParam == imDragDropTableID)
	{
		pomDragDropTabViewer->VertTableScroll(prlModiTab);
	}
	return 0L;
}

//---------------------------------------------------------------------------

LONG DutyDragDropDlg::OnDragBegin(UINT wParam, LONG lParam)
{
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;

	if(wParam == imDragDropTableID)
	{
		omDragDropObject.CreateDWordData(DIT_FROMGROUP_TODUTYROSTER, 1);

		CString olAddText, olTemp;

		olTemp.Format("%d|", prlInlineData->iFlag		);
		olAddText += olTemp;
		olTemp.Format("%d|", prlInlineData->iRow		);
		olAddText += olTemp;
		olTemp.Format("%d|", prlInlineData->iColumn		);
		olAddText += olTemp;
		olTemp.Format("%d|", prlInlineData->iRowOffset	);
		olAddText += olTemp;
		olTemp.Format("%d|", prlInlineData->iColumnOffset);
		olAddText += olTemp;
		olTemp.Format("%s|", prlInlineData->oOldText	);
		olAddText += olTemp;
		olTemp.Format("%s|", prlInlineData->oNewText	);
		olAddText += olTemp;
		olTemp.Format("%d|", prlInlineData->oPoint.x	);
		olAddText += olTemp;
		olTemp.Format("%d",  prlInlineData->oPoint.y	);
		olAddText += olTemp;

		omDragDropObject.AddString(olAddText);

		pomParent->bmDragDopIsOK = true;
		omDragDropObject.BeginDrag();
	}
	return 0L;
}

//---------------------------------------------------------------------------

LONG DutyDragDropDlg::TableLButtonDown(UINT wParam, LONG lParam) 
{
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	int ilRow	 = prlInlineData->iRow;
	int ilColumn = prlInlineData->iColumn;

	if(wParam == imDragDropTableID)
	{
		if(ilRow > -1 && ilColumn > -1) //Table
		{
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
		}
	}
	return 0L;
}

//---------------------------------------------------------------------------

void DutyDragDropDlg::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/) 
{
	CCS_TRY;
	CString olCaption;
	PGPDATA *prlPgp = NULL;
	prlPgp = ogPgpData.GetPgpByUrno(prmViewInfo->lPlanGroupUrno);
	if(prlPgp != NULL)
	{
		olCaption.Format("%s  %s",prlPgp->Pgpc, prmViewInfo->oSelectDate.Format("%d.%m.%Y"));
		SetWindowText(olCaption);
	}

	pomDragDropTabViewer->ChangeView(bpSetHNull, bpSetVNull);
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------