// ShiftAWDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftAWDlg 
//---------------------------------------------------------------------------
void  ProcessDropStaffCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{ 
	ShiftAWDlg *polShiftAWDlg = (ShiftAWDlg*)vpInstance;
	polShiftAWDlg->ProcessDropStaff(ipDDXType,vpDataPointer,ropInstanceName);
}

ShiftAWDlg::ShiftAWDlg(CString *popUrnoList, UINT ipAWType,
					   bool bpSelect,CString opVpfr,CString opVpto,
					   CWnd* pParent /*=NULL*/, CCSButtonCtrl* pButton /*=NULL*/): CDialog(ShiftAWDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftAWDlg)
	//}}AFX_DATA_INIT
	pomParent	= (ShiftRoster_View*)pParent;
	bmSelect	= bpSelect;
	pomUrnoList = popUrnoList;
	imAWType	= ipAWType;
	omVpfr		= opVpfr;
	omVpto		= opVpto;
	pgrid		= NULL;
	pomButton	= pButton;

	lmLastClickedRow = -1;

	//register for DROP_STAFF
	ogDdx.Register((void *)this,DROP_STAFF,CString("DROP_STAFF"), CString("DROP_STAFF"),ProcessDropStaffCf);

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(IDD_SHIFTROSTER_AW_DLG, pomParent);
}

ShiftAWDlg::~ShiftAWDlg(void)
{
	if (::IsWindow(pomParent->m_hWnd))
	{
		pomParent->pomShiftAWDlg = NULL;
	}
}

void ShiftAWDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftAWDlg)
	DDX_Control(pDX, IDC_CHECK_SHOWALL, m_Check_ShowAll);
	DDX_Control(pDX, ID_INFO,			m_B_Info);
	DDX_Control(pDX, IDOK,				m_B_Ok);
	DDX_Control(pDX, IDCANCEL,			m_B_Cancel);
	DDX_Control(pDX, IDC_B_DELETEALL,	m_B_DeleteAll);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ShiftAWDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftAWDlg)
	ON_BN_CLICKED(IDC_B_DELETEALL,		OnBDeleteall)
	ON_BN_CLICKED(ID_INFO,				OnInfo)
	ON_BN_CLICKED(IDC_CHECK_SHOWALL,	OnCheckShowAll)
	ON_MESSAGE(WM_GRID_LBUTTONDOWN,		OnGridLButton)
	ON_MESSAGE(WM_GRID_DRAGBEGIN,		OnGridDragBegin)
	ON_WM_DESTROY()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#define SHIFTAWD_MINROWS 1 //26

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ShiftAWDlg 
//---------------------------------------------------------------------------
//
BOOL ShiftAWDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	IniEmplGrid();

	return TRUE; 
}


//---------------------------------------------------------------------------

void ShiftAWDlg::OnOK() 
{
	if (!pomParent->IsVisibleGplActive())
		return;

	CRowColArray awRows;

	ROWCOL ilMaxItems;
	ROWCOL ilSelRow;

	CGXStyle style;
	long llUrno;

	ilMaxItems = pgrid->GetSelectedRows(awRows, FALSE, FALSE);

	if (ilMaxItems < 90)
	{
		CString olTmpTxt;
		CString olUrno;
		CString olNewUrnos;

		for (ROWCOL i = 0; i < ilMaxItems; i++)
		{
			ilSelRow = awRows[i];
			pgrid->ComposeStyleRowCol(ilSelRow, 1, &style );
			llUrno = (long)style.GetItemDataPtr();

		 	if (llUrno)	//Urno erhalten
			{
				olUrno.Format("%ld", llUrno);

				if (pomParent->omStaffUrnosAll.Find (olUrno) < 0)
				{
					if (pomUrnoList->GetLength() > 0)
					{
						*pomUrnoList += '|' + olUrno;
					}
					else
					{
						*pomUrnoList += olUrno;
					}
					olNewUrnos   += '|' + olUrno;
					pgrid->SetStyleRange(CGXRange().SetRows(ilSelRow), CGXStyle().SetTextColor(RED));
				}
			}
		}

		if (olNewUrnos.GetLength() > 0)
		{
			if (pomParent->omStaffUrnosAll.GetLength() > 0)
			{
				pomParent->omStaffUrnosAll += olNewUrnos;			// with beginning '|'
			}
			else
			{
				pomParent->omStaffUrnosAll += olNewUrnos.Mid(1);	// without beginning '|'
			}
			pomParent->OnAddEmployees(olNewUrnos.Mid(1), pomButton);
		}

		//CDialog::OnOK();
	}
	else
	{
		CString olTxt;
		olTxt.Format(LoadStg(IDS_STRING1773), 90);
		Beep(440,70);
		MessageBox(olTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
}

//---------------------------------------------------------------------------

void ShiftAWDlg::OnBDeleteall() 
{
	if (!pomParent->IsVisibleGplActive())
		return;

	//collecting the employee URNOs of the selected lines
	CRowColArray olRowArray;
	ROWCOL ilCount = pgrid->GetSelectedRows (olRowArray, FALSE, FALSE);

	if (ilCount > 0)
	{
		CString	olUrnos;
		CString	olUrno;
		int ilPos1;
		ROWCOL i;
		CGXStyle olStyle;

		for (i = 0; i < ilCount; i++)
		{
			if (pgrid->GetStyleRowCol (olRowArray[i], 1, olStyle) && olStyle.GetIncludeItemDataPtr())
			{
				olUrno.Format ("%ld", (long)olStyle.GetItemDataPtr());
				ilPos1 = pomParent->omStaffUrnosAll.Find (olUrno);
				if (ilPos1 > -1)
				{
					//add the URNO
					olUrnos += '|' + olUrno;

					//color the text black
					pgrid->SetStyleRange(CGXRange().SetRows(olRowArray[i]), CGXStyle().SetTextColor(BLACK));
				}
			}
		}
		if (olUrnos.GetLength() > 0)
		{
			olUrnos = olUrnos.Mid(1);
			pomParent->OnDeleteEmployees (olUrnos);
		}

		// if employees are deleted after opening this dlg with the left mouse button,
		// we have to delete them from the grid (PRF 3662)
		if (bmSelect == false)
		{
			ilCount = pgrid->GetSelectedRows (olRowArray, FALSE, FALSE);
			for (int j = ilCount-1; j > -1 ; j--)
			{
				pgrid->RemoveRows(olRowArray[j],olRowArray[j]);
			}
			/*while (pgrid->GetSelectedRows (olRowArray, FALSE, FALSE) > 0)
			{
				pgrid->RemoveRows(olRowArray[0],olRowArray[0]);
			}*/
		}
	}
	
	pgrid->SetSelection (NULL);
}

//---------------------------------------------------------------------------

void ShiftAWDlg::OnInfo() 
{
	MessageBox(LoadStg(IDS_STRING1794), LoadStg(IDS_INFO), NULL);
}

//---------------------------------------------------------------------------

void ShiftAWDlg::IniEmplGrid()
{
	CString olLine;
	CString olTmpTxt;

	if(bmSelect == false && pomParent->omMouseClick == "L")
	{
		m_B_Ok.EnableWindow(false);
		//m_B_DeleteAll.EnableWindow(false);
		m_Check_ShowAll.EnableWindow(false);
	}

	m_B_Ok.SetWindowText(LoadStg(SHIFT_STF_B_OK));
	m_B_Cancel.SetWindowText(LoadStg(IDS_C_CLOSE));				//Schlie�en
	m_B_DeleteAll.SetWindowText(LoadStg(IDS_STRING63451));		//Zuweisung l�schen
	m_Check_ShowAll.SetWindowText(LoadStg(IDS_SHOW_ALL));

	SetDlgItemText(ID_INFO,LoadStg(SHIFT_B_INFO));

	m_Check_ShowAll.ShowWindow(SW_SHOW);
	if(pomParent->bmShowAll == true || pomParent->omMouseClick == "L")
	{
		m_Check_ShowAll.SetCheck(1);
	}
	else
	{
		m_Check_ShowAll.SetCheck(0);
	}

	m_B_Info.ShowWindow(SW_SHOW);

	SetWindowText(LoadStg(SHIFT_S_STAFF));

	CString *prlSurn = NULL;
	CString olSurn;
	CMapStringToPtr olSpfMap;
	int ilSpfCount =  ogSpfData.omData.GetSize();
	CMapStringToPtr olSwgMap;
	int ilSwgCount =  ogSwgData.omData.GetSize();
	CMapStringToPtr olSpeMap;
	int ilSpeCount =  ogSpeData.omData.GetSize();
	CMapStringToPtr olScoMap;
	int ilScoCount =  ogScoData.omData.GetSize();

	//Get all Functions
	SPFDATA *prlSpfData = NULL;
	CString olVpto;
	CString olVpfr;

	for (int iSpf = 0; iSpf < ilSpfCount; iSpf++)
	{
		prlSpfData = &ogSpfData.omData[iSpf];
		olSurn.Format("%ld",prlSpfData->Surn);
		olVpto = prlSpfData->Vpto.Format("%Y%m%d");
		olVpfr = prlSpfData->Vpfr.Format("%Y%m%d");
		if(!(olVpto < omVpfr || olVpfr > omVpto) || olVpto == "" || olVpfr == "")
		{
			olTmpTxt.Format("%s(%s)", prlSpfData->Fctc, prlSpfData->Prio);
			if(olSpfMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
			{
				*prlSurn +=  "," + olTmpTxt;
			}
			else
			{
				CString *polCode = new CString;
				*polCode = olTmpTxt;
				olSpfMap.SetAt(olSurn,polCode);
			}
		}
	} //END of Get all Functions

	//Get all Contracts
	for(int iSco = 0; iSco < ilScoCount; iSco++)
	{
		olSurn.Format("%ld",ogScoData.omData[iSco].Surn);
		CString olVpto = ogScoData.omData[iSco].Vpto.Format("%Y%m%d");
		CString olVpfr = ogScoData.omData[iSco].Vpfr.Format("%Y%m%d");
		if(!(olVpto < omVpfr || olVpfr > omVpto) || olVpto == "" || olVpfr == "")
		{
			if(olScoMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
			{
				*prlSurn +=  "," + (CString)ogScoData.omData[iSco].Code;
			}
			else
			{
				CString *polCode = new CString;
				*polCode = (CString)ogScoData.omData[iSco].Code;
				olScoMap.SetAt(olSurn,polCode);
			}
		}
	} //END of Get all Contracts

	//Get all Work Groups
	for(int iSwg=0; iSwg<ilSwgCount;iSwg++)
	{
		olSurn.Format("%ld",ogSwgData.omData[iSwg].Surn);
		CString olVpto = ogSwgData.omData[iSwg].Vpto.Format("%Y%m%d");
		CString olVpfr = ogSwgData.omData[iSwg].Vpfr.Format("%Y%m%d");
		if(!(olVpto < omVpfr || olVpfr > omVpto) || olVpto == "" || olVpfr == "")
		{
			if(olSwgMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
			{
				*prlSurn +=  "," + (CString)ogSwgData.omData[iSwg].Code;
			}
			else
			{
				CString *polCode = new CString;
				*polCode = (CString)ogSwgData.omData[iSwg].Code;
				olSwgMap.SetAt(olSurn,polCode);
			}
		}
	}
	//END of Get all Work Groups

	//Get all Qualifications
	for(int iSpe=0; iSpe<ilSpeCount;iSpe++)
	{
		olSurn.Format("%ld",ogSpeData.omData[iSpe].Surn);
		CString olVpto = ogSpeData.omData[iSpe].Vpto.Format("%Y%m%d");
		CString olVpfr = ogSpeData.omData[iSpe].Vpfr.Format("%Y%m%d");
		if(!(olVpto < omVpfr || olVpfr > omVpto) || olVpto == "" || olVpfr == "")
		{
			if(olSpeMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
			{
				*prlSurn +=  "," + (CString)ogSpeData.omData[iSpe].Code;
			}
			else
			{
				CString *polCode = new CString;
				*polCode = (CString)ogSpeData.omData[iSpe].Code;
				olSpeMap.SetAt(olSurn,polCode);
			}
		}
	}
	//END of Get all Qualifications

	int ilCount =  ogStfData.omData.GetSize();

	CString olQualificationUrnos = pomParent->omViewQualificationUrnos;
	CString olFunctionUrnos = pomParent->omViewFunctionUrnos;
	omStfLines.DeleteAll();

	for (int i = 0; i < ilCount; i++)
	{
		AWSTFVIEWDATA_SHFT rlStfView;

		rlStfView.Peno = ogStfData.omData[i].Peno;
		rlStfView.Perc = ogStfData.omData[i].Perc;
		rlStfView.Finm = ogStfData.omData[i].Finm;
		rlStfView.Lanm = ogStfData.omData[i].Lanm;
		rlStfView.Makr = ogStfData.omData[i].Makr;
		rlStfView.Dodm = ogStfData.omData[i].Dodm;
		rlStfView.Doem = ogStfData.omData[i].Doem;
		
		CString olSurn;
		olSurn.Format("%ld",ogStfData.omData[i].Urno);
		rlStfView.Urno = olSurn;

		if (olSpfMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			rlStfView.Prmc = *prlSurn;//GetListItem(*prlSurn, 1, false, ',');//SortItemList(*prlSurn,',');
		}
		if (olScoMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			rlStfView.Code = SortItemList(*prlSurn,',');
		}
		if (olSwgMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			rlStfView.Wgpc = SortItemList(*prlSurn,',');
		}
		if (olSpeMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			rlStfView.Qual = SortItemList(*prlSurn,',');
		}

		if (bmSelect == true)
		{
			CString olPerUrno;
			PERDATA *polPer;

			//uhi 26.4.01 Filtern nach Qualifikation/ 4.12.01 nach Funktionen
			CString olQualification = rlStfView.Qual;
			CString *polText;

			CString cpTrenner = ',';
			CCSPtrArray<CString> olArray;

			BOOL blEnd = FALSE;

			if (!olQualification.IsEmpty() &&  !olQualificationUrnos.IsEmpty())
			{
				int ilPos;
				while (blEnd == FALSE)
				{
					polText = new CString;
					ilPos	= olQualification.Find(cpTrenner);
					if (ilPos == -1)
					{
						blEnd = TRUE;
						*polText = olQualification;
					}
					else
					{
						*polText = olQualification.Mid(0, olQualification.Find(cpTrenner));
						olQualification = olQualification.Mid(olQualification.Find(cpTrenner)+1, olQualification.GetLength( )-olQualification.Find(cpTrenner)+1);
					}
					olArray.Add(polText);
				}

				for (int i = 0; i < olArray.GetSize(); i++)
				{
					polPer = NULL;
					polPer = ogPerData.GetPerByCode(olArray[i]);
					if (polPer != NULL)
					{
						olPerUrno.Format("%ld", polPer->Urno);							
						if (olQualificationUrnos.Find(olPerUrno, 0) != -1)
						{
							if (FilterFunction(&rlStfView))
							{
								omStfLines.NewAt(omStfLines.GetSize(), rlStfView);
							}
							break;
						}
					}
				}
			}
			else
			{
				if (olQualification.IsEmpty() && olQualificationUrnos.Find("NO_CODE", 0) != -1)
				{
					if (FilterFunction(&rlStfView))
					{
						omStfLines.NewAt(omStfLines.GetSize(), rlStfView);
					}
				}
			}

			olArray.DeleteAll();
		}
		else
		{
			if (pomUrnoList != NULL)
			{
				if(pomUrnoList->Find(rlStfView.Urno) != -1)
				{
					omStfLines.NewAt(omStfLines.GetSize(), rlStfView);
				}
			}
		}
	}

	//Deleting Maps
	CString *prlString;
	CString olText;
  	for(POSITION olPos = olSpfMap.GetStartPosition(); olPos != NULL; )
	{
		olSpfMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpfMap.RemoveAll();
  	for(olPos = olScoMap.GetStartPosition(); olPos != NULL; )
	{
		olScoMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olScoMap.RemoveAll();
  	for(olPos = olSwgMap.GetStartPosition(); olPos != NULL; )
	{
		olSwgMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSwgMap.RemoveAll();
  	for(olPos = olSpeMap.GetStartPosition(); olPos != NULL; )
	{
		olSpeMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpeMap.RemoveAll();
	//END - Deleting Maps

	//Grid initialisieren
	ilCount = omStfLines.GetSize();
	if (pgrid == NULL)
	{
		pgrid = new CGridControl(this, IDC_AW_GRID, 8, __max(SHIFTAWD_MINROWS, ilCount), 0);
	}
	else
	{
		if (pgrid->GetRowCount() > 0)
		{
			pgrid->RemoveRows (1, pgrid->GetRowCount());
		}
	}

	pgrid->omSortInfo.SetSize(2);
	pgrid->omSortInfo[0].sortOrder = pomParent->omSortInfo[0].sortOrder;
	pgrid->omSortInfo[1].sortOrder = pomParent->omSortInfo[1].sortOrder;
	pgrid->omSortInfo[0].nRC = pomParent->omSortInfo[0].nRC;
	pgrid->omSortInfo[1].nRC = pomParent->omSortInfo[1].nRC;
	pgrid->omSortInfo[0].sortType = pomParent->omSortInfo[0].sortType;
	pgrid->omSortInfo[1].sortType = pomParent->omSortInfo[1].sortType;

	//int ilCount;
	int ilColCount;
	long llUrno;

	CString olUrno;

	CGXStyle style;
	
	//�berschriften setzten
	pgrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING367));
	pgrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_S_STAFF));
	pgrid->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_FUNCTION));
	pgrid->SetValueRange(CGXRange(0,4), LoadStg(SHIFT_S_CONTRACT));
	pgrid->SetValueRange(CGXRange(0,5), LoadStg(SHIFT_STF_WGRP));
	pgrid->SetValueRange(CGXRange(0,6), LoadStg(IDS_QUALIFIKATION));
	pgrid->SetValueRange(CGXRange(0,7), LoadStg(IDS_STRING139));
	pgrid->SetValueRange(CGXRange(0,8), LoadStg(IDS_STRING140));

	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pgrid->GetParam()->EnableTrackColWidth(FALSE);
	pgrid->GetParam()->EnableTrackRowHeight(FALSE);
	pgrid->GetParam()->EnableSelection(GX_SELROW|GX_SELMULTIPLE);

	//Scrollbars anzeigen wenn n�tig
	pgrid->SetScrollBarMode(SB_VERT, gxnAutomatic, TRUE);
	pgrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetReadOnly(TRUE));

	//width of the columns
	pgrid->SetColWidth(0, 0, pgrid->Width_LPtoDP(0 * GX_NXAVGWIDTH));	//empty
	pgrid->SetColWidth(1, 1, pgrid->Width_LPtoDP(9 * GX_NXAVGWIDTH));	//personal number
	pgrid->SetColWidth(2, 2, pgrid->Width_LPtoDP(44 * GX_NXAVGWIDTH));	//1st & 2nd name
	pgrid->SetColWidth(3, 3, pgrid->Width_LPtoDP(32 * GX_NXAVGWIDTH));	//function
	pgrid->SetColWidth(4, 4, pgrid->Width_LPtoDP(11 * GX_NXAVGWIDTH));	//contract
	pgrid->SetColWidth(5, 5, pgrid->Width_LPtoDP(14 * GX_NXAVGWIDTH));	//working group
	pgrid->SetColWidth(6, 6, pgrid->Width_LPtoDP(11 * GX_NXAVGWIDTH));	//qualific.
	pgrid->SetColWidth(7, 8, pgrid->Width_LPtoDP(9 * GX_NXAVGWIDTH));	//entry,leaving

	pgrid->EnableAutoGrow(false);
	pgrid->EnableSorting(true);
	pgrid->SetEnableDnD (true);

	FillEmplGrid();

	pgrid->SortRows(CGXRange().SetTable(), pomParent->omSortInfo);

	ilColCount = pgrid->GetColCount ();

	if (bmSelect == true)
	{
		ilCount = pgrid->GetRowCount();
		for (int i=ilCount; i>0; i--)
		{
			pgrid->ComposeStyleRowCol(i, 1, &style);

			llUrno = (long)style.GetItemDataPtr();
			if (llUrno) // Urno erhalten
			{
				olUrno.Format("%d", llUrno);
				if(pomUrnoList->Find(olUrno) != -1)
				{
					POSITION area = pgrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
					pgrid->SetSelection(area,i,0,i,ilColCount);
				}
			}
		}
	}
}

void ShiftAWDlg::OnCheckShowAll() 
{
	if(imAWType == BONUS)
		return;

	if(m_Check_ShowAll.GetCheck() == 1)
	{
		pomParent->bmShowAll = true;
	}
	else
	{
		pomParent->bmShowAll = false;
	}

	//Sortierung abspeichern
	pomParent->omSortInfo[0].nRC = pgrid->omSortInfo[0].nRC;
	pomParent->omSortInfo[0].sortOrder = pgrid->omSortInfo[0].sortOrder;

	pgrid->ShowWindow(SW_HIDE);
	FillEmplGrid();
	pgrid->ShowWindow(SW_SHOW);
}

void ShiftAWDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	if (imAWType == EMPLOYEE)
	{
		if (::IsWindow(pomParent->m_hWnd))
		{
			//Sortierung abspeichern
			pomParent->omSortInfo[0].nRC = pgrid->omSortInfo[0].nRC;
			pomParent->omSortInfo[0].sortOrder = pgrid->omSortInfo[0].sortOrder;
			pomParent->pomShiftAWDlg = NULL;
		}
	}

	if (pgrid != NULL)
	{
		delete pgrid;
	}

	omStfLines.DeleteAll();

	//unregister for DROP_STAFF
	ogDdx.UnRegister(this,NOTUSED);

	CDialog::OnCancel();
}

void ShiftAWDlg::FillEmplGrid()
{
	int ilCount;
	int ilColCount;

	long llUrno;

	CString olLine;
	CString olUrno;
	CString olTmpTxt;

	CGXStyle style;

	// Grid mit Daten f�llen.
	pgrid->GetParam()->SetLockReadOnly(false);

	if (pgrid->GetRowCount() > 0)
	{
		pgrid->RemoveRows(1, pgrid->GetRowCount());
	}

	ilCount = omStfLines.GetSize();
	pgrid->SetRowCount(ilCount);
	STFDATA* prlStfData = NULL;

	for (int i = ilCount - 1; i >= 0; i--)
	{
		// Urno auslesen
		llUrno = atol(omStfLines[i].Urno);

		prlStfData = ogStfData.GetStfByUrno(llUrno);
		olTmpTxt = pomParent->GetFormatedEmployeeName (prlStfData);
		
		COLORREF olColor = 0;
		CString olVpto = omStfLines[i].Dodm.Format("%Y%m%d");
		CString olVpfr = omStfLines[i].Doem.Format("%Y%m%d");
		if(olVpto == "") olVpto = "99999999";
		if(olVpfr == "") olVpfr = "00000000";

		if(!(olVpfr <= omVpto && olVpto >= omVpfr))
		{
			olColor = SILVER;
		}

		if(pomParent->omStaffUrnosAll.Find(omStfLines[i].Urno) != -1)
		{
			if(olColor != 0)
			{
				olColor = FUCHSIA;
			}
			else
			{
				olColor = RED;
			}
		}

		// Urno mit erstem Feld koppeln.						
		pgrid->SetStyleRange (CGXRange (i+1, 1), CGXStyle().SetItemDataPtr((void*)llUrno));
		pgrid->SetValueRange (CGXRange (i+1, 0), CString(""));
		pgrid->SetValueRange (CGXRange (i+1, 1), omStfLines[i].Peno);
		pgrid->SetValueRange (CGXRange (i+1, 2), olTmpTxt);
		pgrid->SetValueRange (CGXRange (i+1, 3), omStfLines[i].Prmc);
		pgrid->SetValueRange (CGXRange (i+1, 4), omStfLines[i].Code);
		pgrid->SetValueRange (CGXRange (i+1, 5), omStfLines[i].Wgpc);
		pgrid->SetValueRange (CGXRange (i+1, 6), omStfLines[i].Qual);
		pgrid->SetValueRange (CGXRange (i+1, 7), omStfLines[i].Doem.Format("%d.%m.%Y"));
		pgrid->SetValueRange (CGXRange (i+1, 8), omStfLines[i].Dodm.Format("%d.%m.%Y"));

		if (olColor != 0)
		{
			pgrid->SetStyleRange(CGXRange().SetRows(i+1), CGXStyle().SetTextColor(olColor));
			if (pomParent->bmShowAll == false && (olColor == FUCHSIA || olColor == RED) && pomParent->omMouseClick == "R")
			{
				pgrid->RemoveRows(i + 1, i + 1);
			}
		}
		else
		{
			pgrid->SetStyleRange(CGXRange().SetRows(i+1), CGXStyle().SetTextColor(RGB(0, 0, 0)));
		}
	}

	pgrid->SortRows(CGXRange().SetTable(), pomParent->omSortInfo);

	ilColCount = pgrid->GetColCount();
	if (bmSelect == true)
	{
		ilCount = pgrid->GetRowCount();
		for (int i = ilCount; i > 0; i--)
		{
			pgrid->ComposeStyleRowCol(i, 1, &style);
			llUrno = (long)style.GetItemDataPtr();
			if (llUrno)	// Urno erhalten
			{
				olUrno.Format("%d", llUrno);
				if (pomUrnoList->Find(olUrno) != -1)
				{
					POSITION area = pgrid->GetParam()->GetRangeList()->AddTail(new CGXRange);
					pgrid->SetSelection(area,i,0,i,ilColCount);
				}
			}
		}
	}

	pgrid->GetParam()->SetLockReadOnly(true);
}


LONG ShiftAWDlg::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
	ROWCOL			ColCount		= pgrid->GetColCount();
	GRIDNOTIFY*		rlNotify		= (GRIDNOTIFY*) lParam;
	CGXRangeList*	polRangeList	= pgrid->GetParam()->GetRangeList();
	CGXStyle olStyle;

	// weder Shift noch Control => nur die eine Zeile markieren, alle anderen Selektierungen aufheben
	if (((wParam & MK_SHIFT) != MK_SHIFT) && ((wParam & MK_CONTROL) != MK_CONTROL))
	{
		pgrid->SetSelection(NULL);
		pgrid->ComposeStyleRowCol(rlNotify->row, 1, &olStyle);
		if (IsEmployeeGreyed((long)olStyle.GetItemDataPtr()) == false)
		{
			pgrid->SelectRange(CGXRange(rlNotify->row, 0, rlNotify->row, ColCount));
		}
	}

	// wenn nur Shift => alles zwischen letzter und aktuell geklickter Zeile markieren
	else if (((wParam & MK_SHIFT) == MK_SHIFT) && ((wParam & MK_CONTROL) != MK_CONTROL))
	{
		if (lmLastClickedRow > -1)
		{
			int ilMin = min(lmLastClickedRow,rlNotify->row);
			int ilMax = max(lmLastClickedRow,rlNotify->row);
			//pgrid->SelectRange(CGXRange(ilMin, 0, ilMax, ColCount));
			for (int i = ilMin; i <= ilMax; i++)
			{
				pgrid->ComposeStyleRowCol(i, 1, &olStyle);
				if (IsEmployeeGreyed((long)olStyle.GetItemDataPtr()) == false)
				{
					pgrid->SelectRange(CGXRange(i, 0, i, ColCount));
				}
			}
		}
	}

	// wenn nur Control => toggeln
	else if (((wParam & MK_SHIFT) != MK_SHIFT) && ((wParam & MK_CONTROL) == MK_CONTROL))
	{
		if (polRangeList->IsAnyCellFromRow (rlNotify->row) > 0)
		{
			// Zeile ist selektiert => de-selektieren
			//pgrid->SelectRange(CGXRange(rlNotify->row, 0, rlNotify->row, ColCount),FALSE,TRUE);
		}
		else
		{
			// Zeile noch nicht selektiert => selektieren
			pgrid->ComposeStyleRowCol(rlNotify->row, 1, &olStyle);
			if (IsEmployeeGreyed((long)olStyle.GetItemDataPtr()) == false)
			{
				pgrid->SelectRange(CGXRange(rlNotify->row, 0, rlNotify->row, ColCount));
			}
		}
	}

	// Zeile merken
	lmLastClickedRow = rlNotify->row;

	return 0L;
}

bool ShiftAWDlg::FilterFunction(AWSTFVIEWDATA_SHFT *popStf)
{
	CString olFunctionUrnos = pomParent->omViewFunctionUrnos;

	if (olFunctionUrnos.Find("NO_CODE", 0) != -1)
	{
		return true;
	}

	PFCDATA *polPfc = NULL;
	CString olFunctions = popStf->Prmc;
	CString olPfcUrno;

	CStringArray	olStrArr;
	CedaData::ExtractTextLineFast (olStrArr, olFunctions.GetBuffer (0), ",");

	CString olTmp;
	char* cpTmp;
	for (int i = 0; i < olStrArr.GetSize(); i++)
	{
		cpTmp = strstr(olStrArr[i].GetBuffer(0),"(");
		if (cpTmp != NULL)
		{
			*cpTmp = '\0';
		}
		polPfc = ogPfcData.GetPfcByFctc(olStrArr[i]);
		if (polPfc != NULL)
		{
			olPfcUrno.Format("%ld", polPfc->Urno);
			if (olFunctionUrnos.Find(olPfcUrno, 0) != -1)
			{
				return true;
			}
		}
	}

	return false;
}

LONG ShiftAWDlg::OnGridDragBegin(WPARAM wParam, LPARAM lParam)
{
	if (!pgrid || !pomParent)
		return 0L;

	//looking after the selection
	OnGridLButton(wParam, lParam);

	if (!pomParent->IsVisibleGplActive())
		return 0L;

	//collecting the employee URNOs of the selected lines
	CRowColArray olRowArray;
	ROWCOL ilCount = pgrid->GetSelectedRows (olRowArray, FALSE, FALSE);

	CString			olUrno;
	CStringArray	olUrnoArray;

	if (ilCount > 0)
	{
		for (ROWCOL i = 0; i < ilCount; i++)
		{
			CGXStyle olStyle;
			if (pgrid->GetStyleRowCol (olRowArray[i], 1, olStyle) && olStyle.GetIncludeItemDataPtr())
			{
				olUrno.Format ("%ld", (long)olStyle.GetItemDataPtr());
				if (pomParent->omStaffUrnosAll.Find (olUrno) < 0)
				{
					olUrnoArray.Add (olUrno);
				}
			}
		}
	}

	if (olUrnoArray.GetSize() > 0)
	{
		omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_STAFF_TABLE, olUrnoArray.GetSize());
		for (int iUrno = 0; iUrno < olUrnoArray.GetSize(); iUrno++)
		{
			omDragDropObject.AddDWord(atol(olUrnoArray[iUrno].GetBuffer(0)));
		}

		// Aktion fortsetzen
		omDragDropObject.BeginDrag();
	}
	return 0L;
}

void ShiftAWDlg::ProcessDropStaff(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (ipDDXType == DROP_STAFF)
	{
		int			ilCount = pgrid->GetRowCount();
		long		llUrno	= 0;
		CGXStyle	style;
		CString		olUrno;
		CString		olUrnos = CString((char*)vpDataPointer);

		for (int i = ilCount; i > 0; i--)
		{
			pgrid->ComposeStyleRowCol(i, 1, &style );
			llUrno = (long)style.GetItemDataPtr();
			if (llUrno)
			{
				olUrno.Format("%d", llUrno);
				if (olUrnos.Find(olUrno) != -1)
				{
					if (m_Check_ShowAll.GetCheck() == 0)
					{
						//delete the line
						pgrid->RemoveRows (i, i);
					}
					else
					{
						//color the line red
						pgrid->SetStyleRange(CGXRange().SetRows(i), CGXStyle().SetTextColor(RED));
					}
				}
			}
		}
	}
}

void ShiftAWDlg::OnDestroy() 
{
	CDialog::OnDestroy();

	if (::IsWindow(pomParent->m_hWnd))
	{
		pomParent->pomShiftAWDlg = NULL;
	}

	delete this;
}

bool ShiftAWDlg::IsEmployeeGreyed(long lpStfUrno)
{
	bool blRet = false;

	STFDATA *prlStfData = ogStfData.GetStfByUrno(lpStfUrno);
	if (prlStfData != NULL)
	{
		CString olVpto = prlStfData->Dodm.Format("%Y%m%d");
		CString olVpfr = prlStfData->Doem.Format("%Y%m%d");
		if(olVpto == "") olVpto = "99999999";
		if(olVpfr == "") olVpfr = "00000000";

		if(!(olVpfr <= omVpto && olVpto >= omVpfr))
		{
			blRet = true;
		}
	}
	return blRet;
}
