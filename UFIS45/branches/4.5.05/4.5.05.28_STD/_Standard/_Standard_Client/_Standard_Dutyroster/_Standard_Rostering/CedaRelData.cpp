// CedaRelData.cpp: - Klasse f�r die Handhabung von REL-Daten (Daily Roster Groups - 
//  Tagesdienstplan Arbeitsgruppen)
//

// BDA 16.02.2000 erstellt aus CedaDrdData.cpp

#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessRelCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_REL_CHANGE, BC_REL_NEW und BC_REL_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaRelData::ProcessRelBc() der entsprechenden 
//	Instanz von CedaRelData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessRelCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaRelData *polRelData = (CedaRelData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polRelData->ProcessRelBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
// Implementation der Klasse CedaRelData
//************************************************************************************************************************************************


//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaRelData::CedaRelData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r REL-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(RELDATA, RelDataRecInfo)
		FIELD_OLEDATE	(Cdat,"CDAT") 	//	Datum der Erstellung
		FIELD_OLEDATE	(Lstu,"LSTU") 	//	Datum der letzten �nderung
		FIELD_CHAR_TRIM (Dors,"DORS")	//	Dutyroster or Shiftroster  ('D' / 'S', default = ' ')
		FIELD_CHAR_TRIM (Hopo,"HOPO") 	//	Home Airport
		FIELD_OLEDATE	(Rlat,"RLAT") 	//	Datum der erfolgten Freigabe durch den RELDPL
		FIELD_OLEDATE	(Rlfr,"RLFR") 	//	Freigabedatum von YYYYMMDD
		FIELD_OLEDATE	(Rlto,"RLTO")	//	Freigabedatum bis YYYYMMDD
		FIELD_CHAR_TRIM (Rosl,"ROSL") 	//	Freizugebende Planungsstufe
		FIELD_CHAR_TRIM (Sjob,"SJOB") 	//	Start Job  ('N' = now / 'L' = later (default))
		FIELD_LONG  	(Splu,"SPLU") 	//	Datensatz-Nr. des Schichtplanes (aus SPL)
		FIELD_LONG  	(Stfu,"STFU")	//	Datensatz-Nr. des Mitarbeiters (aus STF)
		FIELD_LONG  	(Urno,"URNO") 	//	Eindeutige Datensatz-Nr.
		FIELD_CHAR_TRIM (Usec,"USEC") 	//	Anwender (Ersteller)
		FIELD_CHAR_TRIM (Useu,"USEU") 	//	Anwender (letzte �nderung)
	END_CEDARECINFO
	
	// Infostruktur kopieren
	for (int i = 0; i < sizeof(RelDataRecInfo)/sizeof(RelDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RelDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	}
	
	// Tabellenname per Default auf REL setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);
	
	// Feldnamen initialisieren
	strcpy(pcmListOfFields,	"CDAT,LSTU,DORS,HOPO,RLAT,RLFR,RLTO,ROSL,SJOB,SPLU,STFU,URNO,USEC,USEU");

	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	// Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	Register();
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaRelData::~CedaRelData()
{
	TRACE("CedaRelData::~CedaRelData called\n");
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaRelData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaRelData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// REL-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_REL_CHANGE, "CedaRelData", "BC_REL_CHANGE",        ProcessRelCf);
	DdxRegister((void *)this,BC_REL_NEW,    "CedaRelData", "BC_REL_NEW",		   ProcessRelCf);
	DdxRegister((void *)this,BC_REL_DELETE, "CedaRelData", "BC_REL_DELETE",        ProcessRelCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaRelData::ClearAll(bool bpUnregister)
{
CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz abgemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadRelByUrno: liest den REL-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	true, wenn Datensatz erfolgreich gelesen, sonst false
//************************************************************************************************************************************************

bool CedaRelData::ReadRelByUrno(long lpUrno, RELDATA *prpRel)
{
CCS_TRY;
	CString olWhere;
	olWhere.Format("'%ld'", lpUrno);
	// Datensatz aus Datenbank lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	RELDATA *prlRel = new RELDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpRel) == true)
	{  
		// umschreiben::: TRACE("ReadRelByUrno: Datensatz gefunden,\nDrrn: %s\nFctc: %s\nHopo: %s\nSday: %s\nStfu: %d\nUrno: %d\nWgpc: %s\n",
			//prlRel->Drrn, prlRel->Fctc, prlRel->Hopo, prlRel->Sday, prlRel->Stfu, prlRel->Urno, prlRel->Wgpc);
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Kein REL gefunden - aufr�umen und terminieren
		delete prlRel;
		return false;
	}

	return false;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaRelData::Read(char *pspWhere, CMapPtrToPtr *popLoadStfUrnoMap, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY;
	// Return-Code f�r Funktionsaufrufe
	//bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{
		if (!CedaAction2("RT", "")) return false;
	}
	else
	{
		if (!CedaAction2("RT", pspWhere)) return false;
	}

    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		RELDATA *prlRel = new RELDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlRel);
		if(!blMoreRecords)
		{
			// kein weiterer Datensatz
			delete prlRel;
			break;
		}
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde ein Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlRel->Stfu, (void *&)prlVoid) == TRUE))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlRel, REL_NO_SEND_DDX)){
				// melden
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlRel);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				
				// Pointer l�schen
				delete prlRel;
				
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlRel);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			if(popLoadStfUrnoMap != NULL)
			{
				TRACE("Read-Rel: Stfu im UrnoMap nicht gefunden\n");
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlRel);
				}
			}
			
			delete prlRel;
		}
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}
	// Test: Anzahl der gelesenen REL
	TRACE("Read-Rel: %d gelesen\n",ilCountRecord);
	

    return true;
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************************************************
// ReadSpecialData: liest Datens�tze mit Hilfe eines �bergebenen SQL-Strings ein.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************************************************

bool CedaRelData::ReadSpecialData(CCSPtrArray<RELDATA> *popRelArray, char *pspWhere,
								  char *pspFieldList, char *pcpSort, bool ipSYS/*=true*/)
{
CCS_TRY;
	bool ilRc = true;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	if(popRelArray != NULL)	// Datens�tze in den �bergebenen Array
	{
		CString olFieldList(pspFieldList);
		if(olFieldList.IsEmpty())
			olFieldList = pcmListOfFields;	// GetBufferRecord kann leider mit "" nichts anfangen
			
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			RELDATA *prlRel = new RELDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prlRel,olFieldList)) == true)
			{
				// change::: TRACE("CedaRelData::ReadSpecialData(): REL gelesen, Drrn: %s, Fctc: %s, Hopo: %s, Sday: %s, Stfu: %d, Urno: %d, Wgpc: %s\n",
					//prlRel->Drrn, prlRel->Fctc, prlRel->Hopo, prlRel->Sday, prlRel->Stfu, prlRel->Urno, prlRel->Wgpc);
				popRelArray->Add(prlRel);
			}
			else
			{
				delete prlRel;
			}
		}
		if(popRelArray->GetSize() == 0) return false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpRel> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaRelData::Insert(RELDATA *prpRel, bool bpSave /*= true*/)
{
CCS_TRY;
	// �nderungs-Flag setzen
	prpRel->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpRel) == false) return false;
	// Broadcast REL_NEW abschicken
	InsertInternal(prpRel,true);
    return true;
CCS_CATCH_ALL;
return false;
}

/*********************************************************************************************
InsertInternal: 1. pr�fft, ob der neue Datensatz korrekt ist, u.a. auch ob schon ein Datensatz
mit gleichem prim�ren Schl�ssel "SDAY-DRRN-STFU" gespeichert ist, wenn ja - return FALSE 
(es kann nur einen geben), wenn nein
2. f�gt den internen Arrays einen neuen Datensatz hinzu. 
Der Datensatz muss fr�her oder sp�ter explizit durch Aufruf von Save() oder Release() in der Datenbank 
gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
R�ckgabe:	false	->	Fehler
			true	->	alles OK
*********************************************************************************************/

bool CedaRelData::InsertInternal(RELDATA *prpRel, bool bpSendDdx)
{
CCS_TRY;
	if(!IsValidRel(prpRel))
		return false;

	// Datensatz intern anf�gen
	omData.Add(prpRel);

	// Primary-Key dieses Datensatzes als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%ld",prpRel->Stfu);
//	TRACE("Speichere REL mit Schl�ssel = %s\n",olPrimaryKey);
	omKeyMap.SetAt(olPrimaryKey,prpRel);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpRel->Urno,prpRel);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,REL_NEW,(void *)prpRel);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpRel->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaRelData::Update(RELDATA *prpRel)
{
CCS_TRY;
	// Datensatz raussuchen
	if (GetRelByUrno(prpRel->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpRel->IsChanged == DATA_UNCHANGED)
		{
			prpRel->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpRel) == false) return false; 
		// Broadcast REL_CHANGE versenden
		UpdateInternal(prpRel);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaRelData::UpdateInternal(RELDATA *prpRel, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		ogDdx.DataChanged((void *)this,REL_CHANGE,(void *)prpRel);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaRelData::DeleteInternal(RELDATA *prpRel, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpRel->Urno);

	CString olKey;
	// Prim�rschl�ssel generieren
	olKey.Format("%ld", prpRel->Stfu);

	omKeyMap.RemoveKey(olKey);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpRel->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}

	// Broadcast senden
	// Wir brauchen keinen REL-Pointer mitzuschicken, viel sauberer und konsequenter ist, wenn
	// die Daten direkt aus dem Bestand gelesen werden. Gel�schter Datensatz wird damit nicht 
	// auftauchen.
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,REL_DELETE,(void *)0);
	}

CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle RELs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaRelData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/)
{
CCS_TRY;
	RELDATA *prlRel;
	// alle RELs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// REL dieses MAs entfernen?
		if(lpStfUrno == omData[i].Stfu)
		{
			// ja
			if ((prlRel =  GetRelByUrno(omData[i].Urno)) == NULL) return false;
			// REL entfernen
			DeleteInternal(prlRel, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpRel> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaRelData::Delete(RELDATA *prpRel, BOOL bpWithSave)
{
CCS_TRY;
	// Flag setzen
	prpRel->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpRel)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpRel,true);

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// ProcessRelBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaRelData::ProcessRelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
#ifdef _DEBUG
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaRelData::ProcessRelBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	RELDATA *prlRel = NULL;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_REL_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlRel = GetRelByUrno(llUrno);
			if(prlRel != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlRel,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlRel);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_REL_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlRel = new RELDATA;
			GetRecordFromItemList(prlRel,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlRel, REL_SEND_DDX);
		}
		break;
	case BC_REL_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlRel = GetRelByUrno(llUrno);
			if (prlRel != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlRel);
			}
		}
		break;
	default:
		break;
	}
#ifdef _DEBUG
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaRelData::ProcessRelBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG
CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetRelByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

RELDATA *CedaRelData::GetRelByUrno(long lpUrno)
{
CCS_TRY;
	// der Datensatz
	RELDATA *prpRel;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpRel) == TRUE)
	{
		return prpRel;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*********************************************************************************************
// GetRelByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
//	Schichttag (<opSday>), Schichtnummer (<lpDrrn>) und Mitarbeiter-
//	Urno (<lpStfu>).
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

RELDATA *CedaRelData::GetRelByKey(long lpStfu)
{
	// Puffer f�r den Prim�rschl�ssel
	CString olKey;
	// Prim�rschl�ssel generieren
	olKey.Format("%ld", lpStfu);
	//	TRACE("CedaRelData::GetRelByKey: Suche REL mit Key = %s:\n",olKey);
	// der Datensatz
	RELDATA *prlRel = NULL;
	// Datensatz suchen
	if (omKeyMap.Lookup(olKey,(void *&)prlRel) == TRUE)
	{
		// Datensatz gefunden -> Zeiger darauf zur�ckgeben
//		TRACE("CedaRelData::GetRelByKey: REL gefunden\n",olKey);
		return prlRel;
	}
	// Datensatz nicht gefunden -> R�ckgabe NULL
	return NULL;
}

/*********************************************************************************************
GetRelByPfcWithTime: sucht alle Datens�tze im angegebenen Zeitraum opEnd - opStart raus
opSortString bestimmt, wie die R�ckgabedaten sortiert werden sollen
R�ckgabe:	gef�llter popRelData und int Anzahl der gefundenen Datens�tze
*********************************************************************************************/
int CedaRelData::GetRelArrayByTime(COleDateTime opStart, COleDateTime opEnd, char* opSortString, CCSPtrArray<RELDATA> *popRelData)
{
CCS_TRY;

	if(!popRelData || opStart.GetStatus() != COleDateTime::valid|| opEnd.GetStatus() != COleDateTime::valid || opStart > opEnd)
		// Parameter ung�ltig
		return 0;

	popRelData->DeleteAll();

	// Where-Statement f�r REL-Abfrage: alle RELs, die f�r den 
	// ausgew�hlten Tag gelten sortiert nach Mitarbeiter-Funktion, 
	// Arbeitsgruppe und Schl�ssel
	
	CString olWhere;	// Puffer f�r WHERE-Clause
	if(opStart == opEnd)
	{
		olWhere.Format("WHERE SDAY = '%s'", opStart.Format("%Y%m%d"));
	}
	else
	{
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
	}

	ReadSpecialData(popRelData,(char*)(LPCTSTR)olWhere,"","STFU,SDAY",false);
	SortBySurnAndTime(popRelData);
	// Anzahl der gefundenen Datens�tze
	return popRelData->GetSize();

CCS_CATCH_ALL;
	return 0;
}

/*********************************************************************************************
IsValidRel: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten RELs
0. Die L�nge der Datenfelder pr�fen
1. Datenfelder pr�fen:
Drrn	mu� eine String mit Nummer > 0 sein
Fctc	ein von PFC-Codes
Hopo	mu� dem pcmHomeAirport entsprechen
Sday	Datumsangabe in der Form YYYYMMDD
Stfu	!= 0
Urno	!= 0
Wgpc	ein von WGP-Codes
2. pr�fen, da� es nur ein REL pro MA & DRRN & SDAY gibt
  R�ckgabe:	true	->	REL ist OK
			false	->	REL ist nicht OK
*********************************************************************************************/

bool CedaRelData::IsValidRel(RELDATA *popRel)
{
CCS_TRY;
	//EmptyLogString();

	if(!popRel)
	{
		return false;
	}

	/*
	0. Die L�nge der Datenfelder pr�fen
	1. Datenfelder pr�fen:
	Dors	'D', 'S' oder ''
	Rosl	ein char
	Sjob	'N', 'L', ''
	Stfu	!= 0
	Urno	!= 0
	*/
	if(	strlen(popRel->Dors) > REL_DORS_LEN ||
		strlen(popRel->Hopo) > HOPO_LEN ||
		strlen(popRel->Rosl) > ROSL_LEN ||
		strlen(popRel->Sjob) > REL_SJOB_LEN ||
		!popRel->Stfu					    || 
		!popRel->Urno						||
		strlen(popRel->Usec) > USEC_LEN ||
		strlen(popRel->Useu) > USEU_LEN)
	{
		ogRosteringLogText += "REL field length is wrong \n";
		TraceRelData(popRel);
		return false;
	}

	if(strcmp(popRel->Hopo,pcmHomeAirport))
	{
		ogRosteringLogText += "HOPO is wrong \n";
		TraceRelData(popRel);
		return false;
	}

	if(popRel->Rlfr.GetStatus() != COleDateTime::valid) 
	{
		ogRosteringLogText += "RLFR is not valid \n";
		TraceRelData(popRel);
		return false;
	}

	if(popRel->Rlto.GetStatus() != COleDateTime::valid) 
	{
		ogRosteringLogText += "RLTO is not valid \n";
		TraceRelData(popRel);
		return false;
	}

	if(strlen(popRel->Dors) > 0 && strcmp(popRel->Dors, "D") && strcmp(popRel->Dors, "S"))
	{
		ogRosteringLogText += "DORS is wrong \n";
		TraceRelData(popRel);
		return false;
	}

	if(strlen(popRel->Sjob) > 0 && strcmp(popRel->Sjob, "N") && strcmp(popRel->Sjob, "L"))
	{
		ogRosteringLogText += "SJOB is wrong \n";
		TraceRelData(popRel);
		return false;
	}

	// REL ist g�ltig
	return true;
CCS_CATCH_ALL;
return false;
}

void CedaRelData::TraceRelData(RELDATA* prpRel)
{
	GetDefectDataString(ogRosteringLogText, (void *)prpRel);

#ifdef _DEBUG
	TRACE("%s\n",(LPCTSTR)ogRosteringLogText);
	Sleep(5);
#endif _DEBUG
	WriteInRosteringLog(LOGFILE_TRACE);
}


//*******************************************************************************
// Save: speichert den Datensatz <prpRel>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaRelData::Save(RELDATA *prpRel)
{
CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpRel->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpRel->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpRel);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpRel->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpRel->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpRel);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpRel->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpRel->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaRelData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaRelData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,CCSCedaData::pcmReqId,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************
//************************************************************************************************************************
// die folgenden Funktionen sind Hilfsfunktionen zum Manipulieren von 
// Objekten des Typs RELDATA. Die Funktionen wurden als Member der Klasse 
// CedaRelData angelegt, um den Zugriff und die Manipulation von REL-Datens�tzen
// zu zentralisieren und in einer Klasse zu kapseln.
//************************************************************************************************************************
//************************************************************************************************************************

//************************************************************************************************************************************************
// CreateRelData: initialisiert eine Struktur vom Typ RELDATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

RELDATA* CedaRelData::CreateRelData(long lpStfUrno)
{
CCS_TRY;
	// Objekt erzeugen
	RELDATA *prlRelData = new RELDATA;
	// �nderungsflag setzen
	prlRelData->IsChanged = DATA_NEW;
	// Mitarbeiter-Urno
	prlRelData->Stfu = lpStfUrno;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlRelData->Urno = ogBasicData.GetNextUrno();
	// Hopo
	strcpy(prlRelData->Hopo,pcmHomeAirport);
	// Zeiger auf Datensatz zur�ck
	return prlRelData;
CCS_CATCH_ALL;
return NULL;
}

/**********************************************************************************************************
// RELDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
beliebige Pointer k�nnen NULL sein
**********************************************************************************************************/
RELDATA* CedaRelData::CreateRelData(char* ppHopo, long lpStfu)
{
	CCS_TRY;
	//Parameter testen
	if(	ppHopo != 0 && strlen(ppHopo) > HOPO_LEN) return 0;
	
	// Objekt erzeugen
	RELDATA *prlRelData = new RELDATA;
	if(!prlRelData) return 0;
	// �nderungsflag setzen
	prlRelData->IsChanged = DATA_NEW;
	if(	ppHopo != 0 )
		strcpy(prlRelData->Hopo,ppHopo);
	
	prlRelData->Stfu = lpStfu;
	// Zeiger auf Datensatz zur�ck
	return prlRelData;
	CCS_CATCH_ALL;
	return NULL;
}


//************************************************************************************************************************************************
// CompareRelToRel: zwei RELs miteinander vergleichen. Wenn <bpCompareKey> gesetzt ist,
//	werden auch die Felder, die den Schl�ssel ergeben verglichen. 
// R�ckgabe:	false	->	die Werte der Felder beider RELs sind gleich
//				true	->	die Werte der Felder beider RELs sind unterschiedlich
//************************************************************************************************************************************************

bool CedaRelData::CompareRelToRel(RELDATA *popRel1, RELDATA *popRel2,bool bpCompareKey /*= false*/)
{
CCS_TRY;
	// Schl�sselfelder vergleichen, wenn gew�nscht
	if (bpCompareKey && ((popRel1->Stfu != popRel2->Stfu))) 
	{
		return true;
	}
	// andere Felder vergleichen
	if (strcmp(popRel1->Hopo,popRel2->Hopo) != 0)
	{
		return true;
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// CopyRel: kopiert alles, ausser Urno
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaRelData::CopyRel(RELDATA* popRelDataSource,RELDATA* popRelDataTarget)
{
	// Daten kopieren
	strcpy(popRelDataTarget->Hopo,popRelDataSource->Hopo);
	popRelDataTarget->Stfu = popRelDataSource->Stfu;
}

/************************************************************************************************************************************************
Sortieren 
************************************************************************************************************************************************/

static int CompareRelBySurnAndTime( const RELDATA **e1, const RELDATA **e2);

/*****************************************************************************
*****************************************************************************/
void CedaRelData::SortBySurnAndTime(CCSPtrArray<RELDATA> *popRelArray)
{
	popRelArray->Sort(CompareRelBySurnAndTime);
}

//****************************************************************************
// CompareGroupByTimeFunc: vergleicht zwei Arbeitsgruppen nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

static int CompareRelBySurnAndTime( const RELDATA **e1, const RELDATA **e2)
{
	if((**e1).Stfu > (**e2).Stfu)
		return 1;
	else if((**e1).Stfu < (**e2).Stfu)
		return -1;
	return 0;
	//return strcmp((**e1).Sday, (**e2).Sday);
}

