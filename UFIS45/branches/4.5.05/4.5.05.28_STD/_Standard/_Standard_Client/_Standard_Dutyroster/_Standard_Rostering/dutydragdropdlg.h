#ifndef AFX_DUTYDRAGDROPDLG_H__30ECF2C1_BA82_11D2_8E63_0000C002916B__INCLUDED_
#define AFX_DUTYDRAGDROPDLG_H__30ECF2C1_BA82_11D2_8E63_0000C002916B__INCLUDED_

// DutyDragDropDlg.h : Header-Datei
//
#include <stdafx.h>
/*#include "resource.h"
#include <DutyRoster_View.h>
#include <CCSDynTable.h>
#include <CedaBasicData.h>
#include <CCSDragDropCtrl.h>
#include <CViewer.h>
#include <Groups.h>*/

class DutyDragDropTabViewer;

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyDragDropDlg 

class DutyDragDropDlg : public CDialog
{
// Konstruktion
public:
	DutyDragDropDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyDragDropDlg();

private:
	DutyRoster_View *pomParent;

// Dialogfelddaten
	CViewer *pomViewer;
	CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
	CRect omSystemRect;
	CRect omWindowRect;

	//>> DragDrop Table
	UINT imDragDropTableID;
	CCSDynTable *pomDragDropTable;
	CCSEDIT_ATTRIB *prmDragDropIPEAttrib;
	DutyDragDropTabViewer *pomDragDropTabViewer;
	CViewer *pomCurrentDragDropViewer;

// Dialogfelddaten
	//{{AFX_DATA(DutyDragDropDlg)
	enum { IDD = IDD_DUTY_DRAGDROP_DLG };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(DutyDragDropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyDragDropDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnSizing(UINT nSide, LPRECT lpRect );
    afx_msg LONG VMoveTable(UINT wParam, LONG lParam);
    afx_msg LONG VertTableScroll(UINT wParam, LONG lParam);
    afx_msg LONG TableLButtonDown(UINT wParam, LONG lParam);
	afx_msg LONG OnDragBegin(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:

	CCSPtrArray<STFDATA> *pomStfData;
	//CCSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;
	VIEWINFO *prmViewInfo;
	CCSDragDropCtrl omDragDropObject;

	void InternalCancel();
	void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYDRAGDROPDLG_H__30ECF2C1_BA82_11D2_8E63_0000C002916B__INCLUDED_
