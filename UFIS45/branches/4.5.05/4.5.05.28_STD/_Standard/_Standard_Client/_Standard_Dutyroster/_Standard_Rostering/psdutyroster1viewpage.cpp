// PSDutyRoster1ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSDutyRoster1ViewPage 

IMPLEMENT_DYNCREATE(PSDutyRoster1ViewPage, RosterViewPage)

PSDutyRoster1ViewPage::PSDutyRoster1ViewPage() : RosterViewPage(PSDutyRoster1ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSDutyRoster1ViewPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	omSelACTURNO="NULL";
	omSelALTURNO="NULL";
	omTitleStrg = LoadStg(IDS_W_Laden);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSDutyRoster1ViewPage::~PSDutyRoster1ViewPage()
{
}

void PSDutyRoster1ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSDutyRoster1ViewPage)
	DDX_Control(pDX, IDC_SELECT_MAINFUNCONLY, m_SelectMainFuncOnly);
	DDX_Control(pDX, IDC_SELECT_ALLFUNC, m_SelectAllFunc);
	DDX_Control(pDX, IDC_LB_ORGANISATIONS, m_LB_Orgs);
	DDX_Control(pDX, IDC_LB_FUNCTIONS,	m_LB_Functions);
	DDX_Control(pDX, IDC_DAYFROM,	m_DayFrom);
	DDX_Control(pDX, IDC_DAYTO,		m_DayTo);
	DDX_Control(pDX, IDC_P_DAYS,	m_E_PDays);
	DDX_Control(pDX, IDC_M_DAYS,	m_E_MDays);
	DDX_Control(pDX, IDC_PERI_FIX,	m_R_Fix);
	DDX_Control(pDX, IDC_PERI_VAR,	m_R_Var );
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		// Daten schreiben
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Daten lesen
		GetData();
	}
}


BEGIN_MESSAGE_MAP(PSDutyRoster1ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSDutyRoster1ViewPage)
	ON_BN_CLICKED(IDC_PERI_FIX, OnFix)
	ON_BN_CLICKED(IDC_PERI_VAR, OnVar)
	ON_BN_CLICKED(IDC_MARK_ALL_FUNCTIONS, OnMarkAllFunctions)
	ON_BN_CLICKED(IDC_ALL_ORGS, OnAllOrgs)
	ON_BN_CLICKED(IDC_MARK_NO_FUNCTIONS, OnNoFunctions)
	ON_BN_CLICKED(IDC_NO_ORGS, OnNoOrgs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSDutyRoster1ViewPage 
//---------------------------------------------------------------------------

//********************************************************************************************************************
// SetCalledFrom: stellt den Titel ein.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//********************************************************************************************************************
// OnInitDialog: initialisiert die Proppage.
// R�ckgabe:	false	-> Focus auf Control
//				sonst true
//********************************************************************************************************************

BOOL PSDutyRoster1ViewPage::OnInitDialog() 
{
	//Nur auf der Ersten Seite wird OnInitDialog vor SetData aufgerufen !!!!!!!!!
	RosterViewPage::OnInitDialog();

	m_DayFrom.SetTypeToDate(true);
	m_DayFrom.SetTextErrColor(RED);

	m_DayTo.SetTypeToDate(true);
	m_DayTo.SetTextErrColor(RED);

	m_E_PDays.SetTypeToInt(0,99);
	m_E_PDays.SetTextErrColor(RED);

	m_E_MDays.SetTypeToInt(0,99);
	m_E_MDays.SetTextErrColor(RED);

	CString olNoCode5 = "-----";
	CString olNoCode8 = "--------";
	////////// Funktions-Listbox mit Funktionen f�llen
	CString olLine;
	// Inhalt l�schen
	m_LB_Functions.ResetContent();
	// Proportionalschrift einstellen um Tabulator-Effekt zu bekommen
	m_LB_Functions.SetFont(&ogCourier_Regular_8);
	// Anzahl der Funktionen aus globalem Datenhaltungsobjekt
	int ilPfcCount =  ogPfcData.omData.GetSize();
	for(int iPfc=0; iPfc<ilPfcCount; iPfc++)
	{
		// alle Funktionen hinzuf�gen. Am Ende des Eintrages wird die 
		// Urno versteckt
		olLine.Format("%-8s   %-40s     URNO=%d",ogPfcData.omData[iPfc].Fctc, ogPfcData.omData[iPfc].Fctn, ogPfcData.omData[iPfc].Urno);
		m_LB_Functions.AddString(olLine);
	}
	// Eine leeren Zeile einf�gen, f�r alle MA's die keine Funktion haben
	olLine.Format("%-8s   %-40s     URNO=NO_PFCCODE",olNoCode5, LoadStg(IDS_STRING1878));
	m_LB_Functions.AddString(olLine);
	////////// Ende Funktions-Listbox mit Funktionen f�llen

	////////// Organisations-Listbox mit Funktionen f�llen
	// Inhalt l�schen
	m_LB_Orgs.ResetContent();
	// Proportionalschrift einstellen um Tabulator-Effekt zu bekommen
	m_LB_Orgs.SetFont(&ogCourier_Regular_8);
	// Anzahl der Organisationen aus globalem Datenhaltungsobjekt
	int ilOrgCount =  ogOrgData.omData.GetSize();
	for(int iOrg=0; iOrg<ilOrgCount; iOrg++)
	{
		// alle Organisationen hinzuf�gen. Am Ende des Eintrages wird die 
		// Urno versteckt
		olLine.Format("%-8s   %-40s     URNO=%d",ogOrgData.omData[iOrg].Dpt1, ogOrgData.omData[iOrg].Dptn, ogOrgData.omData[iOrg].Urno);
		m_LB_Orgs.AddString(olLine);
	}
	// Eine leeren Zeile einf�gen, f�r alle MA's die keine Organisationseinheit haben
	olLine.Format("%-8s   %-40s     URNO=NO_ORGCODE",olNoCode8, LoadStg(IDS_STRING1879));
	m_LB_Orgs.AddString(olLine);
	////////// Ende Organisations-Listbox mit Funktionen f�llen

	SetStatic();
	SetButtonText();

	return TRUE;
}

//********************************************************************************************************************
// GetData: liest die Daten aus der Oberfl�che ein und speichert 
//	die Werte im StringArray <omValues>.
// R�ckgabe:	false	-> Fehler beim Lesen
//				sonst true
//********************************************************************************************************************

BOOL PSDutyRoster1ViewPage::GetData()
{
	omValues.RemoveAll();
	if(m_R_Fix.GetCheck() == 1)
		omValues.Add("FIX");		//----------------------------0000
	else
		omValues.Add("VARIABLE");	//----------------------------0000
	CString olDay = "0-0";
	bool ilStatus = true;

	CString olDayFrom,olDayTo;
	CTime olTmpTimeFr,olTmpTimeTo;

	if(m_DayFrom.GetStatus() == false && m_DayTo.GetStatus() == false)
	{
		ilStatus = false;
	}
	else if(m_DayFrom.GetStatus() == false && m_DayTo.GetStatus() == true)
	{
		m_DayTo.GetWindowText(olDayTo);
		olTmpTimeTo = DateStringToDate(olDayTo);
		olDayFrom.Format("%d.%s", 1, olTmpTimeTo.Format("%m.%Y"));
		m_DayFrom.SetInitText(olDayFrom);
	}
	else if(m_DayFrom.GetStatus() == true && m_DayTo.GetStatus() == false)
	{
		m_DayFrom.GetWindowText(olDayFrom);
		olTmpTimeFr = DateStringToDate(olDayFrom);
		COleDateTime olOleTime = CTimeToCOleDateTime(olTmpTimeFr);
		int ilDays = CedaDataHelper::GetDaysOfMonth(olOleTime);
		olDayTo.Format("%d.%s",ilDays, olTmpTimeFr.Format("%m.%Y"));
		m_DayTo.SetInitText(olDayTo);
	}

	if (ilStatus)
	{
		m_DayFrom.GetWindowText(olDayFrom);
		m_DayTo.GetWindowText(olDayTo);

		olTmpTimeFr =  DateStringToDate(olDayFrom);
		olTmpTimeTo =  DateStringToDate(olDayTo);

		if (olTmpTimeTo != -1 && olTmpTimeFr != -1)
		{
			if (olTmpTimeFr > olTmpTimeTo)
			{
				// auf Monatsende abschneiden der 'Zeit von' abschneiden
				COleDateTime olOleTime = CTimeToCOleDateTime(olTmpTimeFr);
				int ilDays = CedaDataHelper::GetDaysOfMonth(olOleTime);
				olDay.Format("%s-%s%d",olTmpTimeFr.Format("%Y%m%d"),olTmpTimeFr.Format("%Y%m"),ilDays);
				ilStatus = false;
			}
			else
			{
				if (olTmpTimeFr.Format("%Y%m") == olTmpTimeTo.Format("%Y%m") || bgEnableMonthlyAllowance)
				{
					olDay.Format("%s-%s",olTmpTimeFr.Format("%Y%m%d"),olTmpTimeTo.Format("%Y%m%d"));
				}
				else
				{
					MessageBox(LoadStg(IDS_STRING123), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
					// auf Monatsende abschneiden
					COleDateTime olOleTime = CTimeToCOleDateTime(olTmpTimeFr);
					int ilDays = CedaDataHelper::GetDaysOfMonth(olOleTime);
					olDay.Format("%s-%s%d",olTmpTimeFr.Format("%Y%m%d"),olTmpTimeFr.Format("%Y%m"),ilDays);
					ilStatus = false;
				}
			}
		}
		else
		{
			ilStatus = false;
		}
	}
	omValues.Add(olDay);			//----------------------------1111
	CString olDays = "00-00";
	CString olTmp;
	int ilPlus = 0;
	int ilMinus = 0;

	if(m_E_PDays.GetStatus() == true && m_E_PDays.GetWindowTextLength() > 0)
	{
		m_E_PDays.GetWindowText(olTmp);
		ilPlus = atoi(olTmp);
	}
	if(m_E_MDays.GetStatus() == true && m_E_MDays.GetWindowTextLength() > 0)
	{
		m_E_MDays.GetWindowText(olTmp);
		ilMinus = atoi(olTmp);
	}
	olDays.Format("%02d-%02d", ilPlus, ilMinus);
	omValues.Add(olDays);			//----------------------------2222
	
	////////// selektierte Funktionen ermitteln
	int ilMaxItems;
	// Anzahl der selektierten Funktionen
	ilMaxItems = m_LB_Functions.GetSelCount();
	// Array, der die Indizes der selektierten Funktionen h�lt
	int *pilFuncIndex = new int [ilMaxItems];
	// Indizes ermitteln
	m_LB_Functions.GetSelItems(ilMaxItems, pilFuncIndex );
	CString olTmpTxt, olUrno, olUrnos;
	// alle selektierten Funktionen durchgehen
	for(int i=0; i<ilMaxItems; i++)
	{
		// Eintrag ermitteln
		m_LB_Functions.GetText(pilFuncIndex[i],olTmpTxt);
		// Urno ermitteln (unsichtbar f�r Benutzer am Ende der Textzeile)
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		// Trennzeichen hinzuf�gen, wenn nicht letztes Element
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	// aufr�umen
	delete pilFuncIndex;
	// Urno-Sequenz speichern
	if (olUrnos.GetLength() > 0)
	{
		omValues.Add(olUrnos);		//----------------------------3333
	}
	else
	{
		omValues.Add("NULL");			//----------------------------3333
		//opValues[opValues.GetSize()-1] = "";
	}
	////////// Ende selektierte Funktionen ermitteln

	////////// selektierte Organisationen ermitteln
	olTmpTxt.Empty();
	olUrno.Empty();
	olUrnos.Empty();
	// Anzahl der selektierten Organisationen
	ilMaxItems = m_LB_Orgs.GetSelCount();
	// Array, der die Indizes der selektierten Funktionen h�lt
	int *pilOrgIndex = new int [ilMaxItems];
	// Indizes ermitteln
	m_LB_Orgs.GetSelItems(ilMaxItems, pilOrgIndex);
	// alle selektierten Funktionen durchgehen
	for(i=0; i<ilMaxItems; i++)
	{
		// Eintrag ermitteln
		m_LB_Orgs.GetText(pilOrgIndex[i],olTmpTxt);
		// Urno ermitteln (unsichtbar f�r Benutzer am Ende der Textzeile)
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		// Trennzeichen hinzuf�gen, wenn nicht letztes Element
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	// aufr�umen
	delete pilOrgIndex;
	// Urno-Sequenz speichern
	if (olUrnos.GetLength() > 0)
	{
		omValues.Add(olUrnos);		//----------------------------4444
	}
	else
	{
		omValues.Add("NULL");			//----------------------------4444
		//opValues[opValues.GetSize()-1] = "";
	}
	////////// Ende selektierte Organisationen ermitteln

	if(omSelACTURNO.GetLength())
	{
		omValues.Add(omSelACTURNO);		//----------------------------5555
	}
	else
	{
		omValues.Add("NULL");		//----------------------------5555
	}

	if(omSelALTURNO.GetLength())
	{
		omValues.Add(omSelALTURNO);		//----------------------------6666
	}
	else
	{
		omValues.Add("NULL");		//----------------------------6666
	}


	if(m_SelectAllFunc.GetCheck() == 1)
		omValues.Add("1");			//----------------------------7777
	else
		omValues.Add("0");			//----------------------------7777
	
	return TRUE;
}

//********************************************************************************************************************
// SetData: schreibt die Daten aus <omValues> in die Oberfl�chenelemente.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::SetData()
{
	//*** Set all adjustments to default ************************************
	m_LB_Functions.SetSel(-1,false);
	m_LB_Orgs.SetSel(-1,false);
	
	m_DayFrom.SetInitText("");
	m_DayTo.SetInitText("");
	m_E_PDays.SetInitText("0");
	m_E_MDays.SetInitText("0");
	//*** end> Set all adjustments to default *******************************
	
	// Werte auslesen
	for(int ilValue = 0; ilValue < omValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
		default:
			break;
		case 0:
			if(omValues[0] == "FIX")
			{
				m_R_Fix.SetCheck(1);
				m_R_Var.SetCheck(0);
				OnFix();
			}
			else
			{
				m_R_Fix.SetCheck(0);
				m_R_Var.SetCheck(1);
				OnVar();
			}
			break;
		case 1:
			if(omValues[1].IsEmpty() == FALSE)
			{
				//*** Set Date
				CString olDay,olDayFrom, olDayTo;
				
				olDay = omValues[1];
				
				if(olDay != "0-0")
				{
					olDayFrom = olDay.Left(8);
					olDayTo = olDay.Right(8);
					
					olDayFrom.Format("%s.%s.%s",olDayFrom.Right(2),olDayFrom.Mid(4,2),olDayFrom.Left(4));
					olDayTo.Format("%s.%s.%s",olDayTo.Right(2),olDayTo.Mid(4,2),olDayTo.Left(4));
					
					m_DayFrom.SetInitText(olDayFrom);
					m_DayTo.SetInitText(olDayTo);
				}
				//*** end> Set Date
			}
			break;
		case 2:
			if(omValues[2].IsEmpty() == FALSE)
			{
				CString olTmp;
				int ilPlus  = 0;
				int ilMinus = 0;
				if(omValues[2] == "00-00" || omValues[2].GetLength() < 5)
				{
					ilPlus  = 0;
					ilMinus = 0;
				}
				else
				{
					ilPlus  = atoi(omValues[2].Mid(0,2));
					ilMinus = atoi(omValues[2].Mid(3,2));
				}
				olTmp.Format("%d", ilPlus);
				m_E_PDays.SetInitText(olTmp);
				
				olTmp.Format("%d", ilMinus);
				m_E_MDays.SetInitText(olTmp);
			}
			break;
		case 3:
			if(omValues[3].IsEmpty() == FALSE)
			{
				////////// selektierte Funktionen ermitteln
				CString olTmpTxt, olUrno;
				// Anzahl der geladenen Funktionen
				int ilCount = m_LB_Functions.GetCount();
				// alle Funktionen durchgehen
				for(int i = 0;i<ilCount;i++)
				{
					// Eintragszeile lesen
					m_LB_Functions.GetText(i,olTmpTxt);
					// Urno ermitteln
					olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
					// Urno in Stringliste?
					if(	omValues[3].Find(olUrno) != -1)
						m_LB_Functions.SetSel(i,TRUE);	// ja -> Funktion selektieren
				}
				////////// Ende selektierte Funktionen ermitteln
			}
			
			// Ist der Gruppenansicht aktiviert und eine Planungsgruppe selektiert? 
			// - Dann m�ssen alle dazugeh�rige Funktionen selektiert werden
			//RestorePlanGroupFuncSelection();
			break;
		case 4:
			if(omValues[4].IsEmpty() == FALSE)
			{
				////////// selektierte Organsisationen ermitteln
				CString olTmpTxt, olUrno;
				// Anzahl der geladenen Organisationen
				int ilCount = m_LB_Orgs.GetCount();
				// alle Organisationen durchgehen
				for(int i = 0;i<ilCount;i++)
				{
					// Eintragszeile lesen
					m_LB_Orgs.GetText(i,olTmpTxt);
					// Urno ermitteln
					olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
					// Urno in Stringliste?
					if(omValues[4].Find(olUrno) != -1)
						m_LB_Orgs.SetSel(i,TRUE);	// ja -> Organisationen selektieren
				}
				////////// Ende selektierte Organisationen ermitteln
			}
			break;
		case 5:
			if(omValues[5].GetLength())
			{
				////////// selektierte ACT Urno ermitteln
				omSelACTURNO = omValues[5];
			}
			else
			{
				omSelACTURNO = "NULL";
			}
			break;
		case 6:
			if(omValues[6].GetLength())
			{
				////////// selektierte ALT Urno ermitteln
				omSelALTURNO = omValues[6];
			}
			else
			{
				omSelALTURNO = "NULL";
			}
			break;
		case 7:
			if(omValues[7] == "1")
			{
				// Nach allen Funktionen selektieren
				m_SelectMainFuncOnly.SetCheck(0);
				m_SelectAllFunc.SetCheck(1);
			}
			else
			{
				// Nur nach Stammfunktion selektieren
				m_SelectMainFuncOnly.SetCheck(1);
				m_SelectAllFunc.SetCheck(0);
			}
			break;
		}
	}
}

//********************************************************************************************************************
// OnFix: stellt die Oberfl�che f�r die Angabe eines festen
//	Zeitraumes f�r die Ansicht ein.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnFix() 
{
	// Datumsfelder von/bis verf�gbar machen
	m_DayFrom.EnableWindow(TRUE);
	m_DayTo.EnableWindow(TRUE);
	// Felder f�r die Angabe +/- Tage abschalten
	m_E_PDays.EnableWindow(FALSE);
	m_E_MDays.EnableWindow(FALSE);
}

//********************************************************************************************************************
// OnVar: stellt die Oberfl�che f�r die Angabe eines variablen
//	Zeitraumes f�r die Ansicht ein.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnVar() 
{
	// Datumsfelder von/bis abschalten
	m_DayFrom.EnableWindow(FALSE);
	m_DayTo.EnableWindow(FALSE);
	// Felder f�r die Angabe +/- Tage verf�gbar machen
	m_E_PDays.EnableWindow(TRUE);
	m_E_MDays.EnableWindow(TRUE);
}

//********************************************************************************************************************
// OnMarkAllFunctions: w�hlt alle Funktionen in der Listbox aus.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnMarkAllFunctions() 
{
	int ilItems;
	// Anzahl der Eintr�ge
	ilItems = m_LB_Functions.GetCount();
	// jeden Eintrag selektieren
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Functions.SetSel(i,TRUE);
	}
}

//********************************************************************************************************************
// OnNoFunctions: deselektiert alle Funktionen in der Listbox
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnNoFunctions() 
{
	int ilItems;
	// Anzahl der Eintr�ge
	ilItems = m_LB_Functions.GetCount();
	// jeden Eintrag deselektieren
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Functions.SetSel(i,FALSE);
	}
}

//********************************************************************************************************************
// OnAllOrgs: w�hlt alle Organisationen in der Listbox aus.
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnAllOrgs() 
{
	int ilItems;
	// Anzahl der Eintr�ge
	ilItems = m_LB_Orgs.GetCount();
	// jeden Eintrag selektieren
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Orgs.SetSel(i,TRUE);
	}
}

//********************************************************************************************************************
// OnNoOrgs: deselektiert alle Organisationen in der Listbox
// R�ckgabe:	keine
//********************************************************************************************************************

void PSDutyRoster1ViewPage::OnNoOrgs() 
{
	int ilItems;
	// Anzahl der Eintr�ge
	ilItems = m_LB_Orgs.GetCount();
	// jeden Eintrag deselektieren
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Orgs.SetSel(i,FALSE);
	}
}

void PSDutyRoster1ViewPage::SetStatic()
{
	SetDlgItemText(IDC_S_Zeitraum,LoadStg(IDC_S_Zeitraum));
	if (bgEnableMonthlyAllowance == FALSE)
	{
		// show the label "max. 1 month"
		SetDlgItemText(IDC_S_maxMon,LoadStg(IDC_S_maxMon));
	}
	else
	{
		// hide it
		GetDlgItem(IDC_S_maxMon)->ShowWindow(SW_HIDE);
	}
	SetDlgItemText(IDC_S_aktDay,LoadStg(IDC_S_aktDay));
	SetDlgItemText(IDC_S_tage1,LoadStg(IDC_S_tage1));
	SetDlgItemText(IDC_S_tage2,LoadStg(IDC_S_tage1));
	SetDlgItemText(IDC_S_Fct,LoadStg(IDC_S_Funktion));
	SetDlgItemText(IDC_S_Org,LoadStg(IDC_S_Org));
}

void PSDutyRoster1ViewPage::SetButtonText()
{
	SetDlgItemText(IDC_SELECT_MAINFUNCONLY,LoadStg(IDS_STRING172));	// Nur Stammfunktion*INTXT*
	SetDlgItemText(IDC_SELECT_ALLFUNC,LoadStg(IDS_STRING173));		// Alle zugewiesenen Funktionen*INTXT*

	SetDlgItemText(IDC_PERI_FIX,LoadStg(IDC_PERI_FIX));
	SetDlgItemText(IDC_PERI_VAR,LoadStg(IDC_PERI_VAR));

	SetDlgItemText(IDC_ALL_ORGS,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NO_ORGS,LoadStg(IDS_S_NoOne));
	SetDlgItemText(IDC_MARK_ALL_FUNCTIONS,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_MARK_NO_FUNCTIONS,LoadStg(IDS_S_NoOne));
}


//****************************************************************************
// 
//****************************************************************************

void PSDutyRoster1ViewPage::SetPValues4ViewPage(CStringArray* popValues4ViewPage)
{
	pomValues4ViewPage = popValues4ViewPage;
}

/************************************************************************************
Return: 
- 0, wenn kein Gruppenansicht selektiert
- Pointer zu CString mit der Liste aller Funktionen (FCTCs) aus der selektierten
Planungsgruppe
ACHTUNG! Der Pointer mu� danach gel�scht werden
************************************************************************************/
CString* PSDutyRoster1ViewPage::GetSelectedPlanGroupFunctions()
{
	CString olStrParam;
	if(!GetSafeStringFromArray(*pomValues4ViewPage, 1, olStrParam) || (olStrParam != "GRUPP"))
		return 0;
	// Gruppierung ist aktiviert
	
	// Urno der Planungsgruppe aus n�chstem Parameter lesen
	GetSafeStringFromArray(*pomValues4ViewPage,2,olStrParam);

	// Array mit Funktionen dieser Planungsgruppe erstellen
	// Datensatz der Planungsgruppe lesen
	PGPDATA *prlPgp = ogPgpData.GetPgpByUrno(atol(olStrParam));
	
	// Datensatz gefunden?
	if(prlPgp != NULL)
	{
		//Zur Zeit nur Funktionen (PFC)
		// ja -> Typ 'Funktionen'?
		if(CString(prlPgp->Type) == CString("PFC"))
		{
			// ja -> 
			CString* polRet = new CString(prlPgp->Pgpm);
			*polRet = "|" + *polRet + "|";		// es erleichtert sp�ter das Suchen von Codes im String
			// z.B. *polRet = "|USSL|USSUP|USAGT|"
			return polRet;
		}
	}
	return 0;
}

/*************************************************************************************************
Im Gruppenansicht m�ssen alle zu dieser Planungsgruppe geh�rende Funktionen selektiert werden,
das pr�fen wir und erzwingen die Selektion
*************************************************************************************************/
void PSDutyRoster1ViewPage::RestorePlanGroupFuncSelection()
{
	CString* polFctcPGSelected = GetSelectedPlanGroupFunctions();
	
	if(!polFctcPGSelected)
		return;			// nichts zu tun - kein Gruppenansicht
	
	// selektierte Funktionen ermitteln und kontrollieren, 
	// bei Bedarf Selektion aufzwingen
	
	CString olFctc;
	// Anzahl der geladenen Funktionen
	int ilCount = m_LB_Functions.GetCount();
	// alle Funktionen durchgehen
	for(int i = 0;i<ilCount;i++)
	{
		if(m_LB_Functions.GetSel(i) > 0)	// die Funktion ist bereits selektiert, nichts dagegen
			continue;
		
		// Eintragszeile lesen
		m_LB_Functions.GetText(i,olFctc);
		// Fctc ermitteln
		olFctc = "|" + olFctc.Left(5);
		olFctc.TrimRight(' ');
		olFctc += "|";
		// Fctc in Stringliste?
		if(polFctcPGSelected->Find(olFctc) != -1)
			m_LB_Functions.SetSel(i,TRUE);	// Diese Funktion geh�rt zu der 
		// aktivierten Planungsgruppe, sie darf nicht reselektiert werden
	}
	delete polFctcPGSelected;
}

