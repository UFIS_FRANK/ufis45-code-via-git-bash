// RosterViewPage.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RosterViewPage property page

IMPLEMENT_DYNCREATE(RosterViewPage, CPropertyPage)

RosterViewPage::RosterViewPage() : CPropertyPage(RosterViewPage::IDD)
{
	//{{AFX_DATA_INIT(RosterViewPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

RosterViewPage::RosterViewPage(UINT nIDTemplate, UINT nIDCaption) : CPropertyPage(nIDTemplate, nIDCaption)
{
}

RosterViewPage::RosterViewPage(LPCTSTR lpszTemplateName, UINT nIDCaption) : CPropertyPage(lpszTemplateName, nIDCaption)
{
}

RosterViewPage::~RosterViewPage()
{
}

void RosterViewPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RosterViewPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RosterViewPage, CPropertyPage)
	//{{AFX_MSG_MAP(RosterViewPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RosterViewPage message handlers




//****************************************************************************
// GetSafeStringFromArray: ermittelt den Wert <opValues[<ipIndex>]> und
//	speichert ihn in <opStrParam>. Der Index wird auf G�ltigkeit �berpr�ft.
//	wenn der Index ung�ltig ist, wird <opStrParam> = "" gesetzt.
//	R�ckgabe:	true	-> g�ltiger Index
//				false	-> ung�ltiger Index, Wert ist Leerstring
//****************************************************************************

bool RosterViewPage::GetSafeStringFromArray(CStringArray &opValues, int ipIndex, CString &opStrParam)
{
CCS_TRY;
	// Index ung�ltig?
	if (opValues.GetSize() <= ipIndex){
		// ja -> String ist leer
		opStrParam = "";
		return false;
	}

	// String ermitteln
	opStrParam = opValues[ipIndex];
	return true;
CCS_CATCH_ALL;
return false;
}

