// CedaScoData.h

#ifndef __CEDAPSCODATA__
#define __CEDAPSCODATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SCODATA 
{
	long			Urno;			// Datensatz-Urno
	long			Surn;			// Mitarbeiter-Urno
	char			Code[7];		// Contract Code
	COleDateTime	Vpfr;
	COleDateTime	Vpto;
	char			Cweh[6];


	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SCODATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SCODataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaScoData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SCODATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaScoData();
	~CedaScoData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SCODATA *prpSco);
	bool InsertInternal(SCODATA *prpSco);
	bool Update(SCODATA *prpSco);
	bool UpdateInternal(SCODATA *prpSco);
	bool Delete(long lpUrno);
	bool DeleteInternal(SCODATA *prpSco);
	bool ReadSpecialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SCODATA *prpSco);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SCODATA *GetScoByUrno(long lpUrno);
	int GetScoListBySurnWithTime(long lpSurn,COleDateTime opTime,CCSPtrArray<SCODATA> *popScoData);
	int GetScoListBySurnWithTime(long lpSurn,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SCODATA> *popScoData);
	CString GetCotAndCWEHBySurnWithTime(long lpSurn,COleDateTime opDate, CString* popWeeklyHour=0);
	long GetScoUrnoWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData);
	bool IsValidSco(SCODATA* popSco);
private:
	int FindFirstOfSurn(long lpSurn);
};

//---------------------------------------------------------------------------------------------------------

extern CedaScoData ogScoData;

#endif //__CEDAPSCODATA__
