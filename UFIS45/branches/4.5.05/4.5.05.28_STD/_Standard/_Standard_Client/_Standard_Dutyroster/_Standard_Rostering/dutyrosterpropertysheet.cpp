// DutyRosterPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// DutyRosterPropertySheet
//

DutyRosterPropertySheet::DutyRosterPropertySheet(CString opCalledFrom, CWnd* pParentWnd,CViewer *popViewer, CAccounts *popAccounts, UINT iSelectPage)
						: BasePropertySheet(opCalledFrom, pParentWnd, popViewer, iSelectPage)
{
	omCalledFrom = opCalledFrom;
	pomAccounts = popAccounts;
	
	AddPage(&omDutyRoster1View);
	omDutyRoster1View.SetCalledFrom(omCalledFrom);

	AddPage(&omDutyRoster2View);
	omDutyRoster2View.SetCalledFrom(omCalledFrom);
	
	AddPage(&omDutyRoster3View);
	omDutyRoster3View.SetCalledFrom(omCalledFrom);
	
	AddPage(&omDutyRoster4View);
	omDutyRoster4View.SetCalledFrom(omCalledFrom);
	
	omDutyRoster1View.SetPValues4ViewPage(&omDutyRoster4View.omValues);

	AddPage(&omDutyRoster5View);
	omDutyRoster5View.SetCalledFrom(omCalledFrom);
	omDutyRoster5View.SetAccounts(pomAccounts);
}

/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

void DutyRosterPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("DUTYVIEW1", omDutyRoster1View.omValues);
	pomViewer->GetFilter("DUTYVIEW2", omDutyRoster2View.omValues);
	pomViewer->GetFilter("DUTYVIEW3", omDutyRoster3View.omValues);
	pomViewer->GetFilter("DUTYVIEW4", omDutyRoster4View.omValues);
	pomViewer->GetFilter("DUTYVIEW5", omDutyRoster5View.omValues);
	pomViewer->GetFilter("DUTYVIEW_URNOLIST", omEmployeeList);
}

//---------------------------------------------------------------------------

void DutyRosterPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("DUTYVIEW1", omDutyRoster1View.omValues);
	pomViewer->SetFilter("DUTYVIEW2", omDutyRoster2View.omValues);
	pomViewer->SetFilter("DUTYVIEW3", omDutyRoster3View.omValues);
	pomViewer->SetFilter("DUTYVIEW4", omDutyRoster4View.omValues);
	pomViewer->SetFilter("DUTYVIEW5", omDutyRoster5View.omValues);
	pomViewer->SetFilter("DUTYVIEW_URNOLIST", omEmployeeList);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

//---------------------------------------------------------------------------

int DutyRosterPropertySheet::QueryForDiscardChanges()
{
	return IDOK;
}

//---------------------------------------------------------------------------
