// ShiftRoster_Frm.cpp : implementation file
//
//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	OnToolTipText added, overrides the CMDIChildWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COMBO_HEIGHT 100    // width of edit control for combobox
#define COMBO_WIDTH  160    // drop down height

//*****************************************************************************
// ShiftRoster_Frm
//*****************************************************************************

IMPLEMENT_DYNCREATE(ShiftRoster_Frm, CMDIChildWnd)

//*****************************************************************************
// constructor
//*****************************************************************************

ShiftRoster_Frm::ShiftRoster_Frm()
{
}

//*****************************************************************************
// destructor
//*****************************************************************************

ShiftRoster_Frm::~ShiftRoster_Frm()
{
}

//*****************************************************************************
// 
//*****************************************************************************


BEGIN_MESSAGE_MAP(ShiftRoster_Frm, CMDIChildWnd)
	//{{AFX_MSG_MAP(ShiftRoster_Frm)
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(IDC_B_CODENUMBER, OnUpdateBCodenumber)
	ON_UPDATE_COMMAND_UI(IDC_B_RELEASE, OnUpdateBRelease)
	ON_UPDATE_COMMAND_UI(IDC_B_SAVEPLAN, OnUpdateBSaveplan)
	ON_UPDATE_COMMAND_UI(IDC_B_PLAN, OnUpdateBPlan)
	ON_UPDATE_COMMAND_UI(IDC_B_REQUIREMENT, OnUpdateBRequirement)
	ON_UPDATE_COMMAND_UI(IDC_B_VIEW, OnUpdateBView)
	ON_UPDATE_COMMAND_UI(ID_SHIFT_PRINT, OnShiftPrint)
	ON_WM_CLOSE()
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	ON_WM_MDIACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShiftRoster_Frm message handlers

//****************************************************************************
// OnCreate
//****************************************************************************

int ShiftRoster_Frm::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Toolbar
	if (!m_wndToolBar.CreateEx(this/*, TBSTYLE_FLAT */,TBSTYLE_BUTTON, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_SHIFTFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	// ComboBox der Toolbar erstellen
	if (!CreateComboBox(IDW_COMBO1,1) || !CreateComboBox(IDW_COMBO2,2) || !CreateComboBox(IDW_COMBO3,3) )
	{
		TRACE0("Failed to create combobox in toolbar\n");
		return -1;      // fail to create
	}


	return 0;
}

//****************************************************************************
// CreateComboBox
//****************************************************************************

int ShiftRoster_Frm::CreateComboBox(UINT nID,int nNumber)
{
	// Position der Combobox in der Toolbar
	int ilComboBoxPosition = m_wndToolBar.CommandToIndex(nID);
	ASSERT (ilComboBoxPosition > 0);
	CString csCode;

	CRect rect, comboRect;
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,nID, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	// Es k�nnen bis zu 3 Comboboxen exitieren
	switch (nNumber)
	{
	case 1:
			if (!m_wndToolBar.m_toolBarCombo.Create(CBS_SORT|CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|CBS_AUTOHSCROLL|WS_VSCROLL,
			rect, &m_wndToolBar, nID))	return FALSE;
			csCode = ogPrivList.GetStat("SR_CB_VIEW");
			if(csCode=='1') 
				m_wndToolBar.m_toolBarCombo.EnableWindow(true);
			if(csCode=='0' || csCode=='-')
				m_wndToolBar.m_toolBarCombo.EnableWindow(false);
			break;
	case 2:
			if (!m_wndToolBar.m_toolBarCombo2.Create(CBS_SORT|CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|CBS_AUTOHSCROLL|WS_VSCROLL,
			rect, &m_wndToolBar, nID))	return FALSE;
			csCode = ogPrivList.GetStat("SR_CB_Requirement");
			if(csCode=='1') 
				m_wndToolBar.m_toolBarCombo2.EnableWindow(true);

			if(csCode=='0' || csCode=='-')
				m_wndToolBar.m_toolBarCombo2.EnableWindow(false);

			break;
	case 3:
			if (!m_wndToolBar.m_toolBarCombo3.Create(CBS_SORT|CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|CBS_AUTOHSCROLL|WS_VSCROLL,
			rect, &m_wndToolBar, nID))	return FALSE;
			csCode = ogPrivList.GetStat("SR_CB_PLAN");
			if(csCode=='1') 
			{
				m_wndToolBar.m_toolBarCombo3.EnableWindow(true);
				m_wndToolBar.m_toolBarCombo3.SetDroppedWidth((UINT)(rect.Width() * 1.8));
			}
			else if(csCode=='0' || csCode=='-')
			{
				m_wndToolBar.m_toolBarCombo3.EnableWindow(false);
			}
			break;
	}

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;

	// Es k�nnen bis zu 3 Comboboxen exitieren
	switch (nNumber)
	{
	case 1:	
			m_wndToolBar.m_toolBarCombo.GetWindowRect(&comboRect);
			m_wndToolBar.m_toolBarCombo.ScreenToClient(&comboRect);
			m_wndToolBar.m_toolBarCombo.SetWindowPos(&m_wndToolBar.m_toolBarCombo, rect.left,
			rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
			SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
			break;
	case 2:
			m_wndToolBar.m_toolBarCombo2.GetWindowRect(&comboRect);
			m_wndToolBar.m_toolBarCombo2.ScreenToClient(&comboRect);
			m_wndToolBar.m_toolBarCombo2.SetWindowPos(&m_wndToolBar.m_toolBarCombo2, rect.left,
			rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
			SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
			break;
	case 3:	
			m_wndToolBar.m_toolBarCombo3.GetWindowRect(&comboRect);
			m_wndToolBar.m_toolBarCombo3.ScreenToClient(&comboRect);
			m_wndToolBar.m_toolBarCombo3.SetWindowPos(&m_wndToolBar.m_toolBarCombo3, rect.left,
			rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
			SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
	}
	return TRUE;
}

//****************************************************************************
// Es werden zwei verschiedene Toolbars geladen, um den Save Button
// zu ver�ndern.
//****************************************************************************

void ShiftRoster_Frm::LoadToolBar(BOOL bIsSave) 
{
	if (!bIsSave)
		// ohne attention
		m_wndToolBar.LoadToolBar(IDR_SHIFTFRAME);
	else
		// mit attention
		m_wndToolBar.LoadToolBar(IDR_SHIFTFRAMESAVE);

	// Position der Combobox1 in der Toolbar
	int ilComboBoxPosition = m_wndToolBar.CommandToIndex(IDW_COMBO1);
	ASSERT (ilComboBoxPosition > 0);

	CRect rect, comboRect;
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,IDW_COMBO1, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;
	m_wndToolBar.m_toolBarCombo.GetWindowRect(&comboRect);
	m_wndToolBar.m_toolBarCombo.ScreenToClient(&comboRect);

	m_wndToolBar.m_toolBarCombo.SetWindowPos(&m_wndToolBar.m_toolBarCombo, rect.left,
	rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
	SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);

	// Position der Combobox2 in der Toolbar
	ilComboBoxPosition = m_wndToolBar.CommandToIndex(IDW_COMBO2);
	ASSERT (ilComboBoxPosition > 0);
	
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,IDW_COMBO2, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;
	m_wndToolBar.m_toolBarCombo2.GetWindowRect(&comboRect);
	m_wndToolBar.m_toolBarCombo2.ScreenToClient(&comboRect);

	m_wndToolBar.m_toolBarCombo2.SetWindowPos(&m_wndToolBar.m_toolBarCombo2, rect.left,
	rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
	SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);

	// Position der Combobox3 in der Toolbar
	ilComboBoxPosition = m_wndToolBar.CommandToIndex(IDW_COMBO3);
	ASSERT (ilComboBoxPosition > 0);
	
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,IDW_COMBO3, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;
	m_wndToolBar.m_toolBarCombo3.GetWindowRect(&comboRect);
	m_wndToolBar.m_toolBarCombo3.ScreenToClient(&comboRect);

	m_wndToolBar.m_toolBarCombo3.SetWindowPos(&m_wndToolBar.m_toolBarCombo3, rect.left,
	rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
	SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);

	m_wndToolBar.RedrawWindow();
}

//****************************************************************************
// Erscheinungsbild des Frames �ndern
//****************************************************************************

BOOL ShiftRoster_Frm::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
	cs.style &= ~WS_MAXIMIZEBOX; 
	cs.style &= ~WS_THICKFRAME;
	
	return CMDIChildWnd::PreCreateWindow(cs);
}

//****************************************************************************
// Beim schlie�en des Frames testen ob der View geschlossen werden kann.
//****************************************************************************

void ShiftRoster_Frm::OnClose() 
{
	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL)
	{
		if (((ShiftRoster_View*) GetActiveView())->OkToExit())
			CMDIChildWnd::OnClose();
	}
}

//****************************************************************************
// Update Funktionen
//****************************************************************************

void ShiftRoster_Frm::OnUpdateBCodenumber(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_CODNUM");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
}

void ShiftRoster_Frm::OnUpdateBRelease(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_REL");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
}

void ShiftRoster_Frm::OnUpdateBSaveplan(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_SAVE");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
}

void ShiftRoster_Frm::OnUpdateBPlan(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_PLAN");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
}

void ShiftRoster_Frm::OnUpdateBRequirement(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_CB_REQ");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
	
}

void ShiftRoster_Frm::OnUpdateBView(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("SR_VIEW");
	
	if(csCode=='1' || csCode==' ') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
	
}

// rdr 12.01.2000 added
bool ShiftRoster_Frm::OnToolTipText(UINT, NMHDR *pNMHDR, LRESULT *pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);
	
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	
	CString strTipText;
	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) 
		|| pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		nID = ::GetDlgCtrlID((HWND)nID);
	}

	if (nID !=0)
	{
		CString olMsg;
		olMsg.LoadString(nID);		// if this a valid Res-String?
		CString olTxt = LoadStg(nID);

		if (olTxt.IsEmpty() && olMsg.IsEmpty())
		{
			return false;
		}
		AfxExtractSubString(strTipText, olTxt, 1, '\n');
	}

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#endif

	*pResult = 0;

	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

	return true;
}

//****************************************************************************
// Der Frame wurde aktiviert
//****************************************************************************

void ShiftRoster_Frm::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);
	
	// Nachricht an den Mainframe weitergeben
	if (bActivate)
	{
		CMainFrame* polMainFrame = (CMainFrame*) GetParentFrame();

		polMainFrame->ActiveFrameChanged(SHIFT_VIEW, "", "");
	}
}

void ShiftRoster_Frm::OnShiftPrint(CCmdUI* pCmdUI) 
{
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	CString olNoPrint = CString ("ADR,ZRH,BSL,GVA");
	if (olNoPrint.Find(olCustomer) > -1)
	{
		pCmdUI->Enable(false);
		return;
	}

	CString csCode;
	csCode = ogPrivList.GetStat("SR_PRINT");

	if (csCode == '1') 
	{
		pCmdUI->Enable(true);
	}
	else
	{
		pCmdUI->Enable(false);
	}
}