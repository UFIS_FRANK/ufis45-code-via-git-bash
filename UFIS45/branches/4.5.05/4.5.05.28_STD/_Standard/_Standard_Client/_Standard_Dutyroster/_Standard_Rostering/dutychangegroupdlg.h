#ifndef AFX_DUTYCHANGEGROUPDLG_H__D3332CB1_C582_11D2_8E6E_0000C002916B__INCLUDED_
#define AFX_DUTYCHANGEGROUPDLG_H__D3332CB1_C582_11D2_8E6E_0000C002916B__INCLUDED_

// DutyChangeGroupDlg.h : Header-Datei
//
#include <stdafx.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyChangeGroupDlg 

class DutyChangeGroupDlg : public CDialog
{
// Konstruktion
public:
	DutyChangeGroupDlg(INLINEDATA *prpInlineUpdate1, INLINEDATA *prpInlineUpdate2, CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyChangeGroupDlg();


private:
	DutyRoster_View *pomParent;

	//CSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;
	CCSPtrArray<STFDATA> *pomStfData;
	VIEWINFO *prmViewInfo;
	CToolTipCtrl* m_pToolTip;


	INLINEDATA rmInlineUpdate1;
	INLINEDATA rmInlineUpdate2;
	int imGroupLine1;
	int imGroupLine2;

// Dialogfelddaten
	//{{AFX_DATA(DutyChangeGroupDlg)
	enum { IDD = IDD_DUTY_CHANGEGROUP_DLG };
	CComboBox	m_C_Tauschen1;
	CComboBox	m_C_Tauschen2;
	CComboBox	m_C_Ersetzen1;
	CComboBox	m_C_Ersetzen2;
	CComboBox	m_C_Umsetzen2;
	CButton		m_B_Umsetzen;
	CButton		m_B_Tauschen;
	CButton		m_B_Info;
	CButton		m_B_Ersetzen;
	CButton		m_G_Name2;
	CButton		m_G_Name1;
	CCSEdit		m_E_Ersetzen1;
	CCSEdit		m_E_Ersetzen2;
	CCSEdit		m_E_FuncIs1;
	CCSEdit		m_E_FuncIs2;
	CCSEdit		m_E_FuncStamm1;
	CCSEdit		m_E_FuncStamm2;
	CCSEdit		m_E_GruppIs1;
	CCSEdit		m_E_GruppIs2;
	CCSEdit		m_E_GruppStamm1;
	CCSEdit		m_E_GruppStamm2;
	CCSEdit		m_E_Tauschen1;
	CCSEdit		m_E_Tauschen2;
	CCSEdit		m_E_Umsetzen2;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(DutyChangeGroupDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyChangeGroupDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBErsetzen();
	afx_msg void OnBTauschen();
	afx_msg void OnBUmsetzen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()



public:
	void InternalCancel();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYCHANGEGROUPDLG_H__D3332CB1_C582_11D2_8E6E_0000C002916B__INCLUDED_
