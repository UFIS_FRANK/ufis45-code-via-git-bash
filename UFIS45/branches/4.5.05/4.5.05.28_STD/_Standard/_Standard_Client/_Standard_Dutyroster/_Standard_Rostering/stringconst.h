//File StringConst.h

#define IDC_SHEET_VIEW		10
#define IDC_SHEET_SAVE		11
#define IDC_SHEET_DELETE	12
#define IDC_SHEET_APPLY		13
#define ID_SHEET_FLIGHT_SEASON_TABLE		"View Flight Schedule"
#define ID_SHEET_DISPO_DIAGRAM				"View Dispo"
#define ID_SHEET_COV_DIAGRAM				"View Coverage-Toolbox"
#define ID_SHEET_PREMIS_VIEWER				"View Rules"
#define ID_SHEET_NFPREMIS_VIEWER			"View Non-flight Rules"
#define ID_SHEET_GHSLIST_VIEWER				"View Services"

#define ID_SHEET_EQU_TABLE					"View Equipment"
#define ID_SHEET_DSR_TABLE					"View Daily Shifts"
