// DutyBsdDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static int CompareBsd( const BSDDATA **e1, const BSDDATA **e2);
static int CompareOda( const ODADATA **e1, const ODADATA **e2);

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyBsdDlg 
//---------------------------------------------------------------------------

//****************************************************************************************************
// Konstruktor: <ipOpenFrom> gibt an, welches Modul (Dienst- oder
//  Schichtplanung) den Dialog erzeugt hat.
//****************************************************************************************************

DutyBsdDlg::DutyBsdDlg(CWnd* pParent /*=NULL*/) : CDialog(DutyBsdDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DutyBsdDlg)
	//}}AFX_DATA_INIT
	// erzeugendes Modul (Schicht- oder Dienstplanung)
	// Zeiger auf Fenster des erzeugenden Moduls

	pomParent = pParent;

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(DutyBsdDlg::IDD, pParent);
}

//****************************************************************************************************
// Destruktor
//****************************************************************************************************

DutyBsdDlg::~DutyBsdDlg()
{
	// Arrays leeren
	omBsdLines.DeleteAll();
	omOdaLines.DeleteAll();
	// Tabellenobjekte l�schen
	delete pomBsdTable;
	delete pomOdaTable;
	// D'n'D-Objekt freigeben
	omDragDropObject.Revoke();
}

void DutyBsdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyBsdDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyBsdDlg, CDialog)
	//{{AFX_MSG_MAP(DutyBsdDlg)
	ON_MESSAGE(WM_TABLE_DRAGBEGIN,		OnDragBegin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyBsdDlg 
//---------------------------------------------------------------------------

//****************************************************************************************************
// OnInitDialog: Initialisierung des Dialogs.
//  R�ckgabe:	BOOL ->		TRUE: kein Oberfl�chenelement soll den Fokus erhalten
//							FALSE: ein Oberfl�chenelement soll den Fokus erhalten
//****************************************************************************************************

BOOL DutyBsdDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Icon laden
	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	// Dialog-Titel laden
	SetWindowText(LoadStg(IDS_STRING1618));

	// H�he und Breite einstellen
	int ilDlgWidth = 292;
	int ilDlgHight = 326;

	CRect olDlgRect, olParentRect, olSystemRect;
	// Bildschirmdimensionen ermitteln
	olSystemRect.left	= 0;
	olSystemRect.top	= 0;
	olSystemRect.right  = ::GetSystemMetrics(SM_CXSCREEN);
	olSystemRect.bottom = ::GetSystemMetrics(SM_CYSCREEN);
	
	// Dimension des Parent-Fensters ermitteln
	pomParent->GetWindowRect(&olParentRect);

	// Dimensionen des Dialog-Fensters einstellen
	olDlgRect.left		= olParentRect.left + 5;
	olDlgRect.top		= olParentRect.bottom - (ilDlgHight + 5);
	olDlgRect.right		= olDlgRect.left + ilDlgWidth;
	olDlgRect.bottom	= olDlgRect.top  + ilDlgHight;

	// Gr�sse des Dialogfensters anpassen, wenn es gr�sser als das
	// Parent-Window ist
	if(olDlgRect.right>olSystemRect.right)
	{
		int ilLeft = olDlgRect.right - olSystemRect.right;
		olDlgRect.left -= ilLeft;
		olDlgRect.right -= ilLeft;
	}
	if(olDlgRect.bottom>olSystemRect.bottom)
	{
		int ilTop = olDlgRect.bottom - olSystemRect.bottom;
		olDlgRect.top -= ilTop;
		olDlgRect.bottom -= ilTop;
	}

	// Einstellungen aktivieren
	MoveWindow(&olDlgRect, TRUE);

	// Objekt f�r Basisschichtentabelle erzeugen
	pomBsdTable = new CTable;
	// Mehrfachauswahl zulassen
	pomBsdTable->SetSelectMode(0);//LBS_MULTIPLESEL | LBS_EXTENDEDSEL

	// Objekt f�r Abwesenheitentabelle erzeugen
	pomOdaTable = new CTable;
	// Mehrfachauswahl zulassen
	pomOdaTable->SetSelectMode(0);//LBS_MULTIPLESEL | LBS_EXTENDEDSEL

	// Objekte zur Einstellung der Tabellengr�ssen
	CRect olRectBSD, olRectODA;
	
	// Gr�sse der Basisschichtentabelle einstellen
	olRectBSD.left	= 1;
	olRectBSD.top	= 0;
	olRectBSD.right = 285;
	olRectBSD.bottom = 198;

	// Gr�sse der Abwesenheitentabelle einstellen
	olRectODA.left	= olRectBSD.left;
	olRectODA.top	= olRectBSD.bottom+1;
	olRectODA.right = olRectBSD.right;
	olRectODA.bottom = 301;

	
	// Tabellenobjekte initialisieren
	pomBsdTable->SetTableData(this, 0, 0, 0, 0,
							::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW),
							::GetSysColor(COLOR_HIGHLIGHTTEXT),::GetSysColor(COLOR_HIGHLIGHT),
							&ogCourier_Regular_8, &ogCourier_Regular_8);
	pomOdaTable->SetTableData(this, 0, 0, 0, 0,
							::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW),
							::GetSysColor(COLOR_HIGHLIGHTTEXT),::GetSysColor(COLOR_HIGHLIGHT),
							&ogCourier_Regular_8, &ogCourier_Regular_8);


	//ab hier: UpdateDisplay_BSD_Table

	// Puffer f�r Header-Struktur der Tabellen
	CString olHeader;
	// Header f�r Basisschichten formatieren
	olHeader.Format("%s|%s|%s|%s|%s|%s|%s",LoadStg(IDS_STRING1619),LoadStg(SHIFT_BSD_ESBG),LoadStg(SHIFT_BSD_LSEN),
					LoadStg(SHIFT_BSD_BKF1),LoadStg(SHIFT_BSD_BKT1),LoadStg(SHIFT_BSD_BKD1),LoadStg(SHIFT_BSD_TYPE));

	// Header f�r Basisschichten einstellen
	pomBsdTable->SetHeaderFields(olHeader);
	// Spaltenbreiten einstellen
	pomBsdTable->SetFormatList("8|4|4|4|4|4|1");

	// Basisschichten-Array leeren
	omBsdLines.DeleteAll();
	// Tabellenobjekt aufr�umen
	pomBsdTable->ResetContent();
	// Basisschichten-Array f�llen
	int ilCount =  ogBsdData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
	   omBsdLines.NewAt(omBsdLines.GetSize(), ogBsdData.omData[i]);
	}
	// Basisschichten-Array nach Code sortieren
	omBsdLines.Sort(CompareBsd);
	
	// Tabellen-Objekt mit Datens�tzen f�llen
	ilCount = omBsdLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		BSDDATA rlBsd = omBsdLines[i];
		pomBsdTable->AddTextLine(Format_Table("BSD",&omBsdLines[i]),&omBsdLines[i]);
	}
	// Tabelle anzeigen
	pomBsdTable->DisplayTable();

	//end> UpdateDisplay_BSD_Table

	//ab hier: UpdateDisplay_ODA_Table

	// Spalten-Header formatieren
	olHeader.Format("%s|%s",LoadStg(IDS_STRING1620),LoadStg(IDS_STRING461));

	// Spalten-Header einstellen
	pomOdaTable->SetHeaderFields(olHeader);
	// SPaltenbreite einstellen
	pomOdaTable->SetFormatList("5|31");

	// Array leeren
	omOdaLines.DeleteAll();
	// Tabellen-Objekt aufr�umen
	pomOdaTable->ResetContent();
	// Abwesenheiten-Array f�llen
	ilCount =  ogOdaData.omData.GetSize();
	for(i = 0; i < ilCount; i++)
	{
	   omOdaLines.NewAt(omOdaLines.GetSize(), ogOdaData.omData[i]);
	}
	// Abwesenheiten-Array nach Code sortieren
	omOdaLines.Sort(CompareOda);

	// Tabellen-Objekt mit Datens�tzen f�llen
	ilCount = omOdaLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		ODADATA rlOda = omOdaLines[i];
		pomOdaTable->AddTextLine(Format_Table("ODA",&omOdaLines[i]),&omOdaLines[i]);
	}
	// Abwesenheiten-Tabelle anzeigen
	pomOdaTable->DisplayTable();

	//end> UpdateDisplay_ODA_Table

	// Positionen der Tabellen einstellen
	pomBsdTable->SetPosition(olRectBSD.left,olRectBSD.right,olRectBSD.top,olRectBSD.bottom);
	pomOdaTable->SetPosition(olRectODA.left,olRectODA.right,olRectODA.top,olRectODA.bottom);

	// Fenster anzeigen
	ShowWindow(SW_SHOWNORMAL);
	return TRUE;
}

//****************************************************************************************************
// OnCancel: macht nichts. Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************

void DutyBsdDlg::OnCancel() 
{
	//do nothing
}

//****************************************************************************************************
// OnOK: macht nichts.Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************

void DutyBsdDlg::OnOK() 
{
	//do nothing
}

//****************************************************************************************************
// InternalCancel: 
//****************************************************************************************************

void DutyBsdDlg::InternalCancel() 
{
	CDialog::OnCancel();
	CDialog::DestroyWindow();
	delete this;
}

//****************************************************************************************************
// Format_Table(): formatiert die Daten in <*prpData> je nach
//  Datentyp <opTable> in einem String.
//  R�ckgabe: CString -> formatierter Datensatz
//****************************************************************************************************

CString DutyBsdDlg::Format_Table(CString opTable,void *prpData)
{
	// CString f�r R�ckgabe, speichert den formatierten Datensatz
	CString s;
	// Tabelle "Basisschichten"?
	if(opTable == "BSD")
	{	// ja -> entsprechende Typkonvertierung des Pointers vornehmen
		BSDDATA *prlBsd = (BSDDATA*) prpData;
		// Datenfelder lesen und im String speichern
		s =  CString(prlBsd->Bsdc) + "|";
		s+=  CString(prlBsd->Esbg) + "|";
		s+=  CString(prlBsd->Lsen) + "|";
		s+=  CString(prlBsd->Bkf1) + "|";
		s+=  CString(prlBsd->Bkt1) + "|";
		s+=  CString(prlBsd->Bkd1) + "|";
		s+=  CString(prlBsd->Type);
	}
	// Tabelle "Abwesenheiten"?
	else if(opTable == "ODA")
	{	// ja -> entsprechende Typkonvertierung des Pointers vornehmen
		ODADATA *prlOda = (ODADATA*) prpData;
		// Datenfelder lesen und im String speichern
		s =  CString(prlOda->Sdac) + "|";
		s+=  CString(prlOda->Sdan);
	}
	// formatierten Datensatz zur�ckgeben
	return s;
}

//****************************************************************************************************
// OnDragBegin(): Messagehandler f�r Drag and Drop-Mechanismus
//  R�ckgabe: long -> immer 0L
//****************************************************************************************************

LONG DutyBsdDlg::OnDragBegin(UINT wParam, LONG lParam)
{
	// ItemID ermitteln
	int ilitemID = (int)wParam;

	// Zeiger auf Referenz-Listbox ermitteln
	CListBox *polTable = (CListBox*)lParam;

	// Zeiger auf die beiden vorhandenen Listboxen generieren
	CListBox *polBsdTab = pomBsdTable->GetCTableListBox();
	CListBox *polOdaTab = pomOdaTable->GetCTableListBox();

	// Listbox-Zeiger vergleichen, um Ursprung der D'n'D-Aktion zu
	// ermitteln
	if(polTable == polBsdTab)
	{
		// Ursprung = Basisschichten-Tabelle
		// Zeiger auf Datensatz ermitteln
		BSDDATA *prlBsd = NULL;
		prlBsd = (BSDDATA *)pomBsdTable->GetTextLineData(ilitemID);
		// Daten vorhanden?
		if(prlBsd != NULL)
		{	
			// ACHTUNG: Ziel k�nnen beide Views sein !
			// D'n'D-Objekt initialisieren
			omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 2);
			//omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
			// Basisschichten-Datenobjekt hinzuf�gen
			omDragDropObject.AddString(prlBsd->Bsdc);
			// "Absenderkennung" mitgeben
			omDragDropObject.AddString("BSD");
			// Aktion fortsetzen
			omDragDropObject.BeginDrag();
			// alte Code
//			omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
		}
	}
	if(polTable == polOdaTab)
	{
		// Ursprung = Abwesenheiten-Tabelle
		// Zeiger auf Datensatz ermitteln
		ODADATA *prlOda = NULL;
		prlOda = (ODADATA *)pomOdaTable->GetTextLineData(ilitemID);
		// Daten vorhanden?
		if(prlOda != NULL)
		{
			// ACHTUNG: Ziel k�nnen beide Views sein !
			// D'n'D-Objekt initialisieren
			//omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
			// Hier ist der 2 Parameter eine 2. weil 2 Datenpakete �bergeben werden.
			omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 2);
			// Abwesenheiten-Datenobjekt hinzuf�gen
			omDragDropObject.AddString(prlOda->Sdac);
			// "Absenderkennung" �bergeben
			omDragDropObject.AddString("ODA");
			// Aktion fortsetzen
			omDragDropObject.BeginDrag();

			// alte Code
//			omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
		}
	}
	return 0L;
}

//****************************************************************************************************
//****************************************************************************************************
//	Ab hier: globale Funktionen
//****************************************************************************************************
//****************************************************************************************************

//****************************************************************************************************
// CompareBsd(): vergleicht die Codes (Feld Bscd) zweier Basisschichten-Datens�tze 
//  in Stringform auf lexikalisches gr�sser/kleiner:
//  R�ckgabe: int ->	<0: e1 ist kleiner als e2
//						=0: e1 == e2
//						>0:	e1 ist gr��er als e2
//****************************************************************************************************

static int CompareBsd( const BSDDATA **e1, const BSDDATA **e2)
{
	// den Code vergleichen
	return (strcmp((**e1).Bsdc, (**e2).Bsdc));
}

//****************************************************************************************************
// CompareOda(): vergleicht die Codes (Feld Sdac) zweier Abwesenheiten-Datens�tze 
//  in Stringform auf lexikalisches gr�sser/kleiner:
//  R�ckgabe: int ->	<0: e1 ist kleiner als e2
//						=0: e1 == e2
//						>0:	e1 ist gr��er als e2
//****************************************************************************************************

static int CompareOda( const ODADATA **e1, const ODADATA **e2)
{
	// den Code vergleichen
	return (strcmp((**e1).Sdac, (**e2).Sdac));
}
