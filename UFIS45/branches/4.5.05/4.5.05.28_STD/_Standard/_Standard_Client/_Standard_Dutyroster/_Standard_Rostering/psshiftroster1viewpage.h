#ifndef AFX_PSSHIFTROSTER1VIEWPAGE_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_
#define AFX_PSSHIFTROSTER1VIEWPAGE_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_

// PSShiftRoster1ViewPage.h : Header-Datei
//
#include <Ansicht.h>
#include <RosterViewPage.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSShiftRoster1ViewPage 

class PSShiftRoster1ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSShiftRoster1ViewPage)

// Konstruktion
public:
	PSShiftRoster1ViewPage();
	~PSShiftRoster1ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSShiftRoster1ViewPage)
	enum { IDD = IDD_PSSHIFTROSTER };
	CButton	m_R_Diff;
	CButton	m_R_IstSoll;
	CButton	m_R_AllGSP;
	CButton	m_R_SPlanGSP;
	CButton	m_C_Grupp;
	CButton	m_C_Detail;
	CButton	m_C_Bonus;
	CButton	m_R_Perc;
	CButton	m_R_FLName;
	CStatic	m_S_ShowStatistic;
	CStatic	m_S_ShowGSP;
	CStatic	m_S_ShowDiff;
	CStatic	m_S_Employee;
	CStatic m_S_ShowSP;
	CComboBox m_C_ShowSPCombo;
	CButton	m_R_Free;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSShiftRoster1ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSShiftRoster1ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnRFree();
	afx_msg void OnRFlname();
	afx_msg void OnRPerc();
	afx_msg void OnBStartConfig();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);
	void HideScenario();

private:
	ShiftRoster_View* GetViewInfo();
	void HandleNameRadioButtons();
	CString omTitleStrg;
	CString omConfigString;
	NameConfigurationDlg* prmNameConfigurationDlg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSSHIFTROSTER1VIEWPAGE_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_
