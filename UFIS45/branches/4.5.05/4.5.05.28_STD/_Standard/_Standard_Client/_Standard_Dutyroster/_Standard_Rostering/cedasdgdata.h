#ifndef _CEDASDGDATA_H_
#define _CEDASDGDATA_H_

#include <stdafx.h>

struct SDGDATA
{
	CTime 	 Begi;
	char 	 Days[4];
	char 	 Dnam[33];
	char 	 Dura[4];
	char 	 Peri[4];	
	char 	 Repi[801];
	long 	 Urno;
	char	 Expd[2+1];
	int		IsChanged;

	SDGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno = 0;
		Begi = TIMENULL;
		IsChanged = 0;
	}

};	//typedef struct SdtDataStruct SDGDATA;


// the broadcast CallBack function, has to be outside the CedaSdgData class
void ProcessSdgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

class CedaSdgData: public CedaData
{
// Attributes
public:
    CCSPtrArray<SDGDATA> omData;

    CMapPtrToPtr omUrnoMap;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaSdgData();
	~CedaSdgData();

	bool Read(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<SDGDATA> *popSdg, char *pspWhere, char *pspFieldList, bool ipSYS);
	void PrepareSdgData(SDGDATA *prpSdt);
    bool InsertSdg(const SDGDATA *prpSdgData);    // used in PrePlanTable only
    bool UpdateSdg(const SDGDATA *prpSdgData);    // used in PrePlanTable only
	void ProcessSdgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool AddSdgInternal(SDGDATA *prpSdt);
	bool ClearAll();

	SDGDATA * GetSdgByUrno(long pcpUrno);
	bool ChangeSdgTime(long lpSdtUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool SdtExist(long Urno);
	bool InsertSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool UpdateSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool DeleteSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool SaveSdg(SDGDATA *prpSdg);
	bool DeleteSdgInternal(SDGDATA *prpSdg);
	void GetTimeFrames(long lpUrno, CCSPtrArray<TIMEFRAMEDATA> &opTimes);
	DutyRoster_View* GetViewInfo();
	bool IsSdgOparative(SDGDATA *prpSdg);
	void CheckReloadSDTs();
private:

protected:
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;
	void Register();
};

extern CedaSdgData ogSdgData;

#endif
