// DoubleTourDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDoubleTourDlg 


CDoubleTourDlg::CDoubleTourDlg(CWnd* pParent,DRRDATA* popDrr,DRSDATA* popDrs,CCSPtrArray<STFDATA>* popStfData, DutyRoster_View* pDutyRoster_View)
	: CDialog(CDoubleTourDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDoubleTourDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	pomDrs = popDrs;
	pomDrr = popDrr;
	pomStfData = popStfData;
	pomDutyRoster_View = pDutyRoster_View;
}


void CDoubleTourDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDoubleTourDlg)
	DDX_Control(pDX, IDC_LIST_EMP_2, m_LB_Empl2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDoubleTourDlg, CDialog)
	//{{AFX_MSG_MAP(CDoubleTourDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDoubleTourDlg 

BOOL CDoubleTourDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olDate = CString(pomDrs->Sday);
	olDate = olDate.Right(2) + "." + olDate.Mid(4,2) + "." + olDate.Left(4);
	
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING1907) + " "+ olDate);

	// Static setzen
	SetDlgItemText(IDC_STATIC1,LoadStg(IDS_STRING1908));
	SetDlgItemText(IDC_STATIC2,LoadStg(IDS_STRING1909));
	SetDlgItemText(IDC_STATIC3,LoadStg(IDS_STRING1910));
	SetDlgItemText(IDC_STATIC4,LoadStg(IDS_STRING1911));
	SetDlgItemText(IDC_STATIC5,LoadStg(IDS_STRING1943));
	SetDlgItemText(IDOK,LoadStg(IDS_STRING1912));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));

	
	CString olFullName;
	// Mitarbeiter 1 setzen
	STFDATA *prlStf = ogStfData.GetStfByUrno(pomDrs->Stfu);
	if (prlStf != NULL)
	{
		olFullName = CBasicData::GetFormatedEmployeeName(pomDutyRoster_View->rmViewInfo.iEName, prlStf, "", "");
		SetDlgItemText(IDC_STATIC_EMP1,olFullName);
	}

	// Listbox f�r Mitarbeiter 2 f�llen
	CString olUrno;
	int ilCount = 0;
	for ( int i=0; i<pomStfData->GetSize(); i++ )
	{
		// Mitarbeiter1 und Mitarbeiter2 m�ssen verschieden sein
		if (pomDrs->Stfu != (*pomStfData)[i].Urno)
		{
			// Name und Vorname
			olFullName = CBasicData::GetFormatedEmployeeName(pomDutyRoster_View->rmViewInfo.iEName, &(*pomStfData)[i], "", "");
			m_LB_Empl2.AddString(olFullName);

			// Map f�llen
			olUrno.Format("%ld",(*pomStfData)[i].Urno);
			omStfu2Map.SetAt(olFullName,olUrno);

			// Mitarbeiter in Listbox markieren
			if (pomDrs->Ats1 == (*pomStfData)[i].Urno)
			{
				m_LB_Empl2.SetCurSel(ilCount);
			}
			ilCount++;
		}
	}

	// Radiobuttons setzen
	if (CString(pomDrs->Stat) == "S")
	{
		((CButton*) GetDlgItem(IDC_RADIO2))->SetCheck(1);
		((CButton*) GetDlgItem(IDC_RADIO4))->SetCheck(1);
	}
	else
	{
		((CButton*) GetDlgItem(IDC_RADIO1))->SetCheck(1);
		((CButton*) GetDlgItem(IDC_RADIO3))->SetCheck(1);
	}

	return TRUE;  
}

//**********************************************************************************
//
//**********************************************************************************

void CDoubleTourDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);

	// Mitarbeiter 2 auswerten
	CString olName2;
	m_LB_Empl2.GetText(m_LB_Empl2.GetCurSel(), olName2);
	if (olName2.IsEmpty())
	{
		// TODO Fehlermeldung ausgeben
		MessageBox(LoadStg(IDS_STRING1595));
		RemoveWaitCursor();
		return;	
	}

	// Urno f�r den 2. Mitarbeiter auswerten
	long llStfu2 = 0;
	CString olStfu2;
	if (omStfu2Map.Lookup(olName2, olStfu2))
	{
		llStfu2 = atol(olStfu2);
	}

	//---------------------------------------------------------------------------
	// Daten f�r Mitarbeiter 2 vorbereiten
	
	DRRDATA* polDrr2 = CreateEmpl2Drr(llStfu2);

	if (polDrr2 == NULL)
	{
		MessageBox(LoadStg(IDS_STRING378),NULL,MB_ICONWARNING);
		CDialog::OnCancel();
		RemoveWaitCursor();
		return;
	}

	// Test, ob schon eine Doppeltour f�r MA2 vorliegt
	if (ogDrrData.HasDoubleTour(polDrr2))
	{
		MessageBox(LoadStg(IDS_STRING302),NULL,MB_ICONWARNING);
		RemoveWaitCursor();
		return;
	}

	DRSDATA* polDrs2 = CreateEmpl2Drs(polDrr2,llStfu2);

	if (polDrs2 == NULL)
	{
		MessageBox(LoadStg(IDS_STRING378),NULL,MB_ICONWARNING);
		CDialog::OnCancel();
		RemoveWaitCursor();
		return;
	}
	//---------------------------------------------------------------------------

	DRRDATA olDrrSave;
	ogDrrData.CopyDrr(&olDrrSave, pomDrr);	// Speichern f�r den Abbruchfall

	// Daten f�r den Mitarbeiter 1 setzen
	if (((CButton*) GetDlgItem(IDC_RADIO4))->GetCheck() == 1)	//Schicht des 2. MAs nehmen
	{
		ogDrrData.CopyDrrValues (polDrr2, pomDrr, true);
	}
	EditEmpl1Data(olName2);

	// Daten f�r den Mitarbeiter 2 setzen
	if (!EditEmpl2Data(olName2,polDrr2,polDrs2))
	{
		ogDrrData.CopyDrr(pomDrr,&olDrrSave);	// Wiederherstellen
		MessageBox(LoadStg(IDS_STRING137),NULL,MB_ICONWARNING);
		CDialog::OnCancel();
		RemoveWaitCursor();
		return;
	}

	// Schichten nach Einhaltung der Arbeitsregeln testen
	bool blShiftEnabled = true;

	// ShiftCheck durchf�hren
	blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(pomDrr, pomDutyRoster_View->rmViewInfo.bDoConflictCheck, DRR_CHANGE, false);
	if(blShiftEnabled)
	{
		blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(polDrr2, pomDutyRoster_View->rmViewInfo.bDoConflictCheck, DRR_CHANGE, false);
	}

	// Fehler ausgeben, wenn vorhanden
	if(ogShiftCheck.IsWarning())
	{
		pomDutyRoster_View->ForwardWarnings();
	}

	if(!blShiftEnabled)
	{
		ogDrrData.CopyDrr(pomDrr,&olDrrSave);	// Wiederherstellen
	}
	else if(!SaveData(polDrr2,polDrs2))
	{
		ogDrrData.CopyDrr(pomDrr,&olDrrSave);	// Wiederherstellen
		MessageBox(LoadStg(IDS_STRING138),NULL,MB_ICONWARNING);
		CDialog::OnCancel();
		RemoveWaitCursor();
		return;
	}
	//---------------------------------------------------------------------------

	RemoveWaitCursor();
	CDialog::OnOK();
}

//**********************************************************************************
// Daten f�r speichern
//**********************************************************************************

bool CDoubleTourDlg::SaveData(DRRDATA* popDrr2, DRSDATA* popDrs2)
{
	bool blResult;

	//-------------------------------------------------------------------------
	// Mitarbeiter 1
	//-------------------------------------------------------------------------
	
	// DRS sichern
	if (pomDrs->IsChanged == DATA_NEW)	
	{
		blResult = ogDrsData.Insert(pomDrs);
	}
	else
	{
		blResult = ogDrsData.Update(pomDrs);
	}

	if (!blResult)
	{	
		// Fehler ausgeben
		CString olErrorTxt;
		olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrsData.imLastReturnCode, ogDrsData.omLastErrorMessage);
		MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return false;
	}	

	// Drr sichern
	if (pomDrr->IsChanged == DATA_NEW)
	{
		blResult = ogDrrData.Insert(pomDrr);
	}
	else
	{
		blResult = ogDrrData.Update(pomDrr);
	}

	if (!blResult)
	{	
		// Fehler ausgeben
		CString olErrorTxt;
		olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
		MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return false;
	}	

	//-------------------------------------------------------------------------
	// Mitarbeiter 2
	//-------------------------------------------------------------------------
	
	// DRS sichern
	if (popDrs2->IsChanged == DATA_NEW)
	{
		blResult = ogDrsData.Insert(popDrs2);
	}
	else
	{
		blResult = ogDrsData.Update(popDrs2);
	}

	if (!blResult)
	{	
		// Fehler ausgeben
		CString olErrorTxt;
		olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrsData.imLastReturnCode, ogDrsData.omLastErrorMessage);
		MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return false;
	}	

	// Drr sichern
	if (popDrr2->IsChanged == DATA_NEW)
	{
		blResult = ogDrrData.Insert(popDrr2);
	}
	else
	{
		blResult = ogDrrData.Update(popDrr2);
	}

	if (!blResult)
	{	
		// Fehler ausgeben
		CString olErrorTxt;
		olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
		MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return false;
	}	

	//-------------------------------------------------------------------------
	return true;
}


//**********************************************************************************
// DRR Daten f�r Mitarbeiter 2 vorbereiten
//**********************************************************************************

DRRDATA* CDoubleTourDlg::CreateEmpl2Drr(long lpStfu2)
{
	DRRDATA* polDrr;
	//-------------------------------------------------------------------------
	// aktuellen DRR f�r Mitarbeiter 2 finden
	polDrr = ogDrrData.GetDrrByRoss(pomDrs->Sday,lpStfu2,"1","","A");

	// Kein aktiver DRR gefunden
	if (polDrr == NULL)
	{
		// DRRDATA erzeugen, initialisieren und NICHT in die Datenhaltung mit aufnehmen
		polDrr = ogDrrData.CreateDrrData(lpStfu2,pomDrs->Sday,"2","A","1",false);
	}
	//-------------------------------------------------------------------------
	return polDrr;
}

//**********************************************************************************
// DRR Daten f�r Mitarbeiter 2 vorbereiten
//**********************************************************************************

DRSDATA* CDoubleTourDlg::CreateEmpl2Drs(DRRDATA* popDrr2,long lpStfu2)
{
	DRSDATA* polDrs;
	//-------------------------------------------------------------------------
	// aktuellen DRS f�r Mitarbeiter 2 finden
	polDrs = ogDrsData.GetDrsByDrru(popDrr2->Urno);

	// Kein DRS gefunden
	if (polDrs == NULL)
	{
		polDrs =  ogDrsData.CreateDrsData(lpStfu2, pomDrs->Sday, popDrr2->Urno);
	}
	//-------------------------------------------------------------------------
	return polDrs;
}

//**********************************************************************************
// Daten f�r den 1.Mitarbeiter setzen
//**********************************************************************************

void CDoubleTourDlg::EditEmpl1Data(CString opName)
{
	//-------------------------------------------------------------------------
	// DRS f�r Mitarbeiter 1 bearbeiten
	//-------------------------------------------------------------------------

	// Urno von Mitarbeiter 2 in DRS von Mitarbeiter 1 eintragen.
	CString olStringResult;
	if (omStfu2Map.Lookup(opName, olStringResult))
	{
		pomDrs->Ats1 = atol(olStringResult);
	}

	// Status f�r Mitarbeiter 1 setzen
	if (((CButton*) GetDlgItem(IDC_RADIO1))->GetCheck() == 1)
	{
		strcpy(pomDrs->Stat, "T");
	}
	else
	{
		strcpy(pomDrs->Stat, "S");
	}

	//-------------------------------------------------------------------------
	// DRR f�r Mitarbeiter 1 bearbeiten 
	//-------------------------------------------------------------------------
	strcpy(pomDrr->Drsf,"1");
}

//**********************************************************************************
// Daten f�r den 2.ten Mitarbeiter setzen
// FALSE wenn die Schicht des 2. Mitarbeiters nicht ver�ndert werden durfte. z.B.
// weil MA schon in Doppeltour oder DRA/DRD vorhanden.
//**********************************************************************************

bool CDoubleTourDlg::EditEmpl2Data(CString opName,DRRDATA* popDrr2, DRSDATA* popDrs2)
{
	// DRR holen f�r Mitarbeiter 2 holen
	long llUrno2, llNewUrno;
	CString olStringResult;

	if(omStfu2Map.Lookup(opName, olStringResult) )
		llUrno2 = atol(olStringResult);

	//---------------------------------------------------------------------
	// Beim 2.ten Mitarbeiter den DRS umsetzen 
	//---------------------------------------------------------------------
	popDrs2->Ats1 = pomDrs->Stfu;

	// Status f�r Mitarbeiter 2 setzen
	if (((CButton*) GetDlgItem(IDC_RADIO1))->GetCheck() == 1)
		strcpy(popDrs2->Stat, "S");
	else
		strcpy(popDrs2->Stat, "T");

	//---------------------------------------------------------------------
	// Schichtcode umsetzen
	//---------------------------------------------------------------------

	// Wenn sich die Schichtcodes unterscheiden,Code umsetzen.
	if (pomDrr->Bsdu != popDrr2->Bsdu)
	{
		// Schichtcode f�r 2.ten Mitarbeiter pr�fen
		llNewUrno = ogDrrData.CheckBSDCode(popDrr2,pomDrr->Bsdu,popDrr2->Bsdu);

		// Pr�fung erfolgreich ?
		if (llNewUrno == popDrr2->Bsdu)
		{
			return false;
		}
		else
		{
			// weiter Daten setzen (Schichtbegin usw.)
			bool blCheckMainFuncOnly = ((DutyRoster_View*)m_pParentWnd)->rmViewInfo.bCheckMainFuncOnly;
			if(!ogDrrData.SetDrrDataScod(popDrr2,true,pomDrr->Bsdu,pomDrr->Fctc,false,blCheckMainFuncOnly))
				return false;
		}
	}

	//---------------------------------------------------------------------
	// Beim 2.ten Mitarbeiter den DRR umsetzen und speichern
	//---------------------------------------------------------------------
	strcpy(popDrr2->Drsf,"1");

	return true;
}

