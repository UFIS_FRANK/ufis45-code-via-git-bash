#if !defined(AFX_WISHDIALOG_H__21FE80F5_A313_11D3_8FBD_00500454BF3F__INCLUDED_)
#define AFX_WISHDIALOG_H__21FE80F5_A313_11D3_8FBD_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WishDialog.h : Header-Datei
//
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CWishDialog 

class CWishDialog : public CDialog
{
// Konstruktion
public:
	CWishDialog(CWnd* pParent, CString opUrno, long lpStfuUrno,CString opSday,bool bpReadOnly = false);

// Dialogfelddaten
	//{{AFX_DATA(CWishDialog)
	enum { IDD = IDD_WISH_DIALOG };
	BOOL	m_BIsLocked;
	CString	m_csRems;
	CString	m_csReme;
	CString	m_csRemi;
	CString	m_csWisc;
	//}}AFX_DATA

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CWishDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Urno des Wunsch Datensatzes
	CString omUrno;
	// anfänglich gültiger Wunsch Code
	CString m_StartWisc;
	// Soll ein neuer Datensatz angelegt werden ?
	bool	bmMakeNew;
	// Mitarbeiter Urno
	long	lmStfuUrno;
	// Mitarbeiter Organisationseinheit
	CString	omDpt1;
	// Datum
	CString omSday;
	// Read only Flag
	bool bmReadOnly;
	
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CWishDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadiop1();
	afx_msg void OnRadiop2();
	afx_msg void OnRadiop3();
	afx_msg void OnRadiopn();
	afx_msg void OnDelete();
	afx_msg void OnEditcode();
	virtual void OnOK();
	afx_msg void OnKillfocusWisc();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Setzt die Radiobuttons
	void SetRadioButtons(CString opPrio);
	// Static und Buttontexte setzen.
	void SetText();
	// Setzt Controlls ReadOnly
	void DisableControls();
private:
	DutyRoster_View *pomParent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_WISHDIALOG_H__21FE80F5_A313_11D3_8FBD_00500454BF3F__INCLUDED_
