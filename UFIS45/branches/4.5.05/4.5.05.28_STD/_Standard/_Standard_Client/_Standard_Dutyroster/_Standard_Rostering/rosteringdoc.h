// RosteringDoc.h : interface of the CRosteringDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROSTERINGDOC_H__F66E6B6C_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
#define AFX_ROSTERINGDOC_H__F66E6B6C_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//***************************************************************************
// Klasse CRosteringDoc
//***************************************************************************

class CRosteringDoc : public CDocument
{
protected: // create from serialization only
	CRosteringDoc();
	DECLARE_DYNCREATE(CRosteringDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRosteringDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void SetTitle(LPCTSTR lpszTitle);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRosteringDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRosteringDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROSTERINGDOC_H__F66E6B6C_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
