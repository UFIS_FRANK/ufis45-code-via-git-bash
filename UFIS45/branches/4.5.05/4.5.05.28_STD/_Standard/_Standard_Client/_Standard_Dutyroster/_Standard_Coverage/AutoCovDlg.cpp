// AutoCovDlg.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <AutoCovDlg.h>
#include <BasicData.h>
#include <CovDiagram.h>
#include <CedaSdgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutoCovDlg dialog


CAutoCovDlg::CAutoCovDlg(CWnd* pParent,CStringArray &opBasicShiftViewList)
	: CDialog(CAutoCovDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutoCovDlg)
	m_Adaption = 0;
	m_Direction = 2;
	m_ShiftSelection = 1;
	m_ShiftDeviation = 15;
	m_AdaptIterations = 10;
	m_Factor = 100;
	m_MinShiftCoverage = 90;
	m_UseBaseCoverage = TRUE;
	//}}AFX_DATA_INIT

	imColCount = 6;


	for(int illc = 0; illc < opBasicShiftViewList.GetSize(); illc++)
	{
		omPossibleList.Add(opBasicShiftViewList[illc]);
		omSelectedList.Add(opBasicShiftViewList[illc]);
	}

	pomPossilbeList = new CGridFenster(this);
	pomSelectedList = new CGridFenster(this);
	m_AssignEndTime.SetTypeToTime(true,false);
	m_AssignStartTime.SetTypeToTime(true,false);
	m_AssignEndDate.SetTypeToDate(true);
	m_AssignStartDate.SetTypeToDate(true);

	imMaxDuration = -1;

	imStandardShiftDeviation	= 15;
	imMinStandardShiftCoverage	= 90;

	imHighShiftDeviation		= 15;
	imMinHighShiftCoverage		= 50;
	lmSdgu = 0;
			
}

CAutoCovDlg::~CAutoCovDlg()
{
	delete pomPossilbeList;
	delete pomSelectedList;
}


void CAutoCovDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CAutoCovDlg)
	DDX_Control(pDX, IDC_ASSIGNENDDATE, m_AssignEndDate);
	DDX_Control(pDX, IDC_ASSIGNSTARTDATE, m_AssignStartDate);
	DDX_Control(pDX, IDC_ASSIGNENDTIME, m_AssignEndTime);
	DDX_Control(pDX, IDC_ASSIGNSTARTTIME, m_AssignStartTime);
	DDX_Radio(pDX, IDC_SIMPLEADAPT, m_Adaption);
	DDX_Radio(pDX, IDC_OVERCOV, m_ShiftSelection);
	DDX_Text(pDX, IDC_SHIFTDEV, m_ShiftDeviation);
	DDV_MinMaxInt(pDX, m_ShiftDeviation, 0, 600);
	DDX_Text(pDX, IDC_ADAPTITER, m_AdaptIterations);
	DDV_MinMaxInt(pDX, m_AdaptIterations, 1, 100000);
	DDX_Text(pDX, IDC_FACTOR, m_Factor);
	DDV_MinMaxInt(pDX, m_Factor, 0, 10000);
	DDX_Text(pDX, IDC_SHIFTCOV, m_MinShiftCoverage);
	DDV_MinMaxInt(pDX, m_MinShiftCoverage, 0, 100);
	DDX_Check(pDX, IDC_USE_BASE_COVERAGE, m_UseBaseCoverage);
	//}}AFX_DATA_MAP


}


BOOL CAutoCovDlg::UpdateData(BOOL bSaveAndValidate)	
{
	return CDialog::UpdateData(bSaveAndValidate);	
}

BEGIN_MESSAGE_MAP(CAutoCovDlg, CDialog)
	//{{AFX_MSG_MAP(CAutoCovDlg)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnGridEndEditing)
	ON_BN_CLICKED(IDC_OVERCOV, OnOvercov)
	ON_BN_CLICKED(IDC_UNDERCOV, OnUndercov)
	ON_BN_CLICKED(IDC_BUTTON_INTERNAL, OnInternal)
	ON_BN_CLICKED(IDC_FASTADAPT, OnFastadapt)
	ON_BN_CLICKED(IDC_SIMPLEADAPT, OnSimpleadapt)
	ON_BN_CLICKED(IDC_EVOLUATE, OnEvoluate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoCovDlg message handlers

void CAutoCovDlg::SetData(long lpSdgu,CStringArray &opBasicShiftViewList,CString opSelView,CTime opStartTime, CTime opEndTime ) 
{

	omPossibleList.RemoveAll();
	omSelectedList.RemoveAll();
	for(int illc = 0; illc < opBasicShiftViewList.GetSize(); illc++)
	{
		if(opBasicShiftViewList[illc] != opSelView)
		{
			omPossibleList.Add(opBasicShiftViewList[illc] + "|P|0|100|0|0");
		}
	}
	omSelectedList.Add(opSelView + "|P|0|100|0|0");

	omStartTime = opStartTime;
	omEndTime	= opEndTime;
	lmSdgu		= lpSdgu;

}

void CAutoCovDlg::UpdateData(long lpSdgu,CStringArray &opBasicShiftViewList,CString opSelView) 
{
	CStringArray olCurrentList;
	olCurrentList.Append(omPossibleList);
	olCurrentList.Append(omSelectedList);

//	add new basic shifts to the list of possible shifts
	for(int illc = 0; illc < opBasicShiftViewList.GetSize(); illc++)
	{
		if (!FindShift(opBasicShiftViewList[illc],olCurrentList))			
			omPossibleList.Add(opBasicShiftViewList[illc] + "|P|0|100|0|0");
	}

//	remove no longer existing basic shifts from lists
	for(illc = omPossibleList.GetSize() - 1; illc >= 0;illc--)
	{
		if (!FindShift(omPossibleList[illc],opBasicShiftViewList))			
			omPossibleList.RemoveAt(illc);
	}

	for(illc = omSelectedList.GetSize() - 1; illc >= 0;illc--)
	{
		if (!FindShift(omSelectedList[illc],opBasicShiftViewList))			
			omSelectedList.RemoveAt(illc);
	}

	lmSdgu = lpSdgu;
}

BOOL CAutoCovDlg::FindShift(const CString& ropShift,CStringArray& ropShiftList)
{
	CString olShift = ropShift;
	int ilDel = olShift.Find('|');
	if (ilDel >= 0)
		olShift = olShift.Left(ilDel);

	for(int illc = 0; illc < ropShiftList.GetSize(); illc++)
	{
		CString olCompShift = ropShiftList[illc];
		int ilDel = olCompShift.Find('|');
		if (ilDel >= 0)
			olCompShift = olCompShift.Left(ilDel);
		if (olShift == olCompShift)
			return TRUE;
	}

	return FALSE;
}


void CAutoCovDlg::GetData(CStringArray &opSelectViewList,CTime &opStartTime, CTime &opEndTime ) 
{

	opSelectViewList.RemoveAll();
	for(int illc = 0; illc < omSelectedList.GetSize(); illc++)
	{		
		opSelectViewList.Add(omSelectedList[illc]);
	}
	opStartTime = omStartTime;
	opEndTime = omEndTime;
	
}

void CAutoCovDlg::OnAdd() 
{
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	int ilSelCount = (int)pomPossilbeList->GetSelectedRows( olRows);

	
	if(ilSelCount > 0)
	{
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);
		
		for (int ilLc = olRows.GetSize()-1;ilLc >= 0 ; ilLc--)
		{
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
			
			int ilDummy = (int)olRows[ilLc];
			if(ilDummy > 0)
			{
				int ilRowCount = pomSelectedList->GetRowCount() +1;
				pomSelectedList->InsertRows(ilRowCount,1);
				for(int illc2 = 1; illc2 <= (int) pomPossilbeList->GetColCount();illc2++)
				{
					olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], illc2);

					if(illc2 ==1)
					{
						pomSelectedList->SetStyleRange(CGXRange(ilRowCount, illc2, ilRowCount, illc2), olStyle.SetValue(olText));
					}
					else if(illc2 == 2)
					{
						pomSelectedList->SetStyleRange(CGXRange(ilRowCount, illc2, ilRowCount, illc2),
						CGXStyle( )
						.SetValue(olText)
						.SetVerticalAlignment(DT_VCENTER)
						.SetControl(GX_IDS_CTRL_COMBOBOX)
						.SetChoiceList("P\nA"));
					}
					else
					{
						pomSelectedList->SetStyleRange(CGXRange(ilRowCount, illc2, ilRowCount, illc2),
							CGXStyle().SetValue(olText)
							   .SetEnabled(TRUE)
							   .SetReadOnly(FALSE));
					}
				}
				
				pomPossilbeList->RemoveRows(olRows[ilLc],olRows[ilLc]);
			}
		}

		
	}
}


void CAutoCovDlg::OnRemove() 
{
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	int ilSelCount = (int)pomSelectedList->GetSelectedRows( olRows);

	
	if(ilSelCount > 0)
	{
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);
		
		for (int ilLc = olRows.GetSize()-1;ilLc >= 0 ; ilLc--)
		{
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
			int ilDummy = (int)olRows[ilLc];
			if(ilDummy > 0)
			{
				int ilRowCount = pomPossilbeList->GetRowCount() +1;
				pomPossilbeList->InsertRows(ilRowCount,1);
				for(int illc2 = 1; illc2 <= (int) pomPossilbeList->GetColCount();illc2++)
				{
					olText = pomSelectedList->GetValueRowCol(olRows[ilLc], illc2);
					pomPossilbeList->SetStyleRange(CGXRange(ilRowCount, illc2, ilRowCount, illc2), olStyle.SetValue(olText));
				}
				pomSelectedList->RemoveRows(olRows[ilLc],olRows[ilLc]);
			}
		}
	}
}

void CAutoCovDlg::OnOK() 
{
	if (!UpdateData(TRUE))
		return;

	int ilCount = 0;

	if(!m_AssignStartDate.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1928),LoadStg(IDS_WARNING), MB_OK);
		m_AssignStartDate.SetFocus();
		return;
	}

	if(!m_AssignEndDate.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1928),LoadStg(IDS_WARNING), MB_OK);
		m_AssignEndDate.SetFocus();
		return;
	}
	if(!m_AssignStartTime.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1929),LoadStg(IDS_WARNING), MB_OK);
		m_AssignStartTime.SetFocus();
		return;
	}
	if(!m_AssignEndTime.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1929),LoadStg(IDS_WARNING), MB_OK);
		m_AssignEndTime.SetFocus();
		return;
	}

	CString olStartDate;
	CString olStartTime;
	m_AssignStartDate.GetWindowText(olStartDate);
	m_AssignStartTime.GetWindowText(olStartTime);
	
	CTime olAssignStart = DateHourStringToDate(olStartDate, olStartTime);

	CString olEndDate;
	CString olEndTime;
	m_AssignEndDate.GetWindowText(olEndDate);
	m_AssignEndTime.GetWindowText(olEndTime);
	
	CTime olAssignEnd = DateHourStringToDate(olEndDate, olEndTime);

	if(olAssignEnd <= olAssignStart)
	{
		MessageBox(LoadStg(IDS_STRING1931),LoadStg(IDS_WARNING), MB_OK);
		return;
	}

	if(imMaxDuration > 0 && ((olAssignEnd - olAssignStart).GetDays() > imMaxDuration  ))
	{
		CString olMsg;
		olMsg.Format(LoadStg(IDS_STRING1930),imMaxDuration);
		MessageBox(olMsg,LoadStg(IDS_WARNING), MB_OK);
		return;
	}

	CTime olLoadStart;
	CTime olLoadEnd;
	pogCoverageDiagram->omViewer.GetTimeFrameFromTo(olLoadStart,olLoadEnd);

	if (olAssignStart < olLoadStart)
	{
		MessageBox(LoadStg(IDS_STRING61252),LoadStg(IDS_WARNING), MB_OK);
		m_AssignStartDate.SetFocus();
		return;
	}

	if (olAssignEnd > olLoadEnd)
	{
		MessageBox(LoadStg(IDS_STRING61252),LoadStg(IDS_WARNING), MB_OK);
		m_AssignEndDate.SetFocus();
		return;
	}

	if (lmSdgu != 0)
	{
		SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(lmSdgu);
		if (prlSdg != NULL)
		{
			if(!(olAssignStart >= prlSdg->Dbeg && olAssignStart <= (prlSdg->Dbeg + CTimeSpan(7,0,0,0))))
			{
				MessageBox(LoadStg(IDS_STRING1935), LoadStg(IDS_WARNING), MB_OK);
				m_AssignStartDate.SetFocus();
				return;
			}			

			if(!(olAssignEnd >= prlSdg->Dbeg && olAssignEnd <= (prlSdg->Dbeg + CTimeSpan(7,12,0,0))))
			{
				MessageBox(LoadStg(IDS_STRING1935), LoadStg(IDS_WARNING), MB_OK);
				m_AssignEndDate.SetFocus();
				return;
			}			
		}
	}

	omStartTime = olAssignStart;
	omEndTime = olAssignEnd;

	ilCount = pomSelectedList->GetRowCount();
	CString olTotalText = "";
	omSelectedList.RemoveAll();
	omPossibleList.RemoveAll();
	for (int  ilLc = 1; ilLc <= ilCount; ilLc++)
	{
		olTotalText = "";
		for(int illc2 = 1; illc2 <= (int) pomSelectedList->GetColCount();illc2++)
		{
			olTotalText+= pomSelectedList->GetValueRowCol(ilLc, illc2);
			olTotalText+= "|";
		}
		omSelectedList.Add(olTotalText);
	}


	switch(m_ShiftSelection)
	{
	case 0 :	// High
		imHighShiftDeviation		= m_ShiftDeviation;
		imMinHighShiftCoverage		= m_MinShiftCoverage;
	break;
	case 1 :	// Standard
		imStandardShiftDeviation	= m_ShiftDeviation;
		imMinStandardShiftCoverage	= m_MinShiftCoverage;
	break;
	}

	CDialog::OnOK();
}

BOOL CAutoCovDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	int ilIndex = -1;


	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}
	polWnd = GetDlgItem(IDC_SHIFTDEVTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61368));
	}
	polWnd = GetDlgItem(IDC_MINTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61369));
	}
	polWnd = GetDlgItem(IDC_ADVANCED);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61370));
	}
	polWnd = GetDlgItem(IDC_DIRECTION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61371));
	}
	polWnd = GetDlgItem(IDC_FORWARD);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61372));
	}
	polWnd = GetDlgItem(IDC_BACKWARD);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61373));
	}
	polWnd = GetDlgItem(IDC_TOGGLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61374));
	}
	polWnd = GetDlgItem(IDC_SHIFTSEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61375));
	}
	polWnd = GetDlgItem(IDC_OVERCOV);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61376));
	}
	polWnd = GetDlgItem(IDC_UNDERCOV);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61377));
	}
	polWnd = GetDlgItem(IDC_MINICOV);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61378));
	}
	polWnd = GetDlgItem(IDC_EVOLUATE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61379));
	}
	polWnd = GetDlgItem(IDC_SIMPLEADAPT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1945));
	}
	polWnd = GetDlgItem(IDC_FASTADAPT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61380));
	}
	polWnd = GetDlgItem(IDC_ITERTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61381));
	}
	polWnd = GetDlgItem(IDC_STRATEGY);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61382));
	}
	polWnd = GetDlgItem(IDC_SIMTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61383));
	}
	polWnd = GetDlgItem(IDC_FACTORTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61384));
	}
	polWnd = GetDlgItem(IDC_ASSIGNSTARTTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61385));
	}
	polWnd = GetDlgItem(IDC_ASSIGNENDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61386));
	}

	polWnd = GetDlgItem(IDC_SHIFTCOVTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1828));
	}
	polWnd = GetDlgItem(IDC_SHIFTCOVUNIT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1829));
	}
	polWnd = this->GetDlgItem(IDC_ASSIGNMENT);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1948));
	}

	polWnd = this->GetDlgItem(IDC_BUTTON_INTERNAL);
	if (polWnd != NULL)
	{
		ogBasicData.SetWindowStat("AUTOCOV_DLG_ShowInternals",polWnd);
	}


	SetWindowText(LoadStg(IDS_STRING61389));


	m_AssignStartTime.SetWindowText(omStartTime.Format("%H:%M"));
	m_AssignEndTime.SetWindowText(omEndTime.Format("%H:%M"));
	m_AssignStartDate.SetWindowText(omStartTime.Format("%d.%m.%Y"));
	m_AssignEndDate.SetWindowText(omEndTime.Format("%d.%m.%Y"));

	pomSelectedList->SubclassDlgItem(IDC_SELECTED, this);
	pomPossilbeList->SubclassDlgItem(IDC_POSSIBLE, this);
	pomSelectedList->Initialize();
	pomPossilbeList->Initialize();

	CGXStyle olStyle;

	pomPossilbeList->LockUpdate(TRUE);
	pomPossilbeList->GetParam()->EnableUndo(FALSE);
	pomPossilbeList->GetParam()->EnableTrackColWidth(FALSE);
	pomPossilbeList->GetParam()->EnableTrackRowHeight(FALSE);
//	pomPossilbeList->GetParam()->EnableSelection(GX_SELMULTIPLE  | GX_SELSHIFT   );
	pomPossilbeList->GetParam()->SetNumberedColHeaders(FALSE);

	pomSelectedList->LockUpdate(TRUE);
	pomSelectedList->GetParam()->EnableUndo(FALSE);
	pomSelectedList->GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL);
	pomSelectedList->GetParam()->EnableTrackRowHeight(FALSE);
//	pomSelectedList->GetParam()->EnableSelection(GX_SELMULTIPLE  | GX_SELSHIFT   );
	pomSelectedList->GetParam()->SetNumberedColHeaders(FALSE);

	pomSelectedList->SetDragDropRowEnabled(true);
	pomPossilbeList->SetDragDropRowEnabled(true);

	pomPossilbeList->SetColCount(imColCount);

	pomPossilbeList->SetColWidth(0,6,30);
	pomPossilbeList->SetColWidth(0,5,30);
	pomPossilbeList->SetColWidth(0,4,30);
	pomPossilbeList->SetColWidth(0,3,30);
	pomPossilbeList->SetColWidth(0,2,30);
	pomPossilbeList->SetColWidth(0,1,90);
	
	pomPossilbeList->SetColWidth(0,0,25);


	pomSelectedList->SetColCount(imColCount);

	pomSelectedList->SetColWidth(0,6,30);
	pomSelectedList->SetColWidth(0,5,30);
	pomSelectedList->SetColWidth(0,4,30);
	pomSelectedList->SetColWidth(0,3,30);
	pomSelectedList->SetColWidth(0,2,30);
	pomSelectedList->SetColWidth(0,1,90);
	
	pomSelectedList->SetColWidth(0,0,25);

	pomPossilbeList->SetRowHeight(0, 0, 14);
		
	pomSelectedList->SetRowHeight(0, 0, 14);
		
	pomSelectedList->SetValueType(3,'U');
	pomSelectedList->SetValueType(4,'U');
	
	pomSelectedList->SetCondition(3,4,'<');
	pomSelectedList->SetCondition(4,3,'>');
		
	pomSelectedList->SetValueType(5,'U');
	pomSelectedList->SetValueType(6,'U');

	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomPossilbeList->LockUpdate(FALSE);
	pomSelectedList->LockUpdate(FALSE);
	
	pomPossilbeList->HideCols(2, imColCount); 

	CString olText;
	CString olOrgText;
	int ilCount = omPossibleList.GetSize();

	CStringArray olItemList;
	pomPossilbeList->SetRowCount(0);
	pomPossilbeList->SetRowCount(ilCount);

	pomPossilbeList->SetStyleRange(CGXRange(0,1,0,1), olStyle.SetValue(LoadStg(IDS_STRING1805)));
	CString olChoiceList = "P\nA";
	for (int  ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = omPossibleList[ilLc];

		ExtractItemList(olText, &olItemList, '|');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
		{
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
			
		}
	}
	ilCount = omSelectedList.GetSize();

	pomSelectedList->SetReadOnly(FALSE);
	pomSelectedList->SetRowCount(0);
	pomSelectedList->SetRowCount(ilCount);

	pomSelectedList->SetStyleRange(CGXRange(0,1,0,1), olStyle.SetValue(LoadStg(IDS_STRING1805)));
	pomSelectedList->SetStyleRange(CGXRange(0,2,0,2), olStyle.SetValue(LoadStg(IDS_STRING1806)));
	pomSelectedList->SetStyleRange(CGXRange(0,3,0,3), olStyle.SetValue(LoadStg(IDS_STRING1807)));
	pomSelectedList->SetStyleRange(CGXRange(0,4,0,4), olStyle.SetValue(LoadStg(IDS_STRING1808)));
	pomSelectedList->SetStyleRange(CGXRange(0,5,0,5), olStyle.SetValue(LoadStg(IDS_STRING1809)));
	pomSelectedList->SetStyleRange(CGXRange(0,6,0,6), olStyle.SetValue(LoadStg(IDS_STRING1810)));

	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = omSelectedList[ilLc];
		if (olText[olText.GetLength() - 1] == '|')
			olText = olText.Left(olText.GetLength() - 1);

		ExtractItemList(olText, &olItemList, '|');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
		{
			if(ilItem == 0)
			{
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1),
					olStyle.SetValue(olItemList[ilItem]));
			}
			else if(ilItem == 1)
			{
				olStyle.SetValue(olItemList[ilItem]);
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1),
					CGXStyle( )
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX)
					.SetChoiceList("P\nA")
					.SetValue(olItemList[ilItem]));


			}
			else
			{
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1),
					CGXStyle( ).SetValue(olItemList[ilItem])
							   .SetEnabled(TRUE)
							   .SetReadOnly(FALSE));
			}
		}
	}

	pomSelectedList->GetParam()->EnableMoveRows(true);
	
	pomPossilbeList->GetParam()->EnableMoveRows(true);

	pomSelectedList->Redraw();
	pomPossilbeList->Redraw();

	this->OnInternal();

	this->ActivateAdaption(this->m_Adaption);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


CTime CAutoCovDlg::GetStartTime()
{
	return omStartTime;
}

CTime CAutoCovDlg::GetEndTime()
{
	return omEndTime;
}

bool CAutoCovDlg::GetAdaptionFlag()
{
	return (m_Adaption == 1)?true:false;
}

int  CAutoCovDlg::GetDirection()
{
	return m_Direction;
}

int  CAutoCovDlg::GetShiftSelection()
{
	return m_ShiftSelection;
}

int  CAutoCovDlg::GetShiftDeviation()
{
	return m_ShiftDeviation;
}

int  CAutoCovDlg::GetAdaptIterations()
{
	return m_AdaptIterations;
}

int  CAutoCovDlg::GetFactor()
{
	return m_Factor;
}

int  CAutoCovDlg::GetMinShiftCoverage()
{
	return m_MinShiftCoverage;
}

bool CAutoCovDlg::UseBaseCoverage()
{
	return m_UseBaseCoverage;
}

void CAutoCovDlg::OnGridEndEditing(WPARAM wParam, LPARAM lParam)
{
	//-----------------
	// when leaving a cell, this function checks for text in the other columns of the row and 
	// removes it when necessary (There's only one column per row to be filled)
	//-----------------

	CGridFenster *polGrid = (CGridFenster*) wParam;

	CELLPOS *prlCellPos = (CELLPOS*) lParam;

	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	CString olCellText ;
	bool blIsOk = polGrid->CheckCell(ilRow, ilCol);

	if(!blIsOk)
	{
		
		CString olValue2 = polGrid->GetValueRowCol((ROWCOL)ilRow, 1);
		MessageBox(LoadStg(IDS_STRING61367) + olValue2,LoadStg(IDS_WARNING), MB_OK);
	}
}

void CAutoCovDlg::SetMaxDuration(int ipMaxDur)
{
	imMaxDuration = ipMaxDur;

}


void CAutoCovDlg::OnOvercov() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);	

	switch(m_ShiftSelection)
	{
	case 0 :	// High
		m_ShiftDeviation	= imHighShiftDeviation;
		m_MinShiftCoverage	= imMinHighShiftCoverage;
	break;
	case 1 :	// Standard
		m_ShiftDeviation	= imStandardShiftDeviation;
		m_MinShiftCoverage	= imMinStandardShiftCoverage;
	break;
	}

	UpdateData(FALSE);
}

void CAutoCovDlg::OnUndercov() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);	

	switch(m_ShiftSelection)
	{
	case 0 :	// High
		m_ShiftDeviation	= imHighShiftDeviation;
		m_MinShiftCoverage	= imMinHighShiftCoverage;
	break;
	case 1 :	// Standard
		m_ShiftDeviation	= imStandardShiftDeviation;
		m_MinShiftCoverage	= imMinStandardShiftCoverage;
	break;
	}

	UpdateData(FALSE);
}

void CAutoCovDlg::OnInternal() 
{
	CWnd *polWnd = this->GetDlgItem(IDC_BUTTON_INTERNAL);
	if (!polWnd)
		return;

	CString olText;
	polWnd->GetWindowText(olText);

	if (olText == "<<<")
	{
		olText = ">>>";
		polWnd->SetWindowText(olText);
		
		polWnd = GetDlgItem(IDC_INTERNAL);
		if (polWnd)
		{
			CRect olRect;
			GetWindowRect(olRect);
			CRect olChildRect;
			polWnd->GetWindowRect(olChildRect);
			olRect.bottom = olChildRect.top;
			MoveWindow(olRect,TRUE);
		}
	}
	else
	{
		olText = "<<<";
		polWnd->SetWindowText(olText);		
		polWnd = GetDlgItem(IDC_INTERNAL);
		if (polWnd)
		{
			CRect olRect;
			GetWindowRect(olRect);
			CRect olChildRect;
			polWnd->GetWindowRect(olChildRect);
			olRect.bottom = olChildRect.bottom + 7;
			MoveWindow(olRect,TRUE);
		}
	}
}

void CAutoCovDlg::OnFastadapt() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	ActivateAdaption(this->m_Adaption);
}

void CAutoCovDlg::OnSimpleadapt() 
{
	// TODO: Add your control notification handler code here
	CButton *polBtn = (CButton *)this->GetDlgItem(IDC_USE_BASE_COVERAGE);
	if (polBtn->GetCheck() == 0)
	{
		polBtn->SetCheck(1);
	}

	if (!UpdateData(TRUE))
		return;

	ActivateAdaption(this->m_Adaption);
	
}

void CAutoCovDlg::OnEvoluate() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	ActivateAdaption(this->m_Adaption);
	
}

void CAutoCovDlg::ActivateAdaption(int ipAdaption)
{
	CWnd *polWnd;
	switch(ipAdaption)
	{
	case 0 :
		polWnd = this->GetDlgItem(IDC_OVERCOV);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_UNDERCOV);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_USE_BASE_COVERAGE);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_ADAPTITER);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_SHIFTDEV);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_SHIFTCOV);
		polWnd->EnableWindow(FALSE);
		break;
	case 1:
		polWnd = this->GetDlgItem(IDC_OVERCOV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_UNDERCOV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_USE_BASE_COVERAGE);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_ADAPTITER);
		polWnd->EnableWindow(FALSE);
		polWnd = this->GetDlgItem(IDC_SHIFTDEV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_SHIFTCOV);
		polWnd->EnableWindow(TRUE);
		break;
	case 2:
		polWnd = this->GetDlgItem(IDC_OVERCOV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_UNDERCOV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_USE_BASE_COVERAGE);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_ADAPTITER);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_SHIFTDEV);
		polWnd->EnableWindow(TRUE);
		polWnd = this->GetDlgItem(IDC_SHIFTCOV);
		polWnd->EnableWindow(TRUE);
		break;
	}
}