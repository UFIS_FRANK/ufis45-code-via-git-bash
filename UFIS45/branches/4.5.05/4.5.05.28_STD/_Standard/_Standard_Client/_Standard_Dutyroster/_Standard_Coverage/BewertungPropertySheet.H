// flplanps.h : header file
//



#ifndef _BEWERTUNGEPROPERTYSHEET_H_
#define _BEWERTUNGEPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSBewGhsFilterPage.h>
#include <PSBewPfcFilterPage.h>
#include <PSBewGatFilterPage.h>
#include <PSBewPstFilterPage.h>
#include <PSBewCicFilterPage.h>
#include <PSBewReqFilterPage.h>




/////////////////////////////////////////////////////////////////////////////
// BewertungPropertySheet
//@Man:
//@Memo: Property sheet for the evalution filter
/*@Doc: 
*/
class BewertungPropertySheet : public BasePropertySheet
{
// Construction
public:
	BewertungPropertySheet(CString opCalledFrom,CString opCaption,FilterType ipFilterType, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BewertungPropertySheet)
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Attributes
public:
	CPsBewGhsFilterPage m_DummyPage;
	CPsBewGhsFilterPage m_PsBewGhsFilterPage;
	CPsBewPfcFilterPage m_PsBewPfcFilterPage;
	CPsBewGatFilterPage m_PsBewGatFilterPage;
	CPsBewPstFilterPage m_PsBewPstFilterPage;
	CPsBewCicFilterPage m_PsBewCicFilterPage;
	CPsBewReqFilterPage m_PsBewReqFilterPage;
private:
	CButton		m_Check1;
	CButton		m_Check2;
	CButton		m_Check3;
	CButton		m_FIdxCheck1;
	CButton		m_FIdxCheck2;
	CButton		m_FIdxCheck3;
	CStatic		m_FIdxStatic1;
	CComboBox	m_FIdxCombo1;

	bool blCheck1;
	bool blCheck2;
	bool blCheck3;

	bool bmTypeManSet;
	bool bmFidxManSet;

	FilterType imFilterType;

	CStringArray omSelectedItems;
	protected:
//{{AFX_MSG(BewertungPropertySheet)
	afx_msg void OnCheck1();
	afx_msg void OnCheck2();
	afx_msg void OnCheck3();
	afx_msg void OnFIdxCheck1();
	afx_msg void OnFIdxCheck2();
	afx_msg void OnFIdxCheck3();
	afx_msg void OnViewSelChange();
	afx_msg void OnSave();
	afx_msg void OnDelete();
	afx_msg void OnOK();
	afx_msg void OnCancel();
	afx_msg void OnApply();
	afx_msg void OnComboKillFocus();
	afx_msg	void OnFIdxCombo1SelChange();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


// Operations
public:
	//@ManMemo:Handles the loading of the filter data into the property sheet  
    /*@Doc:
		Fills the SelectedItems Arrays  with the items in the evaluation filter 
    */
	virtual void LoadDataFromViewer();
	//@ManMemo:Handles the saving of the filter data from the property sheet  
    /*@Doc:
		Writes the items from the SelectedItems Arrays  into the evaluation filter 
		opViewName = name of the selected view(filter);
		bpSaveToDb = flag if the changes should be stored in the database or not.
    */
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	private:
	//@ManMemo:Handles hiding and showing of the propertysheets  
    /*@Doc:
    */
	void DisplayPages();
protected:
	void UpdateComboBox();
	CString GetComboBoxText();
	BOOL IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2);
	BOOL IsInArray(CString &ropString, CStringArray &ropArray);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _BEWERTUNGEPROPERTYSHEET_H_
