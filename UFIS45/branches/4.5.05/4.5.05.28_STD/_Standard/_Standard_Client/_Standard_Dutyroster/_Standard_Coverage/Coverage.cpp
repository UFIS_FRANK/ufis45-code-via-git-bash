// Coverage.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <Coverage.h>
#include <CCSGlobl.h>
#include <CCSParam.h>

#include <CedaBasicData.h>

#include <MainFrm.h>
#include <CedaSystabData.h>
#include <CedaFlightData.h>
#include <CedaPfcData.h>
#include <CedaGegData.h>
#include <CedaDemData.h>
#include <CedaPerData.h>
#include <CedaBsdData.h>
#include <CedaSpfData.h>
#include <CedaSdgData.h>
#include <CedaStfData.h>
#include <CedaMsdData.h>
#include <CedaValData.h>
#include <CedaBlkData.h>
#include <CedaSgmData.h>
#include <CedaDrrData.h>
#include <CedaInitModuData.h>

#include <ListBoxDlg.h>

#include <RegisterDlg.h>
#include <LoginDlg.h>
#include <PrivList.h>

#include <LoadTimeSpanDlg.h>
#include <VersionInfo.h>
#include <AatLogin.h>

#include <GUILng.h>
#include <LibGlobl.h>

#ifndef	USE_WINHELP
#	include "AatHelp.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool bgDoTimer;
SYSTEMTIME ogSysTime;

static void InitCoverageDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olGeoFilter;
	CStringArray olRuleFilter;
	// Read the default value from the database in the server


//Dispo Viewer -------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olPossibleFilters.Add("RULES");
	olViewer.SetViewerKey("DISPODDIA");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	olViewer.SetFilter("RULES", olRuleFilter);
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//Coverage-Toolbox Viewer -------------------------------------------------------------------

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("FLIGHT");
	olPossibleFilters.Add("ALC");
	olPossibleFilters.Add("ALT");
	olPossibleFilters.Add("TEMPLATE");
	olPossibleFilters.Add("TEMPBUTTON"); 
	olPossibleFilters.Add("APT");
	olPossibleFilters.Add("APTBUTTON");
	olPossibleFilters.Add("POS");
	olViewer.SetViewerKey("COVERAGEDIA");

	olLastView = olViewer.SelectView();	// remember the current view

	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	CStringArray olFlightFilter;
	CStringArray olAlcFilter;
	CStringArray olAltFilter;
	CStringArray olAptFilter;
	CStringArray olAptButtonFilter;
	CStringArray olTempFilter;
	CStringArray olTempButtonFilter;
	CStringArray olPosFilter;
	
	olFlightFilter.Add("ARRI=1");		// Arrival
	olFlightFilter.Add("DEPA=1");		// Depature
	olFlightFilter.Add("BETR=1");		// Betrieb
	olFlightFilter.Add("PLAN=1");		// Planung
	olFlightFilter.Add("CANC=0");		// Cancelled
	olFlightFilter.Add("PROG=0");		// Prognose
	olFlightFilter.Add("NOOP=0");		// Noop

	olViewer.SetFilter("FLIGHT", olFlightFilter);
	
	olAptButtonFilter.Add("ORIGIN=1");
	olAptButtonFilter.Add("DEST=1");

	olViewer.SetFilter("APTBUTTON", olAptButtonFilter);

	olTempButtonFilter.Add("NOTFLUG=1");
	olTempButtonFilter.Add("FLUG=1");

	olViewer.SetFilter("TEMPBUTTON", olTempButtonFilter);

	olAlcFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("ALC", olAlcFilter);

	olAptFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("APT", olAptFilter);

	olAltFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("ALT",olAltFilter);

	olTempFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("TEMPLATE",olTempFilter);

	olPosFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("POS", olPosFilter);


	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
 



	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("BSH");

	olViewer.SetViewerKey("BASISSCHICHTEN");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");


	CStringArray olBshFilter;

	olBshFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BSH",olBshFilter);


	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
 


	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");

// Auswahl Buttons
	olPossibleFilters.Add("BEWSELBTN");
// Auswahl Buttons
	olPossibleFilters.Add("BEWFIDXBTN");
// Personal
	olPossibleFilters.Add("BEWGHS");
	olPossibleFilters.Add("BEWPFC");
// Equipment
	olPossibleFilters.Add("BEWEQU");
// Location Type
	olPossibleFilters.Add("BEWLCIC");
	olPossibleFilters.Add("BEWLPST");
	olPossibleFilters.Add("BEWLGAT");

	olViewer.SetViewerKey("BEWERTUNG");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	CStringArray olSelButtons;
	CStringArray olFIdxButtons;
	CStringArray olGhsFilter;
	CStringArray olPfcFilter;
	CStringArray olEquFilter;
	CStringArray olLTypFilter;
	CStringArray olLTypFilter2;
	CStringArray olLDefFilter;
	
	olSelButtons.Add("PERS=1");		// Personal
	olSelButtons.Add("EQUI=0");		// Equipment
	olSelButtons.Add("LTYP=0");		// Location Type


	olViewer.SetFilter("BEWSELBTN", olSelButtons);
	
	olFIdxButtons.Add("FTYP=0");		// Personal
	CString olEquType;
	olEquType.Format("EQUTYP=%s",LoadStg(IDS_STRING61216));
	olFIdxButtons.Add(olEquType);		// Equipment type
	olViewer.SetFilter("BEWFIDXBTN", olFIdxButtons);


	olGhsFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWGHS", olGhsFilter);

	olPfcFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWPFC", olPfcFilter);

	olEquFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWEQU", olEquFilter);

	olLTypFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWLCIC", olLTypFilter);

	olLTypFilter2.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWLPST", olLTypFilter);

	olLDefFilter.Add(LoadStg(IDS_STRING61216));
	olViewer.SetFilter("BEWLGAT", olLDefFilter);



	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
 


	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//ShiftRoster Viewer -------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("SHIFTVIEW");
	olPossibleFilters.Add("SHIFTVIEW2");
	olViewer.SetViewerKey("ID_SHEET_SHIFT_VIEW");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//DutyRoster Viewer -------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUTYVIEW");
	olPossibleFilters.Add("DUTYVIEW2");
	olPossibleFilters.Add("DUTYVIEW3");
	olPossibleFilters.Add("DUTYVIEW4");
	olPossibleFilters.Add("DUTYVIEW5");
	olViewer.SetViewerKey("ID_SHEET_DUTY_VIEW");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//?-------------------------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("POSDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//GhsList Viewer -------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GHSLIST");
	olViewer.SetViewerKey("GHSLIST");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GHSLIST",olRuleFilter);
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
//-------------------------------------------------------------------------------------
	olPossibleFilters.RemoveAll();
	olGeoFilter.RemoveAll();
	olRuleFilter.RemoveAll();
	
}


/////////////////////////////////////////////////////////////////////////////
// CCoverageApp

BEGIN_MESSAGE_MAP(CCoverageApp, CWinApp)
	//{{AFX_MSG_MAP(CCoverageApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoverageApp construction

CCoverageApp::CCoverageApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CCoverageApp::~CCoverageApp()
{
	delete CGUILng::TheOne();
}
/////////////////////////////////////////////////////////////////////////////
// The one and only CCoverageApp object

CCoverageApp theApp;

void CCoverageApp::InitialLoad()
{
	bgDoTimer = false;
	bool  blErr  = true;
	int   ilErr = 0;
	char pclConfigPath[142];
	char pclCurrentDate[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	GetPrivateProfileString(pcgAppName, "CURRENTDATE", "",
        pclCurrentDate, sizeof pclCurrentDate, pclConfigPath);
	 if (*pclCurrentDate != '\0')
	 {

		 SYSTEMTIME olTmpTime;

		GetSystemTime(&ogSysTime);
		GetSystemTime(&olTmpTime);
		olTmpTime.wYear = atoi(&pclCurrentDate[6]);
		olTmpTime.wMonth = atoi(&pclCurrentDate[3]);
		olTmpTime.wDay = atoi(&pclCurrentDate[0]);
	
		SetSystemTime(&olTmpTime);
			
	 }


	pogInitialLoad = new CInitialLoadDlg();
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1362));
	}

	CWaitCursor olWait;

	bgIsInitialized = false;

	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("COV Performance START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
/////////////////////////////////////////////////////////////////////////////////////
// add here your cedaXXXdata read methods
/////////////////////////////////////////////////////////////////////////////////////
//--------------------------------------------------------------------------------------
	ogAllBsdData.SetTableName(CString(CString("BSD") + CString(pcgTableExt)));
	ogBsdData.SetTableName(CString(CString("BSD") + CString(pcgTableExt)));
	//ogBsdData.SetHomeAirport(CString(pcgHome));
	ogGegData.SetTableName(CString(CString("GEG") + CString(pcgTableExt)));
	//ogGegData.SetHomeAirport(CString(pcgHome));
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogPerData.SetTableName(CString(CString("PER") + CString(pcgTableExt)));
	//ogPerData.SetHomeAirport(CString(pcgHome));
	ogPfcData.SetTableName(CString(CString("PFC") + CString(pcgTableExt)));
	//ogPfcData.SetHomeAirport(CString(pcgHome));
	ogMsdData.SetTableName(CString(CString("MSD") + CString(pcgTableExt)));
	ogMsdData.omServerName = ogCommHandler.pcmRealHostName;
	//ogSdtData.SetHomeAirport(CString(pcgHome));
	ogSdtData.SetTableName(CString(CString("SDT") + CString(pcgTableExt)));
	ogSdtData.omServerName = ogCommHandler.pcmRealHostName;

	//ogSdtData.SetHomeAirport(CString(pcgHome));
	ogSdgData.SetTableName(CString(CString("SDG") + CString(pcgTableExt)));
	//ogSdgData.SetHomeAirport(CString(pcgHome));
	ogSpfData.SetTableName(CString(CString("SPF") + CString(pcgTableExt)));
	ogSpfData.omServerName = ogCommHandler.pcmRealHostName;
	//ogSpfData.SetHomeAirport(CString(pcgHome));
	ogStfData.SetTableName(CString(CString("STF") + CString(pcgTableExt)));
	//ogStfData.SetHomeAirport(CString(pcgHome));
	ogSgmData.omServerName = ogCommHandler.pcmRealHostName;

	ogDrrData.SetTableName(CString(CString("DRR") + CString(pcgTableExt)));
	ogDrrData.omServerName = ogCommHandler.pcmRealHostName;

	GetSystemTime(&rlPerf);
	TRACE("RMS Performance END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
//--------------------------------------------------------------------------------------
//Update Sync because a crashed application could have set this state
	CTime olCurrentTime; 
	CTime olToTime;
	olCurrentTime = CTime::GetCurrentTime();
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";

//SYNC END
//--------------------------------------------------------------------------------------

	ogDemData.AttachCedaFlightData(&ogCovFlightData);

	ogCfgData.ReadCfgData();
	ogCfgData.ReadMonitorSetup();
// Start Reading

	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetObject("APT", "URNO,TICH,TDI1,TDI2,APC3,APC4,LAND,APFN" );

	if (!ogBasicData.SetLocalDiff())
	{
		if (AfxMessageBox(LoadStg(IDS_STRING123),MB_ICONSTOP|MB_YESNO) == IDNO)
		{
			olWait.Restore();
			AfxAbort();
		}

		olWait.Restore();
	}

	int ilPercent = 100 / (12+18); //Teiler ist gleich Anzahl der zu lesenden Tabellen

	CString olWhere = "";
// FSC insert wegen JobDlg
	ogBCD.SetObject("AFT");
	//unnecessary, this map is set by default ogBCD.AddKeyMap("AFT", "URNO");

	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();

	ogBCD.SetObject("TSR");
	olWhere.Format(" WHERE BTAB = 'AFT' AND OPER = '0' ");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1867));
    ilErr = ogBCD.Read("TSR",olWhere);
	pogInitialLoad->SetProgress(ilPercent);

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(TSR) %.3lf sec\n", flDiff/1000.0 );

/****
	ogBCD.SetObject("ACR", "URNO,REGN,ACT3,ACT5,APUI");
	ogBCD.SetObjectDesc("ACR", LoadStg(IDS_STRING1363));
	ogBCD.AddKeyMap("ACR", "REGN");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1868));
	ogBCD.Read("ACR");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(ACR) %.3lf sec\n", flDiff/1000.0 );
*****/

	ogBCD.SetObject("GPL", "URNO,GSNM,SPLU,VAFR,VATO",true,GplBcFunc);
	ogBCD.SetDdxType("GPL", "IRT", GPL_NEW);
	ogBCD.SetDdxType("GPL", "URT", GPL_CHANGE);
	ogBCD.SetDdxType("GPL", "DRT", GPL_DELETE);

	ogBCD.SetObject("GSP", "URNO,GPLU,BUFR,BUSA,BUSU,BUMO,BUTH,BUTU,BUWE,NSTF,SUFR,SUSA,SUSU,SUMO,SUTH,SUTU,SUWE,P1FR,P1SA,P1SU,P1MO,P1TH,P1TU,P1WE,P2FR,P2SA,P2SU,P2MO,P2TH,P2TU,P2WE" );
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1869));
	ogBCD.Read("GSP");
	pogInitialLoad->SetProgress(ilPercent);
	ogBCD.SetDdxType("GSP", "IRT", GSP_NEW);
	ogBCD.SetDdxType("GSP", "URT", GSP_CHANGE);
	ogBCD.SetDdxType("GSP", "DRT", GSP_DELETE);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(GSP) %.3lf sec\n", flDiff/1000.0 );

	ogBCD.SetObject("SPL", "URNO,SNAM");
	ogBCD.AddKeyMap("SPL", "SNAM");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1870));
	ogBCD.Read("SPL");
	pogInitialLoad->SetProgress(ilPercent);
	ogBCD.SetDdxType("SPL", "IRT", SPL_NEW);
	ogBCD.SetDdxType("SPL", "URT", SPL_CHANGE);
	ogBCD.SetDdxType("SPL", "DRT", SPL_DELETE);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SPL) %.3lf sec\n", flDiff/1000.0 );
	
	CTime olLoadStart, olLoadEnd;
	ogBasicData.GetDiagramStartTime(olLoadStart, olLoadEnd);

	
	olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s'", olLoadStart.Format("%Y%m%d"), olLoadEnd.Format("%Y%m%d"));

	blErr = ogBCD.SetObject("DRS","URNO,HOPO,STFU,SDAY,DRRU,ATS1,STAT",true,DrrBcFunc);
	ogBCD.AddKeyMap("DRS", "SDAY");
	ogBCD.AddKeyMap("DRS", "STFU");
	ogBCD.AddKeyMap("DRS", "DRRU");
	
	blErr = ogBCD.SetObject("DRD","URNO,HOPO,STFU,SDAY,DRRN,DRDT,DRDF,FCTC",true,DrrBcFunc);
	ogBCD.AddKeyMap("DRD", "SDAY");
	ogBCD.AddKeyMap("DRD", "STFU");
	ogBCD.AddKeyMap("DRD", "DRRN");


	blErr = ogBCD.SetObject("DRA","URNO,HOPO,SDAC,STFU,SDAY,DRRN,ABFR,ABTO",true,DrrBcFunc);
	ogBCD.AddKeyMap("DRA", "SDAY");
	ogBCD.AddKeyMap("DRA", "STFU");
	ogBCD.AddKeyMap("DRA", "DRRN");


	blErr = ogBCD.SetObject("DEL","URNO,HOPO,FCTC,DPT1,DELT,DELF,BSDU"); 
	ogBCD.AddKeyMap("DEL", "BSDU");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1872));
    ilErr = ogBCD.Read("DEL");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(DEL) %.3lf sec\n", flDiff/1000.0 );

/*
	blErr = ogBCD.SetObject("REG");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1874));
    ilErr = ogBCD.Read("REG");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(REG) %.3lf sec\n", flDiff/1000.0 );
*/
	if (ogBasicData.IsEquipmentAvailable())
	{
		blErr = ogBCD.SetObject("EQU");
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1877));
		ilErr = ogBCD.Read("EQU");
		pogInitialLoad->SetProgress(ilPercent);
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("Coverage::InitialLoad:Read(EQU) %.3lf sec\n", flDiff/1000.0 );
	}

	blErr = ogBCD.SetObject("CIC");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1878));
    ilErr = ogBCD.Read("CIC");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(CIC) %.3lf sec\n", flDiff/1000.0 );

/***** duplicated 
	blErr = ogBCD.SetObject("PST");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1879));
    ilErr = ogBCD.Read("PST");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(PST) %.3lf sec\n", flDiff/1000.0 );
*******/

	blErr = ogBCD.SetObject("GAT");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1880));
    ilErr = ogBCD.Read("GAT");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(GAT) %.3lf sec\n", flDiff/1000.0 );

	blErr = ogBCD.SetObject("SGR");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1881));
    ilErr = ogBCD.Read("SGR");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SGR) %.3lf sec\n", flDiff/1000.0 );

	blErr = ogBCD.SetObject("SER", "URNO,SNAM");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1882));
    ilErr = ogBCD.Read("SER");
	pogInitialLoad->SetProgress(ilPercent);
	//unnecessary, this map is set by default ogBCD.AddKeyMap("SER", "URNO");
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SER) %.3lf sec\n", flDiff/1000.0 );

	ogCCSParam.BufferParams(ogGlobal);
	//Parameter mittels CCSParam Klasse einlesen
	LoadParameters();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1844));
	pogInitialLoad->SetProgress(ilPercent);

	//pogInitialLoad->SetProgress(2);

	ogCfgData.SetCfgData();
	ogPfcData.Read();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(PFC) %.3lf sec\n", flDiff/1000.0 );
	pogInitialLoad->SetProgress(ilPercent);
	ogGegData.Read();
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(GEG) %.3lf sec\n", flDiff/1000.0 );
	// Load Non-flight premisses
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1367));
	ogBCD.SetObject("ACT", "URNO,ACT3,ACT5,ACFN");
	ogBCD.Read("ACT");
	pogInitialLoad->SetProgress(ilPercent);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1368));
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(ACT) %.3lf sec\n", flDiff/1000.0 );

/*	duplicate 
	ogBCD.SetObject("APT");
	*/
	ogBCD.Read("APT");
	pogInitialLoad->SetProgress(ilPercent);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1369));
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(APT) %.3lf sec\n", flDiff/1000.0 );
	ogBCD.SetObject("ALT","URNO,ALC2,ALC3,ALFN");
	ogBCD.Read("ALT");
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(ALT) %.3lf sec\n", flDiff/1000.0 );
	//MWO: 03.04.2003
	CTime olLoadStartUtc(olLoadStart);
	CTime olLoadEndUtc(olLoadEnd);
	ogBasicData.LocalToUtc(olLoadStartUtc);
	ogBasicData.LocalToUtc(olLoadEndUtc);
	//END MWO: 03.04.2003
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1380));
	ogPerData.Read();
	pogInitialLoad->SetProgress(ilPercent);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(PER) %.3lf sec\n", flDiff/1000.0 );
	ogStfData.Read();
	pogInitialLoad->SetProgress(ilPercent);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1383));
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(STF) %.3lf sec\n", flDiff/1000.0 );
	char pclWhere[2000];
	sprintf(pclWhere," WHERE (VATO >= '%s' OR VATO = ' ' ) AND VAFR <= '%s'", olLoadStart.Format("%Y%m%d%H%M00"), olLoadEnd.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("BSDTAB", "VAFR", "VATO", olLoadStart.Format("%Y%m%d%H%M%S"), olLoadEnd.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03
	ogBsdData.Read(pclWhere);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(BSD) %.3lf sec\n", flDiff/1000.0 );
	ogAllBsdData.Read();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(BSD ALL) %.3lf sec\n", flDiff/1000.0 );
	ogBsdData.Register();
	pogInitialLoad->SetProgress(ilPercent);

	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1673));
	ogSpfData.Read();
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SPF) %.3lf sec\n", flDiff/1000.0 );

	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1792));
	ogValData.ReadValData(olLoadStartUtc,olLoadEndUtc);
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(VAL) %.3lf sec\n", flDiff/1000.0 );

	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1793));
	ogBlkData.ReadBlkData(olLoadStartUtc,olLoadEndUtc);
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(BLK) %.3lf sec\n", flDiff/1000.0 );

	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1794));
	ogSgmData.ReadSgmData(olLoadStartUtc,olLoadEndUtc);
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SGM) %.3lf sec\n", flDiff/1000.0 );

	if (ogBasicData.IsEquipmentAvailable())
	{
		ogBCD.SetObject("EQT","URNO,CDAT,NAME");		
		ogBCD.Read("EQT");
		//unnecessary, this map is set by default  ogBCD.AddKeyMap("EQT","URNO");
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("Coverage::InitialLoad:Read(EQT) %.3lf sec\n", flDiff/1000.0 );
	}

	// Positionen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1388));
	ogBCD.SetObject("PST", "URNO,PNAM");
	ogBCD.SetObjectDesc("PST", LoadStg(IDS_STRING1389));
	ogBCD.Read(CString("PST"));
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(PST) %.3lf sec\n", flDiff/1000.0 );

	ogBCD.SetObject("PAX");
	ogBCD.AddKeyMap("PAX", "AFTU");
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(PAX) %.3lf sec\n", flDiff/1000.0 );


	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1397));
	ogBcHandle.SetFilterRange("SDGTAB", "BEGI", "ENDE", olLoadStart.Format("%Y%m%d%H%M%S"), olLoadEnd.Format("%Y%m%d%H%M%S"));
	sprintf(pclWhere," WHERE ENDE >= '%s' AND BEGI <= '%s'", olLoadStart.Format("%Y%m%d%H%M00"), olLoadEnd.Format("%Y%m%d%H%M00"));
	ogSdgData.Read(pclWhere);
	pogInitialLoad->SetProgress(ilPercent);
	llNow  = GetTickCount(); 
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SDG) %.3lf sec\n", flDiff/1000.0 );


	sprintf(pclWhere," WHERE ESBG BETWEEN '%s' AND '%s' OR LSEN BETWEEN '%s' AND '%s'", olLoadStart.Format("%Y%m%d%H%M00"),olLoadEnd.Format("%Y%m%d%H%M00"),olLoadStart.Format("%Y%m%d%H%M00"),olLoadEnd.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("SDTTAB", "ESBG", "LSEN", olLoadStart.Format("%Y%m%d%H%M%S"), olLoadEnd.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03
	ogSdtData.Read(pclWhere);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SDT) %.3lf sec\n", flDiff/1000.0 );
	
	sprintf(pclWhere," WHERE SDGU IN (SELECT URNO FROM SDGTAB WHERE ENDE >= '%s')",olLoadStart.Format("%Y%m%d%H%M00"));
	ogMsdData.Read(pclWhere);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(MSD) %.3lf sec\n", flDiff/1000.0 );
	//MWO ???
	ogMsdData.Register();
	//END MWO ???
	ogBasicData.ReadAllConfigData();

//-- End Reading

/******************************/
//Daten f�r die Coverage werden geladen

	ogCoverageConflictData.DeleteAllData();
	ogDemData.AttachCedaFlightData(&ogCovFlightData);

	olWhere.Format(" WHERE (VPTO >= '%s' OR VPTO = ' ' ) AND VPFR <= '%s'", olLoadStart.Format("%Y%m%d%H%M00"), olLoadEnd.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("SPETAB", "VPFR", "VPTO", olLoadStart.Format("%Y%m%d%H%M%S"), olLoadEnd.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03
	ogBCD.SetObject("SPE","",true,SpeBcFunc);
	ogBCD.AddKeyMap("SPE", "SURN");
    ilErr = ogBCD.Read("SPE",olWhere);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(SPE) %.3lf sec\n", flDiff/1000.0 );

	olWhere = "";


	ogBasicData.LocalToUtc(olLoadStart);
	ogBasicData.LocalToUtc(olLoadEnd);
	CString T1 = olLoadStart.Format("%d.%m.%Y-%H:%M");
	CString T2 = olLoadEnd.Format("%d.%m.%Y-%H:%M");

	CString olOrg3 = "";
	olOrg3.Format(" AND DES3 = '%s')",pcgHome);
	olWhere += olLoadStart.Format("(TIFA >= '%Y%m%d%H%M00' AND TIFA <=") + olLoadEnd.Format("'%Y%m%d%H%M00'") +olOrg3 ;
/***************	
	olWhere += olLoadStart.Format("((TIFA BETWEEN '%Y%m%d%H%M00' AND ") + olLoadEnd.Format("'%Y%m%d%H%M00'") +olOrg3  +
				olLoadStart.Format(" OR (STOA BETWEEN '%Y%m%d%H%M00' AND ") + olLoadEnd.Format("'%Y%m%d%H%M00'") + olOrg3 + CString(")");
	*********/
	/*************/
	olWhere += " OR ";

	CString olDes3 = "";
	olDes3.Format(" AND ORG3 = '%s')",pcgHome);
	olWhere += olLoadStartUtc.Format("(TIFD >= '%Y%m%d%H%M00' AND TIFD <=") + olLoadEndUtc.Format("'%Y%m%d%H%M00'") + olDes3 ;
/****************
	olWhere += olLoadStart.Format("((TIFD BETWEEN '%Y%m%d%H%M00' AND ") + olLoadEnd.Format("'%Y%m%d%H%M00'") + olDes3  + 
				olLoadStart.Format(" OR (STOD BETWEEN '%Y%m%d%H%M00' AND ") + olLoadEnd.Format("'%Y%m%d%H%M00'") + olDes3 +CString(")");
**********/
	if(!olWhere.IsEmpty())
	{
		olWhere = CString("(") + olWhere + CString(")");
	}

	olWhere += CString(" AND FLNO <> ' '");
	ogCovFlightData.ClearAll();
	
	ogCovFlightData.ReadRotations(olWhere.GetBuffer(0), true);

	ogCovFlightData.SetPreSelection(olLoadStart, olLoadEnd,CString(""), true);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(AFT) %.3lf sec\n", flDiff/1000.0 );

	LoadRules (true);
	llLast  = GetTickCount();

	//------------------------------------------------------------------------------

	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("DEMTAB", "DEBE", "DEEN", olLoadStart.Format("%Y%m%d%H%M%S"), olLoadEnd.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03
	ogDemData.omServerName = ogCommHandler.pcmRealHostName;
	ogDemData.GetLoadFormat();
	ogDemData.Read(olLoadStart,olLoadEnd,omLoadedTplUrnos);
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Coverage::InitialLoad:Read(DEM) %.3lf sec\n", flDiff/1000.0 );
	 
	ogDemData.SetDispoTimeFrame(olLoadStart,olLoadEnd);	

	//Nyan 
	//if ("ADR" == ogCCSParam.GetParamValue("ROSTER","ID_PROD_CUSTOMER",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true))
	if ("Y" == ogCCSParam.GetParamValue("COVERAGE","ID_SAVE_DEMANDS",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true))
	{
		// coverage snapshot header
		ogBCD.SetObject("CSH");
		// coverage snapshot detail
		ogBCD.SetObject("CSD");
	}




////Daten f�r die Coverage laden ende
/***********************************/ 
	//pogInitialLoad->SetProgress(100);

	InitCoverageDefaultView();

	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}

	bgDoTimer = true;

}

/////////////////////////////////////////////////////////////////////////////
// CCoverageApp initialization
BOOL CCoverageApp::InitInstance()
{
	AfxEnableControlContainer();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}


	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}

	//END MWO/RRO 25.03.03

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));


    InitFont();
	CreateBrushes();

	GXInit();

	char pclHomeAirport[100]="";
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "FRA", pclHomeAirport, sizeof(pclHomeAirport), pclConfigPath);

	strcpy(pcgHome,pclHomeAirport);
	//ogAppName="XXX";
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHomeAirport);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	ogCommHandler.SetAppName(ogAppName);

    //GetPrivateProfileString("UClassesEnv", "LANGUAGE", "ENGLISH", pcgLanguage, sizeof pcgLanguage, pclConfigPath);
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }


	// Initialisierung der GUI-Sprache // FSC 03.02.2000
	// in ceda.ini einf�gen !!!!!!!!
	// ;'DE','US','IT' oder 'DE,Test' ect.
	// LANGUAGE=DE
	char pclTmp[128];
//	extern CString ogCurrLng;
	CGUILng* ogGUILng = CGUILng::TheOne();

	//MWO 01.08.03 bei der autocoverage ersteckt sich der function call in so
	//viele Stufen, da� ich einfach hier einglobales objekt brauch => PRF 4840
	pogProgressBarDlg = NULL;
	//END MWO 01.08.03 

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, pclConfigPath);
	ogGUILng->SetLanguage(CString(pclTmp));	

    GetPrivateProfileString(ogAppName, "DBParam", "", pclTmp, sizeof pclTmp, pclConfigPath);
	ogGUILng->SetStoreInDB(CString(pclTmp));	

//--------------------------------------------------------------------------------------
// Register your broadcasts here

	CString olTableName;
	CString olTableExt = CString( pcgTableExt);

	olTableName = CString("ALT") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_ALT_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_ALT_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_ALT_DELETE,	 false,ogAppName);

	olTableName = CString("GHS") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"), BC_GHS_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"), BC_GHS_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"), BC_GHS_DELETE, true);

	olTableName = CString("NFM") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"), BC_NFM_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"), BC_NFM_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"), BC_NFM_DELETE, true);
	
	olTableName = CString("NFP") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"), BC_NFP_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"), BC_NFP_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"), BC_NFP_DELETE, true);
/******************
	olTableName = CString("INF") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"), BC_INF_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"), BC_INF_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"), BC_INF_DELETE, true);
**********/	
	olTableName = CString("PFC") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_PFC_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_PFC_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_PFC_DELETE, true);

	olTableName = CString("STF") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_STF_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_STF_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_STF_DELETE, true);
	
	olTableName = CString("AFT") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("RAC"), BC_FLIGHT_CHANGE, true);

	olTableName = CString("SDB") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("IFR"), BC_FLIGHT_CHANGE, true);
/*
	olTableName = CString("ARC") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("IFR"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);
*/ 
	ogBcHandle.AddTableCommand(CString("CLOTAB"), CString("CLO"), BC_SERVER_SHUTDOWN, true);


	ogBcHandle.AddTableCommand(CString("SNGDSR"),  CString("SBC"), BC_RELOAD_SINGLE_DSR, true);
	ogBcHandle.AddTableCommand(CString("MULDSR"),  CString("SBC"), BC_RELOAD_MULTI_DSR,  true);
	ogBcHandle.AddTableCommand(CString("RELDSR"),  CString("SBC"), BC_RELDSR,            true);

	olTableName = CString("ACC") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_ACC_NEW,		 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_ACC_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_ACC_DELETE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(CString("RELACC"),		  CString("SBC"), BC_RELACC,		 false,ogAppName);

	olTableName = CString("DEM") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_DEM_NEW,		 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_DEM_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_DEM_DELETE,	 false,ogAppName);
	
	olTableName = CString("BSD") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_BSD_NEW,		 true);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_BSD_CHANGE,	 true);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_BSD_DELETE,	 true);
	
	olTableName = CString("SDT") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_SDT_NEW,		 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_SDT_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_SDT_DELETE,	 false,ogAppName);
	
	olTableName = CString("VAL") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_VAL_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_VAL_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_VAL_DELETE,	 false,ogAppName);
	
	olTableName = CString("BLK") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_BLK_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_BLK_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_BLK_DELETE,	 false,ogAppName);
	
	olTableName = CString("SGM") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_SGM_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_SGM_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_SGM_DELETE,	 false,ogAppName);
	
	olTableName = CString("MSD") + olTableExt;
//	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), MSD_NEW,		 false);
//	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), MSD_CHANGE,	 false);
//	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), MSD_DELETE,	 false);
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_MSD_NEW,		 true);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_MSD_CHANGE,	 true);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_MSD_DELETE,	 true);
	
	olTableName = CString("DRR") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_DRR_NEW,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_DRR_CHANGE, false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_DRR_DELETE, false,ogAppName);

	ogBcHandle.AddTableCommand(CString("ENDRLR"),		  CString("SBC"), ENDDRR_RELEASE,		true);
	ogBcHandle.AddTableCommand(CString("RELSDG"),		  CString("SBC"), BC_RELSDG,			false,ogAppName);
	ogBcHandle.AddTableCommand(CString("RELMSD"),		  CString("SBC"), BC_RELMSD,			false,ogAppName);
	ogBcHandle.AddTableCommand(CString("RELDRR"),		  CString("SBC"), BC_RELDRR,            true);
	ogBcHandle.AddTableCommand(CString("RELDEM"),		  CString("SBC"), BC_RELDEM,            true); 
	ogBcHandle.AddTableCommand(CString("RELPBO"),		  CString("SBC"), BC_RELPBO,			false,ogAppName);
	ogBcHandle.AddTableCommand(CString("UPDDEM"),		  CString("SBC"), BC_UPDDEM,			true);

	olTableName = CString("SDG") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,				  CString("IRT"), BC_SDG_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("URT"), BC_SDG_CHANGE,	 false,ogAppName);
	ogBcHandle.AddTableCommand(olTableName,				  CString("DRT"), BC_SDG_DELETE,	 false,ogAppName);

/*******/
	olTableName = CString("AFT") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName, 			  CString("CFL"), BC_CFL_READY,	 true);

	
	// Versionsinformation ermitteln
	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
	CCSCedaData::bmVersCheck = true;
	strncpy(CCSCedaData::pcmVersion,rlInfo.omFileVersion,sizeof(CCSCedaData::pcmVersion));
	CCSCedaData::pcmVersion[sizeof(CCSCedaData::pcmVersion)-1] = '\0';

	strncpy(CCSCedaData::pcmInternalBuild,rlInfo.omPrivateBuild.Right(4),sizeof(CCSCedaData::pcmInternalBuild));
	CCSCedaData::pcmInternalBuild[sizeof(CCSCedaData::pcmInternalBuild)-1] = '\0';

//--------------------------------------------------------------------------------------

	//Because in Coverage Demands need DDX & Broadcasts
	ogDemData.Register();

	ogSdtData.Register();

	ogSdgData.Register();

		// templates (Vorlagen)
	ogCfgData.ReadUfisCedaConfig();


	// Help initialization
#ifndef	USE_WINHELP
	if (!AatHelp::Initialize("COVERAGE"))
	{
		CString olError;
		AatHelp::GetLastError(olError);
		MessageBox(NULL,olError,"Error",MB_OK);
	}
#endif

	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_LISTBOX_DLG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());
	ogBasicData.omUserID = pcgUser;

	ogCommHandler.SetUser(pcgUser);
	strcpy(CCSCedaData::pcmUser, pcgUser);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());


	// Login Zeit setzen
	//uhi 11.10.00
	ogLoginTime = CTime::GetCurrentTime();

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.

//Reading Templates before LoadTimeSpanDlg
	ogBCD.SetObject("TPL", "URNO,FLDA,FLDD,FLDT,TNAM,TYPE,TPST",true,TplBcFunc);
    ogBCD.Read("TPL","where TPST = '1' and APPL like 'RULE%'");
	//unnecessary, this map is set by default  ogBCD.AddKeyMap("TPL", "URNO");


	CLoadTimeSpanDlg olDlg(&olDummyDlg,ogBasicData.omLoadTimeValues,(ogBasicData.GetLoadRelFlag())?1:0);

	if(olDlg.DoModal() == IDOK)
	{
		olDummyDlg.DestroyWindow();
		ogBasicData.omLoadTimeValues.RemoveAll();
		ogBasicData.omValidTemplates.RemoveAll();
		for(int illc = 0; illc < olDlg.omValues.GetSize() ;illc++)
		{
			ogBasicData.omLoadTimeValues.Add(olDlg.omValues.GetAt(illc));
		}
		for( illc = 0; illc < olDlg.omTplUrnos.GetSize() ;illc++)
		{
			ogBasicData.omValidTemplates.Add(olDlg.omTplUrnos.GetAt(illc));
		}
		ogBasicData.SetLoadRelFlag((olDlg.m_AbsTimeVal == 0)?false:true);

		ogBasicData.SetDiagramStartTime(olDlg.omStartTime, olDlg.omEndTime);
	}
	else
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}
	
	InitialLoad();

	// Pr�ft die geladenen Parameter
	CheckParameters();

	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	// create and load the frame with its resources

	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);

	// The one and only window has been initialized, so show and update it.
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CCoverageApp message handlers



/////////////////////////////////////////////////////////////////////////////
// CCoverageApp message handlers


int CCoverageApp::ExitInstance() 
{
#ifndef	USE_WINHELP
	AatHelp::ExitApp();
#endif
	DeleteBrushes();
	if (ogSysTime.wYear != 0)
	{

		SYSTEMTIME olTmpTime;

		GetSystemTime(&olTmpTime);

		olTmpTime.wYear = ogSysTime.wYear;
		olTmpTime.wMonth = ogSysTime.wMonth;
		olTmpTime.wDay = ogSysTime.wDay;

		SetSystemTime(&olTmpTime);
			
	}
	
	// MWO/RRO 26.03.03
	ogBcHandle.ExitApp();
	// END MWO/RRO 26.03.03

	return CWinApp::ExitInstance();
}

void CCoverageApp::LoadParameters()
{
	CString olStringNow = ("19501010101010");
	
	CString olDefaultValidFrom	= olStringNow;
	CString olDefaultValidTo	= "";
	
	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	ogCCSParam.GetParam(ogGlobal,"ID_LAST_INPUT",olStringNow,"25",LoadStg(IDS_STRING1894),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	ogCCSParam.GetParam(ogGlobal,"ID_OPERATION_FL",olStringNow,"N",LoadStg(IDS_STRING1895),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	ogCCSParam.GetParam(ogGlobal,"ID_NEXT_MONTH",olStringNow,"2",LoadStg(IDS_STRING1910),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	ogCCSParam.GetParam("ROSTER","ID_PROD_CUSTOMER",olStringNow,"UNDEF",LoadStg(IDS_STRING1938),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	ogCCSParam.GetParam("COVERAGE","ID_SAVE_DEMANDS",olStringNow,"N",LoadStg(IDS_STRING1949),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);

}

void CCoverageApp::CheckParameters()
{
	//--------------------------------------------------------------------------------
	// Sollten neue Parameter eingef�gt worden sein, Meldung ausgeben.
	CStringList* polMessageList = ogCCSParam.GetMessageList();
	if (polMessageList->GetCount() != 0)
	{
		CListBoxDlg olDlg(polMessageList, LoadStg(IDS_STRING1846),LoadStg(IDS_STRING1847));   
		olDlg.DoModal();
	}
	//--------------------------------------------------------------------------------
}

//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCoverageApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Versionsinformation ermitteln
	VersionInfo rlInfo;
//	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	CString olVersionString;
	olVersionString.Format("%s %s %s  Compiled: %s",rlInfo.omProductName,rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

	// Statics einstellen
	m_Copyright1.SetWindowText(rlInfo.omProductVersion);
	m_Copyright2.SetWindowText(olVersionString);
	m_Copyright3.SetWindowText(rlInfo.omLegalCopyright);
	m_Copyright4.SetWindowText(rlInfo.omCompanyName);
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));

	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);

	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));

	SetWindowText(LoadStg(IDS_COVERAGE_ABOUT));

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoverageApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
#ifdef	USE_WINHELP
	CWinApp::WinHelp(dwData,nCmd);
#else
	AatHelp::WinHelp(dwData, nCmd);
#endif
}

BOOL CCoverageApp::DrrBcFunc(BcStruct *prpBc)
{
	if (pogCoverageDiagram == NULL)
		return -1;

	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "SDAY")
		{
			ilIndex = i;
			break;
		}
	}

	CString olSday;
	if (ilIndex >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olSday = olTmp[ilIndex];
		}
		else
		{
			olSday = "";
		}
	}
	else
		return -1;

	if (olSday.GetLength() > 0)
	{
		CString olTmpSday = olSday + "000000";
		CTime olDay = DBStringToDateTime(olTmpSday);
		CTime olLoadTimeStart,olLoadTimeEnd;
		pogCoverageDiagram->omViewer.GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);
		if (olDay >= olLoadTimeStart && olDay <= olLoadTimeEnd)
			return TRUE;
		else
			return FALSE;
	}
	else
		return -1;
}


BOOL CCoverageApp::GplBcFunc(BcStruct *prpBc)
{
	if (pogCoverageDiagram == NULL)
		return -1;

	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	int ilIndex2 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "VAFR")
		{
			ilIndex1 = i;
		}
		else if (olTmp[i] == "VATO")
		{
			ilIndex2 = i;
		}
	}

	CTime olVafr,olVato;
	if (ilIndex1 >= 0 && ilIndex2 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olVafr = DBStringToDateTime(olTmp[ilIndex1]);
			olVato = DBStringToDateTime(olTmp[ilIndex2]);
		}
		else
		{
			olVafr = TIMENULL;
			olVato = TIMENULL;
		}
	}
	else
		return -1;

	if (olVafr != TIMENULL)
	{
		CTime olLoadTimeStart,olLoadTimeEnd;
		pogCoverageDiagram->omViewer.GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);

		if ((olVato == TIMENULL || olVato >= olLoadTimeStart) && olVafr <= olLoadTimeEnd)
			return TRUE;
		else
			return FALSE;
	}
	else
		return -1;
}

BOOL CCoverageApp::SpeBcFunc(BcStruct *prpBc)
{
	if (pogCoverageDiagram == NULL)
		return -1;

	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	int ilIndex2 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "VPFR")
		{
			ilIndex1 = i;
		}
		else if (olTmp[i] == "VPTO")
		{
			ilIndex2 = i;
		}
	}

	CTime olVafr,olVato;
	if (ilIndex1 >= 0 && ilIndex2 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olVafr = DBStringToDateTime(olTmp[ilIndex1]);
			olVato = DBStringToDateTime(olTmp[ilIndex2]);
		}
		else
		{
			olVafr = TIMENULL;
			olVato = TIMENULL;
		}
	}
	else
		return -1;

	if (olVafr != TIMENULL)
	{
		CTime olLoadTimeStart,olLoadTimeEnd;
		pogCoverageDiagram->omViewer.GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);

		if ((olVato == TIMENULL || olVato >= olLoadTimeStart) && olVafr <= olLoadTimeEnd)
			return TRUE;
		else
			return FALSE;
	}
	else
		return -1;
}

BOOL CCoverageApp::TplBcFunc(BcStruct *prpBc)
{
	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	int ilIndex2 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "TPST")
		{
			ilIndex1 = i;
		}
		else if (olTmp[i] == "APPL")
		{
			ilIndex2 = i;
		}
	}

	CString olTpst,olAppl;
	if (ilIndex1 >= 0 && ilIndex2 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olTpst = olTmp[ilIndex1];
			olAppl = olTmp[ilIndex2];
		}
		else
		{
			olTpst = "";
			olAppl = "";
		}
	}
	else
		return -1;


	if (olTpst.GetLength() > 0 && olAppl.GetLength() > 0)
	{
		if (olTpst == "1" && olAppl.Find("RULE") == 0)
			return TRUE;
		else
			return FALSE;
	}
	else
		return -1;

}


BOOL CCoverageApp::RueBcFunc(BcStruct *prpBc)
{
	CCoverageApp *pAppl = (CCoverageApp *)AfxGetApp();
	if (pAppl == NULL)
		return -1;

	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "UTPL")
		{
			ilIndex1 = i;
			break;
		}
	}


	CString olUtpl;
	if (ilIndex1 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olUtpl = olTmp[ilIndex1];
		}
		else
		{
			olUtpl = "";
		}
	}
	else
		return -1;

	if (olUtpl.GetLength() > 0)
	{
		olTmp.RemoveAll();
		ExtractItemList(pAppl->omLoadedTplUrnos,&olTmp);
		int ilFieldCount = olTmp.GetSize();

		for (int i = 0; i < ilFieldCount; i++)
		{
			if (olTmp[i] == olUtpl)
			{
				return TRUE;
			}
		}

		return FALSE;
	}
	else
		return -1;

}


BOOL CCoverageApp::RpfBcFunc(BcStruct *prpBc)
{
	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "URUD")
		{
			ilIndex1 = i;
			break;
		}
	}


	CString olUrud;
	if (ilIndex1 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olUrud = olTmp[ilIndex1];
		}
		else
		{
			olUrud = "";
		}
	}
	else
		return -1;

	if (olUrud.GetLength() > 0)
	{
		RecordSet olRecord;
		return ogBCD.GetRecord("RUD","URNO",olUrud,olRecord);
	}
	else
		return -1;
}


BOOL CCoverageApp::RpqBcFunc(BcStruct *prpBc)
{
	CStringArray olTmp;
	ExtractItemList(prpBc->Fields, &olTmp);
	int ilFieldCount = olTmp.GetSize();

	int ilIndex1 = -1;
	for (int i = 0; i < ilFieldCount; i++)
	{
		if (olTmp[i] == "UDGR")
		{
			ilIndex1 = i;
			break;
		}
	}


	CString olUdgr;
	if (ilIndex1 >= 0)
	{
		ExtractItemList (prpBc->Data, &olTmp);
		int ilDataCount = olTmp.GetSize();	
			
		if (ilDataCount >= ilFieldCount)
		{
			olUdgr = olTmp[ilIndex1];
		}
		else
		{
			olUdgr = "";
		}
	}
	else
		return -1;

	if (olUdgr.GetLength() > 0)
	{
		RecordSet olRecord;
		return ogBCD.GetRecord("RUD","UDGR",olUdgr,olRecord);
	}
	else
		return -1;
}


void CCoverageApp::LoadRules (bool bpInitial /*=false*/ )
{
	CString olValidTpls = "";
	int ilPercent = 100 / (12+18); 
	int	ilLen;
	DWORD	llNow,llLast;
	double	flDiff;
	char pclWhere[2000];
	
	for (int ilTpl = 0; ilTpl < ogBasicData.omValidTemplates.GetSize();ilTpl++)
	{
		if(ilTpl > 0)
		{
			olValidTpls += (",'" + ogBasicData.omValidTemplates.GetAt(ilTpl)+ "'");
		}
		else
		{
			olValidTpls = "'" + ogBasicData.omValidTemplates.GetAt(ilTpl) + "'";
		}
	}
	if ( olValidTpls != omLoadedTplUrnos )
	{
		llLast = GetTickCount();

		sprintf(pclWhere," where UTPL IN (%s)",olValidTpls);
		// rules
		if ( bpInitial )
			ogBCD.SetObject("RUE", "URNO,RUSN,UTPL,RUST",true,RueBcFunc);
		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1883));
		ogBCD.Read("RUE",pclWhere);
		//unnecessary, this map is set by default  	ogBCD.AddKeyMap("RUE", "URNO");
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("LoadRules: Read(RUE) %.3lf sec\n", flDiff/1000.0 );

		// **************************    
		
		// rule demands  (Bedarfe)
		if (ogBasicData.IsUtplAvailable())
		{
			sprintf(pclWhere," where UTPL IN (%s)",olValidTpls);
			if ( bpInitial )
				ogBCD.SetObject("RUD", "URNO,UGHS,UTPL,RUSN,URUE,RETY,DISP,UTPL",false,RueBcFunc);
		}
		else
		{
			sprintf(pclWhere," where URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s))",olValidTpls);

			if ( bpInitial )
				ogBCD.SetObject("RUD", "URNO,UGHS,UTPL,RUSN,URUE,RETY,DISP");
		}

		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1884));
		ogBCD.Read("RUD", pclWhere );
		//unnecessary, this map is set by default  ogBCD.AddKeyMap("RUD", "URNO");
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("LoadRules: Read(RUD) %.3lf sec\n", flDiff/1000.0 );

		//*******************************
		if (ogBasicData.IsUtplAvailable())
		{
			sprintf(pclWhere, " where urud in (SELECT URNO FROM RUDTAB WHERE UTPL IN (%s))",olValidTpls);
		}
		else
		{
			sprintf(pclWhere, " where urud in (SELECT URNO FROM RUDTAB WHERE (URUE in (SELECT URNO FROM RUETAB WHERE UTPL IN (%s) ) ) )",olValidTpls);
		}

		if ( bpInitial )
		{
			ogBCD.SetObject("RPF", "URNO,URUD,UPFC",true,RpfBcFunc );
			ogBCD.AddKeyMap("RPF", "URUD");
		}
		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1875));
		ogBCD.Read("RPF", pclWhere );
		if ( pogInitialLoad )
			pogInitialLoad->SetProgress(ilPercent);
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("LoadRules: Read(RPF) %.3lf sec\n", flDiff/1000.0 );

		//*******************************
		if ( bpInitial )
		{
			ogBCD.SetObject("REQ","",true,RpfBcFunc);
			ogBCD.AddKeyMap("REQ", "URUD");
		}
		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1873));
		ogBCD.Read("REQ", pclWhere );
		if ( pogInitialLoad )
			pogInitialLoad->SetProgress(ilPercent);
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("LoadRules: Read(REQ) %.3lf sec\n", flDiff/1000.0 );

		//*******************************
		//MWO 01.09.99 ==> Vergessen RLO zu lesen
		if ( bpInitial )
			ogBCD.SetObject("RLO","",true,RpfBcFunc);

		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1871));
		ogBCD.Read("RLO", pclWhere );
		if ( pogInitialLoad )
			pogInitialLoad->SetProgress(ilPercent);
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("LoadRules: Read(RLO) %.3lf sec\n", flDiff/1000.0 );
		//MWO END 

		//*******************************
		ilLen = strlen(pclWhere);
		if (ogBasicData.IsUtplAvailable())
		{
			sprintf ( &(pclWhere[ilLen]), " or udgr in (SELECT UDGR FROM RUDTAB WHERE utpl IN (%s))",olValidTpls);
		}
		else
		{
			sprintf ( &(pclWhere[ilLen]), " or udgr in (SELECT UDGR FROM RUDTAB WHERE (URUE in (select urno from RUETAB where utpl in (%s) )))",olValidTpls);
		}

		if ( bpInitial )
		{
			ogBCD.SetObject("RPQ","",true,RpqBcFunc);
			ogBCD.AddKeyMap("RPQ", "URUD");
		}
		if ( pogInitialLoad )
			pogInitialLoad->SetMessage(LoadStg(IDS_STRING1876));
		ogBCD.Read("RPQ", pclWhere );
		if ( pogInitialLoad )
			pogInitialLoad->SetProgress(ilPercent);
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		TRACE("LoadRules: Read(RPQ) %.3lf sec\n", flDiff/1000.0 );
		omLoadedTplUrnos = olValidTpls;
	}
}