#ifndef AFX_SDGGRAPHIC_H__A4C50922_461F_11D2_8DF0_0000C002916B__INCLUDED_
#define AFX_SDGGRAPHIC_H__A4C50922_461F_11D2_8DF0_0000C002916B__INCLUDED_

// SdgGraphic.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster SdgGraphic 
#include <CCSGlobl.h>


class SdgGraphic : public CWnd
{
// Konstruktion
public:
	SdgGraphic();

	int imWeeks;
// Attribute
public:

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SdgGraphic)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~SdgGraphic();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(SdgGraphic)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SDGGRAPHIC_H__A4C50922_461F_11D2_8DF0_0000C002916B__INCLUDED_
