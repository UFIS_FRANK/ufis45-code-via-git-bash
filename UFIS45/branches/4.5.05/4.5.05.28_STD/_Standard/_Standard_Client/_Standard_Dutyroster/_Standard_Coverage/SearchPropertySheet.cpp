// SearchPropertySheet.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <ccsglobl.h>
#include <BasicData.h>

#include <SearchPropertySheet.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet

IMPLEMENT_DYNAMIC(SearchPropertySheet, CPropertySheet)
SearchPropertySheet::SearchPropertySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	pomParent = pParentWnd;

	ilRealPageIndex[0] = 0;
	ilRealPageIndex[1] = 1;
 
	omLoadStart = CTime::GetCurrentTime();
	omLoadEnd = CTime::GetCurrentTime();

	imPage1Type = SEARCH;
	pomFlightPage = NULL;
	pomFlightPage = new CFlightPage(imPage1Type,omLoadStart,omLoadEnd);
	AddPage(pomFlightPage);
	
}


SearchPropertySheet::SearchPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage ,bool bpShowPage1,
									   	PageType ipPage1Type , CTime opDate1 ,CTime opDate2 )
										:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{

	int ilCurrPageCount = 0;
	pomParent = pParentWnd;

	pomFlightPage = NULL;

	omLoadStart = opDate1;
	omLoadEnd = opDate2;

	if(bpShowPage1)
	{
		pomFlightPage = new CFlightPage(ipPage1Type,opDate1,opDate2);
		AddPage(pomFlightPage);
		pomFlightPage->SetCaption(LoadStg(IDS_STRING61205));
		ilRealPageIndex[ilCurrPageCount] = 0;
		ilCurrPageCount++;
	}
	imPage1Type = ipPage1Type;

}

SearchPropertySheet::~SearchPropertySheet()
{
	if(pomFlightPage != NULL)
	{
		delete pomFlightPage;
	}
}

BOOL SearchPropertySheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();

	CRect olRect;
	CRect olWndRect;
	GetWindowRect(olWndRect);
	olRect = olWndRect;
	int ilHeight = olWndRect.bottom - olWndRect.top;
	olRect.top = (int)(::GetSystemMetrics(SM_CYSCREEN)/2)-(int)(ilHeight/2);
	olRect.bottom = (int)(::GetSystemMetrics(SM_CYSCREEN)/2)+(int)(ilHeight/2) +30;
	
	MoveWindow(olRect);
	
	CRect rcCancel;
	CRect rcOk;
	CRect rcApply;

	CWnd *olOKButton = GetDlgItem(IDOK);
	if (olOKButton != NULL)
	{
		olOKButton->GetWindowRect(rcOk);
		ScreenToClient(rcOk);
		olOKButton->ShowWindow(SW_SHOW);
		olOKButton->EnableWindow(TRUE);
		olOKButton->SetWindowText(LoadStg(IDS_STRING61235));
	}
	CWnd *olCancelButton = GetDlgItem(IDCANCEL);
	if (olCancelButton != NULL)
	{
		olCancelButton->GetWindowRect(rcCancel);
		ScreenToClient(rcCancel);
		olCancelButton->ShowWindow(SW_SHOW);
		olCancelButton->EnableWindow(TRUE);
		olCancelButton->SetWindowText(LoadStg(IDS_STRING61214));

	}
	CWnd *olApplyButton = GetDlgItem(ID_APPLY_NOW);
	if (olApplyButton != NULL)
	{
		olApplyButton->GetWindowRect(rcApply);
		ScreenToClient(rcApply);
		olApplyButton->ShowWindow(SW_SHOW);
		olApplyButton->EnableWindow(TRUE);
		olApplyButton->SetWindowText(LoadStg(IDS_STRING61213));
	}

	olOKButton->MoveWindow(rcOk);
	olCancelButton->MoveWindow(rcApply);
	olApplyButton->MoveWindow(rcCancel);



	return bResult;
}

BEGIN_MESSAGE_MAP(SearchPropertySheet, CPropertySheet)
	//{{AFX_MSG_MAP(SearchPropertySheet)
	ON_BN_CLICKED(IDOK, OnOK)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	ON_BN_CLICKED(ID_APPLY_NOW, OnApply)
	ON_WM_DESTROY()
	ON_WM_NCDESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet message handlers



void SearchPropertySheet::OnApply()
{
	int ilPageIndex = 0;

	ilPageIndex = GetActiveIndex();
	if(ilRealPageIndex[ilPageIndex] == 0) //Flights
	{
		if(imPage1Type == SEARCH)
		{
			if(pomFlightPage != NULL)
			{
				pomFlightPage->ResetDisplay();
			}
			return;
		}		
		if(imPage1Type == CREATEDEM)
		{
			if(pomFlightPage != NULL)
			{
				pomFlightPage->ResetDisplay();
			}
			return;
		}
	}
}

void SearchPropertySheet::OnOK()
{
	int ilPageIndex = 0;

	ilPageIndex = GetActiveIndex();
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if(ilRealPageIndex[ilPageIndex] == 0) //Flights
	{
		if(pomFlightPage != NULL) 
		{
			bool blIsSingleFlight = false;
			if (! pomFlightPage->GetSearchText(omSearchText,blIsSingleFlight))
				return;
			else if (imPage1Type == SEARCH)
			{
				omSearch.SearchFlightData(omSearchText);
			}
			else if (imPage1Type == CREATEDEM)
			{
				int ilSelection = IDNO;
				CTime olFidStart= DBStringToDateTime(pomFlightPage->omFidStart);
				ogBasicData.UtcToLocal(olFidStart);

				CTime olFidEnd  = DBStringToDateTime(pomFlightPage->omFidEnd);
				ogBasicData.UtcToLocal(olFidEnd);

				if (!IsWithIn(olFidStart,olFidEnd,omLoadStart,omLoadEnd))
				{
					ilSelection = AfxMessageBox(LoadStg(IDS_STRING1889),MB_YESNOCANCEL|MB_ICONEXCLAMATION);
				}

				switch(ilSelection)
				{
				case IDNO:		// Create demands
					bgCreateDemIsActive = true;
					bgRuleDiagnosticIsActive = pomFlightPage->m_RuleDiagnosis;

					MessageBox(LoadStg(IDS_STRING61242), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
					omSearch.SendCreateDemData (pomFlightPage->bmCreateFdep,omSearchText,pomFlightPage->bmCreateFid,
												pomFlightPage->omFidStart,pomFlightPage->omFidEnd,blIsSingleFlight,pomFlightPage->m_RuleDiagnosis);
					this->GetParent()->SendMessage(WM_UPDATETOOLBAR,0,0);
					EndDialog(IDOK);
				break;
				case IDYES :	// Change loaded time frame
					bgCreateDemIsActive = true;
					bgRuleDiagnosticIsActive = pomFlightPage->m_RuleDiagnosis;
					MessageBox(LoadStg(IDS_STRING61242), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
					omSearch.SendCreateDemData (pomFlightPage->bmCreateFdep,omSearchText,pomFlightPage->bmCreateFid,
												pomFlightPage->omFidStart,pomFlightPage->omFidEnd,blIsSingleFlight,pomFlightPage->m_RuleDiagnosis);
					this->GetParent()->SendMessage(WM_UPDATETOOLBAR,0,0);
					this->GetParent()->PostMessage(WM_LOAD_TIMEFRAME,olFidStart.GetTime(),olFidEnd.GetTime());															
					EndDialog(IDOK);
				break;
				default:
					EndDialog(IDCANCEL);
				}
			}
			
		}
	}
}


void SearchPropertySheet::OnDestroy() 
{
	CPropertySheet::OnDestroy();
}

void SearchPropertySheet::OnNcDestroy() 
{
	CPropertySheet::OnNcDestroy();
	AfxGetMainWnd()->SendMessage(WM_SEARCH_EXIT);
}

void SearchPropertySheet::DoCreate() 
{
	CPropertySheet::Create( pomParent );
} 

void SearchPropertySheet::OnCancel()
{
	EndDialog(IDCANCEL);
}

void SearchPropertySheet::SetLoadStartEnd(CTime opDate1, CTime opDate2, bool bpSearchIsOpen)
{
	omLoadStart = opDate1;
	omLoadEnd   = opDate2;
	if(pomFlightPage != NULL)
	{
		pomFlightPage->SetSearchStartEnd( opDate1,  opDate2,bpSearchIsOpen);
	}
}

void SearchPropertySheet::AttachTsrArray(const CStringArray * popTsrArray)
{
	omSearch.AttachTsrArray(popTsrArray);
}

void SearchPropertySheet::AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap)
{
	omSearch.AttachFlightDemMap(popFlightDemMap);
}

