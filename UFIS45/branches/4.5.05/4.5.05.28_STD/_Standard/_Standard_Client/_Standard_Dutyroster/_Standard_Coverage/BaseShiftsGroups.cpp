// BaseShiftsGroups.cpp: implementation of the CBaseShiftsGroups class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <coverage.h>
#include <BaseShiftsGroups.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaseShiftsGroups::CBaseShiftsGroups()
{
	lmMaxGroupIndex			= 1;
	lmShiftGroupCount		= 0;
	dmTotalWorkingHours		= 0;
	dmCoveredWorkingHours	= 0;
}

CBaseShiftsGroups::CBaseShiftsGroups(const CBaseShiftsGroups& rhs)
{
	this->lmMaxGroupIndex = rhs.lmMaxGroupIndex;
	this->lmShiftGroupCount = rhs.lmShiftGroupCount;
	this->dmTotalWorkingHours = rhs.dmTotalWorkingHours;
	this->dmCoveredWorkingHours = rhs.dmCoveredWorkingHours;

	for(int illc = 0; illc < rhs.omBaseShiftsGroups.GetSize(); illc++)
	{
		BASESHIFTGROUP *prlBsgData = new BASESHIFTGROUP(rhs.omBaseShiftsGroups[illc]);
		this->omBaseShiftsGroups.Add(prlBsgData);
	}
}

CBaseShiftsGroups&	CBaseShiftsGroups::operator=(const CBaseShiftsGroups& rhs)
{
	if (this != &rhs)
	{
		this->lmMaxGroupIndex = rhs.lmMaxGroupIndex;
		this->lmShiftGroupCount = rhs.lmShiftGroupCount;
		this->dmTotalWorkingHours = rhs.dmTotalWorkingHours;
		this->dmCoveredWorkingHours = rhs.dmCoveredWorkingHours;

		this->omBaseShiftsGroups.DeleteAll();

		for(int illc = 0; illc < rhs.omBaseShiftsGroups.GetSize(); illc++)
		{
			BASESHIFTGROUP *prlBsgData = new BASESHIFTGROUP(rhs.omBaseShiftsGroups[illc]);
			this->omBaseShiftsGroups.Add(prlBsgData);
		}
		
	}

	return *this;
}

CBaseShiftsGroups::~CBaseShiftsGroups()
{
	ResetData();
}
	
void CBaseShiftsGroups::SetTotalWorkingHours(double dpWorkingHours)
{
	dmTotalWorkingHours = dpWorkingHours;
}

double CBaseShiftsGroups::TotalWorkingHours()
{
	return dmTotalWorkingHours;
}

double CBaseShiftsGroups::EvalFunction(double dpUpperBound,double dpMinVal,double dpMaxVal,double dpActVal)
{
	double dlValue = dpUpperBound;
	double dlSteigung = -1;
	double dlTmpMinVal = dpMinVal -1;

	if (dlTmpMinVal < dpActVal)
	{
		dlValue = 0;

		if (dpMaxVal - dlTmpMinVal > 0.1)
		{
			dlSteigung = -(dpUpperBound/(dpMaxVal - dlTmpMinVal));
		}

		if (dpActVal < dpMaxVal)
		{
			dlValue = dpUpperBound + dlSteigung*(dpActVal - dlTmpMinVal);
		}
	}
	return dlValue;
}





	
double CBaseShiftsGroups::EvalGroup(long lpGroupIndex)
{
	double dlGroupVal = 0;
	for(int illc = 0; illc < omBaseShiftsGroups.GetSize(); illc++)
	{
		if(omBaseShiftsGroups[illc].GroupIndex == lpGroupIndex)
		{
			if (omBaseShiftsGroups[illc].UsePercent)
			{
				dlGroupVal = EvalFunction(100000,omBaseShiftsGroups[illc].MinPerc,omBaseShiftsGroups[illc].MaxPerc,omBaseShiftsGroups[illc].ActPerc);
			}
			else
			{
				dlGroupVal = EvalFunction(100000,omBaseShiftsGroups[illc].MinAbs,omBaseShiftsGroups[illc].MaxAbs,omBaseShiftsGroups[illc].ActAbs);
			}
		}
	}
	return dlGroupVal;
}

int CBaseShiftsGroups::GetStartOffset(long lpGroupIndex)
{
	for(int illc = 0; illc < omBaseShiftsGroups.GetSize(); illc++)
	{
		if(omBaseShiftsGroups[illc].GroupIndex == lpGroupIndex)
		{
			return omBaseShiftsGroups[illc].StartOffset;
		}
	}
	return 0;
}

int CBaseShiftsGroups::GetEndOffset(long lpGroupIndex)
{
	for(int illc = 0; illc < omBaseShiftsGroups.GetSize(); illc++)
	{
		if(omBaseShiftsGroups[illc].GroupIndex == lpGroupIndex)
		{
			return omBaseShiftsGroups[illc].EndOffset;
		}
	}
	return 0;
}


long CBaseShiftsGroups::AddData(double dpMinVal,double dpMaxVal,bool bpUsePercent,int ipStartOffset,int ipEndOffset)
{
	BASESHIFTGROUP *prlBsgData = new BASESHIFTGROUP;

	if(bpUsePercent)
	{
		prlBsgData->MinPerc		= dpMinVal;
		prlBsgData->MaxPerc		= dpMaxVal;
		prlBsgData->MinAbs		= 0;
		prlBsgData->MaxAbs		= 0;
	}
	else
	{
		prlBsgData->MinPerc		= 0;
		prlBsgData->MaxPerc		= 0;
		prlBsgData->MinAbs		= dpMinVal;
		prlBsgData->MaxAbs		= dpMaxVal;
	}

	prlBsgData->ActAbs		= 0;
	prlBsgData->ActPerc		= 0;
	prlBsgData->UsePercent	= bpUsePercent;
	prlBsgData->GroupIndex	= lmMaxGroupIndex;
	prlBsgData->StartOffset	= ipStartOffset;
	prlBsgData->EndOffset	= ipEndOffset;
	prlBsgData->WorkingHours= 0;	
	
	omBaseShiftsGroups.Add(prlBsgData);
	
	lmMaxGroupIndex++;
	lmShiftGroupCount++;

	return(lmMaxGroupIndex -1);
	
}
int CBaseShiftsGroups::GetShiftGroupCount()
{
	return lmShiftGroupCount;
}

void CBaseShiftsGroups::ResetData()
{
	lmMaxGroupIndex			= 0;
	lmShiftGroupCount		= 0;
	dmTotalWorkingHours		= 0;
	dmCoveredWorkingHours	= 0;
	omBaseShiftsGroups.DeleteAll();
}


void CBaseShiftsGroups::IncreaseActVal(long lpGroupIndex,double dpWorkingHours)
{
	this->dmCoveredWorkingHours += dpWorkingHours;

	for(int illc = 0; illc < lmShiftGroupCount; illc++)
	{
		if (omBaseShiftsGroups[illc].GroupIndex == lpGroupIndex)
		{
			omBaseShiftsGroups[illc].ActAbs++;
			omBaseShiftsGroups[illc].WorkingHours += dpWorkingHours;
		}

		if (fabs(this->dmTotalWorkingHours) > 1.0e-5)
			omBaseShiftsGroups[illc].ActPerc = (omBaseShiftsGroups[illc].WorkingHours / this->dmTotalWorkingHours) * 100;
		else
			omBaseShiftsGroups[illc].ActPerc = 0;
	}
}
