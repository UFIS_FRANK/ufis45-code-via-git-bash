// SimulationPage.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <SimulationPage.h>
#include <CoverageDiaViewer.h>
#include <SimulationSheet.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SimulationPage property page

IMPLEMENT_DYNCREATE(SimulationPage, CPropertyPage)

SimulationPage::SimulationPage() : CPropertyPage(SimulationPage::IDD)
{
	//{{AFX_DATA_INIT(SimulationPage)
	m_SimulationType = 0;
	m_SimulationDisplay = 0;
	//}}AFX_DATA_INIT

	bmSingleFunction = true;
}

SimulationPage::~SimulationPage()
{
}

void SimulationPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SimulationPage)
	DDX_Radio(pDX, IDC_RADIO_OFFSET, m_SimulationType);
	DDX_Radio(pDX, IDC_RADIO_CALCULATION, m_SimulationDisplay);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SimulationPage, CPropertyPage)
	//{{AFX_MSG_MAP(SimulationPage)
	ON_BN_CLICKED(IDC_RADIO_CUT, OnRadioCut)
	ON_BN_CLICKED(IDC_RADIO_OFFSET, OnRadioOffset)
	ON_BN_CLICKED(IDC_RADIO_SMOOTH, OnRadioSmooth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SimulationPage message handlers

BOOL SimulationPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	// TODO: Add extra initialization here
	CWnd *polWnd = GetDlgItem(IDC_SIMULATION_TYPE);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1812));

	polWnd = GetDlgItem(IDC_RADIO_OFFSET);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1813));

	polWnd = GetDlgItem(IDC_RADIO_CUT);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1814));

	polWnd = GetDlgItem(IDC_RADIO_SMOOTH);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1815));

	polWnd = GetDlgItem(IDC_SIMULATION_DISPLAY);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1830));

	polWnd = GetDlgItem(IDC_RADIO_CALCULATION);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1831));

	polWnd = GetDlgItem(IDC_RADIO_VISUALIZATION);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1832));

	CoverageDiagramViewer *polViewer = ((SimulationSheet *)this->GetParent())->GetViewer();
	ASSERT(polViewer);
	m_SimulationType	= polViewer->SimVariant();
	m_SimulationDisplay = polViewer->SimDisplay();

	CStringArray olFunctions;
	polViewer->DistinctFunctions(olFunctions);
	this->bmSingleFunction = olFunctions.GetSize() == 1;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SimulationPage::OnRadioCut() 
{
	// TODO: Add your control notification handler code here
	CButton *polWnd = (CButton *)GetDlgItem(IDC_RADIO_CUT);
	if (polWnd && polWnd->GetCheck() == 1)
	{
		if (!this->bmSingleFunction)
		{
			MessageBox(LoadStg(IDS_STRING1953),LoadStg(IDS_STRING1942),MB_ICONSTOP);
			UpdateData(FALSE);
			return;
		}
	}

	UpdateData(TRUE);
	CWnd *pParent = GetParent();
	if (pParent)
		pParent->SendMessage(WM_UPDATE_ALL_PAGES,0,(long)this);
	
}

void SimulationPage::OnRadioOffset() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CWnd *pParent = GetParent();
	if (pParent)
		pParent->SendMessage(WM_UPDATE_ALL_PAGES,0,(long)this);
	
}

void SimulationPage::OnRadioSmooth() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CWnd *pParent = GetParent();
	if (pParent)
		pParent->SendMessage(WM_UPDATE_ALL_PAGES,0,(long)this);
	
}

int SimulationPage::CurrentType()
{
	if (::IsWindow(this->m_hWnd))
		UpdateData(TRUE);
	return this->m_SimulationType;
}

void SimulationPage::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	UpdateData(TRUE);

	CoverageDiagramViewer *polViewer = ((SimulationSheet *)this->GetParent())->GetViewer();
	ASSERT(polViewer);

	polViewer->SimVariant(this->m_SimulationType);
	polViewer->SimDisplay(this->m_SimulationDisplay);
	polViewer->SimulationActive(true);
	
	CPropertyPage::OnOK();
}

BOOL SimulationPage::OnApply() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CPropertyPage::OnApply();
}

void SimulationPage::OnCancel() 
{
	// TODO: Add your specialized code here and/or call the base class
	CoverageDiagramViewer *polViewer = ((SimulationSheet *)this->GetParent())->GetViewer();
	ASSERT(polViewer);

	polViewer->SimulationActive(false);
	
	CPropertyPage::OnCancel();
}

void SimulationPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}
