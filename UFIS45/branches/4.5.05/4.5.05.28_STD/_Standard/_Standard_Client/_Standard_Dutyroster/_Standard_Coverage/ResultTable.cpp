// ResultTable.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <ccsglobl.h>
#include <ccsparam.h>
#include <TABLE.h>
#include <BasicData.h>
#include <ResultTable.h>
#include <CedaBasicData.h>
#include <covdiagram.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultTable dialog
static int CompareResName(const RESDATA **e1, const RESDATA **e2);

CResultTable::CResultTable(CWnd* pParent /*=NULL*/,int ipType /*=APLANE*/,CString opOutHeader /*""*/)
	: CDialog(CResultTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(CResultTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomTable = NULL;
	imType = ipType;
	imOutNameMaxSize = 1;
	omOutHeader = opOutHeader;
}

CResultTable::~CResultTable()
{
	//{{AFX_DATA_INIT(CResultTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	if(pomTable != NULL)
	{
		delete pomTable;
		pomTable = NULL;
	}
	omLines.DeleteAll();
	om2Lines.DeleteAll();
	omKeyMap.RemoveAll();
	omKey2Map.RemoveAll();
}

void CResultTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResultTable)
	DDX_Control(pDX, IDC_FRAME, m_Frame);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CResultTable, CDialog)
	//{{AFX_MSG_MAP(CResultTable)
	ON_BN_CLICKED(IDC_SNAPSHOT, OnSnapshot)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResultTable message handlers

void CResultTable::ResetContent()
{
	CCS_TRY
		pomTable->ResetContent();
		imOutNameMaxSize = 1;
	CCS_CATCH_ALL
}

void CResultTable::SetOutHeader(CString opOutHeader)
{
	CCS_TRY
		omOutHeader = opOutHeader;
	CCS_CATCH_ALL
}


void CResultTable::OnOK() 
{
	CCS_TRY
		ShowWindow(SW_HIDE);
		//CDialog::OnOK();
	CCS_CATCH_ALL
}

BOOL CResultTable::OnInitDialog() 
{
	CCS_TRY
		CDialog::OnInitDialog();

		CWnd *polWnd = GetDlgItem(IDOK);
		if(polWnd != NULL)
		{
			polWnd->SetWindowText(LoadStg(IDS_STRING61214));
		}
		
		SetWindowText(LoadStg(IDS_STRING61334));

		polWnd = GetDlgItem(IDC_SNAPSHOT);
		if (polWnd != NULL)
		{
			CString olSaveDemands = ogCCSParam.GetParamValue("COVERAGE","ID_SAVE_DEMANDS",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);

			if (this->imType == COV_TAB && olSaveDemands == "Y")
			{
				polWnd->SetWindowText(LoadStg(IDS_STRING1937));
			}
			else
			{
				polWnd->ShowWindow(SW_HIDE);
				polWnd->EnableWindow(FALSE);
			}
		}
		
		if(pomTable == NULL)
			pomTable = new CTable;
		pomTable->tempFlag = 2;
		pomTable->SetSelectMode(0);

		CRect rect;
		m_Frame.GetWindowRect(&rect);
		ScreenToClient(&rect);
		rect.bottom += rect.top;
		rect.right += rect.left;
		rect.InflateRect(1, 1);     // hiding the CTable window border

		//m_FullList.GetWindowRect(&rect1);
		//ScreenToClient(rect1);
		
		//rect.top +=rect1.bottom+5;
		pomTable->SetTableData(this, rect.left, rect.right,rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);
		CWaitCursor olWait;
		UpdateResults(imType);
		pomTable->DisplayTable();
		pomTable->SetFocus();
		
		// TODO: Add extra initialization here
	
		return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
	CCS_CATCH_ALL
	return FALSE;
}


void CResultTable::UpdateResults(int ipType,bool bpUseKey2Map)
{
	CCS_TRY
		pomTable->ResetContent();
		switch(ipType)
		{
		case APLANE:
			pomTable->SetHeaderFields(LoadStg(IDS_STRING1725));
			pomTable->SetFormatList(LoadStg(IDS_STRING1726));
			break;
		case WKGRP:
			pomTable->SetHeaderFields(LoadStg(IDS_STRING1730));
			pomTable->SetFormatList(LoadStg(IDS_STRING1726));
			break;
		case COV_TAB:
			{
				CString olHeaderText = LoadStg(IDS_STRING1732) + "|" + omOutHeader;
				pomTable->SetHeaderFields(olHeaderText);
				CString olOutTextSize;
				olOutTextSize.Format("%d",2*imOutNameMaxSize);
				CString olFormatList = LoadStg(IDS_STRING1733) + "|" + olOutTextSize;
				pomTable->SetFormatList(olFormatList);
			}
			break;
		default:
			break;
		}
		
		omLines.Sort(CompareResName);
		int ilCount = omLines.GetSize();
		for(int ilLineNo = 0; ilLineNo < ilCount; ilLineNo++)
		{
			RESDATA rlRes = omLines[ilLineNo];
			pomTable->AddTextLine(Format(&omLines[ilLineNo],ipType), NULL);
		}

		if (bpUseKey2Map)
		{
			pomTable->AddTextLine(" | | | ");				
			pomTable->AddTextLine(" | | | ");				
			om2Lines.Sort(CompareResName);
			int ilCount = om2Lines.GetSize();
			for(int ilLineNo = 0; ilLineNo < ilCount; ilLineNo++)
			{
				RESDATA rlRes = om2Lines[ilLineNo];
				pomTable->AddTextLine(Format(&om2Lines[ilLineNo],ipType), NULL);
			}
		}	

		pomTable->DisplayTable();
	CCS_CATCH_ALL
}
void CResultTable::ClearLines()
{
	CCS_TRY
		omLines.DeleteAll();
		om2Lines.DeleteAll();
		omKeyMap.RemoveAll();
		omKey2Map.RemoveAll();
		imOutNameMaxSize = 1;
	CCS_CATCH_ALL
}

BOOL CResultTable::AddResults(char *pcpName,long ipDuration,int ipDura2, CString opOutName,bool bpUseKey2Map)
{
	CCS_TRY
		BOOL blRet = FALSE;
		RESDATA *prlRes;
		imOutNameMaxSize = max(imOutNameMaxSize,opOutName.GetLength());
		if (bpUseKey2Map)
		{
			if (omKey2Map.Lookup(CString(pcpName),(void *&)prlRes) == TRUE)
			{
				prlRes->Dura += ipDuration;
				prlRes->Dura2 += ipDura2;
				prlRes->OutName = opOutName;
				blRet = TRUE;
			}
			else
			{
				prlRes = new RESDATA;
				if(prlRes != NULL)
				{
					prlRes->Name = CString(pcpName);
					prlRes->Dura = ipDuration;
					prlRes->Dura2 = ipDura2;
					prlRes->OutName = opOutName;
					om2Lines.Add(prlRes);
					omKey2Map.SetAt(CString(pcpName),prlRes);
					blRet = TRUE;
				}
			}
		}
		else
		{
			if (omKeyMap.Lookup(CString(pcpName),(void *&)prlRes) == TRUE)
			{
				prlRes->Dura += ipDuration;
				prlRes->Dura2 += ipDura2;
				prlRes->OutName = opOutName;
				blRet = TRUE;
			}
			else
			{
				prlRes = new RESDATA;
				if(prlRes != NULL)
				{
					prlRes->Name = CString(pcpName);
					prlRes->Dura = ipDuration;
					prlRes->Dura2 = ipDura2;
					prlRes->OutName = opOutName;
					omLines.Add(prlRes);
					omKeyMap.SetAt(CString(pcpName),prlRes);
					blRet = TRUE;
				}
			}
		}
		return blRet;
	CCS_CATCH_ALL
	return FALSE;
}

CString CResultTable::Format(RESDATA *prpRes,int ipType)
{
	CCS_TRY
		CString s;
		if(ipType == COV_TAB)
			s.Format("%-32s|%6d|%6d|%s",prpRes->Name,prpRes->Dura,prpRes->Dura2,prpRes->OutName);
		else
			s.Format("%-32s|%6.2f",prpRes->Name,prpRes->Dura/3600.0);

		return s;
	CCS_CATCH_ALL
	return CString("");
}

static int CompareResName(const RESDATA **e1, const RESDATA **e2)
{
		return ((**e1).Name.Compare((**e2).Name));
}

void CResultTable::OnSnapshot()
{
	static bool blFirst = true;
	static int ilCovr = -1;
	static int ilMain = -1;
	static int ilDema = -1;
	static int ilCdat = -1;
	static int ilSdgn = -1;
	static int ilIgbr = -1;
	static int ilUsec = -1;
	static int ilUrno = -1;

	static int ilPnum = -1;
	static int ilNdem = -1;
	static int ilNcov = -1;
	static int ilHurn = -1;
	static int ilHopo = -1;

	CWaitCursor olWait;

	if (blFirst)
	{
		blFirst = false;
		ilCovr = ogBCD.GetFieldIndex("CSH","COVR");
		ilMain = ogBCD.GetFieldIndex("CSH","MAIN");
		ilDema = ogBCD.GetFieldIndex("CSH","DEMA");
		ilCdat = ogBCD.GetFieldIndex("CSH","CDAT");
		ilSdgn = ogBCD.GetFieldIndex("CSH","SDGN");
		ilIgbr = ogBCD.GetFieldIndex("CSH","IGBR");
		ilUsec = ogBCD.GetFieldIndex("CSH","USEC");
		ilUrno = ogBCD.GetFieldIndex("CSH","URNO");


		ilPnum = ogBCD.GetFieldIndex("CSD","PNUM");
		ilNdem = ogBCD.GetFieldIndex("CSD","NDEM");
		ilNcov = ogBCD.GetFieldIndex("CSD","NCOV");
		ilHurn = ogBCD.GetFieldIndex("CSD","HURN");
		ilHopo = ogBCD.GetFieldIndex("CSD","HOPO");

	}

	if (pogCoverageDiagram != NULL)
	{
		RecordSet olCshRecord(ogBCD.GetFieldCount("CSH"));

		olCshRecord.Values[ilUsec] = ogBasicData.omUserID;
		olCshRecord.Values[ilCdat] = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");

		if (!pogCoverageDiagram->GetSnapshotData(olCshRecord.Values[ilCovr],olCshRecord.Values[ilMain],olCshRecord.Values[ilDema],olCshRecord.Values[ilSdgn],olCshRecord.Values[ilIgbr]))
		{
			MessageBox(LoadStg(IDS_STRING1939),LoadStg(IDS_STRING1942),MB_OK);
			return;
		}

		// write header data
		if (!ogBCD.InsertRecord("CSH",olCshRecord,true))
		{
			MessageBox(LoadStg(IDS_STRING1940),LoadStg(IDS_STRING1942),MB_OK);
			return;
		}

		// write detail data
		CString olValue;
		int ilCount = omLines.GetSize();
		for(int ilLineNo = 0; ilLineNo < ilCount; ilLineNo++)
		{
			RESDATA rlRes = omLines[ilLineNo];
			RecordSet olCsdRecord(ogBCD.GetFieldCount("CSD"));


			olValue.Format("%d",ilLineNo);
			olCsdRecord.Values[ilPnum] = olValue;

			olValue.Format("%d",rlRes.Dura);
			olCsdRecord.Values[ilNdem] = olValue;

			olValue.Format("%d",rlRes.Dura2);
			olCsdRecord.Values[ilNcov] = olValue;

			olCsdRecord.Values[ilHurn] = olCshRecord.Values[ilUrno];

			olCsdRecord.Values[ilHopo] = pcgHome;

			if (!ogBCD.InsertRecord("CSD",olCsdRecord,false))
			{
				MessageBox(LoadStg(IDS_STRING1941),LoadStg(IDS_STRING1942),MB_OK);
				return;
			}
						
		}

		if (!ogBCD.Release("CSD"))
		{
			MessageBox(LoadStg(IDS_STRING1941),LoadStg(IDS_STRING1942),MB_OK);
		}
	}	
}
