#ifndef AFX_COVERAGEGRAPHICWND_H__BE9BC621_12EB_11D1_8D02_0000C002916B__INCLUDED_
#define AFX_COVERAGEGRAPHICWND_H__BE9BC621_12EB_11D1_8D02_0000C002916B__INCLUDED_
 
// CoverageGraphicWnd.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CCoverageGraphicWnd 
#include <CCSTimeScale.h>
#include <ccsdragdropctrl.h>
#include <CViewer.h>
#include <ccsdragdropctrl.h>
#include <CoverageDiaViewer.h>
#include <SearchResultsDlg.h>


#define WM_UPDATE_COVERAGE		(WM_USER + 360)
#define WM_UPDATEDEMANDGANTT	(WM_USER + 361)
#define WM_DEMAND_UPDATECOMBO	(WM_USER + 362)
#define WM_UPDATE_BASESHIFT		(WM_USER + 363)
#define	WM_DEMAND_SELECTCOMBO	(WM_USER + 364)

class CoverageDiagramViewer;
class CResultTable;

struct POLYGON_LINE
{
	long			Urno;
	bool			IsGroup;
	CString			UrnoText;
	CString			Lkco;				//Urno of Data, which is represented by that line
	long			CurrentOffset;		//if additive mode is active ==> this is the current offset for relative zeropoint
	COLORREF		Color;				//Color of the line
	CCSPtrArray<int>Values;				//Values for the line
};

struct SPLITTEDSDT
{
	long	Urno;
	CTime	Start;
	CTime	End;
	CTime	Brkf;
	CTime	Brkt;
	int		Factor;
	CString Fctc;
};



class CCoverageGraphicWnd : public CWnd
{
// Konstruktion
public:
	CCoverageGraphicWnd();

// Attribute
public:

	//MWO 03.08.1999
	CRect omOldRectX; //für Fadenkreuz X
	CRect omOldRectY;//für Fadenkreuz Y
	CRect omOldStaticTextRect;
	//MWO END 03.08.1999
	bool bmShowMainCurve;
	CCSPtrArray<POLYGON_LINE> omLines;
	CCSPtrArray<POLYGON_LINE> omShiftLines;
	CCSPtrArray<POLYGON_LINE> omRealLines;

	CCSPtrArray<COLORCONFDATA>	omColors;
	CPen			omPenList[NUMCONFCOLORS];
	CPen			omPrintPenList[NUMCONFCOLORS];
	CBrush			omBrushList[NUMCONFCOLORS];
	CBrush			omPrintBrushList[NUMCONFCOLORS];

	CUIntArray		omGhsUrnos;
	CUIntArray		omPfcUrnos;
	CCSTimeScale*	pomTimeScale;
	int				imLeftScale;


	CUIntArray omShiftArray;
	CUIntArray omCompleteShiftArray;
	CUIntArray omFlightValuesIdx;
	CUIntArray omFlightValues;
	CUIntArray omCompleteFlightValues;
	CUIntArray omSimFlightValues;
	CUIntArray omSimFlightValues2;
	CUIntArray omRealFlightValues;
	CUIntArray omCompleteSimFlightValues2;
	CoverageDiagramViewer *pomViewer;

//Printing Objects
	CDC			omCdc;
	int			imPageNo;
	CFont		omSmallFont_Regular;
	CFont		omMediumFont_Regular;
	CFont		omLargeFont_Regular;
	CFont		omSmallFont_Bold;
	CFont		omMediumFont_Bold;
	CFont		omLargeFont_Bold;
	CFont		omCCSFont;
	CFont		ogCourierNew_Regular_8;
	CFont		ogCourierNew_Bold_8;
	CDC			omMemDc;
	CBitmap*	pomCcsBitmap;
	CDC			omCcsMemDc;
	int			imLogPixelsY;
	int			imLogPixelsX;
	BOOL		bmIsInitialized;
	double		mXFactor;
	CWnd*		pomParent;
	CBitmap*	pomBitmap;
	CBitmap		omBitmap;
	CRgn		omRgn;

	int			imPIdx;
//end printing objects

	CTime		omStartTime;
	CTime		omEndTime;
	int			imXMinutes;
	int			imMaxYCount;
	int			imCountPerUnit;
	double		imOneMinute;
	int			imOneShift;
	bool		bmAdditive;
	CStatic*	pomStatic;
	CStatic*	pomHorzCross;
	CStatic*	pomVertCross; 


	CGridFenster*pomList;
	CGXFont		omCGXFont;
// Operationen
public:
	void Register();
	void UnRegister();

	void ClearPolygonLines(); 
	void AttachViewer(CoverageDiagramViewer *popViewer)
	{
		pomViewer = popViewer;
	}
	void SetAdditive(bool bpAddit = true)
	{
		bmAdditive = bpAddit;
	}
	void SetTimeScale(CCSTimeScale *popTimeScale);
	void SetDimensions(int ipLeftScale);
	void SetTimeFrame(bool bpCalcFlightDemands = true);

	void UpdateForShiftDemandUpdate();

	void CalculateFlightDemands();
	void CalculateShiftDemands();

	void CalculateSimOffsetFlightDemands();
	void CalculateSimCutFlightDemands();
	void CalculateSimSmoothFlightDemands();

	void CalculateSimOffsetFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues);
	void CalculateSimCutFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues);
	void CalculateSimSmoothFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues);

	double CalculateTotalWorkingHours(CTime opAssignStart,CTime opAssignEnd);
	
	bool IsPassSdtFilter(SDTDATA *&prpSdt);
	bool IsPassSdtFxFilter(SDTDATA *&prpSdt);
	bool IsPassSSdtFxFilter(SPLITTEDSDT *&prpSdt);	
	LONG InitializePrinter();
	void SetMarkTime(CPaintDC *pDC,CTime opMarkTimeStart, CTime opMarkTimeEnd);
	void OnCompleteCheckDemands();

	void OnResultTable(CResultTable *popResultTable);	

	int	 CalculateMaxFlightDemandPeak();
	int	 CalculateMinFlightDemandCut();
	int	 CalculateMinFlightDemandCut(const CUIntArray& ropCompleteFlightValues);

	CString omTypeList;
	CString omTypeUrnoList;
	CString omFilterType;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCoverageGraphicWnd)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CCoverageGraphicWnd();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CCoverageGraphicWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg LONG UpdateGraphic(WPARAM wParam, LPARAM lParam);
    afx_msg void OnActuellCellMoved(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimeEval();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CTime omFrameStart;
	CTime omFrameEnd;
	void DrawTimeLines(CPaintDC *pDC,CRect rcPaint);

	int CalcPIdx();

	void FillList();
	int	 FindListRow(int ipIndex);

	void TransformSdtData(CCSPtrArray <SPLITTEDSDT> &ropSplittedSdt,SDTDATA *prlSdt);

	void CheckDemands(CCSPtrArray <DEMDATA> &ropDemList,CString opFileName,CString& ropHeader,CString& ropHeaderFID,CString& ropHeaderCCI,bool bpAppend = false,bool bpDisplay = true);
	void CheckDemand(ofstream& of,int ilCount,DEMDATA& ropDemand,CMapPtrToPtr& ropCCIMap,int& ropSum,int& ropSumFID,int& ropSumCCI);	

	void PrepareSingleShiftPoly();

	void PrepareSingleFlightPoly();

	void SetColorSetup();

	
	static void CoverageWndCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

private:
	CPoint *pomFlightPoly;
	int imFlightPolySize;
	CPoint *pomRealFlightPoly;
	int imRealFlightPolySize;
	CPoint *pomShiftPoly;
	CPoint *pomSimFlightPoly;
	int imSimFlightPolySize;
	CPoint *pomSimFlight2Poly;
	int imSimFlight2PolySize;
	int imShiftPolySize;
	CPoint *pomSingleShiftPoly;
	int imSingleShiftPolySize;
	CPoint *pomSingleFlightPoly;
	int imSingleFligtPolySize;

	CTime omClickTime;
	CCSPtrArray<CPoint> pomPolyLines;
	CCSPtrArray<CPoint> pomPolyRealLines;
	CPen omBlackPen, 
		 omGrayPen,
		 omGreenPen,
		 omRedPen,
		 omWhitePen,
		 omBluePen,
		 omTimeLinePen,
		 omCurrentPen,
		*pomOldPen;
	CBrush omGreenBrush;
	CBrush omLightGreenBrush;
	CBrush omSilverBrush;
	CBrush omBlackBrush;
	UINT imColorCount;
	CTime omCurTime;
	FilterType imFilterIdx;

	int		imActuelRow;
	bool	bmEvaluateSingleFunctionOnly;
	bool	bmSorting;
};




/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_COVERAGEGRAPHICWND_H__BE9BC621_12EB_11D1_8D02_0000C002916B__INCLUDED_

