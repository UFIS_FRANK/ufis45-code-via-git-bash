// SearchPropertySheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet
#ifndef _SEARCHPROPERTY_SHEET_
#define _SEARCHPROPERTY_SHEET_

#include <CCSGlobl.h>
#include <FlightPage.h>
#include <Search.h>


/////////////////////////////////////////////////////////////////////////////
// Class declaration of SearchPropertySheet

//@Man:
//@Memo: Baseclass
//@See:  FilterData, FlightSearchPageDlg, GpeSearchPageDlg, HwSearchPageDlg
/*@Doc:
  No comment on this up to now.
*/
class SearchPropertySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(SearchPropertySheet)

// Construction
public:
    //@ManMemo: Default constructor
	SearchPropertySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	SearchPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0,bool bpShowPage1 = true,
						PageType ipPageType = SEARCH, CTime opDate1 =CTime::GetCurrentTime(),CTime opDate2 =CTime::GetCurrentTime());


// Attributes
public:

	CFlightPage *pomFlightPage;
	
	CWnd *pomParent;

// Operations
public:

	void DoCreate() ;
	void SetLoadStartEnd(CTime opDate1, CTime opDate2,bool bpSearchIsOpen = true);

	void AttachTsrArray(const CStringArray * popTsrArray);
	void AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SearchPropertySheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
public:
    //@ManMemo: Default destructor
	virtual ~SearchPropertySheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(SearchPropertySheet)
	afx_msg void OnOK();
	afx_msg void OnApply();
	afx_msg void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnNcDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int ilRealPageIndex[20];

	CSearch omSearch;

	CString omSearchText;
	PageType imPage1Type;
	CTime omLoadStart;
	CTime omLoadEnd;
};

/////////////////////////////////////////////////////////////////////////////

#endif //_SEARCHPROPERTY_SHEET_ 
