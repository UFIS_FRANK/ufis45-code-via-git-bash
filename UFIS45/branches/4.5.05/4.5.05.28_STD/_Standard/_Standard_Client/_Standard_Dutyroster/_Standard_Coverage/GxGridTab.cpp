// GxGridTab.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <GxGridTab.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GxGridTab

/*CCE*/IMPLEMENT_DYNCREATE(GxGridTab, CGXGridWnd)

GxGridTab::GxGridTab(CWnd *popParent):CGXGridWnd()
{
	pomParent = popParent;
}

GxGridTab::GxGridTab():CGXGridWnd()
{
}

GxGridTab::~GxGridTab()
{
}

void GxGridTab::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
   if(nCol == 3)
   {
	   CString s;
	   s.Format("Button pressed at Row %ld, Column %ld.\n", nRow, nCol);


	   AfxMessageBox(s);

   }
}

BOOL GxGridTab::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	int o = 0;
	o++;
	return TRUE;
}

BOOL GxGridTab::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	CGXGridWnd::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	if(pomParent != NULL)
	{
		long wParam = MAKEWPARAM(nRow, nCol);
		long lParam = (long)this;
		pomParent->PostMessage(WM_GRID_DBL_CKL, wParam, lParam);
	}
	return TRUE;
}

BEGIN_MESSAGE_MAP(GxGridTab, CGXGridWnd)
	//{{AFX_MSG_MAP(GxGridTab)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GxGridTab message handlers
