
// cMsdd.cpp - Class for handling Msd data
//

#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CCSGlobl.h>
#include <CedaMsdData.h>
#include <CedaSdgData.h>
#include <CedaBsdData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CedaBasicData.h>
#include <algorithm>

#include <sys/timeb.h>
#include <time.h>
#define IsEnveloped(start1, end1, start2, end2)    ((start1) <= (end2) && (start1) >= (start2) && (end1) <= (end2) && (end1) >= (start2))

static int CompareByTime(const MSDDATA **e1, const MSDDATA **e2)
{
	return ((**e1).Esbg>(**e2).Esbg) ? 0 : -1;
}

extern "C" int WINAPI GetWorkstationName (LPSTR ws_name);

CedaMsdData::CedaMsdData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(MSDDATA, MsdDataRecInfo)
		FIELD_CHAR_TRIM(Bewc, "BEWC")
		FIELD_CHAR_TRIM(Bkr1, "BKR1")	
		FIELD_CHAR_TRIM(Bsdc, "BSDC")	
		FIELD_CHAR_TRIM(Bsdk, "BSDK")	
		FIELD_CHAR_TRIM(Bsdn, "BSDN")	
		FIELD_CHAR_TRIM(Bsds, "BSDS")	
		FIELD_CHAR_TRIM(Ctrc, "CTRC")	
		FIELD_CHAR_TRIM(Days, "DAYS")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Type, "TYPE")	
		FIELD_CHAR_TRIM(Usec, "USEC")	
		FIELD_CHAR_TRIM(Useu, "USEU")	
		FIELD_INT	   (Sdu1, "SDU1")	
		FIELD_INT	   (Sex1, "SEX1")	
		FIELD_INT	   (Ssh1, "SSH1")	
		FIELD_INT	   (Bkd1, "BKD1")	
		FIELD_LONG	   (Urno, "URNO")	
		FIELD_DATE	   (Bkf1, "BKF1")	
		FIELD_DATE	   (Bkt1, "BKT1")	
		FIELD_DATE	   (Sbgi, "SBGI")	
		FIELD_DATE	   (Seni, "SENI")	
		FIELD_DATE	   (Cdat, "CDAT")	
		FIELD_DATE	   (Esbg, "ESBG")	
		FIELD_DATE	   (Lsen, "LSEN")	
		FIELD_DATE	   (Lstu, "LSTU")	
		FIELD_DATE	   (Brkf, "BRKF")
		FIELD_DATE	   (Brkt, "BRKT")	
		FIELD_LONG	   (Bsdu, "BSDU")	
		FIELD_LONG	   (Sdgu, "SDGU")	
		FIELD_CHAR_TRIM(Fctc, "FCTC")	
    END_CEDARECINFO
    // Copy the record structure
    for (int i = 0; i < sizeof(MsdDataRecInfo)/sizeof(MsdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&MsdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"MSD");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"BEWC,BKR1,BSDC,BSDK,BSDN,BSDS,CTRC,DAYS,"
						   "DNAM,TYPE,USEC,USEU,SDU1,SEX1,"
						   "SSH1,BKD1,URNO,BKF1,BKT1,SBGI,SENI,CDAT,ESBG,LSEN,"
						   "LSTU,BRKF,BRKT,BSDU,SDGU,FCTC");

	 pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer


    //omData.SetSize(600);
	//omData.RemoveAll();
	ClearAll();
}

void CedaMsdData::Register(void)
{
	ogDdx.Register(this,BC_MSD_CHANGE,CString("BC_MSDCHANGE"), CString("MsdDataChange"),ProcessMsdCf);
	ogDdx.Register(this,BC_MSD_NEW,CString("BC_MSDNEW"), CString("MsdDataNew"),ProcessMsdCf);
	ogDdx.Register(this,BC_MSD_DELETE,CString("BC_MSDNEW"), CString("MsdDataNew"),ProcessMsdCf);
	ogDdx.Register(this,BC_RELMSD,CString("BC_RELMSD"),CString("RELMSD"),ProcessMsdCf);
	
}

CedaMsdData::~CedaMsdData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	
}	

bool CedaMsdData::ReadSpecialData(CCSPtrArray<MSDDATA> *popMsd, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	if(strlen(pspFieldList) > 0)
	{
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			strcpy(pclFieldList, pspFieldList);
		}
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popMsd != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			MSDDATA *prpMsd = new MSDDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpMsd,CString(pclFieldList))) == true)
			{
				popMsd->Add(prpMsd);
			}
			else
			{
				delete prpMsd;
			}
		}
		if(popMsd->GetSize() == 0) return false;
	}
    return true;
}



bool CedaMsdData::Read(char *pspWhere /*NULL*/)
{
    //char where[512];
	//omData.DeleteAll();
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	ClearAll();

	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read DB Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
		{
			return false;
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read DB End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		MSDDATA *prlMsd = new MSDDATA;
		if ((ilRc = GetFirstBufferRecord2(prlMsd))==true) //GetBufferRecord(ilLc,prlMsd)) == true)
		{
			prlMsd->IsChanged = DATA_UNCHANGED;

			if(AddMsdInternal(prlMsd)==false)
			{
				delete prlMsd;
			}
		}
		else
		{
			delete prlMsd;
			TRACE("Deleted extra prlMsd\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
}

bool CedaMsdData::ReadRealMsd(char *pspWhere /*NULL*/)
{
    
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	CCSPtrArray<MSDDATA> olDelList;

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(!prlMsd->IsVirtuel)
		{
			prlMsd->IsChanged = DATA_DELETED;
			olDelList.Add(prlMsd);
		}

	}

	for(int i = olDelList.GetSize() - 1; i >= 0; i--)
	{
		DeleteMsdInternal(&olDelList[i]);
	}

	olDelList.RemoveAll();

	
	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read DB Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
		{
			return false;
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read DB End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		MSDDATA *prlMsd = new MSDDATA;
		if ((ilRc = GetFirstBufferRecord2(prlMsd))==true) //GetBufferRecord(ilLc,prlMsd)) == true)
		{
			prlMsd->IsChanged = DATA_UNCHANGED;

			if(AddMsdInternal(prlMsd)==false)
			{
				delete prlMsd;
			}
		}
		else
		{
			delete prlMsd;
			TRACE("Deleted extra prlMsd\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::Read <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
}

bool CedaMsdData::ReadMsdWithSdgu(long lpSdgu)
{
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::ReadMsdWithSdgu Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	CString olWhere;
	olWhere.Format("WHERE SDGU = '%ld'",lpSdgu);

    // Select data from the database
	if (CedaAction2("RT", olWhere.GetBuffer(0)) == false)
	{
		return false;
	}

	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::ReadMsdWithSdgu End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::ReadMsdWithSdgu <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		MSDDATA *prlMsd = new MSDDATA;
		if ((ilRc = GetFirstBufferRecord2(prlMsd))==true) 
		{
			prlMsd->IsChanged = DATA_UNCHANGED;

			if (AddMsdInternal(prlMsd) == false)
			{
				delete prlMsd;
				TRACE("Deleted extra prlMsd\n");
			}
		}
		else
		{
			delete prlMsd;
			TRACE("Deleted extra prlMsd\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaMsdData::ReadMsdWithSdgu <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
		
}

bool CedaMsdData::ClearAll()
{
    omUrnoMap.RemoveAll();
	omDnamMap.RemoveAll();
	omSelectedData.RemoveAll();
	omRedrawDuties.RemoveAll();

    omData.DeleteAll();

    return true;
}


bool CedaMsdData::AddMsdInternal(MSDDATA *prpMsd, bool bpWithDDX /*= false*/)
{
	MSDDATA *prlMsd = NULL;
	if(omUrnoMap.Lookup((void *)prpMsd->Urno,(void *&)prlMsd)==FALSE)
	{
		PrepareMsdData(prpMsd);
		omData.Add(prpMsd);
		omUrnoMap.SetAt((void *)prpMsd->Urno,prpMsd);
		omDnamMap.SetAt((CString)prpMsd->Dnam, prpMsd);
		if(bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,MSD_NEW_SELF,(void *)prpMsd);
		}
		return true;
	}
	else
	{
		return false;
	}
}




/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

bool CedaMsdData::DeleteMsd(MSDDATA *prpMsdData, BOOL bpWithSave,bool bpSendDdx )
{

	bool olRc = true;
		
	prpMsdData->IsChanged = DATA_DELETED;

	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,MSD_DELETE_SELF,(void *)prpMsdData);
	
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		olRc = SaveMsd(prpMsdData);
	}

	DeleteMsdInternal(prpMsdData);
	return olRc;
}

bool CedaMsdData::DeleteMsdWithSplu(long lpSplUrno)
{
	POSITION rlPos;
	long llUrno;

	MSDDATA *prlMsd = NULL;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->SplUrno == lpSplUrno)
		{
			DeleteMsdInternal(prlMsd);
		
		}
	}
	return true;
}

bool CedaMsdData::DeleteMsdWithSdgu(MSDDATA *prpMsdData,bool bpWithSave)
{
	bool olRc = false;
	
	if(GetMsdByUrno(prpMsdData->Urno)==NULL)
	{
		return olRc;
	}

	long llSdgu = prpMsdData->Sdgu;
	long llBsdu = prpMsdData->Bsdu;
	POSITION rlPos;
	CCSPtrArray<MSDDATA> olMsdList;

	CCSPtrArray<TIMEFRAMEDATA> olTimes;
	ogSdgData.GetTimeFrames(llSdgu, olTimes);
	CTimeSpan olSpan;
	
	int ilDayOffset = -999;
	for(int j = olTimes.GetSize()-1;j>=0;j--)
	{
		if(IsBetween(prpMsdData->Esbg,olTimes[j].StartTime,olTimes[j].EndTime))
		{
			olSpan = CTime(prpMsdData->Esbg.GetYear(), prpMsdData->Esbg.GetMonth(), prpMsdData->Esbg.GetDay(), 0, 0, 0) - olTimes[j].StartTime;
			ilDayOffset = olSpan.GetDays();
			break;
		}
	}

	if(ilDayOffset==-999)
	{
		// Error!!
		olTimes.DeleteAll();
		return false;
	}
	CTimeSpan olDayOffset(ilDayOffset,0,0,0);

	int ilDayOfWeek = prpMsdData->Esbg.GetDayOfWeek();

	// Collect all specified MSDs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu==llSdgu && prlMsd->Bsdu == llBsdu /*&& prlMsd->Esbg.GetDayOfWeek() == ilDayOfWeek*/ && strcmp(prlMsd->Fctc,prpMsdData->Fctc)==0 && prpMsdData->IsVirtuel)
		{
			olMsdList.Add(prlMsd);
			olRc = true;
		}
	}

	if(bpWithSave && olRc == true)
	{
		//olMsdList.Sort(CompareByTime);
		CCSPtrArray<MSDDATA> olList;
		CString olDeletedDates = "";
		for(j = olTimes.GetSize()-1;j>=0;j--)
		{
			// Determine date of shift in question
			//CTime olDate = /*prlMsd->Esbg =*/ HourStringToDate(prpMsdData->Esbg.Format("%H%M00"), olTimes[j].StartTime + olDayOffset);
			//TRACE("DATE %s\n",olDate.Format("%d.%m.%Y - %H:%M"));
			MSDDATA *prlDefaultMsd = NULL;
			for(int i = 0; i < olMsdList.GetSize(); i++)
			{
				MSDDATA *prlMsd = &olMsdList[i];
				
				if(IsBetween(prlMsd->Esbg,olTimes[j].StartTime,olTimes[j].EndTime))
				{
					if(prlMsd->Urno==prpMsdData->Urno)		
					{
						prlDefaultMsd = prlMsd;
						break;
					}
					else if(prlMsd->IsSelected == TRUE)
					{
						prlDefaultMsd = prlMsd;
					}
					else
					{
						if(prlDefaultMsd==NULL)
						{
							prlDefaultMsd = prlMsd;
						}
					}
				}
			}
			if(prlDefaultMsd!=NULL)
			{
				//TRACE("Preparing to delete URNO %ld => ESBG <%s>\n",prlDefaultMsd->Urno,prlDefaultMsd->Esbg.Format("%d.%m.%Y - %H:%M"));
				olList.Add(prlDefaultMsd);
			}
		}

		for(int i = olList.GetSize() - 1; i >= 0; i--)
		{
			DeleteMsdInternal(&olList[i]);
		}
		olList.RemoveAll();
		olMsdList.RemoveAll();
	}
	olTimes.DeleteAll();
	return olRc;
}

bool CedaMsdData::DeleteVirtuellMsdsBySdg(long lpSdgu)
{
	bool olRc = false;
	
	long llSdgu = lpSdgu;
	POSITION rlPos;
	CCSPtrArray<MSDDATA> olMsdList;

	// Collect all specified MSDs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu==llSdgu  &&  prlMsd->IsVirtuel)
		{
			olMsdList.Add(prlMsd);
			olRc = true;
		}
	}



	for(int i = olMsdList.GetSize() - 1; i >= 0; i--)
	{
		DeleteMsdInternal(&olMsdList[i]);
	}
	
	return olRc;
}

bool CedaMsdData::DeleteVirtuellMsdsByMsdu(long lpMsdu)
{
	bool olRc = false;
	
	long llMsdu = lpMsdu;
	POSITION rlPos;
	CCSPtrArray<MSDDATA> olMsdList;

	// Collect all specified MSDs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->MsdUrno==llMsdu  &&  prlMsd->IsVirtuel)
		{
			olMsdList.Add(prlMsd);
			olRc = true;
		}
	}



	for(int i = olMsdList.GetSize() - 1; i >= 0; i--)
	{
		DeleteMsdInternal(&olMsdList[i]);
	}
	
	return olRc;
}

/*
bool CedaMsdData::DeleteAllMsdsWithRosu(long lpRosu,bool bpWithDdx)
{
	bool olRc = false;
	POSITION rlPos;
	CCSPtrArray<MSDDATA> olMsdList;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu==lpRosu)
		{
			olMsdList.Add(prlMsd);
			olRc = true;
		}
	}

	if(bpWithDdx)
	{
		for(int j = olMsdList.GetSize()-1;j>=0;j--)
		{
			MSDDATA *prlMsd = &olMsdList[j];
			prlMsd->IsChanged = DATA_DELETED;
			ogDdx.DataChanged((void *)this,MSD_DELETE,(void *)prlMsd);
			DeleteMsdInternal(&olMsdList[j]);	
		}
	}
	olMsdList.RemoveAll();
	return olRc;
}
*/

bool CedaMsdData::DeleteMsdWithRos(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx)
{
	bool olRc = false;
	POSITION rlPos;
	CCSPtrArray<MSDDATA> olMsdList;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu==lpRosu && IsBetween(prlMsd->Esbg,opStart,opEnd))
		{
			olMsdList.Add(prlMsd);
			olRc = true;
			//break;
		}
	}

	for(int j = olMsdList.GetSize()-1;j>=0;j--)
	{
		MSDDATA *prlMsd = &olMsdList[j];
		prlMsd->IsChanged = DATA_DELETED;
		if(bpWithDdx)
			ogDdx.DataChanged((void *)this,MSD_DELETE_SELF,(void *)prlMsd);
		DeleteMsdInternal(&olMsdList[j]);	
	}
	//olMsdList.RemoveAll();
	return olRc;
}


bool CedaMsdData::InsertMsd(MSDDATA *prpMsdData, BOOL bpWithSave,bool bpSendDdx)
{
	bool olRc = true;

	//prpMsdData->IsChanged = DATA_NEW;
	if(prpMsdData->IsChanged == DATA_UNCHANGED)
	{
		prpMsdData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveMsd(prpMsdData);
	}
	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,/*MSD_NEW*/MSD_CHANGE_SELF,(void *)prpMsdData);
	return olRc;
}

bool CedaMsdData::UpdateMsd(MSDDATA *prpMsdData, BOOL bpWithSave,bool bpSendDdx)
{
	bool olRc = true;

	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,MSD_CHANGE_SELF,(void *)prpMsdData);
	if(prpMsdData->IsChanged == DATA_UNCHANGED)
	{
		prpMsdData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveMsd(prpMsdData);

	}


	return olRc;

}


bool CedaMsdData::MsdExist(long Urno)
{
	// don't read from database anymore, just check internal array
	MSDDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
}

bool CedaMsdData::DnamExist(CString opDnam)
{
	MSDDATA *prlData;
	if(omDnamMap.Lookup(opDnam,(void *&)prlData) == TRUE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*void CedaMsdData::GetAllDnam(CStringArray &ropList)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		CString olDnam;
		omDnamMap.GetNextAssoc(rlPos, olDnam, (void *&)prlMsd);
		//ogDdx.DataChanged((void *)this,MSD_CHANGE,(void *)prlMsd);
		ropList.Add(CString(prlMsd->Dnam));
	}
}
*/

void  ProcessMsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogMsdData.ProcessMsdBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}


void  CedaMsdData::ProcessMsdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{


	if (ipDDXType == BC_MSD_CHANGE || ipDDXType == BC_MSD_NEW)
	{
		MSDDATA *prpMsd;
		struct BcStruct *prlMsdData;

		prlMsdData = (struct BcStruct *) vpDataPointer;

		// check if from myself
		char pclWks[200]="";
		CString olWks = prlMsdData->ReqId;
		::GetWorkstationName(pclWks);
		CString olWks2 = pclWks;


		long llUrno;

		if (strstr(prlMsdData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlMsdData->Selection);
		}
		else
		{
			llUrno = atol(prlMsdData->Selection);
		}
	
		if (omUrnoMap.Lookup((void*)llUrno,(void *& )prpMsd) == TRUE)
		{
			GetRecordFromItemList(prpMsd,prlMsdData->Fields,prlMsdData->Data);
			if(olWks == olWks2)
				ogDdx.DataChanged((void *)this,MSD_CHANGE_SELF,(void *)prpMsd);
			else
				ogDdx.DataChanged((void *)this,MSD_CHANGE,(void *)prpMsd);
		}
		else
		{
			prpMsd = new MSDDATA;
			GetRecordFromItemList(prpMsd,prlMsdData->Fields,prlMsdData->Data);
			PrepareMsdData(prpMsd);
			omData.Add(prpMsd);
			omUrnoMap.SetAt((void*)prpMsd->Urno,prpMsd);
			if(olWks == olWks2)
				ogDdx.DataChanged((void *)this,MSD_NEW_SELF,(void *)prpMsd);
			else
				ogDdx.DataChanged((void *)this,MSD_NEW,(void *)prpMsd);

		}
	}
	else if (ipDDXType == BC_MSD_DELETE)
	{
		MSDDATA *prpMsd;
		struct BcStruct *prlMsdData;

		prlMsdData = (struct BcStruct *) vpDataPointer;

		// check if from myself
		char pclWks[200]="";
		CString olWks = prlMsdData->ReqId;
		::GetWorkstationName(pclWks);
		CString olWks2 = pclWks;

		long llUrno;

		if (strstr(prlMsdData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlMsdData->Selection);
		}
		else
		{
			llUrno = atol(prlMsdData->Selection);
		}
	
		
		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpMsd) == TRUE)
		{
			if(olWks == olWks2)
				ogDdx.DataChanged((void *)this, MSD_DELETE_SELF,(void *)prpMsd );
			else
				ogDdx.DataChanged((void *)this, MSD_DELETE,(void *)prpMsd );

			DeleteMsdInternal(prpMsd);
		}
		
	}
	else if (ipDDXType == BC_RELMSD)
	{
		struct BcStruct *prlBcStruct;
		prlBcStruct = (struct BcStruct *) vpDataPointer;
		SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(atol(prlBcStruct->Data));					
		if (prlSdg)
		{
			MSDDATA *prlMsd;
			long llUrno;

			CCSPtrArray<MSDDATA> olList;
			POSITION rlPos;
			
			for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
				if(prlMsd->Sdgu == prlSdg->Urno)
				{
					olList.Add(prlMsd);
					prlMsd->IsChanged = DATA_DELETED;
				}
			}

			for(int i = olList.GetSize() - 1; i >= 0; i--)
			{
				DeleteMsdInternal(&olList[i]);
			}

			olList.RemoveAll();

			this->ReadMsdWithSdgu(prlSdg->Urno);
			
			ogDdx.DataChanged(this,RELMSD,(void *)prlSdg);
		}
	}

}



MSDDATA * CedaMsdData::GetMsdByUrno(long pcpUrno)
{
	MSDDATA *prpMsd;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpMsd) == TRUE)
	{
		return prpMsd;
	}
	return NULL;
}



bool CedaMsdData::ChangeMsdTime(long lpMsdUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave)
{
	MSDDATA *prlMsd = GetMsdByUrno(lpMsdUrno);
	if(prlMsd != NULL)
	{
		ogDdx.DataChanged((void *)this,MSD_CHANGE,(void *)prlMsd);
		if (prlMsd->IsChanged == DATA_UNCHANGED)
		{
			prlMsd->IsChanged = DATA_CHANGED;
		}
		if(bpWithSave == true)
		{
			UpdateMsd(prlMsd, TRUE);
		}
	}
    return true;
}


bool CedaMsdData::DeleteMsdInternal(MSDDATA *prpMsd)
{
	omUrnoMap.RemoveKey((void *)prpMsd->Urno);

	int ilMsdCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMsdCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpMsd->Urno)
		{
			omData.DeleteAt(ilLc);
			break;
		}
	}
    return true;
}



bool CedaMsdData::SaveMsd(MSDDATA *prpMsd)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpMsd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpMsd->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMsd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpMsd->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpMsd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMsd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpMsd->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		if(!prpMsd->IsVirtuel)
		{
			sprintf(pclSelection, "WHERE URNO = '%ld'", prpMsd->Urno);
			olRc = CedaAction("DRT",pclSelection);
			if(!omLastErrorMessage.IsEmpty())
			{
				::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
			}
		}
		break;
	}

   return olRc;
}




void CedaMsdData::GetMsdByBsdu(long lpBsdu,CCSPtrArray<MSDDATA> *popMsdData)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Bsdu == lpBsdu)
		{
			popMsdData->Add(prlMsd);
		}
	}
}




void CedaMsdData::DeleteBySdgu(long lpSdgUrno)
{
	MSDDATA *prlMsd;
	long llUrno;

	CCSPtrArray<MSDDATA> olList;
	POSITION rlPos;
	
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu == lpSdgUrno)
		{
			olList.Add(prlMsd);
			prlMsd->IsChanged = DATA_DELETED;
		}
	}

	for(int i = olList.GetSize() - 1; i >= 0; i--)
	{
		DeleteMsdInternal(&olList[i]);
	}

	char pclSelection[512];
	sprintf(pclSelection, "WHERE SDGU = '%ld'", lpSdgUrno);
	bool olRc = CedaAction("DRT",pclSelection);

	olList.RemoveAll();
}


bool CedaMsdData::UpdateMsdInternal(MSDDATA *prpMsd)
{
	bool blRet = true;
	if(prpMsd!=NULL)
	{
		MSDDATA* prlMsd = GetMsdByUrno(prpMsd->Urno);
		if(prlMsd==NULL)
		{
			blRet = false;
		}
		else
		{
			*prlMsd = *prpMsd;
		}
	}
	else
	{
		blRet = false;
	}
    return blRet;

}


bool CedaMsdData::ChangeTime(long lpUrno,CTime opStartTime,CTime opEndTime,GanttDynType bpDynType,BOOL bpSave /* TRUE */)
{
	MSDDATA *prlMsd = GetMsdByUrno(lpUrno);
	CTimeSpan olTimeDiff,olBrkDura;
	CString olStartTime,olEndTime;
	bool blRet = false;
	olStartTime = opStartTime.Format("%Y.%m.%d - %H:%M");
	olEndTime = opEndTime.Format("%Y.%m.%d - %H:%M");

	if(prlMsd!=NULL)
	{
		switch(bpDynType)
		{
		case GANTT_BREAK:
			if(IsEnveloped(opStartTime, opEndTime, prlMsd->Bkf1,prlMsd->Bkt1))
			{
				if(prlMsd->Brkf != opStartTime || prlMsd->Brkt != opEndTime)
				{
					prlMsd->Brkf = opStartTime;
					prlMsd->Brkt = opEndTime;
					UpdateMsd(prlMsd,bpSave);
					blRet =true;
				}
			}
			break;
		case GANTT_BREAK_PERIOD:
			if((strcmp(prlMsd->Type,"D")!=0 && (prlMsd->Bkf1 != opStartTime || prlMsd->Bkt1 != opEndTime) && IsEnveloped(opStartTime,opEndTime,prlMsd->Esbg,prlMsd->Lsen)) || (strcmp(prlMsd->Type,"D")==0 && IsEnveloped(opStartTime,opEndTime,prlMsd->Sbgi,prlMsd->Seni)))
			{
				prlMsd->Bkf1 = opStartTime;
				prlMsd->Bkt1 = opEndTime;
				UpdateMsd(prlMsd,bpSave);
				blRet =true;
			}
			break;
		case GANTT_SHIFT:
			//(prlMsd->Sbgi != opStartTime || prlMsd->Seni == opEndTime)
			if(IsEnveloped(opStartTime, opEndTime, prlMsd->Esbg,prlMsd->Lsen) && IsEnveloped( prlMsd->Bkf1,prlMsd->Bkt1,opStartTime, opEndTime))
			{
				prlMsd->Sbgi = opStartTime;
				prlMsd->Seni = opEndTime;
				UpdateMsd(prlMsd,bpSave);
				blRet = true;
			}
		default:
			break;
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CCSPtrArray<MSDDATA> &opMsdArray)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			MSDDATA *prlMsd;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
			//TRACE("Begin <%s> End <%s>\n",prlMsd->Esbg.Format("%Y.%m.%d-%H:%M"),prlMsd->Lsen.Format("%Y.%m.%d-%H:%M"));
			if(prlMsd->Esbg<=opEnd && prlMsd->Lsen>=opStart && prlMsd->IsChanged!=DATA_DELETED)
			{
				opMsdArray.Add(prlMsd);
				blRet = true;
			}
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CMapPtrToPtr &opMsdUrnoMap)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			MSDDATA *prlMsd;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
			//TRACE("Begin <%s> End <%s>\n",prlMsd->Esbg.Format("%Y.%m.%d-%H:%M"),prlMsd->Lsen.Format("%Y.%m.%d-%H:%M"));
			CString olStr = CString(prlMsd->Fctc);
			
			if(prlMsd->Esbg<=opEnd && prlMsd->Lsen>=opStart && prlMsd->IsChanged!=DATA_DELETED && !prlMsd->IsVirtuel)
			{
				opMsdUrnoMap.SetAt((void *)prlMsd->Urno,prlMsd);
				blRet = true;
			}
		}
	}
	return blRet;
}


bool CedaMsdData::GetMsdsBetweenTimesAndDrrFilter(CTime opStart,CTime opEnd,CMapPtrToPtr &opMsdUrnoMap,bool bpShowActualDsr,int ipLevel)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			MSDDATA *prlMsd;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
			
			CString olStr = CString(prlMsd->Fctc);
			
			bool blPassFilter = true;
			if(bpShowActualDsr)
			{
				if(*prlMsd->StatPlSt != 'A')
				{
					blPassFilter = false;
				}
			}
			else
			{
				if(prlMsd->Planungsstufe != ipLevel)
				{
					blPassFilter = false;
				}
			}

			if(blPassFilter && prlMsd->Esbg<=opEnd && prlMsd->Lsen>=opStart && *prlMsd->Type == 'R' && prlMsd->IsChanged!=DATA_DELETED)
			{
				opMsdUrnoMap.SetAt((void *)prlMsd->Urno,prlMsd);
				blRet = true;
			}
		}
	}
	return blRet;
}


bool CedaMsdData::GetMsdByGplUrno(long lpGplu, CCSPtrArray <MSDDATA> &opMsdData)
{
	bool blRet =false;
	
	opMsdData.RemoveAll();

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
	
		if(prlMsd->Sdgu == lpGplu )
		{
			opMsdData.Add(prlMsd);
			blRet = true;
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdByGspUrno(long lpGspu, CCSPtrArray <MSDDATA> &opMsdData)
{
	bool blRet =false;
	
	opMsdData.RemoveAll();

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
	
		if(prlMsd->GspUrno == lpGspu )
		{
			opMsdData.Add(prlMsd);
			blRet = true;
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdsBySdgu(long lpSdgu,CCSPtrArray <MSDDATA> &opMsdData,bool bpAdd)
{
	bool blRet =false;
	if(!bpAdd)
		opMsdData.RemoveAll();

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
	
		if(prlMsd->Sdgu == lpSdgu)
		{
			opMsdData.Add(prlMsd);
			blRet = true;
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,long lpSdgu,CMapPtrToPtr &opMsdUrnoMap)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			MSDDATA *prlMsd;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
			//TRACE("Begin <%s> End <%s>\n",prlMsd->Esbg.Format("%Y.%m.%d-%H:%M"),prlMsd->Lsen.Format("%Y.%m.%d-%H:%M"));
			CString olStr = CString(prlMsd->Fctc);
			if(prlMsd->Esbg<=opEnd && prlMsd->Lsen>=opStart && prlMsd->IsChanged!=DATA_DELETED && prlMsd->Sdgu == lpSdgu && prlMsd->IsVirtuel)
			{
				opMsdUrnoMap.SetAt((void *)prlMsd->Urno,prlMsd);
				blRet = true;
			}
		}
	}
	return blRet;
}

bool CedaMsdData::GetMsdsByGplus(CString opGplus,CMapPtrToPtr &opMsdUrnoMap)
{
	bool blRet =false;

	POSITION rlPos;

	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		//TRACE("Begin <%s> End <%s>\n",prlMsd->Esbg.Format("%Y.%m.%d-%H:%M"),prlMsd->Lsen.Format("%Y.%m.%d-%H:%M"));
		CString olStr;
		olStr.Format(",%ld,",prlMsd->Sdgu);
		if(opGplus.Find(olStr) > -1)
		{
			opMsdUrnoMap.SetAt((void *)prlMsd->Urno,prlMsd);
			blRet = true;
		}
	}
	return blRet;
}



void CedaMsdData::PrepareMsdData(MSDDATA *prpMsd)
{

}
bool CedaMsdData::Release(long lpSdgu)
{
	CCS_TRY;

	if (lpSdgu == 0)	// no copy operation
	{
		// Kommandostrings f�r CEDA: INSERT-, UPDATE- und DELETE-Kommando
		CString olIRT, olURT, olDRT;
		// Feldliste: beschreibt die zu speichernden Felder der Tabelle
		CString olFieldList;
		// vollst�ndige Kommandos (Update, Insert und Delete) f�r CedaData::CedaAction()
		// Format:	1.Zeile:		Kommando, Feldliste
		//			1.-n. Zeile:	relevante Felder des Datensatzes (zum L�schen reicht die Urno, sonst alle Felder)
		CString olUpdateString, olInsertString, olDeleteString;
		// Zwischenspeicher
		CString olTmpText;
		// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
		int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;
		// Anzahl der Felder pro Datensatz ermitteln
		CStringArray olFields;	// nur Hilfsarray, wird nicht weiter benutzt
		int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 

		// Kommandostrings (Header) generieren
		olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
		olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
		olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

		CString olOrigFieldList;
		// Paketz�hler: alle hundert Datens�tze einen Schreibvorgang anstossen
		int ilCurrentCount = 0;
		// Gesamtz�hler, wird am Ende benutzt, um zu pr�fen, ob mind. 1 DS geschrieben wurde
		int ilTotalCount = 0;

		// alte Feldliste speichern
		olOrigFieldList = CString(pcmListOfFields);
		// Strings f�r CedaAction initialisieren
		olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

		// Datensynchronisation anstossen
		Synchronise("COVERAGE",true);

		// nein -> dann alle intern gecachten Datens�tze Speichern
		POSITION rlPos;
		for (rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			////////////////////////////////////////////////////////////////////////
			//// �nderung pr�fen und Pakete mit Datens�tzen schn�ren
			////////////////////////////////////////////////////////////////////////
			// die Datensatz-Urno
			long llUrno;
			// der Datensatz
			MSDDATA *prlMSD;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMSD);
			if (prlMSD->IsChanged != DATA_UNCHANGED)
			{
				CString olListOfData;
				CString olCurrentUrno;
				olCurrentUrno.Format("%d",prlMSD->Urno);
				// aus dem Datensatz einen String generieren (omResInfo ist Member von CCSCedaData)
				MakeCedaData(&omRecInfo,olListOfData,prlMSD);
				// was soll mit dem Datensatz geschehen
				switch(prlMSD->IsChanged)
				{
				case DATA_NEW:	// neuer DS
					olInsertString += olListOfData + CString("\n");
					ilNoOfInserts++;
					break;
				case DATA_CHANGED:	// ge�nderter DS
					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoOfUpdates++;
					break;
				case DATA_DELETED:	// DS l�schen
					olDeleteString += olCurrentUrno + CString("\n");
					ilNoOfDeletes++;
					break;
				}
				// Z�hler inkrementieren
				ilCurrentCount++;
				ilTotalCount++;

				// �nderungsflag zur�cksetzen
				prlMSD->IsChanged = DATA_UNCHANGED;
			}
			////////////////////////////////////////////////////////////////////////
			//// Ende �nderung pr�fen und Pakete mit Datens�tzen schn�ren
			////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////
			//// 500er Paket in Datenbank speichern
			////////////////////////////////////////////////////////////////////////
			// 500 Datens�tze im Paket?
			if(ilCurrentCount == 500)
			{
				// ja -> Paket speichern
				// neue Datens�tze erzeugt?
				if(ilNoOfInserts > 0)
				{
					// ja -> speichern
					CedaAction("REL","LATE,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
				}
				// Datens�tze ge�ndert?
				if(ilNoOfUpdates > 0)
				{
					// ja -> speichern
					CedaAction("REL","LATE,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
				}
				// Datens�tze gel�scht?
				if(ilNoOfDeletes > 0)
				{
					// ja -> aus Datenbank l�schen
					CedaAction("REL","LATE,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
				}
				
				// Z�hler und Strings wieder auf Initialwerte setzen
				ilCurrentCount = 0;
				ilNoOfInserts = 0;
				ilNoOfUpdates = 0;
				ilNoOfDeletes = 0;
				olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
				olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
				olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
			}
			////////////////////////////////////////////////////////////////////////
			//// Ende 500er Paket in Datenbank speichern
			////////////////////////////////////////////////////////////////////////
		}//for(rlPos = ......
		
		
		////////////////////////////////////////////////////////////////////////
		//// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		// neue Datens�tze erzeugt?
		if(ilNoOfInserts > 0)
		{
			// ja -> speichern
			CedaAction("REL","LATE,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
		}
		// Datens�tze ge�ndert?
		if(ilNoOfUpdates > 0)
		{
			// ja -> speichern
			CedaAction("REL","LATE,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
		}
		// Datens�tze gel�scht?
		if(ilNoOfDeletes > 0)
		{
			// ja -> aus Datenbank l�schen
			CedaAction("REL","LATE,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		//// Broadcast erzeugen
		////////////////////////////////////////////////////////////////////////

		// gab es neue/ge�nderte/gel�schte Datens�tze?
		/*if(ilTotalCount > 0)
		{
			// ja -> alle betroffenen Mitarbeiter durchgehen
			while(olStfUrnoArray.GetSize() > 0)
			{
				// Mitarbeiter-Z�hler
				int ilUrnos = 0;	
				// String-Puffer f�r die Urnos aller Mitarbeiter
				CString olTmpUrnos;
				// maximal je 60 Mitarbeiter-Urnos im String speichern
				while(ilUrnos < 60 && olStfUrnoArray.GetSize() > 0)
				{
					ilUrnos++;
					olTmpUrnos += CString(",") + olStfUrnoArray[0];
					olStfUrnoArray.RemoveAt(0);
				}
				// f�hrendes Komma eleminieren
				olTmpUrnos = olTmpUrnos.Mid(1);
				// String f�r Broadcasts generieren
				CString olData;
				olData.Format("%s-%s-%s-%s,%s", olRelFrom, olRelTo, olTmpUrnos, opFrom, opTo);
				MakeCedaString(olData);
				// CEDA veranlassen, einen Broadcast zu senden
				CedaAction("SBC","RELDRR","",CCSCedaData::pcmReqId,"",olData.GetBuffer(0));
			}
			// Broadcast f�r Coverage senden 
			CString olRange = olRelFrom + "-" + olRelTo;
			CedaAction("SBC","ENDRLR","","","",olRange.GetBuffer(0));
		}*/
		////////////////////////////////////////////////////////////////////////
		//// Ende Broadcast erzeugen
		////////////////////////////////////////////////////////////////////////
		
		// Datensynchronisation abschliessen
		Synchronise("COVERAGE");
	}		
	else	// copy operation
	{
		// Kommandostrings f�r CEDA: INSERT-, UPDATE- und DELETE-Kommando
		CString olIRT, olURT, olDRT;
		// Feldliste: beschreibt die zu speichernden Felder der Tabelle
		CString olFieldList;
		// vollst�ndige Kommandos (Update, Insert und Delete) f�r CedaData::CedaAction()
		// Format:	1.Zeile:		Kommando, Feldliste
		//			1.-n. Zeile:	relevante Felder des Datensatzes (zum L�schen reicht die Urno, sonst alle Felder)
		CString olUpdateString, olInsertString, olDeleteString;
		// Zwischenspeicher
		CString olTmpText;
		// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
		int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;
		// Anzahl der Felder pro Datensatz ermitteln
		CStringArray olFields;	// nur Hilfsarray, wird nicht weiter benutzt
		int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 

		// Kommandostrings (Header) generieren
		olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
		olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
		olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

		CString olOrigFieldList;
		// Paketz�hler: alle hundert Datens�tze einen Schreibvorgang anstossen
		int ilCurrentCount = 0;
		// Gesamtz�hler, wird am Ende benutzt, um zu pr�fen, ob mind. 1 DS geschrieben wurde
		int ilTotalCount = 0;

		// alte Feldliste speichern
		olOrigFieldList = CString(pcmListOfFields);
		// Strings f�r CedaAction initialisieren
		olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

		// nein -> dann alle intern gecachten Datens�tze Speichern
		POSITION rlPos;
		for (rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			////////////////////////////////////////////////////////////////////////
			//// �nderung pr�fen und Pakete mit Datens�tzen schn�ren
			////////////////////////////////////////////////////////////////////////
			// die Datensatz-Urno
			long llUrno;
			// der Datensatz
			MSDDATA *prlMSD;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMSD);
			if (prlMSD->IsChanged != DATA_UNCHANGED)
			{
				CString olListOfData;
				CString olCurrentUrno;
				olCurrentUrno.Format("%d",prlMSD->Urno);
				// aus dem Datensatz einen String generieren (omResInfo ist Member von CCSCedaData)
				MakeCedaData(&omRecInfo,olListOfData,prlMSD);
				// was soll mit dem Datensatz geschehen
				switch(prlMSD->IsChanged)
				{
				case DATA_NEW:	// neuer DS
					olInsertString += olListOfData + CString("\n");
					ilNoOfInserts++;
					break;
				case DATA_CHANGED:	// ge�nderter DS
					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoOfUpdates++;
					break;
				case DATA_DELETED:	// DS l�schen
					olDeleteString += olCurrentUrno + CString("\n");
					ilNoOfDeletes++;
					break;
				}
				// Z�hler inkrementieren
				ilCurrentCount++;
				ilTotalCount++;

				// �nderungsflag zur�cksetzen
				prlMSD->IsChanged = DATA_UNCHANGED;
			}
			////////////////////////////////////////////////////////////////////////
			//// Ende �nderung pr�fen und Pakete mit Datens�tzen schn�ren
			////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////
			//// 500er Paket in Datenbank speichern
			////////////////////////////////////////////////////////////////////////
			// 500 Datens�tze im Paket?
			if(ilCurrentCount == 500)
			{
				// ja -> Paket speichern
				// neue Datens�tze erzeugt?
				if(ilNoOfInserts > 0)
				{
					// ja -> speichern
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
				}
				// Datens�tze ge�ndert?
				if(ilNoOfUpdates > 0)
				{
					// ja -> speichern
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
				}
				// Datens�tze gel�scht?
				if(ilNoOfDeletes > 0)
				{
					// ja -> aus Datenbank l�schen
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
				}
				
				// Z�hler und Strings wieder auf Initialwerte setzen
				ilCurrentCount = 0;
				ilNoOfInserts = 0;
				ilNoOfUpdates = 0;
				ilNoOfDeletes = 0;
				olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
				olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
				olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
			}
			////////////////////////////////////////////////////////////////////////
			//// Ende 500er Paket in Datenbank speichern
			////////////////////////////////////////////////////////////////////////
		}//for(rlPos = ......
		
		
		////////////////////////////////////////////////////////////////////////
		//// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		// neue Datens�tze erzeugt?
		if(ilNoOfInserts > 0)
		{
			// ja -> speichern
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
		}
		// Datens�tze ge�ndert?
		if(ilNoOfUpdates > 0)
		{
			// ja -> speichern
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
		}
		// Datens�tze gel�scht?
		if(ilNoOfDeletes > 0)
		{
			// ja -> aus Datenbank l�schen
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		//// Ende Broadcast erzeugen
		////////////////////////////////////////////////////////////////////////
		CString olData;
		olData.Format("%ld",lpSdgu);
		CedaAction("SBC","RELMSD","",CCSCedaData::pcmReqId,"",olData.GetBuffer(0));
	}

	CCS_CATCH_ALL;
	return true;
}


bool CedaMsdData::Synchronise(CString opUnknown, bool bpActive)
{
	CCS_TRY;
	CTime olCurrentTime = CTime::GetCurrentTime(); 
	CTime olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";
	char pclSelection[1024]="";
	if (bpActive)
	{
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s,N,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		strcpy(pclSelection, "LATE");
		CedaAction("REL",pclSelection,"",pclData);
		// Synchronisieren OK?
		if(CString(pclSelection).Find("NOT OK") != -1)
		{
			// terminieren
			return false;
		}
	}
	else
	{ 
		// Synchronisation beenden
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s, ,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		CedaAction("REL","LATE","",pclData);
	}

	return true;
	CCS_CATCH_ALL;
	return false;
}