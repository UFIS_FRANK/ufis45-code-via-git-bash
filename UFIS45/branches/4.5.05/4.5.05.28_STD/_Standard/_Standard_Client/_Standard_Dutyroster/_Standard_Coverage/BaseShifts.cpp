// BaseShifts.cpp: implementation of the CBaseShifts class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <coverage.h>
#include <BaseShifts.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaseShifts::CBaseShifts(BOOL bpCopy)
{
	imStartOffSet = 0;
	imShiftCount = 0;
	bmCopy = bpCopy;
}

CBaseShifts::~CBaseShifts()
{
	if (bmCopy)
	{
		omBaseShifts.DeleteAll();
	}
	else
	{
		omBaseShifts.RemoveAll();
	}
}

void CBaseShifts::InitBaseShifts(CCSPtrArray <BASESHIFT> ropBaseShifts, CString opFctc)
{
	CString olFctc = "";

	for(int illc = 0; illc < ropBaseShifts.GetSize();illc++)
	{
		olFctc = ropBaseShifts[illc].Fctc;
		if(olFctc.IsEmpty() || olFctc == opFctc)
		{
			if (bmCopy)
			{
				int ilInd = omBaseShifts.New(ropBaseShifts[illc]);
				omBaseShifts[ilInd].Fctc = opFctc;
			}
			else
			{
				ropBaseShifts[illc].Fctc = opFctc;
				omBaseShifts.Add(&ropBaseShifts[illc]);
			}
		}
	}

	imShiftCount = omBaseShifts.GetSize();
}



int CBaseShifts::GetShiftCount()
{
	return imShiftCount;
}
