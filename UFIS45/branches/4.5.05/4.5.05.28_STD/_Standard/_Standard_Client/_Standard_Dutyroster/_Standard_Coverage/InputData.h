// InputData.h: interface for the CInputData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INPUTDATA_H__6C0340E1_2DD5_11D2_B0FC_204C4F4F5020__INCLUDED_)
#define AFX_INPUTDATA_H__6C0340E1_2DD5_11D2_B0FC_204C4F4F5020__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CInputData  
{
private:
	int n_Input;
public:
	int GetInputSize();
	
	CUIntArray Data;
	CUIntArray InData;
	CUIntArray OptData;
	CInputData();
	void InitData(CUIntArray &ropInputData);
	virtual ~CInputData();

};

#endif // !defined(AFX_INPUTDATA_H__6C0340E1_2DD5_11D2_B0FC_204C4F4F5020__INCLUDED_)
