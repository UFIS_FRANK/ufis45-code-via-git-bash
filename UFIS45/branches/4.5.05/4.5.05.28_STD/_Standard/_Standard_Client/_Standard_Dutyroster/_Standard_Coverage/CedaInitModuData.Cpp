// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>
const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = "Coverage,InitModu,InitModu,Initialisieren (InitModu),B,-";
			//olInitModuData += "Anzeige Button,m_ANSICHT,Ansicht,B,1";

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	//olInitModuData += "Desktop,DESKTOP_CB_Warteraeume,Warteräume,B,1,";
	olInitModuData += ",Desktop,m_REGELN,Regeln Button,B,1";
	olInitModuData += GetInitModuTxtG();

	
	//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	/*
	FILE *prlFp = fopen("c:\\Personal\\Test.txt","w");
	if(prlFp!=NULL)
	{
		fprintf(prlFp,"%s",pclInitModuData);
		fclose(prlFp);
	}
	*/
	delete pclInitModuData;

	return ilRc;
}


//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = "Coverage,InitModu,InitModu,Initialisieren (InitModu),B,-";
			//olInitModuData += "Anzeige Button,m_ANSICHT,Ansicht,B,1";

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	//olInitModuData += "Desktop,DESKTOP_CB_Warteraeume,Warteräume,B,1,";
	olInitModuData += ",Desktop,m_REGELN,Regeln Button,B,1";
	olInitModuData += GetInitModuTxtG();

	return olInitModuData;
}
//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olTmpStr="";
	for(int ilIndex =0;ilIndex<10;ilIndex++)
	{
		switch(ilIndex)
		{
		case 0:
			//Toolbar
			olTmpStr += ",Coverage Toolbar,COV_TB_VIEWBTN," + LoadStg(IDS_STRING61222) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_PRINTBTN," + LoadStg(IDS_STRING61223) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_MASSSTABBTN," + LoadStg(IDS_STRING61224) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_SEARCHBTN," + LoadStg(IDS_STRING61221) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_LTIMEFRAMEBTN," + LoadStg(IDS_STRING61225) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_EVALUATEBTN," + LoadStg(IDS_STRING61226) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_CONFBTN," + LoadStg(IDS_STRING61219) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_OPENFLIGHTSBTN," + LoadStg(IDS_STRING61220) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_STATISTICBTN," + LoadStg(IDS_STRING61227) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_SIMULATIONBTN," + LoadStg(IDS_STRING61228) + ",B,1";
			olTmpStr += ",Coverage Toolbar,COV_TB_SETUPBTN," + LoadStg(IDS_STRING1850) + ",B,1";


			olTmpStr += ",Conflict dialog,CONFLICT_DLG_DeleteList," + LoadStg(IDS_STRING1478) + ",B,1";
			olTmpStr += ",Conflict dialog,CONFLICT_DLG_DBL_CLICK," + LoadStg(IDS_STRING1539) + ",F,1";
			olTmpStr += ",Conflict dialog,CONFLICT_DLG_DELSERVICE," + LoadStg(IDS_STRING1002) + ",I,1";


			//COVERAGE DIALOG
			olTmpStr += ",COVERAGE_DLG,COV_DLG_VIEW," + LoadStg(SHIFT_B_VIEW) + ",G,1";
			olTmpStr +=	",COVERAGE_DLG,COV_DLG_PRINT," + LoadStg(SHIFT_B_PRINT) + ",B,1";
			olTmpStr +=	",COVERAGE_DLG,COV_DLG_EVAL," + LoadStg(IDS_STRING1480) + ",B,1";
			olTmpStr +=	",COVERAGE_DLG,COV_DLG_CONFLICTS," + LoadStg(IDS_STRING1474) + ",B,1";
			olTmpStr += ",COVERAGE_DLG,COV_DLG_FLWORES," + LoadStg(IDS_STRING1481) + ",B,1";

			// Setup dialogue
			olTmpStr += ",Setup dialog,SETUP_DLG_SaveAsDefault," + LoadStg(IDS_STRING1851) + ",B,1";
			olTmpStr += ",Setup dialog,SETUP_DLG_LoadFromDefault," + LoadStg(IDS_STRING1852) + ",B,1";
			
			break;
			
		case 1:

#ifdef	AUTOCOVERAGE
			// AutoCoverage dialogue
			olTmpStr += ",AUTOCOV_DLG,AUTOCOV_DLG_ShowInternals," + LoadStg(IDS_STRING1947) + ",B,-";
#endif

			break;
		case 2:
			//JOB_DLG
			olTmpStr += ",Job dialog,JOB_DLG," + LoadStg(IDS_STRING1540) + ",G,1";
			olTmpStr += ",Job dialog,JOB_DLG_SAVE," + LoadStg(IDS_STRING565) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_NEW," + LoadStg(SHIFT_B_NEW) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_COPY," + LoadStg(ST_COPY) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_DELETE," + LoadStg(IDS_STRING966) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_CONTRACT," + LoadStg(SHIFT_S_CONTRACT) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_AGENT," + LoadStg(IDS_STRING1512) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_TELEX," + LoadStg(IDS_STRING1513) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_TELEX_D," + LoadStg(IDS_STRING1514) + ",B,1";
			olTmpStr += ",Job dialog,JOB_DLG_ROTATION," + LoadStg(IDS_STRING1557) + ",B,1";


			/****************************************************
			// Arrival
			*****************************************************/
			/*
			olTmpStr += ",Job dialog,JOB_DLG_A_FLNO," + LoadStg(IDS_STRING1547) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_CXXD," + LoadStg(IDS_STRING944) + ",X,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_ORG3," + LoadStg(IDS_STRING1116) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_VIA3," + LoadStg(IDS_STRING1322) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_STOA," + LoadStg(IDS_STRING1130) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_TMOA," + LoadStg(IDS_STRING1281) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_ETAI," + LoadStg(IDS_STRING1185) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_LAND," + LoadStg(IDS_STRING1218) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_ONBL," + LoadStg(IDS_STRING1227) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_PSTA," + LoadStg(IDS_STRING1122) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_GTAI," + LoadStg(IDS_STRING1136) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_BLTI," + LoadStg(IDS_STRING1036) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_TTYP," + LoadStg(IDS_STRING1296) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_HTYP," + LoadStg(IDS_STRING1110) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_PAXI," + LoadStg(IDS_STRING1037) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_BAGN," + LoadStg(IDS_STRING1039) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_CGOT," + LoadStg(IDS_STRING1038) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_MAIL," + LoadStg(IDS_STRING1040) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_CCXA," + LoadStg(IDS_STRING1233) + ",X,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_REMA," + LoadStg(IDS_STRING1244) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_A_DES3," + LoadStg(IDS_STRING1177) + ",E,1";
			*/
			/****************************************************
			// Departure
			*****************************************************/
			/*
			olTmpStr += ",Job dialog,JOB_DLG_D_FLNO," + LoadStg(IDS_STRING1548) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_CXXD," + LoadStg(IDS_STRING943) + ",X,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_DES3," + LoadStg(IDS_STRING1177) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_VIA3," + LoadStg(IDS_STRING1322) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_STOD," + LoadStg(IDS_STRING1131) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_ETDI," + LoadStg(IDS_STRING1188) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_SLOT," + LoadStg(IDS_STRING1250) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_OFBL," + LoadStg(IDS_STRING1224) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_AIRB," + LoadStg(IDS_STRING1158) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_PSTD," + LoadStg(IDS_STRING1123) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_GTDI," + LoadStg(IDS_STRING1138) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_CKIC," + LoadStg(IDS_STRING1517) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_COPN," + LoadStg(IDS_STRING1516) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_BAZI," + LoadStg(IDS_STRING1515) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_TTYP," + LoadStg(IDS_STRING1310) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_HTYP," + LoadStg(IDS_STRING1110) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_PAXI," + LoadStg(IDS_STRING1037) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_BAGN," + LoadStg(IDS_STRING1166) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_CGOT," + LoadStg(IDS_STRING1170) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_MAIL," + LoadStg(IDS_STRING1040) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_CCXD," + LoadStg(IDS_STRING1233) + ",X,1";
			olTmpStr += ",Job dialog,JOB_DLG_D_REMA," + LoadStg(IDS_STRING1244) + ",E,1";
			*/
			/* *******************************************
			*   Rest code
			* ********************************************/
			olTmpStr += ",Job dialog,JOB_DLG_TIME," + LoadStg(IDS_STRING1043) + ",G,1";
			olTmpStr += ",Job dialog,JOB_DLG_ETTA," + LoadStg(IDS_STRING1520) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_ETTS," + LoadStg(IDS_STRING1520) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_PRTA," + LoadStg(IDS_STRING870) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_PRTS," + LoadStg(IDS_STRING870) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_TTDA," + LoadStg(IDS_STRING1521) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_TTDS," + LoadStg(IDS_STRING1521) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_DTIA," + LoadStg(IDS_STRING1522) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_DTIS," + LoadStg(IDS_STRING1522) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_TFDA," + LoadStg(IDS_STRING1523) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_TFDS," + LoadStg(IDS_STRING1523) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_POTA," + LoadStg(IDS_STRING866) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_POTS," + LoadStg(IDS_STRING866) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_PRIO1," + LoadStg(PR_DSR_PRIORITY) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_PRIO," + LoadStg(PR_DSR_PRIORITY) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_DRTI," + LoadStg(IDS_STRING1518) + ",C,1";
			olTmpStr += ",Job dialog,JOB_DLG_DUST," + LoadStg(IDS_STRING1519) + ",C,1";
			olTmpStr += ",Job dialog,JOB_DLG_VRGC," + LoadStg(IDS_STRING1012) + ",L,1";
			olTmpStr += ",Job dialog,JOB_DLG_REMA," + LoadStg(IDS_STRING449) + ",E,1";
			olTmpStr += ",Job dialog,JOB_DLG_ARRDEP,"+LoadStg(IDS_STRING1140)+"/"+LoadStg(IDS_STRING1141)+ ",B,1";


			
			break;
		case 3:
		

			//NO_RES_FLT
			olTmpStr += ",No resources list,NRFS_DBL_CLICK," + LoadStg(IDS_STRING1539) + ",F,1";			  
			  
	
			//REGISTERDLG
//			olTmpStr += ",REGISTERDLG,REGISTERDLG_TEXT," + LoadStg(IDS_STRING1468) + ",E,1";
			olTmpStr += ",REGISTERDLG,REGISTERDLG_TIME," + LoadStg(ST_ZEIT) + ",E,1";
//			olTmpStr += ",REGISTERDLG,REGISTERDLG_STARTEN," + LoadStg(IDS_STRING1586) + ",B,1";
			olTmpStr += ",REGISTERDLG,REGISTERDLG_ABBRECHEN," + LoadStg(IDS_STRING522) + ",B,1";
//			olTmpStr += ",REGISTERDLG,REGISTERDLG_REGISTER," + LoadStg(IDS_STRING1551) + ",B,1";
			
			break;
		case 4:
			break;
		default:
			break;
		}
	}

	return olTmpStr;
}
