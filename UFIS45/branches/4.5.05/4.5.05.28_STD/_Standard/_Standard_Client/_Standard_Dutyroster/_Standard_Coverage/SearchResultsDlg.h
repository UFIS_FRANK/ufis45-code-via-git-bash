// SearchResultsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SearchResultsDlg dialog

#ifndef __SEARCH_RESULTS_DLG_
#define __SEARCH_RESULTS_DLG_

/////////////////////////////////////////////////////////////////////////////
// Class declaration of SearchResultsDlg

//@Man:
/*@Doc:
  No comment on this up to now.
*/

//#include "Search.h"
#include <basicdata.h>
#include <GridFenster.h>
#include <CedaflightData.h>

struct FLIGHTSEARCHDATA
{
	long Furn;
	long Urna;
	long Urnd;
	char Flnoa[15];
	char Flnod[15];
	char Regna[15];
	char Regnd[15];
	char Act3a[7];
	char Act3d[7];
	char Orig[7];
	char Dest[7];
	char Tifa[20];
	char Tifd[20];
	char PosA[7];
	char PosD[7];
	char SortTifa[20];
	char SortTifd[20];
	bool IsLoaded;
	bool InGrayed;
	bool OutGrayed;
	FLIGHTSEARCHDATA()
	{
		memset(this,'\0',sizeof(*this));
		IsLoaded = false;
		InGrayed = false;
		OutGrayed = false;
		Furn = 0L;
		Urna = 0L;
		Urnd = 0L;
	}
};

class SearchResultsDlg : public CDialog
{
// Construction
public:
    //@ManMemo: Default constructor
	SearchResultsDlg::SearchResultsDlg(CCSPtrArray<FLIGHTSEARCHDATA> &ropFlights,CWnd* pParent = NULL, CString opFilter = CString(""));  

	SearchResultsDlg::~SearchResultsDlg();
// Dialog Data
	//{{AFX_DATA(SearchResultsDlg)
	enum { IDD = IDD_SEARCH_RESULTS };
	CStatic	m_SearchFilter;
	CStatic	m_TextStatic;
	//}}AFX_DATA

public:

	 void DoCreate() ;
 
	void SetCaptionText(CString opCaption);

	void AttachTsrArray(const CStringArray * popTsrArray);
	void AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap);

	void AttachFlightData(CedaFlightData *popCedaFlightData);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SearchResultsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	private:
    //@ManMemo: imWhichSearch
	int imWhichSearch;
	int imCol;
	int imRow; 

	CMapPtrToPtr omDemMap;

	const CStringArray *pomTsrArray;

	const CMapPtrToPtr *pomFlightDemMap;

	CString omCaption;
	CedaFlightData *pomCedaFlightData;
	CGridFenster *pomQueryList;

	CCSPtrArray <FLIGHTSEARCHDATA> omFlights;

    //@ManMemo: pcmFilter
	CString omFilter;

private:
	static	void ProcessSearchResultsCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
			void UpdateFields(FLIGHTDATA *popFlight,bool bpUpdate);

// Implementation
protected:
	BOOL  OnLButtonClickedRowCol(UINT wParam, LONG lParam) ;
	BOOL  OnLButtonDblClickedRowCol(UINT wParam, LONG lParam) ;
	// Generated message map functions
	//{{AFX_MSG(SearchResultsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	COLORREF	cmGrayed;
};

#endif //__SEARCH_RESULTS_DLG_
