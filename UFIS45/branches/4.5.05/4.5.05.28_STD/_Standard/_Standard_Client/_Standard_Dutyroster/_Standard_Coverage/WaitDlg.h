#if !defined(AFX_WAITDLG_H__ED4E3745_8551_11D3_8FA0_00500454BF3F__INCLUDED_)
#define AFX_WAITDLG_H__ED4E3745_8551_11D3_8FA0_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaitDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CWaitDlg 

class CWaitDlg : public CDialog
{
// Konstruktion
public:
	CWaitDlg(CWnd* pParent = NULL,CString csText = "");   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CWaitDlg)
	enum { IDD = IDD_WAIT_DIALOG };
	CAnimateCtrl	m_oAnimate;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CWaitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	CString		m_csText;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CWaitDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_WAITDLG_H__ED4E3745_8551_11D3_8FA0_00500454BF3F__INCLUDED_
