// stviewer.h : header file
//

#ifndef __DISPODETAILGANTTVIEWER__
#define __DISPODETAILGANTTVIEWER__

//#include "cviewer.h"
#include <CCSPtrArray.h>
#include <CedaDemData.h>
#include <CedaFlightData.h>

struct DEM_BARDATA 
{
	long	Urno; //Unique Record Number
	long	Fkey; //Foreign Key of Premis
	long	Ghsu; //Foreign Key iof Ground handling Services
    CString Text; //Bar Text ==> dynamically generated
    CString Text2; //Bar Text2 ==> dynamically generated
	CString Dtyp; //Duty Type
	CString Rtyp; //Resourcen Typ
    CTime	Dube; //Duty Begin ==> from before/after onbl, ofbl
    CTime	Duen; //Duty End ==> from before/after onbl, ofbl

    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	COLORREF TextColor;
	COLORREF BkColor;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
};
struct DEM_BKBARDATA
{
	long Urno;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	DEM_BKBARDATA(void)
	{
		Urno = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
	}
};

struct DEM_LINEDATA
{
	long Urno;
	long Fkey;
	WORD Disp;
	WORD Dety;
	int RTyp;
	CTime Dube;
	CTime Duen;
	CString Lknm;
	CString Tnam;
	bool isAdditionalSpecial;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<DEM_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<DEM_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF BkColor;
	DEM_LINEDATA(void)
	{
		TextColor = ogColors[BLACK_IDX];
		BkColor = ogColors[SILVER_IDX];
		isAdditionalSpecial = TRUE;
		Lknm = CString("");
		//Lkco = "LKNM - Text";
	}
};
/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer window

class CJobDlg;
class StaffDiagram;

class DispoDetailGanttViewer
{
// Constructions
public:
    DispoDetailGanttViewer(long Fkey);
    ~DispoDetailGanttViewer();
	void ClearAll();

	void Attach(CJobDlg *popAttachWnd);
	void SetDemPointer(CMapPtrToPtr *popDemMap);
	void ChangeViewTo(long lpUrno, CTime opStartTime, CTime opEndTime);

	StaffDiagram *pomStafDiagram;
	CTime omStartTime, omEndTime;
	long lmFkey;
	FLIGHTDATA *prmFlightA;
	FLIGHTDATA *prmFlightD;
	
	CTime omOnBlock;//(1980, 1, 1, 1, 30, 0);
	CTime omOfBlock;//(1980, 1, 1, 5, 30, 0);
// Internal data processing routines
public:
	BOOL IsPassFilter(DEMDATA *prp);
	int CompareLine(DEM_LINEDATA *prpLine1, DEM_LINEDATA *prpLine2);
	void MakeLines();
	void MakeBars();
	void MakeLine(DEMDATA *prpDem);
	void MakeEmptyLine();
	void MakeBar(int ipLineno, DEMDATA *prpDem);

	CString LineText(DEMDATA *prpDem);
	CString BarTextAndValues(DEMDATA *prpDem);

	BOOL FindDutyBar(/*CTime opDube*/long Urno, int &ripLineno);
	BOOL FindBar(/*CTime Dube*/long Urno, int &ripLineno, int &ripBarno);
	BOOL FindBarGlobal(long Urno, int &ripLineno, int &ripBarno);
// Operations
public:
	void SetCurrentFkey(long lpFkey);
	void SetCurrentFlights(FLIGHTDATA *prpFlightA, FLIGHTDATA *prpFlightD);
	long GetCurrentFkey();
	int GetNextFreeLine();
    int GetLineCount();
	int GetRealLineCount();
    DEM_LINEDATA *GetLine( int ipLineno);
    CString GetLineText( int ipLineno, COLORREF *prpBkColor, COLORREF *prpTextColor);
	 int GetVisualMaxOverlapLevel(int ipLineno);
    int GetMaxOverlapLevel(int ipLineno);
    int GetBarCount(int ipLineno);
	int GetBkBarCount(int ipLineno);
	DEM_BKBARDATA *GetBkBar(int ipLineno, int ipBkBarno);
	CString GetBkBarText(int ipLineno, int ipBkBarno);
	void DeleteBkBar(int ipLineno, int ipBkBarno);
    DEM_BARDATA *GetBar( int ipLineno, int ipBarno);
    CString GetBarText(int ipLineno, int ipBarno);

    int CreateLine( DEM_LINEDATA *prpLine);
    void DeleteLine(int ipLineno);
    int CreateBar(int ipLineno, DEM_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipLineno, int ipBarno);

    int GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);

	DEMDATA *GetDemByUrno(long lpUrno);

private:
    int GetFirstOverlapBarno(DEM_LINEDATA *prpLine, int ipBarno);
    DEM_BARDATA *GetBarInTimeOrder(DEM_LINEDATA *prpLine, int ipBarno);
	 void GetOverlappedBarsFromTime( int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

    CTime GetEndTimeOfBarsOnTheLeft(DEM_LINEDATA *prpLine, int ipBarno);
	void ReorderBars(int ipLineno, int ilFirstBar, int ilTotalBars);

// Attributes used for filtering condition
private:

// Attributes
private:
	CJobDlg *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBarBrushGray;
    CCSPtrArray<DEM_LINEDATA> omLines;
	CStringArray omSortOrder;

	CTime omGroundStart;
	CTime omGroundEnd;

	CMapPtrToPtr *pomDemMap;

// Methods which handle changes (from Data Distributor)
public:
	void ProcessDemChange(DEMDATA *prpDem);
	void ProcessDemNew(DEMDATA *prpDem);
	void ProcessDemDelete(DEMDATA *prpDem);
	void ProcessPremisDelete(DEMDATA *prpDem);
};

/////////////////////////////////////////////////////////////////////////////

#endif //__DISPODETAILGANTTVIEWER__
