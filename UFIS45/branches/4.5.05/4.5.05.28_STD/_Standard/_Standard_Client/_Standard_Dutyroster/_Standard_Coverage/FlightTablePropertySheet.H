// flplanps.h : header file
//



#ifndef _FLIGHTTABLEPROPERTYSHEET_H_
#define _FLIGHTTABLEPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSFlightPage.h>
#include <PSAptFilterPage.h>
#include <PSAlcFilterPage.h>
#include <PSAltFilterPage.h>
#include <PSPosFilterPage.h>
#include <PSTemplateFilterPage.h>




/////////////////////////////////////////////////////////////////////////////
// FlightTablePropertySheet

class FlightTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	FlightTablePropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	//TestPage m_TestPage; 
	CPsFlightPage m_PsFlightPage;
	CPsAptFilterPage m_PsAptFilterPage;
	CPsAltFilterPage m_PsAltFilterPage;
	CPsTemplateFilterPage m_PsTemplateFilterPage;
	CPsAlcFilterPage m_PsAlcFilterPage;
	CPsPosFilterPage m_PsPosFilterPage;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _FLIGHTTABLEPROPERTYSHEET_H_
