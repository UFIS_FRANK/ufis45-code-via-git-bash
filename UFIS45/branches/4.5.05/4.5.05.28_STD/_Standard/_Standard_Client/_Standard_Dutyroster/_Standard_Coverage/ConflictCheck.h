// this class checks conflict for flights, duties and employees
// MWO: 03.07.1997


#ifndef __CONFLICT_CHECK__
#define __CONFLICT_CHECK__

#include <CedaFlightData.h>
//#include "CedaDsrData.h"
//#include "CedaGhdData.h"




enum
{
	CFI_F_ARRDEP_ASARR = -60,		//Annahme und Durchgang an einer Ankunft.
	CFI_F_ROTATIONSPLIT = 50,		//Rotation gesplittet an Durchgang.
	CFI_F_DURCH_BEREIT = -40,		//Durchgang --> Bereitstellung.
	CFI_F_DEP_NOMORE_CXX = -33,		//Abflug - Annulierung aufgehoben.
	CFI_F_ARR_NOMORE_CXX= -32,		//Ankunft - Annulierung aufgehoben
	CFI_F_DEP_CXX = -31,				//Abflug annuliert.
	CFI_F_ARR_CXX = -30,				//Ankunft annuliert
	CFI_F_DEP_CHANGED = -21,		//Abflug ge�ndert.
	CFI_F_ARR_CHANGED = -20,		//Ankunft ge�ndert
	CFI_F_NO_PREMIS = -10,			//Keine Pr�misse gefunden
	CFI_F_MORETHAN_1_PREMIS = -11,  //mehr als eine Pr�misse f�r den Flug gefunden
	CFI_F_EMPTY_PREMIS = -9,		//Regel gefunden, jedoch ohne Leistungsdaten
	CFI_F_EDT_CHANGED = -7,			//ETD ge�ndert
	CFI_F_EDA_CHANGED = -6,			//ETA ge�ndert
	CFI_F_EC_CHANGED = -5,			//E/C ge�ndert
	CFI_F_AC_CHANGED = -4,			//A/C ge�ndert
	CFI_F_NI_CHANGED = -3,			//NI-Zeit ge�ndert
	CFI_F_NO_RAMPAGENT = -2,		//Kein Rampagent zugeteilt
	CFI_NOCONFLICT = 0,
	CFI_NOTAKEOVER = 1,
	CFI_OUTOFSHIFT = 2,
	CFI_OVERLAPPED = 3,
	CFI_D_NOT_FINISHED = 4,
	CFI_NOPERMITS = 5,
	CFI_OVER_DUTYEND = 6,			//Einsatzzeit �berschritten
	CFI_STANDBY = 7,				//Mitarbeiter SFCA = SB
	CFI_ECFT_WB = 8,
	CFI_NOQUALIF = 9
};

enum{CFI_NOTACCEPTED, CFI_ACCEPTED};

struct CONFLICTENTRY
{
	int  erroNo;
	long FlightUrno;
	long Fkey;	
	long GhdKey;
	long GhdUrno;
	long DsrUrno;
	CTime Time;
	int  Type;
	char Esnm[10]; 
	char Sfca[10];
	char text[1024];
	int  Status; //
	CONFLICTENTRY(void)
	{	memset(this, '\0', sizeof(*this));
		Time = -1; Type = CFI_NOCONFLICT;
		Status = CFI_NOTACCEPTED;
	}
};


class ConflictCheck//: public CCSObject
{
public:
	ConflictCheck();
	~ConflictCheck();
	void AddConflict(long ilErrorNo, long FlightUrno, long lpFkey, long GhdUrno, long DsrUrno, CString olText);
	void CheckAllConflicts();
	bool AddInternal(CONFLICTENTRY *prpConflict);
	void QuickCheckAllConflicts();
//	void DeleteConflict(GHDDATA *prpGhd, DSRDATA *prpDsr, int ipCfiType);
	void DeleteConflict(long lpFlightUrno, int ipCfiType);
//	void AcceptConflict(GHDDATA *prpGhd, DSRDATA *prpDsr, int ipCfiType);
	bool CheckArray(void);
	int GetDataSize()
	{
		return omData.GetSize();
	}
	void DeleteAllData()
	{
		omPtrMap.RemoveAll();
		omData.DeleteAll();
	}
	void DeleteDataAt(int i)
	{
		if(i<omData.GetSize())
		{
			omPtrMap.RemoveKey(&omData[i]);
			omData.DeleteAt(i);
		}
	}
	CONFLICTENTRY *GetDataAt(int i);
	
private:
	CCSPtrArray<CONFLICTENTRY> omData;
	CMapPtrToPtr omPtrMap;
};

extern ConflictCheck ogConflictData;

#endif //__CONFLICT_CHECK__