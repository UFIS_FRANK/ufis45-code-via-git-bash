#if !defined(AFX_SIMULATIONPAGE_H__DC322379_8D89_11D6_820A_00010215BFDE__INCLUDED_)
#define AFX_SIMULATIONPAGE_H__DC322379_8D89_11D6_820A_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SimulationPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SimulationPage dialog

class SimulationPage : public CPropertyPage
{
	DECLARE_DYNCREATE(SimulationPage)

// Construction
public:
	SimulationPage();
	~SimulationPage();
	int CurrentType();

	void SetCaption(const char *pcpCaption);

// Dialog Data
	//{{AFX_DATA(SimulationPage)
	enum { IDD = IDD_SIMULATION_PAGE };
	int		m_SimulationType;
	int		m_SimulationDisplay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(SimulationPage)
	public:
	virtual void OnOK();
	virtual BOOL OnApply();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(SimulationPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioCut();
	afx_msg void OnRadioOffset();
	afx_msg void OnRadioSmooth();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	char pcmCaption[256];
	bool bmSingleFunction;	

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMULATIONPAGE_H__DC322379_8D89_11D6_820A_00010215BFDE__INCLUDED_)
