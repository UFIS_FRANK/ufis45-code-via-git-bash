#ifndef _CedaUbuData_H_
#define _CedaUbuData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define UBU_SEND_DDX	(true)
#define UBU_NO_SEND_DDX	(false)

// Felddimensionen
#define UBU_DPT1_LEN	(8)
#define UBU_STAR_LEN	(1)
#define UBU_HOPO_LEN	(3)

// Struktur eines UBU-Datensatzes
struct UBUDATA {
	long			Ubud;					//Urno BUDTAB
	char			Dpt1[UBU_DPT1_LEN+2];	//Organisationseinheit
	char			Star[UBU_STAR_LEN+2];	//Mit untergeordneten Einheiten
	char			Hopo[UBU_HOPO_LEN+2];	//Hopo
	long			Urno;					//Urno

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	UBUDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaUbuData class
void ProcessUbuCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaUbuData: kapselt den Zugriff auf die Tabelle UBU (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaUbuData: public CCSCedaData
{
// Funktionen
public:
	bool ReadUbuData();
    // Konstruktor/Destruktor
	CedaUbuData(CString opTableName = "UBU", CString opExtName = "TAB");
	~CedaUbuData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<UBUDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_UBU_CHANGE,BC_UBU_DELETE und BC_UBU_NEW
	void ProcessUbuBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadUbuByUrno(long lpUrno, UBUDATA *prpUbu);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(UBUDATA *prpUbu, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(UBUDATA *prpUbu, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(UBUDATA *prpUbu, bool bpSave = true);

	// Datens�tze suchen
	// UBU nach Urno suchen
	UBUDATA* GetUbuByUrno(long lpUrno);

	// kopiert die Feldwerte eines UBU-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyUbu(UBUDATA* popUbuDataSource,UBUDATA* popUbuDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(UBUDATA *prpUbu);
	// einen Broadcast UBU_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(UBUDATA *prpUbu, bool bpSendDdx);
	// einen Broadcast UBU_CHANGE versenden
	bool UpdateInternal(UBUDATA *prpUbu, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(UBUDATA *prpUbu, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten UBUs
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaUbuData_H_
