// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>

#include <CedaCfgData.h>
#include <CedaBudData.h>
#include <CedaUbuData.h>
#include <CedaOrgData.h>
#include <CedaHdcData.h>
#include <CedaUhdData.h>
#include <CedaScoData.h>
#include <CedaSorData.h>
#include <CedaStfData.h>
#include <CedaCotData.h>
#include <CedaMutData.h>
#include <CedaUmuData.h>
#include <CedaForData.h>

#include <CCSClientWnd.h>
#include <PrivList.h>

#include <CCSBasic.h>
#include <CedaBasicData.h>


/////////////////////////////////////////////////////////////////////////////
// Global Variable Section
const char *pcgAppName  = "Budget"; // Name of *.exe file of this
CString      ogAppName	= "Budget"; // Name of *.exe file of this
const char *pcgAppl     = "BUDGET"; // f�r die 8 Stelligen APPL und APPN Felder in der DB
CString      ogAppl 	= "BUDGET"; // f�r die 8 Stelligen APPL und APPN Felder in der DB

char pcgUser[33]		= "XXX";	// 
char pcgPasswd[33];
char pcgHome[4]			= "XXX";
char pcgTableExt[10]	= "XXX";
char pcgHelpPath[1024]	= "C:\\UFIS\\SYSTEM";
char pcgHome4[5]		= "EDDV";
CString ogCustomer;
bool bpUseGhdBc;
CTime ogLoginTime		= -1;

CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CBasicData      ogBasicData;
CCSBasic		ogCCSBasic;

CedaBasicData	ogBCD("", &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

CedaCfgData	ogCfgData;
CedaBudData ogBudData;
CedaUbuData ogUbuData;
CedaOrgData ogOrgData;
CedaHdcData ogHdcData;
CedaUhdData ogUhdData;
CedaScoData ogScoData;
CedaSorData ogSorData;
CedaStfData ogStfData;
CedaCotData ogCotData;
CedaMutData ogMutData;
CedaUmuData ogUmuData;
CedaForData ogForData;

bool bgIsModal = false;

PrivList ogPrivList;

ofstream of_catch;

int igDaysToRead;

bool bgOnline = true;

CInitialLoadDlg *pogInitialLoad;

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogSmallFonts_Regular_8;
CFont ogSmallFonts_Bold_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_9;
CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;
CFont ogScalingFonts[30];

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;

CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

char cgYes = '+';
char cgNo = ' ';

COLORREF CCSClientWnd::lmBkColor = lgBkColor;
/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	ogColors[0] = BLACK;
	ogColors[1] = WHITE;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = GREEN;
	ogColors[20] = GREEN;
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    BOOL blRet;
	blRet = dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
	blRet = ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
	blRet = ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);
//    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, /*"Small Fonts"*/"MS LineDraw");
    blRet = ogSmallFonts_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Bold_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
	blRet = ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Regular_9.CreateFontIndirect(&logFont);

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		blRet = ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		blRet = ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    blRet = ogMSSansSerif_Regular_8.CreateFontIndirect(&logFont);

//Times	

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_30.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_16.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_12.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_9.CreateFontIndirect(&logFont);

    dc.DeleteDC();
}

