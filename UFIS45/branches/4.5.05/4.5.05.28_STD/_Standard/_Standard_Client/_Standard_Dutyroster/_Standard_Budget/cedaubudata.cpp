// CedaUbuData.cpp - Klasse f�r die Handhabung von Bud-Daten (Budgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaUbuData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessUbuCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_Ubu_CHANGE, BC_Ubu_NEW und BC_Ubu_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaUbuData::ProcessUbuBc() der entsprechenden 
//	Instanz von CedaUbuData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessUbuCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaUbuData *polUbuData = (CedaUbuData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polUbuData->ProcessUbuBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaUbuData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaUbuData::CedaUbuData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Ubu-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(UBUDATA, UbuDataRecInfo)
		FIELD_LONG   	(Ubud,"UBUD")	// Urno BUDTAB
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")	// Organisationseinheit
		FIELD_CHAR_TRIM	(Star,"STAR")	// Mit untergeordneten Einheiten?
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_LONG   	(Urno,"URNO")	// Urno
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(UbuDataRecInfo)/sizeof(UbuDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&UbuDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Bud setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"UBUD,DPT1,STAR,HOPO,URNO");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Buds initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaUbuData::~CedaUbuData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaUbuData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaUbuData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Bud-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_UBU_CHANGE,CString("CedaUbuData"), CString("BC_UBU_CHANGE"), ProcessUbuCf);
	ogDdx.Register((void *)this,BC_UBU_NEW,CString("CedaUbuData"), CString("BC_UBU_NEW"), ProcessUbuCf);
	ogDdx.Register((void *)this,BC_UBU_DELETE,CString("CedaUbuData"), CString("BC_UBU_DELETE"), ProcessUbuCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaUbuData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadBudByUrno: liest den Bud-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaUbuData::ReadUbuByUrno(long lpUrno, UBUDATA *prpUbu)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	UBUDATA *prlUbu = new UBUDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpUbu) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlUbu;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaUbuData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UBUDATA *prlUbu = new UBUDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUbu);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlUbu->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlUbu, UBU_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlUbu;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlUbu;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Ubu: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpUbu> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUbuData::Insert(UBUDATA *prpUbu, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpUbu->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpUbu) == false) return false;
	// Broadcast UBU_NEW abschicken
	InsertInternal(prpUbu,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUbuData::InsertInternal(UBUDATA *prpUbu, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpUbu);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpUbu->Urno,prpUbu);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,UBU_NEW,(void *)prpUbu);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpUbu->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUbuData::Update(UBUDATA *prpUbu,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetUbuByUrno(prpUbu->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpUbu->IsChanged == DATA_UNCHANGED)
		{
			prpUbu->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpUbu) == false) return false; 
		// Broadcast UBU_CHANGE versenden
		UpdateInternal(prpUbu);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUbuData::UpdateInternal(UBUDATA *prpUbu, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Bud in lokaler Datenhaltung suchen
	UBUDATA *prlUbu = GetBudByKey(prpUbu->Sday,prpUbu->Stfu,prpUbu->Budn,prpUbu->Rosl);
	// Bud gefunden?
	if (prlUbu != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlUbu = *prpUbu;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",Ubu_CHANGE);
		ogDdx.DataChanged((void *)this,UBU_CHANGE,(void *)prpUbu);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaUbuData::DeleteInternal(UBUDATA *prpUbu, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,UBU_DELETE,(void *)prpUbu);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpUbu->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpUbu->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpUbu> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUbuData::Delete(UBUDATA *prpUbu, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpUbu->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpUbu)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpUbu,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessUbuBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaUbuData::ProcessUbuBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaUbuData::ProcessUbuBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	UBUDATA *prlUbu = NULL;

	switch(ipDDXType)
	{
	case BC_UBU_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlUbu = new UBUDATA;
			GetRecordFromItemList(prlUbu,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlUbu, UBU_SEND_DDX);
		}
		break;
	case BC_UBU_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUbu = GetUbuByUrno(llUrno);
			if(prlUbu != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlUbu,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlUbu);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_UBU_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUbu = GetUbuByUrno(llUrno);
			if (prlUbu != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlUbu);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaUbuData::ProcessUbuBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetUbuByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

UBUDATA *CedaUbuData::GetUbuByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	UBUDATA *prpUbu;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpUbu) == TRUE)
	{
		return prpUbu;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpUbu>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaUbuData::Save(UBUDATA *prpUbu)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpUbu->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpUbu->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpUbu);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpUbu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUbu->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpUbu);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpUbu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUbu->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUbuData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUbuData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyBudValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaUbuData::CopyUbu(UBUDATA* popUbuDataSource,UBUDATA* popUbuDatTarget)
{
	// Daten kopieren
	/*strcpy(popUbuDataTarget->Rema,popUbuDataSource->Rema);
	strcpy(popUbuDataTarget->Sdac,popUbuDataSource->Sdac);
	popUbuDataTarget->Abfr = popUbuDataSource->Abfr;
	popUbuDataTarget->Abto = popUbuDataSource->Abto;*/
}

bool CedaUbuData::ReadUbuData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UBUDATA *prlUbu = new UBUDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUbu);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlUbu, UBU_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlUbu;
		}
	} while (blMoreRecords);
	
	
	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Ubu: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}
