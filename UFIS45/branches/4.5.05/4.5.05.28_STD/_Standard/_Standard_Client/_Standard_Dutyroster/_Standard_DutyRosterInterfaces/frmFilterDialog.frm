VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Begin VB.Form frmFilterDialog 
   Caption         =   "RMS Staff Organization Units & Functions"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12405
   Icon            =   "frmFilterDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6450
   ScaleWidth      =   12405
   Begin VB.Frame fraStaffPanel 
      Caption         =   "Future Use"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3675
      Index           =   0
      Left            =   12480
      TabIndex        =   44
      Top             =   450
      Width           =   1245
   End
   Begin VB.Frame fraTopPanel 
      Caption         =   "Future Use"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   2
      Left            =   7260
      TabIndex        =   43
      Top             =   450
      Width           =   5085
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   345
      Left            =   10740
      TabIndex        =   42
      Top             =   0
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.Frame fraFilterPanel 
      Caption         =   "Personal Functions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Index           =   1
      Left            =   6270
      TabIndex        =   31
      Top             =   1500
      Width           =   6075
      Begin VB.CheckBox chkRefresh 
         Caption         =   "Refresh"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   270
         Width           =   1065
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Filter"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   "Activates/De-Activates the filter "
         Top             =   270
         Width           =   1065
      End
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2580
         TabIndex        =   38
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.PictureBox ButtonPanel 
         AutoRedraw      =   -1  'True
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Index           =   1
         Left            =   90
         ScaleHeight     =   345
         ScaleWidth      =   5835
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   2145
         Width           =   5895
         Begin VB.CheckBox chkRightFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   3405
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   30
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.CheckBox chkLeftFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   180
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   30
            Visible         =   0   'False
            Width           =   1065
         End
      End
      Begin VB.PictureBox GridPanel 
         Height          =   1425
         Index           =   1
         Left            =   90
         ScaleHeight     =   1365
         ScaleWidth      =   5835
         TabIndex        =   32
         Top             =   660
         Width           =   5895
         Begin TABLib.TAB tabLeftFilter 
            Height          =   915
            Index           =   1
            Left            =   0
            TabIndex        =   33
            Top             =   0
            Width           =   4725
            _Version        =   65536
            _ExtentX        =   8334
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB tabRightFilter 
            Height          =   915
            Index           =   1
            Left            =   4755
            TabIndex        =   34
            Top             =   0
            Width           =   1080
            _Version        =   65536
            _ExtentX        =   1905
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame fraTopPanel 
      Caption         =   "Future Use"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   1
      Left            =   3660
      TabIndex        =   27
      Top             =   450
      Width           =   3465
   End
   Begin VB.Frame fraTopPanel 
      Caption         =   "Future Use"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   0
      Left            =   60
      TabIndex        =   20
      Top             =   450
      Width           =   3465
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   2070
         MaxLength       =   4
         TabIndex        =   26
         Top             =   270
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2640
         MaxLength       =   2
         TabIndex        =   25
         Top             =   270
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   3000
         MaxLength       =   2
         TabIndex        =   24
         Top             =   270
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   3000
         MaxLength       =   2
         TabIndex        =   23
         Top             =   600
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2640
         MaxLength       =   2
         TabIndex        =   22
         Top             =   600
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2070
         MaxLength       =   4
         TabIndex        =   21
         Top             =   600
         Visible         =   0   'False
         Width           =   555
      End
   End
   Begin VB.Frame fraFilterPanel 
      Caption         =   "Organization Units (Departments)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Index           =   0
      Left            =   60
      TabIndex        =   14
      Top             =   1500
      Width           =   6075
      Begin VB.CheckBox chkRefresh 
         Caption         =   "Refresh"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   270
         Width           =   1065
      End
      Begin VB.PictureBox GridPanel 
         Height          =   1425
         Index           =   0
         Left            =   90
         ScaleHeight     =   1365
         ScaleWidth      =   5835
         TabIndex        =   28
         Top             =   660
         Width           =   5895
         Begin TABLib.TAB tabLeftFilter 
            Height          =   915
            Index           =   0
            Left            =   0
            TabIndex        =   29
            Top             =   0
            Width           =   4725
            _Version        =   65536
            _ExtentX        =   8334
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB tabRightFilter 
            Height          =   915
            Index           =   0
            Left            =   4755
            TabIndex        =   30
            Top             =   0
            Width           =   1080
            _Version        =   65536
            _ExtentX        =   1905
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
      End
      Begin VB.PictureBox ButtonPanel 
         AutoRedraw      =   -1  'True
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Index           =   0
         Left            =   90
         ScaleHeight     =   345
         ScaleWidth      =   5835
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   2145
         Width           =   5895
         Begin VB.CheckBox chkLeftFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   180
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   30
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.CheckBox chkRightFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   3405
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   30
            Visible         =   0   'False
            Width           =   1065
         End
      End
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2550
         TabIndex        =   16
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Filter"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Activates/De-Activates the filter "
         Top             =   270
         Width           =   1065
      End
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Close"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   0
      Left            =   2220
      Style           =   1  'Graphical
      TabIndex        =   13
      Tag             =   "CLOSE"
      Top             =   60
      Width           =   1065
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Load"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   12
      Tag             =   "AODB_FILTER"
      Top             =   60
      Width           =   1065
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Adjust"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   2
      Left            =   1140
      Style           =   1  'Graphical
      TabIndex        =   11
      Tag             =   "AODBREFRESH"
      Top             =   60
      Width           =   1065
   End
   Begin VB.Frame fraFredButtonPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7050
      TabIndex        =   1
      Top             =   30
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   6150
      Width           =   12405
      _ExtentX        =   21881
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   21343
            MinWidth        =   1764
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFilterDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim InternalResize As Boolean
Dim LayoutChanged As Boolean
Dim MyGlobalResult As String
Dim MyFilterIniSection As String
Dim MyCurParent As Form

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        chkAppl(Index).Refresh
        Select Case chkAppl(Index).Tag
            Case "CLOSE"
            Case "AODB_FILTER"
            Case "AODBREFRESH"
            Case Else
                chkAppl(Index).Value = 0
        End Select
    Else
        chkAppl(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub HandleClose(CurButton As CheckBox)
    Me.Hide
    CurButton.Value = 0
End Sub

Private Sub chkRefresh_Click(Index As Integer)
    If chkRefresh(Index).Value = 1 Then
        chkRefresh(Index).BackColor = LightGreen
        chkRefresh(Index).Refresh
        Select Case Index
            Case 0
                LoadDepartments 0, tabLeftFilter(0), ""
            Case 1
                LoadFunctions 1, tabLeftFilter(1), ""
            Case Else
        End Select
        chkRefresh(Index).Value = 0
    Else
        chkRefresh(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub Command1_Click()
    If CedaIsConnected Then
        tabLeftFilter(0).WriteToFile "c:\tmp\RmsImpToolFilter0.txt", False
        tabLeftFilter(1).WriteToFile "c:\tmp\RmsImpToolFilter1.txt", False
    ElseIf UfisServer.HostName = "LOCAL" Then
    End If
End Sub

Private Sub Form_Activate()
    Static FilterIsLoaded As Boolean
    If Not FilterIsLoaded Then
        Me.Refresh
        chkRefresh(0).Value = 1
        chkRefresh(1).Value = 1
        FilterIsLoaded = True
    End If
End Sub

Private Sub Form_Load()
    Dim myTop As Long
    Dim myLeft As Long
    InitMyLayout
    StatusBar.ZOrder
    myTop = ((frmMainDialog.height - Me.height) / 2) + frmMainDialog.Top
    myLeft = ((frmMainDialog.Width - Me.Width) / 2) + frmMainDialog.Left
    Me.Top = myTop
    Me.Left = myLeft
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Tag = ""
        Me.Hide
        Cancel = True
    End If
End Sub
'
Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    Dim NewSize As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MaxLin As Long
    If Not InternalResize Then
        NewHeight = Me.ScaleHeight - StatusBar.height - 30
        NewSize = NewHeight - fraStaffPanel(0).Top
        If NewSize >= fraTopPanel(0).height Then
            fraStaffPanel(0).height = NewSize
        End If
        NewSize = Me.ScaleWidth - fraStaffPanel(0).Left - 60
        If NewSize >= 1200 Then
            fraStaffPanel(0).Width = NewSize
            fraStaffPanel(0).Visible = True
        Else
            fraStaffPanel(0).Visible = False
        End If
        NewHeight = NewHeight - fraFilterPanel(0).Top
        If NewHeight > 2100 Then
            fraFilterPanel(0).height = NewHeight
            fraFilterPanel(1).height = NewHeight
            NewTop = NewHeight - ButtonPanel(0).height - 105
            ButtonPanel(0).Top = NewTop
            ButtonPanel(1).Top = NewTop
            NewHeight = NewTop - GridPanel(0).Top - 60
            GridPanel(0).height = NewHeight
            GridPanel(1).height = NewHeight
            NewHeight = NewHeight - 60
            tabLeftFilter(0).height = NewHeight
            tabRightFilter(0).height = NewHeight
            tabLeftFilter(1).height = NewHeight
            tabRightFilter(1).height = NewHeight
        End If
    End If
End Sub

Private Sub InitMyLayout()
    Dim tmpGridHeader As String
    Dim tmpGridLenList As String
    Dim tmpFontSize As Integer
    Dim i As Integer
    
    tmpGridHeader = ","
    tmpGridLenList = "10,10"
    tmpFontSize = 16
    For i = 0 To tabLeftFilter.UBound
        tabLeftFilter(i).ResetContent
        tabLeftFilter(i).HeaderString = tmpGridHeader
        tabLeftFilter(i).HeaderLengthString = tmpGridLenList
        tabLeftFilter(i).SetColumnBoolProperty 0, "Y", "N"
        tabLeftFilter(i).FontName = "Arial"
        tabLeftFilter(i).HeaderFontSize = tmpFontSize
        tabLeftFilter(i).FontSize = tmpFontSize
        tabLeftFilter(i).LineHeight = tmpFontSize + 1
        tabLeftFilter(i).LeftTextOffset = 0
        tabLeftFilter(i).SetTabFontBold True
        tabLeftFilter(i).GridLineColor = DarkGray
        tabLeftFilter(i).CreateCellObj "Marker", vbBlue, vbWhite, tmpFontSize, False, False, True, 0, "Arial"
        tabLeftFilter(i).LifeStyle = True
        'tabLeftFilter(i).AutoSizeByHeader = True
        tabLeftFilter(i).AutoSizeColumns
    
        tabRightFilter(i).ResetContent
        tabRightFilter(i).HeaderString = "Filter"
        tabRightFilter(i).HeaderLengthString = "10"
        tabRightFilter(i).FontName = "Arial"
        tabRightFilter(i).HeaderFontSize = tmpFontSize
        tabRightFilter(i).FontSize = tmpFontSize
        tabRightFilter(i).LineHeight = tmpFontSize + 1
        tabRightFilter(i).LeftTextOffset = 0
        tabRightFilter(i).SetTabFontBold True
        tabRightFilter(i).GridLineColor = DarkGray
        tabRightFilter(i).LifeStyle = True
        tabRightFilter(i).AutoSizeByHeader = True
        tabRightFilter(i).AutoSizeColumns
    
    Next
    
    tabLeftFilter(0).SetColumnBoolProperty 1, "Y", "N"
    
End Sub

Private Sub tabLeftFilter_BoolPropertyChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    Dim tmpBool As String
    Dim tmpCode As String
    If LineNo >= 0 Then
        Select Case ColNo
            Case 0
                ToggleFilterSelection Index, LineNo, NewValue
            Case 1
                tmpBool = tabLeftFilter(Index).GetFieldValue(LineNo, "BOOL")
                tmpCode = tabLeftFilter(Index).GetFieldValue(LineNo, "CODE")
                If tmpBool = "Y" Then AutoSelectFunctions tmpCode, NewValue
            Case Else
        End Select
    End If
End Sub

Private Sub tabLeftFilter_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim CurLine As Long
    Dim tmpBool As String
    If LineNo >= 0 Then
        tmpBool = tabLeftFilter(Index).GetFieldValue(LineNo, "BOOL")
        If tmpBool = "N" Then
            tabLeftFilter(Index).SetFieldValues LineNo, "BOOL", "Y"
            ToggleFilterSelection Index, LineNo, True
        Else
            tabLeftFilter(Index).SetFieldValues LineNo, "BOOL", "N"
            ToggleFilterSelection Index, LineNo, False
        End If
    ElseIf LineNo = -1 Then
        Screen.MousePointer = 11
        CurLine = tabLeftFilter(Index).GetCurrentSelected
        If tabLeftFilter(Index).CurrentSortColumn = ColNo Then
            If tabLeftFilter(Index).SortOrderASC Then
                tabLeftFilter(Index).Sort CStr(ColNo), False, True
            Else
                tabLeftFilter(Index).Sort CStr(ColNo), True, True
            End If
        Else
            tabLeftFilter(Index).Sort CStr(ColNo), True, True
        End If
        tabLeftFilter(Index).AutoSizeByHeader = True
        tabLeftFilter(Index).AutoSizeColumns
        tabLeftFilter(Index).SetCurrentSelection CurLine
        Screen.MousePointer = 0
    Else
    End If
End Sub

Private Sub tabRightFilter_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim SelLine As Long
    Dim SelCol As Long
    Dim tmpCode As String
    If (Selected = True) And (LineNo >= 0) Then
        tmpCode = tabRightFilter(Index).GetColumnValue(LineNo, 0)
        Select Case Index
            Case 0
                SelCol = 2
            Case 1
                SelCol = 2
            Case Else
                SelCol = -1
        End Select
        If SelCol >= 0 Then
            SelLine = GetCodeLine(Index, SelCol, tmpCode)
            If SelLine >= 0 Then
                tabLeftFilter(Index).SetCurrentSelection SelLine
            End If
        End If
    End If
End Sub

Private Sub tabRightFilter_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim SelLine As Long
    Dim SelCol As Long
    Dim tmpCode As String
    If LineNo >= 0 Then
        tmpCode = tabRightFilter(Index).GetColumnValue(LineNo, 0)
        Select Case Index
            Case 0
                SelCol = 2
            Case 1
                SelCol = 2
            Case Else
                SelCol = -1
        End Select
        If SelCol >= 0 Then
            SelLine = GetCodeLine(Index, SelCol, tmpCode)
            If SelLine >= 0 Then
                tabLeftFilter(Index).SetFieldValues SelLine, "BOOL", "N"
                ToggleFilterSelection Index, SelLine, False
            End If
        End If
    End If
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
'        LineNo = LeftFilterTab(Index).GetCurrentSelected
'        If LineNo >= 0 Then
'            HitText = LeftFilterTab(Index).GetColumnValue(LineNo, 0)
'            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
'                LeftFilterTab_SendLButtonDblClick Index, LineNo, 0
'            End If
'        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtVpfr_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpfr(0).Tag = tmpCedaDate
        For i = 0 To 2
            txtVpfr(i).BackColor = vbYellow
            txtVpfr(i).ForeColor = vbBlack
        Next
        'AdjustFilterButtons True
    Else
        txtVpfr(0).Tag = ""
        txtVpfr(Index).BackColor = vbRed
        txtVpfr(Index).ForeColor = vbWhite
        'AdjustFilterButtons False
    End If
End Sub
Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpto(0).Tag = tmpCedaDate
        For i = 0 To 2
            txtVpto(i).BackColor = vbYellow
            txtVpto(i).ForeColor = vbBlack
        Next
        'AdjustFilterButtons True
    Else
        txtVpto(0).Tag = ""
        txtVpto(Index).BackColor = vbRed
        txtVpto(Index).ForeColor = vbWhite
        'AdjustFilterButtons False
    End If
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Public Sub LoadDepartments(Index As Integer, ORGTAB As TABLib.TAB, UseSelKey As String)
    Dim CdrAnsw As Boolean
    Dim tmpGridFields As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpKeys As String
    Dim tmpUstf As String
    Dim tmpSday As String
    Dim tmpFcol As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    Screen.MousePointer = 11
    
    tmpGridFields = "BOOL,FULL,CODE,DPTN,REMA,DPT2,URNO,' ',' ',' '"
    tmpHeader = "S,A,Code,Departments,Remark,DPT2,URNO,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10"
    
    tabRightFilter(Index).ResetContent
    tabRightFilter(Index).Refresh
    
    ORGTAB.ResetContent
    ORGTAB.ShowVertScroller False
    ORGTAB.ShowHorzScroller False
    ORGTAB.HeaderString = tmpHeader
    ORGTAB.HeaderLengthString = tmpLength
    ORGTAB.LogicalFieldList = tmpGridFields
    ORGTAB.AutoSizeByHeader = True
    ORGTAB.AutoSizeColumns
    ORGTAB.Refresh
    
    tmpCmd = "RTA"
    tmpTable = "ORGTAB"
    tmpFields = "'N','N',DPT1,DPTN,REMA,DPT2,URNO,' ',' ',' '"

    ORGTAB.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    ORGTAB.CedaHopo = UfisServer.HOPO
    ORGTAB.CedaIdentifier = ""
    ORGTAB.CedaPort = "3357"
    ORGTAB.CedaReceiveTimeout = "250"
    ORGTAB.CedaRecordSeparator = vbLf
    ORGTAB.CedaSendTimeout = "250"
    ORGTAB.CedaServerName = UfisServer.HostName
    ORGTAB.CedaTabext = UfisServer.TblExt
    ORGTAB.CedaUser = UfisServer.ModName
    ORGTAB.CedaWorkstation = UfisServer.GetMyWorkStationName

    If CedaIsConnected Then
        CdrAnsw = ORGTAB.CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSelKey)
    ElseIf UfisServer.HostName = "LOCAL" Then
        ORGTAB.ReadFromFile "c:\tmp\RmsImpToolFilter0.txt"
    End If
    
    MaxLine = ORGTAB.GetLineCount - 1
    For CurLine = 0 To MaxLine
        'tmpUstf = Trim(ORGTAB.GetFieldValue(CurLine, "USTF"))
        'tmpSday = Trim(ORGTAB.GetFieldValue(CurLine, "ACTI"))
        'tmpFcol = Trim(ORGTAB.GetFieldValue(CurLine, "FCOL"))
        'tmpUstf = Right("0000000000" & tmpUstf, 10)
        'tmpSday = Left(tmpSday, 8)
        'tmpKeys = tmpUstf & "-" & tmpSday & "-" & tmpFcol
        'ORGTAB.SetFieldValues CurLine, "KEYS", tmpKeys
    Next
    
    ORGTAB.ShowVertScroller True
    ORGTAB.ShowHorzScroller True
    
    ORGTAB.AutoSizeByHeader = True
    SetTabSortCols ORGTAB, "DPTN"
    'SetTabIndexes ORGTAB, "KEYS", "KEYS", True
    
    ORGTAB.AutoSizeColumns
    ORGTAB.Refresh
    
    tabRightFilter(Index).ShowVertScroller True
    tabRightFilter(Index).ShowHorzScroller True
    tabRightFilter(Index).Refresh
    
    Screen.MousePointer = 0
End Sub

Public Sub LoadFunctions(Index As Integer, PfcTab As TABLib.TAB, UseSelKey As String)
    Dim CdrAnsw As Boolean
    Dim tmpGridFields As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpKeys As String
    Dim tmpUstf As String
    Dim tmpSday As String
    Dim tmpFcol As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    Screen.MousePointer = 11
    
    tmpGridFields = "BOOL,DPTC,CODE,FCTN,REMA,URNO,' ',' ',' '"
    tmpHeader = "S,Dept,Code,Functions,Remark,URNO,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10"
    
    tabRightFilter(Index).ResetContent
    tabRightFilter(Index).Refresh
    
    PfcTab.ResetContent
    PfcTab.ShowVertScroller False
    PfcTab.ShowHorzScroller False
    PfcTab.HeaderString = tmpHeader
    PfcTab.HeaderLengthString = tmpLength
    PfcTab.LogicalFieldList = tmpGridFields
    PfcTab.AutoSizeByHeader = True
    PfcTab.AutoSizeColumns
    PfcTab.Refresh
    
    tmpCmd = "RTA"
    tmpTable = "PFCTAB"
    tmpFields = "'N',DPTC,FCTC,FCTN,REMA,URNO,' ',' ',' '"

    PfcTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    PfcTab.CedaHopo = UfisServer.HOPO
    PfcTab.CedaIdentifier = ""
    PfcTab.CedaPort = "3357"
    PfcTab.CedaReceiveTimeout = "250"
    PfcTab.CedaRecordSeparator = vbLf
    PfcTab.CedaSendTimeout = "250"
    PfcTab.CedaServerName = UfisServer.HostName
    PfcTab.CedaTabext = UfisServer.TblExt
    PfcTab.CedaUser = UfisServer.ModName
    PfcTab.CedaWorkstation = UfisServer.GetMyWorkStationName

    If CedaIsConnected Then
        CdrAnsw = PfcTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSelKey)
    ElseIf UfisServer.HostName = "LOCAL" Then
        PfcTab.ReadFromFile "c:\tmp\RmsImpToolFilter1.txt"
    End If
    
    MaxLine = PfcTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        'tmpUstf = Trim(PFCTAB.GetFieldValue(CurLine, "USTF"))
        'tmpSday = Trim(PFCTAB.GetFieldValue(CurLine, "ACTI"))
        'tmpFcol = Trim(PFCTAB.GetFieldValue(CurLine, "FCOL"))
        'tmpUstf = Right("0000000000" & tmpUstf, 10)
        'tmpSday = Left(tmpSday, 8)
        'tmpKeys = tmpUstf & "-" & tmpSday & "-" & tmpFcol
        'PFCTAB.SetFieldValues CurLine, "KEYS", tmpKeys
    Next
    
    PfcTab.ShowVertScroller True
    PfcTab.ShowHorzScroller True
    
    PfcTab.AutoSizeByHeader = True
    SetTabSortCols PfcTab, "CODE,DPTC"
    SetTabIndexes PfcTab, "DPTC", "DPTC", True
    
    PfcTab.AutoSizeColumns
    PfcTab.Refresh
    
    tabRightFilter(Index).ShowVertScroller True
    tabRightFilter(Index).ShowHorzScroller True
    tabRightFilter(Index).Refresh
    
    Screen.MousePointer = 0
End Sub

Private Sub ToggleFilterSelection(Index As Integer, LineNo As Long, SetBool As Boolean)
    Dim SelLine As Long
    Dim MaxSel As Long
    Dim tmpCode As String
    Dim tmpData As String
    tmpCode = tabLeftFilter(Index).GetFieldValues(LineNo, "CODE")
    If SetBool = True Then
        tmpData = tmpCode
        tabRightFilter(Index).InsertTextLine tmpData, False
        tabRightFilter(Index).Sort "0", True, True
    Else
        tmpData = tabRightFilter(Index).GetLinesByColumnValue(0, tmpCode, 0)
        If tmpData <> "" Then
            SelLine = Val(tmpData)
            If SelLine >= 0 Then
                tabRightFilter(Index).DeleteLine SelLine
            End If
        End If
    End If
    tabRightFilter(Index).AutoSizeColumns
    tabRightFilter(Index).Refresh
    tabLeftFilter(Index).Refresh
    Select Case Index
        Case 0
            SelLine = SelectFuncLookup(tabLeftFilter(1), "DPTC", tmpCode, SetBool, "DPTC", True)
            tmpData = tabLeftFilter(Index).GetFieldValues(LineNo, "FULL")
            If tmpData = "Y" Then AutoSelectFunctions tmpCode, SetBool
        Case 1
            MaxSel = tabRightFilter(1).GetLineCount
            If MaxSel > 0 Then
                frmMainDialog.picBall(12).Visible = True
            Else
                frmMainDialog.picBall(12).Visible = False
            End If
        Case Else
    End Select
End Sub

Private Sub AutoSelectFunctions(DeptCode As String, SetSelected As Boolean)
    Dim tmpLine As String
    Dim tmpFlag As String
    Dim tmpFunc As String
    Dim LineNo As Long
    Dim SelLine As Long
    Dim MaxSel As Long
    tabLeftFilter(1).SetInternalLineBuffer True
    tmpLine = tabLeftFilter(1).GetLinesByIndexValue("DPTC", DeptCode, 0)
    If Val(tmpLine) > 0 Then
        LineNo = tabLeftFilter(1).GetNextResultLine
        If LineNo >= 0 Then
            While LineNo >= 0
                tmpFlag = tabLeftFilter(1).GetFieldValues(LineNo, "BOOL")
                tmpFunc = tabLeftFilter(1).GetFieldValues(LineNo, "CODE")
                If SetSelected = True Then
                    If tmpFlag = "N" Then
                        tabLeftFilter(1).SetFieldValues LineNo, "BOOL", "Y"
                        tabRightFilter(1).InsertTextLine tmpFunc, False
                    End If
                Else
                    If tmpFlag = "Y" Then
                        tabLeftFilter(1).SetFieldValues LineNo, "BOOL", "N"
                        tmpLine = tabRightFilter(1).GetLinesByColumnValue(0, tmpFunc, 0)
                        If tmpLine <> "" Then
                            SelLine = Val(tmpLine)
                            If SelLine >= 0 Then
                                tabRightFilter(1).DeleteLine SelLine
                            End If
                        End If
                    End If
                End If
                LineNo = tabLeftFilter(1).GetNextResultLine
            Wend
        End If
    End If
    tabLeftFilter(1).SetInternalLineBuffer False
    tabLeftFilter(1).Refresh
    tabRightFilter(1).Sort "0", True, True
    tabRightFilter(1).AutoSizeColumns
    tabRightFilter(1).Refresh
    
    MaxSel = tabRightFilter(1).GetLineCount
    If MaxSel > 0 Then
        frmMainDialog.picBall(12).Visible = True
    Else
        frmMainDialog.picBall(12).Visible = False
    End If
End Sub

Private Function GetCodeLine(Index As Integer, ColNo As Long, SelCode As String) As Long
    Dim SelLine As Long
    Dim tmpData As String
    SelLine = -1
    tmpData = tabLeftFilter(Index).GetLinesByColumnValue(ColNo, SelCode, 0)
    If tmpData <> "" Then SelLine = Val(tmpData)
    GetCodeLine = SelLine
End Function

Private Function SelectFuncLookup(CurTab As TABLib.TAB, IndexName As String, LookValue As String, Selected As Boolean, MarkFields As String, ScrollTo As Boolean) As Long
    Dim tmpLine As String
    Dim tmpFieldList As String
    Dim LineNo As Long
    Dim FirstLine As Long
    Dim LineCount As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim CntColor(0 To 8) As Long
    Dim i As Integer
    MaxLine = CurTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurTab.ResetCellProperties CurLine
    Next
    FirstLine = -1
    If Selected Then
        LineCount = 0
        CurTab.SetInternalLineBuffer True
        tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
        If Val(tmpLine) > 0 Then
            tmpFieldList = MarkFields
            'If tmpFieldList = "" Then tmpFieldList = CurTab.myName
            LineNo = CurTab.GetNextResultLine
            FirstLine = LineNo
            If LineNo >= 0 Then
                If ScrollTo = True Then
                    CurTab.OnVScrollTo LineNo - 2
                    CurTab.SetCurrentSelection LineNo
                End If
                While LineNo >= 0
                    SetTabCellObjects CurTab, LineNo, "MARKER", "Marker", tmpFieldList, True
                    LineCount = LineCount + 1
                    LineNo = CurTab.GetNextResultLine
                Wend
            End If
        Else
            CurTab.SetCurrentSelection -1
            CurTab.OnVScrollTo 0
        End If
        CurTab.SetInternalLineBuffer False
    End If
    CurTab.Refresh
    SelectFuncLookup = FirstLine
End Function

