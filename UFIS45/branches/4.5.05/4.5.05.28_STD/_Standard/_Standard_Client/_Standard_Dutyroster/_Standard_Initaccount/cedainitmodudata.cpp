// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>
const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = GetInitModuTxt();

	//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}

	delete pclInitModuData;

	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = "InitializeAccount,InitModu,InitModu,Initialisieren (InitModu),B,-";
	olInitModuData += GetInitModuTxtG();
	return olInitModuData;
	
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olTmpStr="";
	for(int ilIndex =0;ilIndex<10;ilIndex++)
	{
		switch(ilIndex)
		{
		case 0:
			// nur zum Testen und f�r den 1. Eintrag
			olTmpStr += ",Test,TEST,Test,G,1";
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		default:
			break;
		}
	}

	return olTmpStr;
}
