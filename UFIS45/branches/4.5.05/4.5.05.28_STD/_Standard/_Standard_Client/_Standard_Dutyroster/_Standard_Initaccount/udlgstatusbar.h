#if !defined(AFX_UDLGSTATUSBAR_H_INCLUDED)
#define AFX_UDLGSTATUSBAR_H_INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDlgStatusBar.h : header file
//
#include <CCSPtrArray.h>

enum STYLES
{
	UDSB_SS_DEFAULT,
	UDSB_SS_SUNKEN,
	UDSB_SS_HIGH
};

struct ELEMENT
{
	UINT			iID;
	UINT			iWidth;
	CRect			oRect;

	CStatic			*poStatic;
	UINT			iStyl;
	CString			oText;

	CProgressCtrl	*poProgressCtrl;
	UINT			iProgressPercent;

	UINT			iBmNumber;
	CBitmap			*poBitmap1;
	UINT			iBitmapID1;
	CBitmap			*poBitmap2;
	UINT			iBitmapID2;
	CBitmap			*poBitmap3;
	UINT			iBitmapID3;
	ELEMENT(void)
	{
		iID					= 0;
		iStyl				= UDSB_SS_SUNKEN,
		poStatic			= NULL;
		poProgressCtrl		= NULL;
		poBitmap1			= NULL;
		poBitmap2			= NULL;
		poBitmap3			= NULL;
		iProgressPercent	= 0;
		iWidth				= 0;
		iBmNumber			= 1;
		iBitmapID1			= 0;
		iBitmapID2			= 0;
		iBitmapID3			= 0;
	}
};

/////////////////////////////////////////////////////////////////////////////
// UDlgStatusBar window

class UDlgStatusBar : public CWnd
{
public:
// Allgemein:
// Das Standartelement ist immer ein CStatic mit ID = 1. Es kann nicht gel�scht werden.
// Alle Elemente mit Width = 0 teilen sich gleichm��ig den zur verf�gung stehenden Platz.
// Elemente mit Width != 0 behalten immer ihre feste gr��e.
// Position 1 ist links.

	// Construction
	UDlgStatusBar(CWnd* pParent = NULL, UINT ipID = 0, UINT ipDevStyle = UDSB_SS_SUNKEN);
	
	// Erzeugt ein neues Text-Feld
	UINT SetStatic(CString opText = CString(""), UINT ipPos = 1, UINT ipWidth = 0, UINT ipStyle = UDSB_SS_DEFAULT);
	// �ndert den Text eines Text-Feldes
	void SetStaticText(UINT ipID, CString opText = CString(""));

	// Erzeugt ein neues Progress-Ctrl 
	UINT SetProgress(UINT ipPercent = 0, UINT ipPos = 1, UINT ipWidth = 0);
	// �ndert die Procentzahl eines Progress-Ctrl 
	void SetProgressPos(UINT ipID, UINT ipPercent = 0);

	// Erzeugt ein neues Bitmap 
	UINT SetBitmap(UINT ipResourceID , UINT ipPos = 1, UINT ipWidth = 0);
	// Erzeugt ein neues Mehrfach-Bitmap
	// es kann max. 3 Bitmaps enthalten, die einzeln angezeigt werden k�nnen
	UINT SetBitmaps(UINT ipResourceID1, UINT ipResourceID2, UINT ipResourceID3, UINT ipBmNumber = 1, UINT ipPos = 1, UINT ipWidth = 0);
	// �nder die Anzeige eines Mehrfach-Bitmaps
	void SetBitmapNumber(UINT ipID, UINT ipBmNumber = 1);

	// Enabled/Disabled ein Element (nur Text-Felder)
	void EnableElement(UINT ipID, BOOL bpEnable = TRUE);
	// L�scht die StatusBar
	bool DeleteStatusBar();
	// L�scht einzelne Elemente und gibt deren Platz f�r Andere Elemente frei
	void DeleteElement(UINT ipID);

	// Sizing mu� bei G��en�nderungen des ParentWnd aufgerufen werden
	void Sizing();

// NOT PUPLIC /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// Attributes
private:
	CWnd *pomParent;
	UINT imID;
	UINT imElementID;
	UINT imDevStyle;

	bool bmCreateCompatibleDC;
	CDC omDCComp;
	CBrush omBkgndBrush;
	CPen omPenS;
	CPen omPenH;

	CCSPtrArray<ELEMENT> omElements;
	CRect omStatusBarRect;
	CRect omOldStatusBarRect;
	CRect omParentWndRect;

	int imStatusBarHeight;
	int imParentWndWidth;

	COLORREF omBackgroundColor;
	COLORREF omDisabledTextColor;
	COLORREF omTextColor;
	COLORREF omHighlightColor;
	COLORREF omLightColor;
	COLORREF omShadowColor;
	COLORREF omDkShadowColor;
	COLORREF omSelectColor;

	CFont omMSSSR8;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UDlgStatusBar)
	//}}AFX_VIRTUAL

public:
	virtual ~UDlgStatusBar();

private:
	BOOL Create();
	void PaintBitmap(UINT ipElementNr);
	void MoveElements();
	void ResetContent();

	// Generated message map functions
protected:
	//{{AFX_MSG(UDlgStatusBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDLGSTATUSBAR_H_INCLUDED)
