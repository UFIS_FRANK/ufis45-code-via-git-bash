// UDlgStatusBar.cpp : implementation file
// by Alexander Reitz
// Ver 1.0 / 21.05.1999

#include <stdafx.h>
#include <UDlgStatusBar.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UDlgStatusBar
//---------------------------------------------------------------------------
UDlgStatusBar::UDlgStatusBar(CWnd* pParent /*= NULL*/, UINT ipID/*=0*/, UINT ipDevStyle /*= UDSB_SS_SUNKEN*/)
{
	pomParent = pParent;
	if(pomParent == NULL)
		pomParent = GetParent();
	imID = ipID;
	imElementID = 0;
	omStatusBarRect.SetRectEmpty();
	omOldStatusBarRect.SetRectEmpty();
	omParentWndRect.SetRectEmpty();

	imStatusBarHeight = 20;
	imParentWndWidth  = 0;

	if(ipDevStyle == UDSB_SS_SUNKEN || ipDevStyle == UDSB_SS_HIGH)
		imDevStyle = ipDevStyle;
	else
		imDevStyle = UDSB_SS_SUNKEN;
	
	omBackgroundColor	= GetSysColor(COLOR_3DFACE);
	omTextColor			= GetSysColor(COLOR_BTNTEXT);
	omHighlightColor	= GetSysColor(COLOR_3DHIGHLIGHT);
	omLightColor		= GetSysColor(COLOR_3DLIGHT);
	omDkShadowColor		= GetSysColor(COLOR_3DDKSHADOW);
	omShadowColor		= GetSysColor(COLOR_3DSHADOW);
	omSelectColor		= GetSysColor(COLOR_HIGHLIGHT);

	bmCreateCompatibleDC = true;

	Create();
}
//---------------------------------------------------------------------------
UDlgStatusBar::~UDlgStatusBar()
{
	ResetContent();
}
//---------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(UDlgStatusBar, CWnd)
	//{{AFX_MSG_MAP(UDlgStatusBar)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UDlgStatusBar message handlers
//---------------------------------------------------------------------------
BOOL UDlgStatusBar::Create()
{
	DWORD dwStyle = WS_CHILD | WS_VISIBLE;
	return CWnd::Create(NULL, NULL, dwStyle, omStatusBarRect, pomParent, imID, NULL);
}
//---------------------------------------------------------------------------
int UDlgStatusBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	pomParent->GetClientRect(&omParentWndRect);
	imParentWndWidth  = omParentWndRect.right - omParentWndRect.left;

	omStatusBarRect.left   = omParentWndRect.left;
	omStatusBarRect.top    = omParentWndRect.bottom - imStatusBarHeight;
	omStatusBarRect.right  = omParentWndRect.right;
	omStatusBarRect.bottom = omParentWndRect.bottom;
	MoveWindow(&omStatusBarRect);

	//Create Font
	CDC dc;
	BOOL blRet;
	blRet = dc.CreateCompatibleDC(NULL);
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfWeight = FW_NORMAL;
	logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(logFont.lfFaceName, "MS Sans Serif");
	blRet = omMSSSR8.CreateFontIndirect(&logFont);
    dc.DeleteDC();
	//End Create Font

	SetStatic(""); //Default Textfield ID 1

	return 0;
}
//---------------------------------------------------------------------------
void UDlgStatusBar::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if(bmCreateCompatibleDC)
	{
		bmCreateCompatibleDC = false;
		omDCComp.CreateCompatibleDC(&dc);

		omBkgndBrush.CreateSolidBrush(omBackgroundColor);
		omPenS.CreatePen(PS_SOLID, 1, omShadowColor);
		omPenH.CreatePen(PS_SOLID, 1, omHighlightColor);
	}

	CRect olRect = CRect(0, 0, omStatusBarRect.right, imStatusBarHeight);
	dc.FillRect(olRect,&omBkgndBrush);	

	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		CBitmap *polBitmap = NULL;
		if(omElements[i].poBitmap1 != NULL && omElements[i].iBmNumber == 1)
			polBitmap = omElements[i].poBitmap1;
		else if(omElements[i].poBitmap2 != NULL && omElements[i].iBmNumber == 2)
			polBitmap = omElements[i].poBitmap2;
		else if(omElements[i].poBitmap3 != NULL && omElements[i].iBmNumber == 3)
			polBitmap = omElements[i].poBitmap3;

		if(polBitmap != NULL)
		{
			omDCComp.SelectObject(polBitmap);

			BITMAP olBmInfo;
			polBitmap->GetObject(sizeof(olBmInfo), &olBmInfo);
			int ilTop = (((imStatusBarHeight+4) + olBmInfo.bmHeight)/2) - olBmInfo.bmHeight;
			if(ilTop > ((imStatusBarHeight+4)/2)) ilTop = (imStatusBarHeight+4)/2;
			if(ilTop < 4) ilTop = 4;
			int ilLeft = (((omElements[i].oRect.right-omElements[i].oRect.left)+ olBmInfo.bmWidth)/2) - olBmInfo.bmWidth + omElements[i].oRect.left;
			if(ilLeft > omElements[i].oRect.left + ((omElements[i].oRect.right-omElements[i].oRect.left)/2)) ilLeft = omElements[i].oRect.left + ((omElements[i].oRect.right-omElements[i].oRect.left)/2);
			if(ilLeft < omElements[i].oRect.left) ilLeft = omElements[i].oRect.left;
			dc.BitBlt(ilLeft, ilTop, olBmInfo.bmWidth, olBmInfo.bmHeight, &omDCComp, 0, 0, SRCCOPY);
		}
	}

	dc.SelectObject(&omPenS);
	dc.MoveTo(0, 0);
	dc.LineTo(omStatusBarRect.right, 0);

	dc.SelectObject(&omPenH);
	dc.MoveTo(0, 1);
	dc.LineTo(omStatusBarRect.right, 1);
}
//---------------------------------------------------------------------------
BOOL UDlgStatusBar::OnEraseBkgnd(CDC* pDC) 
{
	Sizing();
	return CWnd::OnEraseBkgnd(pDC);
}
//---------------------------------------------------------------------------
void UDlgStatusBar::Sizing() 
{
	pomParent->GetClientRect(&omParentWndRect);
	imParentWndWidth  = omParentWndRect.right - omParentWndRect.left;

	omStatusBarRect.left   = omParentWndRect.left;
	omStatusBarRect.top    = omParentWndRect.bottom - imStatusBarHeight;
	omStatusBarRect.right  = omParentWndRect.right;
	omStatusBarRect.bottom = omParentWndRect.bottom;

	if(omOldStatusBarRect != omStatusBarRect)
	{
		MoveWindow(&omStatusBarRect);
		omOldStatusBarRect = omStatusBarRect;
	}
	MoveElements();
}
//---------------------------------------------------------------------------
UINT UDlgStatusBar::SetStatic(CString opText /*= CString("")*/, UINT ipPos /*= 1*/, UINT ipWidth /*= 0*/, UINT ipStyle /*= UDSB_SS_DEFAULT*/)
{
	imElementID++;
	int ilPos = ipPos-1;
	if(ilPos<0) ilPos = 0;
	if(ilPos>omElements.GetSize()) ilPos = omElements.GetSize();

	if(ipStyle == UDSB_SS_DEFAULT || (ipStyle != UDSB_SS_SUNKEN && ipStyle != UDSB_SS_HIGH && ipStyle != UDSB_SS_DEFAULT))
		ipStyle = imDevStyle;

	ELEMENT *polSBElement = new ELEMENT;
	polSBElement->poStatic	= new CStatic;
	polSBElement->iID		= imElementID;
	polSBElement->iWidth	= ipWidth;
	polSBElement->iStyl		= ipStyle;
	polSBElement->oText.Format(" %s", opText);
	polSBElement->oRect		= CRect(0,4,0,imStatusBarHeight);
	omElements.InsertAt(ilPos, polSBElement);

	if(polSBElement->iStyl == UDSB_SS_SUNKEN)
		polSBElement->poStatic->Create(polSBElement->oText, WS_CHILD|WS_VISIBLE|SS_SUNKEN|SS_CENTERIMAGE, polSBElement->oRect, this, polSBElement->iID);
	else
		polSBElement->poStatic->Create(polSBElement->oText, WS_CHILD|WS_VISIBLE|SS_CENTERIMAGE, polSBElement->oRect, this, polSBElement->iID);

	polSBElement->poStatic->SetFont(&omMSSSR8);

	InvalidateRect(NULL);
	return imElementID;
}
//---------------------------------------------------------------------------
void UDlgStatusBar::SetStaticText(UINT ipID, CString opText /*= CString("")*/)
{
	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		if(omElements[i].iID == ipID)
		{
			if(omElements[i].poStatic != NULL)
			{
				omElements[i].oText.Format(" %s", opText);
				omElements[i].poStatic->SetWindowText(omElements[i].oText);
			}
		}
	}
}
//---------------------------------------------------------------------------
UINT UDlgStatusBar::SetBitmap(UINT ipResourceID , UINT ipPos /*= 1*/, UINT ipWidth /*= 0*/)
{
	imElementID++;
	int ilPos = ipPos-1;
	if(ilPos<0) ilPos = 0;
	if(ilPos>omElements.GetSize()) ilPos = omElements.GetSize();

	ELEMENT *polSBElement = new ELEMENT;
	polSBElement->poStatic		= new CStatic;
	polSBElement->iID			= imElementID;
	polSBElement->iWidth		= ipWidth;
	polSBElement->iBitmapID1	= ipResourceID;
	polSBElement->poBitmap1		= new CBitmap;
	polSBElement->oRect			= CRect(0,4,0,imStatusBarHeight);
	omElements.InsertAt(ilPos, polSBElement);

	polSBElement->poStatic->Create("", WS_CHILD, polSBElement->oRect, this, polSBElement->iID);
	polSBElement->poBitmap1->LoadBitmap(ipResourceID);

	InvalidateRect(NULL);
	return imElementID;
}
//---------------------------------------------------------------------------
UINT UDlgStatusBar::SetBitmaps(UINT ipResourceID1, UINT ipResourceID2, UINT ipResourceID3, UINT ipBmNumber /*= 1*/, UINT ipPos /*= 1*/, UINT ipWidth /*= 0*/)
{
	imElementID++;
	int ilPos = ipPos-1;
	if(ilPos<0) ilPos = 0;
	if(ilPos>omElements.GetSize()) ilPos = omElements.GetSize();

	ELEMENT *polSBElement = new ELEMENT;
	polSBElement->poStatic		= new CStatic;
	polSBElement->iID			= imElementID;
	polSBElement->iWidth		= ipWidth;
	polSBElement->iBitmapID1	= ipResourceID1;
	polSBElement->iBitmapID2	= ipResourceID2;
	polSBElement->iBitmapID3	= ipResourceID3;
	polSBElement->iBmNumber		= ipBmNumber;
	polSBElement->oRect			= CRect(0,4,13,imStatusBarHeight);
	omElements.InsertAt(ilPos, polSBElement);

	polSBElement->poStatic->Create("", WS_CHILD, polSBElement->oRect, this, polSBElement->iID);

	if(ipResourceID1 != 0)
	{
		polSBElement->poBitmap1= new CBitmap;
		polSBElement->poBitmap1->LoadBitmap(ipResourceID1);
	}
	if(ipResourceID2 != 0)
	{
		polSBElement->poBitmap2= new CBitmap;
		polSBElement->poBitmap2->LoadBitmap(ipResourceID2);
	}
	if(ipResourceID3 != 0)
	{
		polSBElement->poBitmap3= new CBitmap;
		polSBElement->poBitmap3->LoadBitmap(ipResourceID3);
	}
	InvalidateRect(NULL);
	return imElementID;
}
//---------------------------------------------------------------------------
void UDlgStatusBar::SetBitmapNumber(UINT ipID, UINT ipBmNumber /*= 1*/)
{
	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		if(omElements[i].iID == ipID )
		{
			if((ipBmNumber==1)||(ipBmNumber==2 && omElements[i].poBitmap2 != NULL) || (ipBmNumber==3 && omElements[i].poBitmap3 != NULL))
			omElements[i].iBmNumber = ipBmNumber;
			PaintBitmap(i);
		}
	}
}
//---------------------------------------------------------------------------
void UDlgStatusBar::PaintBitmap(UINT ipElementNr) 
{
	int i = ipElementNr;
	if(i >= omElements.GetSize())
		return;
	
	CBitmap *polBitmap = NULL;
	if(omElements[i].poBitmap1 != NULL && omElements[i].iBmNumber == 1)
		polBitmap = omElements[i].poBitmap1;
	else if(omElements[i].poBitmap2 != NULL && omElements[i].iBmNumber == 2)
		polBitmap = omElements[i].poBitmap2;
	else if(omElements[i].poBitmap3 != NULL && omElements[i].iBmNumber == 3)
		polBitmap = omElements[i].poBitmap3;

	CRect olRect;
	olRect = omElements[i].oRect;

	if(polBitmap != NULL)
	{
		CDC *pdc = GetDC();
		pdc->FillRect(olRect, &omBkgndBrush);	
		omDCComp.SelectObject(polBitmap);

		BITMAP olBmInfo;
		polBitmap->GetObject(sizeof(olBmInfo), &olBmInfo);
		int ilTop = (((imStatusBarHeight+4) + olBmInfo.bmHeight)/2) - olBmInfo.bmHeight;
		if(ilTop > ((imStatusBarHeight+4)/2)) ilTop = (imStatusBarHeight+4)/2;
		if(ilTop < 4) ilTop = 4;
		int ilLeft = (((omElements[i].oRect.right-omElements[i].oRect.left)+ olBmInfo.bmWidth)/2) - olBmInfo.bmWidth + omElements[i].oRect.left;
		if(ilLeft > omElements[i].oRect.left + ((omElements[i].oRect.right-omElements[i].oRect.left)/2)) ilLeft = omElements[i].oRect.left + ((omElements[i].oRect.right-omElements[i].oRect.left)/2);
		if(ilLeft < omElements[i].oRect.left) ilLeft = omElements[i].oRect.left;
		pdc->BitBlt(ilLeft, ilTop, olBmInfo.bmWidth, olBmInfo.bmHeight, &omDCComp, 0, 0, SRCCOPY);
		ReleaseDC(pdc);
	}
}
//---------------------------------------------------------------------------
UINT UDlgStatusBar::SetProgress(UINT ipPercent /*= 0*/, UINT ipPos /*= 1*/, UINT ipWidth /*= 0*/)
{
	imElementID++;
	int ilPos = ipPos-1;
	if(ilPos<0) ilPos = 0;
	if(ilPos>omElements.GetSize()) ilPos = omElements.GetSize();

	ELEMENT *polSBElement = new ELEMENT;
	polSBElement->poProgressCtrl	= new CProgressCtrl;
	polSBElement->iID				= imElementID;
	polSBElement->iWidth			= ipWidth;
	polSBElement->iProgressPercent	= ipPercent;
	polSBElement->oRect				= CRect(0,4,0,imStatusBarHeight);
	omElements.InsertAt(ilPos, polSBElement);

	polSBElement->poProgressCtrl->Create(WS_CHILD|WS_VISIBLE|PBS_SMOOTH, polSBElement->oRect, this, polSBElement->iID);
	polSBElement->poProgressCtrl->SetPos(ipPercent);

	InvalidateRect(NULL);
	return imElementID;
}
//---------------------------------------------------------------------------
void UDlgStatusBar::SetProgressPos(UINT ipID, UINT ipPercent /*= 0*/)
{
	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		if(omElements[i].iID == ipID)
		{
			if(omElements[i].poProgressCtrl != NULL)
			{
				omElements[i].iProgressPercent = ipPercent;
				omElements[i].poProgressCtrl->SetPos(ipPercent);
			}
		}
	}
}
//---------------------------------------------------------------------------
void UDlgStatusBar::MoveElements()
{
	int ilElements = omElements.GetSize();
	int ilWidth = 2*(ilElements-1);
	int ilVarElements = 0;
	for(int i=0; i<ilElements; i++)
	{
		ilWidth += omElements[i].iWidth;
		if(omElements[i].iWidth==0)
			ilVarElements++;
	}
	int ilVarWidth  = (imParentWndWidth-ilWidth) / ilVarElements;
	int ilPos = 0;
	for(i=0; i<ilElements; i++)
	{
		CWnd *polWnd = NULL;
		if(omElements[i].poStatic != NULL)
			polWnd =  omElements[i].poStatic;
		if(omElements[i].poProgressCtrl != NULL)
			polWnd = omElements[i].poProgressCtrl;

		if(omElements[i].iWidth==0)
			ilWidth = ilVarWidth;
		else
			ilWidth = omElements[i].iWidth;
		
		CRect olRect;
		if(i == ilElements-1)
			olRect = CRect(ilPos, 4, omStatusBarRect.right, imStatusBarHeight);
		else
			olRect = CRect(ilPos, 4, ilPos+ilWidth, imStatusBarHeight);
		
		ilPos += ilWidth + 2;
		polWnd->MoveWindow(&olRect);
		omElements[i].oRect = olRect;
	}
	for(i=0; i<ilElements; i++)
	{
		if(omElements[i].poStatic != NULL)
		{
			omElements[i].poStatic->SetWindowText(omElements[i].oText);
			omElements[i].poStatic->InvalidateRect(NULL);
		}
		if(omElements[i].poProgressCtrl != NULL)
		{
			omElements[i].poProgressCtrl->SetPos(omElements[i].iProgressPercent);
			omElements[i].poProgressCtrl->InvalidateRect(NULL);
		}
	}
}
//---------------------------------------------------------------------------
void UDlgStatusBar::DeleteElement(UINT ipID)
{
	if(ipID != 1)
	{
		int ilElements = omElements.GetSize();
		for(int i=0; i<ilElements; i++)
		{
			if(omElements[i].iID == ipID)
			{
				if(omElements[i].poStatic != NULL)
					delete omElements[i].poStatic;
				if(omElements[i].poProgressCtrl != NULL)
					delete omElements[i].poProgressCtrl;
				if(omElements[i].poBitmap1 != NULL)
					delete omElements[i].poBitmap1;
				if(omElements[i].poBitmap2 != NULL)
					delete omElements[i].poBitmap2;
				if(omElements[i].poBitmap3 != NULL)
					delete omElements[i].poBitmap3;

				omElements.DeleteAt(i);
				break;
			}
		}
		InvalidateRect(NULL);
	}
}
//---------------------------------------------------------------------------
void UDlgStatusBar::EnableElement(UINT ipID, BOOL bpEnable /*= TRUE*/)
{
	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		if(omElements[i].iID == ipID)
		{
			if(omElements[i].poStatic != NULL)
				omElements[i].poStatic->EnableWindow(bpEnable);
			//if(omElements[i].poProgressCtrl != NULL)
			//	omElements[i].poProgressCtrl->EnableWindow(bpEnable);

			break;
		}
	}
}
//---------------------------------------------------------------------------
bool UDlgStatusBar::DeleteStatusBar()
{
	delete this;
	return true;
}
//---------------------------------------------------------------------------
void UDlgStatusBar::ResetContent()
{
	int ilElements = omElements.GetSize();
	for(int i=0; i<ilElements; i++)
	{
		if(omElements[i].poStatic != NULL)
			delete omElements[i].poStatic;
		if(omElements[i].poProgressCtrl != NULL)
			delete omElements[i].poProgressCtrl;
		if(omElements[i].poBitmap1 != NULL)
			delete omElements[i].poBitmap1;
		if(omElements[i].poBitmap2 != NULL)
			delete omElements[i].poBitmap2;
		if(omElements[i].poBitmap3 != NULL)
			delete omElements[i].poBitmap3;
	}
	omElements.DeleteAll();

	omDCComp.DeleteDC();
 	omBkgndBrush.DeleteObject();
	omPenS.DeleteObject();
    omPenH.DeleteObject();
}
//---------------------------------------------------------------------------
