VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEmployee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private mEmployeeURNO As String
Private mEmployeeFirstName As String
Private mEmployeeSecondName As String
Private mEmployeeEntryDate As String
Private mEmployeeExitDate As String
Private mEmployeePENO As String
Private mEmployeeHasChildren As Boolean

Private mEmployeeWeeklyWorkingHours As New Collection
Private mEmployeeAbsences As New Collection
Private mBalanceZIF As New Collection
Private mBalanceHoliday As New Collection
Private mDutyGroup As New Collection
Private mOrgUnit As New Collection
Private mContractType As New Collection
Private mFunction As New Collection


Property Let EmployeeEntryDate(tmpStr As String)
    mEmployeeEntryDate = tmpStr
End Property
Property Get EmployeeEntryDate() As String
    EmployeeEntryDate = mEmployeeEntryDate
End Property

Property Let EmployeeExitDate(tmpStr As String)
    mEmployeeExitDate = tmpStr
End Property
Property Get EmployeeExitDate() As String
    EmployeeExitDate = mEmployeeExitDate
End Property

Property Let EmployeeSecondName(tmpStr As String)
    mEmployeeSecondName = tmpStr
End Property
Property Get EmployeeSecondName() As String
    EmployeeSecondName = mEmployeeSecondName
End Property


Property Let EmployeeFirstName(tmpStr As String)
    mEmployeeFirstName = tmpStr
End Property
Property Get EmployeeFirstName() As String
    EmployeeFirstName = mEmployeeFirstName
End Property


Property Let EmployeeURNO(tmpStr As String)
    mEmployeeURNO = tmpStr
End Property
Property Get EmployeeURNO() As String
    EmployeeURNO = mEmployeeURNO
End Property

Property Let EmployeePENO(tmpStr As String)
    mEmployeePENO = tmpStr
End Property
Property Get EmployeePENO() As String
    EmployeePENO = mEmployeePENO
End Property

Property Let EmployeeHasChildren(tmpBool As Boolean)
    mEmployeeHasChildren = tmpBool
End Property
Property Get EmployeeHasChildren() As Boolean
    EmployeeHasChildren = mEmployeeHasChildren
End Property


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.05.2001
' .
' . description :   accessing the collection mBalanceHoliday
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Function AddBalanceHoliday(strKey As String, strItem As String)
    If DoesKeyExist(mBalanceHoliday, strKey) <> True Then
        mBalanceHoliday.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveBalanceHoliday(vntKey As Variant)
    On Error Resume Next
    mBalanceHoliday.Remove (vntKey)
End Function

Function RemoveAllBalanceHoliday()
    Dim i As Integer
    For i = 1 To mBalanceHoliday.Count
         mBalanceHoliday.Remove (1)
    Next i
End Function

Property Get ItemBalanceHoliday(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemBalanceHoliday = mBalanceHoliday(vntIndexKey)
End Property

Property Get CountBalanceHoliday() As Long
    CountBalanceHoliday = mBalanceHoliday.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.05.2001
' .
' . description :   accessing the collection mBalanceZIV
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Function AddBalanceZIF(strKey As String, strItem As String)
    If DoesKeyExist(mBalanceZIF, strKey) <> True Then
        mBalanceZIF.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveBalanceZIF(vntKey As Variant)
    On Error Resume Next
    mBalanceZIF.Remove (vntKey)
End Function

Function RemoveAllBalanceZIF()
    Dim i As Integer
    For i = 1 To mBalanceZIF.Count
         mBalanceZIF.Remove (1)
    Next i
End Function

Property Get ItemBalanceZIF(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemBalanceZIF = mBalanceZIF(vntIndexKey)
End Property

Property Get CountBalanceZIF() As Long
    CountBalanceZIF = mBalanceZIF.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   31.05.2001
' .
' . description :   accessing the collection mDutyGroup
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function AddDutyGroup(strKey As String, strItem As String)
    If DoesKeyExist(mDutyGroup, strKey) <> True Then
        mDutyGroup.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveDutyGroup(vntKey As Variant)
    On Error Resume Next
    mDutyGroup.Remove (vntKey)
End Function

Function RemoveAllDutyGroup()
    Dim i As Integer
    For i = 1 To mDutyGroup.Count
         mDutyGroup.Remove (1)
    Next i
End Function

Property Get ItemDutyGroup(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemDutyGroup = mDutyGroup(vntIndexKey)
End Property

Property Get CountDutyGroup() As Long
    CountDutyGroup = mDutyGroup.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   31.05.2001
' .
' . description :   accessing the collection mOrgUnit
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function AddOrgUnit(strKey As String, strItem As String)
    If DoesKeyExist(mOrgUnit, strKey) <> True Then
        mOrgUnit.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveOrgUnit(vntKey As Variant)
    On Error Resume Next
    mOrgUnit.Remove (vntKey)
End Function

Function RemoveAllOrgUnit()
    Dim i As Integer
    For i = 1 To mOrgUnit.Count
         mOrgUnit.Remove (1)
    Next i
End Function

Property Get ItemOrgUnit(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemOrgUnit = mOrgUnit(vntIndexKey)
End Property

Property Get CountOrgUnit() As Long
    CountOrgUnit = mOrgUnit.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   31.05.2001
' .
' . description :   accessing the collection mContractType
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function AddContractType(strKey As String, strItem As String)
    If DoesKeyExist(mContractType, strKey) <> True Then
        mContractType.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveContractType(vntKey As Variant)
    On Error Resume Next
    mContractType.Remove (vntKey)
End Function

Function RemoveAllContractType()
    Dim i As Integer
    For i = 1 To mContractType.Count
         mContractType.Remove (1)
    Next i
End Function

Property Get ItemContractType(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemContractType = mContractType(vntIndexKey)
End Property

Property Get CountContractType() As Long
    CountContractType = mContractType.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   31.05.2001
' .
' . description :   accessing the collection mFunction
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function AddFunction(strKey As String, strItem As String)
    If DoesKeyExist(mFunction, strKey) <> True Then
        mFunction.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveFunction(vntKey As Variant)
    On Error Resume Next
    mFunction.Remove (vntKey)
End Function

Function RemoveAllFunction()
    Dim i As Integer
    For i = 1 To mFunction.Count
         mFunction.Remove (1)
    Next i
End Function

Property Get ItemFunction(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemFunction = mFunction(vntIndexKey)
End Property

Property Get CountFunction() As Long
    CountFunction = mFunction.Count
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.05.2001
' .
' . description :   accessing the collection mEmployeeAbsences
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Function AddEmployeeAbsences(strKey As String, strItem As String)
    If DoesKeyExist(mEmployeeAbsences, strKey) <> True Then
        mEmployeeAbsences.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveEmployeeAbsences(vntKey As Variant)
    On Error Resume Next
    mEmployeeAbsences.Remove (vntKey)
End Function

Function RemoveAllEmployeeAbsences()
    Dim i As Integer
    For i = 1 To mEmployeeAbsences.Count
         mEmployeeAbsences.Remove (1)
    Next i
End Function

Property Get ItemEmployeeAbsences(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemEmployeeAbsences = mEmployeeAbsences(vntIndexKey)
End Property

Property Get CountEmployeeAbsences() As Long
    CountEmployeeAbsences = mEmployeeAbsences.Count
End Property


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.05.2001
' .
' . description :   accessing the collection mEmployeeWeeklyWorkingHours
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Function AddEmployeeWeeklyWorkingHours(strKey As String, strItem As String)
    If DoesKeyExist(mEmployeeWeeklyWorkingHours, strKey) <> True Then
        mEmployeeWeeklyWorkingHours.Add Item:=strItem, key:=strKey
    End If
End Function

Function RemoveEmployeeWeeklyWorkingHours(vntKey As Variant)
    On Error Resume Next
    mEmployeeWeeklyWorkingHours.Remove (vntKey)
End Function

Function RemoveAllEmployeeWeeklyWorkingHours()
    Dim i As Integer
    For i = 1 To mEmployeeWeeklyWorkingHours.Count
         mEmployeeWeeklyWorkingHours.Remove (1)
    Next i
End Function

Property Get ItemEmployeeWeeklyWorkingHours(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemEmployeeWeeklyWorkingHours = mEmployeeWeeklyWorkingHours(vntIndexKey)
End Property

Property Get CountEmployeeWeeklyWorkingHours() As Long
    CountEmployeeWeeklyWorkingHours = mEmployeeWeeklyWorkingHours.Count
End Property


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.05.2001
' .
' . description :   helpers
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
End Function

Private Sub Class_Initialize()
    mEmployeeHasChildren = False
End Sub
