VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'- clsView
'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private mViewName As String
Private mViewYear As Integer
'Private mMaxAbsentPerWeek As Integer
Private mViewAbsentPattern As String

Private mDontCareOrgUnit As Boolean
Private mDontCareDutyGroup As Boolean
Private mDontCareFunction As Boolean
Private mDontCareContract As Boolean

Private mViewOrganizationUnit As New Collection
Private mViewDutyGroup As New Collection
Private mViewFunctionEmployee As New Collection
Private mViewContractType As New Collection
Private mViewAbsenceType As New Collection


Property Let ViewName(tmpViewName As String)
    mViewName = tmpViewName
End Property
Property Get ViewName() As String
    ViewName = mViewName
End Property


Property Let ViewYear(tmpViewYear As Integer)
    mViewYear = tmpViewYear
End Property
Property Get ViewYear() As Integer
    ViewYear = mViewYear
End Property


'Property Let ViewMaxAbsentPerWeek(tmpMaxAbsentPerWeek As Integer)
'    mMaxAbsentPerWeek = tmpMaxAbsentPerWeek
'End Property
'Property Get ViewMaxAbsentPerWeek() As Integer
'    ViewMaxAbsentPerWeek = mMaxAbsentPerWeek
'End Property


Property Let ViewAbsentPattern(tmpValue As String)
    mViewAbsentPattern = tmpValue
End Property
Property Get ViewAbsentPattern() As String
    ViewAbsentPattern = mViewAbsentPattern
End Property


Property Let DontCareOrgUnit(tmpValue As Boolean)
    mDontCareOrgUnit = tmpValue
End Property
Property Get DontCareOrgUnit() As Boolean
    DontCareOrgUnit = mDontCareOrgUnit
End Property


Property Let DontCareDutyGroup(tmpValue As Boolean)
    mDontCareDutyGroup = tmpValue
End Property
Property Get DontCareDutyGroup() As Boolean
    DontCareDutyGroup = mDontCareDutyGroup
End Property


Property Let DontCareFunction(tmpValue As Boolean)
    mDontCareFunction = tmpValue
End Property
Property Get DontCareFunction() As Boolean
    DontCareFunction = mDontCareFunction
End Property


Property Let DontCareContract(tmpValue As Boolean)
    mDontCareContract = tmpValue
End Property
Property Get DontCareContract() As Boolean
    DontCareContract = mDontCareContract
End Property


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   adding entries
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function AddOrgUnit(strIndexKey As String)
    mViewOrganizationUnit.Add strIndexKey
End Function

Function AddDutyGroup(strIndexKey As String)
    mViewDutyGroup.Add strIndexKey
End Function

Function AddFunctionEmployee(strIndexKey As String)
    mViewFunctionEmployee.Add strIndexKey
End Function

Function AddContractType(strIndexKey As String)
    mViewContractType.Add strIndexKey
End Function

Function AddAbsenceType(strIndexKey As String)
    mViewAbsenceType.Add strIndexKey
End Function

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   deleting entries
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function RemoveOrgUnit(vntIndexKey As Variant)
    mViewOrganizationUnit.Remove (vntIndexKey)
End Function

Function RemoveDutyGroup(vntIndexKey As Variant)
    mViewDutyGroup.Remove (vntIndexKey)
End Function

Function RemoveFunctionEmployee(vntIndexKey As Variant)
    mViewFunctionEmployee.Remove (vntIndexKey)
End Function

Function RemoveContractType(vntIndexKey As Variant)
    mViewContractType.Remove (vntIndexKey)
End Function

Function RemoveAbsenceType(vntIndexKey As Variant)
    mViewAbsenceType.Remove vntIndexKey
End Function

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' .
' . description :   deleting entries
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Function RemoveAllOrgUnit()
    Dim i As Integer
    For i = mViewOrganizationUnit.Count To 1 Step -1
         mViewOrganizationUnit.Remove (i)
    Next i
End Function

Function RemoveAllDutyGroup()
    Dim i As Integer
    For i = mViewDutyGroup.Count To 1 Step -1
         mViewDutyGroup.Remove (i)
    Next i
End Function

Function RemoveAllFunctionEmployee()
    Dim i As Integer
    For i = mViewFunctionEmployee.Count To 1 Step -1
         mViewFunctionEmployee.Remove (i)
    Next i
End Function

Function RemoveAllContractType()
    Dim i As Integer
    For i = mViewContractType.Count To 1 Step -1
         mViewContractType.Remove (i)
    Next i
End Function

Function RemoveAllAbsenceType()
    Dim i As Integer
    For i = mViewAbsenceType.Count To 1 Step -1
         mViewAbsenceType.Remove (i)
    Next i
End Function

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   getting single items
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Property Get ItemOrgUnit(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemOrgUnit = mViewOrganizationUnit(vntIndexKey)
End Property

Property Get ItemDutyGroup(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemDutyGroup = mViewDutyGroup(vntIndexKey)
End Property

Property Get ItemFunctionEmployee(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemFunctionEmployee = mViewFunctionEmployee(vntIndexKey)
End Property

Property Get ItemContractType(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemContractType = mViewContractType(vntIndexKey)
End Property

Property Get ItemAbsenceType(vntIndexKey As Variant) As String
    On Error Resume Next
    ItemAbsenceType = mViewAbsenceType(vntIndexKey)
End Property

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   counting the items
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Property Get CountOrgUnit() As Long
    CountOrgUnit = mViewOrganizationUnit.Count
End Property
Property Get CountDutyGroup() As Long
    CountDutyGroup = mViewDutyGroup.Count
End Property
Property Get CountFunctionEmployee() As Long
    CountFunctionEmployee = mViewFunctionEmployee.Count
End Property
Property Get CountContractType() As Long
    CountContractType = mViewContractType.Count
End Property
Property Get CountAbsenceType() As Long
    CountAbsenceType = mViewAbsenceType.Count
End Property

Private Sub Class_Initialize()
    mViewName = "<Default>"
    mViewYear = Year(Now)
    mMaxAbsentPerWeek = 3
End Sub
