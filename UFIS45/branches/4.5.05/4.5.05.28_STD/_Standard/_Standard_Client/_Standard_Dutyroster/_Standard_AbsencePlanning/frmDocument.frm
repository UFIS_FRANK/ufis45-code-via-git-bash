VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmDocument 
   Caption         =   "frmDocument"
   ClientHeight    =   11115
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12840
   Icon            =   "frmDocument.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   741
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   856
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   13440
      Top             =   960
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   480
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   12120
      Width           =   3615
   End
   Begin VB.PictureBox Picture1 
      Height          =   11415
      Left            =   480
      ScaleHeight     =   757
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   853
      TabIndex        =   0
      Top             =   315
      Width           =   12855
      Begin TABLib.TAB TabLeft 
         Height          =   735
         Left            =   840
         TabIndex        =   26
         Top             =   840
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   64
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1167"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   225
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         Tag             =   "1167"
         Top             =   5895
         Width           =   1815
      End
      Begin TABLib.TAB TabRight 
         Height          =   735
         Left            =   11160
         TabIndex        =   27
         Top             =   720
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   64
      End
      Begin VB.TextBox txtTable 
         Height          =   285
         Left            =   1200
         TabIndex        =   25
         Text            =   "DRR"
         Top             =   10800
         Width           =   495
      End
      Begin VB.TextBox txtData 
         Height          =   285
         Left            =   8400
         TabIndex        =   24
         Text            =   "20011005,48372661,460,109873108,29469538,U"
         Top             =   10800
         Width           =   4335
      End
      Begin VB.TextBox txtFields 
         Height          =   285
         Left            =   5160
         TabIndex        =   23
         Text            =   "SDAY,BSDU,SCOD,URNO,STFU,ROSL"
         Top             =   10800
         Width           =   3135
      End
      Begin VB.TextBox txtSelection 
         Height          =   285
         Left            =   2640
         TabIndex        =   22
         Text            =   "WHERE URNO = '109873108'"
         Top             =   10800
         Width           =   2415
      End
      Begin VB.TextBox txtCmd 
         Height          =   285
         Left            =   1800
         TabIndex        =   21
         Text            =   "IRT"
         Top             =   10800
         Width           =   615
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Command5"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   10800
         Width           =   975
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   6840
         TabIndex        =   18
         Text            =   "2001"
         Top             =   8520
         Width           =   1455
      End
      Begin VB.CommandButton Command1 
         Caption         =   "change year"
         Height          =   255
         Left            =   5160
         TabIndex        =   17
         Top             =   8520
         Width           =   1695
      End
      Begin VB.CommandButton Command4 
         Caption         =   "show HiddenServer"
         Height          =   255
         Left            =   3480
         TabIndex        =   16
         Top             =   8520
         Width           =   1695
      End
      Begin VB.CommandButton Command3 
         Caption         =   "show Hidden"
         Height          =   255
         Left            =   1800
         TabIndex        =   15
         Top             =   8520
         Width           =   1695
      End
      Begin VB.CommandButton Command2 
         Caption         =   "show DetailWindow"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   8520
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1164"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   300
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "1164"
         Top             =   7365
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1163"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "1163"
         Top             =   6660
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1162"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   225
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "1162"
         Top             =   5640
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1161"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   210
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "1161"
         Top             =   5385
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1160"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "1160"
         Top             =   4920
         UseMaskColor    =   -1  'True
         Width           =   1815
      End
      Begin VB.Frame FrameLeft 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   360
         TabIndex        =   3
         Top             =   240
         Width           =   15
      End
      Begin VB.Frame FrameLine 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   30
         Left            =   960
         TabIndex        =   2
         Top             =   240
         Width           =   4455
      End
      Begin TABLib.TAB TabHeader 
         Height          =   975
         Left            =   1920
         TabIndex        =   1
         Top             =   600
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   1720
         _StockProps     =   64
      End
      Begin TABLib.TAB TabMain 
         Height          =   1935
         Left            =   3840
         TabIndex        =   4
         Top             =   1800
         Width           =   8655
         _Version        =   65536
         _ExtentX        =   15266
         _ExtentY        =   3413
         _StockProps     =   64
      End
      Begin TABLib.TAB TabBlockedWeeks 
         Height          =   495
         Left            =   2280
         TabIndex        =   10
         Top             =   4680
         Width           =   8895
         _Version        =   65536
         _ExtentX        =   15690
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB TabMaxAbsWeek 
         Height          =   945
         Left            =   2190
         TabIndex        =   11
         Top             =   5235
         Width           =   8895
         _Version        =   65536
         _ExtentX        =   15690
         _ExtentY        =   1667
         _StockProps     =   64
      End
      Begin TABLib.TAB TabHRSMonth 
         Height          =   495
         Left            =   2175
         TabIndex        =   12
         Top             =   6450
         Width           =   8895
         _Version        =   65536
         _ExtentX        =   15690
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB TabHRSQuart 
         Height          =   495
         Left            =   2220
         TabIndex        =   13
         Top             =   7140
         Width           =   8895
         _Version        =   65536
         _ExtentX        =   15690
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin VB.Label lblYear 
         Height          =   495
         Left            =   240
         TabIndex        =   28
         Tag             =   "1190"
         Top             =   1680
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public omViewName As String                ' the name of the view
Public colTTLHeadsAbsent As New Collection ' we have to check the blocked weeks
Public colHrsAbsCol As New Collection      ' for calculating the information of the bottom-TABs
Public colTotalAbsCountWeekly As New Collection      ' for calculating the information of the bottom-TABs
Public colDateColumn As New Collection     ' lookup the column a date belongs to
Public colColumnDate As New Collection     ' lookup the date a column belongs to
Public colAbsType As New Collection        ' lookup the absences to be displayed in the current view

Public colOrgUnit As New Collection
Public colFunction As New Collection
Public colDutyGroup As New Collection
Public colContractType As New Collection

Dim imMonthNumber As Integer                ' number of displayed months (normally (always?) = 12)
Public imLeftTabWidth As Integer                ' the width of the left 3 rows of the TabMain
Public imRightTabWidth As Integer              ' the width of the right 2 rows of the TabMain
Dim imScrollbarWidth As Integer             ' width of a scrollbar
Dim imSrceenHeight As Integer               ' the height of the screen-resolution
Public imSrceenWidth As Integer                ' the width of the screen-resolution

Dim imColWidth As Integer                   ' the width of a column of a week
Dim imLineHeight As Integer                 ' the height for every line

Dim lmFirstColumnColor As Long
Dim lmSecondColumnColor As Long
Dim lmWeekRowColor As Long

Dim imActualYear As Integer

Dim omMainHeaderValues As String
Dim omMainHeaderColors As String

Private bAscendingCol1 As Boolean
Private bAscendingCol2 As Boolean
Private bAscendingCol3 As Boolean
Private imLastLine As Integer
Private imLastScrollLine As Integer
Private omTooltip As New clsTooltip


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   02.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Load()
    On Error GoTo ErrHdl

    ' + + + + + + + + Debug - comment these lines e.g. for testing BC + + + + + + + +
    Command1.Visible = False
    Command2.Visible = False
    Command3.Visible = False
    Command4.Visible = False
    Text1.Visible = False
    Command5.Visible = False
    txtTable.Visible = False
    txtCmd.Visible = False
    txtData.Visible = False
    txtFields.Visible = False
    txtSelection.Visible = False
    ' + + + + + + + + Debug + + + + + + + +
    
    Dim olElement
    Dim olView As clsView
    Dim i As Integer

    imLastLine = 0
    imLastScrollLine = 0

    LoadResStrings Me
    ' some global system calculations
    imScrollbarWidth = ogSystemMetrics.ScrollWidth
    imSrceenHeight = Screen.Height * (1 / Screen.TwipsPerPixelY)
    imSrceenWidth = Screen.Width * (1 / Screen.TwipsPerPixelX)
    Picture1.Move 0, 0, imSrceenWidth, imSrceenHeight

    imMonthNumber = 12                  ' number of displayed months
    Select Case imSrceenWidth
        Case 800:
            MsgBox "Your screen resolution is " + CStr(imSrceenWidth) + " x " + CStr(imSrceenHeight) & _
                ". Please change the resolution to 1024 x 768 at least!", vbCritical, "Insufficient screen resolution"
            End
        Case 1024:
            imLeftTabWidth = 178        ' width of the left TAB (with the names)
            imRightTabWidth = 80        ' width of the left TAB (with the balance information)
            imColWidth = 14             ' width of the columns in the main area
            imLineHeight = 15           ' the height of a line
            TabMain.CreateCellObj "LastName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "FirstName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "DateCol", vbWhite, vbBlack, 10, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_LastName", 12640511, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_FirstName", 12640511, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_DateCol", 12640511, vbBlack, 10, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_LastName", 12648384, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_FirstName", 12648384, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_DateCol", 12648384, vbBlack, 10, False, False, False, 0, "CourierNew"
        Case 1152:
            imLeftTabWidth = 190        ' width of the left TAB (with the names)
            imRightTabWidth = 37        ' width of the left TAB (with the balance information)
            imColWidth = 17             ' width of the columns in the main area
            imLineHeight = 17           ' the height of a line
            TabMain.CreateCellObj "LastName", vbWhite, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "FirstName", vbWhite, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "DateCol", vbWhite, vbBlack, 11, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_LastName", 12640511, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_FirstName", 12640511, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_DateCol", 12640511, vbBlack, 11, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_LastName", 12648384, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_FirstName", 12648384, vbBlack, 13, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_DateCol", 12648384, vbBlack, 11, False, False, False, 0, "CourierNew"
        Case 1280:
            imLeftTabWidth = 190        ' width of the left TAB (with the names)
            imRightTabWidth = 47       ' width of the left TAB (with the balance information)
            imColWidth = 19             ' width of the columns in the main area
            imLineHeight = 20           ' the height of a line
            TabMain.CreateCellObj "LastName", vbWhite, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "FirstName", vbWhite, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "DateCol", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_LastName", 12640511, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_FirstName", 12640511, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_DateCol", 12640511, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_LastName", 12648384, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_FirstName", 12648384, vbBlack, 14, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_DateCol", 12648384, vbBlack, 12, False, False, False, 0, "CourierNew"
        Case Else:
            imLeftTabWidth = 178        ' width of the left TAB (with the names)
            imRightTabWidth = 80        ' width of the left TAB (with the balance information)
            imColWidth = 14             ' width of the columns in the main area
            imLineHeight = 15           ' the height of a line
            TabMain.CreateCellObj "LastName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "FirstName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "DateCol", vbWhite, vbBlack, 10, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_LastName", 12640511, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_FirstName", 12640511, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Marked_DateCol", 12640511, vbBlack, 10, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_LastName", 12648384, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_FirstName", 12648384, vbBlack, 12, False, False, False, 0, "CourierNew"
            TabMain.CreateCellObj "Kids_DateCol", 12648384, vbBlack, 10, False, False, False, 0, "CourierNew"
    End Select
    TabBlockedWeeks.CreateCellObj "Blue", vbBlue, vbBlack, 12, False, False, False, 0, "Wingdings"
    TabBlockedWeeks.CreateCellObj "Black", vbBlack, vbWhite, 12, False, False, False, 0, "Wingdings"
    TabBlockedWeeks.CreateCellObj "Red", vbRed, vbBlack, 12, False, False, False, 0, "Wingdings"

    TabMain.SetColumnProperty 1, "LastName"
    TabMain.SetColumnProperty 2, "FirstName"
    TabMain.SetColumnProperty 3, "DateCol"

    LoadResStrings Me

    ' look after the year of the actual view
    If Trim(Me.omViewName) <> "" Then
        For Each olElement In gcViews
            If olElement.ViewName = Me.omViewName Then
                Set olView = olElement              '"pointer" to the actual view
                imActualYear = olView.ViewYear
                Exit For
            End If
        Next
    Else
        imActualYear = Year(Now)
    End If

    lmFirstColumnColor = 12640511
    lmSecondColumnColor = 12648447
    lmWeekRowColor = 14737632
    bAscendingCol1 = True
    bAscendingCol2 = True
    bAscendingCol3 = True

    Init_TabHeader (imActualYear)
    Init_TabMain
    Init_TabBlockedWeeks
    Init_TabMaxAbsWeek
    Init_TabHRSMonth
    Init_TabHRSQuart
    Init_OptionButtons
    HandleMainData
    Form_Resize
    Set olView = Nothing
    Timer1.Enabled = True
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Form_Load", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Form_Load", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' . description :   if the user activates the form, we have to set the toolbar
' .                 of the main-form.
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Activate()
    On Error GoTo ErrHdl
    Dim olView As clsView
    Dim olElement
    Dim blFound As Boolean

    blFound = False
    For Each olElement In gcViews
        If olElement.ViewName = Me.omViewName Then
            Set olView = olElement
            imActualYear = olView.ViewYear
            blFound = True
            Exit For
        End If
    Next olElement

    If blFound <> False Then
        Module1.fMainForm.cmbViews.Text = Me.omViewName
        'Module1.fMainForm.UpDown1.Value = olView.ViewMaxAbsentPerWeek
        'Module1.fMainForm.txtMaxAbsWeek.Text = olView.ViewMaxAbsentPerWeek
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Form_Activate", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Form_Activate", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' . description :   nearly like the Form_Load, but we needn't set some things twice
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub Form_Reload()
    On Error GoTo ErrHdl
    Dim olElement
    Dim olView As clsView

    ' look after the year of the actual view
    If Trim(Me.omViewName) <> "" Then
        For Each olElement In gcViews
            If olElement.ViewName = Me.omViewName Then
                Set olView = olElement              '"pointer" to the actual view
                imActualYear = olView.ViewYear
                lblYear.Caption = LoadResString(lblYear.Tag) & CStr(olView.ViewYear)
                Exit For
            End If
        Next
    Else
        imActualYear = Year(Now)
    End If

    Init_TabHeader (imActualYear)
    Init_TabMain
    Init_TabBlockedWeeks
    Init_TabMaxAbsWeek
    Init_TabHRSMonth
    Init_TabHRSQuart
    Init_OptionButtons
    Form_Resize
    Set olView = Nothing
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Form_Reload", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Form_Reload", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   02.04.2001 - 16.05.2001
' .
' . description :   this is the resize-event of the form! We have to look after
' .                 the position, size,... of all elements belonging to the document
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Resize()
    On Error Resume Next

    Dim ilLeftOffset As Integer     ' distance (in pixel) of the most left TAB to the left border (of the form)
    Dim ilTopOffset As Integer      ' distance (in pixel) of the topmost TAB to the top border (of the form)
    ilLeftOffset = 1
    ilTopOffset = 10

    ' look after the position of the TABs and Grids
    ' keep in mind: the PictureBox "Picture1" holds EVERYTHING! This was necessary, because
    ' it is not possible in VB6 to scroll a form, but it's possible to scroll a picture box.
    ' (This trick is recommended by BillyBoy himself...
    ' ... see: http://support.microsoft.com/support/KB/ARTICLES/Q109/7/41.asp)
    Picture1.Width = imSrceenWidth
    Picture1.Height = Me.Height * (1 / Screen.TwipsPerPixelY) - HScroll1.Height
    Picture1.Move 0, 0, Picture1.Width, Picture1.Height
    TabHRSMonth.Width = TabHeader.Width + imRightTabWidth
    TabHRSQuart.Width = TabHeader.Width + imRightTabWidth
    TabBlockedWeeks.Width = TabHeader.Width
    TabMaxAbsWeek.Width = TabHeader.Width
    TabHeader.Move (ilLeftOffset + imLeftTabWidth), ilTopOffset, (53 * imColWidth), 60
'    If imSrceenWidth > 1024 Then
'        TabMain.Height = (ScaleHeight - (10 * imLineHeight))
'    Else
'        TabMain.Height = (ScaleHeight - (12 * imLineHeight))
'    End If
    Select Case imSrceenHeight
        Case 768: '(1024 x 768), (2048 x 768), ...
            TabMain.Height = (ScaleHeight - (12 * imLineHeight))
        Case 864: '(1152 x 864), (2304 x 864), ...
            TabMain.Height = (ScaleHeight - (11 * imLineHeight))
        Case Else: 'e.g. (1280 x 1024), ...
            TabMain.Height = (ScaleHeight - (10 * imLineHeight))
    End Select
    If frmHiddenServerConnection.strSHOW_HEADS_PER_WEEK_LINE = "TRUE" Then
        TabMain.Height = TabMain.Height - imLineHeight
    End If

    TabMain.Move ilLeftOffset, TabHeader.Top + TabHeader.Height, TabMain.Width, TabMain.Height
    TabBlockedWeeks.Move ilLeftOffset + imLeftTabWidth, TabMain.Top + TabMain.Height, TabBlockedWeeks.Width, TabBlockedWeeks.Height
    TabMaxAbsWeek.Move ilLeftOffset + imLeftTabWidth, TabBlockedWeeks.Top + TabBlockedWeeks.Height, TabMaxAbsWeek.Width, TabMaxAbsWeek.Height
    TabHRSMonth.Move ilLeftOffset + imLeftTabWidth, TabMaxAbsWeek.Top + TabMaxAbsWeek.Height, TabHRSMonth.Width, TabHRSMonth.Height
    TabHRSQuart.Move ilLeftOffset + imLeftTabWidth, TabHRSMonth.Top + TabHRSMonth.Height, TabHRSQuart.Width, TabHRSQuart.Height

    ' do some paint-improvements of the TAB-painting
    FrameLine.Left = TabMain.Left
    FrameLine.Width = TabMain.Width
    FrameLine.Top = TabMain.Top - 1
    FrameLeft.Left = TabMain.Left
    FrameLeft.Height = TabMain.Height
    FrameLeft.Top = TabMain.Top

    ' look after the option-buttons left beside the bottom-TABs
    Option1(0).Move ilLeftOffset, TabMain.Top + TabMain.Height, imLeftTabWidth, imLineHeight
    Option1(1).Move ilLeftOffset, Option1(0).Top + imLineHeight, imLeftTabWidth, imLineHeight
    Option1(2).Move ilLeftOffset, Option1(1).Top + imLineHeight, imLeftTabWidth, imLineHeight
    If frmHiddenServerConnection.strSHOW_HEADS_PER_WEEK_LINE = "TRUE" Then
        Option1(5).Move ilLeftOffset, Option1(2).Top + imLineHeight, imLeftTabWidth, imLineHeight
        Option1(3).Move ilLeftOffset, Option1(5).Top + imLineHeight, imLeftTabWidth, imLineHeight
    Else
        Option1(5).Visible = False
        Option1(3).Move ilLeftOffset, Option1(2).Top + imLineHeight, imLeftTabWidth, imLineHeight
    End If
    Option1(4).Move ilLeftOffset, Option1(3).Top + imLineHeight, imLeftTabWidth, imLineHeight

    ' position of the left TAB
    Init_TabLeft
    TabLeft.Move TabHeader.Left - TabLeft.Width, TabHeader.Top, TabLeft.Width, TabLeft.Height

    ' position of the right TAB
    Init_TabRight
    TabRight.Move TabHeader.Left + TabHeader.Width, TabHeader.Top + TabHeader.LineHeight, _
                  TabRight.Width, TabRight.Height

    ' Position the scroll bar:
    HScroll1.Left = 0
    If Picture1.Width > ScaleWidth Then
       HScroll1.Top = ScaleHeight - HScroll1.Height
    Else
       HScroll1.Top = ScaleHeight
    End If
    HScroll1.Width = ScaleWidth

    ' Set the scroll bar range
    HScroll1.Max = TabMain.Width - (Me.Width * (1 / Screen.TwipsPerPixelX)) + imScrollbarWidth
    HScroll1.SmallChange = Abs(HScroll1.Max \ 16) + 1
    HScroll1.LargeChange = Abs(HScroll1.Max \ 4) + 1
    HScroll1.ZOrder 0
    Picture1.Left = -HScroll1.Value
    Me.Refresh
    DoEvents
    Module1.Sleep 0.1
    
    ' Set the label for displaying the actual year
    'lblYear.Move ilLeftOffset, TabHeader.Top + imLineHeight, imLeftTabWidth, TabHeader.Height

'    TabMain.EnableInlineEdit True
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.06.2001
' .
' . description :   init all TABs
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Init_TabLeft()
    On Error GoTo ErrHdl
    Dim strHeaderLen As String
    ' do the layout
    TabLeft.ResetContent
    TabLeft.ShowVertScroller False
    TabLeft.ShowRowSelection = False
    TabLeft.MainHeaderOnly = True
    TabLeft.LineHeight = TabHeader.LineHeight
    TabLeft.FontName = "Arial"
    Select Case imSrceenWidth
        Case 1024:
            TabLeft.FontSize = 11
            strHeaderLen = "60,40,"
            TabLeft.Width = 100
            TabLeft.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 11, False, False, True, 0, "Arial"
        Case 1152:
            TabLeft.FontSize = 13
            strHeaderLen = "60,50,"
            TabLeft.Width = 110
            TabLeft.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 13, False, False, True, 0, "Arial"
        Case 1280:
            TabLeft.FontSize = 14
            strHeaderLen = "60,50,"
            TabLeft.Width = 110
            TabLeft.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 14, False, False, True, 0, "Arial"
        Case Else:
            TabLeft.FontSize = 11
            strHeaderLen = "60,40,"
            TabLeft.Width = 100
            TabLeft.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 11, False, False, True, 0, "Arial"
    End Select
    TabLeft.Height = TabHeader.Height ' - TabHeader.LineHeight
    TabLeft.HeaderLengthString = strHeaderLen
    TabLeft.HeaderString = ",,"
    TabLeft.InsertTextLine LoadResString(1190) & "," & CStr(imActualYear), True
    TabLeft.InsertTextLine LoadResString(1141) & ",", True
    TabLeft.InsertTextLine LoadResString(1142) & "," & LoadResString(1143), True
    TabLeft.InsertTextLine LoadResString(1144) & "," & LoadResString(1145), True
    TabLeft.SetLineColor 0, vbBlack, lmWeekRowColor
    TabLeft.SetCellProperty 0, 0, "BoldGrey"
    TabLeft.SetCellProperty 0, 1, "BoldGrey"
    TabLeft.SetCellProperty 1, 0, "BoldGrey"
    TabLeft.SetCellProperty 1, 1, "BoldGrey"
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabLeft", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabLeft", Err
    Err.Clear
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   09.06.2001
' .
' . description :   init all TABs
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Init_TabRight()
    On Error GoTo ErrHdl
    Dim strHeaderLen As String
    Dim strWidth As String
    strWidth = CStr(imRightTabWidth)
    ' do the layout
    TabRight.ResetContent
    TabRight.ShowVertScroller False
    TabRight.ShowRowSelection = False
    TabRight.MainHeaderOnly = True
    TabRight.LineHeight = TabHeader.LineHeight
    TabRight.FontName = "Arial"
    Select Case imSrceenWidth
        Case 1024:
            TabRight.FontSize = 11
            TabRight.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 11, False, False, True, 0, "Arial"
            TabRight.CreateCellObj "Bold", vbWhite, vbBlack, 11, False, False, True, 0, "Arial"
        Case 1152:
            TabRight.FontSize = 13
            TabRight.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 13, False, False, True, 0, "Arial"
            TabRight.CreateCellObj "Bold", vbWhite, vbBlack, 13, False, False, True, 0, "Arial"
        Case 1280:
            TabRight.FontSize = 14
            TabRight.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 14, False, False, True, 0, "Arial"
            TabRight.CreateCellObj "Bold", vbWhite, vbBlack, 14, False, False, True, 0, "Arial"
        Case Else:
            TabRight.FontSize = 11
            TabRight.CreateCellObj "BoldGrey", lmWeekRowColor, vbBlack, 11, False, False, True, 0, "Arial"
            TabRight.CreateCellObj "Bold", vbWhite, vbBlack, 11, False, False, True, 0, "Arial"
    End Select
    TabRight.ColumnAlignmentString = "C"
    TabRight.Width = imRightTabWidth
    strHeaderLen = strWidth '& "," & strWidth
    TabRight.Height = TabHeader.Height - TabHeader.LineHeight
    TabRight.HeaderLengthString = strHeaderLen
    TabRight.HeaderString = ","
    TabRight.InsertTextLine LoadResString(1146), True ' & "," & LoadResString(1146), True
    TabRight.InsertTextLine LoadResString(1147), True ' & "," & LoadResString(1147), True
    TabRight.InsertTextLine LoadResString(1148), True ' & "," & LoadResString(1149), True
    TabRight.SetCellProperty 0, 0, "BoldGrey"
    'TabRight.SetCellProperty 0, 1, "BoldGrey"
    TabRight.SetCellProperty 2, 0, "Bold"
    'TabRight.SetCellProperty 2, 1, "Bold"
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabRight", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabRight", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   02.04.2001 - 15.05.2001
' .
' . description :   init all TABs
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Init_TabHeader(olYear As Integer)
    On Error GoTo ErrHdl
    Dim str As String
    Dim hlpStr1 As String
    Dim hlpStr2 As String

    Dim i, j As Integer
    Dim ilWeekCount As Integer
    Dim blAddRowEnd As Boolean
    Dim olDate As Date
    Dim olFirstDayOfYear As Date
    Dim olLastDayOfYear As Date
    Dim test As Date

    str = "1/1/" + CStr(olYear)
    olFirstDayOfYear = str
    str = "12/31/" + CStr(olYear)
    olLastDayOfYear = str

    ' do the layout
    TabHeader.ResetContent
    TabHeader.ShowVertScroller False
    TabHeader.ShowRowSelection = False
    TabHeader.MainHeader = True
    TabHeader.MainHeaderOnly = True
    Select Case imSrceenWidth
        Case 1024:
            'TabHeader.FontName = "CourierNew"
            TabHeader.FontName = "Arial"
            TabHeader.FontSize = 12
        Case 1152:
            'TabHeader.FontName = "CourierNew"
            TabHeader.FontName = "Arial"
            TabHeader.FontSize = 13
        Case 1280:
            'TabHeader.FontName = "CourierNew"
            TabHeader.FontName = "Arial"
            TabHeader.FontSize = 14
        Case Else:
            TabHeader.FontName = "Arial"
            TabHeader.FontSize = 12
    End Select

    ' init the HeaderLengthString
    str = ""
    For i = 1 To 53 Step 1
        hlpStr1 = CStr(imColWidth)
        str = str + hlpStr1 + ","
    Next i
    TabHeader.HeaderLengthString = str

    ' init the HeaderString and the alignment
    str = ""
    hlpStr1 = ""
    For i = 1 To 53 Step 1
        str = str + ","
        hlpStr1 = hlpStr1 + "R,"
    Next i
    TabHeader.HeaderString = str
    TabHeader.ColumnAlignmentString = hlpStr1

    'init the main header
    str = ""
    hlpStr1 = ""
    ogMonthString = ""
    If ((Weekday(olFirstDayOfYear) > vbSunday) And (Weekday(olFirstDayOfYear) < vbFriday)) Then
        hlpStr2 = LoadResString(1101) + "," + _
                  LoadResString(1102) + "," + _
                  LoadResString(1103) + "," + _
                  LoadResString(1104) + "," + _
                  LoadResString(1105) + "," + _
                  LoadResString(1106) + "," + _
                  LoadResString(1107) + "," + _
                  LoadResString(1108) + "," + _
                  LoadResString(1109) + "," + _
                  LoadResString(1110) + "," + _
                  LoadResString(1111) + "," + _
                  LoadResString(1112)
        If ((Weekday(olLastDayOfYear) > vbSunday) And (Weekday(olLastDayOfYear) < vbThursday)) Then
            hlpStr2 = hlpStr2 + "," + Left(LoadResString(1101), 1)
            blAddRowEnd = True
        End If
    Else
        hlpStr2 = Left(LoadResString(1112), 1) + "," + _
                  LoadResString(1101) + "," + _
                  LoadResString(1102) + "," + _
                  LoadResString(1103) + "," + _
                  LoadResString(1104) + "," + _
                  LoadResString(1105) + "," + _
                  LoadResString(1106) + "," + _
                  LoadResString(1107) + "," + _
                  LoadResString(1108) + "," + _
                  LoadResString(1109) + "," + _
                  LoadResString(1110) + "," + _
                  LoadResString(1111) + "," + _
                  LoadResString(1112)
        blAddRowEnd = False
        str = "1,"
        ogMonthString = "12/" + CStr(olYear - 1) + ","
    End If
    For i = 1 To 12 Step 1
        ilWeekCount = frmHidden.GetNumberOfWeeks(CByte(i), olYear)
        str = str + CStr(ilWeekCount) + ","
        If (i Mod 2) = 0 Then
            hlpStr1 = hlpStr1 + CStr(lmSecondColumnColor) + ","
        Else
            hlpStr1 = hlpStr1 + CStr(lmFirstColumnColor) + ","
        End If
        For j = 1 To ilWeekCount
            ogMonthString = ogMonthString + CStr(Format(i, "00")) + "/" + CStr(olYear) + ","
        Next j
    Next i
    hlpStr1 = hlpStr1 + CStr(lmFirstColumnColor)
    If blAddRowEnd Then
        str = str + "1" + ","
        ogMonthString = ogMonthString + "01/" + CStr(olYear) + ","
    End If
    ogMonthString = Left(ogMonthString, Len(ogMonthString) - 1)
    ogMainHeaderRanges = str

    omMainHeaderColors = hlpStr1
    omMainHeaderValues = hlpStr2
    TabHeader.SetMainHeaderValues ogMainHeaderRanges, omMainHeaderValues, omMainHeaderColors

    ' fill it with the weeks
    If ((Weekday(olFirstDayOfYear) > vbSunday) And (Weekday(olFirstDayOfYear) < vbFriday)) Then
        str = ""
        For i = 1 To 52 Step 1
            hlpStr1 = CStr(i)
            str = str + hlpStr1 + ","
        Next i
    Else
        Dim CountWeeks As Byte
        For i = 1 To 12 Step 1
            CountWeeks = CountWeeks + frmHidden.GetNumberOfWeeks(CByte(i), olYear - 1)
        Next i
        str = CStr(CountWeeks) + ","
        'str = "53,"
        For i = 1 To 52 Step 1
            hlpStr1 = CStr(i)
            str = str + hlpStr1 + ","
        Next i
    End If
    hlpStr1 = "12/31/" + CStr(Year(olFirstDayOfYear))
    If ((Weekday(CDate(hlpStr1)) > vbSunday) And (Weekday(CDate(hlpStr1)) < vbThursday)) Then
        str = str + CStr("1")
    Else
        str = str + CStr("53")
    End If
    TabHeader.InsertTextLine str, False
    TabHeader.SetLineColor 0, vbBlack, lmWeekRowColor

    ' fill it with the first day of the weeks
    str = ""
    olDate = olFirstDayOfYear
    If (Weekday(olDate) <> vbMonday) Then
        While (Weekday(olDate) <> vbMonday)
            olDate = olDate - 1
        Wend
        str = CStr(Day(olDate)) + ","
    End If
    For i = 1 To 12 Step 1
        str = str + frmHidden.GetMondaysOfMonth(CByte(i), olYear)
    Next i
    TabHeader.InsertTextLine str, False

    ' fill it with the last day of the weeks
    str = ""
    For i = 1 To 12 Step 1
        str = str + frmHidden.GetSundaysOfMonth(CByte(i), olYear)
    Next i

    str = str + CStr(frmHidden.GetFirstSundayOfMonth(CByte(1), olYear + 1))
    TabHeader.InsertTextLine str, False
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabHeader", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabHeader", Err
    Err.Clear
End Sub

Private Sub Init_TabMain()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim hlpStr As String
    Dim str As String

    Dim ilWdithName As Integer
    Dim ilWdithSecondName As Integer
    Dim ilWdithEntryDate As Integer
    Dim ilWdithBalance As Integer
    Dim ilLineHeight As Integer

    Select Case imSrceenWidth
        Case 1024:
            TabMain.FontName = "CourierNew"
            TabMain.FontSize = 12
            ilWdithName = 78
            ilWdithSecondName = 60
            ilWdithEntryDate = 39
        Case 1152:
            TabMain.FontName = "CourierNew"
            TabMain.FontSize = 13
            ilWdithName = 80
            ilWdithSecondName = 60
            ilWdithEntryDate = 50
        Case 1280:
            TabMain.FontName = "CourierNew"
            TabMain.FontSize = 14
            ilWdithName = 80
            ilWdithSecondName = 60
            ilWdithEntryDate = 49
        Case Else:
            TabMain.FontName = "CourierNew"
            TabMain.FontSize = 12
            ilWdithName = 78
            ilWdithSecondName = 60
            ilWdithEntryDate = 39
    End Select

    ilWdithBalance = imRightTabWidth
    ilLineHeight = imLineHeight

    ' do the layout
    TabMain.ResetContent
    DoEvents
    TabMain.ShowVertScroller True
    TabMain.ShowRowSelection = True
    TabMain.MainHeaderOnly = True
    
    TabMain.DateTimeSetColumn 3
    TabMain.DateTimeSetInputFormatString 3, "YYYYMMDD"
    TabMain.DateTimeSetOutputFormatString 3, "DDMMMYY"
    TabMain.LineHeight = ilLineHeight
    TabMain.SelectBackColor = RGB(255, 255, 128) '14737632
    TabMain.SelectTextColor = vbBlack

    ' init  the alignment
    str = "L,L,L,R,"
    For i = 1 To 53 Step 1
        str = str + "C,"
    Next i
    str = str + "R,"
    TabMain.ColumnAlignmentString = str

    ' init the month-strings of the TAB
    str = LoadResString(1101) + "," + _
          LoadResString(1102) + "," + _
          LoadResString(1103) + "," + _
          LoadResString(1104) + "," + _
          LoadResString(1105) + "," + _
          LoadResString(1106) + "," + _
          LoadResString(1107) + "," + _
          LoadResString(1108) + "," + _
          LoadResString(1109) + "," + _
          LoadResString(1110) + "," + _
          LoadResString(1111) + "," + _
          LoadResString(1112)
    TabMain.DateTimeSetMonthNames str

    ' init the HeaderLengthString
    str = "0," + CStr(ilWdithName) + "," + CStr(ilWdithSecondName) + "," + CStr(ilWdithEntryDate) + ","
    hlpStr = CStr(imColWidth)
    str = str + CStr(imColWidth + 1) + ","
    For i = 1 To 51 Step 1
        str = str + hlpStr + ","
    Next i
    str = str + CStr(imColWidth - 1) + ","
    str = str + CStr(ilWdithBalance)
    TabMain.HeaderLengthString = str

    ' init the main header string
    TabMain.SetMainHeaderValues "4," + ogMainHeaderRanges + ",1", _
        "", _
        CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + CStr(lmSecondColumnColor) + "," + omMainHeaderColors + "," + CStr(lmSecondColumnColor) + "," + CStr(lmSecondColumnColor)

    ' init the HeaderString
    str = ""
    For i = 1 To 58 Step 1
        str = str + ","
    Next i
    TabMain.HeaderString = str

    ' init the width and height
    TabMain.Width = ilWdithName + ilWdithSecondName + ilWdithEntryDate + ilWdithBalance + 53 * imColWidth + imScrollbarWidth
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabMain", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabMain", Err
    Err.Clear
    Resume Next
End Sub

Private Sub Init_TabBlockedWeeks()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim hlpStr As String
    Dim strHeaderLengthString As String
    Dim strHeaderString As String

    ' do the layout
    TabBlockedWeeks.ResetContent
    TabBlockedWeeks.ShowVertScroller True
    TabBlockedWeeks.ShowRowSelection = False 'True
    TabBlockedWeeks.MainHeaderOnly = True
    TabBlockedWeeks.LineHeight = imLineHeight

    ' init the width and height
    TabBlockedWeeks.Width = TabHeader.Width
    TabBlockedWeeks.Height = imLineHeight

    ' init the Strings (HeaderLengthString and HeaderString)
    hlpStr = CStr(imColWidth)
    For i = 1 To 53 Step 1
        strHeaderLengthString = strHeaderLengthString + hlpStr + ","
        strHeaderString = strHeaderString + ","
    Next i
    TabBlockedWeeks.HeaderLengthString = strHeaderLengthString
    TabBlockedWeeks.HeaderString = strHeaderString
    TabBlockedWeeks.InsertTextLine strHeaderString, True

    ' init the main header string
    TabBlockedWeeks.SetMainHeaderValues ogMainHeaderRanges, _
        ",,,,,,,,,,,,,", _
        CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + CStr(lmSecondColumnColor) + "," + omMainHeaderColors + "," + CStr(lmSecondColumnColor) + "," + CStr(lmSecondColumnColor)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabBlockedWeeks", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabBlockedWeeks", Err
    Err.Clear
End Sub

Private Sub Init_TabMaxAbsWeek()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim hlpStr As String
    Dim strHeaderLengthString As String
    Dim strHeaderString As String

    ' do the layout
    TabMaxAbsWeek.ResetContent
    TabMaxAbsWeek.ShowVertScroller True
    TabMaxAbsWeek.ShowRowSelection = True
    TabMaxAbsWeek.MainHeaderOnly = True
    TabMaxAbsWeek.LineHeight = imLineHeight
    TabMaxAbsWeek.SelectBackColor = RGB(255, 255, 128) '14737632
    TabMaxAbsWeek.SelectTextColor = vbBlack
    Select Case imSrceenWidth
        Case 1024:
            TabMaxAbsWeek.FontName = "CourierNew"
            TabMaxAbsWeek.FontSize = 12
        Case 1152:
            TabMaxAbsWeek.FontName = "CourierNew"
            TabMaxAbsWeek.FontSize = 13
        Case 1280:
            TabMaxAbsWeek.FontName = "CourierNew"
            TabMaxAbsWeek.FontSize = 14
        Case Else:
            TabMaxAbsWeek.FontName = "CourierNew"
            TabMaxAbsWeek.FontSize = 12
    End Select

    ' init the width and height
    TabMaxAbsWeek.Width = TabHeader.Width
    If frmHiddenServerConnection.strSHOW_HEADS_PER_WEEK_LINE = "TRUE" Then
        TabMaxAbsWeek.Height = 3 * imLineHeight
    Else
        TabMaxAbsWeek.Height = 2 * imLineHeight
    End If

    ' init the Strings (HeaderLengthString and HeaderString)
    hlpStr = CStr(imColWidth)
    For i = 1 To 53 Step 1
        strHeaderLengthString = strHeaderLengthString + hlpStr + ","
        strHeaderString = strHeaderString + ","
    Next i
    TabMaxAbsWeek.HeaderLengthString = strHeaderLengthString
    TabMaxAbsWeek.HeaderString = strHeaderString
    TabMaxAbsWeek.InsertTextLine strHeaderString, False
    TabMaxAbsWeek.InsertTextLine strHeaderString, False
    TabMaxAbsWeek.InsertTextLine strHeaderString, True

    ' init the main header string
    TabMaxAbsWeek.SetMainHeaderValues ogMainHeaderRanges, _
        ",,,,,,,,,,,,,", _
        CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + CStr(lmSecondColumnColor) + "," + omMainHeaderColors + "," + CStr(lmSecondColumnColor) + "," + CStr(lmSecondColumnColor)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabMaxAbsWeek", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabMaxAbsWeek", Err
    Err.Clear
End Sub

Private Sub Init_TabHRSMonth()
    On Error GoTo ErrHdl
    Dim i As Integer

    Dim strHeaderLengthString As String
    Dim strHeaderString As String

    ' do the layout
    TabHRSMonth.ResetContent
    TabHRSMonth.ShowVertScroller True
    TabHRSMonth.ShowRowSelection = True
    TabHRSMonth.MainHeaderOnly = True
    TabHRSMonth.LineHeight = imLineHeight
    TabHRSMonth.ColumnAlignmentString = "R,R,R,R,R,R,R,R,R,R,R,R,R,R"
    TabHRSMonth.SelectBackColor = RGB(255, 255, 128) '14737632
    TabHRSMonth.SelectTextColor = vbBlack
    Select Case imSrceenWidth
        Case 1024:
            TabHRSMonth.FontName = "CourierNew"
            TabHRSMonth.FontSize = 12
        Case 1152:
            TabHRSMonth.FontName = "CourierNew"
            TabHRSMonth.FontSize = 13
        Case 1280:
            TabHRSMonth.FontName = "CourierNew"
            TabHRSMonth.FontSize = 14
        Case Else:
            TabHRSMonth.FontName = "CourierNew"
            TabHRSMonth.FontSize = 12
    End Select

    ' init the width and height
    TabHRSMonth.Width = TabHeader.Width + imRightTabWidth
    TabHRSMonth.Height = imLineHeight

    ' init the Strings (HeaderLengthString and HeaderString)
    If GetItem(ogMainHeaderRanges, 1, ",") = 1 Then
        strHeaderLengthString = CStr(imColWidth) + ","
        strHeaderString = ","
        For i = 2 To ItemCount(ogMainHeaderRanges, ",") - 1 Step 1
            strHeaderLengthString = strHeaderLengthString + CStr(GetItem(ogMainHeaderRanges, i, ",") * imColWidth) + ","
            strHeaderString = strHeaderString + ","
        Next i
    ElseIf GetItem(ogMainHeaderRanges, (ItemCount(ogMainHeaderRanges, ",") - 1), ",") = 1 Then
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 1 Step 1
            strHeaderLengthString = strHeaderLengthString + CStr(GetItem(ogMainHeaderRanges, i, ",") * imColWidth) + ","
            strHeaderString = strHeaderString + ","
        Next i
    Else
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 1 Step 1
            strHeaderLengthString = strHeaderLengthString + CStr(GetItem(ogMainHeaderRanges, i, ",") * imColWidth) + ","
            strHeaderString = strHeaderString + ","
        Next i
    End If
    strHeaderLengthString = strHeaderLengthString + CStr(imRightTabWidth) + ","
    strHeaderString = strHeaderString + ","
    TabHRSMonth.HeaderLengthString = strHeaderLengthString
    TabHRSMonth.HeaderString = strHeaderString
    TabHRSMonth.InsertTextLine strHeaderString, True

    ' init the main header string
    TabHRSMonth.SetMainHeaderValues "1,1,1,1,1,1,1,1,1,1,1,1,1,1", _
        ",,,,,,,,,,,,,,", _
        CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + _
        CStr(lmSecondColumnColor) + "," + omMainHeaderColors + "," + _
        CStr(lmSecondColumnColor) + "," + CStr(lmSecondColumnColor) + "," + _
        CStr(lmSecondColumnColor)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabHRSMonth", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabHRSMonth", Err
    Err.Clear
End Sub


Private Sub Init_TabHRSQuart()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim ilAddWidth As Integer

    Dim strHeaderLengthString As String
    Dim strHeaderString As String

    ' do the layout
    TabHRSQuart.ResetContent
    TabHRSQuart.ShowVertScroller True
    TabHRSQuart.ShowRowSelection = True
    TabHRSQuart.MainHeaderOnly = True
    TabHRSQuart.LineHeight = imLineHeight
    TabHRSQuart.ColumnAlignmentString = "R,R,R,R,R,R"
    TabHRSQuart.SelectBackColor = RGB(255, 255, 128) '14737632
    TabHRSQuart.SelectTextColor = vbBlack
    Select Case imSrceenWidth
        Case 1024:
            TabHRSQuart.FontName = "CourierNew"
            TabHRSQuart.FontSize = 12
        Case 1152:
            TabHRSQuart.FontName = "CourierNew"
            TabHRSQuart.FontSize = 13
        Case 1280:
            TabHRSQuart.FontName = "CourierNew"
            TabHRSQuart.FontSize = 14
        Case Else:
            TabHRSQuart.FontName = "CourierNew"
            TabHRSQuart.FontSize = 12
    End Select

    ' init the width and height
    TabHRSQuart.Width = TabHeader.Width + imRightTabWidth
    TabHRSQuart.Height = imLineHeight

    ' init the Strings (HeaderLengthString and HeaderString)
    If GetItem(ogMainHeaderRanges, 1, ",") = 1 Then
        strHeaderLengthString = CStr(imColWidth) + ","
        strHeaderString = ","
        For i = 2 To ItemCount(ogMainHeaderRanges, ",") - 1 Step 1
            ilAddWidth = ilAddWidth + (GetItem(ogMainHeaderRanges, i, ",") * imColWidth)
            If (i - 1) Mod 3 = 0 Then
                strHeaderLengthString = strHeaderLengthString + CStr(ilAddWidth) + ","
                strHeaderString = strHeaderString + ","
                ilAddWidth = 0
            End If
        Next i
    ElseIf GetItem(ogMainHeaderRanges, (ItemCount(ogMainHeaderRanges, ",") - 1), ",") = 1 Then
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 2 Step 1
            ilAddWidth = ilAddWidth + (GetItem(ogMainHeaderRanges, i, ",") * imColWidth)
            If i Mod 3 = 0 Then
                strHeaderLengthString = strHeaderLengthString + CStr(ilAddWidth) + ","
                strHeaderString = strHeaderString + ","
                ilAddWidth = 0
            End If
        Next i
        strHeaderLengthString = strHeaderLengthString + CStr(imColWidth) + ","
        strHeaderString = strHeaderString + ","
    Else
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 1 Step 1
            ilAddWidth = ilAddWidth + (GetItem(ogMainHeaderRanges, i, ",") * imColWidth)
            If i Mod 3 = 0 Then
                strHeaderLengthString = strHeaderLengthString + CStr(ilAddWidth) + ","
                strHeaderString = strHeaderString + ","
                ilAddWidth = 0
            End If
        Next i
    End If

    strHeaderLengthString = strHeaderLengthString + CStr(imRightTabWidth) + ","
    strHeaderString = strHeaderString + ","
    TabHRSQuart.HeaderLengthString = strHeaderLengthString
    TabHRSQuart.HeaderString = strHeaderString
    TabHRSQuart.InsertTextLine strHeaderString, True

    ' init the main header string
    TabHRSQuart.SetMainHeaderValues "1,1,1,1,1,1", _
        ",,,,,,", _
        CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor) + "," + CStr(lmSecondColumnColor) + "," + CStr(lmFirstColumnColor)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_TabHRSQuart", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_TabHRSQuart", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' . description :   initialize the option-buttons
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Init_OptionButtons()
    On Error GoTo ErrHdl
    Dim i As Integer

    For i = 0 To Option1.Count - 1 Step 1
        Option1(i).Height = imLineHeight
        Option1(i).Width = imLeftTabWidth
    Next i
    If frmHiddenServerConnection.strSHOW_HEADS_PER_WEEK_LINE = "FALSE" Then
        Option1(5).Visible = False
    End If

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Init_OptionButtons", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Init_OptionButtons", Err
    Err.Clear
End Sub


Private Sub HandleMainData()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim j As Integer

    Dim tmpStr As String
    Dim strLine As String

    If Trim(Me.omViewName) <> "" Then
    Else ' just add empty lines ...
        For i = 0 To 100
            For j = 0 To 57
                tmpStr = tmpStr + ","
            Next j
            tmpStr = tmpStr + Chr(10)
        Next i

        TabMain.InsertBuffer tmpStr, Chr(10)
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.HandleMainData", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.HandleMainData", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' . description :   here starts the mouse-events
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

' . the horizontal scroll-bar
Private Sub HScroll1_Change()
    On Error GoTo ErrHdl
    Picture1.Left = -HScroll1.Value
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.HScroll1_Change", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.HScroll1_Change", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the option-buttons
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Option1_Click(Index As Integer)
    On Error GoTo ErrHdl
    Select Case Index
        Case 0:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection 0
            TabMaxAbsWeek.SetCurrentSelection -1
            TabHRSMonth.SetCurrentSelection -1
            TabHRSQuart.SetCurrentSelection -1
        Case 1:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection -1
            TabMaxAbsWeek.SetCurrentSelection 0
            TabHRSMonth.SetCurrentSelection -1
            TabHRSQuart.SetCurrentSelection -1
        Case 2:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection -1
            TabMaxAbsWeek.SetCurrentSelection 1
            TabHRSMonth.SetCurrentSelection -1
            TabHRSQuart.SetCurrentSelection -1
        Case 3:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection -1
            TabMaxAbsWeek.SetCurrentSelection -1
            TabHRSMonth.SetCurrentSelection 0
            TabHRSQuart.SetCurrentSelection -1
        Case 4:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection -1
            TabMaxAbsWeek.SetCurrentSelection -1
            TabHRSMonth.SetCurrentSelection -1
            TabHRSQuart.SetCurrentSelection 0
        Case 5:
            TabMain.SetCurrentSelection -1
            TabBlockedWeeks.SetCurrentSelection -1
            TabMaxAbsWeek.SetCurrentSelection 2
            TabHRSMonth.SetCurrentSelection -1
            TabHRSQuart.SetCurrentSelection -1
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.Option1_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.Option1_Click", Err
    Err.Clear
End Sub
Private Sub Option1_GotFocus(Index As Integer)
    TabMain.SetFocus
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabHeader
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabHeader_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
End Sub
Private Sub TabHeader_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
End Sub
Private Sub TabHeader_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabMain
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabMain_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    Dim i As Integer
    For i = 0 To 4
        Option1(i).Value = False
    Next i

    TabBlockedWeeks.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1
End Sub
Private Sub TabMain_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    On Error GoTo ErrHdl
    Me.SetFocus
    Dim i As Integer
    For i = 0 To 4
        Option1(i).Value = False
    Next i
    TabBlockedWeeks.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1

    If ColNo < 4 And LineNo < TabMain.GetLineCount And Me.omViewName <> "" Then
        Dim strLineSTF As String
        Dim strURNO As String

        ' get the ID of the person the click was on
        If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnDetailedWindow") = 1 Then
            strURNO = GetItem(TabMain.GetLineValues(LineNo), 1, ",")
        Else
            strURNO = Trim(ogActualUserURNO)
        End If

        If DoesKeyExist(gcEmployee, strURNO) <> False Then
            ' set the global variables of the actual employee
            Dim olEmployee As clsEmployee
            Set olEmployee = gcEmployee(strURNO)
            ogActualEmployeeNameFirst = olEmployee.EmployeeFirstName
            ogActualEmployeeNameSecond = olEmployee.EmployeeSecondName
            ogActualEmployeePENO = olEmployee.EmployeePENO
            ogActualEmployeeURNO = olEmployee.EmployeeURNO
        Else
            Exit Sub
        End If
        frmDetailWindow.Show vbModal
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.TabMain_SendLButtonDblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.TabMain_SendLButtonDblClick", Err
    Err.Clear
End Sub

Private Sub TabMain_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    If (LineNo < TabMain.GetLineCount) And (ColNo < 4) Then
        If (LineNo <> imLastLine) Or (omTooltip.ToolCount = 0) Then
            imLastLine = LineNo
            ShowToolTip imLastLine
        End If
    Else
        Call omTooltip.RemoveTool(TabMain)
    End If
End Sub

Private Sub TabMain_OnVScroll(ByVal LineNo As Long)
    Dim ilDiff As Integer
    ilDiff = imLastScrollLine - LineNo
    imLastScrollLine = LineNo
    imLastLine = imLastLine - ilDiff

    If omTooltip.ToolCount > 0 Then
        ShowToolTip imLastLine
    End If
End Sub

Private Function ShowToolTip(ByRef LineNo As Integer)
    Dim strToolTip As String
    Dim strURNO As String
    strURNO = TabMain.GetColumnValue(LineNo, 0)
    If DoesKeyExist(gcEmployee, strURNO) Then
        Dim olEmployee As clsEmployee
        Set olEmployee = gcEmployee.Item(strURNO)

        strToolTip = "LineNo: " & CStr(LineNo + 1) & vbCrLf & _
                     "Name: " & olEmployee.EmployeeFirstName & " " & olEmployee.EmployeeSecondName & vbCrLf & _
                     "Pers.No.: " & olEmployee.EmployeePENO & vbCrLf & _
                     "WeeklyWorkingHours: " & CStr(GetUsersWeeklyWorkingHours(olEmployee))

        With omTooltip
            Call .Create(Me)
            .MaxTipWidth = 240
            .DelayTime(ttDelayShow) = 20000
            Call .AddTool(TabMain)
            .ToolText(TabMain) = strToolTip
        End With
    End If
End Function

Private Function GetUsersWeeklyWorkingHours(rEmployee As clsEmployee) As Integer
    
    If rEmployee.CountEmployeeWeeklyWorkingHours > 0 Then
        If rEmployee.CountEmployeeWeeklyWorkingHours > 1 Then
            ' get the valid value for now
            Dim llThisColumnDate As Long
            Dim llHelp As Long
            Dim llBestFit As Long
            Dim j As Integer
            Dim strHelp As String

            llThisColumnDate = CLng(Year(Now) & Format(Month(Now), "00") & Format(Day(Now), "00"))

            For j = 1 To rEmployee.CountEmployeeWeeklyWorkingHours
                strHelp = GetItem(rEmployee.ItemEmployeeWeeklyWorkingHours(j), 1, "@")
                If IsNumeric(strHelp) = True Then
                    llHelp = CLng(Left(strHelp, 8))
                    If llHelp <= llThisColumnDate Then
                        If llHelp > llBestFit Then
                            llBestFit = llHelp
                            GetUsersWeeklyWorkingHours = Left(GetItem(rEmployee.ItemEmployeeWeeklyWorkingHours(j), 2, "@"), 2)
                        End If
                    End If
                End If
            Next j
        Else
            GetUsersWeeklyWorkingHours = Left(GetItem(rEmployee.ItemEmployeeWeeklyWorkingHours(1), 2, "@"), 2)
        End If
    Else
        GetUsersWeeklyWorkingHours = 40
    End If
 
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.GetUsersWeeklyWorkingHours", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.GetUsersWeeklyWorkingHours", Err
    Err.Clear
    Resume Next
End Function

Private Sub TabMain_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    On Error GoTo ErrHdl
    Me.SetFocus
    Dim i As Integer
    For i = 0 To 4
        Option1(i).Value = False
    Next i
    TabBlockedWeeks.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1

    ' now starts the part of the sorting. the sorting is started by a click with the right
    ' mouse-button. the sorting will toggle between descending and ascending (like a flip-flop).
    If Me.omViewName <> "" Then
        If ColNo < 4 Then
            If LineNo < TabMain.GetLineCount Then
                Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1154)
                Call omTooltip.RemoveTool(TabMain)
                Select Case ColNo
                    Case 1:
                        TabMain.Sort ColNo, bAscendingCol1, True
                        If bAscendingCol1 <> True Then
                            bAscendingCol1 = True
                        Else
                            bAscendingCol1 = False
                        End If
                    Case 2:
                        TabMain.Sort ColNo, bAscendingCol2, True
                        If bAscendingCol2 <> True Then
                            bAscendingCol2 = True
                        Else
                            bAscendingCol2 = False
                        End If
                    Case 3:
                        TabMain.Sort ColNo, bAscendingCol3, True
                        If bAscendingCol3 <> True Then
                            bAscendingCol3 = True
                        Else
                            bAscendingCol3 = False
                        End If
                End Select
                Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1153)
                imLastLine = TabMain.GetCurrentSelected
                ShowToolTip imLastLine
            End If
        End If
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.TabMain_SendRButtonClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.TabMain_SendRButtonClick", Err
    Err.Clear
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' Here starts the timer-part
' for coloring the lines after receiving a BC
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub Timer1_Timer()
    TabMain.TimerCheck
    DoEvents
End Sub
Private Sub TabMain_TimerExpired(ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim llLineStatus As Long
    llLineStatus = LineStatus

    If (llLineStatus And 1) = 1 Then
        If (llLineStatus And 4) = 4 Then
            'set it to "children-color"
            TabMain.SetLineColor LineNo, vbBlack, 12648384
            TabMain.SetCellProperty LineNo, 1, "Kids_LastName"
            TabMain.SetCellProperty LineNo, 2, "Kids_FirstName"
            TabMain.SetCellProperty LineNo, 3, "Kids_DateCol"
        Else
            'set it to normal color
            TabMain.SetLineColor LineNo, vbBlack, vbWhite
            TabMain.SetCellProperty LineNo, 1, "LastName"
            TabMain.SetCellProperty LineNo, 2, "FirstName"
            TabMain.SetCellProperty LineNo, 3, "DateCol"
        End If
        llLineStatus = llLineStatus - 1 'And (2147483647 - 1) '2147483647 is the max-value of long => all digits 1
        TabMain.SetLineStatusValue LineNo, llLineStatus
    ElseIf (llLineStatus And 2) = 2 Then
        TabMain.DeleteLine (LineNo)
    End If
    TabMain.RedrawTab
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabBlockedWeeks
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabBlockedWeeks_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMain.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1
    Option1(0).Value = True
    Option1(1).Value = False
    Option1(2).Value = False
    Option1(3).Value = False
    Option1(4).Value = False
End Sub
Private Sub TabBlockedWeeks_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabBlockedWeeks_SendLButtonClick LineNo, ColNo
End Sub
Private Sub TabBlockedWeeks_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabBlockedWeeks_SendLButtonClick LineNo, ColNo
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabMaxAbsWeek
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabMaxAbsWeek_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMain.SetCurrentSelection -1
    TabBlockedWeeks.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1
    Option1(0).Value = False
    If LineNo = 0 Then
        Option1(1).Value = True
        Option1(2).Value = False
    ElseIf LineNo = 1 Then
        Option1(1).Value = False
        Option1(2).Value = True
    End If
    Option1(3).Value = False
    Option1(4).Value = False
End Sub
Private Sub TabMaxAbsWeek_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMaxAbsWeek_SendLButtonClick LineNo, ColNo
End Sub
Private Sub TabMaxAbsWeek_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMaxAbsWeek_SendLButtonClick LineNo, ColNo
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabHRSMonth
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabHRSMonth_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMain.SetCurrentSelection -1
    TabBlockedWeeks.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSQuart.SetCurrentSelection -1
    Option1(0).Value = False
    Option1(1).Value = False
    Option1(2).Value = False
    Option1(3).Value = True
    Option1(4).Value = False
End Sub
Private Sub TabHRSMonth_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabHRSMonth_SendLButtonClick LineNo, ColNo
End Sub
Private Sub TabHRSMonth_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabHRSMonth_SendLButtonClick LineNo, ColNo
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . the TabHRSQuart
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub TabHRSQuart_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabMain.SetCurrentSelection -1
    TabBlockedWeeks.SetCurrentSelection -1
    TabMaxAbsWeek.SetCurrentSelection -1
    TabHRSMonth.SetCurrentSelection -1
    Option1(0).Value = False
    Option1(1).Value = False
    Option1(2).Value = False
    Option1(3).Value = False
    Option1(4).Value = True
End Sub
Private Sub TabHRSQuart_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabHRSQuart_SendLButtonClick LineNo, ColNo
End Sub
Private Sub TabHRSQuart_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Me.SetFocus
    TabHRSQuart_SendLButtonClick LineNo, ColNo
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' . description :   after changing the temporary number for the MaxAbsPerWeek in
' .                 the fMainForm.txtMaxAbsWeek we have to look after the colors
' .                 in the TabBlockedWeeks
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub CheckMaxAbsences()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim ilCompareValue As Integer
    Dim strKey As String
    Dim strLine As String
    strLine = TabMaxAbsWeek.GetLineValues(0)

    Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1155)

    For i = 1 To 53 Step 1
        strKey = CStr(i)
        'ilCompareValue = CInt(TabMaxAbsWeek.GetItem(i - 1))
        ilCompareValue = CInt(GetItem(strLine, i, ","))

        If DoesKeyExist(colTTLHeadsAbsent, strKey) = True Then

            If colTTLHeadsAbsent(strKey) > ilCompareValue Then
                TabBlockedWeeks.SetCellProperty 0, i - 1, "Red"
            ElseIf colTTLHeadsAbsent(strKey) = ilCompareValue Then
                TabBlockedWeeks.SetCellProperty 0, i - 1, "Black"
            Else
                TabBlockedWeeks.SetCellProperty 0, i - 1, "Blue"
            End If
        Else
            If ilCompareValue <> 0 Then
                TabBlockedWeeks.SetCellProperty 0, i - 1, "Blue"
            Else
                TabBlockedWeeks.SetCellProperty 0, i - 1, "Black"
            End If
        End If
    Next i

    'TabMaxAbsWeek.UpdateTextLine 0, strMaxAbs, True
    TabBlockedWeeks.RedrawTab

    Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1153)
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.CheckMaxAbsences", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.CheckMaxAbsences", Err
    Err.Clear
    Resume Next
End Sub

Public Sub CheckLineColors()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim strLine As String
    Dim LineNo As Long

    strLine = TabMain.GetLinesByStatusValue(4, 1)

    For i = 1 To ItemCount(strLine, ",")
        LineNo = CLng(GetItem(strLine, i, ","))
        TabMain.SetLineColor LineNo, vbBlack, 12648384
        TabMain.SetCellProperty LineNo, 1, "Kids_LastName"
        TabMain.SetCellProperty LineNo, 2, "Kids_FirstName"
        TabMain.SetCellProperty LineNo, 3, "Kids_DateCol"
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDocument.CheckMaxAbsences", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDocument.CheckMaxAbsences", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   02.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Unload(Cancel As Integer)
    Unload frmHidden
End Sub








' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
' * * only for debugging
' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Private Sub Command1_Click()
    Init_TabHeader (Text1.Text)
    Init_TabMain
    HandleMainData
    Form_Resize
End Sub

Private Sub Command2_Click()
    frmDetailWindow.Show vbModal
End Sub

Private Sub Command3_Click()
    Dim str As String
    str = "1.12." & Text1.Text
    frmHidden.MonthView1.Value = str
    frmHidden.MonthView1.Refresh
    frmHidden.Show vbModal
End Sub

Private Sub Command4_Click()
    frmHiddenServerConnection.Show vbModal
End Sub
Private Sub Command5_Click()
    Dim strCmd As String
    Dim strSelection As String
    Dim strFields As String
    Dim strData As String
    strCmd = txtCmd.Text
    strSelection = txtSelection.Text
    strFields = txtFields.Text
    strData = txtData.Text

    Select Case txtTable.Text
        Case "DRR":
            Module1.fMainForm.BC.HandleBC_DRRTAB strCmd, strSelection, strFields, strData, 4711
        Case "ACC":
            Module1.fMainForm.BC.HandleBC_ACCTAB strCmd, strSelection, strFields, strData, 4711
        Case "SCO":
            Module1.fMainForm.BC.HandleBC_SCOTAB strCmd, strSelection, strFields, strData, 4711
        Case Else:
            MsgBox "Table " + txtTable.Text + " does not exist."
    End Select
End Sub
