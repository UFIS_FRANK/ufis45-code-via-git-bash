#if !defined(AFX_PROGRESSDLG_H__C754ECB5_2A2C_11D3_A63B_0000C007916B__INCLUDED_)
#define AFX_PROGRESSDLG_H__C754ECB5_2A2C_11D3_A63B_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ProgressDlg dialog

class ProgressDlg : public CDialog
{
// Construction
public:
	ProgressDlg(CWnd* pParent = NULL);   // standard constructor

	void Create();
// Dialog Data
	//{{AFX_DATA(ProgressDlg)
	enum { IDD = IDD_PROGRESS_DLG };
	CProgressCtrl	m_ProgressCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ProgressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	CWnd* pomParent;

	// Generated message map functions
	//{{AFX_MSG(ProgressDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESSDLG_H__C754ECB5_2A2C_11D3_A63B_0000C007916B__INCLUDED_)
