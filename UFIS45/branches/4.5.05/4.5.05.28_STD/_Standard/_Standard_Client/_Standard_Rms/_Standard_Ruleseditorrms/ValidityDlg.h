#if !defined(AFX_VALIDITYDLG_H__FC4E17E1_FD4B_11D2_A616_0000C007916B__INCLUDED_)
#define AFX_VALIDITYDLG_H__FC4E17E1_FD4B_11D2_A616_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



#include <DialogGrid.h>


typedef struct ExcludePeriod
{
	CTime FirstDay;	// first day of excluded period
	CTime LastDay;   // last day of excluded period
} EXCLUDES;


typedef struct ValidityData
{
	char	Vato[15];			// char[14]: Valid From (CCS-DB-Format: yyyymmddHHMMSS) 
	char	Vafr[15];			// char[14]: Valid Until (CCS-DB-Format: yyyymmddHHMMSS) 
	char	Freq[8];			// char[7]: Frequency (valid = 1, inv.= 0) (1st day of week: Monday = 1)
	char	FixF[5];			// char[4]: From time (Fixed) [min since 00:00], e.g. '300' = 05:00 h
	char	FixT[5];			// char[4]: To time (Fixed) [min since 00:00], e.g. '600' = 10:00 h
	CCSPtrArray <EXCLUDES>	Excls;	// CCSPtrArray <EXCLUDES> 
	bool    IsFilled;			// bool: flag indicating the validity data has been set
	char    Vtyp[10];          // validity type: TIFA,TIFD,TIFAD
	bool    IsTurnaround;

	ValidityData(void)
	{
		// memset(this,'\0',sizeof(*this));
		IsFilled = false;
		IsTurnaround = false;
	}
} VALDATA;



class ValidityDlg : public CDialog
{
// Construction
public:
	ValidityDlg(CString opRueUrno, bool bpNew, CWnd* pParent = NULL);   // standard constructor
	~ValidityDlg();

// Dialog Data
	//{{AFX_DATA(ValidityDlg)
	enum { IDD = IDD_VALIDITY_DLG };
	CComboBox	m_ValidityType;
	CButton	m_Chb_Unlimited;
	CDateTimeCtrl	m_TimeToCtrl;
	CDateTimeCtrl	m_TimeFromCtrl;
	CDateTimeCtrl	m_FixTimeTo;
	CDateTimeCtrl	m_FixTimeFrom;
	CDateTimeCtrl	m_DateFromCtrl;
	CDateTimeCtrl	m_DateToCtrl;
	CButton	m_FixedBtn;
	CStatic	m_TimeStatic4;
	CStatic	m_TimeStatic3;
	CStatic	m_TimeStatic2;
	CStatic	m_TimeStatic;
	CButton	m_UntilFrame;
	CButton	m_FromFrame;
	CButton	m_DetailsBtn;
	CButton	m_WednesdayChb;
	CButton	m_TuesdayChb;
	CButton	m_ThursdayChb;
	CButton	m_SundayChb;
	CButton	m_SaturdayChb;
	CButton	m_MondayChb;
	CButton	m_FridayChb;
	CSliderCtrl	m_UntilSlider;
	CSliderCtrl	m_FromSlider;
	BOOL	m_bUnlimited;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ValidityDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
public:
	void GetValData(VALDATA &rrpValData);
	void SetValData(VALDATA *prpValData);
	void SetSliderEnabled(BOOL ipEnable);

protected:
	void SetVafrToCurrentTime();	
	int GetMinutesFromTime(CTime opTime);
	CTime GetTimeFromMinutes(int ipMinutes);
	void InitializeTable();
	void SetStaticTexts();	
	void SortExcludes();
	bool CheckExclIsValid(CTime olTestVal, int ipRow, int ipCol);

	// Generated message map functions
	//{{AFX_MSG(ValidityDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnInvert();
	afx_msg void OnDetails();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnStartEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDatefromBtn();
	afx_msg void OnDatetoBtn();
	afx_msg void OnFixedBtn();
	afx_msg void OnChangeFixedFrom(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeFixedTo(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUnlimitedChk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


// Attributes
	CWnd* pomParent;
	
	CGridFenster *pomGrid;
	
	CString omRueUrno;

	CRect omFromSliderRect;
	CRect omUntilSliderRect;

	VALDATA rmValData;
	
	int imExclCount;

	bool bmDetails;
	bool bmFixedTimes;
	bool bmIsLeftMousePressed;
	bool bmIsGridEnabled;
	bool bmNewFlag;

};


// statics
bool LoadValidity(CString &ropUval, VALDATA &ropValData);
bool SaveValidity(CString &ropUval, VALDATA &ropValData, bool bpNew);
CTimeSpan GetUrnoTimeTillExpiration(CString &ropVato);
CTimeSpan GetVatoTimeTillExpiration(CString &ropVato);
bool CheckForExpirationToWarn();
COleDateTime CTimeToCOleDateTime(CTime &opTime);
COleDateTime DBStringToOleDateTime(CString &opString);


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VALIDITYDLG_H__FC4E17E1_FD4B_11D2_A616_0000C007916B__INCLUDED_)
