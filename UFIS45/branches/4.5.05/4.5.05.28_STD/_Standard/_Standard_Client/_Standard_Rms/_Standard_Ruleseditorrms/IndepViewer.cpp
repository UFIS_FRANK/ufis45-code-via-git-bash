// IndepViewer.cpp: implementation of the CIndepViewer class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <regelwerk.h>
#include <IndepViewer.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIndepViewer::CIndepViewer(long Rkey)
	:RulesGanttViewer(Rkey)	
{
}

CIndepViewer::~CIndepViewer()
{

}

//---------------------------------------------------------------------------
//			filtering and sorting
//---------------------------------------------------------------------------
bool CIndepViewer::IsPassFilter(RecordSet *popRud)
{	
	// Exception handling should not be implemented in this function 
	// Frequently repeated exception handling code might slow down program execution

	//--- return value 
	bool blRet = false;

	//--- get data from CedaBasicData
	CString olUrue;
	CString olUdgr;
	olUrue = popRud->Values[igRudUrueIdx];
	olUdgr = popRud->Values[igRudUdgrIdx];

	//--- compare data with filter conditions
	if ( (olUrue == omRuleUrno) && (olUdgr == omUdgr) )
	{
		blRet = true;
	}

	return blRet;
}

void CIndepViewer::SetCurrentUdgr(CString opUdgr)
{
	omUdgr = opUdgr;
}