// ScrollToolBar.cpp: implementation of the CScrollToolBar class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <ScrollToolBar.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CScrollToolBar, CToolBar)
	//{{AFX_MSG_MAP(CScrollToolBar)
	ON_COMMAND(LEFT, OnLeft)
	ON_COMMAND(RIGHT, OnRight)
	ON_WM_SIZE()
	ON_WM_WINDOWPOSCHANGING()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScrollToolBar::CScrollToolBar()
{
	pomSizes = 0;	
	imButtonAnz = imFirstDisplayed = imLastDisplayed = 0;
	omTotalSize.cx = omTotalSize.cy = 0;
	pomParentRebar = 0;
	imID =0 ;
	bmPartiell=false;
	bmActOnWinPosChanging = true;
	imScrollBtnWidth=0;
}

CScrollToolBar::~CScrollToolBar()
{
	if ( pomSizes )
		delete pomSizes;
}

BOOL CScrollToolBar::AddToRebar ( CScrollRebar *popRebar, UINT idToolBar )
{
	//unsigned long ulStyle1;
	unsigned long ulStyle2;
	
	if (CreateEx(popRebar,TBSTYLE_FLAT|TBSTYLE_AUTOSIZE , WS_CHILD|WS_VISIBLE|CBRS_ALIGN_TOP,
					CRect(0, 0, 0, 0), idToolBar ) && LoadToolBar(idToolBar))
	{
		pomParentRebar = popRebar;
		imID = idToolBar;
		int ilTBStyle = TBSTYLE_FLAT | TBSTYLE_TRANSPARENT;
		int ilWStyle = WS_CHILD|WS_VISIBLE;
		if (omLeftBar.CreateEx(this, ilTBStyle, ilWStyle, CRect(0, 0, 0, 0), (UINT)idToolBar + LEFT))	
			omLeftBar.LoadToolBar(IDR_LEFT_BAR);
		if (omRightBar.CreateEx(this, ilTBStyle, ilWStyle, CRect(0, 0, 0, 0), (UINT)idToolBar + RIGHT))
			omRightBar.LoadToolBar(IDR_RIGHT_BAR);
		//ulStyle1 = RBBS_FIXEDBMP|RBBS_GRIPPERALWAYS;
		ulStyle2 = RBBS_NOGRIPPER|RBBS_FIXEDBMP;
		UINT ilBands = pomParentRebar->GetReBarCtrl().GetBandCount( ) ;
		popRebar->AddBar ( &omLeftBar, (LPCTSTR)0, 0, ulStyle2 );
		popRebar->AddBar ( this, (LPCTSTR)0, 0, ulStyle2  );
		popRebar->AddBar ( &omRightBar, (LPCTSTR)0, 0, ulStyle2 );

		CalculateSizes ();
		imFirstDisplayed = 0;
		imLastDisplayed = imButtonAnz-1;

		REBARBANDINFO slInfo;
		slInfo.cbSize = sizeof ( REBARBANDINFO );
		slInfo.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_STYLE | RBBIM_ID;
		slInfo.cx = slInfo.cxIdeal = omTotalSize.cx;
		slInfo.cxMinChild = 0;
		slInfo.cyMinChild = omTotalSize.cy;
		slInfo.wID = idToolBar;
		slInfo.fStyle = RBBS_NOGRIPPER;
		pomParentRebar->GetReBarCtrl().SetBandInfo ( ilBands+1, &slInfo );

		CRect olRect;
		omLeftBar.GetItemRect(0,&olRect);
	
		slInfo.fMask =  RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_ID | RBBIM_STYLE;
		pomParentRebar->GetReBarCtrl().GetBandInfo(ilBands,&slInfo);
		slInfo.cyMinChild = olRect.Height();
		slInfo.cxMinChild = slInfo.cx = slInfo.cxIdeal = olRect.Width();
		slInfo.fStyle |= RBBS_GRIPPERALWAYS;
		slInfo.wID = (UINT)idToolBar+LEFT;
		pomParentRebar->GetReBarCtrl().SetBandInfo(ilBands,&slInfo);
		imScrollBtnWidth = olRect.Width();
		slInfo.fStyle = RBBS_NOGRIPPER; //| RBBS_FIXEDSIZE;
		slInfo.wID = (UINT)idToolBar+RIGHT;
		pomParentRebar->GetReBarCtrl().SetBandInfo(ilBands+2,&slInfo);
		
		pomParentRebar->GetReBarCtrl().ShowBand(ilBands,FALSE);
		pomParentRebar->GetReBarCtrl().ShowBand(ilBands+2,FALSE);

	}
	return TRUE;
}

void CScrollToolBar::CalculateSizes ()
{
	imButtonAnz = GetToolBarCtrl().GetButtonCount( ) ;
	if ( pomSizes )
		delete pomSizes;
	pomSizes = new CSize[imButtonAnz];		
	CRect	olRect;		
	for ( int i=0; i<imButtonAnz; i++ )
	{
		GetItemRect( i, &olRect );
		pomSizes[i] = olRect.Size();
	}
	GetToolBarCtrl().GetMaxSize( &omTotalSize ) ;
	if ( pomParentRebar )
		pomParentRebar->OnChildSizeChanged ( this, omTotalSize.cx, omTotalSize.cy );
}

void CScrollToolBar::OnSize(UINT nType, int cx, int cy)
{
	CToolBar::OnSize(nType, cx, cy);
	if ( bmActOnWinPosChanging )
		OnSizeChange (cx, cy);
}

void CScrollToolBar::OnLeft()
{
	bool blPfeilEntfaellt = false;
	if (imFirstDisplayed == 1)
	{
		blPfeilEntfaellt = true;
	}

	imLastDisplayed = min ( imButtonAnz-1,imLastDisplayed-1 );
	imFirstDisplayed = max ( 0, imFirstDisplayed-1 );
	if ( blPfeilEntfaellt )
	{
		CRect olRect;
		int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
		pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );

		if ( CalcShownButtons ( olRect.Width(), imFirstDisplayed, imLastDisplayed,
								bmPartiell ) )
			Display();
	}
	else
		Display();
}

void CScrollToolBar::OnRight()
{
	bmPartiell = bmPartiell && (imLastDisplayed<imButtonAnz-1);
	imLastDisplayed = min ( imButtonAnz-1,imLastDisplayed+1 );
	imFirstDisplayed = max ( 0, imFirstDisplayed+1 );
	Display();
	/*
	CRect olRect;
	int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
	pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );
	if ( CalcShownButtons ( olRect.Width(), imFirstDisplayed, imLastDisplayed,
							bmPartiell ) )
		Display();*/
}


void CScrollToolBar::OnWindowPosChanging( WINDOWPOS* lpwndpos )
{
	CToolBar::OnWindowPosChanging( lpwndpos );
	if ( ! (lpwndpos->flags & SWP_NOSIZE ) )
	{
		if ( bmActOnWinPosChanging )
		{
			OnSizeChange (lpwndpos->cx, lpwndpos->cy);
		}
	}

}

int CScrollToolBar::GetUsedWidth ( int ipStart, int ipLast )
{
	int ilRet=0;
	ipStart = max ( ipStart, 0 );
	ipLast = min ( ipLast, imButtonAnz-1 );

	if ( !pomSizes )
		CalculateSizes();
	if ( ipStart==0 && ipLast==imButtonAnz-1 )
	{
//		ilRet = omTotalSize.cx - 4;
		ilRet = omTotalSize.cx - pomSizes[0].cx/3-4;
		return ilRet;
	}
	for ( int i=ipStart; (pomSizes)&&(i<= ipLast); i++ )
		ilRet += pomSizes[i].cx;
	if ( ilRet >0 )
		ilRet -= pomSizes[0].cx/3;
	return ilRet;
}

bool CScrollToolBar::CalcShownButtons ( int ipWidth, int &ipStart, int &ipEnd, 
									    bool &bpPartiell )
{
	int cx, cxNeu;
	int ilEndNeu;

	ipStart = max ( ipStart, 0 );
	ipEnd = min ( ipEnd, imButtonAnz-1 );

	bpPartiell = false;
	cx = GetUsedWidth ( ipStart, ipEnd );

	if ( cx == ipWidth )
		return true;	//  Buttons passen genau
	//  wir haben noch Platz �brig
	if ( cx < ipWidth )		
	{
		if ( ipEnd-ipStart>=imButtonAnz-1 )	//ben�tigte Gr��e kleiner vorhandener Platz,
			return true;					//aber alle Buttons bereits sichtbar
		if ( ipEnd<imButtonAnz-1 )
			ipEnd++;
		else
			ipStart--;
		cxNeu = GetUsedWidth ( ipStart, ipEnd );
		if ( cxNeu <= ipWidth )		//  ein Button mehr pa�t auch noch -> Rekursion
		{
			return CalcShownButtons ( ipWidth, ipStart, ipEnd, bpPartiell ); 
		}
		else
		{
			bpPartiell = ( ipWidth-cx-imScrollBtnWidth < pomSizes[ipEnd].cx/2 );
			return true;
		}
	}
	else
	{		//  ben�tigte Gr��e gr��er als vorhandener Platz
		if ( ipEnd>ipStart )	//  mehr als ein Button dargestellt
		{	
			ilEndNeu = ipEnd-1;
			cxNeu = GetUsedWidth ( ipStart, ilEndNeu );
			if ( cxNeu > ipWidth )		//  ein Button weiniger pa�t immer noch nicht -> Rekursion
				return CalcShownButtons ( ipWidth, ipStart, --ipEnd, bpPartiell );
			else //  ein Knopf weniger w�re zu klein, also letzten nur teilweise darstellen
			{
				//bpPartiell = ( cx-ipWidth < pomSizes[ipEnd].cx*2/3 );	//  rechter Button nicht zu 2/3 sichtbar
				bpPartiell = ( ipWidth-cxNeu-imScrollBtnWidth < pomSizes[ipEnd].cx/2 );
				return true;
			}
		}
		else
		{
			bpPartiell=true;
			return false;
		}
	}
	
}
/*
bool CScrollToolBar::CalcShownButtons ( int ipWidth, int &ipStart, int &ipEnd, 
									    bool &bpPartiell )
{
	int cx;
	
	ipStart = max ( ipStart, 0 );
	ipEnd = min ( ipEnd, imButtonAnz-1 );

	bpPartiell = false;
	cx = GetUsedWidth ( ipStart, ipEnd );

	if ( cx == ipWidth )
		return true;	//  Buttons passen genau
	if ( cx<ipWidth ) 
	{
		if ( ipEnd-ipStart<imButtonAnz-1 )
		{
			//  ben�tigte Gr��e kleiner vorhandener Platz und nicht alle Buttons sichtbar
			if ( ipEnd<imButtonAnz-1 )
				ipEnd++;
			else
				ipStart--;
			return CalcShownButtons ( ipWidth, ipStart, ipEnd, bpPartiell );
		}
		else //  ben�tigte Gr��e kleiner vorhandener Platz aber alle Buttons bereits sichtbar
			return true;
	}
	//  ben�tigte Gr��e gr��er vorhandener Platz
	if ( ipEnd>ipStart )
	{	//  mehr als ein Button dargestellt
		if ( pomSizes && ( cx-pomSizes[ipEnd].cx >= ipWidth ) )
			return CalcShownButtons ( ipWidth, ipStart, --ipEnd, bpPartiell );
		else //  ein Knopf weniger w�re zu klein, also letzten nur teilweise darstellen
		{
			int uebrig =  cx-pomSizes[ipEnd].cx - ipWidth ;
			if ( uebrig < pomSizes[ipEnd].cx *2/3)
				bpPartiell = true;
			return true;
		}
	}
	else
		return false;
}
*/

void CScrollToolBar::Display()
{
	bmActOnWinPosChanging = false;
	int idx, idxL, idxR;
	int ilFirst= imFirstDisplayed;
	int ilLast = imLastDisplayed;
	bool blLastPartiell = bmPartiell;
	idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
	idxL = pomParentRebar->GetReBarCtrl().IDToIndex( imID+LEFT ); 
	idxR = pomParentRebar->GetReBarCtrl().IDToIndex( imID+RIGHT ); 
	for ( int i=0; i<imButtonAnz; i++ )
	{
		UINT ilID = GetItemID( i ) ;
		if ( ilID )
			GetToolBarCtrl().HideButton( ilID, (i<ilFirst)||(i>ilLast) );
	}
	pomParentRebar->GetReBarCtrl().ShowBand(idxL,(ilFirst> 0));
	pomParentRebar->GetReBarCtrl().ShowBand(idxR, (ilLast < imButtonAnz-1) || blLastPartiell );
	bmActOnWinPosChanging =true;
	
}

void CScrollToolBar::OnSizeChange (int cx, int cy)
{
	bool blOk;
	static int ilLastCx = -1;

	if ( !pomSizes )
		CalculateSizes();
//	int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
//	if ( idx >= 0 )
//	{
//		CRect olRect;
//		pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );
//		cx = olRect.Width();
//	}

	if ( ( cx <= 0) || !pomSizes || (cx==ilLastCx) ) 
		return;
	ilLastCx = cx;
	blOk = CalcShownButtons ( cx, imFirstDisplayed, imLastDisplayed, bmPartiell ) ;
	if ( blOk )
		Display();
}

void CScrollToolBar::DeleteStringMap ()
{
	if(m_pStringMap)
	{
		delete m_pStringMap;
		m_pStringMap = NULL;
	}
}


CSize CScrollToolBar::CalcDynamicLayout( int nLength, DWORD dwMode )
{
	CSize olSize = CToolBar::CalcDynamicLayout( nLength, dwMode );
	CalculateSizes ();
	return olSize ;
}

CSize CScrollToolBar::CalcFixedLayout(BOOL bStretch, BOOL bHorz)
{
	CSize olSize = CToolBar::CalcFixedLayout( bStretch, bHorz );
	CalculateSizes ();
	return olSize ;
}

void CScrollToolBar::SetSizes(SIZE sizeButton, SIZE sizeImage)
{
	CToolBar::SetSizes(sizeButton, sizeImage);
	CalcDynamicLayout(0, LM_HORZ | LM_HORZDOCK | LM_COMMIT);
	pomParentRebar->UpdateWindow();
}


CLRToolBar::CLRToolBar()
{
}

BEGIN_MESSAGE_MAP(CLRToolBar, CToolBar)
	ON_WM_LBUTTONDOWN()
	ON_WM_GETMINMAXINFO()
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()

void CLRToolBar::OnLButtonDown( UINT nFlags, CPoint point )
{
	if (m_pDockBar != NULL && OnToolHitTest(point, NULL) == -1)
	{
		CToolBar::OnLButtonDown( nFlags, point );
	}
	else
	{
		CWnd::OnLButtonDown(nFlags, point);
	}
	
}


void CLRToolBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	BOOL		blEnable;
	TBBUTTON	olButton;
	blEnable = m_hWnd && IsWindowVisible() ;
	if ( GetToolBarCtrl( ).GetButton ( 0, &olButton ) )
	{
		GetToolBarCtrl().EnableButton(olButton.idCommand);
	}
}

void CLRToolBar::OnWindowPosChanging( WINDOWPOS* lpwndpos )
{
	RECT slRect;
	GetItemRect( 0, &slRect );
	if ( lpwndpos->cx > slRect.right - slRect.left )
		lpwndpos->flags|=SWP_NOSIZE;
	CToolBar::OnWindowPosChanging ( lpwndpos );
}

void CLRToolBar::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	RECT slRect;
	CToolBar::OnGetMinMaxInfo(lpMMI);
	GetItemRect( 0, &slRect );
    lpMMI->ptMaxSize.x = 
    lpMMI->ptMinTrackSize.x =
    lpMMI->ptMaxTrackSize.x = slRect.right - slRect.left;
}
	

int CLRToolBar::GetWidth()
{
	CRect olRect;
	GetItemRect( 0, &olRect );
	return olRect.Width();
}



BEGIN_MESSAGE_MAP(CScrollRebar, CReBar)
	//{{AFX_MSG_MAP(CScrollToolBar)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CScrollRebar::CScrollRebar()
{

}

CScrollRebar::~CScrollRebar()
{
	CObject	*polObj;
	int i;
	for ( i=0; i<omItems.GetSize(); i++ )
		if ( polObj = omItems[i] )
			delete polObj;
}


void CScrollRebar::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	CReBar::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxSize.y = 
    lpMMI->ptMaxTrackSize.y = GetReBarCtrl().GetRowHeight(0);
}

CScrollToolBar* CScrollRebar::AddScrollToolBar ( UINT idToolBar )
{
	if ( idToolBar >= LEFT )
	{
		TRACE ( "ID eines Scrollbaren Toolbars mu� < 16384 sein.\n" );
		return 0;
	}
	CScrollToolBar *polBar;
	polBar = new CScrollToolBar;
	if ( polBar && polBar->AddToRebar ( this, idToolBar ) )
	{
		omItems.Add ( polBar );
		return polBar;
	}
	if ( polBar )
		delete polBar;
	return 0;
}
	

BOOL CScrollRebar::Create(CWnd* pParentWnd, DWORD dwCtrlStyle/* = RBS_FIXEDORDER*/,
						  DWORD dwStyle /*= WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOP*/,
						  UINT nID /*= AFX_IDW_REBAR*/ )
{
	return CReBar::Create(pParentWnd, dwCtrlStyle, dwStyle, nID );
}
		
CButton* CScrollRebar::AddButton ( LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, 
								   CWnd* pParentWnd, UINT nID  )
{
	CButton *polButton;
	polButton = new CButton;
	if ( polButton && polButton->Create ( lpszCaption, dwStyle, rect, pParentWnd, nID ) && 
		 AddBar ( polButton, 0, 0, RBBS_NOGRIPPER|RBBS_FIXEDBMP ) )
	{
		omItems.Add ( polButton );
		return polButton;
	}
	if ( polButton )
		delete polButton;
	return 0;
}
	
CComboBox* CScrollRebar::AddComboBox ( DWORD dwStyle, const RECT& rect, 
									   CWnd* pParentWnd, UINT nID  )
{
	CComboBox *polCb = new CComboBoxEx;
	if (polCb && polCb->Create(dwStyle, rect, pParentWnd, nID) && 
		 AddBar(polCb, 0, 0, RBBS_NOGRIPPER|RBBS_FIXEDBMP))
	{
		omItems.Add(polCb);
		return polCb;
	}
	
	if (polCb)
		delete polCb;

	return 0;
}

bool CScrollRebar::OnChildSizeChanged ( CWnd * pWnd, int cx, int cy )
{
	if ( !pWnd || !pWnd->m_hWnd )
		return false;
	bool blRet=false;
	REBARBANDINFO slInfo;

	memset ( &slInfo, 0, sizeof(slInfo) ) ;
	slInfo.cbSize = sizeof(slInfo);
	
	CReBarCtrl &rolCtrl = GetReBarCtrl();
	UINT ilBands = rolCtrl.GetBandCount();

	slInfo.fMask =  RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_CHILD ;
	for ( UINT i=0; !blRet&&(i<ilBands); i++ )
	{
		if ( rolCtrl.GetBandInfo(i, &slInfo) && 
			(slInfo.hwndChild==pWnd->m_hWnd ) )
		{
			slInfo.cyMinChild = cy;
			slInfo.cx = slInfo.cxIdeal = cx;
			if ( rolCtrl.SetBandInfo(i,&slInfo) )
				blRet = true;
			else
				break;
		}
	}
	CRect olRect;
	GetClientRect ( &olRect );
	rolCtrl.SizeToRect ( olRect );
	return blRet;
}

int CScrollRebar::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	ASSERT_VALID(this);
	ASSERT(::IsWindow(m_hWnd));

	HWND hWndChild = ::ChildWindowFromPoint(m_hWnd, point);
	CWnd* pWnd = CWnd::FromHandlePermanent(hWndChild);
	if ( (pWnd == NULL) || (hWndChild==m_hWnd ) )
		return CControlBar::OnToolHitTest(point, pTI);

	ASSERT(pWnd->m_hWnd == hWndChild);
	MapWindowPoints( pWnd, &point, 1 ) ;
	return pWnd->OnToolHitTest(point, pTI);
}