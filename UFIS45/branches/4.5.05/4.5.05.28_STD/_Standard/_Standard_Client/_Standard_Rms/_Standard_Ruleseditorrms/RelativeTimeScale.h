// RelativeTimeScale.h : header file
//

#ifndef _RelativeTimeScale_
#define _RelativeTimeScale_

#include <CCSDefines.h>
#include <ccsptrarray.h>
#include <CCSTime.h>

/////////////////////////////////////////////////////////////////////////////
// CRelativeTopScaleIndicator class


/////////////////////////////////////////////////////////////////////////////
// Class declaration of CRelativeTopScaleIndicator

class CRelativeTopScaleIndicator
{
public:
    COLORREF lmColor;
    CTime omStart, omEnd;
};

/////////////////////////////////////////////////////////////////////////////
// RelativeTimeScale window

////////////////////////////////////////////////////////////////////////
// RST 15/07/97:
//	
//@Memo: WM_TSCALE_MARKERMOVED
#define WM_TSCALE_MARKERMOVED        (WM_USER + 510)	// lParam is the MarkerID
														// wParam is the new Time
//@Memo: WM_TSCALE_MARKERSELECT
#define WM_TSCALE_MARKERSELECT       (WM_USER + 511)	// lParam is the selected MarkerID or NULL
														// wParam is the Time

/////////////////////////////////////////////////////////////////////////////
// Class declaration of RelativeTimeScale

//@Man:
//@Memo: Baseclass
/*@Doc:
  No comment on this up to now.
*/



class RelativeTimeScale : public CWnd
{
// Construction
public:
    //@ManMemo: Default constructor
    RelativeTimeScale(CWnd *popParent = NULL);


// Operations
public:
    void SetDisplayStartTime(CTime opDisplayStart);
    void SetTimeInterval(CTimeSpan opInterval);
    void SetDisplayTimeFrame(CTime opDisplayStart, CTimeSpan opDuration, CTimeSpan opInterval);
	void SetReferenceTimes(CTime opInb, CTime opOutb);
	void SetReferenceTexts(CString opInb, CString opOutb);
    CTimeSpan GetDisplayDuration(void);
    CTime GetDisplayStartTime(void);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
    CTime GetTimeFromX(int ipX);
////////////////////////////////////////////////////////////////////////
    int GetXFromTime(CTime opTime);

    void UpdateCurrentTimeLine(void);
	void UpdateCurrentTimeLine(CTime opTime);
    
    void AddTopScaleIndicator(CRelativeTopScaleIndicator *popTSI) { omTSIArray.Add(popTSI); };
    void AddTopScaleIndicator(CTime opStartTime, CTime opEndTime, COLORREF lpColor);
    void DisplayTopScaleIndicator(CDC *popDC);
    void DisplayTopScaleIndicator(void);
    void RemoveAllTopScaleIndicator(void);
	void EnableDisplayCurrentTime(bool bpDisplayCurrentTime);

////////////////////////////////////////////////////////////////////////
// RST 07/15/97:
// This block of code is used to add and change Marker in the TimeScale 
//	
    //@ManMemo: Marker
    /*@ManDoc:
	  Marker: a colored triangle which 'marks' a time or a range 
	  of time in the timescale.
	  This block of code is used to add 
	  and change Marker in the TimeScale.
    */

    // enum Markeralignment: TSM_UP, TSM_LEFT, TSM_RIGHT
	enum {TSM_UP, TSM_LEFT, TSM_RIGHT};

	void AddMarker(int ipID, CTime opTime, 
				   COLORREF opColor = RGB(255,0,0), 
				   int ipRasterWidth = 15, 
				   int ipAlign = TSM_UP,
				   int ipIDAssign = 0,
				   CTime opRangeFrom = TIMENULL, 
				   CTime opRangeTo = TIMENULL,
				   CString opToolTipText = "");
    
	void DeleteMarker(int ipID);
	void DeleteAllMarker();
    bool ChangeMarkerRange(int ipID, CTime opNewTime, CTime opRangeFrom, CTime opRangeTo);
    bool ChangeMarkerColor(int ipID, COLORREF opColor);
    bool ChangeMarkerPos(int ipID, CTime opTime);
	void SetMarkerRaster(int ipID, int ipRasterWidth);

	bool Near(CTime opTime1, CTime opTime2);
	void SetRuleType(int ipType);
    
// Implementation
public:
    virtual ~RelativeTimeScale();
protected:

protected:
    // Generated message map functions
    //{{AFX_MSG(RelativeTimeScale)
    afx_msg void OnPaint();
    afx_msg bool OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
	//afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    
    bool TSIPos(int *ipLeft, int *ipRight);

protected:
	
////////////////////////////////////////////////////////////////////////
// RST 15/07/97:
// This block of code includes the member to handle the Marker in the TimeScale 
//	see also the new windowmessages at the top of the headerfile
	struct REL_MARKER
	{
		int ID;
		CTime ActTime;
		CTime RangeFrom;
		CTime RangeTo;
		int Assign;
		int Align;
		COLORREF Color;
		int	  RasterWidth;
		CString ToolTipText;
		REL_MARKER(){;};
	};
    CCSPtrArray<REL_MARKER> omMarker;
	REL_MARKER *prmCurrMarker;
	bool bmLButtonDown;
	CStatic *pomStaticToolTip;
	bool TestMarkerAssignPos(REL_MARKER *prpMarker, CTime opNewTime);
	CWnd *pomParent;
////////////////////////////////////////////////////////////////////////
	
	CTime omOnBlock;
	CTime omOfBlock;

    CTime omDisplayStart;
    CTimeSpan omInterval;
	
    double fmIntervalWidth;
	    
    int imP0;
    int imP1;
    int imP2;
    int imP3;
	
	int imRuleType; // 0 = turnaround, 1 = inbound, 2 = outbound

    CTime omOldCurrentTime;
	CTime omCurrentTime;
    CPtrArray omTSIArray;
	bool bmDisplayCurrentTime;
	CString omInboundText;
	CString omOutboundText;

public:
    CFont *pomFont;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _RelativeTimeScale_

