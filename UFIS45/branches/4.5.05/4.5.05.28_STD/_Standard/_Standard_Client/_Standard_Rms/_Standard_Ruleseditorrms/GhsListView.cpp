// GhsListView.cpp : implementation file
//

#include <stdafx.h>
#include <RegelWerk.h>
#include <GhsListView.h>
#include <stdlib.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//-------------------------------------------------------------------------------------------------------
//				static functions
//-------------------------------------------------------------------------------------------------------
static int MyCompare(const void *pipItem1,const void *pipItem2);


//-------------------------------------------------------------------------------------------------------
//				construction, destruction
//-------------------------------------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(GhsListView, CPropertyPage)
//-------------------------------------------------------------------------------------------------------

GhsListView::GhsListView() : CPropertyPage(GhsListView::IDD)
{
	//-- exception handling
	CCS_TRY

	//{{AFX_DATA_INIT(GhsListView)
	//}}AFX_DATA_INIT
	/*
	CString olText;
	if ( GetString("STID", "1630", olText ) || !bgUseResourceStrings ) 
		m_strCaption = olText;
	*/
	m_strCaption = GetString(IDS_VIEW_SERVICES);
	//-- exception handling
	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

GhsListView::~GhsListView()
{
	//-- exception handling
	CCS_TRY

	omSerData.DeleteAll();

	//-- exception handling
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//				data exchange
//-------------------------------------------------------------------------------------------------------
void GhsListView::DoDataExchange(CDataExchange* pDX)
{
	//-- exception handling
	CCS_TRY

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GhsListView)
	DDX_Control(pDX, IDC_REMOVE, m_Remove);
	DDX_Control(pDX, IDC_ADD, m_Add);
	DDX_Control(pDX, IDC_VRGCLST, m_VrgcLst);
	DDX_Control(pDX, IDC_FILTER, m_VrgcFilter);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData(omValues);
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData(omValues);
	}

	//-- exception handling
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					message map
//-------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(GhsListView, CPropertyPage)
	//{{AFX_MSG_MAP(GhsListView)
	ON_BN_CLICKED(IDC_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-------------------------------------------------------------------------------------------------------
//					message handling
//-------------------------------------------------------------------------------------------------------

BOOL GhsListView::OnInitDialog() 
{
	BOOL ilRet = FALSE;

	//-- exception handling
	CCS_TRY

	CPropertyPage::OnInitDialog();
	ilRet = OnSetActive();

	//-- exception handling
	CCS_CATCH_ALL

	return ilRet;
}
//-------------------------------------------------------------------------------------------------------


void GhsListView::OnButtonAdd() 
{
	//-- exception handling
	CCS_TRY

	// This function moves SER lines from the left window of SERVIEW to the right one
	int ilMaxItems = m_VrgcLst.GetSelCount();
	int ilLen = 0;
	if(ilMaxItems>0)
	{
		int *pilItems = new int [ilMaxItems];
		m_VrgcLst.GetSelItems(ilMaxItems, pilItems);
		qsort((void *)pilItems, (size_t)ilMaxItems, sizeof(int), MyCompare);
		for(int ilIndex = 0, ilLen = 0; ilIndex < ilMaxItems; ilIndex++)
		{
			int ilTest = pilItems[ilIndex];
			long llItemUrno = m_VrgcLst.GetItemData(pilItems[ilIndex]);

			CString olUrno;
			olUrno.Format("%d", llItemUrno);
			CString olSnam = ogBCD.GetField("SER", "URNO", olUrno, "SNAM");

			if(olSnam != "")
			{
				int ilFilterIndex = m_VrgcFilter.AddString(olSnam);
				if(ilFilterIndex >= 0)
				{
					m_VrgcFilter.SetItemData(ilFilterIndex, llItemUrno);
				}
			}
		}
		for(ilIndex = ilMaxItems;--ilIndex>=0;)
		{
			m_VrgcLst.DeleteString(pilItems[ilIndex]);
		}
		delete pilItems;
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

void GhsListView::OnButtonRemove() 
{
	//-- exception handling
	CCS_TRY

	// This function removes SERlines from the right window of SERVIEW and places them in the left box again
	int ilMaxItems = m_VrgcFilter.GetSelCount();
	if(ilMaxItems > 0)
	{
		int *pilItems = new int [ilMaxItems];
		m_VrgcFilter.GetSelItems(ilMaxItems, pilItems);
		qsort((void *)pilItems, (size_t)ilMaxItems, sizeof(int), MyCompare);
		for(int ilIndex = 0, ilLen = 0; ilIndex < ilMaxItems; ilIndex++)
		{
			int ilTest = pilItems[ilIndex];
			long llItemUrno = m_VrgcFilter.GetItemData(pilItems[ilIndex]);

			CString olUrno;
			olUrno.Format("%d", llItemUrno);
			CString olSnam = ogBCD.GetField("SER", "URNO", olUrno, "SNAM");

			if(olSnam != "")
			{
				int ilLstIndex = m_VrgcLst.AddString(olSnam);
				if(ilLstIndex >= 0)
				{
					m_VrgcLst.SetItemData(ilLstIndex, llItemUrno);
				}
			}
		}
		for (ilIndex = ilMaxItems - 1; ilIndex >= 0; ilIndex--)
		{
			m_VrgcFilter.DeleteString(pilItems[ilIndex]);
		}
		delete pilItems;
	}

	//-- exception handling
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					set methods
//-------------------------------------------------------------------------------------------------------
void GhsListView::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}
//-------------------------------------------------------------------------------------------------------

void GhsListView::SetData(CStringArray &ropValues)
{
	//-- exception handling
	CCS_TRY

	int ilIndex = 0, ilCount, i ;
	int ilFlag;
	CString olUrno = "";
	CString olFilter = "";
	CString olStrg;
	CStringArray	olTmpStr;
	CStringList		olSelectedUrnos;
	m_VrgcLst.ResetContent();
	m_VrgcFilter.ResetContent();
	
	//  f�lle listbox der ausgew�hlten
	for ( i=0; i<ropValues.GetSize(); i++ )
	{
		olFilter = "";
		olStrg = ropValues[i];
		ilFlag = ropValues[i].Find('|');
		if(ilFlag != -1)
		{
			olFilter = ropValues[i].Mid(0,ilFlag);
		}

		if(olFilter.IsEmpty() != TRUE)
		{
			ilCount = ExtractItemList(olFilter, &olTmpStr, ' ');
			for(int ilLC = ilCount - 1; ilLC >= 0; ilLC--)
			{
				CString olFilterUrno = olTmpStr[ilLC];
				CString olSnam = ogBCD.GetField("SER", "URNO", olFilterUrno, "SNAM");
				
				if(olSnam != "")
				{
					ilIndex = m_VrgcFilter.AddString(olSnam);
					if(ilIndex >= 0)
					{
						m_VrgcFilter.SetItemData(ilIndex, atol(olFilterUrno));
						olSelectedUrnos.AddTail ( olFilterUrno );
					}
				}
			}
		}				
	}
	//  F�lle linke List-Box 
	ilCount = ogBCD.GetDataCount("SER");
	RecordSet olRecord;
	for( i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("SER", i, olRecord);
		olUrno = olRecord.Values[igSerUrnoIdx] ;
		if ( olSelectedUrnos.Find( olUrno ) )
			continue;	//  Datensatz ist bereits in rechter Listbox
		ilIndex = m_VrgcLst.AddString(olRecord.Values[igSerSnamIdx]);
		if(ilIndex >= 0)
		{
			m_VrgcLst.SetItemData(ilIndex, atol(olUrno));
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					get methods
//-------------------------------------------------------------------------------------------------------
BOOL GhsListView::GetData(CStringArray &ropValues)
{
	BOOL ilRet = TRUE;

	//-- exception handling
	CCS_TRY

	CString olTmp, olUrnos="";
	ropValues.RemoveAll();
	ropValues.SetSize(3);
	int ilSize = m_VrgcLst.GetCount();
	long llUrno = 0L;
	
	ropValues.SetAt(0,"|");
	for(int i = 0; i < m_VrgcFilter.GetCount(); i++)
	{
		olTmp.Format(" %ld", m_VrgcFilter.GetItemData(i));
		olUrnos += olTmp;
	}
	olUrnos += "|";
	ropValues.SetAt(1, olUrnos);
	return ilRet;

	//-- exception handling
	CCS_CATCH_ALL

	return ilRet;
}



//-------------------------------------------------------------------------------------------------------
//					implementation of static functions
//-------------------------------------------------------------------------------------------------------

static int MyCompare(const void *pipItem1,const void *pipItem2)
{
	int ilItem1 = *(int *)pipItem1;
	int ilItem2 = *(int *)pipItem2;
	
	if(ilItem1>ilItem2)
	{
		return 1;
	}
	else if(ilItem1<ilItem2)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}
//-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------
