#if !defined(AFX_NEWDLG_H__50E76F83_0BCA_11D5_8015_00D0B7E16259__INCLUDED_)
#define AFX_NEWDLG_H__50E76F83_0BCA_11D5_8015_00D0B7E16259__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// NewDlg dialog

class NewDlg : public CDialog
{
// Construction
public:
	NewDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(NewDlg)
	enum { IDD = IDD_NEW_DLG };
	CButton	m_Opt_SingleRule;
	CButton	m_OKBtn;
	int		m_WhatNew;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(NewDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWDLG_H__50E76F83_0BCA_11D5_8015_00D0B7E16259__INCLUDED_)
