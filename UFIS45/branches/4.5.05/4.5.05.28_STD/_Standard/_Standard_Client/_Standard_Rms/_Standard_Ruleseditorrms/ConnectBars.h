#if !defined(AFX_CONNECTBARS_H__B85B3EF3_6FFC_11D3_A674_0000C007916B__INCLUDED_)
#define AFX_CONNECTBARS_H__B85B3EF3_6FFC_11D3_A674_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConnectBars.h : header file
//


#include <RecordSet.h>
#include <RulesGanttViewer.h>


class ConnectBars : public CDialog
{
// Construction
public:
	ConnectBars(RulesGanttViewer *pViewer = NULL, CWnd* pParent = NULL);   // standard constructor
	~ConnectBars();

// implementation

	void Create();
	void Destroy();

	
	// opSource: Urno of source bar (start of connection)
	// opTarget: Urno of target bar (target of connection)
	// ipType:   0 = SF, 1 = SS, 2 = FS, 3 = FF
	void InitData(CString opSource, CString opTarget, int ipType);
	void MoveConnectedBars(int ipOffset, RecordSet *popSource, RecordSet *popTarget);
	void WriteData(int ilSecs, RecordSet *popTarget, RecordSet *popSource);
	void SetStaticTexts();
	bool IsCyclicConnection(RecordSet *popSource, RecordSet *popTarget);

	
// attributes

	CWnd *pomParent;
	RulesGanttViewer *pomGanttViewer;

	RecordSet omRudRecord1;
	RecordSet omRudRecord2;
	
	bool bmIsItemChosen;
	bool bmIsUpdate;

	//{{AFX_DATA(ConnectBars)
	enum { IDD = IDD_CONNECT_BARS };
	CButton	m_Chb_SameRes;
	CEdit	m_Edt_Offset;
	CComboBox	m_ConnectionTypes;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ConnectBars)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// message map
	// Generated message map functions
	//{{AFX_MSG(ConnectBars)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSameResourceChb();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNECTBARS_H__B85B3EF3_6FFC_11D3_A674_0000C007916B__INCLUDED_)
