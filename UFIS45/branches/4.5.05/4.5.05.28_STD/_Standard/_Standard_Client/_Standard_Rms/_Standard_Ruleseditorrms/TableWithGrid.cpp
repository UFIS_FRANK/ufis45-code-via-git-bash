// TableWithGrid.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <basicdata.h>
#include <TableWithGrid.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableWithGrid

CTableWithGrid::CTableWithGrid()
{
	//-- exception handling
	CCS_TRY

    pomGrid = new CTableGrid(this);
    
    // indicates no initialization yet
    bmInited = FALSE;

	//imSelectMode = GX_SELMULTIPLE | GX_SELROW |GX_SELSHIFT|GX_SELCELL ;
	imSelectMode = GX_SELFULL ;

	pomParentWindow		=	NULL;
	imXStart			=	-1;	
	imYStart			=	-1;
	imXEnd				=	-1;
	imYEnd				=	-1;
	lmTextColor			=	0;
	lmTextBkColor		=	0;
	lmHighlightColor	=	0;
	lmHighlightBkColor	=	0;
	bmCalculateNormal	=	false;

	//-- exception handling
	CCS_CATCH_ALL
}

CTableWithGrid::~CTableWithGrid()
{
		//-- exception handling
	CCS_TRY

	
	if ( pomGrid )
	{
		pomGrid->DestroyWindow();
		delete pomGrid;
	}

	DestroyWindow();

	//-- exception handling
	CCS_CATCH_ALL

}


BEGIN_MESSAGE_MAP(CTableWithGrid, CWnd)
	//{{AFX_MSG_MAP(CTableWithGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CTableWithGrid message handlers

bool CTableWithGrid::DisplayTable()
{
	//-- exception handling
	CCS_TRY

    if (!bmInited)  // uninitialized?
    {
        //RC(false);
        return false;
    }

    if (m_hWnd == NULL) // window was not created yet?
    {
        // create the table window
        //DWORD lWndStyle = WS_CHILD | WS_BORDER | WS_VISIBLE;
		DWORD lWndStyle = WS_CHILD | WS_VISIBLE;
		CRect rectWnd;
		if (bmCalculateNormal == false)
		{
			rectWnd = CRect(imXStart, imYStart, imXEnd - imXStart + 1, imYEnd - imYStart + 1);
		}
		else
		{
			rectWnd = CRect(imXStart, imYStart, imXEnd + 1, imYEnd + 1);
		}

        if (Create(NULL, NULL, lWndStyle, rectWnd, pomParentWindow, 1) == 0)
        {
            //RC(false);
            return false;
        }

        // let the list box know which window it must send a message to
        pomGrid->pomTableWindow = this;

        CRect rect;

        GetClientRect(&rect);
		rect.OffsetRect(-2, -2);
		lWndStyle |= WS_DLGFRAME | WS_VSCROLL | WS_HSCROLL;
        if (pomGrid->Create(lWndStyle, rect, this, 1) == 0)
        {
            //RC(false);
            return false;
        }

        // Fix Windows 3.x's bug on list box co-ordinate calculation.
        // Windows 3.x will inflate window position given when created by -1,-1.
        // However, this problem could be fixed by artfully moving window just one time.
        //
		rect.bottom--, rect.right--;
        pomGrid->MoveWindow(&rect, FALSE);
		SetSelectMode(imSelectMode);

  }
	ShowWindow ( SW_SHOW );
	//-- exception handling
	CCS_CATCH_ALL

    return true;
}

bool CTableWithGrid::SetTableData(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor/* = ::GetSysColor(COLOR_WINDOWTEXT)*/,
        COLORREF lpTextBkColor/* = ::GetSysColor(COLOR_WINDOW)*/,
        COLORREF lpHighlightColor/* = ::GetSysColor(COLOR_HIGHLIGHTTEXT)*/,
        COLORREF lpHighlightBkColor/* = ::GetSysColor(COLOR_HIGHLIGHT)*/,
        CFont *popTextFont/* = NULL*/,
        CFont *popHeaderFont/* = NULL*/,
		bool bpCalculateNormal/* = false*/
    )
{
	//-- exception handling
	CCS_TRY
	
	bmCalculateNormal = bpCalculateNormal;

    // destroy the window if parent window changed
    if (popParentWindow != pomParentWindow)
        DestroyWindow();    // this works even if m_hWnd == NULL.

    // initialize all data members
    pomParentWindow = popParentWindow;
    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);
    lmTextColor = lpTextColor;
    lmTextBkColor = lpTextBkColor;
    lmHighlightColor = lpHighlightColor;
    lmHighlightBkColor = lpHighlightBkColor;

    // indicate that window information is initialized
    bmInited = TRUE;

    SetPosition(imXStart, imXEnd, imYStart, imYEnd);
	
	//-- exception handling
	CCS_CATCH_ALL

    return true;
}


bool CTableWithGrid::SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd)
{
	//-- exception handling
	CCS_TRY

    BOOL blRepaint = TRUE;

    if (abs(ipXEnd-ipXStart+1) == imXEnd-imXStart+1 &&
        abs(ipYEnd-ipYStart+1) == imYEnd-imYStart+1)
        blRepaint = FALSE;

    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);

    // recalculate object positions
    //if (blRepaint)
    //    CalculateWidgets();

    // reposition the window, including the owner-drawn list box controls
    if (m_hWnd != NULL)
    {
        CRect rect;
        MoveWindow(imXStart, imYStart, imXEnd-imXStart+1, imYEnd-imYStart+1, blRepaint);
        GetClientRect(&rect);
	//	rect.OffsetRect(-1, -1);
        pomGrid->MoveWindow(&rect, TRUE/*blRepaint*/);
		pomGrid->SetGridRect(TRUE, &rect );
		pomGrid->Redraw();
    }

    //-- exception handling
	CCS_CATCH_ALL

    return true;
}

bool CTableWithGrid::SetHeaderFields(CString opHeaderFields)
{
	//-- exception handling
	CCS_TRY

    omHeaderFields.RemoveAll();

    int i = 0;
	/*
    while (i < opHeaderFields.GetLength())
    {
        for (int n = i; n < opHeaderFields.GetLength() && opHeaderFields[n] != '|'; n++)
            ;
        omHeaderFields.Add(opHeaderFields.Mid(i, n-i));
        i = n + 1;
		ExtractItemList
    }*/
	i = ExtractItemList(opHeaderFields, &omHeaderFields, '|' );
    if ( pomGrid )
		pomGrid->SetHeaderFields ( omHeaderFields );
    //-- exception handling
	CCS_CATCH_ALL

    return true;

}

bool CTableWithGrid::ResetContent()
{
		//-- exception handling
	CCS_TRY

    if (m_hWnd != NULL)     // list box is exist?
        pomGrid->ResetContent();

    //-- exception handling
	CCS_CATCH_ALL

    return true;
}

bool CTableWithGrid::AddTextLine(CString opText, void *pvpData/* = NULL*/)
{
	//-- exception handling
	CCS_TRY

    if ( m_hWnd && pomGrid )     // list box is exist?
    {
		return ( pomGrid->AddTextLine ( opText, pvpData )!=0);
	}
                   
    //-- exception handling
	CCS_CATCH_ALL

    return true;
}

void CTableWithGrid::SetSelectMode(WORD ipSelectMode)
{
	imSelectMode = ipSelectMode;
	if ( pomGrid )
		pomGrid->GetParam()->EnableSelection ( imSelectMode );
}


bool CTableWithGrid::SetFormatList(CString opFormatList)
{
	if ( !pomGrid || !pomGrid->m_hWnd )
		return false;
	CStringArray olWidths;
	CCSPtrArray<double> olScaleFactors;
	int		i=0;
	long	ilSum=0, ilWidth;
	double  dlScale=0.0;

	ExtractItemList(opFormatList, &olWidths, '|' );	
	int     ilCols = olWidths.GetSize();

	for ( i=0; i<ilCols; i++ )
	{
		if ( sscanf ( olWidths[i], "%d", &ilWidth ) >0 )
			ilSum += ilWidth;
	}

	if ( ilSum < 1 )
		return false;
	for ( i=0; i<ilCols; i++ )
	{
		if ( sscanf ( olWidths[i], "%d", &ilWidth ) >0 )
			dlScale = (double)ilWidth / (double)ilSum;
		else
			dlScale = 0.0;
		olScaleFactors.New(dlScale);
	}
	BOOL blRet = pomGrid->SetColWidths ( olScaleFactors );
	olScaleFactors.DeleteAll ();
	return (blRet!=FALSE);
}

UINT CTableWithGrid::GetSelCount ( void *popSource )
{
	CRowColArray olRowArray;
	if ( !pomGrid || ( popSource != pomGrid ) )
		return 0;
	int ilCount = pomGrid->GetSelectedRows( olRowArray, FALSE, FALSE );
	return ilCount;
}
		
		
UINT CTableWithGrid::GetSelDataPtrs ( void*popSource, void**polSelDataPtr, 
									 UINT ipMaxItems )
{
	CRowColArray olRowArray;
	ROWCOL ilRow;
	if ( !pomGrid || ( popSource != pomGrid ) )
		return 0;
	ROWCOL ilCount = pomGrid->GetSelectedRows( olRowArray, FALSE, FALSE );
	ROWCOL k=0;

	//--- prepare drag
	ilCount = min ( ilCount, ipMaxItems );
	for ( ROWCOL i=0; i<ilCount; i++ )
	{
		CGXStyle olStyle;
		ilRow = olRowArray[i];
		if ( pomGrid->GetStyleRowCol( ilRow, 1, olStyle ) && 
			 olStyle.GetIncludeItemDataPtr() )
			polSelDataPtr[k++] = olStyle.GetItemDataPtr() ;
	}
	return k;
}

//////////////////////////////////////////////////////////////////////
// CTableGrid Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CTableGrid

CTableGrid::CTableGrid(CWnd *pParent)
	:CGridFenster(pParent)
{
	imMinRowCount = 10;
	imLastLineWithValues = 0;
}

CTableGrid::~CTableGrid()
{
}


BEGIN_MESSAGE_MAP(CTableGrid, CGridFenster)
	//{{AFX_MSG_MAP(CTableGrid)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CTableGrid message handlers

int CTableGrid::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CGridFenster::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	Initialize ();	
	SetColWidth ( 0, 0, 20 );
	GetParam()->SetNumberedColHeaders(FALSE);                 
	GetParam()->SetNumberedRowHeaders(FALSE); 
	GetParam()->EnableTrackRowHeight(0);
	SetReadOnly(TRUE);
	
//GetParam()->EnableTrackColWidth(0);
//	EnableOleDataSource (GX_DNDMULTI|GX_DNDROWHEADER|GX_DNDTEXT   );
	return 0;
}


BOOL CTableGrid::SetHeaderFields ( CStringArray &ropHeaderFields )
{
	BOOL blRet = TRUE;
	
	
	blRet = ResetContent ();
	SetReadOnly(FALSE);
	ROWCOL ilColCount = GetColCount ();
	ROWCOL ilColsNeeded = ropHeaderFields.GetSize ();
	if ( ilColCount > ilColsNeeded )
		blRet &= RemoveCols ( ilColsNeeded+1, ilColCount );
	else
		if ( ilColCount < ilColsNeeded )
			blRet &= InsertCols ( ilColCount+1, ilColsNeeded-ilColCount );
	if ( blRet )
		for ( ROWCOL i=1; i<=ilColsNeeded; i++ )
			SetValueRange ( CGXRange(0,i), ropHeaderFields[i-1] );
	SetReadOnly(TRUE);
	return blRet;
}

BOOL CTableGrid::AddTextLine( CString opText, void *pvpData )
{
	ROWCOL ilRowCount = GetRowCount();
	ROWCOL	idx = imLastLineWithValues + 1;
	SetReadOnly(FALSE);
	if ( ( idx > ilRowCount ) && !InsertRows ( ilRowCount+1, idx-ilRowCount ) )
	{
		SetReadOnly(TRUE);
		return FALSE;
	}
	CStringArray olEntries;
	BOOL		 blRet = TRUE;
	ExtractItemList(opText, &olEntries, '|' );	
	for ( int i=0; i<min(olEntries.GetSize(),(int)GetColCount() ); i++ )
		blRet &= SetValueRange ( CGXRange(idx,i+1), olEntries[i] );	
	if ( pvpData )
		blRet &= SetStyleRange ( CGXRange(idx,1), 
								 CGXStyle().SetItemDataPtr( pvpData) );
	SetStyleRange ( CGXRange().SetRows(idx), CGXStyle().SetEnabled(FALSE) );

	if ( blRet )
		imLastLineWithValues++;
	SetReadOnly(TRUE);
	return blRet;
}

BOOL CTableGrid::SetColWidths ( CCSPtrArray<double> &ropScaleFactors )
{
	ROWCOL ilCols = GetColCount ();
	int    ilWidth0 = GetColWidth ( 0 );
	int    ilWidth, ilActWidth;
	double dlActWidth;
	ROWCOL ilRowsToResize = min ( (int)ilCols, ropScaleFactors.GetSize() );
	CRect  olRect;
	BOOL   blRet=TRUE;
	
	GetClientRect ( &olRect );
	ilWidth = olRect.Width() - ilWidth0;
	if ( ilWidth <= 0 )
		return FALSE;

	for ( ROWCOL i=0; i<ilRowsToResize; i++ )
	{
		dlActWidth = (double)ilWidth * ropScaleFactors[i];
		ilActWidth = (int)(dlActWidth+0.5);	//  runden
		blRet &= SetColWidth ( i+1, i+1, ilActWidth );
	}
	return blRet;
}

void CTableGrid::SetMinRowCount ( ROWCOL ipMinRowCount )
{
	imMinRowCount = ipMinRowCount;
	if ( m_hWnd )
	{
		ROWCOL ilRows = GetRowCount ();
		if ( ilRows < imMinRowCount )
			InsertRows ( ilRows+1, imMinRowCount-ilRows );
	}
}

BOOL CTableGrid::ResetContent()
{

	SetReadOnly(FALSE);
	BOOL blRet = CGridFenster::ResetContent();
	if ( imMinRowCount > 0 )
		blRet &= InsertRows ( 1, imMinRowCount );
	imLastLineWithValues = 0;
	SetReadOnly(TRUE);
	return blRet;
}

void CTableGrid::SortRows(CGXRange sortRange, CGXSortInfoArray& sortInfo, 
						UINT flags/* = GX_UPDATENOW*/)
{
	CGXRange olRange ;
	if ( olRange.IntersectRange( sortRange, 
								 CGXRange().SetRows(1,imLastLineWithValues) ) )
		CGridFenster::SortRows(olRange, sortInfo, flags );
}

BOOL CTableGrid::DndStartDragDrop(ROWCOL nRow, ROWCOL nCol)
{
	if ( pomTableWindow->pomParentWindow )
		pomTableWindow->pomParentWindow->SendMessage(WM_TABLE_DRAGBEGIN, nRow, (LPARAM)this);
	return FALSE;
}



void CTableGrid::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CRowColArray olRowArray;
	bool blSelected=false;
	ROWCOL nRow;
	
	int ilHitState = HitTest(point, &nRow ) ;
	if ( (ilHitState==GX_CELLHIT) || (ilHitState==GX_HEADERHIT) )
	{
		ROWCOL ilCount = GetSelectedRows( olRowArray, FALSE, FALSE );
		for ( ROWCOL i=0; !blSelected&&(i<ilCount); i++ )
			if ( olRowArray[i] == nRow )
				blSelected = true;
		if ( blSelected && 	pomTableWindow && pomTableWindow->pomParentWindow )
		{
			pomTableWindow->pomParentWindow->SendMessage(WM_TABLE_DRAGBEGIN, nRow, (LPARAM)this);
			return ;
		}

	}
	CGridFenster::OnLButtonDown(nFlags, point);
}


void CTableGrid::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CRowColArray olRowArray;
	bool blSelected=false;
	ROWCOL nRow;
	
	int ilHitState = HitTest(point, &nRow ) ;
	if ( (nRow>0) && !IsSelectingCells( ) && 
		 ( (ilHitState==GX_CELLHIT) || (ilHitState==GX_HEADERHIT) )
	   )
	{
		ROWCOL ilCount = GetSelectedRows( olRowArray, FALSE, FALSE );
		for ( ROWCOL i=0; !blSelected&&(i<ilCount); i++ )
			if ( olRowArray[i] == nRow )
				blSelected = true;
		if ( blSelected )
		{	//  Deselektieren ( wenn CTRL gedr�ckt nur diese Zeile, sonst alles
			CGXRange olRange;
			if ( !(nFlags & MK_CONTROL) )
				olRange.SetTable();
			else
				olRange.SetRows(nRow);
			SelectRange( olRange, FALSE );
			return;
		}
		
	}
	else
		if ( ( nRow>0 ) && (ilHitState==GX_CELLHIT) )
			SelectRange ( CGXRange().SetRows(nRow) );
	CGridFenster::OnLButtonUp(nFlags, point);
}

void CTableGrid::OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey)
{
	if ( pRange && !pRange->IsRows() && (pRange->top>0) && 
		 (pRange->bottom>0) && (pRange->bottom>=pRange->top) )
		SelectRange ( CGXRange().SetRows(pRange->top, pRange->bottom) );
}
