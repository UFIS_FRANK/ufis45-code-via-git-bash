// GhsListviewer.cpp : implementation file
//
// Modification History:

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
//#include "CViewer.h"
#include <GhsListViewer.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GhsListViewer
//
GhsListViewer::GhsListViewer()
{
	
}

GhsListViewer::~GhsListViewer()
{
//    DeleteAll();
}

void GhsListViewer::ChangeViewTo(const char *pcpViewName)
{
	//-- exception handling
	CCS_TRY

	SelectView(pcpViewName);     

	//-- exception handling
	CCS_CATCH_ALL
}

