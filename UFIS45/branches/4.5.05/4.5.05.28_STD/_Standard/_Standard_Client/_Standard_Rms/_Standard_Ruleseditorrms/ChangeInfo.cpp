// ChangeInfo.cpp: implementation of the ChangeInfo class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <regelwerk.h>
#include <ChangeInfo.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <MainFrm.h>
#include <RegelWerkView.h>
#include <RegelWerkView.h>
#include <CollectiveRulesView.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


class CedaBasicData;

#define RUE_ARCHIVE_FIELDS "EVTY,EXCL,RUTY,UTPL"
#define RUD_ARCHIVE_FIELDS "ALOC,DRTY,RETY,FFPD,UCRU"
#define RPF_ARCHIVE_FIELDS "UPFC,FCCO,GTAB"
#define RPQ_ARCHIVE_FIELDS "UPER,QUCO,GTAB"
#define RLO_ARCHIVE_FIELDS "RLOC,REFT,GTAB"
#define REQ_ARCHIVE_FIELDS "UEQU,EQCO,ETYP,GTAB"


//------------------------------------------------------------------------------------------------------------------------
//					Construction/Destruction
//------------------------------------------------------------------------------------------------------------------------

ChangeInfo::ChangeInfo()
{
	//bmIndep = false;

	//pomRue = new RUEDATA;
}
//------------------------------------------------------------------------------------------------------------------------

ChangeInfo::~ChangeInfo()
{
	
}



//------------------------------------------------------------------------------------------------------------------------
//					set methods
//------------------------------------------------------------------------------------------------------------------------
void ChangeInfo::SetCopiedRuleFlag(bool bpCopy /*=true*/)
{
	bmIsCopiedFlag = bpCopy;
}
//------------------------------------------------------------------------------------------------------------------------


/*void ChangeInfo::SaveRuleOrigInfo(CString opRuleUrno, bool bpNew)
{
	CCS_TRY

	pomRue->Urno = opRuleUrno;

	if (bpNew)
	{
		pomRue->bExcl = false;
		pomRue->bFspl = false;
		pomRue->Evty = "0";
		pomRue->Maxt = " ";
		pomRue->Mist = " ";
		pomRue->Runa = " ";
		pomRue->Rusn = " ";
		pomRue->Rust = "0";
		pomRue->Ruty = "0";
	}
	else
	{
		RecordSet olRue(ogBCD.GetFieldCount("RUE"));
		ogBCD.GetRecord("RUE", "URNO", opRuleUrno, olRue);

		pomRue->bExcl = (olRue.Values[igRueExclIdx] == "0") ? false : true;
		pomRue->bFspl = (olRue.Values[igRueFsplIdx] == "1") ? false : true;
		pomRue->Evty = olRue.Values[igRueEvtyIdx];
		pomRue->Maxt = olRue.Values[igRueMaxtIdx];
		pomRue->Mist = olRue.Values[igRueMistIdx];
		pomRue->Runa = olRue.Values[igRueRunaIdx];
		pomRue->Rusn = olRue.Values[igRueRusnIdx];
		pomRue->Rust = olRue.Values[igRueRustIdx];
		pomRue->Ruty = olRue.Values[igRueRutyIdx];
	}

	CCS_CATCH_ALL
}*/
//---------------------------------------------------------------------------------------------------------------------

void ChangeInfo::SetCurrRueUrno(CString opRuleUrno)
{
	omCurrRueUrno = opRuleUrno;
}
//--------------------------------------------------------------------------------------------------------------------

// possible values: RULE_CHECK, RULE_NEW, RULE_COPY, RULE_DELETE
void ChangeInfo::SetChangeState(int ipChangeState)
{
	imChangeState = ipChangeState;
}
//---------------------------------------------------------------------------------------------------------------------

// true if rule changed
// false if unchanged
bool ChangeInfo::IsChanged()
{
	bool blRet = true; 

	switch (imChangeState)
	{
		case RULE_COPY:
			// copied rules have been changed anyway, 
			// no further check needed
			// user has to be asked to save
			blRet = false;
			TRACE ( "ChangeInfo::IsChanged:  Record changed because of RULE_COPY URUE=<%s>\n", omCurrRueUrno );
			break;
	
		case RULE_DELETE:
			// rule was deleted (user has confirmed already)
			// do not ask the user to save
			blRet = true;
			break;

		case RULE_NEW:
		case RULE_CHECK:
		default:
			// Status is RULE_CHECK or RULE_NEW
			// detailed check is necessary to 
			// decide if user should be asked to save
			blRet &= CheckChangesClosely();
			if (blRet)
			{
				blRet &= CheckResourceChanges("RUD", "RUD_TMP", "URUE", omCurrRueUrno);
				if ( !blRet )
					TRACE ( "ChangeInfo::IsChanged:  Record changed says CheckResourceChanges URUE=<%s>\n", omCurrRueUrno );
			}
			else
				TRACE ( "ChangeInfo::IsChanged:  Record changed says CheckChangesClosely URUE=<%s>\n", omCurrRueUrno );

			break;
	}

	return !(blRet);
}
//------------------------------------------------------------------------------------------------------------------------

//-----------------------------
// OUT: false: There are changes
//		true: No changes
//-----------------------------
bool ChangeInfo::CheckChangesClosely()
{
	CMainFrame *polMainWnd = (CMainFrame*) AfxGetMainWnd();
	if (polMainWnd != NULL)
	{
		CView *pActiveView = polMainWnd->GetActiveView();

		if (pActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
		{
			CRegelEditorFormView *polView = (CRegelEditorFormView*) polMainWnd->GetActiveView();
			return polView->CheckChanges();
		}
		else if (pActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
		{
			FlightIndepRulesView *polView = (FlightIndepRulesView*) polMainWnd->GetActiveView();
			return polView->CheckChanges();
		}
		else if (pActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
		{
			CollectiveRulesView *polView = (CollectiveRulesView*) polMainWnd->GetActiveView();
			return polView->CheckChanges();
		}
	}
	TRACE ( "ChangeInfo::CheckChangesClosely:  Record changed because polMainWnd is NULL URUE=<%s>", omCurrRueUrno );
	return false;	
}
//------------------------------------------------------------------------------------------------------------------------

//-----------------------------
//  IN: opOriTab   - name of original table (e.g. "RUD")
//		opTmpTab   - name of temporary copy (e.g. "RUD_TMP")
//		opRefField - name of reference field (e.g. "URUE")
//		opRefVal   - reference value (e.g. urno of current rule)
//
// OUT: false: There are changes
//		true:  No changes
//-----------------------------
bool ChangeInfo::CheckResourceChanges(CString opOriTab, CString opTmpTab, CString opRefField, CString opRefVal)
{
	bool blRet = true;

	CCS_TRY

	
	CCSPtrArray <RecordSet> olOriArr;
	CCSPtrArray <RecordSet> olTmpArr;
	
	ogBCD.GetRecords(opOriTab, opRefField, opRefVal, &olOriArr);
	ogBCD.GetRecords(opTmpTab, opRefField, opRefVal, &olTmpArr);

	int ilOriCount = olOriArr.GetSize();
	int ilTmpCount = olTmpArr.GetSize();

	blRet &= (ilOriCount == ilTmpCount);

	if (blRet)
	{
		RecordSet olOriRec(ogBCD.GetFieldCount(opOriTab));
		CString olOriVal, olTmpVal;	
		int i = 0;
		int ilHopoIdx = ogBCD.GetFieldIndex(opTmpTab,"HOPO");
		int ilUsecIdx = ogBCD.GetFieldIndex(opTmpTab,"USEC");
		int ilUseuIdx = ogBCD.GetFieldIndex(opTmpTab,"USEU");
		int ilLstuIdx = ogBCD.GetFieldIndex(opTmpTab,"LSTU");
		int ilCdatIdx = ogBCD.GetFieldIndex(opTmpTab,"CDAT");

		while ((blRet == true) && (i < ilOriCount))
		{
			int ilIdx = ogBCD.GetFieldIndex(opTmpTab, "URNO");
			olTmpVal = olTmpArr[i].Values[ilIdx];
			if ( !ogBCD.GetRecord(opOriTab, "URNO", olTmpArr[i].Values[ilIdx], olOriRec) )
			{
				blRet = false;
				TRACE ( "ChangeInfo::CheckResourceChanges:  Record URNO=<%s> does not exist in Table <%s>\n", 
						olTmpArr[i].Values[ilIdx], opOriTab );	
			}
			
			int j = 0;
			int ilMax = ogBCD.GetFieldCount(opOriTab);
			while ((blRet == true) && (j < ilMax))
			{
				/*hag20010531: these fields are filled inside classlib and therefore cannot be compared */
				if ( (j != ilHopoIdx) && (j != ilUsecIdx) && (j != ilUseuIdx) &&
					 (j != ilLstuIdx) && (j != ilCdatIdx) )		
				{
					olOriVal = olOriRec.Values[j];
					olTmpVal = olTmpArr[i].Values[j];
					olTmpVal.TrimLeft();
					olOriVal.TrimLeft();

					blRet &= (olOriVal == olTmpVal);
					/*  f�r tests */
					if ( olOriVal != olTmpVal )
					{
						CedaObject *prlObject;
						CString olFieldOri, olFieldTmp;
						if ( ogBCD.omObjectMap.Lookup((LPCSTR)opTmpTab, (void *&)prlObject) )
							olFieldTmp = prlObject->GetFieldList();
						if ( ogBCD.omObjectMap.Lookup((LPCSTR)opOriTab, (void *&)prlObject) )
							olFieldOri = prlObject->GetFieldList();
						TRACE ( "ChangeInfo::CheckResourceChanges:  URNO=<%s> of Table <%s>, FieldNr <%d> of FieldList <%s> Ori <%s> New <%s> \n", 
								olTmpArr[i].Values[ilIdx], opOriTab, j, olFieldOri, olOriVal, olTmpVal );	
					}
				}
				j++;
			}

			if (blRet == true && opOriTab == CString("RUD"))
			{
				blRet &= CheckResourceChanges("RPF", "RPF_TMP", "URUD", olTmpArr[i].Values[ilIdx]);

				if (blRet == true)
				{
					blRet &= CheckResourceChanges("RPQ", "RPQ_TMP", "URUD", olTmpArr[i].Values[ilIdx]);
				}

				if (blRet == true)
				{
					blRet &= CheckResourceChanges("RLO", "RLO_TMP", "URUD", olTmpArr[i].Values[ilIdx]);
				}

				if (blRet == true)
				{
					blRet &= CheckResourceChanges("REQ", "REQ_TMP", "URUD", olTmpArr[i].Values[ilIdx]);
				}
			}
			
			i++;
		}

	}
	else
		TRACE ( "ChangeInfo::CheckResourceChanges:  URUE=<%s>, Oricount <%d> Newcount <%d> \n", omCurrRueUrno, ilOriCount, ilTmpCount );	

	olOriArr.DeleteAll();
	olTmpArr.DeleteAll();

	CCS_CATCH_ALL

	return blRet;
}
//------------------------------------------------------------------------------------------------------------------------

bool ChangeInfo::ArchiveRule(RecordSet &ropRecord)
{
	bool blRet = true; 
	char pclTmp[5];
	RecordSet olOriRec(ogBCD.GetFieldCount("RUE"));

	GetPrivateProfileString ( ogAppName, "LIMITARCHIVING", "NO", pclTmp, 4, pcgConfigPath);
	
	if ( _stricmp ( pclTmp, "YES" ) != 0 )
		return true;

	blRet = ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olOriRec) ;
	if ( blRet )
	{
		CStringArray olFldArray;
		CString		 olOriVal, olTmpVal, olFina;	
		int			 ilActIdx ;         
		int			 ilFldCnt = ExtractItemList(RUE_ARCHIVE_FIELDS, &olFldArray, ',');

		blRet =  false;
		for  ( int j=0; !blRet && (j<ilFldCnt); j++ )
		{
			olFina = olFldArray[j];
			ilActIdx = ogBCD.GetFieldIndex ( "RUE", olFina );
			if ( ilActIdx >= 0 )
			{
				olOriVal = olOriRec.Values[ilActIdx];
				olTmpVal = ropRecord.Values[ilActIdx];
				olTmpVal.TrimLeft();
				olOriVal.TrimLeft();

				blRet |= (olOriVal != olTmpVal);
				/*  f�r tests */
				if ( olOriVal != olTmpVal )
				{
					TRACE ( "ChangeInfo::ArchiveRule:  Rule URNO=<%s> Field <%s>  Ori <%s> New <%s> \n", 
							omCurrRueUrno, olFina, olOriVal, olTmpVal );	
				}
			}
			else
				TRACE ( "ChangeInfo::ArchiveRule:  ERROR Index of <%s> in table <RUE> %d <0  \n", 
						 olFina, ilActIdx );
			j++;
		}
	}
	else
		TRACE ( "ChangeInfo::NeedToArchive:  Didn't find rule URNO=<%s> in DB -> no archiving\n", 
				omCurrRueUrno );	

	if ( !blRet )
		blRet = NeedToArchive("RUD", "RUD_TMP", "URUE", omCurrRueUrno, RUD_ARCHIVE_FIELDS );
	
	return blRet;
}


bool ChangeInfo::NeedToArchive(CString opOriTab, CString opTmpTab, CString opRefField, 
							   CString opRefVal, char *pcpFields )

{

	CCS_TRY

	
	CCSPtrArray <RecordSet> olOriArr;
	CCSPtrArray <RecordSet> olTmpArr;
	bool blRet = false;
	//--- String zerlegen 
	CStringArray olFldArray;
	int ilFldCnt = ExtractItemList(pcpFields, &olFldArray, ',');
	
	//  Empty field list -> nothing to do
	if ( ilFldCnt <= 0 )
		return false;

	ogBCD.GetRecords(opOriTab, opRefField, opRefVal, &olOriArr);
	ogBCD.GetRecords(opTmpTab, opRefField, opRefVal, &olTmpArr);
	int ilOriCount = olOriArr.GetSize();
	int ilTmpCount = olTmpArr.GetSize();
	
	//  blRet = (ilOriCount != ilTmpCount);

	//	PRF8351: As long as the amount of duty requirements is growing, rule won't be archived any longer.
	blRet = (ilOriCount > ilTmpCount);
	
	if (!blRet)
	{	//  count of records in XXXTAB not larger than in XXX_TMP
		RecordSet olOriRec(ogBCD.GetFieldCount(opOriTab));
		RecordSet olTmpRec(ogBCD.GetFieldCount(opTmpTab));
		CString olOriVal, olTmpVal, olFina;	
		int i = 0;
		int ilIdx = ogBCD.GetFieldIndex(opTmpTab, "URNO");
		int ilActIdx;
		
		if ( ilIdx < 0 )
		{
			TRACE ( "ChangeInfo::NeedToArchive:  ERROR Index of URNO in table <%s> %d <0  \n", 
					  opTmpTab, ilIdx );
			blRet = true;			
		}

		while ( (blRet == false) && (i < ilOriCount))
		{
			olOriRec = olOriArr[i];
			olOriVal = olOriArr[i].Values[ilIdx];
			if ( !ogBCD.GetRecord(opTmpTab, "URNO", olOriVal, olTmpRec) )
			{
				blRet = true;
				TRACE ( "ChangeInfo::NeedToArchive:  Record URNO=<%s> does not exist in Table <%s>\n", 
						olOriVal, opTmpTab );	
			}

			int j = 0;
			for  ( j=0; !blRet && (j<ilFldCnt); j++ )
			{
				olFina = olFldArray[j];
				ilActIdx = ogBCD.GetFieldIndex ( opTmpTab,olFina );
				if ( ilActIdx >= 0 )
				{
					olOriVal = olOriRec.Values[ilActIdx];
					olTmpVal = olTmpRec.Values[ilActIdx];
					olTmpVal.TrimLeft();
					olOriVal.TrimLeft();

					blRet |= (olOriVal != olTmpVal);
					/*  f�r tests */
					if ( olOriVal != olTmpVal )
					{
						TRACE ( "ChangeInfo::NeedToArchive:  URNO=<%s> of Table <%s>, Field <%s>  Ori <%s> New <%s> \n", 
								olTmpArr[i].Values[ilIdx], opOriTab, olFina,  olOriVal, olTmpVal );	
					}
				}
				else
					TRACE ( "ChangeInfo::NeedToArchive:  ERROR Index of <%s> in table <%s> %d <0  \n", 
							 olFina, opTmpTab, ilActIdx );
				j++;
			}
			/*	PRF8351: don't care about the resource requested. As long as the amount of duty
						 requirements is growing, rule won't be archived any longer.
			if (blRet == false && opOriTab == CString("RUD"))
			{

				blRet |= NeedToArchive("RPF", "RPF_TMP", "URUD", olTmpArr[i].Values[ilIdx], 
										RPF_ARCHIVE_FIELDS );
				if (blRet == false)
				{
					blRet |= NeedToArchive("RPQ", "RPQ_TMP", "URUD", olTmpArr[i].Values[ilIdx], 
											RPQ_ARCHIVE_FIELDS );
				}

				if (blRet == false)
				{
					blRet |= NeedToArchive("RLO", "RLO_TMP", "URUD", olTmpArr[i].Values[ilIdx], 
											RLO_ARCHIVE_FIELDS );
				}

				if (blRet == false)
				{
					blRet |= NeedToArchive("REQ", "REQ_TMP", "URUD", olTmpArr[i].Values[ilIdx], 
											REQ_ARCHIVE_FIELDS );
				}
			}
			*/
			i++;
		}

	}
	else
		TRACE ( "ChangeInfo::NeedToArchive:  table <%s>, Oricount <%d> Newcount <%d> \n", 
				opOriTab, ilOriCount, ilTmpCount );	

	olOriArr.DeleteAll();
	olTmpArr.DeleteAll();

	CCS_CATCH_ALL

	return blRet;
}
