// stviewer.h : header file
//

#ifndef __RULESGANTTVIEWER__
#define __RULESGANTTVIEWER__

#include <CCSBar.h>
#include <CCSPtrArray.h>



//--- structs
typedef struct ConnectionPosData
{
	CPoint Start;
	CPoint End;

} CON_POSDATA;


typedef struct RudArrowDataStruct
{
	// database values
	int			ArrowType;				// definition of types ==> see MakeArrows()
	int         ConnectionType;         // 0 = SF, 1 = SS, 2 = FS, 3 = FF
	bool		IsHighlighted;			// Is bar highlighted ?
	bool        SameRes;                // use same resources for both parts of a connection
	CTime		XValue;					// XValue of vertical part of line as CTime
	CString		SourceBar;				// Urno of bar connection arrow starts at
	CString		TargetBar;				// Urno of bar connection arrow ends at
	CCSPtrArray <CON_POSDATA> Positions;	// line position data (client coordinates)
	
	RudArrowDataStruct(void)
	{
		ArrowType = -1;
		IsHighlighted = false;
		XValue = -1;
	}

} ARROWDATA;

typedef struct RudBarDataStruct
{
	// database values
	CString	Urno;   // Unique Record Number
	CString Urue;	// rule urno (RUE)
	CTime   Debe;	// start of demand
	CTime   Deen;	// end of demand
	CTime   Prep;	// end of preparation time
	CTime   Fupt;	// start of follow-up treatment time
	CTime   Wayt;	// end of way time there
	CTime   Wayf;	// start of way time from
	CString	Dtyp;	// duty type (== SER.SEER)
	long    Dedu;	// duration of demand [min]
	long	Suti;   // setup time (preparation)
	long    Sdti;   // setdown time (follow-up treatment)
	long    Ttgt;	// time to get there
	long    Ttgf;   // time to get from
	long    Maxd;	// maximal duration of duty
	long    Mind;	// minimal duration of duty
	long    Tsdb;	// time interval at start of demand
	long    Tsde;	// time interval at end of demand
	long    Tsnd;	// time difference to resulting duty
	long    Tspd;	// time difference to preceding duty
	CString Rtdb;	// reference time for start of demand
	CString Rtde;	// reference time for end of demand
	CString Eadb;	// earliest possible start of demand
	CString Lade;	// latest possible end of demand
	CString Name;	// full name of service (SER.SNAM)
	CString Ssnm;	// short name of service (SER.SSNM)
	CString Text;	// short name  ??
	CString Stxt;   // text for status bar
	CString User;	// urno of service (table SER.URNO) 
	CString Ulnk;	// linked resources have the same urno here 
	bool	Ffpd;	// flag partial demand
	CString Uses;	// resources linked by USES have the same urno here 
	CString Rety;	// resource type

	// logical fields
    long FrameType;
    long MarkerType;
    CBrush *MarkerBrush;
	COLORREF TextColor;
	COLORREF BkColor;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	
	RudBarDataStruct(void)
	{
		Urno = "-1";
		Urue = "-1";
		Dtyp = "";
		Rtdb = "";
		Rtde = "";
		Eadb = "",
		Lade = "";
		Name = "";
		Ssnm = "";
		Text = "";
		User = "";
		Debe = -1;
		Deen = -1;
		Dedu = 0;
		Suti = 0;
		Sdti = 0;
		Ttgt = 0;
		Ttgf = 0;
		Maxd = 0;
		Mind = 0;
		Tsdb = 0;
		Tsde = 0;
		Tsnd = 0;
		Tspd = 0;
		FrameType = FRAMERECT;	// enum defined in CCSBar (ClassLib)
		MarkerType = MARKFULL;  // enum defined in CCSBar 
		OverlapLevel = 0;
		Ffpd = false;
	}

} BARDATA;



typedef struct RudLineDataStruct
{
	CString Urno;
	CString Urue;
	CString User;					// Urno of service (former UGHS)
	CTime	Debe;
	CTime	Deen;
	CString Name;
	CString Seco;					// Short name of service (former LKCO)
	CString Ulnk;					// Urno of linked Rud
	int		Disp;					// display position
	bool IsAdditionalSpecial;
	bool IsSelected;
	int ArrowPass;					// Is there an arrow passing the bar(s) in this line ? (0 = none, 1 = on bottom, 2 = on top)
    int MaxOverlapLevel;			// maximum number of overlapped level of bars in this line
    CCSPtrArray <BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray <ARROWDATA> Arrows;	// in order of painting (leftmost is the bottommost)
	//CCSPtrArray <LINKDATA> Links;	// in order of painting (leftmost is the bottommost)
	CCSPtrArray <int> TimeOrder;	// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF BkColor;
    
	RudLineDataStruct(void)
	{
		Disp = -1;
		Urno = "-1";
		Urue = "-1";
		User = -1;
		TextColor = ogColors[BLACK_IDX];
		BkColor = ogColors[SILVER_IDX];
		IsAdditionalSpecial = true;
		IsSelected = false;
		ArrowPass = 0;
		MaxOverlapLevel = 0;
		TextColor = 0;
		BkColor = 0;
	}
} LINEDATA;

typedef struct UnitDataStruct
{
	// database values
	UINT		number;
	CString		Uses;			// common URNO of field USES
	CTime		XStart;			
	CTime		XEnd;			
	CString		EquBar;				// Urno of equipment bar
	CString		StaffBar;			// Urno of employee bar
} UNITDATA;


//--- forward declaration
class RulesGantt;


//---------------------------------------------------------------------------------------------
//						class definition
//---------------------------------------------------------------------------------------------
class RulesGanttViewer
{
//--- Construction
public:
    RulesGanttViewer(long Rkey);
    ~RulesGanttViewer();
	
	void Attach(CWnd *popAttachWnd);
	
	
//--- Internal data processing routines
	void ClearAll();
	virtual bool IsPassFilter(RecordSet *popRud);
	int CompareLine(LINEDATA *prpLine1, LINEDATA *prpLine2);
	void RecalculateDisp();

	void ChangeViewTo(CString opUrno = CString("-1"));
	void ResetGantt();
	void MakeLines();
	void MakeBars();
	void MakeArrows();
	void MakeLine(RecordSet *popRud, int ipLineno = -1);
	void MakeEmptyLine(int ipLineno);
	void MakeBar(RecordSet *popRud, int ipLineno = -1);
	void MakeArrow(int ipConType, int ipType, int itemID, CTime opTime, CString opSourceBar, CString opTargetBar, bool bpSameRes);
	void AddPosData(int ipLine, int ipArrow, int ipX1, int ipY1, int ipX2, int ipY2);
	long CreateLinkBkColor(CString opUlnk);
	long GetLinkBkColor(CString opUlnk);
	void OnDeleteLink(CString opUlnk);
	void ResetLinkColorIndex();

	void MoveThisBar(int ipWhatChanged, CString opNewValue, RecordSet *popRud);
	void MoveTargetBars(int ipWhatChanged, CString opNewValue, RecordSet *popSource);

	void DeselectAllBars();
	void DeselectAllLines();
	
	void SetReferenceTimes(CTime opInb, CTime opOutb);
	void SetBarSelected(int ipRow, int ipBar, bool bpAction);
	void SetArrowHighlighted(CString opSourceBar);
	
	CString LineText(CString *popRudUrno);
	CString BarTextAndValues(RecordSet *popRud, bool bpInLogicalTable=true);
	CString StatusTextAndValues(RecordSet *popRud);

	CTime CalcStartTime(RecordSet *popRud);
	CTime CalcEndTime(RecordSet *popRud);
	CTime CalcConnectionXTime(CTime opSource, CTime opTarget, CTimeSpan opOffset, bool bpIsNegOffset, int ipConType);
/*	CTime CalcConnectedEndTime(RecordSet *popRud);
	CTime CalcConnectedStartTime(RecordSet *popRud);
	bool CalcDuration(int ipDrty, RecordSet *popRud, CTimeSpan &opLeft, CTimeSpan &opDedu, CTimeSpan &opRight);
*/
	bool FindLine(CString opUrno, int &ipLineno);
	bool FindDutyBar(CString opUrno, int &ripLineno);
	bool FindBar(CString opUrno, int ipLineno, int &ripBarno);
	bool FindBarGlobal(RecordSet *popRud, int &ripLineno, int &ripBarno);
	bool FindBarGlobal(CString opRudUrno, int &ripLineno, int &ripBarno);


//--- Operations
public:
	void SetCurrentRuleUrno(long lpRuleUrno);
	void SetCurrentRuleUrno(CString opRuleUrno);
	void SetGantt(RulesGantt *popGantt);
	
	long GetCurrRuleUrno();
	int GetNextFreeLine();
    int GetLineCount();
	int GetRealLineCount();
    LINEDATA *GetLine(int ipLineno);
    CString GetColors(int ipLineno, COLORREF *prpBkColor, COLORREF *prpTextColor);
	int GetVisualMaxOverlapLevel(int ipLineno);
    int GetMaxOverlapLevel(int ipLineno);
    int GetBkBarCount( int ipLineno);
    int GetBarCount(int ipLineno);
    BARDATA *GetBar( int ipLineno, int ipBarno);

	ARROWDATA *GetArrow(int ipLineno, int ipArrowno);
	int GetArrowCount(int itemID);
	bool GetArrowsForLine(int ipLineno, CCSPtrArray <ARROWDATA> &ropArrows);
	bool GetArrowTargetBySource(CString opSourceID, CString &ropTargetID);
	bool GetCompleteArrowBySourceID(CString opSourceID, CCSPtrArray <ARROWDATA> &ropArrows);
	bool GetCompleteArrowByTargetID(CString opTargetID, CCSPtrArray <ARROWDATA> &ropArrows);
	bool GetCompleteHighlightedArrow(CCSPtrArray <ARROWDATA> &ropArrows, CString &ropSourceID, CString &ropTargetID, int &ripType);
	bool GetConnectedBar(CString opBar1, CString &ropBarFrom, CString &ropBarTo);
	bool GetLinkedBars(CString opLinkUrno, CStringArray &ropLinkedBars);

	bool IsBarConnected(CString opBar1, CString opBar2);
	bool IsBarLinked(CString opBar1, CString opBar2);
	bool AreBarsLinked();
	//bool IsRecursion(RecordSet *popRud);
	bool AreBarsLinkedByUses();    
	bool CanBarsBeLinkedByUses();
	void MakeUnits ();
	bool DeleteUnitData ( CString &ropRudUrno );


	CString GetBarText(int ipLineno, int ipBarno);
	CString GetStatusBarText(int ipLineno, int ipBarno);
	bool GetBarSelectState(int ipLine, int ipBar);
	int GetBarType(RecordSet *popRud);
	int GetBarType(CString opBarUrno);


    int CreateLine( LINEDATA *prpLine, int ipLineno = -1);
    void DeleteLine(int ipLineno);
    int CreateBar(int ipLineno, BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipLineno, int ipBarno);
	void DeleteConnection(CString opSourceID);

    int GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2);

private:
    int GetFirstOverlapBarno(LINEDATA *prpLine, int ipBarno);
    BARDATA *GetBarInTimeOrder(LINEDATA *prpLine, int ipBarno);
	void GetOverlappedBarsFromTime( int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

    CTime GetEndTimeOfBarsOnTheLeft(LINEDATA *prpLine, int ipBarno);
	void ReorderBars(int ipLineno, int ilFirstBar, int ilTotalBars);


//--- Attributes
public:
	CString omRuleUrno;				// urno of current rule
	CString omHighlightedArrow;		// contains urno of bar at which arrow starts
	CTime omOnBlock;				//(1980, 1, 1, 1, 10, 0);
	CTime omOfBlock;				//(1980, 1, 1, 5, 50, 0);
	CCSPtrArray <LINEDATA> omLines;
	CCSPtrArray <UNITDATA> omUnits;	

	// background color for group links
	int imActiveLinkColorIdx;
	CMapStringToPtr	omUsedLinkColors;

private:
	CWnd *pomAttachWnd;
	RulesGantt *pomGantt;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBarBrushGray;
    
	CStringArray omSortOrder;

	int imVisibleLines;
	int imDiff;
	

//--- Methods which handle changes (from Data Distributor)
public:
	//void ProcessRudChange(RecordSet *popRud);
	//void ProcessRudDelete(RecordSet *popRud);

	void ProcessRudTmpNew(RecordSet *popRud);
	void ProcessRudTmpChange(RecordSet *popRud);
	void ProcessRudTmpDelete(RecordSet *popRud);
	void ProcessRudConnectionChange(RecordSet *popRud);

	//void ProcessRpfTmpChange(RecordSet *popRpf);
	void ProcessRpqTmpNewOrChange(RecordSet *popRpq);
	//void ProcessRloTmpChange(RecordSet *popRlo);
	void ProcessRuleDelete(RecordSet *popRud);
	void ProcessResourceTmpChange(RecordSet *popRecord, char *pcpTable );
};

#endif //__RULESGANTTVIEWER__
