// Dialog_ExpressionEditor.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <Dialog_ExpressionEditor.h>

#include <CedaBasicData.h>
#include <RecordSet.h>
#include <BasicData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] =__FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif



// kkh on 12/04/2007
bool editStatus = false;
//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
CDialog_ExpressionEditor::CDialog_ExpressionEditor(CString opTplUrno, CString opUrno, CString opRef, CString &ropTsrUrno, CWnd* pParent /*=NULL*/)
	: CDialog(CDialog_ExpressionEditor::IDD, pParent)
{
	CCS_TRY

	//{{AFX_DATA_INIT(CDialog_ExpressionEditor)
	m_Expression =_T("");
	//}}AFX_DATA_INIT

	omDgrUrno = opUrno;
	omRefFieldName = opRef;
	omTplUrno = opTplUrno;
	omTsrUrno = ropTsrUrno;
	
    imStatus = ANFANG;
	imLastStatus = ANFANG;

    imCountOpenBrackets = 0;
	imCountCloseBrackets = 0;
	
	pomSelectionGrid = new CGridFenster(this);

	CCS_CATCH_ALL
} 
//---------------------------------------------------------------------------------------------------------------------

CDialog_ExpressionEditor::~CDialog_ExpressionEditor()
{
	delete pomSelectionGrid;
	pomSelectionGrid = NULL;
}



//---------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//---------------------------------------------------------------------------------------------------------------------
void CDialog_ExpressionEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialog_ExpressionEditor)
	DDX_Control(pDX, EXPRDLG_COB_NAME, m_Combo_Name);
	DDX_Control(pDX, EXPRDLG_BTN_NEW, m_Btn_New);
	DDX_Control(pDX, IDC_EDIT_EXPRESSION, m_ExpressionEdit);
	DDX_Control(pDX, IDC_STATIC_InfoText, m_RefField);
	//}}AFX_DATA_MAP

    DDV_GXGridWnd(pDX, pomSelectionGrid);
}
//---------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CDialog_ExpressionEditor, CDialog)
	//{{AFX_MSG_MAP(CDialog_ExpressionEditor)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnGridFeldClicked)
	ON_BN_CLICKED(IDC_BUTTON_And, OnBUTTONAnd)
	ON_BN_CLICKED(IDC_BUTTON_Assign, OnBUTTONAssign)
	ON_BN_CLICKED(IDC_BUTTON_Bigger, OnBUTTONBigger)
	ON_BN_CLICKED(IDC_BUTTON_Equal, OnBUTTONEqual)
	ON_BN_CLICKED(IDC_BUTTON_KlammerAuf, OnBUTTONKlammerAuf)
	ON_BN_CLICKED(IDC_BUTTON_KlammerZu, OnBUTTONKlammerZu)
	ON_BN_CLICKED(IDC_BUTTON_NotEqual, OnBUTTONNotEqual)
	ON_BN_CLICKED(IDC_BUTTON_Or, OnBUTTONOr)
	ON_BN_CLICKED(IDC_BUTTON_Reset, OnBUTTONReset)
	ON_BN_CLICKED(IDC_BUTTON_Smaller, OnBUTTONSmaller)
	ON_EN_KILLFOCUS(IDC_EDIT_WERT, OnKillfocusEditWert)
	ON_BN_CLICKED(ID_SAVE, OnSave)
	ON_BN_CLICKED(IDC_BUTTON_Correct, OnBUTTONCorrect)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(EXPRDLG_BTN_NEW, OnNew)
	ON_CBN_SELENDOK(EXPRDLG_COB_NAME, OnSelendokName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



///-----------------------------------------------------------------------------------------------------------------------
//					message handlers
//-----------------------------------------------------------------------------------------------------------------------
BOOL CDialog_ExpressionEditor::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
		
	SetStaticTexts();

	  //--- initialize grids
        pomSelectionGrid->SubclassDlgItem(IDC_TABELLE, this);

        CRect olRect;
	    pomSelectionGrid->GetClientRect(olRect);

        pomSelectionGrid->Initialize();
		pomSelectionGrid->SetColCount(3);
        pomSelectionGrid->SetColWidth(0, 0, 20);
		pomSelectionGrid->SetColWidth(1, 1, 40);
		pomSelectionGrid->SetColWidth(2, 2, 110);
		pomSelectionGrid->SetColWidth(3, 3, 50);	// reft

		pomSelectionGrid->HideCols(3, 3);

        pomSelectionGrid->GetParam()->SetNumberedColHeaders(FALSE); 
		pomSelectionGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
		pomSelectionGrid->GetParam()->EnableTrackRowHeight(0); 

		pomSelectionGrid->SetValueRange(CGXRange(0,1), GetString(1480));
		pomSelectionGrid->SetValueRange(CGXRange(0,2), GetString(1481));

	  // Extended Felder anlegen aus Text Feld (in Verbindung mit TXT-Tabelle)
	  // oder EXTE-Feld des TSR-Satzes
	    CString  olExtensionStr;
		CStringArray olExteStrings;  //  wird gef�llt mit Strings aus "TSR.EXTE"
		CStringArray olTextStrings;  //  wird gef�llt mit Strings aus TXT refer. durch "TSR.TEXT"

		olExtensionStr = ogBCD.GetField("TSR", "URNO", omTsrUrno, "EXTE");
		int ilExteCount  = ExtractItemList(olExtensionStr, &olExteStrings, '|');
		bool blTexteOk = FillNamesArray (omTsrUrno, olTextStrings);
		int ilTextCount = olTextStrings.GetSize();

		if(ilExteCount > 0)
		{
		   int ilPos = -1;
		   CString olRefField;
		   CString olRealName;
		   CString olRtab, olRfld, olReft;
		   CString olTmp;
			
		   for (int i = 0; i < ilExteCount; i++)
		   {
			    //--- Zeile einf�gen
                pomSelectionGrid->InsertRows(i + 1, 1);

		        //--- Zeile f�llen
				ogBCD.GetFields("TSR", "URNO", omTsrUrno, "RTAB", "RFLD", olRtab, olRfld);
				//olReft = olRtab + ETX + olRfld;		hag991221

				olTmp = olExteStrings.GetAt(i);
				ilPos = olTmp.Find("(");
				olRefField = olTmp.Left(ilPos);
				olReft = olRtab + ETX + olRefField;	//	hag991221

				if (blTexteOk && (i + 1 < ilTextCount))
				{
					olRealName = olTextStrings[i+1];
				}
				else
				{
					olRealName.Empty();
				}
				
				if (olRealName.IsEmpty() == TRUE)
				{
					olRealName = olTmp.Mid(ilPos + 1, olTmp.GetLength() - 6);
				}

                pomSelectionGrid->SetValueRange(CGXRange(i + 1, 1), olRefField);
				pomSelectionGrid->SetValueRange(CGXRange(i + 1, 2), olRealName);
				pomSelectionGrid->SetValueRange(CGXRange(i + 1, 3), olReft);
		   }

		   //-- grid will not be editable
		   int ilRowCount = pomSelectionGrid->GetRowCount();
		   pomSelectionGrid->SetStyleRange(CGXRange(0, 0, ilRowCount, 2), CGXStyle().SetEnabled(FALSE)
																					.SetReadOnly(TRUE));
		}
		
		// get data from DGRTAB
		RetrieveDataFromDB(omDgrUrno);		

		// show reference table and field
        m_RefField.SetWindowText("<" + omRefFieldName + ">");

		// fill combo box with DGR records
		FillCombo();

	CCS_CATCH_ALL

	return TRUE;   
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnNew() 
{
	// get new urno
	long llUrno = ogBCD.GetNextUrno();
	omDgrUrno.Format("%d", llUrno);

	// clear controls
	m_Combo_Name.SetCurSel(-1);
	m_ExpressionEdit.SetWindowText("");

	// clear variables
	omInternUndoArr.RemoveAll();
	omUserUndoArr.RemoveAll();
	imCountOpenBrackets = 0;
	imCountCloseBrackets = 0;
	omExpression = CString("");
	omUserExpression = CString("");
	OnBUTTONReset();

	//kkh on 12/04/2007
	editStatus = false;
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnSelendokName() 
{
	int ilSel = m_Combo_Name.GetCurSel();
	long llUrno = -1;
	if (ilSel != -1)
	{
		m_Combo_Name.GetLBText(ilSel, omDgrName);
		llUrno = m_Combo_Name.GetItemData(ilSel);
	}

	omDgrUrno.Format("%d", llUrno);
	
	RecordSet olRecord(ogBCD.GetFieldCount("DGR"));
	ogBCD.GetRecord("DGR", "URNO", omDgrUrno, olRecord);
	omDgrName = olRecord.Values[igDgrGnamIdx];
	omExpression = olRecord.Values[igDgrExprIdx];

	omUserExpression = GetFormattedUserExpression(omExpression);
	// kkh 13/04/2007
	//AfxMessageBox(omExpression);
	//AfxMessageBox(omUserExpression);

	// kkh on 12/04/2007
	if (omUserExpression != "")
	{
		editStatus = true;
	}

	SetExpression();
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnGridFeldClicked(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	

	CELLPOS *polPos = (CELLPOS *)lParam;
	int ilRow = polPos->Row;
	int ilCol = polPos->Col;

	if (ilRow > 0)
	{
		// kkh on 12/04/2007
		if((imStatus == ANFANG && editStatus == false) || imStatus == KLAMMER_AUF || imStatus == AND_OR || imStatus == KLAMMER_ZU)  
		{
			 //--- Feld Daten 
			 CString olRealName;
			 CString olReft;

			 olReft = pomSelectionGrid->GetValueRowCol(ilRow, 3); 
			 olRealName = pomSelectionGrid->GetValueRowCol(ilRow, 2);

			 //---  Save Expression for Undo 
			 omInternUndoArr.Add(omExpression);
			 omUserUndoArr.Add(omUserExpression);

			 //--- Expression erweitern
			 omUserExpression += olRealName;
			 omExpression += olReft;
			 
			 //--- Status setzen
			 imLastStatus = imStatus;
			 imStatus = AUSWAHL;
     
			 //--- SetExpression
			 SetExpression();

			 // kkh on 12/04/2007
			 if (imStatus == ANFANG && editStatus == false)
			 {
				editStatus = true;
			 }
		}
	}


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONAnd() 
{
	CCS_TRY

	// KKH - on 11/04/2007 

	if(imStatus == WERT || imStatus == KLAMMER_ZU || editStatus == 1)
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += "&&";
		 omUserExpression += CString (" ") + GetString(1385) + CString (" ");

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = AND_OR;

		 // KKH on 12/04/2007
		 editStatus = false;
		 //--- SetExpression
		 SetExpression();
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONOr() 
{
	//-- exception handling
	CCS_TRY

	// KKH - on 11/04/2007 
	if(imStatus == WERT || imStatus == KLAMMER_ZU || editStatus == 1)
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += "||";
		 omUserExpression += CString(" ") + GetString(1386) + CString(" ");

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = AND_OR;

		 // KKH on 12/04/2007
		 editStatus = false;
		 //--- SetExpression
		 SetExpression();
	}


	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------


void CDialog_ExpressionEditor::OnBUTTONBigger() 
{
	//-- exception handling
	CCS_TRY


	if(imStatus == AUSWAHL)
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += ">";
		 omUserExpression += " > ";

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = OPERATOR;

		 //--- SetExpression
		 SetExpression();
	}


	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONSmaller() 
{
	//-- exception handling
	CCS_TRY

	if(imStatus == AUSWAHL)
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += "<";
		 omUserExpression += " < ";

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = OPERATOR;

		 //--- SetExpression
		 SetExpression();
	}


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONEqual() 
{
	CCS_TRY
	

	// EQUAL can follow a field name or one of the operators '<' or '>'
	if(imStatus == AUSWAHL )
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += "=";		 
		 omUserExpression += " = ";
		 
		 omUserExpression.Replace("<  =", "<=");
		 omUserExpression.Replace(">  =", ">=");

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = OPERATOR;

		 //--- SetExpression
		 SetExpression();
	}
	

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONNotEqual() 
{
	CCS_TRY


	if(imStatus == AUSWAHL)
	{

		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression +="<>";
		 omUserExpression += " <> ";

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = OPERATOR;

		 //--- SetExpression
		 SetExpression();
	}
	
	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONKlammerAuf() 
{
	//-- exception handling
	CCS_TRY

	// kkh on 12/04/2007
	// && editStatus == false
	if((imStatus == ANFANG && editStatus == false) || imStatus == AND_OR || imStatus == KLAMMER_AUF)
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += "(";
		 omUserExpression += " (";

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = KLAMMER_AUF;
		 imCountOpenBrackets++;

		 //--- SetExpression
		 SetExpression();
		 // kkh on 12/04/2007
		 if (imStatus == ANFANG && editStatus == false)
		 {
			editStatus = true;
		 }
	}
	
	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONKlammerZu() 
{
	//-- exception handling
	CCS_TRY

	if((imStatus == WERT || imStatus == KLAMMER_ZU) &&
		(imCountCloseBrackets  < imCountOpenBrackets))
	{
		 //---  Save Expression for Undo 
		 omInternUndoArr.Add(omExpression);
		 omUserUndoArr.Add(omUserExpression);

		 //--- Expression erweitern
		 omExpression += ")";
		 omUserExpression += ") ";

		 //--- Status setzen
		 imLastStatus = imStatus;
		 imStatus = KLAMMER_ZU;
		 imCountCloseBrackets++;

		 //--- SetExpression
		 SetExpression();
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONAssign() 
{
	if(imStatus == OPERATOR)
		OnKillfocusEditWert();
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONReset() 
{
	//-- exception handling
	CCS_TRY

	 //---  Save Expression for Undo
	 if (omInternUndoArr.GetSize() > 0)
	 {
		 if (omInternUndoArr.GetAt(omInternUndoArr.GetUpperBound()) != "")
		 {
			 omInternUndoArr.Add(omExpression);
			 omUserUndoArr.Add(omUserExpression);
		 }
	 }

	 //--- Expression erweitern
     omExpression = "";
	 omUserExpression = "";

	 //--- Status setzen
	 imLastStatus = imStatus;
     imStatus = ANFANG;

	 //--- SetExpression
	 SetExpression();

	 //-- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnBUTTONCorrect() 
{
	//--- copy last string back and remove it from array
    if (omInternUndoArr.GetSize() > 0) 
	{
		omExpression = omInternUndoArr.GetAt(omInternUndoArr.GetSize() - 1);
		omInternUndoArr.RemoveAt(omInternUndoArr.GetSize() - 1);
	}
	if (omUserUndoArr.GetSize() > 0)
	{
		omUserExpression = omUserUndoArr.GetAt(omUserUndoArr.GetSize() - 1);
		omUserUndoArr.RemoveAt(omUserUndoArr.GetSize() - 1);
	}

	//--- set last state
     imStatus = imLastStatus;

	 //--- SetExpression
	 SetExpression();
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnKillfocusEditWert() 
{
	//-- exception handling
	CCS_TRY

	if(imStatus  == OPERATOR)
	{
		 CString  olValue;

 		 CEdit*  polEditControl  = (CEdit*) GetDlgItem(IDC_EDIT_WERT);
                  
				 polEditControl->GetWindowText(olValue);
				 polEditControl->SetWindowText("");

		   if(!olValue.IsEmpty())
		   {
			 // save expression for undo
			 omInternUndoArr.Add(omExpression);
			 omUserUndoArr.Add(omUserExpression);

			 //--- append expression
			 omExpression += olValue;
			 omUserExpression += olValue;

			 //--- set state
			 imLastStatus = imStatus;
			 imStatus = WERT;

			 //--- set expression
			 SetExpression();
		   }
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnSave() 
{
	CCS_TRY

	bool blOk = true;

	// make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	UpdateData(TRUE);
	
	m_Combo_Name.GetWindowText(omDgrName);
	if(omDgrName.IsEmpty())
	{
		AfxMessageBox(GetString(158));
		return;
	}

	//--- Neuen olRecord mit Namen  in DB  einf�gen
	RecordSet  olRecord( ogBCD.GetFieldCount("DGR"));

	CString olUrno;
	if ( !omDgrUrno.IsEmpty() && 
		 ogBCD.GetRecord("DGR", "URNO", omDgrUrno, olRecord) )
	{
		olRecord[igDgrGnamIdx] = omDgrName;
		olRecord[igDgrExprIdx] = omExpression;
		blOk = ogBCD.SetRecord("DGR", "URNO", omDgrUrno, olRecord.Values, true );
	}
	else
	{
		olUrno = ogBCD.GetField("DGR", "GNAM", omDgrName, "URNO");
		if (olUrno.IsEmpty() == TRUE)
		{
			olRecord[igDgrUtplIdx] = omTplUrno;
			olRecord[igDgrGnamIdx] = omDgrName;
			olRecord[igDgrExprIdx] = omExpression;
			olRecord[igDgrReftIdx] = omRefFieldName;
			blOk = ogBCD.InsertRecord("DGR",  olRecord, true);
			if ( blOk )
			{
				omDgrUrno = ogBCD.GetField("DGR", "GNAM", omDgrName, "URNO");
				if (omDgrUrno.IsEmpty() == FALSE)
				{
					long llUrno = atol(omDgrUrno);
					int ilIdx = m_Combo_Name.AddString(omDgrName);
					m_Combo_Name.SetItemData(ilIdx, llUrno);
					m_Combo_Name.SetCurSel (ilIdx);
				}
			}
		}
		else
		{
			MessageBox(GetString(1483), GetString(1482), MB_OK);
		}
	}    

	if ( !blOk )
		MessageBox(GetString(IDS_DGR_SAVE_ERR), GetString(1482), MB_OK);
	// make regular cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnDelete() 
{
	 //--- L�schen aus DB
	if(!omDgrUrno.IsEmpty())
	{
		BOOL blOk = ogBCD.DeleteRecord("DGR", "URNO", omDgrUrno, TRUE);
		if ( blOk )		//   L�schen hat geklappt
		{
			int ilSel = m_Combo_Name.GetCurSel();
			int ilRest;
			
			if ( ilSel >= 0 )
				ilRest = m_Combo_Name.DeleteString(ilSel);
			else
				ilRest = m_Combo_Name.GetCount();
			ilSel = ( ilRest > 0 ) ? 0 : -1 ;
			m_Combo_Name.SetCurSel( ilSel );
			OnSelendokName();
		}
		
	}
	else
	{
		 MessageBox(GetString(1419), GetString(1420)), MB_OK || MB_ICONEXCLAMATION;
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}



//-----------------------------------------------------------------------------------------------------------------------
//					helper methods
//-----------------------------------------------------------------------------------------------------------------------
void CDialog_ExpressionEditor::SetExpression() 
{
	  m_ExpressionEdit.SetWindowText(omUserExpression);
}
//-----------------------------------------------------------------------------------------------------------------------

bool CDialog_ExpressionEditor::RetrieveDataFromDB(CString opUrno) 
{
	bool blErr = true;
	
	//---exception handling
	CCS_TRY


	if (opUrno.IsEmpty() == TRUE)
	{
		OnNew();
	}
	else
	{
		// fill variables
		RecordSet olRecord;
		if (ogBCD.GetRecord("DGR", "URNO", opUrno, olRecord) == false)
		{
			OnNew();
		}
		else
		{
			omDgrName = olRecord.Values[igDgrGnamIdx];
			omExpression = olRecord.Values[igDgrExprIdx];

			// set user expression 
			/*CString olRefField;
			CString olRealName;
			CString olReft;

			int ilCount = pomSelectionGrid->GetRowCount();
			omUserExpression = omExpression;

			for (int i = 0; i <= ilCount; i++)
			{	
				// olRefField = pomSelectionGrid->GetValueRowCol(i, 1);
				olRealName = pomSelectionGrid->GetValueRowCol(i, 2);
				olReft = pomSelectionGrid->GetValueRowCol(i, 3);

				//omUserExpression.Replace(olRefField, olRealName);
				omUserExpression.Replace(olReft, olRealName);
			}

			omUserExpression.Replace("&&", GetString(1385));
			omUserExpression.Replace("||", GetString(1386));*/

			omUserExpression = GetFormattedUserExpression(omExpression);
			// kkh on 12/04/2007
			if (omUserExpression != "")
			{
				editStatus = true;
			}

			//kkh on 13/04/2007 put the existing expression into array for undo purpose
			printf("%s\n", omExpression);
			printf("%s\n", omUserExpression);



		}
	}
		
	SetExpression();

	//---exception handling
	CCS_CATCH_ALL

	return blErr;
}
//-----------------------------------------------------------------------------------------------------------------------

CString CDialog_ExpressionEditor::GetFormattedUserExpression(CString opDBExpression)
{
	CString olRet;

	CCS_TRY


	CString olRealName;
	CString olReft;

	int ilCount = pomSelectionGrid->GetRowCount();
	olRet = opDBExpression;

	for (int i = 0; i <= ilCount; i++)
	{	
		olRealName = pomSelectionGrid->GetValueRowCol(i, 2);
		olReft = pomSelectionGrid->GetValueRowCol(i, 3);

		olRet.Replace(olReft, olRealName);
	}

	CString olTmp;
	olTmp = CString(" ") + GetString(1385) + CString(" ");
	olRet.Replace("&&", olTmp);
	olTmp = CString(" ") + GetString(1386) + CString(" ");
	olRet.Replace("||", olTmp);

	olRet.Replace("=", " = ");
	olRet.Replace("<", " < ");
	olRet.Replace(">", " > ");
	olRet.Replace("<>", " <> ");
	olRet.Replace("(", " (");
	olRet.Replace(")", ") ");


	CCS_CATCH_ALL

	return olRet;
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_ExpressionEditor::FillCombo()
{
	//-------------------------------------------
	// fill combo box with all DGR records with suitable REFT and UTPL
	//-------------------------------------------

	CCSPtrArray <RecordSet> olRecordArr;
	ogBCD.GetRecordsExt("DGR", "REFT", "UTPL", omRefFieldName, omTplUrno, &olRecordArr);
	//ogBCD.GetRecords("DGR", "REFT", omRefFieldName, &olRecordArr);

	CString olName;
	long llUrno;
	int ilIdx;
	
	int ilCount = olRecordArr.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		olName = olRecordArr[i].Values[igDgrGnamIdx];
		llUrno = atol(olRecordArr[i].Values[igDgrUrnoIdx]);
		ilIdx = m_Combo_Name.AddString(olName);
		m_Combo_Name.SetItemData(ilIdx, llUrno);
	}

	m_Combo_Name.SelectString(-1, omDgrName);

	olRecordArr.DeleteAll();
}



//-----------------------------------------------------------------------------------------------------------------------
//					other stuff
//-----------------------------------------------------------------------------------------------------------------------
void CDialog_ExpressionEditor::SetStaticTexts()
{
	CCS_TRY


	CWnd *pWnd = NULL;

	// caption
	SetWindowText(GetString(1420));
	
	// controls
	pWnd = GetDlgItem(ID_SAVE);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(130));

	pWnd = GetDlgItem(IDC_DELETE);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(131));

	pWnd = GetDlgItem(EXPRDLG_BTN_NEW);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1372));

	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1375));

	SetDlgItemLangText ( this, IDC_STATIC, IDS_INFO );

	pWnd = GetDlgItem(EXPRDLG_STA_NAME);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(217));

	SetDlgItemLangText ( this, EXPRDLG_STA_EXPR, IDS_EXPRESSION );
	SetDlgItemLangText ( this, IDC_STATIC_SEL, IDS_SELECTION );
	SetDlgItemLangText ( this, EXPRDLG_STA_OPER, IDS_OPERATOR );

	pWnd = GetDlgItem(IDC_STATIC_VAL);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1521));

	pWnd = GetDlgItem(IDC_BUTTON_And);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1385));

	pWnd = GetDlgItem(IDC_BUTTON_Or);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1386));

	pWnd = GetDlgItem(IDC_BUTTON_Correct);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1393));

	pWnd = GetDlgItem(IDC_BUTTON_Reset);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1394));

	pWnd = GetDlgItem(IDC_BUTTON_Assign);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1397));


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

