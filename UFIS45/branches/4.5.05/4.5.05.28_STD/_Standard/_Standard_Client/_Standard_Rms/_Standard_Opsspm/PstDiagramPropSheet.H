// PstDiagramPropSheet.h : header file
//

#ifndef _PSTDIAPS_H_
#define _PSTDIAPS_H_
#include <PstDiagramArrDepPage.h>
#include <PstDiagramSetupPage.h>

/////////////////////////////////////////////////////////////////////////////
// PstDiagramPropertySheet

class PstDiagramPropertySheet : public BasePropertySheet
{
// Construction
public:
	PstDiagramPropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pagePstArea;
	FilterPage m_pageAirline;
	FilterPage m_pageAgents;
	FilterPage m_pageDemandTypes;
	FilterPage m_pageKeycodes;
	FilterPage m_pageNatures;
	FilterPage m_pageAct;
	PstDiagramArrDepPage m_pageArrDep;
	PstDiagramSetupPage m_pageSetup;
	FilterPage m_pageConflicts;
	FilterPage m_pageFunctions; //PRF 8999
	FilterPage m_pageEquDemandGroups;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();

private:
	void FillEquipmentDemandGroups();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _PSTDIAPS_H_
