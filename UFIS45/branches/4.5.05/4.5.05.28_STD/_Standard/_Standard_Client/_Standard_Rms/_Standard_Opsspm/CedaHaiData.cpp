// CedaHaiData.cpp - Class for Airline Types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaHaiData.h>
#include <BasicData.h>

CedaHaiData::CedaHaiData()
{                  
    BEGIN_CEDARECINFO(HAIDATA, HaiDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Altu,"ALTU")
		FIELD_CHAR_TRIM(Hsna,"HSNA")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(HaiDataRecInfo)/sizeof(HaiDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HaiDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"HAITAB");
    pcmFieldList = "URNO,ALTU,HSNA";
}
 
CedaHaiData::~CedaHaiData()
{
	TRACE("CedaHaiData::~CedaHaiData called\n");
	ClearAll();
}

void CedaHaiData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omAltuMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaHaiData::ReadHaiData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char clSelection[512];
    sprintf(clSelection,"WHERE TASK = '%s'",ogBasicData.GetHandlingAgentForPrm());


    if((ilRc = CedaAction2(pclCom, clSelection)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaHaiData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,clSelection);
	}
	else
	{
		HAIDATA rlHaiData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlHaiData)) == RCSuccess)
			{
				AddHaiInternal(rlHaiData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaHaiData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(HAIDATA), clSelection);
    return ilRc;
}


void CedaHaiData::AddHaiInternal(HAIDATA &rrpHai)
{
	HAIDATA *prlHai = new HAIDATA;
	*prlHai = rrpHai;
	omData.Add(prlHai);
	omUrnoMap.SetAt((void *)prlHai->Urno,prlHai);
	omAltuMap.SetAt((void *)prlHai->Altu,prlHai);
}


HAIDATA* CedaHaiData::GetHaiByUrno(long lpUrno)
{
	HAIDATA *prlHai = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlHai);
	return prlHai;
}

HAIDATA* CedaHaiData::GetHaiByAltu(long lpAltu)
{
	HAIDATA *prlHai = NULL;
	omAltuMap.Lookup((void *)lpAltu, (void *&) prlHai);
	return prlHai;
}


CString CedaHaiData::GetTableName(void)
{
	return CString(pcmTableName);
}



