// StaffDiagramSortPage.h : header file
//

#ifndef _STFDIASO_H_
#define _STFDIASO_H_

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramSortPage dialog

class StaffDiagramSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(StaffDiagramSortPage)

// Construction
public:
	StaffDiagramSortPage();
	~StaffDiagramSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;
	
	//{{AFX_DATA(StaffDiagramSortPage)
	enum { IDD = IDD_STAFFDIAGRAM_SORT_PAGE };
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(StaffDiagramSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffDiagramSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _STFDIASO_H_
