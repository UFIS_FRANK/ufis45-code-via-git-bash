// CedaRnkData.cpp - Formerly Ranks, now Functions in PFCTAB
// see also BasicData::CreateListOfManagers() - map of which of these functions is a manager

#include <stdafx.h>
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccslog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaRnkData.h>


CedaRnkData::CedaRnkData()
{
    BEGIN_CEDARECINFO(RNKDATA, RnkDataRecInfo)
		FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Fctc,"FCTC")
        FIELD_CHAR_TRIM(Fctn,"FCTN")
        FIELD_CHAR_TRIM(Prio,"PRIO")
    END_CEDARECINFO

    for (int i = 0; i < sizeof(RnkDataRecInfo)/sizeof(RnkDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RnkDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"PFCTAB");
    pcmFieldList = "URNO,FCTC,FCTN,PRIO";
}

CedaRnkData::~CedaRnkData()
{
	TRACE("CedaRnkData::~CedaRnkData called\n");
	ClearAll();
}

void CedaRnkData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omFctcMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaRnkData::ReadRnkData()
{
	ClearAll();

	char pclWhere[512];
	CCSReturnCode ilRc = RCSuccess;

	// Select data from the database
	char pclCom[10] = "RT";
	sprintf(pclWhere,"");
	if (CedaAction2(pclCom, pclWhere) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaRnkData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			RNKDATA *prlRnk = new RNKDATA;
			if ((ilRc = GetBufferRecord2(ilLc,prlRnk)) == RCSuccess)
			{
				AddRnkInternal(prlRnk);
			}
			else
			{
				delete prlRnk;
			}
		}
	}

	ogBasicData.Trace("CedaRnkData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RNKDATA), pclWhere);
	return TRUE;
}


BOOL CedaRnkData::AddRnkInternal(RNKDATA *prpRnk)
{
	PrepareRnkData(prpRnk);
	omData.Add(prpRnk);
	omUrnoMap.SetAt((void *)prpRnk->Urno,prpRnk);
	omFctcMap.SetAt(prpRnk->Fctc,prpRnk);
	return RCSuccess;
}

void CedaRnkData::PrepareRnkData(RNKDATA *prpRnk)
{
}

int CedaRnkData::GetRankWeight(const char *pcpFctc)
{
	int ilWeight = -1;
	RNKDATA *prlRnk = GetRnkByFctc(pcpFctc);
	if(prlRnk != NULL)
	{
		ilWeight = atoi(prlRnk->Prio);
	}
	return  ilWeight;
}

RNKDATA *CedaRnkData::GetRnkByUrno(long lpUrno)
{
	RNKDATA *prlRnk = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& ) prlRnk);
	return prlRnk;
}

RNKDATA *CedaRnkData::GetRnkByFctc(const char *pcpFctc)
{
	RNKDATA *prlRnk = NULL;
	omFctcMap.Lookup(pcpFctc,(void *& ) prlRnk);
	return prlRnk;
}

void CedaRnkData::GetAllRanks(CStringArray& ropRanks, bool bpReset /*=true*/)
{
	if(bpReset)
	{
		ropRanks.RemoveAll();
	}

	int ilNumRnks = omData.GetSize();
	for(int ilLc = 0; ilLc < ilNumRnks; ilLc++)
	{
		ropRanks.Add(CString(omData[ilLc].Fctc));
	}
}

CString CedaRnkData::GetTableName(void)
{
	return CString(pcmTableName);
}