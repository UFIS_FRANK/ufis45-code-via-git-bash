#if !defined(AFX_SELPOOLDLG_H__83D5CEA1_B3B4_11D3_92CF_0000B4392C49__INCLUDED_)
#define AFX_SELPOOLDLG_H__83D5CEA1_B3B4_11D3_92CF_0000B4392C49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelPoolDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelPoolDlg dialog

class CSelPoolDlg : public CDialog
{
// Construction
public:
	CSelPoolDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelPoolDlg)
	enum { IDD = IDD_SELPOOLDLG };
	CListBox	m_SelPool;
	CString	m_Title;
	//}}AFX_DATA

	CStringArray *pomPools;
	CString omPool;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelPoolDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelPoolDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELPOOLDLG_H__83D5CEA1_B3B4_11D3_92CF_0000B4392C49__INCLUDED_)
