// DemandTableViewer.cpp -- the viewer for Demand Table
//
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaDemandData.h>
#include <OpssPm.h>
#include <ccstable.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>
#include <DemandTableViewer.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaRpqData.h>
#include <CedaPfcdata.h>
#include <CedaPerData.h>
#include <CedaAcrData.h>
#include <Buttonlist.h>
#include <CedaEquData.h>
#include <CedaReqData.h>
#include <CedaAltData.h>
#include <CedaNatData.h>
#include <CedaAptData.h>
#include <CedaPaxData.h>
#include <CedaLoaData.h>
#include <DemandTable.h> //Singapore
#include <DlgSettings.h> //Singapore 

const CString TERMINALNO2 = "2"; //Singapore
const CString TERMINALNO3 = "3"; //Singapore
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_ALOC,
	BY_OBTY,
	BY_ULNK,
	BY_DEBE,
	BY_DEEN,
	BY_DEDU,
	BY_AFNUM,
	BY_DFNUM,
	BY_TERM,
	BY_NONE
};



static struct DEMDATA_FIELD osfields[]	= 
	{
		{	DEMDATA_FIELD("DETY","",	150,	180)	},
		{	DEMDATA_FIELD("ALOC","",	30,		75)	},
		{	DEMDATA_FIELD("AFNUM","",	60,		125)	},
		{	DEMDATA_FIELD("DFNUM","",	60,		125)	},
		{	DEMDATA_FIELD("DEBE","",	40,		100)	},
		{	DEMDATA_FIELD("DEEN","",	40,		100)	},
		{	DEMDATA_FIELD("DEDU","",	30,		75)	},
		{	DEMDATA_FIELD("FCT","",		50,		125)	},
		{	DEMDATA_FIELD("SERVICE","",	80,		200)	},
		{	DEMDATA_FIELD("PERMITS","",	50,		125)	},
		{	DEMDATA_FIELD("REGN","",	50,		140)	},
		{	DEMDATA_FIELD("BLT1","",	50,		60)	},
		{	DEMDATA_FIELD("CKI","",		80,		100)	},
		{	DEMDATA_FIELD("VAARR","",	50,		120)	},
		{	DEMDATA_FIELD("VADEP","",	50,		120)	},
		{	DEMDATA_FIELD("PSTA","",	30,		120)	},
		{	DEMDATA_FIELD("PSTD","",	30,		120)	},
		{	DEMDATA_FIELD("ACTYPE","",	30,		80)	},
		{	DEMDATA_FIELD("STAFF","",	30,		80)		},
		{	DEMDATA_FIELD("ULNK","",	80,		150)	},
		{	DEMDATA_FIELD("TIFA","",	70,		140)	},
		{	DEMDATA_FIELD("TIFD","",	70,		140)	},
		{	DEMDATA_FIELD("AIRPORT","",	30,		60)	},
		{	DEMDATA_FIELD("GATA","",	30,		120)	},
		{	DEMDATA_FIELD("GATD","",	30,		120)	},
		{	DEMDATA_FIELD("PAXF","",	60,		100)	},
		{	DEMDATA_FIELD("PAXB","",	60,		100)	},
		{	DEMDATA_FIELD("PAXE","",	60,		100)	},
		{   DEMDATA_FIELD("TERM","",    20,     40) }, //Singapore
		{   DEMDATA_FIELD("PNAM","",    150,     250) }, //Singapore
		{   DEMDATA_FIELD("PREM","",    350,    500) }, //Singapore
//		{	DEMDATA_FIELD("CONFF","",	50,		50)	},
//		{	DEMDATA_FIELD("CONFJ","",	50,		50)	},
//		{	DEMDATA_FIELD("CONFY","",	50,		50)	},
		{	DEMDATA_FIELD("SEAT","",	53,		159)},  //Singapore      
	};

const char *DemandTableViewer::omAllocGroupTypes[] =	{
	ALLOCUNITTYPE_GATEGROUP,
	ALLOCUNITTYPE_CICGROUP,
	ALLOCUNITTYPE_REGNGROUP,
	ALLOCUNITTYPE_PSTGROUP,
	NULL
	};


/////////////////////////////////////////////////////////////////////////////
// DemandTableViewer

DemandTableViewer::DemandTableViewer()
{ 
    SetViewerKey("DemandTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, DEMAND_NEW,	CString("DTVIEWER"), CString("Demand New"),	DemandTableCf);
    ogCCSDdx.Register(this, DEMAND_CHANGE,	CString("DTVIEWER"), CString("Demand Change"),	DemandTableCf);
    ogCCSDdx.Register(this, DEMAND_DELETE,	CString("DTVIEWER"), CString("Demand Delete"),	DemandTableCf);
    ogCCSDdx.Register(this, PRM_DEMAND_CHANGE,	CString("DTVIEWER"), CString("Prm Demand Change"),	DemandTableCf);
	ogCCSDdx.Register(this, JOD_NEW,		CString("DTVIEWER"), CString("Jod new"),		DemandTableCf);
	ogCCSDdx.Register(this, JOD_CHANGE,		CString("DTVIEWER"), CString("Jod changed"),	DemandTableCf);
	ogCCSDdx.Register(this, JOD_DELETE,		CString("DTVIEWER"), CString("Jod deleted"),	DemandTableCf);
	ogCCSDdx.Register(this, FLIGHT_CHANGE,	CString("DTVIEWER"), CString("Flight Change"),	DemandTableCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT,	CString("DTVIEWER"), CString("Flight Select"),	DemandTableCf);
	
	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime   = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);

	bmUseAllAlocs	 = false;
	bmUseAllDetys	 = false;
	bmUseAllRetys	 = false;
	bmCompressGroups = false;
	bmUseAllAirlines = false;
	bmUseAllRanks	 = false;
	bmUseAllServices = false;
	bmUseAllPermits  = false;
	bmUseAllNatures  = false;
	bmUseAllCics	 = false;
	bmUseAllBelt1s	 = false;

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = GetString(IDS_STRING61879);
		osfields[1].Name = GetString(IDS_STRING61880);
		osfields[2].Name = GetString(IDS_STRING61881);
		osfields[3].Name = GetString(IDS_STRING61882);
		osfields[4].Name = GetString(IDS_STRING61883);
		osfields[5].Name = GetString(IDS_STRING61884);
		osfields[6].Name = GetString(IDS_STRING61885);
		osfields[7].Name = GetString(IDS_STRING61886);
		osfields[8].Name = GetString(IDS_SERVICECODES);
		osfields[9].Name = GetString(IDS_STRING61887);
		osfields[10].Name = GetString(IDS_STRING61888);
		osfields[11].Name= GetString(IDS_DEMLIST_BLT1);
		osfields[12].Name= GetString(IDS_DEMLIST_CKI);
		osfields[13].Name= GetString(IDS_DEMLIST_VAARR);
		osfields[14].Name= GetString(IDS_DEMLIST_VADEP);
		osfields[15].Name= GetString(IDS_DEMLIST_PSTA);
		osfields[16].Name= GetString(IDS_DEMLIST_PSTD);
		osfields[17].Name= GetString(IDS_STRING61889);
		osfields[18].Name= GetString(IDS_STRING61890);
		osfields[19].Name= GetString(IDS_STRING61891);
		osfields[20].Name= GetString(IDS_STRING61896);
		osfields[21].Name= GetString(IDS_STRING61897);
		osfields[22].Name= GetString(IDS_DEMLIST_DES3);
		osfields[23].Name= GetString(IDS_DEMLIST_GATA);
		osfields[24].Name= GetString(IDS_DEMLIST_GATD);
		osfields[25].Name= GetString(IDS_DEMLIST_PAXF);
		osfields[26].Name= GetString(IDS_DEMLIST_PAXB);
		osfields[27].Name= GetString(IDS_DEMLIST_PAXE);
		osfields[28].Name= GetString(IDS_TERMINAL);
		osfields[29].Name= GetString(IDS_STRING33192);
		osfields[30].Name= GetString(IDS_STRING33193);
//		osfields[27].Name= "Conf F"; //GetString(IDS_DEMLIST_CONFF);
//		osfields[28].Name= "Conf J"; //GetString(IDS_DEMLIST_CONFJ);
//		osfields[29].Name= "Conf Y"; //GetString(IDS_DEMLIST_CONFY);IDS_STRING33192
		osfields[31].Name = GetString(IDS_DEM_SEAT);
	}

	char pclTmpText[512];
	char pclConfigPath[512];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(pcgAppName,"TIFA", "S,E,T,L,O",pclTmpText, sizeof pclTmpText, pclConfigPath);
	omTisa = pclTmpText;

    GetPrivateProfileString(pcgAppName,"TIFD", "S,E,O,A",pclTmpText, sizeof pclTmpText, pclConfigPath);
	omTisd = pclTmpText;

}

DemandTableViewer::~DemandTableViewer()
{
	TRACE("DemandTableViewer: DDX Unregistration \n");
    ogCCSDdx.UnRegister(this, NOTUSED);
	omTableFields.DeleteAll();
	omColumns.DeleteAll();
	DeleteAll();
}

void DemandTableViewer::Attach(CCSTable *popTable)
{
	pomTable = popTable;
}

void DemandTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void DemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.DETV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter();
    PrepareGrouping();
    DeleteAll();    

	EvaluateTableFields();
	MakeColumns();
    MakeLines();
	UpdateDisplay();
}

void DemandTableViewer::CompressGroups(bool blCompress)
{
	if (blCompress != bmCompressGroups)
	{
		bmCompressGroups = blCompress;
		if (bmCompressGroups)
		{
			CCSPtrArray<DEMDATA_LINE> olLinesToRemove;
			for (int i = 0; i < omLines.GetSize(); i++)
			{
				DEMDATA_LINE *prlLine = &omLines[i];
				if (prlLine && prlLine->IsTeamLine)
				{
					olLinesToRemove.Append(prlLine->TeamLines);					
					CompressGroup(prlLine);
				}
			}
			
			for (i = 0; i < olLinesToRemove.GetSize(); i++)
			{
				RemoveLine(&olLinesToRemove[i]);	// remove lines from display, but keep them in team list				
			}
		}
		else
		{
			CCSPtrArray<DEMDATA_LINE> olOldLines = omLines;

			for (int i = 0; i < olOldLines.GetSize(); i++)
			{
				DEMDATA_LINE *prlLine = &olOldLines[i];
				if (prlLine && prlLine->IsTeamLine)
				{
					ExpandGroup(prlLine);
				}
			}
		}

		UpdateDisplay(FALSE);
	}
}

void DemandTableViewer::CompressGroup(DEMDATA_LINE *prpGroup)
{
	if (!prpGroup || !prpGroup->IsTeamLine)
		return;

	prpGroup->Debe = prpGroup->OriDebe;
	prpGroup->Deen = prpGroup->OriDeen;
	prpGroup->Dedu = prpGroup->OriDedu;
	prpGroup->Staff= 1;
	prpGroup->Expanded = false;

	for (int i = 0; i < prpGroup->TeamLines.GetSize(); i++)
	{
		DEMDATA_LINE *prlLine = &prpGroup->TeamLines[i];
		if (prlLine)
		{
			// recalculate team leader line
			if (prlLine->Debe < prpGroup->Debe)
				prpGroup->Debe = prlLine->Debe;
			if (prlLine->Deen > prpGroup->Deen)
				prpGroup->Deen = prlLine->Deen;
			prpGroup->Dedu = prpGroup->Deen - prpGroup->Debe;
			prpGroup->Staff += prlLine->Staff;
		}
	}

	ChangeLine(prpGroup);
}

void DemandTableViewer::ExpandGroup(DEMDATA_LINE *prpGroup)
{
	if (!prpGroup || !prpGroup->IsTeamLine)
		return;

	for (int i = 0; i < prpGroup->TeamLines.GetSize(); i++)
	{
		DEMDATA_LINE *prlLine = &prpGroup->TeamLines[i];
		if (prlLine)
		{
			int ilLinenno = InsertLine(prlLine);			
		}
	}

	prpGroup->Debe = prpGroup->OriDebe;
	prpGroup->Deen = prpGroup->OriDeen;
	prpGroup->Dedu = prpGroup->OriDedu;
	prpGroup->Staff= 1;
	prpGroup->Expanded = true;

	ChangeLine(prpGroup);
}

int DemandTableViewer::RemoveLine(DEMDATA_LINE *prlLine)
{
	if (!prlLine)
		return -1;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		if (omLines[i].DemUrno == prlLine->DemUrno)
		{
			omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prlLine->DemUrno),NULL);
			if (pomTable)
				pomTable->DeleteTextLine(i);
			omLines.DeleteAt(i);
			return i;
		}
	}

	return -1;
}


int DemandTableViewer::ChangeLine(DEMDATA_LINE *prlLine,BOOL bpResort)
{
	if (!prlLine)
		return -1;

	// must we resort the changed line ?
	if (bpResort)
	{
		for (int i = 0; i < omLines.GetSize(); i++)
		{
			if (omLines[i].DemUrno == prlLine->DemUrno)
			{
//				CCSPtrArray<TABLE_COLUMN> rrlColumns;
//				for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
//				{
//					rrlColumns.NewAt(ilC,TABLE_COLUMN());
//				}

				Format(prlLine,omColumns);

				DEMDATA_LINE *prlDisplayedLine = &omLines[i];
				if (prlDisplayedLine != prlLine)
				{
					*prlDisplayedLine = *prlLine;
				}

				omLines.RemoveAt(i);				
				if (pomTable)
					pomTable->DeleteTextLine(i);
				int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
				for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
				{
					if (CompareDemand(prlLine, &omLines[ilLineno]) <= 0)
						break;  
				}
#else
				for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				{
					if (CompareDemand(prlLine, &omLines[ilLineno-1]) >= 0)
						break;  
				}
#endif

				omLines.InsertAt(ilLineno,prlDisplayedLine);
				if (pomTable)
				{
					pomTable->InsertTextLine(ilLineno,omColumns,&omLines[ilLineno]);
					if(IsFinalized(prlLine))
					{
						pomTable->SetTextLineColor(ilLineno, BLACK, GetBackgroundColorForPRM());
					}
					else
					{
						pomTable->SetTextLineColor(ilLineno, BLACK, omLines[ilLineno].Deactivated ? SILVER : WHITE);
					}
				}

//				omColumns.DeleteAll();
				return ilLineno;
			}
		}
	}
	else	// no resort neccessary
	{
		for (int i = 0; i < omLines.GetSize(); i++)
		{
			if (omLines[i].DemUrno == prlLine->DemUrno)
			{
//				CCSPtrArray<TABLE_COLUMN> omColumns;
//				if (pomTable)
//					pomTable->GetTextLineColumns(&omColumns,i);

				Format(prlLine,omColumns);

				DEMDATA_LINE *prlDisplayedLine = &omLines[i];
				if (prlDisplayedLine != prlLine)
				{
					*prlDisplayedLine = *prlLine;
				}

				if (pomTable)
					pomTable->ChangeTextLine(i,&omColumns,&omLines[i]);
				return i;
			}
		}
	}
	return -1;
}

BOOL DemandTableViewer::FindTeamLeaderOf(DEMANDDATA *prpDem,int &ripLineno)
{
	if (!prpDem || prpDem->Ulnk == 0)
		return FALSE;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// is it the team leader itself ?
		if (omLines[i].DemUrno == prpDem->Urno)
		{
			ripLineno = i;
			return TRUE;
		}

		// same group ?
		if (omLines[i].Ulnk == prpDem->Ulnk)
		{
			for (int j = 0; j < omLines[i].TeamLines.GetSize(); j++)			
			{
				if (omLines[i].TeamLines[j].DemUrno == prpDem->Urno)
				{
					ripLineno = i;
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL DemandTableViewer::FindTeamLeaderOf(long lpDemUrno,int &ripLineno)
{
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// is it the team leader itself ?
		if (omLines[i].DemUrno == lpDemUrno)
		{
			if (omLines[i].Ulnk == 0)
				return FALSE;
			else
			{
				ripLineno = i;
				return TRUE;
			}
		}

		// is this line a team line ?
		if (omLines[i].Ulnk != 0 && omLines[i].IsTeamLine)
		{
			for (int j = 0; j < omLines[i].TeamLines.GetSize(); j++)			
			{
				if (omLines[i].TeamLines[j].DemUrno == lpDemUrno)
				{
					ripLineno = i;
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// find team of a newly created demand
BOOL DemandTableViewer::FindTeamOf(DEMANDDATA *prpDem,int &ripLineno)
{
	if (!prpDem || prpDem->Ulnk == 0)
		return FALSE;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// only groups are interesting
		if (omLines[i].Ulnk == 0)
			continue;

		// only team leders are interesting
		if (!omLines[i].IsTeamLine)
			continue;

		if (omLines[i].Ulnk != prpDem->Ulnk)
			continue;

		if (strcmp(omLines[i].Obty,prpDem->Obty) != 0)
			continue;

		if (strcmp(omLines[i].Aloc,prpDem->Aloc) != 0)
			continue;

		DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(omLines[i].DemUrno);
		if (!prlTeamDem)
			continue;

		BOOL blSameGroup = prlTeamDem->Ouri == prpDem->Ouri;
		blSameGroup = blSameGroup && prlTeamDem->Ouro == prpDem->Ouro;

		// same group ?
		if (blSameGroup)
		{
			ripLineno = i;
			return TRUE;
		}
	}

	return FALSE;
}

void DemandTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

	imGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Aloc")
            imGroupBy = BY_ALOC;
        else if (olSortOrder[0] == "Obty")
            imGroupBy = BY_OBTY;
        else if (olSortOrder[0] == "Debe")
            imGroupBy = BY_DEBE;
        else if (olSortOrder[0] == "Deen")
            imGroupBy = BY_DEEN;
        else if (olSortOrder[0] == "Dedu")
            imGroupBy = BY_DEDU;
        else if (olSortOrder[0] == "Ulnk")
            imGroupBy = BY_ULNK;
        else if (olSortOrder[0] == "AFnum")
            imGroupBy = BY_AFNUM;
        else if (olSortOrder[0] == "DFnum")
            imGroupBy = BY_DFNUM;
		else if (olSortOrder[0] == "Term")
			imGroupBy = BY_TERM;
    }

	// we group by ULNK in any case
	if (blIsGrouped)
		imGroupBy = BY_ULNK;

}


void DemandTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Aloc", olFilterValues);
    omCMapForAloc.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllAlocs = true;
	else
	{
		bmUseAllAlocs = false;
		for (i = 0; i < n; i++)
		{
			omCMapForAloc[AlocFromAllocGroupType(olFilterValues[i])] = NULL;
		}
	}

	// delete all alids
	POSITION pos = omCMapForAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		CMapStringToPtr *polAllocGroup = NULL; 
		omCMapForAlids.GetNextAssoc(pos,olKey,(void *&)polAllocGroup);
		if (polAllocGroup)
			delete polAllocGroup;
	}
	omCMapForAlids.RemoveAll();
	bmUseAllAlids.RemoveAll();

	const char **pclAllocGroupType = omAllocGroupTypes;
	void *ptr;
	while(*pclAllocGroupType)
	{
		CString olAllocType = AlocFromAllocGroupType(*pclAllocGroupType);

		if (bmUseAllAlocs)
		{
			bmUseAllAlids[olAllocType] = (void *)1;
		}
		else if (omCMapForAloc.Lookup(olAllocType,ptr))
		{
			CMapStringToPtr *polAllocGroup = new CMapStringToPtr;
			omCMapForAlids[olAllocType] = polAllocGroup;
		    GetFilter(*pclAllocGroupType, olFilterValues);
		    n = olFilterValues.GetSize();
			if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
				bmUseAllAlids[olAllocType] = (void *)1;
			else
			{
				bmUseAllAlids[olAllocType] = (void *)0;

				CCSPtrArray<ALLOCUNIT> ropUnitList;
				for (i = 0; i < n; i++)
				{
					ogAllocData.GetUnitsByGroup(*pclAllocGroupType,olFilterValues[i],ropUnitList,false,false);
				}

				n = ropUnitList.GetSize();
				for (i = 0; i < n; i++)
				{
					polAllocGroup->SetAt(ropUnitList[i].Name,NULL);
				}
			}
		}
		else
		{
			bmUseAllAlids[olAllocType] = (void *)1;
		}

		++pclAllocGroupType;
	}

    GetFilter("Dety", olFilterValues);
    omCMapForDety.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllDetys = true;
	else
	{
		bmUseAllDetys = false;
		for (i = 0; i < n; i++)
		{
			omCMapForDety[ogDemandData.GetDemandTypeFromString(olFilterValues[i])] = NULL;
		}
	}

    GetFilter("Rety", olFilterValues);
    omCMapForRety.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllRetys = true;
	else
	{
		bmUseAllRetys = false;
		for (i = 0; i < n; i++)
		{
			omCMapForRety[olFilterValues[i]] = NULL;
		}
	}


    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllAirlines = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAirlines = true;
		else
		    for (i = 0; i < n; i++)
				omCMapForAlcd[olFilterValues[i]] = NULL;
	}

	GetFilter("Cic", olFilterValues);
	omCMapForCic.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllCics = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllCics = true;
		else
			for (i = 0; i < n; i++)
				omCMapForCic[olFilterValues[i]] = NULL;
	}

	GetFilter("Belt1", olFilterValues);
	omCMapForBelt1.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllBelt1s = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllBelt1s = true;
		else
			for (i = 0; i < n; i++)
				omCMapForBelt1[olFilterValues[i]] = NULL;
	}

    GetFilter("Dienstrang", olFilterValues);
    omCMapForRank.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllRanks = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllRanks = true;
		else
			for (i = 0; i < n; i++)
				omCMapForRank[olFilterValues[i]] = NULL;
	}

    GetFilter("Service", olFilterValues);
    omCMapForServices.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllServices = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllServices = true;
		else
			for (i = 0; i < n; i++)
				omCMapForServices[olFilterValues[i]] = NULL;
	}

    GetFilter("Permits", olFilterValues);
    omCMapForPermits.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPermits = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPermits = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPermits[olFilterValues[i]] = NULL;
	}

    GetFilter("Nature", olFilterValues);
    omCMapForNature.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllNatures = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllNatures = true;
		else
			for (i = 0; i < n; i++)
				omCMapForNature[olFilterValues[i]] = NULL;
	}

    GetFilter("Airport", olFilterValues);
    omCMapForAirport.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllAirports = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAirports = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAirport[olFilterValues[i]] = NULL;
	}
}

void DemandTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

	bmSortDemStartAscending = GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	bmSortDemEndAscending = GetUserData("DEMENDORDER") == "YES" ? true : false;

	bmSortDemTerminalAscending = GetUserData("DEMTERMINALORDER") == "YES" ? true : false;

    omSortOrder.RemoveAll();


    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Aloc")
            ilSortOrderEnumeratedValue = BY_ALOC;
        else if (olSortOrder[i] == "Obty")
            ilSortOrderEnumeratedValue = BY_OBTY;
        else if (olSortOrder[i] == "Debe")
            ilSortOrderEnumeratedValue = BY_DEBE;
        else if (olSortOrder[i] == "Deen")
            ilSortOrderEnumeratedValue = BY_DEEN;
        else if (olSortOrder[i] == "Dedu")
            ilSortOrderEnumeratedValue = BY_DEDU;
        else if (olSortOrder[i] == "AFnum")
            ilSortOrderEnumeratedValue = BY_AFNUM;
        else if (olSortOrder[i] == "DFnum")
            ilSortOrderEnumeratedValue = BY_DFNUM;
		else if (olSortOrder[i] == "Term")
			ilSortOrderEnumeratedValue = BY_TERM;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }

	omSortOrder.Add(BY_ULNK);	// main sort order is grouping
}

int DemandTableViewer::CompareDemand(DEMDATA_LINE *prpDem1, DEMDATA_LINE *prpDem2)
{
	CCS_TRY;
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_ALOC:
            ilCompareResult = strcmp(prpDem1->Aloc,prpDem2->Aloc);
            break;
        case BY_OBTY:
            ilCompareResult = strcmp(prpDem1->Obty,prpDem2->Obty);
            break;
        case BY_DEBE:
			ilCompareResult = (bmSortDemStartAscending) ? 
				prpDem1->Debe.GetTime() - prpDem2->Debe.GetTime() :
				prpDem2->Debe.GetTime() - prpDem1->Debe.GetTime();
            break;
        case BY_DEEN:
			ilCompareResult = (bmSortDemEndAscending) ? 
				prpDem1->Deen.GetTime() - prpDem2->Deen.GetTime() :
				prpDem2->Deen.GetTime() - prpDem1->Deen.GetTime();
            break;
        case BY_DEDU:
            ilCompareResult = (prpDem1->Dedu == prpDem2->Dedu)? 0:
                (prpDem1->Dedu > prpDem2->Dedu)? 1: -1;
            break;
        case BY_AFNUM:
            ilCompareResult = strcmp(prpDem1->AFnum,prpDem2->AFnum);
            break;
        case BY_DFNUM:
            ilCompareResult = strcmp(prpDem1->DFnum,prpDem2->DFnum);
            break;
        case BY_ULNK:
            ilCompareResult = (prpDem1->Ulnk == prpDem2->Ulnk)? 0:
                (prpDem1->Ulnk > prpDem2->Ulnk)? 1: -1;
            break;
		case BY_TERM:
			ilCompareResult = bmSortDemTerminalAscending ?
				CompareTerminalNumber(prpDem1,prpDem2) :
			    CompareTerminalNumber(prpDem2,prpDem1);
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }
	CCS_CATCH_ALL
    return 0;   
}


BOOL DemandTableViewer::IsSameGroup(DEMDATA_LINE *prpDem1,DEMDATA_LINE *prpDem2)
{
    // Compare in the sort order, from the outermost to the innermost
    switch (imGroupBy)
    {
    case BY_ALOC:
        return strcmp(prpDem1->Aloc,prpDem2->Aloc) == 0;
    case BY_OBTY:
        return strcmp(prpDem1->Obty,prpDem2->Obty) == 0;
    case BY_DEBE:
        return (prpDem1->Debe == prpDem2->Debe);
    case BY_DEEN:
        return (prpDem1->Deen == prpDem2->Deen);
    case BY_DEDU:
        return (prpDem1->Dedu == prpDem2->Dedu);
    case BY_AFNUM:
        return strcmp(prpDem1->AFnum,prpDem2->AFnum) == 0;
    case BY_DFNUM:
        return strcmp(prpDem1->DFnum,prpDem2->DFnum) == 0;
    case BY_ULNK:
		if (prpDem1->Dety[0] == prpDem2->Dety[0])
		{
			if (prpDem1->Dety[0] == '0' || prpDem1->Dety[0] == '1')
			{
				if (strcmp(prpDem1->AFnum,prpDem2->AFnum) == 0)
				{
					if (!prpDem1->Ulnk)
						return FALSE;

					if (!prpDem2->Ulnk)
						return FALSE;

					if (strcmp(prpDem1->Obty,prpDem2->Obty) != 0)
						return FALSE;

					if (strcmp(prpDem1->Aloc,prpDem2->Aloc) != 0)
						return FALSE;

					return (prpDem1->Ulnk == prpDem2->Ulnk);
				}
				else
					return FALSE;
			}
			else if (prpDem1->Dety[0] == '2')
			{
				if (strcmp(prpDem1->DFnum,prpDem2->DFnum) == 0)
				{
					if (!prpDem1->Ulnk)
						return FALSE;

					if (!prpDem2->Ulnk)
						return FALSE;

					if (strcmp(prpDem1->Obty,prpDem2->Obty) != 0)
						return FALSE;

					if (strcmp(prpDem1->Aloc,prpDem2->Aloc) != 0)
						return FALSE;

					return (prpDem1->Ulnk == prpDem2->Ulnk);
				}
				else
					return FALSE;
			}
			else
				return FALSE;
		}
		else
			return FALSE;

	case BY_TERM:
		return CompareTerminalNumber(prpDem1,prpDem2) == 0;
    }

    return TRUE;    // assume there's no grouping at all
}


BOOL DemandTableViewer::IsPassFilter(DEMDATA_LINE *prpLine)
{
    void *p;
    BOOL blIsAlocOk = bmUseAllAlocs ? true : omCMapForAloc.Lookup(CString(prpLine->Aloc), p);
    BOOL blIsBelt1Ok = bmUseAllBelt1s ? true : omCMapForBelt1.Lookup(CString(prpLine->Blt1), p);
    BOOL blIsDetyOk = bmUseAllDetys ? true : prpLine->Dety[0] == '\0' ? omCMapForDety.Lookup(CString("7"),p) : omCMapForDety.Lookup(CString(prpLine->Dety), p);
	BOOL blIsAlcdOk = bmUseAllAirlines ? true : (ogDemandData.IsFlightDependingDemand(prpLine->Dety) ? omCMapForAlcd.Lookup(prpLine->Alc2,p) : true);
	BOOL blIsAirportOk = bmUseAllAirports ? true : (ogDemandData.IsFlightDependingDemand(prpLine->Dety) ? omCMapForAirport.Lookup(prpLine->Dest,p) : true);
    BOOL blIsRetyOk = bmUseAllRetys ? true : omCMapForRety.Lookup(prpLine->Rety, p);
	BOOL blIsTimeOk = (omStartTime <= prpLine->Deen && prpLine->Debe <= omEndTime);
	BOOL blIsAlidOk = false;

	if (bmUseAllAlids.Lookup(prpLine->Aloc,p) && p)
		blIsAlidOk = true;
	else
	{
		CMapStringToPtr *polMap;
		if (omCMapForAlids.Lookup(prpLine->Aloc,(void *&)polMap) && polMap != NULL)
		{
			if (strcmp(prpLine->Obty,"FID") == 0)
				blIsAlidOk = true;
			else
				blIsAlidOk = polMap->Lookup(prpLine->Alid,p);
		}		
	}

    BOOL blIsRankOk = bmUseAllRanks;
	CStringArray olRanks;
	ogBasicData.ExtractItemList(prpLine->Fcco,&olRanks,',');
	int ilNumRanks = olRanks.GetSize();
	for (int ilRank = 0; !blIsRankOk && ilRank < ilNumRanks;ilRank++)
	{
		blIsRankOk = omCMapForRank.Lookup(olRanks[ilRank], p);
	}

    BOOL blIsServiceOk = bmUseAllServices;
	CStringArray olServices;
	ogBasicData.ExtractItemList(prpLine->Service,&olServices,',');
	int ilNumServices = olServices.GetSize();
	for (int ilService = 0; !blIsServiceOk && ilService < ilNumServices;ilService++)
	{
		blIsServiceOk = omCMapForServices.Lookup(olServices[ilService], p);
	}


	BOOL blIsCicOk = bmUseAllCics;
	if(!blIsCicOk)
	{
		CString olCic;
		int ilNumCounters = prpLine->CcaList.GetSize();
		for (int ilCic = 0; ilCic < ilNumCounters;ilCic++)
		{
			olCic = prpLine->CcaList[ilCic].Cnam;
			if((blIsCicOk = omCMapForCic.Lookup(olCic, p)) == TRUE)
				break;
		}
	}

	BOOL blIsPermitOk = bmUseAllPermits;

	CStringArray olPermits;
	ogBasicData.ExtractItemList(prpLine->Permits,&olPermits,',');
	int ilNumPermits = olPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(olPermits[ilPermit], p);
	}

    BOOL blIsNatureOk = bmUseAllNatures;
	if (strcmp(prpLine->Obty,"FID") == 0)
	{
		blIsNatureOk = true;
	}
	else if (!blIsNatureOk)
	{
		if (!prpLine->VaArr.IsEmpty())
			blIsNatureOk = omCMapForNature.Lookup(prpLine->VaArr,p);

		if (!blIsNatureOk)
		{
			if (!prpLine->VaDep.IsEmpty())
				blIsNatureOk = omCMapForNature.Lookup(prpLine->VaDep,p);
		}
	}

	BOOL blIsTerminalCorrect = TRUE; //Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		blIsTerminalCorrect = IsTerminalCorrect(prpLine);
	}

    return (blIsAlocOk && blIsAlidOk && blIsDetyOk && blIsRetyOk && blIsTimeOk && blIsAlcdOk && blIsRankOk && blIsServiceOk && blIsPermitOk && blIsNatureOk && blIsCicOk && blIsBelt1Ok && blIsAirportOk && blIsTerminalCorrect); //Singapore
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void DemandTableViewer::MakeLines()
{
	int ilNumDemands = ogDemandData.omData.GetSize();
	for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		DEMANDDATA *prlDem = &ogDemandData.omData[ilDemand];
		// check, if personnel demand
//		if (strlen(prlDem->Rety) <= 0 || strcmp(prlDem->Rety,PERSONNEL_DEMAND) == 0)
		{
			// check, if demand is unresolved
			void *p;
			if (omStartTime <= prlDem->Deen && prlDem->Debe <= omEndTime && !ogJodData.DemandHasJobs(prlDem->Urno) &&
				!omMapUrnoToLine.Lookup(reinterpret_cast<void *>(prlDem->Urno),p))
			{
				if (prlDem->Ulnk == 0)
				{
					MakeLine(prlDem);
				}
				else
				{
					CCSPtrArray<DEMANDDATA> rolTeamDemands;
					ogDemandData.GetTeamDemands(prlDem,rolTeamDemands);
					for (int i = rolTeamDemands.GetSize() - 1; i >= 0; i--)
					{
						void *p;
						if ((omMapUrnoToLine.Lookup(reinterpret_cast<void *>(rolTeamDemands[i].Urno),p) && p != NULL) || ogJodData.DemandHasJobs(rolTeamDemands[i].Urno))
							rolTeamDemands.RemoveAt(i);
					}

					if (rolTeamDemands.GetSize() > 1)
						MakeLine(rolTeamDemands);
					else
						MakeLine(prlDem);
				}
			}
		}
	}
}

void DemandTableViewer::MakeLine(DEMANDDATA *prpDemand,DEMDATA_LINE& opLine)
{
	strcpy(opLine.Aloc,prpDemand->Aloc);
	strcpy(opLine.Alid,prpDemand->Alid);
	strcpy(opLine.Dety,prpDemand->Dety);
	opLine.Rety = ogDemandData.GetStringForResourceType(*prpDemand);
	opLine.Debe = prpDemand->Debe;
	opLine.Deen = prpDemand->Deen;
	opLine.Dedu = prpDemand->Deen - prpDemand->Debe;
	opLine.OriDebe = prpDemand->Debe;
	opLine.OriDeen = prpDemand->Deen;
	opLine.OriDedu = prpDemand->Deen - prpDemand->Debe;
	opLine.Ulnk = prpDemand->Ulnk;
	opLine.DemUrno = prpDemand->Urno;
	opLine.Deactivated = ogDemandData.DemandIsDeactivated(prpDemand);
	strcpy(opLine.Obty,prpDemand->Obty);
	opLine.Staff= 1;
	opLine.Term = GetTerminalNumber(prpDemand); //Singapore

	
		
	if (prpDemand->Uprm != 0L)
	{
		DPXDATA *prlDpxData;
		prlDpxData = ogDpxData.GetDpxByUrno(prpDemand->Uprm);

		if(prlDpxData != NULL)
		{
			opLine.dpx = prlDpxData;
			opLine.Pnam = CString(prlDpxData->Name);
			opLine.Prem = CString(prlDpxData->Rema);
		}
		else
		{
			opLine.dpx = new DPXDATA();
			opLine.dpx->Urno = 0;
		}
	}
	else
	{
		opLine.dpx = new DPXDATA();
		opLine.dpx->Urno = 0;
	}

	RUDDATA *prlRudData = ogRudData.GetRudByUrno(prpDemand->Urud);
	opLine.Service.Empty();
	if(prlRudData != NULL)
	{
		SERDATA *prlSerData = ogSerData.GetSerByUrno(prlRudData->Ughs);
		if(prlSerData != NULL)
			opLine.Service = CString(prlSerData->Seco);
	}

	if (strcmp(prpDemand->Obty,"FID") == 0)
	{
		ACRDATA *polAcr = ogAcrData.GetAcrByUrno(prpDemand->Uref);
		if (polAcr)
		{
			strcpy(opLine.Act3,"");
			strcpy(opLine.Regn,polAcr->Regn);
		}
		else
		{
			strcpy(opLine.Act3,"");
			strcpy(opLine.Regn,"");
		}
	}
	else
	{
	    FLIGHTDATA *prlFlight;
		prlFlight = ogFlightData.GetFlightByUrno(prpDemand->Ouri);
		if (prlFlight)
		{
			strcpy(opLine.Act3,prlFlight->Act3);
			strcpy(opLine.Alc2,ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3));
			strcpy(opLine.Dest,ogAptData.FormatAptString(prlFlight->Des3,prlFlight->Des4));
			strcpy(opLine.AFnum,prlFlight->Fnum);
			strcpy(opLine.Regn,prlFlight->Regn);
			EvaluateTime(prlFlight,opLine);

			opLine.Blt1 = prlFlight->Blt1;
			opLine.VaArr = prlFlight->Ttyp;
			opLine.Psta = prlFlight->Psta;
			opLine.Gata = prlFlight->Gta1;

			if (prpDemand->Dety[0] == INBOUND_DEMAND)
			{
				CString olAlid;
				if (strcmp(opLine.Aloc,ALLOCUNITTYPE_GATE) == 0)
					olAlid = ogFlightData.GetGate(prlFlight);
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_PST) == 0)
					olAlid = ogFlightData.GetPosition(prlFlight);
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_CIC) == 0)
					olAlid = "";
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_REGN) == 0)
					olAlid = "";
				strncpy(opLine.Alid,olAlid,sizeof(opLine.Alid));
				opLine.Alid[sizeof(opLine.Alid)-1] = '\0';
			}
		}

		prlFlight = ogFlightData.GetFlightByUrno(prpDemand->Ouro);
		if (prlFlight)
		{
			strcpy(opLine.Act3,prlFlight->Act3);
			strcpy(opLine.Alc2,ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3));
			strcpy(opLine.Dest,ogAptData.FormatAptString(prlFlight->Des3,prlFlight->Des4));
			strcpy(opLine.DFnum,prlFlight->Fnum);
			strcpy(opLine.Regn,prlFlight->Regn);
			EvaluateTime(prlFlight,opLine);

			opLine.VaDep = prlFlight->Ttyp;
			opLine.Pstd = prlFlight->Pstd;
			opLine.Gatd = prlFlight->Gtd1;

			if (prpDemand->Dety[0] != INBOUND_DEMAND)
			{
				if(ogCcaData.GetCcaListByUaft(prlFlight->Urno, opLine.CcaList) > 0)
				{
					opLine.Cki = ogCcaData.GetCounterRangeForFlight(prlFlight->Urno); // return range og checkin counters for the flight eg. "321-323"
				}

				CString olAlid;
				if (strcmp(opLine.Aloc,ALLOCUNITTYPE_GATE) == 0)
					olAlid = ogFlightData.GetGate(prlFlight);
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_PST) == 0)
					olAlid = ogFlightData.GetPosition(prlFlight);
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_CIC) == 0)
					olAlid = "";
				else if (strcmp(opLine.Aloc,ALLOCUNITTYPE_REGN) == 0)
					olAlid = "";
				strncpy(opLine.Alid,olAlid,sizeof(opLine.Alid));
				opLine.Alid[sizeof(opLine.Alid)-1] = '\0';

				PAXDATA *prlPax = ogPaxData.GetPaxByAftu(prlFlight->Urno);
				if(prlPax != NULL)
				{
					opLine.Paxf.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_FIRST), ogPaxData.GetBookedPassengers(prlPax,PAX_FIRST));
					opLine.Paxb.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_BUSINESS), ogPaxData.GetBookedPassengers(prlPax,PAX_BUSINESS));
					opLine.Paxe.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_ECONOMY), ogPaxData.GetBookedPassengers(prlPax,PAX_ECONOMY));
				}
//				opLine.ConfF.Format("%d", ogLoaData.GetValu(prlFlight->Urno, LOA_PASSENGERS, LOA_FIRST));
//				opLine.ConfJ.Format("%d", ogLoaData.GetValu(prlFlight->Urno, LOA_PASSENGERS, LOA_BUSINESS));
//				opLine.ConfY.Format("%d", ogLoaData.GetValu(prlFlight->Urno, LOA_PASSENGERS, LOA_ECONOMY));
			}
		}

	}

	if(ogRudData.DemandIsOfType(prpDemand->Urud, PERSONNELDEMANDS))
	{
		RPFDATA *prlDemFct = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
		if (prlDemFct)
		{
			if (strcmp(prlDemFct->Fcco,"") == 0)
			{
				opLine.Fcco = "";
				// group of optional functions
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemFct->Upfc,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
					if(prlPfc != NULL)
					{
						if (!opLine.Fcco.IsEmpty())
							opLine.Fcco += ',';
						opLine.Fcco += prlPfc->Fctc;
					}
				}

			}
			else
				opLine.Fcco = prlDemFct->Fcco;
		}
		
		// lli: need to check both urud and udgr for permits
		CCSPtrArray<RPQDATA> olDemandPermits;
		CCSPtrArray<RPQDATA> olDemandPermits2;
		ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
		olDemandPermits.Append(olDemandPermits2);
		for (int ilC = 0; ilC < olDemandPermits.GetSize(); ilC++)
		{
			RPQDATA *prlDemandPermit = &olDemandPermits[ilC];
			if (prlDemandPermit)
			{
				if (strcmp(prlDemandPermit->Quco,"") == 0)
				{
					opLine.Permits = "";
					// this is a header for a group of permits, one of which the emp must have
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
						if(prlPer != NULL)
						{
							if (!opLine.Permits.IsEmpty())
								opLine.Permits += ',';
							opLine.Permits += prlPer->Prmc;
						}
					}
				}
				else	// single permit
					opLine.Permits = prlDemandPermit->Quco;
			}
		}
	}
	else if(ogRudData.DemandIsOfType(prpDemand->Urud, EQUIPMENTDEMANDS))
	{
		REQDATA *prlReq = ogReqData.GetReqByUrud(prpDemand->Urud);
		if(prlReq != NULL)
		{
			EQUDATA *prlEqu = NULL;

			if(strlen(prlReq->Eqco) > 0)
			{
				// single piece of equipment required for this demand
				if((prlEqu = ogEquData.GetEquByUrno(prlReq->Uequ)) != NULL)
				{
					opLine.Fcco = prlEqu->Enam;
				}
			}
			else
			{
				SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlReq->Uequ);
				if(prlSgr != NULL)
				{
					opLine.Fcco = prlSgr->Grpn;
				}
			}
		}
	}
}

void DemandTableViewer::MakeLine(DEMANDDATA *prpDemand)
{
	DEMDATA_LINE olLine ;

	MakeLine(prpDemand,olLine);

	if (IsPassFilter(&olLine))
	{
		int ilLineno = CreateLine(&olLine);
	}
}

void DemandTableViewer::MakeLine(CCSPtrArray<DEMANDDATA>& ropTeamDemands)
{
	DEMDATA_LINE olTeamLine;
	for (int i = 0; i < ropTeamDemands.GetSize(); i++)
	{
		DEMDATA_LINE olLine;
		MakeLine(&ropTeamDemands[i],olLine);
		if (!IsPassFilter(&olLine))
			continue;
		if (olTeamLine.DemUrno == 0)
		{
			olTeamLine = olLine;
			olTeamLine.IsTeamLine = true;
			olTeamLine.Expanded = !bmCompressGroups;
		}
		else
		{
			// add group demand to team leader
			olTeamLine.TeamLines.New(olLine);
			// add demand to map to prevent from adding it multiple times
			omMapUrnoToLine.SetAt(reinterpret_cast<void *>(ropTeamDemands[i].Urno),NULL);

			if (bmCompressGroups)
			{
				// recalculate team leader line
				if (olLine.Debe < olTeamLine.Debe)
					olTeamLine.Debe = olLine.Debe;
				if (olLine.Deen > olTeamLine.Deen)
					olTeamLine.Deen = olLine.Deen;
				olTeamLine.Dedu = olTeamLine.Deen - olTeamLine.Debe;
				olTeamLine.Staff += olLine.Staff;
			}
			else
			{
				int ilLineno = CreateLine(&olLine);
			}
		}
	}

	if (olTeamLine.DemUrno != 0)
	{
		int ilLineno = CreateLine(&olTeamLine);
	}
}

/////////////////////////////////////////////////////////////////////////////
// DemandTableViewer - DEMDATA_LINEDATA array maintenance

void DemandTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);

	omMapUrnoToLine.RemoveAll();

}

int DemandTableViewer::CreateLine(DEMDATA_LINE *prpDem)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareDemand(prpDem, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDemand(prpDem, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		if(IsPassFilter(prpDem) == TRUE)
		{
    omLines.NewAt(ilLineno, *prpDem);
		}
		else
		{
			return -1;
		}
	}
	else
	{
		omLines.NewAt(ilLineno, *prpDem);
	}
	DEMDATA_LINE *prlDem = &omLines[ilLineno];
	if (prlDem)
		omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDem->DemUrno),prlDem);
    return ilLineno;
}

void DemandTableViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

int DemandTableViewer::InsertLine(DEMDATA_LINE *prpDem)
{
	int ilLineno = CreateLine(prpDem);
	if (ilLineno >= 0)
	{
		Format(&omLines[ilLineno],omColumns);
		if (strcmp(omLines[ilLineno].Obty,"AFT") == 0)
		{
			switch(omLines[ilLineno].Dety[0])
			{
			case INBOUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					omColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
				if (TableFieldIndex("DFNUM") >= 0)
				omColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
			break;
			case OUTBOUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					omColumns[TableFieldIndex("AFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				if (TableFieldIndex("DFNUM") >= 0)
					omColumns[TableFieldIndex("DFNUM")].TextColor = RGB(0,0,0);
			break;
			case TURNAROUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					omColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
				if (TableFieldIndex("DFNUM") >= 0)
					omColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
			break;
			}
		}

		if (pomTable)
		{
			pomTable->InsertTextLine(ilLineno,omColumns, &omLines[ilLineno]);
			
			if(IsFinalized(prpDem))
			{
				pomTable->SetTextLineColor(ilLineno, BLACK, GetBackgroundColorForPRM());
			}
			else
			{
				pomTable->SetTextLineColor(ilLineno, BLACK, omLines[ilLineno].Deactivated ? SILVER : WHITE);
			}
		}
	}

	return ilLineno;
}

void DemandTableViewer::MakeColumns()
{
	omColumns.DeleteAll();
	int ilNumFields = omTableFields.GetSize();
	for (int ilC = 0; ilC < ilNumFields; ilC++)
	{
		omColumns.NewAt(ilC,TABLE_COLUMN());
	}
}

/////////////////////////////////////////////////////////////////////////////
// DemandTableViewer - display drawing routine

void DemandTableViewer::UpdateDisplay(BOOL bEraseAll)
{
	if (!pomTable)
		return;

	if (bEraseAll)
	{
		CCSPtrArray<TABLE_HEADER_COLUMN> header;
//		CCSPtrArray<TABLE_COLUMN> columns;
		for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
		{
			header.NewAt(ilC,TABLE_HEADER_COLUMN());
			header[ilC].Text   = omTableFields[ilC].Name;
			header[ilC].Length = omTableFields[ilC].Length;
//			columns.NewAt(ilC,TABLE_COLUMN());
		}


		pomTable->SetHeaderFields(header);

		pomTable->ResetContent();

		int nLineCount = 0;
		nLineCount = omLines.GetSize();


		for (int i = 0; i < nLineCount; i++) 
		{
			Format(&omLines[i],omColumns);
			// Display grouping effect
#if	0
			if (i == omLines.GetSize()-1 || !IsSameGroup(&omLines[i], &omLines[i+1]))
			{
	//            pomTable->SetTextLineSeparator(i, ST_THICK);
				for (int j = 0; j < omColumns.GetSize(); j++)
				{
					omColumns[j].SeparatorType = SEPA_THICK;
					omColumns[j].HorizontalSeparator = SEPA_THICK;
				}
			}
#endif
			if (strcmp(omLines[i].Obty,"AFT") == 0)
			{
				switch(omLines[i].Dety[0])
				{
				case INBOUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						omColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
					if (TableFieldIndex("DFNUM") >= 0)
						omColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				break;
				case OUTBOUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						omColumns[TableFieldIndex("AFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
					if (TableFieldIndex("DFNUM") >= 0)
						omColumns[TableFieldIndex("DFNUM")].TextColor = RGB(0,0,0);
				break;
				case TURNAROUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						omColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
					if (TableFieldIndex("DFNUM") >= 0)
						omColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				break;
				}
			}

			pomTable->AddTextLine(omColumns, &omLines[i]);
		}


		pomTable->DisplayTable();
		//columns.DeleteAll();
		header.DeleteAll();

		for (i = 0; i < nLineCount; i++) 
		{
			DEMDATA_LINE *prlLine = &omLines[i];
			// Display grouping effect
			if (i == omLines.GetSize()-1 || !IsSameGroup(prlLine, &omLines[i+1]))
			{
				pomTable->SetTextLineSeparator(i, SEPA_THICK);
			}

			//Highlight finalized PRM request list. 
			
			if(IsFinalized(prlLine))
			{
				pomTable->SetTextLineColor(i, BLACK, GetBackgroundColorForPRM());
			}
			else
			{
				pomTable->SetTextLineColor(i, BLACK, prlLine->Deactivated ? SILVER : WHITE);
			}
		}

		pomTable->DisplayTable();
	}
	else
	{
		int nLineCount = omLines.GetSize();

		for (int i = 0; i < nLineCount; i++) 
		{
			DEMDATA_LINE *prlLine = &omLines[i];
			// Display grouping effect
			if (i == omLines.GetSize()-1 || !IsSameGroup(prlLine, &omLines[i+1]))
			{
				pomTable->SetTextLineSeparator(i, SEPA_THICK);
			}
			else
			{
				pomTable->SetTextLineSeparator(i, SEPA_NORMAL);
			}
			
			if(IsFinalized(prlLine))
			{
				pomTable->SetTextLineColor(i, BLACK, GetBackgroundColorForPRM());
			}
			else
			{
				pomTable->SetTextLineColor(i, BLACK, prlLine->Deactivated ? SILVER : WHITE);
			}


		}

		pomTable->DisplayTable();
	}

	
}

void DemandTableViewer::Format(DEMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	int ilIndex = TableFieldIndex("DETY");
	if (ilIndex >= 0)
	{
		DEMANDDATA rlDemand;
		strcpy(rlDemand.Dety,prpLine->Dety);
		rrpColumns[ilIndex].Text = ogDemandData.GetStringForDemandType(rlDemand) + CString("/") + prpLine->Rety;
	}

	ilIndex = TableFieldIndex("ALOC");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Aloc;

	ilIndex = TableFieldIndex("AFNUM");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->AFnum;

	ilIndex = TableFieldIndex("DFNUM");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->DFnum;

	ilIndex = TableFieldIndex("DEBE");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Debe, omStartTime);

	ilIndex = TableFieldIndex("DEEN");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Deen, omStartTime);

	ilIndex = TableFieldIndex("DEDU");
	if (ilIndex >= 0)
	{
		CString olMinutes;
		olMinutes.Format("%ld",(long)prpLine->Dedu.GetTotalMinutes());
		rrpColumns[ilIndex].Text = olMinutes;
	}

	ilIndex = TableFieldIndex("FCT");
	if (ilIndex >= 0)
	{
		if (prpLine->IsTeamLine && bmCompressGroups)
			rrpColumns[ilIndex].Text = "";
		else
			rrpColumns[ilIndex].Text = prpLine->Fcco;
	}

	ilIndex = TableFieldIndex("SERVICE");
	if (ilIndex >= 0)
	{
		if (prpLine->IsTeamLine && bmCompressGroups)
			rrpColumns[ilIndex].Text = "";
		else
			rrpColumns[ilIndex].Text = prpLine->Service;
	}

	ilIndex = TableFieldIndex("PERMITS");
	if (ilIndex >= 0)
	{
		if (prpLine->IsTeamLine && bmCompressGroups)
			rrpColumns[ilIndex].Text = "";
		else
			rrpColumns[ilIndex].Text = prpLine->Permits;
	}

	ilIndex = TableFieldIndex("REGN");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Regn;

	ilIndex = TableFieldIndex("ACTYPE");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Act3;

	ilIndex = TableFieldIndex("VAARR");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->VaArr;

	ilIndex = TableFieldIndex("AIRPORT");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Dest;

	ilIndex = TableFieldIndex("BLT1");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Blt1;

	ilIndex = TableFieldIndex("PSTA");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Psta;

	ilIndex = TableFieldIndex("GATA");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Gata;

	ilIndex = TableFieldIndex("GATD");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Gatd;

	ilIndex = TableFieldIndex("PAXF");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Paxf;

	ilIndex = TableFieldIndex("PAXB");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Paxb;

	ilIndex = TableFieldIndex("PAXE");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Paxe;

//	ilIndex = TableFieldIndex("CONFF");
//	if (ilIndex >= 0)
//		rrpColumns[ilIndex].Text = prpLine->ConfF;
//
//	ilIndex = TableFieldIndex("CONFJ");
//	if (ilIndex >= 0)
//		rrpColumns[ilIndex].Text = prpLine->ConfJ;
//
//	ilIndex = TableFieldIndex("CONFY");
//	if (ilIndex >= 0)
//		rrpColumns[ilIndex].Text = prpLine->ConfY;

	ilIndex = TableFieldIndex("CKI");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Cki;

	ilIndex = TableFieldIndex("PSTD");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Pstd;

	ilIndex = TableFieldIndex("VADEP");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->VaDep;

	ilIndex = TableFieldIndex("ULNK");
	if (ilIndex >= 0)
	{
		CString szUlnk;
		szUlnk.Format("%ld",prpLine->Ulnk);
		rrpColumns[ilIndex].Text = szUlnk;
	}

	ilIndex = TableFieldIndex("STAFF");
	if (ilIndex >= 0)
	{
		CString szStaff;
		szStaff.Format("%d",prpLine->Staff);
		rrpColumns[ilIndex].Text = szStaff;
	}

	ilIndex = TableFieldIndex("TIFA");
	if (ilIndex >= 0)
	{
		if (prpLine->Tifa != TIMENULL)
		{
			CString szState;
			if (strlen(prpLine->Tisa))
				szState.Format("(%s)",(const char *)GetTifName(prpLine->Tisa));
			rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Tifa, omStartTime) + szState;
		}
		else
			rrpColumns[ilIndex].Text = "";
	}

	ilIndex = TableFieldIndex("TIFD");
	if (ilIndex >= 0)
	{
		if (prpLine->Tifd != TIMENULL)
		{
			CString szState;
			if (strlen(prpLine->Tisd))
				szState.Format("(%s)",(const char *)GetTifName(prpLine->Tisd));
			rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Tifd, omStartTime) + szState;
		}
		else
			rrpColumns[ilIndex].Text = "";
	}

	ilIndex = TableFieldIndex("TERM");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = GetTerminalNumber(prpLine);
	}

	ilIndex = TableFieldIndex("PNAM");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = prpLine->Pnam;
	}

	ilIndex = TableFieldIndex("PREM");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = prpLine->Prem;
	}

	ilIndex = TableFieldIndex("SEAT");
	if(ilIndex >= 0)
	{
		if(prpLine->dpx->Urno>0)
		{
			rrpColumns[ilIndex].Text = prpLine->dpx->Seat;
		}
		else
		{
			rrpColumns[ilIndex].Text = "";
		}
	}

}

void DemandTableViewer::DemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    DemandTableViewer *polViewer = (DemandTableViewer *)popInstance;
	if(ipDDXType == JOD_NEW || ipDDXType == JOD_CHANGE || ipDDXType == JOD_DELETE)
	{
		polViewer->ProcessJodChanges(ipDDXType,(JODDATA *)vpDataPointer);
	}
	else if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_NEW)
	{
		polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == PRM_DEMAND_CHANGE)
	{
		polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
	}	
	else if (ipDDXType == DEMAND_DELETE)// || ipDDXType == DPX_DELETE)
	{
		polViewer->ProcessDemandDelete((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == FLIGHT_CHANGE || ipDDXType == FLIGHT_SELECT)
	{
		polViewer->ProcessFlightChanges(ipDDXType,(FLIGHTDATA *)vpDataPointer);
	}
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{	
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(DemandTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}


void DemandTableViewer::ProcessJodChanges(int ipDDXType, JODDATA *prpJod)
{
    if (ipDDXType == JOD_NEW)
	{
        ProcessJodNew(prpJod);
	}
	else if (ipDDXType == JOD_CHANGE)
	{
        ProcessJodChange(prpJod);
	}
	else if (ipDDXType == JOD_DELETE)
	{
	    ProcessJodDelete(prpJod);
	}
}

void DemandTableViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if (!prpDemand || ogJodData.DemandHasJobs(prpDemand->Urno))
		return;

	// check, if personnel demand
	if (strlen(prpDemand->Rety) <= 0 || strcmp(prpDemand->Rety,PERSONNEL_DEMAND) == 0)
	{
		DEMDATA_LINE *prlLine = NULL;

		// change already viewed demand ?
		if (omMapUrnoToLine.Lookup(reinterpret_cast<void *>(prpDemand->Urno),reinterpret_cast<void*&>(prlLine)))
		{
			//pogButtonList->SetDemandColor(RED);

			int ilLineno = -1;
			if (FindTeamLeaderOf(prpDemand,ilLineno))	// is part of a team
			{
				DEMDATA_LINE *prlTeamLine = &omLines[ilLineno];
				if (prlTeamLine->DemUrno == prpDemand->Urno)	// the team leader
				{
					if (prlTeamLine->TeamLines.GetSize() > 0)	// any members ?
					{
						for (int ilL = 0; ilL < prlTeamLine->TeamLines.GetSize(); ilL++)
						{
							DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlTeamLine->TeamLines[ilL].DemUrno);
							if (prlTeamDem)
							{
								DEMDATA_LINE olNewTeamLine;
								MakeLine(prlTeamDem,olNewTeamLine);
								olNewTeamLine.IsTeamLine = true;
								for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
								{
									if (i != ilL)
									{
										olNewTeamLine.TeamLines.New(prlTeamLine->TeamLines[i]);
									}
								}

								if (prlLine)	// now delete the old team leader
								{
									ilLineno = DeleteLine(prlLine);
								}

								// and create a new line from changed demand
								DEMDATA_LINE olLine;
								MakeLine(prpDemand,olLine);

								// check if the changed demand line matches the filter criteria
								if (IsPassFilter(&olLine))
								{
									olNewTeamLine.TeamLines.New(olLine);
									omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

									if (!bmCompressGroups)
									{
										ilLineno = InsertLine(&olLine);
									}
								}

								// update new team line
								RecalculateTeamLine(&olNewTeamLine);
								if (bmCompressGroups)
								{
									ilLineno = InsertLine(&olNewTeamLine);
								}
								else
								{
									ilLineno = ChangeLine(&olNewTeamLine,TRUE);
								}

								break;
							}
						}
					}
					else	// no team lines
					{
						if (prlLine)
						{
							ilLineno = DeleteLine(prlLine);
						}

						// and create a new line from changed demand
						DEMDATA_LINE olLine;
						MakeLine(prpDemand,olLine);

						// check if the changed demand line matches the filter criteria
						if (IsPassFilter(&olLine))
						{
							ilLineno = InsertLine(&olLine);
						}

						UpdateDisplay(FALSE);
					}
				}
				else	// only part of a team
				{
					for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
					{
						if (prlTeamLine->TeamLines[i].DemUrno == prpDemand->Urno)
						{
							prlTeamLine->TeamLines.DeleteAt(i);
							break;
						}
					}

					// delete the line, if visible
					if (prlLine)
					{
						ilLineno = DeleteLine(prlLine);
					}

					// create a new line with the changed demand
					DEMDATA_LINE olLine;
					MakeLine(prpDemand,olLine);
					if (IsPassFilter(&olLine))
					{
						prlTeamLine->TeamLines.New(olLine);
						omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

						if (!bmCompressGroups)
						{
							ilLineno = InsertLine(&olLine);
						}
					}
					
					// update team line
					RecalculateTeamLine(prlTeamLine);
					
					ilLineno = ChangeLine(prlTeamLine);
					
					UpdateDisplay(FALSE);
				}
			}
			else	// not a team demand
			{
				// simply delete existing demand
				if (prlLine)
				{
					ilLineno = DeleteLine(prlLine);
				}

				
				///*
				// and create a new line from changed demand
				DEMDATA_LINE olLine;
				MakeLine(prpDemand,olLine);

				// check if the changed demand line matches the filter criteria
				//Delete should not have inserted
				if (IsPassFilter(&olLine))
				{
					ilLineno = InsertLine(&olLine);
				}
				//*/

				UpdateDisplay(FALSE);
			}
		}
		else	// add a newly created demand	
		{
			DEMDATA_LINE olLine;
			MakeLine(prpDemand,olLine);
			if (IsPassFilter(&olLine))
			{
				int ilLineno;
				if (FindTeamOf(prpDemand,ilLineno))
				{
					DEMDATA_LINE *prlTeamLine = &omLines[ilLineno];

					// add group demand to team leader
					prlTeamLine->TeamLines.New(olLine);

					// add demand to map to prevent from adding it multiple times
					omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

					// update team line
					RecalculateTeamLine(prlTeamLine);

					ilLineno = ChangeLine(prlTeamLine);

					if (!bmCompressGroups)
					{
						int ilLineno = InsertLine(&olLine);
					}

					//pogButtonList->SetDemandColor(RED);
					UpdateDisplay(FALSE);
				}
				else
				{
					int ilLineno = InsertLine(&olLine);
					//pogButtonList->SetDemandColor(RED);
					UpdateDisplay(FALSE);
				}
			}
		}
	}
}

void DemandTableViewer::ProcessDemandDelete(DEMANDDATA *prpDemand)
{
	if(prpDemand == NULL)
		return;

	long llDemUrno = prpDemand->Urno;

	// anything to do ?
	DEMDATA_LINE *prlLine;
	if (!omMapUrnoToLine.Lookup(reinterpret_cast<void *>(llDemUrno),reinterpret_cast<void*&>(prlLine)))
		return;

	//pogButtonList->SetDemandColor(RED);

	int ilLineno = -1;
	// check, if demand is part of a team
	if (FindTeamLeaderOf(llDemUrno,ilLineno))
	{
		DEMDATA_LINE *prlTeamLine = &omLines[ilLineno];
		if (prlTeamLine->DemUrno == llDemUrno)	// the team leader ?
		{
			if (prlTeamLine->TeamLines.GetSize() > 0)	// any members available to be the new team leader ?
			{
				for (int ilL = 0; ilL < prlLine->TeamLines.GetSize(); ilL ++)
				{
					// use first available member as new team leader
					DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlLine->TeamLines[ilL].DemUrno);
					if (prlTeamDem)
					{
						DEMDATA_LINE olNewTeamLine;
						MakeLine(prlTeamDem,olNewTeamLine);
						olNewTeamLine.IsTeamLine = true;
						for (int i = 0; i < prlLine->TeamLines.GetSize(); i++)
						{
							if (i != ilL)
							{
								olNewTeamLine.TeamLines.New(prlLine->TeamLines[i]);
							}
						}

						// update team line
						RecalculateTeamLine(&olNewTeamLine);

						// check, if we must insert a new line or change an existing one
						if (bmCompressGroups)
						{
							int ilLineno = InsertLine(&olNewTeamLine);
						}
						else
						{
							int ilLineno = ChangeLine(&olNewTeamLine);
						}

						break;
					}
				}

				DeleteLine(prlTeamLine);
				UpdateDisplay(FALSE);
			}
			else if (prlLine)	// no team lines and visible
			{
				ilLineno = DeleteLine(prlLine);
				if (pomTable)
					pomTable->DisplayTable();
			}
		}
		else	// only part of a team
		{
			for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
			{
				if (prlTeamLine->TeamLines[i].DemUrno == llDemUrno)
				{
					prlTeamLine->TeamLines.DeleteAt(i);
					break;
				}
			}

			// update team line
			RecalculateTeamLine(prlTeamLine);
			ilLineno = ChangeLine(prlTeamLine);

			if (!bmCompressGroups && prlLine)
			{
				ilLineno = DeleteLine(prlLine);
			}
			else
			{
				omMapUrnoToLine.RemoveKey(reinterpret_cast<void *>(llDemUrno));
			}

			UpdateDisplay(FALSE);
		}
	}
	else	// non team demand
	{
		if (prlLine) // visible ?
		{
			ilLineno = DeleteLine(prlLine);
			if (pomTable)
				pomTable->DisplayTable();
		}
	}
}

int DemandTableViewer::ProcessJodNew(JODDATA *prpJod)
{
	if(prpJod == NULL)
		return -1;

	return ProcessJodNew(prpJod->Udem);
}

int DemandTableViewer::ProcessJodNew(long lpUdem)
{
	int ilLineno = -1;	// failed

	// anything to do ?
	DEMDATA_LINE *prlLine;
	if (!omMapUrnoToLine.Lookup(reinterpret_cast<void *>(lpUdem),(void *&)prlLine))
		return ilLineno;

	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lpUdem);
	if (!prlDem)
		return ilLineno;

	// not part of a team ?
	if (prlDem->Ulnk == 0)
	{
		// visible line ?
		if (prlLine)
		{
			ilLineno = DeleteLine(prlLine);			
			if (ilLineno >= 0)
			{
				//pogButtonList->SetDemandColor(RED);
				if (pomTable)
					pomTable->DisplayTable();
				return ilLineno;
			}
		}
		else
			return 0;
	}
	else	// must be a (non) displayed part of a team
	{
		if (FindTeamLeaderOf(prlDem,ilLineno))
		{
			DEMDATA_LINE *prlTeamLine = &omLines[ilLineno];
			if (prlTeamLine->DemUrno == prlDem->Urno)	// the team leader itself
			{
				if (prlTeamLine->TeamLines.GetSize() > 0)
				{
					DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlTeamLine->TeamLines[0].DemUrno);
					DEMDATA_LINE olNewTeamLine;
					MakeLine(prlTeamDem,olNewTeamLine);
					olNewTeamLine.IsTeamLine = true;
					for (int i = 1; i < prlTeamLine->TeamLines.GetSize(); i++)
					{
						olNewTeamLine.TeamLines.New(prlTeamLine->TeamLines[i]);
					}
					// update team line
					RecalculateTeamLine(&olNewTeamLine);
					if (bmCompressGroups)
					{
						int ilLineno = InsertLine(&olNewTeamLine);
					}
					else
					{
						int ilLineno = ChangeLine(&olNewTeamLine);
					}

					DeleteLine(prlTeamLine);
					UpdateDisplay(FALSE);

				}
				else if (prlLine)	// no team lines and visible
				{
					int ilLineno = DeleteLine(prlLine);			
					if (ilLineno >= 0)
					{
						//pogButtonList->SetDemandColor(RED);
						if (pomTable)
							pomTable->DisplayTable();
						return ilLineno;
					}
				}
				else
					return 0;
			}
			else
			{
				// remove demand from team				
				for (int i = prlTeamLine->TeamLines.GetSize() -1; i >= 0; i--)
				{
					if (prlTeamLine->TeamLines[i].DemUrno == prlDem->Urno)
					{
						prlTeamLine->TeamLines.DeleteAt(i);
						break;
					}
				}

				// update team line
				RecalculateTeamLine(prlTeamLine);
				ilLineno = ChangeLine(prlTeamLine);

				if (!bmCompressGroups && prlLine)
				{
					ilLineno = DeleteLine(prlLine);			
				}
				else
				{
					omMapUrnoToLine.RemoveKey(reinterpret_cast<void *>(prlDem->Urno));
				}

				UpdateDisplay(FALSE);

			}
		}
	}

//	if (ilLineno >= 0)
//		pogButtonList->SetDemandColor(RED);

	return ilLineno;
}

int DemandTableViewer::ProcessJodDelete(JODDATA *prpJod)
{
	if(prpJod == NULL)
		return -1;

	return ProcessJodDelete(prpJod->Udem);
}

int DemandTableViewer::ProcessJodDelete(long lpUdem)
{
	DEMDATA_LINE *prlLine;
	if (!omMapUrnoToLine.Lookup(reinterpret_cast<void *>(lpUdem),(void*&)prlLine))
	{
		DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(lpUdem);
		if (prlDemand)
		{
			DEMDATA_LINE olLine;
			MakeLine(prlDemand,olLine);

			if (!IsPassFilter(&olLine))
				return -1;				

			// is this demand part of a team ?
			if (prlDemand->Ulnk != 0)
			{
				int ilLineno;
				if (FindTeamOf(prlDemand,ilLineno))
				{
					DEMDATA_LINE *prlTeamLine = &omLines[ilLineno];					
					// add group demand to team leader
					prlTeamLine->TeamLines.New(olLine);
					// add demand to map to prevent from adding it multiple times
					omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prlDemand->Urno),NULL);

					if (bmCompressGroups)
					{
						// recalculate team leader line
						if (olLine.Debe < prlTeamLine->Debe)
							prlTeamLine->Debe = olLine.Debe;
						if (olLine.Deen > prlTeamLine->Deen)
							prlTeamLine->Deen = olLine.Deen;
						prlTeamLine->Dedu = prlTeamLine->Deen - prlTeamLine->Debe;
						prlTeamLine->Staff += olLine.Staff;
						ChangeLine(prlTeamLine);
						if (pomTable)
							pomTable->DisplayTable();
						//pogButtonList->SetDemandColor(RED);
						return ilLineno;
					}
					else
					{
						int ilLineno = InsertLine(&olLine);
						UpdateDisplay(FALSE);
						//pogButtonList->SetDemandColor(RED);
						return ilLineno;
					}

				}
				else // team does no longer exist 
				{
					olLine.IsTeamLine = true;
					int ilLineno = InsertLine(&olLine);
					if (pomTable)
						pomTable->DisplayTable();
					//pogButtonList->SetDemandColor(RED);
					return ilLineno;
				}
			}
			else
			{
				int ilLineno = InsertLine(&olLine);				
				if (pomTable)
					pomTable->DisplayTable();
				//pogButtonList->SetDemandColor(RED);
				return ilLineno;
			}
		}
	}
	return -1;
}

void DemandTableViewer::ProcessJodChange(JODDATA *prpJod)
{

}

void DemandTableViewer::ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight)
{
	if (ipDDXType == FLIGHT_CHANGE)
	{
		bool blUpdateDisplay = false;
		CCSPtrArray<DEMANDDATA> rolDemands;
		ogDemandData.GetDemandsByFlur(rolDemands,prpFlight->Urno);
		for (int i = 0; i < rolDemands.GetSize(); i++)
		{
			DEMANDDATA *prlDemand = &rolDemands[i];
			DEMDATA_LINE *prlLine = NULL;
			if (prlDemand && omMapUrnoToLine.Lookup((void *)prlDemand->Urno,(void *&)prlLine) && prlLine != NULL)
			{
				EvaluateFlight(prpFlight,*prlLine);	
				ChangeLine(prlLine);
				blUpdateDisplay = true;
			}
		}

		CCSPtrArray<DPXDATA> rolDpxs;
		ogDpxData.GetDpxsByFlight(prpFlight->Urno,rolDpxs,true);
		int ilDpxCnt = rolDpxs.GetSize();
		for(int j = 0 ; j < ilDpxCnt; j ++)
		{
			DPXDATA * prlDpx = &rolDpxs[j];

			if(!strcmp(prpFlight->Flno,prlDpx->FlnuFlno))
			{
				if( prpFlight->Prms != prlDpx->FlnuPrms)
				{
					strcpy(prlDpx->FlnuPrms,prpFlight->Prms);
				}
			}
			else if (!strcmp(prpFlight->Flno,prlDpx->TfluFlno))
			{
				if( prpFlight->Prms != prlDpx->TfluPrms)
				{
					strcpy(prlDpx->TfluPrms,prpFlight->Prms);
				}	
			}

		}


		if (blUpdateDisplay)
			UpdateDisplay(false);
	}
	else if (ipDDXType == FLIGHT_SELECT)
	{
		if (!pomTable)
			return;

		CCSPtrArray<DEMANDDATA> rolDemands;
		ogDemandData.GetDemandsByFlur(rolDemands,prpFlight->Urno);
		for (int i = 0; i < rolDemands.GetSize(); i++)
		{
			DEMANDDATA *prlDemand = &rolDemands[i];
			DEMDATA_LINE *prlLine = NULL;
			if (prlDemand && omMapUrnoToLine.Lookup((void *)prlDemand->Urno,(void *&)prlLine) && prlLine != NULL)
			{
				for (int i = 0; i < omLines.GetSize(); i++)
				{
					if (omLines[i].DemUrno == prlLine->DemUrno)
					{
						pomTable->SelectLine(i,TRUE);						
					}
				}
			}
		}
		
	}
}

int DemandTableViewer::DeleteLine(DEMDATA_LINE *prpLine)
{
	int ilLineno = -1;
	for (int ilC = 0; ilC < omLines.GetSize(); ilC++)
	{
		DEMDATA_LINE *prlDem = &omLines[ilC];
		if (prlDem && prlDem->DemUrno == prpLine->DemUrno)
		{
			omMapUrnoToLine.RemoveKey(reinterpret_cast<void *>(prpLine->DemUrno));
			omLines.DeleteAt(ilC);
			if (pomTable)
				pomTable->DeleteTextLine(ilC);
			ilLineno = ilC;
			break;
		}
	}

	return ilLineno;

}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL DemandTableViewer::PrintDemandLine(DEMDATA_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			for (int i = 0; i < omTableFields.GetSize(); i++)
			{
				if (i == 0)
				{
					rlElement.Alignment  = PRINT_LEFT;
					rlElement.Length     = omTableFields[i].PrintLength;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.FrameRight = PRINT_NOFRAME;
					rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
					rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
					rlElement.pFont      = &pomPrint->omSmallFont_Bold;
					rlElement.Text       = omTableFields[i].Name;
			
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				}
				else
				{
					rlElement.Length     = omTableFields[i].PrintLength;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.Text		 = omTableFields[i].Name;
					if (i == omTableFields.GetSize() -1)
						rlElement.FrameRight = PRINT_FRAMEMEDIUM;
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				}

			}
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		for (int i = 0; i < omTableFields.GetSize(); i++)
		{
			if (i == 0)
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Length     = omTableFields[i].PrintLength;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameRight = PRINT_NOFRAME;
				rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
				rlElement.FrameBottom= ilBottomFrame;
				rlElement.pFont       = &pomPrint->omSmallFont_Bold;
				
			}
			else
			{
				rlElement.Length     = omTableFields[i].PrintLength;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				if (i == omTableFields.GetSize() -1)
					rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			}

			CString olField = omTableFields[i].Field;
			if (olField == "DETY")
			{
				DEMANDDATA rlDemand;
				strcpy(rlDemand.Dety,prpLine->Dety);
				rlElement.Text = ogDemandData.GetStringForDemandType(rlDemand) + CString("/") + prpLine->Rety;
			}
			else if (olField == "ALOC")
				rlElement.Text       = prpLine->Aloc;
			else if (olField == "AFNUM")
				rlElement.Text       = prpLine->AFnum;
			else if (olField == "DFNUM")
				rlElement.Text       = prpLine->DFnum;
			else if (olField == "DEBE")
				rlElement.Text       = ogBasicData.FormatDate(prpLine->Debe, omStartTime);
			else if (olField == "DEEN")
				rlElement.Text       = ogBasicData.FormatDate(prpLine->Deen, omStartTime);
			else if (olField == "DEDU")
			{
				CString olMinutes;
				olMinutes.Format("%ld",(long)prpLine->Dedu.GetTotalMinutes());
				rlElement.Text		= olMinutes;
			}
			else if (olField == "FCT")			
			{
				if (prpLine->IsTeamLine && bmCompressGroups)
					rlElement.Text       = "";
				else
					rlElement.Text       = prpLine->Fcco;
			}
			else if (olField == "SERVICE")			
			{
				if (prpLine->IsTeamLine && bmCompressGroups)
					rlElement.Text       = "";
				else
					rlElement.Text       = prpLine->Service;
			}
			else if (olField == "PERMITS")
			{
				if (prpLine->IsTeamLine && bmCompressGroups)
					rlElement.Text       = "";
				else
					rlElement.Text       = prpLine->Permits;
			}
			else if (olField == "REGN")
				rlElement.Text       = prpLine->Regn;
			else if (olField == "BLT1")
				rlElement.Text       = prpLine->Blt1;
			else if (olField == "CKI")
				rlElement.Text       = prpLine->Cki;
			else if (olField == "VAARR")
				rlElement.Text       = prpLine->VaArr;
			else if (olField == "VADEP")
				rlElement.Text       = prpLine->VaDep;
			else if (olField == "AIRPORT")
				rlElement.Text       = prpLine->Dest;
			else if (olField == "PSTA")
				rlElement.Text       = prpLine->Psta;
			else if (olField == "PSTD")
				rlElement.Text       = prpLine->Pstd;
			else if (olField == "GATA")
				rlElement.Text       = prpLine->Gata;
			else if (olField == "GATD")
				rlElement.Text       = prpLine->Gatd;
			else if (olField == "PAXF")
				rlElement.Text       = prpLine->Paxf;
			else if (olField == "PAXB")
				rlElement.Text       = prpLine->Paxb;
			else if (olField == "PAXE")
				rlElement.Text       = prpLine->Paxe;
//			else if (olField == "CONFF")
//				rlElement.Text       = prpLine->ConfF;
//			else if (olField == "CONFJ")
//				rlElement.Text       = prpLine->ConfJ;
//			else if (olField == "CONFY")
//				rlElement.Text       = prpLine->ConfY;
			else if (olField == "ACTYPE")
				rlElement.Text       = prpLine->Act3;
			else if (olField == "ULNK")
			{
				CString olValue;
				olValue.Format("%ld",prpLine->Ulnk);
				rlElement.Text = olValue;
			}
			else if (olField == "STAFF")
			{
				CString olValue;
				olValue.Format("%d",prpLine->Staff);
				rlElement.Text = olValue;
			}
			else if (olField == "TIFA"  && prpLine->Tifa != TIMENULL)
			{
				CString szState;
				if (strlen(prpLine->Tisa))
					szState.Format("(%s)",(const char *)GetTifName(prpLine->Tisa));
				rlElement.Text       = ogBasicData.FormatDate(prpLine->Tifa, omStartTime) + szState;
			}
			else if (olField == "TIFD" && prpLine->Tifd != TIMENULL)
			{
				CString szState;
				if (strlen(prpLine->Tisd))
					szState.Format("(%s)",(const char *)GetTifName(prpLine->Tisd));
				rlElement.Text       = ogBasicData.FormatDate(prpLine->Tifd, omStartTime) + szState;
			}
			else if (olField == "PNAM")
				rlElement.Text       = prpLine->Pnam;
			else if (olField == "PREM")
				rlElement.Text       = prpLine->Prem;

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

	}
	return TRUE;
}

BOOL DemandTableViewer::PrintDemandHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void DemandTableViewer::PrintView()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) +
	CString(omEndTime.Format("%d.%m.%Y %H:%M"))
			+  GetString(IDS_STRING33143) + ogCfgData.rmUserSetup.DETV;		

	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,100,
		GetString(IDS_STRING61878),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING33144));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintDemandLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
//						break;
					}
					PrintDemandHeader(pomPrint);
				}
				PrintDemandLine(&omLines[i],FALSE);
			}
			PrintDemandLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

}

CTime DemandTableViewer::StartTime() const
{
	return omStartTime;
}

CTime DemandTableViewer::EndTime() const
{
	return omEndTime;
}

int DemandTableViewer::Lines() const
{
	return omLines.GetSize();
}

DEMDATA_LINE*	DemandTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void DemandTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void DemandTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

void DemandTableViewer::GetDefaultViewColumns(CStringArray& ropColumnNames)
{
	CString pclTableFields("DETY,ALOC,AFNUM,DFNUM,DEBE,DEEN,DEDU,FCT,PERMITS,STAFF,REGN,ACTYPE");

	char *token;
	token = strtok(pclTableFields.GetBuffer(0),",");
	while(token)
	{
		ropColumnNames.Add(token);
		token = strtok(NULL,",");
	}
}

void DemandTableViewer::GetAllViewColumns(CStringArray& ropColumnNames)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (!ogBasicData.DisplayDebugInfo() && strcmp(osfields[i].Field,"ULNK") == 0)
			continue;
		if (strcmp(osfields[i].Field,"PNAM") == 0|| strcmp(osfields[i].Field,"PREM") == 0)
		{
			if (ogBasicData.IsPRMRelatedTemplate())
			{
				ropColumnNames.Add(osfields[i].Field);	
			}
		}
		else
		{
			ropColumnNames.Add(osfields[i].Field);	
		}
	}
	
}

CString DemandTableViewer::GetViewColumnName(const CString& ropColumnField)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Field == ropColumnField)
			return osfields[i].Name;	
	}
	
	return "";
}

CString DemandTableViewer::GetViewColumnField(const CString& ropColumnName)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Name == ropColumnName)
			return osfields[i].Field;	
	}
	
	return "";
}

bool DemandTableViewer::EvaluateTableFields()
{

	CStringArray olViewColumns;
	CViewer::GetFilter("Columns",olViewColumns);
	if (!olViewColumns.GetSize())
		GetDefaultViewColumns(olViewColumns);							

	omTableFields.DeleteAll();

	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int ilC = 0; ilC < olViewColumns.GetSize();ilC++)
	{
		for (int i = 0; i < ilSize; i++)
		{
			if (osfields[i].Field.CompareNoCase(olViewColumns[ilC]) == 0)
			{
				omTableFields.New(osfields[i]);
			}
		}

	}

	return true;
}

int DemandTableViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

BOOL DemandTableViewer::RecalculateTeamLine(DEMDATA_LINE *prlTeamLine)
{
	if (!prlTeamLine || !prlTeamLine->IsTeamLine)
		return FALSE;

	// setup team leader data
	DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlTeamLine->DemUrno);
	MakeLine(prlTeamDem,*prlTeamLine);
	prlTeamLine->IsTeamLine = true;
	prlTeamLine->Debe = prlTeamLine->Debe;
	prlTeamLine->Deen = prlTeamLine->Deen;
	prlTeamLine->Dedu = prlTeamLine->Dedu;
	prlTeamLine->Staff= 1;
	
		
	for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
	{
		// recalculate team leader line
		if (prlTeamLine->TeamLines[i].Debe < prlTeamLine->Debe)
			prlTeamLine->Debe = prlTeamLine->TeamLines[i].Debe;
		if (prlTeamLine->TeamLines[i].Deen > prlTeamLine->Deen)
			prlTeamLine->Deen = prlTeamLine->TeamLines[i].Deen;
		prlTeamLine->Dedu = prlTeamLine->Deen - prlTeamLine->Debe;
		prlTeamLine->Staff += prlTeamLine->TeamLines[i].Staff;
	}

	return TRUE;
}

const char *DemandTableViewer::AlocFromAllocGroupType(const char *pcpAllocGroupType)
{
	if (!pcpAllocGroupType)
		return NULL;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_PSTGROUP) == 0)
		return ALLOCUNITTYPE_PST;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_GATEGROUP) == 0)
		return ALLOCUNITTYPE_GATE;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_CICGROUP) == 0)
		return ALLOCUNITTYPE_CIC;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_REGNGROUP) == 0)
		return ALLOCUNITTYPE_REGN;
	else
		return "";
}

BOOL DemandTableViewer::EvaluateFlight(FLIGHTDATA *prpFlight,DEMDATA_LINE& rrpLine)
{
	if (!prpFlight)
		return FALSE;

	BOOL blUpdate = EvaluateTime(prpFlight,rrpLine);
	
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(rrpLine.DemUrno);
	if (prlDemand)
	{
		if (prlDemand->Dety[0] == INBOUND_DEMAND && prlDemand->Ouri == prpFlight->Urno)
		{
			if (strcmp(rrpLine.Act3,prpFlight->Act3) != 0)
			{
				strcpy(rrpLine.Act3,prpFlight->Act3);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.AFnum,prpFlight->Fnum) != 0)
			{
				strcpy(rrpLine.AFnum,prpFlight->Fnum);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Regn,prpFlight->Regn) != 0)
			{
				strcpy(rrpLine.Regn,prpFlight->Regn);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Gata,prpFlight->Gta1) != 0)
			{
				rrpLine.Gata = prpFlight->Gta1;
				blUpdate = TRUE;
			}
			if (strcmp(rrpLine.Blt1,prpFlight->Blt1) != 0)
			{
				rrpLine.Blt1 = prpFlight->Blt1;
				blUpdate = TRUE;
			}
			if (strcmp(rrpLine.VaArr,prpFlight->Ttyp) != 0)
			{
				rrpLine.VaArr = prpFlight->Ttyp;
				blUpdate = TRUE;
			}
			if (strcmp(rrpLine.Psta,prpFlight->Psta) != 0)
			{
				rrpLine.Psta = prpFlight->Psta;
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Regn,prpFlight->Regn) != 0)
			{
				strcpy(rrpLine.Regn,prpFlight->Regn);
				blUpdate = TRUE;
			}

			CString olAlid;
			if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_GATE) == 0)
				olAlid = ogFlightData.GetGate(prpFlight);
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_PST) == 0)
				olAlid = ogFlightData.GetPosition(prpFlight);
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_CIC) == 0)
				olAlid = "";
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_REGN) == 0)
				olAlid = "";
			
			if (strcmp(rrpLine.Alid,olAlid) != 0)
			{
				strncpy(rrpLine.Alid,olAlid,sizeof(rrpLine.Alid));
				rrpLine.Alid[sizeof(rrpLine.Alid)-1] = '\0';
			}
		}
		else if (prlDemand->Ouro == prpFlight->Urno)
		{
			if (strcmp(rrpLine.Act3,prpFlight->Act3) != 0)
			{
				strcpy(rrpLine.Act3,prpFlight->Act3);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.DFnum,prpFlight->Fnum) != 0)
			{
				strcpy(rrpLine.DFnum,prpFlight->Fnum);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Regn,prpFlight->Regn) != 0)
			{
				strcpy(rrpLine.Regn,prpFlight->Regn);
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.VaDep,prpFlight->Ttyp) != 0)
			{
				rrpLine.VaDep = prpFlight->Ttyp;
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Pstd,prpFlight->Pstd) != 0)
			{
				rrpLine.Pstd = prpFlight->Pstd;
				blUpdate = TRUE;
			}

			if (strcmp(rrpLine.Gatd,prpFlight->Gtd1) != 0)
			{
				rrpLine.Gatd = prpFlight->Gtd1;
				blUpdate = TRUE;
			}

			CString olTmp;
			PAXDATA *prlPax = ogPaxData.GetPaxByAftu(prpFlight->Urno);
			if(prlPax != NULL)
			{
				olTmp.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_FIRST), ogPaxData.GetBookedPassengers(prlPax,PAX_FIRST));
				if(rrpLine.Paxf != olTmp)
				{
					rrpLine.Paxf = olTmp;
					blUpdate = TRUE;
				}
				olTmp.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_BUSINESS), ogPaxData.GetBookedPassengers(prlPax,PAX_BUSINESS));
				if(rrpLine.Paxb != olTmp)
				{
					rrpLine.Paxb = olTmp;
					blUpdate = TRUE;
				}
				olTmp.Format("%d/%d", ogPaxData.GetNumSeats(prlPax,PAX_ECONOMY), ogPaxData.GetBookedPassengers(prlPax,PAX_ECONOMY));
				if(rrpLine.Paxe != olTmp)
				{
					rrpLine.Paxe = olTmp;
					blUpdate = TRUE;
				}
			}

//			olTmp.Format("%d", ogLoaData.GetValu(prpFlight->Urno, LOA_PASSENGERS, LOA_FIRST));
//			if(rrpLine.ConfF != olTmp)
//			{
//				rrpLine.ConfF = olTmp;
//				blUpdate = TRUE;
//			}
//			olTmp.Format("%d", ogLoaData.GetValu(prpFlight->Urno, LOA_PASSENGERS, LOA_BUSINESS));
//			if(rrpLine.ConfJ != olTmp)
//			{
//				rrpLine.ConfJ = olTmp;
//				blUpdate = TRUE;
//			}
//			olTmp.Format("%d", ogLoaData.GetValu(prpFlight->Urno, LOA_PASSENGERS, LOA_ECONOMY));
//			if(rrpLine.ConfY != olTmp)
//			{
//				rrpLine.ConfY = olTmp;
//				blUpdate = TRUE;
//			}

			CString olAlid;
			if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_GATE) == 0)
				olAlid = ogFlightData.GetGate(prpFlight);
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_PST) == 0)
				olAlid = ogFlightData.GetPosition(prpFlight);
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_CIC) == 0)
				olAlid = "";
			else if (strcmp(rrpLine.Aloc,ALLOCUNITTYPE_REGN) == 0)
				olAlid = "";
			
			if (strcmp(rrpLine.Alid,olAlid) != 0)
			{
				strncpy(rrpLine.Alid,olAlid,sizeof(rrpLine.Alid));
				rrpLine.Alid[sizeof(rrpLine.Alid)-1] = '\0';
			}
		}
	}

	return blUpdate;
}

BOOL DemandTableViewer::EvaluateTime(FLIGHTDATA *prpFlight,DEMDATA_LINE& rrpLine)
{
	if (!prpFlight)
		return FALSE;

	BOOL blUpdate = FALSE;

	if (prpFlight->Adid[0] == 'A')	// arrival
	{
		switch(prpFlight->Tisa[0])
		{
		case 'S' :	// scheduled
			if (rrpLine.Tisa[0] != prpFlight->Tisa[0]) 
			{
				if (omTisa.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisa,prpFlight->Tisa);
					rrpLine.Tifa = prpFlight->Tifa;
					blUpdate = TRUE;
				}
			}
		break;
		case 'E' :	// estimated
			if (rrpLine.Tisa[0] != prpFlight->Tisa[0]) 
			{
				if (omTisa.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisa,prpFlight->Tisa);
					rrpLine.Tifa = prpFlight->Tifa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisa,"S");
					rrpLine.Tifa = prpFlight->Stoa;
					blUpdate = TRUE;
				}
			}
		break;
		case 'T' :	// ten miles out
			if (rrpLine.Tisa[0] != prpFlight->Tisa[0]) 
			{
				if (omTisa.Find('T') >= 0)
				{
					strcpy(rrpLine.Tisa,prpFlight->Tisa);
					rrpLine.Tifa = prpFlight->Tifa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisa,"E");
					rrpLine.Tifa = prpFlight->Etoa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisa,"S");
					rrpLine.Tifa = prpFlight->Stoa;
					blUpdate = TRUE;
				}
			}
		break;
		case 'L' :	// actual time
			if (rrpLine.Tisa[0] != prpFlight->Tisa[0]) 
			{
				if (omTisa.Find('L') >= 0)
				{
					strcpy(rrpLine.Tisa,prpFlight->Tisa);
					rrpLine.Tifa = prpFlight->Tifa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('T') >= 0)
				{
					strcpy(rrpLine.Tisa,"T");
					rrpLine.Tifa = prpFlight->Tmoa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisa,"E");
					rrpLine.Tifa = prpFlight->Etoa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisa,"S");
					rrpLine.Tifa = prpFlight->Stoa;
					blUpdate = TRUE;
				}
			}
		break;
		case 'O' :	// on block
			if (rrpLine.Tisa[0] != prpFlight->Tisa[0]) 
			{
				if (omTisa.Find('O') >= 0)
				{
					strcpy(rrpLine.Tisa,prpFlight->Tisa);
					rrpLine.Tifa = prpFlight->Tifa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('L') >= 0)
				{
					strcpy(rrpLine.Tisa,"L");
					rrpLine.Tifa = prpFlight->Land;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('T') >= 0)
				{
					strcpy(rrpLine.Tisa,"T");
					rrpLine.Tifa = prpFlight->Tmoa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisa,"E");
					rrpLine.Tifa = prpFlight->Etoa;
					blUpdate = TRUE;
				}
				else if (omTisa.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisa,"S");
					rrpLine.Tifa = prpFlight->Stoa;
					blUpdate = TRUE;
				}
			}
		break;
		}
	}
	else
	{
		strcpy(rrpLine.Tisd,prpFlight->Tisd);
		rrpLine.Tifd = prpFlight->Tifd;

		switch(prpFlight->Tisd[0])
		{
		case 'S' :	// scheduled
			if (rrpLine.Tisd[0] != prpFlight->Tisd[0]) 
			{
				if (omTisd.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisd,prpFlight->Tisd);
					rrpLine.Tifd = prpFlight->Tifd;
					blUpdate = TRUE;
				}
			}
		break;
		case 'E' :	// estimated
			if (rrpLine.Tisd[0] != prpFlight->Tisd[0]) 
			{
				if (omTisd.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisd,prpFlight->Tisd);
					rrpLine.Tifd = prpFlight->Tifd;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisd,"S");
					rrpLine.Tifd = prpFlight->Stod;
					blUpdate = TRUE;
				}
			}
		break;
		case 'O' :	// off block
			if (rrpLine.Tisd[0] != prpFlight->Tisd[0]) 
			{
				if (omTisa.Find('O') >= 0)
				{
					strcpy(rrpLine.Tisd,prpFlight->Tisd);
					rrpLine.Tifd = prpFlight->Tifd;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisd,"E");
					rrpLine.Tifd = prpFlight->Etod;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisd,"S");
					rrpLine.Tifd = prpFlight->Stod;
					blUpdate = TRUE;
				}
			}
		break;
		case 'A' :	// actual time
			if (rrpLine.Tisd[0] != prpFlight->Tisd[0]) 
			{
				if (omTisa.Find('A') >= 0)
				{
					strcpy(rrpLine.Tisd,prpFlight->Tisd);
					rrpLine.Tifd = prpFlight->Tifd;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('O') >= 0)
				{
					strcpy(rrpLine.Tisd,"O");
					rrpLine.Tifd = prpFlight->Ofbl;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('E') >= 0)
				{
					strcpy(rrpLine.Tisd,"E");
					rrpLine.Tifd = prpFlight->Etod;
					blUpdate = TRUE;
				}
				else if (omTisd.Find('S') >= 0)
				{
					strcpy(rrpLine.Tisd,"S");
					rrpLine.Tifd = prpFlight->Stod;
					blUpdate = TRUE;
				}
			}
		break;
		}
	}

	return blUpdate;
}

DEMDATA_LINE *DemandTableViewer::GetLine(DEMANDDATA *prpDem)
{
	if (!prpDem)
		return NULL;

	DEMDATA_LINE *prlLine = NULL;
	if (omMapUrnoToLine.Lookup((void *)prpDem->Urno,(void *&)prlLine))
		return prlLine;
	else
		return NULL;
}

CString DemandTableViewer::GetTifName(const char *popTis)
{
	CString olResult;
	if (!popTis)
		return olResult;

	switch(popTis[0])
	{
	case 'S' :
		olResult = GetString(IDS_STRING61898);
	break;
	case 'E' :
		olResult = GetString(IDS_STRING61899);
	break;
	case 'T' :
		olResult = GetString(IDS_STRING61900);
	break;
	case 'L' :
		olResult = GetString(IDS_STRING61901);
	break;
	case 'O' :
		olResult = GetString(IDS_STRING61902);
	break;
	case 'A' :
		olResult = GetString(IDS_STRING61903);
	break;
	}

	return olResult;
}

//Singapore
CString DemandTableViewer::GetTerminalNumber(DEMDATA_LINE *prpDemLine) const
{
	DEMANDDATA* prlDemandData = NULL;
	if(prpDemLine != NULL)
	{
		prlDemandData = ogDemandData.GetDemandByUrno(prpDemLine->DemUrno);
		if(prlDemandData != NULL)
		{
			return GetTerminalNumber(prlDemandData);
		}
	}
	return "";
}

//Singapore
CString DemandTableViewer::GetTerminalNumber(DEMANDDATA *prpDemand) const
{	
	FLIGHTDATA* prlFlightDataIn = NULL;
	FLIGHTDATA* prlFlightDataOut = NULL;
	if(prpDemand != NULL)
	{
		prlFlightDataIn = ogFlightData.GetFlightByUrno(prpDemand->Ouri);
		prlFlightDataOut= ogFlightData.GetFlightByUrno(prpDemand->Ouro);
	}
	//0=Turnaround/1=Arr/2=Dep
	if(prlFlightDataOut != NULL && prpDemand->Dety[0] == '2')
	{			
		return prlFlightDataOut->Tgd1;
	}
	else if(prlFlightDataIn != NULL && (prpDemand->Dety[0] == '0' || prpDemand->Dety[0] == '1'))
	{
		return prlFlightDataIn->Tga1;
	}
	return "";
}

//Singapore
int DemandTableViewer::CompareTerminalNumber(DEMDATA_LINE *prpDemLine1,DEMDATA_LINE *prpDemLine2)
{
	CString olLine1Terminal = GetTerminalNumber(prpDemLine1);
	CString olLine2Terminal = GetTerminalNumber(prpDemLine2);
	
	int ilCompareResult = 0;
    ilCompareResult = (olLine1Terminal.CompareNoCase(olLine2Terminal) == 0)? 0:
                (olLine1Terminal.CompareNoCase(olLine2Terminal) > 0)? 1: -1;

	return ilCompareResult;
}

//Singapore
BOOL DemandTableViewer::IsTerminalCorrect(DEMDATA_LINE *prpDemand) const
{
	DEMANDDATA* polDemandData = NULL;
	polDemandData = ogDemandData.GetDemandByUrno(prpDemand->DemUrno);
	if(polDemandData != NULL)
	{
		return IsTerminalCorrect(polDemandData);
	}
	else
	{
		return FALSE;
	}		
}

//Singapore
BOOL DemandTableViewer::IsTerminalCorrect(DEMANDDATA *prpDemand) const
{
	BOOL blTerminalCorrect = FALSE;
	if(prpDemand != NULL)
	{
		CString olTerminalNumber = GetTerminalNumber(prpDemand);
		if(olTerminalNumber.IsEmpty())
		{
			RUDDATA *prlRudData = ogRudData.GetRudByUrno(prpDemand->Urud);
			SERDATA *prlSerData = NULL;
			if(prlRudData != NULL)
			{
				prlSerData = ogSerData.GetSerByUrno(prlRudData->Ughs);
			}
			if(prlSerData != NULL)
			{
				if(omTerminal.CompareNoCase(DemandTable::Terminal2) == 0)
				{
					if(CString(prlSerData->Seco).Mid(0,2).CompareNoCase(omTerminal) == 0)
					{
						blTerminalCorrect = TRUE;
					}
					else 
					{
						blTerminalCorrect = FALSE;
					}
				}
				else if(omTerminal.CompareNoCase(DemandTable::Terminal3) == 0)
				{
					if(CString(prlSerData->Seco).Mid(0,2).CompareNoCase(DemandTable::Terminal2) != 0)
					{
						blTerminalCorrect = TRUE;
					}
					else
					{
						blTerminalCorrect = FALSE;
					}

				}
			}
			else
			{
				blTerminalCorrect = FALSE;
			}
		}
		else
		{
			if(omTerminal.CompareNoCase(DemandTable::Terminal2) == 0)
			{
				if(olTerminalNumber.CompareNoCase(TERMINALNO2) == 0)
				{
					blTerminalCorrect = TRUE;
				}
				else 
				{
					blTerminalCorrect = FALSE;
				}
			}			
			else if(omTerminal.CompareNoCase(DemandTable::Terminal3) == 0)
			{
				if(olTerminalNumber.CompareNoCase(TERMINALNO2) != 0)
				{
					blTerminalCorrect = TRUE;
				}
				else 
				{
					blTerminalCorrect = FALSE;
				}
			}
		}
	}
	return blTerminalCorrect;
}

COLORREF DemandTableViewer::GetBackgroundColorForPRM()
{
	if(IsPrivateProfileOn("SHOW_PRM_LOUNGE_COLOUR","NO"))
	{
		CString olColour;
		if(ogDlgSettings.GetValue(CDlgSettings.CTYP_OTHER_COLOURS, CDlgSettings.CKEY_OTHER_COLOURS_PRM_FINALIZED, olColour))
		{
			return (COLORREF) atol(olColour);
		}
	}
	return WHITE;
}

bool DemandTableViewer::IsFinalized(DEMDATA_LINE * prpDem)
{
	bool blResult = false;
	if(prpDem->dpx->Urno >0)
	{
		if(prpDem->Dety[0] == '0' && (!strcmp(prpDem->dpx->FlnuPrms,"Y") || !strcmp(prpDem->dpx->TfluPrms,"Y")))
			blResult = true;
		else if (prpDem->Dety[0] == '1' && !strcmp(prpDem->dpx->FlnuPrms,"Y"))
			blResult = true;
		else if (prpDem->Dety[0] == '2')
		{
			//Check the demand with two flights first.
			if (strlen(prpDem->DFnum)>0 && !strcmp(prpDem->dpx->TfluPrms,"Y"))
				blResult = true;
			//Then check the demand with one fligth.
			else if (strlen(prpDem->AFnum)==0 && strlen(prpDem->DFnum)>0 && !strcmp(prpDem->dpx->FlnuPrms,"Y"))
				blResult = true;
		}
	}

	return blResult;
}