#if !defined(AFX_EXTENDSHIFTDLG_H__D724F774_E084_435B_9BA6_FE719609DE60__INCLUDED_)
#define AFX_EXTENDSHIFTDLG_H__D724F774_E084_435B_9BA6_FE719609DE60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtendShiftDlg.h : header file
//
#include <CedaJobData.h>

/////////////////////////////////////////////////////////////////////////////
// CExtendShiftDlg dialog

class CExtendShiftDlg : public CDialog
{
// Construction
public:
	CExtendShiftDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExtendShiftDlg)
	enum { IDD = IDD_EXTENDSHIFTDLG };
	CString	m_Employees;
	CString	m_Text;
	CTime	m_Actod;
	CTime	m_Actot;
	CEdit	m_CompleteCtrl;
	CEdit	m_AfterCtrl;
	CEdit	m_BeforeCtrl;
	CComboBox	m_Sub1Sub2Ctrl;
	CString	m_Sub1Sub2;
	CString	m_Before;
	CString	m_After;
	CString	m_Complete;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtendShiftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExtendShiftDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnNoButtonClicked();
	afx_msg void OnChangeEZcode();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCSPtrArray <JOBDATA> omJobs;
	CTime omNewShiftEndTime;
	CTime omMinShiftEndTime;
	void CheckOvertimeValue(UINT ipId);
	bool bmRemoveShiftExtension;

public:
	void AddJob(JOBDATA *prpJob);
	bool JobsOutsideTheShift(void);
	bool SetData(CCSPtrArray <JOBDATA> &ropJobs);
	bool SetDataRemoveShiftExtension(JOBDATA *prpPoolJob);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTENDSHIFTDLG_H__D724F774_E084_435B_9BA6_FE719609DE60__INCLUDED_)
