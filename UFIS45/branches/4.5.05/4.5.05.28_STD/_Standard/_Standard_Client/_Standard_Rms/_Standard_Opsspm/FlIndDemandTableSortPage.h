// FlIndDemandTableSortPage.h : header file
//
#ifndef _FLINDDEMANDTBSO_H_
#define _FLINDDEMANDTBSO_H_
/////////////////////////////////////////////////////////////////////////////
// DemandTableSortPage dialog

class FlIndDemandTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FlIndDemandTableSortPage)

// Construction
public:
	FlIndDemandTableSortPage();
	~FlIndDemandTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(FlIndDemandTableSortPage)
	enum { IDD = IDD_FLINDDEMANDTABLE_SORT_PAGE };
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlIndDemandTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlIndDemandTableSortPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnDemEndDirection();
	afx_msg void OnDemStartDirection();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
	CBitmap omUpArrow, omDownArrow;
	bool SetDirectionButton(int ipButtonId, bool bpAscending);

public:
	bool bmDemEndAscending, bmDemStartAscending ,bmDemTerminalAscending;
};

#endif // _FLINDDEMANDTBSO_H_
