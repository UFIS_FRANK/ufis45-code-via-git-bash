// PRMTablePropertySheet.h : header file
//
#ifndef _PRMTBLPS_H_
#define _PRMTBLPS_H_
/////////////////////////////////////////////////////////////////////////////
// PRMTablePropertySheet

#include <BasePropertySheet.h>
#include <PrmTableSortPage.h>
#include <FilterPage.h>
#include <DemandFilterPage.h>

class PRMTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	PRMTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	DemandFilterPage	m_pageColumns;
	FilterPage			m_pageService;	
	FilterPage			m_pageAirline;
	FilterPage			m_pagePrmt;	
	FilterPage			m_pageAdid;	
	FilterPage			m_pageTerminal;
	//added by MAX
	FilterPage          m_pageArrivalLocation;
	PRMTableSortPage	m_pageSort;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();
// Overrides
public:
    // Generated message map functions
    //{{AFX_MSG(PRMTablePropertySheet)
	afx_msg LONG OnPrmDiagramGroupPageChanged(UINT wParam, LONG lParam);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

// Helper routines
private:
	void UpdateEnableFlagInFilterPages();
};

#endif // _PRMTBLPS_H_
/////////////////////////////////////////////////////////////////////////////
