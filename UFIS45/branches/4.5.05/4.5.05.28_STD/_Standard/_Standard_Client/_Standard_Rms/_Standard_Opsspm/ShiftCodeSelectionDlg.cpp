// ShiftCodeSelectionDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <ShiftCodeSelectionDlg.h>
#include <CedaShiftTypeData.h>
#include <CedaOdaData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString TERMINAL2 = "T2"; //Singapore
/////////////////////////////////////////////////////////////////////////////
// CShiftCodeSelectionDlg dialog


CShiftCodeSelectionDlg::CShiftCodeSelectionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShiftCodeSelectionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CShiftCodeSelectionDlg)
	m_ShiftOrAbsence = -1;
	//}}AFX_DATA_INIT

	lmSelectedCodeUrno = 0L;
	bmIsShiftCode = true;			// false if the returned value is an absence code
	bmEnableBothCodes = false;
	bmSelectShiftCodeFirst = true;
}


void CShiftCodeSelectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShiftCodeSelectionDlg)
	DDX_Control(pDX, IDC_SHIFT_CODES, m_ShiftCodesCtrl);
	DDX_Control(pDX, IDC_ABSENCE_CODES, m_AbsenceCodesCtrl);
	DDX_Control(pDX, IDC_CODESLIST, m_CodesList);
	DDX_Radio(pDX, IDC_SHIFT_CODES, m_ShiftOrAbsence);
	DDX_Control(pDX, IDC_EDIT_STARTTIME, m_EditShiftStartTime); //Singapore
	DDX_Control(pDX, IDC_EDIT_ENDTIME, m_EditShiftEndTime);     //Singapore
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CShiftCodeSelectionDlg, CDialog)
	//{{AFX_MSG_MAP(CShiftCodeSelectionDlg)
	ON_LBN_DBLCLK(IDC_CODESLIST, OnDblclkCodesList)
	ON_BN_CLICKED(IDC_SHIFT_CODES, OnChangeCodeType)
	ON_BN_CLICKED(IDC_ABSENCE_CODES, OnChangeCodeType)
	ON_BN_CLICKED(IDC_CHECK_T2, OnCheckT2) //Singapore
	ON_BN_CLICKED(IDC_CHECK_T3, OnCheckT3) //Singapore
	ON_EN_CHANGE(IDC_EDIT_STARTTIME, OnChangeEditShiftStartTime) //Singapore
	ON_EN_CHANGE(IDC_EDIT_ENDTIME, OnChangeEditShiftEndTime)     //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShiftCodeSelectionDlg message handlers

BOOL CShiftCodeSelectionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(bmEnableBothCodes)
	{
		m_ShiftCodesCtrl.EnableWindow(TRUE);
		m_AbsenceCodesCtrl.EnableWindow(TRUE);
	}
	else
	{
		m_ShiftCodesCtrl.EnableWindow(FALSE);
		m_AbsenceCodesCtrl.EnableWindow(FALSE);
	}

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_CHECK_T2);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_CHECK_T3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_EDIT_STARTTIME);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_EDIT_ENDTIME);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	m_ShiftCodesCtrl.SetWindowText(GetString(IDS_SHIFTCODES));
	m_AbsenceCodesCtrl.SetWindowText(GetString(IDS_ABSENCECODES));
	SetWindowText(GetString(IDS_SSC_DLG_TITLE));

	if(bmSelectShiftCodeFirst)
	{
		m_ShiftOrAbsence = 0;
		DisplayShiftCodes();
	}
	else
	{
		m_ShiftOrAbsence = 1;
		DisplayAbsenceCodes();
	}

	if(!omSelectedCode.IsEmpty())
	{
		m_CodesList.SelectString(-1, omSelectedCode);
	}

	//Singapore, for setting the horizontal scrollbar in the list box
	char chConfExtent[10];
	GetPrivateProfileString(pcgAppName,"CONFIGURE_SHIFTHORZEXTENT","NO",chConfExtent,sizeof(chConfExtent),ogBasicData.GetConfigFileName());
	if(strcmp(chConfExtent,"YES") == 0)
	{
		char chHorzExtent[10];
		GetPrivateProfileString(pcgAppName,"HORIZONTAL_SHIFTEXTENT","",chHorzExtent,sizeof(chHorzExtent),ogBasicData.GetConfigFileName());
		if(strlen(chHorzExtent) > 0)
			m_CodesList.SetHorizontalExtent(atoi(chHorzExtent));
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CShiftCodeSelectionDlg::DisplayShiftCodes()
{
	m_CodesList.ResetContent();
	bmIsShiftCode = true;

	CString olText;
	int ilNumShiftCodes = ogShiftTypes.omData.GetSize();
	if(ogBasicData.IsPaxServiceT2() == false)
	{
	for(int ilST = 0; ilST < ilNumShiftCodes; ilST++)
	{
		SHIFTTYPEDATA *prlShiftType = &ogShiftTypes.omData[ilST];
		olText.Format("%s %s-%s %s (%s)",prlShiftType->Bsdc, prlShiftType->Esbg, prlShiftType->Lsen, prlShiftType->Fctc, prlShiftType->Bsdn);
		int ilIndex = m_CodesList.AddString(olText);
		m_CodesList.SetItemData(ilIndex, (DWORD) prlShiftType->Urno);
		}
	}
	else
	{	
		CString olShiftStartTime;
		m_EditShiftStartTime.GetWindowText(olShiftStartTime);
		CString olShiftEndTime;
		m_EditShiftEndTime.GetWindowText(olShiftEndTime);

		for(int ilST = 0; ilST < ilNumShiftCodes; ilST++)
		{
			SHIFTTYPEDATA *prlShiftType = &ogShiftTypes.omData[ilST];			
			if((!olShiftStartTime.IsEmpty() && CString(prlShiftType->Esbg).Mid(0,olShiftStartTime.GetLength()).Compare(olShiftStartTime) != 0)
			|| (!olShiftEndTime.IsEmpty() && CString(prlShiftType->Lsen).Mid(0,olShiftEndTime.GetLength()).Compare(olShiftEndTime) != 0))
			{
				continue;
			}
			olText.Format("%s %s-%s %s (%s)",prlShiftType->Bsdc, prlShiftType->Esbg, prlShiftType->Lsen, prlShiftType->Fctc, prlShiftType->Bsdn);
			int ilIndex = m_CodesList.AddString(olText);
			m_CodesList.SetItemData(ilIndex, (DWORD) prlShiftType->Urno);
		}
	}
}

void CShiftCodeSelectionDlg::DisplayAbsenceCodes()
{
	m_CodesList.ResetContent();
	bmIsShiftCode = false;

	CString olText;
	int ilNumOda = ogOdaData.omData.GetSize();
	for(int ilOda = 0; ilOda < ilNumOda; ilOda++)
	{
		ODADATA *prlOda = &ogOdaData.omData[ilOda];
		olText.Format("%s (%s)",prlOda->Sdac,prlOda->Sdan);
		int ilIndex = m_CodesList.AddString(olText);
		m_CodesList.SetItemData(ilIndex, (DWORD) prlOda->Urno);
	}
}

void CShiftCodeSelectionDlg::OnOK() 
{
	UpdateData();

	int ilIndex = m_CodesList.GetCurSel();
	if(ilIndex < 0)
	{
		MessageBox(GetString(IDS_PLEASESELECTFUNC),"",MB_ICONSTOP);
	}
	else
	{
		lmSelectedCodeUrno = (long) m_CodesList.GetItemData(ilIndex);

		if(bmIsShiftCode)
		{
			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByUrno(lmSelectedCodeUrno);
			if(prlShiftType == NULL)
			{
				MessageBox("Invalid BSD Urno, please contact your system administrator!","Internal Error",MB_ICONSTOP);
			}
			else
			{
				omSelectedCode = prlShiftType->Bsdc;
				CDialog::OnOK();
			}
		}
		else
		{
			ODADATA *prlOda = ogOdaData.GetOdaByUrno(lmSelectedCodeUrno);
			if(prlOda == NULL)
			{
				MessageBox("Invalid ODA Urno, please contact your system administrator!","Internal Error",MB_ICONSTOP);
			}
			else
			{
				omSelectedCode = prlOda->Sdac;
				CDialog::OnOK();
			}
		}
	}
}

void CShiftCodeSelectionDlg::OnDblclkCodesList() 
{
	OnOK();
}

void CShiftCodeSelectionDlg::OnChangeCodeType() 
{
	UpdateData();
	if(m_ShiftOrAbsence == 0)
	{
		DisplayCheckTerminal(SW_SHOW,SW_SHOW); //Singapore
		DisplayShiftCodes();
	}
	else
	{
		DisplayCheckTerminal(SW_SHOW,SW_SHOW); //Singapore
		DisplayAbsenceCodes();
	}
	UpdateData(FALSE);
}

//Singapore
void CShiftCodeSelectionDlg::DisplayCheckTerminal(int ipShowT2, int ipShowT3)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_CHECK_T2);
		if(polButton != NULL)
		{
			if(m_ShiftOrAbsence == 1)
			{
				polButton->SetCheck(0);
			}
			polButton->ShowWindow(ipShowT2);
		}
		polButton = (CButton*)GetDlgItem(IDC_CHECK_T3);
		if(polButton != NULL)
		{
			if(m_ShiftOrAbsence == 1)
			{
				polButton->SetCheck(0);
			}
			polButton->ShowWindow(ipShowT3);
		}
	}	
}

//Singapore
void CShiftCodeSelectionDlg::OnCheckT2()
{
	if(m_ShiftOrAbsence == 1)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_SHIFT_CODES);
		polButton->SetCheck(1);
		polButton = (CButton*)GetDlgItem(IDC_ABSENCE_CODES);
		polButton->SetCheck(0);
		OnChangeCodeType();
	}

	CButton* polButton = (CButton*)GetDlgItem(IDC_CHECK_T3);
	if(polButton != NULL)
	{
		polButton->SetCheck(0);
	}

	polButton = (CButton*)GetDlgItem(IDC_CHECK_T2);
	int ilChecked = polButton->GetCheck();
	
	m_CodesList.ResetContent();
	bmIsShiftCode = true;

	CString olShiftStartTime;
	m_EditShiftStartTime.GetWindowText(olShiftStartTime);
	CString olShiftEndTime;
	m_EditShiftEndTime.GetWindowText(olShiftEndTime);

	CString olText;
	int ilNumShiftCodes = ogShiftTypes.omData.GetSize();
	for(int ilST = 0; ilST < ilNumShiftCodes; ilST++)
	{
		SHIFTTYPEDATA *prlShiftType = &ogShiftTypes.omData[ilST];
		CString olTerminal = strlen(prlShiftType->Bsds) == 0 ? CString(prlShiftType->Bsdc).Mid(0,2) : prlShiftType->Bsds;
		if((ilChecked == 1 && olTerminal.CompareNoCase(TERMINAL2) != 0)
			|| (!olShiftStartTime.IsEmpty() && CString(prlShiftType->Esbg).Mid(0,olShiftStartTime.GetLength()).Compare(olShiftStartTime) != 0)
			|| (!olShiftEndTime.IsEmpty() && CString(prlShiftType->Lsen).Mid(0,olShiftEndTime.GetLength()).Compare(olShiftEndTime) != 0))
		{
			continue;
		}
		olText.Format("%s %s-%s %s (%s)",prlShiftType->Bsdc, prlShiftType->Esbg, prlShiftType->Lsen, prlShiftType->Fctc, prlShiftType->Bsdn);
		int ilIndex = m_CodesList.AddString(olText);
		m_CodesList.SetItemData(ilIndex, (DWORD) prlShiftType->Urno);
	}
}

//Singapore
void CShiftCodeSelectionDlg::OnCheckT3()
{
	if(m_ShiftOrAbsence == 1)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_SHIFT_CODES);
		polButton->SetCheck(1);
		polButton = (CButton*)GetDlgItem(IDC_ABSENCE_CODES);
		polButton->SetCheck(0);
		OnChangeCodeType();
	}

	CButton* polButton = (CButton*)GetDlgItem(IDC_CHECK_T2);
	if(polButton != NULL)
	{
		polButton->SetCheck(0);
	}	
	
	polButton = (CButton*)GetDlgItem(IDC_CHECK_T3);
	int ilChecked = polButton->GetCheck();
	
	m_CodesList.ResetContent();
	bmIsShiftCode = true;

	CString olShiftStartTime;
	m_EditShiftStartTime.GetWindowText(olShiftStartTime);
	CString olShiftEndTime;
	m_EditShiftEndTime.GetWindowText(olShiftEndTime);

	CString olText;
	int ilNumShiftCodes = ogShiftTypes.omData.GetSize();
	for(int ilST = 0; ilST < ilNumShiftCodes; ilST++)
	{
		SHIFTTYPEDATA *prlShiftType = &ogShiftTypes.omData[ilST];
		CString olTerminal = strlen(prlShiftType->Bsds) == 0 ? CString(prlShiftType->Bsdc).Mid(0,2) : prlShiftType->Bsds;
		if((ilChecked == 1 && olTerminal.CompareNoCase(TERMINAL2) == 0)
			|| (!olShiftStartTime.IsEmpty() && CString(prlShiftType->Esbg).Mid(0,olShiftStartTime.GetLength()).Compare(olShiftStartTime) != 0)
			|| (!olShiftEndTime.IsEmpty() && CString(prlShiftType->Lsen).Mid(0,olShiftEndTime.GetLength()).Compare(olShiftEndTime) != 0))
		{
			continue;
		}

		olText.Format("%s %s-%s %s (%s)",prlShiftType->Bsdc, prlShiftType->Esbg, prlShiftType->Lsen, prlShiftType->Fctc, prlShiftType->Bsdn);
		int ilIndex = m_CodesList.AddString(olText);
		m_CodesList.SetItemData(ilIndex, (DWORD) prlShiftType->Urno);
	}
}

//Singapore
void CShiftCodeSelectionDlg::OnChangeEditShiftStartTime()
{
	UpdateDisplayList();
}

//Singapore
void CShiftCodeSelectionDlg::OnChangeEditShiftEndTime()
{
	UpdateDisplayList();
}

//Singapore
void CShiftCodeSelectionDlg::UpdateDisplayList()
{	
	if(m_ShiftOrAbsence == 1)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_SHIFT_CODES);
		polButton->SetCheck(1);
		polButton = (CButton*)GetDlgItem(IDC_ABSENCE_CODES);
		polButton->SetCheck(0);
		OnChangeCodeType();
	}
	else
	{
		CButton* polButtonT2 = (CButton*)GetDlgItem(IDC_CHECK_T2);
		CButton* polButtonT3 = (CButton*)GetDlgItem(IDC_CHECK_T3);
		if(polButtonT2->GetCheck() == 1)
		{
			OnCheckT2();
		}
		else if(polButtonT3->GetCheck() == 1)
		{
			OnCheckT3();
		}
		else
		{
			OnChangeCodeType();
		}
	}
}