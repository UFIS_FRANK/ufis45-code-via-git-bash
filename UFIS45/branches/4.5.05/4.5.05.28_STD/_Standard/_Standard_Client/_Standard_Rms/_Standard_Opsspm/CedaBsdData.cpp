// CedaBsdData.cpp - Shift Code Stammdaten
//

#include <stdafx.h>
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccslog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaBsdData.h>

//CedaBsdData ogBsdData;

CedaBsdData::CedaBsdData()
{
   // Create an array of CEDARECINFO for BSDDATA
    BEGIN_CEDARECINFO(BSDDATA, BsdDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Bsdc,"BSDC")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(BsdDataRecInfo)/sizeof(BsdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BsdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"BSDTAB");
    pcmFieldList = "URNO,BSDC,FCTC";
}

CedaBsdData::~CedaBsdData()
{
	TRACE("CedaBsdData::~CedaBsdData called\n");
	ClearAll();
}

void CedaBsdData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omCodeMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaBsdData::ReadBsdData()
{
	CCSReturnCode ilRc = RCSuccess;

	// Select data from the database
	char pclWhere[100] = "";
	char pclCom[10] = "RT";
	if (CedaAction2(pclCom, pclWhere) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaBsdData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			BSDDATA *prlBsd = new BSDDATA;
			if ((ilRc = GetBufferRecord2(ilLc,prlBsd)) == RCSuccess)
			{
				AddBsdInternal(prlBsd);
			}
			else
			{
				delete prlBsd;
			}
		}
	}
	ogBasicData.Trace("CedaBsdData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(BSDDATA), "");
	return TRUE;
}


BOOL CedaBsdData::AddBsdInternal(BSDDATA *prpBsd)
{
	omData.Add(prpBsd);
	omUrnoMap.SetAt((void *)prpBsd->Urno,prpBsd);
	omCodeMap.SetAt(prpBsd->Bsdc,prpBsd);
	return RCSuccess;
}

BSDDATA *CedaBsdData::GetBsdByUrno(long lpUrno)
{
	BSDDATA *prlBsd = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBsd);
	return prlBsd;
}

BSDDATA *CedaBsdData::GetBsdByCode(char *pcpCode)
{
	BSDDATA *prlBsd = NULL;
	omCodeMap.Lookup(pcpCode,(void *& )prlBsd);
	return prlBsd;
}

CString CedaBsdData::GetTableName(void)
{
	return CString(pcmTableName);
}