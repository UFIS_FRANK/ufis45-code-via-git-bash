
#ifndef COVERVIEWER_H
#define COVERVIEWER_H

#include <stdafx.h>
#include <CCSPtrArray.h>
#include <CedaDemandData.h>
#include <CedaJobData.h>
#include <Coverage.h>
#include <CoverageGraphic.h>
#include <CedaAloData.h>
#include <CedaMetaAloData.h>

/*
#define ROOT_ZE							9999
#define ROT_ZE								9998
#define BLAU_ZE							9997
*/
#define DEFAULT_ZE						9996
#define AERO_ZE							9995
#define A_ZE								9994
#define POOL_ZE							9993
#define GATEAREA_ZE						9992
#define GATE_ZE							9991
#define CCIPOOL_ZE						9990
#define TERM_ZE							9989
#define CLASS_ZE							9988
#define DESK_ZE							9987

#define FULL_HOUR_MATRIX				12
#define HALF_HOUR_MATRIX				6
#define QUARTER_HOUR_MATRIX			3
#define TEN_MINUTES_MATRIX				2
#define FIVE_MINUTES_MATRIX			1

#define MAXVALUES							300

#define MAX_LONG_STRING					10240

extern CedaDemandData	ogFlightDemands;
extern CedaJobData		ogJobData;

struct VIEWERCURVE
{
	CURVE					CurveInfo;		// Zeiger auf CURVE-Struct
};

/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: CoverageViewer
//@See: CViewer, Coverage
/*@Doc:
  No comment on this up to now.
*/
class CoverageViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CoverageViewer();
    //@ManMemo: Default destructor
    ~CoverageViewer();

	//@ManMemo: Read all necessary data to create a coverage
		BOOL PreselectTreeControl();

	//@ManMemo: Configuration of the curves'n'styles used in the graphic
		BOOL ConfigureCurves();

    //@ManMemo: Attach
		void Attach(CWnd *popParent, CCSTree *popTree,CCoverageGraphic *popGraphic, CCoverage *popCoverage);

    //@ManMemo: ReadActualCoverage
		BOOL ReadActualCoverage(int ipMainChoice);

    //@ManMemo: DeleteAll
		void DeleteAll();

	//@ManMemo: ChangeViewTo
		BOOL ChangeViewTo(int ipIntervallTime);
		void ChangeViewTo(CString opView, int ipMainChoice);
		void OnDDX(void *popInstance, int ipDDXType,
				void *vpDataPointer, CString &ropInstanceName);

protected:

private:
	BOOL InitializeTree(int ipMainChoice); 
	TREEITEM *CreateNewItem(TREEITEM *prpParent,ALODATA *prpAlo);
	BOOL ConfigureMyCurve(int ipCurveIndex, VIEWERCURVE *popCurve);
	BOOL preInitialize();
	BOOL ReadCoverageData(int ipMainChoice);
	BOOL NewCurveValues(int ipMatrix);
	int  GetAverageValue(CCSPtrArray<int> array, int ilFirstIndex, int items, BOOL bpRoundUp);
	BOOL GetSelectedTreeItems(char *pcpSelectedTreeItems, int ipVerifyTo);
	void TraceForAllGates(TREEITEM *olNewSelTree, char *pcpGateString, int ipVerifyTo);
	char pcmDemandAlids[MAX_LONG_STRING];
	char pcmJobAlids[MAX_LONG_STRING];


// Attributes
public:

protected:

private:
	CWnd								*pomParent;
	CCSTree							*pomTree;
	CCoverageGraphic				*pomGraphic;
	CCoverage						*pomCoverage;
	CCSPtrArray<VIEWERCURVE>	omData;

};

#endif

