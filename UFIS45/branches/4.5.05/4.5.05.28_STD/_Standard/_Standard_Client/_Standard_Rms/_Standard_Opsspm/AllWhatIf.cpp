#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h> 
#include <AllWhatIf.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <CedaOptData.h>
#include <ccsddx.h>
#include <NewWif.h>
#include <CedaJobData.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/


// the broadcast CallBack function, has to be outside the AllWhatIf class
void AllWhatIfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// CAllWhatIf dialog


CAllWhatIf::CAllWhatIf(CWnd* pParent /*=NULL*/)
	: CDialog(CAllWhatIf::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAllWhatIf)
	//}}AFX_DATA_INIT
}


void CAllWhatIf::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllWhatIf)
	DDX_Control(pDX, IDC_WHATIFLIST, m_AllWhatIfList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAllWhatIf, CDialog)
	//{{AFX_MSG_MAP(CAllWhatIf)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_USE, OnUse)
	ON_BN_CLICKED(IDC_KILL, OnKill)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAllWhatIf message handlers



int CAllWhatIf::flt_get_nth_field (char *Pstr, char *Ptrenn, int n, char **PPresult)
{
  int ilRc = 0;
  int i=0;
  int count=1;

  while (*(Pstr+i) && count < n)
  {
    if ( *(Pstr+i) == '(')
    {
      while (*(Pstr+i) && *(Pstr+i) != ')')
        i++;
    }
    if ( *(Pstr+i))
    {
//      if ( *(Pstr+i) == ';' )
      if ( *(Pstr+i) == *Ptrenn )
	  {
        count++;
      }
      i++;
    }
  }

  if (*(Pstr+i) || count == n)
  {
    *PPresult = Pstr+i;
  }
  else
  {
    ilRc = -1;
  }

  return ilRc;
} /* flt_get_nth_field */


int CAllWhatIf::getNthItemOfField (char *Pstr, char *Ptrenn, int n, char *Presult)
{
  int ilRc = 0;
  int i=0;
  int res_i=0;
  char *Ptmp=NULL;
  int p_level=0;

  ilRc = flt_get_nth_field (Pstr, Ptrenn, n, &Ptmp);
  if (ilRc == 0)
  {
/*
    while (*(Ptmp+i) && (p_level > 0 || ( *(Ptmp+i) != ';' &&
                         *(Ptmp+i) != '\n') && *(Ptmp+i) != '\r') )
*/
    while (*(Ptmp+i) && (p_level > 0 || ( *(Ptmp+i) != *Ptrenn &&
                         *(Ptmp+i) != '\n') && *(Ptmp+i) != '\r') )
    {
      if ( *(Ptmp+i) == '(')
      {
        p_level++;
      }
      if ( *(Ptmp+i) == ')')
      {
        p_level--;
      }
      Presult[res_i] = *(Ptmp + i);
      res_i++;
      i++;
    }
    Presult[res_i] = '\0';
  }

  return ilRc;
} /* getNthItemOfField */


void CAllWhatIf::OnNew() 
{
	char		pclNewWhatIf[82];
	OPTDATA	rlAktOpt;
	char		pclText[1024];
	char		pclWhatIfItem[82];
	BOOL		bpDoUpdate    = FALSE;
	BOOL		bpWhatIfFound = FALSE;
	int		ilIndex		  = m_AllWhatIfList.GetCurSel();
	long		llOldUrno	  = 0L;


	m_AllWhatIfList.GetText(ilIndex, pclText);

	int ilRc = getNthItemOfField(pclText, " ", 1, pclWhatIfItem);
	if ( (ilIndex >= 0) &&
		 (ilRc == 0)       )
	{
		int ilCount = ogOptData.omData.GetSize();
		for (int i = 0; (i < ilCount) && (bpWhatIfFound == FALSE); i++)
		{
			rlAktOpt = ogOptData.omData[i];
			if ( (strcmp(rlAktOpt.Ctyp, "WHAT-IF") == 0)     &&
				 (strcmp(rlAktOpt.Ckey, pclWhatIfItem) == 0)    )
			{
				bpWhatIfFound = TRUE;
				llOldUrno = rlAktOpt.Urno;
			}
		}

		if (bpWhatIfFound)
		{
			sprintf(pclNewWhatIf, "");
			CTime rlVato = rlAktOpt.Vato;
			CNewWif *polNewWifDlg = new CNewWif((CWnd *)this, pclNewWhatIf);
			if(polNewWifDlg->DoModal() == IDOK)
			{
				if (strcmp("", pclNewWhatIf) != 0)
				{
					int ilCount = ogOptData.omData.GetSize();
					for (int i = 0; i < ilCount; i++)
					{
						OPTDATA rlOpt = ogOptData.omData[i];
						if ( (strcmp(rlOpt.Ctyp, "WHAT-IF") == 0)    &&
							 (strcmp(rlOpt.Ckey, pclNewWhatIf) == 0)    )
						{
							bpDoUpdate = TRUE;
						}
					}

					if (!bpDoUpdate)
					{
						CTime olNow = ogBasicData.GetTime();

						//  The offsetvalue for the expiration date can be set in the <CEDA.INI> 
						//  file by the parameter EXPIRATIONOFFSET in the group DISPLAY.
						//  If this parameter is NOT set in the CEDA.INI file, it is defaultly
						//  set to 3!!!
							char pclConfigPath[512];
							char pclTmpText[512];
							if (getenv("CEDA") == NULL)
								 strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
							else
								 strcpy(pclConfigPath, getenv("CEDA"));
							GetPrivateProfileString(pcgAppName, "EXPIRATIONOFFSET", "2",
								pclTmpText, sizeof pclTmpText, pclConfigPath);
							int ilTimeOffset = atoi(pclTmpText);

							CTime olExpire = olNow + CTimeSpan(ilTimeOffset,0,0,0);
							rlAktOpt.Expr  = olExpire;
						
						strcpy(rlAktOpt.Erst, ogUsername);
						strcpy(rlAktOpt.Ckey, pclNewWhatIf);
						rlAktOpt.Erda = olNow;
						rlAktOpt.Vato = rlVato;
						rlAktOpt.Urno = ogBasicData.GetNextUrno();
						rlAktOpt.IsChanged = DATA_NEW;
						ogOptData.AddOpt(&rlAktOpt);

						//Copy all existing entries for the selected What-If to the new one.
						ogJobData.CreateNewWhatIf(rlAktOpt.Ckey, pclWhatIfItem, rlAktOpt.Urno, llOldUrno);
					}
					else
					{
						//MessageBox("Dieses What-If existiert bereits. Speichern abgebrochen.");
						MessageBox(GetString(IDS_STRING61204));
					}
				}
				else
				{
					//MessageBox("Ung�ltige Eingabe. Speichern abgebrochen.");
					MessageBox(GetString(IDS_STRING61205));
				}
			}
			delete polNewWifDlg;
		}
	}
	else
	{
		MessageBox("Es ist kein What-If ausgew�hlt worden.");
	}
}

void CAllWhatIf::OnUse() 
{
	int ilIndex = m_AllWhatIfList.GetCurSel();
    char pclText[1024];
    char pclWhatIfItem[82];

	m_AllWhatIfList.GetText(ilIndex, pclText);

	int ilRc = getNthItemOfField(pclText, " ", 1, pclWhatIfItem);
	if ( (ilIndex >= 0) &&
		 (ilRc == 0)       )
	{
		int	 ilRun         = 0;
		BOOL blWhatIfFound = FALSE;
		int  ilCount = ogOptData.omData.GetSize();

		if (ilCount > 0)
		{
			do
			{
				OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
				if ( (strcmp(prlLocalOpt->Ctyp, "WHAT-IF") == 0)     &&
					 (strcmp(prlLocalOpt->Ckey, pclWhatIfItem) == 0)    )
				{
					if(ogOptData.UseOpt(prlLocalOpt->Urno) == RCSuccess)
					{
						//Yeeha
					}
					else
					{
						//MessageBox("Anwenden abgebrochen.");
						MessageBox(GetString(IDS_STRING61207));
					}


					blWhatIfFound = TRUE;
				}
				ilRun++;
	
			} while ( (ilRun < ilCount) && !blWhatIfFound);
		}
	}
	else
	{
		//MessageBox("Es ist kein What-If ausgew�hlt worden.");
		MessageBox(GetString(IDS_STRING61208));
	}

	// Unregister DDX call back function
	TRACE("AllWhatIf: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);
	
	CDialog::OnCancel();
}

void CAllWhatIf::OnKill() 
{
	int ilIndex = m_AllWhatIfList.GetCurSel();
    char pclText[1024];
    char pclWhatIfItem[82];

	m_AllWhatIfList.GetText(ilIndex, pclText);

	int ilRc = getNthItemOfField(pclText, " ", 1, pclWhatIfItem);
	if ( (ilIndex >= 0) &&
		 (ilRc == 0)       )
	{
		int	 ilRun         = 0;
		BOOL blWhatIfFound = FALSE;
		int  ilCount = ogOptData.omData.GetSize();

		if (ilCount > 0)
		{
			if (MessageBox(GetString(IDS_STRING61209), GetString(IDS_STRING61210), MB_YESNO) == IDYES)
			{
				do
				{
					OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
					if ( (strcmp(prlLocalOpt->Ctyp, "WHAT-IF") == 0)     &&
						 (strcmp(prlLocalOpt->Ckey, pclWhatIfItem) == 0)    )
					{
						if(ogOptData.DeleteOpt(prlLocalOpt->Urno) == RCSuccess)
						{
							//Yeeha
						}
						else
						{
							//MessageBox("L�schen abgebrochen.");
							MessageBox(GetString(IDS_STRING61211));
						}


						blWhatIfFound = TRUE;
					}
					ilRun++;
		
				} while ( (ilRun < ilCount) && !blWhatIfFound);
			}
		}
	}
	else
	{
		//MessageBox("Es ist kein What-If ausgew�hlt worden.");
		MessageBox(GetString(IDS_STRING61212));
	}
}

BOOL CAllWhatIf::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32822)); 

	CWnd *polWnd = GetDlgItem(IDC_NAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32823));
	}
	polWnd = GetDlgItem(IDC_PARAMETER); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32824));
	}
	polWnd = GetDlgItem(IDC_CREATED); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32825));
	}
	polWnd = GetDlgItem(IDC_DATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32826));
	}
	polWnd = GetDlgItem(IDC_EXPIRATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32827));
	}
	polWnd = GetDlgItem(IDC_NEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32828));
	}
	polWnd = GetDlgItem(IDC_USE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32829));
	}
	polWnd = GetDlgItem(IDC_KILL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32830));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32831));
	}

	m_AllWhatIfList.SetFont(&ogCourier_Bold_10);

	UpdateView();

	// Register DDX call back function
	TRACE("AllWhatIf: DDX Registration\n");

	ogCCSDdx.Register((void *)this,OPT_CHANGE,CString("Buttonlist OPT_CHANGE"), CString("OPT_CHANGE"),AllWhatIfCf);
	ogCCSDdx.Register((void *)this,OPT_USE,CString("Buttonlist OPT_USE"), CString("OPT_USE"),AllWhatIfCf);
	ogCCSDdx.Register((void *)this,OPT_DELETE,CString("Buttonlist OPT_DELETE"), CString("OPT_DELETE"),AllWhatIfCf);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAllWhatIf::UpdateView()
{

	m_AllWhatIfList.ResetContent();
	int ilCount = ogOptData.omData.GetSize();
	int ilRun   = 0;

	if (ilCount > 0)
	{
		do
		{
			OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
			if (strcmp(prlLocalOpt->Ctyp, "WHAT-IF") == 0)
			{
				char pclText[255];
				sprintf(pclText, "%-30s %-30s %-15s %-20s %-16s",
								prlLocalOpt->Ckey,
								prlLocalOpt->Para,
								prlLocalOpt->Erst,
								prlLocalOpt->Erda.Format("%d.%m.%Y %H:%M"),
								prlLocalOpt->Expr.Format("%d.%m.%Y %H:%M"));
				m_AllWhatIfList.AddString(pclText);
			}
			ilRun++;

		} while (ilRun < ilCount);
	}
}

void CAllWhatIf::OnCancel() 
{
	// Unregister DDX call back function
	TRACE("AllWhatIf: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);
	
	CDialog::OnCancel();
}

//void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName
static void AllWhatIfCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	CAllWhatIf *polAllWhatIf = (CAllWhatIf *)popInstance;

	if (ipDDXType == OPT_CHANGE)
		polAllWhatIf->WhatIfInsert((OptDataStruct *)vpDataPointer);
	else
	if (ipDDXType == OPT_DELETE)
		polAllWhatIf->WhatIfDelete((OptDataStruct *)vpDataPointer);
}

void CAllWhatIf::WhatIfInsert(OptDataStruct *prpOpt)
{
	if (strcmp(prpOpt->Ctyp,"WHAT-IF") == 0)
	{
		char pclText[255];
		sprintf(pclText, "%-30s %-30s %-15s %-20s %-16s",
						prpOpt->Ckey,
						prpOpt->Para,
						prpOpt->Erst,
						prpOpt->Erda.Format("%d.%m.%Y %H:%M"),
						prpOpt->Vato.Format("%d.%m.%Y %H:%M"));
		m_AllWhatIfList.AddString(pclText);
	}
}

void CAllWhatIf::WhatIfDelete(OptDataStruct *prpOpt)
{
	if (strcmp(prpOpt->Ctyp,"WHAT-IF") == 0)
	{
		int ilIndex = m_AllWhatIfList.FindString(0,prpOpt->Ckey);

		m_AllWhatIfList.DeleteString(ilIndex);
	}
}

