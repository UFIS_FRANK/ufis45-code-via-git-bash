// PrintFlightSelection.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <opsspm.h>
#include <PrintFlightSelection.h>
#include <CedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintFlightSelection dialog


CPrintFlightSelection::CPrintFlightSelection(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintFlightSelection::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintFlightSelection)
	m_SelectedFlight = 2;
	//}}AFX_DATA_INIT
}


void CPrintFlightSelection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintFlightSelection)
	DDX_Radio(pDX, IDC_ARR, m_SelectedFlight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintFlightSelection, CDialog)
	//{{AFX_MSG_MAP(CPrintFlightSelection)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintFlightSelection message handlers

BOOL CPrintFlightSelection::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_PRINTFLIGHTTITLE));

	CWnd *polWnd = NULL;

	if((polWnd = GetDlgItem(IDC_ARR)) != NULL)
	{
		polWnd->SetWindowText(ogFlightData.GetFlightNum(lmFlightArrUrno));
	}

	if((polWnd = GetDlgItem(IDC_DEP)) != NULL)
	{
		polWnd->SetWindowText(ogFlightData.GetFlightNum(lmFlightDepUrno));
	}

	if((polWnd = GetDlgItem(IDC_BOTH)) != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_BOTHFLIGHTS));
	}

	if((polWnd = GetDlgItem(IDOK)) != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}

	if((polWnd = GetDlgItem(IDCANCEL)) != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrintFlightSelection::OnOK() 
{
	UpdateData(TRUE);
	switch(m_SelectedFlight)
	{
		case 0:
			lmFlightDepUrno = 0L;
			break;
		case 1:
			lmFlightArrUrno = 0L;
			break;
		default:
			break;
	}
	
	CDialog::OnOK();
}
