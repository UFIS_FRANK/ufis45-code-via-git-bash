// Class for DLGTAB - Group delegations (Abordnungen) may require a different group name
// or the emp may require a different function (ie Guppenfuhrer), these are created for each
// pool job (created as a result of the delegation)
#ifndef _CEDADLGDATA_H_
#define _CEDADLGDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaJobData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

// Name         Null?    Type
// ------------ -------- ----
// DRRN                  CHAR(1)
// FCTC                  CHAR(5)
// HOPO                  CHAR(3)
// SDAY                  CHAR(8)
// UJOB                  CHAR(10)
// URNO         NOT NULL CHAR(10)
// WGPC                  CHAR(5)

 struct DlgDataStruct
{
	long Urno;
	char Wgpc[6];	// Group Code in WGPTAB
	long Ujob;		// URNO of employee in STFTAB
	char Sday[9];	// Date of deviation yyymmdd
	char Fctc[6];	// Function Code

	DlgDataStruct(void)
	{
		Urno = 0L;
		strcpy(Wgpc,"");
		Ujob = 0L;
		strcpy(Sday,"");
		strcpy(Fctc,"");
	}
};

typedef struct DlgDataStruct DLGDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDlgData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DLGDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUjobMap;
	CString GetTableName(void);

// Operations
public:
	CedaDlgData();
	~CedaDlgData();

	void ProcessDlgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadDlgData();
	DLGDATA* GetDlgByUrno(long lpUrno);
	void PrepareData(DLGDATA *prpDlg);
	DLGDATA *GetDlgByUjob(long lpUjob);
	DLGDATA *Insert(JOBDATA *prpPoolJob, CString opWgpc, CString opFctc);
	void Delete(long lpUrno);

private:
	DLGDATA *AddDlgInternal(DLGDATA &rrpDlg);
	void DeleteDlgInternal(long lpUrno);
	void ClearAll();
};

extern CedaDlgData ogDlgData;

#endif _CEDADLGDATA_H_
