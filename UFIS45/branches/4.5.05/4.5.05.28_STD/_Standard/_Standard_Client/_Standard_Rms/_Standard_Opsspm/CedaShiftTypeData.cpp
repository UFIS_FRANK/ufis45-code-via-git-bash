// CedaShiftTypeData.cpp - Class for handling Shiftloyee data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaShiftTypeData.h>
#include <BasicData.h>


CedaShiftTypeData::CedaShiftTypeData()
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(SHIFTTYPEDATA, ShiftTypeDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Bsdc,"BSDC")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Esbg,"ESBG")
		FIELD_CHAR_TRIM(Lsen,"LSEN")
		FIELD_CHAR_TRIM(Bkf1,"BKF1")
		FIELD_CHAR_TRIM(Bkt1,"BKT1")
		FIELD_CHAR_TRIM(Bkd1,"BKD1")
		FIELD_CHAR_TRIM(Bkdp,"BKDP")
		FIELD_CHAR_TRIM(Bsdn,"BSDN")
		FIELD_CHAR_TRIM(Bsds,"BSDS") //Singpaore
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ShiftTypeDataRecInfo)/sizeof(ShiftTypeDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ShiftTypeDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
	strcpy(pcmTableName,"BSDTAB");
    pcmFieldList = "URNO,BSDC,FCTC,ESBG,LSEN,BKF1,BKT1,BKD1,BKDP,BSDN,BSDS";
}

CedaShiftTypeData::~CedaShiftTypeData()
{
	TRACE("CedaShiftTypeData::~CedaShiftTypeData called\n");
	ClearAll();
}

void CedaShiftTypeData::ClearAll()
{
	omData.DeleteAll();
	omBsdcMap.RemoveAll();
	omUrnoMap.RemoveAll();
}

CCSReturnCode CedaShiftTypeData::ReadShiftTypeData()
{
    char pclWhere[512];
	CCSReturnCode ilRc = RCSuccess;

	// Select data from the database
	char pclCom[10] = "RT";
	sprintf(pclWhere,"WHERE VAFR <= '%s'", ogBasicData.GetTimeframeEndUtc().Format("%Y%m%d%H%M%S"));
    if (CedaAction2(pclCom, pclWhere) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaShiftTypeData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
        return RCFailure;
	}

    // Load data from CCSCedaData into the dynamic array of record
	ClearAll();

    for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		SHIFTTYPEDATA *prlShiftType = new SHIFTTYPEDATA;
		if ((ilRc = GetBufferRecord2(ilLc,prlShiftType)) == RCSuccess)
		{
			omData.Add(prlShiftType);
			omBsdcMap.SetAt(prlShiftType->Bsdc,prlShiftType);
			omUrnoMap.SetAt((void *)prlShiftType->Urno,prlShiftType);
		}
		else
		{
			delete prlShiftType;
		}
	}

	ogBasicData.Trace("CedaShiftTypeData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SHIFTTYPEDATA), pclWhere);
    return RCSuccess;
}

SHIFTTYPEDATA  *CedaShiftTypeData::GetShiftTypeById(const char *pcpShiftTypeId)
{
	return GetShiftTypeByCode(pcpShiftTypeId);
}

SHIFTTYPEDATA  *CedaShiftTypeData::GetShiftTypeByCode(const char *pcpShiftTypeId)
{
	SHIFTTYPEDATA  *prlShiftType;

	if (omBsdcMap.Lookup(pcpShiftTypeId,(void *& )prlShiftType) == TRUE)
	{
		return prlShiftType;
	}
	return NULL;
}

SHIFTTYPEDATA  *CedaShiftTypeData::GetShiftTypeByUrno(long lpUrno)
{
	SHIFTTYPEDATA  *prlShiftType;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlShiftType) == TRUE)
	{
		return prlShiftType;
	}
	return NULL;
}

void CedaShiftTypeData::GetAllShiftTypes(CStringArray& ropShiftTypes, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropShiftTypes.RemoveAll();
	}

	int ilNumShiftTypes = omData.GetSize();
	for (int ilShiftType = 0; ilShiftType < ilNumShiftTypes; ilShiftType++)
	{
		ropShiftTypes.Add(omData[ilShiftType].Bsdc);
	}
}


CString CedaShiftTypeData::GetTableName(void)
{
	return CString(pcmTableName);
}