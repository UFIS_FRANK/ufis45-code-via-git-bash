// PeakTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>

#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <BasicData.h>
#include <CCSPrint.h>
#include <PeakTable.h>

/*********
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
***************/

// Prototypes
static void PeakTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// PeakTable dialog

PeakTable::PeakTable(CWnd* pParent,CTime opStart)
	: CDialog(PeakTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(PeakTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	if (bgIsPreplanMode)
	{
			omDate = opStart;
	}
	else
	{
		omDate = opStart;
	}

	pomTable = new CTable;
    pomTable->tempFlag = 2;
	pomTable->SetSelectMode(0);
	ogPeaks.ReadPeakData();
	ogPeaks.Recalculate(omDate);
	
    CDialog::Create(PeakTable::IDD);
	CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.PKTB,ogCfgData.rmUserSetup.MONS);
    MoveWindow(&olRect);

	if (bgIsPreplanMode)
	{
			omDate = opStart;
	}
	else
	{
		omDate = opStart;
	}

	//CDialog::SetWindowText(CString("Peaksicht am ") + omDate.Format("%d%m%Y") );
	CDialog::SetWindowText(GetString(IDS_STRING61349) + omDate.Format("%d%m%Y") );

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

	UpdateDisplay();

}

PeakTable::~PeakTable()
{
	delete pomTable;	
	TRACE("PeakTable::~PeakTable()\n");
}


void PeakTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PeakTable)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(PeakTable, CDialog)
	//{{AFX_MSG_MAP(PeakTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PeakTable message handlers

BOOL PeakTable::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33005));
	CWnd *polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	polWnd = GetDlgItem(IDC_PEAKA); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33006));
	}
	polWnd = GetDlgItem(IDC_PEAKB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33007));
	}
	polWnd = GetDlgItem(IDC_PEAKCOMMON); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61682));
	}
	polWnd = GetDlgItem(IDC_PEAKSUMMARY); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33008));
	}


    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    MoveWindow(&rect);

	// Register DDX call back function
	TRACE("PeakTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("PEAKTABLE"),
		CString("Redisplay all from What-If"), PeakTableCf);	// for what-if changes
	ogCCSDdx.Register(this, JOB_NEW, CString("PEAKTABLE"),
		CString("Job New"), PeakTableCf);	// for what-if changes
	ogCCSDdx.Register(this, JOB_CHANGE, CString("PEAKTABLE"),
		CString("Job Changed"), PeakTableCf);	// for what-if changes
	ogCCSDdx.Register(this, JOB_DELETE, CString("PEAKTABLE"),
		CString("Job Deleted"), PeakTableCf);	// for what-if changes

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PeakTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("PeakTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void PeakTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
	AfxGetMainWnd()->SendMessage(WM_PEAKTABLE_EXIT);
}

void PeakTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
   if (nType != SIZE_MINIMIZED)
        pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

void PeakTable::UpdateDisplay()
{
  //  pomTable->SetHeaderFields("Peak|von|bis|Soll|Ist-FM|Ist-Ma|Soll|Ist-FM|Ist-Ma|Soll|Ist-FM|Ist-Ma");
  //  pomTable->SetFormatList("8|4|4|6|6|6|6|6|6|6|6");

    //pomTable->SetHeaderFields("Peak|von|bis|Soll|Ma.|Ma-I|FM|FM-I|Soll|Ma.|Ma-I|FM|FM-I|Soll|MA-I|FM-I|Soll|MA|MA-I|FM|FM-I");
    pomTable->SetHeaderFields(GetString(IDS_STRING61576));
    //pomTable->SetFormatList("8|4|4|6|6|6|6|6|6|6|6|6|6|6|6|6|6|6|6|6");
	pomTable->SetFormatList("8|4|4|5|5|5|5|5|5|5|5|5|5|5|5|4|6|5|5|5|5");

	omLines.DeleteAll();
	int nLineCount = 0;

	int ilPeakCount = ogPeaks.omData.GetSize();
    for (int i = 0; i < ilPeakCount; i++)
    {
		PEAKDATA tmpLineData = ogPeaks.omData[i];

		omLines.NewAt( nLineCount++, tmpLineData );
	}

	pomTable->ResetContent();
	nLineCount = omLines.GetSize();
	for(i = 0; i < nLineCount; i++) 
	{
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
	}
		
	pomTable->DisplayTable();

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomTable->GetCTableListBox()->SetFocus();
}

CString PeakTable::Format(PEAKDATA *prpPeak)
{
    CString s;
	char tmpText[128];


	prpPeak->SollSumme = prpPeak->SollA + prpPeak->SollB + prpPeak->SollHalle;
	prpPeak->DefaultMaSumme = prpPeak->DefaultMaA + prpPeak->DefaultMaB + prpPeak->DefaultMaHalle;
	prpPeak->DefaultFmSumme = prpPeak->DefaultFmA + prpPeak->DefaultFmB + prpPeak->DefaultFmHalle;
	prpPeak->PoolFmSumme = prpPeak->PoolFmA + prpPeak->PoolFmB + prpPeak->PoolFmHalle;
	prpPeak->PoolMaSumme = prpPeak->PoolMaA + prpPeak->PoolMaB + prpPeak->PoolMaHalle;

	s =  CString(prpPeak->Peid) + "|";
	s += CString(prpPeak->Tmfr) + "|";
	s += CString(prpPeak->Tmto) + "|";
	sprintf(tmpText,"%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",
			prpPeak->SollA,prpPeak->DefaultMaA,prpPeak->PoolMaA,prpPeak->DefaultFmA,prpPeak->PoolFmA,
			prpPeak->SollB,prpPeak->DefaultMaB,prpPeak->PoolMaB,prpPeak->DefaultFmB,prpPeak->PoolFmB,
			prpPeak->SollHalle,prpPeak->PoolMaHalle,prpPeak->PoolFmHalle,
			prpPeak->SollSumme,prpPeak->DefaultMaSumme,prpPeak->PoolMaSumme,
			prpPeak->DefaultFmSumme,prpPeak->PoolFmSumme);
	s += tmpText;

	return s;
}

BOOL PeakTable::DestroyWindow() 
{
	if ((bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// PeakTable -- implementation of DDX call back function

static void PeakTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	PeakTable *polTable = (PeakTable *)popInstance;

	switch(ipDDXType)
	{
	case REDISPLAY_ALL:
		polTable->UpdateDisplay();
		break;
	case JOB_NEW:
	case JOB_CHANGE:
	case JOB_DELETE:
		polTable->ProcessJobChange((JOBDATA *)vpDataPointer);
		break;
	default:
		;	// no default action
	}
}

void PeakTable::ProcessJobChange(JOBDATA *prpJob)
{
	// If it's a pool job, recalculate peak table
	if (strcmp(prpJob->Jtco, JOBPOOL) == 0)
	{
		ogPeaks.RecalculatePoolJobs(omDate);
		UpdateDisplay();
	}
}


BOOL PeakTable::PrintPeakLine(CCSPrint *pomPrint,PEAKDATA *prpPeak,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	char tmpBuf[124];

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 26680;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

	}
	else
	{

		if (pomPrint->imLineNo == 0)
		{

		/******************** first header line ***********************/

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 586;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("");
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 590;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_NOFRAME;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("Station A");

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 590;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_NOFRAME;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("Station B");

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 354;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_NOFRAME;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("Halle");
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 590;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_NOFRAME;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("Summe");
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();

			/******************** second header line ***********************/
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 350;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString("Peak ");
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 118;
			rlElement.Text       = "von";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 118;
			rlElement.Text       = "bis";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);




			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			rlElement.Length     = 118;
			rlElement.Text       = "Soll";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			rlElement.Text       = "MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);





			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = "Soll";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text       = "MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = "Soll";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text       = "Ist-MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = "Soll";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text       = "MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "Ist-MA";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.Text       = "FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.Text       = "Ist-FM";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			/****************
			if( pomPrint->imLineNo >= (pomPrint->imMaxLines-1) )
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter("Dispositions Arbeitsplatz:    ","Peakliste");
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					pomPrint->omCdc.EndPage();
				}
				PrintPeakHeader(pomPrint,opDutyDate);
			}
			*******************/

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		}
//		else
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 350;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = CString(prpPeak->Peid);
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 118;
			rlElement.Text       = prpPeak->Tmfr;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Text       = prpPeak->Tmto;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMETHIN;

			sprintf(tmpBuf,"%d",prpPeak->SollA);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->DefaultMaA);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolMaA);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->DefaultFmA);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolFmA);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->SollB);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->DefaultMaB);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolMaB);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->DefaultFmB);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolFmB);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->SollHalle);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->PoolMaHalle);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolFmHalle);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->SollSumme);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameRight = PRINT_FRAMETHIN;
			sprintf(tmpBuf,"%d",prpPeak->DefaultMaSumme);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolMaSumme);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->DefaultFmSumme);
			rlElement.Text       = tmpBuf;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			sprintf(tmpBuf,"%d",prpPeak->PoolFmSumme);
			rlElement.Text       = tmpBuf;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			rlElement.FrameTop   = PRINT_FRAMETHIN;

		}
	}

	return TRUE;
}

BOOL PeakTable::PrintPeakHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();

	return TRUE;
}




void PeakTable::OnPrint() 
{
//	omTable.GetCTableListBox()->SetFocus();


	CString omTarget = CString("F�r den ") + CString(omDate.Format("%d.%m.%Y"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(this,PRINT_LANDSCAPE,80,500,150,
		CString("Peakliste"),olPrintDate,omTarget);
	pomPrint->imLineNo = pomPrint->imMaxLines + 1;
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Peakliste";
			pomPrint->imLineHeight = 82;
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if (pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if ( pomPrint->imPageNo > 0)
					{
						pomPrint->PrintFooter("Dispositions Arbeitsplatz:    ","Peakliste");
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					pomPrint->imLineNo = 0;
					PrintPeakHeader(pomPrint);
				}

				PrintPeakLine(pomPrint,&omLines[i],FALSE);
			}
			pomPrint->PrintFooter("Dispositions Arbeitsplatz:    ","Peakliste");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	}

	delete pomPrint;
	pomPrint = NULL;
	/*
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->PreplanPrint(&ogShiftData, (char *)(const char *)omDate);
	delete prn;
	*/
	
}
