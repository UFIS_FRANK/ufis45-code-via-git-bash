#if !defined(AFX_ASSIGNCONFLICTDLG_H__DE348D64_815A_11D3_9278_0000B4392C49__INCLUDED_)
#define AFX_ASSIGNCONFLICTDLG_H__DE348D64_815A_11D3_9278_0000B4392C49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AssignConflictDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAssignConflictDlg dialog

class CAssignConflictDlg : public CDialog
{
// Construction
public:
	CAssignConflictDlg(CWnd* pParent = NULL);   // standard constructor

	CStringArray omConflicts;
	CStringArray omDemands;
	int imSelectedDemandIndex;

// Dialog Data
	//{{AFX_DATA(CAssignConflictDlg)
	enum { IDD = IDD_ASSIGN_CONFLICT_DLG };
	CStatic	m_ConflictList;
	CListBox	m_DemandList;
	CString	m_Title;
	int		m_Choice;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignConflictDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAssignConflictDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeDemandname();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ASSIGNCONFLICTDLG_H__DE348D64_815A_11D3_9278_0000B4392C49__INCLUDED_)
