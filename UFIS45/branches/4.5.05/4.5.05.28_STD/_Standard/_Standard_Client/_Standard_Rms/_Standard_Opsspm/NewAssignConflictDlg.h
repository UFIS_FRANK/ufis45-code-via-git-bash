#if !defined(AFX_NEWASSIGNCONFLICTDLG_H__A2C375A4_A33C_11D4_BFFD_00010215BFE5__INCLUDED_)
#define AFX_NEWASSIGNCONFLICTDLG_H__A2C375A4_A33C_11D4_BFFD_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewAssignConflictDlg.h : header file
//
#include <tscale.h>
#include <NewAssignConflictViewer.h>
#include <NewAssignConflictGantt.h>

/////////////////////////////////////////////////////////////////////////////
// CNewAssignConflictDlg dialog

class CNewAssignConflictDlg : public CDialog
{
// Construction
public:
	CNewAssignConflictDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAssignConflictDlg)
	enum { IDD = IDD_ASSIGNCONFLICTS };
	CButton	m_NonTeamDemandsCtrl;
	CStatic	m_StatusBar;
	CStatic	m_ExplanationCtrl;
	CString	m_Explanation;
	BOOL	m_DisplayNonTeamDemands;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAssignConflictDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAssignConflictDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnNonTeamDemandsClicked();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


	CCSPtrArray <DEMANDDATA> omDemands;
	CCSPtrArray <DEMANDDATA> omTeamDemands;
	CCSPtrArray <DEMANDDATA> omNonTeamDemands;
	CCSPtrArray <JOBDATA> omJobs;
	CCSPtrArray <JODDATA> omJods;
	CCSPtrArray <JOBDATA> omNoDemandJobs;
	FLIGHTDATA *prmFlight, *prmRotation;
	JODDATA rmJod;
	CString omRegn;

	CString omAllocUnitType;

    CTimeScale omTimeScale;
    CStatusBar omStatusBar;
    
    int imStartTimeScalePos;
    int imBottomPos;

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;

	NewAssignConflictViewer omViewer;

public:
	void DisplayGanttData();
	NewAssignConflictGantt omGantt;
	void SetJobsAndDemands(JOBDATA *prpJob, DEMANDDATA *prpDemand);
	void SetJobsAndDemands(CCSPtrArray <JOBDATA> &ropJobs, CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <JODDATA> &ropJods);
	void SetJobsAndDemands(CCSPtrArray <JOBDATA> &ropJobs, CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <JODDATA> &ropJods, CCSPtrArray <DEMANDDATA> &ropNonTeamDemands);
	void GetJods(CCSPtrArray <JODDATA> &ropJods);
	void SetFlightPtr(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation = NULL);
	void SetRegn(const char *pcpRegn);
	bool JobIsAssigned(JOBDATA *prpJob);

private:      
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
	long lmOldUdem;

public:
	bool bmScrolling;
	int  imScrollSpeed;
	void AutoScroll(UINT ipInitialScrollSpeed = 200);
	void OnAutoScroll(void);
    CCSDragDropCtrl m_NewAssignConflictDlgDragDrop;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWASSIGNCONFLICTDLG_H__A2C375A4_A33C_11D4_BFFD_00010215BFE5__INCLUDED_)
