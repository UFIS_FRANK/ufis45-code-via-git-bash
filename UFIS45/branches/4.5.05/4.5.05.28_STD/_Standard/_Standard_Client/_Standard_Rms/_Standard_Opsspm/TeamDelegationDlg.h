#if !defined(AFX_TEAMDELEGATIONDLG_H__E65C1771_BAD0_11D4_801F_00010215BFE5__INCLUDED_)
#define AFX_TEAMDELEGATIONDLG_H__E65C1771_BAD0_11D4_801F_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TeamDelegationDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTeamDelegationDlg dialog

class CTeamDelegationDlg : public CDialog
{
// Construction
public:
	CTeamDelegationDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTeamDelegationDlg)
	enum { IDD = IDD_TEAM_DELEGATION_DLG };
	CComboBox	m_TemplateCombo;
	CComboBox	m_TeamCombo;
	CStatic	m_FromTitleStatic;
	CButton	m_FunctionsButton;
	CStatic	m_PoolTitleStatic;
	CComboBox	m_PoolCombo;
	CListBox	m_EmpList;
	CString	m_ToTitleStatic;
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


	CStringArray	omEmpList;
	CStringArray	omFunctionList;
	CString			omPool;
	CString			omTeam;
	CString			omTemplate;
	long			lmUtpl;
	CTime			omFrom;
	CTime			omTo;
	CTime			omMinFrom;
	CTime			omMaxTo;

	bool			bmCheckTimes;
	void SetEmpList(void);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTeamDelegationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTeamDelegationDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnFunctions();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEAMDELEGATIONDLG_H__E65C1771_BAD0_11D4_801F_00010215BFE5__INCLUDED_)
