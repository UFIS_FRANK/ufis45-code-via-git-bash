// GateTable.h : header file
//
#ifndef __GATETABLE_H__
#define __GATETABLE_H__

#include <GateTableViewer.h>

/////////////////////////////////////////////////////////////////////////////
// GateTable dialog

class GateTable : public CDialog
{
// Construction
public:
	GateTable(CWnd* pParent = NULL);   // standard constructor
	~GateTable();
	void UpdateView();
	void SetCaptionText(void);


private :
    CTable *pomTable; // visual object, the table content
	BOOL bmIsViewOpen;
	GateTableViewer omViewer;
	int m_nDialogBarHeight;
	CRect omWindowRect; //PRF 8712

private:
	static void GateTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void HandleGlobalDateUpdate();
	void SetViewerDate();
	void UpdateComboBox();

// Dialog Data
	//{{AFX_DATA(GateTable)
	enum { IDD = IDD_GATETABLE };
	CComboBox	m_Date;
	//}}AFX_DATA

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GateTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GateTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnView();
	afx_msg void OnPrint();
	afx_msg void OnCloseupViewcombo();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTableUpdateDataCount(); //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long imDropFlightUrno;
	long imDropDemandUrno;
};

#endif //__GATETABLE_H__
