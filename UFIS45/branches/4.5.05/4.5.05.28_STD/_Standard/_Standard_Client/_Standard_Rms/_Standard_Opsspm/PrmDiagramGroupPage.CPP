// ccidiagr.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <PrmDiagramGroupPage.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
static CString ogGroupKeys[] =
	{ "Hall","Region","Line","Halle" };

#define NUMBER_OF_GROUPKEYS	(sizeof(ogGroupKeys) / sizeof(ogGroupKeys[0]))

/////////////////////////////////////////////////////////////////////////////
// PRMDiagramGroupPage property page

IMPLEMENT_DYNCREATE(PRMDiagramGroupPage, CPropertyPage)

PRMDiagramGroupPage::PRMDiagramGroupPage() : CPropertyPage(PRMDiagramGroupPage::IDD)
{
	//{{AFX_DATA_INIT(PRMDiagramGroupPage)
	m_GroupBy = -1;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING61618);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

PRMDiagramGroupPage::~PRMDiagramGroupPage()
{
}

void PRMDiagramGroupPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_GroupBy = GetGroupId(omGroupBy);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PRMDiagramGroupPage)
	DDX_Radio(pDX, IDC_RADIO1, m_GroupBy);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omGroupBy = GetGroupKey(m_GroupBy);
		GetParent()->SendMessage(WM_PRMDIAGRAM_GROUPPAGE_CHANGED, 0, 0);
	}
}


BEGIN_MESSAGE_MAP(PRMDiagramGroupPage, CPropertyPage)
	//{{AFX_MSG_MAP(PRMDiagramGroupPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PRMDiagramGroupPage message handlers

BOOL PRMDiagramGroupPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// PRMDiagramGroupPage -- helper routines

int PRMDiagramGroupPage::GetGroupId(const char *pcpGroupKey)
{
	for (int i = 0; i < NUMBER_OF_GROUPKEYS; i++)
		if (ogGroupKeys[i] == pcpGroupKey)
			return i;

	// If there is no groupping matched, assume first groupping
	return 0;
}

CString PRMDiagramGroupPage::GetGroupKey(int ipGroupId)
{
	if (0 <= ipGroupId && ipGroupId <= NUMBER_OF_GROUPKEYS-1)
		return ogGroupKeys[ipGroupId];

	return "";	// invalid groupping id
}
BOOL PRMDiagramGroupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	//SetWindowText(GetString(IDS_STRING61618)); 

	CWnd *polWnd = GetDlgItem(IDC_RADIO1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32898));
	}

	polWnd = GetDlgItem(IDC_RADIO2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61964));
	}

	polWnd = GetDlgItem(IDC_RADIO3); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61955));
	}

	polWnd = GetDlgItem(IDC_RADIO4); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32897));
	}
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
