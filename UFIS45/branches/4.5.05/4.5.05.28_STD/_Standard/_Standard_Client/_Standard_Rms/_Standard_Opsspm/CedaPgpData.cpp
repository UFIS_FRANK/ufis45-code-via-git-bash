// CedaPgpData.cpp - Class for PGPTAB - Planning Groups

#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaPgpData.h>
#include <BasicData.h>

extern CCSDdx ogCCSDdx;
void  ProcessPgpCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaPgpData::CedaPgpData()
{                  
    BEGIN_CEDARECINFO(PGPDATA, PGPDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Pgpc,"PGPC")
		FIELD_CHAR_TRIM(Pgpm,"PGPM")
		FIELD_CHAR_TRIM(Type,"TYPE")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PGPDATARecInfo)/sizeof(PGPDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PGPDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"PGPTAB");
    pcmFieldList = "URNO,PGPC,PGPM,TYPE";

	ogCCSDdx.Register((void *)this,BC_PGP_CHANGE,CString("PGPDATA"), CString("Pgp changed"),ProcessPgpCf);
	ogCCSDdx.Register((void *)this,BC_PGP_DELETE,CString("PGPDATA"), CString("Pgp deleted"),ProcessPgpCf);
}
 
CedaPgpData::~CedaPgpData()
{
	TRACE("CedaPgpData::~CedaPgpData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaPgpData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

void  ProcessPgpCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaPgpData *)popInstance)->ProcessPgpBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaPgpData::ProcessPgpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPgpData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlPgpData->Selection);
	PGPDATA *prlPgp = GetPgpByUrno(llUrno);
	ogBasicData.LogBroadcast(prlPgpData);

	if(ipDDXType == BC_PGP_CHANGE && prlPgp != NULL)
	{
		// update
		//ogBasicData.Trace("BC_PGP_CHANGE - UPDATE PGPTAB URNO %ld\n",prlPgp->Urno);
		GetRecordFromItemList(prlPgp,prlPgpData->Fields,prlPgpData->Data);
		ogCCSDdx.DataChanged((void *)this,PGP_CHANGE,(void *)prlPgp);
	}
	else if(ipDDXType == BC_PGP_CHANGE && prlPgp == NULL)
	{
		// insert
		PGPDATA rlPgp;
		GetRecordFromItemList(&rlPgp,prlPgpData->Fields,prlPgpData->Data);
		//ogBasicData.Trace("BC_PGP_CHANGE - INSERT PGPTAB URNO %ld\n",rlPgp.Urno);
		prlPgp = AddPgpInternal(rlPgp);
		ogCCSDdx.DataChanged((void *)this,PGP_CHANGE,(void *)prlPgp);
	}
	else if(ipDDXType == BC_PGP_DELETE && prlPgp != NULL)
	{
		// delete
		long llPgpUrno = prlPgp->Urno;
		//ogBasicData.Trace("BC_PGP_DELETE - DELETE PGPTAB URNO %ld\n",llPgpUrno);
		DeletePgpInternal(llPgpUrno);
		ogCCSDdx.DataChanged((void *)this,PGP_DELETE,(void *)llPgpUrno);
	}
}

CCSReturnCode CedaPgpData::ReadPgpData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    sprintf(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPgpData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		PGPDATA rlPgpData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPgpData)) == RCSuccess)
			{
				AddPgpInternal(rlPgpData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPgpData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(PGPDATA), pclWhere);
    return ilRc;
}


PGPDATA *CedaPgpData::AddPgpInternal(PGPDATA &rrpPgp)
{
	PGPDATA *prlPgp = new PGPDATA;
	*prlPgp = rrpPgp;
	PrepareData(prlPgp);
	omData.Add(prlPgp);
	omUrnoMap.SetAt((void *)prlPgp->Urno,prlPgp);
	PrepareFunctionWeights(prlPgp);
	return prlPgp;
}

void CedaPgpData::DeletePgpInternal(long lpUrno)
{
	int ilNumPgps = omData.GetSize();
	for(int ilDel = (ilNumPgps-1); ilDel >= 0; ilDel--)
	{
		PGPDATA *prlPgp = &omData[ilDel];
		if(prlPgp->Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaPgpData::PrepareData(PGPDATA *prpPgp)
{
}

PGPDATA* CedaPgpData::GetPgpByUrno(long lpUrno)
{
	PGPDATA *prlPgp = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPgp);
	return prlPgp;
}

CString CedaPgpData::GetGroupLeaderFunction(long lpUrno)
{
	CString olGroupLeaderFunction;
	PGPDATA *prlPgp = GetPgpByUrno(lpUrno);
	if(prlPgp != NULL)
	{
		olGroupLeaderFunction = prlPgp->Pgpm;
		int ilSepPos = olGroupLeaderFunction.Find(PGP_SEPERATOR);
		if(ilSepPos != -1)
		{
			olGroupLeaderFunction = olGroupLeaderFunction.Left(ilSepPos);
		}
	}

	return olGroupLeaderFunction;

}

CString CedaPgpData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaPgpData::PrepareFunctionWeights(PGPDATA *prpPgp)
{
	if(prpPgp != NULL)
	{
		prpPgp->FunctionWeights.RemoveAll();

		CStringArray olFunctions;
		ogBasicData.ExtractItemList(prpPgp->Pgpm, &olFunctions, PGP_SEPERATOR);
		int ilNumFunctions = olFunctions.GetSize();
		for(int ilF = 0; ilF < ilNumFunctions; ilF++)
		{
			prpPgp->FunctionWeights.SetAt(olFunctions[ilF], (void *) ilF);
		}
	}
}

int CedaPgpData::GetFunctionWeight(long lpUrno, const char *pcpFunction)
{
	int ilWeight = 9999; // emps whose function is not found within the group should be displayed last
	PGPDATA *prlPgp = GetPgpByUrno(lpUrno);
	if(prlPgp != NULL)
	{
		if(!prlPgp->FunctionWeights.Lookup(pcpFunction, (void *&) ilWeight))
			ilWeight = 9999;
	}

	return ilWeight;
}

