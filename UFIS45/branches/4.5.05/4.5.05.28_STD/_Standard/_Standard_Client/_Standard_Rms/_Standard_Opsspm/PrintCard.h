#ifndef _PRINTCARD_H_
#define _PRINTCARD_H_

#include <CCSPtrArray.h>
#include <CCSGlobl.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaJobData.h>
#include <CedaTlxData.h>

//-----------------------------------------------------------------------------

class PrintCard : public CObject
{
public:
//	types
	enum egPrintCardInfo
	{
		PRINTCARD_LANDSCAPE,PRINTCARD_PORTRAIT,
		PRINTCARD_LEFT,PRINTCARD_RIGHT,PRINTCARD_CENTER,
		PRINTCARD_NOFRAME,PRINTCARD_FRAMETHIN,PRINTCARD_FRAMEMEDIUM,PRINTCARD_FRAMETHICK,
		PRINTCARD_SMALL,PRINTCARD_MEDIUM,PRINTCARD_LARGE,
		PRINTCARD_SMALLBOLD,PRINTCARD_MEDIUMBOLD,PRINTCARD_LARGEBOLD
	};

	enum PRINT_OPTION{PRINT_PRINTER,PRINT_PREVIEW};

// Functions
public:
				PrintCard(CWnd *opParent = NULL);
	~			PrintCard();
	void		SetBitmaps(CBitmap *popBitmap = NULL);
	bool		InitializePrinter(egPrintCardInfo ipOrientation = PRINTCARD_PORTRAIT);
	bool		InitializePrinter(CDC *popCDC, egPrintCardInfo ipOrientation = PRINTCARD_PORTRAIT); //lli: for Singapore PRM Phase 2
	bool		InitializePrintSetup(PRINT_OPTION epPrintOption = PRINT_PRINTER, egPrintCardInfo ipOrientation = PRINTCARD_PORTRAIT); //lli: for Singapore PRM Phase 2
	bool		Print(FLIGHTDATA *prpArrFlight,FLIGHTDATA *prpDepFlight, CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <JOBDATA> &ropJobsWithoutDemands);
	bool		Print(int ipPageNumber);
	bool		GetCardData(FLIGHTDATA *prpArrival,FLIGHTDATA *prpDeparture,CCSPtrArray<DEMANDDATA>& ropDemands,CCSPtrArray <JOBDATA> &ropJobsWithoutDemands);	

private:

	struct CARDDUTYDATA
	{
		long	Ujob;		// job urno
		long	Ustf;		// staff urno
		CTime	Acfr;		// actual begin of job
		CTime	Acto;		// actual end of job
		long	GHS_Urno;
		CString GHS_Lknm;
		CString DSR_Esnm;
		CString OpssPmRemark;
		CString DemandFunction;
		CString Team;
		
		CARDDUTYDATA(void)
		{ 
			Acfr = TIMENULL;
			Acto = TIMENULL;
		}
	};

//----------------------------------------------------------------------------

	class CardData
	{
	public:

		CardData();
		~CardData();
		
		CString AFT_Regn;	// registration	
		CString AFT_Act3;	// aircraft 3 letter code			
		CString AFT_Act5;	// aircraft 5 letter code	

		//arival
		long	AFT_UrnoA;	//	
		CString AFT_FlnoA;
		CTime	AFT_Tifa;
		CTime	AFT_Stoa;
		CTime	AFT_Etoa;
		CString AFT_Psta;
		CString	AFT_Org3;
		CString	AFT_Via3A;
		CString	AFT_Gta1;
		CString	AFT_TtypA;
		int		AFT_PaxtA;	// total passengers
		int		AFT_CgotA;	// 
		int		AFT_BagnA;
		int		AFT_MailA;
		CString AFT_Blt1A;

		//departure
		long	AFT_UrnoD;
		CString AFT_FlnoD;
		CTime	AFT_Tifd;
		CTime	AFT_Stod;
		CTime	AFT_Etod;
		CString AFT_CIC;
		CString AFT_Pstd;
		CString	AFT_Des3;
		CString	AFT_Via3D;
		CString	AFT_Gtd1;
		CString	AFT_TtypD;
		int		AFT_PaxtD;
		int		AFT_CgotD;
		int		AFT_BagnD;
		int		AFT_MailD;
		CString AFT_Blt1D;

		CString GHP_Rema;	// rule name
		CString TLX_Text;	// telex text	

		int imGhdLines;
		int imRemaLines;
		int imTelexLines;
		CCSPtrArray<CARDDUTYDATA> CardDutyData;

		CardData(const CardData& Cd);
		const CardData& operator= ( const CardData& Cd);

	};


public:
// Variables
	PRINT_OPTION emPrintOption;
	int imMaxPage;
	int imPreviewPage;
	egPrintCardInfo	imOrientation;
	CDC* pomCdc;

private:
// Functions

	void PrintHeader();
	void PrintTable();
	void PrintRemark();
	void PrintFooter();
	void PrintTelex();
	BOOL LoadBitmapFromBMPFile(LPTSTR szFileName,HBITMAP *phBitmap,HPALETTE *phPalette);
	CString FormatTelexTypeString(CString opTelexTypeString);
	void CheckForLastTelex(CCSPtrArray <TLXDATA> &ropTlxData);

// Variables
	CWnd *pomParent;

	CDC	 omCdc;
	CDC	 omMemDc;
	CDC	 omCcsMemDc;

	CRgn omRgn;

	CFont omCourierNew_Regular_8;
	CFont omCourierNew_Bold_8;
	CFont omArial_Regular_8;
	CFont omArial_Regular_10;
	CFont omArial_Regular_12;
	CFont omArial_Regular_18;
	CFont omArial_Bold_8;
	CFont omArial_Bold_10;
	CFont omArial_Bold_11;
	CFont omArial_Bold_12;
	CFont omArial_Bold_18;

    CPen omThinPen;
    CPen omMediumPen;
    CPen omThickPen;
	CPen omDottedPen;

	CStringArray omGhdUrnos;
    CCSPtrArray<CardData> omCardData;

	CString	omCardName;

	CBitmap *pomBitmap;
	CBitmap omBitmap;

	int imCardNo;
	int imLineHeight;
	int imFirstPos;
	int imActualPos;
	int imMaxLines;
	int	imActualLine;
	int imActualPage;
	int imLeftOffset;
	int	imLogPixelsY;
	int	imLogPixelsX;
	int	imNrOfUrnos;
	bool bmIsInitialized;
	HBITMAP		hmBitmap;
	HPALETTE	hmPalette;

};

#endif


