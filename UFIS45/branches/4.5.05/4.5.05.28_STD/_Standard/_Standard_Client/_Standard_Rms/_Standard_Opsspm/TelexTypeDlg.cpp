// TelexTypeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <TelexTypeDlg.h>
#include <BasicData.h>
#include <ccsglobl.h>
#include <CedaCfgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTelexTypeDlg dialog


CTelexTypeDlg::CTelexTypeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTelexTypeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTelexTypeDlg)
	//}}AFX_DATA_INIT
}


void CTelexTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTelexTypeDlg)
	DDX_Control(pDX, IDC_TELEXTYPELIST, m_TelexTypeListCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTelexTypeDlg, CDialog)
	//{{AFX_MSG_MAP(CTelexTypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTelexTypeDlg message handlers

BOOL CTelexTypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_TTD_TITLE));
	GetDlgItem(IDC_TEXT)->SetWindowText(GetString(IDS_TTD_TEXT));
	GetDlgItem(IDOK)->SetWindowText(GetString(IDS_TTD_OK));
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_TTD_CANCEL));

	if(ogBasicData.omTelexTypes.IsEmpty())
	{
		GetDlgItem(IDC_TEXT)->SetWindowText(GetString(IDS_TTD_NOTELEXTYPES));
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
	else
	{
		CStringArray olTelexTypes, olTelexTypesToLoad;
		ogBasicData.omTelexTypesToLoad = ogCfgData.GetValueByCtypAndCkey(TELEXTYPE_KEY, TELEXTYPE_TEXT, ogBasicData.omTelexTypes); // default is all telexes
		ogBasicData.ExtractItemList(ogBasicData.omTelexTypesToLoad, &olTelexTypesToLoad);

		int ilNumTelexTypesToLoad = olTelexTypesToLoad.GetSize();
		ogBasicData.ExtractItemList(ogBasicData.omTelexTypes, &olTelexTypes);
		int ilNumTelexTypes = olTelexTypes.GetSize();
		for(int ilTT = 0; ilTT < ilNumTelexTypes; ilTT++)
		{
			int ilLine = m_TelexTypeListCtrl.AddString(olTelexTypes[ilTT]);
			if(ilLine != LB_ERR)
			{
				//bool blSelect = (prlCfg == NULL) ? true : false;
				bool blSelect = false;
				for(int ilT = 0; ilT < ilNumTelexTypesToLoad; ilT++)
				{
					if(olTelexTypesToLoad[ilT] == olTelexTypes[ilTT])
						m_TelexTypeListCtrl.SetSel(ilLine);
				}
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTelexTypeDlg::OnOK() 
{
	if(!ogBasicData.omTelexTypes.IsEmpty())
	{
		CString olSelectedTelexes;
		int ilNumSelLines = m_TelexTypeListCtrl.GetSelCount();
		if(ilNumSelLines != LB_ERR && ilNumSelLines > 0)
		{
			CString olTelexType;
			int *pilSelectedLines = new int[ilNumSelLines];
			m_TelexTypeListCtrl.GetSelItems(ilNumSelLines,pilSelectedLines);

			for(int ilT = 0; ilT < ilNumSelLines; ilT++)
			{
				m_TelexTypeListCtrl.GetText(pilSelectedLines[ilT], olTelexType);
				if(!olSelectedTelexes.IsEmpty())
				{
					olSelectedTelexes += ",";
				}
				olSelectedTelexes += olTelexType;
			}
			delete [] pilSelectedLines;
		}
		ogCfgData.DeleteByCtyp(TELEXTYPE_KEY);
		ogCfgData.CreateCfg(TELEXTYPE_KEY, TELEXTYPE_TEXT, olSelectedTelexes);
		ogBasicData.omTelexTypesToLoad = olSelectedTelexes;
		ogBasicData.omTelexTypesPrintOnlyLast = olSelectedTelexes;
	}
	
	CDialog::OnOK();
}
