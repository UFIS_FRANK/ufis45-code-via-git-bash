// TransactionManager.cpp : implementation file
//
#include <stdafx.h>
#include <opsspm.h>
#include <TransactionManager.h>
#include <CedaJobData.h>
#include <CedaJodData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTransactionManager

CTransactionManager::CTransactionManager()
{
}

void CTransactionManager::Start(void)
{
	ogJobData.StartTransaction();
	ogJodData.StartTransaction();
}

void CTransactionManager::End(void)
{
	ogJobData.CommitTransaction();
	ogJodData.CommitTransaction();
}
