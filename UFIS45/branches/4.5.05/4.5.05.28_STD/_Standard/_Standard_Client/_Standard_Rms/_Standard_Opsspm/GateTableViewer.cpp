// ccitblvw.cpp -- the viewer for Gate Table
//
// Written by:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	The old version of this viewer is just enough for primitive displaying.
//	Now we like to have some more drag-and-drop functionality for the Gate Table.
//	So we have to rewrite this viewer to display each flight job in the proper column
//	(each two-columns on the right-side of the table represents one demand or one job,
//	in the other word, it is the same as an indicator in the Gate Diagram.)
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaDemandData.h>
#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>
#include <GateTableViewer.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <CedaAltData.h>

class GateTable; //Singapore

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_GATE,
	BY_FNUM,
	BY_IOFL,
	BY_TIME,
	BY_NONE
};


/////////////////////////////////////////////////////////////////////////////
// GateTableViewer

GateTableViewer::GateTableViewer()
{
    SetViewerKey("GateTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW, CString("GTVIEWER"), CString("Job New"), GateTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("GTVIEWER"), CString("Job Change"), GateTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("GTVIEWER"), CString("Job Delete"), GateTableCf);
    ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("GTVIEWER"), CString("Flight Change"), GateTableCf);
    ogCCSDdx.Register(this, FLIGHT_SELECT, CString("GTVIEWER"), CString("Flight Select"), GateTableCf);

	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
}

GateTableViewer::~GateTableViewer()
{
	TRACE("GateTableViewer: DDX Unregistration \n");
    ogCCSDdx.UnRegister(this, NOTUSED);

DeleteAll();
}

void GateTableViewer::Attach(CTable *popTable)
{
	pomTable = popTable;
}

void GateTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void GateTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.GBLV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter();
    PrepareGrouping();

    DeleteAll();    
    MakeLines();
	MakeIndicators();
	UpdateDisplay();
}

void GateTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

	omGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Gate")
            omGroupBy = BY_GATE;
        else if (olSortOrder[0] == "Fnum")
            omGroupBy = BY_FNUM;
        else if (olSortOrder[0] == "Iofl")
            omGroupBy = BY_IOFL;
        else if (olSortOrder[0] == "Time")
            omGroupBy = BY_TIME;
    }
}


void GateTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Gate", olFilterValues);
    omCMapForGate.RemoveAll();
    n = olFilterValues.GetSize();
    for (i = 0; i < n; i++)
        omCMapForGate[olFilterValues[i]] = NULL;

    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
    for (i = 0; i < n; i++)
        omCMapForAlcd[olFilterValues[i]] = NULL;

//	CTime olTmpDate = ogBasicData.GetTime();
//	omStartTime = CTime(olTmpDate.GetYear(),olTmpDate.GetMonth(),olTmpDate.GetDay(),0,0,0);
//	omEndTime = CTime(olTmpDate.GetYear(),olTmpDate.GetMonth(),olTmpDate.GetDay(),0,0,0);
//	GetFilter("Zeit", olFilterValues);
//	int ilDayOffset = olFilterValues[2][0] - '0';
//	CString olFrom = olFilterValues[0];
//	CString olTo = olFilterValues[1];
//	omStartTime += CTimeSpan(ilDayOffset,atoi(olFrom.Left(2)),atoi(olFrom.Right(2)),0);
//	omEndTime += CTimeSpan(ilDayOffset,atoi(olTo.Left(2)),atoi(olTo.Right(2)),0);
//	omDate = omStartTime.Format("%Y%m%d");
}

void GateTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Gate")
            ilSortOrderEnumeratedValue = BY_GATE;
        else if (olSortOrder[i] == "Fnum")
            ilSortOrderEnumeratedValue = BY_FNUM;
        else if (olSortOrder[i] == "Iofl")
            ilSortOrderEnumeratedValue = BY_IOFL;
        else if (olSortOrder[i] == "Time")
            ilSortOrderEnumeratedValue = BY_TIME;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int GateTableViewer::CompareGate(GATEDATA_LINE *prpGate1, GATEDATA_LINE *prpGate2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_GATE:
            ilCompareResult = (prpGate1->Alid == prpGate2->Alid)? 0:
                (prpGate1->Alid > prpGate2->Alid)? 1: -1;
            break;
        case BY_FNUM:
            ilCompareResult = (prpGate1->Fnum == prpGate2->Fnum)? 0:
                (prpGate1->Fnum > prpGate2->Fnum)? 1: -1;
            break;
        case BY_IOFL:
            ilCompareResult = (prpGate1->FlightIofl == prpGate2->FlightIofl)? 0:
                (prpGate1->FlightIofl > prpGate2->FlightIofl)? 1: -1;
            break;
        case BY_TIME:
            ilCompareResult = (prpGate1->FlightTime == prpGate2->FlightTime)? 0:
                (prpGate1->FlightTime > prpGate2->FlightTime)? 1: -1;
            break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   
}


BOOL GateTableViewer::IsSameGroup(GATEDATA_LINE *prpGate1,
	GATEDATA_LINE *prpGate2)
{


    // Compare in the sort order, from the outermost to the innermost
    switch (omGroupBy)
    {
    case BY_GATE:
        return (prpGate1->Alid == prpGate2->Alid);
    case BY_FNUM:
        return (prpGate1->Fnum == prpGate2->Fnum);
    case BY_IOFL:
        return (prpGate1->FlightIofl == prpGate2->FlightIofl);
    case BY_TIME:
        return (prpGate1->FlightTime == prpGate2->FlightTime);
    }

    return TRUE;    // assume there's no grouping at all
}


BOOL GateTableViewer::IsPassFilter(const char *pcpGate, FLIGHTDATA *prpFlight)
{
	CString olAlc = ogAltData.FormatAlcString(prpFlight->Alc2,prpFlight->Alc3);


    void *p;
    BOOL blIsGateOk = omCMapForGate.Lookup(CString(pcpGate), p);
    BOOL blIsAlcdOk = omCMapForAlcd.Lookup(CString(olAlc), p);

	BOOL blIsTimeOk = FALSE;
	if(ogFlightData.IsArrival(prpFlight))
	{
		blIsTimeOk = (omStartTime < prpFlight->Stoa && prpFlight->Stoa < omEndTime);
	}
	else
	{
		blIsTimeOk = (omStartTime < prpFlight->Stod && prpFlight->Stod < omEndTime);
	}

    return (blIsGateOk && blIsAlcdOk && blIsTimeOk);
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void GateTableViewer::MakeLines()
{
	CCSPtrArray <ALLOCUNIT> olGates;
	ogAllocData.GetUnitsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGates);
	int ilNumGates = olGates.GetSize();
	for(int ilGate = 0; ilGate < ilNumGates; ilGate++)
	{
		ALLOCUNIT *prlGate = &olGates[ilGate];
		MakeLine(prlGate);
	}
//	int nGateArea = ogGateAreas.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < nGateArea; i++)
//    {
//		METAALLOCDATA *prlMeta = &ogGateAreas.omMetaAllocUnitList[i];
//		MakeLine(prlMeta);
//	}
}

//void GateTableViewer::MakeLine(METAALLOCDATA *prpData)
void GateTableViewer::MakeLine(ALLOCUNIT *prpGate)
{
//	int ilGateCount = prpData->AllocationUnits.GetSize();
//	for (int ilGateno = 0; ilGateno < ilGateCount; ilGateno++)
//	{
//		ALLOCDATA *prlAlloc = &prpData->AllocationUnits[ilGateno];
		CCSPtrArray<FLIGHTDATA> olFlights;
		ogFlightData.GetFlightsByGate(olFlights, prpGate->Name);

		int ilFlightCount = olFlights.GetSize();
		for (int ilFlightno = 0; ilFlightno < ilFlightCount; ilFlightno++)
		{
			FLIGHTDATA *prlFlight = &olFlights[ilFlightno];
			if (IsPassFilter(ogFlightData.GetGate(prlFlight), prlFlight))
			{
				GATEDATA_LINE olLine;
				SetFlightData(olLine,prlFlight);
				int ilLineno = CreateLine(&olLine);
			}
		}
//	}
}

void GateTableViewer::SetFlightData(GATEDATA_LINE &ropLine, FLIGHTDATA *prpFlight)
{
	ropLine.FlightUrno = prpFlight->Urno;
	ropLine.Alid = ogFlightData.GetGate(prpFlight);
	ropLine.Fnum = prpFlight->Fnum;
	ropLine.FlightIofl = prpFlight->Adid[0];
	ropLine.FlightTime = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Stoa : prpFlight->Stod);
	if (ogFlightData.IsArrival(prpFlight))
	{
		if(prpFlight->Onbl != TIMENULL)
		{
			ropLine.FlightActualTime = prpFlight->Onbl;
			strcpy(ropLine.TimeStatus,"O");
		}
		else
		{
			if(prpFlight->Tmoa != TIMENULL)
			{
				ropLine.FlightActualTime = prpFlight->Tmoa;
				strcpy(ropLine.TimeStatus,"T");
			}
			else
			{
				if(prpFlight->Etoa != TIMENULL)
				{
					ropLine.FlightActualTime = prpFlight->Etoa;
					strcpy(ropLine.TimeStatus,"E");
				}
				else
				{
					ropLine.FlightActualTime = TIMENULL;
					strcpy(ropLine.TimeStatus," ");
				}
			}

		}

	}
	else
	{
		if(prpFlight->Airb != TIMENULL)
		{
			ropLine.FlightActualTime = prpFlight->Airb;
			strcpy(ropLine.TimeStatus,"A");
		}
		else
		{
			if(prpFlight->Ofbl != TIMENULL)
			{
				ropLine.FlightActualTime = prpFlight->Ofbl;
				strcpy(ropLine.TimeStatus,"O");
			}
			else
			{
				if(prpFlight->Etod != TIMENULL)
				{
					ropLine.FlightActualTime = prpFlight->Etod;
					strcpy(ropLine.TimeStatus,"E");
				}
				else
				{
					ropLine.FlightActualTime = TIMENULL;
					strcpy(ropLine.TimeStatus," ");
				}
			}

		}

	}
}

void GateTableViewer::MakeIndicators()
{
	int ilLineCount = omLines.GetSize();
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(omLines[ilLineno].FlightUrno);
		if (prlFlight == NULL)	// no such flight?
			continue;

		// we create a list of demands for this flight first
		CCSPtrArray<DEMANDDATA> olDemands;
		ogDemandData.GetDemandsByFlur(olDemands, prlFlight->Urno, ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE,PERSONNELDEMANDS);
		int ilDemandCount = olDemands.GetSize();
		for (int ilDemandno = 0; ilDemandno < ilDemandCount; ilDemandno++)
		{
			GATEDATA_INDICATOR rlIndicator;
			rlIndicator.JobUrno = 0L;
			rlIndicator.DemandUrno = olDemands[ilDemandno].Urno;
			rlIndicator.Name = "";
			rlIndicator.Acfr = olDemands[ilDemandno].Debe;
			rlIndicator.Acto = olDemands[ilDemandno].Deen;
			CreateIndicator(ilLineno, &rlIndicator);
		}

		// Then we trying to match every jobs with the demands, and take the
		// remaining part as jobs without demands.
		// (We have to use the stepping backward method.)
		CCSPtrArray<JOBDATA> olJobs;
		ogJobData.GetJobsByFlur(olJobs, prlFlight->Urno,FALSE,ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE); // don't like JOBFM
		for (int ilJobno = 0; ilJobno < olJobs.GetSize(); ilJobno++)
		{
			// prepare information for this guy
			JOBDATA *prlJob = &olJobs[ilJobno];
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			GATEDATA_INDICATOR rlIndicator;
			rlIndicator.JobUrno = prlJob->Urno;
			rlIndicator.DemandUrno = 0L;
			rlIndicator.Name = ogEmpData.GetEmpName(prlEmp);
			rlIndicator.Acfr = prlJob->Acfr;
			rlIndicator.Acto = prlJob->Acto;

			// trying to match this guy to a demand, if find we will replace demand with job
			int ilIndicatorno;
			if ((ilIndicatorno = SearchDemand(prlJob, ilLineno)) != -1)	// demand found?
			{
				rlIndicator.DemandUrno = omLines[ilLineno].Indicators[ilIndicatorno].DemandUrno;
				DeleteIndicator(ilLineno, ilIndicatorno);	// remove the demand
			}
			CreateIndicator(ilLineno, &rlIndicator);	// and insert the job
		}
	}
}

BOOL GateTableViewer::FindFlight(long lpFlightUrno, int &ripLineno)
{
	int ilLineCount = omLines.GetSize();
	for (ripLineno = 0; ripLineno < ilLineCount; ripLineno++)
		if (lpFlightUrno == omLines[ripLineno].FlightUrno)
			return TRUE;
	return FALSE;
}

BOOL GateTableViewer::FindAssignment(long lpJobUrno, int &ripLineno, int &ripIndicatorno)
{
	int ilLineCount = omLines.GetSize();
	for (ripLineno = 0; ripLineno < ilLineCount; ripLineno++)
	{
	    int ilIndicatorCount = omLines[ripLineno].Indicators.GetSize();
		for (ripIndicatorno = 0; ripIndicatorno < ilIndicatorCount; ripIndicatorno++)
			if (lpJobUrno <= omLines[ripLineno].Indicators[ripIndicatorno].JobUrno)
				return TRUE;
	}
	return FALSE;
}

int GateTableViewer::SearchDemand(JOBDATA *prpJob, int ipLineno)
{
	DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
	if(prlDemand == NULL)
		return -1;

	// trying to match one of JODDATA to demand in the indicator
	GATEDATA_LINE *prlLine = &omLines[ipLineno];
	for (int ilIndicatorno = 0; ilIndicatorno < prlLine->Indicators.GetSize(); ilIndicatorno++)
	{
		if (prlDemand->Urno == prlLine->Indicators[ilIndicatorno].DemandUrno)
			return ilIndicatorno;
	}
    return -1;	// not found
}

/////////////////////////////////////////////////////////////////////////////
// GateTableViewer - GATEDATA_LINEDATA array maintenance

void GateTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int GateTableViewer::CreateLine(GATEDATA_LINE *prpGate)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareGate(prpGate, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareGate(prpGate, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpGate);
    return ilLineno;
}

void GateTableViewer::DeleteLine(int ipLineno)
{
	omLines[ipLineno].Indicators.DeleteAll();
    omLines.DeleteAt(ipLineno);
}

int GateTableViewer::CreateIndicator(int ipLineno, GATEDATA_INDICATOR *prpIndicator)
{
    int ilIndicatorCount = omLines[ipLineno].Indicators.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
/*    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->Acfr <= omLines[ipLineno].Indicators[ilIndicatorno].Acfr)
            break;  // should be inserted before Lines[ilIndicatorno]
*/
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->Acfr >= omLines[ipLineno].Indicators[ilIndicatorno-1].Acfr)
            break;  // should be inserted after Lines[ilIndicatorno-1]


    omLines[ipLineno].Indicators.NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void GateTableViewer::DeleteIndicator(int ipLineno, int ipIndicatorno)
{
	omLines[ipLineno].Indicators.DeleteAt(ipIndicatorno);
}

/////////////////////////////////////////////////////////////////////////////
// GateTableViewer - display drawing routine

void GateTableViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Gate|Flug||Sta/d|Act.|S|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis");
    pomTable->SetHeaderFields(GetString(IDS_STRING61569));
    pomTable->SetFormatList("5|10|1|5|5|1|20|9|20|9|20|9|20|9|20|9|20|9");

	pomTable->ResetContent();
	int nLineCount = 0;
	nLineCount = omLines.GetSize();
	for (int i = 0; i < nLineCount; i++) 
	{
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
        // Display grouping effect
        if (i == omLines.GetSize()-1 ||
			!IsSameGroup(&omLines[i], &omLines[i+1]))
            pomTable->SetTextLineSeparator(i, ST_THICK);
	}

	pomTable->DisplayTable();
}

CString GateTableViewer::Format(GATEDATA_LINE *prpLine)
{
    CString s;
	s =  prpLine->Alid + "|" + prpLine->Fnum + "|"
		+ prpLine->FlightIofl + "|" + prpLine->FlightTime.Format("%H%M") + "|" 
		+ prpLine->FlightActualTime.Format("%H%M")  + "|" + prpLine->TimeStatus;

	int ilIndicatorCount = prpLine->Indicators.GetSize();
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
    {
		GATEDATA_INDICATOR *prlIndicator = &prpLine->Indicators[ilIndicatorno];
		s += "|" + prlIndicator->Name + "|";
		s += prlIndicator->Acfr.Format("%H%M") + "-";
		s += prlIndicator->Acto.Format("%H%M");
    }
    return s;
}

void GateTableViewer::GateTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    GateTableViewer *polViewer = (GateTableViewer *)popInstance;

	if(ipDDXType == JOB_NEW || ipDDXType == JOB_CHANGE || ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessJobChanges(ipDDXType,(JOBDATA *)vpDataPointer);
	}
	else if(ipDDXType == FLIGHT_CHANGE || ipDDXType == FLIGHT_SELECT)
	{
		polViewer->ProcessFlightChanges(ipDDXType,(FLIGHTDATA *)vpDataPointer);
	}
	
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{	
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(GateTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}


void GateTableViewer::ProcessJobChanges(int ipDDXType,JOBDATA *prpJob)
{
    if (ipDDXType == JOB_NEW)
	{
        ProcessJobDelete((JOBDATA *)prpJob);
        ProcessJobNew((JOBDATA *)prpJob);
		
		int ilItem;

		if (FindFlight(ogBasicData.GetFlightUrnoForJob(prpJob),ilItem))
		{
			pomTable->ChangeTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}
	else if (ipDDXType == JOB_CHANGE)
	{
        ProcessJobDelete((JOBDATA *)prpJob);
        int ilItem = ProcessJobNew((JOBDATA *)prpJob);
		if ((ilItem != -1) && (ilItem < omLines.GetSize()))
		{
			pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}
	else if (ipDDXType == JOB_DELETE)
	{
	    int ilItem = ProcessJobDelete((JOBDATA *)prpJob);
		if ((ilItem != -1) && (ilItem < omLines.GetSize()))
		{
			pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}
}

void GateTableViewer::ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight)
{
	if (ipDDXType == FLIGHT_CHANGE)
	{
		int ilItem;
		if (FindFlight(prpFlight->Urno,ilItem))
		{
			SetFlightData(omLines[ilItem],prpFlight);
			pomTable->ChangeTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}
	else if (ipDDXType == FLIGHT_SELECT)
	{
		int ilItem;
		if (FindFlight(prpFlight->Urno,ilItem))
		{
			CListBox *polListBox = pomTable->GetCTableListBox();
			if (polListBox)
				polListBox->SetSel(ilItem);
		}
	}

}

/*
void GateTableViewer::ProcessJobChange(JOBDATA *prpJob)
{
        ProcessJobDelete((JOBDATA *)prpJob);
        int ilItem = ProcessJobNew((JOBDATA *)prpJob);
		if (ilItem != -1)
		{
			pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
}
*/

int GateTableViewer::ProcessJobNew(JOBDATA *prpJob)
{
	if (strcmp(prpJob->Jtco, JOBFLIGHT) != 0)
		return -1;

	// For the bar which should be displayed on the screen
	int ilLineno = -1;
	if (FindFlight(ogBasicData.GetFlightUrnoForJob(prpJob), ilLineno))
	{
		// prepare information for this guy
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		GATEDATA_INDICATOR rlIndicator;
		rlIndicator.JobUrno = prpJob->Urno;
		rlIndicator.DemandUrno = 0L;
		rlIndicator.Name = ogEmpData.GetEmpName(prlEmp);
		rlIndicator.Acfr = prpJob->Acfr;
		rlIndicator.Acto = prpJob->Acto;

		// trying to match this guy to a demand, if find we will replace demand with job
		int ilIndicatorno;
		if ((ilIndicatorno = SearchDemand(prpJob, ilLineno)) != -1)	// demand found?
		{
			rlIndicator.DemandUrno = omLines[ilLineno].Indicators[ilIndicatorno].DemandUrno;
			DeleteIndicator(ilLineno, ilIndicatorno);	// remove the demand
		}
		CreateIndicator(ilLineno, &rlIndicator);	// and insert the job
	}
	return ilLineno;
}

int GateTableViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	if (strcmp(prpJob->Jtco, JOBFLIGHT) != 0)
		return -1;

	// For the bar which should be displayed on the screen
	int ilLineno = -1, ilIndicatorno;
	if (FindAssignment(prpJob->Urno, ilLineno, ilIndicatorno))
	{
		GATEDATA_INDICATOR *prlIndicator = &omLines[ilLineno].Indicators[ilIndicatorno];
		if (prlIndicator->DemandUrno == 0L)	// no demand associated?
		{
			DeleteIndicator(ilLineno, ilIndicatorno);
		}
		else
		{
			// we have to delete this job and display the demand instead
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlIndicator->DemandUrno);
			GATEDATA_INDICATOR rlIndicator;
			rlIndicator.JobUrno = 0L;
			rlIndicator.DemandUrno = prlDemand->Urno;
			rlIndicator.Name = "";
			rlIndicator.Acfr = prlDemand->Debe;
			rlIndicator.Acto = prlDemand->Deen;

			DeleteIndicator(ilLineno, ilIndicatorno);	// delete the job's indicator
			CreateIndicator(ilLineno, &rlIndicator);	// and insert one for demand back
		}
	}
	return ilLineno;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL GateTableViewer::PrintGateLine(GATEDATA_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 120;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61637); //Gate
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61609); //"Flug"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 325;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING32823); //"Name"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); //"Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); //"Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 325;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING32823); //"Name"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); //"Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); //"Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 325;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING32823); //"Name"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); //"Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); //"Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 325;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING32823); //"Name"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); //"Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); //"Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 325;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING32823); //"Name"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); //"Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); //"Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		rlElement.Alignment  = PRINT_CENTER;
		rlElement.Length     = 120;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= ilBottomFrame;
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;
		rlElement.Text       = prpLine->Alid;
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.Text       = prpLine->Fnum;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		rlElement.FrameTop   = PRINT_NOFRAME;

		//Now the jobs in line; at the moment only 5 Jobs for more Jobs 
		// there is currently no solution

		int ilCount = prpLine->Indicators.GetSize();
		int ilJobNo = 0;
		int i = 0; 
		do
		{
			GATEDATA_INDICATOR rlIndicator;

			if (ilJobNo > 4)
			{
				ilJobNo = 0;
				pomPrint->PrintLine(rlPrintLine);
				rlPrintLine.DeleteAll();

				rlElement.Alignment  = PRINT_CENTER;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.FrameRight = PRINT_NOFRAME;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.FrameBottom= PRINT_NOFRAME;
				rlElement.pFont       = &pomPrint->omSmallFont_Bold;
				rlElement.Length     = 250;
				rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
				rlElement.FrameBottom= ilBottomFrame;

			}

			if(i < ilCount)
			{
				rlIndicator = prpLine->Indicators[i];
			}
			else
			{
				rlIndicator.Name.Empty();
				rlIndicator.Acfr = TIMENULL;
				rlIndicator.Acto = TIMENULL;
			}
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.Length     = 325;
			rlElement.Text       = rlIndicator.Name;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.Length     = 80;
			rlElement.Text       = rlIndicator.Acfr.Format("%H:%M");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.Length     = 80;
			rlElement.Text       = rlIndicator.Acto.Format("%H:%M");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			ilJobNo++;
			i++;
		} while ((i < ilCount) || (ilJobNo % 5 != 0));

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	}

	return TRUE;
}

BOOL GateTableViewer::PrintGateHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void GateTableViewer::PrintView()
{  
//	CTime olStartTime = ogBasicData.GetTime();
///	CString omTarget = CString("vom ") + CString(olStartTime.Format("%d.%m.%Y %H:%M"));// + "  bis  " +	CString(olEndTime.Format("%d.%m.%Y %H:%M"));		

	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) +
	CString(omEndTime.Format("%d.%m.%Y %H:%M"))
			+  GetString(IDS_STRING33143) + ogCfgData.rmUserSetup.GBLV;		

	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,100,
		GetString(IDS_STRING33144),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING33144));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintGateLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintGateHeader(pomPrint);
				}
				PrintGateLine(&omLines[i],FALSE);
			}
			PrintGateLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

	/*
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->FlightSchedulePrint(&ogShiftData, (char *)(const char *)omDate);
	delete prn;
	*/
}

CTime GateTableViewer::StartTime() const
{
	return omStartTime;
}

CTime GateTableViewer::EndTime() const
{
	return omEndTime;
}

int GateTableViewer::Lines() const
{
	return omLines.GetSize();
}

GATEDATA_LINE*	GateTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void GateTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void GateTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

