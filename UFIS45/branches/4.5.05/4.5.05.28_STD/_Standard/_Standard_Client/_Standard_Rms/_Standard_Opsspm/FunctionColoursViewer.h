#ifndef __FUNCTIONCOLOURSVIEWER_H__
#define __FUNCTIONCOLOURSVIEWER_H__

#include <CCSPtrArray.h>
#include <CViewer.h>
#include <CCSTable.h>
#include <CedaPfcData.h>

struct FUNCTIONCOLOURS_LINE
{
	CString		Fctc;
	CString		Fctn;
	COLORREF	Colour;
	bool		Enabled;

	COLORREF	PrevColour;
	bool		PrevEnabled;

	bool		TeamLeaderLine;

	FUNCTIONCOLOURS_LINE(void)
	{
		Colour = SILVER;
		Enabled = false;
		PrevColour = SILVER;
		PrevEnabled = false;
		TeamLeaderLine = false;
	}
};


struct FUNCTIONCOLOURS_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	FUNCTIONCOLOURS_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	FUNCTIONCOLOURS_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};


class	CCSTable;
struct	TABLE_COLUMN;

class FunctionColoursViewer: public CViewer
{
public:
	FunctionColoursViewer();
    ~FunctionColoursViewer();

	void Attach(CCSTable *popTable);
	void ChangeViewTo();
	void DeleteAll();
	int  CompareFunctionCode(FUNCTIONCOLOURS_LINE *prpLine1, FUNCTIONCOLOURS_LINE *prpLine2);
	BOOL IsPassFilter(PFCDATA *prpPfc);
	void MakeLines();
	void MakeLine(PFCDATA *prpPfc);
	void MakeLineForTeamLeader(void);
	void MakeLineData(PFCDATA *prpPfc, FUNCTIONCOLOURS_LINE &ropLine);
	int  CreateLine(FUNCTIONCOLOURS_LINE *prpLine);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void UpdateTableLine(int ipLine);
	void Format(FUNCTIONCOLOURS_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool EvaluateTableFields(void);
	int  TableFieldIndex(const CString& ropField);
	int  LineCount() const;
	FUNCTIONCOLOURS_LINE *GetLine(int ipLineNo);
	bool bmDisplayDefinedOnly;
	void SaveLineData(void);

private:
    CCSTable			*pomTable;
  
	CCSPtrArray <FUNCTIONCOLOURS_LINE> omLines;
	CCSPtrArray <FUNCTIONCOLOURS_FIELD> omTableFields;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeader;
	CCSPtrArray <TABLE_COLUMN> omColumns;
	void CreateTableColumnsAndHeader();
};

#endif //__BREAKTABLEANDTABLEVIEWER_H__