// MReqTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <resource.h>
#include <table.h>
#include <cviewer.h>

#include <CedaRequestData.h>
#include <DMReqTable.h>
#include <MReqTblV.h>
#include <MReqTable.h>
#include <BasePropertySheet.h>
#include <MReqTaSO.h>
#include <MReqTaPS.h>

#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void MReqTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// MReqTable dialog

MReqTable::MReqTable(CString opTableType,CWnd* pParent /*=NULL*/)
	: CDialog(MReqTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(MReqTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = new CTable;
    pomTable->tempFlag = 2;

    CDialog::Create(MReqTable::IDD);  
    CRect olRect;
	if (opTableType == "R")
	{
		//CDialog::SetWindowText("Mitarbeiter Request");
		CDialog::SetWindowText(GetString(IDS_STRING61347));
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.RQTB,ogCfgData.rmUserSetup.MONS);
	}
	else
	{
		//CDialog::SetWindowText("Mitarbeiter Info");
		CDialog::SetWindowText(GetString(IDS_STRING61348));
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.INFO,ogCfgData.rmUserSetup.MONS);
	}
    MoveWindow(&olRect);
	
	omTableType = opTableType;
	omContextItem = -1;
	bmIsViewOpen = FALSE;

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
   	omViewer.Attach(pomTable,omTableType);
	UpdateView();
}

MReqTable::~MReqTable()
{
	TRACE("MReqTable::~MreqTable()\n");
	delete pomTable;
}

void MReqTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MReqTable)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

void MReqTable::UpdateView()
{
	CString  olViewName = omViewer.GetViewName();
	
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.RQSV;
	}

	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomTable->GetCTableListBox()->SetFocus();
}

void MReqTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	if (polCB != NULL)
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		omViewer.GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = omViewer.GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{
			olViewName = ogCfgData.rmUserSetup.RQSV;
		}

		ilIndex = polCB->FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}

	}
}

BEGIN_MESSAGE_MAP(MReqTable, CDialog)
	//{{AFX_MSG_MAP(MReqTable)
	ON_WM_SIZE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblClk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_BN_CLICKED(IDNEU, OnInsert)
	ON_BN_CLICKED(IDWORKON, OnWorkon)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_COMMAND(11, OnMenuWorkon)
	ON_COMMAND(12, OnMenuDelete)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MReqTable message handlers

BOOL MReqTable::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING61347));
	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDNEU); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33002));
	}
	polWnd = GetDlgItem(IDWORKON); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61352));
	}

	UpdateComboBox();
	
	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    MoveWindow(&rect);

	// Register DDX call back function
	TRACE("MReqTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("MREQTABLE"),
		CString("Redisplay all from What-If"), MReqTableCf);	// for what-if changes

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MReqTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("MReqTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void MReqTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();

}

void MReqTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
        pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

void MReqTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomTable->GetCTableListBox()->SetFocus();

	RequestTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		UpdateView();
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void MReqTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    pomTable->GetCTableListBox()->SetFocus();
}

void MReqTable::OnInsert() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomTable->GetCTableListBox()->SetFocus();

	REQUESTDATA rlRequest;

	DMreqTable dlg(this,&rlRequest,DLG_INSERT);
    if (dlg.DoModal() == IDOK)
	{
		strcpy(rlRequest.Stat,omTableType);
		rlRequest.Urno = ogBasicData.GetNextUrno();
		if (omTableType == "R")
			ogRequests.AddRequest(&rlRequest);
		else
			ogInfos.AddRequest(&rlRequest);
	}
}

void MReqTable::OnWorkon() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomTable->GetCTableListBox()->SetFocus();

	REQUEST_LINEDATA *prlLine = (REQUEST_LINEDATA *)pomTable->GetTextLineData(pomTable->GetCurrentLine());
	if (prlLine != NULL)
	{	
		REQUESTDATA *prlRequest;
		if (omTableType == "R")
		{
			prlRequest = ogRequests.GetRequestByUrno(prlLine->Urno); 
		}
		else
		{
			prlRequest = ogInfos.GetRequestByUrno(prlLine->Urno); 
		}

		REQUESTDATA rlRequest = *prlRequest; 
		DMreqTable dlg(this,&rlRequest,DLG_UPDATE);
		if (dlg.DoModal() == IDOK)
		{
			memcpy(prlRequest,&rlRequest,sizeof(REQUESTDATA));
			prlRequest->IsChanged = DATA_CHANGED;
			if (omTableType == "R")
				ogRequests.SaveRequest(prlRequest);
			else
				ogInfos.SaveRequest(prlRequest);

			ogCCSDdx.DataChanged((void *)this,REQUEST_CHANGE,(void *)prlRequest);
		}
	}
}

BOOL MReqTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	if (omTableType == "I")
		AfxGetMainWnd()->SendMessage(WM_INFOTABLE_EXIT);
	else
	if (omTableType == "R")
		AfxGetMainWnd()->SendMessage(WM_REQUESTTABLE_EXIT);

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

LONG MReqTable::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
	OnWorkon();
	return 0L;
}

LONG MReqTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));

	REQUEST_LINEDATA *prlLine = (REQUEST_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		int ilLc;

		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);
		//menu.AppendMenu(MF_STRING,11,"Bearbeiten");	
		menu.AppendMenu(MF_STRING,11,GetString(IDS_STRING61352));	
		//menu.AppendMenu(MF_STRING,12,"L�schen");	
		menu.AppendMenu(MF_STRING,12,GetString(IDS_STRING61210));	
		ClientToScreen( &olPoint);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
		omContextItem = ipItem;
	}

	return 0L;
}

void MReqTable::OnMenuWorkon()
{
	if (omContextItem != -1)
	{
		REQUEST_LINEDATA *prlLine = (REQUEST_LINEDATA *)pomTable->GetTextLineData(omContextItem);
		if (prlLine != NULL)
		{	
			REQUESTDATA *prlRequest;
			if (omTableType == "R")
			{
				prlRequest = ogRequests.GetRequestByUrno(prlLine->Urno); 
			}
			else
			{
				prlRequest = ogInfos.GetRequestByUrno(prlLine->Urno); 
			}

			REQUESTDATA rlRequest = *prlRequest; 
			DMreqTable dlg(this,&rlRequest,DLG_UPDATE);
			if (dlg.DoModal() == IDOK)
			{
				memcpy(prlRequest,&rlRequest,sizeof(REQUESTDATA));
				prlRequest->IsChanged = DATA_CHANGED;
				if (omTableType == "R")
					ogRequests.SaveRequest(prlRequest);
				else
					ogInfos.SaveRequest(prlRequest);

				ogCCSDdx.DataChanged((void *)this,REQUEST_CHANGE,(void *)prlRequest);
			}
		}
	}
	omContextItem = -1;
}

void MReqTable::OnMenuDelete()
{
	if (omContextItem != -1)
	{
		REQUEST_LINEDATA *prlLine = (REQUEST_LINEDATA *)pomTable->GetTextLineData(omContextItem);
		if (prlLine != NULL)
		{
			if (omTableType == "R")
			{
				ogRequests.DeleteRequest(prlLine->Urno); 
			}
			else
			{
				ogInfos.DeleteRequest(prlLine->Urno); 
			}
		}
	}
	omContextItem = -1;
}

////////////////////////////////////////////////////////////////////////
// MReqTable -- implementation of DDX call back function

static void MReqTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	MReqTable *polTable = (MReqTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
		polTable->UpdateView();
}
