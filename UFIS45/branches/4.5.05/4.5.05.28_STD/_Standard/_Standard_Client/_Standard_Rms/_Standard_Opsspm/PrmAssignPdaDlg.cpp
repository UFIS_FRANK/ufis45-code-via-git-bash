// PrmAssignPdaDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <PrmAssignPdaDlg.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <CedaTplData.h>
#include <AllocData.h>
#include <CedaSerData.h>
#include <CedaDpxData.h>
#include <CedaWroData.h>
#include <CedaEmpData.h>
#include <CedaPdaData.h>
#include <DataSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrmAssignPdaDlg dialog


PrmAssignPdaDlg::PrmAssignPdaDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PrmAssignPdaDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PrmAssignPdaDlg)
	m_FromDate = NULL;
	m_FromTime = NULL;
	m_ToDate = NULL;
	m_ToTime = NULL;

	//}}AFX_DATA_INIT
}

void PrmAssignPdaDlg::InitDefaultValues(long lpStfUrno)
{
	lmUstf = lpStfUrno;
}

void PrmAssignPdaDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == FALSE)
	{
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PrmAssignPdaDlg)
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate == TRUE)
	{
	}
}


BEGIN_MESSAGE_MAP(PrmAssignPdaDlg, CDialog)
	//{{AFX_MSG_MAP(PrmAssignPdaDlg)
//	DDX_Check(pDX, IDC_ALLPDA, bmShowAllCheckBox);
	ON_BN_CLICKED(IDC_ALLPDA, OnShowAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PrmAssignPdaDlg message handlers

BOOL PrmAssignPdaDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = NULL;
	m_ShowAllPdas = false;
	

	EMPDATA *prlStaff = ogEmpData.GetEmpByUrno(lmUstf);
	if (prlStaff != NULL)
	{
		char clTemp[256];
		sprintf(clTemp,"Assign PDA to: %s, %s",prlStaff->Finm,prlStaff->Lanm);
		SetWindowText(clTemp);
	}

	polWnd = GetDlgItem(IDC_PDAIDTEXT);
	if(polWnd != NULL)
	{
		//polWnd->SetWindowText(GetString(IDC_PDAIDTEXT));
		polWnd->SetWindowText("Show All PDA");
	}

	CComboBox *polPdaIds = (CComboBox *) GetDlgItem(IDC_PDAID);
	if(polPdaIds != NULL)
	{
		polPdaIds->ResetContent();

		int ilPdaCount = ogPdaData.omData.GetSize();
		PDADATA *blpPdaData;
		int ilIndex;
		int ilSelected = -1;

		for (int ilLc = 0; ilLc < ilPdaCount; ilLc++) 
		{
			blpPdaData = &ogPdaData.omData[ilLc];
			if (m_ShowAllPdas || blpPdaData->Ustf == 0L || lmUstf == blpPdaData->Ustf)
			{
				ilIndex = polPdaIds->AddString(blpPdaData->Pdid);
				polPdaIds->SetItemData(ilIndex,blpPdaData->Urno);
				if (lmUstf == blpPdaData->Ustf)
				{
					polPdaIds->SetCurSel(ilIndex);
					ilSelected = ilIndex;
				}
			}
		}
		if (ilSelected == -1 && polPdaIds->GetCount() > 0)
		{
			polPdaIds->SetCurSel(0);

		}

	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void PrmAssignPdaDlg::OnShowAll()
{
	CButton *polShowAll = (CButton *) GetDlgItem(IDC_ALLPDA);
	if (polShowAll->GetCheck() )
	{
		m_ShowAllPdas = TRUE;
	}
	else
	{
		m_ShowAllPdas = FALSE;
	}
	CComboBox *polPdaIds = (CComboBox *) GetDlgItem(IDC_PDAID);
	if(polPdaIds != NULL)
	{
		polPdaIds->ResetContent();

		int ilPdaCount = ogPdaData.omData.GetSize();
		PDADATA *blpPdaData;
		int ilIndex;
		int ilSelected = -1;

		for (int ilLc = 0; ilLc < ilPdaCount; ilLc++) 
		{
			blpPdaData = &ogPdaData.omData[ilLc];
			if (m_ShowAllPdas || blpPdaData->Ustf == 0L || blpPdaData->Ustf == lmUstf)
			{
				ilIndex = polPdaIds->AddString(blpPdaData->Pdid);
				polPdaIds->SetItemData(ilIndex,blpPdaData->Urno);
				if (lmUstf == blpPdaData->Ustf)
				{
					polPdaIds->SetCurSel(ilIndex);
					ilSelected = ilIndex;
				}
			}
		}
		if (ilSelected == -1 && polPdaIds->GetCount() > 0)
		{
			polPdaIds->SetCurSel(0);

		}
	}

}


void PrmAssignPdaDlg::OnOK() 
{
	CComboBox *polPdaIds = (CComboBox *) GetDlgItem(IDC_PDAID);
	if(polPdaIds != NULL)
	{
		int ilIndex = polPdaIds->GetCurSel();
		if(ilIndex != CB_ERR)
		{
			PDADATA *prlPda = ogPdaData.GetPdaByUrno((long) polPdaIds->GetItemData(ilIndex));
			if (prlPda->Ustf != 0L)
			{
				ogPdaData.RemoveFromStaff(prlPda->Ustf);
			}
			if (prlPda != 0L)
			{
				ogPdaData.RemoveFromAllPda(lmUstf);
				prlPda->Ustf = lmUstf;
				ogPdaData.Update(prlPda);
			}
		}
	}
	CDialog::OnOK();
}
