// ccitblvw.cpp -- the viewer for Gate Table
//
// Written by:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	The old version of this viewer is just enough for primitive displaying.
//	Now we like to have some more drag-and-drop functionality for the Gate Table.
//	So we have to rewrite this viewer to display each flight job in the proper column
//	(each two-columns on the right-side of the table represents one demand or one job,
//	in the other word, it is the same as an indicator in the Gate Diagram.)
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaDemandData.h>
#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>
#include <FlightJobsTableViewer.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <CedaAltData.h>

// stream defs
#include <fstream.h>
#include <iomanip.h>

class FlightJobsTable; //Singapore

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_FLIGHT,
	BY_ADID,
	BY_TIME1,
	BY_TIME2,
	BY_ACT3,
	BY_NONE
};

static int ByWorkgroupAndShiftCode(const EXPORT_LINE **pppWgSc1, const EXPORT_LINE **pppWgSc2);
static int ByWorkgroupAndShiftCode(const EXPORT_LINE **pppWgSc1, const EXPORT_LINE **pppWgSc2)
{
	int ilRc = 0;
	if((*pppWgSc1)->WorkGroup.IsEmpty() && !(*pppWgSc2)->WorkGroup.IsEmpty())
	{
		ilRc = 1;
	}
	else if(!(*pppWgSc1)->WorkGroup.IsEmpty() && (*pppWgSc2)->WorkGroup.IsEmpty())
	{
		ilRc = -1;
	}
	else
	{
		if((ilRc = strcmp((*pppWgSc1)->WorkGroup, (*pppWgSc2)->WorkGroup)) == 0)
		{
			if((ilRc = strcmp((*pppWgSc1)->ShiftCode, (*pppWgSc2)->ShiftCode)) == 0)
				ilRc = (*pppWgSc2)->Jobs[0].Name.GetLength() - (*pppWgSc1)->Jobs[0].Name.GetLength();
		}
	}
	return ilRc;
}

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (*pppJob1)->Acfr.GetTime() - (*pppJob2)->Acto.GetTime();
}

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableViewer

FlightJobsTableViewer::FlightJobsTableViewer()
{
    SetViewerKey("FlightJobsTable");
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW, CString("FlightJobsTableViewer"), CString("Job New"), FlightJobsTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("FlightJobsTableViewer"), CString("Job Change"), FlightJobsTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("FlightJobsTableViewer"), CString("Job Delete"), FlightJobsTableCf);
    ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("FlightJobsTableViewer"), CString("Flight Change"), FlightJobsTableCf);
    ogCCSDdx.Register(this, FLIGHT_SELECT_FLIGHTJOBSTABLE, CString("FlightJobsTableViewer"), CString("Flight Select"), FlightJobsTableCf);

	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	imColumns = 4;
	bmUseAllAlcs = false;
//	bmUseAllDetys = false;

	// column widths for printing
	imFlightNameLen = 150;
	imAdidNameLen = 40;
	imRegnNameLen = 200;
	imActNameLen = 90;
	imStaStdNameLen = 90;
	imTimeNameLen = 85;
	imTimeTypeNameLen = 40;
	imEmpNameLen = 300;
	imJobTimeNameLen = 85;

	bmDisplayArr = TRUE;
	bmDisplayDep = TRUE;
}

FlightJobsTableViewer::~FlightJobsTableViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);

	DeleteAll();
}

void FlightJobsTableViewer::Attach(CTable *popTable)
{
	pomTable = popTable;
}

void FlightJobsTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void FlightJobsTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.FJTV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter();

    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

void FlightJobsTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

	bmUseAllAlcs = false;
    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAlcs = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAlcd[olFilterValues[i]] = NULL;
	}

//	bmUseAllDetys = false;
//    GetFilter("Dety", olFilterValues);
//    omCMapForDety.RemoveAll();
//    n = olFilterValues.GetSize();
//	if (n > 0)
//	{
//		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
//			bmUseAllDetys = true;
//		else
//			for (i = 0; i < n; i++)
//				omCMapForDety[olFilterValues[i]] = NULL;
//	}


	bmDisplayArr = GetUserData("DISPLAYARR") == "NO" ? FALSE : TRUE;
	bmDisplayDep = GetUserData("DISPLAYDEP") == "NO" ? FALSE : TRUE;
}

void FlightJobsTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

	bmTime1Ascending = GetUserData("TIME1ORDER") == "YES" ? true : false;
	bmTime2Ascending = GetUserData("TIME2ORDER") == "YES" ? true : false;

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Flight")
            ilSortOrderEnumeratedValue = BY_FLIGHT;
        else if (olSortOrder[i] == "Adid")
            ilSortOrderEnumeratedValue = BY_ADID;
        else if (olSortOrder[i] == "Time1")
            ilSortOrderEnumeratedValue = BY_TIME1;
        else if (olSortOrder[i] == "Time2")
            ilSortOrderEnumeratedValue = BY_TIME2;
        else if (olSortOrder[i] == "Act3")
            ilSortOrderEnumeratedValue = BY_ACT3;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int FlightJobsTableViewer::CompareFlight(FLIGHTJOBSTABLE_LINE *prpFlight1, FLIGHTJOBSTABLE_LINE *prpFlight2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_FLIGHT:
            ilCompareResult = strcmp(prpFlight1->Fnum,prpFlight2->Fnum);
            break;
        case BY_ADID:
            ilCompareResult = strcmp(prpFlight1->Adid,prpFlight2->Adid);
            break;
        case BY_TIME1:
            ilCompareResult = (bmTime1Ascending) ?
				prpFlight1->FlightTime.GetTime() - prpFlight2->FlightTime.GetTime() :
				prpFlight2->FlightTime.GetTime() - prpFlight1->FlightTime.GetTime();
            break;
        case BY_TIME2:
            ilCompareResult = (bmTime2Ascending) ?
				prpFlight1->FlightActualTime.GetTime() - prpFlight2->FlightActualTime.GetTime() :
				prpFlight2->FlightActualTime.GetTime() - prpFlight1->FlightActualTime.GetTime();
            break;
        case BY_ACT3:
            ilCompareResult = strcmp(prpFlight1->Act3,prpFlight2->Act3);
            break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   
}



BOOL FlightJobsTableViewer::IsPassFilter(JOBDATA *prpJob)
{
	if(prpJob == NULL)
		return FALSE;

//    void *p;
//    BOOL blIsDetyOk = bmUseAllDetys ? true : prpJob->Dety[0] == '\0' ? omCMapForDety.Lookup(CString("7"),p) : omCMapForDety.Lookup(CString(prpJob->Dety), p);
BOOL blIsDetyOk = TRUE;
    return (blIsDetyOk);
}


BOOL FlightJobsTableViewer::IsPassFilter(FLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return FALSE;

	CString olAlc = ogAltData.FormatAlcString(prpFlight->Alc2,prpFlight->Alc3);

    void *p;
    BOOL blIsAlcdOk = bmUseAllAlcs || omCMapForAlcd.Lookup(CString(olAlc), p);
	BOOL blIsTimeOk = FALSE;
	BOOL blAdidOk = FALSE;
	if(ogFlightData.IsArrival(prpFlight))
	{
		blAdidOk = bmDisplayArr;
		blIsTimeOk = (omStartTime < prpFlight->Stoa && prpFlight->Stoa < omEndTime);
	}
	else
	{
		blAdidOk = bmDisplayDep;
		blIsTimeOk = (omStartTime < prpFlight->Stod && prpFlight->Stod < omEndTime);
	}

    return (blIsAlcdOk && blIsTimeOk && blAdidOk);
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void FlightJobsTableViewer::MakeLines()
{
	int ilNumFlights = ogFlightData.omData.GetSize();
	for(int ilF = 0; ilF < ilNumFlights; ilF++)
	{
		MakeLine(&ogFlightData.omData[ilF]);
	}
}

void FlightJobsTableViewer::MakeLine(FLIGHTDATA *prpFlight)
{
	CUIntArray olReturnFlightTypes;
	int ilNumReturnFlightTypes = GetReturnFlightTypes(prpFlight, olReturnFlightTypes);
	for(int ilF = 0; ilF < ilNumReturnFlightTypes; ilF++)
	{
		MakeLine(prpFlight, olReturnFlightTypes[ilF]);
	}
}

int FlightJobsTableViewer::GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes)
{
	ropReturnFlightTypes.RemoveAll();
	if(prpFlight != NULL)
	{
		if(ogFlightData.IsReturnFlight(prpFlight))
		{
			// add 2 bars for return flights once for inbound once for outbound
			ropReturnFlightTypes.Add(ID_ARR_RETURNFLIGHT);
			ropReturnFlightTypes.Add(ID_DEP_RETURNFLIGHT);
		}
		else
		{
			ropReturnFlightTypes.Add(ID_NOT_RETURNFLIGHT);
		}
	}

	return ropReturnFlightTypes.GetSize();
}

void FlightJobsTableViewer::MakeLine(FLIGHTDATA *prpFlight, int ipReturnFlightType)
{
	if(IsPassFilter(prpFlight))
	{
		FLIGHTJOBSTABLE_LINE olLine;
		SetFlightData(olLine, prpFlight, ipReturnFlightType);
		SetJobData(olLine);
		int ilLineno = CreateLine(&olLine);
	}
}

void FlightJobsTableViewer::SetFlightData(FLIGHTJOBSTABLE_LINE &ropLine, FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	ropLine.FlightUrno = prpFlight->Urno;
	ropLine.Fnum = prpFlight->Fnum;
	ropLine.Regn = prpFlight->Regn;
	ropLine.Act3 = prpFlight->Act3;
	ropLine.ReturnFlightType = ipReturnFlightType;

	if(ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT)
	{
		ropLine.Adid = "A";
		ropLine.FlightTime = prpFlight->Stoa;
		ropLine.FlightActualTime = prpFlight->Tifa;
		strcpy(ropLine.TimeStatus,prpFlight->Tisa);
	}
	else
	{
		ropLine.Adid = "D";
		ropLine.FlightTime = prpFlight->Stod;
		ropLine.FlightActualTime = prpFlight->Tifd;
		strcpy(ropLine.TimeStatus,prpFlight->Tisd);
	}
}

void FlightJobsTableViewer::SetJobData(FLIGHTJOBSTABLE_LINE &ropLine)
{
	ropLine.Jobs.RemoveAll();

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByFlur(olJobs, ropLine.FlightUrno, FALSE, "", "",PERSONNELDEMANDS, false, ropLine.ReturnFlightType);
	olJobs.Sort(ByJobStartTime);

	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
	{	
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	else
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}
	
	GetPrivateProfileString(pcgAppName, "FLREL_JOB_REMARK",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);

	int ilNumJobs = olJobs.GetSize();
	for (int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		JOBDATA *prlJob = &olJobs[ilJ];
		if(IsPassFilter(prlJob))
		{
			FLIGHTJOBSTABLE_JOB rlJob;
			rlJob.JobUrno = prlJob->Urno;

			if (!strcmp(pclTmpText,"YES"))
			{
				rlJob.Name = ogEmpData.GetEmpName(prlJob->Ustf) + " " + ogJobData.GetJobPoolByUrno(prlJob->Urno)->Text;
			}
			else
			{
				rlJob.Name = ogEmpData.GetEmpName(prlJob->Ustf);
			}
			rlJob.Acfr = prlJob->Acfr;
			rlJob.Acto = prlJob->Acto;
			CreateJob(ropLine, &rlJob);
		}
	}
}


BOOL FlightJobsTableViewer::FindFlight(long lpFlightUrno, int &ripLineno, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	if(ripLineno < 0)
		ripLineno = 0;
	else
		ripLineno++;

	int ilLineCount = omLines.GetSize();
	for(; ripLineno < ilLineCount; ripLineno++)
		if (lpFlightUrno == omLines[ripLineno].FlightUrno && omLines[ripLineno].ReturnFlightType == ipReturnFlightType)
			return TRUE;
	return FALSE;
}

BOOL FlightJobsTableViewer::FindJob(long lpJobUrno, int &ripLineno, int &ripJobno)
{
	if(ripLineno < 0)
		ripLineno = 0;
	else
		ripLineno++;

	int ilLineCount = omLines.GetSize();
	for(; ripLineno < ilLineCount; ripLineno++)
	{
	    int ilJobCount = omLines[ripLineno].Jobs.GetSize();
		for (ripJobno = 0; ripJobno < ilJobCount; ripJobno++)
			if (lpJobUrno == omLines[ripLineno].Jobs[ripJobno].JobUrno)
				return TRUE;
	}
	return FALSE;
}


void FlightJobsTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int FlightJobsTableViewer::CreateLine(FLIGHTJOBSTABLE_LINE *prpFlightLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareFlight(prpFlightLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(prpFlightLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpFlightLine);
    return ilLineno;
}

void FlightJobsTableViewer::DeleteLine(int ipLineno)
{
	omLines[ipLineno].Jobs.DeleteAll();
    omLines.DeleteAt(ipLineno);
}

void FlightJobsTableViewer::CreateJob(int ipLineno, FLIGHTJOBSTABLE_JOB *prpJob)
{
	if(ipLineno >= 0 && ipLineno < omLines.GetSize())
		CreateJob(omLines[ipLineno], prpJob);
}

void FlightJobsTableViewer::CreateJob(FLIGHTJOBSTABLE_LINE &ropLine, FLIGHTJOBSTABLE_JOB *prpJob)
{
	int ilJobCount = ropLine.Jobs.GetSize();
    for (int ilJobno = ilJobCount; ilJobno > 0; ilJobno--)
        if (prpJob->Acfr >= ropLine.Jobs[ilJobno-1].Acfr)
            break;

    ropLine.Jobs.NewAt(ilJobno, *prpJob);

	if(ropLine.Jobs.GetSize() > imColumns)
		imColumns = ropLine.Jobs.GetSize();
}

void FlightJobsTableViewer::DeleteJob(int ipLineno, int ipJobno)
{
	omLines[ipLineno].Jobs.DeleteAt(ipJobno);
}

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableViewer - display drawing routine

void FlightJobsTableViewer::UpdateDisplay()
{
	int i;

	// "Flight| |A/C|Sta/d|Act.| "
    CString olHeaderFields = GetString(IDS_FLIGHTJOBSTABLE_TITLE1);
    CString olFormatList("10|1|7|3|6|6|1");
	for(i = 0; i < imColumns; i++)
	{
		olFormatList += "|20|9";
		olHeaderFields += GetString(IDS_FLIGHTJOBSTABLE_TITLE2);
	}

    pomTable->SetFormatList(olFormatList);
    pomTable->SetHeaderFields(olHeaderFields);
	pomTable->ResetContent();
	int nLineCount = 0;
	nLineCount = omLines.GetSize();
	for(i = 0; i < nLineCount; i++) 
	{
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
	}

	pomTable->DisplayTable();
}

CString FlightJobsTableViewer::Format(FLIGHTJOBSTABLE_LINE *prpLine)
{
    CString olText;
	olText =  prpLine->Fnum + "|"
		+ prpLine->Adid + "|" + prpLine->Regn + "|" + prpLine->Act3 + "|" + prpLine->FlightTime.Format("%H%M") + "|" 
		+ prpLine->FlightActualTime.Format("%H%M")  + "|" + prpLine->TimeStatus;

	int ilJobCount = prpLine->Jobs.GetSize();
    for (int ilJobno = 0; ilJobno < ilJobCount; ilJobno++)
    {
		FLIGHTJOBSTABLE_JOB *prlJob = &prpLine->Jobs[ilJobno];
		olText += "|" + prlJob->Name + "|";
		olText += prlJob->Acfr.Format("%H%M") + "-";
		olText += prlJob->Acto.Format("%H%M");
    }
    return olText;
}

void FlightJobsTableViewer::FlightJobsTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    FlightJobsTableViewer *polViewer = (FlightJobsTableViewer *)popInstance;

	if(ipDDXType == JOB_NEW || ipDDXType == JOB_CHANGE || ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessJobChanges(ipDDXType,(JOBDATA *)vpDataPointer);
	}
	else if(ipDDXType == FLIGHT_CHANGE)
	{
		polViewer->ProcessFlightChanges(ipDDXType,(FLIGHTDATA *)vpDataPointer);
	}
	else if (ipDDXType == FLIGHT_SELECT_FLIGHTJOBSTABLE)
	{
		polViewer->ProcessFlightSelect((FLIGHTDATA *)vpDataPointer);
	}

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{		
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(FlightJobsTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void FlightJobsTableViewer::ProcessFlightSelect(FLIGHTDATA *prpFlight)
{
	int ilItem = -1;
	if(FindFlight(prpFlight->Urno,ilItem, prpFlight->ReturnFlightType))
	{
		CListBox *polListBox = pomTable->GetCTableListBox();
		if (polListBox)
			polListBox->SetCurSel(ilItem);
	}
}

void FlightJobsTableViewer::ProcessJobChanges(int ipDDXType,JOBDATA *prpJob)
{
    if(ipDDXType == JOB_NEW || ipDDXType == JOB_CHANGE)
	{
		if(IsPassFilter(prpJob))
		{
	        ProcessJobDelete(prpJob);
		    ProcessJobNew(prpJob, true);
		}
		else
		{
	        ProcessJobDelete(prpJob, true);
		}
	}
	else if (ipDDXType == JOB_DELETE)
	{
	    ProcessJobDelete(prpJob, true);
	}
}

void FlightJobsTableViewer::ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight)
{
	if (ipDDXType == FLIGHT_CHANGE)
	{
		int ilItem = -1;
		if (FindFlight(prpFlight->Urno,ilItem))
		{
			SetFlightData(omLines[ilItem],prpFlight);
			pomTable->ChangeTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}
	else if (ipDDXType == FLIGHT_SELECT)
	{
		int ilItem = -1;
		if (FindFlight(prpFlight->Urno,ilItem))
		{
			CListBox *polListBox = pomTable->GetCTableListBox();
			if (polListBox)
				polListBox->SetSel(ilItem);
		}
	}
}


void FlightJobsTableViewer::ProcessJobNew(JOBDATA *prpJob, bool bpUpdate /*false*/)
{
	int imOldColumns = imColumns;
	long llFlightUrno = ogBasicData.GetFlightUrnoForJob(prpJob);
	int ilLineno = -1;
	while(FindFlight(llFlightUrno, ilLineno))
	{
		FLIGHTJOBSTABLE_LINE *prlLine = &omLines[ilLineno];
		SetJobData(*prlLine);
		if(bpUpdate) pomTable->ChangeTextLine(ilLineno,Format(prlLine), prlLine);
	}
	long llRotationFlightUrno = GetRotationUrnoForJob(prpJob, llFlightUrno);
	if(llRotationFlightUrno != 0L)
	{
		ilLineno = -1;
		while(FindFlight(llRotationFlightUrno, ilLineno))
		{
			FLIGHTJOBSTABLE_LINE *prlLine = &omLines[ilLineno];
			SetJobData(*prlLine);
			if(bpUpdate) pomTable->ChangeTextLine(ilLineno,Format(prlLine), prlLine);
		}
	}
	if(bpUpdate)
	{
		if(imColumns > imOldColumns)
		{
			int ilTopIndex = pomTable->GetCTableListBox()->GetTopIndex();
			DeleteAll();    
			MakeLines();
			UpdateDisplay();
			pomTable->GetCTableListBox()->SetTopIndex(ilTopIndex);
		}
		pomTable->DisplayTable();
	}
}

long FlightJobsTableViewer::GetRotationUrnoForJob(JOBDATA *prpJob, long lpFlightUrno)
{
	long llRotationFlightUrno = 0L;
	if(prpJob != NULL && ogJobData.IsTurnaroundJob(prpJob))
	{
		DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
		if(prlDemand != NULL)
		{
			if(prlDemand->Ouri == lpFlightUrno)
				llRotationFlightUrno = prlDemand->Ouro;
			else if(prlDemand->Ouro == lpFlightUrno)
				llRotationFlightUrno = prlDemand->Ouri;
		}
	}

	return llRotationFlightUrno;
}

void FlightJobsTableViewer::ProcessJobDelete(JOBDATA *prpJob, bool bpUpdate /*false*/)
{
	int ilLineno = -1, ilJobno;
	while(FindJob(prpJob->Urno, ilLineno, ilJobno))
	{
		DeleteJob(ilLineno, ilJobno);
		if(bpUpdate && ilLineno >= 0 && ilLineno < omLines.GetSize())
		{
			pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
		}
	}
	if(bpUpdate)
	{
		pomTable->DisplayTable();
	}
}


CTime FlightJobsTableViewer::StartTime() const
{
	return omStartTime;
}

CTime FlightJobsTableViewer::EndTime() const
{
	return omEndTime;
}

int FlightJobsTableViewer::Lines() const
{
	return omLines.GetSize();
}

FLIGHTJOBSTABLE_LINE*	FlightJobsTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void FlightJobsTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void FlightJobsTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL FlightJobsTableViewer::PrintFlightJobsTableLine(FLIGHTJOBSTABLE_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 605 + ((imEmpNameLen+imJobTimeNameLen+imJobTimeNameLen) * imMaxJobsPerLine);
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = imFlightNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text = GetString(IDS_FJT_FLIGHT);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imAdidNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = " ";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imRegnNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_FJT_REGN);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imActNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_FJT_ACT3);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imStaStdNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_FJT_TIME1);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imTimeNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_FJT_TIME2);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imTimeTypeNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = " ";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			for(int ilJ = 0; ilJ < imMaxJobsPerLine; ilJ++)
			{
				rlElement.FrameRight = PRINT_NOFRAME;

				rlElement.Length     = imEmpNameLen;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.Text       = GetString(IDS_FJT_ASSIGNMENT);
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = imJobTimeNameLen;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = imJobTimeNameLen;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		bool bpFirst = true;
		int ilNumJobs = prpLine->Jobs.GetSize();
		int ilJob = 0;
		while(ilJob < ilNumJobs)
		{
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = imFlightNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= ilBottomFrame;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			if(bpFirst)
				rlElement.Text = prpLine->Fnum;
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imAdidNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->Adid;
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imRegnNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->Regn;
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imActNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->Act3;
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imStaStdNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->FlightTime.Format("%H:%M");
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imTimeNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->FlightActualTime.Format("%H:%M");
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = imTimeTypeNameLen;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_NOFRAME;
			if(bpFirst)
				rlElement.Text = prpLine->TimeStatus;
			else
				rlElement.Text = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			for(int ilCurrJob = 0; ilCurrJob < imMaxJobsPerLine; ilCurrJob++, ilJob++)
			{
				FLIGHTJOBSTABLE_JOB *prlJob = NULL;
				if(ilJob < ilNumJobs)
				{
					prlJob = &prpLine->Jobs[ilJob];
				}
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = imEmpNameLen;
				if(ilJob < ilNumJobs)
				{
					rlElement.Text = prlJob->Name;
				}
				else 
				{
					rlElement.Text = "";
				}
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Alignment  = PRINT_CENTER;
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = imJobTimeNameLen;
				if(ilJob < ilNumJobs)
					rlElement.Text       = prlJob->Acfr.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = imJobTimeNameLen;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				if(ilJob < ilNumJobs)
					rlElement.Text       = prlJob->Acto.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			bpFirst = false;
		}
	}

	return TRUE;
}

BOOL FlightJobsTableViewer::PrintFlightJobsTableHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void FlightJobsTableViewer::PrintView()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,150,GetString(IDS_FLIGHTJOBSTABLE_TITLE),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			imMaxJobsPerLine = (pomPrint->smPaperSize == DMPAPER_A3) ? 6 : 4;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_FLIGHTJOBSTABLE_TITLE));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintFlightJobsTableLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintFlightJobsTableHeader(pomPrint);
				}
				PrintFlightJobsTableLine(&omLines[i],FALSE);
			}
			PrintFlightJobsTableLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

void FlightJobsTableViewer::CreateExport(const char *pclFileName, const char *pclSeperator, const char *pclFunctions)
{
	int ilF;
	CString olString;
	CStringArray olFields;


	CStringArray olColumns;
	CCSPtrArray <EXPORT_COLUMN_INFO> olColumnInfo;
	ogBasicData.ExtractItemList(pclFunctions,&olColumns,';');
	for(ilF = 0; ilF < olColumns.GetSize(); ilF++)
	{
		EXPORT_COLUMN_INFO *prlNewColInfo = new EXPORT_COLUMN_INFO;
		olColumnInfo.Add(prlNewColInfo);
		ogBasicData.ExtractItemList(olColumns[ilF],&prlNewColInfo->Functions,',');
		prlNewColInfo->Title = olColumns[ilF];
		prlNewColInfo->Title.Replace(*pclSeperator,' ');
	}

	ofstream of;
	of.open( pclFileName, ios::out);

	// title
	of  << setw(0) << GetString(IDS_FLIGHTJOBSTABLE_TITLE) << "     "  << omStartTime.Format("%d.%m.%Y/%H:%M") << " - " <<  omEndTime.Format("%d.%m.%Y/%H:%M") << endl;

	// column header
	CString olTmp1, olTmp2;
	for(int ilCol = 0; ilCol < olColumnInfo.GetSize(); ilCol++)
	{
		olTmp1 += olColumnInfo[ilCol].Title + "||";
		olTmp2 += "Name|Assigned Function|";
	}
	olString.Format("Flight|Type|Regn|A/C|STA/D|Act.||Work Group|Shift Codes|%sOTHERS|", olTmp1);
	ogBasicData.ExtractItemList(olString,&olFields,'|');
	for(ilF = 0; ilF < olFields.GetSize(); ilF++) 
		of << setw(0) << olFields[ilF] << setw(0) << pclSeperator;
	of << endl;

	olString.Format("|||||||||%sName|Assigned Function|", olTmp2);
	ogBasicData.ExtractItemList(olString,&olFields,'|');
	for(ilF = 0; ilF < olFields.GetSize(); ilF++) 
		of << setw(0) << olFields[ilF] << setw(0) << pclSeperator;
	of << endl;


	EXPORT_FLIGHT olFlight;
	JOBDATA *prlJob, *prlPJ;
	SHIFTDATA *prlShift;
	CString olWorkgroup, olShiftCode, olFunction;
	FLIGHTDATA *prlRotation;

	for(int i = 0; i < omLines.GetSize(); i++ ) 
	{
		FLIGHTJOBSTABLE_LINE *prlLine = &omLines[i];

		olFlight.Flight = prlLine->Fnum;
		olFlight.Regn = prlLine->Regn;
		olFlight.Act3 = prlLine->Act3;
		olFlight.Time = ogBasicData.FormatDate2(prlLine->FlightTime, omStartTime);
		olFlight.ActualTime = ogBasicData.FormatDate2(prlLine->FlightActualTime, omStartTime);
		olFlight.Tisa = prlLine->TimeStatus;
		olFlight.DemType.Empty();

		CString olDemFunction;
		bool blArr = false, blDep = false, blTurn = false;
		int ilNumJobs = prlLine->Jobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			if((prlJob = ogJobData.GetJobByUrno(prlLine->Jobs[ilJ].JobUrno)) != NULL)
			{
				DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
				if(prlDemand != NULL)
				{
					if(*prlDemand->Dety == '0')
						blTurn = true;
					else if(*prlDemand->Dety == '1')
						blArr = true;
					else if(*prlDemand->Dety == '2')
						blDep = true;

					RPFDATA *prlRpf = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlRpf != NULL) olDemFunction = prlRpf->Fcco;
				}

				if((prlPJ = ogJobData.GetJobByUrno(prlJob->Jour)) != NULL && (prlShift = ogShiftData.GetShiftByUrno(prlPJ->Shur)) != NULL)
				{
					olWorkgroup = ogBasicData.GetTeamForPoolJob(prlPJ);
					olShiftCode = (prlShift != NULL) ? prlShift->Sfca : "";
					olFunction = ogBasicData.GetFunctionForPoolJob(prlPJ);

					bool blFound = false;
					while(!blFound)
					{
						int ilNumLines = olFlight.Lines.GetSize();
						for(int ilL = 0; ilL < ilNumLines; ilL++)
						{
							EXPORT_LINE *prlLine = &olFlight.Lines[ilL];
							if(prlLine->WorkGroup == olWorkgroup && prlLine->ShiftCode == olShiftCode)
							{
								bool blCanDisplayInOtherColumn = true;
								for(int ilCol = 0; !blFound && ilCol < olColumnInfo.GetSize(); ilCol++)
								{
									for(int ilFunc = 0; !blFound && ilFunc < olColumnInfo[ilCol].Functions.GetSize(); ilFunc++)
									{
										if(olColumnInfo[ilCol].Functions[ilFunc] == olFunction)
										{
											blCanDisplayInOtherColumn = false;
											if(prlLine->Jobs[ilCol].Name.IsEmpty())
											{
												
												if(IsPrivateProfileOn("FLREL_JOB_REMARK","NO"))
												{
													prlLine->Jobs[ilCol].Name = ogEmpData.GetEmpName(prlJob->Ustf) + " " + ogJobData.GetJobPoolByUrno(prlJob->Urno)->Text;		
												}
												else
												{
													prlLine->Jobs[ilCol].Name = ogEmpData.GetEmpName(prlJob->Ustf);
												}

												prlLine->Jobs[ilCol].Name.Replace(*pclSeperator,' ');
												if(ogBasicData.JobAndDemHaveDiffFuncs(prlJob))
												{
													prlLine->Jobs[ilCol].AssignedFunction = olDemFunction;
													prlLine->Jobs[ilCol].AssignedFunction.Replace(*pclSeperator,' ');
												}
												blFound = true;
												break;
											}
										}
									}
								}
								if(!blFound && blCanDisplayInOtherColumn && prlLine->OTHER.Name.IsEmpty())
								{
									// a function other than the ones specified so display in the "Other" column
									if(IsPrivateProfileOn("FLREL_JOB_REMARK","NO"))
									{
										prlLine->OTHER.Name = ogEmpData.GetEmpName(prlJob->Ustf) + " " + ogJobData.GetJobPoolByUrno(prlJob->Urno)->Text;		
									}
									else
									{
										prlLine->OTHER.Name = ogEmpData.GetEmpName(prlJob->Ustf);
									}


									prlLine->OTHER.Name.Replace(*pclSeperator,' ');
									if(ogBasicData.JobAndDemHaveDiffFuncs(prlJob))
									{
										prlLine->OTHER.AssignedFunction = olDemFunction;
										prlLine->OTHER.AssignedFunction.Replace(*pclSeperator,' ');
									}
									blFound = true;
									break;
								}
							}
						}
						if(!blFound)
						{
							// no free column found so add a new line for this workgroup/shiftcode combination
							EXPORT_LINE *prlNewLine = new EXPORT_LINE;
							prlNewLine->WorkGroup = olWorkgroup;
							prlNewLine->ShiftCode = olShiftCode;
							olFlight.Lines.Add(prlNewLine);
							for(int ilCol = 0; ilCol < olColumnInfo.GetSize(); ilCol++)
							{
								EXPORT_JOB *prlNewJob = new EXPORT_JOB;
								prlNewLine->Jobs.Add(prlNewJob);
							}
						}
					}
				}
			}
		}

		if(blArr)
		{ 
			if(!olFlight.DemType.IsEmpty()) olFlight.DemType += "/"; 
			olFlight.DemType += "A";
		}
		if(blDep)
		{ 
			if(!olFlight.DemType.IsEmpty()) olFlight.DemType += "/"; 
			olFlight.DemType += "D";
		}
		if(blTurn)
		{ 
			if(!olFlight.DemType.IsEmpty()) olFlight.DemType += "/"; 
			olFlight.DemType += "T";
			if((prlRotation = ogFlightData.GetRotationFlight(prlLine->FlightUrno, prlLine->ReturnFlightType)) != NULL)
				olFlight.Flight += CString("/") + prlRotation->Fnum;
		}
		if(olFlight.DemType.IsEmpty()) olFlight.DemType = "X";

		olFlight.Lines.Sort(ByWorkgroupAndShiftCode);
		bool blFirstLine = true;
		olFields.RemoveAll();

		olFields.Add(olFlight.Flight);
		olFields.Add(olFlight.DemType);
		olFields.Add(olFlight.Regn);
		olFields.Add(olFlight.Act3);
		olFields.Add(olFlight.Time);
		olFields.Add(olFlight.ActualTime);
		olFields.Add(olFlight.Tisa);

		for(int ilL = 0; ilL < olFlight.Lines.GetSize(); ilL++)
		{
			if(!blFirstLine) // add empty flight info if this is not the first line printed for this flight
				for(int ilX = 0; ilX < 7; ilX++) olFields.Add("");

			olFields.Add(olFlight.Lines[ilL].WorkGroup);
			olFields.Add(olFlight.Lines[ilL].ShiftCode);
			for(int ilJ = 0; ilJ < olFlight.Lines[ilL].Jobs.GetSize(); ilJ++)
			{
				olFields.Add(olFlight.Lines[ilL].Jobs[ilJ].Name);
				olFields.Add(olFlight.Lines[ilL].Jobs[ilJ].AssignedFunction);
			}
			olFields.Add(olFlight.Lines[ilL].OTHER.Name);
			olFields.Add(olFlight.Lines[ilL].OTHER.AssignedFunction);

			for(ilF = 0; ilF < olFields.GetSize(); ilF++) 
				of << setw(0) << ogBasicData.FormatFieldForExport(olFields[ilF], pclSeperator) << setw(0) << pclSeperator;
			of << endl;

			blFirstLine = false;
			olFields.RemoveAll();
		}

		if(blFirstLine)
		{
			for(ilF = 0; ilF < olFields.GetSize(); ilF++) 
				of << setw(0) << ogBasicData.FormatFieldForExport(olFields[ilF], pclSeperator) << setw(0) << pclSeperator;
			of << endl;
		}

		for(ilF = 0; ilF < olFlight.Lines.GetSize(); ilF++)
			olFlight.Lines[ilF].Jobs.DeleteAll();
		olFlight.Lines.DeleteAll();
	}

	of << endl;
	of.close();

	olColumnInfo.DeleteAll();
}
