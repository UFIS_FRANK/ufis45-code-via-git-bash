// CedaPstData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaPstData.h>
#include <BasicData.h>

CedaPstData::CedaPstData()
{                  
    BEGIN_CEDARECINFO(PSTDATA, PstDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Pnam,"PNAM")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_DATE(Nafr,"NAFR")
		FIELD_DATE(Nato,"NATO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PstDataRecInfo)/sizeof(PstDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PstDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"PSTTAB");
    pcmFieldList = "URNO,PNAM,VAFR,VATO,NAFR,NATO";
}
 
CedaPstData::~CedaPstData()
{
	TRACE("CedaPstData::~CedaPstData called\n");
	ClearAll();
}

void CedaPstData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaPstData::ReadPstData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPstData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		PSTDATA rlPstData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPstData)) == RCSuccess)
			{
				AddPstInternal(rlPstData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPstData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(PSTDATA), pclWhere);
    return ilRc;
}


void CedaPstData::AddPstInternal(PSTDATA &rrpPst)
{
	PSTDATA *prlPst = new PSTDATA;
	*prlPst = rrpPst;
	omData.Add(prlPst);
	omUrnoMap.SetAt((void *)prlPst->Urno,prlPst);
	omNameMap.SetAt(prlPst->Pnam,prlPst);
}


PSTDATA* CedaPstData::GetPstByUrno(long lpUrno)
{
	PSTDATA *prlPst = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPst);
	return prlPst;
}

PSTDATA* CedaPstData::GetPstByName(const char *pcpName)
{
	PSTDATA *prlPst = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlPst);
	return prlPst;
}

long CedaPstData::GetPstUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	PSTDATA *prlPst = GetPstByName(pcpName);
	if(prlPst != NULL)
	{
		llUrno = prlPst->Urno;
	}
	return llUrno;
}


CString CedaPstData::GetTableName(void)
{
	return CString(pcmTableName);
}


void CedaPstData::GetAllPstNames(CStringArray &ropPstNames)
{
	int ilNumPsts = omData.GetSize();
	for(int ilPst = 0; ilPst < ilNumPsts; ilPst++)
	{
		ropPstNames.Add(omData[ilPst].Pnam);
	}
}