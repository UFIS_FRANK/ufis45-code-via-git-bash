// gbar.cpp : implementation file
//

#include <stdafx.h>
#include <gbar.h>
#include <resource.h>
#include <BasicData.h>

GanttBar::GanttBar()
{
}

GanttBar::GanttBar(CDC *popDC, const CRect &opRect, int ipFrameType,
    int ipMarkerType, CBrush *popMarkerBrush,
    const char *pcpText, CFont *popTextFont, COLORREF lpTextColor, int ipFramePixel,
	BOOL isRightMarker, const CRect &opRightMarkerRect, 
	CBrush *popRightMarkerBrush, CCSPtrArray<BARDECO> *ropDeco, UINT upAlignText,   // fpa
	const char *pcpAddictionalText, UINT upAlignAdditionalText, COLORREF lpFrameColor)
{
	Paint(popDC, opRect, ipFrameType, ipMarkerType, ipFramePixel, popMarkerBrush, pcpText, 
		popTextFont, lpTextColor,isRightMarker,opRightMarkerRect,popRightMarkerBrush,ropDeco,
		upAlignText, pcpAddictionalText, upAlignAdditionalText, lpFrameColor);
}

void GanttBar::Paint(CDC *popDC, const CRect &opRect, int ipFrameType,
    int ipMarkerType, int ipFramePixel, CBrush *popMarkerBrush,
    const char *pcpText, CFont *popTextFont, COLORREF lpTextColor,
	BOOL isRightMarker, const CRect &opRightMarkerRect, 
	CBrush *popRightMarkerBrush, CCSPtrArray<BARDECO> *ropDeco, UINT upAlignText,   // fpa
	const char *pcpAddictionalText, UINT upAlignAdditionalText, COLORREF lpFrameColor)
{
    CRect rcBackground(opRect); // for drawing bar background
    CRect rcMarker(opRect);     // for drawing bar marker
    CRect rcBar(opRect);        // for drawing frame and text
    CBrush brWhite(RGB(255, 255, 255));

    COLORREF nOldTextColor = popDC->SetTextColor(lpTextColor);

    // draw bar background and marker
    switch (ipMarkerType)
    {
    case MARKNONE:
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcBackground, &brWhite);
        break;
    case MARKLEFT:
        rcMarker.right = (rcMarker.left + rcMarker.right) / 2;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcMarker, popMarkerBrush);
        rcBackground.left = rcMarker.right;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcBackground, &brWhite);
        break;
    case MARKRIGHT:
        rcBackground.right = (rcBackground.left + rcBackground.right) / 2;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcBackground, &brWhite);
        rcMarker.left = rcBackground.right;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcMarker, popMarkerBrush);
        break;
    case MARKFULL:
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        popDC->FillRect(&rcMarker, popMarkerBrush);
        break;
    }

	if (isRightMarker)
	{
        popDC->FillRect(&opRightMarkerRect, popRightMarkerBrush);
	}
    // extra pixel to help ExtTextOut() work correctly
    rcBar.right++;

    // draw bar text (with clipping)
    CFont *pOldFont = popDC->SelectObject(popTextFont);
    int nBkMode = popDC->SetBkMode(TRANSPARENT);
    UINT nTextAlign = popDC->SetTextAlign(upAlignText);
	int x;


	// text position can be left/right/centre
	if (upAlignText == TA_LEFT)
		x = opRect.left + 3;
	else if (upAlignText == TA_RIGHT)
		x = opRect.right - 3;
	else
		x = (opRect.left + opRect.right) / 2;
	LOGFONT lf;
	popTextFont->GetLogFont(&lf);
	int height = - (lf.lfHeight); // It is negative
	int y = (opRect.Height() - height) / 2 + opRect.top -1;
    popDC->ExtTextOut(x, y, ETO_CLIPPED, &rcBar, pcpText, lstrlen(pcpText), NULL);
	if (pcpAddictionalText != NULL)
	{
		popDC->SetTextAlign(upAlignAdditionalText);
		if (upAlignAdditionalText == TA_LEFT)
			x = opRect.left + 3;
		else if (upAlignAdditionalText == TA_RIGHT)
			x = opRect.right - 3;
		else
			x = (opRect.left + opRect.right) / 2;
		popDC->ExtTextOut(x, y, ETO_CLIPPED, &rcBar, pcpAddictionalText, lstrlen(pcpAddictionalText), NULL);

	}

    popDC->SetTextAlign(nTextAlign);
    popDC->SetBkMode(nBkMode);
    popDC->SetTextColor(nOldTextColor);
    popDC->SelectObject(pOldFont);

	// paint additional decoration into the bar
	if(ropDeco != NULL)
	{
		int ilCount = ropDeco->GetSize();
		for(int ili = 0; ili < ilCount; ili++)
		{
			BARDECO prlDeco = ropDeco->GetAt(ili);
			CBrush brFillBrush;
			if(prlDeco.Hatched != -1)
				brFillBrush.CreateHatchBrush(prlDeco.Hatched, prlDeco.Color);
			else if(prlDeco.Bitmap != NULL)
				brFillBrush.CreatePatternBrush(prlDeco.Bitmap);
			else
				brFillBrush.CreateSolidBrush(prlDeco.Color);

			switch(prlDeco.type)
			{
			case BD_BAR:
				popDC->FillRect(&prlDeco.Rect, &brFillBrush);
				break;
			case BD_STRING:
				pOldFont = popDC->SelectObject(popTextFont);
				nOldTextColor = popDC->SetTextColor(prlDeco.Color);
				nBkMode = popDC->SetBkMode(TRANSPARENT);
				nTextAlign = popDC->SetTextAlign(TA_CENTER);
				x = (prlDeco.Rect.left + prlDeco.Rect.right) / 2;
				popDC->ExtTextOut(x, prlDeco.Rect.top + 1, ETO_CLIPPED, 
							      &prlDeco.Rect, prlDeco.Value, 
								  prlDeco.Value.GetLength(), NULL);
				popDC->SetTextAlign(nTextAlign);
				popDC->SetBkMode(nBkMode);
				popDC->SetTextColor(nOldTextColor);
				popDC->SelectObject(pOldFont);
				break;
			case BD_REGION:
				if(prlDeco.Region != NULL)
					popDC->FillRgn(prlDeco.Region, &brFillBrush);
				break;
			case BD_CIRCLE:
				{
					CBrush *popOldBrush = popDC->SelectObject(&brFillBrush);
					popDC->Ellipse(&prlDeco.Rect);
					popDC->SelectObject(popOldBrush);
				}
				break;
			}
		}
		ropDeco++;
	}

    // draw frame over bar if necessary
    if (ipFrameType == FRAMERECT)
    {
//		if(ipFramePixel > 1)
//			rcBar.bottom--;

		COLORREF rlColour = lpFrameColor;
		CBrush brBlack(rlColour);
		for(int i = 0; i < ipFramePixel; i++)
		{
			popDC->FrameRect(&rcBar, &brBlack);
			rcBar.top++;
			rcBar.bottom--;
			rcBar.left++;
			rcBar.right--;
		}
    }
	else if (ipFrameType == FRAMEBACKGROUND)
	{
		CBrush brBackground(RGB(147,147,147));
		popDC->FrameRect(&rcBar, &brBackground);
	}

}

void GanttBar::MakeOfflineSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect)
{
	MakeOfflineSymbol(ropDecoData, ropBarRect.left, ropBarRect.top, ropBarRect.right, ropBarRect.bottom);
}

void GanttBar::MakeOfflineSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom)
{
	BARDECO *prlDeco = new BARDECO;
	int ilHeight = ipBottom - ipTop - 2;
	prlDeco->Rect = CRect(ipRight-ilHeight,ipTop+1,ipRight,ipTop+ilHeight+1);
	prlDeco->Color = RGB(255,255,0); // yellow
	prlDeco->type = BD_CIRCLE;
	ropDecoData.Add(prlDeco);
}

void GanttBar::MakeFunctionSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect)
{
	MakeFunctionSymbol(ropDecoData, ropBarRect.left, ropBarRect.top, ropBarRect.right, ropBarRect.bottom);
}

void GanttBar::MakeFunctionSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom)
{
	COLORREF olColour;
	if(ogBasicData.GetDiffFuncColour(olColour))
	{
		BARDECO *prlDeco = new BARDECO;
		int ilHeight = ipBottom - ipTop - 2;
		prlDeco->Rect = CRect(ipRight-ilHeight-1,ipTop+1,ipRight,ipTop+ilHeight+1);
		prlDeco->Color = olColour;
		prlDeco->type = BD_BAR;
		ropDecoData.Add(prlDeco);
	}
}

void GanttBar::MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect)
{
	MakeEmpInformedSymbol(ropDecoData, ropBarRect.left, ropBarRect.top, ropBarRect.right, ropBarRect.bottom);
}

void GanttBar::MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom)
{
	MakeEmpInformedSymbol(ropDecoData, ipLeft, ipTop, ipRight, ipBottom,RGB(0,0,0));

}

void GanttBar::MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom,COLORREF ipColor)
{
	BARDECO *prlDeco = new BARDECO;
	prlDeco->Rect = CRect(ipLeft+1,ipTop+1,ipLeft+11,ipTop+11);
	prlDeco->Color = ipColor;
	prlDeco->type = BD_CIRCLE;
	ropDecoData.Add(prlDeco);
}