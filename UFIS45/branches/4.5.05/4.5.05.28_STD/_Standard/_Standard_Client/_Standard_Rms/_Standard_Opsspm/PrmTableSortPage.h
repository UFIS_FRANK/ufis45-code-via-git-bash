// PRMTableSortPage.h : header file
//
#ifndef _PRMTBLSO_H_
#define _PRMTBLSO_H_
/////////////////////////////////////////////////////////////////////////////
// PRMTableSortPage dialog

class PRMTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(PRMTableSortPage)

// Construction
public:
	PRMTableSortPage();
	~PRMTableSortPage();

// Dialog Data
	CStringArray omSortOrders;

	//{{AFX_DATA(PRMTableSortPage)
	enum { IDD = IDD_PRMTABLE_SORT_PAGE };
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PRMTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PRMTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _PRMTBLSO_H_
