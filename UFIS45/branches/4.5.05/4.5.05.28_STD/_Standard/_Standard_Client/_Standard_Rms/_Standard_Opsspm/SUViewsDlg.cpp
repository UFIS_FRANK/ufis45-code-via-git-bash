// SUViewsDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <cviewer.h>
#include <CedaCfgData.h>
#include <SUViewsDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SUViewsDlg dialog


SUViewsDlg::SUViewsDlg(CWnd* pParent /*=NULL*/, CString *popString, 
					   USERSETUPDATA *popUserSetup, CString popMode)
	: CDialog(SUViewsDlg::IDD, pParent)
{
	pomString = popString;
	pomUserSetup = popUserSetup;
	imMode = popMode;
	//{{AFX_DATA_INIT(SUViewsDlg)
	m_Attentiontbl = _T("");
	m_CCIBel = _T("");
	m_CCIChrt = _T("");
	m_Conflikttbl = _T("");
	m_Flightplan = _T("");
	m_Gatebel = _T("");
	m_Gatechrt = _T("");
	m_EquipmentChart = _T("");
	m_Regnchrt = _T("");
	m_Preplanttbl = _T("");
	m_Requests = _T("");
	m_Staffchart = _T("");
	m_Stafflist = _T("");
	m_Poschrt = _T("");
	//}}AFX_DATA_INIT
}


void SUViewsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SUViewsDlg)
	DDX_Control(pDX, IDC_ATTENTIONTBL, c_Attentiontbl);
	DDX_Control(pDX, IDC_STAFFLIST, c_Stafflist);
	DDX_Control(pDX, IDC_STAFFCHART, c_Staffchart);
	DDX_Control(pDX, IDC_REQUESTS, c_Requests);
	DDX_Control(pDX, IDC_PREPLANTTBL, c_Preplanttbl);
	DDX_Control(pDX, IDC_GATECHART, c_Gatechrt);
	DDX_Control(pDX, IDC_EQUIPMENTCHART, c_EquipmentChart);
	DDX_Control(pDX, IDC_REGNCHART, c_Regnchrt);
	DDX_Control(pDX, IDC_POSCHART, c_Poschrt);
	DDX_Control(pDX, IDC_GATEBEL, c_Gatebel);
	DDX_Control(pDX, IDC_FLIGHTPLAN, c_Flightplan);
	DDX_Control(pDX, IDC_CONFLICTTBL, c_conflicttbl);
	DDX_Control(pDX, IDC_CCICHART, c_Ccichart);
	DDX_Control(pDX, IDC_CCIBEL, c_CciBel);
	DDX_CBString(pDX, IDC_ATTENTIONTBL, m_Attentiontbl);
	DDX_CBString(pDX, IDC_CCIBEL, m_CCIBel);
	DDX_CBString(pDX, IDC_CCICHART, m_CCIChrt);
	DDX_CBString(pDX, IDC_CONFLICTTBL, m_Conflikttbl);
	DDX_CBString(pDX, IDC_FLIGHTPLAN, m_Flightplan);
	DDX_CBString(pDX, IDC_GATEBEL, m_Gatebel);
	DDX_CBString(pDX, IDC_GATECHART, m_Gatechrt);
	DDX_CBString(pDX, IDC_EQUIPMENTCHART, m_EquipmentChart);
	DDX_CBString(pDX, IDC_REGNCHART, m_Regnchrt);
	DDX_CBString(pDX, IDC_PREPLANTTBL, m_Preplanttbl);
	DDX_CBString(pDX, IDC_REQUESTS, m_Requests);
	DDX_CBString(pDX, IDC_STAFFCHART, m_Staffchart);
	DDX_CBString(pDX, IDC_STAFFLIST, m_Stafflist);
	DDX_CBString(pDX, IDC_POSCHART, m_Poschrt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SUViewsDlg, CDialog)
	//{{AFX_MSG_MAP(SUViewsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SUViewsDlg message handlers

void SUViewsDlg::OnOK() 
{
	*pomString = CString("");
	UpdateData();

	if(m_Attentiontbl == CString(""))
		m_Attentiontbl = GetString(IDS_STRINGDEFAULT);
	if(m_CCIBel == CString(""))
		m_CCIBel = GetString(IDS_STRINGDEFAULT);
	if(m_CCIChrt == CString(""))
		m_CCIChrt = GetString(IDS_STRINGDEFAULT);
	if(m_Conflikttbl == CString(""))
		m_Conflikttbl = GetString(IDS_STRINGDEFAULT);
	if(m_Flightplan == CString(""))
		m_Flightplan = GetString(IDS_STRINGDEFAULT);
	if(m_Gatebel == CString(""))
		m_Gatebel = GetString(IDS_STRINGDEFAULT);
	if(m_Gatechrt == CString(""))
		m_Gatechrt = GetString(IDS_STRINGDEFAULT);
	if(m_EquipmentChart == CString(""))
		m_EquipmentChart = GetString(IDS_STRINGDEFAULT);
	if(m_Regnchrt == CString(""))
		m_Regnchrt = GetString(IDS_STRINGDEFAULT);
	if(m_Preplanttbl == CString(""))
		m_Preplanttbl = GetString(IDS_STRINGDEFAULT);
	if(m_Requests == CString(""))
		m_Requests = GetString(IDS_STRINGDEFAULT);
	if(m_Staffchart == CString(""))
		m_Staffchart = GetString(IDS_STRINGDEFAULT);
	if(m_Stafflist == CString(""))
		m_Stafflist = GetString(IDS_STRINGDEFAULT);
	if(m_Poschrt == CString(""))
		m_Poschrt = GetString(IDS_STRINGDEFAULT);
	
	*pomString = CString("ATTV=") + m_Attentiontbl + CString(";")
	+ CString("CCBV=") + m_CCIBel + CString(";")
	+ CString("CCCV=") + m_CCIChrt + CString(";")
	+ CString("CONV=") + m_Conflikttbl + CString(";")
	+ CString("FPLV=") + m_Flightplan + CString(";")
	+ CString("GBLV=") + m_Gatebel + CString(";")
	+ CString("GACV=") + m_Gatechrt + CString(";")
	+ CString("EQCV=") + m_EquipmentChart + CString(";")
	+ CString("RECV=") + m_Regnchrt + CString(";")
	+ CString("VPLV=") + m_Preplanttbl + CString(";")
	+ CString("RQSV=") + m_Requests + CString(";")	
	+ CString("STCV=") + m_Staffchart + CString(";") 
	+ CString("STLV=") + m_Stafflist + CString(";")
	+ CString("PSCV=") + m_Poschrt + CString(";");

	CDialog::OnOK();
}

BOOL SUViewsDlg::OnInitDialog() 
{
	int i = 0;
	int ilIndex = 0;
	int ilCount=0;
	CString olS;

	CDialog::OnInitDialog();

	c_Preplanttbl.EnableWindow(ogBasicData.bmDisplayPrePlanning);
	c_Gatechrt.EnableWindow(ogBasicData.bmDisplayGates);
	c_EquipmentChart.EnableWindow(ogBasicData.bmDisplayEquipment);
	c_Ccichart.EnableWindow(ogBasicData.bmDisplayCheckins);
	c_Gatebel.EnableWindow(ogBasicData.bmDisplayGates);
	c_CciBel.EnableWindow(ogBasicData.bmDisplayCheckins);
	c_Regnchrt.EnableWindow(ogBasicData.bmDisplayRegistrations);
	c_Poschrt.EnableWindow(ogBasicData.bmDisplayPositions);

	CViewer *polViewer = new CViewer();
	CStringArray olData;
	polViewer->SetViewerKey("CciDia");
	polViewer->GetViews(olData);
	ilCount = olData.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData.GetAt(i);
		c_Ccichart.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Ccichart.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData2;
	polViewer->SetViewerKey("CciTab");
	polViewer->GetViews(olData2);
	ilCount = olData2.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData2.GetAt(i);
		c_CciBel.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_CciBel.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData3;
	polViewer->SetViewerKey("ConfTab");
	polViewer->GetViews(olData3);
	ilCount = olData3.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData3.GetAt(i);
		c_conflicttbl.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_conflicttbl.AddString(GetString(IDS_STRINGDEFAULT));
	}
	CStringArray olData4;
	polViewer->SetViewerKey("FltSched");
	polViewer->GetViews(olData4);
	ilCount = olData4.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData4.GetAt(i);
		c_Flightplan.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Flightplan.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData5;
	polViewer->SetViewerKey("GateDia");
	polViewer->GetViews(olData5);
	ilCount = olData5.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData5.GetAt(i);
		c_Gatechrt.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Gatechrt.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData15;
	polViewer->SetViewerKey("EquipmentDiagram");
	polViewer->GetViews(olData15);
	ilCount = olData15.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData15.GetAt(i);
		c_EquipmentChart.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_EquipmentChart.AddString(GetString(IDS_STRINGDEFAULT));
	}


	CStringArray olData14;
	polViewer->SetViewerKey("RegnDia");
	polViewer->GetViews(olData14);
	ilCount = olData14.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData14.GetAt(i);
		c_Regnchrt.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Regnchrt.AddString(GetString(IDS_STRINGDEFAULT));
	}


	CStringArray olData6;
	polViewer->SetViewerKey("GateTab");
	polViewer->GetViews(olData6);
	ilCount = olData6.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData6.GetAt(i);
		c_Gatebel.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Gatebel.AddString(GetString(IDS_STRINGDEFAULT));
	}


	CStringArray olData8;
	polViewer->SetViewerKey("PPTab");
	polViewer->GetViews(olData8);
	ilCount = olData8.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData8.GetAt(i);
		c_Preplanttbl.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Preplanttbl.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData9;
	polViewer->SetViewerKey("ReqTab");
	polViewer->GetViews(olData9);
	ilCount = olData9.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData9.GetAt(i);
		c_Requests.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Requests.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData10;
	polViewer->SetViewerKey("StaffDia");
	polViewer->GetViews(olData10);
	ilCount = olData10.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData10.GetAt(i);
		c_Staffchart.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Staffchart.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData11;
	polViewer->SetViewerKey("StaffTab");
	polViewer->GetViews(olData11);
	ilCount = olData11.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData11.GetAt(i);
		c_Stafflist.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Stafflist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData12;
	polViewer->SetViewerKey("ConfTab");
	polViewer->GetViews(olData12);
	ilCount = olData12.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData12.GetAt(i);
		c_Attentiontbl.AddString(olS);
	}
	if(ilCount == 0)
	{
		//c_Attentiontbl.AddString("<Default>");
		c_Attentiontbl.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData13;
	polViewer->SetViewerKey("PosDia");
	polViewer->GetViews(olData13);
	ilCount = olData13.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData13.GetAt(i);
		c_Poschrt.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Poschrt.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData28;
	polViewer->SetViewerKey("PrmTab");
	polViewer->GetViews(olData28);
	ilCount = olData28.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		olS = olData28.GetAt(i);
		c_Prmtbl.AddString(olS);
	}
	if(ilCount == 0)
	{
		c_Prmtbl.AddString(GetString(IDS_STRINGDEFAULT));
	}


	if(imMode == CString("UPDATE_MODE"))
	{
		ilIndex = c_Stafflist.FindString(0, pomUserSetup->STLV);
		c_Stafflist.SetCurSel(max(ilIndex,0));
		ilIndex = c_Staffchart.FindString(0, pomUserSetup->STCV);
		c_Staffchart.SetCurSel(max(ilIndex,0));
		ilIndex = c_Requests.FindString(0, pomUserSetup->RQSV);
		c_Requests.SetCurSel(max(ilIndex,0));
		ilIndex = c_Preplanttbl.FindString(0, pomUserSetup->VPLV);
		c_Preplanttbl.SetCurSel(max(ilIndex,0));
		ilIndex = c_Gatechrt.FindString(0, pomUserSetup->GACV);
		c_Gatechrt.SetCurSel(max(ilIndex,0));
		ilIndex = c_EquipmentChart.FindString(0, pomUserSetup->EQCV);
		c_EquipmentChart.SetCurSel(max(ilIndex,0));
		ilIndex = c_Gatechrt.FindString(0, pomUserSetup->RECV);
		c_Gatechrt.SetCurSel(max(ilIndex,0));
		ilIndex = c_Gatebel.FindString(0, pomUserSetup->GBLV);
		c_Gatebel.SetCurSel(max(ilIndex,0));
		ilIndex = c_Flightplan.FindString(0, pomUserSetup->FPLV);
		c_Flightplan.SetCurSel(max(ilIndex,0));
		ilIndex = c_conflicttbl.FindString(0, pomUserSetup->CONV);
		c_conflicttbl.SetCurSel(max(ilIndex,0));
		ilIndex = c_Ccichart.FindString(0, pomUserSetup->CCCV);
		c_Ccichart.SetCurSel(max(ilIndex,0));
		ilIndex = c_CciBel.FindString(0, pomUserSetup->CCBV);
		c_CciBel.SetCurSel(max(ilIndex,0));
		ilIndex = c_Attentiontbl.FindString(0, pomUserSetup->ATTV);
		c_Attentiontbl.SetCurSel(max(ilIndex,0));
		ilIndex = c_Poschrt.FindString(0, pomUserSetup->PSCV);
		c_Poschrt.SetCurSel(max(ilIndex,0));

	}
	delete polViewer;
	olData.RemoveAll();
	olData2.RemoveAll();
	olData3.RemoveAll();
	olData4.RemoveAll();
	olData5.RemoveAll();
	olData6.RemoveAll();
	olData8.RemoveAll();
	olData9.RemoveAll();
	olData10.RemoveAll();
	olData11.RemoveAll();
	olData12.RemoveAll();
	olData13.RemoveAll();
	olData14.RemoveAll();
	olData15.RemoveAll();
	return TRUE;  
}
