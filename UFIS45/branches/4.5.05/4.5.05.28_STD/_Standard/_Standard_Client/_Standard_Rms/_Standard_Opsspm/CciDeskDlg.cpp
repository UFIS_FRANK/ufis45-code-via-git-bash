// Ccidesk.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CciDeskDlg.h>
#include <CCSTime.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCciDeskDialog dialog


CCciDeskDialog::CCciDeskDialog(CWnd* pParent /*=NULL*/,CTime opDate)
	: CDialog(CCciDeskDialog::IDD, pParent)
{
	if (opDate == TIMENULL)
	{
		m_Hour = ogBasicData.GetTime();
	}
	else
	{
		m_Date = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
		CTime olNow = ogBasicData.GetTime();
		m_Hour = m_Date + CTimeSpan(0,olNow.GetHour(),olNow.GetMinute(),0);
	}
	int ilRoundedMinute = (m_Hour.GetMinute() + 2) / 5 * 5;
	m_Hour += CTimeSpan(0, 0, ilRoundedMinute - m_Hour.GetMinute(), 0);
	//{{AFX_DATA_INIT(CCciDeskDialog)
	m_CciName = ("");
	m_Desk = ("");
	m_Min = 0;
	m_Duration = -1;
	//}}AFX_DATA_INIT
}


void CCciDeskDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCciDeskDialog)
	DDX_Text(pDX, IDC_CCINAME, m_CciName);
	DDX_Text(pDX, IDC_DESK, m_Desk);
	DDX_CCSTime(pDX, IDC_HOUR, m_Hour);
	DDX_CCSddmmyy(pDX, IDC_DATE, m_Date);
	DDX_Text(pDX, IDC_MIN, m_Min);
	DDV_MinMaxInt(pDX, m_Min, 0, 1440);
	DDX_Radio(pDX, IDC_MIN30, m_Duration);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCciDeskDialog, CDialog)
	//{{AFX_MSG_MAP(CCciDeskDialog)
	ON_EN_KILLFOCUS(IDC_MIN, OnKillfocusMin)
	ON_BN_CLICKED(IDC_1STD, On1std)
	ON_BN_CLICKED(IDC_2STD, On2std)
	ON_BN_CLICKED(IDC_MIN30, OnMin30)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCciDeskDialog message handlers

BOOL CCciDeskDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32888));
	CWnd *polWnd = GetDlgItem(IDC_EMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32849));
	}
	polWnd = GetDlgItem(IDC_CCIDESK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32889));
	}
	polWnd = GetDlgItem(IDC_DURATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32890));
	}
	polWnd = GetDlgItem(IDC_CCIDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32855));
	}
	polWnd = GetDlgItem(IDC_CCITIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32891));
	}
	polWnd = GetDlgItem(IDC_CCIDURATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32890));
	}
	polWnd = GetDlgItem(IDC_CCIMIN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32892));
	}
	polWnd = GetDlgItem(IDC_MIN30); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32893));
	}
	polWnd = GetDlgItem(IDC_2STD); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32894));
	}
	polWnd = GetDlgItem(IDC_1STD); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32895));
	}
	polWnd = GetDlgItem(IDC_SONST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32854));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	

	// DTT Jul.18 -- Round the time to 5 minutes
	int ilRoundedMinute = (m_Hour.GetMinute() + 2) / 5 * 5;
	m_Hour += CTimeSpan(0, 0, ilRoundedMinute - m_Hour.GetMinute(), 0);

	CenterWindow();

   	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCciDeskDialog::OnOK() 
{

	CDialog::OnOK();

	m_Hour = CTime(m_Date.GetYear(), m_Date.GetMonth(),
			m_Date.GetDay(), m_Hour.GetHour(), m_Hour.GetMinute(),
			m_Hour.GetSecond());
}

void CCciDeskDialog::OnKillfocusMin() 
{
	UpdateData();
	switch (m_Min)
	{
	case 30:
  		m_Duration = 0;
		break;
    case 60:
        m_Duration = 1;
		break;
	case 120:
		m_Duration = 2;
		break;
	default:
        m_Duration = 3;
		break; 
	}
	UpdateData(FALSE);
}


void CCciDeskDialog::OnMin30() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	m_Min = 30;
	UpdateData(FALSE);
}

void CCciDeskDialog::On1std() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	m_Min = 60;
	UpdateData(FALSE);
}

void CCciDeskDialog::On2std() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	m_Min = 120;
	UpdateData(FALSE);
}
