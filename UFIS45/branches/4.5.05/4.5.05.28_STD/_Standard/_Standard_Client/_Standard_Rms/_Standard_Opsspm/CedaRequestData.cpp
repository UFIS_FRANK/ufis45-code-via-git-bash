// CedaRequestData.cpp - Class for handling demand profile data
//



#include <stdafx.h>
#include <ccsglobl.h>
#include <resource.h>
#include <CCSCedaCom.h>
#include <OpssPm.h>
#include <ccsddx.h>
#include <CCSCedaData.h>
#include <cxbutton.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <CedaFlightData.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <ConflictTable.h>
#include <mreqtable.h>
#include <ButtonList.h>
#include <CedaDprData.h>
#include <CedaReqData.h>




void  ProcessRequestCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRequestData::CedaRequestData(CString opTableType)
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(REQUESTDATA, RequestDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Stat,"STAT")
        FIELD_DATE(Rdat,"RDAT")
        FIELD_CHAR_TRIM(Cusr,"CUSR")
        FIELD_DATE(Adat,"ADAT")
		FIELD_DATE(Rqda,"RQDA")
		FIELD_CHAR_TRIM(Peno,"PKNO")
		FIELD_CHAR_TRIM(Text,"TEXT")
//		FIELD_CHAR_TRIM(Usec,"USEC")
//        FIELD_DATE(Cdat,"CDAT")
//		FIELD_CHAR_TRIM(Useu,"USEU")
//		FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RequestDataRecInfo)/sizeof(RequestDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RequestDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"NOTTAB");
    pcmFieldList = "URNO,STAT,RDAT,CUSR,ADAT,RQDA,PKNO,TEXT";
	omTableType = opTableType;
	ogCCSDdx.Register((void *)this,BC_REQUEST_CHANGE,CString("REQUESTDATA"), CString("Request changed"),ProcessRequestCf);
}
 
CedaRequestData::~CedaRequestData()
{
	TRACE("CedaRequestData::~CedaRequestData called\n");
	ogCCSDdx.UnRegister(this, NOTUSED);
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaRequestData::ReadRequestData()
{
    char pclWhere[512];
	CCSReturnCode ilRc = RCSuccess;
	int ilLc;

    // Initialize table names and field names
	CTime olTime = ogBasicData.GetTime();
	sprintf(pclWhere,"WHERE RQDA >= '%s000000' AND STAT = '%s'"
		,olTime.Format("%Y%m%d"),omTableType);
	char pclCom[10] = "RT";
    if (CedaAction2(pclCom,pclWhere) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaRequestData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
        return RCFailure;
	}
    omData.DeleteAll();

	int ilCount = 0;
    for (ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		REQUESTDATA *prlRequestData = new REQUESTDATA;

		if ((ilRc = GetBufferRecord2(ilLc,prlRequestData)) == RCSuccess)
		{
			AddRequestInternal(prlRequestData);
				ilCount++;
		}
		else
		{
			delete prlRequestData;
		}
	}
	
	if (ilCount > 0)
	{
		if (omTableType == "R")
			pogButtonList->SetRequestColor(RED);
		else
			pogButtonList->SetInfoColor(RED);
	}
	ogBasicData.Trace("CedaRequestData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(REQUESTDATA), pclWhere);
    return RCSuccess;
}

CCSReturnCode CedaRequestData::AddRequest(REQUESTDATA * prpRequest)
{
	REQUESTDATA *prlRequest = new REQUESTDATA;
	memcpy(prlRequest,prpRequest,sizeof(REQUESTDATA));

	prlRequest->IsChanged = DATA_NEW;
	AddRequestInternal(prlRequest);
	ogCCSDdx.DataChanged((void *)this,REQUEST_NEW,(void *)prlRequest);
	SaveRequest(prlRequest);
//    RC(RCSuccess);
    return RCSuccess;
}


CCSReturnCode CedaRequestData::AddRequestInternal(REQUESTDATA * prpRequest)
{
	omData.Add(prpRequest);
	omUrnoMap.SetAt((void *)prpRequest->Urno,prpRequest);
//  RC(RCSuccess);
    return RCSuccess;
	
}

CCSReturnCode CedaRequestData::SaveRequest(REQUESTDATA * prpRequest)
{

	CCSReturnCode olRc = RCSuccess;
	CString olListOfData;
	char pclWhere[124];
	char pclData[524];


	if ((prpRequest->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return RCSuccess; // no change, nothing to do
	}

	prpRequest->Adat = ogBasicData.GetTime();
	switch(prpRequest->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpRequest);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpRequest->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclWhere,"WHERE URNO = '%ld'",prpRequest->Urno);
		MakeCedaData(olListOfData,prpRequest);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclWhere,"",pclData);
		prpRequest->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclWhere,"WHERE URNO = '%ld'",prpRequest->Urno);
		olRc = CedaAction("DRT",pclWhere);
		if (olRc == RCSuccess)
		{
			for (int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
			{
				if (&omData[ilLc] == prpRequest)
				{
					omData.RemoveAt(ilLc);
					delete prpRequest;
					break;
				}
			}
		}
		break;
	}

	
//	RC(RCSuccess);
    return RCSuccess;

}

CCSReturnCode CedaRequestData::SaveAllRequests(void)
{
	CCSReturnCode olRc = RCSuccess;
	int ilRequestCount = omData.GetSize();
	for ( int ilLc = 0; ilLc < ilRequestCount; ilLc++)
	{
		SaveRequest(&omData[ilLc]);
	}
// RC(olRc);
 return(olRc);
}

CCSReturnCode CedaRequestData::DeleteRequest(long lpUrno)
{

	REQUESTDATA *prpRequest = GetRequestByUrno(lpUrno);
	if (prpRequest != NULL)
	{
		prpRequest->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);

		ogCCSDdx.DataChanged((void *)this,REQUEST_DELETE,(void *)prpRequest);
		SaveRequest(prpRequest);
	}
//    RC(RCSuccess);
    return RCSuccess;
}



REQUESTDATA  *CedaRequestData::GetRequestByUrno(long lpUrno)
{
	REQUESTDATA  *prlRequest;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRequest) == TRUE)
	{
		return prlRequest;
	}
	return NULL;
}


void  ProcessRequestCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_REQUEST_CHANGE :
		((CedaRequestData *)popInstance)->ProcessRequestBc(ipDDXType,
								vpDataPointer,ropInstanceName);
		break;
	}
}


void  CedaRequestData::ProcessRequestBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (ipDDXType == BC_REQUEST_CHANGE)
	{
		REQUESTDATA *prpRequest;
		struct BcStruct *prlRequestData;

		prlRequestData = (struct BcStruct *) vpDataPointer;
		ogBasicData.LogBroadcast(prlRequestData);
		long llUrno;

		if (strstr(prlRequestData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlRequestData->Selection);
		}
		else
		{
			llUrno = atol(prlRequestData->Selection);
		}
		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpRequest) == TRUE)
		{
			GetRecordFromItemList(prpRequest,
				prlRequestData->Fields,prlRequestData->Data);
		}
		else
		{
			prpRequest = new REQUESTDATA;
			GetRecordFromItemList(prpRequest,
				prlRequestData->Fields,prlRequestData->Data);

			AddRequestInternal(prpRequest);
		}
		ogCCSDdx.DataChanged((void *)this,REQUEST_CHANGE,(void *)prpRequest);
	}
}

CString CedaRequestData::GetTableName(void)
{
	return CString(pcmTableName);
}