// CedaValData.cpp - Class for check in counter classes
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaValData.h>
#include <BasicData.h>

static int ByStartTime(const BLKBLOCKEDTIMES **pppBlkTime1, const BLKBLOCKEDTIMES **pppBlkTime2);
static int ByStartTime(const BLKBLOCKEDTIMES **pppBlkTime1, const BLKBLOCKEDTIMES **pppBlkTime2)
{
	return (int)((**pppBlkTime1).Tifr.GetTime() - (**pppBlkTime2).Tito.GetTime());
}

void ProcessValCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaValData::CedaValData()
{                  
    BEGIN_CEDARECINFO(VALDATA, ValDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Urue,"URUE")
		FIELD_LONG(Uval,"UVAL")
		FIELD_CHAR_TRIM(Freq,"FREQ")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_CHAR_TRIM(Timf,"TIMF")
		FIELD_CHAR_TRIM(Timt,"TIMT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ValDataRecInfo)/sizeof(ValDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ValDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_VAL_INSERT, CString("VALDATA"), CString("Validity changed"),ProcessValCf);
	ogCCSDdx.Register((void *)this, BC_VAL_UPDATE, CString("VALDATA"), CString("Validity changed"),ProcessValCf);
	ogCCSDdx.Register((void *)this, BC_VAL_DELETE, CString("VALDATA"), CString("Validity deleted"),ProcessValCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"VALTAB");
    pcmFieldList = "URNO,URUE,UVAL,FREQ,VAFR,VATO,TIMF,TIMT";
}

void ProcessValCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaValData *)popInstance)->ProcessValBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaValData::ProcessValBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlValData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlValData);
	VALDATA rlVal, *prlVal = NULL;
	long llUrno = GetUrnoFromSelection(prlValData->Selection);
	GetRecordFromItemList(&rlVal,prlValData->Fields,prlValData->Data);
	if(llUrno == 0L) llUrno = rlVal.Urno;

	switch(ipDDXType)
	{
		case BC_VAL_INSERT:
		{
			if((prlVal = AddValInternal(rlVal)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, VAL_INSERT, (void *)prlVal);
			}
			break;
		}
		case BC_VAL_UPDATE:
		{
			if((prlVal = GetValByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlVal,prlValData->Fields,prlValData->Data);
				//PrepareDataAfterRead(prlVal);
				ogCCSDdx.DataChanged((void *)this, VAL_UPDATE, (void *)prlVal);
			}
			break;
		}
		case BC_VAL_DELETE:
		{
			if((prlVal = GetValByUrno(llUrno)) != NULL)
			{
				VALDATA olVal = *prlVal;
				DeleteValInternal(prlVal->Urno);
				ogCCSDdx.DataChanged((void *)this, VAL_DELETE, (void *) &olVal);
			}
			break;
		}
	}
}
 
CedaValData::~CedaValData()
{
	TRACE("CedaValData::~CedaValData called\n");
	ClearAll();
}

void CedaValData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos =  omUrueMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrueMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUrueMap.RemoveAll();
	omUvalMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaValData::ReadValData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
    sprintf(pclWhere,"WHERE APPL != 'RULE_AFT' AND APPL!='SERVICE'");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaValData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		VALDATA rlValData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlValData)) == RCSuccess)
			{
				AddValInternal(rlValData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaValData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(VALDATA), pclWhere);
    return ilRc;
}


VALDATA *CedaValData::AddValInternal(VALDATA &rrpVal)
{
	VALDATA *prlVal = new VALDATA;
	*prlVal = rrpVal;
	omData.Add(prlVal);
	CMapPtrToPtr *polSingleMap;
	if(omUrueMap.Lookup((void *) prlVal->Urue, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlVal->Urno,prlVal);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlVal->Urno,prlVal);
		omUrueMap.SetAt((void *)prlVal->Urue,polSingleMap);
	}

	omUvalMap.SetAt((void *)prlVal->Uval, prlVal);
	omUrnoMap.SetAt((void *)prlVal->Urno, prlVal);

	return prlVal;
}

void CedaValData::DeleteValInternal(long lpUrno)
{
	VALDATA *prlVal = NULL;
	int ilNumVals = omData.GetSize();
	for(int ilVal = (ilNumVals-1); ilVal >= 0; ilVal--)
	{
		prlVal = &omData[ilVal];
		if(prlVal->Urno == lpUrno)
		{
			omUvalMap.RemoveKey((void *)prlVal->Uval);
			omUrnoMap.RemoveKey((void *)prlVal->Uval);
			CMapPtrToPtr *polSingleMap;
			if(omUrueMap.Lookup((void *) prlVal->Urue, (void *&) polSingleMap))
			{
				polSingleMap->RemoveKey((void *)prlVal->Urno);
			}

			omData.DeleteAt(ilVal);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}


void CedaValData::GetValListByUrue(long lpUrue, CCSPtrArray <VALDATA> &ropValList, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropValList.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	VALDATA *prlVal;

	if(omUrueMap.Lookup((void *)lpUrue,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlVal);
			ropValList.Add(prlVal);
		}
	}
}

bool CedaValData::IsCurrentlyValid(long lpUrue, bool bpRecordNotFoundMeansValid /*true*/)
{
	bool blIsValid = false;

	CTime olCurrTime = ogBasicData.GetTime();
	CString olCurrHourStr = olCurrTime.Format("%H%M");
	int ilDayOfWeek = atoi(olCurrTime.Format("%w")) + 1;
	char pclDayOfWeek[10]; sprintf(pclDayOfWeek,"%d",ilDayOfWeek);

	CCSPtrArray <VALDATA> olValList;
	GetValListByUrue(lpUrue,olValList);
	int ilNumVals = olValList.GetSize();
	if(ilNumVals > 0)
	{
		for(int ilVal = 0; !blIsValid && ilVal < ilNumVals; ilVal++)
		{
			VALDATA *prlVal = &olValList[ilVal];
			if(olCurrTime >= prlVal->Vafr && olCurrTime <= prlVal->Vato && strchr(prlVal->Freq,*pclDayOfWeek) &&
				olCurrHourStr >= CString(prlVal->Timf) && olCurrHourStr <= CString(prlVal->Timt))
			{
				blIsValid = true;
			}
		}
	}
	else if(bpRecordNotFoundMeansValid)
	{
		blIsValid = true;
	}

	return blIsValid;
}


CString CedaValData::GetTableName(void)
{
	return CString(pcmTableName);
}

VALDATA *CedaValData::GetValByUval(long lpUval)
{
	VALDATA  *prlVal = NULL;
	omUvalMap.Lookup((void *)lpUval,(void *& )prlVal);
	return prlVal;
}

VALDATA *CedaValData::GetValByUrno(long lpUrno)
{
	VALDATA  *prlVal = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlVal);
	return prlVal;
}

int CedaValData::GetBlockedTimes(long lpUval, const CTime &ropStartTime, const CTime &ropEndTime, CCSPtrArray <BLKBLOCKEDTIMES> &ropBlockedTimes)
{
	VALDATA  *prlVal = GetValByUval(lpUval);
	if(prlVal != NULL)
	{
		if(prlVal->Vafr != TIMENULL && prlVal->Vafr > ropStartTime)
		{
			BLKBLOCKEDTIMES *prlBT = new BLKBLOCKEDTIMES;
			prlBT->Tifr = ropStartTime;
			prlBT->Tito = prlVal->Vafr;
			ropBlockedTimes.Add(prlBT);
		}
		if(prlVal->Vato != TIMENULL && prlVal->Vato < ropEndTime)
		{
			BLKBLOCKEDTIMES *prlBT = new BLKBLOCKEDTIMES;
			prlBT->Tifr = prlVal->Vato;
			prlBT->Tito = ropEndTime;
			ropBlockedTimes.Add(prlBT);
		}
	}
	return ropBlockedTimes.GetSize();
}

// return true if the object is completely blocked within the period opFrom/opTo
bool CedaValData::IsCompletelyBlocked(long lpUval, const CTime &ropStartTime, const CTime &ropEndTime)
{
	bool blIsCompletelyBlocked = false;

	CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
	GetBlockedTimes(lpUval, ropStartTime, ropEndTime, olBlockedTimes);
	olBlockedTimes.Sort(ByStartTime);
	int ilNumBlockedTimes = olBlockedTimes.GetSize();
	CTime olStart = TIMENULL, olEnd = TIMENULL;

	for(int ilBT = 0; ilBT < ilNumBlockedTimes; ilBT++)
	{
		BLKBLOCKEDTIMES *prlBt = &olBlockedTimes[ilBT];
		if(olStart == TIMENULL || olEnd == TIMENULL)
		{
			olStart = prlBt->Tifr;
			olEnd = prlBt->Tito;
		}
		else if(IsOverlapped(olStart, olEnd, prlBt->Tifr, prlBt->Tito))
		{
			if(prlBt->Tifr < olStart)
			{
				olStart = prlBt->Tifr;
			}
			if(prlBt->Tito > olEnd)
			{
				olEnd = prlBt->Tito;
			}
		}
	}

	if(IsWithIn(ropStartTime, ropEndTime, olStart, olEnd))
	{
		blIsCompletelyBlocked = true;
	}

	return blIsCompletelyBlocked;
}
