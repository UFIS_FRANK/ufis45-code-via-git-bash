// CedaBltData.h: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#ifndef _CEDABLTDATA_H_
#define _CEDABLTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

struct BltDataStruct
{
	long	Urno;		// Unique Record Number
	char	Bnam[6];	// Belt Name

	BltDataStruct(void)
	{
		Urno = 0L;
		strcpy(Bnam,"");
	}
};

typedef struct BltDataStruct BLTDATA;

class CedaBltData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <BLTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaBltData();
	~CedaBltData();

	CCSReturnCode ReadBltData();
	BLTDATA* GetBltByUrno(long lpUrno);
	void GetAllBltNames(CStringArray &ropBltNames);

private:
	void AddBltInternal(BLTDATA &rrpBlt);
	void ClearAll();
};


extern CedaBltData ogBltData;

#endif _CEDABLTDATA_H_
