// CedaLoaData.h - Loading Table - Contains suplimentary data about flights
// eg num passengers, baggage handling info
#ifndef _CedaLoaData_H_
#define _CedaLoaData_H_

#include "CCSCedaData.h"
#include "CCSPtrArray.h"


#define LOA_FIRST "F"
#define LOA_BUSINESS "B"
#define LOA_ECONOMY "E"

#define LOA_PASSENGERS "PAX"
#define LOA_SEATS "PMX"

#define LOA_BOOKED_LOCAL "PC"
#define LOA_BOOKED_TRANSFER "PT"
#define LOA_CHECKEDIN_LOCAL "PR"
#define LOA_CHECKEDIN_TRANSFER "PS"

//#define LOA_SSTP_NOTDEFINED "NON"
#define LOA_SSTP_NOTDEFINED ""

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct LoaDataStruct
{
	long	Urno;		// Unique Record Number
	char	Type[4];	// Record Type where "PAX" = passengers, "PMX" = Seats, "BAG" = Baggage
	char	Styp[4];	// Subtype eg "F" = First, "B" = Business, "Y" = Economy
	char	Sstp[4];	// Sub-subtype eg "PC" = Local PAX, "PT" = Transfer PAX
	long	Flnu;		// Urno of flight in AFTTAB
	char	Valu[7];	// Value
};

typedef struct LoaDataStruct LOADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaLoaData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <LOADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omFlnuMap; // key is AFT.URNO contains a submap with key "<TYPE><SSTP><STYP>"
	CString GetTableName(void);

// Operations
public:
	CedaLoaData();
	~CedaLoaData();

	bool ReadLoaData(CDWordArray &ropFlightUrnos);
	LOADATA* GetLoaByUrno(long lpUrno);
	int GetValu(long lpFlnu, const char *pcpType, const char *pcpStyp, const char *pcpSstp = LOA_SSTP_NOTDEFINED);
	int GetValu(CMapStringToPtr *popSubMap, const char *pcpType, const char *pcpStyp, const char *pcpSstp = LOA_SSTP_NOTDEFINED);
	void ProcessLoaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int GetBookedPassengers(long lpUaft, const char *pcpClass);
	int GetCheckedInPassengers(long lpUaft, const char *pcpClass);
	int GetNumSeats(long lpUaft, const char *pcpClass);
	bool IsOverBooked(long lpUaft, const char *pcpClass);
	CString GetPaxString(long lpUaft);

private:
	CMapStringToPtr *GetSubMap(long lpFlnu);
	void SendFlightChange(long lpUaft);
	CString MakeKey(const char *pcpType, const char *pcpStyp, const char *pcpSstp);
	void AddLoaInternal(LOADATA &rrpLoa);
	void DeleteLoaInternal(long lpUrno);
	void AddLoaToFlnuMap(LOADATA *prpLoa);
	void DeleteLoaFromFlnuMap(LOADATA *prpLoa);
	void ClearAll();
};


extern CedaLoaData ogLoaData;
#endif _CedaLoaData_H_
