// CPrintPreviewView.h : interface of the CPrintPreviewView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTPREVIEWVIEW_H__330F668C_7402_468E_9FE8_C933F6B27EFD__INCLUDED_)
#define AFX_PRINTPREVIEWVIEW_H__330F668C_7402_468E_9FE8_C933F6B27EFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ccsglobl.h>
#include <afxpriv.h>

struct PRINT_PREVIEW_PAGEDATA
{
	int imPageNo;
	int imStartLineNo;
	int imEndLineNo;
	int imGroupNo;
	CString omTeamName;

	PRINT_PREVIEW_PAGEDATA()
	{
		imPageNo = -1;
		imStartLineNo = -1;
		imEndLineNo = -1;
		imGroupNo = -1;
	}

	PRINT_PREVIEW_PAGEDATA& operator=(const PRINT_PREVIEW_PAGEDATA& rhs)
	{
		imPageNo = rhs.imPageNo;
		imStartLineNo = rhs.imStartLineNo;
		imEndLineNo = rhs.imEndLineNo;
		imGroupNo = rhs.imGroupNo;
		omTeamName = rhs.omTeamName;
		return *this;
	}
	
	PRINT_PREVIEW_PAGEDATA(const PRINT_PREVIEW_PAGEDATA& rhs)
	{
		imPageNo = rhs.imPageNo;
		imStartLineNo = rhs.imStartLineNo;
		imEndLineNo = rhs.imEndLineNo;
		imGroupNo = rhs.imGroupNo;
		omTeamName = rhs.omTeamName;
	}
};

struct PRINT_PREVIEW_DCINFO
{
	CDC* pomDC;
	CPrintInfo* pomPrintInfo;
	
	PRINT_PREVIEW_DCINFO()
	{
		pomDC = NULL;
		pomPrintInfo = NULL;
	}

	PRINT_PREVIEW_DCINFO& operator=(const PRINT_PREVIEW_DCINFO& rhs)
	{
		pomDC = rhs.pomDC;
		pomPrintInfo = rhs.pomPrintInfo;
		return *this;
	}
	
	PRINT_PREVIEW_DCINFO(const PRINT_PREVIEW_DCINFO& rhs)
	{
		pomDC = rhs.pomDC;
		pomPrintInfo = rhs.pomPrintInfo;
	}
};

class PrePlanTable;

class CPrintPreviewView : public CScrollView
{
public: // create from serialization only
	CPrintPreviewView();
	DECLARE_DYNCREATE(CPrintPreviewView)

public:
	CWnd* pomCallMsgWindow;
	CFrameWnd* pomOldFrame;
	int				m_nLogPixelsX;
	int				m_nLogPixelsY;

	BOOL			m_bPrint;
	CRect			rcPrint;

	// Scale current page size.
	BOOL ScaleToDeviceRes(
		// Current device dc.
		CDC* pDC, 
		// Size of new scale.
		CSize& szNew
		);

	void SetFrameWindow(CFrameWnd* ropFrameWnd){pomFrameWnd = ropFrameWnd;}
	CFrameWnd* GetFrameWindow(){return pomFrameWnd;}

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintPreviewView)
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view	
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
    virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);			
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual void OnFilePrintPreview();
	virtual ~CPrintPreviewView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CFrameWnd* pomFrameWnd;
// Generated message map functions
protected:
	//{{AFX_MSG(CPrintPreviewView)
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CPrintPreviewView.cpp
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTPREVIEWVIEW_H__330F668C_7402_468E_9FE8_C933F6B27EFD__INCLUDED_)
