// StartDate.h : header file
//
 
#ifndef _DSTARTD_H_ 
#define _DSTARTD_H_ 
 
 
/////////////////////////////////////////////////////////////////////////////
// StartDate dialog

class StartDate : public CDialog
{
// Construction
public:
    StartDate(CWnd* pParent = NULL);    // standard constructor

// Dialog Data
    //{{AFX_DATA(StartDate)
    enum { IDD = IDD_STARTDATE };
    CString m_DDMM;
    CString m_YYYY;
    //}}AFX_DATA

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(StartDate)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

#endif // _DSTARTD_H_

