// Class for Gates
#ifndef _CEDAGATDATA_H_
#define _CEDAGATDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct GatDataStruct
{
	long	Urno;		// Unique Record Number
	char	Gnam[6];	// Gate Name
	CTime	Vafr;		// Valid from
	CTime	Vato;		// Valid to
	CTime	Nafr;		// Not available from
	CTime	Nato;		// Not available to
	char	Rga1[6];	// Verknupftes Gate 1
	char	Rga2[6];	// Verknupftes Gate 2
	char	Tele[11];	// dog and bone
	char    Term[2];    // Terminal (Singapore)

	GatDataStruct(void)
	{
		Urno = 0L;
		strcpy(Gnam,"");
		Vafr = TIMENULL;
		Vato = TIMENULL;
		Nafr = TIMENULL;
		Nato = TIMENULL;
		strcpy(Rga1,"");
		strcpy(Rga2,"");
		strcpy(Tele,"");
		strcpy(Term,""); //Singapore
	}
};

typedef struct GatDataStruct GATDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaGatData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <GATDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CString GetTableName(void);

// Operations
public:
	CedaGatData();
	~CedaGatData();

	CCSReturnCode ReadGatData();
	GATDATA* GetGatByUrno(long lpUrno);
	GATDATA* GetGatByName(const char *pcpName);
	long GetGatUrnoByName(const char *pcpName);
	void GetAllGateNames(CStringArray &ropGateNames);
	void GetAllValidGateNames(CStringArray &ropGateNames);

private:
	void AddGatInternal(GATDATA &rrpGat);
	void ClearAll();
};


extern CedaGatData ogGatData;
#endif _CEDAGATDATA_H_
