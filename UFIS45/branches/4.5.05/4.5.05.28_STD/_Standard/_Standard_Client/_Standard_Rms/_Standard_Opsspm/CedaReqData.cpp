//
// CedaReqData.cpp - demand<->function list
//
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaReqData.h>
#include <BasicData.h>
#include <CedaRudData.h>

void ProcessReqCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaReqData::CedaReqData()
{                  
    BEGIN_CEDARECINFO(REQDATA, ReqDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Urud,"URUD")
		FIELD_CHAR_TRIM(Eqco,"EQCO")
		FIELD_LONG(Uequ,"UEQU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ReqDataRecInfo)/sizeof(ReqDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ReqDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_REQ_INSERT, CString("REQDATA"), CString("Req changed"),ProcessReqCf);
	ogCCSDdx.Register((void *)this, BC_REQ_UPDATE, CString("REQDATA"), CString("Req changed"),ProcessReqCf);
	ogCCSDdx.Register((void *)this, BC_REQ_DELETE, CString("REQDATA"), CString("Req deleted"),ProcessReqCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"REQTAB");
    pcmFieldList = "URNO,URUD,EQCO,UEQU";
}
 
CedaReqData::~CedaReqData()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void CedaReqData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omUrudMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaReqData::ReadReqData(CDWordArray &ropTplUrnos)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[5000] = "";

	if ( ogRudData.bmUtplExists )
	{
		CString olTplUrnos, olTmp;
		int ilNumTplUrnos = ropTplUrnos.GetSize();
		for(int ilTpl = 0; ilTpl < ilNumTplUrnos; ilTpl++)
		{
			if(ilTpl != 0)
				olTplUrnos += ",";
			olTmp.Format("'%d'", ropTplUrnos[ilTpl]);
			olTplUrnos += olTmp;
		}
		sprintf(pclWhere, "WHERE URUD in (select urno from rudtab where UTPL IN (%s))", olTplUrnos);
	}
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaReqData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		REQDATA rlReqData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlReqData)) == RCSuccess)
			{
				AddReqInternal(rlReqData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaReqData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(REQDATA), pclWhere);
    return ilRc;
}


REQDATA *CedaReqData::AddReqInternal(REQDATA &rrpReq)
{
	REQDATA *prlReq = new REQDATA;
	*prlReq = rrpReq;
	omData.Add(prlReq);
	omUrudMap.SetAt((void *)prlReq->Urud,prlReq);
	omUrnoMap.SetAt((void *)prlReq->Urno,prlReq);
	return prlReq;
}

void CedaReqData::DeleteReqInternal(long lpUrno)
{
	int ilNumReqs = omData.GetSize();
	for(int ilReq = (ilNumReqs-1); ilReq >= 0; ilReq--)
	{
		if(omData[ilReq].Urno == lpUrno)
		{
			omUrudMap.RemoveKey((void *)omData[ilReq].Urud);
			omData.DeleteAt(ilReq);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

REQDATA *CedaReqData::GetReqByUrno(long lpUrno)
{
	REQDATA *prlReq = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlReq);
	return prlReq;
}

REQDATA *CedaReqData::GetReqByUrud(long lpUrud)
{
	REQDATA *prlReq = NULL;
	omUrudMap.Lookup((void *)lpUrud, (void *&) prlReq);
	return prlReq;
}

CString CedaReqData::GetTableName(void)
{
	return CString(pcmTableName);
}

void ProcessReqCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaReqData *)popInstance)->ProcessReqBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaReqData::ProcessReqBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlReqData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlReqData);
	REQDATA rlReq, *prlReq = NULL;
	long llUrno = GetUrnoFromSelection(prlReqData->Selection);
	GetRecordFromItemList(&rlReq,prlReqData->Fields,prlReqData->Data);
	if(llUrno == 0L) llUrno = rlReq.Urno;

	switch(ipDDXType)
	{
		case BC_REQ_INSERT:
		{
			if((prlReq = AddReqInternal(rlReq)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, REQ_INSERT, (void *)prlReq);
			}
			break;
		}
		case BC_REQ_UPDATE:
		{
			if((prlReq = GetReqByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlReq,prlReqData->Fields,prlReqData->Data);
				//PrepareDataAfterRead(prlReq);
				ogCCSDdx.DataChanged((void *)this, REQ_UPDATE, (void *)prlReq);
			}
			break;
		}
		case BC_REQ_DELETE:
		{
			if((prlReq = GetReqByUrno(llUrno)) != NULL)
			{
				DeleteReqInternal(prlReq->Urno);
				ogCCSDdx.DataChanged((void *)this, REQ_DELETE, (void *)llUrno);
			}
			break;
		}
	}
}
