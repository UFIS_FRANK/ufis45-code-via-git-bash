#ifndef _CREQD_H_
#define _CREQD_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: REQUESTDATA
/*@Doc:

  \begin{verbatim}
    SELECT  stid,vafr,vato,days,shfr,shto,
    FROM    shfcki
  \end{verbatim}
*/
struct RequestDataStruct
{
	long  Urno;           // Unique record number
	char  Stat[2];        // Status I=Info
	CTime Rdat;			  // Request Date
	char  Cusr[10];       // Userid of Creator (PKNO)
	CTime Adat;			  // Date of Alteration
	CTime Rqda;			  // Date concerning the request
	char  Peno[8];        // Peno of user concerning the request
	char  Text[1026];

	// date used internal
	BOOL IsChanged;

	RequestDataStruct(void) 
	{ memset(this,'\0',sizeof(*this));Rdat=TIMENULL;Adat=TIMENULL;
	Rqda=TIMENULL;IsChanged=DATA_UNCHANGED;}

 };

typedef struct RequestDataStruct REQUESTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Class for handling peak data
//@See: CCSCedaData, CedaJobData, PrePlanTable
/*@Doc:
  Reads and writes shift data from/to database and stores it in memory. 
  The shift data class will be used in {\bf ShiftTable} table and the 
  {\bf PrePlanTable}.

*/
class CedaRequestData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: REQUESTDATA records read by ReadShiftData()
    CCSPtrArray<REQUESTDATA> omData;
    CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

    //@ManMemo: A map, containing the PKNO fields of all loaded employees.
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf SHIFTDATA},
      the {\bf pcmTableName} with table name {\bf STFCKI}, the data members {\bf omFieldList}
      contains a list of used fields of database view {\bf STFCKI}.
    */
   // CedaRequestData();
	CedaRequestData(CString opTableType);
    //@ManMemo: Destructor, Unregisters the CedaShiftData object from DataDistribution
	~CedaRequestData();

    //@ManMemo: Read all shift types from database at program start
	CCSReturnCode ReadRequestData();
	CCSReturnCode AddRequest(REQUESTDATA * prpRequest);
	CCSReturnCode AddRequestInternal(REQUESTDATA * prpRequest);
	CCSReturnCode SaveRequest(REQUESTDATA * prpRequest);
	CCSReturnCode SaveAllRequests(void);
	CCSReturnCode DeleteRequest(long lpUrno);
	REQUESTDATA  *GetRequestByUrno(long lpUrno);

	void Add(REQUESTDATA& rrlRequestData);
	void ProcessRequestBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

public:
	int imRequestCount;
	int imInfoCount;
	CString omTableType;


private:

};


extern CedaRequestData ogRequests;
extern CedaRequestData ogInfos;

#endif

