// ccitblvw.cpp -- the viewer for PRM Table
//
// Written by:
// somebody which I assumed that it should be Manfred at the middle period of
// the project.
//
// Modification History:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	Tries to revise (especially on the DDX processing) this viewer for
//	drag-and-drop functionality (If the viewer does not process DDX callback,
//	dropping a pool job to PRM Table will always begin at the same time.

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>

#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>

#include <PrmTableViewer.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCicData.h>
#include <fstream.h>
#include <iomanip.h>
#include <CedaAltData.h>
#include <CedaSerData.h>
#include <CedaRudData.h>
#include <ccstable.h>
#include <DlgSettings.h> //Singapore 

class PRMTable; //Singapore


static int ByName(const ALLOCUNIT **ppp1, const ALLOCUNIT **ppp2)
{
	return (int)(strcmp((*ppp1)->Name,(*ppp2)->Name));
}

static int ByName(const CICDATA **ppp1, const CICDATA **ppp2)
{
	return (int)(strcmp((*ppp1)->Cnam,(*ppp2)->Cnam));
}



// Sorting definition:
enum {
    BY_TYPE,
	BY_TIME,
    BY_NAME,
	BY_FLNO,
	BY_FLDA,
	BY_FLDD,
	BY_ALC3,
	BY_FRTO,
	BY_FUNC1,
	BY_NONE
};

static struct DPXDATA_FIELD osfields[]	= 
	{
		{	DPXDATA_FIELD("PRID","",	76,		228)	},          
		{	DPXDATA_FIELD("NAME","",	152,	456)	},    
		{	DPXDATA_FIELD("SEAT","",	53,		159)	},        
		{	DPXDATA_FIELD("PRMT","",	53,		125)	},        
		{	DPXDATA_FIELD("GEND","",	15,		45)	},        
		{	DPXDATA_FIELD("SATI","",	92,		276)	},    
		{	DPXDATA_FIELD("CLOS","",	92,		276)	},    
		{	DPXDATA_FIELD("ETAI","",	53,		159)	},      
		{	DPXDATA_FIELD("ETDI","",	53,		159)	},        
		{	DPXDATA_FIELD("ADID","",	15,		45)	},        
		{	DPXDATA_FIELD("FLNUFLNO","",53,		159)	},    
		{	DPXDATA_FIELD("TFLUFLNO","",53,		159)	},    
		{	DPXDATA_FIELD("LANG","",	22,		66)	},      
		{	DPXDATA_FIELD("NATL","",	22,		66)	},        
		{	DPXDATA_FIELD("HASJOB","",	22,		66)	},      
		{	DPXDATA_FIELD("ABSR","",	31,		100)	},        
		{	DPXDATA_FIELD("ABDT","",	92,		276)	},    
		{	DPXDATA_FIELD("ALOC","",	152,	456)	},        
		{	DPXDATA_FIELD("STAF1","",	152,	456)	},    
		{	DPXDATA_FIELD("FRTO1","",	92,		276)	},        
		{	DPXDATA_FIELD("FUNC1","",	46,		138)		},      
		{	DPXDATA_FIELD("SERV1","",	69,		207)		},      
		{	DPXDATA_FIELD("STAF2","",	152,	456)	},      
		{	DPXDATA_FIELD("FRTO2","",	92,		276)	},        
		{	DPXDATA_FIELD("FUNC2","",	46,		138)		},      
		{	DPXDATA_FIELD("SERV2","",	69,		207)		},      
		{	DPXDATA_FIELD("STAF3","",	152,	456)	},      
		{	DPXDATA_FIELD("FRTO3","",	92,		276)	},        
		{	DPXDATA_FIELD("FUNC3","",	46,		138)		},      
		{	DPXDATA_FIELD("SERV3","",	69,		207)		},      
		{	DPXDATA_FIELD("STAF4","",	152,	456)	},      
		{	DPXDATA_FIELD("FRTO4","",	92,		276)	},        
		{	DPXDATA_FIELD("FUNC4","",	46,		138)		},      
		{	DPXDATA_FIELD("SERV4","",	69,		207)		},      
		{	DPXDATA_FIELD("REMA","",	304,	912)	},    
		{	DPXDATA_FIELD("FLNUPRMS","",22,		66)	},      
		{	DPXDATA_FIELD("TFLUPRMS","",22,		66)	},      
		{	DPXDATA_FIELD("MAIL","",	92,		276)	},      
	};


/////////////////////////////////////////////////////////////////////////////
// PrmTableViewer

PrmTableViewer::PrmTableViewer()
{
    SetViewerKey("PrmTab");
    pomTable = NULL;
	imMaxJobs = 4;
    ogCCSDdx.Register(this, DPX_NEW,	CString("PRMTBLVW"), CString("DPX New"),	PrmTableCf);
    ogCCSDdx.Register(this, DPX_CHANGE, CString("PRMTBLVW"), CString("DPX Change"), PrmTableCf);
    ogCCSDdx.Register(this, DPX_DELETE, CString("PRMTBLVW"), CString("DPX Delete"), PrmTableCf);
    ogCCSDdx.Register(this, DPX_ALLOC, CString("PRMTBLVW"), CString("DPX Alloc"), PrmTableCf);
    ogCCSDdx.Register(this, JOB_NEW,	CString("PRMTBLVW"), CString("Job New"),	PrmTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("PRMTBLVW"), CString("Job Change"), PrmTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("PRMTBLVW"), CString("Job Delete"), PrmTableCf);
    ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("PRMTBLVW"), CString("Flight Change"), PrmTableCf);


	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime	= CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);

	// lli: load PRM-FILTERTRANSITFLIGHT setting from ceda.ini, default is don't check transit flight date
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	bmFilterTransitFlight = false;
	GetPrivateProfileString(pcgAppName, "PRM-FILTERTRANSITFLIGHT",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		bmFilterTransitFlight = true;
	}
	else {
 		bmFilterTransitFlight = false;
	}

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = CString("ID");
		osfields[1].Name = CString("Name");
		osfields[2].Name = CString("Seat");
		osfields[3].Name = CString("Type");
		osfields[4].Name = CString("G");
		osfields[5].Name = CString("Flt.Date");
		osfields[6].Name = CString("Closed");
		osfields[7].Name = CString("STA");
		osfields[8].Name = CString("STD");
		osfields[9].Name = CString("ADT");
		osfields[10].Name = CString("Flight");
		osfields[11].Name= CString("Transit");
		osfields[12].Name= CString("Lang");
		osfields[13].Name= CString("Nat.");
		osfields[14].Name= CString("Jobs");
		osfields[15].Name= CString("Channel");
		osfields[16].Name= CString("BookTime");
		osfields[17].Name= CString("Location");
		osfields[18].Name= CString("Assignment1");
		osfields[19].Name= CString("FromTo1");
		osfields[20].Name= CString("Func1");
		osfields[21].Name= CString("Services1");
		osfields[22].Name= CString("Assignment2");
		osfields[23].Name= CString("FromTo2");
		osfields[24].Name= CString("Func2");
		osfields[25].Name= CString("Services2");
		osfields[26].Name= CString("Assignment3");
		osfields[27].Name= CString("FromTo3");
		osfields[28].Name= CString("Func3");
		osfields[29].Name= CString("Services3");
		osfields[30].Name= CString("Assignment4");
		osfields[31].Name= CString("FromTo4");
		osfields[32].Name= CString("Func4");
		osfields[33].Name= CString("Services4");
		osfields[34].Name= CString("Remarks");
		osfields[35].Name= CString("FF");
		osfields[36].Name= CString("FFT");
		osfields[37].Name= CString("Email");
	}
}

PrmTableViewer::~PrmTableViewer()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
	omTableFields.DeleteAll();
	omColumns.DeleteAll();
	DeleteAll();
}

void PrmTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void PrmTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ogCfgData.rmUserSetup.PRMV = pcpViewName;
	ChangeViewTo(pcpViewName);
}

void PrmTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.PRMV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter(); 

    DeleteAll();

	EvaluateTableFields();
	MakeColumns();
    MakeLines();
	UpdateDisplay(true);
}

BOOL PrmTableViewer::IsPassCurrentFilter(DPXDATA *rpDpx) //igu
{
	return IsPassFilter(rpDpx);
}

void PrmTableViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, n;

	GetFilter("Type", olFilterValues);
	omCMapForType.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForType[olFilterValues[i]] = NULL;
			// we can use NULL here since what the map mapped to is unimportant

	bmUseAllAlcs = false;
  GetFilter("Airline", olFilterValues);
  omCMapForAlcd.RemoveAll();
  n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAlcs = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAlcd[olFilterValues[i]] = NULL;
	}

    GetFilter("Service", olFilterValues);
    omCMapForServices.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllServices = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllServices = true;
		else
			for (i = 0; i < n; i++)
				omCMapForServices[olFilterValues[i]] = NULL;
	}

    GetFilter("Prmt", olFilterValues);
    omCMapForPrmts.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPrmts = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPrmts = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPrmts[olFilterValues[i]] = NULL;
	}

	//added by MAX
	GetFilter("PrmAL", olFilterValues);
    omCMapForPrmLAs.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPrmLAs = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPrmLAs = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPrmLAs[olFilterValues[i]] = NULL;
	}

    GetFilter("Adid", olFilterValues);
    omCMapForAdids.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllAdids = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAdids = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAdids[olFilterValues[i]] = NULL;
	}

	//Terminal
	GetFilter("Terminal", olFilterValues);
    omCMapForTerms.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllTerms = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllTerms = true;
		else
			for (i = 0; i < n; i++)
				omCMapForTerms[olFilterValues[i]] = NULL;
	}
	bmEmptyTermSelected = CheckForEmptyValue(omCMapForTerms, GetString(IDS_STRINGWTFLIGHTS));


}

void PrmTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Name")
            ilSortOrderEnumeratedValue = BY_NAME;
        else if (olSortOrder[i] == "Type")
			ilSortOrderEnumeratedValue = BY_TYPE;
       else if (olSortOrder[i] == "Time")
			ilSortOrderEnumeratedValue = BY_TIME;
      else if (olSortOrder[i] == "Flight")
			ilSortOrderEnumeratedValue = BY_FLNO;
      else if (olSortOrder[i] == "STA")
			ilSortOrderEnumeratedValue = BY_FLDA;
	  else if (olSortOrder[i] == "STD")
			ilSortOrderEnumeratedValue = BY_FLDD;
      else if (olSortOrder[i] == "Airline")
			ilSortOrderEnumeratedValue = BY_ALC3;
      else if (olSortOrder[i] == "FromTo")
			ilSortOrderEnumeratedValue = BY_FRTO;
	  else if (olSortOrder[i] == "Func1")
			ilSortOrderEnumeratedValue = BY_FUNC1;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int PrmTableViewer::ComparePrm(PRMDATA_LINE *prpPrm1, PRMDATA_LINE *prpPrm2)
{
    int n = omSortOrder.GetSize();
	FLIGHTDATA *prlFlight1 = ogFlightData.GetFlightByUrno(prpPrm1->dpx->Flnu);
	FLIGHTDATA *prlFlight2 = ogFlightData.GetFlightByUrno(prpPrm2->dpx->Flnu);
	CTime olTime1 = TIMENULL;
	CTime olTime2 = TIMENULL;
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
		{
        case BY_TYPE:
            ilCompareResult = strcmp(prpPrm1->dpx->Prmt,prpPrm2->dpx->Prmt);
            break;
        case BY_NAME:
            ilCompareResult = strcmp(prpPrm1->dpx->Name,prpPrm2->dpx->Name);
            break;
        case BY_TIME:
			if (prpPrm1->dpx->Abdt == prpPrm2->dpx->Abdt)
				ilCompareResult = 0;
			else
            ilCompareResult = (prpPrm1->dpx->Abdt > prpPrm2->dpx->Abdt)? 1: -1;
			break;
        case BY_FLNO:
            ilCompareResult = strcmp(prpPrm1->dpx->FlnuFlno,prpPrm2->dpx->FlnuFlno);
            break;
        case BY_FLDA:
			/*
			if(prlFlight1 != NULL)
			{
				if (*prlFlight1->Adid == 'A' || prpPrm1->dpx->Tflu>0) 
				{		
					if(prpPrm1->dpx->Tflu>0)
					{
						prlFlight1 = ogFlightData.GetFlightByUrno(prpPrm1->dpx->Tflu);
					}
					olTime1 = (prlFlight1->Etod != TIMENULL) ? prlFlight1->Etod : prlFlight1->Stod;
				}
				else
				{
					ilCompareResult = 1; //To skip the arrival checking
					break;
				}
			}
			if(prlFlight2 != NULL)	
			{
				if (*prlFlight2->Adid == 'A' || prpPrm2->dpx->Tflu>0) 
				{
					if(prpPrm2->dpx->Tflu>0)
					{
						prlFlight2 = ogFlightData.GetFlightByUrno(prpPrm2->dpx->Tflu);
					}
					olTime2 = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
				}
				else
				{
					ilCompareResult = -1; //To skip the arrival checking
					break;
				}
			}
		    ilCompareResult = (olTime1 == olTime2)?0:((olTime1 > olTime2)? 1: -1);
			break;
			*/
			if(prlFlight1 != NULL)
			{
				if (*prlFlight1->Adid == 'A') 
				{
					olTime1 = (prlFlight1->Etoa != TIMENULL) ? prlFlight1->Etoa : prlFlight1->Stoa;
				}
				else
				{
					ilCompareResult = 1; //To skip the arrival checking
					break;
				}
			}
			if(prlFlight2 != NULL)
			{
				if (*prlFlight2->Adid == 'A') 
				{
					olTime2 = (prlFlight2->Etoa != TIMENULL) ? prlFlight2->Etoa : prlFlight2->Stoa;
				}
				else
				{
					ilCompareResult = -1; //To skip the arrival checking
					break;
				}
			}
            ilCompareResult = (olTime1 == olTime2)?0:((olTime1 > olTime2)? 1: -1);
			break;

		case BY_FLDD:
			/*
			if(prlFlight1 != NULL)
			{
				if (*prlFlight1->Adid == 'D' || prpPrm1->dpx->Tflu>0) 
				{		
					if(prpPrm1->dpx->Tflu>0)
					{
						prlFlight1 = ogFlightData.GetFlightByUrno(prpPrm1->dpx->Tflu);
					}
					olTime1 = (prlFlight1->Etod != TIMENULL) ? prlFlight1->Etod : prlFlight1->Stod;
				}
				else
				{
					ilCompareResult = 1; //To skip the arrival checking
					break;
				}
			}
			if(prlFlight2 != NULL)
			{
				if (*prlFlight2->Adid == 'D' || prpPrm2->dpx->Tflu>0) 
				{
					if(prpPrm2->dpx->Tflu>0)
					{
						prlFlight2 = ogFlightData.GetFlightByUrno(prpPrm2->dpx->Tflu);
					}
					olTime2 = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
				}
				else
				{
					ilCompareResult = -1; //To skip the arrival checking
					break;
				}
			}			
            ilCompareResult = (olTime1 == olTime2)?0:((olTime1 > olTime2)? 1: -1);
			break;
			*/
			if(prlFlight1 != NULL)
			{
				if (*prlFlight1->Adid == 'D') 
				{		
					olTime1 = (prlFlight1->Etod != TIMENULL) ? prlFlight1->Etod : prlFlight1->Stod;
				}
				else
				{
					if(prpPrm1->dpx->Tflu>0)
					{
						prlFlight1 = ogFlightData.GetFlightByUrno(prpPrm1->dpx->Tflu);
						olTime1 = (prlFlight1->Etod != TIMENULL) ? prlFlight1->Etod : prlFlight1->Stod;
					}
					else
					{
						ilCompareResult = 1; //To skip the arrival checking
						break;
					}
				}
			}
			if(prlFlight2 != NULL)
			{
				if (*prlFlight2->Adid == 'D') 
				{
					olTime2 = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
				}
				else
				{
					if(prpPrm2->dpx->Tflu>0)
					{
						prlFlight2 = ogFlightData.GetFlightByUrno(prpPrm2->dpx->Tflu);
						olTime2 = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
					}
					else
					{
						ilCompareResult = -1; //To skip the arrival checking
						break;
					}
				}
			}			
            ilCompareResult = (olTime1 == olTime2)?0:((olTime1 > olTime2)? 1: -1);
			break;
        case BY_ALC3:
            ilCompareResult = strcmp(prpPrm1->dpx->FlnuAlc, prpPrm2->dpx->FlnuAlc);
			break;
        case BY_FRTO:
			if (prpPrm1->FromTos.GetSize()==0&&prpPrm2->FromTos.GetSize() == 0)
			{
				ilCompareResult = 0;
			}
			else if (prpPrm1->FromTos.GetSize()==0)
			{
				ilCompareResult = -1; 
			}
			else if (prpPrm2->FromTos.GetSize() == 0)
			{
				ilCompareResult = 1;
			}
			else
			{
				ilCompareResult = strcmp(prpPrm1->FromTos[0], prpPrm2->FromTos[0]);
			}
			break;
		case BY_FUNC1:
			if (prpPrm1->Functions.GetSize()==0&&prpPrm2->Functions.GetSize() == 0)
			{
				ilCompareResult = 0;
			}
			else if (prpPrm1->Functions.GetSize()==0)
			{
				ilCompareResult = -1; 
			}
			else if (prpPrm2->Functions.GetSize() == 0)
			{
				ilCompareResult = 1;
			}
			else
			{
				ilCompareResult = strcmp(prpPrm1->Functions[0], prpPrm2->Functions[0]);
			}
		   break;
		   break;
        }

    if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   
}

CString PrmTableViewer::GetServicesForDpx(long lpUrno)
{
	CMapStringToPtr olCMapForService;
	CString olServices;
	void* p;
	CCSPtrArray<DEMANDDATA> olDemands;
	ogDemandData.GetDemandsByUprm(olDemands,lpUrno,true);
	for (int ilC = 0; ilC < olDemands.GetSize();ilC++)
	{
		DEMANDDATA *prlDem = &olDemands[ilC];
		if (!prlDem)
			continue;

		RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
		if (!prlRud)
			continue;

		SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
		if (!prlSer)
			continue;
		
		if (olCMapForService.Lookup(CString(prlSer->Seco), p))
		{
			continue;
		}
		else
		{
			olCMapForService.SetAt(CString(prlSer->Seco), (void *) NULL);
		}

	}

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByUprm(olJobs, lpUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob<ilNumJobs; ilJob++)
	{
		SERDATA* prlSer = ogSerData.GetSerByUrno(olJobs[ilJob].Ughs);
		if (!prlSer)
			continue;
		
		if (olCMapForService.Lookup(CString(prlSer->Seco), p))
		{
			continue;
		}
		else
		{
			olCMapForService.SetAt(CString(prlSer->Seco), (void *) NULL);
		}
	}	
	void *pvlDummy;
	CString olService;
	for(POSITION rlPos = olCMapForService.GetStartPosition(); rlPos != NULL; )
	{
		olCMapForService.GetNextAssoc(rlPos, olService, (void *&)pvlDummy);
		if(!olServices.IsEmpty())
			olServices += CString(",");
		olServices += olService;
	}

	return olServices;
}

BOOL PrmTableViewer::IsPassFilter(DPXDATA *rpDpx)
{
	void *p;
	BOOL blIsTypeOk = omCMapForType.Lookup(CString(rpDpx->Prmt), p);
	//BOOL blIsAlocOk		= omCMapForAloc.Lookup(CString(pcpHall), p);

	BOOL blIsOK = FALSE;
	if (rpDpx->Sati >= omStartTime && rpDpx->Sati <= omEndTime)
	{
		blIsOK = TRUE;
	}
	if (blIsOK == FALSE)
	{
		if (rpDpx->Fdat >= omStartTime && rpDpx->Fdat <= omEndTime)
		{
			blIsOK = TRUE;
		}
		else if (bmFilterTransitFlight)
		{
			if(rpDpx->Tflu!=0L)
			{
				FLIGHTDATA *prlFlight2;
				prlFlight2 = ogFlightData.GetFlightByUrno(rpDpx->Tflu);
				if(prlFlight2 != NULL)
				{
					if(prlFlight2->Tifd >= omStartTime && prlFlight2->Tifd <= omEndTime)
					{
						blIsOK = TRUE;
					}
				}
			}
		}
	}

	BOOL blIsAlcdOk = FALSE;
	if (bmUseAllAlcs)
	{
		blIsAlcdOk = TRUE;
	}
	else
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(rpDpx->Flnu);
		if(prlFlight == NULL)
			return FALSE;
		CString olAlc = ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3);
		if (omCMapForAlcd.Lookup(CString(olAlc), p))
		{
			blIsAlcdOk = TRUE;
		}
		else
		{
			if(rpDpx->Tflu==0L)
				return FALSE;
			prlFlight = ogFlightData.GetFlightByUrno(rpDpx->Tflu);
			if(prlFlight == NULL)
				return FALSE;
			olAlc = ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3);
			blIsAlcdOk = omCMapForAlcd.Lookup(CString(olAlc), p);
		}
	}

    BOOL blIsServiceOk = bmUseAllServices;
	CStringArray olServices;
	CString olService = GetServicesForDpx(rpDpx->Urno);
	ogBasicData.ExtractItemList(olService,&olServices,',');
	int ilNumServices = olServices.GetSize();
	for (int ilService = 0; !blIsServiceOk && ilService < ilNumServices;ilService++)
	{
		blIsServiceOk = omCMapForServices.Lookup(olServices[ilService], p);
	}

    BOOL blIsPrmtOk = bmUseAllPrmts;
	CStringArray olPrmts;
	CString olPrmt = rpDpx->Prmt;
	ogBasicData.ExtractItemList(olPrmt,&olPrmts,',');
	int ilNumPrmts = olPrmts.GetSize();
	for (int ilPrmt = 0; !blIsPrmtOk && ilPrmt < ilNumPrmts;ilPrmt++)
	{
		blIsPrmtOk = omCMapForPrmts.Lookup(olPrmts[ilPrmt], p);
	}

	//added by MAX
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	BOOL blIsPrmLAOk = true;
	GetPrivateProfileString(pcgAppName, "PRM-PAXLOCFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		blIsPrmLAOk = bmUseAllPrmLAs;
		CStringArray olPrmLAs;
		CString olPrmLA = rpDpx->Aloc;
		ogBasicData.ExtractItemList(olPrmLA,&olPrmLAs,',');
		int ilNumPrmLAs = olPrmLAs.GetSize();
		for (int ilPrmLA = 0; !blIsPrmLAOk && ilPrmLA < ilNumPrmLAs;ilPrmLA++)
		{
			blIsPrmLAOk = omCMapForPrmLAs.Lookup(olPrmLAs[ilPrmLA], p);
		}
	}


    BOOL blIsAdidOk = bmUseAllAdids;
	CString olAdid = CString(rpDpx->FlnuAdid);
	if (strlen(rpDpx->TfluFlno)>0 && rpDpx->TfluAdid=='D') 
		olAdid = CString("T");
//	if (= GetAdidForDpx(rpDpx->Urno);
	if (!bmUseAllAdids)
		blIsAdidOk = omCMapForAdids.Lookup(olAdid, p);


	//Terminal
	
	BOOL blIsTermOk = FALSE;
	if (bmUseAllTerms || !IsPrivateProfileOn("PRM_TERMINALFILTER","NO"))
	{
		blIsTermOk = TRUE;
	}
	else
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(rpDpx->Flnu);
		if(prlFlight == NULL)
			return FALSE;
		CString olTerm = prlFlight->Adid== CString("A")? prlFlight->Tga1 : prlFlight->Tgd1;
		if (omCMapForTerms.Lookup(CString(olTerm), p))
		{
			blIsTermOk = TRUE;
		}
		else
		{
			//if(rpDpx->Tflu==0L)   
			//	return FALSE;

			if(rpDpx->Tflu!=0L)   
			prlFlight = ogFlightData.GetFlightByUrno(rpDpx->Tflu);
			if(prlFlight == NULL)
				return FALSE;
			olTerm = prlFlight->Adid== CString("A")? prlFlight->Tga1 : prlFlight->Tgd1;
			blIsTermOk = omCMapForTerms.Lookup(CString(olTerm), p);
		}
	}
	blIsTermOk = (blIsTermOk || bmEmptyTermSelected);
	
	return (blIsOK&&blIsAlcdOk&&blIsServiceOk&&blIsPrmtOk&&blIsAdidOk&&blIsTermOk&&blIsPrmLAOk);
}


/////////////////////////////////////////////////////////////////////////////
// PrmTableViewer -- code specific to this class

void PrmTableViewer::MakeLines()
{
	int ilDpxCount = ogDpxData.omData.GetSize();
	DPXDATA *blpDpxData;

	for (int ilLc = 0; ilLc < ilDpxCount; ilLc++) 
	{
		blpDpxData = &ogDpxData.omData[ilLc];
		if (IsPassFilter(blpDpxData)) {
			MakeLine(blpDpxData);
		}
	}
}

void PrmTableViewer::MakeLine(DPXDATA *bppDpxData)
{
	PRMDATA_LINE olLine;

	olLine.dpx 		 = bppDpxData;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(bppDpxData->Flnu);
	if(prlFlight != NULL)
	{
		olLine.Flno = prlFlight->Flno;
		if (*prlFlight->Adid == 'A') 
		{
			olLine.Etai = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
		}
		else
		{
			olLine.Etdi = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
		}
	}
	else
	{
		olLine.Flno = "";
	}
	FLIGHTDATA *prlFlight2 = ogFlightData.GetFlightByUrno(bppDpxData->Tflu);
	if(prlFlight2 != NULL)
	{
		olLine.Tfln = prlFlight2->Flno;
		olLine.Etdi = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
	}
	else
	{
		olLine.Tfln = "";
	}
	if (ogJobData.JobForUprmExist(bppDpxData->Urno) )
	{
		olLine.HasJobs = "Yes";
	}
	/*
	olLine.Service = GetServicesForDpx(bppDpxData->Urno);
	if (bgPrmShowAssignment)
	{
		CString olName;
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByUprm(olJobs, bppDpxData->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJob = 0; ilJob<imMaxJobs; ilJob++)
		{
//			EMPDATA *olEmp = ogEmpData.GetEmpByUrno(olJobs[ilJob].Ustf);
			if (ilJob < ilNumJobs)
			{
				olName = ogEmpData.GetEmpName(olJobs[ilJob].Ustf);
				CCSPtrArray<JOBDATA> olPoolJobs;
				ogJobData.GetJobsByShur(olPoolJobs,olJobs[ilJob].Shur,TRUE);
				int ilNumPoolJobs = olPoolJobs.GetSize();
				for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
				{
					JOBDATA *prlPJ = &olPoolJobs[ilPJ];
					if(strlen(prlPJ->Text) > 0)
					{
						olName = olName + CString(" - ") + prlPJ->Text;
						break;
					}
				}
				olLine.Staffs.Add(olName);
				olLine.FromTos.Add(CString(olJobs[ilJob].Acfr.Format("%H%M")) + CString("-") + CString(olJobs[ilJob].Acto.Format("%H%M")));
				olLine.Functions.Add(ogSpfData.GetFunctionBySurn(olJobs[ilJob].Ustf));
				SERDATA* prlSer = ogSerData.GetSerByUrno(olJobs[ilJob].Ughs);
				if (!prlSer)
				{
					olLine.Services.Add(CString(""));
				}
				else
				{
					olLine.Services.Add(CString(prlSer->Seco));
				}
			}
		}
	}
	*/
	int ilPrm = CreateLine(&olLine);
}


/////////////////////////////////////////////////////////////////////////////
// PrmTableViewer -- PRMDATA_LINE array maintenance
//
// DeleteLine() is unnecessary because we can assume that there is no possibility
// that a PRM Desk will be removed out of the system. Furthermore, there is no
// DDX processing for the PRM Desk changes also.

void PrmTableViewer::DeleteAll()
{
	int n = omLines.GetSize();
//	for (int i = 0; i < n; i++)
//		omLines[i].JobData.DeleteAll();
	omLines.DeleteAll();
}

int PrmTableViewer::CreateLine(PRMDATA_LINE *prpPrm)
{
 	prpPrm->Service = GetServicesForDpx(prpPrm->dpx->Urno);
	if (bgPrmShowAssignment)
	{
		CString olName;
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByUprm(olJobs, prpPrm->dpx->Urno);
		int ilNumJobs = olJobs.GetSize();
		prpPrm->Staffs.RemoveAll();
		prpPrm->FromTos.RemoveAll();
		prpPrm->Functions.RemoveAll();
		prpPrm->Services.RemoveAll();
		for(int ilJob = 0; ilJob<imMaxJobs; ilJob++)
		{
//			EMPDATA *olEmp = ogEmpData.GetEmpByUrno(olJobs[ilJob].Ustf);
			if (ilJob < ilNumJobs)
			{
				olName = ogEmpData.GetEmpName(olJobs[ilJob].Ustf);
				CCSPtrArray<JOBDATA> olPoolJobs;
				ogJobData.GetJobsByShur(olPoolJobs,olJobs[ilJob].Shur,TRUE);
				int ilNumPoolJobs = olPoolJobs.GetSize();
				for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
				{
					JOBDATA *prlPJ = &olPoolJobs[ilPJ];
					if(strlen(prlPJ->Text) > 0)
					{
						olName = olName + CString(" - ") + prlPJ->Text;
						break;
					}
				}
				prpPrm->Staffs.Add(olName);
				prpPrm->FromTos.Add(ogBasicData.FormatDate(olJobs[ilJob].Acfr,omStartTime) + CString("-") + ogBasicData.FormatDate(olJobs[ilJob].Acto,omStartTime));
				prpPrm->Functions.Add(ogSpfData.GetFunctionBySurn(olJobs[ilJob].Ustf));
				SERDATA* prlSer = ogSerData.GetSerByUrno(olJobs[ilJob].Ughs);
				if (!prlSer)
				{
					prpPrm->Services.Add(CString(""));
				}
				else
				{
					prpPrm->Services.Add(CString(prlSer->Seco));
				}
			}
		}
	}
   int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (ComparePrm(prpPrm, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePrm(prpPrm, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	PRMDATA_LINE *pPrm = prpPrm;
    omLines.NewAt(ilLineno, *pPrm);
	if (pomTable)
	{
		Format(&omLines[ilLineno],omColumns);
		pomTable->InsertTextLine(ilLineno,omColumns,&omLines[ilLineno]);
		//set line color
		//pomTable->SetTextLineColor(ilLineno, BLACK, PURPLE);
	}
    return ilLineno;
}


void PrmTableViewer::UpdateCurrentDisplay() //igu
{
	//ogCfgData.rmUserSetup.PRMV = pcpViewName;
    //SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter(); 

    DeleteAll();

	EvaluateTableFields();
	MakeColumns();
    MakeLines();
	UpdateDisplay(true);
}

/////////////////////////////////////////////////////////////////////////////
// PrmTableViewer - display drawing routine

void PrmTableViewer::UpdateDisplay(BOOL bpEraseAll)
{
	if (bpEraseAll)
	{
/*		CString olHeader;
		CString olFormat;
		//pomTable->SetHeaderFields("ID|Name|Type|Gender|Time|Closed|Flight|Transit|Lang|Nat");
		if (bgShowPrmClos)
		{
			olHeader = GetString(IDS_PRMTABLE_HEADER2);
			olFormat = CString("10|20|12|3|16|16|9|9|10|10|3");
		}
		else
		{
			if (!bgPrmShowStaStd)
			{
				olHeader = GetString(IDS_PRMTABLE_HEADER);
				olFormat = CString("10|20|9|3|3|9|9|10|10|3|12|14|12|14");
			}
			else
			{
				olHeader = GetString(IDS_PRMTABLE_HEADER3);
				olFormat = CString("10|20|7|5|2|7|7|2|7|7|3|3|3|4|12|4|12");
			}
		}

		if (bgPrmShowAssignment)
		{
			olHeader = olHeader + GetString(IDS_STRING33194);
			olFormat = olFormat + GetString(IDS_STRING33195);
		}

		pomTable->SetHeaderFields(olHeader);
		pomTable->SetFormatList(olFormat);

		pomTable->ResetContent();
		*/

		CCSPtrArray<TABLE_HEADER_COLUMN> header;
//		CCSPtrArray<TABLE_COLUMN> columns;
		for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
		{
			header.NewAt(ilC,TABLE_HEADER_COLUMN());
			header[ilC].Text   = omTableFields[ilC].Name;
			header[ilC].Length = omTableFields[ilC].Length;
//			columns.NewAt(ilC,TABLE_COLUMN());
		}


		pomTable->SetHeaderFields(header);

		pomTable->ResetContent();

		for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++) 
		{
			Format(&omLines[ilLc],omColumns);
			pomTable->AddTextLine(omColumns, &omLines[ilLc]);
//			pomTable->InsertTextLine(ilLc,omColumns, &omLines[ilLc]);
//			pomTable->SetTextLineColor(ilLc, BLACK, WHITE);
		//	pomTable->AddTextLine("Text",&omLines[ilLc]);
		}
	}
	pomTable->DisplayTable();
	//make line color
	COLORREF TextColor;
	COLORREF BackGroundColor;
	getHighLightColor(TextColor,BackGroundColor);
	makeLineColor(TextColor,BackGroundColor);
}

void PrmTableViewer::makeLineColor(COLORREF& textColor,COLORREF& backGroundColor)
{
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++) 
		{
			CString mystr = omLines[ilLc].dpx->Aloc;
			if(mystr.Find("Lounge") != -1)
				pomTable->SetTextLineColor(ilLc, textColor, backGroundColor);
		}
}
void PrmTableViewer::getHighLightColor(COLORREF& textColor,COLORREF& backGroundColor)
{
	//default color
	textColor = BLACK;
	backGroundColor = WHITE;

	//... set color here
	
	if(IsPrivateProfileOn("SHOW_PRM_LOUNGE_COLOUR","NO"))
	{
		CString olColour;
		if(ogDlgSettings.GetValue(CDlgSettings.CTYP_OTHER_COLOURS, CDlgSettings.CKEY_OTHER_COLOURS_PRM_LOCATION_LOUNGE, olColour))
		{
			backGroundColor = (COLORREF) atol(olColour);
		}
	}
	
}
void PrmTableViewer::Format(PRMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	int ilIndex = TableFieldIndex("PRID");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Prid;

	ilIndex = TableFieldIndex("NAME");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Name;

	ilIndex = TableFieldIndex("SEAT");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Seat;

	ilIndex = TableFieldIndex("PRMT");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Prmt;

	ilIndex = TableFieldIndex("GEND");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Gend;

	ilIndex = TableFieldIndex("SATI");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Sati.Format("%d.%m %H:%M"));

	ilIndex = TableFieldIndex("CLOS");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Clos.Format("%d.%m %H:%M"));

	ilIndex = TableFieldIndex("ETAI");
	if (ilIndex >= 0)
	{
		if (prpLine->dpx->FlnuAdid=='A')
		{
			rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Etai,omStartTime);
		}
		else
		{
			rrpColumns[ilIndex].Text = "";
		}
	}

	ilIndex = TableFieldIndex("ETDI");
	if (ilIndex >= 0)
	{
		if (prpLine->dpx->TfluAdid == 'D'||prpLine->dpx->FlnuAdid=='D')
		{
			rrpColumns[ilIndex].Text = ogBasicData.FormatDate(prpLine->Etdi,omStartTime);
		}
		else
		{
			rrpColumns[ilIndex].Text = "";
		}
	}

	ilIndex = TableFieldIndex("ADID");
	if (ilIndex >= 0)
	{
		if (strlen(prpLine->dpx->TfluFlno)>0 && prpLine->dpx->TfluAdid == 'D')
		{
			rrpColumns[ilIndex].Text = CString("T");
		}
		else
		{
			rrpColumns[ilIndex].Text = CString(prpLine->dpx->FlnuAdid);
		}
	}

	ilIndex = TableFieldIndex("FLNUFLNO");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->FlnuFlno);

	ilIndex = TableFieldIndex("TFLUFLNO");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->TfluFlno);

	ilIndex = TableFieldIndex("LANG");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Lang);

	ilIndex = TableFieldIndex("NATL");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Natl);

	ilIndex = TableFieldIndex("HASJOB");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->HasJobs);

	ilIndex = TableFieldIndex("ABSR");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Absr;

	ilIndex = TableFieldIndex("ABDT");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Abdt.Format("%d.%m %H:%M"));

	ilIndex = TableFieldIndex("ALOC");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->dpx->Aloc;

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByUprm(olJobs, prpLine->dpx->Urno);
	int ilNumJobs = olJobs.GetSize();
	CString strTemp;
	
	for (int ilJob = 0;ilJob < imMaxJobs;ilJob++)
	{
		strTemp.Format("STAF%d",ilJob+1);
		ilIndex = TableFieldIndex(strTemp);
		if (ilIndex >= 0)
		{
			if(ilJob<ilNumJobs)
				rrpColumns[ilIndex].Text = prpLine->Staffs[ilJob];
			else
				rrpColumns[ilIndex].Text = "";
		}

		strTemp.Format("FRTO%d",ilJob+1);
		ilIndex = TableFieldIndex(strTemp);
		if (ilIndex >= 0)
		{
			if(ilJob<ilNumJobs)
				rrpColumns[ilIndex].Text = prpLine->FromTos[ilJob];
			else
				rrpColumns[ilIndex].Text = "";
		}

		strTemp.Format("FUNC%d",ilJob+1);
		ilIndex = TableFieldIndex(strTemp);
		if (ilIndex >= 0)
		{
			if(ilJob<ilNumJobs)
				rrpColumns[ilIndex].Text = prpLine->Functions[ilJob];
			else
				rrpColumns[ilIndex].Text = "";
		}

		strTemp.Format("SERV%d",ilJob+1);
		ilIndex = TableFieldIndex(strTemp);
		if (ilIndex >= 0)
		{
			if(ilJob<ilNumJobs)
				rrpColumns[ilIndex].Text = prpLine->Services[ilJob];
			else
				rrpColumns[ilIndex].Text = "";
		}
	}


	ilIndex = TableFieldIndex("REMA");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Rema);
	}

	ilIndex = TableFieldIndex("FLNUPRMS");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->FlnuPrms);
	}

	ilIndex = TableFieldIndex("TFLUPRMS");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->TfluPrms);
	}

	ilIndex = TableFieldIndex("MAIL");
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = CString(prpLine->dpx->Mail);
	}
}
void PrmTableViewer::AddLine(DPXDATA *prpDpx)
{
	MakeLine(prpDpx);
	UpdateDisplay(false);
}

/////////////////////////////////////////////////////////////////////////////
// PrmTableViewer -- DDX processing

void PrmTableViewer::PrmTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
//	return;

    PrmTableViewer *polViewer = (PrmTableViewer *)popInstance;

    if (ipDDXType == DPX_NEW)
	{
        polViewer->ProcessPrmDelete((DPXDATA *)vpDataPointer);
        polViewer->ProcessPrmNew((DPXDATA *)vpDataPointer);
	}
	else if (ipDDXType == DPX_CHANGE)
	{
				// lli: when delete then add, there is chances for some dialogs to access null pointer. so change to update
        polViewer->ProcessPrmUpdate((DPXDATA *)vpDataPointer);
	}
	else if (ipDDXType == DPX_ALLOC)
	{
        if (polViewer->ProcessPrmDelete((DPXDATA *)vpDataPointer))
			polViewer->ProcessPrmNew((DPXDATA *)vpDataPointer);
	}
	else if (ipDDXType == DPX_DELETE)
        polViewer->ProcessPrmDelete((DPXDATA *)vpDataPointer);
	else if (ipDDXType == FLIGHT_CHANGE)
		polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
	else if (ipDDXType == JOB_NEW)
		polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
	else if (ipDDXType == JOB_CHANGE)
		polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
	else if (ipDDXType == JOB_DELETE)
		polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);

}

BOOL PrmTableViewer::ProcessPrmNew(DPXDATA *prpDpx)
{
	MakeLine(prpDpx);
	UpdateDisplay(false);

 	return FALSE;
}

BOOL PrmTableViewer::ProcessPrmDelete(DPXDATA *prpDpx)
{
	
		int  ilNumLines = omLines.GetSize();
		for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
		{
			PRMDATA_LINE *prlLine = GetLine(ilLc);
			if (prlLine != NULL && prlLine->dpx->Urno == prpDpx->Urno)
			{
				omLines.DeleteAt(ilLc);
				if (pomTable)
					pomTable->DeleteTextLine(ilLc);
				UpdateDisplay(false);
				return TRUE;
			}
		}
	return FALSE;
}

// lli: create the Update function to do update so as to avoid accessing null pointer when we do delete and add for update
BOOL PrmTableViewer::ProcessPrmUpdate(DPXDATA *prpDpx)
{
	
	int  ilNumLines = omLines.GetSize();
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		PRMDATA_LINE *prlLine = GetLine(ilLc);
		if (prlLine != NULL && prlLine->dpx->Urno == prpDpx->Urno)
		{
			PRMDATA_LINE olLine;

			olLine.dpx 		 = prpDpx;
			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpDpx->Flnu);
			if(prlFlight != NULL)
			{
				olLine.Flno = prlFlight->Flno;
				if (*prlFlight->Adid == 'A') 
				{
					olLine.Etai = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olLine.Etdi = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
			}
			else
			{
				olLine.Flno = "";
			}
			FLIGHTDATA *prlFlight2 = ogFlightData.GetFlightByUrno(prpDpx->Tflu);
			if(prlFlight2 != NULL)
			{
				olLine.Tfln = prlFlight2->Flno;
				olLine.Etdi = (prlFlight2->Etod != TIMENULL) ? prlFlight2->Etod : prlFlight2->Stod;
			}
			else
			{
				olLine.Tfln = "";
			}
			if (ogJobData.JobForUprmExist(prpDpx->Urno) )
			{
				olLine.HasJobs = "Yes";
			}
			omLines.DeleteAt(ilLc);
			if (pomTable)
				pomTable->DeleteTextLine(ilLc);
			int ilPrm = CreateLine(&olLine);
			UpdateDisplay(false);
			return TRUE;
		}
	}
	return FALSE;
}

// lli: for updating STD and STA column
BOOL PrmTableViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	CCSPtrArray <DPXDATA> olTmpDpxs;
	ogDpxData.GetDpxsByFlight(prpFlight->Urno,olTmpDpxs,true);
	for(int ilFJ = 0; ilFJ < olTmpDpxs.GetSize(); ilFJ++)
	{
		DPXDATA *prlDpx = &olTmpDpxs[ilFJ];
		if (prlDpx != NULL)
		{
			ProcessPrmUpdate(prlDpx);
		}
	}
 	return true;
}

// lli: for updating Jobs and Job Assignments columns
BOOL PrmTableViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if (prpJob->Uprm!=0L)
	{
		DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpJob->Uprm);
		if (prlDpx != NULL)
		{
			ProcessPrmUpdate(prlDpx);
		}
	}
	else if (!strcmp(prpJob->Jtco,JOBPOOL))
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByPoolJob(olJobs, prpJob->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			ProcessJobChange(&olJobs[ilJob]);
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL PrmTableViewer::PrintPRMLine(PRMDATA_LINE *prpLine,BOOL bpIsLastLine)
{
	/***/
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			if (omTableFields.GetSize() < 1)
			{
				MessageBox(NULL,"At least one column should be included to print!", "", MB_OK);
				return FALSE;
			}
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Length     = omTableFields[0].PrintLength;
			rlElement.Text       = omTableFields[0].Name;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_NOFRAME;
			for (int i = 1; i <omTableFields.GetSize();i++)
			{
				rlElement.Length     = omTableFields[i].PrintLength;
				rlElement.Text       = omTableFields[i].Name;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();

		}

		Format(prpLine,omColumns);
		CTime olActual = TIMENULL;
		CString olActualMark;
			
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_FRAMETHIN;
		rlElement.FrameTop   = PRINT_NOFRAME;
		rlElement.FrameBottom= ilBottomFrame;
		rlElement.pFont       = &pomPrint->omSmallFont_Regular;
		rlElement.Length     = omTableFields[0].PrintLength;
		rlElement.Text       = omColumns[0].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
		rlElement.FrameLeft  = PRINT_NOFRAME;

		CString myID="ID";
		CString myName="Name";
		CString myServices1="Services1";
		CString myAssignment1="Assignment1";
		CString myServices2="Services2";
		CString myAssignment2="Assignment2";
		CString myServices3="Services3";
		CString myAssignment3="Assignment3";
		CString myServices4="Services4";
		CString myAssignment4="Assignment4";
		CString myLocation="Location";
		CString myRemarks="Remarks";
		for (int i = 1; i <omColumns.GetSize();i++)
		{
			rlElement.Length     = omTableFields[i].PrintLength;
			rlElement.Text       = omColumns[i].Text;
			
			//added by MAX
			rlElement.Alignment  = PRINT_LEFT;
			CString ColonName=omTableFields[i].Name;
			
			if (ColonName!=myID&&
				ColonName!=myName&&
				ColonName!=myServices1&&
				ColonName!=myAssignment1&&
				ColonName!=myServices2&&
				ColonName!=myAssignment2&&
				ColonName!=myServices3&&
				ColonName!=myAssignment3&&
				ColonName!=myServices4&&
				ColonName!=myAssignment4&&
				ColonName!=myLocation&&
				ColonName!=myRemarks)
			{
				rlElement.Alignment  = PRINT_CENTER;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}
	
	return TRUE;
}

BOOL PrmTableViewer::PrintPRMHeader(CCSPrint *pomPrint)
{
	/***/
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	/***/
	return TRUE;
}

void PrmTableViewer::PrintView()
{  
	
	CString omTarget;
	omTarget.Format(GetString(IDS_STRING61285),"",omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y %H:%M"));
	omTarget += GetString(IDS_STRING61292) + ogCfgData.rmUserSetup.PRMV;		
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,350,100,GetString(IDS_STRING61419),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61356),pcgAppName);
		//if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,"A4",DMPAPER_A4)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING61355));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintPRMLine(&omLines[i],TRUE);
						pomPrint->PrintFooter(olTxt,GetString(IDS_STRING61419));
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintPRMHeader(pomPrint);
				}
				PrintPRMLine(&omLines[i],FALSE);
			}
			PrintPRMLine(NULL,TRUE);
			CString olTxt;
			pomPrint->PrintFooter(olTxt,GetString(IDS_STRING61419));
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

}

// lli: Printing to excel
void PrmTableViewer::CreateExport(const char *pclFileName, const char *pclSeperator, CString opTitle)
{
	ofstream of;
	of.open( pclFileName, ios::out);

	// title
	of  << setw(0) << opTitle << endl;

	// Staff ID
	CString olStaff = CString("Staff ID: ");
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(ogUsername);
	if (prlEmp != NULL)
	{
			of  << setw(0) << olStaff << setw(0) << ogUsername << setw(0)  << pclSeperator << setw(0)  << prlEmp->Shnm << endl;
	}
	else
	{
		of  << setw(0) << olStaff << setw(0) << ogUsername << endl;
	}

	// column header
	int ilF = 0, i = 0;
    CStringArray olFields;
	CString olString;
	if (bgShowPrmClos)
	{
		olString = GetString(IDS_PRMTABLE_HEADER2);
	}
	else
	{
		if (!bgPrmShowStaStd)
		{
			olString = GetString(IDS_PRMTABLE_HEADER);
		}
		else
		{
			olString = GetString(IDS_PRMTABLE_HEADER3);
		}
	}

	if (bgPrmShowAssignment)
	{
		olString = olString + GetString(IDS_STRING33194);
	}

	ogBasicData.ExtractItemList(olString,&olFields,'|');
	for(ilF = 0; ilF < omTableFields.GetSize(); ilF++) of << setw(0) << omTableFields[ilF].Name << setw(0) << pclSeperator;
	of << endl;

	for(i = 0; i < omLines.GetSize(); i++ ) 
	{
		Format(&omLines[i],omColumns);
//		ogBasicData.ExtractItemList(omColumns, &olFields, '|');
		for(ilF = 0; ilF < omColumns.GetSize(); ilF++)
			of << setw(0) << ogBasicData.FormatFieldForExport(omColumns[ilF].Text, pclSeperator) << setw(0) << pclSeperator;
		of << endl;
	}
	
	of << endl;
	of.close();
}

CTime PrmTableViewer::StartTime() const
{
	return omStartTime;
}

CTime PrmTableViewer::EndTime() const
{
	return omEndTime;
}

int PrmTableViewer::Lines() const
{
	return omLines.GetSize();
}

PRMDATA_LINE* PrmTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void PrmTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void PrmTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

void PrmTableViewer::GetDefaultViewColumns(CStringArray& ropColumnNames)
{
	CString olHeader;

	if (bgShowPrmClos)
	{
		olHeader = CString("PRID,NAME,PRMT,GEND,SATI,CLOS,FLNUFLNO,TFLUFLNO,LANG,NATL,HASJOB");
	}
	else
	{
		if (!bgPrmShowStaStd)
		{
			olHeader = CString("PRID,NAME,PRMT,GEND,ADID,FLNUFLNO,TFLUFLNO,LANG,NATL,HASJOB,ABSR,ABDT,ALOC,SATI");
		}
		else
		{
			olHeader = CString("PRID,NAME,SEAT,PRMT,GEND,ETAI,ETDI,ADID,FLNUFLNO,TFLUFLNO,LANG,NATL,HASJOB,ABSR,ABDT,ALOC,SATI");
		}
	}

	if (bgPrmShowAssignment)
	{
		olHeader = olHeader + CString(",STAF1,FRTO1,FUNC1,STAF2,FRTO2,FUNC2,STAF3,FRTO3,FUNC3,STAF4,FRTO4,FUNC4,REMA");
	}

	char *token;
	token = strtok(olHeader.GetBuffer(0),",");
	while(token)
	{
		ropColumnNames.Add(token);
		token = strtok(NULL,",");
	}
}

void PrmTableViewer::GetAllViewColumns(CStringArray& ropColumnNames)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (!ogBasicData.DisplayDebugInfo() && strcmp(osfields[i].Field,"ULNK") == 0)
			continue;
		if (strcmp(osfields[i].Field,"PNAM") == 0|| strcmp(osfields[i].Field,"PREM") == 0)
		{
			if (ogBasicData.IsPRMRelatedTemplate())
			{
				ropColumnNames.Add(osfields[i].Field);	
			}
		}
		else
		{
			ropColumnNames.Add(osfields[i].Field);	
		}
	}
	
}

CString PrmTableViewer::GetViewColumnName(const CString& ropColumnField)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Field == ropColumnField)
			return osfields[i].Name;	
	}
	
	return "";
}

CString PrmTableViewer::GetViewColumnField(const CString& ropColumnName)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Name == ropColumnName)
			return osfields[i].Field;	
	}
	
	return "";
}

bool PrmTableViewer::EvaluateTableFields()
{

	CStringArray olViewColumns;
	CViewer::GetFilter("Columns",olViewColumns);
	if (!olViewColumns.GetSize())
		GetDefaultViewColumns(olViewColumns);							

	omTableFields.DeleteAll();

	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int ilC = 0; ilC < olViewColumns.GetSize();ilC++)
	{
		for (int i = 0; i < ilSize; i++)
		{
			if (osfields[i].Field.CompareNoCase(olViewColumns[ilC]) == 0)
			{
				omTableFields.New(osfields[i]);
			}
		}

	}

	return true;
}

int PrmTableViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

void PrmTableViewer::MakeColumns()
{
	omColumns.DeleteAll();
	int ilNumFields = omTableFields.GetSize();
	for (int ilC = 0; ilC < ilNumFields; ilC++)
	{
		omColumns.NewAt(ilC,TABLE_COLUMN());
	}
}


