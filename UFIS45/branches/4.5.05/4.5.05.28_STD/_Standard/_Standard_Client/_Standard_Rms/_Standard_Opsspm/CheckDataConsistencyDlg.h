#if !defined(AFX_CHECKDATACONSISTENCYDLG_H__B5CA2003_6FC1_11D7_822D_00010215BFE5__INCLUDED_)
#define AFX_CHECKDATACONSISTENCYDLG_H__B5CA2003_6FC1_11D7_822D_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CheckDataConsistencyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCheckDataConsistencyDlg dialog

class CCheckDataConsistencyDlg : public CDialog
{
// Construction
public:
	CCheckDataConsistencyDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCheckDataConsistencyDlg)
	enum { IDD = IDD_CHECKDATACONSISTENCYDLG };
	CString	m_ErrList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCheckDataConsistencyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCheckDataConsistencyDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void CheckDataConsistency(CStringArray &ropErrors);

private:
	CString omNewLine;
	CString FormatError(char *pcpFormatList, ...);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHECKDATACONSISTENCYDLG_H__B5CA2003_6FC1_11D7_822D_00010215BFE5__INCLUDED_)
