// DemandTablePropertySheet.h : header file
//
#ifndef _DEMANDTBPS_H_
#define _DEMANDTBPS_H_
/////////////////////////////////////////////////////////////////////////////
// DemandTablePropertySheet

class DemandTablePropertySheet : public BasePropertySheet
{
	DECLARE_DYNAMIC(DemandTablePropertySheet)

// Construction
public:
	DemandTablePropertySheet(CWnd* pParentWnd = NULL,CViewer *popViewer = NULL, UINT iSelectPage = 0);
	~DemandTablePropertySheet();

// Attributes
public:
	DemandFilterPage	m_pageColumns;
	DemandFilterPage	m_pageAloc;
	FilterPage			m_pageDety;
	FilterPage			m_pageRety;
	FilterPage			m_pageAirline;
	FilterPage			m_pageAirport;
	FilterPage			m_pageNature;
	FilterPage			m_pageRank;	
	FilterPage			m_pageService;	
	FilterPage			m_pagePermits;	
	FilterPage			m_pageCic;	
	FilterPage			m_pageBelt1;	
	DemandTableSortPage m_pageSort;
	CMapStringToPtr		m_pageAlids;

	DemandFilterPage *polPage1;
	DemandFilterPage *polPage2;
	DemandFilterPage *polPage3;
	DemandFilterPage *polPage4;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();

	// Generated message map functions
protected:
	//{{AFX_MSG(DemandTablePropertySheet)
	afx_msg LONG OnUpdateAllPages(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _DEMANDTBPS_H_
/////////////////////////////////////////////////////////////////////////////

