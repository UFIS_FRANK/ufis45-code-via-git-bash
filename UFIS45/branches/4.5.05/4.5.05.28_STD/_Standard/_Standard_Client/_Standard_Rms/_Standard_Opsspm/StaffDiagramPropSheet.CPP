// stfdiaps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaRnkData.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <CedaPolData.h>
#include <CedaPerData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <StaffDiagramSortPage.h>
#include <StaffDiagramGroupPage.h>
#include <BasePropertySheet.h>
#include <StaffDiagramPropSheet.h>
#include <CedaWgpData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramPropertySheet
//

//StaffDiagramPropertySheet::StaffDiagramPropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_STAFF_DIAGRAM, pParentWnd, popViewer, iSelectPage)
StaffDiagramPropertySheet::StaffDiagramPropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61585), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pagePool);
	AddPage(&m_pageRank);
	AddPage(&m_pagePermits);
	AddPage(&m_pageShiftCode);
	AddPage(&m_pageTeam);
	AddPage(&m_pageSort);
	AddPage(&m_pageGroup);
	if(IsPrivateProfileOn("SHOW_STAFF_MARKER_FILTER","NO"))
	{
		AddPage(&m_pageArrDep);
	}

	// Change the caption in tab control of the PropertySheet
	m_pagePool.SetCaption(GetString(IDS_STRING61603));
	m_pageRank.SetCaption(GetString(IDS_STRING61604));
	m_pagePermits.SetCaption(GetString(IDS_PERMIT_TAB));
	m_pageShiftCode.SetCaption(GetString(IDS_STRING61605));
	m_pageTeam.SetCaption(GetString(IDS_STRING61606));

	// Prepare possible values for each PropertyPage
	CStringArray olPools;
	ogPolData.GetPoolNames(olPools);
	int ilNumPools = olPools.GetSize();
	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
	{
		m_pagePool.omPossibleItems.Add(olPools[ilPool]);
	}
	m_pagePool.SetSorted(FALSE);
	m_pagePool.bmSelectAllEnabled = true;

	ogRnkData.GetAllRanks(m_pageRank.omPossibleItems);
	m_pageRank.bmSelectAllEnabled = true;
	ogPerData.GetAllPermits(m_pagePermits.omPossibleItems);
	m_pagePermits.bmSelectAllEnabled = true;
	ogShiftTypes.GetAllShiftTypes(m_pageShiftCode.omPossibleItems);
	m_pageShiftCode.bmSelectAllEnabled = true;
	ogWgpData.GetAllTeams(m_pageTeam.omPossibleItems);
	m_pageTeam.omPossibleItems.Add(GetString(IDS_NOWORKGROUP));
	m_pageTeam.bmSelectAllEnabled = true;

	bmOldHideAbsentEmps = FALSE;
	bmOldHideStandbyEmps = FALSE;
	bmOldHideFidEmps = FALSE;
}

void StaffDiagramPropertySheet::LoadDataFromViewer()
{
	if(!pomViewer->GetFilter("Pool", m_pagePool.omSelectedItems))
	{
		m_pagePool.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Dienstrang", m_pageRank.omSelectedItems))
	{
		m_pageRank.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Permits", m_pagePermits.omSelectedItems))
	{
		m_pagePermits.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Schichtcode", m_pageShiftCode.omSelectedItems))
	{
		m_pageShiftCode.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Team", m_pageTeam.omSelectedItems))
	{
		m_pageTeam.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageGroup.omGroupBy = pomViewer->GetGroup();
	m_pageGroup.m_GroupView = pomViewer->GetUserData("GROUPVIEW") == "YES" ? TRUE : FALSE;
	// for compatibility with version 1.19
	CString olHideCompletelyDelegatedTeams = pomViewer->GetUserData("HIDEDELEG");
	if (olHideCompletelyDelegatedTeams.IsEmpty())
		m_pageGroup.m_HideDelegatedGroups = false;
	else
		m_pageGroup.m_HideDelegatedGroups = olHideCompletelyDelegatedTeams == "YES" ? TRUE : FALSE;

	CString olHideAbsentEmps = pomViewer->GetUserData("HIDEABSENTEMPS");
	if (olHideAbsentEmps.IsEmpty())
		m_pageGroup.m_HideAbsentEmps = false;
	else
		m_pageGroup.m_HideAbsentEmps = olHideAbsentEmps == "YES" ? TRUE : FALSE;
	bmOldHideAbsentEmps = m_pageGroup.m_HideAbsentEmps;

	CString olHideFidEmps = pomViewer->GetUserData("HIDEFIDEMPS");
	if (olHideFidEmps.IsEmpty())
		m_pageGroup.m_HideFidEmps = false;
	else
		m_pageGroup.m_HideFidEmps = olHideFidEmps == "YES" ? TRUE : FALSE;
	bmOldHideFidEmps = m_pageGroup.m_HideFidEmps;

	CString olHideStandbyEmps = pomViewer->GetUserData("HIDESTANDBYEMPS");
	if (olHideStandbyEmps.IsEmpty())
		m_pageGroup.m_HideStandbyEmps = false;
	else
		m_pageGroup.m_HideStandbyEmps = olHideStandbyEmps == "YES" ? TRUE : FALSE;
	bmOldHideStandbyEmps = m_pageGroup.m_HideStandbyEmps;


	CString olVal;

	olVal = pomViewer->GetUserData("STOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmStoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmStoaColour = SILVER;
	}

	olVal = pomViewer->GetUserData("ETOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmEtoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmEtoaColour = WHITE;
	}

	olVal = pomViewer->GetUserData("TMOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmTmoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmTmoaColour = YELLOW;
	}

	olVal = pomViewer->GetUserData("LAND");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmLandColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmLandColour = GREEN;
	}

	olVal = pomViewer->GetUserData("ONBL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmOnblColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmOnblColour = LIME;
	}

	olVal = pomViewer->GetUserData("STOD");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmStodColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmStodColour = SILVER;
	}

	olVal = pomViewer->GetUserData("ETOD");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmEtodColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmEtodColour = WHITE;
	}

	olVal = pomViewer->GetUserData("SLOT");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmSlotColour= atol(olVal);
	}
	else
	{
		m_pageArrDep.lmSlotColour = YELLOW;
	}

	olVal = pomViewer->GetUserData("OFBL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmOfblColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmOfblColour = LIME;
	}

	olVal = pomViewer->GetUserData("AIRB");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmAirbColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmAirbColour = GREEN;
	}

	m_pageArrDep.SetColours();

	olVal = pomViewer->GetUserData("ARRIVAL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmArrCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmArrCheckBox = (ogBasicData.omDepArrMarkerStaff.Find("ARRIVAL") != -1) ? TRUE : FALSE;
	}

	olVal = pomViewer->GetUserData("DEPARTURE");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmDepCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmDepCheckBox = (ogBasicData.omDepArrMarkerStaff.Find("DEPARTURE") != -1) ? TRUE : FALSE;
	}

	olVal = pomViewer->GetUserData("TURNAROUND");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmTurnaroundCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmTurnaroundCheckBox = (ogBasicData.omDepArrMarkerStaff.Find("TURNAROUND") != -1) ? TRUE : FALSE;
	}
}

void StaffDiagramPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Pool", m_pagePool.omSelectedItems);
	pomViewer->SetFilter("Dienstrang", m_pageRank.omSelectedItems);
	pomViewer->SetFilter("Permits", m_pagePermits.omSelectedItems);
	pomViewer->SetFilter("Schichtcode", m_pageShiftCode.omSelectedItems);
	pomViewer->SetFilter("Team", m_pageTeam.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(m_pageGroup.omGroupBy);
	pomViewer->SetUserData("GROUPVIEW",m_pageGroup.m_GroupView ? "YES" : "NO");
	pomViewer->SetUserData("HIDEDELEG",m_pageGroup.m_HideDelegatedGroups ? "YES" : "NO");
	pomViewer->SetUserData("HIDEABSENTEMPS",m_pageGroup.m_HideAbsentEmps ? "YES" : "NO");
	pomViewer->SetUserData("HIDEFIDEMPS",m_pageGroup.m_HideFidEmps ? "YES" : "NO");
	pomViewer->SetUserData("HIDESTANDBYEMPS",m_pageGroup.m_HideStandbyEmps ? "YES" : "NO");

	CString olColour;
	olColour.Format("%ld",m_pageArrDep.lmStoaColour);	pomViewer->SetUserData("STOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmEtoaColour);	pomViewer->SetUserData("ETOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmTmoaColour);	pomViewer->SetUserData("TMOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmLandColour);	pomViewer->SetUserData("LAND",olColour);
	olColour.Format("%ld",m_pageArrDep.lmOnblColour);	pomViewer->SetUserData("ONBL",olColour);
	olColour.Format("%ld",m_pageArrDep.lmStodColour);	pomViewer->SetUserData("STOD",olColour);
	olColour.Format("%ld",m_pageArrDep.lmEtodColour);	pomViewer->SetUserData("ETOD",olColour);
	olColour.Format("%ld",m_pageArrDep.lmSlotColour);	pomViewer->SetUserData("SLOT",olColour);
	olColour.Format("%ld",m_pageArrDep.lmOfblColour);	pomViewer->SetUserData("OFBL",olColour);
	olColour.Format("%ld",m_pageArrDep.lmAirbColour);	pomViewer->SetUserData("AIRB",olColour);

	pomViewer->SetUserData("ARRIVAL",m_pageArrDep.bmArrCheckBox ? "YES" : "NO");
	pomViewer->SetUserData("DEPARTURE",m_pageArrDep.bmDepCheckBox ? "YES" : "NO");
	pomViewer->SetUserData("TURNAROUND",m_pageArrDep.bmTurnaroundCheckBox ? "YES" : "NO");

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int StaffDiagramPropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedPools;
	CStringArray olSelectedRanks;
	CStringArray olSelectedPermits;
	CStringArray olSelectedShifts;
	CStringArray olSelectedTeams;
	CStringArray olSortOrders;
	CString olGroupBy;
	
	pomViewer->GetFilter("Pool", olSelectedPools);
	pomViewer->GetFilter("Dienstrang", olSelectedRanks);
	pomViewer->GetFilter("Permits", olSelectedPermits);
	pomViewer->GetFilter("Schichtcode", olSelectedShifts);
	pomViewer->GetFilter("Team", olSelectedTeams);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();

	if (!IsIdentical(olSelectedRanks, m_pageRank.omSelectedItems) ||
		!IsIdentical(olSelectedPermits, m_pagePermits.omSelectedItems) ||
		!IsIdentical(olSelectedPools, m_pagePool.omSelectedItems) ||
		!IsIdentical(olSelectedShifts, m_pageShiftCode.omSelectedItems) ||
		!IsIdentical(olSelectedTeams, m_pageTeam.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageGroup.omGroupBy) ||
		bmOldHideAbsentEmps != m_pageGroup.m_HideAbsentEmps ||
		bmOldHideFidEmps != m_pageGroup.m_HideFidEmps ||
		bmOldHideStandbyEmps != m_pageGroup.m_HideStandbyEmps)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
