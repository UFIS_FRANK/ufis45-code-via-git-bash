// flviewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>

#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <PrintControl.h>
#include <ConflictViewer.h>
#include <conflict.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <CedaAltData.h>

class ConflictTable; //Singapore

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

enum {
    BY_TYPE,
    BY_WEIGHT,
    BY_ALID,
	BY_TIME,
    BY_NONE
};

const char *ConflictTableViewer::omAllocGroupTypes[] =	{
	ALLOCUNITTYPE_GATEGROUP,
	ALLOCUNITTYPE_CICGROUP,
	ALLOCUNITTYPE_REGNGROUP,
	ALLOCUNITTYPE_PSTGROUP,
	NULL
	};


ConflictTableViewer::ConflictTableViewer()
{
    SetViewerKey("ConfTab");
    pomTable = NULL;

	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	ogCCSDdx.Register(this, FLIGHT_SELECT,	CString("CONFLICTVIEWER"), CString("Flight Select"),ConflictTableCf);
	ogCCSDdx.Register(this, CONFLICT_CHANGE,CString("CONFLICTVIEWER"), CString("Conflict Changed"), ConflictTableCf);
	ogCCSDdx.Register(this, CONFLICT_DELETE,CString("CONFLICTVIEWER"), CString("Conflict Deleted"), ConflictTableCf);
}

ConflictTableViewer::~ConflictTableViewer()
{
	ogCCSDdx.UnRegister(this,NOTUSED);

    DeleteAll();
	omAttentionMap.RemoveAll();
}

void ConflictTableViewer::SetTableType(int ipTableType)
{
	bmTableType = ipTableType;
	if (bmTableType == CFLTABLE_ATTENTION)
	{
		omAttentionMap.RemoveAll();
		ogConflicts.LoadAttentionMap(omAttentionMap);
		ogConflicts.DeleteFromAttentionMap();
	}
}

void ConflictTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

void ConflictTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

void ConflictTableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}


void ConflictTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    

	if (bmTableType == CFLTABLE_ATTENTION)
		ogCfgData.rmUserSetup.ATTV = pcpViewName;
	else
		ogCfgData.rmUserSetup.CONV = pcpViewName;

    SelectView(pcpViewName);
    PrepareFilter();
	PrepareSorter();

	MakeLines();

	UpdateDisplay();

}


void ConflictTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int ilFliterValue, ilNumFilterValues;
    int i, n;

    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
    for (i = 0; i < n; i++)
        omCMapForAlcd[olFilterValues[i]] = NULL;

    GetFilter("Agents", olFilterValues);
    omCMapForAgents.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllAgents = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAgents = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAgents[olFilterValues[i]] = NULL;
	}

    GetFilter("Aloc", olFilterValues);
    omCMapForAloc.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllAlocs = true;
	else
	{
		bmUseAllAlocs = false;
		for (i = 0; i < n; i++)
		{
			omCMapForAloc[AlocFromAllocGroupType(olFilterValues[i])] = NULL;
		}
	}


    GetFilter("Pools", olFilterValues);
    omCMapForPools.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPools = false;
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
	{
		bmUseAllPools = true;
	}
	else
	{
		for (i = 0; i < n; i++)
		{
			omCMapForPools[olFilterValues[i]] = NULL;
		}
	}

    GetFilter("Functions", olFilterValues);
    omCMapForFunctions.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllFunctions = false;
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
	{
		bmUseAllFunctions = true;
	}
	else
	{
		for (i = 0; i < n; i++)
		{
			omCMapForFunctions[olFilterValues[i]] = NULL;
		}
	}

	// delete all alids
	POSITION pos = omCMapForAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		CMapStringToPtr *polAllocGroup = NULL; 
		omCMapForAlids.GetNextAssoc(pos,olKey,(void *&)polAllocGroup);
		if (polAllocGroup)
			delete polAllocGroup;
	}
	omCMapForAlids.RemoveAll();
	bmUseAllAlids.RemoveAll();

	const char **pclAllocGroupType = omAllocGroupTypes;
	void *ptr;
	while(*pclAllocGroupType)
	{
		CString olAllocType = AlocFromAllocGroupType(*pclAllocGroupType);

		if (bmUseAllAlocs)
		{
			bmUseAllAlids[olAllocType] = (void *)1;
		}
		else if (omCMapForAloc.Lookup(olAllocType,ptr))
		{
			CMapStringToPtr *polAllocGroup = new CMapStringToPtr;
			omCMapForAlids[olAllocType] = polAllocGroup;
		    GetFilter(*pclAllocGroupType, olFilterValues);
		    n = olFilterValues.GetSize();
			if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
				bmUseAllAlids[olAllocType] = (void *)1;
			else
			{
				bmUseAllAlids[olAllocType] = (void *)0;

				CCSPtrArray<ALLOCUNIT> ropUnitList;
				for (i = 0; i < n; i++)
				{
					ogAllocData.GetUnitsByGroup(*pclAllocGroupType,olFilterValues[i],ropUnitList,false,false);
				}

				n = ropUnitList.GetSize();
				for (i = 0; i < n; i++)
				{
					polAllocGroup->SetAt(ropUnitList[i].Name,NULL);
				}
			}
		}
		else
		{
			bmUseAllAlids[olAllocType] = (void *)1;
		}

		++pclAllocGroupType;
	}

    GetFilter("ConflictType", olFilterValues);
    omCMapForTypes.RemoveAll();
    ilNumFilterValues = olFilterValues.GetSize();
    for (ilFliterValue = 0; ilFliterValue < ilNumFilterValues; ilFliterValue++)
	{
        omCMapForTypes[olFilterValues[ilFliterValue]] = NULL;
	}

    GetFilter("Object Typ", olFilterValues);
    omCMapForTypeOfObject.RemoveAll();
    ilNumFilterValues = olFilterValues.GetSize();
    for (ilFliterValue = 0; ilFliterValue < ilNumFilterValues; ilFliterValue++)
	{
		CString olFilterValue = olFilterValues[ilFliterValue];
		if(olFilterValue == GetString(IDS_STRING61608)) //Einsatz
		{
		    omCMapForTypeOfObject[CFLO_JOB] = NULL;
		}
		else if(olFilterValue == GetString(IDS_STRING61609)) //Flug
		{
	        omCMapForTypeOfObject[CFLO_FLIGHT] = NULL;
		}
		else if(olFilterValue == GetString(IDS_DEMOBJTYPE)) //Demand
		{
	        omCMapForTypeOfObject[CFLO_DEMAND] = NULL;
		}
	}
	
//	GetFilter("Zeit", olFilterValues);
//	CString olHour = olFilterValues[0].Left(2);
//	CString olMinute = olFilterValues[0].Right(2);
//	CTime olTmpDate = ogBasicData.GetTime();
//	omStartTime = CTime(olTmpDate.GetYear(),olTmpDate.GetMonth(),olTmpDate.GetDay(),atoi(olHour),atoi(olMinute),0);
//	int ilDayOffset = olFilterValues[2][0] - '0';
//	omStartTime += CTimeSpan(ilDayOffset,0,0,0);
//	olHour = olFilterValues[1].Left(2);
//	olMinute = olFilterValues[1].Right(2);
//	omEndTime = CTime(omStartTime.GetYear(),omStartTime.GetMonth(),omStartTime.GetDay(),atoi(olHour),atoi(olMinute),0);
}


void ConflictTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Konflikttyp")
            ilSortOrderEnumeratedValue = BY_TYPE;
        else if (olSortOrder[i] == "Gewichtung")
            ilSortOrderEnumeratedValue = BY_WEIGHT;
        else if (olSortOrder[i] == "Zuordnungseinheit")
            ilSortOrderEnumeratedValue = BY_ALID;
		else if (olSortOrder[i] == "Zeit")
			ilSortOrderEnumeratedValue = BY_TIME;
        else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }

	bmSortTimeAscending = GetUserData("TIMEORDER") == "YES" ? true : false;

#ifdef HAVE_TO_RUN_WITHOUT_VIEW_PROPERTY
    omSortOrder.Add(BY_GATA);
    omSortOrder.Add(BY_GATD);
#endif
}

BOOL ConflictTableViewer::IsPassFilter(CONFLICTENTRY  *prpConflictEntry, int ipCflIndex)
{
	if (!prpConflictEntry)
		return FALSE;

	if ((omStartTime > prpConflictEntry->Data[ipCflIndex].TimeOfConflict) ||
		(omEndTime < prpConflictEntry->Data[ipCflIndex].TimeOfConflict))
			return FALSE;

	CString olAloc,olAlid;
	ogConflicts.GetAllocationFromConflictType(prpConflictEntry,ipCflIndex,olAloc,olAlid);

	if ((prpConflictEntry->Data[ipCflIndex].Confirmed == TRUE) || ( !IsPassFilter(prpConflictEntry->Urno,prpConflictEntry->Data[ipCflIndex].Type,
					  prpConflictEntry->TypeOfObject,
					  olAloc,
					  olAlid)))
		return FALSE;
	else
		return TRUE;

}


BOOL ConflictTableViewer::IsPassFilter(long lpUrno,int ipType, int ipTypeOfObject,const char *pcpAloc, const char *pcpAlid)
{
    void *p;
	BOOL blRc;

	BOOL blIsTypeOk = omCMapForTypes.Lookup(ogConflictConfData[ipType].Name, p);
	BOOL blIsObjectOk = omCMapForTypeOfObject.Lookup((WORD) ipTypeOfObject, p);

    BOOL blIsAlocOk   = bmUseAllAlocs ? true : strlen(pcpAloc) == 0 ? true : omCMapForAloc.Lookup(CString(pcpAloc), p);

    BOOL blIsAlidOk = true;
	if (bmUseAllAlids.Lookup(pcpAloc,p) && p)
		blIsAlidOk = true;
	else if (strlen(pcpAlid) == 0)
		blIsAlidOk = true;
	else
	{
		CMapStringToPtr *polMap;
		if (omCMapForAlids.Lookup(pcpAloc,(void *&)polMap) && polMap != NULL)
		{
			blIsAlidOk = polMap->Lookup(pcpAlid,p);
		}		
	}

    BOOL blIsAlcdOK		= true;
    BOOL blIsAgentOK	= true;

	bool blIsFunctionOK = true;
	bool blIsPoolOK = true;

	if (ipTypeOfObject == CFLO_FLIGHT)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpUrno);
		if (prlFlight)
		{
			blIsAlcdOK	= omCMapForAlcd.Lookup(ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3), p);
			if (bmUseAllAgents || !ogBasicData.HandlingAgentsSupported())
				blIsAgentOK = true;
			else if (strlen(prlFlight->Hapx) > 0 && omCMapForAgents.Lookup(CString(prlFlight->Hapx), p))
				blIsAgentOK = true;
			else if (strlen(prlFlight->Hara) > 0 && omCMapForAgents.Lookup(CString(prlFlight->Hara), p))
				blIsAgentOK = true;
			else
				blIsAgentOK	= omCMapForAgents.Lookup(GetString(IDS_NOAGENT),p);
		}
	}
	else if (ipTypeOfObject == CFLO_JOB)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(lpUrno), *prlPJ = NULL;
		if(prlJob != NULL)
		{
			blIsPoolOK = bmUseAllPools;
			blIsFunctionOK = bmUseAllFunctions;

			if(!blIsPoolOK || !blIsFunctionOK)
			{
				JOBDATA *prlPJ = ogJobData.GetJobByUrno(prlJob->Jour);
				if(prlPJ != NULL)
				{
					if(omCMapForPools.Lookup(prlPJ->Alid, p))
					{
						blIsPoolOK = true;
					}

					CStringArray olFunctions;
					ogBasicData.GetFunctionsForPoolJob(prlPJ, olFunctions);
					int ilNumF = olFunctions.GetSize();
					for(int ilF = 0; ilF < ilNumF; ilF++)
					{
						if(omCMapForFunctions.Lookup(olFunctions[ilF], p))
						{
							blIsFunctionOK = true;
							break;
						}
					}
				}
			}
		}
	}

#ifdef HAVE_TO_RUN_WITHOUT_VIEW_PROPERTY
    return TRUE;    // always allow passing the filter
#endif

	blRc = (blIsAlocOk && blIsAlidOk && blIsAlcdOK && blIsAgentOK && blIsTypeOk && blIsObjectOk && blIsFunctionOK && blIsPoolOK);
    return (blRc);
}


int ConflictTableViewer::CompareConflicts(CONFLICTTABLE_LINEDATA *prpLine1, 
									    CONFLICTTABLE_LINEDATA *prpLine2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_TYPE:
			 ilCompareResult = strcmp(ogConflictConfData[prpLine1->Type].Name,
			     ogConflictConfData[prpLine2->Type].Name);
     //       ilCompareResult = (prpLine1->Type == prpLine2->Type)? 0:
     //           (prpLine1->Type > prpLine2->Type)? 1: -1;
            break;
        case BY_WEIGHT:
            ilCompareResult = (prpLine1->Weight == prpLine2->Weight)? 0:
                (prpLine1->Weight > prpLine2->Weight)? -1: 1;
            break;
        case BY_ALID:
            ilCompareResult = (prpLine1->Alid == prpLine2->Alid)? 0:
                (prpLine1->Alid > prpLine2->Alid)? 1: -1;
            break;
        case BY_TIME:
			ilCompareResult = (bmSortTimeAscending) ? 
				prpLine1->TimeOfConflict.GetTime() - prpLine2->TimeOfConflict.GetTime() :
				prpLine2->TimeOfConflict.GetTime() - prpLine1->TimeOfConflict.GetTime();
            break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   

}


/////////////////////////////////////////////////////////////////////////////
// ConflictTableViewer -- code specific to this class

void ConflictTableViewer::MakeLines()
{

	int ilCfltEntryCount = ogConflicts.omData.GetSize();
	if (ilCfltEntryCount <= 0)
		return;
	for ( int ilLc = 0; ilLc < ilCfltEntryCount; ilLc++)
	{
		CONFLICTENTRY *prlCfltEntry = &ogConflicts.omData[ilLc];
		int ilConflictCount = prlCfltEntry->Data.GetSize();
		for ( int ilCfl = 0; ilCfl < ilConflictCount; ilCfl++)
		{
			if (bmTableType == CFLTABLE_ATTENTION)
			{
				CONFLICTDATA *prlConflictData;
				if (omAttentionMap.Lookup((void *)&prlCfltEntry->Data[ilCfl],(void *&)prlConflictData) == FALSE)
				{
					continue;
				}
			}
			MakeLine(prlCfltEntry,ilCfl);
		}
	}
}

void ConflictTableViewer::MakeLine(CONFLICTENTRY  *prpConflictEntry, int ipCflIndex)
{

	if ((omStartTime > prpConflictEntry->Data[ipCflIndex].TimeOfConflict) || (omEndTime < prpConflictEntry->Data[ipCflIndex].TimeOfConflict))
		return;

	CString olAloc,olAlid;
	ogConflicts.GetAllocationFromConflictType(prpConflictEntry,ipCflIndex,olAloc,olAlid);

	CONFLICTDATA *prlConflictData = &prpConflictEntry->Data[ipCflIndex];
	if((prlConflictData->Confirmed == TRUE) || ( !IsPassFilter(prpConflictEntry->Urno,prlConflictData->Type, prpConflictEntry->TypeOfObject, olAloc, olAlid)))
		return;
 
 		
	CONFLICTTABLE_LINEDATA rlLine;

	rlLine.Type			= prlConflictData->Type;
	rlLine.Weight		= prlConflictData->Weight;
	rlLine.NameOfObject = prpConflictEntry->NameOfObject;
	rlLine.Alid			= olAlid;
	rlLine.TimeOfConflict = prlConflictData->TimeOfConflict;
	CString olExtraText = (prlConflictData->ExtraText.GetLength()) ? CString("  (") + prlConflictData->ExtraText + CString(")") : CString("");
	rlLine.Description	= ogConflictConfData[prlConflictData->Type].Dscr  + olExtraText;
	rlLine.TypeOfObject = prpConflictEntry->TypeOfObject;
	rlLine.Urno			= prpConflictEntry->Urno;
	rlLine.ReturnFlightType = prlConflictData->ReturnFlightType;

	CreateLine(&rlLine);

}

int ConflictTableViewer::CreateLine(CONFLICTTABLE_LINEDATA *prpLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareConflicts(prpLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareConflicts(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	CONFLICTTABLE_LINEDATA rlLine;
    rlLine = *prpLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}

/////////////////////////////////////////////////////////////////////////////
// ConflictTableViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ConflictTableViewer::UpdateDisplay()
{
	if (pomTable)
	{
		//pomTable->SetHeaderFields("Typ|Gew.|Objekt|Zuo.E.|Zeit|Beschreibung");
		pomTable->SetHeaderFields(GetString(IDS_STRING61562));
		//pomTable->SetFormatList("15|4|10|10|4|100");
		pomTable->SetFormatList("25|4|20|10|4|90");

		pomTable->ResetContent();
		for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
		{
			pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
		}

		pomTable->DisplayTable();
	}
}

CString ConflictTableViewer::Format(CONFLICTTABLE_LINEDATA *prpLine)
{
	char pclWeight[24];
	sprintf(pclWeight,"%d",ogConflictConfData[prpLine->Type].Weight);
	
	CString olLineText = CString(ogConflictConfData[prpLine->Type].Name) + "|";
	olLineText +=  CString(pclWeight) + "|";
	olLineText +=  prpLine->NameOfObject + "|";
	olLineText +=  prpLine->Alid + "|";
	olLineText +=  prpLine->TimeOfConflict.Format("%H%M") + "|";
	olLineText +=  prpLine->Description;

    return olLineText;
}

/*
void ConflictTableViewer::PrintView()
{
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->omConflictLines = omLines;
	prn->ConflictPrint();
	delete prn;
}
*/

void ConflictTableViewer::ConflictTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	ConflictTableViewer *polViewer = (ConflictTableViewer *)popInstance;

	if(ipDDXType == FLIGHT_SELECT)
	{
		FLIGHTDATA *prpFlight = (FLIGHTDATA *)vpDataPointer;
		if (!prpFlight)
			return;

		for (int i = 0; i < polViewer->omLines.GetSize(); i++)
		{
			CONFLICTTABLE_LINEDATA *prlLine = &polViewer->omLines[i];
			if (prlLine && prlLine->TypeOfObject == CFLO_FLIGHT)
			{
				if (prlLine->Urno == prpFlight->Urno)
				{
					if (polViewer->pomTable)
					{
						CListBox *polListBox = polViewer->pomTable->GetCTableListBox();
						if (polListBox)
							polListBox->SetSel(i);
						break;
					}
				}
			}
		}
	}
	else if(ipDDXType == CONFLICT_CHANGE)
	{
		polViewer->ProcessConflictChange((CONFLICTID *) vpDataPointer);
	}
	else if(ipDDXType == CONFLICT_DELETE)
	{
		polViewer->ProcessConflictDelete((CONFLICTID *) vpDataPointer);
	}

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{		
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(ConflictTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void ConflictTableViewer::ProcessConflictDelete(CONFLICTID *prpConflictId)
{
	if(prpConflictId != NULL && pomTable != NULL)
	{
		int ilLine = FindConflictLine(prpConflictId->Urno, prpConflictId->Type);
		if(ilLine != -1)
		{
			omLines.DeleteAt(ilLine);
			pomTable->DeleteTextLine(ilLine);
			pomTable->DisplayTable();
		}
	}
}

void ConflictTableViewer::ProcessConflictChange(CONFLICTID *prpConflictId)
{
	if(prpConflictId != NULL && pomTable != NULL)
	{
		int ilLine = FindConflictLine(prpConflictId->Urno, prpConflictId->Type);
		if(ilLine != -1)
		{
			omLines.DeleteAt(ilLine);
			pomTable->DeleteTextLine(ilLine);
		}
		CONFLICTENTRY *prlConflictEntry = ogConflicts.GetConflictEntryByUrno(prpConflictId->Urno);
		if(prlConflictEntry != NULL)
		{
			int ilConflictCount = prlConflictEntry->Data.GetSize();
			for(int ilCfl = 0; ilCfl < ilConflictCount; ilCfl++)
			{
				if(prlConflictEntry->Data[ilCfl].Type == prpConflictId->Type)
				{
					MakeLine(prlConflictEntry, ilCfl);
					int ilLine = FindConflictLine(prpConflictId->Urno, prpConflictId->Type);
					if(ilLine != -1)
					{
						pomTable->InsertTextLine(ilLine,Format(&omLines[ilLine]), &omLines[ilLine]);
						pomTable->DisplayTable();
					}
					break;
				}
			}
		}
	}
}

int ConflictTableViewer::FindConflictLine(long lpUrno, int ipType)
{
	int ilLine = -1;

	int ilNumLine = omLines.GetSize();
	for (int i = 0; i < ilNumLine; i++)
	{
		CONFLICTTABLE_LINEDATA *prlLine = &omLines[i];
		if(prlLine->Urno == lpUrno && prlLine->Type == ipType)
		{
			ilLine = i;
			break;
		}
	}

	return ilLine;
}

const char *ConflictTableViewer::AlocFromAllocGroupType(const char *pcpAllocGroupType)
{
	if (!pcpAllocGroupType)
		return NULL;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_PSTGROUP) == 0)
		return ALLOCUNITTYPE_PST;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_GATEGROUP) == 0)
		return ALLOCUNITTYPE_GATE;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_CICGROUP) == 0)
		return ALLOCUNITTYPE_CIC;
	else if (strcmp(pcpAllocGroupType,ALLOCUNITTYPE_REGNGROUP) == 0)
		return ALLOCUNITTYPE_REGN;
	else
		return "";
}
