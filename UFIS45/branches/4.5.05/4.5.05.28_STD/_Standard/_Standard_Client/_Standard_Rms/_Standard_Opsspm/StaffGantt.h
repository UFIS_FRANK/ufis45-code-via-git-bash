// StaffGantt.h : header file
//

#ifndef _STGANTT_H_
#define _STGANTT_H_


// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

#define FREEEMPSLIST_MENUITEM 12
// IDs 15-30 are used for conflict confirmation !!
#define EDITDLG_MENUITEM 31
#define ALLOCDLG_MENUITEM 32
#define PAUSEDLG_MENUITEM 33
#define FIXJOB_MENUITEM 34
#define INFORMJOB_MENUITEM 35
#define SPLITJOB_MENUITEM 36
#define COPYJOB_MENUITEM 37
#define CONFIRMJOB_MENUITEM 41
#define ENDJOB_MENUITEM 42
#define DELETEJOB_MENUITEM 44
#define STANDBY_MENUITEM 46
#define UNPAIDBREAK_MENUITEM 47
#define CHANGEJOB_MENUITEM 48
#define ABSENCE_MENUITEM 49
#define PRMASSIGNPDA_MENUITEM 50
#define PRMREMOVEPDA_MENUITEM 51
#define DEBUGINFO_MENUITEM 100
#define ACCEPT_ALL_CONFLICTS 15
#define MAXMENUITEM 15
#define EDITFASTLINK_MENUITEM 50
#define DELETEFASTLINK_MENUITEM (EDITFASTLINK_MENUITEM+MAXMENUITEM+1)
#define LASTJOBINFO_MENUITEM 200


/////////////////////////////////////////////////////////////////////////////
// StaffGantt window

class StaffGantt: public CListBox
{
// Operations
public:
	StaffGantt::StaffGantt(StaffDiagramViewer *popViewer = NULL, int ipGroup = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	~StaffGantt();

    void SetViewer(StaffDiagramViewer *popViewer, int ipGroup);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(int index1, int index2);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
	void DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem);
	void Draw3dLines(CDC *pDC, int itemID, const CRect &rcItem);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);
	void SetTopIndex(int ipGanttTopIndex);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);
	long GetActiveJobUrno();

// Attributes

private:
    StaffDiagramViewer *pomViewer;
    int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
    CFont *pomWIFBarFont;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
	CPen omBlackSolidPen1,omWhiteSolidPen1,omBlackSolidPen2, omYellowSolidPen1;
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

	long			lmActiveUrno;
	STAFF_BARDATA	rmActiveBar;
	STAFF_BKBARDATA rmActiveBkBar;
	BOOL			bmActiveBar;
	BOOL			bmActiveBkBar;

	int SelectBar(int ipGroupno, int ipLineno, int ipBarno);

	static CDWordArray omSelectedEmployees;
	void DeselectEmployees();
	void SelectEmployee(int ipGroupno, int ipHighlightLine);
	bool EmployeeIsSelected(int ipGroupno, int ipHighlightLine);


	int imTopIndex;

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over
	int imPoolJobBkBar;         // the current pool job bkbar that the user places mouse over

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint omLastClickedPosition;

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border

// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

private:
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
	void DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);

protected:
    // Generated message map functions
    //{{AFX_MSG(StaffGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG OnUserKeyDown(UINT wParam, LONG lParam);
	afx_msg LONG OnUserKeyUp(UINT wParam, LONG lParam);
	afx_msg void OnMenuFreeEmployees();
	afx_msg void OnMenuStaffBarFix();
	afx_msg void OnMenuStaffInform();
	afx_msg void OnMenuStaffEdit();
	afx_msg void OnMenuChangeJob();
	afx_msg void OnMenuStaffAssign();
	afx_msg void OnMenuPdaAssign();
	afx_msg void OnMenuPdaRemove();
    afx_msg void OnMenuStaffBreak();
	afx_msg void OnMenuPoolJobStandby();
	afx_msg void OnMenuPoolJobAbsence();
	afx_msg void OnMenuBreakUnpayed();
	afx_msg void OnMenuStaffConfirm();
	afx_msg void OnMenuStaffBarEnd();
	afx_msg void OnMenuStaffBarDelete();
	afx_msg void OnMenuStaffBarBreak();
	afx_msg void OnMenuStaffBarAcceptConflict(UINT);
	afx_msg void OnMenuStaffSplitJob();
	afx_msg void OnMenuStaffCopyJob();
	afx_msg void OnMenuEditFastLink(UINT ipId);
	afx_msg void OnMenuDeleteFastLink(UINT ipId);
	afx_msg void OnMenuDebugInfo();
	afx_msg void OnMenuLastJobInfo(); //PRF 8998
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno, int ipPoolJobBkBar);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point) const;
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1) const;
    int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1) const;
	int GetPoolJobFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1) const;
	long GetPoolJobUrnoFromPoint(CPoint opPoint) const;
	bool StaffGantt::GetPoolJobUrnosFromPoint(CPoint opPoint, CDWordArray &ropPoolJobUrnos);
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"
//-DTT Jul.25-----------------------------------------------------------
	BOOL IsDropOnPoolJob(CPoint point);
//----------------------------------------------------------------------
	BOOL IsStairCase(const CString &ropGate);

	CMapPtrToPtr omAcceptConflictsMap, omConflictToJobMap, omMenuItemToEquFastLinkMap;
private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
//-DTT Jul.25-----------------------------------------------------------
	CPoint omDropPosition;
//----------------------------------------------------------------------
	void DragPoolJobBegin(long lpPoolJobUrno);
	void DragPoolJobBegin(CCSPtrArray <JOBDATA> &ropPoolJobs, bool bpTeamAlloc);

	void DragJobBarBegin(int ipLineno, int ipBarno);
//-DTT Jul.25-----------------------------------------------------------
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropPoolJob(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropJobBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlight(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDemandBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropAloc(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect, CString opJobType, CString opAloc);
//	LONG ProcessDropGate(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//	LONG ProcessDropCciDesk(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFIDTable(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropEquipment(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	JOBDATA *GetJob(long lpJobUrno);

	CString DumpField(CString opFieldName, CString opFieldValue);
	CString DumpField(CString opFieldName, bool bpFieldValue);
	CString DumpField(CString opFieldName, CTime opFieldValue);
	CString DumpField(CString opFieldName, long lpFieldValue);
//----------------------------------------------------------------------
};

/////////////////////////////////////////////////////////////////////////////

#endif
