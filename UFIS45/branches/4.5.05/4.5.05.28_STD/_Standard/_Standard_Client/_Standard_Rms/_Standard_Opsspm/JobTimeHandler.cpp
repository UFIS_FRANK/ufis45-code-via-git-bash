// JobTimeHandler.cpp : implementation file
// 

#include <stdafx.h>
#include <JobTimeHandler.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CedaJobData.h>
#include <CedaSprData.h>
#include <Dataset.h>
#include <BasicData.h>
#include <DJobChange.h>
#include <PrivList.h>

const UINT	ucPlannedConfirmation = 560;
const UINT	ucManualConfirmation  = 561;
const UINT	ucCurrentConfirmation = 562;

const UINT	ucPlannedEnded = 570;
const UINT	ucManualEnded  = 571;
const UINT	ucCurrentEnded = 572;

int		JobTimeHandler::simInstall = -1;
UINT	JobTimeHandler::simDefaultConfirmation = -1;
UINT	JobTimeHandler::simDefaultEnded = -1;



JobTimeHandler::JobTimeHandler(JOBDATA *prpJob, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd)
{
	ASSERT(prpJob);
	CCSPtrArray <JOBDATA> olJobs;
	olJobs.Add(prpJob);
	Init(olJobs, popMenu, ipPosConfirm, ipPosEnd);
}

JobTimeHandler::JobTimeHandler(CCSPtrArray <JOBDATA> &ropJobs, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd)
{
	Init(ropJobs, popMenu, ipPosConfirm, ipPosEnd);
}

void JobTimeHandler::Init(CCSPtrArray <JOBDATA> &ropJobs, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd)
{
	ASSERT(popMenu);
	pomMenu			= popMenu;
	pomConfirmMenu	= NULL;
	pomEndMenu		= NULL;
	int ilNumJobs = ropJobs.GetSize();
	ASSERT(ilNumJobs>0);
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		omJobUrnos.Add(ropJobs[ilJob].Urno);
	}

	umCmdConfirmed  = popMenu->GetMenuItemID(ipPosConfirm);
	umCmdEnded		= popMenu->GetMenuItemID(ipPosEnd);

	// check in BDPS-SEC, if we must install our handler or not
	if (simInstall == -1)
	{
		char clStat = ogPrivList.GetStat("JOBTIMEHANDLER");
		if (clStat == '1')
			simInstall = 1;
		else
			simInstall = 0;

		// read default values from ceda.ini -> later use setup dialogue
		char pclConfigPath[512];
		char pclConfirmDefault[256];
		char pclEndedDefault[256];

	    // retrieve the configuration path from OS environment
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcgAppName,"JOBCONFIRMDEFAULT","PLANNED",pclConfirmDefault, sizeof pclConfirmDefault, pclConfigPath);
		if (stricmp(pclConfirmDefault,"PLANNED") == 0)
		{
			simDefaultConfirmation = ucPlannedConfirmation;
		}
		else if (stricmp(pclConfirmDefault,"MANUAL") == 0)
		{
			simDefaultConfirmation = ucManualConfirmation;
		}
		else if (stricmp(pclConfirmDefault,"CURRENT") == 0)
		{
			simDefaultConfirmation = ucCurrentConfirmation;
		}
		else
		{
			simDefaultConfirmation = ucPlannedConfirmation;
		}

		GetPrivateProfileString(pcgAppName,"JOBFINISHDEFAULT","PLANNED",pclEndedDefault, sizeof pclEndedDefault, pclConfigPath);
		if (stricmp(pclEndedDefault,"PLANNED") == 0)
		{
			simDefaultEnded = ucPlannedEnded;
		}
		else if (stricmp(pclEndedDefault,"MANUAL") == 0)
		{
			simDefaultEnded = ucManualEnded;
		}
		else if (stricmp(pclEndedDefault,"CURRENT") == 0)
		{
			simDefaultEnded = ucCurrentEnded;
		}
		else
		{
			simDefaultEnded = ucPlannedEnded;
		}
	}


	// create sub menus, if neccessary
	if (simInstall)
	{

		UINT ulState = popMenu->GetMenuState(ipPosConfirm,MF_BYPOSITION);
		if (!(ulState & MF_DISABLED) && !(ulState & MF_GRAYED))
		{
			pomConfirmMenu	= new CMenu;
			pomConfirmMenu->CreatePopupMenu();
			pomConfirmMenu->AppendMenu(MF_STRING,ucPlannedConfirmation,GetString(IDS_STRING62506));
			pomConfirmMenu->AppendMenu(MF_STRING,ucManualConfirmation,GetString(IDS_STRING62507));
			pomConfirmMenu->AppendMenu(MF_STRING,ucCurrentConfirmation,GetString(IDS_STRING62508));
			pomConfirmMenu->SetDefaultItem(simDefaultConfirmation);
			imPosConfirm = ipPosConfirm+1;
			imPosEnd	 = ipPosEnd+2;
		}
		else
		{
			imPosConfirm = -1;
			imPosEnd	 = ipPosEnd+1;
		}

		ulState = popMenu->GetMenuState(ipPosEnd,MF_BYPOSITION);
		if (!(ulState & MF_DISABLED) && !(ulState & MF_GRAYED))
		{
			pomEndMenu	= new CMenu;
			pomEndMenu->CreatePopupMenu();
			pomEndMenu->AppendMenu(MF_STRING,ucPlannedEnded,GetString(IDS_STRING62506));
			pomEndMenu->AppendMenu(MF_STRING,ucManualEnded,GetString(IDS_STRING62507));
			pomEndMenu->AppendMenu(MF_STRING,ucCurrentEnded,GetString(IDS_STRING62508));
			pomEndMenu->SetDefaultItem(simDefaultEnded);
		}
		else
		{
			imPosEnd	 = -1;
		}
	}
}

JobTimeHandler::~JobTimeHandler()
{
	delete pomConfirmMenu;
	delete pomEndMenu;
}

bool JobTimeHandler::InstallHandler()
{
	if (!simInstall)
		return true;

				
	if (pomConfirmMenu && !pomMenu->InsertMenu(imPosConfirm,MF_BYPOSITION|MF_POPUP|MF_STRING,(UINT)pomConfirmMenu->m_hMenu,GetString(IDS_STRING62505)))
		return false;
		
	if (pomEndMenu && !pomMenu->InsertMenu(imPosEnd,MF_BYPOSITION|MF_POPUP|MF_STRING,(UINT)pomEndMenu->m_hMenu,GetString(IDS_STRING62505)))
		return false;
		
	return true;							
}

bool JobTimeHandler::TrackPopupMenu(const CPoint& ropIp,CWnd *popParent)
{
	if (!popParent)
		return false;

	if (!InstallHandler())
		return false;

	bool blDisableMenuItems = false;
	if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(omJobUrnos))
	{
		if(pomConfirmMenu)
			ogBasicData.DisableAllMenuItems(*pomConfirmMenu);
		if(pomEndMenu)
			ogBasicData.DisableAllMenuItems(*pomEndMenu);
	}

	UINT ilItem = pomMenu->TrackPopupMenu(TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,ropIp.x,ropIp.y,popParent);

	if (ilItem == umCmdConfirmed)	// confirm default pressed -> use default value
	{
		ilItem = simDefaultConfirmation;
	}
	else if (ilItem == umCmdEnded)	// ended default pressed -> use default value
	{
		ilItem = simDefaultEnded;
	}

	CTime olStart = TIMENULL, olEnd = TIMENULL;
	CCSPtrArray <JOBDATA> olJobs;

	bool blRc = true;
	switch(ilItem)
	{
		case 0 :					// cancel or error
			;			
			break;
		case ucPlannedConfirmation: // confirm planned pressed		
		case ucManualConfirmation:  // confirm manual  pressed		
		case ucCurrentConfirmation:  // confirm current pressed		
			blRc = GetJobs(olJobs, olStart, olEnd, 'P');
			break;
		case ucPlannedEnded:		// confirm planned pressed		
		case ucManualEnded:			// confirm manual  pressed		
		case ucCurrentEnded:		// confirm current pressed		
			blRc = GetJobs(olJobs, olStart, olEnd, 'C');
			break;
		default :	// send all other command messages to parent window
			;
			break;
	}

	if(!blRc) return false; // no jobs found to confirm or end

	int ilNumJobs = olJobs.GetSize();


	switch(ilItem)
	{
	case 0 :					// cancel or error
		;			
	break;
	case ucPlannedConfirmation: // confirm planned pressed		
		{
			ogDataSet.ConfirmJob(popParent,olJobs);
		}
	break;
	case ucManualConfirmation:  // confirm manual  pressed		
		{
			CStringArray olEmpNames;
			GetEmpNamesForJobs(olJobs, olEmpNames, 'P');
			DJobChange olDlg(popParent,olEmpNames,olStart,olEnd,false,true);
			if (olDlg.DoModal() == IDOK)
			{
				ogDataSet.ConfirmJob(popParent,olJobs,olDlg.m_Acfr);
			}
		}
	break;
	case ucCurrentConfirmation:  // confirm current pressed		
		{
			ogDataSet.ConfirmJob(popParent,olJobs,ogBasicData.GetTime());
		}
	break;
	case ucPlannedEnded:		// confirm planned pressed		
		{
			ogDataSet.FinishJob(olJobs);
		}
	break;
	case ucManualEnded:			// confirm manual  pressed		
		{
			CStringArray olEmpNames;
			GetEmpNamesForJobs(olJobs, olEmpNames, 'C');
			DJobChange olDlg(popParent,olEmpNames,olStart,olEnd,true,false);
			if (olDlg.DoModal() == IDOK)
			{
				ogDataSet.FinishJob(olJobs,olDlg.m_Acto);
			}
		}
	break;
	case ucCurrentEnded:		// confirm current pressed		
		{
			ogDataSet.FinishJob(olJobs,ogBasicData.GetTime());
		}
	break;
	default :	// send all other command messages to parent window
		{
			WORD ulCmd = HIWORD(0) + LOWORD(ilItem);
			popParent->SendMessage(WM_COMMAND,ulCmd,NULL);
		}
	}

	return true;
}

void JobTimeHandler::GetEmpNamesForJobs(CCSPtrArray <JOBDATA> &ropJobs, CStringArray &ropEmpNames, const char cpStat)
{
	JOBDATA *prlJob = NULL;
	EMPDATA *prlEmp = NULL;
	CString olEmpName;
	int ilNumJobs = ropJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		prlJob = &ropJobs[ilJob];
		if(*prlJob->Stat == cpStat && (prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno)) != NULL)
		{
			ropEmpNames.Add(ogEmpData.GetEmpName(prlEmp));
		}
	}
}

bool JobTimeHandler::GetJobs(CCSPtrArray <JOBDATA> &ropJobs, CTime &ropStart, CTime &ropEnd, const char cpStat, bool bpReset /*=true*/)
{
	if(bpReset)
	{
		ropStart = ropEnd = TIMENULL;
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	int ilNumJobs = omJobUrnos.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		if((prlJob = ogJobData.GetJobByUrno(omJobUrnos[ilJob])) != NULL && *prlJob->Stat == cpStat)
		{
			ropJobs.Add(prlJob);
			if(ropStart == TIMENULL || ropStart > prlJob->Acfr)
			{
				ropStart = prlJob->Acfr;
			}
			if(ropEnd == TIMENULL || ropEnd < prlJob->Acto)
			{
				ropEnd = prlJob->Acto;
			}
		}
	}
	if(ropJobs.GetSize() <= 0 || ropStart == TIMENULL || ropEnd == TIMENULL)
		return false;
	else
		return true;
}
