#ifndef _CEDARLODATA_H_
#define _CEDARLODATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

 // Data fields from table RLOTAB - Resource location
struct RloDataStruct 
{
	long	Urno;		// Unique record number
	char	Gtab[9];	// group table and field
	char	Reft[9];	// Reference table and field
	long	Rloc;		// Resource location
	long	Urud;		// 

	RloDataStruct(void)
	{
		Urno = 0L;
		strcpy(Gtab,"");
		strcpy(Reft,"");
		Rloc = 0L;
		Urud = 0L;
	}
};

typedef struct RloDataStruct RLODATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRloData: public CCSCedaData
{
private:
    CCSPtrArray<RLODATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUrudMap;

public:
    CedaRloData();
    ~CedaRloData();
	void ClearAll();
	BOOL ReadRloData();
	BOOL AddRloInternal(RLODATA *prpRlo);
	RLODATA *GetRloByUrno(long lpUrno);
	RLODATA *GetRloByUrud(long lpUrud);
	int		 GetCountOfRecords();
	CString GetTableName(void);
};

extern CedaRloData ogRloData;
#endif
