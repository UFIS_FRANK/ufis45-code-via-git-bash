// OtherColoursDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <OtherColoursDetailDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDetailDlg dialog


COtherColoursDetailDlg::COtherColoursDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COtherColoursDetailDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COtherColoursDetailDlg)
	m_ColourTitle = _T("");
	m_CfgNameTitle = _T("");
	m_CfgName = _T("");
	m_Enabled = FALSE;
	//}}AFX_DATA_INIT
}


void COtherColoursDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COtherColoursDetailDlg)
	DDX_Text(pDX, IDC_COLOURTITLE, m_ColourTitle);
	DDX_Text(pDX, IDC_CFGNTITLE, m_CfgNameTitle);
	DDX_Text(pDX, IDC_CFGN, m_CfgName);
	DDX_Check(pDX, IDC_STATUS, m_Enabled);
	DDX_Control(pDX, IDC_COLOUR, omColourButton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COtherColoursDetailDlg, CDialog)
	//{{AFX_MSG_MAP(COtherColoursDetailDlg)
	ON_BN_CLICKED(IDC_COLOUR, OnColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDetailDlg message handlers

BOOL COtherColoursDetailDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_OTHCOLOURSDETAILDLG));
	m_ColourTitle = GetString(IDS_OTHCOLOURS_COLOURTITLE);
	GetDlgItem(IDC_STATUS)->SetWindowText(GetString(IDS_OTHCOLOURS_STATUS));
	m_CfgNameTitle = GetString(IDS_FUNCCOLOURS_NAME);
	omColourButton.SetColors(omColour,omColour,omColour);
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COtherColoursDetailDlg::SetData(CString opCfgn, bool bpEnabled, COLORREF opColour)
{
	m_CfgName = opCfgn;
	m_Enabled = bpEnabled ? TRUE : FALSE;
	omColour = opColour;
}

void COtherColoursDetailDlg::GetData(CString &ropCfgn, bool &rbpEnabled, COLORREF &ropColour)
{
	ropCfgn = m_CfgName;
	rbpEnabled = m_Enabled ? true : false;
	ropColour = omColour;
}

void COtherColoursDetailDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		CDialog::OnOK();
	}
}

void COtherColoursDetailDlg::OnColour() 
{
	CColorDialog olColorDlg(omColour);
	if (olColorDlg.DoModal() == IDOK)
	{
		omColour = olColorDlg.GetColor();
		omColourButton.SetColors(omColour,omColour,omColour);
		omColourButton.Invalidate(TRUE);
	}
}
