// stfdiagr.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <StaffDiagramGroupPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_GROUPKEYS	3
static CString ogGroupKeys[NUMBER_OF_GROUPKEYS] =
	{ "Pool", "Team", "Schichtcode" };

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramGroupPage property page

IMPLEMENT_DYNCREATE(StaffDiagramGroupPage, CPropertyPage)

StaffDiagramGroupPage::StaffDiagramGroupPage() : CPropertyPage(StaffDiagramGroupPage::IDD)
{
	//{{AFX_DATA_INIT(StaffDiagramGroupPage)
	m_GroupBy = -1;
	m_GroupView = FALSE;
	m_HideDelegatedGroups = FALSE;
	m_HideAbsentEmps = FALSE;
	m_HideFidEmps = FALSE;
	m_HideStandbyEmps = FALSE;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING61618);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

StaffDiagramGroupPage::~StaffDiagramGroupPage()
{
}

void StaffDiagramGroupPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_GroupBy = GetGroupId(omGroupBy);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StaffDiagramGroupPage)
	DDX_Control(pDX, IDC_GROUPVIEW, m_GroupViewCtrl);
	DDX_Control(pDX, IDC_HIDEDELEGATIONS, m_HideDelegatedGroupsCtrl);
	DDX_Control(pDX, IDC_HIDEABSENTEMPS, m_HideAbsentEmpsCtrl);
	DDX_Control(pDX, IDC_HIDEFIDEMPS, m_HideFidEmpsCtrl);
	DDX_Control(pDX, IDC_HIDESTANDBYEMPS, m_HideStandbyEmpsCtrl);
	DDX_Radio(pDX, IDC_RADIO1, m_GroupBy);
	DDX_Check(pDX, IDC_GROUPVIEW, m_GroupView);
	DDX_Check(pDX, IDC_HIDEDELEGATIONS, m_HideDelegatedGroups);
	DDX_Check(pDX, IDC_HIDEABSENTEMPS, m_HideAbsentEmps);
	DDX_Check(pDX, IDC_HIDEFIDEMPS, m_HideFidEmps);
	DDX_Check(pDX, IDC_HIDESTANDBYEMPS, m_HideStandbyEmps);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omGroupBy = GetGroupKey(m_GroupBy);
	}
}


BEGIN_MESSAGE_MAP(StaffDiagramGroupPage, CPropertyPage)
	//{{AFX_MSG_MAP(StaffDiagramGroupPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramGroupPage message handlers

BOOL StaffDiagramGroupPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramGroupPage -- helper routines

int StaffDiagramGroupPage::GetGroupId(const char *pcpGroupKey)
{
	for (int i = 0; i < NUMBER_OF_GROUPKEYS; i++)
		if (ogGroupKeys[i] == pcpGroupKey)
			return i;

	// If there is no groupping matched, assume first groupping
	return 0;
}

CString StaffDiagramGroupPage::GetGroupKey(int ipGroupId)
{
	if (0 <= ipGroupId && ipGroupId <= NUMBER_OF_GROUPKEYS-1)
		return ogGroupKeys[ipGroupId];

	return "";	// invalid groupping id
}

BOOL StaffDiagramGroupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
//	SetWindowText(GetString(IDS_STRING61618)); 

	CWnd *polWnd = GetDlgItem(IDC_RADIO1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_SDGP_POOL));
	}
	polWnd = GetDlgItem(IDC_RADIO2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61611));
	}
	polWnd = GetDlgItem(IDC_RADIO3); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61612));
	}
	if(ogBasicData.bmDisplayTeams)
	{
		m_GroupViewCtrl.SetWindowText(GetString(IDS_GROUPVIEW));
		m_HideDelegatedGroupsCtrl.SetWindowText(GetString(IDS_HIDEDELEGATEDTEAMS));
	}
	else
	{
		m_GroupViewCtrl.ShowWindow(SW_HIDE);
		m_HideDelegatedGroupsCtrl.ShowWindow(SW_HIDE);
	}

	m_HideAbsentEmpsCtrl.SetWindowText(GetString(IDS_HIDEABSENTEMPS));
	m_HideFidEmpsCtrl.SetWindowText(GetString(IDS_HIDEFIDEMPS));
	m_HideStandbyEmpsCtrl.SetWindowText(GetString(IDS_HIDESTANDBYEMPS));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
