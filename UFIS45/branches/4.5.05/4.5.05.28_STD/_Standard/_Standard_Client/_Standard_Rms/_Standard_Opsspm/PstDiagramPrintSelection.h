// PstDiagramPrintSelection.h : header file
//
#ifndef _PSTDIAGRAMPRINTSELECTION_H_
#define _PSTDIAGRAMPRINTSELECTION_H_

#include <DruckAuswahl.h>
/////////////////////////////////////////////////////////////////////////////
// PstDiagramPrintSelection dialog

class PstDiagramPrintSelection : public CDruckAuswahl
{
// Construction
public:
	PstDiagramPrintSelection(CWnd* pParent = NULL);   // standard constructor

// Dialog Data

	//{{AFX_DATA(PstDiagramPrintSelection)
//	enum { IDD = IDD_DRUCKAUSW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PstDiagramPrintSelection)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PstDiagramPrintSelection)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif _PSTDIAGRAMPRINTSELECTION_H_