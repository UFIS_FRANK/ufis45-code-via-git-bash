// CedaSprData.h - Clocking-on/off times
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaSprData.h>
#include <BasicData.h>
#include <PrivList.h>

void  ProcessSprCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaSprData::CedaSprData()
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(SPRDATA, SprDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_LONG(Ustf,"USTF")
        FIELD_CHAR_TRIM(Fcol,"FCOL")
        FIELD_DATE(Acti,"ACTI")
        FIELD_CHAR_TRIM(Peno,"PKNO")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SprDataRecInfo)/sizeof(SprDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SprDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"SPRTAB");
    pcmFieldList = "URNO,USTF,FCOL,ACTI,PKNO,USEC,CDAT,USEU,LSTU";

	ogCCSDdx.Register((void *)this,BC_SPR_CHANGE,CString("SPRDATA"), CString("Spr changed"),ProcessSprCf);
	ogCCSDdx.Register((void *)this,BC_SPR_DELETE,CString("SPRDATA"), CString("Spr deleted"),ProcessSprCf);

	omSplitShiftMinClockoutTimeBeforeShiftEnd = CTimeSpan(0, 7, 0, 0);
	bmSplitShiftMinDuration = (9 * 60) + 45; // 9 hours 45 mins
	bmSplitShiftMinBreakTime = (2 * 60) + 45; // 2 hours 45 mins

	bmNoChangesAllowedAfterClockout = false;
}
 
CedaSprData::~CedaSprData()
{
	TRACE("CedaSprData::~CedaSprData called\n");

	ogCCSDdx.UnRegisterAll();

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omUstfMap.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUstfMap.RemoveAll();
	omUrnoMap.RemoveAll();

	omData.DeleteAll();
}

void  ProcessSprCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaSprData *)popInstance)->ProcessSprBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaSprData::ProcessSprBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSprData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlSprData);

	long llUrno = GetUrnoFromSelection(prlSprData->Selection);
	if(llUrno == 0L && ipDDXType == BC_SPR_CHANGE)
	{
		SPRDATA rlSpr;
		GetRecordFromItemList(&omRecInfo,&rlSpr,prlSprData->Fields,prlSprData->Data);
		llUrno = rlSpr.Urno;
	}
	
	
	SPRDATA *prlSpr = GetSprByUrno(llUrno);
	SPRDATA rlOriginalSpr;
	if(ipDDXType == BC_SPR_CHANGE && prlSpr != NULL)
	{
		// before reading the newly broadcasted data into the record found,
		// convert the existing dates to UTC so that PrepareDataAfterRead
		// converts them and any new dates from the broadcast to local
		ConvertDatesToUtc(prlSpr);
		GetRecordFromItemList(prlSpr,prlSprData->Fields,prlSprData->Data);
		ConvertDatesToLocal(prlSpr);
		ogCCSDdx.DataChanged((void *)this,SPR_CHANGE,(void *)prlSpr);
	}
	else if(ipDDXType == BC_SPR_CHANGE && prlSpr == NULL)
	{
		// insert a new demand
		SPRDATA rlSpr;
		GetRecordFromItemList(&rlSpr,prlSprData->Fields,prlSprData->Data);
		AddInternal(rlSpr);
		ogCCSDdx.DataChanged((void *)this,SPR_CHANGE,(void *)prlSpr);
	}
	else if(ipDDXType == BC_SPR_DELETE && prlSpr != NULL)
	{
		// delete demand
		rlOriginalSpr = *prlSpr;
		DeleteInternal(prlSpr->Urno);
		ogCCSDdx.DataChanged((void *)this,SPR_DELETE,(void *)rlOriginalSpr.Urno);
	}

}

CCSReturnCode CedaSprData::ReadSprData(CTime opStartTime, CTime opEndTime)
{
	char pclCom[10] = "RT";
    char pclWhere[1000] = "";
	CCSReturnCode ilRc = RCSuccess;

	if(opStartTime != TIMENULL && opEndTime != TIMENULL)
	{
		CTimeSpan olOffset(0, 12, 0, 0);
		CTime olStart = opStartTime - olOffset;
		CTime olEnd = opEndTime + olOffset;
		sprintf(pclWhere, "WHERE ACTI > '%s' AND ACTI < '%s'", olStart.Format("%Y%m%d%H%M%S"), olEnd.Format("%Y%m%d%H%M%S"));
	}

    if((ilRc = CedaAction2(pclCom, pclWhere)) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaSprData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
        return RCFailure;
	}

	SPRDATA rlSprData;
    for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		if ((ilRc = GetBufferRecord2(ilLc,&rlSprData)) == RCSuccess)
		{
			rlSprData.emState = SPRDATA::UNCHANGED;
			AddInternal(rlSprData);
		}
	}

	bmNoChangesAllowedAfterClockout = (ogPrivList.GetStat("GENERAL RO AFTER CLOCKOUT") == '1') ? true : false;

	ogBasicData.Trace("CedaSprData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SPRDATA), pclWhere);
    return RCSuccess;
}


SPRDATA *CedaSprData::AddInternal(SPRDATA &rrpSpr)
{
	SPRDATA *prlSpr = new SPRDATA;
	*prlSpr = rrpSpr;

	ConvertDatesToLocal(prlSpr);
	omData.Add(prlSpr);
	omUrnoMap.SetAt((void *)prlSpr->Urno,prlSpr);

	CMapPtrToPtr *polSingleMap;
	if(omUstfMap.Lookup((void *) prlSpr->Ustf, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prlSpr->Urno,prlSpr);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlSpr->Urno,prlSpr);
		omUstfMap.SetAt((void *)prlSpr->Ustf,polSingleMap);
	}

	return prlSpr;
}

// if the clock on/off times are within the shift +/- tolerance then these times belong to the emp specified in lpUstf
CTime CedaSprData::GetTimeForUstf(long lpUstf, const char *pcpFcol, CTime opShiftStart, CTime opShiftEnd)
{
	SPRDATA* prlSpr = GetSprByUstf(lpUstf,pcpFcol,opShiftStart,opShiftEnd);
	if (!prlSpr)
		return TIMENULL;
	else
		return prlSpr->Acti;
}

SPRDATA *CedaSprData::GetSprByUstf(long lpUstf, const char *pcpFcol, CTime opShiftStart, CTime opShiftEnd)
{
	if(opShiftStart != TIMENULL && opShiftEnd != TIMENULL)
	{
		CTime olStart = opShiftStart - ogBasicData.omShiftStartTolerance;
		CTime olEnd = opShiftEnd + ogBasicData.omShiftEndTolerance;

		CMapPtrToPtr *polSingleMap;
		SPRDATA *prlSpr = NULL;
		long llUrno;

		if(omUstfMap.Lookup((void *)lpUstf, (void *& )polSingleMap))
		{
			SPRDATA *prlBestMatch=NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *& ) prlSpr);
				if (prlSpr != NULL && !strcmp(prlSpr->Fcol,pcpFcol) && prlSpr->Acti >= olStart && prlSpr->Acti <= olEnd)
				{
					if (prlBestMatch == NULL)
					{
						prlBestMatch = prlSpr;
					}
					else if (prlSpr->Lstu > prlBestMatch->Lstu)
					{
						prlBestMatch = prlSpr;
					}
				}
			}
			
			return prlBestMatch;
		}
	}

	return NULL;
}

BOOL CedaSprData::IsValidTime(CTime opShiftStart, CTime opShiftEnd,CTime opActi)
{
	CTime olStart = opShiftStart - ogBasicData.omShiftStartTolerance;
	CTime olEnd   = opShiftEnd + ogBasicData.omShiftEndTolerance;
	return opActi >= olStart && opActi <= olEnd;
}

void CedaSprData::GetValidTime(CTime opShiftStart, CTime opShiftEnd,CTime&opStart,CTime& opEnd)
{
	opStart = opShiftStart - ogBasicData.omShiftStartTolerance;
	opEnd   = opShiftEnd + ogBasicData.omShiftEndTolerance;
}

CString CedaSprData::GetTableName(void)
{
	return CString(pcmTableName);
}

SPRDATA *CedaSprData::GetSprByUrno(long lpUrno)
{
	SPRDATA  *prlSpr = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpr);
	return prlSpr;
}


void CedaSprData::DeleteInternal(long lpUrno)
{
	int ilJobCount = omData.GetSize();
	for (int ilLc = ilJobCount-1; ilLc >= 0; ilLc--)
	{
		SPRDATA *prlSpr = &omData[ilLc];
		if (prlSpr->Urno == lpUrno)
		{
			omUrnoMap.RemoveKey((void *)prlSpr->Urno);
			CMapPtrToPtr *polSingleMap;
			if(omUstfMap.Lookup((void *) prlSpr->Ustf, (void *&) polSingleMap))
			{
				polSingleMap->RemoveKey((void *)prlSpr->Urno);
				if(polSingleMap->GetCount() <= 0)
				{
					omUstfMap.RemoveKey((void *)prlSpr->Ustf);
					delete polSingleMap;
				}
			}
			omData.DeleteAt(ilLc);
		}
	}
	return;
}


void CedaSprData::ConvertDatesToUtc(SPRDATA *prpSpr)
{
	if(prpSpr != NULL)
	{
		ogBasicData.ConvertDateToUtc(prpSpr->Acti);
		ogBasicData.ConvertDateToUtc(prpSpr->Cdat);
		ogBasicData.ConvertDateToUtc(prpSpr->Lstu);
	}
}

void CedaSprData::ConvertDatesToLocal(SPRDATA *prpSpr)
{
	if(prpSpr != NULL)
	{
		ogBasicData.ConvertDateToLocal(prpSpr->Acti);
		ogBasicData.ConvertDateToLocal(prpSpr->Cdat);
		ogBasicData.ConvertDateToLocal(prpSpr->Lstu);
	}
}


BOOL CedaSprData::InitNewSprRecord(long lpUstf, const char *pcpFcol, CTime opActi,SPRDATA& rpData)
{
	ASSERT(pcpFcol);

	rpData.Urno = ogBasicData.GetNextUrno();
	rpData.Ustf = lpUstf;
	strcpy(rpData.Fcol,pcpFcol);
	rpData.Acti = opActi;
	return TRUE;
}

BOOL CedaSprData::AddSprRecord(const SPRDATA& rpData,BOOL bpSendDDX)
{
	SPRDATA rlData(rpData);
	ConvertDatesToUtc(&rlData);
	rlData.emState = SPRDATA::NEW;
	SPRDATA *prlData = AddInternal(rlData);
	ASSERT(prlData);

	BOOL blResult = SaveSprRecord(prlData);

	if (blResult && bpSendDDX)
	{
		ogCCSDdx.DataChanged((void *)this,SPR_CHANGE,(void *)prlData);
	}
	
	return blResult;
}


BOOL CedaSprData::SaveSprRecord(SPRDATA *prpSpr,SPRDATA *prpOldSpr)
{
	ASSERT(prpSpr);

	if (prpSpr->emState == SPRDATA::UNCHANGED)
		return TRUE;

	// set lstu/cdat etc
	CTime olCurrTime = ogBasicData.GetTime();
	CString olUserText;
	olUserText.Format("%s %s %s",ogUsername,"CedaSprData",ogBasicData.GetVersion()); // username + version number and function number (which function did the change)

	prpSpr->Lstu = olCurrTime;
	strcpy(prpSpr->Useu,olUserText.Left(31));

	if(prpSpr->Cdat == TIMENULL || strlen(prpSpr->Usec) <= 0)
	{
		prpSpr->Cdat = olCurrTime;
		strcpy(prpSpr->Usec,olUserText.Left(31));
	}

	ConvertDatesToUtc(prpSpr);
	if (prpOldSpr)
		ConvertDatesToUtc(prpOldSpr);

	CString olListOfData;
	SPRDATA rlSpr = *prpSpr;
	char pclData[2048]		= "";
	char pclSelection[256]	= "";
	char pclCmd[128]		= "";
	int	 olRc = 0;

	switch(prpSpr->emState)
	{
	case SPRDATA::NEW :
		MakeCedaData(&omRecInfo,olListOfData,&rlSpr);
		strcpy(pclData,olListOfData);
		strcpy(pclCmd,"IRT");
		ogBasicData.LogCedaAction(pcmTableName,"IRT","", pcmFieldList, pclData);

		if(!(olRc = CedaAction("IRT","","",pclData)))
		{
			ogBasicData.LogCedaError("CedaSprData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
		}
		else
			prpSpr->emState = SPRDATA::UNCHANGED;
	break;
	case SPRDATA::CHANGED :
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpSpr->Urno);
		strcpy(pclCmd,"URT");

		// update all fields in JOBTAB for this record
		MakeCedaData(&omRecInfo,olListOfData,&rlSpr);
		strcpy(pclData,olListOfData);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		olRc = CedaAction(pclCmd,pclSelection,"",pclData);
	
		if(!olRc)
		{
			ogBasicData.LogCedaError("CedaSprData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
			prpSpr->emState = SPRDATA::UNCHANGED;
	break;
	case SPRDATA::DELETED :
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpSpr->Urno);
		strcpy(pclCmd,"DRT");

		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		olRc = CedaAction(pclCmd,pclSelection);
		if (!olRc)
		{
			ogBasicData.LogCedaError("CedaSprData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	break;
	}

	ConvertDatesToLocal(prpSpr);
	if (prpOldSpr)
		ConvertDatesToLocal(prpOldSpr);

	return olRc != 0;
}

BOOL CedaSprData::ChangeTime(long lpSpr,CTime opActi)
{
	SPRDATA *prlSpr = GetSprByUrno(lpSpr);
	if (!prlSpr)
		return FALSE;

	CString olUsec(prlSpr->Usec);
	if (olUsec.Find("CedaSprData") == -1)	// created by interface
	{
		SPRDATA rlNewSpr(*prlSpr);
		rlNewSpr.Urno = ogBasicData.GetNextUrno();
		rlNewSpr.Acti = opActi;
		return AddSprRecord(rlNewSpr,TRUE);
	}
	else
	{
		SPRDATA rlOldSpr(*prlSpr);

		prlSpr->Acti = opActi;
		prlSpr->emState = SPRDATA::CHANGED;
		if (SaveSprRecord(prlSpr,&rlOldSpr))
		{
			ogCCSDdx.DataChanged((void *)this,SPR_CHANGE,(void *)prlSpr);
			return TRUE;
		}
		else
		{
			*prlSpr = rlOldSpr;
			return FALSE;
		}
	}
}

BOOL CedaSprData::DeleteSprRecord(long lpSpr,BOOL IsFromBC)
{
	SPRDATA *prlSpr = GetSprByUrno(lpSpr);
	if (!prlSpr)
		return FALSE;
	SPRDATA rlSpr(*prlSpr);

	ogCCSDdx.DataChanged((void *)this,SPR_DELETE,(void *)lpSpr);
	DeleteInternal(lpSpr);

	rlSpr.emState = SPRDATA::DELETED;	
	return SaveSprRecord(&rlSpr);
}

bool CedaSprData::ClockedOut(JOBDATA *prpJob)
{
	bool blClockedOut = false;

	if(prpJob != NULL)
	{
		CCSPtrArray <JOBDATA> olJobs;
		olJobs.Add(prpJob);
		blClockedOut = ClockedOut(olJobs);
	}

	return blClockedOut;
}

bool CedaSprData::ClockedOut(CCSPtrArray <JOBDATA> &ropJobs)
{
	bool blClockedOut = false;
	int ilNumJobs = ropJobs.GetSize();
	for(int i = 0; i < ilNumJobs; i++)
	{
		// a team is treated as being clocked-out only if all members have clocked-out
		blClockedOut = true;

		JOBDATA *prlJob = &ropJobs[i];
		if(!ClockedOut(prlJob->Shur, prlJob->Acfr))
		{
			blClockedOut = false;
			break;
		}
	}

	return blClockedOut;
}

bool CedaSprData::ClockedOut(CDWordArray &ropJobUrnos)
{
	bool blClockedOut = false;
	int ilNumJobs = ropJobUrnos.GetSize();
	for(int i = 0; i < ilNumJobs; i++)
	{
		// a team is treated as being clocked-out only if all members have clocked-out
		blClockedOut = true;

		JOBDATA *prlJob = ogJobData.GetJobByUrno(ropJobUrnos[i]);
		if(prlJob != NULL)
		{
			if(!ClockedOut(prlJob->Shur, prlJob->Acfr))
			{
				blClockedOut = false;
				break;
			}
		}
	}

	return blClockedOut;
}

bool CedaSprData::ClockedOut(long lpShur, CTime opJobTime /*=TIMENULL*/)
{
	return ClockedOut(ogShiftData.GetShiftByUrno(lpShur), opJobTime);
}

bool CedaSprData::ClockedOut(SHIFTDATA *prpShift, CTime opJobTime /*=TIMENULL*/)
{
	bool blClockedOut = false;
	if(prpShift != NULL)
	{
		if((blClockedOut = ClockedOut(prpShift->Stfu, prpShift->Avfa, prpShift->Avta)) == true)
			blClockedOut = SplitShiftClockedOut(prpShift, opJobTime);
	}

	return blClockedOut;
}

bool CedaSprData::ClockedOut(long lpStfu, CTime opFrom, CTime opTo)
{
	return (GetTimeForUstf(lpStfu, SPR_ENDSHIFT, opFrom, opTo) == TIMENULL) ? false : true;
}

// A split shift (Singapore only) is a very long shift with a very long break. The employee clocks out twice,
// once around the break time and again at the end of the shift.
// After the first clockout, it should still be possible to change the shift data and create and change jobs
// after the first clockout time but those jobs which start before the first clockout time cannot be changed
//
// opJobTime - if not TIMENULL then we are checking if a single job is editable
//             is IS TIMENULL then we are checking if the shift data are editable
bool CedaSprData::SplitShiftClockedOut(SHIFTDATA *prpShift, CTime opJobTime /*=TIMENULL*/)
{
	bool blSplitShiftClockedOut = true;
	if(prpShift != NULL && IsSplitShift(prpShift))
	{
		CTime olClockoutTime = GetTimeForUstf(prpShift->Stfu, SPR_ENDSHIFT, prpShift->Avfa, prpShift->Avta);
		if(olClockoutTime == TIMENULL)
		{
			// not clocked-out
			blSplitShiftClockedOut = false;
		}
		else
		{
			// check if this is the first clockout
			if((olClockoutTime + omSplitShiftMinClockoutTimeBeforeShiftEnd) < prpShift->Avta)
			{
				if(opJobTime == TIMENULL)
				{
					// no job so only checking if shift data editable
					blSplitShiftClockedOut = false;
				}
				else if(olClockoutTime <= opJobTime)
				{
					// the clockout time is before the job start time so this job is editable
					blSplitShiftClockedOut = false;
				}
			}	
		}
	}

	return blSplitShiftClockedOut;
}

bool CedaSprData::IsSplitShift(SHIFTDATA *prpShift)
{
	bool blIsSplitShift = false;

	if(prpShift != NULL)
	{
		// split shift has a minimum duration ...
		CTimeSpan olDuration = prpShift->Avta - prpShift->Avfa;
		if(olDuration.GetTotalMinutes() >= bmSplitShiftMinDuration)
		{
			// ... and a minimum break time
			olDuration =  prpShift->Sbto - prpShift->Sbfr;
			if(olDuration.GetTotalMinutes() >= bmSplitShiftMinBreakTime)
			{
				blIsSplitShift = true;
			}
		}
	}

	return blIsSplitShift;
}

CString CedaSprData::Dump(long lpUrno)
{
	CString olDumpStr;
	SPRDATA *prlSpr = GetSprByUrno(lpUrno);
	if(prlSpr == NULL)
	{
		olDumpStr.Format("No SprDATA Found for Spr.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlSpr);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}
