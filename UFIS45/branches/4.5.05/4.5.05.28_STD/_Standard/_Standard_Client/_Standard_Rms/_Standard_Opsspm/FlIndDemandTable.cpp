// FlIndDemandTable.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
//#include "ButtonList.h"
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaDemandData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaSprData.h>
#include <CedaJobData.h>
#include <CedaTplData.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <GateTable.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <AvailableEmpsDlg.h>
#include <AvailableEquipmentDlg.h>


#include <FlIndDemandTable.h>
#include <FlIndDemandTablePropertySheet.h>
#include <AutoAssignDlg.h>
#include <CedaPolData.h>
#include <PrintTerminalSelection.h> //Singapore

const CString FlIndDemandTable::Terminal2 = "T2";
const CString FlIndDemandTable::Terminal3 = "T3";

// Prototypes
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTable dialog

static void FidTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
static void FidTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	FlIndDemandTable *polTable = (FlIndDemandTable *)popInstance;

	if(polTable != NULL && ::IsWindow(polTable->m_hWnd))
	{
		switch(ipDDXType)
		{
		case ONLINE_STATUS_CHANGE:
			polTable->GetDlgItem(IDC_ASSIGN)->EnableWindow(bgOnline);
			break;
		case AFLEND_SBC:
			polTable->ProcessEndAssignment();
			break;
		case STARTASSIGNMENT:
			polTable->ProcessStartAssignment();
			break;
		default:
			break;
		}
	}
}

FlIndDemandTable::FlIndDemandTable(CWnd* pParent /*=NULL*/)
	: CDialog(FlIndDemandTable::IDD, pParent)
{
	pomTable = new CTable;
	//pomTable->SetSelectMode(0); // call this for single-selection; multi-selection is required for assign dlg
    pomTable->tempFlag = 2;

	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pomTableT3 = new CTable;
		pomTableT3->tempFlag = 2;	
		pomActiveTable = pomTable;
		pomActiveViewer = &omViewer;
	}

    CDialog::Create(FlIndDemandTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.FITB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::FLIND_TABLE_WINDOWPOS_REG_KEY,blMinimized);	

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		pomTableT3->SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1 - m_nDialogBarHeight, rect.bottom + rect.bottom/2 + 1 - m_nDialogBarHeight - m_nDialogBarHeight);
		omViewer.SetTerminal(Terminal2);
		omViewerT3.SetTerminal(Terminal3);
		omViewer.Attach(pomTable);
		omViewerT3.Attach(pomTableT3);

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}
	else
	{
		ogCCSDdx.UnRegister(&omViewerT3, NOTUSED);
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		omViewer.Attach(pomTable);
		pomActiveTable = pomTable;
		pomActiveViewer = &omViewer;
		bmIsT2Visible = FALSE;
		bmIsT3Visible = FALSE;
	}

	UpdateView();

	EnableToolTips();
	
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}

	ogCCSDdx.Register(this, ONLINE_STATUS_CHANGE, CString("FIDtable"),CString("Online/Offline status change"), FidTableCf);
	ogCCSDdx.Register(this, AFLEND_SBC, CString("FIDtable"),CString("End Assignment"), FidTableCf); // auto-assignment finished
	ogCCSDdx.Register(this, STARTASSIGNMENT, CString("FIDtable"),CString("Start Assignment"), FidTableCf); // auto-assignment finished
}

FlIndDemandTable::~FlIndDemandTable()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	omViewer.Attach(NULL);
	delete pomTable;	
	if(pomAssignButton != NULL)
	{
		pomAssignButton->Detach();
		delete pomAssignButton;
	}
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		omViewerT3.Attach(NULL);
		if(pomTableT3 != NULL)
		{
			delete pomTableT3;
		}
	}
}


void FlIndDemandTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlIndDemandTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_BUTTONT2,m_bTerminal2); //Singapore
	DDX_Control(pDX, IDC_BUTTONT3,m_bTerminal3); //Singapore
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlIndDemandTable, CDialog)
	//{{AFX_MSG_MAP(FlIndDemandTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChange)
	ON_MESSAGE(WM_TABLE_SETTABLEWINDOW,OnTableSetTableWindow) //Singapore
	ON_MESSAGE(WM_TABLE_LIST_HORZSCROLL,OnTableListHorzScroll) //Singapore
	ON_MESSAGE(WM_TABLE_LIST_VERTSCROLL,OnTableListVertScroll) //Singapore
	ON_MESSAGE(WM_TABLE_ACTIVE_SCROLLWND,OnTableActiveScrollWnd) //Singapore
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	ON_COMMAND(11, OnMenuWorkOn)
	ON_COMMAND(12, OnMenuFreeEmployees)
	ON_COMMAND(13, OnMenuAvailableEquipment)
	ON_COMMAND(DEBUGINFO_MENUITEM, OnMenuDebugInfo)
	ON_COMMAND_RANGE(20, 40, OnMenuDeleteAssignment)
	ON_COMMAND_RANGE(60, 200, OnMenuAcceptConflict) //Singapore
	ON_UPDATE_COMMAND_UI(IDC_ASSIGN,OnUpdateUIAssign)
	ON_BN_CLICKED(IDC_ASSIGN, OnAssign)
	ON_BN_CLICKED(IDC_BUTTONT2,OnButtonT2) //Singapore
	ON_BN_CLICKED(IDC_BUTTONT3,OnButtonT3) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTable message handlers

BOOL FlIndDemandTable::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = GetDlgItem(IDC_ASSIGN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61976));
		polWnd->EnableWindow(bgOnline);
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_STATIC_T2T3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT2);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	//UpdateComboBox();

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		m_DragDropTarget.RegisterTarget(this, this);
	}
	else
	{
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());
	}

	// Register DDX call back function
	TRACE("FlIndDemandTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("FlIndDemandTable"),	CString("Redisplay all from What-If"), FlIndDemandTableCf);	// for what-if changes
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("FlIndDemandTable"),CString("Global Date Change"), FlIndDemandTableCf); // change the date of the data displayed


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	UpdateComboBox();


	pomAssignButton = NULL;
	polWnd = GetDlgItem(IDC_ASSIGN); 
	if(polWnd != NULL)
	{
		if(bgOnline)
		{
//			ogBasicData.SetWindowStat("FIDTABLE IDC_ASSIGN",polWnd);
			polWnd->EnableWindow(TRUE);
		}
		else
		{
			polWnd->EnableWindow(FALSE);
		}
		polWnd->SetWindowText(GetString(IDS_STRING61976));

//		if(ogBasicData.IsEnabled("FIDTABLE IDC_ASSIGN"))
		{
			pomAssignButton = new CCSButtonCtrl(true);
			pomAssignButton->Attach(polWnd->m_hWnd);
			if(ogJobData.bmAutoAssignInProgress)
			{
				pomAssignButton->SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlIndDemandTable::SetCaptionText(void)
{
	CString olCaptionText = GetString(IDS_STRING_FID) + omViewer.omStartTime.Format("%d/%H%M") + "-" + omViewer.omEndTime.Format("%H%M ");;
//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

void FlIndDemandTable::UpdateView()
{

	CString  olViewName = omViewer.GetViewName();
	olViewName = omViewer.GetViewName();

	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.FITV;
	}
	
	
	AfxGetApp()->DoWaitCursor(1);
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omViewer.ChangeViewTo(olViewName);
	    omViewerT3.ChangeViewTo(olViewName);
	}
	else
	{
		omViewer.ChangeViewTo(olViewName);
	}
	AfxGetApp()->DoWaitCursor(-1);
	SetCaptionText();

	//Singapore
	OnTableUpdateDataCount();
	
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();
}

void FlIndDemandTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::FLIND_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("FlIndDemandTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void FlIndDemandTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
   	pogButtonList->m_wndFlIndDemandTable = NULL;
	pogButtonList->m_FlIndDemandButton.Recess(FALSE);

}

void FlIndDemandTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
   {
	   //Singapore
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
			{
				pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy/2+1);
				pomTableT3->SetPosition(-1, cx+1, cy/2+2, cy+1);
			}
			else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
			{
				pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy);			
			}
			else if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
			{
				pomTableT3->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
			}
		}
		else
		{
			pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
		}
		GetWindowRect(&omWindowRect); //PRF 8712
   }
}

void FlIndDemandTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();

	FlIndDemandTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateView();
	}
	bmIsViewOpen = FALSE;
}

void FlIndDemandTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omViewer.SelectView(clText);
		omViewerT3.SelectView(clText);
	}
	else
	{
		omViewer.SelectView(clText);
	}
	UpdateView();		
}

void FlIndDemandTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomTable->GetCTableListBox()->SetFocus();
}

void FlIndDemandTable::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();

	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CPrintTerminalSelection olTerminalSelection;
		if(olTerminalSelection.DoModal() == IDOK)
		{
			if(olTerminalSelection.IsTerminal2Selected() == TRUE)
			{
				omViewer.PrintView();
			}
			else if(olTerminalSelection.IsTerminal3Selected() == TRUE)
			{
				omViewerT3.PrintView();
			}
			else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
			{
				FlIndDemandTableViewer olFlIndDemandTableViewer;
				olFlIndDemandTableViewer.Attach(pomTable);
				CCSPtrArray<FLINDEM_LINE> olLines;
				FLINDEM_LINE olLineData;					
				omViewer.GetLines(olLines);
				for(int i = 0 ; i < olLines.GetSize(); i++)
				{
					olLineData = olLines.GetAt(i);
					olFlIndDemandTableViewer.GetLines().NewAt(olFlIndDemandTableViewer.GetLines().GetSize(),olLineData);
				}
				omViewerT3.GetLines(olLines);
				for(i = 0 ; i < olLines.GetSize(); i++)
				{
					olLineData = olLines.GetAt(i);
					olFlIndDemandTableViewer.GetLines().NewAt(olFlIndDemandTableViewer.GetLines().GetSize(),olLineData);
				}
				olFlIndDemandTableViewer.PrintView();				
				olFlIndDemandTableViewer.GetLines().DeleteAll();
				olFlIndDemandTableViewer.Attach(NULL);
			}
		}
	}
	else
	{
		omViewer.PrintView();
	}
}

BOOL FlIndDemandTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// FlIndDemandTable -- implementation of DDX call back function

void FlIndDemandTable::FlIndDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	FlIndDemandTable *polTable = (FlIndDemandTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void FlIndDemandTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a Flight independant job.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal jobs for those staffs.
//

LONG FlIndDemandTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
	FLINDEM_LINE *prlLine = (FLINDEM_LINE *)pomActiveTable->GetTextLineData(wParam);
	if(prlLine != NULL)
	{
		m_DragDropTarget.CreateDWordData(DIT_FID, 1);
		m_DragDropTarget.AddDWord(prlLine->DemandUrno);
		m_DragDropTarget.BeginDrag();
		pomActiveTable->GetCTableListBox()->SetSel(-1,FALSE); //Singapore
	}
	return 0L;
}


LONG FlIndDemandTable::OnDragOver(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	if(FindActiveTableAndViewer(olDropPosition) == FALSE) //Singapore
	{
		CheckScrolling(olDropPosition); //Singapore
		return -1L;
	}
	CheckScrolling(olDropPosition); //Singapore
	int ilDropLineno = pomActiveTable->GetLinenoFromPoint(olDropPosition);

	if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->omLines.GetSize()-1))
		return -1L;	// cannot drop on a blank line

	imDropDemandUrno = pomActiveViewer->omLines[ilDropLineno].DemandUrno;
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if(prlDemand == NULL)
		return -1L;

    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass == DIT_DUTYBAR)
	{
		if(!ogDemandData.DemandIsOfType(prlDemand,PERSONNELDEMANDS))
			return -1L;
	}
	else if(ilClass == DIT_EQUIPMENT)
	{
		if(!ogDemandData.DemandIsOfType(prlDemand,EQUIPMENTDEMANDS))
			return -1L;
	}
	else
	{
		return -1L;	// cannot interpret this object
	}
	
	return 0L;
}

LONG FlIndDemandTable::OnDrop(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	if(FindActiveTableAndViewer(olDropPosition) == FALSE) //Singapore
	{
		return -1L;
	}
	int ilDropLineno = pomActiveTable->GetLinenoFromPoint(olDropPosition);

	if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->omLines.GetSize()-1))
		return -1L;	// cannot drop on a blank line

	imDropDemandUrno = pomActiveViewer->omLines[ilDropLineno].DemandUrno;
	int ilColumn = pomActiveTable->GetColumnnoFromPoint(olDropPosition);
	int ilDemandNo = (ilColumn-6)/2;


    int ilClass = m_DragDropTarget.GetDataClass(); 
	if(ilClass == DIT_DUTYBAR)
	{
		CDWordArray olPoolJobUrnos;
		int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
		JOBDATA *prlPoolJob = NULL;
		for(int ilC = 0; ilC < ilDataCount; ilC++)
		{
			if((prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(ilC))) != NULL)
			{
				olPoolJobUrnos.Add(prlPoolJob->Urno);
			}
		}
		if(olPoolJobUrnos.GetSize() > 0)
		{
			ogDataSet.CreateFlightIndependentJobs(this, olPoolJobUrnos, imDropDemandUrno);
		}
	}
	else if(ilClass == DIT_EQUIPMENT)
	{
		// a piece of equipment dropped on an equipment demand
		long llEquUrno = m_DragDropTarget.GetDataDWord(0);
		ogDataSet.CreateFidEquipmentJob(this, llEquUrno, imDropDemandUrno);
	}
	OnTableUpdateDataCount(); //Singapore
	return 0L;
}

void FlIndDemandTable::OnClose() 
{
   	pogButtonList->m_wndFlIndDemandTable = NULL;
	pogButtonList->m_FlIndDemandButton.Recess(FALSE);
	CDialog::OnClose();
}

void FlIndDemandTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();	
}

void FlIndDemandTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	omViewer.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omViewerT3.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
		omViewerT3.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
	}
}

void FlIndDemandTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.FITV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

LONG FlIndDemandTable::OnTableLButtonDown(UINT ipItem, LONG lpLParam)
{
//	pomTable->GetCTableListBox()->SetSel(-1, FALSE);
//	pomTable->GetCTableListBox()->SetSel(ipItem, TRUE);
//	TableSelChange();
	return 0l;
}

LONG FlIndDemandTable::OnTableSelChange(UINT ipItem, LONG lpLParam)
{
	TableSelChange();
	return 0L;
}

void FlIndDemandTable::TableSelChange(void)
{
	CTime olStart = TIMENULL, olEnd = TIMENULL;
    CListBox *polListBox = pomActiveTable->GetCTableListBox();
	if(polListBox)
	{
		int ilNumSelLines = polListBox->GetSelCount();
		if(ilNumSelLines > 0)
		{
			int *polSelectedLines = new int[ilNumSelLines];
			polListBox->GetSelItems(ilNumSelLines,polSelectedLines);

			for (int i = 0; i < ilNumSelLines; i++)
			{
				FLINDEM_LINE *prlLine = (FLINDEM_LINE *)pomActiveTable->GetTextLineData(polSelectedLines[i]);
				if (prlLine)
				{
					if(olStart == TIMENULL || prlLine->From < olStart)
						olStart = prlLine->From;
					if(olEnd == TIMENULL || prlLine->Until > olEnd)
						olEnd = prlLine->Until;
				}
			}

			delete [] polSelectedLines;

			if(olStart != TIMENULL && olEnd != TIMENULL)
			{
				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = olStart;
				olTimePacket.EndTime   = olEnd;
				ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
			}
		}
	}

// below is a single select version!!!
//	int ilIndex = pomTable->GetCTableListBox()->GetCurSel();
//	if (ilIndex >= 0 && ilIndex < omViewer.omLines.GetSize())
//	{
//		FLINDEM_LINE *prlLine = &omViewer.omLines[ilIndex];
//		if (prlLine != NULL)
//		{
//			TIMEPACKET olTimePacket;
//			olTimePacket.StartTime = prlLine->From;
//			olTimePacket.EndTime   = prlLine->Until;
//			ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
//		}
//	}
}

LONG FlIndDemandTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
//	UINT ilNumLines = omViewer.omLines.GetSize();
//	if (ipItem >= 0 && ipItem < ilNumLines)
//	{
//		FLINDEM_LINE *prlLine = &omViewer.omLines[ipItem];
//		if (prlLine != NULL)
//		{
//			// not implemented yet
//		}
//	}

	return 0L;
}

//Singapore
void FlIndDemandTable::OnTableSetTableWindow(WPARAM wParam, LPARAM lParam)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CTable* polTable = (CTable*)lParam;	
		if(pomTable == polTable)
		{
			omActiveTerminal = Terminal2;
			pomActiveTable = polTable;
			pomActiveViewer = &omViewer;
			pomTableT3->GetCTableListBox()->SetSel(-1,FALSE);
		}
		else if(pomTableT3 == polTable)
		{
			omActiveTerminal = Terminal3;
			pomActiveTable = polTable;
			pomActiveViewer = &omViewerT3;
			pomTable->GetCTableListBox()->SetSel(-1,FALSE);
		}
	}
}

//Singapore
void FlIndDemandTable::OnTableListHorzScroll(WPARAM wParam,LPARAM lParam)
{
	/*if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(pomActiveTable == pomTable)
		{
			pomTableT3->GetCTableListBox()->SendMessage(WM_HSCROLL,wParam,-1);	
		}
		else if(pomActiveTable == pomTableT3)
		{
			pomTable->GetCTableListBox()->SendMessage(WM_HSCROLL,wParam,-1);	
		}
	}*/
}

//Singapore
void FlIndDemandTable::OnTableListVertScroll(WPARAM wParam, LPARAM lParam)
{
	/*if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(pomActiveTable == pomTable)
		{
			pomTableT3->GetCTableListBox()->SendMessage(WM_VSCROLL,wParam,-1);
		}
		else if(pomActiveTable == pomTableT3)
		{
			pomTable->GetCTableListBox()->SendMessage(WM_VSCROLL,wParam,-1);
		}
	}*/
}

//Singapore
void FlIndDemandTable::OnTableActiveScrollWnd(WPARAM wParam, LPARAM lParam)
{
	pomActiveTable = (CTable*)lParam;
}

LONG FlIndDemandTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));

	if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(omActiveTerminal == Terminal3 && bmIsT2Visible == TRUE)
		{
			CRect olRect;
			pomTable->GetClientRect(&olRect);
			olPoint.y += olRect.Height();
		}
	}
//	pomTable->GetCTableListBox()->SetSel(-1, FALSE);
//	pomTable->GetCTableListBox()->SetSel(ipItem);

	FLINDEM_LINE *prlLine = (FLINDEM_LINE *)pomActiveTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemandUrno);

		if(prlDemand != NULL)
		{
			imDropDemandUrno = prlLine->DemandUrno;

			int ilLc;
			
			menu.CreatePopupMenu();
			for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
				menu.RemoveMenu(ilLc, MF_BYPOSITION);

			if(ogDemandData.DemandIsOfType(prlDemand,PERSONNELDEMANDS))
			{
				menu.AppendMenu(MF_STRING,12, GetString(IDS_STRING61376));	// available employees
			}
			else
			{
				menu.AppendMenu(MF_STRING,13, GetString(IDS_AVAILABLEEQUIPMENT));	// available equipment
			}

			omDeleteJobMap.RemoveAll();
			CCSPtrArray <JOBDATA> olJobs;
			if(ogJodData.GetJobsByDemand(olJobs,prlDemand->Urno))
			{
				CString olText;
				int ilIndex;
				JOBDATA *prlJob = NULL;

				int ilNumJobs = olJobs.GetSize();
				for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
				{
					prlJob = &olJobs[ilJ];
					ilIndex = 20 + ilJ;
					olText.Format("%s %s", GetString(IDS_STRING61401), pomActiveViewer->GetAssignmentStringForJob(prlJob));

					UINT ilFlag = MF_STRING;
					if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(prlJob)) ilFlag = MF_STRING || MF_GRAYED;
					menu.AppendMenu(ilFlag, ilIndex, olText);	// Delete Duty
					omDeleteJobMap.SetAt((void *) ilIndex, prlJob);
				}

				if(ogBasicData.IsPaxServiceT2() == true) //Singapore
				{
					CCSPtrArray <CONFLICTDATA> olConflicts;
					int ilConflNum = 60;
					omAcceptConflictsMap.RemoveAll();
					omConflictToJobMap.RemoveAll();
					for(ilJ = 0; ilJ < ilNumJobs; ilJ++)
					{	
						prlJob = &olJobs[ilJ];					
						ogConflicts.GetJobConflicts(olConflicts,prlJob->Urno);
						int ilNumConflicts = olConflicts.GetSize();
						if(ilNumConflicts > 0)
						{							
							for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
							{
								if(olConflicts[ilConfl].Type != CFI_JOBFROMT3TOT2EMPL
									&& olConflicts[ilConfl].Type != CFI_JOBFROMT2TOT3EMPL)
								{
									continue;
								}
								ilConflNum++;
								olText.Format("%s: %s, %s",GetString(IDS_STRING61357),ogEmpData.GetEmpName(prlJob->Ustf),ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
								olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,ogBasicData.GetFlightForJob(prlJob),prlJob);
								if(olConflicts[ilConfl].Confirmed)
								{
									menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
								}
								else
								{
									menu.AppendMenu(MF_STRING, ilConflNum, olText);									
								}
								omAcceptConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
								omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);																
							}
							olConflicts.RemoveAll();
						}
					}
				}
			}
			if(ogBasicData.DisplayDebugInfo())
			{
				menu.AppendMenu(MF_STRING,DEBUGINFO_MENUITEM, "Debug Info");
			}

			ClientToScreen( &olPoint);
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
		}
	}

	return 0L;
}

void FlIndDemandTable::OnMenuWorkOn()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if (prlDemand)
	{
		// not implemented yet
	}
}

void FlIndDemandTable::OnMenuFreeEmployees()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if(prlDemand != NULL)
	{
		CString olAloc = prlDemand->Aloc;
		CString olAlid = prlDemand->Alid;

		CAvailableEmpsDlg olAvailableEmpsDlg;
		olAvailableEmpsDlg.SetAllocUnit(olAloc, olAlid);

		olAvailableEmpsDlg.SetCaptionObject("");

		int ilNumEmps = olAvailableEmpsDlg.GetAvailableEmployeesForDemand(prlDemand, true);
		olAvailableEmpsDlg.AddDemand(prlDemand);

		if(ilNumEmps <= 0)
		{
			// no employees available for the demands
			MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVAILEMPSCAPTION), MB_ICONEXCLAMATION);
		}
		else
		{
			if(olAvailableEmpsDlg.DoModal() == IDOK && olAvailableEmpsDlg.omSelectedPoolJobUrnos.GetSize() > 0)
			{
				ogDataSet.CreateFlightIndependentJobs(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos,prlDemand->Urno);
			}
		}
	}

}

void FlIndDemandTable::OnMenuDebugInfo()
{
	CString olText = "Demand:\n" + ogDemandData.Dump(imDropDemandUrno);
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if(prlDemand != NULL)
	{
		olText += CString("\nRud:\n") + ogRudData.Dump(prlDemand->Urud);
		olText += CString("\nRue:\n") + ogRueData.Dump(prlDemand->Urue);
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
		if(prlRud != NULL) olText += CString("\nSer:\n") + ogSerData.Dump(prlRud->Ughs);
	}
	CCSPtrArray <JOBDATA> olJobs;
	ogJodData.GetJobsByDemand(olJobs, imDropDemandUrno);
	for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
	{
		olText += "\nJob:\n" + ogJobData.Dump(olJobs[ilJ].Urno);
	}
	ogBasicData.DebugInfoMsgBox(olText);
}

void FlIndDemandTable::OnMenuAvailableEquipment()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if(prlDemand != NULL)
	{
		CString olAloc = prlDemand->Aloc;
		CString olAlid = prlDemand->Alid;

		CAvailableEquipmentDlg olAvailableEquipmentDlg;
		olAvailableEquipmentDlg.SetAllocUnit(olAloc, olAlid);
//		olAvailableEquipmentDlg.SetCaptionObject(Name);

		int ilNumEqu = olAvailableEquipmentDlg.GetAvailableEquipmentForDemand(prlDemand, true);
		olAvailableEquipmentDlg.AddDemand(prlDemand);

		if(ilNumEqu <= 0)
		{
			// no employees available for the demands
			MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVEQ_CAPTION), MB_ICONEXCLAMATION);
		}
		else
		{
			if(olAvailableEquipmentDlg.DoModal() == IDOK)
			{
				ilNumEqu = olAvailableEquipmentDlg.omSelectedEquUrnos.GetSize();
				for(int ilE = 0; ilE < ilNumEqu; ilE++)
				{
					long llEquUrno = olAvailableEquipmentDlg.omSelectedEquUrnos[ilE];
					ogDataSet.CreateFidEquipmentJob(this, llEquUrno, prlDemand->Urno);
				}
			}
		}
	}
}

void FlIndDemandTable::OnMenuDeleteAssignment(UINT nID)
{
	JOBDATA *prlJob;
	omDeleteJobMap.Lookup((void *) nID, (void *&) prlJob);
	ogDataSet.DeleteJob(this, prlJob);
}

void FlIndDemandTable::OnUpdateUIAssign(CCmdUI *pCmdUI)
{
	CWnd *pWnd = this->GetDlgItem(IDC_ASSIGN);
	if (pWnd)
	{
		if(bgOnline)
		{
//			ogBasicData.SetWindowStat("FIDTABLE IDC_ASSIGN",pWnd);
			pCmdUI->Enable(TRUE);
			pCmdUI->Enable(pWnd->IsWindowEnabled());
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
	}
}

void FlIndDemandTable::ProcessEndAssignment()
{
	pomAssignButton->SetColors(::GetSysColor(COLOR_BTNFACE),::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void FlIndDemandTable::ProcessStartAssignment()
{
	pomAssignButton->SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}


void FlIndDemandTable::OnAssign()
{
	if(ogJobData.NoAssignmentInProgress(this))
	{
		CStringArray olReductionList; // not used for the moment
		CAutoAssignDlg olAutoAssignDlg(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd(),ALLOCUNITTYPE_FID,olReductionList,this);
		olAutoAssignDlg.DisableFlightFilter();
		int ilRes = IDOK;

		CUIntArray olDemandUrnoList;
		CListBox *polListBox = pomActiveTable->GetCTableListBox();
		if (polListBox && polListBox->GetSelCount() > 0)
		{
			int *polSelectedLines = new int[polListBox->GetSelCount()];
			polListBox->GetSelItems(polListBox->GetSelCount(),polSelectedLines);

			CString olErrors, olError;
			int ilNumSelLines = polListBox->GetSelCount();

			for (int i = 0; i < ilNumSelLines; i++)
			{
				FLINDEM_LINE *prlLine = (FLINDEM_LINE *)pomActiveTable->GetTextLineData(polSelectedLines[i]);
				if (prlLine)
				{
					DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemandUrno);
					if(prlDemand != NULL && !ogDemandData.DemandIsOfType(prlDemand, PERSONNELDEMANDS))
					{
						olError.Format("\n%s/%s/%s %s-%s", prlLine->Name, prlLine->Code, ogDemandData.GetDemandTypeString(prlDemand), prlLine->From.Format("%H%M"), prlLine->Until.Format("%H%M"));
						olErrors += olError;
					}
					else
					{
						olDemandUrnoList.Add(prlLine->DemandUrno);
					}
				}
			}
			delete [] polSelectedLines;

			if(!olErrors.IsEmpty())
			{
				CString olErrText = "The following demands cannot be assigned automatically, do you want to continue?\n" + olErrors;
				ilRes = MessageBox(olErrText, GetString(IDS_STRING61976), MB_ICONEXCLAMATION | MB_OKCANCEL);
			}
		}

		if(ilRes == IDOK)
		{
			if(olDemandUrnoList.GetSize() <= 0)
			{
				MessageBox(GetString(IDS_SELECTEDFLIND), GetString(IDS_STRING61976), MB_ICONSTOP);
			}
			else
			{
				CStringArray olPoolNames;
				CString olPoolString; 

				int ilNumPools = ogPolData.omData.GetSize();
				for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
				{
					POLDATA *prlPol = &ogPolData.omData[ilPool];
					olPoolString.Format("%s#%ld",prlPol->Name,prlPol->Urno);
					olPoolNames.Add(olPoolString);
				}
				CTime olStartTime = (ogBasicData.GetTimeframeStart() > pomActiveViewer->omStartTime) ? ogBasicData.GetTimeframeStart() : pomActiveViewer->omStartTime;
				CTime olEndTime = pomActiveViewer->omEndTime;
				if(olEndTime > ogBasicData.GetTimeframeEnd())
				{
					olEndTime = ogBasicData.GetTimeframeEnd();
				}
				olAutoAssignDlg.SetData(olDemandUrnoList,olPoolNames,olStartTime,olEndTime);
				olAutoAssignDlg.DoModal();
			}
		}
	}
}

int FlIndDemandTable::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	static long ilLastToolTipLine=-1;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = -1;
	long lDemandUrno = -1;
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
		if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->omLines.GetSize()-1))
		{
			if(ilDropLineno == -1)
			{
				ilDropLineno = pomTableT3->GetLinenoFromPoint(olDropPosition);				
				if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->omLines.GetSize()-1))
					return -1L;
				if(ilDropLineno != -1)
				{
					lDemandUrno = omViewerT3.omLines[ilDropLineno].DemandUrno;
				}
			}
			else
			{
				lDemandUrno = omViewer.omLines[ilDropLineno].DemandUrno;
			}
			return -1L;	// cannot drop on a blank line
		}
	}
	else
	{
		ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.omLines.GetSize()-1))
		return -1L;	// cannot drop on a blank line
	}

	if(ilDropLineno != ilLastToolTipLine )
	{
		ilLastToolTipLine = ilDropLineno;
		return -1;
	}

	CString olToolTip;
	DEMANDDATA *prlDemand = NULL;
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		prlDemand = ogDemandData.GetDemandByUrno(lDemandUrno);
	}
	else
	{
		prlDemand = ogDemandData.GetDemandByUrno(omViewer.omLines[ilDropLineno].DemandUrno);
	}
	if(prlDemand != NULL)
	{
		CString olManualDemText;
		if(prlDemand->Flgs[6] == '1')
		{
			olManualDemText.Format(" (%s)",GetString(IDS_MANUALDEMAND));
		}
		RUDDATA *prlRudData = ogRudData.GetRudByUrno(prlDemand->Urud);
		if(prlRudData != NULL)
		{
			SERDATA *prlSerData = ogSerData.GetSerByUrno(prlRudData->Ughs);
			RUEDATA *prlRueData = ogRueData.GetRueByUrno(prlRudData->Urue);
			if(prlSerData != NULL && prlRueData != NULL)
			{
				olToolTip.Format("%s/%s",prlRueData->Rusn,prlSerData->Snam); 
				if(!olManualDemText.IsEmpty())
				{
					olToolTip += olManualDemText;
				}

				if(ogBasicData.DisplayDebugInfo())
				{
					TPLDATA *prlTpl = ogTplData.GetTplByUrno(prlRueData->Utpl);
					if(prlTpl != NULL)
					{
						CString olDebugText;
						olDebugText.Format(" -> TPL.TNAM<%s> RUE.RUNA<%s> RUD.DISP<%s>", prlTpl->Tnam, prlRueData->Runa, prlRudData->Disp);
						olToolTip += olDebugText;
					}
				}
			}
			if(ogDemandData.DemandIsOfType(prlDemand, EQUIPMENTDEMANDS))
			{
				CCSPtrArray <EQUDATA> olEquList;
				olToolTip += CString("  ") + GetString(IDS_REQUIREDEQUIPMENT);
				ogBasicData.GetEquipmentForDemand(prlDemand, olEquList);
				int ilNumEqu = olEquList.GetSize();
				for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
				{
					olToolTip += ((ilEqu > 0) ? CString("/") : CString(": ")) + olEquList[ilEqu].Enam;
				}
			}
		}
		else
		{
			AZADATA *prlAzaData = ogAzaData.GetAzaByUrno(prlDemand->Udgr);
			if (prlAzaData != NULL)
			{
				if(*prlDemand->Dety == P05RES_DEMAND)
				{
					olToolTip.Format("%s/%s",GetString(IDS_P05RESDEMAND), prlAzaData->Seco); // Precoord
				}
				else if(*prlDemand->Dety == CAS_DEMAND)
				{
					olToolTip = GetString(IDS_CASDEMAND); // Casp
				}
				else if(*prlDemand->Dety == MANUAL_THIRDPARTY_DEMAND)
				{
					olToolTip.Format("%s/%s",GetString(IDS_MANUAL3RDDEMAND), prlAzaData->Seco); // Manual 3rd party
				}

			}
		}
	}

	if(!olToolTip.IsEmpty())
	{
		pTI->lpszText = new char[olToolTip.GetLength()+1];
		pTI->uFlags = TTF_NOTBUTTON | TTF_IDISHWND;
		strcpy ( pTI->lpszText, olToolTip );
		pTI->uId = (UINT)m_hWnd;
		pTI->hwnd = ::GetParent(m_hWnd);
		return 1;
	}

	return -1;
}

//Singapore
void FlIndDemandTable::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			if(ogBasicData.IsPaxServiceT2() == true)
			{
				char olDataCountT2[10];
				char olDataCountT3[10];
				polWnd->EnableWindow();			
				itoa(omViewer.omLines.GetSize(),olDataCountT2,10);
				itoa(omViewerT3.omLines.GetSize(),olDataCountT3,10);		
				CString olText;
				olText.Format("%s/%s",olDataCountT2,olDataCountT3);
				polWnd->SetWindowText(olText);
			}
		}
	}
}
//Singapore
BOOL FlIndDemandTable::FindActiveTableAndViewer(CPoint& ropPoint)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{	
		CPoint olPoint = ropPoint;
		CRect olClientRect;

		if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
		{
			return FALSE;
		}


		int ilOffset = 0;
		SCROLLINFO scrollInfo;
		BOOL blScrollInfo = pomTable->GetCTableListBox()->GetScrollInfo(SB_HORZ,&scrollInfo);
		int ilHorzExtent = pomTable->GetCTableListBox()->GetHorizontalExtent();

		if(blScrollInfo == TRUE)
		{
			ilOffset = ilHorzExtent > scrollInfo.nPage ? pomTable->imItemHeight : 0;
		}

		pomTable->GetCTableListBox()->GetWindowRect(&olClientRect);	
		if(bmIsT2Visible == TRUE && (olClientRect.bottom - ilOffset) >=ropPoint.y && olClientRect.top <= ropPoint.y)
		{			
			pomActiveTable = pomTable;
			pomActiveViewer = &omViewer;
			omActiveTerminal = Terminal2;
			return TRUE;
		}
		else
		{	
			blScrollInfo = pomTableT3->GetCTableListBox()->GetScrollInfo(SB_HORZ,&scrollInfo);
			ilHorzExtent = pomTableT3->GetCTableListBox()->GetHorizontalExtent();

			if(blScrollInfo == TRUE)
			{
				ilOffset = ilHorzExtent > scrollInfo.nPage ? pomTableT3->imItemHeight : 0;
			}
			
			pomTableT3->GetCTableListBox()->GetWindowRect(&olClientRect);	
			if(bmIsT3Visible == TRUE && (olClientRect.bottom - ilOffset) >=ropPoint.y && olClientRect.top <= ropPoint.y)
			{				
				pomActiveTable = pomTableT3;
				pomActiveViewer = &omViewerT3;
				omActiveTerminal = Terminal3;
				return TRUE;
			}
		}
	}
	else
	{
		pomActiveTable = pomTable;
		pomActiveViewer = &omViewer;
		return TRUE;
	}
	return FALSE;
}	

//Singapore
void FlIndDemandTable::OnButtonT2()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT2);	
	bmIsT2Visible = !bmIsT2Visible;
	
	if(bmIsT2Visible == TRUE)
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal2.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	
	if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTable->ShowWindow(SW_HIDE);
	}	
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());
		pomTable->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
	{		
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTableT3->GetCTableListBox());
		
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->ShowWindow(SW_HIDE);
		pomTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
	{		
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);	
		pomTable->ShowWindow(SW_SHOW);
		
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

//Singapore
void FlIndDemandTable::OnButtonT3()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT3);	
	bmIsT3Visible = !bmIsT3Visible;
	
	if(bmIsT3Visible == TRUE)
	{
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal3.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsT3Visible == FALSE && bmIsT2Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTableT3->ShowWindow(SW_HIDE);

	}	
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTableT3->GetCTableListBox());
		pomTableT3->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == FALSE && bmIsT2Visible == TRUE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());
		
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
						
		pomTableT3->ShowWindow(SW_HIDE);
		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == TRUE)
	{
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);	
		pomTableT3->ShowWindow(SW_SHOW);

		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
		
		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom -3);
	}
}

//Singapore
void FlIndDemandTable::CheckScrolling(CPoint& ropPoint)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
		{
			const int ilHorzOffset = 15;
			CPoint olPoint = ropPoint;
			CRect olClientRect;
			CRect olClientListRect;
		
			pomTable->GetWindowRect(&olClientRect);
			pomTable->GetCTableListBox()->GetWindowRect(&olClientListRect);

			if(olClientListRect.bottom >= ropPoint.y && olClientListRect.top <= ropPoint.y)
			{
				if((olClientRect.left + ilHorzOffset) >= ropPoint.x)
				{
					pomTable->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINELEFT,0);
				}
				else if((olClientRect.right - ilHorzOffset) <= ropPoint.x)
				{
					pomTable->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINERIGHT,0);
				}
			}
			else
			{
				pomTableT3->GetWindowRect(&olClientRect);
				pomTableT3->GetCTableListBox()->GetWindowRect(&olClientListRect);

				if(olClientListRect.bottom >= ropPoint.y && olClientListRect.top <= ropPoint.y)
				{					
					if((olClientRect.left + ilHorzOffset) >= ropPoint.x)
					{
						pomTableT3->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINELEFT,0);
					}
					else if((olClientRect.right - ilHorzOffset) <= ropPoint.x)
					{
						pomTableT3->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINERIGHT,0);
					}
				}
			}

			pomTable->GetWindowRect(&olClientRect);
			pomTable->GetCTableListBox()->GetWindowRect(&olClientListRect);

			if((olClientRect.bottom - pomTable->imItemHeight) <=ropPoint.y && olClientRect.bottom >=ropPoint.y)
			{
				pomTable->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEDOWN,0);
			}
			else if(olClientListRect.top >=ropPoint.y && (olClientListRect.top - pomTable->imItemHeight) <= ropPoint.y)
			{
				pomTable->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEUP,0);
			}
			else
			{				
				pomTableT3->GetWindowRect(&olClientRect);	
				pomTableT3->GetCTableListBox()->GetWindowRect(&olClientListRect);				
				
				if((olClientRect.bottom - pomTable->imItemHeight) <=ropPoint.y && olClientRect.bottom >=ropPoint.y)
				{
					pomTableT3->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEDOWN,0);
				}
				else if(olClientListRect.top >=ropPoint.y && (olClientListRect.top - pomTableT3->imItemHeight) <= ropPoint.y)
				{
					pomTableT3->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEUP,0);
				}
			}
		}
	}
}

//Singapore
void FlIndDemandTable::OnMenuAcceptConflict(UINT ipId)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		JOBDATA *prlJob;
		// accept a single conflict
		CONFLICTDATA *prlConflict;
		omAcceptConflictsMap.Lookup((void *) ipId, (void *&) prlConflict);
		omConflictToJobMap.Lookup((void *) ipId, (void *&) prlJob);
		ogConflicts.AcceptOneJobConflict(prlJob->Urno, prlConflict->Type);
	}
}

//PRF 8712
void FlIndDemandTable::OnMove(int x, int y)
{	
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}