// LoadTimeSpanDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <PrmDialog.h>
#include <BasicData.h>
#include <resource.h>
#include <SelReportDlg.h>
//#include <PRMFlightSearch.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrmDialog dialog


CPrmDialog::CPrmDialog(CWnd* pParent ,DPXDATA *prpDpx)
	: CDialog(CPrmDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrmDialog)
		m_Owhc1 = -1;
		m_FlnuAdid = -1;
		m_TfluAdid = -1;
		m_IsReadOnly = FALSE;
	//}}AFX_DATA_INIT

		pomDpx = prpDpx;
		bmAllowFreeLocation = FALSE;
}

CPrmDialog::CPrmDialog(CWnd* pParent ,DPXDATA *prpDpx, BOOL CanEditFlight)
	: CDialog(CPrmDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrmDialog)
		m_Owhc1 = -1;
		m_FlnuAdid = -1;
		m_TfluAdid = -1;
		m_IsReadOnly = !CanEditFlight;
	//}}AFX_DATA_INIT

		pomDpx = prpDpx;
		bmAllowFreeLocation = FALSE;
}

CPrmDialog::~CPrmDialog()
{	
//	omValues.RemoveAll();
}

void CPrmDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrmDialog)
	DDX_Control(pDX, IDC_PRID, m_Prid);
	DDX_Control(pDX, IDC_NAME, m_Name);
	DDX_Radio(pDX, IDC_PRM_OWHC1, m_Owhc1);
	DDX_Radio(pDX, IDC_FLNU_ARR, m_FlnuAdid);
	DDX_Radio(pDX, IDC_TLNU_ARR, m_TfluAdid);
	DDX_Control(pDX, IDC_TIME_STOA, m_TimeStoa);
	DDX_Control(pDX, IDC_TIME_ATOA, m_TimeAtoa);
	DDX_Control(pDX, IDC_STOA, m_Stoa);
	DDX_Control(pDX, IDC_ATOA, m_Atoa);
	DDX_Control(pDX, IDC_CLOS, m_Clos);
	DDX_Control(pDX, IDC_BOOKING_DATE, m_Booking);
	DDX_Control(pDX, IDC_TIME_STOA, m_TimeStoa);
	DDX_Control(pDX, IDC_TIME_ATOA, m_TimeAtoa);
	DDX_Control(pDX, IDC_TIME_CLOS, m_TimeClos);
	DDX_Control(pDX, IDC_TIME_BOOKING, m_TimeBooking);
	DDX_Control(pDX, IDC_TIME_FLNUTIME, m_FlnuTime);
	DDX_Control(pDX, IDC_TIME_TFLUTIME, m_TfluTime);
	DDX_Control(pDX, IDC_MAIL, m_Mail);
	DDX_Control(pDX, IDC_REMA, m_Rema);
	DDX_Control(pDX, IDC_SEAT, m_Seat);
	DDX_Control(pDX, IDC_LANG_CB, m_Lang);
	DDX_Control(pDX, IDC_NATL_CB, m_Natl);
	DDX_Control(pDX, IDC_PRM_PRMT_CB, m_PrmType);
	DDX_Control(pDX, IDC_PRM_ABSR_CB, m_BookingChannel);
	DDX_Control(pDX, IDC_PRM_ALOC_CB, m_ArrivalLocation);
	DDX_Control(pDX, IDC_PRM_GENDER_CB, m_Gender);
	DDX_Control(pDX, IDC_FLNUALC1, m_FlnuAlc);
	DDX_Control(pDX, IDC_FLNUNUMBER, m_FlnuNumber);
	DDX_Control(pDX, IDC_FLNUSUFFIX, m_FlnuSuffix);
	DDX_Control(pDX, IDC_TLNUALC, m_TfluAlc);
	DDX_Control(pDX, IDC_TLNUNUMBER, m_TfluNumber);
	DDX_Control(pDX, IDC_TLNUSUFFIX, m_TfluSuffix);
	DDX_Check(pDX, IDC_CHECK_FLNUPRMS, m_CheckFlnuPrms); //Singapore
	DDX_Check(pDX, IDC_CHECK_TFLUPRMS, m_CheckTfluPrms); //Singapore
	DDX_Control(pDX, IDC_FCALLSIGN, m_FCsgn);
	DDX_Control(pDX, IDC_TCALLSIGN, m_TCsgn);
	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrmDialog, CDialog)
	//{{AFX_MSG_MAP(CPrmDialog)
//	ON_BN_CLICKED(IDC_FLIGHTSEARCH,OnFlightSearch)
	ON_BN_CLICKED(IDC_FLNU_ARR, OnGetFlnuTime)
	ON_BN_CLICKED(IDC_FLNU_DEP, OnGetFlnuTime)
	ON_BN_CLICKED(IDC_TLNU_ARR, OnGetTfluTime)
	ON_BN_CLICKED(IDC_TLNU_DEP, OnGetTfluTime)
	ON_EN_KILLFOCUS(IDC_FLNUNUMBER, OnKillfocusFlnuTime)
	ON_EN_KILLFOCUS(IDC_TLNUNUMBER, OnKillfocusTfluTime)
	ON_EN_KILLFOCUS(IDC_FCALLSIGN, OnKillfocusFCsgnTime)
	ON_EN_KILLFOCUS(IDC_TCALLSIGN, OnKillfocusTCsgnTime)
	ON_CBN_SELCHANGE(IDC_PRM_PRMT_CB, OnSelchangePrmtcombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrmDialog message handlers


bool CPrmDialog::checkDate(CString date, CString time,  CTime &value)
{

	if(time.IsEmpty())
	{
		time = CString("00:00");
	}
	if (!date.IsEmpty() ) 
	{
		CTime olDate;
		CTime olTime;
		CString olCedaDate;

		olDate = DateStringToDate(date);
		olTime = HourStringToDate(time, olDate);
		if (olDate != TIMENULL && olTime != TIMENULL)
		{
			//olCedaDate =  olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			value = olTime;
		}
		else
		{
			return false;
		}
	}
	return true;
}

BOOL CPrmDialog::GetData()
{
	UpdateData(TRUE);

	CString stoa,atoa, timeStoa, timeAtoa, booking, timeBooking,clos,timeClos;

	CTime olDate, olTime;
	m_Stoa.GetWindowText(stoa);// TIFA From || TIFD From; Date
	m_Atoa.GetWindowText(atoa);
	m_Booking.GetWindowText(booking);
	m_TimeStoa.GetWindowText(timeStoa);
	m_TimeAtoa.GetWindowText(timeAtoa);
	m_TimeBooking.GetWindowText(timeBooking);


	// Check dates and times

	stoa.TrimLeft();
	atoa.TrimLeft(); 
	timeStoa.TrimLeft(); 
	timeAtoa.TrimLeft(); 
	booking.TrimLeft(); 
	timeBooking.TrimLeft();

	if(!m_Prid.GetStatus())
	{
		CString olText;
		if (imFormat == X50)
		{
			olText = GetString(IDS_PRM_INVALIDPRID); 
		}
		else
		{
			olText = GetString(IDS_INVALID_DIGIT); 
		}
		MessageBox(olText, "", MB_OK);
		return FALSE;

	}

	if(!m_Name.GetStatus())
	{
		CString olText;
		olText = GetString(IDS_INVALID_TEXT); 
		MessageBox(olText, "", MB_OK);
		return FALSE;

	}
	
	if (timeStoa.IsEmpty() && stoa.IsEmpty())
	{
		pomDpx->Sati = TIMENULL;
	}
	else if ( ! checkDate(stoa,timeStoa,pomDpx->Sati) ) 
	{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
	}

	if (timeAtoa.IsEmpty() && atoa.IsEmpty())
	{
		pomDpx->Aati = TIMENULL;
	}
	else if ( ! checkDate(atoa,timeAtoa,pomDpx->Aati) ) 
	{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
	}

	if (timeBooking.IsEmpty() && booking.IsEmpty())
	{
		pomDpx->Abdt = TIMENULL;
	}
	else if ( ! checkDate(booking,timeBooking,pomDpx->Abdt) ) 
	{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
	}

	if (bgShowPrmClos)
	{
		m_Clos.GetWindowText(clos);// TIFA From || TIFD From; Date
		m_TimeClos.GetWindowText(timeClos);

		clos.TrimLeft();
		timeClos.TrimLeft();
		if (timeClos.IsEmpty() && clos.IsEmpty())
		{
			pomDpx->Clos = TIMENULL;
		}
		else if ( ! checkDate(clos,timeClos,pomDpx->Clos) ) 
		{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
		}
	}

	m_Prid.GetWindowText(pomDpx->Prid,sizeof(pomDpx->Prid)-1);
	m_Name.GetWindowText(pomDpx->Name,sizeof(pomDpx->Name)-1);

    m_FlnuAlc.GetWindowText(pomDpx->FlnuAlc,sizeof(pomDpx->FlnuAlc)-1);
    m_FlnuNumber.GetWindowText(pomDpx->FlnuNumber,sizeof(pomDpx->FlnuNumber)-1);
    m_FlnuSuffix.GetWindowText(pomDpx->FlnuSuffix,sizeof(pomDpx->FlnuSuffix));
    m_TfluAlc.GetWindowText(pomDpx->TfluAlc,sizeof(pomDpx->TfluAlc)-1);
    m_TfluNumber.GetWindowText(pomDpx->TfluNumber,sizeof(pomDpx->TfluNumber)-1);
    m_TfluSuffix.GetWindowText(pomDpx->TfluSuffix,sizeof(pomDpx->TfluSuffix));

	m_FCsgn.GetWindowText(pomDpx->FCsgn,sizeof(pomDpx->FCsgn)-1);
	m_TCsgn.GetWindowText(pomDpx->TCsgn,sizeof(pomDpx->TCsgn)-1);

	strcpy(pomDpx->Owhc," ");
	if (m_Owhc1 == 0)
	{
		strcpy(pomDpx->Owhc,"Y");
	}
	else if (m_Owhc1 == 1)
	{
		strcpy(pomDpx->Owhc,"O");
	}
	else if (m_Owhc1 == 2)
	{
		strcpy(pomDpx->Owhc,"N");
	}
	pomDpx->FlnuAdid = ' ';
	if (m_FlnuAdid == 0)
	{
		pomDpx->FlnuAdid = 'A';
	}
	else if (m_FlnuAdid == 1)
	{
		pomDpx->FlnuAdid = 'D';
	}
	if (m_TfluAdid == 0)
	{
		pomDpx->TfluAdid = 'A';
	}
	else if (m_TfluAdid == 1)
	{
		pomDpx->TfluAdid = 'D';
	}
	
	strcpy(pomDpx->FlnuPrms," ");
	if (m_CheckFlnuPrms == FALSE)
	{
		strcpy(pomDpx->FlnuPrms,"N");
	}
	else 
	{
		strcpy(pomDpx->FlnuPrms,"Y");
	}

	strcpy(pomDpx->TfluPrms," ");
	if (m_CheckTfluPrms == FALSE)
	{
		strcpy(pomDpx->TfluPrms,"N");
	}
	else 
	{
		strcpy(pomDpx->TfluPrms,"Y");
	}

	int ilSel = m_Gender.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_Gender.GetLBText(ilSel,pomDpx->Gend);
	}

	m_Mail.GetWindowText(pomDpx->Mail,sizeof(pomDpx->Mail)-1);
	m_Rema.GetWindowText(pomDpx->Rema,sizeof(pomDpx->Rema)-1);
	//pomDpx->Flnu = 0L;
	//pomDpx->Tflu = 0L;
	//pomDpx->Abdt = ogBasicData.GetUtcTime();
	ilSel = m_BookingChannel.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_BookingChannel.GetLBText(ilSel,pomDpx->Absr);
	}
	ilSel = m_ArrivalLocation.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_ArrivalLocation.GetLBText(ilSel,pomDpx->Aloc);
	}
	else if (bmAllowFreeLocation)
	{
		m_ArrivalLocation.GetDlgItemText(1001,pomDpx->Aloc,21);
	}

	//pomDpx->Sati = ogBasicData.GetUtcTime();
	//pomDpx->Aati = ogBasicData.GetUtcTime();

	ilSel = m_PrmType.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_PrmType.GetLBText(ilSel,pomDpx->Prmt);
	}

	ilSel = m_Lang.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_Lang.GetLBText(ilSel,pomDpx->Lang);
	}

	ilSel = m_Natl.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_Natl.GetLBText(ilSel,pomDpx->Natl);
	}

	pomDpx->Tlxu = 0L;

	m_Seat.GetWindowText(pomDpx->Seat,sizeof(pomDpx->Seat)-1);
	pomDpx->Cdat = ogBasicData.GetLocalTime();
	pomDpx->Lstu = ogBasicData.GetLocalTime();
	strcpy(pomDpx->Usec,ogBasicData.omUserID);


	if (pomDpx->Prid == '\0')
	{
		strcpy(pomDpx->Prid,"0");
	}
	return true;
}

void CPrmDialog::SetData()
{
	this->SetData(pomDpx);
}

void CPrmDialog::SetData(DPXDATA *popDpx)
{

	m_Prid.SetWindowText(CString(pomDpx->Prid));
	m_Name.SetWindowText(pomDpx->Name);

	if (pomDpx->Owhc[0] == 'Y')
	{
		m_Owhc1 = 0;
	}
	else if (pomDpx->Owhc[0] == 'O')
	{
		m_Owhc1 = 1;
	}
	else if (pomDpx->Owhc[0] == 'N')
	{
		m_Owhc1 = 2;
	}

	if(strlen(pomDpx->FlnuFlno)>0)
	{
		if (pomDpx->FlnuAdid == 'A')
		{
			m_FlnuAdid = 0;
		}
		else if (pomDpx->FlnuAdid == 'D')
		{
			m_FlnuAdid = 1;
		}
		if (pomDpx->FlnuPrms[0] == 'Y')
		{
			m_CheckFlnuPrms = TRUE;
		}
		else
		{
			m_CheckFlnuPrms = FALSE;
		}
	}

	if(strlen(pomDpx->TfluFlno)>0)
	{
		if (pomDpx->TfluAdid == 'A')
		{
			m_TfluAdid = 0;
		}
		else if (pomDpx->TfluAdid == 'D')
		{
			m_TfluAdid = 1;
		}
		if (pomDpx->TfluPrms[0] == 'Y')
		{
			m_CheckTfluPrms = TRUE;
		}
		else
		{
			m_CheckTfluPrms = FALSE;
		}
	}

	m_Mail.SetWindowText(pomDpx->Mail);
	m_Rema.SetWindowText(pomDpx->Rema);

	int ilSel = m_PrmType.FindString(-1,pomDpx->Prmt);
	if (ilSel > -1)
	m_PrmType.SetCurSel(ilSel);

	ilSel = m_Lang.FindString(-1,pomDpx->Lang);
	if (ilSel > -1)
	m_Lang.SetCurSel(ilSel);

	ilSel = m_Natl.FindString(-1,pomDpx->Natl);
	if (ilSel > -1)
	m_Natl.SetCurSel(ilSel);

	ilSel = m_BookingChannel.FindString(-1,pomDpx->Absr);
	if (ilSel > -1)
	m_BookingChannel.SetCurSel(ilSel);

	ilSel = m_ArrivalLocation.FindString(-1,pomDpx->Aloc);
	if (ilSel > -1)
		m_ArrivalLocation.SetCurSel(ilSel);
	else 
	{
		m_ArrivalLocation.AddString(pomDpx->Aloc);
		ilSel = m_ArrivalLocation.FindString(-1,pomDpx->Aloc);
		if (ilSel > -1)
			m_ArrivalLocation.SetCurSel(ilSel);
	}

	ilSel = m_Gender.FindString(-1,pomDpx->Gend);
	if (ilSel > -1)
	m_Gender.SetCurSel(ilSel);


	m_FlnuAlc.SetWindowText(pomDpx->FlnuAlc);
	m_FlnuNumber.SetWindowText(pomDpx->FlnuNumber);
	m_FlnuSuffix.SetWindowText(pomDpx->FlnuSuffix);

	m_TfluAlc.SetWindowText(pomDpx->TfluAlc);
	m_TfluNumber.SetWindowText(pomDpx->TfluNumber);
	m_TfluSuffix.SetWindowText(pomDpx->TfluSuffix);

	m_FlnuAlc.SetReadOnly(m_IsReadOnly);
	m_FlnuNumber.SetReadOnly(m_IsReadOnly);
	m_FlnuSuffix.SetReadOnly(m_IsReadOnly);
	m_TfluAlc.SetReadOnly(m_IsReadOnly);
	m_TfluNumber.SetReadOnly(m_IsReadOnly);
	m_TfluSuffix.SetReadOnly(m_IsReadOnly);
	m_FCsgn.SetReadOnly(m_IsReadOnly);
	m_TCsgn.SetReadOnly(m_IsReadOnly);

	if (m_IsReadOnly)
	{
		GetDlgItem(IDC_FLNU_ARR)->EnableWindow(FALSE);
		GetDlgItem(IDC_FLNU_DEP)->EnableWindow(FALSE);
		GetDlgItem(IDC_TLNU_ARR)->EnableWindow(FALSE);
		GetDlgItem(IDC_TLNU_DEP)->EnableWindow(FALSE);
	}
	if (!bgPrmUseRema)
	{
		GetDlgItem(IDC_REMA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PRM_REMA)->ShowWindow(SW_HIDE);
	}
	if (!bgPrmUseStat)
	{
		GetDlgItem(IDC_CHECK_FLNUPRMS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_TFLUPRMS)->ShowWindow(SW_HIDE);
	}
	if(pomDpx->Flnu!=0L)
	{
		CTime olTmpTime;
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(pomDpx->Flnu);
		if (*prlFlight->Adid == 'A') 
		{
			olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
		}
		else
		{
			olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
		}
		m_FlnuTime.SetWindowText(olTmpTime.Format("%H:%M"));
		m_FCsgn.SetWindowText(prlFlight->Csgn);		
	}
	if(pomDpx->Tflu!=0L)
	{
		CTime olTmpTime;
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(pomDpx->Tflu);
		if (*prlFlight->Adid == 'A') 
		{
			olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
		}
		else
		{
			olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
		}
		m_TfluTime.SetWindowText(olTmpTime.Format("%H:%M"));
		m_TCsgn.SetWindowText(prlFlight->Csgn);		
	}
	m_FlnuTime.SetReadOnly(true);
	m_TfluTime.SetReadOnly(true);
	GetDlgItem(IDC_TIME_FLNUTIME)->EnableWindow(FALSE);
	GetDlgItem(IDC_TIME_TFLUTIME)->EnableWindow(FALSE);
	//olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
	m_Stoa.SetWindowText(pomDpx->Sati.Format("%d.%m.%Y"));
	m_TimeStoa.SetWindowText(pomDpx->Sati.Format("%H:%M"));

	m_Atoa.SetWindowText(pomDpx->Aati.Format("%d.%m.%Y"));
	m_TimeAtoa.SetWindowText(pomDpx->Aati.Format("%H:%M"));

	if (bgShowPrmClos)
	{
		m_Clos.SetWindowText(pomDpx->Clos.Format("%d.%m.%Y"));
		m_TimeClos.SetWindowText(pomDpx->Clos.Format("%H:%M"));
	}
	else
	{
		m_Clos.SetWindowText("");
		m_TimeClos.SetWindowText("");
	}
	m_Booking.SetWindowText(pomDpx->Abdt.Format("%d.%m.%Y"));
	m_TimeBooking.SetWindowText(pomDpx->Abdt.Format("%H:%M"));
	m_Seat.SetWindowText(pomDpx->Seat);


//		m_FromDate.SetWindowText("");// TIFA From || TIFD From; Date
//    m_FromTime.SetWindowText("");// TIFA From || TIFD From; Time
//    m_ToDate.SetWindowText("");// TIFA To || TIFD To; Date
//    m_ToTime.SetWindowText("");// TIFA To || TIFD To; Time
//    m_RelFrom.SetWindowText("");// RelHBefore
//    m_RelTo.SetWindowText("");// RelHAfter
// 	bool blFlag = false;
}

BOOL CPrmDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	char pclConfigPath[256];
	char pclConfigString[512];
	char pclConfigCopy[512];
    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	CWnd *polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	if (bgShowPrmClos)
	{
		polWnd = GetDlgItem(IDC_PRM_CLOS);
		polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_CLOS);
		polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_TIME_CLOS);
		polWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		polWnd = GetDlgItem(IDC_PRM_CLOS);
		polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CLOS);
		polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TIME_CLOS);
		polWnd->ShowWindow(SW_HIDE);
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDC_PRM_PRID);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33166));
	}
	polWnd = GetDlgItem(IDC_PRM_NAME);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33167));
	}
	polWnd = GetDlgItem(IDC_PRM_LANG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33168));
	}
	polWnd = GetDlgItem(IDC_PRM_NATL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33169));
	}
	/***/
	polWnd = GetDlgItem(IDC_PRM_OWHC1);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33171));
	}

	polWnd = GetDlgItem(IDC_PRM_OWHC2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33172));
	}

	polWnd = GetDlgItem(IDC_PRM_OWHC3);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33189));
	}
   /***/
	polWnd = GetDlgItem(IDC_PRM_MAIL);
	if(polWnd != NULL)
	{
		GetPrivateProfileString(pcgAppName, "PRM-PAXLOCATION",  "NO",pclConfigString, sizeof pclConfigString, pclConfigPath);
		if (!strcmp(pclConfigString,"YES")) {
			polWnd->SetWindowText("Pax Location:");
		}
		else{
			polWnd->SetWindowText(GetString(IDS_STRING33170));
		}

	}

	polWnd = GetDlgItem(IDC_PRM_REMA);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33188));
	}

	polWnd = GetDlgItem(IDC_PRM_PRMT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33173));
	}

	polWnd = GetDlgItem(IDC_PRM_ABSR_CB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33174));
	}
 
	GetPrivateProfileString(pcgAppName, "PRM-ALLOWFREELOCATION",  "NO",pclConfigString, sizeof pclConfigString, pclConfigPath);
	if (!strcmp(pclConfigString,"YES")) {
		bmAllowFreeLocation = true;
	}

	polWnd = GetDlgItem(IDC_PRM_ALOC);
	if(polWnd != NULL)
	{
		if (bmAllowFreeLocation)
		{
			polWnd->SetWindowText(CString("Pax Loc."));
			polWnd->SetWindowPos(&wndTop, 30,322,50,66, SWP_SHOWWINDOW);
			polWnd = GetDlgItem(IDC_PRM_ALOC_CB);
			polWnd->SetWindowPos(&wndTop, 78,322,125,66, SWP_SHOWWINDOW);
		}
		else
		{
			polWnd->SetWindowText(GetString(IDS_STRING33175));
			polWnd = GetDlgItem(IDC_PRM_ALOC_CB);
			polWnd->SetWindowPos(&wndTop, 130,322,73,66, SWP_SHOWWINDOW);
		}
	}

	polWnd = GetDlgItem(IDC_PRM_FLNU);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33176));
	}

	polWnd = GetDlgItem(IDC_PRM_TFLU);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33177));
	}

	polWnd = GetDlgItem(IDC_PRM_GEND);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33178));
	}
	polWnd = GetDlgItem(IDC_PRM_SEAT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33179));
	}
	polWnd = GetDlgItem(IDC_PRM_ABDT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33180));
	}


	polWnd = GetDlgItem(IDC_PRM_SATI);
	if(polWnd != NULL)
	{
		GetPrivateProfileString(pcgAppName, "PRM-USEFLIGHTDATE",  "NO",pclConfigString, sizeof pclConfigString, pclConfigPath);
		if (!strcmp(pclConfigString,"YES")) {
			polWnd->SetWindowText(CString("Flight Date"));
			polWnd = GetDlgItem(IDC_TIME_STOA);
			polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_TIME_ATOA);
			polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_ATOA);
			polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_PRM_AATI);
			polWnd->ShowWindow(SW_HIDE);
		}
		else
		{
			polWnd->SetWindowText(GetString(IDS_STRING33181));
			polWnd = GetDlgItem(IDC_TIME_STOA);
			polWnd->ShowWindow(SW_SHOW);
			polWnd = GetDlgItem(IDC_TIME_ATOA);
			polWnd->ShowWindow(SW_SHOW);
			polWnd = GetDlgItem(IDC_ATOA);
			polWnd->ShowWindow(SW_SHOW);
			polWnd = GetDlgItem(IDC_PRM_AATI);
			polWnd->ShowWindow(SW_SHOW);
		}
	}

	polWnd = GetDlgItem(IDC_PRM_AATI);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33182));
	}


	SetWindowText(GetString(IDS_STRING33183));

	
    GetPrivateProfileString(pcgAppName, "PRM-TYPES", "OWHC",pclConfigString, sizeof (pclConfigString), pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	char *pclToken  = strtok(pclConfigCopy,",");
	while (pclToken)
	{
		m_PrmType.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_PrmType.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}

    GetPrivateProfileString(pcgAppName, "PRM-BOOKING-CHANNELS", "",pclConfigString,  sizeof (pclConfigString),pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	pclToken  = strtok(pclConfigCopy,",");
	while (pclToken)
	{
		m_BookingChannel.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_BookingChannel.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}
			
    GetPrivateProfileString(pcgAppName, "PRM-ARRIVAL-LOCATION", "",pclConfigString,  sizeof (pclConfigString), pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	pclToken  = strtok(pclConfigCopy,",");
	while (pclToken)
	{
		m_ArrivalLocation.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_ArrivalLocation.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}
			
    GetPrivateProfileString(pcgAppName, "PRM-GENDER","",pclConfigString,  sizeof (pclConfigString), pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	pclToken  = strtok(pclConfigCopy,",");
	
	while (pclToken)
	{
		m_Gender.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_Gender.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}


    GetPrivateProfileString(pcgAppName, "PRM-LANGUAGES", "",pclConfigString, sizeof (pclConfigString), pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	pclToken  = strtok(pclConfigCopy,",");
	while (pclToken)
	{
		m_Lang.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_Lang.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}

    GetPrivateProfileString(pcgAppName, "PRM-NATIONALITIES", "",pclConfigString, sizeof (pclConfigString), pclConfigPath);
	strcpy(pclConfigCopy,pclConfigString);
	pclToken  = strtok(pclConfigCopy,",");
	while (pclToken)
	{
		m_Natl.AddString(pclToken);
		if (pclToken == pclConfigCopy) 
		{
			m_Natl.SelectString(0,pclToken);
		}
		pclToken = strtok(NULL,",");
	}

	GetPrivateProfileString(pcgAppName, "PRM-PRIDFORMAT",  "N12",pclConfigString, sizeof pclConfigString, pclConfigPath);
	if (!strcmp(pclConfigString,"X50")) {
		imFormat = X50;
	}
	else {
		imFormat = N12;
	}


	if (imFormat == X50)
	{
		m_Prid.SetTypeToString("X(50)", 50, 1);
		m_Prid.SetBKColor(YELLOW);
	}
	else
	{
		m_Prid.SetTypeToString("#(12)", 12, 1);
		m_Prid.SetBKColor(YELLOW);
	}

	m_Name.SetTypeToString("X(32)",32 ,1);
	m_Name.SetBKColor(YELLOW);

	m_Rema.SetTypeToString("X(256)",256,0);
	m_Rema.SetBKColor(WHITE);

	m_Seat.SetTypeToString("X(7)",7,0);
	m_Seat.SetBKColor(WHITE);

	GetPrivateProfileString(pcgAppName, "PRM-OWHCDEFAULT",  "Y",pclConfigString, sizeof pclConfigString, pclConfigPath);
	if (!strcmp(pclConfigString,"O")) {
		m_Owhc1 = 1;
	}
	else if(!strcmp(pclConfigString,"N")){
		m_Owhc1 = 2;
	}
	else {
		m_Owhc1 = 0;
	}

	SetData();
              
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrmDialog::OnOK() 
{
	char flightDate[24];
//	bool blRc;
	FLIGHTDATA *prlFlight = NULL;
	CTime olArrival, olDeparture;
	bool blCancelled;
	bool blAddOneDay;

	if(GetData())
	{
		if(!m_IsReadOnly)
		{
			
			// lli: FlnuAdid must not equal to TfluAdid
			if (strlen(pomDpx->TfluFlno)>0 && pomDpx->FlnuAdid == pomDpx->TfluAdid)
			{
				CString olText;
				olText = GetString(IDS_PRMSAMEADID); // same arrival or departure
				MessageBox(olText, "", MB_ICONEXCLAMATION);
				return;
			}

			if (pomDpx->FlnuAlc[0] != '\0' || pomDpx->FCsgn[0] != '\0')
			{
				
				if (pomDpx->Sati != NULL)
				{
					strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
					flightDate[8] = '\0';
				}
				else
				{
					strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
					flightDate[8] = '\0';
				}
				

				
				//Flight with callsign
				blAddOneDay = false;

				prlFlight = GetFlightByCallsign(pomDpx->FCsgn,flightDate,pomDpx->FlnuAdid,blCancelled, blAddOneDay);
				
				if (blCancelled)
				{
					return;
				}

				if(prlFlight == NULL)
				{
				prlFlight = GetFlightByAlcFltnFlns(pomDpx->FlnuAlc,
					pomDpx->FlnuNumber,pomDpx->FlnuSuffix,flightDate,pomDpx->FlnuAdid,blCancelled, blAddOneDay);
				}

				if (blCancelled)
				{
					return;
				}

				if (prlFlight != NULL)
				{
					pomDpx->Flnu = prlFlight->Urno;
					strcpy(pomDpx->FlnuFlno,prlFlight->Flno);
					olArrival = prlFlight->Stoa;
					olDeparture = prlFlight->Stod;
				}
				else
				{
					CString olText;
					olText = GetString(IDS_PRMFLIGHTNOTFOUND); // flight not found
					MessageBox(olText, "", MB_ICONEXCLAMATION);
					return;

				}
			}
			/***
			else if ((pomDpx->FlnuNumber[0] == '\0')  &&
				(pomDpx->FlnuNumber[0] == '\0'))
			{
				pomDpx->Flnu = 0L;
				pomDpx->FlnuAlc[0] = '\0';
				pomDpx->FlnuNumber[0] = '\0';
				pomDpx->FlnuSuffix[0] = '\0';
				pomDpx->FlnuFlno[0] = '\0';
				pomDpx->FlnuAdid = ' ';
			}
			***/
			


			if (prlFlight == NULL)
			{
				CString olText;
				olText = GetString(IDS_PRMFLIGHTNOTFOUND); // flight not found
				MessageBox(olText, "", MB_ICONEXCLAMATION);
				return;
			}

			//Start of transit flight checking.
			if (pomDpx->TfluAlc[0] != '\0' || pomDpx->TCsgn[0] != '\0')
			{
				FLIGHTDATA *prlFlight = NULL;
				
				if (pomDpx->Sati != NULL)
				{
					strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
					flightDate[8] = '\0';
				}
				else
				{
					strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
					flightDate[8] = '\0';
				}


				//Get Flight for same day.
				blAddOneDay = false;
				//Flight with callsign
				prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled, blAddOneDay);
				if(prlFlight == NULL)
				{
					prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
					pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid, blCancelled, blAddOneDay);
				}
			
				if (prlFlight != NULL)
				{
					// wny: Arrival is later than departure : allow users to select the flight.
					if (pomDpx->TfluAdid=='D')
					{
						if(prlFlight->Stod<olArrival)
						{
							blAddOneDay = true;
							CTime olDate = olArrival;
							strncpy(flightDate,olDate.Format("%Y%m%d"),8);
							flightDate[8] = '\0';
							//Flight with callsign
							prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled, blAddOneDay);
							if (blCancelled)
							{
								return;
							}

							if(prlFlight == NULL)
							{
								prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
								pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid, blCancelled, blAddOneDay);
							}
							if (blCancelled)
							{
								return;
							}
						}
					}
					else
					{
						if(prlFlight->Stoa<olDeparture)
						{
							blAddOneDay = true;
							CTime olDate = olDeparture;
							strncpy(flightDate,olDate.Format("%Y%m%d"),8);
							flightDate[8] = '\0';
							//Flight with callsign
							prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled, blAddOneDay);
							if (blCancelled)
							{
								return;
							}

							if(prlFlight == NULL)
							{
								prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
								pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid, blCancelled, blAddOneDay);
							}
							if (blCancelled)
							{
								return;
							}
						}
					}

				}

				if (prlFlight != NULL)
				{
					pomDpx->Tflu = prlFlight->Urno;
					strcpy(pomDpx->TfluFlno,prlFlight->Flno);
				}
				else
				{
					CString olText;
					olText = GetString(IDS_PRMTFLIGHTNOTFOUND); // flight not found
					MessageBox(olText, "", MB_ICONEXCLAMATION);
					return;
				}

				/*
				if (prlFlight != NULL)
				{
					// lli: the departure flight must not be earlier than arrival flight otherwise we will load next day's departure flight
					
					blAddOneDay = true;

					if (pomDpx->TfluAdid=='D')
					{
						if(prlFlight->Stod<olArrival)
						{
							//CTime olDate = olArrival+CTimeSpan(1,0,0,0);
							//Will get the flight for the next day inside the GetFlight Functions below
							CTime olDate = olArrival;
							strncpy(flightDate,olDate.Format("%Y%m%d"),8);
							flightDate[8] = '\0';
							
							//CString olText;
							//olText = GetString(IDS_PRMNEXTFLIGHT); // Try to load next date's transit flight
							//MessageBox(olText, "", MB_ICONEXCLAMATION);
							
							//Flight with callsign
							prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled,blAddOneDay);
							if (blCancelled)
							{
								return;
							}
							
							if(prlFlight == NULL)
							{
								prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
									pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid, blCancelled,blAddOneDay);
							}
							if (blCancelled)
							{
								return;
							}
						}
					}
					else
					{
						if(prlFlight->Stoa<olDeparture)
						{
							//CTime olDate = olDeparture+CTimeSpan(1,0,0,0);
							//Will get the flight for the next day inside the GetFlight Functions below
							CTime olDate = olDeparture;
							strncpy(flightDate,olDate.Format("%Y%m%d"),8);
							flightDate[8] = '\0';
							
							//CString olText;
							//olText = GetString(IDS_PRMNEXTFLIGHT); // Try to load next date's transit flight
							//MessageBox(olText, "", MB_ICONEXCLAMATION);
							
							
							//Flight with callsign
							prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled,blAddOneDay);
							if (blCancelled)
							{
								return;
							}

							if(prlFlight == NULL)
							{
								prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
									pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid,blCancelled,blAddOneDay);
							}
							if (blCancelled)
							{
								return;
							}
							
							
						}
					}
					if (prlFlight != NULL)
					{
						pomDpx->Tflu = prlFlight->Urno;
						strcpy(pomDpx->TfluFlno,prlFlight->Flno);
					}
					else
					{
						CString olText;
						olText = GetString(IDS_PRMTFLIGHTNOTFOUND); // flight not found
						MessageBox(olText, "", MB_ICONEXCLAMATION);
						return;
					}
				}
				else
				{
					CTime olDate = olArrival+CTimeSpan(1,0,0,0);
					strncpy(flightDate,olDate.Format("%Y%m%d"),8);
					flightDate[8] = '\0';
					CString olText;
					olText = GetString(IDS_PRMNEXTFLIGHT); // Try to load next date's transit flight
					MessageBox(olText, "", MB_ICONEXCLAMATION);
					
					blAddOneDay = false;
					//Flight with callsign
					prlFlight = GetFlightByCallsign(pomDpx->TCsgn,flightDate,pomDpx->TfluAdid,blCancelled,blAddOneDay);
					if (blCancelled)
					{
						return;
					}				
					
					if(prlFlight == NULL)
					{
						prlFlight = GetFlightByAlcFltnFlns(pomDpx->TfluAlc,
							pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid,blCancelled,blAddOneDay);
					}
					if (blCancelled)
					{
						return;
					}
					
					if (prlFlight != NULL)
					{
						pomDpx->Tflu = prlFlight->Urno;
						strcpy(pomDpx->TfluFlno,prlFlight->Flno);
					}
					else
					{
						CString olText;
						olText = GetString(IDS_PRMTFLIGHTNOTFOUND); // flight not found
						MessageBox(olText, "", MB_ICONEXCLAMATION);
						return;
					}
				}
				*/
			}
			else if ((pomDpx->TfluNumber[0] == '\0')  &&
				(pomDpx->TfluNumber[0] == '\0'))
			{
				pomDpx->Tflu = 0L;
				pomDpx->TfluAlc[0] = '\0';
				pomDpx->TfluNumber[0] = '\0';
				pomDpx->TfluSuffix[0] = '\0';
				pomDpx->TfluFlno[0] = '\0';
				pomDpx->TfluAdid = ' ';
				pomDpx->TfluPrms[0] = '\0';
				pomDpx->TfluPrmu[0] = '\0';
			}

		}
	}
	else
	{
		return;
	}

	EndDialog(IDOK);
}

FLIGHTDATA* CPrmDialog::GetFlightByAlcFltnFlns(const char *pcpAlc,const char *pcpFltn, 
							const char *pcpFlns,const char *pcpFdat,char cpAdid,bool& rbpCancelled, bool& rbpAddOneday)
{
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pcpAlc,pcpFltn,pcpFlns,pcpFdat,cpAdid,false);

	if(rbpAddOneday)
	{
		CString clFdate = (CString)pcpFdat;
		CTime olDate(atoi(clFdate.Mid(0,4)),atoi(clFdate.Mid(4,2)),atoi(clFdate.Mid(6,2)),0,0,0); //DateStringToDate(clFdate);
		olDate = olDate +CTimeSpan(1,0,0,0);
		clFdate = olDate.Format("%Y%m%d");
		ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pcpAlc,pcpFltn,pcpFlns,clFdate,cpAdid,false);
	}



	rbpCancelled = false;
	int ilFlightCount = olFlights.GetSize();
	if (ilFlightCount == 1)
	{
		prlFlight = &olFlights[0];
	}
	if (rbpAddOneday && ilFlightCount > 0)
	{
		CString olText = CString("");
		CString olSelectedFlight = CString("");
		CSelReportDlg olSelReportDlg(CString("Select Flight"));
		FLIGHTDATA *prlTmpFlight;
		for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
		{
			prlTmpFlight = &olFlights[ilLc];
			olText = CString("") + prlTmpFlight->Flno;
			olText += CString(" ");
			olText += prlTmpFlight->Adid;
			olText += CString(" ");
			olText += ogFlightData.GetFlightTime(prlTmpFlight).Format("%H%M");
			olText += CString(" ");
			CString szState;
			if (ogFlightData.IsArrival(prlTmpFlight))
			{
				szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisa));
				olText += szState;
				olText += CString("   STA:");
				olText += prlTmpFlight->Stoa.Format("%H:%M   %d/%m/%Y");
			}
			else
			{
				szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisd));
				olText += szState;
				olText += CString("   STD:");
				olText += prlTmpFlight->Stod.Format("%H:%M   %d/%m/%Y");
			}
			olText += CString(" ");
			olText += CString("  (")+prlTmpFlight->Regn+CString(")");
			olSelReportDlg.SetReports(olText);
		}
		if(olSelReportDlg.DoModal() == IDOK)
		{
			olSelectedFlight = olSelReportDlg.omSelectedReport;
			for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
			{
				prlTmpFlight = &olFlights[ilLc];
				olText = CString("") + prlTmpFlight->Flno;
				olText += CString(" ");
				olText += prlTmpFlight->Adid;
				olText += CString(" ");
				olText += ogFlightData.GetFlightTime(prlTmpFlight).Format("%H%M");
				olText += CString(" ");
				CString szState;
				if (ogFlightData.IsArrival(prlTmpFlight))
				{
					szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisa));
					olText += szState;
					olText += CString("   STA:");
					olText += prlTmpFlight->Stoa.Format("%H:%M   %d/%m/%Y");
				}
				else
				{
					szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisd));
					olText += szState;
					olText += CString("   STD:");
					olText += prlTmpFlight->Stod.Format("%H:%M   %d/%m/%Y");
				}
				olText += CString(" ");
				olText += CString("  (")+prlTmpFlight->Regn+CString(")");
				if (!strcmp(olSelectedFlight,olText))
				{
					prlFlight = prlTmpFlight;
				}
			}
		}
		else
		{
			rbpCancelled = true;
		}
	}
	//else
	//{
	//	prlFlight = NULL;
	//}
	return prlFlight;
}


FLIGHTDATA* CPrmDialog::GetFlightByCallsign(const char *pcpCsgn,const char *pcpFdat,char cpAdid,bool& rbpCancelled, bool& rbpAddOneday)
{
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	
	ogFlightData.GetFlightsByCallsign(olFlights,pcpCsgn,pcpFdat,cpAdid,false);

	if(rbpAddOneday)
	{
		CString clFdate = (CString)pcpFdat;
		CTime olDate(atoi(clFdate.Mid(0,4)),atoi(clFdate.Mid(4,2)),atoi(clFdate.Mid(6,2)),0,0,0); //DateStringToDate(clFdate);
		olDate = olDate +CTimeSpan(1,0,0,0);
		clFdate = olDate.Format("%Y%m%d");
		ogFlightData.GetFlightsByCallsign(olFlights,pcpCsgn,clFdate,cpAdid,false);
	}


	rbpCancelled = false;
	int ilFlightCount = olFlights.GetSize();
	if (ilFlightCount == 1)
	{
		prlFlight = &olFlights[0];
	}
	if (rbpAddOneday && ilFlightCount > 0)
	{
		CString olText = CString("");
		CString olSelectedFlight = CString("");
		CSelReportDlg olSelReportDlg(CString("Select Flight"));
		FLIGHTDATA *prlTmpFlight;
		for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
		{
			prlTmpFlight = &olFlights[ilLc];
			olText = CString("") + prlTmpFlight->Flno;
			olText += CString(" ");
			olText += prlTmpFlight->Adid;
			olText += CString(" ");
			olText += ogFlightData.GetFlightTime(prlTmpFlight).Format("%H%M");
			olText += CString(" ");
			CString szState;
			if (ogFlightData.IsArrival(prlTmpFlight))
			{
				szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisa));
				olText += szState;
				olText += CString("   STA:");
				olText += prlTmpFlight->Stoa.Format("%H:%M   %d/%m/%Y");
			}
			else
			{
				szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisd));
				olText += szState;
				olText += CString("   STD:");
				olText += prlTmpFlight->Stod.Format("%H:%M   %d/%m/%Y");
			}
			olText += CString(" ");
			olText += CString("  (")+prlTmpFlight->Regn+CString(")");
			olSelReportDlg.SetReports(olText);
		}
		if(olSelReportDlg.DoModal() == IDOK)
		{
			olSelectedFlight = olSelReportDlg.omSelectedReport;
			for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
			{
				prlTmpFlight = &olFlights[ilLc];
				olText = CString("") + prlTmpFlight->Flno;
				olText += CString(" ");
				olText += prlTmpFlight->Adid;
				olText += CString(" ");
				olText += ogFlightData.GetFlightTime(prlTmpFlight).Format("%H%M");
				olText += CString(" ");
				CString szState;
				if (ogFlightData.IsArrival(prlTmpFlight))
				{
					szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisa));
					olText += szState;
					olText += CString("   STA:");
					olText += prlTmpFlight->Stoa.Format("%H:%M   %d/%m/%Y");
				}
				else
				{
					szState.Format("(%s)",(const char *)GetTifName(prlTmpFlight->Tisd));
					olText += szState;
					olText += CString("   STD:");
					olText += prlTmpFlight->Stod.Format("%H:%M   %d/%m/%Y");
				}
				olText += CString(" ");
				olText += CString("  (")+prlTmpFlight->Regn+CString(")");
				if (!strcmp(olSelectedFlight,olText))
				{
					prlFlight = prlTmpFlight;
				}
			}
		}
		else
		{
			rbpCancelled = true;
		}
	}
	//else
	//{
	//	prlFlight = NULL;
	//}
	return prlFlight;
}



CString CPrmDialog::GetTifName(const char *popTis)
{
	CString olResult;
	if (!popTis)
		return olResult;

	switch(popTis[0])
	{
	case 'S' :
		olResult = GetString(IDS_STRING61898);
	break;
	case 'E' :
		olResult = GetString(IDS_STRING61899);
	break;
	case 'T' :
		olResult = GetString(IDS_STRING61900);
	break;
	case 'L' :
		olResult = GetString(IDS_STRING61901);
	break;
	case 'O' :
		olResult = GetString(IDS_STRING61902);
	break;
	case 'A' :
		olResult = GetString(IDS_STRING61903);
	break;
	}

	return olResult;
}

void CPrmDialog::OnGetFlnuTime()
{
	OnKillfocusGetTime(FLIGHT);
	/*
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->FlnuAlc[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pomDpx->FlnuAlc,
				pomDpx->FlnuNumber,pomDpx->FlnuSuffix,flightDate,pomDpx->FlnuAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_FlnuTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_FlnuTime.SetWindowText(" ");
			}

		}

		if(pomDpx->FCsgn[0] != '\0')
		{
			OnKillfocusFCsgnTime();
		}
	}
	*/
}

void CPrmDialog::OnGetTfluTime()
{
	OnKillfocusGetTime(TRANSIT);
	/*
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->TfluAlc[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pomDpx->TfluAlc,
				pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_TfluTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_TfluTime.SetWindowText(" ");
			}
		}
		if(pomDpx->TCsgn[0] != '\0')
		{
			OnKillfocusTCsgnTime();
		}
	}
	*/
}


void CPrmDialog::OnKillfocusFlnuTime() 
{
	OnKillfocusGetTime(FLIGHT);
	/*
	// TODO: Add your control notification handler code here
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->FlnuAlc[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pomDpx->FlnuAlc,
				pomDpx->FlnuNumber,pomDpx->FlnuSuffix,flightDate,pomDpx->FlnuAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_FlnuTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_FlnuTime.SetWindowText(" ");
			}

		}
	}
	*/
}


void CPrmDialog::OnKillfocusTfluTime() 
{
	OnKillfocusGetTime(TRANSIT);
	/*
	// TODO: Add your control notification handler code here
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->TfluAlc[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByAlcFltnFlns(olFlights,pomDpx->TfluAlc,
				pomDpx->TfluNumber,pomDpx->TfluSuffix,flightDate,pomDpx->TfluAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_TfluTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_TfluTime.SetWindowText(" ");
			}
		}
	}
	*/
}

void CPrmDialog::OnKillfocusFCsgnTime() 
{
	OnKillfocusGetTime(FLIGHT);
	/*
	// TODO: Add your control notification handler code here
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->FCsgn[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByCallsign(olFlights,pomDpx->FCsgn,pomDpx->FlnuAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_FlnuTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_FlnuTime.SetWindowText(" ");
			}

		}
	}
	*/
}

void CPrmDialog::OnKillfocusTCsgnTime() 
{
	OnKillfocusGetTime(TRANSIT);
	/*
	// TODO: Add your control notification handler code here
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olFlights;
	FLIGHTDATA *prlFlight = NULL;
	if(GetData())
	{
		if (pomDpx->TCsgn[0] != '\0')
		{
			
			if (pomDpx->Sati != NULL)
			{
				strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			else
			{
				strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
				flightDate[8] = '\0';
			}
			
			ogFlightData.GetFlightsByCallsign(olFlights,pomDpx->TCsgn,pomDpx->FlnuAdid);
			int ilFlightCount = olFlights.GetSize();
			if (ilFlightCount >= 1)
			{
				prlFlight = &olFlights[0];
				CTime olTmpTime;
				if (*prlFlight->Adid == 'A') 
				{
					olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
				}
				else
				{
					olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
				}
				m_TfluTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else
			{
				m_TfluTime.SetWindowText(" ");
			}

		}
	}
	*/
}


void CPrmDialog::OnKillfocusGetTime(FlightType type) 
{
	char flightDate[24];
	CCSPtrArray<FLIGHTDATA> olAlcFlights;
	CCSPtrArray<FLIGHTDATA> olCsgnFlights;

	
	
	char flnuAlc[6];
	char flNumber[8];
	char flSuffix[2];
	char adid;
	char csgn[9];

	bool checkAlc = false;
	bool checkCsgn = false;
	bool hasFlight = false;
	
	if(GetData())
	{
		if (pomDpx->Sati != NULL)
		{
			strncpy(flightDate,pomDpx->Sati.Format("%Y%m%d"),8);
			flightDate[8] = '\0';
		}
		else
		{
			strncpy(flightDate,pomDpx->Cdat.Format("%Y%m%d"),8);
			flightDate[8] = '\0';
		}

		if(type == FLIGHT)
		{
			checkAlc  = (pomDpx->FlnuAlc[0] == '\0')? false : true;
			checkCsgn = (pomDpx->FCsgn[0] == '\0')? false: true;
			strncpy(flnuAlc,pomDpx->FlnuAlc,6);
			strncpy(flNumber, pomDpx->FlnuNumber,8);
			strncpy(flSuffix, pomDpx->FlnuSuffix,2);
			adid = pomDpx->FlnuAdid;
			strncpy(csgn,pomDpx->FCsgn,9);
						
		}
		else if (type = TRANSIT)
		{
			checkAlc  = (pomDpx->TfluAlc[0] == '\0')? false : true;
			checkCsgn = (pomDpx->TCsgn[0] == '\0')? false: true;
			strncpy(flnuAlc,pomDpx->TfluAlc,6);
			strncpy(flNumber, pomDpx->TfluNumber,8);
			strncpy(flSuffix, pomDpx->TfluSuffix,2);
			adid = pomDpx->TfluAdid;
			strncpy(csgn,pomDpx->TCsgn,9);
		}

		//LoadFlightByAlc
		if(checkAlc)
		{
			ogFlightData.GetFlightsByAlcFltnFlns(olAlcFlights,flnuAlc,flNumber,flSuffix,flightDate,adid);
		}
		//LoadFlightByCsgn
		if(checkCsgn)
		{
			ogFlightData.GetFlightsByCallsign(olCsgnFlights,csgn,flightDate,adid);
		}

		int ilAlcFlightCount = olAlcFlights.GetSize();
		int ilCsgnFlightCount = olCsgnFlights.GetSize();
		FLIGHTDATA *prlAlcFlight = NULL;
		FLIGHTDATA *prlCsgnFlight = NULL;
		FLIGHTDATA *prlFlight = NULL;


		if(ilAlcFlightCount >=1)
		{
			prlAlcFlight = &olAlcFlights[0];
			prlFlight = prlAlcFlight;
			hasFlight = true;
		}

		if(ilCsgnFlightCount >=1)
		{
			prlCsgnFlight = &olCsgnFlights[0];
			prlFlight = prlCsgnFlight;
			hasFlight = true;
		}

		if(prlAlcFlight != NULL && prlCsgnFlight != NULL)
		{
			if(prlAlcFlight->Urno != prlCsgnFlight->Urno)
			{
				hasFlight = false;
			}
		}

		if (hasFlight)
		{
			CTime olTmpTime;
			if (*prlFlight->Adid == 'A') 
			{
				olTmpTime = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
			}
			else
			{
				olTmpTime = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
			}
			if(type == FLIGHT)
			{
				m_FlnuTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
			else if(type == TRANSIT)
			{
				m_TfluTime.SetWindowText(olTmpTime.Format("%H:%M"));
			}
		}
		else
		{
			if(type == FLIGHT)
			{
				m_FlnuTime.SetWindowText(" ");
			}
			else if(type == TRANSIT)
			{
				m_TfluTime.SetWindowText(" ");
			}			
		}
	}
}


void CPrmDialog::OnSelchangePrmtcombo() 
{
    char clText[64];
	int ilSel = m_PrmType.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_PrmType.GetLBText(ilSel,clText);
	}
	if (!strcmp(clText,"WCHC")||!strcmp(clText,"WCHR")||!strcmp(clText,"WCHS")||!strcmp(clText,"WCOXY"))
	{
		m_Owhc1 = 0;
	}
	else
	{
		m_Owhc1 = 2;
	}

	UpdateData(FALSE);
}

