#if !defined(AFX_DEBUGINFOLOGDLG_H__F0615C41_2D58_11D4_93AD_0050DAE32E69__INCLUDED_)
#define AFX_DEBUGINFOLOGDLG_H__F0615C41_2D58_11D4_93AD_0050DAE32E69__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DebugInfoLogDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDebugInfoLogDlg dialog

class CDebugInfoLogDlg : public CDialog
{
// Construction
public:
	CDebugInfoLogDlg(CWnd* pParent = NULL);   // standard constructor
	~CDebugInfoLogDlg();
	void UpdateList(CString *popMessage);

	CWnd *pomParent;
	int imID;
	bool bmShowUpdates, bmForeground;
	CStringArray omBufferedUpdates;

// Dialog Data
	//{{AFX_DATA(CDebugInfoLogDlg)
	enum { IDD = IDD_DEBUG_INFO_LOG_DLG };
	CListBox	m_ListCtrl;
	CString		m_List;
	BOOL		m_ShowUpdates;
	BOOL	m_Foreground;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugInfoLogDlg)
	public:
	virtual BOOL Create();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDebugInfoLogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnStopstart();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnForeground();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGINFOLOGDLG_H__F0615C41_2D58_11D4_93AD_0050DAE32E69__INCLUDED_)
