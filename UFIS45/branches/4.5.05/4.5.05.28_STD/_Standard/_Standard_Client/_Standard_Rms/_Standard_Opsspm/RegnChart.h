#if !defined(AFX_REGNCHART_H__E70E8872_A7F5_11D3_AC65_0000B45FD487__INCLUDED_)
#define AFX_REGNCHART_H__E70E8872_A7F5_11D3_AC65_0000B45FD487__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegnChart.h : header file
//
#ifndef _CHART_STATE_
#define _CHART_STATE_

enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// RegnChart frame

class RegnDiagram;

class RegnChart : public CFrameWnd
{
	friend RegnDiagram;

	DECLARE_DYNCREATE(RegnChart)
protected:
	RegnChart();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
    int GetHeight();
	void InvalidateGantt();
    ChartState GetState(void) { return imState; };
    void SetState(ChartState ipState) { imState = ipState; };
    
    CTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    RegnDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupNo; };
    void SetViewer(RegnDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
        imGroupNo = ipGroupNo;
    };
    
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    RegnGantt *GetGanttPtr(void) { return &omGantt; };
    CCSButtonCtrl *GetChartButtonPtr(void) { return &omButton; };
    C3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RegnChart)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~RegnChart();

	// Generated message map functions
	//{{AFX_MSG(RegnChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
    ChartState imState;
    int imHeight;
    
    CCSButtonCtrl omButton;
    C3DStatic *pomTopScaleText;
    
    CTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    RegnDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
    CTimeSpan omInterval;
    RegnGantt omGantt;

private:    
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
    static int imStartTopScaleTextPos;
    static int imStartVerticalScalePos;

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGNCHART_H__E70E8872_A7F5_11D3_AC65_0000B45FD487__INCLUDED_)
