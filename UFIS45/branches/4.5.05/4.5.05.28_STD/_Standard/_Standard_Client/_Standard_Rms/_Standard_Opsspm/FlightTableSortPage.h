// FlightTableSortPage.h : header file
//

#ifndef _FLPLANSO_H_
#define _FLPLANSO_H_

/////////////////////////////////////////////////////////////////////////////
// FlightTableSortPage dialog

class FlightTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FlightTableSortPage)

// Construction
public:
	FlightTableSortPage();
	~FlightTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;
	//{{AFX_DATA(FlightTableSortPage)
	enum { IDD = IDD_FLIGHTTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlightTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _FLPLANSO_H_
