//
// CedaReqData.h - demand<->function list
//
#ifndef _CEDAREQDATA_H_
#define _CEDAREQDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ReqDataStruct
{
	long	Urno;		// Unique Record Number
	long	Urud;		// Rule demand URNO (RUDTAB.URNO)
	char	Eqco[6];	// Equipment Code (EQUTAB.GCDE)
	long	Uequ;		// Equipment URNO (EQUTAB.URNO)
};

typedef struct ReqDataStruct REQDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaReqData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <REQDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUrudMap;
	CString GetTableName(void);

// Operations
public:
	CedaReqData();
	~CedaReqData();

	bool ReadReqData(CDWordArray &ropTplUrnos);
	REQDATA *GetReqByUrno(long lpUrno);
	REQDATA *GetReqByUrud(long lpUrud);
	void ProcessReqBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	REQDATA *AddReqInternal(REQDATA &rrpReq);
	void DeleteReqInternal(long lpUrno);
	void ClearAll();
};


extern CedaReqData ogReqData;
#endif _CEDAREQDATA_H_
